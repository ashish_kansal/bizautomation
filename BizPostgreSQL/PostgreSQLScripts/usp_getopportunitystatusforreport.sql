-- Stored procedure definition script usp_GetOpportunityStatusForReport for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION usp_GetOpportunityStatusForReport(                
  --                
v_numDomainID NUMERIC,                  
 v_dtDateFrom TIMESTAMP,                  
 v_intSalesPurchase NUMERIC DEFAULT 0,                
 v_numUserCntID NUMERIC DEFAULT 0,                            
 v_intType NUMERIC DEFAULT 0,                
 v_tintRights SMALLINT DEFAULT 1, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_SQL  VARCHAR(8000);
BEGIN
   If v_tintRights = 1 then                
 --All Records Rights                  
 
      IF v_dtDateFrom = '1753-01-01 12:00:00'::TIMESTAMP then
 
         open SWV_RefCur for
         SELECT OM.numOppId,OM.intPEstimatedCloseDate as DueDate,
  OM.vcpOppName as OpportunityName,OM.monPAmount as Amount,
  coalesce(GetOppLstMileStoneCompltd(numOppId),'') as LastMileStoneCompleted,
  coalesce(GetOppLstStageCompltd(numOppId),'') as LastStageCompleted
         FROM OpportunityMaster OM
         WHERE  OM.numDomainId = v_numDomainID
         and OM.tintoppstatus = 0
         AND OM.tintopptype = v_intSalesPurchase;
      ELSE
         open SWV_RefCur for
         SELECT OM.numOppId,OM.intPEstimatedCloseDate as DueDate,
  OM.vcpOppName as OpportunityName,OM.monPAmount as Amount,
  coalesce(GetOppLstMileStoneCompltd(numOppId),'') as LastMileStoneCompleted,
  coalesce(GetOppLstStageCompltd(numOppId),'') as LastStageCompleted
         FROM OpportunityMaster OM
         WHERE OM.numDomainId = v_numDomainID
         and OM.tintoppstatus = 0
         AND OM.tintopptype = v_intSalesPurchase
         AND OM.intPEstimatedCloseDate <= v_dtDateFrom;
      end if;
   end if;                  
                  
   If v_tintRights = 2 then
 
      IF v_dtDateFrom = '1753-01-01 12:00:00'::TIMESTAMP then
 
         open SWV_RefCur for
         SELECT
         OM.numOppId,OM.intPEstimatedCloseDate as DueDate,
		OM.vcpOppName as OpportunityName,OM.monPAmount as Amount,
		coalesce(GetOppLstMileStoneCompltd(numOppId),'') as LastMileStoneCompleted,
		coalesce(GetOppLstStageCompltd(numOppId),'') as LastStageCompleted
         FROM
         OpportunityMaster OM
         LEFT JOIN
         AdditionalContactsInformation ADC
         ON
         OM.numContactId = ADC.numContactId
         WHERE
         OM.numDomainId = v_numDomainID
         AND OM.tintoppstatus = 0
         AND OM.tintopptype = v_intSalesPurchase
         AND ADC.numTeam IN(select F.numTeam from ForReportsByTeam F WHERE F.numUserCntID = v_numUserCntID and F.numDomainID = v_numDomainID and F.tintType = v_intType);
      ELSE
         open SWV_RefCur for
         SELECT
         OM.numOppId,OM.intPEstimatedCloseDate as DueDate,
		OM.vcpOppName as OpportunityName,OM.monPAmount as Amount,
		coalesce(GetOppLstMileStoneCompltd(numOppId),'') as LastMileStoneCompleted,
		coalesce(GetOppLstStageCompltd(numOppId),'') as LastStageCompleted
         FROM
         OpportunityMaster OM
         LEFT JOIN
         AdditionalContactsInformation ADC
         ON
         OM.numContactId = ADC.numContactId
         WHERE
         OM.numDomainId = v_numDomainID
         AND OM.tintoppstatus = 0
         AND OM.tintopptype = v_intSalesPurchase
         AND OM.intPEstimatedCloseDate <= v_dtDateFrom
         AND ADC.numTeam IN(SELECT F.numTeam FROM ForReportsByTeam F WHERE F.numUserCntID = v_numUserCntID AND F.numDomainID = v_numDomainID AND F.tintType = v_intType);
      end if;
   end if;                  
                  

   If v_tintRights = 3 then
 
      IF v_dtDateFrom = '1753-01-01 12:00:00'::TIMESTAMP then
 
         open SWV_RefCur for
         SELECT OM.numOppId,OM.intPEstimatedCloseDate as DueDate,
  OM.vcpOppName as OpportunityName,OM.monPAmount as Amount,
  coalesce(GetOppLstMileStoneCompltd(numOppId),'') as LastMileStoneCompleted,
  coalesce(GetOppLstStageCompltd(numOppId),'') as LastStageCompleted
         FROM
         OpportunityMaster OM
         INNER JOIN
         DivisionMaster DM
         ON
         OM.numDivisionId = DM.numDivisionID
         WHERE  OM.numDomainId = v_numDomainID
         AND OM.tintoppstatus = 0
         AND OM.tintopptype = v_intSalesPurchase
         AND DM.numTerID  in(select F.numTerritory from ForReportsByTerritory F
            where F.numUserCntId = v_numUserCntID and F.numDomainID = v_numDomainID and F.tintType = v_intType);
      ELSE
         open SWV_RefCur for
         SELECT OM.numOppId,OM.intPEstimatedCloseDate as DueDate,
  OM.vcpOppName as OpportunityName,OM.monPAmount as Amount,
  coalesce(GetOppLstMileStoneCompltd(numOppId),'') as LastMileStoneCompleted,
  coalesce(GetOppLstStageCompltd(numOppId),'') as LastStageCompleted
         FROM OpportunityMaster OM
         INNER JOIN
         DivisionMaster DM
         ON
         OM.numDivisionId = DM.numDivisionID
         WHERE  OM.numDomainId = v_numDomainID
         AND OM.tintoppstatus = 0
         AND OM.tintopptype = v_intSalesPurchase
         AND OM.intPEstimatedCloseDate <= v_dtDateFrom
         AND DM.numTerID in(select F.numTerritory from ForReportsByTerritory F
            where F.numUserCntId = v_numUserCntID and F.numDomainID = v_numDomainID and F.tintType = v_intType);
      end if;
   end if;
   RETURN;
END; $$;


