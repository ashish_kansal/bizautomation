-- Function definition script GetCustFldValueOppItems for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION GetCustFldValueOppItems(v_numFldId NUMERIC(18,0),
 v_numRecordId NUMERIC(18,0),
 v_numItemCode NUMERIC(18,0))
RETURNS TEXT LANGUAGE plpgsql
   AS $$
   DECLARE
   v_vcValue  TEXT DEFAULT '';
   v_fld_Type  VARCHAR(50);
   v_numListID  NUMERIC(18,0);
BEGIN
   select   numlistid, fld_type INTO v_numListID,v_fld_Type FROM
   CFW_Fld_Master WHERE
   Fld_id = v_numFldId;
       
   IF EXISTS(SELECT Fld_Value FROM CFW_Fld_Values_OppItems WHERE fld_id = v_numFldId  AND RecId = v_numRecordId) then
	
      select   coalesce(Fld_Value,'') INTO v_vcValue FROM
      CFW_Fld_Values_OppItems WHERE
      fld_id = v_numFldId
      AND RecId = v_numRecordId;
   ELSE
      select   coalesce(Fld_Value,'') INTO v_vcValue FROM
      CFW_FLD_Values_Item WHERE
      Fld_ID = v_numFldId
      AND RecId = v_numItemCode;
   end if;

   IF (v_fld_Type = 'TextBox' OR v_fld_Type = 'TextArea') then
	
      IF v_vcValue = '' then 
         v_vcValue := '-';
      end if;
   ELSEIF v_fld_Type = 'SelectBox'
   then
	
      IF v_vcValue = '' then
         v_vcValue := '-';
      ELSEIF SWF_IsNumeric(v_vcValue) THEN
         v_vcValue := GetListIemName(CAST(v_vcValue AS NUMERIC));
      end if;
   ELSEIF v_fld_Type = 'CheckBox'
   then
	
      v_vcValue :=(CASE WHEN cast(NULLIF(v_vcValue,'') as INTEGER) = 0 THEN 'No' ELSE 'Yes' END);
   ELSEIF v_fld_Type = 'CheckBoxList'
   then
	
      v_vcValue := COALESCE((SELECT string_agg(vcData,',')
      FROM
      Listdetails
      WHERE
      numListItemID IN(SELECT coalesce(Id,0) FROM SplitIDs(v_vcValue,','))),'');
   end if;
	
   IF v_fld_Type = 'DateField' then
	
      IF v_vcValue = '0' OR v_vcValue = '' then
         v_vcValue := NULL;
      end if;
   ELSE
      IF v_vcValue IS NULL then
         v_vcValue := '';
      end if;
   end if;

   RETURN v_vcValue;
END; $$;

