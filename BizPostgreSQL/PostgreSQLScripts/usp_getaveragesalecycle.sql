-- Stored procedure definition script usp_GetAverageSaleCycle for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION usp_GetAverageSaleCycle(              
  --              
v_numDomainID NUMERIC,                
 v_dtDateFrom TIMESTAMP,                
 v_dtDateTo TIMESTAMP,                
 v_numUserCntID NUMERIC DEFAULT 0,                            
 v_intType NUMERIC DEFAULT 0,              
 v_tintRights SMALLINT DEFAULT 1, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
BEGIN
   If v_tintRights = 1 then              
 --All Records Rights                
 
      open SWV_RefCur for
      SELECT vcFirstName || ' ' || vcLastname  as Employee,(select count(*)  from OpportunityMaster OM where OM.tintopptype = 1 and OM.tintoppstatus = 1
         and OM.numrecowner = A.numContactId and OM.numDomainId = v_numDomainID and  OM.bintAccountClosingDate BETWEEN v_dtDateFrom
         AND v_dtDateTo) AS DealsWon,
 GetOppSuccesfullPercentage(numContactId,v_numDomainID,v_dtDateFrom,v_dtDateTo)  AS Closing,
 GetOppAmount(numContactId,v_numDomainID,v_dtDateFrom,v_dtDateTo) as Amount
      FROM AdditionalContactsInformation A where A.numContactId = v_numUserCntID;
   end if;                
                
   If v_tintRights = 2 then
 
      open SWV_RefCur for
      SELECT vcFirstName || ' ' || vcLastname  as Employee,
(select count(*)  from OpportunityMaster OM where OM.tintopptype = 1 and OM.tintoppstatus = 1 and OM.numrecowner = A.numContactId and
         OM.numDomainId = v_numDomainID and  OM.bintAccountClosingDate BETWEEN v_dtDateFrom
         AND v_dtDateTo) AS DealsWon,
 GetOppSuccesfullPercentage(numContactId,v_numDomainID,v_dtDateFrom,v_dtDateTo)  AS Closing,
 GetOppAmount(numContactId,v_numDomainID,v_dtDateFrom,v_dtDateTo) as Amount
      FROM AdditionalContactsInformation A where A.numTeam in(select F.numTeam from ForReportsByTeam F
         where F.numUserCntID = v_numUserCntID and F.numDomainID = v_numDomainID and F.tintType = v_intType);
   end if;                
                
   If v_tintRights = 3 then
 
      open SWV_RefCur for
      SELECT
      vcFirstName || ' ' || vcLastname  AS Employee
		,(SELECT
         COUNT(*)
         FROM
         OpportunityMaster OM
         INNER JOIN
         DivisionMaster DM
         ON
         OM.numDivisionId = DM.numDivisionID
         WHERE
         OM.tintopptype = 1
         AND OM.tintoppstatus = 1
         AND OM.numrecowner = A.numContactId
         AND OM.numDomainId = v_numDomainID
         AND OM.bintAccountClosingDate BETWEEN  v_dtDateFrom AND v_dtDateTo) AS DealsWon
		,GetOppSuccesfullPercentage(numContactId,v_numDomainID,v_dtDateFrom,v_dtDateTo)  AS Closing
		,GetOppAmount(numContactId,v_numDomainID,v_dtDateFrom,v_dtDateTo) as Amount
      FROM
      AdditionalContactsInformation A
      LEFT OUTER JOIN
      DivisionMaster DM
      ON
      A.numDivisionId = DM.numDivisionID
      WHERE
      DM.numTerID IN(SELECT F.numTerritory FROM ForReportsByTerritory F WHERE F.numUserCntId = v_numUserCntID and F.numDomainID = v_numDomainID and F.tintType = v_intType);
   end if;
   RETURN;
END; $$;


