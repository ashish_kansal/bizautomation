-- Stored procedure definition script USP_WarehouseItems_ChangeInternalLocation for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_WarehouseItems_ChangeInternalLocation(v_numDomainID NUMERIC(18,0)
	,v_numItemCode NUMERIC(18,0)
	,v_numWarehouseItemID NUMERIC(18,0)
	,v_numWarehouseID NUMERIC(18,0)
	,v_numWLocationID NUMERIC(18,0))
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   IF NOT EXISTS(SELECT numWareHouseItemID FROM WareHouseItems WHERE numDomainID = v_numDomainID AND numItemID = v_numItemCode AND numWareHouseID = v_numWarehouseID AND numWLocationID = v_numWLocationID AND numWareHouseItemID <> v_numWarehouseItemID) then
	
      UPDATE
      WareHouseItems
      SET
      numWLocationID = v_numWLocationID
      WHERE
      numWareHouseItemID = v_numWarehouseItemID;
   ELSE
      RAISE EXCEPTION 'DUPLICATE_EXTERNAL_INTERNARL_COMBINATION';
   end if;
   RETURN;
END; $$;


