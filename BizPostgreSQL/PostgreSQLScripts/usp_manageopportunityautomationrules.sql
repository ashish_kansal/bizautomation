CREATE OR REPLACE FUNCTION USP_ManageOpportunityAutomationRules(v_numDomainID NUMERIC,
    v_strItems TEXT,
    v_tintMode SMALLINT,
    v_tintOppType SMALLINT,INOUT SWV_RefCur refcursor)
LANGUAGE plpgsql
   AS $$
   DECLARE
    my_ex_state TEXT;
	my_ex_message TEXT;
	my_ex_detail TEXT;
	my_ex_hint TEXT;
	my_ex_ctx TEXT;
BEGIN
   IF v_tintMode = 0 then
        
      open SWV_RefCur for
      SELECT * FROM OpportunityAutomationRules WHERE numDomainID = v_numDomainID AND (tintOppType = v_tintOppType OR v_tintOppType = 0);
   ELSEIF v_tintMode = 1
   then

      -- BEGIN TRANSACTION
BEGIN
         IF SUBSTR(CAST(v_strItems AS VARCHAR(10)),1,10) <> '' then
            DROP TABLE IF EXISTS tt_TEMPTABLE CASCADE;
            CREATE TEMPORARY TABLE tt_TEMPTABLE
            (
               numRuleID NUMERIC(18,0),
               numBizDocStatus1 NUMERIC(18,0),
               numBizDocStatus2 NUMERIC(18,0),
               numOrderStatus NUMERIC(18,0)
            );
            INSERT INTO tt_TEMPTABLE
            SELECT
            X.numRuleID,
						X.numBizDocStatus1,
						X.numBizDocStatus2,
						X.numOrderStatus
            FROM
			XMLTABLE
			(
				'NewDataSet/Table'
				PASSING 
					CAST(v_strItems AS XML)
				COLUMNS
					id FOR ORDINALITY,
					numRuleID NUMERIC(18,0) PATH 'numRuleID',
					numBizDocStatus1 NUMERIC(18,0) PATH 'numBizDocStatus1',
					numBizDocStatus2 NUMERIC(18,0) PATH 'numBizDocStatus2',
					numOrderStatus NUMERIC(18,0) PATH 'numOrderStatus'
			) X;
					--TODO CHECK IF SALES FULLFILLMENT RULES CONTAINS CREATE INVOICE RULE

            IF(SELECT COUNT(*) FROM tt_TEMPTABLE WHERE numBizDocStatus1 IN(SELECT coalesce(numListItemID,0) FROM Listdetails WHERE numListID = 11 AND numListType = 287 AND numDomainid = v_numDomainID)) > 0 then
					
               IF(SELECT COUNT(WorkFlowActionList.numWFActionID) FROM WorkFlowActionList INNER JOIN WorkFlowMaster ON WorkFlowActionList.numWFID = WorkFlowMaster.numWFID WHERE tintActionType = 6 AND numBizDocTypeID = 287 AND WorkFlowMaster.numDomainID = v_numDomainID) > 0 then
						
                  RAISE EXCEPTION 'WORKFLOW AUTOMATION RULE EXISTS';
                  RETURN;
               end if;
            end if;

					--TODO CHECK IF SALES FULLFILLMENT RULES CONTAINS CREATE PACKING SLIP RULE
            IF(SELECT COUNT(*) FROM tt_TEMPTABLE WHERE numBizDocStatus1 IN(SELECT coalesce(numListItemID,0) FROM Listdetails WHERE numListID = 11 AND numListType = 29397 AND numDomainid = v_numDomainID)) > 0 then
					
               IF(SELECT COUNT(WorkFlowActionList.numWFActionID) FROM WorkFlowActionList INNER JOIN WorkFlowMaster ON WorkFlowActionList.numWFID = WorkFlowMaster.numWFID WHERE tintActionType = 6 AND numBizDocTypeID = 29397 AND WorkFlowMaster.numDomainID = v_numDomainID) > 0 then
						
                  RAISE EXCEPTION 'WORKFLOW AUTOMATION RULE EXISTS';
                  RETURN;
               end if;
            end if;
            DELETE FROM OpportunityAutomationRules WHERE numDomainID = v_numDomainID AND tintOppType = v_tintOppType;
            INSERT  INTO OpportunityAutomationRules(numDomainID,tintOppType,numRuleID,numBizDocStatus1,numBizDocStatus2,numOrderStatus)
            SELECT
            v_numDomainID,v_tintOppType,numRuleID,numBizDocStatus1,numBizDocStatus2,numOrderStatus
            FROM
            tt_TEMPTABLE;
         end if;
         open SWV_RefCur for
         SELECT  '';
         EXCEPTION WHEN OTHERS THEN
              GET STACKED DIAGNOSTICS
			  my_ex_state   = RETURNED_SQLSTATE,
			  my_ex_message = MESSAGE_TEXT,
			  my_ex_detail  = PG_EXCEPTION_DETAIL,
			  my_ex_hint    = PG_EXCEPTION_HINT,
			  my_ex_ctx     = PG_EXCEPTION_CONTEXT;

			 raise exception '% % % % %', my_ex_state, my_ex_message, my_ex_detail, my_ex_hint, my_ex_ctx;
      END;
      -- COMMIT
end if;
END; $$;
