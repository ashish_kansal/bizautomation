-- Stored procedure definition script USP_ApprovePORequistion for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
-- =============================================
-- Author: Sandeep Patel
-- Create date: 14 July 2014
-- Description:	sets unit price approval required flag to false for all items added in order
-- =============================================
CREATE OR REPLACE FUNCTION USP_ApprovePORequistion(v_numOppId NUMERIC(18,0),
	v_numDomainID NUMERIC(18,0),
	v_ApprovalType INTEGER DEFAULT 0)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   UPDATE
   OpportunityMaster
   SET
   intReqPOApproved = v_ApprovalType
   WHERE
   numOppId = v_numOppId;
   RETURN;
END; $$;


