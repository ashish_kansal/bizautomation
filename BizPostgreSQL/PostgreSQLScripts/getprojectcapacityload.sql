-- Function definition script GetProjectCapacityLoad for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION GetProjectCapacityLoad(v_numDomainID NUMERIC(18,0)
	,v_numUsercntID NUMERIC(18,0)
	,v_numTeamID NUMERIC(18,0)
	,v_numProId NUMERIC(18,0)
	,v_numTaskID NUMERIC(18,0)
	,v_tintDateRange SMALLINT -- 1:Today, 2:Week, 3:Month
	,v_dtFromDate TIMESTAMP
	,v_ClientTimeZoneOffset INTEGER)
RETURNS INTEGER LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numDivisionID  NUMERIC(18,0);
   v_numQtyToBuild  DOUBLE PRECISION;
   v_dtPlannedStartDate  TIMESTAMP;
   v_dtActualStartDate  TIMESTAMP DEFAULT NULL;

   v_i  INTEGER DEFAULT 1;
   v_iCount  INTEGER; 	
   v_numTaskAssignee  NUMERIC(18,0);
   v_intTaskType  INTEGER;
   v_numWorkScheduleID  NUMERIC(18,0);
   v_numTempUserCntID  NUMERIC(18,0);
   v_dtStartDate  TIMESTAMP;
   v_numTotalTaskInMinutes  NUMERIC(18,0);
   v_numProductiveTimeInMinutes  NUMERIC(18,0);
   v_tmStartOfDay  TIME;
   v_numTimeLeftForDay  NUMERIC(18,0);
   v_vcWorkDays  VARCHAR(20);
   v_bitParallelStartSet  BOOLEAN DEFAULT false;

   v_j  INTEGER; 
   v_jCount  INTEGER;
   v_numTimeAndExpenseMinues  NUMERIC(18,0);
   v_numAssemblyBuildMinues  NUMERIC(18,0);

   v_numContactID  NUMERIC(18,0);
BEGIN
   v_dtFromDate := v_dtFromDate+CAST(-1*v_ClientTimeZoneOffset::bigint || 'minute' as interval);

   select   numDivisionId INTO v_numDivisionID FROM Domain WHERE numDomainId = v_numDomainID;
	
   select   1, coalesce(dtmStartDate,bintCreatedDate) INTO v_numQtyToBuild,v_dtPlannedStartDate FROM
   ProjectsMaster WHERE numProId = v_numProId;


   IF EXISTS(SELECT ID FROM StagePercentageDetailsTaskTimeLog WHERE numTaskId IN(SELECT numTaskId FROM StagePercentageDetailsTask WHERE numProjectId = v_numProId) AND tintAction = 1) then
	
      select   MIN(dtActionTime) INTO v_dtActualStartDate FROM StagePercentageDetailsTaskTimeLog WHERE numTaskId IN(SELECT numTaskId FROM StagePercentageDetailsTask WHERE numProjectId = v_numProId) AND tintAction = 1;
   end if;

   DROP TABLE IF EXISTS tt_TEMPTASKTIMESEQUENCE CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPTASKTIMESEQUENCE
   (
      numAssignedTo NUMERIC(18,0),
      dtDate DATE,
      numTaskHours NUMERIC(18,0)
   );

   DROP TABLE IF EXISTS tt_TEMPTASKASSIGNEE CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPTASKASSIGNEE
   (
      numAssignedTo NUMERIC(18,0),
      dtLastTaskCompletionTime TIMESTAMP
   );

   BEGIN
      CREATE TEMP SEQUENCE tt_TempTasks_seq INCREMENT BY 1 START WITH 1;
      EXCEPTION WHEN OTHERS THEN
         NULL;
   END;
   DROP TABLE IF EXISTS tt_TEMPTASKS CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPTASKS
   (
      ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1),
      numTaskID NUMERIC(18,0),
      numTaskTimeInMinutes NUMERIC(18,0),
      numTaskAssignee NUMERIC(18,0),
      intTaskType INTEGER --1:Parallel, 2:Sequential
		,
      dtPlannedStartDate TIMESTAMP
   );

   INSERT INTO tt_TEMPTASKS(numTaskID
		,numTaskTimeInMinutes
		,intTaskType
		,numTaskAssignee)
   SELECT
   SPDT.numTaskId
		,((coalesce(SPDT.numHours,0)*60)+coalesce(SPDT.numMinutes,0))*(v_numQtyToBuild -coalesce((SELECT SUM(numProcessedQty) FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskId = SPDT.numTaskId),0))
		,coalesce(SPDT.intTaskType,1)
		,SPDT.numAssignTo
   FROM
   StagePercentageDetailsTask SPDT
   INNER JOIN
   ProjectsMaster pro
   ON
   SPDT.numProjectId = pro.numProId
   WHERE
   SPDT.numDomainID = v_numDomainID
   AND SPDT.numProjectId = v_numProId
   AND NOT EXISTS(SELECT * FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskId = SPDT.numTaskId AND tintAction = 4); 

   UPDATE tt_TEMPTASKS SET dtPlannedStartDate = v_dtPlannedStartDate;

   INSERT INTO tt_TEMPTASKASSIGNEE(numAssignedTo)
   SELECT DISTINCT
   numTaskAssignee
   FROM
   tt_TEMPTASKS;

   select   COUNT(*) INTO v_iCount FROM tt_TEMPTASKS;

   WHILE v_i <= v_iCount LOOP
      select   numTaskAssignee, numTaskTimeInMinutes, intTaskType INTO v_numTaskAssignee,v_numTotalTaskInMinutes,v_intTaskType FROM
      tt_TEMPTASKS WHERE
      ID = v_i;

		-- GET TASK COMPLETION TIME WHICH WILL BE START DATETIME FOR NEXT TASK
      select   WS.ID, (coalesce(WS.numProductiveHours,0)*60)+coalesce(WS.numProductiveMinutes,0), tmStartOfDay, vcWorkDays, (CASE WHEN v_dtActualStartDate IS NULL THEN CAST(TO_CHAR(v_dtPlannedStartDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval),'yyyymmdd') AS TIMESTAMP)+tmStartOfDay+CAST(v_ClientTimeZoneOffset || 'minute' as interval) ELSE v_dtActualStartDate END) INTO v_numWorkScheduleID,v_numProductiveTimeInMinutes,v_tmStartOfDay,v_vcWorkDays,
      v_dtStartDate FROM
      WorkSchedule WS
      INNER JOIN
      UserMaster
      ON
      WS.numUserCntID = UserMaster.numUserDetailId WHERE
      WS.numUserCntID = v_numTaskAssignee;
      IF v_intTaskType = 1 AND EXISTS(SELECT numAssignedTo FROM tt_TEMPTASKASSIGNEE WHERE numAssignedTo = v_numTaskAssignee AND dtLastTaskCompletionTime IS NOT NULL) then
		
         select   dtLastTaskCompletionTime INTO v_dtStartDate FROM tt_TEMPTASKASSIGNEE WHERE numAssignedTo = v_numTaskAssignee;
      ELSEIF EXISTS(SELECT numAssignedTo FROM tt_TEMPTASKASSIGNEE WHERE numAssignedTo = v_numTaskAssignee AND dtLastTaskCompletionTime IS NOT NULL)
      then
		
         select   dtLastTaskCompletionTime INTO v_dtStartDate FROM tt_TEMPTASKASSIGNEE WHERE numAssignedTo = v_numTaskAssignee;
      end if;
      UPDATE tt_TEMPTASKS SET dtPlannedStartDate = v_dtStartDate WHERE ID = v_i;
      IF v_numProductiveTimeInMinutes > 0 AND v_numTotalTaskInMinutes > 0 AND LENGTH(coalesce(v_vcWorkDays,'')) > 0 then
		
         WHILE v_numTotalTaskInMinutes > 0 LOOP
				-- IF ITS WORKING DAY AND ALSO NOT HOLIDAY THEN PROCEED OR INCRESE DATE TO NEXT DAY
            IF EXTRACT(DOW FROM v_dtStartDate)+1 IN(SELECT Id FROM SplitIDs(v_vcWorkDays,',')) AND NOT EXISTS(SELECT ID FROM WorkScheduleDaysOff WHERE numWorkScheduleID = v_numWorkScheduleID AND CAST(v_dtStartDate+CAST(v_ClientTimeZoneOffset::bigint*-1 || 'minute' as interval) AS DATE) BETWEEN dtDayOffFrom:: DATE AND dtDayOffTo:: DATE) then
				
               IF CAST(v_dtStartDate AS DATE) = CAST(TIMEZONE('UTC',now()) AS DATE) then
					
                  v_numTimeLeftForDay :=(EXTRACT(DAY FROM CAST(TO_CHAR(TIMEZONE('UTC',now())+CAST(-v_ClientTimeZoneOffset || 'minute' as interval),'yyyymmdd') AS TIMESTAMP)+v_tmStartOfDay+CAST(v_ClientTimeZoneOffset || 'minute' as interval)+CAST(v_numProductiveTimeInMinutes || 'minute' as interval) -v_dtStartDate)*60*24+EXTRACT(HOUR FROM CAST(TO_CHAR(TIMEZONE('UTC',now())+CAST(-v_ClientTimeZoneOffset || 'minute' as interval),'yyyymmdd') AS TIMESTAMP)+v_tmStartOfDay+CAST(v_ClientTimeZoneOffset || 'minute' as interval)+CAST(v_numProductiveTimeInMinutes || 'minute' as interval) -v_dtStartDate)*60+EXTRACT(MINUTE FROM CAST(TO_CHAR(TIMEZONE('UTC',now())+CAST(-v_ClientTimeZoneOffset || 'minute' as interval),'yyyymmdd') AS TIMESTAMP)+v_tmStartOfDay+CAST(v_ClientTimeZoneOffset || 'minute' as interval)+CAST(v_numProductiveTimeInMinutes || 'minute' as interval) -v_dtStartDate));

						-- CHECK TIME LEFT FOR DAY BASED
                  IF v_numTimeLeftForDay >= v_numProductiveTimeInMinutes then
						
                     v_numTimeLeftForDay := v_numProductiveTimeInMinutes;
                  end if;
               ELSE
                  v_numTimeLeftForDay := v_numProductiveTimeInMinutes;
               end if;
               IF v_numTimeLeftForDay > 0 then
					
                  INSERT INTO tt_TEMPTASKTIMESEQUENCE(numAssignedTo
							,dtDate
							,numTaskHours)
						VALUES(v_numTaskAssignee
							,v_dtStartDate
							,(CASE WHEN v_numTimeLeftForDay > v_numTotalTaskInMinutes THEN v_numTotalTaskInMinutes ELSE v_numTimeLeftForDay END));
						
                  IF v_numTimeLeftForDay > v_numTotalTaskInMinutes then
						
                     v_dtStartDate := v_dtStartDate+CAST(v_numTotalTaskInMinutes || 'minute' as interval);
                  ELSE
                     v_dtStartDate := v_dtStartDate+INTERVAL '1 day';
                  end if;
                  v_numTotalTaskInMinutes := v_numTotalTaskInMinutes -v_numTimeLeftForDay;
               ELSE
                  v_dtStartDate := v_dtStartDate+INTERVAL '1 day';
               end if;
            ELSE
               v_dtStartDate := v_dtStartDate+INTERVAL '1 day';
            end if;
         END LOOP;
      end if;
      UPDATE tt_TEMPTASKASSIGNEE SET dtLastTaskCompletionTime = v_dtStartDate WHERE numAssignedTo = v_numTaskAssignee;
      v_i := v_i::bigint+1;
   END LOOP;


   BEGIN
      CREATE TEMP SEQUENCE tt_TEMPEmployee_seq INCREMENT BY 1 START WITH 1;
      EXCEPTION WHEN OTHERS THEN
         NULL;
   END;
   DROP TABLE IF EXISTS tt_TEMPEMPLOYEE CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPEMPLOYEE
   (
      ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1),
      numContactID NUMERIC(18,0),
      numProductiveMinutes NUMERIC(18,0),
      tmStartOfDay TIME,
      vcWorkDays VARCHAR(20),
      numCapacityLoad NUMERIC(18,0)
   );

   INSERT INTO tt_TEMPEMPLOYEE(numContactID
		,numProductiveMinutes
		,tmStartOfDay
		,vcWorkDays)
   SELECT
   AdditionalContactsInformation.numContactId
		,(coalesce(WorkSchedule.numProductiveHours,0)*60)+coalesce(WorkSchedule.numProductiveMinutes,0)
		,WorkSchedule.tmStartOfDay
		,WorkSchedule.vcWorkDays
   FROM
   AdditionalContactsInformation
   LEFT JOIN
   WorkSchedule
   ON
   AdditionalContactsInformation.numContactId = WorkSchedule.numUserCntID
   WHERE
   AdditionalContactsInformation.numDomainID = v_numDomainID
   AND AdditionalContactsInformation.numDivisionId = v_numDivisionID
   AND ((v_numUsercntID > 0 AND AdditionalContactsInformation.numContactId = v_numUsercntID) OR (v_numTeamID > 0 AND AdditionalContactsInformation.numTeam = v_numTeamID));

   v_j := 1;
   select   COUNT(*) INTO v_jCount FROM tt_TEMPEMPLOYEE;
   WHILE v_j <= v_jCount LOOP
      select   numContactID, numProductiveMinutes INTO v_numContactID,v_numProductiveTimeInMinutes FROM
      tt_TEMPEMPLOYEE WHERE
      ID = v_j;
      v_numTimeAndExpenseMinues := coalesce((SELECT
      SUM((EXTRACT(DAY FROM timeandexpense.dtToDate -timeandexpense.dtFromDate)*60*24+EXTRACT(HOUR FROM timeandexpense.dtToDate -timeandexpense.dtFromDate)*60+EXTRACT(MINUTE FROM timeandexpense.dtToDate -timeandexpense.dtFromDate)))
      FROM
      timeandexpense
      WHERE
      numDomainID = v_numDomainID
      AND numUserCntID = v_numContactID
      AND numCategory = 1
      AND numtype IN(1,2)
      AND 1 =(CASE
      WHEN v_tintDateRange = 1 -- Day
      THEN(CASE WHEN CAST(dtFromDate AS DATE) = CAST(v_dtFromDate AS DATE) THEN 1 ELSE 0 END)
      WHEN v_tintDateRange = 2 -- Week
      THEN(CASE WHEN CAST(dtFromDate AS DATE) BETWEEN CAST(v_dtFromDate AS DATE) AND CAST(v_dtFromDate+INTERVAL '7 day' AS DATE) THEN 1 ELSE 0 END) --DATEADD(DAY,2 - DATEPART(WEEKDAY, GETDATE()), CAST(GETDATE() AS DATE))
      WHEN v_tintDateRange = 3 -- Month
      THEN(CASE WHEN CAST(dtFromDate AS DATE) BETWEEN CAST(v_dtFromDate AS DATE) AND CAST(v_dtFromDate + INTERVAL '1 month' + make_interval(secs => -0.3) AS DATE) THEN 1 ELSE 0 END) --CAST(DATEADD(DAY, 1-DAY(GETUTCDATE()), GETUTCDATE()) AS DATE)
      ELSE 0
      END)),0);
      v_numAssemblyBuildMinues := coalesce((SELECT
      SUM(numTaskHours)
      FROM
      tt_TEMPTASKTIMESEQUENCE
      WHERE
      numAssignedTo = v_numContactID
      AND 1 =(CASE
      WHEN v_tintDateRange = 1 -- Day
      THEN(CASE WHEN CAST(dtDate AS DATE) = CAST(v_dtFromDate AS DATE) THEN 1 ELSE 0 END)
      WHEN v_tintDateRange = 2 -- Week
      THEN(CASE WHEN CAST(dtDate AS DATE) BETWEEN CAST(v_dtFromDate AS DATE) AND CAST(v_dtFromDate+INTERVAL '7 day' AS DATE) THEN 1 ELSE 0 END)
      WHEN v_tintDateRange = 3 -- Month
      THEN(CASE WHEN CAST(dtDate AS DATE) BETWEEN CAST(v_dtFromDate AS DATE) AND CAST(v_dtFromDate+INTERVAL '1 month'+ make_interval(secs => -0.3) AS DATE) THEN 1 ELSE 0 END)
      ELSE 0
      END)),0);
      v_numProductiveTimeInMinutes := v_numProductiveTimeInMinutes*(CASE WHEN v_tintDateRange = 1 THEN 1 WHEN v_tintDateRange = 2 THEN 7 WHEN v_tintDateRange = 3 THEN 30 END);
      UPDATE tt_TEMPEMPLOYEE SET numCapacityLoad =(CASE WHEN v_numProductiveTimeInMinutes > 0 THEN(((v_numAssemblyBuildMinues+v_numTimeAndExpenseMinues)*100)/v_numProductiveTimeInMinutes) ELSE 0 END) WHERE ID = v_j;
      v_j := v_j::bigint+1;
   END LOOP;

   RETURN coalesce((SELECT AVG(numCapacityLoad) FROM tt_TEMPEMPLOYEE),0);
END; $$;

