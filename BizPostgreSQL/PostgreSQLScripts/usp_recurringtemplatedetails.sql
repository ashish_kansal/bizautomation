-- Stored procedure definition script USP_RecurringTemplateDetails for PostgreSQL
CREATE OR REPLACE FUNCTION USP_RecurringTemplateDetails(v_numRecurringId NUMERIC(9,0) DEFAULT 0,
v_numDomainId NUMERIC(9,0) DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for Select numRecurringId,
	   varRecurringTemplateName,
	   chrIntervalType,
	   tintIntervalDays,
	   tintMonthlyType,
	   tintFirstDet,
	   tintWeekDays,
	   tintMonths,
	   dtStartDate,
	   dtEndDate,
	   bitEndType,
	   bitRecurring,
	   bitEndTransactionType,
	   numNoTransaction,
	   numDomainID,
	   coalesce(intBillingBreakup,0) AS intBillingBreakup,
	   vcBreakupPercentage,
	   coalesce(bitBillingTerms,false) AS bitBillingTerms,
	   coalesce(numBillingDays,0) AS numBillingDays,
	   bitBizDocBreakup
   From RecurringTemplate Where numRecurringId = v_numRecurringId And numDomainID = v_numDomainId;
END; $$;












