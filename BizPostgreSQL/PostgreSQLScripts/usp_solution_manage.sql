-- Stored procedure definition script USP_Solution_Manage for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_Solution_Manage(v_numCaseID NUMERIC(9,0) DEFAULT null,
v_numSolnID NUMERIC(9,0) DEFAULT NULL,
v_mode BOOLEAN DEFAULT NULL)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   IF v_mode = false then

      IF NOT EXISTS(SELECT * FROM CaseSolutions where numCaseId = v_numCaseID and numSolnID = v_numSolnID) then
	
         insert into CaseSolutions(numCaseId,numSolnID)
		values(v_numCaseID,v_numSolnID);
      end if;
   end if;

   IF v_mode = true then

      delete from CaseSolutions where numCaseId = v_numCaseID and numSolnID = v_numSolnID;
   end if;
/****** Object:  StoredProcedure [dbo].[USP_SortItemList]    Script Date: 07/26/2008 16:21:21 ******/
   RETURN;
END; $$;


