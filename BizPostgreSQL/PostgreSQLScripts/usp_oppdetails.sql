-- Stored procedure definition script USP_OPPDetails for PostgreSQL
CREATE OR REPLACE FUNCTION USP_OPPDetails(v_numOppID             NUMERIC(9,0)  DEFAULT NULL,
               v_numDomainID          NUMERIC(9,0)  DEFAULT 0,
               v_ClientTimeZoneOffset INTEGER DEFAULT NULL,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT Opp.numOppId,
         numCampainID,
         CONCAT(ADC.vcFirstName,' ',ADC.vcLastname) AS numContactId,
         coalesce(ADC.vcEmail,'') AS vcEmail,
         coalesce(ADC.numPhone,'') AS Phone,
         coalesce(ADC.numPhoneExtension,'') AS PhoneExtension,
         Opp.txtComments,
         Opp.numDivisionId,
         tintopptype,
         cast(coalesce(bintAccountClosingDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval),'1900-01-01 00:00:00.000') as TIMESTAMP) AS bintAccountClosingDate,
         Opp.numContactId AS ContactID,
         tintSource,
		 tintSourceType,
         C2.vcCompanyName || Case when coalesce(D2.numCompanyDiff,0) > 0 then  '  ' || fn_GetListItemName(D2.numCompanyDiff) || ':' || coalesce(D2.vcCompanyDiff,'') else '' end as vcCompanyname,
		 (SELECT  coalesce(vcData,'') FROM Listdetails WHERE numListID = 5 AND numListItemID = C2.numCompanyType AND numDomainid = v_numDomainID LIMIT 1) AS vcCompanyType,
         D2.tintCRMType,
         Opp.vcpOppName,
         intPEstimatedCloseDate,
         GetDealAmount(v_numOppID,TIMEZONE('UTC',now()),0::NUMERIC) AS monPAmount,
         lngPConclAnalysis,
         monPAmount AS OppAmount,
         numSalesOrPurType,
         Opp.tintActive,
		  fn_GetContactName(Opp.numCreatedBy) AS CreatedBy,
         --dbo.fn_GetContactName(Opp.numCreatedby)
         --  + ' '
         --  + CONVERT(VARCHAR(20),dateadd(MINUTE,-@ClientTimeZoneOffset,Opp.bintCreatedDate)) AS CreatedBy,
         fn_GetContactName(Opp.numModifiedBy)
   || ' '
   || TO_CHAR(Opp.bintModifiedDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval),'Mon dd yyyy hh:miAM') AS ModifiedBy,
         fn_GetContactName(Opp.numrecowner) AS RecordOwner,
         Opp.numrecowner,
         coalesce(tintshipped,0) AS tintshipped,
         coalesce(tintoppstatus,0) AS tintOppStatus,
         OpportunityLinkedItems(v_numOppID) AS NoOfProjects,
         Opp.numassignedto,
         Opp.numAssignedBy,
         (SELECT COUNT(* )
      FROM   GenericDocuments
      WHERE  numRecID = v_numOppID
      AND vcDocumentSection = 'O') AS DocumentCount,
         coalesce(OPR.numRecurringId,0) AS numRecurringId,
		 OPR.dtRecurringDate AS dtLastRecurringDate,
         D2.numTerID,
         Link.numOppID AS numParentOppID,
         Link.vcPoppName AS ParentOppName,
         Link.vcSource AS Source,
         Link.numParentProjectID,
         Link.vcProjectName,
         coalesce(C.varCurrSymbol,'') AS varCurrSymbol,
         coalesce(Opp.fltExchangeRate,0) AS fltExchangeRate,
         coalesce(C.chrCurrency,'') AS chrCurrency,
         coalesce(C.numCurrencyID,0) AS numCurrencyID,
		 (Select Count(*) from OpportunityBizDocs
      where numoppid = v_numOppID and bitAuthoritativeBizDocs = 1) As AuthBizDocCount,
		 (SELECT COUNT(distinct numChildOppID) FROM OpportunityLinking OL WHERE OL.numParentOppID = Opp.numOppId) AS ChildCount,
		 (SELECT COUNT(*) FROM RecurringTransactionReport RTR WHERE RTR.numRecTranSeedId = Opp.numOppId)  AS RecurringChildCount
		 ,Opp.bintCreatedDate,
		 coalesce(Opp.bitStockTransfer,false) AS bitStockTransfer,	coalesce(Opp.tintTaxOperator,0) AS tintTaxOperator,
         coalesce(Opp.intBillingDays,0) AS numBillingDays,coalesce(Opp.bitInterestType,false) AS bitInterestType,
         coalesce(Opp.fltInterest,0) AS fltInterest,  coalesce(Opp.fltDiscount,0) AS fltDiscount,
         coalesce(Opp.bitDiscountType,false) AS bitDiscountType,coalesce(Opp.bitBillingTerms,false) AS bitBillingTerms,coalesce(Opp.numDiscountAcntType,0) AS numDiscountAcntType,
         coalesce((SELECT numCountry FROM fn_getOPPAddressDetails(Opp.numOppId,Opp.numDomainId,2::SMALLINT) LIMIT 1),0) AS numShipCountry,
		 coalesce((SELECT numState FROM fn_getOPPAddressDetails(Opp.numOppId,Opp.numDomainId,2::SMALLINT) LIMIT 1),0) AS numShipState,
		 coalesce((SELECT vcState FROM fn_getOPPAddressDetails(Opp.numOppId,Opp.numDomainId,2::SMALLINT) LIMIT 1),'') AS vcShipState,
		 coalesce((SELECT vcCountry FROM fn_getOPPAddressDetails(Opp.numOppId,Opp.numDomainId,2::SMALLINT) LIMIT 1),'')  AS vcShipCountry,
		 coalesce((SELECT vcPostalCode FROM fn_getOPPAddressDetails(Opp.numOppId,Opp.numDomainId,2::SMALLINT) LIMIT 1),'')  AS vcPostalCode,
		 coalesce(vcCouponCode,'') AS vcCouponCode,cast(coalesce(Opp.bitPPVariance,false) as BOOLEAN) AS bitPPVariance,
		 Opp.dtItemReceivedDate,cast(coalesce(bitUseShippersAccountNo,false) as BOOLEAN) AS bitUseShippersAccountNo, coalesce((SELECT DMSA.vcAccountNumber FROm DivisionMasterShippingAccount DMSA WHERE DMSA.numDivisionID = D2.numDivisionID AND DMSA.numShipViaID = Opp.intUsedShippingCompany),'') AS vcShippersAccountNo,
		 coalesce(bitUseMarkupShippingRate,false) AS bitUseMarkupShippingRate,
		 coalesce(numMarkupShippingRate,0) AS numMarkupShippingRate,
		 coalesce(Opp.intUsedShippingCompany,0) AS intUsedShippingCompany,
		 coalesce(Opp.numShippingService,0) AS numOrderShippingService,
		 coalesce(CAST(SR.vcValue2 AS NUMERIC),coalesce(numShippingService,(SELECT  coalesce(SST.intNsoftEnum,0) FROM ShippingServiceTypes AS SST
      WHERE SST.numDomainID = v_numDomainID
      AND SST.numRuleID = 0
      AND SST.vcServiceName IN (SELECT coalesce(OI.vcItemDesc,'') FROM OpportunityItems AS OI
         WHERE OI.numOppId = Opp.numOppId
         AND OI.vcType = 'Service Item') LIMIT 1))) AS numShippingService,
		 coalesce(Opp.monLandedCostTotal,0) AS monLandedCostTotal,Opp.vcLanedCost,
		 coalesce(Opp.vcRecurrenceType,'') AS vcRecurrenceType,
		 coalesce(RecurrenceConfiguration.numRecConfigID,0) AS numRecConfigID,
		 (CASE WHEN coalesce(Opp.vcRecurrenceType,'') = '' THEN 0 ELSE 1 END) AS bitRecur,
		 (CASE WHEN RecurrenceTransaction.numRecTranID IS NULL THEN 0 ELSE 1 END) AS bitRecurred,
		 FormatedDateFromDate(RecurrenceConfiguration.dtStartDate,v_numDomainID) AS dtStartDate,
		 FormatedDateFromDate(RecurrenceConfiguration.dtEndDate,v_numDomainID) AS dtEndDate,
		 RecurrenceConfiguration.vcFrequency  AS vcFrequency,
		 coalesce(RecurrenceConfiguration.bitDisabled,false) AS bitDisabled,
		 coalesce(Opp.dtReleaseDate,'1900-01-01 00:00:00.000') AS dtReleaseDate,
		 coalesce(Opp.dtExpectedDate,NULL::DATE) AS dtExpectedDate,
		 coalesce(Opp.numAccountClass,0) AS numAccountClass,D3.vcPartnerCode || '-' || C3.vcCompanyName as numPartenerSource,
		coalesce(Opp.numPartner,0) AS numPartenerSourceId,
		coalesce(Opp.numPartenerContact,0) AS numPartenerContactId,
		A.vcFirstName || ' ' || A.vcLastname AS numPartenerContact,
		numShipFromWarehouse, CPN.CustomerPartNo,
		Opp.numBusinessProcessID,
		SLP.Slp_Name  AS vcProcessName,
		(CASE WHEN coalesce((SELECT COUNT(*) FROM OpportunityBizDocs WHERE OpportunityBizDocs.numoppid = Opp.numOppId AND numBizDocId IN(287,296,644)),0) > 0 THEN 1 ELSE 0 END) AS bitAuthorativeBizDocAdded
   FROM   OpportunityMaster Opp
   JOIN DivisionMaster D2
   ON Opp.numDivisionId = D2.numDivisionID
   LEFT JOIN DivisionMaster D3 ON Opp.numPartner = D3.numDivisionID
   LEFT JOIN CompanyInfo C3 ON C3.numCompanyId = D3.numCompanyID
   LEFT JOIN AdditionalContactsInformation A ON Opp.numPartenerContact = A.numContactId
   LEFT JOIN Sales_process_List_Master AS SLP ON Opp.numBusinessProcessID = SLP.Slp_Id
   LEFT JOIN CompanyInfo C2
   ON C2.numCompanyId = D2.numCompanyID
   LEFT JOIN(SELECT  OM.vcpOppName,
                                 OM.numOppId,
                                 numChildOppID,
                                 vcSource,
                                 numParentProjectID,
                                 vcProjectName
      FROM   OpportunityLinking
      LEFT JOIN OpportunityMaster OM
      ON numOppId = numParentOppID
      LEFT JOIN ProjectsMaster
      ON numParentProjectID = numProId
      WHERE  numChildOppID = v_numOppID LIMIT 1) Link
   ON Link.numChildOppID = Opp.numOppId
   LEFT OUTER JOIN Currency C
   ON C.numCurrencyID = Opp.numCurrencyID
   LEFT OUTER JOIN OpportunityRecurring OPR
   ON OPR.numOppId = Opp.numOppId
   LEFT OUTER JOIN AdditionalContactsInformation ADC ON
   ADC.numDivisionId = D2.numDivisionID AND  ADC.numContactId = Opp.numContactId
   LEFT JOIN RecurrenceConfiguration ON Opp.numOppId = RecurrenceConfiguration.numOppID AND RecurrenceConfiguration.numType = 1
   LEFT JOIN RecurrenceTransaction ON Opp.numOppId = RecurrenceTransaction.numRecurrOppID
   LEFT JOIN ShippingReport AS SR ON SR.numOppID = Opp.numOppId AND SR.numDomainID = Opp.numDomainId
   LEFT JOIN CustomerPartNumber CPN ON C3.numCompanyId = CPN.numCompanyId
   WHERE  Opp.numOppId = v_numOppID
   AND Opp.numDomainId = v_numDomainID;
END; $$;












