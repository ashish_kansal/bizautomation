-- Stored procedure definition script USP_ManageRuleAction for PostgreSQL
CREATE OR REPLACE FUNCTION USP_ManageRuleAction(v_numRuleID NUMERIC(9,0),
    v_numDomainID NUMERIC(9,0),
    v_numRuleConditionID NUMERIC(9,0),
    v_tintActionType SMALLINT, 
    v_numActionTemplateID NUMERIC(9,0),
	v_vcFieldValue VARCHAR(1000),
	v_tintActionFrom SMALLINT,
	v_tintActionTo SMALLINT,
    v_vcActionFrom VARCHAR(1000),
	v_vcActionTo VARCHAR(1000),
	v_tintActionOrder SMALLINT,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   INSERT  INTO ruleAction(numDomainID,numRuleID,numRuleConditionID,tintActionType,numActionTemplateID,vcFieldValue,
					  tintActionFrom,tintActionTo,vcActionFrom,vcActionTo,tintActionOrder)
            VALUES(v_numDomainID,v_numRuleID,v_numRuleConditionID,v_tintActionType,v_numActionTemplateID,v_vcFieldValue,
					  v_tintActionFrom,v_tintActionTo,v_vcActionFrom,v_vcActionTo,v_tintActionOrder);

            
open SWV_RefCur for SELECT  cast(CURRVAL('RuleAction_seq') as VARCHAR(255));
END; $$;  












