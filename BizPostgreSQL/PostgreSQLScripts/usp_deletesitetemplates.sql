-- Stored procedure definition script USP_DeleteSiteTemplates for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_DeleteSiteTemplates(v_numTemplateID NUMERIC(9,0),
	v_numDomainID NUMERIC(9,0))
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   DELETE FROM SiteTemplates
   WHERE numTemplateID = v_numTemplateID AND numDomainID = v_numDomainID;
   RETURN;
END; $$;





