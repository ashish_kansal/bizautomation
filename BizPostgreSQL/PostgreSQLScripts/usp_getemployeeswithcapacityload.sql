-- Stored procedure definition script USP_GetEmployeesWithCapacityLoad for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetEmployeesWithCapacityLoad(v_numDomainID NUMERIC(18,0)
	,v_numWorkOrderID NUMERIC(18,0)
	,v_tintDateRange SMALLINT -- 1:Today, 2:Week, 3:Month
	,v_dtFromDate TIMESTAMP
	,v_ClientTimeZoneOffset INTEGER,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT
   A.numContactId AS "numEmployeeID"
		,CONCAT(A.vcFirstName,' ',A.vcLastname) as "vcEmployeeName"
		,GetCapacityLoad(v_numDomainID,A.numContactId,0,v_numWorkOrderID,0,v_tintDateRange,v_dtFromDate,
   v_ClientTimeZoneOffset) AS "numCapacityLoad"
   FROM
   UserMaster UM
   INNER JOIN
   AdditionalContactsInformation A
   ON
   UM.numUserDetailId = A.numContactId
   WHERE
   UM.numDomainID = v_numDomainID
   AND UM.numDomainID = A.numDomainID
   AND UM.intAssociate = 1;
   RETURN;
END; $$;
/****** Object:  StoredProcedure [dbo].[usp_GetLayoutInfoDdl]    Script Date: 07/26/2008 16:17:40 ******/













