CREATE OR REPLACE FUNCTION usp_GetListID(  
    v_vcData VARCHAR(50) DEFAULT '',
	v_numListID NUMERIC(9,0) DEFAULT 0,
	v_numCreatedBy NUMERIC(9,0) DEFAULT 0,
	v_bintCreatedDate timestamp DEFAULT null,
	v_numDomainId NUMERIC(9,0) DEFAULT 0, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numListIntId  NUMERIC;
BEGIN
   IF v_vcData <> '' then
		
      select   Listdetails.numListItemID INTO v_numListIntId FROM  Listdetails INNER JOIN
      listmaster ON Listdetails.numListID = listmaster.numListID WHERE  upper(Listdetails.vcData) = upper(v_vcData);
      IF v_numListIntId is null then
			
         INSERT INTO Listdetails(numListID,vcData,numCreatedBy,bintCreatedDate,numDomainid)
				VALUES(v_numListID,v_vcData,v_numCreatedBy,v_bintCreatedDate,v_numDomainId);
				
         open SWV_RefCur for
         SELECT CURRVAL('ListDetails_seq');
      ELSE
         open SWV_RefCur for
         SELECT v_numListIntId;
      end if;
   ELSE  -- Means that there is no entry made
      open SWV_RefCur for
      SELECT 0;
   end if;
   RETURN;
END; $$;


