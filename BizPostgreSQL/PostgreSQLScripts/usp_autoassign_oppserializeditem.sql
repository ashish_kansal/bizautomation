-- Stored procedure definition script USP_AutoAssign_OppSerializedItem for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_AutoAssign_OppSerializedItem(v_numDomainID NUMERIC(9,0) DEFAULT 0,  
    v_numOppID NUMERIC(9,0) DEFAULT 0,  
    v_numOppBizDocsId NUMERIC(9,0) DEFAULT 0)
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_maxID  NUMERIC(9,0);
   v_minID  NUMERIC(9,0);
   v_numItemCode  NUMERIC(9,0);
   v_numOppItemtCode  NUMERIC(9,0);
   v_numWarehouseItmsID  NUMERIC(9,0);
   v_bitLotNo  BOOLEAN;
   v_bitSerialized  BOOLEAN;
   v_numUnitHour  DOUBLE PRECISION;
   v_maxWareID  NUMERIC(9,0);
   v_minWareID  NUMERIC(9,0);
   v_numWarehouseItmsDTLID  NUMERIC(9,0);
   v_numQty  DOUBLE PRECISION;
   v_numUseQty  DOUBLE PRECISION;
BEGIN
   IF(SELECT coalesce(bitAutoSerialNoAssign,false) FROM Domain WHERE v_numDomainID = numDomainId) = true then
	
      BEGIN
         CREATE TEMP SEQUENCE tt_tempSerializedItem_seq INCREMENT BY 1 START WITH 1;
         EXCEPTION WHEN OTHERS THEN
            NULL;
      END;
      DROP TABLE IF EXISTS tt_TEMPSERIALIZEDITEM CASCADE;
      CREATE TEMPORARY TABLE tt_TEMPSERIALIZEDITEM
      (
         ID NUMERIC(18,0) DEFAULT NEXTVAL('tt_tempSerializedItem_seq') NOT NULL,
         numItemCode NUMERIC(9,0),
         numOppItemtCode NUMERIC(9,0),
         numWarehouseItmsID NUMERIC(9,0),
         bitLotNo BOOLEAN,
         bitSerialized BOOLEAN,
         numUnitHour DOUBLE PRECISION
      );
      INSERT INTO
      tt_TEMPSERIALIZEDITEM(numItemCode, numOppItemtCode, numWarehouseItmsID, bitLotNo, bitSerialized, numUnitHour)
      SELECT
      I.numItemCode,
			numOppItemtCode,
			numWarehouseItmsID,
			I.bitLotNo,
			I.bitSerialized,
			oppItems.numUnitHour
      FROM
      OpportunityItems oppItems
      JOIN
      Item I
      ON
      oppItems.numItemCode = I.numItemCode
      WHERE
      I.numDomainID = v_numDomainID
      AND oppItems.numOppId = v_numOppID
      AND (coalesce(I.bitLotNo,false) = true OR coalesce(I.bitSerialized,false) = true);
      IF(SELECT COUNT(*) FROM tt_TEMPSERIALIZEDITEM) > 0 then
         select   max(ID), min(ID) INTO v_maxID,v_minID FROM  tt_TEMPSERIALIZEDITEM;
         BEGIN
            CREATE TEMP SEQUENCE tt_tempWareHouseItmsDTL_seq INCREMENT BY 1 START WITH 1;
            EXCEPTION WHEN OTHERS THEN
               NULL;
         END;
         DROP TABLE IF EXISTS tt_TEMPWAREHOUSEITMSDTL CASCADE;
         CREATE TEMPORARY TABLE tt_TEMPWAREHOUSEITMSDTL
         (
            ID NUMERIC(18,0) DEFAULT NEXTVAL('tt_tempWareHouseItmsDTL_seq') NOT NULL,
            vcSerialNo VARCHAR(100),
            numQty DOUBLE PRECISION,
            numWareHouseItmsDTLID NUMERIC(18,0)
         );
         WHILE v_minID <= v_maxID LOOP
            select   numItemCode, numOppItemtCode, numWarehouseItmsID, bitLotNo, bitSerialized, numUnitHour INTO v_numItemCode,v_numOppItemtCode,v_numWarehouseItmsID,v_bitLotNo,v_bitSerialized,
            v_numUnitHour FROM
            tt_TEMPSERIALIZEDITEM WHERE
            ID = v_minID;
            INSERT INTO tt_TEMPWAREHOUSEITMSDTL(vcSerialNo,numWarehouseItmsDTLID,numQty)
            SELECT
            vcSerialNo,numWareHouseItmsDTLID,coalesce(numQty,0) AS TotalQty
            FROM
            WareHouseItmsDTL
            WHERE
            coalesce(numQty,0) > 0
            AND numWareHouseItemID = v_numWarehouseItmsID
            and numWareHouseItmsDTLID NOT IN(SELECT numWarehouseItmsDTLID FROM OppWarehouseSerializedItem WHERE numOppID = v_numOppID AND numWarehouseItmsID = v_numWarehouseItmsID AND numOppBizDocsId = v_numOppBizDocsId)
            ORDER BY
            TotalQty desc;
            IF ((SELECT SUM(numQty) FROM tt_TEMPWAREHOUSEITMSDTL) >= v_numUnitHour) then
               select   max(ID), min(ID) INTO v_maxWareID,v_minWareID FROM  tt_TEMPWAREHOUSEITMSDTL;
               WHILE (v_numUnitHour > 0 AND v_minWareID <= v_maxWareID) LOOP
                  select   numWarehouseItmsDTLID, numQty INTO v_numWarehouseItmsDTLID,v_numQty FROM
                  tt_TEMPWAREHOUSEITMSDTL WHERE
                  ID = v_minWareID;
                  IF v_numUnitHour >= v_numQty then
						
                     v_numUseQty := v_numQty;
                     v_numUnitHour := v_numUnitHour -v_numQty;
                  ELSEIF v_numUnitHour < v_numQty
                  then
						
                     v_numUseQty := v_numUnitHour;
                     v_numUnitHour := 0;
                  end if;
                  INSERT INTO OppWarehouseSerializedItem(numWarehouseItmsDTLID,numOppID,numOppItemID,numWarehouseItmsID,numQty,numOppBizDocsId)
                  SELECT
                  v_numWarehouseItmsDTLID,v_numOppID,v_numOppItemtCode,v_numWarehouseItmsID,v_numUseQty,v_numOppBizDocsId;
                  v_minWareID := v_minWareID+1;
               END LOOP;
            end if;
            TRUNCATE TABLE tt_TEMPWAREHOUSEITMSDTL;
            v_minID := v_minID+1;
         END LOOP;
         DROP TABLE IF EXISTS tt_TEMPWAREHOUSEITMSDTL CASCADE;
      end if;
      DROP TABLE IF EXISTS tt_TEMPSERIALIZEDITEM CASCADE;
   end if;
   RETURN;
END; $$;


