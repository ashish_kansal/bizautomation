-- Stored procedure definition script USP_eChannelHub_GetAsHtml for PostgreSQL
CREATE OR REPLACE FUNCTION USP_eChannelHub_GetAsHtml(v_numDomainID NUMERIC(18,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_vchtml  TEXT DEFAULT '<div class="row">';
BEGIN
   v_vchtml := CONCAT(v_vchtml,'<div class="col-xs-12 col-md-6"><b>Marketplaces</b><hr style="margin-top:6px;margin-bottom:10px;border-color:#ddd;"/>');
   v_vchtml := CONCAT(v_vchtml,'<div class="row">');
   v_vchtml := CONCAT(v_vchtml,COALESCE((SELECT string_agg(CONCAT('<div class="col-sm-12 col-md-4"><div class="checkbox-inline"><input type="checkbox"',(CASE WHEN coalesce(TEMP.numMarketplaceID,0) > 0 THEN ' checked' ELSE '' END),' id="chkMarketplace',eChannelHub.ID,'" onchange="MarketplaceSelectionChanged();" /><label style="font-weight:normal;" class="lblMarketplaceName">',
   vcMarketplace,(CASE
   WHEN numDivisionID IS NOT NULL
   THEN CONCAT(' <i>',(CASE
      WHEN coalesce(tintCRMType,0) = 1 THEN CONCAT('<a href="../prospects/frmProspects.aspx?DivID=',numDivisionID,'" target="_blank">')
      WHEN coalesce(tintCRMType,0) = 2 THEN CONCAT('<a href="../account/frmAccounts.aspx?DivID=',numDivisionID,'" target="_blank">')
      ELSE CONCAT('<a href="../Leads/frmLeads.aspx?DivID=',numDivisionID,'" target="_blank">')
      END),
      vcCompanyName,'</a></i>')
   ELSE ''
   END),
   '</label></div></div>'),'' ORDER BY vcMarketplace)
   
   FROM
   eChannelHub
   LEFT JOIN(SELECT
      numMarketplaceID
	,DivisionMaster.numDivisionID
	,DivisionMaster.tintCRMType
	,CompanyInfo.vcCompanyName
      FROM
      DomainMarketplace
      LEFT JOIN
      DivisionMaster
      ON
      DomainMarketplace.numDivisionID = DivisionMaster.numDivisionID
      LEFT JOIN
      CompanyInfo
      ON
      DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
      WHERE
      DomainMarketplace.numDomainID = v_numDomainID) TEMP
   ON
   eChannelHub.ID = TEMP.numMarketplaceID
   WHERE
   vcType = 'Marketplaces'),''));
   v_vchtml := CONCAT(v_vchtml,'</div>');
   v_vchtml := CONCAT(v_vchtml,'</div>');
   v_vchtml := CONCAT(v_vchtml,'<div class="col-xs-12 col-md-3"><b>Comparison Shopping</b><hr style="margin-top:6px;margin-bottom:10px;border-color:#ddd;"/>');
   v_vchtml := CONCAT(v_vchtml,'<div class="row">');
   v_vchtml := CONCAT(v_vchtml,COALESCE((SELECT string_agg(CONCAT('<div class="col-sm-12"><div class="checkbox-inline"><input type="checkbox"',(CASE WHEN coalesce(TEMP.numMarketplaceID,0) > 0 THEN ' checked="checked"' ELSE '' END),' id="chkMarketplace',eChannelHub.ID,'" onchange="MarketplaceSelectionChanged();" /><label style="font-weight:normal;" class="lblMarketplaceName">',
   vcMarketplace,(CASE
   WHEN numDivisionID IS NOT NULL
   THEN CONCAT(' <i>',(CASE
      WHEN coalesce(tintCRMType,0) = 1 THEN CONCAT('<a href="../prospects/frmProspects.aspx?DivID=',numDivisionID,'" target="_blank">')
      WHEN coalesce(tintCRMType,0) = 2 THEN CONCAT('<a href="../account/frmAccounts.aspx?DivID=',numDivisionID,'" target="_blank">')
      ELSE CONCAT('<a href="../Leads/frmLeads.aspx?DivID=',numDivisionID,'" target="_blank">')
      END),
      vcCompanyName,'</a></i>')
   ELSE ''
   END),
   '</label></div></div>'),'' ORDER BY vcMarketplace)
   
   FROM
   eChannelHub
   LEFT JOIN(SELECT
      numMarketplaceID
													,DivisionMaster.numDivisionID
													,DivisionMaster.tintCRMType
													,CompanyInfo.vcCompanyName
      FROM
      DomainMarketplace
      LEFT JOIN
      DivisionMaster
      ON
      DomainMarketplace.numDivisionID = DivisionMaster.numDivisionID
      LEFT JOIN
      CompanyInfo
      ON
      DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
      WHERE
      DomainMarketplace.numDomainID = v_numDomainID) TEMP
   ON
   eChannelHub.ID = TEMP.numMarketplaceID
   WHERE
   vcType = 'Comparison Shopping'),''));
   v_vchtml := CONCAT(v_vchtml,'</div>');
   v_vchtml := CONCAT(v_vchtml,'</div>');
   v_vchtml := CONCAT(v_vchtml,'<div class="col-xs-12 col-md-3"><b>3rd Party Shopping Carts</b><hr style="margin-top:6px;margin-bottom:10px;border-color:#ddd;"/>');
   v_vchtml := CONCAT(v_vchtml,'<div class="row">');
   v_vchtml := CONCAT(v_vchtml,COALESCE((SELECT string_agg(CONCAT('<div class="col-sm-12"><div class="checkbox-inline"><input type="checkbox"',(CASE WHEN coalesce(TEMP.numMarketplaceID,0) > 0 THEN ' checked="checked"' ELSE '' END),' id="chkMarketplace',eChannelHub.ID,'" onchange="MarketplaceSelectionChanged();" /><label style="font-weight:normal;" class="lblMarketplaceName">',
   vcMarketplace,(CASE
   WHEN numDivisionID IS NOT NULL
   THEN CONCAT(' <i>',(CASE
      WHEN coalesce(tintCRMType,0) = 1 THEN CONCAT('<a href="../prospects/frmProspects.aspx?DivID=',numDivisionID,'" target="_blank">')
      WHEN coalesce(tintCRMType,0) = 2 THEN CONCAT('<a href="../account/frmAccounts.aspx?DivID=',numDivisionID,'" target="_blank">')
      ELSE CONCAT('<a href="../Leads/frmLeads.aspx?DivID=',numDivisionID,'" target="_blank">')
      END),
      vcCompanyName,'</a></i>')
   ELSE ''
   END),
   '</label></div></div>'),'' ORDER BY vcMarketplace)
   FROM
   eChannelHub
   LEFT JOIN(SELECT
      numMarketplaceID
													,DivisionMaster.numDivisionID
													,DivisionMaster.tintCRMType
													,CompanyInfo.vcCompanyName
      FROM
      DomainMarketplace
      LEFT JOIN
      DivisionMaster
      ON
      DomainMarketplace.numDivisionID = DivisionMaster.numDivisionID
      LEFT JOIN
      CompanyInfo
      ON
      DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
      WHERE
      DomainMarketplace.numDomainID = v_numDomainID) TEMP
   ON
   eChannelHub.ID = TEMP.numMarketplaceID
   WHERE
   vcType = '3rd Party Shopping Carts'),''));
   v_vchtml := CONCAT(v_vchtml,'</div>');
   v_vchtml := CONCAT(v_vchtml,'</div>');
   v_vchtml := CONCAT(v_vchtml,'</div>');

   open SWV_RefCur for SELECT v_vchtml;
END; $$;












