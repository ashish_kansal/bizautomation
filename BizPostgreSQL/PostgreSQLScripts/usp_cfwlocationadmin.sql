-- Stored procedure definition script USP_CFWLocationAdmin for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021


CREATE OR REPLACE FUNCTION USP_CFWLocationAdmin(v_numDomainId NUMERIC(18,0), INOUT SWV_RefCur refcursor)
LANGUAGE plpgsql
   AS $$
BEGIN
   IF(SELECT coalesce(bitEDI,false) FROM Domain WHERE numDomainId = v_numDomainId) = false then
      open SWV_RefCur for
      SELECT Loc_id,loc_name
      FROM CFW_Loc_Master
      WHERE Loc_id <> 19 AND Loc_id <> 20 AND Loc_id <> 21 AND Loc_id NOT IN(12,13,14,15,16,17,18,19,20,21);
   ELSE
      open SWV_RefCur for
      SELECT Loc_id,loc_name
      FROM CFW_Loc_Master
      WHERE Loc_id <> 21 AND Loc_id NOT IN(12,13,14,15,16,17,18,19,20,21);
   end if;
   RETURN;
END; $$;




