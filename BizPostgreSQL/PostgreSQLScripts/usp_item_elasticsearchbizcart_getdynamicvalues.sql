-- Stored procedure definition script USP_Item_ElasticSearchBizCart_GetDynamicValues for PostgreSQL
CREATE OR REPLACE FUNCTION USP_Item_ElasticSearchBizCart_GetDynamicValues(v_numSiteId NUMERIC(18,0)
	,v_numDivisionID NUMERIC(18,0) DEFAULT 0
	,v_IsAvaiablityTokenCheck BOOLEAN DEFAULT false
	,v_numWareHouseID NUMERIC(9,0) DEFAULT 0
	,v_UTV_Item_UOMConversion TEXT DEFAULT '',INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numDomainID  NUMERIC(18,0) DEFAULT 0;
   v_numDefaultRelationship  NUMERIC(18,0) DEFAULT 0;
   v_numDefaultProfile  NUMERIC(18,0) DEFAULT 0;
   v_bitShowInStock  BOOLEAN DEFAULT 0;
   v_isPromotionExist  BOOLEAN DEFAULT 0; 
   v_bitAutoSelectWarehouse  BOOLEAN;
   v_vcWarehouseIDs  VARCHAR(1000);
   v_tintPreLoginPriceLevel SMALLINT;
	v_tintPriceLevel SMALLINT;
BEGIN
   select   numDomainID INTO v_numDomainID FROM
   Sites WHERE
   numSiteID = v_numSiteId;

   DROP TABLE IF EXISTS tt_UOMConversion CASCADE;
   CREATE TEMPORARY TABLE tt_UOMConversion
   (
      numItemCode NUMERIC(18,0)
	  ,UOM DOUBLE PRECISION
      ,UOMPurchase DOUBLE PRECISION
   );

   IF COALESCE(v_UTV_Item_UOMConversion,'') <> '' THEN
		INSERT INTO tt_UOMConversion
		(
			numItemCode
			,UOM
			,UOMPurchase
		)
		SELECT
			numItemCode
			,UOM
			,UOMPurchase
		FROM
		XMLTABLE
		(
			'NewDataSet/dtItem_UOMConversion'
			PASSING 
				CAST(v_UTV_Item_UOMConversion AS XML)
			COLUMNS
				id FOR ORDINALITY,
				numItemCode NUMERIC(9,0) PATH 'numItemCode'
				,UOM DOUBLE PRECISION PATH 'UOM'
				,UOMPurchase DOUBLE PRECISION PATH 'UOMPurchase'
		) AS X;
   END IF;


   IF coalesce(v_numDivisionID,0) > 0 then
		
      select   coalesce(numCompanyType,0), coalesce(vcProfile,0),COALESCE(tintPriceLevel,0)  INTO v_numDefaultRelationship,v_numDefaultProfile,v_tintPriceLevel FROM
      DivisionMaster
      INNER JOIN
      CompanyInfo
      ON
      DivisionMaster.numCompanyID = CompanyInfo.numCompanyId WHERE
      numDivisionID = v_numDivisionID
      AND DivisionMaster.numDomainID = v_numDomainID;
   end if;

   v_isPromotionExist := CASE WHEN EXISTS(select 1 from PromotionOffer where numDomainId = v_numDomainID) THEN 1 ELSE 0 END;


	--KEEP THIS BELOW ABOVE IF CONDITION
   select 
	(CASE WHEN coalesce(v_numDivisionID,0) > 0 THEN v_numDefaultRelationship ELSE numRelationshipId END)
	, (CASE WHEN coalesce(v_numDivisionID,0) > 0 THEN v_numDefaultProfile ELSE numProfileId END)
	, bitShowInStock
	, (CASE WHEN COALESCE(bitShowPriceUsingPriceLevel,false)=true THEN (CASE WHEN v_numDivisionID > 0 THEN (CASE WHEN COALESCE(bitShowPriceUsingPriceLevel,false)=true THEN COALESCE(v_tintPriceLevel,0) ELSE 0 END) ELSE COALESCE(tintPreLoginProceLevel,0) END) ELSE 0 END)
	, coalesce(bitAutoSelectWarehouse,false) INTO v_numDefaultRelationship,v_numDefaultProfile,v_bitShowInStock,v_tintPreLoginPriceLevel,v_bitAutoSelectWarehouse FROM
   eCommerceDTL WHERE
   numSiteId = v_numSiteId;

   IF (v_numWareHouseID = 0 OR v_bitAutoSelectWarehouse = true) then
		
      IF v_bitAutoSelectWarehouse = true then
			
         v_vcWarehouseIDs := coalesce(OVERLAY((SELECT DISTINCT ',' || CAST(numWareHouseID AS VARCHAR(10)) FROM Warehouses WHERE Warehouses.numDomainID = v_numDomainID) placing '' from 1 for 1),'');
      ELSE
         select   coalesce(numDefaultWareHouseID,0) INTO v_numWareHouseID FROM
         eCommerceDTL WHERE
         numDomainID = v_numDomainID;
         v_vcWarehouseIDs := SUBSTR(CAST(v_numWareHouseID AS VARCHAR(1000)),1,1000);
      end if;
   ELSE
      v_vcWarehouseIDs := SUBSTR(CAST(v_numWareHouseID AS VARCHAR(1000)),1,1000);
   end if;


   DROP TABLE IF EXISTS tt_TEMPTBL CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPTBL AS
      SELECT
      T.numItemCode
		,vcItemName
		,numTotalPromotions
		,vcPromoDesc
		,bitRequireCouponCode
		,numItemClassification
		,UOM
		,UOMPurchase
		,T.numVendorID
		,charItemType
		,(CASE WHEN charItemType <> 'S' AND charItemType <> 'N' AND COALESCE(bitKitParent,false)=false
					THEN (CASE 
							WHEN bitAllowBackOrder = true THEN 1
							WHEN (COALESCE(numOnHand,0)<=0) THEN 0
							ELSE 1
						END)
					ELSE 1
			END) AS bitInStock
		,(CASE WHEN charItemType <> 'S' AND charItemType <> 'N'
					THEN (CASE WHEN v_bitShowInStock = true
								THEN (CASE 
										WHEN bitAllowBackOrder = true AND T.numDomainID NOT IN (172) AND COALESCE(numOnHand,0)<=0 AND COALESCE(T.numVendorID,0) > 0 AND GetVendorPreferredMethodLeadTime(T.numVendorID,0) > 0 THEN CONCAT('<font class="vendorLeadTime" style="color:green">Usually ships in ',GetVendorPreferredMethodLeadTime(T.numVendorID,0),' days</font>')
										WHEN bitAllowBackOrder = true AND COALESCE(numOnHand,0)<=0 THEN (CASE WHEN T.numDomainID  IN (172) THEN '<font style="color:green">AVAILABLE</font>' ELSE '<font style="color:green">In Stock</font>' END)
										WHEN (COALESCE(numOnHand,0)<=0) THEN (CASE WHEN T.numDomainID  IN (172) THEN '<font style="color:red">ON HOLD</font>' ELSE '<font style="color:red">Out Of Stock</font>' END)
										ELSE (CASE WHEN T.numDomainID IN (172) THEN '<font style="color:green">AVAILABLE</font>' ELSE '<font style="color:green">In Stock</font>' END)  
									END)
								ELSE ''
							END)
					ELSE ''
				END) AS InStock ,
			CAST(0 AS DOUBLE PRECISION) AS monFirstPriceLevelPrice,
			CAST(0 AS DOUBLE PRECISION) AS fltFirstPriceLevelDiscount,
			(CASE 
				WHEN v_tintPreLoginPriceLevel > 0 
				THEN COALESCE((CASE 
						WHEN PricingOption.tintRuleType = 1 AND PricingOption.tintDiscountType = 1
						THEN COALESCE((CASE WHEN T.charItemType = 'P' THEN ( UOM * COALESCE(W1.monWListPrice,0)) ELSE (UOM * monListPrice) END),0) - (COALESCE((CASE WHEN T.charItemType = 'P' THEN ( UOM * COALESCE(W1.monWListPrice,0)) ELSE (UOM * monListPrice) END),0) * (PricingOption.decDiscount / 100 ) )
						WHEN PricingOption.tintRuleType = 1 AND PricingOption.tintDiscountType = 2
						THEN COALESCE((CASE WHEN T.charItemType = 'P' THEN ( UOM * COALESCE(W1.monWListPrice,0)) ELSE (UOM * monListPrice) END),0) - PricingOption.decDiscount
						WHEN PricingOption.tintRuleType = 2 AND PricingOption.tintDiscountType = 1
						THEN COALESCE(Vendor.monCost,0) + (COALESCE(Vendor.monCost,0) * (PricingOption.decDiscount / 100 ) )
						WHEN PricingOption.tintRuleType = 2 AND PricingOption.tintDiscountType = 2
						THEN COALESCE(Vendor.monCost,0) + PricingOption.decDiscount
						WHEN PricingOption.tintRuleType = 3
						THEN PricingOption.decDiscount
					END),COALESCE(CASE WHEN T.charItemType = 'P' THEN ( UOM * COALESCE(W1.monWListPrice,0)) ELSE (UOM * monListPrice) END, 0))
				ELSE COALESCE(CASE WHEN T.charItemType = 'P' THEN ( UOM * COALESCE(W1.monWListPrice,0)) ELSE (UOM * monListPrice) END, 0)
				END) AS monListPrice,
			W1.monWListPrice,
			coalesce(W1.numWareHouseItemID,0) AS numWareHouseItemID
			
      FROM(SELECT
      I.numItemCode
			,I.vcItemName
			,I.charItemType
			,I.bitKitParent
			,I.bitAllowBackOrder
			,I.numDomainID
			,I.numVendorID
			,I.numItemClassification
			,CAST(0 AS INTEGER) AS numTotalPromotions
			,CAST('' AS VARCHAR(100)) AS vcPromoDesc
			,CAST(0 AS BOOLEAN) AS bitRequireCouponCode
			,SUM(coalesce(WHI.numOnHand,0)) AS numOnHand
			,TEMPItem.UOM
			,TEMPItem.UOMPurchase
			,I.monListPrice
      FROM Item I 
		--INNER JOIN
		--(
		--	SELECT [OutParam] FROM dbo.[SplitString](@vcItemCodes,',')
		--) TEMPItem
		--ON
		--	I.numItemCode = TEMPItem.OutParam
      INNER JOIN tt_UOMConversion AS TEMPItem ON (I.numItemCode = TEMPItem.numItemCode)
      LEFT JOIN WareHouseItems WHI ON (WHI.numItemID = I.numItemCode)
		--OUTER APPLY (SELECT * FROM dbo.fn_GetItemPromotions(I.numDomainID,@numDivisionID,numItemCode,numItemClassification,@numDefaultRelationship,@numDefaultProfile,1)) TablePromotion
      WHERE WHI.numDomainID = v_numDomainID
      GROUP BY
      I.numItemCode,I.vcItemName,I.charItemType,I.bitKitParent,I.bitAllowBackOrder,
      I.numDomainID,I.numVendorID,I.numItemClassification,
			--,ISNULL(TablePromotion.numTotalPromotions,0)
			--,ISNULL(TablePromotion.vcPromoDesc,'''') 
			--,ISNULL(TablePromotion.bitRequireCouponCode,0) 
			TEMPItem.UOM,
      TEMPItem.UOMPurchase,I.monListPrice) T
	  LEFT JOIN Vendor ON Vendor.numVendorID =T.numVendorID AND Vendor.numItemCode=T.numItemCode
	  LEFT JOIN LATERAL (SELECT * FROM PricingTable WHERE numItemCode=T.numItemCode AND COALESCE(numPriceRuleID,0) = 0 AND COALESCE(numCurrencyID,0) = 0 AND v_tintPreLoginPriceLevel > 0 ORDER BY numPricingID OFFSET (CASE WHEN v_tintPreLoginPriceLevel > 0 THEN (v_tintPreLoginPriceLevel-1) ELSE 0 END) ROWS FETCH NEXT 1 ROWS ONLY) As PricingOption ON TRUE
      LEFT JOIN LATERAL(SELECT  numWareHouseItemID,monWListPrice FROM WareHouseItems W
      WHERE  W.numItemID = T.numItemCode
      AND W.numDomainID = v_numDomainID
      AND numWareHouseID IN(SELECT Id FROM SplitIds(v_vcWarehouseIDs,',')) LIMIT 1) AS W1 on TRUE;
		


   IF(v_isPromotionExist = true) then
		
		UPDATE 
			tt_TEMPTBL t1
		SET 
			numTotalPromotions = coalesce((SELECT numTotalPromotions FROM fn_GetItemPromotions(v_numDomainID,v_numDivisionID,t1.numItemCode,t1.numItemClassification,v_numDefaultRelationship,v_numDefaultProfile,1::SMALLINT)),0)
			,vcPromoDesc = coalesce((SELECT vcPromoDesc FROM fn_GetItemPromotions(v_numDomainID,v_numDivisionID,t1.numItemCode,t1.numItemClassification,v_numDefaultRelationship,v_numDefaultProfile,1::SMALLINT)),'')
			,bitRequireCouponCode = coalesce((SELECT bitRequireCouponCode FROM fn_GetItemPromotions(v_numDomainID,v_numDivisionID,t1.numItemCode,t1.numItemClassification,v_numDefaultRelationship,v_numDefaultProfile,1::SMALLINT)),false)
		;
   end if;

   IF(v_numDomainID = 172) then
		
			
			--INNER JOIN Item I ON (I.numItemCode = t1.numItemCode)
      UPDATE tt_TEMPTBL t1
      SET monFirstPriceLevelPrice = coalesce(CASE
      WHEN tintRuleType = 1 AND tintDiscountType = 1
      THEN coalesce((CASE WHEN t1.charItemType = 'P' THEN(UOM*coalesce(t1.monWListPrice,0)) ELSE(UOM*monListPrice) END),0) -(coalesce((CASE WHEN t1.charItemType = 'P' THEN(UOM*coalesce(t1.monWListPrice,0)) ELSE(UOM*monListPrice) END),0)*(decDiscount/100))
      WHEN tintRuleType = 1 AND tintDiscountType = 2
      THEN coalesce((CASE WHEN t1.charItemType = 'P' THEN(UOM*coalesce(t1.monWListPrice,0)) ELSE(UOM*monListPrice) END),0) -decDiscount
      WHEN tintRuleType = 2 AND tintDiscountType = 1
      THEN coalesce(Vendor.monCost,0)+(coalesce(Vendor.monCost,0)*(decDiscount/100))
      WHEN tintRuleType = 2 AND tintDiscountType = 2
      THEN coalesce(Vendor.monCost,0)+decDiscount
      WHEN tintRuleType = 3
      THEN decDiscount
      END,0),fltFirstPriceLevelDiscount = coalesce(CASE
      WHEN tintRuleType = 1 AND tintDiscountType = 1
      THEN decDiscount
      WHEN tintRuleType = 1 AND tintDiscountType = 2
      THEN(decDiscount*100)/(coalesce((CASE WHEN t1.charItemType = 'P' THEN(UOM*coalesce(t1.monWListPrice,0)) ELSE(UOM*monListPrice) END),0) -decDiscount)
      WHEN tintRuleType = 2 AND tintDiscountType = 1
      THEN decDiscount
      WHEN tintRuleType = 2 AND tintDiscountType = 2
      THEN(decDiscount*100)/(coalesce(Vendor.monCost,0)+decDiscount)
      WHEN tintRuleType = 3
      THEN 0
      END,0)
      FROM(SELECT * FROM PricingTable WHERE numItemCode = t1.numItemCode AND coalesce(numCurrencyID,0) = 0 LIMIT 1) AS PT WHERE(Vendor.numVendorID = t1.numVendorID AND Vendor.numItemCode = t1.numItemCode);
   end if;



   open SWV_RefCur for SELECT * From tt_TEMPTBL;

		
   DROP TABLE IF EXISTS tt_UOMConversion CASCADE;
   RETURN;
END; $$;















