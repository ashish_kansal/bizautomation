-- Stored procedure definition script USP_GetRecurringTemplateID for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetRecurringTemplateID(v_numOppId NUMERIC(9,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN    
   open SWV_RefCur for SELECT ROW_NUMBER() OVER(ORDER BY OPR.numDomainID) AS  ID,
		   numOppRecID,
		   OPR.numOppId,
		   coalesce(OPR.numOppBizDocID,0) AS numOppBizDocID,
		   numRecurringId,
		   tintRecurringType,
		   FormatedDateFromDate(dtRecurringDate,OPR.numDomainID) AS dtRecurringDate,
		   bitRecurringZeroAmt,
		   numNoTransactions,
		   OPR.bitBillingTerms,
		   numBillingDays,
		   fltBreakupPercentage,
		   OPR.numDomainID,
		   coalesce(OBD.vcBizDocID,'') AS vcBizDocID,
		   --'Net-' + ISNULL(dbo.GetListIemName(OPR.numBillingDays),'0') AS Billingdays
		   coalesce((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = OM.intBillingDays),'0') AS Billingdays
   FROM OpportunityRecurring OPR INNER JOIN OpportunityMaster OM ON
   OPR.numOppId = OM.numOppId
   LEFT JOIN OpportunityBizDocs OBD ON OBD.numOppBizDocsId = OPR.numOppBizDocID
   WHERE OPR.numOppId = v_numOppId
   and tintRecurringType <> 4; --Do not include Deferred BizDoc
    
END; $$;
