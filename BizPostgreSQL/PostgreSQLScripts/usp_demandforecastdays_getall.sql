-- Stored procedure definition script USP_DemandForecastDays_GetAll for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_DemandForecastDays_GetAll(v_numDomainID NUMERIC(18,0), INOUT SWV_RefCur refcursor)
LANGUAGE plpgsql
   AS $$
BEGIN
   IF EXISTS(SELECT numDFDaysID FROM DemandForecastDays WHERE coalesce(numDomainID,0) = v_numDomainID) then
      open SWV_RefCur for
      SELECT * FROM DemandForecastDays WHERE coalesce(numDomainID,0) = v_numDomainID;
   ELSE
      open SWV_RefCur for
      SELECT * FROM DemandForecastDays WHERE coalesce(numDomainID,0) = 0;
   end if;
   RETURN;
END; $$;


