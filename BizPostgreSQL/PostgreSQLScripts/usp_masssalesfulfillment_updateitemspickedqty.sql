DROP FUNCTION IF EXISTS USP_MassSalesFulfillment_UpdateItemsPickedQty;

CREATE OR REPLACE FUNCTION USP_MassSalesFulfillment_UpdateItemsPickedQty(v_numDomainID NUMERIC(18,0)
	,v_numUserCntID NUMERIC(18,0)
	,v_vcSelectedRecords TEXT
	,v_ClientTimeZoneOffset INTEGER
	,INOUT SWV_RefCur refcursor 
)
LANGUAGE plpgsql
   AS $$
   DECLARE
	v_numStatus NUMERIC(18,0) DEFAULT 0;
	v_hDocItem INTEGER;
	v_i INTEGER DEFAULT 1;
	v_iCount INTEGER;
	v_j INTEGER DEFAULT 1;
	v_jCount INTEGER;
	v_x INTEGER DEFAULT 1;
	v_xCount INTEGER DEFAULT 0;
	v_y INTEGER DEFAULT 1;
	v_yCount INTEGER DEFAULT 0;
	v_numPickedQty DOUBLE PRECISION;
	v_numOppID NUMERIC(18,0);
	v_numOppItemID NUMERIC(18,0);
	v_numOppChildItemID NUMERIC(18,0);
	v_numOppKitChildItemID NUMERIC(18,0);
	v_bitSerial BOOLEAN;
	v_bitLot BOOLEAN;
	v_numWarehouseItemID NUMERIC(18,0);
	v_numQtyLeftToPick DOUBLE PRECISION;
	v_vcWarehouseItemIDs VARCHAR(4000);
	v_vcOppItems TEXT;
	v_numTempWarehouseItemID NUMERIC(18,0);
	v_numWarehousePickedQty DOUBLE PRECISION;
	v_numFromOnHand DOUBLE PRECISION;
	v_numFromOnOrder DOUBLE PRECISION;
	v_numFromAllocation DOUBLE PRECISION;
	v_numFromBackOrder DOUBLE PRECISION;
	v_numToOnHand DOUBLE PRECISION;
	v_numToOnOrder DOUBLE PRECISION;
	v_numToAllocation DOUBLE PRECISION;
	v_numToBackOrder DOUBLE PRECISION;
	v_vcDescription VARCHAR(300);
	v_numSerialWarehoueItemID NUMERIC(18,0);
	v_numMaxID NUMERIC(18,0);
	v_vcSerialLot TEXT;
	v_vcSerialLotNo VARCHAR(200);	
	v_numSerialLotQty DOUBLE PRECISION;
	v_numSerialLotQtyToPick DOUBLE PRECISION;
	v_numWareHouseItmsDTLID NUMERIC(18,0);

	v_numTempOppID NUMERIC(18,0);
	v_vcItems TEXT;
	v_intUsedShippingCompany NUMERIC(18,0);
	v_dtFromDate TIMESTAMP;
	v_numBizDocTempID NUMERIC(18,0);

	my_ex_state TEXT;
	my_ex_message TEXT;
	my_ex_detail TEXT;
	my_ex_hint TEXT;
	my_ex_ctx TEXT;
	v_hDocSerialLotItem  INTEGER;
	v_k  INTEGER DEFAULT 1;
	v_kCount  INTEGER;
	v_USP_CreateBizDocs_numOppBizDocsId NUMERIC(9,0);
	v_USP_CreateBizDocs_monCreditAmount DECIMAL(20,5);
	v_USP_CreateBizDocs_monDealAmount DECIMAL(20,5);
	v_pickLists TEXT;
	v_bitAutoGeneratePackingSlip BOOLEAN;
	v_numwarehouseitmsdtlids numeric[];
	v_numTempWareHouseItmsDTLID numeric(18,0);
BEGIN
	IF EXISTS(SELECT ID FROM MassSalesFulfillmentConfiguration WHERE numDomainID = v_numDomainID) THEN
		SELECT 
			COALESCE(numOrderStatusPicked,0) 
			,COALESCE(bitAutoGeneratePackingSlip,false)
		INTO 
			v_numStatus 
			,v_bitAutoGeneratePackingSlip
		FROM 
			MassSalesFulfillmentConfiguration 
		WHERE 
			numDomainID = v_numDomainID;
	END IF;
   
	DROP TABLE IF EXISTS tt_TEMPORDERS CASCADE;
	CREATE TEMPORARY TABLE tt_TEMPORDERS
	(
		ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1),
		numOppID NUMERIC(18,0),
		numOppItemID NUMERIC(18,0),
		numOppChildItemID NUMERIC(18,0),
		numOppKitChildItemID NUMERIC(18,0),
		numQtyPicked DOUBLE PRECISION
	);
   
	DROP TABLE IF EXISTS tt_TEMP CASCADE;
	CREATE TEMPORARY TABLE tt_TEMP
	(
		ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1),
		numPickedQTY DOUBLE PRECISION,
		vcWarehouseItemIDs XML,
		vcOppItems TEXT,
		numOppChildItemID NUMERIC(18,0),
		numOppKitChildItemID NUMERIC(18,0)
	);

	INSERT INTO tt_TEMP
	(
		numPickedQty,
		vcWarehouseItemIDs,
		vcOppItems,
		numOppChildItemID,
		numOppKitChildItemID
	)
	SELECT
		numPickedQty,
		WarehouseItemIDs,
		vcOppItems,
		numOppChildItemID,
		numOppKitChildItemID
	FROM
		XMLTABLE
		(
			'NewDataSet/Table1'
			PASSING 
				CAST(v_vcSelectedRecords AS XML)
			COLUMNS
				numPickedQty DOUBLE PRECISION PATH 'numPickedQty',
				WarehouseItemIDs XML PATH 'WarehouseItemIDs',
				vcOppItems TEXT PATH 'vcOppItems',
				numOppChildItemID NUMERIC(18,0) PATH 'numOppChildItemID',
				numOppKitChildItemID NUMERIC(18,0) PATH 'numOppKitChildItemID'
		) AS X;

	DROP TABLE IF EXISTS tt_TEMPITEMS CASCADE;
	CREATE TEMPORARY TABLE tt_TEMPITEMS
	(
		ID INTEGER,
		numOppID NUMERIC(18,0),
		numOppItemID NUMERIC(18,0),
		numOppChildItemID NUMERIC(18,0),
		numOppKitChildItemID NUMERIC(18,0),
		bitSerial BOOLEAN,
		bitLot BOOLEAN,
		numWarehouseItemID NUMERIC(18,0),
		numQtyLeftToPick DOUBLE PRECISION
	);

   DROP TABLE IF EXISTS tt_TEMPWAREHOUSELOCATIONS CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPWAREHOUSELOCATIONS
   (
      ID INTEGER,
      numWarehouseItemID NUMERIC(18,0),
      numPickedQty DOUBLE PRECISION,
      "vcSerialLot#" TEXT
   );

   DROP TABLE IF EXISTS tt_TEMPSERIALLOT CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPSERIALLOT
   (
      ID INTEGER,
      numWarehouseItemID NUMERIC(18,0),
      vcSerialNo VARCHAR(300),
      numQty DOUBLE PRECISION
   );

   DROP TABLE IF EXISTS tt_TEMPWAREHOUSEDTL CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPWAREHOUSEDTL
   (
      ID INTEGER,
      numWareHouseItmsDTLID NUMERIC(18,0)
   );

	DROP TABLE IF EXISTS tt_TEMPOIPICKEDLOCATION CASCADE;
	CREATE TEMPORARY TABLE tt_TEMPOIPICKEDLOCATION
	(
		numdomainid numeric,
		numoppid numeric,
		numoppitemid numeric,
		numoppbizdocid numeric,
		numfromwarehouseitemid numeric,
		numtowarehouseitemid numeric,
		numunitpicked numeric(20,5),
		numwarehouseitmsdtlids numeric[]
	);


   SELECT COUNT(*) INTO v_iCount FROM tt_TEMP;

   BEGIN
      -- BEGIN TRANSACTION
		WHILE v_i <= v_iCount LOOP
			SELECT 
				COALESCE(numPickedQty,0)
				,REPLACE(REPLACE(CAST(vcWarehouseItemIDs AS TEXT),'<WarehouseItemIDs>',''),'</WarehouseItemIDs>','')
				,COALESCE(vcOppItems,'')
				,numOppChildItemID
				,numOppKitChildItemID 
			INTO
				v_numPickedQty
				,v_vcWarehouseItemIDs
				,v_vcOppItems
				,v_numOppChildItemID
				,v_numOppKitChildItemID 
			FROM
				tt_TEMP 
			WHERE
				ID = v_i;

			DELETE FROM tt_TEMPITEMS;
			DELETE FROM tt_TEMPWAREHOUSELOCATIONS;
			DELETE FROM tt_TEMPSERIALLOT;
         
			INSERT INTO tt_TEMPITEMS
			(
				ID
				,numOppID
				,numOppItemID
				,numOppChildItemID
				,numOppKitChildItemID
				,bitSerial
				,bitLot
				,numWarehouseItemID
				,numQtyLeftToPick
			)
			SELECT
				ROW_NUMBER() OVER(ORDER BY numOppItemID ASC)
				,OpportunityItems.numOppId
				,TEMP.numOppItemID
				,v_numOppChildItemID
				,v_numOppKitChildItemID
				,coalesce(Item.bitSerialized,false)
				,coalesce(Item.bitLotNo,false)
				,OpportunityItems.numWarehouseItmsID
				,coalesce(OpportunityItems.numUnitHour,0) -coalesce(OpportunityItems.numQtyPicked,0)
			FROM
			(
				SELECT
					CAST(SUBSTR(OutParam,0,POSITION('(' IN OutParam)) AS NUMERIC) AS numOppItemID
				FROM
					SplitString(v_vcOppItems,',')
			) TEMP
			INNER JOIN
				OpportunityItems
			ON
				Temp.numOppItemID = OpportunityItems.numoppitemtCode
				AND v_numOppChildItemID = 0
				AND v_numOppKitChildItemID = 0
			INNER JOIN
				Item
			ON
				OpportunityItems.numItemCode = Item.numItemCode;

			INSERT INTO tt_TEMPITEMS
			(
				ID
				,numOppID
				,numOppItemID
				,numOppChildItemID
				,numOppKitChildItemID
				,bitSerial
				,bitLot
				,numWarehouseItemID
				,numQtyLeftToPick
			)
			SELECT
				ROW_NUMBER() OVER(ORDER BY OpportunityKitItems.numOppItemID ASC)
				,OpportunityKitItems.numOppId
				,TEMP.numOppItemID
				,OpportunityKitItems.numOppChildItemID
				,0
				,coalesce(Item.bitSerialized,false)
				,coalesce(Item.bitLotNo,false)
				,OpportunityKitItems.numWareHouseItemId
				,v_numPickedQty
			FROM
			(
				SELECT
					CAST(SUBSTR(OutParam,0,POSITION('(' IN OutParam)) AS NUMERIC) AS numOppItemID
				FROM
					SplitString(v_vcOppItems,',')
			) TEMP
			INNER JOIN
				OpportunityItems
			ON
				Temp.numOppItemID = OpportunityItems.numoppitemtCode
			INNER JOIN
				OpportunityKitItems
			ON
				Temp.numOppItemID = OpportunityKitItems.numOppItemID
				AND OpportunityKitItems.numOppChildItemID = v_numOppChildItemID
				AND v_numOppKitChildItemID = 0
			INNER JOIN
				Item
			ON
				OpportunityKitItems.numChildItemID = Item.numItemCode;

			INSERT INTO tt_TEMPITEMS
			(
				ID
				,numOppID
				,numOppItemID
				,numOppChildItemID
				,numOppKitChildItemID
				,bitSerial
				,bitLot
				,numWarehouseItemID
				,numQtyLeftToPick
			)
			SELECT
				ROW_NUMBER() OVER(ORDER BY OpportunityKitChildItems.numOppItemID ASC)
				,OpportunityKitChildItems.numOppId
				,TEMP.numOppItemID
				,OpportunityKitChildItems.numOppChildItemID
				,OpportunityKitChildItems.numOppKitChildItemID
				,coalesce(Item.bitSerialized,false)
				,coalesce(Item.bitLotNo,false)
				,OpportunityKitChildItems.numWareHouseItemId
				,v_numPickedQty
			FROM
			(
				SELECT
					CAST(SUBSTR(OutParam,0,POSITION('(' IN OutParam)) AS NUMERIC) AS numOppItemID
				FROM
					SplitString(v_vcOppItems,',')
			) TEMP
			INNER JOIN
				OpportunityItems
			ON
				Temp.numOppItemID = OpportunityItems.numoppitemtCode
			INNER JOIN
				OpportunityKitChildItems
			ON
				Temp.numOppItemID = OpportunityKitChildItems.numOppItemID
				AND OpportunityKitChildItems.numOppChildItemID = v_numOppChildItemID
				AND OpportunityKitChildItems.numOppKitChildItemID = v_numOppKitChildItemID
			INNER JOIN
				OpportunityKitItems
			ON
				OpportunityKitChildItems.numOppChildItemID = OpportunityKitItems.numOppChildItemID
			INNER JOIN
				Item
			ON
				OpportunityKitChildItems.numItemID = Item.numItemCode;
         
			IF LENGTH(coalesce(v_vcWarehouseItemIDs,'')) > 0 THEN
				INSERT INTO tt_TEMPWAREHOUSELOCATIONS
				(
					ID
					,numWarehouseItemID
					,numPickedQty
					,"vcSerialLot#"
				)
				SELECT
					ROW_NUMBER() OVER(ORDER BY ID DESC)
					,ID
					,coalesce(Qty,0)
					,coalesce(SerialLotNo,'')
				FROM
					XMLTABLE
					(
						'NewDataSet/Table1'
						PASSING 
							CAST(v_vcWarehouseItemIDs AS XML)
						COLUMNS
							ID NUMERIC(18,0) PATH 'ID',
							Qty DOUBLE PRECISION PATH 'Qty',
							SerialLotNo TEXT PATH 'SerialLotNo'
					) AS X;
			END IF;
         
			IF COALESCE((SELECT  bitSerial FROM tt_TEMPITEMS LIMIT 1),false) = true AND (SELECT COUNT(*) FROM tt_TEMPWAREHOUSELOCATIONS) > 0 THEN
				v_x := 1;

				SELECT 
					COUNT(*) 
				INTO 
					v_xCount 
				FROM 
					tt_TEMPWAREHOUSELOCATIONS;
            
				WHILE v_x <= v_xCount LOOP
					SELECT 
						numWarehouseItemID
						,COALESCE("vcSerialLot#",'') 
					INTO 
						v_numSerialWarehoueItemID
						,v_vcSerialLot 
					FROM 
						tt_TEMPWAREHOUSELOCATIONS 
					WHERE 
						ID = v_x;
					
					v_numMaxID := coalesce((SELECT MAX(ID) FROM tt_TEMPSERIALLOT),0);
               
					IF v_vcSerialLot <> '' THEN
						INSERT INTO tt_TEMPSERIALLOT
						(
							ID
							,numWarehouseItemID
							,vcSerialNo
							,numQty
						)
						SELECT
							v_numMaxID + ROW_NUMBER() OVER(ORDER BY OutParam)
							,v_numSerialWarehoueItemID
							,CAST(RTRIM(LTRIM(OutParam)) AS VARCHAR(300))
							,1
						FROM
							SplitString(v_vcSerialLot,',');
					END IF;

					v_x := v_x::bigint+1;
				END LOOP;
			ELSEIF COALESCE((SELECT  bitLot FROM tt_TEMPITEMS LIMIT 1),false) = true THEN
				v_x := 1;
				
				SELECT COUNT(*) INTO v_xCount FROM tt_TEMPWAREHOUSELOCATIONS;

				WHILE v_x <= v_xCount LOOP
					SELECT 
						numWarehouseItemID
						,COALESCE("vcSerialLot#",'') 
					INTO 
						v_numSerialWarehoueItemID
						,v_vcSerialLot 
					FROM 
						tt_TEMPWAREHOUSELOCATIONS 
					WHERE 
						ID = v_x;
				
					v_numMaxID := coalesce((SELECT MAX(ID) FROM tt_TEMPSERIALLOT),0);
               
					IF v_vcSerialLot <> '' THEN
						INSERT INTO tt_TEMPSERIALLOT
						(
							ID
							,numWarehouseItemID
							,vcSerialNo
							,numQty
						)
						SELECT
							v_numMaxID + ROW_NUMBER() OVER(ORDER BY OutParam)
							,v_numSerialWarehoueItemID
							,CAST((CASE
									WHEN coalesce(POSITION(substring(OutParam from E'\\(') IN OutParam),0) > 0 AND coalesce(POSITION(substring(OutParam from E'\\)') IN OutParam),0) > 0 AND coalesce(POSITION(substring(OutParam from E'\\)') IN OutParam),0) >(coalesce(POSITION(substring(OutParam from E'\\(') IN OutParam),0)+1)
									THEN RTRIM(LTRIM(SUBSTR(OutParam,0,coalesce(POSITION(substring(OutParam from E'\\(') IN OutParam),0))))
									ELSE ''
								END) AS VARCHAR(300))
							,(CASE
								WHEN coalesce(POSITION(substring(OutParam from E'\\(') IN OutParam),0) > 0 AND coalesce(POSITION(substring(OutParam from E'\\)') IN OutParam),0) > 0 AND coalesce(POSITION(substring(OutParam from E'\\)') IN OutParam),0) >(coalesce(POSITION(substring(OutParam from E'\\(') IN OutParam),0)+1)
								THEN (CASE 
										WHEN SWF_IsNumeric(SUBSTR(OutParam,(coalesce(POSITION(substring(OutParam from E'\\(') IN OutParam),0)+1),(coalesce(POSITION(substring(OutParam from E'\\)') IN OutParam),0) -coalesce(POSITION(substring(OutParam from E'\\(') IN OutParam),0) -1))) = true
										THEN CAST(SUBSTR(OutParam,(coalesce(POSITION(substring(OutParam from E'\\(') IN OutParam),0)+1),(coalesce(POSITION(substring(OutParam from E'\\)') IN OutParam),0) -coalesce(POSITION(substring(OutParam from E'\\(') IN OutParam),0) -1)) AS DOUBLE PRECISION)
										ELSE -1
									END)
								ELSE -1
							END)
						FROM
							SplitString(v_vcSerialLot,',');
					END IF;

					v_x := v_x::bigint+1;
				END LOOP;
			END IF;

			IF EXISTS(SELECT vcSerialNo FROM tt_TEMPSERIALLOT WHERE numQty = -1) THEN
				RAISE EXCEPTION 'INVALID_SERIALLOT_FORMAT';
				RETURN;
			END IF;
			
			v_j := 1;
			v_numwarehouseitmsdtlids := array[]::numeric[];
			
			SELECT COUNT(*) INTO v_jCount FROM tt_TEMPITEMS;

			WHILE v_j <= v_jCount LOOP
				SELECT 
					numOppID
					,numOppItemID
					,numOppChildItemID
					,numOppKitChildItemID
					,bitSerial
					,bitLot
					,numWarehouseItemID
					,numQtyLeftToPick 
				INTO 
					v_numOppID
					,v_numOppItemID
					,v_numOppChildItemID
					,v_numOppKitChildItemID
					,v_bitSerial
					,v_bitLot
					,v_numWarehouseItemID
					,v_numQtyLeftToPick 
				FROM
					tt_TEMPITEMS 
				WHERE
					ID = v_j;
            
				UPDATE
					OpportunityItems
				SET
					numQtyPicked = coalesce(numQtyPicked,0)+(CASE WHEN v_numPickedQty > v_numQtyLeftToPick THEN v_numQtyLeftToPick ELSE v_numPickedQty END)
				WHERE
					numoppitemtCode = v_numOppItemID
					AND coalesce(v_numOppChildItemID,0) = 0
					AND coalesce(v_numOppKitChildItemID,0) = 0;

				IF NOT EXISTS(SELECT ID FROM tt_TEMPORDERS WHERE numOppID = v_numOppID AND numOppItemID = v_numOppItemID AND numOppChildItemID = v_numOppChildItemID AND numOppKitChildItemID = v_numOppKitChildItemID) THEN
					INSERT INTO tt_TEMPORDERS
					(
						numOppID
						,numOppItemID
						,numOppChildItemID
						,numOppKitChildItemID
						,numQtyPicked
					)
					VALUES
					(
						v_numOppID
						,v_numOppItemID
						,v_numOppChildItemID
						,v_numOppKitChildItemID
						,(CASE WHEN v_numPickedQty > v_numQtyLeftToPick THEN v_numQtyLeftToPick ELSE v_numPickedQty END)
					);
				ELSE
					UPDATE
						tt_TEMPORDERS
					SET
						numQtyPicked = coalesce(numQtyPicked,0)+coalesce((CASE WHEN v_numPickedQty > v_numQtyLeftToPick THEN v_numQtyLeftToPick ELSE v_numPickedQty END),0)
					WHERE
						numOppID = v_numOppID
						AND numOppItemID = v_numOppItemID
						AND numOppChildItemID = v_numOppChildItemID
						AND numOppKitChildItemID = v_numOppKitChildItemID;
				END IF;

				v_numPickedQty := v_numPickedQty -(CASE WHEN v_numPickedQty > v_numQtyLeftToPick THEN v_numQtyLeftToPick ELSE v_numPickedQty END);

				IF EXISTS(SELECT numoppitemtCode FROM OpportunityItems WHERE numoppitemtCode = v_numOppItemID AND v_numOppChildItemID = 0 AND v_numOppKitChildItemID = 0 AND coalesce(numQtyPicked,0) > coalesce(numUnitHour,0)) then
					RAISE EXCEPTION 'PICKED_QTY_IS_GREATER_THEN_REMAINING_QTY';
				END IF;
            
				IF EXISTS(SELECT ID FROM tt_TEMPWAREHOUSELOCATIONS WHERE numWarehouseItemID = v_numWarehouseItemID) THEN
					SELECT 
						numWarehouseItemID
						,COALESCE(numPickedQty,0) 
					INTO 
						v_numTempWarehouseItemID
						,v_numWarehousePickedQty 
					FROM
						tt_TEMPWAREHOUSELOCATIONS 
					WHERE
						numWarehouseItemID = v_numWarehouseItemID;
               
					UPDATE
						tt_TEMPWAREHOUSELOCATIONS
					SET
						numPickedQty = coalesce(numPickedQty,0) -(CASE WHEN coalesce(numPickedQty,0) > v_numQtyLeftToPick THEN v_numQtyLeftToPick ELSE coalesce(numPickedQty,0) END)
					WHERE
						numWarehouseItemID = v_numTempWarehouseItemID;

					IF COALESCE(v_bitSerial,false) = true OR COALESCE(v_bitLot,false) = true THEN
						IF  COALESCE((SELECT
										SUM(coalesce(TSL.numQty,0))
									FROM
										WareHouseItmsDTL WID
									INNER JOIN
										tt_TEMPSERIALLOT TSL
									ON
										WID.vcSerialNo = WID.vcSerialNo
									WHERE
										WID.numWareHouseItemID = v_numWarehouseItemID
										AND TSL.numWarehouseItemID = v_numWarehouseItemID
										AND coalesce(TSL.numQty,0) > 0
										AND coalesce(WID.numQty,0) >= coalesce(TSL.numQty,0)),0) >= (CASE WHEN v_numWarehousePickedQty >= v_numQtyLeftToPick THEN v_numQtyLeftToPick ELSE v_numWarehousePickedQty END) 
						THEN
							v_x := 1;
                     
							SELECT COUNT(*) INTO v_xCount FROM tt_TEMPSERIALLOT;
                     
							v_numSerialLotQtyToPick :=(CASE WHEN v_numWarehousePickedQty >= v_numQtyLeftToPick THEN v_numQtyLeftToPick ELSE v_numWarehousePickedQty END);

							WHILE v_x <= v_xCount AND v_numSerialLotQtyToPick > 0 LOOP
								SELECT 
									vcSerialNo
									,numQty 
								INTO 
									v_vcSerialLotNo
									,v_numSerialLotQty 
								FROM 
									tt_TEMPSERIALLOT 
								WHERE 
									ID = v_x;
								
								IF coalesce(v_numSerialLotQty,0) > 0 THEN
									IF v_bitSerial = true THEN
										IF EXISTS(SELECT numWareHouseItmsDTLID FROM WareHouseItmsDTL WHERE numWareHouseItemID = v_numWarehouseItemID AND vcSerialNo = v_vcSerialLotNo AND numQty >= v_numSerialLotQty) THEN
											SELECT 
												numWareHouseItmsDTLID INTO v_numWareHouseItmsDTLID 
											FROM
												WareHouseItmsDTL 
											WHERE
												numWareHouseItemID = v_numWarehouseItemID
												AND vcSerialNo = v_vcSerialLotNo
												AND numQty >= v_numSerialLotQty 
											LIMIT 1;
											
											INSERT INTO OppWarehouseSerializedItem
											(
												numWarehouseItmsDTLID
												,numWarehouseItmsID
												,numOppID
												,numOppItemID
												,numQty
											)
											SELECT
												numWareHouseItmsDTLID
												,numWareHouseItemID
												,v_numOppID
												,v_numOppItemID
												,v_numSerialLotQty
											FROM
												WareHouseItmsDTL
											WHERE
												numWareHouseItmsDTLID = v_numWareHouseItmsDTLID;
											
											v_numSerialLotQtyToPick := v_numSerialLotQtyToPick -v_numSerialLotQty;
											
											UPDATE tt_TEMPSERIALLOT SET numQty = coalesce(numQty,0) -v_numSerialLotQty  WHERE ID = v_x;
											UPDATE WareHouseItmsDTL SET numQty = coalesce(numQty,0) -v_numSerialLotQty  WHERE numWareHouseItmsDTLID = v_numWareHouseItmsDTLID;
										ELSE
											RAISE EXCEPTION 'INVLAID_SERIALLOT_QTY';
											RETURN;
										END IF;
									ELSEIF v_bitLot = true THEN
										IF coalesce((SELECT SUM(numQty) FROM WareHouseItmsDTL WHERE numWareHouseItemID = v_numWarehouseItemID AND vcSerialNo = v_vcSerialLotNo),0) >= v_numSerialLotQty THEN										
											DELETE FROM tt_TEMPWAREHOUSEDTL;
											INSERT INTO tt_TEMPWAREHOUSEDTL
											(
												ID
												,numWareHouseItmsDTLID
											)
											SELECT
												ROW_NUMBER() OVER(ORDER BY numWareHouseItmsDTLID)
												,numWareHouseItmsDTLID
											FROM
												WareHouseItmsDTL
											WHERE
												numWareHouseItemID = v_numWarehouseItemID
												AND vcSerialNo = v_vcSerialLotNo;
											
											v_y := 1;
									
											SELECT COUNT(*) INTO v_yCount FROM tt_TEMPWAREHOUSEDTL;

											WHILE v_y <= v_yCount AND v_numSerialLotQty > 0 LOOP
												SELECT 
													numWareHouseItmsDTLID INTO v_numWareHouseItmsDTLID 
												FROM 
													tt_TEMPWAREHOUSEDTL 
												WHERE 
													ID = v_y;
                                    
												IF COALESCE((SELECT numQty FROM WareHouseItmsDTL WHERE numWareHouseItmsDTLID = v_numWareHouseItmsDTLID),0) > v_numSerialLotQty THEN
													INSERT INTO OppWarehouseSerializedItem
													(
														numWarehouseItmsDTLID
														,numWarehouseItmsID
														,numOppID
														,numOppItemID
														,numQty
													)
													SELECT
														numWareHouseItmsDTLID
														,v_numWarehouseItemID
														,v_numOppID
														,v_numOppItemID
														,v_numSerialLotQty
													FROM
														WareHouseItmsDTL
													WHERE
													   numWareHouseItmsDTLID = v_numWareHouseItmsDTLID;
													   v_numSerialLotQtyToPick := v_numSerialLotQtyToPick -v_numSerialLotQty;
													   v_numSerialLotQty := 0;

													UPDATE tt_TEMPSERIALLOT SET numQty = coalesce(numQty,0) -v_numSerialLotQty WHERE ID = v_x;
													UPDATE WareHouseItmsDTL SET numQty = coalesce(numQty,0) -v_numSerialLotQty  WHERE numWareHouseItmsDTLID = v_numWareHouseItmsDTLID;
												ELSE
													INSERT INTO OppWarehouseSerializedItem
													(
														numWarehouseItmsDTLID
														,numWarehouseItmsID
														,numOppID
														,numOppItemID
														,numQty
													)
													SELECT
														numWareHouseItmsDTLID
														,v_numWarehouseItemID
														,v_numOppID
														,v_numOppItemID
														,numQty
													FROM
														WareHouseItmsDTL
													WHERE
														numWareHouseItmsDTLID = v_numWareHouseItmsDTLID;
														v_numSerialLotQtyToPick := v_numSerialLotQtyToPick - COALESCE((SELECT numQty FROM WareHouseItmsDTL WHERE numWareHouseItmsDTLID = v_numWareHouseItmsDTLID),0);
                                       
													v_numSerialLotQty := v_numSerialLotQty - COALESCE((SELECT numQty FROM WareHouseItmsDTL WHERE numWareHouseItmsDTLID = v_numWareHouseItmsDTLID),0);
                                       
													UPDATE tt_TEMPSERIALLOT SET numQty = coalesce(numQty,0) -coalesce((SELECT numQty FROM WareHouseItmsDTL WHERE numWareHouseItmsDTLID = v_numWareHouseItmsDTLID),0)  WHERE ID = v_x;
													UPDATE WareHouseItmsDTL SET numQty = 0  WHERE numWareHouseItmsDTLID = v_numWareHouseItmsDTLID;
												END IF;

												v_y := v_y::bigint+1;
											END LOOP;
                                 
										IF coalesce(v_numSerialLotQty,0) > 0 THEN	
											RAISE EXCEPTION 'INVLAID_SERIALLOT_QTY';
											RETURN;
										END IF;
									END IF;
								END IF;
							END IF;
							
							v_x := v_x::bigint+1;
						END LOOP;
                     
						IF v_numSerialLotQtyToPick > 0 THEN	
							RAISE EXCEPTION 'SUFFICIENT_SERIALLOT_QTY_NOT_PICKED';
							RETURN;
						END IF;
					ELSE
						RAISE EXCEPTION 'INVLAID_SERIALLOT_QTY';
						RETURN;
					END IF;
				END IF;
				
				v_numQtyLeftToPick := v_numQtyLeftToPick -(CASE WHEN v_numWarehousePickedQty >= v_numQtyLeftToPick THEN v_numQtyLeftToPick ELSE v_numWarehousePickedQty END);
			END IF;
            
			IF v_numQtyLeftToPick > 0 THEN
				SELECT COUNT(*) INTO v_kCount FROM tt_TEMPWAREHOUSELOCATIONS;

				WHILE v_k <= v_kCount LOOP
					SELECT 
						numWarehouseItemID
						,COALESCE(numPickedQty,0) 
					INTO 
						v_numTempWarehouseItemID
						,v_numWarehousePickedQty 
					FROM
						tt_TEMPWAREHOUSELOCATIONS 
					WHERE
						ID = v_k;
                  
					UPDATE
						tt_TEMPWAREHOUSELOCATIONS
					SET
						numPickedQty = COALESCE(numPickedQty,0) - (CASE WHEN coalesce(numPickedQty,0) > v_numQtyLeftToPick THEN v_numQtyLeftToPick ELSE coalesce(numPickedQty,0) END)
					WHERE
						numWarehouseItemID = v_numTempWarehouseItemID;

					IF COALESCE(v_bitSerial,false) = true OR COALESCE(v_bitLot,false) = true THEN
						IF COALESCE((SELECT
										SUM(coalesce(TSL.numQty,0))
									FROM
										WareHouseItmsDTL WID
									INNER JOIN
										tt_TEMPSERIALLOT TSL
									ON
										WID.vcSerialNo = WID.vcSerialNo
									WHERE
										WID.numWareHouseItemID = v_numTempWarehouseItemID
										AND TSL.numWarehouseItemID = v_numTempWarehouseItemID
										AND coalesce(TSL.numQty,0) > 0
										AND coalesce(WID.numQty,0) >= coalesce(TSL.numQty,0)),0) >= (CASE WHEN v_numWarehousePickedQty >= v_numQtyLeftToPick THEN v_numQtyLeftToPick ELSE v_numWarehousePickedQty END) 
						THEN
							v_x := 1;
							
							SELECT 
								COUNT(*) 
							INTO 
								v_xCount 
							FROM 
								tt_TEMPSERIALLOT;
                        
							v_numSerialLotQtyToPick := (CASE WHEN v_numWarehousePickedQty >= v_numQtyLeftToPick THEN v_numQtyLeftToPick ELSE v_numWarehousePickedQty END);
							
							WHILE v_x <= v_xCount AND v_numSerialLotQtyToPick > 0 LOOP
								SELECT 
									vcSerialNo
									,numQty 
								INTO 
									v_vcSerialLotNo
									,v_numSerialLotQty 
								FROM 
									tt_TEMPSERIALLOT 
								WHERE 
									ID = v_x;
								
								IF COALESCE(v_numSerialLotQty,0) > 0 THEN
									IF v_bitSerial = true THEN
										IF EXISTS(SELECT numWareHouseItmsDTLID FROM WareHouseItmsDTL WHERE numWareHouseItemID = v_numTempWarehouseItemID AND vcSerialNo = v_vcSerialLotNo AND numQty >= v_numSerialLotQty) THEN
											SELECT 
												numWareHouseItmsDTLID 
											INTO 
												v_numWareHouseItmsDTLID 
											FROM
												WareHouseItmsDTL 
											WHERE
												numWareHouseItemID = v_numTempWarehouseItemID
												AND vcSerialNo = v_vcSerialLotNo
												AND numQty >= v_numSerialLotQty 
											LIMIT 1;
                                    
											INSERT INTO OppWarehouseSerializedItem
											(
												numWarehouseItmsDTLID
												,numWarehouseItmsID
												,numOppID
												,numOppItemID
												,numQty
											)
											SELECT
												numWareHouseItmsDTLID
												,v_numWarehouseItemID
												,v_numOppID
												,v_numOppItemID
												,v_numSerialLotQty
											FROM
												WareHouseItmsDTL
											WHERE
												numWareHouseItmsDTLID = v_numWareHouseItmsDTLID;

											v_numSerialLotQtyToPick := v_numSerialLotQtyToPick -v_numSerialLotQty;

											UPDATE tt_TEMPSERIALLOT SET numQty = coalesce(numQty,0) -v_numSerialLotQty  WHERE ID = v_x;

											UPDATE WareHouseItmsDTL SET numQty = coalesce(numQty,0) -v_numSerialLotQty,numWareHouseItemID = v_numWarehouseItemID  WHERE numWareHouseItmsDTLID = v_numWareHouseItmsDTLID;
											v_numwarehouseitmsdtlids := array_append(v_numwarehouseitmsdtlids, v_numWareHouseItmsDTLID);
										ELSE
											RAISE NOTICE 'INVLAID_SERIALLOT_QTY';
											RETURN;
										END IF;
									ELSEIF v_bitLot = true THEN										
										IF COALESCE((SELECT SUM(numQty) FROM WareHouseItmsDTL WHERE numWareHouseItemID = v_numTempWarehouseItemID AND vcSerialNo = v_vcSerialLotNo),0) >= v_numSerialLotQty THEN
											DELETE FROM tt_TEMPWAREHOUSEDTL;
											INSERT INTO tt_TEMPWAREHOUSEDTL
											(
												ID
												,numWareHouseItmsDTLID
											)
											SELECT
												ROW_NUMBER() OVER(ORDER BY numWareHouseItmsDTLID)
												,numWareHouseItmsDTLID
											FROM
												WareHouseItmsDTL
											WHERE
												numWareHouseItemID = v_numTempWarehouseItemID
												AND vcSerialNo = v_vcSerialLotNo;
                                    
											v_y := 1;
											
											SELECT COUNT(*) INTO v_yCount FROM tt_TEMPWAREHOUSEDTL;

											WHILE v_y <= v_yCount AND v_numSerialLotQty > 0 LOOP
												SELECT 
													numWareHouseItmsDTLID INTO v_numWareHouseItmsDTLID 
												FROM 
													tt_TEMPWAREHOUSEDTL 
												WHERE 
													ID = v_y;
												
												IF COALESCE((SELECT numQty FROM WareHouseItmsDTL WHERE numWareHouseItmsDTLID = v_numWareHouseItmsDTLID),0) > v_numSerialLotQty THEN
													INSERT INTO WareHouseItmsDTL
													(
														numWareHouseItemID
														,vcSerialNo
														,numQty
													)
													VALUES
													(
														v_numWarehouseItemID
														,v_vcSerialLotNo
														,v_numSerialLotQty
													) RETURNING numWareHouseItmsDTLID INTO v_numTempWareHouseItmsDTLID;

													v_numwarehouseitmsdtlids := array_append(v_numwarehouseitmsdtlids, v_numTempWareHouseItmsDTLID);
													
													INSERT INTO OppWarehouseSerializedItem
													(
														numWarehouseItmsDTLID
														,numWarehouseItmsID
														,numOppID
														,numOppItemID
														,numQty
													)
													SELECT
														v_numTempWareHouseItmsDTLID
														,v_numWarehouseItemID
														,v_numOppID
														,v_numOppItemID
														,v_numSerialLotQty
													FROM
														WareHouseItmsDTL
													WHERE
														numWareHouseItmsDTLID = v_numWareHouseItmsDTLID;
														
													v_numSerialLotQtyToPick := v_numSerialLotQtyToPick -v_numSerialLotQty;

													v_numSerialLotQty := 0;

													UPDATE tt_TEMPSERIALLOT SET numQty = coalesce(numQty,0) -v_numSerialLotQty WHERE ID = v_x;
													UPDATE WareHouseItmsDTL SET numQty = coalesce(numQty,0) -v_numSerialLotQty  WHERE numWareHouseItmsDTLID = v_numWareHouseItmsDTLID;
												ELSE
													INSERT INTO OppWarehouseSerializedItem
													(
														numWarehouseItmsDTLID
														,numWarehouseItmsID
														,numOppID
														,numOppItemID
														,numQty
													)
													SELECT
														numWareHouseItmsDTLID
														,v_numWarehouseItemID
														,v_numOppID
														,v_numOppItemID
														,numQty
													FROM
														WareHouseItmsDTL
													WHERE
														numWareHouseItmsDTLID = v_numWareHouseItmsDTLID;

													v_numSerialLotQtyToPick := v_numSerialLotQtyToPick - COALESCE((SELECT numQty FROM WareHouseItmsDTL WHERE numWareHouseItmsDTLID = v_numWareHouseItmsDTLID),0);
													v_numSerialLotQty := v_numSerialLotQty - COALESCE((SELECT numQty FROM WareHouseItmsDTL WHERE numWareHouseItmsDTLID = v_numWareHouseItmsDTLID),0);

													UPDATE tt_TEMPSERIALLOT SET numQty = coalesce(numQty,0) -coalesce((SELECT numQty FROM WareHouseItmsDTL WHERE numWareHouseItmsDTLID = v_numWareHouseItmsDTLID),0)  WHERE ID = v_x;
													UPDATE WareHouseItmsDTL SET numWareHouseItemID = v_numWarehouseItemID WHERE numWareHouseItmsDTLID = v_numWareHouseItmsDTLID;

													v_numwarehouseitmsdtlids := array_append(v_numwarehouseitmsdtlids, v_numWareHouseItmsDTLID);
												END IF;
												
												v_y := v_y::bigint+1;
											END LOOP;
                                    
											IF coalesce(v_numSerialLotQty,0) > 0 THEN
											   RAISE EXCEPTION 'INVLAID_SERIALLOT_QTY';
											   RETURN;
											END IF;
										END IF;
									END IF;
								END IF;
                           
								v_x := v_x::bigint+1;
							END LOOP;
                        
							IF v_numSerialLotQtyToPick > 0 THEN	
								RAISE EXCEPTION 'SUFFICIENT_SERIALLOT_QTY_NOT_PICKED';
								RETURN;
							END IF;
						ELSE
							RAISE EXCEPTION 'INVLAID_SERIALLOT_QTY';
							RETURN;
						END IF;
					END IF;
                  
					IF v_numWarehousePickedQty > 0 AND (v_numWarehouseItemID <> v_numTempWarehouseItemID) THEN	
						SELECT 
							COALESCE(numOnHand,0)
							,COALESCE(numonOrder,0)
							,COALESCE(numAllocation,0)
							,COALESCE(numBackOrder,0) 
						INTO 
							v_numFromOnHand
							,v_numFromOnOrder
							,v_numFromAllocation
							,v_numFromBackOrder 
						FROM
							WareHouseItems 
						WHERE
							numWareHouseItemID = v_numTempWarehouseItemID;

						SELECT 
							COALESCE(numOnHand,0)
							,COALESCE(numonOrder,0)
							,COALESCE(numAllocation,0)
							,COALESCE(numBackOrder,0) 
						INTO 
							v_numToOnHand
							,v_numToOnOrder
							,v_numToAllocation
							,v_numToBackOrder 
						FROM
							WareHouseItems 
						WHERE
							numWareHouseItemID = v_numWarehouseItemID;

						IF v_numWarehousePickedQty >= v_numQtyLeftToPick THEN
							v_vcDescription := CONCAT('Items picked Qty(:',v_numQtyLeftToPick,')');

							INSERT INTO tt_TEMPOIPICKEDLOCATION
							(
								numdomainid,
								numoppid,
								numoppitemid,
								numfromwarehouseitemid,
								numtowarehouseitemid,
								numunitpicked,
								numwarehouseitmsdtlids
							)
							VALUES
							(
								v_numDomainID
								,v_numOppID
								,v_numOppItemID
								,v_numTempWarehouseItemID
								,v_numWarehouseItemID
								,v_numQtyLeftToPick
								,v_numwarehouseitmsdtlids
							);
                        
							IF (v_numFromOnHand+v_numFromAllocation) >= v_numQtyLeftToPick THEN
							   IF v_numFromOnHand >= v_numQtyLeftToPick THEN
								  v_numFromOnHand := v_numFromOnHand -v_numQtyLeftToPick;
							   ELSE
								  v_numFromOnHand := 0;
								  v_numFromAllocation := v_numFromAllocation -(v_numQtyLeftToPick -v_numFromOnHand);
								  v_numFromBackOrder := v_numFromBackOrder+(v_numQtyLeftToPick -v_numFromOnHand);
							   END IF;
							ELSE
								RAISE NOTICE 'numTempWarehouseItemID(%)',v_numTempWarehouseItemID;
								RAISE NOTICE '%',v_numFromOnHand;
								RAISE NOTICE '%',v_numFromAllocation;
								RAISE NOTICE '%',v_numQtyLeftToPick;
								RAISE EXCEPTION 'AVAILABLE_QUANTITY_LESS_THEN_PICKED_QTY';
							END IF;

							IF v_numToBackOrder >= v_numQtyLeftToPick THEN								
								v_numToBackOrder := v_numToBackOrder -v_numQtyLeftToPick;
								v_numToAllocation := v_numToAllocation+v_numQtyLeftToPick;
							ELSE
							   v_numToAllocation := v_numToAllocation+v_numToBackOrder;
							   v_numToOnHand := v_numToOnHand+(v_numQtyLeftToPick -v_numToBackOrder);
							   v_numToBackOrder := 0;
							END IF;

							v_numQtyLeftToPick := 0;
						ELSE
							v_vcDescription := CONCAT('Items picked (Qty:',v_numWarehousePickedQty,')');

							INSERT INTO tt_TEMPOIPICKEDLOCATION
							(
								numdomainid,
								numoppid,
								numoppitemid,
								numfromwarehouseitemid,
								numtowarehouseitemid,
								numunitpicked,
								numwarehouseitmsdtlids
							)
							VALUES
							(
								v_numDomainID
								,v_numOppID
								,v_numOppItemID
								,v_numTempWarehouseItemID
								,v_numWarehouseItemID
								,v_numWarehousePickedQty
								,v_numwarehouseitmsdtlids
							);

							IF(v_numFromOnHand+v_numFromAllocation) >= v_numWarehousePickedQty THEN
							   IF v_numFromOnHand >= v_numWarehousePickedQty THEN
								  v_numFromOnHand := v_numFromOnHand -v_numWarehousePickedQty;
							   ELSE
								  v_numFromOnHand := 0;
								  v_numFromAllocation := v_numFromAllocation -(v_numWarehousePickedQty -v_numFromOnHand);
								  v_numFromBackOrder := v_numFromBackOrder+(v_numWarehousePickedQty -v_numFromOnHand);
							   END IF;
							ELSE
								RAISE NOTICE '%',v_numFromOnHand;
								RAISE NOTICE '%',v_numFromAllocation;
								RAISE NOTICE '%',v_numWarehousePickedQty;
							   RAISE EXCEPTION 'AVAILABLE_QUANTITY_LESS_THEN_PICKED_QTY';
							END IF;

							IF v_numToBackOrder >= v_numWarehousePickedQty THEN	
							   v_numToBackOrder := v_numToBackOrder -v_numWarehousePickedQty;
							   v_numToAllocation := v_numToAllocation+v_numWarehousePickedQty;
							ELSE
							   v_numToAllocation := v_numToAllocation+v_numToBackOrder;
							   v_numToOnHand := v_numToOnHand+(v_numWarehousePickedQty -v_numToBackOrder);
							   v_numToBackOrder := 0;
							END IF;

							v_numQtyLeftToPick := v_numQtyLeftToPick -v_numWarehousePickedQty;
						END IF;

						--UPDATE INVENTORY AND WAREHOUSE TRACKING
						PERFORM USP_UpdateInventoryAndTracking(v_numOnHand := v_numFromOnHand,v_numOnAllocation := v_numFromAllocation,
						v_numOnBackOrder := v_numFromBackOrder,v_numOnOrder := v_numFromOnOrder,
						v_numWarehouseItemID := v_numTempWarehouseItemID,v_numReferenceID := v_numOppID,
						v_tintRefType := 3::SMALLINT,v_numDomainID := v_numDomainID,
						v_numUserCntID := v_numUserCntID,v_Description := v_vcDescription);	

						v_vcDescription := REPLACE(v_vcDescription,'picked','receieved');

						PERFORM USP_UpdateInventoryAndTracking(v_numOnHand := v_numToOnHand,v_numOnAllocation := v_numToAllocation,v_numOnBackOrder := v_numToBackOrder,
						v_numOnOrder := v_numToOnOrder,v_numWarehouseItemID := v_numWarehouseItemID,
						v_numReferenceID := v_numOppID,
						v_tintRefType := 3::SMALLINT,v_numDomainID := v_numDomainID,v_numUserCntID := v_numUserCntID,
						v_Description := v_vcDescription);

					ELSEIF v_numWarehousePickedQty > 0 THEN	
						IF v_numWarehousePickedQty > v_numFromAllocation THEN	
							RAISE NOTICE '%',v_numFromOnHand;
							RAISE NOTICE '%',v_numFromAllocation;
							RAISE NOTICE '%',v_numWarehousePickedQty;
							RAISE EXCEPTION 'AVAILABLE_QUANTITY_LESS_THEN_PICKED_QTY';
						ELSE
							IF v_numWarehousePickedQty >= v_numQtyLeftToPick THEN								
								v_numQtyLeftToPick := 0;
							ELSE
								v_numQtyLeftToPick := v_numQtyLeftToPick -v_numWarehousePickedQty;
							END IF;
						END IF;
					END IF;
                  
					IF v_numQtyLeftToPick <= 0 then
						EXIT;
					END IF;

					v_k := v_k::bigint+1;
				END LOOP;
			END IF;

            IF v_numPickedQty <= 0 THEN
				EXIT;
            END IF;

			v_j := v_j::bigint+1;
		END LOOP;
         
		IF v_numPickedQty > 0 AND v_numOppChildItemID = 0 AND v_numOppKitChildItemID = 0 THEN	
            RAISE EXCEPTION 'PICKED_QTY_IS_GREATER_THEN_REMAINING_QTY';
        END IF;
         
		v_i := v_i::bigint+1;
    END LOOP;
		
	IF COALESCE(v_numStatus,0) > 0 THEN
		UPDATE
			OpportunityMaster
		SET
			numStatus = v_numStatus
		WHERE
			numDomainId = v_numDomainID
			AND numOppId IN(SELECT DISTINCT OI.numOppId FROM tt_TEMPITEMS T1 INNER JOIN OpportunityItems OI ON T1.numOppItemID = OI.numoppitemtCode)
			AND NOT EXISTS(SELECT
								numoppitemtCode
							FROM
								OpportunityItems
							WHERE
								numOppId = OpportunityMaster.numOppId
								AND coalesce(numWarehouseItmsID,0) > 0
								AND coalesce(bitDropShip,false) = false
								AND coalesce(numUnitHour,0) <> coalesce(numQtyPicked,0));
	END IF;
      
    DROP TABLE IF EXISTS tt_TEMPPICKLIST CASCADE;
    CREATE TEMPORARY TABLE tt_TEMPPICKLIST
    (
        ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1),
        numOppID NUMERIC(18,0),
        vcItems TEXT
    );

    INSERT INTO tt_TEMPPICKLIST
	(
		numOppID
		,vcItems
	)
    SELECT
		numOppID
		,CONCAT('<NewDataSet>',(SELECT 
									string_agg(CONCAT(' <BizDocItems><OppItemID>',numOppItemID,'</OppItemID>','<Quantity>',coalesce(numQtyPicked,0),'</Quantity>','<Notes></Notes><monPrice>0</monPrice></BizDocItems>'),'')
								FROM
									tt_TEMPORDERS TOR
								WHERE
									TOR.numOppID = TORMain.numOppID
									AND coalesce(numOppChildItemID,0) = 0
									AND coalesce(numOppKitChildItemID,0) = 0),'</NewDataSet>')
    FROM
		tt_TEMPORDERS TORMain
    GROUP BY
		numOppID;

    v_k := 1;
      
	SELECT COUNT(*) INTO v_kCount FROM tt_TEMPPICKLIST;

    SELECT 
		numBizDocTempID INTO v_numBizDocTempID 
	FROM
		BizDocTemplate 
	WHERE
		numDomainID = v_numDomainID
		AND numBizDocID = 55206 
	ORDER BY
		COALESCE(bitEnabled,false) DESC
		,COALESCE(bitDefault,false) DESC 
	LIMIT 1;
	  
	v_pickLists := '';

    WHILE v_k <= v_kCount LOOP
		SELECT 
			numOppID
			,vcItems 
		INTO 
			v_numTempOppID
			,v_vcItems 
		FROM 
			tt_TEMPPICKLIST 
		WHERE 
			ID = v_k;
         
		IF coalesce(v_vcItems,'') <> '' THEN
            SELECT 
				COALESCE(intUsedShippingCompany,0)
				,TIMEZONE('UTC',now())+CAST(-1*v_ClientTimeZoneOffset::bigint || 'minute' as interval) 
			INTO 
				v_intUsedShippingCompany
				,v_dtFromDate 
			FROM
				OpportunityMaster 
			WHERE
				numOppId = v_numTempOppID;
            
			v_USP_CreateBizDocs_numOppBizDocsId := 0;
            v_USP_CreateBizDocs_monCreditAmount := 0;
            v_USP_CreateBizDocs_monDealAmount := 0;
            
			SELECT 
				* 
			INTO 
				v_USP_CreateBizDocs_numOppBizDocsId 
			FROM 
				USP_CreateBizDocs(v_numTempOppID,55206,v_numUserCntID,v_USP_CreateBizDocs_numOppBizDocsId,
								'',true,v_vcItems,v_intUsedShippingCompany,'',v_dtFromDate,0,false,'',
								0::SMALLINT,v_USP_CreateBizDocs_monCreditAmount,v_ClientTimeZoneOffset,
								v_USP_CreateBizDocs_monDealAmount,false,v_numBizDocTempID,'','',0,0,
								true,false,false,0,false,NULL,NULL,0::SMALLINT,'',0,false,false,0,false,
								NULL,0);

			UPDATE tt_TEMPOIPICKEDLOCATION SET numoppbizdocid=v_USP_CreateBizDocs_numOppBizDocsId WHERE numOppID=v_numTempOppID;

			v_pickLists = CONCAT(v_pickLists,(CASE WHEN LENGTH(v_pickLists) > 0 THEN ',' ELSE '' END),v_numTempOppID,'-',v_USP_CreateBizDocs_numOppBizDocsId);

			IF v_bitAutoGeneratePackingSlip THEN
				SELECT
					numBizDocTempID INTO v_numBizDocTempID 
				FROM
					BizDocTemplate 
				WHERE
					numDomainID = v_numDomainID
					AND numBizDocID = 29397 
				ORDER BY
					COALESCE(bitEnabled,false) DESC
					,COALESCE(bitDefault,false) DESC 
				LIMIT 1;

				PERFORM USP_CreateBizDocs(v_numTempOppID,29397,v_numUserCntID,0,
										'',true,v_vcItems,v_intUsedShippingCompany,'',v_dtFromDate,0,false,'',
										0::SMALLINT,v_USP_CreateBizDocs_monCreditAmount,v_ClientTimeZoneOffset,
										v_USP_CreateBizDocs_monDealAmount,false,v_numBizDocTempID,'','',0,0,
										true,false,false,0,false,NULL,NULL,0::SMALLINT,'',0,false,false,v_USP_CreateBizDocs_numOppBizDocsId,false,
										NULL,0);
			END IF;
         END IF;
         
		 v_k := v_k::bigint+1;
    END LOOP;

	INSERT INTO OpportunityItemsPickedLocation
	(
		numdomainid,
		numoppid,
		numoppitemid,
		numfromwarehouseitemid,
		numtowarehouseitemid,
		numunitpicked,
		numoppbizdocid,
		numwarehouseitmsdtlids
	)
	SELECT
		numdomainid,
		numoppid,
		numoppitemid,
		numfromwarehouseitemid,
		numtowarehouseitemid,
		numunitpicked,
		numoppbizdocid,
		numwarehouseitmsdtlids
	FROM
		tt_TEMPOIPICKEDLOCATION;

	OPEN SWV_RefCur FOR SELECT v_pickLists AS vcpicklists;
EXCEPTION WHEN OTHERS THEN
     GET STACKED DIAGNOSTICS
			  my_ex_state   = RETURNED_SQLSTATE,
			  my_ex_message = MESSAGE_TEXT,
			  my_ex_detail  = PG_EXCEPTION_DETAIL,
			  my_ex_hint    = PG_EXCEPTION_HINT,
			  my_ex_ctx     = PG_EXCEPTION_CONTEXT;

			 raise exception '% % % % %', my_ex_state, my_ex_message, my_ex_detail, my_ex_hint, my_ex_ctx;
   END;
END; $$;


