DROP FUNCTION IF EXISTS USP_ElasticSearch_GetGeneralLedger;
CREATE OR REPLACE FUNCTION USP_ElasticSearch_GetGeneralLedger(v_numDomainID NUMERIC(18,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_dtStartDate  TIMESTAMP;
   v_dtEndDate  TIMESTAMP;
BEGIN
   select   dtPeriodFrom, dtPeriodTo INTO v_dtStartDate,v_dtEndDate FROM FinancialYear WHERE numDomainId = v_numDomainID AND coalesce(bitCurrentYear,false) = true;

   IF Not v_dtStartDate IS NULL AND  Not v_dtEndDate IS NULL then
	
      open SWV_RefCur for SELECT *,
			CONCAT('<b style="color:#7f7f7f">G/L Transaction (',"Search_TransactionType",'):</b>',FormatedDateFromDate("SearchDate_EntryDate",v_numDomainID),', ',"Search_CompanyName",', ',"Search_Memo",', ',"Search_CompanyName",', ',"SearchNumber_Amount") AS displaytext
      FROM (SELECT
         GJD.numJournalId as id,
				'generalledger'  as module,
				CONCAT('/Accounting/frmNewJournalEntry.aspx?frm=GeneralLedger&JournalId=',GJD.numJournalId) AS url,
				GJD.numTransactionId AS text,
				GJH.datEntry_Date AS "SearchDate_EntryDate",
				coalesce(CI.vcCompanyName,'') AS "Search_CompanyName",
				(CASE WHEN coalesce(GJD.numCreditAmt,0) = 0 THEN GJD.numDebitAmt ELSE GJD.numCreditAmt END) as "SearchNumber_Amount",
				coalesce(GJD.varDescription,'') AS "Search_Memo",
				(CASE WHEN SUBSTR(GJH.varDescription,length(GJH.varDescription) -1+1) = ':' THEN SUBSTR(GJH.varDescription,0,LENGTH(GJH.varDescription)) ELSE coalesce(GJH.varDescription,'') END)  AS "Search_Description",
				CASE
         WHEN coalesce(GJH.numCheckHeaderID,0) <> 0 THEN 'Check'
         WHEN coalesce(GJH.numDepositId,0) <> 0 AND DM.tintDepositePage = 1 THEN 'Deposit'
         WHEN coalesce(GJH.numDepositId,0) <> 0 AND DM.tintDepositePage = 2 THEN 'Received Payment'
         WHEN coalesce(GJH.numOppId,0) <> 0 AND coalesce(GJH.numOppBizDocsId,0) <> 0 AND coalesce(GJH.numBizDocsPaymentDetId,0) = 0 AND OpportunityMaster.tintopptype = 1 THEN 'BizDocs Invoice'
         WHEN coalesce(GJH.numOppId,0) <> 0 AND coalesce(GJH.numOppBizDocsId,0) <> 0 AND coalesce(GJH.numBizDocsPaymentDetId,0) = 0 AND OpportunityMaster.tintopptype = 2 THEN 'BizDocs Purchase'
         WHEN coalesce(GJH.numCategoryHDRID,0) <> 0 THEN 'Time And Expenses'
         WHEN coalesce(GJH.numBillID,0) <> 0 AND coalesce(BH.bitLandedCost,false) = true then 'Landed Cost'
         WHEN coalesce(GJH.numBillID,0) <> 0 THEN 'Bill'
         WHEN coalesce(GJH.numBillPaymentID,0) <> 0 THEN 'Bill Payment'
         WHEN coalesce(GJH.numReturnID,0) <> 0 THEN
            CASE
            WHEN RH.tintReturnType = 1 AND RH.tintReceiveType = 1 THEN 'Sales Return Refund'
            WHEN RH.tintReturnType = 1 AND RH.tintReceiveType = 2 THEN 'Sales Return Credit Memo'
            WHEN RH.tintReturnType = 2 AND RH.tintReceiveType = 2 THEN 'Purchase Return Credit Memo'
            WHEN RH.tintReturnType = 3 AND RH.tintReceiveType = 2 THEN 'Credit Memo'
            WHEN RH.tintReturnType = 4 AND RH.tintReceiveType = 1 THEN 'Refund Against Credit Memo'
            END
         WHEN coalesce(GJH.numOppId,0) <> 0 AND coalesce(GJH.numOppBizDocsId,0) = 0 THEN 'PO Fulfillment'
         WHEN GJH.numJOurnal_Id <> 0 THEN 'Journal'
         END AS "Search_TransactionType",
				coalesce(CAST(CH.numCheckNo AS VARCHAR),'') AS "Search_CheckNo"
         FROM
         General_Journal_Header GJH
         INNER JOIN General_Journal_Details GJD ON GJH.numJOurnal_Id = GJD.numJournalId
         INNER JOIN Chart_Of_Accounts ON GJD.numChartAcntId = Chart_Of_Accounts.numAccountId
         LEFT OUTER JOIN timeandexpense ON GJH.numCategoryHDRID = timeandexpense.numCategoryHDRID
         LEFT OUTER JOIN DivisionMaster ON GJD.numCustomerId = DivisionMaster.numDivisionID and GJD.numDomainId = DivisionMaster.numDomainID
         LEFT OUTER JOIN CompanyInfo CI ON DivisionMaster.numCompanyID = CI.numCompanyId
         LEFT OUTER JOIN OpportunityMaster ON GJH.numOppId = OpportunityMaster.numOppId
         LEFT OUTER JOIN OpportunityBizDocs ON GJH.numOppBizDocsId = OpportunityBizDocs.numOppBizDocsId
         LEFT OUTER JOIN VIEW_BIZPAYMENT ON GJH.numBizDocsPaymentDetId = VIEW_BIZPAYMENT.numBizDocsPaymentDetId
         LEFT OUTER JOIN DepositMaster DM ON DM.numDepositId = GJH.numDepositId
         LEFT OUTER JOIN ReturnHeader RH ON RH.numReturnHeaderID = GJH.numReturnID
         LEFT OUTER JOIN CheckHeader CH ON GJH.numCheckHeaderID = CH.numCheckHeaderID
         LEFT OUTER JOIN BillHeader BH  ON coalesce(GJH.numBillID,0) = BH.numBillID
         WHERE
         GJH.datEntry_Date >= v_dtStartDate
         AND GJH.datEntry_Date <= v_dtEndDate
         AND GJD.numDomainId = v_numDomainID
         AND GJD.numChartAcntId IN(SELECT COA.numAccountId FROM Chart_Of_Accounts COA WHERE numDomainId = v_numDomainID AND numParntAcntTypeID IN(SELECT numAccountTypeID FROM AccountTypeDetail WHERE numDomainID = v_numDomainID  AND vcAccountCode = '01010101'))) TMEP
      ORDER BY
      "SearchDate_EntryDate";
   end if;
END; $$;



			
/*
-- Added Support for Bills in AP Aging summary,by chintan
*/

-- [dbo].[USP_GetAccountPayableAging] 1,null 6719












