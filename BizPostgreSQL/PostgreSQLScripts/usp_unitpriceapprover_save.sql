-- Stored procedure definition script USP_UnitPriceApprover_Save for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_UnitPriceApprover_Save(v_numDomainID NUMERIC(18,0)
	,v_vcUnitPriceApprovers VARCHAR(1000))
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   DELETE FROM UnitPriceApprover WHERE numDomainID = v_numDomainID;

   INSERT INTO UnitPriceApprover(numDomainID,
			numUserID)
   SELECT
   v_numDomainID,
		 CAST(TabAl.a AS NUMERIC) AS String
   FROM(SELECT
      CAST('<Table><M>' || REPLACE(v_vcUnitPriceApprovers,',','</M><M>') || '</M></Table>' AS XML) AS String) AS vcValue
   CROSS JOIN LATERAL(select a from XMLTABLE('Table/M'
      PASSING String
      COLUMNS a VARCHAR(100) PATH '.')) AS TabAl;
RETURN;
END; $$;


