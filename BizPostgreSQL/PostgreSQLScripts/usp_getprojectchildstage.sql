CREATE OR REPLACE FUNCTION USP_GetProjectChildStage(v_numDomainID NUMERIC(9,0) DEFAULT 0,
v_numStageDetailsId NUMERIC(9,0) DEFAULT 0,
v_strType VARCHAR(10) DEFAULT NULL, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numProjectID  NUMERIC;
   v_numOppID  NUMERIC;
   v_numParentStageID  NUMERIC;
   v_numStagePercentageId  NUMERIC;
BEGIN
   select   numProjectid, numOppid, numParentStageID, numStagePercentageId INTO v_numProjectID,v_numOppID,v_numParentStageID,v_numStagePercentageId from StagePercentageDetails where numStageDetailsId = v_numStageDetailsId and numdomainid = v_numDomainID;

   If  v_strType = 'child' then

      open SWV_RefCur for
      select numStageDetailsId,numStagePercentageId,vcStageName,tintPercentage from StagePercentageDetails
      where numParentStageID = v_numStageDetailsId and numdomainid = v_numDomainID and numProjectid = v_numProjectID and numOppid = v_numOppID;
   ELSEIF v_strType = 'Parent'
   then

      open SWV_RefCur for
      select numStageDetailsId,numStagePercentageId,vcStageName,tintPercentage from StagePercentageDetails
      where numParentStageID = v_numParentStageID and numStagePercentageId = v_numStagePercentageId and numdomainid = v_numDomainID and numProjectid = v_numProjectID and numOppid = v_numOppID;
   end if;


   RETURN;
END; $$;


