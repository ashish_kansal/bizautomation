-- Stored procedure definition script USP_GetAcntType for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetAcntType(v_numAccountId NUMERIC(9,0) DEFAULT 0,    
v_numDomainId NUMERIC(9,0) DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   if v_numAccountId <> 0 then

      open SWV_RefCur for Select cast(coalesce(numAcntTypeId,0) as NUMERIC(18,0)) From Chart_Of_Accounts Where numAccountId = v_numAccountId  And numDomainId = v_numDomainId;
   end if;
END; $$;












