-- Stored procedure definition script USP_ManufacturerList for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
-- =============================================  
-- Modified by: <Author,,Sachin Sadhu>  
-- Create date: <Create Date,,24thDec2013>  
-- Description: <Description,,add 3 fields:vcBizDocImagePath,vcBizDocFooter,vcPurBizDocFooter>  
-- =============================================  
CREATE OR REPLACE FUNCTION USP_ManufacturerList(v_numDomainID     NUMERIC(9,0)  DEFAULT 0,
          v_numRelationshipID   NUMERIC(9,0)  DEFAULT 0,
		  v_numShowOnWebField NUMERIC(18,0) DEFAULT 0,
		  v_numRecordID NUMERIC(18,0) DEFAULT 0, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
BEGIN
   IF(v_numRecordID = 0) then

      open SWV_RefCur for
      SELECT
      C.vcCompanyName,C.txtComments,
		(select 
         VcFileName
         from GenericDocuments
         where numRecID = D.numDivisionID
         and  vcDocumentSection = 'A'   and numDomainId = v_numDomainID    and vcfiletype IN('.jpg','.jpeg','.png') LIMIT 1)  AS VcFileName,
		 D.numDivisionID
      FROM
      CompanyInfo AS C
      LEFT JOIN
      DivisionMaster AS D
      ON
      C.numCompanyId = D.numCompanyID
      WHERE
      C.numCompanyType = v_numRelationshipID AND C.numDomainID = v_numDomainID AND
      coalesce((select CAST(Fld_Value AS NUMERIC) from CFW_FLD_Values where  RecId = D.numDivisionID AND Fld_ID = v_numShowOnWebField LIMIT 1),0) = 1
      ORDER BY C.vcCompanyName;
   ELSE
      open SWV_RefCur for
      SELECT
      C.vcCompanyName,C.txtComments,
		(select 
         VcFileName
         from GenericDocuments
         where numRecID = v_numRecordID
         and  vcDocumentSection = 'A'   and numDomainId = v_numDomainID    and vcfiletype IN('.jpg','.jpeg','.png') LIMIT 1)  AS VcFileName,
		 D.numDivisionID
      FROM
      CompanyInfo AS C
      LEFT JOIN
      DivisionMaster AS D
      ON
      C.numCompanyId = D.numCompanyID
      WHERE
      C.numDomainID = v_numDomainID AND D.numDivisionID = v_numRecordID;
   end if;
   RETURN;
END; $$;


