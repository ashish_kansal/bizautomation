-- Stored procedure definition script usp_SetAuthorizationGroupsForSelectedUser for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION usp_SetAuthorizationGroupsForSelectedUser(v_numUserID NUMERIC(9,0),
	v_numGroupID NUMERIC(9,0) DEFAULT 0   
--
)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   IF v_numGroupID = 0 then	-- Signifies that all groups for the selected user be removed
		
      DELETE FROM UserGroups
      WHERE numUserID = v_numUserID;
   ELSE
      INSERT INTO UserGroups(numUserID, numGroupID)
			VALUES(v_numUserID, v_numGroupID);
   end if;
   RETURN;
END; $$;


