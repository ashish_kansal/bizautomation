-- Stored procedure definition script usp_getSaveCustomReport for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION usp_getSaveCustomReport(v_vcCustomReportConfig TEXT,                                
 v_numUserCntID NUMERIC)
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_ReportType  NUMERIC;                                         
 --For Org and Contacts                                           
   v_RelationShip  NUMERIC;                                            
   v_CRMType  NUMERIC;                                              
   v_IncludeContacts  BOOLEAN;                                         
   v_ContactType  NUMERIC;                                        
   v_CustomFields  BOOLEAN;                        
 --For Opportunity and Items                                        
   v_OppType  NUMERIC;                          
   v_ReportName  VARCHAR(100);                              
   v_ReportDesc  VARCHAR(100);                              
   v_ReportTypeConfig  VARCHAR(1000);                             
                                   
                                            
   v_vcReportLayout  CHAR(3);                                            
   v_bitStandardFilters  BOOLEAN;                                            
   v_vcStandardFilterCriteria  VARCHAR(50);                                            
   v_vcStandardFilterStartDate  VARCHAR(10);                                            
   v_vcStandardFilterEndDate  VARCHAR(10);                                            
   v_vcTeams  VARCHAR(250);                                            
   v_vcTerritories  VARCHAR(250);                                            
   v_vcFilterOn  INTEGER;                                            
   v_vcAdvFilterJoining  CHAR(1);                                            
   v_bitRecordCountSummation  BOOLEAN;                                            
                                            
   v_vcReportColumnNames  VARCHAR(4000);                                  
   v_vcAggregateReportColumns  VARCHAR(1000);                              
   v_vcReportAdvFilterWhereClause  VARCHAR(1000);                                            
   v_vcReportStdFilterWhereClause  VARCHAR(1000);                                             
   v_vcReportDataTypeWhereClause  VARCHAR(500);                                            
   v_vcReportWhereClause  VARCHAR(2000);            
        
   v_numDefaultOwner  NUMERIC;                                      
   v_vcViewName  VARCHAR(70);                                          
   v_vcCustomReportQuery  VARCHAR(4000);                       
   v_vcCustomReportQueryMiddle  VARCHAR(4000);                                 
   v_vcCustomReportQueryTrailer  VARCHAR(4000);                                  
          
   v_numCustReportID  NUMERIC;
BEGIN
   select   RD.numEmpId INTO v_numDefaultOwner FROM RoutingLeadDetails RD INNER JOIN RoutingLeads RL ON RD.numRoutID = RL.numRoutID WHERE RL.bitDefault = true;                                          
                                            
	select 
		ReportTypeConfig 
	INTO 
		v_ReportTypeConfig 
	FROM 
		XMLTABLE
		(
			'CustRptConfig/ReportTypeConfig'
			PASSING 
				CAST(v_vcCustomReportConfig AS XML)
			COLUMNS
				id FOR ORDINALITY,
				ReportTypeConfig TEXT PATH 'ReportTypeConfig'
		) AS X;                       
                             
   RAISE NOTICE '%','ReportTypeConfig: ' || coalesce(v_ReportTypeConfig,'');            
          
	SELECT 
		ReportType
		,ReportName
		,ReportDesc 
	INTO 
		v_ReportType
		,v_ReportName
		,v_ReportDesc 
	FROM 
		XMLTABLE
		(
			'CustRptConfig/ReportTypeConfig'
			PASSING 
				CAST(v_vcCustomReportConfig AS XML)
			COLUMNS
				id FOR ORDINALITY
				,ReportType NUMERIC PATH 'ReportType'
				,ReportName VARCHAR(100) PATH 'ReportName'
				,ReportDesc VARCHAR(100) PATH 'ReportDesc'
		) AS X;    
                            
   RAISE NOTICE 'ReportType: %',SUBSTR(CAST(v_ReportType AS VARCHAR(30)),1,30);                              
   RAISE NOTICE 'ReportName: %',SUBSTR(CAST(v_ReportName AS VARCHAR(30)),1,30);                              
   RAISE NOTICE 'ReportDesc: %',SUBSTR(CAST(v_ReportDesc AS VARCHAR(30)),1,30);                  
          
   v_vcViewName := CASE v_ReportType
   WHEN '1' THEN '##OrgContactCustomField'
   WHEN '2' THEN '##OppItemCustomField'
   WHEN '3' THEN 'vw_LeadsCustomReport'
   END;                                          
                                        
   IF v_ReportType = 1 then --Organization and Contacts                              
 
		select 
			RelationShip
			,CRMType
			,IncludeContacts
			,ContactType
			,CustomFields 
		INTO 
			v_RelationShip,v_CRMType,v_IncludeContacts,v_ContactType,v_CustomFields 
		FROM 
			XMLTABLE
			(
				'CustRptConfig/ReportTypeConfig'
				PASSING 
					CAST(v_vcCustomReportConfig AS XML)
				COLUMNS
					id FOR ORDINALITY
					,RelationShip NUMERIC PATH 'RelationShip'
					,CRMType NUMERIC PATH 'CRMType'
					,IncludeContacts BOOLEAN PATH 'IncludeContacts'
					,ContactType NUMERIC PATH 'ContactType'
					,CustomFields BOOLEAN PATH 'CustomFields'
			) AS X;    
   ELSEIF v_ReportType = 2
   then --Opportunity and Items                                        
 
		select   
			OppType
			,CustomFields 
		INTO 
			v_OppType
			,v_CustomFields 
		FROM 
			XMLTABLE
			(
				'CustRptConfig/ReportTypeConfig'
				PASSING 
					CAST(v_vcCustomReportConfig AS XML)
				COLUMNS
					id FOR ORDINALITY
					,OppType NUMERIC PATH 'OppType'
					,CustomFields BOOLEAN PATH 'CustomFields'
			) AS X;  
   end if;                                       
   RAISE NOTICE 'RelationShip: %',SUBSTR(Cast(v_RelationShip As VARCHAR(30)),1,30);                                        
   RAISE NOTICE 'CRMType: %',SUBSTR(Cast(v_CRMType As VARCHAR(30)),1,30);                                        
   RAISE NOTICE 'IncludeContacts: %',SUBSTR(Cast(v_IncludeContacts As VARCHAR(30)),1,30);                                        
   RAISE NOTICE 'ContactType: %',SUBSTR(Cast(v_ContactType As VARCHAR(30)),1,30);                                           
   RAISE NOTICE 'CustomFields: %',SUBSTR(Cast(v_CustomFields As VARCHAR(30)),1,30);                                             
                                        
   RAISE NOTICE 'OppType: %',SUBSTR(Cast(v_OppType As VARCHAR(30)),1,30);                                        
          
   v_vcReportDataTypeWhereClause := CASE v_ReportType
   WHEN 1 THEN
      ' AND tintCRMType = ' || SUBSTR(Cast(v_CRMType As VARCHAR(30)),1,30) ||
      CASE WHEN v_RelationShip > 0 THEN
         ' AND numOrgCompanyType = ' || SUBSTR(Cast(v_RelationShip As VARCHAR(30)),1,30)
      ELSE
         ''
      END
      ||
      CASE WHEN v_IncludeContacts = true THEN
         CASE WHEN v_ContactType > 0 THEN
            ' AND numContContactType = ' || SUBSTR(Cast(v_ContactType As VARCHAR(30)),1,30)
         ELSE
            ''
         END
      ELSE
         ''
      END
   WHEN 2 THEN
      ' AND tintOppType = ' || SUBSTR(Cast(v_OppType As VARCHAR(30)),1,30)
   ELSE
      ''
   END;                                        
                                        
   RAISE NOTICE '%','vcReportDataTypeWhereClause: ' || coalesce(v_vcReportDataTypeWhereClause,'');                                         
          
   select   vcReportLayout, bitStandardFilters, vcStandardFilterCriteria, vcStandardFilterStartDate, vcStandardFilterEndDate, vcTeams, vcTerritories, vcFilterOn, vcAdvFilterJoining, bitRecordCountSummation INTO v_vcReportLayout,v_bitStandardFilters,v_vcStandardFilterCriteria,v_vcStandardFilterStartDate,
   v_vcStandardFilterEndDate,v_vcTeams,v_vcTerritories,
   v_vcFilterOn,v_vcAdvFilterJoining,v_bitRecordCountSummation 
   FROM 
	XMLTABLE
	(
		'CustRptConfig/ReportTypeConfig'
		PASSING 
			CAST(v_vcCustomReportConfig AS XML)
		COLUMNS
			id FOR ORDINALITY
			,vcReportLayout CHAR(3) PATH 'vcReportLayout'
			,bitStandardFilters BOOLEAN PATH 'bitStandardFilters'
			,vcStandardFilterCriteria VARCHAR(50) PATH 'vcStandardFilterCriteria'
			,vcStandardFilterStartDate VARCHAR(10) PATH 'vcStandardFilterStartDate'
			,vcStandardFilterEndDate VARCHAR(10) PATH 'vcStandardFilterEndDate'
			,vcTeams VARCHAR(250) PATH 'vcTeams'
			,vcTerritories VARCHAR(250) PATH 'vcTerritories'
			,vcFilterOn INTEGER PATH 'vcFilterOn'
			,vcAdvFilterJoining CHAR(1) PATH 'vcAdvFilterJoining'
			,bitRecordCountSummation BOOLEAN PATH 'bitRecordCountSummation'
	) AS X;                                             
                                          
   RAISE NOTICE 'vcReportLayout: %',SUBSTR(CAST(v_vcReportLayout AS VARCHAR(30)),1,30);                                            
   RAISE NOTICE 'bitStandardFilters: %',SUBSTR(CAST(v_bitStandardFilters AS VARCHAR(30)),1,30);                                            
   RAISE NOTICE 'vcStandardFilterCriteria: %',SUBSTR(CAST(v_vcStandardFilterCriteria AS VARCHAR(30)),1,30);                                            
   RAISE NOTICE 'vcStandardFilterStartDate: %',SUBSTR(CAST(v_vcStandardFilterStartDate AS VARCHAR(30)),1,30);                                            
   RAISE NOTICE 'vcStandardFilterEndDate: %',SUBSTR(CAST(v_vcStandardFilterEndDate AS VARCHAR(30)),1,30);                                            
   RAISE NOTICE 'vcTeams: %',SUBSTR(CAST(v_vcTeams AS VARCHAR(30)),1,30);                                            
   RAISE NOTICE 'vcTerritories: %',SUBSTR(CAST(v_vcTerritories AS VARCHAR(30)),1,30);                                            
   RAISE NOTICE 'vcFilterOn: %',SUBSTR(CAST(v_vcFilterOn AS VARCHAR(30)),1,30);                                            
   RAISE NOTICE 'vcAdvFilterJoining: %',SUBSTR(CAST(v_vcAdvFilterJoining AS VARCHAR(30)),1,30);                                            
   RAISE NOTICE 'bitRecordCountSummation: %',SUBSTR(CAST(v_bitRecordCountSummation AS VARCHAR(30)),1,30);                                           
          
   v_vcReportStdFilterWhereClause := CASE  v_vcFilterOn
   WHEN '0' THEN
      CASE WHEN v_ReportType = 2 THEN    --Opportunities      
         ' numOppRecOwner = ' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(30)),1,30) || ' '
      ELSE    --Organizations and Contacts      
         ' (numContRecOwner = ' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(30)),1,30) || ' OR numDivRecOwner = ' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(30)),1,30) || ') '
      END
   WHEN '1' THEN
      CASE WHEN v_ReportType = 2 THEN    --Opportunities      
         ' numOppRecOwner = ' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(30)),1,30) ||
         ' AND numOppCreatedBy IN (SELECT DISTINCT numUserId FROM UserTeams WHERE numTeam IN (0,' || coalesce(v_vcTeams,'') ||  ')) AND ((bitPublicFlag = false) OR (bitPublicFlag=true and numOppRecOwner = ' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(30)),1,30) || ')) '
      ELSE    --Organizations and Contacts      
         ' (numContRecOwner = ' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(30)),1,30) || ' OR numDivRecOwner = ' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(30)),1,30) || ') ' ||
         ' AND numContCreatedBy IN (SELECT DISTINCT numUserId FROM UserTeams WHERE numTeam IN (0,' || coalesce(v_vcTeams,'') ||  ')) AND ((bitPublicFlag = false) OR (bitPublicFlag=true and numDivRecOwner = ' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(30)),1,30) || ')) '
      END
   ELSE
      CASE WHEN v_ReportType = 2 THEN  --Opportunities      
         ' numTerritoryId IN (0,' || coalesce(v_vcTerritories,'') || ')   AND ((bitPublicFlag = false) OR (bitPublicFlag = true and numOppRecOwner = ' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(30)),1,30) || ')) '
      ELSE   --Organizations and Contacts      
         ' numTerritoryId IN (0,' || coalesce(v_vcTerritories,'') || ')   AND ((bitPublicFlag = false) OR (bitPublicFlag = true and numDivRecOwner = ' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(30)),1,30) || ')) '
      END
   END
   ||
   CASE v_bitStandardFilters
   WHEN '1' THEN ' AND ' || CASE v_vcStandardFilterCriteria
      WHEN 'All' THEN
         CASE    WHEN v_vcStandardFilterStartDate <> '' AND v_vcStandardFilterEndDate <> ''  THEN
            ' dtRecordCreatedOn >= ' || '''' || coalesce(v_vcStandardFilterStartDate,'') || '''' || ' AND dtRecordCreatedOn <= ' || '''' || coalesce(v_vcStandardFilterEndDate,'') || '''' || ' OR dtRecordLastModifiedOn >= ' || '''' || coalesce(v_vcStandardFilterStartDate,'') || '''' || ' AND dtRecordLastModif
iedOn <= ' || '''' || coalesce(v_vcStandardFilterEndDate,'') || ''''
         WHEN v_vcStandardFilterEndDate <> '' THEN
            ' dtRecordCreatedOn <= ' || '''' || coalesce(v_vcStandardFilterEndDate,'') || '''' || ' AND dtRecordLastModifiedOn <= ' || '''' || coalesce(v_vcStandardFilterEndDate,'') || ''''
         ELSE
            ' dtRecordCreatedOn >= ' || '''' || coalesce(v_vcStandardFilterStartDate,'') || '''' || ' AND dtRecordLastModifiedOn >= ' || '''' || coalesce(v_vcStandardFilterStartDate,'') || ''''
         END
      ELSE
         CASE    WHEN v_vcStandardFilterStartDate <> '' AND v_vcStandardFilterEndDate <> ''  THEN
            coalesce(v_vcStandardFilterCriteria,'') || ' >= ' || '''' || coalesce(v_vcStandardFilterStartDate,'') || '''' || ' AND ' || coalesce(v_vcStandardFilterCriteria,'') || ' <= ' || '''' || coalesce(v_vcStandardFilterEndDate,'') || ''''
         WHEN v_vcStandardFilterEndDate <> '' THEN
            coalesce(v_vcStandardFilterCriteria,'') || ' <= ' || '''' || coalesce(v_vcStandardFilterEndDate,'') || ''''
         ELSE
            coalesce(v_vcStandardFilterCriteria,'') || ' >= ' || '''' || coalesce(v_vcStandardFilterStartDate,'') || ''''
         END
      END
   ELSE ''
   END;                                           
                                          
   RAISE NOTICE '%','vcReportStdFilterWhereClause: ' || coalesce(v_vcReportStdFilterWhereClause,'');                                          
          
	select 
		COALESCE(coalesce(v_vcReportColumnNames,'') || ', ','') || vcDbFieldName || ' AS [' || vcScrFieldName || ']' 
	INTO 
		v_vcReportColumnNames 
	FROM 
	XMLTABLE
	(
		'CustRptConfig/ColAndAdvFilter/vcDbRow'
		PASSING 
			CAST(v_vcCustomReportConfig AS XML)
		COLUMNS
			id FOR ORDINALITY
			,vcDbFieldName VARCHAR(50) PATH 'vcDbFieldName'
			,vcScrFieldName VARCHAR(50) PATH 'vcScrFieldName'
			,numOrderOfDisplay INTEGER PATH 'numOrderOfDisplay'
	) AS X 
	ORDER BY numOrderOfDisplay Asc LIMIT 100;                                            
                                                   
                               
   select   COALESCE(coalesce(v_vcAggregateReportColumns,'') || '|','') ||
   SUBSTR(CAST(bitSumAggregation As CHAR(1)),1,1) || ',' || SUBSTR(CAST(bitAvgAggregation As CHAR(1)),1,1) || ',' || SUBSTR(CAST(bitLValueAggregation As CHAR(1)),1,1) || ',' || SUBSTR(CAST(bitSValueAggregation As CHAR(1)),1,1) INTO v_vcAggregateReportColumns 
   FROM 
   XMLTABLE
	(
		'CustRptConfig/ColAndAdvFilter/vcDbRow'
		PASSING 
			CAST(v_vcCustomReportConfig AS XML)
		COLUMNS
			id FOR ORDINALITY
			,vcDbFieldName VARCHAR(50) PATH 'vcDbFieldName'
			,vcScrFieldName VARCHAR(50) PATH 'vcScrFieldName'
			,bitSumAggregation BOOLEAN PATH 'bitSumAggregation'
			,bitAvgAggregation BOOLEAN PATH 'bitAvgAggregation'
			,bitLValueAggregation BOOLEAN PATH 'bitLValueAggregation'
			,bitSValueAggregation BOOLEAN PATH 'bitSValueAggregation'
			,numOrderOfDisplay INTEGER PATH 'numOrderOfDisplay'
	) AS X 
   ORDER BY numOrderOfDisplay Asc LIMIT 100;                                 
                                           
   RAISE NOTICE '%','@vcAggregateReportColumns: ' || coalesce(v_vcAggregateReportColumns,'');                              
          
   select   COALESCE(coalesce(v_vcReportAdvFilterWhereClause,'') || CASE WHEN v_vcAdvFilterJoining = 'A' THEN ' AND ' ELSE ' OR ' END,'') ||
   vcDbFieldName  ||
   CASE vcAdvFilterOperator
   WHEN 'EQ' THEN ' = ' || '''' || vcAdvFilterValue || ''''
   WHEN 'LT' THEN ' < ' || '''' || vcAdvFilterValue || ''''
   WHEN 'GT' THEN ' > ' || '''' || vcAdvFilterValue || ''''
   WHEN 'LEQ' THEN ' <= ' || '''' || vcAdvFilterValue || ''''
   WHEN 'GEQ' THEN ' >= ' || '''' || vcAdvFilterValue || ''''
   WHEN 'NEQ' THEN ' <> ' || '''' || vcAdvFilterValue || ''''
   WHEN 'STW' THEN ' LIKE ' || '''%' || vcAdvFilterValue || ''''
   WHEN 'LIKE' THEN ' LIKE ' || '''%' || vcAdvFilterValue || '%'''
   WHEN 'NOT LIKE' THEN ' NOT LIKE ' || '''%' || vcAdvFilterValue || '%'''
   END INTO v_vcReportAdvFilterWhereClause 
   FROM 
    XMLTABLE
	(
		'CustRptConfig/ColAndAdvFilter/vcDbRow'
		PASSING 
			CAST(v_vcCustomReportConfig AS XML)
		COLUMNS
			id FOR ORDINALITY
			,vcDbFieldName VARCHAR(50) PATH 'vcDbFieldName'
			,vcAdvFilterOperator  VARCHAR(10) PATH 'vcAdvFilterOperator'
			,vcAdvFilterValue VARCHAR(50) PATH 'vcAdvFilterValue'
	) AS X 
   WHERE vcAdvFilterOperator <> '0';                            
                                      
   RAISE NOTICE '%','vcReportColumnNames: ' || coalesce(v_vcReportColumnNames,'');              
   v_vcReportAdvFilterWhereClause := '(' || coalesce(v_vcReportAdvFilterWhereClause,'') ||  ')';                                           
   RAISE NOTICE '%','vcReportAdvFilterWhereClause: ' || coalesce(v_vcReportAdvFilterWhereClause,'');                                          
                                          
   v_vcReportWhereClause := ' WHERE ' || coalesce(v_vcReportStdFilterWhereClause,'') || CASE WHEN v_vcReportAdvFilterWhereClause <> '' THEN ' AND ' || coalesce(v_vcReportAdvFilterWhereClause,'') ELSE '' END || CASE WHEN v_vcReportDataTypeWhereClause <> '' THEN v_vcReportDataTypeWhereClause ELSE '' END;                                        
                        
   RAISE NOTICE '%','vcReportWhereClause: ' || coalesce(v_vcReportWhereClause,'');                                   
                                           
   v_vcCustomReportQuery := SUBSTR('SELECT ' || coalesce(v_vcReportColumnNames,'') || ' FROM ' || coalesce(v_vcViewName,'') || coalesce(v_vcReportWhereClause,''),1,4000);                                  
   v_vcCustomReportQueryMiddle := ' '  || SUBSTR('SELECT ' || coalesce(v_vcReportColumnNames,'') || ' FROM ' || coalesce(v_vcViewName,'') || coalesce(v_vcReportWhereClause,''),4001, 
   4000);                                  
   v_vcCustomReportQueryTrailer := ' '  || SUBSTR('SELECT ' || coalesce(v_vcReportColumnNames,'') || ' FROM ' || coalesce(v_vcViewName,'') || coalesce(v_vcReportWhereClause,''),8001, 
   4000);                                  
                              
   RAISE NOTICE '%','vcCustomReportQuery: ' || coalesce(v_vcCustomReportQuery,'') || coalesce(v_vcCustomReportQueryMiddle,'') || coalesce(v_vcCustomReportQueryTrailer,'');                          
                   
 -----------------------------------------------------------------------------                            
 ----------------------START SAVING THE REPORT CONFIG-------------------------                            
 -----------------------------------------------------------------------------                            
   INSERT INTO CustRptConfigMaster(vcReportName, vcReportDescription, numCreatedBy, numReportType, vcReportTypeconfig, vcReportQuickQuery, vcReportSummationOrder, numCreatedDate)
   VALUES(v_ReportName, v_ReportDesc, v_numUserCntID, v_ReportType, v_ReportTypeConfig, '-', v_vcAggregateReportColumns, TIMEZONE('UTC',now()));                            
          
 
   v_numCustReportID := CURRVAL('CustRptConfigMaster_seq');             
   UPDATE CustRptConfigMaster Set vcReportQuickQuery = coalesce(v_vcCustomReportQuery,'') || coalesce(v_vcCustomReportQueryMiddle,'') || coalesce(v_vcCustomReportQueryTrailer,'')
   WHERE numCustReportID = v_numCustReportID;          
                         
   INSERT INTO CustRptConfigLayoutAndStdFilters(numCustReportID, vcReportLayout, bitStandardFilters, vcStandardFilterCriteria, vcStandardFilterStartDate, vcStandardFilterEndDate,
      vcTeams, vcTerritories, vcFilterOn, vcAdvFilterJoining, bitRecordCountSummation)
    VALUES(v_numCustReportID, v_vcReportLayout, v_bitStandardFilters, v_vcStandardFilterCriteria, v_vcStandardFilterStartDate, v_vcStandardFilterEndDate,
      v_vcTeams, v_vcTerritories, v_vcFilterOn, v_vcAdvFilterJoining, v_bitRecordCountSummation);                            
                            
 
   INSERT INTO CustRptConfigColumnsAndAdvFilters(numCustReportID, vcDbFieldName, numFieldGroupID, numOrderOfSelection, numOrderOfDisplay, bitSumAggregation,
      bitAvgAggregation, bitLValueAggregation, bitSValueAggregation, vcAdvFilterOperator, vcAdvFilterValue)
   SELECT v_numCustReportID, vcDbFieldName, numFieldGroupID, numOrderOfSelection, numOrderOfDisplay, bitSumAggregation,
      bitAvgAggregation, bitLValueAggregation, bitSValueAggregation, vcAdvFilterOperator, vcAdvFilterValue
   FROM
   XMLTABLE
	(
		'CustRptConfig/ColAndAdvFilter/vcDbRow'
		PASSING 
			CAST(v_vcCustomReportConfig AS XML)
		COLUMNS
			id FOR ORDINALITY
			,vcDbFieldName VARCHAR(50) PATH 'vcDbFieldName'
			,numFieldGroupID NUMERIC PATH 'numFieldGroupID'
			,numOrderOfSelection NUMERIC PATH 'numOrderOfSelection'
			,numOrderOfDisplay NUMERIC PATH 'numOrderOfDisplay'
			,bitSumAggregation BOOLEAN PATH 'bitSumAggregation'
			,bitAvgAggregation BOOLEAN PATH 'bitAvgAggregation'
			,bitLValueAggregation BOOLEAN PATH 'bitLValueAggregation'
			,bitSValueAggregation BOOLEAN PATH 'bitSValueAggregation'
			,vcAdvFilterOperator VARCHAR(10) PATH 'vcAdvFilterOperator'
			,vcAdvFilterValue VARCHAR(50) PATH 'vcAdvFilterValue'
	) AS X;
                           
   RETURN;
END; $$;


