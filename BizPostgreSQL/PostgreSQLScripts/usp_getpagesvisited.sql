-- Stored procedure definition script USP_GetPagesVisited for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetPagesVisited(v_numTrackingID NUMERIC(9,0) DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for select   numTracVisitorsHDRID,
vcPageName,
vcElapsedTime,
dtTime as TimeVisited
   from TrackingVisitorsDTL
   where numTracVisitorsHDRID = v_numTrackingID
   ORDER BY numTracVisitorsDTLID;
END; $$; 












