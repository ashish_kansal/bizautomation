-- Stored procedure definition script USP_GetCartItemPromotionDetail for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_GetCartItemPromotionDetail(v_numDomainID NUMERIC(18,0),
	v_numItemCode NUMERIC(18,0),
	v_numUOMID NUMERIC(18,0),
	v_numShippingCountry NUMERIC(18,0),
	v_cookieId TEXT DEFAULT NULL,
	v_numUserCntId NUMERIC DEFAULT 0,
	v_vcSendCoupon VARCHAR(200) DEFAULT 0, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_vPromotionDesc  TEXT;
   v_vcItemName  VARCHAR(500);
   v_chrItemType  CHAR(1);
   v_numItemGroup  NUMERIC(18,0);
   v_ItemDesc  VARCHAR(1000);
   v_numItemClassification  NUMERIC(18,0);

   v_numWarehouseItemID  NUMERIC(18,0);

	
   v_decmPrice  DECIMAL(18,2);
   v_numShippingPromotionID  NUMERIC(18,0) DEFAULT 0;
   v_numPromotionID  NUMERIC(18,0) DEFAULT 0;

   v_bitRequireCouponCode  VARCHAR(200);

   v_tintOfferTriggerValueType  INTEGER;
   v_fltOfferTriggerValue  DECIMAL;
		
   v_vcShippingDescription  TEXT;
   v_vcQualifiedShipping  TEXT;
   v_bitCoupon  BOOLEAN;
   v_vcCouponCode  VARCHAR(200);
BEGIN
   IF(v_vcSendCoupon = '0') then
      select coalesce(SUM(monTotAmount),0) INTO v_decmPrice from CartItems where numDomainId = v_numDomainID AND vcCookieId = v_cookieId AND numUserCntId = v_numUserCntId;
      SELECT  PromotionID INTO v_numShippingPromotionID from CartItems AS C
      LEFT JOIN PromotionOffer AS P ON C.PromotionID = P.numProId
      LEFT JOIN ShippingPromotions SP ON C.numDomainId = SP.numDomainID where C.numDomainId = v_numDomainID AND C.vcCookieId = v_cookieId AND C.numUserCntId = v_numUserCntId
      AND (coalesce(SP.bitFixShipping1,false) = true OR coalesce(SP.bitFixShipping2,false) = true OR coalesce(SP.bitFreeShiping,false) = true)  AND coalesce(C.bitParentPromotion,false) = true
      AND 1 =(CASE WHEN P.tintOfferTriggerValueType = 1 AND C.numUnitHour >= fltOfferTriggerValue THEN 1 WHEN P.tintOfferTriggerValueType = 2 AND monTotAmount >= fltOfferTriggerValue THEN 1 ELSE 0 END)     LIMIT 1;
      RAISE NOTICE '%',(v_decmPrice);
      IF(coalesce(v_numPromotionID,0) = 0) then
	
	-- GET ITEM DETAILS
         select   vcItemName, Item.txtItemDesc, charItemType, numItemGroup, numItemClassification, (CASE WHEN coalesce(v_numWarehouseItemID,0) > 0 THEN v_numWarehouseItemID ELSE WareHouseItems.numWareHouseItemID END) INTO v_vcItemName,v_ItemDesc,v_chrItemType,v_numItemGroup,v_numItemClassification,
         v_numWarehouseItemID FROM
         Item
         LEFT JOIN LATERAL(SELECT  numWareHouseItemID,monWListPrice FROM WareHouseItems WHERE numItemID = v_numItemCode AND (numWareHouseItemID = v_numWarehouseItemID OR coalesce(v_numWarehouseItemID,0) = 0) LIMIT 1) AS WareHouseItems on TRUE WHERE
         Item.numItemCode = v_numItemCode;


	--IF @chrItemType = 'P' AND ISNULL(@numWarehouseItemID,0) = 0
	--BEGIN
	--	RAISERROR('WAREHOUSE_DOES_NOT_EXISTS',16,1)
	--END

	

	-- CHECK IF PROMOTION CAN BE TRIGGERED FROM THIS ITEM
	
         select   numProId INTO v_numPromotionID FROM
         PromotionOffer PO WHERE
         numDomainId = v_numDomainID
         AND coalesce(bitEnabled,false) = true
         AND coalesce(bitAppliesToSite,false) = true
         AND 1 =(CASE WHEN bitRequireCouponCode = true THEN CASE WHEN coalesce(intCouponCodeUsed,0) < coalesce(tintUsageLimit,0) OR coalesce(tintUsageLimit,0) = 0  THEN 1 ELSE 0 END ELSE 1 END)
         AND 1 =(CASE WHEN bitNeverExpires = true THEN 1 ELSE(CASE WHEN CAST(LOCALTIMESTAMP  AS DATE) >= dtValidFrom AND CAST(LOCALTIMESTAMP  AS DATE) <= dtValidTo THEN 1 ELSE 0 END) END)
         AND (1 =(CASE
         WHEN tintOfferBasedOn = 1 THEN(CASE WHEN(SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId = PO.numProId AND tintRecordType = 5 AND tintType = 1 AND numValue = v_numItemCode) > 0 THEN 1 ELSE 0 END)
         WHEN tintOfferBasedOn = 2 THEN(CASE WHEN(SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId = PO.numProId AND tintRecordType = 5 AND tintType = 2 AND numValue = v_numItemClassification) > 0 THEN 1 ELSE 0 END)
         ELSE 0
         END)
         OR
         1 =(CASE
         WHEN tintDiscoutBaseOn = 1 THEN(CASE WHEN(SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId = PO.numProId AND tintRecordType = 6 AND tintType = 1 AND numValue = v_numItemCode) > 0 THEN 1 ELSE 0 END)
         WHEN tintDiscoutBaseOn = 2 THEN(CASE WHEN(SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId = PO.numProId AND tintRecordType = 6 AND tintType = 2 AND numValue = v_numItemClassification) > 0 THEN 1 ELSE 0 END)
         WHEN tintDiscoutBaseOn = 3 AND tintOfferBasedOn = 1 THEN(CASE WHEN(SELECT COUNT(SimilarItems.numItemCode) FROM SimilarItems WHERE numParentItemCode IN(SELECT numValue FROM PromotionOfferItems WHERE numProId = PO.numProId AND tintRecordType = 5 AND tintType = 1 AND SimilarItems.numItemCode = v_numItemCode)) > 0 THEN 1 ELSE 0 END)
         WHEN tintDiscoutBaseOn = 3 AND tintOfferBasedOn = 2 THEN(CASE WHEN(SELECT COUNT(SimilarItems.numItemCode) FROM SimilarItems WHERE SimilarItems.numItemCode IN(v_numItemCode) AND numParentItemCode IN(SELECT numItemCode FROM Item WHERE numItemClassification IN(SELECT numValue FROM PromotionOfferItems WHERE numProId = PO.numProId AND tintRecordType = 5 AND tintType = 2))) > 0 THEN 1 ELSE 0 END)
         ELSE 0
         END))   ORDER BY
         CASE
         WHEN PO.txtCouponCode IN(SELECT vcCoupon FROM CartItems WHERE numUserCntId = v_numUserCntId AND vcCookieId = v_cookieId) THEN 1
         WHEN PO.tintOfferBasedOn = 1 AND PO.tintDiscoutBaseOn = 1 THEN 2
         WHEN PO.tintOfferBasedOn = 2 AND PO.tintDiscoutBaseOn = 1 THEN 3
         WHEN PO.tintOfferBasedOn = 1 AND PO.tintDiscoutBaseOn = 2 THEN 4
         WHEN PO.tintOfferBasedOn = 2 AND PO.tintDiscoutBaseOn = 2 THEN 5
         WHEN PO.tintOfferBasedOn = 1 AND PO.tintDiscoutBaseOn = 3 THEN 6
         WHEN PO.tintOfferBasedOn = 2 AND PO.tintDiscoutBaseOn = 3 THEN 7
         END LIMIT 1;
      end if;

	
	--IF ISNULL(@numPromotionID,0) > 0
	--BEGIN
		-- Get Top 1 Promotion Based on priority
		

      v_vcCouponCode := OVERLAY((SELECT
      ',' || txtCouponCode
      FROM
      PromotionOffer PO
      WHERE
      numDomainId = v_numDomainID
      AND coalesce(bitRequireCouponCode,false) = true
      AND coalesce(bitEnabled,false) = true
      AND coalesce(bitAppliesToSite,false) = true
      AND 1 =(CASE WHEN bitRequireCouponCode = true THEN CASE WHEN coalesce(intCouponCodeUsed,0) < coalesce(tintUsageLimit,0) OR coalesce(tintUsageLimit,0) = 0  THEN 1 ELSE 0 END ELSE 1 END)
      AND 1 =(CASE WHEN bitNeverExpires = true THEN 1 ELSE(CASE WHEN CAST(LOCALTIMESTAMP  AS DATE) >= dtValidFrom AND CAST(LOCALTIMESTAMP  AS DATE) <= dtValidTo THEN 1 ELSE 0 END) END)
      AND 1 =(CASE
      WHEN tintOfferBasedOn = 1 THEN(CASE WHEN(SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId = PO.numProId AND tintRecordType = 5 AND tintType = 1 AND numValue = v_numItemCode) > 0 THEN 1 ELSE 0 END)
      WHEN tintOfferBasedOn = 2 THEN(CASE WHEN(SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId = PO.numProId AND tintRecordType = 5 AND tintType = 2 AND numValue = v_numItemClassification) > 0 THEN 1 ELSE 0 END)
      ELSE 0
      END)
				
      AND(SELECT COUNT(numCartId) FROM CartItems WHERE vcCoupon = PO.txtCouponCode AND numDomainId = v_numDomainID AND numUserCntId = v_numUserCntId AND vcCookieId = v_cookieId) = 0
      ORDER BY
      CASE
      WHEN PO.tintOfferBasedOn = 1 AND PO.tintDiscoutBaseOn = 1 THEN 1
      WHEN PO.tintOfferBasedOn = 2 AND PO.tintDiscoutBaseOn = 1 THEN 2
      WHEN PO.tintOfferBasedOn = 1 AND PO.tintDiscoutBaseOn = 2 THEN 3
      WHEN PO.tintOfferBasedOn = 2 AND PO.tintDiscoutBaseOn = 2 THEN 4
      WHEN PO.tintOfferBasedOn = 1 AND PO.tintDiscoutBaseOn = 3 THEN 5
      WHEN PO.tintOfferBasedOn = 2 AND PO.tintDiscoutBaseOn = 3 THEN 6
      END) placing '' from 1 for 0);
      v_vcCouponCode := SUBSTR(v_vcCouponCode,2,(LENGTH(v_vcCouponCode)));
      IF(LENGTH(v_vcCouponCode) > 0) then
		
         v_bitRequireCouponCode := CAST(1 AS VARCHAR(200));
      end if;
      select(CASE WHEN PO.bitRequireCouponCode = false OR
			(PO.bitRequireCouponCode = true AND(SELECT COUNT(*) FROM CartItems WHERE PromotionID = PO.numProId AND bitParentPromotion = true AND vcCookieId = v_cookieId AND numUserCntId = v_numUserCntId) > 0)
      THEN CONCAT('Buy ',CASE WHEN tintOfferTriggerValueType = 1 THEN SUBSTR(CAST(fltOfferTriggerValue AS VARCHAR(30)),1,30) ELSE CONCAT('$',fltOfferTriggerValue) END,CASE tintOfferBasedOn
         WHEN 1 THEN CONCAT(' of "',(SELECT OVERLAY((SELECT CONCAT(', ',Item.vcItemName) FROM PromotionOfferItems INNER JOIN Item ON PromotionOfferItems.numValue = Item.numItemCode WHERE numProId = PO.numProId AND tintRecordType = 5 AND tintType = 1) placing '' from 1 for 2)),'"')
         WHEN 2 THEN CONCAT(' of items belonging to classification(s) "',(SELECT OVERLAY((SELECT CONCAT(', ',Listdetails.vcData) FROM PromotionOfferItems INNER JOIN Listdetails ON PromotionOfferItems.numValue = Listdetails.numListItemID WHERE numListID = 36 AND numProId = PO.numProId AND tintRecordType = 5 AND tintType = 2) placing '' from 1 for 2)), 
            '"')
         END,' & get ',CASE tintDiscountType WHEN 1 THEN CONCAT(fltDiscountValue,'% off on') WHEN 2 THEN CONCAT('$',fltDiscountValue,' off on') WHEN 3 THEN CONCAT(fltDiscountValue,' of') END, 
         CASE
         WHEN tintDiscoutBaseOn = 1 THEN
            CONCAT(CONCAT(' "',(SELECT OVERLAY((SELECT CONCAT(',',Item.vcItemName) FROM PromotionOfferItems INNER JOIN Item ON PromotionOfferItems.numValue = Item.numItemCode WHERE numProId = PO.numProId AND tintRecordType = 6 AND tintType = 1) placing '' from 1 for 1)),'"'),(CASE WHEN PO.tintDiscountType = 3 THEN ' free' ELSE '' END),
            '.')
         WHEN tintDiscoutBaseOn = 2 THEN
            CONCAT(CONCAT(' items belonging to classification(s) "',(SELECT OVERLAY((SELECT CONCAT(',',Listdetails.vcData) FROM PromotionOfferItems INNER JOIN Listdetails ON PromotionOfferItems.numValue = Listdetails.numListItemID WHERE numListID = 36 AND numProId = PO.numProId AND tintRecordType = 6 AND tintType = 2) placing '' from 1 for 1)), 
            '"'),(CASE WHEN PO.tintDiscountType = 3 THEN ' free' ELSE '' END),
            '.')
         WHEN tintDiscoutBaseOn = 3 THEN ' related items.'
         ELSE ''
         END)  ELSE '' END) INTO v_vPromotionDesc FROM
      PromotionOffer PO WHERE
      numDomainId = v_numDomainID
      AND numProId = v_numPromotionID;
      IF(LENGTH(v_vcCouponCode) > 0) then
		
         v_bitRequireCouponCode := CAST(1 AS VARCHAR(200));
         v_vPromotionDesc := '';
      end if;
      IF (v_numShippingPromotionID = 0) then
		
         v_numShippingPromotionID := v_numPromotionID;
      end if;
      select(CONCAT((CASE WHEN coalesce(bitFixShipping1,false) = true AND v_decmPrice < monFixShipping1OrderAmount THEN CONCAT('Spend $',(monFixShipping1OrderAmount -v_decmPrice),CASE WHEN v_decmPrice > 0 THEN ' more ' ELSE '' END || ' and get $',
         monFixShipping1Charge,' shipping. ') ELSE '' END),(CASE WHEN coalesce(bitFixShipping2,false) = true AND v_decmPrice < monFixShipping2OrderAmount THEN CONCAT('Spend $',monFixShipping2OrderAmount -v_decmPrice,CASE WHEN v_decmPrice > 0 THEN ' more ' ELSE '' END || ' and get $',
         monFixShipping2Charge,' shipping. ') ELSE '' END),
      (CASE WHEN (bitFreeShiping = true AND numFreeShippingCountry = v_numShippingCountry) AND v_decmPrice < monFreeShippingOrderAmount THEN CONCAT('Spend $',monFreeShippingOrderAmount -v_decmPrice,CASE WHEN v_decmPrice > 0 THEN ' more ' ELSE '' END || ' and shipping is FREE! ') ELSE '' END))), ((CASE WHEN coalesce(bitFixShipping1,false) = true AND v_decmPrice > monFixShipping1OrderAmount AND 1 =(CASE WHEN coalesce(bitFixShipping2,false) = true AND v_decmPrice < monFixShipping2OrderAmount THEN 1 WHEN coalesce(bitFixShipping2,false) = false THEN 1 ELSE 0 END) THEN 'You have qualified for $' || SUBSTR(CAST(monFixShipping1Charge AS VARCHAR(30)),1,30) || ' shipping'
      WHEN coalesce(bitFixShipping2,false) = true AND v_decmPrice > monFixShipping2OrderAmount  AND 1 =(CASE WHEN coalesce(bitFreeShiping,false) = true AND v_decmPrice < monFreeShippingOrderAmount THEN 1 WHEN coalesce(bitFreeShiping,false) = false THEN 1 ELSE 0 END) THEN 'You have qualified for $' || SUBSTR(CAST(monFixShipping2Charge AS VARCHAR(30)),1,30) || ' shipping'
      WHEN (coalesce(bitFreeShiping,false) = true AND numFreeShippingCountry = v_numShippingCountry AND v_decmPrice > monFreeShippingOrderAmount) THEN 'You have qualified for Free Shipping' END)) INTO v_vcShippingDescription,v_vcQualifiedShipping FROM
      PromotionOffer PO
      LEFT JOIN ShippingPromotions SP ON PO.numDomainId = SP.numDomainID WHERE
      PO.numDomainId = v_numDomainID
      AND numProId = v_numShippingPromotionID;
      IF((SELECT COUNT(*) FROM CartItems WHERE numItemCode = v_numItemCode AND numDomainId = v_numDomainID AND numUserCntId = v_numUserCntId AND vcCookieId = v_cookieId AND PromotionID > 0 AND v_vcCouponCode <> '') > 0) then
		
         v_bitRequireCouponCode := 'APPLIED';
         SELECT  vcCoupon INTO v_vcCouponCode FROM CartItems WHERE numItemCode = v_numItemCode AND vcCookieId = v_cookieId AND numUserCntId = v_numUserCntId AND numDomainId = v_numDomainID AND PromotionID > 0 AND v_vcCouponCode <> ''     LIMIT 1;
         select(CONCAT('Buy ',CASE WHEN tintOfferTriggerValueType = 1 THEN SUBSTR(CAST(fltOfferTriggerValue AS VARCHAR(30)),1,30) ELSE CONCAT('$',fltOfferTriggerValue) END,CASE tintOfferBasedOn
         WHEN 1 THEN CONCAT(' of "',(SELECT OVERLAY((SELECT CONCAT(', ',Item.vcItemName) FROM PromotionOfferItems INNER JOIN Item ON PromotionOfferItems.numValue = Item.numItemCode WHERE numProId = PO.numProId AND tintRecordType = 5 AND tintType = 1) placing '' from 1 for 2)),'"')
         WHEN 2 THEN CONCAT(' of items belonging to classification(s) "',(SELECT OVERLAY((SELECT CONCAT(', ',Listdetails.vcData) FROM PromotionOfferItems INNER JOIN Listdetails ON PromotionOfferItems.numValue = Listdetails.numListItemID WHERE numListID = 36 AND numProId = PO.numProId AND tintRecordType = 5 AND tintType = 2) placing '' from 1 for 2)), 
            '"')
         END,' & get ',CASE tintDiscountType WHEN 1 THEN CONCAT(fltDiscountValue,'% off on') WHEN 2 THEN CONCAT('$',fltDiscountValue,' off on') WHEN 3 THEN CONCAT(fltDiscountValue,' of') END, 
         CASE
         WHEN tintDiscoutBaseOn = 1 THEN
            CONCAT(CONCAT(' "',(SELECT OVERLAY((SELECT CONCAT(',',Item.vcItemName) FROM PromotionOfferItems INNER JOIN Item ON PromotionOfferItems.numValue = Item.numItemCode WHERE numProId = PO.numProId AND tintRecordType = 6 AND tintType = 1) placing '' from 1 for 1)),'"'),(CASE WHEN PO.tintDiscountType = 3 THEN ' free' ELSE '' END),
            '.')
         WHEN tintDiscoutBaseOn = 2 THEN
            CONCAT(CONCAT(' items belonging to classification(s) "',(SELECT OVERLAY((SELECT CONCAT(',',Listdetails.vcData) FROM PromotionOfferItems INNER JOIN Listdetails ON PromotionOfferItems.numValue = Listdetails.numListItemID WHERE numListID = 36 AND numProId = PO.numProId AND tintRecordType = 6 AND tintType = 2) placing '' from 1 for 1)), 
            '"'),(CASE WHEN PO.tintDiscountType = 3 THEN ' free' ELSE '' END),
            '.')
         WHEN tintDiscoutBaseOn = 3 THEN ' related items.'
         ELSE ''
         END)) INTO v_vPromotionDesc FROM
         PromotionOffer PO WHERE
         numDomainId = v_numDomainID
         AND txtCouponCode = v_vcCouponCode;
         RAISE NOTICE '%',v_vPromotionDesc;
      end if;
      open SWV_RefCur for
      SELECT v_vPromotionDesc AS vcPromotionDescription,v_vcShippingDescription AS vcShippingDescription,v_vcQualifiedShipping AS vcQualifiedShipping,coalesce(v_bitRequireCouponCode,'False') AS bitRequireCouponCode,v_vcCouponCode AS vcCouponCode,
		coalesce(v_tintOfferTriggerValueType,0) as tintOfferTriggerValueType,coalesce(v_fltOfferTriggerValue,0) as fltOfferTriggerValue;
      RAISE NOTICE '%',v_decmPrice;
   ELSE
      select   PO.bitRequireCouponCode, PO.tintOfferTriggerValueType, PO.fltOfferTriggerValue INTO v_bitRequireCouponCode,v_tintOfferTriggerValueType,v_fltOfferTriggerValue FROM
      PromotionOffer PO WHERE
      numDomainId = v_numDomainID
      AND coalesce(bitEnabled,false) = true
      AND coalesce(bitAppliesToSite,false) = true
      AND txtCouponCode = v_vcSendCoupon;
      select(CONCAT('Buy ',CASE WHEN tintOfferTriggerValueType = 1 THEN SUBSTR(CAST(fltOfferTriggerValue AS VARCHAR(30)),1,30) ELSE CONCAT('$',fltOfferTriggerValue) END,CASE tintOfferBasedOn
      WHEN 1 THEN CONCAT(' of "',(SELECT OVERLAY((SELECT CONCAT(', ',Item.vcItemName) FROM PromotionOfferItems INNER JOIN Item ON PromotionOfferItems.numValue = Item.numItemCode WHERE numProId = PO.numProId AND tintRecordType = 5 AND tintType = 1) placing '' from 1 for 2)),'"')
      WHEN 2 THEN CONCAT(' of items belonging to classification(s) "',(SELECT OVERLAY((SELECT CONCAT(', ',Listdetails.vcData) FROM PromotionOfferItems INNER JOIN Listdetails ON PromotionOfferItems.numValue = Listdetails.numListItemID WHERE numListID = 36 AND numProId = PO.numProId AND tintRecordType = 5 AND tintType = 2) placing '' from 1 for 2)), 
         '"')
      END,' & get ',CASE tintDiscountType WHEN 1 THEN CONCAT(fltDiscountValue,'% off on') WHEN 2 THEN CONCAT('$',fltDiscountValue,' off on') WHEN 3 THEN CONCAT(fltDiscountValue,' of') END, 
      CASE
      WHEN tintDiscoutBaseOn = 1 THEN
         CONCAT(CONCAT(' "',(SELECT OVERLAY((SELECT CONCAT(',',Item.vcItemName) FROM PromotionOfferItems INNER JOIN Item ON PromotionOfferItems.numValue = Item.numItemCode WHERE numProId = PO.numProId AND tintRecordType = 6 AND tintType = 1) placing '' from 1 for 1)),'"'),(CASE WHEN PO.tintDiscountType = 3 THEN ' free' ELSE '' END),
         '.')
      WHEN tintDiscoutBaseOn = 2 THEN
         CONCAT(CONCAT(' items belonging to classification(s) "',(SELECT OVERLAY((SELECT CONCAT(',',Listdetails.vcData) FROM PromotionOfferItems INNER JOIN Listdetails ON PromotionOfferItems.numValue = Listdetails.numListItemID WHERE numListID = 36 AND numProId = PO.numProId AND tintRecordType = 6 AND tintType = 2) placing '' from 1 for 1)), 
         '"'),(CASE WHEN PO.tintDiscountType = 3 THEN ' free' ELSE '' END),
         '.')
      WHEN tintDiscoutBaseOn = 3 THEN ' related items.'
      ELSE ''
      END)) INTO v_vPromotionDesc FROM
      PromotionOffer PO WHERE
      numDomainId = v_numDomainID
      AND txtCouponCode = v_vcSendCoupon;
      open SWV_RefCur for
      SELECT v_bitRequireCouponCode AS bitRequireCouponCode,v_tintOfferTriggerValueType AS tintOfferTriggerValueType,v_vPromotionDesc AS vPromotionDesc,
		v_fltOfferTriggerValue AS fltOfferTriggerValue;
   end if;
   RETURN;
	--END
END; $$;



