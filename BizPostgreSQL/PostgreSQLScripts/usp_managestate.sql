-- Stored procedure definition script USP_ManageState for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ManageState(v_numStateID NUMERIC(9,0) DEFAULT 0,    
v_numDomainID NUMERIC(9,0) DEFAULT 0,       
v_vcState VARCHAR(100) DEFAULT '',  
v_numCountryID NUMERIC(9,0) DEFAULT NULL,      
v_numUserID NUMERIC(9,0) DEFAULT NULL  ,
v_vcAbbreviations VARCHAR(200) DEFAULT NULL,
v_numShippingZone NUMERIC(18,0) DEFAULT 0)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   IF v_numStateID = 0 then
	
      INSERT INTO State(numCountryID,vcState,vcAbbreviations,numDomainID,numCreatedBy,numModifiedBy,bintCreatedDate,bintModifiedDate,numShippingZone)
		VALUES(v_numCountryID,v_vcState,v_vcAbbreviations,v_numDomainID,v_numUserID,v_numUserID,TIMEZONE('UTC',now()),TIMEZONE('UTC',now()),v_numShippingZone);
   ELSE
      UPDATE
      State
      SET
      vcState = v_vcState,numCountryID = v_numCountryID,numDomainID = v_numDomainID,
      numModifiedBy = v_numUserID,vcAbbreviations = v_vcAbbreviations,numShippingZone = v_numShippingZone,
      bintModifiedDate = TIMEZONE('UTC',now())
      WHERE
      numStateID = v_numStateID
      AND numDomainID = v_numDomainID;
   end if;
   RETURN;
END; $$;


