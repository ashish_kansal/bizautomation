CREATE OR REPLACE FUNCTION USP_GetConnectedCOABankAccounts(v_tintMode NUMERIC(18,0) DEFAULT 0,
	v_numDomainID NUMERIC(18,0) DEFAULT NULL,
	v_numUserContactID NUMERIC(18,0) DEFAULT NULL, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
BEGIN
   /* SQLWays Notice: SET TRANSACTION ISOLATION LEVEL READ COMMITTED must be called before procedure call */
-- SET TRANSACTION ISOLATION LEVEL READ COMMITTED
IF v_tintMode = 0 then
	
      open SWV_RefCur for
      SELECT COA.numAccountId,COA.numBankDetailID,COA.vcAccountName,COA.numOpeningBal,FormatedDateTimeFromDate(COA.dtOpeningDate::TIMESTAMP,v_numDomainID) AS dtOpeningDate,
CASE fn_GetDateTimeDifference(LOCALTIMESTAMP,coalesce((SELECT MAX(dtCreatedDate)
         FROM BankStatementHeader WHERE numBankDetailID = BD.numBankDetailID),LOCALTIMESTAMP))
      WHEN '' THEN ''
      ELSE(fn_GetDateTimeDifference(LOCALTIMESTAMP,coalesce((SELECT MAX(dtCreatedDate)
            FROM BankStatementHeader WHERE numBankDetailID = BD.numBankDetailID),LOCALTIMESTAMP)) || ' ago')
      END   AS LastUpdated, BM.vcFIName,BSH.dtCreatedDate
      FROM Chart_Of_Accounts COA
      INNER JOIN BankDetails BD ON  COA.numBankDetailID = BD.numBankDetailID
      INNER JOIN BankMaster BM ON BM.numBankMasterID = BD.numBankMasterID
      LEFT JOIN BankStatementHeader BSH ON BD.numBankDetailID = BSH.numBankDetailID
      WHERE COA.numDomainId = v_numDomainID
      AND COA.numBankDetailID IS NOT NULL
      AND COA.numBankDetailID <> 0
      AND COA.IsConnected = true
      AND COA.bitActive = true
      AND (BSH.dtCreatedDate IS NULL OR BSH.dtCreatedDate =(SELECT MAX(dtCreatedDate) FROM BankStatementHeader WHERE numBankDetailID = BD.numBankDetailID));
   ELSEIF v_tintMode = 1
   then
	
      open SWV_RefCur for
      SELECT COA.numAccountId,COA.numBankDetailID,COA.vcAccountName,COA.numOpeningBal,FormatedDateTimeFromDate(COA.dtOpeningDate::TIMESTAMP,v_numDomainID) AS dtOpeningDate, BM.vcFIName,BSH.dtCreatedDate
      FROM Chart_Of_Accounts COA
      INNER JOIN BankDetails BD ON  COA.numBankDetailID = BD.numBankDetailID
      INNER JOIN BankMaster BM ON BM.numBankMasterID = BD.numBankMasterID
      LEFT JOIN BankStatementHeader BSH ON BD.numBankDetailID = BSH.numBankDetailID
      WHERE COA.numDomainId = v_numDomainID
      AND COA.numBankDetailID IS NOT NULL
      AND BD.vcAccountType <> '4'
      AND COA.numBankDetailID <> 0
      AND COA.IsConnected = true
      AND COA.bitActive = true
      AND (BSH.dtCreatedDate IS NULL OR BSH.dtCreatedDate =(SELECT MAX(dtCreatedDate) FROM BankStatementHeader WHERE numBankDetailID = BD.numBankDetailID));
   end if;
   RETURN;
END; $$;


