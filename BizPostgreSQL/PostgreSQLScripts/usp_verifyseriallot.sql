-- Stored procedure definition script USP_VerifySerialLot for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_VerifySerialLot(v_numOppItemID NUMERIC(18,0),
	v_numWarehouseItemID NUMERIC(18,0),
	v_vcSerialLot VARCHAR(100),
	v_bitLotNo BOOLEAN, INOUT SWV_RefCur refcursor)
LANGUAGE plpgsql
   AS $$
   DECLARE

   v_vcLotNo  VARCHAR(200) DEFAULT '';
   v_numQty  VARCHAR(200) DEFAULT 0;
BEGIN
   IF v_bitLotNo = true then --LOT ITEM
      DROP TABLE IF EXISTS tt_TEMPLOT CASCADE;
      CREATE TEMPORARY TABLE tt_TEMPLOT
      (
         vcLotNo VARCHAR(100),
         numQty INTEGER
      );
      select   SUBSTR(OutParam,0,coalesce(POSITION(substring(v_vcSerialLot from E'\\(') IN v_vcSerialLot),
      0)), CAST(SUBSTR(OutParam,(coalesce(POSITION(substring(v_vcSerialLot from E'\\(') IN v_vcSerialLot),
      0)+1),(coalesce(POSITION(substring(v_vcSerialLot from E'\\)') IN v_vcSerialLot),
      0) -coalesce(POSITION(substring(v_vcSerialLot from E'\\(') IN v_vcSerialLot),
      0) -1)) AS INTEGER) INTO v_vcLotNo,v_numQty FROM
      SplitString(v_vcSerialLot,',');
      IF EXISTS(SELECT numWareHouseItmsDTLID FROM WareHouseItmsDTL WHERE numWareHouseItemID = v_numWarehouseItemID AND vcSerialNo = v_vcLotNo AND coalesce(numQty,0) >= cast(NULLIF(v_numQty,'') as INTEGER)) then
		
         open SWV_RefCur for
         SELECT 1;
      ELSE
         open SWV_RefCur for
         SELECT 0;
      end if;
   ELSE
      IF EXISTS(SELECT numWareHouseItmsDTLID FROM WareHouseItmsDTL WHERE numWareHouseItemID = v_numWarehouseItemID AND vcSerialNo = v_vcSerialLot AND coalesce(numQty,0) > 0) then
		
         open SWV_RefCur for
         SELECT 1;
      ELSE
         open SWV_RefCur for
         SELECT 0;
      end if;
   end if;
   RETURN;
END; $$;

