-- Stored procedure definition script USP_AddImportantItems for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_AddImportantItems(v_numEmailHstrID NUMERIC(9,0))
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_NewEmailHstrID  INTEGER;
BEGIN
   insert into    EmailHistory(vcSubject,vcBody,bintCreatedOn,numNoofTimes,vcItemId,vcChangeKey,bitIsRead,vcSize,bitHasAttachments,
	dtReceivedOn,tintType,chrSource,vcCategory,vcBodyText,numDomainID,numUserCntId,numUid)
   select vcSubject,vcBody,bintCreatedOn,numNoofTimes,vcItemId,vcChangeKey,bitIsRead,vcSize,bitHasAttachments,dtReceivedOn,'4',
	chrSource,vcCategory,vcBodyText,numDomainID,numUserCntId,numUid
   from EmailHistory where numEmailHstrID = v_numEmailHstrID;            
                
   v_NewEmailHstrID := CURRVAL('emailHistory_seq');            
            
   insert into    EmailHStrToBCCAndCC(numEmailHstrID,vcName,tintType,numDivisionId,numEmailId)
   select v_NewEmailHstrID,vcName,tintType,numDivisionId,numEmailId from  EmailHStrToBCCAndCC where numEmailHstrID = v_numEmailHstrID;            
                  
   insert into EmailHstrAttchDtls(numEmailHstrID,vcFileName,vcAttachmentItemId,vcAttachmentType)
   select  v_NewEmailHstrID,vcFileName,vcAttachmentItemId,vcAttachmentType  from EmailHstrAttchDtls where numEmailHstrID = v_numEmailHstrID;
RETURN;
END; $$;


