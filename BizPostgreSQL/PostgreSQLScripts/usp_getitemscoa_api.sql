-- Stored procedure definition script USP_GetItemsCOA_API for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetItemsCOA_API(v_numDomainID NUMERIC(9,0),
          v_WebAPIId    INTEGER,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT   numItemCode,vcItemName,coalesce(I.charItemType,'') AS charItemType,
		coalesce(I.numCOGsChartAcntId,0) AS numCOGsChartAcntId,
		coalesce(I.numAssetChartAcntId,0) AS numAssetChartAcntId,
		coalesce(I.numIncomeChartAcntId,0) AS numIncomeChartAcntId
   FROM    Item I
   LEFT JOIN Chart_Of_Accounts COA ON I.numAssetChartAcntId = COA.numAccountId
   LEFT JOIN Chart_Of_Accounts COA1 ON I.numIncomeChartAcntId = COA1.numAccountId
   LEFT JOIN Chart_Of_Accounts COA2 ON I.numCOGsChartAcntId = COA2.numAccountId
   WHERE   I.numDomainID = v_numDomainID
   AND vcExportToAPI IS NOT NULL
   AND v_WebAPIId IN(SELECT Id FROM SplitIds(vcExportToAPI,','))
   AND I.charItemType = 'P'
   AND (coalesce(I.numAssetChartAcntId,0) = 0
   OR coalesce(I.numCOGsChartAcntId,0) = 0
   OR coalesce(I.numIncomeChartAcntId,0) = 0)
   union
   SELECT   numItemCode,vcItemName,coalesce(I.charItemType,'') AS charItemType,
		coalesce(I.numCOGsChartAcntId,0) AS numCOGsChartAcntId,
		coalesce(I.numAssetChartAcntId,0) AS numAssetChartAcntId,
		coalesce(I.numIncomeChartAcntId,0) AS numIncomeChartAcntId
   FROM    Item I
   LEFT JOIN Chart_Of_Accounts COA1 ON I.numIncomeChartAcntId = COA1.numAccountId
   WHERE   I.numDomainID = v_numDomainID
   AND vcExportToAPI IS NOT NULL
   AND v_WebAPIId IN(SELECT Id FROM SplitIds(vcExportToAPI,','))
   AND I.charItemType = 'N'
   AND coalesce(I.numIncomeChartAcntId,0) = 0
   union
   SELECT   numItemCode,vcItemName,coalesce(I.charItemType,'') AS charItemType,
		coalesce(I.numCOGsChartAcntId,0) AS numCOGsChartAcntId,
		coalesce(I.numAssetChartAcntId,0) AS numAssetChartAcntId,
		coalesce(I.numIncomeChartAcntId,0) AS numIncomeChartAcntId
   FROM    Item I
   LEFT JOIN Chart_Of_Accounts COA1 ON I.numIncomeChartAcntId = COA1.numAccountId
   WHERE   I.numDomainID = v_numDomainID
   AND vcExportToAPI IS NOT NULL
   AND v_WebAPIId IN(SELECT Id FROM SplitIDS(vcExportToAPI,','))
   AND I.charItemType = 'S'
   AND coalesce(I.numIncomeChartAcntId,0) = 0
   union
   SELECT   numItemCode,vcItemName,coalesce(I.charItemType,'') AS charItemType,
		coalesce(I.numCOGsChartAcntId,0) AS numCOGsChartAcntId,
		coalesce(I.numAssetChartAcntId,0) AS numAssetChartAcntId,
		coalesce(I.numIncomeChartAcntId,0) AS numIncomeChartAcntId
   FROM    Item I
   LEFT JOIN Chart_Of_Accounts COA1 ON I.numIncomeChartAcntId = COA1.numAccountId
   WHERE   I.numDomainID = v_numDomainID
   AND I.numItemCode in(SELECT coalesce(WD.numDiscountItemMapping,0) AS numDiscountItemMapping FROM WebAPIDetail WD
      INNER JOIN Domain D ON D.numDomainId = WD.numDomainID
      WHERE WD.numDomainID = v_numDomainID and webAPiID = v_WebAPIId)
   AND coalesce(I.numIncomeChartAcntId,0) = 0
   union
   SELECT   numItemCode,vcItemName,coalesce(I.charItemType,'') AS charItemType,
		coalesce(I.numCOGsChartAcntId,0) AS numCOGsChartAcntId,
		coalesce(I.numAssetChartAcntId,0) AS numAssetChartAcntId,
		coalesce(I.numIncomeChartAcntId,0) AS numIncomeChartAcntId
   FROM    Item I
   LEFT JOIN Chart_Of_Accounts COA1 ON I.numIncomeChartAcntId = COA1.numAccountId
   WHERE   I.numDomainID = v_numDomainID
   AND I.numItemCode in(SELECT coalesce(WD.numSalesTaxItemMapping,0) AS numSalesTaxItemMapping	FROM WebAPIDetail WD
      INNER JOIN Domain D ON D.numDomainId = WD.numDomainID
      WHERE WD.numDomainID = v_numDomainID and webAPiID = v_WebAPIId)
   AND coalesce(I.numIncomeChartAcntId,0) = 0
   union
   SELECT   numItemCode,vcItemName,coalesce(I.charItemType,'') AS charItemType,
		coalesce(I.numCOGsChartAcntId,0) AS numCOGsChartAcntId,
		coalesce(I.numAssetChartAcntId,0) AS numAssetChartAcntId,
		coalesce(I.numIncomeChartAcntId,0) AS numIncomeChartAcntId
   FROM    Item I
   LEFT JOIN Chart_Of_Accounts COA1 ON I.numIncomeChartAcntId = COA1.numAccountId
   WHERE   I.numDomainID = v_numDomainID
   AND I.numItemCode in(SELECT coalesce(D.numShippingServiceItemID,0) AS numShippingServiceItemID FROM WebAPIDetail WD
      INNER JOIN Domain D ON D.numDomainId = WD.numDomainID
      WHERE WD.numDomainID = v_numDomainID and WD.WebApiId = v_WebAPIId)
   AND coalesce(I.numIncomeChartAcntId,0) = 0;
END; $$;
	














