-- Stored procedure definition script usp_GetFirstUser for PostgreSQL
CREATE OR REPLACE FUNCTION usp_GetFirstUser(v_numDomainId NUMERIC   
--
,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT cast(min(numUserId) as VARCHAR(255)) FROM UserMaster WHERE  numDomainID = v_numDomainId;
END; $$;












