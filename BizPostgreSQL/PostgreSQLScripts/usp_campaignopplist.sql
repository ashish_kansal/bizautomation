CREATE OR REPLACE FUNCTION USP_CampaignOppList(v_numCampaignid NUMERIC(9,0),
    v_tintComptype SMALLINT,
    INOUT v_NoofRecds INTEGER , INOUT SWV_RefCur refcursor)
LANGUAGE plpgsql 
   AS $$
   DECLARE
   v_bitIsOnline  BOOLEAN;
   v_strSql  VARCHAR(8000);
   v_strWhere  VARCHAR(8000);
   v_numDomainID  NUMERIC(9,0);
BEGIN
   select   numDomainID INTO v_numDomainID FROM    CampaignMaster WHERE   numCampaignID = v_numCampaignid    LIMIT 1;        

   v_strSql := ' 
           SELECT  C.numCompanyID,
                    Div.numDivisionID,
                    ADC.numContactID,
                    Div.tintCRMType,
                    Opp.numOppId,
                    Opp.vcPOppName AS Name,
                    C.vcCompanyName || '', '' || Div.vcDivisionName AS Company,
--                    GetOppLstStage(Opp.numOppId) AS Stage,
                    GetOppLstMileStonePercentage(numOppId) AS Milestone,
                    Opp.monPAmount
            FROM    OpportunityMaster Opp
                    INNER JOIN AdditionalContactsInformation ADC ON Opp.numContactId = ADC.numContactId
                    INNER JOIN DivisionMaster Div ON Opp.numDivisionId = Div.numDivisionID
                                                     AND ADC.numDivisionId = Div.numDivisionID
                    INNER JOIN CompanyInfo C ON Div.numCompanyID = C.numCompanyId
                    LEFT JOIN CampaignMaster CAMP ON CAMP.numCampaignId = Opp.numCampainID AND CAMP.numDomainID = Opp.numDomainID
            WHERE   Div.numDomainID = ' || SUBSTR(CAST(v_numDomainID AS VARCHAR(30)),1,30) || ' and 
					Opp.numCampainID = ' || SUBSTR(CAST(v_numCampaignid AS VARCHAR(30)),1,30);

    
   IF v_tintComptype = 0 then -- All Records    
        
      RAISE NOTICE '%',v_strSql;
      OPEN SWV_RefCur FOR EXECUTE v_strSql;

   end if;    
    
   IF v_tintComptype = 1 then -- Sales Open opportunities    
        
      v_strWhere := ' AND opp.tintOppstatus = 0 AND opp.tintOppType = 1 ';
      v_strSql := coalesce(v_strSql,'') || coalesce(v_strWhere,'');
      RAISE NOTICE '%',v_strSql;
      OPEN SWV_RefCur FOR EXECUTE v_strSql;

   end if;    
    
   IF v_tintComptype = 2 then -- Purchase Open opportunities    
        
      v_strWhere := ' AND opp.tintOppstatus = 0 AND opp.tintOppType = 2 ';
      v_strSql := coalesce(v_strSql,'') || coalesce(v_strWhere,'');
      RAISE NOTICE '%',v_strSql;
      OPEN SWV_RefCur FOR EXECUTE v_strSql;

   end if;    
    
   IF v_tintComptype = 3 then -- Sales Deal Won    
        
      v_strWhere := ' AND opp.tintOppstatus = 1 AND opp.tintOppType = 1 ';
      v_strSql := coalesce(v_strSql,'') || coalesce(v_strWhere,'');
      RAISE NOTICE '%',v_strSql;
      OPEN SWV_RefCur FOR EXECUTE v_strSql;

   end if;    
    
   IF v_tintComptype = 4 then -- Purchase Deal Won    
        
      v_strWhere := ' AND opp.tintOppstatus = 1 AND opp.tintOppType = 2 ';
      v_strSql := coalesce(v_strSql,'') || coalesce(v_strWhere,'');
      RAISE NOTICE '%',v_strSql;
      OPEN SWV_RefCur FOR EXECUTE v_strSql;

   end if;    
    
   IF v_tintComptype = 5 then -- Sales Deal Lost    
        
      v_strWhere := ' AND opp.tintOppstatus = 2 AND opp.tintOppType = 1 ';
      v_strSql := coalesce(v_strSql,'') || coalesce(v_strWhere,'');
      RAISE NOTICE '%',v_strSql;
      OPEN SWV_RefCur FOR EXECUTE v_strSql;
     
   end if;    
    
   IF v_tintComptype = 6 then -- Purchase Deal Lost    
        
      v_strWhere := ' AND opp.tintOppstatus = 2 AND opp.tintOppType = 2 ';
      v_strSql := coalesce(v_strSql,'') || coalesce(v_strWhere,'');
      RAISE NOTICE '%',v_strSql;
      OPEN SWV_RefCur FOR EXECUTE v_strSql;
   
   end if;
   RETURN;
END; $$;


