-- Stored procedure definition script USP_GetOpeningBalanceForMasterListDetails for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetOpeningBalanceForMasterListDetails(v_numListItemID NUMERIC(9,0),      
v_numDomainId NUMERIC(9,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for Select cast(coalesce(numOpeningBal,0) as DECIMAL(20,5)) AS numOpeningBal From Chart_Of_Accounts
   Where numListItemID = v_numListItemID And numDomainId = v_numDomainId;
END; $$;












