-- Stored procedure definition script USP_InsertJournalEntryForTime for PostgreSQL
CREATE OR REPLACE FUNCTION USP_InsertJournalEntryForTime(v_datEntry_Date TIMESTAMP,
    v_numAmount DECIMAL(20,5),
    v_numJournal_Id NUMERIC(9,0) DEFAULT 0,
    v_numDomainId NUMERIC(9,0) DEFAULT 0,
    v_numUserCntID NUMERIC(9,0) DEFAULT 0,
    v_numCategoryHDRID NUMERIC(9,0) DEFAULT 0,
    v_strRow TEXT DEFAULT '',
    v_numAccountID NUMERIC(9,0) DEFAULT NULL,
    v_numProjectID NUMERIC(9,0) DEFAULT 0,
    v_numClassID NUMERIC DEFAULT 0
--    @Mode AS TINYINT = 0
,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_hDoc3  INTEGER;
BEGIN
   IF v_numProjectID = 0 then  
      v_numProjectID := NULL;
   end if;
   IF v_numClassID = 0 then  
      v_numClassID := NULL;
   end if;
    
   IF v_numJournal_Id = 0 then
      IF v_numAccountID > 0 then
   
         select   numJOurnal_Id INTO v_numJournal_Id From General_Journal_Header Where numCategoryHDRID = v_numCategoryHDRID And numDomainId = v_numDomainId;
         Delete from General_Journal_Details where numJournalId = v_numJournal_Id and numDomainId = v_numDomainId;
         Delete from General_Journal_Header Where numJOurnal_Id = v_numJournal_Id and numDomainId = v_numDomainId;
      end if;
   end if;
	
--                END                     
   IF v_numAccountID = 0 then
      v_numAccountID := NULL;
   end if;
        
   INSERT  INTO General_Journal_Header(datEntry_Date,
                          numAmount,
                          numDomainId,
                          numCategoryHDRID,
                          numChartAcntId,
                          numCreatedBy,
                          datCreatedDate,
                          numProjectID,
                          numClassID)
                VALUES(v_datEntry_Date,
                          v_numAmount,
                          v_numDomainId,
                          v_numCategoryHDRID,
                          v_numAccountID,
                          v_numUserCntID,
                          TIMEZONE('UTC',now()),
                          v_numProjectID,
                          v_numClassID);                              
                
   v_numJournal_Id := CURRVAL('General_Journal_Header_seq');                              
                
           
   open SWV_RefCur for SELECT  v_numJournal_Id;     

-- Insert Journal entry details
   SELECT * INTO v_hDoc3 FROM SWF_Xml_PrepareDocument(v_strRow);                                                                                                             
   BEGIN                                                                                   
--                    IF @Mode = 0 
--                        BEGIN
      INSERT  INTO General_Journal_Details(numJournalId,
                                      numDebitAmt,
                                      numCreditAmt,
                                      numChartAcntId,
                                      numCustomerId,
                                      numDomainId)
      SELECT  v_numJournal_Id,numDebitAmt,numCreditAmt,numChartAcntId,numCustomerId,numDomainId
      FROM(SELECT * FROM      SWF_OpenXml(v_hDoc3,'/NewDataSet/Table1 [TransactionId=0]','numJournalId |numDebitAmt |numCreditAmt |numChartAcntId |numCustomerId |numDomainId') SWA_OpenXml(numJournalId NUMERIC(9,0),numDebitAmt DECIMAL(20,5),numCreditAmt DECIMAL(20,5),
         numChartAcntId NUMERIC(9,0),numCustomerId NUMERIC(9,0),numDomainId NUMERIC(9,0))) X;
   END;
   RETURN;
END; $$;
/****** Object:  StoredProcedure [dbo].[USP_InsertJournalEntryHeader]    Script Date: 07/26/2008 16:19:09 ******/













