-- Stored procedure definition script USP_DemandForecast_Execute for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_DemandForecast_Execute(v_numDFID NUMERIC(18,0)
	,v_vcSelectedIDs TEXT
	,v_numUserCntID NUMERIC(18,0)
	,v_ClientTimeZoneOffset INTEGER
	,v_CurrentPage INTEGER
    ,v_PageSize INTEGER
	,v_columnName TEXT                                                          
	,v_columnSortOrder VARCHAR(10)
	,v_numDomainID NUMERIC(18,0)
	,v_vcRegularSearchCriteria TEXT DEFAULT ''
	,v_bitShowAllItems BOOLEAN DEFAULT NULL
	,v_bitShowHistoricSales BOOLEAN DEFAULT NULL
	,v_numHistoricalAnalysisPattern NUMERIC(18,0) DEFAULT NULL
	,v_bitBasedOnLastYear BOOLEAN DEFAULT NULL
	,v_bitIncludeOpportunity BOOLEAN DEFAULT NULL
	,v_numOpportunityPercentComplete NUMERIC(18,0) DEFAULT NULL
	,v_tintDemandPlanBasedOn SMALLINT DEFAULT NULL
	,v_tintPlanType SMALLINT DEFAULT NULL
    ,INOUT SWV_RefCur refcursor DEFAULT NULL
    ,INOUT SWV_RefCur2 refcursor DEFAULT NULL
	,INOUT SWV_RefCur3 refcursor DEFAULT NULL
)
LANGUAGE plpgsql
AS $$
   DECLARE
   v_dtLastExecution  TIMESTAMP;
   v_bitWarehouseFilter  BOOLEAN DEFAULT false;
   v_bitItemClassificationFilter  BOOLEAN DEFAULT false;
   v_bitItemGroupFilter  BOOLEAN DEFAULT false;
   v_tintUnitsRecommendationForAutoPOBackOrder  SMALLINT;
   v_bitIncludeRequisitions  BOOLEAN;

   v_Nocolumns  SMALLINT DEFAULT 0;

   v_strColumns  TEXT DEFAULT '';
   v_tintOrder  SMALLINT DEFAULT 0;                                                 
   v_vcFieldName  VARCHAR(50);                                                  
   v_vcListItemType  VARCHAR(3);                                             
   v_vcAssociatedControlType  VARCHAR(20);     
   v_numListID  NUMERIC(9,0);
   v_vcDbColumnName  VARCHAR(50);                      
   v_WhereCondition  TEXT DEFAULT '';                   
   v_vcLookBackTableName  VARCHAR(2000);                
   v_bitCustom  BOOLEAN;
   v_bitAllowEdit  BOOLEAN;                   
   v_numFieldId  NUMERIC;  
   v_bitAllowSorting  BOOLEAN;
   v_vcColumnName  VARCHAR(500);             
   v_ListRelID  NUMERIC(9,0); 
   v_SearchQuery  TEXT DEFAULT ''; 
	   
   v_StrSql  TEXT DEFAULT '';
   v_firstRec  INTEGER;
   v_lastRec  INTEGER;
  
   v_strFinal  TEXT;
   v_Prefix  VARCHAR(50);
   SWV_RowCount INTEGER;
   
BEGIN

IF(SELECT COUNT(*) FROM DemandForecast WHERE numDFID = v_numDFID) > 0 then
	
      IF(SELECT COUNT(*) FROM DemandForecastWarehouse WHERE numDFID = v_numDFID) > 0 then
		
         v_bitWarehouseFilter := true;
      end if;
      IF(SELECT COUNT(*) FROM DemandForecastItemClassification WHERE numDFID = 10009) > 0 then
		
         v_bitItemClassificationFilter := true;
      end if;
      IF(SELECT COUNT(*) FROM DemandForecastItemGroup WHERE numDFID = v_numDFID) > 0 then
		
         v_bitItemGroupFilter := true;
      end if;
   end if;

   SELECT coalesce(tintUnitsRecommendationForAutoPOBackOrder,1), coalesce(bitIncludeRequisitions,false) 
   INTO v_tintUnitsRecommendationForAutoPOBackOrder,v_bitIncludeRequisitions 
   FROM Domain 
   WHERE numDomainId = v_numDomainID;

   DROP TABLE IF EXISTS tt_TEMP CASCADE;
   CREATE TEMPORARY TABLE tt_TEMP
   (
      numItemCode NUMERIC(18,0),
      numWarehouseItemID NUMERIC(18,0),
      numUnitHour DOUBLE PRECISION,
      dtReleaseDate DATE
   );

   INSERT INTO tt_TEMP(numItemCode
		,numWarehouseItemID
		,numUnitHour
		,dtReleaseDate)
   SELECT
   Item.numItemCode
		,OpportunityItems.numWarehouseItmsID
		,OpportunityItems.numUnitHour -coalesce(OpportunityItems.numQtyShipped,0)
		,coalesce(OpportunityItems.ItemReleaseDate,OpportunityMaster.dtReleaseDate)
   FROM
   OpportunityMaster
   INNER JOIN
   OpportunityItems
   ON
   OpportunityMaster.numOppId = OpportunityItems.numOppId
   INNER JOIN
   Item
   ON
   OpportunityItems.numItemCode = Item.numItemCode
   INNER JOIN
   WareHouseItems
   ON
   OpportunityItems.numWarehouseItmsID = WareHouseItems.numWareHouseItemID
   WHERE
   OpportunityMaster.numDomainId = v_numDomainID
   AND OpportunityMaster.tintopptype = 1
   AND OpportunityMaster.tintoppstatus = 1
   AND coalesce(OpportunityMaster.tintshipped,0) = 0
   AND(coalesce(OpportunityItems.numUnitHour,0) - coalesce(OpportunityItems.numQtyShipped,0)) > 0
   AND coalesce(OpportunityItems.bitDropShip,false) = false
   AND coalesce(Item.IsArchieve,false) = false
   AND coalesce(Item.bitAssembly,false) =(CASE WHEN v_tintPlanType = 2 THEN true ELSE false END)
   AND coalesce(Item.bitKitParent,false) = false
   AND (OpportunityItems.ItemReleaseDate IS NOT NULL OR OpportunityMaster.dtReleaseDate IS NOT NULL)
   AND 1 =(CASE WHEN LENGTH(coalesce(v_vcSelectedIDs,'')) > 0
   THEN(CASE WHEN Item.numItemCode IN(SELECT Id FROM SplitIDs(v_vcSelectedIDs,',')) THEN 1 ELSE 0 END)
   ELSE 1
   END)
   AND
		(v_bitWarehouseFilter = false OR
   WareHouseItems.numWareHouseID IN(SELECT numWareHouseID FROM DemandForecastWarehouse WHERE numDFID = v_numDFID))
   AND
		(v_bitItemClassificationFilter = false OR
   Item.numItemClassification IN(SELECT numItemClassificationID FROM DemandForecastItemClassification WHERE numDFID = v_numDFID))
   AND
		(v_bitItemGroupFilter = false OR
   Item.numItemGroup IN(SELECT numItemGroupID FROM DemandForecastItemGroup WHERE numDFID = v_numDFID));

   INSERT INTO tt_TEMP(numItemCode
		,numWarehouseItemID
		,numUnitHour
		,dtReleaseDate)
   SELECT
   Item.numItemCode
		,OKI.numWareHouseItemId
		,OKI.numQtyItemsReq - coalesce(OKI.numQtyShipped,0)
		,coalesce(OI.ItemReleaseDate,OM.dtReleaseDate)
   FROM
   OpportunityMaster OM
   INNER JOIN
   OpportunityItems OI
   ON
   OM.numOppId = OI.numOppId
   INNER JOIN
   OpportunityKitItems OKI
   ON
   OI.numoppitemtCode = OKI.numOppItemID
   INNER JOIN
   Item
   ON
   OKI.numChildItemID = Item.numItemCode
   INNER JOIN
   WareHouseItems WI
   ON
   OKI.numWareHouseItemId = WI.numWareHouseItemID
   WHERE
   OM.numDomainId = v_numDomainID
   AND OM.tintopptype = 1
   AND OM.tintoppstatus = 1
   AND coalesce(OM.tintshipped,0) = 0
   AND coalesce(OI.bitDropShip,false) = false
   AND coalesce(OI.bitWorkOrder,false) = false
   AND coalesce(Item.IsArchieve,false) = false
   AND coalesce(Item.bitAssembly,false) =(CASE WHEN v_tintPlanType = 2 THEN true ELSE false END)
   AND coalesce(Item.bitKitParent,false) = false
   AND(coalesce(OKI.numQtyItemsReq,0) - coalesce(OKI.numQtyShipped,0)) > 0
   AND (OM.dtReleaseDate IS NOT NULL OR OI.ItemReleaseDate IS NOT NULL)
   AND 1 =(CASE WHEN LENGTH(coalesce(v_vcSelectedIDs,'')) > 0
   THEN(CASE WHEN Item.numItemCode IN(SELECT Id FROM SplitIDs(v_vcSelectedIDs,',')) THEN 1 ELSE 0 END)
   ELSE 1
   END)
   AND
		(v_bitWarehouseFilter = false OR
   WI.numWareHouseID IN(SELECT numWareHouseID FROM DemandForecastWarehouse WHERE numDFID = v_numDFID))
   AND
		(v_bitItemClassificationFilter = false OR
   Item.numItemClassification IN(SELECT numItemClassificationID FROM DemandForecastItemClassification WHERE numDFID = v_numDFID))
   AND
		(v_bitItemGroupFilter = false OR
   Item.numItemGroup IN(SELECT numItemGroupID FROM DemandForecastItemGroup WHERE numDFID = v_numDFID));

   INSERT INTO tt_TEMP(numItemCode
		,numWarehouseItemID
		,numUnitHour
		,dtReleaseDate)
   SELECT
   Item.numItemCode
		,OKCI.numWareHouseItemId
		,OKCI.numQtyItemsReq - coalesce(OKCI.numQtyShipped,0)
		,coalesce(OI.ItemReleaseDate,OM.dtReleaseDate)
   FROM
   OpportunityMaster OM
   INNER JOIN
   OpportunityItems OI
   ON
   OM.numOppId = OI.numOppId
   INNER JOIN
   OpportunityKitChildItems OKCI
   ON
   OI.numoppitemtCode = OKCI.numOppItemID
   INNER JOIN
   Item
   ON
   OKCI.numItemID = Item.numItemCode
   INNER JOIN
   WareHouseItems WI
   ON
   OKCI.numWareHouseItemId = WI.numWareHouseItemID
   WHERE
   OM.numDomainId = v_numDomainID
   AND OM.tintopptype = 1
   AND OM.tintoppstatus = 1
   AND coalesce(OM.tintshipped,0) = 0
   AND coalesce(OI.bitDropShip,false) = false
   AND coalesce(OI.bitWorkOrder,false) = false
   AND coalesce(Item.IsArchieve,false) = false
   AND coalesce(Item.bitAssembly,false) =(CASE WHEN v_tintPlanType = 2 THEN true ELSE false END)
   AND coalesce(Item.bitKitParent,false) = false
   AND(coalesce(OKCI.numQtyItemsReq,0) - coalesce(OKCI.numQtyShipped,0)) > 0
   AND (OM.dtReleaseDate IS NOT NULL OR OI.ItemReleaseDate IS NOT NULL)
   AND 1 =(CASE WHEN LENGTH(coalesce(v_vcSelectedIDs,'')) > 0
   THEN(CASE WHEN Item.numItemCode IN(SELECT Id FROM SplitIDs(v_vcSelectedIDs,',')) THEN 1 ELSE 0 END)
   ELSE 1
   END)
   AND
		(v_bitWarehouseFilter = false OR
   WI.numWareHouseID IN(SELECT numWareHouseID FROM DemandForecastWarehouse WHERE numDFID = v_numDFID))
   AND
		(v_bitItemClassificationFilter = false OR
   Item.numItemClassification IN(SELECT numItemClassificationID FROM DemandForecastItemClassification WHERE numDFID = v_numDFID))
   AND
		(v_bitItemGroupFilter = false OR
   Item.numItemGroup IN(SELECT numItemGroupID FROM DemandForecastItemGroup WHERE numDFID = v_numDFID));

   INSERT INTO tt_TEMP(numItemCode
		,numWarehouseItemID
		,numUnitHour
		,dtReleaseDate)
   SELECT
   WOD.numChildItemID
		,WOD.numWarehouseItemID
		,WOD.numQtyItemsReq
		,(CASE WHEN OM.numOppId IS NOT NULL THEN coalesce(OI.ItemReleaseDate,OM.dtReleaseDate) ELSE WO.dtmEndDate END)
   FROM
   WorkOrderDetails WOD
   INNER JOIN
   Item
   ON
   WOD.numChildItemID = Item.numItemCode
   INNER JOIN
   WareHouseItems WI
   ON
   WOD.numWarehouseItemID = WI.numWareHouseItemID
   INNER JOIN
   WorkOrder WO
   ON
   WOD.numWOId = WO.numWOId
   LEFT JOIN
   OpportunityItems OI
   ON
   WO.numOppItemID = OI.numoppitemtCode
   LEFT JOIN
   OpportunityMaster OM
   ON
   OI.numOppId = OM.numOppId
   WHERE
   WO.numDomainId = v_numDomainID
   AND WO.numWOStatus <> 23184 -- NOT COMPLETED
   AND coalesce(Item.IsArchieve,false) = false
   AND coalesce(Item.bitAssembly,false) =(CASE WHEN v_tintPlanType = 2 THEN true ELSE false END)
   AND coalesce(Item.bitKitParent,false) = false
   AND coalesce(WOD.numQtyItemsReq,0) > 0
   AND (OM.dtReleaseDate IS NOT NULL OR OI.ItemReleaseDate IS NOT NULL OR WO.dtmEndDate IS NOT NULL)
   AND 1 =(CASE WHEN OM.numOppId IS NOT NULL THEN(CASE WHEN OM.tintoppstatus = 1 THEN 1 ELSE 0 END) ELSE 1 END)
   AND 1 =(CASE WHEN LENGTH(coalesce(v_vcSelectedIDs,'')) > 0
   THEN(CASE WHEN Item.numItemCode IN(SELECT Id FROM SplitIDs(v_vcSelectedIDs,',')) THEN 1 ELSE 0 END)
   ELSE 1
   END)
   AND
		(v_bitWarehouseFilter = false OR
   WI.numWareHouseID IN(SELECT numWareHouseID FROM DemandForecastWarehouse WHERE numDFID = v_numDFID))
   AND
		(v_bitItemClassificationFilter = false OR
   Item.numItemClassification IN (SELECT numItemClassificationID FROM DemandForecastItemClassification WHERE numDFID = v_numDFID))
   AND (v_bitItemGroupFilter = false 
   OR Item.numItemGroup IN (SELECT numItemGroupID FROM DemandForecastItemGroup WHERE numDFID = v_numDFID));

   DROP TABLE IF EXISTS tt_TEMPITEMSDFEXECUTE CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPITEMSDFEXECUTE
   (
      numItemCode NUMERIC(18,0),
      numWarehouseItemID NUMERIC(18,0),
      numUnitHour DOUBLE PRECISION,
      dtReleaseDate TEXT,
      dtReleaseDateHidden TEXT
   );

   INSERT INTO tt_TEMPITEMSDFEXECUTE(numItemCode
		,numWarehouseItemID
		,numUnitHour
		,dtReleaseDate
		,dtReleaseDateHidden)
   SELECT numItemCode
		,numWarehouseItemID
		,SUM(numUnitHour)
		,COALESCE((SELECT string_agg(CONCAT(', <a href="#" onclick="return OpenDFReleaseDateRecords(',T2.numItemCode,
              ',',T2.numWarehouseItemID,',''',CAST(T2.dtReleaseDate AS VARCHAR(10)),''');">',
			  CAST(dtReleaseDate AS VARCHAR(10)),'</a> (',T2.numUnitHour,')'),' ')
			  FROM (SELECT numItemCode, numWarehouseItemID, dtReleaseDate, SUM(numUnitHour) AS numUnitHour
			   		FROM tt_TEMP
			  		GROUP BY numItemCode,numWarehouseItemID,dtReleaseDate)T2
			  WHERE T2.numItemCode = T1.numItemCode
			  AND T2.numWarehouseItemID = T1.numWarehouseItemID),'')
		,COALESCE((SELECT string_agg(CAST(T2.dtReleaseDate AS VARCHAR(10)) || ' (' || T2.numUnitHour || ')',', ')
			  FROM (SELECT numItemCode, numWarehouseItemID, dtReleaseDate, SUM(numUnitHour) AS numUnitHour
			   		FROM tt_TEMP
			  		GROUP BY numItemCode,numWarehouseItemID,dtReleaseDate)T2
			  WHERE T2.numItemCode = T1.numItemCode
			  AND T2.numWarehouseItemID = T1.numWarehouseItemID),'')
   FROM tt_TEMP T1
   GROUP BY numItemCode,numWarehouseItemID;

	---------------------------- Dynamic Query -------------------------------
		
   DROP TABLE IF EXISTS tt_TEMPFORM CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPFORM
   (
      tintOrder SMALLINT,
      vcDbColumnName VARCHAR(50),
      vcOrigDbColumnName VARCHAR(50),
      vcFieldName VARCHAR(50),
      vcAssociatedControlType VARCHAR(50),
      vcListItemType VARCHAR(3),
      numListID NUMERIC(9,0),
      vcLookBackTableName VARCHAR(50),
      bitCustomField BOOLEAN,
      numFieldId NUMERIC,
      bitAllowSorting BOOLEAN,
      bitAllowEdit BOOLEAN,
      bitIsRequired BOOLEAN,
      bitIsEmail BOOLEAN,
      bitIsAlphaNumeric BOOLEAN,
      bitIsNumeric BOOLEAN,
      bitIsLengthValidation BOOLEAN,
      intMaxLength INTEGER,
      intMinLength INTEGER,
      bitFieldMessage BOOLEAN,
      vcFieldMessage VARCHAR(500),
      ListRelID NUMERIC(9,0),
      intColumnWidth INTEGER,
      bitAllowFiltering BOOLEAN,
      vcFieldDataType CHAR(1)
   );

	-- FIRST CHECK IF GRID COLUMNS ARE CONFIGURED
   select   coalesce(SUM(TotalRow),0) INTO v_Nocolumns FROM(SELECT
      COUNT(*) AS TotalRow
      FROM
      View_DynamicColumns
      WHERE
      numFormId = 139
      AND numUserCntID = v_numUserCntID
      AND numDomainID = v_numDomainID
      AND tintPageType = 1
      AND numRelCntType = 0
      AND 1 =(CASE
      WHEN vcOrigDbColumnName = 'numBuildableQty'
      THEN(CASE WHEN v_tintPlanType = 2 THEN 1 ELSE 0 END)
      ELSE 1
      END)
      UNION
      SELECT
      COUNT(*) AS TotalRow
      FROM
      View_DynamicCustomColumns
      WHERE
      numFormId = 139
      AND numUserCntID = v_numUserCntID
      AND numDomainID = v_numDomainID
      AND tintPageType = 1
      AND numRelCntType = 0) TotalRows;

   IF v_Nocolumns = 0 then
	
      INSERT INTO DycFormConfigurationDetails(numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom,intColumnWidth)
      SELECT
      139,numFieldId,0,Row_number() over(order by tintRow desc),v_numDomainID,v_numUserCntID,0,1,false,intColumnWidth
      FROM
      View_DynamicDefaultColumns
      WHERE
      numFormId = 139
      AND bitDefault = true
      AND coalesce(bitSettingField,false) = true
      AND numDomainID = v_numDomainID
      ORDER BY
      tintOrder asc;
   end if;

   INSERT INTO tt_TEMPFORM
   SELECT tintRow+1 as tintOrder,vcDbColumnName,vcOrigDbColumnName,coalesce(vcCultureFieldName,vcFieldName),
		vcAssociatedControlType,vcListItemType,numListID,vcLookBackTableName,bitCustom,numFieldId,bitAllowSorting,bitInlineEdit,
		bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,intMaxLength,intMinLength,bitFieldMessage,
		vcFieldMessage AS vcFieldMessage,ListRelID,intColumnWidth,bitAllowFiltering,vcFieldDataType
   FROM
   View_DynamicColumns
   WHERE
   numFormId = 139
   AND numUserCntID = v_numUserCntID
   AND numDomainID = v_numDomainID
   AND tintPageType = 1
   AND coalesce(bitSettingField,false) = true
   AND coalesce(bitCustom,false) = false
   AND numRelCntType = 0
   AND 1 =(CASE
   WHEN vcOrigDbColumnName = 'numBuildableQty'
   THEN(CASE WHEN v_tintPlanType = 2 THEN 1 ELSE 0 END)
   ELSE 1
   END)
   UNION
   SELECT
   tintRow+1 as tintOrder,vcDbColumnName,vcDbColumnName,vcFieldName,vcAssociatedControlType,'' as vcListItemType,numListID,'',bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,
		bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
		intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,true,''
   FROM
   View_DynamicCustomColumns
   WHERE
   numFormId = 139
   AND numUserCntID = v_numUserCntID
   AND numDomainID = v_numDomainID
   AND tintPageType = 1
   AND coalesce(bitCustom,false) = true
   AND numRelCntType = 0
   ORDER BY
   tintOrder asc;
					
   v_strColumns := CONCAT(' COUNT(*) OVER() TotalRowsCount, Item.numItemCode, Item.vcItemName, COALESCE(Item.vcSKU,'''') vcSKU
	,COALESCE((SELECT vcUnitName FROM UOM WHERE numUOMId = Item.numBaseUnit),''-'') AS vcUOM
	,Item.charItemType AS vcItemType
	,Item.numAssetChartAcntId
	,Item.monAverageCost
	,Item.numBusinessProcessId
	,SPLM.Slp_Name
	,SPLM.numBuildManager
	,fn_GetContactName(SPLM.numBuildManager) vcBuildManager
	,COALESCE((SELECT numContactId FROM AdditionalContactsInformation WHERE numDivisionId = Item.numVendorID ORDER BY COALESCE(bitPrimaryContact,false) DESC LIMIT 1),0) AS numContactID
	,COALESCE(Item.numPurchaseUnit,0) AS numUOM
	,fn_UOMConversion(COALESCE(numBaseUnit, 0), Item.numItemCode, Item.numDomainId, COALESCE(Item.numPurchaseUnit, 0)) fltUOMConversionfactor
	,fn_GetAttributes(WI.numWareHouseItemId::NUMERIC,true::BOOLEAN) AS vcAttributes
	,WI.numWareHouseID
	,WI.numWareHouseItemID
	,W.vcWarehouse
	,COALESCE(V.numVendorID,0) numVendorID
	,COALESCE(V.intMinQty,0) intMinQty
	,COALESCE(monCost,0) * fn_UOMConversion(Item.numBaseUnit,Item.numItemCode,Item.numDomainID,Item.numPurchaseUnit) AS monVendorCost
	,T1.numUnitHour AS numQtyToPurchase, T1.dtReleaseDateHidden, T1.dtReleaseDate,(CASE
		WHEN ',v_tintUnitsRecommendationForAutoPOBackOrder,
   '= 3
		THEN 
			(COALESCE(numBackOrder,0) - COALESCE(numOnOrder,0) + (CASE WHEN ',CAST(COALESCE(v_bitIncludeRequisitions, false) AS VARCHAR),' = true  
											THEN COALESCE((SELECT 
														SUM(numUnitHour) 
													FROM 
														OpportunityItems 
													INNER JOIN 
														OpportunityMaster 
													ON 
														OpportunityItems.numOppID=OpportunityMaster.numOppId 
													WHERE 
														OpportunityMaster.numDomainId=',
   v_numDomainID,'
														AND OpportunityMaster.tintOppType=2 
														AND OpportunityMaster.tintOppStatus=0 
														AND OpportunityItems.numItemCode=Item.numItemCode 
														AND OpportunityItems.numWarehouseItmsID=WI.numWareHouseItemID),0) 
											ELSE 0 
											END)) +
			(CASE WHEN COALESCE(Item.fltReorderQty,0) >= COALESCE(V.intMinQty,0) THEN COALESCE(Item.fltReorderQty,0) ELSE COALESCE(V.intMinQty,0) END) 
		WHEN ',v_tintUnitsRecommendationForAutoPOBackOrder,
   '=2
		THEN 
			(CASE
				WHEN COALESCE(Item.fltReorderQty,0) >= COALESCE(V.intMinQty,0) AND COALESCE(Item.fltReorderQty,0) >= COALESCE(numBackOrder,0) THEN COALESCE(Item.fltReorderQty,0)
				WHEN COALESCE(V.intMinQty,0) >= COALESCE(Item.fltReorderQty,0) AND COALESCE(V.intMinQty,0) >= COALESCE(numBackOrder,0) THEN COALESCE(V.intMinQty,0)
				WHEN COALESCE(numBackOrder,0) >= COALESCE(Item.fltReorderQty,0) AND COALESCE(numBackOrder,0) >= COALESCE(V.intMinQty,0) THEN COALESCE(numBackOrder,0)
				ELSE COALESCE(Item.fltReorderQty,0)
			END) - (COALESCE(numOnOrder,0) + (CASE WHEN ',
   CAST(COALESCE(v_bitIncludeRequisitions, false) AS VARCHAR),' = true  THEN COALESCE((SELECT 
														SUM(numUnitHour) 
													FROM 
														OpportunityItems 
													INNER JOIN 
														OpportunityMaster 
													ON 
														OpportunityItems.numOppID=OpportunityMaster.numOppId 
													WHERE 
														OpportunityMaster.numDomainId=',
   v_numDomainID,'
														AND OpportunityMaster.tintOppType=2 
														AND OpportunityMaster.tintOppStatus=0 
														AND OpportunityItems.numItemCode=Item.numItemCode 
														AND OpportunityItems.numWarehouseItmsID=WI.numWareHouseItemID),0) ELSE 0 END))
		ELSE
			(CASE WHEN COALESCE(Item.fltReorderQty,0) > COALESCE(V.intMinQty,0) THEN COALESCE(Item.fltReorderQty,0) ELSE COALESCE(V.intMinQty,0) END) 
			+ COALESCE(numBackOrder,0) - (COALESCE(numOnOrder,0) + (CASE WHEN ',CAST(COALESCE(v_bitIncludeRequisitions, false) AS VARCHAR),
   ' = true  THEN COALESCE((SELECT 
																													SUM(numUnitHour) 
																												FROM 
																													OpportunityItems 
																												INNER JOIN 
																													OpportunityMaster 
																												ON 
																													OpportunityItems.numOppID=OpportunityMaster.numOppId 
																												WHERE 
																													OpportunityMaster.numDomainId=',v_numDomainID,
   '
																													AND OpportunityMaster.tintOppType=2 
																													AND OpportunityMaster.tintOppStatus=0 
																													AND OpportunityItems.numItemCode=Item.numItemCode 
																													AND OpportunityItems.numWarehouseItmsID=WI.numWareHouseItemID),0) ELSE 0 END)) 
	END) AS numQtyBasedOnPurchasePlan ');

   --Custom field 
   SELECT tintOrder+1, vcDbColumnName, vcFieldName, vcAssociatedControlType, vcListItemType, numListID, vcLookBackTableName, bitCustomField, numFieldId, bitAllowSorting, bitAllowEdit, ListRelID INTO v_tintOrder,v_vcDbColumnName,v_vcFieldName,v_vcAssociatedControlType,v_vcListItemType,
   v_numListID,v_vcLookBackTableName,v_bitCustom,v_numFieldId,
   v_bitAllowSorting,v_bitAllowEdit,v_ListRelID 
   FROM tt_TEMPFORM    
   ORDER BY tintOrder ASC LIMIT 1;   

   WHILE v_tintOrder > 0 LOOP
      IF v_bitCustom = false then
         IF v_vcLookBackTableName = 'AdditionalContactsInformation' then
            v_Prefix := 'ADC.';
         ELSEIF v_vcLookBackTableName = 'DivisionMaster'
         then
            v_Prefix := 'Div.';
         ELSEIF v_vcLookBackTableName = 'OpportunityMaster'
         then
            v_Prefix := 'Opp.';
         ELSEIF v_vcLookBackTableName = 'CompanyInfo'
         then
            v_Prefix := 'cmp.';
         ELSEIF v_vcLookBackTableName = 'DemandForecastGrid'
         then
            v_Prefix := 'DF.';
         ELSEIF v_vcLookBackTableName = 'Warehouses'
         then
            v_Prefix := 'W.';
         ELSEIF v_vcLookBackTableName = 'WareHouseItems'
         then
            v_Prefix := 'WI.';
		ELSEIF v_vcLookBackTableName = 'Vendor'
         then
            v_Prefix := 'V.';
         ELSE
            v_Prefix := CONCAT(v_vcLookBackTableName,'.');
         end if;
         
		 v_vcColumnName := coalesce(v_vcDbColumnName,'') || '~' || SUBSTR(CAST(v_numFieldId AS VARCHAR(10)),1,10) || '~0';
         
		 IF v_vcAssociatedControlType = 'TextBox' then
			
            IF v_vcDbColumnName = 'numQtyToPurchase' then
				
               v_strColumns := coalesce(v_strColumns,'') || ',' || ' (CASE WHEN COALESCE(WI.numBackOrder,0) > COALESCE(WI.numOnOrder,0) THEN  CEILING(COALESCE(WI.numBackOrder,0) - COALESCE(WI.numOnOrder,0))  ELSE 0 END) '  || ' "' || coalesce(v_vcColumnName,'') || '"';
            ELSEIF v_vcDbColumnName = 'moncost'
            then
				
               v_strColumns := coalesce(v_strColumns,'') || ',' || ' COALESCE((SELECT vcUnitName FROM UOM WHERE numUOMId = Item.numBaseUnit),''-'') '  || ' "' || coalesce(v_vcColumnName,'') || '"';
            ELSE
               v_strColumns := coalesce(v_strColumns,'') || ',' ||  coalesce(v_Prefix,'') || coalesce(v_vcDbColumnName,'') || ' "' || coalesce(v_vcColumnName,'') || '"';
            end if;
         ELSEIF v_vcAssociatedControlType = 'Label'
         then
			
            IF v_vcDbColumnName = 'vc7Days' then
				
               v_strColumns := coalesce(v_strColumns,'') || CONCAT(',fn_DemandForecastDaysDisplay(',CAST(COALESCE(v_numDomainID, 0) AS VARCHAR),'::NUMERIC(18,0),',CAST(COALESCE(v_ClientTimeZoneOffset, 0) AS VARCHAR),'::INTEGER,',
               'Item.numItemCode::NUMERIC(18,0),COALESCE(TempLeadDays.numLeadDays,0)::NUMERIC(18,0),WI.numWarehouseItemID::NUMERIC(18,0),7::NUMERIC(18,0),',CAST(COALESCE(v_bitShowHistoricSales, false) AS VARCHAR),'::BOOLEAN,',CAST(COALESCE(v_numHistoricalAnalysisPattern, 0) AS VARCHAR),'::NUMERIC(18,0),'
               ,CAST(COALESCE(v_bitBasedOnLastYear, false) AS VARCHAR),'::BOOLEAN,',CAST(COALESCE(v_bitIncludeOpportunity, false) AS VARCHAR),'::BOOLEAN,',CAST(COALESCE(v_numOpportunityPercentComplete, 0) AS VARCHAR),'::NUMERIC(18,0),'
               ,CAST(COALESCE(v_tintDemandPlanBasedOn, 0) AS VARCHAR),'::SMALLINT) "',v_vcColumnName,
               '"');
            ELSEIF v_vcDbColumnName = 'vc15Days'
            then
				
               v_strColumns := coalesce(v_strColumns,'') || CONCAT(',fn_DemandForecastDaysDisplay(',CAST(COALESCE(v_numDomainID, 0) AS VARCHAR),'::NUMERIC(18,0),',CAST(COALESCE(v_ClientTimeZoneOffset, 0) AS VARCHAR),'::INTEGER,',
               'Item.numItemCode::NUMERIC(18,0),COALESCE(TempLeadDays.numLeadDays,0)::NUMERIC(18,0),WI.numWarehouseItemID::NUMERIC(18,0),15::NUMERIC(18,0),',CAST(COALESCE(v_bitShowHistoricSales, false) AS VARCHAR),'::BOOLEAN,',CAST(COALESCE(v_numHistoricalAnalysisPattern, 0) AS VARCHAR),'::NUMERIC(18,0),'
               ,CAST(COALESCE(v_bitBasedOnLastYear, false) AS VARCHAR),'::BOOLEAN,',CAST(COALESCE(v_bitIncludeOpportunity, false) AS VARCHAR),'::BOOLEAN,',CAST(COALESCE(v_numOpportunityPercentComplete, 0) AS VARCHAR),'::NUMERIC(18,0),'
               ,CAST(COALESCE(v_tintDemandPlanBasedOn, 0) AS VARCHAR),'::SMALLINT) "',v_vcColumnName,
               '"');
            ELSEIF v_vcDbColumnName = 'vc30Days'
            then
				
               v_strColumns := coalesce(v_strColumns,'') || CONCAT(',fn_DemandForecastDaysDisplay(',CAST(COALESCE(v_numDomainID, 0) AS VARCHAR),'::NUMERIC(18,0),',CAST(COALESCE(v_ClientTimeZoneOffset, 0) AS VARCHAR),'::INTEGER,',
               'Item.numItemCode::NUMERIC(18,0),COALESCE(TempLeadDays.numLeadDays,0)::NUMERIC(18,0),WI.numWarehouseItemID::NUMERIC(18,0),30::NUMERIC(18,0),',CAST(COALESCE(v_bitShowHistoricSales, false) AS VARCHAR),'::BOOLEAN,',CAST(COALESCE(v_numHistoricalAnalysisPattern, 0) AS VARCHAR),'::NUMERIC(18,0),'
               ,CAST(COALESCE(v_bitBasedOnLastYear, false) AS VARCHAR),'::BOOLEAN,',CAST(COALESCE(v_bitIncludeOpportunity, false) AS VARCHAR),'::BOOLEAN,',CAST(COALESCE(v_numOpportunityPercentComplete, 0) AS VARCHAR),'::NUMERIC(18,0),'
               ,CAST(COALESCE(v_tintDemandPlanBasedOn, 0) AS VARCHAR),'::SMALLINT) "',v_vcColumnName,
               '"');

            ELSEIF v_vcDbColumnName = 'vc60Days'
            then
				
               v_strColumns := coalesce(v_strColumns,'') || CONCAT(',fn_DemandForecastDaysDisplay(',CAST(COALESCE(v_numDomainID, 0) AS VARCHAR),'::NUMERIC(18,0),',CAST(COALESCE(v_ClientTimeZoneOffset, 0) AS VARCHAR),'::INTEGER,',
               'Item.numItemCode::NUMERIC(18,0),COALESCE(TempLeadDays.numLeadDays,0)::NUMERIC(18,0),WI.numWarehouseItemID::NUMERIC(18,0),60::NUMERIC(18,0),',CAST(COALESCE(v_bitShowHistoricSales, false) AS VARCHAR),'::BOOLEAN,',CAST(COALESCE(v_numHistoricalAnalysisPattern, 0) AS VARCHAR),'::NUMERIC(18,0),'
               ,CAST(COALESCE(v_bitBasedOnLastYear, false) AS VARCHAR),'::BOOLEAN,',CAST(COALESCE(v_bitIncludeOpportunity, false) AS VARCHAR),'::BOOLEAN,',CAST(COALESCE(v_numOpportunityPercentComplete, 0) AS VARCHAR),'::NUMERIC(18,0),'
               ,CAST(COALESCE(v_tintDemandPlanBasedOn, 0) AS VARCHAR),'::SMALLINT) "',v_vcColumnName,
               '"');
            ELSEIF v_vcDbColumnName = 'vc90Days'
            then
				
               v_strColumns := coalesce(v_strColumns,'') || CONCAT(',fn_DemandForecastDaysDisplay(',CAST(COALESCE(v_numDomainID, 0) AS VARCHAR),'::NUMERIC(18,0),',CAST(COALESCE(v_ClientTimeZoneOffset, 0) AS VARCHAR),'::INTEGER,',
               'Item.numItemCode::NUMERIC(18,0),COALESCE(TempLeadDays.numLeadDays,0)::NUMERIC(18,0),WI.numWarehouseItemID::NUMERIC(18,0),90::NUMERIC(18,0),',CAST(COALESCE(v_bitShowHistoricSales, false) AS VARCHAR),'::BOOLEAN,',CAST(COALESCE(v_numHistoricalAnalysisPattern, 0) AS VARCHAR),'::NUMERIC(18,0),'
               ,CAST(COALESCE(v_bitBasedOnLastYear, false) AS VARCHAR),'::BOOLEAN,',CAST(COALESCE(v_bitIncludeOpportunity, false) AS VARCHAR),'::BOOLEAN,',CAST(COALESCE(v_numOpportunityPercentComplete, 0) AS VARCHAR),'::NUMERIC(18,0),'
               ,CAST(COALESCE(v_tintDemandPlanBasedOn, 0) AS VARCHAR),'::SMALLINT) "',v_vcColumnName,
               '"');
            ELSEIF v_vcDbColumnName = 'vc180Days'
            then
				
               v_strColumns := coalesce(v_strColumns,'') || CONCAT(',fn_DemandForecastDaysDisplay(',CAST(COALESCE(v_numDomainID, 0) AS VARCHAR),'::NUMERIC(18,0),',CAST(COALESCE(v_ClientTimeZoneOffset, 0) AS VARCHAR),'::INTEGER,',
               'Item.numItemCode::NUMERIC(18,0),COALESCE(TempLeadDays.numLeadDays,0)::NUMERIC(18,0),WI.numWarehouseItemID::NUMERIC(18,0),180::NUMERIC(18,0),',CAST(COALESCE(v_bitShowHistoricSales, false) AS VARCHAR),'::BOOLEAN,',CAST(COALESCE(v_numHistoricalAnalysisPattern, 0) AS VARCHAR),'::NUMERIC(18,0),'
               ,CAST(COALESCE(v_bitBasedOnLastYear, false) AS VARCHAR),'::BOOLEAN,',CAST(COALESCE(v_bitIncludeOpportunity, false) AS VARCHAR),'::BOOLEAN,',CAST(COALESCE(v_numOpportunityPercentComplete, 0) AS VARCHAR),'::NUMERIC(18,0),'
               ,CAST(COALESCE(v_tintDemandPlanBasedOn, 0) AS VARCHAR),'::SMALLINT) "',v_vcColumnName,
               '"');
            ELSEIF (v_vcDbColumnName = 'vcBuyUOM')
            then
				
               v_strColumns := coalesce(v_strColumns,'') || ',' || ' COALESCE((SELECT vcUnitName FROM UOM WHERE numUOMId = COALESCE(Item.numPurchaseUnit,0)),'''')' || ' "' || coalesce(v_vcColumnName,'') || '"';
            ELSEIF v_vcDbColumnName = 'numOnOrderReq'
            then
				
               v_strColumns := coalesce(v_strColumns,'') || ',' || ' (COALESCE(WI.numOnOrder,0) + COALESCE(WI.numReorder,0)) '  || ' "' || coalesce(v_vcColumnName,'') || '"';
            ELSEIF v_vcDbColumnName = 'numAvailable'
            then
				
               v_strColumns := coalesce(v_strColumns,'') || ',' || 'CONCAT(COALESCE(WI.numOnHand,0) + COALESCE(WI.numAllocation,0),'' ('',COALESCE(WI.numOnHand,0),'')'')'  || ' "' || coalesce(v_vcColumnName,'') || '"';
            ELSEIF v_vcDbColumnName = 'numTotalOnHand'
            then
				
               v_strColumns := coalesce(v_strColumns,'') || ',' || 'CONCAT(COALESCE((SELECT SUM(WIInner.numOnHand) + SUM(WIInner.numAllocation) FROM WarehouseItems WIInner WHERE WIInner.numItemID=Item.numItemCode),0),'' ('',COALESCE((SELECT SUM(WIInner.numOnHand) FROM WarehouseItems WIInner WHERE WIInner.numItemID=Item.numItemCode),0),'')'')'  || ' "' || coalesce(v_vcColumnName,'') || '"';
            ELSEIF v_vcDbColumnName = 'numBuildableQty'
            then
				
               v_strColumns := coalesce(v_strColumns,'') || CONCAT(',fn_GetAssemblyPossibleWOQty(Item.numItemCode,WI.numWareHouseID) "',v_vcColumnName,'"');
            ELSE
               v_strColumns := coalesce(v_strColumns,'') || ',' || coalesce(v_Prefix,'') || coalesce(v_vcDbColumnName,'')  || ' "' || coalesce(v_vcColumnName,'') || '"';
            end if;
         ELSEIF v_vcAssociatedControlType = 'SelectBox' or v_vcAssociatedControlType = 'ListBox'
         then
			
            IF v_vcDbColumnName = 'numVendorID' then
				
               v_strColumns := coalesce(v_strColumns,'') || ',' || ' V.numVendorID '  || ' "' || coalesce(v_vcColumnName,'') || '"';
            ELSEIF v_vcDbColumnName = 'ShipmentMethod'
            then
				
               v_strColumns := coalesce(v_strColumns,'') || ',' || ' '''' '  || ' "' || coalesce(v_vcColumnName,'') || '"';
            ELSE
               v_strColumns := coalesce(v_strColumns,'') || ',' || coalesce(v_Prefix,'') || coalesce(v_vcDbColumnName,'')  || ' "' || coalesce(v_vcColumnName,'') || '"';
            end if;
         ELSEIF v_vcAssociatedControlType = 'DateField'
         then
			
            IF v_vcDbColumnName = 'dtExpectedDelivery' then
				
               v_strColumns := coalesce(v_strColumns,'') || ',' || ' LOCALTIMESTAMP '  || ' "' || coalesce(v_vcColumnName,'') || '"';
            ELSEIF v_vcDbColumnName = 'ItemRequiredDate'
            then
				
               v_strColumns := coalesce(v_strColumns,'') || ',T1.dtReleaseDate "' || coalesce(v_vcColumnName,'') || '"';
            ELSE
               v_strColumns := coalesce(v_strColumns,'') || ',' || coalesce(v_Prefix,'') || coalesce(v_vcDbColumnName,'')  || ' "' || coalesce(v_vcColumnName,'') || '"';
            end if;
         end if;
      end if;
      select   tintOrder+1, vcDbColumnName, vcFieldName, vcAssociatedControlType, vcListItemType, numListID, vcLookBackTableName, bitCustomField, numFieldId, bitAllowSorting, bitAllowEdit, ListRelID INTO v_tintOrder,v_vcDbColumnName,v_vcFieldName,v_vcAssociatedControlType,v_vcListItemType,
      v_numListID,v_vcLookBackTableName,v_bitCustom,v_numFieldId,
      v_bitAllowSorting,v_bitAllowEdit,v_ListRelID 
	  FROM tt_TEMPFORM 
	  WHERE tintOrder > v_tintOrder -1   
	  ORDER BY tintOrder ASC 
	  LIMIT 1;
      
	  GET DIAGNOSTICS SWV_RowCount = ROW_COUNT;
      
	  IF SWV_RowCount = 0 then 
         v_tintOrder := 0;
      end if;
	  
   END LOOP;
	  
   v_StrSql := CONCAT('SELECT ',v_strColumns,' 
						FROM
							tt_TEMPITEMSDFEXECUTE T1
						INNER JOIN
							Item
						ON
							T1.numItemCode = Item.numItemCode
						LEFT JOIN
							Sales_process_List_Master SPLM
						ON
							Item.numBusinessProcessId = SPLM.Slp_Id
						INNER JOIN
							WarehouseItems WI
						ON
							Item.numItemCode=WI.numItemID
							AND T1.numWarehouseItemID=WI.numWarehouseItemID
						INNER JOIN
							Warehouses W
						ON
							WI.numWarehouseID = W.numWareHouseID
						LEFT JOIN
							Vendor V
						ON
							Item.numVendorID = V.numVendorID
							AND Item.numItemCode = V.numItemCode
						LEFT JOIN LATERAL
						(
							SELECT 
								numListValue AS numLeadDays
							FROM
								VendorShipmentMethod VSM
							WHERE
								VSM.numVendorID = V.numVendorID
							ORDER BY
								(CASE WHEN VSM.numWarehouseID=W.numWareHouseID THEN 1 ELSE 0 END) DESC, COALESCE(VSM.numWarehouseID,0) DESC, COALESCE(bitPrimary,false) DESC, bitPreferredMethod DESC, numListItemID ASC
					  		LIMIT 1
						) TempLeadDays ON true
						WHERE
							Item.numDomainID=',v_numDomainID,
   '
							AND 1 = (CASE 
										WHEN ',CAST(v_bitShowAllItems AS VARCHAR),' = true 
										THEN 1 
										ELSE 
											(CASE 
												WHEN ', 
   COALESCE(v_tintDemandPlanBasedOn,1),' = 2
												THEN (CASE 
														WHEN ((COALESCE(WI.numBackOrder,0) 
																- COALESCE((SELECT SUM(WIInner.numOnOrder) FROM WarehouseItems WIInner WHERE WIInner.numItemID=Item.numItemCode),0) 
																- COALESCE((SELECT SUM(COALESCE(WIInner.numOnHand,0) + COALESCE(WIInner.numAllocation,0)) FROM WarehouseItems WIInner WHERE WIInner.numItemID=Item.numItemCode AND WIInner.numWarehouseItemID <> WI.numWarehouseItemID AND WIInner.numWarehouseID = WI.numWarehouseID),0)) > 0 
																OR (COALESCE(numReorder,0) > 0 AND COALESCE((SELECT SUM(COALESCE(WIInner.numOnHand,0) + COALESCE(WIInner.numAllocation,0) + COALESCE(WIInner.numOnOrder,0)) FROM WarehouseItems WIInner WHERE WIInner.numItemID=Item.numItemCode),0) < COALESCE(WI.numReorder,0))) 
														THEN 1
														ELSE 0
													END)
												ELSE
													(CASE 
														WHEN 
															T1.numUnitHour > 0 
														THEN 1 
														ELSE 0 
													END)
											END)
									END)
						');

   IF POSITION('I.' IN v_vcRegularSearchCriteria) > 0 then
	
      v_vcRegularSearchCriteria := REPLACE(v_vcRegularSearchCriteria,'I.','Item.');
   end if;
   IF POSITION('WH.' IN v_vcRegularSearchCriteria) > 0 then
	
      v_vcRegularSearchCriteria := REPLACE(v_vcRegularSearchCriteria,'WH.','W.');
   end if;
	
   IF v_vcRegularSearchCriteria <> '' then
	
      v_StrSql := coalesce(v_StrSql,'') || ' AND ' || coalesce(v_vcRegularSearchCriteria,'');
   end if;
	  
   v_firstRec :=(v_CurrentPage::bigint -1)*v_PageSize::bigint;
   v_lastRec :=(v_CurrentPage::bigint*v_PageSize::bigint+1);

	-- EXECUTE FINAL QUERY
   v_strFinal := CONCAT(v_StrSql,' ORDER BY ',CASE WHEN v_columnName <> '' AND POSITION(v_columnName IN v_strColumns) > 0 THEN v_columnName ELSE 'Item.numItemCode' END,' LIMIT ',v_PageSize,
   ' OFFSET ',(v_CurrentPage::bigint -1)*v_PageSize::bigint);

   RAISE NOTICE '%',CAST(v_strFinal AS TEXT);
   
   OPEN SWV_RefCur FOR
   EXECUTE v_strFinal;
	
   OPEN SWV_RefCur2 FOR
   SELECT * FROM tt_TEMPFORM;

   UPDATE DemandForecast SET dtExecutionDate = LOCALTIMESTAMP+CAST(-v_ClientTimeZoneOffset || ' minute' as interval) WHERE numDFID = v_numDFID;

   OPEN SWV_RefCur3 FOR
   SELECT CAST(v_dtLastExecution+(CAST(COALESCE(-v_ClientTimeZoneOffset, 0) || ' minute' as interval)) AS VARCHAR(19)) AS dtLastExecution;

END;
$$;



