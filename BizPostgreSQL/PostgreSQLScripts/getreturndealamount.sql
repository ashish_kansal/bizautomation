DROP FUNCTION IF EXISTS GetReturnDealAmount;

CREATE OR REPLACE FUNCTION GetReturnDealAmount(v_numReturnHeaderID NUMERIC(18,0),
	v_tintMode SMALLINT)
RETURNS TABLE     
(    
   monAmount DECIMAL(20,5),
   monTotalTax DECIMAL(20,5),
   monTotalDiscount DECIMAL(20,5)  
) LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numDomainID  NUMERIC(18,0);
   v_monTotalDiscount  DECIMAL(20,5);
   v_monAmount  DECIMAL(20,5);
   v_monTotalTax  DECIMAL(20,5);
   v_tintReturnType  SMALLINT;
   v_numTaxItemID  NUMERIC(9,0);
   v_fltPercentage  DOUBLE PRECISION DEFAULT 0;
   v_tintTaxType  SMALLINT DEFAULT 1;
   v_numUnitHour  DOUBLE PRECISION DEFAULT 0;
   v_ItemAmount  DECIMAL(20,5) DEFAULT 0;
   v_numTaxID  NUMERIC(18,0);

   v_i  INTEGER DEFAULT 1;
   v_Count  INTEGER;
   v_numReturnItemID  NUMERIC;
   SWV_RowCount INTEGER;
BEGIN
   DROP TABLE IF EXISTS tt_GETRETURNDEALAMOUNT_RESULT;
   CREATE TEMPORARY TABLE IF NOT EXISTS  tt_GETRETURNDEALAMOUNT_RESULT
   (
      monAmount DECIMAL(20,5),
      monTotalTax DECIMAL(20,5),
      monTotalDiscount DECIMAL(20,5)
   );
   select   ReturnHeader.numDomainID, coalesce(ReturnHeader.monTotalDiscount,0), coalesce(ReturnHeader.monAmount,0), ReturnHeader.tintReturnType INTO v_numDomainID,v_monTotalDiscount,v_monAmount,v_tintReturnType FROM
   ReturnHeader WHERE
   numReturnHeaderID = v_numReturnHeaderID;
 
   IF v_tintMode = 5 OR v_tintMode = 6 OR v_tintMode = 7 OR v_tintMode = 8 OR (v_tintReturnType = 1 AND v_tintMode = 9) then
      v_monTotalTax := 0;
      select   SUM((CASE WHEN v_tintMode = 5 OR v_tintMode = 6 THEN numUnitHour ELSE numUnitHourReceived END)*(CASE WHEN numUnitHour = 0 THEN 0 ELSE CAST(coalesce(monTotAmount,0)/numUnitHour AS DECIMAL(20,5)) END)) INTO v_monAmount FROM
      ReturnItems WHERE
      numReturnHeaderID = v_numReturnHeaderID;
      BEGIN
         CREATE TEMP SEQUENCE tt_TEMPGetReturnDealAmount_seq INCREMENT BY 1 START WITH 1;
         EXCEPTION WHEN OTHERS THEN
            NULL;
      END;
      DROP TABLE IF EXISTS tt_TEMPGetReturnDealAmount CASCADE;
      CREATE TEMPORARY TABLE tt_TEMPGetReturnDealAmount
      (
         ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1),
         numTaxItemID NUMERIC(18,0),
         fltPercentage DOUBLE PRECISION,
         tintTaxType SMALLINT,
         numTaxID NUMERIC(18,0)
      );
      INSERT INTO tt_TEMPGetReturnDealAmount(numTaxItemID,
			fltPercentage,
			tintTaxType,
			numTaxID)
      SELECT
      numTaxItemID,
			fltPercentage,
			coalesce(tintTaxType,1),
			coalesce(numTaxID,0)
      FROM
      OpportunityMasterTaxItems
      WHERE
      numReturnHeaderID = v_numReturnHeaderID
      ORDER BY
      numTaxItemID;
      select   COUNT(*) INTO v_Count FROM tt_TEMPGetReturnDealAmount;
      WHILE v_i <= v_Count LOOP
         select   tt_TEMPGetReturnDealAmount.numTaxItemID, tt_TEMPGetReturnDealAmount.fltPercentage, coalesce(tt_TEMPGetReturnDealAmount.tintTaxType,1), coalesce(tt_TEMPGetReturnDealAmount.numTaxID,0) INTO v_numTaxItemID,v_fltPercentage,v_tintTaxType,v_numTaxID FROM
         tt_TEMPGetReturnDealAmount WHERE
         ID = v_i;
         IF coalesce(v_fltPercentage,0) > 0 then
            v_numReturnItemID := 0;
            v_ItemAmount := 0;
            select   numReturnItemID, (CASE WHEN v_tintMode = 5 OR v_tintMode = 6 THEN numUnitHour ELSE numUnitHourReceived END)*fn_UOMConversion(coalesce(I.numBaseUnit,0),I.numItemCode,v_numDomainID,coalesce(ReturnItems.numUOMId,0)), (CASE WHEN v_tintMode = 5 OR v_tintMode = 6 THEN numUnitHour ELSE numUnitHourReceived END)*(CASE WHEN numUnitHour = 0 THEN 0 ELSE CAST(coalesce(monTotAmount,0)/numUnitHour AS DECIMAL(18,4)) END) INTO v_numReturnItemID,v_numUnitHour,v_ItemAmount FROM
            ReturnItems
            INNER JOIN
            Item I
            ON
            ReturnItems.numItemCode = I.numItemCode WHERE
            numReturnHeaderID = v_numReturnHeaderID   ORDER BY
            numReturnItemID LIMIT 1;
            WHILE v_numReturnItemID > 0 LOOP
               IF(SELECT COUNT(*) from OpportunityItemsTaxItems where numReturnItemID = v_numReturnItemID and numTaxItemID = v_numTaxItemID and coalesce(numTaxID,0) = coalesce(v_numTaxID,0)) > 0 then
					
                  v_monTotalTax := v_monTotalTax+(CASE WHEN v_tintTaxType = 2 THEN(coalesce(v_fltPercentage,0)*coalesce(v_numUnitHour,0)) ELSE v_fltPercentage*v_ItemAmount/100 END);
               end if;
               select   numReturnItemID, (CASE WHEN v_tintMode = 5 OR v_tintMode = 6 THEN numUnitHour ELSE numUnitHourReceived END)*fn_UOMConversion(coalesce(I.numBaseUnit,0),I.numItemCode,v_numDomainID,coalesce(ReturnItems.numUOMId,0)), (CASE WHEN v_tintMode = 5 OR v_tintMode = 6 THEN numUnitHour ELSE numUnitHourReceived END)*(CASE WHEN numUnitHour = 0 THEN 0 ELSE CAST(coalesce(monTotAmount,0)/numUnitHour AS DECIMAL(18,4)) END) INTO v_numReturnItemID,v_numUnitHour,v_ItemAmount FROM
               ReturnItems
               INNER JOIN
               Item I
               ON
               ReturnItems.numItemCode = I.numItemCode WHERE
               numReturnHeaderID = v_numReturnHeaderID  and numReturnItemID > v_numReturnItemID   ORDER BY
               numReturnItemID LIMIT 1;
               GET DIAGNOSTICS SWV_RowCount = ROW_COUNT;
               IF SWV_RowCount = 0 then 
                  v_numReturnItemID := 0;
               end if;
            END LOOP;
         end if;
         v_i := v_i::bigint+1;
      END LOOP;
   end if;
 
 
   INSERT INTO tt_GETRETURNDEALAMOUNT_RESULT
   SELECT coalesce(v_monAmount,0),coalesce(v_monTotalTax,0),coalesce(v_monTotalDiscount,0);
 
 RETURN QUERY (SELECT * FROM tt_GETRETURNDEALAMOUNT_RESULT);
END; $$;

