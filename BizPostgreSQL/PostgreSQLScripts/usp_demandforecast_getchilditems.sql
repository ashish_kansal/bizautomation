-- Stored procedure definition script USP_DemandForecast_GetChildItems for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_DemandForecast_GetChildItems(v_numDomainID NUMERIC(18,0),
	v_numDFID NUMERIC(18,0),
	v_numOppID NUMERIC(18,0),
	v_numOppItemID NUMERIC(18,0),
	v_numItemCode NUMERIC(18,0),
	v_numQty DOUBLE PRECISION,
	v_bitKit BOOLEAN,
	v_bitAssembly BOOLEAN, INOUT SWV_RefCur refcursor)
LANGUAGE plpgsql
   AS $$
BEGIN
   IF v_bitKit = true then
	
      open SWV_RefCur for
      with recursive CTE(numOppChildItemID,numOppID,numOppItemID,numItemCode,bitKitParent,numItemGroup,
      numWarehouseID,numWarehouseItemID,numQty) AS(SELECT
      numOppChildItemID AS numOppChildItemID
				,OKI.numOppId AS numOppID
				,OKI.numOppItemID AS numOppItemID
				,numChildItemID AS numItemCode
				,I.bitKitParent AS bitKitParent
				,I.numItemGroup AS numItemGroup
				,WI.numWareHouseID AS numWarehouseID
				,WI.numWareHouseItemID AS numWarehouseItemID
				,v_numQty*numQtyItemsReq_Orig AS numQty
      FROM
      OpportunityKitItems OKI
      INNER JOIN
      Item I
      ON
      OKI.numChildItemID = I.numItemCode
      INNER JOIN
      WareHouseItems WI
      ON
      OKI.numWareHouseItemId = WI.numWareHouseItemID
      WHERE
      OKI.numOppId = v_numOppID
      AND OKI.numOppItemID = v_numOppItemID
      UNION ALL
      SELECT
      OKCI.numOppKitChildItemID AS numOppChildItemID
				,OKCI.numOppId AS numOppID
				,OKCI.numOppItemID AS numOppItemID
				,OKCI.numItemID AS numItemCode
				,I.bitKitParent AS bitKitParent
				,I.numItemGroup AS numItemGroup
				,WI.numWareHouseID AS numWarehouseID
				,WI.numWareHouseItemID AS numWarehouseItemID
				,c.numQty*numQtyItemsReq_Orig AS numQty
      FROM
      OpportunityKitChildItems OKCI
      INNER JOIN
      CTE c
      ON
      OKCI.numOppChildItemID = c.numOppChildItemID
      INNER JOIN
      Item I
      ON
      OKCI.numItemID = I.numItemCode
      INNER JOIN
      WareHouseItems WI
      ON
      OKCI.numWareHouseItemId = WI.numWareHouseItemID)
      SELECT
      numItemCode
			,(CASE
      WHEN coalesce(c.numItemGroup,0) > 0 AND LENGTH(fn_GetAttributes(numWareHouseItemID,0::BOOLEAN)) > 0
      THEN(CASE
         WHEN EXISTS(SELECT numWareHouseItemID FROM WareHouseItems WIInner WHERE WIInner.numDomainID = v_numDomainID AND  WIInner.numItemID = c.numItemCode AND WIInner.numWareHouseID = c.numWareHouseID AND WIInner.numWareHouseID = -1 AND  fn_GetAttributes(WIInner.numWareHouseItemID,0::BOOLEAN) = fn_GetAttributes(c.numWareHouseItemID,0::BOOLEAN))
         THEN(SELECT  numWareHouseItemID FROM WareHouseItems WIInner WHERE WIInner.numDomainID = v_numDomainID AND  WIInner.numItemID = c.numItemCode AND WIInner.numWareHouseID = c.numWareHouseID AND WIInner.numWareHouseID = -1 AND  fn_GetAttributes(WIInner.numWareHouseItemID,0::BOOLEAN) = fn_GetAttributes(c.numWareHouseItemID,0::BOOLEAN) LIMIT 1)
         ELSE
            c.numWareHouseItemID
         END)
      ELSE(SELECT  numWareHouseItemID FROM WareHouseItems WHERE numDomainID = v_numDomainID AND numItemID = c.numItemCode AND numWareHouseID = c.numWareHouseID AND numWLocationID = -1 LIMIT 1)
      END) AS numWarehouseItemID
			,CEIL(CAST(numQty AS DOUBLE PRECISION)) AS numQty
      FROM
      CTE c
      WHERE
      coalesce(bitKitParent,false) = false;
   ELSEIF v_bitAssembly = true
   then
	
		-- GET ASSEMBLY CHILD ITEMS
      open SWV_RefCur for
      with recursive CTEAssembly(numItemCode,bitAssembly,numItemGroup,numWareHouseID,numWarehouseItemID, 
      numQty) AS(SELECT
      numChildItemID AS numItemCode,
				I.bitAssembly AS bitAssembly,
				I.numItemGroup AS numItemGroup,
				coalesce(WI.numWareHouseID,0) AS numWareHouseID,
				coalesce(WI.numWareHouseItemID,0) AS numWarehouseItemID,
				CAST(DTL.numQtyItemsReq*v_numQty AS DOUBLE PRECISION) AS numQty
      FROM
      ItemDetails DTL
      INNER JOIN
      Item I
      ON
      DTL.numChildItemID = I.numItemCode
      INNER JOIN
      WareHouseItems WI
      ON
      DTL.numWareHouseItemId = WI.numWareHouseItemID
      WHERE
      numItemKitID = v_numItemCode
      UNION ALL
      SELECT
      I.numItemCode AS numItemCode,
				I.bitAssembly AS bitAssembly,
				I.numItemGroup AS numItemGroup,
				coalesce(WI.numWareHouseID,0) AS numWareHouseID,
				coalesce(WI.numWareHouseItemID,0) AS numWarehouseItemID,
				CAST(DTL.numQtyItemsReq*c.numQty AS DOUBLE PRECISION) AS numQty
      FROM
      ItemDetails DTL
      INNER JOIN
      CTEAssembly c
      ON
      DTL.numItemKitID = c.numItemCode
      INNER JOIN
      Item I
      ON
      DTL.numChildItemID = I.numItemCode
      INNER JOIN
      WareHouseItems WI
      ON
      DTL.numWareHouseItemId = WI.numWareHouseItemID
      WHERE
      DTL.numChildItemID != c.numItemCode)
      SELECT
      numItemCode,
			(CASE
      WHEN coalesce(c.numItemGroup,0) > 0 AND LENGTH(fn_GetAttributes(numWareHouseItemID,0::BOOLEAN)) > 0
      THEN(CASE
         WHEN EXISTS(SELECT numWareHouseItemID FROM WareHouseItems WIInner WHERE WIInner.numDomainID = v_numDomainID AND  WIInner.numItemID = c.numItemCode AND WIInner.numWareHouseID = c.numWareHouseID AND WIInner.numWareHouseID = -1 AND  fn_GetAttributes(WIInner.numWareHouseItemID,0::BOOLEAN) = fn_GetAttributes(c.numWareHouseItemID,0::BOOLEAN))
         THEN(SELECT  numWareHouseItemID FROM WareHouseItems WIInner WHERE WIInner.numDomainID = v_numDomainID AND  WIInner.numItemID = c.numItemCode AND WIInner.numWareHouseID = c.numWareHouseID AND WIInner.numWareHouseID = -1 AND  fn_GetAttributes(WIInner.numWareHouseItemID,0::BOOLEAN) = fn_GetAttributes(c.numWareHouseItemID,0::BOOLEAN) LIMIT 1)
         ELSE
            c.numWareHouseItemID
         END)
      ELSE(SELECT  numWareHouseItemID FROM WareHouseItems WHERE numDomainID = v_numDomainID AND numItemID = c.numItemCode AND numWareHouseID = c.numWareHouseID AND numWLocationID = -1 LIMIT 1)
      END) AS numWarehouseItemID
			,CEIL(CAST(numQty AS DOUBLE PRECISION)) AS numQty
      FROM
      CTEAssembly c
      WHERE
      coalesce(bitAssembly,false) = false;
   end if;
   RETURN;
END; $$;
/****** Object:  StoredProcedure [dbo].[USP_EcommerceSettings]    Script Date: 03/25/2009 16:46:21 ******/



