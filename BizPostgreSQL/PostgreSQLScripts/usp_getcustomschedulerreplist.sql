-- Stored procedure definition script USP_getCustomSchedulerRepList for PostgreSQL
CREATE OR REPLACE FUNCTION USP_getCustomSchedulerRepList(v_numReportId NUMERIC(9,0),  
v_numDomainId NUMERIC(9,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for select
   cast(numScheduleId as VARCHAR(255)),
cast(varScheduleName as VARCHAR(255)) ,
case when chrIntervalType = 'D'
   then 'Day '
   when  chrIntervalType = 'M'
   then 'Monthly '
   when  chrIntervalType = 'Y'
   then 'Yearly '
   end
   || ' ,' ||
   CAST(dtStartdate AS VARCHAR(11))
   || ' ,' ||
   case when bitEndTransactionType = true
   then CAST(numnoTransaction AS VARCHAR(20))
   when bitEndTransactionType = false
   then CAST(dtEnddate AS VARCHAR(11))
   end
   as Interaval,
vcReportName,vcReportDescription
   from CustRptScheduler CRS
   join CustomReport CR on CRS.numReportId = CR.numCustomReportId
   where CRS.numReportId = v_numReportId and CRS.numDomainID = v_numDomainId;
END; $$;  
--select * from CustRptScheduler












