-- Stored procedure definition script USP_SaveCategoryDisplayOrder for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_SaveCategoryDisplayOrder(v_strItems TEXT DEFAULT null)
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_hDocItem  INTEGER;
BEGIN
   IF SUBSTR(CAST(v_strItems AS VARCHAR(10)),1,10) <> '' then
        
      UPDATE Category SET intDisplayOrder = X.intDisplayOrder
      FROM 
	  XMLTABLE
			(
				'NewDataSet/CategoryOrder'
				PASSING 
					CAST(v_strItems AS XML)
				COLUMNS
					id FOR ORDINALITY,
					numCategoryID NUMERIC(9,0) PATH 'numCategoryID',
					intDisplayOrder INTEGER PATH 'intDisplayOrder'
			) AS X 
      WHERE Category.numCategoryID = X.numCategoryID;

   end if;
   RETURN;
END; $$;
    
-- =============================================  
-- Author:  <Ajit Kumar Singh>  
-- Create date: <Tuesday, September 02, 2008>  
-- Description: <This procedure is used for Store Module's Sorting Order according to Doamin ID>  
-- =============================================  



