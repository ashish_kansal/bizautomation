-- Stored procedure definition script USP_UpdateBizAutomationTheme for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_UpdateBizAutomationTheme(v_numDomainID NUMERIC(9,0) DEFAULT 0,  
v_vcLogoForBizTheme VARCHAR(500) DEFAULT NULL,
v_bitLogoAppliedToBizTheme BOOLEAN DEFAULT NULL,
v_vcThemeClass VARCHAR(500) DEFAULT NULL,  
v_vcLogoForLoginBizTheme VARCHAR(500) DEFAULT NULL,
v_bitLogoAppliedToLoginBizTheme BOOLEAN DEFAULT NULL,
v_vcLoginURL VARCHAR(500) DEFAULT NULL)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   IF((Select LENGTH(v_vcLogoForBizTheme)) > 0) then
	
      update Domain Set vcLogoForBizTheme = v_vcLogoForBizTheme
      Where numDomainId = v_numDomainID;
   end if;
   IF((Select LENGTH(v_vcLogoForLoginBizTheme)) > 0) then
	
      update Domain Set vcLogoForLoginBizTheme = v_vcLogoForLoginBizTheme
      Where numDomainId = v_numDomainID;
   end if;
   IF((LENGTH(v_vcLoginURL)) > 0) then
	
      IF((SELECT  COUNT(*) FROM  Domain  WHERE  vcLoginURL = v_vcLoginURL AND numDomainId <> v_numDomainID) = 0) then
		
         update Domain Set vcLoginURL = v_vcLoginURL
         Where numDomainId = v_numDomainID;
      end if;
   end if;
   Update Domain Set	bitLogoAppliedToBizTheme = v_bitLogoAppliedToBizTheme,bitLogoAppliedToLoginBizTheme = v_bitLogoAppliedToLoginBizTheme,
   vcThemeClass = v_vcThemeClass
   Where numDomainId = v_numDomainID;
   RETURN;
END; $$;


