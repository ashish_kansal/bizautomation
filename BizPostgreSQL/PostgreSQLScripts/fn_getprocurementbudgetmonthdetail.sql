-- Function definition script fn_GetProcurementBudgetMonthDetail for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION fn_GetProcurementBudgetMonthDetail(v_numProcurementId NUMERIC(9,0),v_tintMonth SMALLINT,v_intYear NUMERIC(9,0),v_numDomainId NUMERIC(9,0),v_numItemGroupId NUMERIC(9,0) DEFAULT 0)
RETURNS VARCHAR(10) LANGUAGE plpgsql
   AS $$
   DECLARE
   v_monAmount  VARCHAR(10);
BEGIN
   select   PBD.monAmount INTO v_monAmount From ProcurementBudgetMaster PBM
   inner join  ProcurementBudgetDetails PBD on PBM.numProcurementBudgetId = PBD.numProcurementId Where PBM.numDomainID = v_numDomainId And PBD.numProcurementId = v_numProcurementId And PBD.numItemGroupId = v_numItemGroupId
   And PBD.tintMonth = v_tintMonth And PBD.intYear = v_intYear;        
   if v_monAmount = '0.00' then 
      v_monAmount := '';
   end if;        
   Return v_monAmount;
END; $$;

