DROP FUNCTION IF EXISTS USP_MirrorOPPBizDocItems;

CREATE OR REPLACE FUNCTION USP_MirrorOPPBizDocItems
(
	v_numOppId NUMERIC(9,0) DEFAULT NULL,
    v_numDomainID NUMERIC(9,0) DEFAULT 0, 
	INOUT SWV_RefCur refcursor default null, 
	INOUT SWV_RefCur2 refcursor default null
)
LANGUAGE plpgsql
AS $$
	DECLARE
	v_DivisionID  NUMERIC(9,0);      
	v_tintTaxOperator  SMALLINT;   
	v_tintType  SMALLINT;   
	v_tintOppStatus  SMALLINT;    
	v_tintOppType  SMALLINT;   
	v_numBizDocTempID  NUMERIC(9,0);
	v_DecimalPoint  SMALLINT;
	v_tintCommitAllocation  SMALLINT;
	v_strSQL TEXT;                                                                        
	v_strSQL1 TEXT;                                       
	v_strSQLCusFields TEXT;                      
	v_strSQLEmpFlds TEXT;                                                                         
	v_intRowNum INTEGER;                                                  
	v_numFldID VARCHAR(15);                                          
	v_vcFldname VARCHAR(50);                                            
	v_numBizDocId NUMERIC(9,0);
	v_strSQLUpdate TEXT;
	v_vcTaxName VARCHAR(100);
	v_numTaxItemID NUMERIC(9,0);
	v_vcDbColumnName VARCHAR(50);
	SWV_ExecDyn VARCHAR(5000);
	SWV_RowCount INTEGER;
BEGIN
	v_numBizDocTempID := 0;
                                                                                                                                                                                                                                                                                                                                          
	SELECT 
		numDivisionId
		,tintTaxOperator
		,tintopptype
		,tintoppstatus
		,tintopptype
		,coalesce(numOppBizDocTempID,0) 
	INTO 
		v_DivisionID,v_tintTaxOperator,v_tintType,v_tintOppStatus,v_tintOppType,v_numBizDocTempID 
	FROM 
		OpportunityMaster 
	WHERE 
		numOppId = v_numOppId;    
                                                                                                                                                                          
	--Decimal points to show in bizdoc 
	SELECT 
		tintDecimalPoints
		,coalesce(tintCommitAllocation,1) 
	INTO
		v_DecimalPoint,v_tintCommitAllocation 
	FROM
		Domain 
	WHERE
		numDomainId = v_numDomainID;
                                                                                          
	v_strSQLCusFields := '';                                                                                          
	v_strSQLEmpFlds := '';                                                                                          
                                                   
	IF v_tintOppType = 1 AND v_tintOppStatus = 0 then
		SELECT numListItemID INTO v_numBizDocId FROM Listdetails WHERE numListID = 27 AND vcData = 'Sales Opportunity' AND constFlag = true;
	ELSEIF v_tintOppType = 1 AND v_tintOppStatus = 1 then
		SELECT numListItemID INTO v_numBizDocId FROM Listdetails WHERE numListID = 27 AND vcData = 'Sales Order' AND constFlag = true;
	ELSEIF v_tintOppType = 2 AND v_tintOppStatus = 0 then
		SELECT numListItemID INTO v_numBizDocId FROM Listdetails WHERE numListID = 27 AND vcData = 'Purchase Opportunity' AND constFlag = true;
	ELSEIF v_tintOppType = 2 AND v_tintOppStatus = 1 then
		SELECT numListItemID INTO v_numBizDocId FROM Listdetails WHERE numListID = 27 AND vcData = 'Purchase Order' AND constFlag = true;
	END IF;
                                                                              
	IF v_tintType = 1 then
		v_tintType := 7;
	ELSE
		v_tintType := 8;
	end if;                                                                                      
                                                                                
	DROP TABLE IF EXISTS tt_TEMP1 CASCADE;
	CREATE TEMPORARY TABLE tt_TEMP1 ON COMMIT DROP AS
    SELECT 
		* 
	FROM
	(
		SELECT
			Opp.vcItemName AS vcItemName,
            0 AS OppBizDocItemID,
			(CASE
			  WHEN OM.tintopptype = 1 AND OM.tintoppstatus = 1
			  THEN(CASE
				 WHEN (I.charItemType = 'P' AND coalesce(Opp.bitDropShip,false) = false AND coalesce(I.bitAsset,false) = false)
				 THEN
					CAST(CheckOrderItemInventoryStatus(Opp.numItemCode,Opp.numUnitHour,Opp.numoppitemtCode,Opp.numWarehouseItmsID,(CASE WHEN coalesce(bitKitParent,false) = true AND coalesce(bitAssembly,false) = true THEN false WHEN coalesce(bitKitParent,false) = true THEN true ELSE false END)) AS VARCHAR(30))
				 ELSE ''
				 END)
			  ELSE ''
			  END) AS vcBackordered,
			coalesce(WI.numBackOrder,0) AS numBackOrder,
            CASE WHEN charitemType = 'P' THEN 'Product'
			  WHEN charitemType = 'S' THEN 'Service'
			  END AS charitemType,
            CASE WHEN charitemType = 'P' THEN 'Inventory'
			WHEN charitemType = 'N' THEN 'Non-Inventory'
			WHEN charitemType = 'S' THEN 'Service'
			END AS vcItemType,
			CAST(Opp.vcItemDesc AS VARCHAR(2500)) AS txtItemDesc,
			Opp.numUnitHour AS numOrgTotalUnitHour,
			fn_UOMConversion(coalesce(I.numBaseUnit,0),I.numItemCode,v_numDomainID,coalesce(Opp.numUOMId,0))*Opp.numUnitHour AS numTotalUnitHour,
            fn_UOMConversion(coalesce(I.numBaseUnit,0),I.numItemCode,v_numDomainID,coalesce(Opp.numUOMId,0))*Opp.numUnitHour AS numUnitHour,
            CAST((fn_UOMConversion(coalesce(Opp.numUOMId,0),I.numItemCode,v_numDomainID,coalesce(I.numBaseUnit,0))*Opp.monPrice) AS DECIMAL(20,5)) AS monPrice,
            CAST(Opp.monTotAmount AS DECIMAL(20,5)) AS Amount,
            Opp.monTotAmount/*Fo calculating sum*/,
            coalesce(CAST(I.monListPrice AS VARCHAR(30)),cast(0 as TEXT)) AS monListPrice,
            I.numItemCode AS ItemCode,
            Opp.numoppitemtCode,
            L.vcData AS numItemClassification,
            CASE WHEN bitTaxable = false THEN 'No'
			ELSE 'Yes'
			END AS Taxable,
            numIncomeChartAcntId AS itemIncomeAccount,
            'NI' AS ItemType,
            0 AS numJournalId,
            0 AS numTransactionId,
            coalesce(Opp.vcModelID,'') AS vcModelID,
            (CASE WHEN charitemType = 'P' THEN USP_GetAttributes(Opp.numWarehouseItmsID,bitSerialized) ELSE coalesce(Opp.vcAttributes,'') END) AS vcAttributes,
            coalesce(vcPartNo,'') AS vcPartNo,
           (CASE WHEN coalesce(Opp.bitMarkupDiscount,false) = true
			THEN 0
			ELSE(coalesce(Opp.monTotAmtBefDiscount,Opp.monTotAmount) -Opp.monTotAmount)
			END)
			AS DiscAmt,
			(CASE WHEN Opp.numUnitHour = 0 THEN 0 ELSE CAST((fn_UOMConversion(coalesce(Opp.numUOMId,0),I.numItemCode,v_numDomainID,coalesce(I.numBaseUnit,0))*(coalesce(Opp.monTotAmount,0)/Opp.numUnitHour)) AS DECIMAL(20,5)) END) As monUnitSalePrice,
            coalesce(Opp.vcNotes,'') AS vcNotes,
            '' AS vcTrackingNo,
            '' AS vcShippingMethod,
            0 AS monShipCost,
            NULL AS dtDeliveryDate,
            Opp.numWarehouseItmsID,
            coalesce(u.vcUnitName,'') AS numUOMId,
            coalesce(Opp.vcManufacturer,'') AS vcManufacturer,
            coalesce(V.monCost,0) AS VendorCost,
            Opp.vcPathForTImage,
            I.fltLength,
            I.fltWidth,
            I.fltHeight,
            I.fltWeight,
            coalesce(I.numVendorID,0) AS numVendorID,
            Opp.bitDropShip AS DropShip,
            coalesce((SELECT 
						string_agg(vcSerialNo || CASE WHEN coalesce(I.bitLotNo,false) = true THEN ' (' || CAST(oppI.numQty AS VARCHAR(15)) || ')' ELSE '' END,', ' ORDER BY vcSerialNo) 
					FROM 
						OppWarehouseSerializedItem oppI
					JOIN 
						WareHouseItmsDTL whi 
					ON 
						oppI.numWarehouseItmsDTLID = whi.numWareHouseItmsDTLID
					WHERE 
						oppI.numOppID = Opp.numOppId
						AND oppI.numOppItemID = Opp.numoppitemtCode
					),'') AS SerialLotNo,
            coalesce(Opp.numUOMId,0) AS numUOM,
            coalesce(u.vcUnitName,'') AS vcUOMName,
            fn_UOMConversion(Opp.numUOMId,Opp.numItemCode,v_numDomainID,NULL::NUMERIC) AS UOMConversionFactor,
            coalesce(Opp.numSOVendorId,0) AS numSOVendorId,
            NULL AS dtRentalStartDate,
            NULL AS dtRentalReturnDate,
            coalesce(W.vcWareHouse,'') AS Warehouse,
            CASE WHEN charitemType = 'P' AND coalesce(numItemGroup,0) > 0 AND coalesce(bitMatrix,false) = false THEN coalesce(WI.vcWHSKU,I.vcSKU) ELSE I.vcSKU END AS vcSKU,
            CASE WHEN charitemType = 'P' AND coalesce(numItemGroup,0) > 0 AND coalesce(bitMatrix,false) = false THEN coalesce(WI.vcBarCode,I.numBarCodeId) ELSE I.numBarCodeId END AS vcBarcode,
			CASE WHEN charitemType = 'P' AND coalesce(numItemGroup,0) > 0 AND coalesce(bitMatrix,false) = false THEN coalesce(WI.vcBarCode,I.numBarCodeId) ELSE I.numBarCodeId END AS numBarCodeId,
            coalesce(Opp.numClassID,0) AS numClassID,
            coalesce(Opp.numProjectID,0) AS numProjectID,
            coalesce(I.numShipClass,0) AS numShipClass,0 AS numOppBizDocID
			,WL.vcLocation AS vcLocation
			,coalesce(bitSerialized,false) AS bitSerialized,coalesce(bitLotNo,false) AS bitLotNo,
            Opp.numOppId,Opp.numUnitHour AS numUnitHourOrig,coalesce(I.numItemCode,0) AS numItemCode,
			coalesce(I.bitKitParent,false) AS bitKitParent,
			0 AS numOppChildItemID,
			0 AS numOppKitChildItemID,
			false AS bitChildItem,
			0 AS tintLevel,
			coalesce(Opp.vcPromotionDetail,'') AS vcPromotionDetail,
			(CASE WHEN coalesce(WI.numWareHouseItemID,0) > 0 THEN W.vcWareHouse ELSE '' END) AS vcWarehouse,
			GetOrderAssemblyKitInclusionDetails(Opp.numOppId,Opp.numoppitemtCode,Opp.numUnitHour,v_tintCommitAllocation,true) AS vcInclusionDetails,
			Opp.numSortOrder
			,CAST(coalesce(Opp.numCost,0) AS DECIMAL(20,5)) AS numCost, numOnHand
			,FormatedDateFromDate(Opp.ItemReleaseDate,v_numDomainID) AS dtReleaseDate
			,(CASE WHEN I.bitKitParent = true THEN coalesce(fn_GetKitAvailabilityByWarehouse(I.numItemCode,W.numWareHouseID),0) ELSE(coalesce(numOnHand,0)+coalesce(numAllocation,0)) END) AS numAvailable
			,CPN.CustomerPartNo
			,coalesce(Opp.bitWinningBid,false) AS bitWinningBid
			,coalesce(Opp.numParentOppItemID,0) AS numParentOppItemID
			,coalesce(Opp.numUnitHour,0) AS numRemainingQty
			,FormatedDateFromDate(Opp.ItemRequiredDate,v_numDomainID) AS ItemRequiredDate
		FROM
		OpportunityItems Opp
		LEFT JOIN
		Item I
		ON
		Opp.numItemCode = I.numItemCode
		LEFT JOIN
		Listdetails L
		ON
		I.numItemClassification = L.numListItemID
		LEFT JOIN
		Vendor V
		ON
		V.numVendorID = v_DivisionID
		AND V.numItemCode = Opp.numItemCode
		LEFT JOIN
		UOM u
		ON
		u.numUOMId = Opp.numUOMId
		LEFT JOIN
		WareHouseItems WI
		ON
		Opp.numWarehouseItmsID = WI.numWareHouseItemID
		AND WI.numDomainID = v_numDomainID
		LEFT JOIN
		Warehouses W
		ON
		WI.numDomainID = W.numDomainID
		AND WI.numWareHouseID = W.numWareHouseID
		LEFT JOIN
		WarehouseLocation WL
		ON
		WL.numWLocationID = WI.numWLocationID
		LEFT JOIN
		OpportunityMaster OM
		ON
		OM.numOppId = Opp.numOppId
		LEFT JOIN
		DivisionMaster DM
		ON
		OM.numDivisionId = DM.numDivisionID
		LEFT JOIN
		CustomerPartNumber CPN
		ON
		Opp.numItemCode = CPN.numItemCode AND CPN.numDomainID = v_numDomainID AND CPN.numCompanyId = DM.numCompanyID
		WHERE
		Opp.numOppId = v_numOppId
		AND (bitShowDeptItem IS NULL OR bitShowDeptItem = false)
	) X
	ORDER BY
	numSortOrder;

	 IF v_DecimalPoint = 1 then
      UPDATE tt_TEMP1 SET monPrice = CAST(monPrice AS DECIMAL(18,1)),Amount = CAST(Amount AS DECIMAL(18,1));
	end if;
	IF v_DecimalPoint = 2 then    
      UPDATE tt_TEMP1 SET monPrice = CAST(monPrice AS DECIMAL(18,2)),Amount = CAST(Amount AS DECIMAL(18,2));
	end if;
	IF v_DecimalPoint = 3 then    
      UPDATE tt_TEMP1 SET monPrice = CAST(monPrice AS DECIMAL(18,3)),Amount = CAST(Amount AS DECIMAL(18,3));
	end if;
	IF v_DecimalPoint = 4 then    
      UPDATE tt_TEMP1 SET monPrice = CAST(monPrice AS DECIMAL(18,4)),Amount = CAST(Amount AS DECIMAL(18,4));
	end if;

	v_strSQLUpdate := '';
   
   EXECUTE 'ALTER TABLE tt_TEMP1 add TotalTax DECIMAL(20,5)';
   EXECUTE 'ALTER TABLE tt_TEMP1 add CRV DECIMAL(20,5)';

   IF v_tintOppType = 1 AND v_tintTaxOperator = 1 then --add Sales tax
	
      v_strSQLUpdate := coalesce(v_strSQLUpdate,'')
      || ',TotalTax = fn_CalBizDocTaxAmt('
      || SUBSTR(CAST(v_numDomainID AS VARCHAR(20)),1,20) || ',0,'
      || SUBSTR(CAST(v_numOppId AS VARCHAR(20)),1,20) || ',' ||
      'numoppitemtCode,CAST(1 AS SMALLINT),Amount,numUnitHour)';
      v_strSQLUpdate := coalesce(v_strSQLUpdate,'')
      || ',CRV = fn_CalBizDocTaxAmt('
      || SUBSTR(CAST(v_numDomainID AS VARCHAR(20)),1,20) || ',1,'
      || SUBSTR(CAST(v_numOppId AS VARCHAR(20)),1,20) || ',' ||
      'numoppitemtCode,CAST(1 AS SMALLINT),Amount,numUnitHour)';
   ELSEIF v_tintOppType = 1 AND v_tintTaxOperator = 2
   then -- remove sales tax
	
      v_strSQLUpdate := coalesce(v_strSQLUpdate,'') || ', TotalTax = 0';
      v_strSQLUpdate := coalesce(v_strSQLUpdate,'') || ',CRV = 0';
   ELSEIF v_tintOppType = 1
   then
	
      v_strSQLUpdate := coalesce(v_strSQLUpdate,'')
      || ',TotalTax = fn_CalBizDocTaxAmt('
      || SUBSTR(CAST(v_numDomainID AS VARCHAR(20)),1,20) || ',0,'
      || SUBSTR(CAST(v_numOppId AS VARCHAR(20)),1,20) || ',' ||
      'numoppitemtCode,CAST(1 AS SMALLINT),Amount,numUnitHour)';
      v_strSQLUpdate := coalesce(v_strSQLUpdate,'')
      || ', CRV = fn_CalBizDocTaxAmt('
      || SUBSTR(CAST(v_numDomainID AS VARCHAR(20)),1,20) || ',1,'
      || SUBSTR(CAST(v_numOppId AS VARCHAR(20)),1,20) || ',' ||
      'numoppitemtCode,CAST(1 AS SMALLINT),Amount,numUnitHour)';
   ELSE
      v_strSQLUpdate := coalesce(v_strSQLUpdate,'') || ', TotalTax = 0';
      v_strSQLUpdate := coalesce(v_strSQLUpdate,'') || ',CRV = 0';
   end if;

   v_numTaxItemID := 0;
   select   vcTaxName, numTaxItemID INTO v_vcTaxName,v_numTaxItemID FROM    TaxItems WHERE   numDomainID = v_numDomainID    LIMIT 1;


   WHILE v_numTaxItemID > 0 LOOP
      SWV_ExecDyn := 'alter table tt_TEMP1 add "' || coalesce(v_vcTaxName,'') || '" DECIMAL(20,5)';
      EXECUTE SWV_ExecDyn;
      IF v_tintOppType = 1 then
         v_strSQLUpdate := coalesce(v_strSQLUpdate,'') || ',"' || coalesce(v_vcTaxName,'')
         || '" = fn_CalBizDocTaxAmt('
         || SUBSTR(CAST(v_numDomainID AS VARCHAR(20)),1,20) || ','
         || SUBSTR(CAST(v_numTaxItemID AS VARCHAR(20)),1,20) || ','
         || SUBSTR(CAST(v_numOppId AS VARCHAR(20)),1,20) || ',' ||
         'numoppitemtCode,CAST(1 AS SMALLINT),Amount,numUnitHour)';
      ELSE
         v_strSQLUpdate := coalesce(v_strSQLUpdate,'') || ', "' || coalesce(v_vcTaxName,'') || '" = 0';
      end if;
      select   vcTaxName, numTaxItemID INTO v_vcTaxName,v_numTaxItemID FROM    TaxItems WHERE   numDomainID = v_numDomainID
      AND numTaxItemID > v_numTaxItemID    LIMIT 1;
      GET DIAGNOSTICS SWV_RowCount = ROW_COUNT;
      IF SWV_RowCount = 0 then
         v_numTaxItemID := 0;
      end if;
   END LOOP;

   IF v_strSQLUpdate <> '' then
        
      v_strSQLUpdate := 'update tt_TEMP1 set ItemCode = ItemCode'
      || coalesce(v_strSQLUpdate,'') || ' where ItemCode > 0';
      RAISE NOTICE '%',v_strSQLUpdate;
      EXECUTE v_strSQLUpdate;
   end if;

   select   numFieldId, vcFieldName, vcDbColumnName, (tintRow+1) INTO v_numFldID,v_vcFldname,v_vcDbColumnName,v_intRowNum FROM    View_DynamicCustomColumns WHERE   numFormId = v_tintType
   AND Grp_id = 5
   AND numDomainID = v_numDomainID
   AND numAuthGroupID = v_numBizDocId
   AND bitCustom = true
   AND coalesce(numRelCntType,0) = v_numBizDocTempID   ORDER BY tintRow LIMIT 1;                                                                                          
   WHILE v_intRowNum > 0 LOOP
      SWV_ExecDyn := 'alter table tt_TEMP1 add "' || coalesce(v_vcDbColumnName,'')
      || '" VARCHAR(100)';
      EXECUTE SWV_ExecDyn;
      v_strSQLUpdate := 'update tt_TEMP1 set ItemCode = ItemCode,"'
      || coalesce(v_vcDbColumnName,'') || '" = GetCustFldValueItem(' || coalesce(v_numFldID,'')
      || ',ItemCode) where ItemCode > 0';
      EXECUTE v_strSQLUpdate;
      select   numFieldId, vcFieldName, vcDbColumnName, (tintRow+1) INTO v_numFldID,v_vcFldname,v_vcDbColumnName,v_intRowNum FROM    View_DynamicCustomColumns WHERE   numFormId = v_tintType
      AND Grp_id = 5
      AND numDomainID = v_numDomainID
      AND numAuthGroupID = v_numBizDocId
      AND bitCustom = true
      AND tintRow >= v_intRowNum
      AND coalesce(numRelCntType,0) = v_numBizDocTempID   ORDER BY tintRow LIMIT 1;
      GET DIAGNOSTICS SWV_RowCount = ROW_COUNT;
      IF SWV_RowCount = 0 then
         v_intRowNum := 0;
      end if;
   END LOOP;


   open SWV_RefCur for
   SELECT  Row_NUmber() OVER(ORDER BY numoppitemtCode) AS RowNumber,
            * FROM    tt_TEMP1
   ORDER BY numSortOrder ASC; 

    

--    SELECT  ISNULL(tintOppStatus, 0)
--    FROM    OpportunityMaster
--    WHERE   numOppID = @numOppId                                                                                                 

   IF(SELECT COUNT(*) FROM View_DynamicColumns WHERE   numFormId = v_tintType AND numDomainID = v_numDomainID
   AND numAuthGroupId = v_numBizDocId AND vcFieldType = 'R' AND coalesce(numRelCntType,0) = v_numBizDocTempID) = 0 then

      INSERT INTO DycFormConfigurationDetails(numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numAuthGroupID,numRelCntType)
      select numFormId,numFieldID,1,tintRow,v_numDomainID,v_numBizDocId,v_numBizDocTempID
      FROM View_DynamicDefaultColumns
      where numFormId = v_tintType AND bitDefault = true AND numDomainID = v_numDomainID AND vcLookBackTableName =(CASE v_numBizDocId WHEN 40911 THEN 'CSGrid' ELSE REPLACE(vcLookBackTableName,'CSGrid','') END) order by tintRow ASC;
      IF(SELECT COUNT(*) FROM DycFrmConfigBizDocsSumm Ds where numBizDocID = v_numBizDocId and Ds.numFormID = v_tintType
      and coalesce(Ds.numBizDocTempID,0) = v_numBizDocTempID and Ds.numDomainID = v_numDomainID) = 0 then
	
         INSERT INTO DycFrmConfigBizDocsSumm(numFormID,numBizDocID,numFieldID,numDomainID,numBizDocTempID)
         SELECT v_tintType,v_numBizDocId,DFM.numFieldID,v_numDomainID,v_numBizDocTempID FROM
         DycFormField_Mapping DFFM
         JOIN DycFieldMaster DFM on DFFM.numModuleID = DFFM.numModuleID and DFFM.numFieldID = DFM.numFieldID
         where numFormID = 16 and DFFM.numDomainID is null;
      end if;
   end if;      

   open SWV_RefCur2 for
   SELECT  vcFieldName AS vcFormFieldName,
            vcDbColumnName,
            vcFieldDataType,
            tintRow AS intRowNum
   FROM    View_DynamicColumns
   WHERE   numFormId = v_tintType
   AND numDomainID = v_numDomainID
   AND numAuthGroupId = v_numBizDocId
   AND vcFieldType = 'R'
   AND coalesce(numRelCntType,0) = v_numBizDocTempID
   UNION
   SELECT  vcFieldName AS vcFormFieldName,
            vcDbColumnName,
            'C' AS vcFieldDataType,
            tintRow AS intRowNum
   FROM    View_DynamicCustomColumns
   WHERE   numFormId = v_tintType
   AND Grp_id = 5
   AND numDomainID = v_numDomainID
   AND numAuthGroupID = v_numBizDocId
   AND bitCustom = true
   AND coalesce(numRelCntType,0) = v_numBizDocTempID
   ORDER BY 4;
   RETURN;
END; $$;


