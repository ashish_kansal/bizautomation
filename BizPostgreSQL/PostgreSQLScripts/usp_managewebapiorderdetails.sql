-- Stored procedure definition script USP_ManageWebAPIOrderDetails for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021



--created by Joseph
CREATE OR REPLACE FUNCTION USP_ManageWebAPIOrderDetails(v_numWebApiOrdDetailId NUMERIC(9,0),
          v_numDomainID NUMERIC(9,0),
          v_vcWebApiOrderId VARCHAR(25),
          v_tintStatus SMALLINT,
          v_numWebApiId NUMERIC(9,0),
          v_numOppid NUMERIC(9,0) DEFAULT 0)
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_Check  INTEGER;
BEGIN
   IF v_numWebApiOrdDetailId = 0 then
  
      select   COUNT(*) INTO v_Check FROM   WebApiOrderDetails WHERE  numDomainID = v_numDomainID AND WebApiId = v_numWebApiId AND vcWebApiOrderId = v_vcWebApiOrderId AND tintStatus = 0;
      IF v_Check = 0 then
	 
         INSERT INTO WebApiOrderDetails(WebApiId,
                    numDomainID,
                    vcWebApiOrderId,
                    tintStatus,
                    dtCreated,
                    numOppId)
        VALUES(v_numWebApiId,
                    v_numDomainID,
                    v_vcWebApiOrderId,
                    v_tintStatus,
                    LOCALTIMESTAMP ,
                    0);
      end if;
   ELSEIF v_numWebApiOrdDetailId <> 0
   then
  
      UPDATE WebApiOrderDetails
      SET    tintStatus = v_tintStatus,numOppId = v_numOppid,dtModified = LOCALTIMESTAMP
      WHERE  numDomainID = v_numDomainID AND vcWebApiOrderId = v_vcWebApiOrderId
      AND WebApiId = v_numWebApiId;
   end if;
   RETURN;
END; $$;










