DROP FUNCTION IF EXISTS fn_FindVendorCost;

CREATE OR REPLACE FUNCTION fn_FindVendorCost
(
	v_numItemCode NUMERIC(18,0),
	v_numDivisionID NUMERIC(18,0) DEFAULT 0,                  
    v_numDomainID NUMERIC(18,0) DEFAULT 0,            
	v_units DOUBLE PRECISION DEFAULT NULL
)
RETURNS DECIMAL(20,5) LANGUAGE plpgsql
AS $$
	DECLARE
		v_VendorCost  DECIMAL(20,5);                  
		v_fltUOMConversionFactor NUMERIC;
		v_monDynamicCost DECIMAL(20,5);
		v_monStaticCost DECIMAL(20,5);
		v_tintCommissionVendorCostType SMALLINT;
BEGIN
	SELECT tintCommissionVendorCostType INTO v_tintCommissionVendorCostType FROM Domain WHERE numDomainID=v_numDomainID;
   
	SELECT
		fn_UOMConversion(numPurchaseUnit,v_numItemCode,v_numDomainID,numBaseUnit)
	INTO
		v_fltUOMConversionFactor
	FROM 
		Item
	WHERE
		numDomainID=v_numDomainID
		AND numItemCode = v_numItemCode;
		

	IF EXISTS(SELECT 
					COALESCE(monCost,0) AS ListPrice 
				FROM 
					Vendor V 
				INNER JOIN 
					Item I
				ON 
					V.numItemCode = I.numItemCode
				WHERE 
					V.numItemCode = v_numItemCode 
					AND V.numDomainID = v_numDomainID 
					AND V.numVendorID = v_numDivisionID) THEN
		SELECT 
			COALESCE(monCost,0) 
		INTO 
			v_VendorCost 
		FROM 
			Vendor V 
		INNER JOIN 
			Item I
		ON 
			V.numItemCode = I.numItemCode 
		WHERE 
			V.numItemCode = v_numItemCode 
			AND V.numDomainID = v_numDomainID
			AND V.numVendorID = v_numDivisionID;
	ELSE
		SELECT 
			COALESCE(monCost,0) 
		INTO 
			v_VendorCost 
		FROM 
			Item
		INNER JOIN
			Vendor
		ON
			Item.numVendorID = Vendor.numVendorID
		WHERE 
			Item.numItemCode = v_numItemCode 
			AND Item.numDomainID = v_numDomainID;
	END IF;      

	If EXISTS (SELECT 
					VendorCostTable.numvendorcosttableid
				FROM 
					Vendor 
				INNER JOIN 
					VendorCostTable 
				ON 
					Vendor.numvendortcode=VendorCostTable.numvendortcode 
				WHERE
					Vendor.numDomainID=v_numDomainID
					AND Vendor.numVendorID=v_numDivisionID 
					AND Vendor.numItemCode=v_numItemCode 
					AND COALESCE(v_units,0) BETWEEN VendorCostTable.intFromQty * COALESCE(v_fltUOMConversionFactor,1) AND VendorCostTable.intToQty * COALESCE(v_fltUOMConversionFactor,1)) THEN
			SELECT 
				COALESCE(VendorCostTable.monDynamicCost,v_VendorCost)
				,COALESCE(VendorCostTable.monStaticCost,v_VendorCost)
			INTO 
				v_monDynamicCost
				,v_monStaticCost
			FROM 
				Vendor 
			INNER JOIN 
				VendorCostTable 
			ON 
				Vendor.numvendortcode=VendorCostTable.numvendortcode 
			WHERE
				Vendor.numDomainID=v_numDomainID
				AND Vendor.numVendorID=v_numDivisionID 
				AND Vendor.numItemCode=v_numItemCode 
				AND COALESCE(v_units,0) BETWEEN VendorCostTable.intFromQty * COALESCE(v_fltUOMConversionFactor,1) AND VendorCostTable.intToQty * COALESCE(v_fltUOMConversionFactor,1);
	ELSEIf EXISTS (SELECT 
					VendorCostTable.numvendorcosttableid
				FROM 
					Item
				INNER JOIN
					Vendor
				ON
					Item.numVendorID = Vendor.numVendorID
				INNER JOIN 
					VendorCostTable 
				ON 
					Vendor.numvendortcode=VendorCostTable.numvendortcode 
				WHERE
					Item.numDomainID=v_numDomainID
					AND Item.numItemCode=v_numItemCode 
					AND COALESCE(v_units,0) BETWEEN VendorCostTable.intFromQty * COALESCE(v_fltUOMConversionFactor,1) AND VendorCostTable.intToQty * COALESCE(v_fltUOMConversionFactor,1)) THEN
			SELECT 
				COALESCE(VendorCostTable.monDynamicCost,v_VendorCost)
				,COALESCE(VendorCostTable.monStaticCost,v_VendorCost)
			INTO 
				v_monDynamicCost
				,v_monStaticCost
			FROM 
				Item
			INNER JOIN
				Vendor
			ON
				Item.numVendorID = Vendor.numVendorID
			INNER JOIN 
				VendorCostTable 
			ON 
				Vendor.numvendortcode=VendorCostTable.numvendortcode 
			WHERE
				Item.numDomainID=v_numDomainID
				AND Item.numItemCode=v_numItemCode 
			AND COALESCE(v_units,0) BETWEEN VendorCostTable.intFromQty * COALESCE(v_fltUOMConversionFactor,1) AND VendorCostTable.intToQty * COALESCE(v_fltUOMConversionFactor,1);
	ELSE
		v_monDynamicCost := v_VendorCost;
		v_monStaticCost := v_VendorCost;
	END IF;

	v_VendorCost := (CASE v_tintCommissionVendorCostType WHEN 1 THEN v_monDynamicCost WHEN 2 THEN v_monStaticCost ELSE v_VendorCost END);
	
	IF v_VendorCost IS NULL THEN
      select(monTotAmount/(case when numUnitHour < 1 then 1 else numUnitHour end)) INTO v_VendorCost from OpportunityItems itm
      join OpportunityMaster mst
      on mst.numOppId = itm.numOppId where  tintopptype = 2 and numItemCode = v_numItemCode and mst.numDivisionId = v_numDivisionID;
	END IF; 
 	
	RETURN v_VendorCost;
END; $$;

