CREATE OR REPLACE FUNCTION ReturnHeader_IUD_TrFunc()
RETURNS TRIGGER LANGUAGE plpgsql
AS $$
BEGIN
	IF (TG_OP = 'DELETE') THEN
		INSERT INTO ElasticSearchModifiedRecords(vcModule,numRecordID, vcAction, numDomainID)  VALUES('Return', OLD.numReturnHeaderID, 'Delete', OLD.numDomainID);
	ELSIF (TG_OP = 'UPDATE') THEN
		INSERT INTO ElasticSearchModifiedRecords(vcModule,numRecordID, vcAction, numDomainID)  VALUES('Return', NEW.numReturnHeaderID, 'Update', NEW.numDomainID);
	ELSIF (TG_OP = 'INSERT') THEN
		INSERT INTO ElasticSearchModifiedRecords(vcModule,numRecordID, vcAction, numDomainID)  VALUES('Return', NEW.numReturnHeaderID, 'Insert', NEW.numDomainID);
	END IF;

	RETURN NULL;
END; $$;
CREATE TRIGGER ReturnHeader_IUD AFTER INSERT OR UPDATE OR DELETE ON ReturnHeader FOR EACH ROW EXECUTE PROCEDURE ReturnHeader_IUD_TrFunc();


