-- Stored procedure definition script usp_UpdateStagePercentageDetails for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION usp_UpdateStagePercentageDetails(v_numStagePercentageId NUMERIC,
    v_tintConfiguration SMALLINT,
    v_vcStageName VARCHAR(1000),
    v_slpid NUMERIC,
    v_numDomainId NUMERIC,
    v_numUserCntID NUMERIC,
    v_numAssignTo NUMERIC DEFAULT 0,
    v_vcMileStoneName VARCHAR(1000) DEFAULT NULL,
    v_tintPercentage SMALLINT DEFAULT NULL,
    v_vcDescription VARCHAR(2000) DEFAULT NULL,
    v_numStageDetailsIds NUMERIC(9,0) DEFAULT NULL,
    v_bitClose BOOLEAN DEFAULT NULL,
    v_numParentStageID NUMERIC DEFAULT NULL,
    v_intDueDays INTEGER DEFAULT NULL,
	v_dtStartDate TIMESTAMP DEFAULT NULL,
	v_dtEndDate TIMESTAMP DEFAULT NULL,
	v_bitTimeBudget BOOLEAN DEFAULT NULL,
	v_bitExpenseBudget BOOLEAN DEFAULT NULL,
	v_monTimeBudget DECIMAL(20,5) DEFAULT NULL,
	v_monExpenseBudget DECIMAL(20,5) DEFAULT NULL,
	v_numSetPrevStageProgress NUMERIC DEFAULT 0,
	v_bitIsDueDaysUsed BOOLEAN DEFAULT NULL,
	v_numTeamId NUMERIC DEFAULT NULL,
	v_bitRunningDynamicMode BOOLEAN DEFAULT NULL,
	v_numStageOrder NUMERIC(18,0) DEFAULT 0)
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numProjectID  NUMERIC;
   v_numOppID  NUMERIC;
   v_TotalR  NUMERIC;
   v_TotalP  NUMERIC;
BEGIN
   IF v_numSetPrevStageProgress > 0 then

      UPDATE StagePercentageDetails
      SET tinProgressPercentage = 100
      WHERE   numdomainid = v_numDomainId
      AND numStageDetailsId < v_numSetPrevStageProgress
      AND slp_id = v_slpid
      AND tinProgressPercentage < 100;
   end if;


   UPDATE  StagePercentageDetails
   SET     numStagePercentageId = v_numStagePercentageId,tintConfiguration = v_tintConfiguration,
   vcStageName = v_vcStageName,numModifiedBy = v_numUserCntID,
   bintModifiedDate = TIMEZONE('UTC',now()),numAssignTo =(case v_numAssignTo when -1 then numAssignTo else v_numAssignTo end),tintPercentage =(case v_numAssignTo when -1 then tintPercentage else v_tintPercentage end),
   tinProgressPercentage =(case v_numAssignTo when -1 then v_tintPercentage else tinProgressPercentage end),vcMileStoneName = v_vcMileStoneName,
   vcDescription = v_vcDescription,bitClose = v_bitClose,numParentStageID = v_numParentStageID,
   intDueDays = v_intDueDays,dtStartDate = v_dtStartDate,
   dtEndDate = v_dtEndDate,bitTimeBudget = v_bitTimeBudget,
   bitExpenseBudget = v_bitExpenseBudget,monTimeBudget = v_monTimeBudget,monExpenseBudget = v_monExpenseBudget,
   bitIsDueDaysUsed = v_bitIsDueDaysUsed,
   numTeamId = v_numTeamId,bitRunningDynamicMode = v_bitRunningDynamicMode,numStageOrder = v_numStageOrder
   WHERE   numStageDetailsId = v_numStageDetailsIds
   AND slp_id = v_slpid
   AND numdomainid = v_numDomainId;

   if v_bitClose = true then

      update  StagePercentageDetails set bitClose = v_bitClose,tinProgressPercentage = v_tintPercentage where numParentStageID  = v_numStageDetailsIds
      AND slp_id = v_slpid
      AND numdomainid = v_numDomainId;
   end if; 


   if v_numParentStageID > 0 then
      select   count(*) INTO v_TotalR from StagePercentageDetails where numParentStageID = v_numParentStageID AND slp_id = v_slpid AND numdomainid = v_numDomainId;
      select   sum(tinProgressPercentage) INTO v_TotalP from StagePercentageDetails where numParentStageID = v_numParentStageID AND slp_id = v_slpid AND numdomainid = v_numDomainId;
      if v_TotalP = v_TotalR*100 then
         Update StagePercentageDetails set tinProgressPercentage = 100,bitClose = true where numStageDetailsId = v_numParentStageID AND slp_id = v_slpid AND numdomainid = v_numDomainId;
      end if;
   end if;


   select   numProjectid, numOppid INTO v_numProjectID,v_numOppID FROM StagePercentageDetails WHERE numStageDetailsId = v_numStageDetailsIds;


--Update total progress
   IF v_numProjectID > 0 then
	 --  numeric(9, 0)
	 --  numeric(9, 0)
      PERFORM USP_GetProjectTotalProgress(v_numDomainId := v_numDomainId,v_numProId := v_numProjectID,v_tintMode := 1::SMALLINT);
   end if; --  tinyint

   IF v_numOppID > 0 then
	 --  numeric(9, 0)
	 --  numeric(9, 0)
      PERFORM USP_GetProjectTotalProgress(v_numDomainId := v_numDomainId,v_numProId := v_numOppID,v_tintMode := 0::SMALLINT);
   end if;
   RETURN;
END; $$; --  tinyint



