-- Stored procedure definition script USP_GetOpportunityAutomationQueueExecutionLog for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_GetOpportunityAutomationQueueExecutionLog(v_numDomainID NUMERIC(18,0) DEFAULT 0,
    v_numOppId NUMERIC(18,0) DEFAULT 0,
    v_ClientTimeZoneOffset INTEGER DEFAULT 0,
     v_CurrentPage INTEGER DEFAULT 0,
    v_PageSize INTEGER DEFAULT 0,
    INOUT v_TotRecs INTEGER DEFAULT 0  , INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for
   SELECT
   FormatedDateFromDate(dtDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval),v_numDomainID) AS dtExecutionDate,
		dtDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) AS dtExecutionTime,
		coalesce(vcMessage,'') AS vcMessage
   FROM
   SalesFulfillmentLog
   WHERE
   numOppID = v_numOppId
   ORDER BY
   numSFLID;
   RETURN;
END; $$;






