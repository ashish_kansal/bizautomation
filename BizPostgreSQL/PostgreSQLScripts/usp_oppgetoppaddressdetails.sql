DROP FUNCTION IF EXISTS USP_OPPGetOppAddressDetails;

CREATE OR REPLACE FUNCTION USP_OPPGetOppAddressDetails(v_numOppId NUMERIC(18,0) DEFAULT NULL,
    v_numDomainID NUMERIC(18,0)  DEFAULT 0,
	v_numOppBizDocID NUMERIC(18,0) DEFAULT 0, INOUT SWV_RefCur refcursor default null, INOUT SWV_RefCur2 refcursor default null, INOUT SWV_RefCur3 refcursor default null, INOUT SWV_RefCur4 refcursor default null, INOUT SWV_RefCur5 refcursor default null, INOUT SWV_RefCur6 refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_tintOppType  SMALLINT;
   v_tintBillType  SMALLINT;
   v_tintShipType  SMALLINT;
 
   v_numParentOppID  NUMERIC;
   v_numDivisionID  NUMERIC;
   v_numContactID  NUMERIC;
   v_numBillToAddressID  NUMERIC(18,0);
   v_numShipToAddressID  NUMERIC(18,0);
   v_numVendorAddressID  NUMERIC(18,0);
BEGIN
   select   tintopptype, tintBillToType, tintShipToType, numDivisionId, numContactId, coalesce(numBillToAddressID,0), coalesce(numShipToAddressID,0), coalesce(numVendorAddressID,0) INTO v_tintOppType,v_tintBillType,v_tintShipType,v_numDivisionID,v_numContactID,
   v_numBillToAddressID,v_numShipToAddressID,v_numVendorAddressID FROM   OpportunityMaster WHERE  numOppId = v_numOppId;


   IF v_tintOppType = 2 then
		
			--************Opp/Order Billing Address************
      IF v_numBillToAddressID > 0 then
			
         open SWV_RefCur for
         SELECT
         '<pre>' || coalesce(AD.vcStreet,'') || '</pre>' || coalesce(AD.vcCity,'') || ', ' || coalesce(fn_GetState(AD.numState),'') || ' ' || coalesce(AD.vcPostalCode,'') || ' <br>' || coalesce(fn_GetListItemName(AD.numCountry),'') AS vcFullAddress,
					coalesce(AD.vcStreet,'') AS vcStreet,
					coalesce(AD.vcCity,'') AS vcCity,
					coalesce(fn_GetState(AD.numState),'') AS vcState,
					coalesce(AD.vcPostalCode,'') AS vcPostalCode,
					coalesce(fn_GetListItemName(AD.numCountry),'') AS vcCountry,
					coalesce(AD.numCountry,0) AS numCountry,
					coalesce(AD.numState,0) AS numState,
					coalesce(AD.vcAddressName,'') AS vcAddressName,
            		(SELECT CMP.vcCompanyName FROM DivisionMaster DM JOIN CompanyInfo CMP ON DM.numCompanyID = CMP.numCompanyId and CMP.numDomainID = AD.numDomainID WHERE DM.numDivisionID = AD.numRecordID) AS vcCompanyName
					,(CASE
         WHEN coalesce(bitAltContact,false) = true
         THEN coalesce(vcAltContact,'')
         ELSE(CASE
            WHEN coalesce(numContact,0) > 0
            THEN
               coalesce((SELECT  CONCAT(vcFirstName,' ',vcLastname) FROM AdditionalContactsInformation WHERE numDomainID = v_numDomainID AND numContactId = numContact LIMIT 1),'')
            ELSE
               coalesce((SELECT  CONCAT(vcFirstName,' ',vcLastname) FROM AdditionalContactsInformation WHERE numDomainID = v_numDomainID AND numDivisionId = AD.numRecordID AND coalesce(bitPrimaryContact,false) = true LIMIT 1),'')
            END)  END) AS vcContact
         FROM
         AddressDetails AD
         WHERE
         AD.numDomainID = v_numDomainID
         AND AD.numAddressID = v_numBillToAddressID;
      ELSEIF EXISTS(SELECT * FROM OpportunityAddress WHERE numOppID = v_numOppId)
      then
         open SWV_RefCur for
         SELECT 
         '<pre>' || coalesce(AD.vcBillStreet,'') || '</pre>' || coalesce(AD.vcBillCity,'') || ', ' || coalesce(fn_GetState(AD.numBillState),'') || ' ' || coalesce(AD.vcBillPostCode,'') || ' <br>' || coalesce(fn_GetListItemName(AD.numBillCountry),'') AS vcFullAddress,
					coalesce(AD.vcBillStreet,'') AS vcStreet,
					coalesce(AD.vcBillCity,'') AS vcCity,
					coalesce(fn_GetState(AD.numBillState),'') AS vcState,
					coalesce(AD.vcBillPostCode,'') AS vcPostalCode,
					coalesce(fn_GetListItemName(AD.numBillCountry),'') AS vcCountry,
					coalesce(AD.vcAddressName,'') AS vcAddressName,
            		CASE WHEN LENGTH(AD.vcBillCompanyName) > 0 THEN AD.vcBillCompanyName ELSE coalesce((SELECT CMP.vcCompanyName FROM CompanyInfo CMP WHERE CMP.numCompanyId = AD.numBillCompanyId and CMP.numDomainID = v_numDomainID),'') END AS vcCompanyName
					,(CASE
         WHEN coalesce(bitAltBillingContact,false) = true
         THEN coalesce(vcAltBillingContact,'')
         ELSE(CASE
            WHEN coalesce(numBillingContact,0) > 0
            THEN
               coalesce((SELECT  CONCAT(vcFirstName,' ',vcLastname) FROM AdditionalContactsInformation WHERE numDomainID = v_numDomainID AND numContactId = numBillingContact LIMIT 1),'')
            ELSE
               coalesce((SELECT  CONCAT(vcFirstName,' ',vcLastname) FROM AdditionalContactsInformation INNER JOIN DivisionMaster ON AdditionalContactsInformation.numDivisionId = DivisionMaster.numDivisionID WHERE AdditionalContactsInformation.numDomainID = v_numDomainID AND DivisionMaster.numCompanyID = AD.numBillCompanyId AND coalesce(bitPrimaryContact,false) = true LIMIT 1),
               '')
            END)  END) AS vcContact
         FROM
         OpportunityAddress AD
         WHERE
         AD.numOppID = v_numOppId LIMIT 1;
      ELSE
         open SWV_RefCur for
         SELECT  '<pre>' || coalesce(AD.vcStreet,'') || '</pre>' || coalesce(AD.vcCity,'') || ', ' || coalesce(fn_GetState(AD.numState),'') || ' ' || coalesce(AD.vcPostalCode,'') || ' <br>' || coalesce(fn_GetListItemName(AD.numCountry),'') AS vcFullAddress,
						coalesce(AD.vcStreet,'') AS vcStreet,
						coalesce(AD.vcCity,'') AS vcCity,
						coalesce(fn_GetState(AD.numState),'') AS vcState,
						coalesce(AD.vcPostalCode,'') AS vcPostalCode,
						coalesce(fn_GetListItemName(AD.numCountry),'') AS vcCountry,
						coalesce(AD.numCountry,0) AS numCountry,
						coalesce(AD.numState,0) AS numState,
						coalesce(AD.vcAddressName,'') AS vcAddressName,
            			CMP.vcCompanyName
						,(CASE
         WHEN coalesce(bitAltContact,false) = true
         THEN coalesce(vcAltContact,'')
         ELSE(CASE
            WHEN coalesce(numContact,0) > 0
            THEN
               coalesce((SELECT  CONCAT(vcFirstName,' ',vcLastname) FROM AdditionalContactsInformation WHERE numDomainID = v_numDomainID AND numContactId = numContact LIMIT 1),'')
            ELSE
               coalesce((SELECT  CONCAT(vcFirstName,' ',vcLastname) FROM AdditionalContactsInformation WHERE numDomainID = v_numDomainID AND numDivisionId = AD.numRecordID AND coalesce(bitPrimaryContact,false) = true LIMIT 1),'')
            END)  END) AS vcContact
         FROM AddressDetails AD
         JOIN DivisionMaster DM ON DM.numDivisionID = AD.numRecordID
         JOIN CompanyInfo CMP ON DM.numCompanyID = CMP.numCompanyId and CMP.numDomainID = AD.numDomainID
         WHERE AD.numDomainID = v_numDomainID AND AD.numRecordID = v_numDivisionID
         AND AD.tintAddressOf = 2 AND AD.tintAddressType = 1 AND AD.bitIsPrimary = true;
      end if;

			--************Opp/Order Shipping Address************
      IF v_numShipToAddressID > 0 then
			
         open SWV_RefCur2 for
         SELECT
         '<pre>' || coalesce(AD.vcStreet,'') || '</pre>' || coalesce(AD.vcCity,'') || ', ' || coalesce(fn_GetState(AD.numState),'') || ' ' || coalesce(AD.vcPostalCode,'') || ' <br>' || coalesce(fn_GetListItemName(AD.numCountry),'') AS vcFullAddress,
					coalesce(AD.vcStreet,'') AS vcStreet,
					coalesce(AD.vcCity,'') AS vcCity,
					coalesce(fn_GetState(AD.numState),'') AS vcState,
					coalesce(AD.vcPostalCode,'') AS vcPostalCode,
					coalesce(fn_GetListItemName(AD.numCountry),'') AS vcCountry,
					coalesce(AD.numCountry,0) AS numCountry,
						coalesce(AD.numState,0) AS numState,
					coalesce(AD.vcAddressName,'') AS vcAddressName,
            		(SELECT CMP.vcCompanyName FROM DivisionMaster DM JOIN CompanyInfo CMP ON DM.numCompanyID = CMP.numCompanyId and CMP.numDomainID = AD.numDomainID WHERE DM.numDivisionID = AD.numRecordID) AS vcCompanyName,
					(CASE
         WHEN coalesce(bitAltContact,false) = true
         THEN coalesce(vcAltContact,'')
         ELSE(CASE
            WHEN coalesce(numContact,0) > 0
            THEN
               coalesce((SELECT  CONCAT(vcFirstName,' ',vcLastname) FROM AdditionalContactsInformation WHERE numDomainID = v_numDomainID AND numContactId = numContact LIMIT 1),'')
            ELSE
               coalesce((SELECT  CONCAT(vcFirstName,' ',vcLastname) FROM AdditionalContactsInformation WHERE numDomainID = v_numDomainID AND numDivisionId = AD.numRecordID AND coalesce(bitPrimaryContact,false) = true LIMIT 1),'')
            END)  END) AS vcContact
         FROM
         AddressDetails AD
         WHERE
         AD.numDomainID = v_numDomainID
         AND AD.numAddressID = v_numShipToAddressID;
      ELSEIF EXISTS(SELECT * FROM OpportunityAddress WHERE numOppID = v_numOppId)
      then
         open SWV_RefCur2 for
         SELECT 
         '<pre>' || coalesce(AD.vcShipStreet,'') || '</pre>' || coalesce(AD.vcShipCity,'') || ', ' || coalesce(fn_GetState(AD.numShipState),'') || ' ' || coalesce(AD.vcShipPostCode,'') || ' <br>' || coalesce(fn_GetListItemName(AD.numShipCountry),'') AS vcFullAddress,
					coalesce(AD.vcShipStreet,'') AS vcStreet,
					coalesce(AD.vcShipCity,'') AS vcCity,
					coalesce(fn_GetState(AD.numShipState),'') AS vcState,
					coalesce(AD.vcShipPostCode,'') AS vcPostalCode,
					coalesce(fn_GetListItemName(AD.numShipCountry),'') AS vcCountry,
					coalesce(AD.vcshipaddressname,'') AS vcAddressName,
            		CASE WHEN LENGTH(AD.vcShipCompanyName) > 0 THEN AD.vcShipCompanyName ELSE coalesce((SELECT CMP.vcCompanyName FROM CompanyInfo CMP WHERE CMP.numCompanyId = AD.numShipCompanyId and CMP.numDomainID = v_numDomainID),'') END AS vcCompanyName
					,(CASE
         WHEN coalesce(bitAltShippingContact,false) = true
         THEN coalesce(vcAltShippingContact,'')
         ELSE(CASE
            WHEN coalesce(numShippingContact,0) > 0
            THEN
               coalesce((SELECT  CONCAT(vcFirstName,' ',vcLastname) FROM AdditionalContactsInformation WHERE numDomainID = v_numDomainID AND numContactId = numShippingContact LIMIT 1),'')
            ELSE
               coalesce((SELECT  CONCAT(vcFirstName,' ',vcLastname) FROM AdditionalContactsInformation WHERE numDomainID = v_numDomainID AND numDivisionId = AD.numShipCompanyId AND coalesce(bitPrimaryContact,false) = true LIMIT 1),
               '')
            END)  END) AS vcContact
         FROM
         OpportunityAddress AD
         WHERE
         AD.numOppID = v_numOppId LIMIT 1;
      ELSE
         open SWV_RefCur2 for
         SELECT  '<pre>' || coalesce(AD.vcStreet,'') || '</pre>' || coalesce(AD.vcCity,'') || ', ' || coalesce(fn_GetState(AD.numState),'') || ' ' || coalesce(AD.vcPostalCode,'') || ' <br>' || coalesce(fn_GetListItemName(AD.numCountry),'') AS vcFullAddress,
					coalesce(AD.vcStreet,'') AS vcStreet,
					coalesce(AD.vcCity,'') AS vcCity,
					coalesce(fn_GetState(AD.numState),'') AS vcState,
					coalesce(AD.vcPostalCode,'') AS vcPostalCode,
					coalesce(fn_GetListItemName(AD.numCountry),'') AS vcCountry,
					coalesce(AD.numCountry,0) AS numCountry,
						coalesce(AD.numState,0) AS numState,
					coalesce(AD.vcAddressName,'') AS vcAddressName,
					CMP.vcCompanyName
					,(CASE
         WHEN coalesce(bitAltContact,false) = true
         THEN coalesce(vcAltContact,'')
         ELSE(CASE
            WHEN coalesce(numContact,0) > 0
            THEN
               coalesce((SELECT  CONCAT(vcFirstName,' ',vcLastname) FROM AdditionalContactsInformation WHERE numDomainID = v_numDomainID AND numContactId = numContact LIMIT 1),'')
            ELSE
               coalesce((SELECT  CONCAT(vcFirstName,' ',vcLastname) FROM AdditionalContactsInformation WHERE numDomainID = v_numDomainID AND numDivisionId = AD.numRecordID AND coalesce(bitPrimaryContact,false) = true LIMIT 1),'')
            END)  END) AS vcContact
         FROM AddressDetails AD
         JOIN DivisionMaster DM ON DM.numDivisionID = AD.numRecordID
         JOIN CompanyInfo CMP ON DM.numCompanyID = CMP.numCompanyId and CMP.numDomainID = AD.numDomainID
         WHERE AD.numDomainID = v_numDomainID AND AD.numRecordID = v_numDivisionID
         AND AD.tintAddressOf = 2 AND AD.tintAddressType = 2 AND AD.bitIsPrimary = true;
      end if;
			
			 --************Employer Billing Address************
      open SWV_RefCur3 for
      SELECT  '<pre>' || coalesce(AD.vcStreet,'') || '</pre>' || coalesce(AD.vcCity,'') || ', ' || coalesce(fn_GetState(AD.numState),'') || ' ' || coalesce(AD.vcPostalCode,'') || ' <br>' || coalesce(fn_GetListItemName(AD.numCountry),'') AS vcFullAddress,
					coalesce(AD.vcStreet,'') AS vcStreet,
					coalesce(AD.vcCity,'') AS vcCity,
					coalesce(fn_GetState(AD.numState),'') AS vcState,
					coalesce(AD.vcPostalCode,'') AS vcPostalCode,
					coalesce(fn_GetListItemName(AD.numCountry),'') AS vcCountry,
					coalesce(AD.numCountry,0) AS numCountry,
						coalesce(AD.numState,0) AS numState,
					coalesce(AD.vcAddressName,'') AS vcAddressName,vcCompanyName
					,(CASE
      WHEN coalesce(bitAltContact,false) = true
      THEN coalesce(vcAltContact,'')
      ELSE(CASE
         WHEN coalesce(numContact,0) > 0
         THEN
            coalesce((SELECT  CONCAT(vcFirstName,' ',vcLastname) FROM AdditionalContactsInformation WHERE numDomainID = v_numDomainID AND numContactId = numContact LIMIT 1),'')
         ELSE
            coalesce((SELECT  CONCAT(vcFirstName,' ',vcLastname) FROM AdditionalContactsInformation WHERE numDomainID = v_numDomainID AND numDivisionId = AD.numRecordID AND coalesce(bitPrimaryContact,false) = true LIMIT 1),'')
         END)
      END) AS vcContact
      FROM Domain D1 JOIN DivisionMaster div1 ON D1.numDivisionId = div1.numDivisionID
      JOIN CompanyInfo Com1 ON Com1.numCompanyId = div1.numCompanyID
      JOIN AddressDetails AD ON AD.numDomainID = div1.numDomainID AND AD.numRecordID = div1.numDivisionID
      WHERE  D1.numDomainId = v_numDomainID
      AND AD.tintAddressOf = 2 AND AD.tintAddressType = 1 AND AD.bitIsPrimary = true;


			--************Employer Shipping Address************
      open SWV_RefCur4 for
      SELECT  '<pre>' || coalesce(AD.vcStreet,'') || '</pre>' || coalesce(AD.vcCity,'') || ', ' || coalesce(fn_GetState(AD.numState),'') || ' ' || coalesce(AD.vcPostalCode,'') || ' <br>' || coalesce(fn_GetListItemName(AD.numCountry),'') AS vcFullAddress,
					coalesce(AD.vcStreet,'') AS vcStreet,
					coalesce(AD.vcCity,'') AS vcCity,
					coalesce(fn_GetState(AD.numState),'') AS vcState,
					coalesce(AD.vcPostalCode,'') AS vcPostalCode,
					coalesce(fn_GetListItemName(AD.numCountry),'') AS vcCountry,
					coalesce(AD.numCountry,0) AS numCountry,
						coalesce(AD.numState,0) AS numState,
					coalesce(AD.vcAddressName,'') AS vcAddressName,vcCompanyName
					,(CASE
      WHEN coalesce(bitAltContact,false) = true
      THEN coalesce(vcAltContact,'')
      ELSE(CASE
         WHEN coalesce(numContact,0) > 0
         THEN
            coalesce((SELECT  CONCAT(vcFirstName,' ',vcLastname) FROM AdditionalContactsInformation WHERE numDomainID = v_numDomainID AND numContactId = numContact LIMIT 1),'')
         ELSE
            coalesce((SELECT  CONCAT(vcFirstName,' ',vcLastname) FROM AdditionalContactsInformation WHERE numDomainID = v_numDomainID AND numDivisionId = AD.numRecordID AND coalesce(bitPrimaryContact,false) = true LIMIT 1),'')
         END)
      END) AS vcContact
      FROM Domain D1 JOIN DivisionMaster div1 ON D1.numDivisionId = div1.numDivisionID
      JOIN CompanyInfo Com1 ON Com1.numCompanyId = div1.numCompanyID
      JOIN AddressDetails AD ON AD.numDomainID = div1.numDomainID AND AD.numRecordID = div1.numDivisionID
      WHERE AD.numDomainID = v_numDomainID
      AND AD.tintAddressOf = 2 AND AD.tintAddressType = 2 AND AD.bitIsPrimary = true; 


			--************Vendor Billing Address************
      open SWV_RefCur5 for
      SELECT  '<pre>' || coalesce(AD.vcStreet,'') || '</pre>' || coalesce(AD.vcCity,'') || ', ' || coalesce(fn_GetState(AD.numState),'') || ' ' || coalesce(AD.vcPostalCode,'') || ' <br>' || coalesce(fn_GetListItemName(AD.numCountry),'') AS vcFullAddress,
						coalesce(AD.vcStreet,'') AS vcStreet,
						coalesce(AD.vcCity,'') AS vcCity,
						coalesce(fn_GetState(AD.numState),'') AS vcState,
						coalesce(AD.vcPostalCode,'') AS vcPostalCode,
						coalesce(fn_GetListItemName(AD.numCountry),'') AS vcCountry,
						coalesce(AD.numCountry,0) AS numCountry,
						coalesce(AD.numState,0) AS numState,
						coalesce(AD.vcAddressName,'') AS vcAddressName,
            			CMP.vcCompanyName
						,(CASE
      WHEN coalesce(bitAltContact,false) = true
      THEN coalesce(vcAltContact,'')
      ELSE(CASE
         WHEN coalesce(numContact,0) > 0
         THEN
            coalesce((SELECT  CONCAT(vcFirstName,' ',vcLastname) FROM AdditionalContactsInformation WHERE numDomainID = v_numDomainID AND numContactId = numContact LIMIT 1),'')
         ELSE
            coalesce((SELECT  CONCAT(vcFirstName,' ',vcLastname) FROM AdditionalContactsInformation WHERE numDomainID = v_numDomainID AND numDivisionId = AD.numRecordID AND coalesce(bitPrimaryContact,false) = true LIMIT 1),'')
         END)  END) AS vcContact
      FROM AddressDetails AD
      JOIN DivisionMaster DM ON DM.numDivisionID = AD.numRecordID
      JOIN CompanyInfo CMP ON DM.numCompanyID = CMP.numCompanyId and CMP.numDomainID = AD.numDomainID
      WHERE AD.numDomainID = v_numDomainID AND AD.numRecordID = v_numDivisionID
      AND AD.tintAddressOf = 2 AND AD.tintAddressType = 1 AND AD.bitIsPrimary = true;


			--************Vendor Shipping Address************
      IF v_numVendorAddressID > 0 then
			
         open SWV_RefCur6 for
         SELECT
         '<pre>' || coalesce(AD.vcStreet,'') || '</pre>' || coalesce(AD.vcCity,'') || ', ' || coalesce(fn_GetState(AD.numState),'') || ' ' || coalesce(AD.vcPostalCode,'') || ' <br>' || coalesce(fn_GetListItemName(AD.numCountry),'') AS vcFullAddress,
					coalesce(AD.vcStreet,'') AS vcStreet,
					coalesce(AD.vcCity,'') AS vcCity,
					coalesce(fn_GetState(AD.numState),'') AS vcState,
					coalesce(AD.vcPostalCode,'') AS vcPostalCode,
					coalesce(fn_GetListItemName(AD.numCountry),'') AS vcCountry,
					coalesce(AD.numCountry,0) AS numCountry,
						coalesce(AD.numState,0) AS numState,
					coalesce(AD.vcAddressName,'') AS vcAddressName,
            		(SELECT CMP.vcCompanyName FROM DivisionMaster DM JOIN CompanyInfo CMP ON DM.numCompanyID = CMP.numCompanyId and CMP.numDomainID = AD.numDomainID WHERE DM.numDivisionID = AD.numRecordID) AS vcCompanyName,
					(CASE
         WHEN coalesce(bitAltContact,false) = true
         THEN coalesce(vcAltContact,'')
         ELSE(CASE
            WHEN coalesce(numContact,0) > 0
            THEN
               coalesce((SELECT  CONCAT(vcFirstName,' ',vcLastname) FROM AdditionalContactsInformation WHERE numDomainID = v_numDomainID AND numContactId = numContact LIMIT 1),'')
            ELSE
               coalesce((SELECT  CONCAT(vcFirstName,' ',vcLastname) FROM AdditionalContactsInformation WHERE numDomainID = v_numDomainID AND numDivisionId = AD.numRecordID AND coalesce(bitPrimaryContact,false) = true LIMIT 1),'')
            END)  END) AS vcContact
         FROM
         AddressDetails AD
         WHERE
         AD.numDomainID = v_numDomainID
         AND AD.numAddressID = v_numVendorAddressID;
      ELSE
         open SWV_RefCur6 for
         SELECT  '<pre>' || coalesce(AD.vcStreet,'') || '</pre>' || coalesce(AD.vcCity,'') || ', ' || coalesce(fn_GetState(AD.numState),'') || ' ' || coalesce(AD.vcPostalCode,'') || ' <br>' || coalesce(fn_GetListItemName(AD.numCountry),'') AS vcFullAddress,
					coalesce(AD.vcStreet,'') AS vcStreet,
					coalesce(AD.vcCity,'') AS vcCity,
					coalesce(fn_GetState(AD.numState),'') AS vcState,
					coalesce(AD.vcPostalCode,'') AS vcPostalCode,
					coalesce(fn_GetListItemName(AD.numCountry),'') AS vcCountry,
					coalesce(AD.numCountry,0) AS numCountry,
						coalesce(AD.numState,0) AS numState,
					coalesce(AD.vcAddressName,'') AS vcAddressName,
					CMP.vcCompanyName
					,(CASE
         WHEN coalesce(bitAltContact,false) = true
         THEN coalesce(vcAltContact,'')
         ELSE(CASE
            WHEN coalesce(numContact,0) > 0
            THEN
               coalesce((SELECT  CONCAT(vcFirstName,' ',vcLastname) FROM AdditionalContactsInformation WHERE numDomainID = v_numDomainID AND numContactId = numContact LIMIT 1),'')
            ELSE
               coalesce((SELECT  CONCAT(vcFirstName,' ',vcLastname) FROM AdditionalContactsInformation WHERE numDomainID = v_numDomainID AND numDivisionId = AD.numRecordID AND coalesce(bitPrimaryContact,false) = true LIMIT 1),'')
            END)  END) AS vcContact
         FROM AddressDetails AD
         JOIN DivisionMaster DM ON DM.numDivisionID = AD.numRecordID
         JOIN CompanyInfo CMP ON DM.numCompanyID = CMP.numCompanyId and CMP.numDomainID = AD.numDomainID
         WHERE AD.numDomainID = v_numDomainID AND AD.numRecordID = v_numDivisionID
         AND AD.tintAddressOf = 2 AND AD.tintAddressType = 2 AND AD.bitIsPrimary = true;
      end if;
   end if;

   IF v_tintOppType = 1 then
		
			--************Opp/Order Billing Address************
      IF v_numBillToAddressID > 0 then
			
         open SWV_RefCur for
         SELECT
         '<pre>' || coalesce(AD.vcStreet,'') || '</pre>' || coalesce(AD.vcCity,'') || ', ' || coalesce(fn_GetState(AD.numState),'') || ' ' || coalesce(AD.vcPostalCode,'') || ' <br>' || coalesce(fn_GetListItemName(AD.numCountry),'') AS vcFullAddress,
					coalesce(AD.vcStreet,'') AS vcStreet,
					coalesce(AD.vcCity,'') AS vcCity,
					coalesce(fn_GetState(AD.numState),'') AS vcState,
					coalesce(AD.vcPostalCode,'') AS vcPostalCode,
					coalesce(fn_GetListItemName(AD.numCountry),'') AS vcCountry,
					coalesce(AD.numCountry,0) AS numCountry,
					coalesce(AD.numState,0) AS numState,
					coalesce(AD.vcAddressName,'') AS vcAddressName,
            		(SELECT CMP.vcCompanyName FROM DivisionMaster DM JOIN CompanyInfo CMP ON DM.numCompanyID = CMP.numCompanyId and CMP.numDomainID = AD.numDomainID WHERE DM.numDivisionID = AD.numRecordID) AS vcCompanyName
					,(CASE
         WHEN coalesce(bitAltContact,false) = true
         THEN coalesce(vcAltContact,'')
         ELSE(CASE
            WHEN coalesce(numContact,0) > 0
            THEN
               coalesce((SELECT  CONCAT(vcFirstName,' ',vcLastname) FROM AdditionalContactsInformation WHERE numDomainID = v_numDomainID AND numContactId = numContact LIMIT 1),'')
            ELSE
               coalesce((SELECT  CONCAT(vcFirstName,' ',vcLastname) FROM AdditionalContactsInformation WHERE numDomainID = v_numDomainID AND numDivisionId = AD.numRecordID AND coalesce(bitPrimaryContact,false) = true LIMIT 1),'')
            END)
         END) AS vcContact
         FROM
         AddressDetails AD
         WHERE
         AD.numDomainID = v_numDomainID
         AND AD.numAddressID = v_numBillToAddressID;
      ELSE
         open SWV_RefCur for
         SELECT
         '<pre>' || coalesce(vcStreet,'') || '</pre>' || coalesce(vcCity,'') || ', ' || coalesce(vcState,'') || ' ' || coalesce(vcPostalCode,'') || ' <br>' || coalesce(vcCountry,'') AS vcFullAddress,
					coalesce(vcStreet,'') AS vcStreet,
					coalesce(vcCity,'') AS vcCity,
					coalesce(vcState,'') AS vcState,
					coalesce(vcPostalCode,'') AS vcPostalCode,
					coalesce(vcCountry,'') AS vcCountry,
					coalesce(vcAddressName,'') AS vcAddressName,coalesce(vcCompanyName,'') AS vcCompanyName
					,vcContact
					,coalesce(numCountry,0) AS numCountry
					,coalesce(numState,0) AS numState
         FROM
         fn_getOPPAddressDetails(v_numOppId,v_numDomainID,1::SMALLINT);
      end if;
		
			--************Opp/Order Shipping Address************
      IF(SELECT coalesce(bitDropShipAddress,false)  FROM OpportunityMaster WHERE numOppId = v_numOppId AND numDomainId = v_numDomainID) = true then
			
         open SWV_RefCur2 for
         SELECT
         '<pre>' || coalesce(vcStreet,'') || '</pre>' || coalesce(vcCity,'') || ', ' || coalesce(vcState,'') || ' ' || coalesce(vcPostalCode,'') || ' <br>' || coalesce(vcCountry,'') AS vcFullAddress,
					coalesce(vcStreet,'') AS vcStreet,
					coalesce(vcCity,'') AS vcCity,
					coalesce(vcState,'') AS vcState,
					coalesce(vcPostalCode,'') AS vcPostalCode,
					coalesce(vcCountry,'') AS vcCountry,
					coalesce(vcAddressName,'') AS vcAddressName,coalesce(vcCompanyName,'') AS vcCompanyName
					,vcContact
					,coalesce(numState,0) AS numState
					,coalesce(numCountry,0) AS numCountry
         FROM
         fn_getOPPAddressDetails(v_numOppId,v_numDomainID,2::SMALLINT);
      ELSEIF coalesce(v_numOppBizDocID,0) > 0 AND(SELECT COUNT(*) FROM(SELECT DISTINCT numShipToAddressID FROM OpportunityBizDocItems OBDI INNER JOIN OpportunityItems OI ON OBDI.numOppItemID = OI.numoppitemtCode WHERE OBDI.numOppBizDocID = v_numOppBizDocID AND coalesce(OI.numShipToAddressID,0) > 0) TEMP) = 1 AND v_tintShipType <> 2
      then
			
         SELECT  numShipToAddressID INTO v_numShipToAddressID FROM OpportunityBizDocItems OBDI INNER JOIN OpportunityItems OI ON OBDI.numOppItemID = OI.numoppitemtCode WHERE OBDI.numOppBizDocID = v_numOppBizDocID AND coalesce(OI.numShipToAddressID,0) > 0     LIMIT 1;
         open SWV_RefCur2 for
         SELECT
         '<pre>' || coalesce(AD.vcStreet,'') || '</pre>' || coalesce(AD.vcCity,'') || ', ' || coalesce(fn_GetState(AD.numState),'') || ' ' || coalesce(AD.vcPostalCode,'') || ' <br>' || coalesce(fn_GetListItemName(AD.numCountry),'') AS vcFullAddress,
					coalesce(AD.vcStreet,'') AS vcStreet,
					coalesce(AD.vcCity,'') AS vcCity,
					coalesce(fn_GetState(AD.numState),'') AS vcState,
					coalesce(AD.vcPostalCode,'') AS vcPostalCode,
					coalesce(fn_GetListItemName(AD.numCountry),'') AS vcCountry,
					coalesce(AD.numCountry,0) AS numCountry,
						coalesce(AD.numState,0) AS numState,
					coalesce(AD.vcAddressName,'') AS vcAddressName,
            		(SELECT CMP.vcCompanyName FROM DivisionMaster DM JOIN CompanyInfo CMP ON DM.numCompanyID = CMP.numCompanyId and CMP.numDomainID = AD.numDomainID WHERE DM.numDivisionID = AD.numRecordID) AS vcCompanyName
					,(CASE
         WHEN coalesce(bitAltContact,false) = true
         THEN coalesce(vcAltContact,'')
         ELSE(CASE
            WHEN coalesce(numContact,0) > 0
            THEN
               coalesce((SELECT  CONCAT(vcFirstName,' ',vcLastname) FROM AdditionalContactsInformation WHERE numDomainID = v_numDomainID AND numContactId = numContact LIMIT 1),'')
            ELSE
               coalesce((SELECT  CONCAT(vcFirstName,' ',vcLastname) FROM AdditionalContactsInformation WHERE numDomainID = v_numDomainID AND numDivisionId = AD.numRecordID AND coalesce(bitPrimaryContact,false) = true LIMIT 1),'')
            END)
         END) AS vcContact
         FROM
         AddressDetails AD
         WHERE
         AD.numDomainID = v_numDomainID
         AND AD.numAddressID = v_numShipToAddressID;
      ELSEIF v_numShipToAddressID > 0
      then
			
         open SWV_RefCur2 for
         SELECT
         '<pre>' || coalesce(AD.vcStreet,'') || '</pre>' || coalesce(AD.vcCity,'') || ', ' || coalesce(fn_GetState(AD.numState),'') || ' ' || coalesce(AD.vcPostalCode,'') || ' <br>' || coalesce(fn_GetListItemName(AD.numCountry),'') AS vcFullAddress,
					coalesce(AD.vcStreet,'') AS vcStreet,
					coalesce(AD.vcCity,'') AS vcCity,
					coalesce(fn_GetState(AD.numState),'') AS vcState,
					coalesce(AD.vcPostalCode,'') AS vcPostalCode,
					coalesce(fn_GetListItemName(AD.numCountry),'') AS vcCountry,
					coalesce(AD.numCountry,0) AS numCountry,
						coalesce(AD.numState,0) AS numState,
					coalesce(AD.vcAddressName,'') AS vcAddressName,
            		(SELECT CMP.vcCompanyName FROM DivisionMaster DM JOIN CompanyInfo CMP ON DM.numCompanyID = CMP.numCompanyId and CMP.numDomainID = AD.numDomainID WHERE DM.numDivisionID = AD.numRecordID) AS vcCompanyName
					,(CASE
         WHEN coalesce(bitAltContact,false) = true
         THEN coalesce(vcAltContact,'')
         ELSE(CASE
            WHEN coalesce(numContact,0) > 0
            THEN
               coalesce((SELECT  CONCAT(vcFirstName,' ',vcLastname) FROM AdditionalContactsInformation WHERE numDomainID = v_numDomainID AND numContactId = numContact LIMIT 1),'')
            ELSE
               coalesce((SELECT  CONCAT(vcFirstName,' ',vcLastname) FROM AdditionalContactsInformation WHERE numDomainID = v_numDomainID AND numDivisionId = AD.numRecordID AND coalesce(bitPrimaryContact,false) = true LIMIT 1),'')
            END)
         END) AS vcContact
         FROM
         AddressDetails AD
         WHERE
         AD.numDomainID = v_numDomainID
         AND AD.numAddressID = v_numShipToAddressID;
      ELSE
         open SWV_RefCur2 for
         SELECT
         '<pre>' || coalesce(vcStreet,'') || '</pre>' || coalesce(vcCity,'') || ', ' || coalesce(vcState,'') || ' ' || coalesce(vcPostalCode,'') || ' <br>' || coalesce(vcCountry,'') AS vcFullAddress,
					coalesce(vcStreet,'') AS vcStreet,
					coalesce(vcCity,'') AS vcCity,
					coalesce(vcState,'') AS vcState,
					coalesce(vcPostalCode,'') AS vcPostalCode,
					coalesce(vcCountry,'') AS vcCountry,
					coalesce(vcAddressName,'') AS vcAddressName,coalesce(vcCompanyName,'') AS vcCompanyName
					,vcContact
					,coalesce(numCountry,0) AS numCountry
					,coalesce(numState,0) AS numState
         FROM
         fn_getOPPAddressDetails(v_numOppId,v_numDomainID,2::SMALLINT);
      end if;
	
			--************Employer Billing Address************ --(For SO take Domain Address)
      open SWV_RefCur3 for
      SELECT  '<pre>' || coalesce(AD.vcStreet,'') || '</pre>' || coalesce(AD.vcCity,'') || ', ' || coalesce(fn_GetState(AD.numState),'') || ' ' || coalesce(AD.vcPostalCode,'') || ' <br>' || coalesce(fn_GetListItemName(AD.numCountry),'') AS vcFullAddress,
					coalesce(AD.vcStreet,'') AS vcStreet,
					coalesce(AD.vcCity,'') AS vcCity,
					coalesce(fn_GetState(AD.numState),'') AS vcState,
					coalesce(AD.vcPostalCode,'') AS vcPostalCode,
					coalesce(fn_GetListItemName(AD.numCountry),'') AS vcCountry,
					coalesce(AD.numCountry,0) AS numCountry,
						coalesce(AD.numState,0) AS numState,
					coalesce(AD.vcAddressName,'') AS vcAddressName,vcCompanyName
					,(CASE
      WHEN coalesce(bitAltContact,false) = true
      THEN coalesce(vcAltContact,'')
      ELSE(CASE
         WHEN coalesce(numContact,0) > 0
         THEN
            coalesce((SELECT  CONCAT(vcFirstName,' ',vcLastname) FROM AdditionalContactsInformation WHERE numDomainID = v_numDomainID AND numContactId = numContact LIMIT 1),'')
         ELSE
            coalesce((SELECT  CONCAT(vcFirstName,' ',vcLastname) FROM AdditionalContactsInformation WHERE numDomainID = v_numDomainID AND numDivisionId = AD.numRecordID AND coalesce(bitPrimaryContact,false) = true LIMIT 1),'')
         END)
      END) AS vcContact
      FROM Domain D1 JOIN DivisionMaster div1 ON D1.numDivisionId = div1.numDivisionID
      JOIN CompanyInfo Com1 ON Com1.numCompanyId = div1.numCompanyID
      JOIN AddressDetails AD ON AD.numDomainID = div1.numDomainID AND AD.numRecordID = div1.numDivisionID
      WHERE  D1.numDomainId = v_numDomainID
      AND AD.tintAddressOf = 2 AND AD.tintAddressType = 1 AND AD.bitIsPrimary = true;


			--************Employer Shipping Address************ --(For SO take Domain Address)
      open SWV_RefCur4 for
      SELECT  '<pre>' || coalesce(AD.vcStreet,'') || '</pre>' || coalesce(AD.vcCity,'') || ', ' || coalesce(fn_GetState(AD.numState),'') || ' ' || coalesce(AD.vcPostalCode,'') || ' <br>' || coalesce(fn_GetListItemName(AD.numCountry),'') AS vcFullAddress,
					coalesce(AD.vcStreet,'') AS vcStreet,
					coalesce(AD.vcCity,'') AS vcCity,
					coalesce(fn_GetState(AD.numState),'') AS vcState,
					coalesce(AD.vcPostalCode,'') AS vcPostalCode,
					coalesce(fn_GetListItemName(AD.numCountry),'') AS vcCountry,
					coalesce(AD.numCountry,0) AS numCountry,
						coalesce(AD.numState,0) AS numState,
					coalesce(AD.vcAddressName,'') AS vcAddressName,vcCompanyName
					,(CASE
      WHEN coalesce(bitAltContact,false) = true
      THEN coalesce(vcAltContact,'')
      ELSE(CASE
         WHEN coalesce(numContact,0) > 0
         THEN
            coalesce((SELECT  CONCAT(vcFirstName,' ',vcLastname) FROM AdditionalContactsInformation WHERE numDomainID = v_numDomainID AND numContactId = numContact LIMIT 1),'')
         ELSE
            coalesce((SELECT  CONCAT(vcFirstName,' ',vcLastname) FROM AdditionalContactsInformation WHERE numDomainID = v_numDomainID AND numDivisionId = AD.numRecordID AND coalesce(bitPrimaryContact,false) = true LIMIT 1),'')
         END)
      END) AS vcContact
      FROM Domain D1 JOIN DivisionMaster div1 ON D1.numDivisionId = div1.numDivisionID
      JOIN CompanyInfo Com1 ON Com1.numCompanyId = div1.numCompanyID
      JOIN AddressDetails AD ON AD.numDomainID = div1.numDomainID AND AD.numRecordID = div1.numDivisionID
      WHERE AD.numDomainID = v_numDomainID
      AND AD.tintAddressOf = 2 AND AD.tintAddressType = 2 AND AD.bitIsPrimary = true;


			 --************Customer Billing Address************
      open SWV_RefCur5 for
      SELECT  '<pre>' || coalesce(AD.vcStreet,'') || '</pre>' || coalesce(AD.vcCity,'') || ', ' || coalesce(fn_GetState(AD.numState),'') || ' ' || coalesce(AD.vcPostalCode,'') || ' <br>' || coalesce(fn_GetListItemName(AD.numCountry),'') AS vcFullAddress,
						coalesce(AD.vcStreet,'') AS vcStreet,
						coalesce(AD.vcCity,'') AS vcCity,
						coalesce(fn_GetState(AD.numState),'') AS vcState,
						coalesce(AD.vcPostalCode,'') AS vcPostalCode,
						coalesce(fn_GetListItemName(AD.numCountry),'') AS vcCountry,
						coalesce(AD.numCountry,0) AS numCountry,
						coalesce(AD.numState,0) AS numState,
						coalesce(AD.vcAddressName,'') AS vcAddressName,
            			CMP.vcCompanyName
						,(CASE
      WHEN coalesce(bitAltContact,false) = true
      THEN coalesce(vcAltContact,'')
      ELSE(CASE
         WHEN coalesce(numContact,0) > 0
         THEN
            coalesce((SELECT  CONCAT(vcFirstName,' ',vcLastname) FROM AdditionalContactsInformation WHERE numDomainID = v_numDomainID AND numContactId = numContact LIMIT 1),'')
         ELSE
            coalesce((SELECT  CONCAT(vcFirstName,' ',vcLastname) FROM AdditionalContactsInformation WHERE numDomainID = v_numDomainID AND numDivisionId = AD.numRecordID AND coalesce(bitPrimaryContact,false) = true LIMIT 1),'')
         END)  END) AS vcContact
      FROM AddressDetails AD
      JOIN DivisionMaster DM ON DM.numDivisionID = AD.numRecordID
      JOIN CompanyInfo CMP ON DM.numCompanyID = CMP.numCompanyId and CMP.numDomainID = AD.numDomainID
      WHERE AD.numDomainID = v_numDomainID AND AD.numRecordID = v_numDivisionID
      AND AD.tintAddressOf = 2 AND AD.tintAddressType = 1 AND AD.bitIsPrimary = true;


			--************Customer Shipping Address************
      open SWV_RefCur6 for
      SELECT  '<pre>' || coalesce(AD.vcStreet,'') || '</pre>' || coalesce(AD.vcCity,'') || ', ' || coalesce(fn_GetState(AD.numState),'') || ' ' || coalesce(AD.vcPostalCode,'') || ' <br>' || coalesce(fn_GetListItemName(AD.numCountry),'') AS vcFullAddress,
					coalesce(AD.vcStreet,'') AS vcStreet,
					coalesce(AD.vcCity,'') AS vcCity,
					coalesce(fn_GetState(AD.numState),'') AS vcState,
					coalesce(AD.vcPostalCode,'') AS vcPostalCode,
					coalesce(fn_GetListItemName(AD.numCountry),'') AS vcCountry,
					coalesce(AD.numCountry,0) AS numCountry,
						coalesce(AD.numState,0) AS numState,
					coalesce(AD.vcAddressName,'') AS vcAddressName,
					CMP.vcCompanyName
					,(CASE
      WHEN coalesce(bitAltContact,false) = true
      THEN coalesce(vcAltContact,'')
      ELSE(CASE
         WHEN coalesce(numContact,0) > 0
         THEN
            coalesce((SELECT  CONCAT(vcFirstName,' ',vcLastname) FROM AdditionalContactsInformation WHERE numDomainID = v_numDomainID AND numContactId = numContact LIMIT 1),'')
         ELSE
            coalesce((SELECT  CONCAT(vcFirstName,' ',vcLastname) FROM AdditionalContactsInformation WHERE numDomainID = v_numDomainID AND numDivisionId = AD.numRecordID AND coalesce(bitPrimaryContact,false) = true LIMIT 1),'')
         END)  END) AS vcContact
      FROM AddressDetails AD
      JOIN DivisionMaster DM ON DM.numDivisionID = AD.numRecordID
      JOIN CompanyInfo CMP ON DM.numCompanyID = CMP.numCompanyId and CMP.numDomainID = AD.numDomainID
      WHERE AD.numDomainID = v_numDomainID AND AD.numRecordID = v_numDivisionID
      AND AD.tintAddressOf = 2 AND AD.tintAddressType = 2 AND AD.bitIsPrimary = true;
   end if;
/****** Object:  StoredProcedure [dbo].[USP_OppManage]    Script Date: 07/26/2008 16:20:19 ******/
   RETURN;
END; $$;


