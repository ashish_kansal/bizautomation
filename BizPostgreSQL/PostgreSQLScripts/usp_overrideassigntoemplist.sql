-- Stored procedure definition script USP_OverrideAssigntoEmpList for PostgreSQL
CREATE OR REPLACE FUNCTION USP_OverrideAssigntoEmpList(v_numDomainId NUMERIC DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT
   cast(O.numContactId as VARCHAR(255)),A.vcFirstName || ' ' || A.vcLastname as Name,cast(O.numAssignedContact as VARCHAR(255))
   FROM
   OverrideAssignTo AS O
   LEFT JOIN
   AdditionalContactsInformation AS A
   ON
   O.numContactId = A.numContactId
   WHERE
   O.numDomainId = v_numDomainId
   ORDER BY
   A.vcFirstName;
END; $$;












