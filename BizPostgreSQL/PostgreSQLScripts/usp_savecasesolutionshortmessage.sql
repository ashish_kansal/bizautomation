-- Stored procedure definition script USP_SaveCaseSolutionShortMessage for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_SaveCaseSolutionShortMessage(v_numDomainID BIGINT ,      
v_numCaseId BIGINT   ,                   
v_vcSolutionShortMessage VARCHAR(250) DEFAULT null ,
v_numUserCntID BIGINT DEFAULT NULL)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   UPDATE Cases SET vcSolutionShortMessage = v_vcSolutionShortMessage WHERE numDomainID = v_numDomainID AND numCaseId = v_numCaseId;
   insert into Comments(numCaseID,vcHeading,txtComments,numCreatedby,bintCreatedDate,numStageID)
values(v_numCaseId,'',v_vcSolutionShortMessage,v_numUserCntID,TIMEZONE('UTC',now()),NULL);


RETURN;
END; $$;
/****** Object:  StoredProcedure [dbo].[USP_ProjectsDTLPL]    Script Date: 07/26/2008 16:20:40 ******/



