-- Stored procedure definition script USP_ManageTimeAndExpense for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ManageTimeAndExpense(INOUT v_numCategoryHDRID NUMERIC(18,0) DEFAULT 0 ,
    v_tintTEType SMALLINT DEFAULT 0,
    v_numCategory NUMERIC(9,0) DEFAULT 0,
    v_dtFromDate TIMESTAMP DEFAULT NULL,
    v_dtToDate TIMESTAMP DEFAULT NULL,
    v_bitFromFullDay BOOLEAN DEFAULT NULL,
    v_bitToFullDay BOOLEAN DEFAULT NULL,
    v_monAmount DECIMAL(20,5) DEFAULT NULL,
    v_numOppId NUMERIC(9,0) DEFAULT 0,
    v_numDivisionID NUMERIC(9,0) DEFAULT 0,
    v_txtDesc TEXT DEFAULT '',
    v_numType NUMERIC(9,0) DEFAULT 0,
    v_numUserCntID NUMERIC(9,0) DEFAULT 0,
    v_numDomainID NUMERIC(9,0) DEFAULT 0,
    v_numCaseId NUMERIC DEFAULT 0,
    v_numProId NUMERIC DEFAULT 0,
    v_numContractId NUMERIC DEFAULT 0,
    v_numOppBizDocsId NUMERIC DEFAULT 0,
    v_numStageId NUMERIC DEFAULT 0,
    v_bitReimburse BOOLEAN DEFAULT false,
    INOUT v_bitLeaveTaken BOOLEAN DEFAULT false ,
    v_numOppItemID NUMERIC DEFAULT 0,
	v_numExpId NUMERIC DEFAULT 0,
	v_numApprovalComplete INTEGER DEFAULT 0,
	v_numServiceItemID INTEGER DEFAULT 0,
	v_numClassID INTEGER DEFAULT 0,
	v_numCreatedBy INTEGER DEFAULT 0)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_records  NUMERIC(10,0);      
   v_monTotAmount  DECIMAL(20,5);
   v_numJournalId  NUMERIC(18,0);
BEGIN
   PERFORM USP_ValidateFinancialYearClosingDate(v_numDomainID,v_dtFromDate);


   IF v_numCategory = 1 AND  v_numType NOT IN(3,4) AND EXISTS(SELECT
   numCategoryHDRID
   FROM
   timeandexpense
   WHERE
   numDomainID = v_numDomainID
   AND numUserCntID = v_numUserCntID
   AND numCategoryHDRID <> v_numCategoryHDRID
   AND numCategory IN(3,4)
   AND CAST(dtFromDate AS DATE) = CAST(v_dtFromDate AS DATE)) then
	
      RAISE EXCEPTION 'LEAVE_ENTRY_EXISTS';
   ELSEIF coalesce(v_numCategoryHDRID,0) = 0 AND  (v_numCategory <> 1 OR (v_numCategory = 1 AND (v_numType NOT IN(3,4) OR (v_numCategory IN(3,4) AND NOT EXISTS(SELECT
   numCategoryHDRID
   FROM
   timeandexpense
   WHERE
   numDomainID = v_numDomainID
   AND numUserCntID = v_numUserCntID
   AND numCategoryHDRID <> v_numCategoryHDRID
   AND numCategory IN(3,4)
   AND CAST(dtFromDate AS DATE) = CAST(v_dtFromDate AS DATE))))))
   then
	
      INSERT INTO timeandexpense(tintTEType,
            numCategory,
            dtFromDate,
            dtToDate,
            bitFromFullDay,
            bittofullday,
            monAmount,
            numOppId,
            numDivisionID,
            txtDesc,
            numtype,
            numUserCntID,
            numDomainID,
            numCaseId,
            numProId,
            numcontractId,
            numOppBizDocsId,
            numTCreatedBy,
            numtModifiedBy,
            dtTCreatedOn,
            dtTModifiedOn,
            numStageId,
            bitReimburse,
			numOppItemID,
			numExpId,
			numApprovalComplete,
			numServiceItemID,
			numClassID)
        VALUES(v_tintTEType,
            v_numCategory,
            v_dtFromDate,
            v_dtToDate,
            v_bitFromFullDay,
            v_bitToFullDay,
            v_monAmount,
            v_numOppId,
            v_numDivisionID,
            v_txtDesc,
            v_numType,
            v_numUserCntID,
            v_numDomainID,
            v_numCaseId,
            v_numProId,
            v_numContractId,
            v_numOppBizDocsId,
            v_numCreatedBy,
            v_numCreatedBy,
            TIMEZONE('UTC',now()),
            TIMEZONE('UTC',now()),
            v_numStageId,
            v_bitReimburse,
			v_numOppItemID,
			v_numExpId,
			v_numApprovalComplete,
			v_numServiceItemID,
			v_numClassID);
		
      v_numCategoryHDRID := CURRVAL('TimeAndExpense_seq');
   ELSEIF coalesce(v_numCategoryHDRID,0) > 0
   then
	
      UPDATE
      timeandexpense
      SET
      dtFromDate = v_dtFromDate,dtToDate = v_dtToDate,monAmount = v_monAmount,numServiceItemID = v_numServiceItemID,
      numClassID = v_numClassID,numDivisionID = v_numDivisionID,
      numOppId = v_numOppId,txtDesc=v_txtDesc
      WHERE
      numCategoryHDRID = v_numCategoryHDRID;
   end if;
   RETURN;
END; $$;


