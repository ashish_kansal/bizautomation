DROP FUNCTION IF EXISTS USP_OpportunityBizDocs_AutoFulFill;

CREATE OR REPLACE FUNCTION USP_OpportunityBizDocs_AutoFulFill(v_numDomainID NUMERIC(18,0),
	v_numUserCntID NUMERIC(18,0),
	v_numOppId NUMERIC(18,0),
	v_numOppBizDocID NUMERIC(18,0))
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_vcItemType  CHAR(1);
   v_bitDropShip  BOOLEAN;
   v_numOppItemID  INTEGER;
   v_numWarehouseItemID  INTEGER;
   v_numQty  DOUBLE PRECISION;
   v_numQtyShipped  DOUBLE PRECISION;
   v_bitSerial  BOOLEAN;
   v_bitLot  BOOLEAN;
   v_bitAssembly  BOOLEAN;
   v_bitKit  BOOLEAN;
   v_tintCommitAllocation  SMALLINT;
   v_bitAllocateInventoryOnPickList  BOOLEAN;

   v_i  INTEGER DEFAULT 1;
   v_COUNT  INTEGER;  
   v_numNewQtyShipped  DOUBLE PRECISION;

	/* VERIFY BIZDOC IS EXISTS OR NOT */
   v_vcDescription  VARCHAR(300);
   v_numAllocation  DOUBLE PRECISION;
   v_numOnHand  DOUBLE PRECISION;

   v_j  INTEGER DEFAULT 0;
   v_ChildCount  INTEGER;
   v_vcChildDescription  VARCHAR(300);
   v_numChildAllocation  DOUBLE PRECISION;
   v_numChildOnHand  DOUBLE PRECISION;
   v_numOppChildItemID  INTEGER;
   v_numChildItemCode  INTEGER;
   v_bitChildIsKit  BOOLEAN;
   v_numChildWarehouseItemID  INTEGER;
   v_numChildQty  DOUBLE PRECISION;
   v_numChildQtyShipped  DOUBLE PRECISION;

   v_k  INTEGER DEFAULT 1;
   v_CountKitChildItems  INTEGER;
   v_vcKitChildDescription  VARCHAR(300);
   v_numKitChildAllocation  DOUBLE PRECISION;
   v_numKitChildOnHand  DOUBLE PRECISION;
   v_numOppKitChildItemID  NUMERIC(18,0);
   v_numKitChildItemCode  NUMERIC(18,0);
   v_numKitChildWarehouseItemID  NUMERIC(18,0);
   v_numKitChildQty  DOUBLE PRECISION;
   v_numKitChildQtyShipped  DOUBLE PRECISION;

   my_ex_state TEXT;
	my_ex_message TEXT;
	my_ex_detail TEXT;
	my_ex_hint TEXT;
	my_ex_ctx TEXT;
   SWV_RCur REFCURSOR;
   v_l  INTEGER DEFAULT 1;
   v_lCount  INTEGER;
   v_numTempWarehouseItemID  NUMERIC(18,0);
   v_vcSerialLot  VARCHAR(100);
   v_vcTempDescription  VARCHAR(500);
   v_numTempQty  DOUBLE PRECISION;
BEGIN
   BEGIN
      IF NOT EXISTS(SELECT numOppBizDocsId FROM OpportunityBizDocs WHERE numOppBizDocsId = v_numOppBizDocID) then
	
         RAISE EXCEPTION 'FULFILLMENT_BIZDOC_IS_DELETED';
      end if;

	/* VERIFY WHETHER BIZDOC IS ALREADY FULLFILLED BY SOMEBODY ELSE */
      IF EXISTS(SELECT numOppBizDocsId FROM OpportunityBizDocs WHERE numOppBizDocsId = v_numOppBizDocID AND bitFulFilled = true) then
	
         RAISE EXCEPTION 'FULFILLMENT_BIZDOC_ALREADY_FULFILLED';
      end if;

	--IF EXISTS(SELECT numWOId FROm WorkOrder WHERE numOppId=@numOppId AND numWOStatus <> 23184) --WORK ORDER NOT YET COMPLETED
	--BEGIN
	--	RAISERROR('WORK_ORDER_NOT_COMPLETED',16,1)
	--END

      select   coalesce(tintCommitAllocation,1) INTO v_tintCommitAllocation FROM Domain WHERE numDomainId = v_numDomainID;
      select   coalesce(bitAllocateInventoryOnPickList,false) INTO v_bitAllocateInventoryOnPickList FROM OpportunityMaster INNER JOIN DivisionMaster ON OpportunityMaster.numDivisionId = DivisionMaster.numDivisionID WHERE numOppId = v_numOppId;
      -- BEGIN TRANSACTION


		-- CHANGE BIZDIC QTY TO AVAIALLBE QTY TO SHIP
		UPDATE
      OpportunityBizDocItems OBI
      SET
      numUnitHour =(CASE
      WHEN OM.tintopptype = 1 AND OM.tintoppstatus = 1
      THEN(CASE
         WHEN (I.charItemType = 'P' AND coalesce(OBI.bitDropShip,false) = false AND coalesce(bitAsset,false) = false)
         THEN(CASE WHEN OBI.numUnitHour <= GetOrderItemAvailableQtyToShip(OBI.numOppItemID,OBI.numWarehouseItmsID,coalesce(I.bitKitParent,false),v_tintCommitAllocation,
            v_bitAllocateInventoryOnPickList) THEN OBI.numUnitHour ELSE GetOrderItemAvailableQtyToShip(OBI.numOppItemID,OBI.numWarehouseItmsID,coalesce(I.bitKitParent,false),v_tintCommitAllocation,
               v_bitAllocateInventoryOnPickList) END)
         ELSE OBI.numUnitHour
         END)
      ELSE OBI.numUnitHour
      END)
      FROM
      OpportunityItems OI INNER JOIN OpportunityMaster OM ON OI.numOppId = OM.numOppId INNER JOIN WareHouseItems WI ON OI.numWarehouseItmsID = WI.numWareHouseItemID, Item I
      WHERE
      OBI.numOppItemID = OI.numoppitemtCode AND OBI.numItemCode = I.numItemCode AND numOppBizDocID = v_numOppBizDocID;

		--CHANGE TOTAL AMOUNT
      UPDATE
      OpportunityBizDocItems OBI
      SET
      monTotAmount =(OI.monTotAmount/OI.numUnitHour)*coalesce(OBI.numUnitHour,0),monTotAmtBefDiscount =(OI.monTotAmtBefDiscount/OI.numUnitHour)*coalesce(OBI.numUnitHour,0)
      FROM
      OpportunityItems OI INNER JOIN OpportunityMaster OM ON OI.numOppId = OM.numOppId, WareHouseItems WI, Item I
      WHERE
      OBI.numOppItemID = OI.numoppitemtCode AND OBI.numWarehouseItmsID = WI.numWareHouseItemID AND OBI.numItemCode = I.numItemCode AND numOppBizDocID = v_numOppBizDocID;
      BEGIN
         CREATE TEMP SEQUENCE tt_TempFinalTable_seq INCREMENT BY 1 START WITH 1;
         EXCEPTION WHEN OTHERS THEN
            NULL;
      END;
      DROP TABLE IF EXISTS tt_TEMPFINALTABLE CASCADE;
      CREATE TEMPORARY TABLE tt_TEMPFINALTABLE
      (
         ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1),
         numItemCode NUMERIC(18,0),
         vcItemType CHAR(1),
         bitSerial BOOLEAN,
         bitLot BOOLEAN,
         bitAssembly BOOLEAN,
         bitKit BOOLEAN,
         numOppItemID NUMERIC(18,0),
         numWarehouseItemID NUMERIC(18,0),
         numQtyShipped DOUBLE PRECISION,
         numQty DOUBLE PRECISION,
         bitDropShip BOOLEAN
      );
      INSERT INTO tt_TEMPFINALTABLE(numItemCode,
			vcItemType,
			bitSerial,
			bitLot,
			bitAssembly,
			bitKit,
			numOppItemID,
			numWarehouseItemID,
			numQtyShipped,
			numQty,
			bitDropShip)
      SELECT
      OpportunityBizDocItems.numItemCode,
			coalesce(Item.charItemType,''),
			coalesce(Item.bitSerialized,false),
			coalesce(Item.bitLotNo,false),
			coalesce(Item.bitAssembly,false),
			coalesce(Item.bitKitParent,false),
			coalesce(OpportunityItems.numoppitemtCode,0),
			coalesce(OpportunityBizDocItems.numWarehouseItmsID,0),
			coalesce(OpportunityItems.numQtyShipped,0),
			coalesce(OpportunityBizDocItems.numUnitHour,0),
			coalesce(OpportunityItems.bitDropShip,false)
      FROM
      OpportunityBizDocs
      INNER JOIN
      OpportunityBizDocItems
      ON
      OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
      INNER JOIN
      OpportunityItems
      ON
      OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtCode
      INNER JOIN
      Item
      ON
      OpportunityBizDocItems.numItemCode = Item.numItemCode
      WHERE
      numOppBizDocsId = v_numOppBizDocID;

		/* VALIDATE ALL THE DETAILS BEFORE FULFILLING ITEMS */
      select   COUNT(*) INTO v_COUNT FROM tt_TEMPFINALTABLE;
      WHILE v_i <= v_COUNT LOOP
         select   numOppItemID, numWarehouseItemID, numQty, bitSerial, bitLot INTO v_numOppItemID,v_numWarehouseItemID,v_numQty,v_bitSerial,v_bitLot FROM
         tt_TEMPFINALTABLE WHERE
         ID = v_i;

			--IF ITEM IS SERIAL ITEM CHEKC THAT VALID AND REQUIRED NUMBER OF SERIALS ARE PROVIDED BY USER
         IF v_bitSerial = true then
			
            IF coalesce((SELECT SUM(numQty) FROM OppWarehouseSerializedItem WHERE numOppID = v_numOppId AND numOppItemID = v_numOppItemID AND coalesce(numOppBizDocsId,0) = 0),0) <> v_numQty then
				
               RAISE EXCEPTION 'REQUIRED_SERIALS_NOT_PROVIDED';
            ELSE
               IF EXISTS(SELECT
               OppWarehouseSerializedItem.numWarehouseItmsDTLID
               FROM
               OppWarehouseSerializedItem
               LEFT JOIN
               WareHouseItmsDTL
               ON
               OppWarehouseSerializedItem.numWarehouseItmsDTLID = WareHouseItmsDTL.numWareHouseItmsDTLID
               WHERE
               numOppID = v_numOppId
               AND numOppItemID = v_numOppItemID
               AND coalesce(numOppBizDocsId,0) = 0
               AND WareHouseItmsDTL.numWareHouseItmsDTLID IS NULL) then
					
                  RAISE EXCEPTION 'INVALID_SERIAL_NUMBERS';
               end if;
            end if;
         end if;
         IF v_bitLot = true then
			
            IF coalesce((SELECT SUM(numQty) FROM OppWarehouseSerializedItem WHERE numOppID = v_numOppId AND numOppItemID = v_numOppItemID AND coalesce(numOppBizDocsId,0) = 0),0) <> v_numQty then
				
               RAISE EXCEPTION 'REQUIRED_LOTNO_NOT_PROVIDED';
            ELSE
               IF EXISTS(SELECT
               OppWarehouseSerializedItem.numWarehouseItmsDTLID
               FROM
               OppWarehouseSerializedItem
               LEFT JOIN
               WareHouseItmsDTL
               ON
               OppWarehouseSerializedItem.numWarehouseItmsDTLID = WareHouseItmsDTL.numWareHouseItmsDTLID
               WHERE
               numOppID = v_numOppId
               AND numOppItemID = v_numOppItemID
               AND coalesce(numOppBizDocsId,0) = 0
               AND WareHouseItmsDTL.numWareHouseItmsDTLID IS NULL) then
					
                  RAISE EXCEPTION 'INVALID_LOT_NUMBERS';
               end if;
            end if;
         end if;
         v_i := v_i::bigint+1;
      END LOOP;
      v_i := 1;
      select   COUNT(*) INTO v_COUNT FROM tt_TEMPFINALTABLE;
      DROP TABLE IF EXISTS tt_TEMPSERIALLOT CASCADE;
      CREATE TEMPORARY TABLE tt_TEMPSERIALLOT
      (
         ID INTEGER,
         numWarehouseItemID NUMERIC(18,0),
         vcSerialLot VARCHAR(100),
         numQty DOUBLE PRECISION
      );
      DROP TABLE IF EXISTS tt_TEMPKITSUBITEMS CASCADE;
      CREATE TEMPORARY TABLE tt_TEMPKITSUBITEMS
      (
         ID INTEGER,
         numOppChildItemID NUMERIC(18,0),
         numChildItemCode NUMERIC(18,0),
         bitChildIsKit BOOLEAN,
         numChildWarehouseItemID NUMERIC(18,0),
         numChildQty DOUBLE PRECISION,
         numChildQtyShipped DOUBLE PRECISION
      );
      DROP TABLE IF EXISTS tt_TEMPKITCHILDKITSUBITEMS CASCADE;
      CREATE TEMPORARY TABLE tt_TEMPKITCHILDKITSUBITEMS
      (
         ID INTEGER,
         numOppKitChildItemID NUMERIC(18,0),
         numKitChildItemCode NUMERIC(18,0),
         numKitChildWarehouseItemID NUMERIC(18,0),
         numKitChildQty DOUBLE PRECISION,
         numKitChildQtyShipped DOUBLE PRECISION
      );
      WHILE v_i <= v_COUNT LOOP
         select   vcItemType, bitSerial, bitLot, bitAssembly, bitKit, bitDropShip, numOppItemID, numQty, numQtyShipped, numWarehouseItemID INTO v_vcItemType,v_bitSerial,v_bitLot,v_bitAssembly,v_bitKit,v_bitDropShip,
         v_numOppItemID,v_numQty,v_numQtyShipped,v_numWarehouseItemID FROM
         tt_TEMPFINALTABLE WHERE
         ID = v_i;

			--MAKE INVENTIRY CHANGES IF INVENTORY ITEM
         IF (UPPER(v_vcItemType) = 'P' AND v_bitDropShip = false) then
			
            v_vcDescription := CONCAT('SO Qty Shipped (Qty:',CAST(v_numQty AS NUMERIC),' Shipped:',CAST(v_numQtyShipped AS NUMERIC),
            ')');
				 --  numeric(9, 0)
							 --  numeric(9, 0)
							 --  tinyint
							 --  varchar(100)
            IF v_bitKit = true then
				
					-- CLEAR DATA OF PREVIOUS ITERATION
               DELETE FROM tt_TEMPKITSUBITEMS;

					-- GET KIT SUB ITEMS DETAIL
               INSERT INTO tt_TEMPKITSUBITEMS(ID,
						numOppChildItemID,
						numChildItemCode,
						bitChildIsKit,
						numChildWarehouseItemID,
						numChildQty,
						numChildQtyShipped)
               SELECT
               ROW_NUMBER() OVER(ORDER BY numOppChildItemID),
						coalesce(numOppChildItemID,0),
						coalesce(I.numItemCode,0),
						coalesce(I.bitKitParent,false),
						coalesce(numWareHouseItemId,0),
						((coalesce(numQtyItemsReq_Orig,0)*coalesce(fn_UOMConversion(OKI.numUOMId,OKI.numChildItemID,v_numDomainID,I.numBaseUnit),1))*v_numQty),
						coalesce(numQtyShipped,0)
               FROM
               OpportunityKitItems OKI
               JOIN
               Item I
               ON
               OKI.numChildItemID = I.numItemCode
               WHERE
               charitemtype = 'P'
               AND coalesce(numWareHouseItemId,0) > 0
               AND OKI.numOppId = v_numOppId
               AND OKI.numOppItemID = v_numOppItemID;
               v_j := 1;
               select   COUNT(*) INTO v_ChildCount FROM tt_TEMPKITSUBITEMS;

					--LOOP OVER ALL CHILD ITEMS AND RELESE ALLOCATION
               WHILE v_j <= v_ChildCount LOOP
                  select   numOppChildItemID, numChildItemCode, bitChildIsKit, numChildWarehouseItemID, numChildQty, numChildQtyShipped INTO v_numOppChildItemID,v_numChildItemCode,v_bitChildIsKit,v_numChildWarehouseItemID,
                  v_numChildQty,v_numChildQtyShipped FROM
                  tt_TEMPKITSUBITEMS WHERE
                  ID = v_j;
                  select   coalesce(numAllocation,0), coalesce(numOnHand,0) INTO v_numChildAllocation,v_numChildOnHand FROM WareHouseItems WHERE numWareHouseItemID = v_numChildWarehouseItemID;

						-- IF CHILD ITEM IS KIT THEN LOOK OUT FOR ITS CHILD ITEMS WHICH ARE STORED IN OpportunityKitChildItems TABLE
                  IF v_bitChildIsKit = true then
						
							-- IF KIT IS ADDED WITHIN KIT THEN THIS SUB KIT HAS ITS OWN ITEM CONFIGURATION WHICH ID STORED IN OpportunityKitChildItems
                     IF EXISTS(SELECT numOppKitChildItemID FROM OpportunityKitChildItems WHERE numOppId = v_numOppId AND numOppItemID = v_numOppItemID AND numOppChildItemID = v_numOppChildItemID) then
							
								-- CLEAR DATA OF PREVIOUS ITERATION
                        DELETE FROM tt_TEMPKITCHILDKITSUBITEMS;

								-- GET SUB KIT SUB ITEMS DETAIL
                        INSERT INTO tt_TEMPKITCHILDKITSUBITEMS(ID,
									numOppKitChildItemID,
									numKitChildItemCode,
									numKitChildWarehouseItemID,
									numKitChildQty,
									numKitChildQtyShipped)
                        SELECT
                        ROW_NUMBER() OVER(ORDER BY numOppKitChildItemID),
									coalesce(numOppKitChildItemID,0),
									coalesce(OKCI.numItemID,0),
									coalesce(OKCI.numWareHouseItemId,0),
									((coalesce(OKCI.numQtyItemsReq_Orig,0)*coalesce(fn_UOMConversion(OKCI.numUOMId,OKCI.numItemID,v_numDomainID,I.numBaseUnit),1))*v_numChildQty),
									coalesce(numQtyShipped,0)
                        FROM
                        OpportunityKitChildItems OKCI
                        JOIN
                        Item I
                        ON
                        OKCI.numItemID = I.numItemCode
                        WHERE
                        charitemtype = 'P'
                        AND coalesce(numWareHouseItemId,0) > 0
                        AND OKCI.numOppId = v_numOppId
                        AND OKCI.numOppItemID = v_numOppItemID
                        AND OKCI.numOppChildItemID = v_numOppChildItemID;
                        v_k := 1;
                        select   COUNT(*) INTO v_CountKitChildItems FROM tt_TEMPKITCHILDKITSUBITEMS;
                        WHILE v_k <= v_CountKitChildItems LOOP
                           select   numOppKitChildItemID, numKitChildItemCode, numKitChildWarehouseItemID, numKitChildQty, numKitChildQtyShipped INTO v_numOppKitChildItemID,v_numKitChildItemCode,v_numKitChildWarehouseItemID,
                           v_numKitChildQty,v_numKitChildQtyShipped FROM
                           tt_TEMPKITCHILDKITSUBITEMS WHERE
                           ID = v_k;
                           select   coalesce(numAllocation,0), coalesce(numOnHand,0) INTO v_numKitChildAllocation,v_numKitChildOnHand FROM WareHouseItems WHERE numWareHouseItemID = v_numKitChildWarehouseItemID;
                           IF v_tintCommitAllocation = 1 OR (v_tintCommitAllocation = 2 AND v_bitAllocateInventoryOnPickList = true) then--When order is created OR Allocation on pick list add
									
										--REMOVE QTY FROM ALLOCATION
                              v_numKitChildAllocation := v_numKitChildAllocation -v_numKitChildQty;
                              IF v_numKitChildAllocation >= 0 then
										
                                 UPDATE
                                 WareHouseItems
                                 SET
                                 numAllocation = v_numKitChildAllocation,dtModified = LOCALTIMESTAMP
                                 WHERE
                                 numWareHouseItemID = v_numKitChildWarehouseItemID;
                              ELSE
                                 RAISE EXCEPTION 'NOTSUFFICIENTQTY_ALLOCATION';
                              end if;
                           ELSE
										--REMOVE QTY FROM ON HAND
                              v_numKitChildOnHand := v_numKitChildOnHand -v_numKitChildQty;
                              IF v_numKitChildOnHand >= 0 then
										
                                 UPDATE
                                 WareHouseItems
                                 SET
                                 numOnHand = v_numKitChildOnHand,dtModified = LOCALTIMESTAMP
                                 WHERE
                                 numWareHouseItemID = v_numKitChildWarehouseItemID;
                              ELSE
                                 RAISE EXCEPTION 'NOTSUFFICIENTQTY_ONHAND';
                              end if;
                           end if;
                           UPDATE
                           OpportunityKitChildItems
                           SET
                           numQtyShipped = coalesce(numQtyShipped,0)+coalesce(v_numKitChildQty,0)
                           WHERE
                           numOppKitChildItemID = v_numOppKitChildItemID
                           AND numOppChildItemID = v_numOppChildItemID
                           AND numOppId = v_numOppId
                           AND numOppItemID = v_numOppItemID;
                           v_vcKitChildDescription := CONCAT('SO CHILD KIT Qty Shipped (Qty:',CAST(v_numKitChildQty AS NUMERIC),' Shipped:',
                           CAST(v_numKitChildQtyShipped AS NUMERIC),')');
                           PERFORM USP_ManageWareHouseItems_Tracking(v_numWarehouseItemID := v_numKitChildWarehouseItemID,v_numReferenceID := v_numOppId,
                           v_tintRefType := 3::SMALLINT,v_vcDescription := v_vcKitChildDescription,
                           v_numModifiedBy := v_numUserCntID,v_numDomainID := v_numDomainID,
                           SWV_RefCur := null);
                           v_k := v_k::bigint+1;
                        END LOOP;
                     end if;
                     UPDATE
                     OpportunityKitItems
                     SET
                     numQtyShipped = coalesce(numQtyShipped,0)+coalesce(v_numChildQty,0)
                     WHERE
                     numOppChildItemID = v_numOppChildItemID
                     AND numOppId = v_numOppId
                     AND numOppItemID = v_numOppItemID;
                     v_vcChildDescription := CONCAT('SO KIT Qty Shipped (Qty:',CAST(v_numChildQty AS NUMERIC),' Shipped:',
                     CAST(v_numChildQtyShipped AS NUMERIC),')');
                     PERFORM USP_ManageWareHouseItems_Tracking(v_numWarehouseItemID := v_numChildWarehouseItemID,v_numReferenceID := v_numOppId,
                     v_tintRefType := 3::SMALLINT,v_vcDescription := v_vcChildDescription,
                     v_numModifiedBy := v_numUserCntID,v_numDomainID := v_numDomainID,
                     SWV_RefCur := null);
                  ELSE
                     IF v_tintCommitAllocation = 1 OR (v_tintCommitAllocation = 2 AND v_bitAllocateInventoryOnPickList = true) then--When order is created OR Allocation on pick list add
							
								--REMOVE QTY FROM ALLOCATION
                        v_numChildAllocation := v_numChildAllocation -v_numChildQty;
                        IF v_numChildAllocation >= 0 then
								
                           UPDATE
                           WareHouseItems
                           SET
                           numAllocation = v_numChildAllocation,dtModified = LOCALTIMESTAMP
                           WHERE
                           numWareHouseItemID = v_numChildWarehouseItemID;
                        ELSE
                           RAISE EXCEPTION 'NOTSUFFICIENTQTY_ALLOCATION';
                        end if;
                     ELSE
								--REMOVE QTY FROM ONHAND
                        v_numChildOnHand := v_numChildOnHand -v_numChildQty;
                        IF v_numChildOnHand >= 0 then
								
                           UPDATE
                           WareHouseItems
                           SET
                           numOnHand = v_numChildOnHand,dtModified = LOCALTIMESTAMP
                           WHERE
                           numWareHouseItemID = v_numChildWarehouseItemID;
                        ELSE
                           RAISE EXCEPTION 'NOTSUFFICIENTQTY_ONHAND';
                        end if;
                     end if;
                     UPDATE
                     OpportunityKitItems
                     SET
                     numQtyShipped = coalesce(numQtyShipped,0)+coalesce(v_numChildQty,0)
                     WHERE
                     numOppChildItemID = v_numOppChildItemID
                     AND numOppId = v_numOppId
                     AND numOppItemID = v_numOppItemID;
                     v_vcChildDescription := CONCAT('SO KIT Qty Shipped (Qty:',CAST(v_numChildQty AS NUMERIC),' Shipped:',
                     CAST(v_numChildQtyShipped AS NUMERIC),')');
                     PERFORM USP_ManageWareHouseItems_Tracking(v_numWarehouseItemID := v_numChildWarehouseItemID,v_numReferenceID := v_numOppId,
                     v_tintRefType := 3::SMALLINT,v_vcDescription := v_vcChildDescription,
                     v_numModifiedBy := v_numUserCntID,v_numDomainID := v_numDomainID,
                     SWV_RefCur := null);
                  end if;
                  v_j := v_j::bigint+1;
               END LOOP;
            ELSE
               IF v_bitSerial = true OR v_bitSerial = true then
                  DELETE FROM tt_TEMPSERIALLOT;
                  INSERT INTO tt_TEMPSERIALLOT(ID
							,numWarehouseItemID
							,vcSerialLot
							,numQty)
                  SELECT
                  ROW_NUMBER() OVER(ORDER BY OWSI.numWarehouseItmsDTLID)
							,OWSI.numWarehouseItmsID
							,coalesce(WID.vcSerialNo,'')
							,coalesce(OWSI.numQty,0)
                  FROM
                  OppWarehouseSerializedItem OWSI
                  INNER JOIN
                  WareHouseItmsDTL WID
                  ON
                  OWSI.numWarehouseItmsDTLID = WID.numWareHouseItmsDTLID
                  INNER JOIN
                  WareHouseItems WI
                  ON
                  OWSI.numWarehouseItmsID = WI.numWareHouseItemID
                  WHERE
                  numOppID = v_numOppId
                  AND numOppItemID = v_numOppItemID
                  AND coalesce(numOppBizDocsId,0) = 0;
                  select   COUNT(*) INTO v_lCount FROM tt_TEMPSERIALLOT;
                  WHILE v_l <= v_lCount LOOP
                     select   numWarehouseItemID, vcSerialLot, numQty INTO v_numTempWarehouseItemID,v_vcSerialLot,v_numTempQty FROM
                     tt_TEMPSERIALLOT WHERE
                     ID = v_l;
                     IF v_numWarehouseItemID <> v_numTempWarehouseItemID then
							
                        IF coalesce((SELECT numOnHand FROM WareHouseItems WHERE numWareHouseItemID = v_numTempWarehouseItemID),0) >= v_numTempQty then
								
                           UPDATE
                           WareHouseItems
                           SET
                           numOnHand = coalesce(numOnHand,0) -v_numTempQty
                           WHERE
                           numWareHouseItemID = v_numTempWarehouseItemID;
									 --  numeric(9, 0)
									 --  numeric(9, 0)
									 --  tinyint
									 --  varchar(100)
                           v_vcTempDescription := CONCAT('Serial/Lot(',v_vcSerialLot,') Picked Qty(',v_numTempQty,')');
                           PERFORM USP_ManageWareHouseItems_Tracking(v_numWarehouseItemID := v_numTempWarehouseItemID,v_numReferenceID := v_numOppId,
                           v_tintRefType := 3::SMALLINT,v_vcDescription := v_vcTempDescription,
                           v_numModifiedBy := v_numUserCntID,v_numDomainID := v_numDomainID,
                           SWV_RefCur := null);
                        ELSE
                           RAISE EXCEPTION 'NOTSUFFICIENTQTY_ONHAND';
                        end if;
                        UPDATE
                        WareHouseItems
                        SET
                        numBackOrder =(CASE WHEN v_numTempQty >= coalesce(numBackOrder,0) THEN 0 ELSE(coalesce(numBackOrder,0) -v_numTempQty) END),numAllocation = coalesce(numAllocation,0)+(CASE WHEN v_numTempQty >= coalesce(numBackOrder,0) THEN coalesce(numBackOrder,0) ELSE 0 END),numOnHand = coalesce(numOnHand,0)+(CASE WHEN v_numTempQty >= coalesce(numBackOrder,0) THEN(v_numTempQty -coalesce(numBackOrder,0)) ELSE 0 END)
                        WHERE
                        numWareHouseItemID = v_numWarehouseItemID;
								 --  numeric(9, 0)
								 --  numeric(9, 0)
								 --  tinyint
								 --  varchar(100)
                        v_vcTempDescription := CONCAT('Serial/Lot(',v_vcSerialLot,') Received Qty(',v_numTempQty,')');
                        PERFORM USP_ManageWareHouseItems_Tracking(v_numWarehouseItemID := v_numWarehouseItemID,v_numReferenceID := v_numOppId,
                        v_tintRefType := 3::SMALLINT,v_vcDescription := v_vcTempDescription,
                        v_numModifiedBy := v_numUserCntID,v_numDomainID := v_numDomainID,SWV_RefCur := null);
                     end if;
                     v_l := v_l::bigint+1;
                  END LOOP;
               end if;
               select   coalesce(numAllocation,0), coalesce(numOnHand,0) INTO v_numAllocation,v_numOnHand FROM WareHouseItems WHERE numWareHouseItemID = v_numWarehouseItemID;
               IF v_tintCommitAllocation = 1 OR (v_tintCommitAllocation = 2 AND v_bitAllocateInventoryOnPickList = true) then--When order is created OR Allocation on pick list add
					
						--REMOVE QTY FROM ALLOCATION
                  v_numAllocation := v_numAllocation -v_numQty;
                  IF (v_numAllocation >= 0) then
						
                     UPDATE
                     WareHouseItems
                     SET
                     numAllocation = v_numAllocation,dtModified = LOCALTIMESTAMP
                     WHERE
                     numWareHouseItemID = v_numWarehouseItemID;
                  ELSE
                     RAISE EXCEPTION 'NOTSUFFICIENTQTY_ALLOCATION';
                  end if;
               ELSE
						--REMOVE QTY FROM ON HAND
                  v_numOnHand := v_numOnHand -v_numQty;
                  IF (v_numOnHand >= 0) then
						
                     UPDATE
                     WareHouseItems
                     SET
                     numOnHand = v_numOnHand,dtModified = LOCALTIMESTAMP
                     WHERE
                     numWareHouseItemID = v_numWarehouseItemID;
                  ELSE
                     RAISE EXCEPTION 'NOTSUFFICIENTQTY_ONHAND';
                  end if;
               end if;
            end if;
            PERFORM USP_ManageWareHouseItems_Tracking(v_numWarehouseItemID := v_numWarehouseItemID,v_numReferenceID := v_numOppId,
            v_tintRefType := 3::SMALLINT,v_vcDescription := v_vcDescription,
            v_numModifiedBy := v_numUserCntID,v_numDomainID := v_numDomainID,SWV_RefCur := null);
         end if;
         UPDATE
         OpportunityItems
         SET
         numQtyShipped = coalesce(numQtyShipped,0)+coalesce(v_numQty,0)
         WHERE
         numoppitemtCode = v_numOppItemID
         AND numOppId = v_numOppId;
         v_i := v_i::bigint+1;
      END LOOP;
      UPDATE OppWarehouseSerializedItem SET numOppBizDocsId = v_numOppBizDocID WHERE numOppID = v_numOppId AND coalesce(numOppBizDocsId,0) = 0;
      UPDATE OpportunityBizDocs SET bitFulFilled = true,dtShippedDate = TIMEZONE('UTC',now()) WHERE numOppBizDocsId = v_numOppBizDocID;
      -- COMMIT
EXCEPTION WHEN OTHERS THEN
         GET STACKED DIAGNOSTICS
			  my_ex_state   = RETURNED_SQLSTATE,
			  my_ex_message = MESSAGE_TEXT,
			  my_ex_detail  = PG_EXCEPTION_DETAIL,
			  my_ex_hint    = PG_EXCEPTION_HINT,
			  my_ex_ctx     = PG_EXCEPTION_CONTEXT;

			 RAISE EXCEPTION '% % % % %', my_ex_state, my_ex_message, my_ex_detail, my_ex_hint, my_ex_ctx;
   END;
   RETURN;
END; $$;



