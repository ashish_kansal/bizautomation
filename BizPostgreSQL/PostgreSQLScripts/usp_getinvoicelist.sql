CREATE OR REPLACE FUNCTION USP_GetInvoiceList(v_numDivisionID NUMERIC(9,0) DEFAULT 0 ,
 v_tintOppType SMALLINT DEFAULT 1,
 v_numDomainID NUMERIC(9,0) DEFAULT NULL, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
BEGIN
   IF (v_tintOppType = 1 OR  v_tintOppType = 2) then

      open SWV_RefCur for
      SELECT OM.numOppId,OM.vcpOppName,OBD.numOppBizDocsId,OBD.vcBizDocID,
		coalesce(OBD.monDealAmount*OM.fltExchangeRate,0) AS monDealAmount,coalesce((OBD.monAmountPaid*OM.fltExchangeRate),0) AS monAmountPaid,
		coalesce(OBD.monDealAmount*OM.fltExchangeRate -OBD.monAmountPaid*OM.fltExchangeRate,0) AS BalanceDue,
		FormatedDateFromDate(OBD.dtFromDate,OM.numDomainId) AS BillingDate,
		CASE coalesce(OM.bitBillingTerms,false)
      WHEN true THEN FormatedDateFromDate(OBD.dtFromDate+CAST(CAST(coalesce((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = coalesce(OM.intBillingDays,0)),0) AS INTEGER) || 'day' as interval),OM.numDomainId)
      WHEN false THEN FormatedDateFromDate(OBD.dtFromDate,OM.numDomainId)
      END AS DueDate,
        GetCreditTerms(OM.numOppId) as Credit,
		fn_GetContactName(OM.numCreatedBy)  as CreatedName,
		CAST(OM.bintCreatedDate AS TIMESTAMP) AS  dtCreatedDate,
		CAST(OM.bintCreatedDate AS TIMESTAMP) AS  numCreatedDate,
		coalesce(OM.numPClosingPercent,0) as monAmountPaid,
		coalesce(GetDealAmount(OM.numOppId,LOCALTIMESTAMP,0::NUMERIC),0) as TotalAmt,
		OM.numContactId,
		coalesce(OM.tintoppstatus,0) As tintOppStatus
      FROM OpportunityMaster OM
      JOIN OpportunityBizDocs OBD ON OM.numOppId = OBD.numoppid
      JOIN DivisionMaster D ON D.numDivisionID = OM.numDivisionId
      JOIN CompanyInfo C ON C.numCompanyId = D.numCompanyID
      WHERE OM.numDomainId = v_numDomainID AND OM.numDomainId = D.numDomainID AND OM.tintopptype = v_tintOppType
      AND D.numDivisionID = v_numDivisionID AND coalesce(bitAuthoritativeBizDocs,0) = 1
      AND OM.tintoppstatus = 1
      AND coalesce(OBD.tintDeferred,0) <> 1 AND  coalesce(OBD.monDealAmount*OM.fltExchangeRate,0) > 0
      AND coalesce(OBD.monDealAmount*OM.fltExchangeRate -OBD.monAmountPaid*OM.fltExchangeRate,0) > 0;
   end if; 
   IF v_tintOppType =  3 then
      open SWV_RefCur for
      SELECT 0 AS numOppId,'Bill' AS vcPOppName,0 AS numOppBizDocsId,'Bill' || CASE WHEN LENGTH(BH.vcReference) = 0 THEN '' ELSE '-' || BH.vcReference END AS vcBizDocID,
						   BH.monAmountDue as monDealAmount,
						   coalesce(BH.monAmtPaid,0) as monAmountPaid,
					       coalesce(BH.monAmountDue,0) -coalesce(BH.monAmtPaid,0) as BalanceDue,
					       FormatedDateFromDate(BH.dtBillDate,BH.numDomainId) AS BillingDate,
						   FormatedDateFromDate(BH.dtDueDate,BH.numDomainId) AS DueDate
      FROM
      BillHeader BH
      WHERE BH.numDomainId = v_numDomainID AND BH.numDivisionId = v_numDivisionID
      AND coalesce(BH.monAmountDue,0) -coalesce(BH.monAmtPaid,0) > 0;
   end if;
   RETURN;
END; $$;





