-- Function definition script fn_ItemUOMConversion for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION fn_ItemUOMConversion(v_numFromUnit NUMERIC,
	v_numItemCode NUMERIC,
	v_numDomainID NUMERIC)
RETURNS DECIMAL(18,5) LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numBaseUnit  NUMERIC(18,0);
   v_MultiplicationFactor  DECIMAL(18,5);
BEGIN
   select   coalesce(numBaseUnit,0) INTO v_numBaseUnit FROM Item WHERE numDomainID = v_numDomainID AND numItemCode = v_numItemCode;
	
   IF coalesce(v_numBaseUnit,0) = 0 then
      RETURN 1;
   end if;
 
   IF(v_numFromUnit = v_numBaseUnit) then
      RETURN 1;
   end if;
	
   with recursive CTE(FromUnit,LevelNum,numSourceUOM,numTargetUOM,numTargetUnit) AS(SELECT
   IUOM.numSourceUOM AS FromUnit,
			CAST(0 AS INTEGER) AS LevelNum,
			IUOM.numSourceUOM AS numSourceUOM,
			IUOM.numTargetUOM AS numTargetUOM,
			IUOM.numTargetUnit AS numTargetUnit
   FROM
   ItemUOMConversion IUOM
   WHERE
   IUOM.numDomainID = v_numDomainID
   AND IUOM.numItemCode = v_numItemCode
   AND IUOM.numSourceUOM = v_numFromUnit
   UNION ALL
   SELECT
   cte.FROMUnit AS FromUnit,
			LevelNum+1 AS LevelNum,
			IUOM.numSourceUOM AS numSourceUOM,
			IUOM.numTargetUOM AS numTargetUOM,
			(IUOM.numTargetUnit*cte.numTargetUnit) AS numTargetUnit
   FROM
   ItemUOMConversion IUOM
   INNER JOIN
   CTE cte
   ON
   IUOM.numSourceUOM = cte.numTargetUOM
   WHERE
   IUOM.numDomainID = v_numDomainID
   AND IUOM.numItemCode = v_numItemCode) select   numTargetUnit INTO v_MultiplicationFactor FROM CTE WHERE FROMUnit = v_numFromUnit AND numTargetUOM = v_numBaseUnit    LIMIT 1;

   IF EXISTS(SELECT v_MultiplicationFactor WHERE v_MultiplicationFactor IS NOT NULL) then
	
      RETURN v_MultiplicationFactor;
   end if; 

   RETURN 1;
END; $$;

