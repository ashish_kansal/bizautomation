CREATE OR REPLACE FUNCTION AfterInsertUpdateTimeAndExpense_TrFunc()
RETURNS TRIGGER LANGUAGE plpgsql
AS $$
BEGIN
	UPDATE 
		OpportunityMaster
	SET 
		monDealAmount = GetDealAmount(NEW.numOppId,TIMEZONE('UTC',now()),0::NUMERIC)
	WHERE 
		numOppId = NEW.numOppId;
   
	RETURN NULL;
END; $$;
CREATE TRIGGER AfterInsertUpdateTimeAndExpense AFTER INSERT OR UPDATE ON timeandexpense FOR EACH ROW EXECUTE PROCEDURE AfterInsertUpdateTimeAndExpense_TrFunc();
  

