-- Function definition script fn_GetChildCategory for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION fn_GetChildCategory(v_numAccountId NUMERIC)
RETURNS VARCHAR(8000) LANGUAGE plpgsql    
  
--Declare @numParentId numeric  
   AS $$
   DECLARE
   v_retval  VARCHAR(2000);
--Declare @numParent varchar(8000)  
--SET @numParentId = (SELECT numParntAcntId FROM Chart_Of_Accounts WHERE numAccountId = @numAccountId)          
--Set @retval = @numParentId   
--
--While @numParentId <> 1
--Begin  
--  Set @numParent = dbo.fn_GetChildCategory(@numParentId)
--  SELECT @numParentId=numParntAcntId FROM Chart_Of_Accounts WHERE numAccountId = @numParentId  
--End  
   
BEGIN
   Return v_retval;
END; $$;

