-- Stored procedure definition script USP_DelPriceBookRule for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_DelPriceBookRule(v_numPriceRuleID NUMERIC(9,0) DEFAULT 0)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   DELETE FROM PricingTable WHERE numPriceRuleID = v_numPriceRuleID;
   DELETE FROM PriceBookRuleItems WHERE numRuleID = v_numPriceRuleID;
   delete from PriceBookRuleDTL where numRuleID = v_numPriceRuleID;
   delete from PriceBookRules where numPricRuleID = v_numPriceRuleID;
   RETURN;
END; $$;




