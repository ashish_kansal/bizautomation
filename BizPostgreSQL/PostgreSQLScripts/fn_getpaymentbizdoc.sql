DROP FUNCTION IF EXISTS fn_GetPaymentBizDoc;

CREATE OR REPLACE FUNCTION fn_GetPaymentBizDoc(v_numBizDocsPaymentDetId NUMERIC(8,0),
 v_numDomainId INTEGER)
RETURNS VARCHAR(2000) LANGUAGE plpgsql
   AS $$
BEGIN
   RETURN COALESCE((SELECT 
						string_agg(CONCAT('Ref: ',vcReference,' Memo: ',vcMemo,' Chek No: ',numCheckNo,' Amount: ',monAmount),', ')
					FROM (SELECT
						numCheckNo,vcReference,vcMemo,SUM(OpportunityBizDocsPaymentDetails.monAmount) monAmount
					FROM
						OpportunityBizDocsPaymentDetails 
					INNER JOIN
						OpportunityBizDocsDetails
					ON 
						OpportunityBizDocsPaymentDetails.numBizDocsPaymentDetId = OpportunityBizDocsDetails.numBizDocsPaymentDetId
					WHERE
						numDomainId=v_numDomainId
						AND OpportunityBizDocsPaymentDetails.numBizDocsPaymentDetId=v_numBizDocsPaymentDetId
					GROUP BY 
						numCheckNo,vcReference,vcMemo) TEMP),'');
END; $$;


