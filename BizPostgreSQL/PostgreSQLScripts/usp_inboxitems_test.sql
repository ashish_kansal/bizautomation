-- Stored procedure definition script USP_InboxItems_test for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
--- Created By Anoop Jayaraj                                                      
CREATE OR REPLACE FUNCTION USP_InboxItems_test(v_PageSize INTEGER ,                            
v_CurrentPage INTEGER,                            
v_srchSubjectBody VARCHAR(100) DEFAULT '',                            
--@srchEmail as varchar(100) ='',                            
v_srchAttachmenttype VARCHAR(100) DEFAULT '',                                   
INOUT v_TotRecs INTEGER  DEFAULT NULL        ,                    
v_ToEmail VARCHAR(100) DEFAULT NULL  ,        
v_columnName VARCHAR(50) DEFAULT NULL ,        
v_columnSortOrder VARCHAR(4) DEFAULT NULL  ,  
v_numDomainId NUMERIC(9,0) DEFAULT NULL,  
v_numUserCntId NUMERIC(9,0) DEFAULT NULL  ,    
v_numNodeId NUMERIC(9,0) DEFAULT NULL,  
v_chrSource CHAR(1) DEFAULT NULL,
v_ClientTimeZoneOffset INTEGER DEFAULT 0, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_firstRec  INTEGER;                                                      
   v_lastRec  INTEGER;                                                      
   v_strSql  VARCHAR(8000);
BEGIN
   v_firstRec :=(v_CurrentPage::bigint -1)*v_PageSize::bigint;                                                      
   v_lastRec :=(v_CurrentPage::bigint*v_PageSize::bigint+1);       
 
   BEGIN
      CREATE TEMP SEQUENCE tt_Inbox_seq INCREMENT BY 1 START WITH 1;
      EXCEPTION WHEN OTHERS THEN
         NULL;
   END;
   DROP TABLE IF EXISTS tt_INBOX CASCADE;
   CREATE TEMPORARY TABLE tt_INBOX 
   (
      ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1),
      numEmailHstrID NUMERIC
   );
   INSERT INTO tt_INBOX(numEmailHstrID)
   SELECT numEmailHstrID FROM EmailHistory
   WHERE    numDomainID = v_numDomainId
   AND numUserCntId = v_numUserCntId
   AND numNodeId = v_numNodeId
   AND (chrSource = 'B'
   OR chrSource = v_chrSource)
   ORDER BY dtReceivedOn DESC; 

--SELECT * FROM [#Inbox] INNER JOIN [EmailHistory] ON [#Inbox].[numEmailHstrID] = [EmailHistory].[numEmailHstrID]
--WHERE [#Inbox].[ID] <20 AND [#Inbox].[ID] >0
       


   open SWV_RefCur for
   SELECT X.* 
--			dbo.GetEmaillAdd(X.numEmailHstrID, 4) AS FromEmail,
--            dbo.GetEmaillName(X.numEmailHstrID, 4) AS FromName,                             
--            dbo.GetEmaillAdd(X.numEmailHstrID, 1) AS ToEmail,
--            dbo.GetEmaillAdd(X.numEmailHstrID, 2) AS CCEmail,
--            dbo.GetEmaillAdd(X.numEmailHstrID, 3) AS BCCEmail,
--            dbo.FormatedDateFromDate(DATEADD(minute, 330, X.bintCreatedOn),
--                                     X.numDomainId) AS bintCreatedOn,
--			dbo.FormatedDateFromDate(X.dtReceivedOn, X.numDomainId) AS dtReceivedOn,
--            dbo.GetEmaillName(X.numEmailHstrID, 1) AS ToName
FROM(SELECT  I.ID,
			EmailHistory.numDomainID,
			EmailHistory.bintCreatedOn,
			EmailHistory.dtReceivedOn,
            EmailHistory.numEmailHstrID,
            coalesce(vcSubject,'') AS vcSubject,
            CAST(vcBody AS TEXT) AS vcBody,
            coalesce(vcItemId,'') AS ItemId,
            coalesce(vcChangeKey,'') AS ChangeKey,
            coalesce(bitIsRead,false) AS IsRead,
            coalesce(vcSize,0) AS vcSize,
            coalesce(bitHasAttachments,false) AS HasAttachments,
            coalesce(EmailHstrAttchDtls.vcFileName,'') AS AttachmentPath,
            coalesce(EmailHstrAttchDtls.vcAttachmentType,'') AS AttachmentType,
            coalesce(EmailHstrAttchDtls.vcAttachmentItemId,'') AS AttachmentItemId,
            coalesce(EmailHistory.tintType,1) AS type,
            coalesce(chrSource,'B') AS chrSource,
            coalesce(vcCategory,'white') AS vcCategory
      FROM    tt_INBOX I
      INNER JOIN EmailHistory ON EmailHistory.numEmailHstrID = I.numEmailHstrID
      LEFT JOIN EmailHstrAttchDtls ON EmailHistory.numEmailHstrID = EmailHstrAttchDtls.numEmailHstrID
      WHERE   I.ID > v_firstRec
      AND I.ID < v_lastRec) X 
--    UNION
--    SELECT  0 AS RowNumber,NULL,NULL,NULL,NULL,NULL,COUNT(*),NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,
--            NULL,NULL,NULL,NULL,NULL,1,NULL,NULL
--    FROM    [#Inbox]
   ORDER BY ID;



   return;                   
   if v_columnName = 'FromName' then
 
      v_columnName :=  'dbo.GetEmaillName(emailHistory.numEmailHstrID,4)';
   end if;        
   if v_columnName = 'FromEmail' then
 
      v_columnName := 'dbo.GetEmaillAdd(emailHistory.numEmailHstrID,4)';
   end if;        
   if v_columnName = 'ToName' then
 
      v_columnName :=  'dbo.GetEmaillName(emailHistory.numEmailHstrID,1)';
   end if;        
   if v_columnName = 'ToEmail' then
 
      v_columnName := 'dbo.GetEmaillAdd(emailHistory.numEmailHstrID,1)';
   end if; 
                  
   v_strSql := 'With tblSubscriber AS (SELECT  distinct(emailHistory.numEmailHstrID) ,            
ROW_NUMBER() OVER (ORDER BY ' || coalesce(v_columnName,'') || ' ' || coalesce(v_columnSortOrder,'') || ') AS  RowNumber             
from emailHistory                                
--left join EmailHstrAttchDtls on emailHistory.numEmailHstrID=EmailHstrAttchDtls.numEmailHstrID                     
--join EmailHStrToBCCAndCC on emailHistory.numEmailHstrID=EmailHStrToBCCAndCC.numEmailHstrID                     
--join EmailMaster on        EmailMaster.numEmailId=EmailHStrToBCCAndCC.numEmailId                     
where numDomainId=' || SUBSTR(CAST(v_numDomainId AS VARCHAR(15)),1,15) || ' and numUserCntId=' || SUBSTR(CAST(v_numUserCntId AS VARCHAR(15)),1,15) ||
   ' and numNodeId=' || SUBSTR(CAST(v_numNodeId AS VARCHAR(15)),1,15);  
    
   if v_chrSource <> '0' then   
      v_strSql := coalesce(v_strSql,'') || ' and (chrSource  = ''B'' or chrSource like ''%' || coalesce(v_chrSource,'') || '%'')';
   end if;   
           
   if v_srchSubjectBody <> '' then 
      v_strSql := coalesce(v_strSql,'') || ' and (vcSubject  like ''%' || coalesce(v_srchSubjectBody,'') || '%'' or  
  vcBodyText like ''%' || coalesce(v_srchSubjectBody,'') || '%''            
  or emailHistory.numEmailHstrID in (select numEmailHstrId from EmailHStrToBCCAndCC where             
 (numemailid in (select numEmailid from emailmaster where vcemailid like ''%' || coalesce(v_srchSubjectBody,'') || '%'')) or   
  vcName like ''%' || coalesce(v_srchSubjectBody,'') || '%'') )';
   end if;                
--if @ToEmail <> '' set @strSql=@strSql +'               
--      and (EmailMaster.vcEmailId like '''+@ToEmail+''' and EmailHStrToBCCAndCC.tintType<>4 )  '            
   if v_srchAttachmenttype <> '' then 
      v_strSql := coalesce(v_strSql,'') || '              
  and  emailHistory.numEmailHstrID in  (select numEmailHstrId from EmailHstrAttchDtls                
  where EmailHstrAttchDtls.vcAttachmentType like ''%' || coalesce(v_srchAttachmenttype,'') || '%'')';
   end if;            
            
            
                 
   v_strSql := coalesce(v_strSql,'') || ')                     
 select RowNumber,            
dbo.GetEmaillAdd(emailHistory.numEmailHstrID,4) as FromEmail,                  
 dbo.GetEmaillName(emailHistory.numEmailHstrID,4) as FromName,                             
--isnull(vcFromEmail,'''') as FromEmail,                                      
dbo.GetEmaillAdd(emailHistory.numEmailHstrID,1) as ToEmail,                                 
  
dbo.GetEmaillAdd(emailHistory.numEmailHstrID,2) as CCEmail,                                
dbo.GetEmaillAdd(emailHistory.numEmailHstrID,3) as BCCEmail,                                     
emailHistory.numEmailHstrID,                            
isnull(vcSubject,'''') as vcSubject,                                      
convert(varchar(max),vcBody) as vcBody,                              
dbo.FormatedDateFromDate( DATEADD(minute,' || CAST(-v_ClientTimeZoneOffset AS VARCHAR(15)) || ',bintCreatedOn),numDomainId) as bintCreatedOn,                              
isnull(vcItemId,'''') as ItemId,                              
isnull(vcChangeKey,'''') as ChangeKey,                              
isnull(bitIsRead,false) as IsRead,                  
isnull(vcSize,0) as vcSize,                              
isnull(bitHasAttachments,false) as HasAttachments,                             
  isnull(EmailHstrAttchDtls.vcFileName,'''') as AttachmentPath,                            
isnull(EmailHstrAttchDtls.vcAttachmentType,'''') as AttachmentType,                      
isnull(EmailHstrAttchDtls.vcAttachmentItemId,'''') as AttachmentItemId,                             
dbo.FormatedDateFromDate(dtReceivedOn,numDomainId) as dtReceivedOn,    
dbo.GetEmaillName(emailHistory.numEmailHstrID,1) as ToName,                          
isnull(emailHistory.tintType,1) as type,                              
isnull(chrSource,''B'') as chrSource,                          
isnull(vcCategory,''white'') as vcCategory                   
 from tblSubscriber T                  
 join emailHistory                   
 on emailHistory.numEmailHstrID=T.numEmailHstrID             
left join EmailHstrAttchDtls on emailHistory.numEmailHstrID=EmailHstrAttchDtls.numEmailHstrID             
where numDomainId=' || SUBSTR(CAST(v_numDomainId AS VARCHAR(15)),1,15) || ' and numUserCntId=' || SUBSTR(CAST(v_numUserCntId AS VARCHAR(15)),1,15) ||
   ' and numNodeId=' || SUBSTR(CAST(v_numNodeId AS VARCHAR(15)),1,15) || ' and                
  RowNumber > ' || SUBSTR(CAST(v_firstRec AS VARCHAR(10)),1,10) || ' and RowNumber < ' || SUBSTR(CAST(v_lastRec AS VARCHAR(10)),1,10) || '              
union                 
 select 0 as RowNumber,null,null,null,null,null,count(*),null,null,            
null,null,null,null,null,null,null,null,null, null,           
null,1,null,null from tblSubscriber  order by RowNumber';            
   RAISE NOTICE '%',v_strSql;             
   OPEN SWV_RefCur FOR EXECUTE v_strSql;
END; $$;


