-- Stored procedure definition script usp_GetBusinessProcessListConfiguration for PostgreSQL
CREATE OR REPLACE FUNCTION usp_GetBusinessProcessListConfiguration(v_numDomainId NUMERIC(9,0),
v_Mode SMALLINT,
v_numSlpId NUMERIC(9,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for select Slp_Id AS "Slp_Id"
,Slp_Name AS "Slp_Name"
,numdomainid AS "numdomainid"
,pro_type AS "pro_type"
,numCreatedby AS "numCreatedby"
,dtCreatedon AS "dtCreatedon"
,numModifedby AS "numModifedby"
,dtModifiedOn AS "dtModifiedOn"
,tintConfiguration AS "tintConfiguration"
,bitAssigntoTeams AS "bitAssigntoTeams"
,bitAutomaticStartTimer AS "bitAutomaticStartTimer"
,numOppId AS "numOppId"
,numTaskValidatorId AS "numTaskValidatorId"
,numProjectId AS "numProjectId"
,numWorkOrderId AS "numWorkOrderId"
,numBuildManager AS "numBuildManager" from Sales_process_List_Master
   where Pro_Type = v_Mode and numDomainID = v_numDomainId and Slp_Id = v_numSlpId;
END; $$;












