-- Stored procedure definition script USP_DeleteSiteMenu for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_DeleteSiteMenu(v_numMenuID NUMERIC(9,0),
	v_numSiteID NUMERIC(9,0))
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   DELETE FROM SiteMenu
   WHERE numMenuID = v_numMenuID AND numSiteID = v_numSiteID;
   RETURN;
END; $$;






