-- Stored procedure definition script USP_DELETE_SHIPPING_LABEL_RULEMASTER for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021

CREATE OR REPLACE FUNCTION USP_DELETE_SHIPPING_LABEL_RULEMASTER(v_numShippingRuleID NUMERIC(18,0),
	v_numDomainID		NUMERIC(18,0))
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   DELETE FROM PackagingRules where numDomainID = v_numDomainID and numShippingRuleID = v_numShippingRuleID;--added by  sachin(31stJuly2014) || To Delete Packaging rules on the Basis of Shipping Rule ID
   DELETE FROM ShippingLabelRuleChild WHERE numShippingRuleID = v_numShippingRuleID;
   DELETE FROM ShippingRuleStateList WHERE numRuleID = v_numShippingRuleID AND numDomainID = v_numDomainID;
   DELETE FROM ShippingLabelRuleMaster WHERE numShippingRuleID = v_numShippingRuleID AND numDomainID = v_numDomainID;
   RETURN;
END; $$;



