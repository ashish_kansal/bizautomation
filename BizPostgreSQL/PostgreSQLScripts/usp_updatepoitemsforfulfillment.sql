-- Stored procedure definition script USP_UpdatePOItemsForFulfillment for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_UpdatePOItemsForFulfillment(v_numQtyReceived DOUBLE PRECISION,
    v_numOppItemID NUMERIC(9,0),
    INOUT v_vcError VARCHAR(200) DEFAULT '' ,
    v_numUserCntID NUMERIC(9,0) DEFAULT NULL,
    v_dtItemReceivedDate TIMESTAMP DEFAULT NULL,
	v_numSelectedWarehouseItemID NUMERIC(18,0) DEFAULT 0,
	v_numVendorInvoiceBizDocID NUMERIC(18,0) DEFAULT 0,
	v_tintMode SMALLINT DEFAULT 0, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
--Transaction is invoked from ADO.net (Purchase Fulfillment) Do not add transaction code here.
-- BEGIN TRY
-- BEGIN TRANSACTION
   AS $$
   DECLARE
   v_numDomain  NUMERIC(18,0);
	          
   v_numWarehouseItemID  NUMERIC;
   v_numWLocationID  NUMERIC(18,0);     
   v_numOldQtyReceived  DOUBLE PRECISION;       
   v_numNewQtyReceived  DOUBLE PRECISION;
	              
   v_bitStockTransfer  BOOLEAN;
   v_numToWarehouseItemID  NUMERIC;
   v_numOppId  NUMERIC;
   v_numUnits  DOUBLE PRECISION;
   v_monPrice  DECIMAL(20,5); 
   v_numItemCode  NUMERIC; 
		
   v_numTotalQuantityReceived  DOUBLE PRECISION;
   v_description  VARCHAR(100);
   v_p3  TEXT DEFAULT '';  
    

   v_TotalOnHand  DOUBLE PRECISION;
   v_monAvgCost  DECIMAL(20,5);	

   v_vcFromLocation  VARCHAR(300);
   v_vcToLocation  VARCHAR(300);
 
   SWV_RCur REFCURSOR;
   v_onHand  DOUBLE PRECISION;
   v_onAllocation  DOUBLE PRECISION;    
   v_onOrder  DOUBLE PRECISION;            
   v_onBackOrder  DOUBLE PRECISION;
BEGIN
   IF coalesce(v_numVendorInvoiceBizDocID,0) > 0 then
	
		-- IF RECEIVED AGAINST VENDOR INVOICE MAKE SURE ENOUGH QTY IS PENDING TO RECIEVED AGAINST VEDOR INVOICE BECAUSE USER CAN RECEIVE QTY WITHOUT SELECTING VENDOR INVOCIE ALSO
      IF(SELECT
      COUNT(*)
      FROM
      OpportunityBizDocItems OBDI
      INNER JOIN
      OpportunityItems OI
      ON
      OBDI.numOppItemID = OI.numoppitemtCode
      WHERE
      numOppBizDocID = v_numVendorInvoiceBizDocID
      AND OBDI.numOppItemID = v_numOppItemID
      AND v_numQtyReceived >(coalesce(OI.numUnitHour,0) -coalesce(OI.numUnitHourReceived,0))) > 0 then
		
         RAISE EXCEPTION 'INVALID_VENDOR_INVOICE_RECEIVE_QTY';
         RETURN;
      end if;
   end if;


   select   coalesce(numWarehouseItmsID,0), coalesce(WI.numWLocationID,0), coalesce(numUnitHourReceived,0), coalesce(OM.bitStockTransfer,false), coalesce(numToWarehouseItemID,0), OM.numOppId, OI.numUnitHour, coalesce(monPrice,0)*(CASE WHEN coalesce(OM.fltExchangeRate,0) = 0 THEN 1 ELSE OM.fltExchangeRate END), OI.numItemCode, OM.numDomainId INTO v_numWarehouseItemID,v_numWLocationID,v_numOldQtyReceived,v_bitStockTransfer,
   v_numToWarehouseItemID,v_numOppId,v_numUnits,v_monPrice,v_numItemCode,
   v_numDomain FROM
   OpportunityItems OI
   INNER JOIN
   OpportunityMaster OM
   ON
   OI.numOppId = OM.numOppId
   LEFT JOIN
   WareHouseItems WI
   ON
   OI.numWarehouseItmsID = WI.numWareHouseItemID WHERE
   numoppitemtCode = v_numOppItemID;


   IF v_bitStockTransfer = true then --added by chintan
	
	--ship item from FROM warehouse
      SELECT * INTO v_p3 FROM USP_UpdateQtyShipped(v_numQtyReceived,v_numOppItemID,v_numUserCntID,v_vcError := v_p3);
      IF LENGTH(v_p3) > 0 then
		
         RAISE EXCEPTION '%',v_p3;
         RETURN;
      end if;
		
		-- Receive item from To Warehouse
      v_numWarehouseItemID := v_numToWarehouseItemID;
   end if;  
    

   v_numTotalQuantityReceived := v_numQtyReceived+v_numOldQtyReceived;
   v_numNewQtyReceived := v_numQtyReceived;
   v_description := CONCAT('PO Qty Received (Qty:',v_numTotalQuantityReceived,')');
	
   IF v_numTotalQuantityReceived > v_numUnits then
	
      RAISE EXCEPTION 'RECEIVED_QTY_MUST_BE_LESS_THEN_OR_EQUAL_TO_ORDERED_QTY';
   end if;

   IF v_numNewQtyReceived <= 0 then
      RETURN;
   end if; 
  
	
   IF coalesce(v_bitStockTransfer,false) = false then
      v_TotalOnHand := 0;
      select(SUM(coalesce(numOnHand,0))+SUM(coalesce(numAllocation,0))) INTO v_TotalOnHand FROM WareHouseItems WHERE numItemID = v_numItemCode; 
						
		--Updating the Average Cost
      select(CASE WHEN coalesce(bitVirtualInventory,false) = true THEN 0 ELSE coalesce(monAverageCost,0) END) INTO v_monAvgCost FROM Item WHERE   numItemCode = v_numItemCode;
      v_monAvgCost :=((v_TotalOnHand*v_monAvgCost)+(v_numNewQtyReceived*v_monPrice))/(v_TotalOnHand+v_numNewQtyReceived);
      UPDATE
      Item
      SET
      monAverageCost =(CASE WHEN coalesce(bitVirtualInventory,false) = true THEN 0 ELSE v_monAvgCost END)
      WHERE
      numItemCode = v_numItemCode;
   end if;	

   IF coalesce(v_numSelectedWarehouseItemID,0) > 0 AND coalesce(v_numSelectedWarehouseItemID,0) <> v_numWarehouseItemID then
      select   coalesce(WL.vcLocation,'') INTO v_vcFromLocation FROM WareHouseItems WI LEFT JOIN WarehouseLocation WL ON WI.numWLocationID = WL.numWLocationID WHERE WI.numWareHouseItemID = v_numWarehouseItemID;
      select   coalesce(WL.vcLocation,'') INTO v_vcToLocation FROM WareHouseItems WI LEFT JOIN WarehouseLocation WL ON WI.numWLocationID = WL.numWLocationID WHERE WI.numWareHouseItemID = v_numSelectedWarehouseItemID;
      UPDATE
      WareHouseItems
      SET
      numonOrder = coalesce(numonOrder,0) -v_numNewQtyReceived,dtModified = LOCALTIMESTAMP
      WHERE
      numWareHouseItemID = v_numWarehouseItemID;
		 --  numeric(9, 0)
			 --  numeric(9, 0)
			 --  tinyint
      v_description := CONCAT('PO Qty Received To Internal Location ',v_vcToLocation,' (Qty:',v_numNewQtyReceived,
      ')');
      PERFORM USP_ManageWareHouseItems_Tracking(v_numWarehouseItemID := v_numWarehouseItemID,v_numReferenceID := v_numOppId,
      v_tintRefType := 4::SMALLINT,v_vcDescription := v_description,v_numModifiedBy := v_numUserCntID,
      v_dtRecordDate := v_dtItemReceivedDate,
      v_numDomainID := v_numDomain,SWV_RefCur := null);

		-- INCREASE THE OnHand Of Destination Location
      UPDATE
      WareHouseItems
      SET
      numBackOrder =(CASE WHEN numBackOrder >= v_numNewQtyReceived THEN coalesce(numBackOrder,0) -v_numNewQtyReceived ELSE 0 END),numAllocation =(CASE WHEN numBackOrder >= v_numNewQtyReceived THEN coalesce(numAllocation,0)+v_numNewQtyReceived ELSE coalesce(numAllocation,0)+coalesce(numBackOrder,0) END),
      numOnHand =(CASE WHEN numBackOrder >= v_numNewQtyReceived THEN numOnHand ELSE coalesce(numOnHand,0)+v_numNewQtyReceived -coalesce(numBackOrder,0) END),dtModified = LOCALTIMESTAMP
      WHERE
      numWareHouseItemID = v_numSelectedWarehouseItemID;
		 --  numeric(9, 0)
			 --  numeric(9, 0)
			 --  tinyint
      v_description := CONCAT('PO Qty Received From Internal Location ',v_vcFromLocation,' (Qty:',v_numNewQtyReceived,
      ')');
      PERFORM USP_ManageWareHouseItems_Tracking(v_numWarehouseItemID := v_numSelectedWarehouseItemID,v_numReferenceID := v_numOppId,
      v_tintRefType := 4::SMALLINT,v_vcDescription := v_description,
      v_numModifiedBy := v_numUserCntID,v_dtRecordDate := v_dtItemReceivedDate,
      v_numDomainID := v_numDomain,SWV_RefCur := null);
      INSERT INTO OpportunityItemsReceievedLocation(numDomainId,
			numOppID,
			numOppItemID,
			numWarehouseItemID,
			numUnitReceieved)
		VALUES(v_numDomain,
			v_numOppId,
			v_numOppItemID,
			v_numSelectedWarehouseItemID,
			v_numNewQtyReceived);
   ELSEIF coalesce(v_numWarehouseItemID,0) > 0
   then
      select   coalesce(numOnHand,0), coalesce(numAllocation,0), coalesce(numonOrder,0), coalesce(numBackOrder,0) INTO v_onHand,v_onAllocation,v_onOrder,v_onBackOrder FROM
      WareHouseItems WHERE
      numWareHouseItemID = v_numWarehouseItemID;
      IF v_onOrder >= v_numNewQtyReceived then
		
         RAISE NOTICE '1 case';
         v_onOrder := v_onOrder -v_numNewQtyReceived;
         IF v_onBackOrder >= v_numNewQtyReceived then
			
            v_onBackOrder := v_onBackOrder -v_numNewQtyReceived;
            v_onAllocation := v_onAllocation+v_numNewQtyReceived;
         ELSE
            v_onAllocation := v_onAllocation+v_onBackOrder;
            v_numNewQtyReceived := v_numNewQtyReceived -v_onBackOrder;
            v_onBackOrder := 0;
            v_onHand := v_onHand+v_numNewQtyReceived;
         end if;
      ELSEIF v_onOrder < v_numNewQtyReceived
      then
		
         RAISE NOTICE '2 case';
         v_onHand := v_onHand+v_onOrder;
         v_onOrder := v_numNewQtyReceived -v_onOrder;
      end if;
      open SWV_RefCur for
      SELECT
      v_onHand AS onHand,
			v_onAllocation AS onAllocation,
			v_onBackOrder AS onBackOrder,
			v_onOrder AS onOrder;
		 --  numeric(9, 0)
			 --  numeric(9, 0)
			 --  tinyint
			 --  varchar(100)
      UPDATE
      WareHouseItems
      SET
      numOnHand = v_onHand,numAllocation = v_onAllocation,numBackOrder = v_onBackOrder,
      numonOrder = v_onOrder,dtModified = LOCALTIMESTAMP
      WHERE
      numWareHouseItemID = v_numWarehouseItemID;
      PERFORM USP_ManageWareHouseItems_Tracking(v_numWarehouseItemID := v_numWarehouseItemID,v_numReferenceID := v_numOppId,
      v_tintRefType := 4::SMALLINT,v_vcDescription := v_description,v_numModifiedBy := v_numUserCntID,
      v_dtRecordDate := v_dtItemReceivedDate,
      v_numDomainID := v_numDomain,SWV_RefCur := null);
   end if;
 
   UPDATE
   OpportunityItems
   SET
   numUnitHourReceived = v_numTotalQuantityReceived,numQtyReceived =(CASE WHEN coalesce(v_tintMode,0) = 1 THEN coalesce(numQtyReceived,0)+coalesce(v_numQtyReceived,0) ELSE coalesce(numQtyReceived,0) END)
   WHERE
   numoppitemtCode = v_numOppItemID;

   UPDATE Item I SET dtItemReceivedDate = v_dtItemReceivedDate FROM OpportunityItems OI WHERE OI.numItemCode = I.numItemCode AND OI.numoppitemtCode = v_numOppItemID;

   IF coalesce(v_numVendorInvoiceBizDocID,0) > 0 then
	
      UPDATE
      OpportunityBizDocItems
      SET
      numVendorInvoiceUnitReceived = coalesce(numVendorInvoiceUnitReceived,0)+coalesce(v_numQtyReceived,0)
      WHERE
      numOppBizDocID = v_numVendorInvoiceBizDocID
      AND numOppItemID = v_numOppItemID;
   end if;

   UPDATE OpportunityMaster SET dtItemReceivedDate = v_dtItemReceivedDate WHERE numOppId = v_numOppId;

   IF EXISTS(SELECT
   OI.numoppitemtCode
   FROM
   OpportunityMaster OM
   INNER JOIN
   OpportunityItems OI
   ON
   OM.numOppId = OI.numOppId
   INNER JOIN
   Item I
   ON
   OI.numItemCode = I.numItemCode
   WHERE
   OM.numOppId = v_numOppId
   AND OM.tintopptype = 2
   AND OM.tintoppstatus = 1
   AND OI.numoppitemtCode = v_numOppItemID
   AND coalesce(I.bitAsset,false) = true) then
	
      PERFORM USP_ItemDepreciation_Save(v_numDomain,v_numOppId,v_numOppItemID);
   end if;
END; $$;	

