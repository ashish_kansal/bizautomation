DROP FUNCTION IF EXISTS USP_ItemsForECommerce;

CREATE OR REPLACE FUNCTION USP_ItemsForECommerce(v_numSiteID NUMERIC(9,0) DEFAULT 0,
    v_numCategoryID NUMERIC(18,0) DEFAULT 0,
    v_numWareHouseID NUMERIC(9,0) DEFAULT 0,
    v_SortBy TEXT DEFAULT 0,
    v_CurrentPage INTEGER DEFAULT NULL,
    v_PageSize INTEGER DEFAULT NULL,
	INOUT v_TotRecs INTEGER DEFAULT 0,
    v_SearchText TEXT DEFAULT '',--Comma seperated item ids
    v_FilterQuery TEXT DEFAULT '',----Comma seperated Attributes Ids
    v_FilterRegularCondition TEXT DEFAULT NULL,
    v_FilterCustomFields TEXT DEFAULT NULL,
    v_FilterCustomWhere TEXT DEFAULT NULL,
	v_numDivisionID NUMERIC(18,0) DEFAULT 0,
	v_numManufacturerID NUMERIC(18,0) DEFAULT 0,
	v_FilterItemAttributes TEXT DEFAULT NULL,
	v_numESItemCodes TEXT DEFAULT '', 
    INOUT SWV_RefCur refcursor DEFAULT NULL,
	INOUT SWV_RefCur2 refcursor DEFAULT NULL
)
LANGUAGE plpgsql
AS $$

DECLARE
   v_numDomainID  NUMERIC(9,0);
   v_numDefaultRelationship  NUMERIC(18,0);
   v_numDefaultProfile  NUMERIC(18,0);
   v_tintPreLoginPriceLevel  SMALLINT;
   v_tintPriceLevel  SMALLINT;
   v_bitDisplayCategory  BOOLEAN;
   v_bitAutoSelectWarehouse  BOOLEAN;

   v_strSQL  TEXT;
   v_firstRec  INTEGER;
   v_lastRec  INTEGER;
   v_Where  TEXT;
   v_SortString  TEXT;
   v_searchPositionColumn  TEXT;
   v_searchPositionColumnGroupBy  TEXT;
   v_row  INTEGER;
   v_data  VARCHAR(100);
   v_dynamicFilterQuery  TEXT;
   v_checkFldType  VARCHAR(100);
   v_count  NUMERIC(9,0);
   v_whereNumItemCode  TEXT;
   v_filterByNumItemCode  VARCHAR(100);
        
   v_vcWarehouseIDs  VARCHAR(1000);
   v_Join  TEXT;
   v_SortFields  TEXT;
   v_SortFields_GroupBy  TEXT;
   v_vcSort  TEXT;

   v_DefaultSort  TEXT;
   v_DefaultSortFields  TEXT;
   v_DefaultSortJoin  TEXT;
   v_intCnt  INTEGER;
   v_intTotalCnt  INTEGER;	
   v_strColumnName  VARCHAR(100);
   v_bitCustom  BOOLEAN;
   v_FieldID  VARCHAR(100);				

   v_fldList  TEXT;
   v_fldList1  TEXT;
   v_bitGetItemForElasticSearch  BOOLEAN;
   v_tmpSQL  TEXT;
   v_tintOrder  SMALLINT;                                                  
   v_vcFormFieldName  VARCHAR(50);                                                  
   v_vcListItemType  VARCHAR(1);                                             
   v_vcAssociatedControlType  VARCHAR(10);                                                  
   v_numListID  NUMERIC(9,0);                                                  
   v_WhereCondition  TEXT;                       
   v_numFormFieldId  NUMERIC;  
	--DECLARE @vcFieldType CHAR(1)
   v_vcFieldType  VARCHAR(15);
   v_FilterCustomFieldIds  TEXT;
   v_strCustomSql  TEXT;
		
   v_fldID  NUMERIC(18,0);
   v_RowNumber  TEXT;
   v_bitSortPriceMode  BOOLEAN;
   v_fldID1  NUMERIC(18,0);
   v_RowNumber1  TEXT;
   v_vcSort1  VARCHAR(10);
   v_vcSortBy  VARCHAR(1000);
BEGIN
   /* SQLWays Notice: SET TRANSACTION ISOLATION LEVEL READ COMMITTED must be called before procedure call */
-- SET TRANSACTION ISOLATION LEVEL READ COMMITTED

v_FilterRegularCondition := regexp_replace(v_FilterRegularCondition,'\[','','gi');
v_FilterRegularCondition := regexp_replace(v_FilterRegularCondition,'\]','','gi');
v_FilterCustomFields := regexp_replace(v_FilterCustomFields,'\[','','gi');
v_FilterCustomFields := regexp_replace(v_FilterCustomFields,'\]','','gi');
v_FilterCustomWhere := regexp_replace(v_FilterCustomWhere,'\[','','gi');
v_FilterCustomWhere := regexp_replace(v_FilterCustomWhere,'\]','','gi');

select   numDomainID INTO v_numDomainID FROM
   Sites WHERE
   numSiteID = v_numSiteID;
        
   IF coalesce(v_numDivisionID,0) > 0 then
	
      SELECT coalesce(numCompanyType,0), coalesce(vcProfile,0), coalesce(tintPriceLevel,0) 
	  INTO v_numDefaultRelationship,v_numDefaultProfile,v_tintPriceLevel 
	  FROM DivisionMaster 
	  INNER JOIN CompanyInfo ON DivisionMaster.numCompanyID = CompanyInfo.numCompanyId 
	  WHERE numDivisionID = v_numDivisionID
      AND DivisionMaster.numDomainID = v_numDomainID;
   end if;

	--KEEP THIS BELOW ABOVE IF CONDITION
   select   coalesce(bitDisplayCategory,false), (CASE WHEN coalesce(v_numDivisionID,0) > 0 THEN v_numDefaultRelationship ELSE numRelationshipId END), (CASE WHEN coalesce(v_numDivisionID,0) > 0 THEN v_numDefaultProfile ELSE numProfileId END), 
   (CASE WHEN coalesce(bitShowPriceUsingPriceLevel,false) = true THEN(CASE WHEN v_numDivisionID > 0 THEN(CASE WHEN coalesce(bitShowPriceUsingPriceLevel,false) = true THEN coalesce(v_tintPriceLevel,0) ELSE 0 END) ELSE coalesce(tintPreLoginProceLevel,0) END) ELSE 0 END), coalesce(bitAutoSelectWarehouse,false) 
   INTO v_bitDisplayCategory,v_numDefaultRelationship,v_numDefaultProfile,v_tintPreLoginPriceLevel,v_bitAutoSelectWarehouse 
   FROM eCommerceDTL 
   WHERE numSiteId = v_numSiteID;
   	
   DROP TABLE IF EXISTS tt_TEMPITEMCAT CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPITEMCAT
   (
      numCategoryID NUMERIC(18,0)
   );
   
   DROP TABLE IF EXISTS tt_TEMPItemCode CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPItemCode
   (
      numItemCode NUMERIC(18,0)
   );

   IF LENGTH(v_FilterCustomFields) > 0 AND LENGTH(v_FilterCustomWhere) > 0 then
      v_FilterCustomFieldIds := Replace(v_FilterCustomFields,'"','');
      v_FilterCustomFieldIds := Replace(v_FilterCustomFieldIds,'"','');
      v_strCustomSql := 'INSERT INTO tt_TEMPItemCode(numItemCode)
								SELECT DISTINCT T.RecId FROM 
														(
															SELECT *   
															FROM  crosstab(
																  ''SELECT RecId, fld_id, MAX(Fld_Value)
																	FROM (
																	SELECT t2.RecId, t1.fld_id, t2.Fld_Value 
																	FROM CFW_Fld_Master t1 
																	JOIN CFW_FLD_Values_Item AS t2 ON t1.Fld_ID = t2.Fld_ID 
																	WHERE t1.Grp_id  In (5,9)
																	AND t1.numDomainID = ' || SUBSTR(CAST(v_numDomainID AS VARCHAR(10)),1,10) || '
																	AND t1.fld_type <> ''''Link'''' 
																	AND t1.fld_id IN (' || COALESCE(v_FilterCustomFieldIds,'') || ')
																	) tmp
																	GROUP BY RecId, fld_id 
																	'')
																AS final_result_pivot
																( RecId NUMERIC(18), "' || REPLACE(COALESCE(v_FilterCustomFields,''),',','" TEXT,"') || '" TEXT) 
														) T  WHERE 1=1 AND ' || coalesce(v_FilterCustomWhere,'');
      RAISE NOTICE '%', v_strCustomSql;
	  EXECUTE v_strCustomSql;
   end if;
		
   v_SortString := '';
   v_searchPositionColumn := '';
   v_searchPositionColumnGroupBy := '';
   v_strSQL := '';
   v_whereNumItemCode := '';
   v_dynamicFilterQuery := '';
		 
   v_firstRec :=(v_CurrentPage::bigint -1)*v_PageSize::bigint;
   v_lastRec :=(v_CurrentPage::bigint*v_PageSize::bigint+1);
                
   IF (v_numWareHouseID = 0 OR v_bitAutoSelectWarehouse = true) then
    
      IF v_bitAutoSelectWarehouse = true then
		
         v_vcWarehouseIDs := COALESCE((SELECT DISTINCT string_agg(CAST(numWareHouseID AS VARCHAR(10)), ',') FROM Warehouses WHERE Warehouses.numDomainID = v_numDomainID), '');
      ELSE
         select   coalesce(numDefaultWareHouseID,0) INTO v_numWareHouseID FROM
         eCommerceDTL WHERE
         numDomainID = v_numDomainID;
         v_vcWarehouseIDs := SUBSTR(CAST(v_numWareHouseID AS VARCHAR(1000)),1,1000);
      end if;
   ELSE
      v_vcWarehouseIDs := SUBSTR(CAST(v_numWareHouseID AS VARCHAR(1000)),1,1000);
   end if;

	--PRIAMRY SORTING USING CART DROPDOWN
   v_Join := '';
   v_SortFields := '';
   v_SortFields_GroupBy := '';
   v_vcSort := '';

   IF v_SortBy = '' then
	
      v_SortString := ' vcItemName Asc ';
   ELSEIF POSITION('~0' IN v_SortBy) > 0 OR POSITION('~1' IN v_SortBy) > 0
   then
      DROP TABLE IF EXISTS tt_FLDVALUES CASCADE;
      CREATE TEMPORARY TABLE tt_FLDVALUES AS
         SELECT * FROM SplitIDsWithPosition(v_SortBy,'~');
      select   RowNumber, id INTO v_RowNumber,v_fldID FROM tt_FLDVALUES WHERE RowNumber = 1;
      select   CASE WHEN id = 0 THEN 'ASC' ELSE 'DESC' END INTO v_vcSort FROM tt_FLDVALUES WHERE RowNumber = 2;
      v_Join := coalesce(v_Join,'') || ' LEFT JOIN CFW_FLD_Values_Item CFVI' || coalesce(v_RowNumber,'') || ' ON I.numItemCode = CFVI' || coalesce(v_RowNumber,'') || '.RecID AND CFVI' || coalesce(v_RowNumber,'') || '.fld_Id = ' || SUBSTR(CAST(v_fldID AS VARCHAR(10)),1,10);
	
      v_SortString := '"fld_Value' || coalesce(v_RowNumber,'') || '" '; 
      v_SortFields := ', CFVI' || coalesce(v_RowNumber,'') || '.fld_Value AS "fld_Value' || coalesce(v_RowNumber,'') || '"';
	  v_SortFields_GroupBy := ', CFVI' || coalesce(v_RowNumber,'') || '.fld_Value ';
   ELSE
      IF POSITION('monListPrice' IN v_SortBy) > 0 then
         select   coalesce(bitSortPriceMode,false) INTO v_bitSortPriceMode FROM eCommerceDTL WHERE numSiteId = v_numSiteID;
         IF v_bitSortPriceMode = true then
			
            v_SortString := ' ';
         ELSE
            v_SortString := v_SortBy;
         end if;
      ELSE
         v_SortString := v_SortBy;
      end if;
   end if;	

	--SECONDARY SORTING USING DEFAULT SETTINGS FROM ECOMMERCE
   DROP TABLE IF EXISTS tt_TEMPSORT CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPSORT AS
      SELECT
      ROW_NUMBER() OVER(ORDER BY Custom,vcFieldName) AS RowID
		,* FROM(SELECT
      CAST(numFieldID AS VARCHAR(9)) || '~0' AS numFieldID,
			coalesce(vcCultureFieldName,vcFieldName) AS vcFieldName,
			0 AS Custom,
			vcDbColumnName
      FROM
      View_DynamicColumns
      WHERE
      numFormId = 84
      AND coalesce(bitSettingField,false) = true
      AND coalesce(bitDeleted,false) = false
      AND coalesce(bitCustom,false) = false
      AND numDomainID = v_numDomainID
      UNION
      SELECT
      CAST(numFieldId AS VARCHAR(9)) || '~1' AS numFieldID,
			vcFieldName ,
			1 AS Custom,
			'' AS vcDbColumnName
      FROM
      View_DynamicCustomColumns
      WHERE
      numFormId = 84
      AND numDomainID = v_numDomainID
      AND Grp_id IN(5,9)
      AND vcAssociatedControlType = 'TextBox'
      AND coalesce(bitCustom,false) = true
      AND tintPageType = 1) TABLE1
      ORDER BY
      Custom,vcFieldName;
		
	--SELECT * FROM tt_TEMPSort
   v_DefaultSort := '';
   v_DefaultSortFields := '';
   v_DefaultSortJoin := '';		
   v_intCnt := 0;
   SELECT COUNT(*) INTO v_intTotalCnt FROM tt_TEMPSORT;
   v_strColumnName := '';
		
   IF(SELECT coalesce(bitEnableSecSorting,false) FROM eCommerceDTL WHERE numSiteId = v_numSiteID) = true then
	
      DROP TABLE IF EXISTS tt_FLDDEFAULTVALUES CASCADE;
      CREATE TEMPORARY TABLE tt_FLDDEFAULTVALUES
      (
         RowNumber INTEGER,
         Id VARCHAR(1000)
      );
      WHILE(v_intCnt < v_intTotalCnt) LOOP
         v_intCnt := v_intCnt::bigint+1;
         select   numFieldID, vcDbColumnName, Custom INTO v_FieldID,v_strColumnName,v_bitCustom FROM tt_TEMPSORT WHERE RowID = v_intCnt;
         IF v_bitCustom = false then
			
            v_DefaultSort :=  coalesce(v_strColumnName,'') || ',' || coalesce(v_DefaultSort,'');
         ELSE
            v_vcSortBy := SUBSTR(CAST(v_FieldID AS VARCHAR(100)),1,100) || '~' || '0';
            INSERT INTO tt_FLDDEFAULTVALUES(RowNumber,Id) SELECT RowNumber, id FROM SplitIDsWithPosition(v_vcSortBy,'~');
            select   RowNumber::bigint+100+v_intCnt::bigint, Id INTO v_RowNumber1,v_fldID1 FROM tt_FLDDEFAULTVALUES WHERE RowNumber = 1;
            v_vcSort1 := 'ASC';
            v_DefaultSortJoin := coalesce(v_DefaultSortJoin,'') || ' LEFT JOIN CFW_FLD_Values_Item CFVI' || coalesce(v_RowNumber1,'') || ' ON I.numItemCode = CFVI' || coalesce(v_RowNumber1,'') || '.RecID AND CFVI' || coalesce(v_RowNumber1,'') || '.fld_Id = ' || SUBSTR(CAST(v_fldID1 AS VARCHAR(10)),1,10);
            v_DefaultSort := '"fld_Value' || coalesce(v_RowNumber1,'') || '" ' || ',' || coalesce(v_DefaultSort,'');
            v_DefaultSortFields := coalesce(v_DefaultSortFields,'') || ', CFVI' || coalesce(v_RowNumber1,'') || '.fld_Value "fld_Value' || coalesce(v_RowNumber1,'') || '"';
         end if;
      END LOOP;
      IF LENGTH(v_DefaultSort) > 0 then
		
         v_DefaultSort := SUBSTR(v_DefaultSort,1,LENGTH(v_DefaultSort) -1);
         v_SortString := coalesce(v_SortString,'') || ',';
      end if;
   end if;
                                		
   IF LENGTH(v_SearchText) > 0 then
	
      IF POSITION('SearchOrder' IN v_SortString) > 0 then
		
         v_searchPositionColumn := ' ,SearchItems.RowNumber AS SearchOrder ';
         v_searchPositionColumnGroupBy := ' ,SearchItems.RowNumber ';
      end if;
      v_Where := ' INNER JOIN SplitIDsWithPosition(''' || coalesce(v_SearchText,'') || ''','','') AS SearchItems 
                        ON SearchItems.Id = I.numItemCode  and SC.numSiteID = ' || SUBSTR(CAST(v_numSiteID AS VARCHAR(15)),1,15) || ' WHERE COALESCE(IsArchieve,false) = false';
   ELSEIF v_SearchText = ''
   then
	
      v_Where := ' WHERE I.numDomainID=' || SUBSTR(CAST(v_numDomainID AS VARCHAR(15)),1,15)
      || ' and SC.numSiteID = ' || SUBSTR(CAST(v_numSiteID AS VARCHAR(15)),1,15)
      || ' AND COALESCE(IsArchieve,false) = false';
   ELSE
      v_Where := CONCAT(' WHERE I.numDomainID=',v_numDomainID,' and SC.numSiteID = ',v_numSiteID,
      ' AND COALESCE(IsArchieve,false) = false');
      IF v_bitDisplayCategory = true then
		
         INSERT INTO tt_TEMPITEMCAT(numCategoryID) 
		 with recursive CTE AS
		 (
			SELECT C.numCategoryID,C.tintLevel,C.numDepCategory,CASE WHEN tintLevel = 1 THEN true ELSE false END AS bitChild
         	FROM SiteCategories SC INNER JOIN Category C ON SC.numCategoryID = C.numCategoryID 
			WHERE SC.numSiteID = v_numSiteID 
			AND (C.numCategoryID = v_numCategoryID OR coalesce(v_numCategoryID,0) = 0) 
			AND numDomainID = v_numDomainID
         	UNION ALL
         	SELECT  C.numCategoryID,C.tintLevel,C.numDepCategory,CTE.bitChild 
			FROM Category C JOIN CTE ON C.numDepCategory = CTE.numCategoryID 
			AND CTE.bitChild = true 
			AND numDomainID = v_numDomainID
		 ) 
		 SELECT DISTINCT C.numCategoryID FROM CTE C JOIN ItemCategory IC ON C.numCategoryID = IC.numCategoryID
         JOIN View_Item_Warehouse ON View_Item_Warehouse.numDOmainID = v_numDomainID AND  View_Item_Warehouse.numItemID = IC.numItemID
         WHERE coalesce(View_Item_Warehouse.numAvailable,0) > 0;
      end if;
   end if;

   v_strSQL := coalesce(v_strSQL,'') || CONCAT('
											DROP TABLE IF EXISTS tt_TEMPItems;
											CREATE TEMPORARY TABLE tt_TEMPItems 
											AS 
											SELECT
											I.numDomainID
											,I.numItemCode
											,ROW_NUMBER() OVER ( Partition by I.numItemCode Order by I.numItemCode) RowID
											,I.bitMatrix
											,I.bitKitParent
											,I.numItemGroup
											,vcItemName
											,I.bintCreatedDate
											,vcManufacturer
											,COALESCE(txtItemDesc,'''') txtItemDesc
											,vcSKU
											,fltHeight
											,fltWidth
											,fltLength
											,vcModelID
											,fltWeight
											,numBaseUnit
											,numSaleUnit
											,numPurchaseUnit
											,bitFreeShipping
											,C.vcCategoryName
											,C.vcDescription as CategoryDesc
											,charItemType
											,monListPrice
											,bitAllowBackOrder
											,bitShowInStock
											,numVendorID
											,numItemClassification',v_searchPositionColumn,v_SortFields,v_DefaultSortFields,
   '
											,SUM(numOnHand) numOnHand
											,SUM(numAllocation) numAllocation
											' ||(CASE WHEN LENGTH(coalesce(v_numESItemCodes,'')) > 0 THEN ',COALESCE(C.numCategoryID,0) numCategoryID' ELSE '' END)  || '
											,COALESCE(I.numManufacturer,0) numManufacturer
											FROM      
											Item AS I
											INNER JOIN eCommerceDTL E ON E.numDomainID = I.numDomainID AND E.numSiteId=',v_numSiteID,
   '
											LEFT JOIN WareHouseItems WI ON WI.numDomainID=' || SUBSTR(CAST(v_numDomainID AS VARCHAR(10)),1,10) || ' AND WI.numItemID=I.numItemCode AND numWareHouseID IN (SELECT CAST(OutParam AS NUMERIC(18)) FROM SplitString(''' || coalesce(v_vcWarehouseIDs,'') || ''','',''))
											INNER JOIN ItemCategory IC ON I.numItemCode = IC.numItemID
											INNER JOIN Category C ON IC.numCategoryID = C.numCategoryID
											INNER JOIN SiteCategories SC ON IC.numCategoryID = SC.numCategoryID 
											',v_Join,' ',v_DefaultSortJoin);

   IF LENGTH(coalesce(v_FilterItemAttributes,'')) > 0 then
	
      v_strSQL := coalesce(v_strSQL,'') || coalesce(v_FilterItemAttributes,'');
   end if;

   IF LENGTH(v_FilterCustomFields) > 0 AND LENGTH(v_FilterCustomWhere) > 0 then
	
      v_strSQL := coalesce(v_strSQL,'') || ' INNER JOIN tt_TEMPItemCode TP ON TP.numItemCode = I.numItemCode ';
   end if;
			
   IF(SELECT COUNT(*) FROM tt_TEMPITEMCAT) > 0 then
	
      v_strSQL := coalesce(v_strSQL,'') || ' INNER JOIN tt_TEMPItemCat TC ON TC.numCategoryID = IC.numCategoryID ';
   ELSEIF v_numManufacturerID > 0
   then
	
      v_Where := REPLACE(v_Where,'WHERE',' WHERE I.numManufacturer = ' || SUBSTR(CAST(COALESCE(v_numManufacturerID, 0) AS VARCHAR(10)),1,10) || ' AND ');
   ELSEIF v_numCategoryID > 0
   then
	
      v_Where := REPLACE(v_Where,'WHERE',' WHERE IC.numCategoryID = ' || SUBSTR(CAST(v_numCategoryID AS VARCHAR(10)),1,10) || ' AND ');
   end if; 
			
   IF(SELECT COUNT(*) FROM EcommerceRelationshipProfile WHERE numSiteID = v_numSiteID AND numRelationship = v_numDefaultRelationship AND numProfile = v_numDefaultProfile) > 0 then
	
      v_Where := coalesce(v_Where,'') || CONCAT(' AND I.numItemClassification NOT IN (SELECT numItemClassification FROM EcommerceRelationshipProfile ERP INNER JOIN ECommerceItemClassification EIC ON EIC.numECommRelatiionshipProfileID=ERP.ID WHERE ERP.numSiteID=',v_numSiteID,' AND ERP.numRelationship=',v_numDefaultRelationship,
      ' AND ERP.numProfile=',v_numDefaultProfile,')');
   end if;

   v_strSQL := coalesce(v_strSQL,'') || coalesce(v_Where,'');
	
   IF LENGTH(v_FilterRegularCondition) > 0 then
	
      v_strSQL := coalesce(v_strSQL,'') || coalesce(v_FilterRegularCondition,'');
   end if;                                         

   IF LENGTH(v_whereNumItemCode) > 0 then
    
      v_strSQL := coalesce(v_strSQL,'') || coalesce(v_filterByNumItemCode,'');
   end if;

   v_strSQL := CONCAT(v_strSQL,' GROUP BY
										I.numDomainID
										,I.numItemCode
										,I.bitMatrix
										,I.bitKitParent
										,I.numItemGroup
										,vcItemName
										,I.bintCreatedDate
										,vcManufacturer
										,txtItemDesc
										,vcSKU
										,fltHeight
										,fltWidth
										,fltLength
										,vcModelID
										,fltWeight
										,numBaseUnit
										,numSaleUnit
										,numPurchaseUnit
										,bitFreeShipping
										,C.vcCategoryName
										,C.vcDescription
										,charItemType
										,monListPrice
										,bitAllowBackOrder
										,bitShowInStock
										,numVendorID
										,numItemClassification
										' ||(CASE WHEN LENGTH(coalesce(v_numESItemCodes,'')) > 0 THEN ',C.numCategoryID' ELSE '' END)  || '
										,I.numManufacturer',v_searchPositionColumnGroupBy,v_SortFields_GroupBy
					  					,v_DefaultSortFields,(CASE WHEN coalesce(v_bitDisplayCategory, false) = true THEN ' HAVING 1 = (CASE WHEN (I.charItemType=''P'' AND COALESCE(I.bitKitParent,false) = false) THEN CASE WHEN (COALESCE(SUM(numOnHand), 0) > 0 OR COALESCE(SUM(numAllocation), 0) > 0) THEN 1 ELSE 0 END ELSE 1 END) ' ELSE '' END) 
   );                 
   
   RAISE NOTICE '%', v_strSQL;
   EXECUTE v_strSQL;
   
   IF v_numDomainID IN(204,214,215) then
	
      v_strSQL := ' DELETE FROM 
									tt_TEMPItems
								WHERE 
									numItemCode IN (
														SELECT 
															F.numItemCode
														FROM 
															tt_TEMPItems AS F
														WHERE 
															COALESCE(F.bitMatrix,false) = true 
															AND EXISTS (
																		SELECT 
																			t1.vcItemName, t1.bitMatrix, t1.numItemGroup, Count(t1.numItemCode)
																		FROM 
																			tt_TEMPItems t1
																		WHERE 
																			t1.vcItemName = F.vcItemName
																			AND t1.bitMatrix = true
																			AND t1.numItemGroup = F.numItemGroup
																		GROUP BY 
																			t1.vcItemName, t1.bitMatrix, t1.numItemGroup
																		HAVING 
																			Count(t1.numItemCode) > 1
																	)
													)
									AND numItemCode NOT IN (
																			SELECT 
																				Min(numItemCode)
																			FROM 
																				tt_TEMPItems AS F
																			WHERE
																				COALESCE(F.bitMatrix,false) = true 
																				AND Exists (
																							SELECT 
																								t2.vcItemName, t2.bitMatrix, t2.numItemGroup, Count(t2.numItemCode)
																							FROM 
																								tt_TEMPItems t2
																							WHERE 
																								t2.vcItemName = F.vcItemName
																								AND t2.bitMatrix = true
																								AND t2.numItemGroup = F.numItemGroup
																							GROUP BY 
																								t2.vcItemName, t2.bitMatrix, t2.numItemGroup
																							HAVING 
																								Count(t2.numItemCode) > 1
																						)
																			GROUP BY 
																				vcItemName, bitMatrix, numItemGroup
																		);';
   end if;
									  
   RAISE NOTICE '%', v_strSQL;
   EXECUTE v_strSQL;
   
   v_strSQL := ' 
			   DROP TABLE IF EXISTS tt_TEMPPagedItems;
			   CREATE TEMPORARY TABLE tt_TEMPPagedItems AS
			   WITH ItemSorted AS 
			   (
			   SELECT  ROW_NUMBER() OVER ( ORDER BY  ' || coalesce(v_SortString,'') || coalesce(v_DefaultSort,'') || ' ' || coalesce(v_vcSort,'') || ' ) AS Rownumber, * 
			   FROM  tt_TEMPItems 
			   WHERE RowID = 1
			   )
			   SELECT * FROM ItemSorted ';
   
   v_fldList := (SELECT string_agg(concat('"',REPLACE(FLd_label,' ','') || '_C','"'), ',' ORDER BY FLd_label) 
				 FROM CFW_Fld_Master WHERE numDomainID = v_numDomainID AND Grp_id = 5);
   v_fldList1 := (SELECT string_agg(concat('"',REPLACE(FLd_label,' ','') || '_' || SUBSTR(CAST(Fld_id AS VARCHAR(30)),1,30),'"'), ','  ORDER BY FLd_label)  
				  FROM CFW_Fld_Master WHERE numDomainID = v_numDomainID AND Grp_id = 9);
   
	/***Changed By: Chirag R:****************/
	/***Note: Changes for getting records Elastic Search Affected Items ********/
    --SET @strSQL = @strSQL || 'SELECT * INTO tt_TEMPPagedItems FROM ItemSorted WHERE Rownumber > ' || CONVERT(VARCHAR(10), @firstRec) || '
    --                            AND   Rownumber < ' || CONVERT(VARCHAR(10), @lastRec) || '
    --                            order by Rownumber; '

   v_bitGetItemForElasticSearch := CASE WHEN LENGTH(v_numESItemCodes) > 0 THEN true ELSE false END;
  
   IF(v_bitGetItemForElasticSearch = true) then
	
      v_strSQL := coalesce(v_strSQL,'') || ' WHERE numItemCode IN (' || coalesce(v_numESItemCodes,'') || ')';
   ELSE
      v_strSQL := coalesce(v_strSQL,'') || ' WHERE Rownumber > ' || SUBSTR(CAST(v_firstRec AS VARCHAR(10)),1,10) || '
							AND   Rownumber < ' || SUBSTR(CAST(v_lastRec AS VARCHAR(10)),1,10);
   end if;
   v_strSQL := coalesce(v_strSQL,'') || ' order by Rownumber; ';
	
   RAISE NOTICE '%', v_strSQL;
   EXECUTE v_strSQL;
   
   /***Changes Completed: Chirag R:****************/                

   v_strSQL := CONCAT('	SELECT  
								Rownumber
								,I.numItemCode
								,vcCategoryName
								,CategoryDesc
								,vcItemName
								,txtItemDesc
								,vcManufacturer
								,vcSKU
								,fltHeight
								,fltWidth
								,fltLength
								,vcModelID
								,fltWeight
								,bitFreeShipping
								,',(CASE
   WHEN v_tintPreLoginPriceLevel > 0
   THEN 'COALESCE((CASE 
											WHEN PricingOption.tintRuleType = 1 AND PricingOption.tintDiscountType = 1
											THEN COALESCE((CASE WHEN I.charItemType = ''P'' THEN ( UOM * COALESCE(W1.monWListPrice,0)) ELSE (UOM * monListPrice) END),0) - (COALESCE((CASE WHEN I.charItemType = ''P'' THEN ( UOM * COALESCE(W1.monWListPrice,0)) ELSE (UOM * monListPrice) END),0) * (PricingOption.decDiscount / 100 ) )
											WHEN PricingOption.tintRuleType = 1 AND PricingOption.tintDiscountType = 2
											THEN COALESCE((CASE WHEN I.charItemType = ''P'' THEN ( UOM * COALESCE(W1.monWListPrice,0)) ELSE (UOM * monListPrice) END),0) - PricingOption.decDiscount
											WHEN PricingOption.tintRuleType = 2 AND PricingOption.tintDiscountType = 1
											THEN COALESCE(Vendor.monCost,0) + (COALESCE(Vendor.monCost,0) * (PricingOption.decDiscount / 100 ) )
											WHEN PricingOption.tintRuleType = 2 AND PricingOption.tintDiscountType = 2
											THEN COALESCE(Vendor.monCost,0) + PricingOption.decDiscount
											WHEN PricingOption.tintRuleType = 3
											THEN PricingOption.decDiscount
										END),COALESCE(CASE WHEN I.charItemType = ''P'' THEN ( UOM * COALESCE(W1.monWListPrice,0)) ELSE (UOM * monListPrice) END, 0))'
   ELSE 'COALESCE(CASE WHEN I.charItemType = ''P'' THEN ( UOM * COALESCE(W1.monWListPrice,0)) ELSE (UOM * monListPrice) END, 0)'
   END),
   ' AS monListPrice,
									COALESCE(CASE WHEN I.charItemType = ''P'' THEN ( UOM * COALESCE(W1.monWListPrice,0)) ELSE (UOM * monListPrice) END, 0) monMSRP					   
									',(CASE WHEN v_numDomainID = 172 THEN ',COALESCE(CASE 
											WHEN tintRuleType = 1 AND tintDiscountType = 1
											THEN COALESCE((CASE WHEN I.charItemType = ''P'' THEN ( UOM * COALESCE(W1.monWListPrice,0)) ELSE (UOM * monListPrice) END),0) - (COALESCE((CASE WHEN I.charItemType = ''P'' THEN ( UOM * COALESCE(W1.monWListPrice,0)) ELSE (UOM * monListPrice) END),0) * ( decDiscount / 100 ) )
											WHEN tintRuleType = 1 AND tintDiscountType = 2
											THEN COALESCE((CASE WHEN I.charItemType = ''P'' THEN ( UOM * COALESCE(W1.monWListPrice,0)) ELSE (UOM * monListPrice) END),0) - decDiscount
											WHEN tintRuleType = 2 AND tintDiscountType = 1
											THEN COALESCE(Vendor.monCost,0) + (COALESCE(Vendor.monCost,0) * ( decDiscount / 100 ) )
											WHEN tintRuleType = 2 AND tintDiscountType = 2
											THEN COALESCE(Vendor.monCost,0) + decDiscount
											WHEN tintRuleType = 3
											THEN decDiscount
										END, 0) AS monFirstPriceLevelPrice,COALESCE(CASE 
										WHEN tintRuleType = 1 AND tintDiscountType = 1 
										THEN decDiscount 
										WHEN tintRuleType = 1 AND tintDiscountType = 2
										THEN (decDiscount * 100) / (COALESCE((CASE WHEN I.charItemType = ''P'' THEN ( UOM * COALESCE(W1.monWListPrice,0)) ELSE (UOM * monListPrice) END),0) - decDiscount)
										WHEN tintRuleType = 2 AND tintDiscountType = 1
										THEN decDiscount
										WHEN tintRuleType = 2 AND tintDiscountType = 2
										THEN (decDiscount * 100) / (COALESCE(Vendor.monCost,0) + decDiscount)
										WHEN tintRuleType = 3
										THEN 0
									END,0) AS fltFirstPriceLevelDiscount' ELSE ',0 AS monFirstPriceLevelPrice, 0 AS fltFirstPriceLevelDiscount' END),'  
								,vcPathForImage
								,vcpathForTImage
								,UOM AS UOMConversionFactor
								,UOMPurchase AS UOMPurchaseConversionFactor
								,(CASE WHEN charItemType <> ''S'' AND charItemType <> ''N'' AND COALESCE(bitKitParent,false)=false
											THEN (CASE 
													WHEN bitAllowBackOrder = true THEN true
													WHEN (COALESCE(I.numOnHand,0)<=0) THEN false
													ELSE true
												END)
											ELSE true
								END) AS bitInStock,
								(
									CASE WHEN COALESCE(bitKitParent,false) = true
									THEN
										(CASE 
											WHEN ((SELECT COUNT(*) FROM ItemDetails ID INNER JOIN Item IInner ON ID.numChildItemID = IInner.numItemCode WHERE ID.numItemKitID = I.numItemCode AND COALESCE(IInner.bitKitParent,false) = true)) > 0 
											THEN 1 
											ELSE 0 
										END)
									ELSE
										0
									END
								) bitHasChildKits
								,(CASE WHEN charItemType <> ''S'' AND charItemType <> ''N''
											THEN ( CASE WHEN bitShowInStock = true
														THEN (CASE 
																WHEN bitAllowBackOrder = true AND I.numDomainID <> 172 AND COALESCE(I.numOnHand,0)<=0 AND COALESCE(I.numVendorID,0) > 0 AND GetVendorPreferredMethodLeadTime(I.numVendorID::NUMERIC,0::NUMERIC) > 0 THEN CONCAT(''<font class="vendorLeadTime" style="color:green">Usually ships in '',GetVendorPreferredMethodLeadTime(I.numVendorID::NUMERIC,0::NUMERIC),'' days</font>'')
																WHEN bitAllowBackOrder = true AND COALESCE(I.numOnHand,0)<=0 THEN (CASE WHEN I.numDomainID = 172 THEN ''<font style="color:green">AVAILABLE</font>'' ELSE ''<font style="color:green">In Stock</font>'' END)
																WHEN (COALESCE(I.numOnHand,0)<=0) THEN (CASE WHEN I.numDomainID = 172 THEN ''<font style="color:red">ON HOLD</font>'' ELSE ''<font style="color:red">Out Of Stock</font>'' END)
																ELSE (CASE WHEN I.numDomainID = 172 THEN ''<font style="color:green">AVAILABLE</font>'' ELSE ''<font style="color:green">In Stock</font>'' END)  
															END)
														ELSE ''''
													END )
											ELSE ''''
										END ) AS InStock
								, COALESCE(W1.numWareHouseItemID,0) "numWareHouseItemID"
								,COALESCE(TablePromotion.numTotalPromotions,0) numTotalPromotions
								,COALESCE(TablePromotion.vcPromoDesc,'''') vcPromoDesc
								,COALESCE(TablePromotion.bitRequireCouponCode,false) bitRequireCouponCode
								' ||(CASE WHEN LENGTH(coalesce(v_numESItemCodes,'')) > 0 THEN ',I.numCategoryID' ELSE '' END)  || '
								,I.numManufacturer
								,I.bintCreatedDate
								,(CASE WHEN COALESCE(I.numItemGroup,0) > 0 AND COALESCE(I.bitMatrix,false) = true THEN fn_GetItemAttributes(I.numDomainID,I.numItemCode,true::BOOLEAN) ELSE '''' END) AS vcAttributes
								',
   (CASE WHEN LENGTH(coalesce(v_fldList,'')) > 0 THEN CONCAT(',',v_fldList) ELSE '' END),(CASE WHEN LENGTH(coalesce(v_fldList1,'')) > 0 THEN CONCAT(',',v_fldList1) ELSE '' END),'
                            FROM 
								tt_TEMPPagedItems I
							LEFT JOIN ItemImages IM ON I.numItemCode = IM.numItemCode AND IM.bitdefault=true AND IM.bitIsImage = true
							LEFT JOIN Vendor ON Vendor.numVendorID =I.numVendorID AND Vendor.numItemCode=I.numItemCode
							LEFT JOIN LATERAL (SELECT * FROM fn_GetItemPromotions(',
   v_numDomainID,',',v_numDivisionID,
   ',I.numItemCode,I.numItemClassification',',',v_numDefaultRelationship,
   ',',v_numDefaultProfile,',1::SMALLINT)) TablePromotion ON true ',(CASE
   WHEN v_tintPreLoginPriceLevel > 0
   THEN CONCAT(' LEFT JOIN LATERAL (SELECT * FROM PricingTable WHERE numItemCode=I.numItemCode AND COALESCE(numPriceRuleID,0) = 0 AND COALESCE(numCurrencyID,0) = 0 ORDER BY numPricingID OFFSET ',CAST(v_tintPreLoginPriceLevel -1 AS VARCHAR(30)),
      ' ROWS FETCH NEXT 1 ROWS ONLY) As PricingOption ON true ')
   ELSE ''
   END),(CASE
   WHEN v_numDomainID = 172
   THEN ' LEFT JOIN LATERAL (SELECT * FROM PricingTable WHERE numItemCode=I.numItemCode AND COALESCE(numCurrencyID,0) = 0 LIMIT 1) AS PT ON true '
   ELSE ''
   END),
   ' 
							LEFT JOIN LATERAL (SELECT numWareHouseItemID,monWListPrice FROM WareHouseItems W 
											WHERE  W.numItemID = I.numItemCode 
											AND W.numDomainID = ', SUBSTR(CAST(v_numDomainID AS VARCHAR(10)),1,10),'
											AND numWareHouseID IN (SELECT CAST(OutParam AS NUMERIC(18)) FROM SplitString(''' || coalesce(v_vcWarehouseIDs,'') || ''','','')) LIMIT 1) AS W1 ON true 			  
								INNER JOIN LATERAL (SELECT fn_UOMConversion(COALESCE(I.numSaleUnit, 0),I.numItemCode, I.numDomainId,COALESCE(I.numBaseUnit, 0)) AS UOM) AS UOM ON true 			  
								INNER JOIN LATERAL (SELECT fn_UOMConversion(COALESCE(I.numPurchaseUnit, 0),I.numItemCode, I.numDomainId,COALESCE(I.numBaseUnit, 0)) AS UOMPurchase) AS UOMPurchase ON true ');
			
   IF LENGTH(coalesce(v_fldList,'')) > 0 then
	
      v_strSQL := coalesce(v_strSQL,'') || ' left join (
											SELECT *   
											FROM  crosstab(
											''SELECT RecId, Fld_label, MAX(Fld_Value) 
											  FROM (
												  SELECT  t1.RecId, REPLACE(t1.Fld_label,'''' '''','''''''') || ''''_C'''' as Fld_label, 
												  CASE t1.fld_type WHEN ''''SelectBox'''' THEN COALESCE(fn_GetListItemName(t2.Fld_Value::NUMERIC),'''''''')
																		WHEN ''''CheckBox'''' THEN COALESCE(fn_getCustomDataValue(t2.Fld_Value::VARCHAR,9::NUMERIC),'''''''')
																		ELSE Replace(COALESCE(t2.Fld_Value,''''0'''')::VARCHAR,''''$'''',''''$'''') END AS Fld_Value 
												  FROM (SELECT numItemCode AS RecId, t1.Fld_Id, t1.Fld_label, t1.fld_type, t1.Grp_id FROM CFW_Fld_Master t1 , tt_TEMPPagedItems where t1.Grp_id = 5 and t1.numDomainID=' || SUBSTR(CAST(v_numDomainID AS VARCHAR(18)),1,18) || ' AND t1.fld_type <> ''''Link'''') t1
												  LEFT JOIN CFW_FLD_Values_Item AS t2 ON t1.Fld_ID = t2.Fld_ID 
												  AND t2.RecId = t1.RecId
												  ORDER BY
													t1.RecId,t1.Fld_label
											  ) tmp
											  GROUP BY RecId, Fld_label 
											  ORDER BY 1,2
											  '')
											  AS final_result_pivot
											  ( RecId NUMERIC(18), ' || REPLACE(COALESCE(v_fldList,''),',',' TEXT,') || ' TEXT) 
											  ) AS pvt on I.numItemCode=pvt.RecId ';
   end if;

   IF LENGTH(coalesce(v_fldList1,'')) > 0 then
	
      v_strSQL := coalesce(v_strSQL,'') || ' left join (
												SELECT *   
												FROM  crosstab(
												''SELECT RecId, Fld_label, MAX(Fld_Value) 
											  	FROM (
													SELECT  t2.numItemCode AS RecId, REPLACE(t1.Fld_label,'''' '''','''''''') || ''''_'''' || CAST(t1.Fld_Id AS VARCHAR) as Fld_label, 
													CASE t1.fld_type WHEN ''''SelectBox'''' THEN COALESCE(fn_GetListItemName(t2.Fld_Value::NUMERIC),'''''''')
																		WHEN ''''CheckBox'''' THEN COALESCE(fn_getCustomDataValue(t2.Fld_Value::VARCHAR,9::NUMERIC),'''''''')
																		ELSE Replace(COALESCE(t2.Fld_Value,''''0'''')::VARCHAR,''''$'''',''''\$\$'''') end AS Fld_Value 
													FROM CFW_Fld_Master t1 
													JOIN ItemAttributes AS t2 ON t1.Fld_ID = t2.Fld_ID where t1.Grp_id = 9 and t1.numDomainID=' || SUBSTR(CAST(v_numDomainID AS VARCHAR(18)),1,18) || ' AND t1.fld_type <> ''''Link''''
													AND t2.numItemCode IN (SELECT numItemCode FROM tt_TEMPPagedItems )
													ORDER BY
													t2.numItemCode,t1.Fld_label
												) tmp
												GROUP BY RecId, Fld_label 
												ORDER BY 1,2
												'')
												AS final_result_pivot ( RecId NUMERIC(18), ' || REPLACE(COALESCE(v_fldList1,''),',',' TEXT,') || ' TEXT)
												) AS pvt1 on I.numItemCode=pvt1.RecId ';
   ELSE
      v_strSQL := REPLACE(v_strSQL,',##FLDLIST1##','');
   end if;

   v_strSQL := coalesce(v_strSQL,'') || ' order by Rownumber; ';                
   --v_strSQL := coalesce(v_strSQL,'') || ' SELECT COUNT(*) INTO v_TotRecs FROM tt_TEMPItems WHERE RowID=1; ';                            

   v_tmpSQL := v_strSQL;
	
   RAISE NOTICE '%',CAST(v_tmpSQL AS TEXT);
   OPEN SWV_RefCur FOR 
   EXECUTE v_strSQL;      
   
   SELECT COUNT(*) INTO v_TotRecs
   FROM tt_TEMPItems 
   WHERE RowID=1;        
   
   v_tintOrder := 0;                                                  
   v_WhereCondition := '';                 
                   
   DROP TABLE IF EXISTS tt_TEMPAVAILABLEFIELDS CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPAVAILABLEFIELDS
   (
      numFormFieldId  NUMERIC(9,0),
      vcFormFieldName VARCHAR(50),
      vcFieldType VARCHAR(15),
      vcAssociatedControlType VARCHAR(50),
      numListID NUMERIC(18,0),
      vcListItemType CHAR(1),
      intRowNum INTEGER,
      vcItemValue VARCHAR(3000)
   );
		   
   INSERT INTO tt_TEMPAVAILABLEFIELDS(numFormFieldId
		,vcFormFieldName
		,vcFieldType
		,vcAssociatedControlType
		,numListID
		,vcListItemType
		,intRowNum)
   SELECT
   Fld_id
		,REPLACE(Fld_label,' ','') AS vcFormFieldName
		,CAST(CASE GRP_ID
   WHEN 4 then 'C'
   WHEN 1 THEN 'D'
   WHEN 9 THEN CAST(Fld_id AS VARCHAR(15))
   ELSE 'C'
   END AS VARCHAR(15)) AS vcFieldType
		,fld_type as vcAssociatedControlType
		,coalesce(C.numlistid,0) AS numListID
		,CASE
   WHEN C.numlistid > 0 THEN 'L'
   ELSE ''
   END AS vcListItemType
		,ROW_NUMBER() OVER(ORDER BY Fld_id ASC) AS intRowNum
   FROM
   CFW_Fld_Master C
   LEFT JOIN
   CFW_Validation V
   ON
   V.numFieldId = C.Fld_id
   WHERE
   C.numDomainID = v_numDomainID
   AND GRP_ID IN(5,9);
		  
   OPEN SWV_RefCur2 FOR
   SELECT * FROM tt_TEMPAVAILABLEFIELDS;
END; 
$$;


