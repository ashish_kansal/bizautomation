-- Stored procedure definition script USP_ManageBizDocApproval for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ManageBizDocApproval(v_intMode INTEGER,
v_numDomainID INTEGER,
v_numBizDocAppID NUMERIC(18,0) DEFAULT 0,
v_numBizDocTypeID NUMERIC(18,0) DEFAULT 0,
v_numRangeFrom NUMERIC(18,2) DEFAULT 0,
v_numRangeTo NUMERIC(18,2) DEFAULT 0,
v_btBizDocCreated BOOLEAN DEFAULT false,
v_btBizDocPaidFull BOOLEAN DEFAULT false,
v_numActionTypeID NUMERIC(18,0) DEFAULT 0,
v_numApproveDocStatusID NUMERIC(18,0) DEFAULT 0,
v_numDeclainDocStatusID NUMERIC(18,0) DEFAULT 0, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
BEGIN
   if v_intMode = 0 then
	
      DELETE FROM BizDocApprovalRuleEmployee WHERE numBizDocAppID
      in(select numBizDocAppID from BizDocApprovalRule where numDomainID = v_numDomainID);
      DELETE FROM BizDocStatusApprove WHERE numDomainID = v_numDomainID;
      DELETE FROM BizDocApprovalRule WHERE numDomainID = v_numDomainID;
      INSERT INTO BizDocApprovalRule  SELECT v_numBizDocTypeID,v_numDomainID,v_numRangeFrom,v_numRangeTo,
								v_btBizDocCreated,v_btBizDocPaidFull,v_numActionTypeID,v_numApproveDocStatusID,
								v_numDeclainDocStatusID;
      open SWV_RefCur for
      SELECT CURRVAL('BizDocApprovalRule_seq');
   ELSEIF v_intMode > 0
   then
	
      INSERT INTO BizDocApprovalRule  SELECT v_numBizDocTypeID,v_numDomainID,v_numRangeFrom,v_numRangeTo,
								v_btBizDocCreated,v_btBizDocPaidFull,v_numActionTypeID,v_numApproveDocStatusID,
								v_numDeclainDocStatusID;
      open SWV_RefCur for
      SELECT CURRVAL('BizDocApprovalRule_seq');
   ELSEIF v_intMode = -1
   then
	
      DELETE FROM BizDocApprovalRuleEmployee WHERE numBizDocAppID
      in(select numBizDocAppID from BizDocApprovalRule where numDomainID = v_numDomainID);
      DELETE FROM BizDocStatusApprove WHERE numDomainID = v_numDomainID;
      DELETE FROM BizDocApprovalRule WHERE numDomainID = v_numDomainID;
      open SWV_RefCur for
      SELECT 1;
   end if;
   RETURN;
END; $$;



