-- Stored procedure definition script USP_MarkReadInboxItems for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_MarkReadInboxItems(v_numEmailHstrID NUMERIC(9,0))
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   update EmailHistory set bitIsRead = true
   where numEmailHstrID = v_numEmailHstrID;
   RETURN;
END; $$;


