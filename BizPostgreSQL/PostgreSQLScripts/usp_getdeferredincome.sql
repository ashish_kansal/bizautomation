-- FUNCTION: public.usp_getdeferredincome(integer, numeric, refcursor)

-- DROP FUNCTION public.usp_getdeferredincome(integer, numeric, refcursor);

CREATE OR REPLACE FUNCTION public.usp_getdeferredincome(
	v_clienttimezoneoffset integer,
	v_numdomainid numeric DEFAULT 0,
	INOUT swv_refcur refcursor DEFAULT NULL::refcursor)
    RETURNS refcursor
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
AS $BODY$
BEGIN
   open SWV_RefCur for Select Opp.vcpOppName As BizDocID, cast(OppBizDocsDet.monAmount as VARCHAR(255)) as TotalIncome,FormatedDateTimeFromDate(OppBizDocsDet.dtCreationDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval),v_numDomainId) as dtCreationDate,
 coalesce(OppBizDocsDet.monAmount,0) -CAST((select cast(coalesce(sum(monAmount),0) as VARCHAR(100)) From OpportunityBizDocsPaymentDetails Where numBizDocsPaymentDetId = OppBizDocsDet.numBizDocsPaymentDetId And bitIntegrated = true) AS NUMERIC) as Balance,
 CAST(OppBizDocsDet.sintDeferredIncomePeriod AS VARCHAR(10)) || ' months, Starting on'  as sintDeferredIncomePeriod,
 FormatedDateTimeFromDate(OppBizDocsDet.dtDeferredIncomeStartDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval),v_numDomainId) as  dtDeferredIncomeStartDate
   From OpportunityBizDocsDetails OppBizDocsDet       
 --inner join OpportunityBizDocsPaymentDetails OppBizDocsPayDet on OppBizDocsDet.numBizDocsPaymentDetId=OppBizDocsPayDet.numBizDocsPaymentDetId            
   Inner Join OpportunityBizDocs OppBizDocs on OppBizDocsDet.numBizDocsId = OppBizDocs.numOppBizDocsId
   Inner Join OpportunityMaster Opp On OppBizDocs.numoppid = Opp.numOppId
   Inner Join AdditionalContactsInformation ADC On Opp.numContactId = ADC.numContactId
   Where OppBizDocsDet.bitAuthoritativeBizDocs = true And OppBizDocsDet.bitSalesDeferredIncome = true And OppBizDocsDet.numDomainId = v_numDomainId
   And coalesce(OppBizDocsDet.monAmount,0) - CAST((select cast(coalesce(sum(monAmount),0) as VARCHAR(100)) From OpportunityBizDocsPaymentDetails Where numBizDocsPaymentDetId = OppBizDocsDet.numBizDocsPaymentDetId And bitIntegrated = true) AS NUMERIC) <> 0;
END;
$BODY$;

ALTER FUNCTION public.usp_getdeferredincome(integer, numeric, refcursor)
    OWNER TO postgres;
