CREATE OR REPLACE FUNCTION USP_OpportunityMaster_UpdatePurchaseOrderCost(
	p_numDomainID NUMERIC(18,0),
	p_numUserCntID NUMERIC(18,0),
	p_vcSelectedRecords TEXT,
	p_ClientTimeZoneOffset INT
)
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE v_i INT DEFAULT 1;
		v_iCount INT;
		v_numOppID NUMERIC(18,0);
		v_j INT DEFAULT 1;
		v_jCount INT;
		v_numVendorID NUMERIC(18,0);
		v_numContactID NUMERIC(18,0);
		v_numCurrencyID NUMERIC(18,0);
		v_intBillingDays INT;
		v_strItems TEXT DEFAULT '';
		v_dtEstimatedCloseDate TIMESTAMP;
		my_ex_state TEXT;
		my_ex_message TEXT;
		my_ex_detail TEXT;
		my_ex_hint TEXT;
		my_ex_ctx TEXT;
BEGIN
	DROP TABLE IF EXISTS tt_TEMPUpdatePurchaseOrderCost CASCADE;
	CREATE TEMPORARY TABLE tt_TEMPUpdatePurchaseOrderCost
	(
		numVendorID NUMERIC(18,0)
		,numSelectedVendorID NUMERIC(18,0)
		,numOppID NUMERIC(18,0)
		,numOppItemID NUMERIC(18,0)
		,numUnits FLOAT
		,monUnitCost DECIMAL(20,5)
	);

	INSERT INTO tt_TEMPUpdatePurchaseOrderCost
	(
		numVendorID
		,numSelectedVendorID
		,numOppID
		,numOppItemID
		,numUnits
		,monUnitCost
	)
	SELECT
		COALESCE(VendorID,0)
		,COALESCE(SelectedVendorID,0)
		,COALESCE(OppID,0)
		,COALESCE(OppItemID,0)
		,COALESCE(Units,0)
		,COALESCE(UnitCost,0)
	FROM
	XMLTABLE
	(
		'NewDataSet/Table1'
		PASSING 
			CAST(p_vcSelectedRecords AS XML)
		COLUMNS
			id FOR ORDINALITY,
			VendorID NUMERIC(18,0) PATH 'VendorID',
			SelectedVendorID NUMERIC(18,0) PATH 'SelectedVendorID',
			OppID NUMERIC(18,0) PATH 'OppID',
			OppItemID NUMERIC(18,0) PATH 'OppItemID',
			Units DOUBLE PRECISION PATH 'Units',
			UnitCost DECIMAL(20,5) PATH 'UnitCost'
	) AS X;

	IF EXISTS (SELECT OB.numOppBizDocsId FROM OpportunityBizDocs OB INNER JOIN tt_TEMPUpdatePurchaseOrderCost T ON OB.numOppId = T.numOppID) THEN
		RAISE EXCEPTION 'BIZ_DOC_EXISTS';
		RETURN;
	ELSE
		DROP TABLE IF EXISTS tt_TEMPVendorItemsUpdatePurchaseOrderCost CASCADE;
		CREATE TEMPORARY TABLE tt_TEMPVendorItemsUpdatePurchaseOrderCost
		(
			ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1)
			,numVendorID NUMERIC(18,0)
			,numItemCode NUMERIC(18,0)
			,numWarehouseItemID NUMERIC(18,0)
			,numUnitHour DOUBLE PRECISION
			,monPrice DECIMAL(20,5)
		);

		INSERT INTO tt_TEMPVendorItemsUpdatePurchaseOrderCost
		(
			numVendorID
			,numItemCode
			,numWarehouseItemID
			,numUnitHour
			,monPrice
		)
		SELECT 
			T.numSelectedVendorID
			,OI.numItemCode
			,OI.numWarehouseItmsID
			,OI.numUnitHour
			,T.monUnitCost
		FROM
			tt_TEMPUpdatePurchaseOrderCost T
		INNER JOIN
			OpportunityItems OI
		ON
			T.numOppID = OI.numOppId
			AND T.numOppItemID = OI.numoppitemtCode
		INNER JOIN
			OpportunityMaster OM
		ON
			OI.numOppId = OM.numOppId
		WHERE
			OM.numDomainId = p_numDomainID
			AND OM.numDivisionId <> T.numSelectedVendorID;

		BEGIN
			DROP TABLE IF EXISTS tt_TEMPOppUpdatePurchaseOrderCost CASCADE;
			CREATE TEMPORARY TABLE tt_TEMPOppUpdatePurchaseOrderCost
			(
				ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1)
				,numOppID NUMERIC(18,0)
			);

			INSERT INTO tt_TEMPOppUpdatePurchaseOrderCost
			(
				numOppID
			)
			SELECT DISTINCT
				numOppID
			FROM
				tt_TEMPUpdatePurchaseOrderCost;

			SELECT COUNT(*) INTO v_iCount FROM tt_TEMPOppUpdatePurchaseOrderCost;

			-- DELETE PO NOT ITEM LEFT IN ORDER OR UPDATE ORDER TOTAL AMOUNT AND TAXES
			WHILE v_i <= v_iCount  LOOP
				SELECT numOppID INTO v_numOppID FROM tt_TEMPOppUpdatePurchaseOrderCost WHERE ID = v_i;

				PERFORM USP_RevertDetailsOpp(v_numOppID,0,1::SMALLINT,p_numUserCntID);

				-- DELETE ITEMS FROM ORDER FOR WHOM ANOTHER VENDOR IS SELECTED TO CREATE NEW ORDER
				DELETE FROM OpportunityItems OI USING
					OpportunityMaster OM, tt_TEMPUpdatePurchaseOrderCost T					
				WHERE
					OI.numOppId = OM.numOppId 
					AND OI.numOppId = T.numOppID
					AND OI.numoppitemtCode = T.numOppItemID
					AND OM.numDomainId = p_numDomainID
					AND OM.numOppId = v_numOppID
					AND OM.numDivisionId <> T.numSelectedVendorID;

				-- UPDATE LINE ITEM UNITS AND COST WHERE VENDOR IS SAME
				UPDATE 
					OpportunityItems OI
				SET
					numUnitHour = COALESCE(T.numUnits,0)
					,monPrice = COALESCE(T.monUnitCost,0)
					,monTotAmount = (COALESCE(T.numUnits,0) * (CASE 
																	WHEN COALESCE(OI.bitDiscountType,false) = false 
																	THEN (COALESCE(T.monUnitCost,0) - (COALESCE(T.monUnitCost,0) * (COALESCE(OI.fltDiscount,0) / 100)))
																	ELSE (COALESCE(T.monUnitCost,0) -COALESCE(OI.fltDiscount,0)) 
																END))  
					,monTotAmtBefDiscount = COALESCE(T.numUnits,0) * COALESCE(T.monUnitCost,0)
				FROM
					OpportunityMaster OM, tt_TEMPUpdatePurchaseOrderCost T
				WHERE
					OM.numOppId = OI.numOppId
					AND OM.numDomainId = p_numDomainID
					AND OM.numOppId = v_numOppID
					AND OM.numDivisionId = T.numSelectedVendorID
					AND OI.numOppId = T.numOppID
					AND OI.numoppitemtCode = T.numOppItemID;

				PERFORM USP_UpdatingInventoryonCloseDeal (v_numOppID := v_numOppID,v_numOppItemId := 0,v_numUserCntID := p_numUserCntID);

				IF EXISTS (SELECT numOppId FROM OpportunityMaster WHERE numDomainId=p_numDomainID AND numOppId=v_numOppID) AND NOT EXISTS (SELECT numoppitemtCode FROM OpportunityItems OI WHERE OI.numOppId=v_numOppID) THEN
					PERFORM USP_DeleteOppurtunity (v_numOppId=v_numOppID,p_numDomainID=p_numDomainID,p_numUserCntID=p_numUserCntID);
				ELSE
					UPDATE OpportunityMaster SET monDealAmount=GetDealAmount(v_numOppID,now()::TIMESTAMP,0),bintModifiedDate=timezone('utc', now()),numModifiedBy=p_numUserCntID WHERE numOppId=v_numOppID;

					--INSERT TAX FOR DIVISION   
					IF (select COALESCE(bitPurchaseTaxCredit,false) from Domain where numDomainId=p_numDomainID)=true THEN
						--Delete Tax for Opportunity Items if item deleted 
						DELETE FROM OpportunityItemsTaxItems WHERE numOppId=v_numOppId AND numOppItemID NOT IN (SELECT numoppitemtCode FROM OpportunityItems WHERE numOppId=v_numOppID);

						--Insert Tax for Opportunity Items
						INSERT INTO OpportunityItemsTaxItems 
						(
							numOppId,
							numOppItemID,
							numTaxItemID,
							numTaxID
						) 
						SELECT 
							v_numOppId,
							OI.numoppitemtCode,
							TI.numTaxItemID,
							0
						FROM 
							OpportunityItems OI 
						JOIN 
							ItemTax IT 
						ON 
							OI.numItemCode=IT.numItemCode 
						JOIN
							TaxItems TI 
						ON 
							TI.numTaxItemID = IT.numTaxItemID 
						WHERE 
							OI.numOppId=v_numOppID 
							AND IT.bitApplicable=true
							AND OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=v_numOppID)
						UNION
						SELECT 
							v_numOppId,
							OI.numoppitemtCode,
							0,
							0
						FROM 
							OpportunityItems OI 
						JOIN 
							Item I 
						ON 
							OI.numItemCode=I.numItemCode
						WHERE 
							OI.numOppId=v_numOppID 
							AND I.bitTaxable=true
							AND OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=v_numOppID)
						UNION
						SELECT
							v_numOppId,
							OI.numoppitemtCode,
							1,
							TD.numTaxID
						FROM
							OpportunityItems OI 
						INNER JOIN
							ItemTax IT
						ON
							IT.numItemCode = OI.numItemCode
						INNER JOIN
							TaxDetails TD
						ON
							TD.numTaxID = IT.numTaxID
							AND TD.numDomainId = p_numDomainID
						WHERE
							OI.numOppId = v_numOppID
							AND OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=v_numOppID);
					END IF;
				END IF;

				v_i := v_i + 1;
			END LOOP;

			DROP TABLE IF EXISTS tt_TEMPVendorUpdatePurchaseOrderCost CASCADE;
			CREATE TEMPORARY TABLE tt_TEMPVendorUpdatePurchaseOrderCost
			(
				ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1)
				,numVendorID NUMERIC(18,0)
				,numContactID NUMERIC(18,0)
				,intBillingDays INT
			);

			INSERT INTO tt_TEMPVendorUpdatePurchaseOrderCost
			(
				numVendorID
				,numContactID
				,intBillingDays
			)
			SELECT DISTINCT
				numVendorID
				,COALESCE((SELECT numContactId FROM AdditionalContactsInformation WHERE numDivisionId=numVendorID ORDER BY COALESCE(bitPrimaryContact,false) DESC LIMIT 1),0)
				,COALESCE(numBillingDays,0)
			FROM
				tt_TEMPVendorItemsUpdatePurchaseOrderCost T
			INNER JOIN
				DivisionMaster
			ON
				T.numVendorID = DivisionMaster.numDivisionID;

			SELECT COALESCE(numCurrencyID,0) INTO v_numCurrencyID FROM Domain WHERE numDomainId = p_numDomainID;

			SELECT COUNT(*) INTO v_jCount FROM tt_TEMPVendorUpdatePurchaseOrderCost;

			WHILE v_j <= v_jCount LOOP			

				SELECT numVendorID,numContactID,intBillingDays INTO v_numVendorID,v_numContactID,v_intBillingDays  FROM tt_TEMPVendorUpdatePurchaseOrderCost WHERE ID = v_j;

				v_strItems := CONCAT('<NewDataSet>',COALESCE((SELECT string_agg(CONCAT('<Item>'
															,'<numoppitemtCode>',0,'</numoppitemtCode>'
														   	,'<numItemCode>',T.numItemCode,'</numItemCode>'
														   	,'<numUnitHour>',COALESCE(numUnitHour,0),'</numUnitHour>'
														   	,'<monPrice>',COALESCE(monPrice,0),'</monPrice>'
													   		,'<monTotAmount>',COALESCE(T.numUnitHour,0) * COALESCE(T.monPrice,0),'</monTotAmount>'
														   	,'<vcItemDesc>',COALESCE(I.txtItemDesc,''),'</vcItemDesc>'
													   	   	,'<numWarehouseItmsID>',COALESCE(T.numWarehouseItemID,0),'</numWarehouseItmsID>'
														   	,'<numToWarehouseItemID>',0,'</numToWarehouseItemID>'
														   	,'<Op_Flag>',1,'</Op_Flag>'
														   	,'<ItemType>',I.charItemType,'</ItemType>'
															,'<DropShip>',0,'</DropShip>'
														   	,'<bitDiscountType>',0,'</bitDiscountType>'
														   	,'<fltDiscount>',0,'</fltDiscount>'
														   	,'<monTotAmtBefDiscount>',COALESCE(T.numUnitHour,0) * COALESCE(T.monPrice,0),'</monTotAmtBefDiscount>'
														   	,'<vcItemName>',vcItemName,'</vcItemName>'
														   	,'<numUOM>',COALESCE(I.numBaseUnit,0),'</numUOM>'
															,'<bitWorkOrder>',false,'</bitWorkOrder>'
														   	,'<numVendorWareHouse>',0,'</numVendorWareHouse>'
														   	,'<numShipmentMethod>',0,'</numShipmentMethod>'
														   	,'<numSOVendorId>',0,'</numSOVendorId>'
														   	,'<numProjectID>',0,'</numProjectID>'
														   	,'<numProjectStageID>',0,'</numProjectStageID>'
														   	,'<Attributes></Attributes>'
														   	,'<bitItemPriceApprovalRequired>',false,'</bitItemPriceApprovalRequired>'
															 ,'</Item>'),'')
															FROM 
																tt_TEMPVendorItemsUpdatePurchaseOrderCost T
															INNER JOIN
																Item I
															ON
																T.numItemCode = I.numItemCode
															WHERE
																T.numVendorID = v_numVendorID),''),'</NewDataSet>');
		
				v_dtEstimatedCloseDate := timezone('utc', now()) + make_interval(mins => -1 * p_ClientTimeZoneOffset);

				--UPDATE MASTER PO AND ADD MERGE PO ITEMS
				PERFORM USP_OppManage (v_numOppID := 0,v_numContactId := v_numContactId,v_numDivisionId := v_numVendorID,v_tintSource := 0,v_vcPOppName := '',
									v_Comments := '',v_bitPublicFlag := false,v_numUserCntID := p_numUserCntID,v_monPAmount := 0,v_numAssignedTo := p_numUserCntID,
									v_numDomainID := p_numDomainID,v_strItems := v_strItems,v_strMilestone := '',v_dtEstimatedCloseDate := v_dtEstimatedCloseDate,v_CampaignID := 0,
									v_lngPConclAnalysis := 0,v_tintOppType := 2::SMALLINT,v_tintActive := 1::SMALLINT,v_numSalesOrPurType := 0,
									v_numRecurringId := 0,v_numCurrencyID := v_numCurrencyID,v_DealStatus := 1::SMALLINT,v_numStatus := 0,v_vcOppRefOrderNo := '',
									v_numBillAddressId := 0,v_numShipAddressId := 0,v_bitStockTransfer := false,v_WebApiId := 0,
									v_tintSourceType := 0::SMALLINT,v_bitDiscountType := false,v_fltDiscount := 0,v_bitBillingTerms := false,
									v_intBillingDays := v_intBillingDays,v_bitInterestType := false,v_fltInterest := 0,v_tintTaxOperator := 0::SMALLINT,
									v_numDiscountAcntType := 0,v_vcWebApiOrderNo := '',v_vcCouponCode := '',v_vcMarketplaceOrderID := 0,
									v_vcMarketplaceOrderDate := NULL,v_vcMarketplaceOrderReportId := '',v_numPercentageComplete := 0,
									v_bitUseShippersAccountNo := false,v_bitUseMarkupShippingRate := false,
									v_numMarkupShippingRate := 0,v_intUsedShippingCompany := 0,v_numShipmentMethod := 0,v_dtReleaseDate := NULL,v_numPartner := 0, v_tintClickBtn := 0,v_numPartenerContactId := 0
									,v_numAccountClass := 0,v_numVendorAddressID := 0, v_numShipFromWarehouse := 0, v_numShippingService := 0,
									v_ClientTimeZoneOffset := 0,v_vcCustomerPO := '',v_bitDropShipAddress := false,v_PromCouponCode := NULL,v_numPromotionId := 0,v_dtExpectedDate := null,v_numProjectID := 0);

				v_j := v_j + 1;
			END LOOP;

		EXCEPTION WHEN OTHERS THEN
				GET STACKED DIAGNOSTICS
				my_ex_state   = RETURNED_SQLSTATE,
				my_ex_message = MESSAGE_TEXT,
				my_ex_detail  = PG_EXCEPTION_DETAIL,
				my_ex_hint    = PG_EXCEPTION_HINT,
				my_ex_ctx     = PG_EXCEPTION_CONTEXT;

				raise exception '% % % % %', my_ex_state, my_ex_message, my_ex_detail, my_ex_hint, my_ex_ctx;
		END;	
	END IF;
END; $$;


