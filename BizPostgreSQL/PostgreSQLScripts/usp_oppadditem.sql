-- Stored procedure definition script USP_OppAddItem for PostgreSQL
CREATE OR REPLACE FUNCTION USP_OppAddItem(v_numItemCode NUMERIC(9,0),
v_vcType VARCHAR(50),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   insert into OpportunityItems(numItemCode,vcType,numUnitHour,monPrice,monTotAmount,vcModelID,vcManufacturer,vcPathForTImage,monVendorCost,monAvgCost)
 --kishan
   select v_numItemCode,v_vcType,0,0,0,(SELECT vcModelID FROM Item WHERE numItemCode = v_numItemCode),(SELECT vcManufacturer FROM Item WHERE numItemCode = v_numItemCode),(SELECT   vcPathForTImage FROM ItemImages WHERE numItemCode = v_numItemCode AND bitDefault = true LIMIT 1),(select coalesce(VN.monCost,0) from Vendor VN INNER JOIN Item IT ON IT.numItemCode = VN.numItemCode and VN.numVendorID = IT.numVendorID where VN.numItemCode = v_numItemCode),
 (SELECT(CASE WHEN coalesce(I.bitVirtualInventory,false) = true THEN 0 ELSE coalesce(I.monAverageCost,0) END) FROM Item I WHERE I.numItemCode = v_numItemCode);

  
open SWV_RefCur for select CURRVAL('OpportunityItems_seq');
END; $$;












