CREATE OR REPLACE FUNCTION USP_OpportunityMaster_GetPOAvailableForMerge(v_numDomainID NUMERIC(18,0),
	v_numLevel SMALLINT,
	v_numVendorID NUMERIC(18,0),
	v_numPOID NUMERIC(18,0), INOUT SWV_RefCur refcursor)
LANGUAGE plpgsql
   AS $$
BEGIN
   IF v_numLevel = 0 then
	
      open SWV_RefCur for
      SELECT
      OpportunityMaster.numDivisionId AS VendorID,
			CompanyInfo.vcCompanyName AS Vendor
      FROM
      OpportunityMaster
      INNER JOIN
      DivisionMaster
      ON
      OpportunityMaster.numDivisionId = DivisionMaster.numDivisionID
      INNER JOIN
      CompanyInfo
      ON
      DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
      WHERE
      tintopptype = 2
      AND tintoppstatus = 1
      AND coalesce(bitStockTransfer,false) = false
      AND coalesce(tintshipped,0) = 0
      AND OpportunityMaster.numDomainId = v_numDomainID
      AND coalesce(bitStopMerge,false) = false
      AND(SELECT COUNT(*) FROM OpportunityItems WHERE numOppId = OpportunityMaster.numOppId) > 0
      AND(SELECT COUNT(*) FROM OpportunityItems WHERE numOppId = OpportunityMaster.numOppId AND coalesce(numUnitHourReceived,0) > 0) = 0
      AND(SELECT COUNT(*) FROM OpportunityBizDocs WHERE numoppid = OpportunityMaster.numOppId AND coalesce(bitAuthoritativeBizDocs,0) = 1) = 0
      GROUP BY
      OpportunityMaster.numDivisionId,CompanyInfo.vcCompanyName
      HAVING
      COUNT(OpportunityMaster.numOppId) > 1;
	--GET PO OF VENDOR
   ELSEIF v_numLevel = 1 AND v_numVendorID > 0
   then
	
      open SWV_RefCur for
      SELECT
      v_numVendorID AS VendorID,
			OpportunityMaster.numOppId AS POID,
			OpportunityMaster.vcpOppName AS POName,
			coalesce(Listdetails.vcData,'') AS POStatus,
			FormatedDateTimeFromDate(OpportunityMaster.bintCreatedDate,v_numDomainID) AS CreatedDate,
			FormatedDateTimeFromDate(OpportunityMaster.intPEstimatedCloseDate,v_numDomainID) AS EstimatedCloseDate
      FROM
      OpportunityMaster
      LEFT JOIN
      Listdetails
      ON
      numListID = 176 AND
      numListItemID = OpportunityMaster.numStatus
      WHERE
      tintopptype = 2
      AND tintoppstatus = 1
      AND coalesce(bitStockTransfer,false) = false
      AND coalesce(tintshipped,0) = 0
      AND OpportunityMaster.numDomainId = v_numDomainID
      AND OpportunityMaster.numDivisionId = v_numVendorID
      AND coalesce(bitStopMerge,false) = false
      AND(SELECT COUNT(*) FROM OpportunityItems WHERE numOppId = OpportunityMaster.numOppId) > 0
      AND(SELECT COUNT(*) FROM OpportunityItems WHERE numOppId = OpportunityMaster.numOppId AND coalesce(numUnitHourReceived,0) > 0) = 0
      AND(SELECT COUNT(*) FROM OpportunityBizDocs WHERE numoppid = OpportunityMaster.numOppId AND coalesce(bitAuthoritativeBizDocs,0) = 1) = 0;
   ELSEIF v_numLevel = 2 AND v_numPOID > 0
   then
	
      open SWV_RefCur for
      SELECT
      v_numPOID AS POID,
			OpportunityItems.numItemCode,
			coalesce(OpportunityItems.vcItemName,'') AS vcItemName,
			coalesce(OpportunityItems.vcModelID,'') AS vcModelID,
			coalesce(OpportunityItems.numUnitHour,0) AS numUnitHour,
			coalesce(Vendor.intMinQty,0) AS intMinQty
      FROM
      OpportunityMaster
      INNER JOIN
      OpportunityItems
      ON
      OpportunityMaster.numOppId = OpportunityItems.numOppId
      LEFT JOIN
      Vendor
      ON
      OpportunityMaster.numDivisionId = Vendor.numVendorID AND
      OpportunityItems.numItemCode = Vendor.numItemCode
      WHERE
      OpportunityMaster.numDomainId = v_numDomainID
      AND OpportunityMaster.numOppId = v_numPOID;
   end if;
   RETURN;
END; $$;


