-- Stored procedure definition script Usp_getFieldName for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION Usp_getFieldName(v_strGrpfld VARCHAR(200),  
v_GroupId NUMERIC, INOUT SWV_RefCur refcursor)
LANGUAGE plpgsql
   AS $$
BEGIN
   if v_strGrpfld not ilike 'CFLD%' then

      open SWV_RefCur for
      select * from CustRptDefaultFields where numFieldGroupId = v_GroupId and vcDbFieldName = v_strGrpfld;
   else
      open SWV_RefCur for
      select FLd_label as vcscrFieldName  from CFW_Fld_Master where Fld_id in(select case when items = 'cfl' then 0 else items end from  Split(v_strGrpfld,'d'));
   end if;
   RETURN;
END; $$;


