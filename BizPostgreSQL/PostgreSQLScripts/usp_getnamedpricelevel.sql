DROP FUNCTION IF EXISTS USP_GetNamedPriceLevel;
CREATE OR REPLACE FUNCTION USP_GetNamedPriceLevel(v_numDomainID NUMERIC(18,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
	open SWV_RefCur for 
	SELECT
		CAST(pnt.tintPriceLevel AS NUMERIC) AS "Id"
		,coalesce(NULLIF(pnt.vcPriceLevelName,''),CONCAT('Price Level ',pnt.tintPriceLevel)) AS "Value"
	FROM
		PricingNamesTable pnt
	WHERE
		pnt.numDomainID = v_numDomainID
	ORDER BY
		pnt.tintPriceLevel;
END; $$;













