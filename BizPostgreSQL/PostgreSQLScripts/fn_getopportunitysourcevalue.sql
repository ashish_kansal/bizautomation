-- Function definition script fn_GetOpportunitySourceValue for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION fn_GetOpportunitySourceValue(v_tintSource NUMERIC(18,0),v_tintSourceType SMALLINT,v_numDomainID NUMERIC(18,0))
RETURNS VARCHAR(100) LANGUAGE plpgsql
   AS $$
   DECLARE
   v_listName  VARCHAR(200);
BEGIN
   IF v_tintSourceType = 1 then --List Master

      IF v_tintSource = 0 then
	
         v_listName := 'Internal Order';
      ELSE
         select   coalesce(vcData,'') INTO v_listName from Listdetails WHERE numListItemID = v_tintSource;
      end if;
   ELSEIF v_tintSourceType = 2
   then --WebSite

      select   coalesce(vcSiteName,'') INTO v_listName FROM   Sites WHERE  numDomainID = v_numDomainID AND numSiteID = v_tintSource;
   ELSEIF v_tintSourceType = 3
   then --WebAPI

      select   coalesce(vcProviderName,'') INTO v_listName FROM WebAPI WHERE WebApiId = v_tintSource;
   ELSEIF v_tintSourceType = 4
   then --Marketplaces (EchannelHub)

      select   coalesce(vcMarketplace,'') INTO v_listName FROM eChannelHub WHERE ID = v_tintSource;
   end if;
	
   RETURN v_listName;
END; $$;

