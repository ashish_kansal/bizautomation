-- Stored procedure definition script USP_LinkedProjects for PostgreSQL
CREATE OR REPLACE FUNCTION USP_LinkedProjects(v_ByteMode INTEGER DEFAULT 0  ,
v_numOppID NUMERIC(9,0) DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   if v_ByteMode = 0 then ---For projects    
 
      open SWV_RefCur for select cast(pm.numProId as VARCHAR(255)),cast(vcProjectName as VARCHAR(255)),CAST('Project' AS CHAR(7)) as Type from  ProjectsMaster pm
      join ProjectsOpportunities Po on pm.numProId = Po.numProId
      where Po.numOppId = v_numOppID
      union all
      select cast(c.numCaseId as VARCHAR(255)),cast(vcCaseNumber as VARCHAR(255)),CAST('Case' AS CHAR(4)) as Type from  Cases c
      join CaseOpportunities co on c.numCaseId = co.numCaseId
      where co.numoppid = v_numOppID;
   end if;
END; $$;












