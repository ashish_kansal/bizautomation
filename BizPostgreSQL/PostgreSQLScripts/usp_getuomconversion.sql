-- Stored procedure definition script USP_GetUOMConversion for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetUOMConversion(v_numDomainID NUMERIC(9,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT cast(c.numUOMConv as VARCHAR(255)),cast(c.numUOM1 as VARCHAR(255)),cast(c.decConv1 as VARCHAR(255)),cast(c.numUOM2 as VARCHAR(255)),cast(c.decConv2 as VARCHAR(255))
   FROM UOMConversion c INNER JOIN UOM u ON c.numUOM1 = u.numUOMId INNER JOIN Domain d ON u.numDomainID = d.numDomainId
   WHERE c.numDomainID = v_numDomainID AND u.numDomainID = v_numDomainID AND d.numDomainId = v_numDomainID AND
   u.tintUnitType =(CASE WHEN coalesce(d.charUnitSystem,'E') = 'E' THEN 1 WHEN d.charUnitSystem = 'M' THEN 2 END);
END; $$;
         












