-- Function definition script swf_dyn_curs_row_count for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION swf_dyn_curs_row_count(SWV_RefCur refcursor)
RETURNS integer
LANGUAGE plpgsql
   AS $$
   DECLARE SWV_RowCount integer;
BEGIN
   MOVE FORWARD ALL IN SWV_RefCur;
   GET DIAGNOSTICS SWV_RowCount = ROW_COUNT;
   MOVE BACKWARD ALL IN SWV_RefCur;
   RETURN SWV_RowCount;
END; $$;

