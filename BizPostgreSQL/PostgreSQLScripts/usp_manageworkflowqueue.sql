DROP FUNCTION IF EXISTS USP_ManageWorkFlowQueue;

CREATE OR REPLACE FUNCTION USP_ManageWorkFlowQueue(v_numWFQueueID NUMERIC(18,0) DEFAULT 0,
    v_numDomainID NUMERIC(18,0) DEFAULT 0,
    v_numUserCntID NUMERIC(18,0) DEFAULT 0,
    v_numRecordID NUMERIC(18,0) DEFAULT 0,
    v_numFormID NUMERIC(18,0) DEFAULT 0,
    v_tintProcessStatus SMALLINT DEFAULT 1,
    v_tintWFTriggerOn SMALLINT DEFAULT NULL,
    v_tintMode SMALLINT DEFAULT NULL,
    v_vcColumnsUpdated VARCHAR(1000) DEFAULT '',
    v_numWFID NUMERIC(18,0) DEFAULT 0,
    v_bitSuccess BOOLEAN DEFAULT false,
    v_vcDescription Text DEFAULT '')
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   IF v_tintMode = 1 then
	
      IF v_numWFQueueID > 0 then
		   
         UPDATE WorkFlowQueue SET tintProcessStatus = v_tintProcessStatus WHERE  numWFQueueID = v_numWFQueueID AND numDomainID = v_numDomainID;
      ELSE
         IF EXISTS(SELECT 1 FROM WorkFlowMaster WHERE numDomainID = v_numDomainID AND numFormID = v_numFormID AND (tintWFTriggerOn = v_tintWFTriggerOn OR (v_tintWFTriggerOn IN(1,2) AND tintWFTriggerOn = 3)) AND bitActive = true) then
			 
            INSERT INTO WorkFlowQueue(numDomainID, numCreatedBy, dtCreatedDate, numRecordID, numFormID, tintProcessStatus, tintWFTriggerOn, numWFID)
            SELECT v_numDomainID, v_numUserCntID, TIMEZONE('UTC',now()), v_numRecordID, v_numFormID, v_tintProcessStatus, tintWFTriggerOn, numWFID
            FROM WorkFlowMaster WHERE numDomainID = v_numDomainID AND numFormID = v_numFormID AND (tintWFTriggerOn = v_tintWFTriggerOn OR (v_tintWFTriggerOn IN(1,2) AND tintWFTriggerOn = 3)) AND bitActive = true
            AND numWFID NOT IN(SELECT numWFID FROM WorkFlowQueue WHERE numDomainID = v_numDomainID AND numRecordID = v_numRecordID AND numFormID = v_numFormID AND (tintWFTriggerOn = v_tintWFTriggerOn OR (v_tintWFTriggerOn IN(1,2) AND tintWFTriggerOn = 3)) AND tintProcessStatus IN(1,2));
            IF v_tintWFTriggerOn = 2 AND LENGTH(v_vcColumnsUpdated) > 0 then
					
               v_tintWFTriggerOn := 4;
               INSERT INTO WorkFlowQueue(numDomainID, numCreatedBy, dtCreatedDate, numRecordID, numFormID, tintProcessStatus, tintWFTriggerOn, numWFID)
               SELECT DISTINCT v_numDomainID, v_numUserCntID, TIMEZONE('UTC',now()), v_numRecordID, v_numFormID, v_tintProcessStatus, v_tintWFTriggerOn, WF.numWFID
               FROM WorkFlowMaster WF JOIN WorkFlowConditionList WFCL ON WF.numWFID = WFCL.numWFID
               JOIN DycFieldMaster DFM ON WFCL.numFieldId = DFM.numFieldID
               WHERE WF.numDomainID = v_numDomainID AND WF.numFormID = v_numFormID AND WF.tintWFTriggerOn = 4 AND WF.bitActive = true AND WFCL.bitCustom = false
               AND DFM.vcOrigDbCOlumnName IN(SELECT OutParam FROM SplitString(v_vcColumnsUpdated,','))
               AND WF.numWFID NOT IN(SELECT numWFID FROM WorkFlowQueue WHERE numDomainID = v_numDomainID AND numRecordID = v_numRecordID AND numFormID = v_numFormID AND tintWFTriggerOn = 4 AND tintProcessStatus IN(1,2));
            end if;
         end if;
			 --Added By:Sachin Sadhu||Date:10thFeb2014
			 --Description:@tintWFTriggerOn value is  coming from Trigger,I=1,U=2,D=3
			 --case of Field(s) Update
         IF EXISTS(SELECT 1 FROM WorkFlowMaster WHERE numDomainID = v_numDomainID AND numFormID = v_numFormID AND (tintWFTriggerOn = v_tintWFTriggerOn OR (v_tintWFTriggerOn IN(1,2) AND tintWFTriggerOn = 4)) AND bitActive = true) then
			 
            INSERT INTO WorkFlowQueue(numDomainID, numCreatedBy, dtCreatedDate, numRecordID, numFormID, tintProcessStatus, tintWFTriggerOn, numWFID)
            SELECT v_numDomainID, v_numUserCntID, TIMEZONE('UTC',now()), v_numRecordID, v_numFormID, v_tintProcessStatus, tintWFTriggerOn, numWFID
            FROM WorkFlowMaster WHERE numDomainID = v_numDomainID AND numFormID = v_numFormID AND (tintWFTriggerOn = v_tintWFTriggerOn OR (v_tintWFTriggerOn IN(1,2) AND tintWFTriggerOn = 3)) AND bitActive = true
            AND numWFID NOT IN(SELECT numWFID FROM WorkFlowQueue WHERE numDomainID = v_numDomainID AND numRecordID = v_numRecordID AND numFormID = v_numFormID AND (tintWFTriggerOn = v_tintWFTriggerOn OR (v_tintWFTriggerOn IN(1,2) AND tintWFTriggerOn = 3)) AND tintProcessStatus IN(1,2));
            IF v_tintWFTriggerOn = 2 AND LENGTH(v_vcColumnsUpdated) > 0 then
					
               v_tintWFTriggerOn := 4;
               INSERT INTO WorkFlowQueue(numDomainID, numCreatedBy, dtCreatedDate, numRecordID, numFormID, tintProcessStatus, tintWFTriggerOn, numWFID)
               SELECT DISTINCT v_numDomainID, v_numUserCntID, TIMEZONE('UTC',now()), v_numRecordID, v_numFormID, v_tintProcessStatus, v_tintWFTriggerOn, WF.numWFID
               FROM WorkFlowMaster WF JOIN WorkFlowConditionList WFCL ON WF.numWFID = WFCL.numWFID
               JOIN DycFieldMaster DFM ON WFCL.numFieldId = DFM.numFieldID
               WHERE WF.numDomainID = v_numDomainID AND WF.numFormID = v_numFormID AND WF.tintWFTriggerOn = 4 AND WF.bitActive = true AND WFCL.bitCustom = false
               AND DFM.vcOrigDbCOlumnName IN(SELECT OutParam FROM SplitString(v_vcColumnsUpdated,','))
               AND WF.numWFID NOT IN(SELECT numWFID FROM WorkFlowQueue WHERE numDomainID = v_numDomainID AND numRecordID = v_numRecordID AND numFormID = v_numFormID AND tintWFTriggerOn = 4 AND tintProcessStatus IN(1,2));
            end if;
         end if;
      end if;
   ELSEIF v_tintMode = 2
   then
        
      INSERT INTO WorkFlowQueueExecution(numWFQueueID, numWFID, dtExecutionDate, bitSuccess, vcDescription)
      SELECT v_numWFQueueID, v_numWFID, TIMEZONE('UTC',now()), v_bitSuccess, v_vcDescription;
      IF coalesce(v_bitSuccess,false) = false then
			
         UPDATE WorkFlowQueue SET tintProcessStatus = 6 WHERE numWFQueueID = v_numWFQueueID;
      end if;
   end if;
   RETURN;
END; $$;





