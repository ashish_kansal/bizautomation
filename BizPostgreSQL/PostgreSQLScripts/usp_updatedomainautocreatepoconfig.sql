-- Stored procedure definition script USP_UpdatedomainAutoCreatePOConfig for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_UpdatedomainAutoCreatePOConfig(v_numDomainID NUMERIC(9,0) DEFAULT 0,                                      
v_bitReOrderPoint BOOLEAN DEFAULT false,
v_numReOrderPointOrderStatus NUMERIC(18,0) DEFAULT 0,
v_tintOppStautsForAutoPOBackOrder SMALLINT DEFAULT 0,
v_tintUnitsRecommendationForAutoPOBackOrder SMALLINT DEFAULT 1,
v_bitIncludeRequisitions BOOLEAN DEFAULT false)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   UPDATE
   Domain
   SET
   bitReOrderPoint = v_bitReOrderPoint,numReOrderPointOrderStatus = v_numReOrderPointOrderStatus,
   tintOppStautsForAutoPOBackOrder = v_tintOppStautsForAutoPOBackOrder,
   tintUnitsRecommendationForAutoPOBackOrder = v_tintUnitsRecommendationForAutoPOBackOrder,bitIncludeRequisitions = v_bitIncludeRequisitions
   WHERE
   numDomainId = v_numDomainID;
   RETURN;
END; $$;


