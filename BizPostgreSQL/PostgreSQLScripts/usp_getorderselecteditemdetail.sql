DROP FUNCTION IF EXISTS USP_GetOrderSelectedItemDetail;

CREATE OR REPLACE FUNCTION USP_GetOrderSelectedItemDetail(v_numDomainID NUMERIC(18,0),
	v_numDivisionID NUMERIC(18,0),
	v_tintOppType SMALLINT,
	v_numItemCode NUMERIC(18,0),
	v_numUOMID NUMERIC(18,0),
	v_numWarehouseItemID NUMERIC(18,0),
	v_numQty DOUBLE PRECISION,
	v_monPrice DOUBLE PRECISION,
	v_numOppID NUMERIC(18,0),
	v_numOppItemID NUMERIC(18,0),
	v_vcSelectedKitChildItems TEXT,
	v_numWarehouseID NUMERIC(18,0),
	v_vcCoupon VARCHAR(200),
	v_numCurrencyID NUMERIC(18,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_vcItemName  VARCHAR(500);
   v_bitDropship  BOOLEAN;
   v_bitMatrix  BOOLEAN;
   v_chrItemType  CHAR(1);
   v_numItemGroup  NUMERIC(18,0);
   v_numBaseUnit  NUMERIC(18,0);
   v_numSaleUnit  NUMERIC(18,0);
   v_fltUOMConversionFactor  DOUBLE PRECISION;
   v_numPurchaseUnit  NUMERIC(18,0);
   v_bitAssemblyOrKit  BOOLEAN;
   v_bitAssembly  BOOLEAN;
   v_bitKit  BOOLEAN;
   v_bitHasChildKits  BOOLEAN;
   v_bitSOWorkOrder  BOOLEAN;
   v_bitCalAmtBasedonDepItems  BOOLEAN;
   v_ItemDesc  VARCHAR(1000);
   v_numItemClassification  NUMERIC(18,0);

   v_monListPrice  DECIMAL(20,5) DEFAULT 0.0000;
   v_monVendorCost  DECIMAL(20,5) DEFAULT 0.0000;
   v_tintPricingBasedOn  SMALLINT DEFAULT 0.0000;

   v_tintRuleType  SMALLINT;
   v_monPriceLevelPrice  DECIMAL(20,5) DEFAULT 0.0000;
   v_monPriceFinalPrice  DECIMAL(20,5) DEFAULT 0.0000;
   v_monPriceRulePrice  DECIMAL(20,5) DEFAULT 0.0000;
   v_monPriceRuleFinalPrice  DECIMAL(20,5) DEFAULT 0.0000;
   v_monLastPrice  DECIMAL(20,5) DEFAULT 0.0000;
   v_dtLastOrderDate  VARCHAR(200) DEFAULT '';

   v_tintDisountType  SMALLINT;
   v_decDiscount  DOUBLE PRECISION;
   v_decDiscountPercentPriceBook DOUBLE PRECISION;

   v_numVendorID  NUMERIC(18,0);
   v_vcMinOrderQty  VARCHAR(5) DEFAULT '-';
   v_vcVendorNotes  VARCHAR(300) DEFAULT '';
   v_numRelationship  NUMERIC(18,0);
   v_numProfile  NUMERIC(18,0);
   v_tintPriceLevel  SMALLINT;
   v_tintDefaultSalesPricing  SMALLINT;
   v_tintPriceBookDiscount  SMALLINT;
   v_tintKitAssemblyPriceBasedOn  SMALLINT;
   v_numBaseCurrency  NUMERIC(18,0);
   v_fltExchangeRate  DOUBLE PRECISION;
   v_bitMarkupDiscount Boolean = false;
   v_numTempWarehouseID  NUMERIC(18,0);
   v_LastDiscountType  BOOLEAN;
   v_LastDiscount  DOUBLE PRECISION;

   v_numPriceRuleID  NUMERIC(18,0);
   v_tintPriceRuleType  SMALLINT;
   v_tintVendorCostType SMALLINT;
   v_tintPricingMethod  SMALLINT;
   v_tintPriceBookDiscountType  SMALLINT;
   v_decPriceBookDiscount  DOUBLE PRECISION;
   v_intQntyItems  INTEGER;
   v_decMaxDedPerAmt  DOUBLE PRECISION;
   v_dtDynamicCostModifiedDate TIMESTAMP;
   v_bitRoundTo BOOLEAN;
   v_tintRoundTo SMALLINT;
	v_monVendorDynamicCost  DECIMAL(20,5);
	v_monVendorStaticCost  DECIMAL(20,5);
BEGIN
   IF coalesce(v_numDivisionID,0) > 0 then
	
      select   coalesce(SUBSTR(CAST(intMinQty AS VARCHAR(30)),1,30),'-'), coalesce(vcNotes,'') INTO v_vcMinOrderQty,v_vcVendorNotes FROM Vendor WHERE numVendorID = v_numDivisionID AND numItemCode = v_numItemCode;
   end if;

   select   numDefaultSalesPricing, coalesce(tintPriceBookDiscount,0), coalesce(numCurrencyID,0),(CASE WHEN COALESCE(tintMarkupDiscountOption,0) = 1 THEN true ELSE false END) INTO v_tintDefaultSalesPricing,v_tintPriceBookDiscount,v_numBaseCurrency,v_bitMarkupDiscount FROM
   Domain WHERE
   numDomainId = v_numDomainID;

   IF v_numBaseCurrency = v_numCurrencyID then
	
      v_fltExchangeRate := 1;
   ELSE
      v_fltExchangeRate := coalesce((SELECT fltExchangeRate FROM Currency WHERE numDomainId = v_numDomainID AND numCurrencyID = v_numCurrencyID),1);
      IF coalesce(v_fltExchangeRate,0) = 0 then
		
         v_fltExchangeRate := 1;
      end if;
   end if;

   IF coalesce(v_numWarehouseID,0) > 0 then
	
      select   numWareHouseItemID INTO v_numWarehouseItemID FROM WareHouseItems WHERE numItemID = v_numItemCode AND numWareHouseID = v_numWarehouseID   ORDER BY numWareHouseItemID ASC LIMIT 1;
   end if;

	-- GET ORGANIZATION DETAL
   select   numCompanyType, vcProfile, coalesce(D.tintPriceLevel,0) INTO v_numRelationship,v_numProfile,v_tintPriceLevel FROM
   DivisionMaster D
   JOIN
   CompanyInfo C
   ON
   C.numCompanyId = D.numCompanyID WHERE
   numDivisionID = v_numDivisionID;       

	-- GET ITEM DETAILS
	select 
		vcItemName
		,bitAllowDropShip
		,Item.txtItemDesc
		,coalesce(Item.bitMatrix,false)
		,charItemType
		,numItemGroup
		,coalesce(bitAssembly,false)
		,coalesce(bitSOWorkOrder,false)
		,coalesce(numBaseUnit,0)
		,(CASE WHEN coalesce(v_numUOMID,0) > 0 THEN v_numUOMID ELSE coalesce(numSaleUnit,0) END)
		,(CASE WHEN coalesce(v_numUOMID,0) > 0 THEN v_numUOMID ELSE coalesce(numPurchaseUnit,0) END)
		,numItemClassification
		,(CASE WHEN coalesce(bitKitParent,false) = true OR coalesce(bitAssembly,false) = true THEN 1 ELSE 0 END)
		,(CASE
			WHEN EXISTS(SELECT ID FROM ItemCurrencyPrice WHERE numDomainID = v_numDomainID AND numItemCode = v_numItemCode AND numCurrencyID = coalesce(v_numCurrencyID,0))
			THEN coalesce((SELECT monListPrice FROM ItemCurrencyPrice WHERE numDomainID = v_numDomainID AND numItemCode = v_numItemCode AND numCurrencyID = coalesce(v_numCurrencyID,0)),0)
			ELSE (CASE WHEN v_fltExchangeRate <> 1 THEN CAST((coalesce(Item.monListPrice,0)/v_fltExchangeRate) AS INTEGER) ELSE coalesce(Item.monListPrice,0) END)
		END)
		,(CASE 
			WHEN COALESCE(bitKitParent,false) = true
			THEN (CASE WHEN v_fltExchangeRate <> 1 THEN CAST((coalesce(fn_GetKitVendorCost(v_numDomainID,Item.numItemCode,v_vcSelectedKitChildItems),0) / v_fltExchangeRate) AS INT) ELSE COALESCE(fn_GetKitVendorCost(v_numDomainID,Item.numItemCode,v_vcSelectedKitChildItems),0) END) * fn_UOMConversion(Item.numBaseUnit,Item.numItemCode,Item.numDomainID,Item.numPurchaseUnit)
			ELSE coalesce((SELECT(CASE WHEN v_fltExchangeRate <> 1 THEN CAST((coalesce(monCost,0)/v_fltExchangeRate) AS INTEGER) ELSE coalesce(monCost,0) END)*fn_UOMConversion(Item.numBaseUnit,Item.numItemCode,Item.numDomainID,Item.numPurchaseUnit) FROM Vendor WHERE numVendorID = Item.numVendorID AND Vendor.numItemCode = Item.numItemCode),0)
		END)
		,coalesce(Item.numVendorID,0), (CASE WHEN coalesce(v_numWarehouseItemID,0) > 0 THEN v_numWarehouseItemID ELSE WareHouseItems.numWareHouseItemID END), coalesce(bitCalAmtBasedonDepItems,false), coalesce(tintKitAssemblyPriceBasedOn,1), coalesce(bitKitParent,false), (CASE
   WHEN coalesce(bitKitParent,false) = true
   THEN(CASE
      WHEN EXISTS(SELECT ID.numItemDetailID FROM ItemDetails ID INNER JOIN Item IInner ON ID.numChildItemID = IInner.numItemCode WHERE ID.numItemKitID = Item.numItemCode AND coalesce(IInner.bitKitParent,false) = true)
      THEN 1
      ELSE 0
      END)
   ELSE 0
   END) INTO v_vcItemName,v_bitDropship,v_ItemDesc,v_bitMatrix,v_chrItemType,v_numItemGroup,
   v_bitAssembly,v_bitSOWorkOrder,v_numBaseUnit,v_numSaleUnit,v_numPurchaseUnit,
   v_numItemClassification,v_bitAssemblyOrKit,v_monListPrice,
   v_monVendorCost,v_numVendorID,v_numWarehouseItemID,v_bitCalAmtBasedonDepItems,
   v_tintKitAssemblyPriceBasedOn,v_bitKit,v_bitHasChildKits FROM
   Item
   LEFT JOIN LATERAL(SELECT  numWareHouseItemID,monWListPrice FROM WareHouseItems WHERE numItemID = v_numItemCode AND (numWareHouseItemID = v_numWarehouseItemID OR coalesce(v_numWarehouseItemID,0) = 0) LIMIT 1) AS WareHouseItems on TRUE WHERE
   Item.numItemCode = v_numItemCode;

   IF coalesce(v_numDivisionID,0) > 0 AND EXISTS(SELECT * FROM Vendor WHERE numItemCode = v_numItemCode AND numVendorID = v_numDivisionID) then
	
      select(CASE WHEN v_fltExchangeRate <> 1 THEN CAST((coalesce(monCost,0)/v_fltExchangeRate) AS INTEGER) ELSE coalesce(monCost,0) END) INTO v_monVendorCost FROM Vendor WHERE numVendorID = v_numDivisionID AND numItemCode = v_numItemCode;
   end if;

	If EXISTS (SELECT 
					VendorCostTable.numvendorcosttableid
				FROM 
					Vendor 
				INNER JOIN 
					VendorCostTable 
				ON 
					Vendor.numvendortcode=VendorCostTable.numvendortcode 
				WHERE
					Vendor.numDomainID=v_numDomainID
					AND Vendor.numVendorID=v_numDivisionID 
					AND Vendor.numItemCode=v_numItemCode 
					AND VendorCostTable.numCurrencyID = v_numCurrencyID
					AND COALESCE(v_numQty,0) BETWEEN VendorCostTable.intFromQty AND VendorCostTable.intToQty
					AND VendorCostTable.monDynamicCost IS NOT NULL) then
			SELECT 
				VendorCostTable.monDynamicCost
				,VendorCostTable.monStaticCost
			INTO 
				v_monVendorDynamicCost
				,v_monVendorStaticCost
			FROM 
				Vendor 
			INNER JOIN 
				VendorCostTable 
			ON 
				Vendor.numvendortcode=VendorCostTable.numvendortcode 
			WHERE
				Vendor.numDomainID=v_numDomainID
				AND Vendor.numVendorID=v_numDivisionID 
				AND Vendor.numItemCode=v_numItemCode 
				AND VendorCostTable.numCurrencyID = v_numCurrencyID
				AND COALESCE(v_numQty,0) BETWEEN VendorCostTable.intFromQty AND VendorCostTable.intToQty;
	ELSE
		v_monVendorDynamicCost := v_monVendorCost;
		v_monVendorStaticCost := v_monVendorCost;
    END IF;


   IF v_chrItemType = 'P' AND coalesce(v_numWarehouseItemID,0) = 0 then
	
      RAISE EXCEPTION 'WAREHOUSE_DOES_NOT_EXISTS';
   end if;

   v_fltUOMConversionFactor := fn_UOMConversion((CASE WHEN  v_tintOppType = 1 THEN v_numSaleUnit ELSE v_numPurchaseUnit END),v_numItemCode,v_numDomainID,v_numBaseUnit);

   IF v_tintOppType = 1 then
	
		/*Use Calculated Price When item is Assembly/Kit,Calculate price based on sum of dependent items.*/
      IF v_bitCalAmtBasedonDepItems = true then
         DROP TABLE IF EXISTS tt_TEMPPRICE CASCADE;
         CREATE TEMPORARY TABLE tt_TEMPPRICE
         (
            bitSuccess BOOLEAN,
            monPrice DECIMAL(20,5)
         );
         INSERT INTO tt_TEMPPRICE(bitSuccess
				,monPrice)
         SELECT
         bitSuccess
				,monMSRPPrice
         FROM
         fn_GetKitAssemblyCalculatedPrice(v_numDomainID,v_numDivisionID,v_numItemCode,v_numQty,v_numWarehouseItemID,
         v_tintKitAssemblyPriceBasedOn,v_numOppID,v_numOppItemID,v_vcSelectedKitChildItems,
         v_numCurrencyID,v_fltExchangeRate);
         IF(SELECT bitSuccess FROM tt_TEMPPRICE) = true then
			
            SELECT monPrice INTO v_monListPrice FROM tt_TEMPPRICE;
         ELSE
            RAISE EXCEPTION 'SELECTED_WAREHOUSE_IS_NOT_AVAILABLE_FOR_ALL_CHILD_ITEMS';
         end if;
      end if;
      IF v_tintDefaultSalesPricing = 1 AND(SELECT
      COUNT(*)
      FROM
      PricingTable
      WHERE
      numItemCode = v_numItemCode
      AND coalesce(numPriceRuleID,0) = 0
      AND 1 =(CASE
      WHEN EXISTS(SELECT * FROM PricingTable WHERE numItemCode = v_numItemCode AND coalesce(numPriceRuleID,0) = 0 AND numCurrencyID = v_numCurrencyID)
      THEN(CASE WHEN coalesce(numCurrencyID,0) = v_numCurrencyID THEN 1 ELSE 0 END)
      ELSE(CASE WHEN coalesce(numCurrencyID,0) = 0 THEN 1 ELSE 0 END)
      END)) > 0 then -- PRICE LEVEL
		
         select   
			tintVendorCostType
			,tintDiscountType
			,coalesce(decDiscount,0)
			,tintRuleType
			,(CASE tintRuleType 
					WHEN 1 
					THEN v_monListPrice 
					WHEN 2 THEN (CASE 
									WHEN tintVendorCostType=1 THEN v_monVendorDynamicCost 
									WHEN tintVendorCostType=2 THEN v_monVendorStaticCost 
									ELSE v_monVendorCost 
								END) 
					ELSE coalesce(decDiscount,0) 
			END)
			,(CASE tintRuleType
         WHEN 1 -- Deduct From List Price
         THEN(CASE tintDiscountType
            WHEN 1 -- PERCENT
            THEN(CASE WHEN v_monListPrice > 0 THEN v_monListPrice -(v_monListPrice*(coalesce(decDiscount,0)/100)) ELSE 0 END)
            WHEN 2 -- FLAT AMOUNT
            THEN(CASE WHEN v_monListPrice > 0 THEN(CASE WHEN(v_monListPrice*v_numQty::bigint*v_fltUOMConversionFactor) -(CASE WHEN v_numCurrencyID <> numCurrencyID THEN CAST((coalesce(decDiscount,0)/v_fltExchangeRate) AS INTEGER) ELSE coalesce(decDiscount,0) END) > 0 THEN((v_monListPrice*v_numQty::bigint*v_fltUOMConversionFactor) -(CASE WHEN v_numCurrencyID <> numCurrencyID THEN CAST((coalesce(decDiscount,0)/v_fltExchangeRate) AS INTEGER) ELSE coalesce(decDiscount,0) END))/(v_numQty::bigint*v_fltUOMConversionFactor) ELSE 0 END) ELSE 0 END)
            WHEN 3 -- NAMED PRICE
            THEN(CASE WHEN v_numCurrencyID <> numCurrencyID THEN CAST((coalesce(decDiscount,0)/v_fltExchangeRate) AS INTEGER) ELSE coalesce(decDiscount,0) END)
            END)
         WHEN 2 -- Add to primary vendor cost
         THEN(CASE tintDiscountType
            WHEN 1  -- PERCENT
            THEN (CASE 
					WHEN tintVendorCostType=1 THEN v_monVendorDynamicCost 
					WHEN tintVendorCostType=2 THEN v_monVendorStaticCost 
					ELSE v_monVendorCost 
				END)+((CASE 
					WHEN tintVendorCostType=1 THEN v_monVendorDynamicCost 
					WHEN tintVendorCostType=2 THEN v_monVendorStaticCost 
					ELSE v_monVendorCost 
				END)*(coalesce(decDiscount,0)/100))*v_fltUOMConversionFactor
            WHEN 2 -- FLAT AMOUNT
            THEN(CASE WHEN((CASE 
								WHEN tintVendorCostType=1 THEN v_monVendorDynamicCost 
								WHEN tintVendorCostType=2 THEN v_monVendorStaticCost 
								ELSE v_monVendorCost 
							END)*v_numQty::bigint*v_fltUOMConversionFactor)+(CASE WHEN v_numCurrencyID <> numCurrencyID THEN CAST((coalesce(decDiscount,0)/v_fltExchangeRate) AS INTEGER) ELSE coalesce(decDiscount,0) END) > 0 THEN((v_monVendorCost*v_numQty::bigint*v_fltUOMConversionFactor)+(CASE WHEN v_numCurrencyID <> numCurrencyID THEN CAST((coalesce(decDiscount,0)/v_fltExchangeRate) AS INTEGER) ELSE coalesce(decDiscount,0) END))/(v_numQty::bigint*v_fltUOMConversionFactor)  ELSE 0 END)
            WHEN 3 -- NAMED PRICE
            THEN(CASE WHEN v_numCurrencyID <> numCurrencyID THEN CAST((coalesce(decDiscount,0)/v_fltExchangeRate) AS INTEGER) ELSE coalesce(decDiscount,0) END)
            END)
         WHEN 3 -- Named price
         THEN(CASE WHEN v_numCurrencyID <> numCurrencyID THEN CAST((coalesce(decDiscount,0)/v_fltExchangeRate) AS INTEGER) ELSE coalesce(decDiscount,0) END)
         END) INTO v_tintVendorCostType,v_tintDisountType,v_decDiscount,v_tintRuleType,v_monPriceLevelPrice,v_monPriceFinalPrice FROM(SELECT
            ROW_NUMBER() OVER(PARTITION BY PricingTable.numItemCode ORDER BY numPricingID) AS Id,
					* FROM
            PricingTable
            WHERE
            PricingTable.numItemCode = v_numItemCode
            AND coalesce(PricingTable.numPriceRuleID,0) = 0
            AND 1 =(CASE
            WHEN EXISTS(SELECT * FROM PricingTable WHERE numItemCode = v_numItemCode AND coalesce(numPriceRuleID,0) = 0 AND numCurrencyID = v_numCurrencyID)
            THEN(CASE WHEN coalesce(numCurrencyID,0) = v_numCurrencyID THEN 1 ELSE 0 END)
            ELSE(CASE WHEN coalesce(numCurrencyID,0) = 0 THEN 1 ELSE 0 END)
            END)) TEMP WHERE
         numItemCode = v_numItemCode
         AND coalesce(numPriceRuleID,0) = 0
         AND 1 =(CASE
         WHEN coalesce(v_tintPriceLevel,0) > 0
         THEN(CASE WHEN Id = v_tintPriceLevel THEN 1 ELSE 0 END)
         ELSE(CASE WHEN v_numQty::bigint*v_fltUOMConversionFactor >= intFromQty AND v_numQty::bigint*v_fltUOMConversionFactor <= intToQty THEN 1 ELSE 0 END)
         END);
      ELSEIF v_tintDefaultSalesPricing = 2 AND(SELECT COUNT(*) FROM PriceBookRules WHERE numDomainID = v_numDomainID AND tintRuleFor = v_tintOppType) > 0
      then -- PRICE RULE
			SELECT  
				numPricRuleID, tintPricingMethod, p.tintRuleType,p.tintVendorCostType, p.tintDiscountType, p.decDiscount, p.intQntyItems,p.decMaxDedPerAmt,P.bitRoundTo,p.tintRoundTo
			INTO 
				v_numPriceRuleID,v_tintPricingMethod,v_tintPriceRuleType,v_tintVendorCostType,v_tintPriceBookDiscountType,v_decPriceBookDiscount,v_intQntyItems,v_decMaxDedPerAmt,v_bitRoundTo,v_tintRoundTo 
			FROM
				PriceBookRules p
			LEFT JOIN
				PriceBookRuleDTL PDTL
			ON
				p.numPricRuleID = PDTL.numRuleID
			LEFT JOIN
				PriceBookRuleItems PBI
			ON
				p.numPricRuleID = PBI.numRuleID
			LEFT JOIN
				PriceBookPriorities PP
			ON
				PP.Step2Value = p.tintStep2
				AND PP.Step3Value = p.tintStep3 
			WHERE
				p.numDomainID = v_numDomainID
				AND tintRuleFor = v_tintOppType
				AND (((tintStep2 = 1 AND PBI.numValue = v_numItemCode) AND (tintStep3 = 1 AND PDTL.numValue = v_numDivisionID)) -- Priority 1
					OR ((tintStep2 = 2 AND PBI.numValue = v_numItemClassification) AND (tintStep3 = 1 AND PDTL.numValue = v_numDivisionID)) -- Priority 2
					OR (tintStep2 = 3 AND (tintStep3 = 1 AND PDTL.numValue = v_numDivisionID)) -- Priority 3

					OR ((tintStep2 = 1 AND PBI.numValue = v_numItemCode) AND (tintStep3 = 2 AND PDTL.numValue = v_numRelationship AND numProfile = v_numProfile)) -- Priority 4
					OR ((tintStep2 = 2 AND PBI.numValue = v_numItemClassification) AND (tintStep3 = 2 AND PDTL.numValue = v_numRelationship AND numProfile = v_numProfile)) -- Priority 5
					OR (tintStep2 = 3 AND (tintStep3 = 2 AND PDTL.numValue = v_numRelationship AND numProfile = v_numProfile)) -- Priority 6

					OR ((tintStep2 = 1 AND PBI.numValue = v_numItemCode) AND tintStep3 = 3) -- Priority 7
					OR ((tintStep2 = 2 AND PBI.numValue = v_numItemClassification) AND tintStep3 = 3) -- Priority 8
					OR (tintStep2 = 3 and tintStep3 = 3) -- Priority 9
				)   
			ORDER BY
				PP.Priority ASC LIMIT 1;

         IF coalesce(v_numPriceRuleID,0) > 0 then
			
            IF v_tintPricingMethod = 1 then -- BASED ON PRICE TABLE
				
               select   tintVendorCostType,tintDiscountType, (CASE WHEN tintDiscountType = 3 THEN 0 ELSE coalesce(decDiscount,0) END), tintRuleType, (CASE tintRuleType WHEN 1 THEN v_monListPrice WHEN 2 THEN v_monVendorCost END), (CASE tintRuleType
               WHEN 1 -- Deduct From List Price
               THEN(CASE tintDiscountType
                  WHEN 1 -- PERCENT
                  THEN(CASE WHEN v_monListPrice > 0 THEN v_monListPrice -(v_monListPrice*(coalesce(decDiscount,0)/100)) ELSE 0 END)
                  WHEN 2 -- FLAT AMOUNT
                  THEN(CASE WHEN v_monListPrice > 0 THEN(CASE WHEN(v_monListPrice*v_numQty::bigint*v_fltUOMConversionFactor) -(CASE WHEN v_numCurrencyID <> numCurrencyID THEN CAST((coalesce(decDiscount,0)/v_fltExchangeRate) AS INTEGER) ELSE coalesce(decDiscount,0) END) > 0 THEN((v_monListPrice*v_numQty::bigint*v_fltUOMConversionFactor) -(CASE WHEN v_numCurrencyID <> numCurrencyID THEN CAST((coalesce(decDiscount,0)/v_fltExchangeRate) AS INTEGER) ELSE coalesce(decDiscount,0) END))/(v_numQty::bigint*v_fltUOMConversionFactor) ELSE 0 END) ELSE 0 END)
                  WHEN 3 -- NAMED PRICE
                  THEN(CASE WHEN v_numCurrencyID <> numCurrencyID THEN CAST((coalesce(decDiscount,0)/v_fltExchangeRate) AS INTEGER) ELSE coalesce(decDiscount,0) END)
                  END)
               WHEN 2 -- Add to primary vendor cost
               THEN(CASE tintDiscountType
                  WHEN 1  -- PERCENT
                  THEN (CASE 
									WHEN tintVendorCostType=1 THEN v_monVendorDynamicCost 
									WHEN tintVendorCostType=2 THEN v_monVendorStaticCost 
									ELSE v_monVendorCost 
								END)+((CASE 
									WHEN tintVendorCostType=1 THEN v_monVendorDynamicCost 
									WHEN tintVendorCostType=2 THEN v_monVendorStaticCost 
									ELSE v_monVendorCost 
								END)*(coalesce(decDiscount,0)/100))
                  WHEN 2 -- FLAT AMOUNT
                  THEN(CASE WHEN((CASE 
									WHEN tintVendorCostType=1 THEN v_monVendorDynamicCost 
									WHEN tintVendorCostType=2 THEN v_monVendorStaticCost 
									ELSE v_monVendorCost 
								END)*v_numQty::bigint*v_fltUOMConversionFactor)+(CASE WHEN v_numCurrencyID <> numCurrencyID THEN CAST((coalesce(decDiscount,0)/v_fltExchangeRate) AS INTEGER) ELSE coalesce(decDiscount,0) END) > 0 THEN((v_monVendorCost*v_numQty::bigint*v_fltUOMConversionFactor)+(CASE WHEN v_numCurrencyID <> numCurrencyID THEN CAST((coalesce(decDiscount,0)/v_fltExchangeRate) AS INTEGER) ELSE coalesce(decDiscount,0) END))/(v_numQty::bigint*v_fltUOMConversionFactor)  ELSE 0 END)
                  WHEN 3 -- NAMED PRICE
                  THEN(CASE WHEN v_numCurrencyID <> numCurrencyID THEN CAST((coalesce(decDiscount,0)/v_fltExchangeRate) AS INTEGER) ELSE coalesce(decDiscount,0) END)
                  END)
               WHEN 3 -- Named price
               THEN(CASE
                  WHEN coalesce(v_tintPriceLevel,0) > 0
                  THEN(SELECT(CASE WHEN v_numCurrencyID <> numCurrencyID THEN CAST((coalesce(decDiscount,0)/v_fltExchangeRate) AS INTEGER) ELSE coalesce(decDiscount,0) END)
                        FROM(SELECT
                           ROW_NUMBER() OVER(PARTITION BY PricingTable.numItemCode ORDER BY numPricingID) AS Id,
																	decDiscount
                           FROM
                           PricingTable
                           WHERE
                           PricingTable.numItemCode = v_numItemCode
                           AND tintRuleType = 3) TEMP
                        WHERE
                        Id = v_tintPriceLevel)
                  ELSE(CASE WHEN v_numCurrencyID <> numCurrencyID THEN CAST((coalesce(decDiscount,0)/v_fltExchangeRate) AS INTEGER) ELSE coalesce(decDiscount,0) END)
                  END)
               END) INTO v_tintVendorCostType,v_tintDisountType,v_decDiscount,v_tintRuleType,v_monPriceRulePrice,v_monPriceRuleFinalPrice FROM
               PricingTable WHERE
               numPriceRuleID = v_numPriceRuleID
               AND 1 =(CASE
               WHEN EXISTS(SELECT * FROM PricingTable WHERE numItemCode = v_numItemCode AND coalesce(numPriceRuleID,0) = 0 AND numCurrencyID = v_numCurrencyID)
               THEN(CASE WHEN coalesce(numCurrencyID,0) = v_numCurrencyID THEN 1 ELSE 0 END)
               ELSE(CASE WHEN coalesce(numCurrencyID,0) = 0 THEN 1 ELSE 0 END)
               END)
               AND v_numQty::bigint*v_fltUOMConversionFactor >= intFromQty
               AND v_numQty::bigint*v_fltUOMConversionFactor <= intToQty;

			    v_decDiscountPercentPriceBook = v_decDiscount;
            ELSEIF v_tintPricingMethod = 2
            then -- BASED ON RULE
				
               v_tintRuleType := v_tintPriceRuleType;
               v_monPriceRulePrice :=(CASE v_tintPriceRuleType WHEN 1 THEN v_monListPrice WHEN 2 THEN (CASE 
									WHEN v_tintVendorCostType=1 THEN v_monVendorDynamicCost 
									WHEN v_tintVendorCostType=2 THEN v_monVendorStaticCost 
									ELSE v_monVendorCost 
								END) END);
               IF (v_tintPriceBookDiscountType = 1) then -- Percentage 
					
                  v_tintDisountType := v_tintPriceBookDiscountType;
                  v_decDiscount := v_decPriceBookDiscount*(CAST(v_numQty::bigint AS decimal)/v_intQntyItems::bigint);
				  v_decDiscountPercentPriceBook = v_decDiscount;
                  v_decDiscount := v_decPriceBookDiscount*(CASE WHEN v_tintPriceRuleType = 2 THEN (CASE 
									WHEN v_tintVendorCostType=1 THEN v_monVendorDynamicCost 
									WHEN v_tintVendorCostType=2 THEN v_monVendorStaticCost 
									ELSE v_monVendorCost 
								END) ELSE v_monListPrice END)/100;
                  v_decMaxDedPerAmt := v_decMaxDedPerAmt*(CASE WHEN v_tintPriceRuleType = 2 THEN (CASE 
									WHEN v_tintVendorCostType=1 THEN v_monVendorDynamicCost 
									WHEN v_tintVendorCostType=2 THEN v_monVendorStaticCost 
									ELSE v_monVendorCost 
								END) ELSE v_monListPrice END)/100;
                  IF (v_decDiscount > v_decMaxDedPerAmt) then
                     v_decDiscount := v_decMaxDedPerAmt;
                  end if;
                  IF (v_tintPriceRuleType = 1) then
                     v_monPriceRuleFinalPrice :=(v_monListPrice -v_decDiscount);
                  end if;
                  IF (v_tintPriceRuleType = 2) then
                     v_monPriceRuleFinalPrice :=((CASE 
									WHEN v_tintVendorCostType=1 THEN v_monVendorDynamicCost 
									WHEN v_tintVendorCostType=2 THEN v_monVendorStaticCost 
									ELSE v_monVendorCost 
								END)+v_decDiscount);
                  end if;
                  v_monPriceRuleFinalPrice :=(CASE WHEN v_numBaseCurrency <> v_numCurrencyID THEN CAST((coalesce(v_monPriceRuleFinalPrice,0)/v_fltExchangeRate) AS INTEGER) ELSE coalesce(v_monPriceRuleFinalPrice,0) END);
               end if;
               IF (v_tintPriceBookDiscountType = 2) then -- Flat discount 
					
                  v_decDiscount :=(CASE WHEN v_numBaseCurrency <> v_numCurrencyID THEN CAST((coalesce(v_decPriceBookDiscount,0)/v_fltExchangeRate) AS INTEGER) ELSE coalesce(v_decPriceBookDiscount,0) END)*((v_numQty::bigint*v_fltUOMConversionFactor)/v_intQntyItems::bigint);
                  IF (v_decDiscount > v_decMaxDedPerAmt) then
                     v_decDiscount := v_decMaxDedPerAmt;
                  end if;
                  IF (v_tintPriceRuleType = 1) then
                     v_monPriceRuleFinalPrice :=((v_monListPrice*v_numQty::bigint*v_fltUOMConversionFactor) -v_decDiscount)/(v_numQty::bigint*v_fltUOMConversionFactor);
                  end if;
                  IF (v_tintPriceRuleType = 2) then
                     v_monPriceRuleFinalPrice :=(((CASE 
									WHEN v_tintVendorCostType=1 THEN v_monVendorDynamicCost 
									WHEN v_tintVendorCostType=2 THEN v_monVendorStaticCost 
									ELSE v_monVendorCost 
								END)*v_numQty::bigint*v_fltUOMConversionFactor)+v_decDiscount)/(v_numQty::bigint*v_fltUOMConversionFactor);
                  end if;
               end if;

				IF v_tintPriceRuleType = 2 THEN
					v_bitMarkupDiscount := true;
				END IF;
            end if;
         end if;
      end if;
	
		-- GET Last Price
      select((coalesce(monPrice,0)*coalesce(OpportunityMaster.fltExchangeRate,1))/v_fltExchangeRate), coalesce(OpportunityItems.bitDiscountType,true), (CASE
      WHEN coalesce(OpportunityItems.bitDiscountType,true) = true
      THEN((coalesce(OpportunityItems.fltDiscount,0)*coalesce(OpportunityMaster.fltExchangeRate,1))/v_fltExchangeRate)
      ELSE coalesce(OpportunityItems.fltDiscount,0) END), FormatedDateFromDate(OpportunityMaster.bintCreatedDate,v_numDomainID) INTO v_monLastPrice,v_LastDiscountType,v_LastDiscount,v_dtLastOrderDate FROM
      OpportunityItems
      JOIN
      OpportunityMaster
      ON
      OpportunityItems.numOppId = OpportunityMaster.numOppId WHERE
      OpportunityMaster.numDomainId = v_numDomainID
      AND tintopptype = v_tintOppType
      AND tintoppstatus = 1
      AND numItemCode = v_numItemCode   ORDER BY
      OpportunityMaster.numOppId DESC LIMIT 1;
      IF v_tintDefaultSalesPricing = 1 AND coalesce(v_monPriceFinalPrice,0) > 0 then
		
			-- WE ARE NOT DISPLAYING DISCOUNT SEPERATELY IN PRICE LEVEL
         v_tintPricingBasedOn := 1;
         v_monPrice := v_monPriceFinalPrice;
         v_tintDisountType := 1;
         v_decDiscount := 0;
      ELSEIF v_tintDefaultSalesPricing = 2 AND coalesce(v_monPriceRuleFinalPrice,0) > 0
      then
		
         v_tintPricingBasedOn := 2;
         IF v_tintDisountType = 3 then
			
            v_monPrice := (CASE 
								WHEN COALESCE(v_bitRoundTo,false) = true AND COALESCE(v_tintRoundTo,0) > 0 
								THEN ROUND(v_monPriceRuleFinalPrice) + (CASE v_tintRoundTo WHEN 1 THEN -0.01 WHEN 2 THEN -0.05 WHEN 3 THEN 0 ELSE 0 END) 
								ELSE v_monPriceRuleFinalPrice 
							END);
            v_tintDisountType := 1;
            v_decDiscount := 0;
         ELSE
            IF v_tintPriceBookDiscount = 1 then -- Display Unit Price And Disocunt Price Seperately
				
				v_monPrice := v_monPriceRulePrice;
				IF v_tintDisountType = 1 THEN
					v_decDiscount := v_decDiscountPercentPriceBook;
				END IF;
            ELSE
               v_monPrice := (CASE 
								WHEN COALESCE(v_bitRoundTo,false) = true AND COALESCE(v_tintRoundTo,0) > 0 
								THEN ROUND(v_monPriceRuleFinalPrice) + (CASE v_tintRoundTo WHEN 1 THEN -0.01 WHEN 2 THEN -0.05 WHEN 3 THEN 0 ELSE 0 END) 
								ELSE v_monPriceRuleFinalPrice 
							END);
               v_tintDisountType := 1;
               v_decDiscount := 0;
            end if;
         end if;
      ELSEIF v_tintDefaultSalesPricing = 3 AND coalesce(v_monLastPrice,0) > 0
      then
		
         v_tintPricingBasedOn := 3;
         v_monPrice := v_monLastPrice;
         v_tintDisountType := v_LastDiscountType;
         v_decDiscount := v_LastDiscount;
      ELSE
         v_tintPricingBasedOn := 0;
         v_tintDisountType := 1;
         v_decDiscount := 0;
         v_tintRuleType := 0;
         IF v_tintOppType = 1 then
            v_monPrice := v_monListPrice;
         ELSE
            v_monPrice := (CASE 
									WHEN v_tintVendorCostType=1 THEN v_monVendorDynamicCost 
									WHEN v_tintVendorCostType=2 THEN v_monVendorStaticCost 
									ELSE v_monVendorCost 
								END);
         end if;
      end if;
   ELSEIF v_tintOppType = 2
   then      
      If EXISTS (SELECT 
					VendorCostTable.numvendorcosttableid
				FROM 
					Vendor 
				INNER JOIN 
					VendorCostTable 
				ON 
					Vendor.numvendortcode=VendorCostTable.numvendortcode 
				WHERE
					Vendor.numDomainID=v_numDomainID
					AND Vendor.numVendorID=v_numDivisionID 
					AND Vendor.numItemCode=v_numItemCode 
					AND VendorCostTable.numCurrencyID = v_numCurrencyID
					AND COALESCE(v_numQty,0) BETWEEN VendorCostTable.intFromQty AND VendorCostTable.intToQty
					AND VendorCostTable.monDynamicCost IS NOT NULL) then
			SELECT 
				VendorCostTable.monDynamicCost
				,dtDynamicCostModifiedDate
			INTO 
				v_monPrice
				,v_dtDynamicCostModifiedDate
			FROM 
				Vendor 
			INNER JOIN 
				VendorCostTable 
			ON 
				Vendor.numvendortcode=VendorCostTable.numvendortcode 
			WHERE
				Vendor.numDomainID=v_numDomainID
				AND Vendor.numVendorID=v_numDivisionID 
				AND Vendor.numItemCode=v_numItemCode 
				AND VendorCostTable.numCurrencyID = v_numCurrencyID
				AND COALESCE(v_numQty,0) BETWEEN VendorCostTable.intFromQty AND VendorCostTable.intToQty;
      ELSE
         v_monPrice := v_monVendorCost;
      end if;
      v_monPriceLevelPrice := 0;
      v_monPriceFinalPrice := 0;
      v_monPriceRulePrice := 0;
      v_monPriceRuleFinalPrice := 0;
      v_monLastPrice := 0;
      v_dtLastOrderDate := NULL;
      v_tintDisountType := 1;
      v_decDiscount := 0;
   end if;

   IF coalesce(v_numWarehouseItemID,0) > 0 then
	
      select   numWareHouseID INTO v_numTempWarehouseID FROM WareHouseItems WHERE numWareHouseItemID = v_numWarehouseItemID;
   ELSE
      v_numTempWarehouseID := v_numWarehouseID;
   end if;

   open SWV_RefCur for SELECT
   v_vcItemName AS vcItemName,
		v_bitDropship AS bitDropship,
		v_chrItemType AS ItemType,
		v_bitMatrix AS bitMatrix,
		v_numItemClassification AS ItemClassification,
		v_numItemGroup AS ItemGroup,
		v_ItemDesc AS ItemDescription,
		v_bitAssembly AS bitAssembly,
		v_bitKit AS bitKitParent,
		v_bitHasChildKits AS bitHasChildKits,
		v_bitSOWorkOrder AS bitSOWorkOrder,
		case when coalesce(v_bitAssembly,false) = true then fn_GetAssemblyPossibleWOQty(v_numItemCode,v_numTempWarehouseID) else 0 end AS MaxWorkOrderQty,
		v_tintPricingBasedOn AS tintPriceBaseOn,
		v_monPrice AS monPrice,
		v_fltUOMConversionFactor AS UOMConversionFactor,
		(CASE WHEN v_tintOppType = 1 THEN v_monListPrice ELSE (CASE 
									WHEN v_tintVendorCostType=1 THEN v_monVendorDynamicCost 
									WHEN v_tintVendorCostType=2 THEN v_monVendorStaticCost 
									ELSE v_monVendorCost 
								END) END) AS monListPrice,
		(CASE 
			WHEN v_tintVendorCostType=1 THEN v_monVendorDynamicCost 
			WHEN v_tintVendorCostType=2 THEN v_monVendorStaticCost 
			ELSE v_monVendorCost 
		END) AS VendorCost,
		v_numVendorID AS numVendorID,
		v_tintRuleType AS tintRuleType,
		v_monPriceLevelPrice AS monPriceLevel,
		v_monPriceFinalPrice AS monPriceLevelFinalPrice,
		v_monPriceRulePrice AS monPriceRule,
		v_monPriceRuleFinalPrice AS monPriceRuleFinalPrice,
		v_monLastPrice AS monLastPrice,
		v_dtLastOrderDate AS LastOrderDate,
		v_tintDisountType AS DiscountType,
		v_decDiscount AS Discount,
		v_numBaseUnit AS numBaseUnit,
		v_numSaleUnit AS numSaleUnit,
		v_numPurchaseUnit AS numPurchaseUnit,
		fn_GetUOMName(v_numBaseUnit) AS vcBaseUOMName,
		fn_GetUOMName(v_numSaleUnit) AS vcSaleUOMName,
		v_numPurchaseUnit AS numPurchaseUnit,
		v_numWarehouseItemID AS numWarehouseItemID,
		coalesce((SELECT numOnHand FROM WareHouseItems WI WHERE WI.numWareHouseItemID = v_numWarehouseItemID),0) AS numOnhand,
		fn_GetItemTransitCount(v_numItemCode,v_numDomainID) AS OrdersInTransist,
		(SELECT COUNT(*) FROM SimilarItems SI INNER JOIN Item I ON I.numItemCode = SI.numItemCode WHERE SI.numDomainId = v_numDomainID AND SI.numParentItemCode = v_numItemCode) AS RelatedItemsCount,
		v_tintPriceLevel AS tintPriceLevel,
		v_vcMinOrderQty AS vcMinOrderQty,
		v_vcVendorNotes AS vcVendorNotes,
		v_bitCalAmtBasedonDepItems AS bitCalAmtBasedonDepItems,
		v_tintKitAssemblyPriceBasedOn AS tintKitAssemblyPriceBasedOn,
		v_dtDynamicCostModifiedDate AS dtDynamicCostModifiedDate,
		v_bitMarkupDiscount AS bitMarkupDiscount;
END; $$; 












