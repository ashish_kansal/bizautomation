-- Stored procedure definition script USP_GetDeferredIncomeFromDomain for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_GetDeferredIncomeFromDomain(v_numDomainId NUMERIC(9,0) DEFAULT 0,      
v_numOppId NUMERIC(9,0) DEFAULT 0,      
v_numOppBizDocsId NUMERIC(9,0) DEFAULT 0,      
INOUT v_bitDeferredIncome BOOLEAN  DEFAULT NULL,      
INOUT v_bitBizDocId BOOLEAN  DEFAULT NULL,    
INOUT v_tintOppType SMALLINT  DEFAULT NULL,  
INOUT v_numRecurringId NUMERIC(9,0) DEFAULT 0)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_AuthoritativeBizDocs  NUMERIC(9,0);       
   v_numBizDocId  NUMERIC(9,0);
BEGIN
   v_bitDeferredIncome := false;      
   v_numBizDocId := 0;       
   v_AuthoritativeBizDocs := 0;      
    
   select   coalesce(bitDeferredIncome,false) INTO v_bitDeferredIncome From Domain Where numDomainId = v_numDomainId;      
   select   tintopptype, coalesce(OPR.numRecurringId,0) INTO v_tintOppType,v_numRecurringId From OpportunityMaster Opp LEFT OUTER JOIN OpportunityRecurring OPR ON Opp.numOppId = OPR.numOppId Where numDomainId = v_numDomainId  And Opp.numOppId = v_numOppId;         
   If v_tintOppType = 1 then
      select   coalesce(numAuthoritativeSales,0) INTO v_AuthoritativeBizDocs From AuthoritativeBizDocs Where numDomainId = v_numDomainId;
   ELSEIF v_tintOppType = 2
   then
      select   coalesce(numAuthoritativePurchase,0) INTO v_AuthoritativeBizDocs From AuthoritativeBizDocs Where numDomainId = v_numDomainId;
   end if;      
 
       
   select   numBizDocId INTO v_numBizDocId From OpportunityBizDocs Where numOppBizDocsId = v_numOppBizDocsId;      
   if   v_AuthoritativeBizDocs = v_numBizDocId then
      v_bitBizDocId := true;
   Else
      v_bitBizDocId := false;
   end if;
   RETURN;
END; $$;
/****** Object:  StoredProcedure [dbo].[USP_GetDepositDetails]    Script Date: 07/26/2008 16:17:17 ******/



