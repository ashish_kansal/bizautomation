-- Stored procedure definition script usp_DeleteTemplate for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION usp_DeleteTemplate(v_numTemplateID NUMERIC,
v_bintDateDeleted NUMERIC,
v_numDeletedby NUMERIC
--
)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   update TemplateDocuments set bitStatus = true,bintDateDeleted = v_bintDateDeleted,numDateDeletedby = v_numDeletedby where numTemplateID = v_numTemplateID;
   RETURN;
END; $$;


