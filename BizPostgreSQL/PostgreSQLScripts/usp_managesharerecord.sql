-- Stored procedure definition script USP_ManageShareRecord for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ManageShareRecord(v_numDomainID NUMERIC(9,0),        
	 v_numRecordID NUMERIC(9,0),      
	 v_numModuleID SMALLINT ,    
	 v_strXml   TEXT DEFAULT '',
	 v_tintMode SMALLINT DEFAULT 0, INOUT SWV_RefCur refcursor default null, INOUT SWV_RefCur2 refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_hDoc1  INTEGER;
BEGIN
   IF v_tintMode = 0 then --Select
	
      open SWV_RefCur for
      SELECT CAST(A.numContactId AS VARCHAR(18)) || '~0' AS numContactID ,A.vcFirstName || ' ' || A.vcLastname as vcUserName
      from UserMaster UM join AdditionalContactsInformation A  on UM.numUserDetailId = A.numContactId
      where UM.numDomainID = v_numDomainID  and UM.intAssociate = 1 and UM.numDomainID = A.numDomainID AND A.numContactId NOT IN(SELECT numAssignedTo FROM ShareRecord WHERE numDomainID = v_numDomainID AND numModuleID = v_numModuleID AND numRecordID = v_numRecordID)
      order by vcUserName;

      open SWV_RefCur2 for
      SELECT CAST(SR.numAssignedTo AS VARCHAR(18)) || '~' || CAST(coalesce(SR.numContactType,0) AS VARCHAR(18)) AS numContactID,
				A.vcFirstName || ' ' || A.vcLastname || CASE WHEN coalesce(SR.numContactType,0) > 0 THEN '(' || fn_GetListItemName(SR.numContactType) || ')' ELSE '' END as vcUserName
      FROM ShareRecord SR JOIN UserMaster UM ON UM.numUserDetailId = SR.numAssignedTo
      JOIN AdditionalContactsInformation A ON UM.numUserDetailId = A.numContactId
      WHERE SR.numDomainID = v_numDomainID AND SR.numModuleID = v_numModuleID AND SR.numRecordID = v_numRecordID
      AND UM.numDomainID = v_numDomainID and UM.numDomainID = A.numDomainID
      order by vcUserName;
   ELSEIF v_tintMode = 1
   then --Insert
      delete from ShareRecord where numDomainID = v_numDomainID and numRecordID = v_numRecordID  AND numModuleID = v_numModuleID;
    
      insert into ShareRecord(numDomainID,numRecordID,numAssignedTo,numContactType,numModuleID)
      select v_numDomainID,v_numRecordID,X.numAssignedTo,X.numContactType,v_numModuleID
      from
	  XMLTABLE
			(
				'NewDataSet/Table'
				PASSING 
					CAST(v_strXml AS XML)
				COLUMNS
					id FOR ORDINALITY,
					numAssignedTo NUMERIC(9,0) PATH 'numAssignedTo',
					numContactType NUMERIC(9,0) PATH 'numContactType'
			) X;

   end if;

    
/****** Object:  StoredProcedure [dbo].[USP_ManageTabsInCuSFields]    Script Date: 07/26/2008 16:20:03 ******/
   RETURN;
END; $$;


