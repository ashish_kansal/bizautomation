-- Stored procedure definition script usp_GetSurveyInformation4ForkedExecution for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION usp_GetSurveyInformation4ForkedExecution(v_numSurId NUMERIC,          
 v_vcQuestionIDs VARCHAR(50), INOUT SWV_RefCur refcursor , INOUT SWV_RefCur2 refcursor)
LANGUAGE plpgsql            
--This procedure will return the selected survey Questions and Answer Information for the selected survey          
   AS $$
   DECLARE
   v_SqlSelect  VARCHAR(4000);
BEGIN
   RAISE NOTICE '%',v_numSurId;
   RAISE NOTICE '%',v_vcQuestionIDs;
   v_SqlSelect := 'SELECT  numSurID, numQID, vcQuestion, tIntAnsType, intNumOfAns        
  FROM SurveyQuestionMaster            
  WHERE numSurID = ' || SUBSTR(CAST(v_numSurId AS VARCHAR(30)),1,30) || '
  AND numQID IN (' || coalesce(v_vcQuestionIDs,'') || ')  
  ORDER BY numQID';
   OPEN SWV_RefCur FOR EXECUTE v_SqlSelect;
        
   v_SqlSelect := 'SELECT numSurID, numQID, numAnsID, vcAnsLabel, 0 as boolSurveyRuleAttached, 0 as boolDeleted
  FROM SurveyAnsMaster            
  WHERE  numSurID = ' || SUBSTR(CAST(v_numSurId AS VARCHAR(30)),1,30) || '
  AND numQID IN (' || coalesce(v_vcQuestionIDs,'') || ')  
  ORDER BY numQID,numAnsID';     
   OPEN SWV_RefCur2 FOR EXECUTE v_SqlSelect;
   RETURN;
END; $$;


