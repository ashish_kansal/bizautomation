DROP FUNCTION IF EXISTS USP_GetAccountPayableAging_Invoice;

CREATE OR REPLACE FUNCTION USP_GetAccountPayableAging_Invoice(v_numDomainId NUMERIC(9,0) DEFAULT 0,
    v_vcFlag VARCHAR(20) DEFAULT NULL,
	v_numAccountClass NUMERIC(9,0) DEFAULT 0,
	v_dtFromDate DATE DEFAULT NULL,
	v_dtToDate DATE DEFAULT NULL,
	v_ClientTimeZoneOffset INTEGER DEFAULT 0, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_AuthoritativePurchaseBizDocId  INTEGER;
   v_baseCurrency  NUMERIC;
   v_strSql  TEXT;
   v_strSql1  TEXT;
   v_strCheck  TEXT;
   v_strJournal  TEXT;
   v_strCommission  TEXT;
   v_strReturn  TEXT;
   v_strSqlCredit  TEXT;
BEGIN
   select   coalesce(numAuthoritativePurchase,0) INTO v_AuthoritativePurchaseBizDocId FROM
   AuthoritativeBizDocs WHERE
   numDomainId =  v_numDomainId;
	------------------------------------------      
   select   numCurrencyID INTO v_baseCurrency FROM Domain WHERE numDomainId = v_numDomainId;
	/*when any Order is placed in Foreign currency (other than base currency)then show it in purple color */
	------------------------------------------      
   IF v_dtFromDate IS NULL then
      v_dtFromDate := '1990-01-01 00:00:00.000';
   end if;
   IF v_dtToDate IS NULL then
      v_dtToDate := TIMEZONE('UTC',now());
   end if;

   v_strSql := ' DROP TABLE IF EXISTS tt_TempRecords CASCADE; CREATE TEMPORARY TABLE tt_TempRecords AS SELECT X.* from (SELECT 
               OM.numOppId,
               OM.vcPOppName,
               OM.tintOppType,
               OB.numOppBizDocsId,
               OB.vcBizDocID,
               COALESCE(OB.monDealAmount * OM.fltExchangeRate,
                      0) TotalAmount,--OM.monPAmount
               (COALESCE(TablePayment.monPaidAmount,0) * OM.fltExchangeRate) AmountPaid,
               COALESCE(OB.monDealAmount * OM.fltExchangeRate
                        - COALESCE(TablePayment.monPaidAmount,0) * OM.fltExchangeRate,0)  BalanceDue,
						CASE COALESCE(bitBillingTerms,false) 
                 WHEN true THEN OB.dtFromDate + make_interval(days => COALESCE((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = COALESCE(intBillingDays,0)), 0))
                 WHEN false THEN ob.dtFromDate
               END AS dtDueDate,
               CASE COALESCE(bitBillingTerms,false) 
                 WHEN true THEN FormatedDateFromDate(OB.dtFromDate + make_interval(days => COALESCE((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = COALESCE(intBillingDays,0)), 0)),
                                                          ' || SUBSTR(CAST(v_numDomainId AS VARCHAR(15)),1,15) || ')
                 WHEN false THEN FormatedDateFromDate(ob.dtFromDate,' || SUBSTR(CAST(v_numDomainId AS VARCHAR(15)),1,15) || ')
               END AS DueDate,
               bitBillingTerms,
               intBillingDays,
               OB.dtCreatedDate,
               COALESCE(C.varCurrSymbol,'''') varCurrSymbol,
               OM.fltExchangeRate,
               OB.dtFromDate,
			   0 AS numBizDocsPaymentDetId,CO.vcCompanyName,ADC.vcFirstname || '' ''|| ADC.vcLastName AS vcContactName,
                           fn_GetContactName(OM.numRecOwner) AS RecordOwner,
						   fn_GetContactName(OM.numAssignedTo) AS AssignedTo
						   ,CASE WHEN ADC.numPhone IS NULL THEN '''' ELSE ADC.numPhone || (CASE WHEN numPhoneExtension IS NULL THEN '''' WHEN numPhoneExtension='''' THEN '''' ELSE '','' || ADC.numPhoneExtension END) END AS vcCustPhone,
						 OM.numCurrencyID,OB.vcRefOrderNo
        FROM   OpportunityMaster OM
               INNER JOIN DivisionMaster DM ON OM.numDivisionId = DM.numDivisionID
               INNER JOIN CompanyInfo CO ON CO.numCompanyId = DM.numCompanyID
               LEFT JOIN AdditionalContactsInformation ADC ON ADC.numDivisionId=DM.numDivisionID AND ADC.numContactId=OM.numContactId
               INNER JOIN OpportunityBizDocs OB ON OB.numOppId = OM.numOppID
               LEFT JOIN Currency C ON OM.numCurrencyID = C.numCurrencyID
			   LEFT JOIN
				(
					SELECT
						BPD.numOppBizDocsID
						,SUm(BPD.monAmount) monPaidAmount
					FROM	
						BillPaymentDetails BPD
					INNER JOIN
						BillPaymentHeader BPH
					ON
						BPD.numBillPaymentID=BPH.numBillPaymentID
					WHERE
						BPH.numDomainId = ' || SUBSTR(CAST(v_numDomainId AS VARCHAR(15)),1,15) || '
						AND CAST(BPH.dtPaymentDate AS DATE) <= ''' || SUBSTR(CAST(v_dtToDate AS VARCHAR(30)),1,30) || '''
					GROUP BY
						BPD.numOppBizDocsID
				) TablePayment ON TablePayment.numOppBizDocsID = OB.numOppBizDocsId
        WHERE  OM.tintOppType = 2
               AND OM.tintOppStatus = 1
               AND om.numDomainId = ' || SUBSTR(CAST(v_numDomainId AS VARCHAR(15)),1,15) || '
               AND OB.numBizDocId = ' || CAST(coalesce(v_AuthoritativePurchaseBizDocId,0) AS VARCHAR(15)) || '
			   AND (OM.numAccountClass= ' || SUBSTR(CAST(v_numAccountClass AS VARCHAR(30)),1,30) || ' OR ' || SUBSTR(CAST(v_numAccountClass AS VARCHAR(30)),1,30) || '=0)
			   AND OB.dtCreatedDate::DATE  <= ''' || SUBSTR(CAST(v_dtToDate AS VARCHAR(30)),1,30) || '''
               AND COALESCE(OB.tintDeferred,0) <> 1
			   AND (COALESCE(OB.monDealAmount * COALESCE(OM.fltExchangeRate,1), 0) - COALESCE(COALESCE(TablePayment.monPaidAmount,0) * COALESCE(OM.fltExchangeRate, 1), 0)) > 0';
               
   v_strSql1 := ' UNION 
		SELECT  
               0 AS numOppId,
               ''Bill'' vcPOppName,
               -1 AS tintOppType,-- flag for bills 
               0 numOppBizDocsId,
               CASE WHEN LENGTH(BH.vcReference)=0 THEN ''Open Bill'' ELSE BH.vcReference END AS vcBizDocID,
               BH.monAmountDue as TotalAmount,
               COALESCE(TablePayment.monAmtPaid, 0) as AmountPaid,
               COALESCE(BH.monAmountDue, 0) - COALESCE(TablePayment.monAmtPaid, 0) as BalanceDue,
			   BH.dtDueDate,
               FormatedDateFromDate(BH.dtDueDate,BH.numDomainID) AS DueDate,
               false bitBillingTerms,
               0 intBillingDays,
               BH.dtCreatedDate dtCreatedDate,
               '''' varCurrSymbol,
               1 AS fltExchangeRate,
               BH.dtDueDate AS dtFromDate,
               0 numBizDocsPaymentDetId
			   ,CO.vcCompanyName
			   ,ADC.vcFirstname || '' ''|| ADC.vcLastName AS vcContactName
			   ,'''' AS RecordOwner
			   ,'''' AS AssignedTo
				,CASE WHEN ADC.numPhone IS NULL THEN '''' ELSE ADC.numPhone || (CASE WHEN numPhoneExtension IS NULL THEN '''' WHEN numPhoneExtension='''' THEN '''' ELSE '','' || ADC.numPhoneExtension END) END AS vcCustPhone
				,' || SUBSTR(CAST(v_baseCurrency AS VARCHAR(15)),1,15) || ',''''  as vcRefOrderNo 
        FROM    BillHeader BH INNER JOIN DivisionMaster DM ON BH.numDivisionId = DM.numDivisionID
                  INNER JOIN CompanyInfo CO ON CO.numCompanyId = DM.numCompanyID
                  LEFT JOIN AdditionalContactsInformation ADC ON ADC.numDivisionId=DM.numDivisionID AND COALESCE(ADC.bitPrimaryContact,false)=true
				  LEFT JOIN LATERAL
				(
					SELECT
						BPD.numBillID
						,SUM(monAmount) monAmtPaid
					FROM 
						BillPaymentDetails BPD
					INNER JOIN
						BillPaymentHeader BPH
					ON
						BPD.numBillPaymentID=BPH.numBillPaymentID
					WHERE
						BPH.numDomainId = ' || SUBSTR(CAST(v_numDomainId AS VARCHAR(15)),1,15) || '
						AND CAST(BPH.dtPaymentDate AS DATE) <= ''' || SUBSTR(CAST(v_dtToDate AS VARCHAR(30)),1,30) || '''
					GROUP BY
						BPD.numBillID
				) TablePayment ON TablePayment.numBillID=BH.numBillID
        WHERE   
                BH.numDomainID = ' || SUBSTR(CAST(v_numDomainId AS VARCHAR(15)),1,15) || '
                AND COALESCE(BH.monAmountDue, 0) - COALESCE(TablePayment.monAmtPaid, 0) > 0
				AND (BH.numAccountClass= ' || SUBSTR(CAST(v_numAccountClass AS VARCHAR(30)),1,30) || ' OR ' || SUBSTR(CAST(v_numAccountClass AS VARCHAR(30)),1,30) || '=0)
				AND BH.dtBillDate::DATE  <= ''' || SUBSTR(CAST(v_dtToDate AS VARCHAR(30)),1,30) || '''';
				
   v_strCheck := ' UNION ALL
                SELECT  
					0 AS numOppId,
						   ''Check'' vcPOppName,
						   -3 AS tintOppType,-- flag for Checks
						   0 numOppBizDocsId,
						   '''' AS vcBizDocID,
						   COALESCE(CD.monAmount, 0) as TotalAmount,
						   COALESCE(CD.monAmount, 0) as AmountPaid,
					       0 as BalanceDue,
						   CH.dtCheckDate AS dtDueDate,
						   FormatedDateFromDate(CH.dtCheckDate,CH.numDomainID) AS DueDate,
						   false bitBillingTerms,
						   0 intBillingDays,
						   CH.dtCreatedDate dtCreatedDate,
						   '''' varCurrSymbol,
						   1 AS fltExchangeRate,
						   CH.dtCheckDate AS dtFromDate,
						   0 as numBizDocsPaymentDetId
						  ,CO.vcCompanyName
						   ,ADC.vcFirstname || '' ''|| ADC.vcLastName AS vcContactName
						   ,'''' AS RecordOwner
						   ,'''' AS AssignedTo
						   ,CASE WHEN ADC.numPhone IS NULL THEN '''' ELSE ADC.numPhone || (CASE WHEN numPhoneExtension IS NULL THEN '''' WHEN numPhoneExtension='''' THEN '''' ELSE '','' || ADC.numPhoneExtension END) END AS vcCustPhone
						   ,' || SUBSTR(CAST(v_baseCurrency AS VARCHAR(15)),1,15) || '
						   ,'''' as vcRefOrderNo 
                FROM    CheckHeader CH
				INNER JOIN DivisionMaster DM ON CH.numDivisionId = DM.numDivisionID
					INNER JOIN CompanyInfo CO ON CO.numCompanyId = DM.numCompanyID
					LEFT OUTER JOIN AdditionalContactsInformation ADC ON ADC.numDivisionId=DM.numDivisionID AND COALESCE(ADC.bitPrimaryContact,false)=true
						JOIN  CheckDetails CD ON CH.numCheckHeaderID=CD.numCheckHeaderID
                        INNER JOIN Chart_Of_Accounts COA ON COA.numAccountId = CD.numChartAcntId
                WHERE   CH.numDomainID = ' || SUBSTR(CAST(v_numDomainId AS VARCHAR(15)),1,15) || ' AND CH.tintReferenceType=1
						AND COA.vcAccountCode LIKE ''01020102%''
						AND (CH.numAccountClass= ' || SUBSTR(CAST(v_numAccountClass AS VARCHAR(30)),1,30) || ' OR ' || SUBSTR(CAST(v_numAccountClass AS VARCHAR(30)),1,30) || '=0)
						AND CH.dtCheckDate <= ''' || SUBSTR(CAST(v_dtToDate AS VARCHAR(30)),1,30) || '''';

   v_strJournal := ' UNION ALL
                SELECT  
					GJH.numJournal_Id numOppId,
					''Journal'' vcPOppName,
					-1 tintOppType,
					0 numOppBizDocsId,
					'''' vcBizDocID,
					CASE WHEN GJD.numDebitAmt > 0 THEN -1 * GJD.numDebitAmt ELSE GJD.numCreditAmt END TotalAmount,
					0 AmountPaid,
					CASE WHEN GJD.numDebitAmt > 0 THEN -1 * GJD.numDebitAmt ELSE GJD.numCreditAmt END BalanceDue,
					GJH.datEntry_Date AS dtDueDate,
					FormatedDateFromDate(GJH.datEntry_Date,' || SUBSTR(CAST(v_numDomainId AS VARCHAR(15)),1,15) || ') AS DueDate,
					false bitBillingTerms,
					0 intBillingDays,
					GJH.datCreatedDate,
					'''' varCurrSymbol,
					1 fltExchangeRate,
					GJH.datEntry_Date AS dtFromDate,
					0 numBizDocsPaymentDetId
					,CO.vcCompanyName
					,ADC.vcFirstname || '' ''|| ADC.vcLastName AS vcContactName
					,'''' AS RecordOwner
					,'''' AS AssignedTo
					,CASE WHEN ADC.numPhone IS NULL THEN '''' ELSE ADC.numPhone || (CASE WHEN numPhoneExtension IS NULL THEN '''' WHEN numPhoneExtension='''' THEN '''' ELSE '','' || ADC.numPhoneExtension END) END AS vcCustPhone
					,GJD.numCurrencyID
					,'''' as vcRefOrderNo
                FROM    General_Journal_Header GJH
                        INNER JOIN General_Journal_Details GJD ON GJH.numJournal_Id = GJD.numJournalId
                                                                      AND GJH.numDomainId = GJD.numDomainId
                        INNER JOIN Chart_Of_Accounts COA ON COA.numAccountId = GJD.numChartAcntId
						INNER JOIN DivisionMaster DM ON GJD.numCustomerID = DM.numDivisionID
						INNER JOIN CompanyInfo CO ON CO.numCompanyId = DM.numCompanyID
						Left JOIN AdditionalContactsInformation ADC ON ADC.numContactId=GJD.numContactId 
                WHERE   GJH.numDomainId = ' || SUBSTR(CAST(v_numDomainId AS VARCHAR(15)),1,15) || ' 
                        AND COA.vcAccountCode LIKE ''01020102%''
                        AND COALESCE(numOppId, 0) = 0
                        AND COALESCE(numOppBizDocsId, 0) = 0
                        AND COALESCE(GJH.numBillID,0)=0 AND COALESCE(GJH.numBillPaymentID,0)=0 AND COALESCE(GJH.numCheckHeaderID,0)=0 AND COALESCE(GJH.numReturnID,0)=0
                        AND COALESCE(GJH.numPayrollDetailID,0)=0
						AND (GJH.numAccountClass= ' || SUBSTR(CAST(v_numAccountClass AS VARCHAR(30)),1,30) || ' OR ' || SUBSTR(CAST(v_numAccountClass AS VARCHAR(30)),1,30) || '=0)
						AND GJH.datEntry_Date <= ''' || SUBSTR(CAST(v_dtToDate AS VARCHAR(30)),1,30) || '''';

   v_strCommission :=  ' UNION ALL
                SELECT  
					Opp.numOppId,
					Opp.vcPOppName,
					Opp.tintOppType,
					OBD.numOppBizDocsId,
					OBD.vcBizDocID,
					BDC.numComissionAmount,
					0 AmountPaid,
					BDC.numComissionAmount BalanceDue,
					CASE COALESCE(Opp.bitBillingTerms,false) 
						WHEN true THEN OBD.dtFromDate + make_interval(days => COALESCE((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = COALESCE(Opp.intBillingDays,0)), 0))
						WHEN false THEN OBD.dtFromDate
					END AS dtDueDate,
					CASE COALESCE(Opp.bitBillingTerms,false) 
						WHEN true THEN FormatedDateFromDate(OBD.dtFromDate + make_interval(days => COALESCE((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = COALESCE(Opp.intBillingDays,0)), 0)),
																' || SUBSTR(CAST(v_numDomainId AS VARCHAR(15)),1,15) || ')
						WHEN false THEN FormatedDateFromDate(OBD.dtFromDate,' || SUBSTR(CAST(v_numDomainId AS VARCHAR(15)),1,15) || ')
					END AS DueDate,
					Opp.bitBillingTerms,
					Opp.intBillingDays,
					OBD.dtCreatedDate,
					'''' varCurrSymbol,
					Opp.fltExchangeRate,
					OBD.dtFromDate,
					0 AS numBizDocsPaymentDetId
					,CO.vcCompanyName
					,ADC.vcFirstname || '' ''|| ADC.vcLastName AS vcContactName
					,'''' AS RecordOwner
					,'''' AS AssignedTo
					,CASE WHEN ADC.numPhone IS NULL THEN '''' ELSE ADC.numPhone || (CASE WHEN numPhoneExtension IS NULL THEN '''' WHEN numPhoneExtension='''' THEN '''' ELSE '','' || ADC.numPhoneExtension END) END AS vcCustPhone
					,Opp.numCurrencyID
					,OBD.vcRefOrderNo
                FROM    BizDocComission BDC
                        JOIN OpportunityBizDocs OBD ON BDC.numOppBizDocId = OBD.numOppBizDocsId
                        join OpportunityMaster Opp on OPP.numOppId = OBD.numOppId
						INNER JOIN DivisionMaster DM ON Opp.numDivisionId = DM.numDivisionID
					   INNER JOIN CompanyInfo CO ON CO.numCompanyId = DM.numCompanyID
					   LEFT JOIN AdditionalContactsInformation ADC ON ADC.numDivisionId=DM.numDivisionID AND ADC.numContactId=Opp.numContactId
                        JOIN Domain D on D.numDomainID = BDC.numDomainID
                        LEFT JOIN PayrollTracking PT ON BDC.numComissionID=PT.numComissionID
                WHERE   OPP.numDomainID = ' || SUBSTR(CAST(v_numDomainId AS VARCHAR(15)),1,15) || '
                        and BDC.numDomainID = ' || SUBSTR(CAST(v_numDomainId AS VARCHAR(15)),1,15) || '
                        AND (Opp.numAccountClass= ' || SUBSTR(CAST(v_numAccountClass AS VARCHAR(30)),1,30) || ' OR ' || SUBSTR(CAST(v_numAccountClass AS VARCHAR(30)),1,30) || '=0)
						AND OBD.dtCreatedDate <= ''' || SUBSTR(CAST(v_dtToDate AS VARCHAR(30)),1,30) || '''
						AND COALESCE(BDC.bitCommisionPaid,false)=false
                        AND OBD.bitAuthoritativeBizDocs = 1
						AND COALESCE(BDC.numComissionAmount, 0) > 0';

   v_strReturn := ' UNION ALL
			 SELECT 
				0 AS numOppId,
				''Return'' vcPOppName,
				-1 AS tintOppType,
				0 numOppBizDocsId,
				'''' AS vcBizDocID,
				(CASE WHEN GJD.numDebitAmt > 0 THEN GJD.numDebitAmt * -1 ELSE GJD.numCreditAmt END) as TotalAmount,
				0 as AmountPaid,
				CASE WHEN GJD.numDebitAmt > 0 THEN GJD.numDebitAmt * -1 ELSE GJD.numCreditAmt END as BalanceDue,
				GJH.datEntry_Date AS dtDueDate,
				FormatedDateFromDate(GJH.datEntry_Date,RH.numDomainID) AS DueDate,
				false bitBillingTerms,
				0 intBillingDays,
				RH.dtCreatedDate dtCreatedDate,
				'''' varCurrSymbol,
				1 AS fltExchangeRate,
				GJH.datEntry_Date AS dtFromDate,
				0 as numBizDocsPaymentDetId
				,CO.vcCompanyName
				,ADC.vcFirstname || '' ''|| ADC.vcLastName AS vcContactName
				,'''' AS RecordOwner
				,'''' AS AssignedTo
				,CASE WHEN ADC.numPhone IS NULL THEN '''' ELSE ADC.numPhone || (CASE WHEN numPhoneExtension IS NULL THEN '''' WHEN numPhoneExtension='''' THEN '''' ELSE '','' || ADC.numPhoneExtension END) END AS vcCustPhone
				,GJD.numCurrencyID
				,'''' as vcRefOrderNo
			 FROM   ReturnHeader RH JOIN General_Journal_Header GJH ON RH.numReturnHeaderID = GJH.numReturnID
			JOIN General_Journal_Details GJD ON GJH.numJournal_Id = GJD.numJournalId 
			INNER JOIN Chart_Of_Accounts COA ON COA.numAccountId = GJD.numChartAcntId
			INNER JOIN DivisionMaster DM ON RH.numDivisionId = DM.numDivisionID
			INNER JOIN CompanyInfo CO ON CO.numCompanyId = DM.numCompanyID
			LEFT JOIN AdditionalContactsInformation ADC ON ADC.numDivisionId=DM.numDivisionID AND ADC.numContactId=RH.numContactId
			WHERE RH.tintReturnType=4 AND RH.numDomainId=' || SUBSTR(CAST(v_numDomainId AS VARCHAR(15)),1,15) || '  AND COA.vcAccountCode LIKE ''01020102%''
					AND (monBizDocAmount > 0) AND COALESCE(RH.numBillPaymentIDRef,0)>0
					AND (Rh.numAccountClass= ' || SUBSTR(CAST(v_numAccountClass AS VARCHAR(30)),1,30) || ' OR ' || SUBSTR(CAST(v_numAccountClass AS VARCHAR(30)),1,30) || '=0)
					AND GJH.datEntry_Date <= ''' || SUBSTR(CAST(v_dtToDate AS VARCHAR(30)),1,30) || '''';

   v_strSqlCredit := CONCAT(' UNION ALL
							SELECT 
								0 AS numOppId,
								''Credit'' vcPOppName,
								-1 AS tintOppType,
								0 numOppBizDocsId,
								'''' AS vcBizDocID,
								COALESCE(monPaymentAmount,0) as TotalAmount,
								COALESCE(monAppliedAmount,0) as AmountPaid,
								(COALESCE(monPaymentAmount,0) - COALESCE(monAppliedAmount,0)) * -1 as BalanceDue,
								BPH.dtPaymentDate AS dtDueDate,
								FormatedDateFromDate(BPH.dtPaymentDate,BPH.numDomainID) AS DueDate,
								false bitBillingTerms,
								0 intBillingDays,
								BPH.dtCreateDate dtCreatedDate,
								'''' varCurrSymbol,
								1 AS fltExchangeRate,
								BPH.dtCreateDate AS dtFromDate,
								0 as numBizDocsPaymentDetId
								,CO.vcCompanyName
								,'''' AS vcContactName
								,'''' AS RecordOwner
								,'''' AS AssignedTo
								,'''' AS vcCustPhone
								,0
								,'''' as vcRefOrderNo							
							FROM 
								BillPaymentHeader BPH
							LEFT JOIN DivisionMaster DM ON BPH.numDivisionId = DM.numDivisionID
							LEFT JOIN CompanyInfo CO ON CO.numCompanyId = DM.numCompanyID
							WHERE 
								BPH.numDomainId=',v_numDomainId,'
								AND (COALESCE(monPaymentAmount,0) - COALESCE(monAppliedAmount,0)) > 0
								AND (BPH.numAccountClass=',
   v_numAccountClass,' OR ',v_numAccountClass,
   '=0)
								AND BPH.dtPaymentDate <= ''',v_dtToDate,''''); 
    
   
   IF (v_vcFlag = '0+30') then
      
      v_strSql := coalesce(v_strSql,'') || ' AND 
                dtFromDate + make_interval(days => CASE 
                                 WHEN bitBillingTerms = true THEN COALESCE((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = COALESCE(intBillingDays,0)), 0)
                                 ELSE 0
                               END) BETWEEN GetUTCDateWithoutTime() AND public.GetUTCDateWithoutTime()+ interval ''31 day''
               AND GetUTCDateWithoutTime() <= dtFromDate + make_interval(days => CASE 
                                                 WHEN bitBillingTerms = true THEN COALESCE((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = COALESCE(intBillingDays,0)), 0)
                                                 ELSE 0
                                               END) ';
      v_strSql1 := coalesce(v_strSql1,'') || ' AND BH.dtDueDate BETWEEN GetUTCDateWithoutTime() AND public.GetUTCDateWithoutTime()+ interval ''31 day''';
   ELSEIF (v_vcFlag = '30+60')
   then
        
      v_strSql := coalesce(v_strSql,'') || ' AND  
                 dtFromDate + make_interval(days => CASE 
                                     WHEN bitBillingTerms = true THEN COALESCE((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = COALESCE(intBillingDays,0)), 0)
                                   ELSE 0
                                 END) BETWEEN public.GetUTCDateWithoutTime()+ interval ''31 day'' AND public.GetUTCDateWithoutTime()+ interval ''61 day''
                 AND GetUTCDateWithoutTime() <= dtFromDate + make_interval(days => CASE 
																WHEN bitBillingTerms = true THEN COALESCE((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = COALESCE(intBillingDays,0)), 0)
                                                   ELSE 0
                                                 END)
                 ';
      v_strSql1 := coalesce(v_strSql1,'') || ' AND BH.dtDueDate BETWEEN public.GetUTCDateWithoutTime()+ interval ''31 day'' AND public.GetUTCDateWithoutTime()+ interval ''61 day''';
   ELSEIF (v_vcFlag = '60+90')
   then
          
      v_strSql := coalesce(v_strSql,'') || ' AND  
                   dtFromDate + make_interval(days => CASE 
                                     WHEN bitBillingTerms = true THEN COALESCE((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = COALESCE(intBillingDays,0)), 0)
                                     ELSE 0
                                   END) BETWEEN public.GetUTCDateWithoutTime()+ interval ''61 day'' AND public.GetUTCDateWithoutTime()+ interval ''91 day''
                   AND GetUTCDateWithoutTime() <= dtFromDate + make_interval(days => CASE 
																  WHEN bitBillingTerms = true THEN COALESCE((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = COALESCE(intBillingDays,0)), 0)
                                                     ELSE 0
                                                   END) ';
      v_strSql1 := coalesce(v_strSql1,'') || ' AND BH.dtDueDate BETWEEN public.GetUTCDateWithoutTime()+ interval ''61 day'' AND public.GetUTCDateWithoutTime()+ interval ''91 day''';
   ELSEIF (v_vcFlag = '90+')
   then
            
      v_strSql := coalesce(v_strSql,'') || ' AND  
                     dtFromDate + make_interval(days => CASE 
                                       WHEN bitBillingTerms = true THEN COALESCE((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = COALESCE(intBillingDays,0)), 0)
                                       ELSE 0
                                     END) > public.GetUTCDateWithoutTime()+ interval ''91 day''
                     AND GetUTCDateWithoutTime() <= dtFromDate + make_interval(days => CASE 
                                                       WHEN bitBillingTerms = true THEN COALESCE((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = COALESCE(intBillingDays,0)), 0)
                                                       ELSE 0
                                                     END)
                     ';
      v_strSql1 := coalesce(v_strSql1,'') || ' AND BH.dtDueDate > public.GetUTCDateWithoutTime()+ interval ''91 day''';
   ELSEIF (v_vcFlag = '0-30')
   then
              
      v_strSql := coalesce(v_strSql,'') || ' AND  
                       GetUTCDateWithoutTime() > dtFromDate + make_interval(days => CASE 
                                                        WHEN bitBillingTerms = true THEN COALESCE((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = COALESCE(intBillingDays,0)), 0)
                                                        ELSE 0
                                                      END)
                       AND DATEDIFF (''day'',dtFromDate + make_interval(days => CASE 
                                                      WHEN bitBillingTerms = true THEN COALESCE((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = COALESCE(intBillingDays,0)), 0)
                                                      ELSE 0
                                                    END),GetUTCDateWithoutTime()) BETWEEN 0 AND 30
                       ';
      v_strSql1 := coalesce(v_strSql1,'') || ' AND DATEDIFF (''day'',BH.dtDueDate,GetUTCDateWithoutTime()) BETWEEN 0 AND 30 ';
   ELSEIF (v_vcFlag = '30-60')
   then
                
      v_strSql := coalesce(v_strSql,'') || ' AND  
                         GetUTCDateWithoutTime() > dtFromDate + make_interval(days => CASE 
                                                          WHEN bitBillingTerms = true THEN COALESCE((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = COALESCE(intBillingDays,0)), 0)
                                                          ELSE 0
                                                        END)
                         AND DATEDIFF (''day'',dtFromDate + make_interval(days => CASE 
                                                        WHEN bitBillingTerms = true THEN COALESCE((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = COALESCE(intBillingDays,0)), 0)
                                                        ELSE 0
                                                      END),GetUTCDateWithoutTime()) BETWEEN 31 AND 60
                         ';
      v_strSql1 := coalesce(v_strSql1,'') || ' AND DATEDIFF (''day'',BH.dtDueDate,GetUTCDateWithoutTime()) BETWEEN 31 AND 60 ';
   ELSEIF (v_vcFlag = '60-90')
   then
                  
      v_strSql := coalesce(v_strSql,'') || ' AND  
                           GetUTCDateWithoutTime() > dtFromDate + make_interval(days => CASE 
                                                            WHEN bitBillingTerms = true THEN COALESCE((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = COALESCE(intBillingDays,0)), 0)
                                                            ELSE 0
                                                          END)
                           AND DATEDIFF (''day'',dtFromDate + make_interval(days => CASE 
                                                          WHEN bitBillingTerms = true THEN COALESCE((SELECT numNetDueInDays FROM BillingTerms 
																											WHERE numTermsID = COALESCE(intBillingDays,0)), 0)
                                                          ELSE 0
                                                        END),GetUTCDateWithoutTime()) BETWEEN 61 AND 90
                           ';
      v_strSql1 := coalesce(v_strSql1,'') || ' AND DATEDIFF (''day'',BH.dtDueDate,GetUTCDateWithoutTime()) BETWEEN 61 AND 90 ';
   ELSEIF (v_vcFlag = '90-')
   then
                    
      v_strSql := coalesce(v_strSql,'') || ' AND  
                             GetUTCDateWithoutTime() > dtFromDate + make_interval(days => CASE 
                                                              WHEN bitBillingTerms = true THEN COALESCE((SELECT numNetDueInDays FROM BillingTerms 
																											WHERE numTermsID = COALESCE(intBillingDays,0)), 0)
                                                              ELSE 0
                                                            END)
                             AND DATEDIFF (''day'',dtFromDate + make_interval(days => CASE 
                                                            WHEN bitBillingTerms = true THEN COALESCE((SELECT numNetDueInDays FROM BillingTerms 
																											WHERE numTermsID = COALESCE(intBillingDays,0)), 0)
                                                            ELSE 0
                                                          END),GetUTCDateWithoutTime()) > 90
                             ';
      v_strSql1 := coalesce(v_strSql1,'') || ' AND DATEDIFF (''day'',BH.dtDueDate,GetUTCDateWithoutTime()) > 90 ';
   end if;
                    
   v_strSql := coalesce(v_strSql,'') || coalesce(v_strSql1,'') || coalesce(v_strCheck,'') || coalesce(v_strJournal,'') || coalesce(v_strCommission,'') || coalesce(v_strReturn,'') || coalesce(v_strSqlCredit,'');

   v_strSql := coalesce(v_strSql,'') || ' ) X';
   
   RAISE NOTICE '%',CAST(v_strSql AS TEXT);

   EXECUTE v_strSql;

   OPEN SWV_RefCur FOR EXECUTE 'SELECT *,CASE when DATEDIFF (''day'' ,dtDueDate ,now()::TIMESTAMP) > 0 THEN DATEDIFF (''day'' ,dtDueDate ,now()::TIMESTAMP) ELSE NULL END AS DaysLate,CASE WHEN ' || SUBSTR(CAST(v_baseCurrency AS VARCHAR(15)),1,15) || ' <> numCurrencyID THEN 2 ELSE 1 END AS CurrencyCount
    FROM tt_TempRecords 
    WHERE  COALESCE(TotalAmount,0) <> 0 AND COALESCE(BalanceDue,0) <> 0';
   RETURN;
END; $$;


