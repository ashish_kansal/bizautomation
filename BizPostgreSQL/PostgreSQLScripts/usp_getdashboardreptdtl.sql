-- Stored procedure definition script USP_GetDashBoardReptDTL for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_GetDashBoardReptDTL(v_numDashBoardReptID NUMERIC(9,0), INOUT SWV_RefCur refcursor)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_tintReportCategory  SMALLINT;
   v_numReportID  NUMERIC(9,0);
   v_numGroupUserCntID  NUMERIC(9,0);
   v_tintComAppliesTo  SMALLINT;
BEGIN
   select   tintReportCategory, numReportID, numGroupUserCntID INTO v_tintReportCategory,v_numReportID,v_numGroupUserCntID FROM Dashboard WHERE numDashBoardReptID = v_numDashBoardReptID;
 
   IF v_tintReportCategory = 1 then
 
      open SWV_RefCur for
      select numReportID,numGroupUserCntID,tintReportType,vcHeader,vcFooter,
 tintChartType,bitGroup,tintColumn,bintRows,tintRow,
 (select max(D.tintRow) from Dashboard D where D.numGroupUserCntID = D1.numGroupUserCntID and D.bitGroup = D1.bitGroup and D.tintColumn = D1.tintColumn) as MaxRow ,
 (select min(D.tintRow) from Dashboard D where D.numGroupUserCntID = D1.numGroupUserCntID and D.bitGroup = D1.bitGroup and D.tintColumn = D1.tintColumn) as MinRow,
  bitGridType,varGrpflt,textQueryGrp,textQuery ,tintReportCategory
      from
      CustomReport Cus
      Join  Dashboard D1
      on Cus.numCustomReportId = D1.numReportID
      where numDashBoardReptID = v_numDashBoardReptID;
   ELSEIF v_tintReportCategory = 2
   then
      select   coalesce(tintComAppliesTo,0) INTO v_tintComAppliesTo FROM Domain WHERE numDomainId =(SELECT UM.numDomainID FROM
         CommissionReports Cus Join Dashboard D1  on Cus.numComReports = D1.numReportID JOIN UserMaster UM ON UM.numUserDetailId = Cus.numContactID
         where numDashBoardReptID = v_numDashBoardReptID);
      if v_tintComAppliesTo = 1 then

         open SWV_RefCur for
         select numReportID,numGroupUserCntID,tintReportType,I.vcItemName || CASE cus.tintAssignTo WHEN 1 THEN '(Assign To)' WHEN 2 THEN '(Record Owner)' END || ' - Commission Report' AS vcHeader,vcFooter,
 tintChartType,tintColumn,tintRow,
 (select max(D.tintRow) from Dashboard D where D.numGroupUserCntID = D1.numGroupUserCntID and D.bitGroup = D1.bitGroup and D.tintColumn = D1.tintColumn) as MaxRow ,
 (select min(D.tintRow) from Dashboard D where D.numGroupUserCntID = D1.numGroupUserCntID and D.bitGroup = D1.bitGroup and D.tintColumn = D1.tintColumn) as MinRow,
 'exec GetCommissionItemReport @numDomainId,@numUserCntID,@numItemCode=' || CAST(cus.numItemCode AS VARCHAR(10)) || ',@tintAssignTo=' || CAST(cus.tintAssignTo AS VARCHAR(10)) || ',@bitCommContact=0' AS textQueryGrp,tintReportCategory
         from
         CommissionReports cus
         Join  Dashboard D1  on cus.numComReports = D1.numReportID
         JOIN Item I ON cus.numItemCode = I.numItemCode
         where numDashBoardReptID = v_numDashBoardReptID;
      ELSEIF v_tintComAppliesTo = 2
      then

         open SWV_RefCur for
         select numReportID,numGroupUserCntID,tintReportType,GetListIemName(numItemCode) || CASE cus.tintAssignTo WHEN 1 THEN '(Assign To)' WHEN 2 THEN '(Record Owner)' END || ' - Commission Report' AS vcHeader,vcFooter,
 tintChartType,tintColumn,tintRow,
 (select max(D.tintRow) from Dashboard D where D.numGroupUserCntID = D1.numGroupUserCntID and D.bitGroup = D1.bitGroup and D.tintColumn = D1.tintColumn) as MaxRow ,
 (select min(D.tintRow) from Dashboard D where D.numGroupUserCntID = D1.numGroupUserCntID and D.bitGroup = D1.bitGroup and D.tintColumn = D1.tintColumn) as MinRow,
 'exec GetCommissionItemReport @numDomainId,@numUserCntID,@numItemCode=' || CAST(cus.numItemCode AS VARCHAR(10)) || ',@tintAssignTo=' || CAST(cus.tintAssignTo AS VARCHAR(10)) || ',@bitCommContact=0' AS textQueryGrp,tintReportCategory
         from
         CommissionReports cus
         Join  Dashboard D1  on cus.numComReports = D1.numReportID
         where numDashBoardReptID = v_numDashBoardReptID;
      ELSEIF v_tintComAppliesTo = 3
      then

         open SWV_RefCur for
         select numReportID,numGroupUserCntID,tintReportType,'All Items' || CASE cus.tintAssignTo WHEN 1 THEN '(Assign To)' WHEN 2 THEN '(Record Owner)' END || ' - Commission Report' AS vcHeader,vcFooter,
 tintChartType,tintColumn,tintRow,
 (select max(D.tintRow) from Dashboard D where D.numGroupUserCntID = D1.numGroupUserCntID and D.bitGroup = D1.bitGroup and D.tintColumn = D1.tintColumn) as MaxRow ,
 (select min(D.tintRow) from Dashboard D where D.numGroupUserCntID = D1.numGroupUserCntID and D.bitGroup = D1.bitGroup and D.tintColumn = D1.tintColumn) as MinRow,
 'exec GetCommissionItemReport @numDomainId,@numUserCntID,@numItemCode=' || CAST(cus.numItemCode AS VARCHAR(10)) || ',@tintAssignTo=' || CAST(cus.tintAssignTo AS VARCHAR(10)) || ',@bitCommContact=0' AS textQueryGrp,tintReportCategory
         from
         CommissionReports cus
         Join  Dashboard D1  on cus.numComReports = D1.numReportID
         where numDashBoardReptID = v_numDashBoardReptID;
      end if;
   end if;
   RETURN;
END; $$;


