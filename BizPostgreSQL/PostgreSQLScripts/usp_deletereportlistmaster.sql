-- Stored procedure definition script USP_DeleteReportListMaster for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_DeleteReportListMaster(v_numDomainID NUMERIC(18,0),
	v_numReportID NUMERIC(18,0))
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   DELETE FROM ReportFieldsList WHERE numReportID = v_numReportID; 

   DELETE FROM ReportFilterList WHERE numReportID = v_numReportID; 

   DELETE FROM ReportSummaryGroupList WHERE numReportID = v_numReportID; 

--DELETE FROM ReportMatrixBreakList WHERE numReportID=@numReportID 

   DELETE FROM ReportMatrixAggregateList WHERE numReportID = v_numReportID; 

   DELETE FROM ReportListMaster WHERE numDomainID = v_numDomainID AND numReportID = v_numReportID;
   RETURN;
END; $$;


