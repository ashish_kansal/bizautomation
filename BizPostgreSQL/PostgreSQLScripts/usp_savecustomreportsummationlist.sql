-- Stored procedure definition script usp_SaveCustomReportSummationList for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION usp_SaveCustomReportSummationList(v_ReportId NUMERIC(9,0) DEFAULT 0,    
v_strSummationList TEXT DEFAULT '')
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_hDoc  INTEGER;
BEGIN
	DELETE FROM CustReportSummationlist WHERE numCustomReportId = v_ReportId;    
                     
	INSERT INTO CustReportSummationlist
	(
		numCustomReportId,vcFieldName,bitSum,bitAvg,bitMax,bitMin
	)
	SELECT 
		v_ReportId,X.vcField,X.bitSum,X.bitAvg,X.bitMax,X.bitMin
	FROM
	XMLTABLE
	(
		'NewDataSet/Table'
		PASSING 
			CAST(v_strSummationList AS XML)
		COLUMNS
			id FOR ORDINALITY,
			vcField VARCHAR(200) PATH 'vcField',
			bitSum BOOLEAN PATH 'bitSum',
			bitAvg BOOLEAN PATH 'bitAvg',
			bitMax BOOLEAN PATH 'bitMax',
			bitMin BOOLEAN PATH 'bitMin'
	) AS X; 

   RETURN;
END; $$;


