-- Stored procedure definition script USP_CaseProjectCosting for PostgreSQL
CREATE OR REPLACE FUNCTION USP_CaseProjectCosting(v_numDivId NUMERIC,    
v_numCaseId NUMERIC,    
v_numProId NUMERIC,    
v_numDomainId NUMERIC,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for select
   cast(case when TE.numCaseId <> 0 then vcCaseNumber when TE.numProId <> 0 then vcProjectName end as VARCHAR(255)) as name,
case when numcategory = 1 then 'Time' when numCategory = 2 then 'Expense' end as Type,
case when TE.numtype = 1 then 'Billable' when TE.numtype = 2 then  'Non-Billable' end as TimeType,
case when TE.numtype = 1 then monamount  when TE.numtype = 2 then  0 end as RateHr,
case when numCategory = 1 then
      CAST(CAST((EXTRACT(DAY FROM TE.dtToDate -TE.dtFromDate)*60*24+EXTRACT(HOUR FROM TE.dtToDate -TE.dtFromDate)*60+EXTRACT(MINUTE FROM TE.dtToDate -TE.dtFromDate))*0.016666666666666666666666666666667 AS DECIMAL(10,2)) AS VARCHAR(20))
   when numCategory = 2 then '-' end
   as hrs,
case when numCategory = 1 then
      case when TE.numtype = 1 then
         CAST((EXTRACT(DAY FROM TE.dtToDate -TE.dtFromDate)*60*24+EXTRACT(HOUR FROM TE.dtToDate -TE.dtFromDate)*60+EXTRACT(MINUTE FROM TE.dtToDate -TE.dtFromDate))*monamount*0.016666666666666666666666666666667 AS DECIMAL(10,2))
      when TE.numtype = 2 then  0 end
   when numCategory = 2 then
      case when TE.numtype = 1 then monamount  when TE.numtype = 2 then  0 end
   end
   as AmountBilled,
case when numCategory = 1 then
      case when TE.numtype = 1 then
         case when bithourlyrate = true then
            CAST((EXTRACT(DAY FROM TE.dtToDate -TE.dtFromDate)*60*24+EXTRACT(HOUR FROM TE.dtToDate -TE.dtFromDate)*60+EXTRACT(MINUTE FROM TE.dtToDate -TE.dtFromDate))*0.016666666666666666666666666666667*monhourlyrate AS DECIMAL(10,2))
         else
            0
         end
      when TE.numtype = 2 then 0 end
   when numCategory = 2 then
      case when bithourlyrate = true then
         monamount*monhourlyrate
      else
         0
      end
   end
   as labourrate,
case when numCategory = 1 then
      CAST(CAST((EXTRACT(DAY FROM TE.dtToDate -TE.dtFromDate)*60*24+EXTRACT(HOUR FROM TE.dtToDate -TE.dtFromDate)*60+EXTRACT(MINUTE FROM TE.dtToDate -TE.dtFromDate))*0.016666666666666666666666666666667 AS DECIMAL(10,2)) AS VARCHAR(20)) || '  ,' || vcUserName
   when numCategory = 2 then ' - ,' || vcUserName  end as  TimeEntered
   from timeandexpense TE
   left join Cases C on TE.numCaseId = C.numCaseId
   left join ProjectsMaster P on TE.numProId = P.numProId
   join UserMaster UM on TE.numTCreatedBy = numuserid
   where
(TE.numCaseId =(case when  v_numCaseId > 0 then  v_numCaseId else 0 end)
   or TE.numProId =(case when  v_numProId > 0 then  v_numProId else 0 end)) and
((C.numDivisionID = v_numDivId and C.numDomainID = v_numDomainId)
   or (P.numDivisionId = v_numDivId and P.numdomainId = v_numDomainId));
END; $$;













