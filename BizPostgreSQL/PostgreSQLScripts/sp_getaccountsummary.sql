-- Stored procedure definition script SP_GetAccountSummary for PostgreSQL
CREATE OR REPLACE FUNCTION SP_GetAccountSummary(v_numDomainId INTEGER,
v_dtFromDate TIMESTAMP,
v_dtToDate TIMESTAMP,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT COA.numAccountId,vcAccountName,numParntAcntTypeID,vcAccountDescription,vcAccountCode,
 coalesce(COA.numOriginalOpeningBal,0)+(SELECT coalesce(SUM(GJD.numDebitAmt),0) -coalesce(SUM(GJD.numCreditAmt),0)
      FROM
      General_Journal_Details GJD INNER JOIN
      General_Journal_Header GJH ON GJD.numJournalId = GJH.numJOurnal_Id AND
      GJD.numDomainId = COA.numDomainId AND
      GJD.numChartAcntId = COA.numAccountId AND
      GJH.datEntry_Date < v_dtFromDate) AS OPENING,
(SELECT cast(coalesce(SUM(GJD.numDebitAmt),0) as NUMERIC(18,0))
      FROM
      General_Journal_Details GJD INNER JOIN
      General_Journal_Header GJH ON GJD.numJournalId = GJH.numJOurnal_Id AND
      GJD.numDomainId = COA.numDomainId AND
      GJD.numChartAcntId = COA.numAccountId AND
      GJH.datEntry_Date BETWEEN v_dtFromDate AND v_dtToDate) as DEBIT,
(SELECT cast(coalesce(SUM(GJD.numCreditAmt),0) as NUMERIC(18,0))
      FROM
      General_Journal_Details GJD INNER JOIN
      General_Journal_Header GJH ON GJD.numJournalId = GJH.numJOurnal_Id AND
      GJD.numDomainId = COA.numDomainId AND
      GJD.numChartAcntId = COA.numAccountId AND
      GJH.datEntry_Date BETWEEN v_dtFromDate AND v_dtToDate) as CREDIT
   FROM Chart_Of_Accounts COA
   WHERE COA.numDomainId = v_numDomainId;
END; $$;













