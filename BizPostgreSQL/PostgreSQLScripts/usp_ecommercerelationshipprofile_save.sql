-- Stored procedure definition script USP_EcommerceRelationshipProfile_Save for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_EcommerceRelationshipProfile_Save(v_numSiteID NUMERIC(18,0)
	,v_numRelationship NUMERIC(18,0)
	,v_numProfile NUMERIC(18,0)
	,v_vcItemClassificationIds VARCHAR(500))
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_ID  INTEGER;
BEGIN
   IF coalesce(v_numSiteID,0) = 0 OR NOT EXISTS(SELECT * FROM Sites WHERE numSiteID = v_numSiteID) then
	
      RAISE EXCEPTION 'SITE_DOES_NOT_EXISTS';
      RETURN;
   ELSEIF coalesce(v_numRelationship,0) = 0
   then
	
      RAISE EXCEPTION 'RELATIONSHIP_REQUIRED';
      RETURN;
   ELSEIF coalesce(v_numProfile,0) = 0
   then
	
      RAISE EXCEPTION 'PROFILE_REQUIRED';
      RETURN;
   end if;

   DROP TABLE IF EXISTS tt_TEMP CASCADE;
   CREATE TEMPORARY TABLE tt_TEMP
   (
      numItemClassification NUMERIC(18,0)
   );

   INSERT INTO tt_TEMP  SELECT Id FROM SplitIDs(v_vcItemClassificationIds,',');

   IF(SELECT COUNT(*) FROM tt_TEMP) = 0 then
	
      RAISE EXCEPTION 'ITEMCLASSIFICATION_REQUIRED';
      RETURN;
   end if;

   IF EXISTS(SELECT * FROM
   EcommerceRelationshipProfile
   INNER JOIN
   ECommerceItemClassification
   ON
   EcommerceRelationshipProfile.ID = ECommerceItemClassification.numECommRelatiionshipProfileID
   WHERE
   numSiteID = v_numSiteID
   AND numRelationship = v_numRelationship
   AND numProfile = v_numProfile
   AND numItemClassification IN(SELECT numItemClassification FROM tt_TEMP)) then
	
      RAISE EXCEPTION 'DUPLICATE_SELECTION';
      RETURN;
   end if;

   INSERT INTO EcommerceRelationshipProfile(numSiteID
		,numRelationship
		,numProfile)
	VALUES(v_numSiteID
		,v_numRelationship
		,v_numProfile);

		v_ID := CURRVAL('EcommerceRelationshipProfile_seq');
	
   INSERT INTO ECommerceItemClassification(numECommRelatiionshipProfileID
		,numItemClassification)
   SELECT
   v_ID
		,numItemClassification
   FROM
   tt_TEMP;
END; $$;
/****** Object:  StoredProcedure [dbo].[USP_EcommerceSettings]    Script Date: 03/25/2009 16:46:21 ******/



