-- Stored procedure definition script USP_GetMutltiAR for PostgreSQL
Create or replace FUNCTION USP_GetMutltiAR(v_numParentDomainID  INTEGER,
 v_dtFromDate TIMESTAMP,
 v_dtToDate TIMESTAMP,
v_numSubscriberID INTEGER,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   DROP TABLE IF EXISTS tt_ARSUMMARY CASCADE;
   CREATE TEMPORARY TABLE tt_ARSUMMARY
   (
      vcDomainName VARCHAR(150),
      Total  NUMERIC(19,4)
   );

   INSERT INTO tt_ARSUMMARY
   select DN.vcDomainName,Sum(Total) as Total
   from VIEW_ARDAILYSUMMARY VARD
   INNER JOIN(SELECT * FROM Domain DN WHERE DN.numSubscriberID = v_numSubscriberID) DN ON VARD.numDomainID  =  DN.numDomainId
   INNER JOIN(SELECT * FROM Domain DNV WHERE DNV.numSubscriberID = v_numSubscriberID) DNV ON VARD.numDomainID = DNV.numDomainId INNER JOIN
   FinancialYear FY ON FY.numDomainId = DN.numDomainId AND
   FY.dtPeriodFrom <= v_dtFromDate and dtPeriodTo >= v_dtFromDate
   AND (DN.numParentDomainID = v_numParentDomainID or DN.numDomainId = v_numParentDomainID)
   AND datEntry_Date between FY.dtPeriodFrom and v_dtFromDate+INTERVAL '-1 day'
   GROUP BY DN.vcDomainName
   union
   select DN.vcDomainName,Sum(Total) as Total
   from VIEW_ARDAILYSUMMARY VARD
   INNER JOIN(SELECT * FROM Domain DN WHERE DN.numSubscriberID = v_numSubscriberID) DN ON VARD.numDomainID  =  DN.numDomainId
   INNER JOIN(SELECT * FROM Domain DNV WHERE DNV.numSubscriberID = v_numSubscriberID) DNV ON VARD.numDomainID = DNV.numDomainId
   AND (DN.numDomainId = v_numParentDomainID)
   AND datEntry_Date between v_dtFromDate and v_dtToDate
   GROUP BY DN.vcDomainName

----------------------------------------
   union 
--------------------------
   select DN.vcDomainName,Sum(Total) as Total
   from VIEW_ARDAILYSUMMARY VARD
   INNER JOIN(select * from Domain DN where DN.numSubscriberID = v_numSubscriberID) DN ON VARD.vcDomainCode  ilike  DN.vcDomainCode || '%'
   INNER JOIN(SELECT * FROM Domain DNV  WHERE DNV.numSubscriberID = v_numSubscriberID) DNV ON VARD.numDomainID = DNV.numDomainId INNER JOIN
   FinancialYear FY ON FY.numDomainId = DN.numDomainId AND
   FY.dtPeriodFrom <= v_dtFromDate and dtPeriodTo >= v_dtFromDate
   AND (DN.numParentDomainID = v_numParentDomainID or DN.numDomainId = v_numParentDomainID)
   AND datEntry_Date between FY.dtPeriodFrom and v_dtFromDate+INTERVAL '-1 day'
   GROUP BY DN.vcDomainName
   union
   select DN.vcDomainName,Sum(Total) as Total
   from VIEW_ARDAILYSUMMARY VARD
   INNER JOIN(SELECT * FROM Domain DN WHERE DN.numSubscriberID = v_numSubscriberID) DN  ON VARD.vcDomainCode  ilike  DN.vcDomainCode || '%'
   INNER JOIN(SELECT * FROM Domain DNV WHERE DNV.numSubscriberID = v_numSubscriberID) DNV  ON VARD.numDomainID = DNV.numDomainId
   AND (DN.numParentDomainID = v_numParentDomainID)
   AND datEntry_Date between v_dtFromDate and v_dtToDate
   GROUP BY DN.vcDomainName;



open SWV_RefCur for SELECT vcDomainName, Sum(Total) as Total FROM tt_ARSUMMARY GROUP BY vcDomainName;

   RETURN;
END; $$;














