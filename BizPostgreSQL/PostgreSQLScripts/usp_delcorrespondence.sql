-- Stored procedure definition script USP_DelCorrespondence for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_DelCorrespondence(v_numID NUMERIC(9,0) DEFAULT 0,
v_tintType SMALLINT DEFAULT NULL)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   if v_tintType = 1 then

      DELETE FROM Correspondence WHERE numEmailHistoryID = v_numID;
      delete from EmailHStrToBCCAndCC where numEmailHstrID = v_numID;
      delete from EmailHistory where numEmailHstrID = v_numID;
   ELSEIF v_tintType = 2
   then

      DELETE FROM Correspondence WHERE numCommID = v_numID;
      delete from Communication where numCommId = v_numID;
   end if;
   RETURN;
END; $$;


