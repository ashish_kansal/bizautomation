-- Stored procedure definition script usp_GetAllSystemModules for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION usp_GetAllSystemModules(v_tintGroupType SMALLINT DEFAULT 0, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
BEGIN
   IF EXISTS(SELECT ID FROM ModuleMasterGroupType WHERE tintGroupType = v_tintGroupType) then
	
      open SWV_RefCur for
      SELECT
      ModuleMaster.numModuleID
			,ModuleMaster.vcModuleName
			,ModuleMasterGroupType.tintGroupType
      FROM
      ModuleMaster
      INNER JOIN
      ModuleMasterGroupType
      ON
      ModuleMaster.numModuleID = ModuleMasterGroupType.numModuleID
      WHERE
      ModuleMasterGroupType.tintGroupType = v_tintGroupType
      ORDER BY
      vcModuleName ASC;
   ELSE
      open SWV_RefCur for
      SELECT
      ModuleMaster.numModuleID
			,ModuleMaster.vcModuleName
			,tintGroupType
      FROM
      ModuleMaster
      WHERE
      tintGroupType = v_tintGroupType
      ORDER BY
      vcModuleName ASC;
   end if;
   RETURN;
END; $$;


