-- Stored procedure definition script USP_RollUpBudgets for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_RollUpBudgets(v_numDomainId NUMERIC(9,0) DEFAULT 0,                                                
 v_intFiscalYear INTEGER DEFAULT 0,     
 v_tintType INTEGER DEFAULT 0, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_strSQL  TEXT;                   
   v_strMonSQL  VARCHAR(8000);     
   v_strProcureSQL  VARCHAR(8000);   
   v_strMarketSQL  VARCHAR(8000);  
   v_dtFiscalStDate  TIMESTAMP;                          
   v_dtFiscalEndDate  TIMESTAMP;
   v_numMonth  SMALLINT;  
   v_len  VARCHAR(1000);                   
   v_Date  TIMESTAMP;
BEGIN
   v_strMonSQL := ''; 
   v_strProcureSQL := '';   
                
   v_strMarketSQL := '';
   v_numMonth := 1;                                
 --Select @numMonth=tintFiscalStartMonth From Domain Where numDomainId=@numDomainId                            
   v_Date := TIMEZONE('UTC',now())+CAST(v_tintType || 'year' as interval);          
     
   While v_numMonth <= 12 LOOP
      v_dtFiscalStDate := GetFiscalStartDate(GetFiscalyear(v_Date,v_numDomainId)::INTEGER,v_numDomainId);
      v_dtFiscalStDate := v_dtFiscalStDate+CAST(v_numMonth -1 || 'month' as interval);
      v_dtFiscalEndDate :=  GetFiscalStartDate(GetFiscalyear(v_Date,v_numDomainId)::INTEGER,v_numDomainId)+INTERVAL '1 year'+INTERVAL '-1 day';
      v_strMonSQL := coalesce(v_strMonSQL,'') || ' ' ||  '(Select sum(monAmount) from OperationBudgetMaster OBM inner join OperationBudgetDetails OBD On OBM.numBudgetId=OBD.numBudgetId 
                                        Where OBM.numDomainId=' || SUBSTR(CAST(v_numDomainId AS VARCHAR(10)),1,10) || ' And OBD.numChartAcntId=RecordID And OBD.tintmonth=' || CAST(EXTRACT(month FROM v_dtFiscalStDate) AS VARCHAR(10)) || ' And OBD.intYear=' || CAST(EXTRACT(year FROM v_dtFiscalStDate) AS VARCHAR(10)) || ')'
      || ' as ''' || CAST(EXTRACT(month FROM v_dtFiscalStDate) AS VARCHAR(2)) || '~' || CAST(EXTRACT(year FROM v_dtFiscalStDate) AS VARCHAR(4)) || ''',';
      v_strProcureSQL := coalesce(v_strProcureSQL,'') || ' ' || ' (Select sum(monAmount) From ProcurementBudgetMaster PBM Inner join ProcurementBudgetDetails PBD on PBM.numProcurementBudgetId=PBD.numProcurementId Where PBD.tintMonth=' || CAST(EXTRACT(month FROM v_dtFiscalStDate) AS VARCHAR(2)) || ' And PBD.intYear=' || CAST(EXTRACT(year FROM v_dtFiscalStDate) AS VARCHAR(4))
      || ' And PBD.numItemGroupId=IG.numItemGroupID And PBM.numDomainId=' || SUBSTR(CAST(v_numDomainId AS VARCHAR(10)),1,10) || ')'
      || ' as ''' || CAST(EXTRACT(month FROM v_dtFiscalStDate) AS VARCHAR(2)) || '~' || CAST(EXTRACT(year FROM v_dtFiscalStDate) AS VARCHAR(4)) || ''',';
      v_strMarketSQL := coalesce(v_strMarketSQL,'') || '' || ' (Select Sum(monAmount) From MarketBudgetMaster MBM Inner join MarketBudgetDetails MBD on   MBM.numMarketBudgetId=MBD.numMarketId Where MBD.tintMonth=' || CAST(EXTRACT(month FROM v_dtFiscalStDate) AS VARCHAR(2)) || ' And MBD.intYear=' || CAST(EXTRACT(year FROM v_dtFiscalStDate) AS VARCHAR(4))
      || ' And MBD.numListItemID=LD.numListItemID And MBM.numDomainId=' || SUBSTR(CAST(v_numDomainId AS VARCHAR(10)),1,10) || ')'
      || ' as ''' || CAST(EXTRACT(month FROM v_dtFiscalStDate) AS VARCHAR(2)) || '~' || CAST(EXTRACT(year FROM v_dtFiscalStDate) AS VARCHAR(4)) || ''',';
      v_numMonth := v_numMonth+1;
   END LOOP;       
   v_dtFiscalStDate := GetFiscalStartDate(GetFiscalyear(v_Date,1::NUMERIC(9,0))::INTEGER,1::NUMERIC);                  
     
   v_strMonSQL := coalesce(v_strMonSQL,'') || '(  Select Sum(OBD.monAmount)  From OperationBudgetMaster OBM
inner join OperationBudgetDetails OBD on OBM.numBudgetId=OBD.numBudgetId
 Where OBM.numDomainId=' || SUBSTR(CAST(v_numDomainId AS VARCHAR(10)),1,10) || ' And OBD.numChartAcntId=RecordID
   And (((OBD.tintMonth between Month(''' || SUBSTR(CAST(v_dtFiscalStDate AS VARCHAR(100)),1,100) || ''') and 12) and OBD.intYear=Year(''' || SUBSTR(CAST(v_dtFiscalStDate AS VARCHAR(100)),1,100) || ''')) Or ((OBD.tintMonth between 1 and month(''' || SUBSTR(CAST(v_dtFiscalEndDate AS VARCHAR(100)),1,100) || ''')) and OBD.intYear=Year(''' || SUBSTR(CAST(v_dtFiscalEndDate AS VARCHAR(100)),1,100) || '''))))  as Total';                
 

   v_strProcureSQL := coalesce(v_strProcureSQL,'') || '(  Select Sum(PBD.monAmount)  From ProcurementBudgetMaster PBM
inner join ProcurementBudgetDetails PBD on PBM.numProcurementBudgetId=PBD.numProcurementId
 Where PBM.numDomainId=' || SUBSTR(CAST(v_numDomainId AS VARCHAR(10)),1,10) || ' And PBD.numItemGroupId=IG.numItemGroupID  And (((PBD.tintMonth between Month(''' || SUBSTR(CAST(v_dtFiscalStDate AS VARCHAR(100)),1,100) || ''') and 12) and PBD.intYear=Year(''' || SUBSTR(CAST(v_dtFiscalStDate AS VARCHAR(100)),1,100) || ''')) Or ((PBD.tintMonth between 1 and month(''' || SUBSTR(CAST(v_dtFiscalEndDate AS VARCHAR(100)),1,100) || ''')) and PBD.intYear=Year(''' || SUBSTR(CAST(v_dtFiscalEndDate AS VARCHAR(100)),1,100) || '''))))  as Total';                
   v_strMarketSQL := coalesce(v_strMarketSQL,'') || '(  Select Sum(MBD.monAmount)  From MarketBudgetMaster MBM
inner join MarketBudgetDetails MBD on MBM.numMarketBudgetId=MBD.numMarketId
 Where MBM.numDomainId=' || SUBSTR(CAST(v_numDomainId AS VARCHAR(10)),1,10) || ' And MBD.numListItemID=LD.numListItemID  And (((MBD.tintMonth between Month(''' || SUBSTR(CAST(v_dtFiscalStDate AS VARCHAR(100)),1,100) || ''') and 12) and MBD.intYear=Year(''' || SUBSTR(CAST(v_dtFiscalStDate AS VARCHAR(100)),1,100) || ''')) Or ((MBD.tintMonth between 1 and month(''' || SUBSTR(CAST(v_dtFiscalEndDate AS VARCHAR(100)),1,100) || ''')) and MBD.intYear=Year(''' || SUBSTR(CAST(v_dtFiscalEndDate AS VARCHAR(100)),1,100) || '''))))  as Total';                
                                      
   v_strSQL := '  with recursive RecursionCTE(RecordID,ParentRecordID,vcCatgyName,TOC,T)                      
     as(select numAccountId AS RecordID,numParntAcntId AS ParentRecordID,vcCatgyName AS vcCatgyName,CAST('''' AS VARCHAR(1000)) AS TOC,CAST('''' AS VARCHAR(1000)) AS T                      
      from Chart_Of_Accounts                      
     where numParntAcntId is null and numDomainID =' || SUBSTR(CAST(v_numDomainId AS VARCHAR(10)),1,10)
   || ' union all                      
       select R1.numAccountId AS RecordID,R1.numParntAcntId AS ParentRecordID,R1.vcCatgyName AS vcCatgyName,case when OCTET_LENGTH(R2.TOC) > 0 
		then CAST(case when POSITION(''.'' IN R2.TOC) > 0 then R2.TOC else R2.TOC || ''.'' end                      
       || cast(R1.numAccountId as VARCHAR(10)) AS VARCHAR(1000)) else CAST(cast(R1.numAccountId as VARCHAR(10)) AS VARCHAR(1000))                       
         end AS TOC,case when OCTET_LENGTH(R2.TOC) > 0 then  CAST(''&nbsp&nbsp'' || R2.T AS VARCHAR(1000))                      
         else CAST('''' AS VARCHAR(1000)) end AS T  from Chart_Of_Accounts as R1 
        join RecursionCTE as R2 on R1.numParntAcntId = R2.RecordID and R1.numDomainID =' || SUBSTR(CAST(v_numDomainId AS VARCHAR(10)),1,10) || ' and R1.numAccountId in(Select OBD.numChartAcntId From  OperationBudgetMaster OBM                        
      inner join OperationBudgetDetails OBD on OBM.numBudgetId = OBD.numBudgetId                                          
      Where  OBD.intYear =' || SUBSTR(CAST(v_intFiscalYear AS VARCHAR(5)),1,5) ||
   ' And OBM.numDomainId =' || SUBSTR(CAST(v_numDomainId AS VARCHAR(5)),1,5) || '))                       
    select RecordID as numChartAcntId,ParentRecordID as numParentAcntId,T+vcCatgyName as vcCategoryName,TOC,' || SUBSTR(CAST(v_strMonSQL AS VARCHAR(8000)),1,8000) || ' from RecursionCTE  Where ParentRecordID is not null ';                      
 
   v_strSQL := coalesce(v_strSQL,'') || ' union all /** \n **--** \n **/ Select IG.numItemGroupID as numChartAcntId,CAST(null as INTEGER) as numParentAcntId,IG.vcItemGroup as vcCategoryName,CAST(null as INTEGER) as TOC,' || SUBSTR(CAST(v_strProcureSQL AS VARCHAR(8000)),1,8000)
   || ' From ItemGroups IG Where IG.numDomainID =' || SUBSTR(CAST(v_numDomainId AS VARCHAR(10)),1,10);

   v_strSQL := coalesce(v_strSQL,'') || ' union all 
     Select LD.numListItemID as numChartAcntId,CAST(null as INTEGER) as numParentAcntId,LD.vcData as vcCategoryName,CAST(null as INTEGER) as TOC,' || SUBSTR(CAST(v_strMarketSQL AS VARCHAR(8000)),1,8000)
   || ' From ListDetails LD Where LD.numListID = 22 And LD.numDomainID =' || SUBSTR(CAST(v_numDomainId AS VARCHAR(10)),1,10) || ' Order by 4';          
   
   RAISE NOTICE '%',v_strSQL;                                   
   OPEN SWV_RefCur FOR EXECUTE v_strSQL;
   RETURN;
END; $$;


