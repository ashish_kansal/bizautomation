-- Stored procedure definition script usp_DeletePageElement for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION usp_DeletePageElement(v_numPageID INTEGER,
	v_numModuleID INTEGER,
	v_vcParentElementInitiatingAction VARCHAR(20) DEFAULT '',
	v_vcChildElementInitiatingAction VARCHAR(20) DEFAULT ''   
--
)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   DELETE FROM 	PageActionElements
   WHERE numModuleID = v_numModuleID
   AND numPageID = v_numPageID
   AND vcElementInitiatingAction = v_vcParentElementInitiatingAction
   AND vcChildElementInitiatingAction = v_vcChildElementInitiatingAction;
   RETURN;
END; $$;


