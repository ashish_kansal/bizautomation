-- Stored procedure definition script USP_GetOperationBudgetDetails for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_GetOperationBudgetDetails(v_numDomainId NUMERIC(9,0) DEFAULT 0,                                                  
 v_numBudgetId NUMERIC(9,0) DEFAULT 0,                  
 v_intFiscalYear INTEGER DEFAULT 0,       
 v_tintType INTEGER DEFAULT 0, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_strSQL  VARCHAR(8000);                         
   v_strMonSQL  VARCHAR(8000);
   v_numMonth  SMALLINT;       
   v_dtFiscalStDate  TIMESTAMP;                            
   v_dtFiscalEndDate  TIMESTAMP;                 
   v_Date  TIMESTAMP;
BEGIN
   v_strMonSQL := '';                        
   v_numMonth := 1;                                  
 --Select @numMonth=tintFiscalStartMonth From Domain Where numDomainId=@numDomainId                              
   v_Date := TIMEZONE('UTC',now())+CAST(v_tintType || 'year' as interval);            
   RAISE NOTICE '@Date ======%',SUBSTR(CAST(v_Date AS VARCHAR(100)),1,100);          
   While v_numMonth <= 12 LOOP
      v_dtFiscalStDate := GetFiscalStartDate(GetFiscalyear(v_Date,v_numDomainId)::INTEGER,v_numDomainId);
      v_dtFiscalStDate := v_dtFiscalStDate+CAST(v_numMonth -1 || 'month' as interval);
      v_dtFiscalEndDate :=  GetFiscalStartDate(GetFiscalyear(v_Date,v_numDomainId)::INTEGER,v_numDomainId)+INTERVAL '1 year'+INTERVAL '-1 day';
      RAISE NOTICE '%',v_dtFiscalEndDate;
      RAISE NOTICE '%',v_dtFiscalStDate;          
     --Set @strMonSQL=@strMonSQL+' '+  'dbo.fn_GetBudgetMonthDet('+Convert(varchar(2),month(@dtFiscalStDate))+',dbo.GetFiscalyear(getutcdate(),'+convert(varchar(18),@numDomainId)+'),RecordID)' + ' as ''' +Convert(varchar(2),month(@dtFiscalStDate))           
  
       
         
      v_strMonSQL := coalesce(v_strMonSQL,'') || ' ' ||  'dbo.fn_GetBudgetMonthDet(' || SUBSTR(CAST(v_numBudgetId AS VARCHAR(10)),1,10) || ',' || CAST(EXTRACT(month FROM v_dtFiscalStDate) AS VARCHAR(2)) || ',' || CAST(EXTRACT(year FROM v_dtFiscalStDate) AS VARCHAR(4)) || ',' || SUBSTR(CAST(v_numDomainId AS VARCHAR(10)),1,10) || ',RecordID)' || ' as ''' ||
      CAST(EXTRACT(month FROM v_dtFiscalStDate)
      AS VARCHAR(2)) || '~' || CAST(EXTRACT(year FROM v_dtFiscalStDate) AS VARCHAR(4)) || ''',';
      v_numMonth := v_numMonth+1;
   END LOOP;      
   v_dtFiscalStDate := GetFiscalStartDate(GetFiscalyear(v_Date,v_numDomainId)::INTEGER,v_numDomainId);                        
   v_strMonSQL := coalesce(v_strMonSQL,'') || 
-- ' dbo.fn_GetBudgetMonthTotalAmt('+Convert(varchar(10),@numBudgetId)+','''+convert(varchar(500),@Date)+''',RecordID,'+convert(varchar(10),@numDomainId)+') as Total' +      
   '(Select Sum(OBD.monAmount)  From OperationBudgetMaster OBM  
inner join OperationBudgetDetails OBD on OBM.numBudgetId=OBD.numBudgetId  
 Where OBM.numDomainId=' || SUBSTR(CAST(v_numDomainId AS VARCHAR(10)),1,10) || ' And OBD.numBudgetId=' || SUBSTR(CAST(v_numBudgetId AS VARCHAR(10)),1,10) || ' And OBD.numChartAcntId=RecordID  
   And (((OBD.tintMonth between Month(''' || SUBSTR(CAST(v_dtFiscalStDate AS VARCHAR(100)),1,100) || ''') and 12) and OBD.intYear=Year(''' || SUBSTR(CAST(v_dtFiscalStDate AS VARCHAR(100)),1,100) || ''')) Or ((OBD.tintMonth between 1 and month(''' || SUBSTR(CAST(v_dtFiscalEndDate AS VARCHAR(100)),1,100) || '''))
 and OBD.intYear=Year(''' || SUBSTR(CAST(v_dtFiscalEndDate AS VARCHAR(100)),1,100) || '''))))  as Total
,(Select top 1 isnull(vcComments,'''') From OperationBudgetDetails Where numBudgetId=' || SUBSTR(CAST(v_numBudgetId AS VARCHAR(10)),1,10) || ' and tintMonth=Month(''' || SUBSTR(CAST(v_dtFiscalEndDate AS VARCHAR(100)),1,100) || ''') 
And intYear=Year(''' || SUBSTR(CAST(v_dtFiscalEndDate AS VARCHAR(100)),1,100) || ''') And numChartAcntId=RecordID)  as  Comments ';                       
   RAISE NOTICE '%',v_strMonSQL;                                     
   v_strSQL := '  with recursive RecursionCTE(RecordID,ParentRecordID,vcCatgyName,TOC,T)                        
     as                        
     (                        
     select numAccountId AS RecordID,numParntAcntId AS ParentRecordID,vcCatgyName AS vcCatgyName,CAST('''' AS VARCHAR(1000)) AS TOC,CAST('''' AS VARCHAR(1000)) AS T                        
      from Chart_Of_Accounts                        
     where numParntAcntId is null and numDomainID =' || SUBSTR(CAST(v_numDomainId AS VARCHAR(10)),1,10)
   || ' union all                        
       select R1.numAccountId AS RecordID,                        
        R1.numParntAcntId AS ParentRecordID,                        
     R1.vcCatgyName AS vcCatgyName,                        
        case when OCTET_LENGTH(R2.TOC) > 0                        
         then CAST(case when POSITION(''.'' IN R2.TOC) > 0 then R2.TOC else R2.TOC || ''.'' end                        
             || cast(R1.numAccountId as VARCHAR(10)) AS VARCHAR(1000))                         
         else CAST(cast(R1.numAccountId as VARCHAR(10)) AS VARCHAR(1000))                         
         end AS TOC,case when OCTET_LENGTH(R2.TOC) > 0                        
         then  CAST(''&nbsp&nbsp'' || R2.T AS VARCHAR(1000))                        
         else CAST('''' AS VARCHAR(1000))                        
         end AS T                        
       from Chart_Of_Accounts as R1                               
      join RecursionCTE as R2 on R1.numParntAcntId = R2.RecordID and R1.numDomainID =' || SUBSTR(CAST(v_numDomainId AS VARCHAR(10)),1,10) || ' and R1.numAccountId in(Select OBD.numChartAcntId From  OperationBudgetMaster OBM                          
      inner join OperationBudgetDetails OBD on OBM.numBudgetId = OBD.numBudgetId                                            
      Where OBD.numBudgetId =' || SUBSTR(CAST(v_numBudgetId AS VARCHAR(5)),1,5) || 'And OBD.intYear =' || SUBSTR(CAST(v_intFiscalYear AS VARCHAR(5)),1,5) ||
   ' And OBM.numDomainId =' || SUBSTR(CAST(v_numDomainId AS VARCHAR(5)),1,5) || '))                         
    select RecordID as numChartAcntId,ParentRecordID as numParentAcntId,T+vcCatgyName as vcCategoryName,TOC,' || SUBSTR(CAST(v_strMonSQL AS VARCHAR(8000)),1,8000) || ' from RecursionCTE  Where ParentRecordID is not null order by TOC asc';                        
   RAISE NOTICE '%',v_strSQL;                                     
   OPEN SWV_RefCur FOR EXECUTE v_strSQL;
   RETURN;
END; $$;


