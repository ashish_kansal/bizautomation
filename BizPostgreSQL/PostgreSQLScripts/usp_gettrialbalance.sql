-- Stored procedure definition script USP_GetTrialBalance for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_GetTrialBalance(v_numDomainId NUMERIC(9,0),                                                  
v_dtFromDate TIMESTAMP,                                                
v_dtToDate TIMESTAMP,
v_ClientTimeZoneOffset INTEGER, 
v_numAccountClass NUMERIC(9,0) DEFAULT 0,
v_DateFilter VARCHAR(20) DEFAULT NULL,
v_ReportColumn VARCHAR(20) DEFAULT NULL,
v_bitAddTotalRow BOOLEAN DEFAULT NULL, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_PLCHARTID  NUMERIC(18,0) DEFAULT 0;
   v_FinancialYearStartDate  TIMESTAMP;
   v_dtFinYearFromJournal  TIMESTAMP;
   v_dtFinYearFrom  TIMESTAMP;

   v_columns  VARCHAR(8000) DEFAULT '';
   v_PivotColumns  VARCHAR(8000) DEFAULT '';
   v_sql  TEXT DEFAULT '';
   v_monProfitLossAmount  DECIMAL(20,5) DEFAULT 0;
BEGIN
	select   COA.numAccountId INTO v_PLCHARTID FROM Chart_Of_Accounts COA WHERE numDomainId = v_numDomainId AND bitProfitLoss = true;

	v_FinancialYearStartDate := GetFiscalStartDate(CAST(EXTRACT(YEAR FROM LOCALTIMESTAMP) AS INT),v_numDomainId);

	DROP TABLE IF EXISTS tt_TEMPQUARTERGetTrialBalance CASCADE;
	CREATE TEMPORARY TABLE tt_TEMPQUARTERGetTrialBalance
	(
		ID INTEGER,
		StartDate TIMESTAMP,
		EndDate TIMESTAMP
	);

	INSERT INTO tt_TEMPQUARTERGetTrialBalance  VALUES(1,v_FinancialYearStartDate,v_FinancialYearStartDate + make_interval(months => 3) + make_interval(secs => -0.003));
	INSERT INTO tt_TEMPQUARTERGetTrialBalance  VALUES(2,v_FinancialYearStartDate + make_interval(months => 3),v_FinancialYearStartDate + make_interval(months => 6) + make_interval(secs => -0.003));
	INSERT INTO tt_TEMPQUARTERGetTrialBalance  VALUES(3,v_FinancialYearStartDate + make_interval(months => 6),v_FinancialYearStartDate + make_interval(months => 9) + make_interval(secs => -0.003));
	INSERT INTO tt_TEMPQUARTERGetTrialBalance  VALUES(4,v_FinancialYearStartDate + make_interval(months => 9),v_FinancialYearStartDate + make_interval(months => 12) + make_interval(secs => -0.003));

	
	IF v_DateFilter = 'CurYear' then
		v_dtFromDate := v_FinancialYearStartDate;
		v_dtToDate := v_FinancialYearStartDate + make_interval(years => 1) + make_interval(secs => -0.003);
	ELSEIF v_DateFilter = 'PreYear' then
		v_dtFromDate := v_FinancialYearStartDate  + make_interval(years => -1);
		v_dtToDate := v_FinancialYearStartDate  + make_interval(years => -1) + make_interval(years => 1) + make_interval(secs => -0.003);
	ELSEIF v_DateFilter = 'CurPreYear' then
		v_dtFromDate := v_FinancialYearStartDate  + make_interval(years => -1);
		v_dtToDate := v_FinancialYearStartDate  + make_interval(years => 1) + make_interval(secs => -0.003);
	ELSEIF v_DateFilter = 'CuQur' then
		select StartDate, EndDate INTO v_dtFromDate,v_dtToDate FROM tt_TEMPQUARTERGetTrialBalance WHERE LOCALTIMESTAMP BETWEEN StartDate AND EndDate;
	ELSEIF v_DateFilter = 'CurPreQur' then
		select StartDate + make_interval(months => -3), EndDate INTO v_dtFromDate,v_dtToDate FROM tt_TEMPQUARTERGetTrialBalance WHERE LOCALTIMESTAMP BETWEEN StartDate AND EndDate;
	ELSEIF v_DateFilter = 'PreQur' then
		select StartDate + make_interval(months => -3), EndDate + make_interval(months => -3) INTO v_dtFromDate,v_dtToDate FROM tt_TEMPQUARTERGetTrialBalance WHERE LOCALTIMESTAMP BETWEEN StartDate AND EndDate;
	ELSEIF v_DateFilter = 'LastMonth' then
		v_dtFromDate := date_trunc('month', now()::timestamp) + make_interval(months => -1);
		v_dtToDate := date_trunc('month', now()::timestamp) + make_interval(secs => -0.003);
	ELSEIF v_DateFilter = 'ThisMonth' then
		v_dtFromDate := date_trunc('month', now()::timestamp);
		v_dtToDate := date_trunc('month', now()::timestamp) + make_interval(months => 1) + make_interval(secs => -0.003);
	ELSEIF v_DateFilter = 'CurPreMonth' then
		v_dtFromDate := date_trunc('month', now()::timestamp) + make_interval(months => -1);
		v_dtToDate := date_trunc('month', now()::timestamp) + make_interval(months => 1) + make_interval(secs => -0.003);
	end if;

   DROP TABLE IF EXISTS tt_VIEW_JOURNALGetTrialBalance CASCADE;
   CREATE TEMPORARY TABLE tt_VIEW_JOURNALGetTrialBalance
   (
      numAccountId NUMERIC(18,0),
      vcAccountCode VARCHAR(50),
      COAvcAccountCode VARCHAR(100),
      datEntry_Date TIMESTAMP,
      Debit DECIMAL(20,5),
      Credit DECIMAL(20,5)
   );

   INSERT INTO tt_VIEW_JOURNALGetTrialBalance  SELECT numAccountId,vcAccountCode,COAvcAccountCode,datEntry_Date,Debit,Credit FROM VIEW_JOURNAL WHERE numDomainID = v_numDomainId AND (numAccountClass = v_numAccountClass OR v_numAccountClass = 0);

   select   MIN(datentry_date) INTO v_dtFinYearFromJournal FROM tt_VIEW_JOURNALGetTrialBalance;
   SELECT dtPeriodFrom INTO v_dtFinYearFrom FROM FinancialYear WHERE dtPeriodFrom <= v_dtFromDate AND dtPeriodTo >= v_dtFromDate AND numDomainId = v_numDomainId;

	DROP TABLE IF EXISTS tt_TEMPDIRECTREPORTGetTrialBalance CASCADE;
	CREATE TEMPORARY TABLE tt_TEMPDIRECTREPORTGetTrialBalance AS
	WITH RECURSIVE DirectReport AS
	(  
		WITH RECURSIVE DirectReportInternal AS
		(
		SELECT 
			CAST('' AS VARCHAR) AS ParentId,
			CAST(CONCAT(ATD.numAccountTypeID, '#', 0) AS VARCHAR) AS vcCompundParentKey, 
			ATD.numAccountTypeID, 
			CAST(0 AS NUMERIC) AS numAccountID,
			CAST(ATD.vcAccountType AS VARCHAR(300)),
			ATD.vcAccountCode,
			1 AS LEVEL, 
			CAST(CONCAT(TO_CHAR(COALESCE(ATD.tintSortOrder,0), 'FM000'),(CASE vcAccountCode WHEN '0101' THEN '1' WHEN '0102' THEN '2' WHEN '0105' THEN '3' WHEN '0103' THEN '4' WHEN '0106' THEN '5' WHEN '0104' THEN '6' END), ATD.vcAccountCode) AS VARCHAR(300)) AS Struc,
			FALSE AS bitTotal,
			FALSE AS bitIsSubAccount
		FROM 
			AccountTypeDetail AS ATD
		WHERE 
			ATD.numDomainID = v_numdomainid 
			AND ATD.vcAccountCode IN ('0101','0102','0103','0104','0105','0106')
		UNION ALL
		SELECT 
			CAST('' AS VARCHAR) AS ParentId,
			CAST(CONCAT(ATD.numAccountTypeID,'#',0) AS VARCHAR) AS vcCompundParentKey, 
			ATD.numAccountTypeID, 
			CAST(0 AS NUMERIC) AS numAccountID,
			CAST(CONCAT('Total ',ATD.vcAccountType) AS VARCHAR(300)),
			ATD.vcAccountCode,
			1 AS LEVEL, 
			CAST(CONCAT(TO_CHAR(COALESCE(ATD.tintSortOrder,0),'FM000'),(CASE vcAccountCode WHEN '0101' THEN '1' WHEN '0102' THEN '2' WHEN '0105' THEN '3' WHEN '0103' THEN '4' WHEN '0106' THEN '5' WHEN '0104' THEN '6' END),ATD.vcAccountCode,'#Total') AS VARCHAR(300)) AS Struc,
			TRUE AS bitTotal,
			FALSE AS bitIsSubAccount
		FROM 
			AccountTypeDetail AS ATD
		WHERE 
			ATD.numDomainID = v_numdomainid 
			AND ATD.vcAccountCode IN ('0101','0102','0103','0104','0105','0106')
			AND COALESCE(v_bitaddtotalrow, FALSE) = TRUE
		UNION ALL
		SELECT 
			D.vcCompundParentKey AS ParentId, 
            CAST(CONCAT(ATD.numAccountTypeID,'#',0) AS VARCHAR) AS vcCompundParentKey,
            ATD.numAccountTypeID,
            CAST(0 AS NUMERIC) AS numAccountID,
            CAST(ATD.vcAccountType AS VARCHAR(300)) AS vcAccountType,
            ATD.vcAccountCode,
            LEVEL + 1 AS LEVEL, 
            CAST(d.Struc || '#' || CONCAT(TO_CHAR(COALESCE(ATD.tintSortOrder,0), 'FM000'), ATD.vcAccountCode) AS VARCHAR(300)) AS Struc,
            FALSE AS bitTotal,
            FALSE AS bitIsSubAccount
		FROM 
			AccountTypeDetail AS ATD 
		JOIN 
			DirectReportInternal D 
		ON 
			D.numAccountTypeID = ATD.numParentID 
			AND D.bitTotal = FALSE
		WHERE 
			ATD.numDomainID = v_numdomainid 
		)	
		SELECT * FROM DirectReportInternal
		UNION ALL
		SELECT 
			D.vcCompundParentKey AS ParentId, 
			CAST(CONCAT(ATD.numAccountTypeID,'#',0) AS VARCHAR) AS vcCompundParentKey,
			ATD.numAccountTypeID,
			CAST(0 AS NUMERIC) AS numAccountID,
			CAST(CONCAT('Total ',ATD.vcAccountType) AS VARCHAR(300)) AS vcAccountType,
			ATD.vcAccountCode,
			LEVEL + 1 AS LEVEL, 
			CAST(CONCAT(d.Struc,'#',TO_CHAR(COALESCE(ATD.tintSortOrder,0),'FM000'),ATD.vcAccountCode,'#Total') AS VARCHAR(300)) AS Struc,
			TRUE AS bitTotal,
			FALSE AS bitIsSubAccount
        FROM 
			AccountTypeDetail AS ATD 
		JOIN 
			DirectReport D 
		ON 
			D.numAccountTypeID = ATD.numParentID 
			AND D.bitTotal = FALSE
        WHERE 
			ATD.numDomainID = v_numdomainid 
			AND COALESCE(v_bitaddtotalrow, FALSE) = TRUE
	), DirectReport1  AS
	(
		WITH RECURSIVE DirectReport1INTERNAL AS 
		(
			SELECT 
				ParentId, vcCompundParentKey, numAccountTypeID, vcAccountType,vcAccountCode, LEVEL,numAccountId,Struc, false AS bitProfitLoss, bitTotal, bitIsSubAccount
			FROM
				DirectReport
			UNION ALL
			SELECT 
				CAST(CONCAT(COA.numParntAcntTypeId,'#',0) AS VARCHAR) AS ParentId,
				CAST(CONCAT(COA.numParntAcntTypeId,'#',COA.numAccountId) AS VARCHAR) AS vcCompundParentKey,
				CAST(NULL AS NUMERIC(18)) AS numAccountTypeID,
				CAST(vcAccountName AS VARCHAR(300)) AS vcAccountType,
				COA.vcAccountCode,
				LEVEL + 1 AS LEVEL,
				COA.numAccountId, 
				CAST(CONCAT(d.Struc ,'#',COA.numAccountId) AS VARCHAR(300)) AS Struc,
				COA.bitProfitLoss,
				FALSE AS bitTotal,
				COALESCE(COA.bitIsSubAccount, FALSE) AS bitIsSubAccount
			FROM 
				Chart_of_Accounts AS COA 
			JOIN 
				DirectReport1INTERNAL D 
			ON 
				D.numAccountTypeID = COA.numParntAcntTypeId 
				AND D.bitTotal = FALSE
			WHERE 
				COA.numDomainID = v_numdomainid 
				AND COALESCE(COA.bitIsSubAccount, FALSE) = FALSE 
		)
		SELECT 
			ParentId, vcCompundParentKey, numAccountTypeID, vcAccountType,vcAccountCode, LEVEL,numAccountId,Struc,bitProfitLoss,bitTotal,bitIsSubAccount
		FROM
			DirectReport1INTERNAL
		UNION ALL
		SELECT 
			CAST(CONCAT(COA.numParntAcntTypeId,'#',COA.numParentAccId) AS VARCHAR) AS ParentId,
			CAST(CONCAT(COA.numParntAcntTypeId,'#',COA.numAccountId) AS VARCHAR) AS vcCompundParentKey,
			CAST(NULL AS NUMERIC(18)) AS numAccountTypeID,
			CAST(vcAccountName AS VARCHAR(300)) AS vcAccountType,
			COA.vcAccountCode,
			LEVEL + 1 AS LEVEL,
			COA.numAccountId, 
			CAST(d.Struc || '#' || CAST(COA.numAccountId AS VARCHAR) AS VARCHAR(300)) AS Struc,
			COA.bitProfitLoss,
			FALSE AS bitTotal,
			COALESCE(COA.bitIsSubAccount, FALSE) AS bitIsSubAccount
		FROM 
			Chart_of_Accounts AS COA 
		JOIN 
			DirectReport1 D 
		ON 
			D.numAccountId = COA.numParentAccId 
			AND D.bitTotal = FALSE
        WHERE 
			COA.numDomainID = v_numdomainid 
			AND COALESCE(COA.bitIsSubAccount, FALSE) = TRUE
	)
	SELECT 
		ParentId
		,vcCompundParentKey
		,numAccountTypeID
		,vcAccountType
		,LEVEL
		,vcAccountCode
		,numAccountId
		,Struc
		,bitProfitLoss
		,bitTotal
		,bitIsSubAccount 
	FROM 
		DirectReport1;  

	IF v_bitAddTotalRow = true THEN
	
		INSERT INTO tt_TEMPDIRECTREPORTGetTrialBalance
		(
			ParentId,
			vcCompundParentKey,
			numAccountTypeID,
			vcAccountType,
			LEVEL,
			vcAccountCode,
			numAccountId,
			Struc,
			bitTotal
		)
		SELECT
			ParentId,
			vcCompundParentKey,
			numAccountTypeID,
			CONCAT('Total ',vcAccountType),
			LEVEL,
			vcAccountCode,
			numAccountId,
			CONCAT(COA.Struc,'#Total'),
			true
		FROM
			tt_TEMPDIRECTREPORTGetTrialBalance COA
		WHERE
			coalesce(COA.numAccountId,0) > 0
			AND coalesce(COA.bitTotal,false) = false
			AND(SELECT COUNT(*) FROM tt_TEMPDIRECTREPORTGetTrialBalance COAInner WHERE COAInner.Struc ilike COA.Struc || '%') > 1;

		UPDATE 
			tt_TEMPDIRECTREPORTGetTrialBalance DRMain 
		SET 
			vcAccountType = SUBSTR(vcAccountType,7,LENGTH(vcAccountType) -6)  
		WHERE 
			coalesce(DRMain.bitTotal,false) = true 
			AND coalesce(numAccountId,0) = 0 
			AND DRMain.Struc IN (SELECT CONCAT(DRMain1.Struc,'#Total') FROm tt_TEMPDIRECTREPORTGetTrialBalance DRMain1 WHERE coalesce(DRMain1.bitTotal,false) = false AND coalesce(DRMain1.numAccountId,0) = 0 AND(SELECT COUNT(*) FROM tt_TEMPDIRECTREPORTGetTrialBalance DR1 WHERE coalesce(DR1.numAccountId,0) > 0 AND DR1.Struc ilike DRMain1.Struc || '%') = 0);
      
		DELETE FROM tt_TEMPDIRECTREPORTGetTrialBalance DRMain  WHERE coalesce(DRMain.bitTotal,false) = false AND coalesce(numAccountId,0) = 0 AND(SELECT COUNT(*) FROM tt_TEMPDIRECTREPORTGetTrialBalance DR1 WHERE coalesce(DR1.numAccountId,0) > 0 AND DR1.Struc ilike DRMain.Struc || '%') = 0;
	END IF;

   IF v_ReportColumn = 'Year' then
	
      DROP TABLE IF EXISTS tt_TEMPYEARMONTHGetTrialBalance CASCADE;
      CREATE TEMPORARY TABLE tt_TEMPYEARMONTHGetTrialBalance
      (
         intYear INTEGER,
         intMonth INTEGER,
         MonthYear VARCHAR(50),
         StartDate TIMESTAMP,
         EndDate TIMESTAMP,
         ProfitLossAmount DECIMAL(20,5)
      );
      INSERT INTO
      tt_TEMPYEARMONTHGetTrialBalance   with recursive CTE AS(SELECT
      EXTRACT(YEAR FROM v_dtFromDate) AS yr,
				EXTRACT(MONTH FROM v_dtFromDate) AS mm,
				TO_CHAR(v_dtFromDate,'FMMonth') AS mon,
				(TO_CHAR(v_dtFromDate,'FMMon') || '_' || CAST(EXTRACT(YEAR FROM v_dtFromDate) AS VARCHAR(30))) AS MonthYear,
				CAST(make_date(EXTRACT(YEAR FROM v_dtFromDate)::INT,EXTRACT(MONTH FROM v_dtFromDate)::INT,
      1) AS TIMESTAMP) AS dtStartDate,
				CAST(make_date(EXTRACT(YEAR FROM v_dtFromDate)::INT,EXTRACT(MONTH FROM v_dtFromDate)::INT,
      1)+INTERVAL '1 month' AS TIMESTAMP)+INTERVAL '-0.002' AS dtEndDate,
				v_dtFromDate AS new_date
      UNION ALL
      SELECT
      EXTRACT(YEAR FROM new_date+INTERVAL '1 day') AS yr,
				EXTRACT(MONTH FROM new_date+INTERVAL '1 day') AS mm,
				TO_CHAR(new_date+INTERVAL '1 day','FMMonth') AS mon,
				(TO_CHAR(new_date+INTERVAL '1 day','FMMon') || '_' || CAST(EXTRACT(YEAR FROM new_date+INTERVAL '1 day') AS VARCHAR(30))) AS MonthYear,
				CAST(make_date(EXTRACT(YEAR FROM new_date+INTERVAL '1 day')::INT,EXTRACT(MONTH FROM new_date+INTERVAL '1 day')::INT,
      1) AS TIMESTAMP) AS dtStartDate,
				CAST(make_date(EXTRACT(YEAR FROM new_date+INTERVAL '1 day')::INT,EXTRACT(MONTH FROM new_date+INTERVAL '1 day')::INT,
      1)+INTERVAL '1 month' AS TIMESTAMP)+INTERVAL '-0.002' AS dtEndDate,
				new_date+INTERVAL '1 day' AS new_date
      FROM CTE
      WHERE new_date+INTERVAL '1 day' < v_dtToDate) SELECT
      yr,mm,MonthYear,dtStartDate,dtEndDate,0
      FROM
      CTE
      GROUP BY
      yr,mm,MonthYear,dtStartDate,dtEndDate
      ORDER BY
      yr,mm;
      INSERT INTO tt_TEMPYEARMONTHGetTrialBalance  VALUES(5000,5000,CAST('Total' AS VARCHAR(50)),v_dtFromDate,v_dtToDate,0);
		
      UPDATE
      tt_TEMPYEARMONTHGetTrialBalance
      SET
      EndDate =(CASE WHEN EndDate < v_dtToDate THEN EndDate ELSE v_dtToDate END);
      UPDATE
      tt_TEMPYEARMONTHGetTrialBalance
      SET
      ProfitLossAmount =(SELECT coalesce(sum(Credit),0) -coalesce(sum(Debit),0) FROM tt_VIEW_JOURNALGetTrialBalance VJ WHERE numaccountid = v_PLCHARTID AND vcAccountCode ilike '0103%' AND datentry_date between StartDate and EndDate)+(SELECT coalesce(sum(Credit),0) -coalesce(sum(Debit),0) FROM tt_VIEW_JOURNALGetTrialBalance VJ WHERE vcAccountCode ilike '0104%' AND datentry_date between StartDate and EndDate)+(SELECT coalesce(sum(Credit),0) -coalesce(sum(Debit),0) FROM tt_VIEW_JOURNALGetTrialBalance VJ WHERE vcAccountCode ilike '0106%' AND datentry_date between StartDate and EndDate)+(SELECT coalesce(sum(Credit),0) -coalesce(sum(Debit),0) FROM tt_VIEW_JOURNALGetTrialBalance VJ WHERE vcAccountCode ilike '0103%' AND datentry_date between StartDate and EndDate)+(SELECT coalesce(sum(Credit),0) -coalesce(sum(Debit),0) FROM tt_VIEW_JOURNALGetTrialBalance VJ WHERE (vcAccountCode ilike '0103%' OR vcAccountCode ilike '0104%' OR vcAccountCode  ilike '0106%') AND datentry_date <=  StartDate+INTERVAL '-0.003 ')+(SELECT coalesce(sum(Credit),0) -coalesce(sum(Debit),0) FROM tt_VIEW_JOURNALGetTrialBalance VJ WHERE numaccountid = v_PLCHARTID AND datentry_date <=  StartDate+INTERVAL '-0.003 ');
      v_columns := COALESCE((SELECT string_agg('COALESCE(' || MonthYear  || ',0) AS "' || REPLACE(MonthYear,'_',' ') || '"',','  ORDER BY intYear,intMonth) FROM tt_TEMPYEARMONTHGetTrialBalance),'');
      v_PivotColumns := COALESCE((SELECT string_agg(MonthYear,',' ORDER BY intYear,intMonth) FROM tt_TEMPYEARMONTHGetTrialBalance),'');

      DROP TABLE IF EXISTS tt_TEMPVIEWDATAYEARGetTrialBalance CASCADE;
      CREATE TEMPORARY TABLE tt_TEMPVIEWDATAYEARGetTrialBalance AS
         SELECT
         COA.ParentId,
			COA.vcCompundParentKey,
			COA.numAccountTypeID,
			COA.vcAccountType,
			COA.LEVEL,
			COA.vcAccountCode,
			COA.numAccountId,
			COA.Struc,
			COA.bitTotal,
			Period.MonthYear,
			Period.ProfitLossAmount,
			(CASE
         WHEN bitProfitLoss = true
         THEN -Period.ProfitLossAmount
         ELSE(CASE
            WHEN COA.vcAccountCode ilike '0103%' OR COA.vcAccountCode ilike '0104%' OR COA.vcAccountCode ilike '0106%' THEN 0
            ELSE coalesce(t2.OPENING,0)
            END)+(coalesce(Debit,0) -coalesce(Credit,0))
         END) AS Amount
		
         FROM
         tt_TEMPDIRECTREPORTGetTrialBalance COA
         LEFT JOIN LATERAL(SELECT
         MonthYear,
				StartDate,
				EndDate,
				ProfitLossAmount
         FROM
         tt_TEMPYEARMONTHGetTrialBalance) AS Period  on TRUE
         LEFT JOIN LATERAL(SELECT
         SUM(Debit) AS Debit,
				SUM(Credit) AS Credit
         FROM
         tt_VIEW_JOURNALGetTrialBalance V
         WHERE
         V.numaccountid = COA.numAccountID
         AND datentry_date BETWEEN Period.StartDate AND Period.EndDate) AS t1  on TRUE
         LEFT JOIN LATERAL(SELECT
         SUM(Debit -Credit) AS OPENING
         FROM
         tt_VIEW_JOURNALGetTrialBalance VJ
         WHERE
         VJ.numaccountid = COA.numAccountID
         AND datentry_date BETWEEN(CASE WHEN COA.vcAccountCode ilike '0103%' OR COA.vcAccountCode ilike '0104%' OR COA.vcAccountCode ilike '0106%' THEN v_dtFinYearFrom ELSE v_dtFinYearFromJournal END)
         AND  Period.StartDate+INTERVAL '-0.003 ')  AS t2 on TRUE
         WHERE
         coalesce(COA.bitTotal,false) = false;

		 v_sql := CONCAT('SELECT
							ParentId AS "ParentId",
							vcCompundParentKey AS "vcCompundParentKey",
							numAccountId AS "numAccountId",
							numAccountTypeID AS "numAccountTypeID",
							vcAccountType AS "vcAccountType",
							LEVEL AS "LEVEL",
							vcAccountCode AS "vcAccountCode",
							Struc AS "Struc",
							"Type",
							bitTotal AS "bitTotal",',v_columns,' FROM (SELECT ParentId,vcCompundParentKey,numAccountTypeID,vcAccountType,LEVEL,vcAccountCode,numAccountId,Struc,"Type",bitTotal,
											' , v_PivotColumns , '
										FROM 
											crosstab (
												''SELECT 
								(''''#'''' || COA.Struc || ''''#'''') AS row_id,
								COA.ParentId, 
								COA.vcCompundParentKey,
								COA.numAccountId,
								COA.numAccountTypeID, 
								COA.vcAccountType, 
								COA.LEVEL, 
								COA.vcAccountCode, 
								COA.bitTotal,
								(''''#'''' || COA.Struc || ''''#'''') AS Struc,
								(CASE WHEN COALESCE(COA.numAccountId,0) > 0 AND COALESCE(COA.bitTotal,false) = false THEN 1 ELSE 2 END) AS "Type",
								V.MonthYear,
								COALESCE(SUM(Amount),0) AS Amount
							FROM 
								tt_TEMPDIRECTREPORTGetTrialBalance COA 
							LEFT OUTER JOIN 
								tt_TEMPVIEWDATAYEARGetTrialBalance V 
							ON  
								V.Struc iLIKE REPLACE(COA.Struc,''''#Total'''','''''''') || (CASE WHEN ',coalesce(v_bitAddTotalRow,false)::VARCHAR,'=true AND COALESCE(COA.numAccountId,0) > 0 AND COA.bitTotal=false THEN '''''''' ELSE ''''%'''' END)
							GROUP BY 
								COA.ParentId, COA.vcCompundParentKey, COA.numAccountTypeID, COA.vcAccountType, COA.LEVEL, COA.vcAccountCode, COA.numAccountId,COA.Struc,COA.bitProfitLoss,V.MonthYear,V.ProfitLossAmount,COA.bitTotal
							UNION
							SELECT 
								''''-1'''',
								'''''''', 
								''''-1'''',
								NULL,
								NULL,
								''''Total'''',
								-1, 
								NULL, 
								true,
								''''-1'''',
								2,
								MonthYear,
								COALESCE((SELECT 
											COALESCE(SUM(Debit) - SUM(Credit),0)
										FROM 
											VIEW_Journal_Master
										WHERE 
											numDomainID = ',v_numDomainId,'
											AND (AccountCode iLIKE ''''0101%'''' 
											OR AccountCode iLIKE ''''0102%''''
											OR AccountCode iLIKE ''''0103%''''
											OR AccountCode iLIKE ''''0104%''''
											OR AccountCode iLIKE ''''0105%''''
											OR AccountCode iLIKE ''''0106%'''')
											AND (numClassIDDetail=',coalesce(v_numAccountClass,0),' OR ',coalesce(v_numAccountClass,0),' = 0)
											AND datEntry_Date BETWEEN tt_TEMPYEARMONTHGetTrialBalance.StartDate AND tt_TEMPYEARMONTHGetTrialBalance.EndDate),0)
							FROM
								tt_TEMPYEARMONTHGetTrialBalance order by 1'', $q$ values (''' || REPLACE(REPLACE(v_PivotColumns,'"',''),',','''),(''') || ''') $q$
											
											) AS final_result(
											row_id TEXT
											,ParentId TEXT
											,vcCompundParentKey TEXT
											,numAccountId NUMERIC
											,numAccountTypeID NUMERIC
											,vcAccountType TEXT
											,LEVEL INT
											,vcAccountCode TEXT
											,bitTotal BOOLEAN
											,Struc TEXT
											,"Type" SMALLINT, ' || REPLACE(v_PivotColumns,',',' numeric(20,5),') || ' numeric(20,5))) AS TEMP
											ORDER BY (CASE WHEN Struc = ''-1'' THEN ''#9999999999999#'' ELSE Struc END),"Type" DESC');
      
      RAISE NOTICE '%',v_sql;
      OPEN SWV_RefCur FOR EXECUTE v_sql;
   ELSEIF v_ReportColumn = 'Quarter'
   then
	
      DROP TABLE IF EXISTS tt_TEMPYEARMONTHQUARTERGetTrialBalance CASCADE;
      CREATE TEMPORARY TABLE tt_TEMPYEARMONTHQUARTERGetTrialBalance
      (
         intYear INTEGER,
         intQuarter INTEGER,
         MonthYear VARCHAR(50),
         StartDate TIMESTAMP,
         EndDate TIMESTAMP,
         ProfitLossAmount DECIMAL(20,5)
      );
      INSERT INTO
      tt_TEMPYEARMONTHQUARTERGetTrialBalance   with recursive CTE AS(SELECT
      GetFiscalyear(v_dtFromDate,v_numDomainId) AS yr,
				GetFiscalQuarter(v_dtFromDate,v_numDomainId) AS qq,
				('Q' || cast(GetFiscalQuarter(v_dtFromDate,v_numDomainId) AS VARCHAR(30)) || '_' || cast(GetFiscalyear(v_dtFromDate,v_numDomainId) AS VARCHAR(30))) AS MonthYear,
				CASE GetFiscalQuarter(v_dtFromDate,v_numDomainId)
      WHEN 1 THEN GetFiscalStartDate(GetFiscalyear(v_dtFromDate,v_numDomainId)::INTEGER,v_numDomainId)+INTERVAL '0 month'
      WHEN 2 THEN GetFiscalStartDate(GetFiscalyear(v_dtFromDate,v_numDomainId)::INTEGER,v_numDomainId)+INTERVAL '3 month'
      WHEN 3 THEN GetFiscalStartDate(GetFiscalyear(v_dtFromDate,v_numDomainId)::INTEGER,v_numDomainId)+INTERVAL '6 month'
      WHEN 4 THEN GetFiscalStartDate(GetFiscalyear(v_dtFromDate,v_numDomainId)::INTEGER,v_numDomainId)+INTERVAL '9 month'
      END AS dtStartDate,
				CASE GetFiscalQuarter(v_dtFromDate,v_numDomainId)
      WHEN 1 THEN GetFiscalStartDate(GetFiscalyear(v_dtFromDate,v_numDomainId)::INTEGER,v_numDomainId)+INTERVAL '3 month'+INTERVAL '-0.003 '
      WHEN 2 THEN GetFiscalStartDate(GetFiscalyear(v_dtFromDate,v_numDomainId)::INTEGER,v_numDomainId)+INTERVAL '6 month'+INTERVAL '-0.003 '
      WHEN 3 THEN GetFiscalStartDate(GetFiscalyear(v_dtFromDate,v_numDomainId)::INTEGER,v_numDomainId)+INTERVAL '9 month'+INTERVAL '-0.003 '
      WHEN 4 THEN GetFiscalStartDate(GetFiscalyear(v_dtFromDate,v_numDomainId)::INTEGER,v_numDomainId)+INTERVAL '12 month'+INTERVAL '-0.003 '
      END AS dtEndDate,
				v_dtFromDate AS new_date
      UNION ALL
      SELECT
      GetFiscalyear(new_date+INTERVAL '1 day',v_numDomainId) AS yr,
				GetFiscalQuarter(new_date+INTERVAL '1 day',v_numDomainId) AS qq,
				('Q' || cast(GetFiscalQuarter(new_date+INTERVAL '1 day',v_numDomainId) AS VARCHAR(30)) || '_' || cast(GetFiscalyear(new_date+INTERVAL '1 day',v_numDomainId) AS VARCHAR(30))) AS MonthYear,
				CASE GetFiscalQuarter(new_date+INTERVAL '1 day',v_numDomainId)
      WHEN 1 THEN GetFiscalStartDate(GetFiscalyear(new_date+INTERVAL '1 day',v_numDomainId)::INTEGER,v_numDomainId)+INTERVAL '0 month'
      WHEN 2 THEN GetFiscalStartDate(GetFiscalyear(new_date+INTERVAL '1 day',v_numDomainId)::INTEGER,v_numDomainId)+INTERVAL '3 month'
      WHEN 3 THEN GetFiscalStartDate(GetFiscalyear(new_date+INTERVAL '1 day',v_numDomainId)::INTEGER,v_numDomainId)+INTERVAL '6 month'
      WHEN 4 THEN GetFiscalStartDate(GetFiscalyear(new_date+INTERVAL '1 day',v_numDomainId)::INTEGER,v_numDomainId)+INTERVAL '9 month'
      END AS dtStartDate,
				CASE GetFiscalQuarter(new_date+INTERVAL '1 day',v_numDomainId)
      WHEN 1 THEN GetFiscalStartDate(GetFiscalyear(new_date+INTERVAL '1 day',v_numDomainId)::INTEGER,v_numDomainId)+INTERVAL '3 month'+INTERVAL '-0.003 '
      WHEN 2 THEN GetFiscalStartDate(GetFiscalyear(new_date+INTERVAL '1 day',v_numDomainId)::INTEGER,v_numDomainId)+INTERVAL '6 month'+INTERVAL '-0.003 '
      WHEN 3 THEN GetFiscalStartDate(GetFiscalyear(new_date+INTERVAL '1 day',v_numDomainId)::INTEGER,v_numDomainId)+INTERVAL '9 month'+INTERVAL '-0.003 '
      WHEN 4 THEN GetFiscalStartDate(GetFiscalyear(new_date+INTERVAL '1 day',v_numDomainId)::INTEGER,v_numDomainId)+INTERVAL '12 month'+INTERVAL '-0.003 '
      END AS dtEndDate,
				new_date+INTERVAL '1 day' AS new_date
      FROM CTE
      WHERE new_date+INTERVAL '1 day' < v_dtToDate) SELECT
      yr,qq,MonthYear,dtStartDate,dtEndDate,0
      FROM
      CTE
      GROUP BY
      yr,qq,MonthYear,dtStartDate,dtEndDate
      ORDER BY
      yr,qq;
      INSERT INTO tt_TEMPYEARMONTHQUARTERGetTrialBalance  VALUES(5000,5000,CAST('Total' AS VARCHAR(50)),v_dtFromDate,v_dtToDate,0);
		
      UPDATE
      tt_TEMPYEARMONTHQUARTERGetTrialBalance
      SET
      EndDate =(CASE WHEN EndDate < v_dtToDate THEN EndDate ELSE v_dtToDate END);
      UPDATE
      tt_TEMPYEARMONTHQUARTERGetTrialBalance
      SET
      ProfitLossAmount =(SELECT coalesce(sum(Credit),0) -coalesce(sum(Debit),0) FROM tt_VIEW_JOURNALGetTrialBalance VJ WHERE numaccountid = v_PLCHARTID AND vcAccountCode ilike '0103%' AND datentry_date between StartDate and EndDate)+(SELECT coalesce(sum(Credit),0) -coalesce(sum(Debit),0) FROM tt_VIEW_JOURNALGetTrialBalance VJ WHERE vcAccountCode ilike '0104%' AND datentry_date between StartDate and EndDate)+(SELECT coalesce(sum(Credit),0) -coalesce(sum(Debit),0) FROM tt_VIEW_JOURNALGetTrialBalance VJ WHERE vcAccountCode ilike '0106%' AND datentry_date between StartDate and EndDate)+(SELECT coalesce(sum(Credit),0) -coalesce(sum(Debit),0) FROM tt_VIEW_JOURNALGetTrialBalance VJ WHERE vcAccountCode ilike '0103%' AND datentry_date between StartDate and EndDate)+(SELECT coalesce(sum(Credit),0) -coalesce(sum(Debit),0) FROM tt_VIEW_JOURNALGetTrialBalance VJ WHERE (vcAccountCode ilike '0103%' OR vcAccountCode ilike '0104%' OR vcAccountCode  ilike '0106%') AND datentry_date <=  StartDate+INTERVAL '-0.003 ')+(SELECT coalesce(sum(Credit),0) -coalesce(sum(Debit),0) FROM tt_VIEW_JOURNALGetTrialBalance VJ WHERE numaccountid = v_PLCHARTID AND datentry_date <=  StartDate+INTERVAL '-0.003 ');
      v_columns := COALESCE((SELECT string_agg('COALESCE(' || MonthYear  || ',0) AS "' || REPLACE(MonthYear,'_',' ') || '"',',' ORDER BY intYear,intQuarter) FROM tt_TEMPYEARMONTHQUARTERGetTrialBalance));
      v_PivotColumns := COALESCE((SELECT string_agg(MonthYear,',' ORDER BY intYear,intQuarter)  FROM tt_TEMPYEARMONTHQUARTERGetTrialBalance),'');
      DROP TABLE IF EXISTS tt_TEMPVIEWDATAQUARTERGetTrialBalance CASCADE;
      CREATE TEMPORARY TABLE tt_TEMPVIEWDATAQUARTERGetTrialBalance AS
         SELECT
         COA.ParentId,
			COA.vcCompundParentKey,
			COA.numAccountTypeID,
			COA.vcAccountType,
			COA.LEVEL,
			COA.vcAccountCode,
			COA.numAccountId,
			COA.Struc,
			COA.bitTotal,
			Period.MonthYear,
			Period.ProfitLossAmount,
			(CASE
         WHEN bitProfitLoss = true
         THEN -Period.ProfitLossAmount
         ELSE(CASE
            WHEN COA.vcAccountCode ilike '0103%' OR COA.vcAccountCode ilike '0104%' OR COA.vcAccountCode ilike '0106%' THEN 0
            ELSE coalesce(t2.OPENING,0)
            END)+(coalesce(Debit,0) -coalesce(Credit,0))
         END) AS Amount
		
         FROM
         tt_TEMPDIRECTREPORTGetTrialBalance COA
         LEFT JOIN LATERAL(SELECT
         MonthYear,
				StartDate,
				EndDate,
				ProfitLossAmount
         FROM
         tt_TEMPYEARMONTHQUARTERGetTrialBalance) AS Period
         LEFT JOIN LATERAL(SELECT
         SUM(Debit) AS Debit,
				SUM(Credit) AS Credit
         FROM
         tt_VIEW_JOURNALGetTrialBalance V
         WHERE
         V.numaccountid = COA.numAccountID
         AND datentry_date BETWEEN Period.StartDate AND Period.EndDate) AS t1
         LEFT JOIN LATERAL(SELECT
         SUM(Debit -Credit) AS OPENING
         FROM
         tt_VIEW_JOURNALGetTrialBalance VJ
         WHERE
         VJ.numaccountid = COA.numAccountID
         AND datentry_date BETWEEN(CASE WHEN COA.vcAccountCode ilike '0103%' OR COA.vcAccountCode ilike '0104%' OR COA.vcAccountCode ilike '0106%' THEN v_dtFinYearFrom ELSE v_dtFinYearFromJournal END)
         AND  Period.StartDate+INTERVAL '-0.003 ')  AS t2 on TRUE on TRUE on TRUE
         WHERE
         coalesce(COA.bitTotal,false) = false;

		 v_sql := CONCAT('SELECT
							ParentId AS "ParentId",
							vcCompundParentKey AS "vcCompundParentKey",
							numAccountId AS "numAccountId",
							numAccountTypeID AS "numAccountTypeID",
							vcAccountType AS "vcAccountType",
							LEVEL AS "LEVEL",
							vcAccountCode AS "vcAccountCode",
							Struc AS "Struc",
							"Type",
							bitTotal AS "bitTotal",',v_columns,' FROM (SELECT ParentId,vcCompundParentKey,numAccountTypeID,vcAccountType,LEVEL,vcAccountCode,numAccountId,Struc,"Type",bitTotal,
											' , v_PivotColumns , '
										FROM 
											crosstab (
												''SELECT 
								(''''#'''' || COA.Struc || ''''#'''') AS row_id,
								COA.ParentId, 
								COA.vcCompundParentKey,
								COA.numAccountId,
								COA.numAccountTypeID, 
								COA.vcAccountType, 
								COA.LEVEL, 
								COA.vcAccountCode, 
								COA.bitTotal,
								(''''#'''' || COA.Struc || ''''#'''') AS Struc,
								(CASE WHEN COALESCE(COA.numAccountId,0) > 0 AND COALESCE(COA.bitTotal,false) = false THEN 1 ELSE 2 END) AS "Type",
								V.MonthYear,
								COALESCE(SUM(Amount),0) AS Amount
							FROM 
								tt_TEMPDIRECTREPORTGetTrialBalance COA 
							LEFT OUTER JOIN 
								tt_TEMPVIEWDATAQUARTERGetTrialBalance V 
							ON  
								V.Struc iLIKE REPLACE(COA.Struc,''''#Total'''','''''''') || (CASE WHEN ',coalesce(v_bitAddTotalRow,false)::VARCHAR,'=true AND COALESCE(COA.numAccountId,0) > 0 AND COA.bitTotal=false THEN '''''''' ELSE ''''%'''' END)
							GROUP BY 
								COA.ParentId, COA.vcCompundParentKey, COA.numAccountTypeID, COA.vcAccountType, COA.LEVEL, COA.vcAccountCode, COA.numAccountId,COA.Struc,COA.bitProfitLoss,V.MonthYear,V.ProfitLossAmount,COA.bitTotal
							UNION
							SELECT 
								''''-1'''',
								'''''''', 
								''''-1'''',
								NULL,
								NULL,
								''''Total'''',
								-1, 
								NULL, 
								true,
								''''-1'''',
								2,
								MonthYear,
								COALESCE((SELECT 
											COALESCE(SUM(Debit) - SUM(Credit),0)
										FROM 
											VIEW_Journal_Master
										WHERE 
											numDomainID = ',v_numDomainId,'
											AND (AccountCode iLIKE ''''0101%'''' 
											OR AccountCode iLIKE ''''0102%''''
											OR AccountCode iLIKE ''''0103%''''
											OR AccountCode iLIKE ''''0104%''''
											OR AccountCode iLIKE ''''0105%''''
											OR AccountCode iLIKE ''''0106%'''')
											AND (numClassIDDetail=',coalesce(v_numAccountClass,0),' OR ',coalesce(v_numAccountClass,0),' = 0)
											AND datEntry_Date BETWEEN tt_TEMPYEARMONTHQUARTERGetTrialBalance.StartDate AND tt_TEMPYEARMONTHQUARTERGetTrialBalance.EndDate),0)
							FROM
								tt_TEMPYEARMONTHQUARTERGetTrialBalance order by 1'', $q$ values (''' || REPLACE(REPLACE(v_PivotColumns,'"',''),',','''),(''') || ''') $q$
											
											) AS final_result(
											row_id TEXT
											,ParentId TEXT
											,vcCompundParentKey TEXT
											,numAccountId NUMERIC
											,numAccountTypeID NUMERIC
											,vcAccountType TEXT
											,LEVEL INT
											,vcAccountCode TEXT
											,bitTotal BOOLEAN
											,Struc TEXT
											,"Type" SMALLINT, ' || REPLACE(v_PivotColumns,',',' numeric(20,5),') || ' numeric(20,5))) AS TEMP
											ORDER BY (CASE WHEN Struc = ''-1'' THEN ''#9999999999999#'' ELSE Struc END),"Type" DESC');

      RAISE NOTICE '%',v_sql;
      OPEN SWV_RefCur FOR EXECUTE v_sql;
   ELSE
		-- GETTING P&amp;L VALUE
		
      v_monProfitLossAmount :=(SELECT coalesce(sum(Credit),0) -coalesce(sum(Debit),0) FROM tt_VIEW_JOURNALGetTrialBalance VJ WHERE numaccountid = v_PLCHARTID AND vcAccountCode ilike '0103%' AND datentry_date between v_dtFromDate and v_dtToDate)+(SELECT coalesce(sum(Credit),0) -coalesce(sum(Debit),0) FROM tt_VIEW_JOURNALGetTrialBalance VJ WHERE vcAccountCode ilike '0104%' AND datentry_date BETWEEN v_dtFromDate AND v_dtToDate)+(SELECT coalesce(sum(Credit),0) -coalesce(sum(Debit),0) FROM tt_VIEW_JOURNALGetTrialBalance VJ WHERE vcAccountCode ilike '0106%' AND datentry_date BETWEEN v_dtFromDate AND v_dtToDate)+(SELECT coalesce(sum(Credit),0) -coalesce(sum(Debit),0) FROM tt_VIEW_JOURNALGetTrialBalance VJ WHERE vcAccountCode ilike '0103%' AND datentry_date BETWEEN v_dtFromDate AND v_dtToDate)+(SELECT coalesce(sum(Credit),0) -coalesce(sum(Debit),0) FROM tt_VIEW_JOURNALGetTrialBalance VJ WHERE (vcAccountCode ilike '0103%' OR vcAccountCode ilike '0104%' OR vcAccountCode  ilike '0106%') AND datentry_date <=  v_dtFromDate+INTERVAL '-0.003 ')+(SELECT coalesce(sum(Credit),0) -coalesce(sum(Debit),0) FROM tt_VIEW_JOURNALGetTrialBalance VJ WHERE numaccountid = v_PLCHARTID AND datentry_date <=  v_dtFromDate+INTERVAL '-0.003 ');
      DROP TABLE IF EXISTS tt_TEMPVIEWDATAGetTrialBalance CASCADE;
      CREATE TEMPORARY TABLE tt_TEMPVIEWDATAGetTrialBalance ON COMMIT DROP AS
         SELECT
         COA.ParentId,
			COA.numAccountTypeID,
			COA.vcAccountType,
			COA.LEVEL,
			COA.vcAccountCode,
			COA.numAccountId,
			COA.Struc,
			COA.bitTotal,
			(CASE
         WHEN bitProfitLoss = true
         THEN -v_monProfitLossAmount
         ELSE(CASE
            WHEN COA.vcAccountCode ilike '0103%' OR COA.vcAccountCode ilike '0104%' OR COA.vcAccountCode ilike '0106%' THEN 0
            ELSE
               coalesce(t2.OPENING,0)
            END)+(coalesce(Debit,0) -coalesce(Credit,0))
         END) AS Amount
		
         FROM
         tt_TEMPDIRECTREPORTGetTrialBalance COA
         LEFT JOIN LATERAL(SELECT
         SUM(Debit) AS Debit,
				SUM(Credit) AS Credit
         FROM
         tt_VIEW_JOURNALGetTrialBalance V
         WHERE
         V.numaccountid = COA.numAccountID
         AND datentry_date BETWEEN v_dtFromDate AND v_dtToDate) AS t1
         LEFT JOIN LATERAL(SELECT
         SUM(Debit -Credit) AS OPENING
         FROM
         tt_VIEW_JOURNALGetTrialBalance VJ
         WHERE
         VJ.numaccountid = COA.numAccountID
         AND datentry_date BETWEEN(CASE WHEN COA.vcAccountCode ilike '0103%' OR COA.vcAccountCode ilike '0104%' OR COA.vcAccountCode ilike '0106%' THEN v_dtFinYearFrom ELSE v_dtFinYearFromJournal END)
         AND  v_dtFromDate+INTERVAL '-0.003 ')  AS t2 on TRUE on TRUE
         WHERE
         coalesce(COA.bitTotal,false) = false;
      open SWV_RefCur for SELECT * FROM (
      SELECT
      COA.ParentId AS "ParentId",
			COA.vcCompundParentKey AS "vcCompundParentKey",
			COA.numAccountId AS "numAccountId",
			COA.numAccountTypeID AS "numAccountTypeID",
			COA.vcAccountType AS "vcAccountType",
			COA.LEVEL AS "LEVEL",
			COA.vcAccountCode AS "vcAccountCode",
			COA.bitTotal AS "bitTotal",
			('#' || COA.Struc || '#') AS "Struc",
			(CASE WHEN coalesce(COA.numAccountId,0) > 0 AND coalesce(COA.bitTotal,false) = false THEN 1 ELSE 2 END) AS "Type",
			coalesce(SUM(Amount),0) AS "Total"
      FROM
      tt_TEMPDIRECTREPORTGetTrialBalance COA
      LEFT OUTER JOIN
      tt_TEMPVIEWDATAGetTrialBalance V
      ON
      V.Struc ilike REPLACE(COA.Struc,'#Total','') ||(CASE WHEN v_bitAddTotalRow = true AND coalesce(COA.numAccountId,0) > 0 AND COA.bitTotal = false THEN '' ELSE '%' END)
      GROUP BY
      COA.ParentId,COA.vcCompundParentKey,COA.numAccountTypeID,COA.vcAccountType, 
      COA.LEVEL,COA.vcAccountCode,COA.numAccountId,COA.Struc,COA.bitProfitLoss,
      COA.bitTotal
      UNION
      SELECT
      '',
			'-1',
			null,
			-1,
			'Total',
			0,
			null,
			true,
			'-1' AS Struc,
			2,
			coalesce((SELECT
         SUM(Debit) -SUM(Credit)
         FROM
         view_journal_master
         WHERE
         numDomainId = v_numDomainId
         AND (AccountCode ilike '0101%'
         OR AccountCode ilike '0102%'
         OR AccountCode ilike '0103%'
         OR AccountCode ilike '0104%'
         OR AccountCode ilike '0105%'
         OR AccountCode ilike '0106%')
         AND (numClassIDDetail = coalesce(v_numAccountClass,0) OR coalesce(v_numAccountClass,0) = 0)
         AND datEntry_Date BETWEEN v_dtFromDate AND v_dtToDate),0) AS Total ) TEMP
      ORDER BY
      (CASE WHEN "Struc" = '-1' THEN '#99999999#' ELSE "Struc" END),"Type" desc;
   end if;

   DROP TABLE IF EXISTS tt_VIEW_JOURNALGetTrialBalance CASCADE;
   RETURN;
END; $$;

/****** Object:  StoredProcedure [dbo].[USP_InsertIntoEmailHistory]    Script Date: 07/26/2008 16:19:09 ******/



