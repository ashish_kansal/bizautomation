-- Stored procedure definition script usp_ManageDivisionsPopUps for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION usp_ManageDivisionsPopUps(v_numCompanyId NUMERIC DEFAULT 0,
	v_vcCompanyName VARCHAR(100) DEFAULT '',
	v_numDivisionID NUMERIC DEFAULT 0
--
)
RETURNS VOID LANGUAGE plpgsql      
 -- This Procedure will INSERT a new Company OR UPDATE existing Company into the CompanyInfo Table.
-- If inserted, it will return the auto-generated ID for that company.
-- The CompanyID, if passed will ensure that the UPDATE Query is Executed.
-- If the Company ID is not passed, it will execute INSERT Query.
-- Declare the Input Variables.
   AS $$
   DECLARE
   v_strSQL  VARCHAR(1000);	--Declared to use as String for SQL Query.
   v_numCompanyIDInternal  NUMERIC;
   v_extractedNumCompanyid  NUMERIC; 
   v_numGetDivision  NUMERIC;
BEGIN
   v_numGetDivision := 0;
	
   select   max(numCompanyId) INTO v_extractedNumCompanyid from CompanyInfo where  vcCompanyName  = v_vcCompanyName;
   update DivisionMaster set numCompanyID =  v_extractedNumCompanyid where numCompanyID = v_numCompanyId and numDivisionID = v_numDivisionID;
	
   select   count(numDivisionID) INTO v_numGetDivision from DivisionMaster where numCompanyID = v_numCompanyId;
   If v_numGetDivision = 0 then
	
      Delete from CompanyInfo where numCompanyId = v_numCompanyId;
   end if;
   RETURN;
	
	--delete from companyinfo where numcompanyid = @extractedNumCompanyid
	
END; $$;


