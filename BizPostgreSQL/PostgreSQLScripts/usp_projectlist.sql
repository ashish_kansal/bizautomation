-- Stored procedure definition script USP_ProjectList for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ProjectList(v_numUserCntID NUMERIC(9,0) DEFAULT 0,                                    
 v_numDomainID NUMERIC(9,0) DEFAULT 0,                                    
 v_tintUserRightType SMALLINT DEFAULT 0,                                    
 v_tintSortOrder SMALLINT DEFAULT 4,                                          
 v_SortChar CHAR(1) DEFAULT '0',                                    
 v_FirstName VARCHAR(100) DEFAULT '',                                    
 v_LastName VARCHAR(100) DEFAULT '',                                    
 v_CustName VARCHAR(100) DEFAULT '' ,                                    
 v_CurrentPage INTEGER DEFAULT NULL,                                    
 v_PageSize INTEGER DEFAULT NULL,                                    
 INOUT v_TotRecs INTEGER  DEFAULT NULL,                                    
 v_columnName VARCHAR(50) DEFAULT NULL,                                    
 v_columnSortOrder VARCHAR(10) DEFAULT NULL,                                    
 v_numDivisionID NUMERIC(9,0) DEFAULT NULL ,                                   
 v_bitPartner BOOLEAN DEFAULT false,
 v_ClientTimeZoneOffset INTEGER DEFAULT NULL,
 v_numProjectStatus NUMERIC(9,0) DEFAULT NULL,
 v_bitOpenProjects BOOLEAN DEFAULT false,
 v_intMode SMALLINT DEFAULT 0, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_strSql  VARCHAR(8000);                                                            
   v_firstRec  INTEGER;                                     
   v_lastRec  INTEGER;
   v_column  VARCHAR(50);
   v_join  VARCHAR(400);            
   v_lookbckTable  VARCHAR(50);                
   v_strShareRedordWith  VARCHAR(300);
   v_tintOrder  SMALLINT;                                                    
   v_vcFieldName  VARCHAR(50);                                                    
   v_vcListItemType  VARCHAR(3);                                               
   v_vcListItemType1  VARCHAR(1);                                                   
   v_vcAssociatedControlType  VARCHAR(20);                                                    
   v_numListID  NUMERIC(9,0);                                                    
   v_vcDbColumnName  VARCHAR(20);                        
   v_WhereCondition  VARCHAR(2000);                         
   v_vcLookBackTableName  VARCHAR(2000);                  
   v_bitCustom  BOOLEAN;  
   v_bitAllowEdit  CHAR(1);                 
   v_numFieldId  NUMERIC;  
   v_bitAllowSorting  CHAR(1); 
   v_vcColumnName  VARCHAR(500);    
	                  
   v_Nocolumns  SMALLINT;                  
   v_ListRelID  NUMERIC(9,0);
   v_Prefix  VARCHAR(5);
   SWV_RowCount INTEGER;
BEGIN
   IF v_intMode = 0 then
      v_column := v_columnName;
      v_join := '';
      v_lookbckTable := '';
      IF v_column ilike 'CFW.Cust%' then
	
         v_column := 'CFW.Fld_Value';
         v_join := ' left Join CFW_FLD_Values_Pro CFW on CFW.RecId=pro.numProid  and CFW.fld_id= ' || REPLACE(v_columnName,'CFW.Cust','');
      end if;
      IF v_column ilike 'DCust%' then
	
         v_column := 'LstCF.vcData';
         v_join := ' left Join CFW_FLD_Values_Pro CFW on CFW.RecId=pro.numProid and CFW.fld_id= ' || REPLACE(v_columnName,'DCust','');
         v_join := coalesce(v_join,'') || ' left Join ListDetails LstCF on LstCF.numListItemID=CFW.Fld_Value';
      end if;
      v_firstRec :=(v_CurrentPage::bigint -1)*v_PageSize::bigint;
      v_lastRec :=(v_CurrentPage::bigint*v_PageSize::bigint+1);
      v_strShareRedordWith := ' pro.numProId in (select SR.numRecordID FROM ShareRecord SR WHERE SR.numDomainID=' || SUBSTR(CAST(v_numDomainID AS VARCHAR(15)),1,15) || ' AND SR.numModuleID=5 AND SR.numAssignedTo=' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15) || ') ';
      v_strSql := '';
      v_strSql := '
	DECLARE @numDomain NUMERIC
	SET @numDomain = ' || SUBSTR(CAST(v_numDomainID AS VARCHAR(15)),1,15) || ';

	with tblProjects as (SELECT ';
      IF (v_tintSortOrder = 5 OR v_tintSortOrder = 6) then
         v_strSql := coalesce(v_strSql,'') || ' top 20 ';
      end if;
      v_strSql := coalesce(v_strSql,'') || ' ROW_NUMBER() OVER (ORDER BY ' || coalesce(v_column,'') || ' ' || coalesce(v_columnSortOrder,'') || ') AS RowNumber,pro.numProId,
							 COUNT(*) OVER(PARTITION BY NULL) AS TotalRowCount                            
							 FROM ProjectsMaster pro';
      IF v_bitPartner = true then 
         v_strSql := coalesce(v_strSql,'') || ' left join ProjectsContacts ProCont on ProCont.numProId=Pro.numProId         
										   and ProCont.bitPartner=1 and ProCont.numContactId=' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15) || '';
      end if;
      v_strSql := coalesce(v_strSql,'') || ' join additionalContactsinformation addCon on addCon.numContactId = pro.numCustPrjMgr                                              
						  join DivisionMaster Div on Div.numDivisionID=addCon.numDivisionID                                              
						  join CompanyInfo cmp on cmp.numCompanyID=div.numCompanyID 
						  left join ProjectProgress PP on PP.numProID=pro.numProID ' || coalesce(v_join,'');
      v_strSql := coalesce(v_strSql,'') || ' where tintProStatus = 0 and Div.numDomainID=@numDomain ';
      IF v_FirstName <> '' then 
         v_strSql := coalesce(v_strSql,'') || 'and addCon.vcFirstName  like ''' || coalesce(v_FirstName,'') || '%''';
      end if;
      IF v_LastName <> '' then 
         v_strSql := coalesce(v_strSql,'') || 'and addCon.vcLastName like ''' || coalesce(v_LastName,'') || '%''';
      end if;
      IF v_CustName <> '' then 
         v_strSql := coalesce(v_strSql,'') || 'and cmp.vcCompanyName like ''' || coalesce(v_CustName,'') || '%''';
      end if;                                                
	 
	--SET @strSql = @strSql + 'and (isnull(pro.numProjectStatus,0) = '+ convert(varchar(10),@numProjectStatus) + ' OR (' + convert(varchar(10),@numProjectStatus) + ' = 0 and isnull(pro.numProjectStatus,0) <> 27492))'
      v_strSql := coalesce(v_strSql,'') || 'and (isnull(pro.numProjectStatus,0) = ' || SUBSTR(CAST(v_numProjectStatus AS VARCHAR(10)),1,10) || ' OR ' || SUBSTR(CAST(v_numProjectStatus AS VARCHAR(10)),1,10) || '=0)';
      IF v_bitOpenProjects = true then 
         v_strSql := coalesce(v_strSql,'') || 'and isnull(pro.numProjectStatus,0) <> 27492';
      end if;
      IF v_numDivisionID <> 0 then 
         v_strSql := coalesce(v_strSql,'') || ' And div.numDivisionID =' || SUBSTR(CAST(v_numDivisionID AS VARCHAR(15)),1,15);
      end if;
      IF v_SortChar <> '0' then 
         v_strSql := coalesce(v_strSql,'') || ' And Pro.vcProjectName like ''' || coalesce(v_SortChar,'') || '%''';
      end if;
      IF v_tintUserRightType = 1 then 
         v_strSql := coalesce(v_strSql,'') || ' AND (Pro.numRecOwner = ' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15) || ' or Pro.numAssignedTo = ' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15) || ' or  Pro.numProId in (select distinct(numProID) from ProjectsStageDetails         
	where numAssignTo =' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15) || ')' || CASE WHEN v_bitPartner = true THEN ' or ( ProCont.bitPartner=1 and ProCont.numContactId=' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15) || ')' ELSE '' END  || ' or ' || coalesce(v_strShareRedordWith,'') || ')';
      ELSEIF v_tintUserRightType = 2
      then 
         v_strSql := coalesce(v_strSql,'') || ' and  (Div.numTerID in (select numTerritoryID from  UserTerritory where numUserCntID= ' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15) || ' ) or Div.numTerID=0 or Pro.numAssignedTo= ' || SUBSTR(CAST(v_numUserCntID
         AS VARCHAR(15)),1,15)  || ' or  Pro.numProId in (select distinct(numProID) from ProjectsStageDetails                     
	where numAssignTo =' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15) || ')' || ' or ' || coalesce(v_strShareRedordWith,'') || ')';
      end if;
      IF v_tintSortOrder = 1 then  
         v_strSql := coalesce(v_strSql,'') || ' AND (Pro.numRecOwner= ' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15)  || ' or Pro.numAssignedTo= ' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15)  || ' or  Pro.numProId in (select distinct(numProID) from ProjectsStageDetails          
	where numAssignTo =' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15) || ')' || ' or ' || coalesce(v_strShareRedordWith,'') || ')';
      ELSEIF v_tintSortOrder = 2
      then  
         v_strSql := coalesce(v_strSql,'')  || ' AND (Pro.numRecOwner in (select A.numContactID from AdditionalContactsInformation A                                                         
	where A.numManagerID=' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15) || ') or Pro.numAssignedTo in (select A.numContactID from AdditionalContactsInformation A                                                         
	where A.numManagerID=' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15) || ') or  Pro.numProId in (select distinct(numProID) from ProjectsStageDetails                     
	where numAssignTo in (select A.numContactID from AdditionalContactsInformation A                                                         
	where A.numManagerID=' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15) || '))' || ' or ' || coalesce(v_strShareRedordWith,'') || ')';
      ELSEIF v_tintSortOrder = 4
      then  
         v_strSql := coalesce(v_strSql,'') || ' AND Pro.bintCreatedDate > ''' || CAST(TIMEZONE('UTC',now())+INTERVAL '-8 day' AS VARCHAR(20)) || '''';
      ELSEIF v_tintSortOrder = 6
      then  
         v_strSql := coalesce(v_strSql,'') || ' and Pro.numModifiedBy= ' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15);
      ELSEIF v_tintSortOrder = 4
      then  
         v_strSql := coalesce(v_strSql,'') || ' and Pro.numRecOwner= ' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15);
      ELSEIF v_tintSortOrder = 5
      then  
         v_strSql := coalesce(v_strSql,'') || ' and Pro.numRecOwner= ' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15);
      end if;                                             
	 
	--IF @vcRegularSearchCriteria<>'' set @strSql=@strSql+' and ' + @vcRegularSearchCriteria 
	--IF @vcCustomSearchCriteria<>'' set @strSql=@strSql+' and pro.numProid in (select distinct CFW.RecId from CFW_FLD_Values_Pro CFW where ' + @vcCustomSearchCriteria + ')'
	                                                    
      v_strSql := coalesce(v_strSql,'') || ')';
      v_tintOrder := 0;
      v_WhereCondition := '';
      v_Nocolumns := 0;
      select   coalesce(SUM(TotalRow),0) INTO v_Nocolumns FROM(SELECT COUNT(*) AS TotalRow FROM View_DynamicColumns
         WHERE numFormId = 82 
	--AND numUserCntID=@numUserCntID 
         AND numDomainID = v_numDomainID
         AND tintPageType = 1
         UNION
         SELECT COUNT(*) AS TotalRow FROM View_DynamicCustomColumns
         WHERE numFormId = 82 
	--AND numUserCntID=@numUserCntID 
         AND numDomainID = v_numDomainID
         AND tintPageType = 1) TotalRows;
      DROP TABLE IF EXISTS tt_TEMPFORM CASCADE;
      CREATE TEMPORARY TABLE tt_TEMPFORM 
      (
         tintOrder SMALLINT,
         vcDbColumnName VARCHAR(50),
         vcOrigDbColumnName VARCHAR(50),
         vcFieldName VARCHAR(50),
         vcAssociatedControlType VARCHAR(50),
         vcListItemType VARCHAR(3),
         numListID NUMERIC(9,0),
         vcLookBackTableName VARCHAR(50),
         bitCustomField BOOLEAN,
         numFieldId NUMERIC,
         bitAllowSorting BOOLEAN,
         bitAllowEdit BOOLEAN,
         bitIsRequired BOOLEAN,
         bitIsEmail BOOLEAN,
         bitIsAlphaNumeric BOOLEAN,
         bitIsNumeric BOOLEAN,
         bitIsLengthValidation BOOLEAN,
         intMaxLength INTEGER,
         intMinLength INTEGER,
         bitFieldMessage BOOLEAN,
         vcFieldMessage VARCHAR(500),
         ListRelID NUMERIC(9,0),
         intColumnWidth INTEGER,
         bitAllowFiltering BOOLEAN,
         vcFieldDataType CHAR(1)
      );
      v_strSql := coalesce(v_strSql,'') || ' select ADC.numContactId,DM.numDivisionID,isnull(DM.numTerID,0) as numTerID,pro.numCustPrjMgr,DM.tintCRMType,pro.numProId,RowNumber,Pro.numRecOwner';
      IF v_Nocolumns = 0 then
	
         INSERT INTO DycFormConfigurationDetails(numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom,intColumnWidth)
         SELECT 82,numFieldId,0,ROW_NUMBER() OVER(ORDER BY tintRow DESC),v_numDomainID,v_numUserCntID,NULL,1,0,intColumnWidth
         FROM View_DynamicDefaultColumns
         WHERE numFormId = 82 AND bitDefault = 1 AND coalesce(bitSettingField,0) = 1 AND numDomainID = v_numDomainID
         ORDER BY tintOrder ASC;
      end if;
      INSERT INTO tt_TEMPFORM
      SELECT  tintRow+1 AS tintOrder,vcDbColumnName,vcOrigDbColumnName,coalesce(vcCultureFieldName,vcFieldName), vcAssociatedControlType,vcListItemType,
			numListID,vcLookBackTableName ,bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,
			bitIsLengthValidation,intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage AS vcFieldMessage,ListRelID,intColumnWidth,bitAllowFiltering,
			vcFieldDataType
      FROM View_DynamicColumns
      WHERE numFormId = 82 
	--AND numUserCntID=@numUserCntID 
      AND numDomainID = v_numDomainID
      AND tintPageType = 1
      AND coalesce(bitSettingField,0) = 1 AND coalesce(bitCustom,0) = 0
      UNION
      SELECT tintRow+1 AS tintOrder,vcDbColumnName,vcDbColumnName,vcFieldName,vcAssociatedControlType,'' AS vcListItemType,numListID,'',bitCustom,numFieldId,
		   bitAllowSorting,bitAllowEdit,bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,intMaxLength,intMinLength,
		   bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,false,''
      FROM View_DynamicCustomColumns
      WHERE numFormId = 82 
	--AND numUserCntID=@numUserCntID 
      AND numDomainID = v_numDomainID
      AND tintPageType = 1
      AND coalesce(bitCustom,0) = 1
      ORDER BY tintOrder ASC;  
	
	--set @DefaultNocolumns=  @Nocolumns                
	 
	
--	SELECT TOP 1 tintOrder+1,vcDbColumnName,vcFieldName,                
--	  vcAssociatedControlType,vcListItemType,numListID,vcLookBackTableName                                               
--	,bitCustomField,numFieldId,bitAllowSorting,bitAllowEdit,
--	ListRelID
--	  FROM  #tempForm ORDER BY tintOrder ASC 

      select   tintOrder+1, vcDbColumnName, vcFieldName, vcAssociatedControlType, vcListItemType, numListID, vcLookBackTableName, bitCustomField, numFieldId, bitAllowSorting, bitAllowEdit, ListRelID INTO v_tintOrder,v_vcDbColumnName,v_vcFieldName,v_vcAssociatedControlType,v_vcListItemType,
      v_numListID,v_vcLookBackTableName,v_bitCustom,v_numFieldId,
      v_bitAllowSorting,v_bitAllowEdit,v_ListRelID FROM  tt_TEMPFORM    ORDER BY tintOrder ASC LIMIT 1;
      RAISE NOTICE '%',v_tintOrder;
      WHILE v_tintOrder > 0 LOOP                                                      
--	print @vcAssociatedControlType        
--	print @vcFieldName  
--	print @vcDbColumnName 

         IF v_bitCustom = false then
            IF v_vcLookBackTableName = 'AdditionalContactsInformation' then
               v_Prefix := 'ADC.';
            end if;
            IF v_vcLookBackTableName = 'DivisionMaster' then
               v_Prefix := 'DM.';
            end if;
            IF v_vcLookBackTableName = 'ProjectsMaster' then
               v_Prefix := 'Pro.';
            end if;
            v_vcColumnName := coalesce(v_vcDbColumnName,'') || '~' || SUBSTR(CAST(v_numFieldId AS VARCHAR(10)),1,10) || '~0';
            RAISE NOTICE '%',v_vcAssociatedControlType;
            RAISE NOTICE '%',v_Prefix;
            RAISE NOTICE '%',v_vcDbColumnName;
            RAISE NOTICE '%',v_vcColumnName;
            IF v_vcAssociatedControlType = 'SelectBox' then
		 
               IF v_vcListItemType = 'LI' then
		 
                  v_strSql := coalesce(v_strSql,'') || ',L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.vcData' || ' [' || coalesce(v_vcColumnName,'') || ']';
                  v_WhereCondition := coalesce(v_WhereCondition,'') || ' left Join ListDetails L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || ' on L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.numListItemID=' || coalesce(v_vcDbColumnName,'');
               ELSEIF v_vcListItemType = 'S'
               then
		 
                  v_strSql := coalesce(v_strSql,'') || ',S' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.vcState' || ' [' || coalesce(v_vcColumnName,'') || ']';
                  v_WhereCondition := coalesce(v_WhereCondition,'') || ' left join State S' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || ' on S' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.numStateID=' || coalesce(v_vcDbColumnName,'');
               ELSEIF v_vcListItemType = 'PP'
               then
		 
                  v_strSql := coalesce(v_strSql,'') || ',ISNULL(PP.intTotalProgress,0)' || ' [' || coalesce(v_vcColumnName,'') || ']';
                  v_WhereCondition := coalesce(v_WhereCondition,'') || ' LEFT JOIN ProjectProgress PP on Pro.numProID = PP.numProID ';
               ELSEIF v_vcListItemType = 'T'
               then
		 
                  v_strSql := coalesce(v_strSql,'') || ',L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.vcData' || ' [' || coalesce(v_vcColumnName,'') || ']';
                  v_WhereCondition := coalesce(v_WhereCondition,'') || ' left Join ListDetails L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || ' on L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.numListItemID=' || coalesce(v_vcDbColumnName,'');
               ELSEIF v_vcListItemType = 'U'
               then
		 
--			print @vcAssociatedControlType        
--			PRINT @Prefix
--			print @vcDbColumnName 
--			print @vcColumnName 

                  v_strSql := coalesce(v_strSql,'') || ',dbo.fn_GetContactName(' || coalesce(v_Prefix,'') || coalesce(v_vcDbColumnName,'') || ') [' || coalesce(v_vcColumnName,'') || ']';
               end if;
            ELSEIF v_vcAssociatedControlType = 'DateField'
            then
	   
               IF v_vcDbColumnName = 'intDueDate' then
									
                  v_strSql := coalesce(v_strSql,'') || ',case when convert(varchar(11),' || coalesce(v_Prefix,'') || coalesce(v_vcDbColumnName,'') || ' )= convert(varchar(11),getdate()) then ''<b><font color=red>Today</font></b>''';
                  v_strSql := coalesce(v_strSql,'') || 'when convert(varchar(11),' || coalesce(v_Prefix,'') || coalesce(v_vcDbColumnName,'') || '  )= convert(varchar(11),dateadd(day,-1,getdate())) then ''<b><font color=purple>YesterDay</font></b>''';
                  v_strSql := coalesce(v_strSql,'') || 'when convert(varchar(11),' || coalesce(v_Prefix,'') || coalesce(v_vcDbColumnName,'') || '  )= convert(varchar(11),dateadd(day,1,getdate())) then''<b><font color=orange>Tommorow</font></b>'' ';
                  v_strSql := coalesce(v_strSql,'') || 'else dbo.FormatedDateFromDate(' || coalesce(v_Prefix,'') || coalesce(v_vcDbColumnName,'') || ',' || SUBSTR(CAST(v_numDomainID AS VARCHAR(10)),1,10) || ') end  [' || coalesce(v_vcColumnName,'') || ']';
               ELSE
                  v_strSql := coalesce(v_strSql,'') || ',case when convert(varchar(11),DateAdd(minute, ' || CAST(-v_ClientTimeZoneOffset AS VARCHAR(15)) || ',' || coalesce(v_Prefix,'') || coalesce(v_vcDbColumnName,'') || ') )= convert(varchar(11),getdate()) then ''<b><font color=red>Today</font></b>''';
                  v_strSql := coalesce(v_strSql,'') || 'when convert(varchar(11),DateAdd(minute, ' || CAST(-v_ClientTimeZoneOffset AS VARCHAR(15)) || ',' || coalesce(v_Prefix,'') || coalesce(v_vcDbColumnName,'') || ') )= convert(varchar(11),dateadd(day,-1,getdate())) then ''<b><font color=purple>YesterDay</font></b>''';
                  v_strSql := coalesce(v_strSql,'') || 'when convert(varchar(11),DateAdd(minute, ' || CAST(-v_ClientTimeZoneOffset AS VARCHAR(15)) || ',' || coalesce(v_Prefix,'') || coalesce(v_vcDbColumnName,'') || ') )= convert(varchar(11),dateadd(day,1,getdate())) then''<b><font color=orange>Tommorow</font></b>'' ';
                  v_strSql := coalesce(v_strSql,'') || 'else dbo.FormatedDateFromDate(DateAdd(minute, ' || CAST(-v_ClientTimeZoneOffset AS VARCHAR(15)) || ',' || coalesce(v_Prefix,'') || coalesce(v_vcDbColumnName,'') || '),' || SUBSTR(CAST(v_numDomainID AS VARCHAR(10)),1,10) || ') end  [' || coalesce(v_vcColumnName,'') || ']';
               end if;
            ELSEIF v_vcAssociatedControlType = 'TextBox'
            then
	   
               v_strSql := coalesce(v_strSql,'') || ',' || CASE
               WHEN v_vcDbColumnName = 'numAge' THEN 'year(getutcdate())-year(bintDOB)'
               WHEN v_vcDbColumnName = 'numDivisionID' THEN 'DM.numDivisionID'
               WHEN v_vcDbColumnName = 'textSubject' THEN 'convert(varchar(max),textSubject)'
               ELSE coalesce(v_Prefix,'') || coalesce(v_vcDbColumnName,'') END || ' [' || coalesce(v_vcColumnName,'') || ']';
            ELSEIF v_vcAssociatedControlType = 'TextArea'
            then
	
               v_strSql := coalesce(v_strSql,'') || ', '''' ' || ' [' || coalesce(v_vcColumnName,'') || ']';
            ELSEIF v_vcAssociatedControlType = 'CheckBox'
            then
	
               v_strSql := coalesce(v_strSql,'') || ', isnull(' || coalesce(v_vcDbColumnName,'') || ',0) [' || coalesce(v_vcColumnName,'') || ']';
            ELSEIF v_vcAssociatedControlType = 'Popup' AND v_vcDbColumnName = 'numShareWith'
            then
			
               v_strSql := coalesce(v_strSql,'') || ',(SELECT SUBSTRING(
		(SELECT '','' + A.vcFirstName+'' ''+A.vcLastName + CASE WHEN ISNULL(SR.numContactType,0)>0 THEN ''('' + dbo.fn_GetListItemName(SR.numContactType) + '')'' ELSE '''' END
		FROM ShareRecord SR JOIN UserMaster UM ON UM.numUserDetailId=SR.numAssignedTo 
								 JOIN AdditionalContactsInformation A ON UM.numUserDetailId=A.numContactID 
			 WHERE SR.numDomainID=pro.numDomainID AND SR.numModuleID=5 AND SR.numRecordID=pro.numProId
					AND UM.numDomainID=pro.numDomainID and UM.numDomainID=A.numDomainID FOR XML PATH('''')),2,200000)) [' || coalesce(v_vcColumnName,'') || ']';
            end if;
         ELSEIF v_bitCustom = true
         then
	           
	--   select @vcFieldName=FLd_label,@vcAssociatedControlType=fld_type,@vcDbColumnName='Cust'+Convert(varchar(10),Fld_Id)  from CFW_Fld_Master                                           
	--   where CFW_Fld_Master.Fld_Id = @numFieldId                                      
	      
            v_vcColumnName := coalesce(v_vcDbColumnName,'') || '~' || SUBSTR(CAST(v_numFieldId AS VARCHAR(10)),1,10) || '~1';
            IF v_vcAssociatedControlType = 'TextBox' OR v_vcAssociatedControlType = 'TextArea' then
	   
               v_strSql := coalesce(v_strSql,'') || ',CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.Fld_Value  [' || coalesce(v_vcColumnName,'') || ']';
               v_WhereCondition := coalesce(v_WhereCondition,'') || ' left Join CFW_FLD_Values_Pro CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '           
		on CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.Fld_Id=' || SUBSTR(CAST(v_numFieldId AS VARCHAR(10)),1,10) || 'and CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.RecId=pro.numProid   ';
            ELSEIF v_vcAssociatedControlType = 'Checkbox'
            then
	   
               v_strSql := coalesce(v_strSql,'') || ',case when isnull(CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.Fld_Value,0)=0 then ''No'' when isnull(CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.Fld_Value,0)=1 then ''Yes'' end   [' || coalesce(v_vcColumnName,'') || ']';
               v_WhereCondition := coalesce(v_WhereCondition,'') || ' left Join CFW_FLD_Values_Pro CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '           
		on CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.Fld_Id=' || SUBSTR(CAST(v_numFieldId AS VARCHAR(10)),1,10) || 'and CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.RecId=pro.numProid   ';
            ELSEIF v_vcAssociatedControlType = 'DateField'
            then
	   
               v_strSql := coalesce(v_strSql,'') || ',dbo.FormatedDateFromDate(CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.Fld_Value,' || SUBSTR(CAST(v_numDomainID AS VARCHAR(10)),1,10) || ')  [' || coalesce(v_vcColumnName,'') || ']';
               v_WhereCondition := coalesce(v_WhereCondition,'') || ' left Join CFW_FLD_Values_Pro CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '           
		on CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.Fld_Id=' || SUBSTR(CAST(v_numFieldId AS VARCHAR(10)),1,10) || 'and CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.RecId=pro.numProid   ';
            ELSEIF v_vcAssociatedControlType = 'SelectBox'
            then
	   
               v_vcDbColumnName := 'DCust' || SUBSTR(CAST(v_numFieldId AS VARCHAR(10)),1,10);
               v_strSql := coalesce(v_strSql,'') || ',L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.vcData' || ' [' || coalesce(v_vcColumnName,'') || ']';
               v_WhereCondition := coalesce(v_WhereCondition,'') || ' left Join CFW_FLD_Values_Pro CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '           
		 on CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.Fld_Id=' || SUBSTR(CAST(v_numFieldId AS VARCHAR(10)),1,10) || 'and CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.RecId=pro.numProid   ';
               v_WhereCondition := coalesce(v_WhereCondition,'') || ' left Join ListDetails L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || ' on L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.numListItemID=CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.Fld_Value';
            end if;
         end if;
         select   tintOrder+1, vcDbColumnName, vcFieldName, vcAssociatedControlType, vcListItemType, numListID, vcLookBackTableName, bitCustomField, numFieldId, bitAllowSorting, bitAllowEdit, ListRelID INTO v_tintOrder,v_vcDbColumnName,v_vcFieldName,v_vcAssociatedControlType,v_vcListItemType,
         v_numListID,v_vcLookBackTableName,v_bitCustom,v_numFieldId,
         v_bitAllowSorting,v_bitAllowEdit,v_ListRelID FROM tt_TEMPFORM WHERE tintOrder > v_tintOrder -1   ORDER BY tintOrder ASC LIMIT 1;
         GET DIAGNOSTICS SWV_RowCount = ROW_COUNT;
         IF SWV_RowCount = 0 then 
            v_tintOrder := 0;
         end if;
      END LOOP;
      v_strSql := coalesce(v_strSql,'') || ' ,TotalRowCount                                                                  
	FROM ProjectsMaster pro                          
	join additionalContactsinformation ADC                                              
	on ADC.numContactId=pro.numCustPrjMgr                                              
	join DivisionMaster DM                                               
	on DM.numDivisionID=ADC.numDivisionID                                              
	join CompanyInfo cmp                                             
	on cmp.numCompanyID=DM.numCompanyID ' || coalesce(v_WhereCondition,'') ||  ' join tblProjects T on T.numProID=Pro.numProID';
      v_strSql := coalesce(v_strSql,'') || ' WHERE RowNumber > ' || SUBSTR(CAST(v_firstRec AS VARCHAR(10)),1,10) || ' and RowNumber <' || SUBSTR(CAST(v_lastRec AS VARCHAR(10)),1,10) || ' order by RowNumber';
      RAISE NOTICE '%',v_strSql;
      OPEN SWV_RefCur FOR EXECUTE v_strSql;
      open SWV_RefCur for
      SELECT * FROM tt_TEMPFORM;
	--SET @TotRecs = (SELECT COUNT(*) FROM #tempForm)                                    
	
   ELSE
      BEGIN
         CREATE TEMP SEQUENCE tt_tempTable_seq;
         EXCEPTION WHEN OTHERS THEN
            NULL;
      END;
      DROP TABLE IF EXISTS tt_TEMPTABLE CASCADE;
      CREATE TEMPORARY TABLE tt_TEMPTABLE 
      ( 
         ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1) PRIMARY KEY,
         numProID VARCHAR(15),
         Name VARCHAR(100),
         DealID VARCHAR(100),
         numOppID VARCHAR(15),
         numDivisionID VARCHAR(15),
         numCompanyID VARCHAR(100),
         tintCRMType VARCHAR(10),
         numcontactid VARCHAR(15),
         CustJobContact VARCHAR(110),
         LstCmtMilestone  VARCHAR(100),
         lstCompStage VARCHAR(100),
         numTerID VARCHAR(15),
         numRecOwner VARCHAR(15),
         AssignedToBy VARCHAR(50),
         DueDate VARCHAR(50),
         intTotalProgress INTEGER
      );                                    
                                    
                                    
--declare @strSql as varchar(8000)                                                  
      v_strSql := '';
      v_strSql := 'SELECT ';
      IF (v_tintSortOrder = 5 OR v_tintSortOrder = 6) then 
         v_strSql := coalesce(v_strSql,'') || ' top 20 ';
      end if;
      v_strSql := coalesce(v_strSql,'') || ' pro.numProId,                                     
    pro.vcProjectName AS Name,
    dbo.FormatedDateFromDate(pro.intDueDate,pro.numDomainID) AS DueDate, 
    isnull(PP.intTotalProgress,0) intTotalProgress,
    opp.vcPOppName as DealID,                                    
    isnull(opp.numOppId,0) numOppId,                                    
    div.numDivisionID,                                    
    div.numCompanyID,                                    
    div.tintCRMType,                                    
    addCon.numContactID,                                    
    addCon.vcFirstName + '' '' + addCon.vcLastName AS CustJobContact,                                     
    dbo.GetProLstMileStoneCompltd(pro.numProId) as LstCmtMilestone,                                    
    dbo.GetProLstStageCompltd(pro.numProId,' || SUBSTR(CAST(v_ClientTimeZoneOffset AS VARCHAR(10)),1,10) || ') as lstCompStage,                                    
    div.numTerId,                                    
    pro.numRecOwner,                      
     dbo.fn_GetContactName(Pro.numAssignedTo)+''/ ''+   dbo.fn_GetContactName(Pro.numAssignedBy) AssignedToBy                               
   FROM ProjectsMaster pro                                    
   left join  OpportunityMaster opp                                    
   on opp.numOppId=pro.numOppId 
   left join ProjectProgress PP 
   on PP.numProID = pro.numProID ';
      IF v_bitPartner = true then 
         v_strSql := coalesce(v_strSql,'') || ' left join ProjectsContacts ProCont on ProCont.numProId=Pro.numProId and ProCont.bitPartner=1 and ProCont.numContactId=' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15) || '';
      end if;
      v_strSql := coalesce(v_strSql,'') || ' join additionalContactsinformation addCon                                    
   on addCon.numContactId=pro.numCustPrjMgr                                    
  join DivisionMaster Div                                     
   on Div.numDivisionID=addCon.numDivisionID                                    
  join CompanyInfo C                                    
   on C.numCompanyID=div.numCompanyID ';
      v_strSql := coalesce(v_strSql,'') || ' where tintProStatus=0            
  and Div.numDomainID=' || SUBSTR(CAST(v_numDomainID AS VARCHAR(15)),1,15) || '';
      IF v_FirstName <> '' then 
         v_strSql := coalesce(v_strSql,'') || 'and addCon.vcFirstName  like ''' || coalesce(v_FirstName,'') || '%''';
      end if;
      IF v_LastName <> '' then 
         v_strSql := coalesce(v_strSql,'') || 'and addCon.vcLastName like ''' || coalesce(v_LastName,'') || '%''';
      end if;
      IF v_CustName <> '' then 
         v_strSql := coalesce(v_strSql,'') || 'and C.vcCompanyName like ''' || coalesce(v_CustName,'') || '%''';
      end if;
      v_strSql := coalesce(v_strSql,'') || 'and (isnull(pro.numProjectStatus,0) = ' || SUBSTR(CAST(v_numProjectStatus AS VARCHAR(10)),1,10) || ' OR ' || SUBSTR(CAST(v_numProjectStatus AS VARCHAR(10)),1,10) || '=0)';
      IF v_bitOpenProjects = true then 
         v_strSql := coalesce(v_strSql,'') || 'and isnull(pro.numProjectStatus,0) <> 27492';
      end if;
      IF v_numDivisionID <> 0 then 
         v_strSql := coalesce(v_strSql,'') || ' And div.numDivisionID =' || SUBSTR(CAST(v_numDivisionID AS VARCHAR(15)),1,15);
      end if;
      IF v_SortChar <> '0' then 
         v_strSql := coalesce(v_strSql,'') || ' And Pro.vcProjectName like ''' || coalesce(v_SortChar,'') || '%''';
      end if;
      IF v_tintUserRightType = 1 then 
         v_strSql := coalesce(v_strSql,'') || ' AND (Pro.numRecOwner = ' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15) || ' or Pro.numAssignedTo = ' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15) || ' or  Pro.numProId in (select distinct(numProID) from ProjectsStageDetails          
 
where numAssignTo =' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15) || ')' || CASE WHEN v_bitPartner = true THEN ' or ( ProCont.bitPartner=1 and ProCont.numContactId=' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15) || ')' ELSE '' END  || ')';
      ELSEIF v_tintUserRightType = 2
      then 
         v_strSql := coalesce(v_strSql,'') || ' and  (Div.numTerID in (select numTerritoryID from  UserTerritory where numUserCntID= ' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15) || ' ) or Div.numTerID=0 or Pro.numAssignedTo= ' || SUBSTR(CAST(v_numUserCntID
         AS VARCHAR(15)),1,15)  || ' or  Pro.numProId in (select distinct(numProID) from ProjectsStageDetails           
where numAssignTo =' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15) || '))';
      end if;
      IF v_tintSortOrder = 1 then  
         v_strSql := coalesce(v_strSql,'') || ' AND (Pro.numRecOwner= ' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15)  || ' or Pro.numAssignedTo= ' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15)  || ' or  Pro.numProId in (select distinct(numProID) from ProjectsStageDetails          
 
where numAssignTo =' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15) || '))';
      ELSEIF v_tintSortOrder = 2
      then  
         v_strSql := coalesce(v_strSql,'')  || ' AND (Pro.numRecOwner in (select A.numContactID from AdditionalContactsInformation A                                               
where A.numManagerID=' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15) || ') or Pro.numAssignedTo in (select A.numContactID from AdditionalContactsInformation A                                               
where A.numManagerID=' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15) || ') or  Pro.numProId in (select distinct(numProID) from ProjectsStageDetails           
where numAssignTo in (select A.numContactID from AdditionalContactsInformation A                                               
where A.numManagerID=' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15) || ')))';
      ELSEIF v_tintSortOrder = 4
      then  
         v_strSql := coalesce(v_strSql,'') || ' AND Pro.bintCreatedDate > ''' || CAST(TIMEZONE('UTC',now())+INTERVAL '-8 day' AS VARCHAR(20)) || '''';
      ELSEIF v_tintSortOrder = 6
      then  
         v_strSql := coalesce(v_strSql,'') || ' and Pro.numModifiedBy= ' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15) || '  ORDER BY pro.bintmodifieddate desc ';
      end if;
      IF v_tintSortOrder = 1 then  
         v_strSql := coalesce(v_strSql,'') || ' ORDER BY ' || coalesce(v_columnName,'') || ' ' || coalesce(v_columnSortOrder,'');
      ELSEIF v_tintSortOrder = 2
      then  
         v_strSql := coalesce(v_strSql,'') || ' ORDER BY ' || coalesce(v_columnName,'') || ' ' || coalesce(v_columnSortOrder,'');
      ELSEIF v_tintSortOrder = 3
      then  
         v_strSql := coalesce(v_strSql,'') || ' ORDER BY ' || coalesce(v_columnName,'') || ' ' || coalesce(v_columnSortOrder,'');
      ELSEIF v_tintSortOrder = 4
      then  
         v_strSql := coalesce(v_strSql,'') || ' and Pro.numRecOwner= ' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15) || ' ORDER BY ' || coalesce(v_columnName,'') || ' ' || coalesce(v_columnSortOrder,'');
      ELSEIF v_tintSortOrder = 5
      then  
         v_strSql := coalesce(v_strSql,'') || ' and Pro.numRecOwner= ' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15) || ' ORDER BY ' || coalesce(v_columnName,'') || ' ' || coalesce(v_columnSortOrder,'');
      ELSEIF v_tintSortOrder = 6
      then  
         v_strSql := coalesce(v_strSql,'') || ' , ' || coalesce(v_columnName,'') || ' ' || coalesce(v_columnSortOrder,'');
      end if;
      RAISE NOTICE '%',v_strSql;
      EXECUTE 'INSERT INTO tt_TEMPTABLE(                                    
 numProId,                                    
      Name,                                    
      DueDate,
      intTotalProgress,
      DealID,                                    
      numOppId,                                    
 numDivisionID,                                    
 numCompanyID,                                    
 tintCRMType,                                    
 numContactID,                                    
 CustJobContact,                                    
 LstCmtMilestone,                                    
 lstCompStage,                                    
 numTerId,                                    
 numRecOwner,                      
 AssignedToBy)                                    
' || v_strSql;
      v_firstRec :=(v_CurrentPage::bigint -1)*v_PageSize::bigint;
      v_lastRec :=(v_CurrentPage::bigint*v_PageSize::bigint+1);
      open SWV_RefCur for
      SELECT * FROM tt_TEMPTABLE WHERE ID > v_firstRec AND ID < v_lastRec;
      SELECT COUNT(*) INTO v_TotRecs FROM tt_TEMPTABLE;
   end if;
   RETURN;
END; $$; 


