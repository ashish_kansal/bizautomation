CREATE OR REPLACE FUNCTION usp_GetCustomTabLayoutInfoDdl
(
	v_numUserCntId NUMERIC(9,0) DEFAULT 0,                              
	v_intcoulmn NUMERIC(9,0) DEFAULT NULL,                              
	v_numDomainID NUMERIC(9,0) DEFAULT 0 ,                    
	v_PageId NUMERIC(9,0) DEFAULT 4,              
	v_numRelation NUMERIC(9,0) DEFAULT 0,
	v_numFormID NUMERIC(9,0) DEFAULT 0,
	v_tintPageType SMALLINT DEFAULT NULL,
	v_numTabID NUMERIC(9,0) DEFAULT 0, 
	INOUT SWV_RefCur refcursor default null
)
LANGUAGE plpgsql
AS $$
	DECLARE
	v_sql TEXT;
BEGIN
	if(v_intcoulmn <> 0) then--Fields added to layout

		v_sql := 'select 
					fld_label as vcfieldName
					,fld_id as numFieldId
					,DFCD.intRowNum as tintRow
					,true as bitCustomField
				from 
					CFW_Fld_Master CFM 
				join CFw_Grp_Master CGM on cast(NULLIF(CFM.subgrp,'''') as NUMERIC(18,0)) = CGM.Grp_id 
				join DycFormConfigurationDetails DFCD on DFCD.numAuthGroupID = CGM.Grp_id and DFCD.numFieldId = CFM.fld_id';

		IF  v_PageId = 12 or  v_PageId = 13 or  v_PageId = 14 or v_PageId = 4 then
			v_sql := coalesce(v_sql,'') || ' join CFW_Fld_Dtl CFD on CFM.fld_id = CFD.numFieldId';
		end if;

		v_sql := coalesce(v_sql,'') || ' where 
											CGM.numDomainID=' || COALESCE(v_numDomainID,0) ||'
											AND CGM.Grp_id=' || COALESCE(v_numTabID,0) ||'
											AND CFM.numDomainID=' || COALESCE(v_numDomainID,0) ||'
											AND DFCD.numUserCntID=' || COALESCE(v_numUserCntId,0) ||' 
											AND DFCD.numDomainID=' || COALESCE(v_numDomainID,0) ||'
											AND DFCD.numFormId=' || COALESCE(v_numFormID,0) ||' 
											AND DFCD.numRelCntType=' || COALESCE(v_numRelation,0) ||' 
											AND DFCD.tintPageType=' || COALESCE(v_tintPageType,0) || '
											AND DFCD.numAuthGroupID=' || COALESCE(v_numTabID,0) ||'
											and DFCD.intColumnNum = ' || COALESCE(v_intcoulmn,0);

		IF  v_PageId = 12 or  v_PageId = 13 or  v_PageId = 14 then
			v_sql := coalesce(v_sql,'') || ' AND (CFM.grp_id=' || v_PageId ||' or CFM.grp_id=1) and CFD.numRelation=' || COALESCE(v_numRelation,0);
		end if;

		IF  v_PageId = 4 then
			v_sql := coalesce(v_sql,'') || ' AND CFM.grp_id=' || v_PageId ||' and CFD.numRelation=' || COALESCE(v_numRelation,0);
		ELSE
			v_sql := coalesce(v_sql,'') || ' AND CFM.grp_id=' || v_PageId;
		end if;

		v_sql := coalesce(v_sql,'') || ' order by tintRow';
		RAISE NOTICE '%',v_sql;
		OPEN SWV_RefCur FOR EXECUTE v_sql;
   end if;                        
                        
                          
   if v_intcoulmn = 0 then --Available Fields
      v_sql := 'select fld_label as vcfieldName ,fld_id as numFieldId,1 as bitCustomField
from CFW_Fld_Master CFM join CFw_Grp_Master CGM on cast(NULLIF(CFM.subgrp,'''') as NUMERIC(18,0)) = CGM.Grp_id ';
      IF  v_PageId = 12 or  v_PageId = 13 or  v_PageId = 14 or v_PageId = 4 then

         v_sql := coalesce(v_sql,'') || ' join CFW_Fld_Dtl CFD on CFM.Fld_id=CFD.numFieldId ';
      end if;
      v_sql := coalesce(v_sql,'') || ' where CGM.numDomainID=' || COALESCE(v_numDomainID,0) ||' and CGM.Grp_id=' || COALESCE(v_numTabID,0) ||' and CFM.numDomainID=' || COALESCE(v_numDomainID,0) ||' 
AND fld_id not IN (SELECT numFieldId FROM DycFormConfigurationDetails where           
 numUserCntID=' || COALESCE(v_numUserCntId,0) ||' and numDomainID=' || COALESCE(v_numDomainID,0) ||'  
 AND numFormId=' || COALESCE(v_numFormID,0) ||'  and COALESCE(bitCustom,false)=true and numRelCntType=' || COALESCE(v_numRelation,0) ||'AND tintPageType=' || COALESCE(v_tintPageType,0) || '
AND numAuthGroupID=' || COALESCE(v_numTabID,0) ||')';

      IF  v_PageId = 12 or  v_PageId = 13 or  v_PageId = 14 then
         v_sql := coalesce(v_sql,'') || ' AND (CFM.grp_id=' || v_PageId ||' or CFM.grp_id=1) and CFD.numRelation=' || COALESCE(v_numRelation,0);
      end if;
      IF  v_PageId = 4 then

         v_sql := coalesce(v_sql,'') || ' AND CFM.grp_id=' || v_PageId ||' and CFD.numRelation=' || COALESCE(v_numRelation,0);
      ELSE
         v_sql := coalesce(v_sql,'') || ' AND CFM.grp_id=' || v_PageId ||'';
      end if;
      v_sql := coalesce(v_sql,'') || ' order by subgrp';
      RAISE NOTICE '%',v_sql;
      OPEN SWV_RefCur FOR EXECUTE v_sql;
   end if;
   RETURN;
END; $$;


