-- Stored procedure definition script Usp_GetHelpByPageUrl for PostgreSQL
CREATE OR REPLACE FUNCTION Usp_GetHelpByPageUrl(v_pageUrl VARCHAR(100),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for select * from HelpMaster WHERE helpLinkingPageURL ilike '%' || coalesce(v_pageUrl,'') || '%' LIMIT 1;-- helpPageUrl=@pageUrl      
END; $$; 
-- exec USP_GetHelpCategories @numHelpCategoryID=3,@tintMode=0












