-- Stored procedure definition script usp_GenDocList for PostgreSQL
CREATE OR REPLACE FUNCTION usp_GenDocList(v_numDomainID NUMERIC(18,0) DEFAULT 0,           
	v_numUserCntID NUMERIC(18,0) DEFAULT 0,	                   
	v_tintUserRightType SMALLINT DEFAULT 0,
	v_CurrentPage INTEGER DEFAULT NULL,
	v_PageSize INTEGER DEFAULT NULL,
	v_columnName VARCHAR(50) DEFAULT NULL,
	v_columnSortOrder VARCHAR(10) DEFAULT NULL,
	v_DocCategory NUMERIC(18,0) DEFAULT 0,
	v_numDocStatus NUMERIC(18,0) DEFAULT 0,
	v_ClientTimeZoneOffset INTEGER DEFAULT NULL,
	v_numGenericDocId NUMERIC(18,0) DEFAULT 0,
	v_SortChar CHAR(1) DEFAULT '0',
	v_tintDocumentType SMALLINT DEFAULT NULL,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   IF v_columnSortOrder <> 'Desc' AND v_columnSortOrder <> 'Asc' then
	
      v_columnSortOrder := 'Asc';
   end if;

   open SWV_RefCur for SELECT
   COUNT(*) OVER() AS numTotalRecords,
		cast(numGenericDocId as VARCHAR(255)),
		cast(VcFileName as VARCHAR(255)) ,
		cast(vcDocName as VARCHAR(255)) ,
		cast(numDocCategory as VARCHAR(255)),
		(CASE numDocCategory
   WHEN 369 THEN CONCAT('Email Template',' <font color=green>(',coalesce(EMM.vcModuleName,'-'),
      ')</font> ')
   ELSE fn_GetListItemName(numDocCategory)
   END) AS Category,
		cast(vcfiletype as VARCHAR(255)),
		cast(cUrlType as VARCHAR(255)),
		fn_GetListItemName(numDocStatus) as BusClass,
		fn_GetContactName(numModifiedBy) || ',' || CAST(bintModifiedDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) AS VARCHAR(20)) as Mod
   FROM
   GenericDocuments
   LEFT JOIN
   EmailMergeModule EMM
   ON
   EMM.numModuleID = GenericDocuments.numModuleId
   AND EMM.tintModuleType = 0
   WHERE
   numDomainID = v_numDomainID
   AND (coalesce(v_numGenericDocId,0) = 0 OR numGenericDocId = v_numGenericDocId)
   AND tintDocumentType = 1
   AND (coalesce(v_numDocStatus,0) = 0 OR numDocStatus = v_numDocStatus)
   AND (coalesce(v_DocCategory,0) = 0 OR numDocCategory = v_DocCategory)
   AND 1 =(CASE WHEN v_tintUserRightType = 1 THEN(CASE WHEN numCreatedby = v_numUserCntID THEN 1 ELSE 0 END) ELSE 1 END)
   AND 1 =(CASE WHEN v_SortChar <> '0' THEN(CASE WHEN vcDocName ilike CONCAT(v_SortChar,'%') THEN 1 ELSE 0 END) ELSE 1 END)
   AND 1 =(CASE v_tintDocumentType
   WHEN 2 THEN(CASE WHEN coalesce(VcFileName,'') ilike '#SYS#%' THEN 1 ELSE 0 END)
   WHEN 1 THEN(CASE WHEN coalesce(VcFileName,'') NOT ilike '#SYS#%' THEN 1 ELSE 0 END)
   ELSE 1
   END)
   ORDER BY
   CASE
   WHEN v_columnSortOrder = 'Desc' AND v_columnName <> 'Mod' THEN
      CASE v_columnName
      WHEN 'vcDocName' THEN vcDocName
      WHEN 'Category' THEN(CASE numDocCategory
         WHEN 369 THEN CONCAT('Email Template',' <font color=green>(',coalesce(EMM.vcModuleName,'-'),
            ')</font> ')
         ELSE fn_GetListItemName(numDocCategory)
         END)
      WHEN 'vcfiletype' THEN vcfiletype
      WHEN 'BusClass' THEN fn_GetListItemName(numDocStatus)
      END
   END DESC,CASE v_columnSortOrder WHEN 'Desc' THEN
      CASE v_columnName
      WHEN 'Mod' THEN bintModifiedDate
      ELSE bintcreateddate
      END
   END DESC,CASE WHEN v_columnSortOrder = 'Asc' AND  v_columnName <> 'Mod' THEN
      CASE v_columnName
      WHEN 'vcDocName' THEN vcDocName
      WHEN 'Category' THEN(CASE numDocCategory
         WHEN 369 THEN CONCAT('Email Template',' <font color=green>(',coalesce(EMM.vcModuleName,'-'),
            ')</font> ')
         ELSE fn_GetListItemName(numDocCategory)
         END)
      WHEN 'vcfiletype' THEN vcfiletype
      WHEN 'BusClass' THEN fn_GetListItemName(numDocStatus)
      END
   END ASC,
   CASE v_columnSortOrder WHEN 'Asc' THEN
      CASE v_columnName
      WHEN 'Mod' THEN bintModifiedDate
      ELSE bintcreateddate
      END
   END ASC;
END; $$;












