DROP FUNCTION IF EXISTS USP_GetPriceTable;

CREATE OR REPLACE FUNCTION USP_GetPriceTable(v_numPriceRuleID NUMERIC(18,0),
    v_numItemCode NUMERIC(18,0) 
	,v_numDomainID NUMERIC(18,0)
	,v_numCurrencyID NUMERIC(18,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for
   with  PricingCte AS(SELECT  pt.numPricingID,
		pt.numPriceRuleID,
		pt.intFromQty,
		pt.intToQty,
		pt.tintRuleType,
		pt.tintVendorCostType,
		pt.tintDiscountType,
		CAST(pt.decDiscount AS DECIMAL(18,4)) AS decDiscount,
		ROW_NUMBER() OVER(ORDER BY numPricingID) as rowId
   FROM    PricingTable pt
   WHERE   coalesce(numPriceRuleID,0) = v_numPriceRuleID AND coalesce(numItemCode,0) = v_numItemCode AND coalesce(numCurrencyID,0) = v_numCurrencyID)
   SELECT  numPricingID
	,numPriceRuleID
	,intFromQty
	,intToQty
	,tintRuleType
	,tintVendorCostType
	,tintDiscountType
	,decDiscount
	,coalesce(NULLIF(pnt.vcPriceLevelName,''),CONCAT('Price Level ',rowId)) AS vcName
   FROM PricingCte
   LEFT JOIN PricingNamesTable pnt ON pnt.tintPriceLevel = PricingCte.rowId AND pnt.numDomainID = v_numDomainID
   WHERE rowId IS NOT NULL OR (pnt.vcPriceLevelName IS NOT NULL AND pnt.vcPriceLevelName <> '')
   ORDER BY numPricingID;
END; $$;












