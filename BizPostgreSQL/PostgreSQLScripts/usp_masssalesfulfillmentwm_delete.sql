-- Stored procedure definition script USP_MassSalesFulfillmentWM_Delete for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_MassSalesFulfillmentWM_Delete(v_numDomainID NUMERIC(18,0)
	,v_numID NUMERIC(18,0))
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   DELETE FROM MassSalesFulfillmentWM WHERE numDomainID = v_numDomainID AND ID = v_numID;
   RETURN;
END; $$;


