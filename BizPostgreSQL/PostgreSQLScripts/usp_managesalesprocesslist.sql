-- Stored procedure definition script USP_ManageSalesprocessList for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ManageSalesprocessList(v_numSalesProsID NUMERIC(9,0) DEFAULT 0,
    v_vcSlpName VARCHAR(50) DEFAULT '',
    v_numUserCntID NUMERIC(9,0) DEFAULT 0,
    v_tintProType SMALLINT DEFAULT NULL,
    v_numDomainID NUMERIC(9,0) DEFAULT 0,
    v_numStageNumber NUMERIC DEFAULT 0,
	v_bitAssigntoTeams BOOLEAN DEFAULT false,
	v_bitAutomaticStartTimer BOOLEAN DEFAULT false,
	v_numOppId NUMERIC(18,0) DEFAULT 0,
	v_numTaskValidatorId NUMERIC(18,0) DEFAULT 0,
	v_numBuildManager NUMERIC(18,0) DEFAULT 0, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
BEGIN
   IF v_numSalesProsID = 0 then
    
      INSERT  INTO Sales_process_List_Master(Slp_Name,
                                                 numDomainID,
                                                 Pro_Type,
                                                 numCreatedby,
                                                 dtCreatedon,
                                                 numModifedby,
                                                 dtModifiedOn,tintConfiguration,bitAutomaticStartTimer,bitAssigntoTeams,numOppId,numTaskValidatorId,numBuildManager)
        VALUES(v_vcSlpName,
                  v_numDomainID,
                  v_tintProType,
                  v_numUserCntID,
                  TIMEZONE('UTC',now()),
                  v_numUserCntID,
                  TIMEZONE('UTC',now()),v_numStageNumber,v_bitAutomaticStartTimer,v_bitAssigntoTeams,v_numOppId,v_numTaskValidatorId,v_numBuildManager);
        
      v_numSalesProsID := CURRVAL('Sales_process_List_Master_seq');
      open SWV_RefCur for
      SELECT  v_numSalesProsID;
   ELSE
      IF(v_numOppId > 0) then
		
         UPDATE  Sales_process_List_Master
         SET     Slp_Name = v_vcSlpName
         WHERE   Slp_Id = v_numSalesProsID;
         open SWV_RefCur for
         SELECT  v_numSalesProsID;
      ELSE
         UPDATE  Sales_process_List_Master
         SET     Slp_Name = v_vcSlpName,numModifedby = v_numUserCntID,dtModifiedOn = TIMEZONE('UTC',now()),
         bitAssigntoTeams = v_bitAssigntoTeams,bitAutomaticStartTimer = v_bitAutomaticStartTimer,
         numTaskValidatorId = v_numTaskValidatorId,
         numBuildManager = v_numBuildManager
         WHERE   Slp_Id = v_numSalesProsID;
         open SWV_RefCur for
         SELECT  v_numSalesProsID;
      end if;
   end if;
   RETURN;
END; $$;


