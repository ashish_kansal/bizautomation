-- Stored procedure definition script USP_ConEmpListFromTerr for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ConEmpListFromTerr(v_numDomainID NUMERIC(9,0) DEFAULT 0,  
v_bitPartner BOOLEAN DEFAULT false,  
v_numContactID NUMERIC(9,0) DEFAULT 0,
v_numTerID NUMERIC(9,0) DEFAULT 0, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
BEGIN
   IF v_bitPartner = false then
	
      open SWV_RefCur for
      SELECT
      A.numContactId
			,CONCAT(A.vcFirstName,' ',A.vcLastname) as vcUserName
			,coalesce(AGM.tintGroupType,0) AS tintGroupType
      FROM
      UserMaster UM
      INNER JOIN
      AuthenticationGroupMaster AGM
      ON
      UM.numGroupID = AGM.numGroupID
      INNER JOIN
      AdditionalContactsInformation A
      ON
      UM.numUserDetailId = A.numContactId
      WHERE
      UM.numDomainID = v_numDomainID
      AND UM.intAssociate = 1
      AND A.numContactId in(select numUserCntID from UserTerritory where numTerritoryID = v_numTerID and numDomainID = v_numDomainID)
      UNION
      SELECT
      A.numContactId
			,vcCompanyName || ' - ' || A.vcFirstName || ' ' || A.vcLastname AS vcUserName
			,0 AS tintGroupType
      FROM AdditionalContactsInformation A
      join DivisionMaster D
      on D.numDivisionID = A.numDivisionId
      join ExtarnetAccounts E
      on E.numDivisionID = D.numDivisionID
      join ExtranetAccountsDtl DTL
      on DTL.numExtranetID = E.numExtranetID
      join CompanyInfo C
      on C.numCompanyId = D.numCompanyID
      WHERE
      A.numDomainID = v_numDomainID
      AND D.numDivisionID <>(select numDivisionId from Domain where numDomainId = v_numDomainID) and  D.numTerID = v_numTerID;
   end if;  
   IF v_bitPartner = true then
	
      open SWV_RefCur for
      SELECT
      A.numContactId
			,A.vcFirstName || ' ' || A.vcLastname AS vcUserName
			,coalesce(AGM.tintGroupType,0) AS tintGroupType
      FROM
      UserMaster UM
      INNER JOIN
      AuthenticationGroupMaster AGM
      ON
      UM.numGroupID = AGM.numGroupID
      JOIN
      AdditionalContactsInformation A
      ON
      UM.numUserDetailId = A.numContactId
      WHERE
      UM.numDomainID = v_numDomainID
      AND UM.intAssociate = 1
      AND A.numContactId IN(SELECT numUserCntID FROM UserTerritory WHERE numTerritoryID = v_numTerID AND numDomainID = v_numDomainID)
      UNION
      SELECT
      A.numContactId
			,vcCompanyName || ' - ' || A.vcFirstName || ' ' || A.vcLastname as vcUserName
			,0 AS tintGroupType
      FROM AdditionalContactsInformation A
      join DivisionMaster D
      on D.numDivisionID = A.numDivisionId
      join CompanyInfo C
      on C.numCompanyId = D.numCompanyID
      WHERE
      A.numContactId = v_numContactID
      AND D.numTerID = v_numTerID
      AND D.numDivisionID <>(SELECT numDivisionId FROM Domain WHERE numDomainId = v_numDomainID);
   end if;
   RETURN;
END; $$;


