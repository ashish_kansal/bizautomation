CREATE OR REPLACE FUNCTION USP_DeleteNews(v_numNewsID NUMERIC(9,0) DEFAULT 0)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   delete from news where numNewsID = v_numNewsID;
   RETURN;
END; $$;


