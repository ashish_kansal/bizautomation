-- Stored procedure definition script USP_AddressDetails_UpdateRecordID for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_AddressDetails_UpdateRecordID(v_numDomainID NUMERIC(18,0)
	,v_numAddressID NUMERIC(18,0)
	,v_numRecordID NUMERIC(18,0))
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   UPDATE
   AddressDetails
   SET
   numRecordID = v_numRecordID
   WHERE
   numDomainID = v_numDomainID
   AND numAddressID = v_numAddressID;
   RETURN;
END; $$;


