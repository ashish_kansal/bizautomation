-- Stored procedure definition script usp_GetSearchableColumns4SearchRecords for PostgreSQL
CREATE OR REPLACE FUNCTION usp_GetSearchableColumns4SearchRecords(INOUT SWV_RefCur refcursor)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT * FROM DynamicAdvSearchView WHERE 1 = 2;
END; $$;












