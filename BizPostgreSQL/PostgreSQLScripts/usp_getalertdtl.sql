-- Stored procedure definition script USP_GetAlertDTL for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetAlertDTL(v_numAlertDTLID NUMERIC(9,0) DEFAULT 0,
v_numDomainId NUMERIC(9,0) DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for select * from AlertDTL HDR
   join AlertDomainDtl DTL
   on HDR.numAlertDTLid = DTL.numAlertDTLid
   where HDR.numAlertDTLid = v_numAlertDTLID and numDomainID = v_numDomainId;
END; $$;












