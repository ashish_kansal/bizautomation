-- Stored procedure definition script USP_BizDocTemplate_ManageLogo for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
-- =============================================  
-- Author:  <Author,,Sachin Sadhu>  
-- Create date: <Create Date,,23rdDec2013>  
-- Description: <Description,,To Save Logo in BizDocTemplate Table>  
-- =============================================  
CREATE OR REPLACE FUNCTION USP_BizDocTemplate_ManageLogo(v_numBizDocTempID NUMERIC(18,0) ,
      v_numDomainId NUMERIC(9,0) DEFAULT 0 ,
      v_vcImagePath VARCHAR(100) DEFAULT '' ,
      v_sintbyteMode SMALLINT DEFAULT 0)
RETURNS VOID LANGUAGE plpgsql   
 -- Add the parameters for the stored procedure here  
   AS $$
BEGIN
  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
  
          
    
   IF v_sintbyteMode = 0 then
      UPDATE  BizDocTemplate
      SET     vcBizDocImagePath = v_vcImagePath
      WHERE   numDomainID = v_numDomainId
      AND numBizDocTempID = v_numBizDocTempID;
   ELSEIF v_sintbyteMode = 1
   then
      UPDATE  BizDocTemplate
      SET     vcBizDocImagePath = v_vcImagePath
      WHERE   numDomainID = v_numDomainId
      AND numBizDocTempID = v_numBizDocTempID;
   end if;
   RETURN;
END; $$;  



