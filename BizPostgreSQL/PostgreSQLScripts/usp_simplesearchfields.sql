-- Stored procedure definition script USP_SimpleSearchfields for PostgreSQL
CREATE OR REPLACE FUNCTION USP_SimpleSearchfields(v_numGroupID NUMERIC(9,0) DEFAULT 0,      
v_numDomainID NUMERIC(9,0) DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for select CAST(numFieldID AS VARCHAR(15)) || '~' || vcDbColumnName || '~' || vcAssociatedControlType
   || '~' || vcFieldType || '~' || CAST(numListID AS VARCHAR(15)) || '~' || vcListItemType as "ID",vcFieldName as "vcFieldName",tintRow as "intRownum"
   from View_DynamicColumns
   where numFormId = 6  and numAuthGroupId = v_numGroupID and coalesce(bitCustom,false) = false and numDomainID = v_numDomainID
   union
   select  CAST(numFieldId AS VARCHAR(15)) || '~' || vcFieldName || '~' || vcAssociatedControlType
   || '~' || vcFieldType || '~' || CAST(coalesce(numListID,0) AS VARCHAR(15)) || '~L' as "ID",vcFieldName as "vcFieldName",tintRow as "intRownum "      -- 'C' to vcFieldType changed by chitnan to fix GIS bug in simple search
   from View_DynamicCustomColumns
   where numFormId = 6  AND numAuthGroupID = v_numGroupID AND coalesce(bitCustom,false) = true
   AND numDomainID = v_numDomainID order by "intRownum";
END; $$; 
 
 












