DROP FUNCTION IF EXISTS fn_GetPOItemVendorLeadTimesDays;

CREATE OR REPLACE FUNCTION fn_GetPOItemVendorLeadTimesDays
(
	p_numDomainID NUMERIC(18,0)
	,p_numDivisionID NUMERIC(18,0)
	,p_numQty NUMERIC
	,p_numItemGroupID NUMERIC(18,0)
	,p_numShipVia NUMERIC(18,0)
	,p_numShippingService NUMERIC(18,0)
	,p_numShipFromAddressID NUMERIC(18,0)
	,p_numShipToCountry NUMERIC(18,0)
	,p_numShipToState NUMERIC(18,0)
)
RETURNS INT LANGUAGE plpgsql
   AS $$
BEGIN
	RETURN (SELECT 
				(VLT.vcdata->'DaysToArrive')::INT
			FROM 
				VendorLeadTimes VLT
			INNER JOIN
				DivisionMaster DM
			ON
				VLT.numdivisionid=DM.numdivisionid
			WHERE
				DM.numdomainid=p_numdomainid
				AND VLT.numdivisionid = p_numDivisionID
				AND p_numQty BETWEEN (VLT.vcdata->'FromQuantity')::NUMERIC AND (VLT.vcdata->'ToQuantity')::NUMERIC
				AND EXISTS (SELECT * FROM jsonb_array_elements(vcdata->'ItemGroups') WHERE value IS NOT NULL AND value::NUMERIC = p_numItemGroupID)
				AND (VLT.vcdata->'ShipVia')::NUMERIC = p_numShipVia
				AND EXISTS (SELECT * FROM jsonb_array_elements(vcdata->'ShippingServices') WHERE value IS NOT NULL AND value::NUMERIC = p_numShippingService)
				AND EXISTS (SELECT * FROM jsonb_array_elements(vcdata->'ShipFromLocations') WHERE value IS NOT NULL AND value::NUMERIC = p_numShipFromAddressID)
				AND (VLT.vcdata->'ShipToCountry')::NUMERIC = p_numShipToCountry
				AND ((SELECT COUNT(*) FROM jsonb_array_elements(vcdata->'ShipToStates')) = 0 OR EXISTS (SELECT * FROM jsonb_array_elements(vcdata->'ShipToStates') WHERE value IS NULL OR value::NUMERIC = p_numShipToState))
			ORDER BY
				numvendorleadtimesid DESC
			LIMIT 1);
END; $$;

