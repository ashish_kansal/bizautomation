-- Function definition script GetItemAffectedList for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION GetItemAffectedList(v_numRuleID NUMERIC,
      v_tinType SMALLINT,
      v_numDomainID NUMERIC(10,0))
RETURNS TEXT LANGUAGE plpgsql
   AS $$
   DECLARE
   v_ItemList  VARCHAR(2000);
BEGIN
   IF v_tinType = 1 then
				
      select string_agg(RTRIM(LTRIM(I.vcItemName)),',') INTO v_ItemList FROM    PromotionOfferItems POI
      INNER JOIN Item I ON I.numItemCode = POI.numValue
      LEFT OUTER JOIN ShippingLabelRuleMaster SR ON SR.numShippingRuleID = POI.numProId AND I.numDomainID = SR.numDomainID WHERE   SR.intItemAffected = v_tinType
      AND SR.numShippingRuleID = v_numRuleID
      AND SR.numDomainID = v_numDomainID
      AND POI.tintType = 1
      AND POI.tintRecordType = 3;
   ELSEIF v_tinType = 2
   then
				
      select string_agg(RTRIM(LTRIM(LD.vcData)),',') INTO v_ItemList FROM    PromotionOfferItems POI
      INNER JOIN Listdetails LD ON LD.numListItemID = POI.numValue
      LEFT OUTER JOIN ShippingLabelRuleMaster SR ON SR.numShippingRuleID = POI.numProId AND LD.numDomainid = SR.numDomainID WHERE   SR.intItemAffected = v_tinType
      AND SR.numShippingRuleID = v_numRuleID
      AND SR.numDomainID = v_numDomainID
      AND POI.tintType = 3
      AND POI.tintRecordType = 3;
   end if;
                  
   RETURN coalesce(v_ItemList,'');
END; $$;

