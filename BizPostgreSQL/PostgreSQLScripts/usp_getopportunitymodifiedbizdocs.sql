-- Stored procedure definition script USP_GetOpportunityModifiedBizDocs for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetOpportunityModifiedBizDocs(v_numBizDocTempID NUMERIC(18,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT  numoppid, numOppBizDocsId FROM OpportunityBizDocs
   WHERE numBizDocTempID = v_numBizDocTempID
   ORDER BY numOppBizDocsId DESC LIMIT 1;
END; $$;












