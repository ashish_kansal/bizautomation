-- Stored procedure definition script USP_CompanyInfo_SearchByName for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
-- =============================================
-- Author:		Sandeep Patel
-- Create date: 11 DEC 2019
-- Description:	Search customer
-- =============================================
CREATE OR REPLACE FUNCTION USP_CompanyInfo_SearchByName(v_numDomainID NUMERIC(18,0),
    v_str VARCHAR(1000),
    v_numPageIndex INTEGER,
    v_numPageSize INTEGER,
	INOUT v_numTotalRecords INTEGER , INOUT SWV_RefCur refcursor)
LANGUAGE plpgsql
   AS $$
BEGIN
   select   COUNT(*) INTO v_numTotalRecords FROM
   CompanyInfo
   INNER JOIN
   DivisionMaster
   ON
   CompanyInfo.numCompanyId = DivisionMaster.numCompanyID WHERE
   CompanyInfo.numDomainID = v_numDomainID
   AND DivisionMaster.numDomainID = v_numDomainID
   AND vcCompanyName ilike CONCAT('%',v_str,'%');

   open SWV_RefCur for
   SELECT
   numDivisionID AS id
		,vcCompanyName AS text
   FROM
   CompanyInfo
   INNER JOIN
   DivisionMaster
   ON
   CompanyInfo.numCompanyId = DivisionMaster.numCompanyID
   WHERE
   CompanyInfo.numDomainID = v_numDomainID
   AND DivisionMaster.numDomainID = v_numDomainID
   AND vcCompanyName ilike CONCAT('%',v_str,'%')
   ORDER BY
   vcCompanyName;
   RETURN;
END; $$;


