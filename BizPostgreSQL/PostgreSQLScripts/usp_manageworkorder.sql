-- Stored procedure definition script USP_ManageWorkOrder for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ManageWorkOrder(v_numItemCode NUMERIC(18,0),
v_numWareHouseItemID NUMERIC(18,0),
v_Units INTEGER,
v_numDomainID NUMERIC(18,0),
v_numUserCntID NUMERIC(18,0),
v_vcInstruction VARCHAR(1000),
v_bintCompliationDate TIMESTAMP,
v_numAssignedTo NUMERIC(9,0),
v_numSalesOrderId NUMERIC(18,0) DEFAULT 0,
v_numBuildProcessId NUMERIC(18,0) DEFAULT 0,
v_dtmProjectFinishDate TIMESTAMP DEFAULT NULL,
v_dtmEndDate TIMESTAMP DEFAULT NULL,
v_dtmStartDate TIMESTAMP DEFAULT NULL)
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numWarehouseID  NUMERIC(18,0);
   v_numWOId  NUMERIC(9,0);
   v_numDivisionId  NUMERIC(9,0); 
   v_txtItemDesc  VARCHAR(1000);
   v_vcItemName  VARCHAR(300);
	
   v_Description  VARCHAR(1000);
   my_ex_state TEXT;
	my_ex_message TEXT;
	my_ex_detail TEXT;
	my_ex_hint TEXT;
	my_ex_ctx TEXT;
   v_numNewProcessId  NUMERIC(18,0) DEFAULT 0;

   SWV_RCur REFCURSOR;
   SWV_RCur2 REFCURSOR;
BEGIN
   -- BEGIN TRAN
v_numWarehouseID := coalesce((SELECT numWareHouseID FROM WareHouseItems WHERE numWareHouseItemID = v_numWareHouseItemID),0);
   IF EXISTS(SELECT
   Item.numItemCode
   FROM
   Item
   INNER JOIN
   ItemDetails Dtl
   ON
   numChildItemID = numItemCode
   WHERE
   numItemKitID = v_numItemCode
   AND charItemType = 'P'
   AND NOT EXISTS(SELECT numWareHouseItemID FROM WareHouseItems WHERE numItemID = Item.numItemCode AND numWareHouseID = v_numWarehouseID)) then
	
      RAISE EXCEPTION 'SELECTED_WAREHOUSE_IS_NOT_AVAILABLE_FOR_ALL_CHILD_ITEMS';
      RETURN;
   end if;
   select   coalesce(txtItemDesc,''), coalesce(vcItemName,'') INTO v_txtItemDesc,v_vcItemName FROM Item WHERE numItemCode = v_numItemCode;
   IF coalesce(v_numAssignedTo,0) = 0 then
	
      v_numAssignedTo := coalesce((SELECT numBuildManager FROM Sales_process_List_Master WHERE Slp_Id = v_numBuildProcessId),0);
   end if;
   INSERT INTO WorkOrder(numItemCode,numQtyItemsReq,numWareHouseItemId,numCreatedBy,
		bintCreatedDate,numDomainId,numWOStatus,vcItemDesc,vcInstruction,
		bintCompliationDate,numAssignedTo,numOppId,numBuildProcessId,dtmStartDate,dtmEndDate)
	VALUES(v_numItemCode,v_Units,v_numWareHouseItemID,v_numUserCntID,
		TIMEZONE('UTC',now()),v_numDomainID,0,v_txtItemDesc,v_vcInstruction,
		v_bintCompliationDate,v_numAssignedTo,v_numSalesOrderId,v_numBuildProcessId,v_dtmStartDate,v_dtmEndDate) RETURNING numWOId INTO v_numWOId;
	
   PERFORM USP_UpdateNameTemplateValueForWorkOrder(3::SMALLINT,v_numDomainID,v_numWOId);
   IF(v_numBuildProcessId > 0) then
      INSERT  INTO Sales_process_List_Master(Slp_Name,
                                                 numDomainID,
                                                 Pro_Type,
                                                 numCreatedby,
                                                 dtCreatedon,
                                                 numModifedby,
                                                 dtModifiedOn,tintConfiguration,bitAutomaticStartTimer,bitAssigntoTeams,numOppId,numTaskValidatorId,
												 numProjectId,numWorkOrderId)
      SELECT Slp_Name,
				numDomainID,
				Pro_Type,
				numCreatedby,
				dtCreatedon,
				numModifedby,
				dtModifiedOn,tintConfiguration,bitAutomaticStartTimer,bitAssigntoTeams,0,numTaskValidatorId,0,v_numWOId FROM Sales_process_List_Master WHERE Slp_Id = v_numBuildProcessId RETURNING slp_id INTO v_numNewProcessId;
       
      UPDATE WorkOrder SET numBuildProcessId = v_numNewProcessId WHERE numWOId = v_numWOId;
      INSERT INTO StagePercentageDetails(numStagePercentageId,
			tintConfiguration,
			vcStageName,
			numdomainid,
			numCreatedBy,
			bintCreatedDate,
			numModifiedBy,
			bintModifiedDate,
			slp_id,
			numAssignTo,
			vcMileStoneName,
			tintPercentage,
			tinProgressPercentage,
			dtStartDate,
			numParentStageID,
			intDueDays,
			numProjectid,
			numOppid,
			vcDescription,
			bitIsDueDaysUsed,
			numTeamId,
			bitRunningDynamicMode,
			numStageOrder,
			numWorkOrderId)
      SELECT
      numStagePercentageId,
			tintConfiguration,
			vcStageName,
			numdomainid,
			v_numUserCntID,
			LOCALTIMESTAMP,
			v_numUserCntID,
			LOCALTIMESTAMP,
			v_numNewProcessId,
			numAssignTo,
			vcMileStoneName,
			tintPercentage,
			tinProgressPercentage,
			LOCALTIMESTAMP,
			numParentStageID,
			intDueDays,
			0,
			0,
			vcDescription,
			bitIsDueDaysUsed,
			numTeamId,
			bitRunningDynamicMode,
			numStageOrder,
			v_numWOId
      FROM
      StagePercentageDetails
      WHERE
      slp_id = v_numBuildProcessId;
      INSERT INTO StagePercentageDetailsTask(numStageDetailsId,
			vcTaskName,
			numHours,
			numMinutes,
			numAssignTo,
			numDomainID,
			numCreatedBy,
			dtmCreatedOn,
			numOppId,
			numProjectId,
			numParentTaskId,
			bitDefaultTask,
			bitSavedTask,
			numOrder,
			numReferenceTaskId,
			numWorkOrderId)
      SELECT(SELECT  SFD.numStageDetailsId FROM StagePercentageDetails AS SFD WHERE SFD.vcStageName = SP.vcStageName AND SFD.numWorkOrderId = v_numWOId LIMIT 1),
			vcTaskName,
			numHours,
			numMinutes,
			ST.numAssignTo,
			v_numDomainID,
			v_numUserCntID,
			LOCALTIMESTAMP,
			0,
			0,
			0,
			true,
			CASE WHEN SP.bitRunningDynamicMode = true THEN false ELSE true END,
			numOrder,
			numTaskId,
			v_numWOId
      FROM
      StagePercentageDetailsTask AS ST
      LEFT JOIN
      StagePercentageDetails As SP
      ON
      ST.numStageDetailsId = SP.numStageDetailsId
      WHERE
      coalesce(ST.numOppId,0) = 0 AND coalesce(ST.numProjectId,0) = 0 AND
      ST.numStageDetailsId IN(SELECT numStageDetailsId FROM
         StagePercentageDetails As ST
         WHERE
         ST.slp_id = v_numBuildProcessId)
      ORDER BY ST.numOrder;
   end if;
   INSERT INTO WorkOrderDetails(numWOId,numItemKitID,numChildItemID,numQtyItemsReq,numWarehouseItemID,vcItemDesc,sintOrder,numQtyItemsReq_Orig,numUOMId)
   SELECT
   v_numWOId,numItemKitID,numItemCode,
		CAST(((DTL.numQtyItemsReq*coalesce(fn_UOMConversion(DTL.numUOMID,Item.numItemCode,Item.numDomainID,Item.numBaseUnit),1))*v_Units::bigint) AS DOUBLE PRECISION),
		coalesce((SELECT  numWareHouseItemID FROM WareHouseItems WHERE numItemID = Item.numItemCode AND numWareHouseID = v_numWarehouseID ORDER BY coalesce(numWLocationID,0),numWareHouseItemID LIMIT 1),0),
		coalesce(DTL.vcItemDesc,txtItemDesc),
		coalesce(sintOrder,0),
		DTL.numQtyItemsReq,
		DTL.numUOMID
   FROM
   Item
   INNER JOIN
   ItemDetails DTL
   ON
   numChildItemID = numItemCode
   WHERE
   numItemKitID = v_numItemCode;

	--UPDATE ON ORDER OF ASSEMBLY
   UPDATE
   WareHouseItems
   SET
   numonOrder = coalesce(numonOrder,0)+v_Units,dtModified = LOCALTIMESTAMP
   WHERE
   numWareHouseItemID = v_numWareHouseItemID; 
	
	--MAKE ENTRY IN WAREHOUSE TRACKING TABLE
	 --  numeric(9, 0)
	 --  numeric(9, 0)
	 --  tinyint
	 --  varchar(100)
   v_Description := CONCAT('Work Order Created (Qty:',v_Units,')');
   PERFORM USP_ManageWareHouseItems_Tracking(v_numWareHouseItemID := v_numWareHouseItemID,v_numReferenceID := v_numWOId,
   v_tintRefType := 2::SMALLINT,v_vcDescription := v_Description,v_numModifiedBy := v_numUserCntID,
   v_numDomainID := v_numDomainID,SWV_RefCur := null);
   PERFORM USP_ManageInventoryWorkOrder(v_numWOId,v_numDomainID,v_numUserCntID);
   -- COMMIT
EXCEPTION WHEN OTHERS THEN
     GET STACKED DIAGNOSTICS
			  my_ex_state   = RETURNED_SQLSTATE,
			  my_ex_message = MESSAGE_TEXT,
			  my_ex_detail  = PG_EXCEPTION_DETAIL,
			  my_ex_hint    = PG_EXCEPTION_HINT,
			  my_ex_ctx     = PG_EXCEPTION_CONTEXT;

			 RAISE EXCEPTION '% % % % %', my_ex_state, my_ex_message, my_ex_detail, my_ex_hint, my_ex_ctx;
END; $$;



