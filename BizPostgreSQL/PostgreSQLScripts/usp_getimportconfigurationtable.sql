-- Stored procedure definition script USP_GetImportConfigurationTable for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetImportConfigurationTable(v_numRelation NUMERIC,          
v_numDomain NUMERIC,
v_ImportType SMALLINT,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   if(select count(*) from RecImportConfg where numDomainID = v_numDomain and numRelationShip = v_numRelation and Importtype = v_ImportType) = 0 then
      v_numDomain := 0;
   end if;           
            
      
   open SWV_RefCur for select HDR.vcFormFieldName,DTL.numFormFieldId   as numFormFieldId,intcolumn,bitCustomFld,cCtype,vcAssociatedControlType,vcDbColumnName,coalesce(HDR.numListID,0) AS numListID
   from RecImportConfg DTL
   join DynamicFormFieldMaster HDR on DTL.numFormFieldId = HDR.numFormFieldId
   where DTL.numDomainID = v_numDomain and DTL.numRelationShip = v_numRelation and bitCustomFld = false   and ImportType = v_ImportType
   union
   select CFW.FLd_label as vcFormFieldName,DTL.numFormFieldId  as numFormFieldId,intcolumn,bitCustomFld,cCtype,fld_type as vcAssociatedControlType,'a' as vcDbColumnName ,coalesce(CFW.numlistid,0) AS numListID
   from RecImportConfg DTL
   join CFW_Fld_Master CFW on DTL.numFormFieldId = CFW.Fld_id
   where DTL.numDomainID = v_numDomain and DTL.numRelationShip = v_numRelation and bitCustomFld = true  and ImportType = v_ImportType
   order by 3;
END; $$;
