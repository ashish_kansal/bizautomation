-- Stored procedure definition script USP_Item_GetChildMembership for PostgreSQL
CREATE OR REPLACE FUNCTION USP_Item_GetChildMembership(v_numDomainID NUMERIC(18,0),
      v_numItemCode NUMERIC(18,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT
		Item.numItemCode,
		Item.vcItemName,
		CASE
   WHEN coalesce(Item.bitAssembly,false) = true THEN 'Assembly'
   WHEN coalesce(Item.bitKitParent,false) = true THEN 'Kit'
   END AS ParentItemType,
		ItemDetails.numQtyItemsReq AS numQtyItemsReq
   FROM
   ItemDetails
   INNER JOIN
   Item
   ON
   ItemDetails.numItemKitID = Item.numItemCode
   WHERE
   numChildItemID = v_numItemCode
   UNION ALL
   SELECT
   Item.numItemCode,
		ItemGroups.vcItemGroup AS vcItemName,
		'Item Group' AS ParentItemType,
		0 AS numQtyItemsReq
   FROM
   Item
   INNER JOIN
   ItemGroups
   ON
   Item.numItemGroup = ItemGroups.numItemGroupID
   WHERE
   Item.numDomainID = v_numDomainID
   AND numItemCode = v_numItemCode
   UNION ALL
   SELECT
   Item.numItemCode,
		Item.vcItemName,
		'Related Items' AS ParentItemType,
		0 AS numQtyItemsReq
   FROM
   SimilarItems
   INNER JOIN
   Item
   ON
   SimilarItems.numParentItemCode = Item.numItemCode
   WHERE
   SimilarItems.numDomainId = v_numDomainID
   AND SimilarItems.numItemCode = v_numItemCode;
   RETURN;
END; $$;
