-- Stored procedure definition script USP_BroadCastList for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_BroadCastList(v_numUserCntID NUMERIC(9,0) DEFAULT 0,          
 v_numDomainID NUMERIC(9,0) DEFAULT 0,           
 v_CurrentPage INTEGER DEFAULT NULL,          
 v_PageSize INTEGER DEFAULT NULL,          
 INOUT v_TotRecs INTEGER  DEFAULT NULL,          
 v_columnName VARCHAR(50) DEFAULT NULL,          
 v_columnSortOrder VARCHAR(10) DEFAULT NULL, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_strSql  VARCHAR(8000);          
          
          
   v_firstRec  INTEGER;           
   v_lastRec  INTEGER;
BEGIN
   BEGIN
      CREATE TEMP SEQUENCE tt_tempTable_seq;
      EXCEPTION WHEN OTHERS THEN
         NULL;
   END;
   drop table IF EXISTS tt_TEMPTABLE CASCADE;
   create TEMPORARY TABLE tt_TEMPTABLE 
   ( 
      ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1) PRIMARY KEY,
      numBroadCastID NUMERIC(18,0),
      vcBroadCastName VARCHAR(100),
      numBroadCastBy VARCHAR(100),
      bintBroadCastDate TIMESTAMP,
      numTotalRecipients NUMERIC(18,0),
      UnsucessFull NUMERIC(18,0),
      bitBroadcasted BOOLEAN
   );          
          
          
   v_strSql := ' select numBroadCastID,        
 vcBroadCastName as vcBroadCastName,        
 fn_GetContactName(numBroadCastBy) as numBroadCastBy,        
 bintBroadCastDate,numTotalRecipients,        
 (numTotalRecipients -numTotalSussessfull) as UnsucessFull,
 bitBroadcasted
          
from Broadcast where numDomainId =' || SUBSTR(CAST(v_numDomainID AS VARCHAR(9)),1,9) || ' order by bintBroadCastDate desc';        
--set @strSql=@strSql + ' ORDER BY ' + @columnName +' '+ @columnSortOrder        
   RAISE NOTICE '%',v_strSql;        
   EXECUTE 'insert into tt_TEMPTABLE( numBroadCastID,        
      vcBroadCastName,          
      numBroadCastBy,          
      bintBroadCastDate,          
      numTotalRecipients,          
      UnsucessFull,
      bitBroadcasted 
      )          
' || v_strSql;           
          
   v_firstRec :=(v_CurrentPage::bigint -1)*v_PageSize::bigint;          
   v_lastRec :=(v_CurrentPage::bigint*v_PageSize::bigint+1);          
   open SWV_RefCur for
   select *,coalesce(CAST((select sum(numNoofTimes) from BroadCastDtls where BroadCastDtls.numBroadcastID = tt_TEMPTABLE.numBroadCastID) AS VARCHAR(15)),'-') as numNoofTimes ,coalesce(CAST((select sum(intNoOfClick) from BroadCastDtls where BroadCastDtls.numBroadcastID = tt_TEMPTABLE.numBroadCastID) AS VARCHAR(15)),'-') as intNoOfClick,coalesce(CAST((select sum(CAST(bitUnsubscribe AS INTEGER)) from BroadCastDtls where BroadCastDtls.numBroadcastID = tt_TEMPTABLE.numBroadCastID) AS VARCHAR(15)),'-') as bitUnsubscribe,(select count(*) from BroadCastDtls where numBroadCastID = tt_TEMPTABLE.numBroadCastID) as numCountRecipients   from tt_TEMPTABLE where ID > v_firstRec and ID < v_lastRec; 

   select count(*) INTO v_TotRecs from tt_TEMPTABLE;          
   RETURN;
END; $$;


