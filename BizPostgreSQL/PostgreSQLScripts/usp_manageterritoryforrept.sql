-- Stored procedure definition script USP_ManageTerritoryForRept for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ManageTerritoryForRept(v_numUserCntId NUMERIC(9,0),    
v_numDomainId NUMERIC(9,0),    
v_tintType SMALLINT,    
v_strTerritory VARCHAR(4000))
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_separator_position  INTEGER;    
   v_strPosition  VARCHAR(1000);
BEGIN
   delete from ForReportsByTerritory where numUserCntId = v_numUserCntId
   and numDomainID = v_numDomainId and tintType = v_tintType;    
     
    
   while coalesce(POSITION(substring(v_strTerritory from E'\\,') IN v_strTerritory),
   0) <> 0 LOOP -- Begin for While Loop    
      v_separator_position := coalesce(POSITION(substring(v_strTerritory from E'\\,') IN v_strTerritory),
      0);
      v_strPosition := SUBSTR(v_strTerritory,1,v_separator_position -1);
      v_strTerritory := OVERLAY(v_strTerritory placing '' from 1 for v_separator_position);
      insert into ForReportsByTerritory(numUserCntId,numTerritory,numDomainID,tintType)
     values(v_numUserCntId,CAST(v_strPosition AS NUMERIC),v_numDomainId,v_tintType);
   END LOOP;
   RETURN;
END; $$;


