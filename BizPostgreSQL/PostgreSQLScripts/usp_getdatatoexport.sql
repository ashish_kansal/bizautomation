-- Stored procedure definition script USP_GetDataToExport for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetDataToExport(v_numDomainID NUMERIC(9,0)
--@tintMode TINYINT=1
,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
	
--	IF @tintMode=1
--	BEGIN
   AS $$
   DECLARE
   v_vcTables  VARCHAR(100);
   v_dtLastDate  VARCHAR(100);
BEGIN
   select   vcExportTables, FormatedDateTimeFromDate(dtLastExportedOn,numDomainID) INTO v_vcTables,v_dtLastDate FROM ExportDataSettings WHERE numDomainID = v_numDomainID;
   open SWV_RefCur for SELECT *,v_dtLastDate AS dtLastExportedOn FROM SplitIDs(v_vcTables,',');
   RETURN;
--	END
--	IF @tintMode=2
--	BEGIN
--		DECLARE @vcExportTables VARCHAR(100)
--		SELECT @vcExportTables=[vcExportTables] FROM [ExportDataSettings] WHERE [numDomainID]=@numDomainID
--	
--	--Organization Which includes all contacts
--		IF EXISTS( SELECT * FROM dbo.[SplitIDs](@vcExportTables,',') WHERE Id = 1)
--		BEGIN
--			EXEC USP_ExportDataBackUp @numDomainID,1
--		END
--		
--		--Opportunity and orders
--		IF EXISTS( SELECT * FROM dbo.[SplitIDs](@vcExportTables,',') WHERE Id = 2)
--		BEGIN
--			EXEC USP_ExportDataBackUp @numDomainID,2
--		END
--		
--/*For demo server uncomment following and comment out above*/		
----		SELECT TOP 10 [vcDivisionName],[vcBillCity],[vcBillCountry],[vcBillPostCode],[vcBilState] FROM [DivisionMaster] WHERE [numDomainID]=72 
--
--	END 
END; $$;













