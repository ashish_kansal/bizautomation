-- Stored procedure definition script USP_UpdateGoogleRefreshToken for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_UpdateGoogleRefreshToken(v_numUserCntId NUMERIC(18,0),
    v_numDomainId NUMERIC(18,0),
	v_vcGoogleRefreshToken TEXT)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   UPDATE  ImapUserDetails
   SET     vcGoogleRefreshToken = v_vcGoogleRefreshToken
   WHERE   numUserCntID = v_numUserCntId
   AND numDomainID = v_numDomainId;
   RETURN;
END; $$;
--Created by Anoop Jayaraj



