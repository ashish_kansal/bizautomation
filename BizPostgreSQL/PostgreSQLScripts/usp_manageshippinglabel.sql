-- Stored procedure definition script USP_ManageShippingLabel for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ManageShippingLabel(v_numUserCntId NUMERIC(18,0),
    v_strItems TEXT)
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_hDocItem  INTEGER;
BEGIN
	UPDATE 
		ShippingBox
	SET 
		vcShippingLabelImage = X.vcShippingLabelImage
		,vcTrackingNumber = X.vcTrackingNumber
		,bitIsMasterTrackingNo = X.bitIsMasterTrackingNo
		,dtCreateDate = TIMEZONE('UTC',now())
		,numCreatedBy = v_numUserCntId
	FROM
    XMLTABLE
	(
		'NewDataSet/Table1'
		PASSING 
			CAST(v_strItems AS XML)
		COLUMNS
			id FOR ORDINALITY,
			numBoxID NUMERIC(18,0) PATH 'numBoxID',
			vcShippingLabelImage VARCHAR(100) PATH 'vcShippingLabelImage',
			vcTrackingNumber VARCHAR(50) PATH 'vcTrackingNumber',
			bitIsMasterTrackingNo BOOLEAN PATH 'bitIsMasterTrackingNo'
	) AS X
	WHERE 
		X.numBoxID = ShippingBox.numBoxID;


-- Copy tracking number to each line item of BizDoc on generation Shipping Lable

   DROP TABLE IF EXISTS tt_TEMP_TRACKINGNO CASCADE;
   CREATE TEMPORARY TABLE tt_TEMP_TRACKINGNO
   ( 
      numBoxID NUMERIC(18,0),
      vcTrackingNumber VARCHAR(50) 
   );

   INSERT INTO tt_TEMP_TRACKINGNO
   SELECT    numBoxID,vcTrackingNumber
   FROM  
   XMLTABLE
	(
		'NewDataSet/Table1'
		PASSING 
			CAST(v_strItems AS XML)
		COLUMNS
			id FOR ORDINALITY,
			numBoxID NUMERIC(18,0) PATH 'numBoxID',
			vcTrackingNumber VARCHAR(50) PATH 'vcTrackingNumber'
	) AS X;
  
   UPDATE OpportunityBizDocItems
   SET vcTrackingNo = X.vcTrackingNumber
   FROM(SELECT SR.numOppBizDocItemID,vcTrackingNumber FROM  tt_TEMP_TRACKINGNO t INNER JOIN ShippingReportItems SR ON SR.numBoxID = t.numBoxID) X
   WHERE X.numOppBizDocItemID = OpportunityBizDocItems.numOppBizDocItemID;	


   DROP TABLE IF EXISTS tt_TEMP_TRACKINGNO CASCADE;
   RETURN;
END; $$;


