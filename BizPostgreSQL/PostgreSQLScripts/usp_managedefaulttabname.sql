-- Stored procedure definition script usp_ManageDefaultTabName for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION usp_ManageDefaultTabName(v_numDomainId NUMERIC,  
v_Mode NUMERIC,  
v_vcLead VARCHAR(50) DEFAULT '',  
v_vcContact VARCHAR(50) DEFAULT '',  
v_vcProspect VARCHAR(50) DEFAULT '',  
v_vcAccount VARCHAR(50) DEFAULT '',
v_vcLeadPlural VARCHAR(50) DEFAULT '',  
v_vcContactPlural VARCHAR(50) DEFAULT '',  
v_vcProspectPlural VARCHAR(50) DEFAULT '',  
v_vcAccountPlural VARCHAR(50) DEFAULT '')
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
  
--Insert into table  
   if v_Mode = 1 then

      if(select count(*) from DefaultTabs where numDomainID = v_numDomainId) > 0 then
  
         update DefaultTabs set
         vcLead = v_vcLead,vcContact = v_vcContact,vcProspect = v_vcProspect,vcAccount = v_vcAccount,
         vcLeadPlural = v_vcLeadPlural,vcContactPlural = v_vcContactPlural,
         vcProspectPlural = v_vcProspectPlural,vcAccountPlural = v_vcAccountPlural
         where  numDomainID = v_numDomainId;
      else
         insert into DefaultTabs(numDomainID,vcLead,vcContact,vcProspect,vcAccount)
      values(v_numDomainId,v_vcLead,v_vcContact,v_vcProspect,v_vcAccount);
      end if;
   end if;  
  
--delete 
   if v_Mode = 2 then

      delete FROM DefaultTabs
      where numDomainID = v_numDomainId;
   end if;
   RETURN;
END; $$;


