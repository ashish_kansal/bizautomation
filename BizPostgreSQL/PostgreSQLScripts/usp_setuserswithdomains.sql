-- Stored procedure definition script usp_SetUsersWithDomains for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE  OR REPLACE FUNCTION usp_SetUsersWithDomains(v_numUserID NUMERIC(9,0),                                    
	v_vcUserName VARCHAR(50),                                    
	v_vcUserDesc VARCHAR(250),                              
	v_numGroupId NUMERIC(9,0),                                           
	v_numUserDetailID NUMERIC ,                              
	v_numUserCntID NUMERIC(9,0),                              
	v_strTerritory VARCHAR(4000) ,                              
	v_numDomainID NUMERIC(9,0),
	v_strTeam VARCHAR(4000),
	v_vcEmail VARCHAR(100),
	v_vcPassword VARCHAR(100),          
	v_Active BOOLEAN,
	v_numDefaultClass NUMERIC,
	v_numDefaultWarehouse NUMERIC(18,0),
	v_vcLinkedinId VARCHAR(300) DEFAULT null,
	v_intAssociate INTEGER DEFAULT 0,
	v_ProfilePic VARCHAR(100) DEFAULT null
	,v_bitPayroll BOOLEAN DEFAULT false
	,v_monHourlyRate DECIMAL(20,5) DEFAULT 0
	,v_tintPayrollType SMALLINT DEFAULT 0
	,v_tintHourType SMALLINT DEFAULT 0
	,v_monOverTimeRate DECIMAL(20,5) DEFAULT 0
	,v_bitOauthImap BOOLEAN DEFAULT false
	,v_tintMailProvider SMALLINT DEFAULT 3)
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numNewFullUsers  INTEGER;
   v_numNewLimitedAccessUsers  INTEGER;
   v_separator_position  INTEGER;
   v_strPosition  VARCHAR(1000);
   v_APIPublicKey  VARCHAR(20);
BEGIN
   v_numNewFullUsers := coalesce((SELECT
   COUNT(*)
   FROM
   UserMaster
   INNER JOIN
   AuthenticationGroupMaster
   ON
   UserMaster.numGroupID = AuthenticationGroupMaster.numGroupID
   WHERE
   UserMaster.numDomainID = v_numDomainID
   AND AuthenticationGroupMaster.tintGroupType = 1
   AND coalesce(UserMaster.bitactivateflag,false) = true
   AND UserMaster.numUserId <> coalesce(v_numUserID,0)),0);

   v_numNewLimitedAccessUsers := coalesce((SELECT
   COUNT(*)
   FROM
   UserMaster
   INNER JOIN
   AuthenticationGroupMaster
   ON
   UserMaster.numGroupID = AuthenticationGroupMaster.numGroupID
   WHERE
   UserMaster.numDomainID = v_numDomainID
   AND AuthenticationGroupMaster.tintGroupType = 4
   AND coalesce(UserMaster.bitactivateflag,false) = true
   AND UserMaster.numUserId <> coalesce(v_numUserID,0)),0);
		
   IF coalesce(v_Active,false) = true AND coalesce((SELECT tintGroupType FROM AuthenticationGroupMaster WHERE numGroupID = v_numGroupId),0) = 1 AND EXISTS(SELECT numSubscriberID FROM Subscribers WHERE numTargetDomainID = v_numDomainID AND(v_numNewFullUsers::bigint+1) > coalesce(intNoofUsersSubscribed,0)) then
	
      RAISE EXCEPTION 'FULL_USERS_EXCEED';
      RETURN;
   ELSEIF coalesce(v_Active,false) = true AND coalesce((SELECT tintGroupType FROM AuthenticationGroupMaster WHERE numGroupID = v_numGroupId),0) = 4 AND EXISTS(SELECT numSubscriberID FROM Subscribers WHERE numTargetDomainID = v_numDomainID AND(v_numNewLimitedAccessUsers::bigint+1) > coalesce(intNoofLimitedAccessUsers,0))
   then
	
      RAISE EXCEPTION 'LIMITED_ACCESS_USERS_EXCEED';
      RETURN;
   end if;

   IF v_numUserID = 0 then
      v_APIPublicKey := GenerateBizAPIPublicKey();
      INSERT INTO UserMaster(vcUserName
			,vcUserDesc
			,numGroupID
			,numUserDetailId
			,numModifiedBy
			,bintModifiedDate
			,vcEmailID
			,vcPassword
			,numDomainID
			,numCreatedBy
			,bintCreatedDate
			,bitactivateflag
			,numDefaultClass
			,numDefaultWarehouse
			,vcBizAPIPublicKey
			,vcBizAPISecretKey
			,bitBizAPIAccessEnabled
			,vcLinkedinId
			,intAssociate
			,bitpayroll
			,monHourlyRate
			,tintPayrollType
			,tintHourType
			,monOverTimeRate
			,bitOauthImap
			,tintMailProvider)
		VALUES(v_vcUserName
			,v_vcUserDesc
			,v_numGroupId
			,v_numUserDetailID
			,v_numUserCntID
			,TIMEZONE('UTC',now())
			,v_vcEmail
			,v_vcPassword
			,v_numDomainID
			,v_numUserCntID
			,TIMEZONE('UTC',now())
			,v_Active
			,v_numDefaultClass
			,v_numDefaultWarehouse
			,v_APIPublicKey
			,uuid_generate_v4()
			,false
			,v_vcLinkedinId
			,v_intAssociate
			,v_bitPayroll
			,v_monHourlyRate
			,v_tintPayrollType
			,v_tintHourType
			,(CASE WHEN coalesce(v_monOverTimeRate, 0) = 0 THEN v_monHourlyRate ELSE v_monOverTimeRate END)
			,v_bitOauthImap
			,coalesce(v_tintMailProvider, 3)) RETURNING numUserID INTO v_numUserID;
		
      INSERT INTO BizAPIThrottlePolicy(RateLimitKey,
			bitIsIPAddress,
			numPerSecond,
			numPerMinute,
			numPerHour,
			numPerDay,
			numPerWeek)
		VALUES(v_APIPublicKey,
			false,
			3,
			60,
			1200,
			28800,
			200000);
		
      PERFORM Resource_Add(v_vcUserName,v_vcUserDesc,v_vcEmail,1,v_numDomainID,v_numUserDetailID);
   ELSE
      UPDATE
      UserMaster
      SET
      vcUserName = v_vcUserName,vcUserDesc = v_vcUserDesc,numGroupID = v_numGroupId,
      numUserDetailId = v_numUserDetailID,numModifiedBy = v_numUserCntID,
      bintModifiedDate = TIMEZONE('UTC',now()),vcEmailID = v_vcEmail,vcPassword = v_vcPassword,
      bitactivateflag = v_Active,numDefaultClass = v_numDefaultClass,
      numDefaultWarehouse = v_numDefaultWarehouse,vcLinkedinId = v_vcLinkedinId,
      intAssociate = v_intAssociate,bitpayroll = v_bitPayroll,monHourlyRate = v_monHourlyRate,
      tintPayrollType = v_tintPayrollType,tintHourType = v_tintHourType,
      monOverTimeRate =(CASE WHEN coalesce(v_monOverTimeRate,0) = 0 THEN v_monHourlyRate ELSE v_monOverTimeRate END),bitOauthImap = v_bitOauthImap,
      tintMailProvider = coalesce(v_tintMailProvider,3)
      WHERE
      numUserId = v_numUserID;
      IF NOT EXISTS(SELECT * FROM Resource WHERE numUserCntId = v_numUserDetailID and numDomainId = v_numDomainID) then
		
         PERFORM Resource_Add(v_vcUserName,v_vcUserDesc,v_vcEmail,1,v_numDomainID,v_numUserDetailID);
      end if;
   end if;
                       
   DELETE FROM UserTerritory WHERE numUserCntID = v_numUserDetailID and numDomainID = v_numDomainID;                              
          INSERT INTO UserTerritory(numUserCntID,numDomainID,numTerritoryID) SELECT v_numUserDetailID,v_numDomainID,Id FROM SplitIDs(v_strTerritory,',');	
                       
                                           
   DELETE FROM UserTeams WHERE numUserCntID = v_numUserDetailID AND numDomainID = v_numDomainID;                              
   INSERT INTO UserTeams(numUserCntID
			,numTeam
			,numDomainID) SELECT v_numUserDetailID,Id,v_numDomainID FROM SplitIDs(v_strTeam,',');	
                                      
   DELETE FROM
   ForReportsByTeam
   WHERE
   numUserCntID = v_numUserDetailID
   AND numDomainID = v_numDomainID AND numTeam NOT IN(SELECT numTeam FROM UserTeams WHERE numUserCntID = v_numUserCntID AND numDomainID = v_numDomainID);
END; $$;


