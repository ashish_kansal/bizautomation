-- Stored procedure definition script USP_TabMasterDTLs for PostgreSQL
CREATE OR REPLACE FUNCTION USP_TabMasterDTLs(v_numDomainID NUMERIC(9,0) DEFAULT 0,
    v_tintTabType INTEGER DEFAULT NULL,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT  numTabId as "numTabId",
            CASE WHEN bitFixed = true
   THEN CASE WHEN EXISTS(SELECT numTabName
         FROM   TabDefault
         WHERE  numDomainID = v_numDomainID
         AND numTabId = TM.numTabId
         AND tintTabType = v_tintTabType)
      THEN(SELECT numTabName
            FROM      TabDefault
            WHERE     numDomainID = v_numDomainID
            AND numTabId = TM.numTabId
            AND tintTabType = v_tintTabType)
      ELSE numTabName
      END
   ELSE numTabName
   END AS "numTabname",
            Remarks AS "Remarks",
            tintTabType AS "tintTabType",
            vcURL AS "vcURL",
            bitFixed AS "bitFixed",
            numDomainID AS "numDomainID"
   FROM    TabMaster TM
   WHERE   (numDomainID = v_numDomainID
   OR bitFixed = true)
   AND tintTabType = v_tintTabType;
/****** Object:  StoredProcedure [dbo].[USP_TerritoryName]    Script Date: 07/26/2008 16:21:24 ******/
   RETURN;
END; $$;












