-- Stored procedure definition script USP_GetBizDocTemplate for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetBizDocTemplate(v_numDomainID NUMERIC ,
    v_tintTemplateType SMALLINT ,
    v_numBizDocTempID NUMERIC,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
    
    
   open SWV_RefCur for SELECT
   BizDocTemplate.numDomainID,
        numBizDocID ,
        txtBizDocTemplate ,
        txtCSS ,
        coalesce(bitEnabled,false) AS bitEnabled ,
        numOppType ,
        coalesce(vcTemplateName,'') AS vcTemplateName ,
        coalesce(bitDefault,false) AS bitDefault ,
        BizDocTemplate.vcBizDocFooter ,
        BizDocTemplate.vcPurBizDocFooter,
		numOrientation,
		bitKeepFooterBottom,
		coalesce(numRelationship,0) AS numRelationship,
		coalesce(numProfile,0) AS numProfile,
		coalesce(bitDisplayKitChild,false) AS bitDisplayKitChild,
		coalesce(numAccountClass,0) AS numAccountClass,
		coalesce(BizDocTemplate.vcBizDocImagePath,Domain.vcBizDocImagePath) As vcBizDocImagePath
   FROM
   BizDocTemplate
   INNER JOIN
   Domain
   ON
   BizDocTemplate.numDomainID = Domain.numDomainId
   WHERE
   BizDocTemplate.numDomainID = v_numDomainID
   AND tintTemplateType = v_tintTemplateType
   AND (numBizDocTempID = v_numBizDocTempID OR (v_numBizDocTempID = 0 AND v_tintTemplateType != 0));
END; $$;

