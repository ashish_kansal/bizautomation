CREATE OR REPLACE FUNCTION USP_GetOpportunitySalesTemplate(v_numSalesTemplateID NUMERIC(9,0)  DEFAULT 0,
          v_numOppId NUMERIC(9,0) DEFAULT NULL,
          v_numDomainID NUMERIC(9,0) DEFAULT NULL,
          v_tintMode SMALLINT DEFAULT NULL,
          v_numDivisionID NUMERIC(9,0) DEFAULT 0,
          v_ClientTimeZoneOffset INTEGER DEFAULT 0,
		  v_numSiteID NUMERIC(18,0) DEFAULT 0, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
BEGIN
   IF v_tintMode = 0 then

      IF v_numOppId > 0 then
  
         select   numDivisionId INTO v_numDivisionID FROM   OpportunityMaster WHERE  numOppId = v_numOppId  AND numDomainId = v_numDomainID;
      end if;
      RAISE NOTICE '%',v_numDivisionID;
      IF (v_numDivisionID > 0) then
    
         open SWV_RefCur for
         SELECT numSalesTemplateID,
             vcTemplateName,
             tintType,
             CASE tintType
         WHEN 0 THEN 'Specific'
         WHEN 1 THEN 'Generic'
         END AS TemplateType ,
             numOppId,
             numDivisionID,
             fn_GetComapnyName(numDivisionID) AS OrganizationName,
             numDomainID, tintAppliesTo,
			 fn_GetContactName(OpportunitySalesTemplate.numCreatedby) || ' ' || CAST(OpportunitySalesTemplate.dtCreatedDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) AS VARCHAR(20)) AS CreatedBy
         FROM   OpportunitySalesTemplate
         WHERE  (numSalesTemplateID = v_numSalesTemplateID
         OR v_numSalesTemplateID = 0)
         AND numDivisionID = v_numDivisionID
         AND numDomainID = v_numDomainID;
      end if;
   ELSEIF v_tintMode = 1
   then

      IF coalesce(v_numSiteID,0) > 0 AND coalesce((SELECT COUNT(*) FROM OpportunitySalesTemplate WHERE numDomainID = v_numDomainID AND numDivisionID = v_numDivisionID AND tintType = 0),0) = 0 then
	
         INSERT INTO OpportunitySalesTemplate(vcTemplateName,tintType,numDivisionID,numDomainID,dtCreatedDate,numcreatedby,nummodifiedby)
		VALUES(CONCAT('List - ',coalesce((SELECT vcSiteName FROM Sites WHERE numDomainID = v_numDomainID AND numSiteID = v_numSiteID), '')),0,v_numDivisionID,v_numDomainID,TIMEZONE('UTC',now()),0,0);
      end if;
      open SWV_RefCur for
      SELECT
      numSalesTemplateID,
        vcTemplateName,
		CAST(numSalesTemplateID AS VARCHAR(20)) || ',' || CAST(coalesce(numOppId,0)  AS VARCHAR(20)) AS numSalesTemplateID1,
        (SELECT COUNT(*) FROM SalesTemplateItems WHERE numSalesTemplateID = OpportunitySalesTemplate.numSalesTemplateID) AS ItemCount, tintAppliesTo, numDivisionID,
		fn_GetContactName(OpportunitySalesTemplate.numCreatedby) || ' ' || CAST(OpportunitySalesTemplate.dtCreatedDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) AS VARCHAR(20)) AS CreatedBy
      FROM
      OpportunitySalesTemplate
      WHERE
      numDomainID = v_numDomainID
      AND tintType  = 1 -- Generic  
      UNION
      SELECT
      numSalesTemplateID,
        vcTemplateName,
        CAST(numSalesTemplateID AS VARCHAR(20)) || ',' || CAST(coalesce(numOppId,0)  AS VARCHAR(20)) AS numSalesTemplateID1,
        (SELECT COUNT(*) FROM SalesTemplateItems WHERE numSalesTemplateID = OpportunitySalesTemplate.numSalesTemplateID) AS ItemCount, tintAppliesTo, numDivisionID,
		fn_GetContactName(OpportunitySalesTemplate.numCreatedby) || ' ' || CAST(OpportunitySalesTemplate.dtCreatedDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) AS VARCHAR(20)) AS CreatedBy
      FROM
      OpportunitySalesTemplate
      WHERE
      numDomainID = v_numDomainID
      AND numDivisionID = v_numDivisionID
      AND tintType  = 0;
   ELSEIF v_tintMode = 2
   then

      open SWV_RefCur for
      SELECT numSalesTemplateID,
             vcTemplateName,
             tintType,
             CASE tintType
      WHEN 0 THEN 'Specific'
      WHEN 1 THEN 'Generic'
      END AS TemplateType ,
             numOppId,
             numDivisionID,
             fn_GetComapnyName(numDivisionID) AS OrganizationName,
             numDomainID, tintAppliesTo,
			 fn_GetContactName(OpportunitySalesTemplate.numCreatedby) || ' ' || CAST(OpportunitySalesTemplate.dtCreatedDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) AS VARCHAR(20)) AS CreatedBy
      FROM   OpportunitySalesTemplate
      WHERE  (numSalesTemplateID = v_numSalesTemplateID
      OR v_numSalesTemplateID = 0)
      AND numDomainID = v_numDomainID;
   ELSEIF v_tintMode = 3
   then --select all generic template

      open SWV_RefCur for
      SELECT numSalesTemplateID,
             vcTemplateName,
             tintType,
             CASE tintType
      WHEN 0 THEN 'Specific'
      WHEN 1 THEN 'Generic'
      END AS TemplateType ,
             numOppId,
             numDivisionID,
             fn_GetComapnyName(numDivisionID) AS OrganizationName,
             numDomainID, tintAppliesTo,
			 fn_GetContactName(OpportunitySalesTemplate.numCreatedby) || ' ' || CAST(OpportunitySalesTemplate.dtCreatedDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) AS VARCHAR(20)) AS CreatedBy
      FROM   OpportunitySalesTemplate
      WHERE  numDomainID = v_numDomainID
      AND tintType = 1;
   end if;
   RETURN;
END; $$;
