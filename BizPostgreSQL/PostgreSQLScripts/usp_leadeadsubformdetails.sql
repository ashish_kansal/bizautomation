-- Stored procedure definition script USP_LeadeadSubformDetails for PostgreSQL
CREATE OR REPLACE FUNCTION USP_LeadeadSubformDetails(v_numDomainId NUMERIC(18,0) DEFAULT 0,
 v_numSubFormId NUMERIC(18,0) DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT
   coalesce(bitByPassRoutingRules,false) AS bitByPassRoutingRules,
		coalesce(numAssignTo,0) AS numAssignTo,
		coalesce(bitDripCampaign,false) AS bitDripCampaign,
		coalesce(numDripCampaign,0) AS numDripCampaign,
		vcFormName
   FROM
   LeadsubForms
   WHERE
   numDomainID = v_numDomainId
   AND numSubFormId = v_numSubFormId;
END; $$;
