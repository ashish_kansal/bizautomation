-- Stored procedure definition script USP_GetSitePages for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_GetSitePages(v_numPageID   NUMERIC(9,0),
          v_numSiteID   NUMERIC(9,0),
          v_numDomainID NUMERIC(9,0), INOUT SWV_RefCur refcursor , INOUT SWV_RefCur2 refcursor , INOUT SWV_RefCur3 refcursor)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for
   SELECT numPageID,
		 numSiteID,
         coalesce(SP.vcPageName,'') AS vcPageName,
         coalesce(SP.vcPageURL,'') AS vcPageURL,
         coalesce(SP.tintPageType,2) AS tintPageType,
         coalesce(SP.vcPageTitle,'') AS vcPageTitle,
         numTemplateID,
         (SELECT vcTemplateName FROM SiteTemplates st WHERE st.numTemplateID = SP.numTemplateID) AS vcTemplateName,
         --ISNULL(MT.vcMetaTag,'') vcMetaTag,
         coalesce(vcMetaDescription,'') AS vcMetaDescription,
         coalesce(vcMetaKeywords,'') AS vcMetaKeywords,
         coalesce(MT.numMetaID,0) AS numMetaID,
         coalesce((SELECT vcHostName FROM Sites WHERE numSiteID = v_numSiteID),
   '') AS vcHostName,
		 CASE WHEN bitIsActive = true THEN 'Inactivate'
   ELSE 'Activate'
   END AS Status,
		 bitIsActive,
		 coalesce(bitIsMaintainScroll,false) AS bitIsMaintainScroll
   FROM   SitePages SP
   LEFT OUTER JOIN MetaTags MT
   ON MT.numReferenceID = SP.numPageID
   WHERE  (numPageID = v_numPageID
   OR v_numPageID = 0)
   AND numSiteID = v_numSiteID
   AND numDomainID = v_numDomainID
   ORDER BY
   vcPageName;

   IF v_numPageID > 0 then
    
      --Select StyleSheets for Page
      open SWV_RefCur2 for
      SELECT S.numCssID
      FROM   StyleSheetDetails SD
      INNER JOIN StyleSheets S
      ON SD.numCssID = S.numCssID
      WHERE  numPageID = v_numPageID
      AND tintType = 0;
      --Select Javascripts for Page
      open SWV_RefCur3 for
      SELECT S.numCssID
      FROM   StyleSheetDetails SD
      INNER JOIN StyleSheets S
      ON SD.numCssID = S.numCssID
      WHERE  numPageID = v_numPageID
      AND tintType = 1;
   end if;
   RETURN;
END; $$;


