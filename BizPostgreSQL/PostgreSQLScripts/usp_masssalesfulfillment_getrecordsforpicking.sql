-- Stored procedure definition script USP_MassSalesFulfillment_GetRecordsForPicking for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_MassSalesFulfillment_GetRecordsForPicking(v_numDomainID NUMERIC(18,0)
	,v_numUserCntID NUMERIC(18,0)
	,v_bitGroupByOrder BOOLEAN
	,v_numBatchID NUMERIC(18,0)
	,v_vcSelectedRecords TEXT, INOUT SWV_RefCur refcursor , INOUT SWV_RefCur2 refcursor)
LANGUAGE plpgsql
   AS $$
BEGIN
   DROP TABLE IF EXISTS tt_TEMPFIELDSRIGHT CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPFIELDSRIGHT
   (
      ID VARCHAR(50),
      numFieldID NUMERIC(18,0),
      vcFieldName VARCHAR(100),
      vcOrigDbColumnName VARCHAR(100),
      bitAllowSorting BOOLEAN,
      bitAllowFiltering BOOLEAN,
      vcAssociatedControlType VARCHAR(20),
      bitCustomField BOOLEAN,
      intRowNum INTEGER,
      intColumnWidth DOUBLE PRECISION
   );
   INSERT INTO tt_TEMPFIELDSRIGHT(ID
		,numFieldID
		,vcFieldName
		,vcOrigDbColumnName
		,bitAllowSorting
		,bitAllowFiltering
		,vcAssociatedControlType
		,bitCustomField
		,intRowNum
		,intColumnWidth)
   SELECT
   CONCAT(142,'~',DFM.numFieldID,'~',0)
		,DFM.numFieldID
		,DFFM.vcFieldName
		,DFM.vcOrigDbCOlumnName
		,DFFM.bitAllowSorting
		,DFFM.bitAllowFiltering
		,DFM.vcAssociatedControlType
		,CAST(0 AS BOOLEAN)
		,DFCD.intRowNum
		,coalesce(DFCD.intColumnWidth,0)
   FROM
   DycFormConfigurationDetails DFCD
   INNER JOIN
   DycFormField_Mapping DFFM
   ON
   DFCD.numFormID = DFFM.numFormID
   AND DFCD.numFieldID = DFFM.numFieldID
   INNER JOIN
   DycFieldMaster DFM
   ON
   DFFM.numFieldID = DFM.numFieldID
   WHERE
   DFCD.numDomainID = v_numDomainID
   AND DFCD.numUserCntID = v_numUserCntID
   AND DFCD.numFormID = 142
   AND coalesce(DFFM.bitDeleted,false) = false
   AND coalesce(DFFM.bitSettingField,false) = true
   AND coalesce(DFCD.bitCustom,false) = false;

   IF(SELECT COUNT(*) FROM tt_TEMPFIELDSRIGHT) = 0 then
	
      INSERT INTO tt_TEMPFIELDSRIGHT(ID
			,numFieldID
			,vcFieldName
			,vcOrigDbColumnName
			,bitAllowSorting
			,bitAllowFiltering
			,vcAssociatedControlType
			,bitCustomField
			,intRowNum
			,intColumnWidth)
      SELECT
      CONCAT(142,'~',DFM.numFieldID,'~',0)
			,DFM.numFieldID
			,DFFM.vcFieldName
			,DFM.vcOrigDbCOlumnName
			,DFFM.bitAllowSorting
			,DFFM.bitAllowFiltering
			,DFM.vcAssociatedControlType
			,CAST(0 AS BOOLEAN)
			,DFFM."order"
			,100
      FROM
      DycFormField_Mapping DFFM
      INNER JOIN
      DycFieldMaster DFM
      ON
      DFFM.numFieldID = DFM.numFieldID
      WHERE
      DFFM.numFormID = 142
      AND coalesce(DFFM.bitDeleted,false) = false
      AND coalesce(DFFM.bitSettingField,false) = true
      AND (coalesce(DFFM.bitDefault,false) = true OR coalesce(DFFM.bitRequired,false) = true);
   ELSE
      INSERT INTO tt_TEMPFIELDSRIGHT(ID
			,numFieldID
			,vcFieldName
			,vcOrigDbColumnName
			,bitAllowSorting
			,bitAllowFiltering
			,vcAssociatedControlType
			,bitCustomField
			,intRowNum
			,intColumnWidth)
      SELECT
      CONCAT(142,'~',DFM.numFieldID,'~',0)
			,DFM.numFieldID
			,DFFM.vcFieldName
			,DFM.vcOrigDbCOlumnName
			,DFFM.bitAllowSorting
			,DFFM.bitAllowFiltering
			,DFM.vcAssociatedControlType
			,CAST(0 AS BOOLEAN)
			,DFFM."order"
			,100
      FROM
      DycFormField_Mapping DFFM
      INNER JOIN
      DycFieldMaster DFM
      ON
      DFFM.numFieldID = DFM.numFieldID
      WHERE
      DFFM.numFormID = 142
      AND coalesce(DFFM.bitDeleted,false) = false
      AND coalesce(DFFM.bitSettingField,false) = true
      AND coalesce(DFFM.bitRequired,false) = true
      AND DFM.numFieldID NOT IN(SELECT T1.numFieldID FROM tt_TEMPFIELDSRIGHT T1);
   end if;


   BEGIN
      CREATE TEMP SEQUENCE tt_TEMP_seq INCREMENT BY 1 START WITH 1;
      EXCEPTION WHEN OTHERS THEN
         NULL;
   END;
   DROP TABLE IF EXISTS tt_TEMP CASCADE;
   CREATE TEMPORARY TABLE tt_TEMP
   (
      ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1),
      numOppID NUMERIC(18,0),
      numOppItemID NUMERIC(18,0)
   );

   IF coalesce(v_numBatchID,0) > 0 then
	
      INSERT INTO tt_TEMP(numOppID
			,numOppItemID)
      SELECT
      numOppID
			,coalesce(numOppItemID,0)
      FROM
      MassSalesFulfillmentBatchOrders
      WHERE
      numBatchID = v_numBatchID;
   ELSE
      INSERT INTO tt_TEMP(numOppID
			,numOppItemID)
      SELECT
      CAST(SUBSTR(OutParam,0,POSITION('-' IN OutParam)) AS NUMERIC(18,0)) AS numOppID
			,CAST(SUBSTR(OutParam,POSITION('-' IN OutParam)+1,LENGTH(OutParam)) AS NUMERIC(18,0)) AS numOppItemID
      FROM
      SplitString(v_vcSelectedRecords,',');
   end if;

   DROP TABLE IF EXISTS tt_TEMPITEMS CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPITEMS
   (
      ID NUMERIC(18,0),
      numOppID NUMERIC(18,0),
      numOppItemID NUMERIC(18,0),
      numOppChildItemID NUMERIC(18,0),
      numOppKitChildItemID NUMERIC(18,0),
      numItemCode NUMERIC(18,0),
      numWarehouseID NUMERIC(18,0),
      numWarehouseItemID NUMERIC(18,0),
      vcWarehouse VARCHAR(300),
      vcLocation VARCHAR(300),
      vcKitChildItems TEXT,
      vcKitChildKitItems TEXT,
      vcPoppName VARCHAR(300),
      numBarCodeId VARCHAR(300),
      vcItemName VARCHAR(300),
      vcItemDesc TEXT,
      bitSerial BOOLEAN,
      bitLot BOOLEAN,
      bitKit BOOLEAN,
      vcSKU VARCHAR(300),
      vcPathForTImage TEXT,
      vcInclusionDetails TEXT,
      numRemainingQty DOUBLE PRECISION,
      numQtyPicked DOUBLE PRECISION,
      numQtyRequiredKit DOUBLE PRECISION
   );

   INSERT INTO tt_TEMPITEMS(ID
		,numOppID
		,numOppItemID
		,numItemCode
		,numWarehouseID
		,numWarehouseItemID
		,vcWarehouse
		,vcLocation
		,vcKitChildItems
		,vcKitChildKitItems
		,vcPoppName
		,numBarCodeId
		,vcItemName
		,vcItemDesc
		,bitSerial
		,bitLot
		,bitKit
		,vcSKU
		,vcPathForTImage
		,vcInclusionDetails
		,numRemainingQty
		,numQtyPicked)
   SELECT
   T1.ID
		,OpportunityMaster.numOppId
		,OpportunityItems.numoppitemtCode
		,OpportunityItems.numItemCode
		,WareHouseItems.numWareHouseID
		,WareHouseItems.numWareHouseItemID
		,Warehouses.vcWareHouse
		,WarehouseLocation.vcLocation
		,coalesce(OVERLAY((SELECT CONCAT(', ',OpportunityItems.numItemCode,'-',OKI.numChildItemID) FROM OpportunityKitItems OKI WHERE OKI.numOppItemID = 1 ORDER BY OKI.numChildItemID) placing '' from 1 for 1),'') AS vcKitChildItems
		,coalesce(OVERLAY((SELECT CONCAT(', ',OpportunityItems.numItemCode,'-',OKI.numChildItemID,'-',OKCI.numItemID) FROM OpportunityKitChildItems OKCI INNER JOIN OpportunityKitItems OKI ON OKCI.numOppChildItemID = OKI.numOppChildItemID  WHERE OKCI.numOppItemID = 1 ORDER BY OKI.numChildItemID,OKCI.numItemID) placing '' from 1 for 1),'') AS vcKitChildKitItems
		,CAST(OpportunityMaster.vcpOppName AS VARCHAR(300))
		,Item.numBarCodeId
		,Item.vcItemName
		,Item.txtItemDesc AS vcItemDesc
		,coalesce(Item.bitSerialized,false)
		,coalesce(Item.bitLotNo,false)
		,coalesce(Item.bitKitParent,false)
		,Item.vcSKU
		,coalesce(ItemImages.vcPathForTImage,'') AS vcPathForTImage
		,coalesce(OpportunityItems.vcInclusionDetail,'') AS vcInclusionDetails
		,coalesce(OpportunityItems.numUnitHour,0) -coalesce(OpportunityItems.numQtyPicked,0) AS numRemainingQty
		,0 AS numQtyPicked
   FROM
   tt_TEMP T1
   INNER JOIN
   OpportunityMaster
   ON
   OpportunityMaster.numOppId = T1.numOppID
   INNER JOIN
   OpportunityItems
   ON
   OpportunityMaster.numOppId = OpportunityItems.numOppId
   AND (OpportunityItems.numoppitemtCode = T1.numOppItemID OR coalesce(T1.numOppItemID,0) = 0)
   INNER JOIN
   Item
   ON
   OpportunityItems.numItemCode = Item.numItemCode
   LEFT JOIN
   ItemImages
   ON
   ItemImages.numDomainId = v_numDomainID
   AND ItemImages.numItemCode = Item.numItemCode
   AND ItemImages.bitDefault = true
   AND ItemImages.bitIsImage = true
   INNER JOIN
   WareHouseItems
   ON
   OpportunityItems.numWarehouseItmsID = WareHouseItems.numWareHouseItemID
   INNER JOIN
   Warehouses
   ON
   WareHouseItems.numWareHouseID = Warehouses.numWareHouseID
   LEFT JOIN
   WarehouseLocation
   ON
   WareHouseItems.numWLocationID = WarehouseLocation.numWLocationID
   WHERE
   OpportunityMaster.numDomainId = v_numDomainID
   AND coalesce(OpportunityItems.numUnitHour,0) -coalesce(OpportunityItems.numQtyPicked,0) > 0
   ORDER BY
   T1.ID,OpportunityItems.numSortOrder;

   INSERT INTO tt_TEMPITEMS(ID
		,numOppID
		,numOppItemID
		,numOppChildItemID
		,numItemCode
		,numWarehouseID
		,numWarehouseItemID
		,vcWarehouse
		,vcLocation
		,vcKitChildItems
		,vcKitChildKitItems
		,vcPoppName
		,numBarCodeId
		,vcItemName
		,vcItemDesc
		,bitSerial
		,bitLot
		,bitKit
		,vcSKU
		,vcPathForTImage
		,vcInclusionDetails
		,numRemainingQty
		,numQtyPicked
		,numQtyRequiredKit)
   SELECT
   T1.ID
		,OpportunityMaster.numOppId
		,OpportunityItems.numoppitemtCode
		,OpportunityKitItems.numOppChildItemID
		,Item.numItemCode
		,WareHouseItems.numWareHouseID
		,WareHouseItems.numWareHouseItemID
		,Warehouses.vcWareHouse
		,WarehouseLocation.vcLocation
		,'' AS vcKitChildItems
		,'' AS vcKitChildKitItems
		,CAST(OpportunityMaster.vcpOppName AS VARCHAR(300))
		,Item.numBarCodeId
		,Item.vcItemName
		,Item.txtItemDesc AS vcItemDesc
		,coalesce(Item.bitSerialized,false)
		,coalesce(Item.bitLotNo,false)
		,coalesce(Item.bitKitParent,false)
		,Item.vcSKU
		,coalesce(ItemImages.vcPathForTImage,'') AS vcPathForTImage
		,coalesce(OpportunityItems.vcInclusionDetail,'') AS vcInclusionDetails
		,(coalesce(OpportunityItems.numUnitHour,0) -coalesce(OpportunityItems.numQtyPicked,0))*coalesce(OpportunityKitItems.numQtyItemsReq_Orig,0) AS numRemainingQty
		,0 AS numQtyPicked
		,coalesce(OpportunityKitItems.numQtyItemsReq_Orig,0)
   FROM
   tt_TEMP T1
   INNER JOIN
   OpportunityMaster
   ON
   OpportunityMaster.numOppId = T1.numOppID
   INNER JOIN
   OpportunityItems
   ON
   OpportunityMaster.numOppId = OpportunityItems.numOppId
   AND (OpportunityItems.numoppitemtCode = T1.numOppItemID OR coalesce(T1.numOppItemID,0) = 0)
   INNER JOIN
   OpportunityKitItems
   ON
   OpportunityMaster.numOppId = OpportunityItems.numOppId
   AND (OpportunityKitItems.numOppItemID = T1.numOppItemID OR OpportunityKitItems.numOppItemID = OpportunityItems.numoppitemtCode)
   INNER JOIN
   Item
   ON
   OpportunityKitItems.numChildItemID = Item.numItemCode
   LEFT JOIN
   ItemImages
   ON
   ItemImages.numDomainId = v_numDomainID
   AND ItemImages.numItemCode = Item.numItemCode
   AND ItemImages.bitDefault = true
   AND ItemImages.bitIsImage = true
   INNER JOIN
   WareHouseItems
   ON
   OpportunityKitItems.numWareHouseItemId = WareHouseItems.numWareHouseItemID
   INNER JOIN
   Warehouses
   ON
   WareHouseItems.numWareHouseID = Warehouses.numWareHouseID
   LEFT JOIN
   WarehouseLocation
   ON
   WareHouseItems.numWLocationID = WarehouseLocation.numWLocationID
   WHERE
   OpportunityMaster.numDomainId = v_numDomainID
   AND coalesce(OpportunityItems.numUnitHour,0) -coalesce(OpportunityItems.numQtyPicked,0) > 0
   AND coalesce(Item.bitKitParent,false) = false
   ORDER BY
   T1.ID,OpportunityItems.numSortOrder;

   INSERT INTO tt_TEMPITEMS(ID
		,numOppID
		,numOppItemID
		,numOppChildItemID
		,numOppKitChildItemID
		,numItemCode
		,numWarehouseID
		,numWarehouseItemID
		,vcWarehouse
		,vcLocation
		,vcKitChildItems
		,vcKitChildKitItems
		,vcPoppName
		,numBarCodeId
		,vcItemName
		,vcItemDesc
		,bitSerial
		,bitLot
		,bitKit
		,vcSKU
		,vcPathForTImage
		,vcInclusionDetails
		,numRemainingQty
		,numQtyPicked
		,numQtyRequiredKit)
   SELECT
   T1.ID
		,OpportunityMaster.numOppId
		,OpportunityItems.numoppitemtCode
		,OpportunityKitChildItems.numOppChildItemID
		,OpportunityKitChildItems.numOppKitChildItemID
		,Item.numItemCode
		,WareHouseItems.numWareHouseID
		,WareHouseItems.numWareHouseItemID
		,Warehouses.vcWareHouse
		,WarehouseLocation.vcLocation
		,'' AS vcKitChildItems
		,'' AS vcKitChildKitItems
		,CAST(OpportunityMaster.vcpOppName AS VARCHAR(300))
		,Item.numBarCodeId
		,Item.vcItemName
		,Item.txtItemDesc AS vcItemDesc
		,coalesce(Item.bitSerialized,false)
		,coalesce(Item.bitLotNo,false)
		,coalesce(Item.bitKitParent,false)
		,Item.vcSKU
		,coalesce(ItemImages.vcPathForTImage,'') AS vcPathForTImage
		,coalesce(OpportunityItems.vcInclusionDetail,'') AS vcInclusionDetails
		,(coalesce(OpportunityItems.numUnitHour,0) -coalesce(OpportunityItems.numQtyPicked,0))*coalesce(OpportunityKitItems.numQtyItemsReq_Orig,0)*coalesce(OpportunityKitChildItems.numQtyItemsReq_Orig,0) AS numRemainingQty
		,0 AS numQtyPicked
		,coalesce(OpportunityKitItems.numQtyItemsReq_Orig,0)*coalesce(OpportunityKitChildItems.numQtyItemsReq_Orig,0)
   FROM
   tt_TEMP T1
   INNER JOIN
   OpportunityMaster
   ON
   OpportunityMaster.numOppId = T1.numOppID
   INNER JOIN
   OpportunityItems
   ON
   OpportunityMaster.numOppId = OpportunityItems.numOppId
   AND (OpportunityItems.numoppitemtCode = T1.numOppItemID OR coalesce(T1.numOppItemID,0) = 0)
   INNER JOIN
   OpportunityKitChildItems
   ON
   OpportunityMaster.numOppId = OpportunityItems.numOppId
   AND (OpportunityKitChildItems.numOppItemID = T1.numOppItemID OR OpportunityKitChildItems.numOppItemID = OpportunityItems.numoppitemtCode)
   INNER JOIN
   OpportunityKitItems
   ON
   OpportunityKitChildItems.numOppChildItemID = OpportunityKitItems.numOppChildItemID
   INNER JOIN
   Item
   ON
   OpportunityKitChildItems.numItemID = Item.numItemCode
   LEFT JOIN
   ItemImages
   ON
   ItemImages.numDomainId = v_numDomainID
   AND ItemImages.numItemCode = Item.numItemCode
   AND ItemImages.bitDefault = true
   AND ItemImages.bitIsImage = true
   INNER JOIN
   WareHouseItems
   ON
   OpportunityKitChildItems.numWareHouseItemId = WareHouseItems.numWareHouseItemID
   INNER JOIN
   Warehouses
   ON
   WareHouseItems.numWareHouseID = Warehouses.numWareHouseID
   LEFT JOIN
   WarehouseLocation
   ON
   WareHouseItems.numWLocationID = WarehouseLocation.numWLocationID
   WHERE
   OpportunityMaster.numDomainId = v_numDomainID
   AND coalesce(OpportunityItems.numUnitHour,0) -coalesce(OpportunityItems.numQtyPicked,0) > 0
   AND coalesce(Item.bitKitParent,false) = false
   ORDER BY
   T1.ID,OpportunityItems.numSortOrder;

   IF coalesce(v_bitGroupByOrder,false) = true then
	
      open SWV_RefCur for
      SELECT
      TEMP.numOppID AS "numOppID"
			,(CASE WHEN TEMP.bitKit = true OR coalesce(TEMP.numOppChildItemID,0) > 0 OR coalesce(TEMP.numOppKitChildItemID,0) > 0 THEN TEMP.numOppItemID ELSE 0 END) AS "numOppItemID"
			,TEMP.numOppChildItemID AS "numOppChildItemID"
			,TEMP.numOppKitChildItemID AS "numOppKitChildItemID"
			,TEMP.numItemCode AS "numItemCode"
			,TEMP.numWarehouseID AS "numWarehouseID"
			,TEMP.vcKitChildItems AS "vcKitChildItems"
			,TEMP.vcKitChildKitItems AS "vcKitChildKitItems"
			,COALESCE((SELECT string_agg(CONCAT(T1.numOppItemID,'(',T1.numRemainingQty,')'),',')
         FROM
         tt_TEMPITEMS T1
         WHERE
         T1.numOppID = TEMP.numOppID
         AND T1.numItemCode = TEMP.numItemCode
         AND T1.numWarehouseID = TEMP.numWarehouseID
		 AND (CASE WHEN T1.bitKit = true OR coalesce(T1.numOppChildItemID,0) > 0 OR coalesce(T1.numOppKitChildItemID,0) > 0 THEN T1.numOppItemID ELSE 0 END) = MIN((CASE WHEN TEMP.bitKit = true OR coalesce(TEMP.numOppChildItemID,0) > 0 OR coalesce(TEMP.numOppKitChildItemID,0) > 0 THEN TEMP.numOppItemID ELSE 0 END))
         AND coalesce(T1.vcKitChildItems,'') = coalesce(TEMP.vcKitChildItems,'')
         AND coalesce(T1.vcKitChildKitItems,'') = coalesce(TEMP.vcKitChildKitItems,'')),'') AS "vcOppItemIDs"
			,vcPoppName AS "vcPoppName"
			,numBarCodeId AS "numBarCodeId"
			,CONCAT(TEMP.vcWarehouse,(CASE WHEN LENGTH(coalesce(TEMP.vcLocation,'')) > 0 THEN CONCAT(' (',TEMP.vcLocation,')') ELSE '' END)) AS "vcLocation"
			,vcItemName AS "vcItemName"
			,vcItemDesc AS "vcItemDesc"
			,bitSerial AS "bitSerial"
			,bitLot AS "bitLot"
			,bitKit AS "bitKit"
			,vcSKU AS "vcSKU"
			,fn_GetItemAttributes(v_numDomainID,TEMP.numItemCode,0::BOOLEAN) AS "vcAttributes"
			,vcPathForTImage AS "vcPathForTImage"
			,MIN(vcInclusionDetails) AS "vcInclusionDetails"
			,SUM(TEMP.numRemainingQty) AS "numRemainingQty"
			,0 AS "numQtyPicked"
			,numQtyRequiredKit AS "numQtyRequiredKit"
			,(CASE 
				WHEN TEMP.bitKit = true
				THEN
					CONCAT('[',COALESCE((SELECT string_agg(CONCAT('{"WarehouseItemID":',WIInner.numWareHouseItemID,', "WarehouseLocationID":',
								coalesce(WL.numWLocationID,0),', "Available":',COALESCE((SELECT 
																							MIN(TEMPF.numAvailable) 
																						FROM 
																						(
																							SELECT
																								CAST(FLOOR(CASE
																												WHEN SUM((coalesce(numOnHand,0) + coalesce(numAllocation,0))) = 0 THEN 0
																												WHEN SUM((coalesce(numOnHand,0) + coalesce(numAllocation,0))) >= numQtyRequiredKit AND numQtyRequiredKit > 0 THEN SUM((coalesce(numOnHand,0) + coalesce(numAllocation,0)))/numQtyRequiredKit
																												ELSE 0
																											END) AS DOUBLE PRECISION) AS numAvailable
																							FROM
																								tt_TEMPITEMS TEMPInner
																							INNER JOIN
																								WareHouseItems WIInner
																							ON
																								WIInner.numDomainID = v_numDomainID
																								AND WIInner.numItemID = TEMPInner.numItemCode
																								AND WIInner.numWareHouseID = TEMPInner.numWarehouseID								
																							WHERE
																								TEMPInner.numOppItemID = MIN((CASE WHEN TEMP.bitKit = true OR coalesce(TEMP.numOppChildItemID,0) > 0 OR coalesce(TEMP.numOppKitChildItemID,0) > 0 THEN TEMP.numOppItemID ELSE 0 END))
																								AND COALESCE(TEMPInner.bitKit,false)=false
																							GROUP BY
																								TEMPInner.numItemCode
																								,TEMPInner.numWarehouseID
																								,TEMPInner.numQtyRequiredKit) TEMPF)
																						,0),', "Location":""}'),',')
								FROM
								WareHouseItems WIInner
								LEFT JOIN
								WarehouseLocation WL
								ON
								WIInner.numWLocationID = WL.numWLocationID
								WHERE
								WIInner.numDomainID = v_numDomainID
								AND WIInner.numItemID = TEMP.numItemCode
								AND WIInner.numWareHouseID = TEMP.numWarehouseID),''),']')
				ELSE
					(CASE
						WHEN EXISTS(SELECT numWareHouseItemID FROM WareHouseItems WIInner INNER JOIN WarehouseLocation WL ON WIInner.numWLocationID = WL.numWLocationID WHERE WIInner.numDomainID = v_numDomainID AND WIInner.numItemID = TEMP.numItemCode AND WIInner.numWareHouseID = TEMP.numWarehouseID)
						THEN CONCAT('[',COALESCE((SELECT string_agg(CONCAT('{"WarehouseItemID":',WIInner.numWareHouseItemID,', "WarehouseLocationID":',
								coalesce(WL.numWLocationID,0),', "Available":',(coalesce(WIInner.numOnHand,0)+coalesce(WIInner.numAllocation,0)),', "Location":"',
								WL.vcLocation,'"}'),',' ORDER BY WL.vcLocation)
								FROM
								WareHouseItems WIInner
								LEFT JOIN
								WarehouseLocation WL
								ON
								WIInner.numWLocationID = WL.numWLocationID
								WHERE
								WIInner.numDomainID = v_numDomainID
								AND WIInner.numItemID = TEMP.numItemCode
								AND WIInner.numWareHouseID = TEMP.numWarehouseID
								AND (coalesce(WL.numWLocationID,0) > 0 OR WIInner.numWareHouseItemID = TEMP.numWarehouseItemID)
								),''),']')	
						ELSE CONCAT('[',COALESCE((SELECT string_agg(CONCAT('{"WarehouseItemID":',WIInner.numWareHouseItemID,', "WarehouseLocationID":',
										coalesce(WL.numWLocationID,0),', "Available":',(coalesce(WIInner.numOnHand,0)+coalesce(WIInner.numAllocation,0)),', "Location":""}'),',')
										FROM
										WareHouseItems WIInner
										LEFT JOIN
										WarehouseLocation WL
										ON
										WIInner.numWLocationID = WL.numWLocationID
										WHERE
										WIInner.numDomainID = v_numDomainID
										AND WIInner.numItemID = TEMP.numItemCode
										AND WIInner.numWareHouseID = TEMP.numWarehouseID),''),']')
				  END)
			END) AS "vcWarehouseLocations"
      FROM
      tt_TEMPITEMS TEMP
      GROUP BY
      TEMP.numOppID,(CASE WHEN TEMP.bitKit = true OR coalesce(TEMP.numOppChildItemID,0) > 0 OR coalesce(TEMP.numOppKitChildItemID,0) > 0 THEN TEMP.numOppItemID ELSE 0 END),TEMP.numOppChildItemID,TEMP.numOppKitChildItemID,
      TEMP.numItemCode,TEMP.numWarehouseID,TEMP.numWarehouseItemID,TEMP.vcKitChildItems,
      TEMP.vcKitChildKitItems,TEMP.vcPoppName,TEMP.vcPathForTImage,
      TEMP.vcItemName,TEMP.bitSerial,TEMP.bitLot,TEMP.bitKit,TEMP.vcItemDesc,
      TEMP.vcSKU,TEMP.numBarCodeId,TEMP.vcWarehouse,TEMP.vcLocation,
      TEMP.numQtyRequiredKit
      ORDER BY
      MIN(ID),(CASE WHEN TEMP.bitKit = true OR coalesce(TEMP.numOppChildItemID,0) > 0 OR coalesce(TEMP.numOppKitChildItemID,0) > 0 THEN TEMP.numOppItemID ELSE 0 END),numOppChildItemID,numOppKitChildItemID;
   ELSE
      open SWV_RefCur for
      SELECT
			0 AS "numOppId"
			,(CASE WHEN TEMP.bitKit = true OR coalesce(TEMP.numOppChildItemID,0) > 0 OR coalesce(TEMP.numOppKitChildItemID,0) > 0 THEN TEMP.numOppItemID ELSE 0 END) AS "numOppItemID"
			,TEMP.numOppChildItemID AS "numOppChildItemID"
			,TEMP.numOppKitChildItemID AS "numOppKitChildItemID"
			,TEMP.numItemCode AS "numItemCode"
			,TEMP.numWarehouseID AS "numWarehouseID"
			,TEMP.vcKitChildItems AS "vcKitChildItems"
			,TEMP.vcKitChildKitItems AS "vcKitChildKitItems"
			,COALESCE((SELECT 
							string_agg(CONCAT(T1.numOppItemID,'(',T1.numRemainingQty,')'),',')
						FROM
							tt_TEMPITEMS T1
						WHERE
							T1.numItemCode = TEMP.numItemCode
							AND T1.numWarehouseID = TEMP.numWarehouseID
							AND (CASE WHEN T1.bitKit = true OR coalesce(T1.numOppChildItemID,0) > 0 OR coalesce(T1.numOppKitChildItemID,0) > 0 THEN T1.numOppItemID ELSE 0 END) = MIN((CASE WHEN TEMP.bitKit = true OR coalesce(TEMP.numOppChildItemID,0) > 0 OR coalesce(TEMP.numOppKitChildItemID,0) > 0 THEN TEMP.numOppItemID ELSE 0 END))
							AND coalesce(T1.vcKitChildItems,'') = coalesce(TEMP.vcKitChildItems,'')
							AND coalesce(T1.vcKitChildKitItems,'') = coalesce(TEMP.vcKitChildKitItems,'')),'') AS "vcOppItemIDs"
			,'' AS "vcPoppName"
			,numBarCodeId AS "numBarCodeId"
			,CONCAT(TEMP.vcWarehouse,(CASE WHEN LENGTH(coalesce(TEMP.vcLocation,'')) > 0 THEN CONCAT(' (',TEMP.vcLocation,')') ELSE '' END)) AS "vcLocation"
			,vcItemName AS "vcItemName"
			,vcItemDesc AS "vcItemDesc"
			,bitSerial AS "bitSerial"
			,bitLot AS "bitLot"
			,bitKit AS "bitKit"
			,vcSKU AS "vcSKU"
			,fn_GetItemAttributes(v_numDomainID,TEMP.numItemCode,0::BOOLEAN) AS "vcAttributes"
			,vcPathForTImage AS "vcPathForTImage"
			,MIN(vcInclusionDetails) AS "vcInclusionDetails"
			,SUM(TEMP.numRemainingQty) AS "numRemainingQty"
			,0 AS "numQtyPicked"
			,numQtyRequiredKit AS "numQtyRequiredKit"
			,(CASE 
				WHEN TEMP.bitKit = true
				THEN
					CONCAT('[',COALESCE((SELECT string_agg(CONCAT('{"WarehouseItemID":',WIInner.numWareHouseItemID,', "WarehouseLocationID":',
								coalesce(WL.numWLocationID,0),', "Available":',COALESCE((SELECT 
																							MIN(TEMPF.numAvailable) 
																						FROM 
																						(
																							SELECT
																								CAST(FLOOR(CASE
																												WHEN SUM((coalesce(numOnHand,0) + coalesce(numAllocation,0))) = 0 THEN 0
																												WHEN SUM((coalesce(numOnHand,0) + coalesce(numAllocation,0))) >= numQtyRequiredKit AND numQtyRequiredKit > 0 THEN SUM((coalesce(numOnHand,0) + coalesce(numAllocation,0)))/numQtyRequiredKit
																												ELSE 0
																											END) AS DOUBLE PRECISION) AS numAvailable
																							FROM
																								tt_TEMPITEMS TEMPInner
																							INNER JOIN
																								WareHouseItems WIInner
																							ON
																								WIInner.numDomainID = v_numDomainID
																								AND WIInner.numItemID = TEMPInner.numItemCode
																								AND WIInner.numWareHouseID = TEMPInner.numWarehouseID								
																							WHERE
																								TEMPInner.numOppItemID = MIN((CASE WHEN TEMP.bitKit = true OR coalesce(TEMP.numOppChildItemID,0) > 0 OR coalesce(TEMP.numOppKitChildItemID,0) > 0 THEN TEMP.numOppItemID ELSE 0 END))
																								AND COALESCE(TEMPInner.bitKit,false)=false
																							GROUP BY
																								TEMPInner.numItemCode
																								,TEMPInner.numWarehouseID
																								,TEMPInner.numQtyRequiredKit) TEMPF)
																						,0),', "Location":""}'),',')
								FROM
								WareHouseItems WIInner
								LEFT JOIN
								WarehouseLocation WL
								ON
								WIInner.numWLocationID = WL.numWLocationID
								WHERE
								WIInner.numDomainID = v_numDomainID
								AND WIInner.numItemID = TEMP.numItemCode
								AND WIInner.numWareHouseID = TEMP.numWarehouseID),''),']')
				ELSE
					(CASE
						  WHEN EXISTS(SELECT numWareHouseItemID FROM WareHouseItems WIInner INNER JOIN WarehouseLocation WL ON WIInner.numWLocationID = WL.numWLocationID WHERE WIInner.numDomainID = v_numDomainID AND WIInner.numItemID = TEMP.numItemCode AND WIInner.numWareHouseID = TEMP.numWarehouseID)
						  THEN CONCAT('[',COALESCE((SELECT string_agg( CONCAT('{"WarehouseItemID":',WIInner.numWareHouseItemID,', "WarehouseLocationID":',
								coalesce(WL.numWLocationID,0),', "Available":',(coalesce(WIInner.numOnHand,0)+coalesce(WIInner.numAllocation,0)),', "Location":"',
								WL.vcLocation,'"}'),',' ORDER BY WL.vcLocation)
								FROM
								WareHouseItems WIInner
								LEFT JOIN
								WarehouseLocation WL
								ON
								WIInner.numWLocationID = WL.numWLocationID
								WHERE
								WIInner.numDomainID = v_numDomainID
								AND WIInner.numItemID = TEMP.numItemCode
								AND WIInner.numWareHouseID = TEMP.numWarehouseID
								AND (coalesce(WL.numWLocationID,0) > 0 OR WIInner.numWareHouseItemID = TEMP.numWarehouseItemID)
								),''),']')
						  ELSE CONCAT('[',COALESCE((SELECT string_agg(CONCAT('{"WarehouseItemID":',WIInner.numWareHouseItemID,', "WarehouseLocationID":',
								coalesce(WL.numWLocationID,0),', "Available":',(coalesce(WIInner.numOnHand,0)+coalesce(WIInner.numAllocation,0)),', "Location":""}'),',')
								FROM
								WareHouseItems WIInner
								LEFT JOIN
								WarehouseLocation WL
								ON
								WIInner.numWLocationID = WL.numWLocationID
								WHERE
								WIInner.numDomainID = v_numDomainID
								AND WIInner.numItemID = TEMP.numItemCode
								AND WIInner.numWareHouseID = TEMP.numWarehouseID),''),']')
					END)
			END) AS "vcWarehouseLocations"
      FROM
      tt_TEMPITEMS TEMP
      GROUP BY(CASE WHEN TEMP.bitKit = true OR coalesce(TEMP.numOppChildItemID,0) > 0 OR coalesce(TEMP.numOppKitChildItemID,0) > 0 THEN TEMP.numOppItemID ELSE 0 END),TEMP.numOppChildItemID,TEMP.numOppKitChildItemID,TEMP.numItemCode,
      TEMP.numWarehouseID,TEMP.numWarehouseItemID,TEMP.vcKitChildItems,
      TEMP.vcKitChildKitItems,TEMP.vcPathForTImage,TEMP.vcItemName,TEMP.vcItemDesc,
      TEMP.bitSerial,TEMP.bitLot,TEMP.vcSKU,TEMP.bitKit,TEMP.numBarCodeId,
      TEMP.vcWarehouse,TEMP.vcLocation,TEMP.numQtyRequiredKit
      ORDER BY
      MIN(ID),(CASE WHEN TEMP.bitKit = true OR coalesce(TEMP.numOppChildItemID,0) > 0 OR coalesce(TEMP.numOppKitChildItemID,0) > 0 THEN TEMP.numOppItemID ELSE 0 END),numOppChildItemID,numOppKitChildItemID;
   end if;
	
   open SWV_RefCur2 for
   SELECT ID AS "ID",
      numFieldID AS "numFieldID",
      vcFieldName AS "vcFieldName",
      vcOrigDbColumnName AS "vcOrigDbColumnName",
      bitAllowSorting AS "bitAllowSorting",
      bitAllowFiltering AS "bitAllowFiltering",
      vcAssociatedControlType AS "vcAssociatedControlType",
      bitCustomField AS "bitCustomField",
      intRowNum AS "intRowNum",
      intColumnWidth AS "intColumnWidth" FROM tt_TEMPFIELDSRIGHT ORDER BY intRowNum;
   RETURN;
END; $$;


