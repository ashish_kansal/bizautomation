CREATE OR REPLACE FUNCTION USP_GetManageNews(INOUT v_numNewsID NUMERIC(9,0) DEFAULT 0 ,  
v_vcHeading VARCHAR(1000) DEFAULT '',  
v_vcDate TIMESTAMP DEFAULT null,  
v_vcDesc TEXT DEFAULT '',  
v_vcImagePath VARCHAR(500) DEFAULT '',  
v_vcURL VARCHAR(500) DEFAULT '',  
v_bitActive BOOLEAN DEFAULT false,
v_tintType SMALLINT DEFAULT 0)
LANGUAGE plpgsql
   AS $$
BEGIN
   if v_numNewsID = 0 then

      insert into news(vcNewsHeading,vcDate,vcDesc,vcImagePath,vcURL,vcCreatedDate,bitActive,tintType)
values(v_vcHeading,v_vcDate,v_vcDesc,v_vcImagePath,v_vcURL,TIMEZONE('UTC',now()),v_bitActive,v_tintType);

      v_numNewsID := CURRVAL('news_seq');
   ELSEIF v_numNewsID > 0
   then

      update news set vcNewsHeading = v_vcHeading,vcDate = v_vcDate,vcDesc = v_vcDesc,vcImagePath = v_vcImagePath,
      vcURL = v_vcURL,vcModifieddate = TIMEZONE('UTC',now()),bitActive = v_bitActive,
      tintType = v_tintType
      where numNewsID = v_numNewsID;
   end if;
   RETURN;
END; $$;


