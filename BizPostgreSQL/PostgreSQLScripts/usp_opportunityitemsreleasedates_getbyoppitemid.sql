-- Stored procedure definition script USP_OpportunityItemsReleaseDates_GetByOppItemID for PostgreSQL
CREATE OR REPLACE FUNCTION USP_OpportunityItemsReleaseDates_GetByOppItemID(v_numDomainID NUMERIC(18,0),
	v_numOppID NUMERIC(18,0),
	v_numOppItemID NUMERIC(18,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT
		numID,
		CONCAT(FormatedDateFromDate(dtReleaseDate,v_numDomainID),'(',numQty,'),',CASE tintStatus WHEN 1 THEN 'Open' ELSE 'Purchased' END) AS vcDesctiption,
		dtReleaseDate,
		numQty,
		tintStatus
   FROM
   OpportunityItemsReleaseDates
   WHERE
   numOppId = v_numOppID
   AND numOppItemID = v_numOppItemID;
END; $$;











