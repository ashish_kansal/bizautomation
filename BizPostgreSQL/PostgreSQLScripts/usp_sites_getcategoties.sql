-- Stored procedure definition script USP_Sites_GetCategoties for PostgreSQL
CREATE OR REPLACE FUNCTION USP_Sites_GetCategoties(v_numDomainID NUMERIC(18,0),
	v_numSiteID NUMERIC(18,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numCategoryProfileID  NUMERIC(18,0);
BEGIN
   select   CPS.numCategoryProfileID INTO v_numCategoryProfileID FROM
   Sites S
   JOIN
   CategoryProfileSites CPS
   ON
   S.numSiteID = CPS.numSiteID
   JOIN
   CategoryProfile CP
   ON
   CPS.numCategoryProfileID = CP.ID WHERE
   S.numSiteID = v_numSiteID;

   open SWV_RefCur for
   with recursive CategoryList(numCategoryID,numParentCategoryID,vcCategoryName,tintLevel,intDisplayOrder,
   vcDescription)
   As(SELECT
   C.numCategoryID AS numCategoryID,
			coalesce(C.numDepCategory,0) AS numParentCategoryID,
			coalesce(C.vcCategoryName,'') AS vcCategoryName,
			0  AS tintLevel,
			coalesce(C.intDisplayOrder,0) AS intDisplayOrder,
			C.vcDescription AS vcDescription
   FROM
   SiteCategories SC
   INNER JOIN
   Category C
   ON
   SC.numCategoryID = C.numCategoryID
   WHERE
   SC.numSiteID = v_numSiteID
   AND C.numCategoryProfileID = v_numCategoryProfileID
   AND C.numDomainID = v_numDomainID
   AND coalesce(numDepCategory,0) = 0
   UNION ALL
   SELECT
   C.numCategoryID AS numCategoryID,
			coalesce(C.numDepCategory,0) AS numParentCategoryID,
			coalesce(C.vcCategoryName,'') AS vcCategoryName,
			coalesce(CL.tintLevel+1,0) AS tintLevel,
			coalesce(C.intDisplayOrder,0) AS intDisplayOrder,
			C.vcDescription AS vcDescription
   FROM
   SiteCategories SC
   INNER JOIN
   Category C
   ON
   SC.numCategoryID = C.numCategoryID
   INNER JOIN
   CategoryList CL
   ON
   C.numDepCategory = CL.numCategoryID
   WHERE
   SC.numSiteID = v_numSiteID
   AND C.numCategoryProfileID = v_numCategoryProfileID
   AND C.numDomainID = v_numDomainID)
   SELECT * FROM CategoryList ORDER BY tintLevel,intDisplayOrder;
   RETURN;
END; $$;

/****** Object:  StoredProcedure [dbo].[USP_UpdateCartItem]    Script Date: 11/08/2011 17:49:01 ******/













