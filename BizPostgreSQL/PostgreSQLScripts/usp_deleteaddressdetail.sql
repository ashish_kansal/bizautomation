-- Stored procedure definition script USP_DeleteAddressDetail for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_DeleteAddressDetail(v_numDomainID NUMERIC,
v_numAddressID NUMERIC)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   IF(SELECT bitIsPrimary FROM AddressDetails WHERE numAddressID = v_numAddressID) = true then

      RAISE EXCEPTION 'PRIMARY';
      RETURN;
   end if;
   DELETE FROM AddressDetails WHERE numAddressID = v_numAddressID AND numDomainID = v_numDomainID;
END; $$;



