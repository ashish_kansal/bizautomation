-- Function definition script GetTotalProgress for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION GetTotalProgress(v_numDomainID NUMERIC(18,0)
	,v_numRecordID NUMERIC(18,0)
	,v_tintRecordType SMALLINT --1:WorkOrder, 2:Project
	,v_tintProgressFor SMALLINT -- 1:Record, 2:Milestome, 3:Stage
	,v_vcMileStone VARCHAR(200)
	,v_numStageDetailsId NUMERIC(18,0))
RETURNS INTEGER LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numTotalProgress  INTEGER DEFAULT 0;
   v_numTotalStageTask  NUMERIC(18,0) DEFAULT 0;
   v_numCompletedStageTask  NUMERIC(18,0) DEFAULT 0;
BEGIN
   DROP TABLE IF EXISTS tt_TEMPSTAGES CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPSTAGES
   (
      vcMileStoneName VARCHAR(200),
      numMilestonePercentage INTEGER,
      numStageDetailsId NUMERIC(18,0),
      vcStageName VARCHAR(200),
      numStagePercentage INTEGER,
      numTotalStageTask INTEGER,
      numCompletedStageTask INTEGER,
      numStageProgress INTEGER,
      numMilestoneProgress INTEGER
   );
   INSERT INTO tt_TEMPSTAGES(vcMileStoneName
		,numMilestonePercentage
		,numStageDetailsId
		,vcStageName
		,numStagePercentage
		,numTotalStageTask
		,numCompletedStageTask)
   SELECT
   StagePercentageDetails.vcMileStoneName
		,stagepercentagemaster.numstagepercentage AS numMilestonePercentage
		,StagePercentageDetails.numStageDetailsId
		,StagePercentageDetails.vcStageName
		,StagePercentageDetails.tintPercentage
		,coalesce((SELECT COUNT(*) FROM StagePercentageDetailsTask SPDT WHERE SPDT.numStageDetailsId = StagePercentageDetails.numStageDetailsId),0) AS numTotalStageTask
		,coalesce((SELECT COUNT(*) FROM StagePercentageDetailsTask SPDT WHERE SPDT.numStageDetailsId = StagePercentageDetails.numStageDetailsId AND EXISTS(SELECT ID FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskId = SPDT.numTaskId AND tintAction = 4)),0) AS numCompletedStageTask
   FROM
   StagePercentageDetails
   INNER JOIN
   stagepercentagemaster
   ON
   StagePercentageDetails.numStagePercentageId = stagepercentagemaster.numstagepercentageid
   WHERE
   numDomainId = v_numDomainID
   AND 1 =(CASE
   WHEN v_tintRecordType = 1 THEN(CASE WHEN numWorkOrderId = v_numRecordID THEN 1 ELSE 0 END)
   WHEN v_tintRecordType = 2 THEN(CASE WHEN numProjectID = v_numRecordID THEN 1 ELSE 0 END)
   ELSE 0
   END);
   v_numTotalStageTask := coalesce((SELECT numTotalStageTask FROM tt_TEMPSTAGES WHERE numStageDetailsId = v_numStageDetailsId),0);
   v_numCompletedStageTask := coalesce((SELECT numCompletedStageTask FROM tt_TEMPSTAGES WHERE numStageDetailsId = v_numStageDetailsId),0);
   UPDATE tt_TEMPSTAGES SET numStageProgress =(CASE WHEN numTotalStageTask > 0 THEN CAST((numCompletedStageTask::bigint*100) AS decimal)/numTotalStageTask::bigint ELSE 0 END);

   UPDATE tt_TEMPSTAGES SET numMilestoneProgress = CAST((numStageProgress::bigint*numStagePercentage::bigint) AS decimal)/100;

   IF v_tintProgressFor = 1 then
	
      v_numTotalProgress := coalesce((SELECT AVG(numMilestoneProgress) FROM(SELECT SUM(numMilestoneProgress) AS numMilestoneProgress FROM tt_TEMPSTAGES GROUP BY vcMileStoneName) TEMP),0);
   ELSEIF v_tintProgressFor = 2
   then
	
      v_numTotalProgress := coalesce((SELECT SUM(numMilestoneProgress) FROM tt_TEMPSTAGES WHERE vcMileStoneName = v_vcMileStone),0);
   ELSEIF v_tintProgressFor = 3
   then
	
      v_numTotalProgress := coalesce((SELECT numStageProgress FROM tt_TEMPSTAGES WHERE numStageDetailsId = v_numStageDetailsId),0);
   end if;
  
   RETURN v_numTotalProgress;
END; $$;

