CREATE OR REPLACE FUNCTION USP_GetListItems
(
	v_Listdata TEXT DEFAULT '',
	v_numDomainID NUMERIC(9,0) DEFAULT 0
	,INOUT SWV_RefCur refcursor DEFAULT NULL
)
LANGUAGE plpgsql
   AS $$
BEGIN                                      
	open SWV_RefCur for 
	SELECT 
		cast(numListID as VARCHAR(255))
		,cast(numListItemID as VARCHAR(255))
		,cast(vcData as VARCHAR(255)) 
	FROM 
		Listdetails
	INNER JOIN
	(
		SELECT list FROM
	XMLTABLE
	(
		'NewDataSet/Table'
		PASSING 
			CAST(v_Listdata AS XML)
		COLUMNS
			list NUMERIC(18,0) PATH 'list'
	)) AS X 
	ON
		Listdetails.numListID = X.list
	WHERE 
		(constFlag = true or numDomainid = v_numDomainID);

   RETURN;
END; $$;