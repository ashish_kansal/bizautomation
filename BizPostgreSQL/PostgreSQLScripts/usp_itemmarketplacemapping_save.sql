-- Stored procedure definition script USP_ItemMarketplaceMapping_Save for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ItemMarketplaceMapping_Save(v_numDomainID NUMERIC(18,0)
	,v_numItemCode NUMERIC(18,0)
	,v_numMarketplaceID NUMERIC(18,0)
	,v_vcMarketplaceUniqueID VARCHAR(300))
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   IF EXISTS(SELECT ID FROM ItemMarketplaceMapping WHERE numDomainID = v_numDomainID AND numItemCode = v_numItemCode AND numMarketplaceID = v_numMarketplaceID) then
	
      UPDATE
      ItemMarketplaceMapping
      SET
      vcMarketplaceUniqueID = v_vcMarketplaceUniqueID
      WHERE
      numDomainID = v_numDomainID
      AND numItemCode = v_numItemCode
      AND numMarketplaceID = v_numMarketplaceID;
   ELSE
      INSERT INTO ItemMarketplaceMapping(numDomainID
			,numItemCode
			,numMarketplaceID
			,vcMarketplaceUniqueID)
		VALUES(v_numDomainID
			,v_numItemCode
			,v_numMarketplaceID
			,v_vcMarketplaceUniqueID);
   end if;
   RETURN;
END; $$;


