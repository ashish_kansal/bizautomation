-- Stored procedure definition script USP_GetItemAttributes for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetItemAttributes(v_numDomainID NUMERIC(9,0),
	v_numWareHouseID NUMERIC(9,0) DEFAULT 0,
    v_FilterQuery VARCHAR(2000) DEFAULT '',
    v_numCategoryID NUMERIC(18,0) DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT
		Fld_id
		,fld_type
		,FLd_label
		,numlistid
		,bitAutocomplete
   FROM
   CFW_Fld_Master
   WHERE
   numDomainID = v_numDomainID
   AND Grp_id = 9
   ORDER BY bitAutocomplete;
   RETURN;
END; $$;













