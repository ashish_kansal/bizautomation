CREATE OR REPLACE FUNCTION usp_getCustomReportResultSet(v_vcCustomReportConfig TEXT,                                                          
 v_numUserCntID NUMERIC,                      
 v_numRefreshTimeInterval NUMERIC,                      
 v_numDomainId NUMERIC,
 v_tintType BIGINT, INOUT SWV_RefCur refcursor , INOUT SWV_RefCur2 refcursor)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_ReportType  NUMERIC;                                                     
 --For Org and Contacts                                                       
   v_RelationShip  NUMERIC;                                                        
   v_CRMType  NUMERIC;                                                          
   v_IncludeContacts  BOOLEAN;                                                     
   v_ContactType  NUMERIC;                                                    
   v_CustomFields  BOOLEAN;                                    
 --For Opportunity and Items                                                    
   v_OppType  NUMERIC;                                          
                                               
                                                        
   v_vcReportLayout  CHAR(3);                                                        
   v_bitStandardFilters  BOOLEAN;                                                        
   v_vcStandardFilterCriteria  VARCHAR(50);                                                        
   v_vcStandardFilterStartDate  VARCHAR(10);                                                        
   v_vcStandardFilterEndDate  VARCHAR(10);                                                        
   v_vcTeams  VARCHAR(250);                                                        
   v_vcTerritories  VARCHAR(250);                                                        
   v_vcFilterOn  INTEGER;                                                        
   v_vcAdvFilterJoining  CHAR(1);                                                        
   v_bitRecordCountSummation  BOOLEAN;                                                        
                                                        
   v_vcReportColumnNames  VARCHAR(4000);                                              
   v_vcAggregateReportColumns  VARCHAR(1000);                                          
   v_vcReportAdvFilterWhereClause  VARCHAR(1000);                                                        
   v_vcReportStdFilterWhereClause  VARCHAR(1000);                                                         
   v_vcReportDataTypeWhereClause  VARCHAR(500);                                                        
   v_vcReportWhereClause  VARCHAR(2000);                   
                
   v_numDefaultOwner  NUMERIC;
   v_iDoc  INTEGER;                                                        
   v_vcViewName  VARCHAR(70);                                                      
   v_vcCustomReportQuery  VARCHAR(4000);                       
   v_vcCustomReportQueryMiddle  VARCHAR(4000);                                             
   v_vcCustomReportQueryTrailer  VARCHAR(4000);
   SWV_ExecDyn  VARCHAR(5000);
BEGIN
   select   RD.numEmpId INTO v_numDefaultOwner FROM
   RoutingLeadDetails RD
   INNER JOIN
   RoutingLeads RL
   ON
   RD.numRoutID = RL.numRoutID WHERE
   RL.bitDefault = true AND RL.numDomainID = v_numDomainId;                           
                                                        
   SELECT * INTO v_iDoc FROM SWF_Xml_PrepareDocument(v_vcCustomReportConfig);                                                        
                                                           
   select   ReportType INTO v_ReportType FROM SWF_OpenXml(v_iDoc,'/CustRptConfig/ReportTypeConfig','ReportType') SWA_OpenXml(ReportType NUMERIC);                              
   RAISE NOTICE 'ReportType: %',SUBSTR(CAST(v_ReportType AS VARCHAR(30)),1,30);                              
   v_vcViewName := CASE v_ReportType
   WHEN '1' THEN '##OrgContactCustomField'
   WHEN '2' THEN '##OppItemCustomField'
   WHEN '3' THEN 'dbo.vw_LeadsCustomReport'
   END;                                                      
                                              
   IF v_ReportType = 1 then --Organization and Contacts                                                    
 
      select   RelationShip, CRMType, IncludeContacts, ContactType, CustomFields INTO v_RelationShip,v_CRMType,v_IncludeContacts,v_ContactType,v_CustomFields FROM SWF_OpenXml(v_iDoc,'/CustRptConfig/ReportTypeConfig','RelationShip |CRMType |IncludeContacts |ContactType |CustomFields') SWA_OpenXml(RelationShip NUMERIC,CRMType NUMERIC,IncludeContacts BOOLEAN,ContactType NUMERIC,
      CustomFields BOOLEAN);
   ELSEIF v_ReportType = 2
   then --Opprotunity and Items                                                    
 
      select   OppType, CustomFields INTO v_OppType,v_CustomFields FROM SWF_OpenXml(v_iDoc,'/CustRptConfig/ReportTypeConfig','OppType |CustomFields') SWA_OpenXml(OppType NUMERIC,CustomFields BOOLEAN);
   end if;                                                   
   RAISE NOTICE 'RelationShip: %',SUBSTR(Cast(v_RelationShip As VARCHAR(30)),1,30);                                                    
   RAISE NOTICE 'CRMType: %',SUBSTR(Cast(v_CRMType As VARCHAR(30)),1,30);                                                    
   RAISE NOTICE 'IncludeContacts: %',SUBSTR(Cast(v_IncludeContacts As VARCHAR(30)),1,30);                                                    
   RAISE NOTICE 'ContactType: %',SUBSTR(Cast(v_ContactType As VARCHAR(30)),1,30);                                                       
   RAISE NOTICE 'CustomFields: %',SUBSTR(Cast(v_CustomFields As VARCHAR(30)),1,30);                                                         
                                                    
   RAISE NOTICE 'OppType: %',SUBSTR(Cast(v_OppType As VARCHAR(30)),1,30);                                                    
                                                    
   v_vcReportDataTypeWhereClause := CASE v_ReportType
   WHEN 1 THEN
      ' AND tintCRMType = ' || SUBSTR(Cast(v_CRMType As VARCHAR(30)),1,30) ||
      CASE WHEN v_RelationShip > 0 THEN
         ' AND numOrgCompanyType = ' || SUBSTR(Cast(v_RelationShip As VARCHAR(30)),1,30)
      ELSE
         ''
      END
      ||
      CASE WHEN v_IncludeContacts = true THEN
         CASE WHEN v_ContactType > 0 THEN
            ' AND numContContactType = ' || SUBSTR(Cast(v_ContactType As VARCHAR(30)),1,30)
         ELSE
            ''
         END
      ELSE
         ''
      END
   WHEN 2 THEN
      ' AND tintOppType = ' || SUBSTR(Cast(v_OppType As VARCHAR(30)),1,30)
   ELSE
      ''
   END;                          
                                                    
   RAISE NOTICE '%','vcReportDataTypeWhereClause: ' || coalesce(v_vcReportDataTypeWhereClause,'');                                                     
                                                    
   select   vcReportLayout, bitStandardFilters, vcStandardFilterCriteria, vcStandardFilterStartDate, vcStandardFilterEndDate, vcTeams, vcTerritories, vcFilterOn, vcAdvFilterJoining, bitRecordCountSummation INTO v_vcReportLayout,v_bitStandardFilters,v_vcStandardFilterCriteria,v_vcStandardFilterStartDate,
   v_vcStandardFilterEndDate,v_vcTeams,v_vcTerritories,
   v_vcFilterOn,v_vcAdvFilterJoining,v_bitRecordCountSummation FROM SWF_OpenXml(v_iDoc,'/CustRptConfig/LayoutAndStdFilter','vcReportLayout |bitStandardFilters |vcStandardFilterCriteria |vcStandardFilterStartDate |vcStandardFilterEndDate |vcTeams |vcTerritories |vcFilterOn |vcAdvFilterJoining |bitRecordCountSummation') SWA_OpenXml(vcReportLayout CHAR(3),bitStandardFilters BOOLEAN,vcStandardFilterCriteria VARCHAR(50),
   vcStandardFilterStartDate VARCHAR(10),vcStandardFilterEndDate VARCHAR(10),
   vcTeams VARCHAR(250),vcTerritories VARCHAR(250),vcFilterOn INTEGER,
   vcAdvFilterJoining CHAR(1),bitRecordCountSummation BOOLEAN);                                                        
   RAISE NOTICE '@ReportType%',SUBSTR(CAST(v_ReportType AS VARCHAR(30)),1,30);                    
   RAISE NOTICE 'vcReportLayout: %',SUBSTR(CAST(v_vcReportLayout AS VARCHAR(30)),1,30);                                                        
   RAISE NOTICE 'bitStandardFilters: %',SUBSTR(CAST(v_bitStandardFilters AS VARCHAR(30)),1,30);                                                        
   RAISE NOTICE 'vcStandardFilterCriteria: %',SUBSTR(CAST(v_vcStandardFilterCriteria AS VARCHAR(30)),1,30);                                                        
   RAISE NOTICE 'vcStandardFilterStartDate: %',SUBSTR(CAST(v_vcStandardFilterStartDate AS VARCHAR(30)),1,30);                                                        
   RAISE NOTICE 'vcStandardFilterEndDate: %',SUBSTR(CAST(v_vcStandardFilterEndDate AS VARCHAR(30)),1,30);                                                        
   RAISE NOTICE 'vcTeams: %',SUBSTR(CAST(v_vcTeams AS VARCHAR(30)),1,30);                                                        
   RAISE NOTICE 'vcTerritories: %',SUBSTR(CAST(v_vcTerritories AS VARCHAR(30)),1,30);                                                        
   RAISE NOTICE 'vcFilterOn: %',SUBSTR(CAST(v_vcFilterOn AS VARCHAR(30)),1,30);                                                 
   RAISE NOTICE 'vcAdvFilterJoining: %',SUBSTR(CAST(v_vcAdvFilterJoining AS VARCHAR(30)),1,30);                                                        
   RAISE NOTICE 'bitRecordCountSummation: %',SUBSTR(CAST(v_bitRecordCountSummation AS VARCHAR(30)),1,30);                                                       
                                                      
   v_vcReportStdFilterWhereClause := CASE  v_vcFilterOn
   WHEN '0' THEN
      CASE WHEN v_ReportType = 2 THEN    --Opportunities              
         ' numOppRecOwner = ' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(30)),1,30) || ' '
      ELSE    --Organizations and Contacts              
         ' (numContRecOwner = ' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(30)),1,30) || ' OR numDivRecOwner = ' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(30)),1,30) || ') '
      END
   WHEN '1' THEN
      CASE WHEN v_ReportType = 2 THEN    --Opportunities              
         ' numOppRecOwner = ' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(30)),1,30) ||
         ' AND numOppCreatedBy IN (SELECT numContactId FROM AdditionalContactsInformation WHERE numTeam IN (select numTeam From ForReportsByTeam Where numuserCntid =' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(30)),1,30) || 'numDomainid=' || SUBSTR(CAST(v_numDomainId AS VARCHAR(15)),1,15) || 'tintType=32)) AND ((bitPublicFlag = false) OR (bitPublicFlag=1 and numOppRecOwner = ' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(30)),1,30) || ')) '
      ELSE    --Organizations and Contacts              
         ' (numContRecOwner = ' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(30)),1,30) || ' OR numDivRecOwner = ' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(30)),1,30) || ') ' ||
         ' AND numContCreatedBy IN (SELECT numContactId FROM AdditionalContactsInformation WHERE numTeam IN (select numTeam From ForReportsByTeam Where numuserCntid =' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(30)),1,30) || 'numDomainid=' || SUBSTR(CAST(v_numDomainId AS VARCHAR(15)),1,15) || 'tintType=32)) AND ((bitPublicFlag = false) OR (bitPublicFlag=1 and numDivRecOwner = ' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(30)),1,30) || ')) '
      END
   ELSE
      CASE WHEN v_ReportType = 2 THEN  --Opportunities              
         ' numTerritoryId IN (select F.numTerritory from ForReportsByTerritory F where F.numuserCntid=' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15) || ' and F.numDomainid=' || SUBSTR(CAST(v_numDomainId AS VARCHAR(15)),1,15) || ' and F.tintType=' || SUBSTR(CAST(v_tintType AS VARCHAR(15)),1,15) || ')  AND ((bitPublicFlag = false) OR (bitPublicFlag = true an
d numOppRecOwner = ' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(30)),1,30) || ')) '
      ELSE   --Organizations and Contacts              
         ' numTerritoryId IN (select F.numTerritory from ForReportsByTerritory F where F.numuserCntid=' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15) || ' and F.numDomainid=' || SUBSTR(CAST(v_numDomainId AS VARCHAR(15)),1,15) || ' and F.tintType=' || SUBSTR(CAST(v_tintType AS VARCHAR(15)),1,15) || ')  AND ((bitPublicFlag = false) OR (bitPublicFlag = true an
d numDivRecOwner = ' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(30)),1,30) || ')) '
      END
   END
   ||
   CASE v_bitStandardFilters
   WHEN '1' THEN ' AND ' || CASE v_vcStandardFilterCriteria
      WHEN 'All' THEN
         CASE    WHEN v_vcStandardFilterStartDate <> '' AND v_vcStandardFilterEndDate <> ''  THEN
            ' dtRecordCreatedOn >= ' || '''' || coalesce(v_vcStandardFilterStartDate,'') || '''' || ' AND dtRecordCreatedOn <= ' || '''' || coalesce(v_vcStandardFilterEndDate,'') || '''' || ' OR dtRecordLastModifiedOn >= ' || '''' || coalesce(v_vcStandardFilterStartDate,'') || '''' || '          
               AND dtRecordLastModifiedOn <= ' || '''' || coalesce(v_vcStandardFilterEndDate,'') || ''''
         WHEN v_vcStandardFilterEndDate <> '' THEN
            ' dtRecordCreatedOn <= ' || '''' || coalesce(v_vcStandardFilterEndDate,'') || '''' || ' AND dtRecordLastModifiedOn <= ' || '''' || coalesce(v_vcStandardFilterEndDate,'') || ''''
         ELSE
            ' dtRecordCreatedOn >= ' || '''' || coalesce(v_vcStandardFilterStartDate,'') || '''' || ' AND dtRecordLastModifiedOn >= ' || '''' || coalesce(v_vcStandardFilterStartDate,'') || ''''
         END
      ELSE
         CASE    WHEN v_vcStandardFilterStartDate <> '' AND v_vcStandardFilterEndDate <> ''  THEN
            coalesce(v_vcStandardFilterCriteria,'') || ' >= ' || '''' || coalesce(v_vcStandardFilterStartDate,'') || '''' || ' AND ' || coalesce(v_vcStandardFilterCriteria,'') || ' <= ' || '''' || coalesce(v_vcStandardFilterEndDate,'') || ''''
         WHEN v_vcStandardFilterEndDate <> '' THEN
            coalesce(v_vcStandardFilterCriteria,'') || ' <= ' || '''' || coalesce(v_vcStandardFilterEndDate,'') || ''''
         ELSE
            coalesce(v_vcStandardFilterCriteria,'') || ' >= ' || '''' || coalesce(v_vcStandardFilterStartDate,'') || ''''
         END
      END
   ELSE ''
   END;                                                      
                                                      
   RAISE NOTICE '%','vcReportStdFilterWhereClause: ' || coalesce(v_vcReportStdFilterWhereClause,'');                                                      
                                                 
   select   COALESCE(coalesce(v_vcReportColumnNames,'') || ', ','') || vcDbFieldName || ' AS [' || vcScrFieldName || ']' INTO v_vcReportColumnNames FROM SWF_OpenXml(v_iDoc,'/CustRptConfig/ColAndAdvFilter/vcDbRow','vcDbFieldName |vcScrFieldName |numOrderOfDisplay') SWA_OpenXml(vcDbFieldName VARCHAR(50),vcScrFieldName VARCHAR(50),numOrderOfDisplay INTEGER)    ORDER BY numOrderOfDisplay Asc LIMIT 100;                                                        
                    
                                           
   select   COALESCE(coalesce(v_vcAggregateReportColumns,'') || '|','') ||
   SUBSTR(CAST(bitSumAggregation As CHAR(1)),1,1) || ',' || SUBSTR(CAST(bitAvgAggregation As CHAR(1)),1,1) || ',' || SUBSTR(CAST(bitLValueAggregation As CHAR(1)),1,1) || ',' || SUBSTR(CAST(bitSValueAggregation As CHAR(1)),1,1) INTO v_vcAggregateReportColumns FROM SWF_OpenXml(v_iDoc,'/CustRptConfig/ColAndAdvFilter/vcDbRow','vcDbFieldName |vcScrFieldName |bitSumAggregation |bitAvgAggregation |bitLValueAggregation |bitSValueAggregation |numOrderOfDisplay') SWA_OpenXml(vcDbFieldName VARCHAR(50),vcScrFieldName VARCHAR(50),bitSumAggregation BOOLEAN,
   bitAvgAggregation BOOLEAN,bitLValueAggregation BOOLEAN,bitSValueAggregation BOOLEAN,
   numOrderOfDisplay INTEGER)    ORDER BY numOrderOfDisplay Asc LIMIT 100;                                             
                                                       
   RAISE NOTICE '%','@vcAggregateReportColumns: ' || coalesce(v_vcAggregateReportColumns,'');                                          
                                          
   select   COALESCE(coalesce(v_vcReportAdvFilterWhereClause,'') || CASE WHEN v_vcAdvFilterJoining = 'A' THEN ' AND ' ELSE ' OR ' END,'') ||
   vcDbFieldName  ||
   CASE vcAdvFilterOperator
   WHEN 'EQ' THEN ' = ' || '''' || vcAdvFilterValue || ''''
   WHEN 'LT' THEN ' < ' || '''' || vcAdvFilterValue || ''''
   WHEN 'GT' THEN ' > ' || '''' || vcAdvFilterValue || ''''
   WHEN 'LEQ' THEN ' <= ' || '''' || vcAdvFilterValue || ''''
   WHEN 'GEQ' THEN ' >= ' || '''' || vcAdvFilterValue || ''''
   WHEN 'NEQ' THEN ' <> ' || '''' || vcAdvFilterValue || ''''
   WHEN 'STW' THEN ' LIKE ' || '''%' || vcAdvFilterValue || ''''
   WHEN 'LIKE' THEN ' LIKE ' || '''%' || vcAdvFilterValue || '%'''
   WHEN 'NOT LIKE' THEN ' NOT LIKE ' || '''%' || vcAdvFilterValue || '%'''
   END INTO v_vcReportAdvFilterWhereClause FROM SWF_OpenXml(v_iDoc,'/CustRptConfig/ColAndAdvFilter/vcDbRow','vcDbFieldName |vcAdvFilterOperator |vcAdvFilterValue') SWA_OpenXml(vcDbFieldName VARCHAR(50),vcAdvFilterOperator VARCHAR(10),vcAdvFilterValue VARCHAR(50)) WHERE vcAdvFilterOperator <> '0';                                        
                                                  
   RAISE NOTICE '%','vcReportColumnNames: ' || coalesce(v_vcReportColumnNames,'');                           
   v_vcReportAdvFilterWhereClause := '(' || coalesce(v_vcReportAdvFilterWhereClause,'') ||  ')';                                 
   RAISE NOTICE '%','vcReportAdvFilterWhereClause: ' || coalesce(v_vcReportAdvFilterWhereClause,'');                                                      
                                                      
   v_vcReportWhereClause := ' WHERE ' || coalesce(v_vcReportStdFilterWhereClause,'') || CASE WHEN v_vcReportAdvFilterWhereClause <> '' THEN '          
                                AND ' || coalesce(v_vcReportAdvFilterWhereClause,'') ELSE '' END || CASE WHEN v_vcReportDataTypeWhereClause <> '' THEN v_vcReportDataTypeWhereClause ELSE '' END || '        
                                AND numDomainId = ' || SUBSTR(Cast(v_numDomainId AS VARCHAR(30)),1,30);        
                                        
   PERFORM SWF_Xml_RemoveDocument(v_iDoc);                                       
                                    
   IF v_ReportType  = 2 then
      RAISE NOTICE '%',('usp_OppItemCustomFields ' || SUBSTR(Cast(v_numRefreshTimeInterval AS VARCHAR(30)),1,30));
      SWV_ExecDyn := 'usp_OppItemCustomFields ' || coalesce(v_numRefreshTimeInterval,'');
      EXECUTE SWV_ExecDyn;
   ELSEIF v_ReportType  = 1
   then
 
      RAISE NOTICE '%',('usp_OrganizationContactsCustomFields ' || SUBSTR(Cast(v_numRefreshTimeInterval AS VARCHAR(30)),1,30));
      SWV_ExecDyn := 'usp_OrganizationContactsCustomFields ' || coalesce(v_numRefreshTimeInterval,'');
      EXECUTE SWV_ExecDyn;
   end if;                                    
   RAISE NOTICE '%','vcReportWhereClause: ' || coalesce(v_vcReportWhereClause,'');                                                         
                                                       
   v_vcCustomReportQuery := SUBSTR('SELECT ' || coalesce(v_vcReportColumnNames,''),1,3900);                           
   v_vcCustomReportQueryMiddle := SUBSTR('SELECT ' || coalesce(v_vcReportColumnNames,''),3901,4000);                           
   v_vcCustomReportQueryTrailer := ' FROM ' || coalesce(v_vcViewName,'') || ' ' || coalesce(v_vcReportWhereClause,'');                          
                                                      
   RAISE NOTICE '%','vcCustomReportQuery: ' || coalesce(v_vcCustomReportQuery,'') || coalesce(v_vcCustomReportQueryMiddle,'') || coalesce(v_vcCustomReportQueryTrailer,'');                                                   
                                     
                                    
   EXECUTE coalesce(v_vcCustomReportQuery,'') || coalesce(v_vcCustomReportQueryMiddle,'') || coalesce(v_vcCustomReportQueryTrailer,'');                                          
                                          
    
   SWV_ExecDyn := 'SELECT ''' ||  coalesce(v_vcAggregateReportColumns,'') || ''' AS AggregateReportColumns';
   OPEN SWV_RefCur FOR EXECUTE SWV_ExecDyn;       
   open SWV_RefCur2 for
   SELECT v_bitRecordCountSummation as RecordCountSummation;
   RETURN;
END; $$;


