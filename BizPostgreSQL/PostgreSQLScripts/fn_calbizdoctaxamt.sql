CREATE OR REPLACE FUNCTION fn_CalBizDocTaxAmt
(
	v_numDomainID NUMERIC(9,0),
	v_numTaxItemID NUMERIC(9,0),
	v_numOppID NUMERIC(9,0),
	v_numOppItemID NUMERIC(9,0),
	v_tintMode SMALLINT,
	v_monAmount DOUBLE PRECISION,
	v_numUnitHour DOUBLE PRECISION
)
RETURNS DECIMAL(20,5) LANGUAGE plpgsql
   AS $$
   DECLARE
   v_monTaxAmount  DECIMAL(20,5) DEFAULT 0;

   v_tintTaxOperator  SMALLINT;
BEGIN
   v_tintTaxOperator := 0;
   
   IF v_tintMode = 1 then --Order
	
      select   coalesce(tintTaxOperator,0) INTO v_tintTaxOperator FROM OpportunityMaster WHERE numOppId = v_numOppID and numDomainId = v_numDomainID;

		--- Skip tax applicable checking if tintTaxOperator =1 (Forcibly apply sales tax )
      IF v_numTaxItemID = 0 and v_tintTaxOperator = 1 then
		
         select   SUM(coalesce(CASE WHEN OMTI.tintTaxType = 2 THEN coalesce(OMTI.fltPercentage,0)*coalesce(v_numUnitHour,0) ELSE  coalesce(OMTI.fltPercentage,0)*coalesce(v_monAmount,0)/100 END,0)) INTO v_monTaxAmount FROM
         OpportunityMasterTaxItems OMTI WHERE
         OMTI.numOppId = v_numOppID
         AND OMTI.numTaxItemID = 0;
      ELSEIF v_numTaxItemID = 0 and v_tintTaxOperator = 2
      then
		
         v_monTaxAmount := 0;
      ELSEIF(SELECT COUNT(*) FROM OpportunityItemsTaxItems WHERE numOppId = v_numOppID AND numOppItemID = v_numOppItemID AND numTaxItemID = v_numTaxItemID) > 0
      then
		
         select   SUM(coalesce(CASE WHEN OMTI.tintTaxType = 2 THEN coalesce(OMTI.fltPercentage,0)*coalesce(v_numUnitHour,0) ELSE  coalesce(OMTI.fltPercentage,0)*coalesce(v_monAmount,0)/100 END,0)) INTO v_monTaxAmount FROM
         OpportunityItemsTaxItems OITI
         INNER JOIn
         OpportunityMasterTaxItems OMTI
         ON
         OITI.numTaxItemID = OMTI.numTaxItemID
         AND coalesce(OITI.numTaxID,0) = coalesce(OMTI.numTaxID,0) WHERE
         OMTI.numOppId = v_numOppID
         AND OITI.numOppItemID = v_numOppItemID
         AND OITI.numTaxItemID = v_numTaxItemID;
      end if;
   end if;

   IF v_tintMode = 2 then --Return RMA
	
      IF(SELECT COUNT(*) FROM OpportunityItemsTaxItems WHERE numReturnItemID = v_numOppItemID and numTaxItemID = v_numTaxItemID) > 0 then
		
         select   SUM(coalesce(CASE WHEN OMTI.tintTaxType = 2 THEN coalesce(OMTI.fltPercentage,0)*coalesce(v_numUnitHour,0) ELSE  coalesce(OMTI.fltPercentage,0)*coalesce(v_monAmount,0)/100 END,0)) INTO v_monTaxAmount FROM
         OpportunityItemsTaxItems OITI
         INNER JOIn
         OpportunityMasterTaxItems OMTI
         ON
         OITI.numTaxItemID = OMTI.numTaxItemID
         AND coalesce(OITI.numTaxID,0) = coalesce(OMTI.numTaxID,0) WHERE
         OITI.numReturnHeaderID = v_numOppID
         AND OITI.numTaxItemID = v_numTaxItemID
         AND OITI.numReturnItemID = v_numOppItemID;
      end if;
   end if;

   RETURN coalesce(v_monTaxAmount,0);
END; $$;

