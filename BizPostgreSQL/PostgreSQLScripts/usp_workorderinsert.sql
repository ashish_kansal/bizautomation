-- Stored procedure definition script USP_WorkOrderInsert for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_WorkOrderInsert(v_numItemCode NUMERIC(9,0),
v_numUnitHour NUMERIC(9,0),
v_numWarehouseItmsID NUMERIC(9,0),
v_numOppID NUMERIC(9,0) DEFAULT 0,
v_numDomainID NUMERIC(9,0) DEFAULT NULL,
v_numUserCntID NUMERIC(9,0) DEFAULT NULL)
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numWOId  NUMERIC(9,0);
   v_txtItemDesc  VARCHAR(1000);
   v_numWarehouseID  NUMERIC(18,0);
BEGIN
   select   coalesce(txtItemDesc,'') INTO v_txtItemDesc FROM Item WHERE numItemCode = v_numItemCode;
   select   numWareHouseID INTO v_numWarehouseID FROM WareHouseItems WHERE numWareHouseItemID = v_numWarehouseItmsID;

   DROP TABLE IF EXISTS tt_TEMPITEM CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPITEM
   (
      numItemKitID NUMERIC(18,0),
      numItemCode NUMERIC(18,0),
      numWarehouseItmsID NUMERIC(18,0),
      numCalculatedQty DOUBLE PRECISION,
      txtItemDesc VARCHAR(500),
      sintOrder INTEGER,
      numUOMId NUMERIC(18,0),
      numQtyItemsReq DOUBLE PRECISION,
      charItemType CHAR
   );

   INSERT INTO tt_TEMPITEM(numItemKitID
		,numItemCode
		,numCalculatedQty
		,numWarehouseItmsID
		,txtItemDesc
		,sintOrder
		,numQtyItemsReq
		,numUOMId
		,charItemType) with recursive CTE(numItemKitID,numItemCode,numCalculatedQty,txtItemDesc,sintOrder,numUOMId,
   numQtyItemsReq,charItemType)
   AS(SELECT
   CAST(v_numItemCode AS NUMERIC(18,0)) AS numItemKitID
			,numItemCode AS numItemCode
			,CAST(((DTL.numQtyItemsReq*coalesce(fn_UOMConversion(DTL.numUOMID,Item.numItemCode,Item.numDomainID,Item.numBaseUnit),1))*v_numUnitHour) AS DOUBLE PRECISION) AS numCalculatedQty
			,coalesce(DTL.vcItemDesc,txtItemDesc) AS txtItemDesc
			,coalesce(DTL.sintOrder,0) AS sintOrder
			,DTL.numUOMID AS numUOMId
			,DTL.numQtyItemsReq AS numQtyItemsReq
			,Item.charItemType AS charItemType
   FROM
   Item
   INNER JOIN
   ItemDetails DTL
   ON
   numChildItemID = numItemCode
   WHERE
   numItemKitID = v_numItemCode
   UNION ALL
   SELECT
   CAST(DTL.numItemKitID AS NUMERIC(18,0)) AS numItemKitID
			,i.numItemCode AS numItemCode
			,CAST(((DTL.numQtyItemsReq*coalesce(fn_UOMConversion(DTL.numUOMID,i.numItemCode,i.numDomainID,i.numBaseUnit),1))*c.numCalculatedQty) AS DOUBLE PRECISION) AS numCalculatedQty
			,coalesce(DTL.vcItemDesc,i.txtItemDesc) AS txtItemDesc
			,coalesce(DTL.sintOrder,0) AS sintOrder
			,DTL.numUOMID AS numUOMId
			,DTL.numQtyItemsReq AS numQtyItemsReq
			,i.charItemType AS charItemType
   FROM
   Item i
   INNER JOIN
   ItemDetails DTL
   ON
   DTL.numChildItemID = i.numItemCode
   INNER JOIN
   CTE c
   ON
   DTL.numItemKitID = c.numItemCode) SELECT
   numItemKitID
		,numItemCode
		,numCalculatedQty
		,coalesce((SELECT  numWareHouseItemID FROM WareHouseItems WHERE numItemID = numItemCode AND numWareHouseID = v_numWarehouseID ORDER BY numWareHouseItemID LIMIT 1),0)
		,txtItemDesc
		,sintOrder
		,numQtyItemsReq
		,numUOMId
		,charItemType
   FROM
   CTE;


   IF EXISTS(SELECT numItemKitID FROM tt_TEMPITEM WHERE charItemType = 'P' AND coalesce(numWarehouseItmsID,0) = 0) then
	
      RAISE NOTICE 'SELECTED_WAREHOUSE_IS_NOT_AVAILABLE_FOR_ALL_CHILD_ITEMS';
      RETURN;
   ELSE
      INSERT INTO WorkOrder(numItemCode
			,numQtyItemsReq
			,numWareHouseItemId
			,numCreatedBy
			,bintCreatedDate
			,numDomainId
			,numWOStatus
			,numOppId
			,vcItemDesc)
		VALUES(v_numItemCode
			,v_numUnitHour
			,v_numWarehouseItmsID
			,v_numUserCntID
			,TIMEZONE('UTC',now())
			,v_numDomainID
			,0
			,v_numOppID
			,v_txtItemDesc);
		
      v_numWOId := CURRVAL('WorkOrder_seq');
      INSERT  INTO WorkOrderDetails(numWOId
			,numItemKitID
			,numChildItemID
			,numQtyItemsReq
			,numWarehouseItemID
			,vcItemDesc
			,sintOrder
			,numQtyItemsReq_Orig
			,numUOMId)
      SELECT
      v_numWOId
			,numItemKitID
			,numItemCode
			,numCalculatedQty
			,numWarehouseItmsID
			,txtItemDesc
			,sintOrder
			,numQtyItemsReq
			,numUOMId
      FROM
      tt_TEMPITEM;
   end if;
END; $$;



