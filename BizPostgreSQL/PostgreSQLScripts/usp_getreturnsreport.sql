-- Stored procedure definition script USP_GetReturnsReport for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_GetReturnsReport(v_numDomainId NUMERIC(9,0)  DEFAULT 0,
               v_CurrentPage INTEGER DEFAULT NULL,
               v_PageSize    INTEGER DEFAULT NULL,
               INOUT v_TotRecs     INTEGER   DEFAULT NULL,
               v_tintMode SMALLINT DEFAULT 0,
               v_numDivisionID NUMERIC(9,0) DEFAULT 0, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
    /*paging logic*/
   AS $$
   DECLARE
   v_firstRec  INTEGER;
   v_lastRec  INTEGER;
   v_numListItemID  NUMERIC(9,0);
BEGIN
   v_firstRec :=(v_CurrentPage::bigint
   -1)*v_PageSize::bigint;
   v_lastRec :=(v_CurrentPage::bigint*v_PageSize::bigint+1);
                      
   IF v_tintMode = 0 then
      select   numListItemID INTO v_numListItemID FROM   Listdetails WHERE  vcData = 'Returned' AND numListID = 49 AND constFlag = true;
   ELSEIF v_tintMode = 1
   then
      select   numListItemID INTO v_numListItemID FROM   Listdetails WHERE  vcData = 'Pending' AND numListID = 49 AND constFlag = true;
   end if;
                      
    
   RAISE NOTICE '%',v_numListItemID;
   open SWV_RefCur for
   with  salesreturn
   AS(SELECT Row_number()
   OVER(ORDER BY opp.numItemCode) AS row,
                    I.vcItemName,
                    opp.vcItemDesc AS itemdesc,
                    opp.numUnitHour AS quantity,
                    r.numQtyToReturn AS quantitytoreturn,
                    r.numqtyreturned AS quantityreturned,
                    r.numreturnstatus,
                    r.numreasonforreturnid,
                    (r.monprice*r.numtotalqty) AS AmountSold,
                    (r.monprice*r.numqtyreturned) AS AmountReturned,
                    (r.numqtyreturned*100/r.numtotalqty) AS ReturnPercent,
                    opp.monPrice,
                    opp.monTotAmount,
                    r.numtotalqty,
                    '' AS serial,
                    I.vcModelID
   FROM OpportunityItems AS opp
   INNER JOIN Returns AS r
   ON opp.numOppId = r.numOppID
   INNER JOIN OpportunityMaster om
   ON om.numOppId = opp.numOppId
   INNER JOIN Item I
   ON I.numItemCode = opp.numItemCode
   WHERE  opp.numItemCode = r.numitemcode
   AND om.numDomainId = v_numDomainId
   AND r.numreturnstatus = v_numListItemID
   AND (v_numDivisionID = 0 OR om.numDivisionId = v_numDivisionID))
   SELECT *,GetListIemName(numreasonforreturnid) AS reasonforreturn,GetListIemName(numreturnstatus) AS ReturnStatus
   FROM   salesreturn
   WHERE  row > v_firstRec
   AND row < v_lastRec;
   RETURN;
END; $$;



