-- Stored procedure definition script USP_ConEmpListBasedonTeam for PostgreSQL
CREATE OR REPLACE FUNCTION USP_ConEmpListBasedonTeam(v_numDomainID NUMERIC(9,0) DEFAULT 0,
    v_numUserCntID NUMERIC(9,0) DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT
   DISTINCT A.numContactId,
        CONCAT(A.vcFirstName,' ',A.vcLastname) AS "vcUserName",
		coalesce(AGM.tintGroupType,0) AS "tintGroupType"
   FROM
   UserMaster UM
   INNER JOIN
   AuthenticationGroupMaster AGM
   ON
   UM.numGroupID = AGM.numGroupID
   JOIN
   AdditionalContactsInformation A
   ON
   UM.numUserDetailId = A.numContactId
   LEFT OUTER JOIN
   UserTeams UT
   ON
   UT.numUserCntID = UM.numUserDetailId
   WHERE
   UM.numDomainID = v_numDomainID
   AND UM.intAssociate = 1
   AND UT.numTeam IN(SELECT cast(numTeam as NUMERIC(18,0)) FROM UserTeams WHERE numUserCntID = v_numUserCntID AND numDomainID = v_numDomainID);
END; $$;












