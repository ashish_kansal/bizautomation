-- Stored procedure definition script USP_InsertEmailMaster for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE  OR REPLACE FUNCTION USP_InsertEmailMaster(v_vcEmailId VARCHAR(2000),
    v_numDomainId NUMERIC(18,0))
RETURNS VOID LANGUAGE plpgsql --DECLARE @vcEmailId AS VARCHAR(2000)
   AS $$
   DECLARE
   v_Email  VARCHAR(200);
   v_EmailName  VARCHAR(200);
   v_EmailAdd  VARCHAR(200);
   v_StartPos  INTEGER;
   v_EndPos  INTEGER;
   v_numEmailid  NUMERIC(9,0);
--DECLARE @numDomainId AS NUMERIC(18, 0)
--DECLARE @numEmailId AS NUMERIC(18,0)
   v_vcNewEmailId  VARCHAR(2000);
   v_numcontactid  NUMERIC(9,0);
BEGIN

--SET @numDomainId = NULL
--SET @vcNewEmailId ='|pinkal.patel@bizautomation.com,Pinkal Patel|pinkalpatel2007@rediff.com,Pinkal Patel|pinkalpatel2099@rediff.com,'
--SET @vcEmailId = '|pinkal.patel@bizautomation.com,Pinkal Patel|pinkalpatel2007@rediff.com,Pinkal Patel|pinkalpatel2099@rediff.com,'
   v_StartPos := 0;
   v_vcEmailId := coalesce(v_vcEmailId,'') || '#^#';
   IF v_vcEmailId <> '' then
        
      WHILE POSITION('#^#' IN v_vcEmailId) > 0 LOOP
         v_Email := LTRIM(RTRIM(SUBSTR(v_vcEmailId,1,POSITION('#^#' IN v_vcEmailId)+2)));
         RAISE NOTICE '%',v_Email;
         v_numEmailid := 0;
         IF POSITION('$^$' IN v_Email) > 0 then
                        
            v_EmailName := LTRIM(RTRIM(SUBSTR(v_Email,0,POSITION('$^$' IN v_Email))));
            v_EmailAdd := LTRIM(RTRIM(SUBSTR(v_Email,LENGTH(v_EmailName)+1,POSITION('$^$' IN v_Email)+LENGTH(v_Email) -1)));
            v_EmailAdd := REPLACE(v_EmailAdd,'$^$','');
            v_EmailAdd := REPLACE(v_EmailAdd,'#^#','');
						---------Start Insert Into Email master-----------------------------------
            select   COUNT(numEmailId) INTO v_numEmailid FROM    EmailMaster WHERE   vcEmailId = v_EmailAdd
            AND numDomainID = v_numDomainId;
                        --PRINT @numEmailid
            IF v_numEmailid = 0 then
               select   numContactId INTO v_numcontactid from AdditionalContactsInformation where vcEmail = v_EmailAdd
               AND numDomainID = v_numDomainId    LIMIT 1;
               INSERT  INTO EmailMaster(vcEmailId,
                                              vcName,
                                              numDomainID,numContactID)
                                    VALUES(v_EmailAdd,
                                              v_EmailName,
                                              v_numDomainId,v_numcontactid);
            Else
               update EmailMaster set vcName = v_EmailName  WHERE vcEmailId = v_EmailAdd
               AND numDomainID = v_numDomainId;
            end if;
         end if;
         v_vcEmailId := LTRIM(RTRIM(SUBSTR(v_vcEmailId,POSITION('#^#' IN v_vcEmailId)+3,LENGTH(v_vcEmailId))));
      END LOOP;
   end if;
   RETURN;
END; $$;
    
               
                 



