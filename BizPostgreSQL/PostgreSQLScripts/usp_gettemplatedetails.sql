-- Stored procedure definition script usp_GetTemplateDetails for PostgreSQL
CREATE OR REPLACE FUNCTION usp_GetTemplateDetails(v_numDomainID NUMERIC,  
v_numUserID NUMERIC,  
v_numPageID NUMERIC  
--
,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for select numTemplateID , vcTemplateName , bintDateTimeUpload , vcFileName , numPageID  from TemplateDocuments where numDomainid = v_numDomainID and numPageID = v_numPageID and bitStatus = false;
END; $$;