-- Stored procedure definition script USP_GetProbyDivId for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_GetProbyDivId(v_numdivisionid NUMERIC(9,0)  ,  
v_byteMode SMALLINT,  
v_numDomainId NUMERIC(9,0), INOUT SWV_RefCur refcursor)
LANGUAGE plpgsql
   AS $$
BEGIN
   if v_byteMode = 1 then

      open SWV_RefCur for
      select numProId,vcProjectName from ProjectsMaster where  numDivisionId = v_numdivisionid  and tintProStatus = 0  and numdomainId = v_numDomainId;
   end if;  
  
   if v_byteMode = 0 then

      open SWV_RefCur for
      select numProId,vcProjectName from ProjectsMaster where numDivisionId = v_numdivisionid  and tintProStatus <> 1 and numdomainId = v_numDomainId;
   end if;

   if v_byteMode = 3 then

      open SWV_RefCur for
      select numProId,vcProjectName from ProjectsMaster where  tintProStatus <> 1 and numdomainId = v_numDomainId;
   end if;


   IF v_byteMode = 2 then

      open SWV_RefCur for
      SELECT numProId,vcProjectName
      FROM ProjectsMaster
      WHERE numDivisionId = v_numdivisionid
      AND tintProStatus <> 1
      AND numdomainId = v_numDomainId
      AND coalesce(numAccountID,0) <> 0;
   end if;
   RETURN;
END; $$;


