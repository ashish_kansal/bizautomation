-- Stored procedure definition script USP_ManageAlertEmails for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ManageAlertEmails(v_numAlertDTLID NUMERIC(9,0) DEFAULT 0,  
v_numAlertEmailID NUMERIC(9,0) DEFAULT 0,  
v_vcEmailID VARCHAR(100) DEFAULT '',  
v_byteMode SMALLINT DEFAULT 0 ,
v_numDomainID NUMERIC(9,0) DEFAULT NULL)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   if v_byteMode = 1 then

      delete from AlertEmailDTL where numAlertEmailID = v_numAlertEmailID and numDomainID = v_numDomainID;
   end if;  
  
   if v_byteMode = 2 then

      insert into  AlertEmailDTL(numAlertDTLid,vcEmailID,numDomainID)
values(v_numAlertDTLID,v_vcEmailID,v_numDomainID);
   end if;
   RETURN;
END; $$;


