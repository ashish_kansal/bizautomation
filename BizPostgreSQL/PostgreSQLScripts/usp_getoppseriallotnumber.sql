-- Stored procedure definition script USP_GetOppSerialLotNumber for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_GetOppSerialLotNumber(v_numOppID NUMERIC(18,0),
	v_numOppItemID NUMERIC(18,0),
	v_numWarehouseItemID NUMERIC(18,0), INOUT SWV_RefCur refcursor , INOUT SWV_RefCur2 refcursor)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numDomainID  NUMERIC(18,0);
   v_numItemID  NUMERIC(18,0);
   v_numWarehouseID  NUMERIC(18,0);
BEGIN

	
   select   numDomainId INTO v_numDomainID FROM OpportunityMaster WHERE numOppId = v_numOppID;
   select   numItemID, numWareHouseID INTO v_numItemID,v_numWarehouseID FROM WareHouseItems WHERE numWareHouseItemID = v_numWarehouseItemID;

	-- Available Serail Lot Numbers
   open SWV_RefCur for
   SELECT
   ROW_NUMBER() OVER(ORDER BY dExpirationDate DESC) AS OrderNo,
		numWareHouseItmsDTLID,
		vcSerialNo,
		numQty,
		FormatedDateFromDate(dExpirationDate,v_numDomainID) AS dExpirationDate
   FROM
   WareHouseItmsDTL
   WHERE
   numWareHouseItemID IN(SELECT numWareHouseItemID FROM WareHouseItems WHERE numItemID = v_numItemID AND numWareHouseID = v_numWarehouseID)
   AND coalesce(numQty,0) > 0
   ORDER BY
   dExpirationDate DESC;

    -- Selected Serail Lot Numbers
   open SWV_RefCur2 for
   SELECT
   ROW_NUMBER() OVER(ORDER BY WareHouseItmsDTL.dExpirationDate DESC) AS OrderNo,
		WareHouseItmsDTL.numWareHouseItmsDTLID,
		WareHouseItmsDTL.vcSerialNo,
		FormatedDateFromDate(WareHouseItmsDTL.dExpirationDate,v_numDomainID) AS dExpirationDate,
		OppWarehouseSerializedItem.numQty,
		OppWarehouseSerializedItem.numOppBizDocsId
   FROM
   OppWarehouseSerializedItem
   JOIN
   WareHouseItmsDTL
   ON
   OppWarehouseSerializedItem.numWarehouseItmsDTLID = WareHouseItmsDTL.numWareHouseItmsDTLID
   WHERE
   numOppID = v_numOppID AND
   numOppItemID = v_numOppItemID
   ORDER BY
   WareHouseItmsDTL.dExpirationDate DESC;
   RETURN;
END; $$;





