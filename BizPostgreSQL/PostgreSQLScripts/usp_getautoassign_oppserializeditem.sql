-- Stored procedure definition script USP_GetAutoAssign_OppSerializedItem for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetAutoAssign_OppSerializedItem(v_numOppID NUMERIC(9,0) DEFAULT 0,  
    v_numOppBizDocsId NUMERIC(9,0) DEFAULT 0,
    v_numWarehouseItmsID NUMERIC(9,0) DEFAULT 0,
	v_bitSerialized BOOLEAN DEFAULT NULL,
    v_bitLotNo BOOLEAN DEFAULT NULL,
	v_numUnitHour DOUBLE PRECISION DEFAULT NULL,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_vcSerialNoList  VARCHAR(2000);
   v_maxWareID  NUMERIC(9,0);
   v_minWareID  NUMERIC(9,0);
   v_vcSerialNo  VARCHAR(100);
   v_numWarehouseItmsDTLID  NUMERIC(9,0);
   v_numQty  NUMERIC(9,0);
   v_numUseQty  NUMERIC(9,0);
BEGIN
   v_vcSerialNoList := '';

   BEGIN
      CREATE TEMP SEQUENCE tt_tempWareHouseItmsDTL_seq INCREMENT BY 1 START WITH 1;
      EXCEPTION WHEN OTHERS THEN
         NULL;
   END;
   drop table IF EXISTS tt_TEMPWAREHOUSEITMSDTL CASCADE;
   Create TEMPORARY TABLE tt_TEMPWAREHOUSEITMSDTL 
   (
      ID NUMERIC(18,0) DEFAULT NEXTVAL('tt_tempWareHouseItmsDTL_seq') NOT NULL,
      vcSerialNo VARCHAR(100),
      numQty NUMERIC(18,0),
      numWareHouseItmsDTLID NUMERIC(18,0)
   );   

   INSERT INTO tt_TEMPWAREHOUSEITMSDTL(vcSerialNo,numWarehouseItmsDTLID,numQty)
   SELECT
   vcSerialNo,numWareHouseItmsDTLID,coalesce(numQty,0) as TotalQty
   FROM
   WareHouseItmsDTL
   WHERE
   coalesce(numQty,0) > 0
   AND numWareHouseItemID = v_numWarehouseItmsID
   AND numWareHouseItmsDTLID NOT IN(SELECT numWarehouseItmsDTLID FROM OppWarehouseSerializedItem WHERE numOppID = v_numOppID AND numWarehouseItmsID = v_numWarehouseItmsID AND numOppBizDocsId = v_numOppBizDocsId)
   ORDER BY TotalQty desc;

   IF ((Select sum(numQty) from tt_TEMPWAREHOUSEITMSDTL) >= v_numUnitHour) then
      select   max(ID), min(ID) INTO v_maxWareID,v_minWareID FROM  tt_TEMPWAREHOUSEITMSDTL;
      WHILE (v_numUnitHour > 0 and v_minWareID <= v_maxWareID) LOOP
         select   vcSerialNo, numWarehouseItmsDTLID, numQty INTO v_vcSerialNo,v_numWarehouseItmsDTLID,v_numQty FROM  tt_TEMPWAREHOUSEITMSDTL where ID = v_minWareID;
         IF v_numUnitHour >= v_numQty then
		
            v_numUseQty := v_numQty;
            v_numUnitHour := v_numUnitHour -v_numQty;
         ELSEIF v_numUnitHour < v_numQty
         then
		
            v_numUseQty := v_numUnitHour;
            v_numUnitHour := 0;
         end if;
         IF v_bitSerialized = true then
            v_vcSerialNoList := coalesce(v_vcSerialNoList,'') || coalesce(v_vcSerialNo,'') || ',';
         ELSEIF v_bitLotNo = true
         then
            v_vcSerialNoList := coalesce(v_vcSerialNoList,'') || coalesce(v_vcSerialNo,'') || '(' || SUBSTR(Cast(v_numUseQty as VARCHAR(10)),1,10) || '),';
         end if;
         v_minWareID := v_minWareID+1;
      END LOOP;
   end if;

   drop table IF EXISTS tt_TEMPWAREHOUSEITMSDTL CASCADE;

   open SWV_RefCur for select SUBSTR(v_vcSerialNoList,0,LENGTH(v_vcSerialNoList)) as vcSerialNoList;
END; $$;












