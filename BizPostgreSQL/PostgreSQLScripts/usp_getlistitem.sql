-- Stored procedure definition script USP_GetListItem for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetListItem(v_numListItemID NUMERIC(9,0) DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for select caoalesce(vcData,'') as "vcData" from Listdetails where numListItemID = v_numListItemID;
END; $$;












