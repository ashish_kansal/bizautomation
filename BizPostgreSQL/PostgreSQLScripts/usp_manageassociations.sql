-- Stored procedure definition script usp_ManageAssociations for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION usp_ManageAssociations(v_numAssociationID NUMERIC DEFAULT 0,                          
 v_numAssociatedFromDivID NUMERIC DEFAULT NULL,                          
 v_numCompID NUMERIC DEFAULT NULL,                          
 v_numDivID NUMERIC DEFAULT NULL,                          
 v_bitParentOrg BOOLEAN DEFAULT NULL,                           
 v_bitChildOrg BOOLEAN DEFAULT NULL,                          
 v_numTypeID NUMERIC DEFAULT NULL,                          
 v_bitDeleted BOOLEAN DEFAULT false,                          
 v_numUserCntID NUMERIC DEFAULT NULL,                           
 v_numDomainID NUMERIC DEFAULT NULL,  
 v_bitShareportal BOOLEAN DEFAULT NULL,
 v_bitTypeLabel BOOLEAN DEFAULT NULL,
 v_numAssoTypeLabel NUMERIC(9,0) DEFAULT 0,
 v_numContactID NUMERIC(9,0) DEFAULT NULL, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_Count  INTEGER;         
   v_vcParentOrgName  VARCHAR(100);
BEGIN
   select   numCompanyID INTO v_numCompID FROM DivisionMaster WHERE numDivisionID = v_numDivID;
  
   IF EXISTS(SELECT 1 FROM CompanyAssociations
   WHERE numDomainID = v_numDomainID AND numAssociateFromDivisionID != v_numAssociatedFromDivID AND numDivisionID = v_numDivID AND coalesce(bitChildOrg,false) = true AND v_bitChildOrg = true) then
  
      open SWV_RefCur for
      SELECT 'DuplicateChild';
      RETURN;
   ELSEIF EXISTS(SELECT 1 FROM CompanyAssociations
   WHERE numDomainID = v_numDomainID AND numAssociateFromDivisionID = v_numAssociatedFromDivID AND numDivisionID = v_numDivID)
   then
  
 -- 	SELECT 'Duplicate'
      UPDATE CompanyAssociations SET
      numReferralType = v_numTypeID,bitDeleted = false,bitParentOrg = v_bitParentOrg,
      bitChildOrg = v_bitChildOrg,numModifiedBy = v_numUserCntID,numModifiedOn = TIMEZONE('UTC',now()),
      bitShareportal  = v_bitShareportal
      WHERE numDomainID = v_numDomainID AND numAssociateFromDivisionID = v_numAssociatedFromDivID AND numDivisionID = v_numDivID;
      RETURN;
   ELSE              
      IF v_bitTypeLabel = true then
        
         UPDATE CompanyAssociations SET bitTypeLabel = false WHERE numAssociateFromDivisionID = v_numAssociatedFromDivID;
      end if;
      INSERT INTO CompanyAssociations(numAssociateFromDivisionID, numCompanyID, numDivisionID, bitParentOrg,
            bitChildOrg, numReferralType, bitDeleted, numCreateBy, numCreatedOn,
            numModifiedBy, numModifiedOn, numDomainID,bitShareportal,bitTypeLabel,numAssoTypeLabel,numContactID)
            VALUES(v_numAssociatedFromDivID, v_numCompID, v_numDivID, v_bitParentOrg,
            v_bitChildOrg, v_numTypeID, v_bitDeleted, v_numUserCntID, TIMEZONE('UTC',now()),
            v_numUserCntID, TIMEZONE('UTC',now()), v_numDomainID,v_bitShareportal,v_bitTypeLabel,v_numAssoTypeLabel,v_numContactID);
            
      open SWV_RefCur for
      SELECT '0';
   end if;
END; $$;             



