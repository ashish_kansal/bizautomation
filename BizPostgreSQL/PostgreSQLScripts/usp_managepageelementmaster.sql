-- Stored procedure definition script USP_ManagePageElementMaster for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE PROCEDURE USP_ManagePageElementMaster(v_numElementId NUMERIC(9,0) DEFAULT 0,
    v_vcElementName VARCHAR(50) DEFAULT NULL,
    v_vcUserControlPath VARCHAR(200) DEFAULT NULL,
    v_vcTagName VARCHAR(50) DEFAULT NULL,
    v_bitCustomization BOOLEAN DEFAULT NULL,
    v_bitAdd BOOLEAN DEFAULT false,
    v_bitDelete BOOLEAN DEFAULT false,
    v_bitFlag BOOLEAN DEFAULT false)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numNewElementId  NUMERIC(9,0);
   v_strMsg  VARCHAR(200);
BEGIN
   IF v_bitFlag = false then
      BEGIN
         -- BEGIN TRAN
select   coalesce(MAX(numElementID),200)+1 INTO v_numNewElementId FROM PageElementMaster WHERE numElementID >= 200;
         INSERT  INTO PageElementMaster(numElementID,vcElementName,
                                                     vcUserControlPath,
                                                     vcTagName,
                                                     bitCustomization,
                                                     bitAdd,
                                                     bitDelete)
                VALUES(v_numNewElementId,v_vcElementName,
                          v_vcUserControlPath,
                          v_vcTagName,
                          v_bitCustomization,
                          v_bitAdd,
                          v_bitDelete);
                
         DROP TABLE IF EXISTS tt_TEMPTABLE CASCADE;
         CREATE TEMPORARY TABLE tt_TEMPTABLE 
         ( 
            vcAttributeName VARCHAR(500),
            vcControlType VARCHAR(200),
            vcControlValues VARCHAR(200),
            bitEditor BOOLEAN 
         );
         INSERT  INTO tt_TEMPTABLE
         SELECT  vcAttributeName,
                                vcControlType,
                                vcControlValues,
                                bitEditor
         FROM    PageElementAttributes
         WHERE   numElementID = v_numElementId;
         INSERT  INTO PageElementAttributes
         SELECT  v_numNewElementId,
                                vcAttributeName,
                                vcControlType,
                                vcControlValues,
                                bitEditor
         FROM    tt_TEMPTABLE;
         INSERT  INTO EmailMergeModule(numModuleID,
                                                vcModuleName,
                                                tintModuleType)
                VALUES(v_numNewElementId,
                          v_vcElementName,
                          1);
                
         DROP TABLE IF EXISTS tt_TEMPTABLE1 CASCADE;
         CREATE TEMPORARY TABLE tt_TEMPTABLE1 
         ( 
            vcMergeField VARCHAR(500),
            vcMergeFieldValue VARCHAR(200),
            tintModuleType SMALLINT 
         );
         INSERT  INTO tt_TEMPTABLE1
         SELECT  vcMergeField,
                                vcMergeFieldValue,
                                tintModuleType
         FROM    EmailMergeFields
         WHERE   numModuleID = v_numElementId;
         INSERT  INTO EmailMergeFields
         SELECT  vcMergeField,
                                vcMergeFieldValue,
                                v_numNewElementId,
                                tintModuleType
         FROM    tt_TEMPTABLE1;
         DROP TABLE IF EXISTS tt_TEMPTABLE CASCADE;
         DROP TABLE IF EXISTS tt_TEMPTABLE1 CASCADE;
         -- COMMIT
EXCEPTION WHEN OTHERS THEN
            v_strMsg := SQLERRM;
            RAISE NOTICE '%',v_strMsg;
            -- ROLLBACK 
            RETURN;
      END;
   ELSE
      DELETE  FROM EmailMergeFields
      WHERE   numModuleID = v_numElementId;
      DELETE  FROM EmailMergeModule
      WHERE   numModuleID = v_numElementId;
      DELETE  FROM PageElementDetail
      WHERE   numElementID = v_numElementId;
      DELETE  FROM PageElementAttributes
      WHERE   numElementID = v_numElementId;
      DELETE  FROM PageElementMaster
      WHERE   numElementID = v_numElementId;
   end if;
END; $$;



