CREATE OR REPLACE FUNCTION USP_DeleteItemList(v_numListItemID NUMERIC(9,0) DEFAULT 0,            
v_numListId NUMERIC(9,0) DEFAULT 0)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   IF coalesce((SELECT constFlag FROM Listdetails where numListItemID = v_numListItemID),false) = false then
	
      IF v_numListId = 27 AND EXISTS(SELECT numDomainId FROM AuthoritativeBizDocs WHERE numAuthoritativeSales = v_numListItemID OR numAuthoritativePurchase = v_numListItemID) then
		
         RAISE EXCEPTION 'USED_AS_AUTHORITATIVE_BIZDOC';
      ELSE
         DELETE FROM Listdetails WHERE numListItemID = v_numListItemID;   
         DELETE FROM FieldRelationshipDTL WHERE numPrimaryListItemID = v_numListItemID;
         DELETE FROM FieldRelationshipDTL WHERE numSecondaryListItemID = v_numListItemID;
      end if;
   end if;
   RETURN;
END; $$;


