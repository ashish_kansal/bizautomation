-- Stored procedure definition script USP_ManageException for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ManageException(v_vcMessage TEXT, 
v_numDomainID NUMERIC(9,0), 
v_numUserContactID NUMERIC(9,0), 
v_vcURL VARCHAR(1000), 
v_vcReferrer  VARCHAR(1000), 
v_vcBrowser  VARCHAR(100), 
v_vcBrowserAgent  VARCHAR(500))
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   insert into ExceptionLog(vcMessage, numDomainID, numUserContactID, dtCreatedDate, vcURL, vcReferrer, vcBrowser, vcBrowserAgent)
values(v_vcMessage, v_numDomainID, v_numUserContactID, LOCALTIMESTAMP , v_vcURL, v_vcReferrer, v_vcBrowser, v_vcBrowserAgent);
RETURN;
END; $$;


