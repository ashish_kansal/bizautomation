-- Stored procedure definition script USP_DeleteWorkFlowMaster for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_DeleteWorkFlowMaster(v_numDomainID NUMERIC(18,0),
	v_numWFID NUMERIC(18,0))
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   DELETE FROM WorkFlowTriggerFieldList WHERE numWFID = v_numWFID; 

   DELETE FROM WorkFlowConditionList WHERE numWFID = v_numWFID; 

   DELETE FROM WorkFlowActionUpdateFields WHERE numWFActionID IN(SELECT numWFActionID FROM WorkFlowActionList WHERE numWFID = v_numWFID);

   DELETE FROM WorkFlowActionList WHERE numWFID = v_numWFID; 


   DELETE FROM WorkFlowMaster WHERE numDomainID = v_numDomainID AND numWFID = v_numWFID;
/****** Object:  StoredProcedure [dbo].[USP_DocumentWorkFlow]    Script Date: 07/26/2008 16:15:46 ******/
   RETURN;
END; $$;


