-- Stored procedure definition script USP_InboxTreeSort_UpdateSortOrder for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
Create or replace FUNCTION USP_InboxTreeSort_UpdateSortOrder(v_numSortOrder INTEGER,
    v_numUserCntID INTEGER,        
	v_numDomainID INTEGER,        
	v_NodeID INTEGER,        
	v_ParentId INTEGER)
RETURNS VOID LANGUAGE plpgsql
	-- Add the parameters for the stored procedure here
   AS $$
BEGIN

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	

    -- Insert statements for procedure here
   UPDATE InboxTreeSort
   SET numSortOrder = v_numSortOrder,numParentID = v_ParentId
   WHERE numNodeID = v_NodeID AND
   numDomainID = v_numDomainID AND
   numUserCntID = v_numUserCntID;
   RETURN;
END; $$;

/****** Object:  StoredProcedure [dbo].[USP_InsertCartItem]    Script Date: 11/08/2011 17:53:22 ******/



