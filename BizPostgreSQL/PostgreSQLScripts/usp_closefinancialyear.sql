-- Stored procedure definition script USP_CloseFinancialYear for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021

CREATE OR REPLACE FUNCTION USP_CloseFinancialYear(v_numDomainID NUMERIC(9,0),
v_numFinYearId NUMERIC(9,0), --- ACTIVE FINCIALYEAR 
v_numNextFinYearId NUMERIC(9,0), --- Selected FIN year
v_tintCloseMode SMALLINT)
RETURNS VOID LANGUAGE plpgsql --- Trial Close or Audit Close (1=Tral close , 2 =Audit Close)

   AS $$
   DECLARE
   v_dtFromDate  TIMESTAMP;
   v_dtToYear  TIMESTAMP;
   v_dtOpening  TIMESTAMP;
   v_PROFIT  DECIMAL(20,5);
BEGIN
   select dtPeriodFrom INTO v_dtFromDate from FinancialYear where numFinYearId = v_numFinYearId and  numDomainId = v_numDomainID;
   select dtPeriodTo INTO v_dtToYear from FinancialYear where numFinYearId = v_numFinYearId and  numDomainId = v_numDomainID;
   select dtPeriodFrom INTO v_dtOpening from FinancialYear where numFinYearId = v_numNextFinYearId and  numDomainId = v_numDomainID;


   DELETE FROM ChartAccountOpening WHERE numFinYearId = v_numNextFinYearId AND numDomainID = v_numDomainID;

--Insert New Opening balance for given new financial year for all Asset,liability and Equity Accounts
-- New Opening Balance = (Sum(Debit) - Sum(Credit)) of ledger + Opening Balance of CurrentFinancial Year
   INSERT INTO ChartAccountOpening(numDomainID, numAccountId, numFinYearId, monOpening, dtOpenDate)
   SELECT DISTINCT v_numDomainID,COA.numAccountId,v_numNextFinYearId,
coalesce((SELECT coalesce(SUM(GJD.numDebitAmt),0) -coalesce(SUM(GJD.numCreditAmt),0)
      FROM
      General_Journal_Details GJD INNER JOIN
      General_Journal_Header GJH ON GJD.numJournalId = GJH.numJOurnal_Id AND
      GJD.numDomainId = COA.numDomainId AND
      GJD.numChartAcntId = COA.numAccountId AND
      GJH.datEntry_Date between v_dtFromDate and v_dtToYear),0)/*+
ISNULL((SELECT monOpening FROM ChartAccountOpening CAO WHERE 
CAO.numAccountId=COA.numAccountId AND numFinYearId=@numFinYearId),0)*/,v_dtOpening
   FROM Chart_Of_Accounts COA
   WHERE COA.numDomainId = v_numDomainID AND
	(COA.vcAccountCode ilike '0101%' OR COA.vcAccountCode ilike '0102%' OR COA.vcAccountCode ilike '0105%');

----------------------------
--UPDATE PL account with Profit for Active Finacial Year
----------------------------
   v_PROFIT := 0;

   select   coalesce(SUM(GJD.numDebitAmt),0) -coalesce(SUM(GJD.numCreditAmt),0) INTO v_PROFIT FROM
   General_Journal_Details GJD INNER JOIN
   General_Journal_Header GJH ON GJD.numJournalId = GJH.numJOurnal_Id INNER JOIN
   Chart_Of_Accounts COA ON GJD.numChartAcntId = COA.numAccountId AND
   COA.numDomainId = v_numDomainID AND
   GJD.numDomainId = COA.numDomainId AND
   GJD.numChartAcntId = COA.numAccountId AND
   GJH.datEntry_Date between v_dtFromDate and v_dtToYear AND
(COA.vcAccountCode ilike '0103%' OR COA.vcAccountCode ilike '0104%' OR COA.vcAccountCode ilike '0106%');
--set @PROFIT=@PROFIT*(-1) -- chintan patel said that store profit and loss as it is.

   UPDATE ChartAccountOpening  set monOpening = coalesce(monOpening,0)+coalesce(v_PROFIT,0)
   WHERE numAccountId =(SELECT  numAccountId from Chart_Of_Accounts where numDomainId = v_numDomainID and bitProfitLoss = true LIMIT 1)
   and numDomainID = v_numDomainID and numFinYearId = v_numNextFinYearId;

   IF v_tintCloseMode = 1 then
	
      UPDATE FinancialYear SET bitCloseStatus = true where numFinYearId = v_numFinYearId and numDomainId = v_numDomainID;
   ELSE
      UPDATE FinancialYear SET bitAuditStatus = true where numFinYearId = v_numFinYearId and  numDomainId = v_numDomainID;
   end if;
   RETURN;
END; $$;
--SELECT  DISTINCT CAO.numAccountId,COA.vcAccountName ,monOpening,dtOpenDate FROM ChartAccountOpening CAO INNER JOIN dbo.Chart_Of_Accounts COA ON CAO.numAccountId = COA.numAccountId WHERE CAO.numDomainId=@numDomainID AND numFinYearId=@numNextFinYearId
--select * from Financialyear
--- Created By Anoop jayaraj       
--- Modified By Gangadhar 03/05/2008                                                  


