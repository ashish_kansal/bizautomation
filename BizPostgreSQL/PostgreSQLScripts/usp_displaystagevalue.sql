-- Stored procedure definition script usp_DisplayStageValue for PostgreSQL
CREATE OR REPLACE FUNCTION usp_DisplayStageValue(v_numStagePercentageId NUMERIC   
--
,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT numstagepercentage as "numstagepercentage" FROM stagepercentagemaster WHERE numstagepercentageid = v_numStagePercentageId;
END; $$;












