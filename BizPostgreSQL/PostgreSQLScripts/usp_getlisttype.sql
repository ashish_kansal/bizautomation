-- Stored procedure definition script usp_GetListType for PostgreSQL
CREATE OR REPLACE FUNCTION usp_GetListType(v_numListItemId NUMERIC    --
,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
	open SWV_RefCur for 
	select 
		vcData as "vcData"
	from 
		Listdetails 
	where 
		numListID = 5 
		and numListItemID = v_numListItemId;
END; $$;