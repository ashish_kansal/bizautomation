-- Stored procedure definition script usp_AOIGenerated for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION usp_AOIGenerated(v_numDomainID NUMERIC,
 v_numUserID NUMERIC,
 v_numStartDate NUMERIC DEFAULT 0,  
 v_numEndDate NUMERIC DEFAULT 0,  
 v_numWeek NUMERIC DEFAULT 0,  
 v_numInfoSource NUMERIC DEFAULT 0,  
 v_numRelationShip NUMERIC DEFAULT 0,  
 v_tintRepType SMALLINT DEFAULT 0, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numFullStDate  NUMERIC;  
   v_numFullEndDate  NUMERIC;
BEGIN
   v_numFullStDate := v_numStartDate*1000000;  
   v_numFullEndDate :=(v_numEndDate*1000000)+595959; 
   If v_tintRepType = 1 then  --Monthly Rep  
	
      open SWV_RefCur for
      SELECT AL.numAOIID, AM.vcAOIName, Count(AL.numAOIID) AS countAOI
      FROM AOIContactLink AL, AOIMaster AM, AdditionalContactsInformation ACI, DivisionMaster DM, CompanyInfo CI
      WHERE AM.numAOIID = AL.numAOIID
      AND
      AL.numContactID = ACI.numContactId
      AND
      COALESCE(ACI.bintModifiedDate,ACI.bintCreatedDate) between v_numFullStDate:: BYTEA and v_numFullEndDate:: BYTEA
      AND
      ACI.numDivisionId = DM.numDivisionID
      AND
      DM.numCompanyID = CI.numCompanyId
      AND(CASE
      WHEN v_numInfoSource = 0 THEN 0
      ELSE CI.vcHow
      END)
      =(CASE
      WHEN v_numInfoSource = 0 THEN 0
      ELSE v_numInfoSource
      END)
      AND(CASE
      WHEN v_numRelationShip = 0 THEN 0
      ELSE CI.numCompanyType
      END)
      =(CASE
      WHEN v_numRelationShip = 0 THEN 0
      ELSE v_numRelationShip
      END)
      GROUP BY AL.numAOIID,AM.vcAOIName;
   ELSEIF v_tintRepType = 2
   then --Weekly Rep  
	
      open SWV_RefCur for
      SELECT AL.numAOIID, AM.vcAOIName, Count(AL.numAOIID) AS countAOI
      FROM AOIContactLink AL, AOIMaster AM, AdditionalContactsInformation ACI, DivisionMaster DM, CompanyInfo CI
      WHERE AM.numAOIID = AL.numAOIID
      And
      AL.numContactID = ACI.numContactId
      And
      COALESCE(ACI.bintModifiedDate,ACI.bintCreatedDate) between v_numFullStDate:: BYTEA and v_numFullEndDate:: BYTEA
      And
      ACI.numDivisionId = DM.numDivisionID
      AND
      DM.numCompanyID = CI.numCompanyId
      And(CASE
      WHEN v_numInfoSource = 0 THEN 0
      ELSE CI.vcHow
      END)
      =(CASE
      WHEN v_numInfoSource = 0 THEN 0
      ELSE v_numInfoSource
      END)
      AND(CASE
      WHEN v_numRelationShip = 0 THEN 0
      ELSE CI.numCompanyType
      END)
      =(CASE
      WHEN v_numRelationShip = 0 THEN 0
      ELSE v_numRelationShip
      END)
      GROUP BY AL.numAOIID,AM.vcAOIName;
   end if;
   RETURN;
END; $$;


