-- Function definition script fn_GetExtAccessDtls for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION fn_GetExtAccessDtls(v_numExtranetID NUMERIC)
RETURNS VARCHAR(10) LANGUAGE plpgsql
   AS $$
   DECLARE
   v_retData  VARCHAR(10);
   v_numDivisionID  NUMERIC;
   v_countContacts  NUMERIC;
   v_countExtContacts  NUMERIC;
BEGIN
   select   numDivisionID INTO v_numDivisionID from ExtarnetAccounts where numExtranetID = v_numExtranetID;

   select   count(*) INTO v_countContacts from AdditionalContactsInformation where numDivisionId = v_numDivisionID and vcEmail <> '';
   select   count(*) INTO v_countExtContacts from ExtranetAccountsDtl where numExtranetID = v_numExtranetID and tintAccessAllowed = 1;

   if v_countExtContacts = 0 then
      v_retData := 'No';
   ELSEIF v_countExtContacts = v_countContacts
   then
      v_retData := 'All Users';
   ELSEIF v_countExtContacts < v_countContacts
   then
      v_retData := 'Some Users';
   end if;


   return v_retData;
END; $$;

