-- Stored procedure definition script USP_GetPageIps for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetPageIps(v_From TIMESTAMP,                  
v_To TIMESTAMP,                  
v_searchTerm VARCHAR(500)          ,  
v_numDomainID NUMERIC(9,0),
v_ClientOffsetTime INTEGER,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for select cast(vcUserHostAddress as VARCHAR(255)),
cast(case when vcUserDomain is null then vcUserHostAddress
   when vcUserDomain = '-' then vcUserHostAddress
   else vcUserDomain end as VARCHAR(255)) as vcUserDomain
   from TrackingVisitorsDTL
   join  TrackingVisitorsHDR hdr
   on numTrackingID = numTracVisitorsHDRID
   where vcpagename
   ilike v_searchTerm and numDomainID = v_numDomainID and hdr.dtcreated between v_From+CAST(v_ClientOffsetTime || 'minute' as interval) and v_To+CAST(v_ClientOffsetTime || 'minute' as interval);
END; $$;












