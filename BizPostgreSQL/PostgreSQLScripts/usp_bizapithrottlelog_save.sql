DROP FUNCTION IF EXISTS USP_BizAPIThrottleLog_Save;

CREATE OR REPLACE FUNCTION USP_BizAPIThrottleLog_Save(v_vcIP VARCHAR(50),
	 v_vcBizAPIAccessKey VARCHAR(50),
	 v_vcEndPoint TEXT,
	 v_dtLogDate TIMESTAMP)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
	
	
   INSERT INTO BizAPIThrottleLog(vcIP,
	 vcBizAPIAccessKey,
	 vcEndPoint,
	 dtLogDate)
	VALUES(v_vcIP,
	 v_vcBizAPIAccessKey,
	 v_vcEndPoint,
	 v_dtLogDate);
RETURN;
END; $$;


