-- Stored procedure definition script USP_ManageFormFieldValidation for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ManageFormFieldValidation(v_numDomainID NUMERIC(9,0),
 v_numFormId NUMERIC(9,0),
 v_numFormFieldId NUMERIC(9,0),
 v_strFieldList TEXT DEFAULT '')
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   if exists(select * from DynamicFormField_Validation where numDomainID = v_numDomainID and numFormId = v_numFormId and numFormFieldId = v_numFormFieldId) then

      UPDATE  DynamicFormField_Validation
      SET     bitIsRequired = X.bitIsRequired,bitIsNumeric = X.bitIsNumeric,bitIsAlphaNumeric = X.bitIsAlphaNumeric,
      bitIsEmail = X.bitIsEmail,bitIsLengthValidation = X.bitIsLengthValidation,
      intMaxLength = X.intMaxLength,intMinLength = X.intMinLength,
      bitFieldMessage = X.bitFieldMessage,vcFieldMessage = X.vcFieldMessage,
      vcToolTip = X.vcToolTip
      FROM
	  XMLTABLE
		(
			'NewDataSet/Table'
			PASSING 
				CAST(v_strFieldList AS XML)
			COLUMNS
				id FOR ORDINALITY,
				bitIsRequired BOOLEAN PATH 'bitIsRequired',
				bitIsNumeric BOOLEAN PATH 'bitIsNumeric',
				bitIsEmail BOOLEAN PATH 'bitIsEmail',
				bitIsAlphaNumeric BOOLEAN PATH 'bitIsAlphaNumeric',
				bitIsLengthValidation BOOLEAN PATH 'bitIsLengthValidation',
				intMaxLength INTEGER PATH 'intMaxLength',
				intMinLength INTEGER PATH 'intMinLength',
				bitFieldMessage BOOLEAN PATH 'bitFieldMessage',
				vcFieldMessage VARCHAR(500) PATH 'vcFieldMessage',
				vcToolTip VARCHAR(1000) PATH 'vcToolTip'
		) AS X
      WHERE   numDomainID = v_numDomainID and numFormId = v_numFormId and numFormFieldId = v_numFormFieldId;
   ELSE
      INSERT INTO DynamicFormField_Validation(numFormId,numFormFieldId,numDomainID,
		bitIsRequired,
		bitIsNumeric,
		bitIsAlphaNumeric,
		bitIsEmail,
		bitIsLengthValidation,
		intMaxLength,
		intMinLength,
		bitFieldMessage,
		vcFieldMessage,vcToolTip)
      SELECT
      v_numFormId,v_numFormFieldId,v_numDomainID,
		X.bitIsRequired,
		X.bitIsNumeric,
		X.bitIsAlphaNumeric,
		X.bitIsEmail,
		X.bitIsLengthValidation,
		X.intMaxLength,
		X.intMinLength,
		X.bitFieldMessage,
		X.vcFieldMessage,X.vcToolTip
      FROM
	   XMLTABLE
		(
			'NewDataSet/Table'
			PASSING 
				CAST(v_strFieldList AS XML)
			COLUMNS
				id FOR ORDINALITY,
				bitIsRequired BOOLEAN PATH 'bitIsRequired',
				bitIsNumeric BOOLEAN PATH 'bitIsNumeric',
				bitIsEmail BOOLEAN PATH 'bitIsEmail',
				bitIsAlphaNumeric BOOLEAN PATH 'bitIsAlphaNumeric',
				bitIsLengthValidation BOOLEAN PATH 'bitIsLengthValidation',
				intMaxLength INTEGER PATH 'intMaxLength',
				intMinLength INTEGER PATH 'intMinLength',
				bitFieldMessage BOOLEAN PATH 'bitFieldMessage',
				vcFieldMessage VARCHAR(500) PATH 'vcFieldMessage',
				vcToolTip VARCHAR(1000) PATH 'vcToolTip'
		) AS X;	  
   end if;
   
   RETURN;
END; $$;



