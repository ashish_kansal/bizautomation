CREATE OR REPLACE FUNCTION USP_WorkOrder_GetRecursiveByID(v_numDomainID NUMERIC(18,0),
	v_numWOID NUMERIC(18,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
	open SWV_RefCur for
	with recursive CTE(numWOId,numLevel) AS
   (SELECT
		numWOId AS numWOID,
		1 AS numLevel
	FROM
		WorkOrder
	WHERE
		numDomainId = v_numDomainID
		AND numWOId = v_numWOID
	UNION ALL
	SELECT
		WorkOrder.numWOId AS numWOID,
		C.numLevel+1 AS numLevel
	FROM
		WorkOrder
	INNER JOIN
		CTE C
	ON
		WorkOrder.numParentWOID = C.numWOID)

	SELECT numWOId AS "numWOId", numLevel AS "numLevel" FROM CTE ORDER BY numLevel DESC;
   
	RETURN;
END; $$;













