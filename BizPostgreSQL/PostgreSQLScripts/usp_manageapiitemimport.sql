-- Stored procedure definition script USP_ManageApiItemImport for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021

--created by Joseph
CREATE OR REPLACE FUNCTION USP_ManageApiItemImport(v_numDomainID NUMERIC(18,0),
	v_numWebApiID NUMERIC(18,0),
	v_UserContactID NUMERIC(18,0),
	v_FlagImport VARCHAR(100))
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   UPDATE WebAPIDetail SET vcFourteenthFldValue = v_FlagImport,vcFifteenthFldValue = v_UserContactID
   WHERE WebApiId = v_numWebApiID AND numDomainID = v_numDomainID;
   RETURN;
END; $$;
        


