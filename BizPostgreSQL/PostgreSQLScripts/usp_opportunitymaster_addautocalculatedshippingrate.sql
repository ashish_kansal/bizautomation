-- Stored procedure definition script USP_OpportunityMaster_AddAutoCalculatedShippingRate for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_OpportunityMaster_AddAutoCalculatedShippingRate(v_numDomainID NUMERIC(18,0)
	,v_numOppID NUMERIC(18,0)
	,v_dtItemReleaseDate DATE
	,v_numShipFromWarehouse NUMERIC(18,0)
	,v_monShipRate DECIMAL(20,5)
	,v_vcDescription VARCHAR(300))
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numShippingServiceItemID  NUMERIC(18,0);
   v_numOppItemID  NUMERIC(18,0);
BEGIN
   select   coalesce(numShippingServiceItemID,0) INTO v_numShippingServiceItemID FROM
   Domain WHERE
   numDomainId = v_numDomainID; 

   IF NOT EXISTS(SELECT numItemCode FROM  Item WHERE numDomainID = v_numDomainID AND numItemCode = v_numShippingServiceItemID) then
	
      RAISE EXCEPTION 'SHIPPING_ITEM_NOT_SET';
      RETURN;
   end if;

   v_numOppItemID := coalesce((SELECT
   OI.numoppitemtCode
   FROM
   OpportunityItems OI
   INNER JOIN
   OpportunityMaster OM
   ON
   OI.numOppId = OM.numOppId
   WHERE
   OI.numOppId = v_numOppID
   AND OI.numItemCode = v_numShippingServiceItemID
   AND coalesce(OI.numShipFromWarehouse,0) = v_numShipFromWarehouse
   AND coalesce(OI.ItemReleaseDate,OM.dtReleaseDate) = v_dtItemReleaseDate),0);

   IF coalesce(v_numOppItemID,0) > 0 then
	
      IF NOT EXISTS(SELECT OBI.numOppBizDocItemID FROM OpportunityBizDocs OB INNER JOIN OpportunityBizDocItems OBI ON OB.numOppBizDocsId = OBI.numOppBizDocID WHERE OB.numoppid = v_numOppID AND OBI.numOppItemID = v_numOppItemID) then
		
         UPDATE
         OpportunityItems OI
         SET
         numUnitHour = 1,monPrice = v_monShipRate,monTotAmount = v_monShipRate,
         monTotAmtBefDiscount = v_monShipRate
			
         WHERE
         numoppitemtCode = v_numOppItemID;
         UPDATE
         OpportunityBizDocItems
         SET
         numUnitHour = 1,monPrice = v_monShipRate,monTotAmount = v_monShipRate,monTotAmtBefDiscount = v_monShipRate
         WHERE
         numOppItemID = v_numOppItemID;
      end if;
   ELSE
      INSERT INTO OpportunityItems(numOppId
			,numItemCode
			,numUnitHour
			,monPrice
			,monTotAmount
			,monTotAmtBefDiscount
			,bitDiscountType
			,fltDiscount
			,vcItemName
			,vcItemDesc
			,vcSKU
			,vcManufacturer
			,vcModelID
			,numShipFromWarehouse
			,ItemReleaseDate)
      SELECT
      v_numOppID
			,v_numShippingServiceItemID
			,1
			,v_monShipRate
			,v_monShipRate
			,v_monShipRate
			,true
			,0
			,vcItemName
			,v_vcDescription
			,vcSKU
			,vcManufacturer
			,vcModelID
			,v_numShipFromWarehouse
			,v_dtItemReleaseDate
      FROM
      Item
      WHERE
      numDomainID = v_numDomainID
      AND numItemCode = v_numShippingServiceItemID;
      v_numOppItemID := CURRVAL('OpportunityItems_seq');

		--Insert Tax for Opportunity Items
      INSERT INTO OpportunityItemsTaxItems(numOppId,
			numOppItemID,
			numTaxItemID,
			numTaxID)
      SELECT
      v_numOppID,
			OI.numoppitemtCode,
			TI.numTaxItemID,
			0
      FROM
      OpportunityItems OI
      JOIN
      ItemTax IT
      ON
      OI.numItemCode = IT.numItemCode
      JOIN
      TaxItems TI
      ON
      TI.numTaxItemID = IT.numTaxItemID
      WHERE
      OI.numOppId = v_numOppID
      AND OI.numoppitemtCode = v_numOppItemID
      AND IT.bitApplicable = true
      UNION
      SELECT
      v_numOppID,
			OI.numoppitemtCode,
			0,
			0
      FROM
      OpportunityItems OI
      JOIN
      Item I
      ON
      OI.numItemCode = I.numItemCode
      WHERE
      OI.numOppId = v_numOppID
      AND OI.numoppitemtCode = v_numOppItemID
      AND I.bitTaxable = true
      UNION
      SELECT
      v_numOppID,
			OI.numoppitemtCode,
			1,
			TD.numTaxID
      FROM
      OpportunityItems OI
      INNER JOIN
      ItemTax IT
      ON
      IT.numItemCode = OI.numItemCode
      INNER JOIN
      TaxDetails TD
      ON
      TD.numTaxID = IT.numTaxID
      AND TD.numDomainId = v_numDomainID
      WHERE
      OI.numOppId = v_numOppID
      AND OI.numoppitemtCode = v_numOppItemID;
   end if;
	
   UPDATE
   OpportunityMaster
   SET
   monDealAmount = GetDealAmount(v_numOppID,LOCALTIMESTAMP,0::NUMERIC)
   WHERE
   numDomainId = v_numDomainID
   AND numOppId = v_numOppID;
END; $$;


