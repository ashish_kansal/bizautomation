-- Stored procedure definition script USP_Subscribers_GetCustomerReport for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_Subscribers_GetCustomerReport(v_bitPayingCustomerOnly BOOLEAN
	,v_SortChar CHAR(1)
	,v_tintSortOrder SMALLINT
	,v_vcSearchWord VARCHAR(100)
	,v_ClientTimeZoneOffset INTEGER
	,v_tintLastLoggedIn SMALLINT
	,v_numPageIndex INTEGER
	,v_numPageSize INTEGER
	,v_vcSortColumn VARCHAR(100)
	,v_vcSortOrder VARCHAR(10), INOUT SWV_RefCur refcursor)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_vcSQL  TEXT;
BEGIN
   DROP TABLE IF EXISTS tt_TEMPCUTOMER CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPCUTOMER
   (
      numDivisionID NUMERIC(18,0),
      vcCompanyName VARCHAR(300),
      vcCompanyNameURL VARCHAR(500),
      dtSubEndDate TIMESTAMP,
      vcSubEndDate VARCHAR(50),
      dtLastLoggedIn TIMESTAMP,
      vcLastLoggedIn VARCHAR(50),
      intSubscribedFullUsers INTEGER,
      intFullUsersUniqueLogin INTEGER,
      intFullUsersAllLogin INTEGER,
      intUsedFullUsers INTEGER,
      monFullUserCost DECIMAL(20,5),
      intSubscribedLimitedAccessUsers INTEGER,
      intUsedLimitedAccessUsers INTEGER,
      intSubscribedBusinessPortalUsers INTEGER,
      intUsedBusinessPortalUsers INTEGER,
      monSiteCost DECIMAL(20,5),
      monTotalBiz DECIMAL(20,5),
      numTotalSalesOrder INTEGER,
      monTotalGrossProfit DECIMAL(20,5),
      numTotalBizPercent INTEGER,
      numTotalSize DOUBLE PRECISION,
      numTotalIncome DECIMAL(20,5)
   );

   INSERT INTO tt_TEMPCUTOMER(numDivisionID
		,vcCompanyName
		,vcCompanyNameURL
		,dtSubEndDate
		,vcSubEndDate
		,dtLastLoggedIn
		,vcLastLoggedIn
		,intSubscribedFullUsers
		,intFullUsersUniqueLogin
		,intFullUsersAllLogin
		,intUsedFullUsers
		,monFullUserCost
		,intSubscribedLimitedAccessUsers
		,intUsedLimitedAccessUsers
		,intSubscribedBusinessPortalUsers
		,intUsedBusinessPortalUsers
		,monSiteCost
		,monTotalBiz
		,numTotalSalesOrder
		,monTotalGrossProfit
		,numTotalBizPercent
		,numTotalSize)
   SELECT
   DivisionMaster.numDivisionID
		,coalesce(vcCompanyName,'-')
		,CONCAT('<a href=',(CASE
   WHEN DivisionMaster.tintCRMType = 2
   THEN CONCAT('../account/frmAccounts.aspx?DivID=',DivisionMaster.numDivisionID,'')
   WHEN tintCRMType = 1
   THEN CONCAT('../prospects/frmProspects.aspx?DivID=',DivisionMaster.numDivisionID,'')
   ELSE CONCAT('../Leads/frmLeads.aspx?DivID=',DivisionMaster.numDivisionID,'')
   END),'>',coalesce(vcCompanyName,'-'),'</a>') AS vcCompanyNameURL
		,dtSubEndDate
		,CAST(FormatedDateFromDate(dtSubEndDate,1::NUMERIC) AS VARCHAR(50)) as dtSubEndDate
		,dtLastLoggedIn
		,FormatedDateTimeFromDate(dtLastLoggedIn+CAST(-v_ClientTimeZoneOffset || 'minute' as interval),1::NUMERIC) AS dtLastLoggedIn
		,coalesce(intNoofUsersSubscribed,0) AS intSubscribedFullUsers
		,coalesce((SELECT
      COUNT(DISTINCT numContactID)
      FROM
      UserAccessedDTL
      INNER JOIN
      UserMaster
      ON
      UserAccessedDTL.numContactID = UserMaster.numUserDetailId
      LEFT JOIN
      AuthenticationGroupMaster
      ON
      UserMaster.numGroupID = AuthenticationGroupMaster.numGroupID
      WHERE
      UserAccessedDTL.numDomainId = UserAccessedDTL.numDomainId
      AND UserAccessedDTL.numDivisionID = DivisionMaster.numDivisionID
      AND (AuthenticationGroupMaster.tintGroupType = 1 OR coalesce(AuthenticationGroupMaster.tintGroupType,0) = 0)
      AND 1 =(CASE
      WHEN v_tintLastLoggedIn = 1 THEN(CASE WHEN dtLoggedInTime BETWEEN TIMEZONE('UTC',now())+INTERVAL '-7 day' AND TIMEZONE('UTC',now()) THEN 1 ELSE 0 END)
      WHEN v_tintLastLoggedIn = 2 THEN(CASE WHEN dtLoggedInTime BETWEEN TIMEZONE('UTC',now())+INTERVAL '-14 day' AND TIMEZONE('UTC',now()) THEN 1 ELSE 0 END)
      WHEN v_tintLastLoggedIn = 3 THEN(CASE WHEN dtLoggedInTime BETWEEN TIMEZONE('UTC',now())+INTERVAL '-30 day' AND TIMEZONE('UTC',now()) THEN 1 ELSE 0 END)
      ELSE 1
      END)),0) AS intFullUsersUniqueLogin
		,coalesce((SELECT
      COUNT(*)
      FROM
      UserAccessedDTL
      INNER JOIN
      UserMaster
      ON
      UserAccessedDTL.numContactID = UserMaster.numUserDetailId
      LEFT JOIN
      AuthenticationGroupMaster
      ON
      UserMaster.numGroupID = AuthenticationGroupMaster.numGroupID
      WHERE
      UserAccessedDTL.numDomainId = UserAccessedDTL.numDomainId
      AND UserAccessedDTL.numDivisionID = DivisionMaster.numDivisionID
      AND (AuthenticationGroupMaster.tintGroupType = 1 OR coalesce(AuthenticationGroupMaster.tintGroupType,0) = 0)
      AND 1 =(CASE
      WHEN v_tintLastLoggedIn = 1 THEN(CASE WHEN dtLoggedInTime BETWEEN TIMEZONE('UTC',now())+INTERVAL '-7 day' AND TIMEZONE('UTC',now()) THEN 1 ELSE 0 END)
      WHEN v_tintLastLoggedIn = 2 THEN(CASE WHEN dtLoggedInTime BETWEEN TIMEZONE('UTC',now())+INTERVAL '-14 day' AND TIMEZONE('UTC',now()) THEN 1 ELSE 0 END)
      WHEN v_tintLastLoggedIn = 3 THEN(CASE WHEN dtLoggedInTime BETWEEN TIMEZONE('UTC',now())+INTERVAL '-30 day' AND TIMEZONE('UTC',now()) THEN 1 ELSE 0 END)
      ELSE 1
      END)),0) AS intFullUsersAllLogin
		,(CASE
   WHEN coalesce(intNoofUsersSubscribed,0) > 0
   THEN(coalesce((SELECT
         COUNT(DISTINCT numContactID)
         FROM
         UserAccessedDTL
         INNER JOIN
         UserMaster
         ON
         UserAccessedDTL.numContactID = UserMaster.numUserDetailId
         LEFT JOIN
         AuthenticationGroupMaster
         ON
         UserMaster.numGroupID = AuthenticationGroupMaster.numGroupID
         WHERE
         UserAccessedDTL.numDomainId = UserAccessedDTL.numDomainId
         AND UserAccessedDTL.numDivisionID = DivisionMaster.numDivisionID
         AND (AuthenticationGroupMaster.tintGroupType = 1 OR coalesce(AuthenticationGroupMaster.tintGroupType,0) = 0)
         AND 1 =(CASE
         WHEN v_tintLastLoggedIn = 1 THEN(CASE WHEN dtLoggedInTime BETWEEN TIMEZONE('UTC',now())+INTERVAL '-7 day' AND TIMEZONE('UTC',now()) THEN 1 ELSE 0 END)
         WHEN v_tintLastLoggedIn = 2 THEN(CASE WHEN dtLoggedInTime BETWEEN TIMEZONE('UTC',now())+INTERVAL '-14 day' AND TIMEZONE('UTC',now()) THEN 1 ELSE 0 END)
         WHEN v_tintLastLoggedIn = 3 THEN(CASE WHEN dtLoggedInTime BETWEEN TIMEZONE('UTC',now())+INTERVAL '-30 day' AND TIMEZONE('UTC',now()) THEN 1 ELSE 0 END)
         ELSE 1
         END)),0)*100)/coalesce(intNoofUsersSubscribed,0)
   ELSE 0
   END) AS intUsedFullUsers
		,coalesce(monFullUserCost,79.95) AS monFullUserCost
		,coalesce(intNoofLimitedAccessUsers,0) AS intSubscribedLimitedAccessUsers
		,(CASE
   WHEN coalesce(intNoofLimitedAccessUsers,0) > 0
   THEN(coalesce((SELECT
         COUNT(DISTINCT numContactID)
         FROM
         UserAccessedDTL
         INNER JOIN
         UserMaster
         ON
         UserAccessedDTL.numContactID = UserMaster.numUserDetailId
         INNER JOIN
         AuthenticationGroupMaster
         ON
         UserMaster.numGroupID = AuthenticationGroupMaster.numGroupID
         WHERE
         UserAccessedDTL.numDomainId = UserAccessedDTL.numDomainId
         AND UserAccessedDTL.numDivisionID = DivisionMaster.numDivisionID
         AND AuthenticationGroupMaster.tintGroupType = 4
         AND 1 =(CASE
         WHEN v_tintLastLoggedIn = 1 THEN(CASE WHEN dtLoggedInTime BETWEEN TIMEZONE('UTC',now())+INTERVAL '-7 day' AND TIMEZONE('UTC',now()) THEN 1 ELSE 0 END)
         WHEN v_tintLastLoggedIn = 2 THEN(CASE WHEN dtLoggedInTime BETWEEN TIMEZONE('UTC',now())+INTERVAL '-14 day' AND TIMEZONE('UTC',now()) THEN 1 ELSE 0 END)
         WHEN v_tintLastLoggedIn = 3 THEN(CASE WHEN dtLoggedInTime BETWEEN TIMEZONE('UTC',now())+INTERVAL '-30 day' AND TIMEZONE('UTC',now()) THEN 1 ELSE 0 END)
         ELSE 1
         END)),0)*100)/coalesce(intNoofLimitedAccessUsers,0)
   ELSE 0
   END) AS intUsedLimitedAccessUsers
		,coalesce(intNoofBusinessPortalUsers,0) AS intSubscribedBusinessPortalUsers
		,(CASE
   WHEN coalesce(intNoofBusinessPortalUsers,0) > 0
   THEN(coalesce((SELECT
         COUNT(DISTINCT ExtranetAccountsDtl.numContactID)
         FROM
         ExtranetAccountsDtl
         INNER JOIN
         ExtarnetAccounts
         ON
         ExtranetAccountsDtl.numExtranetID = ExtarnetAccounts.numExtranetID
         INNER JOIN
         UserAccessedDTL
         ON
         ExtranetAccountsDtl.numDomainID = UserAccessedDTL.numDomainId
         AND ExtarnetAccounts.numDivisionID = UserAccessedDTL.numDivisionID
         AND ExtranetAccountsDtl.numContactID = CAST(UserAccessedDTL.numContactID AS VARCHAR)
         WHERE
         ExtranetAccountsDtl.numDomainID = Subscribers.numTargetDomainID
         AND 1 =(CASE
         WHEN v_tintLastLoggedIn = 1 THEN(CASE WHEN dtLoggedInTime BETWEEN TIMEZONE('UTC',now())+INTERVAL '-7 day' AND TIMEZONE('UTC',now()) THEN 1 ELSE 0 END)
         WHEN v_tintLastLoggedIn = 2 THEN(CASE WHEN dtLoggedInTime BETWEEN TIMEZONE('UTC',now())+INTERVAL '-14 day' AND TIMEZONE('UTC',now()) THEN 1 ELSE 0 END)
         WHEN v_tintLastLoggedIn = 3 THEN(CASE WHEN dtLoggedInTime BETWEEN TIMEZONE('UTC',now())+INTERVAL '-30 day' AND TIMEZONE('UTC',now()) THEN 1 ELSE 0 END)
         ELSE 1
         END)),0)*100)/coalesce(intNoofBusinessPortalUsers,0)
   ELSE 0
   END) AS intUsedBusinessPortalUsers
		,coalesce((SELECT COUNT(*) FROM Sites SI WHERE SI.numDomainID = Subscribers.numTargetDomainID AND coalesce(SI.bitIsActive,false) = true AND LENGTH(coalesce(SI.vcLiveURL,'')) > 0),0)*coalesce(monSiteCost,0) AS monSiteCost
		,((coalesce(intNoofUsersSubscribed,0)*coalesce(monFullUserCost,79.95))+(coalesce(intNoofLimitedAccessUsers,0)*coalesce(monLimitedAccessUserCost,24.95))+(coalesce(intNoofBusinessPortalUsers,0)*coalesce(monBusinessPortalUserCost,9.95))+(coalesce((SELECT COUNT(*) FROM Sites SI WHERE SI.numDomainID = Subscribers.numTargetDomainID AND coalesce(SI.bitIsActive,false) = true AND LENGTH(coalesce(SI.vcLiveURL,'')) > 0),0)*coalesce(monSiteCost,0))) AS monTotalBiz
		,coalesce((SELECT COUNT(*) FROM OpportunityMaster OM WHERE OM.numDomainId = Subscribers.numTargetDomainID AND OM.tintopptype = 1 AND OM.tintoppstatus = 1 AND OM.bintCreatedDate BETWEEN TIMEZONE('UTC',now())+INTERVAL '-30 day' AND TIMEZONE('UTC',now())),0) AS numTotalSalesOrder
		,coalesce((SELECT SUM(GetOrderProfitAmountOrMargin(OM.numDomainId,OM.numOppId,1::SMALLINT)) FROM OpportunityMaster OM WHERE OM.numDomainId = Subscribers.numTargetDomainID AND OM.tintopptype = 1 AND OM.tintoppstatus = 1 AND OM.bintCreatedDate BETWEEN TIMEZONE('UTC',now())+INTERVAL '-30 day' AND TIMEZONE('UTC',now())),0) AS monTotalGrossProfit
		,0 AS numTotalBizPercent
		,coalesce((SELECT SUM(coalesce(SpaceOccupied,0)) FROM View_InboxCount VI WHERE VI.numDomainID = Subscribers.numTargetDomainID),0) AS numTotalSize
   FROM
   Subscribers
   INNER JOIN
   Domain
   ON
   Subscribers.numTargetDomainID = Domain.numDomainId
   INNER JOIN
   DivisionMaster
   ON
   Domain.numDivisionId = DivisionMaster.numDivisionID
   INNER JOIN
   CompanyInfo
   ON
   DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
   WHERE
		(v_SortChar = '0' OR vcCompanyName ilike CONCAT(v_SortChar,'%'))
   AND 1 =(CASE
   WHEN LENGTH(coalesce(v_vcSearchWord,'')) > 0 AND v_tintSortOrder = 1 THEN(CASE WHEN vcCompanyName ilike CONCAT('%',v_vcSearchWord,'%') THEN 1 ELSE 0 END)
   WHEN LENGTH(coalesce(v_vcSearchWord,'')) > 0 AND v_tintSortOrder = 2 THEN(CASE WHEN DivisionMaster.numDivisionID IN(SELECT numDivisionId FROM AdditionalContactsInformation A WHERE vcEmail ilike CONCAT('%',v_vcSearchWord,'%')) THEN 1 ELSE 0 END)
   WHEN v_tintSortOrder = 3 THEN(CASE WHEN Subscribers.bitTrial = true THEN 1 ELSE 0 END)
   WHEN v_tintSortOrder = 4 THEN(CASE WHEN Subscribers.bitActive = 1 THEN 1 ELSE 0 END)
   WHEN v_tintSortOrder = 5 THEN(CASE WHEN Subscribers.bitActive = 0 THEN 1 ELSE 0 END)
   WHEN v_tintSortOrder = 6 THEN(CASE WHEN Subscribers.bitActive = 0 AND Subscribers.dtSuspendedDate <= TIMEZONE('UTC',now())+INTERVAL '30 day' THEN 1 ELSE 0 END)
   WHEN v_tintSortOrder = 7 THEN(CASE WHEN Subscribers.bitActive = 0 AND Subscribers.dtSuspendedDate >= TIMEZONE('UTC',now())+INTERVAL '30 day' THEN 1 ELSE 0 END)
   ELSE 1
   END)
   AND 1 =(CASE
   WHEN v_bitPayingCustomerOnly = true
   THEN(CASE WHEN coalesce(bitTrial,false) = false THEN 1 ELSE 0 END)
   ELSE 1
   END)
   AND 1 =(CASE
   WHEN v_tintLastLoggedIn = 1 THEN(CASE WHEN coalesce((SELECT COUNT(*) FROM UserAccessedDTL UAD WHERE UAD.numDivisionID = DivisionMaster.numDivisionID AND UAD.dtLoggedInTime BETWEEN TIMEZONE('UTC',now())+INTERVAL '-7 day' AND TIMEZONE('UTC',now())),0) > 0 THEN 1 ELSE 0 END)
   WHEN v_tintLastLoggedIn = 2 THEN(CASE WHEN coalesce((SELECT COUNT(*) FROM UserAccessedDTL UAD WHERE UAD.numDivisionID = DivisionMaster.numDivisionID AND UAD.dtLoggedInTime BETWEEN TIMEZONE('UTC',now())+INTERVAL '-14 day' AND TIMEZONE('UTC',now())),0) > 0 THEN 1 ELSE 0 END)
   WHEN v_tintLastLoggedIn = 3 THEN(CASE WHEN coalesce((SELECT COUNT(*) FROM UserAccessedDTL UAD WHERE UAD.numDivisionID = DivisionMaster.numDivisionID AND UAD.dtLoggedInTime BETWEEN TIMEZONE('UTC',now())+INTERVAL '-30 day' AND TIMEZONE('UTC',now())),0) > 0 THEN 1 ELSE 0 END)
   ELSE 1
   END);

   UPDATE tt_TEMPCUTOMER SET numTotalBizPercent =(CASE WHEN coalesce(monTotalGrossProfit,0) > 0 THEN((monTotalBiz*100)/monTotalGrossProfit) ELSE 0 END);
   UPDATE tt_TEMPCUTOMER SET numTotalIncome = coalesce((SELECT SUM(monTotalBiz) FROM tt_TEMPCUTOMER),0);

   v_vcSQL := 'SELECT COUNT(*) OVER() AS numTotalRecords,* FROM tt_TEMPCUTOMER';
	
   IF LENGTH(coalesce(v_vcSortColumn,'')) > 0 then
	
      v_vcSQL := CONCAT(v_vcSQL,' ORDER BY ',v_vcSortColumn,' ',v_vcSortOrder);
   ELSE
      v_vcSQL := CONCAT(v_vcSQL,' ORDER BY monTotalBiz DESC');
   end if;

   v_vcSQL := CONCAT(v_vcSQL,' OFFSET ',(v_numPageIndex::bigint -1)*v_numPageSize::bigint,
   ' ROWS FETCH NEXT ',v_numPageSize,' ROWS ONLY');
		
   OPEN SWV_RefCur FOR EXECUTE v_vcSQL;

   RETURN;
END; $$;


