-- Stored procedure definition script USP_PopulateCustomFlds for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_PopulateCustomFlds(v_numRelation NUMERIC(9,0) DEFAULT 0,  
v_numContactType NUMERIC(9,0) DEFAULT 0, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
BEGIN
   if v_numRelation <> 0 and v_numContactType = 0 then

      open SWV_RefCur for
      select 1 as Type,fld_id,fld_label from CFW_Fld_Master join CFW_Fld_Dtl
      on Fld_id = numFieldId
      where (Fld_type = 'TextBox' or Fld_type = 'TextArea') and Grp_id = 1 and numRelation = v_numRelation;
   end if;  
   if v_numRelation = 0 and v_numContactType <> 0 then

      open SWV_RefCur for
      select 2 as Type ,fld_id,fld_label from CFW_Fld_Master join CFW_Fld_Dtl
      on Fld_id = numFieldId
      where (Fld_type = 'TextBox' or Fld_type = 'TextArea') and Grp_id = 4 and numRelation = v_numContactType;
   end if;  
   if v_numRelation <> 0 and v_numContactType <> 0 then

      open SWV_RefCur for
      select 1 as Type,fld_id,fld_label from CFW_Fld_Master join CFW_Fld_Dtl
      on Fld_id = numFieldId
      where (Fld_type = 'TextBox' or Fld_type = 'TextArea') and Grp_id = 1 and numRelation = v_numRelation
      union
      select 2 as Type,fld_id,fld_label from CFW_Fld_Master join CFW_Fld_Dtl
      on Fld_id = numFieldId
      where (Fld_type = 'TextBox' or Fld_type = 'TextArea') and Grp_id = 4 and numRelation = v_numContactType;
   end if;

   if v_numRelation = 0 and v_numContactType = 0 then

      open SWV_RefCur for
      select 2 as Type ,fld_id,fld_label from CFW_Fld_Master join CFW_Fld_Dtl
      on Fld_id = numFieldId
      where (Fld_type = 'TextBox' or Fld_type = 'TextArea') and Grp_id = 4 and numRelation = v_numContactType;
   end if;
   RETURN;
END; $$;


