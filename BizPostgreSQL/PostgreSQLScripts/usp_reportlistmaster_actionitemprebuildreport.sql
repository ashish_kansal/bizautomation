-- Stored procedure definition script USP_ReportListMaster_ActionItemPreBuildReport for PostgreSQL
CREATE OR REPLACE FUNCTION USP_ReportListMaster_ActionItemPreBuildReport(v_numDomainID NUMERIC(18,0)
	,v_numUserCntID NUMERIC(18,0)
	,v_ClientTimeZoneOffset INTEGER
	,v_vcTimeLine VARCHAR(50)
	,v_vcTeritorry TEXT
	,v_vcFilterBy SMALLINT  -- 1:Assign TO, 2: Record Owner, 3: Teams (Based on Assigned To) 
	,v_vcFilterValue TEXT
	,v_bitTask TEXT,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   DROP TABLE IF EXISTS tt_TEMP CASCADE;
   CREATE TEMPORARY TABLE tt_TEMP
   (
      ID NUMERIC(18,0),
      vcOrganizationName VARCHAR(300),
      vcComments TEXT,
      vcType VARCHAR(50),
      dtDate TIMESTAMP,
      tintType SMALLINT
   );
   INSERT INTO tt_TEMP(ID
		,vcOrganizationName
		,vcComments
		,vcType
		,dtDate
		,tintType)
   SELECT
   Communication.numCommId
		,CompanyInfo.vcCompanyName
		,coalesce(Communication.textDetails,'')
		,coalesce(LD.vcData,'')
		,dtStartTime+CAST(-v_ClientTimeZoneOffset || 'minute' as interval)
		,1
   FROM
   Communication
   INNER JOIN
   AdditionalContactsInformation
   ON
   Communication.numContactId = AdditionalContactsInformation.numContactId
   INNER JOIN
   DivisionMaster
   ON
   AdditionalContactsInformation.numDivisionId = DivisionMaster.numDivisionID
   INNER JOIN
   CompanyInfo
   ON
   DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
   LEFT JOIN
   Listdetails LD
   ON
   LD.numListID = 73
   AND (LD.numDomainid = v_numDomainID OR LD.constFlag = true)
   AND LD.numListItemID = Communication.bitTask
   WHERE
   Communication.numDomainID = v_numDomainID
   AND AdditionalContactsInformation.numDomainID = v_numDomainID
   AND DivisionMaster.numDomainID = v_numDomainID
   AND CompanyInfo.numDomainID = v_numDomainID
   AND Communication.bitClosedFlag = false
   AND Communication.bitTask <> 973
   AND CAST(dtEndTime+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) AS DATE) = CAST(TIMEZONE('UTC',now())+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) AS DATE)
   AND 1 =(CASE WHEN coalesce(v_numUserCntID,0) > 0 THEN(CASE WHEN (Communication.numAssign  = v_numUserCntID  OR Communication.numCreatedBy = v_numUserCntID) THEN 1 ELSE 0 END) ELSE 1 END);


   INSERT INTO tt_TEMP(ID
		,vcOrganizationName
		,vcComments
		,vcType
		,dtDate
		,tintType)
   SELECT
   Activity.ActivityID
		,Comp.vcCompanyName
		,coalesce(Activity.ActivityDescription,'')
		,CAST('Calendar' AS VARCHAR(50))
		,StartDateTimeUtc+CAST(-v_ClientTimeZoneOffset || 'minute' as interval)
		,2
   FROM
   Activity
   INNER JOIN
   ActivityResource
   ON
   Activity.ActivityID = ActivityResource.ActivityID
   INNER JOIN
   Resource Res
   ON
   ActivityResource.ResourceID = Res.ResourceID
   INNER JOIN
   AdditionalContactsInformation ACI
   ON
   ACI.numContactId = Res.numUserCntId
   INNER JOIN
   DivisionMaster Div
   ON
   ACI.numDivisionId = Div.numDivisionID
   INNER JOIN
   OpportunityMaster
   ON
   OpportunityMaster.numDivisionId = Div.numDivisionID
   INNER JOIN
   Communication
   ON
   Communication.numDivisionID = Div.numDivisionID
   INNER JOIN
   CompanyInfo Comp
   ON
   Div.numCompanyID = Comp.numCompanyId
   WHERE
   OpportunityMaster.numDomainId = v_numDomainID
   AND Res.numDomainId = v_numDomainID
   AND 1 =(CASE WHEN coalesce(v_numUserCntID,0) > 0 THEN(CASE WHEN Res.numUserCntId = v_numUserCntID THEN 1 ELSE 0 END) ELSE 1 END)
   AND ACI.numDomainID = v_numDomainID
   AND Div.numDomainID = v_numDomainID
   AND Comp.numDomainID = v_numDomainID
   AND CAST(StartDateTimeUtc+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) AS DATE) = CAST(TIMEZONE('UTC',now())+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) AS DATE)
		--AND DATEADD(MINUTE,-@ClientTimeZoneOffset,startdatetimeutc) >= DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE()) AND DATEADD(MINUTE,-@ClientTimeZoneOffset,startdatetimeutc) < CAST(CONVERT(CHAR(8), GETUTCDATE(), 112) + ' 00:00:00.00' AS DATETIME)
   AND Status <> -1
   AND Activity.ActivityID NOT IN(SELECT DISTINCT(numActivityId) FROM Communication WHERE numDomainID = v_numDomainID AND 1 =(CASE WHEN coalesce(v_numUserCntID,0) > 0 THEN(CASE WHEN (numAssign = v_numUserCntID OR numCreatedBy = v_numUserCntID) THEN 1 ELSE 0 END) ELSE 1 END))
   AND 1 =(CASE WHEN LENGTH(coalesce(v_vcTeritorry,'')) > 0 THEN(CASE WHEN Div.numTerID IN(SELECT ID FROM SplitIDs(v_vcFilterValue,',')) THEN 1 ELSE 0 END) ELSE 1 END)
   AND 1 =(CASE
   WHEN LENGTH(coalesce(v_vcFilterValue,'')) > 0 AND v_vcFilterBy IN(1,2,3)
   THEN(CASE v_vcFilterBy
      WHEN 1  -- Assign To
      THEN(CASE WHEN OpportunityMaster.numassignedto IN(SELECT ID FROM SplitIDs(v_vcFilterValue,',')) THEN 1 ELSE 0 END)
      WHEN 2  -- Record Owner
      THEN(CASE WHEN OpportunityMaster.numrecowner IN(SELECT ID FROM SplitIDs(v_vcFilterValue,',')) THEN 1 ELSE 0 END)
      WHEN 3  -- Teams (Based on Assigned To)
      THEN(CASE WHEN OpportunityMaster.numassignedto IN(SELECT numUserCntID FROM UserTeams WHERE numDomainID = v_numDomainID AND numTeam IN(SELECT ID FROM SplitIDs(v_vcFilterValue,','))) THEN 1 ELSE 0 END)
      END)
   ELSE 1
   END)
   AND 1 =(CASE WHEN LENGTH(coalesce(v_bitTask,'')) > 0 THEN(CASE WHEN Communication.bitTask IN(SELECT ID FROM SplitIDs(v_bitTask,',')) THEN 1 ELSE 0 END) ELSE 1 END);
		

	--INSERT INTO @TEMP
	--(
	--	ID
	--	,vcOrganizationName
	--	,vcComments
	--	,vcType
	--	,dtDate
	--	,tintType
	--)
	--SELECT
	--	(CASE BAD.btDocType WHEN 2 THEN BAD.numOppBizDocsId WHEN 3 THEN BAD.numOppBizDocsId ELSE BDA.numBizActionID END)
	--	,CI.vcCompanyName
	--	,ISNULL(BAD.vcComment,'')
	--	,(CASE BAD.btDocType WHEN 2  THEN  'Document Approval Request' ELSE 'BizDoc Approval Request' END)
	--	,DATEADD(MINUTE,-@ClientTimeZoneOffset,dtCreatedDate)
	--	,2
	--FROM
	--	BizDocAction BDA 
	--INNER JOIN
	--	AdditionalContactsInformation ACI
	--ON
	--	BDA.numContactId = ACI.numContactId
	--INNER JOIN
	--	DivisionMaster DM
	--ON
	--	ACI.numDivisionId = DM.numDivisionID
	--INNER JOIN
	--	CompanyInfo CI
	--ON
	--	DM.numCompanyID = CI.numCompanyId
	--LEFT JOIN 
	--	dbo.BizActionDetails BAD
	--ON 
	--	BDA.numBizActionId = BAD.numBizActionId
	--WHERE
	--	BDA.numDomainId =@numDomainID
	--	AND 1 = (CASE WHEN ISNULL(@numUserCntID,0) > 0 THEN (CASE WHEN BDA.numContactId = @numUserCntID THEN 1 ELSE 0 END) ELSE 1 END)
	--	AND ACI.numDomainID=@numDomainID
	--	AND DM.numDomainID=@numDomainID
	--	AND CI.numDomainID=@numDomainID
	--	AND BDA.numStatus=0 
	--	AND CAST(DATEADD(MINUTE,-@ClientTimeZoneOffset,dtCreatedDate) AS DATE) = CAST(DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE()) AS DATE)
	
	
open SWV_RefCur for SELECT *,FormatedDateTimeFromDate(dtDate,v_numDomainID) AS dtFormatted FROM tt_TEMP ORDER BY dtDate ASC;
END; $$;












