-- Stored procedure definition script USP_GroupName for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GroupName(v_numGroupID NUMERIC(9,0) DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for select vcGrpName as "vcGrpName" from Groups where numGrpId = v_numGroupID;
END; $$;

