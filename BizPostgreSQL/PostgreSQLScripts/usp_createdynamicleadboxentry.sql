CREATE OR REPLACE FUNCTION usp_CreateDynamicLeadBoxEntry(v_numDomainId NUMERIC(9,0),                                   
 v_numUserId NUMERIC(9,0),                                         
 v_strXML TEXT,INOUT SWV_RefCur refcursor)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numLeadBoxId  NUMERIC;
   v_iDoc  INTEGER;
   SWV_Label_value varchar(30);
   SWV_error INTEGER;
BEGIN
   SWV_error := 0;
   SWV_Label_value := 'SWL_Start_Label';
   << SWL_Label >>
   LOOP
      CASE
      WHEN SWV_Label_value = 'SWL_Start_Label' THEN
         begin
            Insert into DynamicLeadBoxFormDataMaster(numDomainId, numUserId, numCreatedDate)
 values(v_numDomainId, v_numUserId, TIMEZONE('UTC',now()));
            EXCEPTION
            WHEN OTHERS
            THEN
               SWV_error := -1;
         end;
         v_numLeadBoxId := CURRVAL('DynamicLeadBoxFormDataMaster_seq');
         If SWV_error <> 0 then 
            SWV_Label_value := 'ERR_HANDLER';
            CONTINUE SWL_Label;
         end if;
         SWV_error := 0;
         SELECT * INTO v_iDoc FROM SWF_Xml_PrepareDocument(v_strXML);
         begin
            INSERT INTO DynamicLeadBoxFormDataDetails(numLeadBoxId,
  vcDbColumnName,
  vcFieldName,
  vcDbColumnValue,
  vcDbColumnValueText,
  numRowNum,
  numColumnNum,
  vcAssociatedControlType,
  boolAOIField)
            SELECT v_numLeadBoxId, * FROM SWF_OpenXml(v_iDoc,'/FormFields/FormField','vcDbColumnName |vcFieldName |vcDbColumnValue |vcDbColumnValueText |numRowNum |numColumnNum |vcAssociatedControlType |boolAOIField') SWA_OpenXml(vcDbColumnName VARCHAR(50),vcFieldName VARCHAR(50),vcDbColumnValue NUMERIC,
            vcDbColumnValueText VARCHAR(50),numRowNum NUMERIC,numColumnNum NUMERIC,
            vcAssociatedControlType VARCHAR(50),boolAOIField BOOLEAN);
            EXCEPTION
            WHEN OTHERS
            THEN
               SWV_error := -1;
         end;
         If SWV_error <> 0 then 
            SWV_Label_value := 'ERR_HANDLER';
            CONTINUE SWL_Label;
         end if;
         SWV_error := 0;
         PERFORM SWF_Xml_RemoveDocument(v_iDoc);
         open SWV_RefCur for
         SELECT v_numLeadBoxId;
         SWV_Label_value := 'ERR_HANDLER';
      WHEN SWV_Label_value = 'ERR_HANDLER' THEN
         RETURN;
         EXIT SWL_Label;
      END CASE;
   END LOOP;
END; $$;


