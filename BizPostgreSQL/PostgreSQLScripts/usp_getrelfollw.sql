-- Stored procedure definition script USP_GetRelFollw for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetRelFollw(v_numRelationship NUMERIC(9,0) DEFAULT 0,
v_numDomainID NUMERIC(9,0) DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
	open SWV_RefCur for 
	select 
		numFollowID
		,L2.vcData as Follow
		,numRelFolID
		,numRelationshipID
		,L1.vcData as RelName 
	from 
		RelFollowupStatus
	join 
		Listdetails L1
	on 
		numRelationshipID = L1.numListItemID
	join 
		Listdetails L2
	on 
		numFollowID = L2.numListItemID
	where 
		numRelationshipID = v_numRelationship 
		and L2.numDomainid = v_numDomainID;
END; $$;












