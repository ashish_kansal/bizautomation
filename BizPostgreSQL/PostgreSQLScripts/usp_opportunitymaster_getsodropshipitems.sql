DROP FUNCTION IF EXISTS USP_OpportunityMaster_GetSODropshipItems;

CREATE OR REPLACE FUNCTION USP_OpportunityMaster_GetSODropshipItems
(
	v_numDomainID NUMERIC(18,0)
	,v_numOppID NUMERIC(18,0)
	,INOUT v_SWV_RefCur refcursor
	,INOUT v_SWV_RefCur2 refcursor
	,INOUT v_SWV_RefCur3 refcursor
)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numDivisionID  NUMERIC(18,0);
   v_tintDefaultCost  NUMERIC(18,0);
BEGIN
   IF NOT EXISTS(SELECT * FROM OpportunityMaster WHERE numDomainId = v_numDomainID AND numOppId = v_numOppID) then	
      RAISE NOTICE 'INVALID_OPPID';
      RETURN;
   END IF; 

   select   coalesce(numCost,0) INTO v_tintDefaultCost FROM Domain WHERE numDomainId = v_numDomainID;
   select   numDivisionId INTO v_numDivisionID FROM OpportunityMaster WHERE numDomainId = v_numDomainID AND numOppId = v_numOppID;

   open v_SWV_RefCur for
   SELECT
   OI.numItemCode
		,CONCAT(I.vcItemName,CASE WHEN fn_GetAttributes(OI.numWarehouseItmsID,0::BOOLEAN) <> '' THEN CONCAT(' (',fn_GetAttributes(OI.numWarehouseItmsID,0::BOOLEAN),')') ELSE '' END) AS vcItemName
		,I.vcModelID
		,(CASE WHEN coalesce(I.numItemGroup,0) > 0 AND bitMatrix = false THEN coalesce(WI.vcWHSKU,coalesce(I.vcSKU,''))  ELSE coalesce(I.vcSKU,'') END) AS vcSKU
		,OI.vcNotes
		,I.charItemType
		,coalesce((SELECT  vcPathForTImage FROM ItemImages WHERE numDomainId = v_numDomainID AND numItemCode = I.numItemCode AND bitDefault = true LIMIT 1),
   '') AS vcPathForTImage
		,coalesce(OI.numWarehouseItmsID,0) AS numWarehouseItemID
		,OI.numUnitHour
		,coalesce(OI.numUOMId,0) AS numUOMID
		,coalesce(U.vcUnitName,'-') AS vcUOMName
		,fn_UOMConversion(OI.numUOMId,OI.numItemCode,OM.numDomainId,I.numBaseUnit) AS fltUOMConversionFactor
		,coalesce(I.numVendorID,0) AS numVendorID
		,CASE WHEN v_tintDefaultCost = 3 THEN(coalesce(V.monCost,0)/fn_UOMConversion(I.numPurchaseUnit,I.numItemCode,I.numDomainID,I.numBaseUnit)) ELSE coalesce(I.monAverageCost,0) END AS monCost
		,coalesce(fn_UOMConversion(I.numPurchaseUnit,I.numItemCode,I.numDomainID,I.numBaseUnit),1) AS fltUOMConversionFactorForPurchase
		,coalesce(V.intMinQty,0) AS intMinQty
		,coalesce(v_tintDefaultCost,0) AS tintDefaultCost
   FROM
   OpportunityItems OI
   INNER JOIN OpportunityMaster OM ON OM.numOppId = OI.numOppId
   INNER JOIN Item I ON I.numItemCode = OI.numItemCode
   LEFT JOIN WareHouseItems WI ON OI.numWarehouseItmsID = WI.numWareHouseItemID
   LEFT JOIN UOM U ON U.numUOMId = OI.numUOMId
   LEFT JOIN Vendor V ON V.numVendorID = I.numVendorID	AND I.numItemCode = V.numItemCode
   WHERE
   OI.numOppId = v_numOppID
   AND OM.numDomainId = v_numDomainID
   AND OM.tintopptype = 1
   AND OM.tintoppstatus = 1
   AND coalesce(OI.bitDropShip,false) = true
   AND 1 =(CASE WHEN(SELECT COUNT(*) FROM OpportunityLinking OMInner INNER JOIN OpportunityItems OIInner ON OMInner.numChildOppID = OIInner.numOppId WHERE OMInner.numParentOppID = v_numOppID AND OIInner.numItemCode = OI.numItemCode AND coalesce(OIInner.numWarehouseItmsID,0) = coalesce(OI.numWarehouseItmsID,0)) > 0 THEN 0 ELSE 1 END);


   PERFORM USP_GetOppAddress(v_numOppID,1::SMALLINT,0,'v_SWV_RefCur2');
   OPEN v_SWV_RefCur2 FOR FETCH ALL IN "v_SWV_RefCur2";

   open v_SWV_RefCur3 for
   SELECT fn_getOPPAddress(v_numOppID,v_numDomainID,2::SMALLINT) AS ShippingAdderss;
END; $$;
/****** Object:  StoredProcedure [dbo].[USP_Projects]    Script Date: 07/26/2008 16:20:39 ******/



