-- Stored procedure definition script USP_GetGLTransactions for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_GetGLTransactions(v_numDomainID NUMERIC,
      v_dtFromDate TIMESTAMP,
      v_dtToDate TIMESTAMP,
      v_ClientTimeZoneOffset INTEGER,  
      v_numItemID NUMERIC(9,0) DEFAULT 0,
      v_CurrentPage INTEGER DEFAULT 0,
	  v_PageSize INTEGER DEFAULT 0,
      INOUT v_TotRecs INTEGER DEFAULT 0  ,
      v_numDivisionID NUMERIC(9,0) DEFAULT 0, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_firstRec  INTEGER;
   v_lastRec  INTEGER;
BEGIN
   v_dtFromDate := v_dtFromDate+CAST(v_ClientTimeZoneOffset || 'minute' as interval);
   v_dtToDate := v_dtToDate+CAST(v_ClientTimeZoneOffset || 'minute' as interval);

   DROP TABLE IF EXISTS tt_TEMPTABLE CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPTABLE ON COMMIT DROP AS
      SELECT  numDomainId,
                numAccountId,
                TransactionType,
                CompanyName,
                FormatedDateFromDate(datEntry_Date,numDomainId) AS Date,
                varDescription,
                coalesce(BizPayment,'') || ' ' || coalesce(CheqNo,'') AS Narration,
                BizDocID,
                TranRef,
                TranDesc,
                CAST(coalesce(numDebitAmt,0) AS VARCHAR(20)) AS numDebitAmt,
                CAST(coalesce(numCreditAmt,0) AS VARCHAR(20)) AS numCreditAmt,
                vcAccountName,
                '' AS balance,
                numJournal_Id AS JournalId,
				numCheckHeaderID AS CheckId,
				numCashCreditCardId AS CashCreditCardId,
				numTransactionId,
				numOppId,
				numOppBizDocsId,
				numDepositId,
				numCategoryHDRID,
				tintTEType,
				numCategory,
				dtFromDate,
				numUserCntID,bitReconcile,bitCleared,numBillID,numBillPaymentID,numReturnID,
				Row_number() over(order by datEntry_Date DESC) AS RowNumber 
      FROM    VIEW_GENERALLEDGER
      WHERE   datEntry_Date BETWEEN  v_dtFromDate AND v_dtToDate
      AND numDomainId = v_numDomainID
      AND (numItemID = v_numItemID OR v_numItemID = 0)
      AND (numDivisionID = v_numDivisionID OR v_numDivisionID = 0)
      ORDER BY datEntry_Date DESC; 
         
   v_firstRec :=(v_CurrentPage::bigint -1)*v_PageSize::bigint;
   v_lastRec :=(v_CurrentPage::bigint*v_PageSize::bigint+1);
   SELECT COUNT(*) INTO v_TotRecs FROM   tt_TEMPTABLE;
         
   open SWV_RefCur for
   SELECT * FROM tt_TEMPTABLE WHERE ROWNUMBER > v_firstRec and ROWNUMBER < v_lastRec order by ROWNUMBER;
   RETURN;
END; $$;



