CREATE OR REPLACE FUNCTION USP_GetProjectCommission(v_numUserCntID         NUMERIC(9,0)  DEFAULT 0,
               v_numDomainId          NUMERIC(9,0)  DEFAULT 0,
               v_ClientTimeZoneOffset INTEGER DEFAULT NULL,
               v_dtStartDate          TIMESTAMP DEFAULT NULL,
               v_dtEndDate            TIMESTAMP DEFAULT NULL,
               v_numProID NUMERIC DEFAULT NULL,
               v_tintMode SMALLINT DEFAULT NULL, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
BEGIN
   IF v_tintMode = 1 then
   
      open SWV_RefCur for
      SELECT PM.numProId,PM.vcProjectID,PM.vcProjectName,
   FormatedDateFromDate(BC.dtProCompletionDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval),PM.numdomainId) AS dtCompletionDate,
   PM.monTotalIncome,PM.monTotalExpense,PM.monTotalGrossProfit,
   BC.monProTotalIncome,BC.monProTotalExpense,BC.monProTotalGrossProfit,
		Case When (PM.numRecOwner = v_numUserCntID and PM.numAssignedTo = v_numUserCntID) then  'Project Owner/Assigned'
      When PM.numRecOwner = v_numUserCntID then 'Project Owner'
      When PM.numAssignedTo = v_numUserCntID then 'Project Assigned' end as EmpRole,
									BC.numComissionAmount as decTotalCommission,BC.decCommission
      FROM ProjectsMaster PM
      INNER JOIN BizDocComission BC on BC.numProId = PM.numProId
      where PM.numdomainId = v_numDomainId-- AND PM.numProjectStatus=27492
      And (PM.numRecOwner = v_numUserCntID or PM.numAssignedTo = v_numUserCntID)
      AND (PM.dtCompletionDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) between v_dtStartDate And v_dtEndDate) AND
      BC.numUserCntID = v_numUserCntID AND BC.numDomainId = v_numDomainId AND BC.bitCommisionPaid = false and coalesce(BC.numProId,0) > 0
      ORDER BY PM.dtCompletionDate;
   ELSEIF v_tintMode = 2
   then
   
      open SWV_RefCur for
      SELECT PM.numProId,PM.vcProjectID,PM.vcProjectName,
   FormatedDateFromDate(BC.dtProCompletionDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval),PM.numdomainId) AS dtCompletionDate,
   PM.monTotalIncome,PM.monTotalExpense,PM.monTotalGrossProfit,
   BC.monProTotalIncome,BC.monProTotalExpense,BC.monProTotalGrossProfit,
		Case When (PM.numRecOwner = BC.numUserCntID and PM.numAssignedTo = BC.numUserCntID) then  'Project Owner/Assigned'
      When PM.numRecOwner = BC.numUserCntID then 'Project Owner'
      When PM.numAssignedTo = BC.numUserCntID then 'Project Assigned' end as EmpRole,
									BC.numComissionAmount as decTotalCommission,BC.decCommission,
									coalesce((SELECT SUM(coalesce(monCommissionAmt,0))
         FROM PayrollTracking WHERE coalesce(BC.numComissionID,0) = coalesce(numComissionID,0)),
      0) AS monPaidAmount,fn_GetContactName(coalesce(BC.numUserCntID,0)) AS vcUserName
      FROM ProjectsMaster PM
      INNER JOIN BizDocComission BC on BC.numProId = PM.numProId
      WHERE PM.numProId = v_numProID AND PM.numdomainId = v_numDomainId -- AND PM.numProjectStatus=27492
      ORDER BY PM.dtCompletionDate;
   end if;
   RETURN;
END; $$;



