-- Stored procedure definition script ActivityDelByItemId for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION ActivityDelByItemId(v_itemid VARCHAR(500))
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   delete FROM tmpCalandarTbl where itemId = v_itemid;


   delete FROM ActivityResource where ActivityID in(select ActivityID from  Activity
   where itemId = v_itemid);

   delete FROM Recurrence where recurrenceid in(select RecurrenceID from  Activity
   where itemId = v_itemid and RecurrenceID <> -999);

   delete FROM Activity
   where itemId = v_itemid;
   RETURN;
END; $$;


