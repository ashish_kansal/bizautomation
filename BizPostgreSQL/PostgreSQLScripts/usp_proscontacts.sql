CREATE OR REPLACE FUNCTION USP_ProsContacts(v_numDivisionID NUMERIC(9,0) DEFAULT 0,  
v_numDomainID NUMERIC(9,0) DEFAULT 0,
v_numUserCntID NUMERIC(9,0) DEFAULT 0,
v_ClientTimeZoneOffset INTEGER DEFAULT NULL,
v_numUserGroupID NUMERIC(9,0) DEFAULT 0,
INOUT SWV_RefCur refcursor default null,
INOUT SWV_RefCur2 refcursor default null)
LANGUAGE plpgsql            
   AS $$
   DECLARE
   v_numRelCntType  NUMERIC(9,0);

   v_strSql  VARCHAR(8000); 

   v_tintOrder  SMALLINT;                                                        
   v_vcFieldName  VARCHAR(50);                                                        
   v_vcListItemType  VARCHAR(3);                                                   
   v_vcListItemType1  VARCHAR(1);                                                       
   v_vcAssociatedControlType  VARCHAR(20);                                                        
   v_numListID  NUMERIC(9,0);                                                        
   v_vcDbColumnName  VARCHAR(20);                            
   v_WhereCondition  VARCHAR(2000);                             
   v_vcLookBackTableName  VARCHAR(2000);                      
   v_bitCustom  BOOLEAN;  
   v_bitAllowEdit  BOOLEAN;                 
   v_numFieldId  NUMERIC;  
   v_bitAllowSorting BOOLEAN;                   
   v_vcColumnName  VARCHAR(500);                            
   v_Nocolumns  SMALLINT;                      
   v_ListRelID  NUMERIC(9,0);
   v_Prefix  VARCHAR(5);
   SWV_RowCount INTEGER;
BEGIN
   select   coalesce(CI.numCompanyType,0) INTO v_numRelCntType from  DivisionMaster DM JOIN CompanyInfo CI ON DM.numCompanyID = CI.numCompanyId where DM.numDomainID = v_numDomainID and DM.numDivisionID = v_numDivisionID;

   IF EXISTS(SELECT * FROM View_DynamicColumns where numFormId = 56 and numDomainID = v_numDomainID AND numAuthGroupId = v_numUserGroupID AND coalesce(numRelCntType,0) = 0) OR EXISTS(SELECT * FROM View_DynamicCustomColumns where numFormId = 56 and numDomainID = v_numDomainID AND numAuthGroupID = v_numUserGroupID AND coalesce(numRelCntType,0) = 0) then
	
      v_numRelCntType := 0;
   end if;


   v_tintOrder := 0;                                                        
   v_WhereCondition := '';                       
                         
   v_Nocolumns := 0;                      
  
   select   coalesce(sum(TotalRow),0) INTO v_Nocolumns from(Select count(*) AS TotalRow from View_DynamicColumns where numFormId = 56 and numDomainID = v_numDomainID AND numAuthGroupId = v_numUserGroupID AND numRelCntType = v_numRelCntType
      Union
      Select count(*) AS TotalRow from View_DynamicCustomColumns where numFormId = 56 and numDomainID = v_numDomainID AND numAuthGroupID = v_numUserGroupID AND numRelCntType = v_numRelCntType) TotalRows;

   DROP TABLE IF EXISTS tt_TEMPFORM CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPFORM 
   (
      tintOrder SMALLINT,
      vcDbColumnName VARCHAR(50),
      vcOrigDbColumnName VARCHAR(50),
      vcFieldName VARCHAR(50),
      vcAssociatedControlType VARCHAR(50),
      vcListItemType VARCHAR(3),
      numListID NUMERIC(9,0),
      vcLookBackTableName VARCHAR(50),
      bitCustomField BOOLEAN,
      numFieldId NUMERIC,
      bitAllowSorting BOOLEAN,
      bitAllowEdit BOOLEAN,
      bitIsRequired BOOLEAN,
      bitIsEmail BOOLEAN,
      bitIsAlphaNumeric BOOLEAN,
      bitIsNumeric BOOLEAN,
      bitIsLengthValidation BOOLEAN,
      intMaxLength INTEGER,
      intMinLength INTEGER,
      bitFieldMessage BOOLEAN,
      vcFieldMessage VARCHAR(500),
      ListRelID NUMERIC(9,0),
      intColumnWidth INTEGER
   );
                 
     
   if v_Nocolumns = 0 then

      INSERT INTO DycFormConfigurationDetails(numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numAuthGroupID,numRelCntType,bitCustom,intColumnWidth)
      select 56,numFieldId,1,Row_number() over(order by tintRow desc),v_numDomainID,v_numUserGroupID,0,0,intColumnWidth
      FROM View_DynamicDefaultColumns
      where numFormId = 56 and bitDefault = true AND coalesce(bitSettingField,false) = true and numDomainID = v_numDomainID
      order by tintOrder asc;
   end if;
                 
   v_strSql := ' select CAST(coalesce(ADC.numContactId,0) AS VARCHAR(10)) as numContactId            
                    
       ,coalesce(DM.numDivisionID,0) AS numDivisionID, coalesce(DM.numTerID,0) AS numTerID,coalesce(ADC.numRecOwner,0) AS numRecOwner,coalesce(DM.tintCRMType,0) AS tintCRMType,
	   coalesce(ADC.bitPrimaryContact,false) as bitPrimaryContact ';                      
                         
   INSERT INTO tt_TEMPFORM
   select tintRow+1 as tintOrder,vcDbColumnName,vcOrigDbColumnName,coalesce(vcCultureFieldName,vcFieldName),
 vcAssociatedControlType,vcListItemType,numListID,vcLookBackTableName
,bitCustom,numFieldId,bitAllowSorting,bitInlineEdit,
bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage AS vcFieldMessage,ListRelID ,intColumnWidth
   FROM View_DynamicColumns
   where numFormId = 56 and numDomainID = v_numDomainID AND numAuthGroupId = v_numUserGroupID AND numRelCntType = v_numRelCntType
   AND coalesce(bitSettingField,false) = true AND coalesce(bitCustom,false) = false
   UNION
   select tintRow+1 as tintOrder,vcDbColumnName,vcDbColumnName,vcFieldName,
 vcAssociatedControlType,'' as vcListItemType,numListID,''
,bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,
bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth
   from View_DynamicCustomColumns
   where numFormId = 56 and numDomainID = v_numDomainID AND numAuthGroupID = v_numUserGroupID AND numRelCntType = v_numRelCntType
   AND coalesce(bitCustom,false) = true
   order by tintOrder asc;  
   
                                                      
   select   tintOrder+1, vcDbColumnName, vcFieldName, vcAssociatedControlType, vcListItemType, numListID, vcLookBackTableName, bitCustomField, numFieldId, bitAllowSorting, bitAllowEdit, ListRelID INTO v_tintOrder,v_vcDbColumnName,v_vcFieldName,v_vcAssociatedControlType,v_vcListItemType,
   v_numListID,v_vcLookBackTableName,v_bitCustom,v_numFieldId,
   v_bitAllowSorting,v_bitAllowEdit,v_ListRelID from  tt_TEMPFORM    order by tintOrder asc LIMIT 1;            

                   
   while v_tintOrder > 0 LOOP
      if v_bitCustom = false then
         if v_vcLookBackTableName = 'AdditionalContactsInformation' then
            v_Prefix := 'ADC.';
         end if;
         if v_vcLookBackTableName = 'DivisionMaster' then
            v_Prefix := 'DM.';
         end if;
         v_vcColumnName := coalesce(v_vcDbColumnName,'') || '~' || SUBSTR(CAST(v_numFieldId AS VARCHAR(10)),1,10) || '~0';
         if v_vcAssociatedControlType = 'SelectBox' then
        
            if v_vcListItemType = 'LI' then
     
               v_strSql := coalesce(v_strSql,'') || ',L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.vcData' || ' "' || coalesce(v_vcColumnName,'') || '"';
               v_WhereCondition := coalesce(v_WhereCondition,'') || ' left Join ListDetails L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || ' on L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.numListItemID=' || coalesce(v_vcDbColumnName,'');
            ELSEIF v_vcListItemType = 'S'
            then
     
               v_strSql := coalesce(v_strSql,'') || ',S' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.vcState' || ' "' || coalesce(v_vcColumnName,'') || '"';
               v_WhereCondition := coalesce(v_WhereCondition,'') || ' left join State S' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || ' on S' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.numStateID=' || coalesce(v_vcDbColumnName,'');
            ELSEIF v_vcListItemType = 'T'
            then
     
               v_strSql := coalesce(v_strSql,'') || ',L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.vcData' || ' "' || coalesce(v_vcColumnName,'') || '"';
               v_WhereCondition := coalesce(v_WhereCondition,'') || ' left Join ListDetails L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || ' on L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.numListItemID=' || coalesce(v_vcDbColumnName,'');
            ELSEIF v_vcListItemType = 'U'
            then
    
               v_strSql := coalesce(v_strSql,'') || ',fn_GetContactName(' || coalesce(v_Prefix,'') || coalesce(v_vcDbColumnName,'') || ') "' || coalesce(v_vcColumnName,'') || '"';
            end if;
         ELSEIF v_vcAssociatedControlType = 'DateField'
         then
 
            v_strSql := coalesce(v_strSql,'') || ',case when ' || coalesce(v_Prefix,'') || coalesce(v_vcDbColumnName,'') || ' + make_interval(mins => ' || CAST(-v_ClientTimeZoneOffset AS VARCHAR(15)) || ')::DATE = CAST(now() AS DATE) then ''<b><font color=red>Today</font></b>''';
            v_strSql := coalesce(v_strSql,'') || 'when ' || coalesce(v_Prefix,'') || coalesce(v_vcDbColumnName,'') || ' + make_interval(mins => ' || CAST(-v_ClientTimeZoneOffset AS VARCHAR(15)) || ')::DATE = CAST(now() + make_interval(days => -1) AS DATE) then ''<b><font color=purple>YesterDay</font></b>''';
            v_strSql := coalesce(v_strSql,'') || 'when ' || coalesce(v_Prefix,'') || coalesce(v_vcDbColumnName,'') || ' + make_interval(mins => ' || CAST(-v_ClientTimeZoneOffset AS VARCHAR(15)) || ')::DATE = CAST(now() + make_interval(days => 1) AS DATE) then''<b><font color=orange>Tommorow</font></b>'' ';
            v_strSql := coalesce(v_strSql,'') || 'else FormatedDateFromDate(' || coalesce(v_Prefix,'') || coalesce(v_vcDbColumnName,'') || ' + make_interval(mins => ' || CAST(-v_ClientTimeZoneOffset AS VARCHAR(15)) || ')::DATE,' || SUBSTR(CAST(v_numDomainID AS VARCHAR(10)),1,10) || ') end  "' || coalesce(v_vcColumnName,'') || '"';
         ELSEIF v_vcAssociatedControlType = 'TextBox' OR v_vcAssociatedControlType = 'Label'
         then

            v_strSql := coalesce(v_strSql,'') || ',' || case  when v_vcDbColumnName = 'numAge' then 'EXTRACT(YEAR FROM timezone(''utc'', now()))-EXTRACT(YEAR FROM bintDOB)'
            when v_vcDbColumnName = 'numDivisionID' then 'DM.numDivisionID' else v_vcDbColumnName end || ' "' || coalesce(v_vcColumnName,'') || '"';
         else
            v_strSql := coalesce(v_strSql,'') || ', ' || coalesce(v_vcDbColumnName,'') || ' "' || coalesce(v_vcColumnName,'') || '"';
         end if;
      ELSEIF v_bitCustom = true
      then

         v_vcColumnName := coalesce(v_vcDbColumnName,'') || '~' || SUBSTR(CAST(v_numFieldId AS VARCHAR(10)),1,10) || '~1';
         select   FLd_label, fld_type, 'Cust' || SUBSTR(CAST(Fld_id AS VARCHAR(10)),1,10) INTO v_vcFieldName,v_vcAssociatedControlType,v_vcDbColumnName from CFW_Fld_Master where    CFW_Fld_Master.Fld_id = v_numFieldId;
         if v_vcAssociatedControlType = 'TextBox' or v_vcAssociatedControlType = 'TextArea' then
   
            v_strSql := coalesce(v_strSql,'') || ',CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.Fld_Value  "' || coalesce(v_vcColumnName,'') || '"';
            v_WhereCondition := coalesce(v_WhereCondition,'') || ' left Join CFW_FLD_Values_Cont CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '                 
    on CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.Fld_Id=' || SUBSTR(CAST(v_numFieldId AS VARCHAR(10)),1,10) || 'and CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.RecId=ADC.numContactId   ';
         ELSEIF v_vcAssociatedControlType = 'CheckBox'
         then
   
            v_strSql := coalesce(v_strSql,'') || ',case when COALESCE(CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.Fld_Value,0)=0 then ''No'' when COALESCE(CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.Fld_Value,0)=1 then ''Yes'' end   "' || coalesce(v_vcColumnName,'') || '"';
            v_WhereCondition := coalesce(v_WhereCondition,'') || ' left Join CFW_FLD_Values_Cont CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '               
    on CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.Fld_Id=' || SUBSTR(CAST(v_numFieldId AS VARCHAR(10)),1,10) || 'and CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.RecId=ADC.numContactId   ';
         ELSEIF v_vcAssociatedControlType = 'DateField'
         then
   
            v_strSql := coalesce(v_strSql,'') || ',FormatedDateFromDate(CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.Fld_Value,' || SUBSTR(CAST(v_numDomainID AS VARCHAR(10)),1,10) || ')  "' || coalesce(v_vcColumnName,'') || '"';
            v_WhereCondition := coalesce(v_WhereCondition,'') || ' left Join CFW_FLD_Values_Cont CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '                 
    on CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.Fld_Id=' || SUBSTR(CAST(v_numFieldId AS VARCHAR(10)),1,10) || 'and CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.RecId=ADC.numContactId   ';
         ELSEIF v_vcAssociatedControlType = 'SelectBox'
         then
   
            v_vcDbColumnName := 'DCust' || SUBSTR(CAST(v_numFieldId AS VARCHAR(10)),1,10);
            v_strSql := coalesce(v_strSql,'') || ',L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.vcData' || ' "' || coalesce(v_vcColumnName,'') || '"';
            v_WhereCondition := coalesce(v_WhereCondition,'') || ' left Join CFW_FLD_Values_Cont CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '                 
     on CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.Fld_Id=' || SUBSTR(CAST(v_numFieldId AS VARCHAR(10)),1,10) || 'and CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.RecId=ADC.numContactId   ';
            v_WhereCondition := coalesce(v_WhereCondition,'') || ' left Join ListDetails L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || ' on L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.numListItemID=CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.Fld_Value';
         end if;
      end if;
      select   tintOrder+1, vcDbColumnName, vcFieldName, vcAssociatedControlType, vcListItemType, numListID, vcLookBackTableName, bitCustomField, numFieldId, bitAllowSorting, bitAllowEdit, ListRelID INTO v_tintOrder,v_vcDbColumnName,v_vcFieldName,v_vcAssociatedControlType,v_vcListItemType,
      v_numListID,v_vcLookBackTableName,v_bitCustom,v_numFieldId,
      v_bitAllowSorting,v_bitAllowEdit,v_ListRelID from tt_TEMPFORM WHERE tintOrder > v_tintOrder -1   order by tintOrder asc LIMIT 1;
      GET DIAGNOSTICS SWV_RowCount = ROW_COUNT;
      if SWV_RowCount = 0 then 
         v_tintOrder := 0;
      end if;
   END LOOP;                             
                          
                          
                                  
   v_strSql := coalesce(v_strSql,'') || ' FROM AdditionalContactsInformation ADC                                                                             
  JOIN DivisionMaster DM ON ADC.numDivisionId = DM.numDivisionID                                                                            
  JOIN CompanyInfo CI ON DM.numCompanyID = CI.numCompanyId
  LEFT JOIN AddressDetails AD ON AD.numRecordID = ADC.numContactID AND AD.tintAddressOf=1 AND AD.tintAddressType=0 AND AD.bitIsPrimary=true
   ' || coalesce(v_WhereCondition,'') || '                     
  WHERE CI.numDomainID=DM.numDomainID   
  and DM.numDomainID=ADC.numDomainID  and ADC.numDivisionID = ' || SUBSTR(CAST(v_numDivisionID AS VARCHAR(18)),1,18) || ' and ADC.numDomainID=  ' || SUBSTR(CAST(v_numDomainID AS VARCHAR(18)),1,18);
                                               
   RAISE NOTICE '%',v_strSql;                              
                      
   OPEN SWV_RefCur FOR EXECUTE v_strSql;


   open SWV_RefCur2 for
   SELECT * FROM tt_TEMPFORM;

   RETURN;
END; $$;  



