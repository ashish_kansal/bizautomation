-- Stored procedure definition script USP_GetOperationBudgetDetailsForImpact for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetOperationBudgetDetailsForImpact(v_numDomainId NUMERIC(9,0) DEFAULT 0,            
v_numBudgetId NUMERIC(9,0) DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
	open SWV_RefCur for 
	Select distinct 
		CAST(OBD.numChartAcntId AS VARCHAR(10)) || '~' || CAST(OBD.numParentAcntId AS VARCHAR(10))  as numChartAcntId
		,COA.vcAccountName as  ChartAcntname 
	From 
		OperationBudgetMaster OBM
	inner join OperationBudgetDetails OBD  on OBM.numBudgetId = OBD.numBudgetID
	inner join Chart_Of_Accounts COA on OBD.numChartAcntId = COA.numAccountId
	Where 
		OBM.numBudgetId = v_numBudgetId 
		And OBD.intYear = EXTRACT(year FROM TIMEZONE('UTC',now())) 
		And OBM.numDomainId = v_numDomainId;
END; $$;












