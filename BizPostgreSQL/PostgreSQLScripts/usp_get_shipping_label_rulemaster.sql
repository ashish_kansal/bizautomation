CREATE OR REPLACE FUNCTION USP_GET_SHIPPING_LABEL_RULEMASTER
(
	v_numShippingRuleID NUMERIC(18,0) DEFAULT 0,
	v_numDomainID NUMERIC(18,0) DEFAULT 0,
	INOUT SWV_RefCur refcursor default null
)
LANGUAGE plpgsql
AS $$
	DECLARE v_SQL  TEXT;
BEGIN
	v_SQL := ' SELECT DISTINCT
					SR.numShippingRuleID,
					vcRuleName,
					tintShipBasedOn,
					numSourceCompanyID,
					numSourceShipID,
					intItemAffected,
						CASE coalesce(intItemAffected,0)
						WHEN 1 THEN(SELECT COUNT(*) FROM PromotionOfferItems POI WHERE POI.numProId = SR.numShippingRuleID AND POI.tintType = 1 AND POI.tintRecordType = 3)
						WHEN 2 THEN(SELECT COUNT(*) FROM PromotionOfferItems POI WHERE POI.numProId = SR.numShippingRuleID AND POI.tintType = 3 AND POI.tintRecordType = 3)
					END AS ItemCount,
					SR.tintType,
					SR.numDomainID,
					CASE coalesce(intItemAffected,0)
						WHEN 1 THEN(GetItemAffectedList(SR.numShippingRuleID,CAST(1 AS SMALLINT),SR.numDomainID))
						WHEN 2 THEN(GetItemAffectedList(SR.numShippingRuleID,CAST(2 AS SMALLINT),SR.numDomainID))
						ELSE ''ALL''
					END AS ItemsAffected,
					''Item '' || CASE coalesce(tintShipBasedOn,0)
									WHEN 1 THEN ''Weight''
									WHEN 2 THEN ''Price''
									WHEN 3 THEN ''Quantity''
								END AS ShipBasedOn,
					coalesce(WD.vcProviderName,''ALL'') AS SourceMarketplace,
					CASE WHEN numSourceShipID = 91 THEN ''Fedex'' 
							WHEN numSourceShipID = 88 THEN ''UPS'' 
							WHEN numSourceShipID = 90 THEN ''USPS'' 
							WHEN numSourceShipID = 101 THEN ''Amazon Standard'' 
							WHEN numSourceShipID = 102 THEN ''ShippingMethodStandard'' 
							WHEN numSourceShipID = 103 THEN ''Other [E-Bay]'' 
							ELSE ''Other'' 
					END	 
					AS SourceShipMethod		 
				FROM ShippingLabelRuleMaster SR 
				LEFT JOIN ShippingLabelRuleChild SRC ON SR.numShippingRuleID = SRC.numShippingRuleID
				LEFT JOIN WebAPI WD ON WD.WebApiId = SR.numSourceCompanyID
				WHERE SR.numDomainID = ' || v_numDomainID || ' AND (SR.numShippingRuleID = ' || v_numShippingRuleID || ' OR ' || coalesce(v_numShippingRuleID,0) || '=0);';
				
 
	OPEN SWV_RefCur FOR EXECUTE v_SQL;
	RETURN;
END; $$;


