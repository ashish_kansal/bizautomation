-- Stored procedure definition script USP_CampaignList for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_CampaignList(         
           
--          
v_numUserCntID NUMERIC(9,0) DEFAULT 0,          
 v_numDomainID NUMERIC(9,0) DEFAULT 0,         
 v_numRegion NUMERIC(9,0) DEFAULT 0,          
 v_numCampaignType NUMERIC(9,0) DEFAULT 0,         
 v_SortChar CHAR(1) DEFAULT '0',         
 v_CurrentPage INTEGER DEFAULT NULL,          
 v_PageSize INTEGER DEFAULT NULL,          
 INOUT v_TotRecs INTEGER  DEFAULT NULL,          
 v_columnName VARCHAR(50) DEFAULT NULL,          
 v_columnSortOrder VARCHAR(10) DEFAULT NULL, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_strSql  VARCHAR(8000);          
        
        
-- REPLACE THIS CODE WITH COALESCE(vcCampaignName, '''') AS vcCampaignName if need online/offline text back
--CASE bitIsOnline WHEN 1 THEN  COALESCE(vcCampaignName, '''') + ''<font color="green"> (online) </font>''
--        WHEN 0 THEN COALESCE(vcCampaignName, '''') + ''<font color="purple"> (offline)</font>''
--        END vcCampaignName,
          
   v_firstRec  INTEGER;           
   v_lastRec  INTEGER;
BEGIN
   BEGIN
      CREATE TEMP SEQUENCE tt_tempTable_seq;
      EXCEPTION WHEN OTHERS THEN
         NULL;
   END;
   drop table IF EXISTS tt_TEMPTABLE CASCADE;
   create TEMPORARY TABLE tt_TEMPTABLE 
   ( 
      ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1) PRIMARY KEY,
      numCampaignId VARCHAR(15),
      intLaunchDate VARCHAR(25),
      intEndDate VARCHAR(25),
      vcCampaignName VARCHAR(200),
      CampaignType VARCHAR(50),
      CampaignStatus VARCHAR(50),
      CampaignRegion VARCHAR(50),
      monCampaignCost DECIMAL(20,5),
      income DECIMAL(20,5),
      ROI  DECIMAL(20,5)
   );          
          
          
   v_strSql := 'select numCampaignId,        
FormatedDateFromDate(intLaunchDate,' || SUBSTR(CAST(v_numDomainID AS VARCHAR(15)),1,15) || ') as intLaunchDate,        
FormatedDateFromDate(intEndDate,' || SUBSTR(CAST(v_numDomainID AS VARCHAR(15)),1,15) || ') as intEndDate,        
COALESCE(vcCampaignName, '''') AS vcCampaignName,
fn_GetListItemName(numCampaignType)as CampaignType,        
fn_GetListItemName(numCampaignStatus)as CampaignStatus,        
fn_GetListItemName(numRegion)as CampaignRegion,        
--monCampaignCost AS monCampaignCost1,        
COALESCE((SELECT SUM(monAmount) FROM BillDetails BD JOIN BillHeader BH ON BD.numBillID = BH.numBillID
WHERE numCampaignID = CampaignMaster.numCampaignId AND numDomainID = CampaignMaster.numDomainID ),0) AS monCampaignCost,
GetIncomeforCampaign(numCampaignId,2::SMALLINT,null,null) as income,        
GetIncomeforCampaign(numCampaignId,4::SMALLINT,null,null) as ROI        
from CampaignMaster        
where CampaignMaster.numDomainID=' || SUBSTR(CAST(v_numDomainID AS VARCHAR(15)),1,15);         
          
          
   if v_numRegion <> 0 then 
      v_strSql := coalesce(v_strSql,'') || ' And numRegion =' || SUBSTR(CAST(v_numRegion AS VARCHAR(15)),1,15);
   end if;           
   if v_numCampaignType <> 0 then 
      v_strSql := coalesce(v_strSql,'') || ' And numCampaignType =' || SUBSTR(CAST(v_numCampaignType AS VARCHAR(15)),1,15);
   end if;        
   if v_SortChar <> '0' then 
      v_strSql := coalesce(v_strSql,'') || ' And vcCampaignName like ''' || coalesce(v_SortChar,'') || '%''';
   end if;        
   if v_columnName <> '' then  
      v_strSql := coalesce(v_strSql,'') || ' ORDER BY ' || coalesce(v_columnName,'') || ' ' || coalesce(v_columnSortOrder,'');
   end if;        
   RAISE NOTICE '%',v_strSql;      
   EXECUTE 'insert into tt_TEMPTABLE(          
  numCampaignId ,          
 intLaunchDate,          
 intEndDate,          
 vcCampaignName,          
 CampaignType,          
 CampaignStatus,          
 CampaignRegion,          
 monCampaignCost,          
 income,          
 ROI)          
' || v_strSql;           
          
   v_firstRec :=(v_CurrentPage::bigint -1)*v_PageSize::bigint;          
   v_lastRec :=(v_CurrentPage::bigint*v_PageSize::bigint+1);          
   open SWV_RefCur for
   select * from tt_TEMPTABLE where ID > v_firstRec and ID < v_lastRec;          
   select count(*) INTO v_TotRecs from tt_TEMPTABLE;          
   RETURN;
END; $$;


