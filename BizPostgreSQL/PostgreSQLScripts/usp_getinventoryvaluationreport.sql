DROP FUNCTION IF EXISTS USP_GetInventoryValuationReport;

CREATE OR REPLACE FUNCTION USP_GetInventoryValuationReport(v_numDomainId NUMERIC(9,0) DEFAULT 0,
    v_ClientTimeZoneOffset INTEGER DEFAULT NULL,
	v_dtDate DATE DEFAULT NULL,
	v_tintCurrentPage INTEGER DEFAULT NULL,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   IF v_dtDate IS NULL then
      v_dtDate := CAST(TIMEZONE('UTC',now())+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) AS DATE);
   end if;

   open SWV_RefCur for SELECT
		COUNT(*) OVER() AS TotalRecords
		,Item.numItemCode
		,Item.vcItemName
		,Item.vcSKU
		,Item.numBarCodeId
		,SUM(coalesce(TEMP.numOnHand,0)+coalesce(TEMP.numAllocation,0)) AS numTotalOnHand
		,CAST((CASE WHEN SUM(coalesce(TEMP.numOnHand,0)+coalesce(TEMP.numAllocation,0)) > 0 THEN(SUM((coalesce(TEMP.numOnHand,0)+coalesce(TEMP.numAllocation,0))*coalesce(TEMP.monAverageCost,0))/SUM(coalesce(TEMP.numOnHand,0)+coalesce(TEMP.numAllocation,0))) ELSE SUM((coalesce(TEMP.numOnHand,0)+coalesce(TEMP.numAllocation,0))*coalesce(TEMP.monAverageCost,0)) END) AS DECIMAL(20,5)) AS monAverageCost
		,SUM((coalesce(TEMP.numOnHand,0)+coalesce(TEMP.numAllocation,0))*coalesce(TEMP.monAverageCost,0)) AS monTotalValuation
   FROM
   Item
   INNER JOIN
   WareHouseItems
   ON
   Item.numItemCode = WareHouseItems.numItemID
   LEFT JOIN LATERAL(SELECT * FROM
      WareHouseItems_Tracking
      WHERE
      WareHouseItems_Tracking.numWareHouseItemID = WareHouseItems.numWareHouseItemID
      AND CAST(WareHouseItems_Tracking.dtCreatedDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) AS DATE) <= v_dtDate
      ORDER BY
      dtCreatedDate DESC LIMIT 1) TEMP ON TRUE
   WHERE
   Item.numDomainID = v_numDomainId
   AND coalesce(Item.bitKitParent,false) = false
   AND TEMP.numWareHouseItemTrackingID IS NOT NULL
   AND (TEMP.numOnHand > 0 OR TEMP.numAllocation > 0)
   GROUP BY
   Item.numItemCode,Item.vcItemName,Item.vcSKU,Item.numBarCodeId
   ORDER BY
   vcItemName
   OFFSET
		CAST((CASE WHEN v_tintCurrentPage=-1 THEN 0 ELSE (v_tintCurrentPage-1) * 100 END) AS INTEGER) ROWS
	FETCH NEXT 
		CAST((CASE WHEN v_tintCurrentPage=-1 THEN 1000000000 ELSE 100 END) AS INTEGER) ROWS ONLY;
END; $$;












