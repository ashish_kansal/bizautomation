CREATE OR REPLACE FUNCTION USP_ManageTaxCountryConfiguration(v_numDomainID NUMERIC(9,0) DEFAULT 0,    
v_tintMode SMALLINT DEFAULT NULL,
v_numTaxCountryConfi NUMERIC(9,0) DEFAULT NULL,
v_numCountry NUMERIC(9,0) DEFAULT NULL,
v_tintBaseTax SMALLINT DEFAULT NULL,
v_tintBaseTaxOnArea SMALLINT DEFAULT NULL, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
BEGIN
   IF v_tintMode = 1 then

      open SWV_RefCur for
      select numTaxCountryConfi,fn_GetListItemName(numCountry) as vCountryName,
		Case tintBaseTax when 1 then 'Billing Address' when 2 then 'Shipping Address' end as vcBaseTax,
		Case tintBaseTaxOnArea when 0 then 'State' when 1 then 'City' when 2 then 'Zip Code/Postal' end as vcBaseTaxOnArea  from TaxCountryConfi where numDomainID = v_numDomainID;
   ELSEIF v_tintMode = 2
   then

      open SWV_RefCur for
      select numTaxCountryConfi,numCountry,tintBaseTax,tintBaseTaxOnArea from TaxCountryConfi where numDomainID = v_numDomainID and numTaxCountryConfi = v_numTaxCountryConfi;
   ELSEIF v_tintMode = 3
   then

      IF EXISTS(SELECT numTaxCountryConfi FROM TaxCountryConfi where numDomainID = v_numDomainID and numCountry = v_numCountry) then
	
         Update TaxCountryConfi set tintBaseTax = v_tintBaseTax,tintBaseTaxOnArea = v_tintBaseTaxOnArea where numTaxCountryConfi = v_numTaxCountryConfi and numDomainID = v_numDomainID;
      ELSE
         Insert into TaxCountryConfi(numDomainID,numCountry,tintBaseTax,tintBaseTaxOnArea)
							values(v_numDomainID,v_numCountry,v_tintBaseTax,v_tintBaseTaxOnArea);
      end if;

	  open SWV_RefCur for
      select numTaxCountryConfi,fn_GetListItemName(numCountry) as vCountryName,
		Case tintBaseTax when 1 then 'Billing Address' when 2 then 'Shipping Address' end as vcBaseTax,
		Case tintBaseTaxOnArea when 0 then 'State' when 1 then 'City' when 2 then 'Zip Code/Postal' end as vcBaseTaxOnArea  from TaxCountryConfi where numDomainID = v_numDomainID;
   ELSEIF v_tintMode = 4
   then

      open SWV_RefCur for
      select numTaxCountryConfi,numCountry,tintBaseTax,tintBaseTaxOnArea from TaxCountryConfi where numDomainID = v_numDomainID and numCountry = v_numCountry;
   end if;
   RETURN;
END; $$;



