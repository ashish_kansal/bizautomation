-- Stored procedure definition script usp_CreateInsert for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION usp_CreateInsert(v_strTableName VARCHAR(100)    
--     
)
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_StrColumns  VARCHAR(8000);
   v_EXECStatMent  VARCHAR(8000);
BEGIN
        
/*Declare  @strTableName Varchar(100)         
SET  @strTableName ='BATCH_MASTER'  */      
   v_StrColumns  := '';        
   select   coalesce(v_StrColumns,'') || sysColumns.column_name  || ', ' INTO v_StrColumns from
   sysColumns
   INNER JOIN
   sysobjects
   ON
   sysColumns.table_name = sysobjects.table_name Where
   sysobjects.routine_name = v_strTableName;        
        
   v_StrColumns  := SUBSTR(v_StrColumns,1,LENGTH(v_StrColumns) -1);        
   v_EXECStatMent := ' SELECT ''INSERT INTO ' || coalesce(v_strTableName,'') || ' ( ' || coalesce(v_StrColumns,'')   || ' ) VALUES ( '''''' || CAST(?3?/** SDynExp 4 **/  AS' || REPLACE(v_StrColumns,',',') +  ''''''  ,''''''  + Convert(Varchar,') || ' AS/** EDynExp 3 **/ VARCHAR(30)) || '''''')'' From ' || coalesce(v_strTableName,'');        
   RAISE NOTICE '%',v_EXECStatMent;  
   EXECUTE v_EXECStatMent;
   RETURN;
END; $$;


