-- Stored procedure definition script USP_GetAssetItem for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetAssetItem(v_numItemCode NUMERIC(18,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for select numDivID as numDivId,
		coalesce(numDeptId,0) as numDeptId,
		fn_GetComapnyName(numDivID) as Company,
		fn_GetListItemName(numDeptId) as Department,
		coalesce(btAppreciation,false) as btAppreciation,
		coalesce(btDepreciation,false) as btDepreciation,
		coalesce(numAppvalue,0) as numAppvalue,
		coalesce(numDepValue,0)as numDepValue,
		dtPurchase,
		dtWarrentyTill
   from CompanyAssets CA
   where CA.numItemCode = v_numItemCode;
END; $$;












