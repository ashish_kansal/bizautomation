-- Stored procedure definition script Usp_CheckServiceItemInOrder for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021

CREATE OR REPLACE FUNCTION Usp_CheckServiceItemInOrder(v_numOppID		NUMERIC(18,0),
	v_numDomainID	NUMERIC(18,0),
	v_numItemCode	NUMERIC(18,0),
	v_sMode			SMALLINT DEFAULT 0, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_RecCount  INTEGER;
BEGIN
   IF v_sMode = 0 then
		
      IF coalesce(v_numOppID,0) <> 0 then
				
         select   COUNT(*) INTO v_RecCount FROM
         OpportunityMaster OM
         JOIN OpportunityItems OI ON OM.numOppId = OI.numOppId WHERE OI.numOppId = v_numOppID
         AND numDomainId = v_numDomainID
         AND numItemCode = v_numItemCode;
         open SWV_RefCur for
         SELECT CASE WHEN v_RecCount > 0 THEN 1 ELSE 0 END AS Result;
      ELSE
         select   COUNT(*) INTO v_RecCount FROM Item WHERE numItemCode = v_numItemCode AND numDomainID = v_numDomainID AND Item.charItemType = 'S';
         open SWV_RefCur for
         SELECT CASE WHEN v_RecCount > 0 THEN 1 ELSE 0 END AS Result;
      end if;
   ELSEIF v_sMode = 1
   then
		
      select   COUNT(*) INTO v_RecCount FROM
      OpportunityMaster OM
      JOIN OpportunityBizDocs OI ON OM.numOppId = OI.numoppid AND bitAuthoritativeBizDocs = 1
      JOIN OpportunityBizDocItems OBI ON OI.numOppBizDocsId = OBI.numOppBizDocID AND OBI.numItemCode = v_numItemCode WHERE OI.numoppid = v_numOppID
      AND numDomainId = v_numDomainID;
      IF v_RecCount = 0 then
			
         select   COUNT(*) INTO v_RecCount FROM
         OpportunityMaster OM
         JOIN OpportunityBizDocs OI ON OM.numOppId = OI.numoppid AND bitAuthoritativeBizDocs = 1 WHERE OI.numoppid = v_numOppID
         AND numDomainId = v_numDomainID;
      end if;
      open SWV_RefCur for
      SELECT CASE WHEN v_RecCount > 0 THEN 1 ELSE 0 END AS Result;
   end if;
   RETURN;
END; $$;

--Created by anoop jayaraj                                                          


