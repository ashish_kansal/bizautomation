-- Stored procedure definition script USP_GetCommunicationOpenRecord for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetCommunicationOpenRecord(v_numDomainID NUMERIC(9,0),
v_numOpenRecordID NUMERIC(9,0),
v_ClientTimeZoneOffset INTEGER,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT Comm.numCommId,fn_GetContactName(Comm.numContactId) AS vcContactName,
fn_GetComapnyName(Comm.numDivisionID) AS vcDivisionName
,GetListIemName(Comm.bitTask) AS vcTask,GetListIemName(Comm.numActivity) AS vcActivity,GetListIemName(Comm.numStatus) AS vcStatus,
FormatedDateTimeFromDate(dtStartTime+CAST(-v_ClientTimeZoneOffset || 'minute' as interval),Comm.numDomainID) as dtStartTime,
FormatedDateTimeFromDate(CASE WHEN bitTask = 974 THEN dtStartTime ELSE dtEndTime END+CAST(-v_ClientTimeZoneOffset || 'minute' as interval),Comm.numDomainID) as dtEndTime,
fn_GetContactName(Comm.numAssign) AS vcAssignTo
   FROM  Communication Comm JOIN Correspondence Corr ON Comm.numCommId = Corr.numCommID
   WHERE Comm.numDomainID = v_numDomainID AND Corr.numDomainID = Comm.numDomainID
   AND Corr.tintCorrType IN(1,2,3,4) AND Corr.numOpenRecordID = v_numOpenRecordID;
END; $$;
-- EXEC USP_GetGeneralLedger 72,89,'2008-10-05 15:13:47.873','2009-10-05 15:13:47.873','','','','','','','',''













