-- Stored procedure definition script USP_SaveProcurementBudgetDetails for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_SaveProcurementBudgetDetails(v_numDomainId NUMERIC(9,0) DEFAULT 0,                 
v_numProcurementId NUMERIC(9,0) DEFAULT 0,                
v_strRow TEXT DEFAULT '',          
v_intYear INTEGER DEFAULT 0,        
v_intType INTEGER DEFAULT 0,      
v_sintByte SMALLINT DEFAULT 0,    
v_numItemGroupId NUMERIC(9,0) DEFAULT 0)
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_hDoc3  INTEGER;
   v_Date  TIMESTAMP;                    
   v_dtFiscalStDate  TIMESTAMP;                              
   v_dtFiscalEndDate  TIMESTAMP;
BEGIN            
   v_Date := TIMEZONE('UTC',now())+CAST(v_intType || 'year' as interval);               
   v_dtFiscalStDate := GetFiscalStartDate(GetFiscalyear(v_Date,v_numDomainId)::INTEGER,v_numDomainId);                           
   v_dtFiscalEndDate :=  GetFiscalStartDate(GetFiscalyear(v_Date,v_numDomainId)::INTEGER,v_numDomainId)+INTERVAL '1 year'+INTERVAL '-1 day';                     
        
   if v_sintByte = 0 then 
		--To Delete the Records          
		DELETE FROM ProcurementBudgetDetails PBD using ProcurementBudgetMaster PBM where PBM.numProcurementBudgetId = PBD.numProcurementId AND PBD.numProcurementId = v_numProcurementId And (PBD.tintMonth between EXTRACT(Month FROM v_dtFiscalStDate) and 12 and PBD.intYear = EXTRACT(Year FROM v_dtFiscalStDate)) Or (PBD.tintMonth between 1 and EXTRACT(month FROM v_dtFiscalEndDate)  and PBD.intYear = EXTRACT(Year FROM v_dtFiscalEndDate));
		Insert Into ProcurementBudgetDetails(numProcurementId,numItemGroupId,tintMonth,intYear,monAmount,vcComments)
		Select 
			numProcurementId,numItemGroupId,tintMonth,intYear,monAmount,Comments 
		from
		XMLTABLE
		(
			'NewDataSet/Table1'
			PASSING 
				CAST(v_strRow AS XML)
			COLUMNS
				id FOR ORDINALITY,
				numProcurementId NUMERIC(9,0) PATH 'numProcurementId',
				numItemGroupId NUMERIC(9,0) PATH 'numItemGroupId',
				tintMonth SMALLINT PATH 'tintMonth',
				intYear INTEGER PATH 'intYear',
				monAmount DECIMAL(20,5) PATH 'monAmount',
				Comments VARCHAR(100) PATH 'Comments'
		) AS X;
	Else      
		--To Delete Record            
		DELETE FROM ProcurementBudgetDetails PBD using ProcurementBudgetMaster PBM where PBM.numProcurementBudgetId = PBD.numProcurementId AND PBD.numProcurementId = v_numProcurementId And PBD.numItemGroupId = v_numItemGroupId
		And ((PBD.tintMonth between EXTRACT(month FROM TIMEZONE('UTC',now())) and 12 and PBD.intYear = EXTRACT(year FROM v_Date)) Or (PBD.tintMonth between 1 and 12 and PBD.intYear = EXTRACT(Year FROM v_Date)+1));
      
		Insert Into ProcurementBudgetDetails
		(
			numProcurementId,numItemGroupId,tintMonth,intYear,monAmount,vcComments
		)
		Select 
			numProcurementId,numItemGroupId,tintMonth,intYear,monAmount,Comments 
		from
		XMLTABLE
		(
			'NewDataSet/Table1'
			PASSING 
				CAST(v_strRow AS XML)
			COLUMNS
				id FOR ORDINALITY,
				numProcurementId NUMERIC(9,0) PATH 'numProcurementId',
				numItemGroupId NUMERIC(9,0) PATH 'numItemGroupId',
				tintMonth SMALLINT PATH 'tintMonth',
				intYear INTEGER PATH 'intYear',
				monAmount DECIMAL(20,5) PATH 'monAmount',
				Comments VARCHAR(100) PATH 'Comments'
		) AS X;
   end if;
   RETURN;
END; $$;


