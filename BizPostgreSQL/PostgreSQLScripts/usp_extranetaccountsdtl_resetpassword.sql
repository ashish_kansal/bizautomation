-- Stored procedure definition script USP_ExtranetAccountsDtl_ResetPassword for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ExtranetAccountsDtl_ResetPassword(v_numDomainID NUMERIC(18,0),
    v_vcResetID VARCHAR(500),
    v_vcNewPassword VARCHAR(100))
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   IF EXISTS(SELECT numExtranetDtlID FROM ExtranetAccountsDtl WHERE vcResetLinkID = v_vcResetID AND(EXTRACT(DAY FROM TIMEZONE('UTC',now()) -vcResetLinkCreatedTime)*60*24+EXTRACT(HOUR FROM TIMEZONE('UTC',now()) -vcResetLinkCreatedTime)*60+EXTRACT(MINUTE FROM TIMEZONE('UTC',now()) -vcResetLinkCreatedTime)) <= 10) then
	
      UPDATE
      ExtranetAccountsDtl
      SET
      vcPassword = v_vcNewPassword
      WHERE
      vcResetLinkID = v_vcResetID;
      UPDATE
      ExtranetAccountsDtl
      SET
      vcResetLinkID = ''
      WHERE
      vcResetLinkID = v_vcResetID;
   ELSE
      RAISE EXCEPTION 'INVALID_RESET_LINK';
   end if;
   RETURN;
END; $$;


