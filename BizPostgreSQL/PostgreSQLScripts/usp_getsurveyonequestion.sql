-- Stored procedure definition script usp_GetSurveyOneQuestion for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION usp_GetSurveyOneQuestion(   
--
v_numQuestionID NUMERIC,
	v_numContactID NUMERIC DEFAULT 0, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
--This procedure will return all surveys for the selected Domain.
   AS $$
BEGIN

/*		select vcCompanyName+', '+vcDivisionName as companyname, vcContactFName+' '+vcContactLName as contactname, vcPosition as position, 
		vcEmail as email, vcProfile as profile, vcPhone+', '+vcExtn  as phone, vcanstext as answer from surveyrespondants, surveyresponse
		where surveyresponse.numrespondantid = surveyrespondants.numrespondantid
		and numquestionid =@numQuestionID
*/
   If v_numContactID = 0 then

      open SWV_RefCur for
      select vcCompanyName || ', ' || vcDivisionName as companyname, vcContactFName || ' ' || vcContactLName as contactname, vcPosition as "position",
		vcEmail as email, vcProfile as profile, vcPhone || ', ' || vcExtn  as phone, vcanstext as answer,
		vcquestion as question, surveyrespondants.bitpromoted from surveyrespondants, SurveyResponse, SurveyQuestionMaster
      where SurveyResponse.numRespondantID = surveyrespondants.numrespondantid
      and SurveyResponse.numQuestionID = SurveyQuestionMaster.numQID
      and numquestionid = v_numQuestionID;
   Else
      open SWV_RefCur for
      select vcCompanyName || ', ' || vcDivisionName as companyname, vcContactFName || ' ' || vcContactLName as contactname, vcPosition as "position",
		vcEmail as email, vcProfile as profile, vcPhone || ', ' || vcExtn  as phone, vcanstext as answer,
		vcquestion as question, surveyrespondants.bitpromoted from surveyrespondants, SurveyResponse, SurveyQuestionMaster
      where SurveyResponse.numRespondantID = surveyrespondants.numrespondantid
      and SurveyResponse.numQuestionID = SurveyQuestionMaster.numQID
      and numquestionid = v_numQuestionID
      and vcContactFName =(Select vcFirstName from AdditionalContactsInformation where numContactId = v_numContactID);
   end if;
   RETURN;
END; $$;


