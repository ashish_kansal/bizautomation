-- Function definition script GetItemQuantityToOrder for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION GetItemQuantityToOrder(v_numItemCode NUMERIC(9,0),
	  v_numWarehouseItemID NUMERIC(9,0),
      v_numDomainID NUMERIC(9,0),
	  v_numAnalysisDays DOUBLE PRECISION,
	  v_numForecastDays DOUBLE PRECISION,
	  v_numTotalQtySold DOUBLE PRECISION,
	  v_numVendorID NUMERIC(18,0))
RETURNS TABLE 
(
   numItemCode NUMERIC(9,0), 
   numOnHand DOUBLE PRECISION,  
   numOnOrder DOUBLE PRECISION, 
   numAllocation DOUBLE PRECISION,
   numBackOrder DOUBLE PRECISION, 
   numReOrder DOUBLE PRECISION,
   numQtyToOrder INTEGER, 
   dtOrderDate TIMESTAMP, 
   vcOrderDate VARCHAR(50),
   numDivisionID NUMERIC(18,0), 
   monListPrice DECIMAL(20,5),
   intMinQty INTEGER,
   intLeadTimeDays INTEGER
) LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numQtySoldPerDay  DOUBLE PRECISION DEFAULT 0;
   v_numRequiredForecastQuantity  DOUBLE PRECISION DEFAULT 0;
   v_numQtyToOrder  DOUBLE PRECISION DEFAULT 0;
   v_numOnHand  DOUBLE PRECISION DEFAULT 0;
   v_numOnOrder  DOUBLE PRECISION DEFAULT 0;
   v_numAllocation  DOUBLE PRECISION DEFAULT 0;
   v_numBackOrder  DOUBLE PRECISION DEFAULT 0;
   v_numReOrder  DOUBLE PRECISION DEFAULT 0;
   v_dtOrderDate  TIMESTAMP;
   v_numDivisionID  NUMERIC(18,0) DEFAULT 0;
   v_monListPrice  DECIMAL(20,5) DEFAULT 0;
   v_numMinimumQty  INTEGER DEFAULT 0;
   v_intLeadTimeDays  INTEGER DEFAULT 0;
   v_numDays  INTEGER DEFAULT 0;

	-- Get Back Order Quantity of Item
BEGIN
   DROP TABLE IF EXISTS tt_GETITEMQUANTITYTOORDER_TEMP CASCADE;
   CREATE TEMPORARY TABLE IF NOT EXISTS  tt_GETITEMQUANTITYTOORDER_TEMP
   (
      numItemCode NUMERIC(9,0),
      numOnHand DOUBLE PRECISION,
      numOnOrder DOUBLE PRECISION,
      numAllocation DOUBLE PRECISION,
      numBackOrder DOUBLE PRECISION,
      numReOrder DOUBLE PRECISION,
      numQtyToOrder INTEGER,
      dtOrderDate TIMESTAMP,
      vcOrderDate VARCHAR(50),
      numDivisionID NUMERIC(18,0),
      monListPrice DECIMAL(20,5),
      intMinQty INTEGER,
      intLeadTimeDays INTEGER
   );
   select   coalesce(WareHouseItems.numOnHand,0), coalesce(WareHouseItems.numOnOrder,0), coalesce(WareHouseItems.numAllocation,0), coalesce(WareHouseItems.numBackOrder,0), coalesce(WareHouseItems.numReorder,0) INTO v_numOnHand,v_numOnOrder,v_numAllocation,v_numBackOrder,v_numReOrder FROM
   WareHouseItems WHERE
   numItemID = v_numItemCode AND
   numWareHouseItemID = v_numWarehouseItemID;

	--If total quantity sold for selected period is greater then 0 then process further
   IF v_numTotalQtySold > 0 then
	
      v_numQtySoldPerDay := v_numTotalQtySold/v_numAnalysisDays;

		--If average quantity sold per day is greater then 0 then process further
      IF v_numQtySoldPerDay > 0 then
		
         v_numRequiredForecastQuantity := v_numQtySoldPerDay*coalesce(v_numForecastDays,0);

			-- If item already have some back order quantity then we have to add it to required quantity to purchase
         v_numRequiredForecastQuantity := v_numRequiredForecastQuantity+coalesce(v_numBackOrder,0);
         v_numQtyToOrder := coalesce(CEIL(v_numRequiredForecastQuantity),0);


			--If item has enought quantity for required days then no need to 
			-- then there is no need to make order
         IF(v_numOnHand+v_numOnOrder) > v_numQtyToOrder then
			
            v_numQtyToOrder := 0;
         ELSE
				-- Else get required quantity ot order after removing items on hand
            v_numQtyToOrder := v_numQtyToOrder -(v_numOnHand+v_numOnOrder);
         end if;
      end if;
   end if;

   IF coalesce(v_numQtyToOrder,0) > 0 then
	
		-- When this function is called from USP_DemandForecast_Execute then vendor is passed null 
		-- so get performance improvment becaues vendor price checking is not happend for all items
      IF coalesce(v_numVendorID,0) > 0 then
		
		-- Select Item Vendor Detail
         select   numDivisionID, monPrice, intMinQty, intLeadTimeDays INTO v_numDivisionID,v_monListPrice,v_numMinimumQty,v_intLeadTimeDays FROM
         GetItemPurchaseVendorPrice(v_numVendorID,v_numItemCode,v_numDomainID,v_numQtyToOrder);
      end if;
		

		-- Find number of days after which order needs to be placed
		-- Lets on hand = 60, itemes sold per days = 10 
		-- then number of days  = (60/10) - @numLaedTimeDays
      v_numDays := FLOOR((v_numOnHand+v_numOnOrder)/v_numQtySoldPerDay) -v_intLeadTimeDays::bigint;
      IF v_numDays < 0 then
         v_numDays := 0;
      end if;
      v_dtOrderDate := LOCALTIMESTAMP+CAST(v_numDays || 'day' as interval);
      IF TO_CHAR(v_dtOrderDate,'Day') = 'Saturday' then
		
         v_dtOrderDate := v_dtOrderDate+INTERVAL '-1 day';
         IF CAST(v_dtOrderDate AS DATE) < CAST(LOCALTIMESTAMP  AS DATE) then
            v_dtOrderDate := LOCALTIMESTAMP;
         end if;
      ELSEIF TO_CHAR(v_dtOrderDate,'Day') = 'Sunday'
      then
		
         v_dtOrderDate := v_dtOrderDate+INTERVAL '-2 day';
         IF CAST(v_dtOrderDate AS DATE) < CAST(LOCALTIMESTAMP  AS DATE) then
            v_dtOrderDate := LOCALTIMESTAMP;
         end if;
      end if;
		
		-- If order date goes more than required forecast date select date with number of forecast date
      IF CAST(v_dtOrderDate AS DATE) > CAST(LOCALTIMESTAMP+CAST(v_numForecastDays || 'day' as interval) AS DATE) then
		
         v_dtOrderDate := LOCALTIMESTAMP+CAST(v_numForecastDays || 'day' as interval);
      end if;
   end if;

	

   INSERT INTO tt_GETITEMQUANTITYTOORDER_TEMP(numItemCode,
			numOnHand,
			numOnOrder,
			numAllocation,
			numBackOrder,
			numReOrder,
			numQtyToOrder,
			dtOrderDate,
			vcOrderDate,
			numDivisionID,
			monListPrice,
			intMinQty,
			intLeadTimeDays)
	VALUES(v_numItemCode,
			v_numOnHand,
			v_numOnOrder,
			v_numAllocation,
			v_numBackOrder,
			v_numReOrder,
			CEIL(v_numQtyToOrder),
			v_dtOrderDate,
			(CASE WHEN CAST(v_dtOrderDate AS DATE) = CAST(LOCALTIMESTAMP AS DATE) THEN
      '<b><font color=red>Today</font></b>'
   WHEN CAST(v_dtOrderDate AS DATE) = CAST(LOCALTIMESTAMP+INTERVAL '1 day' AS DATE) THEN
      '<b><font color=purple>Tomorrow</font></b>'
   ELSE
      FormatedDateFromDate(v_dtOrderDate,v_numDomainID)
   END),
			v_numDivisionID,
			v_monListPrice,
			v_numMinimumQty,
			v_intLeadTimeDays);

	RETURN QUERY (SELECT * FROM tt_GETITEMQUANTITYTOORDER_TEMP);
END; $$;

