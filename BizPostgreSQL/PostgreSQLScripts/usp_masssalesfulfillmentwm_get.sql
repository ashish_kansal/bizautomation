-- Stored procedure definition script USP_MassSalesFulfillmentWM_Get for PostgreSQL
CREATE OR REPLACE FUNCTION USP_MassSalesFulfillmentWM_Get(v_numDomainID NUMERIC(18,0)
	,v_numPageIndex INTEGER
	,v_numPageSize INTEGER
	,v_vcSources TEXT
	,v_vcCountries TEXT
	,v_vcStates TEXT,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT
   COUNT(*) OVER() AS "numTotalRecords"
		,MSFWM.ID AS "ID"
		,(CASE
   WHEN coalesce(numOrderSource,0) = 0 AND tintSourceType = 1 THEN 'Internal Order'
   WHEN coalesce(numOrderSource,0) > 0 AND tintSourceType = 1 THEN coalesce(LDOS.vcData,'-')
   WHEN tintSourceType = 2 THEN coalesce(S.vcSiteName,'-')
   ELSE '-'
   END) AS "vcOrderSource"
		,coalesce(LDCountry.vcData,'-') AS "vcCountry"
		,COALESCE((SELECT string_agg(CONCAT((CASE WHEN LENGTH(coalesce(LDS.vcAbbreviations,'')) > 0 THEN LDS.vcAbbreviations ELSE coalesce(LDS.vcState,'') END)),', ' ORDER BY(CASE WHEN LENGTH(coalesce(LDS.vcAbbreviations,'')) > 0 THEN LDS.vcAbbreviations ELSE coalesce(LDS.vcState,'') END))
      FROM
      MassSalesFulfillmentWMState MSFWMS
      INNER JOIN
      State LDS
      ON
      LDS.numDomainID = v_numDomainID
      AND MSFWMS.numStateID = LDS.numStateID
      WHERE
      MSFWMS.numMSFWMID = MSFWM.ID),'')  AS "vcStates"
		,COALESCE((SELECT string_agg(CONCAT('(',intOrder,') ',coalesce(vcWareHouse,'-')),', ' ORDER BY MSFWWP.intOrder)
      FROM
      MassSalesFulfillmentWP MSFWWP
      INNER JOIN
      Warehouses W
      ON
      W.numDomainID = v_numDomainID
      AND MSFWWP.numWarehouseID = W.numWareHouseID
      WHERE
      MSFWWP.numMSFWMID = MSFWM.ID),'') AS "vcWarehouses"
   FROM
   MassSalesFulfillmentWM MSFWM
   LEFT JOIN
   Listdetails LDOS
   ON
   LDOS.numListID = 9
   AND MSFWM.numOrderSource = LDOS.numListItemID
   AND MSFWM.tintSourceType = 1
   LEFT JOIN
   Sites S
   ON
   S.numDomainID = v_numDomainID
   AND MSFWM.numOrderSource = S.numSiteID
   AND MSFWM.tintSourceType = 2
   LEFT JOIN
   Listdetails LDCountry
   ON
   LDCountry.numListID = 40
   AND MSFWM.numCountryID = LDCountry.numListItemID
   WHERE
   MSFWM.numDomainID = v_numDomainID
   AND (LENGTH(coalesce(v_vcSources,'')) = 0 OR CONCAT(numOrderSource,'~',tintSourceType) IN(SELECT OutParam  FROM SplitString(v_vcSources,',')))
   AND (LENGTH(coalesce(v_vcCountries,'')) = 0 OR numCountryID IN(SELECT ID  FROM SplitIDs(v_vcCountries,',')))
   AND (LENGTH(coalesce(v_vcStates,'')) = 0 OR EXISTS(SELECT MSFWMS.ID  FROM MassSalesFulfillmentWMState MSFWMS WHERE MSFWMS.numMSFWMID = MSFWM.ID AND MSFWMS.numStateID IN(SELECT ID FROM SplitIDs(v_vcStates,','))))
   ORDER BY
   LDOS.vcData;
END; $$;












