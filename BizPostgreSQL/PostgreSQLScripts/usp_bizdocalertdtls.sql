-- Stored procedure definition script USP_BizDOcAlertDtls for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_BizDOcAlertDtls(v_numBizDocID NUMERIC DEFAULT 0,
v_bitCreated BOOLEAN DEFAULT false,
v_bitModified BOOLEAN DEFAULT false,
v_bitApproved BOOLEAN DEFAULT false,
v_numDomainID NUMERIC DEFAULT NULL, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
BEGIN
   if v_numBizDocID = 0 then
      open SWV_RefCur for
      select numBizDocAlertID,fn_GetListItemName(numBizDocID) as BizDoc,
	CAST(bitCreated AS VARCHAR(30)) || '~' ||
      CAST(bitModified AS VARCHAR(30)) || '~' ||
      CAST(bitApproved AS VARCHAR(30)) as "Is",
	numEmailTemplate,
	vcDocName,
	CAST(bitOppOwner AS VARCHAR(30)) || '~' ||
      CAST(bitCCManager AS VARCHAR(30)) || '~' ||
      CAST(bitPriConatct AS VARCHAR(30)) as "To"
      from BizDocAlerts Biz
      left join GenericDocuments as Doc
      on Doc.numGenericDocID = Biz.numEmailTemplate
      WHERE Biz.numDomainID = v_numDomainID;
   end if;
      
   if v_numBizDocID > 0 then

      IF	v_bitCreated = true then
	
         open SWV_RefCur for
         select * from BizDocAlerts where numBizDocID = v_numBizDocID AND numDomainID = v_numDomainID
         AND bitCreated = v_bitCreated;
      ELSEIF v_bitModified = true
      then
	
         open SWV_RefCur for
         select * from BizDocAlerts where numBizDocID = v_numBizDocID AND numDomainID = v_numDomainID
         AND bitModified = v_bitModified;
      ELSEIF v_bitApproved = true
      then
	
         open SWV_RefCur for
         select * from BizDocAlerts where numBizDocID = v_numBizDocID AND numDomainID = v_numDomainID
         AND bitApproved = v_bitApproved;
      ELSE
         open SWV_RefCur for
         select * from BizDocAlerts where numBizDocID = v_numBizDocID AND numDomainID = v_numDomainID;
      end if;
   end if;
   RETURN;
END; $$;
	


