-- Stored procedure definition script USP_IncomeExpenseBiz for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021

CREATE OR REPLACE FUNCTION USP_IncomeExpenseBiz(INOUT SWV_RefCur refcursor , INOUT SWV_RefCur2 refcursor)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_discardDivision  VARCHAR(500);
BEGIN
   v_discardDivision := '1,16511,16222,15991,99630,99707,99708,99709,99710,99711,99712,92938,98189';
	
	 DROP TABLE IF EXISTS tt_TEMP CASCADE;
	CREATE TEMPORARY TABLE tt_TEMP ON COMMIT DROP AS WITH    tblSubscriber
              AS ( SELECT   ROW_NUMBER() OVER ( ORDER BY numSubscriberID DESC ) AS RowNumber,
                            numSubscriberID
                   FROM     Subscribers
                            JOIN Divisionmaster D ON D.numDivisionID = Subscribers.numDivisionID
                            JOIN Companyinfo C ON C.numCompanyID = D.numCompanyID
                            JOIN AdditionalContactsInformation A ON A.numContactID = Subscribers.numAdminContactID
                   WHERE    bitDeleted = false
                 )
	
  
   
      SELECT  RowNumber,
                T.numSubscriberID,
                S.numDivisionid,
                numContactId,
                vcCompanyName,
                coalesce(vcFirstName,'') || ' ' || coalesce(vcLastname,'') AS AdminContact,
                intNoofUsersSubscribed,
                intNoOfPartners,
                FormatedDateTimeFromDate(dtSubStartDate,1::NUMERIC) AS dtSubStartDate,
                FormatedDateTimeFromDate(dtSubEndDate,1::NUMERIC) AS dtSubEndDate,
                CASE WHEN bitTrial = false THEN 'No'
      WHEN bitTrial = true THEN 'Yes'
      ELSE 'No'
      END AS Trial,
                CASE WHEN bitActive = 0 THEN 'Suspended'
      WHEN bitActive = 1 THEN 'Active'
      ELSE 'Suspended'
      END AS Status,
                FormatedDateTimeFromDate(dtSuspendedDate+INTERVAL '330 minute',1::NUMERIC) AS dtSuspendedDate,
                vcSuspendedReason,
                vcEmail,
                coalesce((SELECT SUM(coalesce(SpaceOccupied,0))
      FROM   View_InboxCount
      WHERE  numDomainID = S.numTargetDomainID),0) AS TotalSize
	
      FROM    tblSubscriber T
      JOIN Subscribers S ON S.numSubscriberID = T.numSubscriberID
      JOIN DivisionMaster D ON D.numDivisionID = S.numDivisionid
      JOIN CompanyInfo C ON C.numCompanyId = D.numCompanyID
      JOIN AdditionalContactsInformation A ON A.numContactId = S.numAdminContactID
      AND bitTrial = false
      AND bitActive = 1
      AND dtSubEndDate > GetUTCDateWithoutTime()+INTERVAL '-2 month'
      AND S.numDivisionid NOT IN(SELECT id FROM SplitIDs(v_discardDivision,','))
      ORDER BY dtsubEndDate DESC; 
        
        
   open SWV_RefCur for
   SELECT * FROM tt_TEMP; 
        
   open SWV_RefCur2 for
   SELECT SUM(intNoofUsersSubscribed*49) AS TotalIncome,sum(intNoofUsersSubscribed) as totalpaidSubscriber FROM tt_TEMP; 
        
   RETURN;
END; $$;


