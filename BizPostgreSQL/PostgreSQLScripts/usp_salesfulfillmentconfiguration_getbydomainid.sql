-- Stored procedure definition script USP_SalesFulfillmentConfiguration_GetByDomainID for PostgreSQL
CREATE OR REPLACE FUNCTION USP_SalesFulfillmentConfiguration_GetByDomainID(v_numDomainID NUMERIC(18,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT * FROM SalesFulfillmentConfiguration WHERE numDomainID = v_numDomainID;
END; $$;











