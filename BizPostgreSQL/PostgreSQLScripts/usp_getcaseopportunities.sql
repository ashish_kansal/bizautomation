-- Stored procedure definition script Usp_GetCaseOpportunities for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION Usp_GetCaseOpportunities(v_numcaseId NUMERIC,
    v_numDomainId NUMERIC,
    v_vcOppName VARCHAR(200) DEFAULT '', INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_OppID  NUMERIC;
BEGIN
   IF v_vcOppName = '' then
        
      open SWV_RefCur for
      SELECT  OI.numOppId,
                    OI.numoppitemtCode,
                    OI.vcPathForTImage,
                    OI.numItemCode,
                    OI.vcItemName,
                    OI.vcModelID,
                    OI.vcItemDesc,
                    OI.numUnitHour,
                    OI.monTotAmount,
                    GetSerialNumberList(OI.numoppitemtCode) AS vcSerialNo,
                    C.vcCaseNumber
      FROM    CaseOpportunities CO
      INNER JOIN OpportunityItems OI ON CO.numOppItemID = OI.numoppitemtCode
      AND CO.numoppid = OI.numOppId
      INNER JOIN Cases C ON C.numCaseId = CO.numCaseId
      WHERE   CO.numDomainid = v_numDomainId
      AND CO.numCaseId = v_numcaseId;
   ELSE
      select   numOppId INTO v_OppID FROM    OpportunityMaster OM WHERE   OM.numDomainId = v_numDomainId
      AND OM.numDivisionId IN(SELECT numDivisionID FROM Cases WHERE numCaseId = v_numcaseId)
      AND OM.numContactId IN(SELECT numContactId FROM Cases WHERE numCaseId = v_numcaseId)
      AND OM.tintopptype = 1 -- only sales orders
      AND OM.vcpOppName ilike '%' || coalesce(v_vcOppName,'') || '%'    LIMIT 1;
      open SWV_RefCur for
      SELECT  OI.numOppId,
                    OI.numoppitemtCode,
                    OI.vcPathForTImage,
                    OI.numItemCode,
                    OI.vcItemName,
                    OI.vcModelID,
                    OI.vcItemDesc,
                    OI.numUnitHour,
                    OI.monTotAmount,
                    GetSerialNumberList(OI.numoppitemtCode) AS vcSerialNo
      FROM
      OpportunityItems OI
      INNER JOIN OpportunityMaster OM ON OI.numOppId = OM.numOppId
      WHERE   OM.numDomainId = v_numDomainId
      AND OI.numOppId = v_OppID;
   end if;
            

/****** Object:  StoredProcedure [dbo].[USP_GetCaseReportForAgents]    Script Date: 07/26/2008 16:16:30 ******/
   RETURN;
END; $$;


