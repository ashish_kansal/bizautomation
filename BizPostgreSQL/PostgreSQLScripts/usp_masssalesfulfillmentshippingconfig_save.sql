-- Stored procedure definition script USP_MassSalesFulfillmentShippingConfig_Save for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_MassSalesFulfillmentShippingConfig_Save(v_numDomainID NUMERIC(18,0)
	,v_numOrderSource NUMERIC(18,0)
	,v_tintSourceType SMALLINT
	,v_tintType SMALLINT
	,v_vcPriorities VARCHAR(50)
	,v_numShipVia NUMERIC(18,0)
	,v_numShipService NUMERIC(18,0)
	,v_bitOverride BOOLEAN
	,v_fltWeight DOUBLE PRECISION
	,v_numShipViaOverride NUMERIC(18,0))
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   IF EXISTS(SELECT ID FROM MassSalesFulfillmentShippingConfig WHERE numDomainID = v_numDomainID AND numOrderSource = v_numOrderSource AND tintSourceType = v_tintSourceType) then
	
      UPDATE
      MassSalesFulfillmentShippingConfig
      SET
      tintType = v_tintType,vcPriorities = v_vcPriorities,numShipVia = v_numShipVia,
      numShipService = v_numShipService,bitOverride = v_bitOverride,fltWeight = v_fltWeight,
      numShipViaOverride = v_numShipViaOverride
      WHERE
      numDomainID = v_numDomainID
      AND numOrderSource = v_numOrderSource
      AND tintSourceType = v_tintSourceType;
   ELSE
      INSERT INTO MassSalesFulfillmentShippingConfig(numDomainID
			,numOrderSource
			,tintSourceType
			,tintType
			,vcPriorities
			,numShipVia
			,numShipService
			,bitOverride
			,fltWeight
			,numShipViaOverride)
		VALUES(v_numDomainID
			,v_numOrderSource
			,v_tintSourceType
			,v_tintType
			,v_vcPriorities
			,v_numShipVia
			,v_numShipService
			,v_bitOverride
			,v_fltWeight
			,v_numShipViaOverride);
   end if;
   RETURN;
END; $$;


