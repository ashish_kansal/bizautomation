-- FUNCTION: public.usp_getchildopportunity(numeric, numeric, refcursor)

-- DROP FUNCTION public.usp_getchildopportunity(numeric, numeric, refcursor);

CREATE OR REPLACE FUNCTION public.usp_getchildopportunity(
	v_numoppid numeric,
	v_numdomainid numeric,
	INOUT swv_refcur refcursor DEFAULT NULL::refcursor)
    RETURNS refcursor
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
AS $BODY$
BEGIN
-- This function was converted on Sat Apr 10 15:46:46 2021 using Ispirer SQLWays 9.0 Build 6532 64bit Licensed to BizAutomation - Sandeep Patel - India - Ispirer MnMTK 2020 MSSQLServer to PostgreSQL Database Migration Professional Project License (50 GB, 750 Tables, 426500 LOC, 3 Extension Requests, 1 Month, 20210508).
   open SWV_RefCur for SELECT vcpOppName, OM.numContactId, OM.numDivisionId, CAST(OM.txtComments AS TEXT) AS Comments,
			cast(coalesce(LD.vcData,'') as VARCHAR(255)) AS vcStatus,
			cast(coalesce(ACI.vcFirstName || ' ' || ACI.vcLastname,'') as VARCHAR(50)) AS RecordContact,
			cast(coalesce(CI.vcCompanyName,'') as VARCHAR(255)) AS RecordOrganization, OM.numOppId,
			OM.tintopptype
   FROM  OpportunityLinking OL
   INNER JOIN OpportunityMaster OM	ON OL.numChildOppID = OM.numOppId
   LEFT JOIN Listdetails LD ON LD.numListItemID = OM.numStatus
   AND (LD.numDomainid = v_numDomainId OR constFlag = TRUE)
   LEFT JOIN AdditionalContactsInformation ACI ON ACI.numContactId = OM.numContactId
   LEFT JOIN DivisionMaster DM ON DM.numDivisionID = OM.numDivisionId
   LEFT JOIN CompanyInfo CI ON CI.numCompanyId = DM.numCompanyID
   LEFT OUTER JOIN OpportunityItems OI ON OI.numOppId = OM.numOppId
   LEFT OUTER JOIN Item I ON OI.numItemCode = I.numItemCode
   WHERE OM.numDomainId = v_numDomainId
   AND OL.numParentOppID = v_numOppId
   UNION
   SELECT vcpOppName, OM.numContactId, OM.numDivisionId, CAST(OM.txtComments AS TEXT) AS Comments,
			cast(coalesce(LD.vcData,'') as VARCHAR(255)) AS vcStatus,
			cast(coalesce(ACI.vcFirstName || ' ' || ACI.vcLastname,'') as VARCHAR(50)) AS RecordContact,
			cast(coalesce(CI.vcCompanyName,'') as VARCHAR(255)) AS RecordOrganization, OM.numOppId,
			OM.tintopptype
   FROM RecurringTransactionReport RTR
   INNER JOIN OpportunityMaster OM ON RTR.numRecTranOppID = OM.numOppId
   LEFT JOIN Listdetails LD ON LD.numListItemID = OM.numStatus
   AND (LD.numDomainid = v_numDomainId OR constFlag = TRUE)
   LEFT JOIN AdditionalContactsInformation ACI ON ACI.numContactId = OM.numContactId
   LEFT JOIN DivisionMaster DM ON DM.numDivisionID = OM.numDivisionId
   LEFT JOIN CompanyInfo CI ON CI.numCompanyId = DM.numCompanyID
   LEFT OUTER JOIN OpportunityItems OI ON OI.numOppId = OM.numOppId
   LEFT OUTER JOIN Item I ON OI.numItemCode = I.numItemCode
   WHERE  OM.numDomainId = v_numDomainId
   AND numRecTranSeedId = v_numOppId;
END;
$BODY$;

ALTER FUNCTION public.usp_getchildopportunity(numeric, numeric, refcursor)
    OWNER TO postgres;
