-- Stored procedure definition script Report_VendorCOI for PostgreSQL
CREATE OR REPLACE FUNCTION Report_VendorCOI(v_AccountId INTEGER DEFAULT 0 ,
v_CompanyId INTEGER DEFAULT NULL,
v_BranchId INTEGER DEFAULT NULL,
v_VendorId INTEGER DEFAULT NULL,
v_InsuranceType SMALLINT DEFAULT 0,
v_SortColumn TEXT DEFAULT 'ExpirationDate' ,
v_SortOder TEXT DEFAULT 'Desc' ,
v_PageNo INTEGER DEFAULT 1 ,
v_Records INTEGER DEFAULT 20,
v_DefaultAccessCompanyId INTEGER DEFAULT NULL ,
v_CurrentUserId INTEGER DEFAULT NULL,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   IF (v_CompanyId = 0) then
      v_CompanyId := NULL;
   end if;
   IF (v_BranchId = 0) then
      v_BranchId := NULL;
   end if;
   IF (v_VendorId = 0) then
      v_VendorId := NULL;
   end if;
   IF (v_InsuranceType = 0) then
      v_InsuranceType := NULL;
   end if;

   open SWV_RefCur for
   with CTE
   AS(SELECT
   cast(VC.CertificateOfInsuranceId as VARCHAR(255)),
cast(VC.CompanyId as BIGINT),
cast(C.CompanyName as VARCHAR(255)),
cast(VC.BranchId as VARCHAR(255)),
cast(b.BranchName as VARCHAR(255)),
cast(VC.VendorId as VARCHAR(255)),
cast(V.VendorName as VARCHAR(255)),
cast(VC.LiabilityBroker as VARCHAR(255)),
cast(IC.InsuranceCompanyName as VARCHAR(255)),
cast(VC.LiabilityDefaultEmail as VARCHAR(255)) AS BrokerEmail,
cast(VC.LiabilityDefaultPhoneNumber as VARCHAR(255)) AS BrokertPhoneNumber,
cast(VC.LiabilityExpirationDate as VARCHAR(255)) AS ExpirationDate,
1 AS InsuranceType
   FROM VendorCOI VC
   JOIN Company C ON C.CompanyId = VC.CompanyId
   JOIN Branch b ON b.BranchId = VC.BranchId
   JOIN Vendor V ON V.VendorId = VC.VendorId
   LEFT JOIN InsuranceCompany IC ON IC.InsuranceCompanyId = VC.LiabilityInsuranceCompanyId
   WHERE coalesce(LiabilityExpirationDate,'') <> '' AND LiabilityInsuranceCompanyId > 0
   AND VC.AccountId = v_AccountId
   UNION ALL
   SELECT
   cast(VC.CertificateOfInsuranceId as VARCHAR(255)),
cast(VC.CompanyId as BIGINT),
cast(C.CompanyName as VARCHAR(255)),
cast(VC.BranchId as VARCHAR(255)),
cast(b.BranchName as VARCHAR(255)),
cast(VC.VendorId as VARCHAR(255)),
cast(V.VendorName as VARCHAR(255)),
cast(VC.LiabilityBroker as VARCHAR(255)),
cast(IC.InsuranceCompanyName as VARCHAR(255)),
cast(VC.AutomobileDefaultEmail as VARCHAR(255)) AS BrokerEmail,
cast(VC.AutomobileDefaultPhoneNumber as VARCHAR(255)) AS BrokertPhoneNumber,
cast(VC.AutomobileExpirationDate as VARCHAR(255)) AS ExpirationDate,
2 AS InsuranceType
   FROM VendorCOI VC
   JOIN Company C ON C.CompanyId = VC.CompanyId
   JOIN Branch b ON b.BranchId = VC.BranchId
   JOIN Vendor V ON V.VendorId = VC.VendorId
   JOIN InsuranceCompany IC ON IC.InsuranceCompanyId = VC.AutomobileInsuranceCompanyId
   WHERE coalesce(VC.AutomobileExpirationDate,'') <> '' AND VC.AutomobileInsuranceCompanyId > 0
   AND VC.AccountId = v_AccountId
   UNION ALL
   SELECT
   cast(VC.CertificateOfInsuranceId as VARCHAR(255)),
cast(VC.CompanyId as BIGINT),
cast(C.CompanyName as VARCHAR(255)),
cast(VC.BranchId as VARCHAR(255)),
cast(b.BranchName as VARCHAR(255)),
cast(VC.VendorId as VARCHAR(255)),
cast(V.VendorName as VARCHAR(255)),
cast(VC.ExcessiveUmbrellaBroker as VARCHAR(255)),
cast(IC.InsuranceCompanyName as VARCHAR(255)),
cast(VC.ExcessiveUmbrellaDefaultEmail as VARCHAR(255)) AS BrokerEmail,
cast(VC.ExcessiveUmbrellaDefaultPhoneNumber as VARCHAR(255)) AS BrokertPhoneNumber,
cast(VC.ExcessiveUmbrellaExpirationDate as VARCHAR(255)) AS ExpirationDate,
3 AS InsuranceType
   FROM VendorCOI VC
   JOIN Company C ON C.CompanyId = VC.CompanyId
   JOIN Branch b ON b.BranchId = VC.BranchId
   JOIN Vendor V ON V.VendorId = VC.VendorId
   JOIN InsuranceCompany IC ON IC.InsuranceCompanyId = VC.ExcessiveUmbrellaInsuranceCompanyId
   WHERE coalesce(VC.ExcessiveUmbrellaExpirationDate,'') <> '' AND VC.ExcessiveUmbrellaInsuranceCompanyId > 0
   AND VC.AccountId = v_AccountId
   UNION ALL
   SELECT
   cast(VC.CertificateOfInsuranceId as VARCHAR(255)),
cast(VC.CompanyId as BIGINT),
cast(C.CompanyName as VARCHAR(255)),
cast(VC.BranchId as VARCHAR(255)),
cast(b.BranchName as VARCHAR(255)),
cast(VC.VendorId as VARCHAR(255)),
cast(V.VendorName as VARCHAR(255)),
cast(VC.WorkersCompensationBroker as VARCHAR(255)),
cast(IC.InsuranceCompanyName as VARCHAR(255)),
cast(VC.WorkersCompensationDefaultEmail as VARCHAR(255)) AS BrokerEmail,
cast(VC.WorkersCompensationDefaultPhoneNumber as VARCHAR(255)) AS BrokertPhoneNumber,
cast(VC.WorkersCompensationExpirationDate as VARCHAR(255)) AS ExpirationDate,
4 AS InsuranceType
   FROM VendorCOI VC
   JOIN Company C ON C.CompanyId = VC.CompanyId
   JOIN Branch b ON b.BranchId = VC.BranchId
   JOIN Vendor V ON V.VendorId = VC.VendorId
   JOIN InsuranceCompany IC ON IC.InsuranceCompanyId = VC.ExcessiveUmbrellaInsuranceCompanyId
   WHERE coalesce(VC.WorkersCompensationExpirationDate,'') <> '' AND VC.ExcessiveUmbrellaInsuranceCompanyId > 0
   AND VC.AccountId = v_AccountId
   UNION ALL
   SELECT
   cast(VC.CertificateOfInsuranceId as VARCHAR(255)),
cast(VC.CompanyId as BIGINT),
cast(C.CompanyName as VARCHAR(255)),
cast(VC.BranchId as VARCHAR(255)),
cast(b.BranchName as VARCHAR(255)),
cast(VC.VendorId as VARCHAR(255)),
cast(V.VendorName as VARCHAR(255)),
cast(VC.OtherBroker as VARCHAR(255)),
cast(IC.InsuranceCompanyName as VARCHAR(255)),
cast(VC.OtherDefaultEmail as VARCHAR(255)) AS BrokerEmail,
cast(VC.OtherDefaultPhoneNumber as VARCHAR(255)) AS BrokertPhoneNumber,
cast(VC.OtherExpirationDate as VARCHAR(255)) AS ExpirationDate,
5 AS InsuranceType
   FROM VendorCOI VC
   JOIN Company C ON C.CompanyId = VC.CompanyId
   JOIN Branch b ON b.BranchId = VC.BranchId
   JOIN Vendor V ON V.VendorId = VC.VendorId
   JOIN InsuranceCompany IC ON IC.InsuranceCompanyId = VC.OtherInsuranceCompanyId
   WHERE coalesce(VC.OtherExpirationDate,'') <> '' AND VC.OtherInsuranceCompanyId > 0
   AND VC.AccountId = v_AccountId)
   SELECT *,
COUNT(*) OVER(PARTITION BY '''') AS TotalRecords
   FROM    CTE
   WHERE  (CTE.BranchId IN(SELECT cast(BranchId as VARCHAR(255)) FROM UserBranchAccess WHERE UserId = v_CurrentUserId)
   OR v_CurrentUserId = -1)
   ORDER BY CASE WHEN (v_SortColumn = 'CompanyName'
   AND v_SortOder = 'ASC') THEN CompanyName
   END ASC,CASE WHEN (v_SortColumn = 'CompanyName'
   AND v_SortOder = 'DESC') THEN CompanyName
   END DESC,CASE WHEN (v_SortColumn = 'BranchName'
   AND v_SortOder = 'ASC') THEN BranchName
   END ASC,CASE WHEN (v_SortColumn = 'BranchName'
   AND v_SortOder = 'DESC') THEN BranchName
   END DESC,CASE WHEN (v_SortColumn = 'VendorName'
   AND v_SortOder = 'ASC') THEN VendorName
   END ASC,CASE WHEN (v_SortColumn = 'VendorName'
   AND v_SortOder = 'DESC') THEN VendorName
   END DESC,CASE WHEN (v_SortColumn = 'InsuranceType'
   AND v_SortOder = 'ASC') THEN InsuranceType
   END ASC,CASE WHEN (v_SortColumn = 'InsuranceType'
   AND v_SortOder = 'DESC') THEN InsuranceType
   END DESC;
END; $$;













