-- Stored procedure definition script USP_LeadConversionReport for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_LeadConversionReport(v_numUserCntID NUMERIC DEFAULT 0,               
v_numDomainID NUMERIC DEFAULT 0,              
v_dtStartDate TIMESTAMP DEFAULT NULL,                                        
v_dtEndDate TIMESTAMP DEFAULT NULL,                     
v_tintType INTEGER DEFAULT NULL,                                       
v_numLeadGroupId INTEGER DEFAULT NULL,
INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numTotalLeadAssigned  NUMERIC;                    
   v_numTotalLeadAccountAssigned  NUMERIC;                    
   v_numTotalLeadOwned  NUMERIC;                    
   v_numTotalLeadAccountOwned  NUMERIC;                    
   v_numTotalLeadAmountAssigned  NUMERIC;                    
   v_numTotalLeadAmountOwned  NUMERIC;  
   v_numTotalLeadAmount  NUMERIC;
BEGIN
   If v_tintType = 1 then

      If v_numLeadGroupId <> 0 then
              
 --Total Leads Assigned                     
                    
         select   count(*) INTO v_numTotalLeadAssigned From DivisionMaster DM Where DM.numDomainID = v_numDomainID
         And DM.numGrpId = v_numLeadGroupId and (tintCRMType = 0 or (tintCRMType = 1 and  DM.bintLeadPromBy is not null) or
(tintCRMType = 2 and  DM.bintLeadPromBy is not null and bintProsPromBy is not null))
         And DM.numAssignedTo in(select numContactId from  AdditionalContactsInformation
            where numTeam in(select numTeam from ForReportsByTeam where numUserCntID = v_numUserCntID and tintType = 34))
         And DM.bintCreatedDate  between v_dtStartDate And v_dtEndDate;           
          
                     
                    
---Total Leads Converted Into Prospects (Of those Leads Assigned)                     
                    
                    
         select   count(*) INTO v_numTotalLeadAccountAssigned From DivisionMaster DM Where DM.bintLeadPromBy is Not Null And DM.tintCRMType = 1
         And DM.numDomainID = v_numDomainID And DM.numGrpId = v_numLeadGroupId
         And DM.numAssignedTo  in(select numContactId from  AdditionalContactsInformation
            where numTeam in(select numTeam from ForReportsByTeam where numUserCntID = v_numUserCntID and tintType = 34))
         And DM.bintCreatedDate between v_dtStartDate And v_dtEndDate;                     
                    
--Total Leads Owned                     
                    
         select   count(*) INTO v_numTotalLeadOwned From DivisionMaster DM Where DM.numDomainID = v_numDomainID and (tintCRMType = 0 or (tintCRMType = 1 and  DM.bintLeadPromBy is not null) or
(tintCRMType = 2 and  DM.bintLeadPromBy is not null and bintProsPromBy is not null))
         And DM.numGrpId = v_numLeadGroupId And DM.numRecOwner
         in(select numContactId from  AdditionalContactsInformation
            where numTeam in(select numTeam from ForReportsByTeam where numUserCntID = v_numUserCntID and tintType = 34))
         And DM.bintCreatedDate  between v_dtStartDate And v_dtEndDate;                     
                    
                    
--Total Leads Converted Into Prospects (Of Leads Owned)                    
                    
         select   count(*) INTO v_numTotalLeadAccountOwned From DivisionMaster DM Where  DM.bintLeadPromBy is not null And DM.tintCRMType = 1
         And DM.numDomainID = v_numDomainID And DM.numGrpId = v_numLeadGroupId
         And DM.numRecOwner  in(select numContactId from  AdditionalContactsInformation
            where numTeam in(select numTeam from ForReportsByTeam where numUserCntID = v_numUserCntID and tintType = 34))
         And DM.bintCreatedDate between v_dtStartDate And v_dtEndDate;                     
                    
--Total Amount Won By Total Lead Assigned                    
                    
         select   coalesce(sum(monPAmount),0) INTO v_numTotalLeadAmountAssigned From OpportunityMaster Where tintoppstatus = 0 And tintopptype = 1 And numDivisionId in(Select numDivisionID
            From DivisionMaster Where  bintLeadPromBy is not null And tintCRMType = 1
            And numDomainID = v_numDomainID And numGrpId = v_numLeadGroupId And numAssignedTo in(select numContactId from  AdditionalContactsInformation
               where numTeam in(select numTeam from ForReportsByTeam where numUserCntID = v_numUserCntID and tintType = 34))
            And bintCreatedDate  between v_dtStartDate And v_dtEndDate);                    
                    
                           
--Total Amount Won by Total Lead Owned                    
                    
         select   coalesce(sum(monPAmount),0) INTO v_numTotalLeadAmountOwned From OpportunityMaster Where tintoppstatus = 0 And tintopptype = 1 And numDivisionId in(Select numDivisionID
            From DivisionMaster Where bintLeadPromBy is not null And  tintCRMType = 1
            And numDomainID = v_numDomainID And numGrpId = v_numLeadGroupId
            And numRecOwner  in(select numContactId from  AdditionalContactsInformation
               where numTeam in(select numTeam from ForReportsByTeam where numUserCntID = v_numUserCntID and tintType = 34))
            And bintCreatedDate between v_dtStartDate And v_dtEndDate);                    
     
  
                         
--Total Amount in Pipeline                 
                    
         select   coalesce(sum(monPAmount),0) INTO v_numTotalLeadAmount From OpportunityMaster Where tintoppstatus = 0 And tintopptype = 1 And numDivisionId in(Select numDivisionID
            From DivisionMaster Where bintLeadPromBy is not null And  tintCRMType = 1
            And numDomainID = v_numDomainID And numGrpId = v_numLeadGroupId
            And (numRecOwner  in(select numContactId from  AdditionalContactsInformation
               where numTeam in(select numTeam from ForReportsByTeam where numUserCntID = v_numUserCntID and tintType = 34))
            Or numAssignedTo in(select numContactId from  AdditionalContactsInformation
               where numTeam in(select numTeam from ForReportsByTeam where numUserCntID = v_numUserCntID and tintType = 34)))
            And bintCreatedDate between v_dtStartDate And v_dtEndDate);
         open SWV_RefCur for
         Select
         v_numTotalLeadAssigned AS TotalLeadAssigned,
v_numTotalLeadOwned AS TotalLeadOwned,
v_numTotalLeadAccountAssigned AS TotalLeadAccountAssigned,
v_numTotalLeadAccountOwned AS TotalLeadAccountOwned,
v_numTotalLeadAmountAssigned AS TotalLeadAmountAssigned,
v_numTotalLeadAmountOwned AS TotalLeadAmountOwned,
v_numTotalLeadAmount AS TotalLeadAmount;
      end if;
      if v_numLeadGroupId = 0 then
              
 --Total Leads Assigned                     
                    
         select   count(*) INTO v_numTotalLeadAssigned From DivisionMaster DM Where DM.numDomainID = v_numDomainID
         And DM.numAssignedTo in(select numContactId from  AdditionalContactsInformation
            where numTeam in(select numTeam from ForReportsByTeam where numUserCntID = v_numUserCntID and tintType = 34))
         and (tintCRMType = 0 or (tintCRMType = 1 and  DM.bintLeadPromBy is not null) or (tintCRMType = 2 and  DM.bintLeadPromBy is not null and bintProsPromBy is not null))
         And DM.bintCreatedDate  between v_dtStartDate And v_dtEndDate;                      
                    
---Total Leads Converted Into Prospects (Of those Leads Assigned)                     
                    
                    
         select   count(*) INTO v_numTotalLeadAccountAssigned From DivisionMaster DM Where DM.bintLeadPromBy is not null And DM.tintCRMType = 1 And
         DM.numDomainID = v_numDomainID And DM.numAssignedTo in(select numContactId from  AdditionalContactsInformation
            where numTeam in(select numTeam from ForReportsByTeam where numUserCntID = v_numUserCntID and tintType = 34))
         And DM.bintCreatedDate  between v_dtStartDate And v_dtEndDate;                     
                    
--Total Leads Owned                     
                    
         select   count(*) INTO v_numTotalLeadOwned From DivisionMaster DM Where DM.numDomainID = v_numDomainID  and (tintCRMType = 0 or (tintCRMType = 1 and  DM.bintLeadPromBy is not null) or (tintCRMType = 2 and  DM.bintLeadPromBy is not null and bintProsPromBy is not null))
         And DM.numRecOwner in(select numContactId from  AdditionalContactsInformation
            where numTeam in(select numTeam from ForReportsByTeam where numUserCntID = v_numUserCntID and tintType = 34))
         And DM.bintCreatedDate  between v_dtStartDate And v_dtEndDate;                     
                    
                    
--Total Leads Converted Into Prospect (Of Leads Owned)                    
                    
         select   count(*) INTO v_numTotalLeadAccountOwned From DivisionMaster DM Where DM.bintLeadPromBy is not null And DM.tintCRMType = 1 And DM.numDomainID = v_numDomainID
         And DM.numRecOwner in(select numContactId from  AdditionalContactsInformation
            where numTeam in(select numTeam from ForReportsByTeam where numUserCntID = v_numUserCntID and tintType = 34))
         And DM.bintCreatedDate between v_dtStartDate And v_dtEndDate;                     
                    
--Total Amount Won By Total Lead Assigned                    
                    
         select   coalesce(sum(monPAmount),0) INTO v_numTotalLeadAmountAssigned From OpportunityMaster Where tintoppstatus = 0 And tintopptype = 1 And numDivisionId in(Select numDivisionID
            From DivisionMaster Where bintLeadPromBy Is Not Null And tintCRMType = 1
            AND numDomainID = v_numDomainID And numAssignedTo in(select numContactId from  AdditionalContactsInformation
               where numTeam in(select numTeam from ForReportsByTeam where numUserCntID = v_numUserCntID and tintType = 34))
            And bintCreatedDate Between v_dtStartDate And v_dtEndDate);                    
                    
                    
                    
--Total Amount Won by Total Lead Owned                    
                    
         select   coalesce(sum(monPAmount),0) INTO v_numTotalLeadAmountOwned From OpportunityMaster Where tintoppstatus = 0 And tintopptype = 1 And numDivisionId in(Select numDivisionID
            From DivisionMaster Where bintLeadPromBy is not null And tintCRMType = 1
            And numDomainID = v_numDomainID And numRecOwner in(select numContactId from  AdditionalContactsInformation
               where numTeam in(select numTeam from ForReportsByTeam where numUserCntID = v_numUserCntID and tintType = 34))
            And bintCreatedDate between v_dtStartDate And v_dtEndDate);   
  
--  Total Amount in Pipeline   
  
         select   coalesce(sum(monPAmount),0) INTO v_numTotalLeadAmount From OpportunityMaster Where tintoppstatus = 0 And tintopptype = 1 And numDivisionId in(Select numDivisionID
            From DivisionMaster Where bintLeadPromBy is not null And tintCRMType = 1
            And numDomainID = v_numDomainID And (numRecOwner in(select numContactId from  AdditionalContactsInformation
               where numTeam in(select numTeam from ForReportsByTeam where numUserCntID = v_numUserCntID and tintType = 34))  or numAssignedTo in(select numContactId from  AdditionalContactsInformation
               where numTeam in(select numTeam from ForReportsByTeam where numUserCntID = v_numUserCntID and tintType = 34)))
            And bintCreatedDate between v_dtStartDate And v_dtEndDate);
         open SWV_RefCur for
         Select
         v_numTotalLeadAssigned AS TotalLeadAssigned,
v_numTotalLeadOwned AS TotalLeadOwned,
v_numTotalLeadAccountAssigned AS TotalLeadAccountAssigned,
v_numTotalLeadAccountOwned AS TotalLeadAccountOwned,
v_numTotalLeadAmountAssigned AS TotalLeadAmountAssigned,
v_numTotalLeadAmountOwned AS TotalLeadAmountOwned,
v_numTotalLeadAmount AS TotalLeadAmount;
      end if;
   end if;          
          
   If v_tintType = 2 then

      if v_numLeadGroupId <> 0 then
              
 --Total Leads Assigned                     
                    
         select   count(*) INTO v_numTotalLeadAssigned From DivisionMaster DM Where DM.numDomainID = v_numDomainID And DM.numGrpId = v_numLeadGroupId
         And (tintCRMType = 0 or (tintCRMType = 1 and  DM.bintLeadPromBy is not null) or (tintCRMType = 2 and  DM.bintLeadPromBy is not null and bintProsPromBy is not null))
         And DM.numAssignedTo in(select numContactId from  AdditionalContactsInformation
            where numTeam in(select numTeam from ForReportsByTeam where numUserCntID = v_numUserCntID and tintType = 34))
         And DM.bintCreatedDate  Between v_dtStartDate And v_dtEndDate;           
          
                     
                    
---Total Leads Converted Into Accounts (Of those Leads Assigned)                     
                    
                    
         select   count(*) INTO v_numTotalLeadAccountAssigned From DivisionMaster DM Where DM.bintLeadPromBy is Not Null AND DM.tintCRMType = 2 And DM.numDomainID = v_numDomainID
         And DM.numGrpId = v_numLeadGroupId AND DM.numAssignedTo in(select numContactId from  AdditionalContactsInformation
            where numTeam in(select numTeam from ForReportsByTeam where numUserCntID = v_numUserCntID and tintType = 34))
         And DM.bintCreatedDate Between v_dtStartDate And v_dtEndDate;                     
                    
--Total Leads Owned                     
                    
         select   count(*) INTO v_numTotalLeadOwned From DivisionMaster DM Where DM.numDomainID = v_numDomainID and (tintCRMType = 0 or (tintCRMType = 1 and  DM.bintLeadPromBy is not null) or (tintCRMType = 2 and  DM.bintLeadPromBy is not null and bintProsPromBy is not null))
         And DM.numGrpId = v_numLeadGroupId And DM.numRecOwner in(select numContactId from  AdditionalContactsInformation
            where numTeam in(select numTeam from ForReportsByTeam where numUserCntID = v_numUserCntID and tintType = 34))
         And DM.bintCreatedDate  between v_dtStartDate And v_dtEndDate;                     
                    
                    
--Total Leads Converted Into Accounts (Of Leads Owned)             
                    
         select   count(*) INTO v_numTotalLeadAccountOwned From DivisionMaster DM Where DM.bintLeadPromBy is not null And DM.tintCRMType = 2 And DM.numDomainID = v_numDomainID
         And DM.numGrpId = v_numLeadGroupId And DM.numRecOwner in(select numContactId from  AdditionalContactsInformation
            where numTeam in(select numTeam from ForReportsByTeam where numUserCntID = v_numUserCntID and tintType = 34))
         And DM.bintCreatedDate between v_dtStartDate And v_dtEndDate;                     
                    
--Total Amount Won By Total Lead Assigned                    
                    
         select   coalesce(sum(monPAmount),0) INTO v_numTotalLeadAmountAssigned From OpportunityMaster Where tintoppstatus = 1 And tintopptype = 1 And numDivisionId in(Select numDivisionID
            From DivisionMaster Where bintLeadPromBy is not null And tintCRMType = 2
            And numDomainID = v_numDomainID And numGrpId = v_numLeadGroupId
            And numAssignedTo in(select numContactId from  AdditionalContactsInformation
               where numTeam in(select numTeam from ForReportsByTeam where numUserCntID = v_numUserCntID and tintType = 34))
            And bintCreatedDate  between v_dtStartDate And v_dtEndDate);                    
                    
                           
--Total Amount Won by Total Lead Owned                    
                    
         select   coalesce(sum(monPAmount),0) INTO v_numTotalLeadAmountOwned From OpportunityMaster Where tintoppstatus = 1 And tintopptype = 1 And numDivisionId in(Select numDivisionID
            From DivisionMaster Where bintLeadPromBy is not null And tintCRMType = 2
            And numDomainID = v_numDomainID And numGrpId = v_numLeadGroupId
            And numRecOwner  in(select numContactId from  AdditionalContactsInformation
               where numTeam in(select numTeam from ForReportsByTeam where numUserCntID = v_numUserCntID and tintType = 34))
            And bintCreatedDate between v_dtStartDate And v_dtEndDate);                    
            
                 
--Total Amount Won                     
                    
         select   coalesce(sum(monPAmount),0) INTO v_numTotalLeadAmount From OpportunityMaster Where tintoppstatus = 1 And tintopptype = 1 And numDivisionId in(Select numDivisionID
            From DivisionMaster Where bintLeadPromBy is not null And tintCRMType = 2
            And numDomainID = v_numDomainID And numGrpId = v_numLeadGroupId
            And (numRecOwner in(select numContactId from  AdditionalContactsInformation
               where numTeam in(select numTeam from ForReportsByTeam where numUserCntID = v_numUserCntID and tintType = 34))
            Or numAssignedTo in(select numContactId from  AdditionalContactsInformation
               where numTeam in(select numTeam from ForReportsByTeam where numUserCntID = v_numUserCntID and tintType = 34)))
            And bintCreatedDate between v_dtStartDate And v_dtEndDate);
         open SWV_RefCur for
         Select
         v_numTotalLeadAssigned AS TotalLeadAssigned,
v_numTotalLeadOwned AS TotalLeadOwned,
v_numTotalLeadAccountAssigned AS TotalLeadAccountAssigned,
v_numTotalLeadAccountOwned AS TotalLeadAccountOwned,
v_numTotalLeadAmountAssigned AS TotalLeadAmountAssigned,
v_numTotalLeadAmountOwned AS TotalLeadAmountOwned,
v_numTotalLeadAmount AS TotalLeadAmount;
      end if;
      if v_numLeadGroupId = 0 then
              
 --Total Leads Assigned                     
                    
         select   count(*) INTO v_numTotalLeadAssigned From DivisionMaster DM Where DM.numDomainID = v_numDomainID And DM.numAssignedTo in(select numContactId from  AdditionalContactsInformation
            where numTeam in(select numTeam from ForReportsByTeam where numUserCntID = v_numUserCntID and tintType = 34))
         And (tintCRMType = 0 or (tintCRMType = 1 and  DM.bintLeadPromBy is not null) or (tintCRMType = 2 and  DM.bintLeadPromBy is not null and bintProsPromBy is not null))
         And DM.bintCreatedDate  between v_dtStartDate And v_dtEndDate;                      
                    
---Total Leads Converted Into Accounts (Of those Leads Assigned)                     
                    
                    
         select   count(*) INTO v_numTotalLeadAccountAssigned From DivisionMaster DM Where DM.bintLeadPromBy is not null And DM.tintCRMType = 2
         And DM.numDomainID = v_numDomainID And DM.numAssignedTo in(select numContactId from  AdditionalContactsInformation
            where numTeam in(select numTeam from ForReportsByTeam where numUserCntID = v_numUserCntID and tintType = 34))
         And DM.bintCreatedDate  Between v_dtStartDate And v_dtEndDate;                     
                    
--Total Leads Owned                     
                    
         select   count(*) INTO v_numTotalLeadOwned From DivisionMaster DM Where DM.numDomainID = v_numDomainID And DM.numRecOwner in(select numContactId from  AdditionalContactsInformation
            where numTeam in(select numTeam from ForReportsByTeam where numUserCntID = v_numUserCntID and tintType = 34))
         And (tintCRMType = 0 or (tintCRMType = 1 and  DM.bintLeadPromBy is not null) or (tintCRMType = 2 and  DM.bintLeadPromBy is not null and bintProsPromBy is not null))
         And DM.bintCreatedDate  between v_dtStartDate And v_dtEndDate;                     
                    
                    
--Total Leads Converted Into Accounts (Of Leads Owned)                    
                    
         select   count(*) INTO v_numTotalLeadAccountOwned From DivisionMaster DM Where DM.numDomainID = v_numDomainID And DM.bintLeadPromBy is not null And DM.tintCRMType = 2
         And DM.numRecOwner in(select numContactId from  AdditionalContactsInformation
            where numTeam in(select numTeam from ForReportsByTeam where numUserCntID = v_numUserCntID and tintType = 34))
         And DM.bintCreatedDate between v_dtStartDate And v_dtEndDate;                     
                    
--Total Amount Won By Total Lead Assigned                    
                    
         select   coalesce(sum(monPAmount),0) INTO v_numTotalLeadAmountAssigned From OpportunityMaster Where numDivisionId in(Select numDivisionID From DivisionMaster
            Where numDomainID = v_numDomainID And numAssignedTo in(select numContactId from  AdditionalContactsInformation
               where numTeam in(select numTeam from ForReportsByTeam where numUserCntID = v_numUserCntID and tintType = 34))
            And bintLeadPromBy is not null And tintCRMType = 2
            And bintCreatedDate between v_dtStartDate And v_dtEndDate);                    
                    
                    
                    
--Total Amount Won by Total Lead Owned                    
                    
         select   coalesce(sum(monPAmount),0) INTO v_numTotalLeadAmountOwned From OpportunityMaster Where numDivisionId in(Select numDivisionID From DivisionMaster
            Where numDomainID = v_numDomainID And numRecOwner  in(select numContactId from  AdditionalContactsInformation
               where numTeam in(select numTeam from ForReportsByTeam where numUserCntID = v_numUserCntID and tintType = 34))
            And bintLeadPromBy is Not Null And tintCRMType = 2
            And bintCreatedDate between v_dtStartDate And v_dtEndDate);       
  
  
--  Total Amount Won   
  
  
         select   coalesce(sum(monPAmount),0) INTO v_numTotalLeadAmount From OpportunityMaster Where numDivisionId in(Select numDivisionID From DivisionMaster
            Where numDomainID = v_numDomainID And (numRecOwner  in(select numContactId from  AdditionalContactsInformation
               where numTeam in(select numTeam from ForReportsByTeam where numUserCntID = v_numUserCntID and tintType = 34))
            Or numAssignedTo in(select numContactId from  AdditionalContactsInformation
               where numTeam in(select numTeam from ForReportsByTeam where numUserCntID = v_numUserCntID and tintType = 34)))
            And bintLeadPromBy is Not Null And tintCRMType = 2
            And bintCreatedDate between v_dtStartDate And v_dtEndDate);
         open SWV_RefCur for
         Select
         v_numTotalLeadAssigned AS TotalLeadAssigned,
v_numTotalLeadOwned AS TotalLeadOwned,
v_numTotalLeadAccountAssigned AS TotalLeadAccountAssigned,
v_numTotalLeadAccountOwned AS TotalLeadAccountOwned,
v_numTotalLeadAmountAssigned AS TotalLeadAmountAssigned,
v_numTotalLeadAmountOwned AS TotalLeadAmountOwned,
v_numTotalLeadAmount AS TotalLeadAmount;
      end if;
   end if;
   RETURN;
END; $$;


