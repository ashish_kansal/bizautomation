-- Stored procedure definition script USP_GetTeamsWithCapacityLoadForProject for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetTeamsWithCapacityLoadForProject(v_numDomainID NUMERIC(18,0)
	,v_numProId NUMERIC(18,0)
	,v_tintDateRange SMALLINT -- 1:Today, 2:Week, 3:Month
	,v_dtFromDate TIMESTAMP
	,v_ClientTimeZoneOffset INTEGER,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT
   cast(LD.numListItemID as VARCHAR(255)) AS numTeamID
		,cast(coalesce(vcData,'') as VARCHAR(255)) as vcTeamName
		,GetProjectCapacityLoad(v_numDomainID,0,LD.numListItemID,v_numProId,0,v_tintDateRange,v_dtFromDate,
   v_ClientTimeZoneOffset) AS numCapacityLoad
   FROM
   Listdetails LD
   WHERE
   LD.numDomainid = v_numDomainID
   AND LD.numListID = 35;
END; $$;















