-- Stored procedure definition script USP_ManageFormFieldGroupConfigurarion for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ManageFormFieldGroupConfigurarion(
v_numDomainId NUMERIC DEFAULT 0,  
v_numFormId NUMERIC DEFAULT 0,                        
v_numGroupId NUMERIC DEFAULT 0,                                    
v_numFormFieldGroupId NUMERIC DEFAULT 0,
v_tintActionType SMALLINT   DEFAULT 0,
v_vcGroupName VARCHAR(500) DEFAULT '',               
INOUT v_vcOutput SMALLINT  DEFAULT NULL
,INOUT SWV_RefCur refcursor default null
)
LANGUAGE plpgsql
   AS $$
BEGIN
   IF(v_tintActionType = 1) then
	
      IF((SELECT COUNT(*) FROM FormFieldGroupConfigurarion where numFormId = v_numFormId AND numDomainID = v_numDomainId AND vcGroupName = v_vcGroupName) > 0) then
		
         v_vcOutput := 2;
      ELSE
         INSERT INTO FormFieldGroupConfigurarion(numFormId,
				numGroupId,
				vcGroupName,
				numDomainID)VALUES(v_numFormId,
				0,
				v_vcGroupName,
				v_numDomainId);
			
         v_vcOutput := 1;
      end if;
   end if;
   IF(v_tintActionType = 2) then
	
      IF((SELECT COUNT(*) FROM FormFieldGroupConfigurarion where numFormId = v_numFormId AND numDomainID = v_numDomainId AND vcGroupName = v_vcGroupName AND numFormFieldGroupId <> v_numFormFieldGroupId) > 0) then
		
         v_vcOutput := 2;
      ELSE
         UPDATE
         FormFieldGroupConfigurarion
         SET
         vcGroupName = v_vcGroupName
         WHERE
         numFormFieldGroupId = v_numFormFieldGroupId;
         v_vcOutput := 3;
      end if;
   end if;
   IF(v_tintActionType = 3) then
	
      DELETE FROM BizFormWizardMasterConfiguration WHERE coalesce(numFormFieldGroupId,0) = v_numFormFieldGroupId;
      DELETE FROM FormFieldGroupConfigurarion WHERE numFormFieldGroupId = v_numFormFieldGroupId;
      v_vcOutput := 4;
   end if;
   IF(v_tintActionType = 4) then
	
      open SWV_RefCur for
      SELECT
      vcGroupName AS "vcGroupName",
			numFormFieldGroupId AS "numFormFieldGroupId"
      FROM
      FormFieldGroupConfigurarion
      WHERE
      numFormId = v_numFormId AND numDomainID = v_numDomainId
      ORDER BY
      numOrder;
   end if;
   IF(v_tintActionType = 5) then
	
      UPDATE FormFieldGroupConfigurarion AS ST SET numOrder = "Row#" FROM(SELECT Items,ROW_NUMBER()  OVER(ORDER BY(SELECT 1)) AS "Row#" FROM Split(v_vcGroupName,',')) AS T
      WHERE ST.numFormFieldGroupId = T.Items AND(ST.numDomainID = v_numDomainId AND numFormId = v_numFormId AND ST.numFormFieldGroupId = T.Items);
   end if;
   RETURN;
END; $$;



