-- Stored procedure definition script USP_OrganizationRatingRule_Save for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_OrganizationRatingRule_Save(v_numDomainID NUMERIC(18,0),
	v_numUserCntID NUMERIC(18,0),
	v_vcRules TEXT)
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE

   v_i  INTEGER DEFAULT 1;
   v_COUNT  INTEGER;
   v_numORRID  NUMERIC(18,0);
   v_tintPerformance  SMALLINT;
   v_numFromAmount  NUMERIC(18,0);
   v_numToAmount  NUMERIC(18,0);
   v_numOrganizationRatingID  NUMERIC(18,0);
  my_ex_state TEXT;
	my_ex_message TEXT;
	my_ex_detail TEXT;
	my_ex_hint TEXT;
	my_ex_ctx TEXT;
BEGIN
   BEGIN
      -- BEGIN TRANSACTION

      BEGIN
         CREATE TEMP SEQUENCE tt_TEMP_seq INCREMENT BY 1 START WITH 1;
         EXCEPTION WHEN OTHERS THEN
            NULL;
      END;
      DROP TABLE IF EXISTS tt_TEMP CASCADE;
      CREATE TEMPORARY TABLE tt_TEMP
      (
         ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1),
         tintPerformance SMALLINT,
         numFromAmount NUMERIC(18,0),
         numToAmount NUMERIC(18,0),
         numOrganizationRatingID NUMERIC(18,0)
      );
      INSERT INTO tt_TEMP(tintPerformance
		,numFromAmount
		,numToAmount
		,numOrganizationRatingID)
      SELECT
      tintPerformance
		,numFromAmount
		,numToAmount
		,numOrganizationRatingID
      FROM
	   XMLTABLE
		(
			'NewDataSet/Table'
			PASSING 
				CAST(v_vcRules AS XML)
			COLUMNS
				id FOR ORDINALITY,
				tintPerformance SMALLINT PATH 'tintPerformance',
				numFromAmount NUMERIC(18,0) PATH 'numFromAmount',
				numToAmount NUMERIC(18,0) PATH 'numToAmount',
				numOrganizationRatingID NUMERIC(18,0) PATH 'numOrganizationRatingID'
		) AS X;

      IF(SELECT COUNT(*) FROM(SELECT DISTINCT tintPerformance FROM tt_TEMP) T1) > 1 then
	
         RAISE EXCEPTION 'DIFFERENT_PERFORMANCE_RATING';
      end if;
      DELETE FROM OrganizationRatingRule WHERE numDomainID = v_numDomainID;
      select   COUNT(*) INTO v_COUNT FROM tt_TEMP;
      WHILE v_i <= v_COUNT LOOP
         select   tintPerformance, numFromAmount, numToAmount, numOrganizationRatingID INTO v_tintPerformance,v_numFromAmount,v_numToAmount,v_numOrganizationRatingID FROM
         tt_TEMP WHERE
         ID = v_i;
         IF EXISTS(SELECT * FROM OrganizationRatingRule WHERE numDomainID = v_numDomainID AND (v_numFromAmount BETWEEN numFromAmount AND numToAmount OR v_numToAmount BETWEEN numFromAmount AND numToAmount)) then
		
            RAISE EXCEPTION 'RULE_EXISTS_WITH_NUMBER';
         end if;

		-- INSERT NEW RULES
         INSERT INTO OrganizationRatingRule(numDomainID
			,tintPerformance
			,numFromAmount
			,numToAmount
			,numOrganizationRatingID
			,numCreatedBy
			,dtCreated)
		VALUES(v_numDomainID
			,v_tintPerformance
			,v_numFromAmount
			,v_numToAmount
			,v_numOrganizationRatingID
			,v_numUserCntID
			,TIMEZONE('UTC',now()));
		
         v_i := v_i::bigint+1;
      END LOOP;
      IF(SELECT COUNT(*) FROM OrganizationRatingRule WHERE numDomainID = v_numDomainID) > 0 then
	
         SELECT DISTINCT tintPerformance INTO v_tintPerformance FROM OrganizationRatingRule WHERE numDomainID = v_numDomainID;
         PERFORM USP_CompanyInfo_UpdateRating(v_numDomainID,0,v_tintPerformance::SMALLINT);
         UPDATE DycFormField_Mapping SET bitAllowEdit = false,bitInlineEdit = false WHERE numFieldID IN(SELECT numFieldID FROM DycFieldMaster WHERE vcDbColumnName = 'numCompanyRating') AND numFormID IN(34,35,36);
      ELSE
         UPDATE DycFormField_Mapping SET bitAllowEdit = true,bitInlineEdit = true WHERE numFieldID IN(SELECT numFieldID FROM DycFieldMaster WHERE vcDbColumnName = 'numCompanyRating') AND numFormID IN(34,35,36);
      end if;
      -- COMMIT
EXCEPTION WHEN OTHERS THEN
         -- ROLLBACK TRANSACTION
  GET STACKED DIAGNOSTICS
			  my_ex_state   = RETURNED_SQLSTATE,
			  my_ex_message = MESSAGE_TEXT,
			  my_ex_detail  = PG_EXCEPTION_DETAIL,
			  my_ex_hint    = PG_EXCEPTION_HINT,
			  my_ex_ctx     = PG_EXCEPTION_CONTEXT;

			 raise exception '% % % % %', my_ex_state, my_ex_message, my_ex_detail, my_ex_hint, my_ex_ctx;
   END;
   RETURN;
END; $$;

