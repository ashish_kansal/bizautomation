-- Stored procedure definition script USP_WarehouseItems_GetByItemAndExternalLocation for PostgreSQL
CREATE OR REPLACE FUNCTION USP_WarehouseItems_GetByItemAndExternalLocation(v_numDomainID NUMERIC(18,0),
v_numWareHouseID NUMERIC(18,0),
v_numItemCode NUMERIC(18,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT
		numWareHouseID AS "numWareHouseID",
		numWareHouseItemID as "numWareHouseItemID"
   FROM
   WareHouseItems
   WHERE
   numDomainID = v_numDomainID
   AND numItemID = v_numItemCode
   AND numWareHouseID = v_numWareHouseID
   ORDER BY
   numWareHouseItemID;
END; $$;











