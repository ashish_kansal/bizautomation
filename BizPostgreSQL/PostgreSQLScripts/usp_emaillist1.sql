-- Stored procedure definition script USP_EmailList1 for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_EmailList1(v_numUserCntID NUMERIC,                                     
v_tintSortOrder NUMERIC DEFAULT 4,                                    
v_dtFromDate TIMESTAMP DEFAULT NULL,                                   
v_dtToDate TIMESTAMP DEFAULT NULL,                                  
v_MessageFrom VARCHAR(100) DEFAULT '',                                  
v_SeachKeyword VARCHAR(100) DEFAULT '',                                    
v_CurrentPage INTEGER DEFAULT NULL,                                  
v_PageSize INTEGER DEFAULT NULL,                                  
INOUT v_TotRecs INTEGER  DEFAULT NULL,                                  
v_columnName VARCHAR(50) DEFAULT NULL,                                  
v_columnSortOrder VARCHAR(10) DEFAULT NULL,                                  
v_numContactID NUMERIC(9,0) DEFAULT NULL, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql                                  
                                                     
--Create a Temporary table to hold data                                  
   AS $$
   DECLARE
   v_strSql  VARCHAR(5000);                                  
                                  
   v_intCount  INTEGER;      
    
   v_firstRec  INTEGER;                                  
   v_lastRec  INTEGER;
   v_SelColumn  VARCHAR(20);
BEGIN
   BEGIN
      CREATE TEMP SEQUENCE tt_tempTable_seq;
      EXCEPTION WHEN OTHERS THEN
         NULL;
   END;
   drop table IF EXISTS tt_TEMPTABLE CASCADE;
   Create TEMPORARY TABLE tt_TEMPTABLE 
   ( 
      ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1) PRIMARY KEY,
      numEmailHstrID VARCHAR(15)
   );                                  
                                  
                                   
                                  
                                  
   if exists(select * from UserMaster where numUserDetailId = v_numContactID and bitactivateflag = true) then

      select   Count(*) INTO v_intCount from emailusers where numContactID = v_numContactID and numUserCntID = v_numUserCntID;
   else 
      v_intCount := 1;
   end if;    
    
                             
                                
   if v_MessageFrom <> '' then

      if v_intCount > 0 then
         v_SelColumn := ',' || coalesce(v_columnName,'');
         if v_columnName = 'numEmailHstrID' then 
            v_SelColumn := '';
         end if;
         v_strSql := 'select distinct(X.numEmailHstrID) from (select HDR.numEmailHstrID' || coalesce(v_SelColumn,'') || ' from EmailHistory HDR        
  left join EmailHStrToBCCAndCC DTL        
  on DTL.numEmailHstrID=HDR.numEmailHstrID                         
  where (bintCreatedOn between ''' || SUBSTR(CAST(v_dtFromDate AS VARCHAR(20)),1,20) || ''' and ''' || SUBSTR(CAST(v_dtToDate AS VARCHAR(20)),1,20) || ''') and                                  
 (vcFromEmail = ''' || coalesce(v_MessageFrom,'') || ''' or vcEmail = ''' || coalesce(v_MessageFrom,'') || ''')';
         if v_SeachKeyword <> '' then
 
            if v_tintSortOrder = 0 then  
               v_strSql := coalesce(v_strSql,'') || '                                   
   and (vcFromEmail like ''%' || coalesce(v_SeachKeyword,'') || '%''                                   
   or vcEmail like ''%' || coalesce(v_SeachKeyword,'') || '%''                                  
   or vcBody like ''%' || coalesce(v_SeachKeyword,'') || '%''                                  
   or vcSubject like ''%' || coalesce(v_SeachKeyword,'') || '%'')';
            ELSEIF v_tintSortOrder = 2
            then  
               v_strSql := coalesce(v_strSql,'') || ' and vcFromEmail like ''%' || coalesce(v_SeachKeyword,'') || '%''';
            ELSEIF v_tintSortOrder = 3
            then  
               v_strSql := coalesce(v_strSql,'') || ' and tintType=1 and vcEmail like ''%' || coalesce(v_SeachKeyword,'') || '%''';
            ELSEIF v_tintSortOrder = 4
            then  
               v_strSql := coalesce(v_strSql,'') || ' and vcSubject like ''%' || coalesce(v_SeachKeyword,'') || '%''';
            ELSEIF v_tintSortOrder = 5
            then  
               v_strSql := coalesce(v_strSql,'') || ' and vcBody like ''%' || coalesce(v_SeachKeyword,'') || '%''';
            end if;
         end if;
         v_strSql := coalesce(v_strSql,'') || ')X ORDER BY X.' || coalesce(v_columnName,'') || ' ' || coalesce(v_columnSortOrder,'');
         RAISE NOTICE '%',v_strSql;
         EXECUTE 'insert into tt_TEMPTABLE (numEmailHstrID)         
 ' || v_strSql;
      end if;
   end if;                  
                                
   v_firstRec :=(v_CurrentPage::bigint -1)*v_PageSize::bigint;                                  
   v_lastRec :=(v_CurrentPage::bigint*v_PageSize::bigint+1);                                  
   open SWV_RefCur for
   select E.numEmailHstrID,vcFromEmail, GetEmaillAdd(E.numEmailHstrID,1::SMALLINT) as vcMessageTo,VcSubject, bintCreatedOn,bintCreatedOn as CreatedOn,coalesce(CAST(numNoofTimes AS VARCHAR(15)),'-') as numNoofTimes from tt_TEMPTABLE T
   JOIN EmailHistory E
   ON E.numEmailHstrID = T.numEmailHstrID
   where ID > v_firstRec and ID < v_lastRec;                                  
   select count(*) INTO v_TotRecs from tt_TEMPTABLE;                                  
   RETURN;
END; $$;


