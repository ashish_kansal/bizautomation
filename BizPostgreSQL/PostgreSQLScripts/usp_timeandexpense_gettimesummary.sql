DROP FUNCTION IF EXISTS USP_TimeAndExpense_GetTimeSummary;

CREATE OR REPLACE FUNCTION USP_TimeAndExpense_GetTimeSummary(v_numDomainID NUMERIC(18,0)
	,v_dtFromDate DATE
	,v_dtToDate DATE
	,v_vcEmployeeName VARCHAR(200)
	,v_vcTeams TEXT
	,v_tintPayrollType SMALLINT
	,v_ClientTimeZoneOffset INTEGER,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numTotalTime  NUMERIC(18,0);
   v_numTotalBillableTime  NUMERIC(18,0);
   v_numTotalNonBillableTime  NUMERIC(18,0);
BEGIN
   DROP TABLE IF EXISTS tt_TEMPGetTimeSummary CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPGetTimeSummary
   (
      numType SMALLINT,
      numMinutes INTEGER
   );
   INSERT INTO tt_TEMPGetTimeSummary(numType
		,numMinutes)
   SELECT
   TEMPTotalTime.numType
		,TEMPTotalTime.numMinutes
   FROM
   UserMaster
   INNER JOIN
   AdditionalContactsInformation
   ON
   UserMaster.numUserDetailId = AdditionalContactsInformation.numContactId
   LEFT JOIN LATERAL(SELECT * FROM
      fn_GetPayrollEmployeeTime(v_numDomainID,CAST(UserMaster.numUserDetailId AS INTEGER),v_dtFromDate,v_dtToDate,v_ClientTimeZoneOffset)) TEMPTotalTime on TRUE
   WHERE
   UserMaster.numDomainID = v_numDomainID
   AND coalesce(UserMaster.bitactivateflag,false) = true
   AND (coalesce(v_vcEmployeeName,'') = '' OR AdditionalContactsInformation.vcFirstName ilike CONCAT('%',v_vcEmployeeName,'%') OR AdditionalContactsInformation.vcLastname ilike CONCAT('%',v_vcEmployeeName,'%'))
   AND (coalesce(v_vcTeams,'') = '' OR AdditionalContactsInformation.numTeam IN(SELECT Id FROM SplitIDs(v_vcTeams,',')))
   AND 1 =(CASE WHEN v_tintPayrollType = 0 THEN 1 ELSE(CASE WHEN coalesce(UserMaster.tintPayrollType,0) = v_tintPayrollType THEN 1 ELSE 0 END) END);


   v_numTotalTime := coalesce((SELECT SUM(numMinutes) FROM tt_TEMPGetTimeSummary WHERE numType NOT IN(4)),
   0);
   v_numTotalBillableTime := coalesce((SELECT SUM(numMinutes) FROM tt_TEMPGetTimeSummary WHERE numType = 1),0);
   v_numTotalNonBillableTime := coalesce((SELECT SUM(numMinutes) FROM tt_TEMPGetTimeSummary WHERE  numType NOT IN(1,4)),
   0);

   open SWV_RefCur for SELECT
   cast(CONCAT(TO_CHAR(v_numTotalTime/60,'FM999999999900'),':',TO_CHAR(MOD(v_numTotalTime,60),'FM999999999900')) as VARCHAR(255)) AS vcTotalTime
		,cast(CONCAT(TO_CHAR(v_numTotalBillableTime/60,'FM999999999900'),':',TO_CHAR(MOD(v_numTotalBillableTime,60),'FM999999999900')) as VARCHAR(255)) AS vcTotalBillableTime
		,cast(CONCAT(TO_CHAR(v_numTotalNonBillableTime/60,'FM999999999900'),':',TO_CHAR(MOD(v_numTotalNonBillableTime,60),'FM999999999900')) as VARCHAR(255)) AS vcTotalNonBillableTime;
END; $$;












