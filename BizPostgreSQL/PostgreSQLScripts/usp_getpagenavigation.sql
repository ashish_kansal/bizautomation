CREATE OR REPLACE FUNCTION USP_GetPageNavigation(v_numModuleID NUMERIC(9,0),      
v_numDomainID NUMERIC(9,0),
v_numGroupID NUMERIC(18,0), INOUT SWV_RefCur refcursor)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numParentID  NUMERIC(18,0);
BEGIN
   if v_numModuleID = 14 then

      open SWV_RefCur for
      select  PND.numPageNavID,
        numModuleID,
        numParentID,
        vcPageNavName,
        coalesce(vcNavURL,'') as vcNavURL,
        vcImageURL,
        0 AS intSortOrder
      from    PageNavigationDTL PND
      JOIN TreeNavigationAuthorization TNA ON PND.numPageNavID = TNA.numPageNavID  --AND PND.numTabID = TNA.numTabID       
      WHERE  1 = 1
      AND  coalesce(TNA.bitVisible,false) = true
              --OR ISNULL(numParentID, 0) = 0
              --OR ISNULL(TNA.bitVisible, 0) = 0
            
      AND coalesce(PND.bitVisible,false) = true
      AND numModuleID = 14
      AND TNA.numDomainID = v_numDomainID
      AND numGroupID = v_numGroupID
      union
      select  1111 AS numPageNavID,
        14 AS numModuleID,
        133 AS numParentID,
        'Regular Documents' as vcPageNavName,
        '../Documents/frmRegularDocList.aspx?Category=0',
        '../images/tf_note.gif' AS vcImageURL,
        1
      union
      SELECT  Ld.numListItemID,
        14 AS numModuleID,
        1111 AS numParentID,
        vcData AS vcPageNavName,
        '../Documents/frmRegularDocList.aspx?Category='
      || cast(Ld.numListItemID as VARCHAR(10)) AS vcImageURL,
        '../images/tf_note.gif',
        coalesce(intSortOrder,Ld.sintOrder) AS SortOrder
      FROM    Listdetails Ld
      left join listorder LO on Ld.numListItemID = LO.numListItemID
      and LO.numDomainId = v_numDomainID
      WHERE   Ld.numListID = 29
      and (constFlag = true
      or Ld.numDomainid = v_numDomainID)
      union
      SELECT  Ld.numListItemID AS numPageNavID,
        14 AS numModuleID,
        133 AS numParentID,
        vcData AS vcPageNavName,
        '../Documents/frmDocList.aspx?Category='
      || CAST(Ld.numListItemID AS VARCHAR(10)) AS vcNavURL,
        '../images/tf_note.gif' AS vcImageURL,
        -3
      FROM    Listdetails Ld
      left join listorder LO on Ld.numListItemID = LO.numListItemID
      and LO.numDomainId = v_numDomainID
      inner join AuthoritativeBizDocs AB on (Ld.numListItemID = AB.numAuthoritativeSales)
      WHERE   Ld.numListID = 27
      and (constFlag = true
      or Ld.numDomainid = v_numDomainID)
      and AB.numDomainId = v_numDomainID
      union
      SELECT  Ld.numListItemID AS numPageNavID,
        14 AS numModuleID,
        133 AS numParentID,
        vcData AS vcPageNavName,
        '../Documents/frmDocList.aspx?Category='
      || CAST(Ld.numListItemID AS VARCHAR(10)) AS vcNavURL,
        '../images/tf_note.gif' AS vcImageURL,
        -3
      FROM    Listdetails Ld
      left join listorder LO on Ld.numListItemID = LO.numListItemID
      and LO.numDomainId = v_numDomainID
      inner join AuthoritativeBizDocs AB on (Ld.numListItemID = AB.numAuthoritativePurchase)
      WHERE   Ld.numListID = 27
      and (constFlag = true
      or Ld.numDomainid = v_numDomainID)
      and AB.numDomainId = v_numDomainID
      UNION
      select  0 AS numPageNavID,
        14 AS numModuleID,
        Ld.numListItemID AS numParentID,
        LT.vcData as vcPageNavName,
        '../Documents/frmDocList.aspx?Category='
      || CAST(Ld.numListItemID AS VARCHAR(10)) || '&Status='
      || CAST(LT.numListItemID AS VARCHAR(10)) AS vcNavURL,
        '../images/Text.gif' AS vcImageURL,
        0
      from
      Listdetails LT
      LEFT JOIN LATERAL(SELECT * FROM
         Listdetails
         WHERE
         numListID = 27
         AND (constFlag = true OR numDomainid = v_numDomainID)
         AND (numListItemID IN(SELECT AB.numAuthoritativeSales FROM AuthoritativeBizDocs AB WHERE AB.numDomainId = v_numDomainID)
         or numListItemID IN(SELECT AB.numAuthoritativePurchase FROM AuthoritativeBizDocs AB WHERE AB.numDomainId = v_numDomainID))) Ld on TRUE
      where   LT.numListID = 11
      and LT.numDomainid = v_numDomainID
      union
      select  2222 AS numPageNavID,
        14 AS numModuleID,
        133 AS numParentID,
        'Other BizDocs' as vcPageNavName,
        '',
        '../images/tf_note.gif' AS vcImageURL,
        -1
      union
      SELECT  Ld.numListItemID AS numPageNavID,
        14 AS numModuleID,
        2222 AS numParentID,
        vcData AS vcPageNavName,
        '../Documents/frmDocList.aspx?Category='
      || CAST(Ld.numListItemID AS VARCHAR(10)) AS vcNavURL,
        '../images/Text.gif' AS vcImageURL,
        -1
      FROM    Listdetails Ld
      where   Ld.numListID = 27
      and (constFlag = true
      or Ld.numDomainid = v_numDomainID)
      and Ld.numListItemID not in(select    AB.numAuthoritativeSales
         from      AuthoritativeBizDocs AB
         where     AB.numDomainId = v_numDomainID)
      and Ld.numListItemID not in(select    AB.numAuthoritativePurchase
         from      AuthoritativeBizDocs AB
         where     AB.numDomainId = v_numDomainID)
      union
      SELECT  ls.numListItemID AS numPageNavID,
        14 AS numModuleID,
        Ld.numListItemID as numParentID,
        ls.vcData AS vcPageNavName,
        '../Documents/frmDocList.aspx?Category='
      || CAST(Ld.numListItemID AS VARCHAR(10)) AS vcNavURL,
        '' AS vcImageURL,
        0
      FROM
      Listdetails Ld
      LEFT JOIN LATERAL(SELECT * FROM
         Listdetails ls
         WHERE
         ls.numDomainid = v_numDomainID
         AND ls.numListID = 11) ls on TRUE
      WHERE
      Ld.numListID = 27
      AND (Ld.constFlag = true OR Ld.numDomainid = v_numDomainID)
      AND Ld.numListItemID not in(select    AB.numAuthoritativeSales
         from      AuthoritativeBizDocs AB
         where     AB.numDomainId = v_numDomainID
         union
         select    AB.numAuthoritativePurchase
         from      AuthoritativeBizDocs AB
         where     AB.numDomainId = v_numDomainID)
      ORDER BY
      3,1;
   ELSEIF v_numModuleID = 35
   then

      open SWV_RefCur for
      select PND.numPageNavID,numModuleID,numParentID,vcPageNavName,coalesce(vcNavURL,'') as vcNavURL,vcImageURL ,0 AS sintOrder
      from PageNavigationDTL PND
      JOIN TreeNavigationAuthorization TNA ON PND.numPageNavID = TNA.numPageNavID  --AND PND.numTabID = TNA.numTabID       
      WHERE  1 = 1
      AND  coalesce(TNA.bitVisible,false) = true
              --OR ISNULL(numParentID, 0) = 0
              --OR ISNULL(TNA.bitVisible, 0) = 0
            
      AND coalesce(PND.bitVisible,false) = true
      AND numModuleID = v_numModuleID --AND bitVisible=1  
      AND TNA.numDomainID = v_numDomainID
      AND numGroupID = v_numGroupID
      UNION
      SELECT numFRID,35,CASE numFinancialViewID WHEN 26770 THEN 96 /*Profit & Loss*/
      WHEN 26769 THEN 176 /*Income & Expense*/
      WHEN 26768 THEN 98 /*Cash Flow Statement*/
      WHEN 26767 THEN 97 /*Balance Sheet*/
      WHEN 26766 THEN 81 /*A/R & A/P Aging*/
      WHEN 26766 THEN 82 /*A/R & A/P Aging*/
      ELSE 0
      END
	  ,vcReportName,CASE numFinancialViewID
      WHEN 26770 THEN(SELECT vcNavURL FROM PageNavigationDTL WHERE numPageNavID = 96 AND numModuleID = 35)  /*Profit & Loss*/
      WHEN 26769 THEN(SELECT vcNavURL FROM PageNavigationDTL WHERE numPageNavID = 176 AND numModuleID = 35) /*Income & Expense*/
      WHEN 26768 THEN(SELECT vcNavURL FROM PageNavigationDTL WHERE numPageNavID = 98 AND numModuleID = 35) /*Cash Flow Statement*/
      WHEN 26767 THEN(SELECT vcNavURL FROM PageNavigationDTL WHERE numPageNavID = 97 AND numModuleID = 35) /*Balance Sheet*/
      WHEN 26766 THEN(SELECT vcNavURL FROM PageNavigationDTL WHERE numPageNavID = 81 AND numModuleID = 35) /*A/R & A/P Aging*/
      WHEN 26766 THEN(SELECT vcNavURL FROM PageNavigationDTL WHERE numPageNavID = 82 AND numModuleID = 35) /*A/R & A/P Aging*/
      ELSE ''
      END ||
      CASE POSITION('?' IN(CASE numFinancialViewID
      WHEN 26770 THEN(SELECT vcNavURL FROM PageNavigationDTL WHERE numPageNavID = 96 AND numModuleID = 35)  /*Profit & Loss*/
      WHEN 26769 THEN(SELECT vcNavURL FROM PageNavigationDTL WHERE numPageNavID = 176 AND numModuleID = 35) /*Income & Expense*/
      WHEN 26768 THEN(SELECT vcNavURL FROM PageNavigationDTL WHERE numPageNavID = 98 AND numModuleID = 35) /*Cash Flow Statement*/
      WHEN 26767 THEN(SELECT vcNavURL FROM PageNavigationDTL WHERE numPageNavID = 97 AND numModuleID = 35) /*Balance Sheet*/
      WHEN 26766 THEN(SELECT vcNavURL FROM PageNavigationDTL WHERE numPageNavID = 81 AND numModuleID = 35) /*A/R & A/P Aging*/
      WHEN 26766 THEN(SELECT vcNavURL FROM PageNavigationDTL WHERE numPageNavID = 82 AND numModuleID = 35) /*A/R & A/P Aging*/
      ELSE ''
      END))
      WHEN 0 THEN '?FRID='
      ELSE '&FRID='
      END
      || CAST(numFRID AS VARCHAR(30))
      AS url,'',0 FROM FinancialReport WHERE numDomainID = v_numDomainID
      order by 3,1;
   ELSEIF v_numModuleID = -1 AND v_numDomainID = -1
   then

      open SWV_RefCur for
      SELECT PND.numPageNavID,numModuleID,numParentID,vcPageNavName,coalesce(vcNavURL,'') as vcNavURL,vcImageURL ,0 AS sintOrder
      FROM PageNavigationDTL PND
      JOIN TreeNavigationAuthorization TNA ON PND.numPageNavID = TNA.numPageNavID  --AND PND.numTabID = TNA.numTabID       
      WHERE  1 = 1
      AND  coalesce(TNA.bitVisible,false) = true
              --OR ISNULL(numParentID, 0) = 0
              --OR ISNULL(TNA.bitVisible, 0) = 0
            
      AND coalesce(PND.bitVisible,false) = true
	--AND TNA.numDomainID = @numDomainID
      AND numGroupID = v_numGroupID
	--WHERE bitVisible=1  
      ORDER BY numParentID,numPageNavID;
   ELSEIF v_numModuleID = 10
   then
      select   numParentID INTO v_numParentID FROM PageNavigationDTL WHERE numModuleID = 10    LIMIT 1;
      RAISE NOTICE '%',v_numParentID;
      open SWV_RefCur for
      SELECT * FROM(SELECT PND.numPageNavID,numModuleID,numParentID,vcPageNavName,coalesce(vcNavURL,'') as vcNavURL,vcImageURL ,0 AS sintOrder
         FROM PageNavigationDTL PND
         JOIN TreeNavigationAuthorization TNA ON PND.numPageNavID = TNA.numPageNavID  --AND PND.numTabID = TNA.numTabID       
         WHERE  1 = 1
         AND  coalesce(TNA.bitVisible,false) = true
         AND coalesce(PND.bitVisible,false) = true
         AND numModuleID = v_numModuleID
         AND TNA.numDomainID = v_numDomainID
         AND numGroupID = v_numGroupID
         UNION ALL
         SELECT  numListItemID AS numPageNavID,
		    (SELECT numModuleID FROM ModuleMaster WHERE vcModuleName = 'Opportunities') AS numModuleID,
			v_numParentID AS numParentID,
			coalesce(vcData,'') || 's' AS vcPageNavName,
			'../Opportunity/frmOpenBizDocs.aspx?BizDocName=' || vcData || '&BizDocID=' || CAST(LD.numListItemID AS VARCHAR(10)) AS vcNavURL,
			'../images/Text.gif' AS vcImageURL,
			0 AS bitVisible
         FROM Listdetails LD
         JOIN TreeNavigationAuthorization TNA ON LD.numListItemID = TNA.numPageNavID  AND coalesce(bitVisible,false) = true
         WHERE numListID = 27
         AND coalesce(TNA.numDomainID,0) = v_numDomainID
         AND coalesce(TNA.numGroupID,0) = v_numGroupID) AS FINAL
      ORDER BY numParentID,FINAL.numPageNavID;
   ELSEIF (v_numModuleID = 7 OR v_numModuleID = 36)
   then

      open SWV_RefCur for
      select    PND.numPageNavID,
            numModuleID,
            numParentID,
            vcPageNavName,
            coalesce(vcNavURL,'') as vcNavURL,
            vcImageURL,
            0 AS sintOrder
      from      PageNavigationDTL PND
      WHERE     1 = 1
      AND coalesce(PND.bitVisible,false) = true
      AND numModuleID = v_numModuleID
      order by  numParentID,numPageNavID;
   ELSEIF v_numModuleID = 13
   then

      open SWV_RefCur for
      select PND.numPageNavID,
        PND.numModuleID,
        numParentID,
        vcPageNavName,
        coalesce(vcNavURL,'') as vcNavURL,
        vcImageURL,
        coalesce(PND.intSortOrder,0) AS sintOrder
      from   PageNavigationDTL PND
      JOIN TreeNavigationAuthorization TNA ON PND.numPageNavID = TNA.numPageNavID  --AND PND.numTabID = TNA.numTabID       
      WHERE  1 = 1
      AND  coalesce(TNA.bitVisible,false) = true
				  --OR ISNULL(numParentID, 0) = 0
				  --OR ISNULL(TNA.bitVisible, 0) = 0
				
      AND coalesce(PND.bitVisible,false) = true
      AND PND.numModuleID = v_numModuleID --AND bitVisible=1  
      AND numGroupID = v_numGroupID
      order by
      sintOrder,(CASE WHEN (v_numModuleID = 13 AND PND.numPageNavID = 79) THEN 2000 ELSE 0 END),numParentID,numPageNavID;
   else
      open SWV_RefCur for
      select PND.numPageNavID,
        PND.numModuleID,
        numParentID,
        vcPageNavName,
        coalesce(vcNavURL,'') as vcNavURL,
        vcImageURL,
        0 AS sintOrder
      from   PageNavigationDTL PND
      JOIN TreeNavigationAuthorization TNA ON PND.numPageNavID = TNA.numPageNavID  --AND PND.numTabID = TNA.numTabID       
      WHERE  1 = 1
      AND  coalesce(TNA.bitVisible,false) = true
              --OR ISNULL(numParentID, 0) = 0
              --OR ISNULL(TNA.bitVisible, 0) = 0
            
      AND coalesce(PND.bitVisible,false) = true
      AND PND.numModuleID = v_numModuleID --AND bitVisible=1  
      AND numGroupID = v_numGroupID
      order by numParentID,intSortOrder,numPageNavID;
   end if;
   RETURN;
END; $$;



