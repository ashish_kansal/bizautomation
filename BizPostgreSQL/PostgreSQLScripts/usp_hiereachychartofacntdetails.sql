-- Stored procedure definition script USP_HiereachyChartOfAcntDetails for PostgreSQL
CREATE OR REPLACE FUNCTION USP_HiereachyChartOfAcntDetails(v_numParentAcntID NUMERIC(9,0) DEFAULT 0,        
v_numDomainID NUMERIC(9,0) DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   If v_numParentAcntID = 1 then
      Select numAccountId INTO v_numParentAcntID From Chart_Of_Accounts Where numParntAcntTypeID is null and numAcntTypeId is null and numDomainId = v_numDomainID;
   end if; --and numAccountId = 1         
   RAISE NOTICE '%',v_numParentAcntID;    
   If v_numParentAcntID is not null then
  
      RAISE NOTICE 'SP';
      Delete from HierChartOfAct where numDomainID = v_numDomainID;
      PERFORM USP_ArranageHierChartAct(v_numParentAcntID,v_numDomainID);
   end if;    
   open SWV_RefCur for select cast(numChartOfAcntID as VARCHAR(255)) as numChartOfAcntID,cast(vcCategoryName as VARCHAR(255)) from HierChartOfAct
   where numDomainID = v_numDomainID and  numChartOfAcntID <> v_numParentAcntID;
END; $$;












