-- Stored procedure definition script USP_PreDefinedReptLinks for PostgreSQL
CREATE OR REPLACE FUNCTION USP_PreDefinedReptLinks(v_rpTGroup NUMERIC(9,0) DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for select RPTHeading as "RPTHeading",RptDesc as "RptDesc",vcFileName as "vcFileName" from ReportList
   join PageMaster
   on numPageID = NumID
   where rptGroup = v_rpTGroup and numModuleID = 8 AND bitEnable = true
   order by rptSequence;
END; $$;












