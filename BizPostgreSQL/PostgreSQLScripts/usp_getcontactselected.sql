-- Stored procedure definition script USP_GetContactSelected for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetContactSelected(v_numcontractId NUMERIC(18,0),

v_numDomainId NUMERIC(18,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for select cast(CC.numContactId as VARCHAR(255)),vcEmail,vcFirstName || '-' || vcLastname as Name from ContractsContact CC
   join AdditionalContactsInformation ACI on ACI.numContactId = CC.numContactId
   where numcontractId = v_numcontractId and CC.numDomainId = v_numDomainId;
END; $$;












