CREATE OR REPLACE FUNCTION USP_BizFormWizardMasterConfiguration_Save(v_numDomainID NUMERIC(18,0),
	v_numFormID NUMERIC(18,0),
	v_numGroupID NUMERIC(18,0),
	v_numRelCntType NUMERIC(18,0),
	v_tintPageType SMALLINT,
	v_pageID NUMERIC(18,0),
	v_bitGridConfiguration BOOLEAN DEFAULT false,
	v_strFomFld TEXT DEFAULT NULL,
	v_numFormFieldGroupId NUMERIC(18,0) DEFAULT 0)
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE                                                      
   v_totalRecordCount  INTEGER DEFAULT 0;
   v_SelectedId  INTEGER DEFAULT 1;
   v_numUserCntId  NUMERIC(18,0) DEFAULT 0;
BEGIN


   DELETE FROM
   BizFormWizardMasterConfiguration
   WHERE
   numDomainID = v_numDomainID AND
   numFormID = v_numFormID AND
   numGroupID = v_numGroupID AND
   numRelCntType = v_numRelCntType AND
   tintPageType = v_tintPageType AND
   bitGridConfiguration = v_bitGridConfiguration AND
   coalesce(numFormFieldGroupId,0) = coalesce(v_numFormFieldGroupId,0);

   DELETE FROM
   DycFormConfigurationDetails
   WHERE
   numFormID = v_numFormID
   AND numDomainID = v_numDomainID
   AND numUserCntID IN(SELECT numUserDetailId FROM UserMaster WHERE numDomainID =  v_numDomainID AND numGroupID = v_numGroupID)
   AND coalesce(numRelCntType,0) = coalesce(v_numRelCntType,0)
   AND coalesce(tintPageType,0) = coalesce(v_tintPageType,0) AND
   coalesce(numFormFieldGroupId,0) = coalesce(v_numFormFieldGroupId,0);       
	
   DROP TABLE IF EXISTS tt_USERDETAILS CASCADE;
   CREATE TEMPORARY TABLE tt_USERDETAILS AS
      SELECT numUserDetailId
		
      FROM
      UserMaster
      WHERE
      numDomainID =  v_numDomainID AND numGroupID = v_numGroupID;

   ALTER TABLE tt_USERDETAILS
   add ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1) not null;
   Select Count(*) INTO v_totalRecordCount From tt_USERDETAILS;
   While (v_SelectedId <= v_totalRecordCount) LOOP
      Select  numUserDetailID INTO v_numUserCntId From tt_USERDETAILS where ID = v_SelectedId     LIMIT 1;
      INSERT INTO DycFormConfigurationDetails(numFormID,
				numFieldID,
				intColumnNum,
				intRowNum,
				numDomainID,
				numAuthGroupID,
				numRelCntType,
				tintPageType,
				bitCustom,
				numUserCntID,
				numFormFieldGroupId,
				bitDefaultByAdmin)
      SELECT
			v_numFormID,
			CAST(replace(replace(X.numFormFieldId,'R',''),'C','') AS NUMERIC),
			X.intColumnNum,
			X.intRowNum,
			v_numDomainID,
			v_numGroupID,
			v_numRelCntType,
			v_tintPageType,
			(CASE WHEN POSITION('C' IN X.numFormFieldId) > 0 THEN true ELSE false END),
			v_numUserCntId,
			v_numFormFieldGroupId,
			true
      FROM
	  (
		SELECT
			unnest(xpath('//numFormFieldId/text()', v_strFomFld::xml))::text::VARCHAR as numFormFieldId,
			unnest(xpath('//vcFieldType/text()', v_strFomFld::xml))::text::CHAR(1) as vcFieldType,
			unnest(xpath('//intColumnNum/text()', v_strFomFld::xml))::text::integer as intColumnNum,
			unnest(xpath('//intRowNum/text()', v_strFomFld::xml))::text::integer as intRowNum,
			unnest(xpath('//boolAOIField/text()', v_strFomFld::xml))::text::BOOLEAN as boolAOIField
		) X;

      v_SelectedId := v_SelectedId::bigint+1;
   END LOOP;

   INSERT INTO BizFormWizardMasterConfiguration(numFormID,
			numFieldID,
			intColumnNum,
			intRowNum,
			numDomainID,
			numGroupID,
			numRelCntType,
			tintPageType,
			bitCustom,
			bitGridConfiguration,
			numFormFieldGroupId)
   SELECT
   v_numFormID,
		CAST(replace(replace(X.numFormFieldId,'R',''),'C','') AS NUMERIC),
		X.intColumnNum,
		X.intRowNum,
		v_numDomainID,
		v_numGroupID,
		v_numRelCntType,
		v_tintPageType,
		(CASE WHEN POSITION('C' IN X.numFormFieldId) > 0 THEN true ELSE false END),
		v_bitGridConfiguration,
		v_numFormFieldGroupId
   FROM(SELECT
			unnest(xpath('//numFormFieldId/text()', v_strFomFld::xml))::text::VARCHAR as numFormFieldId,
			unnest(xpath('//vcFieldType/text()', v_strFomFld::xml))::text::CHAR(1) as vcFieldType,
			unnest(xpath('//intColumnNum/text()', v_strFomFld::xml))::text::integer as intColumnNum,
			unnest(xpath('//intRowNum/text()', v_strFomFld::xml))::text::integer as intRowNum,
			unnest(xpath('//boolAOIField/text()', v_strFomFld::xml))::text::BOOLEAN as boolAOIField
		) X;      

   RETURN;
END; $$;


