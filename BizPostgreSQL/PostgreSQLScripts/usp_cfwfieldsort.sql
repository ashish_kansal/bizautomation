-- Stored procedure definition script Usp_cfwFieldSort for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION Usp_cfwFieldSort(v_strFiledId VARCHAR(4000) DEFAULT '',  
v_RelaionId NUMERIC(9,0) DEFAULT NULL,  
v_TabId NUMERIC(9,0) DEFAULT NULL,
v_numDomainID NUMERIC(9,0) DEFAULT NULL, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_separator_position  INTEGER;  
   v_strPosition  VARCHAR(1000);  
   v_count  INTEGER;  
   v_strSql  VARCHAR(4000);
BEGIN
   v_strSql := 'delete from CFW_Fld_Dtl where numFieldId not in (';   
   v_count := 1;  
  
   while coalesce(POSITION(substring(v_strFiledId from E'\\,') IN v_strFiledId),
   0) <> 0 LOOP -- Begin for While Loop  
      v_separator_position := coalesce(POSITION(substring(v_strFiledId from E'\\,') IN v_strFiledId),
      0);
      v_strPosition := SUBSTR(v_strFiledId,1,v_separator_position -1);
      v_strFiledId := OVERLAY(v_strFiledId placing '' from 1 for v_separator_position);
      if exists(select * from CFW_Fld_Dtl where numFieldId = CAST(v_strPosition AS NUMERIC) and numRelation = v_RelaionId) then
   
         update CFW_Fld_Dtl set numOrder = v_count where numFieldId = CAST(v_strPosition AS NUMERIC) and numRelation = v_RelaionId;
      else
         insert into CFW_Fld_Dtl(numFieldId,numRelation,numOrder)
     values(CAST(v_strPosition AS NUMERIC),v_RelaionId,v_count);
      end if;
      v_strSql := coalesce(v_strSql,'') || coalesce(v_strPosition,'') || ',';
      v_count := v_count::bigint+1;
   END LOOP;  
   v_strSql := coalesce(v_strSql,'')  || '0) and numRelation=' || SUBSTR(CAST(v_RelaionId AS VARCHAR(10)),1,10) || ' and numFieldId in( Select fld_id from CFW_Fld_Master where subgrp=' || SUBSTR(CAST(v_TabId AS VARCHAR(10)),1,10) || ' and numDomainID= ' || SUBSTR(CAST(v_numDomainID AS VARCHAR(10)),1,10) || ')';
     
   OPEN SWV_RefCur FOR EXECUTE v_strSql;
   RETURN;
END; $$;


