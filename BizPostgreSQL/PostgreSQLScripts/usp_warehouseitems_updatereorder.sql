-- Stored procedure definition script USP_WarehouseItems_UpdateReorder for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_WarehouseItems_UpdateReorder(v_numDomainID NUMERIC(18,0),
	v_numUserCntID NUMERIC(18,0),
	v_numItemCode NUMERIC(18,0),
	v_numWarehouseItemID NUMERIC(18,0),
	v_numReorder DOUBLE PRECISION)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   IF coalesce((SELECT numReorder FROM WareHouseItems WHERE numWareHouseItemID = v_numWarehouseItemID),0) <> v_numReorder then
	
      UPDATE
      WareHouseItems
      SET
      numReorder = v_numReorder,dtModified = LOCALTIMESTAMP
      WHERE
      numWareHouseItemID = v_numWarehouseItemID;
      UPDATE Item SET bintModifiedDate = TIMEZONE('UTC',now()),numModifiedBy = v_numUserCntID WHERE numItemCode = v_numItemCode AND numDomainID = v_numDomainID;
   end if;
   RETURN;
END; $$;


