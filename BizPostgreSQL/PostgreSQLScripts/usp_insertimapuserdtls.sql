-- Stored procedure definition script USP_InsertImapUserDtls for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_InsertImapUserDtls(v_vcImapPassword VARCHAR(100),
    v_vcImapServerUrl VARCHAR(200),
    v_numImapSSLPort NUMERIC,
    v_bitImapSsl BOOLEAN,
    v_bitImap BOOLEAN,
    v_numUserCntId NUMERIC(9,0),--tintMode=0 then supply UserID instead of UserContactID
    v_numDomainID NUMERIC(9,0),
    v_bitUseUserName BOOLEAN,
    v_numLastUID NUMERIC(9,0),
    v_tintMode SMALLINT,
    v_vcImapUserName VARCHAR(50)--For Support Email Address
)
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numUserDetailId  NUMERIC(9,0);
BEGIN
   v_numUserDetailId := v_numUserCntId;

--IF @numUserCntId=-1 : For Support Email 
   IF v_numUserCntId > 0 then

      select   numUserDetailId INTO v_numUserDetailId FROM    UserMaster WHERE   numUserId = v_numUserCntId;
      UPDATE UserMaster SET tintMailProvider = 3 WHERE numUserId = v_numUserCntId;
   end if;

--delete [ImapUserDetails] where numdomainId = @numDomainID and numUserCntId = @numUserDetailId
   IF v_tintMode = 0 then
    
      IF NOT EXISTS(SELECT * FROM    ImapUserDetails
      WHERE   numDomainID = v_numDomainID
      AND numUserCntID = v_numUserDetailId) then
            
         INSERT  INTO ImapUserDetails(bitImap,
                                                 vcImapServerUrl,
                                                 vcImapPassword,
                                                 bitSSl,
                                                 numPort,
                                                 numDomainID,
                                                 numUserCntID,
                                                 bitUseUserName,
                                                 tintRetryLeft,
                                                 numLastUID,
                                                 bitNotified,vcImapUserName)
                VALUES(v_bitImap,
                          v_vcImapServerUrl,
                          v_vcImapPassword,
                          v_bitImapSsl,
                          v_numImapSSLPort,
                          v_numDomainID,
                          v_numUserDetailId,
                          v_bitUseUserName,
                          10,
                          0,
                          true,v_vcImapUserName);
      ELSE
         UPDATE  ImapUserDetails
         SET     bitImap = v_bitImap,vcImapServerUrl = v_vcImapServerUrl,vcImapPassword = v_vcImapPassword,
         bitSSl = v_bitImapSsl,numPort = v_numImapSSLPort,bitUseUserName = v_bitUseUserName,
         bitNotified = true,tintRetryLeft = 10,vcImapUserName = v_vcImapUserName
         WHERE   numDomainID = v_numDomainID
         AND numUserCntID = v_numUserDetailId;
      end if;
   ELSEIF v_tintMode = 1
   then
        
      UPDATE  ImapUserDetails
      SET     numLastUID = v_numLastUID
      WHERE   numDomainID = v_numDomainID
      AND numUserCntID = v_numUserCntId;
   ELSEIF v_tintMode = 2
   then
        
      UPDATE  ImapUserDetails
      SET     bitNotified = true
      WHERE   numDomainID = v_numDomainID
      AND numUserCntID = v_numUserCntId;
   end if;
   RETURN;
END; $$;



