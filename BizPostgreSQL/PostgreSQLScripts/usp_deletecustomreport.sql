-- Stored procedure definition script usp_DeleteCustomReport for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE PROCEDURE usp_DeleteCustomReport(v_numCustReportID NUMERIC,          
 v_numUserCntID NUMERIC ,
v_numDomainID NUMERIC(9,0))
LANGUAGE plpgsql
   AS $$
BEGIN
   DELETE FROM CustReportFields WHERE numCustomReportId = v_numCustReportID;          
   DELETE FROM CustReportFilterlist WHERE numCustomReportId = v_numCustReportID;        
   DELETE FROM CustReportOrderlist WHERE numCustomReportId = v_numCustReportID;        
   DELETE FROM CustReportSummationlist WHERE numCustomReportId = v_numCustReportID;        
       
   delete FROM CustRptSchContacts where numScheduleId in(select numScheduleid from CustRptScheduler where numReportId = v_numCustReportID);      
   delete FROM CustRptScheduler where numReportId = v_numCustReportID;      
   delete FROM MyReportList where numRPTId = v_numCustReportID and tintType = 1 and numUserCntID = v_numUserCntID;  
        
   DELETE FROM CustomReport WHERE numCustomReportId = v_numCustReportID AND numCreatedBy = v_numUserCntID    and numdomainId = v_numDomainID;      
   RETURN;
END; $$;


