-- Stored procedure definition script usp_GetCircularDistribution for PostgreSQL
CREATE OR REPLACE FUNCTION usp_GetCircularDistribution(v_numDomainID NUMERIC,        
 v_numRoutID NUMERIC,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT ACI.numContactId AS numUserID, CONCAT(ACI.vcFirstName,' ',ACI.vcLastname) AS vcUserName
   FROM UserMaster UM INNER JOIN AdditionalContactsInformation ACI ON UM.numDomainID = ACI.numDomainID
   WHERE UM.numDomainID = v_numDomainID
   AND numContactId = numUserDetailID
   AND ACI.numContactId NOT IN(SELECT cast(numEmpId as NUMERIC(18,0)) FROM RoutingLeadDetails
      WHERE numRoutID = v_numRoutID)
   ORDER BY ACI.vcFirstName;
END; $$;












