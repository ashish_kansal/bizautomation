-- Stored procedure definition script USP_GenericDocuments_GetByFileName for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GenericDocuments_GetByFileName(v_numDomainID NUMERIC(18,0)
	,v_VcFileName VARCHAR(200),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT * FROM
   GenericDocuments
   WHERE
   numDomainId = v_numDomainID
   AND VcFileName = v_VcFileName;
END; $$;












