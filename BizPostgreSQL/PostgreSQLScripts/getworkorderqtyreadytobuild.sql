-- Function definition script GetWorkOrderQtyReadyToBuild for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION GetWorkOrderQtyReadyToBuild(v_numWOID NUMERIC(18,0)
	,v_numQty DOUBLE PRECISION)
RETURNS DOUBLE PRECISION LANGUAGE plpgsql
   AS $$
   DECLARE
   v_fltQtyReadyToBuild  DOUBLE PRECISION DEFAULT v_numQty;
   v_i  INTEGER DEFAULT 1;
   v_iCount  INTEGER;
   v_numTempWOID  NUMERIC(18,0);
BEGIN
   IF EXISTS(SELECT numWOId FROM WorkOrder WHERE numWOId = v_numWOID AND numWOStatus <> 23184) then
      DROP TABLE IF EXISTS tt_GetWorkOrderQtyReadyToBuild CASCADE;
      CREATE TEMPORARY TABLE tt_GetWorkOrderQtyReadyToBuild
      (
         ItemLevel INTEGER,
         numParentWOID NUMERIC(18,0),
         numWOID NUMERIC(18,0),
         numOppID NUMERIC(18,0),
         numOppItemID NUMERIC(18,0),
         numOppChildItemID NUMERIC(18,0),
         numOppKitChildItemID NUMERIC(18,0),
         numItemCode NUMERIC(18,0),
         vcItemName TEXT,
         numQty DOUBLE PRECISION,
         numQtyReq_Orig DOUBLE PRECISION,
         numWarehouseItemID NUMERIC(18,0),
         bitWorkOrder BOOLEAN,
         tintBuildStatus SMALLINT,
         numQtyReadyToBuild DOUBLE PRECISION
      );
      BEGIN
         CREATE TEMP SEQUENCE tt_TEMPWO_seq INCREMENT BY 1 START WITH 1;
         EXCEPTION WHEN OTHERS THEN
            NULL;
      END;
      DROP TABLE IF EXISTS tt_TEMPWO CASCADE;
      CREATE TEMPORARY TABLE tt_TEMPWO
      (
         ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1),
         numWOID NUMERIC(18,0)
      );
      INSERT
      INTO tt_GetWorkOrderQtyReadyToBuild   with recursive CTEWorkOrder(ItemLevel,numParentWOID,numWOID,numOppID,numOppItemID,numOppChildItemID,
      numOppKitChildItemID,numItemKitID,numQtyItemsReq,numWarehouseItemID,
      bitWorkOrder,tintBuildStatus,numQtyReadyToBuild) AS(SELECT
      CAST(1 AS INTEGER) AS ItemLevel,
				numParentWOID AS numParentWOID,
				numWOId AS numWOID,
				numOppID AS numOppID,
				numOppItemID AS numOppItemID,
				numOppChildItemID AS numOppChildItemID,
				numOppKitChildItemID AS numOppKitChildItemID,
				numItemCode AS numItemKitID,
				v_numQty AS numQtyItemsReq,
				numWareHouseItemId AS numWarehouseItemID,
				true AS bitWorkOrder,
				(CASE WHEN numWOStatus = 23184 THEN 2 ELSE 0 END) AS tintBuildStatus,
				(CASE WHEN numWOStatus = 23184 THEN CAST(numQtyItemsReq AS DOUBLE PRECISION) ELSE CAST(0 AS DOUBLE PRECISION) END) AS numQtyReadyToBuild
      FROM
      WorkOrder
      WHERE
      numWOId = v_numWOID
      UNION ALL
      SELECT
      c.ItemLevel+2 AS ItemLevel,
				c.numWOID AS numParentWOID,
				WorkOrder.numWOId AS numWOID,
				WorkOrder.numOppId AS numOppID,
				WorkOrder.numOppItemID AS numOppItemID,
				WorkOrder.numOppChildItemID AS numOppChildItemID,
				WorkOrder.numOppKitChildItemID AS numOppKitChildItemID,
				WorkOrder.numItemCode AS numItemKitID,
				CAST(WorkOrder.numQtyItemsReq AS DOUBLE PRECISION) AS numQtyItemsReq,
				WorkOrder.numWareHouseItemId AS numWarehouseItemID,
				true AS bitWorkOrder,
				(CASE WHEN numWOStatus = 23184 THEN 2 ELSE 0 END) AS tintBuildStatus,
				(CASE WHEN numWOStatus = 23184 THEN CAST(WorkOrder.numQtyItemsReq AS DOUBLE PRECISION) ELSE CAST(0 AS DOUBLE PRECISION) END) AS numQtyReadyToBuild
      FROM
      WorkOrder
      INNER JOIN
      CTEWorkOrder c
      ON
      WorkOrder.numParentWOID = c.numWOID) SELECT
      ItemLevel,
			numParentWOID,
			numWOID,
			numOppID,
			numOppItemID,
			numOppChildItemID,
			numOppKitChildItemID,
			numItemCode,
			vcItemName,
			CTEWorkOrder.numQtyItemsReq,
			0,
			CTEWorkOrder.numWarehouseItemID,
			bitWorkOrder,
			tintBuildStatus,
			0
      FROM
      CTEWorkOrder
      INNER JOIN
      Item
      ON
      CTEWorkOrder.numItemKitID = Item.numItemCode
      LEFT JOIN
      WareHouseItems
      ON
      CTEWorkOrder.numWarehouseItemID = WareHouseItems.numWareHouseItemID;
      UPDATE
      tt_GetWorkOrderQtyReadyToBuild T1
      SET
      numQtyReq_Orig =(SELECT WorkOrderDetails.numQtyItemsReq_Orig FROM WorkOrderDetails INNER JOIN WorkOrder ON WorkOrderDetails.numWOId = WorkOrder.numWOId WHERE WorkOrder.numWOId = T1.numParentWOID AND WorkOrderDetails.numChildItemID = T1.numItemCode AND WorkOrderDetails.numWarehouseItemID = T1.numWarehouseItemID AND WorkOrder.numOppId = T1.numOppID AND WorkOrder.numOppItemID = T1.numOppItemID AND WorkOrder.numOppChildItemID = T1.numOppChildItemID AND WorkOrder.numOppKitChildItemID = T1.numOppKitChildItemID)
		
      WHERE
      numParentWOID > 0;
      INSERT INTO
      tt_GetWorkOrderQtyReadyToBuild
      SELECT
      t1.ItemLevel::bigint+1,
			t1.numWOID,
			NULL,
			numOppID,
			numOppItemID,
			numOppChildItemID,
			numOppKitChildItemID,
			WorkOrderDetails.numChildItemID,
			Item.vcItemName,
			WorkOrderDetails.numQtyItemsReq,
			WorkOrderDetails.numQtyItemsReq_Orig,
			WorkOrderDetails.numWarehouseItemID,
			false AS bitWorkOrder,
			(CASE
      WHEN t1.tintBuildStatus = 2
      THEN 2
      ELSE(CASE
         WHEN Item.charItemType = 'P'
         THEN
            CASE
            WHEN 1 = 2
            THEN(CASE WHEN coalesce(numQtyItemsReq,0) > WareHouseItems.numOnHand THEN 0 ELSE 1 END)
            ELSE(CASE WHEN coalesce(numQtyItemsReq,0) > WareHouseItems.numAllocation THEN 0 ELSE 1 END)
            END
         ELSE 1
         END)
      END),
			(CASE
      WHEN t1.tintBuildStatus = 2
      THEN WorkOrderDetails.numQtyItemsReq
      ELSE(CASE
         WHEN Item.charItemType = 'P'
         THEN
            CASE
            WHEN 1 = 2
            THEN(CASE WHEN coalesce(numQtyItemsReq,0) <= WareHouseItems.numOnHand THEN(WorkOrderDetails.numQtyItemsReq/numQtyItemsReq_Orig) ELSE CAST(coalesce(numOnHand,0)/numQtyItemsReq_Orig AS INTEGER) END)
            ELSE(CASE WHEN coalesce(numQtyItemsReq,0) <= WareHouseItems.numAllocation THEN(WorkOrderDetails.numQtyItemsReq/numQtyItemsReq_Orig) ELSE CAST(coalesce(numAllocation,0)/numQtyItemsReq_Orig AS INTEGER) END)
            END
         ELSE(WorkOrderDetails.numQtyItemsReq/numQtyItemsReq_Orig)
         END)
      END)
      FROM
      WorkOrderDetails
      INNER JOIN
      tt_GetWorkOrderQtyReadyToBuild t1
      ON
      WorkOrderDetails.numWOId = t1.numWOID
      INNER JOIN
      Item
      ON
      WorkOrderDetails.numChildItemID = Item.numItemCode
      AND 1 =(CASE WHEN(SELECT COUNT(*) FROM tt_GetWorkOrderQtyReadyToBuild TInner WHERE TInner.bitWorkOrder = true AND TInner.numParentWOID = t1.numWOID AND TInner.numItemCode = Item.numItemCode) > 0 THEN 0 ELSE 1 END)
      LEFT JOIN
      WareHouseItems
      ON
      WorkOrderDetails.numWarehouseItemID = WareHouseItems.numWareHouseItemID;
      INSERT INTO tt_TEMPWO(numWOID) SELECT numWOID FROM tt_GetWorkOrderQtyReadyToBuild WHERE bitWorkOrder = true ORDER BY ItemLevel DESC;
      select   COUNT(*) INTO v_iCount FROM tt_TEMPWO;
      WHILE v_i <= v_iCount LOOP
         select   numWOID INTO v_numTempWOID FROM tt_TEMPWO WHERE ID = v_i;
         UPDATE
         tt_GetWorkOrderQtyReadyToBuild T1
         SET
         numQtyReadyToBuild =(CASE WHEN coalesce(numQty,0) <=(SELECT MIN(numQtyReadyToBuild) FROM tt_GetWorkOrderQtyReadyToBuild TInner WHERE TInner.numParentWOID = T1.numWOID) THEN (numQty/numQtyReq_Orig) ELSE CAST(coalesce((SELECT MIN(numQtyReadyToBuild) FROM tt_GetWorkOrderQtyReadyToBuild TInner WHERE TInner.numParentWOID = T1.numWOID),0)/numQtyReq_Orig AS INTEGER) END)
			
         WHERE
         numWOID = v_numTempWOID
         AND numParentWOID > 0;
         v_i := v_i::bigint+1;
      END LOOP;
      SELECT MIN(numQtyReadyToBuild) INTO v_fltQtyReadyToBuild FROM tt_GetWorkOrderQtyReadyToBuild WHERE numParentWOID = v_numWOID;
   end if;

   RETURN v_fltQtyReadyToBuild;
END; $$;

