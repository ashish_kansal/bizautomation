-- Stored procedure definition script USP_GetTaskForUserActivity for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetTaskForUserActivity(v_numUserCntId NUMERIC(9,0) DEFAULT 0,    
v_numDomainID NUMERIC(18,0) DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for 
   SELECT 
		T.dtmDueDate AS "dtmDueDate"
		,T.vcMileStoneName AS "vcMileStoneName"
		,T.OrderType AS "OrderType"
		,T.vcStageName AS "vcStageName"
		,T.vcPOppName AS "vcPOppName"
		,T.vcTaskName AS "vcTaskName"
		,T.numOppId AS "numOppId"
		,T.numStageDetailsId AS "numStageDetailsId"
		,T.numOppId AS "numOppId"
		,T.numStagePercentageId AS "numStagePercentageId"
		,T.numStagePercentage AS "tintPercentage"
		,T.tintConfiguration  AS "tintConfiguration"
		FROM(SELECT
      T.vcTaskName,
		CASE WHEN S.bitIsDueDaysUsed = true THEN TO_CHAR(CAST(S.dtStartDate AS DATE)+CAST(S.intDueDays || 'day' as interval),
         'mm/dd/yyyy')
      ELSE TO_CHAR(CAST(S.dtStartDate AS DATE)+CAST((SELECT  CAST(Items AS DECIMAL(18,2)) FROM Split(fn_calculateTotalHoursMinutesByStageDetails(T.numStageDetailsId,T.numOppId,T.numProjectId),'_') LIMIT 1) || 'day' as interval),
         'mm/dd/yyyy') END AS dtmDueDate,
		CASE WHEN OP.tintopptype = 0 THEN 'Sales Opportunity'
      WHEN OP.tintopptype = 1 THEN 'Sales Order' WHEN OP.tintopptype = 2 THEN 'Purchase Order' ELSE '' END AS OrderType,
			 OP.vcpOppName,
			 S.vcMileStoneName,
			 S.vcStageName,
			 T.numStageDetailsId,
			 S.numOppid,
			 S.numStagePercentageId,
			 SM.numstagepercentage,
			 S.tintConfiguration
      FROM StagePercentageDetailsTask AS T
      LEFT JOIN StagePercentageDetails AS S
      ON S.numStageDetailsId = T.numStageDetailsId AND S.numOppid = T.numOppId
      LEFT JOIN OpportunityMaster AS OP
      ON T.numOppId = OP.numOppId
      LEFT JOIN stagepercentagemaster AS SM
      ON S.numStagePercentageId = SM.numstagepercentageid
      WHERE T.numOppId > 0 AND S.numOppid > 0 AND T.bitSavedTask = true AND T.bitTaskClosed = false AND T.numDomainID = v_numDomainID AND
      1 =(CASE WHEN T.numAssignTo = v_numUserCntId OR (S.numTeamId > 0
      AND v_numUserCntId IN(SELECT numContactId FROM AdditionalContactsInformation WHERE numTeam = S.numTeamId)) THEN 1 ELSE 0 END)) AS T;
END; $$;
