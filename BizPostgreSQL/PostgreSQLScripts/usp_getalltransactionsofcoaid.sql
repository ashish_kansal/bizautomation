CREATE OR REPLACE FUNCTION USP_GetAllTransactionsOfCOAId
(
	v_numAccountID NUMERIC(18,0),
	v_CurrentPage INTEGER,
	v_PageSize INTEGER,
	INOUT v_TotRecs INTEGER,
	INOUT SWV_RefCur refcursor)
LANGUAGE plpgsql
AS $$
	DECLARE
		v_firstRec INTEGER;                                                              
		v_lastRec INTEGER;
		v_strSql VARCHAR(5000); 
		v_strSql1 VARCHAR(5000);
BEGIN
	v_firstRec :=(v_CurrentPage::bigint -1)*v_PageSize::bigint;                             
	v_lastRec :=(v_CurrentPage::bigint*v_PageSize::bigint+1);                                                                    
                                  
	v_strSql := 'SELECT BST.numTransactionID,BST.monAmount,BST.vcCheckNumber,BST.dtDatePosted,BST.vcFITransactionID, BST.vcPayeeName, BST.vcMemo,
				BST.vcTxType,BD.vcAccountNumber,BD.vcAccountType,BM.vcFIName,
				coalesce((SELECT  vcCompanyname || ''~'' || CAST(D.numDivisionID AS VARCHAR(10)) FROM CompanyInfo C  
				Join DivisionMaster D  on D.numCompanyID = C.numCompanyId WHERE vcCompanyname ilike  ''%'' ||  BST.vcPayeeName  || ''%'' LIMIT 1),'''')
				AS CompanyName,
				coalesce((SELECT  DM.numDepositId  FROM DepositMaster DM 
				WHERE DM.monDepositAmount = ABS(BST.monAmount) AND 
				DM.dtDepositDate  BETWEEN BST.dtDatePosted+INTERVAL ''-10 day'' AND BST.dtDatePosted+INTERVAL ''10 day'' LIMIT 1),0) AS numReferenceID,
				6 AS ReferenceTypeId
				FROM BankStatementTransactions BST 
				INNER JOIN BankStatementHeader BSH ON BST.numStatementID = BSH.numStatementID 
				INNER JOIN BankDetails BD ON BSH.numBankDetailID = BD.numBankDetailID 
				INNER JOIN BankMaster BM ON BD.numBankMasterID = BM.numBankMasterID';

	v_strSql := coalesce(v_strSql,'') || ' WHERE BST.numAccountID = ' || COALESCE(v_numAccountID,0)  || ' AND BST.monAmount > 0 AND BST.bitIsActive = true AND
				NOT EXISTS(SELECT * FROM BankTransactionMapping BTM WHERE BTM.numTransactionID = BST.numTransactionID)
				UNION ALL/** \n **--** \n **/SELECT BST.numTransactionID,BST.monAmount,BST.vcCheckNumber,BST.dtDatePosted,BST.vcFITransactionID, BST.vcPayeeName, BST.vcMemo,
				BST.vcTxType,BD.vcAccountNumber,BD.vcAccountType,BM.vcFIName,
				coalesce((SELECT  vcCompanyname || ''~'' || CAST(D.numDivisionID AS VARCHAR(10)) FROM CompanyInfo C  
				Join DivisionMaster D  on D.numCompanyID = C.numCompanyId WHERE vcCompanyname ilike  ''%'' ||  BST.vcPayeeName  || ''%'' LIMIT 1),'''')
				AS CompanyName,
				coalesce((SELECT  CH.numCheckHeaderID  FROM CheckHeader CH 
				WHERE CH.monAmount = ABS(BST.monAmount) AND 
				CH.dtCheckDate  BETWEEN BST.dtDatePosted+INTERVAL ''-10 day'' AND BST.dtDatePosted+INTERVAL ''10 day'' LIMIT 1),0) AS numReferenceID, 
				1 AS ReferenceTypeId
				FROM BankStatementTransactions BST 
				INNER JOIN BankStatementHeader BSH ON BST.numStatementID = BSH.numStatementID 
				INNER JOIN BankDetails BD ON BSH.numBankDetailID = BD.numBankDetailID 
				INNER JOIN BankMaster BM ON BD.numBankMasterID = BM.numBankMasterID';
	
	v_strSql := coalesce(v_strSql,'') || ' WHERE BST.numAccountID = ' || COALESCE(v_numAccountID,0)  || 'AND BST.monAmount < 0 AND BST.bitIsActive = true AND
				NOT EXISTS(SELECT * FROM BankTransactionMapping BTM WHERE BTM.numTransactionID = BST.numTransactionID)';

   

   EXECUTE 'DROP TABLE IF EXISTS tt_TEMPTABLE CASCADE;
		   Create TEMPORARY TABLE tt_TEMPTABLE 
		   (
			  numTransactionId NUMERIC(18,0),
			  monAmount DECIMAL(20,5),
			  vcCheckNumber VARCHAR(50),
			  dtDatePosted TIMESTAMP,
			  vcFITransactionID VARCHAR(50),
			  vcPayeeName VARCHAR(50),
			  vcMemo VARCHAR(50),
			  vcTxType VARCHAR(50),
			  vcAccountNumber VARCHAR(50),
			  vcAccountType VARCHAR(50),
			  vcFIName VARCHAR(50),
			  CompanyName VARCHAR(50),
			  numReferenceID NUMERIC(18,0),
			  ReferenceTypeId NUMERIC(18,0)
		   ); 
			INSERT into tt_TEMPTABLE ' || v_strSql;

   v_strSql1 := 'with  Paging
         AS(SELECT Row_number() OVER(ORDER BY dtDatePosted) AS row,* from tt_TEMPTABLE) select * from Paging where  row >' || v_firstRec || ' and row < ' || v_lastRec; 

	OPEN SWV_RefCur FOR EXECUTE v_strSql1;

	SELECT COUNT(*) INTO v_TotRecs from tt_TEMPTABLE;

	RETURN;
END; $$;


