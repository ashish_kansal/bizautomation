-- Stored procedure definition script usp_GetOpportunityOpenForReport for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION usp_GetOpportunityOpenForReport(v_numDomainId NUMERIC,    
v_intSalesPurchase NUMERIC DEFAULT 0,    
v_numUserCntId NUMERIC DEFAULT 0,    
v_intType SMALLINT DEFAULT 0,    
v_tintRights SMALLINT DEFAULT 1, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
BEGIN
   IF v_tintRights = 1 then    
--All Records Rights    
    

      open SWV_RefCur for
      SELECT OM.numOppId as numOppID,OM.vcpOppName as OpportunityName,coalesce(GetOppLstMileStoneCompltd(OM.numOppId),'') as LastMileStoneCompleted,OM.bintCreatedDate as bintCreatedDate,
 coalesce(GetOppLstStage(OM.numOppId),'') as LastStageCompleted, fn_GetContactName(OM.numrecowner) as numRecOwner,
fn_GetContactName(OM.numassignedto)  as numAssignedTo, OM.monPAmount as monPAmount
      FROM OpportunityMaster OM
      WHERE OM.numDomainId = v_numDomainId
      AND OM.tintoppstatus = 0
      AND OM.tintopptype = v_intSalesPurchase;
   end if;    
        
   IF v_tintRights = 2 then

      open SWV_RefCur for
      SELECT OM.numOppId as numOppID,OM.vcpOppName as OpportunityName,coalesce(GetOppLstMileStoneCompltd(OM.numOppId),'') as LastMileStoneCompleted,OM.bintCreatedDate as bintCreatedDate,
 coalesce(GetOppLstStage(OM.numOppId),'') as LastStageCompleted, fn_GetContactName(OM.numrecowner) as numRecOwner,
 fn_GetContactName(OM.numassignedto) as numAssignedTo, OM.monPAmount as monPAmount
      FROM OpportunityMaster OM
      join AdditionalContactsInformation ADC
      on ADC.numContactId = OM.numrecowner
      WHERE OM.numDomainId = v_numDomainId
      AND OM.tintoppstatus = 0
      AND OM.tintopptype = v_intSalesPurchase
      AND ADC.numTeam in(SELECT F.numTeam FROM ForReportsByTeam F
         WHERE F.numUserCntID = v_numUserCntId and F.numDomainID = v_numDomainId and F.tintType = v_intType);
   end if;    
    
   if v_tintRights = 3 then

      open SWV_RefCur for
      SELECT OM.numOppId as numOppID,OM.vcpOppName as OpportunityName,coalesce(GetOppLstMileStoneCompltd(OM.numOppId),'') as LastMileStoneCompleted,OM.bintCreatedDate as bintCreatedDate,
 coalesce(GetOppLstStage(OM.numOppId),'') as LastStageCompleted, fn_GetContactName(OM.numrecowner) as numRecOwner,
 fn_GetContactName(OM.numassignedto) as numAssignedTo, OM.monPAmount as monPAmount
      FROM OpportunityMaster OM
      join DivisionMaster D
      on D.numDivisionID = OM.numDivisionId
      WHERE OM.numDomainId = v_numDomainId
      AND OM.tintoppstatus = 0
      AND OM.tintopptype = v_intSalesPurchase
      AND D.numTerID in(SELECT F.numTerritory FROM ForReportsByTerritory F
         WHERE F.numUserCntId = v_numUserCntId and F.numDomainID = v_numDomainId and F.tintType = v_intType);
   end if;
   RETURN;
END; $$;


