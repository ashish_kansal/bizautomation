-- Stored procedure definition script USP_ManageItemsGroupsDTL_OPPNACC for PostgreSQL
CREATE OR REPLACE FUNCTION USP_ManageItemsGroupsDTL_OPPNACC(v_numItemGroupID NUMERIC(9,0) DEFAULT 0,  
v_numOppAccAttrID NUMERIC(9,0) DEFAULT 0,  
v_numQtyItemsReq NUMERIC(9,0) DEFAULT 0, 
v_numDefaultSelect NUMERIC(9,0) DEFAULT 0, 
v_byteMode SMALLINT DEFAULT NULL,
v_numDomainID NUMERIC(9,0) DEFAULT 0,
v_vcName VARCHAR(100) DEFAULT NULL,
v_bitCombineAssemblies BOOLEAN DEFAULT NULL,
v_numMapToDropdownFld NUMERIC(18,0) DEFAULT 0,
v_numProfileItem NUMERIC(18,0) DEFAULT NULL,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   if v_byteMode = 0 then

      if v_numItemGroupID = 0 then
 
         insert into ItemGroups(vcItemGroup,numDomainID,bitCombineAssemblies,numMapToDropdownFld,numProfileItem)
   values(v_vcName,v_numDomainID,v_bitCombineAssemblies,v_numMapToDropdownFld,v_numProfileItem);
    
         v_numItemGroupID := CURRVAL('ItemGroups_seq');
      end if;
      delete from ItemGroupsDTL where numItemGroupID = v_numItemGroupID and numOppAccAttrID = v_numOppAccAttrID;
      insert into ItemGroupsDTL(numItemGroupID,numOppAccAttrID,numQtyItemsReq,numDefaultSelect,tintType)
values(v_numItemGroupID,v_numOppAccAttrID,v_numQtyItemsReq,v_numDefaultSelect,1);
   end if;  
  
   if v_byteMode = 1 then

      delete from ItemGroupsDTL where numItemGroupID = v_numItemGroupID AND numOppAccAttrID = v_numOppAccAttrID AND tintType = 1;
   end if;

   open SWV_RefCur for select v_numItemGroupID;
END; $$;













