CREATE OR REPLACE FUNCTION USP_OpportunityMaster_GetDataForEDI940(v_numDomainID NUMERIC(18,0)
	,v_numOppID NUMERIC(18,0), INOUT SWV_RefCur refcursor , INOUT SWV_RefCur2 refcursor)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for
   SELECT
   OpportunityMaster.numOppId
		,OpportunityMaster.vcpOppName
		,CompanyInfo.vcCompanyName
		,Domain.vcDomainName
		,OpportunityMaster.numDomainId
		,OpportunityMaster.numDivisionId
		,CONCAT(OpportunityMaster.numDomainId,'-',OpportunityMaster.numDivisionId) AS vcDomainDivisionID
		,coalesce(OpportunityMaster.txtComments,'') AS vcComments
		,coalesce(AdditionalContactsInformation.vcFirstName,'') AS vcFirstName
		,coalesce(AdditionalContactsInformation.vcLastname,'') AS vcLastName
		,coalesce(AdditionalContactsInformation.numCell,'') AS vcPhone
		,coalesce(OpportunityMaster.vcOppRefOrderNo,'') AS "vcCustomerPO#"
		,coalesce((SELECT  vcStreet FROM fn_getOPPAddressDetails(OpportunityMaster.numOppId,OpportunityMaster.numDomainId,1::SMALLINT) LIMIT 1),'') AS vcBillStreet
		,coalesce((SELECT  vcCity FROM fn_getOPPAddressDetails(OpportunityMaster.numOppId,OpportunityMaster.numDomainId,1::SMALLINT) LIMIT 1),'') AS vcBillCity
		,coalesce((SELECT  vcState FROM fn_getOPPAddressDetails(OpportunityMaster.numOppId,OpportunityMaster.numDomainId,1::SMALLINT) LIMIT 1),'') AS vcBillState
		,coalesce((SELECT  vcCountry FROM fn_getOPPAddressDetails(OpportunityMaster.numOppId,OpportunityMaster.numDomainId,1::SMALLINT) LIMIT 1),'') AS vcBillCountry
		,coalesce((SELECT  vcPostalCode FROM fn_getOPPAddressDetails(OpportunityMaster.numOppId,OpportunityMaster.numDomainId,1::SMALLINT) LIMIT 1),'') AS vcBillPostalCode
		,coalesce((SELECT  vcStreet FROM fn_getOPPAddressDetails(OpportunityMaster.numOppId,OpportunityMaster.numDomainId,2::SMALLINT) LIMIT 1),'') AS vcShipStreet
		,coalesce((SELECT  vcCity FROM fn_getOPPAddressDetails(OpportunityMaster.numOppId,OpportunityMaster.numDomainId,2::SMALLINT) LIMIT 1),'') AS vcShipCity
		,coalesce((SELECT  vcState FROM fn_getOPPAddressDetails(OpportunityMaster.numOppId,OpportunityMaster.numDomainId,2::SMALLINT) LIMIT 1),'') AS vcShipState
		,coalesce((SELECT  vcCountry FROM fn_getOPPAddressDetails(OpportunityMaster.numOppId,OpportunityMaster.numDomainId,2::SMALLINT) LIMIT 1),'') AS vcShipCountry
		,coalesce((SELECT  vcPostalCode FROM fn_getOPPAddressDetails(OpportunityMaster.numOppId,OpportunityMaster.numDomainId,2::SMALLINT) LIMIT 1),'') AS vcShipPostalCode
		,coalesce((SELECT  vcDescription FROM DivisionMasterShippingConfiguration WHERE DivisionMasterShippingConfiguration.numDivisionID = DivisionMaster.numDivisionID LIMIT 1),'') AS vcShippingInstruction
		,(CASE WHEN coalesce(DivisionMaster.bitShippingLabelRequired,false) = true THEN 'Yes' ELSE 'No' END) AS bitShippingLabelRequired
   FROM
   OpportunityMaster
   INNER JOIN
   Domain
   ON
   OpportunityMaster.numDomainId = Domain.numDomainId
   INNER JOIN
   DivisionMaster
   ON
   OpportunityMaster.numDivisionId = DivisionMaster.numDivisionID
   INNER JOIN
   CompanyInfo
   ON
   DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
   LEFT JOIN
   AdditionalContactsInformation
   ON
   OpportunityMaster.numContactId = AdditionalContactsInformation.numContactId
   WHERE
   OpportunityMaster.numDomainId = v_numDomainID
   AND OpportunityMaster.numOppId = v_numOppID;


   open SWV_RefCur2 for
   SELECT
   Item.vcItemName
		,CASE
   WHEN coalesce(Item.numItemGroup,0) > 0 AND coalesce(Item.bitMatrix,false) = false
   THEN(CASE WHEN LENGTH(coalesce(WareHouseItems.vcWHSKU,'')) > 0 THEN WareHouseItems.vcWHSKU ELSE coalesce(Item.vcSKU,'') END)
   ELSE(CASE WHEN LENGTH(coalesce(OpportunityItems.vcSKU,'')) > 0 THEN OpportunityItems.vcSKU ELSE coalesce(Item.vcSKU,'') END)
   END AS vcSKU
		,CAST((fn_UOMConversion(coalesce(Item.numBaseUnit,0),OpportunityItems.numItemCode,v_numDomainID, 
   coalesce(OpportunityItems.numUOMId,0))*OpportunityItems.numUnitHour) AS NUMERIC(18,2)) AS numUnits
		,coalesce(UOM.vcUnitName,'Units') AS vcUOM
		,(CASE
   WHEN charItemType = 'P' THEN 'Inventory Item'
   WHEN charItemType = 'S' THEN 'Service'
   WHEN charItemType = 'A' THEN 'Accessory'
   WHEN charItemType = 'N' THEN 'Non-Inventory Item'
   END) AS vcItemType
		,coalesce(OpportunityItems.vcNotes,'') AS vcNotes
		,OpportunityItems.numoppitemtCode AS BizOppItemID
   FROM
   OpportunityItems
   INNER JOIN
   Item
   ON
   OpportunityItems.numItemCode = Item.numItemCode
   LEFT JOIN
   WareHouseItems
   ON
   OpportunityItems.numWarehouseItmsID = WareHouseItems.numWareHouseItemID
   LEFT JOIN
   UOM
   ON
   OpportunityItems.numUOMId = UOM.numUOMId
   WHERE
   numOppId = v_numOppID
   AND coalesce(OpportunityItems.bitDropShip,false) = false;
   RETURN;
END; $$;


