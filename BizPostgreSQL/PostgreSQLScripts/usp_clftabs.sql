-- Stored procedure definition script USP_CLFTabs for PostgreSQL
CREATE OR REPLACE FUNCTION USP_CLFTabs(v_locId NUMERIC(9,0) DEFAULT 0,  
v_numDomainID NUMERIC(9,0) DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for Select cast(Grp_id as VARCHAR(255)),cast(coalesce(Grp_Name,'') as VARCHAR(255)) as grp_name
   from  CFw_Grp_Master
   where Grp_Name is not null and loc_Id = v_locId
   and numDomainID = v_numDomainID and tintType = 0;
END; $$; -- means allow one to add custom field only to normal field tab
-- Check Bug ID 454 for more information












