CREATE OR REPLACE FUNCTION usp_GetSolutionForEdit(v_numSolnID NUMERIC  
,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
	open SWV_RefCur for 
	SELECT 
		numSolnID as "numSolnID"
		,numCategory as "numCategory"
		,vcSolnTitle as "vcSolnTitle"
		,txtSolution as "txtSolution"
		,vcLink as "vcLink"
	FROM 
		SolutionMaster 
	WHERE 
		numSolnID = v_numSolnID;
END; $$;

