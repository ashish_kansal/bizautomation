-- Stored procedure definition script usp_DeleteRule for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION usp_DeleteRule(v_numRuleID NUMERIC     
--  
)
RETURNS VOID LANGUAGE plpgsql  
--This procedure will delete the rule pertaining to the Rule ID passed as parameter.  
   AS $$
BEGIN
   delete from RoutingLeadDetails WHERE numRoutID = v_numRuleID; 
   delete from RoutinLeadsValues WHERE numRoutID = v_numRuleID; 
   DELETE FROM RoutingLeads
   WHERE
   numRoutID = v_numRuleID;
   RETURN;
END; $$;


