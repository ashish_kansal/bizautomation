-- Stored procedure definition script USP_GetCashFlow for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_GetCashFlow(v_numDomainId INTEGER,
v_dtFromDate TIMESTAMP,
v_dtToDate TIMESTAMP,
v_ClientTimeZoneOffset INTEGER, --Added by Chintan to enable calculation of date according to client machine      
v_numAccountClass NUMERIC(9,0) DEFAULT 0, INOUT SWV_RefCur refcursor default null, INOUT SWV_RefCur2 refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numFinYear  INTEGER;
   v_dtFinYearFrom  TIMESTAMP;
   v_CashOpen  DECIMAL(20,5);
BEGIN
   DROP TABLE IF EXISTS tt_CASHFLOW CASCADE;
   CREATE TEMPORARY TABLE tt_CASHFLOW
   (
      Description1 VARCHAR(200),
      Description2 VARCHAR(200),
      Description3 VARCHAR(200),
      vcAccountType VARCHAR(100),
      vcAccountCode VARCHAR(100),
      Amount DECIMAL(20,5)
   );
                           
 

   SELECT numFinYearId INTO v_numFinYear FROM FinancialYear WHERE dtPeriodFrom <= v_dtFromDate AND
   dtPeriodTo >= v_dtFromDate AND numDomainId = v_numDomainId;

   SELECT dtPeriodFrom INTO v_dtFinYearFrom FROM FinancialYear WHERE dtPeriodFrom <= v_dtFromDate AND
   dtPeriodTo >= v_dtFromDate AND numDomainId = v_numDomainId;

   v_dtFromDate := v_dtFromDate+CAST(v_ClientTimeZoneOffset || 'minute' as interval);
   v_dtToDate := v_dtToDate+CAST(v_ClientTimeZoneOffset || 'minute' as interval);


   INSERT INTO tt_CASHFLOW
   select
   CAST((CASE SUBSTR(ATD.vcAccountCode,1,4)
   WHEN '0102' THEN '1) Cash flows from (used in) operating activities'
   WHEN '0103' THEN '1) Cash flows from (used in) operating activities'
   WHEN '0104' THEN '1) Cash flows from (used in) operating activities'
   WHEN '0105' THEN '3) Cash flows from (used in) financing activities'
   WHEN '0106' THEN '1) Cash flows from (used in) operating activities'   END) AS VARCHAR(200)) AS Description1,
CAST((CASE SUBSTR(ATD.vcAccountCode,1,6)
   WHEN '010101' THEN '1) Cash flows from (used in) operating activities'
   WHEN '010102' THEN '2) Cash flows from (used in) investing activities' END) AS VARCHAR(200)) AS Description2,
CAST((CASE ATD.vcAccountCode WHEN '0101' THEN '  Cash flows from (used in) financing activities'
   END) AS VARCHAR(200)) AS Description3,
 ATD.vcAccountType,CAST(ATD.vcAccountCode AS VARCHAR(100)),sum(numCreditAmt)+(sum(numDebitAmt)*(-1)) as Amount from General_Journal_Details GJD INNER JOIN(SELECT  numJournalID,datEntry_Date FROM VIEW_CASHFLOWJOURNAL
      WHERE numDomainID = v_numDomainId AND (numAccountClass = v_numAccountClass OR v_numAccountClass = 0) GROUP BY  numJournalID,datEntry_Date) VCF on
   VCF.numJournalID = GJD.numJournalId INNER JOIN
   Chart_Of_Accounts COA ON
   GJD.numChartAcntId = COA.numAccountId INNER JOIN
   AccountTypeDetail ATD ON COA.numAcntTypeId = ATD.numAccountTypeID
   AND COA.numDomainId = v_numDomainId AND COA.bitActive = true AND
   GJD.numDomainId = v_numDomainId and
   VCF.datEntry_Date BETWEEN v_dtFromDate AND v_dtToDate AND
   GJD.numTransactionId not in(select numTranSactionID from VIEW_CASHFLOWJOURNAL VCF1 WHERE VCF1.numJournalID = VCF.numJournalID)
   group by ATD.vcAccountType,ATD.vcAccountCode;

   v_CashOpen := 0;


   select   coalesce(monOpening,0) INTO v_CashOpen from ChartAccountOpening CAO INNER JOIN Chart_Of_Accounts COA
   ON COA.numAccountId = CAO.numAccountId and
   numFinYearId = v_numFinYear and CAO.numDomainID = v_numDomainId and COA.numDomainId = v_numDomainId AND
(COA.vcAccountCode ilike '01010101%' OR
   COA.vcAccountCode ilike  '01010102%') WHERE COA.bitActive = true;

   select   v_CashOpen+coalesce(SUM(GJD.numDebitAmt),0) -coalesce(SUM(GJD.numCreditAmt),0) INTO v_CashOpen FROM
   General_Journal_Details GJD INNER JOIN
   General_Journal_Header GJH ON GJD.numJournalId = GJH.numJOurnal_Id
   INNER JOIN Chart_Of_Accounts COA ON
   GJD.numChartAcntId = COA.numAccountId AND
   GJD.numDomainId = COA.numDomainId AND
   COA.numDomainId = v_numDomainId AND
   GJD.numDomainId = v_numDomainId  AND
   GJH.numDomainId = v_numDomainId  AND
(COA.vcAccountCode ilike '01010101%' OR
   COA.vcAccountCode ilike  '01010102%') AND
   GJH.datEntry_Date BETWEEN v_dtFinYearFrom AND v_dtFromDate+INTERVAL '-1 day' WHERE COA.bitActive = true AND (GJH.numAccountClass = v_numAccountClass OR v_numAccountClass = 0);

   open SWV_RefCur for
   SELECT coalesce(Description1,coalesce(Description2,Description3)) As Caption,SUM(Amount) AS AmountTotal FROM tt_CASHFLOW  GROUP BY coalesce(Description1,coalesce(Description2,Description3));

   open SWV_RefCur2 for
   SELECT coalesce(Description1,coalesce(Description2,Description3)) As Caption,vcAccountType,Amount  FROM tt_CASHFLOW
   UNION
   SELECT CAST('4) Cash and cash equivalents, beginning of the period' AS VARCHAR(200)),CAST('Cash & Bank Account' AS VARCHAR(100)),v_CashOpen
   order by 1;


   RETURN;
END; $$;
/****** Object:  StoredProcedure [dbo].[USP_GetChartAcntDetails]    Script Date: 09/25/2009 15:47:30 ******/



