-- Stored procedure definition script USP_GetChildItems for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetChildItems(v_numKitId NUMERIC(9,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for select cast(numItemCode as VARCHAR(255)),cast(vcItemName as VARCHAR(255)),cast(txtItemDesc as VARCHAR(255)),cast(DTL.numQtyItemsReq as VARCHAR(255)),0 as numOppChildItemID,
cast(Item.monListPrice as VARCHAR(255)) AS monListPrice,
CAST(Item.monListPrice AS VARCHAR(30)) || '/' || coalesce(UOM.vcUnitName,'Units') as UnitPrice,
case when charItemType = 'P' then 'Inventory Item' when charItemType = 'S' then 'Service' when charItemType = 'A' then 'Accessory' when charItemType = 'N' then 'Non-Inventory Item' end as charItemType,cast(charItemType as VARCHAR(255)) as ItemType
   from Item
   join ItemDetails DTL
   on numChildItemID = numItemCode
   LEFT JOIN UOM ON UOM.numUOMId = Item.numBaseUnit
   where  numItemKitID = v_numKitId;
END; $$;












