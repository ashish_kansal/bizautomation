-- Stored procedure definition script usp_ManageRMAInventory for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION usp_ManageRMAInventory(v_numReturnHeaderID NUMERIC(9,0) DEFAULT 0,
    v_numDomainId NUMERIC(9,0) DEFAULT NULL,
    v_numUserCntID NUMERIC(9,0) DEFAULT 0,
    v_tintFlag SMALLINT DEFAULT NULL  --1: Receive 2: Revert
)
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numOppItemID  NUMERIC(18,0);
   v_tintReturnType  SMALLINT;
   v_tintReceiveType  SMALLINT;
   v_numReturnItemID  NUMERIC;
   v_itemcode  NUMERIC;
   v_numUnits  DOUBLE PRECISION;
   v_numWareHouseItemID  NUMERIC;
   v_numWLocationID  NUMERIC(18,0);
   v_monAmount  DECIMAL(20,5); 
   v_onHand  DOUBLE PRECISION;
   v_onOrder  DOUBLE PRECISION;
   v_onBackOrder  DOUBLE PRECISION;
   v_onAllocation  DOUBLE PRECISION;
   v_TotalOnHand  DOUBLE PRECISION;
   v_monItemAverageCost  DECIMAL(20,5);
   v_monReturnAverageCost  DECIMAL(20,5);
   v_monNewAverageCost  DECIMAL(20,5);
   v_description  VARCHAR(100);
   v_bitKitParent  BOOLEAN;
   v_bitKitChild  BOOLEAN;
   v_k  INTEGER DEFAULT 1;
   v_kCount  INTEGER;
   v_numQtyReturnRemainingToDelete  DOUBLE PRECISION;
   v_i  INTEGER DEFAULT 1;
   v_iCOUNT  INTEGER;
   v_numQtyReturned  DOUBLE PRECISION;
   v_numTempReturnWarehouseItemID  NUMERIC(18,0);

   v_descriptionInner  TEXT DEFAULT CONCAT('Purchase Return Delete (Qty:',v_numQtyReturned,')');

   v_numRemainingQtyToReturn  DOUBLE PRECISION DEFAULT v_numUnits;
   v_numTempOnHand  DOUBLE PRECISION;
   v_numTempOIRLID  NUMERIC(18,0);
   v_numTempWarehouseItemID  NUMERIC(18,0);
   v_numRemaingUnits  DOUBLE PRECISION;

   v_j  INTEGER DEFAULT 1;
   v_jCOUNT  INTEGER;

   v_tintRefType  SMALLINT;
BEGIN
   select   tintReturnType, tintReceiveType INTO v_tintReturnType,v_tintReceiveType FROM
   ReturnHeader WHERE
   numReturnHeaderID = v_numReturnHeaderID;

   BEGIN
      CREATE TEMP SEQUENCE tt_TEMPReturnItems_seq INCREMENT BY 1 START WITH 1;
      EXCEPTION WHEN OTHERS THEN
         NULL;
   END;
   DROP TABLE IF EXISTS tt_TEMPRETURNITEMS CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPRETURNITEMS
   (
      ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1),
      numReturnItemID NUMERIC(18,0),
      numItemCode NUMERIC(18,0),
      numUnitHourReceived DOUBLE PRECISION,
      numWareHouseItemID NUMERIC(18,0),
      monTotAmount DECIMAL(20,5),
      numOppItemID NUMERIC(18,0),
      monAverageCost DECIMAL(20,5),
      monReturnAverageCost DECIMAL(20,5),
      bitKitParent BOOLEAN,
      bitKitChild BOOLEAN
   );

   INSERT INTO tt_TEMPRETURNITEMS(numReturnItemID
		,numItemCode
		,numUnitHourReceived
		,numWareHouseItemID
		,monTotAmount
		,numOppItemID
		,monAverageCost
		,monReturnAverageCost
		,bitKitParent
		,bitKitChild)
   SELECT
   numReturnItemID,
		RI.numItemCode,
		RI.numUnitHourReceived,
  		coalesce(numWareHouseItemID,0),
		RI.monTotAmount,
		RI.numOppItemID
		,coalesce(I.monAverageCost,0)
		,coalesce(OBDI.monAverageCost,RI.monAverageCost)
		,coalesce(I.bitKitParent,false)
		,CAST(0 AS BOOLEAN)
   FROM
   ReturnItems RI
   JOIN
   Item I
   ON
   RI.numItemCode = I.numItemCode
   LEFT JOIN
   OpportunityBizDocItems OBDI
   ON
   RI.numOppBizDocItemID = OBDI.numOppBizDocItemID
   WHERE
   numReturnHeaderID = v_numReturnHeaderID
   AND (charitemtype = 'P' OR 1 =(CASE
   WHEN v_tintReturnType = 1 THEN
      CASE
      WHEN coalesce(I.bitKitParent,false) = true AND coalesce(I.bitAssembly,false) = true THEN 0
      WHEN coalesce(I.bitKitParent,false) = true THEN 1
      ELSE 0
      END
   ELSE
      0
   END))
   ORDER BY
   RI.numReturnItemID;

	-- INSERT KIT CHILD ITEMS
   INSERT INTO tt_TEMPRETURNITEMS(numReturnItemID
		,numItemCode
		,numUnitHourReceived
		,numWareHouseItemID
		,monTotAmount
		,numOppItemID
		,monAverageCost
		,monReturnAverageCost
		,bitKitParent
		,bitKitChild)
   SELECT
   numReturnItemID,
		RI.numItemCode,
		RI.numUnitHourReceived*coalesce(OKI.numQtyItemsReq_Orig,0),
  		coalesce((SELECT  WIInner.numWareHouseItemID FROM WareHouseItems WIInner WHERE WIInner.numItemID = I.numItemCode AND WIInner.numWareHouseID = WI.numWareHouseID AND coalesce(WIInner.numWLocationID,0) = coalesce(WI.numWLocationID,0) LIMIT 1),0),
		RI.monTotAmount,
		RI.numOppItemID
		,coalesce(I.monAverageCost,0)
		,coalesce(OKI.monAvgCost,0)
		,coalesce(I.bitKitParent,false)
		,true
   FROM
   ReturnItems RI
   INNER JOIN
   WareHouseItems WI
   ON
   RI.numWareHouseItemID = WI.numWareHouseItemID
   INNER JOIN
   OpportunityKitItems OKI
   ON
   RI.numOppItemID = OKI.numOppItemID
   INNER JOIN
   Item I
   ON
   OKI.numChildItemID = I.numItemCode
   WHERE
   RI.numReturnHeaderID = v_numReturnHeaderID
   AND coalesce(RI.numOppItemID,0) > 0
   AND coalesce(OKI.numWareHouseItemId,0) > 0;

   INSERT INTO tt_TEMPRETURNITEMS(numReturnItemID
		,numItemCode
		,numUnitHourReceived
		,numWareHouseItemID
		,monTotAmount
		,numOppItemID
		,monAverageCost
		,monReturnAverageCost
		,bitKitParent
		,bitKitChild)
   SELECT
   numReturnItemID,
		RI.numItemCode,
		RI.numUnitHourReceived*coalesce(OKI.numQtyItemsReq_Orig,0)*coalesce(OKCI.numQtyItemsReq_Orig,0),
  		coalesce((SELECT  WIInner.numWareHouseItemID FROM WareHouseItems WIInner WHERE WIInner.numItemID = I.numItemCode AND WIInner.numWareHouseID = WI.numWareHouseID AND coalesce(WIInner.numWLocationID,0) = coalesce(WI.numWLocationID,0) LIMIT 1),0),
		RI.monTotAmount,
		RI.numOppItemID
		,coalesce(I.monAverageCost,0)
		,coalesce(OKCI.monAvgCost,0)
		,coalesce(I.bitKitParent,false)
		,true
   FROM
   ReturnItems RI
   INNER JOIN
   WareHouseItems WI
   ON
   RI.numWareHouseItemID = WI.numWareHouseItemID
   INNER JOIN
   OpportunityKitChildItems OKCI
   ON
   RI.numOppItemID = OKCI.numOppItemID
   INNER JOIN
   OpportunityKitItems OKI
   ON
   OKCI.numOppChildItemID = OKI.numOppChildItemID
   INNER JOIN
   Item I
   ON
   OKCI.numItemID = I.numItemCode
   WHERE
   RI.numReturnHeaderID = v_numReturnHeaderID
   AND coalesce(RI.numOppItemID,0) > 0
   AND coalesce(OKCI.numWareHouseItemId,0) > 0;

   select   COUNT(*) INTO v_kCount FROM tt_TEMPRETURNITEMS;

   WHILE v_k <= v_kCount LOOP
      select   numReturnItemID, numItemCode, numUnitHourReceived, coalesce(numWareHouseItemID,0), monTotAmount, numOppItemID, coalesce(monAverageCost,0), coalesce(monReturnAverageCost,0), coalesce(bitKitParent,false), coalesce(bitKitChild,false) INTO v_numReturnItemID,v_itemcode,v_numUnits,v_numWareHouseItemID,v_monAmount,
      v_numOppItemID,v_monItemAverageCost,v_monReturnAverageCost,v_bitKitParent,
      v_bitKitChild FROM
      tt_TEMPRETURNITEMS WHERE
      ID = v_k    LIMIT 1;
      IF coalesce(v_bitKitParent,false) = false then
		
         IF v_numWareHouseItemID > 0 then
			
            select   SUM(coalesce(numOnHand,0))+SUM(coalesce(numAllocation,0)) INTO v_TotalOnHand FROM WareHouseItems WHERE numItemID = v_itemcode;
            select   coalesce(numOnHand,0), coalesce(numAllocation,0), coalesce(numonOrder,0), coalesce(numBackOrder,0), coalesce(numWLocationID,0) INTO v_onHand,v_onAllocation,v_onOrder,v_onBackOrder,v_numWLocationID FROM
            WareHouseItems WHERE
            numWareHouseItemID = v_numWareHouseItemID; 
			
				--Receive : SalesReturn 
				
				--Revert : PurchaseReturn
				
				--Receive : PurchaseReturn 
            IF (v_tintFlag = 1 AND v_tintReturnType = 1) then
				
               v_description := CONCAT('Sales Return Receive',(CASE WHEN coalesce(v_bitKitChild,false) = true THEN ' Kit Child ' ELSE ' ' END),'(Qty:',v_numUnits,')');
               IF v_onBackOrder >= v_numUnits then
					
                  v_onBackOrder := v_onBackOrder -v_numUnits;
                  v_onAllocation := v_onAllocation+v_numUnits;
               ELSE
                  v_onAllocation := v_onAllocation+v_onBackOrder;
                  v_numUnits := v_numUnits -v_onBackOrder;
                  v_onBackOrder := 0;
                  v_onHand := v_onHand+v_numUnits;
               end if;
               IF v_TotalOnHand+v_numUnits > 0 then
					
                  v_monNewAverageCost :=((v_TotalOnHand*v_monItemAverageCost)+(v_numUnits*v_monReturnAverageCost))/(v_TotalOnHand+v_numUnits);
               ELSE
                  v_monNewAverageCost := v_monItemAverageCost;
               end if;
               UPDATE
               Item
               SET
               monAverageCost =(CASE WHEN coalesce(bitVirtualInventory,false) = true THEN 0 ELSE v_monNewAverageCost END)
               WHERE
               numItemCode = v_itemcode;
				--Revert : SalesReturn
            ELSEIF (v_tintFlag = 2 AND v_tintReturnType = 1)
            then
				
               v_description := CONCAT('Sales Return Delete',(CASE WHEN coalesce(v_bitKitChild,false) = true THEN ' Kit Child ' ELSE ' ' END),'(Qty:',v_numUnits,')');
               IF v_onHand  -v_numUnits >= 0 then
					
                  v_onHand := v_onHand -v_numUnits;
               ELSEIF(v_onHand+v_onAllocation)  -v_numUnits >= 0
               then
					
                  v_numUnits := v_numUnits -v_onHand;
                  v_onHand := 0;
                  v_onBackOrder := v_onBackOrder+v_numUnits;
                  v_onAllocation := v_onAllocation -v_numUnits;
               ELSEIF(v_onHand+v_onBackOrder+v_onAllocation) -v_numUnits >= 0
               then
					
                  v_numUnits := v_numUnits -v_onHand;
                  v_onHand := 0;
                  v_numUnits := v_numUnits -v_onAllocation;
                  v_onAllocation := 0;
                  v_onBackOrder := v_onBackOrder+v_numUnits;
               end if;
               IF v_TotalOnHand -v_numUnits > 0 then
					
                  v_monNewAverageCost :=((v_TotalOnHand*v_monItemAverageCost) -(v_numUnits*v_monReturnAverageCost))/(v_TotalOnHand -v_numUnits);
               ELSE
                  v_monNewAverageCost := v_monItemAverageCost;
               end if;
               UPDATE
               Item
               SET
               monAverageCost =(CASE WHEN coalesce(bitVirtualInventory,false) = true THEN 0 ELSE v_monNewAverageCost END)
               WHERE
               numItemCode = v_itemcode;
            ELSEIF (v_tintFlag = 2 AND v_tintReturnType = 2)
            then
               v_numQtyReturnRemainingToDelete := v_numUnits;
               IF EXISTS(SELECT ID FROM OpportunityItemsReceievedLocationReturn WHERE numReturnID = v_numReturnHeaderID AND numReturnItemID = v_numReturnItemID) then
					
						-- DELETE RETURN QTY
                  DROP TABLE IF EXISTS tt_TEMPRETURN CASCADE;
                  CREATE TEMPORARY TABLE tt_TEMPRETURN
                  (
                     ID INTEGER,
                     numWarehouseItemID NUMERIC(18,0),
                     numReturnedQty DOUBLE PRECISION
                  );
                  INSERT INTO tt_TEMPRETURN(ID,
							numWarehouseItemID,
							numReturnedQty)
                  SELECT
                  ROW_NUMBER() OVER(ORDER BY numWarehouseItemID)
							,numWarehouseItemID
							,SUM(numReturnedQty)
                  FROM
                  OpportunityItemsReceievedLocationReturn OITLR
                  INNER JOIN
                  OpportunityItemsReceievedLocation OIRL
                  ON
                  OITLR.numOIRLID = OIRL.ID
                  WHERE
                  numReturnID = v_numReturnHeaderID
                  AND numReturnItemID = v_numReturnItemID
                  GROUP BY
                  numWarehouseItemID;
                  select   COUNT(*) INTO v_iCOUNT FROM tt_TEMPRETURN;
                  WHILE v_i <= v_iCOUNT LOOP
                     select   numWarehouseItemID, numReturnedQty INTO v_numTempReturnWarehouseItemID,v_numQtyReturned FROM
                     tt_TEMPRETURN WHERE
                     ID = v_i;
						
						-- INCREASE THE OnHand Of Destination Location
                     UPDATE
                     WareHouseItems
                     SET
                     numBackOrder =(CASE WHEN numBackOrder >= v_numQtyReturned THEN coalesce(numBackOrder,0) -v_numQtyReturned ELSE 0 END),numAllocation =(CASE WHEN numBackOrder >= v_numQtyReturned THEN coalesce(numAllocation,0)+v_numQtyReturned ELSE coalesce(numAllocation,0)+coalesce(numBackOrder,0) END),
                     numOnHand =(CASE WHEN numBackOrder >= v_numQtyReturned THEN numOnHand ELSE coalesce(numOnHand,0)+v_numQtyReturned -coalesce(numBackOrder,0) END),dtModified = LOCALTIMESTAMP
                     WHERE
                     numWareHouseItemID = v_numTempReturnWarehouseItemID;
						 --  numeric(9, 0)
							 --  numeric(9, 0)
							 --  tinyint
							 --  varchar(100)
                     v_numQtyReturnRemainingToDelete := coalesce(v_numQtyReturnRemainingToDelete,0) -coalesce(v_numQtyReturned,0);
                     PERFORM USP_ManageWareHouseItems_Tracking(v_numWareHouseItemID := v_numTempReturnWarehouseItemID,v_numReferenceID := v_numReturnHeaderID,
                     v_tintRefType := 5::SMALLINT,v_vcDescription := v_descriptionInner,
                     v_numModifiedBy := v_numUserCntID,v_numDomainId := v_numDomainId,
                     SWV_RefCur := null);
                     v_i := v_i::bigint+1;
                  END LOOP;
                  DELETE FROM OpportunityItemsReceievedLocationReturn WHERE numReturnID = v_numReturnHeaderID AND numReturnItemID = v_numReturnItemID;
               end if;
               IF coalesce(v_numQtyReturnRemainingToDelete,0) > 0 then
					
                  v_description := CONCAT('Purchase Return Delete',(CASE WHEN coalesce(v_bitKitChild,false) = true THEN ' Kit Child ' ELSE ' ' END),'(Qty:',v_numQtyReturnRemainingToDelete,
                  ')');
                  IF v_onBackOrder >= v_numQtyReturnRemainingToDelete then
						
                     v_onBackOrder := v_onBackOrder -v_numQtyReturnRemainingToDelete;
                     v_onAllocation := v_onAllocation+v_numQtyReturnRemainingToDelete;
                  ELSE
                     v_onAllocation := v_onAllocation+v_onBackOrder;
                     v_numQtyReturnRemainingToDelete := v_numQtyReturnRemainingToDelete -v_onBackOrder;
                     v_onBackOrder := 0;
                     v_onHand := v_onHand+v_numQtyReturnRemainingToDelete;
                  end if;
               end if;
               IF v_TotalOnHand+v_numUnits > 0 then
					
                  v_monNewAverageCost :=((v_TotalOnHand*v_monItemAverageCost)+(v_numUnits*v_monReturnAverageCost))/(v_TotalOnHand+v_numUnits);
               ELSE
                  v_monNewAverageCost := v_monItemAverageCost;
               end if;
               UPDATE
               Item
               SET
               monAverageCost =(CASE WHEN coalesce(bitVirtualInventory,false) = true THEN 0 ELSE v_monNewAverageCost END)
               WHERE
               numItemCode = v_itemcode;
            ELSEIF (v_tintFlag = 1 AND v_tintReturnType = 2)
            then
				

					-- FIRST TRY TO RETURN QTY FROM OTHER WAREHOUSES WHERE PURCHASE ORDER IS RECIEVED
               IF EXISTS(SELECT ID FROM OpportunityItemsReceievedLocation OIRL WHERE numOppItemID = v_numOppItemID AND OIRL.numUnitReceieved -coalesce((SELECT SUM(numReturnedQty) FROM OpportunityItemsReceievedLocationReturn WHERE numOIRLID = OIRL.ID),0) >= 0) then
                  DROP TABLE IF EXISTS tt_TEMP CASCADE;
                  CREATE TEMPORARY TABLE tt_TEMP
                  (
                     ID INTEGER,
                     numOIRLID NUMERIC(18,0),
                     numWarehouseItemID NUMERIC(18,0),
                     numUnitReceieved DOUBLE PRECISION,
                     numReturnedQty DOUBLE PRECISION
                  );
                  INSERT INTO
                  tt_TEMP
                  SELECT
                  ROW_NUMBER() OVER(ORDER BY OIRL.ID),
							OIRL.ID,
							OIRL.numWarehouseItemID,
							OIRL.numUnitReceieved,
							coalesce((SELECT SUM(numReturnedQty) FROM OpportunityItemsReceievedLocationReturn WHERE numOIRLID = OIRL.ID),0)
                  FROM
                  OpportunityItemsReceievedLocation OIRL
                  WHERE
                  numOppItemID = v_numOppItemID
                  AND(OIRL.numUnitReceieved -coalesce((SELECT SUM(numReturnedQty) FROM OpportunityItemsReceievedLocationReturn WHERE numOIRLID = OIRL.ID),0)) > 0
                  ORDER BY(OIRL.numUnitReceieved -coalesce((SELECT SUM(numReturnedQty) FROM OpportunityItemsReceievedLocationReturn WHERE numOIRLID = OIRL.ID),0)) DESC;
                  select   COUNT(*) INTO v_jCOUNT FROM tt_TEMP;
                  WHILE v_j <= v_jCOUNT AND v_numRemainingQtyToReturn > 0 LOOP
                     select   numOIRLID, T1.numWarehouseItemID, coalesce(WI.numOnHand,0), coalesce(numUnitReceieved,0) -coalesce(numReturnedQty,0) INTO v_numTempOIRLID,v_numTempWarehouseItemID,v_numTempOnHand,v_numRemaingUnits FROM
                     tt_TEMP T1
                     INNER JOIN
                     WareHouseItems WI
                     ON
                     T1.numWarehouseItemID = WI.numWareHouseItemID WHERE
                     ID = v_j;
                     IF v_numTempOnHand >= v_numRemaingUnits then
						
                        UPDATE
                        WareHouseItems
                        SET
                        numOnHand = coalesce(numOnHand,0) -(CASE WHEN v_numRemaingUnits >= v_numRemainingQtyToReturn THEN v_numRemainingQtyToReturn ELSE v_numRemaingUnits END),dtModified = LOCALTIMESTAMP
                        WHERE
                        numWareHouseItemID = v_numTempWarehouseItemID;
                        INSERT INTO OpportunityItemsReceievedLocationReturn(numOIRLID
								,numReturnID
								,numReturnItemID
								,numReturnedQty)
							VALUES(v_numTempOIRLID
								,v_numReturnHeaderID
								,v_numReturnItemID
								,(CASE WHEN v_numRemaingUnits >= v_numRemainingQtyToReturn THEN v_numRemainingQtyToReturn ELSE v_numRemaingUnits END));
							 --  numeric(9, 0)
								 --  numeric(9, 0)
								 --  tinyint
								 --  varchar(100)
								
                        v_descriptionInner := CONCAT('Purchase Return Receive (Qty:',(CASE WHEN v_numRemaingUnits >= v_numRemainingQtyToReturn THEN v_numRemainingQtyToReturn ELSE v_numRemaingUnits END),
                        ')');
                        PERFORM USP_ManageWareHouseItems_Tracking(v_numWareHouseItemID := v_numTempWarehouseItemID,v_numReferenceID := v_numReturnHeaderID,
                        v_tintRefType := 5::SMALLINT,v_vcDescription := v_descriptionInner,
                        v_numModifiedBy := v_numUserCntID,v_numDomainId := v_numDomainId,
                        SWV_RefCur := null);
                        v_numRemainingQtyToReturn := v_numRemainingQtyToReturn -(CASE WHEN v_numRemaingUnits >= v_numRemainingQtyToReturn THEN v_numRemainingQtyToReturn ELSE v_numRemaingUnits END);
                     ELSEIF v_numTempOnHand > 0
                     then
						
                        UPDATE
                        WareHouseItems
                        SET
                        numOnHand = numOnHand -(CASE WHEN v_numTempOnHand >= v_numRemainingQtyToReturn THEN v_numRemainingQtyToReturn ELSE v_numTempOnHand END),dtModified = LOCALTIMESTAMP
                        WHERE
                        numWareHouseItemID = v_numTempWarehouseItemID;
                        INSERT INTO OpportunityItemsReceievedLocationReturn(numOIRLID
								,numReturnID
								,numReturnItemID
								,numReturnedQty)
							VALUES(v_numTempOIRLID
								,v_numReturnHeaderID
								,v_numReturnItemID
								,(CASE WHEN v_numTempOnHand >= v_numRemainingQtyToReturn THEN v_numRemainingQtyToReturn ELSE v_numTempOnHand END));
							 --  numeric(9, 0)
								 --  numeric(9, 0)
								 --  tinyint
								 --  varchar(100)
								
                        v_descriptionInner := CONCAT('Purchase Return Receive (Qty:',(CASE WHEN v_numTempOnHand >= v_numRemainingQtyToReturn THEN v_numRemainingQtyToReturn ELSE v_numTempOnHand END),
                        ')');
                        PERFORM USP_ManageWareHouseItems_Tracking(v_numWareHouseItemID := v_numTempWarehouseItemID,v_numReferenceID := v_numReturnHeaderID,
                        v_tintRefType := 5::SMALLINT,v_vcDescription := v_descriptionInner,
                        v_numModifiedBy := v_numUserCntID,v_numDomainId := v_numDomainId,
                        SWV_RefCur := null);
                        v_numRemainingQtyToReturn := v_numRemainingQtyToReturn -(CASE WHEN v_numTempOnHand >= v_numRemainingQtyToReturn THEN v_numRemainingQtyToReturn ELSE v_numTempOnHand END);
                     end if;
                     v_j := v_j::bigint+1;
                  END LOOP;
               end if;
               IF v_numRemainingQtyToReturn > 0 then
																
                  v_description := CONCAT('Purchase Return Receive',(CASE WHEN coalesce(v_bitKitChild,false) = true THEN ' Kit Child ' ELSE ' ' END),'(Qty:',v_numRemainingQtyToReturn,
                  ')');
                  IF v_onHand  -v_numRemainingQtyToReturn >= 0 then
					
                     v_onHand := v_onHand -v_numRemainingQtyToReturn;
                  ELSE
                     RAISE EXCEPTION 'PurchaseReturn_Qty';
                     RETURN;
                  end if;
               end if;
               IF v_TotalOnHand -v_numUnits > 0 then
							
                  v_monNewAverageCost :=((v_TotalOnHand*v_monItemAverageCost) -(v_numUnits*v_monReturnAverageCost))/(v_TotalOnHand -v_numUnits);
               ELSE
                  v_monNewAverageCost := v_monItemAverageCost;
               end if;
               UPDATE
               Item
               SET
               monAverageCost =(CASE WHEN coalesce(bitVirtualInventory,false) = true THEN 0 ELSE v_monNewAverageCost END)
               WHERE
               numItemCode = v_itemcode;
            end if;
            IF v_tintReturnType = 1 OR (v_tintReturnType = 2 AND v_numWareHouseItemID > 0 AND (coalesce(v_numRemainingQtyToReturn,0) > 0 OR coalesce(v_numQtyReturnRemainingToDelete,0) > 0)) then
               UPDATE
               WareHouseItems
               SET
               numOnHand = v_onHand,numAllocation = v_onAllocation,numBackOrder = v_onBackOrder,
               numonOrder = v_onOrder,dtModified = LOCALTIMESTAMP
               WHERE
               numWareHouseItemID = v_numWareHouseItemID;
					 --  numeric(9, 0)
					 --  numeric(9, 0)
					 --  tinyint
					 --  varchar(100)
               v_tintRefType := 5;
               PERFORM USP_ManageWareHouseItems_Tracking(v_numWareHouseItemID := v_numWareHouseItemID,v_numReferenceID := v_numReturnHeaderID,
               v_tintRefType := v_tintRefType::SMALLINT,v_vcDescription := v_description,
               v_numModifiedBy := v_numUserCntID,v_numDomainId := v_numDomainId,
               SWV_RefCur := null);
            end if;
         ELSE
            RAISE EXCEPTION 'MISSING_WAREHOUSE';
         end if;
      end if;
      v_k := v_k::bigint+1;
   END LOOP;
END; $$;


