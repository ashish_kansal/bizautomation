-- Stored procedure definition script USP_General_Journal_Details_CheckIfSalesClearingEntryExists for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_General_Journal_Details_CheckIfSalesClearingEntryExists(v_numDomainID NUMERIC(18,0)
	,v_numOppID NUMERIC(18,0), INOUT SWV_RefCur refcursor)
LANGUAGE plpgsql
   AS $$
BEGIN
   IF EXISTS(SELECT
   numTransactionId
   FROM
   General_Journal_Header GJH
   INNER JOIN
   General_Journal_Details GJD
   ON
   GJH.numJOurnal_Id = GJD.numJournalId
   INNER JOIN
   AccountingCharges AC
   ON
   GJD.numChartAcntId = AC.numAccountID
   AND AC.numDomainID = v_numDomainID
   AND AC.numChargeTypeId = 25
   WHERE
   GJH.numOppId = v_numOppID
   AND GJH.numDomainId = v_numDomainID) then
	
      open SWV_RefCur for
      SELECT 1;
   ELSE
      open SWV_RefCur for
      SELECT 0;
   end if;
   RETURN;
END; $$;



