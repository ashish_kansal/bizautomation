-- Stored procedure definition script USP_GetShippingRelationshipsProfiles for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetShippingRelationshipsProfiles(v_numDomainID NUMERIC(9,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT DISTINCT
   cast((SELECT cast(CONCAT((SELECT cast(vcData as VARCHAR(255)) from Listdetails LD
         JOIN  ShippingRules S ON LD.numListItemID = S.numRelationship
         WHERE LD.numListItemID = S.numRelationship AND S.numRuleID = SR.numRuleID),' / ',
      (SELECT cast(vcData as VARCHAR(255)) from Listdetails LD
         JOIN  ShippingRules S ON LD.numListItemID = S.numProfile
         WHERE LD.numListItemID = S.numProfile  AND S.numRuleID = SR.numRuleID)) as VARCHAR(255))) as VARCHAR(255)) AS vcRelProfile,
	cast((SELECT cast(CONCAT((SELECT cast(numListItemID as VARCHAR(255)) from Listdetails LD
         JOIN  ShippingRules S ON LD.numListItemID = S.numRelationship
         WHERE LD.numListItemID = S.numRelationship AND S.numRuleID = SR.numRuleID),'-',(SELECT cast(numListItemID as VARCHAR(255)) from Listdetails LD
         JOIN  ShippingRules S ON LD.numListItemID = S.numProfile
         WHERE LD.numListItemID = S.numProfile  AND S.numRuleID = SR.numRuleID)) as VARCHAR(255))) as VARCHAR(255)) AS numRelProfileID
   FROM ShippingRules SR
   WHERE numDomainId = v_numDomainID AND SR.numRelationship > 0 AND SR.numProfile > 0;
END; $$;












