-- Stored procedure definition script usp_UsersTerritory for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION usp_UsersTerritory(v_numUserID NUMERIC(9,0),    
 v_vcUserName VARCHAR(50),    
 v_vcUserDesc VARCHAR(250),    
 v_numTerritoryID NUMERIC,       
 v_numUserDetailID NUMERIC  --
)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   UPDATE UserMaster SET vcUserName = v_vcUserName,vcUserDesc = v_vcUserDesc,numUserDetailId = v_numUserDetailID
   WHERE numUserId = v_numUserID;
   RETURN;
END; $$;


