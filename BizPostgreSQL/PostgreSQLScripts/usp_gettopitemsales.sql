-- Stored procedure definition script USP_GetTopItemSales for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_GetTopItemSales(v_numDomainId NUMERIC(18,0),
      v_numDivisionId NUMERIC(18,0),
      v_IsUnit BOOLEAN DEFAULT false, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_strSql  VARCHAR(8000);
BEGIN
   DROP TABLE IF EXISTS tt_TEMP CASCADE;
   CREATE TEMPORARY TABLE tt_TEMP
   (
      ItemName VARCHAR(2000),
      TotalUnits NUMERIC(18,2),
      Revenue NUMERIC(18,2)
   );
   v_strSql := ' Insert Into tt_TEMP  SELECT  I.vcItemName As ItemName,SUM(opp.numUnitHour) AS TotalUnits
        ,CAST(SUM(opp.numUnitHour*monPrice) AS DECIMAL(10,2)) AS Revenue
			   FROM  OpportunityItems opp
			   INNER JOIN OpportunityMaster om ON opp.numOppId = om.numOppId
			   JOIN Item I ON opp.numItemCode = I.numItemCode
			   WHERE   om.numDomainId = ' || SUBSTR(CAST(v_numDomainId AS VARCHAR(30)),1,30)
   || ' --and Opp.numOppId=12772 order by numoppitemtCode ASC 
			   AND OM.numDivisionID = ' || SUBSTR(CAST(v_numDivisionId AS VARCHAR(30)),1,30) || ' 
			   GROUP BY opp.numitemcode,I.vcItemName';
   RAISE NOTICE '%',v_strSql;
   EXECUTE v_strSql;
    	   
   IF v_IsUnit = false then
            
      open SWV_RefCur for
      SELECT * FROM tt_TEMP ORDER BY TotalUnits ASC;
   ELSE
      open SWV_RefCur for
      SELECT * FROM tt_TEMP ORDER BY Revenue ASC;
   end if;
    
   RETURN;
END; $$;
			
			 

--EXEC USP_GetTopOrders 93871,85853,1,5,110


