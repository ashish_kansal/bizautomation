-- Function definition script fn_AdvSearchColumnName for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION fn_AdvSearchColumnName(v_numlistID NUMERIC, v_vclistType CHAR(2))
RETURNS VARCHAR(100) LANGUAGE plpgsql
   AS $$
   DECLARE
   v_listName  VARCHAR(100);
BEGIN
   v_listName := '';  
  
   IF v_vclistType = 'L' then
 
      select   vcData INTO v_listName FROM Listdetails WHERE numListItemID = v_numlistID;
   end if;  
   IF v_vclistType = 'G' then
 
      select   vcGrpName INTO v_listName FROM Groups WHERE numGrpId = v_numlistID;
   end if;  
   IF v_vclistType = 'T' then
 
      select   vcTerName INTO v_listName FROM TerritoryMaster WHERE numTerID = v_numlistID;
   end if;  
   IF v_vclistType = 'U' then
 
      select   vcUserName INTO v_listName FROM UserMaster WHERE numUserId = v_numlistID;
   end if;  
   IF v_vclistType = 'S' then
 
      select   vcState INTO v_listName FROM State WHERE numStateID = v_numlistID;
   end if; 

   RETURN v_listName;
END; $$;

