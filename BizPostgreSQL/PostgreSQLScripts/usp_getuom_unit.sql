-- Stored procedure definition script USP_GetUOM_Unit for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetUOM_Unit(v_numDomainID NUMERIC(9,0),
 v_numUOMId NUMERIC(9,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT numUOMId,vcUnitName,tintUnitType,bitEnabled FROM UOM WHERE numDomainID = v_numDomainID AND numUOMId = v_numUOMId;
END; $$;












