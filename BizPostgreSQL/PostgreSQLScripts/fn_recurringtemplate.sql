-- Function definition script fn_RecurringTemplate for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION fn_RecurringTemplate(v_tintFirstDet SMALLINT,v_tintWeekDays SMALLINT,v_DNextDate TIMESTAMP)
RETURNS TIMESTAMP LANGUAGE plpgsql
   AS $$
   DECLARE
   v_vcRetValue  VARCHAR(100);  
--To Get First Sunday of a Month ---  
BEGIN
	If v_tintFirstDet=1 And v_tintWeekDays=1 then
		v_vcRetValue := '1900-01-01'::TIMESTAMP  + make_interval(days =>  6) + make_interval(weeks => DATEDIFF('week','1900-01-01'::TIMESTAMP,(v_DNextDate + make_interval(days => 1-CAST(EXTRACT(day from v_DNextDate) AS INTEGER)))));
	END IF;  
	--To Get Second Sunday of a Month ---  
	If v_tintFirstDet=2 And v_tintWeekDays=1 then
		v_vcRetValue := '1900-01-01'::TIMESTAMP  + make_interval(days =>  6) + make_interval(weeks => DATEDIFF('week','1900-01-01'::TIMESTAMP,(v_DNextDate + make_interval(days => 8-CAST(EXTRACT(day from v_DNextDate) AS INTEGER)))));
	END IF;  
	--To Get Third Sunday of a Month--  
	If v_tintFirstDet=3 And v_tintWeekDays=1 then
		v_vcRetValue := '1900-01-01'::TIMESTAMP  + make_interval(days =>  6) + make_interval(weeks => DATEDIFF('week','1900-01-01'::TIMESTAMP,(v_DNextDate + make_interval(days => 15-CAST(EXTRACT(day from v_DNextDate) AS INTEGER)))));
	END IF;
	--To Get Fourth Sunday of a Month--  
	If v_tintFirstDet=4 And v_tintWeekDays=1 then
		v_vcRetValue := '1900-01-01'::TIMESTAMP  + make_interval(days =>  6) + make_interval(weeks => DATEDIFF('week','1900-01-01'::TIMESTAMP,(v_DNextDate + make_interval(days => 22-CAST(EXTRACT(day from v_DNextDate) AS INTEGER)))));
	END IF;
	--To Get Fifty Sunday of a Month  
	If v_tintFirstDet=5 And v_tintWeekDays=1 then
		v_vcRetValue := '1900-01-01'::TIMESTAMP  + make_interval(days =>  6) + make_interval(weeks => DATEDIFF('week','1900-01-01'::TIMESTAMP,(v_DNextDate + make_interval(days => 29-CAST(EXTRACT(day from v_DNextDate) AS INTEGER)))));
	END IF;
  
----------------------------------------------------------------------------------------------  
	--To Get First Monday of Month --- 
	If v_tintFirstDet=1 And v_tintWeekDays=2 then
		v_vcRetValue := '1900-01-01'::TIMESTAMP + make_interval(weeks => DATEDIFF('week','1900-01-01'::TIMESTAMP,(v_DNextDate + make_interval(days => 6-CAST(EXTRACT(day from v_DNextDate) AS INTEGER)))));
	END IF;  
	--Second  monday of month   
	If v_tintFirstDet=2 And v_tintWeekDays=2 then
		v_vcRetValue := '1900-01-01'::TIMESTAMP  + make_interval(weeks => DATEDIFF('week','1900-01-01'::TIMESTAMP,(v_DNextDate + make_interval(days => 13-CAST(EXTRACT(day from v_DNextDate) AS INTEGER)))));
	END IF;  
	--Third Monday of Month 
	If v_tintFirstDet=3 And v_tintWeekDays=2 then
		v_vcRetValue := '1900-01-01'::TIMESTAMP + make_interval(weeks => DATEDIFF('week','1900-01-01'::TIMESTAMP,(v_DNextDate + make_interval(days => 20-CAST(EXTRACT(day from v_DNextDate) AS INTEGER)))));
	END IF;
	--Fourth Monday of Month    
	If v_tintFirstDet=4 And v_tintWeekDays=2 then
		v_vcRetValue := '1900-01-01'::TIMESTAMP + make_interval(weeks => DATEDIFF('week','1900-01-01'::TIMESTAMP,(v_DNextDate + make_interval(days => 27-CAST(EXTRACT(day from v_DNextDate) AS INTEGER)))));
	END IF;
	--Fifth Month of month   
	If v_tintFirstDet=5 And v_tintWeekDays=2 then
		v_vcRetValue := '1900-01-01'::TIMESTAMP + make_interval(weeks => DATEDIFF('week','1900-01-01'::TIMESTAMP,(v_DNextDate + make_interval(days => 34-CAST(EXTRACT(day from v_DNextDate) AS INTEGER)))));
	END IF;  
------------------------------------------------------------------------------------------------  
	--To Get First Tuesday of Month --- 
	If v_tintFirstDet=1 And v_tintWeekDays=3 then
		v_vcRetValue := '1900-01-01'::TIMESTAMP  + make_interval(days =>  1) + make_interval(weeks => DATEDIFF('week','1900-01-01'::TIMESTAMP,(v_DNextDate + make_interval(days => 5-CAST(EXTRACT(day from v_DNextDate) AS INTEGER)))));
	END IF;  
	--To Get Second Tuesday of a Month ---  
	If v_tintFirstDet=2 And v_tintWeekDays=3 then
		v_vcRetValue := '1900-01-01'::TIMESTAMP  + make_interval(days =>  1) + make_interval(weeks => DATEDIFF('week','1900-01-01'::TIMESTAMP,(v_DNextDate + make_interval(days => 12-CAST(EXTRACT(day from v_DNextDate) AS INTEGER)))));
	END IF;  
	--To Get Third Tuesday of a Month--  
	If v_tintFirstDet=3 And v_tintWeekDays=3 then
		v_vcRetValue := '1900-01-01'::TIMESTAMP  + make_interval(days =>  1) + make_interval(weeks => DATEDIFF('week','1900-01-01'::TIMESTAMP,(v_DNextDate + make_interval(days => 19-CAST(EXTRACT(day from v_DNextDate) AS INTEGER)))));
	END IF;
	--To Get Fourth Tuesday of a Month--  
	If v_tintFirstDet=4 And v_tintWeekDays=3 then
		v_vcRetValue := '1900-01-01'::TIMESTAMP  + make_interval(days =>  1) + make_interval(weeks => DATEDIFF('week','1900-01-01'::TIMESTAMP,(v_DNextDate + make_interval(days => 26-CAST(EXTRACT(day from v_DNextDate) AS INTEGER)))));
	END IF;
	--To Get Fifty Tuesday of a Month  
	If v_tintFirstDet=5 And v_tintWeekDays=3 then
		v_vcRetValue := '1900-01-01'::TIMESTAMP  + make_interval(days =>  1) + make_interval(weeks => DATEDIFF('week','1900-01-01'::TIMESTAMP,(v_DNextDate + make_interval(days => 33-CAST(EXTRACT(day from v_DNextDate) AS INTEGER)))));
	END IF;  
---------------------------------------------------------------------------------------------------  
	--To Get First Wednesday of Month --- 
	If v_tintFirstDet=1 And v_tintWeekDays=4 then
		v_vcRetValue := '1900-01-01'::TIMESTAMP  + make_interval(days =>  2) + make_interval(weeks => DATEDIFF('week','1900-01-01'::TIMESTAMP,(v_DNextDate + make_interval(days => 5-CAST(EXTRACT(day from v_DNextDate) AS INTEGER)))));
	END IF;  
	--To Get Second Wednesday of a Month ---  
	If v_tintFirstDet=2 And v_tintWeekDays=4 then
		v_vcRetValue := '1900-01-01'::TIMESTAMP  + make_interval(days =>  2) + make_interval(weeks => DATEDIFF('week','1900-01-01'::TIMESTAMP,(v_DNextDate + make_interval(days => 12-CAST(EXTRACT(day from v_DNextDate) AS INTEGER)))));
	END IF;  
	--To Get Third Wednesday of a Month--  
	If v_tintFirstDet=3 And v_tintWeekDays=4 then
		v_vcRetValue := '1900-01-01'::TIMESTAMP  + make_interval(days =>  2) + make_interval(weeks => DATEDIFF('week','1900-01-01'::TIMESTAMP,(v_DNextDate + make_interval(days => 19-CAST(EXTRACT(day from v_DNextDate) AS INTEGER)))));
	END IF;
	--To Get Fourth Wednesday of a Month--  
	If v_tintFirstDet=4 And v_tintWeekDays=4 then
		v_vcRetValue := '1900-01-01'::TIMESTAMP  + make_interval(days =>  2) + make_interval(weeks => DATEDIFF('week','1900-01-01'::TIMESTAMP,(v_DNextDate + make_interval(days => 26-CAST(EXTRACT(day from v_DNextDate) AS INTEGER)))));
	END IF;
	--To Get Fifty Wednesday of a Month  
	If v_tintFirstDet=5 And v_tintWeekDays=4 then
		v_vcRetValue := '1900-01-01'::TIMESTAMP  + make_interval(days =>  2) + make_interval(weeks => DATEDIFF('week','1900-01-01'::TIMESTAMP,(v_DNextDate + make_interval(days => 33-CAST(EXTRACT(day from v_DNextDate) AS INTEGER)))));
	END IF;
----------------------------------------------------------------------------------------------------------  
	--To Get First Thursday of a Month ---
	If v_tintFirstDet=1 And v_tintWeekDays=5 then
		v_vcRetValue := '1900-01-01'::TIMESTAMP  + make_interval(days =>  3) + make_interval(weeks => DATEDIFF('week','1900-01-01'::TIMESTAMP,(v_DNextDate + make_interval(days => 4-CAST(EXTRACT(day from v_DNextDate) AS INTEGER)))));
	END IF;  
	--To Get Second Thursday of a Month ---  
	If v_tintFirstDet=2 And v_tintWeekDays=5then
		v_vcRetValue := '1900-01-01'::TIMESTAMP  + make_interval(days =>  3) + make_interval(weeks => DATEDIFF('week','1900-01-01'::TIMESTAMP,(v_DNextDate + make_interval(days => 11-CAST(EXTRACT(day from v_DNextDate) AS INTEGER)))));
	END IF;  
	--To Get Third Thursday of a Month--  
	If v_tintFirstDet=3 And v_tintWeekDays=5 then
		v_vcRetValue := '1900-01-01'::TIMESTAMP  + make_interval(days =>  3) + make_interval(weeks => DATEDIFF('week','1900-01-01'::TIMESTAMP,(v_DNextDate + make_interval(days => 18-CAST(EXTRACT(day from v_DNextDate) AS INTEGER)))));
	END IF;
	--To Get Fourth Thursday of a Month--  
	If v_tintFirstDet=4 And v_tintWeekDays=5 then
		v_vcRetValue := '1900-01-01'::TIMESTAMP  + make_interval(days =>  3) + make_interval(weeks => DATEDIFF('week','1900-01-01'::TIMESTAMP,(v_DNextDate + make_interval(days => 25-CAST(EXTRACT(day from v_DNextDate) AS INTEGER)))));
	END IF;
	--To Get Fifty Thursday of a Month  
	If v_tintFirstDet=5 And v_tintWeekDays=5 then
		v_vcRetValue := '1900-01-01'::TIMESTAMP  + make_interval(days =>  3) + make_interval(weeks => DATEDIFF('week','1900-01-01'::TIMESTAMP,(v_DNextDate + make_interval(days => 32-CAST(EXTRACT(day from v_DNextDate) AS INTEGER)))));
	END IF;
----------------------------------------------------------------------------------------------------  
	--To Get First Friday of a Month ---
	If v_tintFirstDet=1 And v_tintWeekDays=6 then
		v_vcRetValue := '1900-01-01'::TIMESTAMP  + make_interval(days =>  4) + make_interval(weeks => DATEDIFF('week','1900-01-01'::TIMESTAMP,(v_DNextDate + make_interval(days => 2-CAST(EXTRACT(day from v_DNextDate) AS INTEGER)))));
	END IF;  
	--To Get Second Friday of a Month ---  
	If v_tintFirstDet=2 And v_tintWeekDays=6 then
		v_vcRetValue := '1900-01-01'::TIMESTAMP  + make_interval(days =>  4) + make_interval(weeks => DATEDIFF('week','1900-01-01'::TIMESTAMP,(v_DNextDate + make_interval(days => 8-CAST(EXTRACT(day from v_DNextDate) AS INTEGER)))));
	END IF;  
	--To Get Third Friday of a Month--  
	If v_tintFirstDet=3 And v_tintWeekDays=6 then
		v_vcRetValue := '1900-01-01'::TIMESTAMP  + make_interval(days =>  4) + make_interval(weeks => DATEDIFF('week','1900-01-01'::TIMESTAMP,(v_DNextDate + make_interval(days => 15-CAST(EXTRACT(day from v_DNextDate) AS INTEGER)))));
	END IF;
	--To Get Fourth Friday of a Month--  
	If v_tintFirstDet=4 And v_tintWeekDays=6 then
		v_vcRetValue := '1900-01-01'::TIMESTAMP  + make_interval(days =>  4) + make_interval(weeks => DATEDIFF('week','1900-01-01'::TIMESTAMP,(v_DNextDate + make_interval(days => 21-CAST(EXTRACT(day from v_DNextDate) AS INTEGER)))));
	END IF;
	--To Get Fifty Friday of a Month  
	If v_tintFirstDet=5 And v_tintWeekDays=6 then
		v_vcRetValue := '1900-01-01'::TIMESTAMP  + make_interval(days =>  4) + make_interval(weeks => DATEDIFF('week','1900-01-01'::TIMESTAMP,(v_DNextDate + make_interval(days => 28-CAST(EXTRACT(day from v_DNextDate) AS INTEGER)))));
	END IF; 
-------------------------------------------------------------------------------------------------------
	--To Get First Saturday of a Month ---
	If v_tintFirstDet=1 And v_tintWeekDays=7 then
		v_vcRetValue := '1900-01-01'::TIMESTAMP  + make_interval(days =>  5) + make_interval(weeks => DATEDIFF('week','1900-01-01'::TIMESTAMP,(v_DNextDate + make_interval(days => 1-CAST(EXTRACT(day from v_DNextDate) AS INTEGER)))));
	END IF;  
	--To Get Second Saturday of a Month ---  
	If v_tintFirstDet=2 And v_tintWeekDays=7 then
		v_vcRetValue := '1900-01-01'::TIMESTAMP  + make_interval(days =>  5) + make_interval(weeks => DATEDIFF('week','1900-01-01'::TIMESTAMP,(v_DNextDate + make_interval(days => 8-CAST(EXTRACT(day from v_DNextDate) AS INTEGER)))));
	END IF;  
	--To Get Third Saturday of a Month--  
	If v_tintFirstDet=3 And v_tintWeekDays=7 then
		v_vcRetValue := '1900-01-01'::TIMESTAMP  + make_interval(days =>  5) + make_interval(weeks => DATEDIFF('week','1900-01-01'::TIMESTAMP,(v_DNextDate + make_interval(days => 15-CAST(EXTRACT(day from v_DNextDate) AS INTEGER)))));
	END IF;
	--To Get Fourth Saturday of a Month--  
	If v_tintFirstDet=4 And v_tintWeekDays=7 then
		v_vcRetValue := '1900-01-01'::TIMESTAMP  + make_interval(days =>  5) + make_interval(weeks => DATEDIFF('week','1900-01-01'::TIMESTAMP,(v_DNextDate + make_interval(days => 22-CAST(EXTRACT(day from v_DNextDate) AS INTEGER)))));
	END IF;
	--To Get Fifty Saturday of a Month  
	If v_tintFirstDet=5 And v_tintWeekDays=7 then
		v_vcRetValue := '1900-01-01'::TIMESTAMP  + make_interval(days =>  5) + make_interval(weeks => DATEDIFF('week','1900-01-01'::TIMESTAMP,(v_DNextDate + make_interval(days => 29-CAST(EXTRACT(day from v_DNextDate) AS INTEGER)))));
	END IF;
  
   Return v_vcRetValue;
END; $$;

