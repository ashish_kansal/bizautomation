CREATE OR REPLACE FUNCTION USP_DeleteContracts(v_numDomainID NUMERIC(18,0),
v_vcContractId VARCHAR(200) DEFAULT '')
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   DELETE FROM Contracts WHERE numContractId IN (SELECT Id FROM SplitIDs(v_vcContractId,',')) AND numDomainId = v_numDomainID;
   RETURN;
END; $$;




