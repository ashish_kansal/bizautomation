-- Function definition script GetAccountedEmbeddedCost for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION GetAccountedEmbeddedCost(v_numOppBizDocID NUMERIC,
      v_tintMode SMALLINT)
RETURNS DECIMAL(20,5) LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numJournal_Id  NUMERIC;
   v_monCost  DECIMAL(20,5);
BEGIN
   IF v_tintMode = 1 then
	
		--SELECT @monCost = ISNULL(SUM([monCost]),0) FROM [EmbeddedCost] WHERE [numOppBizDocID]=@numOppBizDocID
      select   coalesce(SUM(monEmbeddedCost),0) INTO v_monCost FROM OpportunityBizDocItems WHERE numOppBizDocID = v_numOppBizDocID AND bitEmbeddedCost = true;
   ELSEIF v_tintMode = 2
   then
	
      select   numJOurnal_Id INTO v_numJournal_Id FROM General_Journal_Header WHERE numOppBizDocsId = v_numOppBizDocID;
      select   coalesce(SUM(numCreditAmt),0) INTO v_monCost FROM General_Journal_Details WHERE numJournalId = v_numJournal_Id AND chBizDocItems = 'EC';
   end if;
	
	
   RETURN v_monCost;
END; $$;

