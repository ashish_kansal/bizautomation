-- Stored procedure definition script USP_OpportunityMaster_AddVendor for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_OpportunityMaster_AddVendor(v_numDomainID NUMERIC(18,0)
	,v_numOppID NUMERIC(18,0))
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_i  INTEGER DEFAULT 1;
   v_iCount  INTEGER;
   v_numDivisionId  NUMERIC(18,0);
   v_numItemCode  NUMERIC(18,0);
   v_monPrice  DECIMAL(20,5);
BEGIN
   BEGIN
      CREATE TEMP SEQUENCE tt_TEMP_seq INCREMENT BY 1 START WITH 1;
      EXCEPTION WHEN OTHERS THEN
         NULL;
   END;
   DROP TABLE IF EXISTS tt_TEMP CASCADE;
   CREATE TEMPORARY TABLE tt_TEMP
   (
      ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1),
      numDivisionID NUMERIC(18,0),
      numItemCode NUMERIC(18,0),
      monPrice DECIMAL(20,5)
   );
   INSERT INTO tt_TEMP(numDivisionID
		,numItemCode
		,monPrice)
   SELECT
   numDivisionId
		,numItemCode
		,monPrice
   FROM
   OpportunityMaster OM
   INNER JOIN
   OpportunityItems OI
   ON
   OM.numOppId = OI.numOppId
   WHERE
   OM.numDomainId = v_numDomainID
   AND OM.numOppId = v_numOppID
   AND OM.tintopptype = 2
   AND OM.tintoppstatus = 1;

   select   COUNT(*) INTO v_iCount FROM tt_TEMP;

   WHILE v_i <= v_iCount LOOP
      select   numDivisionID, numItemCode, coalesce(monPrice,0) INTO v_numDivisionId,v_numItemCode,v_monPrice FROM
      tt_TEMP WHERE ID = v_i;
      PERFORM USP_AddVendor(v_numDivisionId,v_numDomainID,v_numItemCode,false,'',0,v_monPrice);
      v_i := v_i::bigint+1;
   END LOOP;
   RETURN;
END; $$;


