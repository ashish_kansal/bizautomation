-- Stored procedure definition script USP_GET_DATA_FOR_ROLLBACK for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GET_DATA_FOR_ROLLBACK(INOUT SWV_RefCur refcursor)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT  HISTORY.numHistoryID
			,MASTER.intImportFileID
			,coalesce(vcImportFileName,'') AS vcImportFileName
		    ,coalesce(numMasterID,0) AS numMasterID
		    ,coalesce(numRecordAdded,0) AS numRecordAdded
		    ,coalesce(numRecordUpdated,0) AS numRecordUpdated
		    ,coalesce(numErrors,0) AS numErrors
		    ,coalesce(numDuplicates,0) AS numDuplicates
		    ,coalesce(dtCreateDate,null::TIMESTAMP) AS dtCreateDate
		    ,coalesce(dtImportDate,null::TIMESTAMP) AS dtImportDate
		    ,coalesce(numDomainID,0)  AS numDomainID
		    ,coalesce(numUserContactID,0) AS numUserContactID
		    ,coalesce(tintStatus,0) AS tintStatus
		    ,btHasSendEmail
			,coalesce(vcHistory_Added_Value,'') AS ItemCodes
			,dtHistoryDateTime
   FROM Import_File_Master MASTER
   INNER JOIN Import_History HISTORY ON MASTER.intImportFileID = HISTORY.intImportFileID
   AND MASTER.numHistoryID = HISTORY.numHistoryID
   WHERE 1 = 1
	--AND (tintStatus = 2 OR tintStatus = 3)
   AND tintStatus = 2
   AND (coalesce(numRecordAdded,0) > 0 OR coalesce(numRecordAdded,0) = 0)
   ORDER BY intImportFileID,dtImportDate;
END; $$;













