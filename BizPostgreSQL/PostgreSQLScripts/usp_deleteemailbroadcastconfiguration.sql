CREATE OR REPLACE FUNCTION USP_DeleteEmailBroadcastConfiguration(v_numConfigurationID NUMERIC(18,0) ,
  v_numDomainID NUMERIC(18,0))
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   IF EXISTS(SELECT * FROM Broadcast WHERE numConfigurationID = v_numConfigurationID) then
      RAISE EXCEPTION 'PRIMARY';
      RETURN;
   ELSE
      DELETE FROM EmailBroadcastConfiguration where numConfigurationID = v_numConfigurationID AND numDomainID = v_numDomainID;
   end if;	
END; $$;


