-- Stored procedure definition script USP_SaveUpdatedReconcileBalance for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_SaveUpdatedReconcileBalance(v_strText TEXT  DEFAULT '',
               v_numReconcileID NUMERIC(9,0) DEFAULT NULL)
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_hDocItem  INTEGER;
BEGIN
   UPDATE General_Journal_Details SET bitReconcile = false,bitCleared = false,numReconcileID = 0 WHERE numReconcileID = v_numReconcileID;
  
   IF SUBSTR(CAST(v_strText AS VARCHAR(10)),1,10) <> '' then
      UPDATE General_Journal_Details
      SET    bitReconcile = X.bitReconcile,bitCleared = X.bitCleared,numReconcileID =(CASE WHEN coalesce(X.bitReconcile,false) = true OR coalesce(X.bitCleared,false) = true THEN v_numReconcileID ELSE 0 END)
      FROM  
	  XMLTABLE
		(
			'NewDataSet/recon'
			PASSING 
				CAST(v_strText AS XML)
			COLUMNS
				id FOR ORDINALITY,
				numTransactionId NUMERIC(18,0) PATH 'numTransactionId',
				bitReconcile BOOLEAN PATH 'bitReconcile',
				bitCleared BOOLEAN PATH 'bitCleared'
		) AS X
      WHERE  General_Journal_Details.numTransactionId = X.numTransactionId;

   end if;
   RETURN;
END; $$;



