-- Stored procedure definition script USP_ScheduledReportsGroupReports_UpdateSortOrder for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ScheduledReportsGroupReports_UpdateSortOrder(v_numDomainID NUMERIC(18,0)
	,v_numSRGID NUMERIC(18,0)
	,v_numSRGRID NUMERIC(18,0)
	,v_intSortOrder INTEGER)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   UPDATE
   ScheduledReportsGroupReports SRGR
   SET
   intSortOrder = coalesce(v_intSortOrder,0)
   FROM
   ScheduledReportsGroup SRG
   WHERE
   SRG.ID = SRGR.numSRGID AND(SRG.numDomainID = v_numDomainID
   AND numSRGID = v_numSRGID
   AND SRGR.ID = v_numSRGRID);
   RETURN;
END; $$;


