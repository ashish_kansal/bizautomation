-- Stored procedure definition script USP_EmailHistory_MarkAsSpam for PostgreSQL
CREATE OR REPLACE FUNCTION USP_EmailHistory_MarkAsSpam(v_vcEmailHstrID TEXT)
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_strSQL  TEXT;
BEGIN
   v_strSQL := 'DELETE FROM EmailHStrToBCCAndCC WHERE numEmailHstrID IN(' || coalesce(v_vcEmailHstrID,'') || ')';
   EXECUTE v_strSQL;
    
   v_strSQL := 'DELETE FROM EmailHstrAttchDtls WHERE numEmailHstrID IN(' || coalesce(v_vcEmailHstrID,'') || ')';
   EXECUTE v_strSQL;   

   v_strSQL := 'DELETE FROM EmailHistory WHERE numEmailHstrID IN(' || coalesce(v_vcEmailHstrID,'') || ')';
   EXECUTE v_strSQL;
   RETURN;
END; $$;
/****** Object:  StoredProcedure [dbo].[USP_MoveCopyItems]    Script Date: 07/26/2008 16:20:12 ******/













