-- Stored procedure definition script Usp_GetBillingTerms for PostgreSQL
CREATE OR REPLACE FUNCTION Usp_GetBillingTerms(v_numTermsID	INTEGER
	,v_numDomainID NUMERIC(18,0)
	,v_ClientTimeZoneOffset INTEGER
	,v_tintApplyTo SMALLINT,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT cast(numTermsID as VARCHAR(255)),cast(vcTerms as VARCHAR(255)),cast(tintApplyTo as VARCHAR(255)),cast(numNetDueInDays as VARCHAR(255)),cast(numDiscount as VARCHAR(255)),cast(numDiscountPaidInDays as VARCHAR(255)),cast(numListItemID as VARCHAR(255)),cast(numDomainID as VARCHAR(255)),
	(CAST(numTermsID AS VARCHAR(10)) || '~' || CAST(numNetDueInDays AS VARCHAR(10))) AS vcValue,
	CASE WHEN(SELECT COUNT(*) FROM OpportunityMaster WHERE OpportunityMaster.intBillingDays = numTermsID AND OpportunityMaster.numDomainId = v_numDomainID) > 0 THEN 1 ELSE 0 END AS IsUsed,
	cast(coalesce(numInterestPercentage,0) as VARCHAR(255)) AS numInterestPercentage,
	cast(coalesce(numInterestPercentageIfPaidAfterDays,0) as VARCHAR(255)) AS numInterestPercentageIfPaidAfterDays,
	fn_GetContactName(numCreatedBy) || ' ' || CAST(dtCreatedDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) AS VARCHAR(20)) AS CreatedBy,
    fn_GetContactName(numModifiedBy) || ' ' || CAST(dtModifiedDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) AS VARCHAR(20)) AS ModifiedBy
   FROM BillingTerms
   WHERE (numTermsID = v_numTermsID OR v_numTermsID = 0)
   AND numDomainID = v_numDomainID
   AND coalesce(bitActive,false) = true
   AND (coalesce(v_tintApplyTo,0) = 0 OR tintApplyTo = v_tintApplyTo);
END; $$;












