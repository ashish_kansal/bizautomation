-- Stored procedure definition script USP_CopyFilesTONewDomain for PostgreSQL
Create or replace FUNCTION USP_CopyFilesTONewDomain(INOUT SWV_RefCur refcursor)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT VcFileName,numDomainId FROM GenericDocuments WHERE LENGTH(VcFileName) > 0
   UNION
   SELECT VcFileName,numDomainID FROM SpecificDocuments WHERE LENGTH(VcFileName) > 0
   UNION
   SELECT vcBankImagePath AS VcFileName,numDomainId FROM Domain WHERE LENGTH(vcBankImagePath) > 0
   UNION
   SELECT vcBizDocFooter AS VcFileName,numDomainId FROM Domain WHERE LENGTH(vcBizDocFooter) > 0
   UNION
   SELECT vcBizDocImagePath AS VcFileName,numDomainId FROM Domain WHERE LENGTH(vcBizDocImagePath) > 0
   UNION
   SELECT vcCompanyImagePath AS VcFileName,numDomainId FROM Domain WHERE LENGTH(vcCompanyImagePath) > 0
   UNION
   SELECT vcPortalLogo AS VcFileName,numDomainId FROM Domain WHERE LENGTH(vcPortalLogo) > 0
   UNION
   SELECT vcPurBizDocFooter AS VcFileName,numDomainId FROM Domain WHERE LENGTH(vcPurBizDocFooter) > 0;
END; $$;











