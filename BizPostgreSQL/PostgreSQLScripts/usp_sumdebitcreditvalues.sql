CREATE OR REPLACE FUNCTION USP_SumDebitCreditValues(v_numDomainId NUMERIC(9,0) DEFAULT 0,  
v_numJournalId NUMERIC(9,0) DEFAULT 0,  
v_numAccountsType NUMERIC(9,0) DEFAULT 0, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
BEGIN
   if v_numAccountsType = 1 then --To Sum Debit Values  

      open SWV_RefCur for
      Select sum(numDebitAmt) from General_Journal_Details Where numDomainId = v_numDomainId
      And numJournalId = v_numJournalId;
   end if;  
   if v_numAccountsType = 2 then --To Sum Credit Values  

      open SWV_RefCur for
      Select sum(numCreditAmt) from General_Journal_Details Where numDomainId = v_numDomainId
      And numJournalId = v_numJournalId;
   end if;
   RETURN;
END; $$;


