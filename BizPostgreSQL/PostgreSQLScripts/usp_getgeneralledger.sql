-- Stored procedure definition script USP_GetGeneralLedger for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_GetGeneralLedger(v_numDomainID INTEGER,
      v_vcAccountId VARCHAR(4000),
      v_dtFromDate TIMESTAMP,
      v_dtToDate TIMESTAMP,
      v_vcTranType VARCHAR(50) DEFAULT '',
      v_varDescription VARCHAR(50) DEFAULT '',
      v_CompanyName VARCHAR(50) DEFAULT '',
      v_vcBizPayment VARCHAR(50) DEFAULT '',
      v_vcCheqNo VARCHAR(50) DEFAULT '',
      v_vcBizDocID VARCHAR(50) DEFAULT '',
      v_vcTranRef VARCHAR(50) DEFAULT '',
      v_vcTranDesc VARCHAR(50) DEFAULT '',
      v_numDivisionID NUMERIC(9,0) DEFAULT 0,
      v_ClientTimeZoneOffset INTEGER DEFAULT NULL,  --Added by Chintan to enable calculation of date according to client machine
      v_charReconFilter CHAR(1) DEFAULT '',
      v_tintMode SMALLINT DEFAULT 0,
      v_numItemID NUMERIC(9,0) DEFAULT 0,
      v_CurrentPage INTEGER DEFAULT 0,
	  v_PageSize INTEGER DEFAULT 0,
      INOUT v_TotRecs INTEGER DEFAULT 0  ,
	  v_TransactionID NUMERIC(9,0) DEFAULT 0, INOUT SWV_RefCur refcursor default null, INOUT SWV_RefCur2 refcursor default null)
LANGUAGE plpgsql -- Added by Chandan 26 Feb 2013 to avoid parameter sniffing

    
		/*RollUp of Sub Accounts */
   AS $$
   DECLARE
   v_firstRec  INTEGER;
   v_lastRec  INTEGER;
BEGIN
   DROP TABLE IF EXISTS tt_TEMP CASCADE;
   CREATE TEMPORARY TABLE tt_TEMP ON COMMIT DROP AS
      SELECT  COA2.numAccountId  /*,COA1.vcAccountCode ,COA2.vcAccountCode*/
      FROM    Chart_Of_Accounts COA1
      INNER JOIN Chart_Of_Accounts COA2
      ON COA1.numDomainId = COA2.numDomainId
      AND COA2.vcAccountCode ilike COA1.vcAccountCode  || '%'
      WHERE   COA1.numAccountId IN(SELECT  cast(ID AS NUMERIC(9,0)) FROM SplitIDs(v_vcAccountId,','))
      AND COA1.numDomainId = v_numDomainID;
   v_vcAccountId := coalesce(OVERLAY((SELECT ',' || CAST(numAccountID AS VARCHAR(10)) FROM tt_TEMP) placing '' from 1 for 1),''); 
    
   open SWV_RefCur for
   SELECT v_numDomainID AS numDomainID,FormatedDateFromDate(dtAsOnDate,v_numDomainID::NUMERIC) AS Date,* FROM fn_GetOpeningBalance(v_vcAccountId,v_numDomainID,v_dtFromDate,v_ClientTimeZoneOffset);

   DROP TABLE IF EXISTS tt_TEMPTABLE CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPTABLE ON COMMIT DROP AS
      SELECT  numDomainId,
                numAccountId,
                TransactionType,
                CompanyName,
                FormatedDateFromDate(datEntry_Date,numDomainId) AS Date,
                varDescription,
                coalesce(BizPayment,'') || ' ' || coalesce(CheqNo,'') AS Narration,
                BizDocID,
                TranRef,
                TranDesc,
                CAST(coalesce(numDebitAmt,0) AS VARCHAR(20)) AS numDebitAmt,
                CAST(coalesce(numCreditAmt,0) AS VARCHAR(20)) AS numCreditAmt,
                vcAccountName,
                '' AS balance,
                numJournal_Id AS JournalId,
				numCheckHeaderID AS CheckId,
				numCashCreditCardId AS CashCreditCardId,
				numTransactionId,
				numOppId,
				numOppBizDocsId,
				numDepositId,
				numCategoryHDRID,
				tintTEType,
				numCategory,
				dtFromDate,
				numUserCntID,bitReconcile,bitCleared,numBillID,numBillPaymentID,numReturnID,
				OVERLAY((SELECT ', ' || CAST(COA.vcAccountName AS VARCHAR(4000))
      FROM General_Journal_Details AS GJD JOIN Chart_Of_Accounts AS COA ON GJD.numChartAcntId = COA.numAccountId
      WHERE GJD.numJournalId = VIEW_GENERALLEDGER.numJournal_Id
      AND COA.numDomainId = VIEW_GENERALLEDGER.numDomainId
      AND COA.numAccountId <> VIEW_GENERALLEDGER.numAccountId) placing '' from 1 for 2) AS vcSplitAccountName,
				Row_number() over(order by datEntry_Date DESC) AS RowNumber 
      FROM    VIEW_GENERALLEDGER
      WHERE   datEntry_Date BETWEEN  v_dtFromDate AND v_dtToDate -- Removed Tmezone bug id 1029 
        --instead of minus for datEntry_Date i have used it for From and To parameter .. DateAdd(minute, -@ClientTimeZoneOffset, datEntry_Date)
      AND (varDescription ilike coalesce(v_varDescription,'') || '%' OR LENGTH(v_varDescription) = 0)
      AND (BizPayment ilike coalesce(v_vcBizPayment,'') || '%' OR LENGTH(v_vcBizPayment) = 0)
      AND (CheqNo ilike coalesce(v_vcCheqNo,'') || '%' OR LENGTH(v_vcCheqNo) = 0)
      AND numDomainId = v_numDomainID
      AND (BizDocID ilike v_vcBizDocID OR LENGTH(v_vcBizDocID) = 0)
      AND (TranRef ilike v_vcTranRef OR LENGTH(v_vcTranRef) = 0)
      AND (TranDesc ilike v_vcTranDesc OR LENGTH(v_vcTranDesc) = 0)
      AND (TransactionType ilike coalesce(v_vcTranType,'') || '%' OR LENGTH(v_vcTranType) = 0)
      AND (bitCleared =(CASE v_charReconFilter WHEN 'C' THEN true ELSE false END)  OR RTRIM(v_charReconFilter) = 'A')
      AND (bitReconcile =(CASE v_charReconFilter WHEN 'R' THEN true ELSE false END)  OR RTRIM(v_charReconFilter) = 'A')
      AND (CompanyName ilike coalesce(v_CompanyName,'') || '%' OR LENGTH(v_CompanyName) = 0)
      AND (numDivisionID = v_numDivisionID OR v_numDivisionID = 0)
      AND (numAccountId IN(SELECT numAccountID FROM tt_TEMP) OR  v_tintMode = 1)
      AND (numItemID = v_numItemID OR v_numItemID = 0)
      ORDER BY datEntry_Date ASC; 
         
   IF v_tintMode = 1 then
      v_firstRec :=(v_CurrentPage::bigint -1)*v_PageSize::bigint;
      v_lastRec :=(v_CurrentPage::bigint*v_PageSize::bigint+1);
      SELECT COUNT(*) INTO v_TotRecs FROM   tt_TEMPTABLE;
      open SWV_RefCur2 for
      SELECT * FROM tt_TEMPTABLE WHERE ROWNUMBER > v_firstRec and ROWNUMBER < v_lastRec order by ROWNUMBER;
   ELSEIF v_tintMode = 2
   then
		 
      open SWV_RefCur2 for
      SELECT * FROM tt_TEMPTABLE where numTransactionId = v_TransactionID;
   ELSE
      open SWV_RefCur2 for
      SELECT * FROM tt_TEMPTABLE;
   end if;
         
		  
		  
   DROP TABLE IF EXISTS tt_TEMP CASCADE;
   RETURN;
END; $$;


