-- Stored procedure definition script USP_GetPromotionOfferDtl for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_GetPromotionOfferDtl( --1=Promotions,2=ShippingRule,3-BatchProcessing,4-PackageRule
v_numProId NUMERIC(9,0) DEFAULT 0,    
v_numDomainID NUMERIC(9,0) DEFAULT 0,   
v_tintRuleAppType SMALLINT DEFAULT NULL,
v_numSiteID NUMERIC(9,0) DEFAULT 0,
v_tintRecordType SMALLINT DEFAULT NULL, INOUT SWV_RefCur refcursor default null, INOUT SWV_RefCur2 refcursor default null)
LANGUAGE plpgsql
   AS $$
BEGIN
   IF v_tintRuleAppType = 1 then
	
      open SWV_RefCur for
      SELECT
      numProItemId
			,numProId
			,numValue
			,vcItemName
			,txtItemDesc
			,vcModelID
      FROM
      Item
      JOIN
      PromotionOfferItems
      ON
      numValue = numItemCode
      WHERE
      numDomainID = v_numDomainID
      AND numProId = v_numProId
      AND tintType = 1
      AND tintRecordType = v_tintRecordType;
   ELSEIF v_tintRuleAppType = 2
   then
	
      open SWV_RefCur for
      SELECT
      numProItemId
			,numProId
			,numValue
			,vcCategoryName
      FROM
      PromotionOfferItems  POI
      INNER JOIN
      Category SC
      ON
      SC.numCategoryID = POI.numValue
      WHERE
      numProId = v_numProId
      AND tintType = 2
      AND tintRecordType = v_tintRecordType;

      open SWV_RefCur2 for
      SELECT
      SC.numCategoryID,
			coalesce(vcCategoryName,'-') AS vcCategoryName
      FROM
      SiteCategories SC
      JOIN
      Category C
      ON
      SC.numCategoryID = C.numCategoryID
      WHERE
      C.numDomainID = v_numDomainID
      AND SC.numSiteID = v_numSiteID
      AND SC.numCategoryID NOT IN(select numValue from PromotionOfferItems where tintType = 2 and numProId = v_numProId AND tintRecordType = v_tintRecordType);
   ELSEIF coalesce(v_tintRuleAppType,0) = 3
   then
	
      open SWV_RefCur for
      SELECT * FROM Listdetails WHERE numListID = 461 AND numDomainid = v_numDomainID;

      open SWV_RefCur2 for
      SELECT
      numProItemId
			,numProId
			,numValue
			,vcData
      FROM
      PromotionOfferItems POI
      INNER JOIN
      Listdetails LD
      ON
      POI.numValue = LD.numListItemID
      WHERE
      numListID = 461
      AND numProId = v_numProId
      AND tintType = 3
      AND tintRecordType = v_tintRecordType;
   ELSEIF coalesce(v_tintRuleAppType,0) = 4
   then
	
      open SWV_RefCur for
      SELECT * FROM Listdetails WHERE numListID = 36 AND numDomainid = v_numDomainID;

      open SWV_RefCur2 for
      SELECT
      numProItemId
			,numProId
			,numValue
			,vcData
      FROM
      PromotionOfferItems POI
      INNER JOIN
      Listdetails LD
      ON
      POI.numValue = LD.numListItemID
      WHERE
      numListID = 36
      AND numProId = v_numProId
      AND tintType = 4
      AND tintRecordType = v_tintRecordType;
   end if;
   RETURN;
END; $$;


