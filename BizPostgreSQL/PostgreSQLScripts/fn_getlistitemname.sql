-- Function definition script fn_GetListItemName for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION fn_GetListItemName(v_numlistID NUMERIC)
RETURNS VARCHAR(100) LANGUAGE plpgsql
   AS $$
   DECLARE
   v_listName  VARCHAR(100);
BEGIN
   select   coalesce(vcData,'') INTO v_listName from Listdetails where numListItemID = v_numlistID;

   return coalesce(v_listName,'-');
END; $$;

