-- Stored procedure definition script USP_UpdatedomainRentalSetting for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_UpdatedomainRentalSetting(v_numDomainID NUMERIC(9,0) DEFAULT 0,
v_bitRentalItem BOOLEAN DEFAULT false,
v_numRentalItemClass NUMERIC(9,0) DEFAULT NULL,
v_numRentalHourlyUOM NUMERIC(9,0) DEFAULT NULL,
v_numRentalDailyUOM NUMERIC(9,0) DEFAULT NULL,
v_tintRentalPriceBasedOn SMALLINT DEFAULT NULL)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
      UPDATE
      Domain
      SET
      bitRentalItem = v_bitRentalItem,numRentalItemClass = v_numRentalItemClass,
      numRentalHourlyUOM = v_numRentalHourlyUOM,numRentalDailyUOM = v_numRentalDailyUOM,
      tintRentalPriceBasedOn = v_tintRentalPriceBasedOn
      WHERE
      numDomainId = v_numDomainID;

   RETURN;
END; $$;


