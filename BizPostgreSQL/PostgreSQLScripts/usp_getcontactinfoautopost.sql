-- Stored procedure definition script usp_GetContactInfoAutoPost for PostgreSQL
CREATE OR REPLACE FUNCTION usp_GetContactInfoAutoPost(v_vcCompanyname VARCHAR(50),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT vcLastname, vcFirstName,vcFirstName || ' ' || vcLastname as Name,vcGivenName, vcPosition, vcEmail, vcDepartment,
    numPhone, numPhoneExtension, vcFax, numContactType,   cast(vcData as VARCHAR(255)) ,
    numContactId, bintDOB, txtNotes, AC.numCreatedBy,
   DM.numTerID, --New          
   DM.numCreatedBy --New          
   FROM AdditionalContactsInformation AC join
   DivisionMaster DM on DM.numDivisionID = AC.numDivisionId
   left join Listdetails lst on AC.numContactType = lst.numListItemID
   WHERE AC.numDivisionId in(select d.numDivisionID from CompanyInfo a join DivisionMaster d
      on  a.numCompanyId = d.numCompanyID  where vcCompanyname ilike coalesce(v_vcCompanyname,'') || '%');
END; $$;












