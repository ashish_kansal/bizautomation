-- Function definition script fn_GetAccountSummary for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION fn_GetAccountSummary(v_numDomainId INTEGER,
v_dtFromDate VARCHAR(50) DEFAULT '',
v_dtToDate VARCHAR(50) DEFAULT '')
RETURNS table
(
   numAccountId INTEGER,
   vcAccountName VARCHAR(100),
   numParntAcntTypeID INTEGER,
   vcAccountDescription VARCHAR(100),
   vcAccountCode VARCHAR(50),
   dtAsOnDate TIMESTAMP,
   mnOpeningBalace NUMERIC(19,4),
   mnuDebit  NUMERIC(19,4),
   mnuCredit NUMERIC(19,4)
) LANGUAGE plpgsql
   AS $$
BEGIN
   DROP TABLE IF EXISTS tt_FN_GETACCOUNTSUMMARY_COAOPENINGBALANCE CASCADE;
   CREATE TEMPORARY TABLE IF NOT EXISTS  tt_FN_GETACCOUNTSUMMARY_COAOPENINGBALANCE
   (
      numAccountId INTEGER,
      vcAccountName VARCHAR(100),
      numParntAcntTypeID INTEGER,
      vcAccountDescription VARCHAR(100),
      vcAccountCode VARCHAR(50),
      dtAsOnDate TIMESTAMP,
      mnOpeningBalace NUMERIC(19,4),
      mnuDebit  NUMERIC(19,4),
      mnuCredit NUMERIC(19,4)
   );
   INSERT INTO tt_FN_GETACCOUNTSUMMARY_COAOPENINGBALANCE
   SELECT COA.numAccountId,COA.vcAccountName,COA.numParntAcntTypeID,COA.vcAccountDescription,COA.vcAccountCode,
 CAST(v_dtFromDate AS TIMESTAMP),CAST(coalesce(COA.numOriginalOpeningBal,0)+(SELECT coalesce(SUM(GJD.numDebitAmt),0) -coalesce(SUM(GJD.numCreditAmt),0)
      FROM
      General_Journal_Details GJD INNER JOIN
      General_Journal_Header GJH ON GJD.numJournalId = GJH.numJOurnal_Id AND
      GJD.numDomainId = COA.numDomainId AND
      GJD.numChartAcntId = COA.numAccountId AND
      GJH.datEntry_Date < CAST(v_dtFromDate AS timestamp)) AS NUMERIC(19,4)) AS OPENING,
CAST((SELECT coalesce(SUM(GJD.numDebitAmt),0)
      FROM
      General_Journal_Details GJD INNER JOIN
      General_Journal_Header GJH ON GJD.numJournalId = GJH.numJOurnal_Id AND
      GJD.numDomainId = COA.numDomainId AND
      GJD.numChartAcntId = COA.numAccountId AND
      GJH.datEntry_Date BETWEEN CAST(v_dtFromDate AS timestamp) AND CAST(v_dtToDate AS timestamp)) AS NUMERIC(19,4)) as DEBIT,
CAST((SELECT coalesce(SUM(GJD.numCreditAmt),0)
      FROM
      General_Journal_Details GJD INNER JOIN
      General_Journal_Header GJH ON GJD.numJournalId = GJH.numJOurnal_Id AND
      GJD.numDomainId = COA.numDomainId AND
      GJD.numChartAcntId = COA.numAccountId AND
      GJH.datEntry_Date BETWEEN CAST(v_dtFromDate AS timestamp) AND CAST(v_dtToDate AS timestamp)) AS NUMERIC(19,4)) as CREDIT
   FROM Chart_Of_Accounts COA
   WHERE COA.numDomainId = v_numDomainId;
	


RETURN QUERY (SELECT * FROM tt_FN_GETACCOUNTSUMMARY_COAOPENINGBALANCE);
END; $$;

