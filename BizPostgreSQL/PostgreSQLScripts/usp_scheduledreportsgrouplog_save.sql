-- Stored procedure definition script USP_ScheduledReportsGroupLog_Save for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ScheduledReportsGroupLog_Save(v_numSRGID NUMERIC(18,0)
	,v_vcException TEXT)
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numDomainID  NUMERIC(18,0);
BEGIN
   UPDATE ScheduledReportsGroup SET intNotOfTimeTried = coalesce(intNotOfTimeTried,0)+1 WHERE ID = v_numSRGID;

   IF coalesce((SELECT intNotOfTimeTried FROM ScheduledReportsGroup WHERE ID = v_numSRGID),0) = 5 then
      select   numDomainID INTO v_numDomainID FROM ScheduledReportsGroup WHERE ID = v_numSRGID;
      PERFORM USP_ScheduledReportsGroup_UpdateNextDate(v_numDomainID,v_numSRGID);
   end if;

   INSERT INTO ScheduledReportsGroupLog(numSRGID
		,dtExecutionDate
		,vcException)
	VALUES(v_numSRGID
		,TIMEZONE('UTC',now())
		,v_vcException);
RETURN;
END; $$;


