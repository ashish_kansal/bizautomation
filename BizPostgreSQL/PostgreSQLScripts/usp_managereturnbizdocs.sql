-- Stored procedure definition script USP_ManageReturnBizDocs for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021

CREATE OR REPLACE FUNCTION USP_ManageReturnBizDocs(v_numReturnHeaderID NUMERIC(9,0) DEFAULT 0,
      v_tintReceiveType SMALLINT DEFAULT NULL,
      v_numReturnStatus NUMERIC(9,0) DEFAULT NULL,
      v_numDomainId NUMERIC(9,0) DEFAULT NULL,
      v_numUserCntID NUMERIC(9,0) DEFAULT NULL,
      v_numAccountID NUMERIC(9,0) DEFAULT NULL,
      v_vcCheckNumber VARCHAR(50) DEFAULT NULL,
      v_IsCreateRefundReceipt BOOLEAN DEFAULT NULL,
      v_numItemCode	NUMERIC(18,0) DEFAULT NULL)
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_tintType  SMALLINT;
   v_tintReturnType  SMALLINT;
   v_tintOrigReceiveType  SMALLINT;
	
   v_numBizdocTempID  NUMERIC(18,0);
   v_monAmount  DECIMAL(20,5);
   v_monTotalTax  DECIMAL(20,5);
   v_monTotalDiscount  DECIMAL(20,5);  
		
   v_numDepositIDRef  NUMERIC(18,0);
   my_ex_state TEXT;
	my_ex_message TEXT;
	my_ex_detail TEXT;
	my_ex_hint TEXT;
	my_ex_ctx TEXT;
   v_i  INTEGER DEFAULT 1;
   v_COUNT  INTEGER; 
   v_numTempOppID  NUMERIC(18,0);
   v_numTempOppItemID  NUMERIC(18,0);
   v_bitTempLotNo  BOOLEAN;
   v_numTempQty  DOUBLE PRECISION; 
   v_numTempWareHouseItmsDTLID  INTEGER;
BEGIN
   BEGIN
      -- BEGIN TRANSACTION
select   tintReturnType, tintReceiveType INTO v_tintReturnType,v_tintOrigReceiveType FROM ReturnHeader WHERE numReturnHeaderID = v_numReturnHeaderID;
      IF v_tintOrigReceiveType = 1 then
	
         RAISE EXCEPTION 'REFUND_RECEIPT_ALREADY_ISSUED';
      ELSEIF v_tintOrigReceiveType = 2
      then
	
         RAISE EXCEPTION 'CREDIT_MEMO_ALREADY_ISSUED';
      ELSEIF v_tintOrigReceiveType = 3
      then
	
         RAISE EXCEPTION 'REFUND_ALREADY_ISSUED';
      end if;
      RAISE NOTICE '%',v_tintReturnType;
      RAISE NOTICE '%',v_tintReceiveType;
      IF v_tintReturnType = 1 OR v_tintReturnType = 2 then
    		

		--SALES RETURN
         IF v_tintReturnType = 1 then
		
            UPDATE
            WareHouseItmsDTL WID
            SET
            numWareHouseItemID = OWSI.numWarehouseItmsID,numQty = 1
            FROM
            OppWarehouseSerializedItem OWSI INNER JOIN WareHouseItems ON OWSI.numWarehouseItmsID = WareHouseItems.numWareHouseItemID INNER JOIN Item ON WareHouseItems.numItemID = Item.numItemCode
            WHERE
            OWSI.numWarehouseItmsDTLID = WID.numWareHouseItmsDTLID AND(OWSI.numReturnHeaderID = v_numReturnHeaderID
            AND coalesce(Item.bitSerialized,false) = true);
            INSERT INTO WareHouseItmsDTL(numWareHouseItemID
				,vcSerialNo
				,numQty)
            SELECT
            OWSI.numWarehouseItmsID
				,WID.vcSerialNo
				,OWSI.numQty
            FROM
            OppWarehouseSerializedItem OWSI
            INNER JOIN
            WareHouseItmsDTL WID
            ON
            OWSI.numWarehouseItmsDTLID = WID.numWareHouseItmsDTLID
            INNER JOIN
            WareHouseItems
            ON
            OWSI.numWarehouseItmsID = WareHouseItems.numWareHouseItemID
            INNER JOIN
            Item
            ON
            WareHouseItems.numItemID = Item.numItemCode
            WHERE
            OWSI.numReturnHeaderID = v_numReturnHeaderID
            AND coalesce(Item.bitLotNo,false) = true;
         ELSEIF v_tintReturnType = 2
         then
		
            IF(SELECT
            COUNT(*)
            FROM
            WareHouseItmsDTL WHIDL
            INNER JOIN
            OppWarehouseSerializedItem OWSI
            ON
            WHIDL.numWareHouseItmsDTLID = OWSI.numWarehouseItmsDTLID
            AND WHIDL.numWareHouseItemID = OWSI.numWarehouseItmsID
            INNER JOIN
            WareHouseItems
            ON
            WHIDL.numWareHouseItemID = WareHouseItems.numWareHouseItemID
            INNER JOIN
            Item
            ON
            WareHouseItems.numItemID = Item.numItemCode
            WHERE
            numReturnHeaderID = v_numReturnHeaderID
            AND 1 =(CASE
            WHEN Item.bitLotNo = true AND coalesce(WHIDL.numQty,0) < coalesce(OWSI.numQty,0) THEN 1
            WHEN Item.bitSerialized = true AND coalesce(WHIDL.numQty,0) = 0 THEN 1
            ELSE 0
            END)) > 0 then
			
               RAISE EXCEPTION 'SERIAL/LOT#_USED';
            ELSE
				-- REMOVE SERIAL/LOT# NUUMBER FROM INVENTORY
               UPDATE WareHouseItmsDTL WHIDL
               SET numQty =(CASE WHEN Item.bitLotNo = true THEN coalesce(WHIDL.numQty,0) -coalesce(OWSI.numQty,0) ELSE 0 END)
               FROM
               OppWarehouseSerializedItem OWSI, WareHouseItems INNER JOIN Item ON WareHouseItems.numItemID = Item.numItemCode
               WHERE(WHIDL.numWareHouseItmsDTLID = OWSI.numWarehouseItmsDTLID
               AND WHIDL.numWareHouseItemID = OWSI.numWarehouseItmsID) AND WHIDL.numWareHouseItemID = WareHouseItems.numWareHouseItemID AND numReturnHeaderID = v_numReturnHeaderID;
            end if;
         end if;
         PERFORM usp_ManageRMAInventory(v_numReturnHeaderID,v_numDomainId,v_numUserCntID,1::SMALLINT);
      end if;
      v_tintType :=(CASE When v_tintReturnType = 1 AND v_tintReceiveType = 1 THEN 5
      When v_tintReturnType = 1 AND v_tintReceiveType = 2 THEN 3
      When v_tintReturnType = 2 AND v_tintReceiveType = 2 THEN 4
      ELSE 0
      END);
      RAISE NOTICE '%',v_tintType;
      IF 	v_tintType > 0 then
	
         PERFORM USP_UpdateBizDocNameTemplate(v_tintType::SMALLINT,v_numDomainId,v_numReturnHeaderID);
      end if;
      v_numBizdocTempID := 0;
      IF v_tintReturnType = 1 OR v_tintReturnType = 2 then
    
         IF v_tintReceiveType = 2 AND v_tintReturnType = 1 then
		
            select   numBizDocTempID INTO v_numBizdocTempID FROM    BizDocTemplate WHERE   numOppType = 1 AND bitDefault = true AND bitEnabled = true
            AND numDomainID = v_numDomainId AND numBizDocID IN(SELECT  numListItemID
               FROM    Listdetails WHERE   vcData = 'Sales Credit Memo' AND constFlag = true LIMIT 1)    LIMIT 1;
         ELSEIF v_tintReturnType = 1 OR v_tintReceiveType = 1
         then
        
            select   numBizDocTempID INTO v_numBizdocTempID FROM BizDocTemplate WHERE   numOppType = 1 AND bitDefault = true AND bitEnabled = true AND numDomainID = v_numDomainId
            AND numBizDocID =(SELECT  numListItemID
               FROM    Listdetails WHERE   vcData = 'Refund Receipt' AND constFlag = true LIMIT 1)    LIMIT 1;
         ELSEIF v_tintReceiveType = 2 AND v_tintReturnType = 2
         then
        
            select   numBizDocTempID INTO v_numBizdocTempID FROM    BizDocTemplate WHERE   numOppType = 2 AND bitDefault = true AND bitEnabled = true
            AND numDomainID = v_numDomainId AND numBizDocID IN(SELECT  numListItemID
               FROM    Listdetails WHERE   vcData = 'Purchase Credit Memo' AND constFlag = true LIMIT 1)    LIMIT 1;
         end if;
      end if;
      v_tintType := CASE When v_tintReturnType = 1 AND v_tintReceiveType = 1 THEN 9
      When v_tintReturnType = 1 AND v_tintReceiveType = 2 THEN 7
      When v_tintReturnType = 1 AND v_tintReceiveType = 3 THEN 9
      When v_tintReturnType = 2 AND v_tintReceiveType = 2 THEN 8
      When v_tintReturnType = 3 THEN 10
      When v_tintReturnType = 4 THEN 9
      END;
      select   monAmount, monTotalTax, monTotalDiscount INTO v_monAmount,v_monTotalTax,v_monTotalDiscount FROM GetReturnDealAmount(v_numReturnHeaderID,v_tintType);
      UPDATE  ReturnHeader
      SET     tintReceiveType = v_tintReceiveType,numReturnStatus = v_numReturnStatus,
      numModifiedBy = v_numUserCntID,dtModifiedDate = TIMEZONE('UTC',now()),
      numAccountID = v_numAccountID,vcCheckNumber = v_vcCheckNumber,IsCreateRefundReceipt = v_IsCreateRefundReceipt,
      numBizdocTempID = CASE When v_numBizdocTempID > 0 THEN v_numBizdocTempID ELSE numBizdocTempID END,monBizDocAmount = v_monAmount+v_monTotalTax -v_monTotalDiscount,
      numItemCode = v_numItemCode
      WHERE   numReturnHeaderID = v_numReturnHeaderID;
      select   coalesce(numDepositIDRef,0) INTO v_numDepositIDRef FROM ReturnHeader AS RH WHERE RH.numReturnHeaderID = v_numReturnHeaderID;
      RAISE NOTICE '%',v_numDepositIDRef;
      IF v_numDepositIDRef > 0 then
	
         IF NOT EXISTS(SELECT * FROM DepositeDetails AS DD WHERE DD.numDepositID = v_numDepositIDRef) AND v_tintReceiveType = 1 AND v_tintReturnType = 4 then
		
            UPDATE DepositMaster SET monAppliedAmount = coalesce((SELECT SUM(ReturnHeader.monAmount) FROM ReturnHeader
            WHERE ReturnHeader.numDepositIDRef = v_numDepositIDRef),0),numReturnHeaderID = v_numReturnHeaderID,monRefundAmount = monDepositAmount -coalesce((SELECT SUM(ReturnHeader.monAmount) FROM ReturnHeader WHERE ReturnHeader.numDepositIDRef = v_numDepositIDRef),0)
            WHERE numDepositId = v_numDepositIDRef;
         ELSE
            UPDATE DepositMaster SET monAppliedAmount = v_monAmount+coalesce((SELECT SUM(DD.monAmountPaid) FROM DepositeDetails AS DD WHERE DD.numDepositID = v_numDepositIDRef),0),monRefundAmount = monDepositAmount -coalesce((SELECT SUM(DD.monAmountPaid) FROM DepositeDetails AS DD WHERE DD.numDepositID = v_numDepositIDRef),0)
            WHERE numDepositId = v_numDepositIDRef;
         end if;
      end if;
      -- COMMIT
EXCEPTION WHEN OTHERS THEN
     GET STACKED DIAGNOSTICS
			  my_ex_state   = RETURNED_SQLSTATE,
			  my_ex_message = MESSAGE_TEXT,
			  my_ex_detail  = PG_EXCEPTION_DETAIL,
			  my_ex_hint    = PG_EXCEPTION_HINT,
			  my_ex_ctx     = PG_EXCEPTION_CONTEXT;

			 raise exception '% % % % %', my_ex_state, my_ex_message, my_ex_detail, my_ex_hint, my_ex_ctx;
   END;
   RETURN;
END; $$;


