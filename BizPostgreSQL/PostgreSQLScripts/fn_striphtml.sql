-- Function definition script fn_StripHTML for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021

CREATE OR REPLACE FUNCTION fn_StripHTML(v_HTMLText TEXT)
RETURNS TEXT LANGUAGE plpgsql
   AS $$
   DECLARE
   v_Start  INTEGER;
   v_End  INTEGER;
   v_Length  INTEGER;
BEGIN
   v_Start := POSITION('<!--' IN v_HTMLText);
   v_End := CASE WHEN POSITION('-->' IN SUBSTR(v_HTMLText,POSITION('<!--' IN v_HTMLText))) > 0 THEN POSITION('-->' IN SUBSTR(v_HTMLText,POSITION('<!--' IN v_HTMLText)))+POSITION('<!--' IN v_HTMLText) -1 ELSE 0 END;
   v_Length :=(v_End::bigint -v_Start::bigint)+3;
   WHILE v_Start > 0 AND v_End > 0 AND v_Length > 0 LOOP
      v_HTMLText := OVERLAY(v_HTMLText placing '' from v_Start for v_Length);
      v_Start := POSITION('<!--' IN v_HTMLText);
      v_End := CASE WHEN POSITION('-->' IN SUBSTR(v_HTMLText,POSITION('<!--' IN v_HTMLText))) > 0 THEN POSITION('-->' IN SUBSTR(v_HTMLText,POSITION('<!--' IN v_HTMLText)))+POSITION('<!--' IN v_HTMLText) -1 ELSE 0 END;
      v_Length :=(v_End::bigint -v_Start::bigint)+3;
   END LOOP;

   v_Start := POSITION('<' IN v_HTMLText);
   v_End := CASE WHEN POSITION('>' IN SUBSTR(v_HTMLText,POSITION('<' IN v_HTMLText))) > 0 THEN POSITION('>' IN SUBSTR(v_HTMLText,POSITION('<' IN v_HTMLText)))+POSITION('<' IN v_HTMLText) -1 ELSE 0 END;
   v_Length :=(v_End::bigint -v_Start::bigint)+1;
   WHILE v_Start > 0 AND v_End > 0 AND v_Length > 0 LOOP
      v_HTMLText := OVERLAY(v_HTMLText placing '' from v_Start for v_Length);
      v_Start := POSITION('<' IN v_HTMLText);
      v_End := CASE WHEN POSITION('>' IN SUBSTR(v_HTMLText,POSITION('<' IN v_HTMLText))) > 0 THEN POSITION('>' IN SUBSTR(v_HTMLText,POSITION('<' IN v_HTMLText)))+POSITION('<' IN v_HTMLText) -1 ELSE 0 END;
      v_Length :=(v_End::bigint -v_Start::bigint)+1;
   END LOOP;

   RETURN LTRIM(RTRIM(REPLACE(v_HTMLText,'&nbsp;','')));
END; $$;

