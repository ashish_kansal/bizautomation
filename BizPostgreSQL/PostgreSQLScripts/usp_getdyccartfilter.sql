-- Stored procedure definition script usp_GetDycCartFilter for PostgreSQL
CREATE OR REPLACE FUNCTION usp_GetDycCartFilter(v_numFormID NUMERIC(18,0) DEFAULT 0,
	v_numSiteID NUMERIC(18,0) DEFAULT 0,
	v_numDomainID NUMERIC(18,0)  DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT 	coalesce(DCF.numFieldID,0) AS numFieldID,
		coalesce(DCF.numFormID,0) AS numFormID,
		coalesce(DCF.numSiteID,0) AS numSiteID,
		coalesce(DCF.numDomainID,0) AS numDomainID,
		coalesce(DCF.numFilterType,0) AS numFilterType,
		coalesce(DCF.bitCustomField,false) AS bitCustomField,
		coalesce(DCF.tintOrder,0) AS tintOrder,
		coalesce(vcSiteName,'') AS vcSiteName,
		CASE WHEN coalesce(DCF.bitCustomField,false) = false THEN coalesce((SELECT vcFieldName FROM DycFieldMaster DM WHERE DCF.numFieldID = DM.numFieldID),'')
   WHEN coalesce(DCF.bitCustomField,false) = true THEN coalesce((SELECT CFM.FLd_label FROM CFW_Fld_Master CFM WHERE CFM.Fld_id =  DCF.numFieldID),'')
   END AS vcFieldName
   FROM DycCartFilters DCF
   LEFT JOIN Sites S ON DCF.numSiteID = S.numSiteID AND DCF.numDomainID = S.numDomainID
   WHERE (numFormID = v_numFormID  OR  v_numFormID = 0)
   AND (DCF.numSiteID = v_numSiteID OR v_numSiteID = 0)
   AND (DCF.numDomainID = v_numDomainID OR v_numDomainID = 0)
   ORDER BY bitCustomField;
END; $$; 













