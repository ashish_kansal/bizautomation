-- Stored procedure definition script USP_ReportListMaster_Top10ReasonsDealWins for PostgreSQL
CREATE OR REPLACE FUNCTION USP_ReportListMaster_Top10ReasonsDealWins(v_numDomainID NUMERIC(18,0)
	,v_ClientTimeZoneOffset INTEGER
	,v_tintOppType NUMERIC(18,0)
	,v_vcDealAmount TEXT,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_TotalDealWon  DOUBLE PRECISION DEFAULT 0.0;
BEGIN
   select   COUNT(numOppId) INTO v_TotalDealWon FROM
   OpportunityMaster WHERE
   numDomainId = v_numDomainID
   AND tintopptype =(v_tintOppType)
   AND tintoppstatus = 1
   AND bintOppToOrder IS NOT NULL;
	
   open SWV_RefCur for SELECT 
   Reason
		,(NoOfOppToOrder::bigint*100)/(CASE WHEN coalesce(v_TotalDealWon,0) = 0 THEN 1 ELSE v_TotalDealWon END) AS TotalDealWonPercent
   FROM(SELECT
      coalesce(Listdetails.vcData,'-') AS Reason
			,COUNT(numOppId) AS NoOfOppToOrder
      FROM
      OpportunityMaster
      LEFT JOIN
      Listdetails
      ON
      OpportunityMaster.lngPConclAnalysis = Listdetails.numListItemID
      AND Listdetails.numListID = 12
      AND (Listdetails.numDomainid = v_numDomainID OR coalesce(Listdetails.constFlag,false) = true)
      WHERE
      OpportunityMaster.numDomainId = v_numDomainID
      AND tintopptype =(v_tintOppType)
      AND tintoppstatus = 1
      AND bintOppToOrder IS NOT NULL
      AND (1 =(CASE WHEN POSITION('1-5K' IN v_vcDealAmount) > 0 THEN(CASE WHEN OpportunityMaster.monDealAmount BETWEEN 1000:: DECIMAL(20,5) AND 5000:: DECIMAL(20,5) THEN 1 ELSE 0 END) ELSE 0 END)
      OR 1 =(CASE WHEN POSITION('5-10K' IN v_vcDealAmount) > 0 THEN(CASE WHEN OpportunityMaster.monDealAmount BETWEEN 5000:: DECIMAL(20,5) AND 10000:: DECIMAL(20,5) THEN 1 ELSE 0 END) ELSE 0 END)
      OR 1 =(CASE WHEN POSITION('10-20K' IN v_vcDealAmount) > 0 THEN(CASE WHEN OpportunityMaster.monDealAmount BETWEEN 10000:: DECIMAL(20,5) AND 20000:: DECIMAL(20,5) THEN 1 ELSE 0 END) ELSE 0 END)
      OR 1 =(CASE WHEN POSITION('20-50K' IN v_vcDealAmount) > 0 THEN(CASE WHEN OpportunityMaster.monDealAmount BETWEEN 20000:: DECIMAL(20,5) AND 50000:: DECIMAL(20,5) THEN 1 ELSE 0 END) ELSE 0 END)
      OR 1 =(CASE WHEN POSITION('>50K' IN v_vcDealAmount) > 0 THEN(CASE WHEN OpportunityMaster.monDealAmount > 50000 THEN 1 ELSE 0 END) ELSE 0 END))
      GROUP BY
      Listdetails.vcData) TEMP
   ORDER BY
   NoOfOppToOrder DESC LIMIT 10;
END; $$;












