DROP FUNCTION IF EXISTS USP_UserMaster_UpdateActiveUsers;

CREATE OR REPLACE FUNCTION USP_UserMaster_UpdateActiveUsers(v_numDomainID NUMERIC(18,0)
	,v_vcUsers TEXT)
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numNewFullUsers  INTEGER;
   v_numNewLimitedAccessUsers  INTEGER;
BEGIN
   IF coalesce(v_vcUsers,'') <> '' then
      DROP TABLE IF EXISTS tt_TEMPUpdateActiveUsers CASCADE;
      CREATE TEMPORARY TABLE tt_TEMPUpdateActiveUsers
      (
         numUserID NUMERIC(18,0),
         bitActivateFlag BOOLEAN
      );
      INSERT INTO tt_TEMPUpdateActiveUsers(numUserID,
			bitActivateFlag)
      SELECT
      numUserID,
			bitActivateFlag
      FROM
	  XMLTABLE
		(
			'NewDataSet/Users'
			PASSING 
				CAST(v_vcUsers AS XML)
			COLUMNS
				id FOR ORDINALITY,
				numUserID NUMERIC(18,0) PATH 'numUserID',
				bitActivateFlag BOOLEAN PATH 'bitActivateFlag'
		) X;

      v_numNewFullUsers := coalesce((SELECT
      COUNT(*)
      FROM
      UserMaster
      INNER JOIN
      AuthenticationGroupMaster
      ON
      UserMaster.numGroupID = AuthenticationGroupMaster.numGroupID
      LEFT JOIN
      tt_TEMPUpdateActiveUsers TEMP
      ON
      UserMaster.numUserId = TEMP.numUserID
      WHERE
      UserMaster.numDomainID = v_numDomainID
      AND AuthenticationGroupMaster.tintGroupType = 1
      AND(CASE WHEN coalesce(TEMP.numUserID,0) > 0 THEN coalesce(TEMP.bitActivateFlag,false) ELSE coalesce(UserMaster.bitactivateflag,false) END) = true),0);
      v_numNewLimitedAccessUsers := coalesce((SELECT
      COUNT(*)
      FROM
      UserMaster
      INNER JOIN
      AuthenticationGroupMaster
      ON
      UserMaster.numGroupID = AuthenticationGroupMaster.numGroupID
      LEFT JOIN
      tt_TEMPUpdateActiveUsers TEMP
      ON
      UserMaster.numUserId = TEMP.numUserID
      WHERE
      UserMaster.numDomainID = v_numDomainID
      AND AuthenticationGroupMaster.tintGroupType = 4
      AND(CASE WHEN coalesce(TEMP.numUserID,0) > 0 THEN coalesce(TEMP.bitActivateFlag,false) ELSE coalesce(UserMaster.bitactivateflag,false) END) = true),0);
      IF EXISTS(SELECT numSubscriberID FROM Subscribers WHERE numTargetDomainID = v_numDomainID AND v_numNewFullUsers > coalesce(intNoofUsersSubscribed,0)) then
		
         RAISE EXCEPTION 'FULL_USERS_EXCEED';
      ELSEIF EXISTS(SELECT numSubscriberID FROM Subscribers WHERE numTargetDomainID = v_numDomainID AND v_numNewLimitedAccessUsers > coalesce(intNoofLimitedAccessUsers,0))
      then
		
         RAISE EXCEPTION 'LIMITED_ACCESS_USERS_EXCEED';
      ELSE
         UPDATE
         UserMaster UM
         SET
         bitActivateFlag = T.bitActivateFlag
         FROM
         tt_TEMPUpdateActiveUsers T
         WHERE
         UM.numUserId = T.numUserID AND UM.numDomainID = v_numDomainID;
      end if;
   end if;
   RETURN;
END; $$;


