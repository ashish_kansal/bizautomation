-- Stored procedure definition script USP_ManageElectiveItemFields for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ManageElectiveItemFields(v_numDomainID NUMERIC(9,0),
v_vcElectiveItemFields VARCHAR(500) DEFAULT '')
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   UPDATE Domain SET vcElectiveItemFields = v_vcElectiveItemFields WHERE numDomainId = v_numDomainID;
   RETURN;
END; $$;


