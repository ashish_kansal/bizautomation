-- Stored procedure definition script USP_ManageWorkOrderStatus for PostgreSQL
CREATE OR REPLACE FUNCTION USP_ManageWorkOrderStatus(v_numWOId NUMERIC(9,0) DEFAULT 0,
v_numWOStatus NUMERIC(9,0) DEFAULT NULL,
v_numUserCntID NUMERIC(9,0) DEFAULT 0,
v_numDomainID NUMERIC(18,0) DEFAULT NULL)
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numDomain  NUMERIC(18,0);
   v_CurrentWorkOrderStatus  NUMERIC(18,0);
   my_ex_state TEXT;
	my_ex_message TEXT;
	my_ex_detail TEXT;
	my_ex_hint TEXT;
	my_ex_ctx TEXT;
   v_PendingChildWO  NUMERIC(18,0);
   v_numWareHouseItemID  NUMERIC(9,0);
   v_numQtyItemsReq  DOUBLE PRECISION;
   v_numQtyBuilt  DOUBLE PRECISION;
   v_numOppId  NUMERIC(9,0);
   v_numOppItemID  NUMERIC(18,0);
BEGIN
   BEGIN
      -- BEGIN TRANSACTION
v_numDomain := v_numDomainID;

	--GET CURRENT STATUS OF WORK ORDER
      select   numWOStatus INTO v_CurrentWorkOrderStatus FROM WorkOrder WHERE numWOId = v_numWOId AND numDomainId = v_numDomainID;
      IF coalesce(v_CurrentWorkOrderStatus,0) = 23184 AND v_numWOStatus = 23184 then
	
         RAISE EXCEPTION 'WORKORDER_IS_ALREADY_COMPLETED';
         RETURN;
      end if;

	--ASSIGN NEW STATUS TO WORK ORDER
      UPDATE WorkOrder SET numWOStatus = v_numWOStatus WHERE numWOId = v_numWOId AND numDomainId = v_numDomainID AND numWOStatus <> 23184; 

	--IF CURRENT WORK ORDER STATUS IS NOT COMPLETED AND NEW WORK ORDER STATUS IS COMPLETED THEN MAKE INVENTORY CHANGES
      IF coalesce(v_CurrentWorkOrderStatus,0) <> 23184 AND v_numWOStatus = 23184 then
	
         with recursive CTE(numWOID) AS(SELECT
         cast(numWOId as BIGINT) AS numWOID
         FROM
         WorkOrder
         WHERE
         numParentWOID = v_numWOId
         AND numWOStatus <> 23184
         UNION ALL
         SELECT
         cast(WorkOrder.numWOId as BIGINT) AS numWOID
         FROM
         WorkOrder
         INNER JOIN
         CTE c
         ON
         WorkOrder.numParentWOID = c.numWOID
         WHERE
         WorkOrder.numWOStatus <> 23184)
         select   COUNT(*) INTO v_PendingChildWO FROM CTE;

		--WE HAVE IMPLEMENTED CODE IN SUCH WAY THAT FIRST CHILD WORK ORDER GET COMPLETED
		--SO IF SOME REASON CHILD WORK ORDER IS NOT COMPLETED THAN RAISERROR
         IF coalesce(v_PendingChildWO,0) > 0 then
		
            RAISE EXCEPTION 'CHILD WORK ORDER STILL NOT COMPLETED';
         end if;
         IF EXISTS(SELECT
         WorkOrderDetails.numWODetailId
         FROM
         WorkOrderDetails
         LEFT JOIN LATERAL(SELECT
            SUM(numPickedQty) AS numPickedQty
            FROM
            WorkOrderPickedItems
            WHERE
            WorkOrderPickedItems.numWODetailId = WorkOrderDetails.numWODetailId) TEMP on TRUE
         WHERE
         WorkOrderDetails.numWOId = v_numWOId
         AND coalesce(numWareHouseItemId,0) > 0
         AND coalesce(WorkOrderDetails.numQtyItemsReq,0) <> coalesce(TEMP.numPickedQty,0)) then
		
            RAISE EXCEPTION 'BOM_LEFT_TO_BE_PICKED';
            RETURN;
         end if;
         select   numWareHouseItemId, numQtyItemsReq, coalesce(numQtyBuilt,0), coalesce(numOppId,0) INTO v_numWareHouseItemID,v_numQtyItemsReq,v_numQtyBuilt,v_numOppId FROM
         WorkOrder WHERE
         numWOId = v_numWOId
         AND numDomainId = v_numDomainID;
         UPDATE WorkOrder SET bintCompletedDate = LOCALTIMESTAMP,numQtyBuilt = numQtyItemsReq WHERE numWOId = v_numWOId AND numDomainId = v_numDomainID;
      end if;
      -- COMMIT
EXCEPTION WHEN OTHERS THEN
         -- ROLLBACK TRANSACTION
  GET STACKED DIAGNOSTICS
			  my_ex_state   = RETURNED_SQLSTATE,
			  my_ex_message = MESSAGE_TEXT,
			  my_ex_detail  = PG_EXCEPTION_DETAIL,
			  my_ex_hint    = PG_EXCEPTION_HINT,
			  my_ex_ctx     = PG_EXCEPTION_CONTEXT;

			 raise exception '% % % % %', my_ex_state, my_ex_message, my_ex_detail, my_ex_hint, my_ex_ctx;
   END;
   RETURN;
END; $$;












