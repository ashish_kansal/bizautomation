CREATE OR REPLACE FUNCTION Get_Customers(v_PageIndex INTEGER,
v_PageSize INTEGER DEFAULT 10, INOUT SWV_RefCur refcursor DEFAULT null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_start  INTEGER;
   v_end  INTEGER;
BEGIN
   v_start :=(v_PageIndex::bigint*10)+1;
   v_end := v_start::bigint+v_PageSize::bigint -1;
   open SWV_RefCur for SELECT * FROM Customers WHERE CustomerID BETWEEN v_start AND v_end;
END; $$;






