CREATE OR REPLACE FUNCTION USP_PerAnysCompanyList
(
	v_numUserCntID NUMERIC,                                           
	v_RepType SMALLINT,                                        
	v_Condition CHAR(1),                                         
	v_numStartDate TIMESTAMP,                                          
	v_numEndDate TIMESTAMP,                                          
	v_CurrentPage INTEGER,                                          
	v_PageSize INTEGER,                                          
	INOUT v_TotRecs INTEGER ,                                          
	v_columnName VARCHAR(50),                                          
	v_columnSortOrder VARCHAR(10),                                        
	v_numDomainId NUMERIC(9,0),                                  
	v_strUserIDs VARCHAR(1000),                      
	v_numTeamType SMALLINT,
	INOUT SWV_RefCur refcursor
)
LANGUAGE plpgsql
AS $$
	DECLARE
	v_strSql TEXT;
	v_firstRec INTEGER;                                          
	v_lastRec INTEGER;
BEGIN
	if v_strUserIDs = '' then 
		v_strUserIDs := '0';
	end if;                                        
                                  
	if v_RepType <> 3 then                                        
		BEGIN
			CREATE TEMP SEQUENCE tt_tempTable_seq;
			EXCEPTION WHEN OTHERS THEN
			NULL;
		END;
		drop table IF EXISTS tt_TEMPTABLE CASCADE;
		Create TEMPORARY TABLE tt_TEMPTABLE 
		( 
			ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1) PRIMARY KEY,
			CompanyName VARCHAR(255),
			numTerID VARCHAR(15),
			PrimaryContact VARCHAR(110),
			Phone VARCHAR(25),
			vcEmail VARCHAR(50),
			numCompanyID VARCHAR(15),
			numDivisionID VARCHAR(15),
			numcontactid VARCHAR(15),
			numCompanyRating VARCHAR(15),
			vcRating VARCHAR(50),
			numStatusID  VARCHAR(15),
			vcStatus VARCHAR(50),
			numcreatedby VARCHAR(15),
			numRecOwner VARCHAR(15)
		);
		v_strSql := 'SELECT 
						CMP.vcCompanyName || '' - <I>'' || DM.vcDivisionName || ''</I>'' as CompanyName,                                             
						DM.numTerID,                                            
						ADC.vcLastName || '' '' || ADC.vcFirstName as PrimaryContact,
						case when ADC.numPhone <> '''' then ADC.numPhone || case when ADC.numPhoneExtension <> '''' then '' - '' || ADC.numPhoneExtension else '''' end  else '''' end as Phone,                                            
						ADC.vcEmail As vcEmail,                                            
						CMP.numCompanyId AS numCompanyID,                                            
						DM.numDivisionID As numDivisionID,                                            
						ADC.numContactId AS numContactID,                                             
						CMP.numCompanyRating AS numCompanyRating,                                             
						LD.vcData as vcRating,                                              
						DM.numStatusID AS numStatusID,                                             
						LD1.vcData as vcStatus,                                            
						DM.numCreatedBy AS numCreatedby, DM.numRecOwner                                           
					FROM  CompanyInfo CMP                                          
					join DivisionMaster DM on DM.numCompanyID = CMP.numCompanyId                              
					join AdditionalContactsInformation ADC on ADC.numDivisionId = DM.numDivisionID                    
					left join ListDetails LD on LD.numListItemID = CMP.numCompanyRating                                           
					left join ListDetails LD1 on LD1.numListItemID = DM.numStatusID                                         
					WHERE coalesce(ADC.bitPrimaryContact,false) = true ';                                        
                                        
		-- No of Leads prmoted to Prospects                                        
		if v_Condition = 'L' and v_RepType = 1 then  
			v_strSql := coalesce(v_strSql,'') || ' 
						AND DM.tintCRMType= 1 
						and bintLeadProm between ''' || v_numStartDate || ''' and ''' || v_numEndDate || '''                                             
						and bintLeadPromBy in (select numContactId from AdditionalContactsInformation where numTeam in(select numTeam from ForReportsByTeam where numUserCntID=' || COALESCE(v_numUserCntID,0) || ' and numDomainID=' || COALESCE(v_numDomainId,0) || ' and tintType=' || COALESCE(v_numTeamType,0) || ')) 
						ORDER BY ' || coalesce(v_columnName,'') || ' ' || coalesce(v_columnSortOrder,'');
		end if;
		if v_Condition = 'L' and v_RepType = 2 then  
			v_strSql := coalesce(v_strSql,'') || '
							AND DM.tintCRMType= 1 
							and bintLeadProm between ''' || v_numStartDate || ''' and ''' || v_numEndDate || '''                                             
							and bintLeadPromBy =' || COALESCE(v_numDomainId,0) || ' 
							ORDER BY ' || coalesce(v_columnName,'') || ' ' || coalesce(v_columnSortOrder,'');
		end if;                                        
                                        
		-- Total Prospects Added                                        
		if v_Condition = 'P' and v_RepType = 1 then  
			v_strSql := coalesce(v_strSql,'') || '
						and DM.tintCRMType=1 
						and ((DM.bintCreatedDate between ''' || v_numStartDate || ''' and ''' || v_numEndDate || ''' 
						and DM.numCreatedBy in (select numContactId from AdditionalContactsInformation where numTeam in (select numTeam from ForReportsByTeam where numUserCntID=' || COALESCE(v_numUserCntID,0) || ' and numDomainID=' || COALESCE(v_numDomainId,0) || ' and tintType=' || COALESCE(v_numTeamType,0) || '))) 
						or (bintLeadProm between ''' || v_numStartDate || ''' and ''' || v_numEndDate || ''' and bintLeadPromBy in (select numContactId from AdditionalContactsInformation where numTeam in(select numTeam from ForReportsByTeam where numUserCntID=' || COALESCE(v_numUserCntID,0) || ' and numDomainID=' || COALESCE(v_numDomainId,0) || ' and tintType=' || COALESCE(v_numTeamType,0) || ')))) 
						ORDER BY ' || coalesce(v_columnName,'') || ' ' || coalesce(v_columnSortOrder,'');
		end if;
		if v_Condition = 'P' and v_RepType = 2 then  
			v_strSql := coalesce(v_strSql,'') || ' 
						and DM.tintCRMType=1 
						and ((DM.bintCreatedDate between ''' || v_numStartDate || ''' and ''' || v_numEndDate || '''  and DM.numCreatedBy =' || COALESCE(v_numUserCntID,0) || ')
							or (bintLeadProm between ''' || v_numStartDate || ''' and ''' || v_numEndDate || ''' and bintLeadPromBy =' || v_numUserCntID || '))  
						ORDER BY ' || coalesce(v_columnName,'') || ' ' || coalesce(v_columnSortOrder,'');
		end if;                                        
        
		-- Total Prospects Promoted to Accounts                                        
		if v_Condition = 'R' and v_RepType = 1 then  
			v_strSql := coalesce(v_strSql,'') || ' 
						and DM.tintCRMType=2 
						and bintProsProm between ''' || v_numStartDate || ''' and ''' || v_numEndDate || ''' 
						and bintProsPromBy in (select numContactId from AdditionalContactsInformation where numTeam in(select numTeam from ForReportsByTeam where numUserCntID=' || COALESCE(v_numUserCntID,0) || ' and numDomainID=' || COALESCE(v_numDomainId,0) || ' and tintType=' || COALESCE(v_numTeamType,0) || '))  ORDER BY ' || coalesce(v_columnName,'') || ' ' || coalesce(v_columnSortOrder,'');
		end if;
		if v_Condition = 'R' and v_RepType = 2 then  
			v_strSql := coalesce(v_strSql,'') || ' 
						and DM.tintCRMType=2                                         
						and bintProsProm between ''' || v_numStartDate || ''' and ''' || v_numEndDate || ''' and bintProsPromBy =' || COALESCE(v_numUserCntID,0) || '  ORDER BY ' || coalesce(v_columnName,'') || ' ' || coalesce(v_columnSortOrder,'');
		end if;                                   
 
     
     
                            
-- Total Accounts Added                                        
      if v_Condition = 'A' and v_RepType = 1 then  
         v_strSql := coalesce(v_strSql,'') || ' and DM.tintCRMType=2 and ((DM.bintCreatedDate between ''' || v_numStartDate || ''' and ''' || v_numEndDate || '''                                         
and DM.numRecOwner in (select numContactId from AdditionalContactsInformation                                           
where numTeam in(select numTeam from ForReportsByTeam where numUserCntID=' || COALESCE(v_numUserCntID,0) || ' and numDomainID=' || COALESCE(v_numDomainId,0) || ' and tintType=' || COALESCE(v_numTeamType,0) || ')))                                   
  
    
or (bintProsProm between ''' || v_numStartDate || ''' and ''' || v_numEndDate || '''                                         
and bintProsPromBy in (select numContactId from AdditionalContactsInformation                                             
where numTeam in(select numTeam from ForReportsByTeam where numUserCntID=' || COALESCE(v_numUserCntID,0) || '                                         
and numDomainID=' || COALESCE(v_numDomainId,0) || ' and tintType=' || COALESCE(v_numTeamType,0) || '))))  ORDER BY ' || coalesce(v_columnName,'') || ' ' || coalesce(v_columnSortOrder,'');
      end if;
      if v_Condition = 'A' and v_RepType = 2 then  
         v_strSql := coalesce(v_strSql,'') || ' and DM.tintCRMType=2 and (DM.bintCreatedDate between ''' || v_numStartDate || ''' and ''' || v_numEndDate || '''                                         
and DM.numRecOwner =' || COALESCE(v_numUserCntID,0) || ')                                               
   or (bintProsProm between ''' || v_numStartDate || ''' and ''' || v_numEndDate || '''                                         
and bintProsPromBy =' || COALESCE(v_numUserCntID,0) || ')  ORDER BY ' || coalesce(v_columnName,'') || ' ' || coalesce(v_columnSortOrder,'');
      end if;
      RAISE NOTICE '%',v_strSql;
      EXECUTE 'insert into tt_TEMPTABLE (CompanyName,                                          
        numTerID,                                          
        PrimaryContact,                                          
        Phone,                                          
   vcEmail,                                          
   numCompanyID ,                                          
   numDivisionID,                                          
   numContactID,                                          
   numCompanyRating,                                          
   vcRating,                                          
   numStatusID,                                          
   vcStatus,                                          
   numCreatedby,                                          
   numRecOwner)                                          
' || v_strSql;
      v_firstRec :=(v_CurrentPage::bigint -1)*v_PageSize::bigint;
      v_lastRec :=(v_CurrentPage::bigint*v_PageSize::bigint+1);
      open SWV_RefCur for
      select * from tt_TEMPTABLE where ID > v_firstRec and ID < v_lastRec;
      select count(*) INTO v_TotRecs from tt_TEMPTABLE;
   end if;                                  
              
   if v_RepType = 3 then

      v_strSql := 'SELECT ADC1.numContactId,coalesce(vcFirstName,''-'') as vcFirstName,coalesce(vcLastName,''-'') as vcLastName,(SELECT  count(*)                                           
    FROM  CompanyInfo CMP                                
    join DivisionMaster DM on DM.numCompanyID=CMP.numCompanyID                                 
    join AdditionalContactsInformation ADC on ADC.numDivisionID=DM.numDivisionID WHERE coalesce(ADC.bitPrimaryContact,false)=true';                                        
                                        
-- No of Leads prmoted to Prospects                                        
      if v_Condition = 'L' then  
         v_strSql := coalesce(v_strSql,'') || '  and bintLeadPromBy=ADC1.numRecOwner AND DM.tintCRMType= 1                        
and  (bintLeadProm between ''' || v_numStartDate || ''' and ''' || v_numEndDate || ''' ) ) as NoofOpp FROM  AdditionalContactsInformation ADC1                                    
WHERE ADC1.numContactId in (' || coalesce(v_strUserIDs,'') || ')';
      end if;
      if v_Condition = 'P' then   
         v_strSql := coalesce(v_strSql,'') || 'and DM.tintCRMType=1 and ((DM.bintCreatedDate between ''' || v_numStartDate || '''                                         
and ''' || v_numEndDate || ''' and DM.numRecOwner =ADC1.numContactId) or (bintLeadProm between ''' || v_numStartDate || ''' and ''' || v_numEndDate || '''                          
 and bintLeadPromBy =ADC1.numContactId) )) as NoofOpp FROM  AdditionalContactsInformation ADC1                                     
WHERE  ADC1.numContactId in (' || coalesce(v_strUserIDs,'') || ')';
      end if;
      if v_Condition = 'R' then 
         v_strSql := coalesce(v_strSql,'') || ' and DM.tintCRMType=2                                         
and bintProsProm between ''' || v_numStartDate || ''' and ''' || v_numEndDate || ''' and bintProsPromBy =ADC1.numContactId) as NoofOpp FROM  AdditionalContactsInformation ADC1                                       
WHERE  ADC1.numContactId in (' || coalesce(v_strUserIDs,'') || ')';
      end if;
      if v_Condition = 'A' then  
         v_strSql := coalesce(v_strSql,'') || ' and DM.tintCRMType=2 and (DM.bintCreatedDate between ''' || v_numStartDate || ''' and ''' || v_numEndDate || '''                                        
and DM.numRecOwner =ADC1.numContactId )                                               
   or (bintProsProm between ''' || v_numStartDate || ''' and ''' || v_numEndDate || '''                                         
and bintProsPromBy  =ADC1.numContactId )) as NoofOpp FROM  AdditionalContactsInformation ADC1                                     
WHERE  ADC1.numContactId in (' || coalesce(v_strUserIDs,'') || ')';
      end if;
      RAISE NOTICE '%',(v_strSql);
      OPEN SWV_RefCur FOR EXECUTE v_strSql;
   end if;
   RETURN;
END; $$;


