-- Stored procedure definition script USP_CustomQueryReport_Execute for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_CustomQueryReport_Execute(v_numReportID NUMERIC(18,0))
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_vcQuery  TEXT;
BEGIN
   select   CAST(vcQuery AS TEXT) INTO v_vcQuery FROM CustomQueryReport WHERE numReportID = v_numReportID;

   IF LENGTH(v_vcQuery) > 0 then
		v_vcQuery := REPLACE(v_vcQuery,'@','v_');
		v_vcQuery := REPLACE(v_vcQuery,'dbo.','');
		v_vcQuery := REPLACE(v_vcQuery,'ISNULL','COALESCE');
      EXECUTE v_vcQuery;
   end if;
   RETURN;
END; $$;

