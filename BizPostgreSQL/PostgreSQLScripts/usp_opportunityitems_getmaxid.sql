-- Stored procedure definition script USP_OpportunityItems_GetMaxID for PostgreSQL
CREATE OR REPLACE FUNCTION USP_OpportunityItems_GetMaxID(INOUT SWV_RefCur refcursor)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT MAX(numoppitemtCode)+1 AS numOppItemID FROM OpportunityItems;
END; $$;












