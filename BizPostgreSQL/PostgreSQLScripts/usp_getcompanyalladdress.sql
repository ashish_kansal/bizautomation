-- FUNCTION: public.usp_getcompanyalladdress(numeric, numeric, refcursor)

-- DROP FUNCTION public.usp_getcompanyalladdress(numeric, numeric, refcursor);

CREATE OR REPLACE FUNCTION public.usp_getcompanyalladdress(
	v_numdomainid numeric,
	v_numdivisionid numeric,
	INOUT swv_refcur refcursor DEFAULT NULL::refcursor)
    RETURNS refcursor
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
AS $BODY$
BEGIN
-- This function was converted on Sat Apr 10 15:52:52 2021 using Ispirer SQLWays 9.0 Build 6532 64bit Licensed to BizAutomation - Sandeep Patel - India - Ispirer MnMTK 2020 MSSQLServer to PostgreSQL Database Migration Professional Project License (50 GB, 750 Tables, 426500 LOC, 3 Extension Requests, 1 Month, 20210508).
   BEGIN
      CREATE TEMP SEQUENCE tt_Temp_Location_seq;
      EXCEPTION WHEN OTHERS THEN
         NULL;
   END;
   drop table IF EXISTS tt_TEMP_LOCATION CASCADE;
   create TEMPORARY TABLE tt_TEMP_LOCATION
   (
      numLocId NUMERIC DEFAULT NEXTVAL('tt_Temp_Location_seq'),
      vcLocation VARCHAR(2000)
   );

   insert into tt_TEMP_LOCATION(vcLocation)
   select
   CAST(coalesce(AD1.vcStreet,'') || ',' ||
   coalesce(AD1.vcCity,'') || ',' ||
   coalesce(fn_GetState(AD1.numState),'') || ',' ||
   coalesce(AD1.vcPostalCode,'') || ',' ||
   coalesce(fn_GetListName(AD1.numCountry,0::BOOLEAN),'') AS VARCHAR(2000))  as Location
   from AddressDetails AD1 where numDomainID = v_numDomainID and numRecordID = v_numDivisionID AND tintAddressOf = 2
   UNION
   select
   CAST(coalesce(AD1.vcStreet,'') || ',' ||
   coalesce(AD1.vcCity,'') || ',' ||
   coalesce(fn_GetState(AD1.numState),'') || ',' ||
   coalesce(AD1.vcPostalCode,'') || ',' ||
   coalesce(fn_GetListName(AD1.numCountry,0::BOOLEAN),'') AS VARCHAR(2000))  as Location
   from AddressDetails AD1 where numDomainID = v_numDomainID and numRecordID = v_numDivisionID AND tintAddressOf = 1;

open SWV_RefCur for select numLocId,replace(vcLocation,',,','') as vcLocation  from tt_TEMP_LOCATION where LENGTH(vcLocation) > 10;
   
   RETURN;
END;
$BODY$;

ALTER FUNCTION public.usp_getcompanyalladdress(numeric, numeric, refcursor)
    OWNER TO postgres;
