CREATE OR REPLACE FUNCTION USP_GetProjectTeamContact(v_numProjectTeamId NUMERIC(9,0) DEFAULT 0,
    v_numDomainId NUMERIC(9,0) DEFAULT 0        
,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT PT.numProjectTeamId,
            PT.numContactId
   FROM    ProjectTeam PT
   JOIN AdditionalContactsInformation CO ON PT.numContactId = CO.numContactId
   WHERE   PT.numProjectTeamId = v_numProjectTeamId
   AND CO.numDomainID = v_numDomainId;
   RETURN;
END; $$;














