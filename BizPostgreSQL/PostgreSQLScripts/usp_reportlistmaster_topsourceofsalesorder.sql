-- Stored procedure definition script USP_ReportListMaster_TopSourceOfSalesOrder for PostgreSQL
CREATE OR REPLACE FUNCTION USP_ReportListMaster_TopSourceOfSalesOrder(v_numDomainID NUMERIC(18,0)
	,v_ClientTimeZoneOffset INTEGER
	,v_vcDealAmount TEXT,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_TotalSalesOrderCount  NUMERIC(18,4);
   v_i  INTEGER DEFAULT 1;
   v_iCount  INTEGER;
   v_tintSource  VARCHAR(100);
   v_bitPartner  BOOLEAN;
   v_numPartner  NUMERIC(18,0);
   v_TotalSalesOrderBySource  INTEGER;
   v_TotalSalesOrderAmountBySource  DECIMAL(20,5);
BEGIN
   v_vcDealAmount := coalesce(v_vcDealAmount,'');

   select DISTINCT  COUNT(*) INTO v_TotalSalesOrderCount FROM
   OpportunityMaster WHERE
   numDomainId = v_numDomainID
   AND tintopptype = 1
   AND tintoppstatus = 1
   AND bintCreatedDate >= TIMEZONE('UTC',now())+INTERVAL '-12 month';

   BEGIN
      CREATE TEMP SEQUENCE tt_TEMP_seq INCREMENT BY 1 START WITH 1;
      EXCEPTION WHEN OTHERS THEN
         NULL;
   END;
   DROP TABLE IF EXISTS tt_TEMP CASCADE;
   CREATE TEMPORARY TABLE tt_TEMP
   (
      ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1),
      tintSource VARCHAR(100),
      bitPartner BOOLEAN,
      numPartner NUMERIC(18,0),
      vcSource VARCHAR(300),
      TotalOrdersCount INTEGER,
      TotalOrdersPercent NUMERIC(18,2),
      TotalOrdersAmount DECIMAL(20,5)
   );

   INSERT INTO tt_TEMP(tintSource
		,vcSource
		,bitPartner
		,numPartner)
   SELECT
   CAST('0~0' AS VARCHAR(100))
		,CAST('-' AS VARCHAR(300))
		,CAST(0 AS BOOLEAN)
		,CAST(0 AS NUMERIC(18,0))
   UNION
   SELECT
   CAST('0~1' AS VARCHAR(100))
		,CAST('Internal Order' AS VARCHAR(300))
		,CAST(0 AS BOOLEAN)
		,CAST(0 AS NUMERIC(18,0))
   UNION
   SELECT
   CONCAT(numSiteID,'~2')
		,vcSiteName
		,CAST(0 AS BOOLEAN)
		,CAST(0 AS NUMERIC(18,0))
   FROM
   Sites
   WHERE
   numDomainID = v_numDomainID
   UNION
   SELECT DISTINCT
   CONCAT(WebApiId,'~3')
		,vcProviderName
		,CAST(0 AS BOOLEAN)
		,CAST(0 AS NUMERIC(18,0))
   FROM
   WebAPI
   UNION
   SELECT
   CONCAT(Ld.numListItemID,'~1')
		,coalesce(vcRenamedListName,vcData)
		,CAST(0 AS BOOLEAN)
		,CAST(0 AS NUMERIC(18,0))
   FROM
   Listdetails Ld
   LEFT JOIN
   listorder LO
   ON
   Ld.numListItemID = LO.numListItemID
   AND LO.numDomainId = v_numDomainID
   WHERE
   Ld.numListID = 9
   AND (constFlag = true OR Ld.numDomainid = v_numDomainID)
   UNION
   SELECT DISTINCT
   CAST('' AS VARCHAR(100))
		,CAST('' AS VARCHAR(300))
		,CAST(1 AS BOOLEAN)
		,numPartner
   FROM
   OpportunityMaster
   WHERE
   numDomainId = v_numDomainID
   AND tintopptype = 1
   AND tintoppstatus = 1
   AND coalesce(numPartner,0) > 0
   AND bintCreatedDate >= TIMEZONE('UTC',now())+INTERVAL '-12 month';

   select   COUNT(*) INTO v_iCount FROM tt_TEMP;

   WHILE v_i <= v_iCount LOOP
      select   coalesce(tintSource,''), bitPartner, numPartner INTO v_tintSource,v_bitPartner,v_numPartner FROM tt_TEMP WHERE ID = v_i;
      select   COUNT(numOppId) INTO v_TotalSalesOrderBySource FROM
      OpportunityMaster WHERE
      numDomainId = v_numDomainID
      AND tintopptype = 1
      AND tintoppstatus = 1
      AND 1 =(CASE
      WHEN v_bitPartner = true
      THEN CASE WHEN numPartner = v_numPartner THEN 1 ELSE 0 END
      ELSE CASE WHEN CONCAT(coalesce(tintSource,0),'~',coalesce(tintSourceType,0)) = v_tintSource AND coalesce(numPartner,0) = 0 THEN 1 ELSE 0 END
      END)
      AND bintCreatedDate >= TIMEZONE('UTC',now())+INTERVAL '-12 month';
      select   SUM(monTotAmount) INTO v_TotalSalesOrderAmountBySource FROM
      OpportunityItems OI
      INNER JOIN
      OpportunityMaster OM
      ON
      OI.numOppId = OM.numOppId
      INNER JOIN
      Item I
      ON
      OI.numItemCode = I.numItemCode WHERE
      OM.numDomainId = v_numDomainID
      AND I.numDomainID = v_numDomainID
      AND tintopptype = 1
      AND tintoppstatus = 1
      AND 1 =(CASE
      WHEN v_bitPartner = true
      THEN CASE WHEN numPartner = v_numPartner THEN 1 ELSE 0 END
      ELSE CASE WHEN CONCAT(coalesce(tintSource,0),'~',coalesce(tintSourceType,0)) = v_tintSource AND coalesce(numPartner,0) = 0 THEN 1 ELSE 0 END
      END)
      AND OM.bintCreatedDate >= TIMEZONE('UTC',now())+INTERVAL '-12 month'
			--
      AND (1 =(CASE WHEN POSITION('1-5K' IN v_vcDealAmount) > 0 THEN(CASE WHEN monDealAmount BETWEEN 1000:: DECIMAL(20,5) AND 5000:: DECIMAL(20,5) THEN 1 ELSE 0 END) ELSE 0 END)
      OR 1 =(CASE WHEN POSITION('5-10K' IN v_vcDealAmount) > 0 THEN(CASE WHEN monDealAmount BETWEEN 5000:: DECIMAL(20,5) AND 10000:: DECIMAL(20,5) THEN 1 ELSE 0 END) ELSE 0 END)
      OR 1 =(CASE WHEN POSITION('10-20K' IN v_vcDealAmount) > 0 THEN(CASE WHEN monDealAmount BETWEEN 10000:: DECIMAL(20,5) AND 20000:: DECIMAL(20,5) THEN 1 ELSE 0 END) ELSE 0 END)
      OR 1 =(CASE WHEN POSITION('20-50K' IN v_vcDealAmount) > 0 THEN(CASE WHEN monDealAmount BETWEEN 20000:: DECIMAL(20,5) AND 50000:: DECIMAL(20,5) THEN 1 ELSE 0 END) ELSE 0 END)
      OR 1 =(CASE WHEN POSITION('>50K' IN v_vcDealAmount) > 0 THEN(CASE WHEN monDealAmount > 50000 THEN 1 ELSE 0 END) ELSE 0 END));
      UPDATE
      tt_TEMP
      SET
      vcSource =(CASE
      WHEN v_bitPartner = true
      THEN coalesce((SELECT CI.vcCompanyName FROM DivisionMaster DM INNER JOIN CompanyInfo CI ON DM.numCompanyID = CI.numCompanyId WHERE DM.numDomainID = v_numDomainID AND numDivisionID = v_numPartner),'-')
      ELSE vcSource
      END),TotalOrdersPercent =(v_TotalSalesOrderBySource::bigint*100)/coalesce(v_TotalSalesOrderCount,1),
      TotalOrdersAmount = coalesce(v_TotalSalesOrderAmountBySource,0),
      TotalOrdersCount = v_TotalSalesOrderBySource
      WHERE
      ID = v_i;
      v_i := v_i::bigint+1;
   END LOOP;

   open SWV_RefCur for SELECT vcSource,TotalOrdersPercent,TotalOrdersAmount FROM tt_TEMP WHERE coalesce(TotalOrdersPercent,0) > 0 ORDER BY TotalOrdersPercent DESC;
END; $$;












