-- Stored procedure definition script USP_MassStockTransfer_GetLast20Transfer for PostgreSQL
CREATE OR REPLACE FUNCTION USP_MassStockTransfer_GetLast20Transfer(v_numDomainID NUMERIC(18,0)
	,v_ClientTimeZoneOffset INTEGER,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT 
   FormatedDateFromDate(dtTransferredDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval),v_numDomainID) AS dtTransferredDate
		,Item.numItemCode
		,Item.vcItemName
		,MassStockTransfer.numQty
		,CONCAT(WFrom.vcWareHouse,CASE WHEN WLFrom.numWLocationID IS NOT NULL THEN CONCAT(' (',WLFrom.vcLocation,')') ELSE '' END) AS vcFrom
		,CONCAT(WTo.vcWareHouse,CASE WHEN WLTo.numWLocationID IS NOT NULL THEN CONCAT(' (',WLTo.vcLocation,')') ELSE '' END) AS vcTo
		,coalesce(numFromQtyBefore,0) AS numFromQtyBefore
		,coalesce(numFromQtyAfter,0) AS numFromQtyAfter
		,coalesce(numToQtyBefore,0) AS numToQtyBefore
		,coalesce(numToQtyAfter,0) AS numToQtyAfter
   FROM
   MassStockTransfer
   INNER JOIN
   Item
   ON
   MassStockTransfer.numItemCode = Item.numItemCode
   INNER JOIN
   WareHouseItems WIFrom
   ON
   MassStockTransfer.numFromWarehouseItemID = WIFrom.numWareHouseItemID
   INNER JOIN
   Warehouses WFrom
   ON
   WIFrom.numWareHouseID = WFrom.numWareHouseID
   LEFT JOIN
   WarehouseLocation WLFrom
   ON
   WIFrom.numWLocationID = WLFrom.numWLocationID
   INNER JOIN
   WareHouseItems WITo
   ON
   MassStockTransfer.numToWarehouseItemID = WITo.numWareHouseItemID
   INNER JOIN
   Warehouses WTo
   ON
   WITo.numWareHouseID = WTo.numWareHouseID
   LEFT JOIN
   WarehouseLocation WLTo
   ON
   WITo.numWLocationID = WLTo.numWLocationID
   WHERE
   MassStockTransfer.numDomainId = v_numDomainID
   ORDER BY
   ID DESC LIMIT 20;
END; $$;












