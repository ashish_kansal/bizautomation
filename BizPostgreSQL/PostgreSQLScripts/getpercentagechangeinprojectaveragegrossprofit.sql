-- Function definition script GetPercentageChangeInProjectAverageGrossProfit for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION GetPercentageChangeInProjectAverageGrossProfit(v_numDomainID NUMERIC(18,0),
	 v_dtStartDate DATE,
	 v_dtEndDate DATE)
RETURNS NUMERIC(18,2) LANGUAGE plpgsql
   AS $$
   DECLARE
   v_ProjectGrossProfitChange  NUMERIC(18,2) DEFAULT 0.00;
   v_ProjectGrossProfit1  INTEGER;
   v_ProjectGrossProfit2  INTEGER;
BEGIN
   select   AVG(GrossProfit) INTO v_ProjectGrossProfit1 FROM(SELECT
      GetProjectGrossProfit(v_numDomainID,numProId) AS GrossProfit
      FROM
      ProjectsMaster
      WHERE
      numdomainId = v_numDomainID
      AND bintCreatedDate BETWEEN v_dtStartDate AND v_dtEndDate) TEMP;

   select   AVG(GrossProfit) INTO v_ProjectGrossProfit2 FROM(SELECT
      GetProjectGrossProfit(v_numDomainID,numProId) AS GrossProfit
      FROM
      ProjectsMaster
      WHERE
      numdomainId = v_numDomainID
      AND bintCreatedDate BETWEEN v_dtStartDate+INTERVAL '-1 year' AND v_dtEndDate+INTERVAL '-1 year') TEMP;


   v_ProjectGrossProfitChange := 100.0*(coalesce(v_ProjectGrossProfit1,0) -coalesce(v_ProjectGrossProfit2,0))/(CASE WHEN coalesce(v_ProjectGrossProfit2,0) = 0 THEN 1 ELSE coalesce(v_ProjectGrossProfit2,0) END);


   RETURN coalesce(v_ProjectGrossProfitChange,0.00);
END; $$;

