-- Stored procedure definition script usp_UpdateOpptAlertRules for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION usp_UpdateOpptAlertRules(v_numStagePercentageId NUMERIC(18,0) DEFAULT 0,
	v_monAmt DECIMAL(20,5) DEFAULT 0,
	v_vcCondn VARCHAR(10) DEFAULT '',
	v_numNoOfDays NUMERIC(18,0) DEFAULT 0,
	v_numModifiedBy NUMERIC(18,0) DEFAULT 0,
	v_bintModifiedDate BIGINT DEFAULT 0,
	v_numRuleId NUMERIC DEFAULT 0,
	v_numEmpId NUMERIC DEFAULT 0,
	v_numDetsFlag NUMERIC DEFAULT 0   
--
)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   IF v_numDetsFlag = 0 then
	
      UPDATE OpptRuleMaster
      SET numStagePercentageID = v_numStagePercentageId,monAmt = v_monAmt,vcCondn = v_vcCondn,
      numNoOfDays = v_numNoOfDays,numModifiedBy = v_numModifiedBy,bintModifiedDate = v_bintModifiedDate
      WHERE numruleid = v_numRuleId;
   ELSE
      UPDATE RuleDetails SET
      numEmpId = v_numEmpId
      WHERE numRuleId = v_numRuleId;
   end if;
   RETURN;
END; $$;


