-- Stored procedure definition script usp_UpdateUserGroups for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION usp_UpdateUserGroups(v_numGroupID NUMERIC, 
	v_numUserID NUMERIC   
--
)
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numRecCount  NUMERIC;
BEGIN
   select   COUNT(numGroupID) INTO v_numRecCount FROM UserGroups WHERE numGroupID = v_numGroupID
   AND numUserID = v_numUserID;

   IF v_numRecCount = 0 then
	
      INSERT INTO UserGroups
			 VALUES(v_numGroupID, v_numUserID);
   end if;
   RETURN;
END; $$;


