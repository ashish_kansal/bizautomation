-- Stored procedure definition script usp_UpdateMyLeadBox for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION usp_UpdateMyLeadBox(v_numUserID NUMERIC DEFAULT 0,
	v_bitWebSite BOOLEAN DEFAULT false,
	v_bitPosition BOOLEAN DEFAULT false,
	v_bitNotes BOOLEAN DEFAULT false,
	v_bitAnnualRevenue BOOLEAN DEFAULT false,
	v_bitNumEmployees BOOLEAN DEFAULT false,
	v_bitHowHeard BOOLEAN DEFAULT false,
	v_bitProfile BOOLEAN DEFAULT false,
	v_vcRedirectURL VARCHAR(100) DEFAULT ' ',
	v_vcInterested1 VARCHAR(100) DEFAULT ' ',
	v_vcInterested2 VARCHAR(100) DEFAULT ' ',
	v_vcInterested3 VARCHAR(100) DEFAULT ' ',
	v_vcInterested4 VARCHAR(100) DEFAULT ' ',
	v_vcInterested5 VARCHAR(100) DEFAULT ' ',
	v_vcInterested6 VARCHAR(100) DEFAULT ' ',
	v_vcInterested7 VARCHAR(100) DEFAULT ' ',
	v_vcInterested8 VARCHAR(100) DEFAULT ' '   
--
)
RETURNS VOID LANGUAGE plpgsql

	--Input Parameter List

   AS $$
BEGIN

	--Updating the field in LeadBox Table

   update LeadBox set vcInterested1 = v_vcInterested1,vcInterested2 = v_vcInterested2,vcInterested3 = v_vcInterested3,
   vcInterested4 = v_vcInterested4,vcInterested5 = v_vcInterested5,
   vcInterested6 = v_vcInterested6,vcInterested7 = v_vcInterested7,
   vcInterested8 = v_vcInterested8,bitWebSite = v_bitWebSite,bitPosition = v_bitPosition,
   bitNotes = v_bitNotes,bitAnnualRevenue = v_bitAnnualRevenue,bitNumEmployees = v_bitNumEmployees,
   bitHowHeard = v_bitHowHeard,bitProfile = v_bitProfile,
   vcRedirectURL = v_vcRedirectURL  where numUserID = v_numUserID;
   RETURN;
END; $$;


