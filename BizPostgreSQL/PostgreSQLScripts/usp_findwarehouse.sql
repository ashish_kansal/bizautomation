-- Stored procedure definition script USP_FindWareHouse for PostgreSQL
CREATE OR REPLACE FUNCTION USP_FindWareHouse(v_numDivisionId NUMERIC(9,0) DEFAULT 0,
v_numDomainID NUMERIC(9,0) DEFAULT 0,
v_tintMode SMALLINT DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_WarehouseID  NUMERIC(9,0);  
   v_ShipCountry  NUMERIC(9,0);  
   v_ShipState  NUMERIC(9,0);  
   v_ShipPostalCode  VARCHAR(10);
BEGIN
   v_WarehouseID := 0; 
--IF @tintMode =0
--BEGIN
   if  v_numDivisionId > 0 then
		
      select   numCountry, numState, vcPostalCode INTO v_ShipCountry,v_ShipState,v_ShipPostalCode from AddressDetails where numRecordID = v_numDivisionId  and numDomainID = v_numDomainID AND tintAddressOf = 2 AND tintAddressType = 2 and bitIsPrimary = true;
      if (SWF_IsNumeric(v_ShipPostalCode) = true and v_ShipState > 0 and v_ShipCountry > 0) then
			
         if exists(select * from WareHouseDetails where numCountryID = v_ShipCountry and numStateID = v_ShipState and numDomainID = v_numDomainID and CAST(v_ShipPostalCode AS NUMERIC) between numFromPinCode and numToPinCode) then
			   
            select   numWareHouse INTO v_WarehouseID from WareHouseDetails where numCountryID = v_ShipCountry and numStateID = v_ShipState and numDomainID = v_numDomainID and CAST(v_ShipPostalCode AS NUMERIC) between numFromPinCode and numToPinCode    LIMIT 1;
         ELSEIF exists(select * from WareHouseDetails where numCountryID = v_ShipCountry and numStateID = v_ShipState and numDomainID = v_numDomainID)
         then
			   
            select   numWareHouse INTO v_WarehouseID from WareHouseDetails where numCountryID = v_ShipCountry and numStateID = v_ShipState and numDomainID = v_numDomainID    LIMIT 1;
         ELSEIF exists(select * from WareHouseDetails where numCountryID = v_ShipCountry and numDomainID = v_numDomainID)
         then
			   
            select   numWareHouse INTO v_WarehouseID from WareHouseDetails where numCountryID = v_ShipCountry and numDomainID = v_numDomainID    LIMIT 1;
         end if;
      else
         if exists(select * from WareHouseDetails where numCountryID = v_ShipCountry and numStateID = v_ShipState and numDomainID = v_numDomainID and numStateID <> 0) then
			   
            select   numWareHouse INTO v_WarehouseID from WareHouseDetails where numCountryID = v_ShipCountry and numStateID = v_ShipState and numDomainID = v_numDomainID and numStateID <> 0    LIMIT 1;
         ELSEIF exists(select * from WareHouseDetails where numCountryID = v_ShipCountry and numDomainID = v_numDomainID)
         then
			   
            select   numWareHouse INTO v_WarehouseID from WareHouseDetails where numCountryID = v_ShipCountry and numDomainID = v_numDomainID    LIMIT 1;
         end if;
      end if;
   end if;

   if   v_WarehouseID = 0 then
		
      if ((select count(*) from Warehouses where numDomainID = v_numDomainID) = 1) then
			
         select   numWareHouseID INTO v_WarehouseID from Warehouses where numDomainID = v_numDomainID;
      end if;
   end if;
   open SWV_RefCur for SELECT coalesce(v_WarehouseID,0);
END; $$;












