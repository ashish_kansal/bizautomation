-- Stored procedure definition script USP_ReportListMaster_BankPreBuildReport for PostgreSQL
CREATE OR REPLACE FUNCTION USP_ReportListMaster_BankPreBuildReport(v_numDomainID NUMERIC(18,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT
   CAST(SUM(numOpeningBal) AS DECIMAL(20,5)) AS MoneyInBank
   FROM
   Chart_Of_Accounts
   WHERE
   numDomainId = v_numDomainID
   AND numParntAcntTypeID IN(SELECT numAccountTypeID FROM AccountTypeDetail WHERE numDomainID = v_numDomainID AND vcAccountCode = '01010101')
   AND numAccountId NOT IN(SELECT cast(numAccountID as DECIMAL(20,5)) FROM AccountingCharges WHERE numDomainID = v_numDomainID AND numChargeTypeId = 9);
END; $$;












