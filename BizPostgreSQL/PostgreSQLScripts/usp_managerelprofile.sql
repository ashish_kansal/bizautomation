-- Stored procedure definition script USP_ManageRelProfile for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ManageRelProfile(v_numRelationship NUMERIC(9,0),    
v_numProfile NUMERIC(9,0),    
v_numRelProID NUMERIC(9,0)    ,
v_numDomainID NUMERIC(9,0))
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_maxOrder  INTEGER;
BEGIN
   if v_numRelProID > 0 then

      update  RelProfile set numRelationshipID = v_numRelationship,numProfileID = v_numProfile where numRelProID = v_numRelProID;
   else
      select   coalesce(max(numOrder),0) INTO v_maxOrder from RelProfile
      join Listdetails L1
      on numRelationshipID = L1.numListItemID
      join Listdetails L2
      on numProfileID = L2.numListItemID where numRelationshipID = v_numRelationship and L2.numDomainid = v_numDomainID;
      insert into RelProfile(numRelationshipID,numProfileID,numOrder) values(v_numRelationship,v_numProfile,v_maxOrder::bigint+1);
   end if;
   RETURN;
END; $$;


