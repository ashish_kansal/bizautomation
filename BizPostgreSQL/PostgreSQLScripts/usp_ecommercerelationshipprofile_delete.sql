-- Stored procedure definition script USP_EcommerceRelationshipProfile_Delete for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_EcommerceRelationshipProfile_Delete(v_numSiteID NUMERIC(18,0)
	,v_vcIds VARCHAR(1000))
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   DELETE FROM EcommerceRelationshipProfile WHERE numSiteID = v_numSiteID AND ID IN(SELECT Id FROM SplitIDs(v_vcIds,','));
   RETURN;
END; $$;


