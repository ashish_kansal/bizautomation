-- Stored procedure definition script USP_BizRecurrence_Execute for PostgreSQL
Create or replace FUNCTION USP_BizRecurrence_Execute(INOUT SWV_RefCur refcursor)
LANGUAGE plpgsql
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
   AS $$
   DECLARE
   v_Date  DATE DEFAULT LOCALTIMESTAMP;

   v_Count  INTEGER DEFAULT 0;
   v_i  INTEGER DEFAULT 1;

   v_numRecConfigID  NUMERIC(18,0);
   v_numOppID  NUMERIC(18,0);
   v_numOppBizDocID  NUMERIC(18,0);
BEGIN
  
 
   DROP TABLE IF EXISTS tt_TEMPTABLE CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPTABLE
   (
      RowNo INTEGER,
      numRecConfigID NUMERIC(18,0),
	--numDomainID NUMERIC(18,0),
	--numUserCntID NUMERIC(18,0),
      numOppID NUMERIC(18,0),
      numOppBizDocID NUMERIC(18,0)
	--dtEndDate DATE,
	--numType SMALLINT,
	--numFrequency SMALLINT
   );

   INSERT INTO tt_TEMPTABLE
   SELECT
   ROW_NUMBER() OVER(ORDER BY numRecConfigID ASC) As RowNo,
	numRecConfigID,
	numOppID,
	numOppBizDocID
   FROM
   RecurrenceConfiguration
   WHERE
   dtNextRecurrenceDate <= v_Date
   AND coalesce(bitDisabled,false) = false
   AND coalesce(bitCompleted,false) = false
   AND numType = 2; --Invoice

   select   COUNT(*) INTO v_Count FROM tt_TEMPTABLE; 

   IF v_Count > 0 then
      WHILE v_i <= v_Count LOOP
         select   numRecConfigID, numOppID, numOppBizDocID INTO v_numRecConfigID,v_numOppID,v_numOppBizDocID FROM
         tt_TEMPTABLE WHERE
         RowNo = v_i;
		

		-- If there are no item left to add to invoice then set bitcompleted to 1 for invoice so it will not be recurred
         IF(SELECT
         COUNT(*)
         FROM
         OpportunityBizDocItems OBDI
         INNER JOIN(SELECT * FROM(SELECT
               OI.numoppitemtCode,
							OI.numUnitHour AS QtyOrdered,
							(OI.numUnitHour -coalesce(SUM(OBI.numUnitHour),0)) AS QtytoFulFill
               FROM
               OpportunityItems OI
               LEFT JOIN
               OpportunityMaster OM
               ON
               OI.numOppId = OM.numOppId
               LEFT JOIN
               OpportunityBizDocItems OBI
               ON
               OBI.numOppItemID = OI.numoppitemtCode AND OBI.numOppBizDocID IN(SELECT numOppBizDocsId FROM OpportunityBizDocs OB WHERE OB.numoppid = OI.numOppId AND coalesce(OB.bitAuthoritativeBizDocs,0) = 1)
               LEFT JOIN
               WareHouseItems WI
               ON
               WI.numWareHouseItemID = OI.numWarehouseItmsID
               INNER JOIN
               Item I
               ON
               I.numItemCode = OI.numItemCode
               WHERE
               OI.numOppId = v_numOppID
               GROUP BY
               OI.numoppitemtCode,OI.numUnitHour) X
            WHERE
            X.QtytoFulFill > 0) AS TEMP
         ON
         OBDI.numOppItemID = TEMP.numoppitemtCode
         WHERE
         OBDI.numOppBizDocID = v_numOppBizDocID) = 0 then
		
            UPDATE RecurrenceConfiguration SET bitCompleted = true WHERE numRecConfigID = v_numRecConfigID;
         end if;
         v_i := v_i::bigint+1;
      END LOOP;
   end if;




--DECLARE @i INT = 1
--DECLARE @COUNT INT
--DECLARE @numRecConfigID NUMERIC(18,0)
--DECLARE @numDomainID NUMERIC(18,0)
--DECLARE @numUserCntID NUMERIC(18,0)
--DECLARE @numOppID NUMERIC(18,0)
--DECLARE @numOppBizDocID NUMERIC(18,0)
--DECLARE @dtEndDate NUMERIC(18,0)
--DECLARE @numType NUMERIC(18,0)
--DECLARE @numFrequency NUMERIC(18,0)

 --Get all the entries from RecurrenceConfiguration table where dtNextRecurrenceDate = @Date
 --@Date will be today date wich is getting passed from windows service which is executed per day

--INSERT INTO
--	@TEMPTABLE
   open SWV_RefCur for SELECT
   cast(ROW_NUMBER() OVER(ORDER BY numRecConfigID ASC) as VARCHAR(255)) As RowNo,
	numRecConfigID,
	cast(RecurrenceConfiguration.numDomainID as VARCHAR(255)),
	cast(RecurrenceConfiguration.numCreatedBy as VARCHAR(255)) AS numUserCntID,
	cast(RecurrenceConfiguration.numOppID as VARCHAR(255)),
	cast(RecurrenceConfiguration.dtNextRecurrenceDate as VARCHAR(255)),
	numOppBizDocID,
	cast(dtEndDate as VARCHAR(255)),
	cast(numType as VARCHAR(255)),
	cast(numFrequency as VARCHAR(255)),
	cast(Domain.bitAutolinkUnappliedPayment as VARCHAR(255)),
	OpportunityMaster.tintopptype,
	OpportunityMaster.numDivisionId
   FROM
   RecurrenceConfiguration
   INNER JOIN
   Domain
   ON
   RecurrenceConfiguration.numDomainID = Domain.numDomainId
   INNER JOIN
   OpportunityMaster
   ON
   RecurrenceConfiguration.numOppID = OpportunityMaster.numOppId
   WHERE
   dtNextRecurrenceDate <= v_Date
   AND coalesce(bitDisabled,false) = false
   AND coalesce(bitCompleted,false) = false;
END; $$;  













