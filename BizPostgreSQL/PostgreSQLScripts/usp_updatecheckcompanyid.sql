-- Stored procedure definition script USP_UpdateCheckCompanyId for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_UpdateCheckCompanyId(v_numCheckId NUMERIC(9,0) DEFAULT 0,
v_numCheckCompanyId NUMERIC(9,0) DEFAULT 0,
v_numCompanyId NUMERIC(9,0) DEFAULT 0)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   Update CheckDetails set numCheckCompanyId = v_numCheckCompanyId,numCustomerId = v_numCompanyId Where numCheckId = v_numCheckId;
   RETURN;
END; $$;


