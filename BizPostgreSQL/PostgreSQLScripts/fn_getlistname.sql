CREATE OR REPLACE FUNCTION fn_GetListName
(
	v_numValueID NUMERIC
	,v_bitTerritory BOOLEAN DEFAULT false
)
RETURNS VARCHAR(100) 
LANGUAGE plpgsql
AS $$
	DECLARE
	v_vcRetValue  VARCHAR(250);
BEGIN
	v_vcRetValue := '';

	IF v_bitTerritory = false then
		SELECT vcData INTO v_vcRetValue FROM Listdetails WHERE numListItemID = v_numValueID;
	ELSE
		SELECT vcTerName INTO v_vcRetValue FROM TerritoryMaster WHERE numTerID = v_numValueID;
	END IF;
   
	RETURN v_vcRetValue;
END; $$;

