-- Stored procedure definition script USP_ArrangeDashboard for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ArrangeDashboard(v_numDashboardReptID NUMERIC(9,0) DEFAULT 0,
v_charMove CHAR(1) DEFAULT NULL,
v_strItems TEXT DEFAULT '')
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numGroupUserCntId  NUMERIC(9,0);
   v_bitGroup  BOOLEAN;
   v_tintColumn  SMALLINT;
   v_tintRow  SMALLINT;
   v_hDocItem  INTEGER;
BEGIN
   select   numGroupUserCntID, bitGroup, tintColumn, tintRow INTO v_numGroupUserCntId,v_bitGroup,v_tintColumn,v_tintRow from Dashboard where numDashBoardReptID = v_numDashboardReptID;

   if v_charMove = 'L' then

      update Dashboard set tintRow = tintRow -1 where numGroupUserCntID = v_numGroupUserCntId
      and bitGroup = v_bitGroup and tintColumn = v_tintColumn and tintRow > v_tintRow;
      update Dashboard set tintRow = tintRow+1 where numGroupUserCntID = v_numGroupUserCntId
      and bitGroup = v_bitGroup and tintColumn = v_tintColumn -1;
      update Dashboard set tintColumn = tintColumn -1,tintRow = 1 where numDashBoardReptID = v_numDashboardReptID;
   ELSEIF v_charMove = 'R'
   then

      update Dashboard set tintRow = tintRow -1 where numGroupUserCntID = v_numGroupUserCntId
      and bitGroup = v_bitGroup and tintColumn = v_tintColumn and tintRow > v_tintRow;
      update Dashboard set tintRow = tintRow+1 where numGroupUserCntID = v_numGroupUserCntId
      and bitGroup = v_bitGroup and tintColumn = v_tintColumn+1;
      update Dashboard set tintColumn = tintColumn+1,tintRow = 1 where numDashBoardReptID = v_numDashboardReptID;
   ELSEIF v_charMove = 'U'
   then

      update Dashboard set tintRow = tintRow+1 where numGroupUserCntID = v_numGroupUserCntId
      and bitGroup = v_bitGroup and tintColumn = v_tintColumn and tintRow = v_tintRow -1;
      update Dashboard set tintRow = tintRow -1 where numDashBoardReptID = v_numDashboardReptID;
   ELSEIF v_charMove = 'D'
   then

      update Dashboard set tintRow = tintRow -1 where numGroupUserCntID = v_numGroupUserCntId
      and bitGroup = v_bitGroup and tintColumn = v_tintColumn and tintRow = v_tintRow+1;
      update Dashboard set tintRow = tintRow+1 where numDashBoardReptID = v_numDashboardReptID;
   ELSEIF v_charMove = 'X'
   then

      delete FROM Dashboard where numDashBoardReptID = v_numDashboardReptID;
   ELSEIF v_charMove = 'A'
   then
      IF SUBSTR(CAST(v_strItems AS VARCHAR(10)),1,10) <> '' then
         UPDATE Dashboard SET tintColumn = X.tintColumn,tintRow = X.tintRow
         FROM
		 XMLTABLE
		(
			'NewDataSet/Table'
			PASSING 
				CAST(v_strItems AS XML)
			COLUMNS
				id FOR ORDINALITY,
				numDashBoardReptID NUMERIC(9,0) PATH 'numDashBoardReptID',
				tintColumn SMALLINT PATH 'tintColumn',
				tintRow SMALLINT PATH 'tintRow'
		) AS X 
         WHERE Dashboard.numDashBoardReptID = x.numDashBoardReptID;
    
      end if;
   end if;
   RETURN;
END; $$;



