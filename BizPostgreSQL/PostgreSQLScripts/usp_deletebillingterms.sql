CREATE OR REPLACE FUNCTION Usp_DeleteBillingTerms(v_numTermsID		BIGINT,
	v_numDomainID	NUMERIC(18,0))
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   IF EXISTS(SELECT * FROM DivisionMaster WHERE numBillingDays = v_numTermsID AND numDomainID = v_numDomainID) then
      RAISE EXCEPTION 'DEPENDANT';
      RETURN;
   end if;	
	
   DELETE FROM BillingTerms WHERE numTermsID = v_numTermsID AND numDomainID = v_numDomainID;
END; $$;


