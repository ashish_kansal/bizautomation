-- Stored procedure definition script USP_DeleteContractsContacts for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_DeleteContractsContacts(v_numContactID NUMERIC(18,0),
v_numContractID NUMERIC(18,0),
v_numDomainID NUMERIC(18,0))
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   delete FROM ContractsContact where
   numContactId = v_numContactID
   and numContractID = v_numContractID and numDomainId = v_numDomainID;
   RETURN;
END; $$;


