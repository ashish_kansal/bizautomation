CREATE OR REPLACE FUNCTION USP_InventoryDashboard(v_numDomainID INTEGER,
v_PeriodFrom Date,
v_PeriodTo Date,
v_Type INTEGER,
v_Top INTEGER, INOUT SWV_RefCur refcursor)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_Sql  VARCHAR(500);
BEGIN
   drop table IF EXISTS tt_MOSTSOLD CASCADE;
   create TEMPORARY TABLE tt_MOSTSOLD
   (
      Item VARCHAR(250),
      SalesUnit DOUBLE PRECISION
   );

   if v_Type = 1 then -- Most Sold
	
      insert into tt_MOSTSOLD
      select  vcItemName || ',' || ItemClass as Item, sum(numUnitHour) as SalesUnit
      from VIEW_INVENTORYOPPSALES where numDomainID = v_numDomainID
      and OppDate between v_PeriodFrom and v_PeriodTo
      group by vcItemName || ',' || ItemClass;
      v_Sql := 'select * from tt_MOSTSOLD order by SalesUnit Desc LIMIT ' || cast(v_Top as VARCHAR);
      OPEN SWV_RefCur FOR EXECUTE v_Sql;
   ELSEIF v_Type = 2
   then --- Highest Avg Cost
	
      insert into tt_MOSTSOLD
      select  vcItemName || ',' || ItemClass as Item, Max(AvgCost) as AvgCost
      from VIEW_INVENTORYOPPSALES where numDomainID = v_numDomainID
      and OppDate between v_PeriodFrom and v_PeriodTo
      group by vcItemName || ',' || ItemClass;
      v_Sql := 'select * from tt_MOSTSOLD order by SalesUnit Desc LIMIT ' || cast(v_Top as VARCHAR);
      OPEN SWV_RefCur FOR EXECUTE v_Sql;
   ELSEIF v_Type = 3
   then --- Highest Cogs
	
      insert into tt_MOSTSOLD
      select  vcItemName || ',' || ItemClass as Item, sum(COGS) as COGS
      from VIEW_INVENTORYOPPSALES where numDomainID = v_numDomainID
      and OppDate between v_PeriodFrom and v_PeriodTo
      group by vcItemName || ',' || ItemClass;
      v_Sql := 'select * from tt_MOSTSOLD order by SalesUnit Desc LIMIT ' || cast(v_Top as VARCHAR);
      OPEN SWV_RefCur FOR EXECUTE v_Sql;
   ELSEIF v_Type = 4
   then --- Highest Profit
	
      insert into tt_MOSTSOLD
      select  vcItemName || ',' || ItemClass as Item, sum(Profit) as Profit
      from VIEW_INVENTORYOPPSALES where numDomainID = v_numDomainID
      and OppDate between v_PeriodFrom and v_PeriodTo
      group by vcItemName || ',' || ItemClass;
      v_Sql := 'select * from tt_MOSTSOLD order by SalesUnit Desc LIMIT ' || cast(v_Top as VARCHAR);
      OPEN SWV_RefCur FOR EXECUTE v_Sql;
   ELSEIF v_Type = 5
   then --- Highest Return
	
      insert into tt_MOSTSOLD
      select  vcItemName || ',' || ItemClass as Item, sum(SalesReturn) as Return
      from VIEW_INVENTORYOPPSALES where numDomainID = v_numDomainID
      and OppDate between v_PeriodFrom and v_PeriodTo
      group by vcItemName || ',' || ItemClass;
      v_Sql := 'select * from tt_MOSTSOLD order by SalesUnit Desc LIMIT ' || cast(v_Top as VARCHAR);
      OPEN SWV_RefCur FOR EXECUTE v_Sql;
   end if;

   RETURN;
END; $$;

