-- Stored procedure definition script USP_getCustomReportGridType for PostgreSQL
CREATE OR REPLACE FUNCTION USP_getCustomReportGridType(v_numUserCntID NUMERIC(9,0),    
v_numDomainId NUMERIC(9,0),
v_bitGridType BOOLEAN,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for select vcReportName,numCustomReportId as numReportId from CustomReport
   where numCreatedBy = v_numUserCntID and numdomainId = v_numDomainId and bitGridType = v_bitGridType;
END; $$;












