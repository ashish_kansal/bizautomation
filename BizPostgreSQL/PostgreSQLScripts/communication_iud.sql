CREATE OR REPLACE FUNCTION Communication_IUD_TrFunc()
RETURNS TRIGGER LANGUAGE plpgsql
AS $$
DECLARE 
	v_tintWFTriggerOn  SMALLINT;
	v_numRecordID NUMERIC(18,0);
	v_numUserCntID NUMERIC(18,0);
	v_numDomainID NUMERIC(18,0);
	v_vcColumnsUpdated VARCHAR(1000) DEFAULT '';
BEGIN
	IF (TG_OP = 'DELETE') THEN
		INSERT INTO ElasticSearchModifiedRecords(vcModule,numRecordID, vcAction, numDomainID)  VALUES('ActionItem', OLD.numCommId, 'Delete', OLD.numDomainID);
		v_tintWFTriggerOn := 5;
		v_numRecordID := OLD.numCommId;
		v_numUserCntID := OLD.numModifiedBy;
		v_numDomainID := OLD.numDomainID;
	ELSIF (TG_OP = 'UPDATE') THEN
		INSERT INTO ElasticSearchModifiedRecords(vcModule,numRecordID, vcAction, numDomainID)  VALUES('ActionItem', NEW.numCommId, 'Update', NEW.numDomainID);
		v_tintWFTriggerOn := 2;
		v_numRecordID := NEW.numCommId;
		v_numUserCntID := NEW.numModifiedBy;
		v_numDomainID := NEW.numDomainID;

		IF OLD.numActivity <> NEW.numActivity THEN
			v_vcColumnsUpdated = CONCAT(v_vcColumnsUpdated,'numActivity,');
		END IF;
		IF OLD.numAssignedBy <> NEW.numAssignedBy THEN
			v_vcColumnsUpdated = CONCAT(v_vcColumnsUpdated,'numAssignedBy,');
		END IF;
		IF OLD.numAssign <> NEW.numAssign THEN
			v_vcColumnsUpdated = CONCAT(v_vcColumnsUpdated,'numAssign,');
		END IF;
		IF OLD.numDivisionID <> NEW.numDivisionID THEN
			v_vcColumnsUpdated = CONCAT(v_vcColumnsUpdated,'numDivisionID,');
		END IF;
		IF OLD.dtStartTime <> NEW.dtStartTime THEN
			v_vcColumnsUpdated = CONCAT(v_vcColumnsUpdated,'dtStartTime,');
		END IF;
		IF OLD.numStatus <> NEW.numStatus THEN
			v_vcColumnsUpdated = CONCAT(v_vcColumnsUpdated,'numStatus,');
		END IF;
		IF OLD.intRemainderMins <> NEW.intRemainderMins THEN
			v_vcColumnsUpdated = CONCAT(v_vcColumnsUpdated,'intRemainderMins,');
		END IF;
		IF OLD.intSnoozeMins <> NEW.intSnoozeMins THEN
			v_vcColumnsUpdated = CONCAT(v_vcColumnsUpdated,'intSnoozeMins,');
		END IF;
		IF OLD.bitTask <> NEW.bitTask THEN
			v_vcColumnsUpdated = CONCAT(v_vcColumnsUpdated,'bitTask,');
		END IF;

		v_vcColumnsUpdated = TRIM(v_vcColumnsUpdated,',');

	ELSIF (TG_OP = 'INSERT') THEN
		INSERT INTO ElasticSearchModifiedRecords(vcModule,numRecordID, vcAction, numDomainID)  VALUES('ActionItem', NEW.numCommId, 'Insert', NEW.numDomainID);
		v_tintWFTriggerOn := 1;
		v_numRecordID := NEW.numCommId;
		v_numUserCntID := New.numCreatedBy;
		v_numDomainID := NEW.numDomainID;
	END IF;

	PERFORM USP_ManageWorkFlowQueue(v_numWFQueueID := 0,v_numDomainID := v_numDomainID,v_numUserCntID := v_numUserCntID,
		   v_numRecordID := v_numRecordID,v_numFormID := 124,v_tintProcessStatus := 1::SMALLINT,
		   v_tintWFTriggerOn := v_tintWFTriggerOn::SMALLINT,
		   v_tintMode := 1::SMALLINT,v_vcColumnsUpdated := v_vcColumnsUpdated);

	RETURN NULL;
END; $$;
CREATE TRIGGER Communication_IUD AFTER INSERT OR UPDATE OR DELETE ON Communication FOR EACH ROW EXECUTE PROCEDURE Communication_IUD_TrFunc();


