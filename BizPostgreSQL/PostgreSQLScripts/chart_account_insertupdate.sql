CREATE OR REPLACE FUNCTION Chart_Account_InsertUpdate
(
	v_numAccountId NUMERIC(18,0),
	v_numParntAcntTypeId NUMERIC(18,0),
	v_numOriginalOpeningBal NUMERIC(19,4),
	v_numOpeningBal NUMERIC(19,4),
	v_dtOpeningDate TIMESTAMP,
	v_bitActive BOOLEAN,
	v_bitFixed BOOLEAN,
	v_numDomainId NUMERIC(18,0),
	v_numListItemID NUMERIC(18,0),
	v_bitDepreciation BOOLEAN,
	v_bitOpeningBalanceEquity BOOLEAN,
	v_monEndingOpeningBal NUMERIC(19,4),
	v_monEndingBal VARCHAR(50),
	v_dtEndStatementDate TIMESTAMP,
	v_chBizDocItems CHAR(2),
	v_numAcntTypeId NUMERIC(18,0),
	v_vcAccountCode VARCHAR(50),
	v_vcAccountName VARCHAR(100),
	v_vcAccountDescription VARCHAR(100),
	v_bitProfitLoss BOOLEAN,
	v_dtDepreciationCostDate TIMESTAMP,
	v_monDepreciationCost NUMERIC(19,4),
	v_vcNumber VARCHAR(50),
	v_bitIsSubAccount BOOLEAN,
	v_numParentAccId NUMERIC(18,0),
	v_intLevel INTEGER,
	v_IsBankAccount BOOLEAN,
	v_vcStartingCheckNumber VARCHAR(50),
	v_vcBankRountingNumber VARCHAR(50),
	v_IsConnected BOOLEAN,
	v_numBankDetailID NUMERIC(18,0),
	INOUT SWV_RefCur REFCURSOR
)
LANGUAGE plpgsql
AS $$
BEGIN
	IF NOT EXISTS(SELECT * FROM Chart_Of_Accounts where numAccountId = v_numAccountId) then
		INSERT INTO Chart_Of_Accounts
		(
			numParntAcntTypeID
			,numOriginalOpeningBal
			,numOpeningBal
			,dtOpeningDate
			,bitActive
			,bitFixed
			,numDomainId
			,numListItemID
			,bitDepreciation
			,bitOpeningBalanceEquity
			,monEndingOpeningBal
			,monEndingBal
			,dtEndStatementDate
			,chBizDocItems
			,numAcntTypeId
			,vcAccountCode
			,vcAccountName
			,vcAccountDescription
			,bitProfitLoss
			,dtDepreciationCostDate
			,monDepreciationCost
			,vcNumber
			,bitIsSubAccount
			,numParentAccId
			,intLevel
			,IsBankAccount
			,vcStartingCheckNumber
			,vcBankRountingNumber
			,IsConnected
			,numBankDetailID
		)
		VALUES
		(
			v_numParntAcntTypeId,
			CAST(v_numOriginalOpeningBal AS DECIMAL(20,5)),
			CAST(v_numOpeningBal AS DECIMAL(20,5)),
			v_dtOpeningDate,
			v_bitActive,
			v_bitFixed,
			v_numDomainId,
			v_numListItemID,
			v_bitDepreciation,
			v_bitOpeningBalanceEquity,
			CAST(v_monEndingOpeningBal AS DECIMAL(20,5)),
			CAST(v_monEndingBal AS DECIMAL(20,5)),
			v_dtEndStatementDate,
			v_chBizDocItems,
			v_numAcntTypeId,
			v_vcAccountCode,
			v_vcAccountName,
			v_vcAccountDescription,
			v_bitProfitLoss,
			v_dtDepreciationCostDate,
			CAST(v_monDepreciationCost AS DECIMAL(20,5)),
			v_vcNumber,
			v_bitIsSubAccount,
			v_numParentAccId,
			v_intLevel,
			v_IsBankAccount,
			v_vcStartingCheckNumber,
			v_vcBankRountingNumber,
			v_IsConnected,
			v_numBankDetailID
		);
	ELSE
		UPDATE 
			Chart_Of_Accounts
		SET
			numParntAcntTypeID = coalesce(v_numParntAcntTypeId,numParntAcntTypeID),numOriginalOpeningBal = CAST(v_numOriginalOpeningBal AS DECIMAL(20,5)),numOpeningBal = CAST(v_numOpeningBal AS DECIMAL(20,5)),dtOpeningDate = v_dtOpeningDate,
			bitActive = v_bitActive,bitFixed = v_bitFixed,numDomainId = v_numDomainId,
			numListItemID = v_numListItemID,bitDepreciation = v_bitDepreciation,
			bitOpeningBalanceEquity = v_bitOpeningBalanceEquity,monEndingOpeningBal = CAST(v_monEndingOpeningBal AS DECIMAL(20,5)),
			monEndingBal = CAST(v_monEndingBal AS DECIMAL(20,5)),
			dtEndStatementDate = v_dtEndStatementDate,
			chBizDocItems = v_chBizDocItems,numAcntTypeId = coalesce(v_numAcntTypeId,numAcntTypeId),
			vcAccountCode = v_vcAccountCode,vcAccountName = v_vcAccountName,
			vcAccountDescription = v_vcAccountDescription,bitProfitLoss = v_bitProfitLoss,
			dtDepreciationCostDate = v_dtDepreciationCostDate,monDepreciationCost = CAST(v_monDepreciationCost AS DECIMAL(20,5)),
			vcNumber = v_vcNumber,
			bitIsSubAccount = v_bitIsSubAccount,numParentAccId = v_numParentAccId,
			intLevel = v_intLevel,IsBankAccount = v_IsBankAccount,vcStartingCheckNumber = v_vcStartingCheckNumber,
			vcBankRountingNumber = v_vcBankRountingNumber,
			IsConnected = v_IsConnected,numBankDetailID = v_numBankDetailID
		WHERE
			numAccountId = v_numAccountId;
   end if;

   OPEN SWV_RefCur FOR SELECT v_numAccountId as numAccountId;
END; $$;




