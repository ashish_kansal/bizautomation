-- Stored procedure definition script USP_EditJournalEntryHeader for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_EditJournalEntryHeader(v_numJournal_Id NUMERIC(9,0) DEFAULT 0,                      
v_numRecurringId NUMERIC(9,0) DEFAULT 0,            
v_datEntry_Date TIMESTAMP DEFAULT NULL,            
v_numAmount DECIMAL(20,5) DEFAULT NULL,        
v_AccountId NUMERIC(9,0) DEFAULT 0,  
v_numDomainId NUMERIC(9,0) DEFAULT 0,
v_vcDescription VARCHAR(1000) DEFAULT NULL,
v_numClassID NUMERIC DEFAULT null)
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_bitOpeningBalance  BOOLEAN;
BEGIN
   if v_numRecurringId = 0 then 
      v_numRecurringId := null;
   end if;      
   select   coalesce(bitOpeningBalance,false) INTO v_bitOpeningBalance From  General_Journal_Header Where numJOurnal_Id = v_numJournal_Id  And numDomainId = v_numDomainId;              
   Update General_Journal_Header Set datEntry_Date = v_datEntry_Date,numAmount = v_numAmount,numRecurringId = v_numRecurringId,
   varDescription = v_vcDescription,numClassID = v_numClassID Where numJOurnal_Id = v_numJournal_Id And numDomainId = v_numDomainId;
   if v_bitOpeningBalance = true then
 
      RAISE NOTICE '%',v_bitOpeningBalance;
      RAISE NOTICE 'SP';
      RAISE NOTICE '%',v_numAmount;
   end if;
   RETURN;
END; $$;


