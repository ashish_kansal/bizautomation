-- Stored procedure definition script USP_GetShippingRuleDetails for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_GetShippingRuleDetails(v_numDomainID NUMERIC(18,0)
	,v_numWareHouseId VARCHAR(20)
	,v_ZipCode VARCHAR(20)
	,v_StateId NUMERIC(18,0)	
	,v_numDivisionID NUMERIC(18,0)
	,v_numSiteID NUMERIC(18,0)
	,v_numCountryId NUMERIC(18,0), INOUT SWV_RefCur refcursor , INOUT SWV_RefCur2 refcursor)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numRuleID  NUMERIC;
   v_numRelationship  NUMERIC(18,0);
   v_numProfile  NUMERIC(18,0);
BEGIN
   IF(SELECT coalesce(bitEnableStaticShippingRule,false) FROM Domain WHERE numDomainId = v_numDomainID) = true then
	
      IF coalesce(v_numSiteID,0) > 0 AND coalesce(v_numDivisionID,0) = 0 then
         select   coalesce(numRelationshipId,0), coalesce(numProfileId,0) INTO v_numRelationship,v_numProfile FROM
         eCommerceDTL WHERE
         numSiteId = v_numSiteID;
         SELECT 
         SR.numRuleID INTO v_numRuleID FROM
         ShippingRules SR
         INNER JOIN
         ShippingRuleStateList SRS
         ON
         SR.numRuleID = SRS.numRuleID WHERE
								(SR.numProfile = v_numProfile OR coalesce(SR.numProfile,0) = 0)
         AND (SR.numRelationship = v_numRelationship OR coalesce(SR.numRelationship,0) = 0)
         AND POSITION(v_numWareHouseId IN SR.numWareHouseID) > 0
         AND SR.numDomainId = v_numDomainID
         AND coalesce(SRS.numCountryID,0) = coalesce(v_numCountryId,0)
         AND (SRS.numStateID = v_StateId OR coalesce(SRS.numStateID,0) = 0)
         AND (coalesce(SRS.vcZipPostal,'') = '' OR v_ZipCode ilike(SRS.vcZipPostal || '%'))   ORDER BY(CASE
         WHEN coalesce(SRS.numCountryID,0) > 0 AND coalesce(SRS.numStateID,0) > 0 AND LENGTH(coalesce(SRS.vcZipPostal,'')) > 0 THEN 1
         WHEN coalesce(SRS.numCountryID,0) > 0 AND coalesce(SRS.numStateID,0) = 0 AND LENGTH(coalesce(SRS.vcZipPostal,'')) > 0 THEN 2
         WHEN coalesce(SRS.numCountryID,0) > 0 AND coalesce(SRS.numStateID,0) > 0 AND LENGTH(coalesce(SRS.vcZipPostal,'')) = 0 THEN 3
         WHEN coalesce(SRS.numCountryID,0) > 0 AND coalesce(SRS.numStateID,0) = 0 AND LENGTH(coalesce(SRS.vcZipPostal,'')) = 0 THEN 4
         ELSE 5
         END),(CASE
         WHEN coalesce(SR.numRelationship,0) > 0 AND coalesce(SR.numProfile,0) > 0 THEN 1
         WHEN coalesce(SR.numRelationship,0) > 0 AND coalesce(SR.numProfile,0) = 0 THEN 2
         WHEN coalesce(SR.numRelationship,0) = 0 AND coalesce(SR.numProfile,0) > 0 THEN 3
         ELSE 4
         END),LENGTH(coalesce(SRS.vcZipPostal,''))  LIMIT 1;
      ELSE
         SELECT 
         SR.numRuleID INTO v_numRuleID FROM
         ShippingRules SR
         INNER JOIN
         CompanyInfo C
         ON
								(SR.numProfile = C.vcProfile OR coalesce(SR.numProfile,0) = 0)
         AND (SR.numRelationship = C.numCompanyType OR coalesce(SR.numRelationship,0) = 0)
         INNER JOIN
         ShippingRuleStateList SRS
         ON
         SR.numRuleID = SRS.numRuleID
         INNER JOIN
         DivisionMaster DM
         ON
         C.numCompanyId = DM.numCompanyID WHERE
         DM.numDivisionID = v_numDivisionID
         AND POSITION(v_numWareHouseId IN SR.numWareHouseID) > 0
         AND SR.numDomainId = v_numDomainID
         AND coalesce(SRS.numCountryID,0) = coalesce(v_numCountryId,0)
         AND (SRS.numStateID = v_StateId OR coalesce(SRS.numStateID,0) = 0)
         AND (coalesce(SRS.vcZipPostal,'') = '' OR v_ZipCode ilike(SRS.vcZipPostal || '%'))   ORDER BY(CASE
         WHEN coalesce(SRS.numCountryID,0) > 0 AND coalesce(SRS.numStateID,0) > 0 AND LENGTH(coalesce(SRS.vcZipPostal,'')) > 0 THEN 1
         WHEN coalesce(SRS.numCountryID,0) > 0 AND coalesce(SRS.numStateID,0) = 0 AND LENGTH(coalesce(SRS.vcZipPostal,'')) > 0 THEN 2
         WHEN coalesce(SRS.numCountryID,0) > 0 AND coalesce(SRS.numStateID,0) > 0 AND LENGTH(coalesce(SRS.vcZipPostal,'')) = 0 THEN 3
         WHEN coalesce(SRS.numCountryID,0) > 0 AND coalesce(SRS.numStateID,0) = 0 AND LENGTH(coalesce(SRS.vcZipPostal,'')) = 0 THEN 4
         ELSE 5
         END),(CASE
         WHEN coalesce(SR.numRelationship,0) > 0 AND coalesce(SR.numProfile,0) > 0 THEN 1
         WHEN coalesce(SR.numRelationship,0) > 0 AND coalesce(SR.numProfile,0) = 0 THEN 2
         WHEN coalesce(SR.numRelationship,0) = 0 AND coalesce(SR.numProfile,0) > 0 THEN 3
         ELSE 4
         END),LENGTH(coalesce(SRS.vcZipPostal,''))  LIMIT 1;
      end if;
      open SWV_RefCur for
      SELECT * FROM ShippingRules WHERE numRuleID = v_numRuleID;
      open SWV_RefCur2 for
      SELECT ROW_NUMBER() OVER(ORDER BY numServiceTypeID ASC) AS RowNum,
				CAST(monRate AS DECIMAL(20,2)) AS monRate,
		 * FROM ShippingServiceTypes WHERE numRuleID = v_numRuleID;
   end if;
   RETURN;
END; $$; 


