-- Function definition script GetItemPurchaseVendorPrice for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION GetItemPurchaseVendorPrice(v_numDivisionID NUMERIC(18,0),
	  v_numItemCode NUMERIC(18,0),
	  v_numDomainID NUMERIC(18,0), 
	  v_numUnits DOUBLE PRECISION)
RETURNS TABLE 
(
   numDivisionID NUMERIC(9,0),
   intMinQty INTEGER, 
   monPrice DECIMAL(20,5), 
   intLeadTimeDays INTEGER
) LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numMinimumQty  INTEGER DEFAULT 0;
   v_intLeadTimeDays  INTEGER DEFAULT 0;
   v_monListPrice  DECIMAL(20,5) DEFAULT 0;
   v_numRelationship  INTEGER DEFAULT 0;
   v_numProfile  NUMERIC(9,0);
BEGIN
   DROP TABLE IF EXISTS tt_GETITEMPURCHASEVENDORPRICE_TEMP CASCADE;
   CREATE TEMPORARY TABLE IF NOT EXISTS  tt_GETITEMPURCHASEVENDORPRICE_TEMP
   (
      numDivisionID NUMERIC(9,0),
      intMinQty INTEGER, 
      monPrice DECIMAL(20,5), 
      intLeadTimeDays INTEGER
   );
   IF coalesce(v_numDivisionID,0) = 0 then
	
		-- CHECK IF PRIMARY VENDOR EXISTS
      select   V.numVendorID, V.intMinQty, coalesce(VSM.numListValue,0) INTO v_numDivisionID,v_numMinimumQty,v_intLeadTimeDays FROM
      Item I
      INNER JOIN
      Vendor V
      ON
      V.numVendorID = I.numVendorID
      LEFT JOIN
      VendorShipmentMethod VSM
      ON
      V.numVendorID = VSM.numVendorID
      AND VSM.numDomainId = v_numDomainID
      AND VSM.bitPrimary = true
      AND VSM.bitPreferredMethod = true WHERE
      V.numDomainID = v_numDomainID AND
      I.numItemCode = v_numItemCode; 

		-- IF PRIMARY VENDOR IS NOT AVAILABLE THEN SELECT ANY VENDOR AVAILABLE
      IF coalesce(v_numDivisionID,0) = 0 then
		
         select   V.numVendorID, V.intMinQty, coalesce(VSM.numListValue,0) INTO v_numDivisionID,v_numMinimumQty,v_intLeadTimeDays FROM
         Vendor V
         LEFT JOIN
         VendorShipmentMethod VSM
         ON
         V.numVendorID = VSM.numVendorID
         AND VSM.numDomainId = v_numDomainID
         AND VSM.bitPrimary = true
         AND VSM.bitPreferredMethod = true WHERE
         V.numDomainID = v_numDomainID AND V.numItemCode = v_numItemCode    LIMIT 1;
      end if;
   end if;

	-- IF ANY VENDOR IS AVAILABLE THEN GO FOR GETTING PRICE
   IF coalesce(v_numDivisionID,0) > 0 then
	
      select   numCompanyType, vcProfile INTO v_numRelationship,v_numProfile FROM
      DivisionMaster D
      JOIN
      CompanyInfo C
      ON
      C.numCompanyId = D.numCompanyID WHERE
      D.numDivisionID = v_numDivisionID;
      select   coalesce(monCost,0), V.intMinQty, coalesce(VSM.numListValue,0) INTO v_monListPrice,v_numMinimumQty,v_intLeadTimeDays FROM
      Vendor V
      INNER JOIN
      Item I
      ON
      V.numItemCode = I.numItemCode
      LEFT JOIN
      VendorShipmentMethod VSM
      ON
      V.numVendorID = VSM.numVendorID
      AND VSM.numDomainId = v_numDomainID
      AND VSM.bitPrimary = true
      AND VSM.bitPreferredMethod = true WHERE
      V.numItemCode = v_numItemCode AND
      V.numDomainID = v_numDomainID AND
      V.numVendorID = v_numDivisionID;
	
		--CHECK IF PRICE RULE IS APPLICABLE
      IF(SELECT
      COUNT(*)
      FROM
      Item I
      LEFT JOIN PriceBookRules P ON I.numDomainID = P.numDomainID
      LEFT JOIN PriceBookRuleDTL PDTL ON P.numPricRuleID = PDTL.numRuleID
      LEFT JOIN PriceBookRuleItems PBI ON P.numPricRuleID = PBI.numRuleID
      LEFT JOIN PriceBookPriorities PP ON PP.Step2Value = P.tintStep2 AND PP.Step3Value = P.tintStep3
      WHERE
      numItemCode = v_numItemCode AND
      tintRuleFor = 2 AND
			(
				((tintStep2 = 1 AND PBI.numValue = v_numItemCode) AND (tintStep3 = 1 AND PDTL.numValue = v_numDivisionID)) -- Priority 1
      OR ((tintStep2 = 2 AND PBI.numValue = I.numItemClassification) AND (tintStep3 = 1 AND PDTL.numValue = v_numDivisionID)) -- Priority 2
      OR (tintStep2 = 3 AND (tintStep3 = 1 AND PDTL.numValue = v_numDivisionID)) -- Priority 3

      OR ((tintStep2 = 1 AND PBI.numValue = v_numItemCode) AND (tintStep3 = 2 AND PDTL.numValue = v_numRelationship AND numProfile = v_numProfile)) -- Priority 4
      OR ((tintStep2 = 2 AND PBI.numValue = I.numItemClassification) AND (tintStep3 = 2 AND PDTL.numValue = v_numRelationship AND numProfile = v_numProfile)) -- Priority 5
      OR (tintStep2 = 3 AND (tintStep3 = 2 AND PDTL.numValue = v_numRelationship AND numProfile = v_numProfile)) -- Priority 6

      OR ((tintStep2 = 1 AND PBI.numValue = v_numItemCode) AND tintStep3 = 3) -- Priority 7
      OR ((tintStep2 = 2 AND PBI.numValue = I.numItemClassification) AND tintStep3 = 3) -- Priority 8
      OR (tintStep2 = 3 and tintStep3 = 3) -- Priority 9
			)) > 0 then
		
         select(CASE
         WHEN GetPriceBasedOnPriceBook(P.numPricRuleID,v_monListPrice,v_numUnits,I.numItemCode) > 0
         THEN GetPriceBasedOnPriceBook(P.numPricRuleID,v_monListPrice,v_numUnits,I.numItemCode)
         ELSE v_monListPrice
         END) INTO v_monListPrice FROM
         Item I
         LEFT JOIN PriceBookRules P ON I.numDomainID = P.numDomainID
         LEFT JOIN PriceBookRuleDTL PDTL ON P.numPricRuleID = PDTL.numRuleID
         LEFT JOIN PriceBookRuleItems PBI ON P.numPricRuleID = PBI.numRuleID
         LEFT JOIN PriceBookPriorities PP ON PP.Step2Value = P.tintStep2 AND PP.Step3Value = P.tintStep3 WHERE
         numItemCode = v_numItemCode AND
         tintRuleFor = 2 AND
				(
					((tintStep2 = 1 AND PBI.numValue = v_numItemCode) AND (tintStep3 = 1 AND PDTL.numValue = v_numDivisionID)) -- Priority 1
         OR ((tintStep2 = 2 AND PBI.numValue = I.numItemClassification) AND (tintStep3 = 1 AND PDTL.numValue = v_numDivisionID)) -- Priority 2
         OR (tintStep2 = 3 AND (tintStep3 = 1 AND PDTL.numValue = v_numDivisionID)) -- Priority 3

         OR ((tintStep2 = 1 AND PBI.numValue = v_numItemCode) AND (tintStep3 = 2 AND PDTL.numValue = v_numRelationship AND numProfile = v_numProfile)) -- Priority 4
         OR ((tintStep2 = 2 AND PBI.numValue = I.numItemClassification) AND (tintStep3 = 2 AND PDTL.numValue = v_numRelationship AND numProfile = v_numProfile)) -- Priority 5
         OR (tintStep2 = 3 AND (tintStep3 = 2 AND PDTL.numValue = v_numRelationship AND numProfile = v_numProfile)) -- Priority 6

         OR ((tintStep2 = 1 AND PBI.numValue = v_numItemCode) AND tintStep3 = 3) -- Priority 7
         OR ((tintStep2 = 2 AND PBI.numValue = I.numItemClassification) AND tintStep3 = 3) -- Priority 8
         OR (tintStep2 = 3 and tintStep3 = 3) -- Priority 9
				)   ORDER BY PP.Priority ASC;
      end if;
   end if;


   INSERT INTO tt_GETITEMPURCHASEVENDORPRICE_TEMP(numDivisionID, intMinQty, monPrice, intLeadTimeDays) VALUES(v_numDivisionID, v_numMinimumQty, v_monListPrice, v_intLeadTimeDays);

	RETURN QUERY (SELECT * FROM tt_GETITEMPURCHASEVENDORPRICE_TEMP);
END; $$;

