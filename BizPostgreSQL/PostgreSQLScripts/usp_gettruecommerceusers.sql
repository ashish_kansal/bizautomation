-- Stored procedure definition script USP_GetTrueCommerceUsers for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetTrueCommerceUsers(INOUT SWV_RefCur refcursor)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT
		DomainSFTPDetail.numDomainID
		,DomainSFTPDetail.vcUsername
		,DomainSFTPDetail.vcPassword
		,DomainSFTPDetail.vcExportPath
		,DomainSFTPDetail.vcImportPath
   FROM
   DomainSFTPDetail
   INNER JOIN
   Domain
   ON
   DomainSFTPDetail.numDomainID = Domain.numDomainId
   WHERE
   coalesce(Domain.bitEDI,false) = true
   AND tintType = 2;
END; $$;












