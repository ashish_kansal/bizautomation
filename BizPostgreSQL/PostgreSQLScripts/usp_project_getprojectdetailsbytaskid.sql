-- Stored procedure definition script USP_Project_GetProjectDetailsByTaskId for PostgreSQL
CREATE OR REPLACE FUNCTION USP_Project_GetProjectDetailsByTaskId(v_numDomainID NUMERIC(18,0)
	,v_numTaskId NUMERIC(18,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT  P.*,C.vcCompanyName FROM StagePercentageDetailsTask AS ST LEFT JOIN ProjectsMaster AS P ON ST.numProjectId = P.numProId
   LEFT JOIN DivisionMaster AS D ON D.numDivisionID = P.numDivisionId
   LEFT JOIN CompanyInfo AS C ON C.numCompanyId = D.numCompanyID
   WHERE ST.numDomainID = v_numDomainID AND ST.numTaskId = v_numTaskId AND ST.numProjectId > 0 LIMIT 1;
   RETURN;
END; $$;














