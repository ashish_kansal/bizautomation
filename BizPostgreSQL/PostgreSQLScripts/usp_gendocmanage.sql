-- Stored procedure definition script usp_GenDocManage for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION usp_GenDocManage(v_numDomainID NUMERIC(9,0) DEFAULT null,          
 v_vcDocDesc TEXT DEFAULT '',          
 v_VcFileName VARCHAR(100) DEFAULT '',          
 v_vcDocName VARCHAR(100) DEFAULT '',          
 v_numDocCategory NUMERIC(9,0) DEFAULT null,          
 v_vcfiletype VARCHAR(10) DEFAULT '',          
 v_numDocStatus NUMERIC(9,0) DEFAULT null,          
 v_numUserCntID NUMERIC(9,0) DEFAULT null,          
 INOUT v_numGenDocId NUMERIC(9,0) DEFAULT 0 ,          
 v_cUrlType VARCHAR(1) DEFAULT '',        
 v_vcSubject VARCHAR(500) DEFAULT '' ,  
 v_vcDocumentSection VARCHAR(10) DEFAULT NULL,
 v_numRecID NUMERIC(9,0) DEFAULT 0,
 v_tintDocumentType SMALLINT DEFAULT 1,-- 1 = generic, 2= specific document
 v_numModuleID NUMERIC DEFAULT 0,
 v_vcContactPosition VARCHAR(200) DEFAULT '',
 v_numFormFieldGroupId NUMERIC(18,0) DEFAULT 0)
LANGUAGE plpgsql
   AS $$
BEGIN
   if v_numGenDocId = 0 then

      IF v_numModuleID = 0 then 
         v_numModuleID := null;
      end if;
      insert into GenericDocuments(numDomainId,
 vcDocdesc,
 VcFileName,
 VcDocName,
 numDocCategory,
 vcfiletype,
 numDocStatus,
 numCreatedBy,
 bintCreatedDate,
 numModifiedBy,
 bintModifiedDate,
 cUrlType,
 vcSubject,
 vcDocumentSection,
 numRecID,
 tintDocumentType,
 numModuleId,vcContactPosition,numFormFieldGroupId)
values(v_numDomainID,
 v_vcDocDesc,
 v_VcFileName,
 v_vcDocName,
 v_numDocCategory,
 v_vcfiletype,
 v_numDocStatus,
 v_numUserCntID,
 TIMEZONE('UTC',now()),
 v_numUserCntID,
 TIMEZONE('UTC',now()),
 v_cUrlType,
 v_vcSubject,
 v_vcDocumentSection,
 v_numRecID,
 v_tintDocumentType,
 v_numModuleID,v_vcContactPosition,v_numFormFieldGroupId);

      v_numGenDocId := CURRVAL('GenericDocuments_seq');
   else
      IF v_numModuleID = 0 then  
         v_numModuleID := NULL;
      end if;
      update  GenericDocuments
      set
      vcDocdesc = v_vcDocDesc,VcFileName = v_VcFileName,VcDocName = v_vcDocName,numDocCategory = v_numDocCategory,
      vcfiletype = v_vcfiletype,numDocStatus = v_numDocStatus,
      numModifiedBy = v_numUserCntID,bintModifiedDate = TIMEZONE('UTC',now()),
      cUrlType = v_cUrlType,vcSubject = v_vcSubject,vcDocumentSection = v_vcDocumentSection,--Bug ID 1149
      numModuleId = v_numModuleID,vcContactPosition = v_vcContactPosition
      where numGenericDocID = v_numGenDocId;
   end if;
   RETURN;
END; $$;


