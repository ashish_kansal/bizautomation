-- Stored procedure definition script USP_GetMultiAR for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_GetMultiAR(v_numParentDomainID  INTEGER,
 v_dtFromDate TIMESTAMP,
 v_dtToDate TIMESTAMP,
v_numSubscriberID INTEGER,
INOUT SWV_RefCur refcursor default null,
v_Rollup INTEGER DEFAULT 0)
LANGUAGE plpgsql
   AS $$
BEGIN
   DROP TABLE IF EXISTS tt_ARSUMMARY CASCADE;
   CREATE TEMPORARY TABLE tt_ARSUMMARY
   (
      vcDomainName VARCHAR(150),
      Total  DECIMAL(20,5)
   );

   INSERT INTO tt_ARSUMMARY
   select DN.vcDomainName,Sum(Total) as Total
   from VIEW_ARDAILYSUMMARY VARD
   INNER JOIN(SELECT * FROM Domain DN WHERE DN.numSubscriberID = v_numSubscriberID) DN ON VARD.numDomainID  =  DN.numDomainId
   INNER JOIN(SELECT * FROM Domain DNV WHERE DNV.numSubscriberID = v_numSubscriberID) DNV ON VARD.numDomainID = DNV.numDomainId INNER JOIN
   FinancialYear FY ON FY.numDomainId = DN.numDomainId AND
   FY.dtPeriodFrom <= v_dtFromDate and dtPeriodTo >= v_dtFromDate
   AND (DN.numParentDomainID = v_numParentDomainID or DN.numDomainId = v_numParentDomainID)
   AND datEntry_Date between FY.dtPeriodFrom and v_dtFromDate+INTERVAL '-1 day'
   GROUP BY DN.vcDomainName
   union
   select DN.vcDomainName,Sum(Total) as Total
   from VIEW_ARDAILYSUMMARY VARD
   INNER JOIN(SELECT * FROM Domain DN WHERE DN.numSubscriberID = v_numSubscriberID) DN ON VARD.numDomainID  =  DN.numDomainId
   INNER JOIN(SELECT * FROM Domain DNV WHERE DNV.numSubscriberID = v_numSubscriberID) DNV ON VARD.numDomainID = DNV.numDomainId
   AND (DN.numDomainId = v_numParentDomainID)
   AND datEntry_Date between v_dtFromDate and v_dtToDate
   GROUP BY DN.vcDomainName

----------------------------------------
   union 
--------------------------
   select DN.vcDomainName,Sum(Total) as Total
   from VIEW_ARDAILYSUMMARY VARD
   INNER JOIN(select * from Domain DN where DN.numSubscriberID = v_numSubscriberID) DN ON VARD.vcDomainCode  ilike  DN.vcDomainCode || '%'
   INNER JOIN(SELECT * FROM Domain DNV  WHERE DNV.numSubscriberID = v_numSubscriberID) DNV ON VARD.numDomainID = DNV.numDomainId INNER JOIN
   FinancialYear FY ON FY.numDomainId = DN.numDomainId AND
   FY.dtPeriodFrom <= v_dtFromDate and dtPeriodTo >= v_dtFromDate
   AND (DN.numParentDomainID = v_numParentDomainID or DN.numDomainId = v_numParentDomainID)
   AND datEntry_Date between FY.dtPeriodFrom and v_dtFromDate+INTERVAL '-1 day'
   GROUP BY DN.vcDomainName
   union
   select DN.vcDomainName,Sum(Total) as Total
   from VIEW_ARDAILYSUMMARY VARD
   INNER JOIN(SELECT * FROM Domain DN WHERE DN.numSubscriberID = v_numSubscriberID) DN  ON VARD.vcDomainCode  ilike  DN.vcDomainCode || '%'
   INNER JOIN(SELECT * FROM Domain DNV WHERE DNV.numSubscriberID = v_numSubscriberID) DNV  ON VARD.numDomainID = DNV.numDomainId
   AND (DN.numParentDomainID = v_numParentDomainID)
   AND datEntry_Date between v_dtFromDate and v_dtToDate
   GROUP BY DN.vcDomainName;

   if v_Rollup = 0 then
	
      open SWV_RefCur for
      SELECT vcDomainName, Sum(Total) as Total FROM tt_ARSUMMARY GROUP BY vcDomainName;
   ELSEIF v_Rollup = 1
   then
	
      open SWV_RefCur for
      SELECT 'AR Roll-Up' AS  vcDomainName, coalesce(Sum(Total),0) as Total FROM tt_ARSUMMARY;
   end if;
   RETURN;
END; $$;


--EXEC USP_GetMultiBS @numParentDomainID=0,@dtFromDate='01/Jan/2008',@dtToDate='31/Dec/2008',@numSubscriberId=103,@RollUp=1
-- Created by Sojan        
-- [USP_GetChartAcntDetails] @numParentDomainID=72,@dtFromDate='2007-01-01 00:00:00:000',@dtToDate='2009-09-25 00:00:00:000'                            


