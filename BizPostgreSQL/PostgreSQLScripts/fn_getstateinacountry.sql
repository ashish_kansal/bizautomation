-- Function definition script fn_GetStateInACountry for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION fn_GetStateInACountry(v_numCountryID NUMERIC, v_numDomainID NUMERIC, v_returnType CHAR(2))
RETURNS VARCHAR(4000) LANGUAGE plpgsql
   AS $$      
--Thsi function will take the ID for the Country and return the states or the state ids      
   DECLARE
   v_vcRetValue  VARCHAR(4000);
BEGIN
   IF v_returnType = 'NM' then
 
      select   COALESCE(coalesce(v_vcRetValue,'') || ',','') || '''' || Replace(Replace(vcState,CHR(13),''),CHR(10),'') || '''' INTO v_vcRetValue FROM AllCountriesAndStates WHERE numCountryId = v_numCountryID
      AND (numDomainID = v_numDomainID OR constFlag = 1);
   ELSE
      select   COALESCE(coalesce(v_vcRetValue,'') || ',','') || '''' || SUBSTR(CAST(numStateID AS VARCHAR(10)),1,10) || '''' INTO v_vcRetValue FROM AllCountriesAndStates WHERE numCountryId = v_numCountryID
      AND (numDomainID = v_numDomainID OR constFlag = 1);
   end if;      
   RETURN v_vcRetValue;
END; $$;

