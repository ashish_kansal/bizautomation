-- Stored procedure definition script usp_GetDynamicAOIFromLeadBox for PostgreSQL
CREATE OR REPLACE FUNCTION usp_GetDynamicAOIFromLeadBox(v_numLeadBoxID NUMERIC,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT cast(AOIM.numAOIID as VARCHAR(255)), Case When LTrim(vcDbColumnValueText) = '' Then 0 ELSE 1 END as Status
   FROM AOIMaster AOIM, DynamicLeadBoxFormDataDetails DL
   Where DL.boolAOIField = 1
   AND DL.vcFieldName = AOIM.vcAOIName
   AND DL.numLeadBoxId = v_numLeadBoxID;
END; $$;












