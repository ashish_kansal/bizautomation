-- Stored procedure definition script usp_GetWeblinkLabels for PostgreSQL
CREATE OR REPLACE FUNCTION usp_GetWeblinkLabels(v_numDivisionID NUMERIC DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for 
   SELECT  
		vcWebLabel1 as "vcWebLink1Label"
		, vcWebLabel2 as "vcWebLink2Label"
		, vcWebLabel3 as "vcWebLink3Label"
		, vcWebLabel4 as "vcWebLink4Label"
   FROM CompanyInfo C
   join DivisionMaster D
   on D.numCompanyID = C.numCompanyId
   WHERE numDivisionID = v_numDivisionID LIMIT 1;
END; $$;












