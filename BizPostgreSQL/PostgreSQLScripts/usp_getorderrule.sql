-- Stored procedure definition script USP_GetOrderRule for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetOrderRule(v_numRuleID NUMERIC(18,0),
v_numDomainID NUMERIC(18,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for select numRuleID,numBillToID,numShipToID,numBizDocStatus,
(case numBillToID when 0 then 'Employer' when 1 then 'Customer' end) as "Bill To",
(case numShipToID when 0 then 'Employer' when 1 then 'Customer' end) as "Ship To",
(case btFullPaid when false then 'No' when true then 'Yes' end) as "Amount Paid Full",
(case numBizDocStatus when 0 then '-Select One--' else ITB.vcData end) AS "Biz Doc Status"
   from OrderAutoRule OAR  LEFT OUTER JOIN(select LD.vcData,LD.numListItemID from Listdetails LD,listmaster LM  where LM.numListID = 11
      and LD.numListID = LM.numListID AND LD.numDomainid = v_numDomainID)	 ITB ON ITB.numListItemID = CAST(OAR.numBizDocStatus AS NUMERIC)
   where OAR.numDomainID = v_numDomainID;
END; $$;
--EXEC USP_GetOrderRuleDetails @numDomainID=72,@numRuleID=1












