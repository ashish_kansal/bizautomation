-- Function definition script fn_GetEmailAlert for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION fn_GetEmailAlert(v_numDomainId NUMERIC,
	v_numContactId NUMERIC,
	v_numUserContactId NUMERIC)
RETURNS table 
(
   numAlert NUMERIC,
   vcMsg VARCHAR(50)
) LANGUAGE plpgsql
   AS $$
   DECLARE
   v_bitOpenActionItem  BOOLEAN;
   v_vcOpenActionMsg  VARCHAR(50);
   v_bitOpencases  BOOLEAN;
   v_vcOpenCasesMsg  VARCHAR(50);
   v_bitOpenProject  BOOLEAN;
   v_vcOpenProjectMsg  VARCHAR(50);
   v_bitOpenSalesOpp  BOOLEAN;
   v_vcOpenSalesOppMsg  VARCHAR(50);
   v_bitOpenPurchaseOpp  BOOLEAN;
   v_vcOpenPurchaseOppMsg  VARCHAR(50);
   v_bitBalancedue  BOOLEAN;
   v_vcBalancedueMsg  VARCHAR(50);
   v_monBalancedue  DECIMAL(20,5); 
   v_bitARAccount  BOOLEAN;
   v_vcARAccountMsg  VARCHAR(50);
   v_bitAPAccount  BOOLEAN;
   v_vcAPAccountMsg  VARCHAR(50);
   v_bitUnreadEMail  BOOLEAN;
   v_intUnreadEmail  INTEGER;
   v_vcUnreadEmailMsg  VARCHAR(50);
   v_bitPurchasedPast  BOOLEAN;
   v_monPurchasedPast  DECIMAL(20,5); 
   v_vcPurchasedPastMsg  VARCHAR(50);
   v_bitSoldPast  BOOLEAN;
   v_monSoldPast  DECIMAL(20,5); 
   v_vcSoldPastMsg  VARCHAR(50);
   v_bitCustomField  BOOLEAN;
   v_numCustomFieldId  NUMERIC; 
   v_vcCustomFieldValue  VARCHAR(50);
   v_vcCustomfieldMsg  VARCHAR(50);
   v_numCount  NUMERIC; 
   v_strMsg  VARCHAR(50);
   v_numDivisionID  NUMERIC; 
   v_OpenActionItemCount  NUMERIC;
   v_OpenCaseCount  NUMERIC;
   v_OpenProjectCount  NUMERIC;
   v_OpenSalesOppCount  NUMERIC;
   v_OpenPurchaseOppCount  NUMERIC;
   v_TotalBalanceDue  NUMERIC;
   v_UnreadEmailCount  NUMERIC;
   v_PastPurchaseOrderValue  NUMERIC;
   v_PastSalesOrderValue  NUMERIC;
BEGIN
   DROP TABLE IF EXISTS tt_FN_GETEMAILALERT_TBL CASCADE;
   CREATE TEMPORARY TABLE IF NOT EXISTS  tt_FN_GETEMAILALERT_TBL
   (
      numAlert NUMERIC,
      vcMsg VARCHAR(50)
   );
   v_numCount := 0;
   select   coalesce(bitOpenActionItem,false), vcOpenActionMsg, coalesce(bitOpenCases,false), vcOpenCasesMsg, coalesce(bitOpenProject,false), vcOpenProjectMsg, coalesce(bitOpenSalesOpp,false), vcOpenSalesOppMsg, coalesce(bitOpenPurchaseOpp,false), vcOpenPurchaseOppMsg, coalesce(bitBalancedue,false), monBalancedue, vcBalancedueMsg, coalesce(bitARAccount,false), vcARAccountMsg, coalesce(bitAPAccount,false), vcAPAccountMsg, coalesce(bitUnreadEmail,false), intUnreadEmail, vcUnreadEmailMsg, coalesce(bitPurchasedPast,false), monPurchasedPast, vcPurchasedPastMsg, coalesce(bitSoldPast,false), monSoldPast, vcSoldPastMsg, coalesce(bitCustomField,false), numCustomFieldId, vcCustomFieldValue, vcCustomFieldMsg INTO v_bitOpenActionItem,v_vcOpenActionMsg,v_bitOpencases,v_vcOpenCasesMsg,
   v_bitOpenProject,v_vcOpenProjectMsg,v_bitOpenSalesOpp,v_vcOpenSalesOppMsg,
   v_bitOpenPurchaseOpp,v_vcOpenPurchaseOppMsg,v_bitBalancedue,v_monBalancedue,
   v_vcBalancedueMsg,v_bitARAccount,v_vcARAccountMsg,v_bitAPAccount,
   v_vcAPAccountMsg,v_bitUnreadEMail,v_intUnreadEmail,v_vcUnreadEmailMsg,
   v_bitPurchasedPast,v_monPurchasedPast,v_vcPurchasedPastMsg,v_bitSoldPast,
   v_monSoldPast,v_vcSoldPastMsg,v_bitCustomField,v_numCustomFieldId,
   v_vcCustomFieldValue,v_vcCustomfieldMsg FROM AlertConfig WHERE AlertConfig.numDomainID = v_numDomainId AND AlertConfig.numContactId = v_numUserContactId;
      
      
      --TAKE divisionID
     
   SELECT numDivisionId INTO v_numDivisionID FROM AdditionalContactsInformation WHERE numContactId = v_numContactId;
      
   select   CASE WHEN v_bitOpencases = true THEN SUM(OpenCaseCount) ELSE 0 END, CASE WHEN v_bitOpenProject = true THEN SUM(OpenProjectCount) ELSE 0 END, CASE WHEN v_bitOpenSalesOpp = true THEN SUM(OpenSalesOppCount) ELSE 0 END, CASE WHEN v_bitOpenPurchaseOpp = true THEN SUM(OpenPurchaseOppCount) ELSE 0 END, CASE WHEN v_bitBalancedue = true THEN SUM(TotalBalanceDue) ELSE 0 END, CASE WHEN v_bitUnreadEMail = true THEN SUM(UnreadEmailCount) ELSE 0 END, CASE WHEN v_bitPurchasedPast = true THEN SUM(PastPurchaseOrderValue) ELSE 0 END, CASE WHEN v_bitSoldPast = true THEN SUM(PastSalesOrderValue) ELSE 0 END, SUM(CASE WHEN v_bitOpenActionItem = true THEN CASE WHEN v.numContactId = v_numContactId THEN 1 ELSE 0 END ELSE 0 END) INTO v_OpenCaseCount,v_OpenProjectCount,v_OpenSalesOppCount,v_OpenPurchaseOppCount,
   v_TotalBalanceDue,v_UnreadEmailCount,v_PastPurchaseOrderValue,
   v_PastSalesOrderValue,v_OpenActionItemCount FROM VIEW_Email_Alert_Config v WHERE v.numDomainid = v_numDomainId AND v.numDivisionID = v_numDivisionID;
      
   IF v_bitOpenActionItem = true then
      
      if v_OpenActionItemCount > 0 then
			
         v_numCount := v_numCount+1;
         v_strMsg := v_vcOpenActionMsg;
      end if;
   end if;	
   IF v_bitOpencases = true then
      
      if v_OpenCaseCount > 0 then
			
         v_numCount := v_numCount+1;
         v_strMsg := v_vcOpenCasesMsg;
      end if;
   end if; 
   IF v_bitOpenProject = true then
	 
      IF v_OpenProjectCount > 0 then
			 
         v_numCount := v_numCount+1;
         v_strMsg := v_vcOpenProjectMsg;
      end if;
   end if;
   IF v_bitOpenSalesOpp = true then
	 
      IF v_OpenSalesOppCount > 0 then
			 
         v_numCount := v_numCount+1;
         v_strMsg := v_vcOpenSalesOppMsg;
      end if;
   end if;
   IF v_bitOpenPurchaseOpp = true then
	  
      IF v_OpenPurchaseOppCount > 0 then
			
         v_numCount := v_numCount+1;
         v_strMsg := v_vcOpenPurchaseOppMsg;
      end if;
   end if;
   IF v_bitBalancedue = true then
	  
      IF v_TotalBalanceDue > v_monBalancedue then
			
         v_numCount := v_numCount+1;
         v_strMsg := v_vcBalancedueMsg;
      end if;
   end if; 
   IF v_bitUnreadEMail = true then
	  
      IF v_UnreadEmailCount >= v_intUnreadEmail then
			 
         v_numCount := v_numCount+1;
         v_strMsg := v_vcUnreadEmailMsg;
      end if;
   end if;
   IF v_bitPurchasedPast = true then
	  
      IF v_PastPurchaseOrderValue >= v_monPurchasedPast then
			
         v_numCount := v_numCount+1;
         v_strMsg := v_vcPurchasedPastMsg;
      end if;
   end if;
   IF v_bitSoldPast = true then
	  
      IF v_PastSalesOrderValue >= v_monSoldPast then
			
         v_numCount := v_numCount+1;
         v_strMsg := v_vcSoldPastMsg;
      end if;
   end if; 
   IF v_numCount > 1 then
      
      v_strMsg := 'Alert';
   end if;
   INSERT INTO tt_FN_GETEMAILALERT_TBL  SELECT v_numCount ,v_strMsg; 
      RETURN QUERY (SELECT * FROM tt_FN_GETEMAILALERT_TBL);
END; $$; 
      
      ---SELECT * FROM VIEW_Email_Alert_Config
/****** Object:  UserDefinedFunction [dbo].[fn_GetExpenseDtlsbySalesStgID]    Script Date: 07/26/2008 18:12:37 ******/

