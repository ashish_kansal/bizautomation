-- Stored procedure definition script usp_GetSalesVsQuota for PostgreSQL
CREATE OR REPLACE FUNCTION usp_GetSalesVsQuota(v_numDomainID NUMERIC,          
 v_intYear INTEGER,          
 v_intFromMonth INTEGER,          
 v_intToMonth INTEGER,          
 v_dtDateFrom TIMESTAMP,          
 v_dtDateTo TIMESTAMP,          
 v_numTerID NUMERIC DEFAULT 0,          
 v_numUserCntID NUMERIC DEFAULT 0,          
 v_tintRights SMALLINT DEFAULT 3,          
 v_intType NUMERIC DEFAULT 0             
--          
,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_Quota  DECIMAL(20,5);          
   v_DealsWon  DECIMAL(20,5);          
   v_Pipeline  DECIMAL(20,5);          
   v_SUM  DECIMAL(20,5);          
   v_QuotaPer  DOUBLE PRECISION;          
   v_DealsWonPer  DOUBLE PRECISION;          
   v_PipelinePer  DOUBLE PRECISION;
BEGIN
   v_Quota := 0;          
   v_DealsWon := 0;          
   v_Pipeline := 0;          
   v_SUM := 0;          
   v_QuotaPer := 0;          
   v_DealsWonPer := 0;          
   v_PipelinePer := 0;          
          
         
    
   IF v_intType = 0 then
     
   
		 --Quota          
      select   SUM(monQuota) INTO v_Quota FROM Forecast WHERE (sintYear >= v_intYear AND sintYear <= v_intYear)
      AND (tintMonth >= v_intFromMonth AND tintMonth <= v_intToMonth) and numCreatedBy = v_numUserCntID;
   else
	 --Quota          
      select   SUM(monQuota) INTO v_Quota FROM Forecast
      join AdditionalContactsInformation
      on numContactId = Forecast.numCreatedBy WHERE (sintYear >= v_intYear AND sintYear <= v_intYear)
      AND (tintMonth >= v_intFromMonth AND tintMonth <= v_intToMonth)
      AND numTeam is not null
      AND numTeam in(select F.numTeam from ForReportsByTeam F
         where F.numUserCntID = v_numUserCntID and F.numDomainID = v_numDomainID and F.tintType = v_intType);
   end if;

 
       
          
   IF v_tintRights = 3 then --All Records          
 
      IF v_intType = 0 then
             
  --Deals Won          
         select   SUM(monPAmount) INTO v_DealsWon FROM OpportunityMaster WHERE tintoppstatus = 1   and tintopptype = 1
         AND bintAccountClosingDate BETWEEN v_dtDateFrom AND v_dtDateTo
         AND numDomainId = v_numDomainID;          
              
            
  --Deals in Pipeline          
         select   SUM(monPAmount) INTO v_Pipeline FROM OpportunityMaster WHERE tintoppstatus = 0 and tintopptype = 1
         AND intPEstimatedCloseDate BETWEEN v_dtDateFrom AND v_dtDateTo
         AND numDomainId = v_numDomainID;
      ELSE          
  --Deals Won          
         select   SUM(OM.monPAmount) INTO v_DealsWon FROM AdditionalContactsInformation ACI INNER JOIN OpportunityMaster OM ON ACI.numContactId = OM.numrecowner WHERE tintoppstatus = 1   and tintopptype = 1
         AND OM.bintAccountClosingDate BETWEEN v_dtDateFrom AND v_dtDateTo
         AND OM.numDomainId = v_numDomainID
         AND ACI.numTeam is not null
         AND ACI.numTeam in(select F.numTeam from ForReportsByTeam F
            where F.numUserCntID = v_numUserCntID and F.numDomainID = v_numDomainID and F.tintType = v_intType);            
          
              
            
  --Deals in Pipeline          
         select   SUM(OM.monPAmount) INTO v_Pipeline FROM AdditionalContactsInformation ACI INNER JOIN OpportunityMaster OM ON ACI.numContactId = OM.numrecowner WHERE OM.tintoppstatus = 0     and tintopptype = 1
         AND OM.intPEstimatedCloseDate BETWEEN v_dtDateFrom AND v_dtDateTo
         AND OM.numDomainId = v_numDomainID
         AND ACI.numTeam is not null
         AND ACI.numTeam in(select F.numTeam from ForReportsByTeam F
            where F.numUserCntID = v_numUserCntID and F.numDomainID = v_numDomainID and F.tintType = v_intType);
      end if;
   end if;          
          
   IF v_tintRights = 2 then --Territory Records          
 
      IF v_intType = 0 then
             
  --Deals Won          
         select   SUM(OM.monPAmount) INTO v_DealsWon FROM OpportunityMaster OM INNER JOIN DivisionMaster DM ON OM.numDivisionId = DM.numDivisionID WHERE OM.tintoppstatus = 1  and tintopptype = 1
         AND OM.bintAccountClosingDate BETWEEN v_dtDateFrom AND v_dtDateTo
         AND OM.numDomainId = v_numDomainID
         AND DM.numTerID in(SELECT numTerritoryID FROM UserTerritory WHERE numUserCntID = v_numUserCntID);          
            
  --Deals in Pipeline          
         select   SUM(OM.monPAmount) INTO v_Pipeline FROM OpportunityMaster OM INNER JOIN DivisionMaster DM ON OM.numDivisionId = DM.numDivisionID WHERE OM.tintoppstatus = 0   and tintopptype = 1
         AND OM.intPEstimatedCloseDate BETWEEN v_dtDateFrom AND v_dtDateTo
         AND OM.numDomainId = v_numDomainID
         AND DM.numTerID in(SELECT numTerritoryID FROM UserTerritory WHERE numUserCntID = v_numUserCntID);
      ELSE          
  --Deals Won          
         select   SUM(OM.monPAmount) INTO v_DealsWon FROM AdditionalContactsInformation ACI
         INNER JOIN OpportunityMaster OM ON ACI.numContactId = OM.numrecowner INNER JOIN DivisionMaster DM ON DM.numDivisionID = OM.numDivisionId WHERE OM.tintoppstatus = 1 and tintopptype = 1
         AND OM.bintAccountClosingDate BETWEEN v_dtDateFrom AND v_dtDateTo
         AND OM.numDomainId = v_numDomainID
         AND DM.numTerID in(SELECT numTerritoryID FROM UserTerritory WHERE numUserCntID = v_numUserCntID)
         AND ACI.numTeam is not null
         AND ACI.numTeam in(select F.numTeam from ForReportsByTeam F
            where F.numUserCntID = v_numUserCntID and F.numDomainID = v_numDomainID and F.tintType = v_intType);            
          
            
  --Deals in Pipeline          
         select   SUM(OM.monPAmount) INTO v_Pipeline FROM AdditionalContactsInformation ACI
         INNER JOIN OpportunityMaster OM ON ACI.numContactId = OM.numrecowner INNER JOIN DivisionMaster DM ON DM.numDivisionID = OM.numDivisionId WHERE OM.tintoppstatus = 0 and tintopptype = 1
         AND OM.intPEstimatedCloseDate BETWEEN v_dtDateFrom AND v_dtDateTo
         AND OM.numDomainId = v_numDomainID
         AND DM.numTerID in(SELECT numTerritoryID FROM UserTerritory WHERE numUserCntID = v_numUserCntID)
         AND ACI.numTeam is not null
         AND ACI.numTeam in(select F.numTeam from ForReportsByTeam F
            where F.numUserCntID = v_numUserCntID and F.numDomainID = v_numDomainID and F.tintType = v_intType);
      end if;
   end if;          
          
   IF v_tintRights = 1 then --Owner Records          
 
      IF v_intType = 0 then
             
  --Deals Won          
         select   SUM(OM.monPAmount) INTO v_DealsWon FROM OpportunityMaster OM WHERE OM.tintoppstatus = 1    and tintopptype = 1
         AND OM.bintAccountClosingDate BETWEEN v_dtDateFrom AND v_dtDateTo
         AND OM.numDomainId = v_numDomainID
         AND OM.numrecowner = v_numUserCntID;          
          
            
  --Deals in Pipeline          
         select   SUM(OM.monPAmount) INTO v_Pipeline FROM OpportunityMaster OM WHERE OM.tintoppstatus = 0   and tintopptype = 1
         AND OM.intPEstimatedCloseDate BETWEEN v_dtDateFrom AND v_dtDateTo
         AND OM.numDomainId = v_numDomainID
         AND OM.numrecowner = v_numUserCntID;
      ELSE          
  --Deals Won          
         select   SUM(OM.monPAmount) INTO v_DealsWon FROM AdditionalContactsInformation ACI
         INNER JOIN OpportunityMaster OM ON ACI.numRecOwner = OM.numrecowner WHERE OM.tintoppstatus = 1      and tintopptype = 1
         AND OM.bintAccountClosingDate BETWEEN v_dtDateFrom AND v_dtDateTo
         AND OM.numDomainId = v_numDomainID
         AND ACI.numTeam is not null
         AND ACI.numTeam in(select F.numTeam from ForReportsByTeam F
            where F.numUserCntID = v_numUserCntID and F.numDomainID = v_numDomainID and F.tintType = v_intType);            
          
              
            
  --Deals in Pipeline          
         select   SUM(OM.monPAmount) INTO v_Pipeline FROM AdditionalContactsInformation ACI
         INNER JOIN OpportunityMaster OM ON ACI.numRecOwner = OM.numrecowner WHERE OM.tintoppstatus = 0 and tintopptype = 1
         AND OM.intPEstimatedCloseDate BETWEEN v_dtDateFrom AND v_dtDateTo
         AND OM.numDomainId = v_numDomainID
         AND ACI.numTeam is not null
         AND ACI.numTeam in(select F.numTeam from ForReportsByTeam F
            where F.numUserCntID = v_numUserCntID and F.numDomainID = v_numDomainID and F.tintType = v_intType);
      end if;
   end if;          
          
   v_Pipeline := coalesce(v_Pipeline,0);          
   v_Quota := coalesce(v_Quota,0);          
   v_DealsWon := coalesce(v_DealsWon,0);          
           
          
   v_SUM := v_Quota+v_DealsWon+v_Pipeline;           
   IF v_SUM > 0 then
 
      v_QuotaPer :=(v_Quota/v_SUM)*100;
      v_DealsWonPer :=(v_DealsWon/v_SUM)*100;
      v_PipelinePer :=(v_Pipeline/v_SUM)*100;
   ELSE
      v_QuotaPer := 0;
      v_DealsWonPer := 0;
      v_PipelinePer := 0;
   end if;          
   open SWV_RefCur for SELECT  v_QuotaPer As QuotaPer, v_Quota As Quota, v_DealsWonPer As DealsWonPer, v_DealsWon As DealsWon, v_PipelinePer AS PipelinePer, v_Pipeline AS Pipeline;
END; $$;












