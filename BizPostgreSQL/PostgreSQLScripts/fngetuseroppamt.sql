-- Function definition script fnGetUserOppAmt for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION fnGetUserOppAmt(v_numContactID NUMERIC)
RETURNS DECIMAL(20,5) LANGUAGE plpgsql
   AS $$
   DECLARE
   v_monTotal  DECIMAL(20,5);
BEGIN
   select   SUM(monPAmount) INTO v_monTotal FROM OpportunityMaster WHERE numContactId = v_numContactID;

   RETURN v_monTotal;
END; $$;

