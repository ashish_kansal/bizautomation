-- Stored procedure definition script USP_DeleteSite for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_DeleteSite(v_numSiteID   NUMERIC(9,0),
          v_numDomainID NUMERIC(9,0))
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   DELETE FROM PromotionOfferContacts WHERE numProId in(select numRuleID from  ShippingRules where numSiteID = v_numSiteID  and numDomainId = v_numDomainID) AND tintRecordType = 2;
   DELETE FROM PromotionOfferItems WHERE numProId in(select numRuleID from  ShippingRules where numSiteID = v_numSiteID  and numDomainId = v_numDomainID) AND tintRecordType = 2;
   DELETE  FROM ShippingRules
   WHERE
   numDomainId = v_numDomainID
   AND numSiteID = v_numSiteID;


   DELETE FROM SiteSubscriberDetails WHERE numSiteID = v_numSiteID;
   DELETE FROM SiteBreadCrumb WHERE       numSiteID = v_numSiteID;
    
   DELETE FROM StyleSheetDetails WHERE numCssID IN(SELECT numCssID FROM StyleSheets WHERE  numSiteID = v_numSiteID);
   DELETE FROM StyleSheets WHERE numSiteID = v_numSiteID AND numDomainID = v_numDomainID;
   DELETE FROM SiteTemplates WHERE  numSiteID = v_numSiteID AND numDomainID = v_numDomainID;
   DELETE FROM PageElementDetail WHERE numSiteID = v_numSiteID AND numDomainID = v_numDomainID;
   DELETE FROM SiteMenu WHERE numSiteID = v_numSiteID AND numDomainID = v_numDomainID;
   DELETE FROM SitePages WHERE numSiteID = v_numSiteID AND numDomainID = v_numDomainID;
   DELETE FROM SiteCategories WHERE numSiteID = v_numSiteID;
   
    
    --Delete Shipping Rules 
   DELETE FROM ShippingRuleStateList WHERE numRuleID IN(SELECT numRuleID FROM ShippingRules WHERE numDomainId = v_numDomainID AND numSiteID = v_numSiteID);   
           
   DELETE FROM ShippingServiceTypes WHERE numRuleID IN(SELECT numRuleID FROM ShippingRules WHERE  numDomainId = v_numDomainID AND numSiteID = v_numSiteID);  
  
   DELETE  FROM ShippingRules
   WHERE   numDomainId = v_numDomainID
   AND numSiteID = v_numSiteID;
    
    --Delete Review ,Rating  and ReviewDetail
   DELETE FROM Ratings
   WHERE       numSiteID = v_numSiteID
   AND numDomainID = v_numDomainID;
   DELETE FROM ReviewDetail
   WHERE       numReviewId IN(SELECT numReviewId FROM Review WHERE numSiteId = v_numSiteID AND numDomainID = v_numDomainID);
   DELETE FROM Review
   WHERE       numSiteId = v_numSiteID AND numDomainID = v_numDomainID;

   DELETE FROM Sites
   WHERE       numSiteID = v_numSiteID
   AND numDomainID = v_numDomainID;

   RETURN;
END; $$;



