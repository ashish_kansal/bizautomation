DROP FUNCTION IF EXISTS USP_AdvancedSearchOpp;

CREATE OR REPLACE FUNCTION USP_AdvancedSearchOpp(v_WhereCondition VARCHAR(4000) DEFAULT '',
v_numDomainID NUMERIC(9,0) DEFAULT 0,
v_numUserCntID NUMERIC(9,0) DEFAULT 0,
v_CurrentPage INTEGER DEFAULT NULL,
v_PageSize INTEGER DEFAULT NULL,                                                                  
INOUT v_TotRecs INTEGER  DEFAULT NULL,                                                                                                           
v_columnSortOrder VARCHAR(10) DEFAULT NULL,    
v_ColumnName VARCHAR(50) DEFAULT '',    
v_SortCharacter CHAR(1) DEFAULT NULL,                
v_SortColumnName VARCHAR(50) DEFAULT '',    
v_LookTable VARCHAR(10) DEFAULT '',
v_strMassUpdate VARCHAR(2000) DEFAULT '',
v_vcRegularSearchCriteria TEXT DEFAULT '',
v_vcCustomSearchCriteria TEXT DEFAULT '',
v_vcDisplayColumns TEXT DEFAULT '',
v_GetAll BOOLEAN DEFAULT false, INOUT SWV_RefCur refcursor default null, INOUT SWV_RefCur2 refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_tintOrder  SMALLINT;                                  
   v_vcFormFieldName  VARCHAR(50);                                  
   v_vcListItemType  VARCHAR(3);                             
   v_vcListItemType1  VARCHAR(3);                                 
   v_vcAssociatedControlType  VARCHAR(20);                                  
   v_numListID  NUMERIC(9,0);                                  
   v_vcDbColumnName  VARCHAR(30);                                   
   v_ColumnSearch  VARCHAR(10);
   v_vcLookBackTableName  VARCHAR(50);
   v_bitIsSearchBizDoc  BOOLEAN;




   v_strSql  VARCHAR(8000);
   v_bitBillAddressJoinAdded  BOOLEAN;
   v_bitShipAddressJoinAdded  BOOLEAN;
   v_vcLocationID  VARCHAR(100) DEFAULT '2,6';

	-- IF USER HAS SELECTED ITS OWN DISPLAY COLUMNS THEN USE IT
   v_bitCustom  BOOLEAN;
   v_numFieldGroupID  INTEGER;
   v_vcColumnName  VARCHAR(200);
   v_numFieldID  NUMERIC(18,0);

   v_firstRec  INTEGER;                                                                        
   v_lastRec  INTEGER;                                                                        
   v_from  VARCHAR(8000);
   v_strReplace  VARCHAR(2000);
   v_Prefix  VARCHAR(10);
   SWV_RowCount INTEGER;
BEGIN
   IF POSITION('OpportunityBizDocs' IN v_WhereCondition) > 0 then

      v_bitIsSearchBizDoc := true;
   ELSE
      v_bitIsSearchBizDoc := false;
   end if;
 --Added By Sachin Sadhu||Date:12thJune2014
  --Purpose:To add Functionaly :Search with Notes Field-OpportunityBizDocItems
  --Set Manually To fullfill "All" Selection in Search
   IF POSITION('vcNotes' IN v_WhereCondition) > 0 then
	
      v_bitIsSearchBizDoc := true;
   end if;
--End of script

   if (v_SortCharacter <> '0' and v_SortCharacter <> '') then 
      v_ColumnSearch := v_SortCharacter;
   end if;   
   BEGIN
      CREATE TEMP SEQUENCE tt_tempTableAdvancedSearchOpp_seq;
      EXCEPTION WHEN OTHERS THEN
         NULL;
   END;
   drop table IF EXISTS tt_TEMPTABLEAdvancedSearchOpp CASCADE;
   Create TEMPORARY TABLE tt_TEMPTABLEAdvancedSearchOpp 
   ( 
	  ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1) PRIMARY KEY,
      numOppID NUMERIC(18,0),
      tintCRMType SMALLINT,
      numcontactid NUMERIC(18,0),
      numDivisionID NUMERIC(18,0),
      numOppBizDocID VARCHAR(15)
   );  
  
   v_strSql := 'select OppMas.numOppId, DM.tintCRMType, OppMas.numDivisionId, ADC.numContactId ';
 
   IF v_bitIsSearchBizDoc = true then --Added to eliminate duplicate records 
      v_strSql := coalesce(v_strSql,'') || ' ,OpportunityBizDocs.numOppBizDocsId ';
   ELSE
      v_strSql := coalesce(v_strSql,'') || ' ,0 AS numOppBizDocsId ';
   end if;

   IF v_SortColumnName = 'CalAmount' then

      ALTER TABLE tt_TEMPTABLEAdvancedSearchOpp add CalAmount DECIMAL(20,5);
      v_strSql := coalesce(v_strSql,'') || ' ,GetDealAmount(OppMas.numOppId,TIMEZONE(''UTC'',now()),0) as CalAmount';
   end if;	

   IF v_SortColumnName = 'monDealAmount' then

      ALTER TABLE tt_TEMPTABLEAdvancedSearchOpp add monDealAmount DECIMAL(20,5);
      v_strSql := coalesce(v_strSql,'') || ' ,coalesce((SELECT SUM(coalesce(BD.monDealAmount,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = OppMas.numOppId AND coalesce(bitAuthoritativeBizDocs,0) = 1),0) as monDealAmount';
   end if;	

   IF v_SortColumnName = 'monAmountPaid' then

      ALTER TABLE tt_TEMPTABLEAdvancedSearchOpp add monAmountPaid DECIMAL(20,5);
      v_strSql := coalesce(v_strSql,'') || ' ,coalesce((SELECT SUM(coalesce(BD.monAmountPaid,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = OppMas.numOppId AND coalesce(bitAuthoritativeBizDocs,0) = 1),0) AS monAmountPaid';
   end if;	

   v_strSql := coalesce(v_strSql,'') || ' from   
OpportunityMaster OppMas   
Join AdditionalContactsinformation ADC on OppMas.numContactId = ADC.numContactId  
join DivisionMaster DM on Dm.numDivisionId = OppMas.numDivisionId  
JOIN CompanyInfo C ON DM.numCompanyID = C.numCompanyId   
';  
   IF v_bitIsSearchBizDoc = true then --Added to eliminate duplicate records 

      IF POSITION('vcNotes' IN v_WhereCondition) > 0 then
		
         v_strSql := coalesce(v_strSql,'') || ' left join OpportunityBizDocs on OppMas.numOppId = OpportunityBizDocs.numOppId left join OpportunityBizDocItems on OpportunityBizDocItems.numOppBizDocID = OpportunityBizDocs.numOppBizDocsId ';
      ELSE
         v_strSql := coalesce(v_strSql,'') || ' left join OpportunityBizDocs on OppMas.numOppId = OpportunityBizDocs.numOppId ';
      end if;
   end if;
	
   IF v_SortColumnName = 'numBillCountry' OR v_SortColumnName = 'numBillState'
   OR v_ColumnName = 'numBillCountry' OR v_ColumnName = 'numBillState' then

      v_strSql := coalesce(v_strSql,'') || ' left Join AddressDetails AD1 on AD1.numRecordID = DM.numDivisionID and AD1.tintAddressOf = 2 and AD1.tintAddressType = 1 AND AD1.bitIsPrimary = true and AD1.numDomainID = ADC.numDomainID ';
   ELSEIF v_SortColumnName = 'numShipCountry' OR v_SortColumnName = 'numShipState'
   OR v_ColumnName = 'numShipCountry' OR v_ColumnName = 'numShipState'
   then

      v_strSql := coalesce(v_strSql,'') || ' left Join AddressDetails AD2 on AD2.numRecordID = DM.numDivisionID and AD2.tintAddressOf = 2 and AD2.tintAddressType = 2 AND AD2.bitIsPrimary = true and AD2.numDomainID = ADC.numDomainID ';
   end if;

   if (v_SortColumnName <> '') then
  
      select   vcListItemType INTO v_vcListItemType1 from View_DynamicDefaultColumns where vcDbColumnName = v_SortColumnName and numFormId = 15 and numDomainID = v_numDomainID    LIMIT 1;
      if v_vcListItemType1 = 'LI' then
  
         IF v_SortColumnName = 'numBillCountry' then
			
            v_strSql := coalesce(v_strSql,'') || ' left Join ListDetails L2 on L2.numListItemID = AD1.numCountry';
         ELSEIF v_SortColumnName = 'numShipCountry'
         then
			
            v_strSql := coalesce(v_strSql,'') || ' left Join ListDetails L2 on L2.numListItemID = AD2.numCountry';
         ELSE
            v_strSql := coalesce(v_strSql,'') || ' left Join ListDetails L2 on L2.numListItemID =' || coalesce(v_SortColumnName,'');
         end if;
      ELSEIF v_vcListItemType1 = 'S'
      then
    
         IF v_SortColumnName = 'numBillState' then
		
            v_strSql := coalesce(v_strSql,'') || ' left join State S2 on S2.numStateID = AD1.numState';
         ELSEIF v_SortColumnName = 'numShipState'
         then
	    
            v_strSql := coalesce(v_strSql,'') || ' left join State S2 on S2.numStateID = AD2.numState';
         end if;
      end if;
   end if;                              
   v_strSql := coalesce(v_strSql,'') || ' where OppMas.numDomainID  = ' || SUBSTR(CAST(v_numDomainID AS VARCHAR(15)),1,15) ||   coalesce(v_WhereCondition,'');                                      
     
  
   if (v_ColumnName <> '' and  v_ColumnSearch <> '') then

      if v_vcListItemType = 'LI' then
    
         IF v_ColumnName = 'numBillCountry' then
			
            v_strSql := coalesce(v_strSql,'') || ' left Join ListDetails L1 on L1.numListItemID=AD1.numCountry';
         ELSEIF v_ColumnName = 'numShipCountry'
         then
			
            v_strSql := coalesce(v_strSql,'') || ' left Join ListDetails L1 on L1.numListItemID=AD2.numCountry';
         ELSE
            v_strSql := coalesce(v_strSql,'') || ' and L1.vcData ILIKE ''' || coalesce(v_ColumnSearch,'') || '%''';
         end if;
      ELSEIF v_vcListItemType = 'S'
      then
    
         IF v_ColumnName = 'numBillState' then
		
            v_strSql := coalesce(v_strSql,'') || ' left join State S1 on S1.numStateID=AD1.numState';
         ELSEIF v_ColumnName = 'numShipState'
         then
	    
            v_strSql := coalesce(v_strSql,'') || ' left join State S1 on S1.numStateID=AD2.numState';
         end if;
      else 
         v_strSql := coalesce(v_strSql,'') || ' and ' ||
         case when v_ColumnName = 'numAssignedTo' then 'OppMas.' || coalesce(v_ColumnName,'')
         else v_ColumnName end
         || ' ILIKE ''' || coalesce(v_ColumnSearch,'')  || '%''';
      end if;
   end if;                              
   if (v_SortColumnName <> '') then
  
      if v_vcListItemType1 = 'LI' then
    
         v_strSql := coalesce(v_strSql,'') || ' order by L2.vcData ' || coalesce(v_columnSortOrder,'');
      ELSEIF v_vcListItemType1 = 'S'
      then
		
         v_strSql := coalesce(v_strSql,'') || ' order by S2.vcState ' || coalesce(v_columnSortOrder,'');
      ELSE
		--SET @strSql  = REPLACE(@strSql,'distinct','')
         v_strSql := coalesce(v_strSql,'') || ' order by ' || coalesce(v_SortColumnName,'') || ' ' || coalesce(v_columnSortOrder,'');
      end if;
   end if;   
  
   EXECUTE 'insert into tt_TEMPTABLEAdvancedSearchOpp (numOppID,
      tintCRMType,
      numcontactid,
      numDivisionID,
      numOppBizDocID' || CASE WHEN LOWER(v_SortColumnName) = LOWER('CalAmount') THEN ',CalAmount' WHEN LOWER(v_SortColumnName) = LOWER('monDealAmount') THEN ',monDealAmount' WHEN LOWER(v_SortColumnName) = LOWER('monAmountPaid') THEN ',monAmountPaid' ELSE '' END  || ')' || v_strSql;    
   RAISE NOTICE '%',v_strSql; 
   RAISE NOTICE '====================================================';  
         
         --SELECT * FROM #tempTable               
   v_strSql := '';                                  
                                  
   v_bitBillAddressJoinAdded := false;
   v_bitShipAddressJoinAdded := false;
                      
   v_tintOrder := 0;                                  
   v_WhereCondition := '';                               
   v_strSql := 'select OppMas.numOppId, DM.tintCRMType, OppMas.numDivisionID, ADC.numContactId, OppMas.numRecOwner  ';
   IF v_bitIsSearchBizDoc = true then --Added to eliminate duplicate records 

      v_strSql := coalesce(v_strSql,'') || ' ,OpportunityBizDocs.numOppBizDocsId ';
   end if;

   DROP TABLE IF EXISTS tt_TEMPSELECTEDCOLUMNS CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPSELECTEDCOLUMNS
   (
      numFieldID NUMERIC(18,0),
      bitCustomField BOOLEAN,
      tintOrder INTEGER,
      intColumnWidth DOUBLE PRECISION
   );
   IF LENGTH(coalesce(v_vcDisplayColumns,'')) > 0 then
      BEGIN
         CREATE TEMP SEQUENCE tt_TempIDs_seq INCREMENT BY 1 START WITH 1;
         EXCEPTION WHEN OTHERS THEN
            NULL;
      END;
      DROP TABLE IF EXISTS tt_TEMPIDS CASCADE;
      CREATE TEMPORARY TABLE tt_TEMPIDS
      (
         ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1),
         vcFieldID VARCHAR(300)
      );
      INSERT INTO tt_TEMPIDS(vcFieldID)
      SELECT
      OutParam
      FROM
      SplitString(v_vcDisplayColumns,',');
      INSERT INTO tt_TEMPSELECTEDCOLUMNS(numFieldID,
			bitCustomField,
			tintOrder,
			intColumnWidth)
      SELECT
      numFormFieldID
			,bitCustomField
			,(SELECT(ID::bigint -1) FROM tt_TEMPIDS T3 WHERE T3.vcFieldID = TEMP.vcFieldID)
			,0
      FROM(SELECT
         numFieldID as numFormFieldID,
				false AS bitCustomField,
				CONCAT(numFieldID,'~0') AS vcFieldID
         FROM
         View_DynamicDefaultColumns
         WHERE
         numFormId = 15
         AND bitInResults = true
         AND bitDeleted = false
         AND numDomainID = v_numDomainID
         UNION
         SELECT
         c.Fld_id AS numFormFieldID
				,true AS bitCustomField
				,CONCAT(Fld_id,'~1') AS vcFieldID
         FROM
         CFW_Fld_Master c
         LEFT JOIN
         CFW_Validation V ON V.numFieldId = c.Fld_id
         JOIN
         CFW_Loc_Master L on c.Grp_id = L.Loc_id
         WHERE
         c.numDomainID = v_numDomainID
         AND GRP_ID IN(SELECT ID FROM SplitIDs(v_vcLocationID,','))) TEMP
      WHERE
      vcFieldID IN(SELECT vcFieldID FROM tt_TEMPIDS);
   end if;

	-- IF USER SELECTED DISPLAY COLUMNS IS NOT AVAILABLE THAN GET COLUMNS CONFIGURED FROM SEARCH RESULT GRID
   IF(SELECT COUNT(*) FROM tt_TEMPSELECTEDCOLUMNS) = 0 then
	
      INSERT INTO tt_TEMPSELECTEDCOLUMNS(numFieldID,
			bitCustomField,
			tintOrder,
			intColumnWidth)
      SELECT
      numFormFieldID
			,bitCustomField
			,tintOrder
			,intColumnWidth
      FROM(SELECT
         A.numFormFieldID,
				false AS bitCustomField,
				A.tintOrder,
				A.intColumnWidth
         FROM    AdvSerViewConf A
         JOIN View_DynamicDefaultColumns D ON D.numFieldID = A.numFormFieldID
         WHERE   A.numFormID = 15
         AND D.bitInResults = true
         AND D.bitDeleted = false
         AND D.numDomainID = v_numDomainID
         AND D.numFormId = 15
         AND A.numDomainID = v_numDomainID
         AND A.numUserCntID = v_numUserCntID
         AND coalesce(A.bitCustom,false) = false
         UNION
         SELECT
         A.numFormFieldID,
				true AS bitCustomField,
				A.tintOrder,
				A.intColumnWidth
         FROM    AdvSerViewConf A
         JOIN CFW_Fld_Master C ON C.Fld_id = A.numFormFieldID
         WHERE   A.numFormID = 15
         AND C.numDomainID = v_numDomainID
         AND A.numDomainID = v_numDomainID
         AND A.numUserCntID = v_numUserCntID
         AND coalesce(A.bitCustom,false) = true) T1
      ORDER BY
      tintOrder;
   end if;

	-- IF USER HASN'T CONFIGURED COLUMNS FROM SEARCH RESULT GRID THEN RETURN DEFAULT COLUMNS
   IF(SELECT COUNT(*) FROM tt_TEMPSELECTEDCOLUMNS) = 0 then
	
      INSERT INTO tt_TEMPSELECTEDCOLUMNS(numFieldID,
			bitCustomField,
			tintOrder,
			intColumnWidth)
      SELECT
      numFieldID,
			CAST(0 AS BOOLEAN),
			1,
			0
      FROM
      View_DynamicDefaultColumns
      WHERE
      numFormId = 15
      AND numDomainID = v_numDomainID
      AND numFieldID = 96;
   end if;

   select   tintOrder, numFormFieldID, vcDbColumnName, vcFieldName, vcAssociatedControlType, vcListItemType, numListID, vcLookBackTableName, bitCustom, numGroupID INTO v_tintOrder,v_numFieldID,v_vcDbColumnName,v_vcFormFieldName,v_vcAssociatedControlType,
   v_vcListItemType,v_numListID,v_vcLookBackTableName,v_bitCustom,
   v_numFieldGroupID FROM(SELECT
      T1.tintOrder::bigint+1 AS tintOrder,
			D.numFieldID AS numFormFieldID,
			vcDbColumnName,
			vcFieldName,
			vcAssociatedControlType,
			vcListItemType,
			numListID,
			vcLookBackTableName,
			false AS bitCustom,
			0 AS numGroupID
      FROM
      View_DynamicDefaultColumns D
      INNER JOIN
      tt_TEMPSELECTEDCOLUMNS T1
      ON
      D.numFieldID = T1.numFieldID
      WHERE
      D.numDomainID = v_numDomainID
      AND D.numFormId = 15
      AND coalesce(T1.bitCustomField,false) = false
      UNION
      SELECT
      T1.tintOrder::bigint+1 AS tintOrder,
			Fld_id AS numFormFieldID,
			CONCAT('Cust',Fld_id),
			Fld_label,
			c.fld_type,
			'',
			c.numlistid,
			'',
			true AS bitCustom,
			Grp_id
      FROM
      CFW_Fld_Master c
      INNER JOIN
      tt_TEMPSELECTEDCOLUMNS T1
      ON
      c.Fld_id = T1.numFieldID
      WHERE
      c.numDomainID = v_numDomainID
      AND coalesce(T1.bitCustomField,false) = true) T1    ORDER BY
   tintOrder asc LIMIT 1;                                                             

   while v_tintOrder > 0 LOOP
      IF v_bitCustom = false then
         IF v_vcLookBackTableName = 'OpportunityMaster' then
            v_Prefix := 'OppMas.';
         end if;
         v_vcColumnName := coalesce(v_vcDbColumnName,'') || '~' || SUBSTR(CAST(v_numFieldID AS VARCHAR(10)),1,10) || '~0';
         if v_vcAssociatedControlType = 'SelectBox' then
        
            IF v_vcDbColumnName = 'tintSource' then
            
               v_strSql := coalesce(v_strSql,'')
               || ',fn_GetOpportunitySourceValue(COALESCE(OppMas.tintSource,0),COALESCE(OppMas.tintSourceType,0),OppMas.numDomainID) '
               || ' "' || coalesce(v_vcColumnName,'') || '"';
            ELSEIF v_vcListItemType = 'U'
            then
			
               v_strSql := coalesce(v_strSql,'') || ',fn_GetContactName(' || coalesce(v_Prefix,'') || coalesce(v_vcDbColumnName,'') || ') AS "' || coalesce(v_vcColumnName,'') || '"';
            ELSEIF v_vcListItemType = 'LI'
            then
			
               IF v_numListID = 40 then--Country
				
                  v_strSql := coalesce(v_strSql,'') || ',L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.vcData' || ' "' || coalesce(v_vcColumnName,'') || '"';
                  IF v_vcDbColumnName = 'numBillCountry' then
				
                     IF v_bitBillAddressJoinAdded = false then
					
                        v_WhereCondition := coalesce(v_WhereCondition,'') || ' left Join AddressDetails AD1 on AD1.numRecordId= DM.numDivisionID and AD1.tintAddressOf=2 and AD1.tintAddressType=1 AND AD1.bitIsPrimary=true and AD1.numDomainID= ADC.numDomainID ';
                        v_bitBillAddressJoinAdded := true;
                     end if;
                     v_WhereCondition := coalesce(v_WhereCondition,'') || ' left Join ListDetails L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || ' on L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.numListItemID=AD1.numCountry';
                  ELSEIF v_vcDbColumnName = 'numShipCountry'
                  then
				
                     IF v_bitShipAddressJoinAdded = false then
					
                        v_WhereCondition := coalesce(v_WhereCondition,'') || ' left Join AddressDetails AD2 on AD2.numRecordId= DM.numDivisionID and AD2.tintAddressOf=2 and AD2.tintAddressType=2 AND AD2.bitIsPrimary=true and AD2.numDomainID= ADC.numDomainID ';
                        v_bitShipAddressJoinAdded := true;
                     end if;
                     v_WhereCondition := coalesce(v_WhereCondition,'') || ' left Join ListDetails L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || ' on L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.numListItemID=AD2.numCountry';
                  end if;
               ELSE
                  v_strSql := coalesce(v_strSql,'') || ',L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.vcData' || ' "' || coalesce(v_vcColumnName,'') || '"';
                  if v_vcDbColumnName = 'numSalesOrPurType' AND v_numListID = 46 then
                     v_WhereCondition := coalesce(v_WhereCondition,'') || ' left Join ListDetails L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || ' on L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.numListItemID=OppMas.numSalesOrPurType and OppMas.tintOppType = 2 ';
                  ELSEIF v_vcDbColumnName = 'numSalesOrPurType' AND v_numListID = 45
                  then
                     v_WhereCondition := coalesce(v_WhereCondition,'') || ' left Join ListDetails L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || ' on L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.numListItemID=OppMas.numSalesOrPurType and OppMas.tintOppType = 1 ';
                  else
                     v_WhereCondition := coalesce(v_WhereCondition,'') || ' left Join ListDetails L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || ' on L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.numListItemID=' || coalesce(v_vcDbColumnName,'');
                  end if;
               end if;
            end if;
         ELSEIF v_vcAssociatedControlType = 'ListBox'
         then
		
            if v_vcListItemType = 'S' then
			  
               v_strSql := coalesce(v_strSql,'') || ',S' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.vcState' || ' "' || coalesce(v_vcColumnName,'') || '"';
               IF v_vcDbColumnName = 'numBillState' then
				
                  IF v_bitBillAddressJoinAdded = false then
					
                     v_WhereCondition := coalesce(v_WhereCondition,'') || ' left Join AddressDetails AD1 on AD1.numRecordId= DM.numDivisionID and AD1.tintAddressOf=2 and AD1.tintAddressType=1 AND AD1.bitIsPrimary=true and AD1.numDomainID= ADC.numDomainID ';
                     v_bitBillAddressJoinAdded := true;
                  end if;
                  v_WhereCondition := coalesce(v_WhereCondition,'') || ' left join State S' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || ' on S' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.numStateID=AD1.numState';
               ELSEIF v_vcDbColumnName = 'numShipState'
               then
				
                  IF v_bitShipAddressJoinAdded = false then
					
                     v_WhereCondition := coalesce(v_WhereCondition,'') || ' left Join AddressDetails AD2 on AD2.numRecordId= DM.numDivisionID and AD2.tintAddressOf=2 and AD2.tintAddressType=2 AND AD2.bitIsPrimary=true and AD2.numDomainID= ADC.numDomainID ';
                     v_bitShipAddressJoinAdded := true;
                  end if;
                  v_WhereCondition := coalesce(v_WhereCondition,'') || ' left join State S' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || ' on S' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.numStateID=AD2.numState';
               end if;
            end if;
         ELSEIF v_vcAssociatedControlType = 'CheckBox'
         then
		   
            v_strSql := coalesce(v_strSql,'') || ',case when COALESCE(' || coalesce(v_vcDbColumnName,'') || ',0)=0 then ''No'' when COALESCE(' || coalesce(v_vcDbColumnName,'') || ',0)=1 then ''Yes'' end  "' || coalesce(v_vcColumnName,'') || '"';
         else
            IF v_bitIsSearchBizDoc = false then
					
               IF v_vcLookBackTableName = 'OpportunityBizDocs' AND v_vcDbColumnName != 'monDealAmount' AND v_vcDbColumnName != 'monAmountPaid' then
						  
                  v_vcDbColumnName := '''''';
               end if;
            end if;
            v_strSql := coalesce(v_strSql,'') || ',' ||
            case
            when v_vcLookBackTableName = 'OpportunityMaster' AND v_vcDbColumnName = 'monDealAmount' then 'OppMas.monDealAmount'
            when v_vcDbColumnName = 'numAge' then 'EXTRACT(YEAR FROM timezone(''utc'', now()))-EXTRACT(YEAR FROM bintDOB)'
            when v_vcDbColumnName = 'numDivisionID' then 'DM.numDivisionID'
            when v_vcDbColumnName = 'vcProgress' then  ' fn_OppTotalProgress(OppMas.numOppId)'
            when v_vcDbColumnName = 'intPEstimatedCloseDate' or v_vcDbColumnName = 'dtDateEntered' or v_vcDbColumnName = 'bintShippedDate'  then
               'FormatedDateFromDate(' || coalesce(v_vcDbColumnName,'') || ',' || SUBSTR(CAST(v_numDomainID AS VARCHAR(10)),1,10) || ')'
            WHEN v_vcDbColumnName = 'CalAmount' THEN 'getdealamount(OppMas.numOppId,timezone(''utc'', now()),0)'
            WHEN v_vcLookBackTableName = 'OpportunityBizDocs' AND v_vcDbColumnName = 'monDealAmount' THEN 'COALESCE((SELECT SUM(COALESCE(BD.monDealAmount,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = OppMas.numOppId AND COALESCE(bitAuthoritativeBizDocs,0)=1),0)'
            WHEN v_vcLookBackTableName = 'OpportunityBizDocs' AND v_vcDbColumnName = 'monAmountPaid' THEN 'COALESCE((SELECT SUM(COALESCE(BD.monAmountPaid,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = OppMas.numOppId AND COALESCE(bitAuthoritativeBizDocs,0)=1),0)'
            else v_vcDbColumnName end || ' "' || coalesce(v_vcColumnName,'') || '"';
         end if;
      ELSEIF v_bitCustom = true
      then
	
		--SET @vcColumnName= CONCAT(@vcFormFieldName,'~','Cust',@numFieldId)
         v_vcColumnName := coalesce(v_vcDbColumnName,'') || '~' || SUBSTR(CAST(v_numFieldID AS VARCHAR(10)),1,10) || '~1';
         IF v_vcAssociatedControlType = 'TextBox' or v_vcAssociatedControlType = 'TextArea' then
		
            v_strSql := coalesce(v_strSql,'') || ',CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.Fld_Value  "' || coalesce(v_vcColumnName,'') || '"';
            v_WhereCondition := coalesce(v_WhereCondition,'')
            || ' left Join CFW_Fld_Values_Opp CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3)
            || ' on CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3)
            || '.Fld_Id=' || SUBSTR(CAST(v_numFieldID AS VARCHAR(10)),1,10)
            || 'and CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3)
            || '.RecId=OppMas.numOppId ';
         ELSEIF v_vcAssociatedControlType = 'CheckBox'
         then
		
            v_strSql := coalesce(v_strSql,'') || ',case when COALESCE(CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.Fld_Value,0)='''' then 0 else  COALESCE(CFW'
            || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.Fld_Value,0) end "' || coalesce(v_vcColumnName,'') || '"';
            v_WhereCondition := coalesce(v_WhereCondition,'') || ' left Join CFW_Fld_Values_Opp CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '             
			on CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.Fld_Id=' || SUBSTR(CAST(v_numFieldID AS VARCHAR(10)),1,10) || 'and CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.RecId=OppMas.numOppId ';
         ELSEIF v_vcAssociatedControlType = 'DateField'
         then
		
            v_strSql := coalesce(v_strSql,'') || ',FormatedDateFromDate(CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.Fld_Value,' || SUBSTR(CAST(v_numDomainID AS VARCHAR(10)),1,10) || ')  "' || coalesce(v_vcColumnName,'') || '"';
            v_WhereCondition := coalesce(v_WhereCondition,'') || ' left Join CFW_Fld_Values_Opp CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '                 
			on CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.Fld_Id=' || SUBSTR(CAST(v_numFieldID AS VARCHAR(10)),1,10) || 'and CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.RecId=OppMas.numOppId ';
         ELSEIF v_vcAssociatedControlType = 'SelectBox'
         then
		
            v_vcDbColumnName := 'DCust' || SUBSTR(CAST(v_numFieldID AS VARCHAR(10)),1,10);
            v_strSql := coalesce(v_strSql,'') || ',L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.vcData' || ' "' || coalesce(v_vcColumnName,'') || '"';
            v_WhereCondition := coalesce(v_WhereCondition,'') || ' left Join CFW_Fld_Values_Opp CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '                 
				on CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.Fld_Id=' || SUBSTR(CAST(v_numFieldID AS VARCHAR(10)),1,10) || 'and CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.RecId=OppMas.numOppId ';
            v_WhereCondition := coalesce(v_WhereCondition,'') || ' left Join ListDetails L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || ' on CAST(L' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.numListItemID AS TEXT)=CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.Fld_Value';
         ELSEIF v_vcAssociatedControlType = 'CheckBoxList'
         then
		
            v_strSql := coalesce(v_strSql,'') || ',COALESCE((SELECT string_agg(vcData,'', '') FROM ListDetails WHERE numListID= ' || SUBSTR(CAST(v_numListID AS VARCHAR(30)),1,30) || ' AND numlistitemid IN (SELECT COALESCE(Id,0) FROM SplitIDs(CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.Fld_Value,'','')),''''))  "' || coalesce(v_vcColumnName,'') || '"';
            v_WhereCondition := coalesce(v_WhereCondition,'')
            || ' left Join CFW_Fld_Values_Opp CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3)
            || ' on CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3)
            || '.Fld_Id=' || SUBSTR(CAST(v_numFieldID AS VARCHAR(10)),1,10)
            || 'and CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3)
            || '.RecId=OppMas.numOppId ';
         end if;
      end if;
      select   tintOrder, numFormFieldID, vcDbColumnName, vcFieldName, vcAssociatedControlType, vcListItemType, numListID, vcLookBackTableName, bitCustom, numGroupID INTO v_tintOrder,v_numFieldID,v_vcDbColumnName,v_vcFormFieldName,v_vcAssociatedControlType,
      v_vcListItemType,v_numListID,v_vcLookBackTableName,v_bitCustom,
      v_numFieldGroupID FROM(SELECT
         T1.tintOrder::bigint+1 AS tintOrder,
			D.numFieldID AS numFormFieldID,
			vcDbColumnName,
			vcFieldName,
			vcAssociatedControlType,
			vcListItemType,
			numListID,
			vcLookBackTableName,
			false AS bitCustom,
			0 AS numGroupID
         FROM
         View_DynamicDefaultColumns D
         INNER JOIN
         tt_TEMPSELECTEDCOLUMNS T1
         ON
         D.numFieldID = T1.numFieldID
         WHERE
         D.numDomainID = v_numDomainID
         AND D.numFormId = 15
         AND coalesce(T1.bitCustomField,false) = false
         AND T1.tintOrder > v_tintOrder -1
         UNION
         SELECT
         T1.tintOrder::bigint+1 AS tintOrder,
			Fld_id AS numFormFieldID,
			CONCAT('Cust',Fld_id),
			Fld_label,
			c.fld_type,
			'',
			c.numlistid,
			'',
			true AS bitCustom,
			Grp_id
         FROM
         CFW_Fld_Master c
         INNER JOIN
         tt_TEMPSELECTEDCOLUMNS T1
         ON
         c.Fld_id = T1.numFieldID
         WHERE
         c.numDomainID = v_numDomainID
         AND coalesce(T1.bitCustomField,false) = true
         AND T1.tintOrder > v_tintOrder -1) T1    ORDER BY
      tintOrder asc LIMIT 1;
      GET DIAGNOSTICS SWV_RowCount = ROW_COUNT;
      if SWV_RowCount = 0 then 
         v_tintOrder := 0;
      end if;
   END LOOP;   
   v_firstRec :=(v_CurrentPage::bigint -1)*v_PageSize::bigint;                          
   v_lastRec := (v_CurrentPage*v_PageSize+1);
   select count(*) INTO v_TotRecs from tt_TEMPTABLEAdvancedSearchOpp;   


   v_from := ' from OpportunityMaster OppMas   
Join AdditionalContactsinformation ADC on OppMas.numContactId = ADC.numContactId  
join DivisionMaster DM on Dm.numDivisionId = OppMas.numDivisionId  
JOIN CompanyInfo C ON DM.numCompanyID = C.numCompanyId ';

   IF v_bitIsSearchBizDoc = true then --Added to eliminate duplicate records 
 --Added By Sachin Sadhu||Date:12thJune2014
  --Purpose:To add Functionaly :Search with Notes Field-OpportunityBizDocItems

      IF POSITION('vcNotes' IN v_WhereCondition) > 0 then
		
         v_from := coalesce(v_from,'') || ' left join OpportunityBizDocs on OppMas.numOppId = OpportunityBizDocs.numOppId left join OpportunityBizDocItems on OpportunityBizDocItems.numOppBizDocID=OpportunityBizDocs.numOppBizDocsId ';
      ELSE
         v_from := coalesce(v_from,'') || ' left join OpportunityBizDocs on OppMas.numOppId = OpportunityBizDocs.numOppId ';
      end if;
   end if;
	--End of Script

   v_strSql := coalesce(v_strSql,'') || coalesce(v_from,'');

 
 
   if LENGTH(v_strMassUpdate) > 1 then
      v_strReplace := case when v_LookTable = 'OppMas' OR v_LookTable = 'Opportunit' then 'OppMas.numOppID' ELSE '' end  || coalesce(v_from,'');
      v_strMassUpdate := replace(v_strMassUpdate,'@$replace',v_strReplace);
      RAISE NOTICE '%',v_strMassUpdate;
      EXECUTE v_strMassUpdate;
   end if;         




   v_strSql := coalesce(v_strSql,'') || coalesce(v_WhereCondition,'');


   v_strSql := coalesce(v_strSql,'') || ' join tt_TEMPTABLEAdvancedSearchOpp T on T.numOppID=OppMas.numOppID WHERE 1=1 '; 
	
   IF coalesce(v_GetAll,false) = false then
	
      v_strSql := coalesce(v_strSql,'') || ' AND ID > ' || SUBSTR(CAST(v_firstRec AS VARCHAR(10)),1,10) || ' and ID <' || SUBSTR(CAST(v_lastRec AS VARCHAR(10)),1,10);
   end if;

	
   IF v_bitIsSearchBizDoc = true then
 
      v_strSql := coalesce(v_strSql,'') || '  and T.numOppBizDocID = OpportunityBizDocs.numOppBizDocsID ';
   end if;
 
   v_strSql := coalesce(v_strSql,'') || ' order by ID';
   RAISE NOTICE '%',v_strSql;
   OPEN SWV_RefCur FOR EXECUTE v_strSql;                                                                     

   open SWV_RefCur2 for
   SELECT
   tintOrder,
		numFormFieldID,
		vcDbColumnName,
		vcFieldName,
		vcAssociatedControlType,
		vcListItemType,
		numListID,
		vcLookBackTableName,
		numFieldID,
		intColumnWidth,
		bitCustom as bitCustomField,
		numGroupID,
		vcOrigDbColumnName,
		bitAllowSorting,
		bitAllowEdit,
		ListRelID,
		bitAllowFiltering,
		vcFieldDataType
   FROM(SELECT
      T1.tintOrder::bigint+1 AS tintOrder,
			D.numFieldID AS numFormFieldID,
			vcDbColumnName,
			vcFieldName,
			vcAssociatedControlType,
			vcListItemType,
			numListID,
			vcLookBackTableName,
			D.numFieldID,
			coalesce(T1.intColumnWidth,0) AS intColumnWidth,
			coalesce(D.bitCustom,0) AS bitCustom,
			0 AS numGroupID
			,vcOrigDbColumnName
			,bitAllowSorting,
			bitAllowEdit,
			ListRelID,
			bitAllowFiltering,
			vcFieldDataType
      FROM
      View_DynamicDefaultColumns D
      INNER JOIN
      tt_TEMPSELECTEDCOLUMNS T1
      ON
      D.numFieldID = T1.numFieldID
      WHERE
      D.numDomainID = v_numDomainID
      AND D.numFormId = 15
      AND coalesce(T1.bitCustomField,false) = false
      AND T1.tintOrder > v_tintOrder -1
      UNION
      SELECT
      T1.tintOrder::bigint+1 AS tintOrder,
			Fld_id AS numFormFieldID,
			CONCAT('Cust',Fld_id),
			Fld_label,
			c.fld_type,
			'',
			c.numlistid,
			'',
			Fld_id,
			coalesce(T1.intColumnWidth,0) AS intColumnWidth,
			1 AS bitCustom,
			Grp_id
			,'',
			false,
			false,
			0,
			false,
			''
      FROM
      CFW_Fld_Master c
      INNER JOIN
      tt_TEMPSELECTEDCOLUMNS T1
      ON
      c.Fld_id = T1.numFieldID
      WHERE
      c.numDomainID = v_numDomainID
      AND coalesce(T1.bitCustomField,false) = true
      AND T1.tintOrder > v_tintOrder -1) T1
   ORDER BY
   tintOrder asc;
   RETURN;
END; $$;  

--Left Join OpportunityItems OppItems on OppItems.numOppId = OppMas.numOppId   
--left join Item on Item.numItemCode = OppItems.numItemCode  
--left join WareHouseItems on Item.numItemCode = WareHouseItems.numItemId  
--left Join WareHouses WareHouse on WareHouse.numWareHouseId =WareHouseItems.numWareHouseId  
--left Join WareHouseItmsDTL WareHouseItemDTL on WareHouseItemDTL.numWareHouseItemID = WareHouse.numWareHouseId


