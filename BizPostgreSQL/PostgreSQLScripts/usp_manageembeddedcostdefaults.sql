-- Stored procedure definition script USP_ManageEmbeddedCostDefaults for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ManageEmbeddedCostDefaults(v_numCostCatID NUMERIC,
    v_numDomainID NUMERIC,
    v_numUserCntID NUMERIC,
    v_strItems TEXT)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   IF v_numCostCatID > 0 then
      DELETE FROM EmbeddedCostDefaults WHERE numCostCatID = v_numCostCatID AND numDomainID = v_numDomainID;
      IF SUBSTR(CAST(v_strItems AS VARCHAR(10)),1,10) <> '' then
                    
         INSERT  INTO EmbeddedCostDefaults(numCostCatID,
                                  numCostCenterID,
                                  numAccountID,
                                  numDivisionID,
                                  numPaymentMethod,
                                  numDomainID,
                                  numCreatedBy,
                                  dtCreatedDate,
                                  numModifiedBy,
                                  dtModifiedDate)
         SELECT  v_numCostCatID,
										X.numCostCenterID,
                                        X.numAccountID,
                                        X.numDivisionID,
                                        X.numPaymentMethod,
                                        v_numDomainID,
                                        v_numUserCntID,
                                        TIMEZONE('UTC',now()),
                                        v_numUserCntID,
                                        TIMEZONE('UTC',now())
         FROM
		 XMLTABLE
			(
				'NewDataSet/Table1'
				PASSING 
					CAST(v_strItems AS XML)
				COLUMNS
					id FOR ORDINALITY,
					numCostCenterID NUMERIC PATH 'numCostCenterID',
					numAccountID NUMERIC PATH 'numAccountID',
					numDivisionID NUMERIC PATH 'numDivisionID',
					numPaymentMethod NUMERIC PATH 'numPaymentMethod'
			) AS X;

      end if;
   end if;
   RETURN;
END; $$;


