-- Stored procedure definition script usp_GetCaseDetailsForAccount for PostgreSQL
CREATE OR REPLACE FUNCTION usp_GetCaseDetailsForAccount(v_numDivisionID NUMERIC(9,0) DEFAULT 0     
--  
,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT     cast(Cases.numCaseId as VARCHAR(255)),cast(Cases.vcCaseNumber as VARCHAR(255)), cast(Cases.intTargetResolveDate as VARCHAR(255)) , cast(Cases.textSubject as VARCHAR(255)), cast(Cases.numStatus as VARCHAR(255)) ,fn_GetListItemName(Cases.numStatus) AS vcCaseStatus , cast(Cases.textDesc as VARCHAR(255)),
                      cast(Cases.textInternalComments as VARCHAR(255)), cast(Cases.numCreatedby as VARCHAR(255)), DivisionMaster.numDivisionID
   FROM         AdditionalContactsInformation INNER JOIN
   Cases ON AdditionalContactsInformation.numContactId = Cases.numContactId INNER JOIN
   DivisionMaster ON AdditionalContactsInformation.numDivisionId = DivisionMaster.numDivisionID
   WHERE DivisionMaster.numDivisionID = v_numDivisionID and Cases.numStatus <> 136;
END; $$;












