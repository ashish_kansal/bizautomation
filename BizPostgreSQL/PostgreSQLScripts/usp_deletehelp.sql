-- Stored procedure definition script USP_DeleteHelp for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_DeleteHelp(v_numHelpID NUMERIC(9,0))
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   DELETE FROM HelpMaster WHERE numHelpID = v_numHelpID;
   RETURN;
END; $$;


