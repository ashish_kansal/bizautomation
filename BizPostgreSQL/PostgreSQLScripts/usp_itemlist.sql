-- Stored procedure definition script USP_ItemList for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ItemList(v_ItemClassification NUMERIC(9,0) DEFAULT 0,                                    
v_KeyWord VARCHAR(100) DEFAULT '',                                    
v_IsKit SMALLINT DEFAULT NULL,                                    
v_SortChar CHAR(1) DEFAULT '0',                                     
v_CurrentPage INTEGER DEFAULT NULL,                                      
v_PageSize INTEGER DEFAULT NULL,                                      
INOUT v_TotRecs INTEGER  DEFAULT NULL,                                      
v_columnName VARCHAR(50) DEFAULT NULL,                                      
v_columnSortOrder VARCHAR(10) DEFAULT NULL,                      
v_numDomainID NUMERIC(9,0) DEFAULT 0,          
v_ItemType CHAR(1) DEFAULT NULL,          
v_bitSerialized BOOLEAN DEFAULT NULL,        
v_numItemGroup NUMERIC(9,0) DEFAULT NULL,  
v_bitAssembly BOOLEAN DEFAULT NULL, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_firstRec  INTEGER;                                      
   v_lastRec  INTEGER;                                      
   v_strSql  VARCHAR(5000);
BEGIN
   v_firstRec :=(v_CurrentPage::bigint -1)*v_PageSize::bigint;                                      
   v_lastRec :=(v_CurrentPage::bigint*v_PageSize::bigint+1);                                     
                                     
                                      
                                      
   v_strSql := 'With tblItem AS (select                  
Row_Number() Over (ORDER BY ' || coalesce(v_columnName,'') || ' ' || coalesce(v_columnSortOrder,'') || ' ) as RunningCount,numItemCode,vcItemName,txtItemDesc,                                   
case when charItemType=''P'' then ''Inventory Item'' when charItemType=''N'' then ''Non Inventory Item'' when charItemType=''S'' then ''Service'' when charItemType=''A'' then ''Accessory'' end as ItemType,                                    
sum(numOnHand) as numQtyOnHand,                
sum(numOnOrder) as numQtyOnOrder,                                    
sum(numReorder) as numQtyReorder,                
sum(numBackOrder) as numQtyBackOrder,                
sum(numAllocation) as numQtyOnAllocation,                
isnull(com.vcCompanyName,''-'') as vendor                               
from item                   
left join WareHouseItems                 
on numItemID=numItemCode                                
left join divisionmaster div                                  
on numVendorid=div.numDivisionId                                  
left join companyInfo com                                  
on com.numCompanyid=div.numcompanyID where Item.numDomainID=' || SUBSTR(CAST(v_numDomainID AS VARCHAR(15)),1,15);   
   if v_bitAssembly = true then  
      v_strSql := coalesce(v_strSql,'') || ' and bitAssembly= ' || SUBSTR(CAST(v_bitAssembly AS VARCHAR(2)),1,2) || '';
   ELSEIF v_IsKit = cast(NULLIF('1','') as SMALLINT)
   then  
      v_strSql := coalesce(v_strSql,'') || ' and (bitAssembly=false or  bitAssembly is null) and bitKitParent= ' || SUBSTR(CAST(v_IsKit AS VARCHAR(2)),1,2) || '';
   end if;             
   if v_ItemType <> '' then  
      v_strSql := coalesce(v_strSql,'') || ' and charItemType= ''' || coalesce(v_ItemType,'') || '''';
   end if;          
   if v_bitSerialized = true then  
      v_strSql := coalesce(v_strSql,'') || ' and bitSerialized=true ';
   end if;                                                 
   if v_SortChar <> '0' then  
      v_strSql := coalesce(v_strSql,'') || ' and vcItemName like ''' || coalesce(v_SortChar,'') || '%''';
   end if;                                     
   if v_ItemClassification <> cast(NULLIF('0','') as NUMERIC(9,0)) then 
      v_strSql := coalesce(v_strSql,'') || ' and numItemClassification=' || SUBSTR(CAST(v_ItemClassification AS VARCHAR(15)),1,15);
   end if;                                       
   if v_KeyWord <> '' then 
      v_strSql := coalesce(v_strSql,'') || ' and (vcItemName like ''' || coalesce(v_KeyWord,'') || '%'' or txtItemDesc like  ''' || coalesce(v_KeyWord,'') || '%'')';
   end if;          
   if v_numItemGroup > 0 then  
      v_strSql := coalesce(v_strSql,'') || ' and numItemGroup = ' ||  SUBSTR(CAST(v_numItemGroup AS VARCHAR(20)),1,20);
   end if;     
   if v_numItemGroup = -1 then   
      v_strSql := coalesce(v_strSql,'') || ' and numItemGroup in (select numItemGroupID from ItemGroups where numDomainID=' ||  SUBSTR(CAST(v_numDomainID AS VARCHAR(20)),1,20) || ')';
   end if;                                    
   v_strSql := coalesce(v_strSql,'') || ' group by numItemCode,vcItemName,txtItemDesc,charItemType,vcCompanyName,vcDivisionName)';              
              
              
              
              
     
   v_strSql := coalesce(v_strSql,'') || 'select * from tblItem where  RunningCount >' || SUBSTR(CAST(v_firstRec AS VARCHAR(15)),1,15) || ' and RunningCount < ' || SUBSTR(CAST(v_lastRec AS VARCHAR(15)),1,15) || '              
              
union select 0,count(*),null,null,null,null,null,null,null,null,null from tblItem order by RunningCount ';                                            
   RAISE NOTICE '%',v_strSql;          
   OPEN SWV_RefCur FOR EXECUTE v_strSql;
   RETURN;
END; $$;


