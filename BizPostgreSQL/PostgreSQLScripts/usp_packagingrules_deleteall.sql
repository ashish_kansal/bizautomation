-- Stored procedure definition script USP_PackagingRules_DeleteAll for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021

-- =============================================
-- Author:		Sachin Sadhu
-- Create date: 31stJult2014
-- Description:	To Delete Packaging rules on the Basis of Shipping Rule ID
-- =============================================
Create or replace FUNCTION USP_PackagingRules_DeleteAll(v_numDomainID NUMERIC(18,0),
	v_numShippingRuleID NUMERIC(18,0))
RETURNS VOID LANGUAGE plpgsql
	-- Add the parameters for the stored procedure here
   AS $$
BEGIN

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	

    -- Insert statements for procedure here
   Delete from PackagingRules where numDomainID = v_numDomainID and numShippingRuleID = v_numShippingRuleID;
   RETURN;
END; $$;


---------------------------------------------------


