-- Stored procedure definition script USP_ECampaignList for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ECampaignList(v_numUserCntID NUMERIC(9,0) DEFAULT 0,            
 v_numDomainID NUMERIC(9,0) DEFAULT 0,              
 v_SortChar CHAR(1) DEFAULT '0',           
 v_CurrentPage INTEGER DEFAULT NULL,            
 v_PageSize INTEGER DEFAULT NULL,            
 INOUT v_TotRecs INTEGER  DEFAULT NULL,            
 v_columnName VARCHAR(50) DEFAULT NULL,            
 v_columnSortOrder VARCHAR(10) DEFAULT NULL, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_strSql  VARCHAR(8000);            
          
          
            
   v_firstRec  INTEGER;             
   v_lastRec  INTEGER;
BEGIN
   BEGIN
      CREATE TEMP SEQUENCE tt_tempTable_seq;
      EXCEPTION WHEN OTHERS THEN
         NULL;
   END;
   drop table IF EXISTS tt_TEMPTABLE CASCADE;
   create TEMPORARY TABLE tt_TEMPTABLE 
   ( 
      ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1) PRIMARY KEY,
      numECampaignID NUMERIC(9,0),
      vcECampName VARCHAR(100),
      txtDesc TEXT,
      NoOfUsersAssigned INTEGER,
      NoOfEmailSent INTEGER,
      NoOfEmailOpened INTEGER,
      NoOfLinks INTEGER,
      NoOfLinkClicked INTEGER
   );            
            
            
   v_strSql := 'select 
numECampaignID,          
vcECampName,          
txtDesc, 
coalesce((SELECT COUNT(*) FROM ConECampaign WHERE numECampaignID = ECampaign.numECampaignID AND coalesce(ConECampaign.bitEngaged,false) = true), 0) AS NoOfUsersAssigned,
coalesce((SELECT COUNT(*) FROM ConECampaignDTL INNER JOIN ECampaignDTLs ON ConECampaignDTL.numECampDTLID = ECampaignDTLs.numECampDTLId WHERE coalesce(ECampaignDTLs.numEmailTemplate,0) > 0 AND numECampID = ECampaign.numECampaignID AND coalesce(bitSend,false) = true AND coalesce(tintDeliveryStatus,0) = 1),0) AS NoOfEmailSent,
coalesce((SELECT COUNT(*) FROM ConECampaignDTL INNER JOIN ECampaignDTLs ON ConECampaignDTL.numECampDTLID = ECampaignDTLs.numECampDTLId WHERE coalesce(ECampaignDTLs.numEmailTemplate,0) > 0 AND numECampID = ECampaign.numECampaignID AND coalesce(bitSend,false) = true AND coalesce(tintDeliveryStatus,0) = 1 AND coalesce(bitEmailRead,false) = true),0) AS NoOfEmailOpened,
(SELECT COUNT(*) FROM ConECampaignDTLLinks WHERE numConECampaignDTLID IN(SELECT coalesce(CECDTL.numConECampDTLID,0) FROM ConECampaignDTL CECDTL WHERE numConECampID IN(SELECT coalesce(CEC.numConEmailCampID,0) FROM ConECampaign CEC WHERE numECampaignID = ECampaign.numECampaignID))) AS NoOfLinks,
(SELECT COUNT(*) FROM ConECampaignDTLLinks WHERE coalesce(bitClicked,false) = true AND numConECampaignDTLID IN(SELECT coalesce(CECDTL.numConECampDTLID,0) FROM ConECampaignDTL CECDTL WHERE numConECampID IN(SELECT coalesce(CEC.numConEmailCampID,0) FROM ConECampaign CEC WHERE numECampaignID = ECampaign.numECampaignID))) AS NoOfLinkClicked
from ECampaign          
where coalesce(bitDeleted,false) = false AND numDomainID = ' || SUBSTR(CAST(v_numDomainID AS VARCHAR(15)),1,15);           
            
   if v_SortChar <> '0' then 
      v_strSql := coalesce(v_strSql,'') || ' And vcECampName ilike ''' || coalesce(v_SortChar,'') || '%''';
   end if;          
   if v_columnName <> '' then  
      v_strSql := coalesce(v_strSql,'') || ' ORDER BY ' || coalesce(v_columnName,'') || ' ' || coalesce(v_columnSortOrder,'');
   end if;   
  
   RAISE NOTICE '%',v_strSql;         
   EXECUTE 'insert into tt_TEMPTABLE
(            
	numECampaignID,            
	vcECampName,            
	txtDesc,
	NoOfUsersAssigned,
	NoOfEmailSent,
	NoOfEmailOpened,
	NoOfLinks,
	NoOfLinkClicked 
)            
' || v_strSql;             
            
   v_firstRec :=(v_CurrentPage::bigint -1)*v_PageSize::bigint;            
   v_lastRec :=(v_CurrentPage::bigint*v_PageSize::bigint+1);            
--Don't change column sequence because its being used for Drip Campaign DropDown in Contact Details
   open SWV_RefCur for
   select numECampaignID,vcECampName,txtDesc,NoOfUsersAssigned,NoOfEmailSent,NoOfEmailOpened,NoOfLinks,NoOfLinkClicked from tt_TEMPTABLE where ID > v_firstRec and ID < v_lastRec;            
   select count(*) INTO v_TotRecs from tt_TEMPTABLE;            
   RETURN;
END; $$;


