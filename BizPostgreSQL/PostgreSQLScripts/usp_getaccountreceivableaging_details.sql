-- Stored procedure definition script USP_GetAccountReceivableAging_Details for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_GetAccountReceivableAging_Details(v_numDomainId NUMERIC(9,0)  DEFAULT 0,
    v_numDivisionID NUMERIC(9,0) DEFAULT NULL,
    v_vcFlag VARCHAR(20) DEFAULT NULL,
	v_numAccountClass NUMERIC(9,0) DEFAULT 0,
	v_dtFromDate DATE DEFAULT NULL,
	v_dtToDate DATE DEFAULT NULL,
	v_ClientTimeZoneOffset INTEGER DEFAULT 0,
	v_tintBasedOn SMALLINT DEFAULT 1,
	INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_strSql  TEXT;
   v_strSql1  TEXT;
   v_strCredit  TEXT;
   v_AuthoritativeSalesBizDocId  INTEGER;
   v_baseCurrency  NUMERIC;
BEGIN
   v_strCredit := '';
	
   select   coalesce(numAuthoritativeSales,0) INTO v_AuthoritativeSalesBizDocId FROM   AuthoritativeBizDocs WHERE  numDomainId = v_numDomainId;--287
    ------------------------------------------      
   select   numCurrencyID INTO v_baseCurrency FROM Domain WHERE numDomainId = v_numDomainId;
	/*when any Order is placed in Foreign currency (other than base currency)then show it in purple color */
	------------------------------------------      
   IF v_dtFromDate IS NULL then
      v_dtFromDate := '1990-01-01 00:00:00.000';
   end if;
   IF v_dtToDate IS NULL then
      v_dtToDate := TIMEZONE('UTC',now());
   end if;
    --Get Master data
	 
   v_strSql := '
     SELECT DISTINCT OM.numOppId,
					 -1 AS numJournal_Id,
					 -1 as numDepositId, -1 as tintDepositePage,-1 as numReturnHeaderID,
                        OM.vcPOppName,
                        OB.numOppBizDocsId,
                        OB.vcBizDocID,
                        COALESCE(OB.monDealAmount
                                 ,0) TotalAmount,--OM.monPAmount
                        (COALESCE(TablePayments.monPaidAmount,0)
                           ) AmountPaid,
                        COALESCE(OB.monDealAmount * COALESCE(OM.fltExchangeRate, 1), 0) - COALESCE(COALESCE(TablePayments.monPaidAmount,0) * COALESCE(OM.fltExchangeRate, 1),0) BalanceDue,
						FormatedDateFromDate(ob.dtFromDate,' || SUBSTR(CAST(v_numDomainId AS VARCHAR(15)),1,15) || ') dtDate,
                        CASE COALESCE(bitBillingTerms,false) 
                          WHEN true THEN FormatedDateFromDate(OB.dtFromDate + make_interval(days => COALESCE((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = COALESCE(intBillingDays,0)), 0)),
                                                                   ' || SUBSTR(CAST(v_numDomainId AS VARCHAR(15)),1,15) || ')
                          WHEN false THEN FormatedDateFromDate(ob.dtFromDate,' || SUBSTR(CAST(v_numDomainId AS VARCHAR(15)),1,15) || ')
                        END AS DueDate,
                        bitBillingTerms,
                        intBillingDays,
                        OB.dtCreatedDate,
                        COALESCE(C.varCurrSymbol,'''') varCurrSymbol,
                        CASE WHEN ' || SUBSTR(CAST(v_baseCurrency AS VARCHAR(15)),1,15) || ' <> OM.numCurrencyID THEN 2 ELSE 1 END AS CurrencyCount,
                        OB.vcRefOrderNo ,OM.numDivisionId
        FROM   OpportunityMaster OM INNER JOIN DivisionMaster DM ON OM.numDivisionId = DM.numDivisionID
               LEFT OUTER JOIN OpportunityBizDocs OB ON OB.numOppId = OM.numOppID
               LEFT OUTER JOIN Currency C ON OM.numCurrencyID = C.numCurrencyID
			   LEFT JOIN LATERAL
			   (
					SELECT 
						SUM(monAmountPaid) AS monPaidAmount
					FROM
						DepositeDetails DD
					INNER JOIN
						DepositMaster DM
					ON
						DD.numDepositID=DM.numDepositId
					WHERE 
						numOppID = OM.numOppID
						AND numOppBizDocsID=OB.numOppBizDocsID
						AND 1 = (CASE WHEN COALESCE(DM.numReturnHeaderID,0) > 0 THEN (CASE WHEN (DD.dtCreatedDate + make_interval(mins => -1 * ' || SUBSTR(CAST(v_ClientTimeZoneOffset AS VARCHAR(30)),1,30) || '))::DATE <= ''' || SUBSTR(CAST(v_dtToDate AS VARCHAR(30)),1,30) || ''' THEN 1 ELSE 0 END) ELSE (CASE WHEN DM.dtDepositDate::DATE <= ''' || SUBSTR(CAST(v_dtToDate AS VARCHAR(30)),1,30) || ''' THEN 1 ELSE 0 END) END)
				) TablePayments ON TRUE
        WHERE  OM.tintOppType = 1
               AND OM.tintOppStatus = 1
               AND om.numDomainId = ' || SUBSTR(CAST(v_numDomainId AS VARCHAR(15)),1,15) || '
               AND OB.numBizDocId = ' || CAST(coalesce(v_AuthoritativeSalesBizDocId,0) AS VARCHAR(15)) || '
               AND OB.bitAuthoritativeBizDocs=1
               AND COALESCE(OB.tintDeferred,0) <> 1
               AND (DM.numDivisionID = ' || SUBSTR(CAST(v_numDivisionID AS VARCHAR(15)),1,15) || ' OR ' || SUBSTR(CAST(v_numDivisionID AS VARCHAR(30)),1,30) || '=0)
			   AND (OM.numAccountClass= ' || SUBSTR(CAST(v_numAccountClass AS VARCHAR(30)),1,30) || ' OR ' || SUBSTR(CAST(v_numAccountClass AS VARCHAR(30)),1,30) || '=0)
			   AND (dtFromDate + make_interval(days => CASE WHEN OM.bitBillingTerms = true  AND ' || SUBSTR(CAST(v_tintBasedOn AS VARCHAR(30)),1,30) || ' = 1
								 THEN COALESCE((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = COALESCE(OM.intBillingDays,0)), 0)
								 ELSE 0 
							END))::DATE <= ''' || SUBSTR(CAST(v_dtToDate AS VARCHAR(30)),1,30) || '''
               AND COALESCE(OB.monDealAmount
                            ,0) > 0  AND COALESCE(OB.monDealAmount * OM.fltExchangeRate
                        - COALESCE(TablePayments.monPaidAmount,0) * OM.fltExchangeRate,0) != 0';
    
    	--Show Impact of AR journal entries in report as well 
    	
   v_strSql1 := '    	
		UNION 
		 SELECT 
				-1 AS numOppId,
				GJH.numJournal_Id AS numJournal_Id,
				-1 as numDepositId, -1 as tintDepositePage,-1 as numReturnHeaderID,
				''Journal'' vcPOppName,
				0 numOppBizDocsId,
				'''' vcBizDocID,
				CASE WHEN GJD.numDebitAmt > 0 THEN GJD.numDebitAmt ELSE -1*GJD.numCreditAmt END TotalAmount,
				0 AmountPaid,
				0 BalanceDue,
				FormatedDateFromDate(GJH.datEntry_Date,' || SUBSTR(CAST(v_numDomainId AS VARCHAR(15)),1,15) || ') AS dtDate,
				FormatedDateFromDate(GJH.datEntry_Date,' || SUBSTR(CAST(v_numDomainId AS VARCHAR(15)),1,15) || ') AS DueDate,
				false bitBillingTerms,
				0 intBillingDays,
				GJH.datCreatedDate,
				COALESCE(C.varCurrSymbol, '''') varCurrSymbol,
				1 AS CurrencyCount,'''' as vcRefOrderNo,0 as numDivisionId
		 FROM   General_Journal_Header GJH
				INNER JOIN General_Journal_Details GJD ON GJH.numJournal_Id = GJD.numJournalId
															  AND GJH.numDomainId = GJD.numDomainId
				INNER JOIN Chart_Of_Accounts COA ON COA.numAccountId = GJD.numChartAcntId
				LEFT OUTER JOIN Currency C ON GJD.numCurrencyID = C.numCurrencyID
		 WHERE  GJH.numDomainId = ' || SUBSTR(CAST(v_numDomainId AS VARCHAR(15)),1,15) || '
				and GJD.numCustomerID =' || SUBSTR(CAST(v_numDivisionID AS VARCHAR(15)),1,15) || '
				AND COA.vcAccountCode LIKE ''01010105%'' 
				AND COALESCE(numOppId,0)=0 AND COALESCE(numOppBizDocsId,0)=0 
				AND COALESCE(GJH.numDepositID,0)=0 AND COALESCE(GJH.numReturnID,0)=0
				AND (GJH.numAccountClass= ' || SUBSTR(CAST(v_numAccountClass AS VARCHAR(30)),1,30) || ' OR ' || SUBSTR(CAST(v_numAccountClass AS VARCHAR(30)),1,30) || '=0)
				AND GJH.datEntry_Date::DATE <= ''' || SUBSTR(CAST(v_dtToDate AS VARCHAR(30)),1,30) || '''';				
    
   v_strCredit := ' UNION SELECT -1 AS numOppId,
								-1 AS numJournal_Id,
								numDepositId,tintDepositePage,COALESCE(numReturnHeaderID,0) as numReturnHeaderID,
                      			CASE WHEN tintDepositePage=2 THEN ''Unapplied Payment'' ELSE ''Credit Memo'' END AS vcPOppName,
								0 numOppBizDocsId,
								'''' vcBizDocID,
								(COALESCE(monDepositAmount,0) - COALESCE(monAppliedAmount,0)) * -1 AS TotalAmount,
								0 AmountPaid,
								(COALESCE(monDepositAmount,0) - COALESCE(monAppliedAmount,0)) * -1 AS BalanceDue,
								FormatedDateFromDate(DM.dtDepositDate,' || SUBSTR(CAST(v_numDomainId AS VARCHAR(15)),1,15) || ') AS dtDate,
								FormatedDateFromDate(DM.dtDepositDate,' || SUBSTR(CAST(v_numDomainId AS VARCHAR(15)),1,15) || ') AS DueDate,
								false bitBillingTerms,
								0 intBillingDays,
								DM.dtCreationDate,
								'''' varCurrSymbol,
								1 AS CurrencyCount,'''' as vcRefOrderNo,0 as numDivisionId
		 FROM  DepositMaster DM 
		WHERE DM.numDomainId=' || SUBSTR(CAST(v_numDomainId AS VARCHAR(15)),1,15) || ' AND tintDepositePage IN(2,3)  
		AND DM.numDivisionId = ' || SUBSTR(CAST(v_numDivisionID AS VARCHAR(15)),1,15) || '
		AND (DM.numAccountClass= ' || SUBSTR(CAST(v_numAccountClass AS VARCHAR(30)),1,30) || ' OR ' || SUBSTR(CAST(v_numAccountClass AS VARCHAR(30)),1,30) || '=0)
		AND DM.dtDepositDate::DATE  <= ''' || SUBSTR(CAST(v_dtToDate AS VARCHAR(30)),1,30) || '''
		AND ((COALESCE(monDepositAmount,0) - COALESCE(monAppliedAmount,0)) <> 0)
		AND COALESCE(DM.numReturnHeaderID,0) = 0 
		UNION 
			SELECT -1 AS numOppId,
					-1 AS numJournal_Id,
					numDepositId,tintDepositePage,COALESCE(DM.numReturnHeaderID,0) as numReturnHeaderID,
                    CASE WHEN tintDepositePage=2 THEN ''Unapplied Payment'' ELSE ''Credit Memo'' END AS vcPOppName,
					0 numOppBizDocsId,
					'''' vcBizDocID,
					(COALESCE(monDepositAmount,0) - (CASE 
														WHEN RH.tintReturnType=4 
														THEN COALESCE(monAppliedAmount,0)
														ELSE COALESCE((SELECT SUM(DD.monAmountPaid) FROM DepositeDetails DD WHERE DD.numDepositID=DM.numDepositId AND DD.dtCreatedDate + make_interval(mins => -1 * ' || SUBSTR(CAST(v_ClientTimeZoneOffset AS VARCHAR(30)),1,30) || ') <= ''' || SUBSTR(CAST(v_dtToDate AS VARCHAR(30)),1,30) || '''),0)
													END)) * -1 AS TotalAmount,
					0 AmountPaid,
					(COALESCE(monDepositAmount,0) - (CASE 
													WHEN RH.tintReturnType=4 
													THEN COALESCE(monAppliedAmount,0)
													ELSE COALESCE((SELECT SUM(DD.monAmountPaid) FROM DepositeDetails DD WHERE DD.numDepositID=DM.numDepositId AND DD.dtCreatedDate + make_interval(mins => -1 * ' || SUBSTR(CAST(v_ClientTimeZoneOffset AS VARCHAR(30)),1,30) || ') <= ''' || SUBSTR(CAST(v_dtToDate AS VARCHAR(30)),1,30) || '''),0)
													END)) * -1 AS BalanceDue,
					FormatedDateFromDate(DM.dtDepositDate,' || SUBSTR(CAST(v_numDomainId AS VARCHAR(15)),1,15) || ') AS dtDate,
					FormatedDateFromDate(DM.dtDepositDate,' || SUBSTR(CAST(v_numDomainId AS VARCHAR(15)),1,15) || ') AS DueDate,
					false bitBillingTerms,
					0 intBillingDays,
					DM.dtCreationDate,
					'''' varCurrSymbol,
					1 AS CurrencyCount,'''' as vcRefOrderNo,0 as numDivisionId
		 FROM  DepositMaster DM 
		 LEFT JOIN	
				ReturnHeader RH
			ON
				DM.numReturnHeaderID = RH.numReturnHeaderID
		WHERE DM.numDomainId=' || SUBSTR(CAST(v_numDomainId AS VARCHAR(15)),1,15) || ' AND tintDepositePage IN(2,3)  
		AND DM.numDivisionId = ' || SUBSTR(CAST(v_numDivisionID AS VARCHAR(15)),1,15) || '
		AND (DM.numAccountClass= ' || SUBSTR(CAST(v_numAccountClass AS VARCHAR(30)),1,30) || ' OR ' || SUBSTR(CAST(v_numAccountClass AS VARCHAR(30)),1,30) || '=0)
		AND DM.dtDepositDate::DATE  <= ''' || SUBSTR(CAST(v_dtToDate AS VARCHAR(30)),1,30) || '''
		AND CAST(COALESCE(monDepositAmount,0) - (CASE 
												WHEN RH.tintReturnType=4 
												THEN COALESCE(monAppliedAmount,0)
												ELSE COALESCE((SELECT SUM(DD.monAmountPaid) FROM DepositeDetails DD WHERE DD.numDepositID=DM.numDepositId AND DD.dtCreatedDate + make_interval(mins => -1 * ' || SUBSTR(CAST(v_ClientTimeZoneOffset AS VARCHAR(30)),1,30) || ') <= ''' || SUBSTR(CAST(v_dtToDate AS VARCHAR(30)),1,30) || '''),0)
											END) AS DECIMAL(18,2)) <> 0
		AND COALESCE(DM.numReturnHeaderID,0) > 0';  
    
   IF (v_vcFlag = '0+30') then
      
      v_strSql := coalesce(v_strSql,'') ||
      'AND OB.dtFromDate + make_interval(days => CASE 
                                 WHEN bitBillingTerms = true THEN COALESCE((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = COALESCE(intBillingDays,0)), 0)
                                 ELSE 0
                               END) BETWEEN GetUTCDateWithoutTime() AND public.GetUTCDateWithoutTime()+ interval ''31 day''
               AND GetUTCDateWithoutTime() <= OB.dtFromDate + make_interval(days => CASE 
                                                                --WHEN bitBillingTerms = true THEN COALESCE(fn_GetListItemName(COALESCE(intBillingDays,0)),0)
                                                                WHEN bitBillingTerms = true THEN COALESCE((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = COALESCE(intBillingDays,0)), 0)
                                                                ELSE 0
                                                              END)';
      v_strSql1 := coalesce(v_strSql1,'') || 'AND GJH.datEntry_Date BETWEEN GetUTCDateWithoutTime() AND public.GetUTCDateWithoutTime()+ interval ''31 day''';
      v_strCredit := coalesce(v_strCredit,'') || 'AND DM.dtDepositDate BETWEEN GetUTCDateWithoutTime() AND public.GetUTCDateWithoutTime()+ interval ''31 day''';
   ELSEIF (v_vcFlag = '30+60')
   then
        
      v_strSql := coalesce(v_strSql,'') ||
      'AND OB.dtFromDate + make_interval(days => CASE 
                                   WHEN bitBillingTerms = true THEN COALESCE((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = COALESCE(intBillingDays,0)), 0)
                                   ELSE 0
                                 END) BETWEEN public.GetUTCDateWithoutTime()+ interval ''31 day'' AND public.GetUTCDateWithoutTime()+ interval ''61 day''
                 AND GetUTCDateWithoutTime() <= OB.dtFromDate + make_interval(days => CASE 
                                                                  WHEN bitBillingTerms = true THEN COALESCE((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = COALESCE(intBillingDays,0)), 0)
                                                                  ELSE 0
                                                                END)';
      v_strSql1 := coalesce(v_strSql1,'') || 'AND GJH.datEntry_Date BETWEEN public.GetUTCDateWithoutTime()+ interval ''31 day'' AND public.GetUTCDateWithoutTime()+ interval ''61 day''';
      v_strCredit := coalesce(v_strCredit,'') || 'AND DM.dtDepositDate BETWEEN public.GetUTCDateWithoutTime()+ interval ''31 day'' AND public.GetUTCDateWithoutTime()+ interval ''61 day''';
   ELSEIF (v_vcFlag = '60+90')
   then
          
      v_strSql := coalesce(v_strSql,'') ||
      'AND OB.dtFromDate + make_interval(days => CASE 
                                     WHEN bitBillingTerms = true THEN COALESCE((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = COALESCE(intBillingDays,0)), 0)
                                     ELSE 0
                                   END) BETWEEN public.GetUTCDateWithoutTime()+ interval ''61 day'' AND public.GetUTCDateWithoutTime()+ interval ''91 day''
                   AND GetUTCDateWithoutTime() <= OB.dtFromDate + make_interval(days => CASE 
                                                                    WHEN bitBillingTerms = true THEN COALESCE((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = COALESCE(intBillingDays,0)), 0)
                                                                    ELSE 0
                                                                  END)';
      v_strSql1 := coalesce(v_strSql1,'') || 'AND GJH.datEntry_Date BETWEEN public.GetUTCDateWithoutTime()+ interval ''61 day'' AND public.GetUTCDateWithoutTime()+ interval ''91 day''';
      v_strCredit := coalesce(v_strCredit,'') || 'AND DM.dtDepositDate BETWEEN public.GetUTCDateWithoutTime()+ interval ''61 day'' AND public.GetUTCDateWithoutTime()+ interval ''91 day''';
   ELSEIF (v_vcFlag = '90+')
   then
            
      v_strSql := coalesce(v_strSql,'') ||
      'AND OB.dtFromDate + make_interval(days => CASE 
                                       WHEN bitBillingTerms = true THEN COALESCE((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = COALESCE(intBillingDays,0)), 0)
                                       ELSE 0
                                     END) > public.GetUTCDateWithoutTime()+ interval ''91 day''
                     AND GetUTCDateWithoutTime() <= OB.dtFromDate + make_interval(days => CASE 
                                                                      --WHEN bitBillingTerms = true THEN COALESCE(fn_GetListItemName(COALESCE(intBillingDays,0)),0)
                                                                      WHEN bitBillingTerms = true THEN COALESCE((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = COALESCE(intBillingDays,0)), 0)
                                                                      ELSE 0
                                                                    END)';
      v_strSql1 := coalesce(v_strSql1,'') || 'AND GJH.datEntry_Date  > public.GetUTCDateWithoutTime()+ interval ''91 day''';
      v_strCredit := coalesce(v_strCredit,'') || 'AND DM.dtDepositDate  > public.GetUTCDateWithoutTime()+ interval ''91 day''';
   ELSEIF (v_vcFlag = '0-30')
   then
              
      v_strSql := coalesce(v_strSql,'') ||
      'AND GetUTCDateWithoutTime() > OB.dtFromDate + make_interval(days => CASE 
                                                                       WHEN bitBillingTerms = true THEN COALESCE((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = COALESCE(intBillingDays,0)), 0)
                                                                       ELSE 0
                                                                     END)
                       AND datediff(''day'',OB.dtFromDate + make_interval(days => CASE 
                                                      WHEN bitBillingTerms = true THEN COALESCE((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = COALESCE(intBillingDays,0)), 0)
                                                      ELSE 0
                                                    END),GetUTCDateWithoutTime()) BETWEEN 0 AND 30 ';
      v_strSql1 := coalesce(v_strSql1,'') || 'AND datediff(''day'',GJH.datEntry_Date, GetUTCDateWithoutTime()) BETWEEN 0 AND 30 ';
      v_strCredit := coalesce(v_strCredit,'') || 'AND datediff(''day'',DM.dtDepositDate, GetUTCDateWithoutTime()) BETWEEN 0 AND 30 ';
   ELSEIF (v_vcFlag = '30-60')
   then
                
      v_strSql := coalesce(v_strSql,'') ||
      'AND GetUTCDateWithoutTime() > OB.dtFromDate + make_interval(days => CASE 
                                                                         WHEN bitBillingTerms = true THEN COALESCE((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = COALESCE(intBillingDays,0)), 0)
                                                                         ELSE 0
                                                                       END)
                         AND datediff(''day'',OB.dtFromDate + make_interval(days => CASE 
                                                        WHEN bitBillingTerms = true THEN COALESCE((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = COALESCE(intBillingDays,0)), 0)
                                                        ELSE 0
                                                      END),GetUTCDateWithoutTime()) BETWEEN 31 AND 60';
      v_strSql1 := coalesce(v_strSql1,'') || 'AND datediff(''day'',GJH.datEntry_Date, GetUTCDateWithoutTime()) BETWEEN 31 AND 60 ';
      v_strCredit := coalesce(v_strCredit,'') || 'AND datediff(''day'',DM.dtDepositDate, GetUTCDateWithoutTime()) BETWEEN 31 AND 60 ';
   ELSEIF (v_vcFlag = '60-90')
   then
                  
      v_strSql := coalesce(v_strSql,'') ||
      'AND GetUTCDateWithoutTime() > OB.dtFromDate + make_interval(days => CASE 
                                                                           WHEN bitBillingTerms = true THEN COALESCE((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = COALESCE(intBillingDays,0)), 0)
                                                                           ELSE 0
                                                                         END)
                           AND datediff(''day'',OB.dtFromDate + make_interval(days => CASE 
                                                          WHEN bitBillingTerms = true THEN COALESCE((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = COALESCE(intBillingDays,0)), 0)
                                                          ELSE 0
                                                        END),GetUTCDateWithoutTime()) BETWEEN 61 AND 90';
      v_strSql1 := coalesce(v_strSql1,'') || 'AND datediff(''day'',GJH.datEntry_Date, GetUTCDateWithoutTime()) BETWEEN 61 AND 90 ';
      v_strCredit := coalesce(v_strCredit,'') || 'AND datediff(''day'',DM.dtDepositDate, GetUTCDateWithoutTime()) BETWEEN 61 AND 90 ';
   ELSEIF (v_vcFlag = '90-')
   then
                    
      v_strSql := coalesce(v_strSql,'') ||
      'AND GetUTCDateWithoutTime() > OB.dtFromDate + make_interval(days => CASE 
                                                                             WHEN bitBillingTerms = true THEN COALESCE((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = COALESCE(intBillingDays,0)), 0)
                                                                             ELSE 0
                                                                           END)
                             AND datediff(''day'',OB.dtFromDate + make_interval(days => CASE 
                                                            WHEN bitBillingTerms = true THEN COALESCE((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = COALESCE(intBillingDays,0)), 0)
                                                            ELSE 0
                                                          END),GetUTCDateWithoutTime()) > 90';
      v_strSql1 := coalesce(v_strSql1,'') || 'AND datediff(''day'',GJH.datEntry_Date, GetUTCDateWithoutTime()) > 90';
      v_strCredit := coalesce(v_strCredit,'') || 'AND datediff(''day'',DM.dtDepositDate, GetUTCDateWithoutTime()) > 90';
   ELSE
      v_strSql := coalesce(v_strSql,'') || '';
   end if;
                    

   RAISE NOTICE '%',CAST((coalesce(v_strSql,'') || coalesce(v_strSql1,'') || coalesce(v_strCredit,'')) AS TEXT);
   OPEN SWV_RefCur FOR EXECUTE coalesce(v_strSql,'') || coalesce(v_strSql1,'') || coalesce(v_strCredit,'');
   RETURN;
END; $$;
/****** Object:  StoredProcedure USP_GetAccountReceivableAging_Invoice    Script Date: 05/24/2010 03:48:41 ******/



