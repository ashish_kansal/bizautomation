-- Stored procedure definition script USP_GetReceivePaymentDetailsForOpportunity for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetReceivePaymentDetailsForOpportunity(v_numDomainId NUMERIC(9,0) DEFAULT 0,    
 v_numOppId NUMERIC(9,0) DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for Select Count(*) From OpportunityBizDocsDetails OppBizDocsDet
   Inner Join OpportunityBizDocs OppBizDocs on OppBizDocsDet.numBizDocsId = OppBizDocs.numOppBizDocsId
   Inner Join OpportunityMaster Opp On OppBizDocs.numoppid = Opp.numOppId
   Inner Join AdditionalContactsInformation ADC On Opp.numContactId = ADC.numContactId
   Inner Join DivisionMaster Div On Opp.numDivisionId = Div.numDivisionID AND ADC.numDivisionId = Div.numDivisionID
   Inner Join CompanyInfo C On Div.numCompanyID = C.numCompanyId
   Where OppBizDocsDet.numDomainId = v_numDomainId And Opp.numOppId = v_numOppId; --And OppBizDocsDet.bitIntegratedToAcnt=0      
END; $$;












