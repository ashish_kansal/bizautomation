-- Stored procedure definition script USP_OpportuntityCommission for PostgreSQL
CREATE OR REPLACE FUNCTION USP_OpportuntityCommission(v_numUserId NUMERIC(9,0) DEFAULT 0,
 v_numUserCntID NUMERIC(9,0) DEFAULT 0,
v_numDomainId NUMERIC(9,0) DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_bitCommOwner  BOOLEAN;  
   v_bitCommAssignee  BOOLEAN; 
   v_decCommOwner  DECIMAL(10,2);  
   v_decCommAssignee  DECIMAL(10,2);
BEGIN
   select   bitCommOwner, bitCommAssignee, coalesce(fltCommOwner,0), coalesce(fltCommAssignee,0) INTO v_bitCommOwner,v_bitCommAssignee,v_decCommOwner,v_decCommAssignee from UserMaster Where numUserId = 1;  
 
   open SWV_RefCur for Select Opp.vcpOppName AS Name ,Case when Opp.tintoppstatus = 0 then 'Open' Else 'Close' End as OppStatus, Opp.monPAmount as DealAmount,
  CAST('Deal Owner' AS CHAR(10)) as EmpRole,cast((Select cast(coalesce(fltCommOwner,0) as VARCHAR(100)) From UserMaster Where numUserId = 1) as VARCHAR(255)) as CommissionPer,coalesce(Opp.monPAmount,0)*v_decCommOwner/100 as CommissionAmt
   From OpportunityMaster Opp Where Opp.numrecowner = 1 And Opp.tintoppstatus = 1
   Union all
   Select Opp.vcpOppName AS Name ,Case when Opp.tintoppstatus = 0 then 'Open' Else 'Close' End as OppStatus, Opp.monPAmount as DealAmount,
  CAST('Deal Assigned' AS CHAR(13)) as EmpRole,cast((Select cast(coalesce(fltCommAssignee,0) as VARCHAR(100)) From UserMaster Where numUserId = 1) as VARCHAR(255)) as CommissionPer,coalesce(Opp.monPAmount,0)*v_decCommAssignee/100 as CommissionAmt
   From OpportunityMaster Opp Where Opp.numassignedto = 1 And Opp.tintoppstatus = 1;
END; $$;












