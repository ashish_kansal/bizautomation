-- Stored procedure definition script Temp_GetDealAmount for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
--EXEC Temp_GetDealAmount 5804,'2009-12-11 23:55:04.497',5182  
CREATE OR REPLACE FUNCTION Temp_GetDealAmount(v_numOppID NUMERIC,
      v_dt TIMESTAMP,
      v_numOppBizDocsId NUMERIC DEFAULT 0, INOUT SWV_RefCur refcursor default null, INOUT SWV_RefCur2 refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_DivisionID  NUMERIC(9,0);                         
                                  
   v_TaxPercentage  DOUBLE PRECISION;                    
  
   v_shippingAmt  NUMERIC(19,4);    
   v_TotalTaxAmt  NUMERIC(19,4);     
   v_decDisc  DOUBLE PRECISION; 
   v_bitDiscType  BOOLEAN;      
   v_decInterest  DOUBLE PRECISION;
   v_bitBillingTerms  BOOLEAN;     
   v_intBillingDays  INTEGER;    
   v_bitInterestType  BOOLEAN;    
   v_fltInterest  DOUBLE PRECISION;    
   v_dtCreatedDate  VARCHAR(20);    
   v_strDate  TIMESTAMP;    
   v_TotalAmt  NUMERIC(19,4);    
   v_TotalAmtAfterDisc  NUMERIC(19,4);    
   v_fltDiscount  DOUBLE PRECISION;  
   v_tintOppStatus  SMALLINT;  
   v_tintshipped  SMALLINT; 
   v_dtshipped  TIMESTAMP; 
   v_numBillCountry  NUMERIC(9,0);
   v_numBillState  NUMERIC(9,0);
   v_numDomainID  NUMERIC(9,0);
   v_tintOppType  SMALLINT;
   v_numItemCode  NUMERIC(9,0);
   v_ItemAmount  NUMERIC(19,4);
   v_numTaxItemID  NUMERIC(9,0);
   v_numoppitemtCode  NUMERIC(9,0);


   v_numOppBizDocItemID  NUMERIC(9,0);
   SWV_RowCount INTEGER;
BEGIN
   v_decInterest := 0;    
   v_decDisc := 0;    
   v_TotalAmt := CAST(0 AS NUMERIC(19,4));

   select   numDivisionId, 0, 0, 0, 0, 0, 0, tintoppstatus, tintshipped, numDomainId, tintopptype INTO v_DivisionID,v_shippingAmt,v_bitBillingTerms,v_intBillingDays,v_bitInterestType,
   v_fltInterest,v_fltDiscount,v_tintOppStatus,v_tintshipped,v_numDomainID,
   v_tintOppType FROM    OpportunityMaster WHERE   numOppId = v_numOppID;          
                                                 

   IF v_numOppBizDocsId = 0 then
            
      RAISE NOTICE '@numOppBizDocsId = 0  ';
   ELSE
      RAISE NOTICE '@numOppBizDocsId >0  ';
      select   coalesce(monShipCost,0), coalesce(bitBillingTerms,false), coalesce(intBillingDays,0), coalesce(bitInterestType,false), coalesce(fltInterest,0), coalesce(fltDiscount,0), dtShippedDate, coalesce(bitDiscountType,false), dtCreatedDate INTO v_shippingAmt,v_bitBillingTerms,v_intBillingDays,v_bitInterestType,v_fltInterest,
      v_fltDiscount,v_dtshipped,v_bitDiscType,v_dtCreatedDate FROM    OpportunityBizDocs WHERE   numOppBizDocsId = v_numOppBizDocsId;
      select   coalesce(SUM(X.amount),0), coalesce(SUM(X.amount*X.tax/100),0) INTO v_TotalAmt,v_TotalTaxAmt FROM(SELECT    CAST(numoppitemtCode AS VARCHAR(20))
         || 'I' AS numoppitemtCode,
                                    OBD.monTotAmount AS Amount,
                                    CASE WHEN bitTaxable = false THEN 0
         WHEN bitTaxable = true
         THEN(SELECT  fltPercentage
               FROM    OpportunityBizDocTaxItems
               WHERE   numOppBizDocID = v_numOppBizDocsId
               AND numTaxItemID = 0)
         END AS Tax
         FROM      OpportunityItems opp
         JOIN Item i ON opp.numItemCode = i.numItemCode
         JOIN OpportunityBizDocItems OBD ON OBD.numOppItemID = opp.numoppitemtCode
         WHERE     numOppId = v_numOppID
         AND OBD.numOppBizDocID = v_numOppBizDocsId
         UNION  
  --timeandexpense (expense)  
         SELECT    CAST(numCategoryHDRID AS VARCHAR(20))
         || 'T' AS numoppitemtCode,
                                    monAmount AS Amount,
                                    CAST(0 AS INTEGER) AS Tax
         FROM      timeandexpense
         WHERE     (numOppId = v_numOppID
         OR numOppBizDocsId = v_numOppBizDocsId)
         AND numCategory = 2
         AND numtype = 1
         UNION   
  --timeandexpense (time)  
         SELECT    CAST(numCategoryHDRID AS VARCHAR(20))
         || 'E' AS numoppitemtCode,
                                    CAST((EXTRACT(DAY FROM dtToDate -dtFromDate)*60*24+EXTRACT(HOUR FROM dtToDate -dtFromDate)*60+EXTRACT(MINUTE FROM dtToDate -dtFromDate)) AS DECIMAL(10,2))*monAmount/60 AS Amount,
                                    CAST(0 AS INTEGER) AS Tax
         FROM      timeandexpense
         WHERE     (numOppId = v_numOppID
         OR numOppBizDocsId = v_numOppBizDocsId)
         AND numCategory = 1
         AND numtype = 1) x;
   end if;   
            
   open SWV_RefCur for
   SELECT    CAST(numCategoryHDRID AS VARCHAR(20))
   || 'E' AS numoppitemtCode,
                                    CAST((EXTRACT(DAY FROM dtToDate -dtFromDate)*60*24+EXTRACT(HOUR FROM dtToDate -dtFromDate)*60+EXTRACT(MINUTE FROM dtToDate -dtFromDate)) AS DECIMAL(10,2))*monAmount/60 AS amount,
                                    0 AS Tax
   FROM      timeandexpense
   WHERE     (numOppId = v_numOppID
   OR numOppBizDocsId = v_numOppBizDocsId)
   AND numCategory = 1
   AND numtype = 1;

   IF v_tintOppType = 1 then
      v_numTaxItemID := 0;
      IF v_numOppBizDocsId = 0 then
                    
         select   numoppitemtCode, numItemCode, monTotAmount INTO v_numoppitemtCode,v_numItemCode,v_ItemAmount FROM    OpportunityItems WHERE   numOppId = v_numOppID   ORDER BY numoppitemtCode LIMIT 1;
         WHILE v_numoppitemtCode > 0 LOOP
            select   numTaxItemID INTO v_numTaxItemID FROM    TaxItems WHERE   numDomainID = v_numDomainID    LIMIT 1;
            WHILE v_numTaxItemID > 0 LOOP
               v_TotalTaxAmt := v_TotalTaxAmt+fn_CalItemTaxAmt(v_numDomainID,v_DivisionID,v_numItemCode,v_numTaxItemID)*v_ItemAmount/100;
               select   numTaxItemID INTO v_numTaxItemID FROM    TaxItems WHERE   numDomainID = v_numDomainID
               AND numTaxItemID > v_numTaxItemID    LIMIT 1;
               GET DIAGNOSTICS SWV_RowCount = ROW_COUNT;
               IF SWV_RowCount = 0 then
                  v_numTaxItemID := 0;
               end if;
            END LOOP;
            select   numoppitemtCode, numItemCode, monTotAmount INTO v_numoppitemtCode,v_numItemCode,v_ItemAmount FROM    OpportunityItems WHERE   numOppId = v_numOppID
            AND numoppitemtCode > v_numoppitemtCode   ORDER BY numoppitemtCode LIMIT 1;
            GET DIAGNOSTICS SWV_RowCount = ROW_COUNT;
            IF SWV_RowCount = 0 then
               v_numoppitemtCode := 0;
            end if;
         END LOOP;
      ELSE
         v_numOppBizDocItemID := 0;
         select   numOppBizDocItemID, I.numItemCode, monTotAmount INTO v_numOppBizDocItemID,v_numItemCode,v_ItemAmount FROM    OpportunityBizDocItems OB
         JOIN Item I ON I.numItemCode = OB.numItemCode WHERE   numOppBizDocID = v_numOppBizDocsId
         AND bitTaxable = true   ORDER BY numOppBizDocItemID LIMIT 1;
         WHILE v_numOppBizDocItemID > 0 LOOP
            select   numTaxItemID INTO v_numTaxItemID FROM    TaxItems WHERE   numDomainID = v_numDomainID    LIMIT 1;
            WHILE v_numTaxItemID > 0 LOOP
               v_TotalTaxAmt := v_TotalTaxAmt+coalesce((SELECT   fltPercentage
               FROM     OpportunityBizDocTaxItems
               WHERE    numOppBizDocID = v_numOppBizDocsId
               AND numTaxItemID = v_numTaxItemID),0)*v_ItemAmount/100;
               select   numTaxItemID INTO v_numTaxItemID FROM    TaxItems WHERE   numDomainID = v_numDomainID
               AND numTaxItemID > v_numTaxItemID    LIMIT 1;
               GET DIAGNOSTICS SWV_RowCount = ROW_COUNT;
               IF SWV_RowCount = 0 then
                  v_numTaxItemID := 0;
               end if;
            END LOOP;
            select   numOppBizDocItemID, I.numItemCode, monTotAmount INTO v_numOppBizDocItemID,v_numItemCode,v_ItemAmount FROM    OpportunityBizDocItems OB
            JOIN Item I ON I.numItemCode = OB.numItemCode WHERE   numOppBizDocID = v_numOppBizDocsId
            AND bitTaxable = true
            AND numOppBizDocItemID > v_numOppBizDocItemID    LIMIT 1;
            GET DIAGNOSTICS SWV_RowCount = ROW_COUNT;
            IF SWV_RowCount = 0 then
               v_numOppBizDocItemID := 0;
            end if;
         END LOOP;
      end if;
   end if;


   IF v_tintOppType = 2 then
      v_TotalTaxAmt := CAST(0 AS NUMERIC(19,4));
   end if;
  
   v_TotalAmtAfterDisc := v_TotalAmt; 
   IF v_tintOppStatus = 1 then
            
      IF v_tintshipped = 1 then
         v_dt := coalesce(v_dtshipped,v_dt);
      end if;
      IF v_bitBillingTerms = true then
                    
         v_strDate := v_dtCreatedDate:: timestamp+CAST(v_intBillingDays || 'day' as interval);
         IF v_bitInterestType = false then
                            
            IF v_strDate > v_dt then
                                    
               v_decDisc := 0;
            ELSE
               v_decDisc := v_fltInterest;
            end if;
         ELSE
            IF v_strDate > v_dt then
                                    
               v_decInterest := 0;
            ELSE
               v_decInterest := v_fltInterest;
            end if;
         end if;
      end if;
      IF v_decDisc > 0 then
                    
         IF v_bitDiscType = false then
            v_TotalAmtAfterDisc := CAST(v_TotalAmt -((v_TotalAmt*v_fltDiscount/100)+((v_TotalAmt*(100 -v_fltDiscount)/100)*v_decDisc/100))+v_shippingAmt+v_TotalTaxAmt AS NUMERIC(19,4));
         ELSEIF v_bitDiscType = true
         then
            v_TotalAmtAfterDisc := CAST(v_TotalAmt
            -(v_fltDiscount+(v_TotalAmt
            -v_fltDiscount)*v_decDisc/100)+v_shippingAmt+v_TotalTaxAmt AS NUMERIC(19,4));
         end if;
      ELSE
         IF v_bitDiscType = false then
            v_TotalAmtAfterDisc := CAST(v_TotalAmt+(v_decInterest*(v_TotalAmt*(100 -v_fltDiscount)/100)/100)
            -(v_TotalAmt*v_fltDiscount/100)+v_shippingAmt+v_TotalTaxAmt AS NUMERIC(19,4));
         ELSEIF v_bitDiscType = true
         then
            v_TotalAmtAfterDisc := CAST(v_TotalAmt+(v_decInterest*(v_TotalAmt
            -v_fltDiscount)/100) -(v_fltDiscount)+v_shippingAmt+v_TotalTaxAmt AS NUMERIC(19,4));
         end if;
      end if;
   ELSE
      IF v_bitDiscType = false then
         v_TotalAmtAfterDisc := CAST(v_TotalAmt+v_shippingAmt+v_TotalTaxAmt -(v_TotalAmt*v_fltDiscount/100) AS NUMERIC(19,4));
      ELSEIF v_bitDiscType = true
      then
         v_TotalAmtAfterDisc := CAST(v_TotalAmt+v_shippingAmt+v_TotalTaxAmt -v_fltDiscount AS NUMERIC(19,4));
      end if;
   end if;

 
  
   open SWV_RefCur2 for
   SELECT  v_TotalAmtAfterDisc;
   RETURN;
END; $$;

--select dbo.GetDealAmount(963,getdate(),1528)


