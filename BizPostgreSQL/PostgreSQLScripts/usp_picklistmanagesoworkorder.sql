-- Stored procedure definition script USP_PickListManageSOWorkOrder for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_PickListManageSOWorkOrder(v_numDomainID NUMERIC(18,0),
	v_numUserCntID NUMERIC(18,0),
	v_numOppID NUMERIC(18,0),
	v_numOppBizDocID NUMERIC(18,0))
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numItemCode  NUMERIC(18,0);
   v_numUnitHour  DOUBLE PRECISION;
   v_numWarehouseItmsID  NUMERIC(18,0);
   v_vcItemDesc  VARCHAR(1000);
   v_numWOId  NUMERIC(18,0);
   v_vcInstruction  VARCHAR(1000);
   v_bintCompliationDate  TIMESTAMP;
   v_numWOAssignedTo  NUMERIC(18,0);
   v_numoppitemtCode  NUMERIC(18,0);
   v_numWarehouseID  NUMERIC(18,0);
   v_numBuildProcessID  NUMERIC(18,0);
   v_i  INTEGER DEFAULT 1;
   v_iCount  INTEGER;
   v_numNewProcessId  NUMERIC(18,0) DEFAULT 0;
BEGIN
   BEGIN
      CREATE TEMP SEQUENCE tt_TEMP_seq INCREMENT BY 1 START WITH 1;
      EXCEPTION WHEN OTHERS THEN
         NULL;
   END;
   DROP TABLE IF EXISTS tt_TEMP CASCADE;
   CREATE TEMPORARY TABLE tt_TEMP
   (
      ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1),
      numoppitemtCode NUMERIC(18,0),
      numItemCode NUMERIC(18,0),
      numUnitHour DOUBLE PRECISION,
      numWarehouseItmsID NUMERIC(18,0),
      vcItemDesc VARCHAR(1000),
      vcInstruction VARCHAR(1000),
      bintCompliationDate TIMESTAMP,
      numWOAssignedTo NUMERIC(18,0),
      numBuildProcessID NUMERIC(18,0)
   );
   INSERT INTO tt_TEMP(numoppitemtCode
		,numItemCode
		,numUnitHour
		,numWarehouseItmsID
		,vcItemDesc
		,vcInstruction
		,bintCompliationDate
		,numBuildProcessID)
   SELECT
   numoppitemtCode
		,OpportunityItems.numItemCode
		,OpportunityBizDocItems.numUnitHour
		,OpportunityItems.numWarehouseItmsID
		,OpportunityItems.vcItemDesc
		,vcInstruction
		,bintCompliationDate
		,coalesce(numBusinessProcessId,0)
   FROM
   OpportunityBizDocItems
   INNER JOIN
   OpportunityItems
   ON
   OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtCode
   INNER JOIN
   Item
   ON
   OpportunityItems.numItemCode = Item.numItemCode
   WHERE
   numOppID = v_numOppID
   AND numOppBizDocID = v_numOppBizDocID
   AND coalesce(bitWorkOrder,false) = true
   AND coalesce(OpportunityBizDocItems.numUnitHour,0) > 0;

   select   COUNT(*) INTO v_iCount FROM tt_TEMP;

   WHILE v_i <= v_iCount LOOP
      select   numItemCode, numUnitHour, numWarehouseItmsID, vcItemDesc, vcInstruction, bintCompliationDate, numoppitemtCode, numBuildProcessID INTO v_numItemCode,v_numUnitHour,v_numWarehouseItmsID,v_vcItemDesc,v_vcInstruction,
      v_bintCompliationDate,v_numoppitemtCode,v_numBuildProcessID FROM
      tt_TEMP WHERE
      ID = v_i;
      v_numWarehouseID := coalesce((SELECT numWareHouseID FROM WareHouseItems WHERE numWareHouseItemID = v_numWarehouseItmsID),0);
      IF EXISTS(SELECT
      Item.numItemCode
      FROM
      Item
      INNER JOIN
      ItemDetails Dtl
      ON
      numChildItemID = numItemCode
      WHERE
      numItemKitID = v_numItemCode
      AND charItemType = 'P'
      AND NOT EXISTS(SELECT numWareHouseItemID FROM WareHouseItems WHERE numItemID = Item.numItemCode AND numWareHouseID = v_numWarehouseID)) then
		
         RAISE NOTICE 'SELECTED_WAREHOUSE_IS_NOT_AVAILABLE_FOR_ALL_CHILD_ITEMS';
         RETURN;
      ELSE
         INSERT INTO WorkOrder(numItemCode
				,numQtyItemsReq
				,numWareHouseItemId
				,numCreatedBy
				,bintCreatedDate
				,numDomainId
				,numWOStatus
				,numOppId
				,vcItemDesc
				,vcInstruction
				,bintCompliationDate
				,numAssignedTo
				,numOppItemID)
			VALUES(v_numItemCode
				,v_numUnitHour
				,v_numWarehouseItmsID
				,v_numUserCntID
				,TIMEZONE('UTC',now())
				,v_numDomainID
				,0
				,v_numOppID
				,v_vcItemDesc
				,v_vcInstruction
				,v_bintCompliationDate
				,coalesce((SELECT numBuildManager FROM Sales_process_List_Master WHERE Slp_Id = v_numBuildProcessID), 0)
				,v_numoppitemtCode);
			
         v_numWOId := CURRVAL('WorkOrder_seq');
         PERFORM USP_UpdateNameTemplateValueForWorkOrder(3::SMALLINT,v_numDomainID,v_numWOId);
         IF(v_numBuildProcessID > 0) then
            INSERT  INTO Sales_process_List_Master(Slp_Name,
														 numDomainID,
														 Pro_Type,
														 numCreatedby,
														 dtCreatedon,
														 numModifedby,
														 dtModifiedOn,tintConfiguration,bitAutomaticStartTimer,bitAssigntoTeams,numOppId,numTaskValidatorId,
														 numProjectId,numWorkOrderId)
            SELECT Slp_Name,
						numDomainID,
						Pro_Type,
						numCreatedby,
						dtCreatedon,
						numModifedby,
						dtModifiedOn,tintConfiguration,bitAutomaticStartTimer,bitAssigntoTeams,0,numTaskValidatorId,0,v_numWOId FROM Sales_process_List_Master WHERE Slp_Id = v_numBuildProcessID;
            v_numNewProcessId := CURRVAL('Sales_process_List_Master_seq');
            UPDATE WorkOrder SET numBuildProcessId = v_numNewProcessId WHERE numWOId = v_numWOId;
            INSERT INTO StagePercentageDetails(numStagePercentageId,
					tintConfiguration,
					vcStageName,
					numdomainid,
					numCreatedBy,
					bintCreatedDate,
					numModifiedBy,
					bintModifiedDate,
					slp_id,
					numAssignTo,
					vcMileStoneName,
					tintPercentage,
					tinProgressPercentage,
					dtStartDate,
					numParentStageID,
					intDueDays,
					numProjectid,
					numOppid,
					vcDescription,
					bitIsDueDaysUsed,
					numTeamId,
					bitRunningDynamicMode,
					numStageOrder,
					numWorkOrderId)
            SELECT
            numStagePercentageId,
					tintConfiguration,
					vcStageName,
					numdomainid,
					v_numUserCntID,
					LOCALTIMESTAMP,
					v_numUserCntID,
					LOCALTIMESTAMP,
					v_numNewProcessId,
					numAssignTo,
					vcMileStoneName,
					tintPercentage,
					tinProgressPercentage,
					LOCALTIMESTAMP,
					numParentStageID,
					intDueDays,
					0,
					0,
					vcDescription,
					bitIsDueDaysUsed,
					numTeamId,
					bitRunningDynamicMode,
					numStageOrder,
					v_numWOId
            FROM
            StagePercentageDetails
            WHERE
            slp_id = v_numBuildProcessID;
            INSERT INTO StagePercentageDetailsTask(numStageDetailsId,
					vcTaskName,
					numHours,
					numMinutes,
					numAssignTo,
					numDomainID,
					numCreatedBy,
					dtmCreatedOn,
					numOppId,
					numProjectId,
					numParentTaskId,
					bitDefaultTask,
					bitSavedTask,
					numOrder,
					numReferenceTaskId,
					numWorkOrderId)
            SELECT(SELECT  SFD.numStageDetailsId FROM StagePercentageDetails AS SFD WHERE SFD.vcStageName = SP.vcStageName AND SFD.numWorkOrderId = v_numWOId LIMIT 1),
					vcTaskName,
					numHours,
					numMinutes,
					ST.numAssignTo,
					v_numDomainID,
					v_numUserCntID,
					LOCALTIMESTAMP,
					0,
					0,
					0,
					1,
					CASE WHEN SP.bitRunningDynamicMode = true THEN 0 ELSE 1 END,
					numOrder,
					numTaskId,
					v_numWOId
            FROM
            StagePercentageDetailsTask AS ST
            LEFT JOIN
            StagePercentageDetails As SP
            ON
            ST.numStageDetailsId = SP.numStageDetailsId
            WHERE
            coalesce(ST.numOppId,0) = 0 AND coalesce(ST.numProjectId,0) = 0 AND
            ST.numStageDetailsId IN(SELECT numStageDetailsId FROM
               StagePercentageDetails As ST
               WHERE
               ST.slp_id = v_numBuildProcessID)
            ORDER BY ST.numOrder;
         end if;
         INSERT INTO WorkOrderDetails(numWOId,numItemKitID,numChildItemID,numQtyItemsReq,numWarehouseItemID,vcItemDesc,sintOrder,numQtyItemsReq_Orig,numUOMId)
         SELECT
         v_numWOId,numItemKitID,numItemCode,
				CAST(((DTL.numQtyItemsReq*coalesce(fn_UOMConversion(DTL.numUOMID,Item.numItemCode,Item.numDomainID,Item.numBaseUnit),1))*v_numUnitHour) AS DOUBLE PRECISION),
				coalesce((SELECT  numWareHouseItemID FROM WareHouseItems WHERE numItemID = Item.numItemCode AND numWareHouseID = v_numWarehouseID ORDER BY coalesce(numWLocationID,0),numWareHouseItemID LIMIT 1),0),
				coalesce(DTL.vcItemDesc,txtItemDesc),
				coalesce(sintOrder,0),
				DTL.numQtyItemsReq,
				DTL.numUOMID
         FROM
         Item
         INNER JOIN
         ItemDetails DTL
         ON
         numChildItemID = numItemCode
         WHERE
         numItemKitID = v_numItemCode;
      end if;
      v_i := v_i::bigint+1;
   END LOOP;
END; $$;


