-- Stored procedure definition script USP_GetJournalId for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetJournalId(v_numCashCreditCardId NUMERIC(9,0) DEFAULT 0,            
v_numDomainId NUMERIC(9,0) DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numJournalId  NUMERIC(9,0);
BEGIN
   open SWV_RefCur for Select numJOurnal_Id From General_Journal_Header Where numCashCreditCardId = v_numCashCreditCardId and numDomainId = v_numDomainId;
END; $$;












