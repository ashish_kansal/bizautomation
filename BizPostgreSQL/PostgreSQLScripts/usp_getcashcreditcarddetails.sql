-- Stored procedure definition script USP_GetCashCreditCardDetails for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetCashCreditCardDetails(v_numCashCreditId NUMERIC(9,0) DEFAULT 0,    
v_numDomainId NUMERIC(9,0) DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for Select * From CashCreditCardDetails Where numCashCreditId = v_numCashCreditId And numDomainID = v_numDomainId;
END; $$;












