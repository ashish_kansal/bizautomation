CREATE OR REPLACE FUNCTION usp_getDynamicFormFieldsList(v_numDomainID NUMERIC(9,0),
 v_numFormId NUMERIC(9,0),
 v_tintMode SMALLINT DEFAULT 0, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
BEGIN
   IF v_tintMode = 0 then

      open SWV_RefCur for
      select DFFM.numFormID,DFM.numFieldID as numFormFieldId,coalesce(DFFM.vcFieldName,DFM.vcFieldName) as vcFormFieldName,
 coalesce(DFV.vcNewFormFieldName,'') AS vcNewFormFieldName,
 coalesce(DFFM.vcAssociatedControlType,DFM.vcAssociatedControlType) as vcAssociatedControlType,
coalesce(DFMMaster.tintFlag,0) AS tintFlag,
  coalesce(DFV.bitIsRequired,false) AS bitIsRequired,coalesce(DFV.bitIsEmail,false) AS bitIsEmail,coalesce(DFV.bitIsAlphaNumeric,false) AS bitIsAlphaNumeric,coalesce(DFV.bitIsNumeric,false) AS bitIsNumeric,coalesce(DFV.bitIsLengthValidation,false) AS bitIsLengthValidation,
  coalesce(DFV.vcToolTip,coalesce(DFM.vcToolTip,'')) AS vcToolTip
      FROM DycFormField_Mapping DFFM
      JOIN DycFieldMaster DFM on DFFM.numModuleID = DFFM.numModuleID and DFFM.numFieldID = DFM.numFieldID
      left join DynamicFormField_Validation DFV on DFV.numDomainID = v_numDomainID AND DFM.numFieldID = DFV.numFormFieldId AND DFV.numFormId = DFFM.numFormID
      LEFT JOIN dynamicFormMaster DFMMaster ON DFFM.numFormID = DFMMaster.numFormID
      where DFFM.numFormID = v_numFormId AND coalesce(DFM.bitDeleted,false) = false ORDER BY vcFormFieldName;
   ELSEIF v_tintMode = 1
   then

      open SWV_RefCur for
      select numFormId,CAST(numFieldID AS VARCHAR(10)) || '~' || vcAssociatedControlType || '~' || vcOrigDbColumnName || '~' || coalesce(vcListItemType,'') || '~' || CAST(coalesce(numListID,0) AS VARCHAR(10)) AS numFormFieldId,vcFieldName as vcFormFieldName
      FROM View_DynamicDefaultColumns
      where numDomainID = v_numDomainID AND numFormId = v_numFormId AND coalesce(bitAllowGridColor,false) = true ORDER BY vcFormFieldName;
   end if;
   RETURN;
END; $$;


