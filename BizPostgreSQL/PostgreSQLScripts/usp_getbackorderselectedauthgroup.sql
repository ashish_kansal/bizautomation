-- Stored procedure definition script USP_GetBackOrderSelectedAuthGroup for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetBackOrderSelectedAuthGroup(v_numDomainID NUMERIC(9,0) DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT cast(AGB.numGroupID as VARCHAR(255)),cast(AGM.vcGroupName as VARCHAR(255))
   FROM AuthenticationGroupBackOrder AGB
   JOIN AuthenticationGroupMaster AGM
   ON AGB.numGroupID = AGM.numGroupID
   WHERE AGB.numDomainID = v_numDomainID;
END; $$;












