-- Stored procedure definition script USP_EmailHistory_GetByIDsString for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_EmailHistory_GetByIDsString(v_vcEmailHstrID TEXT,
	v_numDomainID NUMERIC(18,0),
	v_numUserCntID NUMERIC(18,0), INOUT SWV_RefCur refcursor)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_strSQL  TEXT;
BEGIN
   v_strSQL := 'SELECT numEmailHstrID,numUid,vcNodeName,chrSource FROM EmailHistory LEFT JOIN InboxTreeSort ON EmailHistory.numNodeID = InboxTreeSort.numNodeID WHERE 
	EmailHistory.numDomainID =' || SUBSTR(CAST(v_numDomainID AS VARCHAR(9)),1,9) || ' AND EmailHistory.numUserCntId =' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(9)),1,9) || ' AND 
	EmailHistory.numEmailHstrID IN(' || coalesce(v_vcEmailHstrID,'') || ')';
	
   OPEN SWV_RefCur FOR EXECUTE v_strSQL;
   RETURN;
END; $$;





