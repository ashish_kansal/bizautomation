-- Stored procedure definition script USP_GetInventoryAuditReport for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_GetInventoryAuditReport(v_numDomainID NUMERIC(9,0), INOUT SWV_RefCur refcursor , INOUT SWV_RefCur2 refcursor)
LANGUAGE plpgsql
   AS $$
BEGIN
 

-----------------------------SO-------------------------
   DROP TABLE IF EXISTS 	tt_TEMPSO CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPSO ON COMMIT DROP AS
      select Opp.numItemCode,Opp.numWarehouseItmsID,SUM(Opp.numUnitHour -coalesce(Opp.numQtyShipped,0)) AS SOQty

      FROM   OpportunityItems Opp
      JOIN OpportunityMaster OM ON Opp.numOppId = OM.numOppId
      JOIN Item I ON Opp.numItemCode = I.numItemCode
      JOIN DivisionMaster DM ON OM.numDivisionId = DM.numDivisionID
      LEFT JOIN CompanyInfo CI ON CI.numCompanyId = DM.numCompanyID
      WHERE   OM.numDomainId = v_numDomainID
      AND I.numDomainID = v_numDomainID
      AND coalesce(OM.tintshipped,0) = 0 AND OM.tintoppstatus = 1
      AND OM.tintopptype = 1 AND Opp.numWarehouseItmsID > 0 AND coalesce(Opp.bitDropShip,false) = false
      GROUP BY Opp.numItemCode,Opp.numWarehouseItmsID;

   open SWV_RefCur for
   SELECT i.vcItemName,W.vcWareHouse,WI.numAllocation,WI.numBackOrder,coalesce(WI.numAllocation,0)+coalesce(WI.numBackOrder,0) AS WTotal,temp.SOQty
   FROM Item i JOIN tt_TEMPSO temp ON i.numItemCode = temp.numItemCode
   JOIN WareHouseItems WI ON WI.numItemID = i.numItemCode AND WI.numWareHouseItemID = temp.numWarehouseItmsID
   JOIN Warehouses W ON W.numWareHouseID = WI.numWareHouseID
   WHERE(coalesce(WI.numAllocation,0)+coalesce(WI.numBackOrder,0)) <> temp.SOQty;		
			
		


----------------------PO--------------
   DROP TABLE IF EXISTS 	tt_TEMPPO CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPPO ON COMMIT DROP AS
      select Opp.numItemCode,Opp.numWarehouseItmsID,SUM(Opp.numUnitHour -coalesce(Opp.numUnitHourReceived,0)) AS POQty

      FROM   OpportunityItems Opp
      JOIN OpportunityMaster OM ON Opp.numOppId = OM.numOppId
      JOIN Item I ON Opp.numItemCode = I.numItemCode
      JOIN DivisionMaster DM ON OM.numDivisionId = DM.numDivisionID
      LEFT JOIN CompanyInfo CI ON CI.numCompanyId = DM.numCompanyID
      WHERE   OM.numDomainId = v_numDomainID
      AND I.numDomainID = v_numDomainID
      AND coalesce(OM.tintshipped,0) = 0 AND OM.tintoppstatus = 2
      AND OM.tintopptype = 1 AND Opp.numWarehouseItmsID > 0 AND coalesce(Opp.bitDropShip,false) = false
      GROUP BY Opp.numItemCode,Opp.numWarehouseItmsID;

   open SWV_RefCur2 for
   SELECT i.vcItemName,W.vcWareHouse,WI.numonOrder,temp.POQty
   FROM Item i JOIN tt_TEMPPO temp ON i.numItemCode = temp.numItemCode
   JOIN WareHouseItems WI ON WI.numItemID = i.numItemCode AND WI.numWareHouseItemID = temp.numWarehouseItmsID
   JOIN Warehouses W ON W.numWareHouseID = WI.numWareHouseID
   WHERE coalesce(WI.numonOrder,0) <> temp.POQty;		
			

/****** Object:  StoredProcedure [dbo].[USP_GetInvoiceList]    Script Date: 07/26/2008 16:17:34 ******/
   RETURN;
END; $$;


