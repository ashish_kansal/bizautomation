-- Stored procedure definition script usp_GetMySales for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION usp_GetMySales(          
--          
v_numDomainID NUMERIC,          
 v_dtDateFrom TIMESTAMP,          
 v_dtDateTo TIMESTAMP,          
 v_numUserCntID NUMERIC,          
 v_intType NUMERIC, INOUT SWV_RefCur refcursor)
LANGUAGE plpgsql
   AS $$
BEGIN
   IF v_intType = 0 then
 
      open SWV_RefCur for
      select  A.vcFirstName || ' ' || A.vcLastname as Name,sum(monPAmount) as Amount  from OpportunityMaster OM
      join AdditionalContactsInformation A on A.numContactId = OM.numrecowner
      where OM.tintoppstatus = 1
      AND OM.numDomainId = v_numDomainID
      AND OM.bintAccountClosingDate BETWEEN v_dtDateFrom AND v_dtDateTo
      AND OM.numrecowner = v_numUserCntID
      group by A.vcFirstName,A.vcLastname order by Amount desc LIMIT 5;
   ELSE
      open SWV_RefCur for
      select  A.vcFirstName || ' ' || A.vcLastname as Name,sum(monPAmount) as Amount  from OpportunityMaster OM
      join AdditionalContactsInformation A on A.numContactId = OM.numrecowner
      where OM.tintoppstatus = 1
      AND OM.numDomainId = v_numDomainID
      AND OM.bintAccountClosingDate BETWEEN v_dtDateFrom AND v_dtDateTo
      AND A.numTeam is not null
      AND A.numTeam in(select F.numTeam from ForReportsByTeam F
         where F.numUserCntID = v_numUserCntID and F.numDomainID = v_numDomainID and F.tintType = v_intType)
      group by A.vcFirstName,A.vcLastname order by Amount desc LIMIT 5;
   end if;
   RETURN;
END; $$;


