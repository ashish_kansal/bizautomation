-- Stored procedure definition script USP_ManageTransactionHistory for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021

CREATE OR REPLACE FUNCTION USP_ManageTransactionHistory(INOUT v_numTransHistoryID NUMERIC(18,0) ,
	v_numDomainID NUMERIC(18,0),
	v_numDivisionID NUMERIC(18,0),
	v_numContactID NUMERIC(18,0),
	v_numOppID NUMERIC(18,0),
	v_numOppBizDocsID NUMERIC(18,0),
	v_vcTransactionID VARCHAR(100),
	v_tintTransactionStatus SMALLINT,
	v_vcPGResponse VARCHAR(200),
	v_tintType SMALLINT,
	v_monAuthorizedAmt DECIMAL(20,5),
	v_monCapturedAmt DECIMAL(20,5),
	v_monRefundAmt DECIMAL(20,5),
	v_vcCardHolder VARCHAR(500),
	v_vcCreditCardNo VARCHAR(500),
	v_vcCVV2 VARCHAR(200),
	v_tintValidMonth SMALLINT,
	v_intValidYear INTEGER,
	v_vcSignatureFile VARCHAR(100),
	v_numUserCntID NUMERIC(18,0),
	v_numCardType NUMERIC(9,0),
	v_vcResponseCode VARCHAR(500) DEFAULT '',
	v_intPaymentGateWay INTEGER DEFAULT 0)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_OldCapturedAmt  DECIMAL(20,5);
   v_OldRefundAmt  DECIMAL(20,5);
BEGIN

   /* SQLWays Notice: SET TRANSACTION ISOLATION LEVEL READ COMMITTED must be called before procedure call */
-- SET TRANSACTION ISOLATION LEVEL READ COMMITTED
IF EXISTS(SELECT numTransHistoryID FROM TransactionHistory WHERE numTransHistoryID = v_numTransHistoryID) then
      select   coalesce(monCapturedAmt,0), coalesce(monRefundAmt,0) INTO v_OldCapturedAmt,v_OldRefundAmt FROM
      TransactionHistory WHERE
      numTransHistoryID = v_numTransHistoryID;
      IF v_tintTransactionStatus = 2 then -- captured
	
         v_OldCapturedAmt := v_OldCapturedAmt+coalesce(v_monCapturedAmt,0);
         v_OldRefundAmt := coalesce(v_monRefundAmt,0)+coalesce(v_OldRefundAmt,0);
      ELSEIF v_tintTransactionStatus = 5
      then -- Credited
	
         v_OldRefundAmt := coalesce(v_monRefundAmt,0)+coalesce(v_OldRefundAmt,0);
      end if;
      UPDATE TransactionHistory SET
      tintTransactionStatus = v_tintTransactionStatus,monCapturedAmt = v_OldCapturedAmt,
      monRefundAmt = v_OldRefundAmt,numModifiedBy = v_numUserCntID,
      vcPGResponse =(CASE WHEN v_tintTransactionStatus = 2 OR v_tintTransactionStatus = 4 THEN v_vcPGResponse ELSE vcPGResponse END),dtModifiedDate = TIMEZONE('UTC',now())
      WHERE
      numTransHistoryID = v_numTransHistoryID AND numDomainID = v_numDomainID;
   ELSE
      INSERT INTO TransactionHistory(numDomainID,
		numDivisionID,
		numContactID,
		numOppID,
		numOppBizDocsID,
		vcTransactionID,
		tintTransactionStatus,
		vcPGResponse,
		tintType,
		monAuthorizedAmt,
		monCapturedAmt,
		monRefundAmt,
		vcCardHolder,
		vcCreditCardNo,
		vcCVV2,
		tintValidMonth,
		intValidYear,
		vcSignatureFile,
		numCreatedBy,
		dtCreatedDate,
		numModifiedBy,
		dtModifiedDate,
		numCardType,
		vcResponseCode,
		intPaymentGateWay) VALUES(v_numDomainID,
		v_numDivisionID,
		v_numContactID,
		v_numOppID,
		v_numOppBizDocsID,
		v_vcTransactionID,
		v_tintTransactionStatus,
		v_vcPGResponse,
		v_tintType,
		v_monAuthorizedAmt,
		v_monCapturedAmt,
		v_monRefundAmt,
		v_vcCardHolder,
		v_vcCreditCardNo,
		v_vcCVV2,
		v_tintValidMonth,
		v_intValidYear,
		v_vcSignatureFile,
		v_numUserCntID,
		TIMEZONE('UTC',now()),
		v_numUserCntID,
		TIMEZONE('UTC',now()),
		v_numCardType,
		v_vcResponseCode,
		coalesce(v_intPaymentGateWay, 0)) RETURNING numTransHistoryID INTO v_numTransHistoryID;
   end if;
   RETURN;
END; $$;


