DROP FUNCTION IF EXISTS USP_OppUpDateBizDocsInvoiceDetails;

CREATE OR REPLACE FUNCTION USP_OppUpDateBizDocsInvoiceDetails(v_numOppBizDocsId NUMERIC(9,0) DEFAULT null,                      
 v_UserCntID NUMERIC(9,0) DEFAULT null,                     
 v_Comments VARCHAR(1000) DEFAULT null,
 --@numBizDocStatus as numeric(9),
 v_numShipCompany NUMERIC(9,0) DEFAULT NULL,
 v_vcTrackingURL VARCHAR(1000) DEFAULT NULL)
RETURNS VOID LANGUAGE plpgsql                      
/* Modifed to update selective fields */
   AS $$
   DECLARE
   v_sql  VARCHAR(4000);
BEGIN
   v_sql := 'update OpportunityBizDocs Set ';

    v_sql := coalesce(v_sql,'') || ' vcComments=''' || coalesce(v_Comments,'') || '''';

   IF(v_UserCntID > 0) then
      v_sql := coalesce(v_sql,'') || ' ,numModifiedBy=' || SUBSTR(CAST(v_UserCntID AS VARCHAR(10)),1,10);
   end if;

  

   IF(v_numShipCompany > 0) then
      v_sql := coalesce(v_sql,'') || ' ,numShipVia=' || SUBSTR(CAST(v_numShipCompany AS VARCHAR(10)),1,10);
   end if;
   IF(LENGTH(v_vcTrackingURL) > 0) then
      v_sql := coalesce(v_sql,'') || ' ,vcTrackingURL=''' || coalesce(v_vcTrackingURL,'') || '''';
   end if;
   
   v_sql := coalesce(v_sql,'') || 'where numOppBizDocsId= ' || SUBSTR(CAST(v_numOppBizDocsId AS VARCHAR(10)),1,10);

   RAISE NOTICE '%',v_sql;
   EXECUTE v_sql;
   RETURN;
END; $$;

--update OpportunityBizDocs
--	set  vcPurchaseOdrNo=@vcPurchaseOdrNo,                      
--	numShipVia=@numShipCompany,                      
--	numModifiedBy=@UserCntID,                      
--	vcComments=@Comments,                      
--	dtModifiedDate=getutcdate(),
--    numShipDoc= @numShipDoc,
--    numBizDocStatus=@numBizDocStatus,
--    vcTrackingURL=@vcTrackingURL                     
--	where numOppBizDocsId=@numOppBizDocsId
--GO



