DROP FUNCTION IF EXISTS USP_GetWorkFlowMasterDetail;

Create or replace FUNCTION USP_GetWorkFlowMasterDetail(v_numDomainID NUMERIC(18,0),
	v_numWFID NUMERIC(18,0),
	v_ClientTimeZoneOffset INTEGER, INOUT SWV_RefCur refcursor , INOUT SWV_RefCur2 refcursor , INOUT SWV_RefCur3 refcursor , INOUT SWV_RefCur4 refcursor , INOUT SWV_RefCur5 refcursor)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numFormId  NUMERIC(18,0);
   v_vcLocationID  VARCHAR(100);
BEGIN
	OPEN SWV_RefCur FOR
	SELECT
		COALESCE(fn_GetContactName(WF.numCreatedBy),'-') || ' ' || CAST(WF.dtCreatedDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) AS VARCHAR(20)) as "CreatedBy"
		,coalesce(fn_GetContactName(WF.numModifiedBy),'-') || ' ' || CAST(WF.dtModifiedDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) AS VARCHAR(20)) as "ModifiedBy"
		,numWFID AS "numWFID"
		,numDomainID AS "numDomainID"
		,vcWFName AS "vcWFName"
		,vcWFDescription AS "vcWFDescription"
		,numCreatedBy AS "numCreatedBy"
		,numModifiedBy AS "numModifiedBy"
		,dtCreatedDate AS "dtCreatedDate"
		,dtModifiedDate AS "dtModifiedDate"
		,bitActive AS "bitActive"
		,numFormID AS "numFormID"
		,tintWFTriggerOn AS "tintWFTriggerOn"
		,vcWFAction AS "vcWFAction"
		,vcDateField AS "vcDateField"
		,intDays AS "intDays"
		,intActionOn AS "intActionOn"
		,bitCustom AS "bitCustom"
		,numFieldID AS "numFieldID"
		,COALESCE(vcWebsitePage,'') AS "vcWebsitePage"
		,COALESCE(intWebsiteDays,0) AS "intWebsiteDays"
	FROM 
		WorkFlowMaster WF 
	WHERE
		WF.numDomainID = v_numDomainID AND WF.numWFID = v_numWFID;
--please

   DROP TABLE IF EXISTS tt_TEMPFIELD CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPFIELD
   (
      numFieldID NUMERIC,
      vcFieldName VARCHAR(50),
      vcDbColumnName VARCHAR(50),
      vcOrigDbColumnName VARCHAR(50),
      vcFieldDataType CHAR(1),
      vcAssociatedControlType VARCHAR(50),
      vcListItemType VARCHAR(3),
      numListID NUMERIC,
      bitCustom BOOLEAN,
      vcLookBackTableName VARCHAR(50),
      bitAllowFiltering BOOLEAN,
      bitAllowEdit BOOLEAN
   );  
   select   numFormID INTO v_numFormId from WorkFlowMaster WHERE numDomainID = v_numDomainID  AND numWFID = v_numWFID;

	IF (v_numFormId = 71) then
		v_numFormId := 49;
	elseif (v_numFormId = 148) then
		v_numFormId := 68;
	end if;
--Regular Fields  
   INSERT INTO tt_TEMPFIELD
   SELECT numFieldID,vcFieldName,
     vcDbColumnName,vcOrigDbColumnName,vcFieldDataType,vcAssociatedControlType,
     vcListItemType,numListID,false AS bitCustom,vcLookBackTableName,bitAllowFiltering,bitAllowEdit
   FROM View_DynamicDefaultColumns
   where numFormId = v_numFormId and numDomainID = v_numDomainID
   AND coalesce(bitWorkFlowField,false) = true AND coalesce(bitCustom,0) = 0;  
  
  
   v_vcLocationID := '0';  
   select   coalesce(vcLocationID,'0') INTO v_vcLocationID from dynamicFormMaster where numFormID = v_numFormId;  
  
   IF v_numFormId IN(69,70,49,72,73,124) then

      IF LENGTH(coalesce(v_vcLocationID,'')) > 0 then
	
         v_vcLocationID := CONCAT(v_vcLocationID,',1,12,13,14');
      ELSE
         v_vcLocationID := '1,12,13,14';
      end if;
   end if;


--Custom Fields     
   INSERT INTO tt_TEMPFIELD
   SELECT coalesce(CFM.Fld_id,0) as numFieldID,CFM.FLd_label as vcFieldName,
    CAST('Cust' || CAST(Fld_Id AS VARCHAR(10)) AS VARCHAR(50)) as vcDbColumnName,CAST('Cust' || CAST(Fld_Id AS VARCHAR(10)) AS VARCHAR(50)) AS vcOrigDbColumnName,CASE CFM.fld_type WHEN 'SelectBox' THEN 'V' WHEN 'DateField' THEN 'D' ELSE 'V' END AS vcFieldDataType,
    CFM.fld_type AS vcAssociatedControlType,CAST(CASE WHEN CFM.fld_type = 'SelectBox' THEN 'LI' ELSE '' END AS VARCHAR(3)) AS vcListItemType,coalesce(CFM.numlistid,0) as numListID,
    true AS bitCustom,L.vcCustomLookBackTableName AS vcLookBackTableName,false AS bitAllowFiltering,false AS bitAllowEdit
   FROM CFW_Fld_Master CFM LEFT JOIN CFW_Validation V ON V.numFieldId = CFM.Fld_id
   JOIN CFW_Loc_Master L on CFM.Grp_id = L.Loc_id
   WHERE   CFM.numDomainID = v_numDomainID
   AND GRP_ID IN(SELECT ID FROM SplitIDs(v_vcLocationID,','));  



--Fire Trigger Events on Fields
	open SWV_RefCur2 for
	SELECT 
		numFieldID AS "numFieldID"
		,bitCustom AS "bitCustom"
		,numWFID AS "numWFID"
		,numWFTriggerFieldID AS "numWFTriggerFieldID"
		,CAST(numFieldId AS VARCHAR(20)) || '_' || CASE WHEN bitCustom = false THEN 'False' ELSE 'True' End AS "FieldID"
		,(SELECT tt_TEMPFIELD.vcFieldName FROM tt_TEMPFIELD WHERE tt_TEMPFIELD.numFieldID=WorkFlowTriggerFieldList.numFieldID) AS "FieldName"
	FROM 
		WorkFlowTriggerFieldList 
	WHERE 
		numWFID = v_numWFID;

   open SWV_RefCur3 for
   SELECT 
		numWFConditionID AS "numWFConditionID"
		,numWFID AS "numWFID"
		,t1.numFieldID AS "numFieldID"
		,t1.bitCustom AS "bitCustom"
		,vcFilterValue AS "vcFilterValue"
		,vcFilterOperator AS "vcFilterOperator"
		,vcFilterANDOR AS "vcFilterANDOR"
		,vcFilterData AS "vcFilterData"
		,intCompare AS "intCompare"
		,CAST(CAST(WorkFlowConditionList.numFieldId AS VARCHAR(20)) || '_' || CASE WHEN WorkFlowConditionList.bitCustom = false THEN 'False' ELSE 'True' End AS VARCHAR(80)) AS "FieldID"
		,t1.vcFieldName AS "FieldName"
		,t1.vcLookBackTableName AS "vcLookBackTableName"
		,CASE WHEN  vcFilterOperator = 'eq' THEN 'equals' WHEN vcFilterOperator = 'ne' THEN 'not equal to' WHEN vcFilterOperator = 'gt' THEN 'greater than' WHEN vcFilterOperator = 'ge' THEN 'greater or equal' WHEN vcFilterOperator = 'lt' THEN 'less than' WHEN vcFilterOperator = 'le' THEN 'less or equal' WHEN (vcFilterOperator = 'Before' OR vcFilterOperator = 'After') THEN 'is' ELSE 'NA' END AS "FilterOperator"
   FROM
   WorkFlowConditionList
   LEFT JOIN
   tt_TEMPFIELD t1
   ON
   t1.numFieldID = WorkFlowConditionList.numFieldId
   WHERE
   numWFID = v_numWFID;


   open SWV_RefCur4 for
   SELECT numWFActionID AS "numWFActionID"
      ,numWFID AS "numWFID"
      ,tintActionType AS "tintActionType"
      ,numTemplateID AS "numTemplateID"
      ,vcEmailToType AS "vcEmailToType"
	  ,tintEmailFrom AS "tintEmailFrom"
      ,tintTicklerActionAssignedTo AS "tintTicklerActionAssignedTo"
      ,vcAlertMessage AS "vcAlertMessage"
      ,tintActionOrder AS "tintActionOrder"
      ,tintIsAttachment AS "tintIsAttachment"
      ,vcApproval AS "vcApproval"
      ,tintSendMail AS "tintSendMail"
      ,numBizDocTypeID AS "numBizDocTypeID"
      ,numBizDocTemplateID AS "numBizDocTemplateID"
      ,vcSMSText AS "vcSMSText"
      ,vcMailSubject AS "vcMailSubject"
      ,vcMailBody AS "vcMailBody"
      ,vcEmailSendTo AS "vcEmailSendTo"
	  ,CASE WHEN tintActionType = 1 THEN(SELECT  VcDocName
         FROM    GenericDocuments
         WHERE   numDocCategory = 369
         AND tintDocumentType = 1 --1 =generic,2=specific  
         AND coalesce(vcDocumentSection,'') <> 'M'
         AND numDomainId = v_numDomainID  AND coalesce(VcFileName,'') not ilike '#SYS#%' AND numGenericDocID = numTemplateID)
   WHEN	tintActionType = 2 then(Select TemplateName From tblActionItemData  where numDomainID = v_numDomainID AND RowID = numTemplateID)
   ELSE '0'  END AS "EmailTemplate",
         CASE WHEN vcEmailToType = '1' THEN 'Owner of trigger record' WHEN vcEmailToType = '2' THEN 'Assignee of trigger record'  WHEN vcEmailToType = '1,2' THEN 'Owner of trigger record,Assignee of trigger record'  WHEN   vcEmailToType = '3' THEN 'Primary contact of trigger record' WHEN   vcEmailToType = '1,3' THEN 'Owner of trigger record,Primary contact of trigger record' WHEN vcEmailToType = '2,3' THEN 'Primary contact of trigger record,Assignee of trigger record'  WHEN vcEmailToType = '1,2,3' THEN  'Owner of trigger record,Primary contact of trigger record,Assignee of trigger record' ELSE NULL END AS "EmailToType",
         CASE WHEN tintTicklerActionAssignedTo = 1 THEN 'Owner of trigger record' ELSE 'Assignee of trigger record' END AS "TicklerAssignedTo",
		 (select vcTemplateName from BizDocTemplate where numBizDocTempID = numBizDocTemplateID) as  "vcBizDocTemplate",
		 (SELECT coalesce(vcRenamedListName,vcData) as vcData FROM Listdetails Ld
      left join listorder LO
      on Ld.numListItemID = LO.numListItemID and LO.numDomainId = v_numDomainID
      WHERE Ld.numListID = 27 AND (constFlag = true or Ld.numDomainid = v_numDomainID)    AND  Ld.numListItemID = numBizDocTypeID) as "vcBizDoc" ,
		 (SELECT Case When numOppType = 2 then 'Purchase' else 'Sales' end As BizDocType from BizDocTemplate WHERE numBizDocTempID = numBizDocTemplateID) as "vcBizDocType",
		 (SELECT numOppType from BizDocTemplate where numBizDocTempID = numBizDocTemplateID) as "numOppType",
		 coalesce(vcApproval,'') as "vcApprovalName"
   FROM WorkFlowActionList WHERE numWFID = v_numWFID;

 
	open SWV_RefCur5 for
	SELECT numWFActionUFID AS "numWFActionUFID"
      ,numWFActionID AS "numWFActionID"
      ,tintModuleType AS "tintModuleType"
      ,numFieldID AS "numFieldID"
      ,bitCustom AS "bitCustom"
      ,vcValue AS "vcValue"
      ,vcData AS "vcData"
	,CAST(CAST(numFieldId AS VARCHAR(20)) || '_' || CASE WHEN bitCustom = false THEN 'False' ELSE 'True' End AS VARCHAR(80)) AS "FieldID"
	,(SELECT tt_TEMPFIELD.vcFieldName FROM tt_TEMPFIELD WHERE tt_TEMPFIELD.numFieldID = WorkFlowActionUpdateFields.numFieldID) AS "FieldName"  FROM WorkFlowActionUpdateFields WHERE numWFActionID IN(SELECT numWFActionID FROM WorkFlowActionList WHERE numWFID = v_numWFID);
   RETURN;
END; $$;


