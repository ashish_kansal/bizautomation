-- Stored procedure definition script USP_GetSubscriptionHistory for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetSubscriptionHistory(v_numSubscriberID NUMERIC(9,0) DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for select numSubscriberDTLID, numSubscriberID, intNoofSubscribers,
dtSubStartDate, dtSubRenewDate,case when bitTrial = false then 'No' when bitTrial = true then 'Yes' else 'No' End as Trial,
case when bitStatus = false then 'Supended' when bitStatus = true then 'Active' else 'Supended' End as Status, dtSuspendedDate, vcSuspendedReason, numTargetDomainID,
numDomainID, numCreatedBy, dtCreatedDate from SubscriberHstr
   where numSubscriberID = v_numSubscriberID;
END; $$;

