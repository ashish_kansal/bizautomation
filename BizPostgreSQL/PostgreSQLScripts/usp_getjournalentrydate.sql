-- Stored procedure definition script USP_GetJournalEntryDate for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetJournalEntryDate(v_numJournalId NUMERIC(9,0) DEFAULT 0,    
v_numDomainId NUMERIC(9,0) DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for Select numJOurnal_Id,
		 numRecurringId,
		 datEntry_Date,
		 varDescription,
		 numAmount,
		 numDomainId,
		 numCheckId,
		 numCashCreditCardId,
		 numChartAcntId,
		 numOppId,
		 numOppBizDocsId,
		 numDepositId,
		 numBizDocsPaymentDetId,
		 bitOpeningBalance,
		 dtLastRecurringDate,
		 numNoTransactions,
		 numCreatedBy,
		 datCreatedDate,
		 numCategoryHDRID,
		 numProjectID,cast(coalesce(numClassID,0) as NUMERIC(18,0)) AS numClassID  from General_Journal_Header Where numJOurnal_Id = v_numJournalId And numDomainId = v_numDomainId;
END; $$;












