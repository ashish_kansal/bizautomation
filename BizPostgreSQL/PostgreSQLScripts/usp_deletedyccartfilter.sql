-- Stored procedure definition script usp_DeleteDycCartFilter for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021

CREATE OR REPLACE FUNCTION usp_DeleteDycCartFilter(v_numFieldID NUMERIC(18,0),
	v_numFormID NUMERIC(18,0),
	v_numSiteID NUMERIC(18,0),
	v_numDomainID NUMERIC(18,0))
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   DELETE FROM DycCartFilters
   WHERE
   numFieldID = v_numFieldID
   AND numFormID = v_numFormID
   AND numSiteID = v_numSiteID
   AND numDomainID = v_numDomainID;
   RETURN;
END; $$;



