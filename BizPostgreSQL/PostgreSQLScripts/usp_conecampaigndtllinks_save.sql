-- Stored procedure definition script USP_ConECampaignDTLLinks_Save for PostgreSQL
CREATE OR REPLACE FUNCTION USP_ConECampaignDTLLinks_Save(v_numConECampDTLID NUMERIC(9,0) DEFAULT 0,
 v_strBroadcastingLink TEXT DEFAULT '',INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_hDoc4  INTEGER;
BEGIN
	INSERT INTO ConECampaignDTLLinks
	(
		numConECampaignDTLID,
		vcOriginalLink
	)
	SELECT
		v_numConECampDTLID,
        X.vcOriginalLink
	FROM
	XMLTABLE
	(
		'LinkRecords/Table'
		PASSING 
			CAST(v_strBroadcastingLink AS XML)
		COLUMNS
			id FOR ORDINALITY,
			vcOriginalLink TEXT PATH 'vcOriginalLink'
	) AS X;       

	open SWV_RefCur for SELECT * FROM ConECampaignDTLLinks WHERE numConECampaignDTLID = v_numConECampDTLID;
END; $$;












