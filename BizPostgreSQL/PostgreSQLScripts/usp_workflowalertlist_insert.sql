-- Stored procedure definition script USP_WorkFlowAlertList_Insert for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_WorkFlowAlertList_Insert(v_vcAlertMessage TEXT DEFAULT Null,   
 v_numWFID NUMERIC(18,0) DEFAULT NULL,  
 v_intAlertStatus INTEGER DEFAULT NULL,  
 v_numDomainID NUMERIC(18,0) DEFAULT NULL)
RETURNS VOID LANGUAGE plpgsql   
 -- Add the parameters for the stored procedure here  
   AS $$
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
   
  
    -- Insert statements for procedure here  
  
   
   INSERT INTO WorkFlowAlertList(vcAlertMessage,numDomainID,numWFID,intAlertStatus)
     VALUES(v_vcAlertMessage,v_numDomainID,v_numWFID,v_intAlertStatus);
RETURN;
END; $$;  


