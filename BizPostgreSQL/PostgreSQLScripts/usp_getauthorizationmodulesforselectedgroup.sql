-- Stored procedure definition script usp_GetAuthorizationModulesForSelectedGroup for PostgreSQL
CREATE OR REPLACE FUNCTION usp_GetAuthorizationModulesForSelectedGroup(v_numGroupID NUMERIC(9,0)   
--
,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT DISTINCT cast(numModuleID as VARCHAR(255)) FROM GroupAuthorization
   WHERE numGroupID = v_numGroupID;
END; $$;












