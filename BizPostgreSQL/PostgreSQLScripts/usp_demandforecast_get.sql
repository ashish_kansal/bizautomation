-- Stored procedure definition script USP_DemandForecast_Get for PostgreSQL
CREATE OR REPLACE FUNCTION USP_DemandForecast_Get(v_numDomainID NUMERIC(18,0),
	v_numPageIndex INTEGER,
	v_numPageSize INTEGER,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_TotalRowCount  INTEGER DEFAULT 0;
BEGIN
   select   COUNT(*) INTO v_TotalRowCount FROM DemandForecast WHERE numDomainID = v_numDomainID;

   open SWV_RefCur for SELECT * FROM(SELECT
      ROW_NUMBER() OVER(ORDER BY DemandForecast.numDFID) AS NUMBER,
				DemandForecast.numDomainID,
				DemandForecast.numDFID,
				DemandForecast.bitIncludeOpenReleaseDates,
				SUBSTR(TableItemClassification.list,1,LENGTH(TableItemClassification.list) -1) AS vcItemClassification,
				SUBSTR(TableItemGroup.list,1,LENGTH(TableItemGroup.list) -1) AS vcItemGroups,
				SUBSTR(TableWarehouse.list,1,LENGTH(TableWarehouse.list) -1) AS vcWarehouse,
				DemandForecastAnalysisPattern.vcAnalysisPattern AS vcAnalysisPattern,
				DemandForecastDays.vcDays AS vcForecastDays,
				coalesce(v_TotalRowCount,0) AS TotalRowCount
      FROM
      DemandForecast
      LEFT JOIN
      DemandForecastAnalysisPattern
      ON
      DemandForecastAnalysisPattern.numDFAPID  = DemandForecast.numDFAPID
      LEFT JOIN
      DemandForecastDays
      ON
      DemandForecastDays.numDFDaysID = DemandForecast.numDFDaysID
      LEFT JOIN LATERAL(SELECT
         string_agg(vcData,', ') AS list
         FROM
         Listdetails
         WHERE
         numListItemID IN(SELECT
            numItemClassificationID
            FROM
            DemandForecastItemClassification
            WHERE
            numDFID = DemandForecast.numDFID)) TableItemClassification ON TRUE
      LEFT JOIN LATERAL(SELECT
         string_agg(vcItemGroup,', ') AS list
         FROM
         ItemGroups
         WHERE
         numItemGroupID IN(SELECT
            numItemGroupID
            FROM
            DemandForecastItemGroup
            WHERE
            numDFID = DemandForecast.numDFID)) TableItemGroup  ON TRUE
      LEFT JOIN LATERAL(SELECT
         string_agg(vcWareHouse,', ') AS list
         FROM
         Warehouses
         WHERE
         numWareHouseID IN(SELECT
            numWareHouseID
            FROM
            DemandForecastWarehouse
            WHERE
            numDFID = DemandForecast.numDFID)) TableWarehouse  ON TRUE
      WHERE
      DemandForecast.numDomainID = v_numDomainID) AS TEMP
   WHERE
   NUMBER BETWEEN((v_numPageIndex::bigint -1)*v_numPageSize::bigint+1) AND(v_numPageIndex::bigint*v_numPageSize::bigint);
   RETURN;
END; $$;













