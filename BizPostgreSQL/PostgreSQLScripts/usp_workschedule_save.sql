-- Stored procedure definition script USP_WorkSchedule_Save for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_WorkSchedule_Save(v_numWorkScheduleID NUMERIC(18,0)
	,v_numDomainID NUMERIC(18,0)
	,v_numUserCntID NUMERIC(18,0)
	,v_vcWorkDays VARCHAR(20)
	,v_numWorkHours SMALLINT
	,v_numWorkMinutes SMALLINT
	,v_numProductiveHours SMALLINT
	,v_numProductiveMinutes SMALLINT
	,v_tmStartOfDay TIME)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   IF EXISTS(SELECT ID FROM WorkSchedule WHERE numDomainID = v_numDomainID AND ID = coalesce(v_numWorkScheduleID,0)) then
	
      UPDATE
      WorkSchedule
      SET
      numWorkHours = v_numWorkHours,numWorkMinutes = v_numWorkMinutes,numProductiveHours = v_numProductiveHours,
      numProductiveMinutes = v_numProductiveMinutes,
      tmStartOfDay = v_tmStartOfDay,vcWorkDays = v_vcWorkDays
      WHERE
      ID = v_numWorkScheduleID;
   ELSE
      INSERT INTO WorkSchedule(numDomainID
			,numUserCntID
			,numWorkHours
			,numWorkMinutes
			,numProductiveHours
			,numProductiveMinutes
			,tmStartOfDay
			,vcWorkDays)
		VALUES(v_numDomainID
			,v_numUserCntID
			,v_numWorkHours
			,v_numWorkMinutes
			,v_numProductiveHours
			,v_numProductiveMinutes
			,v_tmStartOfDay
			,v_vcWorkDays);
   end if;
   RETURN;
END; $$;


