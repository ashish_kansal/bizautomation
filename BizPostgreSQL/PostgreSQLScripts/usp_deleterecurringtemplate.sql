-- Stored procedure definition script USP_DeleteRecurringTemplate for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_DeleteRecurringTemplate(v_numRecurringId NUMERIC(9,0) DEFAULT 0,
v_numDomainId NUMERIC(9,0) DEFAULT 0)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   Delete From RecurringTemplate Where numRecurringId = v_numRecurringId And numDomainID = v_numDomainId;
   RETURN;
END; $$;


