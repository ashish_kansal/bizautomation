CREATE OR REPLACE FUNCTION USP_HowOftenPagesVisitedIP
(
	v_From VARCHAR(12),              
	v_To VARCHAR(12),              
	v_PageId VARCHAR(10),
	INOUT SWV_RefCur refcursor
)
LANGUAGE plpgsql
AS $$
	DECLARE
	v_strSql TEXT;
BEGIN
   v_strSql := 'select 
					distinct(vcUserHostAddress)
					,vcUserDomain 
				from 
					TrackingVisitorsHDR R 
				Join 
					TrackingVisitorsDTL T on T.numTracVisitorsHDRID = R.numTrackingID      
				where 
					T.numPageID = cast(NULLIF(''' || coalesce(v_PageId,'') || ''','''') as NUMERIC(18,0)) and dtCreated between ''' || coalesce(v_From,'') || ''' and ''' || coalesce(v_To,'') || '''';      
                            
   RAISE NOTICE '%',v_strSql;                
    
   OPEN SWV_RefCur FOR EXECUTE v_strSql;
   RETURN;
END; $$;


