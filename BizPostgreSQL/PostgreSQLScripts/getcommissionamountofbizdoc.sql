CREATE OR REPLACE FUNCTION GetCommissionAmountOfBizDoc(v_numDomainId NUMERIC,
      v_numOppBizDocID NUMERIC(9,0),
      v_numOppID NUMERIC(9,0), INOUT SWV_RefCur refcursor)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numShippingServiceItemID  NUMERIC(18,0);
   v_numDiscountServiceItemID  NUMERIC(18,0);
   v_tintCommissionType  SMALLINT;
   v_tintComAppliesTo  SMALLINT; 
   v_bitIncludeTaxAndShippingInCommission  BOOLEAN;
   v_numassignedto  NUMERIC;
   v_numrecowner  NUMERIC;
   v_numItemCode  NUMERIC;
   v_numUnitHour  DOUBLE PRECISION;
   v_monTotAmount  DECIMAL(20,5);
   v_numTotalUnitHour  DOUBLE PRECISION;
   v_monTotalTotAmount  DECIMAL(20,5);
   v_monCommission  DECIMAL(20,5);
   v_numComRuleID  NUMERIC;
   v_tintComBasedOn  SMALLINT;
   v_tinComDuration  SMALLINT;
   v_tintComType  SMALLINT;
   v_dFrom  TIMESTAMP;
   v_dTo  TIMESTAMP;
   v_decCommission  DECIMAL(18,0);
   v_numDivisionID  NUMERIC;
   v_bitCommContact  BOOLEAN;
   v_numItemClassification  NUMERIC;
   v_numUserCntID  NUMERIC;
   v_VendorCostAssignee  DECIMAL(20,5);
   v_TotalVendorCostAssignee  DECIMAL(20,5);
   v_numOppBizDocItemID  NUMERIC;
   v_numoppitemtCode  NUMERIC;
   v_UserName  VARCHAR(100);
   v_bitEmpContract  BOOLEAN;
   v_TempmonTotAmount  DECIMAL(20,5);
   v_TempVendorCostAssignee  DECIMAL(20,5);
   v_numCOmissionID  NUMERIC;
   v_vcItemName  VARCHAR(300);

			--Get AssignTo and Record Owner of Oppertunity
   Biz_Items CURSOR
   FOR SELECT  BDI.numItemCode,
                                SUM(coalesce(BDI.numUnitHour,0)),
                                SUM(coalesce(BDI.monTotAmount,0)),
                                SUM(coalesce(OT.monVendorCost,0)*coalesce(BDI.numUnitHour,0)),
                                MAX(BDI.numOppBizDocItemID),
                                MAX(OT.numoppitemtCode)
   FROM    OpportunityMaster OM
   JOIN OpportunityBizDocs BD ON BD.numoppid = OM.numOppId
   JOIN OpportunityBizDocItems BDI ON BD.numOppBizDocsId = BDI.numOppBizDocID
   INNER JOIN OpportunityItems OT ON OT.numoppitemtCode = BDI.numOppItemID
   WHERE   tintopptype = 1
   AND tintoppstatus = 1
   AND numDomainId = v_numDomainId
   AND bitAuthoritativeBizDocs = 1 /*AND isnull(BD.monAmountPaid,0) >=BD.monDealAmount*/ --bug id 982
   AND BD.numOppBizDocsId = v_numOppBizDocID /*AND isnull(BD.monAmountPaid,0)>0 */
   AND BD.monDealAmount > 0
   AND BDI.numItemCode NOT IN(v_numDiscountServiceItemID)  /*Do not include Discount Item*/
   AND 1 =(CASE WHEN BDI.numItemCode <> v_numShippingServiceItemID
   THEN 1
   ELSE 0
   END) /*Condition to include/exclude include Shipping Item */
   GROUP BY BDI.numItemCode
   UNION ALL
   SELECT  BDI.numItemCode,
                                SUM(coalesce(BDI.numUnitHour,0)),
                                SUM(coalesce(BDI.monTotAmount,0)),
                                SUM(coalesce(OT.monVendorCost,0)*coalesce(BDI.numUnitHour,0)),
                                MAX(BDI.numOppBizDocItemID),
                                MAX(OT.numoppitemtCode)
   FROM    OpportunityMaster OM
   JOIN OpportunityBizDocs BD ON BD.numoppid = OM.numOppId
   JOIN OpportunityBizDocItems BDI ON BD.numOppBizDocsId = BDI.numOppBizDocID
   INNER JOIN OpportunityItems OT ON OT.numoppitemtCode = BDI.numOppItemID
   WHERE   tintopptype = 1
   AND tintoppstatus = 1
   AND numDomainId = v_numDomainId
   AND bitAuthoritativeBizDocs = 1 /*AND isnull(BD.monAmountPaid,0) >=BD.monDealAmount*/ --bug id 982
   AND BD.numOppBizDocsId = v_numOppBizDocID /*AND isnull(BD.monAmountPaid,0)>0 */
   AND BD.monDealAmount > 0
   AND BDI.numItemCode NOT IN(v_numDiscountServiceItemID)  /*Do not include Discount Item*/
   AND 1 =(CASE WHEN v_bitIncludeTaxAndShippingInCommission = true
   THEN(CASE WHEN BDI.numItemCode = v_numShippingServiceItemID
      THEN 1
      ELSE 0
      END)
   ELSE 0
   END) /*Condition to include/exclude include Shipping Item */
                                        
   GROUP BY BDI.numItemCode;

   v_i  INTEGER;
   v_intFromNextTier  INTEGER;
						
   v_nexttier  VARCHAR(100);
BEGIN
   DROP TABLE IF EXISTS tt_TEMPUSERCOMISSION CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPUSERCOMISSION
   (
      numcontactid NUMERIC,
      numComission DECIMAL(20,5),
      numCOmissionID NUMERIC,
      vcContactName VARCHAR(250),
      numoppitemtCode NUMERIC,
      bitEmpContract BOOLEAN,
      vcItemName VARCHAR(300)
   );
    

/*Do not include Shipping Item & Discount Item*/
   v_numShippingServiceItemID := 0;
   v_numDiscountServiceItemID := 0;
        


--Get CommissionType (total amounts paid : 1 ,gross profit : 2 ,Project Gross Profit : 3)
--        SELECT  @numShippingServiceItemID = CASE WHEN ISNULL(bitIncludeTaxAndShippingInCommission,
--                                                             0) = 1
--                                                 THEN ISNULL(numShippingServiceItemID,
--                                                             0)
--                                                 WHEN ISNULL(bitIncludeTaxAndShippingInCommission,
--                                                             0) = 0 THEN 0
--                                                 ELSE ISNULL(numShippingServiceItemID,
--                                                             0)
--                                            END, -- Applying Domain Settings for Including Shipping Cost for Commission Calculations
   select   coalesce(numShippingServiceItemID,0), coalesce(numDiscountServiceItemID,0), coalesce(tintCommissionType,1), coalesce(tintComAppliesTo,3), coalesce(bitIncludeTaxAndShippingInCommission,true) INTO v_numShippingServiceItemID,v_numDiscountServiceItemID,v_tintCommissionType,
   v_tintComAppliesTo,v_bitIncludeTaxAndShippingInCommission FROM    Domain WHERE   numDomainId = v_numDomainId;

--If Commission set for Project Gross Profit then don't calculate for Order
   IF v_tintCommissionType = 3 then
            
      open SWV_RefCur for
      SELECT * FROM    tt_TEMPUSERCOMISSION;
                
      RETURN;
   end if;


--bug id 982: No need to check for biz docs AmountPaid and DealAmount must equal 
   IF EXISTS(SELECT * FROM    OpportunityMaster OM
   LEFT JOIN OpportunityBizDocs BD ON BD.numoppid = OM.numOppId
   WHERE   tintopptype = 1
   AND tintoppstatus = 1
   AND numDomainId = v_numDomainId
   AND bitAuthoritativeBizDocs = 1 /*AND isnull(BD.monAmountPaid,0) >=BD.monDealAmount*/--bug id 982
   AND BD.numOppBizDocsId = v_numOppBizDocID /*AND isnull(BD.monAmountPaid,0)>0*/
   AND BD.monDealAmount > 0) then
      v_numCOmissionID := 0;
      select   numassignedto, numrecowner INTO v_numassignedto,v_numrecowner FROM    OpportunityMaster OM
      LEFT JOIN OpportunityBizDocs BD ON BD.numoppid = OM.numOppId WHERE   tintopptype = 1
      AND tintoppstatus = 1
      AND numDomainId = v_numDomainId
      AND bitAuthoritativeBizDocs = 1
      AND BD.numOppBizDocsId = v_numOppBizDocID;
			

			--Cursor for each Items belogs to Oppertunity Biz Docs
      OPEN Biz_Items;
      FETCH NEXT FROM Biz_Items INTO v_numItemCode,v_numUnitHour,v_monTotAmount,v_VendorCostAssignee,v_numOppBizDocItemID,
      v_numoppitemtCode;
      WHILE FOUND LOOP
         v_i := 1;
					
					--Loop for AssignTo i=1 and RecordOwner i=2 (set tintAssignTo=i)
         WHILE v_i < 3 LOOP
            IF v_i = 1 then
               v_numUserCntID := v_numassignedto;
            ELSEIF v_i = 2
            then
               v_numUserCntID := v_numrecowner;
            end if;
						
					--Check For Commission Contact	
            IF EXISTS(SELECT  UM.numUserId
            FROM    UserMaster UM
            JOIN AdditionalContactsInformation A ON UM.numUserDetailId = A.numContactId
            WHERE   UM.numDomainID = v_numDomainId
            AND UM.numDomainID = A.numDomainID
            AND A.numContactId = v_numUserCntID) then
                                    
               v_bitCommContact := false;
               v_numDivisionID := 0;
            ELSE
               v_bitCommContact := true;
               select   A.numDivisionId INTO v_numDivisionID FROM    AdditionalContactsInformation A WHERE   A.numContactId = v_numUserCntID;
            end if;
            v_TempmonTotAmount := v_monTotAmount;
            v_TempVendorCostAssignee := v_VendorCostAssignee;
						
						--SELECT @numUserCntID,@i,@numItemCode
					--Individual Items tintComAppliesTo=1 and tintType=1
                                
						
					--All Items	 tintComAppliesTo=3
            IF EXISTS(SELECT  CR.numComRuleID
            FROM    CommissionRules CR
            JOIN CommissionRuleItems CRI ON CR.numComRuleID = CRI.numComRuleID
            JOIN CommissionRuleContacts CRC ON CR.numComRuleID = CRC.numComRuleId
            WHERE   CR.numDomainID = v_numDomainId
            AND CR.tintComAppliesTo = 1
            AND CR.tintAssignTo = v_i
            AND CRI.numValue = v_numItemCode
            AND CRI.tintType = 1
            AND CRC.numValue =(CASE v_bitCommContact
            WHEN false THEN v_numUserCntID
            WHEN true THEN v_numDivisionID
            END)
            AND CRC.bitCommContact = v_bitCommContact)
            AND v_tintComAppliesTo = 1 then
                                    
               select   CR.numComRuleID, CR.tintComBasedOn, CR.tinComDuration, CR.tintComType INTO v_numComRuleID,v_tintComBasedOn,v_tinComDuration,v_tintComType FROM    CommissionRules CR
               JOIN CommissionRuleItems CRI ON CR.numComRuleID = CRI.numComRuleID
               JOIN CommissionRuleContacts CRC ON CR.numComRuleID = CRC.numComRuleId WHERE   CR.numDomainID = v_numDomainId
               AND CR.tintComAppliesTo = 1
               AND CR.tintAssignTo = v_i
               AND CRI.numValue = v_numItemCode
               AND CRI.tintType = 1
               AND CRC.numValue =(CASE v_bitCommContact
               WHEN false THEN v_numUserCntID
               WHEN true THEN v_numDivisionID
               END)
               AND CRC.bitCommContact = v_bitCommContact    LIMIT 1;
						
					--Items Classification	tintComAppliesTo=2 and tintType=2
            ELSEIF EXISTS(SELECT  CR.numComRuleID
            FROM    CommissionRules CR
            JOIN CommissionRuleItems CRI ON CR.numComRuleID = CRI.numComRuleID
            JOIN CommissionRuleContacts CRC ON CR.numComRuleID = CRC.numComRuleId
            JOIN Item I ON CRI.numValue = I.numItemClassification
            WHERE   CR.numDomainID = v_numDomainId
            AND CR.tintComAppliesTo = 2
            AND CR.tintAssignTo = v_i
            AND I.numItemCode = v_numItemCode
            AND CRI.tintType = 2
            AND CRC.numValue =(CASE v_bitCommContact
            WHEN false THEN v_numUserCntID
            WHEN true THEN v_numDivisionID
            END)
            AND CRC.bitCommContact = v_bitCommContact)
            AND v_tintComAppliesTo = 2
            then
                                        
               select   CR.numComRuleID, CR.tintComBasedOn, CR.tinComDuration, CR.tintComType, CRI.numValue INTO v_numComRuleID,v_tintComBasedOn,v_tinComDuration,v_tintComType,v_numItemClassification FROM    CommissionRules CR
               JOIN CommissionRuleItems CRI ON CR.numComRuleID = CRI.numComRuleID
               JOIN CommissionRuleContacts CRC ON CR.numComRuleID = CRC.numComRuleId
               JOIN Item I ON CRI.numValue = I.numItemClassification WHERE   CR.numDomainID = v_numDomainId
               AND CR.tintComAppliesTo = 2
               AND CR.tintAssignTo = v_i
               AND I.numItemCode = v_numItemCode
               AND CRI.tintType = 2
               AND CRC.numValue =(CASE v_bitCommContact
               WHEN false THEN v_numUserCntID
               WHEN true THEN v_numDivisionID
               END)
               AND CRC.bitCommContact = v_bitCommContact    LIMIT 1;
            ELSEIF EXISTS(SELECT  CR.numComRuleID
            FROM    CommissionRules CR
            JOIN CommissionRuleContacts CRC ON CR.numComRuleID = CRC.numComRuleId
            WHERE   CR.tintComAppliesTo = 3
            AND CR.numDomainID = v_numDomainId
            AND CR.tintAssignTo = v_i
            AND CRC.numValue =(CASE v_bitCommContact
            WHEN false THEN v_numUserCntID
            WHEN true THEN v_numDivisionID
            END)
            AND CRC.bitCommContact = v_bitCommContact)
            AND v_tintComAppliesTo = 3
            then
                                            
               select   CR.numComRuleID, CR.tintComBasedOn, CR.tinComDuration, CR.tintComType INTO v_numComRuleID,v_tintComBasedOn,v_tinComDuration,v_tintComType FROM    CommissionRules CR
               JOIN CommissionRuleContacts CRC ON CR.numComRuleID = CRC.numComRuleId WHERE   CR.tintComAppliesTo = 3
               AND CR.numDomainID = v_numDomainId
               AND CR.tintAssignTo = v_i
               AND CRC.numValue =(CASE v_bitCommContact
               WHEN false THEN v_numUserCntID
               WHEN true THEN v_numDivisionID
               END)
               AND CRC.bitCommContact = v_bitCommContact    LIMIT 1;
            end if;
            IF v_numComRuleID > 0 then
                                    
						--Get From and To date for Commission Calculation
               IF v_tinComDuration = 1 then --Month
                  v_dFrom := DATE_TRUNC('month', now())::timestamp;
                  v_dTo := DATE_TRUNC('month', now())::timestamp + INTERVAL '1 month' - INTERVAL '1 day';
               ELSEIF v_tinComDuration = 2
               then --Quarter
                  v_dFrom := DATE_TRUNC('quarter', now())::timestamp;
                  v_dTo := DATE_TRUNC('quarter', now())::timestamp + INTERVAL '3 months' - INTERVAL '1 day';
               ELSEIF v_tinComDuration = 3
               then --Year
                  v_dFrom := DATE_TRUNC('year', now())::timestamp;
                  v_dTo := DATE_TRUNC('year', now())::timestamp + INTERVAL '12 months' - INTERVAL '1 day';
               end if; 
						
						
						--Total of Units and Amount for all oppertunity belogs to selected Item and selected AssignTo/RecordOwner
               select   coalesce(SUM(BDI.numUnitHour),0), coalesce(SUM(BDI.monTotAmount),0), coalesce(SUM(coalesce(OT.monVendorCost,0)*coalesce(BDI.numUnitHour,0)),
               0) INTO v_numTotalUnitHour,v_monTotalTotAmount,v_TotalVendorCostAssignee FROM    OpportunityMaster OM
               JOIN OpportunityBizDocs BD ON BD.numoppid = OM.numOppId
               JOIN OpportunityBizDocItems BDI ON BD.numOppBizDocsId = BDI.numOppBizDocID
               INNER JOIN OpportunityItems OT ON OT.numoppitemtCode = BDI.numOppItemID WHERE   tintopptype = 1
               AND tintoppstatus = 1
               AND numDomainId = v_numDomainId
               AND bitAuthoritativeBizDocs = 1 /*AND isnull(BD.monAmountPaid,0) >=BD.monDealAmount*/ --bug id 982
               AND BD.numOppBizDocsId <> v_numOppBizDocID /*AND isnull(BD.monAmountPaid,0)>0*/
               AND BD.monDealAmount > 0 
							--AND (numassignedto=@numUserCntID OR numrecowner=@numUserCntID) 
               AND 1 =(CASE v_i
               WHEN 1
               THEN CASE v_bitCommContact
                  WHEN false THEN CASE WHEN numassignedto = v_numUserCntID THEN 1
                     ELSE 0
                     END
                  WHEN true THEN CASE WHEN numassignedto IN(SELECT  numContactId
                        FROM    AdditionalContactsInformation AC
                        WHERE   AC.numDivisionId = v_numDivisionID) THEN 1
                     ELSE 0
                     END
                  END
               WHEN 2
               THEN CASE v_bitCommContact
                  WHEN false THEN CASE WHEN numrecowner = v_numUserCntID THEN 1
                     ELSE 0
                     END
                  WHEN true THEN CASE WHEN numrecowner IN(SELECT    numContactId
                        FROM      AdditionalContactsInformation AC
                        WHERE     AC.numDivisionId = v_numDivisionID) THEN 1
                     ELSE 0
                     END
                  END
               END)	 				 	 
							--BDI.numItemCode=@numItemCode
               AND 1 =(CASE v_tintComAppliesTo
               WHEN 1
               THEN CASE WHEN BDI.numItemCode = v_numItemCode THEN 1
                  ELSE 0
                  END
               WHEN 2
               THEN CASE WHEN BDI.numItemCode IN(SELECT  numItemCode
                     FROM    Item
                     WHERE   numItemClassification = v_numItemClassification
                     AND numDomainID = v_numDomainId) THEN 1
                  ELSE 0
                  END
               WHEN 3 THEN 1
               ELSE 0
               END)
               AND BD.numOppBizDocsId IN(SELECT  OBD.numOppBizDocsId
                  FROM    OpportunityBizDocsDetails BDD
                  INNER JOIN OpportunityBizDocs OBD ON BDD.numBizDocsId = OBD.numOppBizDocsId
                  INNER JOIN OpportunityMaster OM ON OM.numOppId = OBD.numoppid
                  WHERE   tintopptype = 1
                  AND tintoppstatus = 1
                  AND OM.numDomainId = v_numDomainId
                  AND OBD.bitAuthoritativeBizDocs = 1
                  AND BDD.dtCreationDate BETWEEN v_dFrom AND v_dTo
                  AND 1 =(CASE v_i
                  WHEN 1 THEN CASE v_bitCommContact
                     WHEN false THEN CASE WHEN OM.numassignedto = v_numUserCntID THEN 1
                        ELSE 0
                        END
                     WHEN true THEN CASE WHEN OM.numassignedto IN(SELECT    numContactId
                           FROM      AdditionalContactsInformation AC
                           WHERE     AC.numDivisionId = v_numDivisionID) THEN 1
                        ELSE 0
                        END
                     END
                  WHEN 2 THEN CASE v_bitCommContact
                     WHEN false THEN CASE WHEN OM.numrecowner = v_numUserCntID THEN 1
                        ELSE 0
                        END
                     WHEN true THEN CASE WHEN OM.numrecowner IN(SELECT  numContactId
                           FROM    AdditionalContactsInformation AC
                           WHERE   AC.numDivisionId = v_numDivisionID) THEN 1
                        ELSE 0
                        END
                     END
                  END)	 				 	 				 
														--(OM.numassignedto=@numUserCntID OR OM.numrecowner=@numUserCntID)
                  GROUP BY OBD.numOppBizDocsId /*HAVING SUM(BDD.monAmount)>=MAX(OBD.monDealAmount)*/ ); --bug id 982
			
			
						--If Commission based on gross profit	
               IF v_tintCommissionType = 2 then
                                            
                  v_TempmonTotAmount := v_TempmonTotAmount
                  -v_TempVendorCostAssignee;
                  v_monTotalTotAmount := v_monTotalTotAmount
                  -v_TotalVendorCostAssignee+v_TempmonTotAmount;
               ELSE
                  v_monTotalTotAmount := v_monTotalTotAmount+v_TempmonTotAmount;
               end if;
               v_numTotalUnitHour := v_numTotalUnitHour+v_numUnitHour;
               IF v_tintComBasedOn = 1 then --Amount Sold
                                            
                  select   decCommission, intTo+1 INTO v_decCommission,v_intFromNextTier FROM    CommissionRuleDtl WHERE   numComRuleID = v_numComRuleID
                  AND v_monTotalTotAmount BETWEEN intFrom:: DECIMAL(20,5) AND intTo:: DECIMAL(20,5)    LIMIT 1;
               ELSEIF v_tintComBasedOn = 2
               then --Units Sold
                                                
                  select   decCommission, intTo+1 INTO v_decCommission,v_intFromNextTier FROM    CommissionRuleDtl WHERE   numComRuleID = v_numComRuleID
                  AND v_numTotalUnitHour BETWEEN intFrom:: DOUBLE PRECISION AND intTo:: DOUBLE PRECISION    LIMIT 1;
               end if;
               select   SUBSTR(CAST(numComRuleDtlID AS VARCHAR(10)),1,10)
               || ','
               || SUBSTR(CAST(numComRuleID AS VARCHAR(10)),1,10)
               || ','
               || SUBSTR(CAST(intFrom AS VARCHAR(10)),1,10)
               || ','
               || SUBSTR(CAST(intTo AS VARCHAR(10)),1,10)
               || ','
               || SUBSTR(CAST(decCommission AS VARCHAR(10)),1,10) INTO v_nexttier FROM    CommissionRuleDtl WHERE   numComRuleID = v_numComRuleID
               AND intFrom >= v_intFromNextTier   ORDER BY intFrom LIMIT 1;
               IF v_tintComType = 1 then --Percentage
                  v_monCommission := v_TempmonTotAmount*v_decCommission/100;
               ELSEIF v_tintComType = 2
               then --Flat
                  v_monCommission := v_decCommission*v_numUnitHour;
               end if;
               IF v_monCommission > 0 then
                                            
                  select   coalesce(adc.vcFirstName || ' ' || adc.vcLastname,'-'), CASE WHEN um.numUserDetailId > 0 THEN 0
                  ELSE 1
                  END INTO v_UserName,v_bitEmpContract FROM    AdditionalContactsInformation adc
                  LEFT JOIN UserMaster um ON adc.numContactId = um.numUserDetailId
                  AND um.numDomainID = v_numDomainId
                  AND um.bitactivateflag = true WHERE   adc.numContactId = v_numUserCntID;
                  select   vcItemName INTO v_vcItemName FROM    Item WHERE   numItemCode = v_numItemCode
                  AND numDomainID = v_numDomainId;
                  IF NOT EXISTS(SELECT * FROM    BizDocComission
                  WHERE   numDomainId = v_numDomainId
                  AND numUserCntID = v_numUserCntID
                  AND numOppBizDocId = v_numOppBizDocID
                  AND numOppBizDocItemID = v_numOppBizDocItemID) then
                                                    
                     INSERT  INTO BizDocComission(numDomainId,
                                                                  numUserCntID,
                                                                  numOppBizDocId,
                                                                  numComissionAmount,
                                                                  numOppBizDocItemID,
                                                                  numComRuleID,
                                                                  tintComType,
                                                                  tintComBasedOn,
                                                                  decCommission,
                                                                  vcnexttier,
                                                                  numOppID)
                                                        VALUES(v_numDomainId,
                                                                  v_numUserCntID,
                                                                  v_numOppBizDocID,
                                                                  v_monCommission,
                                                                  v_numOppBizDocItemID,
                                                                  v_numComRuleID,
                                                                  v_tintComType,
                                                                  v_tintComBasedOn,
                                                                  v_decCommission,
                                                                  v_nexttier,
                                                                  v_numOppID);
                                                        
                     v_numCOmissionID := CURRVAL('BizDocComission_seq');
                  ELSE
                     select   numComissionID INTO v_numCOmissionID FROM    BizDocComission WHERE   numDomainId = v_numDomainId
                     AND numUserCntID = v_numUserCntID
                     AND numOppBizDocId = v_numOppBizDocID
                     AND numOppBizDocItemID = v_numOppBizDocItemID;
                  end if;
                  INSERT  INTO tt_TEMPUSERCOMISSION
                  SELECT  v_numUserCntID,
                                                                v_monCommission,
                                                                v_numCOmissionID,
                                                                CAST(v_UserName AS VARCHAR(250)),
                                                                v_numoppitemtCode,
                                                                v_bitEmpContract,
                                                                v_vcItemName;
               ELSE
                  DELETE  FROM BizDocComission
                  WHERE   numDomainId = v_numDomainId
                  AND numUserCntID = v_numUserCntID
                  AND numOppBizDocId = v_numOppBizDocID
                  AND numOppBizDocItemID = v_numOppBizDocItemID;
               end if;
            end if;
            v_numComRuleID := 0;
            v_tintComBasedOn := 0;
            v_tinComDuration := 0;
            v_tintComType := 0;
            v_monCommission := 0;
            v_bitCommContact := false;
            v_numDivisionID := 0;
            v_i := v_i::bigint+1;
         END LOOP;
         FETCH NEXT FROM Biz_Items INTO v_numItemCode,v_numUnitHour,v_monTotAmount,v_VendorCostAssignee,v_numOppBizDocItemID, 
         v_numoppitemtCode;
      END LOOP;
      CLOSE Biz_Items;
   end if;

   open SWV_RefCur for
   SELECT * FROM    tt_TEMPUSERCOMISSION;
END; $$;



