-- Stored procedure definition script USP_Item_GetDetailForStockTransfer for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_Item_GetDetailForStockTransfer(v_numDomainId NUMERIC(18,0),
    v_numItemCode NUMERIC(18,0),
	v_numFromWarehouseItemID NUMERIC(18,0),
	v_numToWarehouseItemID NUMERIC(18,0), INOUT SWV_RefCur refcursor)
LANGUAGE plpgsql
   AS $$
BEGIN
   IF coalesce(v_numFromWarehouseItemID,0) > 0 AND coalesce(v_numToWarehouseItemID,0) > 0 then
	
      open SWV_RefCur for
      SELECT
      Item.numItemCode AS numFromItemCode
			,coalesce(Item.bitSerialized,false) AS bitSerialized
			,coalesce(Item.bitLotNo,false) AS bitLotNo
			,CONCAT(vcItemName,', ',Warehouses.vcWareHouse,(CASE WHEN LENGTH(coalesce(WarehouseLocation.vcLocation,'')) > 0 THEN CONCAT(' (',WarehouseLocation.vcLocation,')') ELSE '' END)) AS vcFrom
			,WareHouseItems.numWareHouseItemID AS numFromWarehouseItemID
			,coalesce(WareHouseItems.numOnHand,0) AS numFromOnHand
			,coalesce(WareHouseItems.numOnHand,0)+coalesce(WareHouseItems.numAllocation,0) AS numFromAvailable
			,coalesce(WareHouseItems.numBackOrder,0) AS numFromBackOrder
			,WarehouseTo.numItemCode AS numToItemCode
			,WarehouseTo.vcTo
			,WarehouseTo.numWareHouseItemID AS numToWarehouseItemID
			,WarehouseTo.numToAvailable
			,WarehouseTo.numToBackOrder
			,WareHouseItems.numWareHouseID AS numFromWarehouseID
			,WarehouseTo.numWareHouseID AS numToWarehouseID
      FROM
      Item
      INNER JOIN
      WareHouseItems
      ON
      WareHouseItems.numItemID = Item.numItemCode
      AND WareHouseItems.numWareHouseItemID = v_numFromWarehouseItemID
      INNER JOIN
      Warehouses
      ON
      WareHouseItems.numWareHouseID = Warehouses.numWareHouseID
      LEFT JOIN
      WarehouseLocation
      ON
      WareHouseItems.numWLocationID = WarehouseLocation.numWLocationID
      CROSS JOIN LATERAL(SELECT
         numItemCode
				,CONCAT(vcItemName,', ',WTo.vcWareHouse,(CASE WHEN LENGTH(coalesce(WLTo.vcLocation,'')) > 0 THEN CONCAT(' (',WLTo.vcLocation,')') ELSE '' END)) AS vcTo
				,WHITo.numWareHouseID
				,WHITo.numWareHouseItemID
				,coalesce(WHITo.numOnHand,0)+coalesce(WHITo.numAllocation,0) AS numToAvailable
				,coalesce(WHITo.numBackOrder,0) AS numToBackOrder
         FROM
         WareHouseItems WHITo
         INNER JOIN
         Item ITo
         ON
         WHITo.numItemID = ITo.numItemCode
         INNER JOIN
         Warehouses WTo
         ON
         WHITo.numWareHouseID = WTo.numWareHouseID
         LEFT JOIN
         WarehouseLocation WLTo
         ON
         WHITo.numWLocationID = WLTo.numWLocationID
         WHERE
         WHITo.numWareHouseItemID = v_numToWarehouseItemID) WarehouseTo
      WHERE
      Item.numDomainID = v_numDomainId
      AND Item.numItemCode = v_numItemCode;
   ELSE
      open SWV_RefCur for
      SELECT
      coalesce(vcSKU,'') AS vcSKU
			,fn_GetItemAttributes(v_numDomainId,v_numItemCode,false) AS vcAttributes
			,coalesce(Item.bitSerialized,false) AS bitSerialized
			,coalesce(Item.bitLotNo,false) AS bitLotNo
      FROM
      Item
      WHERE
      numDomainID = v_numDomainId
      AND numItemCode = v_numItemCode;
   end if;
   RETURN;
END; $$;


