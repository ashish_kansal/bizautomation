-- Stored procedure definition script USP_GetItemsAttrWarehouse for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_GetItemsAttrWarehouse(v_numItemCode NUMERIC(9,0) DEFAULT 0,          
v_strAtrr VARCHAR(100) DEFAULT NULL,          
v_numWarehouseItemID NUMERIC(9,0) DEFAULT 0, INOUT SWV_RefCur refcursor default null, INOUT SWV_RefCur2 refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_bitSerialize  BOOLEAN;                      
   v_str  VARCHAR(2000);                    
   v_numItemGroupID  NUMERIC(9,0);
   v_strSQL  VARCHAR(1000);
   v_ID  NUMERIC(9,0);                        
   v_numCusFlDItemID  VARCHAR(20);                        
   v_fld_label  VARCHAR(100);
   v_Cnt  INTEGER;
   v_SplitOn  CHAR(1);
   SWV_RowCount INTEGER;
BEGIN
   v_str := '';                       
                        
                        
   select   numItemGroup, bitSerialized INTO v_numItemGroupID,v_bitSerialize from Item where numItemCode = v_numItemCode;                           
              
                        
                       
   open SWV_RefCur for
   select numWareHouseItemID,vcWarehouse,W.numWareHouseID,coalesce(numOnHand,0) as OnHand ,coalesce(numOnOrder,0) as OnOrder ,coalesce(numReorder,0) as Reorder ,coalesce(numAllocation,0) as Allocation ,coalesce(numBackOrder,0) as BackOrder,
 0 as Op_Flag from WareHouseItems
   join Warehouses W
   on W.numWareHouseID = WareHouseItems.numWareHouseID
   where numWarehouseItemID = v_numWarehouseItemID;          
          
   if v_bitSerialize = true then
      v_strSQL := '';
      if v_strAtrr != '' then
         v_Cnt := 1;
         v_SplitOn := ',';
         v_strSQL := 'SELECT recid            
     FROM CFW_Fld_Values_Serialized_Items where  bitSerialized =' || SUBSTR(CAST(v_bitSerialize AS VARCHAR(1)),1,1);
         While (POSITION(v_SplitOn IN v_strAtrr) > 0) LOOP
            if  v_Cnt = 1 then 
               v_strSQL := coalesce(v_strSQL,'') || ' and fld_value=''' ||(ltrim(rtrim(SUBSTR(v_strAtrr,1,POSITION(v_SplitOn IN v_strAtrr) -1)))) || '''';
            else 
               v_strSQL := coalesce(v_strSQL,'') || ' or fld_value=''' ||(ltrim(rtrim(SUBSTR(v_strAtrr,1,POSITION(v_SplitOn IN v_strAtrr) -1)))) || '''';
            end if;
            v_strAtrr := SUBSTR(v_strAtrr,POSITION(v_SplitOn IN v_strAtrr)+1,LENGTH(v_strAtrr));
            v_Cnt := v_Cnt::bigint+1;
         END LOOP;
         if  v_Cnt = 1 then 
            v_strSQL := coalesce(v_strSQL,'') || ' and fld_value=''' || coalesce(v_strAtrr,'') || ''' GROUP BY recid            
   HAVING count(*) > ' || CAST(v_Cnt::bigint -1 AS VARCHAR(10));
         else 
            v_strSQL := coalesce(v_strSQL,'') || ' or fld_value=''' || coalesce(v_strAtrr,'') || ''' GROUP BY recid            
   HAVING count(*) > ' || CAST(v_Cnt::bigint -1 AS VARCHAR(10));
         end if;
      end if;           
          
          
 --Create a Temporary table to hold data                                                            
      BEGIN
         CREATE TEMP SEQUENCE tt_tempTable_seq;
         EXCEPTION WHEN OTHERS THEN
            NULL;
      END;
      drop table IF EXISTS tt_TEMPTABLE CASCADE;
      Create TEMPORARY TABLE tt_TEMPTABLE 
      ( 
         ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1) PRIMARY KEY,
         numCusFlDItemID NUMERIC(9,0)
      );
      insert into tt_TEMPTABLE(numCusFlDItemID)
      select distinct(numOppAccAttrID) from ItemGroupsDTL where numItemGroupID = v_numItemGroupID and tintType = 2;
      v_ID := 0;
      select   ID, numCusFlDItemID, fld_label INTO v_ID,v_numCusFlDItemID,v_fld_label from tt_TEMPTABLE
      join CFW_Fld_Master on numCusFlDItemID = Fld_ID     LIMIT 1;
      while v_ID > 0 LOOP
         v_str := coalesce(v_str,'') || ',  dbo.GetCustFldItems(' || coalesce(v_numCusFlDItemID,'') || ',9,WDTL.numWareHouseItemID,0) as [' || coalesce(v_fld_label,'') || ']';
         select   ID, numCusFlDItemID, fld_label INTO v_ID,v_numCusFlDItemID,v_fld_label from tt_TEMPTABLE
         join CFW_Fld_Master on numCusFlDItemID = Fld_ID and ID > v_ID     LIMIT 1;
         GET DIAGNOSTICS SWV_RowCount = ROW_COUNT;
         if SWV_RowCount = 0 then 
            v_ID := 0;
         end if;
      END LOOP;
      v_str := 'select numWareHouseItmsDTLID,WDTL.numWareHouseItemID, vcSerialNo,0 as Op_Flag,WDTL.vcComments as Comments' || case when v_bitSerialize = true then v_str else '' end || ' from WareHouseItmsDTL WDTL                             
 join WareHouseItems WI                             
 on WDTL.numWareHouseItemID=WI.numWareHouseItemID                              
 where ISNULL(WDTL.numQty,0)>0 and WI.numWarehouseItemID=' || SUBSTR(CAST(v_numWarehouseItemID AS VARCHAR(15)),1,15);
      if v_strAtrr != '' then  
         v_str := coalesce(v_str,'') || ' and  numWareHouseItmsDTLID in (' || coalesce(v_strSQL,'') || ')';
      end if;
      RAISE NOTICE '%',v_str;
      EXECUTE v_str;
      open SWV_RefCur2 for
      select Fld_label,fld_id,fld_type,numlistid,vcURL from tt_TEMPTABLE
      join CFW_Fld_Master on numCusFlDItemID = Fld_ID;
 
   else
      open SWV_RefCur2 for
      select numWareHouseItmsDTLID,numWareHouseItemID, vcSerialNo,0 as Op_Flag,vcComments as Comments from    WareHouseItmsDTL where      numWareHouseItemID = v_numWarehouseItemID;
   end if;
   RETURN;
END; $$;


