-- Stored procedure definition script USP_DeleteRedirectConfig for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021

CREATE OR REPLACE FUNCTION USP_DeleteRedirectConfig(v_numDomainID NUMERIC(18,0),
	v_numSiteID NUMERIC(18,0),
	v_numRedirectConfigID NUMERIC(18,0) DEFAULT 0)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN


   DELETE FROM RedirectConfig
   WHERE numDomainID = v_numDomainID
   AND numSiteID = v_numSiteID
   AND numRedirectConfigID = v_numRedirectConfigID;
   RETURN;
END; $$;




