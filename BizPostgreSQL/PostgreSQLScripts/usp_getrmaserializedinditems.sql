-- Stored procedure definition script USP_GetRMASerializedindItems for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_GetRMASerializedindItems(v_numDomainId NUMERIC(9,0) DEFAULT 0,      
v_numReturnHeaderID NUMERIC(9,0) DEFAULT 0,  
v_numReturnItemID  NUMERIC(9,0) DEFAULT 0, INOUT SWV_RefCur refcursor default null, INOUT SWV_RefCur2 refcursor default null, INOUT SWV_RefCur3 refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_tintReturnType  SMALLINT;
   v_numOppId  NUMERIC(18,0);
   v_numWareHouseItemID  NUMERIC(9,0);
   v_numItemID  NUMERIC(9,0);
   v_numOppItemCode  NUMERIC(9,0);
   v_numUnitHour  DOUBLE PRECISION;
   v_bitSerialize  BOOLEAN;
   v_bitLotNo  BOOLEAN;                     

   v_str  VARCHAR(2000);
   v_strSQL  VARCHAR(2000);
   v_ColName  VARCHAR(50);  
   v_numItemGroupID  NUMERIC(9,0);                      
                      
   v_ID  NUMERIC(9,0);                      
   v_numCusFlDItemID  VARCHAR(20);                      
   v_fld_label  VARCHAR(100);
   v_fld_type  VARCHAR(100);
   SWV_RowCount INTEGER;
BEGIN
   select   tintReturnType, coalesce(numOppId,0) INTO v_tintReturnType,v_numOppId FROM ReturnHeader WHERE numDomainID = v_numDomainId AND numReturnHeaderID = v_numReturnHeaderID;
	
   v_str := '';        
      
   select   OI.numWarehouseItmsID, WI.numItemID, OI.numoppitemtCode, RI.numUnitHour INTO v_numWareHouseItemID,v_numItemID,v_numOppItemCode,v_numUnitHour from
   ReturnItems RI JOIN OpportunityItems OI ON RI.numOppItemID = OI.numoppitemtCode
   join WareHouseItems WI on OI.numWarehouseItmsID = WI.numWareHouseItemID where  RI.numReturnHeaderID = v_numReturnHeaderID AND RI.numReturnItemID = v_numReturnItemID
   AND OI.numOppId = v_numOppId;    
                      
   select   numItemGroup, coalesce(bitSerialized,false), coalesce(bitLotNo,false) INTO v_numItemGroupID,v_bitSerialize,v_bitLotNo from Item where numItemCode = v_numItemID;                      

   v_ColName := 'numWareHouseItemID,0';                  
            
--Create a Temporary table to hold data                                                          
   BEGIN
      CREATE TEMP SEQUENCE tt_tempTable_seq;
      EXCEPTION WHEN OTHERS THEN
         NULL;
   END;
   drop table IF EXISTS tt_TEMPTABLE CASCADE;
   Create TEMPORARY TABLE tt_TEMPTABLE 
   ( 
      ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1) PRIMARY KEY,
      numCusFlDItemID NUMERIC(9,0)
   );                       
                      
   insert into tt_TEMPTABLE(numCusFlDItemID)
   select numOppAccAttrID from ItemGroupsDTL where numItemGroupID = v_numItemGroupID and tintType = 2;     
              
   v_ID := 0;                      
   select   ID, numCusFlDItemID, fld_label, fld_type INTO v_ID,v_numCusFlDItemID,v_fld_label,v_fld_type from tt_TEMPTABLE
   join CFW_Fld_Master on numCusFlDItemID = Fld_ID     LIMIT 1;                                   
   while v_ID > 0 LOOP
      v_str := coalesce(v_str,'') || ',  dbo.GetCustFldItems(' || coalesce(v_numCusFlDItemID,'') || ',9,' || coalesce(v_ColName,'') || ') as [' || coalesce(v_fld_label,'') || ']';
      v_str := coalesce(v_str,'') || ',  dbo.GetCustFldItemsValue(' || coalesce(v_numCusFlDItemID,'') || ',9,' || coalesce(v_ColName,'') || ',''' || coalesce(v_fld_type,'') || ''') as [' || coalesce(v_fld_label,'') || 'Value]';
      select   ID, numCusFlDItemID, fld_label INTO v_ID,v_numCusFlDItemID,v_fld_label from tt_TEMPTABLE
      join CFW_Fld_Master on numCusFlDItemID = Fld_ID and ID > v_ID     LIMIT 1;
      GET DIAGNOSTICS SWV_RowCount = ROW_COUNT;
      if SWV_RowCount = 0 then 
         v_ID := 0;
      end if;
   END LOOP;        
      
   open SWV_RefCur for
   select numWareHouseItemID,vcWareHouse,
coalesce(u.vcUnitName,'') AS vcBaseUOMName
,v_bitSerialize AS bitSerialize,v_bitLotNo AS bitLotNo,I.vcItemName,v_numUnitHour AS numUnitHour
   from OpportunityItems Opp
   join WareHouseItems on Opp.numWarehouseItmsID = WareHouseItems.numWareHouseItemID
   join Warehouses on Warehouses.numWareHouseID = WareHouseItems.numWareHouseID
   join Item I on Opp.numItemCode = I.numItemCode
   LEFT JOIN  UOM u ON u.numUOMId = I.numBaseUnit
   where Opp.numoppitemtCode = v_numOppItemCode;    
      
      
      
   v_strSQL := CONCAT('select WID.numWareHouseItmsDTLID,numWareHouseItemID,vcSerialNo,0 as Op_Flag,
  COALESCE(OSI.numQty,0) 
  as TotalQty,0 as UsedQty
  ',v_str,',false as bitAdded
 from OppWarehouseSerializedItem OSI join WareHouseItmsDTL WID     
 on WID.numWareHouseItmsDTLID= OSI.numWarehouseItmsDTLID                           
 where numOppID=',
   v_numOppId,' and numWareHouseItemID=',
   coalesce(v_numWareHouseItemID,0),' AND OSI.numWarehouseItmsDTLID NOT IN (SELECT OISInner.numWarehouseItmsDTLID FROM OppWarehouseSerializedItem OISInner INNER JOIN ReturnHeader RH ON OISInner.numReturnHeaderID=RH.numReturnHeaderID WHERE RH.numOppId=',v_numOppId,')');
   RAISE NOTICE '%',v_strSQL;                     
   OPEN SWV_RefCur2 FOR EXECUTE v_strSQL;     
  
  
   open SWV_RefCur3 for
   select Fld_label,fld_id,fld_type,numlistid,vcURL from tt_TEMPTABLE
   join CFW_Fld_Master on numCusFlDItemID = Fld_ID;                    
   RETURN;
END; $$;


