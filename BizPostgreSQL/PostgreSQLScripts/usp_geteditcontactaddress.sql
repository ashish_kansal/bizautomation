-- Stored procedure definition script usp_GetEditContactAddress for PostgreSQL
CREATE OR REPLACE FUNCTION usp_GetEditContactAddress(v_numContactID NUMERIC DEFAULT 0                                
--                              
,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT cast(vcPStreet as VARCHAR(255)) ,cast(vcPCity as VARCHAR(255)), cast(vcPState as VARCHAR(255)),cast(vcPPostalCode as VARCHAR(255)),cast(vcPCountry as VARCHAR(255)),
cast(vcContactLocation as VARCHAR(255)),cast(vcStreet as VARCHAR(255)),cast(vcCity as VARCHAR(255)),cast(intPostalCode as VARCHAR(255)),cast(vcState as VARCHAR(255)),cast(vcCountry as VARCHAR(255)),
cast(vcContactLocation1 as VARCHAR(255)),cast(vcStreet1 as VARCHAR(255)),cast(vcCity1 as VARCHAR(255)),cast(vcPostalCode1 as VARCHAR(255)),cast(vcState1 as VARCHAR(255)),cast(vcCountry1 as VARCHAR(255)),
cast(vcContactLocation2 as VARCHAR(255)),cast(vcStreet2 as VARCHAR(255)),cast(vcCity2 as VARCHAR(255)),cast(vcPostalCode2 as VARCHAR(255)),cast(vcState2 as VARCHAR(255)),cast(vcCountry2 as VARCHAR(255)),
cast(vcContactLocation3 as VARCHAR(255)),cast(vcStreet3 as VARCHAR(255)),cast(vcCity3 as VARCHAR(255)),cast(vcPostalCode3 as VARCHAR(255)),cast(vcState3 as VARCHAR(255)),cast(vcCountry3 as VARCHAR(255)),
cast(vcContactLocation4 as VARCHAR(255)),cast(vcStreet4 as VARCHAR(255)),cast(vcCity4 as VARCHAR(255)),cast(vcPostalCode4 as VARCHAR(255)),cast(vcState4 as VARCHAR(255)),cast(vcCountry4 as VARCHAR(255))
   FROM AdditionalContactsInformation A left join  contactAddress  C
   on C.numContactID = A.numContactId
   WHERE A.numContactId = v_numContactID;
   RETURN;
END; $$;
/****** Object:  StoredProcedure [dbo].[USP_GetEFormConfiguration]    Script Date: 07/26/2008 16:17:26 ******/













