-- Stored procedure definition script USP_UpdatingInventoryForEmbeddedKits for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_UpdatingInventoryForEmbeddedKits(v_numOppItemID NUMERIC(18,0),
	v_Units DOUBLE PRECISION,
	v_Mode SMALLINT,
	v_numOppID NUMERIC(9,0),
	v_tintMode SMALLINT DEFAULT 0, -- 0:Add/Edit,1:Delete,2: DEMOTED TO OPPORTUNITY
	v_numUserCntID NUMERIC(9,0) DEFAULT NULL)
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numDomain  NUMERIC(18,0);
   v_tintOppType  SMALLINT DEFAULT 0;
   v_minRowNumber  INTEGER;
   v_maxRowNumber  INTEGER;

   v_numOppChildItemID  NUMERIC(18,0);
   v_numItemCode  NUMERIC(18,0);
   v_bitAssembly  BOOLEAN;
   v_numWareHouseItemID  NUMERIC(18,0);
	
   v_onHand  DOUBLE PRECISION;
   v_onAllocation  DOUBLE PRECISION;
   v_onOrder  DOUBLE PRECISION;
   v_onBackOrder  DOUBLE PRECISION;
   v_onReOrder  DOUBLE PRECISION;
   v_numUnits  DOUBLE PRECISION;
   v_QtyShipped  DOUBLE PRECISION;
	

   v_description  VARCHAR(300);
   SWV_RCur REFCURSOR;
   SWV_RCur2 REFCURSOR;
BEGIN
   select   numDomainId, tintopptype INTO v_numDomain,v_tintOppType FROM
   OpportunityMaster WHERE
   numOppId = v_numOppID;

   DROP TABLE IF EXISTS tt_TEMPKITS CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPKITS ON COMMIT DROP AS
      SELECT
      OKI.numOppChildItemID,
		I.numItemCode,
		I.bitAssembly,
		numWareHouseItemId,
		numQtyItemsReq,
		numQtyShipped,
		ROW_NUMBER() OVER(ORDER BY numOppChildItemID) AS RowNumber
	
      FROM
      OpportunityKitItems OKI
      JOIN
      Item I
      ON
      OKI.numChildItemID = I.numItemCode
      WHERE
      charitemtype = 'P'
      AND numWareHouseItemId > 0
      AND numWareHouseItemId IS NOT NULL
      AND OKI.numOppId = v_numOppID
      AND OKI.numOppItemID = v_numOppItemID; 
  

   select   MIN(RowNumber), MAX(RowNumber) INTO v_minRowNumber,v_maxRowNumber FROM
   tt_TEMPKITS;


   WHILE  v_minRowNumber <= v_maxRowNumber LOOP
      select   numOppChildItemID, numItemCode, coalesce(bitAssembly,false), numWareHouseItemID, numQtyItemsReq, numQtyShipped INTO v_numOppChildItemID,v_numItemCode,v_bitAssembly,v_numWareHouseItemID,v_numUnits,
      v_QtyShipped FROM
      tt_TEMPKITS WHERE
      RowNumber = v_minRowNumber;
      IF v_Mode = 0 then
         v_description := CONCAT('SO KIT insert/edit (Qty:',v_numUnits,' Shipped:',v_QtyShipped,')');
      ELSEIF v_Mode = 1
      then
         IF v_tintMode = 2 then
			
            v_description := CONCAT('SO DEMOTED TO OPPORTUNITY KIT (Qty:',v_numUnits,' Shipped:',v_QtyShipped,
            ')');
         ELSE
            v_description := CONCAT('SO KIT Deleted (Qty:',v_numUnits,' Shipped:',v_QtyShipped,')');
         end if;
      ELSEIF v_Mode = 2
      then
         v_description := CONCAT('SO KIT Close (Qty:',v_numUnits,' Shipped:',v_QtyShipped,')');
      ELSEIF v_Mode = 3
      then
         v_description := CONCAT('SO KIT Re-Open (Qty:',v_numUnits,' Shipped:',v_QtyShipped,')');
      end if;

		-- IF KIT ITEM CONTAINS ANOTHER KIT AS CHILD THEN WE HAVE TO UPDATE INVENTORY OF ITEMS OF KIT CHILD ITEMS
      IF EXISTS(SELECT numOppKitChildItemID FROM OpportunityKitChildItems WHERE numOppId = v_numOppID AND numOppItemID = v_numOppItemID AND numOppChildItemID = v_numOppChildItemID) then
		
			 --  numeric(9, 0)
				 --  numeric(9, 0)
				 --  tinyint
				 --  varchar(100)
         PERFORM USP_ManageWareHouseItems_Tracking(v_numWareHouseItemID := v_numWareHouseItemID,v_numReferenceID := v_numOppID,
         v_tintRefType := 3::SMALLINT,v_vcDescription := v_description,v_numModifiedBy := v_numUserCntID,
         v_numDomainID := v_numDomain,SWV_RefCur := null);

         PERFORM USP_WareHouseItems_ManageInventoryKitWithinKit(v_numOppID := v_numOppID,v_numOppItemID := v_numOppItemID,v_numOppChildItemID := v_numOppChildItemID,
         v_Mode := v_Mode::SMALLINT,v_tintMode := v_tintMode::SMALLINT,
         v_numDomainID := v_numDomain,v_numUserCntID := v_numUserCntID,
         v_tintOppType := v_tintOppType::SMALLINT);
      ELSE
         select   coalesce(numOnHand,0), coalesce(numAllocation,0), coalesce(numonOrder,0), coalesce(numBackOrder,0), coalesce(numReorder,0) INTO v_onHand,v_onAllocation,v_onOrder,v_onBackOrder,v_onReOrder FROM
         WareHouseItems WHERE
         numWareHouseItemID = v_numWareHouseItemID;
         IF v_Mode = 0 then --insert/edit
    		
            v_numUnits := v_numUnits -v_QtyShipped;
            IF v_onHand >= v_numUnits then
				
               v_onHand := v_onHand -v_numUnits;
               v_onAllocation := v_onAllocation+v_numUnits;
            ELSEIF v_onHand < v_numUnits
            then
				
               v_onAllocation := v_onAllocation+v_onHand;
               v_onBackOrder := v_onBackOrder+v_numUnits -v_onHand;
               v_onHand := 0;
            end if;
         ELSEIF v_Mode = 1
         then --Revert
			
            IF v_QtyShipped > 0 then
				
               IF v_tintMode = 0 then
                  v_numUnits := v_numUnits -v_QtyShipped;
               ELSEIF v_tintMode = 1 OR v_tintMode = 2
               then
                  v_onAllocation := v_onAllocation+v_QtyShipped;
               end if;
            end if;
            IF v_numUnits >= v_onBackOrder then
				
               v_numUnits := v_numUnits -v_onBackOrder;
               v_onBackOrder := 0;
               IF (v_onAllocation -v_numUnits >= 0) then
                  v_onAllocation := v_onAllocation -v_numUnits;
               end if;
               v_onHand := v_onHand+v_numUnits;
            ELSEIF v_numUnits < v_onBackOrder
            then
				
               IF (v_onBackOrder -v_numUnits > 0) then
                  v_onBackOrder := v_onBackOrder -v_numUnits;
               end if;
            end if;
         ELSEIF v_Mode = 2
         then --Close
			
            v_numUnits := v_numUnits -v_QtyShipped;
            IF v_onAllocation >= v_numUnits then
				
               v_onAllocation := v_onAllocation -v_numUnits;
            ELSE
               RAISE EXCEPTION 'NOTSUFFICIENTQTY_ALLOCATION';
            end if;
         ELSEIF v_Mode = 3
         then --Re-Open
			
            v_numUnits := v_numUnits -v_QtyShipped;
            v_onAllocation := v_onAllocation+v_numUnits;
         end if;
			 --  numeric(9, 0)
				 --  numeric(9, 0)
				 --  tinyint
				 --  varchar(100)
         UPDATE
         WareHouseItems
         SET
         numOnHand = v_onHand,numAllocation = v_onAllocation,numBackOrder = v_onBackOrder,
         dtModified = LOCALTIMESTAMP
         WHERE
         numWareHouseItemID = v_numWareHouseItemID;
         PERFORM USP_ManageWareHouseItems_Tracking(v_numWareHouseItemID := v_numWareHouseItemID,v_numReferenceID := v_numOppID,
         v_tintRefType := 3::SMALLINT,v_vcDescription := v_description,v_numModifiedBy := v_numUserCntID,
         v_numDomainID := v_numDomain,SWV_RefCur := null);
      end if;
      v_minRowNumber := v_minRowNumber::bigint+1;
   END LOOP;	
  
   DROP TABLE IF EXISTS tt_TEMPKITS CASCADE;
   RETURN;
END; $$;


