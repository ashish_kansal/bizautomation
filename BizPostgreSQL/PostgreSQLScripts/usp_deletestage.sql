-- Stored procedure definition script usp_DeleteStage for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION usp_DeleteStage(v_numDomainId NUMERIC(9,0),
v_strStageDetailsIds VARCHAR(500))
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
	IF COALESCE(v_strStageDetailsIds,'') <> '' THEN
		delete from StagePercentageDetails where
		numStageDetailsId in(select Id from SplitIDs(v_strStageDetailsIds,','))
		and numdomainid = v_numDomainId;
	END IF;

   RETURN;
END; $$;


