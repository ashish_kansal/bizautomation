-- Stored procedure definition script USP_WorkFlowMaster_History for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
  
 
-- =============================================      
-- Author:  <Author,,Sachin Sadhu>      
-- Create date: <Create Date,,3rdApril2014>      
-- Description: <Description,,To maintain Work rule execution history>      
-- =============================================      
Create or replace FUNCTION USP_WorkFlowMaster_History(v_numDomainID NUMERIC(18,0),      
v_numFormID INTEGER  ,    
v_CurrentPage INTEGER,                                                              
v_PageSize INTEGER,                                                              
INOUT v_TotRecs INTEGER ,           
v_SortChar CHAR(1) DEFAULT '0' ,                                                             
v_columnName VARCHAR(50) DEFAULT NULL,                                                              
v_columnSortOrder VARCHAR(50) DEFAULT NULL  ,      
v_SearchStr  VARCHAR(50) DEFAULT NULL, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql       
 -- Add the parameters for the stored procedure here      
      
 -- SET NOCOUNT ON added to prevent extra result sets from      
 -- interfering with SELECT statements.      
   AS $$
   DECLARE
   v_strSql  VARCHAR(8000);                                                        
          
   v_firstRec  INTEGER;                                                              
   v_lastRec  INTEGER;
BEGIN
      
      
    -- Insert statements for procedure here      
   BEGIN
      CREATE TEMP SEQUENCE tt_tempTable_seq;
      EXCEPTION WHEN OTHERS THEN
         NULL;
   END;
   DROP TABLE IF EXISTS tt_TEMPTABLE CASCADE;
   Create TEMPORARY TABLE tt_TEMPTABLE 
   ( 
      ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1) PRIMARY KEY,
      numWFID NUMERIC(18,0),
      numFormId NUMERIC(18,0),
      vcWFName VARCHAR(50),
      vcPOppName VARCHAR(500),
      vcWFDescription TEXT,
      numRecordId NUMERIC(18,0),
      tintProcessStatus INTEGER,
      bitSuccess BOOLEAN,
      vcDescription TEXT,
      vcFormName VARCHAR(50),
      dtExecutionDate TIMESTAMP,
      numOppID NUMERIC(18,0),
      Status VARCHAR(50),
      TriggeredOn VARCHAR(50),
      vcusername VARCHAR(200),
      dtCreatedDate     TIMESTAMP
   );           
   v_strSql := '  SELECT       
      
   m.numWFID,      
   m.numFormID,    
   m.vcWFName,      
   coalesce(o.vcPOppName,'''') as vcPOppName,      
   m.vcWFDescription,      
   q.numRecordID,      
   q.tintProcessStatus,      
   e.bitSuccess,      
  coalesce(e.vcDescription,''Waiting'') as  vcDescription,      
   DFM.vcFormName,      
   e.dtExecutionDate,    
   o.numOppId,     
   CASE WHEN q.tintProcessStatus = 1 THEN ''Pending Execution'' WHEN q.tintProcessStatus = 2 THEN ''In Progress''  WHEN q.tintProcessStatus = 3 THEN ''Success'' WHEN q.tintProcessStatus = 4 then ''Failed'' WHEN q.tintProcessStatus = 6 then ''Condition Not Matched'' else ''No WF Found'' END AS STATUS,      
   CASE WHEN (m.tintWFTriggerOn = 1 AND m.intDays = 0) THEN ''Create'' WHEN m.tintWFTriggerOn = 2 THEN ''Edit'' WHEN m.tintWFTriggerOn = 3 THEN ''Create or Edit'' WHEN m.tintWFTriggerOn = 4 THEN ''Fields Update'' WHEN m.tintWFTriggerOn = 5 THEN ''Delete'' WHEN m.intDays > 0 THEN     
''Date Field'' ELSE ''NA'' end AS TriggeredOn,  
 fn_GetContactName(coalesce(m.numCreatedBy,0)) AS vcUserName   ,
 q.dtCreatedDate   
  FROM WorkFlowMaster as m       
  INNER JOIN WorkFlowQueue AS q ON m.numWFID = q.numWFID       
  LEFT JOIN WorkFlowQueueExecution AS e ON e.numWFQueueID = q.numWFQueueID      
  LEFT JOIN OpportunityMaster o ON q.numRecordID = o.numOppId  AND o.numDomainId = m.numDomainID
  JOIN DynamicFormMaster DFM ON m.numFormID = DFM.numFormId AND DFM.tintFlag = 3 AND q.tintProcessStatus NOT IN(6,5)  WHERE m.numDomainID =' || SUBSTR(CAST(v_numDomainID AS VARCHAR(15)),1,15);       
          
   if v_SortChar <> '0' then 
      v_strSql := coalesce(v_strSql,'') || ' And m.vcWFName ilike ''' || coalesce(v_SortChar,'') || '%''';
   end if;           
      
   if v_numFormID <> 0 then 
      v_strSql := coalesce(v_strSql,'') || ' And m.numFormID = ' || SUBSTR(CAST(v_numFormID AS VARCHAR(15)),1,15) || '';
   end if;         
      
   if v_SearchStr <> '' then 
      v_strSql := coalesce(v_strSql,'') || ' And (m.vcWFName ilike ''%' || coalesce(v_SearchStr,'') || '%'' or       
m.vcWFDescription ilike ''%' || coalesce(v_SearchStr,'') || '%'') ';
   end if;       
          
   v_strSql := coalesce(v_strSql,'') || 'ORDER BY ' || coalesce(v_columnName,'') || ' ' || coalesce(v_columnSortOrder,'');          
     
   EXECUTE 'insert into tt_TEMPTABLE( numWFID ,    
            numFormID ,    
            vcWFName ,    
            vcPOppName ,    
            vcWFDescription ,    
            numRecordID ,    
            tintProcessStatus ,    
            bitSuccess ,    
            vcDescription ,    
            vcFormName ,    
            dtExecutionDate ,    
            numOppId ,    
            STATUS ,    
            TriggeredOn,vcUserName,dtCreatedDate) ' || v_strSql;          
          
   v_firstRec :=(v_CurrentPage::bigint -1)*v_PageSize::bigint;                                                              
   v_lastRec :=(v_CurrentPage::bigint*v_PageSize::bigint+1);                                                               
      
   select   COUNT(*) INTO v_TotRecs FROM tt_TEMPTABLE;       
       
   open SWV_RefCur for
   SELECT
   ID AS RowNo,* From tt_TEMPTABLE T
   WHERE ID > v_firstRec and ID < v_lastRec  order by ID;      
       
       
--   SELECT       
--   ROW_NUMBER() OVER(ORDER BY m.numWFID ASC) AS RowNo,    
--   m.numWFID,      
--   m.vcWFName,      
--   o.vcPOppName,      
--   m.vcWFDescription,      
--   q.numRecordID,      
--   q.tintProcessStatus,      
--   e.bitSuccess,      
--   e.vcDescription ,      
--   DFM.vcFormName,      
--   e.dtExecutionDate,    
--   o.numOppId,      
--   CASE WHEN q.tintProcessStatus=1 THEN 'Pending Execution' WHEN q.tintProcessStatus= 2 THEN 'In Progress' WHEN q.tintProcessStatus=3 THEN 'Success' WHEN q.tintProcessStatus=4 then 'Failed' else 'No WF Found' END AS STATUS,      
--   CASE WHEN (m.tintWFTriggerOn=1 AND m.intDays=0)THEN 'Create' WHEN m.tintWFTriggerOn=2 THEN 'Edit' WHEN m.tintWFTriggerOn=3 THEN 'Create or Edit' WHEN m.tintWFTriggerOn=4 THEN 'Fields Update' WHEN m.tintWFTriggerOn=5 THEN 'Delete' WHEN m.intDays>0 TH
  
--E--N     
--'Date Field' ELSE 'NA' end AS TriggeredOn      
--  FROM dbo.WorkFlowMaster as m       
--  INNER JOIN dbo.WorkFlowQueue AS q ON m.numWFID=q.numWFID       
--  LEFT JOIN dbo.WorkFlowQueueExecution AS e ON e.numWFQueueID=q.numWFQueueID      
--  INNER JOIN dbo.OpportunityMaster o ON q.numRecordID =o.numOppId      
--  JOIN DynamicFormMaster DFM ON m.numFormID=DFM.numFormID AND DFM.tintFlag=3  join #tempTable T on T.numWFID=m.numWFID      
--    
--   WHERE ID > @firstRec and ID < @lastRec AND m.numDomainID=@numDomainID order by ID      
         
   RETURN;
END; $$; 



