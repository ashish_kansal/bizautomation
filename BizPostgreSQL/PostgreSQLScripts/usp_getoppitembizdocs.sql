DROP FUNCTION IF EXISTS USP_GetOppItemBizDocs;

CREATE OR REPLACE FUNCTION USP_GetOppItemBizDocs(v_numBizDocID NUMERIC(9,0),
    v_numDomainID NUMERIC(9,0),
    v_numOppID NUMERIC(9,0),
    v_numOppBizDocID NUMERIC(9,0),
    v_bitRentalBizDoc BOOLEAN DEFAULT false, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_tintCommitAllocation  SMALLINT;
   v_bitAllocateInventoryOnPickList  BOOLEAN;
   v_numSalesAuthoritativeBizDocID  INTEGER;
BEGIN
   select   coalesce(tintCommitAllocation,1) INTO v_tintCommitAllocation FROM Domain WHERE numDomainId = v_numDomainID;
   select   coalesce(bitAllocateInventoryOnPickList,false) INTO v_bitAllocateInventoryOnPickList FROM OpportunityMaster INNER JOIN DivisionMaster ON OpportunityMaster.numDivisionId = DivisionMaster.numDivisionID WHERE numOppId = v_numOppID;

   select   numAuthoritativeSales INTO v_numSalesAuthoritativeBizDocID FROM AuthoritativeBizDocs WHERE numDomainId = v_numDomainID;


   IF v_numOppBizDocID = 0 then
      IF v_bitRentalBizDoc = true then
        
         open SWV_RefCur for
         SELECT * FROM(SELECT    OI.numoppitemtCode,
                                OI.numItemCode,
                                OI.vcItemName,
                                OI.vcModelID,
                                OI.numUnitHour AS QtyOrdered,
                                (OI.numUnitHour -coalesce(SUM(OBI.numUnitHour),0)) AS QtytoFulFilled,
                                (OI.numUnitHour -coalesce(SUM(OBI.numUnitHour),0)) AS QtytoFulFill,
								(CASE WHEN coalesce(OI.numUnitHour,0) > 0 THEN((OI.numUnitHour -coalesce(SUM(OBI.numUnitHour),0))/OI.numUnitHour)*100 ELSE 0 END) AS numRemainingPercent,
								(CASE WHEN coalesce(OI.numUnitHour,0) > 0 THEN((coalesce(SUM(OBI.numUnitHour),0)*100)/OI.numUnitHour) ELSE 0 END) AS numIncludedPercent,
                                coalesce(numAllocation,0) AS numAllocation,
                                coalesce(numBackOrder,0) AS numBackOrder,
                                CASE WHEN OBI.vcNotes IS NULL THEN OI.vcNotes ELSE OBI.vcNotes END AS vcNotes,
                                '' AS vcTrackingNo,
                                '' AS vcShippingMethod,
                                0 AS monShipCost,
                                LOCALTIMESTAMP  AS dtDeliveryDate,
                                CAST(1 AS BOOLEAN) AS Included,
                                coalesce(OI.numQtyShipped,0) AS numQtyShipped,
                                coalesce(OI.numUnitHourReceived,0) AS numQtyReceived,
                                I.charItemType,
                                SUBSTR((SELECT  ',' || vcSerialNo
               || CASE WHEN coalesce(I.bitLotNo,false) = true THEN ' (' || CAST(oppI.numQty AS VARCHAR(15)) || ')'
               ELSE ''
               END
               FROM    OppWarehouseSerializedItem oppI
               JOIN WareHouseItmsDTL whi ON oppI.numWarehouseItmsDTLID = whi.numWareHouseItmsDTLID
               WHERE   oppI.numOppID = v_numOppID
               AND oppI.numOppItemID = OI.numoppitemtCode
               AND oppI.numOppBizDocsId = v_numOppBizDocID
               ORDER BY vcSerialNo),2,200000) AS SerialLotNo,
                                I.bitSerialized,
                                coalesce(I.bitLotNo,false) AS bitLotNo,
                                OI.numWarehouseItmsID,
                                coalesce(I.numItemClassification,0) AS numItemClassification,
                                I.vcSKU,
                                fn_GetAttributes(OI.numWarehouseItmsID,I.bitSerialized) AS Attributes,
                                OM.bintCreatedDate AS dtRentalStartDate,
                                LOCALTIMESTAMP  AS dtRentalReturnDate,
                                coalesce(OI.numUOMId,0) AS numUOM,
                                OI.monPrice AS monOppItemPrice,
                                coalesce(OBI.monPrice,OI.monPrice) AS monOppBizDocPrice,
								OI.monTotAmount AS monOppBizDocAmount,
                                coalesce(OI.vcItemDesc,'') AS vcItemDesc,
								coalesce(OI.bitDropShip,false) AS bitDropShip,
								coalesce(OI.ItemReleaseDate,OM.dtReleaseDate) AS dtReleaseDate
            FROM      OpportunityItems OI
            LEFT JOIN OpportunityMaster OM ON OI.numOppId = OM.numOppId
            LEFT JOIN OpportunityBizDocs OB ON OB.numoppid = OI.numOppId
            AND OB.numBizDocId = v_numBizDocID
            LEFT JOIN OpportunityBizDocItems OBI ON OB.numOppBizDocsId = OBI.numOppBizDocID
            AND OBI.numOppItemID = OI.numoppitemtCode
            LEFT JOIN WareHouseItems WI ON WI.numWareHouseItemID = OI.numWarehouseItmsID
            INNER JOIN Item I ON I.numItemCode = OI.numItemCode
            WHERE     OI.numOppId = v_numOppID
            AND coalesce(OI.bitMappingRequired,false) = false
            AND OI.numoppitemtCode NOT IN(SELECT  OBDI.numOppItemID
               FROM    OpportunityBizDocs OBD
               JOIN OpportunityBizDocItems OBDI ON OBD.numOppBizDocsId = OBDI.numOppBizDocID
               WHERE   OBD.numoppid = v_numOppID)
            GROUP BY  OI.numoppitemtCode,OI.numItemCode,OI.vcItemName,OI.vcModelID,OI.vcNotes,
            OBI.vcNotes,OI.numUnitHour,numAllocation,numBackOrder,OI.numQtyShipped,
            OI.numUnitHourReceived,I.charItemType,I.bitSerialized,I.bitLotNo,OI.numWarehouseItmsID,
            I.numItemClassification,I.vcSKU,OI.numWarehouseItmsID,
            OBI.dtRentalStartDate,OBI.dtRentalReturnDate,OI.numUOMId,OM.bintCreatedDate,
            OI.monPrice,OI.monTotAmount,OBI.monPrice,OI.vcItemDesc,OI.bitDropShip,
            OI.ItemReleaseDate,OM.dtReleaseDate) X
         WHERE   X.QtytoFulFill > 0;
      ELSE
         open SWV_RefCur for
         SELECT * FROM(SELECT    OI.numoppitemtCode,
                                OI.numItemCode,
                                OI.vcItemName,
                                OI.vcModelID,
                                OI.numUnitHour AS QtyOrdered,
								CASE
            WHEN v_numBizDocID = 296
            THEN(CASE
               WHEN (I.charItemType = 'P' AND coalesce(OI.bitDropShip,false) = false AND coalesce(bitAsset,false) = false)
               THEN(CASE WHEN(OI.numUnitHour -coalesce(SUM(OBI.numUnitHour),0)) <= GetOrderItemAvailableQtyToShip(OI.numoppitemtCode,OI.numWarehouseItmsID,coalesce(I.bitKitParent,false),v_tintCommitAllocation,
                  v_bitAllocateInventoryOnPickList) THEN(OI.numUnitHour -coalesce(SUM(OBI.numUnitHour),0)) ELSE GetOrderItemAvailableQtyToShip(OI.numoppitemtCode,OI.numWarehouseItmsID,coalesce(I.bitKitParent,false),v_tintCommitAllocation,
                     v_bitAllocateInventoryOnPickList) END)
               ELSE(OI.numUnitHour -coalesce(SUM(OBI.numUnitHour),0))
               END)
            ELSE(OI.numUnitHour -coalesce(SUM(OBI.numUnitHour),0))
            END AS QtytoFulFilled,
                                CASE
            WHEN v_numBizDocID = 296
            THEN(CASE
               WHEN (I.charItemType = 'P' AND coalesce(OI.bitDropShip,false) = false AND coalesce(bitAsset,false) = false)
               THEN(CASE WHEN(OI.numUnitHour -coalesce(SUM(OBI.numUnitHour),0)) <= GetOrderItemAvailableQtyToShip(OI.numoppitemtCode,OI.numWarehouseItmsID,coalesce(I.bitKitParent,false),v_tintCommitAllocation,
                  v_bitAllocateInventoryOnPickList) THEN(OI.numUnitHour -coalesce(SUM(OBI.numUnitHour),0)) ELSE GetOrderItemAvailableQtyToShip(OI.numoppitemtCode,OI.numWarehouseItmsID,coalesce(I.bitKitParent,false),v_tintCommitAllocation,
                     v_bitAllocateInventoryOnPickList) END)
               ELSE(OI.numUnitHour -coalesce(SUM(OBI.numUnitHour),0))
               END)
            ELSE(OI.numUnitHour -coalesce(SUM(OBI.numUnitHour),0))
            END AS QtytoFulFill,
								(CASE WHEN coalesce(OI.numUnitHour,0) > 0 THEN((OI.numUnitHour -coalesce(SUM(OBI.numUnitHour),0))/OI.numUnitHour)*100 ELSE 0 END) AS numRemainingPercent,
								(CASE WHEN coalesce(OI.numUnitHour,0) > 0 THEN((coalesce(SUM(OBI.numUnitHour),0)*100)/OI.numUnitHour) ELSE 0 END) AS numIncludedPercent,
                                coalesce(numAllocation,0) AS numAllocation,
                                coalesce(numBackOrder,0) AS numBackOrder,
                                OI.vcNotes AS vcNotes,
                                '' AS vcTrackingNo,
                                '' AS vcShippingMethod,
                                0 AS monShipCost,
                                LOCALTIMESTAMP  AS dtDeliveryDate,
                                CAST(1 AS BOOLEAN) AS Included,
                                coalesce(OI.numQtyShipped,0) AS numQtyShipped,
                                coalesce(OI.numUnitHourReceived,0) AS numQtyReceived,
                                I.charItemType,
                                SUBSTR((SELECT  ',' || vcSerialNo
               || CASE WHEN coalesce(I.bitLotNo,false) = true THEN ' (' || CAST(oppI.numQty AS VARCHAR(15)) || ')'
               ELSE ''
               END
               FROM    OppWarehouseSerializedItem oppI
               JOIN WareHouseItmsDTL whi ON oppI.numWarehouseItmsDTLID = whi.numWareHouseItmsDTLID
               WHERE   oppI.numOppID = v_numOppID
               AND oppI.numOppItemID = OI.numoppitemtCode
               AND oppI.numOppBizDocsId = v_numOppBizDocID
               ORDER BY vcSerialNo),2,200000) AS SerialLotNo,
                                I.bitSerialized,
                                coalesce(I.bitLotNo,false) AS bitLotNo,
                                OI.numWarehouseItmsID,
                                coalesce(I.numItemClassification,0) AS numItemClassification,
                                I.vcSKU,
                                fn_GetAttributes(OI.numWarehouseItmsID,I.bitSerialized) AS Attributes,
                                NULL AS dtRentalStartDate,
                                NULL AS dtRentalReturnDate,
                                coalesce(OI.numUOMId,0) AS numUOM,
                                OI.monPrice AS monOppItemPrice,
                                OI.monPrice AS monOppBizDocPrice,
								OI.monTotAmount AS monOppBizDocAmount,
                                coalesce(OI.vcItemDesc,'') AS vcItemDesc,
								coalesce(OI.bitDropShip,false) AS bitDropShip,
								coalesce(OI.ItemReleaseDate,OM.dtReleaseDate) AS dtReleaseDate
            FROM      OpportunityItems OI
            LEFT JOIN OpportunityMaster OM ON OI.numOppId = OM.numOppId
            LEFT JOIN
            OpportunityBizDocItems OBI
            ON
            OBI.numOppItemID = OI.numoppitemtCode
            AND OBI.numOppBizDocID IN(SELECT
               numOppBizDocsId
               FROM
               OpportunityBizDocs OB
               WHERE
               OB.numoppid = OI.numOppId
               AND 1 =(CASE
               WHEN v_numBizDocID = v_numSalesAuthoritativeBizDocID --Invoice OR Deferred Income
               THEN(CASE WHEN numBizDocId = 304 OR (coalesce(OB.bitAuthoritativeBizDocs,0) = 1 AND coalesce(OB.numDeferredBizDocID,0) = 0) THEN 1 ELSE 0 END)
               WHEN  v_numBizDocID = 304
               THEN(CASE WHEN coalesce(OB.bitAuthoritativeBizDocs,0) = 1 THEN 1 ELSE 0 END)
               ELSE(CASE WHEN numBizDocId = v_numBizDocID THEN 1 ELSE 0 END)
               END))
            LEFT JOIN WareHouseItems WI ON WI.numWareHouseItemID = OI.numWarehouseItmsID
            INNER JOIN Item I ON I.numItemCode = OI.numItemCode
            WHERE     OI.numOppId = v_numOppID AND coalesce(OI.bitMappingRequired,false) = false
            GROUP BY  OI.numoppitemtCode,OI.numItemCode,OI.vcItemName,OI.vcModelID,OI.vcNotes,
            OI.numUnitHour,numAllocation,numBackOrder,OI.numQtyShipped,OI.numUnitHourReceived,
            I.charItemType,I.bitSerialized,I.bitLotNo,I.bitAsset,I.bitKitParent,
            OI.numWarehouseItmsID,I.numItemClassification,I.vcSKU,OI.numWarehouseItmsID,
            OI.numUOMId,OI.monPrice,OI.monTotAmount,OI.vcItemDesc,
            OI.bitDropShip,OI.ItemReleaseDate,OM.dtReleaseDate) X
         WHERE   X.QtytoFulFill > 0;
      end if;
   ELSEIF v_bitRentalBizDoc = true
   then
        
      open SWV_RefCur for
      SELECT  OI.numoppitemtCode,
                    OI.numItemCode,
                    OI.vcItemName,
                    OI.vcModelID,
                    OI.numUnitHour AS QtyOrdered,
                    OBI.numUnitHour AS QtytoFulFilled,
                    (OI.numUnitHour -coalesce(SUM(OBI.numUnitHour),0)) AS QtytoFulFill,
					(CASE WHEN coalesce(OI.numUnitHour,0) > 0 THEN((OI.numUnitHour -coalesce(SUM(OBI.numUnitHour),0))/OI.numUnitHour)*100 ELSE 0 END) AS numRemainingPercent,
					(CASE WHEN coalesce(OI.numUnitHour,0) > 0 THEN((coalesce(SUM(OBI.numUnitHour),0)*100)/OI.numUnitHour) ELSE 0 END) AS numIncludedPercent,
                    coalesce(numAllocation,0) AS numAllocation,
                    coalesce(numBackOrder,0) AS numBackOrder,
                    CASE WHEN OBI.vcNotes IS NULL THEN OI.vcNotes ELSE OBI.vcNotes END AS vcNotes,
                    coalesce(OBI.vcTrackingNo,'') AS vcTrackingNo,
                    OBI.vcShippingMethod,
                    OBI.monShipCost,
                    OBI.dtDeliveryDate,
                    CAST(1 AS BOOLEAN) AS Included,
                    coalesce(OI.numQtyShipped,0) AS numQtyShipped,
                    coalesce(OI.numUnitHourReceived,0) AS numQtyReceived,
                    I.charItemType,
                    SUBSTR((SELECT  ',' || vcSerialNo
         || CASE WHEN coalesce(I.bitLotNo,false) = true
         THEN ' ('
            || CAST(oppI.numQty AS VARCHAR(15))
            || ')'
         ELSE ''
         END
         FROM    OppWarehouseSerializedItem oppI
         JOIN WareHouseItmsDTL whi ON oppI.numWarehouseItmsDTLID = whi.numWareHouseItmsDTLID
         WHERE   oppI.numOppID = v_numOppID
         AND oppI.numOppItemID = OI.numoppitemtCode
         AND oppI.numOppBizDocsId = v_numOppBizDocID
         ORDER BY vcSerialNo),2,200000) AS SerialLotNo,
                    I.bitSerialized,
                    coalesce(I.bitLotNo,false) AS bitLotNo,
                    OI.numWarehouseItmsID,
                    coalesce(I.numItemClassification,0) AS numItemClassification,
                    I.vcSKU,
                    fn_GetAttributes(OI.numWarehouseItmsID,I.bitSerialized) AS Attributes,
                    OBI.dtRentalStartDate,
                    OBI.dtRentalReturnDate,
                    coalesce(OI.numUOMId,0) AS numUOM,
                    OI.monPrice AS monOppItemPrice,
                    coalesce(OBI.monPrice,OI.monPrice) AS monOppBizDocPrice,
					OI.monTotAmount AS monOppBizDocAmount,
                    coalesce(OI.vcItemDesc,'') AS vcItemDesc,
					coalesce(OI.bitDropShip,false) AS bitDropShip,
					coalesce(OI.ItemReleaseDate,OM.dtReleaseDate) AS dtReleaseDate
      FROM    OpportunityItems OI
      LEFT JOIN OpportunityMaster OM ON OI.numOppId = OM.numOppId
      JOIN OpportunityBizDocs OB ON OB.numoppid = OI.numOppId
      JOIN OpportunityBizDocItems OBI ON OB.numOppBizDocsId = OBI.numOppBizDocID
      AND OBI.numOppItemID = OI.numoppitemtCode
      LEFT JOIN WareHouseItems WI ON WI.numWareHouseItemID = OI.numWarehouseItmsID
      INNER JOIN Item I ON I.numItemCode = OI.numItemCode
      WHERE   OI.numOppId = v_numOppID
      AND OB.numOppBizDocsId = v_numOppBizDocID
      GROUP BY OI.numoppitemtCode,OI.numItemCode,OI.vcItemName,OI.vcModelID,OI.vcNotes,
      OI.numUnitHour,numAllocation,numBackOrder,OBI.vcNotes,OBI.vcTrackingNo,
      OBI.vcShippingMethod,OBI.monShipCost,OBI.dtDeliveryDate,OBI.numUnitHour,
      OI.numQtyShipped,OI.numUnitHourReceived,I.charItemType,I.bitSerialized,
      I.bitLotNo,OI.numWarehouseItmsID,I.numItemClassification,I.vcSKU,
      OI.numWarehouseItmsID,OBI.dtRentalStartDate,OBI.dtRentalReturnDate,OI.numUOMId,
      OI.monPrice,OI.monTotAmount,OBI.monPrice,OI.vcItemDesc,OI.bitDropShip,
      OI.ItemReleaseDate,OM.dtReleaseDate;
   ELSE
      open SWV_RefCur for
      SELECT  OI.numoppitemtCode,
                    OI.numItemCode,
                    OI.vcItemName,
                    OI.vcModelID,
                    OI.numUnitHour AS QtyOrdered,
                    OBI.numUnitHour AS QtytoFulFilled,
                    (OI.numUnitHour -coalesce(SUM(OBI.numUnitHour),0) -coalesce(TEMPDeferredIncomeItem.numUnitHour,0)) AS QtytoFulFill,
					(CASE WHEN coalesce(OI.numUnitHour,0) > 0 THEN((OI.numUnitHour -coalesce(SUM(OBI.numUnitHour),0))/OI.numUnitHour)*100 ELSE 0 END) AS numRemainingPercent,
					(CASE WHEN coalesce(OI.numUnitHour,0) > 0 THEN((coalesce(SUM(OBI.numUnitHour),0)*100)/OI.numUnitHour) ELSE 0 END) AS numIncludedPercent,
                    coalesce(numAllocation,0) AS numAllocation,
                    coalesce(numBackOrder,0) AS numBackOrder,
                    CASE WHEN OBI.vcNotes IS NULL THEN OI.vcNotes ELSE OBI.vcNotes END AS vcNotes,
                    coalesce(OBI.vcTrackingNo,'') AS vcTrackingNo,
                    OBI.vcShippingMethod,
                    OBI.monShipCost,
                    OBI.dtDeliveryDate,
                    CAST(1 AS BOOLEAN) AS Included,
                    coalesce(OI.numQtyShipped,0) AS numQtyShipped,
                    coalesce(OI.numUnitHourReceived,0) AS numQtyReceived,
                    I.charItemType,
                    SUBSTR((SELECT  ',' || vcSerialNo
         || CASE WHEN coalesce(I.bitLotNo,false) = true
         THEN ' ('
            || CAST(oppI.numQty AS VARCHAR(15))
            || ')'
         ELSE ''
         END
         FROM    OppWarehouseSerializedItem oppI
         JOIN WareHouseItmsDTL whi ON oppI.numWarehouseItmsDTLID = whi.numWareHouseItmsDTLID
         WHERE   oppI.numOppID = v_numOppID
         AND oppI.numOppItemID = OI.numoppitemtCode
         AND oppI.numOppBizDocsId = v_numOppBizDocID
         ORDER BY vcSerialNo),2,200000) AS SerialLotNo,
                    I.bitSerialized,
                    coalesce(I.bitLotNo,false) AS bitLotNo,
                    OI.numWarehouseItmsID,
                    coalesce(I.numItemClassification,0) AS numItemClassification,
                    I.vcSKU,
                    fn_GetAttributes(OI.numWarehouseItmsID,I.bitSerialized) AS Attributes,
                    OBI.dtRentalStartDate,
                    OBI.dtRentalReturnDate,
                    coalesce(OI.numUOMId,0) AS numUOM,
                    OI.monPrice AS monOppItemPrice,
                    coalesce(OBI.monPrice,OI.monPrice) AS monOppBizDocPrice,
					OI.monTotAmount AS monOppBizDocAmount,
                    coalesce(OI.vcItemDesc,'') AS vcItemDesc,
					coalesce(OI.bitDropShip,false) AS bitDropShip,
					coalesce(OI.ItemReleaseDate,OM.dtReleaseDate) AS dtReleaseDate
      FROM    OpportunityItems OI
      LEFT JOIN OpportunityMaster OM ON OI.numOppId = OM.numOppId
      JOIN OpportunityBizDocs OB ON OB.numoppid = OI.numOppId
      JOIN OpportunityBizDocItems OBI ON OB.numOppBizDocsId = OBI.numOppBizDocID
      AND OBI.numOppItemID = OI.numoppitemtCode
      LEFT JOIN WareHouseItems WI ON WI.numWareHouseItemID = OI.numWarehouseItmsID
      INNER JOIN Item I ON I.numItemCode = OI.numItemCode
      LEFT JOIN LATERAL(SELECT
         SUM(OpportunityBizDocItems.numUnitHour) AS numUnitHour
         FROM
         OpportunityBizDocs
         INNER JOIN
         OpportunityBizDocItems
         ON
         OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
         WHERE
         numoppid = v_numOppID
         AND numOppBizDocID <> v_numOppBizDocID
         AND	1 =(CASE
         WHEN v_numBizDocID = 304 --DEFERRED BIZDOC OR INVOICE
         THEN(CASE WHEN (numBizDocId = v_numSalesAuthoritativeBizDocID OR numBizDocId = 304) THEN 1 ELSE 0 END) -- WHEN DEFERRED BIZDOC(304) IS BEING ADDED SUBTRACT ITEMS ADDED TO INVOICE AND OTHER DEFERRED BIZDOC IN QtytoFulFill
         ELSE(CASE WHEN numBizDocId = v_numBizDocID THEN 1 ELSE 0 END)
         END)
         AND OpportunityBizDocItems.numOppItemID = OI.numoppitemtCode) TEMPDeferredIncomeItem on TRUE
      WHERE   OI.numOppId = v_numOppID
      AND OB.numOppBizDocsId = v_numOppBizDocID
      GROUP BY OI.numoppitemtCode,OI.numItemCode,OI.vcItemName,OI.vcModelID,OI.vcNotes,
      OI.numUnitHour,numAllocation,numBackOrder,OBI.vcNotes,OBI.vcTrackingNo,
      OBI.vcShippingMethod,OBI.monShipCost,OBI.dtDeliveryDate,OBI.numUnitHour,
      OI.numQtyShipped,OI.numUnitHourReceived,I.charItemType,I.bitSerialized,
      I.bitLotNo,OI.numWarehouseItmsID,I.numItemClassification,I.vcSKU,
      OI.numWarehouseItmsID,OBI.dtRentalStartDate,OBI.dtRentalReturnDate,OI.numUOMId,
      OI.monPrice,OI.monTotAmount,OBI.monPrice,OI.vcItemDesc,OI.bitDropShip,
      OI.ItemReleaseDate,OM.dtReleaseDate,TEMPDeferredIncomeItem.numUnitHour
      UNION
      SELECT  numoppitemtCode,
                                numItemCode,
                                vcItemName,
                                vcModelID,
                                QtyOrdered,
                                QtytoFulFilled,
                                QtytoFulFill,
								numRemainingPercent,
								numIncludedPercent,
                                numAllocation,
                                numBackOrder,
                                vcNotes,
                                vcTrackingNo,
                                vcShippingMethod,
                                monShipCost,
                                dtDeliveryDate,
                                Included,
                                numQtyShipped,
                                numQtyReceived,
                                charItemType,
                                SerialLotNo,
                                bitSerialized,
                                bitLotNo,
                                numWarehouseItmsID,
                                numItemClassification,
                                vcSKU,
                                Attributes,
                                dtRentalStartDate,
                                dtRentalReturnDate,
                                numUOM,
                                monOppItemPrice,
                                monOppBizDocPrice,
								monOppBizDocAmount,
                                vcItemDesc,
								bitDropShip,
								dtReleaseDate
      FROM(SELECT    OI.numoppitemtCode,
                                OI.numItemCode,
                                OI.vcItemName,
                                OI.vcModelID,
                                OI.numUnitHour AS QtyOrdered,
                                (OI.numUnitHour -coalesce(SUM(OBI.numUnitHour),0)) AS QtytoFulFilled,
                                (OI.numUnitHour -coalesce(SUM(OBI.numUnitHour),0)) AS QtytoFulFill,
								(CASE WHEN coalesce(OI.numUnitHour,0) > 0 THEN((OI.numUnitHour -coalesce(SUM(OBI.numUnitHour),0))/OI.numUnitHour)*100 ELSE 0 END) AS numRemainingPercent,
								(CASE WHEN coalesce(OI.numUnitHour,0) > 0 THEN((coalesce(SUM(OBI.numUnitHour),0)*100)/OI.numUnitHour) ELSE 0 END) AS numIncludedPercent,
                                coalesce(numAllocation,0) AS numAllocation,
                                coalesce(numBackOrder,0) AS numBackOrder,
                                CASE WHEN OBI.vcNotes IS NULL THEN OI.vcNotes ELSE OBI.vcNotes END AS vcNotes,
                                '' AS vcTrackingNo,
                                '' AS vcShippingMethod,
                                1 AS monShipCost,
                                null::timestamp AS dtDeliveryDate,
                                false AS Included,
                                coalesce(OI.numQtyShipped,0) AS numQtyShipped,
                                coalesce(OI.numUnitHourReceived,0) AS numQtyReceived,
                                I.charItemType,
                                SUBSTR((SELECT  ',' || vcSerialNo
            || CASE WHEN coalesce(I.bitLotNo,false) = true THEN ' (' || CAST(oppI.numQty AS VARCHAR(15)) || ')'
            ELSE ''
            END
            FROM    OppWarehouseSerializedItem oppI
            JOIN WareHouseItmsDTL whi ON oppI.numWarehouseItmsDTLID = whi.numWareHouseItmsDTLID
            WHERE   oppI.numOppID = v_numOppID
            AND oppI.numOppItemID = OI.numoppitemtCode
            AND oppI.numOppBizDocsId = v_numOppBizDocID
            ORDER BY vcSerialNo),2,200000) AS SerialLotNo,
                                I.bitSerialized,
                                coalesce(I.bitLotNo,false) AS bitLotNo,
                                OI.numWarehouseItmsID,
                                coalesce(I.numItemClassification,0) AS numItemClassification,
                                I.vcSKU,
                                fn_GetAttributes(OI.numWarehouseItmsID,I.bitSerialized) AS Attributes,
                                OBI.dtRentalStartDate,
                                OBI.dtRentalReturnDate,
                                coalesce(OI.numUOMId,0) AS numUOM,
                                OI.monPrice AS monOppItemPrice,
                                coalesce(OBI.monPrice,OI.monPrice) AS monOppBizDocPrice,
								OI.monTotAmount AS monOppBizDocAmount,
                                coalesce(OI.vcItemDesc,'') AS vcItemDesc,
								coalesce(OI.bitDropShip,false) AS bitDropShip,
								coalesce(OI.ItemReleaseDate,OM.dtReleaseDate) AS dtReleaseDate
         FROM      OpportunityItems OI
         LEFT JOIN OpportunityMaster OM ON OI.numOppId = OM.numOppId
         LEFT JOIN OpportunityBizDocs OB ON OB.numoppid = OI.numOppId
         AND OB.numBizDocId = v_numBizDocID
         LEFT JOIN OpportunityBizDocItems OBI ON OB.numOppBizDocsId = OBI.numOppBizDocID
         AND OBI.numOppItemID = OI.numoppitemtCode
         LEFT JOIN WareHouseItems WI ON WI.numWareHouseItemID = OI.numWarehouseItmsID
         INNER JOIN Item I ON I.numItemCode = OI.numItemCode
         WHERE     OI.numOppId = v_numOppID
         AND OI.numoppitemtCode NOT IN(SELECT  numOppItemID
            FROM    OpportunityBizDocItems OBI2
            JOIN OpportunityBizDocs OB2 ON OBI2.numOppBizDocID = OB2.numOppBizDocsId
            WHERE   numoppid = v_numOppID
            AND numBizDocId = v_numBizDocID)
         GROUP BY  OI.numoppitemtCode,OI.numItemCode,OI.vcItemName,OI.vcModelID,OI.vcNotes,
         OBI.vcNotes,OI.numUnitHour,numAllocation,numBackOrder,OBI.numUnitHour,
         OI.numQtyShipped,OI.numUnitHourReceived,I.charItemType,I.bitSerialized,
         I.bitLotNo,OI.numWarehouseItmsID,I.numItemClassification,I.vcSKU,OI.numWarehouseItmsID,
         OBI.dtRentalStartDate,OBI.dtRentalReturnDate,OI.numUOMId,
         OI.monPrice,OI.monTotAmount,OBI.monPrice,OI.vcItemDesc,OI.bitDropShip,
         OI.ItemReleaseDate,OM.dtReleaseDate) X
      WHERE   X.QtytoFulFill > 0;
   end if;
   RETURN;
END; $$;




