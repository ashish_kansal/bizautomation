-- Stored procedure definition script USP_GetWarehouseItemDetails for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetWarehouseItemDetails(v_numWareHouseItemID		BIGINT,
	v_numDomainID			NUMERIC(18,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT coalesce(WI.numWareHouseItemID,0) AS numWareHouseItemID,
		coalesce(numItemID,0) AS numItemID,
		coalesce(WI.numWareHouseID,0) AS numWareHouseID,
		coalesce(numOnHand,0) AS numOnHand,
		coalesce(numOnOrder,0) AS numOnOrder,
		coalesce(numReorder,0) AS numReorder,
		coalesce(numAllocation,0) AS numAllocation,
		coalesce(numBackOrder,0) AS numBackOrder,
		coalesce(monWListPrice,0) AS monWListPrice,
		coalesce(vcLocation,'') AS vcLocation,
		coalesce(vcWHSKU,'') AS vcWHSKU,
		coalesce(vcBarCode,'') AS vcBarCode,
		coalesce(I.numDomainID,0) AS numDomainID,
		coalesce(dtModified,null::TIMESTAMP) AS dtModified,
		coalesce(numWLocationID,0) AS numWLocationID
   FROM WareHouseItems WI
   JOIN Warehouses W ON WI.numWareHouseID = W.numWareHouseID
   JOIN Item I ON WI.numItemID = I.numItemCode AND WI.numDomainID = I.numDomainID
   WHERE WI.numWareHouseItemID = v_numWareHouseItemID
   AND WI.numDomainID = v_numDomainID;
END; $$;













