CREATE OR REPLACE FUNCTION USP_WorkOrder_PutAway(v_numDomainID NUMERIC(18,0)
	,v_numUserCntID NUMERIC(18,0)
	,v_numWOID NUMERIC(18,0)
	,v_numQtyReceived DOUBLE PRECISION
	,v_dtItemReceivedDate TIMESTAMP
	,v_vcWarehouses TEXT
	,v_tintMode SMALLINT)
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_hDocItem  INTEGER;
   v_numItemCode  NUMERIC(18,0);
   v_monListPrice  DECIMAL(20,5);
   v_numWarehouseID  NUMERIC(18,0);
   v_numWarehouseItemID  NUMERIC(18,0);
   v_numOverheadServiceItemID  NUMERIC(18,0);
   v_monAverageCost  DECIMAL(20,5);
   v_newAverageCost  DECIMAL(20,5);
   v_monOverheadCost  DECIMAL(20,5); 
   v_monLabourCost  DECIMAL(20,5);
   v_CurrentAverageCost  DECIMAL(20,5);
   v_TotalCurrentOnHand  DOUBLE PRECISION;
   v_bitLot  BOOLEAN;
   v_bitSerial  BOOLEAN;
   v_numOldQtyReceived  DOUBLE PRECISION;
   v_numUnits  DOUBLE PRECISION;
   v_description  VARCHAR(300);
   v_i  INTEGER;
   v_j  INTEGER;
   v_k  INTEGER;
   v_iCount  INTEGER DEFAULT 0;
   v_jCount  INTEGER DEFAULT 0;
   v_kCount  INTEGER DEFAULT 0;
   v_vcSerialLot  TEXT;
   v_numWareHouseItmsDTLID  NUMERIC(18,0);
   v_vcSerailLotNumber  VARCHAR(300);
   v_numSerialLotQty  DOUBLE PRECISION;
   v_numTempWLocationID  NUMERIC(18,0);
   v_numTempWarehouseItemID  NUMERIC(18,0);
   v_numTempReceivedQty  DOUBLE PRECISION;
   v_numLotWareHouseItmsDTLID  NUMERIC(18,0);
   v_numLotQty  DOUBLE PRECISION;
   SWV_RCur REFCURSOR;
   v_dtDATE  TIMESTAMP DEFAULT TIMEZONE('UTC',now());
   v_vcDescription  VARCHAR(100) DEFAULT 'INSERT WareHouse';
BEGIN
   IF coalesce(v_numQtyReceived,0) <= 0 then
	
      RAISE EXCEPTION 'QTY_TO_RECEIVE_NOT_PROVIDED';
      RETURN;
   end if; 

   IF LENGTH(coalesce(v_vcWarehouses,'')) = 0 then
	
      RAISE EXCEPTION 'LOCATION_NOT_SELECTED';
      RETURN;
   end if;

   DROP TABLE IF EXISTS tt_TEMPWAREHOUSELOT CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPWAREHOUSELOT
   (
      ID INTEGER,
      numWareHouseItmsDTLID NUMERIC(18,0),
      vcSerialNo VARCHAR(300),
      numQty DOUBLE PRECISION
   );

   DROP TABLE IF EXISTS tt_TEMPSERIALLOT CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPSERIALLOT
   (
      ID INTEGER,
      vcSerialNo VARCHAR(300),
      numQty DOUBLE PRECISION
   );

   BEGIN
      CREATE TEMP SEQUENCE tt_TEMPWarehouseLocations_seq INCREMENT BY 1 START WITH 1;
      EXCEPTION WHEN OTHERS THEN
         NULL;
   END;
   DROP TABLE IF EXISTS tt_TEMPWAREHOUSELOCATIONS CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPWAREHOUSELOCATIONS
   (
      ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1),
      numWarehouseLocationID NUMERIC(18,0),
      numWarehouseItemID NUMERIC(18,0),
      numReceivedQty DOUBLE PRECISION,
      vcSerialLotNo TEXT
   );

   INSERT INTO tt_TEMPWAREHOUSELOCATIONS(numWarehouseLocationID
		,numWarehouseItemID
		,numReceivedQty
		,vcSerialLotNo)
   SELECT(CASE WHEN coalesce(bitNewLocation,false) = true THEN ID ELSE 0 END)
		,(CASE WHEN coalesce(bitNewLocation,false) = true THEN 0 ELSE ID END)
		,coalesce(Qty,0)
		,coalesce(SerialLotNo,'')
   FROM  XMLTABLE
			(
				'NewDataSet/Table1'
				PASSING 
					CAST(v_vcWarehouses AS XML)
				COLUMNS
					idPK FOR ORDINALITY,
					bitNewLocation BOOLEAN PATH 'bitNewLocation'
					,ID NUMERIC(18,0) PATH 'ID'
					,Qty DOUBLE PRECISION PATH 'Qty'
					,SerialLotNo TEXT PATH 'SerialLotNo'
			);

   IF v_numQtyReceived <> coalesce((SELECT SUM(numReceivedQty) FROM tt_TEMPWAREHOUSELOCATIONS),0) then
	
      RAISE EXCEPTION 'INALID_QTY_RECEIVED';
      RETURN;
   end if;

   IF EXISTS(SELECT numWOId FROM WorkOrder WHERE numDomainId = v_numDomainID AND numWOId = v_numWOID) then
      select   
		Item.numItemCode
		, coalesce(Item.monListPrice,0)
		, WareHouseItems.numWareHouseID
		, WareHouseItems.numWareHouseItemID
		, coalesce(bitLotNo,false)
		, coalesce(bitSerialized,false)
		, coalesce(numQtyItemsReq,0)
		, coalesce(numUnitHourReceived,0) 
		INTO v_numItemCode,v_monListPrice,v_numWarehouseID,v_numWarehouseItemID,v_bitLot,
      v_bitSerial,v_numUnits,v_numOldQtyReceived FROM
      WorkOrder
      INNER JOIN
      Item
      ON
      WorkOrder.numItemCode = Item.numItemCode
      INNER JOIN
      WareHouseItems
      ON
      WareHouseItems.numWareHouseItemID = WorkOrder.numWareHouseItemId WHERE
      WorkOrder.numDomainId = v_numDomainID
      AND WorkOrder.numWOId = v_numWOID;
      IF(v_numQtyReceived+v_numOldQtyReceived) > v_numUnits then
		
         RAISE EXCEPTION 'RECEIVED_QTY_MUST_BE_LESS_THEN_OR_EQUAL_TO_ORDERED_QTY';
         RETURN;
      end if;
      select   coalesce(numOverheadServiceItemID,0) INTO v_numOverheadServiceItemID FROM Domain WHERE numDomainId = v_numDomainID;
      select(CASE WHEN coalesce(bitVirtualInventory,false) = true THEN 0 ELSE monAverageCost END) INTO v_CurrentAverageCost FROM Item WHERE numItemCode = v_numItemCode;
      select   coalesce((SUM(numOnHand)+SUM(numAllocation)),0) INTO v_TotalCurrentOnHand FROM WareHouseItems WHERE numItemID = v_numItemCode;
      RAISE NOTICE '%',v_CurrentAverageCost;
      RAISE NOTICE '%',v_TotalCurrentOnHand;

		-- Store hourly rate for all task assignee
      UPDATE
      StagePercentageDetailsTask SPDT
      SET
      monHourlyRate = coalesce(UM.monHourlyRate,0)
      FROM
      UserMaster UM
      WHERE
      SPDT.numAssignTo = UM.numUserDetailId AND(SPDT.numDomainID = v_numDomainID
      AND SPDT.numWorkOrderId = v_numWOID);
      select   SUM(numQtyItemsReq*coalesce(I.monAverageCost,0)) INTO v_newAverageCost FROM
      WorkOrderDetails WOD
      LEFT JOIN
      Item I
      ON
      I.numItemCode = WOD.numChildItemID WHERE
      WOD.numWOId = v_numWOID
      AND I.charItemType = 'P';
      IF v_numOverheadServiceItemID > 0 AND EXISTS(SELECT numWODetailId FROM WorkOrderDetails WHERE numWOId = v_numWOID AND numChildItemID = v_numOverheadServiceItemID) then
		
         SELECT SUM(numQtyItemsReq*coalesce(Item.monListPrice,0)) INTO v_monOverheadCost FROM WorkOrderDetails WOD INNER JOIN Item ON WOD.numChildItemID = Item.numItemCode WHERE WOD.numWOId = v_numWOID AND WOD.numChildItemID = v_numOverheadServiceItemID;
      end if;
      v_monLabourCost := GetWorkOrderLabourCost(v_numDomainID,v_numWOID);
      UPDATE WorkOrder SET monAverageCost = v_newAverageCost,monLabourCost = coalesce(v_monLabourCost,0),
      monOverheadCost = coalesce(v_monOverheadCost,0) WHERE numWOId = v_numWOID;
      v_newAverageCost :=((v_CurrentAverageCost*v_TotalCurrentOnHand)+(v_newAverageCost+coalesce(v_monOverheadCost,0)+coalesce(v_monLabourCost,0)))/(v_TotalCurrentOnHand+v_numQtyReceived);
      UPDATE
      WareHouseItems
      SET
      numonOrder = coalesce(numonOrder,0) -v_numQtyReceived,dtModified = LOCALTIMESTAMP
      WHERE
      numWareHouseItemID = v_numWarehouseItemID;
		 --  numeric(9, 0)
			 --  numeric(9, 0)
			 --  tinyint
      v_description := CONCAT('Work Order Received (Qty:',v_numQtyReceived,')');
      PERFORM USP_ManageWareHouseItems_Tracking(v_numWarehouseItemID := v_numWarehouseItemID,v_numReferenceID := v_numWOID,
      v_tintRefType := 2::SMALLINT,v_vcDescription := v_description,v_numModifiedBy := v_numUserCntID,
      v_dtRecordDate := v_dtItemReceivedDate,
      v_numDomainID := v_numDomainID,SWV_RefCur := null);
      v_i := 1;
      SELECT COUNT(*) INTO v_iCount FROM tt_TEMPWAREHOUSELOCATIONS;
      WHILE v_i <= v_iCount LOOP
         select   coalesce(numWarehouseLocationID,0), coalesce(numWarehouseItemID,0), coalesce(numReceivedQty,0), coalesce(vcSerialLotNo,'') INTO v_numTempWLocationID,v_numTempWarehouseItemID,v_numTempReceivedQty,v_vcSerialLot FROM
         tt_TEMPWAREHOUSELOCATIONS WHERE
         ID = v_i;
         IF (coalesce(v_bitLot,false) = true OR coalesce(v_bitSerial,false) = true) then
			
            IF LENGTH(coalesce(v_vcSerialLot,'')) = 0 then
				
               RAISE EXCEPTION 'SERIALLOT_REQUIRED';
               RETURN;
            ELSE
               DELETE FROM tt_TEMPSERIALLOT;
               IF coalesce(v_bitSerial,false) = true then
					
                  INSERT INTO tt_TEMPSERIALLOT(ID
							,vcSerialNo
							,numQty)
                  SELECT
                  ROW_NUMBER() OVER(ORDER BY OutParam)
							,CAST(RTRIM(LTRIM(OutParam)) AS VARCHAR(300))
							,1
                  FROM
                  SplitString(v_vcSerialLot,',');
               ELSEIF v_bitLot = true
               then
					
                  INSERT INTO tt_TEMPSERIALLOT(ID
							,vcSerialNo
							,numQty)
                  SELECT
                  ROW_NUMBER() OVER(ORDER BY OutParam)
							,CAST((CASE
                  WHEN coalesce(POSITION(substring(OutParam from E'\\(') IN OutParam),0) > 0 AND coalesce(POSITION(substring(OutParam from E'\\)') IN OutParam),0) > 0 AND coalesce(POSITION(substring(OutParam from E'\\)') IN OutParam),0) >(coalesce(POSITION(substring(OutParam from E'\\(') IN OutParam),0)+1)
                  THEN RTRIM(LTRIM(SUBSTR(OutParam,0,coalesce(POSITION(substring(OutParam from E'\\(') IN OutParam),0))))
                  ELSE ''
                  END) AS VARCHAR(300))
							,(CASE
                  WHEN coalesce(POSITION(substring(OutParam from E'\\(') IN OutParam),0) > 0 AND coalesce(POSITION(substring(OutParam from E'\\)') IN OutParam),0) > 0 AND coalesce(POSITION(substring(OutParam from E'\\)') IN OutParam),0) >(coalesce(POSITION(substring(OutParam from E'\\(') IN OutParam),0)+1)
                  THEN(CASE WHEN SWF_IsNumeric(SUBSTR(OutParam,(coalesce(POSITION(substring(OutParam from E'\\(') IN OutParam),0)+1),(coalesce(POSITION(substring(OutParam from E'\\)') IN OutParam),0) -coalesce(POSITION(substring(OutParam from E'\\(') IN OutParam),0) -1))) = true
                     THEN CAST(SUBSTR(OutParam,(coalesce(POSITION(substring(OutParam from E'\\(') IN OutParam),0)+1),(coalesce(POSITION(substring(OutParam from E'\\)') IN OutParam),0) -coalesce(POSITION(substring(OutParam from E'\\(') IN OutParam),0) -1)) AS DOUBLE PRECISION)
                     ELSE -1
                     END)
                  ELSE -1
                  END)
                  FROM
                  SplitString(v_vcSerialLot,',');
               end if;
               IF EXISTS(SELECT vcSerialNo FROM tt_TEMPSERIALLOT WHERE numQty = -1) then
					
                  RAISE EXCEPTION 'INVALID_SERIAL_NO';
                  RETURN;
               ELSEIF v_numTempReceivedQty <> coalesce((SELECT SUM(numQty) FROM tt_TEMPSERIALLOT),0)
               then
					
                  RAISE EXCEPTION 'INALID_SERIALLOT';
                  RETURN;
               end if;
            end if;
         end if;
         IF coalesce(v_numTempWarehouseItemID,0) = 0 then
			
            IF EXISTS(SELECT numWLocationID FROM WarehouseLocation WHERE numDomainID = v_numDomainID AND numWLocationID = coalesce(v_numTempWLocationID,0)) then
				
               IF EXISTS(SELECT numWareHouseItemID FROM WareHouseItems WHERE numDomainID = v_numDomainID AND numItemID = v_numItemCode AND numWareHouseID = v_numWarehouseID AND numWLocationID = v_numTempWLocationID) then
					
                  SELECT  numWareHouseItemID INTO v_numTempWarehouseItemID FROM WareHouseItems WHERE numDomainID = v_numDomainID AND numItemID = v_numItemCode AND numWareHouseID = v_numWarehouseID AND numWLocationID = v_numTempWLocationID     LIMIT 1;
               ELSE
					
                  INSERT INTO WareHouseItems(numItemID,
							numWareHouseID,
							numWLocationID,
							numOnHand,
							numAllocation,
							numonOrder,
							numBackOrder,
							numReorder,
							monWListPrice,
							vcLocation,
							vcWHSKU,
							vcBarCode,
							numDomainID,
							dtModified)
						VALUES(v_numItemCode,
							v_numWarehouseID,
							v_numTempWLocationID,
							0,
							0,
							0,
							0,
							0,
							v_monListPrice,
							coalesce((SELECT vcLocation FROM WarehouseLocation WHERE numDomainID = v_numDomainID AND numWLocationID = coalesce(v_numTempWLocationID, 0)), ''),
							'',
							'',
							v_numDomainID,
							LOCALTIMESTAMP);
						
                  v_numTempWarehouseItemID := CURRVAL('WareHouseItems_seq');
                  PERFORM USP_ManageWareHouseItems_Tracking(v_numWarehouseItemID := v_numTempWarehouseItemID,v_numReferenceID := v_numItemCode,
                  v_tintRefType := 1::SMALLINT,v_vcDescription := v_vcDescription,
                  v_numModifiedBy := v_numUserCntID,v_ClientTimeZoneOffset := 0,v_dtRecordDate := v_dtDATE,
                  v_numDomainID := v_numDomainID,SWV_RefCur := null);
               end if;
            ELSE
               RAISE EXCEPTION 'INVALID_WAREHOUSE_LOCATION';
               RETURN;
            end if;
         end if;
         IF NOT EXISTS(SELECT numWareHouseItemID FROM WareHouseItems WHERE numDomainID = v_numDomainID AND numItemID = v_numItemCode AND numWareHouseItemID = v_numTempWarehouseItemID) then
			
            RAISE EXCEPTION 'INVALID_WAREHOUSE';
            RETURN;
         ELSE
				-- INCREASE THE OnHand Of Destination Location
            UPDATE
            WareHouseItems
            SET
            numBackOrder =(CASE WHEN numBackOrder > v_numTempReceivedQty THEN coalesce(numBackOrder,0) -v_numTempReceivedQty ELSE 0 END),numAllocation =(CASE WHEN numBackOrder > v_numTempReceivedQty THEN coalesce(numAllocation,0)+v_numTempReceivedQty ELSE coalesce(numAllocation,0)+coalesce(numBackOrder,0) END),
            numOnHand =(CASE WHEN numBackOrder > v_numTempReceivedQty THEN numOnHand ELSE coalesce(numOnHand,0)+(v_numTempReceivedQty -coalesce(numBackOrder,0)) END),dtModified = LOCALTIMESTAMP
            WHERE
            numWareHouseItemID = v_numTempWarehouseItemID;
				 --  numeric(9, 0)
					 --  numeric(9, 0)
					 --  tinyint
            v_description := CONCAT('Work Order Qty Put-Away (Qty:',v_numTempReceivedQty,')');
            PERFORM USP_ManageWareHouseItems_Tracking(v_numWarehouseItemID := v_numTempWarehouseItemID,v_numReferenceID := v_numWOID,
            v_tintRefType := 2::SMALLINT,v_vcDescription := v_description,
            v_numModifiedBy := v_numUserCntID,v_dtRecordDate := v_dtItemReceivedDate,
            v_numDomainID := v_numDomainID,SWV_RefCur := null);
            IF (coalesce(v_bitLot,false) = true OR coalesce(v_bitSerial,false) = true) then
				
               v_j := 1;
               SELECT COUNT(*) INTO v_jCount FROM tt_TEMPSERIALLOT;
               WHILE (v_j <= v_jCount AND v_numTempReceivedQty > 0) LOOP
                  select   coalesce(vcSerialNo,''), coalesce(numQty,0) INTO v_vcSerailLotNumber,v_numSerialLotQty FROM
                  tt_TEMPSERIALLOT WHERE
                  ID = v_j;
                  IF v_numSerialLotQty > 0 then
						
                     IF v_bitSerial = true AND(SELECT COUNT(*) FROM WareHouseItems INNER JOIN WareHouseItmsDTL ON WareHouseItems.numWareHouseItemID = WareHouseItmsDTL.numWareHouseItemID WHERE WareHouseItems.numItemID = v_numItemCode AND LOWER(vcSerialNo) = LOWER(v_vcSerailLotNumber)) > 0 then
							
                        RAISE EXCEPTION 'DUPLICATE_SERIAL_NO';
                        RETURN;
                     end if;
                     INSERT INTO WareHouseItmsDTL(numWareHouseItemID
								,vcSerialNo
								,numQty
								,bitAddedFromPO)
							VALUES(v_numTempWarehouseItemID
								,v_vcSerailLotNumber
								,(CASE WHEN v_numTempReceivedQty >= v_numSerialLotQty THEN v_numSerialLotQty ELSE v_numTempReceivedQty END)
								,1);
							
                     v_numWareHouseItmsDTLID := CURRVAL('WareHouseItmsDTL_seq');
                     INSERT INTO OppWarehouseSerializedItem(numWarehouseItmsDTLID
								,numWOID
								,numWarehouseItmsID
								,numQty)
							VALUES(v_numWareHouseItmsDTLID
								,v_numWOID
								,v_numTempWarehouseItemID
								,(CASE WHEN v_numTempReceivedQty >= v_numSerialLotQty THEN v_numSerialLotQty ELSE v_numTempReceivedQty END));
							
                     IF v_numTempReceivedQty >= v_numSerialLotQty then
							
                        v_numTempReceivedQty := v_numTempReceivedQty -v_numSerialLotQty;
                        UPDATE tt_TEMPSERIALLOT SET numQty = 0  WHERE ID = v_j;
                     ELSE
                        UPDATE tt_TEMPSERIALLOT SET numQty = numQty -v_numTempReceivedQty  WHERE ID = v_j;
                        v_numTempReceivedQty := 0;
                     end if;
                  end if;
                  v_j := v_j::bigint+1;
               END LOOP;
               IF v_numTempReceivedQty > 0 then
					
                  RAISE EXCEPTION 'INALID_SERIALLOT';
                  RETURN;
               end if;
            end if;
         end if;
         v_i := v_i::bigint+1;
      END LOOP;
      UPDATE
      WorkOrder
      SET
      numUnitHourReceived = coalesce(numUnitHourReceived,0)+coalesce(v_numQtyReceived,0),numQtyReceived =(CASE WHEN coalesce(v_tintMode,0) = 1 THEN coalesce(numQtyReceived,0)+coalesce(v_numQtyReceived,0) ELSE coalesce(numQtyReceived,0) END)
      WHERE
      numDomainId = v_numDomainID
      AND numWOId = v_numWOID;
   ELSE
      RAISE EXCEPTION 'WORKORDER_DOES_NOT_EXISTS';
   end if;
END; $$;


