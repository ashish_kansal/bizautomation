-- Stored procedure definition script usp_GetDuplicateCompany for PostgreSQL
CREATE OR REPLACE FUNCTION usp_GetDuplicateCompany(v_numCompanyID NUMERIC,
v_vcCompanyName VARCHAR(100),
v_numDivisionID NUMERIC
--
,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for select cast(CI.numCompanyId as VARCHAR(255)), cast(CI.vcCompanyName as VARCHAR(255)), DM.vcDivisionName
   from CompanyInfo CI INNER JOIN DivisionMaster DM ON CI.numCompanyId = DM.numCampaignID
   where CI.vcCompanyName = v_vcCompanyName
   And DM.numDivisionID <> v_numDivisionID and DM.bitPublicFlag = false and DM.tintCRMType in(1,2)
   Group by CI.numCompanyId,CI.vcCompanyName,DM.vcDivisionName;
END; $$;












