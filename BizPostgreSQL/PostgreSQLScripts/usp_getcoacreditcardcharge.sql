-- Stored procedure definition script USP_GetCOACreditCardCharge for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetCOACreditCardCharge(v_numTransChargeID NUMERIC(9,0),
    v_numCreditCardTypeId NUMERIC(9,0),
	v_numDomainID NUMERIC(9,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT cast(numTransChargeID as VARCHAR(255)),
		cast(numAccountID as VARCHAR(255)),
		cast(fltTransactionCharge as VARCHAR(255)),
		cast(numCreditCardTypeId as VARCHAR(255)),
		cast(tintBareBy as VARCHAR(255))
   FROM COACreditCardCharge
   WHERE (numTransChargeID = v_numTransChargeID OR v_numTransChargeID = 0)
   AND (numCreditCardTypeId = v_numCreditCardTypeId OR v_numCreditCardTypeId = 0)
   AND numDomainID = v_numDomainID;
   RETURN;
END; $$;


/****** Object:  StoredProcedure [dbo].[USP_GetCOARelationship]    Script Date: 09/25/2009 16:13:43 ******/













