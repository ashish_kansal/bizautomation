-- Function definition script Getproname for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION Getproname(v_numProid NUMERIC(9,0))
RETURNS VARCHAR(100) LANGUAGE plpgsql
   AS $$
   DECLARE
   v_Getproname  VARCHAR(100);
BEGIN
   select   vcProjectName INTO v_Getproname from ProjectsMaster where numOppId = v_numProid;
   RETURN v_Getproname;
END; $$;

