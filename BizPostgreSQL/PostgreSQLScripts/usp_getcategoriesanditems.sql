-- Stored procedure definition script USP_GetCategoriesAndItems for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_GetCategoriesAndItems(v_numDomainID NUMERIC(9,0) DEFAULT 0,    
v_numCategoryID NUMERIC(9,0) DEFAULT NULL,    
v_numWareHouseID NUMERIC(9,0) DEFAULT 0,  
v_str VARCHAR(100) DEFAULT NULL, INOUT SWV_RefCur refcursor default null, INOUT SWV_RefCur2 refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_bitShowInStock  BOOLEAN;    
   v_bitShowQuantity  BOOLEAN;
BEGIN
    
--declare @tintColumns as tinyint    
   v_bitShowInStock := false;    
   v_bitShowQuantity := false;    
--set @tintColumns=1    
    
--,@tintColumns=isnull(tintItemColumns,1)    
   select   coalesce(bitShowInStock,0), coalesce(bitShowQOnHand,0) INTO v_bitShowInStock,v_bitShowQuantity from eCommerceDTL where numDomainID = v_numDomainID;    
    
    
   if v_numCategoryID > 0 then

      open SWV_RefCur for
      select C1.numCategoryID,C1.vcCategoryName from Category C1
      where C1.numDepCategory = v_numCategoryID and numDomainID = v_numDomainID;
      open SWV_RefCur2 for
      select X.*,(case when charItemType <> 'S' then(Case when v_bitShowInStock = true then(Case when numWareHouseID is null then '<font color=red>Out Of Stock</font>'  when bitAllowBackOrder = true then 'In Stock' else(Case when X.OnHand > 0 then 'In Stock' ||(Case when v_bitShowQuantity = true then '(' || CAST(X.OnHand AS VARCHAR(20)) || ')' else '' end)
               else '<font color=red>Out Of Stock</font>' end) end)    else '' end) else '' end) as InStock
      from(select numItemCode,vcItemName,coalesce(Sum(numOnHand),0) as OnHand,charItemType,bitAllowBackOrder,
 --kishan
 (SELECT  II.vcPathForTImage
            FROM ItemImages II
            WHERE II.numItemCode = Item.numItemCode
            AND II.bitDefault = true
            AND II.numDomainId = v_numDomainID LIMIT 1)
         AS vcPathForTImage,
  txtItemDesc,
  numWareHouseID  from Item
         join ItemCategory
         on numItemCode = numItemID
         left join  WareHouseItems W
         on W.numItemID = numItemCode  and numWareHouseID = v_numWareHouseID
         where numCategoryID = v_numCategoryID  and Item.numDomainID = v_numDomainID     AND W.numDomainID = v_numDomainID
         group by numItemCode,vcItemName,charItemType,bitAllowBackOrder,txtItemDesc,numWareHouseID) X;
   else
      open SWV_RefCur for
      select X.*,(case when charItemType <> 'S' then(Case when v_bitShowInStock = true then(Case when numWareHouseID is null then '<font color=red>Out Of Stock</font>'  when bitAllowBackOrder = true then 'In Stock' else(Case when X.OnHand > 0 then 'In Stock' ||(Case when v_bitShowQuantity = true then '(' || CAST(X.OnHand AS VARCHAR(20)) || ')' else '' end)
               else '<font color=red>Out Of Stock</font>' end) end)    else '' end) else '' end) as InStock
      from(select numItemCode,vcItemName,coalesce(Sum(numOnHand),0) as OnHand,charItemType,bitAllowBackOrder,
 --kishan
 (SELECT  II.vcPathForTImage
            FROM ItemImages II
            WHERE II.numItemCode = Item.numItemCode
            AND II.bitDefault = true
            AND II.numDomainId = v_numDomainID LIMIT 1)
         AS
         vcPathForTImage,
 txtItemDesc,
 numWareHouseID
         from Item
         left join  WareHouseItems W
         on W.numItemID = numItemCode  and numWareHouseID = v_numWareHouseID
         where Item.numDomainID = v_numDomainID     AND W.numDomainID = v_numDomainID  and  (vcItemName ilike '%' || coalesce(v_str,'') || '%' or txtItemDesc ilike '%' || coalesce(v_str,'') || '%')
         group by numItemCode,vcItemName,charItemType,bitAllowBackOrder,txtItemDesc,numWareHouseID) X;
   end if;
   RETURN;
END; $$;      
   
    
    
--select @tintColumns


