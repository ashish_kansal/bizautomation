-- Function definition script fn_GetSubStgDtlsbySalesStgID for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION fn_GetSubStgDtlsbySalesStgID(v_numOppID NUMERIC, v_numOppStageID NUMERIC,v_tintType SMALLINT)
RETURNS VARCHAR(100) LANGUAGE plpgsql
   AS $$
--Thsi function will take the ID fo rthe Territory or the List Details and return the corresponding name.
   DECLARE
   v_vcRetValue  VARCHAR(100);
   SWV_RowCount INTEGER;
BEGIN
   v_vcRetValue := '';

   select COUNT(*) INTO v_vcRetValue from  OpportunitySubStageDetails where numOppId = v_numOppID and numStageDetailsId = v_numOppStageID;
	
   GET DIAGNOSTICS SWV_RowCount = ROW_COUNT;
   if SWV_RowCount > 0 then 
      v_vcRetValue := CAST(1 AS VARCHAR(100));
   else 
      v_vcRetValue := CAST(0 AS VARCHAR(100));
   end if;
   RETURN v_vcRetValue;
END; $$;

