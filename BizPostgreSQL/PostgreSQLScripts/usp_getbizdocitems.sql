-- FUNCTION: public.usp_getbizdocitems(numeric, numeric, numeric, refcursor)

-- DROP FUNCTION public.usp_getbizdocitems(numeric, numeric, numeric, refcursor);

CREATE OR REPLACE FUNCTION public.usp_getbizdocitems(
	v_numoppid numeric,
	v_numoppbizdocid numeric,
	v_numdomainid numeric,
	INOUT swv_refcur refcursor DEFAULT NULL::refcursor)
    RETURNS refcursor
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
AS $BODY$
BEGIN
   open SWV_RefCur for SELECT    OBDI.numOppBizDocID,
				OBDI.numOppItemID,
				OBDI.numItemCode,
				OBDI.numUnitHour,
				OBDI.monPrice,
				OBDI.monTotAmount,
				OBDI.vcItemDesc,
				OBDI.numWarehouseItmsID,
				OBDI.vcType,
				OBDI.vcAttributes,
				OBDI.bitDropShip,
				OBDI.bitDiscountType,
				OBDI.fltDiscount,
				OBDI.monTotAmtBefDiscount,
				OBDI.vcNotes,
				OBDI.vcTrackingNo,
				OBDI.bitEmbeddedCost,
				OBDI.monEmbeddedCost,
				OBDI.monShipCost,
				OBDI.vcShippingMethod,
				OBDI.dtDeliveryDate,
				OBDI.dtRentalStartDate,
				OBDI.dtRentalReturnDate,
				OBDI.tintTrackingStatus,
				coalesce(OI.vcItemDesc,'') AS vcItemDesc,
				coalesce(OI.vcItemName,'') AS vcItemName
   FROM OpportunityBizDocItems OBDI
   JOIN OpportunityItems OI ON OI.numoppitemtCode = CAST(OBDI.numOppItemID AS NUMERIC)
   JOIN OpportunityMaster OM ON OM.numOppId = OI.numOppId
   WHERE 1 = 1
   AND OM.numOppId = v_numOppID
   AND numOppBizDocID = v_numOppBizDocID;
   RETURN;
END;
$BODY$;

ALTER FUNCTION public.usp_getbizdocitems(numeric, numeric, numeric, refcursor)
    OWNER TO postgres;
