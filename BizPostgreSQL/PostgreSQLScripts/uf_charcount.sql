-- Function definition script uf_charCount for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION uf_charCount(v_string VARCHAR(8000), v_character CHAR(1))
RETURNS SMALLINT LANGUAGE plpgsql
   AS $$
   DECLARE
   v_stringtrunc  VARCHAR(8000);
BEGIN
   v_stringtrunc := REPLACE(LOWER(v_string),LOWER(v_character),''); -- remove the specified character 
   RETURN(LENGTH(v_string) -LENGTH(v_stringtrunc)); -- return the difference in length, this is the char count
END; $$;
/****** Object:  UserDefinedFunction [dbo].[ufn_GetDaysInYear]    Script Date: 07/26/2008 18:13:15 ******/

