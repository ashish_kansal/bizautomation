-- Trigger definition script StagePercentageDetailsTaskTimeLog_Update for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021

CREATE OR REPLACE FUNCTION StagePercentageDetailsTaskTimeLog_Update_TrFunc()
RETURNS TRIGGER LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numDomainID  NUMERIC(18,0);
   v_numContractsLogId  NUMERIC(18,0);
   v_numContactID  NUMERIC(18,0);
   v_numUsedTime  INTEGER;
   v_numTaskID  NUMERIC(18,0);
BEGIN
   BEGIN
      IF EXISTS(SELECT
      CL.numContractId
      FROM
      ContractsLog CL
      INNER JOIN
      Contracts C
      ON
      CL.numContractId = C.numContractId
      INNER JOIN
      new_table
      ON
      C.numDomainId = new_table.numDomainID
      AND CL.numReferenceId = new_table.numTaskID
      AND vcDetails = '1') then
         select   new_table.numDomainID, numContractsLogId, CL.numContractId, numUsedTime, new_table.numTaskID INTO v_numDomainID,v_numContractsLogId,v_numContactID,v_numUsedTime,v_numTaskID FROM
         ContractsLog CL
         INNER JOIN
         Contracts C
         ON
         CL.numContractId = C.numContractId
         INNER JOIN
         new_table
         ON
         C.numDomainId = new_table.numDomainID
         AND CL.numReferenceId = new_table.numTaskID
         AND vcDetails = '1';
         IF coalesce((SELECT  tintAction FROM StagePercentageDetailsTaskTimeLog WHERE numTaskId = v_numTaskID ORDER BY ID DESC LIMIT 1),0) = 4 then
		
            UPDATE Contracts SET timeUsed = coalesce(timeUsed,0) -coalesce(v_numUsedTime,0),dtmModifiedOn = TIMEZONE('UTC',now()) WHERE numContractId = v_numContactID;

			-- GET UPDATED TIME
            v_numUsedTime := CAST(GetTimeSpendOnTaskByProject(v_numDomainID,v_numTaskID,2::SMALLINT) AS INTEGER);
            UPDATE ContractsLog SET numUsedTime =(v_numUsedTime::bigint*60) WHERE numContractsLogId = v_numContractsLogId;
         ELSE
            UPDATE Contracts SET timeUsed = coalesce(timeUsed,0) -coalesce(v_numUsedTime,0),dtmModifiedOn = TIMEZONE('UTC',now()) WHERE numContractId = v_numContactID;
            DELETE FROM ContractsLog WHERE numContractsLogId = v_numContractsLogId;
         end if;
      end if;
   END;
   RETURN NULL;
   END; $$;
CREATE TRIGGER StagePercentageDetailsTaskTimeLog_Update
AFTER UPDATE
ON StagePercentageDetailsTaskTimeLog
REFERENCING NEW TABLE AS new_table
FOR EACH ROW
   EXECUTE PROCEDURE StagePercentageDetailsTaskTimeLog_Update_TrFunc();


