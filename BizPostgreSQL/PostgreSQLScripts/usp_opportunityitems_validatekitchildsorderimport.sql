-- Stored procedure definition script USP_OpportunityItems_ValidateKitChildsOrderImport for PostgreSQL
CREATE OR REPLACE FUNCTION USP_OpportunityItems_ValidateKitChildsOrderImport(v_numDomainID NUMERIC(18,0),
    v_numItemCode NUMERIC(18,0),
    v_vcKitChilds TEXT,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_vcKitChildItemCodes  TEXT DEFAULT '';

	

   v_bitKitParent  BOOLEAN;
   v_numKitItemCode  NUMERIC(18,0);
   v_numKitChildItemCode  NUMERIC(18,0);
   v_vcKitItemName  TEXT;
   v_vcKitChildItemName  TEXT;

	
   v_posComma  INTEGER;
   v_strKeyVal  TEXT;
   v_i  INTEGER DEFAULT 1;
   v_iCount  INTEGER;
   v_vcTempKits  TEXT;
   v_vcTempKitChilds  TEXT;
   v_vcTempKitItemName  TEXT;
   v_vcTempKitChildItemName  TEXT;
   v_iPOSComma  INTEGER;
BEGIN
   BEGIN
      CREATE TEMP SEQUENCE tt_TempSelectedItems_seq INCREMENT BY 1 START WITH 1;
      EXCEPTION WHEN OTHERS THEN
         NULL;
   END;
   DROP TABLE IF EXISTS tt_TEMPSELECTEDITEMS CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPSELECTEDITEMS
   (
      ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1),
      vcKitItemName VARCHAR(300),
      numKitItemCode NUMERIC(18,0),
      vcKitChildItemName VARCHAR(300),
      numKitChildItemCode NUMERIC(18,0)
   );
   select   coalesce(bitKitParent,false) INTO v_bitKitParent FROM Item WHERE numItemCode = v_numItemCode;

   IF coalesce(v_bitKitParent,false) = true AND(SELECT
   COUNT(*)
   FROM
   ItemDetails
   INNER JOIN
   Item
   ON
   ItemDetails.numChildItemID = Item.numItemCode
   WHERE
   ItemDetails.numItemKitID = v_numItemCode
   AND coalesce(Item.bitKitParent,false) = true) > 0 then
      IF LENGTH(coalesce(v_vcKitChilds,'')) > 0 then
         BEGIN
            CREATE TEMP SEQUENCE tt_TempKits_seq INCREMENT BY 1 START WITH 1;
            EXCEPTION WHEN OTHERS THEN
               NULL;
         END;
         DROP TABLE IF EXISTS tt_TEMPKITS CASCADE;
         CREATE TEMPORARY TABLE tt_TEMPKITS
         (
            ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1),
            vcKit TEXT
         );
         v_vcKitChilds := RTRIM(v_vcKitChilds);
         v_posComma := coalesce(POSITION(substring(v_vcKitChilds from E'#\\,#') IN v_vcKitChilds),
         0);
         IF v_posComma > 0 then
			
            WHILE v_posComma > 0 LOOP
               v_strKeyVal := LTRIM(RTRIM(SUBSTR(v_vcKitChilds,1,v_posComma -1)));
               INSERT INTO tt_TEMPKITS(vcKit) VALUES(v_strKeyVal);
					
               v_vcKitChilds := SUBSTR(v_vcKitChilds,v_posComma+3,LENGTH(v_vcKitChilds) -v_posComma);
               v_posComma := coalesce(POSITION(substring(v_vcKitChilds from E'#\\,#') IN v_vcKitChilds),
               0);
            END LOOP;
            INSERT INTO tt_TEMPKITS(vcKit) VALUES(v_vcKitChilds);
         ELSE
            INSERT INTO tt_TEMPKITS(vcKit) VALUES(v_vcKitChilds);
         end if;
         select   COUNT(*) INTO v_iCount FROM tt_TEMPKITS;
         WHILE v_i <= v_iCount LOOP
            select   coalesce(vcKit,cast(0 as TEXT)) INTO v_vcTempKits FROM tt_TEMPKITS WHERE ID = v_i;
            v_posComma := coalesce(POSITION(substring(v_vcTempKits from '#:#') IN v_vcTempKits),
            0);
            IF v_posComma > 0 then
				
               v_vcTempKitItemName := coalesce(LTRIM(RTRIM(SUBSTR(v_vcTempKits,1,v_posComma -1))),'');
               v_vcTempKitChilds := coalesce(SUBSTR(v_vcTempKits,v_posComma+3,LENGTH(v_vcTempKits) -v_posComma),'');
               v_iPOSComma := coalesce(POSITION(substring(v_vcTempKitChilds from '#~#') IN v_vcTempKitChilds),0);
               IF v_iPOSComma > 0 then
					
                  WHILE v_iPOSComma > 0 LOOP
                     v_strKeyVal := LTRIM(RTRIM(SUBSTR(v_vcTempKitChilds,1,v_iPOSComma -1)));
                     INSERT INTO tt_TEMPSELECTEDITEMS(vcKitItemName,vcKitChildItemName) VALUES(CAST(v_vcTempKitItemName AS VARCHAR(300)),CAST(v_strKeyVal AS VARCHAR(300)));
							
                     v_vcTempKitChilds := SUBSTR(v_vcTempKitChilds,v_iPOSComma+3,LENGTH(v_vcTempKitChilds) -v_iPOSComma);
                     v_iPOSComma := coalesce(POSITION(substring(v_vcTempKitChilds from '#~#') IN v_vcTempKitChilds),0);
                  END LOOP;
                  INSERT INTO tt_TEMPSELECTEDITEMS(vcKitItemName,vcKitChildItemName) VALUES(CAST(v_vcTempKitItemName AS VARCHAR(300)),CAST(v_vcTempKitChilds AS VARCHAR(300)));
               ELSE
                  INSERT INTO tt_TEMPSELECTEDITEMS(vcKitItemName,vcKitChildItemName) VALUES(CAST(v_vcTempKitItemName AS VARCHAR(300)),CAST(v_vcTempKitChilds AS VARCHAR(300)));
               end if;
               v_vcTempKits := SUBSTR(v_vcTempKits,v_posComma+3,LENGTH(v_vcTempKits) -v_posComma);
            ELSE
               RAISE EXCEPTION 'INVALID_INCLISION_DETAIL';
               RETURN;
            end if;
            v_i := v_i::bigint+1;
         END LOOP;
      ELSE
         IF(SELECT
         COUNT(*)
         FROM
         ItemDetails
         INNER JOIN
         Item
         ON
         ItemDetails.numChildItemID = Item.numItemCode
         WHERE
         ItemDetails.numItemKitID = v_numItemCode
         AND coalesce(Item.bitKitParent,false) = true
         AND coalesce(Item.bitKitSingleSelect,false) = true) > 0 then
			
            RAISE EXCEPTION 'INVALID_INCLISION_DETAIL';
            RETURN;
         end if;
      end if;
      v_i := 1;
      select   COUNT(*) INTO v_iCount FROM tt_TEMPSELECTEDITEMS;
      WHILE v_i <= v_iCount LOOP
         select   coalesce(vcKitItemName,''), coalesce(vcKitChildItemName,'') INTO v_vcKitItemName,v_vcKitChildItemName FROM tt_TEMPSELECTEDITEMS WHERE ID = v_i;
         IF(SELECT COUNT(*) FROM Item WHERE numDomainID = v_numDomainID AND vcItemName = v_vcKitItemName) = 0 then
			
            RAISE EXCEPTION 'KIT_ITEM_NOT_FOUND';
            RETURN;
         ELSEIF(SELECT COUNT(*) FROM Item WHERE numDomainID = v_numDomainID AND vcItemName = v_vcKitItemName) > 1
         then
			
            RAISE EXCEPTION 'MULTIPLE_KIT_ITEM_FOUND';
            RETURN;
         ELSEIF(SELECT COUNT(*) FROM Item WHERE numDomainID = v_numDomainID AND vcItemName = v_vcKitChildItemName) = 0
         then
			
            RAISE EXCEPTION 'KIT_CHILD_ITEM_NOT_FOUND';
            RETURN;
         ELSEIF(SELECT COUNT(*) FROM Item WHERE numDomainID = v_numDomainID AND vcItemName = v_vcKitChildItemName) > 1
         then
			
            RAISE EXCEPTION 'MULTIPLE_KIT_CHILD_ITEM_FOUND';
            RETURN;
         ELSE
            SELECT numItemCode INTO v_numKitItemCode FROM Item WHERE numDomainID = v_numDomainID AND vcItemName = v_vcKitItemName;
            SELECT numItemCode INTO v_numKitChildItemCode FROM Item WHERE numDomainID = v_numDomainID AND vcItemName = v_vcKitChildItemName;
            IF NOT EXISTS(SELECT
            numItemDetailID
            FROM
            ItemDetails
            INNER JOIN
            Item
            ON
            ItemDetails.numChildItemID = Item.numItemCode
            WHERE
            numItemKitID = v_numItemCode
            AND numChildItemID = v_numKitItemCode
            AND coalesce(bitKitParent,false) = true) then
				
               RAISE EXCEPTION 'INVALID_KIT_ITEM';
               RETURN;
            end if;
            IF NOT EXISTS(SELECT
            numItemDetailID
            FROM
            ItemDetails
            WHERE
            numItemKitID = v_numKitItemCode
            AND numChildItemID = v_numKitChildItemCode) then
				
               RAISE EXCEPTION 'INVALID_KIT_CHILD_ITEM';
               RETURN;
            end if;
            UPDATE
            tt_TEMPSELECTEDITEMS
            SET
            numKitItemCode = v_numKitItemCode,numKitChildItemCode = v_numKitChildItemCode
            WHERE
            ID = v_i;
         end if;
         v_i := v_i::bigint+1;
      END LOOP;
      IF EXISTS(SELECT
      numItemDetailID
      FROM
      ItemDetails
      INNER JOIN
      Item
      ON
      ItemDetails.numChildItemID = Item.numItemCode
      WHERE
      ItemDetails.numItemKitID = v_numItemCode
      AND coalesce(Item.bitKitParent,false) = true
      AND coalesce(Item.bitKitSingleSelect,false) = true
      AND numChildItemID NOT IN(SELECT numKitItemCode FROM tt_TEMPSELECTEDITEMS)) then
		
         RAISE EXCEPTION 'CHILD_KIT_VALUES_NOT_PROVIDED';
         RETURN;
      end if;
      v_vcKitChildItemCodes := OVERLAY((SELECT ',' || CONCAT(numKitItemCode,'-',numKitChildItemCode) FROM tt_TEMPSELECTEDITEMS T1) placing REPEAT(' ',0) from 1 for 1);
   ELSE
      v_vcKitChildItemCodes := '';
   end if;

   open SWV_RefCur for SELECT v_vcKitChildItemCodes;
END; $$;












