-- Stored procedure definition script USP_OpportunityMaster_GetARContact for PostgreSQL
CREATE OR REPLACE FUNCTION USP_OpportunityMaster_GetARContact(v_numDomainID NUMERIC(18,0)
	,v_numOppID NUMERIC(18,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT 
   ACI.numContactId
		,ACI.vcEmail
   FROM
   OpportunityMaster OM
   INNER JOIN
   Domain D
   ON
   OM.numDomainId = D.numDomainId
   INNER JOIN
   AdditionalContactsInformation ACI
   ON
   OM.numDivisionId = ACI.numDivisionId
   WHERE
   OM.numDomainId = v_numDomainID
   AND OM.numOppId = v_numOppID
   AND 1 =(CASE WHEN coalesce(D.numARContactPosition,0) > 0 AND ACI.vcPosition = D.numARContactPosition THEN 1 ELSE 0 END) LIMIT 1;
END; $$;












