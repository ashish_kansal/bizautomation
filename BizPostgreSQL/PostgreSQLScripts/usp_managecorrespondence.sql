-- Stored procedure definition script USP_ManageCorrespondence for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ManageCorrespondence(INOUT v_numCorrespondenceID NUMERIC DEFAULT 0 ,
    v_numCommID NUMERIC DEFAULT 0,
    v_numEmailHistoryID NUMERIC DEFAULT 0,
    v_tintCorrType SMALLINT DEFAULT NULL,
    v_numDomainID NUMERIC DEFAULT NULL,
    v_numOpenRecordID NUMERIC DEFAULT NULL,
	v_monMRItemAmount DECIMAL(20,5) DEFAULT 0)
LANGUAGE plpgsql
   AS $$
BEGIN


   IF v_numCommID = 0 then
      v_numCommID := NULL;
   end if;
   IF v_numEmailHistoryID = 0 then
      v_numEmailHistoryID := NULL;
   end if;
   IF v_numCorrespondenceID = 0 then
        
      INSERT  INTO Correspondence(numCommID,
                      numEmailHistoryID,
                      tintCorrType,
                      numDomainID,
					  numOpenRecordID,
					  monMRItemAmount)
            VALUES(v_numCommID,
                      v_numEmailHistoryID,
                      v_tintCorrType,
                      v_numDomainID,
					  v_numOpenRecordID,
					  v_monMRItemAmount);
            
      v_numCorrespondenceID := CURRVAL('Correspondence_seq');
   ELSE
      UPDATE  Correspondence
      SET     numCommID = v_numCommID,numEmailHistoryID = v_numEmailHistoryID,tintCorrType = v_tintCorrType,
      numOpenRecordID = v_numOpenRecordID,monMRItemAmount = v_monMRItemAmount
      WHERE   numCorrespondenceID = v_numCorrespondenceID;
   end if;

   UPDATE OpportunityBizDocs SET bitIsEmailSent = true WHERE numoppid = v_numOpenRecordID;
/****** Object:  StoredProcedure [dbo].[USP_ManageCreditBalanceHistory]    Script Date: 01/22/2009 01:41:51 ******/
   RETURN;
END; $$;


