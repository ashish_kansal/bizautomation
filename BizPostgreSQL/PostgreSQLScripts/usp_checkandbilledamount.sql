-- Stored procedure definition script USP_CheckAndBilledAmount for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_CheckAndBilledAmount(v_numDivisionID			NUMERIC(18,0),
	v_numOppBizDocId			NUMERIC(18,0),
	v_numBillID				NUMERIC(18,0),
	v_numAmount				DECIMAL(20,5),
	v_numDomainID			NUMERIC(18,0), INOUT SWV_RefCur refcursor)
LANGUAGE plpgsql
   AS $$
BEGIN
   IF v_numBillID > 0 then
	
      open SWV_RefCur for
      SELECT  CASE WHEN coalesce(BH.monAmountDue,0) = v_numAmount THEN 1
      WHEN coalesce(BH.monAmountDue,0) >= coalesce(SUM(monAmount),0)+v_numAmount THEN 2
      WHEN coalesce(BH.monAmountDue,0) < coalesce(SUM(monAmount),0)+v_numAmount THEN 3
      END AS IsPaidInFull
      FROM BillPaymentDetails AS BPD
      JOIN BillPaymentHeader AS BPH ON BPH.numBillPaymentID = BPD.numBillPaymentID
      JOIN BillHeader AS BH ON BH.numBillID = BPD.numBillID
      WHERE BH.numBillID = v_numBillID
      AND BH.numDomainId = v_numDomainID
      AND BH.numDivisionId = v_numDivisionID
      GROUP BY monAmountDue;
   ELSEIF v_numOppBizDocId > 0
   then
	
      open SWV_RefCur for
      SELECT  CASE WHEN coalesce(OBD.monDealAmount,0) = v_numAmount THEN 1
      WHEN coalesce(OBD.monDealAmount,0) = coalesce(SUM(BPD.monAmount),0)+v_numAmount THEN 2
      WHEN coalesce(OBD.monDealAmount,0) > coalesce(SUM(BPD.monAmount),0)+v_numAmount THEN 3
      WHEN coalesce(OBD.monDealAmount,0) < coalesce(SUM(BPD.monAmount),0)+v_numAmount THEN 4
      END AS IsPaidInFull
      FROM BillPaymentDetails AS BPD
      JOIN BillPaymentHeader AS BPH ON BPH.numBillPaymentID = BPD.numBillPaymentID
      JOIN OpportunityBizDocs AS OBD ON OBD.numOppBizDocsId = BPD.numOppBizDocsID
      JOIN OpportunityMaster AS OM ON OM.numOppId = OBD.numoppid AND BPH.numDomainId = OM.numDomainId
      WHERE OBD.numOppBizDocsId = v_numOppBizDocId
      AND OM.numDomainId = v_numDomainID
      AND OM.numDivisionId = v_numDivisionID
      GROUP BY OBD.monDealAmount;
   end if;
   RETURN;

END; $$;



