-- Function definition script GetCommissionContactItemList for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION GetCommissionContactItemList(v_numRuleID NUMERIC,
      v_tintMode SMALLINT)
RETURNS TEXT LANGUAGE plpgsql
   AS $$
   DECLARE
   v_ItemList  TEXT;
   v_tintAssignTo  SMALLINT;
BEGIN
   IF v_tintMode = 0 then
      select   tintAssignTo INTO v_tintAssignTo FROM CommissionRules WHERE numComRuleID = v_numRuleID;
      IF v_tintAssignTo = 3 then
			
         select string_agg(CONCAT(D.vcPartnerCode,'-',C.vcCompanyName,' (Partner)'),', ') INTO v_ItemList FROM
         CommissionRuleContacts CC
         INNER JOIN
         DivisionMaster D
         ON
         CC.numValue = D.numDivisionID
         INNER JOIN
         CompanyInfo C
         ON
         D.numCompanyID = C.numCompanyId WHERE
         CC.numComRuleId = v_numRuleID;
      ELSE
         select string_agg(CASE CC.bitCommContact WHEN true THEN(SELECT vcCompanyName FROM DivisionMaster D join CompanyInfo C on C.numCompanyId = D.numCompanyID  where D.numDivisionID = CC.numValue) || '(Commission Contact)' ELSE fn_GetContactName(numValue) END,', ') INTO v_ItemList FROM    CommissionRuleContacts CC WHERE   CC.numComRuleId = v_numRuleID;
      end if;
   end if;
        
   IF v_tintMode = 1 then
        
      select string_agg(vcItemName,', ') INTO v_ItemList FROM    CommissionRuleItems CI
      INNER JOIN Item I ON I.numItemCode = CI.numValue WHERE   CI.numComRuleID = v_numRuleID    LIMIT 10;
   end if;
      
   IF v_tintMode = 2 then
        
      select string_agg(vcData,', ') INTO v_ItemList FROM    CommissionRuleItems CI
      INNER JOIN Listdetails LI ON LI.numListItemID = CI.numValue WHERE   CI.numComRuleID = v_numRuleID    LIMIT 10;
   end if;
   IF v_tintMode = 3 then
        
      select string_agg(C.vcCompanyName,', ') INTO v_ItemList FROM    CommissionRuleOrganization PD
      INNER JOIN DivisionMaster DM ON DM.numDivisionID = PD.numValue
      LEFT OUTER JOIN CompanyInfo C ON C.numCompanyId = DM.numCompanyID
      LEFT OUTER JOIN CommissionRules PB ON PB.numComRuleID = PD.numComRuleID WHERE   PB.tintComOrgType = 1
      AND PB.numComRuleID = v_numRuleID    LIMIT 10;
   end if;
        
   IF v_tintMode = 4 then
        
      select string_agg((LI.vcData || '/' || LI1.vcData),', ') INTO v_ItemList FROM    CommissionRuleOrganization PD
      INNER JOIN Listdetails LI ON LI.numListItemID = PD.numValue
      Inner JOIN Listdetails LI1 ON LI1.numListItemID = PD.numProfile
      LEFT OUTER JOIN CommissionRules PB ON PB.numComRuleID = PD.numComRuleID WHERE   PB.tintComOrgType = 2
      AND PB.numComRuleID = v_numRuleID    LIMIT 10;
   end if;
        
   RETURN coalesce(v_ItemList,'');
END; $$;

