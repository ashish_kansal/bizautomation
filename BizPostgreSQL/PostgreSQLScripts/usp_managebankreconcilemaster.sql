DROP FUNCTION IF EXISTS USP_ManageBankReconcileMaster;

CREATE OR REPLACE FUNCTION USP_ManageBankReconcileMaster(v_tintMode SMALLINT, 
    v_numDomainID NUMERIC(18,0),
    v_numReconcileID NUMERIC(18,0),
    v_numCreatedBy NUMERIC(18,0),
    v_dtStatementDate TIMESTAMP,
    v_monBeginBalance DECIMAL(20,5),
    v_monEndBalance DECIMAL(20,5),
    v_numChartAcntId NUMERIC(18,0),
    v_monServiceChargeAmount DECIMAL(20,5),
    v_numServiceChargeChartAcntId NUMERIC(18,0),
    v_dtServiceChargeDate TIMESTAMP,
    v_monInterestEarnedAmount DECIMAL(20,5),
    v_numInterestEarnedChartAcntId NUMERIC(18,0),
    v_dtInterestEarnedDate TIMESTAMP,
    v_bitReconcileComplete BOOLEAN DEFAULT false,
	v_vcFileName VARCHAR(300) DEFAULT '',
	v_vcFileData TEXT DEFAULT '', INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql

--Validation of closed financial year
   AS $$
   DECLARE
   v_numCreditAmt  DECIMAL(20,5);
   v_numDebitAmt  DECIMAL(20,5);
   v_hDocItem  INTEGER;
BEGIN
   PERFORM USP_ValidateFinancialYearClosingDate(v_numDomainID,v_dtStatementDate);
	
   IF v_tintMode = 1 then --SELECT
      select   coalesce(SUM(numCreditAmt),0), coalesce(SUM(numDebitAmt),0) INTO v_numCreditAmt,v_numDebitAmt FROM
      General_Journal_Details WHERE
      numDomainId = v_numDomainID
      AND numReconcileID = v_numReconcileID
      AND numChartAcntId =(SELECT numChartAcntId FROM BankReconcileMaster WHERE  numDomainID = v_numDomainID AND numReconcileID = v_numReconcileID)
      AND numJournalId NOT IN(SELECT numJOurnal_Id FROM General_Journal_Header WHERE numDomainId = v_numDomainID AND numReconcileID = v_numReconcileID AND coalesce(bitReconcileInterest,false) = false);
      open SWV_RefCur for
      SELECT
      numReconcileID
			,numDomainID
			,numCreatedBy
			,dtCreatedDate
			,dtStatementDate
			,monBeginBalance
			,monEndBalance
			,numChartAcntId
			,monServiceChargeAmount
			,numServiceChargeChartAcntId
			,dtServiceChargeDate
			,monInterestEarnedAmount
			,numInterestEarnedChartAcntId
			,dtInterestEarnedDate
			,bitReconcileComplete
			,dtReconcileDate
			,v_numCreditAmt AS Payment
			,v_numDebitAmt AS Deposit
			,fn_GetContactName(coalesce(numCreatedBy,0)) AS vcReconciledby
			,fn_GetChart_Of_AccountsName(coalesce(numChartAcntId,0)) AS vcAccountName,coalesce(vcFileName,'') AS vcFileName
      FROM
      BankReconcileMaster
      WHERE
      numDomainID = v_numDomainID
      AND numReconcileID = v_numReconcileID;
   ELSEIF v_tintMode = 2
   then --Insert/Update
	
      IF v_numReconcileID > 0 then --Update
			
         UPDATE BankReconcileMaster
         SET    dtStatementDate = v_dtStatementDate,monBeginBalance = v_monBeginBalance, 
         monEndBalance = v_monEndBalance,numChartAcntId = v_numChartAcntId,monServiceChargeAmount = v_monServiceChargeAmount, 
         numServiceChargeChartAcntId = v_numServiceChargeChartAcntId,
         dtServiceChargeDate = v_dtServiceChargeDate, 
         monInterestEarnedAmount = v_monInterestEarnedAmount,numInterestEarnedChartAcntId = v_numInterestEarnedChartAcntId,
         dtInterestEarnedDate = v_dtInterestEarnedDate
         WHERE  numDomainID = v_numDomainID AND numReconcileID = v_numReconcileID;
      ELSE
         INSERT INTO BankReconcileMaster(numDomainID, numCreatedBy, dtCreatedDate, dtStatementDate, monBeginBalance, monEndBalance, numChartAcntId, monServiceChargeAmount, numServiceChargeChartAcntId,dtServiceChargeDate,monInterestEarnedAmount, numInterestEarnedChartAcntId,dtInterestEarnedDate,vcFileName)
         SELECT v_numDomainID, v_numCreatedBy, TIMEZONE('UTC',now()), v_dtStatementDate, v_monBeginBalance, v_monEndBalance, v_numChartAcntId, v_monServiceChargeAmount, v_numServiceChargeChartAcntId,v_dtServiceChargeDate,v_monInterestEarnedAmount, v_numInterestEarnedChartAcntId,v_dtInterestEarnedDate,v_vcFileName;
         v_numReconcileID := CURRVAL('BankReconcileMaster_seq');
         IF LENGTH(v_vcFileName) > 0 then
  
            INSERT INTO BankReconcileFileData(numDomainID,
						numReconcileID,
						dtEntryDate,
						vcReference,
						fltAmount,
						vcPayee,
						vcDescription)
            SELECT
            v_numDomainID,
						v_numReconcileID,
						dtEntryDate,
						vcReference,
						fltAmount,
						vcPayee,
						vcDescription
            FROM
			XMLTABLE
			(
				'BankStatement/Trasactions'
				PASSING 
					CAST(v_vcFileData AS XML)
				COLUMNS
					id FOR ORDINALITY,
					dtEntryDate DATE PATH 'dtEntryDate',
					vcReference VARCHAR(500) PATH 'vcReference',
					fltAmount DOUBLE PRECISION PATH 'fltAmount',
					vcPayee VARCHAR(1000) PATH 'vcPayee',
					vcDescription VARCHAR(1000) PATH 'vcDescription'
			) AS X;
         end if;
      end if;
      open SWV_RefCur for
      SELECT numReconcileID, numDomainID, numCreatedBy, dtCreatedDate, dtStatementDate, monBeginBalance, monEndBalance, numChartAcntId, monServiceChargeAmount, numServiceChargeChartAcntId, monInterestEarnedAmount, numInterestEarnedChartAcntId
      FROM   BankReconcileMaster WHERE  numDomainID = v_numDomainID AND numReconcileID = v_numReconcileID;
   ELSEIF v_tintMode = 3
   then --Delete
	 
	    --Delete Service Charge,Interest Earned and Adjustment entry
      DELETE FROM General_Journal_Details WHERE numJournalId IN(SELECT numJOurnal_Id FROM General_Journal_Header WHERE numDomainId = v_numDomainID AND coalesce(numReconcileID,0) = v_numReconcileID);
      DELETE FROM General_Journal_Header WHERE numDomainId = v_numDomainID AND coalesce(numReconcileID,0) = v_numReconcileID;
	 
	    --Set Journal_Details entry to bitReconcile=0 & bitCleared=0
      UPDATE General_Journal_Details SET bitReconcile = false,bitCleared = false,numReconcileID = NULL WHERE numDomainId = v_numDomainID AND numReconcileID = v_numReconcileID;
      DELETE FROM BankReconcileFileData WHERE numDomainID = v_numDomainID AND numReconcileID = v_numReconcileID;
      DELETE FROM BankReconcileMaster WHERE numDomainID = v_numDomainID AND numReconcileID = v_numReconcileID;
   ELSEIF v_tintMode = 4
   then -- Select based on numChartAcntId and bitReconcileComplete
	 
      open SWV_RefCur for
      SELECT *,(SELECT coalesce(SUM(numCreditAmt),0) FROM General_Journal_Details WHERE numDomainId = v_numDomainID AND numReconcileID = BRM.numReconcileID /*AND ISNULL(bitReconcile,0)=1*/ AND numChartAcntId = BRM.numChartAcntId) AS monPayment
		,(SELECT coalesce(SUM(numDebitAmt),0) FROM General_Journal_Details WHERE numDomainId = v_numDomainID AND numReconcileID = BRM.numReconcileID /*AND ISNULL(bitReconcile,0)=1*/ AND numChartAcntId = BRM.numChartAcntId) AS monDeposit
      FROM BankReconcileMaster BRM WHERE  numDomainID = v_numDomainID AND numChartAcntId = v_numChartAcntId
      AND coalesce(bitReconcileComplete,false) = v_bitReconcileComplete ORDER BY numReconcileID DESC;
   ELSEIF v_tintMode = 5
   then -- Complete Bank Reconcile
	 
      UPDATE General_Journal_Details SET bitReconcile = true,bitCleared = false WHERE numDomainId = v_numDomainID AND numReconcileID = v_numReconcileID;
      UPDATE BankReconcileFileData SET bitReconcile = true,bitCleared = false WHERE numDomainID = v_numDomainID AND numReconcileID = v_numReconcileID AND coalesce(bitCleared,false) = true;
      UPDATE BankReconcileMaster SET bitReconcileComplete = true,dtReconcileDate = TIMEZONE('UTC',now()),numCreatedBy = v_numCreatedBy
      WHERE numDomainID = v_numDomainID AND numReconcileID = v_numReconcileID;
   ELSEIF v_tintMode = 6
   then -- Get Last added entry for numChartAcntId
	
      open SWV_RefCur for
      SELECT * FROM BankReconcileMaster BRM WHERE  numDomainID = v_numDomainID AND numChartAcntId = v_numChartAcntId
      AND coalesce(bitReconcileComplete,false) = true ORDER BY numReconcileID DESC LIMIT 1;
   ELSEIF v_tintMode = 7
   then -- Select based on numChartAcntId
	
      open SWV_RefCur for
      SELECT *,(SELECT coalesce(SUM(numCreditAmt),0) FROM General_Journal_Details WHERE numDomainId = v_numDomainID AND numReconcileID = BRM.numReconcileID AND numChartAcntId = BRM.numChartAcntId) AS monPayment
		,(SELECT coalesce(SUM(numDebitAmt),0) FROM General_Journal_Details WHERE numDomainId = v_numDomainID AND numReconcileID = BRM.numReconcileID AND numChartAcntId = BRM.numChartAcntId) AS monDeposit
      FROM BankReconcileMaster BRM WHERE  numDomainID = v_numDomainID AND numChartAcntId = v_numChartAcntId
      ORDER BY numReconcileID DESC;
   end if;
   RETURN;
END; $$;
    


