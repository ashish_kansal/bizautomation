-- Function definition script fn_GetOrderItemInventory for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION fn_GetOrderItemInventory(v_numOppID NUMERIC(18,0) DEFAULT 0)
RETURNS table
(
   numItemCode NUMERIC(18,0),
   numOnHand NUMERIC(18,0),
   numOnOrder NUMERIC(18,0),
   numOnBackOrder NUMERIC(18,0),
   numOnAllocation NUMERIC(18,0)
) LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numoppitemtCode  NUMERIC(18,0); 
   v_itemcode  NUMERIC(18,0);
   v_numWareHouseItemID  NUMERIC(18,0);
   v_onHand  NUMERIC(18,0);                          
   v_onOrder  NUMERIC(18,0);                                       
   v_onBackOrder  NUMERIC(18,0);                                            
   v_onAllocation  NUMERIC(18,0);
   SWV_RowCount INTEGER;
BEGIN
   DROP TABLE IF EXISTS tt_FN_GETORDERITEMINVENTORY_TABLEINVENTORY CASCADE;
   CREATE TEMPORARY TABLE IF NOT EXISTS  tt_FN_GETORDERITEMINVENTORY_TABLEINVENTORY
   (
      numItemCode NUMERIC(18,0),
      numOnHand NUMERIC(18,0),
      numOnOrder NUMERIC(18,0),
      numOnBackOrder NUMERIC(18,0),
      numOnAllocation NUMERIC(18,0)
   );
   select   numoppitemtCode, OI.numItemCode, coalesce(numWarehouseItmsID,0) INTO v_numoppitemtCode,v_itemcode,v_numWareHouseItemID FROM
   OpportunityItems OI
   JOIN
   OpportunityMaster OM
   ON
   OI.numOppId = OM.numOppId
   JOIN
   Item I
   ON
   OI.numItemCode = I.numItemCode WHERE
   charitemtype = 'P' AND
   coalesce(bitKitParent,false) = false AND
   coalesce(bitAssembly,false) = false AND
   coalesce(bitAsset,false) = false AND
   coalesce(bitRental,false) = false AND
   OI.numOppId = v_numOppID AND
		(bitDropShip = false OR bitDropShip IS NULL) AND
   coalesce(OI.bitWorkOrder,false) = false   ORDER BY OI.numoppitemtCode LIMIT 1;

   WHILE v_numoppitemtCode > 0 LOOP
      INSERT INTO  tt_FN_GETORDERITEMINVENTORY_TABLEINVENTORY(numItemCode,
			numOnHand,
			numOnOrder,
			numOnBackOrder,
			numOnAllocation)
      SELECT
      v_itemcode,
			coalesce(WareHouseItems.numOnHand,0),
			coalesce(WareHouseItems.numOnOrder,0),
			coalesce(WareHouseItems.numBackOrder,0),
			coalesce(WareHouseItems.numAllocation,0)
      FROM
      WareHouseItems
      WHERE
      numWareHouseItemID = v_numWareHouseItemID;
      select   numoppitemtCode, OI.numItemCode, coalesce(numWarehouseItmsID,0) INTO v_numoppitemtCode,v_itemcode,v_numWareHouseItemID FROM
      OpportunityItems OI
      JOIN OpportunityMaster OM ON OI.numOppId = OM.numOppId
      JOIN Item I ON OI.numItemCode = I.numItemCode WHERE
      charitemtype = 'P' AND
      coalesce(bitKitParent,false) = false AND
      coalesce(bitAssembly,false) = false AND
      coalesce(bitAsset,false) = false AND
      coalesce(bitRental,false) = false AND
      OI.numOppId = v_numOppID AND
				(bitDropShip = false OR bitDropShip IS NULL) AND
      coalesce(OI.bitWorkOrder,false) = false AND
      OI.numoppitemtCode > v_numoppitemtCode   ORDER BY OI.numoppitemtCode LIMIT 1;
      GET DIAGNOSTICS SWV_RowCount = ROW_COUNT;
      IF SWV_RowCount = 0 then
         v_numoppitemtCode := 0;
      end if;
   END LOOP;
   RETURN QUERY (SELECT * FROM tt_FN_GETORDERITEMINVENTORY_TABLEINVENTORY);
END; $$;

