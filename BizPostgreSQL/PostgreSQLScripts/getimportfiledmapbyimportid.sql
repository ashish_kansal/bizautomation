-- Stored procedure definition script GetImportFiledMapByImportId for PostgreSQL
CREATE OR REPLACE FUNCTION GetImportFiledMapByImportId(v_ImportFileMasterId INTEGER,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT cast(ImportFiledMapId as VARCHAR(255))
      ,cast(ImportFileMasterId as VARCHAR(255))
      ,cast(FromFiledName as VARCHAR(255))
      ,cast(ToFileFieldName as VARCHAR(255))
      ,cast(CSVFileIndex as VARCHAR(255))
   FROM ImportFileFieldMaping
   WHERE ImportFileMasterId = v_ImportFileMasterId;
END; $$;












