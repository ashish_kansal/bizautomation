-- Stored procedure definition script USP_GetEstimateShipping for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetEstimateShipping(v_numDomainID NUMERIC(18,0)
	,v_numShipVia INTEGER
	,v_numShipService INTEGER,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT
   numServiceTypeID
		,0 AS numRuleID
		,numShippingCompanyID
		,vcServiceName
		,monRate
		,intNsoftEnum
		,intFrom
		,intTo
		,fltMarkup
		,bitMarkUpType
		,bitEnabled
		,1 AS tintFixedShippingQuotesMode
		,L.vcData AS vcShippingCompanyName
		,S.vcItemClassification
		,SR.bitItemClassification
		,SR.numWareHouseID
		,SR.numSiteID
		,SR.numRuleID AS numItemClassificationRuleID
   FROM
   ShippingServiceTypes S
   LEFT JOIN
   Listdetails AS L
   ON
   S.numShippingCompanyID = L.numListItemID
   LEFT JOIN ShippingRules AS SR ON S.numRuleID = SR.numRuleID
   WHERE
   S.numDomainID = v_numDomainID
   AND 1 =(CASE
   WHEN coalesce(v_numShipVia,0) > 0 AND coalesce(v_numShipService,0) > 0
   THEN(CASE WHEN coalesce(S.numShippingCompanyID,0) = v_numShipVia AND coalesce(S.intNsoftEnum,0) = v_numShipService THEN 1 ELSE 0 END)
   ELSE(CASE WHEN coalesce(bitEnabled,false) = true THEN 1 ELSE 0 END)
   END);
   RETURN;
END; $$;      

