-- Stored procedure definition script USP_DeleteProcessFromOpportunity for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_DeleteProcessFromOpportunity(v_numOppId NUMERIC(18,0) DEFAULT 0,
v_numProjectId NUMERIC(18,0) DEFAULT 0)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   IF(v_numOppId > 0) then
	
      UPDATE OpportunityMaster SET numBusinessProcessID = NULL WHERE numOppId = v_numOppId;
      DELETE FROM StagePercentageDetails WHERE numOppid = v_numOppId;
      DELETE FROM ProjectProcessStageDetails WHERE numOppId = v_numOppId;
      DELETE FROM ProjectProgress WHERE numOppId = v_numOppId;
      DELETE FROM StagePercentageDetailsTask WHERE numOppId = v_numOppId;
      DELETE FROM Sales_process_List_Master WHERE numOppId = v_numOppId;
   end if;
   IF(v_numProjectId > 0) then
	
      UPDATE ProjectsMaster SET numBusinessProcessID = NULL,dtCompletionDate = NULL WHERE numProId = v_numProjectId;
      DELETE FROM StagePercentageDetails WHERE numProjectid = v_numProjectId;
      DELETE FROM ProjectProcessStageDetails WHERE numProjectId = v_numProjectId;
      DELETE FROM ProjectProgress WHERE numProId = v_numProjectId;
      DELETE FROM StagePercentageDetailsTaskTimeLog WHERE numTaskId IN(SELECT numTaskId FROM StagePercentageDetailsTask WHERE numProjectId = v_numProjectId);
      DELETE FROM StagePercentageDetailsTask WHERE numProjectId = v_numProjectId;
      DELETE FROM Sales_process_List_Master WHERE numProjectId = v_numProjectId;
   end if;
   RETURN;
END; $$; 



