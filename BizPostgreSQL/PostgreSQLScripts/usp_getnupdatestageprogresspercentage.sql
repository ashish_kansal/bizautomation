-- Stored procedure definition script USP_GetNUpdateStageProgressPercentage for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_GetNUpdateStageProgressPercentage(v_numDomainid NUMERIC DEFAULT 0,
	v_numStageId NUMERIC DEFAULT 0,   
    v_numRecID NUMERIC DEFAULT 0 ,
    v_tintMode SMALLINT DEFAULT 0, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numProjectID  NUMERIC;
   v_numOppID  NUMERIC;
BEGIN
   IF v_tintMode = 0 then

      open SWV_RefCur for
      SELECT  numStageDetailsId FROM StagePercentageDetails sp
      WHERE
      sp.numdomainid = v_numDomainid
      AND numStageDetailsId < v_numStageId
      AND coalesce(sp.numOppid,0) = v_numRecID
      AND tinProgressPercentage < 100
      ORDER BY numStageDetailsId ASC;
   ELSEIF v_tintMode = 1
   then

      open SWV_RefCur for
      SELECT  numStageDetailsId FROM StagePercentageDetails sp
      WHERE
      sp.numdomainid = v_numDomainid
      AND numStageDetailsId < v_numStageId
      AND coalesce(sp.numProjectid,0) = v_numRecID
      AND tinProgressPercentage < 100
      ORDER BY numStageDetailsId ASC;
   end if;



   RETURN;
END; $$;


