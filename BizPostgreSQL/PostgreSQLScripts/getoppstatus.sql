-- Function definition script GetOppStatus for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION GetOppStatus(v_numOppid NUMERIC(9,0))
RETURNS VARCHAR(100) LANGUAGE plpgsql
   AS $$
   DECLARE
   v_status  SMALLINT;
   v_Shipping  SMALLINT;
   v_vcStatus  VARCHAR(100);
BEGIN
   select   tintoppstatus, coalesce(tintshipped,0) INTO v_status,v_Shipping from OpportunityMaster where numOppId = v_numOppid;


   if v_status = 1 then

      if v_Shipping = 0 then 
         v_vcStatus := 'Deal Won';
      end if;
      if v_Shipping = 1 then 
         v_vcStatus := 'Deal Won(Completed)';
      end if;
   ELSEIF v_status = 2
   then 
      v_vcStatus := 'Deal Lost';
   else 
      v_vcStatus := 'Open';
   end if;

   RETURN v_vcStatus;
END; $$;

