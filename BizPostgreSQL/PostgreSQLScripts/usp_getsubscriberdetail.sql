-- Stored procedure definition script USP_GetSubscriberDetail for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetSubscriberDetail(v_numSubscriberID NUMERIC(18,0),
	v_numDomainID NUMERIC(18,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_BranchCount  INTEGER;
BEGIN
   select   COUNT(*) INTO v_BranchCount FROM Domain WHERE numSubscriberID = v_numSubscriberID;
  
   open SWV_RefCur for SELECT
   vcCompanyName,
		coalesce(intNoofUsersSubscribed,0) AS intNoofUsersSubscribed,
		coalesce(intNoofLimitedAccessUsers,0) AS intNoofLimitedAccessUsers,
		coalesce(intNoofBusinessPortalUsers,0) AS intNoofBusinessPortalUsers,
		coalesce(intFullUserConcurrency,0) AS intFullUserConcurrency,
		coalesce((SELECT COUNT(*) FROM Sites SI WHERE SI.numDomainID = S.numTargetDomainID AND coalesce(SI.bitIsActive,false) = true AND LENGTH(coalesce(SI.vcLiveURL,'')) > 0),0) AS intNoofSites,
		monFullUserCost,
		monLimitedAccessUserCost,
		monBusinessPortalUserCost,
		monSiteCost,
		coalesce(intNoOfPartners,0) AS intNoOfPartners,
		dtSubStartDate,
		dtSubEndDate,
		coalesce(bitTrial,false) AS bitTrial,
		numAdminContactID,
		coalesce(bitActive,0) AS bitActive,
		numTargetDomainID,
		dtSuspendedDate,
		coalesce(vcSuspendedReason,'') AS vcSuspendedReason,
		S.numCreatedBy,
		S.dtCreatedDate,
		S.numModifiedBy,
		S.dtModifiedDate,
		coalesce(v_BranchCount,0) AS BranchCount,"Do".tintLogin,
		coalesce(intNoofPartialSubscribed,0) AS intNoofPartialSubscribed,
		coalesce(intNoofMinimalSubscribed,0) AS intNoofMinimalSubscribed,
		dtEmailStartDate,
		dtEmailEndDate,
		coalesce(intNoOfEmail,0) AS intNoOfEmail,
		coalesce(S.bitEnabledAccountingAudit,false) AS bitEnabledAccountingAudit,
		coalesce(S.bitEnabledNotifyAdmin,false) AS bitEnabledNotifyAdmin,
		coalesce("Do".bitAllowToEditHelpFile,false) AS bitAllowToEditHelpFile
   FROM
   Subscribers S
   INNER JOIN
   DivisionMaster D
   ON
   D.numDivisionID = S.numDivisionid
   INNER JOIN
   CompanyInfo C
   ON
   C.numCompanyId = D.numCompanyID
   INNER JOIN
   Domain "Do"
   ON
   S.numTargetDomainID = "Do".numDomainId
   WHERE
   S.numSubscriberID = v_numSubscriberID
   AND S.numDomainID = v_numDomainID;
END; $$;
