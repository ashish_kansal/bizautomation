-- Function definition script fn_MonthName for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION fn_MonthName(v_intMonth INTEGER)
RETURNS VARCHAR(10) LANGUAGE plpgsql
   AS $$
BEGIN
   RETURN
   CASE
   WHEN v_intMonth = 1 THEN 'January'
   WHEN v_intMonth = 2 THEN 'February'
   WHEN v_intMonth = 3 THEN 'March'
   WHEN v_intMonth = 4 THEN 'April'
   WHEN v_intMonth = 5 THEN 'May'
   WHEN v_intMonth = 6 THEN 'June'
   WHEN v_intMonth = 7 THEN 'July'
   WHEN v_intMonth = 8 THEN 'August'
   WHEN v_intMonth = 9 THEN 'September'
   WHEN v_intMonth = 10 THEN 'October'
   WHEN v_intMonth = 11 THEN 'November'
   WHEN v_intMonth = 12 THEN 'December'
   END;
END; $$;

