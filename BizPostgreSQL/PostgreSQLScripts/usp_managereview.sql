-- Stored procedure definition script USP_ManageReview for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ManageReview(INOUT v_numReviewID NUMERIC(9,0) DEFAULT 0 ,    
v_tintTypeId INTEGER DEFAULT 0,       
v_vcReferenceId	 VARCHAR(100) DEFAULT '',  
v_vcReviewTitle VARCHAR(50) DEFAULT '',      
v_vcReviewComment VARCHAR(1000) DEFAULT '' ,
v_bitEmailMe BOOLEAN DEFAULT NULL ,
v_vcIpAddress VARCHAR(100) DEFAULT NULL ,
v_numContactId NUMERIC(18,0) DEFAULT NULL ,
v_numSiteId NUMERIC(18,0) DEFAULT NULL ,
v_numDomainId NUMERIC(18,0) DEFAULT NULL ,
v_vcCreatedDate VARCHAR(100) DEFAULT NULL,
v_bitHide BOOLEAN DEFAULT FALSE,
v_bitApproved BOOLEAN DEFAULT TRUE)
LANGUAGE plpgsql
   AS $$
BEGIN
   IF v_numReviewID = 0 then
    
      INSERT INTO Review(tintTypeId ,
        vcReferenceId,
        vcReviewTitle,
        vcReviewComment,
        bitEmailMe ,
        vcIpaddress,
        numContactId ,
        numSiteId ,
        numDomainID ,
        dtCreatedDate ,
        bitHide ,
		bitApproved)
      values(v_tintTypeId ,
        v_vcReferenceId,
        v_vcReviewTitle,
        v_vcReviewComment,
        v_bitEmailMe ,
        v_vcIpAddress,
        v_numContactId ,
        v_numSiteId ,
        v_numDomainId ,
        CAST(v_vcCreatedDate AS TIMESTAMP),
        v_bitHide ,
		v_bitApproved);
         
      v_numReviewID := CURRVAL('Review_seq');
   else
      UPDATE Review set
      vcReviewTitle = v_vcReviewTitle,vcReviewComment = v_vcReviewComment,bitHide =  v_bitHide,
      bitApproved = v_bitApproved
      where numReviewId = v_numReviewID;
   end if;

   RETURN;
END; $$;


