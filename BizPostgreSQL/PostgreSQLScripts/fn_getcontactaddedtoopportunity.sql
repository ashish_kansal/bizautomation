CREATE OR REPLACE FUNCTION fn_GetContactAddedToOpportunity(v_numOppId NUMERIC)
RETURNS VARCHAR(2000) LANGUAGE plpgsql
   AS $$
   DECLARE
   v_ContactList  VARCHAR(2000);
BEGIN
   SELECT STRING_AGG(CAST(numContactId AS VARCHAR),', ') INTO v_ContactList FROM OpportunityContact WHERE numOppId = v_numOppId;
    
   RETURN coalesce(v_ContactList,'');
END; $$;

