-- Stored procedure definition script USP_ReportListMaster_Top10CustomerByProfitAmountPreBuildReport for PostgreSQL
CREATE OR REPLACE FUNCTION USP_ReportListMaster_Top10CustomerByProfitAmountPreBuildReport(v_numDomainID NUMERIC(18,0)
	,v_ClientTimeZoneOffset INTEGER
	,v_vcTimeLine VARCHAR(50)
	,v_tintControlField INTEGER
	,v_numRecordCount INTEGER,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numShippingItemID  NUMERIC(18,0);
   v_numDiscountItemID  NUMERIC(18,0);
   v_ProfitCost  INTEGER;
BEGIN
   select   numShippingServiceItemID, numDiscountServiceItemID, numCost INTO v_numShippingItemID,v_numDiscountItemID,v_ProfitCost FROM Domain WHERE numDomainId = v_numDomainID;


   open SWV_RefCur for SELECT numDivisionID
		,(CASE tintCRMType WHEN 1 THEN CONCAT('~/prospects/frmProspects.aspx?DivID=',numDivisionID) WHEN 2 THEN CONCAT('~/Account/frmAccounts.aspx?DivID=',numDivisionID) ELSE CONCAT('~/Leads/frmLeads.aspx?DivID=',numDivisionID) END) AS URL
		,vcCompanyName
		,SUM(Profit) AS Profit
   FROM(SELECT
      DM.numDivisionID
			,tintCRMType
			,CI.vcCompanyName
			,CAST(coalesce(GetOrderItemProfitAmountOrMargin(v_numDomainID,OM.numOppId,OI.numoppitemtCode,1::SMALLINT),0) as DECIMAL(20,5)) AS Profit
			,coalesce(monTotAmount,0) AS monTotAmount
      FROM
      OpportunityMaster OM
      INNER JOIN
      OpportunityItems OI
      ON
      OM.numOppId = OI.numOppId
      INNER JOIN
      DivisionMaster DM
      ON
      OM.numDivisionId = DM.numDivisionID
      INNER JOIN
      CompanyInfo CI
      ON
      DM.numCompanyID = CI.numCompanyId
      INNER JOIN
      Item I
      ON
      OI.numItemCode = I.numItemCode
      WHERE
      OM.numDomainId = v_numDomainID
      AND coalesce(OI.monTotAmount,0) <> 0
      AND coalesce(OI.numUnitHour,0) <> 0
      AND coalesce(OM.tintopptype,0) = 1
      AND coalesce(OM.tintoppstatus,0) = 1
      AND I.numItemCode NOT IN(v_numShippingItemID,v_numDiscountItemID)) TEMP
   GROUP BY
   numDivisionID,vcCompanyName,tintCRMType
   HAVING(CASE
   WHEN v_tintControlField = 2 THEN(SUM(Profit)/SUM(monTotAmount))*100
   WHEN v_tintControlField = 1 THEN SUM(Profit)
   END) > 0
		--(SUM(Profit)/SUM(monTotAmount)) * 100 > 0
   ORDER BY
   CASE
   WHEN v_tintControlField = 2 THEN(SUM(Profit)/SUM(monTotAmount))*100
   WHEN v_tintControlField = 1 THEN SUM(Profit)
   END DESC;
		--(SUM(Profit)/SUM(monTotAmount)) * 100 DESC
	--HAVING
	--	SUM(Profit) > 0
	--ORDER BY
	--	SUM(Profit) DESC
END; $$;












