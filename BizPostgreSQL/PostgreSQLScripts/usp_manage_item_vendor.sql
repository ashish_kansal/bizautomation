-- Stored procedure definition script USP_MANAGE_ITEM_VENDOR for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021

CREATE OR REPLACE FUNCTION USP_MANAGE_ITEM_VENDOR(INOUT v_numVendorTcode		NUMERIC(18,0) ,
	v_numItemKitID		NUMERIC(18,0),
	v_numVendorID		NUMERIC(18,0),
	v_vcPartNo			VARCHAR(100),
	v_numDomainID		NUMERIC(18,0),
	v_monCost			DECIMAL(20,5) ,
	v_intMinQty			INTEGER,
	v_bitIsPrimaryVendor	BOOLEAN,
	v_intMode			BOOLEAN,
	v_vcNotes VARCHAR(300) DEFAULT '')
LANGUAGE plpgsql
   AS $$
BEGIN
   IF(SELECT COUNT(*) FROM Vendor WHERE numDomainID = v_numDomainID AND numItemCode = v_numItemKitID) = 0 then
	
      v_bitIsPrimaryVendor := true;
   end if;

   IF v_intMode = false then
	
      select   numVendorTcode INTO v_numVendorTcode FROM Vendor WHERE numDomainID = v_numDomainID AND numItemCode = v_numItemKitID AND numVendorID = v_numVendorID;
      IF coalesce(v_numVendorTcode,0) = 0 then
			
         INSERT INTO  Vendor(numVendorID, vcPartNo, numDomainID, monCost, numItemCode, intMinQty, vcNotes)
								 VALUES(v_numVendorID,v_vcPartNo,v_numDomainID,v_monCost,v_numItemKitID,v_intMinQty, v_vcNotes);
				 
         RAISE NOTICE '%',v_numVendorTcode;
         v_numVendorTcode := CURRVAL('Vendor_seq');
         RAISE NOTICE '%',v_numVendorTcode;
      ELSE
         select   numItemCode, numDomainID, numVendorID INTO v_numItemKitID,v_numDomainID,v_numVendorID FROM Vendor WHERE numVendorTcode = v_numVendorTcode;
         UPDATE Vendor	SET	vcPartNo = v_vcPartNo,monCost = v_monCost,intMinQty = v_intMinQty,vcNotes = v_vcNotes
         WHERE 1 = 1
         AND numVendorTcode = v_numVendorTcode
         AND numDomainID = v_numDomainID;
      end if;
      IF v_bitIsPrimaryVendor = true then
			
         UPDATE Item SET numVendorID = v_numVendorID WHERE numItemCode = v_numItemKitID AND numDomainID = v_numDomainID;
      end if;
   end if;
	
--	 PRINT @numVendorTcode 
--	 SET @numVendorTcode = SCOPE_IDENTITY()		 
--	 PRINT @numVendorTcode 
	 
   IF v_intMode = true then
	
      DELETE FROM Vendor WHERE 1 = 1 AND numVendorTcode = v_numVendorTcode;
   end if;
   RETURN;
END; $$;
/****** Object:  StoredProcedure [dbo].[USP_ManageAddAmount]    Script Date: 07/26/2008 16:19:28 ******/



