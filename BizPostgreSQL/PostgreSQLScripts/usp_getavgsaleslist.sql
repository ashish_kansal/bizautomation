-- Stored procedure definition script USP_GetAvgSalesList for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_GetAvgSalesList(v_numUserCntID NUMERIC,                                                              
 v_tintUserRightType SMALLINT,     
 v_intSort INTEGER,                                                                                                   
 v_columnName VARCHAR(50) DEFAULT '',                                                            
 v_columnSortOrder VARCHAR(10) DEFAULT '',                                    
 v_ListType SMALLINT DEFAULT 0,                                    
 v_TeamType INTEGER DEFAULT 0,       
 v_TeamMemID NUMERIC(9,0) DEFAULT 0,                                
 v_numDomainID NUMERIC(9,0) DEFAULT 0,              
 v_dtFromDate TIMESTAMP DEFAULT NULL,          
 v_dtToDate TIMESTAMP DEFAULT NULL, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_strSql  VARCHAR(7000);
BEGIN
   v_strSql := 'SELECT OM.numOppId,    
   OM.numDivisionId,                                                                               
   DM.bintCreatedDate as bintCreatedDate,                                                              
   OM.bintAccountClosingDate as bintAccountClosingDate,    
   VcCompanyName as CName,                                                                                                                                                                                                     
   DATE_PART(''day'',OM.bintAccountClosingDate -DM.bintCreatedDate) as days,                                                                                                                                                                                     
   fn_GetContactName(OM.numRecOwner) as numRecOwner,                                                                                                                                                                                                     
   fn_GetContactName(OM.numAssignedTo) as AssignedTo,     
   OM.monPAmount,
  ADC.numContactId,                      
 DM.numCompanyID,                    
  DM.numTerID,
  DM.tintCRMType                                                                       
   FROM        
   OpportunityMaster OM                                                            
   join DivisionMaster DM                                        
   on DM.numDivisionID = OM.numDivisionId    
   join CompanyInfo C                                        
   on C.numCompanyId = DM.numCompanyID      
   join AdditionalContactsInformation ADC    
   on ADC.numContactId = OM.numRecOwner                                                             
   WHERE    
OM.numDomainId =' || SUBSTR(CAST(v_numDomainID AS VARCHAR(15)),1,15) || '        
and OM.bintCreatedDate >= ''' || SUBSTR(CAST(v_dtFromDate AS VARCHAR(20)),1,20) || '''        
and OM.bintCreatedDate <= ''' || SUBSTR(CAST(v_dtToDate AS VARCHAR(20)),1,20) || '''    
and OM.tintOppStatus =1  
and OM.numOppID = (SELECT numOppID from    
OpportunityMaster OM where OM.numDivisionId=DM.numDivisionId and bintAccountClosingDate is not null  order by bintAccountClosingDate Asc LIMIT 1)  
';  
  
   if v_intSort <> 0 then 
      v_strSql := coalesce(v_strSql,'') || ' AND OM.monPAmount >=  ' || SUBSTR(CAST(v_intSort AS VARCHAR(15)),1,15) || '';
   end if;     
   if v_tintUserRightType = 1 then 
      v_strSql := coalesce(v_strSql,'') || ' AND OM.numRecOwner = ' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15) || '';
   end if;    
    
   if v_tintUserRightType = 2 then 
      v_strSql := coalesce(v_strSql,'') || ' and ADC.numTeam in(select F.numTeam from ForReportsByTeam F         
where F.numuserCntid=' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15) || ' and F.numDomainid=' || SUBSTR(CAST(v_numDomainID AS VARCHAR(15)),1,15) || ' and F.tintType=' || SUBSTR(CAST(v_TeamType AS VARCHAR(5)),1,5) || ')';
   ELSEIF v_tintUserRightType = 3
   then 
      v_strSql := coalesce(v_strSql,'') || ' AND (DM.numTerID in (select numTerritoryID from  UserTerritory where numUserCntID= ' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15) || ' ) or DM.numTerID=0)';
   end if;                                            
  
                    
   RAISE NOTICE '%',v_strSql;                                            
                                 
   OPEN SWV_RefCur FOR EXECUTE v_strSql;
   RETURN;
END; $$;


