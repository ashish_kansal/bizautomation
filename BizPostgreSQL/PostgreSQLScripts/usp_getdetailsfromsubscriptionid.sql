-- FUNCTION: public.usp_getdetailsfromsubscriptionid(character varying, refcursor)

-- DROP FUNCTION public.usp_getdetailsfromsubscriptionid(character varying, refcursor);

CREATE OR REPLACE FUNCTION public.usp_getdetailsfromsubscriptionid(
	v_subscriptionid character varying DEFAULT ''::character varying,
	INOUT swv_refcur refcursor DEFAULT NULL::refcursor)
    RETURNS refcursor
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
AS $BODY$
BEGIN
   open SWV_RefCur for 
   SELECT  
	U.numUserId
	,numUserDetailId
	,coalesce(vcEmailID,'') AS vcEmailID
	,coalesce(resourceId,0) AS resourceId,
	coalesce(U.numDomainID,0) AS numDomainID
	,coalesce(Div.numDivisionID,0) AS numDivisionID
	,coalesce(E.bitExchangeIntegration,false) AS bitExchangeIntegration
	,coalesce(E.bitAccessExchange,false) AS bitAccessExchange
	,coalesce(E.vcExchPath,'') AS vcExchPath
	,coalesce(E.vcExchDomain,'') AS vcExchDomain
	,coalesce(D.vcExchUserName,'') AS vcExchUserName 
	,coalesce(vcFirstName,'') || ' ' || coalesce(vcLastname,'') as ContactName
	,Case when E.bitAccessExchange = false then  E.vcExchPassword when E.bitAccessExchange = TRUE then D.vcExchPassword end as vcExchPassword
	,Case when E.bitAccessExchange = false then  E.vcExchPath when E.bitAccessExchange = TRUE then D.vcExchPath end as vcExchPath           
   FROM UserMaster U
   left join ExchangeUserDetails E
   on E.numUserID = U.numUserId
   Join Domain D
   on D.numDomainId = U.numDomainID
   left join DivisionMaster Div
   on D.numDivisionId = Div.numDivisionID
   left join CompanyInfo C
   on C.numCompanyId = Div.numDivisionID
   left join AdditionalContactsInformation A
   on  A.numContactId = U.numUserDetailId
   left join Resource R
   on  R.numUserCntId = U.numUserDetailId
   WHERE SubscriptionId = v_SubscriptionId LIMIT 1;
END;
$BODY$;

ALTER FUNCTION public.usp_getdetailsfromsubscriptionid(character varying, refcursor)
    OWNER TO postgres;
