CREATE OR REPLACE FUNCTION usp_AddMasterListDetails(   
v_numListID NUMERIC,  
 v_vcItemName VARCHAR(150) DEFAULT '',  
 v_vcItemType CHAR(1) DEFAULT 'L',  
 v_numDomainID NUMERIC(9,0) DEFAULT 1,  
 v_numCreatedBy NUMERIC(9,0) DEFAULT NULL,  
 v_bintCreatedDate TIMESTAMP DEFAULT NULL, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_itemCount  INTEGER;
BEGIN
   IF v_vcItemType = 'L' then
  
      select   COUNT(*) INTO v_itemCount FROM Listdetails WHERE vcData = v_vcItemName AND numDomainid = v_numDomainID
      AND numListID = v_numListID;
      IF v_itemCount = 0 then
        
         INSERT INTO Listdetails(numListID, vcData, numCreatedBy, bintCreatedDate, numDomainid, constFlag)
    VALUES(v_numListID, v_vcItemName, v_numCreatedBy, v_bintCreatedDate, v_numDomainID, false);
                      
         open SWV_RefCur for
         SELECT true;
      ELSE
         open SWV_RefCur for
         SELECT false;
      end if;
   end if;  
   IF v_vcItemType = 'T' then
  
      select   COUNT(*) INTO v_itemCount FROM TerritoryMaster WHERE vcTerName = v_vcItemName AND numDomainid = v_numDomainID;
      IF v_itemCount = 0 then
   
         INSERT INTO TerritoryMaster(vcTerName, numCreatedBy, bintCreatedDate, numDomainid)
    VALUES(v_vcItemName, v_numCreatedBy, v_bintCreatedDate, v_numDomainID);
              
         open SWV_RefCur for
         SELECT true;
      ELSE
         open SWV_RefCur for
         SELECT false;
      end if;
   end if;
   RETURN;
END; $$;


