-- Stored procedure definition script USP_UpdateChartAcntOpenBal for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_UpdateChartAcntOpenBal(v_numChartAcntId NUMERIC(9,0),                          
v_numDomainId NUMERIC(9,0),                          
v_numOpeningBal DECIMAL(20,5))
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numOldOpeningBal  DECIMAL(20,5);                  
   v_numNewOpeningBal  DECIMAL(20,5);                  
   v_strSQl  VARCHAR(5000);                        
   v_strSQl2  VARCHAR(5000);                 
   v_strUpdateOpenEquityBal  VARCHAR(5000);                 
   v_numAcntType  NUMERIC(9,0);                
   v_numJournalId  NUMERIC(9,0);  
   v_numAccountIdOpeningEquity  NUMERIC(9,0);
BEGIN
   v_strSQl := '';                
   v_strSQl2 := '';                
   v_strUpdateOpenEquityBal := '';                 
                     
   select   numOriginalOpeningBal, numAcntTypeId INTO v_numOldOpeningBal,v_numAcntType From Chart_Of_Accounts Where numAccountId = v_numChartAcntId;                        
   Select numAccountId INTO v_numAccountIdOpeningEquity From Chart_Of_Accounts Where bitOpeningBalanceEquity = true  and numDomainId = v_numDomainId;   
                     
   Update Chart_Of_Accounts Set numOriginalOpeningBal = v_numOpeningBal Where numAccountId = v_numChartAcntId And  numDomainId = v_numDomainId;                
                        
   v_strSQl := fn_ParentCategory(v_numChartAcntId::VARCHAR(100),v_numDomainId::INTEGER);                                                  
   RAISE NOTICE '%',('strSQL' || coalesce(v_strSQl,''));                      
   If v_strSQl <> '' then
    
      v_strSQl2 := 'update Chart_Of_Accounts  set numopeningbal = coalesce(numopeningbal,0) - ' || SUBSTR(CAST(v_numOldOpeningBal AS VARCHAR(30)),1,30) || ' where  numAccountId in(' || SUBSTR(CAST(v_numChartAcntId AS VARCHAR(30)),1,30) || ',' || coalesce(v_strSQl,'') || ') And numDomainId =' || SUBSTR(CAST(v_numDomainId AS VARCHAR(10)),1,10);
      If v_numAcntType = 813 then
         v_strUpdateOpenEquityBal := 'update Chart_Of_Accounts  set numopeningbal = coalesce(numopeningbal,0) - ' || SUBSTR(CAST(v_numOldOpeningBal AS VARCHAR(30)),1,30) ||
         ' ,numOriginalOpeningBal = coalesce(numOriginalOpeningBal,0) - ' || SUBSTR(CAST(v_numOldOpeningBal AS VARCHAR(30)),1,30) || ' where  numAccountId =' || SUBSTR(CAST(v_numAccountIdOpeningEquity AS VARCHAR(10)),1,10) || ' And numDomainId =' || SUBSTR(CAST(v_numDomainId AS VARCHAR(10)),1,10);
      end if;
      If v_numAcntType = 816 then
         v_strUpdateOpenEquityBal := 'update Chart_Of_Accounts  set numopeningbal = coalesce(numopeningbal,0)+ ' || SUBSTR(CAST(v_numOldOpeningBal AS VARCHAR(30)),1,30) ||
         ' ,numOriginalOpeningBal = coalesce(numOriginalOpeningBal,0)+ ' || SUBSTR(CAST(v_numOldOpeningBal AS VARCHAR(30)),1,30) ||  ' where  numAccountId =' || SUBSTR(CAST(v_numAccountIdOpeningEquity AS VARCHAR(10)),1,10)  || ' And numDomainId =' || SUBSTR(CAST(v_numDomainId AS VARCHAR(10)),1,10);
      end if;
   Else
      v_strSQl2 := 'update Chart_Of_Accounts  set   numopeningbal = coalesce(numopeningbal,0) - ' || SUBSTR(CAST(v_numOldOpeningBal AS VARCHAR(30)),1,30) || ' where  numAccountId in(' || SUBSTR(CAST(v_numChartAcntId AS VARCHAR(30)),1,30) || ') And numDomainId =' || SUBSTR(CAST(v_numDomainId AS VARCHAR(10)),1,10);
      If v_numAcntType = 813 then
         v_strUpdateOpenEquityBal := 'update Chart_Of_Accounts  set numopeningbal = coalesce(numopeningbal,0) - ' || SUBSTR(CAST(v_numOldOpeningBal AS VARCHAR(30)),1,30) ||
         ' ,numOriginalOpeningBal = coalesce(numOriginalOpeningBal,0) - ' || SUBSTR(CAST(v_numOldOpeningBal AS VARCHAR(30)),1,30) ||  ' where  numAccountId =' || SUBSTR(CAST(v_numAccountIdOpeningEquity AS VARCHAR(10)),1,10) || ' And numDomainId =' || SUBSTR(CAST(v_numDomainId AS VARCHAR(10)),1,10);
      end if;
      If v_numAcntType = 816 then
         v_strUpdateOpenEquityBal := 'update Chart_Of_Accounts  set numopeningbal = coalesce(numopeningbal,0)+ ' || SUBSTR(CAST(v_numOldOpeningBal AS VARCHAR(30)),1,30) ||
         ' ,numOriginalOpeningBal = coalesce(numOriginalOpeningBal,0)+ ' || SUBSTR(CAST(v_numOldOpeningBal AS VARCHAR(30)),1,30) ||  ' where  numAccountId =' || SUBSTR(CAST(v_numAccountIdOpeningEquity AS VARCHAR(10)),1,10) || ' And numDomainId =' || SUBSTR(CAST(v_numDomainId AS VARCHAR(10)),1,10);
      end if;
   end if;                
   EXECUTE v_strSQl2;                                            
   RAISE NOTICE '%',(v_strSQl2);                   
   EXECUTE v_strUpdateOpenEquityBal;                
   RAISE NOTICE '%',(v_strUpdateOpenEquityBal);                                            
  -- To Get the New Value from the table for Update purpose                        
   select   numOriginalOpeningBal INTO v_numNewOpeningBal From Chart_Of_Accounts Where numAccountId = v_numChartAcntId    And numDomainId = v_numDomainId;                    
     -- Print (@numNewOpeningBal)                      
            
   if v_strSQl <> '' then
      
      v_strSQl2 := 'update Chart_Of_Accounts  set numopeningbal = coalesce(numopeningbal,0)+ ' || SUBSTR(CAST(v_numNewOpeningBal AS VARCHAR(30)),1,30) || ' where  numAccountId in(' || SUBSTR(CAST(v_numChartAcntId AS VARCHAR(30)),1,30) || ',' || coalesce(v_strSQl,'') || ') And numDomainId =' || SUBSTR(CAST(v_numDomainId AS VARCHAR(10)),1,10);
      if v_numAcntType = 813 then
           
         v_strUpdateOpenEquityBal := 'update Chart_Of_Accounts  set numopeningbal = coalesce(numopeningbal,0)+ ' || SUBSTR(CAST(v_numNewOpeningBal AS VARCHAR(30)),1,30) ||
         ' ,numOriginalOpeningBal = coalesce(numOriginalOpeningBal,0)+ ' || SUBSTR(CAST(v_numNewOpeningBal AS VARCHAR(30)),1,30) ||  ' where  numAccountId =' || SUBSTR(CAST(v_numAccountIdOpeningEquity AS VARCHAR(10)),1,10) || ' And numDomainId =' || SUBSTR(CAST(v_numDomainId AS VARCHAR(10)),1,10);                                            
          
            -- To update Opening Balance in General_Journal_Header and General_Journal_Details Table          
         Update General_Journal_Header Set numAmount = v_numOpeningBal Where numChartAcntId = v_numChartAcntId And numDomainId = v_numDomainId;
         select   numJOurnal_Id INTO v_numJournalId From General_Journal_Header Where numChartAcntId = v_numChartAcntId And numDomainId = v_numDomainId;
         Update General_Journal_Details Set numDebitAmt = v_numOpeningBal Where numJournalId = v_numJournalId And numDomainId = v_numDomainId;
      end if;
      if v_numAcntType = 816 then
          
         v_strUpdateOpenEquityBal := 'update Chart_Of_Accounts  set numopeningbal = coalesce(numopeningbal,0) - ' || SUBSTR(CAST(v_numNewOpeningBal AS VARCHAR(30)),1,30) ||
         ' ,numOriginalOpeningBal = coalesce(numOriginalOpeningBal,0) - ' || SUBSTR(CAST(v_numNewOpeningBal AS VARCHAR(30)),1,30) ||  ' where  numAccountId =' || SUBSTR(CAST(v_numAccountIdOpeningEquity AS VARCHAR(10)),1,10)   || ' And numDomainId =' || SUBSTR(CAST(v_numDomainId AS VARCHAR(10)),1,10);                                                 
                     
            -- To update Opening Balance in General_Journal_Header and General_Journal_Details Table          
         Update General_Journal_Header Set numAmount = v_numOpeningBal Where numChartAcntId = v_numChartAcntId And numDomainId = v_numDomainId;
         select   numJOurnal_Id INTO v_numJournalId From General_Journal_Header Where numChartAcntId = v_numChartAcntId And numDomainId = v_numDomainId;
         Update General_Journal_Details Set numCreditAmt = v_numOpeningBal Where numJournalId = v_numJournalId  And numDomainId = v_numDomainId;
      end if;
   else
      RAISE NOTICE 'sp';
      v_strSQl2 := 'update Chart_Of_Accounts  set numopeningbal = coalesce(numopeningbal,0)+ ' || SUBSTR(CAST(v_numNewOpeningBal AS VARCHAR(30)),1,30) || ' where  numAccountId in(' || SUBSTR(CAST(v_numChartAcntId AS VARCHAR(30)),1,30) || ')';
      if v_numAcntType = 813 then
         
         v_strUpdateOpenEquityBal := 'update Chart_Of_Accounts  set numopeningbal = coalesce(numopeningbal,0)+ ' || SUBSTR(CAST(v_numNewOpeningBal AS VARCHAR(30)),1,30) ||
         ' ,numOriginalOpeningBal = coalesce(numOriginalOpeningBal,0)+ ' || SUBSTR(CAST(v_numNewOpeningBal AS VARCHAR(30)),1,30) || ' where  numAccountId =' || SUBSTR(CAST(v_numAccountIdOpeningEquity AS VARCHAR(10)),1,10) || ' And numDomainId =' || SUBSTR(CAST(v_numDomainId AS VARCHAR(10)),1,10);                   
            -- To update Opening Balance in General_Journal_Header and General_Journal_Details Table          
         Update General_Journal_Header Set numAmount = v_numOpeningBal Where numChartAcntId = v_numChartAcntId   And numDomainId = v_numDomainId;
         select   numJOurnal_Id INTO v_numJournalId From General_Journal_Header Where numChartAcntId = v_numChartAcntId  And numDomainId = v_numDomainId;
         Update General_Journal_Details Set numDebitAmt = v_numOpeningBal Where numJournalId = v_numJournalId  And numDomainId = v_numDomainId;
      end if;
      if v_numAcntType = 816 then
          
         v_strUpdateOpenEquityBal := 'update Chart_Of_Accounts  set numopeningbal = coalesce(numopeningbal,0) - ' || SUBSTR(CAST(v_numNewOpeningBal AS VARCHAR(30)),1,30) ||
         ' ,numOriginalOpeningBal = coalesce(numOriginalOpeningBal,0) - ' || SUBSTR(CAST(v_numNewOpeningBal AS VARCHAR(30)),1,30) || ' where  numAccountId =' || SUBSTR(CAST(v_numAccountIdOpeningEquity AS VARCHAR(10)),1,10) || ' And numDomainId =' || SUBSTR(CAST(v_numDomainId AS VARCHAR(10)),1,10);                                                      
                     
            -- To update Opening Balance in General_Journal_Header and General_Journal_Details Table          
         Update General_Journal_Header Set numAmount = v_numOpeningBal Where numChartAcntId = v_numChartAcntId;
         select   numJOurnal_Id INTO v_numJournalId From General_Journal_Header Where numChartAcntId = v_numChartAcntId;
         Update General_Journal_Details Set numCreditAmt = v_numOpeningBal Where numJournalId = v_numJournalId;
      end if;
   end if;                
                       
   EXECUTE v_strSQl2;                                            
   RAISE NOTICE '%',(v_strSQl2);                   
   EXECUTE v_strUpdateOpenEquityBal;                
   RAISE NOTICE '%',(v_strUpdateOpenEquityBal);
   RETURN;
END; $$;


