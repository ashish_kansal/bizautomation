CREATE OR REPLACE FUNCTION get_month_end(v_date TIMESTAMP)
RETURNS TIMESTAMP LANGUAGE plpgsql
   AS $$
BEGIN
	RETURN '1900-01-01':: date + make_interval(month => datediff('month','1900-01-01'::date,v_date + make_interval(month => 1))) + make_interval(secs => -0.3);
END; $$;

