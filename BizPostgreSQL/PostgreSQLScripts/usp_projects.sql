-- Stored procedure definition script USP_Projects for PostgreSQL
CREATE OR REPLACE FUNCTION USP_Projects(v_numProId NUMERIC(9,0) DEFAULT null ,  
v_numDomainID NUMERIC(9,0) DEFAULT NULL,  
v_ClientTimeZoneOffset INTEGER DEFAULT NULL,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for select  pro.numProId,
  pro.vcProjectName,
  pro.numIntPrjMgr,
  pro.numOppId,
  pro.intDueDate,
  pro.numCustPrjMgr,
  pro.numDivisionId,
  pro.txtComments,
  Div.tintCRMType,
  Div.vcDivisionName,
  com.vcCompanyName,
  A.vcFirstName || ' ' || A.vcLastname AS vcContactName,
  coalesce(A.vcEmail,'') AS vcEmail,
         coalesce(A.numPhone,'') AS Phone,
         coalesce(A.numPhoneExtension,'') AS PhoneExtension,
		 		 (SELECT  coalesce(vcData,'') FROM Listdetails WHERE numListID = 5 AND numListItemID = com.numCompanyType AND numDomainid = v_numDomainID LIMIT 1) AS vcCompanyType,
pro.numcontractId,
  fn_GetContactName(pro.numCreatedBy) || ' ' || pro.bintCreatedDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) as vcCreatedBy ,
  fn_GetContactName(pro.numModifiedBy) || ' ' || pro.bintModifiedDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) as vcModifiedby,
  fn_GetContactName(pro.numCompletedBy) || ' ' || pro.dtCompletionDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) as vcFinishedby,
  pro.numdomainId,
  fn_GetContactName(pro.numRecOwner) as vcRecOwner,  pro.numRecOwner,   Div.numTerID,
  pro.numAssignedby,pro.numAssignedTo,
(select  count(*) from GenericDocuments where numRecID = v_numProId and  vcDocumentSection = 'P') as DocumentCount,
 numAccountID,
 '' AS TimeAndMaterial,
coalesce((select sum(coalesce(monTimeBudget,0)) from StagePercentageDetails where numProjectid = v_numProId),0) AS TimeBudget,
coalesce((select sum(coalesce(monExpenseBudget,0)) from StagePercentageDetails where numProjectid = v_numProId),0) AS ExpenseBudget,
coalesce((select min(coalesce(dtStartDate,LOCALTIMESTAMP)) + make_interval(days => -1) from StagePercentageDetails where numProjectid = v_numProId),LOCALTIMESTAMP) AS dtStartDate,
coalesce((select max(coalesce(dtEndDate,LOCALTIMESTAMP)) + make_interval(days => 1) from StagePercentageDetails where numProjectid = v_numProId),LOCALTIMESTAMP) AS dtEndDate,
coalesce(numProjectType,0) AS numProjectType,
coalesce(fn_GetExpenseDtlsbyProStgID(pro.numProId,0::NUMERIC,0::SMALLINT),'') as UsedExpense,
coalesce(fn_GeTimeDtlsbyProStgID_BT_NBT(pro.numProId,0,1::SMALLINT),'Billable Time (0)  Non Billable Time (0)') as UsedTime,
coalesce(pro.numProjectStatus,0) as numProjectStatus,
(SELECT coalesce(vcStreet,'') || ',<pre>' || coalesce(vcCity,'') || ',<pre>' ||  fn_GetState(numState) || ',<pre>' || fn_GetListItemName(numCountry) || ',<pre>' || vcPostalCode
      FROM AddressDetails WHERE numDomainID = pro.numdomainId AND numRecordID = pro.numAddressID AND tintAddressOf = 4 AND tintAddressType = 2 AND bitIsPrimary = true) as ShippingAddress,
pro.numBusinessProcessID,
SLP.Slp_Name AS vcProcessName,
pro.dtCompletionDate AS vcProjectedFinish,
NULL AS dtActualStartDate,
pro.dtmEndDate+CAST(-1*v_ClientTimeZoneOffset::bigint || 'minute' as interval) AS dtmEndDate,
pro.dtmStartDate+CAST(-1*v_ClientTimeZoneOffset::bigint || 'minute' as interval) AS dtmStartDate,
coalesce(pro.numClientBudget,0) AS numClientBudget,
0 AS numClientBudgetBalance,
(CASE
   WHEN EXISTS(SELECT CInner.numContractId FROM Contracts CInner WHERE CInner.numDomainId = v_numDomainID AND CInner.numDivisonId = pro.numDivisionId AND intType = 1)
   THEN true
   ELSE false
   END) AS bitContractExists,
0 AS numClientBudgetBalance,
(CASE
   WHEN EXISTS(SELECT CInner.numContractId FROM Contracts CInner WHERE CInner.numDomainId = v_numDomainID AND CInner.numDivisonId = pro.numDivisionId AND intType = 1)
   THEN fn_SecondsConversion(coalesce((SELECT((coalesce(numHours,0)*60*60)+(coalesce(numMinutes,0)*60)) -coalesce((SELECT SUM(CLInner.numUsedTime) FROM ContractsLog CLInner WHERE CLInner.numContractId = C.numContractId),0) FROM Contracts C WHERE C.numDomainId = v_numDomainID AND C.numDivisonId = pro.numDivisionId AND intType = 1 ORDER BY C.numContractId DESC LIMIT 1),0)::INT)
   ELSE ''
   END) AS timeLeft,
(CASE
   WHEN EXISTS(SELECT CInner.numContractId FROM Contracts CInner WHERE CInner.numDomainId = v_numDomainID AND CInner.numDivisonId = pro.numDivisionId AND intType = 1)
   THEN coalesce((SELECT((coalesce(numHours,0)*60*60)+(coalesce(numMinutes,0)*60)) -coalesce((SELECT SUM(CLInner.numUsedTime) FROM ContractsLog CLInner WHERE CLInner.numContractId = C.numContractId),0) FROM Contracts C WHERE C.numDomainId = v_numDomainID AND C.numDivisonId = pro.numDivisionId AND intType = 1 ORDER BY C.numContractId DESC LIMIT 1),0)
   ELSE 0
   END) AS timeLeftInMinutes,
coalesce(bitDisplayTimeToExternalUsers,false)  AS bitDisplayTimeToExternalUsers
   from ProjectsMaster pro
   left join DivisionMaster Div
   on pro.numDivisionId = Div.numDivisionID
   LEFT JOIN AdditionalContactsInformation A ON pro.numCustPrjMgr = A.numContactId
   left join CompanyInfo com
   on Div.numCompanyID = com.numCompanyId
   LEFT JOIN AddressDetails AD ON AD.numAddressID = pro.numAddressID
   LEFT JOIN Sales_process_List_Master AS SLP ON pro.numBusinessProcessID = SLP.Slp_Id
   where numProId = v_numProId    and pro.numdomainId = v_numDomainID;
END; $$;












