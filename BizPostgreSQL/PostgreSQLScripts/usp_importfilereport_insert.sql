-- Stored procedure definition script USP_ImportFileReport_Insert for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021

CREATE OR REPLACE FUNCTION USP_ImportFileReport_Insert(v_intImportFileID BIGINT,
	v_intRowNumber INTEGER,
	v_vcMessage VARCHAR(1000),
	v_numRecordID NUMERIC(18,0),
	v_vcErrorMessage TEXT,
	v_vcStackStrace TEXT)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   INSERT INTO Import_File_Report(intImportFileID
		,intRowNumber
		,vcMessage
		,numRecordID
		,vcErrorMessage
		,vcStackStrace)
	VALUES(v_intImportFileID
		,v_intRowNumber
		,v_vcMessage
		,v_numRecordID
		,v_vcErrorMessage
		,v_vcStackStrace);


RETURN;
END; $$;



