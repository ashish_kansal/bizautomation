-- Stored procedure definition script usp_UpdateEmailHistory for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION usp_UpdateEmailHistory()
RETURNS VOID LANGUAGE plpgsql   
--
   AS $$
   DECLARE
   v_bIntEmailSendDate  BIGINT;
BEGIN
   INSERT INTO EmailHistory(bintEmailDate ,vcFrom ,vcTo ,vcSubject ,txtbody , numDomainID)
   SELECT bintSendEmailDate , vcEmailFrom ,  ',' || vcEmailTo || ' , ' ||  vcEmailCC , vcEmailSubject ,vcEmailBody , fn_getDomainID(vcEmailFrom || ' , ' ||  vcEmailTo || ' , ' ||  vcEmailCC)
   FROM TempEmails
   WHERE fn_getValidEmailIds(vcEmailFrom || ' , ' ||  vcEmailTo || ' , ' ||  vcEmailCC) = 1
   AND bolUpload = 0;
 
   UPDATE TempEmails SET bolUpload = 1;
   RETURN;
END; $$;


