-- Function definition script OpportunityLinkedItems for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION OpportunityLinkedItems(v_numOppID NUMERIC(9,0))
RETURNS NUMERIC(9,0) LANGUAGE plpgsql
   AS $$
   DECLARE
   v_RetVal  NUMERIC(9,0);
BEGIN
   v_RetVal := 0;
	
   select   v_RetVal+coalesce(count(*),0) INTO v_RetVal from
   ProjectsOpportunities where numOppId = v_numOppID; 

   select   v_RetVal+coalesce(count(*),0) INTO v_RetVal from
   CaseOpportunities where numoppid = v_numOppID;  
   return v_RetVal;
END; $$;

