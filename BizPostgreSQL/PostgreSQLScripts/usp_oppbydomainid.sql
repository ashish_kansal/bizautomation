-- Stored procedure definition script USP_OPPByDomainID for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_OPPByDomainID(v_numDomainId NUMERIC(9,0) DEFAULT null, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
BEGIN
   if v_numDomainId = 0 then

      open SWV_RefCur for
      Select vcItemName,numItemCode from Item ORDER by vcItemName;
   else
      open SWV_RefCur for
      Select vcItemName,numItemCode from Item where numDomainID = v_numDomainId ORDER by vcItemName;
   end if;
   RETURN;
END; $$;


