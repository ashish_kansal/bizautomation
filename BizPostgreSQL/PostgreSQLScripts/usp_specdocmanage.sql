CREATE OR REPLACE FUNCTION usp_SpecDocManage(v_numDomainID NUMERIC(9,0) DEFAULT null,              
 v_vcDocDesc VARCHAR(1000) DEFAULT '',              
 v_VcFileName VARCHAR(100) DEFAULT '',              
 v_vcDocName VARCHAR(100) DEFAULT '',              
 v_numDocCategory NUMERIC(9,0) DEFAULT null,              
 v_vcfiletype VARCHAR(10) DEFAULT '',              
 v_numDocStatus NUMERIC(9,0) DEFAULT null,              
 v_numContactID NUMERIC(9,0) DEFAULT null,              
 INOUT v_numSpecDocId NUMERIC(9,0) DEFAULT 0 ,              
 v_cUrlType VARCHAR(1) DEFAULT '',            
 v_numRecID NUMERIC(9,0) DEFAULT 0 ,        
 v_strType VARCHAR(10) DEFAULT NULL,          
 v_byteMode SMALLINT DEFAULT NULL,          
 v_numGenericDocID NUMERIC(9,0) DEFAULT NULL, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_FileName  VARCHAR(200);          
   v_DocName  VARCHAR(50);          
   v_DocCategory  VARCHAR(20);          
   v_UrlType  VARCHAR(1);          
   v_FileType  VARCHAR(10);          
   v_BusClass  VARCHAR(20);          
   v_DocDesc  VARCHAR(1000);
BEGIN
   if  v_byteMode = 0 then

      if v_numSpecDocId = 0 then
  
         insert into SpecificDocuments(numDomainID,
   vcDocDesc,
   VcFileName,
   vcDocName,
   numDocCategory,
   vcfiletype,
   numDocStatus,
   numCreatedBy,
   bintCreatedDate,
   numModifiedBy,
   bintModifiedDate,
   cUrlType,
   numRecID,
   vcDocumentSection)
  values(v_numDomainID,
   v_vcDocDesc,
   v_VcFileName,
   v_vcDocName,
   v_numDocCategory,
   v_vcfiletype,
   v_numDocStatus,
   v_numContactID,
   TIMEZONE('UTC',now()),
   v_numContactID,
   TIMEZONE('UTC',now()),
   v_cUrlType,
   v_numRecID,
   v_strType);
  
         v_numSpecDocId := CURRVAL('SpecificDocuments_seq');
      else
         update  SpecificDocuments
         set
         vcDocDesc = v_vcDocDesc,VcFileName = v_VcFileName,vcDocName = v_vcDocName,numDocCategory = v_numDocCategory,
         vcfiletype = v_vcfiletype,numDocStatus = v_numDocStatus,
         numModifiedBy = v_numContactID,bintModifiedDate = TIMEZONE('UTC',now()),
         cUrlType = v_cUrlType
         where numSpecificDocID = v_numSpecDocId and numDomainID = v_numDomainID;
      end if;
   end if;          
          
   if v_byteMode = 1 then
      select   VcFileName, VcDocName, numDocCategory, cUrlType, vcfiletype, numDocStatus, vcDocdesc INTO v_FileName,v_DocName,v_DocCategory,v_UrlType,v_FileType,v_BusClass,v_DocDesc from GenericDocuments where numGenericDocID = v_numGenericDocID;
      if v_numSpecDocId = 0 then
  
         insert into SpecificDocuments(numDomainID,
   vcDocDesc,
   VcFileName,
   vcDocName,
   numDocCategory,
   vcfiletype,
   numDocStatus,
   numCreatedBy,
   bintCreatedDate,
   numModifiedBy,
   bintModifiedDate,
   cUrlType,
   numRecID,
   vcDocumentSection)
  values(v_numDomainID,
   v_DocDesc,
   v_FileName,
   v_DocName,
   v_DocCategory,
   v_FileType,
   v_BusClass,
   v_numContactID,
   TIMEZONE('UTC',now()),
   v_numContactID,
   TIMEZONE('UTC',now()),
   v_UrlType,
   v_numRecID,
   v_strType);
      else
         update  SpecificDocuments
         set
         vcDocDesc = v_vcDocDesc,VcFileName = v_VcFileName,vcDocName = v_vcDocName,numDocCategory = v_numDocCategory,
         vcfiletype = v_vcfiletype,numDocStatus = v_numDocStatus,
         numModifiedBy = v_numContactID,bintModifiedDate = TIMEZONE('UTC',now()),
         cUrlType = v_cUrlType
         where numSpecificDocID = v_numSpecDocId  and numDomainID = v_numDomainID;
      end if;
   end if;        
   if  v_byteMode = 2 then

      open SWV_RefCur for
      select   numDomainID,
   vcDocDesc,
   VcFileName,
   vcDocName,
   numDocCategory,
   vcfiletype,
   numDocStatus,
   numCreatedBy,
   bintCreatedDate,
   numModifiedBy,
   bintModifiedDate,
   cUrlType,
   numRecID,
   vcDocumentSection
      from SpecificDocuments where numSpecificDocID = v_numSpecDocId  and numDomainID = v_numDomainID;
   end if;
   RETURN;
END; $$;


