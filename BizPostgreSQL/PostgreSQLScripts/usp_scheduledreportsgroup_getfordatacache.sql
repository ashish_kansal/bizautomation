CREATE OR REPLACE FUNCTION USP_ScheduledReportsGroup_GetForDataCache(v_numDomainID NUMERIC(18,0)
	,v_numSRGID NUMERIC(18,0)
	,v_tintMode SMALLINT, INOUT SWV_RefCur refcursor)
LANGUAGE plpgsql
   AS $$
BEGIN
   IF v_tintMode = 1 AND EXISTS(SELECT ID FROM ScheduledReportsGroup WHERE numDomainID = v_numDomainID AND ID = v_numSRGID) then
	
      open SWV_RefCur for
      SELECT * FROM ScheduledReportsGroup WHERE numDomainID = v_numDomainID AND ID = v_numSRGID;
   ELSEIF v_tintMode = 2
   then
	
      open SWV_RefCur for
      SELECT * FROM
      ScheduledReportsGroup
      WHERE
      coalesce(tintDataCacheStatus,0) <> 1 --0: NOT STARTED, 1: STARTED, 2: FINISHED
      AND (EXTRACT(HOUR FROM LOCALTIMESTAMP) >= 0 AND EXTRACT(HOUR FROM LOCALTIMESTAMP) <= 7) -- BETWEEN 0 AM TO 7 AM
      AND 1 =(CASE
      WHEN dtNextDate IS NULL AND CAST(dtStartDate AS DATE) <= CAST(TIMEZONE('UTC',now())+INTERVAL '1 day' AS DATE) THEN 1
      WHEN dtNextDate IS NOT NULL AND CAST(dtNextDate AS DATE) <= CAST(TIMEZONE('UTC',now())+INTERVAL '1 day' AS DATE) THEN 1
      ELSE 0
      END);
   end if;
   RETURN;
END; $$;


