-- Stored procedure definition script USP_ManageFavorites for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ManageFavorites(v_byteMode SMALLINT DEFAULT 0,    
v_numContactID NUMERIC(9,0) DEFAULT 0,    
v_numUserCntID NUMERIC(9,0) DEFAULT 0,  
v_cType CHAR(1) DEFAULT NULL)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   if v_byteMode = 0 then

      delete from Favorites where numUserCntID = v_numUserCntID and  numContactID = v_numContactID;
      insert  into Favorites(numUserCntID,numContactID,cType) values(v_numUserCntID,v_numContactID,v_cType);
   end if;    
    
   if v_byteMode = 1 then

      delete from Favorites where numUserCntID = v_numUserCntID and  numContactID = v_numContactID;
   end if;
   RETURN;
END; $$;


