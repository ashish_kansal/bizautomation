-- Stored procedure definition script USP_ConEmpListByTeamId for PostgreSQL
CREATE OR REPLACE FUNCTION USP_ConEmpListByTeamId(v_numDomainID NUMERIC(9,0) DEFAULT 0,    
v_numTeamId NUMERIC(9,0) DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT
   A.numContactId AS numContactID,A.vcFirstName || ' ' || A.vcLastname as vcUserName
   FROM
   AdditionalContactsInformation AS A
   WHERE
   A.numDomainID = v_numDomainID AND
   A.numTeam = v_numTeamId
   ORDER BY
   vcUserName;
END; $$; 












