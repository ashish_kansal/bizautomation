-- Stored procedure definition script USP_CheckDuplicateCheckNo for PostgreSQL
CREATE OR REPLACE FUNCTION USP_CheckDuplicateCheckNo(v_numCheckId NUMERIC(9,0) DEFAULT 0,        
v_numCheckNo NUMERIC(9,0) DEFAULT 0,   
v_numDomainId NUMERIC(9,0) DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for Select Count(*) From CheckDetails Where numCheckNo = v_numCheckNo And numCheckId <> v_numCheckId And numDomainId = v_numDomainId;
END; $$;












