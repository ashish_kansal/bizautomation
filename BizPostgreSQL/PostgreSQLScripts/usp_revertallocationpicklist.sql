-- Stored procedure definition script USP_RevertAllocationPickList for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_RevertAllocationPickList(v_numDomainID NUMERIC(18,0)
	,v_numOppID NUMERIC(18,0)
	,v_numOppBizDocID NUMERIC(18,0)
	,v_numUserCntID NUMERIC(18,0))
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_OppType  VARCHAR(2);              
   v_itemcode  NUMERIC;       
   v_numWareHouseItemID  NUMERIC(18,0);
   v_numWLocationID  NUMERIC(18,0);                     
   v_numUnits  DOUBLE PRECISION;                                              
   v_onHand  DOUBLE PRECISION;                                            
   v_onOrder  DOUBLE PRECISION;                                            
   v_onBackOrder  DOUBLE PRECISION;                                              
   v_onAllocation  DOUBLE PRECISION;
   v_numQtyShipped  DOUBLE PRECISION;
   v_numUnitHourReceived  DOUBLE PRECISION;
   v_numDeletedReceievedQty  DOUBLE PRECISION;
   v_numoppitemtCode  NUMERIC(9,0); 
   v_monAmount  DECIMAL(20,5);   
   v_Kit  BOOLEAN;                                        
   v_bitKitParent  BOOLEAN;		
   v_description  VARCHAR(100);
   v_bitWorkOrder  BOOLEAN;

   v_numRentalIN  DOUBLE PRECISION;
   v_numRentalOut  DOUBLE PRECISION;
   v_numRentalLost  DOUBLE PRECISION;
   v_bitAsset  BOOLEAN;

   v_j  INTEGER DEFAULT 0;
   v_ChildCount  INTEGER;
   v_vcChildDescription  VARCHAR(300);
   v_numChildAllocation  DOUBLE PRECISION;
   v_numChildBackOrder  DOUBLE PRECISION;
   v_numChildOnHand  DOUBLE PRECISION;
   v_numOppChildItemID  INTEGER;
   v_numChildItemCode  INTEGER;
   v_bitChildIsKit  BOOLEAN;
   v_numChildWarehouseItemID  INTEGER;
   v_numChildQty  DOUBLE PRECISION;
   v_numChildQtyShipped  DOUBLE PRECISION;

	

   v_k  INTEGER DEFAULT 1;
   v_CountKitChildItems  INTEGER;
   v_vcKitChildDescription  VARCHAR(300);
   v_numKitChildAllocation  DOUBLE PRECISION;
   v_numKitChildBackOrder  DOUBLE PRECISION;
   v_numKitChildOnHand  DOUBLE PRECISION;
   v_numOppKitChildItemID  NUMERIC(18,0);
   v_numKitChildItemCode  NUMERIC(18,0);
   v_numKitChildWarehouseItemID  NUMERIC(18,0);
   v_numKitChildQty  DOUBLE PRECISION;
   v_numKitChildQtyShipped  DOUBLE PRECISION;

	

   v_numFulfillmentBizDocFromPickList  NUMERIC(18,0);
   SWV_RCur REFCURSOR;
   SWV_RCur2 REFCURSOR;
   SWV_RowCount INTEGER;
BEGIN
   DROP TABLE IF EXISTS tt_TEMPKITSUBITEMS CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPKITSUBITEMS
   (
      ID INTEGER,
      numOppChildItemID NUMERIC(18,0),
      numChildItemCode NUMERIC(18,0),
      bitChildIsKit BOOLEAN,
      numChildWarehouseItemID NUMERIC(18,0),
      numChildQty DOUBLE PRECISION,
      numChildQtyShipped DOUBLE PRECISION
   );
   DROP TABLE IF EXISTS tt_TEMPKITCHILDKITSUBITEMS CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPKITCHILDKITSUBITEMS
   (
      ID INTEGER,
      numOppKitChildItemID NUMERIC(18,0),
      numKitChildItemCode NUMERIC(18,0),
      numKitChildWarehouseItemID NUMERIC(18,0),
      numKitChildQty DOUBLE PRECISION,
      numKitChildQtyShipped DOUBLE PRECISION
   );
   select   numOppBizDocsId INTO v_numFulfillmentBizDocFromPickList FROM OpportunityBizDocs WHERE numoppid = v_numOppID AND numBizDocId = 296 AND numSourceBizDocId = v_numOppBizDocID;
				
   select   numoppitemtCode, OI.numItemCode, coalesce(OBDI.numUnitHour,0), coalesce(OI.numWarehouseItmsID,0), coalesce(numWLocationID,0), (CASE WHEN bitKitParent = true
   AND bitAssembly = true THEN 0
   WHEN bitKitParent = true THEN 1
   ELSE 0
   END), coalesce(OBDI.monTotAmount,0)*(CASE WHEN coalesce(fltExchangeRate,0) = 0 THEN 1 ELSE fltExchangeRate END), coalesce(OBDIFulfillment.numUnitHour,0), coalesce(numUnitHourReceived,0), coalesce(numDeletedReceievedQty,0), coalesce(bitKitParent,false), tintopptype, coalesce(OI.numRentalIN,0), coalesce(OI.numRentalOut,0), coalesce(OI.numRentalLost,0), coalesce(I.bitAsset,false), coalesce(OI.bitWorkOrder,false) INTO v_numoppitemtCode,v_itemcode,v_numUnits,v_numWareHouseItemID,v_numWLocationID,
   v_Kit,v_monAmount,v_numQtyShipped,v_numUnitHourReceived,v_numDeletedReceievedQty,
   v_bitKitParent,v_OppType,v_numRentalIN,v_numRentalOut,
   v_numRentalLost,v_bitAsset,v_bitWorkOrder FROM
   OpportunityBizDocItems OBDI
   LEFT JOIN
   OpportunityBizDocItems OBDIFulfillment
   ON
   OBDI.numOppItemID = OBDIFulfillment.numOppItemID
   AND OBDIFulfillment.numOppBizDocID = v_numFulfillmentBizDocFromPickList
   INNER JOIN
   OpportunityItems OI
   ON
   OBDI.numOppItemID = OI.numoppitemtCode
   LEFT JOIN
   WareHouseItems WI
   ON
   OBDI.numWarehouseItmsID = WI.numWareHouseItemID
   JOIN
   OpportunityMaster OM
   ON
   OI.numOppId = OM.numOppId
   JOIN
   Item I
   ON
   OI.numItemCode = I.numItemCode WHERE
		(charitemtype = 'P' OR 1 =(CASE
   WHEN tintopptype = 1
   THEN CASE WHEN coalesce(I.bitKitParent,false) = true AND coalesce(I.bitAssembly,false) = true THEN 0 WHEN coalesce(I.bitKitParent,false) = true THEN 1 ELSE 0 END
   ELSE 0
   END))
   AND OBDI.numOppBizDocID = v_numOppBizDocID
   AND coalesce(OI.bitDropShip,0) = 0
   AND coalesce(OBDI.numUnitHour,0) > 0   ORDER BY
   OI.numoppitemtCode LIMIT 1;

	
   WHILE v_numoppitemtCode > 0 LOOP
      IF v_numWareHouseItemID > 0 then
        
         select   coalesce(numOnHand,0), coalesce(numAllocation,0), coalesce(numonOrder,0), coalesce(numBackOrder,0) INTO v_onHand,v_onAllocation,v_onOrder,v_onBackOrder FROM
         WareHouseItems WHERE
         numWareHouseItemID = v_numWareHouseItemID;
      end if;
      v_description := CONCAT('SO Pick List Deleted (Qty:',v_numUnits,' Shipped:',v_numQtyShipped,')');
      IF v_Kit = true AND v_numUnits <> v_numQtyShipped then
		
			-- CLEAR DATA OF PREVIOUS ITERATION
         DELETE FROM tt_TEMPKITSUBITEMS;

			-- GET KIT SUB ITEMS DETAIL
         INSERT INTO tt_TEMPKITSUBITEMS(ID,
				numOppChildItemID,
				numChildItemCode,
				bitChildIsKit,
				numChildWarehouseItemID,
				numChildQty,
				numChildQtyShipped)
         SELECT
         ROW_NUMBER() OVER(ORDER BY numOppChildItemID),
				coalesce(numOppChildItemID,0),
				coalesce(I.numItemCode,0),
				coalesce(I.bitKitParent,false),
				coalesce(numWareHouseItemId,0),
				((coalesce(numQtyItemsReq_Orig,0)*coalesce(fn_UOMConversion(OKI.numUOMId,OKI.numChildItemID,v_numDomainID,I.numBaseUnit),1))*v_numUnits),
				((coalesce(numQtyItemsReq_Orig,0)*coalesce(fn_UOMConversion(OKI.numUOMId,OKI.numChildItemID,v_numDomainID,I.numBaseUnit),1))*v_numQtyShipped)
         FROM
         OpportunityKitItems OKI
         JOIN
         Item I
         ON
         OKI.numChildItemID = I.numItemCode
         WHERE
         charitemtype = 'P'
         AND coalesce(numWareHouseItemId,0) > 0
         AND OKI.numOppId = v_numOppID
         AND OKI.numOppItemID = v_numoppitemtCode;
         v_j := 1;
         select   COUNT(*) INTO v_ChildCount FROM tt_TEMPKITSUBITEMS;

			--LOOP OVER ALL CHILD ITEMS AND RELESE ALLOCATION
         WHILE v_j <= v_ChildCount LOOP
            select   numOppChildItemID, numChildItemCode, bitChildIsKit, numChildWarehouseItemID, numChildQty, numChildQtyShipped INTO v_numOppChildItemID,v_numChildItemCode,v_bitChildIsKit,v_numChildWarehouseItemID,
            v_numChildQty,v_numChildQtyShipped FROM
            tt_TEMPKITSUBITEMS WHERE
            ID = v_j;
            select   coalesce(numOnHand,0) INTO v_numChildOnHand FROM WareHouseItems WHERE numWareHouseItemID = v_numChildWarehouseItemID;

				-- IF CHILD ITEM IS KIT THEN LOOK OUT FOR ITS CHILD ITEMS WHICH ARE STORED IN OpportunityKitChildItems TABLE
            IF v_bitChildIsKit = true then
				
					-- IF KIT IS ADDED WITHIN KIT THEN THIS SUB KIT HAS ITS OWN ITEM CONFIGURATION WHICH ID STORED IN OpportunityKitChildItems
               IF EXISTS(SELECT numOppKitChildItemID FROM OpportunityKitChildItems WHERE numOppId = v_numOppID AND numOppItemID = v_numoppitemtCode AND numOppChildItemID = v_numOppChildItemID) then
					
						-- CLEAR DATA OF PREVIOUS ITERATION
                  DELETE FROM tt_TEMPKITCHILDKITSUBITEMS;

						-- GET SUB KIT SUB ITEMS DETAIL
                  INSERT INTO tt_TEMPKITCHILDKITSUBITEMS(ID,
							numOppKitChildItemID,
							numKitChildItemCode,
							numKitChildWarehouseItemID,
							numKitChildQty,
							numKitChildQtyShipped)
                  SELECT
                  ROW_NUMBER() OVER(ORDER BY numOppKitChildItemID),
							coalesce(numOppKitChildItemID,0),
							coalesce(OKCI.numItemID,0),
							coalesce(OKCI.numWareHouseItemId,0),
							((coalesce(OKCI.numQtyItemsReq_Orig,0)*coalesce(fn_UOMConversion(OKCI.numUOMId,OKCI.numItemID,v_numDomainID,I.numBaseUnit),1))*v_numChildQty),
							((coalesce(OKCI.numQtyItemsReq_Orig,0)*coalesce(fn_UOMConversion(OKCI.numUOMId,OKCI.numItemID,v_numDomainID,I.numBaseUnit),1))*v_numChildQtyShipped)
                  FROM
                  OpportunityKitChildItems OKCI
                  JOIN
                  Item I
                  ON
                  OKCI.numItemID = I.numItemCode
                  WHERE
                  charitemtype = 'P'
                  AND coalesce(numWareHouseItemId,0) > 0
                  AND OKCI.numOppId = v_numOppID
                  AND OKCI.numOppItemID = v_numoppitemtCode
                  AND OKCI.numOppChildItemID = v_numOppChildItemID;
                  v_k := 1;
                  select   COUNT(*) INTO v_CountKitChildItems FROM tt_TEMPKITCHILDKITSUBITEMS;
                  WHILE v_k <= v_CountKitChildItems LOOP
                     select   numOppKitChildItemID, numKitChildItemCode, numKitChildWarehouseItemID, numKitChildQty, numKitChildQtyShipped INTO v_numOppKitChildItemID,v_numKitChildItemCode,v_numKitChildWarehouseItemID,
                     v_numKitChildQty,v_numKitChildQtyShipped FROM
                     tt_TEMPKITCHILDKITSUBITEMS WHERE
                     ID = v_k;
                     select   coalesce(numAllocation,0), coalesce(numBackOrder,0) INTO v_numKitChildAllocation,v_numKitChildBackOrder FROM WareHouseItems WHERE numWareHouseItemID = v_numKitChildWarehouseItemID;
                     v_vcKitChildDescription := CONCAT('SO Pick List Child Kit Deleted (Qty:',v_numKitChildQty,' Shipped:',v_numKitChildQtyShipped,
                     ')');
                     IF v_numKitChildQtyShipped > 0 then
							
                        v_numKitChildQty := v_numKitChildQty -v_numKitChildQtyShipped;
                     end if;
                     IF v_numKitChildAllocation+v_numKitChildBackOrder >= v_numKitChildQty then
							
                        UPDATE
                        WareHouseItems
                        SET
                        numOnHand = coalesce(numOnHand,0)+(CASE WHEN coalesce(numBackOrder,0) >= v_numKitChildQty THEN 0 ELSE(v_numKitChildQty -coalesce(numBackOrder,0)) END),numAllocation =(CASE WHEN coalesce(numBackOrder,0) >= v_numKitChildQty THEN coalesce(numAllocation,0) ELSE coalesce(numAllocation,0) -(v_numKitChildQty -coalesce(numBackOrder,0)) END),numBackOrder =(CASE WHEN coalesce(numBackOrder,0) >= v_numKitChildQty THEN coalesce(numBackOrder,0) -v_numKitChildQty ELSE 0 END),
                        dtModified = LOCALTIMESTAMP
                        WHERE
                        numWareHouseItemID = v_numKitChildWarehouseItemID;
                     ELSE
                        RAISE NOTICE 'INVALID_INVENTORY';
                     end if;
                     PERFORM USP_ManageWareHouseItems_Tracking(v_numWareHouseItemID := v_numKitChildWarehouseItemID,v_numReferenceID := v_numOppID,
                     v_tintRefType := 3::SMALLINT,v_vcDescription := v_vcKitChildDescription,
                     v_numModifiedBy := v_numUserCntID,v_numDomainID := v_numDomainID,
                     SWV_RefCur := null);
                     v_k := v_k::bigint+1;
                  END LOOP;
                  v_vcChildDescription := CONCAT('SO Pick List KIT Deleted (Qty:',v_numChildQty,' Shipped:',v_numChildQtyShipped,
                  ')');
                  PERFORM USP_ManageWareHouseItems_Tracking(v_numWareHouseItemID := v_numChildWarehouseItemID,v_numReferenceID := v_numOppID,
                  v_tintRefType := 3::SMALLINT,v_vcDescription := v_vcChildDescription,
                  v_numModifiedBy := v_numUserCntID,v_numDomainID := v_numDomainID,
                  SWV_RefCur := null);
               end if;
            ELSE
               select   coalesce(numAllocation,0), coalesce(numBackOrder,0) INTO v_numChildAllocation,v_numChildBackOrder FROM WareHouseItems WHERE numWareHouseItemID = v_numChildWarehouseItemID;
               v_vcChildDescription := CONCAT('SO Pick List Kit Deleted (Qty:',v_numChildQty,' Shipped:',v_numChildQtyShipped,
               ')');
               IF v_numChildQtyShipped > 0 then
					
                  v_numChildQty := v_numChildQty -v_numChildQtyShipped;
               end if;
               IF v_numChildAllocation+v_numChildBackOrder >= v_numChildQty then
					
                  UPDATE
                  WareHouseItems
                  SET
                  numOnHand = coalesce(numOnHand,0)+(CASE WHEN coalesce(numBackOrder,0) >= v_numChildQty THEN 0 ELSE(v_numChildQty -coalesce(numBackOrder,0)) END),numAllocation =(CASE WHEN coalesce(numBackOrder,0) >= v_numChildQty THEN coalesce(numAllocation,0) ELSE coalesce(numAllocation,0) -(v_numChildQty -coalesce(numBackOrder,0)) END),numBackOrder =(CASE WHEN coalesce(numBackOrder,0) >= v_numChildQty THEN coalesce(numBackOrder,0) -v_numChildQty ELSE 0 END),
                  dtModified = LOCALTIMESTAMP
                  WHERE
                  numWareHouseItemID = v_numChildWarehouseItemID;
               ELSE
                  RAISE NOTICE 'INVALID_INVENTORY';
               end if;
               PERFORM USP_ManageWareHouseItems_Tracking(v_numWareHouseItemID := v_numChildWarehouseItemID,v_numReferenceID := v_numOppID,
               v_tintRefType := 3::SMALLINT,v_vcDescription := v_vcChildDescription,
               v_numModifiedBy := v_numUserCntID,v_numDomainID := v_numDomainID,
               SWV_RefCur := null);
            end if;
            v_j := v_j::bigint+1;
         END LOOP;
      ELSEIF v_numUnits <> v_numQtyShipped
      then
		
         IF v_numQtyShipped > 0 then
			
            v_numUnits := v_numUnits -v_numQtyShipped;
         end if;
         IF v_onAllocation+v_onBackOrder >= v_numUnits then
			
            UPDATE
            WareHouseItems
            SET
            numOnHand = coalesce(numOnHand,0)+(CASE WHEN coalesce(numBackOrder,0) >= v_numUnits THEN 0 ELSE(v_numUnits -coalesce(numBackOrder,0)) END),numAllocation =(CASE WHEN coalesce(numBackOrder,0) >= v_numUnits THEN coalesce(numAllocation,0) ELSE coalesce(numAllocation,0) -(v_numUnits -coalesce(numBackOrder,0)) END),numBackOrder =(CASE WHEN coalesce(numBackOrder,0) >= v_numUnits THEN coalesce(numBackOrder,0) -v_numUnits ELSE 0 END),
            dtModified = LOCALTIMESTAMP
            WHERE
            numWareHouseItemID = v_numWareHouseItemID;
         ELSE
            RAISE NOTICE '%',v_numUnits;
            RAISE NOTICE '%',v_onAllocation;
            RAISE NOTICE '%',v_onBackOrder;
            RAISE NOTICE '%',v_numWareHouseItemID;
            RAISE NOTICE 'INVALID_INVENTORY';
         end if;
      end if;
      IF v_numUnits <> v_numQtyShipped then
		
         IF v_numWareHouseItemID > 0 AND v_description <> 'DO NOT ADD WAREHOUSE TRACKING' then
			
					 --  numeric(9, 0)
					 --  numeric(9, 0)
					 --  tinyint
					 --  varchar(100)
            PERFORM USP_ManageWareHouseItems_Tracking(v_numWareHouseItemID := v_numWareHouseItemID,v_numReferenceID := v_numOppID,
            v_tintRefType := 3::SMALLINT,v_vcDescription := v_description,v_numModifiedBy := v_numUserCntID,
            v_numDomainID := v_numDomainID,SWV_RefCur := null);
         end if;
      end if;
      select   numoppitemtCode, OI.numItemCode, coalesce(OBDI.numUnitHour,0), coalesce(OI.numWarehouseItmsID,0), coalesce(numWLocationID,0), (CASE WHEN bitKitParent = true
      AND bitAssembly = true THEN 0
      WHEN bitKitParent = true THEN 1
      ELSE 0
      END), coalesce(OBDI.monTotAmount,0)*(CASE WHEN coalesce(fltExchangeRate,0) = 0 THEN 1 ELSE fltExchangeRate END), coalesce(OBDIFulfillment.numUnitHour,0), coalesce(numUnitHourReceived,0), coalesce(numDeletedReceievedQty,0), coalesce(bitKitParent,false), tintopptype, coalesce(OI.numRentalIN,0), coalesce(OI.numRentalOut,0), coalesce(OI.numRentalLost,0), coalesce(I.bitAsset,false), coalesce(OI.bitWorkOrder,false) INTO v_numoppitemtCode,v_itemcode,v_numUnits,v_numWareHouseItemID,v_numWLocationID,
      v_Kit,v_monAmount,v_numQtyShipped,v_numUnitHourReceived,v_numDeletedReceievedQty,
      v_bitKitParent,v_OppType,v_numRentalIN,v_numRentalOut,
      v_numRentalLost,v_bitAsset,v_bitWorkOrder FROM
      OpportunityBizDocItems OBDI
      LEFT JOIN
      OpportunityBizDocItems OBDIFulfillment
      ON
      OBDI.numOppItemID = OBDIFulfillment.numOppItemID
      AND OBDIFulfillment.numOppBizDocID = v_numFulfillmentBizDocFromPickList
      INNER JOIN
      OpportunityItems OI
      ON
      OBDI.numOppItemID = OI.numoppitemtCode
      LEFT JOIN
      WareHouseItems WI
      ON
      OBDI.numWarehouseItmsID = WI.numWareHouseItemID
      JOIN
      OpportunityMaster OM
      ON
      OI.numOppId = OM.numOppId
      JOIN
      Item I
      ON
      OI.numItemCode = I.numItemCode WHERE
			(charitemtype = 'P' OR 1 =(CASE
      WHEN tintopptype = 1
      THEN CASE WHEN coalesce(I.bitKitParent,false) = true AND coalesce(I.bitAssembly,false) = true THEN 0 WHEN coalesce(I.bitKitParent,false) = true THEN 1 ELSE 0 END
      ELSE 0
      END))
      AND OBDI.numOppBizDocID = v_numOppBizDocID
      AND coalesce(OI.bitDropShip,0) = false
      AND coalesce(OBDI.numUnitHour,0) > 0
      AND OI.numoppitemtCode > v_numoppitemtCode   ORDER BY
      OI.numoppitemtCode LIMIT 1;
      GET DIAGNOSTICS SWV_RowCount = ROW_COUNT;
      IF SWV_RowCount = 0 then
         v_numoppitemtCode := 0;
      end if;
   END LOOP;
   RETURN;
END; $$;


