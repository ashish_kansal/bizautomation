-- Function definition script GetProjectIncomeExpense for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION GetProjectIncomeExpense(v_numDomainID NUMERIC,v_numProId NUMERIC)
RETURNS TABLE     
(    
   monIncome DECIMAL(20,5),
   monExpense DECIMAL(20,5),
   monBalance DECIMAL(20,5)  
) LANGUAGE plpgsql
   AS $$
   DECLARE
   v_monIncome  DECIMAL(20,5);
   v_monExpense  DECIMAL(20,5);
BEGIN
   DROP TABLE IF EXISTS tt_GETPROJECTINCOMEEXPENSE_RESULT CASCADE;
   CREATE TEMPORARY TABLE IF NOT EXISTS  tt_GETPROJECTINCOMEEXPENSE_RESULT
   (
      monIncome DECIMAL(20,5),
      monExpense DECIMAL(20,5),
      monBalance DECIMAL(20,5)
   );
   v_monIncome := 0;
   v_monExpense := 0;
 
			--Sales Order
   select   v_monIncome+coalesce(SUM(coalesce(OPP.monPrice,0)*CASE numType
   WHEN 1 THEN coalesce(OPP.numUnitHour,0)
   ELSE coalesce(CAST((CAST((EXTRACT(DAY FROM TE.dtToDate -TE.dtFromDate)*60*24+EXTRACT(HOUR FROM TE.dtToDate -TE.dtFromDate)*60+EXTRACT(MINUTE FROM TE.dtToDate -TE.dtFromDate)) AS DECIMAL(10,1))/60) AS DECIMAL(10,1)),0)
   END:: bigint),
   0) INTO v_monIncome FROM    timeandexpense TE
   INNER JOIN OpportunityMaster OM ON TE.numOppId = OM.numOppId
   INNER JOIN OpportunityItems OPP ON OPP.numOppId = TE.numOppId
   AND OPP.numoppitemtCode = TE.numOppItemID AND OPP.numProjectID = TE.numProId
   INNER JOIN Item I ON OPP.numItemCode = I.numItemCode WHERE   numProId = v_numProId
   AND TE.numDomainID = v_numDomainID
   AND numType = 1 --Billable 
   AND OM.tintopptype = 1;

			--Sales Order
   select   v_monIncome+coalesce(SUM(coalesce(OPP.monPrice,0)*coalesce(OPP.numUnitHour,0)),
   0) INTO v_monIncome FROM    OpportunityMaster OM INNER JOIN OpportunityItems OPP ON OPP.numOppId = OM.numOppId
   INNER JOIN Item I ON OPP.numItemCode = I.numItemCode WHERE  OM.numDomainId = v_numDomainID AND OPP.numProjectID = v_numProId
   AND OPP.numProjectStageID = 0 AND OM.tintopptype = 1;

			--Purchase Order
   select   v_monExpense+coalesce(SUM(OPP.monPrice*OPP.numUnitHour),0) INTO v_monExpense from  OpportunityMaster OM
   JOIN OpportunityItems OPP ON OM.numOppId = OPP.numOppId
   LEFT OUTER JOIN Item I ON OPP.numItemCode = I.numItemCode
   LEFT OUTER JOIN Chart_Of_Accounts COA ON I.numCOGsChartAcntId = COA.numAccountId where  OM.numDomainId = v_numDomainID
   AND OPP.numProjectID = v_numProId
   AND OM.tintopptype = 2;	

			 --Bill
   select   v_monExpense+coalesce(SUM(coalesce(BD.monAmount,0)),0) INTO v_monExpense from  ProjectsOpportunities PO
   JOIN BillHeader BH ON BH.numBillID = PO.numBillId
   JOIN BillDetails BD ON BH.numBillID = BD.numBillID AND PO.numProId = BD.numProjectID where  PO.numDomainId = v_numDomainID
   AND PO.numProId = v_numProId
   and coalesce(PO.numBillId,0) > 0;
			
			
			--Bill		
   select   v_monExpense+coalesce(SUM(coalesce(BD.monAmount,0)),0) INTO v_monExpense from  BillHeader BH
   JOIN BillDetails BD ON BH.numBillID = BD.numBillID where  BH.numDomainId = v_numDomainID
   AND BD.numProjectID = v_numProId;
				
				
			--Employee Expense							
   select   v_monExpense+coalesce(SUM(coalesce(Cast((EXTRACT(DAY FROM dttodate -TE.dtFromDate)*60*24+EXTRACT(HOUR FROM dttodate -TE.dtFromDate)*60+EXTRACT(MINUTE FROM dttodate -TE.dtFromDate)) as DOUBLE PRECISION)/Cast(60 as DOUBLE PRECISION),0)*coalesce(UM.monHourlyRate,0)),0) INTO v_monExpense FROM    timeandexpense TE join UserMaster UM on UM.numUserDetailId = TE.numUserCntID WHERE   numProId = v_numProId
   AND TE.numDomainID = v_numDomainID
   AND numType in(1,2) --Non Billable & Billable
   AND TE.numUserCntID > 0;
--					AND bitReimburse=1 and  numCategory=2  

			
			--Deposit
   select   v_monIncome+coalesce(SUM(coalesce(DED.monAmountPaid,0)),0) INTO v_monIncome from  DepositMaster DEM JOIN DepositeDetails DED ON DEM.numDepositId = DED.numDepositID WHERE DEM.tintDepositePage = 1 AND DEM.numDomainId = v_numDomainID
   AND DED.numProjectID = v_numProId;
			
			
			--Check					
   select   v_monExpense+coalesce(SUM(coalesce(CD.monAmount,0)),0) INTO v_monExpense from  CheckHeader CH JOIN CheckDetails CD ON CH.numCheckHeaderID = CD.numCheckHeaderID WHERE CH.tintReferenceType = 1 AND CH.numDomainID = v_numDomainID
   AND CD.numProjectID = v_numProId;		
			
			
			--Journal
   select   v_monExpense+coalesce(SUM(coalesce(GD.numDebitAmt,0)),0) INTO v_monExpense from  General_Journal_Header GH JOIN General_Journal_Details GD ON GH.numJOurnal_Id = GD.numJournalId
   JOIN Chart_Of_Accounts COA ON COA.numAccountId = GD.numChartAcntId WHERE coalesce(GH.numCheckHeaderID,0) = 0 AND coalesce(GH.numBillPaymentID,0) = 0 AND coalesce(GH.numBillID,0) = 0
   AND coalesce(GH.numDepositId,0) = 0 AND coalesce(GH.numOppId,0) = 0 AND coalesce(GH.numOppBizDocsId,0) = 0
   AND coalesce(GH.numReturnID,0) = 0
   AND GH.numDomainId = v_numDomainID  AND GD.numProjectID = v_numProId
   AND coalesce(GD.numDebitAmt,0) > 0	AND (COA.vcAccountCode ilike '0103%' OR  COA.vcAccountCode ilike '0104%' OR  COA.vcAccountCode ilike '0106%');	
				
			
			 --Journal		
   select   v_monIncome+coalesce(SUM(coalesce(GD.numCreditAmt,0)),0) INTO v_monIncome from  General_Journal_Header GH JOIN General_Journal_Details GD ON GH.numJOurnal_Id = GD.numJournalId
   JOIN Chart_Of_Accounts COA ON COA.numAccountId = GD.numChartAcntId WHERE coalesce(GH.numCheckHeaderID,0) = 0 AND coalesce(GH.numBillPaymentID,0) = 0 AND coalesce(GH.numBillID,0) = 0
   AND coalesce(GH.numDepositId,0) = 0 AND coalesce(GH.numOppId,0) = 0 AND coalesce(GH.numOppBizDocsId,0) = 0
   AND coalesce(GH.numReturnID,0) = 0
   AND GH.numDomainId = v_numDomainID  AND GD.numProjectID = v_numProId
   AND coalesce(GD.numCreditAmt,0) > 0  AND (COA.vcAccountCode ilike '0103%' OR  COA.vcAccountCode ilike '0104%' OR  COA.vcAccountCode ilike '0106%');
					
 
   INSERT INTO tt_GETPROJECTINCOMEEXPENSE_RESULT
   SELECT coalesce(v_monIncome,0),coalesce(v_monExpense,0),coalesce(v_monIncome,0) -coalesce(v_monExpense,0);
 
 RETURN QUERY (SELECT * FROM tt_GETPROJECTINCOMEEXPENSE_RESULT);
END; $$;

