-- Stored procedure definition script USP_GetCountFromJournalDetails for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetCountFromJournalDetails(v_numChartAcntId  NUMERIC(9,0) DEFAULT 0,                           
 v_numAcntTypeId NUMERIC(9,0) DEFAULT 0,                                  
 v_numDomainId NUMERIC(9,0) DEFAULT 0,  
 v_dtToDate TIMESTAMP DEFAULT NULL,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numAccountIdOpeningEquity  NUMERIC(9,0);
BEGIN
   Select numAccountId INTO v_numAccountIdOpeningEquity From Chart_Of_Accounts Where bitOpeningBalanceEquity = true  and numDomainId = v_numDomainId;         
                   
                                                                                                                                           
   open SWV_RefCur for Select  Count(*) From General_Journal_Header  GJH
   Inner Join  General_Journal_Details GJD on GJH.numJOurnal_Id = GJD.numJournalId
   where GJD.numChartAcntId in(Select numAccountId From Chart_Of_Accounts Where numAcntTypeId = v_numAcntTypeId And numDomainId = v_numDomainId And coalesce(bitOpeningBalanceEquity,false) = false)
   And GJH.numDomainId = v_numDomainId And GJH.datEntry_Date <= CAST('' || CAST(v_dtToDate AS VARCHAR(300)) AS timestamp);
END; $$;












