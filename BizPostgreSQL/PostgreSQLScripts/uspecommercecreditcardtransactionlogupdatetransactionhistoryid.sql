-- Stored procedure definition script USPECommerceCreditCardTransactionLogUpdateTransactionHistoryID for PostgreSQL
CREATE OR REPLACE FUNCTION USPECommerceCreditCardTransactionLogUpdateTransactionHistoryID(v_numID NUMERIC(18,0)
	,v_numTransHistoryID NUMERIC(18,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   UPDATE
   ECommerceCreditCardTransactionLog
   SET
   numTransHistoryID = v_numTransHistoryID
   WHERE
   ID = v_numID;

   open SWV_RefCur for SELECT cast(CURRVAL('Table_seq') as VARCHAR(255));
END; $$;













