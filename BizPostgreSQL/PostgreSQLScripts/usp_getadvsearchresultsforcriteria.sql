-- Stored procedure definition script usp_GetAdvSearchResultsForCriteria for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION usp_GetAdvSearchResultsForCriteria(v_vcColumnsList VARCHAR(500),                                                    
  v_vcWhereCondition VARCHAR(4000),                                               
  v_vcAdditionalWhereCondition VARCHAR(200),                           
  v_vcCustomFieldGroups VARCHAR(20),                                                 
  v_vcCustomFieldSearchKeyword VARCHAR(100),                                  
  v_vcSortColumn VARCHAR(50),                                                   
  v_vcSortOrder VARCHAR(50),                           
  v_tIntSearchContext SMALLINT,                   
  v_bitOnlyPrimaryContacts BOOLEAN,                                               
  v_vcRatingInSurvey VARCHAR(75),                           
  v_tIntEmailLink SMALLINT,                                              
  v_numNosRowsReturned INTEGER,                                                 
  v_numCurrentPage INTEGER,                                               
  v_numUserCntId NUMERIC,                                               
  v_numDomainId NUMERIC, INOUT SWV_RefCur refcursor)
LANGUAGE plpgsql                                            
  --SET @vcWhereCondition = Replace(@vcWhereCondition,'''', char(39))                    
  --EXEC ('usp_CreateTempTableForAdvSearch')                                                           
   AS $$
   DECLARE
   v_vcNewColumnsList  VARCHAR(2000);
   v_sqlString  VARCHAR(8000);                                                          
   v_sqlStringSelect  VARCHAR(8000);               
   v_sqlStringCustomFieldSelect  VARCHAR(8000);                          
   v_sqlStringDropTable  VARCHAR(100);                                                    
   v_sqlCombinedStringSelect  VARCHAR(8000);                                                           
   v_sqlCountString  VARCHAR(8000);                                                      
   v_sqlStringCoalesce  VARCHAR(8000);                                 
   v_sqlFilterPrimaryString  VARCHAR(100);         
   v_sqlRatingInSurvey  VARCHAR(400);        
   v_intTopOfResultSet  INTEGER;                                              
   v_intStartOfResultSet  INTEGER;
   v_vcStartSurveyRating  VARCHAR(10);        
   v_vcEndSurveyRating  VARCHAR(10);        
   v_vcSurveyIds  VARCHAR(50);               
                          
   v_vcCustomColumnsSelectQuery  VARCHAR(1500);                          
   v_vcCustomColumnsWhereCondition  VARCHAR(1000);                          
   v_vcCustomColumnsSalesPurchaseOppWhereCondition  VARCHAR(100);
BEGIN
   IF v_numCurrentPage <> -9652 then
  
      v_vcNewColumnsList := REPLACE(v_vcColumnsList,'vcCompanyName','''<a href="javascript: GoOrgDetails('' + Cast(numDivisionId AS NVarchar) + '');">'' + vcCompanyName + ''</a>'' AS vcCompanyName');
      IF v_tIntEmailLink = 1 then
         v_vcNewColumnsList := REPLACE(v_vcNewColumnsList,'vcEmail','''<a href="mailto:'' + vcEmail + ''">'' + vcEmail + ''</a>'' AS vcEmail');
      ELSE
         v_vcNewColumnsList := REPLACE(v_vcNewColumnsList,'vcEmail','''<a href="../common/callemail.aspx?Lsemail='' + vcEmail + ''&ContID='' + Cast(numContactId AS NVarchar) + ''" target=_blank>'' + vcEmail + ''</a>'' AS vcEmail');
      end if;
      v_vcNewColumnsList := REPLACE(v_vcNewColumnsList,'vcFirstName','''<a href="javascript: GoContactDetails('' + Cast(numContactId AS NVarchar) + '');">'' + vcFirstName + ''</a>'' AS vcFirstName');
      v_vcNewColumnsList := REPLACE(v_vcNewColumnsList,'vcLastName','''<a href="javascript: GoContactDetails('' + Cast(numContactId AS NVarchar) + '');">'' + vcLastName + ''</a>'' AS vcLastName');
      v_vcNewColumnsList := coalesce(v_vcNewColumnsList,'') || ',''<input Type=CheckBox name=cbSR'' + Cast(numCompanyID AS NVarchar) + '' value="'' + Cast(numCompanyID AS NVarchar) + ''_'' + Cast(numContactID AS NVarchar) + ''_'' +                         
                                                  Cast(numDivisionID AS NVarchar) + ''">'' AS ''cbBox''';
   ELSE
      v_vcNewColumnsList := coalesce(v_vcColumnsList,'') || ','''' AS cbBox';
   end if;                                  
                   
  --PRINT @vcNewColumnsList                                  
   v_intTopOfResultSet := v_numNosRowsReturned::bigint*v_numCurrentPage::bigint;                                                          
   v_intStartOfResultSet := v_numNosRowsReturned::bigint*(v_numCurrentPage::bigint -1);                  
                
   IF v_bitOnlyPrimaryContacts = true then
      v_sqlFilterPrimaryString := ' AND numContactType = 70 ';
   ELSE
      v_sqlFilterPrimaryString := '';
   end if;                    
   IF v_vcRatingInSurvey <> '' then
      v_vcSurveyIds := PARSENAME(v_vcRatingInSurvey,1);
      v_vcEndSurveyRating := PARSENAME(v_vcRatingInSurvey,2);
      v_vcStartSurveyRating := PARSENAME(v_vcRatingInSurvey,3);
      v_sqlRatingInSurvey := ' AND EXISTS(SELECT numRegisteredRespondentContactId FROM SurveyRespondentsMaster ' || CASE WHEN v_vcSurveyIds = '*' THEN '' ELSE ' WHERE numSurID IN (' || coalesce(v_vcSurveyIds,'') || ')' END  || ' GROUP BY numRegisteredRespondentContactId 
  
   
       
       
                                          HAVING SUM(numSurRating) >= ' || coalesce(v_vcStartSurveyRating,'') || ' AND SUM(numSurRating) <=  ' || coalesce(v_vcEndSurveyRating,'') ||
      ' AND numRegisteredRespondentContactId = numContactId        
                                         )';
   ELSE
      v_sqlRatingInSurvey := '';
   end if;               
                          
   IF v_vcCustomFieldGroups <> '' then
      select   COALESCE(coalesce(v_vcCustomColumnsSelectQuery,'') || ' UNION ','') ||
      'SELECT ' || CASE Items When 1 Then '''D''' When 4 THEN '''C''' ELSE '''O''' END || ' AS cType, M.Fld_ID, V.RecId,                          
                Case WHEN (M.Fld_type = ''CheckBox'' AND V.Fld_Value = 1) THEN M.Fld_Label                        
                WHEN (M.Fld_type = ''SelectBox'') THEN dbo.fn_AdvSearchColumnName(V.Fld_Value,''L'')                        
                ELSE V.Fld_Value                        
                END As Fld_Value FROM CFW_Fld_Master M,' ||  CASE Items When 1 Then 'CFW_FLD_Values' When 4 THEN 'CFW_FLD_Values_Cont' ELSE 'CFW_FLD_Values_Opp' END || ' V ' ||
      'WHERE M.Fld_id = V.Fld_ID' INTO v_vcCustomColumnsSelectQuery FROM Split(v_vcCustomFieldGroups,',');
      select   COALESCE(coalesce(v_vcCustomColumnsWhereCondition,'') || ' OR ','') ||
      '(Fld_Value LIKE ' || Case WHEN v_tIntSearchContext = 1 THEN '''%' ELSE '''' END || coalesce(v_vcCustomFieldSearchKeyword,'') || '%''' ||
      ' AND cType = ' || CASE Items When 1 Then '''D''' When 4 THEN '''C''' ELSE '''O''' END ||
      ' AND RecId = ' || CASE Items When 1 Then 'numDivisionId' When 4 THEN 'numContactId' ELSE 'numOppId' END || ')' INTO v_vcCustomColumnsWhereCondition FROM Split(v_vcCustomFieldGroups,',');
      v_vcCustomColumnsSalesPurchaseOppWhereCondition := CASE WHEN (POSITION(',2,' IN ',' || coalesce(v_vcCustomFieldGroups,'') || ',') > 0 AND POSITION(',6,' IN ',' || coalesce(v_vcCustomFieldGroups,'') || ',') > 0) THEN ''
      WHEN (POSITION(',2,' IN ',' || coalesce(v_vcCustomFieldGroups,'') || ',') > 0 AND POSITION(',6,' IN ',' || coalesce(v_vcCustomFieldGroups,'') || ',') = 0) THEN ' AND tIntOppType = 1 '
      WHEN (POSITION(',2,' IN ',' || coalesce(v_vcCustomFieldGroups,'') || ',') = 0 AND POSITION(',6,' IN ',' || coalesce(v_vcCustomFieldGroups,'') || ',') > 0) THEN ' AND tIntOppType = 2 '
      ELSE ''
      END;
   end if;                          
                          
   IF v_vcCustomFieldGroups = '' then  --No Custom Fields Involved in the Search                          
      v_sqlString :=  'SELECT TOP 100 PERCENT ' || coalesce(v_vcColumnsList,'') || '                                                                 
     FROM dbo.DynamicAdvSearchView AS TableOne WHERE ' || coalesce(v_vcWhereCondition,'') || coalesce(v_vcAdditionalWhereCondition,'') || coalesce(v_sqlFilterPrimaryString,'') || coalesce(v_sqlRatingInSurvey,'') || '  
     AND numDomainId = ' || SUBSTR(Cast(v_numDomainId As VARCHAR(30)),1,30) || '   
     GROUP BY ' || coalesce(v_vcColumnsList,'') || '                                                     
     ORDER BY ' || coalesce(v_vcSortColumn,'') || ' ' || coalesce(v_vcSortOrder,'');
   ELSE  --Custom Fields Involved in the Search                          
      v_sqlString :=  'SELECT TOP 100 PERCENT ' || coalesce(v_vcColumnsList,'') || '                                                                 
     FROM dbo.DynamicAdvSearchView AS TableOne, (' || coalesce(v_vcCustomColumnsSelectQuery,'') || ') TableCustomOne WHERE ' || coalesce(v_vcWhereCondition,'') || coalesce(v_vcAdditionalWhereCondition,'') || coalesce(v_sqlFilterPrimaryString,'') || coalesce(v_sqlRatingInSurvey,'') || '    
     AND (' || coalesce(v_vcCustomColumnsWhereCondition,'') || ') ' ||
      coalesce(v_vcCustomColumnsSalesPurchaseOppWhereCondition,'') || '   
     AND numDomainId = ' || SUBSTR(Cast(v_numDomainId As VARCHAR(30)),1,30) || '                                                        
     GROUP BY ' || coalesce(v_vcColumnsList,'') || '                                                     
     ORDER BY ' || coalesce(v_vcSortColumn,'') || ' ' || coalesce(v_vcSortOrder,'');
   end if;                                              
                          
                                              
   v_sqlString := 'Select IDENTITY(int) AS numRow, ' || coalesce(v_vcColumnsList,'') || ' INTO #tmpTableX FROM (' || coalesce(v_sqlString,'') || ') AS tmpTable1';                                            
   IF v_numCurrentPage <> -9652 then
      v_sqlStringSelect := 'SELECT ' || coalesce(v_vcNewColumnsList,'') || ' FROM #tmpTableX WHERE numRow > ' || SUBSTR(CAST(v_intStartOfResultSet AS VARCHAR(9)),1,9) ||  ' AND numRow <= ' || SUBSTR(CAST(v_intTopOfResultSet AS VARCHAR(9)),1,9);
   ELSE
      v_sqlStringSelect := 'SELECT TOP 100 PERCENT ' || coalesce(v_vcNewColumnsList,'') || ' FROM #tmpTableX';
   end if;                                       
                                      
                                            
   v_sqlStringCoalesce := 'SELECT TOP 100 PERCENT Cast(numCompanyID AS NVarchar) + ''_'' + Cast(numContactID AS NVarchar) + ''_'' + Cast(numDivisionID AS NVarchar) AS ''vcCoalesce'' FROM #tmpTableX';                        
   v_sqlCountString := 'SELECT COALESCE(Max(numRow),0) AS RowCountRecords FROM #tmpTableX';                              
                                  
   v_sqlStringDropTable := 'DROP TABLE #tmpTableX';                                            
                                          
   v_sqlCombinedStringSelect := coalesce(v_sqlString,'') ||
   CHR(10) ||
   coalesce(v_sqlStringSelect,'') ||
   CHR(10) ||
   coalesce(v_sqlCountString,'') ||
   CHR(10) ||
   coalesce(v_sqlStringCoalesce,'') ||
   CHR(10) ||
   coalesce(v_sqlStringDropTable,'');                                            
                                            
   OPEN SWV_RefCur FOR EXECUTE v_sqlCombinedStringSelect;                                            
   RAISE NOTICE '%',(v_sqlCombinedStringSelect);
   RETURN;
END; $$;


