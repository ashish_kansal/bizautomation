-- Function definition script GetOpportunityStage for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION GetOpportunityStage(v_numOppId NUMERIC(9,0),v_numUsercntId NUMERIC(9,0),v_numDomainId NUMERIC(9,0))
RETURNS VARCHAR(4000) LANGUAGE plpgsql
   AS $$
   DECLARE
   v_stage  VARCHAR(4000);
BEGIN
   v_stage := '';
   select   case when v_stage = '' then vcStageDetail else  coalesce(v_stage,'') || '<br>' || vcStageDetail end INTO v_stage from OpportunityStageDetails OSd
   join  OpportunityMaster opp    on opp.numOppId = OSd.numoppid where OSd.numoppid = v_numOppId and  bitStageCompleted = false
   and OSd.numAssignTo = v_numUsercntId and opp.numDomainId = v_numDomainId;
--	print @stage
   return v_stage;
END; $$;

