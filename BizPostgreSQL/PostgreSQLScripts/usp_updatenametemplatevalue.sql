-- Stored procedure definition script USP_UpdateNameTemplateValue for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_UpdateNameTemplateValue(v_tintType SMALLINT,
v_numDomainID NUMERIC,
v_RecordID NUMERIC)
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_Tempate  VARCHAR(200);
   v_OrgName  VARCHAR(100);
	
   v_numSequenceId  NUMERIC(9,0);
   v_numMinLength  NUMERIC(9,0);
BEGIN
   v_numSequenceId := 0;

   IF NOT EXISTS(SELECT * FROM NameTemplate WHERE   numDomainID = v_numDomainID AND tintModuleID = 1) then
                
      INSERT  INTO NameTemplate(numDomainID,vcModuleName,vcNameTemplate,
                                                    tintModuleID,numSequenceId,numMinLength,numRecordID)
      SELECT  v_numDomainID,vcModuleName,vcNameTemplate,
                                    tintModuleID,numSequenceId,numMinLength,numRecordID
      FROM    NameTemplate
      WHERE   numDomainID = 0	AND tintModuleID = 1;
   end if;
		
	--Get Custom Template If enabled
   select   coalesce(vcNameTemplate,''), coalesce(numSequenceId,1), coalesce(numMinLength,4) INTO v_Tempate,v_numSequenceId,v_numMinLength FROM NameTemplate WHERE numDomainID = v_numDomainID
   AND tintModuleID = 1 AND numRecordID = v_tintType;
	
	--Replace Other Values
   v_Tempate := REPLACE(v_Tempate,'$YEAR$',TO_CHAR(TIMEZONE('UTC',now()),'YYYY'));
   v_Tempate := REPLACE(v_Tempate,'$MONTH$',TO_CHAR(TIMEZONE('UTC',now()),'Month'));
   v_Tempate := REPLACE(v_Tempate,'$DAY$',TO_CHAR(TIMEZONE('UTC',now()),'DD'));
   v_Tempate := REPLACE(v_Tempate,'$QUARTER$',EXTRACT(quarter FROM TIMEZONE('UTC',now()))::VARCHAR);
		
--		SELECT DATENAME(quarter,GETUTCDATE())
	
		--Replace Organization Name
   IF POSITION(lower(v_Tempate) IN '$organizationname$') >= 0 then
		
      select   coalesce(C.vcCompanyName,'') INTO v_OrgName FROM OpportunityMaster OM INNER JOIN DivisionMaster DM ON OM.numDivisionId = DM.numDivisionID
      INNER JOIN CompanyInfo C ON C.numCompanyId = DM.numCompanyID WHERE numOppId = v_RecordID;
      v_Tempate := REPLACE(v_Tempate,'$OrganizationName$',v_OrgName);
   end if;
	
   v_Tempate := REPLACE(v_Tempate,'$OrderID$',v_RecordID:: TEXT );
		
   v_Tempate := coalesce(v_Tempate,'') || coalesce(REPEAT('0',(v_numMinLength -LENGTH(v_numSequenceId::VARCHAR))::INT),'') || SUBSTR(CAST(v_numSequenceId AS VARCHAR(18)),1,18);
	
   UPDATE OpportunityMaster SET vcpOppName = coalesce(v_Tempate,coalesce(vcpOppName,'')) WHERE numOppId = v_RecordID;		
		
   UPDATE NameTemplate SET numSequenceId = v_numSequenceId+1  WHERE numDomainID = v_numDomainID
   AND tintModuleID = 1 AND numRecordID = v_tintType;
   RETURN;
END; $$;
/*
EXEC USP_UpdateNameTemplateValue
	@tintType = 1, -- TINYINT
	@numDomainID = 1, -- NUMERIC
	@RecordID = 12494 -- numeric
SELECT * FROM dbo.OpportunityMaster WHERE numDomainId=1	AND numOppId=12494

EXEC USP_UpdateNameTemplateValue
	@tintType = 3, -- TINYINT
	@numDomainID = 1, -- NUMERIC
	@RecordID = 12138 -- numeric
SELECT * FROM dbo.OpportunityBizDocs WHERE numOppId=12494
*/

