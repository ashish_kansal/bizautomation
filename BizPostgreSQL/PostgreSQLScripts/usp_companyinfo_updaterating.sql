-- Stored procedure definition script USP_CompanyInfo_UpdateRating for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_CompanyInfo_UpdateRating(v_numDomainID NUMERIC(18,0),
	v_numDivisionID NUMERIC(18,0),
	v_tintPerformance SMALLINT)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   BEGIN
      -- BEGIN TRANSACTION
UPDATE
      CompanyInfo CI
      SET
      numCompanyRating = coalesce((SELECT numOrganizationRatingID FROM OrganizationRatingRule ORR WHERE ORR.numDomainID = v_numDomainID AND Temp.Amount  BETWEEN ORR.numFromAmount:: DECIMAL(20,5) AND ORR.numToAmount:: DECIMAL(20,5)),0)
      FROM
      DivisionMaster D LEFT JOIN LATERAL(SELECT
      SUM(OM.monDealAmount) AS Amount
      FROM
      OpportunityMaster OM
      WHERE
      OM.numDivisionId = D.numDivisionID
      AND OM.tintopptype = 1
      AND tintoppstatus = 1
      AND 1 =(CASE v_tintPerformance
      WHEN 1 THEN(CASE WHEN OM.bintCreatedDate BETWEEN TIMEZONE('UTC',now())+INTERVAL '-1 month' AND TIMEZONE('UTC',now()):: BYTEA THEN 1 ELSE 0 END)
      WHEN 2 THEN(CASE WHEN OM.bintCreatedDate BETWEEN TIMEZONE('UTC',now())+INTERVAL '-3 month' AND TIMEZONE('UTC',now()):: BYTEA THEN 1 ELSE 0 END)
      WHEN 3 THEN(CASE WHEN OM.bintCreatedDate BETWEEN TIMEZONE('UTC',now())+INTERVAL '-1 year' AND TIMEZONE('UTC',now()):: BYTEA THEN 1 ELSE 0 END)
      END)) AS Temp ON TRUE
      WHERE
      CI.numCompanyId = D.numCompanyID AND(CI.numDomainID = v_numDomainID
      AND D.numDomainID = v_numDomainID
      AND(D.numDivisionID = v_numDivisionID OR v_numDivisionID = 0));
      -- COMMIT
EXCEPTION WHEN OTHERS THEN
         NULL;
         -- ROLLBACK TRANSACTION
END;
   RETURN;
END; $$;

