-- Stored procedure definition script usp_getFormConfigFields for PostgreSQL
CREATE OR REPLACE FUNCTION usp_getFormConfigFields(v_numDomainId NUMERIC(9,0),                                     
 v_numFormId INTEGER,            
 v_numAuthGroupId NUMERIC(9,0),
 v_numBizDocTemplateID NUMERIC(9,0) DEFAULT 0,
v_numSubFormId NUMERIC(9,0) DEFAULT NULL,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT DISTINCT numFormId, numFieldID || vcFieldType AS numFormFieldId, vcFieldName as vcFormFieldName, vcFieldName As vcNewFormFieldName,
 vcFieldType,
 vcAssociatedControlType, numListID,
 vcDbColumnName, vcListItemType,tintColumn as intColumnNum,tintRow as intRowNum,bitRequired as boolRequired,
 boolAOIField, numAuthGroupId, vcFieldDataType ,coalesce(vcLookBackTableName,'') AS vcLookBackTableName,
 coalesce(numSubFormId,0) AS numSubFormId
   FROM View_DynamicColumns
   WHERE numFormId = v_numFormId AND numDomainID = v_numDomainId
   AND numAuthGroupId = v_numAuthGroupId
   and (numRelCntType = v_numBizDocTemplateID or v_numBizDocTemplateID = 0)
   AND coalesce(numSubFormId,0) = coalesce(v_numSubFormId,0)
   AND coalesce(vcLookBackTableName,'') =(CASE v_numAuthGroupId WHEN 40911 THEN 'CSGrid' ELSE REPLACE(coalesce(vcLookBackTableName,''),'CSGrid','') END);
END; $$;
