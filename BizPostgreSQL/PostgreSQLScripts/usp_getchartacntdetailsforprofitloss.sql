-- Stored procedure definition script USP_GetChartAcntDetailsForProfitLoss for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetChartAcntDetailsForProfitLoss(v_numDomainId NUMERIC(9,0),                                          
v_dtFromDate TIMESTAMP,                                        
v_dtToDate TIMESTAMP,
v_ClientTimeZoneOffset INTEGER, --Added by Chintan to enable calculation of date according to client machine                                                   
v_numAccountClass NUMERIC(9,0) DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_CURRENTPL  DECIMAL(20,5);
   v_PLOPENING  DECIMAL(20,5);
   v_PLCHARTID  NUMERIC(8,0);
--DECLARE @numFinYear INT;
   v_TotalIncome  DECIMAL(20,5);
   v_TotalExpense  DECIMAL(20,5);
   v_TotalCOGS  DECIMAL(20,5);
   v_dtFinYearFrom  TIMESTAMP;
   v_CurrentPL_COA  DECIMAL(20,5);


--set @numFinYear= (SELECT numFinYearId FROM FINANCIALYEAR WHERE dtPeriodFrom <= @dtFromDate AND  
--dtPeriodTo >=@dtFromDate AND numDomainId= @numDomainId);

--set @dtFinYearFrom = (SELECT dtPeriodFrom FROM FINANCIALYEAR WHERE dtPeriodFrom <= @dtFromDate AND  
--dtPeriodTo >=@dtFromDate AND numDomainId= @numDomainId);
BEGIN
   DROP TABLE IF EXISTS tt_VIEW_JOURNAL CASCADE;
   CREATE TEMPORARY TABLE tt_VIEW_JOURNAL AS
      SELECT * FROM VIEW_JOURNAL WHERE numDomainID = v_numDomainId AND (numAccountClass = v_numAccountClass OR v_numAccountClass = 0);

   select   MIN(datentry_date) INTO v_dtFinYearFrom FROM tt_VIEW_JOURNAL WHERE numDomainId = v_numDomainId;


   v_dtFromDate := v_dtFromDate+CAST(v_ClientTimeZoneOffset || 'minute' as interval);
   v_dtToDate := v_dtToDate+CAST(v_ClientTimeZoneOffset || 'minute' as interval);


--select * from view_journal where numDomainid=72

   DROP TABLE IF EXISTS tt_PLSUMMARY CASCADE;
   CREATE TEMPORARY TABLE tt_PLSUMMARY 
   (
      numAccountId NUMERIC(9,0),
      vcAccountName VARCHAR(250),
      numParntAcntTypeID NUMERIC(9,0),
      vcAccountDescription VARCHAR(250),
      vcAccountCode VARCHAR(50),
      Opening DECIMAL(20,5),
      Debit DECIMAL(20,5),
      Credit DECIMAL(20,5),
      bitIsSubAccount BOOLEAN
   );

   INSERT INTO  tt_PLSUMMARY
   SELECT COA.numAccountId,vcAccountName,numParntAcntTypeID,vcAccountDescription,vcAccountCode,

/*isnull((SELECT SUM(isnull(monOpening,0)) from CHARTACCOUNTOPENING CAO WHERE
--numFinYearId=@numFinYear and numDomainID=@numDomainId 
--AND CAO.numAccountId IN (SELECT numAccountId FROM dbo.Chart_Of_Accounts WHERE numDomainId = @numDomainId AND vcAccountCode like COA.vcAccountCode + '--' ) ),0) 
--
--+*/

/*ISNULL((SELECT sum(Debit-Credit) FROM #view_journal VJ
--WHERE VJ.numDomainId=@numDomainId AND
--	VJ.COAvcAccountCode like COA.vcAccountCode + '--' AND
--	datEntry_Date BETWEEN @dtFinYearFrom AND  DATEADD(Minute,-1,@dtFromDate) ),0)*/ 0 AS OPENING,
coalesce((SELECT sum(Debit) FROM tt_VIEW_JOURNAL VJ
      WHERE VJ.numDomainId = v_numDomainId AND
      VJ.COAvcAccountCode ilike COA.vcAccountCode || '%' /*VJ.numAccountId=COA.numAccountId*/ AND
      datentry_date BETWEEN  v_dtFromDate AND v_dtToDate),0) as DEBIT,
coalesce((SELECT sum(Credit) FROM tt_VIEW_JOURNAL VJ
      WHERE VJ.numDomainId = v_numDomainId AND
      VJ.COAvcAccountCode ilike COA.vcAccountCode || '%' /*VJ.numAccountId=COA.numAccountId*/ AND
      datentry_date BETWEEN  v_dtFromDate AND v_dtToDate),0) as CREDIT,
	coalesce(COA.bitIsSubAccount,false)
   FROM Chart_Of_Accounts COA
   WHERE COA.numDomainId = v_numDomainId AND COA.bitActive = true AND
      (COA.vcAccountCode ilike '0103%' OR
   COA.vcAccountCode ilike '0104%' OR
   COA.vcAccountCode ilike '0106%');

   DROP TABLE IF EXISTS tt_PLOUTPUT CASCADE;
   CREATE TEMPORARY TABLE tt_PLOUTPUT 
   (
      numAccountId NUMERIC(9,0),
      vcAccountName VARCHAR(250),
      numParntAcntTypeID NUMERIC(9,0),
      vcAccountDescription VARCHAR(250),
      vcAccountCode VARCHAR(50),
      Opening DECIMAL(20,5),
      Debit DECIMAL(20,5),
      Credit DECIMAL(20,5)
   );

   INSERT INTO tt_PLOUTPUT
   SELECT ATD.numAccountTypeID,CAST(ATD.vcAccountType AS VARCHAR(250)),ATD.numParentID, CAST('' AS VARCHAR(250)),ATD.vcAccountCode,
 coalesce(SUM(Opening),0) as Opening,
coalesce(Sum(Debit),0) as Debit,coalesce(Sum(Credit),0) as Credit
   FROM
   AccountTypeDetail ATD RIGHT OUTER JOIN
   tt_PLSUMMARY PL ON
   PL.vcAccountCode ilike ATD.vcAccountCode || '%'
   AND ATD.numDomainID = v_numDomainId AND
(ATD.vcAccountCode ilike '0103%' OR
   ATD.vcAccountCode ilike '0104%' OR
   ATD.vcAccountCode ilike '0106%')
   WHERE
   PL.bitIsSubAccount = false
   GROUP BY
   ATD.numAccountTypeID,ATD.vcAccountCode,ATD.vcAccountType,ATD.numParentID;



   ALTER TABLE tt_PLSUMMARY
   DROP COLUMN bitIsSubAccount;

--SELECT * FROM #PLOutPut
--------------------------------------------------------
-- GETTING P&L VALUE
   v_CURRENTPL := 0;	
   v_PLOPENING := 0;


   select   COA.numAccountId INTO v_PLCHARTID FROM Chart_Of_Accounts COA WHERE numDomainId = v_numDomainId and
   bitProfitLoss = true;

   select   coalesce(sum(Credit),0) -coalesce(sum(Debit),0) INTO v_CurrentPL_COA FROM tt_VIEW_JOURNAL VJ WHERE numDomainId = v_numDomainId
   AND numAccountId = v_PLCHARTID AND datentry_date between  v_dtFromDate AND v_dtToDate;

   select   coalesce(sum(Opening),0)+coalesce(sum(Credit),0) -coalesce(sum(Debit),0) INTO v_TotalIncome FROM
   tt_PLOUTPUT P WHERE
   vcAccountCode IN('0103');

   select   coalesce(sum(Opening),0)+coalesce(sum(Credit),0) -coalesce(sum(Debit),0) INTO v_TotalExpense FROM
   tt_PLOUTPUT P WHERE
   vcAccountCode IN('0104');

   select   coalesce(sum(Opening),0)+coalesce(sum(Credit),0) -coalesce(sum(Debit),0) INTO v_TotalCOGS FROM
   tt_PLOUTPUT P WHERE
   vcAccountCode IN('0106');

   RAISE NOTICE 'TotalIncome=									%',SUBSTR(CAST(v_TotalIncome AS VARCHAR(20)),1,20);
   RAISE NOTICE 'TotalExpense=									%',SUBSTR(CAST(v_TotalExpense AS VARCHAR(20)),1,20);
   RAISE NOTICE 'TotalCOGS=										%',SUBSTR(CAST(v_TotalCOGS AS VARCHAR(20)),1,20);

   v_CURRENTPL := v_CurrentPL_COA+v_TotalIncome+v_TotalExpense+v_TotalCOGS; 
   RAISE NOTICE 'Current Profit/Loss = (Income - expense - cogs)= %',SUBSTR(CAST(v_CURRENTPL AS VARCHAR(20)),1,20);
--PRINT @CURRENTPL


--SELECT @CURRENTPL = @CURRENTPL - ISNULL(SUM(Opening),0)+ISNULL(sum(Debit),0)-ISNULL(sum(Credit),0) FROM
--#PLOutPut P WHERE 
--vcAccountCode IN ('0104')

   v_CURRENTPL := v_CURRENTPL*(-1);



   select   coalesce(sum(Credit),0) -coalesce(sum(Debit),0) INTO v_PLOPENING FROM tt_VIEW_JOURNAL VJ WHERE numDomainId = v_numDomainId
   AND (vcAccountCode  ilike '0103%' or  vcAccountCode  ilike '0104%' or  vcAccountCode  ilike '0106%')
   AND datentry_date <=  v_dtFromDate+INTERVAL '-1 minute';

   select   coalesce(v_PLOPENING,0)+coalesce(sum(Credit),0) -coalesce(sum(Debit),0) INTO v_PLOPENING FROM tt_VIEW_JOURNAL VJ WHERE numDomainId = v_numDomainId
   AND numAccountId = v_PLCHARTID AND datentry_date <=  v_dtFromDate+INTERVAL '-1 minute';
--PRINT @PLCHARTID
--SELECT sum(Credit-Debit) FROM #view_journal VJ
--WHERE VJ.numDomainId=@numDomainId AND
--	VJ.numAccountId=@PLCHARTID
--	AND datEntry_Date <= @dtFromDate-1
--	
--	PRINT @dtFromDate
	
/*SELECT   @PLOPENING  = 
--ISNULL((SELECT sum(Credit-Debit) FROM #view_journal VJ
--WHERE VJ.numDomainId=@numDomainId AND
--	VJ.numAccountId=COA.numAccountId 
--	AND datEntry_Date <= @dtFromDate-1 ),0)
-- FROM Chart_of_Accounts COA WHERE numDomainID=@numDomainId and
--bitProfitLoss=1;*/
/*SELECT @PLOPENING = ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM #PLOutPut P WHERE numDomainID=@numDomainID 
--
--AND  vcAccountCode  IN( '0103' ,'0104' ,'0106')
--AND datEntry_Date <=  DATEADD(Minute,-1,@dtFromDate);
--
--SELECT @PLOPENING = ISNULL(@PLOPENING,0) + ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM #PLOutPut P WHERE numDomainID=@numDomainID
--AND numAccountId=@PLCHARTID
--*/
--SELECT  @CURRENTPL=@CURRENTPL + 		   
--ISNULL((SELECT sum(Credit-Debit) FROM #view_journal VJ
--WHERE VJ.numDomainId=@numDomainId AND
--	VJ.numAccountId=@PLCHARTID AND
--	datEntry_Date BETWEEN @dtFromDate AND @dtToDate),0) ;

   v_CURRENTPL := v_CURRENTPL*(-1);
--SELECT @PLCHARTID,@CURRENTPL,@PLOPENING

-----------------------------------------------------------------

   DROP TABLE IF EXISTS tt_PLSHOW CASCADE;
   CREATE TEMPORARY TABLE tt_PLSHOW 
   (
      numAccountId NUMERIC(9,0),
      vcAccountName VARCHAR(250),
      numParntAcntTypeID NUMERIC(9,0),
      vcAccountDescription VARCHAR(250),
      vcAccountCode VARCHAR(50),
      Opening DECIMAL(20,5),
      Debit DECIMAL(20,5),
      Credit DECIMAL(20,5),
      Balance VARCHAR(50),
      AccountCode1  VARCHAR(100),
      vcAccountName1 VARCHAR(250),
      Type INTEGER
   );

   INSERT INTO tt_PLSHOW
   SELECT *,CAST(CAST((/*Opening +*/ Credit -Debit) AS VARCHAR(50)) AS VARCHAR(250)) as Balance,
CAST(CASE
   WHEN LENGTH(P.vcAccountCode) > 4 THEN REPEAT('&nbsp;',LENGTH(P.vcAccountCode) -4) || P.vcAccountCode
   ELSE P.vcAccountCode
   END AS NUMERIC(9,0)) AS AccountCode1,
 CASE
   WHEN LENGTH(P.vcAccountCode) > 4 THEN REPEAT('&nbsp;',LENGTH(P.vcAccountCode) -4) || P.vcAccountName
   ELSE P.vcAccountName
   END AS vcAccountName1, CAST(1 AS VARCHAR(50)) AS Type
   FROM tt_PLSUMMARY P
   UNION
   select  CAST(0 AS NUMERIC(8,0)),CAST(CAST('' AS VARCHAR(50)) AS VARCHAR(250)),CAST(CAST(0 AS VARCHAR(50)) AS NUMERIC(9,0)),CAST('' AS VARCHAR(250)),CAST(CAST('0103A' AS INTEGER) AS VARCHAR(50)),0,0,0, CAST('________________' AS VARCHAR(50)) ,CAST('' AS VARCHAR(100)),CAST('' AS VARCHAR(250)),2
   UNION
   Select CAST(0 AS NUMERIC(8,0)),CAST(CAST('Total Income :' AS VARCHAR(50)) AS VARCHAR(250)),CAST(CAST(0 AS VARCHAR(50)) AS NUMERIC(9,0)),CAST('' AS VARCHAR(250)),CAST(CAST('0103A1' AS INTEGER) AS VARCHAR(50)),0,0,0,CAST(v_TotalIncome AS VARCHAR(50)) ,CAST('' AS VARCHAR(100)),CAST('Total Income :' AS VARCHAR(250)),2
   UNION
   select  CAST(0 AS NUMERIC(8,0)),CAST(CAST('' AS VARCHAR(50)) AS VARCHAR(250)),CAST(CAST(0 AS VARCHAR(50)) AS NUMERIC(9,0)),CAST('' AS VARCHAR(250)),CAST(CAST('0103A2' AS INTEGER) AS VARCHAR(50)),0,0,0, CAST('' AS VARCHAR(50)) ,CAST('' AS VARCHAR(100)),CAST('' AS VARCHAR(250)),2
   UNION
   SELECT *,CAST(CAST((Opening*case SUBSTR(O.vcAccountCode,1,4) when '0103' then(-1) else 1 end)+(Debit*case SUBSTR(O.vcAccountCode,1,4) when '0103' then(-1) else 1 end) -(Credit*case SUBSTR(O.vcAccountCode,1,4) when '0103' then(-1) else 1 end)  AS VARCHAR(50)) AS VARCHAR(250)) as Balance,
CAST(CASE
   WHEN LENGTH(O.vcAccountCode) > 4 THEN REPEAT('&nbsp;',LENGTH(O.vcAccountCode) -4) || O.vcAccountCode
   ELSE O.vcAccountCode
   END AS NUMERIC(9,0)) AS AccountCode1,
 CASE
   WHEN LENGTH(O.vcAccountCode) > 4 THEN REPEAT('&nbsp;',LENGTH(O.vcAccountCode) -4) || O.vcAccountName
   ELSE O.vcAccountName
   END AS vcAccountName1, CAST(2 AS VARCHAR(50)) as Type
   FROM tt_PLOUTPUT O
   UNION
   select  CAST(0 AS NUMERIC(8,0)),CAST(CAST('' AS VARCHAR(50)) AS VARCHAR(250)),CAST(CAST(0 AS VARCHAR(50)) AS NUMERIC(9,0)),CAST('' AS VARCHAR(250)),CAST(CAST('0104A' AS INTEGER) AS VARCHAR(50)),0,0,0, CAST('________________' AS VARCHAR(50)) ,CAST('' AS VARCHAR(100)),CAST('' AS VARCHAR(250)),2
   UNION
   Select CAST(0 AS NUMERIC(8,0)),CAST(CAST('Total Expense :' AS VARCHAR(50)) AS VARCHAR(250)),CAST(CAST(0 AS VARCHAR(50)) AS NUMERIC(9,0)),CAST('' AS VARCHAR(250)),CAST(CAST('0104A1' AS INTEGER) AS VARCHAR(50)),0,0,0,CAST(v_TotalExpense AS VARCHAR(50)) ,CAST('' AS VARCHAR(100)),CAST('Total Expense :' AS VARCHAR(250)),2
   UNION
   select  CAST(0 AS NUMERIC(8,0)),CAST(CAST('' AS VARCHAR(50)) AS VARCHAR(250)),CAST(CAST(0 AS VARCHAR(50)) AS NUMERIC(9,0)),CAST('' AS VARCHAR(250)),CAST(CAST('0104A2' AS INTEGER) AS VARCHAR(50)),0,0,0, CAST('' AS VARCHAR(50)) ,CAST('' AS VARCHAR(100)),CAST('' AS VARCHAR(250)),2 
 -----------Added by chintan for COGS change
   UNION
   select  CAST(0 AS NUMERIC(8,0)),CAST(CAST('' AS VARCHAR(50)) AS VARCHAR(250)),CAST(CAST(0 AS VARCHAR(50)) AS NUMERIC(9,0)),CAST('' AS VARCHAR(250)),CAST(CAST('0106A' AS INTEGER) AS VARCHAR(50)),0,0,0, CAST('________________' AS VARCHAR(50)) ,CAST('' AS VARCHAR(100)),CAST('' AS VARCHAR(250)),2
   UNION
   Select CAST(0 AS NUMERIC(8,0)),CAST(CAST('Total COGS :' AS VARCHAR(50)) AS VARCHAR(250)),CAST(CAST(0 AS VARCHAR(50)) AS NUMERIC(9,0)),CAST('' AS VARCHAR(250)),CAST(CAST('0106A1' AS INTEGER) AS VARCHAR(50)),0,0,0,CAST(v_TotalCOGS AS VARCHAR(50)) ,CAST('' AS VARCHAR(100)),CAST('Total COGS :' AS VARCHAR(250)),2
   UNION
   select  CAST(0 AS NUMERIC(8,0)),CAST(CAST('' AS VARCHAR(50)) AS VARCHAR(250)),CAST(CAST(0 AS VARCHAR(50)) AS NUMERIC(9,0)),CAST('' AS VARCHAR(250)),CAST(CAST('0106A2' AS INTEGER) AS VARCHAR(50)),0,0,0, CAST('' AS VARCHAR(50)) ,CAST('' AS VARCHAR(100)),CAST('' AS VARCHAR(250)),2 
 -----------End of block, Added by chintan for COGS change

--COA.numAccountId,vcAccountName,numParntAcntTypeID,vcAccountDescription,vcAccountCode
   UNION
   Select v_PLCHARTID,CAST(CAST('Profit / (Loss) Current :' AS VARCHAR(50)) AS VARCHAR(250)),CAST(CAST(0 AS VARCHAR(50)) AS NUMERIC(9,0)),CAST('' AS VARCHAR(250)),CAST(CAST('051' AS INTEGER) AS VARCHAR(50)),0,0,0, CASE WHEN v_CURRENTPL < 0 THEN '(' || CAST(v_CURRENTPL  AS VARCHAR(50)) || ')' ELSE CAST(v_CURRENTPL AS VARCHAR(50)) END  ,CAST('' AS VARCHAR(100)),CAST('Profit / (Loss) Current :' AS VARCHAR(250)),3
   UNION
   select v_PLCHARTID,CAST(CAST('Profit / (Loss) Opening :' AS VARCHAR(50)) AS VARCHAR(250)),CAST(CAST(0 AS VARCHAR(50)) AS NUMERIC(9,0)),CAST('' AS VARCHAR(250)),CAST(CAST('052' AS INTEGER) AS VARCHAR(50)),0,0,0,CASE WHEN v_PLOPENING < 0 THEN '(' || CAST(v_PLOPENING AS VARCHAR(50)) || ')' ELSE CAST(v_PLOPENING AS VARCHAR(50)) END  ,CAST('' AS VARCHAR(100)),CAST('Profit / (Loss) Opening :' AS VARCHAR(250)),4
   UNION
   Select  v_PLCHARTID, CAST(CAST('Net Profit/(Loss) :' AS VARCHAR(50)) AS VARCHAR(250)),CAST(CAST(0 AS VARCHAR(50)) AS NUMERIC(9,0)),CAST('' AS VARCHAR(250)),CAST(CAST('053' AS INTEGER) AS VARCHAR(50)),0,0,0, CASE WHEN(v_PLOPENING+v_CURRENTPL) < 0 THEN '(' || CAST(v_PLOPENING+v_CURRENTPL AS VARCHAR(50)) || ')' ELSE CAST(v_PLOPENING+v_CURRENTPL AS VARCHAR(50)) END  ,CAST('' AS VARCHAR(100)),CAST('Net Profit/(Loss) :' AS VARCHAR(250)),5;



open SWV_RefCur for select  A.numAccountId ,
        A.vcAccountName ,
        A.numParntAcntTypeID ,
        A.vcAccountDescription ,
        A.vcAccountCode ,
        A.Opening ,
        A.Debit ,
        A.Credit ,
        A.Balance ,
        A.AccountCode1 ,
        CASE WHEN LENGTH(A.vcAccountCode) > 4
   THEN REPEAT('&nbsp;',LENGTH(A.vcAccountCode) -4) || A.vcAccountName
   ELSE A.vcAccountName
   END AS vcAccountName1, A.vcAccountCode ,
        A.TYPE
   from tt_PLSHOW A ORDER BY A.vcAccountCode ASC;

   DROP TABLE IF EXISTS tt_PLOUTPUT CASCADE;
   DROP TABLE IF EXISTS tt_PLSUMMARY CASCADE;
   DROP TABLE IF EXISTS tt_PLSHOW CASCADE;
   RETURN;
END; $$;
-- exec USP_GetChartAcntDetailsForProfitLoss @numDomainId=171,@dtFromDate='2013-02-14 00:00:00:000',@dtToDate='2013-02-28 23:59:59:000',@ClientTimeZoneOffset=-330












