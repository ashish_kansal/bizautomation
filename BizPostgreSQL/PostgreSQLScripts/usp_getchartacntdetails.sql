-- Stored procedure definition script USP_GetChartAcntDetails for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_GetChartAcntDetails(v_numDomainId NUMERIC(9,0),
    v_dtFromDate TIMESTAMP,
    v_dtToDate TIMESTAMP,
    v_ClientTimeZoneOffset INTEGER, --Added by Chintan to enable calculation of date according to client machine
    v_numAccountClass NUMERIC(9,0) DEFAULT 0, INOUT SWV_RefCur refcursor default null, INOUT SWV_RefCur2 refcursor default null)
LANGUAGE plpgsql    
--        DECLARE @numFinYear INT ;
   AS $$
   DECLARE
   v_dtFinYearFromJournal  TIMESTAMP;
   v_dtFinYearFrom  TIMESTAMP;
   v_CURRENTPL  DECIMAL(20,5);
   v_PLOPENING  DECIMAL(20,5);
   v_PLCHARTID  NUMERIC(8,0);
   v_TotalIncome  DECIMAL(20,5);
   v_TotalExpense  DECIMAL(20,5);
   v_TotalCOGS  DECIMAL(20,5);
   v_CurrentPL_COA  DECIMAL(20,5);

--        SET @numFinYear = ( SELECT  numFinYearId
--                            FROM    FINANCIALYEAR
--                            WHERE   dtPeriodFrom <= @dtFromDate
--                                    AND dtPeriodTo >= @dtFromDate
--                                    AND numDomainId = @numDomainId
--                          ) ;
BEGIN
   select   MIN(datEntry_Date) INTO v_dtFinYearFromJournal FROM VIEW_JOURNAL WHERE numDomainID = v_numDomainId AND (numAccountClass = v_numAccountClass OR v_numAccountClass = 0);
   SELECT   dtPeriodFrom INTO v_dtFinYearFrom FROM     FinancialYear WHERE    dtPeriodFrom <= v_dtFromDate
   AND dtPeriodTo >= v_dtFromDate
   AND numDomainId = v_numDomainId;

   DROP TABLE IF EXISTS tt_VIEW_JOURNAL CASCADE;
   CREATE TEMPORARY TABLE tt_VIEW_JOURNAL AS
      SELECT * FROM VIEW_JOURNAL WHERE numDomainID = v_numDomainId AND (numAccountClass = v_numAccountClass OR v_numAccountClass = 0);

   v_dtToDate := v_dtToDate+CAST(v_ClientTimeZoneOffset || 'minute' as interval);
/*Timezone Logic.. Stored Date are in UTC/GMT  */
--SET @dtToDate = DATEADD(DAY, DATEDIFF(DAY, '19000101',  @dtToDate), '23:59:59');
--SELECT @dtFromDate,@ClientTimeZoneOffset,DATEADD(minute, @ClientTimeZoneOffset, @dtFromDate)
--        SET @dtFromDate = DATEADD(minute, @ClientTimeZoneOffset, @dtFromDate) ;

        /*Comment by chintan
--        Reason: Standard US was facing balance not matching for undeposited func(check video for more info)
--        We do not need timezone settings since all journal entry date does not store time. we only store date. 
--        */
--        SET @dtToDate = DATEADD(minute, @ClientTimeZoneOffset, @dtToDate) ;


--PRINT @dtFromDate
--PRINT @dtToDate
--PRINT @numFinYear 
--PRINT @dtFinYearFrom 

--select * from view_journal where numDomainid=72

		
   DROP TABLE IF EXISTS tt_PLSUMMARY CASCADE;
   CREATE TEMPORARY TABLE tt_PLSUMMARY
   (
      numAccountId NUMERIC(9,0),
      vcAccountName VARCHAR(250),
      numParntAcntTypeID NUMERIC(9,0),
      vcAccountDescription VARCHAR(250),
      vcAccountCode VARCHAR(50),
      Opening DECIMAL(20,5),
      Debit DECIMAL(20,5),
      Credit DECIMAL(20,5),
      bitIsSubAccount BOOLEAN,
      numParentAccID NUMERIC(9,0),
      Balance DECIMAL(20,5)
   );

   INSERT  INTO tt_PLSUMMARY
   SELECT  COA.numAccountId,
                        vcAccountName,
                        numParntAcntTypeID,
                        vcAccountDescription,
                        vcAccountCode,
                      /*  ISNULL(( SELECT SUM(ISNULL(monOpening, 0))
                                 FROM   CHARTACCOUNTOPENING CAO
                                 WHERE  numFinYearId = @numFinYear
                                        AND numDomainID = @numDomainId
                                        AND CAO.numAccountId IN (SELECT numAccountId FROM dbo.Chart_Of_Accounts WHERE numDomainId = @numDomainId AND vcAccountCode like COA.vcAccountCode + '--' )
                               ), 0)
                        + */ 
						CASE WHEN COA.vcAccountCode ilike '0103%' OR
   COA.vcAccountCode ilike '0104%' OR
   COA.vcAccountCode ilike '0106%'
   THEN 0
   ELSE coalesce(t1.OPENING,0)
   END as OPENING,
                        coalesce(t.DEBIT,0) AS DEBIT,
                        coalesce(t.CREDIT,0) AS CREDIT
                               ,coalesce(COA.bitIsSubAccount,false),coalesce(COA.numParentAccId,0) AS numParentAccID,0
   FROM    Chart_Of_Accounts COA
   LEFT JOIN LATERAL(SELECT   SUM(Debit -Credit) AS OPENING
      FROM     tt_VIEW_JOURNAL VJ
      WHERE    VJ.numDomainId = v_numDomainId
      AND VJ.numaccountid = COA.numAccountID /*AND VJ.COAvcAccountCode like COA.vcAccountCode + '--'*/ /*VJ.numAccountId = COA.numAccountId*/
      AND datentry_date BETWEEN(CASE WHEN COA.vcAccountCode ilike '0103%' OR COA.vcAccountCode ilike '0104%' OR COA.vcAccountCode ilike '0106%' THEN v_dtFinYearFrom ELSE v_dtFinYearFromJournal END)
      AND  v_dtFromDate+INTERVAL '-1 minute' AND (VJ.numAccountClass = v_numAccountClass OR v_numAccountClass = 0)) AS t1
   LEFT JOIN LATERAL(SELECT   SUM(Debit) as DEBIT,SUM(Credit) as CREDIT
      FROM   tt_VIEW_JOURNAL VJ
      WHERE  VJ.numDomainId = v_numDomainId
      AND VJ.numaccountid = COA.numAccountID /*AND VJ.COAvcAccountCode like COA.vcAccountCode + '--'*/ /*VJ.numAccountId = COA.numAccountId*/
      AND datentry_date BETWEEN v_dtFromDate AND v_dtToDate AND (VJ.numAccountClass = v_numAccountClass OR v_numAccountClass = 0)) as t on TRUE on TRUE
   WHERE   COA.numDomainId = v_numDomainId;
--SELECT @dtFinYearFrom,@dtFromDate - 1,* FROM #PLSummary
   DROP TABLE IF EXISTS tt_PLOUTPUT CASCADE;
   CREATE TEMPORARY TABLE tt_PLOUTPUT
   (
      numAccountId NUMERIC(9,0),
      vcAccountName VARCHAR(250),
      numParntAcntTypeID NUMERIC(9,0),
      vcAccountDescription VARCHAR(250),
      vcAccountCode VARCHAR(50),
      Opening DECIMAL(20,5),
      Debit DECIMAL(20,5),
      Credit DECIMAL(20,5),
      numParentAccID NUMERIC(9,0),
      Balance DECIMAL(20,5)
   );

   INSERT  INTO tt_PLOUTPUT
   SELECT  ATD.numAccountTypeID,
                        CAST(ATD.vcAccountType AS VARCHAR(250)),
                        ATD.numParentID,
                        CAST('' AS VARCHAR(250)),
                        ATD.vcAccountCode,
                        coalesce(SUM(Opening),0) AS Opening,
                        coalesce(SUM(Debit),0) AS Debit,
                        coalesce(SUM(Credit),0) AS Credit,0,0
   FROM    /*Chart_Of_Accounts c JOIN [AccountTypeDetail] ATD ON ATD.[numAccountTypeID] = c.[numParntAcntTypeId]*/
   AccountTypeDetail ATD
   RIGHT OUTER JOIN tt_PLSUMMARY PL ON PL.vcAccountCode ilike ATD.vcAccountCode || '%' AND LENGTH(PL.vcAccountCode) > LENGTH(ATD.vcAccountCode)
                        --(PL.vcAccountCode LIKE c.vcAccountCode OR (PL.vcAccountCode LIKE c.vcAccountCode + '--' AND PL.numParentAccID>0))
   AND ATD.numDomainID = v_numDomainId
				--WHERE  PL.bitIsSubAccount=0
   GROUP BY ATD.numAccountTypeID,ATD.vcAccountCode,ATD.vcAccountType,ATD.numParentID;

   ALTER TABLE tt_PLSUMMARY
   DROP COLUMN bitIsSubAccount;


-- GETTING P&L VALUE

   v_CURRENTPL := 0;	
   v_PLOPENING := 0;


   select   COA.numAccountId INTO v_PLCHARTID FROM Chart_Of_Accounts COA WHERE numDomainId = v_numDomainId and
   bitProfitLoss = true;

   select   coalesce(sum(Credit),0) -coalesce(sum(Debit),0) INTO v_CurrentPL_COA FROM tt_VIEW_JOURNAL VJ WHERE numDomainId = v_numDomainId
   AND numAccountId = v_PLCHARTID AND datentry_date between  v_dtFromDate AND v_dtToDate;

   select   coalesce(sum(Opening),0)+coalesce(sum(Credit),0) -coalesce(sum(Debit),0) INTO v_TotalIncome FROM
   tt_PLOUTPUT P WHERE
   vcAccountCode IN('0103');

   select   coalesce(sum(Opening),0)+coalesce(sum(Credit),0) -coalesce(sum(Debit),0) INTO v_TotalExpense FROM
   tt_PLOUTPUT P WHERE
   vcAccountCode IN('0104');

   select   coalesce(sum(Opening),0)+coalesce(sum(Credit),0) -coalesce(sum(Debit),0) INTO v_TotalCOGS FROM
   tt_PLOUTPUT P WHERE
   vcAccountCode IN('0106');

   RAISE NOTICE 'TotalIncome=									%',SUBSTR(CAST(v_TotalIncome AS VARCHAR(20)),1,20);
   RAISE NOTICE 'TotalExpense=									%',SUBSTR(CAST(v_TotalExpense AS VARCHAR(20)),1,20);
   RAISE NOTICE 'TotalCOGS=										%',SUBSTR(CAST(v_TotalCOGS AS VARCHAR(20)),1,20);

 /*@CurrentPL_COA +*/ 
   v_CURRENTPL := v_TotalIncome+v_TotalExpense+v_TotalCOGS; 
   RAISE NOTICE 'Current Profit/Loss = (Income - expense - cogs)= %',SUBSTR(CAST(v_CURRENTPL AS VARCHAR(20)),1,20);

   v_CURRENTPL := v_CURRENTPL*(-1);

   select   coalesce(sum(Credit),0) -coalesce(sum(Debit),0) INTO v_PLOPENING FROM tt_VIEW_JOURNAL VJ WHERE numDomainId = v_numDomainId
   AND (vcAccountCode  ilike '0103%' or  vcAccountCode  ilike '0104%' or  vcAccountCode  ilike '0106%')
   AND datentry_date <=  v_dtFromDate+INTERVAL '-1 minute';

   select   coalesce(v_PLOPENING,0)+coalesce(sum(Credit),0) -coalesce(sum(Debit),0) INTO v_PLOPENING FROM tt_VIEW_JOURNAL VJ WHERE numDomainId = v_numDomainId
   AND numAccountId = v_PLCHARTID AND datentry_date <=  v_dtFromDate+INTERVAL '-1 minute';

   RAISE NOTICE '@PLOPENING=%',SUBSTR(CAST(v_PLOPENING AS VARCHAR(20)),1,20);
   RAISE NOTICE '@CURRENTPL=%',SUBSTR(CAST(v_CURRENTPL AS VARCHAR(20)),1,20);
   RAISE NOTICE '-(@PLOPENING + @CURRENTPL)=%',CAST((-(v_PLOPENING+v_CURRENTPL)) AS VARCHAR(20));


   v_CURRENTPL := v_CURRENTPL*(-1);

   UPDATE tt_PLSUMMARY SET Opening = CASE WHEN vcAccountCode = '0105010101' THEN -v_PLOPENING ELSE Opening END,Balance = CASE WHEN vcAccountCode = '0105010101' THEN -(v_PLOPENING+v_CURRENTPL) ELSE Opening+Debit -Credit END;
--WHERE vcAccountCode = '0105010101'
                        

	UPDATE 
		tt_PLOUTPUT tt_PLOUTPUT 
	SET 
		Opening = T.Opening
		,Debit = T.Debit
		,Credit = T.Credit
		,Balance = T.Balance
   FROM
   (
	SELECT 
		PO.vcAccountCode
		,coalesce(SUM(PL.Opening),0) AS Opening
		,coalesce(SUM(PL.Debit),0) AS Debit
		,coalesce(SUM(PL.Credit),0) AS Credit
		,coalesce(SUM(PL.Balance),0) AS Balance 
	FROM 
		tt_PLOUTPUT PO
	RIGHT OUTER JOIN 
		tt_PLSUMMARY PL 
	ON 
		PL.vcAccountCode ilike PO.vcAccountCode || '%' 
		AND LENGTH(PL.vcAccountCode) > LENGTH(PO.vcAccountCode)
   GROUP BY PO.vcAccountCode
	)T WHERE T.vcAccountCode = tt_PLOUTPUT.vcAccountCode;

--SELECT PO.vcAccountCode,ISNULL(SUM(PL.Opening), 0) AS Opening,ISNULL(SUM(PL.Debit), 0) AS Debit,ISNULL(SUM(PL.Credit), 0) AS Credit,
--ISNULL(SUM(PL.Balance), 0) AS Balance FROM #PLOutPut PO
--RIGHT OUTER JOIN #PLSummary PL ON PL.vcAccountCode LIKE PO.vcAccountCode + '--' AND LEN(PL.vcAccountCode)>LEN(PO.vcAccountCode)
--GROUP BY PO.vcAccountCoder

--WHERE vcAccountCode = '0105010101'
--------------------------------------------------------
		--UPDATE #PLSummary SET Opening = @PLOPENING WHERE #PLSummary.vcAccountCode = '0105010101'

   open SWV_RefCur for
   SELECT  P.numAccountId,
				P.vcAccountName,
				P.numParntAcntTypeID,
				P.vcAccountDescription,
				P.vcAccountCode,
				/*(CASE WHEN P.[vcAccountCode] = '0105010101' THEN -@PLOPENING ELSE P.Opening END) AS*/ P.Opening,
				P.Debit ,
				P.Credit,
				P.numParentAccID,
                /*(CASE WHEN P.[vcAccountCode] = '0105010101' THEN -(@PLOPENING + @CURRENTPL) ELSE Opening + Debit - Credit END) AS*/ P.Balance,
                CASE WHEN LENGTH(P.vcAccountCode) > 4 THEN REPEAT('&nbsp;',LENGTH(P.vcAccountCode) -4) || P.vcAccountCode
   ELSE P.vcAccountCode
   END AS AccountCode1,
                CASE WHEN LENGTH(P.vcAccountCode) > 4 THEN REPEAT('&nbsp;',LENGTH(P.vcAccountCode) -4) || P.vcAccountName
   ELSE P.vcAccountName
   END AS vcAccountName1,
                1 AS TYPE
   FROM    tt_PLSUMMARY P
   LEFT JOIN Chart_Of_Accounts C ON C.numAccountId = P.numAccountId
   WHERE   LENGTH(coalesce(P.vcAccountCode,'')) > 2
   UNION
   SELECT  O.numAccountId,
				O.vcAccountName,
				O.numParntAcntTypeID,
				O.vcAccountDescription,
				O.vcAccountCode,
				/*(CASE WHEN O.[vcAccountCode] = '0105010101' THEN -@PLOPENING ELSE O.Opening END) AS*/ Opening,
				O.Debit ,
				O.Credit,
				O.numParentAccID,
                /*(CASE WHEN O.[vcAccountCode] = '0105010101' THEN -(@PLOPENING + @CURRENTPL) ELSE Opening + Debit - Credit END) AS*/ Balance,
                CASE WHEN LENGTH(O.vcAccountCode) > 4
   THEN REPEAT('&nbsp;',LENGTH(O.vcAccountCode) -4) || O.vcAccountCode
   ELSE O.vcAccountCode
   END AS AccountCode1,
                CASE WHEN LENGTH(O.vcAccountCode) > 4
   THEN REPEAT('&nbsp;',LENGTH(O.vcAccountCode) -4) || O.vcAccountName
   ELSE O.vcAccountName
   END AS vcAccountName1,
                2 AS TYPE
                --CAST(O.[vcAccountCode] AS deci)  vcSortableAccountCode
   FROM    tt_PLOUTPUT O
--                LEFT JOIN dbo.Chart_Of_Accounts C ON C.numAccountId = O.numAccountId
   WHERE   LENGTH(coalesce(O.vcAccountCode,'')) > 2
   ORDER BY 5,TYPE; 
 
    --SELECT * FROM #PLOutPut ;
    --SELECT * FROM #PLSummary ;

   open SWV_RefCur2 for
   SELECT(SUM(Opening) -v_PLOPENING) AS OpeningTotal,
                SUM(Debit) AS DebitTotal,
                SUM(Credit) AS CreditTotal,
                (SUM(Opening)+SUM(Debit) -SUM(Credit) -v_PLOPENING) AS BalanceTotal
   FROM    tt_PLSUMMARY;

        
   RETURN;
END; $$;



