-- Stored procedure definition script USP_GETWebService for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GETWebService(v_vcToken VARCHAR(500),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT numWebServiceId as "numWebServiceId",
	vcToken as "vcToken",
	numDomainID as "numDomainID"
   FROM WebService
   WHERE
   vcToken = v_vcToken;
END; $$;















