-- Stored procedure definition script CreateCustomReportsForNewDomain for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION CreateCustomReportsForNewDomain(v_numdomainId NUMERIC,
v_numContactId NUMERIC)
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_minId  NUMERIC;
   v_maxId  NUMERIC;
   v_customrptid  NUMERIC(18,0);
   SWV_RowCount INTEGER;
BEGIN
   BEGIN
      CREATE TEMP SEQUENCE tt_tempTable_seq;
      EXCEPTION WHEN OTHERS THEN
         NULL;
   END;
   drop table IF EXISTS tt_TEMPTABLE CASCADE;
   Create TEMPORARY TABLE tt_TEMPTABLE 
   ( 
      ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1) PRIMARY KEY,
      numReportId NUMERIC(18,0)
   ); 
   insert into tt_TEMPTABLE(numReportId) select(numCustomReportId) from CustomReport where numdomainId = 1 and coalesce(bitDefault,false) = true;
--select count(*) from #tempTable
   select   min(numReportId), max(numReportId) INTO v_minId,v_maxId from tt_TEMPTABLE;


	

   while v_minId <> 0 LOOP
      insert into CustomReport(vcReportName,
vcReportDescription,
textQuery,
numCreatedBy,
numdomainId,
numModifiedBy,
numModuleId,
numGroupId,
bitFilterRelAnd,
varstdFieldId,
custFilter,
FromDate,
ToDate,
bintRows,
bitGridType,
varGrpfld,
varGrpOrd,
varGrpflt,
textQueryGrp,
FilterType,
AdvFilterStr,
OrderbyFld,
Orderf,
MttId,
MttValue,
CompFilter1,
CompFilter2,
CompFilterOpr,
bitDrillDown,
bitSortRecCount,bitReportDisable)
      select
      vcReportName,
vcReportDescription,
textQuery,
v_numContactId,
v_numdomainId,
v_numContactId,
numModuleId,
numGroupId,
bitFilterRelAnd,
varstdFieldId,
custFilter,
FromDate,
ToDate,
bintRows,
bitGridType,
varGrpfld,
varGrpOrd,
varGrpflt,
textQueryGrp,
FilterType,
AdvFilterStr,
OrderbyFld,
Orderf,
MttId,
MttValue,
CompFilter1,
CompFilter2,
CompFilterOpr,bitDrillDown,
bitSortRecCount,bitReportDisable from  CustomReport x where numdomainId = 1   and numCustomReportId = v_minId;
      v_customrptid := CURRVAL('customreport_seq');
      insert into CustReportFields(numCustomReportId,
vcFieldName,
vcValue)
      select v_customrptid,
vcFieldName,
vcValue from CustReportFields
      where numCustomReportId = v_minId;
      insert into CustReportFilterlist(numCustomReportId,
vcFieldsText,
vcFieldsValue,
vcFieldsOperator,
vcFilterValue,
vcFilterValued)
      select v_customrptid,
vcFieldsText,
vcFieldsValue,
vcFieldsOperator,
vcFilterValue,
vcFilterValued from  CustReportFilterlist
      where numCustomReportId = v_minId;
      insert into CustReportOrderlist(numCustomReportId,
vcFieldText,
vcValue,
numOrder)
      select v_customrptid,
vcFieldText,
vcValue,
numOrder from  CustReportOrderlist
      where numCustomReportId = v_minId;
      insert into CustReportSummationlist(numCustomReportId,
vcFieldName,
bitSum,
bitAvg,
bitMin,
bitMax)
      select v_customrptid,
vcFieldName,
bitSum,
bitAvg,
bitMin,
bitMax from CustReportSummationlist
      where numCustomReportId = v_minId;
      select   min(numReportId) INTO v_minId from tt_TEMPTABLE where numReportId > v_minId group by numReportId  order by numReportId LIMIT 1;
      GET DIAGNOSTICS SWV_RowCount = ROW_COUNT;
      if SWV_RowCount = 0 then 
         v_minId := 0;
      end if;
   END LOOP; 

   drop table IF EXISTS tt_TEMPTABLE CASCADE;
   RETURN;
END; $$;


