CREATE OR REPLACE FUNCTION USP_ValidateBizDocFulfillment(v_numDomainID NUMERIC(9,0),
      v_numOppID NUMERIC(9,0),
      v_numFromOppBizDocsId NUMERIC(9,0),
      v_numBizDocId NUMERIC(9,0))
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numPackingSlipBizDocId  NUMERIC(9,0);
   v_numAuthInvoice  NUMERIC(9,0);
   v_numFulfillmentOrderBizDocId  NUMERIC(9,0);
BEGIN
   select   numListItemID INTO v_numPackingSlipBizDocId FROM Listdetails WHERE vcData = 'Packing Slip' AND constFlag = true AND numListID = 27;
	    
   select   coalesce(numAuthoritativeSales,0) INTO v_numAuthInvoice From AuthoritativeBizDocs Where numDomainId = v_numDomainID; 
	    
   v_numFulfillmentOrderBizDocId := 296;
	    
   IF v_numFromOppBizDocsId > 0 then
	    
      RAISE NOTICE '1';
   ELSE
      IF v_numFulfillmentOrderBizDocId = v_numBizDocId then
			
				--Only a Sales Order can create a Fulfillment Order (Don’t allow an FO to be created from an Opportunity)
         IF EXISTS(SELECT numOppId FROM OpportunityMaster OM WHERE OM.numDomainId = v_numDomainID AND OM.numOppId = v_numOppID AND OM.tintoppstatus = 0) then
				
            RAISE EXCEPTION 'NOT_ALLOWED_FulfillmentOrder';
            RETURN;
         end if;
      end if;
   end if;
END; $$;


