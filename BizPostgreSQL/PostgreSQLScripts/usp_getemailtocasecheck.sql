-- Stored procedure definition script USP_GetEmailToCaseCheck for PostgreSQL
Create or replace FUNCTION USP_GetEmailToCaseCheck(v_numDomainID NUMERIC(9,0),            
v_vcFromEmail VARCHAR(50),
v_vcCaseNumber VARCHAR(50),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numcontactid  NUMERIC(9,0);
   v_numDivisionId  NUMERIC(9,0);
   v_numCaseId  NUMERIC(9,0);
BEGIN
   v_numcontactid := 0;
   v_numDivisionId := 0;
   v_numCaseId := 0;
   
   select   ADC.numContactId, ADC.numDivisionId INTO v_numcontactid,v_numDivisionId from AdditionalContactsInformation ADC
   JOIN DivisionMaster DM ON ADC.numDivisionId = DM.numDivisionID WHERE coalesce(DM.bitEmailToCase,false) = true AND
   DM.numDomainID = v_numDomainID AND ADC.vcEmail = v_vcFromEmail
   AND ADC.numDomainID = v_numDomainID    LIMIT 1;
		
   IF LENGTH(v_vcCaseNumber) > 0 AND v_vcCaseNumber != '0'
   AND coalesce(v_numcontactid,0) <> 0 AND coalesce(v_numDivisionId,0) <> 0 then
	
      select   numCaseId INTO v_numCaseId FROM Cases WHERE numDomainID = v_numDomainID
      AND numDivisionID = v_numDivisionId AND numContactId = v_numcontactid
      AND vcCaseNumber ilike '%' || coalesce(v_vcCaseNumber,'')    LIMIT 1;
   end if;
	
   open SWV_RefCur for SELECT cast(coalesce(v_numcontactid,0) as NUMERIC(9,0)) AS numcontactid,cast(coalesce(v_numDivisionId,0) as NUMERIC(9,0)) AS numDivisionId,cast(coalesce(v_numCaseId,0) as NUMERIC(9,0)) AS numCaseId;
END; $$;
          












