-- Stored procedure definition script USP_GetAuthoritativeOpportunityCount for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetAuthoritativeOpportunityCount(v_numDomainId NUMERIC(9,0) DEFAULT 0,      
	v_numOppId NUMERIC(9,0) DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_tinyOppType  SMALLINT;
   v_numAuthoritativeBizDocId  NUMERIC(9,0);
   v_numBizDocsId  NUMERIC(9,0);
BEGIN
   select   coalesce(tintopptype,0) INTO v_tinyOppType FROM OpportunityMaster WHERE numOppId = v_numOppId;       
  
   IF v_tinyOppType = 1 then
      select   coalesce(numAuthoritativeSales,0) INTO v_numAuthoritativeBizDocId FROM AuthoritativeBizDocs WHERE numDomainId = v_numDomainId;
   ELSEIF v_tinyOppType = 2
   then
      select   coalesce(numAuthoritativePurchase,0) INTO v_numAuthoritativeBizDocId FROM AuthoritativeBizDocs WHERE numDomainId = v_numDomainId;
   end if;      
 
   open SWV_RefCur for SELECT
   COUNT(*)
   FROM
   OpportunityBizDocs
   WHERE
   numoppid = v_numOppId
   AND ((numBizDocId = v_numAuthoritativeBizDocId AND coalesce(bitAuthoritativeBizDocs,0) = 1) OR numBizDocId = 304); -- 304 - IS DEFERRED INCOME BIZDOC 
END; $$;












