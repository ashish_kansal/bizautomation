-- Stored procedure definition script USP_GetImagePath for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_GetImagePath(v_numDomainId NUMERIC(9,0) DEFAULT 0,  
 INOUT v_vcBizDocImagePath VARCHAR(100) DEFAULT '' ,
 INOUT v_vcCompanyImagePath VARCHAR(100) DEFAULT '' ,
 INOUT v_vcBankImagePath VARCHAR(100) DEFAULT '')
LANGUAGE plpgsql
   AS $$
BEGIN
   select   coalesce(vcBizDocImagePath,''), coalesce(vcCompanyImagePath,''), coalesce(vcBankImagePath,'') INTO v_vcBizDocImagePath,v_vcCompanyImagePath,v_vcBankImagePath From Domain Where numDomainId = v_numDomainId;
   RETURN;
END; $$;


