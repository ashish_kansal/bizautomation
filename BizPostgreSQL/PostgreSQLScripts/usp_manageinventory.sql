DROP FUNCTION IF EXISTS usp_ManageInventory;

CREATE OR REPLACE FUNCTION usp_ManageInventory(v_itemcode NUMERIC(9,0) DEFAULT 0,
    v_numWareHouseItemID NUMERIC(9,0) DEFAULT 0,
    v_monAmount DECIMAL(20,5) DEFAULT NULL,
    v_tintOpptype SMALLINT DEFAULT NULL,
    v_numUnits DOUBLE PRECISION DEFAULT NULL,
    v_QtyShipped DOUBLE PRECISION DEFAULT NULL,
    v_QtyReceived DOUBLE PRECISION DEFAULT NULL,
    v_monPrice DECIMAL(20,5) DEFAULT NULL,
    v_tintFlag SMALLINT DEFAULT NULL,  --1: SO/PO insert/edit 2:SO/PO Close 3:SO/PO Re-Open 4:Sales Fulfillment Release 5:Sales Fulfillment Revert
    v_numOppID NUMERIC(9,0) DEFAULT 0,
    v_numoppitemtCode NUMERIC(9,0) DEFAULT 0,
    v_numUserCntID NUMERIC(9,0) DEFAULT 0,
	v_bitWorkOrder BOOLEAN DEFAULT false)
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_onHand  DOUBLE PRECISION;                                    
   v_onOrder  DOUBLE PRECISION; 
   v_onBackOrder  DOUBLE PRECISION;
   v_onAllocation  DOUBLE PRECISION;
   v_onReOrder  DOUBLE PRECISION;
   v_monAvgCost  DECIMAL(20,5); 
   v_bitKitParent  BOOLEAN;
   v_bitAssembly  BOOLEAN;
   v_numBuildProcessId  NUMERIC(18,0);
   v_numOrigUnits  NUMERIC;		
   v_description  VARCHAR(100);
   v_bitAsset  BOOLEAN;
   v_numDomainID  NUMERIC; 
   v_dtReleaseDate  TIMESTAMP;

   v_dtItemReceivedDate  TIMESTAMP;
   
   v_TotalOnHand  NUMERIC;
   v_tintRefType  SMALLINT;
   v_numDomain  NUMERIC(18,0);
   v_numDeletedReceievedQty  NUMERIC(18,0);
   v_i  INTEGER DEFAULT 1;
   v_COUNT  INTEGER;
   v_numOIRLID  NUMERIC(18,0);
   v_numTempWarehouseItemID  NUMERIC(18,0);
   v_numTempUnitReceieved  DOUBLE PRECISION;
   v_numTempDeletedReceievedQty  DOUBLE PRECISION;
BEGIN
   IF coalesce(v_numUserCntID,0) = 0 then
	
      RAISE EXCEPTION 'INVALID_USERID';
      RETURN;
   end if;

   select   coalesce(bitAsset,false), numDomainID, (CASE WHEN coalesce(bitVirtualInventory,false) = true THEN 0 ELSE coalesce(monAverageCost,0) END), (CASE
   WHEN coalesce(bitKitParent,false) = true AND coalesce(bitAssembly,false) = true THEN false
   WHEN coalesce(bitKitParent,false) = true THEN true
   ELSE false
   END), coalesce(bitAssembly,false), coalesce(numBusinessProcessId,0) INTO v_bitAsset,v_numDomainID,v_monAvgCost,v_bitKitParent,v_bitAssembly,v_numBuildProcessId FROM
   Item WHERE
   numItemCode = v_itemcode;                                   
    
   select   ItemReleaseDate INTO v_dtReleaseDate FROM
   OpportunityItems WHERE
   numOppId = v_numOppID
   AND numoppitemtCode = v_numoppitemtCode;

   IF v_numWareHouseItemID > 0 then
    
      select   coalesce(numOnHand,0), coalesce(numAllocation,0), coalesce(numonOrder,0), coalesce(numBackOrder,0), coalesce(numReorder,0) INTO v_onHand,v_onAllocation,v_onOrder,v_onBackOrder,v_onReOrder FROM    WareHouseItems WHERE   numWareHouseItemID = v_numWareHouseItemID;
   end if;
   
   v_dtItemReceivedDate := NULL;
   
   v_TotalOnHand := 0;  
   select(SUM(coalesce(numOnHand,0))+SUM(coalesce(numAllocation,0))) INTO v_TotalOnHand FROM WareHouseItems WHERE numItemID = v_itemcode;
					
   v_numOrigUnits := v_numUnits;
            
   IF v_tintFlag = 1 then  --1: SO/PO insert/edit 
    
      IF v_tintOpptype = 1 then
          
		--When change below code also change to USP_ManageWorkOrderStatus  

			/* When QtyShipped has been updated form sales fulfillment then it need to be deducted */
         v_description := CONCAT('SO insert/edit (Qty:',v_numUnits,' Shipped:',v_QtyShipped,')');
         IF v_bitKitParent = true then
            
            PERFORM USP_UpdatingInventoryForEmbeddedKits(v_numoppitemtCode,v_numOrigUnits,0::SMALLINT,v_numOppID,0::SMALLINT,v_numUserCntID);
         ELSE
            v_numUnits := v_numUnits -v_QtyShipped;
            IF v_onHand >= v_numUnits then
                
               v_onHand := v_onHand -v_numUnits;
               v_onAllocation := v_onAllocation+v_numUnits;
            ELSEIF v_onHand < v_numUnits
            then
                
               v_onAllocation := v_onAllocation+v_onHand;
               v_onBackOrder := v_onBackOrder+v_numUnits -v_onHand;
               v_onHand := 0;
            end if;
            IF v_bitAsset = false then--Not Asset
				
               UPDATE  WareHouseItems
               SET     numOnHand = v_onHand,numAllocation = v_onAllocation,numBackOrder = v_onBackOrder,
               dtModified = LOCALTIMESTAMP
               WHERE   numWareHouseItemID = v_numWareHouseItemID;
            end if;
         end if;
      ELSEIF v_tintOpptype = 2
      then
        
         v_description := CONCAT('PO insert/edit (Qty:',v_numUnits,' Received:',v_QtyReceived,')');
		            
			/* When QtyReceived has been updated form purchase fulfillment then it need to be deducted */
         v_numUnits := v_numUnits -v_QtyReceived;
         v_onOrder := v_onOrder+v_numUnits;
         UPDATE
         WareHouseItems
         SET
         numOnHand = v_onHand,numAllocation = v_onAllocation,numBackOrder = v_onBackOrder,
         numonOrder = v_onOrder,dtModified = LOCALTIMESTAMP
         WHERE
         numWareHouseItemID = v_numWareHouseItemID;
      end if;
   ELSEIF v_tintFlag = 2
   then --2:SO/PO Close
    
      RAISE NOTICE '@OppType=%',SUBSTR(CAST(v_tintOpptype AS VARCHAR(5)),1,5);
      IF v_tintOpptype = 1 then
        
         v_description := CONCAT('SO Close (Qty:',v_numUnits,' Shipped:',v_QtyShipped,')');
         IF v_bitKitParent = true then
            
            PERFORM USP_UpdatingInventoryForEmbeddedKits(v_numoppitemtCode,v_numOrigUnits,2::SMALLINT,v_numOppID,0::SMALLINT,v_numUserCntID);
         ELSE       
				/* When QtyShipped has been updated form sales fulfillment then it need to be deducted */
            v_numUnits := v_numUnits -v_QtyShipped;
            IF v_bitAsset = false then--Not Asset
				
               IF v_onAllocation >= v_numUnits then
					
                  v_onAllocation := v_onAllocation -v_numUnits;
               ELSE
                  RAISE EXCEPTION 'NOTSUFFICIENTQTY_ALLOCATION';
               end if;
            ELSE
               v_onAllocation := v_onAllocation;
            end if;
            UPDATE
            WareHouseItems
            SET
            numOnHand = v_onHand,numAllocation = v_onAllocation,numBackOrder = v_onBackOrder,
            dtModified = LOCALTIMESTAMP
            WHERE
            numWareHouseItemID = v_numWareHouseItemID;
         end if;
      ELSEIF v_tintOpptype = 2
      then
        
         v_description := CONCAT('PO Close (Qty:',v_numUnits,' Received:',v_QtyReceived,')');
                        
			/* When QtyReceived has been updated form purchase fulfillment then it need to be deducted */
         v_numUnits := v_numUnits -v_QtyReceived;
         If v_numUnits > 0 then
			
            IF EXISTS(SELECT 1 FROM OpportunityMaster WHERE numOppId = v_numOppID AND coalesce(bitStockTransfer,false) = false) then
				
					--Updating the Average Cost
               IF v_TotalOnHand+v_numUnits <= 0 then
					
                  v_monAvgCost := v_monAvgCost;
               ELSE
                  v_monAvgCost :=((v_TotalOnHand*v_monAvgCost)+(v_numUnits*v_monPrice))/(v_TotalOnHand+v_numUnits);
               end if;
               UPDATE
               Item
               SET
               monAverageCost =(CASE WHEN coalesce(bitVirtualInventory,false) = true THEN 0 ELSE v_monAvgCost END)
               WHERE
               numItemCode = v_itemcode;
            end if;
            IF v_onOrder >= v_numUnits then
				
               v_onOrder := v_onOrder -v_numUnits;
               IF v_onBackOrder >= v_numUnits then
					
                  v_onBackOrder := v_onBackOrder -v_numUnits;
                  v_onAllocation := v_onAllocation+v_numUnits;
               ELSE
                  v_onAllocation := v_onAllocation+v_onBackOrder;
                  v_numUnits := v_numUnits -v_onBackOrder;
                  v_onBackOrder := 0;
                  v_onHand := v_onHand+v_numUnits;
               end if;
            ELSEIF v_onOrder < v_numUnits
            then
				
               RAISE EXCEPTION 'INVALID ON ORDER';
               RETURN;
            end if;

			UPDATE OpportunityItems SET numUnitHourReceived = COALESCE(numUnitHourReceived,0) + v_numUnits WHERE numoppitemtcode=v_numoppitemtCode;

            UPDATE
            WareHouseItems
            SET
            numOnHand = v_onHand,numAllocation = v_onAllocation,numBackOrder = v_onBackOrder,
            numonOrder = v_onOrder,dtModified = LOCALTIMESTAMP
            WHERE
            numWareHouseItemID = v_numWareHouseItemID;
            select   dtItemReceivedDate INTO v_dtItemReceivedDate FROM OpportunityMaster WHERE numOppId = v_numOppID;
         end if;
      end if;
   ELSEIF v_tintFlag = 3
   then --3:SO/PO Re-Open
    
      RAISE NOTICE '@OppType=%',SUBSTR(CAST(v_tintOpptype AS VARCHAR(5)),1,5);
      IF v_tintOpptype = 1 then
        
         v_description := CONCAT('SO Re-Open (Qty:',v_numUnits,' Shipped:',v_QtyShipped,')');
         IF v_bitKitParent = true then
            
            PERFORM USP_UpdatingInventoryForEmbeddedKits(v_numoppitemtCode,v_numOrigUnits,3::SMALLINT,v_numOppID,0::SMALLINT,v_numUserCntID);
         ELSE
				/* When QtyShipped has been updated form sales fulfillment then it need to be deducted */
            v_numUnits := v_numUnits -v_QtyShipped;
            IF v_bitAsset = false then--Not Asset
				
               v_onAllocation := v_onAllocation+v_numUnits;
            ELSE
               v_onAllocation := v_onAllocation;
            end if;
            UPDATE
            WareHouseItems
            SET
            numOnHand = v_onHand,numAllocation = v_onAllocation,numBackOrder = v_onBackOrder,
            dtModified = LOCALTIMESTAMP
            WHERE
            numWareHouseItemID = v_numWareHouseItemID;
         end if;
      ELSEIF v_tintOpptype = 2
      then
         select   coalesce(numDeletedReceievedQty,0) INTO v_numDeletedReceievedQty FROM OpportunityItems WHERE numoppitemtCode = v_numoppitemtCode;
         UPDATE OpportunityItems SET numDeletedReceievedQty = 0 WHERE numoppitemtCode = v_numoppitemtCode;

			/* When QtyReceived has been updated form purchase fulfillment then it need to be deducted */
         v_numUnits := v_numUnits -v_QtyReceived;
            
			--Updating the Average Cost
         IF EXISTS(SELECT 1 FROM OpportunityMaster WHERE numOppId = v_numOppID AND coalesce(bitStockTransfer,false) = false) then
			
            IF v_TotalOnHand -v_numUnits <= 0 then
				
               v_monAvgCost := 0;
            ELSE
               v_monAvgCost :=((v_TotalOnHand*v_monAvgCost) -(v_numUnits*v_monPrice))/(v_TotalOnHand -v_numUnits);
            end if;
            UPDATE
            Item
            SET
            monAverageCost =(CASE WHEN coalesce(bitVirtualInventory,false) = true THEN 0 ELSE v_monAvgCost END)
            WHERE
            numItemCode = v_itemcode;
         end if;
         DROP TABLE IF EXISTS tt_TEMPRECEIEVEDITEMS CASCADE;
         CREATE TEMPORARY TABLE tt_TEMPRECEIEVEDITEMS
         (
            ID INTEGER,
            numOIRLID NUMERIC(18,0),
            numWarehouseItemID NUMERIC(18,0),
            numUnitReceieved DOUBLE PRECISION,
            numDeletedReceievedQty DOUBLE PRECISION
         );
         INSERT INTO
         tt_TEMPRECEIEVEDITEMS
         SELECT
         ROW_NUMBER() OVER(ORDER BY ID),
				ID,
				numWarehouseItemID,
				numUnitReceieved,
				coalesce(numDeletedReceievedQty,0)
         FROM
         OpportunityItemsReceievedLocation
         WHERE
         numDomainId = v_numDomainID
         AND numOppID = v_numOppID
         AND numOppItemID = v_numoppitemtCode;
         select   COUNT(*) INTO v_COUNT FROM tt_TEMPRECEIEVEDITEMS;
         WHILE v_i <= v_COUNT LOOP
            select   numOIRLID, coalesce(numWarehouseItemID,0), coalesce(numUnitReceieved,0), numDeletedReceievedQty INTO v_numOIRLID,v_numTempWarehouseItemID,v_numTempUnitReceieved,v_numTempDeletedReceievedQty FROM
            tt_TEMPRECEIEVEDITEMS WHERE
            ID = v_i;
            v_description := CONCAT('PO Re-Open (Qty:',v_numTempUnitReceieved,' Received:',v_QtyReceived,')');
            UPDATE
            WareHouseItems
            SET
            numOnHand = coalesce(numOnHand,0) -(coalesce(v_numTempUnitReceieved,0) -v_numTempDeletedReceievedQty),dtModified = LOCALTIMESTAMP
            WHERE
            numWareHouseItemID = v_numTempWarehouseItemID;
				 --  numeric(9, 0)
					 --  numeric(9, 0)
					 --  tinyint
					 --  varchar(100)
            UPDATE OpportunityItemsReceievedLocation SET numDeletedReceievedQty = 0 WHERE ID = v_numOIRLID;
            PERFORM USP_ManageWareHouseItems_Tracking(v_numWareHouseItemID := v_numTempWarehouseItemID,v_numReferenceID := v_numOppID,
            v_tintRefType := 4::SMALLINT,v_vcDescription := v_description,
            v_numModifiedBy := v_numUserCntID,v_dtRecordDate := v_dtItemReceivedDate,
            v_numDomainID := v_numDomainID,SWV_RefCur := null);
            v_i := v_i::bigint+1;
         END LOOP;
         DELETE FROM OpportunityItemsReceievedLocation WHERE numOppID = v_numOppID AND numOppItemID = v_numoppitemtCode;
         v_description := CONCAT('PO Re-Open (Qty:',v_numUnits,' Received:',v_QtyReceived,' Deleted:',v_numDeletedReceievedQty,
         ')');
         UPDATE
         WareHouseItems
         SET
         numOnHand = v_onHand -(v_numUnits -v_numDeletedReceievedQty),numonOrder = v_onOrder+v_numUnits,
         dtModified = LOCALTIMESTAMP
         WHERE
         numWareHouseItemID = v_numWareHouseItemID
         AND coalesce(numWLocationID,0) <> -1;
         UPDATE
         WareHouseItems
         SET
         numonOrder = v_onOrder+v_numUnits,dtModified = LOCALTIMESTAMP
         WHERE
         numWareHouseItemID = v_numWareHouseItemID
         AND coalesce(numWLocationID,0) = -1;
      end if;
   end if;
            
            
            
   IF v_numWareHouseItemID > 0 AND v_description <> 'DO NOT ADD WAREHOUSE TRACKING' then
	 --  numeric(9, 0)
			 --  numeric(9, 0)
			 --  tinyint
			 --  varchar(100)
      IF v_tintOpptype = 1 then --SO
         v_tintRefType := 3;
      ELSEIF v_tintOpptype = 2
      then --PO
         v_tintRefType := 4;
      end if;
      SELECT  numDomainID INTO v_numDomain FROM WareHouseItems WHERE WareHouseItems.numWareHouseItemID = v_numWareHouseItemID     LIMIT 1;
      PERFORM USP_ManageWareHouseItems_Tracking(v_numWareHouseItemID := v_numWareHouseItemID,v_numReferenceID := v_numOppID,
      v_tintRefType := v_tintRefType::SMALLINT,v_vcDescription := v_description,
      v_numModifiedBy := v_numUserCntID,v_dtRecordDate := v_dtItemReceivedDate,
      v_numDomainID := v_numDomain,SWV_RefCur := null);
   end if;
END; $$;


