-- Stored procedure definition script USP_GET_IMPORT_MAPPED_FIELDS_DATA for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GET_IMPORT_MAPPED_FIELDS_DATA(v_numDomainID NUMERIC(18,0),
	v_numFormID NUMERIC(18,0),
	v_vcFields TEXT,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE

   v_hDocItem  INTEGER;
BEGIN
   DROP TABLE IF EXISTS tt_TEMPFIELDS CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPFIELDS
   (
      numFieldID NUMERIC(18,0),
      intMapColumnNo INTEGER,
      bitCustom BOOLEAN
   );

   IF LENGTH(COALESCE(v_vcFields,'')) > 0 THEN
		INSERT INTO tt_TEMPFIELDS
		(
			numFieldID
			,intMapColumnNo
			,bitCustom
		)
		SELECT
			FormFieldID
			,ImportFieldID
			,IsCustomField
		FROM
		XMLTABLE
		(
		'NewDataSet/Table1'
		PASSING 
			CAST(v_vcFields AS XML)
		COLUMNS
			id FOR ORDINALITY,
			FormFieldID NUMERIC(18,0) PATH 'FormFieldID',
			ImportFieldID NUMERIC(18,0) PATH 'ImportFieldID',
			IsCustomField BOOLEAN PATH 'IsCustomField'
		) AS X;
   END IF;

   open SWV_RefCur for SELECT * FROM(SELECT DISTINCT
      cast(PARENT.numFieldID as NUMERIC(18,0)) AS FormFieldID
			,IFFM.intMapColumnNo AS ImportFieldID
			,PARENT.vcDbColumnName AS dbFieldName
			,PARENT.vcFieldName AS FormFieldName
			,coalesce(PARENT.vcAssociatedControlType,'') AS vcAssociatedControlType
			,coalesce(PARENT.bitDefault,false) AS bitDefault
			,coalesce(PARENT.vcFieldType,'') AS vcFieldType
			,coalesce(PARENT.vcListItemType,'') AS vcListItemType
			,coalesce(PARENT.vcLookBackTableName,'') AS vcLookBackTableName
			,PARENT.vcOrigDbColumnName AS vcOrigDbColumnName
			,coalesce(PARENT.vcPropertyName,'') AS vcPropertyName
			,false AS bitCustomField,
			PARENT.numDomainID AS DomainID
			,PARENT.vcFieldDataType
			,PARENT.intFieldMaxLength
      FROM
      View_DynamicDefaultColumns PARENT
      INNER JOIN
      tt_TEMPFIELDS IFFM
      ON
      PARENT.numFieldID = IFFM.numFieldID
      AND coalesce(IFFM.bitCustom,false) = false
      WHERE
      PARENT.numDomainID = v_numDomainID
      AND numFormID = v_numFormID
      UNION ALL
      SELECT DISTINCT
      IFFM.numFieldID AS FormFieldID
			,IFFM.intMapColumnNo AS ImportFieldID
			,CFW.FLd_label AS dbFieldName
			,CFW.FLd_label AS FormFieldName
			,CASE WHEN CFW.fld_type = 'Drop Down List Box' THEN 'SelectBox' ELSE CFW.fld_type END AS vcAssociatedControlType
			,false AS bitDefault
			,'R' AS vcFieldType
			,'' AS vcListItemType
			,'' AS  vcLookBackTableName
			,CFW.FLd_label AS vcOrigDbColumnName
			,'' AS vcPropertyName
			,true AS bitCustomField
			,0
			,'' AS vcFieldDataType
			,0 AS intFieldMaxLength
      FROM tt_TEMPFIELDS IFFM
      INNER JOIN CFW_Fld_Master CFW ON IFFM.numFieldID = CFW.Fld_id AND coalesce(bitCustom,false) = true
      INNER JOIN CFW_Loc_Master LOC ON CFW.Grp_id = LOC.Loc_id
      INNER JOIN DycFormSectionDetail DFSD ON DFSD.Loc_id = LOC.Loc_id
      WHERE (DFSD.numFormID = v_numFormID OR DFSD.numFormID =(CASE WHEN v_numFormID = 20 THEN 48 ELSE v_numFormID END))
      AND Grp_id = DFSD.Loc_id) TABLE1
   ORDER BY
   ImportFieldID DESC;
END; $$;

