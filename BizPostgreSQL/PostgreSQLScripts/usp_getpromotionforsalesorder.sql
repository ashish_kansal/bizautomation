-- Stored procedure definition script USP_GetPromotionForSalesOrder for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetPromotionForSalesOrder(v_numDomainID NUMERIC(18,0),
	v_numDivisionID NUMERIC(18,0),
	v_tintOppType SMALLINT,
	v_numItemCode NUMERIC(18,0),
	v_numUOMID NUMERIC(18,0),
	v_numWarehouseItemID NUMERIC(18,0),
	v_numQty INTEGER,
	v_monPrice DOUBLE PRECISION,
	v_numOppID NUMERIC(18,0),
	v_numOppItemID NUMERIC(18,0),
	v_vcSelectedKitChildItems TEXT,
	v_numShippingCountry NUMERIC(18,0),
	v_numWarehouseID NUMERIC(18,0),
	v_vcCoupon VARCHAR(200),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numItemClassification  NUMERIC(18,0);
   v_numPromotionID  NUMERIC(18,0);

	-- GET ITEM DETAILS
BEGIN
   select   numItemClassification INTO v_numItemClassification FROM
   Item
   LEFT JOIN LATERAL(SELECT  numWareHouseItemID,monWListPrice FROM WareHouseItems WHERE numItemID = v_numItemCode AND (numWareHouseItemID = v_numWarehouseItemID OR coalesce(v_numWarehouseItemID,0) = 0) LIMIT 1) AS WareHouseItems on TRUE WHERE
   Item.numItemCode = v_numItemCode;

	-- CHECK IF PROMOTION CAN BE TRIGGERED FROM THIS ITEM
   select   numProId INTO v_numPromotionID FROM
   PromotionOffer PO WHERE
   numDomainId = v_numDomainID
   AND coalesce(bitEnabled,false) = true
   AND coalesce(bitApplyToInternalOrders,false) = true
   AND 1 =(CASE WHEN bitRequireCouponCode = true THEN CASE WHEN coalesce(intCouponCodeUsed,0) < coalesce(tintUsageLimit,0) OR coalesce(tintUsageLimit,0) = 0  THEN 1 ELSE 0 END ELSE 1 END)
   AND 1 =(CASE WHEN bitNeverExpires = true THEN 1 ELSE(CASE WHEN CAST(LOCALTIMESTAMP  AS DATE) >= dtValidFrom AND CAST(LOCALTIMESTAMP  AS DATE) <= dtValidTo THEN 1 ELSE 0 END) END)
   AND 1 =(CASE
   WHEN tintOfferBasedOn = 1 THEN(CASE WHEN(SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId = PO.numProId AND tintRecordType = 5 AND tintType = 1 AND numValue = v_numItemCode) > 0 THEN 1 ELSE 0 END)
   WHEN tintOfferBasedOn = 2 THEN(CASE WHEN(SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId = PO.numProId AND tintRecordType = 5 AND tintType = 2 AND numValue = v_numItemClassification) > 0 THEN 1 ELSE 0 END)
   ELSE 0
   END)   ORDER BY
   CASE
   WHEN coalesce(PO.bitRequireCouponCode,false) = true AND PO.txtCouponCode = v_vcCoupon THEN 1
   WHEN PO.tintOfferBasedOn = 1 AND PO.tintDiscoutBaseOn = 1 THEN 2
   WHEN PO.tintOfferBasedOn = 2 AND PO.tintDiscoutBaseOn = 1 THEN 3
   WHEN PO.tintOfferBasedOn = 1 AND PO.tintDiscoutBaseOn = 2 THEN 4
   WHEN PO.tintOfferBasedOn = 2 AND PO.tintDiscoutBaseOn = 2 THEN 5
   WHEN PO.tintOfferBasedOn = 1 AND PO.tintDiscoutBaseOn = 3 THEN 6
   WHEN PO.tintOfferBasedOn = 2 AND PO.tintDiscoutBaseOn = 3 THEN 7
   END LIMIT 1;


   IF coalesce(v_numPromotionID,0) > 0 then
	
		-- Get Top 1 Promotion Based on priority

      open SWV_RefCur for SELECT
      numProId
			,vcProName
			,bitNeverExpires
			,bitRequireCouponCode
			,txtCouponCode
			,tintUsageLimit
			,intCouponCodeUsed
			,bitFreeShiping
			,monFreeShippingOrderAmount
			,numFreeShippingCountry
			,bitFixShipping1
			,monFixShipping1OrderAmount
			,monFixShipping1Charge
			,bitFixShipping2
			,monFixShipping2OrderAmount
			,monFixShipping2Charge
			,tintOfferTriggerValueType
			,fltOfferTriggerValue
			,tintOfferBasedOn
			,fltDiscountValue
			,tintDiscountType
			,tintDiscoutBaseOn
			,FormatedDateFromDate(dtValidTo,v_numDomainID) AS dtExpire
			,(CASE tintOfferBasedOn
      WHEN 1 THEN(SELECT OVERLAY((SELECT CONCAT(',',numValue) FROM PromotionOfferItems WHERE numProId = PO.numProId AND tintRecordType = 5 AND tintType = 1) placing '' from 1 for 1))
      WHEN 2 THEN(SELECT OVERLAY((SELECT CONCAT(',',numValue) FROM PromotionOfferItems WHERE numProId = PO.numProId AND tintRecordType = 5 AND tintType = 2) placing '' from 1 for 1))
      END) As vcPromotionItems
			,(CASE tintDiscoutBaseOn
      WHEN 1 THEN(SELECT OVERLAY((SELECT CONCAT(',',numValue) FROM PromotionOfferItems WHERE numProId = PO.numProId AND tintRecordType = 6 AND tintType = 1) placing '' from 1 for 1))
      WHEN 2 THEN(SELECT OVERLAY((SELECT CONCAT(',',numValue) FROM PromotionOfferItems WHERE numProId = PO.numProId AND tintRecordType = 6 AND tintType = 2) placing '' from 1 for 1))
      WHEN 3 THEN(SELECT OVERLAY((SELECT CONCAT(',',SimilarItems.numItemCode) FROM SimilarItems WHERE 1 =(CASE
               WHEN tintOfferBasedOn = 1 THEN CASE WHEN numParentItemCode IN(SELECT numValue FROM PromotionOfferItems WHERE numProId = PO.numProId AND tintRecordType = 5 AND tintType = 1) THEN 1 ELSE 0 END
               WHEN tintOfferBasedOn = 2 THEN CASE WHEN numParentItemCode IN(SELECT numItemCode FROM Item WHERE numDomainID = v_numDomainID AND numItemClassification IN(SELECT numValue FROM PromotionOfferItems WHERE numProId = PO.numProId AND tintRecordType = 5 AND tintType = 2)) THEN 1 ELSE 0 END
               ELSE 0
               END)) placing '' from 1 for 1))
      END) As vcItems
			,CONCAT('Buy ',CASE WHEN tintOfferTriggerValueType = 1 THEN SUBSTR(fltOfferTriggerValue,1,30) ELSE CONCAT('$',fltOfferTriggerValue) END,CASE tintOfferBasedOn
      WHEN 1 THEN CONCAT(' of "',(SELECT OVERLAY((SELECT CONCAT(', ',Item.vcItemName) FROM PromotionOfferItems INNER JOIN Item ON PromotionOfferItems.numValue = Item.numItemCode WHERE numProId = PO.numProId AND tintRecordType = 5 AND tintType = 1) placing '' from 1 for 2)),'"')
      WHEN 2 THEN CONCAT(' of items belonging to classification(s) "',(SELECT OVERLAY((SELECT CONCAT(', ',Listdetails.vcData) FROM PromotionOfferItems INNER JOIN Listdetails ON PromotionOfferItems.numValue = Listdetails.numListItemID WHERE numListID = 36 AND numProId = PO.numProId AND tintRecordType = 5 AND tintType = 2) placing '' from 1 for 2)), 
         '"')
      END,' & get ',CASE tintDiscountType WHEN 1 THEN CONCAT(fltDiscountValue,'% off on') WHEN 2 THEN CONCAT('$',fltDiscountValue,' off on') WHEN 3 THEN CONCAT(fltDiscountValue,' of') END, 
      CASE
      WHEN tintDiscoutBaseOn = 1 THEN
         CONCAT(CONCAT(' "',(SELECT OVERLAY((SELECT CONCAT(',',Item.vcItemName) FROM PromotionOfferItems INNER JOIN Item ON PromotionOfferItems.numValue = Item.numItemCode WHERE numProId = PO.numProId AND tintRecordType = 6 AND tintType = 1) placing '' from 1 for 1)), 
         '"'),(CASE WHEN PO.tintDiscountType = 3 THEN ' free' ELSE '' END),
         '.')
      WHEN tintDiscoutBaseOn = 2 THEN
         CONCAT(CONCAT(' items belonging to classification(s) "',(SELECT OVERLAY((SELECT CONCAT(',',Listdetails.vcData) FROM PromotionOfferItems INNER JOIN Listdetails ON PromotionOfferItems.numValue = Listdetails.numListItemID WHERE numListID = 36 AND numProId = PO.numProId AND tintRecordType = 6 AND tintType = 2) placing '' from 1 for 1)), 
         '"'),(CASE WHEN PO.tintDiscountType = 3 THEN ' free' ELSE '' END),
         '.')
      WHEN tintDiscoutBaseOn = 3 THEN ' related items.'
      ELSE ''
      END) AS vcPromotionDescription
				,CONCAT((CASE WHEN coalesce(bitFixShipping1,false) = true THEN CONCAT('Spend $',monFixShipping1OrderAmount,' and get $',monFixShipping1Charge,
         ' shipping. ') ELSE '' END),(CASE WHEN coalesce(bitFixShipping2,false) = true  THEN CONCAT('Spend $',monFixShipping2OrderAmount,' and get $',monFixShipping2Charge,
         ' shipping. ') ELSE '' END),(CASE WHEN (bitFreeShiping = true AND numFreeShippingCountry = v_numShippingCountry) THEN CONCAT('Spend $',monFreeShippingOrderAmount,' and shipping is FREE! ') ELSE '' END)) AS vcShippingDescription,
				CASE
      WHEN coalesce(PO.bitRequireCouponCode,false) = true AND PO.txtCouponCode = v_vcCoupon THEN 1
      WHEN PO.tintOfferBasedOn = 1 AND PO.tintDiscoutBaseOn = 1 THEN 2
      WHEN PO.tintOfferBasedOn = 2 AND PO.tintDiscoutBaseOn = 1 THEN 3
      WHEN PO.tintOfferBasedOn = 1 AND PO.tintDiscoutBaseOn = 2 THEN 4
      WHEN PO.tintOfferBasedOn = 2 AND PO.tintDiscoutBaseOn = 2 THEN 5
      WHEN PO.tintOfferBasedOn = 1 AND PO.tintDiscoutBaseOn = 3 THEN 6
      WHEN PO.tintOfferBasedOn = 2 AND PO.tintDiscoutBaseOn = 3 THEN 7
      END AS tintPriority
      FROM
      PromotionOffer PO
      LEFT JOIN ShippingPromotions SP ON PO.numDomainId = SP.numDomainID
      WHERE
      PO.numDomainId = v_numDomainID
      AND numProId = v_numPromotionID;
   end if;
END; $$; 












