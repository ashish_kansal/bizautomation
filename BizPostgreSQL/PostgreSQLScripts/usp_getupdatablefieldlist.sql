-- Stored procedure definition script usp_GetUpdatableFieldList for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION usp_GetUpdatableFieldList(v_numDomainID NUMERIC(9,0),
 v_numFormID NUMERIC DEFAULT 1, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
BEGIN
   IF v_numFormID = 1 then

      open SWV_RefCur for
      SELECT vcFieldName as vcFormFieldName, vcOrigDbColumnName || '~' || vcLookBackTableName || '~' || vcListItemType || '~' || CAST(numListID AS VARCHAR(10)) || '~' || vcFieldDataType || '~' || vcAssociatedControlType || '~' || CAST(numFieldID AS VARCHAR(10)) || '~' || vcDbColumnName as Value
      FROM View_DynamicDefaultColumns
      WHERE numDomainID = v_numDomainID and bitAllowEdit = true
      AND bitDeleted = false and numFormId = v_numFormID
      UNION
      select FLd_label,'Custom~' || case when Grp_id = 1 then 'DivisionMaster~LI~' when Grp_id = 4 then 'Contacts~LI~' end || CAST(coalesce(numlistid,0) AS VARCHAR(10)) || '~' || Case fld_type WHEN 'SelectBox' THEN 'N~' ELSE 'V~' END || fld_type || '~' || CAST(Fld_id AS VARCHAR(10)) || '~' as Value
      from  CFW_Fld_Master
      where (Grp_id = 1 or Grp_id = 4) and fld_type <> 'link' and numDomainID = v_numDomainID
      UNION
      select 'Organization Record Owner',CAST('Ownership~DivisionMaster~~~~SelectBox~~~' AS VARCHAR(1)) as Value
      UNION
      select 'Contact Record Owner',CAST('Ownership~Contacts~~~~SelectBox~~~' AS VARCHAR(1)) as Value
      UNION
      select 'Drip Campaign',CAST('DripCampaign~Contacts~~~~SelectBox~~~' AS VARCHAR(1)) as Value ORDER BY vcFormFieldName;
   ELSEIF v_numFormID = 15
   then

      open SWV_RefCur for
      SELECT vcFieldName as vcFormFieldName, vcOrigDbColumnName || '~' || vcLookBackTableName || '~' || vcListItemType || '~' || CAST(numListID AS VARCHAR(10)) || '~' || vcFieldDataType || '~' || vcAssociatedControlType || '~' || CAST(numFieldID AS VARCHAR(10)) || '~' || vcDbColumnName as Value
      FROM View_DynamicDefaultColumns
      WHERE numDomainID = v_numDomainID and bitAllowEdit = true
      AND bitDeleted = false and numFormId = v_numFormID and vcOrigDbColumnName != 'numContactType';
   ELSEIF v_numFormID = 29
   then -- adv search (item search)

      open SWV_RefCur for
      SELECT vcFieldName as vcFormFieldName, vcOrigDbColumnName || '~' || vcLookBackTableName || '~' || vcListItemType || '~' || CAST(numListID AS VARCHAR(10)) || '~' || vcFieldDataType || '~' || vcAssociatedControlType || '~' || CAST(numFieldID AS VARCHAR(10)) || '~' || vcDbColumnName as Value
      FROM View_DynamicDefaultColumns
      WHERE numDomainID = v_numDomainID and bitAllowEdit = true
      AND bitDeleted = false and numFormId = v_numFormID AND vcLookBackTableName <> 'WareHouseItmsDTL'
      AND vcDbColumnName <> 'vcPartNo' AND vcDbColumnName <> 'monAverageCost'
      AND vcOrigDbColumnName <> 'numAllocation' AND vcOrigDbColumnName <> 'numBackOrder'
      AND vcOrigDbColumnName <> 'numOnHand' AND vcOrigDbColumnName <> 'numOnOrder'
      UNION
      SELECT vcFieldName as vcFormFieldName, vcOrigDbColumnName || '~' || vcLookBackTableName || '~' || coalesce(vcListItemType,'') || '~' || CAST(numListID AS VARCHAR(10)) || '~' || vcFieldDataType || '~' || vcAssociatedControlType || '~' || CAST(numFieldID AS VARCHAR(10)) || '~' || vcDbColumnName as Value
      FROM View_DynamicDefaultColumns
      WHERE numDomainID = v_numDomainID and numFormId = 29 AND vcLookBackTableName = 'WareHouseItems'
      AND vcOrigDbColumnName <> 'numAllocation' AND vcOrigDbColumnName <> 'numBackOrder'
      AND vcOrigDbColumnName <> 'numOnHand' AND vcOrigDbColumnName <> 'numOnOrder'
      Union
      SELECT 'Tax : ' || coalesce(vcTaxName,'') as vcFormFieldName, 'Tax~Tax~~0~Y~CheckBox~' || CAST(numTaxItemID AS VARCHAR(10)) || '~' as Value
      FROM TaxItems
      WHERE numDomainID = v_numDomainID;
   end if;
   RETURN;
END; $$;

 


