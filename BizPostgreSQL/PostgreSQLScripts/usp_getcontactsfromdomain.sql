-- Stored procedure definition script USP_GetContactsFromDomain for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetContactsFromDomain(v_numDomainId NUMERIC(9,0) DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for select numContactId,vcFirstName || ' ' || vcLastname as Name from AdditionalContactsInformation A
   join Domain D
   on D.numDomainId = A.numDomainID
   where A.numDivisionId = D.numDivisionId
   and D.numDomainId = v_numDomainId;
END; $$;












