DROP FUNCTION IF EXISTS GetChildOperationBudgetDetails;

CREATE OR REPLACE FUNCTION GetChildOperationBudgetDetails(v_numDomainId NUMERIC(9,0) DEFAULT 0,          
v_numBudgetId NUMERIC(9,0) DEFAULT 0,      
v_sintByte SMALLINT DEFAULT 0,  
v_intType INTEGER DEFAULT 0,  
v_Id NUMERIC(9,0) DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   Drop table IF EXISTS tt_TEMPGetChildOperationBudgetDetails CASCADE;
   Create TEMPORARY TABLE tt_TEMPGetChildOperationBudgetDetails
   (
      numBudgetId NUMERIC(9,0),
      vcmonthName VARCHAR(100),
      monAmount DECIMAL(20,5)
   );          
--         
-- Declare @monAmount as DECIMAL(20,5)       
-- Declare @month as tinyint     
-- Declare @vcMonDescription as varchar(200)     
-- Declare @Date as datetime  
-- Declare @dtStartDate as datetime  
-- Set @month=month(getutcdate())        
--  Set @Date = dateadd(year,@intType,getutcdate())      
--if @sintByte=0      
--Begin      
--    While @month<=24                              
--  Begin              
--  Set @dtStartDate='01/01/'+convert(varchar(4),year(getutcdate()))             
--  Set @dtStartDate=dateadd(month,@month-1,@dtStartDate)             
--  Insert into #temp(numBudgetId,vcmonthName,monAmount) Values  
--        (@numBudgetId, convert(varchar(3),DateName(month,@dtStartDate)),dbo.fn_GetBudgetMonthDet(@numBudgetId,month(@dtStartDate),year(@dtStartDate),@Id))  
--  Set @month=@month+1
-- End  
--End  
   
   open SWV_RefCur for Select * From tt_TEMPGetChildOperationBudgetDetails;          
   Drop table IF EXISTS tt_TEMPGetChildOperationBudgetDetails CASCADE;
   RETURN;
END; $$;













