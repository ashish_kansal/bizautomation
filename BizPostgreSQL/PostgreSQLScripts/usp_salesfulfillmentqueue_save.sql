DROP FUNCTION IF EXISTS USP_SalesFulfillmentQueue_Save;

CREATE OR REPLACE FUNCTION USP_SalesFulfillmentQueue_Save(v_numDomainId NUMERIC(18,0),
	v_numUserCntId NUMERIC(18,0),
	v_numOppID NUMERIC(18,0),
	v_numStatus NUMERIC(18,0))
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   IF(SELECT
   COUNT(*)
   FROM
   SalesFulfillmentConfiguration
   WHERE
   numDomainID = v_numDomainId
   AND coalesce(bitActive,false) = true
   AND 1 =(CASE
   WHEN coalesce(bitRule1IsActive,false) = true AND tintRule1Type = 2 AND numRule1OrderStatus = v_numStatus THEN 1
   WHEN coalesce(bitRule2IsActive,false) = true AND numRule2OrderStatus = v_numStatus THEN 1
   WHEN coalesce(bitRule3IsActive,false) = true AND numRule3OrderStatus = v_numStatus THEN 1
   WHEN coalesce(bitRule4IsActive,false) = true AND numRule4OrderStatus = v_numStatus THEN 1
   ELSE 0
   END)) > 0 then
	
      INSERT INTO SalesFulfillmentQueue(numDomainID
			,numUserCntID
			,numOppID
			,numOrderStatus
			,dtDate
			,bitExecuted
			,intNoOfTimesTried)
		VALUES(v_numDomainId
			,v_numUserCntId
			,v_numOppID
			,v_numStatus
			,TIMEZONE('UTC',now())
			,false
			,0);
   end if;
   RETURN;
END; $$;



