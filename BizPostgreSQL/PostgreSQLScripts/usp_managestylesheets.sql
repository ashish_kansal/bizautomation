-- Stored procedure definition script USP_ManageStyleSheets for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ManageStyleSheets( -- 0- css , 1- javascript
v_numCssID NUMERIC(9,0) DEFAULT 0,
 v_StyleName VARCHAR(50) DEFAULT NULL,
 v_StyleFileName VARCHAR(50) DEFAULT NULL,
 v_numSiteID NUMERIC(9,0) DEFAULT NULL,
 v_numDomainID NUMERIC(9,0) DEFAULT NULL,
 v_tintType SMALLINT DEFAULT 0, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_intDisplayOrder  INTEGER;
BEGIN
   IF v_numCssID = 0 AND (NOT EXISTS(SELECT StyleName FROM StyleSheets WHERE StyleName = v_StyleName
   AND numSiteID = v_numSiteID
   AND tintType = v_tintType)) then

      IF v_numSiteID = 0 then
         v_numSiteID := NULL;
      end if;
      IF v_tintType = 0 then
      
         INSERT INTO StyleSheets(StyleName,
                        styleFileName,
                        numSiteID,
                        numDomainID,
                        tintType)
           VALUES(v_StyleName,
                        v_StyleFileName,
                        v_numSiteID,
                        v_numDomainID,
                        v_tintType);
      ELSEIF v_tintType = 1
      then
     
         select(coalesce(MAX(intDisplayOrder),0)+1) INTO v_intDisplayOrder FROM StyleSheets WHERE  numSiteID = v_numSiteID AND numDomainID =  v_numDomainID;
         INSERT INTO StyleSheets(StyleName,
                        styleFileName,
                        numSiteID,
                        numDomainID,
                        tintType,
                        intDisplayOrder)
           VALUES(v_StyleName,
                        v_StyleFileName,
                        v_numSiteID,
                        v_numDomainID,
                        v_tintType,
                        v_intDisplayOrder);
      ELSEIF v_tintType = 3
      then
	 
         INSERT INTO StyleSheets(StyleName,
                        styleFileName,
                        numSiteID,
                        numDomainID,
                        tintType)
           VALUES(v_StyleName,
                        v_StyleFileName,
                        v_numSiteID,
                        v_numDomainID,
                        v_tintType);
      ELSEIF v_tintType = 4
      then
     
         select(coalesce(MAX(intDisplayOrder),0)+1) INTO v_intDisplayOrder FROM StyleSheets WHERE  numSiteID = v_numSiteID AND numDomainID =  v_numDomainID;
         INSERT INTO StyleSheets(StyleName,
                        styleFileName,
                        numSiteID,
                        numDomainID,
                        tintType,
                        intDisplayOrder)
           VALUES(v_StyleName,
                        v_StyleFileName,
                        v_numSiteID,
                        v_numDomainID,
                        v_tintType,
                        v_intDisplayOrder);
      end if;
      open SWV_RefCur for
      SELECT CURRVAL('StyleSheets_seq');
   ELSE
      UPDATE StyleSheets SET
      StyleName = v_StyleName
--  [StyleFileName] = @StyleFileName
      WHERE numCssID = v_numCssID AND numDomainID = v_numDomainID;
      open SWV_RefCur for
      SELECT v_numCssID;
   end if;
   RETURN;
END; $$;




