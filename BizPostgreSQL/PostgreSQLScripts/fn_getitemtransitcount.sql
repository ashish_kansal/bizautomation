-- Function definition script fn_GetItemTransitCount for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION fn_GetItemTransitCount(v_numItemcode NUMERIC,
	v_numDomainID NUMERIC)
RETURNS INTEGER LANGUAGE plpgsql
   AS $$
   DECLARE
   v_ItemTransitCount  INTEGER;
BEGIN
   select   COUNT(*) INTO v_ItemTransitCount FROM OpportunityMaster Opp INNER JOIN OpportunityItems OppI ON Opp.numOppId = OppI.numOppId
   INNER JOIN Item I on OppI.numItemCode = I.numItemCode
   LEFT JOIN  UOM u ON u.numUOMId = OppI.numUOMId WHERE Opp.tintopptype = 2 and Opp.tintoppstatus = 1 and Opp.tintshipped = 0
   and Opp.numDomainId = v_numDomainID AND OppI.numItemCode = v_numItemcode
   and coalesce(OppI.numUnitHour,0) -coalesce(OppI.numUnitHourReceived,0) > 0;

   RETURN v_ItemTransitCount;
END; $$;

