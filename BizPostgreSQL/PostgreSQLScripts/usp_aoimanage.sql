-- Stored procedure definition script USP_AOIManage for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_AOIManage(v_numContactID NUMERIC(9,0) DEFAULT null,  
v_strAOI TEXT DEFAULT '')
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   delete from AOIContactLink where numContactID = v_numContactID;  
  
   insert into AOIContactLink(numAOIID,numContactID,tintSelected)
	select 
		X.numAOIId,v_numContactID,X.Status  
	from
	XMLTABLE
	(
		'NewDataSet/Table'
		PASSING 
			CAST(v_strAOI AS XML)
		COLUMNS
			id FOR ORDINALITY,
			numAOIId NUMERIC(9,0) PATH 'numAOIId',
			Status SMALLINT PATH 'Status'
	) AS X;
  
   RETURN;
END; $$;


