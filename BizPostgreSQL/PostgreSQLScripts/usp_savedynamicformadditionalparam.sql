-- Stored procedure definition script usp_saveDynamicFormAdditionalParam for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION usp_saveDynamicFormAdditionalParam(v_numFormId INTEGER,                   
 v_vcAdditionalParam VARCHAR(100) ,  
 v_numGrpId NUMERIC(9,0),
 v_numDomainID NUMERIC(9,0))
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   if not exists(select * from DynamicFormMasterParam where numFormID = v_numFormId and numDomainID = v_numDomainID) then

      insert into DynamicFormMasterParam(numDomainID,vcAdditionalParam,numGrpId,numFormID)
	values(v_numDomainID,v_vcAdditionalParam,v_numGrpId,v_numFormId);
   else
      Update DynamicFormMasterParam
      set vcAdditionalParam = v_vcAdditionalParam,numGrpId = v_numGrpId
      where numFormID = v_numFormId and numDomainID = v_numDomainID;
   end if;
   RETURN;
END; $$;


