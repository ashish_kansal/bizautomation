-- Stored procedure definition script usp_GetPageElements for PostgreSQL
CREATE OR REPLACE FUNCTION usp_GetPageElements(v_numPageID NUMERIC(9,0) DEFAULT 0,
	v_numModuleID  NUMERIC(9,0) DEFAULT 0,
	v_numUserID  NUMERIC(9,0) DEFAULT 0   
--
,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   IF v_numUserID <> 0 then
			
      open SWV_RefCur for SELECT  cast(PageActionElements.vcActionName as VARCHAR(255)), cast(PageActionElements.vcElementInitiatingAction as VARCHAR(255)), cast(PageActionElements.vcElementInitiatingActionType as VARCHAR(255)),
				cast(PageActionElements.vcChildElementInitiatingAction as VARCHAR(255)), cast(PageActionElements.vcChildElementInitiatingActionType as VARCHAR(255)),
				cast(GroupAuthorization.intViewAllowed as VARCHAR(255)), cast(GroupAuthorization.intAddAllowed as VARCHAR(255)), cast(GroupAuthorization.intUpdateAllowed as VARCHAR(255)),
				cast(GroupAuthorization.intDeleteAllowed as VARCHAR(255)), cast(GroupAuthorization.intExportAllowed as VARCHAR(255))
      FROM         GroupAuthorization INNER JOIN
      PageActionElements ON GroupAuthorization.numModuleID = PageActionElements.numModuleID AND
      GroupAuthorization.numPageID = PageActionElements.numPageID INNER JOIN
      UserGroups ON GroupAuthorization.numGroupID = UserGroups.numGroupID
      WHERE     PageActionElements.numPageID = v_numPageID AND PageActionElements.numModuleID = v_numModuleID AND UserGroups.numUserID = v_numUserID;
   end if;
END; $$;












