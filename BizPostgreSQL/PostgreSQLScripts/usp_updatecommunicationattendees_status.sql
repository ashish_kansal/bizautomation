-- Stored procedure definition script USP_UpdateCommunicationAttendees_Status for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_UpdateCommunicationAttendees_Status(v_numCommId NUMERIC(9,0) DEFAULT 0,
v_numUserCntId NUMERIC(9,0) DEFAULT 0,
v_numStatus NUMERIC(9,0) DEFAULT 0)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   UPDATE CommunicationAttendees SET numStatus = v_numStatus where
   numCommId = v_numCommId AND numContactId = v_numUserCntId;
   RETURN;
END; $$;

--Created by Anoop Jayaraj


