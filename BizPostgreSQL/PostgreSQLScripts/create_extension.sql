-- Function definition script create_extension for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
DO $$
BEGIN
	IF EXISTS(SELECT 1 FROM pg_available_extensions WHERE "name"='pgcrypto') AND
		NOT EXISTS(SELECT 1 FROM pg_extension WHERE "extname"='pgcrypto') THEN
		CREATE EXTENSION pgcrypto;
	END IF;
END$$;

