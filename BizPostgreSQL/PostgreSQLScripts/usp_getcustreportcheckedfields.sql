-- Stored procedure definition script usp_getCustReportCheckedFields for PostgreSQL
CREATE OR REPLACE FUNCTION usp_getCustReportCheckedFields(v_ReportId NUMERIC DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for select * from CustReportFields where numCustomReportId = v_ReportId;
END; $$;












