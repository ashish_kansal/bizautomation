-- Stored procedure definition script USP_WareHouseItmsDTL_CheckForDuplicate for PostgreSQL
CREATE OR REPLACE FUNCTION USP_WareHouseItmsDTL_CheckForDuplicate(v_numDomainID NUMERIC(9,0),
	v_numWarehouseID NUMERIC(18,0),
	v_numWarehouseLocationID NUMERIC(18,0),
	v_numItemCode NUMERIC(18,0),
	v_bitSerial BOOLEAN,
    v_vcSerialLot TEXT,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
	--IMPORTANT: THIS PROCEDURE IS ALSO CALLED FROM "USP_WarehouseItems_Save" 
   AS $$
   DECLARE
   v_bitDuplicate  BOOLEAN DEFAULT 0;
BEGIN
   DROP TABLE IF EXISTS tt_TEMPSERIALLOT CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPSERIALLOT
   (
      vcSerialLot VARCHAR(100),
      numQty INTEGER,
      dtExpirationDate TIMESTAMP
   );

   INSERT INTO
   tt_TEMPSERIALLOT
   SELECT
   vcSerialLot,
		numQty,
		NULLIF(dtExpirationDate,'')::TIMESTAMP
   FROM
   XMLTABLE
	(
		'SerialLots/SerialLot'
		PASSING 
			CAST(v_vcSerialLot AS XML)
		COLUMNS
			id FOR ORDINALITY,
			vcSerialLot VARCHAR(100) PATH 'vcSerialLot',
			numQty INTEGER PATH 'numQty',
			dtExpirationDate VARCHAR(100) PATH 'dtExpirationDate'
	) X;

	--SERIAL ITEM
   IF v_bitSerial = true then
	
		--NOTE: USER CAN NOT ADD DUPLICATE SERIAL NUMBER DOESN�T MATTER IF WAREHOUSE & LOCATION COMBINATION IS UNIQUE
      IF(SELECT
      COUNT(*)
      FROM
      Warehouses
      INNER JOIN
      WareHouseItems
      ON
      Warehouses.numWareHouseID = WareHouseItems.numWareHouseID
      INNER JOIN
      WareHouseItmsDTL
      ON
      WareHouseItems.numWareHouseItemID = WareHouseItmsDTL.numWareHouseItemID
      WHERE
      Warehouses.numDomainID = v_numDomainID
      AND WareHouseItems.numItemID = v_numItemCode
      AND WareHouseItmsDTL.vcSerialNo IN(SELECT coalesce(vcSerialLot,'') FROM tt_TEMPSERIALLOT)) > 0 then
		
         v_bitDuplicate := true;
      end if;
   ELSE
		--NOTE: USER CAN ADD SAME LOT NUMBER IN UNIQUE WAREHOUSE & LOCATION COMBINATION BUT NOT IN SAME
      IF(SELECT
      COUNT(*)
      FROM
      WareHouseItmsDTL
      INNER JOIN
      WareHouseItems
      ON
      WareHouseItmsDTL.numWareHouseItemID = WareHouseItems.numWareHouseItemID
      WHERE
      WareHouseItems.numWareHouseID = v_numWarehouseID
      AND WareHouseItems.numItemID = v_numItemCode
      AND coalesce(WareHouseItems.numWLocationID,0) = coalesce(v_numWarehouseLocationID,0)
      AND WareHouseItmsDTL.vcSerialNo IN(SELECT coalesce(vcSerialLot,'') FROM tt_TEMPSERIALLOT)) > 0 then
		
         v_bitDuplicate := true;
      end if;
   end if;

   open SWV_RefCur for SELECT v_bitDuplicate;
END; $$;












