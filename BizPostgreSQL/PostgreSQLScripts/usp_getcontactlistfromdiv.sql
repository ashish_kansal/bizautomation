-- Stored procedure definition script USP_GetContactListFromDiv for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetContactListFromDiv(v_numDivisionId NUMERIC,  
v_numDomainId NUMERIC ,  
v_ClientTimeZoneOffset NUMERIC,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for select
   c.numContractId,
c.numDivisionID,
vcContractName,
case when bitdays = true then
      coalesce(CAST(case when DATE_PART('day',TIMEZONE('UTC',now()):: timestamp -bintStartdate:: timestamp) >= 0 then DATE_PART('day',TIMEZONE('UTC',now()):: timestamp -bintStartdate:: timestamp) else 0 end
      AS VARCHAR(100)),
      cast(0 as TEXT)) || ' of ' || coalesce(CAST(DATE_PART('day',bintExpDate:: timestamp -bintStartDate:: timestamp) AS VARCHAR(100)),cast(0 as TEXT))
   else '-' end as Days,
case when bitincidents = true then
      coalesce(CAST(GetIncidents(c.numContractId,v_numDomainId,c.numDivisionID) AS VARCHAR(100)),cast(0 as TEXT)) || ' of ' || coalesce(CAST(numincidents AS VARCHAR(100)),cast(0 as TEXT))
   else '-' end as Incidents,
case when bitamount = true then
      coalesce(CAST(GetContractRemainingAmount(c.numContractId,c.numDivisionID,v_numDomainId) AS VARCHAR(100)),cast(0 as TEXT)) || ' of ' || CAST(CAST(c.numAmount+(Select coalesce(sum(monAmount),0) From OpportunityBizDocsDetails Where
         numContractId = c.numContractId) AS DECIMAL(10,2)) AS VARCHAR(100))
   else '-' end  as Amount,
case when bithour = true then
      coalesce(CAST(GetContractRemainingHours(c.numContractId,c.numDivisionID,v_numDomainId) AS VARCHAR(100)),cast(0 as TEXT)) || ' of ' || CAST(c.numHours AS VARCHAR(100))
   else '-' end as Hours,
FormatedDateFromDate(c.bintcreatedOn+CAST(-v_ClientTimeZoneOffset || 'minute' as interval),v_numDomainId) as bintCreatedOn ,
com.vcCompanyName 
   from ContractManagement c
   join DivisionMaster d on c.numDivisionID = d.numDivisionID
   join CompanyInfo com on d.numCompanyID = com.numCompanyId
   WHERE c.numDivisionID = v_numDivisionId and c.numdomainId = v_numDomainId;
END; $$;












