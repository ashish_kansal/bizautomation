-- Stored procedure definition script usp_GetAssociations for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION usp_GetAssociations(v_numDivID NUMERIC(9,0),
    v_bitGetAssociatedTo BOOLEAN DEFAULT true,
	v_numAssociationId NUMERIC(9,0) DEFAULT 0,
	v_numDominId NUMERIC(9,0) DEFAULT 0, INOUT SWV_RefCur refcursor default null, INOUT SWV_RefCur2 refcursor default null)
LANGUAGE plpgsql
   AS $$
BEGIN
   IF v_numAssociationId > 0 then
		
      open SWV_RefCur for
      SELECT CA.*, CMP.vcCompanyName FROM CompanyAssociations AS CA
      LEFT OUTER JOIN DivisionMaster DIV ON DIV.numDivisionID = CA.numDivisionID
      LEFT OUTER JOIN CompanyInfo CMP ON DIV.numCompanyID = CMP.numCompanyId
      WHERE CA.numAssociationID = v_numAssociationId AND CA.numDomainID = v_numDominId;
   ELSEIF v_bitGetAssociatedTo = true
   then
            
      open SWV_RefCur for
      SELECT  numAssociationID,
                        CMP.vcCompanyName,
                        CA.numCompanyID,
                        DIV.vcDivisionName,
                        CA.numDivisionID,
                        (SELECT    vcCompanyName
         FROM      DivisionMaster INNER JOIN CompanyInfo ON DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
         WHERE     DivisionMaster.numDivisionID = CA.numDivisionID) AS TargetCompany,
                        coalesce(ACI.numContactId,ACIDiv.numContactId) AS numContactID,
                        coalesce(ACI.vcFirstName,coalesce(ACIDiv.vcFirstName,'')) || ' '
      || coalesce(ACI.vcLastname,coalesce(ACIDiv.vcLastname,'')) AS ContactName,
                        coalesce(ACI.numPhone,coalesce(ACIDiv.numPhone,'')) || ', '
      || coalesce(ACI.numPhoneExtension,coalesce(ACIDiv.numPhoneExtension,'')) AS numPhone,
                        coalesce(ACI.vcEmail,coalesce(ACIDiv.numPhoneExtension,'')) AS vcEmail,
                        LD.vcData,
                        LD1.vcData AS Relationship,
                        LD2.vcData AS OrgProfile,
                        DIV.tintCRMType,
                        CASE WHEN CA.bitChildOrg = true THEN '(Child)'
      ELSE ''
      END AS bitParentOrg,
                        CASE WHEN bitShareportal = false THEN 'No'
      ELSE 'Yes'
      END AS Shareportal,coalesce(fn_GetState(AD2.numState),'') AS ShipTo,
						CAST(1 AS BOOLEAN) AS Allowedit
      FROM    CompanyAssociations CA
      LEFT OUTER JOIN DivisionMaster DIV ON DIV.numDivisionID = CA.numAssociateFromDivisionID
      LEFT OUTER JOIN CompanyInfo CMP ON DIV.numCompanyID = CMP.numCompanyId
      LEFT OUTER JOIN AdditionalContactsInformation ACI ON ACI.numContactId = CA.numContactID
      LEFT OUTER JOIN Listdetails LD ON LD.numListItemID = CA.numReferralType
      LEFT OUTER JOIN DivisionMaster DIV1 ON DIV1.numDivisionID = CA.numDivisionID
      LEFT OUTER JOIN CompanyInfo CMP1 ON CMP1.numCompanyId = CA.numCompanyID AND DIV1.numCompanyID = CMP1.numCompanyId
      LEFT OUTER JOIN AdditionalContactsInformation ACIDiv ON ACIDiv.numDivisionId = DIV1.numDivisionID AND coalesce(ACIDiv.bitPrimaryContact,false) = true
      LEFT OUTER JOIN Listdetails LD1 ON LD1.numListItemID = CMP1.numCompanyType
      LEFT OUTER JOIN Listdetails LD2 ON LD2.numListItemID = CMP1.vcProfile
      LEFT JOIN AddressDetails AD2 ON AD2.numDomainID = DIV.numDomainID
      AND AD2.numRecordID = CA.numDivisionID AND AD2.tintAddressOf = 2 AND AD2.tintAddressType = 2 AND AD2.bitIsPrimary = true
      WHERE   CA.numAssociateFromDivisionID = v_numDivID
      AND CA.bitDeleted = false
      union
      SELECT  numAssociationID,
                        CMP.vcCompanyName,
                        CA.numCompanyID,
                        DIV.vcDivisionName,
                        CA.numAssociateFromDivisionID as numDivisionID,
                        (SELECT    vcCompanyName
         FROM      DivisionMaster INNER JOIN CompanyInfo ON DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
         WHERE     DivisionMaster.numDivisionID = CA.numAssociateFromDivisionID) AS TargetCompany,
                        ACI.numContactId,
                        coalesce(ACI.vcFirstName,'') || ' '
      || coalesce(ACI.vcLastname,'') AS ContactName,
                        coalesce(ACI.numPhone,'') || ', '
      || coalesce(ACI.numPhoneExtension,'') AS numPhone,
                        ACI.vcEmail,
                        LD.vcData,
                        LD1.vcData AS OrgProfile,
                        LD2.vcData AS Relationship ,
                        DIV.tintCRMType,
                        CASE 
      WHEN CA.bitChildOrg = true THEN '(Parent)'
      ELSE ''
      END AS bitParentOrg,
                        CASE WHEN bitShareportal = false THEN 'No'
      ELSE 'Yes'
      END AS Shareportal,coalesce(fn_GetState(AD2.numState),'') AS ShipTo,
						CAST(0 AS BOOLEAN) AS Allowedit
      FROM    CompanyAssociations CA
      LEFT OUTER JOIN DivisionMaster DIV ON DIV.numDivisionID = CA.numDivisionID
      LEFT OUTER JOIN CompanyInfo CMP ON DIV.numCompanyID = CMP.numCompanyId
      LEFT OUTER JOIN AdditionalContactsInformation ACI ON ACI.numContactId = CA.numContactID
      LEFT OUTER JOIN Listdetails LD ON LD.numListItemID = CA.numReferralType --numReferralType
      LEFT OUTER JOIN CompanyInfo CMP1 ON CMP1.numCompanyId = CA.numCompanyID
      LEFT OUTER JOIN Listdetails LD1 ON LD1.numListItemID = CMP1.numCompanyType
      LEFT OUTER JOIN Listdetails LD2 ON LD2.numListItemID = CMP1.vcProfile
      LEFT JOIN AddressDetails AD2 ON AD2.numDomainID = DIV.numDomainID
      AND AD2.numRecordID = CA.numAssociateFromDivisionID AND AD2.tintAddressOf = 2 AND AD2.tintAddressType = 2 AND AD2.bitIsPrimary = true
      WHERE   CA.numDivisionID = v_numDivID
      AND CA.bitDeleted = false
      ORDER BY TargetCompany;

      open SWV_RefCur2 for
      SELECT  CompanyInfo.vcCompanyName,
						DIV.numDivisionID,
                        LD.vcData AS OrgProfile,
                        LD1.vcData AS Relationship,
                        coalesce(ACI.vcFirstName,'') || ' '
      || coalesce(ACI.vcLastname,'') AS ContactName,
                        coalesce(ACI.numPhone,'') || ', '
      || coalesce(ACI.numPhoneExtension,'') AS numPhone,
                        ACI.vcEmail,
                        ACI.numContactId,coalesce(fn_GetState(AD2.numState),'') AS ShipTo
      FROM    DivisionMaster AS DIV
      LEFT OUTER JOIN CompanyInfo ON DIV.numCompanyID = CompanyInfo.numCompanyId
      LEFT OUTER JOIN Listdetails AS LD ON CompanyInfo.vcProfile = LD.numListItemID
      LEFT OUTER JOIN Listdetails AS LD1 ON CompanyInfo.numCompanyType = LD1.numListItemID
      LEFT OUTER JOIN AdditionalContactsInformation AS ACI ON DIV.numDivisionID = ACI.numDivisionId
      LEFT JOIN AddressDetails AD2 ON AD2.numDomainID = DIV.numDomainID
      AND AD2.numRecordID = DIV.numDivisionID AND AD2.tintAddressOf = 2 AND AD2.tintAddressType = 2 AND AD2.bitIsPrimary = true
      WHERE   DIV.numDivisionID = v_numDivID
      AND ACI.bitPrimaryContact = true;
   ELSE
      open SWV_RefCur for
      SELECT  numAssociationID,
                        CMP.vcCompanyName AS Company,
                        LD.vcData
      FROM
      CompanyAssociations CA
      LEFT OUTER JOIN DivisionMaster DIV ON DIV.numDivisionID = CA.numDivisionID
      LEFT OUTER JOIN CompanyInfo CMP ON DIV.numCompanyID = CMP.numCompanyId
      LEFT OUTER JOIN Listdetails LD ON LD.numListItemID = CA.numReferralType --numReferralType
      WHERE   CA.bitDeleted = false
      AND CA.numDivisionID = v_numDivID
      AND DIV.numDivisionID = CA.numAssociateFromDivisionID
      AND CMP.numCompanyId = DIV.numCompanyID
      AND LD.numListItemID = CA.numReferralType
      GROUP BY numAssociationID,CMP.vcCompanyName,DIV.vcDivisionName,LD.vcData
      ORDER BY vcCompanyName,vcDivisionName;
   end if;
   RETURN;
END; $$;



