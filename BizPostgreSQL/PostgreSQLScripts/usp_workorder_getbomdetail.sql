-- Stored procedure definition script USP_WorkOrder_GetBOMDetail for PostgreSQL
CREATE OR REPLACE FUNCTION USP_WorkOrder_GetBOMDetail(v_numDomainID NUMERIC(18,0)                            
	,v_numWOID NUMERIC(18,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_tintCommitAllocation  SMALLINT;
BEGIN
   select   coalesce(tintCommitAllocation,1) INTO v_tintCommitAllocation FROM Domain WHERE numDomainId = v_numDomainID;

   DROP TABLE IF EXISTS tt_TEMPGetBOMDetail CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPGetBOMDetail
   (
      numParentID NUMERIC(18,0),
      ID NUMERIC(18,0),
      numOppID NUMERIC(18,0),
      numItemCode NUMERIC(18,0),
      vcItemName VARCHAR(500),
      numRequiredQty DOUBLE PRECISION,
      numWarehouseItemID NUMERIC(18,0),
      vcWareHouse VARCHAR(200),
      numOnHand DOUBLE PRECISION,
      numAvailable DOUBLE PRECISION,
      numOnOrder DOUBLE PRECISION,
      numAllocation DOUBLE PRECISION,
      numBackOrder DOUBLE PRECISION,
      monCost DECIMAL(20,5),
      bitWorkOrder BOOLEAN,
      bitReadyToBuild BOOLEAN
   );

   INSERT INTO tt_TEMPGetBOMDetail(numParentID
		,ID
		,numItemCode
		,vcItemName
		,numRequiredQty
		,numWarehouseItemID
		,vcWareHouse
		,numOnHand
		,numAvailable
		,numOnOrder
		,numAllocation
		,numBackOrder
		,monCost
		,bitWorkOrder
		,bitReadyToBuild) with recursive CTEWorkOrder(numParentWOID,numWOId,numOppId,numWODetailId,numItemCode,vcItemName,numQtyItemsReq,
   numWarehouseItemID,monCost,bitReadyToBuild) AS(SELECT
   CAST(0 AS NUMERIC) AS numParentWOID,
			WOD.numWOId AS numWOId,
			WorkOrder.numOppId AS numOppId,
			WOD.numWODetailId AS numWODetailId,
			Item.numItemCode AS numItemCode,
			CONCAT(Item.vcItemName,(CASE WHEN coalesce(Item.vcSKU,'') <> '' THEN CONCAT(' (',Item.vcSKU,')') ELSE '' END)) AS vcItemName,
			WOD.numQtyItemsReq AS numQtyItemsReq,
			WOD.numWarehouseItemID AS numWarehouseItemID,
			coalesce(WOD.monAverageCost,coalesce(Item.monAverageCost,0))*coalesce(WOD.numQtyItemsReq,0) AS monCost,
			(CASE WHEN WorkOrder.numWOStatus = 23184 THEN true ELSE false END) AS bitReadyToBuild
   FROM
   WorkOrder
   INNER JOIN
   WorkOrderDetails WOD
   ON
   WorkOrder.numWOId = WOD.numWOId
   INNER JOIN
   Item
   ON
   WOD.numChildItemID = Item.numItemCode
   WHERE
   WorkOrder.numDomainId = v_numDomainID
   AND WorkOrder.numWOId = v_numWOID
   UNION ALL
   SELECT
   CTEWorkOrder.numWODetailId AS numParentWOID
			,WorkOrder.numWOId AS numWOId
			,WorkOrder.numOppId AS numOppId
			,WorkOrderDetails.numWODetailId AS numWODetailId
			,Item.numItemCode AS numItemCode,
			CONCAT(Item.vcItemName,(CASE WHEN coalesce(Item.vcSKU,'') <> '' THEN CONCAT(' (',Item.vcSKU,')') ELSE '' END)) AS vcItemName,
			WorkOrderDetails.numQtyItemsReq AS numQtyItemsReq,
			WorkOrderDetails.numWarehouseItemID AS numWarehouseItemID,
			coalesce(WorkOrderDetails.monAverageCost,coalesce(Item.monAverageCost,0))*coalesce(WorkOrderDetails.numQtyItemsReq,0) AS monCost,
			CAST((CASE WHEN WorkOrder.numWOStatus = 23184 THEN true ELSE false END) AS BOOLEAN) AS bitReadyToBuild
   FROM
   WorkOrder
   INNER JOIN
   CTEWorkOrder
   ON
   WorkOrder.numParentWOID = CTEWorkOrder.numWOID
   AND WorkOrder.numItemCode = CTEWorkOrder.numItemCode
   INNER JOIN
   WorkOrderDetails
   ON
   WorkOrder.numWOId = WorkOrderDetails.numWOId
   INNER JOIN
   Item
   ON
   WorkOrderDetails.numChildItemID = Item.numItemCode) SELECT
   CTEWorkOrder.numParentWOID
		,CTEWorkOrder.numWODetailId
		,numItemCode
		,vcItemName
		,numQtyItemsReq
		,CTEWorkOrder.numWarehouseItemID
		,Warehouses.vcWareHouse
		,coalesce(WareHouseItems.numOnHand,0)+coalesce(WareHouseItems.numAllocation,0)
		,WareHouseItems.numOnHand
		,WareHouseItems.numonOrder
		,WareHouseItems.numAllocation
		,WareHouseItems.numBackOrder
		,coalesce(CTEWorkOrder.monCost,0)
		,CAST((CASE WHEN(SELECT COUNT(*) FROM WorkOrder WOInner WHERE WOInner.numParentWOID = CTEWorkOrder.numWOId AND WOInner.numItemCode = CTEWorkOrder.numItemCode) > 0 THEN 1 ELSE 0 END) AS BOOLEAN)
		,CAST(CASE
   WHEN coalesce(WareHouseItems.numWareHouseItemID,0) > 0 AND coalesce(CTEWorkOrder.bitReadyToBuild,false) = false
   THEN
      CASE
      WHEN(CASE WHEN coalesce(CTEWorkOrder.numOppId,0) > 0 THEN 1 ELSE 0 END) = 1 AND v_tintCommitAllocation = 2
      THEN(CASE WHEN coalesce(numQtyItemsReq,0) >(coalesce(WareHouseItems.numOnHand,0)+coalesce((SELECT
            SUM(coalesce(WIInner.numOnHand,0)+coalesce(WIInner.numAllocation,0))
            FROM
            WareHouseItems WIInner
            WHERE
            WIInner.numItemID = WareHouseItems.numItemID
            AND WIInner.numWareHouseID = WareHouseItems.numWareHouseID
            AND WIInner.numWareHouseItemID <> WareHouseItems.numWareHouseItemID),0)) THEN 0 ELSE 1 END)
      ELSE(CASE WHEN coalesce(numQtyItemsReq,0) >(coalesce(WareHouseItems.numAllocation,0)+coalesce((SELECT
            SUM(coalesce(WIInner.numOnHand,0)+coalesce(WIInner.numAllocation,0))
            FROM
            WareHouseItems WIInner
            WHERE
            WIInner.numItemID = WareHouseItems.numItemID
            AND WIInner.numWareHouseID = WareHouseItems.numWareHouseID
            AND WIInner.numWareHouseItemID <> WareHouseItems.numWareHouseItemID),
         0)) THEN 0 ELSE 1 END)
      END
   ELSE 1
   END AS BOOLEAN)
   FROM
   CTEWorkOrder
   LEFT JOIN
   WareHouseItems
   ON
   CTEWorkOrder.numWarehouseItemID = WareHouseItems.numWareHouseItemID
   LEFT JOIN
   Warehouses
   ON
   WareHouseItems.numWareHouseID = Warehouses.numWareHouseID;

	
open SWV_RefCur for SELECT numParentID AS "numParentID"
		,ID AS "ID"
		,numItemCode AS "numItemCode"
		,vcItemName AS "vcItemName"
		,numRequiredQty AS "numRequiredQty"
		,numWarehouseItemID AS "numWarehouseItemID"
		,vcWareHouse AS "vcWareHouse"
		,numOnHand AS "numOnHand"
		,numAvailable AS "numAvailable"
		,numOnOrder AS "numOnOrder"
		,numAllocation AS "numAllocation"
		,numBackOrder AS "numBackOrder"
		,monCost AS "monCost"
		,bitWorkOrder AS "bitWorkOrder"
		,bitReadyToBuild AS "bitReadyToBuild" FROM tt_TEMPGetBOMDetail;
END; $$;












