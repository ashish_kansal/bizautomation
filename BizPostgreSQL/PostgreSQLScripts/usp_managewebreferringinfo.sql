-- Stored procedure definition script USP_ManageWebReferringInfo for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ManageWebReferringInfo(v_numDomainID NUMERIC(9,0) DEFAULT 0,
	v_searchText VARCHAR(500) DEFAULT NULL,
	v_numListItemID NUMERIC(9,0) DEFAULT 0)
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_LeadSource  VARCHAR(100);
	--@numDivisionID AS NUMERIC(18,0), @LeadSource AS VARCHAR(100)

	--SELECT @numDivisionID = numDivisionID
	--FROM  CompanyInfo CMP    
	--	JOIN DivisionMaster DM ON DM.numCompanyID = CMP.numCompanyID   
	--WHERE vcHow = @numListItemID AND DM.numDomainID = @numDomainID    

BEGIN
   select   vcData INTO v_LeadSource FROM Listdetails WHERE numListItemID = v_numListItemID AND numDomainid = v_numDomainID;   
	
	--print 'SELECT * FROM TrackingVisitorsHDR 
	--			WHERE numDivisionID = @numDivisionID 
	--				AND numDomainID = @numDomainID 
	--				AND vcOrginalRef LIKE ''--' + @searchText + '--'''

   IF EXISTS(SELECT * FROM TrackingVisitorsHDR
   WHERE numListItemID = v_numListItemID --numDivisionID = @numDivisionID 
   AND numDomainID = v_numDomainID
   AND vcOrginalRef  ilike '%' || coalesce(v_searchText,'') || '%') then
	
      RAISE EXCEPTION 'DUPLICATE_REFERRER';
      RETURN;
   ELSE
		--IF @numDivisionID IS NULL
		--BEGIN
 	--		RAISERROR ( 'DIVISION_NULL',16, 1 )
 	--		RETURN ;
		--END
      INSERT INTO TrackingVisitorsHDR(vcOrginalRef,numListItemID,numDomainID,vcReferrer)
		VALUES(v_searchText, v_numListItemID, v_numDomainID,v_LeadSource);
   end if;

	/*DECLARE @Text AS VARCHAR(500)

	SELECT @Text = vcOrginalRef 
	FROM TrackingVisitorsHDR 
	WHERE numDivisionID = @numDivisionID AND numDomainID = @numDomainID */

END; $$;


