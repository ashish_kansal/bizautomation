-- Stored procedure definition script USP_ManageStageTask for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ManageStageTask(v_numDomainID NUMERIC(9,0) DEFAULT 0,    
v_numStageDetailsId NUMERIC(18,0) DEFAULT 0,           
v_vcTaskName VARCHAR(500) DEFAULT '',           
v_numHours NUMERIC(9,0) DEFAULT 0,           
v_numMinutes NUMERIC(9,0) DEFAULT 0,           
v_numAssignTo NUMERIC(18,0) DEFAULT 0,           
v_numCreatedBy NUMERIC(18,0) DEFAULT 0,
v_numOppId NUMERIC(18,0) DEFAULT 0,   
v_numProjectId NUMERIC(18,0) DEFAULT 0,   
v_numParentTaskId NUMERIC(18,0) DEFAULT 0,
v_bitTaskClosed BOOLEAN DEFAULT false,
v_numTaskId NUMERIC DEFAULT 0,
v_bitSavedTask BOOLEAN DEFAULT false,
v_bitAutoClosedTaskConfirmed BOOLEAN DEFAULT false,
v_intTaskType INTEGER DEFAULT 0,
v_numWorkOrderID NUMERIC(18,0) DEFAULT 0)
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_bitDefaultTask  BOOLEAN DEFAULT false;
   v_vcRunningTaskName  VARCHAR(500) DEFAULT 0;
   v_numTaskValidatorId  NUMERIC(18,0) DEFAULT 0;
   v_numTaskSlNo  NUMERIC(18,0) DEFAULT 0;
   v_numStageOrder  NUMERIC(18,0) DEFAULT 0;
   v_numStagePercentageId  NUMERIC(18,0) DEFAULT 0;
   v_bitRunningDynamicMode  BOOLEAN DEFAULT false;
   v_recordCount  INTEGER DEFAULT 1;
   v_numReferenceTaskId  NUMERIC DEFAULT 0;
   v_numReferenceStageDetailsId  NUMERIC DEFAULT 0;
   v_intTotalProgress  INTEGER DEFAULT 0;
   v_intTotalTaskCount  INTEGER DEFAULT 0;
   v_intTotalTaskClosed  INTEGER DEFAULT 0;
BEGIN
   IF(v_numTaskId = 0) then
	
      IF(v_numOppId > 0 OR v_numProjectId > 0 OR v_numWorkOrderID > 0) then
		
         v_bitDefaultTask := false;
         v_bitSavedTask := true;
      end if;
      v_bitTaskClosed := false;
      INSERT INTO StagePercentageDetailsTask(numStageDetailsId,
			vcTaskName,
			numHours,
			numMinutes,
			numAssignTo,
			numDomainID,
			numCreatedBy,
			dtmCreatedOn,
			numOppId,
			numProjectId,
			numParentTaskId,
			bitDefaultTask,
			bitTaskClosed,
			bitSavedTask,
			intTaskType,
			numWorkOrderId)
		VALUES(v_numStageDetailsId,
			v_vcTaskName,
			v_numHours,
			v_numMinutes,
			v_numAssignTo,
			v_numDomainID,
			v_numCreatedBy,
			LOCALTIMESTAMP ,
			v_numOppId,
			v_numProjectId,
			v_numParentTaskId,
			v_bitDefaultTask,
			v_bitTaskClosed,
			v_bitSavedTask,
			v_intTaskType,
			v_numWorkOrderID);
		
      UPDATE
      StagePercentageDetails
      SET
      intTaskType = v_intTaskType
      WHERE
      numStageDetailsId = v_numStageDetailsId;
   ELSE
      select   numStageDetailsId, vcTaskName, bitDefaultTask, numOrder INTO v_numStageDetailsId,v_vcRunningTaskName,v_bitDefaultTask,v_numTaskSlNo FROM StagePercentageDetailsTask WHERE numTaskId = v_numTaskId    LIMIT 1;
      select   numTaskValidatorId INTO v_numTaskValidatorId FROM Sales_process_List_Master WHERE Slp_Id =(SELECT  slp_id FROM StagePercentageDetails WHERE numStageDetailsId = v_numStageDetailsId LIMIT 1);
      select   bitRunningDynamicMode, numStageOrder, numStagePercentageId INTO v_bitRunningDynamicMode,v_numStageOrder,v_numStagePercentageId FROM
      StagePercentageDetails WHERE
      numStageDetailsId = v_numStageDetailsId    LIMIT 1;
      RAISE NOTICE '%',v_bitRunningDynamicMode;
      IF(v_bitRunningDynamicMode = true AND (v_numOppId > 0 OR v_numProjectId > 0 OR v_numWorkOrderID > 0)) then
         IF(v_bitDefaultTask = true) then
			
            SELECT COUNT(*) INTO v_recordCount FROM StagePercentageDetailsTask WHERE numParentTaskId = v_numTaskId AND numStageDetailsId = v_numStageDetailsId;
            UPDATE
            StagePercentageDetailsTask
            SET
            bitSavedTask = false
            WHERE
            bitDefaultTask = true AND numStageDetailsId = v_numStageDetailsId;
         end if;
         IF(v_recordCount = 0) then
            DELETE FROM
            StagePercentageDetailsTask
            WHERE
            numParentTaskId IN(SELECT numTaskId FROM StagePercentageDetailsTask WHERE
            bitDefaultTask = true AND numStageDetailsId = v_numStageDetailsId);
            select   numReferenceTaskId INTO v_numReferenceTaskId FROM StagePercentageDetailsTask WHERE numTaskId = v_numTaskId    LIMIT 1;
            select   numStageDetailsId INTO v_numReferenceStageDetailsId FROM StagePercentageDetailsTask WHERE numTaskId = v_numReferenceTaskId    LIMIT 1;
            INSERT INTO StagePercentageDetailsTask(numStageDetailsId,
				vcTaskName,
				numHours,
				numMinutes,
				numAssignTo,
				numDomainID,
				numCreatedBy,
				dtmCreatedOn,
				numOppId,
				numProjectId,
				numParentTaskId,
				bitDefaultTask,
				bitTaskClosed,
				bitSavedTask,
				numReferenceTaskId,
				numWorkOrderId)
            SELECT
            v_numStageDetailsId,
				vcTaskName,
				numHours,
				numMinutes,
				numAssignTo,
				v_numDomainID,
				v_numCreatedBy,
				LOCALTIMESTAMP,
				v_numOppId,
				v_numProjectId,
				v_numTaskId,
				false,
				false,
				true,
				numTaskId,
				v_numWorkOrderID
            FROM
            StagePercentageDetailsTask AS ST
            WHERE
            ST.numTaskId IN(SELECT numSecondaryListItemID FROM FieldRelationshipDTL WHERE
               numPrimaryListItemID = v_numReferenceTaskId AND
               numFieldRelID =(SELECT  numFieldRelID FROM FieldRelationship WHERE numPrimaryListID = v_numReferenceStageDetailsId AND coalesce(bitTaskRelation,false) = true LIMIT 1)) AND ST.numOppId = 0 AND ST.numProjectId = 0 AND ST.numDomainID = v_numDomainID;
         end if;
      end if;
      IF((v_bitAutoClosedTaskConfirmed = true AND (v_numOppId > 0 OR v_numProjectId > 0 OR v_numWorkOrderID > 0) AND v_numTaskValidatorId = 2 AND v_bitTaskClosed = true) OR ((v_numOppId > 0 OR v_numProjectId > 0 OR v_numWorkOrderID > 0) AND v_numTaskValidatorId = 1 AND v_bitTaskClosed = true)) then
		
         IF(coalesce(v_numTaskSlNo,0) > 0) then
			
            UPDATE
            StagePercentageDetailsTask
            SET
            bitTaskClosed = true,numCreatedBy = v_numCreatedBy,dtmUpdatedOn = LOCALTIMESTAMP
            WHERE
            numStageDetailsId = v_numStageDetailsId AND numOrder < v_numTaskSlNo;
         ELSE
            UPDATE
            StagePercentageDetailsTask
            SET
            bitTaskClosed = true,numCreatedBy = v_numCreatedBy,dtmUpdatedOn = LOCALTIMESTAMP
            WHERE
            numStageDetailsId = v_numStageDetailsId AND numTaskId < v_numTaskId;
         end if;
      end if;
      IF(((v_numOppId > 0 OR v_numProjectId > 0 OR v_numWorkOrderID > 0) AND v_numTaskValidatorId = 3 AND v_bitTaskClosed = true) OR (v_bitAutoClosedTaskConfirmed = true AND (v_numOppId > 0 OR v_numProjectId > 0 OR v_numWorkOrderID > 0) AND v_numTaskValidatorId = 4 AND v_bitTaskClosed = true)) then
		
         IF(coalesce(v_numTaskSlNo,0) > 0) then
			
            UPDATE
            StagePercentageDetailsTask
            SET
            bitTaskClosed = true,numCreatedBy = v_numCreatedBy,dtmUpdatedOn = LOCALTIMESTAMP
            WHERE
            numStageDetailsId = v_numStageDetailsId AND numOrder < v_numTaskSlNo;
         ELSE
            UPDATE
            StagePercentageDetailsTask
            SET
            bitTaskClosed = true,numCreatedBy = v_numCreatedBy,dtmUpdatedOn = LOCALTIMESTAMP
            WHERE
            numStageDetailsId = v_numStageDetailsId AND numTaskId < v_numTaskId;
         end if;
         IF(coalesce(v_numStageOrder,0) > 0) then
			
            UPDATE
            StagePercentageDetailsTask
            SET
            bitTaskClosed = true,numCreatedBy = v_numCreatedBy,dtmUpdatedOn = LOCALTIMESTAMP
            WHERE
            numOppId = v_numOppId AND
            numProjectId = v_numProjectId AND
            coalesce(numWorkOrderId,0) = v_numWorkOrderID AND
            numStageDetailsId IN(SELECT numStageDetailsId FROM StagePercentageDetails WHERE numStageOrder < v_numStageOrder AND numStagePercentageId = v_numStagePercentageId AND (numOppid = v_numOppId AND numProjectid = v_numProjectId AND coalesce(numWorkOrderId,0) = v_numWorkOrderID));
         ELSE
            UPDATE
            StagePercentageDetailsTask
            SET
            bitTaskClosed = true,numCreatedBy = v_numCreatedBy,dtmUpdatedOn = LOCALTIMESTAMP
            WHERE
            numOppId = v_numOppId AND
            numProjectId = v_numProjectId AND
            coalesce(numWorkOrderId,0) = v_numWorkOrderID AND
            numStageDetailsId IN(SELECT numStageDetailsId FROM StagePercentageDetails WHERE numStageDetailsId < v_numStageDetailsId AND numStagePercentageId = v_numStagePercentageId AND numOppid = v_numOppId AND numProjectid = v_numProjectId AND coalesce(numWorkOrderId,0) = v_numWorkOrderID);
         end if;
         UPDATE
         StagePercentageDetailsTask
         SET
         bitTaskClosed = true,numCreatedBy = v_numCreatedBy,dtmUpdatedOn = LOCALTIMESTAMP
         WHERE
         numOppId = v_numOppId AND
         numProjectId = v_numProjectId AND
         coalesce(numWorkOrderId,0) = v_numWorkOrderID AND
         numStageDetailsId IN(SELECT numStageDetailsId FROM StagePercentageDetails WHERE numStagePercentageId < v_numStagePercentageId AND numOppid = v_numOppId AND numProjectid = v_numProjectId AND coalesce(numWorkOrderId,0) = v_numWorkOrderID);
      end if;
      UPDATE
      StagePercentageDetailsTask
      SET
      numAssignTo = v_numAssignTo
      WHERE
      numTaskId = v_numTaskId;
      IF(v_numHours > 0) then
		
         UPDATE
         StagePercentageDetailsTask
         SET
         numHours = v_numHours
         WHERE
         numTaskId = v_numTaskId;
      end if;
      IF(v_numMinutes > 0) then
		
         UPDATE
         StagePercentageDetailsTask
         SET
         numMinutes = v_numMinutes
         WHERE
         numTaskId = v_numTaskId;
      end if;
      IF(LENGTH(v_vcTaskName) > 0) then
		
         UPDATE
         StagePercentageDetailsTask
         SET
         vcTaskName = v_vcTaskName
         WHERE
         numTaskId = v_numTaskId;
      end if;
      IF(v_intTaskType > 0) then
		
         UPDATE
         StagePercentageDetails
         SET
         intTaskType = v_intTaskType
         WHERE
         numStageDetailsId =(SELECT  numStageDetailsId FROM StagePercentageDetailsTask WHERE numTaskId = v_numTaskId LIMIT 1);
      end if;
      UPDATE
      StagePercentageDetailsTask
      SET
      bitSavedTask = v_bitSavedTask,bitTaskClosed = v_bitTaskClosed,numCreatedBy = v_numCreatedBy,
      dtmUpdatedOn = LOCALTIMESTAMP
      WHERE
      numTaskId = v_numTaskId;
   end if;
   IF(v_numOppId > 0 OR v_numProjectId > 0 OR v_numWorkOrderID > 0) then
      SELECT COUNT(*) INTO v_intTotalTaskCount FROM StagePercentageDetailsTask WHERE (numOppId = v_numOppId AND numProjectId = v_numProjectId AND coalesce(numWorkOrderId,0) = v_numWorkOrderID) AND bitSavedTask = true;
      SELECT COUNT(*) INTO v_intTotalTaskClosed FROM StagePercentageDetailsTask WHERE (numOppId = v_numOppId AND numProjectId = v_numProjectId AND coalesce(numWorkOrderId,0) = v_numWorkOrderID) AND bitSavedTask = true AND bitTaskClosed = true;
      IF(v_intTotalTaskCount > 0 AND v_intTotalTaskClosed > 0) then
		
         v_intTotalProgress := ROUND(CAST((CAST((v_intTotalTaskClosed::bigint*100) AS decimal)/v_intTotalTaskCount::bigint) AS NUMERIC),0);
      end if;
      IF((SELECT COUNT(*) FROM ProjectProgress WHERE (coalesce(numOppId,0) = v_numOppId AND coalesce(numProId,0) = v_numProjectId AND coalesce(numWorkOrderId,0) = v_numWorkOrderID) AND numDomainID = v_numDomainID) > 0) then
		
         UPDATE ProjectProgress SET intTotalProgress = v_intTotalProgress WHERE (coalesce(numOppId,0) = v_numOppId AND coalesce(numProId,0) = v_numProjectId AND coalesce(numWorkOrderId,0) = v_numWorkOrderID) AND numDomainID = v_numDomainID;
      ELSE
         IF(v_numOppId = 0) then
			
            v_numOppId := NULL;
         end if;
         IF(v_numProjectId = 0) then
			
            v_numProjectId := NULL;
         end if;
         IF (v_numWorkOrderID = 0) then
			
            v_numWorkOrderID := NULL;
         end if;
         INSERT INTO ProjectProgress(numOppId,
				numProId,
				numWorkOrderId,
				numDomainID,
				intTotalProgress)VALUES(v_numOppId,
				v_numProjectId,
				v_numWorkOrderID,
				v_numDomainID,
				v_intTotalProgress);
      end if;
   end if;
   RETURN;
END; $$; 
/****** Object:  StoredProcedure [dbo].[USP_AssCntCase]    Script Date: 07/26/2008 16:14:53 ******/



