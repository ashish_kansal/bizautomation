-- Stored procedure definition script USP_GetContactsListFromDomain for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetContactsListFromDomain(v_numDomainId NUMERIC(18,0)
	,v_tintGroupType SMALLINT DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT
   numContactId
		,numUserID
		,coalesce(vcFirstName,'') || ' ' || coalesce(vcLastname,'') as Name
		,coalesce(vcPassword,'') as vcPassword
		,CASE WHEN vcEmailID IS NULL THEN coalesce(vcEmail,'') ELSE coalesce(vcEmailID,'') END as EmailID
		,bitActivateFlag
		,coalesce(AGM.vcGroupName,'') AS vcGroupName
		,(CASE WHEN tintGroupType = 4 THEN 'Limited Access User' ELSE 'Full User' END) AS vcGroupType
   FROM
   AdditionalContactsInformation A
   INNER JOIN
   Domain D
   ON
   D.numDomainId = A.numDomainID
   LEFT JOIN
   UserMaster U
   ON
   U.numUserDetailId = A.numContactId
   LEFT JOIN
   AuthenticationGroupMaster AGM
   ON
   AGM.numDomainID = U.numDomainID AND AGM.numGroupID = U.numGroupID
   WHERE
   A.numDivisionId = D.numDivisionId
   AND D.numDomainId = v_numDomainId
   AND (coalesce(v_tintGroupType,0) = 0 OR tintGroupType = v_tintGroupType);
END; $$;












