-- Stored procedure definition script USP_OPPContactInfo for PostgreSQL
CREATE OR REPLACE FUNCTION USP_OPPContactInfo(v_numOppId NUMERIC(9,0) DEFAULT 0,
v_numDomainId NUMERIC(9,0) DEFAULT NULL,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT  D.numDivisionID,a.numContactId,vcCompanyName || ', ' || coalesce(Lst.vcData,'-') as Company,
  a.vcFirstName || ' ' || a.vcLastname as Name,
  case when a.numPhone <> '' then a.numPhone || case when a.numPhoneExtension <> '' then ' - ' || a.numPhoneExtension else '' end  else '' end as Phone,
 a.vcEmail as Email,
  b.vcData as ContactRole,
  opp.numRole as ContRoleId,
  coalesce(bitPartner,false) as bitPartner,
  a.numContactType,CASE WHEN opp.bitSubscribedEmailAlert = true THEN 'Yes' ELSE 'No' END AS SubscribedEmailAlert FROM OpportunityContact opp
   join AdditionalContactsInformation a on
   a.numContactId = opp.numContactID
   join DivisionMaster D
   on a.numDivisionId = D.numDivisionID
   join CompanyInfo C
   on D.numCompanyID = C.numCompanyId
   left join Listdetails b
   on b.numListItemID = opp.numRole
   left join Listdetails Lst
   on Lst.numListItemID = C.numCompanyType
   WHERE opp.numOppId = v_numOppId and a.numDomainID = v_numDomainId;
END; $$;












