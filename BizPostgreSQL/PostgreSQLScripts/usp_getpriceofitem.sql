DROP FUNCTION IF EXISTS USP_GetPriceOfItem;

CREATE OR REPLACE FUNCTION USP_GetPriceOfItem(v_numItemID NUMERIC(18,0) DEFAULT 0,
v_intQuantity INTEGER DEFAULT 0,
v_numDomainID NUMERIC(18,0) DEFAULT 0,
v_numDivisionID NUMERIC(18,0) DEFAULT 0,
v_numWarehouseID NUMERIC(18,0) DEFAULT 0,
v_numWarehouseItemID NUMERIC(18,0) DEFAULT 0,
v_vcSelectedKitChildItems TEXT DEFAULT '',
v_numSiteID NUMERIC(18,0) DEFAULT 0
,INOUT SWV_RefCur refcursor DEFAULT NULL
,INOUT SWV_RefCur2 refcursor DEFAULT NULL
,INOUT SWV_RefCur3 refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE 
		Ref1 refcursor;
		Ref2 refcursor;
		Ref3 refcursor;
BEGIN
   IF coalesce(v_numWarehouseItemID,0) = 0 then
	
      select   numWareHouseItemID INTO v_numWarehouseItemID FROM WareHouseItems WHERE numItemID = v_numItemID AND numWareHouseID = v_numWarehouseID    LIMIT 1;
   end if;

   SELECT * INTO SWV_RefCur,SWV_RefCur2,SWV_RefCur3 FROM USP_ItemPricingRecomm(v_numItemID:: NUMERIC,v_intQuantity::NUMERIC,
   0:: NUMERIC,v_numDivisionID:: NUMERIC,
   v_numDomainID:: NUMERIC,1:: SMALLINT,0.0:: DECIMAL(20,5),
   v_numWarehouseItemID:: NUMERIC,
   2:: SMALLINT,v_vcSelectedKitChildItems:: TEXT,
   0:: NUMERIC, v_numSiteID::NUMERIC,Ref1,Ref2,Ref3);

   RETURN;
END; $$;


