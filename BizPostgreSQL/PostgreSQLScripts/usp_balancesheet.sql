-- Stored procedure definition script USP_BalanceSheet for PostgreSQL
CREATE OR REPLACE FUNCTION USP_BalanceSheet(v_numAcntType NUMERIC(9,0) DEFAULT 0,                        
 v_numDomainId NUMERIC(9,0) DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numParntAcntId  NUMERIC(9,0);
BEGIN
   Select numAccountId INTO v_numParntAcntId From Chart_Of_Accounts Where numParntAcntId is null and numAcntType is null and numDomainId = v_numDomainId; --and numAccountId = 1       
       
                                                                                                                                    
   open SWV_RefCur for Select CA.numAccountId as numAccountId,cast(coalesce(bitOpeningBalanceEquity,false) as BOOLEAN) as bitOpeningBalanceEquity
   from Chart_Of_Accounts CA  Left outer join Listdetails LD on CA.numAcntType = LD.numListItemID
   Where CA.numDomainId = v_numDomainId And CA.numAcntType = v_numAcntType
   And CA.numAccountId <> v_numParntAcntId And CA.numParntAcntId = v_numParntAcntId;        
  --And CA.numOpeningBal is not null         
END; $$;













