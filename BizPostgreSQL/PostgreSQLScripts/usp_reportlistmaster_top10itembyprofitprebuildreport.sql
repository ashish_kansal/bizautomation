-- Stored procedure definition script USP_ReportListMaster_Top10ItemByProfitPreBuildReport for PostgreSQL
CREATE OR REPLACE FUNCTION USP_ReportListMaster_Top10ItemByProfitPreBuildReport(v_numDomainID NUMERIC(18,0)
	--,@ClientTimeZoneOffset INT
	,v_vcTimeLine VARCHAR(50)
	,v_vcGroupBy VARCHAR(50)
	,v_vcTeritorry TEXT
	,v_vcFilterBy SMALLINT  -- 1:Assign TO, 2: Record Owner, 3: Teams (Based on Assigned To) 
	,v_vcFilterValue TEXT
	,v_dtFromDate DATE
	,v_dtToDate DATE
	,v_numRecordCount INTEGER
	,v_tintControlField INTEGER,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numShippingItemID  NUMERIC(18,0);
   v_numDiscountItemID  NUMERIC(18,0);
BEGIN
   select   numShippingServiceItemID, numDiscountServiceItemID INTO v_numShippingItemID,v_numDiscountItemID FROM Domain WHERE numDomainId = v_numDomainID;


   open SWV_RefCur for SELECT cast(numItemCode as VARCHAR(255))
		,cast(vcItemName as VARCHAR(255)),
		(CASE WHEN v_tintControlField = 1 THEN SUM(Profit)
   WHEN v_tintControlField = 2 THEN(SUM(Profit)/SUM(monTotAmount))*100
   WHEN v_tintControlField = 3 THEN SUM(monTotAmount)  END) as Profit 	 		
		--,SUM(Profit) Profit
   FROM(SELECT
      cast(I.numItemCode as VARCHAR(255))
			,cast(I.vcItemName as VARCHAR(255))
			,cast(coalesce(monTotAmount,0) as DECIMAL(20,5)) AS monTotAmount
			,coalesce(GetOrderItemProfitAmountOrMargin(v_numDomainID,OM.numOppId,OI.numoppitemtCode,1::SMALLINT),0) AS Profit
      FROM
      OpportunityMaster OM
      INNER JOIN
      OpportunityItems OI
      ON
      OM.numOppId = OI.numOppId
      INNER JOIN
      Item I
      ON
      OI.numItemCode = I.numItemCode
      Left JOIN
      Vendor V
      ON
      V.numVendorID = I.numVendorID
      AND V.numItemCode = I.numItemCode
      WHERE
      OM.numDomainId = v_numDomainID
      AND coalesce(OM.tintopptype,0) = 1
      AND coalesce(OM.tintoppstatus,0) = 1
      AND OM.bintCreatedDate >= TIMEZONE('UTC',now())+INTERVAL '-12 month'
      AND I.numItemCode NOT IN(v_numShippingItemID,v_numDiscountItemID)) TEMP
   GROUP BY
   numItemCode,vcItemName
   HAVING(CASE
   WHEN v_tintControlField = 1 THEN SUM(Profit)
   WHEN v_tintControlField = 2 THEN(SUM(Profit)/SUM(monTotAmount))*100
   WHEN v_tintControlField = 3 THEN SUM(monTotAmount)
   END) > 0
   ORDER BY(CASE
   WHEN v_tintControlField = 1 THEN SUM(Profit)
   WHEN v_tintControlField = 2 THEN(SUM(Profit)/SUM(monTotAmount))*100
   WHEN v_tintControlField = 3 THEN SUM(monTotAmount)
   END) DESC;
END; $$;












