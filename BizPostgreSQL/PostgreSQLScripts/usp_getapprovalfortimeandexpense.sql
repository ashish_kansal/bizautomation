-- FUNCTION: public.usp_getapprovalfortimeandexpense(numeric, timestamp without time zone, timestamp without time zone, numeric, integer, refcursor)

-- DROP FUNCTION public.usp_getapprovalfortimeandexpense(numeric, timestamp without time zone, timestamp without time zone, numeric, integer, refcursor);

CREATE OR REPLACE FUNCTION public.usp_getapprovalfortimeandexpense(
	v_numdomainid numeric DEFAULT 0,
	v_startdate timestamp without time zone DEFAULT NULL::timestamp without time zone,
	v_enddate timestamp without time zone DEFAULT NULL::timestamp without time zone,
	v_numusercntid numeric DEFAULT 0,
	v_clienttimezoneoffset integer DEFAULT NULL::integer,
	INOUT swv_refcur refcursor DEFAULT NULL::refcursor)
    RETURNS refcursor
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
AS $BODY$
BEGIN
-- This function was converted on Sat Apr 10 15:02:02 2021 using Ispirer SQLWays 9.0 Build 6532 64bit Licensed to BizAutomation - Sandeep Patel - India - Ispirer MnMTK 2020 MSSQLServer to PostgreSQL Database Migration Professional Project License (50 GB, 750 Tables, 426500 LOC, 3 Extension Requests, 1 Month, 20210508).
   IF(v_startDate IS NULL) then
	 
      open SWV_RefCur for
      SELECT  CASE WHEN tintTEType = 0 THEN 'T & E'
      WHEN tintTEType = 1
      THEN CASE WHEN numCategory = 1
         THEN CASE WHEN(SELECT  tintopptype
               FROM    OpportunityMaster
               WHERE   numOppId = TE.numOppId) = 1 THEN 'Sales Time'
            ELSE 'Purch Time'
            END
         ELSE CASE WHEN(SELECT  tintopptype
               FROM    OpportunityMaster
               WHERE   numOppId = TE.numOppId) = 1 THEN 'Sales Exp'
            ELSE 'Purch Exp'
            END
         END
      WHEN tintTEType = 2
      THEN CASE WHEN numCategory = 1 THEN 'Project Time'
         ELSE 'Project Exp'
         END
      WHEN tintTEType = 3
      THEN CASE WHEN numCategory = 1 THEN 'Case Time'
         ELSE 'Case Exp'
         END
      WHEN tintTEType = 4
      THEN 'Non-Paid Leave'
      WHEN tintTEType = 5
      THEN 'Expense'
      END AS chrFrom,
                    numCategoryHDRID,
                    tintTEType,
                    CASE WHEN numCategory = 1
      THEN 'Time (' ||(CAST(dtFromDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) AS VARCHAR(30))
         || '-'
         || CAST(dtToDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) AS VARCHAR(30)))
         || ')'
      WHEN numCategory = 2 AND coalesce(bitReimburse,false) = true AND numType = 2  THEN 'Reimbursable Expense'
      WHEN numCategory = 2 AND coalesce(bitReimburse,false) = true AND numType = 6 THEN 'Billable + Reimbursable Expense'
      WHEN numCategory = 2 AND coalesce(bitReimburse,false) = true AND numType = 1 THEN 'Billable Expense'
                         --WHEN numCategory = 2 AND ISNULL(bitReimburse,0) = 0 AND numType = 2 THEN 'Non-Billable Expense'
                         --WHEN numCategory = 3 THEN 'Paid Leave'
                         --WHEN numCategory = 4 THEN 'Non Paid Leave'
      END AS Category,
                    CASE WHEN numType = 1 THEN 'Billable'
      WHEN numType = 2 THEN 'Non-Billable'
      WHEN numType = 5 THEN 'Reimbursable Expense'
      WHEN numType = 6 THEN 'Billable'
                         --WHEN numType = 3 THEN 'Paid Leave'
                         --WHEN numType = 4 THEN 'Non-Paid'
      END AS Type,
                    CASE WHEN numCategory = 1
      THEN dtFromDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval)
      ELSE dtFromDate
      END AS dtFromDate,
                    CASE WHEN numCategory = 1
      THEN dtToDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval)
      ELSE dtToDate
      END AS dtToDate,
                    bitFromFullDay,
                    bitToFullDay,
                    CAST(monAmount AS DECIMAL(10,2)) AS monAmount,
                    TE.numOppId,
                    OM.vcpOppName,
					CA.vcAccountName,
                    TE.numDivisionID,
                    txtDesc,
                    numUserCntID,
                    coalesce(ACI.vcFirstName,'') || ' ' || coalesce(ACI.vcLastname,'') AS vcEmployee,
                    TE.numDomainID,
                    TE.numcontractId,
                    numCaseid,
                    TE.numProId,
                    PM.vcProjectID,
                    numOppBizDocsId,
                    numStageId AS numStageID,
                    numCategory,
                    numtype,
                    CASE WHEN numCategory = 1
      THEN CAST(CAST((EXTRACT(DAY FROM dtToDate -dtFromDate)*60*24+EXTRACT(HOUR FROM dtToDate -dtFromDate)*60+EXTRACT(MINUTE FROM dtToDate -dtFromDate)) AS DOUBLE PRECISION)/60 AS VARCHAR(30))
      WHEN numCategory = 2
      THEN CAST(CAST(monAmount AS DECIMAL(10,2)) AS VARCHAR(30))
      WHEN numCategory = 3
      THEN(CASE WHEN bitFromFullDay = false THEN 'HDL'
         WHEN bitFromFullDay = true THEN 'FDL'
         END)
      WHEN numCategory = 4
      THEN(CASE WHEN bitFromFullDay = false THEN 'HDL'
         WHEN bitFromFullDay = true THEN 'FDL'
         END)
      END AS Detail
                    ,CAST(CAST((EXTRACT(DAY FROM dtToDate -dtFromDate)*60*24+EXTRACT(HOUR FROM dtToDate -dtFromDate)*60+EXTRACT(MINUTE FROM dtToDate -dtFromDate)) AS DOUBLE PRECISION)/60 AS VARCHAR(30)) AS TimeValue
                    ,CASE WHEN numCategory = 1 THEN CAST((CAST((EXTRACT(DAY FROM dtToDate -dtFromDate)*60*24+EXTRACT(HOUR FROM dtToDate -dtFromDate)*60+EXTRACT(MINUTE FROM dtToDate -dtFromDate)) AS DOUBLE PRECISION)/60)*coalesce(TE.monAmount,1) AS DECIMAL(10,2))
      WHEN numCategory = 2 THEN CAST(coalesce(TE.monAmount,1) AS DECIMAL(10,2))
      ELSE 0
      END AS ExpenseValue
                    ,(SELECT coalesce(numItemCode,0) FROM Item  WHERE numItemCode IN(SELECT numItemCode FROM OpportunityItems WHERE numoppitemtCode = TE.numOppItemID)) AS numItemCode
					,(SELECT coalesce(numClassID,0) FROM OpportunityItems WHERE numoppitemtCode = TE.numOppItemID) AS numClassID
					,(SELECT coalesce(numClassID,0) FROM OpportunityItems WHERE numoppitemtCode = TE.numOppItemID) AS numClassID
					,(SELECT coalesce(vcItemDesc,'') FROM OpportunityItems WHERE numoppitemtCode = TE.numOppItemID) AS vcItemDesc
					,CASE WHEN TE.numApprovalComplete = 6 THEN 'Approved' WHEN TE.numApprovalComplete = -1 THEN 'Declined'  WHEN coalesce(TE.numApprovalComplete,0) = 0 THEN '' ELSE 'Pending For Approval' END as ApprovalStatus
					,TE.numApprovalComplete,TE.numServiceItemID,TE.numClassID as numEClassId,TE.numExpId
      FROM    timeandexpense TE
      LEFT JOIN OpportunityMaster OM ON OM.numOppId = TE.numOppId AND TE.numDomainID = OM.numDomainId
      LEFT JOIN ProjectsMaster PM ON PM.numProId = TE.numProId AND TE.numDomainID = PM.numdomainId
      LEFT JOIN Chart_Of_Accounts CA ON CA.numAccountId = TE.numExpId AND TE.numDomainID = CA.numDomainId
      LEFT JOIN AdditionalContactsInformation ACI ON ACI.numContactId = TE.numUserCntID
      LEFT JOIN ApprovalConfig AS A on TE.numUserCntID = A.numUserId AND A.numDomainID = v_numDomainID
      WHERE  (A.numLevel1Authority = v_numUserCntID OR A.numLevel2Authority = v_numUserCntID OR A.numLevel3Authority = v_numUserCntID
      OR A.numLevel4Authority = v_numUserCntID OR A.numLevel5Authority = v_numUserCntID) AND TE.numApprovalComplete NOT IN(0,-1);
   ELSE
      open SWV_RefCur for
      SELECT  CASE WHEN tintTEType = 0 THEN 'T & E'
      WHEN tintTEType = 1
      THEN CASE WHEN numCategory = 1
         THEN CASE WHEN(SELECT  tintopptype
               FROM    OpportunityMaster
               WHERE   numOppId = TE.numOppId) = 1 THEN 'Sales Time'
            ELSE 'Purch Time'
            END
         ELSE CASE WHEN(SELECT  tintopptype
               FROM    OpportunityMaster
               WHERE   numOppId = TE.numOppId) = 1 THEN 'Sales Exp'
            ELSE 'Purch Exp'
            END
         END
      WHEN tintTEType = 2
      THEN CASE WHEN numCategory = 1 THEN 'Project Time'
         ELSE 'Project Exp'
         END
      WHEN tintTEType = 3
      THEN CASE WHEN numCategory = 1 THEN 'Case Time'
         ELSE 'Case Exp'
         END
      WHEN tintTEType = 4
      THEN 'Non-Paid Leave'
      WHEN tintTEType = 5
      THEN 'Expense'
      END AS chrFrom,
                    numCategoryHDRID,
                    tintTEType,
                    CASE WHEN numCategory = 1
      THEN 'Time (' ||(CAST(dtFromDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) AS VARCHAR(30))
         || '-'
         || CAST(dtToDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) AS VARCHAR(30)))
         || ')'
      WHEN numCategory = 2 AND coalesce(bitReimburse,false) = true AND numType = 2  THEN 'Reimbursable Expense'
      WHEN numCategory = 2 AND coalesce(bitReimburse,false) = true AND numType = 6 THEN 'Billable + Reimbursable Expense'
      WHEN numCategory = 2 AND coalesce(bitReimburse,false) = false AND numType = 1 THEN 'Billable Expense'
                         --WHEN numCategory = 2 AND ISNULL(bitReimburse,0) = 0 AND numType = 2 THEN 'Non-Billable Expense'
                         --WHEN numCategory = 3 THEN 'Paid Leave'
                         --WHEN numCategory = 4 THEN 'Non Paid Leave'
      END AS Category,
                    CASE WHEN numType = 1 THEN 'Billable'
      WHEN numType = 2 THEN 'Non-Billable'
      WHEN numType = 5 THEN 'Reimbursable Expense'
      WHEN numType = 6 THEN 'Billable'
                         --WHEN numType = 3 THEN 'Paid Leave'
                         --WHEN numType = 4 THEN 'Non-Paid'
      END AS Type,
                    CASE WHEN numCategory = 1
      THEN dtFromDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval)
      ELSE dtFromDate
      END AS dtFromDate,
                    CASE WHEN numCategory = 1
      THEN dtToDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval)
      ELSE dtToDate
      END AS dtToDate,
                    bitFromFullDay,
                    bitToFullDay,
                    CAST(monAmount AS DECIMAL(10,2)) AS monAmount,
                    TE.numOppId,
                    OM.vcpOppName,
					CA.vcAccountName,
                    TE.numDivisionID,
                    txtDesc,
                    numUserCntID,
                    coalesce(ACI.vcFirstName,'') || ' ' || coalesce(ACI.vcLastname,'') AS vcEmployee,
                    TE.numDomainID,
                    TE.numcontractId,
                    numCaseid,
                    TE.numProId,
                    PM.vcProjectID,
                    numOppBizDocsId,
                    numStageId AS numStageID,
                    numCategory,
                    numtype,
                    CASE WHEN numCategory = 1
      THEN CAST(CAST((EXTRACT(DAY FROM dtToDate -dtFromDate)*60*24+EXTRACT(HOUR FROM dtToDate -dtFromDate)*60+EXTRACT(MINUTE FROM dtToDate -dtFromDate)) AS DOUBLE PRECISION)/60 AS VARCHAR(30))
      WHEN numCategory = 2
      THEN CAST(CAST(monAmount AS DECIMAL(10,2)) AS VARCHAR(30))
      WHEN numCategory = 3
      THEN(CASE WHEN bitFromFullDay = false THEN 'HDL'
         WHEN bitFromFullDay = true THEN 'FDL'
         END)
      WHEN numCategory = 4
      THEN(CASE WHEN bitFromFullDay = false THEN 'HDL'
         WHEN bitFromFullDay = true THEN 'FDL'
         END)
      END AS Detail
                    ,CAST(CAST((EXTRACT(DAY FROM dtToDate -dtFromDate)*60*24+EXTRACT(HOUR FROM dtToDate -dtFromDate)*60+EXTRACT(MINUTE FROM dtToDate -dtFromDate)) AS DOUBLE PRECISION)/60 AS VARCHAR(30)) AS TimeValue
                    ,CASE WHEN numCategory = 1 THEN CAST((CAST((EXTRACT(DAY FROM dtToDate -dtFromDate)*60*24+EXTRACT(HOUR FROM dtToDate -dtFromDate)*60+EXTRACT(MINUTE FROM dtToDate -dtFromDate)) AS DOUBLE PRECISION)/60)*coalesce(TE.monAmount,1) AS DECIMAL(10,2))
      WHEN numCategory = 2 THEN CAST(coalesce(TE.monAmount,1) AS DECIMAL(10,2))
      ELSE 0
      END AS ExpenseValue
                    ,(SELECT coalesce(numItemCode,0) FROM Item  WHERE numItemCode IN(SELECT numItemCode FROM OpportunityItems WHERE numoppitemtCode = TE.numOppItemID)) AS numItemCode
					,(SELECT coalesce(numClassID,0) FROM OpportunityItems WHERE numoppitemtCode = TE.numOppItemID) AS numClassID
					,(SELECT coalesce(numClassID,0) FROM OpportunityItems WHERE numoppitemtCode = TE.numOppItemID) AS numClassID
					,(SELECT coalesce(vcItemDesc,'') FROM OpportunityItems WHERE numoppitemtCode = TE.numOppItemID) AS vcItemDesc
					,CASE WHEN TE.numApprovalComplete = 6 THEN 'Approved' WHEN TE.numApprovalComplete = -1 THEN 'Declined'  WHEN coalesce(TE.numApprovalComplete,0) = 0 THEN '' ELSE 'Waiting For Approve' END as ApprovalStatus
					,TE.numApprovalComplete,TE.numServiceItemID,TE.numClassID as numEClassId,TE.numExpId
      FROM    timeandexpense TE
      LEFT JOIN OpportunityMaster OM ON OM.numOppId = TE.numOppId AND TE.numDomainID = OM.numDomainId
      LEFT JOIN ProjectsMaster PM ON PM.numProId = TE.numProId AND TE.numDomainID = PM.numdomainId
      LEFT JOIN Chart_Of_Accounts CA ON CA.numAccountId = TE.numExpId AND TE.numDomainID = CA.numDomainId
      LEFT JOIN AdditionalContactsInformation ACI ON ACI.numContactId = TE.numUserCntID
      LEFT JOIN ApprovalConfig AS A on TE.numUserCntID = A.numUserId AND A.numDomainID = v_numDomainID
      WHERE  (A.numLevel1Authority = v_numUserCntID OR A.numLevel2Authority = v_numUserCntID OR A.numLevel3Authority = v_numUserCntID
      OR A.numLevel4Authority = v_numUserCntID OR A.numLevel5Authority = v_numUserCntID) AND TE.numApprovalComplete NOT IN(0,-1)
      AND TE.dtTCreatedOn BETWEEN v_startDate AND v_endDate;
   end if;
   RETURN;
END;
$BODY$;

ALTER FUNCTION public.usp_getapprovalfortimeandexpense(numeric, timestamp without time zone, timestamp without time zone, numeric, integer, refcursor)
    OWNER TO postgres;
