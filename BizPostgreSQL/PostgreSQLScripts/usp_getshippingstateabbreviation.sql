CREATE OR REPLACE FUNCTION USP_GetShippingStateAbbreviation
(
	INOUT SWV_RefCur refcursor DEFAULT NULL
)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for
   SELECT vcStateName,vcStateCode FROM ShippingStateMaster;
END; $$;
