-- Stored procedure definition script USP_GetListItemData for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetListItemData(v_vcFldValue TEXT,
	v_numDomainID NUMERIC(18,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT COALESCE((SELECT
      string_agg(coalesce(vcData,''),', ')
      FROM
      Listdetails
      WHERE
      numDomainid = v_numDomainID
      AND numListItemID IN (SELECT Id FROM SplitIDs(v_vcFldValue,','))),'') as "vcData";
END; $$;












