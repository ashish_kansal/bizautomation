-- Stored procedure definition script USP_BestAccountClosedReport for PostgreSQL
CREATE OR REPLACE FUNCTION USP_BestAccountClosedReport(v_numDivisionId INTEGER,      
 v_numDomainId INTEGER,    
 v_numStatus SMALLINT,  
 v_dtDateFrom TIMESTAMP,                      
 v_dtDateTo TIMESTAMP,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT  Opp.numOppId,
  Opp.vcpOppName AS Name,
  GetOppStatus(Opp.numOppId) as STAGE,
  Opp.intPEstimatedCloseDate AS CloseDate,
  Opp.numrecowner,
  ADC.vcFirstName || ' ' || ADC.vcLastname AS Contact,
  C.vcCompanyName AS Company,
C.numCompanyId,
  Div.tintCRMType,
  ADC.numContactId,
  Div.numDivisionID,
  Div.numTerID,
  Opp.monPAmount,
  ADC1.vcFirstName || ' ' || ADC1.vcLastname as UserName,
  fn_GetContactName(Opp.numassignedto) || '/ ' || fn_GetContactName(Opp.numAssignedBy) as AssignedToBy
   FROM OpportunityMaster Opp
   INNER JOIN AdditionalContactsInformation ADC
   ON Opp.numContactId = ADC.numContactId
   INNER JOIN DivisionMaster Div
   ON Opp.numDivisionId = Div.numDivisionID AND ADC.numDivisionId = Div.numDivisionID
   INNER JOIN CompanyInfo C
   ON Div.numCompanyID = C.numCompanyId
   left join AdditionalContactsInformation ADC1 on ADC1.numContactId = Opp.numrecowner Where Opp.tintopptype = 1
   And Opp.tintoppstatus = v_numStatus AND Div.numDivisionID = v_numDivisionId AND Opp.numDomainId = v_numDomainId
   And Opp.bintCreatedDate Between v_dtDateFrom And v_dtDateTo;
END; $$;













