CREATE OR REPLACE FUNCTION USP_SaveContactColumnConfiguration(v_numDomainID NUMERIC(9,0),      
 v_numUserCntId NUMERIC(9,0),    
 v_FormId SMALLINT ,  
v_numtype NUMERIC(9,0) ,  
v_strXml   TEXT DEFAULT '')
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   delete from InitialListColumnConf where numDomainID = v_numDomainID and numFormId = v_FormId
   and numUserCntID = v_numUserCntId and numtype = v_numtype;  
                           
   INSERT INTO InitialListColumnConf(numDomainID
           ,numUserCntID
           ,numFormId
           ,numFormFieldId
           ,numtype
           ,tintOrder)
   select v_numDomainID,v_numUserCntId,v_FormId,X.numFormFieldID,v_numtype,X.tintOrder FROM 
   XMLTABLE
	(
		'NewDataSet/Table'
		PASSING 
			CAST(v_strXml AS XML)
		COLUMNS
			id FOR ORDINALITY,
			numFormFieldID NUMERIC(18,0) PATH 'numFormFieldID',
			tintOrder SMALLINT PATH 'tintOrder'
	) AS X;                     
                                    
   RETURN;
END; $$;


