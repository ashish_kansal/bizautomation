-- Stored procedure definition script USP_TimeAndExpensesDetailsForPay for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_TimeAndExpensesDetailsForPay(v_numUserCntID NUMERIC(18,0),
	v_numDomainID NUMERIC(18,0),    
	v_numComPayPeriodID NUMERIC(18,0),
	v_ClientTimeZoneOffset INTEGER, INOUT SWV_RefCur refcursor , INOUT SWV_RefCur2 refcursor)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for
   SELECT
   numCategoryHDRID,
		TE.dtFromDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) AS dtFromDate,
		TE.dtToDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) AS dtToDate,
		CASE
   WHEN TE.numCategory = 1 THEN CAST(CAST((EXTRACT(DAY FROM TE.dtToDate -TE.dtFromDate)*60*24+EXTRACT(HOUR FROM TE.dtToDate -TE.dtFromDate)*60+EXTRACT(MINUTE FROM TE.dtToDate -TE.dtFromDate)) AS DOUBLE PRECISION)/60 AS VARCHAR(30))
   WHEN TE.numCategory = 2 THEN CAST(monAmount AS VARCHAR(30))
   WHEN TE.numCategory = 3 THEN(Case when bitFromFullDay = false  then  'HDL'
      WHEN bitFromFullDay = true THEN  'FDL' end)
   END AS Detail,
		Case
   WHEN TE.numCategory = 1 AND TE.numtype = 1 AND OM.numOppId > 0
   THEN((coalesce(CAST((EXTRACT(DAY FROM TE.dtToDate -TE.dtFromDate)*60*24+EXTRACT(HOUR FROM TE.dtToDate -TE.dtFromDate)*60+EXTRACT(MINUTE FROM TE.dtToDate -TE.dtFromDate)) AS DOUBLE PRECISION)/CAST(60 AS DOUBLE PRECISION),
      0))*TE.monAmount)
   WHEN  OM.numOppId > 0 THEN TE.monAmount
   ELSE 0
   END AS ClientCharge,
		CASE
   WHEN TE.numCategory = 1 THEN 'Time'
   WHEN TE.numCategory = 2 THEN(CASE WHEN TE.bitReimburse = true THEN 'Expense(Reimb)' ELSE 'Expense' END)
   ELSE 'Leave'
   END AS RecType,
		CASE WHEN TE.numtype = 1 THEN 'Billable' WHEN TE.numtype = 2 THEN 'Non-Billable' WHEN TE.numtype = 3 then 'Paid Leave' WHEN TE.numtype = 4 then 'Non-Paid' END as Type,
		CASE
   WHEN tintTEType = 0 THEN 'T & E'
   WHEN tintTEType = 1 THEN(CASE WHEN TE.numCategory = 1 THEN 'Sales Time' ELSE 'Sales Exp' END)
   WHEN tintTEType = 2 THEN(CASE WHEN TE.numCategory = 1 THEN 'Project Time' ELSE 'Project Exp' END)
   WHEN tintTEType = 3 THEN(CASE WHEN TE.numCategory = 1 then 'Case Time' ELSE 'Case Exp' END)
   end as chrFrom,
		coalesce(txtDesc,'') AS Description,
		U.vcUserName,
		(SELECT CAST(U.vcUserName AS VARCHAR(30)) || '(' || CAST(L.dtmApprovedOn AS VARCHAR(30)) || ')' || ', ' AS "data()"
      FROM Approval_transaction_log as L
      LEFT JOIN UserMaster as U ON L.numApprovedBy = U.numUserDetailId
      WHERE numRecordId = TE.numCategoryHDRID order by int_transaction_id desc) as ApprovedBy
   FROM
   TimeAndExpenseCommission TEC
   INNER JOIN
   timeandexpense TE
   ON
   TEC.numTimeAndExpenseID = TE.numCategoryHDRID
   LEFT JOIN
   UserMaster as U
   ON
   TE.numTCreatedBy = U.numUserDetailId
   LEFT JOIN
   OpportunityMaster OM
   ON
   TE.numOppId = OM.numOppId
   WHERE
   TEC.numComPayPeriodID = v_numComPayPeriodID
   AND TE.numUserCntID = v_numUserCntID;       
		
   open SWV_RefCur2 for
   SELECT
   vcpOppName
		,vcProjectName
		,fn_GetContactName(SPDTC.numUserCntID) AS vcUserName
		,vcMileStoneName
		,vcStageName
		,vcTaskName
		,FormatedDateFromDate(CAST(dtmCreatedOn+CAST(-1*(EXTRACT(DAY FROM LOCALTIMESTAMP -TIMEZONE('UTC',now()))*60*60*24+EXTRACT(HOUR FROM LOCALTIMESTAMP -TIMEZONE('UTC',now()))*60*60+EXTRACT(MINUTE FROM LOCALTIMESTAMP -TIMEZONE('UTC',now()))*60+EXTRACT(SECOND FROM LOCALTIMESTAMP -TIMEZONE('UTC',now()))) || 'second' as interval)+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) AS DATE),v_numDomainID) AS dtCreatedDate
		,SPDTC.numHours
   FROM
   StagePercentageDetailsTaskCommission SPDTC
   INNER JOIN
   StagePercentageDetailsTask SPDT
   ON
   SPDTC.numTaskID = SPDT.numTaskId
   INNER JOIN
   StagePercentageDetails SPD
   ON
   SPDT.numStageDetailsId = SPD.numStageDetailsId
   LEFT JOIN
   OpportunityMaster OM
   ON
   SPDT.numOppId = OM.numOppId
   LEFT JOIN
   ProjectsMaster PM
   ON
   SPDT.numProjectId = PM.numProId
   WHERE
   SPDTC.numComPayPeriodID = v_numComPayPeriodID
   AND SPDTC.numUserCntID = v_numUserCntID;
   RETURN;
END; $$;


