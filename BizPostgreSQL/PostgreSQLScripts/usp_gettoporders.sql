CREATE OR REPLACE FUNCTION USP_GetTopOrders(v_numDivisionID NUMERIC,
    v_numContactID NUMERIC,
    v_tintOppType SMALLINT,
    v_TopCount INTEGER,
    v_numDomainID NUMERIC, INOUT SWV_RefCur refcursor , INOUT SWV_RefCur2 refcursor)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for
   SELECT FormatedDateFromDate(OM.bintCreatedDate,OM.numDomainId) AS bintCreatedDate,OM.numOppId,OM.monDealAmount,
        OM.bintCreatedDate AS DummyColumn,OM.vcpOppName -- Added dummy column because on production somehow without it doesn't sort result on date desc,might be because of formatted datetime
   FROM    OpportunityMaster OM
   INNER JOIN DivisionMaster DM ON OM.numDivisionId = DM.numDivisionID
   INNER JOIN AdditionalContactsInformation ACI ON OM.numContactId = ACI.numContactId
   WHERE   OM.tintopptype = v_tintOppType
   AND OM.numDivisionId = v_numDivisionID
   AND OM.numContactId = v_numContactID
   AND OM.numDomainId = v_numDomainID
   AND OM.tintoppstatus = 1
   ORDER BY OM.bintCreatedDate desc;
       
   open SWV_RefCur2 for
   SELECT   OI.numOppId,
                OI.numoppitemtCode,
                OI.vcPathForTImage,
                OI.numItemCode,
                OI.vcItemName,
                OI.vcModelID,
                OI.vcItemDesc,
                OI.numUnitHour,
                OI.monTotAmount,
                GetSerialNumberList(OI.numoppitemtCode) AS vcSerialNo
   FROM     OpportunityItems OI
   INNER JOIN Item I ON OI.numItemCode = I.numItemCode
   WHERE    I.numDomainID = v_numDomainID
   AND OI.numOppId IN(SELECT OM.numOppId
      FROM    OpportunityMaster OM
      INNER JOIN DivisionMaster DM ON OM.numDivisionId = DM.numDivisionID
      INNER JOIN AdditionalContactsInformation ACI ON OM.numContactId = ACI.numContactId
      WHERE   OM.tintopptype = v_tintOppType
      AND OM.numDivisionId = v_numDivisionID
      AND OM.numContactId = v_numContactID
      AND OM.numDomainId = v_numDomainID
      AND OM.tintoppstatus = 1
      ORDER BY OM.bintCreatedDate desc);
   RETURN;
END; $$;


