-- Stored procedure definition script USP_GetSolution for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_GetSolution(v_numCategory NUMERIC(9,0) DEFAULT null,        
v_KeyWord VARCHAR(50) DEFAULT NULL,        
v_SortChar CHAR(1) DEFAULT '0',        
v_columnName VARCHAR(50) DEFAULT NULL,        
v_columnSortOrder VARCHAR(10) DEFAULT NULL,        
INOUT v_TotRecs INTEGER  DEFAULT NULL,    
v_numDomainID NUMERIC(9,0) DEFAULT 0, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_strSql  VARCHAR(8000);
BEGIN
   v_strSql := 'select numSolnID,        
 vcSolnTitle,txtSolution         
 from SolutionMaster         
 where numDomainID =' || SUBSTR(CAST(v_numDomainID AS VARCHAR(15)),1,15);

   if v_KeyWord <> '' then 
      v_strSql := coalesce(v_strSql,'') || 'and (vcSolnTitle ilike ''%' || coalesce(v_KeyWord,'') || '%''        
 or txtSolution ilike ''%' || coalesce(v_KeyWord,'') || '%'')';
   end if;        
      
   if v_numCategory <> 0 then 
      v_strSql := coalesce(v_strSql,'') || ' and  numCategory =' || SUBSTR(CAST(v_numCategory AS VARCHAR(15)),1,15);
   end if;       
   if v_SortChar <> '0' then 
      v_strSql := coalesce(v_strSql,'') || ' And vcSolnTitle ilike ''' || coalesce(v_SortChar,'') || '%''';
   end if;         
   if v_columnName is not null then 
      v_strSql := coalesce(v_strSql,'') || ' ORDER BY ' || coalesce(v_columnName,'') || ' ' || coalesce(v_columnSortOrder,'');
   end if;        
      
   OPEN SWV_RefCur FOR EXECUTE v_strSql;     
   
   RETURN;
END; $$;


