-- Stored procedure definition script USP_UserMaster_UpdateEmailAlias for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_UserMaster_UpdateEmailAlias(v_numDomainID NUMERIC(18,0),
	v_numUserID NUMERIC(18,0),
    v_vcEmailAlias VARCHAR(100),
	v_vcSMTPPassword VARCHAR(100))
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   UPDATE
   UserMaster
   SET
   vcEmailAlias = v_vcEmailAlias,vcEmailAliasPassword = v_vcSMTPPassword
   WHERE
   numDomainID = v_numDomainID
   AND numUserId = v_numUserID;
   RETURN;
END; $$;


