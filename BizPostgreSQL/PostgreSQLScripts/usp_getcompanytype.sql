-- Stored procedure definition script usp_GetCompanyType for PostgreSQL
CREATE OR REPLACE FUNCTION usp_GetCompanyType(v_numCompanyId NUMERIC 
--  
,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT cast(numCompanyType as VARCHAR(255)) FROM CompanyInfo WHERE numCompanyId = v_numCompanyId;
END; $$;












