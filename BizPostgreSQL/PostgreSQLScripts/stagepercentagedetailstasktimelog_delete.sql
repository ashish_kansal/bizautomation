CREATE OR REPLACE FUNCTION StagePercentageDetailsTaskTimeLog_Delete_TrFunc()
RETURNS TRIGGER LANGUAGE plpgsql
AS $$
	DECLARE
	v_numContractsLogId  NUMERIC(18,0);
	v_numContactID  NUMERIC(18,0);
	v_numUsedTime  INTEGER;
BEGIN
      IF EXISTS(SELECT
					CL.numContractId
				FROM
					ContractsLog CL
				INNER JOIN
					Contracts C
				ON
					CL.numContractId = C.numContractId
				WHERE
					C.numDomainId = OLD.numDomainID
					AND CL.numReferenceId = OLD.numTaskID
					AND vcDetails = '1') then
        SELECT 
			numContractsLogId, CL.numContractId, numUsedTime INTO v_numContractsLogId,v_numContactID,v_numUsedTime 
		FROM
			ContractsLog CL
        INNER JOIN
			Contracts C
        ON
			CL.numContractId = C.numContractId
        WHERE
			C.numDomainId = OLD.numDomainID
			AND CL.numReferenceId = OLD.numTaskID
			AND vcDetails = '1';

        UPDATE Contracts SET timeUsed = coalesce(timeUsed,0) -coalesce(v_numUsedTime,0),dtmModifiedOn = TIMEZONE('UTC',now()) WHERE numContractId = v_numContactID;
         
		DELETE FROM ContractsLog WHERE numContractsLogId = v_numContractsLogId;
    end if;

	RETURN NULL;
END; $$;
CREATE TRIGGER StagePercentageDetailsTaskTimeLog_Delete AFTER DELETE ON StagePercentageDetailsTaskTimeLog FOR EACH ROW EXECUTE PROCEDURE StagePercentageDetailsTaskTimeLog_Delete_TrFunc();


