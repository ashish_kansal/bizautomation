-- Stored procedure definition script USP_ManageBizDocActionItem for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ManageBizDocActionItem(v_numDomainID NUMERIC,
v_dtFromDate TIMESTAMP,
v_dtTodate TIMESTAMP)
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numEmployeeID  NUMERIC(18,0);
   v_numDivisionID  NUMERIC(18,0);
   v_numOppBizDocsId  NUMERIC(18,0);
   v_numTempEmployeeId  NUMERIC(18,0);
   v_numBizActionId  NUMERIC(18,0);
   v_numContactID  NUMERIC(18,0);
   v_numActionTypeID  NUMERIC(18,0);
   v_btDocType  SMALLINT;
   v_numBizDocAppId  NUMERIC(18,0);
   BizDocActionItem CURSOR FOR
   select * from tt_TEMP_BIZDOCACTIONITEM order by numEmployeeID;
BEGIN
   drop table IF EXISTS tt_TEMP_BIZDOCACTIONITEM CASCADE;
   create TEMPORARY TABLE tt_TEMP_BIZDOCACTIONITEM
   (
      numBizDocAppId NUMERIC(18,0),
      numOppBizDocsId NUMERIC(18,0),
      numEmployeeID NUMERIC(18,0),
      numDivisionID NUMERIC(18,0),
      numContactID NUMERIC(18,0),
      numActionTypeID NUMERIC(18,0),
      btDocType SMALLINT 
   );


--select * from  View_BizDocForApproval where BizDocId like 'ABC inc-SO-January-2010-A6056-BD-5450--'
--select * from BizDocApprovalRule
--update BizDocApprovalRule set btBizDocPaidFull=0

   insert into tt_TEMP_BIZDOCACTIONITEM
   select BA.numBizDocAppID,numOppBizDocsId,numEmployeeID,AC.numDivisionId,AC.numContactId,BA.numActionTypeID,1 as DocType from
   View_BizDocForApproval VB
   INNER JOIN BizDocApprovalRule BA ON  VB.numBizDocId = BA.numBizDocTypeId and BA.btBizDocPaidFull = true AND BA.numDomainID = VB.numDomainID
   INNER JOIN BizDocApprovalRuleEmployee BE ON VB.BizDocAmount = BizDocPaidAmount AND BE.numBizDocAppID = BA.numBizDocAppID
   INNER JOIN AdditionalContactsInformation AC ON AC.numContactId = BE.numEmployeeID AND AC.numDomainID = BA.numDomainID
   where
   dtCreatedDate BETWEEN v_dtFromDate and v_dtTodate and
   VB.BizDocAmount between BA.numRangeFrom and BA.numRangeTo and
   VB.numDomainID = v_numDomainID
   UNION
   select  BA.numBizDocAppID,numOppBizDocsId,numEmployeeID,AC.numDivisionId,AC.numContactId,BA.numActionTypeID,1 as DocType from
   View_BizDocForApproval VB
   INNER JOIN BizDocApprovalRule BA ON BA.numBizDocTypeId = VB.numBizDocId and BA.btBizDocPaidFull = false AND BA.numDomainID = v_numDomainID
   INNER JOIN BizDocApprovalRuleEmployee BE ON BE.numBizDocAppID = BA.numBizDocAppID
   INNER JOIN AdditionalContactsInformation AC ON AC.numContactId = BE.numEmployeeID AND AC.numDomainID = v_numDomainID
   where
   dtCreatedDate BETWEEN v_dtFromDate and v_dtTodate and
   VB.BizDocAmount between BA.numRangeFrom and BA.numRangeTo and
   VB.numDomainID = v_numDomainID
   UNION
--select * from View_BizDoc
   select  0,numOppBizDocsId,BizDocCreatedBy,AC.numDivisionId,AC.numContactId,0 AS numActionTypeID,1 as DocType from
   View_BizDoc VB
   INNER JOIN AdditionalContactsInformation AC ON VB.numDomainID = AC.numDomainID
   INNER JOIN(SELECT * FROM DocumentWorkflow  DF WHERE DF.cDocType = 'B') DF ON AC.numContactId = DF.numContactID AND VB.numOppBizDocsId = DF.numDocID
   where DF.dtCreatedDate BETWEEN v_dtFromDate and v_dtTodate and
   VB.numDomainID = v_numDomainID
   UNION
   select 0,
  numGenericDocID,
c.numContactId,
  numDivisionId,
	 numRecID,
  0 as numActionTypeID, 0 as DocType
   from GenericDocuments A INNER JOIN DocumentWorkflow B ON A.numGenericDocID = B.numDocID INNER JOIN AdditionalContactsInformation c ON B.numContactID = c.numContactId
   where  A.numGenericDocID = numDocID and
   A.numDomainId = v_numDomainID  AND
   B.dtCreatedDate between v_dtFromDate and v_dtTodate 
--and 
--		 A.numSpecificDocID not in 
--		(select numOppBizDocsId from BizActionDetails where btDocType=0) 

   UNION
   select 0,
  numGenericDocID,
c.numContactId,
  numDivisionId,
	 A.numCreatedBy,
  0 as numActionTypeID, 0 as DocType
   from GenericDocuments A INNER JOIN DocumentWorkflow B ON A.numGenericDocID = B.numDocID INNER JOIN AdditionalContactsInformation c ON B.numContactID = c.numContactId
   where  A.numDomainId = v_numDomainID    AND
   B.dtCreatedDate between v_dtFromDate and v_dtTodate; 
--and 
--		 A.numGenericDocID not in 
--		(select numGenericDocID from BizActionDetails where btDocType=0)

--select * from GenericDocuments
--select * from #temp_BizDocActionItem order by numEmployeeID


   OPEN BizDocActionItem;

   v_numTempEmployeeId := 0;

   FETCH NEXT FROM BizDocActionItem into v_numBizDocAppId,v_numOppBizDocsId,v_numEmployeeID,v_numDivisionID,v_numContactID,
   v_numActionTypeID,v_btDocType;
   WHILE FOUND LOOP
      if v_numTempEmployeeId <> v_numEmployeeID then
			
         insert into BizDocAction(numContactId ,numDivisionId,numStatus,numDomainID,dtCreatedDate,numAssign,numCreatedBy,bitTask,numBizDocAppId)
				values(v_numEmployeeID,v_numDivisionID,0,v_numDomainID,LOCALTIMESTAMP ,v_numContactID,1,v_numActionTypeID,v_numBizDocAppId);
				
         v_numBizActionId := CURRVAL('BizDocAction_seq');
      end if;
      INSERT INTO BizActionDetails(numBizActionId,numOppBizDocsId,btStatus,btDocType)
			VALUES(v_numBizActionId,v_numOppBizDocsId,0,v_btDocType);
			
      v_numTempEmployeeId := v_numEmployeeID;
      FETCH NEXT FROM BizDocActionItem into v_numBizDocAppId,v_numOppBizDocsId,v_numEmployeeID,v_numDivisionID,v_numContactID,
      v_numActionTypeID,v_btDocType;
   END LOOP;

   CLOSE BizDocActionItem;

--select * from #temp_BizDocActionItem;
   drop table IF EXISTS tt_TEMP_BIZDOCACTIONITEM CASCADE;
   RETURN;
END; $$;

/****** Object:  StoredProcedure [dbo].[usp_GetCircularDistribution]    Script Date: 07/26/2008 16:16:40 ******/



