-- Stored procedure definition script USP_UpdatedomainMinimumPriceUpdate for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_UpdatedomainMinimumPriceUpdate(v_numApprovalRuleID NUMERIC(18,0) DEFAULT 0, 
	v_numListItemID NUMERIC(18,0) DEFAULT 0,                                    
	v_numDomainID NUMERIC(18,0) DEFAULT 0,
	v_numAbovePercent NUMERIC(18,2) DEFAULT NULL,
	v_numBelowPercent NUMERIC(18,2) DEFAULT NULL,
	v_numBelowPriceField NUMERIC(18,0) DEFAULT NULL,
	v_bitCostApproval BOOLEAN DEFAULT NULL,
	v_bitListPriceApproval BOOLEAN DEFAULT NULL,
	v_bitMarginPriceViolated BOOLEAN DEFAULT NULL,
	v_vcUnitPriceApprover VARCHAR(1) DEFAULT NULL,
	v_byteMode INTEGER DEFAULT NULL ,
	v_IsMinUnitPriceRule BOOLEAN DEFAULT NULL)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   UPDATE Domain SET bitMinUnitPriceRule =  v_IsMinUnitPriceRule WHERE numDomainId = v_numDomainID;                      
   IF EXISTS(SELECT numApprovalRuleID FROM ApprovalProcessItemsClassification WHERE numDomainID = v_numDomainID AND coalesce(numListItemID,0) = coalesce(v_numListItemID,0)) then
	
      UPDATE
      ApprovalProcessItemsClassification
      SET
      numAbovePercent = v_numAbovePercent,numBelowPercent = v_numBelowPercent,
      numBelowPriceField = v_numBelowPriceField,bitCostApproval = v_bitCostApproval,
      bitListPriceApproval = v_bitListPriceApproval,bitMarginPriceViolated = v_bitMarginPriceViolated
      WHERE
      numDomainID = v_numDomainID
      AND coalesce(numListItemID,0) = coalesce(v_numListItemID,0);
   ELSE
      INSERT INTO ApprovalProcessItemsClassification(numListItemID
			,numDomainID
			,numAbovePercent
			,numBelowPercent
			,numBelowPriceField
			,bitCostApproval
			,bitListPriceApproval
			,bitMarginPriceViolated)
		VALUES(v_numListItemID
			,v_numDomainID
			,v_numAbovePercent
			,v_numBelowPercent
			,v_numBelowPriceField
			,v_bitCostApproval
			,v_bitListPriceApproval
			,v_bitMarginPriceViolated);
   end if;
   RETURN;
END; $$;



