-- Stored procedure definition script USP_RepItems for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_RepItems(v_numDomainID NUMERIC,              
  v_dtFromDate TIMESTAMP,              
  v_dtToDate TIMESTAMP,              
  v_numUserCntID NUMERIC DEFAULT 0,                   
  v_tintType SMALLINT DEFAULT NULL,
  v_numSort INTEGER DEFAULT NULL,
  v_numDivisionId NUMERIC DEFAULT NULL, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_vcSql  VARCHAR(2500);
BEGIN
   DROP TABLE IF EXISTS tt_TEMPREPITEMS CASCADE;
   create TEMPORARY TABLE tt_TEMPREPITEMS
   (
      numItemCode NUMERIC(9,0),
      vcItemName VARCHAR(300),
      vcModelID VARCHAR(250),
      txtItemDesc VARCHAR(1000),
      Type VARCHAR(100),
      Amount NUMERIC(9,2),
      TotalItem DOUBLE PRECISION,
      OnHand DOUBLE PRECISION,
      OnOrder DOUBLE PRECISION,
      BackOrder DOUBLE PRECISION,
      Allocation DOUBLE PRECISION
   );
 
   INSERT INTO tt_TEMPREPITEMS
   SELECT  numItemCode,
        vcItemName,
        vcModelID,
        txtItemDesc,
        CAST(CASE WHEN charItemType = 'P' THEN 'Product'
   WHEN charItemType = 'S' THEN 'Service'
   END AS VARCHAR(100)) AS TYPE,
        0,
        0,
        (SELECT    coalesce(SUM(numOnHand),0)
      FROM      WareHouseItems
      WHERE     numItemID = numItemCode) AS OnHand,
        (SELECT    coalesce(SUM(numonOrder),0)
      FROM      WareHouseItems
      WHERE     numItemID = numItemCode) AS OnOrder,
        (SELECT    coalesce(SUM(numBackOrder),0)
      FROM      WareHouseItems
      WHERE     numItemID = numItemCode) AS BackOrder,
        (SELECT    coalesce(SUM(numAllocation),0)
      FROM      WareHouseItems
      WHERE     numItemID = numItemCode) AS Allocation
   FROM    Item
   WHERE   numDomainID = v_numDomainID;

   UPDATE tt_TEMPREPITEMS
   SET Amount = coalesce(X.Amount,0),TotalItem = coalesce(X.TotalItem,0)
   FROM(SELECT OppItem.numItemCode,SUM(OppItem.numUnitHour) AS TotalItem,SUM(monTotAmount) AS Amount
   FROM   OpportunityMaster Opp
   JOIN OpportunityItems OppItem ON OppItem.numOppId = Opp.numOppId
   WHERE  Opp.numDomainId = v_numDomainID AND Opp.tintopptype = 1
   AND Opp.tintoppstatus = 1
   AND Opp.numDivisionId =(CASE v_numDivisionId
   WHEN 0
   THEN Opp.numDivisionId
   ELSE v_numDivisionId
   END)
   AND Opp.bintCreatedDate >= v_dtFromDate
   AND Opp.bintCreatedDate <= v_dtToDate
   GROUP BY  OppItem.numItemCode) X
   WHERE tt_TEMPREPITEMS.numItemCode = X.numItemCode;

   v_vcSql := 'SELECT * FROM tt_TEMPREPITEMS WHERE TotalItem > 0 ORDER BY ' ||(Case v_numSort WHEN 0 THEN 'Amount' ELSE 'TotalItem'  END) || ' desc';

   OPEN SWV_RefCur FOR EXECUTE v_vcSql;
   RETURN;
END; $$;
--


