-- Stored procedure definition script USP_SaveAdvanceSearchGridColumnWidth for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_SaveAdvanceSearchGridColumnWidth(v_numDomainID NUMERIC(9,0) DEFAULT 0,  
v_numUserCntID NUMERIC(9,0) DEFAULT 0,
v_str TEXT DEFAULT NULL,
v_numViewID INTEGER DEFAULT NULL)
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numGroupID  NUMERIC(18,0);

BEGIN
   select   numGroupID INTO v_numGroupID FROM UserMaster WHERE numUserDetailId = v_numUserCntID;
                                                                     

   drop table IF EXISTS tt_TEMPTABLE CASCADE;
   Create TEMPORARY TABLE tt_TEMPTABLE
   (
      numFormId NUMERIC(9,0),
      numFieldId NUMERIC(9,0),
      bitCustom BOOLEAN,
      intColumnWidth INTEGER
   );

   insert into tt_TEMPTABLE(numFormId,numFieldId,bitCustom,intColumnWidth)
   SELECT 
	numFormId,numFieldId,bitCustom,intColumnWidth
	FROM 
	XMLTABLE
		(
			'NewDataSet/GridColumnWidth'
			PASSING 
				CAST(v_str AS XML)
			COLUMNS
				id FOR ORDINALITY,
				numFormId NUMERIC(9,0) PATH 'numFormId',
				numFieldId NUMERIC(9,0) PATH 'numFieldId',
				bitCustom BOOLEAN PATH 'bitCustom',
				intColumnWidth INTEGER PATH 'intColumnWidth'
		) AS X;

   UPDATE
   AdvSerViewConf t1
   SET
   intColumnWidth = temp.intColumnWidth
   FROM
   tt_TEMPTABLE temp
   WHERE
   t1.numFormFieldID = temp.numFieldId AND(t1.numFormID = 1
   AND temp.numFormId = 1
   AND t1.tintViewID = v_numViewID
   AND t1.numDomainID = v_numDomainID
   AND t1.numGroupID = v_numGroupID
   AND coalesce(t1.bitCustom,false) = temp.bitCustom); -- RIGHT NOW CUSTOM FIELDS ARE NOT AVAILABLE FOR SELECTION IN ADVANCE SEARCH GRID
	
   UPDATE
   DycFormConfigurationDetails DFCD
   SET
   intColumnWidth = temp.intColumnWidth
   FROM
   tt_TEMPTABLE temp
   WHERE(DFCD.numFormID = temp.numFormId
   AND DFCD.numFieldID = temp.numFieldId
   AND coalesce(DFCD.bitCustom,false) = temp.bitCustom) AND(DFCD.numDomainID = v_numDomainID
   AND DFCD.numUserCntID = v_numUserCntID
   AND DFCD.tintPageType = 1
   AND temp.numFormId <> 1);                                               

   drop table IF EXISTS tt_TEMPTABLE CASCADE;                                        

   RETURN;
END; $$;


