-- Stored procedure definition script USP_GetSalesTemplateItems for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetSalesTemplateItems(v_numSalesTemplateID NUMERIC(18,0),
    v_numDomainID NUMERIC(18,0),
	v_numDivisionID NUMERIC(18,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_tintPriceLevel  SMALLINT;
   v_tintDefaultSalesPricing  SMALLINT;
BEGIN
   select   numDefaultSalesPricing INTO v_tintDefaultSalesPricing FROM Domain WHERE numDomainId = v_numDomainID;

   select   coalesce(D.tintPriceLevel,0) INTO v_tintPriceLevel FROM
   DivisionMaster D WHERE
   numDivisionID = v_numDivisionID;       

   open SWV_RefCur for SELECT
   X.*
		,(SELECT  vcCategoryName FROM Category WHERE numCategoryID IN(SELECT numCategoryID FROM ItemCategory IC WHERE X.numItemCode = IC.numItemID) LIMIT 1) AS vcCategoryName
   FROM(SELECT DISTINCT
      STI.numSalesTemplateItemID,
            STI.numSalesTemplateID,
            STI.numSalesTemplateItemID AS numoppitemtCode,
            STI.numItemCode,
            STI.numUnitHour,
            CASE WHEN coalesce(v_numDivisionID,0) > 0 AND coalesce(v_tintPriceLevel,0) > 0 AND v_tintDefaultSalesPricing = 1 AND(SELECT COUNT(*) FROM PricingTable WHERE numItemCode = STI.numItemCode AND coalesce(numPriceRuleID,0) = 0 AND coalesce(numCurrencyID,0) = 0) > 0 THEN
         coalesce((SELECT
            coalesce((CASE
            WHEN TEMP.tintRuleType = 1 AND TEMP.tintDiscountType = 1
            THEN coalesce(I.monListPrice,0) -(coalesce(I.monListPrice,0)*(TEMP.decDiscount/100))
            WHEN TEMP.tintRuleType = 1 AND TEMP.tintDiscountType = 2
            THEN coalesce(I.monListPrice,0) -TEMP.decDiscount
            WHEN TEMP.tintRuleType = 2 AND TEMP.tintDiscountType = 1
            THEN coalesce(Vendor.monCost,0)+(coalesce(Vendor.monCost,0)*(TEMP.decDiscount/100))
            WHEN TEMP.tintRuleType = 2 AND TEMP.tintDiscountType = 2
            THEN coalesce(Vendor.monCost,0)+TEMP.decDiscount
            WHEN TEMP.tintRuleType = 3
            THEN TEMP.decDiscount
            END),STI.monPrice)
            FROM(SELECT
               ROW_NUMBER() OVER(PARTITION BY PricingTable.numItemCode ORDER BY numPricingID) AS Id,
							* FROM
               PricingTable
               WHERE
               PricingTable.numItemCode = STI.numItemCode
               AND coalesce(numCurrencyID,0) = 0) TEMP
            WHERE
            numItemCode = STI.numItemCode
            AND (tintRuleType = 3 AND Id = v_tintPriceLevel)),STI.monPrice) ELSE STI.monPrice END AS monPrice,
            STI.monTotAmount,
            STI.numSourceID,
            STI.numWarehouseID,
            STI.Warehouse,
            STI.numWarehouseItmsID,
            STI.ItemType,
            STI.ItemType as vcType,
            STI.Attributes,
			STI.Attributes as vcAttributes,
            STI.AttrValues,
            I.bitFreeShipping AS FreeShipping, --Use from Item table
            STI.Weight,
            STI.Op_Flag,
            STI.bitDiscountType,
            STI.fltDiscount,
            STI.monTotAmtBefDiscount,
            STI.ItemURL,
            STI.numDomainID,
            '' AS vcItemDesc,
            (SELECT  vcPathForTImage FROM ItemImages II WHERE II.numItemCode = I.numItemCode AND II.numDomainId = I.numDomainID AND II.bitDefault = true LIMIT 1) AS vcPathForTImage,
            I.vcItemName,
            false AS DropShip,
			false as bitDropShip,
            I.bitTaxable,
            coalesce(STI.numUOM,0) AS numUOM,
			coalesce(STI.numUOM,0) as numUOMId,
            coalesce(STI.vcUOMName,'') AS vcUOMName,
            coalesce(STI.UOMConversionFactor,1) AS UOMConversionFactor,
			coalesce(STI.numVendorWareHouse,0) as numVendorWareHouse
			,coalesce(STI.numShipmentMethod,0) as numShipmentMethod
			,coalesce(STI.numSOVendorId,0) as numSOVendorId
			,false as bitWorkOrder
			,I.charItemType
			,fn_GetUOMName(I.numBaseUnit) AS vcBaseUOMName
			,0 AS numSortOrder
      FROM
      SalesTemplateItems STI
      INNER JOIN Item I ON I.numItemCode = STI.numItemCode
      LEFT JOIN Vendor ON I.numItemCode = Vendor.numItemCode AND I.numVendorID = Vendor.numVendorID
      LEFT JOIN UOM U ON U.numUOMId = I.numSaleUnit
      WHERE
      STI.numSalesTemplateID = v_numSalesTemplateID
      AND STI.numDomainID = v_numDomainID) X;
END; $$;
--Created By Anoop Jayaraj          
--exec USP_ItemDetailsForEcomm @numItemCode=859064,@numWareHouseID=1074,@numDomainID=172,@numSiteId=90












