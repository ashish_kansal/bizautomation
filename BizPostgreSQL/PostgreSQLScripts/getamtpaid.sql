-- Function definition script GetAmtPaid for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION GetAmtPaid(v_numOppid NUMERIC(9,0))
RETURNS DECIMAL(20,5) LANGUAGE plpgsql
   AS $$
   DECLARE
   v_AmtPaid  DECIMAL(20,5);
BEGIN
   select   tintoppstatus INTO v_AmtPaid from OpportunityMaster where numOppId = v_numOppid    LIMIT 1;

   RETURN v_AmtPaid;
END; $$;

