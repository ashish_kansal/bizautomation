-- Stored procedure definition script USP_RecurringTransactionList for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_RecurringTransactionList(v_numFilterBy NUMERIC(9,0) DEFAULT 0,                        
v_ChrInterval CHAR(1) DEFAULT NULL,                        
v_numAcntType NUMERIC(9,0) DEFAULT 0,                  
v_varCustomerOrVendorSearch VARCHAR(500) DEFAULT NULL,              
v_numDomainId NUMERIC(9,0) DEFAULT 0,      
v_ClientTimeZoneOffset INTEGER DEFAULT NULL)
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_strSql  VARCHAR(8000);
BEGIN
   v_strSql := '';                        
   If v_numFilterBy = 0 then

      v_strSql := 'Select                         
RT.numRecurringId As RecurringId, dbo.fn_GetContactName(CCCD.numCreatedBy) As CreatedBy,DateAdd(minute,' || CAST(-v_ClientTimeZoneOffset AS VARCHAR(10)) || ', CCCD.dtCreatedDate) As CreatedDate, Null DepositId,          
CCCD.numCashCreditId As CashCreditId,Null CheckId, Null as OppId, GJH.numJournal_Id JournalId, 0 MoneyIn,  1 MoneyOut, RT.varRecurringTemplateName as TemplateName,                    
CI.vcCompanyName as CustomerName,dbo.fn_GetAcntTypeDescription(CCCD.numCashCreditId,''CC'') As AccntType,                
Case When CCCD.bitMoneyOut=false then ''Cash'' Else ''Credit Card Details'' End  as TransactionType,                    
case when chrIntervalType=''D'' then ''Daily, ''  when chrIntervalType=''M'' then ''Monthly , ''                    
When chrIntervalType=''Y'' then ''Yearly , '' end as IntervalType,                     
CCCD.monAmount as monAmount,RT.dtStartDate As StartDate, RT.dtEndDate  As EndDate                        
From RecurringTemplate RT                        
inner join CashCreditCardDetails CCCD on RT.numRecurringId =CCCD.numRecurringId            
inner join General_Journal_Header GJH on CCCD.numCashCreditId=GJH.numCashCreditCardId              
Left outer join DivisionMaster as DM on CCCD.numPurchaseId=DM.numDivisionID                                                                                     
Left outer join CompanyInfo as CI on  DM.numCompanyID=CI.numCompanyId           
Where RT.numDomainId=' || SUBSTR(CAST(v_numDomainId AS VARCHAR(5)),1,5);
      If v_varCustomerOrVendorSearch <> '' then
         v_strSql := coalesce(v_strSql,'') || '  And CI.vcCompanyName like ''' || SUBSTR(CAST(v_varCustomerOrVendorSearch AS VARCHAR(500)),1,500) || '%''';
      end if;
      v_strSql := coalesce(v_strSql,'') || 'Union All                        
Select                         
RT.numRecurringId As RecurringId, dbo.fn_GetContactName(CD.numCreatedBy) As CreatedBy, DateAdd(minute,' || CAST(-v_ClientTimeZoneOffset AS VARCHAR(10)) || ',CD.dtCreatedDate) As CreatedDate,Null DepositId,                 
Null CashCreditId,CD.numCheckId As  CheckId, Null as OppId, GJH.numJournal_Id JournalId, 0 MoneyIn,1 MoneyOut, RT.varRecurringTemplateName As TemplateName,                
CI.vcCompanyName as CustomerName,                    
dbo.fn_GetAcntTypeDescription(CD.numCheckId,''C'') As AccntType,''Checks'' as TransactionType,case when chrIntervalType=''D'' then ''Daily, ''                   
when chrIntervalType=''M'' then ''Monthly , '' When chrIntervalType=''Y'' then ''Yearly,''end as IntervalType,                    
CD.monCheckAmt as monAmount,RT.dtStartDate As StartDate, RT.dtEndDate  As EndDate                  
From RecurringTemplate RT                        
Inner join CheckDetails CD on RT.numRecurringId =CD.numRecurringId                  
inner join General_Journal_Header GJH on CD.numCheckId=GJH.numCheckId                
Left outer join DivisionMaster as DM on CD.numCustomerId=DM.numDivisionID                                                                                     
Left outer join CompanyInfo as CI on  DM.numCompanyID=CI.numCompanyId                     
Where RT.numDomainId=' || SUBSTR(CAST(v_numDomainId AS VARCHAR(5)),1,5);
      If v_varCustomerOrVendorSearch <> '' then
         v_strSql := coalesce(v_strSql,'') || ' And  CI.vcCompanyName like ''' || SUBSTR(CAST(v_varCustomerOrVendorSearch AS VARCHAR(500)),1,500) || '%''';
      end if;
      v_strSql := coalesce(v_strSql,'') || 'Union All                        
Select                         
RT.numRecurringId As RecurringId,dbo.fn_GetContactName(GJH.numCreatedBy)As CreatedBy,DateAdd(minute,' || CAST(-v_ClientTimeZoneOffset AS VARCHAR(10)) || ',GJH.datCreatedDate) As CreatedDate, Null DepositId, Null As CashCreditId,                
Null As CheckId, Null as OppId,GJH.numJournal_Id As JournalId, 0 MoneyIn,0 MoneyOut, RT.varRecurringTemplateName as TemplateName,Null as CustomerName,                
dbo.fn_GetAcntTypeDescription(GJH.numJournal_Id,''JD'') As AccntType,                      
''Journal'' as TransactionType,case when chrIntervalType=''D'' then ''Daily, ''  when chrIntervalType=''M'' then ''Monthly , '' When chrIntervalType=''Y'' then ''Yearly,''                      
end as IntervalType, GJH.numAmount as monAmount, RT.dtStartDate As StartDate, RT.dtEndDate  As EndDate                        
From RecurringTemplate RT                        
Inner join General_Journal_Header GJH on RT.numRecurringId =GJH.numRecurringId Where RT.numDomainId=' || SUBSTR(CAST(v_numDomainId AS VARCHAR(5)),1,5);
      v_strSql := coalesce(v_strSql,'') || 'Union All            
 Select                         
RT.numRecurringId As RecurringId,dbo.fn_GetContactName(DD.numCreatedBy) As CreatedBy,DateAdd(minute,' || CAST(-v_ClientTimeZoneOffset AS VARCHAR(10)) || ', DD.dtCreationDate) As CreatedDate,                
DD.numDepositId As DepositId, Null CashCreditId,Null CheckId, Null as OppId, GJH.numJournal_Id JournalId, 1 MoneyIn,0 MoneyOut, RT.varRecurringTemplateName as TemplateName,                
NULL as CustomerName,dbo.fn_GetAcntTypeDescription(DD.numDepositId,''DD'') As AccntType,                      
''Deposit'' as TransactionType,case when chrIntervalType=''D'' then ''Daily, ''    when chrIntervalType=''M'' then ''Monthly , ''  When chrIntervalType=''Y'' then ''Yearly , ''                  
end as IntervalType,DD.monDepositAmount As monAmount,RT.dtStartDate As StartDate, RT.dtEndDate  As EndDate                        
From RecurringTemplate RT                        
inner join DepositDetails DD on RT.numRecurringId =DD.numRecurringId             
inner join General_Journal_Header GJH on DD.numDepositId=GJH.numDepositId   
Where RT.numDomainId=' || SUBSTR(CAST(v_numDomainId AS VARCHAR(5)),1,5);
      v_strSql := coalesce(v_strSql,'') || 'Union All            
 Select                         
RT.numRecurringId As RecurringId,dbo.fn_GetContactName(Opp.numCreatedBy) As CreatedBy,DateAdd(minute,' || CAST(-v_ClientTimeZoneOffset AS VARCHAR(10)) || '/** SDynId 1 6 **/ /** SDynId 1 4 **/ /** SDynId 1 2 **/ /** SDynExp 1 **/ Select                         
RT.numRecurringId As RecurringId, fn_GetContactName(CCCD.numCreatedBy) As CreatedBy,CCCD.dtCreatedDate+CAST(?1? || ''minute'' as interval) As CreatedDate, CAST(null as INTEGER) AS DepositId,          
CCCD.numCashCreditId As CashCreditId,CAST(null as VARCHAR(255)) AS CheckId, CAST(null as NUMERIC(18,0)) as OppId, GJH.numJournal_Id AS JournalId, 0 AS MoneyIn,  1 AS MoneyOut, RT.varRecurringTemplateName as TemplateName,                    
CI.vcCompanyName as CustomerName,fn_GetAcntTypeDescription(CCCD.numCashCreditId,''CC'') As AccntType,                
Case When CCCD.bitMoneyOut = false then ''Cash'' Else ''Credit Card Details'' End  as TransactionType,                    
case when chrIntervalType = ''D'' then ''Daily, ''  when chrIntervalType = ''M'' then ''Monthly , ''                    
When chrIntervalType = ''Y'' then ''Yearly , '' end as IntervalType,                     
CCCD.monAmount as monAmount,RT.dtStartDate As StartDate, RT.dtEndDate  As EndDate                        
From RecurringTemplate RT                        
inner join CashCreditCardDetails CCCD on RT.numRecurringId = CCCD.numRecurringId            
inner join General_Journal_Header GJH on CCCD.numCashCreditId = GJH.numCashCreditCardId              
Left outer join DivisionMaster as DM on CCCD.numPurchaseId = DM.numDivisionID                                                                                     
Left outer join CompanyInfo as CI on  DM.numCompanyID = CI.numCompanyId           
Where RT.numDomainId =/** EDynExp 2 **/ ?2?/** EDynId 1 2 **/ /** SDynExp 3 **/ Union All                        
Select                         
RT.numRecurringId As RecurringId, fn_GetContactName(CD.numCreatedBy) As CreatedBy, CD.dtCreatedDate+CAST(?3? || ''minute'' as interval) As CreatedDate,CAST(null as INTEGER) AS DepositId,                 
CAST(null as NUMERIC(18,0)) AS CashCreditId,CD.numCheckId As  CheckId, CAST(null as NUMERIC(18,0)) as OppId, GJH.numJournal_Id AS JournalId, 0 AS MoneyIn,1 AS MoneyOut, RT.varRecurringTemplateName As TemplateName,                
CI.vcCompanyName as CustomerName,                    
fn_GetAcntTypeDescription(CD.numCheckId,''C'') As AccntType,CAST(''Checks'' AS VARCHAR(19)) as TransactionType,case when chrIntervalType = ''D'' then ''Daily, ''                   
when chrIntervalType = ''M'' then ''Monthly , '' When chrIntervalType = ''Y'' then ''Yearly,'' end as IntervalType,                    
CD.monCheckAmt as monAmount,RT.dtStartDate As StartDate, RT.dtEndDate  As EndDate                  
From RecurringTemplate RT                        
Inner join CheckDetails CD on RT.numRecurringId = CD.numRecurringId                  
inner join General_Journal_Header GJH on CD.numCheckId = GJH.numCheckId                
Left outer join DivisionMaster as DM on CD.numCustomerId = DM.numDivisionID                                                                                     
Left outer join CompanyInfo as CI on  DM.numCompanyID = CI.numCompanyId                     
Where RT.numDomainId =/** EDynExp 4 **/ ?4?/** EDynId 1 4 **/ /** SDynExp 5 **/ Union All                        
Select                         
RT.numRecurringId As RecurringId,fn_GetContactName(GJH.numCreatedBy) As CreatedBy,GJH.datCreatedDate+CAST(?5? || ''minute'' as interval) As CreatedDate, CAST(null as INTEGER) AS DepositId, CAST(null as NUMERIC(18,0)) As CashCreditId,                
CAST(null as VARCHAR(255)) As CheckId, CAST(null as NUMERIC(18,0)) as OppId,GJH.numJournal_Id As JournalId, 0 AS MoneyIn,0 AS MoneyOut, RT.varRecurringTemplateName as TemplateName,CAST(null as VARCHAR(100)) as CustomerName,                
fn_GetAcntTypeDescription(GJH.numJournal_Id,''JD'') As AccntType,                      
CAST(''Journal'' AS VARCHAR(19)) as TransactionType,case when chrIntervalType = ''D'' then ''Daily, ''  when chrIntervalType = ''M'' then ''Monthly , '' When chrIntervalType = ''Y'' then ''Yearly,''                      
end as IntervalType, CAST(GJH.numAmount AS DECIMAL(19,4)) as monAmount, RT.dtStartDate As StartDate, RT.dtEndDate  As EndDate                        
From RecurringTemplate RT                        
Inner join General_Journal_Header GJH on RT.numRecurringId = GJH.numRecurringId Where RT.numDomainId =/** EDynExp 6 **/ ?6?/** EDynId 1 6 **/ /** SDynExp 7 **/ Union All            
 Select                         
RT.numRecurringId As RecurringId,fn_GetContactName(Opp.numCreatedBy) As CreatedBy,Opp.bintCreatedDate+CAST(?7? || ''minute'' as interval) As CreatedDate,                
CAST(null as INTEGER) As DepositId, CAST(null as NUMERIC(18,0)) AS CashCreditId,CAST(null as VARCHAR(255)) AS CheckId,Opp.numOppId as OppId, GJH.numJournal_Id AS JournalId, 0 AS MoneyIn, 0 AS MoneyOut,RT.varRecurringTemplateName as TemplateName,CI.vcCompanyName as CustomerName,fn_GetAcntTypeDescription(Opp.numOppId,''Opp'') As
 AccntType,                      
CAST(''Opportunity'' AS VARCHAR(19)) as TransactionType,case when chrIntervalType = ''D'' then ''Daily, ''    when chrIntervalType = ''M'' then ''Monthly , ''  When chrIntervalType = ''Y'' then ''Yearly , ''                  
end as IntervalType,CAST(Opp.monPAmount AS DECIMAL(19,4)) As monAmount,RT.dtStartDate As StartDate, RT.dtEndDate  As EndDate                        
From OpportunityMaster Opp 
LEFT OUTER JOIN OpportunityRecurring OPR ON OPR.numOppId = Opp.numOppId
inner join RecurringTemplate RT on RT.numRecurringId = OPR.numRecurringId
inner join General_Journal_Header GJH on Opp.numOppId = GJH.numOppId And GJH.numBizDocsPaymentDetId is null  
Left outer join DivisionMaster as DM on Opp.numDivisionId = DM.numDivisionID                                                                                     
Left outer join CompanyInfo as CI on  DM.numCompanyID = CI.numCompanyId                 
Where RT.numDomainId =' || SUBSTR(CAST(v_numDomainId AS VARCHAR(5)),1,5);
      If v_varCustomerOrVendorSearch <> '' then
         v_strSql := coalesce(v_strSql,'') || ' And  CI.vcCompanyName like ''' || SUBSTR(CAST(v_varCustomerOrVendorSearch AS VARCHAR(500)),1,500) || '%''';
      end if;
      RAISE NOTICE '@strSql%',CAST(LENGTH(v_strSql) AS VARCHAR(10));
      RAISE NOTICE '%',v_strSql;
      EXECUTE v_strSql;
   end if;                        
   If v_numFilterBy = 1 then

      v_strSql := ' Select                         
RT.numRecurringId As RecurringId,dbo.fn_GetContactName(CCCD.numCreatedBy) As CreatedBy, DateAdd(minute,' || CAST(-v_ClientTimeZoneOffset AS VARCHAR(10)) || ',CCCD.dtCreatedDate) As CreatedDate, Null DepositId,                 
CCCD.numCashCreditId As CashCreditId,Null CheckId,Null as OppId, GJH.numJournal_Id JournalId, 0 MoneyIn,1 MoneyOut, RT.varRecurringTemplateName as TemplateName,                
CI.vcCompanyName as CustomerName,dbo.fn_GetAcntTypeDescription(CCCD.numCashCreditId,''CC'') As AccntType,                      
Case When CCCD.bitMoneyOut=false then ''Cash'' Else ''Credit Card Details'' End as TransactionType,case when chrIntervalType=''D'' then ''Daily, ''    when chrIntervalType=''M'' then ''Monthly , ''  When chrIntervalType=''Y'' then ''Yearly , ''               
  
end as IntervalType,CCCD.monAmount As monAmount,RT.dtStartDate As StartDate, RT.dtEndDate  As EndDate                        
From RecurringTemplate RT                        
inner join CashCreditCardDetails CCCD on RT.numRecurringId =CCCD.numRecurringId             
inner join General_Journal_Header GJH on CCCD.numCashCreditId=GJH.numCashCreditCardId                     
Left outer join DivisionMaster as DM on CCCD.numPurchaseId=DM.numDivisionID                                                                                     
Left outer join CompanyInfo as CI on  DM.numCompanyID=CI.numCompanyId         
 Where RT.numDomainId=' || SUBSTR(CAST(v_numDomainId AS VARCHAR(5)),1,5);
      If v_varCustomerOrVendorSearch <> '' then
         v_strSql := coalesce(v_strSql,'') || ' And CI.vcCompanyName like ''' || SUBSTR(CAST(v_varCustomerOrVendorSearch AS VARCHAR(500)),1,500) || '%''';
      end if;
      v_strSql := coalesce(v_strSql,'') || ' Union All                        
Select                         
RT.numRecurringId As RecurringId,dbo.fn_GetContactName(CD.numCreatedBy) As CreatedBy,DateAdd(minute,' || CAST(-v_ClientTimeZoneOffset AS VARCHAR(10)) || '/** SDynId 1 10 **/ /** SDynExp 1 **/  Select                         
RT.numRecurringId As RecurringId,fn_GetContactName(CCCD.numCreatedBy) As CreatedBy, CCCD.dtCreatedDate+CAST(?1? || ''minute'' as interval) As CreatedDate, CAST(null as INTEGER) AS DepositId,                 
CCCD.numCashCreditId As CashCreditId,CAST(null as VARCHAR(255)) AS CheckId,CAST(null as INTEGER) as OppId, GJH.numJournal_Id AS JournalId, 0 AS MoneyIn,1 AS MoneyOut, RT.varRecurringTemplateName as TemplateName,                
CI.vcCompanyName as CustomerName,fn_GetAcntTypeDescription(CCCD.numCashCreditId,''CC'') As AccntType,                      
Case When CCCD.bitMoneyOut = false then ''Cash'' Else ''Credit Card Details'' End as TransactionType,case when chrIntervalType = ''D'' then ''Daily, ''    when chrIntervalType = ''M'' then ''Monthly , ''  When chrIntervalType = ''Y'' then ''Yearly , ''               
  
end as IntervalType,CCCD.monAmount As monAmount,RT.dtStartDate As StartDate, RT.dtEndDate  As EndDate                        
From RecurringTemplate RT                        
inner join CashCreditCardDetails CCCD on RT.numRecurringId = CCCD.numRecurringId             
inner join General_Journal_Header GJH on CCCD.numCashCreditId = GJH.numCashCreditCardId                     
Left outer join DivisionMaster as DM on CCCD.numPurchaseId = DM.numDivisionID                                                                                     
Left outer join CompanyInfo as CI on  DM.numCompanyID = CI.numCompanyId         
 Where RT.numDomainId =/** EDynExp 2 **/ ?2?/** EDynId 1 10 **/ /** SDynExp 3 **/  Union All                        
Select                         
RT.numRecurringId As RecurringId,fn_GetContactName(CD.numCreatedBy) As CreatedBy,CD.dtCreatedDate+CAST(?3? || ''minute'' as interval) As CreatedDate, CAST(null as INTEGER) AS DepositId, CAST(null as NUMERIC(18,0)) AS CashCreditId,                
CD.numCheckId As  CheckId,CAST(null as INTEGER) as OppId, GJH.numJournal_Id AS JournalId, 0 AS MoneyIn,1 AS MoneyOut, RT.varRecurringTemplateName As TemplateName,CI.vcCompanyName as CustomerName,                 
fn_GetAcntTypeDescription(CD.numCheckId,''C'') As AccntType,                       
CAST(''Checks'' AS VARCHAR(19)) as TransactionType,case when chrIntervalType = ''D'' then ''Daily, ''   when chrIntervalType = ''M'' then ''Monthly , ''  When chrIntervalType = ''Y'' then                    
''Yearly,'' end  as IntervalType,CD.monCheckAmt as monAmount,                  
RT.dtStartDate As StartDate, RT.dtEndDate  As EndDate                        
From RecurringTemplate RT                        
inner join CheckDetails CD on RT.numRecurringId = CD.numRecurringId             
inner join General_Journal_Header GJH on CD.numCheckId = GJH.numCheckId                       
Left outer join DivisionMaster as DM on CD.numCustomerId = DM.numDivisionID                                                                                     
Left outer join CompanyInfo as CI on  DM.numCompanyID = CI.numCompanyId          
 Where RT.numDomainId =' || SUBSTR(CAST(v_numDomainId AS VARCHAR(5)),1,5);
      If v_varCustomerOrVendorSearch <> '' then
         v_strSql := coalesce(v_strSql,'') || ' And CI.vcCompanyName like ''' || SUBSTR(CAST(v_varCustomerOrVendorSearch AS VARCHAR(500)),1,500) || '%''';
      end if;
      RAISE NOTICE '%',v_strSql;
      EXECUTE v_strSql;
   end if;                        
                  
          
   If v_numFilterBy = 2 then

      v_strSql := ' Select                         
RT.numRecurringId As RecurringId,dbo.fn_GetContactName(DD.numCreatedBy) As CreatedBy,DateAdd(minute,' || CAST(-v_ClientTimeZoneOffset AS VARCHAR(10)) || ', DD.dtCreationDate) As CreatedDate,                
DD.numDepositId As DepositId, Null CashCreditId,Null CheckId,Null as OppId, GJH.numJournal_Id JournalId, 1 MoneyIn,0 MoneyOut, RT.varRecurringTemplateName as TemplateName,                
NULL as CustomerName,dbo.fn_GetAcntTypeDescription(DD.numDepositId,''DD'') As AccntType,                      
''Deposit'' as TransactionType,case when chrIntervalType=''D'' then ''Daily, ''    when chrIntervalType=''M'' then ''Monthly , ''  When chrIntervalType=''Y'' then ''Yearly , ''                  
end as IntervalType,DD.monDepositAmount As monAmount,RT.dtStartDate As StartDate, RT.dtEndDate  As EndDate                        
From RecurringTemplate RT                        
inner join DepositDetails DD on RT.numRecurringId =DD.numRecurringId             
inner join General_Journal_Header GJH on DD.numDepositId=GJH.numDepositId                     
----Left outer join DivisionMaster as DM on CCCD.numPurchaseId=DM.numDivisionID                                                                                     
----Left outer join CompanyInfo as CI on  DM.numCompanyID=CI.numCompanyId            
Where RT.numDomainId=' || SUBSTR(CAST(v_numDomainId AS VARCHAR(5)),1,5);
      RAISE NOTICE '%',v_strSql;
      EXECUTE v_strSql;
   end if;              
                
   If  v_numFilterBy = 3 Or v_numFilterBy = 4 then

      v_strSql := 'Select                        
RT.numRecurringId As RecurringId,dbo.fn_GetContactName(CCCD.numCreatedBy) As CreatedBy,DateAdd(minute,' || CAST(-v_ClientTimeZoneOffset AS VARCHAR(10)) || ',CCCD.dtCreatedDate) As CreatedDate,   Null DepositId,              
 CCCD.numCashCreditId As CashCreditId,Null CheckId,Null as OppId, GJH.numJournal_Id JournalId, 0 MoneyIn,1 MoneyOut, RT.varRecurringTemplateName as TemplateName,                    
CI.vcCompanyName as CustomerName,dbo.fn_GetAcntTypeDescription(CCCD.numCashCreditId,''CC'') As AccntType, Case When CCCD.bitMoneyOut=false then ''Cash'' Else ''Credit Card Details'' End  as TransactionType,                    
case when chrIntervalType=''D'' then ''Daily, ''  when chrIntervalType=''M'' then ''Monthly , ''                    
When chrIntervalType=''Y'' then ''Yearly , '' end as IntervalType, CCCD.monAmount as MonAmount,                     
RT.dtStartDate As StartDate, RT.dtEndDate  As EndDate                        
From RecurringTemplate RT                        
inner join CashCreditCardDetails CCCD on RT.numRecurringId=CCCD.numRecurringId             
inner join General_Journal_Header GJH on CCCD.numCashCreditId=GJH.numCashCreditCardId                        
Left outer join DivisionMaster as DM on CCCD.numPurchaseId=DM.numDivisionID                               
Left outer join CompanyInfo as CI on  DM.numCompanyID=CI.numCompanyId Where RT.numDomainId=' || SUBSTR(CAST(v_numDomainId AS VARCHAR(5)),1,5);
      If v_numAcntType <> 0 then
         v_strSql := coalesce(v_strSql,'') || ' And  CCCD.numAcntTypeId=' || SUBSTR(CAST(v_numAcntType AS VARCHAR(10)),1,10);
      end if;
      If v_ChrInterval <> 'A' then
         v_strSql := coalesce(v_strSql,'') || ' And RT.chrIntervalType = ''' || SUBSTR(CAST(v_ChrInterval AS VARCHAR(10)),1,10) || '''';
      end if;
      If v_varCustomerOrVendorSearch <> '' And  v_numAcntType <> 0 then
         v_strSql := coalesce(v_strSql,'') || ' And CI.vcCompanyName like ''' || SUBSTR(CAST(v_varCustomerOrVendorSearch AS VARCHAR(500)),1,500) || '--''';
      ELSEIF v_varCustomerOrVendorSearch <> '' And v_ChrInterval <> 'A'
      then
         v_strSql := coalesce(v_strSql,'') || ' And CI.vcCompanyName ilike ''' || SUBSTR(CAST(v_varCustomerOrVendorSearch AS VARCHAR(500)),1,500) || '%''';
      ELSEIF v_varCustomerOrVendorSearch <> '' And v_ChrInterval = 'A' And v_numAcntType = 0
      then
         v_strSql := coalesce(v_strSql,'') || ' And  CI.vcCompanyName like ''' || SUBSTR(CAST(v_varCustomerOrVendorSearch AS VARCHAR(500)),1,500) || '--''';
      end if;
      v_strSql := coalesce(v_strSql,'') || ' Union All                        
Select RT.numRecurringId As RecurringId,dbo.fn_GetContactName(CD.numCreatedBy)As CreatedBy,DateAdd(minute,' || CAST(-v_ClientTimeZoneOffset AS VARCHAR(10)) || ',CD.dtCreatedDate) As CreatedDate,Null DepositId,          
Null CashCreditId,CD.numCheckId As  CheckId,Null as OppId, GJH.numJournal_Id JournalId, 0 MoneyIn, 1MoneyOut, RT.varRecurringTemplateName As TemplateName,             
CI.vcCompanyName as CustomerName, dbo.fn_GetAcntTypeDescription(CD.numCheckId,''C'') As AccntType, ''Checks'' as TransactionType,                    
case when chrIntervalType=''D'' then ''Daily, ''   when chrIntervalType=''M'' then ''Monthly , ''                    
When chrIntervalType=''Y'' then ''Yearly,'' end as IntervalType, CD.monCheckAmt as monAmount,                     
RT.dtStartDate As StartDate, RT.dtEndDate  As EndDate                        
From RecurringTemplate RT                        
inner join CheckDetails CD on RT.numRecurringId =CD.numRecurringId               
inner join General_Journal_Header GJH on CD.numCheckId=GJH.numCheckId                    
Left outer join DivisionMaster as DM on CD.numCustomerId=DM.numDivisionID                                                                                     
Left outer join CompanyInfo as CI on  DM.numCompanyID=CI.numCompanyId Where RT.numDomainId=' || SUBSTR(CAST(v_numDomainId AS VARCHAR(5)),1,5);
      If v_numAcntType <> 0 then
         v_strSql := coalesce(v_strSql,'') || ' And CD.numAcntTypeId=' || SUBSTR(CAST(v_numAcntType AS VARCHAR(10)),1,10);
      end if;
      If v_ChrInterval <> 'A' then
         v_strSql := coalesce(v_strSql,'') || ' And RT.chrIntervalType = ''' || SUBSTR(CAST(v_ChrInterval AS VARCHAR(10)),1,10) || '''';
      end if;
      If v_varCustomerOrVendorSearch <> '' And  v_numAcntType <> 0 then
         v_strSql := coalesce(v_strSql,'') || ' And CI.vcCompanyName like ''' || SUBSTR(CAST(v_varCustomerOrVendorSearch AS VARCHAR(500)),1,500) || '--''';
      ELSEIF v_varCustomerOrVendorSearch <> '' And v_ChrInterval <> 'A'
      then
         v_strSql := coalesce(v_strSql,'') || ' And CI.vcCompanyName ilike ''' || SUBSTR(CAST(v_varCustomerOrVendorSearch AS VARCHAR(500)),1,500) || '%''';
      ELSEIF v_varCustomerOrVendorSearch <> '' And v_ChrInterval = 'A' And v_numAcntType = 0
      then
         v_strSql := coalesce(v_strSql,'') || ' And CI.vcCompanyName like ''' || SUBSTR(CAST(v_varCustomerOrVendorSearch AS VARCHAR(500)),1,500) || '--''';
      end if;
      v_strSql := coalesce(v_strSql,'') || ' Union All Select                         
RT.numRecurringId As RecurringId,dbo.fn_GetContactName(DD.numCreatedBy) As CreatedBy,DateAdd(minute,' || CAST(-v_ClientTimeZoneOffset AS VARCHAR(10)) || ',DD.dtCreationDate) As CreatedDate,                
DD.numDepositId As DepositId, Null CashCreditId,Null CheckId,Null as OppId, GJH.numJournal_Id JournalId, 1 MoneyIn,0 MoneyOut, RT.varRecurringTemplateName as TemplateName,                
NULL as CustomerName,dbo.fn_GetAcntTypeDescription(DD.numDepositId,''DD'') As AccntType,                      
''Deposit'' as TransactionType,case when chrIntervalType=''D'' then ''Daily, ''    when chrIntervalType=''M'' then ''Monthly , ''  When chrIntervalType=''Y'' then ''Yearly , ''                  
end as IntervalType,DD.monDepositAmount As monAmount,RT.dtStartDate As StartDate, RT.dtEndDate  As EndDate                        
From RecurringTemplate RT                        
inner join DepositDetails DD on RT.numRecurringId =DD.numRecurringId             
inner join General_Journal_Header GJH on DD.numDepositId=GJH.numDepositId                     
----Left outer join DivisionMaster as DM on CCCD.numPurchaseId=DM.numDivisionID                                                                                     
----Left outer join CompanyInfo as CI on  DM.numCompanyID=CI.numCompanyId               
Where RT.numDomainId=' || SUBSTR(CAST(v_numDomainId AS VARCHAR(5)),1,5);
      If v_numAcntType <> 0 then
         v_strSql := coalesce(v_strSql,'') || ' And DD.numChartAcntId=' || SUBSTR(CAST(v_numAcntType AS VARCHAR(10)),1,10);
      end if;
      If v_ChrInterval <> 'A' then
         v_strSql := coalesce(v_strSql,'') || ' And RT.chrIntervalType=''' || SUBSTR(CAST(v_ChrInterval AS VARCHAR(10)),1,10) || '''';
      end if;
      RAISE NOTICE '%',v_strSql;
      EXECUTE v_strSql;
   end if;
   RETURN;
END; $$;



