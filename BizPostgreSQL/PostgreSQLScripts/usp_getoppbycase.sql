-- Stored procedure definition script USP_GetOppByCase for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetOppByCase(v_numCaseId NUMERIC,  
v_numDomainId NUMERIC,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for select cast(c.numoppid as VARCHAR(255)) ,M.vcpOppName
   from CaseOpportunities c
   join OpportunityMaster M
   on c.numoppid = M.numOppId
   where c.numCaseId = v_numCaseId and c.numDomainid = v_numDomainId  and tintopptype = 1;
END; $$;












