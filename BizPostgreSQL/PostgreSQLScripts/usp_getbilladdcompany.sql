-- Stored procedure definition script USP_GetBillAddCompany for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_GetBillAddCompany(v_numDivisionId NUMERIC(9,0),        
v_numContactID NUMERIC(9,0),    
v_numDomainID NUMERIC(9,0) DEFAULT 0, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
BEGIN
   if   v_numDivisionId > 0 then

      open SWV_RefCur for
      select
      vcCompanyName,
   AD1.vcStreet AS vcBillstreet,
   AD1.vcCity AS vcBillCity,
   AD1.numState AS vcBilState,
   AD1.vcPostalCode AS vcBillPostCode,
   AD1.numCountry AS vcBillCountry,
   AD2.vcStreet AS vcShipStreet,
   AD2.vcCity AS vcShipCity,
   AD2.numState AS vcShipState,
   AD2.vcPostalCode AS vcShipPostCode,
   AD2.numCountry AS vcShipCountry,
   tintBillingTerms,
   numBillingDays,
   tintInterestType,
   fltInterest,
   vcWebSite,
   coalesce(fn_GetState(AD1.numState),'-') as BillState,
   fn_GetListItemName(AD1.numCountry) as BillCountry,
   D.vcComPhone
      from DivisionMaster D
      Join CompanyInfo C
      on C.numCompanyId = D.numCompanyID
      LEFT JOIN AddressDetails AD1 ON AD1.numDomainID = D.numDomainID
      AND AD1.numRecordID = D.numDivisionID AND AD1.tintAddressOf = 2 AND AD1.tintAddressType = 1 AND AD1.bitIsPrimary = true
      LEFT JOIN AddressDetails AD2 ON AD2.numDomainID = D.numDomainID
      AND AD2.numRecordID = D.numDivisionID AND AD2.tintAddressOf = 2 AND AD2.tintAddressType = 2 AND AD2.bitIsPrimary = true
      where
      D.numDivisionID = v_numDivisionId   and D.numDomainID = v_numDomainID;
   ELSEIF v_numContactID > 0
   then

      open SWV_RefCur for
      select
      vcFirstName,vcLastname,vcEmail,numPhone as vcPhone, vcCompanyName,
   AD1.vcStreet AS vcBillstreet,
   AD1.vcCity AS vcBillCity,
   AD1.numState AS vcBilState,
   AD1.vcPostalCode AS vcBillPostCode,
   AD1.numCountry AS vcBillCountry,
   AD2.vcStreet AS vcShipStreet,
   AD2.vcCity AS vcShipCity,
   AD2.numState AS vcShipState,
   AD2.vcPostalCode AS vcShipPostCode,
   AD2.numCountry AS vcShipCountry,
   tintBillingTerms,
   numBillingDays,
   tintInterestType,
   fltInterest,
   coalesce(vcWebSite,'') as vcWebSite,coalesce(AD.vcStreet,'') as vcStreet ,
   coalesce(AD.vcCity,'') as vcCity ,coalesce(AD.vcPostalCode,'') as vcPPostalCode ,
   coalesce(AD.numState,0) as numState,coalesce(AD.numCountry,0) as numCountry,
   coalesce(fn_GetState(AD1.numState),'-') as BillState,
   fn_GetListItemName(AD1.numCountry) as BillCountry,
   coalesce(fn_GetState(AD.numState),'-') as PState,
   fn_GetListItemName(AD.numCountry) as PCountry,
   D.vcComPhone
      from
      DivisionMaster D
      Join CompanyInfo C
      on C.numCompanyId = D.numCompanyID
      join AdditionalContactsInformation A
      on A.numDivisionId = D.numDivisionID
      LEFT JOIN AddressDetails AD ON AD.numDomainID = D.numDomainID
      AND AD.numRecordID = A.numContactId AND AD.tintAddressOf = 1 AND AD.tintAddressType = 0 AND AD.bitIsPrimary = true
      LEFT JOIN AddressDetails AD1 ON AD1.numDomainID = D.numDomainID
      AND AD1.numRecordID = D.numDivisionID AND AD1.tintAddressOf = 2 AND AD1.tintAddressType = 1 AND AD1.bitIsPrimary = true
      LEFT JOIN AddressDetails AD2 ON AD2.numDomainID = D.numDomainID
      AND AD2.numRecordID = D.numDivisionID AND AD2.tintAddressOf = 2 AND AD2.tintAddressType = 2 AND AD2.bitIsPrimary = true
      where
      A.numContactId = v_numContactID   and A.numDomainID = v_numDomainID;
   end if;
   RETURN;
END; $$;


