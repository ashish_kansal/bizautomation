-- Stored procedure definition script USP_Cases_CT for PostgreSQL
Create or replace FUNCTION USP_Cases_CT(v_numDomainID NUMERIC(18,0) DEFAULT 0,
    v_numUserCntID NUMERIC(18,0) DEFAULT 0,
    v_numRecordID NUMERIC(18,0) DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_Columns_Updated  VARCHAR(1000);
BEGIN
	--NOTE: LOGIC IS MOVED TO TRIGGER BECAUSE CHANGE TRACKING IS NOT AVAILABLE in POSTGRESQL
	open SWV_RefCur for select v_Columns_Updated;
END; $$;















