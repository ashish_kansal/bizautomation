-- Stored procedure definition script USP_MoveCopyItems for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_MoveCopyItems(v_numEmailHstrID NUMERIC(9,0)                   ,
v_numNodeId NUMERIC(9,0),
v_ModeType BOOLEAN)
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_NewEmailHstrID  INTEGER;
BEGIN
   if v_ModeType = false then
      insert into    EmailHistory(vcSubject,vcBody,bintCreatedOn,numNoofTimes,vcItemId,vcChangeKey,bitIsRead,vcSize,bitHasAttachments,
 dtReceivedOn,tintType,chrSource,vcCategory,vcBodyText,numDomainID,numUserCntId,numUid,numNodeId,vcFrom,vcTo,numEMailId)
      select vcSubject,vcBody,bintCreatedOn,numNoofTimes,vcItemId,vcChangeKey,bitIsRead,vcSize,bitHasAttachments,dtReceivedOn,tintType,
 chrSource,vcCategory,vcBodyText,numDomainID,numUserCntId,numUid ,v_numNodeId,vcFrom,vcTo,numEMailId
      from EmailHistory where numEmailHstrID = v_numEmailHstrID;
      v_NewEmailHstrID := CURRVAL('emailHistory_seq');
      insert into    EmailHStrToBCCAndCC(numEmailHstrID,vcName,tintType,numDivisionId,numEmailId)
      select v_NewEmailHstrID,vcName,tintType,numDivisionId,numEmailId from  EmailHStrToBCCAndCC where numEmailHstrID = v_numEmailHstrID;
      insert into EmailHstrAttchDtls(numEmailHstrID,vcFileName,vcAttachmentItemId,vcAttachmentType)
      select  v_NewEmailHstrID,vcFileName,vcAttachmentItemId,vcAttachmentType  from EmailHstrAttchDtls where numEmailHstrID = v_numEmailHstrID;
   else
      update EmailHistory set numNodeId = v_numNodeId  where numEmailHstrID = v_numEmailHstrID;
   end if;
   RETURN;
END; $$;


