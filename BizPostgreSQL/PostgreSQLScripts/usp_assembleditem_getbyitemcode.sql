CREATE OR REPLACE FUNCTION USP_AssembledItem_GetByItemCode(v_numDomainID NUMERIC(18,0),
	v_numItemCode NUMERIC(18,0),
	v_ClientTimeZoneOffset INTEGER
	,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT
		ID,
		tintType,
		AssembledType,
		numAssembledQty,
		monAverageCost,
		numWarehouseItemID,
		vcWarehouse,
		FormatedDateTimeFromDate(dtCreatedDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval),v_numDomainID) As dtCreatedDate,
		vcUserName
   FROM(SELECT
      ID,
			1 AS tintType,
			'Build' AS AssembledType,
			(numAssembledQty -coalesce(numDisassembledQty,0)) as numAssembledQty,
			coalesce(AI.monAverageCost,0) AS monAverageCost,
			AI.numWarehouseItemID,
			CONCAT(vcWarehouse,CASE WHEN LENGTH(WI.vcLocation) > 0 THEN CONCAT('(',WL.vcLocation,')') ELSE '' END) AS vcWarehouse,
			dtCreatedDate,
			coalesce((SELECT cast(vcUserName as VARCHAR(255)) FROM UserMaster WHERE numUserDetailId = AI.numCreatedBy),'') AS vcUserName
      FROM
      AssembledItem AI
      INNER JOIN
      WareHouseItems WI
      ON
      AI.numWarehouseItemID = WI.numWareHouseItemID
      INNER JOIN
      Warehouses W
      ON
      WI.numWareHouseID = W.numWareHouseID
      LEFT JOIN
      WarehouseLocation WL
      ON
      WI.numWLocationID = WL.numWLocationID
      WHERE
      AI.numDomainID = v_numDomainID
      AND AI.numItemCode = v_numItemCode
      UNION
      SELECT
			numWOId,
			2 AS tintType,
			'Work Order' AS AssembledType,
			WO.numQtyItemsReq  as numAssembledQty,
			coalesce(WO.monAverageCost,0) AS monAverageCost,
			WO.numWareHouseItemId,
			CONCAT(vcWarehouse,CASE WHEN LENGTH(WI.vcLocation) > 0 THEN CONCAT('(',WL.vcLocation,')') ELSE '' END) AS vcWarehouse,
			WO.bintCreatedDate as dtCreatedDate,
			coalesce((SELECT cast(vcUserName as VARCHAR(255)) FROM UserMaster WHERE numUserDetailId = WO.numCreatedBy),'') AS vcUserName
      FROM
      WorkOrder WO
      INNER JOIN
      WareHouseItems WI
      ON
      WO.numWareHouseItemId = WI.numWareHouseItemID
      INNER JOIN
      Warehouses W
      ON
      WI.numWareHouseID = W.numWareHouseID
      LEFT JOIN
      WarehouseLocation WL
      ON
      WI.numWLocationID = WL.numWLocationID
      WHERE
      coalesce(numOppId,0) = 0
      AND coalesce(numParentWOID,0) = 0
      AND numWOStatus = 23184
      AND numItemCode = v_numItemCode) AS TEMP
   ORDER BY
   dtCreatedDate DESC;
   RETURN;
END; $$;









