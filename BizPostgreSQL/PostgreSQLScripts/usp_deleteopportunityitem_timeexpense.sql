-- Stored procedure definition script USP_DeleteOpportunityItem_TimeExpense for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_DeleteOpportunityItem_TimeExpense(v_numDomainId NUMERIC(9,0),
    v_numOppID NUMERIC(9,0),
	v_numoppitemtCode NUMERIC(9,0))
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numCategoryHDRID  NUMERIC(9,0);
BEGIN
	select 
		numCategoryHDRID INTO v_numCategoryHDRID 
	FROM 
		OpportunityItems OPP
	Join 
		timeandexpense TE on OPP.numOppId = TE.numOppId AND OPP.numoppitemtCode = TE.numOppItemID AND OPP.numProjectID = TE.numProId where OPP.numOppId = v_numOppID and OPP.numoppitemtCode = v_numoppitemtCode;

   if v_numCategoryHDRID > 0 then
      PERFORM USP_DeleteTimExpLeave(v_numCategoryHDRID,v_numDomainId);
   end if;
   RETURN;
END; $$;




