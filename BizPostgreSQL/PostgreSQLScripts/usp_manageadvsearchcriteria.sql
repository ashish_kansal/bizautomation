-- Stored procedure definition script USP_ManageAdvSearchCriteria for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ManageAdvSearchCriteria(v_numContactID NUMERIC,
    v_numDomainID NUMERIC,
    v_numFormID NUMERIC,
    v_strItems XML,
    v_tintMode SMALLINT, INOUT SWV_RefCur refcursor)
LANGUAGE plpgsql
   AS $$
BEGIN
   IF v_tintMode = 0 then
        
      open SWV_RefCur for
      SELECT  vcFormFieldName,
                    intSearchOperator
      FROM    AdvSearchCriteria
      WHERE   numFormID = v_numFormID
      AND numContactID = v_numContactID
      AND numDomainID = v_numDomainID;
   end if;

   IF v_tintMode = 1 then
      DELETE  FROM AdvSearchCriteria
      WHERE   numContactID = v_numContactID
      AND numFormID = v_numFormID
      AND numDomainID = v_numDomainID;
      IF SUBSTR(CAST(v_strItems AS VARCHAR(10)),1,10) <> '' then
                
         INSERT  INTO AdvSearchCriteria(numDomainID,
                                                         numFormID,
                                                         vcFormFieldName,
                                                         numContactID,
                                                         intSearchOperator)
		 SELECT  v_numDomainID,
                                    v_numFormID,
                                    X.vcFormFieldName,
                                    v_numContactID,
                                    X.intSearchOperator
		FROM  XMLTABLE
		(
			'NewDataSet/Table'
			PASSING 
				v_strItems 
			COLUMNS
				id FOR ORDINALITY,
				vcFormFieldName VARCHAR(50) PATH 'vcFormFieldName'
				,intSearchOperator INTEGER PATH 'intSearchOperator'
		) AS X;
        
      end if;
      open SWV_RefCur for
      SELECT  '';
   end if;
   RETURN;
END; $$;



