-- Stored procedure definition script USP_GetSettingsVisitor for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetSettingsVisitor(INOUT SWV_RefCur refcursor)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for select * from  TrackSettingsVisitor LIMIT 1;
END; $$;












