-- Stored procedure definition script USP_GetMemReptByTeam for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetMemReptByTeam(v_numDomainId NUMERIC(9,0),    
v_numUserCntId NUMERIC(9,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for select distinct(A.numContactId),A.vcFirstName || ' ' || A.vcLastname as vcUserName from UserMaster U
   Join AdditionalContactsInformation A
   on A.numContactId = U.numUserDetailId
   join UserTeams  UT
   on UT.numUserCntID = A.numContactId
   where A.numTeam in(select cast(numTeam as NUMERIC(18,0)) from ForReportsByTeam F where F.numUserCntID = v_numUserCntId and F.numDomainID = v_numDomainId and F.tintType = 5)
   and U.numDomainID = v_numDomainId;
END; $$;












