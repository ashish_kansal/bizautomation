CREATE OR REPLACE FUNCTION USP_DeleteSimilarItem(v_numDomainID NUMERIC(9,0),
	v_numParentItemCode NUMERIC(9,0),
	v_numItemCode NUMERIC(9,0))
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
DELETE FROM SimilarItems WHERE numDomainId = v_numDomainID AND numItemCode = v_numItemCode AND numParentItemCode = v_numParentItemCode;
   RETURN;
END; $$;


