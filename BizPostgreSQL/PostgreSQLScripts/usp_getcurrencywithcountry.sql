-- Stored procedure definition script USP_GetCurrencyWithCountry for PostgreSQL
Create or replace FUNCTION USP_GetCurrencyWithCountry(v_numDomanID NUMERIC(9,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for Select CAST(C.numCurrencyID AS VARCHAR(10)) || ':' || C.varCurrSymbol || ':' || CAST(C.fltExchangeRate AS VARCHAR(20)) AS vcCurrencDetail,cast(C.numCurrencyID as VARCHAR(255)),
D.vcData || ' - ' || C.chrCurrency  AS vcCurrencyDesc
   from Currency C JOIN Listdetails D ON C.numCountryId = D.numListItemID
   where C.numDomainId = v_numDomanID and C.bitEnabled = true AND D.numDomainid = v_numDomanID AND D.numListID = 40
   order by bitEnabled desc,vcCurrencyDesc Asc;
/****** Object:  StoredProcedure [dbo].[USP_GetCustomerCreditCardInfo]    Script Date: 05/07/2009 22:00:29 ******/
   RETURN;
END; $$;












