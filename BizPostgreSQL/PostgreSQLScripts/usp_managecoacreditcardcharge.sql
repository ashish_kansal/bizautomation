-- Stored procedure definition script USP_ManageCOACreditCardCharge for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ManageCOACreditCardCharge(INOUT v_numTransChargeID NUMERIC(9,0) DEFAULT 0 ,
	v_numAccountId NUMERIC(9,0) DEFAULT NULL,
	v_fltTransactionCharge DOUBLE PRECISION DEFAULT NULL,
	v_tintBareBy SMALLINT DEFAULT 0,
	v_numCreditCardTypeId NUMERIC(9,0) DEFAULT NULL,
	v_numDomainID NUMERIC(9,0) DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   IF NOT EXISTS(SELECT * FROM  COACreditCardCharge WHERE numDomainID = v_numDomainID AND numCreditCardTypeId = v_numCreditCardTypeId) then 
      INSERT INTO COACreditCardCharge(numAccountID,
		fltTransactionCharge,
		tintBareBy,
		numCreditCardTypeId,
		numDomainID)
	VALUES(v_numAccountId,
		v_fltTransactionCharge,
		v_tintBareBy,
		v_numCreditCardTypeId,
		v_numDomainID);
	
      v_numTransChargeID := CURRVAL('COACreditCardCharge_seq');
   ELSE
      UPDATE COACreditCardCharge SET
      numAccountID = v_numAccountId,fltTransactionCharge = v_fltTransactionCharge,
      tintBareBy = v_tintBareBy,numCreditCardTypeId = v_numCreditCardTypeId
      WHERE numTransChargeID = v_numTransChargeID;
   end if;





/****** Object:  StoredProcedure [dbo].[USP_ManageCOARelationship]    Script Date: 09/25/2009 16:12:22 ******/
   RETURN;
END; $$;


