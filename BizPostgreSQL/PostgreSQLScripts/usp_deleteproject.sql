-- Stored procedure definition script Usp_DeleteProject for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION Usp_DeleteProject(v_numProId NUMERIC(9,0),  
v_numDomainID NUMERIC(9,0))
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   if exists(select * from ProjectsMaster where numProId = v_numProId and numdomainId = v_numDomainID) then

      DELETE FROM ProjectsOpportunities WHERE numProId = v_numProId AND numDomainId = v_numDomainID;
      delete FROM RecentItems where numRecordID =  v_numProId and chrRecordType = 'P';
      DELETE FROM ProjectProgress WHERE numProId = v_numProId AND numDomainID = v_numDomainID;
      delete from ProjectsStageDetails where numProId = v_numProId;        
-- delete from ProjectsSubStageDetails where numProId=@numProId              Not being used
      delete from timeandexpense where numProId = v_numProId;
      delete from ProjectsDependency where numProId = v_numProId;
      delete from ProjectsContacts where numProId = v_numProId;
      delete from ProjectsMaster where numProId = v_numProId;
   end if;
   RETURN;
END; $$;


