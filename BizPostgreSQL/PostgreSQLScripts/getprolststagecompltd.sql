-- Function definition script GetProLstStageCompltd for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION GetProLstStageCompltd(v_numProid NUMERIC(9,0),v_ClientTimeZoneOffset INTEGER)
RETURNS VARCHAR(500) LANGUAGE plpgsql
   AS $$
   DECLARE
   v_StageDetail  VARCHAR(500);  
   v_DueDate TIMESTAMP;  
   v_ComDate TIMESTAMP;  
   v_numProStageId NUMERIC;
BEGIN
   select  numProStageId INTO v_numProStageId from ProjectsStageDetails where numProId = v_numProid and numstagepercentage != 100 and bitStageCompleted = true
   and bintStageComDate =(select  max(bintStageComDate) from ProjectsStageDetails
      where numProId = v_numProid and bitStageCompleted = true LIMIT 1)     LIMIT 1;  
  
   if COALESCE(v_numProStageId,0) <> 0 then

      select vcstagedetail INTO v_StageDetail from ProjectsStageDetails where numProStageId = v_numProStageId;
      select bintDueDate INTO v_DueDate from ProjectsStageDetails where numProStageId = v_numProStageId;
      select bintStageComDate+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) INTO v_ComDate from ProjectsStageDetails where numProStageId = v_numProStageId;
      v_StageDetail := coalesce(v_StageDetail,'') || ' ,' || (CASE WHEN v_ComDate IS NOT NULL THEN TO_CHAR(v_ComDate,'mon dd yyyy HH12:MI AM') ELSE '-' END) || ' ,' || (CASE WHEN v_DueDate IS NOT NULL THEN TO_CHAR(v_DueDate,'mon dd yyyy HH12:MI AM') ELSE '-' END);
   end if;  
  
   return v_StageDetail;
END; $$;

