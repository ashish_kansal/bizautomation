CREATE OR REPLACE FUNCTION USP_GetOppItemCodesFor850(v_numOppId NUMERIC(18,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
	open SWV_RefCur for 
	SELECT 
		numoppitemtCode as "numoppitemtCode"
	FROM 
		OpportunityItems 
	WHERE 
		numOppId = v_numOppId;
END; $$;













