DROP FUNCTION IF EXISTS USP_ReOpenOppertunity;
CREATE OR REPLACE FUNCTION USP_ReOpenOppertunity(v_OppID NUMERIC(9,0),
v_numUserCntID NUMERIC(9,0),
v_numDomainID NUMERIC(9,0))
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_status  VARCHAR(2);            
   v_OppType  VARCHAR(2);  
   v_fltExchangeRate  DOUBLE PRECISION; 
			          
   v_numoppitemtCode  NUMERIC;            
   v_numUnits  DOUBLE PRECISION;              
   v_numWarehouseItemID  NUMERIC;       
   v_itemcode  NUMERIC;        
   v_QtyShipped  DOUBLE PRECISION;
   v_QtyReceived  DOUBLE PRECISION;
   v_monPrice  DECIMAL(20,5);  
   v_Kit  BOOLEAN;  
   v_bitWorkOrder  BOOLEAN;
    
  
	my_ex_state TEXT;
	my_ex_message TEXT;
	my_ex_detail TEXT;
	my_ex_hint TEXT;
	my_ex_ctx TEXT;
   v_i  INTEGER DEFAULT 1;
   v_iCount  INTEGER;
   v_numOppItemID  NUMERIC(18,0);
   SWV_RowCount INTEGER;
BEGIN
   BEGIN
	-- Added by Manish Anjara : 17th Sep,2014
	-- Check whether order is already opened or not. If already opened then show a warning message
      IF(SELECT coalesce(tintshipped,0) FROM OpportunityMaster WHERE OpportunityMaster.numOppId = v_OppID AND OpportunityMaster.numDomainId = v_numDomainID) = 0 then
	
         RAISE EXCEPTION 'ORDER_OPENED';
      end if;
      -- BEGIN TRANSACTION
select   tintoppstatus, tintopptype, (CASE WHEN coalesce(fltExchangeRate,0) = 0 THEN 1 ELSE fltExchangeRate END) INTO v_status,v_OppType,v_fltExchangeRate from OpportunityMaster where numOppId = v_OppID AND numDomainId = v_numDomainID;
      update OpportunityMaster set tintshipped = 0,bintAccountClosingDate = NULL,bintClosedDate = NULL where  numOppId = v_OppID  AND numDomainId = v_numDomainID;
      UPDATE OpportunityBizDocs SET dtShippedDate = NULL WHERE numoppid = v_OppID AND numBizDocId <> 296;
        			
			-- WE ARE REVERTING RECEIVED (PURCHASED) QTY ONLY BECAUSE USER HAS TO DELETE FULFILLMENT ORDER TO REVERSE SHIPPED (SALED) QTY
      IF cast(NULLIF(v_OppType,'') as INTEGER) = 2 then
			
				--SET Received qty to 0	
         UPDATE OpportunityItems SET numUnitHourReceived = 0,numQtyShipped = 0,numQtyReceived = 0 WHERE numOppId = v_OppID;
         UPDATE OpportunityKitItems SET numQtyShipped = 0 WHERE numOppId = v_OppID;
         BEGIN
            CREATE TEMP SEQUENCE tt_TEMP_seq INCREMENT BY 1 START WITH 1;
            EXCEPTION WHEN OTHERS THEN
               NULL;
         END;
         DROP TABLE IF EXISTS tt_TEMP CASCADE;
         CREATE TEMPORARY TABLE tt_TEMP
         (
            ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1),
            numOppItemID NUMERIC(18,0)
         );
         INSERT INTO tt_TEMP(numOppItemID)
         SELECT
         OI.numoppitemtCode
         FROM
         OpportunityItems OI
         INNER JOIN
         Item I
         ON
         OI.numItemCode = I.numItemCode
         WHERE
         numOppId = v_OppID
         AND coalesce(bitAsset,false) = true;
         select   COUNT(*) INTO v_iCount FROM tt_TEMP;
         WHILE v_i <= v_iCount LOOP
            select   numOppItemID INTO v_numOppItemID FROM tt_TEMP WHERE ID = v_i;
            PERFORM USP_ItemDepreciation_Save(v_numDomainID,v_OppID,v_numOppItemID);
            v_i := v_i::bigint+1;
         END LOOP;
      end if;
			
			-- UnArchive item based on Item's individual setting
      IF cast(NULLIF(v_OppType,'') as INTEGER) = 1 then
			
         UPDATE Item I SET IsArchieve = CASE WHEN EXISTS(SELECT OpportunityItems.numItemCode FROM OpportunityItems
         WHERE OpportunityItems.numItemCode = I.numItemCode
         AND OpportunityItems.numoppitemtCode = OI.numoppitemtCode)
         THEN false
         ELSE true
         END
         FROM OpportunityItems OI
         WHERE I.numItemCode = OI.numItemCode AND(numOppId = v_OppID
         AND I.numDomainID = v_numDomainID
         AND coalesce(I.bitArchiveItem,false) = true);
      end if;     
			----------------------------------------------------------------------------            
			--Updating Item Details            
			-----------------------------------------------------------------------            
            
			---Updating the inventory Items once the deal is won              
      select   numoppitemtCode, OI.numItemCode, coalesce(numUnitHour,0), coalesce(numQtyShipped,0), coalesce(numUnitHourReceived,0), coalesce(numWarehouseItmsID,0), coalesce(monPrice,0)*v_fltExchangeRate, (case when bitKitParent = true and bitAssembly = true then 0 when bitKitParent = true then 1 else 0 end), bitWorkOrder INTO v_numoppitemtCode,v_itemcode,v_numUnits,v_QtyShipped,v_QtyReceived,v_numWarehouseItemID,
      v_monPrice,v_Kit,v_bitWorkOrder from OpportunityItems OI join Item I
      on OI.numItemCode = I.numItemCode and numOppId = v_OppID and (bitDropShip = false or bitDropShip is null) where (charitemtype = 'P' OR 1 =(CASE WHEN cast(NULLIF(v_OppType,'') as INTEGER) = 1 THEN
         CASE WHEN coalesce(I.bitKitParent,false) = true AND coalesce(I.bitAssembly,false) = true THEN 0
         WHEN coalesce(I.bitKitParent,false) = true THEN 1
         ELSE 0 END
      ELSE 0 END))
      AND I.numDomainID = v_numDomainID   order by OI.numoppitemtCode LIMIT 1;
      while v_numoppitemtCode > 0 LOOP
         PERFORM usp_ManageInventory(v_itemcode,v_numWarehouseItemID,0,v_OppType::SMALLINT,v_numUnits,v_QtyShipped,
         v_QtyReceived,v_monPrice,3::SMALLINT,v_OppID,v_numoppitemtCode,
         v_numUserCntID,v_bitWorkOrder);
         select   numoppitemtCode, OI.numItemCode, coalesce(numUnitHour,0), coalesce(numQtyShipped,0), coalesce(numUnitHourReceived,0), coalesce(numWarehouseItmsID,0), coalesce(monPrice,0)*v_fltExchangeRate, (case when bitKitParent = true and bitAssembly = true then 0 when bitKitParent = true then 1 else 0 end) INTO v_numoppitemtCode,v_itemcode,v_numUnits,v_QtyShipped,v_QtyReceived,v_numWarehouseItemID,
         v_monPrice,v_Kit from OpportunityItems OI
         join Item I
         on OI.numItemCode = I.numItemCode and numOppId = v_OppID where (charitemtype = 'P' OR 1 =(CASE WHEN cast(NULLIF(v_OppType,'') as INTEGER) = 1 THEN
            CASE WHEN coalesce(I.bitKitParent,false) = true AND coalesce(I.bitAssembly,false) = true THEN 0
            WHEN coalesce(I.bitKitParent,false) = true THEN 1
            ELSE 0 END
         ELSE 0 END))
         and OI.numoppitemtCode > v_numoppitemtCode and (bitDropShip = false or bitDropShip is null)
         AND I.numDomainID = v_numDomainID   order by OI.numoppitemtCode LIMIT 1;
         GET DIAGNOSTICS SWV_RowCount = ROW_COUNT;
         if SWV_RowCount = 0 then 
            v_numoppitemtCode := 0;
         end if;
      END LOOP;
      IF cast(NULLIF(v_OppType,'') as INTEGER) = 2 then
			
         IF(SELECT
         COUNT(*)
         FROM
         OppWarehouseSerializedItem OWSI
         INNER JOIN
         OpportunityMaster OM
         ON
         OWSI.numOppID = OM.numOppId
         AND tintopptype = 1
         WHERE
         OWSI.numWarehouseItmsDTLID IN(SELECT numWarehouseItmsDTLID FROM OppWarehouseSerializedItem OWSIInner WHERE OWSIInner.numOppID = v_OppID)) > 0 then
				
            RAISE EXCEPTION 'SERIAL/LOT#_USED';
         ELSE
					-- REMOVE SERIAL/LOT# NUMBER FROM INVENTORY BECAUSE PURCHASE ORDER IS DELETED
            UPDATE WareHouseItmsDTL WHIDL
            SET numQty = coalesce(WHIDL.numQty,0) -coalesce(OWSI.numQty,0)
            FROM
            OppWarehouseSerializedItem OWSI, WareHouseItems INNER JOIN Item ON WareHouseItems.numItemID = Item.numItemCode
            WHERE(WHIDL.numWareHouseItmsDTLID = OWSI.numWarehouseItmsDTLID
            AND WHIDL.numWareHouseItemID = OWSI.numWarehouseItmsID) AND WHIDL.numWareHouseItemID = WareHouseItems.numWareHouseItemID AND(OWSI.numOppID = v_OppID
            AND coalesce(Item.bitLotNo,false) = true);
            DELETE FROM WareHouseItmsDTL WHIDL using OppWarehouseSerializedItem OWSI, WareHouseItems INNER JOIN Item ON WareHouseItems.numItemID = Item.numItemCode where WHIDL.numWareHouseItmsDTLID = OWSI.numWarehouseItmsDTLID
             AND WHIDL.numWareHouseItemID = WareHouseItems.numWareHouseItemID AND WHIDL.numWareHouseItemID = OWSI.numWarehouseItmsID AND OWSI.numOppID = v_OppID
            AND coalesce(Item.bitSerialized,false) = true;
         end if;
         DELETE FROM OppWarehouseSerializedItem WHERE numOppID = v_OppID;
      end if;
      IF cast(NULLIF(v_OppType,'') as INTEGER) = 2 then
			
         DELETE FROM General_Journal_Details WHERE numJournalId IN(SELECT numJOurnal_Id FROM General_Journal_Header WHERE numDomainId = v_numDomainID AND numOppId = v_OppID AND coalesce(numOppBizDocsId,0) = 0
         AND coalesce(numBillID,0) = 0 AND coalesce(numBillPaymentID,0) = 0 AND coalesce(numPayrollDetailID,0) = 0
         AND coalesce(numCheckHeaderID,0) = 0 AND coalesce(numReturnID,0) = 0
         AND coalesce(numDepositId,0) = 0);
         DELETE FROM General_Journal_Header WHERE numDomainId = v_numDomainID AND numOppId = v_OppID AND coalesce(numOppBizDocsId,0) = 0
         AND coalesce(numBillID,0) = 0 AND coalesce(numBillPaymentID,0) = 0 AND coalesce(numPayrollDetailID,0) = 0
         AND coalesce(numCheckHeaderID,0) = 0 AND coalesce(numReturnID,0) = 0
         AND coalesce(numDepositId,0) = 0;
      end if;
      -- COMMIT
EXCEPTION WHEN OTHERS THEN
  GET STACKED DIAGNOSTICS
			  my_ex_state   = RETURNED_SQLSTATE,
			  my_ex_message = MESSAGE_TEXT,
			  my_ex_detail  = PG_EXCEPTION_DETAIL,
			  my_ex_hint    = PG_EXCEPTION_HINT,
			  my_ex_ctx     = PG_EXCEPTION_CONTEXT;

			 raise exception '% % % % %', my_ex_state, my_ex_message, my_ex_detail, my_ex_hint, my_ex_ctx;
   END;
   RETURN;
END; $$;




