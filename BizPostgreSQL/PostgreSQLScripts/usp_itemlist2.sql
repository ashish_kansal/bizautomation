-- Stored procedure definition script usp_itemlist2 for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION usp_itemlist2(v_ItemClassification NUMERIC(9,0) DEFAULT 0,
    v_KeyWord VARCHAR(1000) DEFAULT '',
    v_IsKit SMALLINT DEFAULT NULL,
    v_SortChar CHAR(1) DEFAULT '0',
    v_CurrentPage INTEGER DEFAULT NULL,
    v_PageSize INTEGER DEFAULT NULL,
    INOUT v_TotRecs INTEGER  DEFAULT NULL, 
    v_columnName VARCHAR(50) DEFAULT NULL,
    v_columnSortOrder VARCHAR(10) DEFAULT NULL,
    v_numDomainID NUMERIC(9,0) DEFAULT 0,
    v_ItemType CHAR(1) DEFAULT NULL,
    v_bitSerialized BOOLEAN DEFAULT NULL,
    v_numItemGroup NUMERIC(9,0) DEFAULT NULL,
    v_bitAssembly BOOLEAN DEFAULT NULL,
    v_numUserCntID NUMERIC(9,0) DEFAULT NULL,
    v_Where	TEXT DEFAULT NULL,
    v_IsArchive BOOLEAN DEFAULT false, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_column  VARCHAR(50);              
   v_join  VARCHAR(400);                  
   v_firstRec  INTEGER;                                        
   v_lastRec  INTEGER;                                        
   v_Nocolumns  SMALLINT;               
   v_bitLocation  BOOLEAN;  
   v_strSql  VARCHAR(8000);
   v_strWhere  VARCHAR(8000);
   v_tintOrder  SMALLINT;                                                  
   v_vcFieldName  VARCHAR(50);                                                  
   v_vcListItemType  VARCHAR(3);                                             
   v_vcListItemType1  VARCHAR(1);                                                 
   v_vcAssociatedControlType  VARCHAR(10);                                                  
   v_numListID  NUMERIC(9,0);                                                  
   v_vcDbColumnName  VARCHAR(20);                      
   v_WhereCondition  VARCHAR(2000);                       
   v_vcLookBackTableName  VARCHAR(2000);                
   v_bitCustom  BOOLEAN;                  
   v_numFieldId  NUMERIC;  
   v_bitAllowSorting  CHAR(1);   
   v_bitAllowEdit  CHAR(1);                   
                 
   v_ListRelID  NUMERIC(9,0);
   v_CustomFieldType  VARCHAR(10);
   v_fldId  VARCHAR(10);
   SWV_RowCount INTEGER;
BEGIN
   IF v_columnName = 'OnHand' then
      v_columnName := 'numOnHand';
   ELSEIF v_columnName = 'Backorder'
   then
      v_columnName := 'numBackOrder';
   ELSEIF v_columnName = 'OnOrder'
   then
      v_columnName := 'numOnOrder';
   ELSEIF v_columnName = 'OnAllocation'
   then
      v_columnName := 'numAllocation';
   ELSEIF v_columnName = 'Reorder'
   then
      v_columnName := 'numReorder';
   ELSEIF v_columnName = 'monStockValue'
   then
      v_columnName := 'sum(numOnHand) * Isnull(monAverageCost,0)';
   ELSEIF v_columnName = 'ItemType'
   then
      v_columnName := 'charItemType';
   end if;

  
   v_column := v_columnName;              
   v_join := '';           
   IF v_columnName ilike '%Cust%' then
      v_fldId := REPLACE(v_columnName,'Cust','');
      select   coalesce(vcAssociatedControlType,'') INTO v_CustomFieldType FROM View_DynamicCustomColumns WHERE numFieldId = v_fldId    LIMIT 1;
      IF coalesce(v_CustomFieldType,'') = 'SelectBox' OR coalesce(v_CustomFieldType,'') = 'ListBox' then
				
         v_join := coalesce(v_join,'') || ' left Join CFW_FLD_Values_Item CFW on CFW.RecId=item.numItemCode and CFW.fld_id= ' || coalesce(v_fldId,'') || ' ';
         v_join := coalesce(v_join,'') || ' left Join ListDetails LstCF on LstCF.numListItemID = CFW.Fld_Value  ';
         v_columnName := ' LstCF.vcData ';
      ELSE
         v_join := ' left Join CFW_FLD_Values_Item CFW on CFW.RecId=item.numItemCode and CFW.fld_id= ' || coalesce(v_fldId,'') || ' ';
         v_columnName := 'CFW.Fld_Value';
      end if;
   end if;                                                
           
                                      
   v_firstRec :=(v_CurrentPage::bigint -1)*v_PageSize::bigint;                                        
   v_lastRec :=(v_CurrentPage::bigint*v_PageSize::bigint+1);   
  
   v_Nocolumns := 0;                
 
   select   coalesce(SUM(TotalRow),0) INTO v_Nocolumns FROM(SELECT    COUNT(*) AS TotalRow
      FROM      View_DynamicColumns
      WHERE     numFormId = 21
      AND numUserCntID = v_numUserCntID
      AND numDomainID = v_numDomainID
      AND tintPageType = 1 AND coalesce(numRelCntType,0) = 0
      UNION
      SELECT    COUNT(*) AS TotalRow
      FROM      View_DynamicCustomColumns
      WHERE     numFormId = 21
      AND numUserCntID = v_numUserCntID
      AND numDomainID = v_numDomainID
      AND tintPageType = 1 AND coalesce(numRelCntType,0) = 0) TotalRows;
               
 
   if v_Nocolumns = 0 then

      INSERT INTO DycFormConfigurationDetails(numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom,intColumnWidth,numViewId)
      select 21,numFieldID,0,Row_number() over(order by tintRow desc),v_numDomainID,v_numUserCntID,0,1,0,intColumnWidth,0
      FROM    View_DynamicDefaultColumns
      WHERE   numFormId = 21
      AND bitDefault = 1
      AND coalesce(bitSettingField,0) = 1
      AND numDomainID = v_numDomainID
      ORDER BY tintOrder ASC;
   end if;

   select(CASE WHEN COUNT(*) > 0 THEN 1
   ELSE 0
   END) INTO v_bitLocation FROM    View_DynamicColumns WHERE   numFormId = 21
   AND numUserCntID = v_numUserCntID
   AND numDomainID = v_numDomainID
   AND tintPageType = 1
   AND bitCustom = 0
   AND coalesce(bitSettingField,0) = 1
   AND vcDbColumnName = 'vcWareHouse';
                                                                   
                                        
   v_strWhere := ' AND 1=1';
    
   v_strSql := 'select numItemCode';
  
--    SET @strSql = @strSql + ' from item                     
        
        
   IF v_bitAssembly = '1' then
      v_strWhere := coalesce(v_strWhere,'') || ' and bitAssembly= ' || SUBSTR(CAST(v_bitAssembly AS VARCHAR(2)),1,2) || '';
   ELSEIF v_IsKit = cast(NULLIF('1','') as SMALLINT)
   then
      v_strWhere := coalesce(v_strWhere,'') || ' and (bitAssembly=0 or  bitAssembly is null) and bitKitParent= ' || SUBSTR(CAST(v_IsKit AS VARCHAR(2)),1,2) || '';
   end if;               

   IF v_ItemType = 'R' then
      v_strWhere := coalesce(v_strWhere,'') || ' and numItemClassification=(select numRentalItemClass from domain where numDomainId=' || SUBSTR(CAST(v_numDomainID AS VARCHAR(20)),1,20) || ')';
   ELSEIF v_ItemType <> ''
   then
      v_strWhere := coalesce(v_strWhere,'') || ' and charItemType= ''' || coalesce(v_ItemType,'') || '''';
   end if;            

   IF v_bitSerialized = true then
      v_strWhere := coalesce(v_strWhere,'') || ' and (bitSerialized=1 or bitLotNo=1)';
   end if;                                                   
        
   IF v_SortChar <> '0' then
      v_strWhere := coalesce(v_strWhere,'') || ' and vcItemName like ''' || coalesce(v_SortChar,'') || '%''';
   end if;                                       
        
   IF v_ItemClassification <> cast(NULLIF('0','') as NUMERIC(9,0)) then
      v_strWhere := coalesce(v_strWhere,'') || ' and numItemClassification=' || SUBSTR(CAST(v_ItemClassification AS VARCHAR(15)),1,15);
   end if;    
    
   IF v_IsArchive = false then
      v_strWhere := coalesce(v_strWhere,'') || ' AND ISNULL(Item.IsArchieve,0) = 0';
   end if;
        
   IF v_KeyWord <> '' then
        
      IF POSITION('vcCompanyName' IN v_KeyWord) > 0 then
                
         v_strWhere := coalesce(v_strWhere,'') || ' and item.numItemCode in (select Vendor.numItemCode from Vendor join 
											  divisionMaster div on div.numdivisionid=Vendor.numVendorid 
											  join companyInfo com  on com.numCompanyID=div.numCompanyID  
											  WHERE Vendor.numDomainID=' || SUBSTR(CAST(v_numDomainID AS VARCHAR(15)),1,15) || '  
											  and Vendor.numItemCode= item.numItemCode and com.' || coalesce(v_KeyWord,'') || ')';
         v_join := coalesce(v_join,'') || ' LEFT JOIN divisionmaster div on numVendorid=div.numDivisionId ';
      ELSE
         v_strWhere := coalesce(v_strWhere,'') || ' and ' || coalesce(v_KeyWord,'');
      end if;
   end if;
        
   IF v_numItemGroup > 0 then
      v_strWhere := coalesce(v_strWhere,'') || ' and numItemGroup = ' || SUBSTR(CAST(v_numItemGroup AS VARCHAR(20)),1,20);
   end if;    
               
   IF v_numItemGroup = -1 then
      v_strWhere := coalesce(v_strWhere,'') || ' and numItemGroup in (select numItemGroupID from ItemGroups where numDomainID=' || SUBSTR(CAST(v_numDomainID AS VARCHAR(20)),1,20) || ')';
   end if;                                      
        
   v_strWhere := coalesce(v_strWhere,'') || coalesce(v_Where,'') || ' group by numItemCode ';  
  
   IF v_bitLocation = true then
      v_strWhere := coalesce(v_strWhere,'') || '  ,vcWarehouse';
   end if;
        
   v_join := coalesce(v_join,'') || ' LEFT JOIN WareHouseItems ON numItemID = numItemCode ';
   v_join := coalesce(v_join,'') || ' LEFT JOIN Warehouses W ON W.numWareHouseID = WareHouseItems.numWareHouseID ';
        
   IF v_columnName <> 'sum(numOnHand) * Isnull(monAverageCost,0)' then
        
      v_strWhere := coalesce(v_strWhere,'') || ',' || coalesce(v_columnName,'');
      IF POSITION('LEFT JOIN WareHouseItems' IN v_join) = 0 then
            
         v_join := coalesce(v_join,'') || ' LEFT JOIN WareHouseItems ON numItemID = numItemCode ';
      end if;
   end if;
    
  
   v_strSql := coalesce(v_strSql,'') || ' FROM Item  ' || coalesce(v_join,'') || '   
	LEFT JOIN ListDetails LD ON LD.numListItemID = Item.numShipClass
	LEFT JOIN ItemCategory IC ON IC.numItemID = Item.numItemCode
	WHERE Item.numDomainID= ' || SUBSTR(CAST(v_numDomainID AS VARCHAR(15)),1,15) || '
	AND ( WareHouseItems.numDomainID = ' || SUBSTR(CAST(v_numDomainID AS VARCHAR(15)),1,15) || ' OR WareHouseItems.numWareHouseItemID IS NULL) ' || coalesce(v_strWhere,'');
    
   v_strSql := coalesce(v_strSql,'') || ' ORDER BY ' || coalesce(v_columnName,'') || ' ' || coalesce(v_columnSortOrder,''); 
 
   RAISE NOTICE '%',v_strSql;

   BEGIN
      CREATE TEMP SEQUENCE tt_tempItemList_seq INCREMENT BY 1 START WITH 1;
      EXCEPTION WHEN OTHERS THEN
         NULL;
   END;
   DROP TABLE IF EXISTS tt_TEMPITEMLIST CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPITEMLIST 
   (
      RowNo INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1) NOT NULL,
      numItemCode NUMERIC(18,0)
   );
--	INSERT INTO #tempItemList 
   OPEN SWV_RefCur FOR EXECUTE v_strSql;
	--DELETE FROM #tempItemList WHERE RowNo NOT IN (SELECT MIN(RowNo) FROM #tempItemList GROUP BY numItemCode)

   BEGIN
      CREATE TEMP SEQUENCE tt_tempItemList1_seq INCREMENT BY 1 START WITH 1;
      EXCEPTION WHEN OTHERS THEN
         NULL;
   END;
   DROP TABLE IF EXISTS tt_TEMPITEMLIST1 CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPITEMLIST1 
   (
      RunningCount INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1) NOT NULL,
      numItemCode NUMERIC(18,0),
      TotalRowCount INTEGER
   );
--	INSERT INTO #tempItemList1 (numItemCode) SELECT numItemCode FROM #tempItemList
--	UPDATE #tempItemList1 SET TotalRowCount=(SELECT COUNT(*) FROM #tempItemList1)
--	
   v_strSql := ' CREATE TEMPORARY TABLE tt_TBLITEM AS    select                    
 min(RunningCount) as RunningCount,
 min(TotalRowCount) as TotalRowCount,I.numItemCode,I.vcItemName,I.txtItemDesc,I.charItemType,                                     
case when charItemType = ''P'' then ''Inventory Item'' when charItemType = ''N'' then ''Non Inventory Item'' when charItemType = ''S'' then ''Service'' when charItemType = ''A'' then ''Accessory'' end as ItemType,                                      
coalesce(sum(numOnHand),0) as numOnHand,                  
coalesce(sum(numOnOrder),0) as numOnOrder,                                      
coalesce(sum(numReorder),0) as numReorder,                  
coalesce(sum(numBackOrder),0) as numBackOrder,                  
coalesce(sum(numAllocation),0) as numAllocation,                  
vcCompanyName,  
CAST(coalesce(monAverageCost,0) AS DECIMAL(10,2)) AS monAverageCost ,
CAST(coalesce(monAverageCost,0)*SUM(numOnHand) AS DECIMAL(10,2)) AS monStockValue,
I.vcModelID,I.monListPrice,I.vcManufacturer,I.numBarCodeId,I.fltLength,I.fltWidth,I.fltHeight,I.fltWeight,I.vcSKU,LD.vcData AS numShipClass '; 

   IF v_bitLocation = true then
      v_strSql := coalesce(v_strSql,'') || '  ,vcWarehouse';
   end if;  

   v_strSql := coalesce(v_strSql,'') || '  FROM tt_TEMPITEMLIST1 as tblItem JOIN Item I ON tblItem.numItemCode = I.numItemCode 
left join WareHouseItems on numItemID = I.numItemCode                                  
left join DivisionMaster div  on I.numVendorID = div.numDivisionID                                    
left join CompanyInfo com on com.numCompanyId = div.numCompanyID   
left join Warehouses W  on W.numWareHouseID = WareHouseItems.numWareHouseID 
LEFT JOIN ListDetails LD ON LD.numListItemID = I.numShipClass
    where  RunningCount >' || SUBSTR(CAST(v_firstRec AS VARCHAR(15)),1,15)
   || ' and RunningCount < ' || SUBSTR(CAST(v_lastRec AS VARCHAR(15)),1,15)
   || ' group by I.numItemCode,vcCompanyName,monAverageCost,I.vcSKU,LD.vcData,I.vcItemName,    I.txtItemDesc,I.charItemType,I.vcModelID,I.monListPrice,I.vcManufacturer,    I.numBarCodeId,I.fltLength,I.fltWidth,I.fltHeight,I.fltWeight ';  
          
   IF v_bitLocation = true then
      v_strSql := coalesce(v_strSql,'') || '  ,vcWarehouse';
   end if;  
        
   v_strSql := coalesce(v_strSql,'') || ' order by RunningCount ';   
          
   v_tintOrder := 0;                                                  
   v_WhereCondition := '';                 
                   
   
   
   DROP TABLE IF EXISTS tt_TEMPFORM CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPFORM
   (
      tintOrder SMALLINT,
      vcDbColumnName VARCHAR(50),
      vcFieldName VARCHAR(50),
      vcAssociatedControlType VARCHAR(50),
      vcListItemType CHAR(3),
      numListID NUMERIC(9,0),
      vcLookBackTableName VARCHAR(50),
      bitCustomField BOOLEAN,
      numFieldId NUMERIC,
      bitAllowSorting BOOLEAN,
      bitAllowEdit BOOLEAN,
      bitIsRequired BOOLEAN,
      bitIsEmail BOOLEAN,
      bitIsAlphaNumeric BOOLEAN,
      bitIsNumeric BOOLEAN,
      bitIsLengthValidation BOOLEAN,
      intMaxLength INTEGER,
      intMinLength INTEGER,
      bitFieldMessage BOOLEAN,
      vcFieldMessage VARCHAR(500),
      ListRelID NUMERIC(9,0)
   );


          

   INSERT  INTO tt_TEMPFORM
   SELECT  tintRow+1 AS tintOrder,
                            vcDbColumnName,
                            coalesce(vcCultureFieldName,vcFieldName),
                            vcAssociatedControlType,
                            vcListItemType,
                            numListID,
                            vcLookBackTableName,
                            bitCustom,
                            numFieldId,
                            bitAllowSorting,
                            bitAllowEdit,
                            bitIsRequired,
                            bitIsEmail,
                            bitIsAlphaNumeric,
                            bitIsNumeric,
                            bitIsLengthValidation,
                            intMaxLength,
                            intMinLength,
                            bitFieldMessage,
                            vcFieldMessage AS vcFieldMessage,
                            ListRelID
   FROM    View_DynamicColumns
   WHERE   numFormId = 21
   AND numUserCntID = v_numUserCntID
   AND numDomainID = v_numDomainID
   AND tintPageType = 1
   AND coalesce(bitSettingField,0) = 1
   AND coalesce(bitCustom,0) = 0
   AND coalesce(numRelCntType,0) = 0
   UNION
   SELECT  tintRow+1 AS tintOrder,
                            vcDbColumnName,
                            vcFieldName,
                            vcAssociatedControlType,
                            '' AS vcListItemType,
                            numListID,
                            '',
                            bitCustom,
                            numFieldId,
                            bitAllowSorting,
                            bitAllowEdit,
                            bitIsRequired,
                            bitIsEmail,
                            bitIsAlphaNumeric,
                            bitIsNumeric,
                            bitIsLengthValidation,
                            intMaxLength,
                            intMinLength,
                            bitFieldMessage,
                            vcFieldMessage,
                            ListRelID
   FROM    View_DynamicCustomColumns
   WHERE   numFormId = 21
   AND numUserCntID = v_numUserCntID
   AND numDomainID = v_numDomainID
   AND tintPageType = 1
   AND coalesce(bitCustom,0) = 1
   AND coalesce(numRelCntType,0) = 0
   ORDER BY tintOrder ASC;      
            
    
            
   v_strSql := coalesce(v_strSql,'')
   || ' select TotalRowCount,RunningCount,temp.numItemCode,vcItemName,txtItemDesc,charItemType,ItemType,numOnHand, LD.vcData AS numShipClass,                  
				   numOnOrder,numReorder,numBackOrder,numAllocation,vcCompanyName,vcModelID,monListPrice,vcManufacturer,numBarCodeId,fltLength,fltWidth,fltHeight,
				   fltWeight,(Select SUM(WO.numQtyItemsReq) from WorkOrder WO where WO.numItemCode=temp.numItemCode and WO.numWOStatus=0) as WorkOrder,
				   monAverageCost, monStockValue,vcSKU';     
  
   IF v_bitLocation = true then
      v_strSql := coalesce(v_strSql,'') || '  ,vcWarehouse';
   end if;                                               

     --WHERE bitCustomField=1
   select   tintOrder+1, vcDbColumnName, vcFieldName, vcAssociatedControlType, vcListItemType, numListID, vcLookBackTableName, bitCustomField, numFieldId, bitAllowSorting, bitAllowEdit, ListRelID INTO v_tintOrder,v_vcDbColumnName,v_vcFieldName,v_vcAssociatedControlType,v_vcListItemType,
   v_numListID,v_vcLookBackTableName,v_bitCustom,v_numFieldId,
   v_bitAllowSorting,v_bitAllowEdit,v_ListRelID FROM    tt_TEMPFORM    ORDER BY tintOrder ASC LIMIT 1;            

   WHILE v_tintOrder > 0 LOOP
      IF v_bitCustom = false then
               
         RAISE NOTICE '%',v_vcDbColumnName;
         IF v_vcDbColumnName = 'vcPathForTImage' then
		   			
            v_strSql := coalesce(v_strSql,'') || ',ISNULL(II.vcPathForTImage,'''') AS [vcPathForTImage]';
            v_WhereCondition := coalesce(v_WhereCondition,'') || ' LEFT JOIN ItemImages II ON II.numItemCode = temp.numItemCode AND bitDefault = 1';
         end if;
      ELSEIF v_bitCustom = true
      then
                
         select   FLd_label, fld_type, 'Cust'
         || SUBSTR(CAST(Fld_id AS VARCHAR(10)),1,10) INTO v_vcFieldName,v_vcAssociatedControlType,v_vcDbColumnName FROM    CFW_Fld_Master WHERE   CFW_Fld_Master.Fld_id = v_numFieldId;                 
     
              
--    print @vcAssociatedControlType                
         IF v_vcAssociatedControlType = 'TextBox'
         OR v_vcAssociatedControlType = 'TextArea' then
                        
            v_strSql := coalesce(v_strSql,'') || ',CFW'  || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3)  || '.Fld_Value  [' || coalesce(v_vcFieldName,'') || '~' || coalesce(v_vcDbColumnName,'') || ']';
            v_WhereCondition := coalesce(v_WhereCondition,'') || ' left Join CFW_FLD_Values_Item CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3)
            || '                 
    on CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.Fld_Id='
            || SUBSTR(CAST(v_numFieldId AS VARCHAR(10)),1,10)
            || 'and CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3)
            || '.RecId=temp.numItemCode   ';
         ELSEIF v_vcAssociatedControlType = 'CheckBox'
         then
                            
            v_strSql := coalesce(v_strSql,'')
            || ',case when isnull(CFW'
            || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3)
            || '.Fld_Value,0)=0 then ''No'' when isnull(CFW'
            || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3)
            || '.Fld_Value,0)=1 then ''Yes'' end   ['
            || coalesce(v_vcFieldName,'') || '~' || coalesce(v_vcDbColumnName,'')
            || ']';
            v_WhereCondition := coalesce(v_WhereCondition,'')
            || ' left Join CFW_FLD_Values_Item CFW'
            || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3)
            || '             
    on CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.Fld_Id='
            || SUBSTR(CAST(v_numFieldId AS VARCHAR(10)),1,10)
            || 'and CFW'
            || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3)
            || '.RecId=temp.numItemCode   ';
         ELSEIF v_vcAssociatedControlType = 'DateField'
         then
                                
            v_strSql := coalesce(v_strSql,'')
            || ',dbo.FormatedDateFromDate(CFW'
            || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3)
            || '.Fld_Value,'
            || SUBSTR(CAST(v_numDomainID AS VARCHAR(10)),1,10)
            || ')  [' || coalesce(v_vcFieldName,'') || '~'
            || coalesce(v_vcDbColumnName,'') || ']';
            v_WhereCondition := coalesce(v_WhereCondition,'')
            || ' left Join CFW_FLD_Values_Item CFW'
            || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3)
            || '                 
    on CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.Fld_Id='
            || SUBSTR(CAST(v_numFieldId AS VARCHAR(10)),1,10)
            || 'and CFW'
            || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3)
            || '.RecId=temp.numItemCode   ';
         ELSEIF v_vcAssociatedControlType = 'SelectBox'
         then
                                    
            v_vcDbColumnName := 'Cust'
            || SUBSTR(CAST(v_numFieldId AS VARCHAR(10)),1,10);
            v_strSql := coalesce(v_strSql,'') || ',L'
            || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3)
            || '.vcData' || ' [' || coalesce(v_vcFieldName,'')
            || '~' || coalesce(v_vcDbColumnName,'') || ']';
            v_WhereCondition := coalesce(v_WhereCondition,'')
            || ' left Join CFW_FLD_Values_Item CFW'
            || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3)
            || '                 
     on CFW' || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3) || '.Fld_Id='
            || SUBSTR(CAST(v_numFieldId AS VARCHAR(10)),1,10)
            || 'and CFW'
            || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3)
            || '.RecId=temp.numItemCode    ';
            v_WhereCondition := coalesce(v_WhereCondition,'')
            || ' left Join ListDetails L'
            || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3)
            || ' on L'
            || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3)
            || '.numListItemID=CFW'
            || SUBSTR(CAST(v_tintOrder AS VARCHAR(3)),1,3)
            || '.Fld_Value';
         end if;
      end if;
             --AND bitCustomField=1
      select   tintOrder+1, vcDbColumnName, vcFieldName, vcAssociatedControlType, vcListItemType, numListID, vcLookBackTableName, bitCustomField, numFieldId, bitAllowSorting, bitAllowEdit, ListRelID INTO v_tintOrder,v_vcDbColumnName,v_vcFieldName,v_vcAssociatedControlType,v_vcListItemType,
      v_numListID,v_vcLookBackTableName,v_bitCustom,v_numFieldId,
      v_bitAllowSorting,v_bitAllowEdit,v_ListRelID FROM    tt_TEMPFORM WHERE   tintOrder > v_tintOrder -1   ORDER BY tintOrder ASC LIMIT 1;
      GET DIAGNOSTICS SWV_RowCount = ROW_COUNT;
      IF SWV_RowCount = 0 then
         v_tintOrder := 0;
      end if;
   END LOOP;                       
      
   RAISE NOTICE '%',v_bitLocation;
   v_strSql := coalesce(v_strSql,'') || ' from #tblItem temp
			LEFT JOIN ListDetails LD ON LD.vcData = temp.numShipClass AND LD.numListID=461 and LD.numDomainID = '  || SUBSTR(CAST(v_numDomainID AS VARCHAR(20)),1,20) || ' '
   || coalesce(v_WhereCondition,'')
   || ' where  RunningCount >' || SUBSTR(CAST(v_firstRec AS VARCHAR(15)),1,15)
   || ' and RunningCount < ' || SUBSTR(CAST(v_lastRec AS VARCHAR(15)),1,15)
   || ' order by RunningCount';   

   v_strSql := REPLACE(v_strSql,'|',','); 
   RAISE NOTICE '%',v_strSql;
   --EXEC ( @strSql)  

   UPDATE  tt_TEMPFORM
   SET     vcDbColumnName = CASE WHEN bitCustomField = true
   THEN vcFieldName || '~' || vcDbColumnName
   ELSE vcDbColumnName
   END; 
   open SWV_RefCur for
   SELECT * FROM tt_TEMPFORM;

    

   DROP TABLE IF EXISTS tt_TEMPITEMLIST CASCADE;
   DROP TABLE IF EXISTS tt_TEMPITEMLIST1 CASCADE;
   RETURN;
END; $$;


