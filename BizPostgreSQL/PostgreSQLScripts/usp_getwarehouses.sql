DROP FUNCTION IF EXISTS USP_GetWarehouses;

CREATE OR REPLACE FUNCTION USP_GetWarehouses(v_numDomainID NUMERIC(9,0) DEFAULT 0,        
v_numWareHouseID NUMERIC(9,0) DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
	OPEN SWV_RefCur FOR 
	SELECT
		numWareHouseID
		,(CASE WHEN AD.numAddressID IS NOT NULL THEN AD.vcStreet ELSE vcWStreet END) AS vcWStreet
		,(CASE WHEN AD.numAddressID IS NOT NULL THEN AD.vcCity ELSE vcWCity END) AS vcWCity
		,(CASE WHEN AD.numAddressID IS NOT NULL THEN AD.vcPostalCode ELSE vcWPinCode END) AS vcWPinCode
		,(CASE WHEN AD.numAddressID IS NOT NULL THEN AD.numState ELSE numWState END) AS numWState
		,(CASE WHEN AD.numAddressID IS NOT NULL THEN AD.numCountry ELSE numWCountry END) AS numWCountry
		,W.numDomainID
		,vcWareHouse
		,W.numAddressID
		,W.vcPrintNodeAPIKey
		,W.vcPrintNodePrinterID
		,(CASE WHEN AD.numAddressID IS NULL THEN '' ELSE CONCAT(coalesce(vcStreet,''),'<br/> ',coalesce(vcCity,''),', ',coalesce(fn_GetState(numState),''),
		', ',coalesce(vcPostalCode,''),'<br/> ',coalesce(fn_GetListItemName(numCountry),'')) END) AS vcAddress
		,coalesce((SELECT  vcStateCode FROM ShippingStateMaster WHERE vcStateName =(CASE WHEN AD.numState IS NOT NULL THEN(fn_GetState(AD.numState)) ELSE(fn_GetState(W.numWState)) END) LIMIT 1),'') AS StateAbbr
		,coalesce((SELECT vcCountryCode from ShippingCountryMaster where vcCountryName =(CASE WHEN AD.numCountry IS NOT NULL THEN(fn_GetListItemName(AD.numCountry)) ELSE(fn_GetListItemName(numWCountry)) END) LIMIT 1),'') AS CountryAbbr
   FROM Warehouses W
   LEFT JOIN
   AddressDetails AD
   ON
   W.numAddressID = AD.numAddressID
   WHERE
   W.numDomainID = v_numDomainID
   AND (numWareHouseID = v_numWareHouseID  OR coalesce(v_numWareHouseID,0) = 0);
END; $$;

