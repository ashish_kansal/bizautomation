-- Stored procedure definition script USP_GetRecurringPart for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_GetRecurringPart(v_numoppId NUMERIC(9,0) DEFAULT 0,  
v_numDomainId NUMERIC(9,0) DEFAULT 0,  
INOUT v_bitVisible BOOLEAN  DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   IF EXISTS(SELECT * FROM RecurrenceConfiguration WHERE numType = 1 AND numOppID = v_numoppId AND numDomainID = v_numDomainId) then
      v_bitVisible := false;
   ELSE
      v_bitVisible := true;
   end if;
   RETURN;
END; $$;


