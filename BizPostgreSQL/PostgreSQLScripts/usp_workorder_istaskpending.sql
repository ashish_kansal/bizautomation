CREATE OR REPLACE FUNCTION USP_WorkOrder_IsTaskPending(v_numDomainID NUMERIC(18,0)                            
	,v_numWOID NUMERIC(18,0), INOUT SWV_RefCur refcursor)
LANGUAGE plpgsql
   AS $$
BEGIN
	IF EXISTS(SELECT
					SPDT.numTaskId
				FROM
					StagePercentageDetailsTask SPDT
				WHERE
					SPDT.numDomainID = v_numDomainID
					AND SPDT.numWorkOrderId = v_numWOID
					AND coalesce((SELECT COUNT(*) FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskId = SPDT.numTaskId AND tintAction = 4),0) = 0) 
	THEN	
		open SWV_RefCur for
		SELECT 1;
	ELSE
		open SWV_RefCur for
		SELECT 0;
	end if;
   
	RETURN;
END; $$;


