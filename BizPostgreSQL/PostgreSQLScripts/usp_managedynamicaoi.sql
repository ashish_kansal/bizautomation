-- Stored procedure definition script USP_ManageDynamicAOI for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ManageDynamicAOI(v_strAOI TEXT DEFAULT '',
v_numDomainID NUMERIC(9,0) DEFAULT 0,
v_numFormID NUMERIC(9,0) DEFAULT 0,
v_numGroupID NUMERIC(9,0) DEFAULT 0)
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_hDoc1  INTEGER;
BEGIN
   delete from DynamicFormAOIConf where numDomainID = v_numDomainID and numFormID = v_numFormID
   and numGroupID = v_numGroupID;                                  
                                
	insert into DynamicFormAOIConf(numAOIID,numFormID,numGroupID,numDomainID)
	select 
		unnest(xpath('//numAOIID/text()', v_strAOI::xml))::text::NUMERIC,
		v_numFormID,
		v_numGroupID,
		v_numDomainID                        
                                
   RETURN;
END; $$;


