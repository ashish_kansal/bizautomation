-- Function definition script GetPriceBookItemList for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION GetPriceBookItemList(v_numRuleID NUMERIC,
      v_tintMode SMALLINT)
RETURNS VARCHAR(2000) LANGUAGE plpgsql
   AS $$
   DECLARE
   v_ItemList  VARCHAR(2000);
BEGIN
   IF v_tintMode = 1 then
        
      select string_agg(vcItemName,', ') INTO v_ItemList FROM    PriceBookRuleItems PD
      INNER JOIN Item I ON I.numItemCode = PD.numValue
      LEFT OUTER JOIN PriceBookRules PB ON PB.numPricRuleID = PD.numRuleID WHERE   PB.tintStep2 = 1
      AND PB.numPricRuleID = v_numRuleID    LIMIT 10;
   end if;
      
   IF v_tintMode = 2 then
        
      select string_agg(vcData,', ') INTO v_ItemList FROM    PriceBookRuleItems PD
      INNER JOIN Listdetails LI ON LI.numListItemID = PD.numValue
      LEFT OUTER JOIN PriceBookRules PB ON PB.numPricRuleID = PD.numRuleID WHERE   PB.tintStep2 = 2
      AND PB.numPricRuleID = v_numRuleID    LIMIT 10;
   end if;
   IF v_tintMode = 3 then
        
      select string_agg(C.vcCompanyName,', ') INTO v_ItemList FROM    PriceBookRuleDTL PD
      INNER JOIN DivisionMaster DM ON DM.numDivisionID = PD.numValue
      LEFT OUTER JOIN CompanyInfo C ON C.numCompanyId = DM.numCompanyID
      LEFT OUTER JOIN PriceBookRules PB ON PB.numPricRuleID = PD.numRuleID WHERE   PB.tintStep3 = 1
      AND PB.numPricRuleID = v_numRuleID    LIMIT 10;
   end if;
        
   IF v_tintMode = 4 then
        
      select string_agg((LI.vcData || '/' || LI1.vcData),', ') INTO v_ItemList FROM    PriceBookRuleDTL PD
      INNER JOIN Listdetails LI ON LI.numListItemID = PD.numValue
      Inner JOIN Listdetails LI1 ON LI1.numListItemID = PD.numProfile
      LEFT OUTER JOIN PriceBookRules PB ON PB.numPricRuleID = PD.numRuleID WHERE   PB.tintStep3 = 2
      AND PB.numPricRuleID = v_numRuleID    LIMIT 10;
   end if;
        
        
   RETURN coalesce(v_ItemList,'');
END; $$; 
/****** Object:  UserDefinedFunction [dbo].[GetProjectedBankBalance]    Script Date: 07/26/2008 18:13:09 ******/

