DROP FUNCTION IF EXISTS USP_ItemDetailsForEcomm;

CREATE OR REPLACE FUNCTION USP_ItemDetailsForEcomm(v_numItemCode NUMERIC(9,0),          
v_numWareHouseID NUMERIC(9,0),    
v_numDomainID NUMERIC(9,0),
v_numSiteId NUMERIC(9,0),
v_vcCookieId TEXT DEFAULT null,
v_numUserCntID NUMERIC(9,0) DEFAULT 0,
v_numDivisionID NUMERIC(18,0) DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL,INOUT SWV_RefCur2 refcursor DEFAULT NULL)
LANGUAGE plpgsql
/*Removed warehouse parameter, and taking Default warehouse by default from DB*/
    
   AS $$
   DECLARE
   v_bitShowInStock  BOOLEAN;        
   v_bitShowQuantity  BOOLEAN;
   v_bitAutoSelectWarehouse  BOOLEAN;              
   v_numDefaultWareHouseID  NUMERIC(9,0);
   v_numDefaultRelationship  NUMERIC(18,0);
   v_numDefaultProfile  NUMERIC(18,0);
   v_tintPreLoginPriceLevel  SMALLINT;
   v_tintPriceLevel  SMALLINT;
   v_bitMatrix  BOOLEAN;
   v_numItemGroup  NUMERIC(18,0);
   v_vcWarehouseIDs  VARCHAR(1000);
   v_UOMConversionFactor  DECIMAL(18,5);
      
   v_strSql1  TEXT;
   v_strSql2  TEXT;
   v_strSql3  TEXT;
   v_strSql4  TEXT;
	
   SWV_ExecDyn TEXT;
   v_tintOrder  SMALLINT;                                                  
   v_vcFormFieldName  VARCHAR(50);                                                  
   v_vcListItemType  VARCHAR(1);                                             
   v_vcAssociatedControlType  VARCHAR(50);                                                  
   v_numListID  NUMERIC(9,0);                                                  
   v_WhereCondition  VARCHAR(2000);                       
   v_numFormFieldId  NUMERIC;  
   v_vcFieldType  CHAR(1);
   v_GRP_ID  INTEGER;
   SWV_RowCount INTEGER;
BEGIN
   v_bitShowInStock := false;        
   v_bitShowQuantity := false;        
       
        
   IF coalesce(v_numDivisionID,0) > 0 then
	
      select   coalesce(numCompanyType,0), coalesce(vcProfile,0), coalesce(tintPriceLevel,0) INTO v_numDefaultRelationship,v_numDefaultProfile,v_tintPriceLevel FROM
      DivisionMaster
      INNER JOIN
      CompanyInfo
      ON
      DivisionMaster.numCompanyID = CompanyInfo.numCompanyId WHERE
      numDivisionID = v_numDivisionID
      AND DivisionMaster.numDomainID = v_numDomainID;
   end if;     
      
	--KEEP THIS BELOW ABOVE IF CONDITION 
   select   coalesce(bitShowInStock,false), coalesce(bitShowQOnHand,false), coalesce(bitAutoSelectWarehouse,false), (CASE WHEN coalesce(v_numDivisionID,0) > 0 THEN v_numDefaultRelationship ELSE numRelationshipId END), (CASE WHEN coalesce(v_numDivisionID,0) > 0 THEN v_numDefaultProfile ELSE numProfileId END), (CASE WHEN coalesce(bitShowPriceUsingPriceLevel,false) = true THEN(CASE WHEN v_numDivisionID > 0 THEN(CASE WHEN coalesce(bitShowPriceUsingPriceLevel,false) = true THEN coalesce(v_tintPriceLevel,0) ELSE 0 END) ELSE coalesce(tintPreLoginProceLevel,0) END) ELSE 0 END) INTO v_bitShowInStock,v_bitShowQuantity,v_bitAutoSelectWarehouse,v_numDefaultRelationship,
   v_numDefaultProfile,v_tintPreLoginPriceLevel FROM
   eCommerceDTL WHERE
   numSiteID = v_numSiteId;

   IF v_numWareHouseID = 0 then
	
      select   coalesce(numDefaultWareHouseID,0) INTO v_numDefaultWareHouseID FROM eCommerceDTL WHERE numDomainID = v_numDomainID;
      v_numWareHouseID := v_numDefaultWareHouseID;
   end if;

   IF v_bitAutoSelectWarehouse = true then
	
      v_vcWarehouseIDs := coalesce(OVERLAY((SELECT DISTINCT ',' || CAST(numWareHouseID AS VARCHAR(10)) FROM Warehouses WHERE Warehouses.numDomainID = v_numDomainID) placing '' from 1 for 1),'');
   ELSE
      v_vcWarehouseIDs := SUBSTR(CAST(v_numWareHouseID AS VARCHAR(1000)),1,1000);
   end if;

	/*If qty is not found in default warehouse then choose next warehouse with sufficient qty */
   IF v_bitAutoSelectWarehouse = true AND(SELECT COUNT(*) FROM WareHouseItems WHERE  numItemID = v_numItemCode AND numWareHouseID = v_numWareHouseID) = 0 then
	
      select   numWareHouseID INTO v_numDefaultWareHouseID FROM
      WareHouseItems WHERE
      numItemID = v_numItemCode
      AND numOnHand > 0   ORDER BY
      numOnHand DESC LIMIT 1;
      IF (coalesce(v_numDefaultWareHouseID,0) > 0) then
         v_numWareHouseID := v_numDefaultWareHouseID;
      end if;
   end if;

   select   coalesce(fn_UOMConversion(coalesce(I.numSaleUnit,0),I.numItemCode,I.numDomainID,coalesce(I.numBaseUnit,0)),0), coalesce(bitMatrix,false), coalesce(numItemGroup,0) INTO v_UOMConversionFactor,v_bitMatrix,v_numItemGroup FROM
   Item I WHERE
   numItemCode = v_numItemCode;   	

   v_strSql1 := CONCAT('SELECT 
								I.numItemCode
								,vcItemName
								,txtItemDesc
								,charItemType
								,COALESCE(I.bitKitParent,false) bitKitParent
								,COALESCE(bitCalAmtBasedonDepItems,false) bitCalAmtBasedonDepItems
								,COALESCE(I.bitMatrix,false) bitMatrix,(CASE WHEN (COALESCE(I.bitKitParent,false) = true OR COALESCE(I.bitAssembly,false) = true) AND COALESCE(bitCalAmtBasedonDepItems,false) = true THEN GetCalculatedPriceForKitAssembly(I.numDomainID,',v_numDivisionID,',I.numItemCode,1,0,COALESCE(I.tintKitAssemblyPriceBasedOn,1)::SMALLINT,0,0,'''',0,1) ELSE 
								',(CASE
   WHEN v_tintPreLoginPriceLevel > 0
   THEN CONCAT('COALESCE((CASE 
											WHEN PricingOption.tintRuleType = 1 AND PricingOption.tintDiscountType = 1
											THEN COALESCE((CASE WHEN I.charItemType = ''P'' THEN (',v_UOMConversionFactor,' * COALESCE(W.monWListPrice,0)) ELSE (',
      v_UOMConversionFactor,' * monListPrice) END),0) - (COALESCE((CASE WHEN I.charItemType = ''P'' THEN (',v_UOMConversionFactor,
      ' * COALESCE(W.monWListPrice,0)) ELSE (',v_UOMConversionFactor,' * monListPrice) END),0) * (PricingOption.decDiscount / 100 ) )
											WHEN PricingOption.tintRuleType = 1 AND PricingOption.tintDiscountType = 2
											THEN COALESCE((CASE WHEN I.charItemType = ''P'' THEN (',
      v_UOMConversionFactor,' * COALESCE(W.monWListPrice,0)) ELSE (',v_UOMConversionFactor,
      ' * monListPrice) END),0) - PricingOption.decDiscount
											WHEN PricingOption.tintRuleType = 2 AND PricingOption.tintDiscountType = 1
											THEN COALESCE(Vendor.monCost,0) + (COALESCE(Vendor.monCost,0) * (PricingOption.decDiscount / 100 ) )
											WHEN PricingOption.tintRuleType = 2 AND PricingOption.tintDiscountType = 2
											THEN COALESCE(Vendor.monCost,0) + PricingOption.decDiscount
											WHEN PricingOption.tintRuleType = 3
											THEN (',
      v_UOMConversionFactor,' * PricingOption.decDiscount)
										END),COALESCE(CASE WHEN I.charItemType=''P'' THEN ',v_UOMConversionFactor,' * W.monWListPrice ELSE ',
      v_UOMConversionFactor,' * monListPrice END,0))')
   ELSE CONCAT('COALESCE(CASE WHEN I.charItemType=''P'' THEN ',v_UOMConversionFactor,
      ' * W.monWListPrice ELSE ',v_UOMConversionFactor,' * monListPrice END,0)')
   END),
   'END) AS monListPrice
								,(CASE WHEN (COALESCE(I.bitKitParent,false) = true OR COALESCE(I.bitAssembly,false) = true) AND COALESCE(bitCalAmtBasedonDepItems,false) = true THEN GetCalculatedPriceForKitAssembly(I.numDomainID,',
   v_numDivisionID,',I.numItemCode,1,0,COALESCE(I.tintKitAssemblyPriceBasedOn,1)::SMALLINT,0,0,'''',0,1) ELSE COALESCE(CASE WHEN I.charItemType=''P'' 
											THEN ',
   v_UOMConversionFactor,' * W.monWListPrice  
											ELSE ',
   v_UOMConversionFactor,' * monListPrice 
										END,0) END) AS monMSRP
										,numItemClassification
										,bitTaxable
										,vcSKU
										,I.numModifiedBy
										,(CASE WHEN EXISTS (SELECT 1 FROM ItemImages WHERE ItemImages.numItemCode = I.numItemCode) THEN CONCAT(''<Images>'',(SELECT string_agg(CONCAT(''<ItemImages '',
											'' numItemImageId="'',numItemImageId,''"''
											'' vcPathForImage="'',vcPathForImage,''"''
											'' vcPathForTImage="'',vcPathForTImage,''"''
											'' bitDefault="'',(CASE WHEN bitDefault THEN 1 ELSE 0 END),''"''
											'' intDisplayOrder="'',(CASE
																	WHEN bitDefault = true THEN -1
																	ELSE coalesce(intDisplayOrder,0)
																END),''" />''),'''' ORDER BY CASE WHEN bitDefault = true THEN -1 ELSE coalesce(intDisplayOrder,0) END ASC)
										FROM
											ItemImages
										WHERE
											ItemImages.numItemCode = I.numItemCode
										),''</Images>'') ELSE NULL END) AS vcImages
										,CONCAT(''<Documents>'',(SELECT string_agg(CONCAT(''<ItemImages '',
											'' vcPathForImage="'',vcPathForImage,''" />''),'''' ORDER BY coalesce(intDisplayOrder,0) ASC)
										FROM
											ItemImages
										WHERE
											ItemImages.numItemCode = I.numItemCode
										),''</Documents>'') AS vcDocuments
										,COALESCE(bitSerialized,false) as bitSerialized
										,vcModelID
										,numItemGroup
										,(COALESCE(sum(numOnHand),0) / ',
   v_UOMConversionFactor,') as numOnHand
										,sum(numOnOrder) as numOnOrder,                    
										sum(numReorder)  as numReorder,                    
										sum(numAllocation)  as numAllocation,                    
										sum(numBackOrder)  as numBackOrder,              
										COALESCE(fltWeight,0) as fltWeight,              
										COALESCE(fltHeight,0) as fltHeight,              
										COALESCE(fltWidth,0) as fltWidth,              
										COALESCE(fltLength,0) as fltLength,              
										case when charItemType=''P''then COALESCE(bitFreeShipping,false) else true end as bitFreeShipping,
										Case When (case when charItemType=''P''then COALESCE(bitFreeShipping,false) else true end )=true then ''Free shipping'' else ''Calculated at checkout'' end as Shipping,              
										COALESCE(bitAllowBackOrder,false) as bitAllowBackOrder,bitShowInStock,bitShowQOnHand,COALESCE(numWareHouseItemID,0)  as numWareHouseItemID,
										(CASE WHEN charItemType<>''S'' AND charItemType<>''N'' AND COALESCE(bitKitParent,false)=false THEN         
										 (CASE 
											WHEN I.bitAllowBackOrder = true THEN true
											WHEN (COALESCE(WAll.numTotalOnHand,0)<=0) THEN false
											ELSE true
										END) ELSE true END ) as bitInStock,
										(
											CASE WHEN COALESCE(bitKitParent,false) = true
											THEN
												(CASE 
													WHEN ((SELECT COUNT(*) FROM ItemDetails ID INNER JOIN Item IInner ON ID.numChildItemID = IInner.numItemCode WHERE ID.numItemKitID = I.numItemCode AND COALESCE(IInner.bitKitParent,false) = true)) > 0 
													THEN 1 
													ELSE 0 
												END)
											ELSE
												0
											END
										) bitHasChildKits,
										(case when charItemType<>''S'' AND charItemType<>''N'' AND COALESCE(bitKitParent,false)=false then         
										 (Case when ',CASE WHEN v_bitShowInStock THEN '1' ELSE '0' END,
   '=1  then 
										 (CASE 
											WHEN I.bitAllowBackOrder = true AND I.numDomainID <> 172 AND COALESCE(WAll.numTotalOnHand,0)<=0 AND COALESCE(I.numVendorID,0) > 0 AND GetVendorPreferredMethodLeadTime(I.numVendorID,0) > 0 THEN CONCAT(''<font class="vendorLeadTime" style="color:green">Usually ships in '',GetVendorPreferredMethodLeadTime(I.numVendorID,0),'' days</font>'')
											WHEN I.bitAllowBackOrder = true AND COALESCE(WAll.numTotalOnHand,0)<=0 THEN (CASE WHEN I.numDomainID = 172 THEN ''<font style="color:green">AVAILABLE</font>'' ELSE ''<font style="color:green">In Stock</font>'' END)
											WHEN (COALESCE(WAll.numTotalOnHand,0)<=0) THEN (CASE WHEN I.numDomainID = 172 THEN ''<font style="color:red">ON HOLD</font>'' ELSE ''<font style="color:red">Out Of Stock</font>'' END)
											ELSE (CASE WHEN I.numDomainID = 172 THEN ''<font style="color:green">AVAILABLE</font>'' ELSE ''<font style="color:green">In Stock</font>'' END)  
										END) else '''' end) ELSE '''' END ) as InStock,
										COALESCE(numSaleUnit,0) AS numUOM,
										COALESCE(vcUnitName,'''') AS vcUOMName,
										 ',
   v_UOMConversionFactor,' AS UOMConversionFactor,
										I.numCreatedBy,
										I.numBarCodeId,
										I.vcManufacturer,
										(SELECT  COUNT(*) FROM  Review where vcReferenceId = I.numItemCode::VARCHAR and numDomainId = I.numDomainId and numSiteId = ', 
   v_numSiteId,' AND bitHide = false ) as ReviewCount ,
										(SELECT  COUNT(*) FROM  Ratings where vcReferenceId = I.numItemCode::VARCHAR and numDomainId = I.numDomainId and numSiteId = ',v_numSiteId,
   '  ) as RatingCount ,
										MT.vcPageTitle,
										MT.vcMetaKeywords,
										MT.vcMetaDescription,
										(SELECT vcCategoryName  FROM  Category WHERE numCategoryID =(SELECT numCategoryID FROM ItemCategory WHERE numItemID = I.numItemCode LIMIT 1)) as vcCategoryName
										,COALESCE(TablePromotion.numTotalPromotions,0) numTotalPromotions
										,COALESCE(TablePromotion.vcPromoDesc,'''') vcPromoDesc
										,COALESCE(TablePromotion.bitRequireCouponCode,false) bitRequireCouponCode');
   v_strSql2 := CONCAT(' from Item I 
										LEFT JOIN Vendor ON Vendor.numVendorID =I.numVendorID AND Vendor.numItemCode=I.numItemCode
										LEFT JOIN MetaTags MT ON MT.numReferenceID = I.numItemCode AND MT.tintMetaTagFor =2
										LEFT JOIN LATERAL (SELECT * FROM fn_GetItemPromotions(',v_numDomainID,
   ',',v_numDivisionID,',I.numItemCode,I.numItemClassification',',',
   v_numDefaultRelationship,',',v_numDefaultProfile,',2::SMALLINT)) TablePromotion ON TRUE ' ||(CASE WHEN v_tintPreLoginPriceLevel > 0 THEN CONCAT(' LEFT JOIN LATERAL (SELECT tintRuleType,tintDiscountType,decDiscount FROM PricingTable WHERE numItemCode=I.numItemCode AND COALESCE(numPriceRuleID,0) = 0 AND COALESCE(numCurrencyID,0) = 0 ORDER BY numPricingID OFFSET ',v_tintPreLoginPriceLevel -1,
      ' ROWS FETCH NEXT 1 ROWS ONLY) As PricingOption ON TRUE ') ELSE '' END),
   '                 
										left join  WareHouseItems W                
										on W.numItemID=I.numItemCode and numWareHouseID= ',
   v_numWareHouseID,'
										 LEFT JOIN LATERAL (SELECT SUM(numOnHand) numTotalOnHand FROM WareHouseItems W 
																							  WHERE  W.numItemID = I.numItemCode
																							  AND numWareHouseID IN (SELECT Id FROM SplitIds(''',v_vcWarehouseIDs,
   ''','',''))
																							  AND W.numDomainID = ',v_numDomainID,
   ') AS WAll ON TRUE
										left join  eCommerceDTL E          
										on E.numDomainID=I.numDomainID     
										LEFT JOIN UOM u ON u.numUOMId=I.numSaleUnit');

   v_strSql3 := CONCAT(' where COALESCE(I.IsArchieve,false)=false AND I.numDomainID=',v_numDomainID,' AND I.numItemCode=',
   v_numItemCode,CASE WHEN(SELECT COUNT(*) FROM EcommerceRelationshipProfile WHERE numSiteID = v_numSiteId AND numRelationship = v_numDefaultRelationship AND numProfile = v_numDefaultProfile) > 0
   THEN
      CONCAT(' AND I.numItemClassification NOT IN (SELECT numItemClassification FROM EcommerceRelationshipProfile ERP INNER JOIN ECommerceItemClassification EIC ON EIC.numECommRelatiionshipProfileID=ERP.ID WHERE ERP.numSiteID=',v_numSiteId,' AND ERP.numRelationship=',v_numDefaultRelationship,
      ' AND ERP.numProfile=',v_numDefaultProfile,')')
   ELSE
      ''
   END,' GROUP BY I.numItemCode, vcItemName, txtItemDesc, charItemType, monListPrice,numItemClassification, bitTaxable, vcSKU, bitKitParent, bitCalAmtBasedonDepItems, bitAssembly, tintKitAssemblyPriceBasedOn, WAll.numTotalOnHand,         
										I.numDomainID,I.numModifiedBy,  bitSerialized, vcModelID,numItemGroup, fltWeight,fltHeight,fltWidth,MT.vcPageTitle,MT.vcMetaKeywords,MT.vcMetaDescription,          
										fltLength,bitFreeShipping,bitAllowBackOrder,bitShowInStock,bitShowQOnHand ,numWareHouseItemID,vcUnitName,I.numCreatedBy,I.numBarCodeId,W.monWListPrice,I.vcManufacturer,
										numSaleUnit, I.numVendorID, I.bitMatrix,numTotalPromotions,vcPromoDesc,bitRequireCouponCode,Vendor.monCost ',(CASE WHEN v_tintPreLoginPriceLevel > 0 THEN ',PricingOption.tintRuleType,PricingOption.tintDiscountType,PricingOption.decDiscount' ELSE '' END));

   v_strSql4 := ' select numItemCode,vcItemName,txtItemDesc,charItemType, bitMatrix, monListPrice,monMSRP,numItemClassification, bitTaxable, vcSKU, bitKitParent, bitCalAmtBasedonDepItems,                  
										tblItem.numModifiedBy, vcImages,vcDocuments, bitSerialized, vcModelID, numItemGroup,numOnHand,numOnOrder,numReorder,numAllocation, 
										numBackOrder, fltWeight, fltHeight, fltWidth, fltLength, bitFreeShipping,bitAllowBackOrder,bitShowInStock,
										bitShowQOnHand,numWareHouseItemID,bitInStock,bitHasChildKits,InStock,numUOM,vcUOMName,UOMConversionFactor,tblItem.numCreatedBy,numBarCodeId,vcManufacturer,ReviewCount,RatingCount,Shipping,COALESCE(vcPageTitle,'''') vcPageTitle,COALESCE(vcMetaKeywords,'''') as vcMetaKeywords ,COALESCE(vcMetaDescription,'''') as vcMetaDescription,COALESCE(vcCategoryName,'''') as vcCategoryName,numTotalPromotions,vcPromoDesc,bitRequireCouponCode from tblItem ORDER BY numOnHand DESC';


   RAISE NOTICE '%',CAST(CONCAT('WITH tblItem AS (',v_strSql1,v_strSql2,v_strSql3,')',v_strSql4) AS TEXT);

   SWV_ExecDyn := 'WITH tblItem AS (' || coalesce(v_strSql1,'') || coalesce(v_strSql2,'') || coalesce(v_strSql3,'') || ')' || coalesce(v_strSql4,'');
   OPEN SWV_RefCur FOR EXECUTE SWV_ExecDyn;


   v_tintOrder := 0;                                                  
   v_WhereCondition := '';                 
                   
              
   DROP TABLE IF EXISTS tt_TEMPAVAILABLEFIELDS CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPAVAILABLEFIELDS
   (
      numFormFieldId NUMERIC(9,0),
      vcFormFieldName VARCHAR(50),
      vcFieldType CHAR(1),
      vcAssociatedControlType VARCHAR(50),
      numListID NUMERIC(18,0),
      vcListItemType CHAR(1),
      intRowNum INTEGER,
      vcItemValue VARCHAR(3000),
      GRP_ID INTEGER
   );
   
   INSERT INTO tt_TEMPAVAILABLEFIELDS(numFormFieldId
		,vcFormFieldName
		,vcFieldType
		,vcAssociatedControlType
        ,numListID
		,vcListItemType
		,intRowNum
		,GRP_ID)
   SELECT
   Fld_id
		,REPLACE(Fld_label,' ','') AS vcFormFieldName
		,CASE GRP_ID WHEN 4 then 'C' WHEN 1 THEN 'D' ELSE 'C' END AS vcFieldType
		,fld_type as vcAssociatedControlType
		,coalesce(C.numlistid,0) AS numListID
		,CASE WHEN C.numlistid > 0 THEN 'L' ELSE '' END AS vcListItemType
		,ROW_NUMBER() OVER(ORDER BY Fld_id asc) AS intRowNum
		,GRP_ID
   FROM
   CFW_Fld_Master C
   LEFT JOIN
   CFW_Validation V
   ON
   V.numFieldId = C.Fld_id
   WHERE
   C.numDomainID = v_numDomainID
   AND GRP_ID  = 5;

   select   intRowNum, numFormFieldId, vcFormFieldName, vcFieldType, vcAssociatedControlType, numListID, vcListItemType, GRP_ID INTO v_tintOrder,v_numFormFieldId,v_vcFormFieldName,v_vcFieldType,v_vcAssociatedControlType,
   v_numListID,v_vcListItemType,v_GRP_ID FROM
   tt_TEMPAVAILABLEFIELDS    ORDER BY
   intRowNum ASC LIMIT 1;
   
   while v_tintOrder > 0 LOOP
      IF v_GRP_ID = 5 then
		
         IF v_vcAssociatedControlType = 'TextBox' or v_vcAssociatedControlType = 'TextArea' then
			
            UPDATE tt_TEMPAVAILABLEFIELDS SET vcItemValue =(SELECT Fld_Value FROM CFW_FLD_Values_Item WHERE Fld_ID = v_numFormFieldId AND RecId = v_numItemCode) WHERE numFormFieldId = v_numFormFieldId;
         ELSEIF v_vcAssociatedControlType = 'CheckBox'
         then
			
            UPDATE tt_TEMPAVAILABLEFIELDS SET vcItemValue =(SELECT case when coalesce(Fld_Value,'0') = '0' then 'No' when coalesce(Fld_Value,'0') = '1' then 'Yes' END FROM CFW_FLD_Values_Item WHERE Fld_ID = v_numFormFieldId AND RecId = v_numItemCode) WHERE numFormFieldId = v_numFormFieldId;
         ELSEIF v_vcAssociatedControlType = 'DateField'
         then
			IF is_date((SELECT Fld_Value FROM CFW_FLD_Values_Item WHERE Fld_ID = v_numFormFieldId AND RecId = v_numItemCode)) THEN
				UPDATE tt_TEMPAVAILABLEFIELDS SET vcItemValue =(SELECT FormatedDateFromDate(Fld_Value::TIMESTAMP,v_numDomainID) FROM CFW_FLD_Values_Item WHERE Fld_ID = v_numFormFieldId AND RecId = v_numItemCode) WHERE numFormFieldId = v_numFormFieldId;
			END IF;
         ELSEIF v_vcAssociatedControlType = 'SelectBox'
         then
			
            UPDATE tt_TEMPAVAILABLEFIELDS SET vcItemValue =(SELECT L.vcData FROM CFW_FLD_Values_Item CFW left Join Listdetails L
            on L.numListItemID::VARCHAR = CFW.Fld_Value
            WHERE CFW.Fld_ID = v_numFormFieldId AND CFW.RecId = v_numItemCode) WHERE numFormFieldId = v_numFormFieldId;
         end if;
      end if;
      select   intRowNum, numFormFieldId, vcFormFieldName, vcFieldType, vcAssociatedControlType, numListID, vcListItemType, GRP_ID INTO v_tintOrder,v_numFormFieldId,v_vcFormFieldName,v_vcFieldType,v_vcAssociatedControlType,
      v_numListID,v_vcListItemType,v_GRP_ID FROM
      tt_TEMPAVAILABLEFIELDS WHERE
      intRowNum > v_tintOrder   ORDER BY
      intRowNum ASC LIMIT 1;
      GET DIAGNOSTICS SWV_RowCount = ROW_COUNT;
      IF SWV_RowCount = 0 then 
         v_tintOrder := 0;
      end if;
   END LOOP;   


   INSERT INTO tt_TEMPAVAILABLEFIELDS(numFormFieldId
		,vcFormFieldName
		,vcFieldType
		,vcAssociatedControlType
		,numListID
		,vcListItemType
		,intRowNum
		,vcItemValue
		,GRP_ID)
   SELECT
   Fld_id
		,REPLACE(Fld_label,' ','') AS vcFormFieldName
		,CASE GRP_ID WHEN 4 then 'C' WHEN 1 THEN 'D' ELSE 'C' END AS vcFieldType
		,fld_type as vcAssociatedControlType
		,coalesce(C.numlistid,0) AS numListID
		,CASE WHEN C.numlistid > 0 THEN 'L' ELSE '' END AS vcListItemType
		,ROW_NUMBER() OVER(ORDER BY Fld_id asc) AS intRowNum
		,CAST('' AS VARCHAR(3000))
		,GRP_ID
   FROM
   CFW_Fld_Master C
   LEFT JOIN
   CFW_Validation V
   ON
   V.numFieldId = C.Fld_id
   WHERE
   C.numDomainID = v_numDomainID
   AND GRP_ID = 9;

   IF coalesce(v_bitMatrix,false) = true AND coalesce(v_numItemGroup,0) > 0 then
	
      UPDATE
      tt_TEMPAVAILABLEFIELDS TEMP
      SET
      vcItemValue = FLD_ValueName
      FROM(SELECT
      CFW_Fld_Master.Fld_id
				,CASE
      WHEN CFW_Fld_Master.fld_type = 'SelectBox' THEN GetListIemName(CASE WHEN isnumeric(Fld_Value::text) THEN Fld_Value::NUMERIC ELSE 0 END)
      WHEN CFW_Fld_Master.fld_type = 'CheckBox' THEN (CASE WHEN coalesce(Fld_Value,'0') = '1' THEN 'Yes' ELSE 'No' END)
      ELSE Fld_Value::VARCHAR
      END AS FLD_ValueName
      FROM
      CFW_Fld_Master
      INNER JOIN
      ItemGroupsDTL
      ON
      CFW_Fld_Master.Fld_id = ItemGroupsDTL.numOppAccAttrID
      AND ItemGroupsDTL.tintType = 2
      LEFT JOIN
      ItemAttributes
      ON
      CFW_Fld_Master.Fld_id = ItemAttributes.Fld_ID
      AND ItemAttributes.numItemCode = v_numItemCode
      LEFT JOIN
      Listdetails LD
      ON
      CFW_Fld_Master.numlistid = LD.numListID
      WHERE
      CFW_Fld_Master.numDomainID = v_numDomainID
      AND ItemGroupsDTL.numItemGroupID = v_numItemGroup
      GROUP BY
      CFW_Fld_Master.FLd_label,CFW_Fld_Master.Fld_id,CFW_Fld_Master.fld_type,
      CFW_Fld_Master.bitAutocomplete,CFW_Fld_Master.numlistid,CFW_Fld_Master.vcURL,
      ItemAttributes.Fld_Value) T1 
      WHERE
      TEMP.numFormFieldId = T1.fld_id AND TEMP.GRP_ID = 9;
   end if;
  
   OPEN SWV_RefCur2 FOR SELECT * FROM tt_TEMPAVAILABLEFIELDS;

   RETURN;
END; $$;












