-- Stored procedure definition script USP_ElasticSearchBizCart_GetDisableDomain for PostgreSQL
CREATE OR REPLACE FUNCTION USP_ElasticSearchBizCart_GetDisableDomain(INOUT SWV_RefCur refcursor)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT DISTINCT
   EC.numDomainID
   FROM
   eCommerceDTL EC
   LEFT JOIN
   eCommerceDTL OtherSite
   ON
   EC.numDomainID = OtherSite.numDomainID
   AND (DATE_PART('day',TIMEZONE('UTC',now()):: timestamp -EC.dtElasticSearchDisableDatetime:: timestamp) <= 30 OR EC.bitElasticSearch = true)
   WHERE
   EC.dtElasticSearchDisableDatetime Is not null
   AND DATE_PART('day',TIMEZONE('UTC',now()):: timestamp -EC.dtElasticSearchDisableDatetime:: timestamp) > 30
   AND EC.bitElasticSearch = false AND OtherSite.numDomainID IS NULL;
END; $$;












