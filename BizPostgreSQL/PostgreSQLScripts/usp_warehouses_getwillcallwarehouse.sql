-- Stored procedure definition script USP_Warehouses_GetWillCallWarehouse for PostgreSQL
CREATE OR REPLACE FUNCTION USP_Warehouses_GetWillCallWarehouse(v_numDomainID NUMERIC(18,0)
,v_numWarehouseID NUMERIC(18,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT numWareHouseID, coalesce(vcWStreet,'') || ', ' || coalesce(vcWCity,'') || ', ' ||  coalesce(fn_GetState(numWState),'') || ', ' || coalesce(fn_GetListItemName(numWCountry),'') || ' - ' || coalesce(vcWPinCode,'') AS vcFullAddress FROM Warehouses WHERE numDomainID = v_numDomainID AND (numWareHouseID = v_numWarehouseID OR coalesce(v_numWarehouseID,0) = 0) LIMIT 1;
   RETURN;
END; $$;













