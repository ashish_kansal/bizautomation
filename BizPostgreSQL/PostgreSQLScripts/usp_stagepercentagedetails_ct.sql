Create or replace FUNCTION USP_StagePercentageDetails_CT(v_numDomainID NUMERIC(18,0) DEFAULT 0,
    v_numUserCntID NUMERIC(18,0) DEFAULT 0,
    v_numRecordID NUMERIC(18,0) DEFAULT 0, INOUT SWV_RefCur refcursor default null, INOUT SWV_RefCur2 refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_tintWFTriggerOn  SMALLINT;
   v_Columns_Updated  VARCHAR(1000);
BEGIN
	--NOTE: LOGIC IS MOVED TO TRIGGER BECAUSE CHANGE TRACKING IS NOT AVAILABLE in POSTGRESQL

	OPEN SWV_RefCur FOR SELECT v_Columns_Updated;

	OPEN SWV_RefCur2 FOR SELECT v_tintWFTriggerOn;

	RETURN;
END; $$;
	
