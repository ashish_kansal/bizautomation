DROP FUNCTION IF EXISTS USP_TimeAndExpense_GetExpenseSummary;

CREATE OR REPLACE FUNCTION USP_TimeAndExpense_GetExpenseSummary(v_numDomainID NUMERIC(18,0)
	,v_dtFromDate DATE
	,v_dtToDate DATE
	,v_vcEmployeeName VARCHAR(200)
	,v_vcTeams TEXT
	,v_tintPayrollType SMALLINT
	,v_ClientTimeZoneOffset INTEGER,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   DROP TABLE IF EXISTS tt_TEMPGetExpenseSummary CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPGetExpenseSummary
   (
      numType SMALLINT,
      monExpense DECIMAL(20,5)
   );
   INSERT INTO tt_TEMPGetExpenseSummary(numType
		,monExpense)
   SELECT
   numType
		,monExpense
   FROM
   UserMaster
   INNER JOIN
   AdditionalContactsInformation
   ON
   UserMaster.numUserDetailId = AdditionalContactsInformation.numContactId
   LEFT JOIN LATERAL(SELECT
      numType
			,monExpense
      FROM
      fn_GetPayrollEmployeeExpense(v_numDomainID,CAST(UserMaster.numUserDetailId AS INTEGER),v_dtFromDate,v_dtToDate,v_ClientTimeZoneOffset)) TEMPExpense on TRUE
   WHERE
   UserMaster.numDomainID = v_numDomainID
   AND coalesce(UserMaster.bitactivateflag,false) = true
   AND (coalesce(v_vcEmployeeName,'') = '' OR AdditionalContactsInformation.vcFirstName ilike CONCAT('%',v_vcEmployeeName,'%') OR AdditionalContactsInformation.vcLastname ilike CONCAT('%',v_vcEmployeeName,'%'))
   AND (coalesce(v_vcTeams,'') = '' OR AdditionalContactsInformation.numTeam IN(SELECT Id FROM SplitIDs(v_vcTeams,',')))
   AND 1 =(CASE WHEN v_tintPayrollType = 0 THEN 1 ELSE(CASE WHEN coalesce(UserMaster.tintPayrollType,0) = v_tintPayrollType THEN 1 ELSE 0 END) END);


	
open SWV_RefCur for SELECT
   cast(coalesce((SELECT SUBSTR(TO_CHAR('1900-01-01':: date+CAST(coalesce(SUM(monExpense),0) || 'minute' as interval),'hh24:mi:ss'),1,5) FROM tt_TEMPGetExpenseSummary),
   cast(0 as TEXT)) as VARCHAR(5)) AS monTotalExpense
		,cast(coalesce((SELECT SUBSTR(TO_CHAR('1900-01-01':: date+CAST(coalesce(SUM(monExpense),0) || 'minute' as interval),'hh24:mi:ss'),1,5) FROM tt_TEMPGetExpenseSummary WHERE numType = 1),cast(0 as TEXT)) as VARCHAR(5)) AS monTotalBillableExpense
		,cast(coalesce((SELECT SUBSTR(TO_CHAR('1900-01-01':: date+CAST(coalesce(SUM(monExpense),0) || 'minute' as interval),'hh24:mi:ss'),1,5) FROM tt_TEMPGetExpenseSummary WHERE numType = 2),cast(0 as TEXT)) as VARCHAR(5)) AS monTotalReimbursableExpense
		,cast(coalesce((SELECT SUBSTR(TO_CHAR('1900-01-01':: date+CAST(coalesce(SUM(monExpense),0) || 'minute' as interval),'hh24:mi:ss'),1,5) FROM tt_TEMPGetExpenseSummary WHERE numType = 6),cast(0 as TEXT)) as VARCHAR(5)) AS monTotalBillableReimbursableExpense;
END; $$;












