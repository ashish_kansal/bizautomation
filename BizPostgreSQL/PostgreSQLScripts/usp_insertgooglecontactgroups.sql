-- Stored procedure definition script USP_InsertGoogleContactGroups for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_InsertGoogleContactGroups(v_numDomainID NUMERIC(9,0) DEFAULT 0,
v_numContactId NUMERIC(9,0) DEFAULT 0,
v_tintType SMALLINT DEFAULT 0,
v_vcGroupId VARCHAR(50) DEFAULT 0,
v_tintMode SMALLINT DEFAULT NULL)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   IF v_tintMode = 1 then

      insert into GoogleContactGroups(numDomainID,numContactId,tintType,vcGroupId)
		values(v_numDomainID,v_numContactId,v_tintType,v_vcGroupId);
   ELSEIF v_tintMode = 2
   then

      DELETE FROM GoogleContactGroups WHERE numDomainID = v_numDomainID AND numContactId = v_numContactId AND tintType = v_tintType;
   end if;
   RETURN;
END; $$;
/****** Object:  StoredProcedure [dbo].[USP_InsertImapNewMails]    Script Date: 07/26/2008 16:19:07 ******/



