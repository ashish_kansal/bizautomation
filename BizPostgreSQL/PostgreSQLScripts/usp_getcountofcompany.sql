-- Stored procedure definition script USP_GetCountOfCompany for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetCountOfCompany(v_numAlerDTLId NUMERIC(9,0) DEFAULT 0,
v_numContactID NUMERIC(9,0) DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for select count(*) from AlertContactsAndCompany where numAlerDTLId = v_numAlerDTLId and numContactID = v_numContactID;
END; $$;












