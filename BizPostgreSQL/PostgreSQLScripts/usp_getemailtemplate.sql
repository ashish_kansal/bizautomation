-- Stored procedure definition script USP_GetEmailtemplate for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetEmailtemplate(v_numDomainID NUMERIC(9,0) DEFAULT 0,
v_numModuleID NUMERIC DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for select cast(VcDocName as VARCHAR(255)) , cast(numGenericDocID as VARCHAR(255)) from GenericDocuments
   where  numDocCategory = 369
   And
   numDomainId = v_numDomainID
   AND (numModuleId = v_numModuleID OR v_numModuleID = 0);
END; $$;












