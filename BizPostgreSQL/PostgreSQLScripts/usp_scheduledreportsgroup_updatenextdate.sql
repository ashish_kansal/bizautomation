-- Stored procedure definition script USP_ScheduledReportsGroup_UpdateNextDate for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ScheduledReportsGroup_UpdateNextDate(v_numDomainID NUMERIC(18,0)
	,v_numSRGID NUMERIC(18,0))
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_tintFrequency  SMALLINT;
   v_dtStartDate  TIMESTAMP;
BEGIN
   select   tintFrequency, dtStartDate INTO v_tintFrequency,v_dtStartDate FROM ScheduledReportsGroup WHERE numDomainID = v_numDomainID AND ID = v_numSRGID;

   IF coalesce(v_tintFrequency,0) = 1 then
	
      IF v_dtStartDate < TIMEZONE('UTC',now()) then
		
         WHILE CAST(v_dtStartDate AS DATE) <= CAST(TIMEZONE('UTC',now()) AS DATE) LOOP
            v_dtStartDate := v_dtStartDate+INTERVAL '1 day';
         END LOOP;
         UPDATE ScheduledReportsGroup SET dtNextDate = v_dtStartDate,intNotOfTimeTried = 0 WHERE numDomainID = v_numDomainID AND ID = v_numSRGID;
      ELSE
         UPDATE ScheduledReportsGroup SET dtNextDate = dtStartDate+INTERVAL '1 day',intNotOfTimeTried = 0 WHERE numDomainID = v_numDomainID AND ID = v_numSRGID;
      end if;
   ELSEIF coalesce(v_tintFrequency,0) = 2
   then
	
      IF v_dtStartDate < TIMEZONE('UTC',now()) then
		
         WHILE CAST(v_dtStartDate AS DATE) <= CAST(TIMEZONE('UTC',now()) AS DATE) LOOP
            v_dtStartDate := v_dtStartDate+1*INTERVAL '1 week';
         END LOOP;
         UPDATE ScheduledReportsGroup SET dtNextDate = v_dtStartDate,intNotOfTimeTried = 0 WHERE numDomainID = v_numDomainID AND ID = v_numSRGID;
      ELSE
         UPDATE ScheduledReportsGroup SET dtNextDate = dtStartDate+1*INTERVAL '1 week',intNotOfTimeTried = 0 WHERE numDomainID = v_numDomainID AND ID = v_numSRGID;
      end if;
   ELSEIF coalesce(v_tintFrequency,0) = 3
   then
	
      IF v_dtStartDate < TIMEZONE('UTC',now()) then
		
         WHILE CAST(v_dtStartDate AS DATE) <= CAST(TIMEZONE('UTC',now()) AS DATE) LOOP
            v_dtStartDate := v_dtStartDate+INTERVAL '1 month';
         END LOOP;
         UPDATE ScheduledReportsGroup SET dtNextDate = v_dtStartDate,intNotOfTimeTried = 0 WHERE numDomainID = v_numDomainID AND ID = v_numSRGID;
      ELSE
         UPDATE ScheduledReportsGroup SET dtNextDate = dtStartDate+INTERVAL '1 month',intNotOfTimeTried = 0 WHERE numDomainID = v_numDomainID AND ID = v_numSRGID;
      end if;
   end if;
   RETURN;
END; $$;


