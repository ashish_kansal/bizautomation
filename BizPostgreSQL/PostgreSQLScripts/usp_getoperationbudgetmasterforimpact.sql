-- Stored procedure definition script USP_GetOperationBudgetMasterForImpact for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetOperationBudgetMasterForImpact(v_numDomainId NUMERIC(9,0) DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for Select cast(numBudgetId as VARCHAR(255)),cast(vcBudgetName as VARCHAR(255)) From OperationBudgetMaster
   Where numDomainId = v_numDomainId;
END; $$;












