-- Stored procedure definition script usp_UpdateQBAccounting for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION usp_UpdateQBAccounting(v_vcCompanyName VARCHAR(255),
	v_numLastUpdateDate NUMERIC,
	v_vcAccountNo VARCHAR(20),
	v_numOpenARBalance DOUBLE PRECISION,
	v_numCreditLimit DOUBLE PRECISION,
	v_vcTerms VARCHAR(30)   
	--
)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   update QBAccounting  set numLastUpdateDate = v_numLastUpdateDate,vcAccountNo = v_vcAccountNo,numOpenARBalance = v_numOpenARBalance,
   numCreditLimit = v_numCreditLimit,vcTerms = v_vcTerms where vcCompanyName = v_vcCompanyName;
   RETURN;
END; $$;


