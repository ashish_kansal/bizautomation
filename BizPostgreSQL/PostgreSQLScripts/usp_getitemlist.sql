CREATE OR REPLACE FUNCTION usp_getItemList(v_numDomainId NUMERIC(9,0),
v_varFilter VARCHAR(30),
v_type SMALLINT DEFAULT 0, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
BEGIN
   if v_type = 0 then
      open SWV_RefCur for
      select numItemCode AS "numItemCode",vcItemName AS "vcItemName" from Item where numDomainID = v_numDomainId  and vcItemName ilike v_varFilter;
   end if;

   if v_type = 1 then
      open SWV_RefCur for
      select numWareHouseID AS "numWareHouseID",vcWareHouse AS "vcWareHouse" from Warehouses where numDomainID = 1;
   end if;

   if v_type = 2 then
      open SWV_RefCur for
      select numItemCode AS "numItemCode",vcItemName AS "vcItemName",coalesce(monListPrice,0) AS "monListPrice" from Item where numDomainID = v_numDomainId and charItemType = v_varFilter ORDER BY vcItemName;
   end if;

   if v_type = 3 then
      open SWV_RefCur for
      select CAST(numItemCode AS VARCHAR(10)) || '~' || CAST(coalesce(monListPrice,0) AS VARCHAR(10)) AS "numItemCode",vcItemName AS "vcItemName" from Item where numDomainID = v_numDomainId and charItemType = v_varFilter ORDER BY vcItemName;
   end if;
   RETURN;
END; $$;



