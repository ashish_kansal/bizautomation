-- Stored procedure definition script USP_SalesOrderConfiguration_Get for PostgreSQL
CREATE OR REPLACE FUNCTION USP_SalesOrderConfiguration_Get(v_numDomainID NUMERIC(18,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	

   open SWV_RefCur for SELECT * FROM SalesOrderConfiguration WHERE numDomainID = v_numDomainID;
END; $$;












