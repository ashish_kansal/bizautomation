-- Stored procedure definition script usp_GetBestAccountsForReport for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION usp_GetBestAccountsForReport(                          
  --                          
v_numDomainID NUMERIC,                            
 v_dtDateFrom TIMESTAMP,                            
 v_dtDateTo TIMESTAMP,                            
 v_numUserCntID NUMERIC DEFAULT 0,                                        
 v_intType NUMERIC DEFAULT 0,                          
 v_tintRights SMALLINT DEFAULT 1, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
BEGIN
   If v_tintRights = 1 then                          
 --All Records Rights                            
                             
  --- Check the Condition Here                         
      open SWV_RefCur for
      SELECT  CI.vcCompanyName || '-' || DM.vcDivisionName  AS CustomerDivision,DM.numDivisionID,fn_GetPrimaryContact(DM.numDivisionID) as ContactID,CI.numCompanyId,DM.tintCRMType,
  fn_GetContactName(OM.numrecowner) As AccountOwner,
  (select sum(M.monPAmount) from OpportunityMaster M where  M.numDivisionId = DM.numDivisionID  And tintopptype = 1 and tintoppstatus = 1 And bintCreatedDate BETWEEN v_dtDateFrom AND v_dtDateTo) As ClosedDealsAmount,
  (select count(*) from OpportunityMaster M where  M.numDivisionId = DM.numDivisionID  And tintopptype = 1 and tintoppstatus = 1 And bintCreatedDate BETWEEN v_dtDateFrom AND v_dtDateTo) as DealsClosed,
  (select sum(M.monPAmount) from OpportunityMaster M where  M.numDivisionId = DM.numDivisionID  And tintopptype = 1 and tintoppstatus = 0 And bintCreatedDate BETWEEN v_dtDateFrom AND v_dtDateTo) as OpportunitiesInPipelineAmount,
  (select count(*) from OpportunityMaster M where  M.numDivisionId = DM.numDivisionID  And tintopptype = 1 and tintoppstatus = 0 And bintCreatedDate BETWEEN v_dtDateFrom AND v_dtDateTo) as OpportunitiesInPipeline,
  LD.vcData as Industry
      FROM  OpportunityMaster OM
      JOIN DivisionMaster DM
      ON DM.numDivisionID = OM.numDivisionId
      JOIN AdditionalContactsInformation ACI
      ON ACI.numContactId = OM.numContactId
      JOIN CompanyInfo CI
      ON CI.numCompanyId = DM.numCompanyID
      left JOIN Listdetails LD
      ON LD.numListItemID = CI.numCompanyIndustry
      WHERE DM.numDomainID = v_numDomainID and DM.numRecOwner = v_numUserCntID And OM.bintCreatedDate BETWEEN v_dtDateFrom AND v_dtDateTo
      GROUP BY CI.vcCompanyName,DM.vcDivisionName,CI.numCompanyId,DM.tintCRMType,DM.numDivisionID,
      LD.vcData,OM.numrecowner;
   end if;                            
                            
   If v_tintRights = 2 then
 
      open SWV_RefCur for
      SELECT  CI.vcCompanyName || '-' || DM.vcDivisionName  AS CustomerDivision, DM.numDivisionID, fn_GetPrimaryContact(DM.numDivisionID) as ContactID,CI.numCompanyId,DM.tintCRMType,
  fn_GetContactName(OM.numrecowner) As AccountOwner,                           
--  SUM(OM.monPAmount) As ClosedDealsAmount,       
 (select SUM(M.monPAmount) from OpportunityMaster M INNER JOIN AdditionalContactsInformation ACI ON ACI.numContactId = M.numContactId where  M.numDivisionId = DM.numDivisionID And tintopptype = 1 and M.tintoppstatus = 1
         And M.bintCreatedDate BETWEEN v_dtDateFrom AND v_dtDateTo) as ClosedDealsAmount,
 (select count(*) from OpportunityMaster M INNER JOIN AdditionalContactsInformation ACI ON ACI.numContactId = M.numContactId where  M.numDivisionId = DM.numDivisionID And
         tintopptype = 1  and M.tintoppstatus = 1 And M.bintCreatedDate BETWEEN v_dtDateFrom AND v_dtDateTo) as DealsClosed,        
   --SUM(OM.numOppID) as OpportunitiesInPipelineAmount,             
  (select SUM(M.monPAmount) from OpportunityMaster M where  M.numDivisionId = DM.numDivisionID  And tintopptype = 1 and  tintoppstatus = 0 And bintCreatedDate BETWEEN v_dtDateFrom AND v_dtDateTo) as OpportunitiesInPipelineAmount,
  (select count(*) from OpportunityMaster M where  M.numDivisionId = DM.numDivisionID  And tintopptype = 1 and tintoppstatus = 0 And bintCreatedDate BETWEEN v_dtDateFrom AND v_dtDateTo) as OpportunitiesInPipeline,
  LD.vcData as Industry
      FROM  OpportunityMaster OM
      JOIN DivisionMaster DM
      ON DM.numDivisionID = OM.numDivisionId
      JOIN AdditionalContactsInformation ACI
      ON ACI.numContactId = OM.numContactId
      JOIN CompanyInfo CI
      ON CI.numCompanyId = DM.numCompanyID
      left JOIN Listdetails LD
      ON LD.numListItemID = CI.numCompanyIndustry
      join AdditionalContactsInformation  ADC
      on ADC.numContactId = OM.numrecowner
      WHERE DM.numDomainID = v_numDomainID and DM.numRecOwner = v_numUserCntID And OM.bintCreatedDate BETWEEN v_dtDateFrom AND v_dtDateTo
      and ADC.numTeam in(select F.numTeam from ForReportsByTeam F
         where F.numUserCntID = v_numUserCntID and F.numDomainID = v_numDomainID and F.tintType = v_intType)
      GROUP BY CI.vcCompanyName,DM.vcDivisionName,DM.numDivisionID,CI.numCompanyId,DM.tintCRMType, 
      ACI.vcFirstName,ACI.vcLastname,ACI.numPhone,ACI.numPhoneExtension,
      LD.vcData,OM.numrecowner;
   end if;                            
                            
   If v_tintRights = 3 then
 
      open SWV_RefCur for
      SELECT  CI.vcCompanyName || '-' || DM.vcDivisionName  AS CustomerDivision,DM.numDivisionID, fn_GetPrimaryContact(DM.numDivisionID) as ContactID,CI.numCompanyId,DM.tintCRMType,
  fn_GetContactName(OM.numrecowner) As AccountOwner,                           
--  SUM(OM.monPAmount) As ClosedDealsAmount,        
  (select SUM(M.monPAmount) from OpportunityMaster M where  M.numDivisionId = DM.numDivisionID And tintopptype = 1 and tintoppstatus = 1 And bintCreatedDate BETWEEN v_dtDateFrom AND v_dtDateTo) as ClosedDealsAmount,
  (select count(*) from OpportunityMaster M where  M.numDivisionId = DM.numDivisionID  And tintopptype = 1 and tintoppstatus = 1 And bintCreatedDate BETWEEN v_dtDateFrom AND v_dtDateTo) as DealsClosed,         
  --SUM(OM.numOppID) as OpportunitiesInPipelineAmount,         
  (select SUM(M.monPAmount) from OpportunityMaster M where  M.numDivisionId = DM.numDivisionID  And tintopptype = 1 and tintoppstatus = 0 And bintCreatedDate BETWEEN v_dtDateFrom AND v_dtDateTo) as OpportunitiesInPipelineAmount,
   (select count(*) from OpportunityMaster M where  M.numDivisionId = DM.numDivisionID  And tintopptype = 1 and tintoppstatus = 0 And bintCreatedDate BETWEEN v_dtDateFrom AND v_dtDateTo) as OpportunitiesInPipeline,
  LD.vcData as Industry
      FROM  OpportunityMaster OM
      JOIN DivisionMaster DM
      ON DM.numDivisionID = OM.numDivisionId
      JOIN AdditionalContactsInformation ACI
      ON ACI.numContactId = OM.numContactId
      JOIN CompanyInfo CI
      ON CI.numCompanyId = DM.numCompanyID
      left JOIN Listdetails LD
      ON LD.numListItemID = CI.numCompanyIndustry
      WHERE DM.numDomainID = v_numDomainID and DM.numRecOwner = v_numUserCntID  And OM.bintCreatedDate BETWEEN v_dtDateFrom AND v_dtDateTo
      and DM.numTerID in(select F.numTerritory from ForReportsByTerritory F   -- Added By Siva                           
         where F.numUserCntId = v_numUserCntID and F.numDomainID = v_numDomainID and F.tintType = v_intType)
      GROUP BY CI.vcCompanyName,DM.vcDivisionName,DM.numDivisionID,CI.numCompanyId,DM.tintCRMType,  
      ACI.vcFirstName,ACI.vcLastname,ACI.numPhone,ACI.numPhoneExtension,
      LD.vcData,OM.numrecowner;
   end if;
   RETURN;
END; $$;


