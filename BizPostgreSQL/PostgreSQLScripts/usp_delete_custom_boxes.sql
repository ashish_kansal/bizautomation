-- Stored procedure definition script USP_DELETE_CUSTOM_BOXES for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_DELETE_CUSTOM_BOXES(v_numPackageTypeID		NUMERIC(18,0))
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   DELETE FROM CustomPackages WHERE numPackageTypeID = v_numPackageTypeID;
   RETURN;
END; $$;

	


