-- Function definition script fn_UOMConversion for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION fn_UOMConversion(v_numFromUnit NUMERIC,
	v_numItemCode NUMERIC,
	v_numDomainID NUMERIC,
	v_numToUnit NUMERIC DEFAULT null)
RETURNS DOUBLE PRECISION LANGUAGE plpgsql
   AS $$
   DECLARE
   v_bitEnableItemLevelUOM  BOOLEAN DEFAULT false;
   v_MultiplicationFactor  DOUBLE PRECISION;
BEGIN
   select   coalesce(bitEnableItemLevelUOM,false) INTO v_bitEnableItemLevelUOM FROM Domain WHERE numDomainId = v_numDomainID;
 
   IF coalesce(v_numToUnit,0) = 0 then
      select   numBaseUnit INTO v_numToUnit FROM Item WHERE numDomainID = v_numDomainID AND numItemCode = v_numItemCode;
   end if;

   IF EXISTS(SELECT v_numToUnit WHERE v_numToUnit IS null) then
      RETURN 1;
   end if;
 
   IF(v_numFromUnit = v_numToUnit) then
      RETURN 1;
   end if;

   IF v_bitEnableItemLevelUOM = false then
	
      with recursive recursiveTable(FROMUnit,LevelNum,numUOM1,decConv1,numUOM2,decConv2,multiplicationFactor) as(select  numUOM1 AS FROMUnit,CAST(0 AS INTEGER) AS LevelNum
					,ct.numUOM1 AS numUOM1,ct.decConv1 AS decConv1,ct.numUOM2 AS numUOM2,ct.decConv2 AS decConv2
					,CAST(ct.decConv2/ct.decConv1 AS DOUBLE PRECISION) AS multiplicationFactor
      from UOMConversion ct
      where   ct.numUOM1 = v_numFromUnit AND ct.numDomainID = v_numDomainID
      union all
      select  FROMUnit AS FROMUnit,LevelNum+1 AS LevelNum
					,rt.numUOM2 AS numUOM1,rt.decConv2 AS decConv1,ct.numUOM2 AS numUOM2,ct.decConv2 AS decConv2
					,(rt.multiplicationFactor*CAST(ct.decConv2/ct.decConv1 AS DOUBLE PRECISION)) AS multiplicationFactor
      from UOMConversion ct
      inner join recursiveTable rt on rt.numUOM2 = ct.numUOM1 WHERE ct.numDomainID = v_numDomainID) select   multiplicationFactor INTO v_MultiplicationFactor from recursiveTable where FROMUnit = v_numFromUnit and numUOM2 = v_numToUnit    LIMIT 1;
      IF EXISTS(SELECT v_MultiplicationFactor WHERE v_MultiplicationFactor IS NOT null) then
		
         RETURN v_MultiplicationFactor;
      ELSE
         with recursive recursiveTable(FROMUnit,LevelNum,numUOM1,decConv1,numUOM2,decConv2,multiplicationFactor) as(select  numUOM2 AS FROMUnit,CAST(0 AS INTEGER) AS LevelNum
						,ct.numUOM1 AS numUOM1,ct.decConv1 AS decConv1,ct.numUOM2 AS numUOM2,ct.decConv2 AS decConv2
						,CAST(ct.decConv1/ct.decConv2 AS DOUBLE PRECISION) AS multiplicationFactor
         from UOMConversion ct
         where   ct.numUOM2 = v_numFromUnit AND ct.numDomainID = v_numDomainID
         union all
         select  FROMUnit AS FROMUnit,LevelNum+1 AS LevelNum
						,ct.numUOM1 AS numUOM1,ct.decConv1 AS decConv1,ct.numUOM2 AS numUOM2,ct.decConv2 AS decConv2
						,CAST(rt.multiplicationFactor*CAST((ct.decConv1/ct.decConv2) AS DOUBLE PRECISION) AS DOUBLE PRECISION) AS multiplicationFactor
         from UOMConversion ct
         inner join recursiveTable rt on rt.numUOM1 = ct.numUOM2 WHERE ct.numDomainID = v_numDomainID) select   multiplicationFactor INTO v_MultiplicationFactor from recursiveTable where FROMUnit = v_numFromUnit and numUOM1 = v_numToUnit    LIMIT 1;
         IF EXISTS(SELECT v_MultiplicationFactor WHERE v_MultiplicationFactor IS NOT null) then
			
            RETURN v_MultiplicationFactor;
         end if;
      end if;
   ELSE
      with recursive CTE(FromUnit,LevelNum,numSourceUOM,numTargetUOM,numTargetUnit) AS(SELECT
      IUOM.numSourceUOM AS FromUnit,
				CAST(0 AS INTEGER) AS LevelNum,
				IUOM.numSourceUOM AS numSourceUOM,
				IUOM.numTargetUOM AS numTargetUOM,
				IUOM.numTargetUnit AS numTargetUnit
      FROM
      ItemUOMConversion IUOM
      WHERE
      IUOM.numDomainID = v_numDomainID
      AND IUOM.numItemCode = v_numItemCode
      AND IUOM.numSourceUOM = v_numFromUnit
      UNION ALL
      SELECT
      cte.FROMUnit AS FromUnit,
				LevelNum+1 AS LevelNum,
				IUOM.numSourceUOM AS numSourceUOM,
				IUOM.numTargetUOM AS numTargetUOM,
				(IUOM.numTargetUnit*cte.numTargetUnit) AS numTargetUnit
      FROM
      ItemUOMConversion IUOM
      INNER JOIN
      CTE cte
      ON
      IUOM.numSourceUOM = cte.numTargetUOM
      WHERE
      IUOM.numDomainID = v_numDomainID
      AND IUOM.numItemCode = v_numItemCode) select   numTargetUnit INTO v_MultiplicationFactor FROM CTE WHERE FROMUnit = v_numFromUnit AND numTargetUOM = v_numToUnit    LIMIT 1;
      IF EXISTS(SELECT v_MultiplicationFactor WHERE v_MultiplicationFactor IS NOT NULL) then
		
         RETURN v_MultiplicationFactor;
      ELSE
         with recursive CTE(FromUnit,LevelNum,numSourceUOM,numTargetUOM,numTargetUnit) AS(SELECT
         IUOM.numTargetUOM AS FromUnit,
					CAST(0 AS INTEGER) AS LevelNum,
					IUOM.numSourceUOM AS numSourceUOM,
					IUOM.numTargetUOM AS numTargetUOM,
					(1/IUOM.numTargetUnit) AS numTargetUnit
         FROM
         ItemUOMConversion IUOM
         WHERE
         IUOM.numDomainID = v_numDomainID
         AND IUOM.numItemCode = v_numItemCode
         AND IUOM.numTargetUOM = v_numFromUnit
         UNION ALL
         SELECT
         cte.FROMUnit AS FromUnit,
					LevelNum+1 AS LevelNum,
					IUOM.numSourceUOM AS numSourceUOM,
					IUOM.numTargetUOM AS numTargetUOM,
					((1/IUOM.numTargetUnit)*cte.numTargetUnit) AS numTargetUnit
         FROM
         ItemUOMConversion IUOM
         INNER JOIN
         CTE cte
         ON
         IUOM.numTargetUOM = cte.numSourceUOM
         WHERE
         IUOM.numDomainID = v_numDomainID
         AND IUOM.numItemCode = v_numItemCode) select   numTargetUnit INTO v_MultiplicationFactor from cte where FROMUnit = v_numFromUnit and numSourceUOM = v_numToUnit    LIMIT 1;
         IF EXISTS(SELECT v_MultiplicationFactor WHERE v_MultiplicationFactor IS NOT null) then
			
            RETURN v_MultiplicationFactor;
         end if;
      end if;
   end if;

   RETURN 1;
END; $$;

