-- Stored procedure definition script USP_ManageCOARelationship for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ManageCOARelationship(INOUT v_numCOARelationshipID NUMERIC(9,0) DEFAULT 0 ,
	v_numRelationshipID NUMERIC(9,0) DEFAULT NULL,
	v_numARAccountId NUMERIC(9,0) DEFAULT NULL,
	v_numAPAccountId NUMERIC(9,0) DEFAULT NULL,
	v_numDomainID NUMERIC(9,0) DEFAULT NULL,
	v_numUserCntId NUMERIC(9,0) DEFAULT NULL,
	v_numARParentAcntTypeID NUMERIC(9,0) DEFAULT NULL,
	v_numAPParentAcntTypeID NUMERIC(9,0) DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN

   IF v_numARParentAcntTypeID = 0 then
	
      select   numParntAcntTypeID INTO v_numARParentAcntTypeID FROM Chart_Of_Accounts WHERE numAccountId = v_numARAccountId;
   end if;
   IF v_numAPParentAcntTypeID = 0 then
	
      select   numParntAcntTypeID INTO v_numAPParentAcntTypeID FROM Chart_Of_Accounts WHERE numAccountId = v_numAPAccountId;
   end if;
   IF v_numCOARelationshipID = 0 then 
      INSERT INTO COARelationships(numRelationshipID,
		numARParentAcntTypeID,
		numAPParentAcntTypeID,
		numARAccountId,
		numAPAccountId,
		numDomainID,
		dtCreateDate,
		dtModifiedDate,
		numCreatedBy,
		numModifiedBy)
	VALUES(v_numRelationshipID,
		v_numARParentAcntTypeID,
		v_numAPParentAcntTypeID,
		v_numARAccountId,
		v_numAPAccountId,
		v_numDomainID,
		TIMEZONE('UTC',now()),
		TIMEZONE('UTC',now()),
		v_numUserCntId,
		v_numUserCntId);
	
      v_numCOARelationshipID := CURRVAL('COARelationships_seq');
   ELSE
      RAISE NOTICE '%',v_numAPParentAcntTypeID;
      UPDATE COARelationships SET
      numARAccountId = v_numARAccountId,numAPAccountId = v_numAPAccountId,numARParentAcntTypeID = v_numARParentAcntTypeID,
      numAPParentAcntTypeID = v_numAPParentAcntTypeID,
      dtModifiedDate = TIMEZONE('UTC',now()),numModifiedBy = v_numUserCntId
      WHERE numCOARelationshipID = v_numCOARelationshipID
      AND numDomainID = v_numDomainID;
   end if;
   RETURN;
END; $$;



