-- Stored procedure definition script USP_GetEmailUsers for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_GetEmailUsers(v_numDomainID NUMERIC(9,0) DEFAULT 0,            
v_numContactID NUMERIC(9,0) DEFAULT 0,            
v_byteMode SMALLINT DEFAULT NULL, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
BEGIN
   if v_byteMode = 0 then
      open SWV_RefCur for
      select distinct(numContactId),vcFirstName || ' ' || vcLastname as vcUserName from UserMaster U
      join AdditionalContactsInformation
      on numContactId = numUserDetailId
      where bitActivateFlag = true and U.numDomainID = v_numDomainID;
   end if;            
            
            
   if v_byteMode = 1 then
      open SWV_RefCur for
      select distinct(A.numContactId),vcFirstName || ' ' || vcLastname as vcUserName from UserMaster  U
      join emailusers E
      on E.numContactID = U.numUserDetailId
      join AdditionalContactsInformation A
      on A.numContactId = numUserDetailId
      where bitActivateFlag = true and E.numUserCntID = v_numContactID;
   end if;
   RETURN;
END; $$;


