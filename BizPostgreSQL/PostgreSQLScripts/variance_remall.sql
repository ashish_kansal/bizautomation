CREATE OR REPLACE FUNCTION Variance_RemAll (
	p_ActivityID NUMERIC(18,0)
)
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE p_VarianceKey uuid;
BEGIN
	

	-- Read the variance correlation ID from the root 
	--
	SELECT
		VarianceID INTO p_VarianceKey
	FROM
		Activity
	WHERE
		ActivityID = p_ActivityID;

	IF NOT p_VarianceKey IS NULL THEN
		UPDATE Activity SET VarianceID = NULL WHERE ActivityID = p_ActivityID;
		DELETE FROM Activity WHERE VarianceID = p_VarianceKey;
	END IF;
RETURN;
END; $$;


