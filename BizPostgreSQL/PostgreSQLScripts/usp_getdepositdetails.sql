-- Stored procedure definition script USP_GetDepositDetails for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_GetDepositDetails(v_numDepositId NUMERIC(9,0) DEFAULT 0 ,
    v_numDomainId NUMERIC(9,0) DEFAULT 0 ,
    v_tintMode SMALLINT DEFAULT 0, -- 1= Get Undeposited Payments 
    v_numCurrencyID NUMERIC(9,0) DEFAULT 0,
    v_CurrentPage INTEGER DEFAULT NULL,
    v_PageSize INTEGER DEFAULT NULL,
    INOUT v_TotRecs INTEGER  DEFAULT NULL,
    v_numAccountClass NUMERIC(18,0) DEFAULT 0, INOUT SWV_RefCur refcursor default null, INOUT SWV_RefCur2 refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numBaseCurrencyID  NUMERIC;
   v_BaseCurrencySymbol  VARCHAR(3);
   v_firstRec  INTEGER;
   v_lastRec  INTEGER;
BEGIN
   select   coalesce(numCurrencyID,0) INTO v_numBaseCurrencyID FROM Domain WHERE numDomainId = v_numDomainId;
   select   coalesce(varCurrSymbol,'') INTO v_BaseCurrencySymbol FROM Currency WHERE numCurrencyID = v_numBaseCurrencyID;
      
   v_firstRec :=(v_CurrentPage::bigint -1)*v_PageSize::bigint;
   v_lastRec :=(v_CurrentPage::bigint*v_PageSize::bigint+1);
			  
   IF v_tintMode = 0 then
            
      open SWV_RefCur for
      SELECT  numDepositId AS "numDepositId",
                        fn_GetComapnyName(numDivisionID) AS "vcCompanyName" ,
                        numDivisionID AS "numDivisionID" ,
                        numChartAcntId AS "numChartAcntId" ,
                        FormatedDateFromDate(dtDepositDate,DM.numDomainId) AS "dtDepositDate" ,
                        dtDepositDate AS "dtOrigDepositDate",
                        monDepositAmount AS "monDepositAmount" ,
                        numPaymentMethod AS "numPaymentMethod" ,
                        vcReference AS "vcReference" ,
                        vcMemo AS "vcMemo" ,
                        tintDepositeToType AS "tintDepositeToType",
						tintDepositePage AS "tintDepositePage",
						bitDepositedToAcnt AS "bitDepositedToAcnt"
                        ,DM.numCurrencyID AS "numCurrencyID"
						,coalesce(C.varCurrSymbol,'') AS "varCurrSymbol"
						,coalesce(NULLIF(DM.fltExchangeRate,0),1) AS "fltExchangeRateReceivedPayment"
						,v_BaseCurrencySymbol AS "BaseCurrencySymbol"
						,coalesce(monRefundAmount,0) AS "monRefundAmount"
						,coalesce(DM.numAccountClass,0) AS "numAccountClass"
      FROM    DepositMaster DM
      LEFT JOIN Currency C ON C.numCurrencyID = DM.numCurrencyID
      WHERE   numDepositId = v_numDepositId
      AND DM.numDomainId = v_numDomainId;

      open SWV_RefCur2 for
      SELECT  DD.numDepositeDetailID AS "numDepositeDetailID",
                        DD.numDepositID AS "numDepositID",
                        DD.numOppBizDocsID AS "numOppBizDocsID",
                        DD.numOppID AS "numOppID",
                        DD.monAmountPaid AS "monAmountPaid",
                        DD.numChildDepositID AS "numChildDepositID",
                        DD.numPaymentMethod AS "numPaymentMethod",
                        DD.vcMemo AS "vcMemo",
                        DD.vcReference AS "vcReference",
                        DD.numClassID AS "numClassID",
                        DD.numProjectID AS "numProjectID",
                        DD.numReceivedFrom AS "numReceivedFrom",
                        DD.numAccountID AS "numAccountID",
                        GJD.numTransactionId AS "numTransactionID",
                        GJD1.numTransactionId AS "numTransactionIDHeader"
                        ,(CASE WHEN coalesce(DD.numChildDepositID,0) = 0 THEN coalesce(DMO.numCurrencyID,0) ELSE coalesce(DM.numCurrencyID,0) END) AS "numCurrencyID"
						,(CASE WHEN coalesce(DD.numChildDepositID,0) = 0 THEN coalesce(NULLIF(DMO.fltExchangeRate,0),1) ELSE coalesce(NULLIF(DM.fltExchangeRate,0),1) END) AS "fltExchangeRateReceivedPayment"
      FROM    DepositeDetails DD
      INNER JOIN DepositMaster DMO ON DD.numDepositID = DMO.numDepositId
      LEFT JOIN DepositMaster DM ON DM.numDepositId = DD.numChildDepositID
      LEFT JOIN General_Journal_Details GJD ON GJD.numReferenceID = DD.numDepositeDetailID AND GJD.tintReferenceType = 7
      LEFT JOIN General_Journal_Details GJD1 ON GJD1.numReferenceID = DD.numDepositID AND GJD1.tintReferenceType = 6
      WHERE   DD.numDepositID = v_numDepositId;
   ELSEIF v_tintMode = 1
   then
                
                
                --Get Undeposited payments
                
				--Get Saved deposit entries for edit mode
      DROP TABLE IF EXISTS tt_TEMPDEPOSITS CASCADE;
      CREATE TEMPORARY TABLE tt_TEMPDEPOSITS ON COMMIT DROP AS
         SELECT * FROM(SELECT  Row_number() OVER(ORDER BY DM.dtDepositDate DESC) AS row,
							DD.numDepositeDetailID ,
							DM.numDepositId ,
							DM.numChartAcntId ,
							FormatedDateFromDate(dtDepositDate,DM.numDomainId) AS dtDepositDate ,
							DD.monAmountPaid AS monDepositAmount,
							DD.numPaymentMethod ,
							DD.vcReference ,
							DD.vcMemo ,
							DD.numReceivedFrom AS numDivisionID,
							fn_GetComapnyName(DM.numDivisionID) AS ReceivedFrom,
							bitDepositedToAcnt,
							 (SELECT
            CASE tintSourceType
            WHEN 1 THEN GetListIemName(tintSource)
            WHEN 2 THEN(SELECT  vcSiteName FROM Sites WHERE numSiteID = tintSource LIMIT 1)
            WHEN 3 THEN(SELECT  vcProviderName FROM WebAPI WHERE WebApiId = tintSource LIMIT 1)
            ELSE '' END
            FROM OpportunityMaster WHERE numOppId IN(SELECT  numOppID FROM DepositeDetails WHERE numDepositID = DM.numDepositId AND numOppID > 0 LIMIT 1)) AS vcSource,
							CASE WHEN DM.numTransHistoryID > 0 THEN GetListIemName(coalesce(TH.numCardType,0)) ELSE '' END  AS vcCardType
							,DM.numCurrencyID
							,coalesce(C.varCurrSymbol,'') AS varCurrSymbol
							,coalesce(NULLIF(DM.fltExchangeRate,0),1) AS fltExchangeRateReceivedPayment
							,v_BaseCurrencySymbol AS BaseCurrencySymbol
         FROM    DepositeDetails DD
         INNER JOIN DepositMaster DM ON DD.numChildDepositID = DM.numDepositId
         LEFT JOIN TransactionHistory TH ON TH.numTransHistoryID = DM.numTransHistoryID
         LEFT JOIN Currency C ON C.numCurrencyID = DM.numCurrencyID
         WHERE   DD.numDepositID = v_numDepositId AND (coalesce(DM.numAccountClass,0) = v_numAccountClass OR v_numAccountClass = 0)
         UNION
         SELECT Row_number() OVER(ORDER BY DM.dtDepositDate DESC) AS row,
							0 AS numDepositeDetailID ,
							numDepositId ,
							numChartAcntId ,
							FormatedDateFromDate(dtDepositDate,DM.numDomainId) AS dtDepositDate ,
							monDepositAmount ,
							numPaymentMethod ,
							vcReference ,
							vcMemo ,
							DM.numDivisionID,
							fn_GetComapnyName(DM.numDivisionID) AS ReceivedFrom,
							bitDepositedToAcnt,
							(SELECT
            CASE tintSourceType
            WHEN 1 THEN GetListIemName(tintSource)
            WHEN 2 THEN(SELECT  vcSiteName FROM Sites WHERE numSiteID = tintSource LIMIT 1)
            WHEN 3 THEN(SELECT  vcProviderName FROM WebAPI WHERE WebApiId = tintSource LIMIT 1)
            ELSE '' END
            FROM OpportunityMaster WHERE numOppId IN(SELECT  numOppID FROM DepositeDetails WHERE numDepositID = DM.numDepositId AND numOppID > 0 LIMIT 1)) AS vcSource,
							CASE WHEN DM.numTransHistoryID > 0 THEN GetListIemName(coalesce(TH.numCardType,0)) ELSE '' END  AS vcCardType
							,DM.numCurrencyID
							,coalesce(C.varCurrSymbol,'') AS varCurrSymbol
							,coalesce(NULLIF(DM.fltExchangeRate,0),1) AS fltExchangeRateReceivedPayment
							,v_BaseCurrencySymbol AS BaseCurrencySymbol
         FROM    DepositMaster DM
         INNER JOIN Chart_Of_Accounts COA ON DM.numChartAcntId = COA.numAccountId
         LEFT JOIN TransactionHistory TH ON DM.numTransHistoryID = TH.numTransHistoryID
         LEFT JOIN Currency C ON C.numCurrencyID = DM.numCurrencyID
         WHERE   DM.numDomainId = v_numDomainId
         AND tintDepositeToType = 2
         AND coalesce(bitDepositedToAcnt,false) = false
         AND (coalesce(DM.numCurrencyID,v_numBaseCurrencyID) = v_numCurrencyID OR v_numCurrencyID = 0)
         AND (coalesce(DM.numAccountClass,0) = v_numAccountClass OR v_numAccountClass = 0))  Deposits;
      select   COUNT(*) INTO v_TotRecs FROM tt_TEMPDEPOSITS;
      open SWV_RefCur for
      SELECT  
			row AS "row",
			numDepositeDetailID AS "numDepositeDetailID",
			numDepositId AS "numDepositId",
			numChartAcntId AS "numChartAcntId",
			dtDepositDate AS "dtDepositDate",
			monAmountPaid AS "monDepositAmount",
			numPaymentMethod AS "numPaymentMethod",
			vcReference AS "vcReference",
			vcMemo AS "vcMemo",
			numReceivedFrom AS "numDivisionID",
			ReceivedFrom AS "ReceivedFrom",
			bitDepositedToAcnt AS "bitDepositedToAcnt",
			vcSource AS "vcSource"
			,CASE WHEN DM.numTransHistoryID > 0 THEN GetListIemName(coalesce(TH.numCardType,0)) ELSE '' END  AS "vcCardType"
			,DM.numCurrencyID AS "numCurrencyID"
			,coalesce(C.varCurrSymbol,'') AS "varCurrSymbol"
			,coalesce(NULLIF(DM.fltExchangeRate,0),1) AS "fltExchangeRateReceivedPayment"
			,v_BaseCurrencySymbol AS "BaseCurrencySymbol" 
		FROM 
			tt_TEMPDEPOSITS
      WHERE   row > v_firstRec
      AND row < v_lastRec;
                    
					
			-- get new deposite Entries
      open SWV_RefCur2 for
      SELECT  DD.numDepositeDetailID AS "numDepositeDetailID",
							DM.numDepositId AS "numDepositId",
							FormatedDateFromDate(dtDepositDate,DM.numDomainId) AS dtDepositDate ,
							DD.monAmountPaid AS "monAmountPaid",
							DD.numPaymentMethod AS "numPaymentMethod",
							DD.vcReference AS "vcReference",
							DD.vcMemo AS "vcMemo",
							DD.numAccountID AS "numAccountID",
--							 COA.vcAccountCode numAcntType,
							DD.numClassID AS "numClassID",
							DD.numProjectID AS "numProjectID",
							DD.numReceivedFrom AS "numReceivedFrom",
							fn_GetComapnyName(DD.numReceivedFrom) AS "ReceivedFrom",
							bitDepositedToAcnt AS "bitDepositedToAcnt"
      FROM    DepositeDetails DD
      INNER JOIN DepositMaster DM ON DD.numDepositID = DM.numDepositId
--							LEFT JOIN dbo.Chart_Of_Accounts COA ON DD.numAccountID = COA.numAccountId
      WHERE   DD.numDepositID = v_numDepositId AND coalesce(numChildDepositID,0) = 0
      AND (coalesce(DM.numAccountClass,0) = v_numAccountClass OR v_numAccountClass = 0);
   end if;
   RETURN;
END; $$;


