CREATE OR REPLACE FUNCTION usp_DeleteOppDocs(v_numDocID NUMERIC(9,0) DEFAULT 0)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   DELETE FROM  opptdocuments WHERE numDocID = v_numDocID;
   RETURN;
END; $$;


