DROP FUNCTION IF EXISTS usp_GetCommissionsContacts;

CREATE OR REPLACE FUNCTION usp_GetCommissionsContacts(v_numDomainID NUMERIC,
v_byteMode SMALLINT, INOUT SWV_RefCur refcursor)
LANGUAGE plpgsql
   AS $$
BEGIN
   IF v_byteMode = 1 then
  
      open SWV_RefCur for
      select  CC.numCommContactID,D.numDivisionID,A.numContactId,A.vcFirstName || ' ' || A.vcLastname || '(' || C.vcCompanyName || ')' as vcUserName,C.vcCompanyName
      from CommissionContacts CC JOIN DivisionMaster D ON CC.numDivisionID = D.numDivisionID
      JOIN AdditionalContactsInformation A ON D.numDivisionID = A.numDivisionId
      join CompanyInfo C on C.numCompanyId = D.numCompanyID
      where CC.numDomainID = v_numDomainID AND numCompanyType <> 93 ORDER BY CONCAT(A.vcFirstName,' ',A.vcLastname) ASC;
   ELSE
      open SWV_RefCur for
      select  CC.numCommContactID,D.numDivisionID,C.vcCompanyName
      from CommissionContacts CC JOIN DivisionMaster D ON CC.numDivisionID = D.numDivisionID
      join CompanyInfo C on C.numCompanyId = D.numCompanyID
      where CC.numDomainID = v_numDomainID;
   end if;
   RETURN;
END; $$;


