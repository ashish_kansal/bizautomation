CREATE OR REPLACE FUNCTION CompanyInfo_IUD_TrFunc()
RETURNS TRIGGER LANGUAGE plpgsql
   AS $$
   DECLARE
   v_action  CHAR(1) DEFAULT 'I';
BEGIN
	IF (TG_OP = 'DELETE') THEN
		INSERT INTO ElasticSearchModifiedRecords(vcModule,numRecordID, vcAction, numDomainID) SELECT 'Organization',numDivisionID,'Delete',OLD.numDomainID FROM DivisionMaster WHERE DivisionMaster.numCompanyID = OLD.numCompanyId;
		RETURN OLD;
	ELSIF (TG_OP = 'UPDATE') THEN
		INSERT INTO ElasticSearchModifiedRecords(vcModule,numRecordID, vcAction, numDomainID)  VALUES('Organization', NEW.numCompanyId, 'Update', NEW.numDomainID);

		IF NEW.vcCompanyName <> OLD.vcCompanyName then
            INSERT INTO ElasticSearchModifiedRecords(vcModule,numRecordID, vcAction, numDomainID) SELECT 'Contact',numContactId,'Update',NEW.numDomainID FROM DivisionMaster INNER JOIN AdditionalContactsInformation ON DivisionMaster.numDivisionID = AdditionalContactsInformation.numDivisionId WHERE NEW.numCompanyId = DivisionMaster.numCompanyID;
            INSERT INTO ElasticSearchModifiedRecords(vcModule,numRecordID, vcAction, numDomainID) SELECT 'Opp/Order',numOppId,'Update',NEW.numDomainID FROM DivisionMaster INNER JOIN OpportunityMaster ON DivisionMaster.numDivisionID = OpportunityMaster.numDivisionId WHERE NEW.numCompanyId = DivisionMaster.numCompanyID;
            INSERT INTO ElasticSearchModifiedRecords(vcModule,numRecordID, vcAction, numDomainID) SELECT 'ActionItem',numCommId,'Update',NEW.numDomainID FROM DivisionMaster INNER JOIN Communication ON DivisionMaster.numDivisionID = Communication.numDivisionID WHERE NEW.numCompanyId = DivisionMaster.numCompanyID;
            INSERT INTO ElasticSearchModifiedRecords(vcModule,numRecordID, vcAction, numDomainID) SELECT 'BizDoc',numOppBizDocsId,'Update',NEW.numDomainID FROM DivisionMaster INNER JOIN OpportunityMaster ON DivisionMaster.numDivisionID = OpportunityMaster.numDivisionId INNER JOIN OpportunityBizDocs ON OpportunityMaster.numOppId = OpportunityBizDocs.numoppid WHERE NEW.numCompanyId = DivisionMaster.numCompanyID;
            INSERT INTO ElasticSearchModifiedRecords(vcModule,numRecordID, vcAction, numDomainID) SELECT 'Case',numCaseId,'Update',NEW.numDomainID FROM DivisionMaster INNER JOIN Cases ON DivisionMaster.numDivisionID = Cases.numDivisionID WHERE NEW.numCompanyId = DivisionMaster.numCompanyID;
            INSERT INTO ElasticSearchModifiedRecords(vcModule,numRecordID, vcAction, numDomainID) SELECT 'Project',numProId,'Update',NEW.numDomainID FROM DivisionMaster INNER JOIN ProjectsMaster ON DivisionMaster.numDivisionID = ProjectsMaster.numDivisionId WHERE NEW.numCompanyId = DivisionMaster.numCompanyID;
            INSERT INTO ElasticSearchModifiedRecords(vcModule,numRecordID, vcAction, numDomainID) SELECT 'Return',numReturnHeaderID,'Update',NEW.numDomainID FROM DivisionMaster INNER JOIN ReturnHeader ON DivisionMaster.numDivisionID = ReturnHeader.numDivisionId WHERE NEW.numCompanyId = DivisionMaster.numCompanyID;
         END IF;

		RETURN NEW;
	ELSIF (TG_OP = 'INSERT') THEN
		INSERT INTO ElasticSearchModifiedRecords(vcModule,numRecordID, vcAction, numDomainID)  VALUES('Organization', NEW.numCompanyId, 'Insert', NEW.numDomainID);
		RETURN NEW;
	END IF;

	RETURN NULL;
END; $$;
CREATE TRIGGER CompanyInfo_IUD AFTER INSERT OR UPDATE OR DELETE ON CompanyInfo FOR EACH ROW EXECUTE PROCEDURE CompanyInfo_IUD_TrFunc();


