-- Stored procedure definition script Recurrence_Upd for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION Recurrence_Upd(v_EndDateUtc		TIMESTAMP,
	v_DayOfWeekMaskUtc	INTEGER,
	v_UtcOffset		INTEGER,
	v_DayOfMonth		INTEGER,
	v_MonthOfYear		INTEGER,
	v_PeriodMultiple		INTEGER,
	v_Period			CHAR(1),
	v_EditType		INTEGER,
	v_LastReminderDateTimeUtc	TIMESTAMP,
	v_RecurrenceID		INTEGER)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   UPDATE
   Recurrence
   SET
   EndDateUtc = v_EndDateUtc,DayOfWeekMaskUtc = v_DayOfWeekMaskUtc,UtcOffset = v_UtcOffset,
   DayOfMonth = v_DayOfMonth,MonthOfYear = v_MonthOfYear,
   PeriodMultiple = v_PeriodMultiple,Period = v_Period,EditType = v_EditType,
   LastReminderDateTimeUtc = v_LastReminderDateTimeUtc
   WHERE
   Recurrence.recurrenceid = v_RecurrenceID;
   RETURN;
END; $$;


