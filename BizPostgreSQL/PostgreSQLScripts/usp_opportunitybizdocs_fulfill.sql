

/****** Object:  StoredProcedure [dbo].[USP_OpportunityBizDocs_FulFill]    Script Date: 5/2/2021 1:54:19 PM ******/


Create or replace FUNCTION USP_OpportunityBizDocs_FulFill(v_numDomainID NUMERIC(18,0),
	v_numUserCntID NUMERIC(18,0),
	v_numOppId NUMERIC(18,0),
	v_numOppBizDocID NUMERIC(18,0),
	v_vcItems TEXT,
	v_dtShippedDate TIMESTAMP)
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_vcItemType  CHAR(1);
   v_bitDropShip  BOOLEAN;
   v_numOppItemID  INTEGER;
   v_numWarehouseItemID  INTEGER;
   v_numQty  DOUBLE PRECISION;
   v_numQtyShipped  DOUBLE PRECISION;
   v_numXmlQty  DOUBLE PRECISION;
   v_vcSerialLot  TEXT;
   v_numSerialCount  INTEGER;
   v_bitSerial  BOOLEAN;
   v_bitLot  BOOLEAN;
   v_bitAssembly  BOOLEAN;
   v_bitKit  BOOLEAN;

   v_i  INTEGER DEFAULT 1;
   v_COUNT  INTEGER;  
   v_numNewQtyShipped  DOUBLE PRECISION;


	/* VERIFY BIZDOC IS EXISTS OR NOT */
   v_idoc  INTEGER;
   v_vcDescription  VARCHAR(300);
   v_numAllocation  DOUBLE PRECISION;

   v_j  INTEGER DEFAULT 0;
   v_ChildCount  INTEGER;
   v_vcChildDescription  VARCHAR(300);
   v_numChildAllocation  DOUBLE PRECISION;
   v_numOppChildItemID  INTEGER;
   v_numChildItemCode  INTEGER;
   v_bitChildIsKit  BOOLEAN;
   v_numChildWarehouseItemID  INTEGER;
   v_numChildQty  DOUBLE PRECISION;
   v_numChildQtyShipped  DOUBLE PRECISION;

   v_k  INTEGER DEFAULT 1;
   v_CountKitChildItems  INTEGER;
   v_vcKitChildDescription  VARCHAR(300);
   v_numKitChildAllocation  DOUBLE PRECISION;
   v_numOppKitChildItemID  NUMERIC(18,0);
   v_numKitChildItemCode  NUMERIC(18,0);
   v_numKitChildWarehouseItemID  NUMERIC(18,0);
   v_numKitChildQty  DOUBLE PRECISION;
   v_numKitChildQtyShipped  DOUBLE PRECISION;

   my_ex_state TEXT;
	my_ex_message TEXT;
	my_ex_detail TEXT;
	my_ex_hint TEXT;
	my_ex_ctx TEXT;
BEGIN
-- This function was converted on Sun May 02 13:58:58 2021 using Ispirer SQLWays 9.0 Build 6532 64bit Licensed to BizAutomation - Sandeep Patel - India - Ispirer MnMTK 2020 MSSQLServer to PostgreSQL Database Migration Professional Project License (50 GB, 750 Tables, 426500 LOC, 3 Extension Requests, 1 Month, 20210508).
   BEGIN
      IF NOT EXISTS(SELECT numOppBizDocsId FROM OpportunityBizDocs WHERE numOppBizDocsId = v_numOppBizDocID) then
	
         RAISE EXCEPTION 'FULFILLMENT_BIZDOC_IS_DELETED';
      end if;

	/* VERIFY WHETHER BIZDOC IS ALREADY FULLFILLED BY SOMEBODY ELSE */
      IF EXISTS(SELECT numOppBizDocsId FROM OpportunityBizDocs WHERE numOppBizDocsId = v_numOppBizDocID AND bitFulfilled = true) then
	
         RAISE EXCEPTION 'FULFILLMENT_BIZDOC_ALREADY_FULFILLED';
      end if;
      -- BEGIN TRANSACTION
UPDATE WareHouseItmsDTL WHIDL
      SET numQty =(CASE WHEN Item.bitLotNo = true THEN coalesce(WHIDL.numQty,0)+coalesce(OWSI.numQty,0) ELSE 1 END)
      FROM
      OppWarehouseSerializedItem OWSI, WareHouseItems INNER JOIN Item ON WareHouseItems.numItemID = Item.numItemCode
      WHERE(WHIDL.numWareHouseItmsDTLID = OWSI.numWarehouseItmsDTLID
      AND WHIDL.numWareHouseItemID = OWSI.numWarehouseItmsID) AND WHIDL.numWareHouseItemID = WareHouseItems.numWareHouseItemID AND(OWSI.numOppID = v_numOppId
      AND OWSI.numOppBizDocsId IS NULL);
      BEGIN
         CREATE TEMP SEQUENCE tt_TempFinalTable_seq INCREMENT BY 1 START WITH 1;
         EXCEPTION WHEN OTHERS THEN
            NULL;
      END;
      DROP TABLE IF EXISTS tt_TEMPFINALTABLE CASCADE;
      CREATE TEMPORARY TABLE tt_TEMPFINALTABLE
      (
         ID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1),
         numItemCode NUMERIC(18,0),
         vcItemType CHAR(1),
         bitSerial BOOLEAN,
         bitLot BOOLEAN,
         bitAssembly BOOLEAN,
         bitKit BOOLEAN,
         numOppItemID NUMERIC(18,0),
         numWarehouseItemID NUMERIC(18,0),
         numQtyShipped DOUBLE PRECISION,
         numQty DOUBLE PRECISION,
         bitDropShip BOOLEAN,
         vcSerialLot TEXT
      );
      INSERT INTO tt_TEMPFINALTABLE(numItemCode,
				vcItemType,
				bitSerial,
				bitLot,
				bitAssembly,
				bitKit,
				numOppItemID,
				numWarehouseItemID,
				numQtyShipped,
				numQty,
				bitDropShip,
				vcSerialLot)
      SELECT
      OpportunityBizDocItems.numItemCode,
				coalesce(Item.charItemType,''),
				coalesce(Item.bitSerialized,false),
				coalesce(Item.bitLotNo,false),
				coalesce(Item.bitAssembly,false),
				coalesce(Item.bitKitParent,false),
				coalesce(OpportunityItems.numoppitemtcode,0),
				coalesce(OpportunityBizDocItems.numWarehouseItmsID,0),
				coalesce(OpportunityItems.numQtyShipped,0),
				coalesce(OpportunityBizDocItems.numUnitHour,0),
				coalesce(OpportunityItems.bitDropShip,false),
				''
      FROM
      OpportunityBizDocs
      INNER JOIN
      OpportunityBizDocItems
      ON
      OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
      INNER JOIN
      OpportunityItems
      ON
      OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtCode
      INNER JOIN
      Item
      ON
      OpportunityBizDocItems.numItemCode = Item.numItemCode
      WHERE
      numOppBizDocsId = v_numOppBizDocID
      AND coalesce(OpportunityItems.bitDropShip,false) = false
      AND coalesce(OpportunityItems.numWarehouseItmsID,0) > 0;
      DROP TABLE IF EXISTS tt_TEMPITEMXML CASCADE;
      CREATE TEMPORARY TABLE tt_TEMPITEMXML
      (
         numOppItemID NUMERIC(18,0),
         numQty DOUBLE PRECISION,
         numWarehouseItemID NUMERIC(18,0),
         vcSerialLot TEXT
      );
      SELECT * INTO v_idoc FROM SWF_Xml_PrepareDocument(v_vcItems);
      INSERT INTO
      tt_TEMPITEMXML
      SELECT
      numOppItemID,
				numQty,
				numWarehouseItemID,
				vcSerialLot
      FROM
      SWF_OpenXml(v_idoc,'/Items/Item','numOppItemID |numQty |numWarehouseItemID |vcSerialLot') SWA_OpenXml(numOppItemID NUMERIC(18,0),numQty DOUBLE PRECISION,numWarehouseItemID NUMERIC(18,0),
      vcSerialLot TEXT);
      select   COUNT(*) INTO v_COUNT FROM tt_TEMPFINALTABLE;
      DROP TABLE IF EXISTS tt_TEMPLOT CASCADE;
      CREATE TEMPORARY TABLE tt_TEMPLOT
      (
         vcLotNo VARCHAR(100),
         numQty DOUBLE PRECISION
      );
      WHILE v_i <= v_COUNT LOOP
         select   numOppItemID, numWarehouseItemID, numQty, bitSerial, bitLot INTO v_numOppItemID,v_numWarehouseItemID,v_numQty,v_bitSerial,v_bitLot FROM
         tt_TEMPFINALTABLE WHERE
         ID = v_i;

				--GET QUENTITY RECEIVED FROM XML PARAMETER
         select   numQty, vcSerialLot, (CASE WHEN (v_bitSerial = true OR v_bitLot = true) THEN(SELECT COUNT(*) FROM SplitString(vcSerialLot,',')) ELSE 0 END) INTO v_numXmlQty,v_vcSerialLot,v_numSerialCount FROM
         tt_TEMPITEMXML WHERE
         numOppItemID = v_numOppItemID; 

				--CHECK IF QUANTITY OF ITEM IN OpportunityBizDocItems TBALE IS SAME AS QUANTITY SUPPLIED FROM XML
         IF v_numQty <> v_numXmlQty then
				
            RAISE EXCEPTION 'QTY_MISMATCH';
         end if;

				--IF ITEM IS SERIAL ITEM CHEKC THAT VALID AND REQUIRED NUMBER OF SERIALS ARE PROVIDED BY USER
         IF v_bitSerial = true then
				
            IF v_numSerialCount <> v_numQty then
					
               RAISE EXCEPTION 'REQUIRED_SERIALS_NOT_PROVIDED';
            ELSE
               IF(SELECT COUNT(*) FROM WareHouseItmsDTL WHERE numWareHouseItemID = v_numWarehouseItemID AND numQty > 0 AND vcSerialNo IN(SELECT OutParam FROM SplitString(v_vcSerialLot,','))) <> v_numSerialCount then
						
                  RAISE EXCEPTION 'INVALID_SERIAL_NUMBERS';
               end if;
            end if;
         end if;
         IF v_bitLot = true then
				
					-- DELETE DATA OF PREVIOUS ITERATION FROM TABLE
            DELETE FROM tt_TEMPLOT;

					--CONVERTS COMMA SEPERATED STRING TO LOTNo AND QTY 
            INSERT INTO
            tt_TEMPLOT
            SELECT
            SUBSTR(OutParam,0,coalesce(POSITION(substring(OutParam from E'\\(') IN OutParam),0)),
						CAST(SUBSTR(OutParam,(coalesce(POSITION(substring(OutParam from E'\\(') IN OutParam),0)+1),(coalesce(POSITION(substring(OutParam from E'\\)') IN OutParam),0) -coalesce(POSITION(substring(OutParam from E'\\(') IN OutParam),0) -1)) AS INTEGER)
            FROM
            SplitString(v_vcSerialLot,',');

					--CHECKS WHETHER PROVIDED QTY OF LOT NO IS SAME IS REQUIRED QTY
            IF(SELECT SUM(numQty) FROM tt_TEMPLOT) <> v_numQty then
					
               RAISE EXCEPTION 'REQUIRED_LOTNO_NOT_PROVIDED';
            ELSE
						-- CHECKS WHETHER LOT NO EXISTS IN WareHouseItmsDTL
               IF(SELECT COUNT(*) FROM WareHouseItmsDTL WHERE numWareHouseItemID = v_numWarehouseItemID AND vcSerialNo IN(SELECT vcLotNo FROM tt_TEMPLOT)) <>(SELECT COUNT(*) FROM tt_TEMPLOT) then
						
                  RAISE EXCEPTION 'INVALID_LOT_NUMBERS';
               ELSE
							-- CHECKS WHETHER LOT NO HAVE ENOUGH QTY TO FULFILL ITEM QTY
                  IF(SELECT
                  COUNT(*)
                  FROM
                  WareHouseItmsDTL
                  INNER JOIN
                  tt_TEMPLOT TEMPLOT
                  ON
                  WareHouseItmsDTL.vcSerialNo = TEMPLOT.vcLotNo
                  WHERE
                  numWareHouseItemID = v_numWarehouseItemID
                  AND TEMPLOT.numQty > WareHouseItmsDTL.numQty) > 0 then
							
                     RAISE EXCEPTION 'SOME_LOTNO_DO_NOT_HAVE_ENOUGH_QTY';
                  end if;
               end if;
            end if;
         end if;
         UPDATE tt_TEMPFINALTABLE SET vcSerialLot = v_vcSerialLot WHERE ID = v_i;
         v_i := v_i::bigint+1;
      END LOOP;
      v_i := 1;
      select   COUNT(*) INTO v_COUNT FROM tt_TEMPFINALTABLE;
      DROP TABLE IF EXISTS tt_TEMPKITSUBITEMS CASCADE;
      CREATE TEMPORARY TABLE tt_TEMPKITSUBITEMS
      (
         ID INTEGER,
         numOppChildItemID NUMERIC(18,0),
         numChildItemCode NUMERIC(18,0),
         bitChildIsKit BOOLEAN,
         numChildWarehouseItemID NUMERIC(18,0),
         numChildQty DOUBLE PRECISION,
         numChildQtyShipped DOUBLE PRECISION
      );
      DROP TABLE IF EXISTS tt_TEMPKITCHILDKITSUBITEMS CASCADE;
      CREATE TEMPORARY TABLE tt_TEMPKITCHILDKITSUBITEMS
      (
         ID INTEGER,
         numOppKitChildItemID NUMERIC(18,0),
         numKitChildItemCode NUMERIC(18,0),
         numKitChildWarehouseItemID NUMERIC(18,0),
         numKitChildQty DOUBLE PRECISION,
         numKitChildQtyShipped DOUBLE PRECISION
      );
      WHILE v_i <= v_COUNT LOOP
         select   vcItemType, bitSerial, bitLot, bitAssembly, bitKit, bitDropShip, numOppItemID, numQty, numQtyShipped, numWarehouseItemID, vcSerialLot INTO v_vcItemType,v_bitSerial,v_bitLot,v_bitAssembly,v_bitKit,v_bitDropShip,
         v_numOppItemID,v_numQty,v_numQtyShipped,v_numWarehouseItemID,v_vcSerialLot FROM
         tt_TEMPFINALTABLE WHERE
         ID = v_i;

			--MAKE INVENTIRY CHANGES IF INVENTORY ITEM
         IF (UPPER(v_vcItemType) = 'P' AND v_bitDropShip = false) then
			
            v_vcDescription := CONCAT('SO Qty Shipped (Qty:',v_numQty,' Shipped:',v_numQtyShipped,')');
				 --  numeric(9, 0)
							 --  numeric(9, 0)
							 --  tinyint
							 --  varchar(100)
            IF v_bitKit = true then
				
					-- CLEAR DATA OF PREVIOUS ITERATION
               DELETE FROM tt_TEMPKITSUBITEMS;

					-- GET KIT SUB ITEMS DETAIL
               INSERT INTO tt_TEMPKITSUBITEMS(ID,
						numOppChildItemID,
						numChildItemCode,
						bitChildIsKit,
						numChildWarehouseItemID,
						numChildQty,
						numChildQtyShipped)
               SELECT
               ROW_NUMBER() OVER(ORDER BY numOppChildItemID),
						coalesce(numOppChildItemID,0),
						coalesce(I.numItemCode,0),
						coalesce(I.bitKitParent,false),
						coalesce(numWareHouseItemId,0),
						((coalesce(numQtyItemsReq_Orig,0)*coalesce(fn_UOMConversion(OKI.numUOMId,OKI.numChildItemID,v_numDomainID,I.numBaseUnit),1))*v_numQty),
						coalesce(numQtyShipped,0)
               FROM
               OpportunityKitItems OKI
               JOIN
               Item I
               ON
               OKI.numChildItemID = I.numItemCode
               WHERE
               charitemtype = 'P'
               AND coalesce(numWareHouseItemId,0) > 0
               AND OKI.numOppID = v_numOppId
               AND OKI.numOppItemID = v_numOppItemID;
               v_j := 1;
               select   COUNT(*) INTO v_ChildCount FROM tt_TEMPKITSUBITEMS;

					--LOOP OVER ALL CHILD ITEMS AND RELESE ALLOCATION
               WHILE v_j <= v_ChildCount LOOP
                  select   numOppChildItemID, numChildItemCode, bitChildIsKit, numChildWarehouseItemID, numChildQty, numChildQtyShipped INTO v_numOppChildItemID,v_numChildItemCode,v_bitChildIsKit,v_numChildWarehouseItemID,
                  v_numChildQty,v_numChildQtyShipped FROM
                  tt_TEMPKITSUBITEMS WHERE
                  ID = v_j;
                  select   coalesce(numAllocation,0) INTO v_numChildAllocation FROM WareHouseItems WHERE numWareHouseItemID = v_numChildWarehouseItemID;

						-- IF CHILD ITEM IS KIT THEN LOOK OUT FOR ITS CHILD ITEMS WHICH ARE STORED IN OpportunityKitChildItems TABLE
                  IF v_bitChildIsKit = true then
						
							-- IF KIT IS ADDED WITHIN KIT THEN THIS SUB KIT HAS ITS OWN ITEM CONFIGURATION WHICH ID STORED IN OpportunityKitChildItems
                     IF EXISTS(SELECT numOppKitChildItemID FROM OpportunityKitChildItems WHERE numOppID = v_numOppId AND numOppItemID = v_numOppItemID AND numOppChildItemID = v_numOppChildItemID) then
							
								-- CLEAR DATA OF PREVIOUS ITERATION
                        DELETE FROM tt_TEMPKITCHILDKITSUBITEMS;

								-- GET SUB KIT SUB ITEMS DETAIL
                        INSERT INTO tt_TEMPKITCHILDKITSUBITEMS(ID,
									numOppKitChildItemID,
									numKitChildItemCode,
									numKitChildWarehouseItemID,
									numKitChildQty,
									numKitChildQtyShipped)
                        SELECT
                        ROW_NUMBER() OVER(ORDER BY numOppKitChildItemID),
									coalesce(numOppKitChildItemID,0),
									coalesce(OKCI.numItemID,0),
									coalesce(OKCI.numWareHouseItemId,0),
									((coalesce(OKCI.numQtyItemsReq_Orig,0)*coalesce(fn_UOMConversion(OKCI.numUOMId,OKCI.numItemID,v_numDomainID,I.numBaseUnit),1))*v_numChildQty),
									coalesce(numQtyShipped,0)
                        FROM
                        OpportunityKitChildItems OKCI
                        JOIN
                        Item I
                        ON
                        OKCI.numItemID = I.numItemCode
                        WHERE
                        charitemtype = 'P'
                        AND coalesce(numWareHouseItemId,0) > 0
                        AND OKCI.numOppID = v_numOppId
                        AND OKCI.numOppItemID = v_numOppItemID
                        AND OKCI.numOppChildItemID = v_numOppChildItemID;
                        v_k := 1;
                        select   COUNT(*) INTO v_CountKitChildItems FROM tt_TEMPKITCHILDKITSUBITEMS;
                        WHILE v_k <= v_CountKitChildItems LOOP
                           select   numOppKitChildItemID, numKitChildItemCode, numKitChildWarehouseItemID, numKitChildQty, numKitChildQtyShipped INTO v_numOppKitChildItemID,v_numKitChildItemCode,v_numKitChildWarehouseItemID,
                           v_numKitChildQty,v_numKitChildQtyShipped FROM
                           tt_TEMPKITCHILDKITSUBITEMS WHERE
                           ID = v_k;
                           select   coalesce(numAllocation,0) INTO v_numKitChildAllocation FROM WareHouseItems WHERE numWareHouseItemID = v_numKitChildWarehouseItemID;

									--REMOVE QTY FROM ALLOCATION
                           v_numKitChildAllocation := v_numKitChildAllocation -v_numKitChildQty;
                           IF v_numKitChildAllocation >= 0 then
									
                              UPDATE
                              WareHouseItems
                              SET
                              numAllocation = v_numKitChildAllocation,dtModified = LOCALTIMESTAMP
                              WHERE
                              numWareHouseItemID = v_numKitChildWarehouseItemID;
                              UPDATE
                              OpportunityKitChildItems
                              SET
                              numQtyShipped = coalesce(numQtyShipped,0)+coalesce(v_numKitChildQty,0)
                              WHERE
                              numOppKitChildItemID = v_numOppKitChildItemID
                              AND numOppChildItemID = v_numOppChildItemID
                              AND numOppId = v_numOppId
                              AND numOppItemID = v_numOppItemID;
                              v_vcKitChildDescription := CONCAT('SO CHILD KIT Qty Shipped (Qty:',v_numKitChildQty,' Shipped:',v_numKitChildQtyShipped,
                              ')');
                              PERFORM USP_ManageWareHouseItems_Tracking(v_numWarehouseItemID := v_numKitChildWarehouseItemID:: VARCHAR,v_numReferenceID := v_numOppId:: VARCHAR,
                              v_tintRefType := 3:: VARCHAR,v_vcDescription := v_vcKitChildDescription,
                              v_numModifiedBy := v_numUserCntID:: VARCHAR,
                              v_numDomainID := v_numDomainID:: VARCHAR,
                           SWV_RefCur := null);
                           ELSE
                              RAISE EXCEPTION 'NOTSUFFICIENTQTY_ALLOCATION';
                           end if;
                           v_k := v_k::bigint+1;
                        END LOOP;
                     end if;
                     UPDATE
                     OpportunityKitItems
                     SET
                     numQtyShipped = coalesce(numQtyShipped,0)+coalesce(v_numChildQty,0)
                     WHERE
                     numOppChildItemID = v_numOppChildItemID
                     AND numOppId = v_numOppId
                     AND numOppItemID = v_numOppItemID;
                     v_vcChildDescription := CONCAT('SO KIT Qty Shipped (Qty:',v_numChildQty,' Shipped:',v_numChildQtyShipped,
                     ')');
                     PERFORM USP_ManageWareHouseItems_Tracking(v_numWarehouseItemID := v_numChildWarehouseItemID:: VARCHAR,v_numReferenceID := v_numOppId:: VARCHAR,
                     v_tintRefType := 3:: VARCHAR,v_vcDescription := v_vcChildDescription,
                     v_numModifiedBy := v_numUserCntID:: VARCHAR,
                     v_numDomainID := v_numDomainID:: VARCHAR,
                           SWV_RefCur := null);
                  ELSE
							--REMOVE QTY FROM ALLOCATION
                     v_numChildAllocation := v_numChildAllocation -v_numChildQty;
                     IF v_numChildAllocation >= 0 then
							
                        UPDATE
                        WareHouseItems
                        SET
                        numAllocation = v_numChildAllocation,dtModified = LOCALTIMESTAMP
                        WHERE
                        numWareHouseItemID = v_numChildWarehouseItemID;
                        UPDATE
                        OpportunityKitItems
                        SET
                        numQtyShipped = coalesce(numQtyShipped,0)+coalesce(v_numChildQty,0)
                        WHERE
                        numOppChildItemID = v_numOppChildItemID
                        AND numOppId = v_numOppId
                        AND numOppItemID = v_numOppItemID;
                        v_vcChildDescription := CONCAT('SO KIT Qty Shipped (Qty:',v_numChildQty,' Shipped:',v_numChildQtyShipped,
                        ')');
                        PERFORM USP_ManageWareHouseItems_Tracking(v_numWarehouseItemID := v_numChildWarehouseItemID:: VARCHAR,v_numReferenceID := v_numOppId:: VARCHAR,
                        v_tintRefType := 3:: VARCHAR,v_vcDescription := v_vcChildDescription,
                        v_numModifiedBy := v_numUserCntID:: VARCHAR,
                        v_numDomainID := v_numDomainID:: VARCHAR,
                           SWV_RefCur := null);
                     ELSE
                        RAISE EXCEPTION 'NOTSUFFICIENTQTY_ALLOCATION';
                     end if;
                  end if;
                  v_j := v_j::bigint+1;
               END LOOP;
            ELSE
               select   coalesce(numAllocation,0) INTO v_numAllocation FROM WareHouseItems WHERE numWareHouseItemID = v_numWarehouseItemID;
				       
					--REMOVE QTY FROM ALLOCATION
               v_numAllocation := v_numAllocation -v_numQty;
               IF (v_numAllocation >= 0) then
					
                  UPDATE
                  WareHouseItems
                  SET
                  numAllocation = v_numAllocation,dtModified = LOCALTIMESTAMP
                  WHERE
                  numWareHouseItemID = v_numWarehouseItemID;
                  IF v_bitSerial = true then
						
                     DELETE FROM OppWarehouseSerializedItem WHERE numOppID = v_numOppId AND numOppItemID = v_numOppItemID AND numWarehouseItmsID = v_numWarehouseItemID;
                     INSERT INTO OppWarehouseSerializedItem(numWarehouseItmsDTLID,
								numOppID,
								numOppItemID,
								numWarehouseItmsID,
								numQty,
								numOppBizDocsId)
                     SELECT(SELECT  numWarehouseItmsDTLID FROM WareHouseItmsDTL WHERE numWareHouseItemID = v_numWarehouseItemID AND vcSerialNo = OutParam AND  numQty = 1 LIMIT 1),
								v_numOppId,
								v_numOppItemID,
								v_numWarehouseItemID,
								1,
								v_numOppBizDocID
                     FROM
                     SplitString(v_vcSerialLot,',');
                     UPDATE WareHouseItmsDTL SET numQty = 0 WHERE numWareHouseItemID = v_numWarehouseItemID AND vcSerialNo IN(SELECT OutParam FROM SplitString(v_vcSerialLot,','));
                  end if;
                  IF v_bitLot = true then
						
                     DELETE FROM tt_TEMPLOT;
                     INSERT INTO
                     tt_TEMPLOT
                     SELECT
                     SUBSTR(OutParam,0,coalesce(POSITION(substring(OutParam from E'\\(') IN OutParam),0)),
								CAST(SUBSTR(OutParam,(coalesce(POSITION(substring(OutParam from E'\\(') IN OutParam),0)+1),(coalesce(POSITION(substring(OutParam from E'\\)') IN OutParam),0) -coalesce(POSITION(substring(OutParam from E'\\(') IN OutParam),0) -1)) AS INTEGER)
                     FROM
                     SplitString(v_vcSerialLot,',');
                     DELETE FROM OppWarehouseSerializedItem WHERE numOppID = v_numOppId AND numOppItemID = v_numOppItemID AND numWarehouseItmsID = v_numWarehouseItemID;
                     INSERT INTO OppWarehouseSerializedItem(numWarehouseItmsDTLID,
								numOppID,
								numOppItemID,
								numWarehouseItmsID,
								numQty,
								numOppBizDocsId)
                     SELECT(SELECT  numWarehouseItmsDTLID FROM WareHouseItmsDTL WHERE numWareHouseItemID = v_numWarehouseItemID AND vcSerialNo = TEMPLOT.vcLotNo AND numQty >= TEMPLOT.numQty LIMIT 1),
								v_numOppId,
								v_numOppItemID,
								v_numWarehouseItemID,
								TEMPLOT.numQty,
								v_numOppBizDocID
                     FROM
                     tt_TEMPLOT TEMPLOT;
                     UPDATE	WareHouseItmsDTL TEMPWareHouseItmsDTL
                     SET numQty = coalesce(TEMPWareHouseItmsDTL.numQty,0) -coalesce(TEMPLOT.numQty,0)
                     FROM
                     tt_TEMPLOT TEMPLOT
                     WHERE
                     TEMPWareHouseItmsDTL.vcSerialNo = TEMPLOT.vcLotNo AND TEMPWareHouseItmsDTL.numWareHouseItemID = v_numWarehouseItemID;
                  end if;
               ELSE
                  RAISE NOTICE 'PARENT: %',SUBSTR(CAST(v_numChildWarehouseItemID AS VARCHAR(30)),1,30);
                  RAISE EXCEPTION 'NOTSUFFICIENTQTY_ALLOCATION';
               end if;
            end if;
            PERFORM USP_ManageWareHouseItems_Tracking(v_numWarehouseItemID := v_numWarehouseItemID:: VARCHAR,v_numReferenceID := v_numOppId:: VARCHAR,
            v_tintRefType := 3:: VARCHAR,v_vcDescription := v_vcDescription,
            v_numModifiedBy := v_numUserCntID:: VARCHAR,v_numDomainID := v_numDomainID:: VARCHAR,
                           SWV_RefCur := null);
         end if;
         UPDATE
         OpportunityItems
         SET
         numQtyShipped = coalesce(numQtyShipped,0)+coalesce(v_numQty,0)
         WHERE
         numoppitemtCode = v_numOppItemID
         AND numOppId = v_numOppId;
         v_i := v_i::bigint+1;
      END LOOP;
      UPDATE OpportunityBizDocs SET bitFulfilled = true,dtShippedDate = v_dtShippedDate WHERE numOppBizDocsId = v_numOppBizDocID;
      -- COMMIT
EXCEPTION WHEN OTHERS THEN
         -- ROLLBACK TRANSACTION
		GET STACKED DIAGNOSTICS
			  my_ex_state   = RETURNED_SQLSTATE,
			  my_ex_message = MESSAGE_TEXT,
			  my_ex_detail  = PG_EXCEPTION_DETAIL,
			  my_ex_hint    = PG_EXCEPTION_HINT,
			  my_ex_ctx     = PG_EXCEPTION_CONTEXT;

			 RAISE EXCEPTION '% % % % %', my_ex_state, my_ex_message, my_ex_detail, my_ex_hint, my_ex_ctx;
   END;
   RETURN;
END; $$;


