-- Stored procedure definition script USP_DeleteFromCampaign for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
Create or replace FUNCTION USP_DeleteFromCampaign(v_numContactID NUMERIC(9,0),
v_numECampaignID NUMERIC(9,0))
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numConEmailCampID  NUMERIC(9,0);
BEGIN
   select   numConEmailCampID INTO v_numConEmailCampID FROM   ConECampaign WHERE  numContactID = v_numContactID
   AND numECampaignID = v_numECampaignID;
        
   UPDATE ConECampaign SET bitEngaged = false WHERE numConEmailCampID = v_numConEmailCampID;
   UPDATE AdditionalContactsInformation SET numECampaignID = NULL WHERE numContactId = v_numContactID;
   RETURN;
END; $$;
            


