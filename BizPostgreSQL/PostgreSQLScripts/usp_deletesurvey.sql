-- Stored procedure definition script usp_DeleteSurvey for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION usp_DeleteSurvey(v_numSurveyID NUMERIC,    
 v_numDomainID NUMERIC    
--      
)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
      
 --First Delete All Survey Responses related to this Survey.      
   DELETE FROM SurveyResponse
   WHERE numRespondantID IN(SELECT numRespondantID
   FROM SurveyRespondentsMaster
   WHERE numSurID = v_numSurveyID);      
    
 --Delete all Survey Resposen for the Current Survey   
   DELETE FROM  SurveyResponse
   WHERE numSurID = v_numSurveyID;     
  
 --Then Delete All Survey Respondants related to this Survey.      
   DELETE FROM  SurveyRespondentsChild
   WHERE numSurID = v_numSurveyID;    
    
   DELETE FROM  SurveyRespondentsMaster
   WHERE numSurID = v_numSurveyID;      
  
 --Deletes the Rules for the Survey Answers    
   DELETE FROM SurveyWorkflowRules
   WHERE numSurID = v_numSurveyID;      
    
 --Then Delete All Survey Ans Master related to this Survey.      
   DELETE FROM SurveyAnsMaster
   WHERE numSurID = v_numSurveyID;      
    
 --Then Delete All Survey Question Master related to this Survey.      
   DELETE FROM SurveyQuestionMaster
   WHERE numSurID = v_numSurveyID;      
 
  --Then Delete The Survey from the Survey Template.      
   DELETE FROM StyleSheetDetails WHERE numSurTemplateId IN(SELECT numSurTemplateId FROM  SurveyTemplate
   WHERE numSurID = v_numSurveyID
   AND numDomainID = v_numDomainID);
     
 --Then Delete The Survey from the Survey Template.      
   DELETE FROM SurveyTemplate
   WHERE numSurID = v_numSurveyID
   AND numDomainID = v_numDomainID;   
  
 --Then Delete The Survey from the Survey Master.      
   DELETE FROM SurveyMaster
   WHERE numSurID = v_numSurveyID
   AND numDomainID = v_numDomainID;
   RETURN;
END; $$;


