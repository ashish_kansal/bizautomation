-- Stored procedure definition script USP_UpdatedomainSMTP for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_UpdatedomainSMTP(v_numDomainID NUMERIC(9,0) DEFAULT 0,
v_SMTPAuth BOOLEAN DEFAULT NULL   ,      
v_SMTPSSL BOOLEAN DEFAULT NULL   ,      
v_vcSMTPPassword VARCHAR(100) DEFAULT NULL,       
v_SMTPServer VARCHAR(200) DEFAULT NULL,       
v_SMTPPort  NUMERIC(9,0) DEFAULT NULL,      
v_bitSMTPServer BOOLEAN DEFAULT NULL ,    
v_vcSMTPUserName VARCHAR(100) DEFAULT NULL,
v_vcPSMTPDisplayName VARCHAR(50) DEFAULT NULL)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   update Domain
   set
   bitPSMTPAuth = v_SMTPAuth,vcPSmtpPassword = v_vcSMTPPassword,vcPSMTPServer = v_SMTPServer,
   numPSMTPPort = v_SMTPPort,bitPSMTPSSL  =  v_SMTPSSL,bitPSMTPServer = v_bitSMTPServer,
   vcPSMTPUserName = v_vcSMTPUserName,vcPSMTPDisplayName = v_vcPSMTPDisplayName
   where numDomainId = v_numDomainID;
   RETURN;
END; $$;


