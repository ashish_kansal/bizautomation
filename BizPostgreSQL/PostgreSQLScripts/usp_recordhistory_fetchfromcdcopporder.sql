-- Stored procedure definition script USP_RecordHistory_FetchFromCDCOppOrder for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_RecordHistory_FetchFromCDCOppOrder()
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_i  NUMERIC(18,0) DEFAULT 1;
   v_iCOUNT  NUMERIC(18,0);
   v_vbStartlsn  BYTEA;
   v_vcUpdatemask  BYTEA;
   v_tintOperation  SMALLINT;
   v_sqlVal  BYTEA;
   v_numOppID  NUMERIC(18,0);
   v_numOppBizDocsID  NUMERIC(18,0);
   v_vcBizDocID  VARCHAR(300);
   v_numDomainID  NUMERIC(18,0);
   v_bintModifiedDate  TIMESTAMP;
   v_numModifiedBy  NUMERIC(18,0);

   v_Fld_ID  NUMERIC(18,0);
   v_Fld_Value  TEXT;

   v_ErrorMessage  VARCHAR(4000);
   v_ErrorNumber  TEXT;
   v_ErrorSeverity  INTEGER;
   v_ErrorState  INTEGER;
   v_ErrorLine  INTEGER;
   v_ErrorProcedure  VARCHAR(200);
   v_j  INTEGER DEFAULT 1;
   v_jCOUNT  INTEGER;
   v_dtDate  TIMESTAMP;
   v_vcColumnName  VARCHAR(200);
   v_vcOldValue  TEXT;
   v_vcNewValue  TEXT;
   v_numNewModifiedBy  NUMERIC(18,0) DEFAULT -1;
BEGIN
   DROP TABLE IF EXISTS tt_TEMPOPPORTUNITYHISTORY CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPOPPORTUNITYHISTORY
   (
      vbStartlsn BYTEA,
      numRecordID NUMERIC(18,0),
      numDomainID NUMERIC(18,0),
      dtDate TIMESTAMP,
      numUserCntID NUMERIC(18,0),
      vcEvent VARCHAR(200),
      vcFieldName VARCHAR(200),
      vcOldValue TEXT,
      vcNewValue TEXT,
      vcDescription TEXT,
      vcHiddenDescription TEXT,
      numFieldID NUMERIC(18,0),
      bitCustomField BOOLEAN
   );
   DROP TABLE IF EXISTS tt_TEMPTABLE CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPTABLE
   (
      ID NUMERIC(18,0),
      vbStartlsn BYTEA
   );
   INSERT INTO
   tt_TEMPTABLE
   SELECT
   ROW_NUMBER() OVER(ORDER BY __$start_lsn),
		__$start_lsn
   FROM
   cdc.dbo_OpportunityMaster_CT
   WHERE
   __$operation IN(2,3)
   GROUP BY
   __$start_lsn;

   select   COUNT(*) INTO v_iCOUNT FROM tt_TEMPTABLE;

   WHILE v_i <= v_iCOUNT LOOP
      select   vbStartlsn INTO v_vbStartlsn FROM tt_TEMPTABLE WHERE ID = v_i;
      select   __$operation, numOppID, numDomainID, (SELECT MAX(bintModifiedDate) FROM cdc.dbo_OpportunityMaster_CT WHERE __$start_lsn = v_vbStartlsn), numModifiedBy INTO v_tintOperation,v_numOppID,v_numDomainID,v_bintModifiedDate,v_numModifiedBy FROM
      cdc.dbo_OpportunityMaster_CT WHERE
      __$start_lsn = v_vbStartlsn    LIMIT 1;
      IF v_tintOperation = 2 then --INSERT
		
         INSERT INTO tt_TEMPOPPORTUNITYHISTORY(vbStartlsn,
				numRecordID,
				numDomainID,
				dtDate,
				numUserCntID,
				vcEvent,
				numFieldID,
				bitCustomField)
			VALUES(CAST(v_vbStartlsn AS BYTEA),
				v_numOppID,
				v_numDomainID,
				v_bintModifiedDate,
				v_numModifiedBy,
				CAST('New' AS VARCHAR(200)),
				NULL,
				CAST(0 AS BOOLEAN));
      ELSEIF v_tintOperation = 3
      then --UPDATE - (COLUMN VALUES BEFORE UPDATE)
         DROP TABLE IF EXISTS tt_TMEPCHANGEDCOLUMNS CASCADE;
         CREATE TEMPORARY TABLE tt_TMEPCHANGEDCOLUMNS
         (
            ID INTEGER,
            dtDate TIMESTAMP,
            vcColumnName VARCHAR(200),
            vcOldValue TEXT,
            vcNewValue TEXT
         );
         DELETE FROM tt_TMEPCHANGEDCOLUMNS;
         INSERT INTO tt_TMEPCHANGEDCOLUMNS(ID,
				dtDate,
				vcColumnName,
				vcOldValue,
				vcNewValue)
         SELECT
         ROW_NUMBER() OVER(ORDER BY TempUpdateBefore.__$start_lsn),
				sys.fn_cdc_map_lsn_to_time(TempUpdateBefore.__$start_lsn)+CAST((EXTRACT(DAY FROM TIMEZONE('UTC',now()) -LOCALTIMESTAMP)*60*60*24+EXTRACT(HOUR FROM TIMEZONE('UTC',now()) -LOCALTIMESTAMP)*60*60+EXTRACT(MINUTE FROM TIMEZONE('UTC',now()) -LOCALTIMESTAMP)*60+EXTRACT(SECOND FROM TIMEZONE('UTC',now()) -LOCALTIMESTAMP)) || 'second' as interval),
				TempUpdateBefore.column_name,
				TempUpdateBefore.old_value,
				TempUpdateAfter.new_value
         FROM(SELECT
            __$start_lsn,
						column_name,
						old_value
            FROM(SELECT
               __$start_lsn,
							CASE WHEN (sys.fn_cdc_is_bit_set(sys.fn_cdc_get_column_ordinal('dbo_OpportunityMaster','numAssignedTo'),__$update_mask) = 1) THEN CAST(numAssignedTo AS TEXT) ELSE NULL END AS numAssignedTo,
							CASE WHEN (sys.fn_cdc_is_bit_set(sys.fn_cdc_get_column_ordinal('dbo_OpportunityMaster','bintModifiedDate'),__$update_mask) = 1) THEN CAST(bintModifiedDate AS TEXT) ELSE NULL END AS bintModifiedDate,
							CASE WHEN (sys.fn_cdc_is_bit_set(sys.fn_cdc_get_column_ordinal('dbo_OpportunityMaster','numModifiedBy'),__$update_mask) = 1) THEN CAST(numModifiedBy AS TEXT) ELSE NULL END AS numModifiedBy,
							CASE WHEN (sys.fn_cdc_is_bit_set(sys.fn_cdc_get_column_ordinal('dbo_OpportunityMaster','numCampainID'),__$update_mask) = 1) THEN CAST(numCampainID as TEXT) ELSE NULL END AS numCampainID,
							CASE WHEN (sys.fn_cdc_is_bit_set(sys.fn_cdc_get_column_ordinal('dbo_OpportunityMaster','numStatus'),__$update_mask) = 1) THEN CAST(numStatus as TEXT) ELSE NULL END AS numStatus,
							CASE WHEN (sys.fn_cdc_is_bit_set(sys.fn_cdc_get_column_ordinal('dbo_OpportunityMaster','txtComments'),__$update_mask) = 1) THEN CAST(txtComments as TEXT) ELSE NULL END AS txtComments,
							CASE WHEN (sys.fn_cdc_is_bit_set(sys.fn_cdc_get_column_ordinal('dbo_OpportunityMaster','vcPOppName'),__$update_mask) = 1) THEN CAST(vcPOppName AS TEXT) ELSE NULL END AS vcPOppName,
							CASE WHEN (sys.fn_cdc_is_bit_set(sys.fn_cdc_get_column_ordinal('dbo_OpportunityMaster','intPEstimatedCloseDate'),__$update_mask) = 1) THEN CAST(intPEstimatedCloseDate AS TEXT) ELSE NULL END AS intPEstimatedCloseDate,
							CASE WHEN (sys.fn_cdc_is_bit_set(sys.fn_cdc_get_column_ordinal('dbo_OpportunityMaster','tintSource'),__$update_mask) = 1) THEN CAST(tintSource AS TEXT) ELSE NULL END AS tintSource,
							CASE WHEN (sys.fn_cdc_is_bit_set(sys.fn_cdc_get_column_ordinal('dbo_OpportunityMaster','numSalesOrPurType'),__$update_mask) = 1) THEN CAST(numSalesOrPurType AS TEXT) ELSE NULL END AS numSalesOrPurType,
							CASE WHEN (sys.fn_cdc_is_bit_set(sys.fn_cdc_get_column_ordinal('dbo_OpportunityMaster','lngPConclAnalysis'),__$update_mask) = 1) THEN CAST(lngPConclAnalysis AS TEXT) ELSE NULL END AS lngPConclAnalysis,
							CASE WHEN (sys.fn_cdc_is_bit_set(sys.fn_cdc_get_column_ordinal('dbo_OpportunityMaster','tintActive'),__$update_mask) = 1) THEN CAST(tintActive AS TEXT) ELSE NULL END AS tintActive,
							CASE WHEN (sys.fn_cdc_is_bit_set(sys.fn_cdc_get_column_ordinal('dbo_OpportunityMaster','vcOppRefOrderNo'),__$update_mask) = 1) THEN CAST(vcOppRefOrderNo AS TEXT) ELSE NULL END AS vcOppRefOrderNo,
							CASE WHEN (sys.fn_cdc_is_bit_set(sys.fn_cdc_get_column_ordinal('dbo_OpportunityMaster','numBusinessProcessID'),__$update_mask) = 1) THEN CAST(numBusinessProcessID AS TEXT) ELSE NULL END AS numBusinessProcessID,
							CASE WHEN (sys.fn_cdc_is_bit_set(sys.fn_cdc_get_column_ordinal('dbo_OpportunityMaster','numRecOwner'),__$update_mask) = 1) THEN CAST(numRecOwner AS TEXT) ELSE NULL END AS numRecOwner,
							CASE WHEN (sys.fn_cdc_is_bit_set(sys.fn_cdc_get_column_ordinal('dbo_OpportunityMaster','tintOppStatus'),__$update_mask) = 1) THEN CAST(tintOppStatus AS TEXT) ELSE NULL END AS tintOppStatus,
							CASE WHEN (sys.fn_cdc_is_bit_set(sys.fn_cdc_get_column_ordinal('dbo_OpportunityMaster','bintClosedDate'),__$update_mask) = 1) THEN CAST(bintClosedDate AS TEXT) ELSE NULL END AS bintClosedDate,
							CASE WHEN (sys.fn_cdc_is_bit_set(sys.fn_cdc_get_column_ordinal('dbo_OpportunityMaster','tintshipped'),__$update_mask) = 1) THEN CAST(tintshipped AS TEXT) ELSE NULL END AS tintshipped,
							CASE WHEN (sys.fn_cdc_is_bit_set(sys.fn_cdc_get_column_ordinal('dbo_OpportunityMaster','numPercentageComplete'),__$update_mask) = 1) THEN CAST(numPercentageComplete AS TEXT) ELSE NULL END AS numPercentageComplete
               FROM
               cdc.fn_cdc_get_all_changes_dbo_OpportunityMaster(v_vbStartlsn,v_vbStartlsn,N'all update old')
               WHERE
               __$operation = 3) as t1
            UNPIVOT(old_value FOR column_name IN(numAssignedTo,bintModifiedDate,numModifiedBy,numCampainID,numStatus,txtComments,
            vcPOppName,intPEstimatedCloseDate,tintSource,numSalesOrPurType,
            lngPConclAnalysis,tintActive,vcOppRefOrderNo,numBusinessProcessID,numRecOwner,
            tintOppStatus,bintClosedDate,tintshipped,numPercentageComplete)) as unp) as TempUpdateBefore
         INNER JOIN(SELECT
            __$start_lsn,
						column_name,
						new_value
            FROM(SELECT
               __$start_lsn,
							CASE WHEN (sys.fn_cdc_is_bit_set(sys.fn_cdc_get_column_ordinal('dbo_OpportunityMaster','numAssignedTo'),__$update_mask) = 1) THEN CAST(numAssignedTo AS TEXT) ELSE NULL END AS numAssignedTo,
							CASE WHEN (sys.fn_cdc_is_bit_set(sys.fn_cdc_get_column_ordinal('dbo_OpportunityMaster','bintModifiedDate'),__$update_mask) = 1) THEN CAST(bintModifiedDate AS TEXT) ELSE NULL END AS bintModifiedDate,
							CASE WHEN (sys.fn_cdc_is_bit_set(sys.fn_cdc_get_column_ordinal('dbo_OpportunityMaster','numModifiedBy'),__$update_mask) = 1) THEN CAST(numModifiedBy AS TEXT) ELSE NULL END AS numModifiedBy,
							CASE WHEN (sys.fn_cdc_is_bit_set(sys.fn_cdc_get_column_ordinal('dbo_OpportunityMaster','numCampainID'),__$update_mask) = 1) THEN CAST(numCampainID as TEXT) ELSE NULL END AS numCampainID,
							CASE WHEN (sys.fn_cdc_is_bit_set(sys.fn_cdc_get_column_ordinal('dbo_OpportunityMaster','numStatus'),__$update_mask) = 1) THEN CAST(numStatus as TEXT) ELSE NULL END AS numStatus,
							CASE WHEN (sys.fn_cdc_is_bit_set(sys.fn_cdc_get_column_ordinal('dbo_OpportunityMaster','txtComments'),__$update_mask) = 1) THEN CAST(txtComments as TEXT) ELSE NULL END AS txtComments,
							CASE WHEN (sys.fn_cdc_is_bit_set(sys.fn_cdc_get_column_ordinal('dbo_OpportunityMaster','vcPOppName'),__$update_mask) = 1) THEN CAST(vcPOppName AS TEXT) ELSE NULL END AS vcPOppName,
							CASE WHEN (sys.fn_cdc_is_bit_set(sys.fn_cdc_get_column_ordinal('dbo_OpportunityMaster','intPEstimatedCloseDate'),__$update_mask) = 1) THEN CAST(intPEstimatedCloseDate AS TEXT) ELSE NULL END AS intPEstimatedCloseDate,
							CASE WHEN (sys.fn_cdc_is_bit_set(sys.fn_cdc_get_column_ordinal('dbo_OpportunityMaster','tintSource'),__$update_mask) = 1) THEN CAST(tintSource AS TEXT) ELSE NULL END AS tintSource,
							CASE WHEN (sys.fn_cdc_is_bit_set(sys.fn_cdc_get_column_ordinal('dbo_OpportunityMaster','numSalesOrPurType'),__$update_mask) = 1) THEN CAST(numSalesOrPurType AS TEXT) ELSE NULL END AS numSalesOrPurType,
							CASE WHEN (sys.fn_cdc_is_bit_set(sys.fn_cdc_get_column_ordinal('dbo_OpportunityMaster','lngPConclAnalysis'),__$update_mask) = 1) THEN CAST(lngPConclAnalysis AS TEXT) ELSE NULL END AS lngPConclAnalysis,
							CASE WHEN (sys.fn_cdc_is_bit_set(sys.fn_cdc_get_column_ordinal('dbo_OpportunityMaster','tintActive'),__$update_mask) = 1) THEN CAST(tintActive AS TEXT) ELSE NULL END AS tintActive,
							CASE WHEN (sys.fn_cdc_is_bit_set(sys.fn_cdc_get_column_ordinal('dbo_OpportunityMaster','vcOppRefOrderNo'),__$update_mask) = 1) THEN CAST(vcOppRefOrderNo AS TEXT) ELSE NULL END AS vcOppRefOrderNo,
							CASE WHEN (sys.fn_cdc_is_bit_set(sys.fn_cdc_get_column_ordinal('dbo_OpportunityMaster','numBusinessProcessID'),__$update_mask) = 1) THEN CAST(numBusinessProcessID AS TEXT) ELSE NULL END AS numBusinessProcessID,
							CASE WHEN (sys.fn_cdc_is_bit_set(sys.fn_cdc_get_column_ordinal('dbo_OpportunityMaster','numRecOwner'),__$update_mask) = 1) THEN CAST(numRecOwner AS TEXT) ELSE NULL END AS numRecOwner,
							CASE WHEN (sys.fn_cdc_is_bit_set(sys.fn_cdc_get_column_ordinal('dbo_OpportunityMaster','tintOppStatus'),__$update_mask) = 1) THEN CAST(tintOppStatus AS TEXT) ELSE NULL END AS tintOppStatus,
							CASE WHEN (sys.fn_cdc_is_bit_set(sys.fn_cdc_get_column_ordinal('dbo_OpportunityMaster','bintClosedDate'),__$update_mask) = 1) THEN CAST(bintClosedDate AS TEXT) ELSE NULL END AS bintClosedDate,
							CASE WHEN (sys.fn_cdc_is_bit_set(sys.fn_cdc_get_column_ordinal('dbo_OpportunityMaster','tintshipped'),__$update_mask) = 1) THEN CAST(tintshipped AS TEXT) ELSE NULL END AS tintshipped,
							CASE WHEN (sys.fn_cdc_is_bit_set(sys.fn_cdc_get_column_ordinal('dbo_OpportunityMaster','numPercentageComplete'),__$update_mask) = 1) THEN CAST(numPercentageComplete AS TEXT) ELSE NULL END AS numPercentageComplete
               FROM
               cdc.fn_cdc_get_all_changes_dbo_OpportunityMaster(v_vbStartlsn,v_vbStartlsn,N'all') -- 'all update old' is not necessary here
               WHERE
               __$operation = 4) as t2
            UNPIVOT(new_value FOR column_name IN(numAssignedTo,bintModifiedDate,numModifiedBy,numCampainID,numStatus,txtComments,
            vcPOppName,intPEstimatedCloseDate,tintSource,numSalesOrPurType,
            lngPConclAnalysis,tintActive,vcOppRefOrderNo,numBusinessProcessID,numRecOwner,
            tintOppStatus,bintClosedDate,tintshipped,numPercentageComplete)) as unp) as TempUpdateAfter -- after update
         ON
         TempUpdateBefore.__$start_lsn = TempUpdateAfter.__$start_lsn
         AND TempUpdateBefore.column_name = TempUpdateAfter.column_name;
         select   COUNT(*) INTO v_jCOUNT FROM tt_TMEPCHANGEDCOLUMNS;
         WHILE v_j <= v_jCOUNT LOOP
            select   dtDate, vcColumnName, vcOldValue, vcNewValue INTO v_dtDate,v_vcColumnName,v_vcOldValue,v_vcNewValue FROM
            tt_TMEPCHANGEDCOLUMNS WHERE
            ID = v_j;
            IF v_vcColumnName = 'numModifiedBy' then
				
					-- KEEP VALUES IN TEMPORARY VARIABLE AND VALUES FOR ALL FIELDS UPDATE AFTER LOOP COMPLETES.
               v_numNewModifiedBy := CAST(v_vcNewValue AS NUMERIC(18,0));
            ELSEIF v_vcColumnName = 'bintModifiedDate'
            then
				
               BEGIN
						/* Opportunity Updated Items */
                  INSERT INTO tt_TEMPOPPORTUNITYHISTORY(vbStartlsn,
							numRecordID,
							numDomainID,
							dtDate,
							numUserCntID,
							vcEvent,
							vcFieldName,
							vcOldValue,
							vcNewValue,
							vcDescription,
							vcHiddenDescription,
							numFieldID,
							bitCustomField)
                  SELECT
                  CAST(v_vbStartlsn AS BYTEA),
							v_numOppID,
							v_numDomainID,
							v_bintModifiedDate,
							v_numModifiedBy,
							CAST('Changed Item' AS VARCHAR(200)),
							CAST((CASE TempUpdateBefore.column_name
                  WHEN 'numUnitHour' THEN 'Unit Hour'
                  WHEN 'monPrice' THEN 'Price'
                  WHEN 'vcItemName' THEN 'Item Name'
                  WHEN 'vcItemDesc' THEN 'Item Description'
                  WHEN 'numWarehouseItmsID' THEN 'Warehouse'
                  WHEN 'bitDropship' THEN 'Drop Ship'
                  WHEN 'numUnitHourReceived' THEN 'Received Qty'
                  WHEN 'bitDiscountType' THEN 'Discount Type'
                  WHEN 'fltDiscount' THEN 'Discount'
                  WHEN 'numQtyShipped' THEN 'Shipped Qty'
                  WHEN 'numUOMId' THEN 'UOM'
                  WHEN 'numToWarehouseItemID' THEN 'Stock Transfer To Warehouse'
                  ELSE TempUpdateBefore.column_name
                  END) AS VARCHAR(200)),
							(CASE
                  WHEN TempUpdateBefore.column_name = 'numWarehouseItmsID' OR TempUpdateBefore.column_name = 'numToWarehouseItemID' THEN(SELECT
                        CONCAT(W.vcWareHouse,CASE WHEN WL.vcLocation IS NOT NULL THEN CONCAT(' (',WL.vcLocation,')') ELSE '' END)
                        FROM
                        WareHouseItems WI
                        INNER JOIN
                        Warehouses W
                        ON
                        WI.numWareHouseID = W.numWareHouseID
                        LEFT JOIN
                        WarehouseLocation WL
                        ON
                        WI.numWLocationID = WL.numWLocationID
                        WHERE
                        numWarehouseItemID = TempUpdateBefore.old_value)
                  WHEN TempUpdateBefore.column_name = 'bitDiscountType' THEN(CASE WHEN TempUpdateBefore.old_value = 1 THEN 'Flat Amount' ELSE 'Percentage' END)
                  WHEN TempUpdateBefore.column_name = 'numUOMId' THEN(SELECT vcUnitName FROM UOM WHERE numDomainId = v_numDomainID AND numUOMId = TempUpdateBefore.old_value)
                  ELSE
                     TempUpdateBefore.old_value
                  END),
							(CASE
                  WHEN TempUpdateBefore.column_name = 'numWarehouseItmsID' OR TempUpdateBefore.column_name = 'numToWarehouseItemID' THEN(SELECT
                        CONCAT(W.vcWareHouse,CASE WHEN WL.vcLocation IS NOT NULL THEN CONCAT(' (',WL.vcLocation,')') ELSE '' END)
                        FROM
                        WareHouseItems WI
                        INNER JOIN
                        Warehouses W
                        ON
                        WI.numWareHouseID = W.numWareHouseID
                        LEFT JOIN
                        WarehouseLocation WL
                        ON
                        WI.numWLocationID = WL.numWLocationID
                        WHERE
                        numWarehouseItemID = TempUpdateAfter.new_value)
                  WHEN TempUpdateBefore.column_name = 'bitDiscountType' THEN(CASE WHEN TempUpdateAfter.new_value = 1 THEN 'Flat Amount' ELSE 'Percentage' END)
                  WHEN TempUpdateBefore.column_name = 'numUOMId' THEN(SELECT vcUnitName FROM UOM WHERE numDomainId = v_numDomainID AND numUOMId = TempUpdateAfter.new_value)
                  ELSE
                     TempUpdateAfter.new_value
                  END),
							CONCAT('Item Code:',TempUpdateAfter.ItemCode,', Item Name:',TempUpdateAfter.ItemName),
							CONCAT('OppItemID:',TempUpdateAfter.OppItemID,', numWarehouseItemID:',TempUpdateAfter.WarehouseItemID),
							NULL,
							CAST(0 AS BOOLEAN)
                  FROM(SELECT
                     __$start_lsn,
									OppItemID,
									ItemCode,
									ItemName,
									WarehouseItemID,
									column_name,
									old_value
                     FROM(SELECT
                        __$start_lsn,
										numOppItemtCode As OppItemID,
										numItemCode As ItemCode,
										vcItemName As ItemName,
										numWarehouseItmsID As WarehouseItemID,
										CASE WHEN (sys.fn_cdc_is_bit_set(sys.fn_cdc_get_column_ordinal('dbo_OpportunityItems','numItemCode'),__$update_mask) = 1) THEN CAST(numItemCode AS TEXT) ELSE NULL END AS numItemCode,
										CASE WHEN (sys.fn_cdc_is_bit_set(sys.fn_cdc_get_column_ordinal('dbo_OpportunityItems','numUnitHour'),__$update_mask) = 1) THEN CAST(numUnitHour AS TEXT) ELSE NULL END AS numUnitHour,
										CASE WHEN (sys.fn_cdc_is_bit_set(sys.fn_cdc_get_column_ordinal('dbo_OpportunityItems','monPrice'),__$update_mask) = 1) THEN CAST(monPrice AS TEXT) ELSE NULL END AS monPrice,
										CASE WHEN (sys.fn_cdc_is_bit_set(sys.fn_cdc_get_column_ordinal('dbo_OpportunityItems','vcItemName'),__$update_mask) = 1) THEN CAST(vcItemName as TEXT) ELSE NULL END AS vcItemName,
										CASE WHEN (sys.fn_cdc_is_bit_set(sys.fn_cdc_get_column_ordinal('dbo_OpportunityItems','vcItemDesc'),__$update_mask) = 1) THEN CAST(vcItemDesc as TEXT) ELSE NULL END AS vcItemDesc,
										CASE WHEN (sys.fn_cdc_is_bit_set(sys.fn_cdc_get_column_ordinal('dbo_OpportunityItems','numWarehouseItmsID'),__$update_mask) = 1) THEN CAST(numWarehouseItmsID as TEXT) ELSE NULL END AS numWarehouseItmsID,
										CASE WHEN (sys.fn_cdc_is_bit_set(sys.fn_cdc_get_column_ordinal('dbo_OpportunityItems','bitDropship'),__$update_mask) = 1) THEN CAST(bitDropship AS TEXT) ELSE NULL END AS bitDropship,
										CASE WHEN (sys.fn_cdc_is_bit_set(sys.fn_cdc_get_column_ordinal('dbo_OpportunityItems','numUnitHourReceived'),__$update_mask) = 1) THEN CAST(numUnitHourReceived AS TEXT) ELSE NULL END AS numUnitHourReceived,
										CASE WHEN (sys.fn_cdc_is_bit_set(sys.fn_cdc_get_column_ordinal('dbo_OpportunityItems','bitDiscountType'),__$update_mask) = 1) THEN CAST(bitDiscountType AS TEXT) ELSE NULL END AS bitDiscountType,
										CASE WHEN (sys.fn_cdc_is_bit_set(sys.fn_cdc_get_column_ordinal('dbo_OpportunityItems','fltDiscount'),__$update_mask) = 1) THEN CAST(fltDiscount AS TEXT) ELSE NULL END AS fltDiscount,
										CASE WHEN (sys.fn_cdc_is_bit_set(sys.fn_cdc_get_column_ordinal('dbo_OpportunityItems','numQtyShipped'),__$update_mask) = 1) THEN CAST(numQtyShipped AS TEXT) ELSE NULL END AS numQtyShipped,
										CASE WHEN (sys.fn_cdc_is_bit_set(sys.fn_cdc_get_column_ordinal('dbo_OpportunityItems','numUOMId'),__$update_mask) = 1) THEN CAST(numUOMId AS TEXT) ELSE NULL END AS numUOMId,
										CASE WHEN (sys.fn_cdc_is_bit_set(sys.fn_cdc_get_column_ordinal('dbo_OpportunityItems','numToWarehouseItemID'),__$update_mask) = 1) THEN CAST(numToWarehouseItemID AS TEXT) ELSE NULL END AS numToWarehouseItemID
                        FROM
                        cdc.fn_cdc_get_all_changes_dbo_OpportunityItems(v_vbStartlsn,v_vbStartlsn,N'all update old')
                        WHERE
                        __$operation = 3) as t1
                     UNPIVOT(old_value FOR column_name IN(numItemCode,numUnitHour,monPrice,vcItemName,vcItemDesc,numWarehouseItmsID,
                     bitDropship,numUnitHourReceived,bitDiscountType,fltDiscount,numToWarehouseItemID)) as unp) as TempUpdateBefore
                  INNER JOIN(SELECT
                     __$start_lsn,
									OppItemID,
									ItemCode,
									ItemName,
									WarehouseItemID,
									column_name,
									new_value
                     FROM(SELECT
                        __$start_lsn,
										numOppItemtCode As OppItemID,
										numItemCode As ItemCode,
										vcItemName As ItemName,
										numWarehouseItmsID As WarehouseItemID,
										CASE WHEN (sys.fn_cdc_is_bit_set(sys.fn_cdc_get_column_ordinal('dbo_OpportunityItems','numItemCode'),__$update_mask) = 1) THEN CAST(numItemCode AS TEXT) ELSE NULL END AS numItemCode,
										CASE WHEN (sys.fn_cdc_is_bit_set(sys.fn_cdc_get_column_ordinal('dbo_OpportunityItems','numUnitHour'),__$update_mask) = 1) THEN CAST(numUnitHour AS TEXT) ELSE NULL END AS numUnitHour,
										CASE WHEN (sys.fn_cdc_is_bit_set(sys.fn_cdc_get_column_ordinal('dbo_OpportunityItems','monPrice'),__$update_mask) = 1) THEN CAST(monPrice AS TEXT) ELSE NULL END AS monPrice,
										CASE WHEN (sys.fn_cdc_is_bit_set(sys.fn_cdc_get_column_ordinal('dbo_OpportunityItems','vcItemName'),__$update_mask) = 1) THEN CAST(vcItemName as TEXT) ELSE NULL END AS vcItemName,
										CASE WHEN (sys.fn_cdc_is_bit_set(sys.fn_cdc_get_column_ordinal('dbo_OpportunityItems','vcItemDesc'),__$update_mask) = 1) THEN CAST(vcItemDesc as TEXT) ELSE NULL END AS vcItemDesc,
										CASE WHEN (sys.fn_cdc_is_bit_set(sys.fn_cdc_get_column_ordinal('dbo_OpportunityItems','numWarehouseItmsID'),__$update_mask) = 1) THEN CAST(numWarehouseItmsID as TEXT) ELSE NULL END AS numWarehouseItmsID,
										CASE WHEN (sys.fn_cdc_is_bit_set(sys.fn_cdc_get_column_ordinal('dbo_OpportunityItems','bitDropship'),__$update_mask) = 1) THEN CAST(bitDropship AS TEXT) ELSE NULL END AS bitDropship,
										CASE WHEN (sys.fn_cdc_is_bit_set(sys.fn_cdc_get_column_ordinal('dbo_OpportunityItems','numUnitHourReceived'),__$update_mask) = 1) THEN CAST(numUnitHourReceived AS TEXT) ELSE NULL END AS numUnitHourReceived,
										CASE WHEN (sys.fn_cdc_is_bit_set(sys.fn_cdc_get_column_ordinal('dbo_OpportunityItems','bitDiscountType'),__$update_mask) = 1) THEN CAST(bitDiscountType AS TEXT) ELSE NULL END AS bitDiscountType,
										CASE WHEN (sys.fn_cdc_is_bit_set(sys.fn_cdc_get_column_ordinal('dbo_OpportunityItems','fltDiscount'),__$update_mask) = 1) THEN CAST(fltDiscount AS TEXT) ELSE NULL END AS fltDiscount,
										CASE WHEN (sys.fn_cdc_is_bit_set(sys.fn_cdc_get_column_ordinal('dbo_OpportunityItems','numQtyShipped'),__$update_mask) = 1) THEN CAST(numQtyShipped AS TEXT) ELSE NULL END AS numQtyShipped,
										CASE WHEN (sys.fn_cdc_is_bit_set(sys.fn_cdc_get_column_ordinal('dbo_OpportunityItems','numUOMId'),__$update_mask) = 1) THEN CAST(numUOMId AS TEXT) ELSE NULL END AS numUOMId,
										CASE WHEN (sys.fn_cdc_is_bit_set(sys.fn_cdc_get_column_ordinal('dbo_OpportunityItems','numToWarehouseItemID'),__$update_mask) = 1) THEN CAST(numToWarehouseItemID AS TEXT) ELSE NULL END AS numToWarehouseItemID
                        FROM
                        cdc.fn_cdc_get_all_changes_dbo_OpportunityItems(v_vbStartlsn,v_vbStartlsn,N'all') -- 'all update old' is not necessary here
                        WHERE
                        __$operation = 4) as t2
                     UNPIVOT(new_value FOR column_name IN(numItemCode,numUnitHour,monPrice,vcItemName,vcItemDesc,numWarehouseItmsID,
                     bitDropship,numUnitHourReceived,bitDiscountType,fltDiscount,numToWarehouseItemID)) as unp) as TempUpdateAfter -- after update
                  ON
                  TempUpdateBefore.__$start_lsn = TempUpdateAfter.__$start_lsn
                  AND TempUpdateBefore.column_name = TempUpdateAfter.column_name
                  AND TempUpdateBefore.OppItemID = TempUpdateAfter.OppItemID
                  AND coalesce(TempUpdateBefore.WarehouseItemID,0) = coalesce(TempUpdateAfter.WarehouseItemID,0);
                  EXCEPTION WHEN OTHERS THEN
						/* If the specified LSN range does not fall within the change tracking timeline for the capture instance, the function returns error 208 
						("An insufficient number of arguments were supplied for the procedure or function cdc.fn_cdc_get_all_changes."). */
                     NULL;
               END;
               BEGIN
						/* Opportunity Deleted Items */
                  INSERT INTO tt_TEMPOPPORTUNITYHISTORY(vbStartlsn,
							numRecordID,
							numDomainID,
							dtDate,
							numUserCntID,
							vcEvent,
							vcDescription,
							vcHiddenDescription,
							numFieldID,
							bitCustomField)
                  SELECT
                  CAST(v_vbStartlsn AS BYTEA),
							v_numOppID,
							v_numDomainID,
							v_bintModifiedDate,
							v_numModifiedBy,
							CAST('Removed Item' AS VARCHAR(200)),
							CONCAT('Item Code:',TempDelete.ItemCode,', Item Name:',TempDelete.ItemName),
							CONCAT('OppItemID:',TempDelete.OppItemID,', numWarehouseItemID:',TempDelete.WarehouseItemID),
							NULL,
							CAST(0 AS BOOLEAN)
                  FROM(SELECT
                     __$start_lsn,
									numOppItemtCode As OppItemID,
									numItemCode As ItemCode,
									vcItemName As ItemName,
									numWarehouseItmsID As WarehouseItemID
                     FROM
                     cdc.dbo_OpportunityItems_CT
                     WHERE
                     __$operation = 1
                     AND __$start_lsn = v_vbStartlsn) AS TempDelete;
                  EXCEPTION WHEN OTHERS THEN
						/* If the specified LSN range does not fall within the change tracking timeline for the capture instance, the function returns error 208 
						("An insufficient number of arguments were supplied for the procedure or function cdc.fn_cdc_get_all_changes."). */
                     NULL;
               END;
            ELSEIF v_vcColumnName = 'vcPOppName'
            then
				
					-- IF UPDATE FIELD ID vcPOppName
					-- IF OPPID IS EXISTS IN TEMP TABLE AND COMMIT DATE TIME IS SAME AS FOUND IN TEMP TABLE
					-- THEN IGNORE CHANGE BECUASE IT IS MODIFIED AFTER OPP/ORDER INSERT AND NOT SEPERATE TRANSACTION
               IF NOT EXISTS(SELECT * FROM tt_TEMPOPPORTUNITYHISTORY WHERE vbStartlsn = v_vbStartlsn AND dtDate = v_dtDate) then
					
                  INSERT INTO tt_TEMPOPPORTUNITYHISTORY(vbStartlsn,
							numRecordID,
							numDomainID,
							dtDate,
							numUserCntID,
							vcEvent,
							vcFieldName,
							vcOldValue,
							vcNewValue,
							numFieldID,
							bitCustomField)
						VALUES(CAST(v_vbStartlsn AS BYTEA),
							v_numOppID,
							v_numDomainID,
							v_dtDate,
							v_numModifiedBy,
							CAST('Change' AS VARCHAR(200)),
							CAST('Opp/Order Name' AS VARCHAR(200)),
							v_vcOldValue,
							v_vcNewValue,
							NULL,
							CAST(0 AS BOOLEAN));
               end if;
            ELSE
               INSERT INTO tt_TEMPOPPORTUNITYHISTORY(vbStartlsn,
						numRecordID,
						numDomainID,
						dtDate,
						numUserCntID,
						vcEvent,
						vcFieldName,
						vcOldValue,
						vcNewValue,
						numFieldID,
						bitCustomField)
					VALUES(CAST(v_vbStartlsn AS BYTEA),
						v_numOppID,
						v_numDomainID,
						v_dtDate,
						v_numModifiedBy,
						CAST('Change' AS VARCHAR(200)),
						CAST((CASE
               WHEN v_vcColumnName = 'numAssignedTo' THEN 'Assigned To'
               WHEN v_vcColumnName = 'numCampainID' THEN 'Campaign'
               WHEN v_vcColumnName = 'numStatus' THEN 'Status'
               WHEN v_vcColumnName = 'txtComments' THEN 'Comments'
               WHEN v_vcColumnName = 'intPEstimatedCloseDate' THEN 'Estimated Close Date'
               WHEN v_vcColumnName = 'tintSource' THEN 'Source'
               WHEN v_vcColumnName = 'numSalesOrPurType' THEN 'Sales/Purchase Type'
               WHEN v_vcColumnName = 'lngPConclAnalysis' THEN 'Conclusion Reason'
               WHEN v_vcColumnName = 'tintActive' THEN 'Active'
               WHEN v_vcColumnName = 'vcOppRefOrderNo' THEN 'Customer PO#'
               WHEN v_vcColumnName = 'numBusinessProcessID' THEN 'Business Process'
               WHEN v_vcColumnName = 'numRecOwner' THEN 'Record Owner'
               WHEN v_vcColumnName = 'tintOppStatus' THEN 'Deal Status'
               WHEN v_vcColumnName = 'bintClosedDate' THEN 'Closed Date'
               WHEN v_vcColumnName = 'tintshipped' THEN 'Order Closed'
               WHEN v_vcColumnName = 'numPercentageComplete' THEN 'Percentage Complete'
               END) AS VARCHAR(200)),
						(CASE v_vcColumnName
               WHEN 'numAssignedTo' THEN(SELECT vcUserName FROM UserMaster WHERE numDomainID = v_numDomainID AND numUserDetailId = v_vcOldValue)
               WHEN 'numCampainID' THEN
                  coalesce((SELECT vcCampaignName FROM CampaignMaster WHERE numDomainID = v_numDomainID AND numCampaignID = v_vcOldValue), '')
               WHEN 'numStatus' THEN
                  coalesce((SELECT vcData FROM Listdetails WHERE numDomainID = v_numDomainID AND numListID = 176 AND numListItemID = v_vcOldValue), '')
               WHEN 'tintSource' THEN
                  coalesce((SELECT vcData FROM Listdetails WHERE numDomainID = v_numDomainID AND numListID = 9 AND numListItemID = v_vcOldValue), '')
               WHEN 'numSalesOrPurType' THEN
                  coalesce((SELECT vcData FROM Listdetails WHERE numDomainID = v_numDomainID AND numListID = 45 AND numListItemID = v_vcOldValue), '')
               WHEN 'tintActive' THEN(CASE WHEN cast(NULLIF(v_vcOldValue,'') as INTEGER) = 1 THEN 'Yes' ELSE 'No' END)
               WHEN 'numBusinessProcessID' THEN
                  coalesce((SELECT Slp_Name FROM Sales_process_List_Master WHERE numDomainID = v_numDomainID AND Slp_Id = v_vcOldValue), '')
               WHEN 'numRecOwner' THEN(SELECT vcUserName FROM UserMaster WHERE numDomainID = v_numDomainID AND numUserDetailId = v_vcOldValue)
               WHEN 'tintOppStatus' THEN(CASE v_vcOldValue WHEN 1 THEN 'Deal Won' WHEN 2 THEN 'Deal Lost' ELSE 'Open' END)
               WHEN 'tintshipped' THEN(CASE v_vcOldValue WHEN 1 THEN 'Closed' ELSE 'Open' END)
               WHEN 'numPercentageComplete' THEN
                  coalesce((SELECT vcData FROM Listdetails WHERE numDomainID = v_numDomainID AND numListID = 50 AND numListItemID = v_vcOldValue), '')
               ELSE
                  v_vcOldValue
               END),
						(CASE v_vcColumnName
               WHEN 'numAssignedTo' THEN(SELECT vcUserName FROM UserMaster WHERE numDomainID = v_numDomainID AND numUserDetailId = v_vcNewValue)
               WHEN 'numCampainID' THEN
                  coalesce((SELECT vcCampaignName FROM CampaignMaster WHERE numDomainID = v_numDomainID AND numCampaignID = v_vcNewValue), '')
               WHEN 'numStatus' THEN
                  coalesce((SELECT vcData FROM Listdetails WHERE numDomainID = v_numDomainID AND numListID = 176 AND numListItemID = v_vcNewValue), '')
               WHEN 'tintSource' THEN
                  coalesce((SELECT vcData FROM Listdetails WHERE numDomainID = v_numDomainID AND numListID = 9 AND numListItemID = v_vcNewValue), '')
               WHEN 'numSalesOrPurType' THEN
                  coalesce((SELECT vcData FROM Listdetails WHERE numDomainID = v_numDomainID AND numListID = 45 AND numListItemID = v_vcNewValue), '')
               WHEN 'tintActive' THEN(CASE WHEN cast(NULLIF(v_vcNewValue,'') as INTEGER) = 1 THEN 'Yes' ELSE 'No' END)
               WHEN 'numBusinessProcessID' THEN
                  coalesce((SELECT Slp_Name FROM Sales_process_List_Master WHERE numDomainID = v_numDomainID AND Slp_Id = v_vcNewValue), '')
               WHEN 'numRecOwner' THEN(SELECT vcUserName FROM UserMaster WHERE numDomainID = v_numDomainID AND numUserDetailId = v_vcNewValue)
               WHEN 'tintOppStatus' THEN(CASE v_vcNewValue WHEN 1 THEN 'Deal Won' WHEN 2 THEN 'Deal Lost' ELSE 'Open' END)
               WHEN 'tintshipped' THEN(CASE v_vcNewValue WHEN 1 THEN 'Closed' ELSE 'Open' END)
               WHEN 'numPercentageComplete' THEN
                  coalesce((SELECT vcData FROM Listdetails WHERE numDomainID = v_numDomainID AND numListID = 50 AND numListItemID = v_vcNewValue), '')
               ELSE
                  v_vcNewValue
               END),
						NULL,
						CAST(0 AS BOOLEAN));
            end if;
            v_j := v_j::bigint+1;
         END LOOP;
         IF v_numNewModifiedBy <> -1 then
			
            UPDATE tt_TEMPOPPORTUNITYHISTORY SET numUserCntID = v_numNewModifiedBy WHERE vbStartlsn = v_vbStartlsn;
         end if;
      end if;
      v_i := v_i+1;
   END LOOP;

	--CLEAR RETRIVED DATA FROM CDC TABLE
   DELETE FROM cdc.dbo_OpportunityItems_CT WHERE __$start_lsn IN(SELECT vbStartlsn FROM tt_TEMPTABLE);
   DELETE FROM cdc.dbo_OpportunityMaster_CT WHERE __$start_lsn IN(SELECT vbStartlsn FROM tt_TEMPTABLE);

	/************************************ OPP/ORDER BIZDOCS ************************************/
   DELETE FROM tt_TEMPTABLE;

   INSERT INTO
   tt_TEMPTABLE
   SELECT
   ROW_NUMBER() OVER(ORDER BY __$start_lsn),
		__$start_lsn
   FROM
   cdc.dbo_OpportunityBizDocs_CT
   WHERE
   __$operation IN(1,2,3)
   GROUP BY
   __$start_lsn;

   v_i := 1;
   select   COUNT(*) INTO v_iCOUNT FROM tt_TEMPTABLE;


   WHILE v_i <= v_iCOUNT LOOP
      select   vbStartlsn INTO v_vbStartlsn FROM tt_TEMPTABLE WHERE ID = v_i;
      select   __$operation, cdc.dbo_OpportunityBizDocs_CT.numOppID, numOppBizDocsId, vcBizDocID, numDomainID, cdc.dbo_OpportunityBizDocs_CT.dtModifiedDate, cdc.dbo_OpportunityBizDocs_CT.numModifiedBy INTO v_tintOperation,v_numOppID,v_numOppBizDocsID,v_vcBizDocID,v_numDomainID,
      v_bintModifiedDate,v_numModifiedBy FROM
      cdc.dbo_OpportunityBizDocs_CT
      JOIN
      OpportunityMaster
      ON
      cdc.dbo_OpportunityBizDocs_CT.numOppID = OpportunityMaster.numOppId WHERE
      __$start_lsn = v_vbStartlsn    LIMIT 1;
      IF v_tintOperation = 2 then --INSERT
		
         INSERT INTO tt_TEMPOPPORTUNITYHISTORY(vbStartlsn,
				numRecordID,
				numDomainID,
				dtDate,
				numUserCntID,
				vcEvent,
				vcDescription,
				vcHiddenDescription,
				numFieldID,
				bitCustomField)
			VALUES(CAST(v_vbStartlsn AS BYTEA),
				v_numOppID,
				v_numDomainID,
				v_bintModifiedDate,
				v_numModifiedBy,
				CAST('Added BizDoc' AS VARCHAR(200)),
				coalesce((SELECT vcBizDocID FROM OpportunityBizDocs WHERE numoppid = v_numOppID AND numOppBizDocsId = v_numOppBizDocsID), ''),
				CONCAT('OppID:',v_numOppID,', OppBizDocsID:',v_numOppBizDocsID),
				NULL,
				CAST(0 AS BOOLEAN));
      ELSEIF v_tintOperation = 1
      then --DELETE
		
         INSERT INTO tt_TEMPOPPORTUNITYHISTORY(vbStartlsn,
				numRecordID,
				numDomainID,
				dtDate,
				numUserCntID,
				vcEvent,
				vcDescription,
				vcHiddenDescription,
				numFieldID,
				bitCustomField)
			VALUES(CAST(v_vbStartlsn AS BYTEA),
				v_numOppID,
				v_numDomainID,
				v_bintModifiedDate,
				v_numModifiedBy,
				CAST('Removed BizDoc' AS VARCHAR(200)),
				v_vcBizDocID,
				CONCAT('OppID:',v_numOppID,', OppBizDocsID:',v_numOppBizDocsID),
				NULL,
				CAST(0 AS BOOLEAN));
      ELSEIF v_tintOperation = 3
      then --UPDATE
		
         DELETE FROM tt_TMEPCHANGEDCOLUMNS;
         INSERT INTO tt_TMEPCHANGEDCOLUMNS(ID,
				dtDate,
				vcColumnName,
				vcOldValue,
				vcNewValue)
         SELECT
         ROW_NUMBER() OVER(ORDER BY TempUpdateBefore.__$start_lsn),
				sys.fn_cdc_map_lsn_to_time(TempUpdateBefore.__$start_lsn)+CAST((EXTRACT(DAY FROM TIMEZONE('UTC',now()) -LOCALTIMESTAMP)*60*60*24+EXTRACT(HOUR FROM TIMEZONE('UTC',now()) -LOCALTIMESTAMP)*60*60+EXTRACT(MINUTE FROM TIMEZONE('UTC',now()) -LOCALTIMESTAMP)*60+EXTRACT(SECOND FROM TIMEZONE('UTC',now()) -LOCALTIMESTAMP)) || 'second' as interval),
				TempUpdateBefore.column_name,
				TempUpdateBefore.old_value,
				TempUpdateAfter.new_value
         FROM(SELECT
            __$start_lsn,
						column_name,
						old_value
            FROM(SELECT
               __$start_lsn,
							CASE WHEN (sys.fn_cdc_is_bit_set(sys.fn_cdc_get_column_ordinal('dbo_OpportunityBizDocs','dtModifiedDate'),__$update_mask) = 1) THEN CAST(dtModifiedDate AS TEXT) ELSE NULL END AS dtModifiedDate,
							CASE WHEN (sys.fn_cdc_is_bit_set(sys.fn_cdc_get_column_ordinal('dbo_OpportunityBizDocs','numModifiedBy'),__$update_mask) = 1) THEN CAST(numModifiedBy AS TEXT) ELSE NULL END AS numModifiedBy,
							CASE WHEN (sys.fn_cdc_is_bit_set(sys.fn_cdc_get_column_ordinal('dbo_OpportunityBizDocs','monAmountPaid'),__$update_mask) = 1) THEN CAST(monAmountPaid as TEXT) ELSE NULL END AS monAmountPaid,
							CASE WHEN (sys.fn_cdc_is_bit_set(sys.fn_cdc_get_column_ordinal('dbo_OpportunityBizDocs','vcComments'),__$update_mask) = 1) THEN CAST(vcComments as TEXT) ELSE NULL END AS vcComments,
							CASE WHEN (sys.fn_cdc_is_bit_set(sys.fn_cdc_get_column_ordinal('dbo_OpportunityBizDocs','vcBizDocID'),__$update_mask) = 1) THEN CAST(vcBizDocID as TEXT) ELSE NULL END AS vcBizDocID,
							CASE WHEN (sys.fn_cdc_is_bit_set(sys.fn_cdc_get_column_ordinal('dbo_OpportunityBizDocs','numShipVia'),__$update_mask) = 1) THEN CAST(numShipVia AS TEXT) ELSE NULL END AS numShipVia,
							CASE WHEN (sys.fn_cdc_is_bit_set(sys.fn_cdc_get_column_ordinal('dbo_OpportunityBizDocs','vcTrackingNo'),__$update_mask) = 1) THEN CAST(vcTrackingNo AS TEXT) ELSE NULL END AS vcTrackingNo,
							CASE WHEN (sys.fn_cdc_is_bit_set(sys.fn_cdc_get_column_ordinal('dbo_OpportunityBizDocs','numBizDocStatus'),__$update_mask) = 1) THEN CAST(numBizDocStatus AS TEXT) ELSE NULL END AS numBizDocStatus,
							CASE WHEN (sys.fn_cdc_is_bit_set(sys.fn_cdc_get_column_ordinal('dbo_OpportunityBizDocs','dtFromDate'),__$update_mask) = 1) THEN CAST(dtFromDate AS TEXT) ELSE NULL END AS dtFromDate
               FROM
               cdc.fn_cdc_get_all_changes_dbo_OpportunityBizDocs(v_vbStartlsn,v_vbStartlsn,N'all update old')
               WHERE
               __$operation = 3) as t1
            UNPIVOT(old_value FOR column_name IN(dtModifiedDate,numModifiedBy,monAmountPaid,vcComments,vcBizDocID,numShipVia,
            vcTrackingNo,numBizDocStatus,dtFromDate)) as unp) as TempUpdateBefore
         INNER JOIN(SELECT
            __$start_lsn,
						column_name,
						new_value
            FROM(SELECT
               __$start_lsn,
							CASE WHEN (sys.fn_cdc_is_bit_set(sys.fn_cdc_get_column_ordinal('dbo_OpportunityBizDocs','dtModifiedDate'),__$update_mask) = 1) THEN CAST(dtModifiedDate AS TEXT) ELSE NULL END AS dtModifiedDate,
							CASE WHEN (sys.fn_cdc_is_bit_set(sys.fn_cdc_get_column_ordinal('dbo_OpportunityBizDocs','numModifiedBy'),__$update_mask) = 1) THEN CAST(numModifiedBy AS TEXT) ELSE NULL END AS numModifiedBy,
							CASE WHEN (sys.fn_cdc_is_bit_set(sys.fn_cdc_get_column_ordinal('dbo_OpportunityBizDocs','monAmountPaid'),__$update_mask) = 1) THEN CAST(monAmountPaid as TEXT) ELSE NULL END AS monAmountPaid,
							CASE WHEN (sys.fn_cdc_is_bit_set(sys.fn_cdc_get_column_ordinal('dbo_OpportunityBizDocs','vcComments'),__$update_mask) = 1) THEN CAST(vcComments as TEXT) ELSE NULL END AS vcComments,
							CASE WHEN (sys.fn_cdc_is_bit_set(sys.fn_cdc_get_column_ordinal('dbo_OpportunityBizDocs','vcBizDocID'),__$update_mask) = 1) THEN CAST(vcBizDocID as TEXT) ELSE NULL END AS vcBizDocID,
							CASE WHEN (sys.fn_cdc_is_bit_set(sys.fn_cdc_get_column_ordinal('dbo_OpportunityBizDocs','numShipVia'),__$update_mask) = 1) THEN CAST(numShipVia AS TEXT) ELSE NULL END AS numShipVia,
							CASE WHEN (sys.fn_cdc_is_bit_set(sys.fn_cdc_get_column_ordinal('dbo_OpportunityBizDocs','vcTrackingNo'),__$update_mask) = 1) THEN CAST(vcTrackingNo AS TEXT) ELSE NULL END AS vcTrackingNo,
							CASE WHEN (sys.fn_cdc_is_bit_set(sys.fn_cdc_get_column_ordinal('dbo_OpportunityBizDocs','numBizDocStatus'),__$update_mask) = 1) THEN CAST(numBizDocStatus AS TEXT) ELSE NULL END AS numBizDocStatus,
							CASE WHEN (sys.fn_cdc_is_bit_set(sys.fn_cdc_get_column_ordinal('dbo_OpportunityBizDocs','dtFromDate'),__$update_mask) = 1) THEN CAST(dtFromDate AS TEXT) ELSE NULL END AS dtFromDate
               FROM
               cdc.fn_cdc_get_all_changes_dbo_OpportunityBizDocs(v_vbStartlsn,v_vbStartlsn,N'all') -- 'all update old' is not necessary here
               WHERE
               __$operation = 4) as t2
            UNPIVOT(new_value FOR column_name IN(dtModifiedDate,numModifiedBy,monAmountPaid,vcComments,vcBizDocID,numShipVia,
            vcTrackingNo,numBizDocStatus,dtFromDate)) as unp) as TempUpdateAfter -- after update
         ON
         TempUpdateBefore.__$start_lsn = TempUpdateAfter.__$start_lsn
         AND TempUpdateBefore.column_name = TempUpdateAfter.column_name;
         v_j := 1;
         select   COUNT(*) INTO v_jCOUNT FROM tt_TMEPCHANGEDCOLUMNS;
         WHILE v_j <= v_jCOUNT LOOP
            select   dtDate, vcColumnName, vcOldValue, vcNewValue INTO v_dtDate,v_vcColumnName,v_vcOldValue,v_vcNewValue FROM
            tt_TMEPCHANGEDCOLUMNS WHERE
            ID = v_j;
            v_numNewModifiedBy := -1;
            IF v_vcColumnName = 'numModifiedBy' then
				
					-- KEEP VALUES IN TEMPORARY VARIABLE AND VALUES FOR ALL FIELDS UPDATE AFTER LOOP COMPLETES.
               v_numNewModifiedBy := CAST(v_vcNewValue AS NUMERIC(18,0));
            ELSEIF v_vcColumnName = 'dtModifiedDate'
            then
				
               BEGIN
						/* OpportunityBizDocs Updated Items */
                  INSERT INTO tt_TEMPOPPORTUNITYHISTORY(vbStartlsn,
							numRecordID,
							numDomainID,
							dtDate,
							numUserCntID,
							vcEvent,
							vcFieldName,
							vcOldValue,
							vcNewValue,
							vcDescription,
							vcHiddenDescription,
							numFieldID,
							bitCustomField)
                  SELECT
                  CAST(v_vbStartlsn AS BYTEA),
							v_numOppID,
							v_numDomainID,
							v_bintModifiedDate,
							v_numModifiedBy,
							CAST('Changed BizDoc Item' AS VARCHAR(200)),
							CAST((CASE TempUpdateBefore.column_name
                  WHEN 'numUnitHour' THEN 'Unit Hour'
                  WHEN 'vcNotes' THEN 'Notes'
                  ELSE TempUpdateBefore.column_name
                  END) AS VARCHAR(200)),
							TempUpdateBefore.old_value,
							TempUpdateAfter.new_value,
							CONCAT('Item Code:',TempUpdateAfter.ItemCode,', Item Name:',TempUpdateAfter.ItemName),
							CONCAT('OppBizDocItemID:',TempUpdateAfter.OppItemID,', numWarehouseItemID:',TempUpdateAfter.WarehouseItemID),
							NULL,
							CAST(0 AS BOOLEAN)
                  FROM(SELECT
                     __$start_lsn,
									OppItemID,
									ItemCode,
									ItemName,
									WarehouseItemID,
									column_name,
									old_value
                     FROM(SELECT
                        __$start_lsn,
										numOppBizDocItemID As OppItemID,
										CDCOppBixDocItems.numItemCode As ItemCode,
										vcItemName As ItemName,
										numWarehouseItmsID As WarehouseItemID,
										CASE WHEN (sys.fn_cdc_is_bit_set(sys.fn_cdc_get_column_ordinal('dbo_OpportunityBizDocItems','numItemCode'),__$update_mask) = 1) THEN CAST(CDCOppBixDocItems.numItemCode AS TEXT) ELSE NULL END AS numItemCode,
										CASE WHEN (sys.fn_cdc_is_bit_set(sys.fn_cdc_get_column_ordinal('dbo_OpportunityBizDocItems','numUnitHour'),__$update_mask) = 1) THEN CAST(numUnitHour AS TEXT) ELSE NULL END AS numUnitHour,
										CASE WHEN (sys.fn_cdc_is_bit_set(sys.fn_cdc_get_column_ordinal('dbo_OpportunityBizDocItems','vcNotes'),__$update_mask) = 1) THEN CAST(vcNotes AS TEXT) ELSE NULL END AS vcNotes
                        FROM
                        cdc.fn_cdc_get_all_changes_dbo_OpportunityBizDocItems(v_vbStartlsn,v_vbStartlsn,N'all update old') AS CDCOppBixDocItems
                        JOIN
                        Item
                        ON
                        CDCOppBixDocItems.numItemCode = Item.numItemCode
                        WHERE
                        __$operation = 3) as t1
                     UNPIVOT(old_value FOR column_name IN(numItemCode,numUnitHour,vcNotes)) as unp) as TempUpdateBefore
                  INNER JOIN(SELECT
                     __$start_lsn,
									OppItemID,
									ItemCode,
									ItemName,
									WarehouseItemID,
									column_name,
									new_value
                     FROM(SELECT
                        __$start_lsn,
										numOppBizDocItemID As OppItemID,
										CDCOppBixDocItems.numItemCode As ItemCode,
										Item.vcItemName As ItemName,
										numWarehouseItmsID As WarehouseItemID,
										CASE WHEN (sys.fn_cdc_is_bit_set(sys.fn_cdc_get_column_ordinal('dbo_OpportunityBizDocItems','numItemCode'),__$update_mask) = 1) THEN CAST(CDCOppBixDocItems.numItemCode AS TEXT) ELSE NULL END AS numItemCode,
										CASE WHEN (sys.fn_cdc_is_bit_set(sys.fn_cdc_get_column_ordinal('dbo_OpportunityBizDocItems','numUnitHour'),__$update_mask) = 1) THEN CAST(numUnitHour AS TEXT) ELSE NULL END AS numUnitHour,
										CASE WHEN (sys.fn_cdc_is_bit_set(sys.fn_cdc_get_column_ordinal('dbo_OpportunityBizDocItems','vcNotes'),__$update_mask) = 1) THEN CAST(vcNotes AS TEXT) ELSE NULL END AS vcNotes
                        FROM
                        cdc.fn_cdc_get_all_changes_dbo_OpportunityBizDocItems(v_vbStartlsn,v_vbStartlsn,N'all') AS CDCOppBixDocItems -- 'all update old' is not necessary here
                        JOIN
                        Item
                        ON
                        CDCOppBixDocItems.numItemCode = Item.numItemCode
                        WHERE
                        __$operation = 4) as t2
                     UNPIVOT(new_value FOR column_name IN(numItemCode,numUnitHour,vcNotes)) as unp) as TempUpdateAfter -- after update
                  ON
                  TempUpdateBefore.__$start_lsn = TempUpdateAfter.__$start_lsn
                  AND TempUpdateBefore.column_name = TempUpdateAfter.column_name
                  AND TempUpdateBefore.OppItemID = TempUpdateAfter.OppItemID
                  AND coalesce(TempUpdateBefore.WarehouseItemID,0) = coalesce(TempUpdateAfter.WarehouseItemID,0);
                  EXCEPTION WHEN OTHERS THEN
						/* If the specified LSN range does not fall within the change tracking timeline for the capture instance, the function returns error 208 
						("An insufficient number of arguments were supplied for the procedure or function cdc.fn_cdc_get_all_changes."). */
                     NULL;
               END;
               BEGIN
						/* OpportunityBizDocs Deleted Items */
                  INSERT INTO tt_TEMPOPPORTUNITYHISTORY(vbStartlsn,
							numRecordID,
							numDomainID,
							dtDate,
							numUserCntID,
							vcEvent,
							vcDescription,
							vcHiddenDescription,
							numFieldID,
							bitCustomField)
                  SELECT
                  CAST(v_vbStartlsn AS BYTEA),
							v_numOppID,
							v_numDomainID,
							v_bintModifiedDate,
							v_numModifiedBy,
							CAST('Removed BizDoc Item' AS VARCHAR(200)),
							CONCAT('Item Code:',TempDelete.ItemCode,', Item Name:',TempDelete.ItemName),
							CONCAT('OppBizDocItemID:',TempDelete.OppItemID,', numWarehouseItemID:',TempDelete.WarehouseItemID),
							NULL,
							CAST(0 AS BOOLEAN)
                  FROM(SELECT
                     __$start_lsn,
									CDCOppBixDocItems.numOppBizDocItemID As OppItemID,
									CDCOppBixDocItems.numItemCode As ItemCode,
									vcItemName As ItemName,
									numWarehouseItmsID As WarehouseItemID
                     FROM
                     cdc.dbo_OpportunityBizDocItems_CT CDCOppBixDocItems
                     JOIN
                     Item
                     ON
                     CDCOppBixDocItems.numItemCode = Item.numItemCode
                     WHERE
                     __$operation = 1
                     AND __$start_lsn = v_vbStartlsn) AS TempDelete;
                  EXCEPTION WHEN OTHERS THEN
						/* If the specified LSN range does not fall within the change tracking timeline for the capture instance, the function returns error 208 
						("An insufficient number of arguments were supplied for the procedure or function cdc.fn_cdc_get_all_changes."). */
                     NULL;
               END;
            ELSE
               INSERT INTO tt_TEMPOPPORTUNITYHISTORY(vbStartlsn,
						numRecordID,
						numDomainID,
						dtDate,
						numUserCntID,
						vcEvent,
						vcFieldName,
						vcOldValue,
						vcNewValue,
						numFieldID,
						bitCustomField)
					VALUES(CAST(v_vbStartlsn AS BYTEA),
						v_numOppID,
						v_numDomainID,
						v_dtDate,
						v_numModifiedBy,
						CAST('Changed BizDoc' AS VARCHAR(200)),
						CAST(CASE
               WHEN v_vcColumnName = 'monAmountPaid' THEN 'Paid Amount'
               WHEN v_vcColumnName = 'vcComments' THEN 'Comments'
               WHEN v_vcColumnName = 'vcBizDocID' THEN 'BizDoc ID'
               WHEN v_vcColumnName = 'numShipVia' THEN 'Shipping Company'
               WHEN v_vcColumnName = 'vcTrackingNo' THEN 'Tracking No'
               WHEN v_vcColumnName = 'numBizDocStatus' THEN 'BizDoc Status'
               WHEN v_vcColumnName = 'dtFromDate' THEN 'Billing Date'
               END AS VARCHAR(200)),
						(CASE v_vcColumnName
               WHEN 'numShipVia' THEN
                  coalesce((SELECT vcData FROM Listdetails WHERE numDomainID = v_numDomainID AND numListID = 82 AND numListItemID = v_vcOldValue), '')
               WHEN 'numBizDocStatus' THEN
                  coalesce((SELECT vcData FROM Listdetails WHERE numDomainID = v_numDomainID AND numListID = 11 AND numListItemID = v_vcOldValue), '')
               ELSE
                  v_vcOldValue
               END),
						(CASE v_vcColumnName
               WHEN 'numShipVia' THEN
                  coalesce((SELECT vcData FROM Listdetails WHERE numDomainID = v_numDomainID AND numListID = 82 AND numListItemID = v_vcNewValue), '')
               WHEN 'numBizDocStatus' THEN
                  coalesce((SELECT vcData FROM Listdetails WHERE numDomainID = v_numDomainID AND numListID = 11 AND numListItemID = v_vcNewValue), '')
               ELSE
                  v_vcNewValue
               END),
						NULL,
						CAST(0 AS BOOLEAN));
            end if;
            v_j := v_j::bigint+1;
         END LOOP;
         IF v_numNewModifiedBy <> -1 then
			
            UPDATE tt_TEMPOPPORTUNITYHISTORY SET numUserCntID = v_numNewModifiedBy WHERE vbStartlsn = v_vbStartlsn;
         end if;
      end if;
      v_i := v_i+1;
   END LOOP;

	--CLEAR RETRIVED DATA FROM CDC TABLE
   DELETE FROM cdc.dbo_OpportunityBizDocItems_CT WHERE __$start_lsn IN(SELECT vbStartlsn FROM tt_TEMPOPPORTUNITYHISTORY);
   DELETE FROM cdc.dbo_OpportunityBizDocs_CT WHERE __$start_lsn IN(SELECT vbStartlsn FROM tt_TEMPOPPORTUNITYHISTORY);
	

	/************************************ OPP/ORDER CUSTOM FIELDS **********************************/

   DELETE FROM tt_TEMPTABLE;

   INSERT INTO
   tt_TEMPTABLE
   SELECT
   ROW_NUMBER() OVER(ORDER BY __$start_lsn),
		__$start_lsn
   FROM
   cdc.dbo_CFW_Fld_Values_Opp_CT
   WHERE
   __$operation IN(2,3)
   GROUP BY
   __$start_lsn;

   v_i := 1;
   select   COUNT(*) INTO v_iCOUNT FROM tt_TEMPTABLE;
   WHILE v_i <= v_iCOUNT LOOP
      select   vbStartlsn INTO v_vbStartlsn FROM tt_TEMPTABLE WHERE ID = v_i;
      select   __$operation, numOppId, numDomainID, Fld_Id, Fld_Value, cdc.dbo_CFW_Fld_Values_Opp_CT.bintModifiedDate, cdc.dbo_CFW_Fld_Values_Opp_CT.numModifiedBy INTO v_tintOperation,v_numOppID,v_numDomainID,v_Fld_ID,v_Fld_Value,v_bintModifiedDate,
      v_numModifiedBy FROM
      cdc.dbo_CFW_Fld_Values_Opp_CT
      JOIN
      OpportunityMaster
      ON
      cdc.dbo_CFW_Fld_Values_Opp_CT.RecId = OpportunityMaster.numOppId WHERE
      __$start_lsn = v_vbStartlsn    LIMIT 1;
      IF v_tintOperation = 2 then --INSERT
		
         INSERT INTO tt_TEMPOPPORTUNITYHISTORY(vbStartlsn,
				numRecordID,
				numDomainID,
				dtDate,
				numUserCntID,
				vcEvent,
				vcOldValue,
				vcNewValue,
				vcDescription,
				vcHiddenDescription,
				numFieldID,
				bitCustomField)
			VALUES(CAST(v_vbStartlsn AS BYTEA),
				v_numOppID,
				v_numDomainID,
				v_bintModifiedDate,
				v_numModifiedBy,
				CAST('Change' AS VARCHAR(200)),
				'',
				GetCustFldValue(v_Fld_ID,NULL::SMALLINT,v_numOppID),
				'',
				'',
				v_Fld_ID,
				CAST(1 AS BOOLEAN));
      ELSEIF v_tintOperation = 3
      then --UPDATE
		
         INSERT INTO tt_TEMPOPPORTUNITYHISTORY(vbStartlsn,
				numRecordID,
				numDomainID,
				dtDate,
				numUserCntID,
				vcEvent,
				vcOldValue,
				vcNewValue,
				numFieldID,
				vcFieldName,
				bitCustomField)
         SELECT
         CAST(v_vbStartlsn AS BYTEA),
				v_numOppID,
				v_numDomainID,
				TempUpdateAfter.bintModifiedDate,
				TempUpdateAfter.numModifiedBy,
				CAST('Change' AS VARCHAR(200)),
				fn_GetCustFldStringValue(TempUpdateBefore.Fld_ID,v_numOppID,TempUpdateBefore.old_value),
				fn_GetCustFldStringValue(TempUpdateBefore.Fld_ID,v_numOppID,TempUpdateAfter.new_value),
				TempUpdateBefore.Fld_ID,
				(SELECT FLd_label FROM CFW_Fld_Master DFM WHERE DFM.Fld_id = TempUpdateBefore.Fld_ID) AS vcFieldName,
				CAST(1 AS BOOLEAN)
         FROM(SELECT
            __$start_lsn,
						Fld_id,
						RecId,
						numModifiedBy,
						bintModifiedDate,
						old_value
            FROM(SELECT
               __$start_lsn,
							Fld_id,
							RecId,
							numModifiedBy,
							bintModifiedDate,
							CASE WHEN (sys.fn_cdc_is_bit_set(sys.fn_cdc_get_column_ordinal('dbo_CFW_Fld_Values_Opp','Fld_Value'),__$update_mask) = 1) THEN CAST(Fld_Value AS TEXT) ELSE NULL END AS Fld_Value
               FROM
               cdc.fn_cdc_get_all_changes_dbo_CFW_Fld_Values_Opp(v_vbStartlsn,v_vbStartlsn,N'all update old')
               WHERE
               __$operation = 3) as t1
            UNPIVOT(old_value FOR column_name IN(Fld_Value)) as unp) as TempUpdateBefore
         INNER JOIN(SELECT
            __$start_lsn,
						Fld_id,
						RecId,
						numModifiedBy,
						bintModifiedDate,
						new_value
            FROM(SELECT
               __$start_lsn,
							Fld_id,
							RecId,
							numModifiedBy,
							bintModifiedDate,
							CASE WHEN (sys.fn_cdc_is_bit_set(sys.fn_cdc_get_column_ordinal('dbo_CFW_Fld_Values_Opp','Fld_Value'),__$update_mask) = 1) THEN CAST(Fld_Value AS TEXT) ELSE NULL END AS Fld_Value
               FROM
               cdc.fn_cdc_get_all_changes_dbo_CFW_Fld_Values_Opp(v_vbStartlsn,v_vbStartlsn,N'all') -- 'all update old' is not necessary here
               WHERE
               __$operation = 4) as t2
            UNPIVOT(new_value FOR column_name IN(Fld_Value)) as unp) as TempUpdateAfter -- after update
         ON
         TempUpdateBefore.__$start_lsn = TempUpdateAfter.__$start_lsn
         AND TempUpdateBefore.Fld_id = TempUpdateAfter.Fld_id
         AND TempUpdateBefore.RecId = TempUpdateAfter.RecId;
      end if;
      v_i := v_i+1;
   END LOOP;

	--CLEAR RETRIVED DATA FROM CDC TABLE
   DELETE FROM cdc.dbo_CFW_Fld_Values_Opp_CT WHERE __$start_lsn IN(SELECT vbStartlsn FROM tt_TEMPOPPORTUNITYHISTORY);

   BEGIN
      -- BEGIN TRANSACTION


		--INSERT DATA INTO OUR RECORD HISTORY TABLE
		INSERT INTO RecordHistory(numDomainID,
			numUserCntID,
			dtDate,
			numRecordID,
			numRHModuleMasterID,
			vcEvent,
			numFieldID,
			vcFieldName,
			bitCustomField,
			vcOldValue,
			vcNewValue,
			vcDescription,
			vcHiddenDescription)
      SELECT
      numDomainID,
			numUserCntID,
			coalesce(dtDate,TIMEZONE('UTC',now())),
			numRecordID,
			3,
			vcEvent,
			numFieldID,
			vcFieldName,
			bitCustomField,
			vcOldValue,
			vcNewValue,
			vcDescription,
			vcHiddenDescription
      FROM
      tt_TEMPOPPORTUNITYHISTORY;
      -- COMMIT
EXCEPTION WHEN OTHERS THEN
         -- ROLLBACK TRANSACTION
v_ErrorMessage := SQLERRM;
         v_ErrorNumber := sqlstate;
         v_ErrorSeverity := ERROR_SEVERITY();
         v_ErrorState := ERROR_STATE();
         v_ErrorLine := ERROR_LINE();
         v_ErrorProcedure := coalesce(ERROR_PROCEDURE(),'-');
         RAISE NOTICE '% % % % % % ',v_ErrorMessage,v_ErrorNumber,v_ErrorSeverity,v_ErrorState,v_ErrorProcedure,
         v_ErrorLine;
   END;
   RETURN;
END; $$;

