-- Stored procedure definition script USP_DeleteMasterTabs for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_DeleteMasterTabs(v_numTabId NUMERIC(9,0) DEFAULT 0)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   delete from TabMaster where numTabId = v_numTabId and bitFixed = false;
   RETURN;
END; $$;


