-- Stored procedure definition script USP_ActivityFormConfiguration_Save for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ActivityFormConfiguration_Save(v_numDomainID NUMERIC(18,0),
	v_numGroupId NUMERIC(18,0),
	v_bitFollowupStatus BOOLEAN DEFAULT false,
	v_bitPriority BOOLEAN DEFAULT false,
	v_bitActivity BOOLEAN DEFAULT false,
	v_bitCustomField BOOLEAN DEFAULT false,
	v_bitFollowupAnytime BOOLEAN DEFAULT false,
	v_bitAttendee BOOLEAN DEFAULT false,
	v_bitTitle BOOLEAN DEFAULT false,
	v_bitDescription BOOLEAN DEFAULT false,
	v_bitLocation BOOLEAN DEFAULT false,
		v_numUserCntId NUMERIC(18,0) DEFAULT NULL)
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
	my_ex_state TEXT;
	my_ex_message TEXT;
	my_ex_detail TEXT;
	my_ex_hint TEXT;
	my_ex_ctx TEXT;
BEGIN
   BEGIN
      -- BEGIN TRANSACTION
IF EXISTS(SELECT numActivityAuthenticationId FROM ActivityFormAuthentication WHERE numDomainID = v_numDomainID AND numGroupId = v_numGroupId) then
		
         UPDATE
         ActivityFormAuthentication
         SET
         bitFollowupStatus = v_bitFollowupStatus,bitPriority = v_bitPriority,bitActivity = v_bitActivity,
         bitCustomField = v_bitCustomField,bitFollowupAnytime = v_bitFollowupAnytime,
         bitAttendee = v_bitAttendee
         WHERE
         numDomainID = v_numDomainID AND
         numGroupId = v_numGroupId;
      ELSE
         INSERT INTO ActivityFormAuthentication(numDomainID,
					numGroupId,
					bitFollowupStatus,
					bitPriority,
					bitActivity,
					bitCustomField,
					bitFollowupAnytime,
					bitAttendee)
			VALUES(v_numDomainID,
					v_numGroupId,
					v_bitFollowupStatus,
					v_bitPriority,
					v_bitActivity,
					v_bitCustomField,
					v_bitFollowupAnytime,
					v_bitAttendee);
      end if;
      IF EXISTS(SELECT numDisplayId FROM ActivityDisplayConfiguration WHERE numDomainID = v_numDomainID AND numUserCntId = v_numUserCntId) then
		
         UPDATE
         ActivityDisplayConfiguration
         SET
         bitTitle = v_bitTitle,bitLocation = v_bitLocation,bitDescription = v_bitDescription
         WHERE
         numDomainID = v_numDomainID AND
         numUserCntId = v_numUserCntId;
      ELSE
         INSERT INTO ActivityDisplayConfiguration(numDomainID,
					numUserCntId,
					bitTitle,
					bitLocation,
					bitDescription)
			VALUES(v_numDomainID,
					v_numUserCntId,
					v_bitTitle,
					v_bitLocation,
					v_bitDescription);
      end if;
      -- COMMIT
EXCEPTION WHEN OTHERS THEN
         -- ROLLBACK TRANSACTION
			GET STACKED DIAGNOSTICS
			  my_ex_state   = RETURNED_SQLSTATE,
			  my_ex_message = MESSAGE_TEXT,
			  my_ex_detail  = PG_EXCEPTION_DETAIL,
			  my_ex_hint    = PG_EXCEPTION_HINT,
			  my_ex_ctx     = PG_EXCEPTION_CONTEXT;

			 raise exception '% % % % %', my_ex_state, my_ex_message, my_ex_detail, my_ex_hint, my_ex_ctx;
   END;
   RETURN;
END; $$;



