-- Stored procedure definition script USP_GetAuthorizativeOpportuntiy for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_GetAuthorizativeOpportuntiy(v_numDomainId NUMERIC(9,0) DEFAULT 0,  
v_numOppId NUMERIC(9,0) DEFAULT 0, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_tintOppType  SMALLINT;
BEGIN
   select   tintopptype INTO v_tintOppType From OpportunityMaster Where numOppId = v_numOppId And  numDomainId = v_numDomainId;         
   RAISE NOTICE '%',v_tintOppType; 
 
   If v_tintOppType = 1 then
      open SWV_RefCur for
      Select coalesce(numAuthoritativeSales,0) From AuthoritativeBizDocs Where numDomainId = v_numDomainId;
   ELSEIF v_tintOppType = 2
   then
      open SWV_RefCur for
      Select coalesce(numAuthoritativePurchase,0) From AuthoritativeBizDocs Where numDomainId = v_numDomainId;
   end if;
   RETURN;
END; $$;


