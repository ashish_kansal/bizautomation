-- Stored procedure definition script USP_HierarchyGridForProcumentBudget for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_HierarchyGridForProcumentBudget(v_numDomainId NUMERIC(9,0) DEFAULT 0,          
 v_numProcurementId NUMERIC(9,0) DEFAULT 0,          
 v_intType INTEGER DEFAULT 0, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numItemGroupId  NUMERIC(9,0);                              
   v_vcItemGroup  VARCHAR(150);                              
   v_Count  NUMERIC(9,0);                 
   v_lstrSQL  VARCHAR(8000);             
   v_lstr  VARCHAR(8000);                 
   v_numMonth  SMALLINT;
   v_Date  TIMESTAMP;     
   v_dtFiscalStDate  TIMESTAMP;                        
   v_dtFiscalEndDate  TIMESTAMP;
BEGIN
   v_numMonth := 1;                
   v_lstrSQL := '';             
   v_lstr := '';             
   v_Date := TIMEZONE('UTC',now())+CAST(v_intType || 'year' as interval);               
              
   While v_numMonth <= 12 LOOP
      v_dtFiscalStDate := GetFiscalStartDate(GetFiscalyear(v_Date,v_numDomainId)::INTEGER,v_numDomainId);
      v_dtFiscalStDate := v_dtFiscalStDate+CAST(v_numMonth -1 || 'month' as interval);
      v_dtFiscalEndDate :=  GetFiscalStartDate(GetFiscalyear(v_Date,v_numDomainId)::INTEGER,v_numDomainId)+INTERVAL '1 year'+INTERVAL '-1 day';
      RAISE NOTICE '%',v_dtFiscalStDate;
      v_lstrSQL := coalesce(v_lstrSQL,'') || ' ' ||  'fn_GetProcurementBudgetMonthDetail(' || SUBSTR(CAST(v_numProcurementId AS VARCHAR(10)),1,10) || ',' || CAST(EXTRACT(month FROM v_dtFiscalStDate) AS VARCHAR(2)) || '::SMALLINT,' || CAST(EXTRACT(year FROM v_dtFiscalStDate) AS VARCHAR(4)) || ',' || SUBSTR(CAST(v_numDomainId AS VARCHAR(10)),1,10) ||
      ',IG.numItemGroupID)'
      || ' as "' || CAST(EXTRACT(month FROM v_dtFiscalStDate) AS VARCHAR(2)) || '~' || CAST(EXTRACT(year FROM v_dtFiscalStDate) AS VARCHAR(4)) || '",';
      v_numMonth := v_numMonth+1;
   END LOOP; 
   v_dtFiscalStDate := GetFiscalStartDate(GetFiscalyear(v_Date,v_numDomainId)::INTEGER,v_numDomainId);               
   v_lstrSQL := coalesce(v_lstrSQL,'') ||
	--' fn_GetProcurementBudgetMonthTotalAmt('+Convert(varchar(10),@numProcurementId)+','''+convert(varchar(500),@Date)+''',IG.numItemGroupID,'+convert(varchar(10),@numDomainId)+') as Total' +         
	--',fn_GetProcurementBudgetComments('+Convert(varchar(10),@numProcurementId)+','+Convert(varchar(10),@numDomainId)+',IG.numItemGroupID) as  Comments'         
   ' (Select Sum(PBD.monAmount)  From ProcurementBudgetMaster PBM  
	inner join ProcurementBudgetDetails PBD on PBM.numProcurementBudgetId=PBD.numProcurementId  
	Where PBM.numDomainId=' || SUBSTR(CAST(v_numDomainId AS VARCHAR(10)),1,10) || ' And PBD.numItemGroupId=IG.numItemGroupID  And ((PBD.tintMonth between EXTRACT(MONTH FROM ''' || SUBSTR(CAST(v_dtFiscalStDate AS VARCHAR(100)),1,100) || '''::TIMESTAMP) and 12) and PBD.intYear=EXTRACT(YEAR FROM ''' || SUBSTR(CAST(v_dtFiscalStDate AS VARCHAR(100)),1,100) ||
   '''::TIMESTAMP)) Or ((PBD.tintMonth between 1 and EXTRACT(MONTH FROM ''' || SUBSTR(CAST(v_dtFiscalEndDate AS VARCHAR(100)),1,100) || '''::TIMESTAMP)) and PBD.intYear=EXTRACT(YEAR FROM ''' || SUBSTR(CAST(v_dtFiscalEndDate AS VARCHAR(100)),1,100) || '''::TIMESTAMP)))  as Total
    ,(Select COALESCE(PBD.vcComments,'''') From ProcurementBudgetMaster PBM    
	inner join ProcurementBudgetDetails PBD on PBM.numProcurementBudgetId=PBD.numProcurementId     
	Where PBM.numDomainId=' || SUBSTR(CAST(v_numDomainId AS VARCHAR(10)),1,10) || ' And PBD.numProcurementId=' || SUBSTR(CAST(v_numProcurementId AS VARCHAR(10)),1,10) || ' And PBD.numItemGroupId=IG.numItemGroupID'
   || ' And PBD.tintMonth=EXTRACT(MONTH FROM ''' || SUBSTR(CAST(v_dtFiscalEndDate AS VARCHAR(100)),1,100) || '''::TIMESTAMP) And PBD.intYear=EXTRACT(YEAR FROM ''' || SUBSTR(CAST(v_dtFiscalEndDate AS VARCHAR(100)),1,100) || '''::TIMESTAMP) LIMIT 1)as  Comments';  
   RAISE NOTICE '%',v_lstrSQL;                      
   v_lstr  := 'Select IG.numItemGroupID as numItemGroupId,IG.vcItemGroup as vcItemGroupName,IG.numDomainID as numDomainID,' || SUBSTR(CAST(v_lstrSQL AS VARCHAR(8000)),1,8000)
   || ' From ItemGroups IG Where IG.numDomainID =' || SUBSTR(CAST(v_numDomainId AS VARCHAR(10)),1,10);                
               
   RAISE NOTICE '%',(v_lstr);            
   OPEN SWV_RefCur FOR EXECUTE v_lstr;
   RETURN;
END; $$;


