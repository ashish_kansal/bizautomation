-- Stored procedure definition script USP_GetDeleteTask for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_GetDeleteTask(v_numDomainID NUMERIC(9,0) DEFAULT 0,    
v_numTaskId NUMERIC(18,0) DEFAULT 0)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   DELETE FROM  StagePercentageDetailsTask WHERE numTaskId = v_numTaskId AND numDomainID = v_numDomainID;
   RETURN;
END; $$; 


