-- Stored procedure definition script USP_GetOpenCases for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetOpenCases(v_numDivisionID NUMERIC(9,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
	open SWV_RefCur for 
	select 
		numCaseId AS "numCaseId1"
		,coalesce(vcCaseNumber,'') || ', ' || coalesce(fn_GetContactName(numCreatedby),'') || ', ' || coalesce(CAST(bintCreatedDate AS VARCHAR),'') || ', ' || coalesce(textSubject,'') as "vcCaseNumber"
		,CAST(numCaseId AS VARCHAR(15)) || '~' || CAST(numDivisionID AS VARCHAR(15)) || '~' || CAST(numContactId AS VARCHAR(15)) as "numCaseId"
	from 
		Cases 
	where 
		numStatus <> 36 
		and numDivisionID = v_numDivisionID;
END; $$;
