-- Stored procedure definition script USP_GetPromotionOffer for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_GetPromotionOffer(v_numProId NUMERIC(9,0) DEFAULT 0,
    v_byteMode SMALLINT DEFAULT 0,
    v_numDomainID NUMERIC(9,0) DEFAULT 0,
	v_ClientTimeZoneOffset INTEGER DEFAULT NULL, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
BEGIN
   IF v_byteMode = 0 then
    
      open SWV_RefCur for
      SELECT
      numProId
			,vcProName
			,numDomainId
			,(CASE WHEN dtValidFrom IS NOT NULL AND coalesce(bitNeverExpires,false) = false THEN dtValidFrom+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) ELSE dtValidFrom END) AS dtValidFrom
			,(CASE WHEN dtValidTo IS NOT NULL AND coalesce(bitNeverExpires,false) = false THEN dtValidTo+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) ELSE dtValidTo END) AS dtValidTo
			,bitNeverExpires
			,tintOfferTriggerValueType
			,fltOfferTriggerValue
			,tintOfferBasedOn
			,fltDiscountValue
			,tintDiscountType
			,tintDiscoutBaseOn
			,numCreatedBy
			,dtCreated
			,numModifiedBy
			,dtModified
			,tintCustomersBasedOn
			,tintOfferTriggerValueTypeRange
			,fltOfferTriggerValueRange
			,IsOrderBasedPromotion
			,vcShortDesc
			,vcLongDesc
			,tintItemCalDiscount
			,coalesce(bitUseOrderPromotion,false) AS bitUseOrderPromotion
			,coalesce(numOrderPromotionID,0) AS numOrderPromotionID
			,coalesce(monDiscountedItemPrice,0) AS monDiscountedItemPrice
      FROM
      PromotionOffer
      WHERE
      numProId = v_numProId AND coalesce(IsOrderBasedPromotion,false) = false;
   ELSEIF v_byteMode = 1
   then
    
      open SWV_RefCur for
      SELECT
      PO.numProId,
            PO.vcProName,
			(CASE WHEN coalesce(PO.IsOrderBasedPromotion,false) = true THEN 'Order based' ELSE 'Item based' END) AS vcPromotionType,
			(CASE
      WHEN coalesce(PO.numOrderPromotionID,0) > 0
      THEN(CASE WHEN coalesce(POOrder.bitNeverExpires,false) = true THEN 'Promotion Never Expires' ELSE CONCAT(FormatedDateFromDate((CASE WHEN POOrder.dtValidFrom IS NOT NULL THEN POOrder.dtValidFrom+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) ELSE POOrder.dtValidFrom END),v_numDomainID),' to ',FormatedDateFromDate((CASE WHEN POOrder.dtValidTo IS NOT NULL THEN POOrder.dtValidTo+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) ELSE POOrder.dtValidTo END),v_numDomainID)) END)
      ELSE(CASE WHEN coalesce(PO.bitNeverExpires,false) = true THEN 'Promotion Never Expires' ELSE CONCAT(FormatedDateFromDate((CASE WHEN PO.dtValidFrom IS NOT NULL THEN PO.dtValidFrom+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) ELSE PO.dtValidFrom END),v_numDomainID),' to ',FormatedDateFromDate((CASE WHEN PO.dtValidTo IS NOT NULL THEN PO.dtValidTo+CAST(-v_ClientTimeZoneOffset || 'minute' as interval) ELSE PO.dtValidTo END),v_numDomainID)) END)
      END) AS vcDateValidation
			,(CASE
      WHEN coalesce(PO.bitRequireCouponCode,false) = true OR coalesce(PO.numOrderPromotionID,0) > 0 THEN
         COALESCE((SELECT string_agg(vcValue,', ') FROM (SELECT CONCAT(vcDiscountCode,' (',coalesce(SUM(DCU.intCodeUsed),0),')') vcValue
            FROM DiscountCodes  D
            LEFT JOIN DiscountCodeUsage DCU ON D.numDiscountId = DCU.numDiscountId
            WHERE D.numPromotionID =(CASE WHEN coalesce(PO.numOrderPromotionID,0) > 0 THEN PO.numOrderPromotionID ELSE PO.numProId END) GROUP BY D.vcDiscountCode,DCU.numDiscountId) X),'')
      ELSE ''
      END) AS vcCouponCode
			,(CASE WHEN coalesce(PO.bitApplyToInternalOrders,false) = true THEN 'Yes' ELSE 'No' END) AS vcInternalOrders
			,(CASE WHEN coalesce(PO.bitAppliesToSite,false) = true THEN COALESCE((SELECT string_agg(Sites.vcSiteName,', ') FROM PromotionOfferSites POS INNER JOIN Sites ON POS.numSiteID = Sites.numSiteID WHERE POS.numPromotionID = PO.numProId),'') ELSE '' END) AS vcSites
			,CASE WHEN coalesce(PO.IsOrderBasedPromotion,false) = true THEN(CASE WHEN coalesce(PO.bitUseForCouponManagement,false) = false THEN CONCAT('Buy ',CASE PO.tintDiscountType WHEN 1 THEN
               COALESCE((SELECT string_agg(CONCAT('$',numOrderAmount,' & get ',coalesce(fltDiscountValue,0),' % off'),', ')
                  FROM PromotionOfferOrder POO
                  WHERE POO.numPromotionID =  PO.numProId),'')
            WHEN 2 THEN
               COALESCE((SELECT string_agg(CONCAT('$',numOrderAmount,' & get $',coalesce(fltDiscountValue,0),' off'),', ')
                  FROM PromotionOfferOrder POO
                  WHERE POO.numPromotionID =  PO.numProId),'')
            ELSE '' END) ELSE '' END)
      ELSE
         CONCAT('Buy ',CASE WHEN PO.tintOfferTriggerValueType = 1 THEN CAST(PO.fltOfferTriggerValue AS VARCHAR(30)) ELSE CONCAT('$',PO.fltOfferTriggerValue) END,
         CASE PO.tintOfferBasedOn
         WHEN 1 THEN CONCAT(' of "',COALESCE((SELECT string_agg(Item.vcItemName,', ') FROM PromotionOfferItems INNER JOIN Item ON PromotionOfferItems.numValue = Item.numItemCode WHERE numProId = PO.numProId AND tintRecordType = 5 AND tintType = 1),''),'"')
         WHEN 2 THEN CONCAT(' of items belonging to classification(s) "',COALESCE((SELECT string_agg(Listdetails.vcData,', ') FROM PromotionOfferItems INNER JOIN Listdetails ON PromotionOfferItems.numValue = Listdetails.numListItemID WHERE numListID = 36 AND numProId = PO.numProId AND tintRecordType = 5 AND tintType = 2),''), 
            '"')
         END,' & get ',CASE
         WHEN (PO.tintDiscountType = 1 AND PO.tintDiscoutBaseOn <> 4) THEN
            CONCAT(PO.fltDiscountValue,'% off on')
         WHEN (PO.tintDiscountType = 2 AND PO.tintDiscoutBaseOn <> 4) THEN
            CONCAT('$',PO.fltDiscountValue,' off on')
         WHEN (PO.tintDiscountType = 3 AND PO.tintDiscoutBaseOn <> 4) THEN
            CONCAT(PO.fltDiscountValue,' of') END,CASE
         WHEN PO.tintDiscoutBaseOn = 1 THEN
            CONCAT(CONCAT(' "',COALESCE((SELECT string_agg(Item.vcItemName,', ') FROM PromotionOfferItems INNER JOIN Item ON PromotionOfferItems.numValue = Item.numItemCode WHERE numProId = PO.numProId AND tintRecordType = 6 AND tintType = 1),''),'"'),(CASE WHEN PO.tintDiscountType = 3 THEN ' free' ELSE '' END),
            '.')
         WHEN PO.tintDiscoutBaseOn = 2 THEN
            CONCAT(CONCAT(' items belonging to classification(s) "',COALESCE((SELECT string_agg(Listdetails.vcData,', ') FROM PromotionOfferItems INNER JOIN Listdetails ON PromotionOfferItems.numValue = Listdetails.numListItemID WHERE numListID = 36 AND numProId = PO.numProId AND tintRecordType = 6 AND tintType = 2),''), 
            '"'),(CASE WHEN PO.tintDiscountType = 3 THEN ' free' ELSE '' END),
            '.')
         WHEN PO.tintDiscoutBaseOn = 3 THEN ' related items.'
         WHEN PO.tintDiscoutBaseOn = 4 THEN CONCAT(PO.fltDiscountValue,' quantity free for any item of equal or lesser value')
         WHEN PO.tintDiscoutBaseOn = 5 THEN CONCAT(PO.fltDiscountValue,' quantity for any item of equal or lesser value at $',
            coalesce(PO.monDiscountedItemPrice,0))
         WHEN PO.tintDiscoutBaseOn = 6 THEN CONCAT(CONCAT(' "',COALESCE((SELECT string_agg(Item.vcItemName,', ') FROM PromotionOfferItems INNER JOIN Item ON PromotionOfferItems.numValue = Item.numItemCode WHERE numProId = PO.numProId AND tintRecordType = 6 AND tintType = 4),''),'"'),(CASE WHEN PO.tintDiscountType = 3 THEN CONCAT(' at $',coalesce(PO.monDiscountedItemPrice,0)) ELSE '' END),
            '.')
         ELSE ''
         END)
      END AS vcPromotionRule,
				(CASE WHEN(SELECT COUNT(*) FROM OpportunityItems JOIN OpportunityMaster ON OpportunityItems.numOppId = OpportunityMaster.numOppId WHERE numDomainId = v_numDomainID AND OpportunityItems.numPromotionID = PO.numProId) > 0 THEN false ELSE true END) AS bitCanEdit
				,PO.bitEnabled
				, PO.tintCustomersBasedOn
				, PO.tintOfferTriggerValueTypeRange
				, PO.fltOfferTriggerValueRange
				, PO.IsOrderBasedPromotion
				, coalesce(PO.monDiscountedItemPrice,0) AS monDiscountedItemPrice
      FROM
      PromotionOffer PO
      LEFT JOIN
      PromotionOffer POOrder
      ON
      PO.numOrderPromotionID = POOrder.numProId
      WHERE
      PO.numDomainId = v_numDomainID
      ORDER BY
      PO.dtCreated DESC;
   end if;
   RETURN;
END; $$;      
  


