-- Stored procedure definition script usp_GetDefaultSurveyRating for PostgreSQL
CREATE OR REPLACE FUNCTION usp_GetDefaultSurveyRating(v_numDomainID NUMERIC,       
 v_numSurId NUMERIC,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT * FROM SurveyCreateRecord WHERE
   numSurID = v_numSurId AND coalesce(bitDefault,false) = true LIMIT 1;
END; $$;












