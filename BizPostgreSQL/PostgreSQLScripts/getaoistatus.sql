-- Function definition script GetAOIStatus for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION GetAOIStatus(v_numAOIID NUMERIC(9,0),v_numContactID NUMERIC(9,0))
RETURNS SMALLINT LANGUAGE plpgsql
   AS $$
   DECLARE
   v_Status  SMALLINT;
BEGIN
   select   tintSelected INTO v_Status from AOIContactLink where v_numAOIID = numAOIID and v_numContactID = numContactID;
   return v_Status;
END; $$;

