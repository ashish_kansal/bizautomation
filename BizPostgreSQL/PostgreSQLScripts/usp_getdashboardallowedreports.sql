CREATE OR REPLACE FUNCTION Usp_GetDashboardAllowedReports(v_numDomainId NUMERIC(9,0) ,
v_numGroupId NUMERIC(9,0),
v_tintMode SMALLINT DEFAULT 0, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
BEGIN
   IF v_tintMode = 0 then --All Custom Reports

      open SWV_RefCur for
      SELECT RLM.numReportID,RLM.vcReportName,RLM.vcReportDescription,RMM.vcModuleName,RMGM.vcGroupName,
	CASE WHEN coalesce(DAR.numReportID,0) = 0 THEN CAST('false' AS BOOLEAN) ELSE CAST('true' AS BOOLEAN) END AS bitallowed
      from ReportListMaster RLM JOIN ReportModuleMaster RMM ON RLM.numReportModuleID = RMM.numReportModuleID
      JOIN ReportModuleGroupMaster RMGM ON RLM.numReportModuleGroupID = RMGM.numReportModuleGroupID
      LEFT join ReportDashboardAllowedReports DAR on RLM.numReportID = DAR.numReportID AND DAR.numDomainID = v_numDomainId AND DAR.numGrpID =  v_numGroupId AND DAR.tintReportCategory = 0
      where RLM.numDomainID = v_numDomainId
      order by RLM.numReportID;
   ELSEIF v_tintMode = 1
   then --Only Allowed Custom Reports

      open SWV_RefCur for
      SELECT RLM.numReportID,RLM.vcReportName,RLM.vcReportDescription,RMM.vcModuleName,RMGM.vcGroupName,
	CASE WHEN coalesce(DAR.numReportID,0) = 0 THEN CAST('false' AS BOOLEAN) ELSE CAST('true' AS BOOLEAN) END AS bitallowed
      from ReportListMaster RLM JOIN ReportModuleMaster RMM ON RLM.numReportModuleID = RMM.numReportModuleID
      JOIN ReportModuleGroupMaster RMGM ON RLM.numReportModuleGroupID = RMGM.numReportModuleGroupID
      join ReportDashboardAllowedReports DAR on RLM.numReportID = DAR.numReportID
      where RLM.numDomainID = v_numDomainId AND DAR.numDomainID = v_numDomainId AND DAR.numGrpID =  v_numGroupId AND DAR.tintReportCategory = 0
      order by RLM.numReportID;
   ELSEIF v_tintMode = 2
   then --All KPI Groups Reports

      open SWV_RefCur for
      SELECT KPI.numReportKPIGroupID,KPI.vcKPIGroupName,KPI.vcKPIGroupDescription,
	CASE KPI.tintKPIGroupReportType WHEN 3 THEN 'KPI' WHEN 4 THEN 'ScoreCard' END AS vcKPIGroupReportType,
	CASE WHEN coalesce(DAR.numReportID,0) = 0 THEN CAST('false' AS BOOLEAN) ELSE CAST('true' AS BOOLEAN) END AS bitallowed
      from ReportKPIGroupListMaster KPI
      LEFT join ReportDashboardAllowedReports DAR on KPI.numReportKPIGroupID = DAR.numReportID AND DAR.numDomainID = v_numDomainId AND DAR.numGrpID =  v_numGroupId AND DAR.tintReportCategory = 1
      where KPI.numDomainID = v_numDomainId
      order by KPI.numReportKPIGroupID;
   ELSEIF v_tintMode = 3
   then --Only Allowed KPI Groups Reports

      open SWV_RefCur for
      SELECT KPI.numReportKPIGroupID,KPI.vcKPIGroupName,KPI.vcKPIGroupDescription,
	CASE KPI.tintKPIGroupReportType WHEN 3 THEN 'KPI' WHEN 4 THEN 'ScoreCard' END AS vcKPIGroupReportType,
	CASE WHEN coalesce(DAR.numReportID,0) = 0 THEN CAST('false' AS BOOLEAN) ELSE CAST('true' AS BOOLEAN) END AS bitallowed
      from ReportKPIGroupListMaster KPI
      join ReportDashboardAllowedReports DAR on KPI.numReportKPIGroupID = DAR.numReportID
      where KPI.numDomainID = v_numDomainId AND DAR.numDomainID = v_numDomainId AND DAR.numGrpID =  v_numGroupId AND DAR.tintReportCategory = 1
      order by KPI.numReportKPIGroupID;
   ELSEIF v_tintMode = 4
   then --Default Reports

      open SWV_RefCur for
      SELECT
      RLM.numReportID
		,RLM.vcReportName
		,RLM.intDefaultReportID
      FROM
      ReportListMaster RLM
      INNER JOIN
      ReportDashboardAllowedReports DAR
      ON
      RLM.intDefaultReportID = DAR.numReportID
      AND DAR.numDomainID = v_numDomainId
      AND DAR.numGrpID =  v_numGroupId
      AND DAR.tintReportCategory = 2
      WHERE
      coalesce(RLM.bitDefault,false) = true
      ORDER BY
      RLM.vcReportName;
   ELSEIF v_tintMode = 5
   then -- Default Reports With Permission

      open SWV_RefCur for
      SELECT
      RLM.intDefaultReportID
		,RLM.vcReportName
		,RLM.vcReportDescription
		,CASE WHEN coalesce(DAR.numReportID,0) = 0 THEN CAST('false' AS BOOLEAN) ELSE CAST('true' AS BOOLEAN) END AS bitallowed
      FROM
      ReportListMaster RLM
      LEFT JOIN
      ReportDashboardAllowedReports DAR
      ON
      RLM.intDefaultReportID = DAR.numReportID
      AND DAR.numDomainID = v_numDomainId
      AND DAR.numGrpID =  v_numGroupId
      AND DAR.tintReportCategory = 2
      WHERE
      coalesce(RLM.numDomainID,0) = 0
      AND coalesce(RLM.bitDefault,false) = true
      ORDER BY
      RLM.vcReportName;
   end if;
   RETURN;
END; $$;   


