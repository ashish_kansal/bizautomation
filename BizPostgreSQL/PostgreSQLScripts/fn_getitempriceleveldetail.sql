-- Function definition script fn_GetItemPriceLevelDetail for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION fn_GetItemPriceLevelDetail(v_numDomainID NUMERIC(18,0)
	,v_numItemCode NUMERIC(18,0)
	,v_numWarehouseID NUMERIC(18,0))
RETURNS TEXT LANGUAGE plpgsql
   AS $$
   DECLARE
   v_vcValue  TEXT DEFAULT '';
   
   v_numWareHouseItemID  BIGINT;
   v_monListPrice  DECIMAL(20,5) DEFAULT 0;
   v_monVendorCost  DECIMAL(20,5) DEFAULT 0;
   v_listPriceTable  TEXT;

   v_listPriceRule  TEXT;
BEGIN
   select   coalesce(numWareHouseItemID,0) INTO v_numWareHouseItemID FROM WareHouseItems WHERE numWareHouseID = v_numWarehouseID AND numItemID = v_numItemCode AND coalesce(numWLocationID,0) <> -1    LIMIT 1;

   IF v_numWareHouseItemID > 0 then
	
      select CONCAT('<b>Warehouse Price</b><br/>',coalesce(W.vcWareHouse,''),' - ',coalesce(WI.monWListPrice,0)) INTO v_vcValue FROM
      Item I
      LEFT JOIN
      WareHouseItems WI
      ON
      WI.numItemID = I.numItemCode AND WI.numDomainID = I.numDomainID
      LEFT JOIN
      Warehouses W
      ON
      W.numWareHouseID = WI.numWareHouseID WHERE
      coalesce(I.numItemCode,0) = v_numItemCode
      AND (W.numWareHouseID = coalesce(v_numWarehouseID,0) OR coalesce(v_numWarehouseID,0) = 0)
      AND coalesce(numWLocationID,0) <> -1;
   end if;

   IF v_numWareHouseItemID > 0 then
	
      select   coalesce(monWListPrice,0) INTO v_monListPrice FROM WareHouseItems WHERE numWareHouseItemID = v_numWareHouseItemID;
   ELSE
      select   monListPrice INTO v_monListPrice FROM Item WHERE numItemCode = v_numItemCode;
   end if; 

   select COALESCE(monCost,0) INTO v_monVendorCost from Vendor V inner join Item I on v.numVendorID=I.numVendorID and 
V.numItemCode=I.numItemCode 
where v.numItemCode=v_numItemCode;

   v_monVendorCost := fn_GetVendorCost(v_numItemCode);


	-- PRICE TABLE
   IF EXISTS(SELECT * FROM PricingTable WHERE numItemCode = v_numItemCode AND coalesce(numCurrencyID,0) = 0) then
      select  string_agg(
      CONCAT(CAST(CASE WHEN tintRuleType = 1 AND tintDiscountType = 1 --Deduct from List price & Percentage
      THEN v_monListPrice -(v_monListPrice*(decDiscount/100))
      WHEN tintRuleType = 1 AND tintDiscountType = 2 --Deduct from List price & Flat discount
      THEN v_monListPrice -decDiscount
      WHEN tintRuleType = 2 AND tintDiscountType = 1 --Add to Primary Vendor Cost & Percentage
      THEN v_monVendorCost+(v_monVendorCost*(decDiscount/100))
      WHEN tintRuleType = 2 AND tintDiscountType = 2 --Add to Primary Vendor Cost & Flat discount
      THEN v_monVendorCost+decDiscount
      WHEN tintRuleType = 3 --Named Price
      THEN decDiscount
      END AS DECIMAL(20,5)),' (',intFromQty,'-',intToQty,')'),', ' ORDER BY numPricingID) INTO v_listPriceTable FROM
      PricingTable PT WHERE
      coalesce(PT.numItemCode,0) = v_numItemCode
      AND coalesce(numCurrencyID,0) = 0 ;
      IF LENGTH(v_listPriceTable) > 0 then
		
         v_vcValue := CONCAT(v_vcValue,'<br/><b>Price Table</b><br/>',v_listPriceTable);
      end if;
   end if;

   IF EXISTS(SELECT * FROM
   Item I
   JOIN PriceBookRules P ON I.numDomainID = P.numDomainID
   LEFT JOIN PriceBookRuleDTL PDTL ON P.numPricRuleID = PDTL.numRuleID
   LEFT JOIN PriceBookRuleItems PBI ON P.numPricRuleID = PBI.numRuleID
   LEFT JOIN PriceBookPriorities PP ON PP.Step2Value = P.tintStep2 AND PP.Step3Value = P.tintStep3
   JOIN PricingTable PT ON P.numPricRuleID = PT.numPriceRuleID AND coalesce(PT.numCurrencyID,0) = 0
   WHERE
   I.numItemCode = v_numItemCode
   AND P.tintRuleFor = 1
   AND P.tintPricingMethod = 1
   AND
			(
				((tintStep2 = 1 AND PBI.numValue = v_numItemCode) AND tintStep3 = 3) -- Priority 7
   OR ((tintStep2 = 2 AND PBI.numValue = I.numItemClassification) AND tintStep3 = 3) -- Priority 8
   OR (tintStep2 = 3 and tintStep3 = 3) -- Priority 9
			)) then
      select string_agg(  COALESCE(coalesce(v_listPriceRule,'') || ', ','') ||
      CONCAT(CAST(CASE WHEN PT.tintRuleType = 1 AND PT.tintDiscountType = 1 --Deduct from List price & Percentage
      THEN v_monListPrice -(v_monListPrice*(PT.decDiscount/100))
      WHEN PT.tintRuleType = 1 AND PT.tintDiscountType = 2 --Deduct from List price & Flat discount
      THEN v_monListPrice -PT.decDiscount
      WHEN PT.tintRuleType = 2 AND PT.tintDiscountType = 1 --Add to Primary Vendor Cost & Percentage
      THEN v_monVendorCost+(v_monVendorCost*(PT.decDiscount/100))
      WHEN PT.tintRuleType = 2 AND PT.tintDiscountType = 2 --Add to Primary Vendor Cost & Flat discount
      THEN v_monVendorCost+PT.decDiscount END AS DECIMAL(20,5)),' (',PT.intFromQty,'-',PT.intToQty,')'),', ' ORDER BY PP.Priority ASC) INTO v_listPriceRule FROM
      Item I
      JOIN PriceBookRules P ON I.numDomainID = P.numDomainID
      LEFT JOIN PriceBookRuleDTL PDTL ON P.numPricRuleID = PDTL.numRuleID
      LEFT JOIN PriceBookRuleItems PBI ON P.numPricRuleID = PBI.numRuleID
      LEFT JOIN PriceBookPriorities PP ON PP.Step2Value = P.tintStep2 AND PP.Step3Value = P.tintStep3
      LEFT JOIN PricingTable PT ON P.numPricRuleID = PT.numPriceRuleID AND coalesce(PT.numCurrencyID,0) = 0 WHERE
      I.numItemCode = v_numItemCode
      AND P.tintRuleFor = 1
      AND P.tintPricingMethod = 1
      AND
			(
				((tintStep2 = 1 AND PBI.numValue = v_numItemCode) AND tintStep3 = 3) -- Priority 7
      OR ((tintStep2 = 2 AND PBI.numValue = I.numItemClassification) AND tintStep3 = 3) -- Priority 8
      OR (tintStep2 = 3 and tintStep3 = 3) -- Priority 9
			);
      IF LENGTH(v_listPriceRule) > 0 then
		
         v_vcValue := CONCAT(v_vcValue,'<br/><b>Price Rule</b><br/>',v_listPriceRule);
      end if;
   end if;

   RETURN v_vcValue;
END; $$;

