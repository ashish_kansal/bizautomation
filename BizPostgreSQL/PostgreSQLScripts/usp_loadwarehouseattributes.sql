-- Stored procedure definition script USP_LoadWarehouseAttributes for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_LoadWarehouseAttributes(v_numItemCode VARCHAR(20) DEFAULT '',  
v_strAtrr VARCHAR(1000) DEFAULT '', INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_bitSerialize  BOOLEAN;  
   v_strSQL  VARCHAR(1000);
   v_Cnt  INTEGER;
   v_SplitOn  CHAR(1);
BEGIN
   select   bitSerialized INTO v_bitSerialize from Item where numItemCode = v_numItemCode;  
  
   if v_strAtrr != '' then
      v_Cnt := 1;
      v_SplitOn := ',';
      v_strSQL := 'SELECT recid  
      FROM CFW_Fld_Values_Serialized_Items where  bitSerialized =' || SUBSTR(CAST(v_bitSerialize AS VARCHAR(1)),1,1);
      While (POSITION(v_SplitOn IN v_strAtrr) > 0) LOOP
         if  v_Cnt = 1 then 
            v_strSQL := coalesce(v_strSQL,'') || ' and fld_value = ''' ||(ltrim(rtrim(SUBSTR(v_strAtrr,1,POSITION(v_SplitOn IN v_strAtrr) -1)))) || '''';
         else 
            v_strSQL := coalesce(v_strSQL,'') || ' or fld_value = ''' ||(ltrim(rtrim(SUBSTR(v_strAtrr,1,POSITION(v_SplitOn IN v_strAtrr) -1)))) || '''';
         end if;
         v_strAtrr := SUBSTR(v_strAtrr,POSITION(v_SplitOn IN v_strAtrr)+1,LENGTH(v_strAtrr));
         v_Cnt := v_Cnt::bigint+1;
      END LOOP;
      v_strSQL := coalesce(v_strSQL,'') || ' or fld_value = ''' || coalesce(v_strAtrr,'') || ''' GROUP BY recid  
    HAVING count(*) > ' || CAST(v_Cnt::bigint -1 AS VARCHAR(10));
   end if;  
  
   if v_bitSerialize = false then

      if v_strAtrr <> '' then
         v_strSQL := 'select distinct(WareHouseItems.numWareHouseItemId),vcWareHouse,numOnHand,numOnOrder,numReorder,numAllocation,numBackOrder from WareHouseItems  
   join Warehouses on Warehouses.numWareHouseID = WareHouseItems.numWareHouseID  
   where numItemID =' || coalesce(v_numItemCode,'') || ' and numWareHouseItemId in(' || coalesce(v_strSQL,'') || ')';
      ELSEIF v_strAtrr = ''
      then
         v_strSQL := 'select distinct(WareHouseItems.numWareHouseItemId),vcWareHouse,numOnHand,numOnOrder,numReorder,numAllocation,numBackOrder from WareHouseItems  
   join Warehouses on Warehouses.numWareHouseID = WareHouseItems.numWareHouseID  
   where numItemID =' || coalesce(v_numItemCode,'');
      end if;
      OPEN SWV_RefCur FOR EXECUTE v_strSQL;
   ELSEIF v_bitSerialize = true
   then

      if v_strAtrr <> '' then
         v_strSQL := 'select distinct(WareHouseItems.numWareHouseItemId),vcWareHouse,numOnHand,numOnOrder,numReorder,numAllocation,numBackOrder from WareHouseItems   
  join WareHouseItmsDTL on WareHouseItmsDTL.numWareHouseItemID = WareHouseItems.numWareHouseItemId  
  join Warehouses on Warehouses.numWareHouseID = WareHouseItems.numWareHouseID  
  where numItemID =' || coalesce(v_numItemCode,'') || ' and numWareHouseItmsDTLID in(' || coalesce(v_strSQL,'') || ')';
      ELSEIF v_strAtrr = ''
      then
         v_strSQL := 'select distinct(WareHouseItems.numWareHouseItemId),vcWareHouse,numOnHand,numOnOrder,numReorder,numAllocation,numBackOrder from WareHouseItems   
  join WareHouseItmsDTL on WareHouseItmsDTL.numWareHouseItemID = WareHouseItems.numWareHouseItemId  
  join Warehouses on Warehouses.numWareHouseID = WareHouseItems.numWareHouseID  
  where numItemID =' || coalesce(v_numItemCode,'');
      end if;
      OPEN SWV_RefCur FOR EXECUTE v_strSQL;
   else 
      open SWV_RefCur for
      select 0;
   end if;  
  
   RAISE NOTICE '%',v_strSQL;
   RETURN;
END; $$;


