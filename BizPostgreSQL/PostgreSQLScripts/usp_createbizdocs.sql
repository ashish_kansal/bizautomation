DROP FUNCTION IF EXISTS USP_CreateBizDocs;

CREATE OR REPLACE FUNCTION USP_CreateBizDocs(v_numOppId NUMERIC(9,0) DEFAULT null,                        
 v_numBizDocId NUMERIC(9,0) DEFAULT null,                        
 v_numUserCntID NUMERIC(9,0) DEFAULT null,                        
 INOUT v_numOppBizDocsId NUMERIC(9,0) DEFAULT NULL ,
 v_vcComments VARCHAR(1000) DEFAULT NULL,
 v_bitPartialFulfillment BOOLEAN DEFAULT NULL,
 v_strBizDocItems TEXT DEFAULT '' ,
 v_numShipVia NUMERIC(9,0) DEFAULT NULL,
 v_vcTrackingURL VARCHAR(1000) DEFAULT NULL,
 v_dtFromDate TIMESTAMP DEFAULT NULL,
 v_numBizDocStatus NUMERIC(9,0) DEFAULT 0,
 v_bitRecurringBizDoc BOOLEAN DEFAULT NULL,
 v_numSequenceId VARCHAR(50) DEFAULT '',
 v_tintDeferred SMALLINT DEFAULT 0,
 INOUT v_monCreditAmount DECIMAL(20,5) DEFAULT 0 ,
 v_ClientTimeZoneOffset INTEGER DEFAULT 0,
 INOUT v_monDealAmount DECIMAL(20,5) DEFAULT 0 ,
 v_bitRentalBizDoc BOOLEAN DEFAULT false,
 v_numBizDocTempID NUMERIC(9,0) DEFAULT 0,
 v_vcTrackingNo VARCHAR(500) DEFAULT NULL,
 v_vcRefOrderNo VARCHAR(100) DEFAULT NULL,
 v_OMP_SalesTaxPercent DECIMAL DEFAULT 0, --(joseph) Added to Get Online Marketplace Sales Tax Percentage
 v_numFromOppBizDocsId NUMERIC(9,0) DEFAULT 0,
 v_bitTakeSequenceId BOOLEAN DEFAULT false,
 v_bitAllItems BOOLEAN DEFAULT false,
 v_bitNotValidateBizDocFulfillment BOOLEAN DEFAULT false,
 v_fltExchangeRateBizDoc DECIMAL DEFAULT 0,
 v_bitRecur BOOLEAN DEFAULT false,
 v_dtStartDate DATE DEFAULT NULL,
 v_dtEndDate DATE DEFAULT NULL,
 v_numFrequency SMALLINT DEFAULT 0,
 v_vcFrequency VARCHAR(20) DEFAULT '',
 v_numRecConfigID NUMERIC(18,0) DEFAULT 0,
 v_bitDisable BOOLEAN DEFAULT false,
 v_bitInvoiceForDeferred BOOLEAN DEFAULT false,
 v_numSourceBizDocId NUMERIC(18,0) DEFAULT 0,
 v_bitShippingBizDoc BOOLEAN DEFAULT false,
 v_vcVendorInvoiceName  VARCHAR(100) DEFAULT NULL,
 v_numARAccountID NUMERIC(18,0) DEFAULT 0)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_numDivisionID  NUMERIC(9,0);
   v_numDomainID  NUMERIC(9,0);
   v_tintOppType  SMALLINT;	
   v_numFulfillmentOrderBizDocId  NUMERIC(9,0);
   v_bitShippingGenerated  BOOLEAN DEFAULT 0;

   v_tintCommitAllocation  SMALLINT;
   v_bitAllocateInventoryOnPickList  BOOLEAN;

  my_ex_state TEXT;
	my_ex_message TEXT;
	my_ex_detail TEXT;
	my_ex_hint TEXT;
	my_ex_ctx TEXT;
   v_tintShipped  SMALLINT;	
   v_dtShipped  TIMESTAMP;
   v_bitAuthBizdoc  BOOLEAN;
   v_dtCreatedDate  TIMESTAMP;
   SWV_numSequenceId BIGINT;
   SWV_RCur refcursor;
   v_USP_RecurringAddOpportunity_ValidationMessage VARCHAR(200);
   v_numTop1DeferredBizDocID  NUMERIC(18,0);
BEGIN
	IF coalesce(v_numFromOppBizDocsId,0) > 0 then
	
         IF(SELECT
         COUNT(*)
         FROM(SELECT
            OBDI.numOppItemID
					,OI.numUnitHour AS OrderedQty
					,OBDI.numUnitHour AS FromBizDocQty
					,OtherSameBizDocType.BizDocQty
            FROM
            OpportunityBizDocs OBD
            INNER JOIN
            OpportunityBizDocItems OBDI
            ON
            OBD.numOppBizDocsId = OBDI.numOppBizDocID
            INNER JOIN
            OpportunityItems OI
            ON
            OI.numoppitemtCode = OBDI.numOppItemID
            LEFT JOIN LATERAL(SELECT
               SUM(OpportunityBizDocItems.numUnitHour) AS BizDocQty
               FROM
               OpportunityBizDocs
               INNER JOIN
               OpportunityBizDocItems
               ON
               OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
               WHERE
               OpportunityBizDocs.numoppid = OBD.numOppId
               AND OpportunityBizDocItems.numOppItemID = OBDI.numOppItemID
               AND OpportunityBizDocs.numBizDocId = v_numBizDocId) AS OtherSameBizDocType on TRUE
            WHERE
            OBD.numOppBizDocsId = v_numFromOppBizDocsId) X
         WHERE
         coalesce(X.FromBizDocQty,0) >(coalesce(X.OrderedQty,0) -coalesce(X.BizDocQty,0))) > 0 then
			RETURN;
			-- IF SOMEONE HAS ALREADY CREATED OTHE SAME BIZDOC TYPE AND ENOUGHT QTY IS NOT LEFT FOR ALL ITEMS OF BIZDOC THAN BIZDOC CAN NOT BE CREATED AGAINST VENDOR INVOICE
            --RAISE EXCEPTION 'BIZDOC_ALREADY_GENERATED_AGAINST_SOURCE';
         end if;
      end if;

   BEGIN
      IF coalesce(v_numSequenceId,'') = '' then
	
         v_bitTakeSequenceId := true;
      end if;
      select   numDomainId, numDivisionId, tintopptype INTO v_numDomainID,v_numDivisionID,v_tintOppType FROM
      OpportunityMaster WHERE
      numOppId = v_numOppId;
      select   coalesce(tintCommitAllocation,1) INTO v_tintCommitAllocation FROM Domain WHERE numDomainId = v_numDomainID;
      select   coalesce(bitAllocateInventoryOnPickList,false) INTO v_bitAllocateInventoryOnPickList FROM DivisionMaster WHERE numDivisionID = v_numDivisionID;
      
      v_numFulfillmentOrderBizDocId := 296;
      IF coalesce(v_fltExchangeRateBizDoc,0) = 0 then
         select   fltExchangeRate INTO v_fltExchangeRateBizDoc FROM OpportunityMaster where numOppId = v_numOppId;
      end if;
      IF(v_bitShippingBizDoc = true) then
	
         v_bitShippingGenerated := true;
      ELSE
         IF((SELECT COUNT(numBizDocId) FROM OpportunityBizDocs WHERE bitShippingGenerated = true AND numoppid = v_numOppId) = 0) then
	
            IF((SELECT  CAST(numDefaultSalesShippingDoc AS INTEGER) FROM Domain WHERE numDomainId = v_numDomainID LIMIT 1) = v_numBizDocId) then
		
               v_bitShippingGenerated := true;
            end if;
         end if;
      end if;
      IF v_numBizDocTempID = 0 then
	
         select   numBizDocTempID INTO v_numBizDocTempID FROM
         BizDocTemplate WHERE
         numBizDocID = v_numBizDocId
         AND numDomainID = v_numDomainID and numOppType = v_tintOppType
         AND tintTemplateType = 0
         AND coalesce(bitDefault,false) = true
         AND coalesce(bitEnabled,false) = true;
      end if;
      IF v_numOppBizDocsId > 0 AND v_numBizDocId = 29397 AND v_tintCommitAllocation = 2 AND v_bitAllocateInventoryOnPickList = true then
	
		-- Revert Allocation
         PERFORM USP_RevertAllocationPickList(v_numDomainID,v_numOppId,v_numOppBizDocsId,v_numUserCntID);
      end if;
      IF v_numOppBizDocsId = 0 AND v_numBizDocId = 296 AND v_tintCommitAllocation = 2 AND v_bitAllocateInventoryOnPickList = true then
	
		-- Revert Allocation
         IF coalesce(v_numFromOppBizDocsId,0) = 0 AND coalesce(v_numSourceBizDocId,0) = 0 then
		
            RAISE EXCEPTION 'FULFILLMENT_BIZDOC_REQUIRED_PICKLIST_FOR_ALLOCATE_ON_PICKLIST_SETTING';
         ELSEIF coalesce(v_numFromOppBizDocsId,0) > 0 AND coalesce(v_numSourceBizDocId,0) = 0
         then
		
            v_numSourceBizDocId := v_numFromOppBizDocsId;
         end if;
      end if;
      IF v_numOppBizDocsId = 0 then
	
         IF (
			(v_bitPartialFulfillment = true AND NOT EXISTS(SELECT * FROM OpportunityBizDocs WHERE numoppid = v_numOppId AND numBizDocId = v_numBizDocId AND bitPartialFulfilment = false))
         OR (NOT EXISTS(SELECT * FROM OpportunityBizDocs WHERE numoppid = v_numOppId AND numBizDocId = v_numBizDocId AND bitPartialFulfilment = false)
         AND NOT EXISTS(SELECT * FROM OpportunityBizDocs WHERE numoppid = v_numOppId AND numBizDocId = v_numBizDocId AND bitPartialFulfilment = true))
         OR (v_bitRecurringBizDoc = true) -- Added new flag to add unit qty in case of recurring bizdoc
         OR (v_numFromOppBizDocsId > 0)) then
		
			--select @numOppBizDocsId
            select   coalesce(tintshipped,0), numDomainId INTO v_tintShipped,v_numDomainID FROM
            OpportunityMaster WHERE numOppId = v_numOppId;
            IF v_tintOppType = 1 AND v_bitNotValidateBizDocFulfillment = false then
               PERFORM USP_ValidateBizDocFulfillment(v_numDomainID,v_numOppId,v_numFromOppBizDocsId,v_numBizDocId);
            end if;
            IF v_tintShipped = 1 then
               v_dtShipped := TIMEZONE('UTC',now());
            ELSE
               v_dtShipped := null;
            end if;
            IF v_tintOppType = 1 AND(SELECT numAuthoritativeSales FROM AuthoritativeBizDocs WHERE numDomainId = v_numDomainID) = v_numBizDocId then
               v_bitAuthBizdoc := true;
            ELSEIF v_tintOppType = 2 AND(SELECT numAuthoritativePurchase FROM AuthoritativeBizDocs WHERE numDomainId = v_numDomainID) = v_numBizDocId
            then
               v_bitAuthBizdoc := true;
            ELSE
               v_bitAuthBizdoc := false;
            end if;
            v_dtCreatedDate := TIMEZONE('UTC',now());
            IF v_tintDeferred = 1 then
			
               v_dtCreatedDate := v_dtFromDate+CAST(v_ClientTimeZoneOffset || 'minute' as interval);
            end if;
            IF v_bitTakeSequenceId = true then
			
				--Sequence #
               DROP TABLE IF EXISTS tt_TEMPSEQUENCE CASCADE;
               CREATE TEMPORARY TABLE tt_TEMPSEQUENCE 
               (
                  numSequenceId BIGINT 
               );
               SELECT USP_GetScalerValue(v_numDomainID,33::SMALLINT,v_numBizDocId::VARCHAR,0,'SWV_RCur') INTO SWV_RCur;
               FETCH SWV_RCur INTO SWV_numSequenceId;
               WHILE FOUND LOOP
                  INSERT INTO tt_TEMPSEQUENCE  VALUES(SWV_numSequenceId);

                  FETCH SWV_RCur INTO SWV_numSequenceId;
               END LOOP;
               CLOSE SWV_RCur;
               select   coalesce(numSequenceId,0) INTO v_numSequenceId FROM tt_TEMPSEQUENCE;
               DROP TABLE IF EXISTS tt_TEMPSEQUENCE CASCADE;
            end if;	
			
			--To insert Tracking Id of previous record into the new record
            IF v_vcTrackingNo IS NULL OR v_vcTrackingNo = '' then
			
               select   vcTrackingNo INTO v_vcTrackingNo FROM OpportunityBizDocs WHERE numoppid = v_numOppId   ORDER BY numOppBizDocsId DESC LIMIT 1;
            end if;
            INSERT INTO OpportunityBizDocs(numoppid
				,numBizDocId
				,numCreatedBy
				,dtCreatedDate
				,numModifiedBy
				,dtModifiedDate
				,vcComments
				,bitPartialFulfilment
				,numShipVia
				,vcTrackingURL
				,dtFromDate
				,numBizDocStatus
				,dtShippedDate
				,bitAuthoritativeBizDocs
				,tintDeferred
				,bitRentalBizDoc
				,numBizDocTempID
				,vcTrackingNo
				,vcRefOrderNo
				,numSequenceId
				,numBizDocStatusOLD
				,fltExchangeRateBizDoc
				,bitAutoCreated
				,numMasterBizdocSequenceID
				,bitShippingGenerated
				,numSourceBizDocId
				,vcVendorInvoice
				,numARAccountID)
			VALUES(v_numOppId
				,v_numBizDocId
				,v_numUserCntID
				,v_dtCreatedDate
				,v_numUserCntID
				,TIMEZONE('UTC',now())
				,v_vcComments
				,v_bitPartialFulfillment
				,v_numShipVia
				,v_vcTrackingURL
				,v_dtFromDate
				,v_numBizDocStatus
				,v_dtShipped
				,CAST(coalesce(v_bitAuthBizdoc, false) AS INTEGER)
				,v_tintDeferred
				,v_bitRentalBizDoc
				,v_numBizDocTempID
				,v_vcTrackingNo
				,v_vcRefOrderNo
				,v_numSequenceId
				,0
				,v_fltExchangeRateBizDoc
				,false
				,v_numSequenceId
				,v_bitShippingGenerated
				,v_numSourceBizDocId
				,v_vcVendorInvoiceName
				,v_numARAccountID);
			
            v_numOppBizDocsId := CURRVAL('OpportunityBizDocs_seq');
		
			--Added By:Sachin Sadhu||Date:27thAug2014
			--Purpose :MAke entry in WFA queue 
            IF EXISTS(SELECT 1 FROM OpportunityMaster WHERE numOppId = v_numOppId AND tintopptype = 1 AND coalesce(tintoppstatus,0) = 1) then
			
				 -- numeric(18, 0)							
				 -- numeric(18, 0)							
				 -- numeric(18, 0)							
				 -- numeric(18, 0)							
				 -- numeric(18, 0)					
				 -- numeric(18, 0)					
				 -- tinyint						
               PERFORM USP_ManageOpportunityAutomationQueue(v_numOppQueueID := 0,v_numDomainID := v_numDomainID,v_numOppId := v_numOppId,
               v_numOppBizDocsId := v_numOppBizDocsId,v_numOrderStatus := 0,v_numUserCntID := v_numUserCntID,
               v_tintProcessStatus := 1::SMALLINT,v_tintMode := 1::SMALLINT);
            end if; 
		   --end of script

			--Deferred BizDocs : Create Recurring entry only for create Account Journal
            if v_tintDeferred = 1 then
			
               v_USP_RecurringAddOpportunity_ValidationMessage := '';
               PERFORM USP_RecurringAddOpportunity(v_numOppId,v_numOppBizDocsId,0,4::SMALLINT,v_dtCreatedDate,false,v_numDomainID,
               false,0,100,v_USP_RecurringAddOpportunity_ValidationMessage);
            end if;

			-- Update name template set for bizdoc
			--DECLARE @tintType TINYINT
			--SELECT @tintType = CASE @tintOppType WHEN 1 THEN 3 WHEN 2 THEN 4 END;
            PERFORM USP_UpdateBizDocNameTemplate(v_tintOppType::SMALLINT,v_numDomainID,v_numOppBizDocsId);
            IF v_numFromOppBizDocsId > 0 then
			
               INSERT INTO OpportunityBizDocItems(numOppBizDocID
					,numOppItemID
					,numItemCode
					,numUnitHour
					,monPrice
					,monTotAmount
					,vcItemDesc
					,numWarehouseItmsID
					,vcType
					,vcAttributes
					,bitDropShip
					,bitDiscountType
					,fltDiscount
					,monTotAmtBefDiscount
					,vcNotes
					,bitEmbeddedCost
					,monEmbeddedCost
					,dtRentalStartDate
					,dtRentalReturnDate
					,tintTrackingStatus)
               SELECT
               v_numOppBizDocsId
					,OBDI.numOppItemID
					,OBDI.numItemCode
					,OBDI.numUnitHour - COALESCE(TEMPAddedBizDocs.numUnitHour,0)
					,OBDI.monPrice
					,OBDI.monTotAmount
					,OBDI.vcItemDesc
					,OBDI.numWarehouseItmsID
					,OBDI.vcType
					,OBDI.vcAttributes
					,OBDI.bitDropShip
					,OBDI.bitDiscountType
					,OBDI.fltDiscount
					,OBDI.monTotAmtBefDiscount
					,OBDI.vcNotes
					,OBDI.bitEmbeddedCost
					,OBDI.monEmbeddedCost
					,OBDI.dtRentalStartDate
					,OBDI.dtRentalReturnDate
					,OBDI.tintTrackingStatus
               FROM
               OpportunityBizDocs OBD
               JOIN OpportunityBizDocItems OBDI ON OBD.numOppBizDocsId = OBDI.numOppBizDocID
               JOIN OpportunityItems OI ON OBDI.numOppItemID = OI.numoppitemtCode
               JOIN Item I ON OBDI.numItemCode = I.numItemCode
			   LEFT JOIN LATERAL
			   (
					SELECT
						SUM(OBDIInner.numUnitHour) numUnitHour
					FROM
						OpportunityBizDocs OBDInner
					JOIN 
						OpportunityBizDocItems OBDIInner 
					ON 
						OBDInner.numOppBizDocsId = OBDIInner.numOppBizDocID
						AND OBDIInner.numOppItemID = OBDI.numOppItemID
					WHERE
						OBDInner.numoppid = v_numOppId
						AND OBDInner.numBizDocID=v_numBizDocId
			   ) TEMPAddedBizDocs ON TRUE
               WHERE
               OBD.numoppid = v_numOppId
               AND OBD.numOppBizDocsId = v_numFromOppBizDocsId
			  AND OBDI.numUnitHour - COALESCE(TEMPAddedBizDocs.numUnitHour,0) > 0;
            ELSEIF (v_bitPartialFulfillment = true and OCTET_LENGTH(v_strBizDocItems) > 2)
            then
			
               IF OCTET_LENGTH(v_strBizDocItems) > 2 then
				
                  IF v_bitRentalBizDoc = true then
						
                     update OpportunityItems OI set monPrice = OBZ.monPrice,monTotAmount = OBZ.monTotAmount,monTotAmtBefDiscount = OBZ.monTotAmount
                     from
					 XMLTABLE
					(
						'NewDataSet/BizDocItems'
						PASSING 
							CAST(v_strBizDocItems AS XML)
						COLUMNS
							OppItemID NUMERIC(9,0) PATH 'OppItemID'
							,Quantity DOUBLE PRECISION PATH 'Quantity'
							,monPrice DECIMAL(20,5) PATH 'monPrice'
							,monTotAmount DECIMAL(20,5) PATH 'monTotAmount'
					) AS OBZ 
                     where OBZ.OppItemID = OI.numoppitemtCode AND OBZ.OppItemID = OI.numoppitemtCode;
                  end if;
                  insert into
                  OpportunityBizDocItems(numOppBizDocID,numOppItemID,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,vcType,vcAttributes,bitDropShip,bitDiscountType,fltDiscount,monTotAmtBefDiscount,vcNotes,/*vcTrackingNo,vcShippingMethod,monShipCost,dtDeliveryDate,*/dtRentalStartDate,dtRentalReturnDate)
                  select v_numOppBizDocsId,numoppitemtCode,numItemCode,Quantity,CASE WHEN v_tintOppType = 2 THEN OBZ.monPrice ELSE OI.monPrice END,CASE WHEN v_tintOppType = 2 THEN OBZ.monPrice*Quantity ELSE(monTotAmount/numUnitHour)*Quantity END,vcItemDesc,numWarehouseItmsID,vcType,vcAttributes,bitDropShip,bitDiscountType,
					   case when bitDiscountType = false then fltDiscount when bitDiscountType = true then(fltDiscount/numUnitHour)*Quantity else fltDiscount end ,CASE WHEN v_tintOppType = 2 THEN OBZ.monPrice*Quantity ELSE(monTotAmtBefDiscount/numUnitHour)*Quantity END,Notes,/*TrackingNo,vcShippingMethod,monShipCost,dtDeliveryDate,*/dtRentalStartDate,dtRentalReturnDate  from OpportunityItems OI
                  Join
				  XMLTABLE
					(
						'NewDataSet/BizDocItems'
						PASSING 
							CAST(v_strBizDocItems AS XML)
						COLUMNS
							OppItemID NUMERIC(9,0) PATH 'OppItemID'
							,Quantity DOUBLE PRECISION PATH 'Quantity'
							,Notes VARCHAR(500) PATH 'Notes'
							,monPrice DECIMAL(20,5) PATH 'monPrice'
							,dtRentalStartDate TIMESTAMP PATH 'dtRentalStartDate'
							,dtRentalReturnDate TIMESTAMP PATH 'dtRentalReturnDate'
					) AS OBZ 
                  on OBZ.OppItemID = OI.numoppitemtCode
                  where  numOppId = v_numOppId AND coalesce(OI.numUnitHour,0) > 0 AND coalesce(OI.numUnitHour,0) > 0;
                 
               end if;
            ELSE
               IF v_numBizDocId <> 304 AND v_bitAuthBizdoc = true AND v_bitInvoiceForDeferred = true then
				 -- 304 IS DEFERRED INVOICE BIZDOC
                  select   OpportunityBizDocs.numOppBizDocsId INTO v_numTop1DeferredBizDocID FROM
                  OpportunityBizDocs WHERE
                  numoppid = v_numOppId
                  AND numOppBizDocsId NOT IN(SELECT coalesce(numDeferredBizDocID,0) FROM OpportunityBizDocs WHERE numoppid = v_numOppId)
                  AND OpportunityBizDocs.numBizDocId = 304   ORDER BY
                  numOppBizDocsId LIMIT 1;
                  INSERT INTO OpportunityBizDocItems(numOppBizDocID,
						numOppItemID,
						numItemCode,
						numUnitHour,
						monPrice,
						monTotAmount,
						vcItemDesc,
						numWarehouseItmsID,
						vcType,
						vcAttributes,
						bitDropShip,
						bitDiscountType,
						fltDiscount,
						monTotAmtBefDiscount,
						vcNotes)
                  SELECT
                  v_numOppBizDocsId,
						numoppitemtCode,
						OI.numItemCode,
						(CASE v_bitRecurringBizDoc WHEN true THEN 1 ELSE coalesce(TempBizDoc.numUnitHour,0) END) AS numUnitHour,
						OI.monPrice,
						(CASE v_bitRecurringBizDoc
                  WHEN true THEN monPrice*1
                  ELSE(OI.monTotAmount/OI.numUnitHour)*coalesce(TempBizDoc.numUnitHour,0)
                  END) AS monTotAmount,
						vcItemDesc,
						numWarehouseItmsID,
						vcType,vcAttributes,
						bitDropShip,
						bitDiscountType,
						(CASE
                  WHEN bitDiscountType = false THEN fltDiscount
                  WHEN bitDiscountType = true THEN((fltDiscount/OI.numUnitHour)*coalesce(TempBizDoc.numUnitHour,0))
                  ELSE fltDiscount
                  END),
						(monTotAmtBefDiscount/OI.numUnitHour)*coalesce(TempBizDoc.numUnitHour,0),
						OI.vcNotes
                  FROM
                  OpportunityItems OI
                  JOIN
                  Item AS I
                  ON
                  I.numItemCode = OI.numItemCode
                  INNER JOIN(SELECT
                     OpportunityBizDocItems.numOppItemID,
								OpportunityBizDocItems.numUnitHour
                     FROM
                     OpportunityBizDocs
                     JOIN
                     OpportunityBizDocItems
                     ON
                     OpportunityBizDocs.numOppBizDocsId  = OpportunityBizDocItems.numOppBizDocID
                     WHERE
                     numoppid = v_numOppId
                     AND OpportunityBizDocs.numOppBizDocsId = v_numTop1DeferredBizDocID) TempBizDoc
                  ON
                  TempBizDoc.numOppItemID = OI.numoppitemtCode
                  WHERE
                  numoppid = v_numOppId
                  AND coalesce(OI.bitMappingRequired,false) = true
                  AND coalesce(OI.numUnitHour,0) > 0
                  AND (v_bitRecurringBizDoc = true OR coalesce(TempBizDoc.numUnitHour,0) > 0);
                  UPDATE OpportunityBizDocs SET numDeferredBizDocID = v_numTop1DeferredBizDocID WHERE numOppBizDocsId = v_numOppBizDocsId;
               ELSE
                  INSERT INTO OpportunityBizDocItems(numOppBizDocID,
						numOppItemID,
						numItemCode,
						numUnitHour,
						monPrice,
						monTotAmount,
						vcItemDesc,
						numWarehouseItmsID,
						vcType,
						vcAttributes,
						bitDropShip,
						bitDiscountType,
						fltDiscount,
						monTotAmtBefDiscount,
						vcNotes)
                  SELECT
                  v_numOppBizDocsId,
						numoppitemtCode,
						OI.numItemCode,
						(CASE v_bitRecurringBizDoc
                  WHEN true
                  THEN 1
                  ELSE(OI.numUnitHour -coalesce(TempBizDoc.numUnitHour,0))
                  END) AS numUnitHour,
						OI.monPrice,
						(CASE v_bitRecurringBizDoc
                  WHEN true THEN monPrice*1
                  ELSE(OI.monTotAmount/OI.numUnitHour)*(OI.numUnitHour -coalesce(TempBizDoc.numUnitHour,0))
                  END) AS monTotAmount,
						vcItemDesc,
						numWarehouseItmsID,
						vcType,vcAttributes,
						bitDropShip,
						bitDiscountType,
						(CASE
                  WHEN bitDiscountType = false THEN fltDiscount
                  WHEN bitDiscountType = true THEN((fltDiscount/OI.numUnitHour)*(OI.numUnitHour -coalesce(TempBizDoc.numUnitHour,0)))
                  ELSE fltDiscount
                  END),
						(monTotAmtBefDiscount/OI.numUnitHour)*(OI.numUnitHour -coalesce(TempBizDoc.numUnitHour,0)),
						OI.vcNotes
                  FROM
                  OpportunityItems OI
                  JOIN
                  Item AS I
                  ON
                  I.numItemCode = OI.numItemCode
                  LEFT JOIN LATERAL(SELECT
                     SUM(OBDI.numUnitHour) AS numUnitHour
                     FROM
                     OpportunityBizDocs OBD
                     JOIN
                     OpportunityBizDocItems OBDI
                     ON
                     OBDI.numOppBizDocID  = OBD.numOppBizDocsId
                     AND OBDI.numOppItemID = OI.numoppitemtCode
                     WHERE
                     numoppid = v_numOppId
                     AND 1 =(CASE
                     WHEN v_bitAuthBizdoc = true
                     THEN(CASE WHEN (numBizDocId = 304 OR (numBizDocId = v_numBizDocId AND coalesce(numDeferredBizDocID,0) = 0)) THEN 1 ELSE 0 END)
                     WHEN v_numBizDocId = 304 --Deferred Income
                     THEN(CASE WHEN (numBizDocId = v_numBizDocId OR coalesce(OBD.bitAuthoritativeBizDocs,0) = 1) THEN 1 ELSE 0 END)
                     ELSE(CASE WHEN numBizDocId = v_numBizDocId THEN 1 ELSE 0 END)
                     END)) TempBizDoc on TRUE
                  WHERE
                  numoppid = v_numOppId
                  AND coalesce(OI.numUnitHour,0) > 0
                  AND coalesce(OI.bitMappingRequired,false) = false
                  AND (v_bitRecurringBizDoc = true OR(coalesce(OI.numUnitHour,0) -coalesce(TempBizDoc.numUnitHour,0)) > 0);
               end if;
            end if;
            v_numOppBizDocsId := v_numOppBizDocsId;
         ELSE
            v_numOppBizDocsId := 0;
            v_monDealAmount := 0;
            v_monCreditAmount := 0;
            RAISE EXCEPTION 'NOT_ALLOWED';
         end if;
      ELSE
         UPDATE
         OpportunityBizDocs
         SET
         numModifiedBy = v_numUserCntID,dtModifiedDate = TIMEZONE('UTC',now()),
         vcComments = v_vcComments,numShipVia = v_numShipVia,vcTrackingURL = v_vcTrackingURL,
         dtFromDate = v_dtFromDate,numBizDocStatus = v_numBizDocStatus,
         numSequenceId = v_numSequenceId,numBizDocTempID = v_numBizDocTempID,
         vcTrackingNo = v_vcTrackingNo,vcRefOrderNo = v_vcRefOrderNo,fltExchangeRateBizDoc = v_fltExchangeRateBizDoc,
         bitShippingGenerated = v_bitShippingGenerated,
         vcVendorInvoice = v_vcVendorInvoiceName,numARAccountID = v_numARAccountID
         WHERE
         numOppBizDocsId = v_numOppBizDocsId;

		--Added By:Sachin Sadhu||Date:27thAug2014
		--Purpose :MAke entry in WFA queue 
         IF EXISTS(SELECT 1 FROM OpportunityMaster WHERE numOppId = v_numOppId AND tintopptype = 1 AND coalesce(tintoppstatus,0) = 1) then
		
			 -- numeric(18, 0)							
			 -- numeric(18, 0)							
			 -- numeric(18, 0)							
			 -- numeric(18, 0)							
			 -- numeric(18, 0)					
			 -- numeric(18, 0)					
			 -- tinyint						
            PERFORM USP_ManageOpportunityAutomationQueue(v_numOppQueueID := 0,v_numDomainID := v_numDomainID,v_numOppId := v_numOppId,
            v_numOppBizDocsId := v_numOppBizDocsId,v_numOrderStatus := 0,v_numUserCntID := v_numUserCntID,
            v_tintProcessStatus := 1::SMALLINT,v_tintMode := 1::SMALLINT);
         end if; 
		--end of code

         IF OCTET_LENGTH(v_strBizDocItems) > 2 then
            DELETE FROM
            OpportunityBizDocItems
            WHERE
            numOppItemID NOT IN(SELECT
            OppItemID
            FROM 
			XMLTABLE
			(
				'NewDataSet/BizDocItems'
				PASSING 
					CAST(v_strBizDocItems AS XML)
				COLUMNS
					OppItemID NUMERIC(9,0) PATH 'OppItemID'
			))
            AND numOppBizDocID = v_numOppBizDocsId;
            IF v_bitRentalBizDoc = true then
			
               update OpportunityItems OI set monPrice = OBZ.monPrice,monTotAmount = OBZ.monTotAmount,monTotAmtBefDiscount = OBZ.monTotAmount
               from
			    XMLTABLE
				(
					'NewDataSet/BizDocItems'
					PASSING 
						CAST(v_strBizDocItems AS XML)
					COLUMNS
						OppItemID NUMERIC(9,0) PATH 'OppItemID'
						,Quantity DOUBLE PRECISION PATH 'Quantity'
						,monPrice DECIMAL(20,5) PATH 'monPrice'
						,monTotAmount DECIMAL(20,5) PATH 'monTotAmount'
				) AS OBZ 
               where OBZ.OppItemID = OI.numoppitemtCode AND OBZ.OppItemID = OI.numoppitemtCode;
            end if;
            update OpportunityBizDocItems OBI set numUnitHour = Quantity,monPrice = CASE WHEN v_tintOppType = 2 THEN OBZ.monPrice ELSE OI.monPrice END,monTotAmount = CASE WHEN v_tintOppType = 2 THEN OBZ.monPrice*Quantity ELSE(OI.monTotAmount/OI.numUnitHour)*Quantity END,
            vcNotes = Notes,fltDiscount = case when OI.bitDiscountType = false then OI.fltDiscount when OI.bitDiscountType = true then(OI.fltDiscount/OI.numUnitHour)*Quantity else OI.fltDiscount end,monTotAmtBefDiscount =(OI.monTotAmtBefDiscount/OI.numUnitHour)*Quantity,                                                                    
			/*OBI.vcTrackingNo=TrackingNo,OBI.vcShippingMethod=OBZ.vcShippingMethod,OBI.monShipCost=OBZ.monShipCost,OBI.dtDeliveryDate=OBZ.dtDeliveryDate*/
			dtRentalStartDate = OBZ.dtRentalStartDate,
            dtRentalReturnDate = OBZ.dtRentalReturnDate
            from OpportunityItems OI Join
			 XMLTABLE
			(
				'NewDataSet/BizDocItems'
				PASSING 
					CAST(v_strBizDocItems AS XML)
				COLUMNS
					OppItemID NUMERIC(9,0) PATH 'OppItemID'
					,Quantity DOUBLE PRECISION PATH 'Quantity'
					,Notes VARCHAR(500) PATH 'Notes'
					,monPrice DECIMAL(20,5) PATH 'monPrice'
					,dtRentalStartDate TIMESTAMP PATH 'dtRentalStartDate'
					,dtRentalReturnDate TIMESTAMP PATH 'dtRentalReturnDate'
			) AS OBZ 
			ON OBZ.OppItemID = OI.numoppitemtCode
            where  OBI.numOppItemID = OI.numoppitemtCode AND(OI.numOppId = v_numOppId and OBI.numOppBizDocID = v_numOppBizDocsId AND coalesce(OI.numUnitHour,0) > 0);
            
			--TrackingNo varchar(500),
			--vcShippingMethod varchar(100),
			--monShipCost DECIMAL(20,5),
			--dtDeliveryDate DATETIME
            insert into
            OpportunityBizDocItems(numOppBizDocID,numOppItemID,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,vcType,vcAttributes,bitDropShip,bitDiscountType,fltDiscount,monTotAmtBefDiscount,vcNotes/*,vcTrackingNo,vcShippingMethod,monShipCost,dtDeliveryDate*/)
            select v_numOppBizDocsId,numoppitemtCode,numItemCode,Quantity,CASE WHEN v_tintOppType = 2 THEN OBZ.monPrice ELSE OI.monPrice END,CASE WHEN v_tintOppType = 2 THEN OBZ.monPrice*Quantity ELSE(monTotAmount/numUnitHour)*Quantity END,vcItemDesc,numWarehouseItmsID,vcType,vcAttributes,bitDropShip,bitDiscountType,
			case when bitDiscountType = false then fltDiscount when bitDiscountType = true then(fltDiscount/numUnitHour)*Quantity else fltDiscount end ,(monTotAmtBefDiscount/numUnitHour)*Quantity,Notes/*,TrackingNo,vcShippingMethod,monShipCost,dtDeliveryDate*/ from OpportunityItems OI
            Join
			XMLTABLE
				(
					'NewDataSet/BizDocItems'
					PASSING 
						CAST(v_strBizDocItems AS XML)
					COLUMNS
						OppItemID NUMERIC(9,0) PATH 'OppItemID'
						,Quantity DOUBLE PRECISION PATH 'Quantity'
						,monPrice DECIMAL(20,5) PATH 'monPrice'
						,Notes VARCHAR(500) PATH 'Notes'
				) AS OBZ 			
            on OBZ.OppItemID = OI.numoppitemtCode and OI.numoppitemtCode not in(select numOppItemID from OpportunityBizDocItems where numOppBizDocID = v_numOppBizDocsId)
            where  numOppId = v_numOppId AND coalesce(OI.numUnitHour,0) > 0;
    
         end if;
      end if;
      IF coalesce((SELECT numBizDocId FROM OpportunityBizDocs WHERE numOppBizDocsId = v_numOppBizDocsId),0) = 55206 then
	
         UPDATE
         OpportunityItems OI
         SET
         numQtyPicked = coalesce((SELECT
         SUM(OBDI.numUnitHour)
         FROM
         OpportunityBizDocs OBD
         INNER JOIN
         OpportunityBizDocItems OBDI
         ON
         OBD.numOppBizDocsId = OBDI.numOppBizDocID
         AND OI.numoppitemtCode = OBDI.numOppItemID
         WHERE
         OBD.numoppid = OI.numOppId
         AND OBD.numBizDocId = 55206),0)
		
         WHERE
         OI.numOppId = v_numOppId;
      end if;
      IF EXISTS(SELECT
      OBDI.numOppItemID
      FROM
      OpportunityBizDocs OBD
      INNER JOIN
      OpportunityBizDocItems OBDI
      ON
      OBD.numOppBizDocsId = OBDI.numOppBizDocID
      LEFT JOIN
      OpportunityItems OI
      ON
      OBDI.numOppItemID = OI.numoppitemtCode
      WHERE
      OBD.numoppid = v_numOppId
      AND OBD.numBizDocId = v_numBizDocId
      AND OBD.numBizDocId IN(287,296,29397)
      GROUP BY
      OBDI.numOppItemID,OI.numUnitHour
      HAVING
      SUM(coalesce(OBDI.numUnitHour,0)) > coalesce(OI.numUnitHour,0)) then
	
         RAISE EXCEPTION 'BIZDOC_QTY_MORE_THEN_ORDERED_QTY';
      end if;
      IF v_numOppBizDocsId > 0 then
	
         v_monDealAmount := coalesce(GetDealAmount(v_numOppId,TIMEZONE('UTC',now()),v_numOppBizDocsId),
         0);
         IF v_numBizDocId = 297 OR v_numBizDocId = 299 then
		
            IF NOT EXISTS(SELECT * FROM NameTemplate WHERE numDomainID = v_numDomainID AND numRecordID = v_numBizDocId AND tintModuleID = 2) then
			
               INSERT INTO NameTemplate(numDomainID,tintModuleID,numRecordID,vcNameTemplate,numSequenceId,numMinLength)
               SELECT
               v_numDomainID,2,v_numBizDocId,UPPER(SUBSTR(GetListIemName(v_numBizDocId),0,4)) || '-',1,4;
               v_numSequenceId := CAST(1 AS VARCHAR(50));
            ELSE
               select   numSequenceId INTO v_numSequenceId FROM NameTemplate WHERE numDomainID = v_numDomainID AND numRecordID = v_numBizDocId AND tintModuleID = 2;
               UPDATE NameTemplate SET numSequenceId = coalesce(numSequenceId,0)+1 WHERE numDomainID = v_numDomainID AND numRecordID = v_numBizDocId AND tintModuleID = 2;
            end if;
         end if;
         UPDATE
         OpportunityBizDocs
         SET
         vcBizDocID = vcBizDocName || coalesce(v_numSequenceId,''),numSequenceId = CASE WHEN (numBizDocId = 297 OR numBizDocId = 299) THEN v_numSequenceId ELSE numSequenceId END,monDealAmount = v_monDealAmount
         WHERE
         numOppBizDocsId = v_numOppBizDocsId;
      end if;

	-- 1 if recurrence is enabled for authorative bizdoc
      IF coalesce(v_bitRecur,false) = true then
	
		-- First Check if receurrece is already created for sales order because user should not 
		-- be able to create recurrence on both order and invoice
         IF(SELECT COUNT(*) FROM RecurrenceConfiguration WHERE numOppID = v_numOppId AND numType = 1) > 0 then
		
            RAISE EXCEPTION 'RECURRENCE_ALREADY_CREATED_FOR_ORDER';
		--User can not create recurrence on bizdoc where all items are added to bizdoc
         ELSEIF(SELECT CheckAllItemQuantityAddedToBizDoc(v_numOppId,v_numOppBizDocsId)) = true
         then
		
            RAISE EXCEPTION 'RECURRENCE_NOT_ALLOWED_ALL_ITEM_QUANTITY_ADDED_TO_INVOICE';
         ELSE
            PERFORM USP_RecurrenceConfiguration_Insert(v_numDomainID := v_numDomainID,v_numUserCntID := v_numUserCntID,v_numOppId := v_numOppId,
            v_numOppBizDocID := v_numOppBizDocsId,v_dtStartDate := v_dtStartDate,
            v_dtEndDate := NULL,v_numType := 2::SMALLINT,v_vcType := 'Invoice',
            v_vcFrequency := v_vcFrequency,v_numFrequency := v_numFrequency::SMALLINT);
            UPDATE OpportunityMaster SET vcRecurrenceType = 'Invoice' WHERE numOppId = v_numOppId;
         end if;
      ELSEIF coalesce(v_numRecConfigID,0) > 0 AND coalesce(v_bitDisable,false) = true
      then
	
         UPDATE
         RecurrenceConfiguration
         SET
         bitDisabled = true,numDisabledBy = v_numUserCntID,dtDisabledDate = LOCALTIMESTAMP
         WHERE
         numRecConfigID = v_numRecConfigID;
      end if;
      IF v_numOppBizDocsId > 0 AND v_numBizDocId = 29397 AND v_tintCommitAllocation = 2 AND v_bitAllocateInventoryOnPickList = true then
	
         PERFORM USP_PickListManageSOWorkOrder(v_numDomainID,v_numUserCntID,v_numOppId,v_numOppBizDocsId);
		-- Allocate Inventory
         PERFORM USP_ManageInventoryPickList(v_numDomainID,v_numOppId,v_numOppBizDocsId,v_numUserCntID);
      end if;
      EXCEPTION WHEN OTHERS THEN
         GET STACKED DIAGNOSTICS
			  my_ex_state   = RETURNED_SQLSTATE,
			  my_ex_message = MESSAGE_TEXT,
			  my_ex_detail  = PG_EXCEPTION_DETAIL,
			  my_ex_hint    = PG_EXCEPTION_HINT,
			  my_ex_ctx     = PG_EXCEPTION_CONTEXT;

			 raise exception '% % % % %', my_ex_state, my_ex_message, my_ex_detail, my_ex_hint, my_ex_ctx;
   END;
/****** Object:  StoredProcedure [dbo].[USP_DeleteItem]    Script Date: 03/25/2009 16:15:36 ******/
   RETURN;
END; $$;


