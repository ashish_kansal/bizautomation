CREATE OR REPLACE FUNCTION fn_GeTimeDtlsbyProStgID
(
	p_numProID numeric
	,p_numProStageID numeric
	,p_tintType smallint
)            
RETURNS VARCHAR(100) LANGUAGE plpgsql            
AS $$
	DECLARE
	v_vcRetValue VARCHAR(250);       
BEGIN
	SELECT 
		CAST(CAST(SUM(DateDiff('minute',dtFromDate,dtToDate)) / 60 AS DECIMAL(10, 2)) AS VARCHAR(100)) 
		|| ' ( Amt ' 
		|| CASE 
				WHEN numType = 1 
				THEN CAST(CAST( SUM(monamount * CAST(CAST(DateDiff('minute',dtFromDate,dtToDate) AS DECIMAL(10,2))/60 AS DECIMAL(10,2))) AS DECIMAL(10,2)) AS VARCHAR(100))
				ELSE '0.00'
			END || ')'  INTO v_vcRetValue
from  timeandexpense    
where 
numProID=p_numProID and numStageID=p_numProStageID
 AND
 numCategory = 1 and tintTEType = 2
 AND numType= p_tintType
GROUP BY dtFromDate,dtToDate,numType;

            
 RETURN v_vcRetValue;             
END; $$;

