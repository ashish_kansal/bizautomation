CREATE OR REPLACE FUNCTION fn_GetContactAddedToContract(v_numDomainId NUMERIC,
      v_numConractID NUMERIC)
RETURNS VARCHAR(2000) LANGUAGE plpgsql
   AS $$
   DECLARE
   v_ContactList  VARCHAR(2000);
BEGIN
   select STRING_AGG(CAST(numContactId AS VARCHAR),', ') INTO v_ContactList FROM ContractsContact WHERE numDomainId = v_numDomainId AND numContractID = v_numConractID;
    
   RETURN coalesce(v_ContactList,'');
END; $$;

