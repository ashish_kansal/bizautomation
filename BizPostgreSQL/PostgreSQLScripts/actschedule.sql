-- Function definition script ActSchedule for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION ActSchedule(v_numUserCntID NUMERIC(9,0),v_dtStartDate TIMESTAMP,v_tintTask INTEGER,v_offSet INTEGER)
RETURNS VARCHAR(1000) LANGUAGE plpgsql
   AS $$
   DECLARE
   v_vcSchedule  VARCHAR(1000);  
   v_numCommId  NUMERIC(9,0);  
   v_dtEnd  TIMESTAMP;
   SWV_RowCount INTEGER;
BEGIN
   v_dtEnd := v_dtStartDate+INTERVAL '1 day';  
  
   v_numCommId := 0;  
   v_vcSchedule := '';  
   if v_tintTask > 0 then

      select   numCommId, SUBSTR(CAST(dtStartTime+CAST(-v_offSet || 'minute' as interval) AS VARCHAR(30)),
      length(CAST(dtStartTime+CAST(-v_offSet || 'minute' as interval) AS VARCHAR(30))) -8+1) || '-' || SUBSTR(CAST(dtEndTime+CAST(-v_offSet || 'minute' as interval) AS VARCHAR(30)),
      length(CAST(dtEndTime+CAST(-v_offSet || 'minute' as interval) AS VARCHAR(30))) -8+1) INTO v_numCommId,v_vcSchedule from Communication where numAssign = v_numUserCntID and (dtStartTime+CAST(-v_offSet || 'minute' as interval) between v_dtStartDate and v_dtEnd or dtEndTime+CAST(-v_offSet || 'minute' as interval) between v_dtStartDate and v_dtEnd) and bitTask = v_tintTask and bitClosedFlag = false    LIMIT 1;
      while v_numCommId > 0 LOOP
         select   numCommId, coalesce(v_vcSchedule,'') || ', ' || SUBSTR(CAST(dtStartTime+CAST(-v_offSet || 'minute' as interval) AS VARCHAR(30)),
         length(CAST(dtStartTime+CAST(-v_offSet || 'minute' as interval) AS VARCHAR(30))) -8+1) || '-' || SUBSTR(CAST(dtEndTime+CAST(-v_offSet || 'minute' as interval) AS VARCHAR(30)),
         length(CAST(dtEndTime+CAST(-v_offSet || 'minute' as interval) AS VARCHAR(30))) -8+1) INTO v_numCommId,v_vcSchedule from Communication where numAssign = v_numUserCntID
         and (dtStartTime+CAST(-v_offSet || 'minute' as interval) between v_dtStartDate and v_dtEnd or
         dtEndTime+CAST(-v_offSet || 'minute' as interval) between v_dtStartDate and v_dtEnd) and bitTask = v_tintTask and bitClosedFlag = false
         and numCommId > v_numCommId    LIMIT 1;
         GET DIAGNOSTICS SWV_RowCount = ROW_COUNT;
         if SWV_RowCount = 0 then 
            v_numCommId := 0;
         end if;
      END LOOP;
   else
      select   numCommId, SUBSTR(CAST(dtStartTime+CAST(-v_offSet || 'minute' as interval) AS VARCHAR(30)),
      length(CAST(dtStartTime+CAST(-v_offSet || 'minute' as interval) AS VARCHAR(30))) -8+1) || '-' || SUBSTR(CAST(dtEndTime+CAST(-v_offSet || 'minute' as interval) AS VARCHAR(30)),
      length(CAST(dtEndTime+CAST(-v_offSet || 'minute' as interval) AS VARCHAR(30))) -8+1) INTO v_numCommId,v_vcSchedule from Communication where numAssign = v_numUserCntID and (dtStartTime+CAST(-v_offSet || 'minute' as interval) between v_dtStartDate and v_dtEnd or dtEndTime+CAST(-v_offSet || 'minute' as interval) between v_dtStartDate and v_dtEnd) and bitClosedFlag = false and bitTask in(1,2)    LIMIT 1;
      while v_numCommId > 0 LOOP
         select   numCommId, coalesce(v_vcSchedule,'') || ', ' || SUBSTR(CAST(dtStartTime+CAST(-v_offSet || 'minute' as interval) AS VARCHAR(30)),
         length(CAST(dtStartTime+CAST(-v_offSet || 'minute' as interval) AS VARCHAR(30))) -8+1) || '-' || SUBSTR(CAST(dtEndTime+CAST(-v_offSet || 'minute' as interval) AS VARCHAR(30)),
         length(CAST(dtEndTime+CAST(-v_offSet || 'minute' as interval) AS VARCHAR(30))) -8+1) INTO v_numCommId,v_vcSchedule from Communication where numAssign = v_numUserCntID and (dtStartTime+CAST(-v_offSet || 'minute' as interval) between v_dtStartDate and v_dtEnd or dtEndTime+CAST(-v_offSet || 'minute' as interval) between v_dtStartDate and v_dtEnd)
         and bitClosedFlag = false and numCommId > v_numCommId and bitTask in(1,2)    LIMIT 1;
         GET DIAGNOSTICS SWV_RowCount = ROW_COUNT;
         if SWV_RowCount = 0 then 
            v_numCommId := 0;
         end if;
      END LOOP;
   end if;  
  
  
  
   return Replace(v_vcSchedule,'M','');
END; $$;

