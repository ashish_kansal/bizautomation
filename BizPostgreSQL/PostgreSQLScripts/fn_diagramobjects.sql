-- Function definition script fn_diagramobjects for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021

CREATE OR REPLACE FUNCTION fn_diagramobjects()
RETURNS INTEGER LANGUAGE plpgsql
   AS $$
   DECLARE
   v_id_upgraddiagrams  INTEGER;
   v_id_sysdiagrams  INTEGER;
   v_id_helpdiagrams  INTEGER;
   v_id_helpdiagramdefinition  INTEGER;
   v_id_creatediagram  INTEGER;
   v_id_renamediagram  INTEGER;
   v_id_alterdiagram  INTEGER; 
   v_id_dropdiagram  INTEGER;
   v_InstalledObjects  INTEGER;
BEGIN
   v_InstalledObjects := 0;

   v_id_upgraddiagrams := object_id(N'dbo.sp_upgraddiagrams');
   v_id_sysdiagrams := object_id(N'dbo.sysdiagrams');
   v_id_helpdiagrams := object_id(N'dbo.sp_helpdiagrams');
   v_id_helpdiagramdefinition := object_id(N'dbo.sp_helpdiagramdefinition');
   v_id_creatediagram := object_id(N'dbo.sp_creatediagram');
   v_id_renamediagram := object_id(N'dbo.sp_renamediagram');
   v_id_alterdiagram := object_id(N'dbo.sp_alterdiagram');
   v_id_dropdiagram := object_id(N'dbo.sp_dropdiagram');

   if v_id_upgraddiagrams is not null then
      v_InstalledObjects := v_InstalledObjects::bigint+1;
   end if;
   if v_id_sysdiagrams is not null then
      v_InstalledObjects := v_InstalledObjects::bigint+2;
   end if;
   if v_id_helpdiagrams is not null then
      v_InstalledObjects := v_InstalledObjects::bigint+4;
   end if;
   if v_id_helpdiagramdefinition is not null then
      v_InstalledObjects := v_InstalledObjects::bigint+8;
   end if;
   if v_id_creatediagram is not null then
      v_InstalledObjects := v_InstalledObjects::bigint+16;
   end if;
   if v_id_renamediagram is not null then
      v_InstalledObjects := v_InstalledObjects::bigint+32;
   end if;
   if v_id_alterdiagram  is not null then
      v_InstalledObjects := v_InstalledObjects::bigint+64;
   end if;
   if v_id_dropdiagram is not null then
      v_InstalledObjects := v_InstalledObjects::bigint+128;
   end if;
		
   return v_InstalledObjects;
END; $$;

