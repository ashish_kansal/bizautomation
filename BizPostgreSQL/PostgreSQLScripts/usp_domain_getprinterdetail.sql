-- Stored procedure definition script USP_Domain_GetPrinterDetail for PostgreSQL
CREATE OR REPLACE FUNCTION USP_Domain_GetPrinterDetail(v_numDomainID NUMERIC(18,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT
		coalesce(vcPrinterIPAddress,'')  AS "vcPrinterIPAddress",
		coalesce(vcPrinterPort,'') AS "vcPrinterPort"
   FROM
   Domain
   WHERE
   numDomainId = v_numDomainID;
END; $$;











