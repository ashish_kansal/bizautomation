CREATE OR REPLACE FUNCTION USP_SalesFulfillmentWorkflow(v_numDomainID NUMERIC(9,0),
      v_numOppID NUMERIC(9,0),
      v_numOppBizDocsId NUMERIC(9,0),
      v_numUserCntID NUMERIC(9,0),
      v_numBizDocStatus NUMERIC(9,0),
	  INOUT v_numOppAuthBizDocsId NUMERIC(9,0) DEFAULT 0 ,
	  v_bitNotValidateBizDocFulfillment BOOLEAN DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE
   my_ex_state TEXT;
	my_ex_message TEXT;
	my_ex_detail TEXT;
	my_ex_hint TEXT;
	my_ex_ctx TEXT;
    
   v_numBizDocId  NUMERIC(9,0);
   v_numRuleID  NUMERIC(9,0);
   v_numPackingSlipBizDocId  NUMERIC(9,0);
   v_numAuthInvoice  NUMERIC(9,0);
   v_tintOppType  NUMERIC(9,0);
   v_USP_CreateBizDocs_monCreditAmount DECIMAL(20,5);
   v_USP_CreateBizDocs_monDealAmount DECIMAL(20,5);
   v_USP_CreateBizDocs_numOppBizDocsId NUMERIC(9,0);
BEGIN
   BEGIN		
		--DECLARE @numBizDocStatus AS NUMERIC(9);SET @numBizDocStatus=0
      v_numBizDocId := 0;
      v_numRuleID := 0;
      v_tintOppType := 0;
		 /*@numBizDocStatus=ISNULL(OBD.numBizDocStatus,0),*/
      select   tintopptype, OBD.numBizDocId INTO v_tintOppType,v_numBizDocId FROM OpportunityMaster OM JOIN OpportunityBizDocs OBD ON OM.numOppId = OBD.numoppid WHERE OM.numDomainId = v_numDomainID AND OM.numOppId = v_numOppID AND OBD.numOppBizDocsId = v_numOppBizDocsId; 
				/*AND ISNULL(OBD.numBizDocStatus,0)!=0 AND ISNULL(OBD.numBizDocStatus,0)!=ISNULL(OBD.numBizDocStatusOLD,0)*/
		
      IF v_tintOppType = 1 then
		
         IF v_numBizDocStatus > 0 AND v_numBizDocId > 0 then
		
            IF v_numBizDocId = 296 then
			
               select   coalesce(numRuleID,0) INTO v_numRuleID FROM OpportunityAutomationRules WHERE numDomainID = v_numDomainID AND numBizDocStatus1 = v_numBizDocStatus AND numRuleID IN(1,2,3,4,5,6);
               IF v_numRuleID = 1 then --create an invoice for all items within the fulfillment order
				
                  select   coalesce(numAuthoritativeSales,0) INTO v_numAuthInvoice From AuthoritativeBizDocs Where AuthoritativeBizDocs.numDomainId = v_numDomainID;
                  v_USP_CreateBizDocs_monCreditAmount := 0;
                  v_USP_CreateBizDocs_monDealAmount := 0;
                  SELECT * INTO v_numOppAuthBizDocsId FROM USP_CreateBizDocs(v_numOppID := v_numOppID,v_numBizDocId := v_numAuthInvoice,v_numUserCntID := v_numUserCntID,
                  v_numOppBizDocsId := v_numOppAuthBizDocsId,v_vcComments := '',
                  v_bitPartialFulfillment := true,v_strBizDocItems := '',v_numShipVia := 0,
                  v_vcTrackingURL := '',v_dtFromDate := '2013-1-2 10:55:29.610',
                  v_numBizDocStatus := 0,v_bitRecurringBizDoc := false,v_numSequenceId := '',
                  v_tintDeferred := 0::SMALLINT,v_monCreditAmount := v_USP_CreateBizDocs_monCreditAmount,
                  v_ClientTimeZoneOffset := 0,v_monDealAmount := v_USP_CreateBizDocs_monDealAmount,
                  v_bitRentalBizDoc := false,v_numBizDocTempID := 0,
                  v_vcTrackingNo := '',v_vcRefOrderNo := '',v_OMP_SalesTaxPercent := 0,
                  v_numFromOppBizDocsId := v_numOppBizDocsId,v_bitTakeSequenceId := true,
                  v_bitNotValidateBizDocFulfillment := v_bitNotValidateBizDocFulfillment);
               ELSEIF v_numRuleID = 2
               then --create a packing slip for its inventory items
				
                  select   numListItemID INTO v_numPackingSlipBizDocId FROM Listdetails WHERE Listdetails.vcData = 'Packing Slip' AND Listdetails.constFlag = 1 AND Listdetails.numListID = 27;
                  v_USP_CreateBizDocs_numOppBizDocsId := 0;
                  v_USP_CreateBizDocs_monCreditAmount := 0;
                  v_USP_CreateBizDocs_monDealAmount := 0;
                  PERFORM USP_CreateBizDocs(v_numOppID := v_numOppID,v_numBizDocId := v_numPackingSlipBizDocId,v_numUserCntID := v_numUserCntID,
                  v_numOppBizDocsId := v_USP_CreateBizDocs_numOppBizDocsId,
                  v_vcComments := '',v_bitPartialFulfillment := true,v_strBizDocItems := '',
                  v_numShipVia := 0,v_vcTrackingURL := '',v_dtFromDate := '2013-1-2 10:56:4.477',
                  v_numBizDocStatus := 0,v_bitRecurringBizDoc := false,
                  v_numSequenceId := '',v_tintDeferred := 0::SMALLINT,v_monCreditAmount := v_USP_CreateBizDocs_monCreditAmount,
                  v_ClientTimeZoneOffset := 0,
                  v_monDealAmount := v_USP_CreateBizDocs_monDealAmount,v_bitRentalBizDoc := false,
                  v_numBizDocTempID := 0,v_vcTrackingNo := '',v_vcRefOrderNo := '',
                  v_OMP_SalesTaxPercent := 0,v_numFromOppBizDocsId := v_numOppBizDocsId,
                  v_bitTakeSequenceId := true,v_bitNotValidateBizDocFulfillment := v_bitNotValidateBizDocFulfillment);
               ELSEIF v_numRuleID = 3
               then --release its inventory items from allocation
                  BEGIN
					
                     PERFORM USP_SalesFulfillmentUpdateInventory(v_numDomainID := v_numDomainID,v_numOppID := v_numOppID,v_numOppBizDocsId := v_numOppBizDocsId,
                     v_numUserCntID := v_numUserCntID,v_tintMode := 1::SMALLINT);
                     EXCEPTION WHEN OTHERS THEN
                       GET STACKED DIAGNOSTICS
							  my_ex_state   = RETURNED_SQLSTATE,
							  my_ex_message = MESSAGE_TEXT,
							  my_ex_detail  = PG_EXCEPTION_DETAIL,
							  my_ex_hint    = PG_EXCEPTION_HINT,
							  my_ex_ctx     = PG_EXCEPTION_CONTEXT;
                        IF my_ex_message iLIKE '%NotSufficientQty_Allocation%' then
						
							 --  tinyint
								 --  numeric(18, 0)
								 --  numeric(18, 0)
								 --  text
                           select   coalesce(numBizDocStatus1,0) INTO v_numBizDocStatus FROM OpportunityAutomationRules WHERE numDomainID = v_numDomainID AND coalesce(numRuleID,0) = '6';
                           PERFORM USP_UpdateSingleFieldValue(v_tintMode := 27::SMALLINT,v_numUpdateRecordID := v_numOppBizDocsId,v_numUpdateValueID := v_numBizDocStatus,
                           v_vcText := '',v_numDomainID := v_numDomainID); --  numeric(18, 0)
								
                           INSERT INTO OppFulfillmentBizDocsStatusHistory(numDomainID,numOppId,numOppBizDocsId,numBizDocStatus,numUserCntID,dtCreatedDate) VALUES(v_numDomainID,v_numOppID,v_numOppBizDocsId,v_numBizDocStatus,v_numUserCntID,TIMEZONE('UTC',now()));
                        end if;

                        raise exception '% % % % %', my_ex_state, my_ex_message, my_ex_detail, my_ex_hint, my_ex_ctx;
                  END;
               ELSEIF v_numRuleID = 4
               then --return its inventory items back to allocation
				
                  select   coalesce(numAuthoritativeSales,0) INTO v_numAuthInvoice From AuthoritativeBizDocs Where AuthoritativeBizDocs.numDomainId = v_numDomainID;
                  IF EXISTS(SELECT numOppBizDocsId FROM OpportunityMaster OM JOIN OpportunityBizDocs OBD ON OM.numOppId = OBD.numoppid
                  WHERE OM.numDomainId = v_numDomainID AND OM.numOppId = v_numOppID  AND OBD.numBizDocId IN(v_numAuthInvoice)) then
						
                     RAISE EXCEPTION 'AlreadyInvoice_DoNotReturn';
                     RETURN;
                  ELSE
								 --  numeric(9, 0)
							 --  numeric(9, 0)
							 --  numeric(9, 0)
							  --  numeric(9, 0) 
                     PERFORM USP_SalesFulfillmentUpdateInventory(v_numDomainID := v_numDomainID,v_numOppID := v_numOppID,v_numOppBizDocsId := v_numOppBizDocsId,
                     v_numUserCntID := v_numUserCntID,v_tintMode := 2::SMALLINT);
                  end if;
               ELSEIF v_numRuleID = 5
               then --Remove the Fulfillment Order from the Sales Fulfillment queue
				
                  RAISE NOTICE 'Rule-5';
               end if;
            end if;
         end if;
         IF v_numBizDocStatus > 0 then
		
            INSERT INTO OppFulfillmentBizDocsStatusHistory(numDomainID,
					numOppId,
					numOppBizDocsId,
					numBizDocStatus,
					numUserCntID,
					dtCreatedDate) VALUES( 
					/* numDomainId - numeric(18, 0) */ v_numDomainID,
					/* numOppId - numeric(18, 0) */ v_numOppID,
					/* numOppBizDocsId - numeric(18, 0) */ v_numOppBizDocsId,
					/* numBizDocStatus - numeric(18, 0) */ v_numBizDocStatus,
					/* numUserCntID - numeric(18, 0) */ v_numUserCntID,
					/* dtCreatedDate - datetime */ TIMEZONE('UTC',now()));
         end if;
      end if;
      EXCEPTION WHEN OTHERS THEN
          GET STACKED DIAGNOSTICS
			  my_ex_state   = RETURNED_SQLSTATE,
			  my_ex_message = MESSAGE_TEXT,
			  my_ex_detail  = PG_EXCEPTION_DETAIL,
			  my_ex_hint    = PG_EXCEPTION_HINT,
			  my_ex_ctx     = PG_EXCEPTION_CONTEXT;

			 raise exception '% % % % %', my_ex_state, my_ex_message, my_ex_detail, my_ex_hint, my_ex_ctx;
   END;
END; $$;
/****** Object:  StoredProcedure [dbo].[USP_SaveCheckCompanyDetails]    Script Date: 07/26/2008 16:20:55 ******/



