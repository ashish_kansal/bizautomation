-- Stored procedure definition script usp_ManageStageItemDependency for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION usp_ManageStageItemDependency(v_numStageDetailID NUMERIC DEFAULT 0,
    v_numDependantOnID NUMERIC DEFAULT 0 ,
    v_tintMode SMALLINT DEFAULT 0)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   IF v_tintMode = 0 then

      insert into StageDependency(numStageDetailId,numDependantOnID) values(v_numStageDetailID,v_numDependantOnID);
   ELSEIF v_tintMode = 1
   then
      delete from StageDependency where numStageDetailId = v_numStageDetailID and numDependantOnID = v_numDependantOnID;
   end if;
/****** Object:  StoredProcedure [dbo].[USP_ManageStyleSheets]    Script Date: 08/08/2009 15:03:00 ******/
   RETURN;
END; $$;


