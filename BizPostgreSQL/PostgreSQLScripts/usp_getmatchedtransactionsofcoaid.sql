CREATE OR REPLACE FUNCTION USP_GetMatchedTransactionsOfCOAId
(
	v_numAccountID NUMERIC(18,0),
	v_CurrentPage INTEGER,
	v_PageSize INTEGER,
	INOUT v_TotRecs INTEGER, 
	INOUT SWV_RefCur refcursor
)
LANGUAGE plpgsql  
   AS $$
   DECLARE
   v_firstRec  INTEGER;                                                              
   v_lastRec  INTEGER;
   v_strSql  VARCHAR(5000); 
   v_strSql1  VARCHAR(5000);
BEGIN
	v_firstRec :=(v_CurrentPage::bigint -1)*v_PageSize::bigint;                             
	v_lastRec :=(v_CurrentPage::bigint*v_PageSize::bigint+1);                                                                    
                                     
	v_strSql := 'SELECT 
						BST.dtDatePosted,
						BST.numTransactionID,
						BST.monAmount,
						DM.monDepositAmount AS MAmount, 
						BST.vcPayeeName || BST.vcMemo AS vcDescription,
						C.vcAccountName, 
						BTM.tintReferenceType,
						BTM.numReferenceID, 
						CAST(CAST(BST.dtDatePosted AS DATE) AS VARCHAR) || '' Deposit ('' || COALESCE(C.vcAccountName,'''') || '') to '' || COALESCE(CI.vcCompanyname,''-'') || '' for $'' || CAST(ABS(BST.monAmount) AS VARCHAR) AS TransMatchDetails
					FROM BankStatementTransactions  BST 
					LEFT JOIN BankTransactionMapping BTM ON BST.numTransactionID = BTM.numTransactionID
					LEFT JOIN DepositMaster DM  ON BTM.numReferenceID = DM.numDepositId
					LEFT JOIN DepositeDetails DD ON DD.numDepositId = DM.numDepositId
					LEFT JOIN Chart_Of_Accounts C ON DD.numAccountID = C.numAccountId
					LEFT JOIN DivisionMaster D ON DD.numReceivedFrom	= D.numDivisionID
					LEFT JOIN companyinfo CI ON D.numCompanyID= CI.numCompanyID ';

	v_strSql := coalesce(v_strSql,'') || ' WHERE 
												BST.numAccountId = ' || COALESCE(v_numAccountID,0)  || ' 
												AND BST.bitIsActive = true 
												AND EXISTS (SELECT * FROM BankTransactionMapping BTM WHERE BTM.numTransactionID = BST.numTransactionID AND BTM.numReferenceID > 0 AND  BTM.tintReferenceType = 6 AND BTM.bitIsActive = true)

				UNION ALL
				SELECT 
					BST.dtDatePosted,
					BST.numTransactionID,
					BST.monAmount, 
					CD.monAmount  * -1 AS MAmount,
					BST.vcPayeeName || BST.vcMemo AS vcDescription,
					C.vcAccountName, 
					BTM.tintReferenceType,
					BTM.numReferenceID, 
					CAST(CAST(BST.dtDatePosted AS DATE) AS VARCHAR) || '' Expense ('' || COALESCE(C.vcAccountName,'''') || '') to '' || COALESCE(CI.vcCompanyname,''-'') || '' for $'' || CAST(ABS(BST.monAmount) AS VARCHAR) AS TransMatchDetails
				FROM BankStatementTransactions  BST 
				LEFT JOIN BankTransactionMapping BTM ON BST.numTransactionID = BTM.numTransactionID
				LEFT JOIN CheckHeader CH ON BTM.numReferenceID = CH.numCheckHeaderID
				LEFT JOIN CheckDetails CD ON CH.numCheckHeaderID = CD.numCheckHeaderID
				LEFT JOIN Chart_Of_Accounts C ON CD.numChartAcntId = C.numAccountId
				LEFT JOIN DivisionMaster D ON CD.numCustomerId	= D.numDivisionID
				LEFT JOIN companyinfo CI ON D.numCompanyID= CI.numCompanyID ';

	v_strSql := coalesce(v_strSql,'') || ' WHERE 
												BST.numAccountId = ' || SUBSTR(CAST(v_numAccountID AS VARCHAR(15)),1,15)  ||' 
												AND BST.bitIsActive = true 
												AND EXISTS (SELECT * FROM BankTransactionMapping BTM WHERE BTM.numTransactionID = BST.numTransactionID AND BTM.numreferenceId > 0 AND  BTM.tintReferenceType = 1 AND BTM.bitIsActive = true)';

	DROP TABLE IF EXISTS tt_TEMPTABLE CASCADE;
	Create TEMPORARY TABLE tt_TEMPTABLE 
	(
		dtDatePosted TIMESTAMP,
		numTransactionId NUMERIC(18,0),
		monAmount DECIMAL(20,5),
		MAmount DECIMAL(20,5),
		vcDescription VARCHAR(150),
		vcAccountName VARCHAR(50),
		tintReferenceType NUMERIC(18,0),
		numReferenceID NUMERIC(18,0),
		TransMatchDetails VARCHAR(150)
	); 
 
   EXECUTE 'INSERT into tt_TEMPTABLE ' || v_strSql;

	v_strSql1 := 'with  Paging
         AS(SELECT Row_number() OVER(ORDER BY dtDatePosted) AS row,* from tt_TEMPTABLE) select * from Paging where  row >' || COALESCE(v_firstRec,0) || ' and row < ' || COALESCE(v_lastRec,0); 

	OPEN SWV_RefCur FOR EXECUTE v_strSql1;

	SELECT COUNT(*) INTO v_TotRecs from tt_TEMPTABLE;

   RETURN;
END; $$;



