CREATE OR REPLACE FUNCTION usp_GetBizDocReport(                
  --                
v_numDomainID NUMERIC,                  
 v_intSortOrder NUMERIC,                  
 v_vcChooseBizDoc VARCHAR(30),                  
 v_numUserCntID NUMERIC DEFAULT 0,                                 
 v_intType NUMERIC DEFAULT 0,                
 v_tintRights SMALLINT DEFAULT 1, INOUT SWV_RefCur refcursor default null)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_strSQL  VARCHAR(8000);
BEGIN
   If v_tintRights = 1 then                
 --All Records Rights                  
 
      v_strSQL := 'select *,(X.GrandTotal -X.AmountPaid) as BalanceDue from(SELECT Opp.numOppId,div.tintCRMType,numOppBizDocsId,com.numCompanyId,div.numDivisionID,fn_GetPrimaryContact(div.numDivisionID) as ContactID,dtCreatedDate as DueDate,
 GetDealAmount(Opp.numOppId,TIMEZONE(''UTC'',now()),0) as GrandTotal,              
 coalesce(monAmountPaid,0) as AmountPaid,case when intPEstimatedCloseDate < TIMEZONE(''UTC'',now()) then NULL          
 when intPEstimatedCloseDate > TIMEZONE(''UTC'',now()) then DATE_PART(''day'',TIMEZONE(''UTC'',now()):: timestamp -intPEstimatedCloseDate) end as DaysPastdue,            
 vcCompanyName || '', '' || vcDivisionName as CustomerDivision,              
 vcBizDocID as BizDocID, '''' as BillingTerms ,numBizDocId             
 FROM OpportunityMaster Opp            
 join OpportunityBizDocs BizDocs    
 on Opp.numOppId = BizDocs.numOppId
 join DivisionMaster div          
 on div.numDivisionID = Opp.numDivisionId          
 join CompanyInfo com          
 on com.numCompanyId = div.numCompanyID            
 where Opp.numDomainID=' || COALESCE(v_numDomainID,0)::VARCHAR || ' AND BizDocs.numCreatedBy =' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15) || ') X Where 1 = 1 ';
      if v_vcChooseBizDoc <> '0' then 
         v_strSQL := coalesce(v_strSQL,'') || ' and X.numBizDocId =' || coalesce(v_vcChooseBizDoc,'');
      end if;
      if v_intSortOrder = 1 then  
         v_strSQL := coalesce(v_strSQL,'') || ' order by (X.GrandTotal -X.AmountPaid),X.DaysPastdue';
      end if;
      if v_intSortOrder = 2 then  
         v_strSQL := coalesce(v_strSQL,'') || ' order by X.DaysPastdue,(X.GrandTotal -X.AmountPaid)';
      end if;
      RAISE NOTICE '%',v_strSQL;
      OPEN SWV_RefCur FOR EXECUTE v_strSQL;
   end if;                  
                  
   If v_tintRights = 2 then
 
      v_strSQL := 'select * from (SELECT Opp.numOppid,div.tintCRMtype,numOppBizDocsId,com.numCompanyId,div.numDivisionID,dbo.fn_GetPrimaryContact(div.numDivisionID) as ContactID,dtCreatedDate as DueDate,monPAmount as GrandTotal,              
 isnull(monAmountPaid,0) as AmountPaid, (monPAmount-isnull(monAmountPaid,0)) as BalanceDue,              
 case when intPEstimatedCloseDate< TIMEZONE(''UTC'',now()) then NULL         
 when intPEstimatedCloseDate>TIMEZONE(''UTC'',now()) then DATE_PART(''day'',TIMEZONE(''UTC'',now()):: timestamp -intPEstimatedCloseDate) end as DaysPastdue,             
 vcCompanyName || '', '' || vcDivisionName as CustomerDivision,              
 vcBizDocID as BizDocID, '''' as BillingTerms ,numBizDocId             
 FROM OpportunityMaster Opp            
 join OpportunityBizDocs BizDocs        
 on Opp.numOppId = BizDocs.numOppId
 join DivisionMaster Div          
 on Div.numDivisionID=Opp.numDivisionID          
 join Companyinfo Com          
 on Com.numCompanyID=Div.numCompanyID 
 join AdditionalContactsInformation  ADC
 on ADC.numContactId= Opp.numRecOwner          
 where ADC.numTeam in(select F.numTeam from ForReportsByTeam F                     
 where Opp.numDomainID=' || COALESCE(v_numDomainID,0)::VARCHAR || ' AND F.numUserCntid=' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15) || ' and F.numDomainid=' || SUBSTR(CAST(v_numDomainID AS VARCHAR(15)),1,15) || ' and F.tintType=' || SUBSTR(CAST(v_intType AS VARCHAR(15)),1,15) || '))X Where 1=1 ';
      if v_vcChooseBizDoc <> '0' then 
         v_strSQL := coalesce(v_strSQL,'') || ' and X.numBizDocId=' || coalesce(v_vcChooseBizDoc,'');
      end if;
      if v_intSortOrder = 1 then  
         v_strSQL := coalesce(v_strSQL,'') || ' order by X.BalanceDue,X.DaysPastdue';
      end if;
      if v_intSortOrder = 2 then  
         v_strSQL := coalesce(v_strSQL,'') || ' order by X.DaysPastdue,X.BalanceDue';
      end if;
      RAISE NOTICE '%',v_strSQL;
      OPEN SWV_RefCur FOR EXECUTE v_strSQL;
   end if;                  

                  
   If v_tintRights = 3 then
 
      v_strSQL := 'select * from (SELECT Opp.numOppid,div.tintCRMtype,numOppBizDocsId,com.numCompanyId,div.numDivisionID,dbo.fn_GetPrimaryContact(div.numDivisionID) as ContactID,dtCreatedDate as DueDate,monPAmount as GrandTotal,              
 isnull(monAmountPaid,0) as AmountPaid, (monPAmount-isnull(monAmountPaid,0)) as BalanceDue,           
 case when intPEstimatedCloseDate<TIMEZONE(''UTC'',now()) then NULL           
 when intPEstimatedCloseDate>TIMEZONE(''UTC'',now()) then DATE_PART(''day'',TIMEZONE(''UTC'',now()):: timestamp -intPEstimatedCloseDate) end as DaysPastdue,             
 vcCompanyName||'', ''|| vcDivisionName as CustomerDivision,              
 vcBizDocID as BizDocID, '''' BillingTerms ,numBizDocId             
 FROM OpportunityMaster Opp   
 JOIN OpportunityBizDocs BizDocs                  
 on Opp.numOppId = BizDocs.numOppId
 join DivisionMaster Div          
 on Div.numDivisionID=Opp.numDivisionID          
 join Companyinfo Com          
 on Com.numCompanyID=Div.numCompanyID            
where Opp.numDomainID=' || COALESCE(v_numDomainID,0)::VARCHAR || ' AND Div.numTerId in(select F.numTerritory from ForReportsByTerritory F                     
     where F.numUserCntid=' || SUBSTR(CAST(v_numUserCntID AS VARCHAR(15)),1,15) || ' and F.numDomainid=' || SUBSTR(CAST(v_numDomainID AS VARCHAR(15)),1,15) || ' and F.tintType=' || SUBSTR(CAST(v_intType AS VARCHAR(15)),1,15) || '))X Where 1=1 ';
      if v_vcChooseBizDoc <> '0' then 
         v_strSQL := coalesce(v_strSQL,'') || ' and X.numBizDocId=' || coalesce(v_vcChooseBizDoc,'');
      end if;
      if v_intSortOrder = 1 then  
         v_strSQL := coalesce(v_strSQL,'') || ' order by X.BalanceDue,X.DaysPastdue';
      end if;
      if v_intSortOrder = 2 then  
         v_strSQL := coalesce(v_strSQL,'') || ' order by X.DaysPastdue,X.BalanceDue';
      end if;
      RAISE NOTICE '%',v_strSQL;
      OPEN SWV_RefCur FOR EXECUTE v_strSQL;
   end if;
   RETURN;
END; $$;


