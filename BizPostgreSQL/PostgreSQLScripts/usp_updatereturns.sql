-- Stored procedure definition script Usp_UpdateReturns for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION Usp_UpdateReturns(v_numReturnID NUMERIC(9,0), 
--@numQtyReturned NUMERIC(9),
v_numReturnStatus NUMERIC(9,0),
v_numModifiedBy NUMERIC(9,0),
v_vcReferencePO VARCHAR(50) DEFAULT '')
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   UPDATE
   Returns
   SET 
      --[numQtyReturned] = @numQtyReturned,
   numreturnstatus = v_numReturnStatus,numModifiedBy = v_numModifiedBy,dtModifiedDate = LOCALTIMESTAMP,
   vcReferencePO = v_vcReferencePO
   WHERE
   numReturnID = v_numReturnID;
   RETURN;
END; $$;


