-- Stored procedure definition script USP_GetAcivitybyActId for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_GetAcivitybyActId(v_ActivityId NUMERIC,v_mode BOOLEAN, INOUT SWV_RefCur refcursor)
LANGUAGE plpgsql
   AS $$
BEGIN
   if v_mode = false then

      open SWV_RefCur for
      SELECT
      coalesce(Activity.AllDayEvent,false) as AllDayEvent,
  Activity.ActivityDescription,
  Activity.Duration,
  Activity.Location,
  Activity.ActivityID,
  Activity.StartDateTimeUtc,
  Activity.Subject,
  Activity.EnableReminder,
  Activity.ReminderInterval,
  Activity.ShowTimeAs,
  Activity.Importance,
  Activity.Status,
  Activity.RecurrenceID,
  Activity.VarianceID,
ActivityResource.ResourceID,
Activity.itemId,
Activity.ChangeKey,
Activity.ItemIDOccur,
  Activity._ts,Activity.GoogleEventId,ACI.numContactId,Div.numDomainID,coalesce(Activity.Color,0) as Color
      FROM
      Activity INNER JOIN ActivityResource ON Activity.ActivityID = ActivityResource.ActivityID
      join Resource res on ActivityResource.ResourceID = res.ResourceID  JOIN
      AdditionalContactsInformation ACI on ACI.numContactId = res.numUserCntId JOIN DivisionMaster Div
      ON ACI.numDivisionId = Div.numDivisionID
      WHERE
      Activity.ActivityID = v_ActivityId and  Activity.OriginalStartDateTimeUtc IS NULL;
   else
      open SWV_RefCur for
      SELECT
      coalesce(Activity.AllDayEvent,false) as AllDayEvent,
  Activity.ActivityDescription,
  Activity.Duration,
  Activity.Location,
  Activity.ActivityID,
  Activity.StartDateTimeUtc,
  Activity.Subject,
  Activity.EnableReminder,
  Activity.ReminderInterval,
  Activity.ShowTimeAs,
  Activity.Importance,
  Activity.Status,
  Activity.RecurrenceID,
  Activity.VarianceID,
ActivityResource.ResourceID,
Activity.itemId,
Activity.ChangeKey,
Activity.ItemIDOccur,
  Activity._ts,Activity.GoogleEventId,ACI.numContactId,Div.numDomainID,coalesce(Activity.Color,0) as Color,
R.EndDateUtc,R.DayOfWeekMaskUtc,R.UtcOffset,R.DayOfMonth,R.MonthOfYear,R.PeriodMultiple,R.Period
      FROM
      Activity INNER JOIN ActivityResource ON Activity.ActivityID = ActivityResource.ActivityID
      join Resource res on ActivityResource.ResourceID = res.ResourceID
      JOIN AdditionalContactsInformation ACI on ACI.numContactId = res.numUserCntId
      JOIN DivisionMaster Div ON ACI.numDivisionId = Div.numDivisionID
      left outer join Recurrence R on  Activity.RecurrenceID = R.recurrenceid
      WHERE
      Activity.ActivityID = v_ActivityId;
   end if;
   RETURN;
END; $$;


