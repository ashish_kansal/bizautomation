-- Function definition script GetTotalExpense for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION GetTotalExpense(v_numUserCntID NUMERIC,v_numDomainId NUMERIC,v_dtStartDate TIMESTAMP ,v_dtEndDate TIMESTAMP)
RETURNS DECIMAL(10,2) LANGUAGE plpgsql
   AS $$
   DECLARE
   v_Expense  DECIMAL;
BEGIN
   v_Expense := 0;          
      
   select   v_Expense+coalesce((Sum(Cast(monAmount as DOUBLE PRECISION))),0) INTO v_Expense from timeandexpense Where numCategory = 2 And numtype in(1,2) And numUserCntID = v_numUserCntID And numDomainID = v_numDomainId And
--and		 dtTCreatedOn between   @dtStartDate and    @dtEndDate              
(dtFromDate between v_dtStartDate And v_dtEndDate Or dtToDate between v_dtStartDate And v_dtEndDate);     
     
   Return v_Expense;
END; $$;

