-- Function definition script GetProjectStage for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION GetProjectStage(v_numProid NUMERIC(9,0),v_numUsercntId NUMERIC(9,0),v_numDomainId NUMERIC(9,0))
RETURNS VARCHAR(4000) LANGUAGE plpgsql
   AS $$
   DECLARE
   v_stage  VARCHAR(4000);
BEGIN
   v_stage := '';
   select   case when v_stage = '' then vcStageDetail else
      coalesce(v_stage,'') || '<br>' || vcStageDetail
   end INTO v_stage from ProjectsStageDetails PSd
   join  ProjectsMaster pro    on pro.numProId = PSd.numProId where PSd.numProId = v_numProid and  bitStageCompleted = false
   and PSd.numAssignTo = v_numUsercntId and pro.numdomainId = v_numDomainId;
   return v_stage;
END; $$;

