-- Stored procedure definition script USP_ManageUsersForRept for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_ManageUsersForRept(v_numUserCntId NUMERIC(9,0),
    v_numDomainId NUMERIC(9,0),
    v_tintType SMALLINT,
    v_strUser VARCHAR(4000),
    v_tintUserType SMALLINT)
RETURNS VOID LANGUAGE plpgsql
   AS $$
   DECLARE
   v_separator_position  INTEGER;    
   v_strPosition  VARCHAR(1000);
BEGIN
   DELETE  FROM ForReportsByUser
   WHERE   numContactID = v_numUserCntId
   AND numDomainID = v_numDomainId
   AND tintType = v_tintType;    
     
    
   WHILE coalesce(POSITION(substring(v_strUser from E'\\,') IN v_strUser),0) <> 0 LOOP -- Begin for While Loop    
      v_separator_position := coalesce(POSITION(substring(v_strUser from E'\\,') IN v_strUser),0);
      v_strPosition := SUBSTR(v_strUser,1,v_separator_position -1);
      v_strUser := OVERLAY(v_strUser placing '' from 1 for v_separator_position);
      INSERT  INTO ForReportsByUser(numContactID,
                                             numSelectedUserCntID,
                                             numDomainID,
                                             tintType,tintUserType)
        VALUES(v_numUserCntId,
                  CAST(v_strPosition AS NUMERIC),
                  v_numDomainId,
                  v_tintType,
                  v_tintUserType);
   END LOOP;
   RETURN;
END; $$;


