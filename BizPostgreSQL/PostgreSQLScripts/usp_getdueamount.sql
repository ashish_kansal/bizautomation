-- Stored procedure definition script USP_GetDueAmount for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetDueAmount(v_numDivisionID NUMERIC(9,0) DEFAULT 0,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_Amt  DECIMAL(20,5);                        
   v_Paid  DECIMAL(20,5);      
                 
   v_AmountDueSO  DECIMAL(20,5);                    
   v_AmountPastDueSO  DECIMAL(20,5); 

   v_AmountDuePO  DECIMAL(20,5);                    
   v_AmountPastDuePO  DECIMAL(20,5); 
                   
   v_CompanyCredit  DECIMAL(20,5);   
          
   v_PCreditMemo  DECIMAL(20,5);            
   v_SCreditMemo  DECIMAL(20,5);            

   v_UTCtime  TIMESTAMP;	       
   v_vcShippersAccountNo  VARCHAR(100);
   v_numDefaultShippingServiceID  NUMERIC(18,0);
   v_intShippingCompany  NUMERIC(18,0);
   v_numPartnerSource  NUMERIC(18,0);
   v_bitAutoCheckCustomerPart  BOOLEAN;
BEGIN
   v_Amt := 0;                        
   v_Paid := 0;      
                 
   v_AmountDueSO := 0;                    
   v_AmountPastDueSO := 0; 

   v_AmountDuePO := 0;                    
   v_AmountPastDuePO := 0; 
                   
   v_CompanyCredit := 0;   
          
   v_PCreditMemo := 0;            
   v_SCreditMemo := 0;            

   v_UTCtime := GetUTCDateWithoutTime();

--Sales Order Total Amount Due                
   select   coalesce(sum(OppBD.monDealAmount),0) INTO v_Amt from OpportunityMaster  Opp
   join  OpportunityBizDocs OppBD
   on    OppBD.numoppid = Opp.numOppId where bitAuthoritativeBizDocs = 1 and numDivisionId = v_numDivisionID and tintoppstatus = 1   and tintopptype = 1;              
               
   select   coalesce(sum(monAmountPaid),0) INTO v_Paid from OpportunityMaster  Opp
   join  OpportunityBizDocs OppBD
   on    OppBD.numoppid = Opp.numOppId where bitAuthoritativeBizDocs = 1 and numDivisionId = v_numDivisionID and tintoppstatus = 1   and tintopptype = 1;               
                
   v_AmountDueSO := v_Amt -v_Paid;                
         
	IF TRUNC(v_AmountDueSO,2) = 0 THEN
		v_AmountDueSO := 0;
	END IF;

--Sales Order Total Amount Past Due                
   select   coalesce(sum(OppBD.monDealAmount),0) INTO v_Amt from OpportunityMaster   Opp
   join  OpportunityBizDocs OppBD
   on    OppBD.numoppid = Opp.numOppId where  bitAuthoritativeBizDocs = 1 and numDivisionId = v_numDivisionID and tintoppstatus = 1   and tintopptype = 1
   AND OppBD.dtFromDate+CAST(CASE WHEN Opp.bitBillingTerms = true
   THEN CAST(coalesce((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = coalesce(Opp.intBillingDays,0)),0) AS INTEGER)
   ELSE 0
   END || 'day' as interval) < v_UTCtime;
--AND DATEADD(day,CASE WHEN Opp.bitBillingTerms = 1 
--					 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(Opp.intBillingDays,0)),0))  
--					 ELSE 0 
--				END, OppBD.[dtFromDate]) < @UTCtime				

   select   coalesce(SUM(OppBD.monAmountPaid),0) INTO v_Paid from OpportunityMaster  Opp
   join  OpportunityBizDocs OppBD on    OppBD.numoppid = Opp.numOppId where  OppBD.bitAuthoritativeBizDocs = 1 and Opp.numDivisionId = v_numDivisionID and tintoppstatus = 1  and tintopptype = 1
   AND OppBD.dtFromDate+CAST(CASE WHEN Opp.bitBillingTerms = true
   THEN CAST(coalesce((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = coalesce(Opp.intBillingDays,0)),0) AS INTEGER)
   ELSE 0
   END || 'day' as interval) < v_UTCtime;
--AND DATEADD(day,CASE WHEN Opp.bitBillingTerms = 1 
--					 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(Opp.intBillingDays,0)),0)) 
--					 ELSE 0 
--				END, OppBD.[dtFromDate]) <@UTCtime				

   v_AmountPastDueSO := v_Amt -v_Paid;                    
                    
	IF TRUNC(v_AmountPastDueSO,2) = 0 THEN
		v_AmountPastDueSO := 0;
	END IF;

--Purchase Order Total Amount Due                
   select   coalesce(sum(OppBD.monDealAmount),0) INTO v_Amt from OpportunityMaster  Opp
   join  OpportunityBizDocs OppBD
   on    OppBD.numoppid = Opp.numOppId where bitAuthoritativeBizDocs = 1 and numDivisionId = v_numDivisionID and tintoppstatus = 1   and tintopptype = 2;              
               
   select   coalesce(sum(monAmountPaid),0) INTO v_Paid from OpportunityMaster  Opp
   join  OpportunityBizDocs OppBD
   on    OppBD.numoppid = Opp.numOppId where bitAuthoritativeBizDocs = 1 and numDivisionId = v_numDivisionID and tintoppstatus = 1   and tintopptype = 2;              
                
   v_AmountDuePO := v_Amt -v_Paid;      
   
	IF TRUNC(v_AmountDuePO,2) = 0 THEN
		v_AmountDuePO := 0;
	END IF;
         
--Purchase Order Total Amount Past Due                
   select   coalesce(sum(OppBD.monDealAmount),0) INTO v_Amt from OpportunityMaster   Opp
   join  OpportunityBizDocs OppBD
   on    OppBD.numoppid = Opp.numOppId where  bitAuthoritativeBizDocs = 1 and numDivisionId = v_numDivisionID and tintoppstatus = 1   and tintopptype = 2
   AND OppBD.dtFromDate+CAST(CASE WHEN Opp.bitBillingTerms = true
   THEN CAST(coalesce((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = coalesce(Opp.intBillingDays,0)),0) AS INTEGER)
   ELSE 0
   END || 'day' as interval) < v_UTCtime;
--AND DATEADD(day,CASE WHEN Opp.bitBillingTerms = 1 
--					 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(Opp.intBillingDays,0)),0))  
--					 ELSE 0 
--				END, OppBD.[dtFromDate]) <@UTCtime
				

   select   coalesce(SUM(OppBD.monAmountPaid),0) INTO v_Paid from OpportunityMaster  Opp
   join  OpportunityBizDocs OppBD on    OppBD.numoppid = Opp.numOppId where  OppBD.bitAuthoritativeBizDocs = 1 and Opp.numDivisionId = v_numDivisionID and tintoppstatus = 1  and tintopptype = 2
   AND OppBD.dtFromDate+CAST(CASE WHEN Opp.bitBillingTerms = true
   THEN CAST(coalesce((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = coalesce(Opp.intBillingDays,0)),0) AS INTEGER)
   ELSE 0
   END || 'day' as interval) < v_UTCtime;
--AND DATEADD(day,CASE WHEN Opp.bitBillingTerms = 1 
--					 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(Opp.intBillingDays,0)),0)) 
--					 ELSE 0 
--				END, OppBD.[dtFromDate]) <@UTCtime

   v_AmountPastDuePO := v_Amt -v_Paid;  

	IF TRUNC(v_AmountPastDuePO,2) = 0 THEN
		v_AmountPastDuePO := 0;
	END IF;

--Company Cresit Limit                    
   select   CAST(case when SWF_IsNumeric(fn_GetListItemName(numCompanyCredit)) = true then cast(REPLACE(fn_GetListItemName(numCompanyCredit),',','') as INTEGER) else 0 end AS DECIMAL(20,5)) INTO v_CompanyCredit from CompanyInfo C
   join DivisionMaster d
   on d.numCompanyID = C.numCompanyId where d.numDivisionID = v_numDivisionID;                    
                    
 
--Purchase Credit Memo
   select   coalesce(BPH.monPaymentAmount,0) -coalesce(BPH.monAppliedAmount,0) INTO v_PCreditMemo from BillPaymentHeader BPH where BPH.numDivisionId = v_numDivisionID AND coalesce(numReturnHeaderID,0) > 0;                  

--Sales Credit Memo
   select   coalesce(DM.monDepositAmount,0) -coalesce(DM.monAppliedAmount,0) INTO v_SCreditMemo from DepositMaster DM where DM.numDivisionID = v_numDivisionID AND tintDepositePage = 3 AND coalesce(numReturnHeaderID,0) > 0;  

   select   coalesce(vcShippersAccountNo,''), coalesce(numDefaultShippingServiceID,0), coalesce(intShippingCompany,0), coalesce(numPartenerSource,0), coalesce(bitAutoCheckCustomerPart,false) INTO v_vcShippersAccountNo,v_numDefaultShippingServiceID,v_intShippingCompany,
   v_numPartnerSource,v_bitAutoCheckCustomerPart FROM DivisionMaster WHERE numDivisionID = v_numDivisionID;

   open SWV_RefCur for select cast(coalesce(v_AmountDueSO,0) as DECIMAL(20,5)) as AmountDueSO,cast(coalesce(v_AmountPastDueSO,0) as DECIMAL(20,5)) as AmountPastDueSO,
cast(coalesce(v_AmountDuePO,0) as DECIMAL(20,5)) as AmountDuePO,cast(coalesce(v_AmountPastDuePO,0) as DECIMAL(20,5)) as AmountPastDuePO,
coalesce(v_CompanyCredit,0) -(coalesce(v_AmountDueSO,0)+coalesce(v_AmountDuePO,0)) as RemainingCredit,
cast(coalesce(v_PCreditMemo,0) as DECIMAL(20,5)) as PCreditMemo,
cast(coalesce(v_SCreditMemo,0) as DECIMAL(20,5)) as SCreditMemo,
cast(coalesce(v_CompanyCredit,0) as DECIMAL(20,5)) AS CreditLimit,
v_vcShippersAccountNo AS vcShippersAccountNo,
(CASE WHEN(SELECT COUNT(*) FROM DivisionMasterShippingAccount WHERE numDivisionID = v_numDivisionID) > 0 THEN true ELSE false END) AS bitUseShippersAccountNo,
v_numDefaultShippingServiceID AS numDefaultShippingServiceID,
v_intShippingCompany AS intShippingCompany,
v_numPartnerSource AS numPartnerSource,
v_bitAutoCheckCustomerPart AS bitAutoCheckCustomerPart;
END; $$;













