-- Stored procedure definition script USP_GetActualLeadSource for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetActualLeadSource(v_numDomainId		NUMERIC(9,0),
	v_FromDate	TIMESTAMP,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT GetDomainNameFromURL(TV.vcReferrer) AS SourceDomain,
COUNT(*) AS TotalCount,
COUNT(CASE WHEN (DM.tintCRMType = 0 or DM.tintCRMType = 1)  THEN DM.numDivisionID END) AS LeadsCount,
COUNT(CASE WHEN DM.tintCRMType = 2 THEN DM.numDivisionID END) AS AccountsCount,
COUNT(CASE WHEN DM.numDivisionID = OM.numDivisionId THEN OM.numOppId END) AS OrderCount,
cast(coalesce(SUM(CASE WHEN DM.numDivisionID = OM.numDivisionId THEN OM.monDealAmount END),0) as DECIMAL(20,5)) AS AmountGenerated
   FROM TrackingVisitorsHDR TV
   INNER JOIN DivisionMaster DM ON TV.numDivisionID = DM.numDivisionID
   LEFT JOIN OpportunityMaster OM ON DM.numDivisionID = OM.numDivisionId
   WHERE TV.numDomainID = v_numDomainId
   AND DM.bitActiveInActive = true
   AND TV.dtcreated > v_FromDate
   AND TV.vcReferrer IS NOT NULL
   AND TV.vcReferrer <> ''
   GROUP BY GetDomainNameFromURL(vcReferrer)
   ORDER BY LeadsCount DESC;
END; $$;












