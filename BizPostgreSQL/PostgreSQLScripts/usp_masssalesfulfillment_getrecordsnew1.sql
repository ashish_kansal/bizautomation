-- Stored procedure definition script USP_MassSalesFulfillment_GetRecordsNew1 for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_MassSalesFulfillment_GetRecordsNew1(v_numDomainID NUMERIC(18,0)
	,v_numUserCntID NUMERIC(18,0)
	,v_ClientTimeZoneOffset INTEGER
	,v_numViewID NUMERIC(18,0)
	,v_numWarehouseID NUMERIC(18,0)
	,v_bitGroupByOrder BOOLEAN
	,v_bitRemoveBO BOOLEAN
	,v_numShippingZone NUMERIC(18,0)
	,v_bitIncludeSearch BOOLEAN
	,v_vcOrderStauts TEXT
	,v_tintFilterBy SMALLINT
	,v_vcFilterValue TEXT
	,v_vcCustomSearchValue TEXT
	,v_numBatchID NUMERIC(18,0)
	,v_tintPackingViewMode SMALLINT
	,v_tintPrintBizDocViewMode SMALLINT
	,v_tintPendingCloseFilter SMALLINT
	,v_numPageIndex INTEGER
	,INOUT v_vcSortColumn VARCHAR(100) 
	,INOUT v_vcSortOrder VARCHAR(4) 
	,INOUT v_numTotalRecords NUMERIC(18,0) , INOUT SWV_RefCur refcursor)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_tintPackingMode  SMALLINT;
   v_tintInvoicingType  SMALLINT;
   v_tintCommitAllocation  SMALLINT;
   v_bitEnablePickListMapping  BOOLEAN;
   v_bitEnableFulfillmentBizDocMapping  BOOLEAN;
   v_numShippingServiceItemID  NUMERIC(18,0);
   v_numDefaultSalesShippingDoc  NUMERIC(18,0);
   v_vcSelect  TEXT DEFAULT CONCAT('SELECT 
												COUNT(*) OVER() AS TotalRecords
												,OpportunityMaster.numOppId
												,OpportunityMaster.dtExpectedDate AS dtExpectedDateOrig
												,DivisionMaster.numDivisionID
												,DivisionMaster.numTerID
												,ISNULL(DivisionMaster.intShippingCompany,0) AS numPreferredShipVia
												,ISNULL(DivisionMaster.numDefaultShippingServiceID,0) numPreferredShipService
												,OpportunityBizDocs.numOppBizDocsId AS numOppBizDocID
												,OpportunityBizDocs.vcBizDocID
												,ISNULL(OpportunityBizDocs.monDealAmount,0) - ISNULL(OpportunityBizDocs.monAmountPaid,0) AS monAmountToPay
												,',(CASE WHEN coalesce(v_bitGroupByOrder,false) = true THEN '0' ELSE 'OpportunityItems.numoppitemtCode' END),' AS numOppItemID
												',
   (CASE WHEN coalesce(v_bitGroupByOrder,false) = true
   THEN ',0 AS numRemainingQty'
   ELSE ',(CASE 
															WHEN @numViewID = 1
															THEN ISNULL(OpportunityItems.numUnitHour,0) - ISNULL(OpportunityItems.numQtyPicked,0)
															WHEN @numViewID = 2 AND @tintPackingViewMode = 2
															THEN (CASE WHEN ISNULL(@tintPackingMode,1) = 2 THEN ISNULL(OpportunityItems.numQtyPicked,0) - ISNULL(OpportunityItems.numQtyShipped,0) ELSE ISNULL(OpportunityItems.numUnitHour,0) - ISNULL(OpportunityItems.numQtyShipped,0) END)
															WHEN @numViewID = 2 AND @tintPackingViewMode = 3 AND ISNULL(TEMPOrder.numOppBizDocID,0) = 0
															THEN (CASE WHEN ISNULL(@tintPackingMode,1) = 2 THEN ISNULL(OpportunityItems.numQtyPicked,0) ELSE ISNULL(OpportunityItems.numUnitHour,0) END) - ISNULL((SELECT 
																																																						SUM(OpportunityBizDocItems.numUnitHour)
																																																					FROM
																																																						OpportunityBizDocs
																																																					INNER JOIN
																																																						OpportunityBizDocItems
																																																					ON
																																																						OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
																																																					WHERE
																																																						OpportunityBizDocs.numOppId = OpportunityMaster.numOppID
																																																						AND OpportunityBizDocs.numBizDocId=@numDefaultSalesShippingDoc
					 																																																AND OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtcode),0)
															WHEN @numViewID = 2 AND @tintPackingViewMode = 3 AND ISNULL(OpportunityBizDocs.numOppBizDocsId,0) > 0
															THEN ISNULL(OpportunityBizDocItems.numUnitHour,0)
															WHEN @numViewID = 3
															THEN (CASE 
																	WHEN ISNULL(@tintInvoicingType,0)=1 
																	THEN ISNULL(OpportunityItems.numQtyShipped,0) 
																	ELSE ISNULL(OpportunityItems.numUnitHour,0) 
																END) - ISNULL((SELECT 
																					SUM(OpportunityBizDocItems.numUnitHour)
																				FROM
																					OpportunityBizDocs
																				INNER JOIN
																					OpportunityBizDocItems
																				ON
																					OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
																				WHERE
																					OpportunityBizDocs.numOppId = OpportunityMaster.numOppID
																					AND OpportunityBizDocs.numBizDocId=287
																					AND OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtcode),0)
															ELSE 0
														END) numRemainingQty'
   END),
   ','''' dtAnticipatedDelivery,'''' vcShipStatus,'''' vcPaymentStatus,0 numShipRate
													,(CASE 
														WHEN @numViewID = 2 AND @tintPackingViewMode = 2 AND ISNULL(@bitEnablePickListMapping,0) = 1 
														THEN CONCAT(''['',STUFF((SELECT 
																	CONCAT('', {"numOppBizDocsId":'',OB.numOppBizDocsId,'', "vcBizDocName":"'',OB.vcBizDocID,'' '',ISNULL(BT.vcTemplateName,''''),''"}'') 
																FROM 
																	OpportunityBizDocs OB
																LEFT JOIN
																	BizDocTemplate AS BT
																ON
																	OB.numBizDocTempID=BT.numBizDocTempID
																WHERE 
																	OB.numOppId = OpportunityMaster.numOppID
																	AND OB.numBizDocId=29397
																ORDER BY
																	OB.numOppBizDocsId
																FOR XML PATH('''')),1,1,''''),'']'')
														WHEN @numViewID = 3 AND ISNULL(@bitEnableFulfillmentBizDocMapping,0) = 1 
														THEN  CONCAT(''['',STUFF((SELECT 
																	CONCAT('', {"numOppBizDocsId":'',OB.numOppBizDocsId,'', "vcBizDocName":"'',OB.vcBizDocID,'' '',ISNULL(BT.vcTemplateName,''''),''"}'') 
																FROM 
																	OpportunityBizDocs OB
																LEFT JOIN
																	BizDocTemplate AS BT
																ON
																	OB.numBizDocTempID=BT.numBizDocTempID
																WHERE 
																	OB.numOppId = OpportunityMaster.numOppID
																	AND OB.numBizDocId IN (29397,296)
																ORDER BY
																	OB.numOppBizDocsId
																FOR XML PATH('''')),1,1,''''),'']'')
														ELSE ''''
													END) vcLinkingBizDocs');
   v_vcFrom  TEXT DEFAULT ' FROM
										OpportunityMaster
									INNER JOIN
										DivisionMaster
									ON
										OpportunityMaster.numDivisionId = DivisionMaster.numDivisionID
									INNER JOIN
										CompanyInfo
									ON
										DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
									INNER JOIN
										OpportunityItems
									ON
										OpportunityMaster.numOppId = OpportunityItems.numOppId
									LEFT JOIN 
										MassSalesFulfillmentBatchOrders
									ON
										MassSalesFulfillmentBatchOrders.numBatchID = @numBatchID
										AND OpportunityMaster.numOppId = MassSalesFulfillmentBatchOrders.numOppID
										AND (OpportunityItems.numOppItemtCode = MassSalesFulfillmentBatchOrders.numOppItemID OR ISNULL(MassSalesFulfillmentBatchOrders.numOppItemID,0) = 0)
									LEFT JOIN
										WareHouseItems
									ON
										OpportunityItems.numWarehouseItmsID = WareHouseItems.numWareHouseItemID
									LEFT JOIN
										WarehouseLocation
									ON
										WareHouseItems.numWLocationID = WarehouseLocation.numWLocationID
									INNER JOIN
										Item
									ON
										OpportunityItems.numItemCode = Item.numItemCode
									LEFT JOIN 
										OpportunityBizDocs
									ON
										OpportunityMaster.numOppId = OpportunityBizDocs.numOppId
										AND OpportunityBizDocs.numBizDocID= (CASE 
																				WHEN @numViewID=4 THEN 287 
																				WHEN @numViewID=6 AND @tintPrintBizDocViewMode=1 THEN 55206 
																				WHEN @numViewID=6 AND @tintPrintBizDocViewMode=2 THEN 29397 
																				WHEN @numViewID=6 AND @tintPrintBizDocViewMode=3 THEN 296 
																				WHEN @numViewID=6 AND @tintPrintBizDocViewMode=4 THEN 287 
																			END)
										AND 1 = (CASE WHEN @numViewID IN (4,6) THEN 1 ELSE 0 END)';

   v_vcWhere  TEXT DEFAULT CONCAT(' WHERE
												OpportunityMaster.numDomainId=@numDomainID
												AND OpportunityMaster.tintOppType = 1
												AND OpportunityMaster.tintOppStatus = 1
												AND 1 = (CASE 
															WHEN @numViewID=6 AND @tintPendingCloseFilter=1 THEN (CASE WHEN ISNULL(OpportunityMaster.tintshipped,0) = 0 THEN 1 ELSE 0 END)
															WHEN @numViewID=6 AND @tintPendingCloseFilter=2 THEN (CASE WHEN ISNULL(OpportunityMaster.tintshipped,0) = 1 THEN 1 ELSE 0 END)
															WHEN @numViewID=6 AND @tintPendingCloseFilter=3 THEN ''''
															ELSE (CASE WHEN ISNULL(OpportunityMaster.tintshipped,0) = 0 THEN 1 ELSE 0 END)
														END)
												AND 1 = (CASE WHEN @numViewID = 2 AND @tintPackingViewMode = 3 THEN (CASE WHEN ISNULL(Item.bitContainer,0)=1 THEN 0 ELSE 1 END) ELSE 1 END)
												AND 1 = (CASE WHEN @numViewID <> 6 AND ISNULL(@numWarehouseID,0) > 0 AND ISNULL(OpportunityItems.numWarehouseItmsID,0) > 0 THEN (CASE WHEN @numWarehouseID=WareHouseItems.numWarehouseID THEN 1 ELSE 0 END) ELSE 1 END)
												AND 1 = (CASE 
															WHEN @numViewID IN (1,2) AND ISNULL(@bitRemoveBO,0) = 1
															THEN (CASE 
																		WHEN (Item.charItemType = ''p'' AND ISNULL(OpportunityItems.bitDropShip, 0) = 0 AND ISNULL(Item.bitAsset,0)=0)
																		THEN 
																			(CASE 
																				WHEN dbo.CheckOrderItemInventoryStatus(OpportunityItems.numItemCode,(CASE 
																																						WHEN @numViewID = 1
																																						THEN ISNULL(OpportunityItems.numUnitHour,0) - ISNULL(OpportunityItems.numQtyPicked,0)
																																						WHEN @numViewID = 2 AND @tintPackingViewMode = 2
																																						THEN (CASE WHEN ISNULL(@tintPackingMode,1) = 2 THEN ISNULL(OpportunityItems.numQtyPicked,0) - ISNULL(OpportunityItems.numQtyShipped,0) ELSE ISNULL(OpportunityItems.numUnitHour,0) - ISNULL(OpportunityItems.numQtyShipped,0) END)
																																						WHEN @numViewID = 2 AND @tintPackingViewMode = 3
																																						THEN (CASE WHEN ISNULL(@tintPackingMode,1) = 2 THEN ISNULL(OpportunityItems.numQtyPicked,0) ELSE ISNULL(OpportunityItems.numUnitHour,0) END) - ISNULL((SELECT 
																																																																													SUM(OpportunityBizDocItems.numUnitHour)
																																																																												FROM
																																																																													OpportunityBizDocs
																																																																												INNER JOIN
																																																																													OpportunityBizDocItems
																																																																												ON
																																																																													OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
																																																																												WHERE
																																																																													OpportunityBizDocs.numOppId = OpportunityMaster.numOppID
																																																																													AND OpportunityBizDocs.numBizDocId=@numDefaultSalesShippingDoc
					 																																																																							AND OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtcode),0)
				
																																						ELSE OpportunityItems.numUnitHour
																																					END),OpportunityItems.numoppitemtCode,OpportunityItems.numWarehouseItmsID,(CASE WHEN ISNULL(Item.bitKitParent,0) = 1 THEN 1 ELSE 0 END)) > 0 THEN 0 ELSE 1 END)
																							ELSE 1
																						END) 
																					ELSE 1
																				END)
											
												AND 1 = (CASE 
															WHEN LEN(ISNULL(@vcOrderStauts,'''')) > 0 
															THEN (CASE 
																	WHEN ISNULL(@bitIncludeSearch,0) = 0 
																	THEN (CASE WHEN OpportunityMaster.numStatus NOT IN (SELECT Id FROM dbo.SplitIDs(@vcOrderStauts,'','')) THEN 1 ELSE 0 END) 
																	ELSE (CASE WHEN OpportunityMaster.numStatus IN (SELECT Id FROM dbo.SplitIDs(@vcOrderStauts,'','')) THEN 1 ELSE 0 END) 
																END)
															ELSE 1 
														END)
												AND 1 = (CASE 
															WHEN LEN(ISNULL(@vcFilterValue,'''')) > 0
															THEN
																CASE 
																	WHEN @tintFilterBy=1 --Item Classification
																	THEN (CASE 
																			WHEN ISNULL(@bitIncludeSearch,0) = 0 
																			THEN (CASE WHEN Item.numItemClassification NOT IN (SELECT Id FROM dbo.SplitIDs(@vcFilterValue,'','')) THEN 1 ELSE 0 END) 
																			ELSE (CASE WHEN Item.numItemClassification IN (SELECT Id FROM dbo.SplitIDs(@vcFilterValue,'','')) THEN 1 ELSE 0 END) 
																		END)
																	ELSE 1
																END
															ELSE 1
														END)

												AND 1 = (CASE
															WHEN @numViewID = 1 THEN (CASE WHEN ISNULL(OpportunityItems.numUnitHour,0) - ISNULL(OpportunityItems.numQtyPicked,0) > 0 THEN 1 ELSE 0 END)
															WHEN @numViewID = 2 THEN (CASE 
																							WHEN @tintPackingViewMode = 1 
																							THEN (CASE 
																									WHEN ISNULL((SELECT COUNT(*) FROM OpportunityItems OIInner WHERE OIInner.numOppID=OpportunityMaster.numOppId AND OIInner.numItemCode=ISNULL(@numShippingServiceItemID,0)),0) = 0 
																									THEN 1 
																									ELSE 0 
																								END)
																							WHEN @tintPackingViewMode = 2
																							THEN (CASE 
																									WHEN (CASE WHEN ISNULL(@tintPackingMode,1) = 2 THEN ISNULL(OpportunityItems.numQtyPicked,0) ELSE ISNULL(OpportunityItems.numUnitHour,0) END) - ISNULL(OpportunityItems.numQtyShipped,0) > 0
																									THEN 1 
																									ELSE 0 
																								END)
																							WHEN @tintPackingViewMode = 3
																							THEN (CASE 
																									WHEN (CASE WHEN ISNULL(@tintPackingMode,1) = 2 THEN ISNULL(OpportunityItems.numQtyPicked,0) ELSE ISNULL(OpportunityItems.numUnitHour,0) END) - ISNULL((SELECT 
																																																																SUM(OpportunityBizDocItems.numUnitHour)
																																																															FROM
																																																																OpportunityBizDocs
																																																															INNER JOIN
																																																																OpportunityBizDocItems
																																																															ON
																																																																OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
																																																															WHERE
																																																																OpportunityBizDocs.numOppId = OpportunityMaster.numOppID
																																																																AND OpportunityBizDocs.numBizDocId=@numDefaultSalesShippingDoc
																																																																AND OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtcode),0) > 0
																									THEN 1 
																									ELSE 0 
																								END)
																							ELSE 0
																						END)
															WHEN @numViewID = 3
															THEN (CASE 
																	WHEN (CASE 
																			WHEN ISNULL(@tintInvoicingType,0)=1 
																			THEN ISNULL(OpportunityItems.numQtyShipped,0) 
																			ELSE ISNULL(OpportunityItems.numUnitHour,0) 
																		END) - ISNULL((SELECT 
																							SUM(OpportunityBizDocItems.numUnitHour)
																						FROM
																							OpportunityBizDocs
																						INNER JOIN
																							OpportunityBizDocItems
																						ON
																							OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
																						WHERE
																							OpportunityBizDocs.numOppId = OpportunityMaster.numOppID
																							AND OpportunityBizDocs.numBizDocId=287
																							AND OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtcode),0) > 0 
																	THEN 1
																	ELSE 0 
																END)
															WHEN @numViewID = 4 THEN 1
															WHEN @numViewID = 5 THEN 1
															WHEN @numViewID = 6 THEN 1
														END)
												AND 1 = (CASE
															WHEN @numViewID = 1 THEN (CASE WHEN ISNULL(WareHouseItems.numWareHouseItemID,0) > 0 AND ISNULL(OpportunityItems.bitDropship,0) = 0 THEN 1 ELSE 0 END)
															WHEN @numViewID = 2 AND @tintPackingViewMode <> 2 THEN (CASE WHEN ISNULL(WareHouseItems.numWareHouseItemID,0) > 0 AND ISNULL(OpportunityItems.bitDropship,0) = 0 THEN 1 ELSE 0 END)
															WHEN @numViewID = 2 AND @tintPackingViewMode = 2 THEN 1
															WHEN @numViewID = 3 THEN 1
															WHEN @numViewID = 4 THEN 1
															WHEN @numViewID = 5 THEN 1
															WHEN @numViewID = 6 THEN 1
															ELSE 0
														END)
												AND 1 = (CASE WHEN ISNULL(@numBatchID,0) > 0 THEN (CASE WHEN MassSalesFulfillmentBatchOrders.ID IS NOT NULL THEN 1 ELSE 0 END) ELSE 1 END)
												AND 1 = (CASE WHEN @numViewID = 4 OR @numViewID=6 THEN (CASE WHEN OpportunityBizDocs.numOppBizDocsId IS NOT NULL THEN 1 ELSE 0 END) ELSE 1 END)
												AND 1 =	(CASE WHEN @numViewID = 4 THEN (CASE WHEN ISNULL(OpportunityBizDocs.monDealAmount,0) - ISNULL(OpportunityBizDocs.monAmountPaid,0) > 0 THEN 1 ELSE 0 END) ELSE 1 END)
												AND 1 = (CASE 
															WHEN @numViewID = 5 AND ISNULL(@tintPendingCloseFilter,0) = 2
															THEN 
																(CASE 
																	WHEN (SELECT 
																				COUNT(*) 
																			FROM 
																			(
																				SELECT
																					OI.numoppitemtCode,
																					ISNULL(OI.numUnitHour,0) AS OrderedQty,
																					ISNULL(TempFulFilled.FulFilledQty,0) AS FulFilledQty
																				FROM
																					OpportunityItems OI
																				INNER JOIN
																					Item I
																				ON
																					OI.numItemCode = I.numItemCode
																				OUTER APPLY
																				(
																					SELECT
																						SUM(OpportunityBizDocItems.numUnitHour) AS FulFilledQty
																					FROM
																						OpportunityBizDocs
																					INNER JOIN
																						OpportunityBizDocItems 
																					ON
																						OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
																					WHERE
																						OpportunityBizDocs.numOppId = OpportunityMaster.numOppID
																						AND ISNULL(OpportunityBizDocs.numBizDocId,0) = 296
																						AND OpportunityBizDocItems.numOppItemID = OI.numoppitemtCode
																				) AS TempFulFilled
																				WHERE
																					OI.numOppID = OpportunityMaster.numOppID
																					AND ISNULL(OI.numUnitHour,0) > 0
																					AND ISNULL(OI.bitDropShip,0) = 0
																			) X
																			WHERE
																				X.OrderedQty <> X.FulFilledQty) = 0
																		AND (SELECT 
																				COUNT(*) 
																			FROM 
																			(
																				SELECT
																					OI.numoppitemtCode,
																					ISNULL(OI.numUnitHour,0) AS OrderedQty,
																					ISNULL(TempInvoice.InvoicedQty,0) AS InvoicedQty
																				FROM
																					OpportunityItems OI
																				INNER JOIN
																					Item I
																				ON
																					OI.numItemCode = I.numItemCode
																				OUTER APPLY
																				(
																					SELECT
																						SUM(OpportunityBizDocItems.numUnitHour) AS InvoicedQty
																					FROM
																						OpportunityBizDocs
																					INNER JOIN
																						OpportunityBizDocItems 
																					ON
																						OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
																					WHERE
																						OpportunityBizDocs.numOppId = OpportunityMaster.numOppID
																						AND OpportunityBizDocItems.numOppItemID = OI.numoppitemtCode
																						AND ISNULL(OpportunityBizDocs.bitAuthoritativeBizDocs,0) = 1
																				) AS TempInvoice
																				WHERE
																					OI.numOppID = OpportunityMaster.numOppID
																					AND ISNULL(OI.numUnitHour,0) > 0
																			) X
																			WHERE
																				X.OrderedQty > X.InvoicedQty) = 0
																	THEN 1 
																	ELSE 0 
																END) 
															ELSE 1 
														END)
												AND 1 = (CASE 
															WHEN ISNULL(@numShippingZone,0) > 0 
															THEN (CASE 
																		WHEN ISNULL((SELECT TOP 1 numState FROM fn_getOPPAddressDetails(OpportunityMaster.numOppId,OpportunityMaster.numDomainID,2)),0) > 0 
																		THEN
																			CASE 
																				WHEN (SELECT COUNT(*) FROM dbo.State WHERE numDomainID=@numDomainId AND numStateID =  (SELECT TOP 1 numState FROM fn_getOPPAddressDetails(OpportunityMaster.numOppId,OpportunityMaster.numDomainID,2)) AND numShippingZone=@numShippingZone) > 0 
																				THEN 1
																				ELSE 0
																			END
																		ELSE 0 
																	END) 
															ELSE 1 
														END)',(CASE WHEN v_numViewID = 2 AND v_tintPackingViewMode = 3 THEN ' AND 1 = (CASE WHEN ISNULL(OpportunityItems.numWarehouseItmsID,0) > 0 AND ISNULL(OpportunityItems.bitDropShip,0) = 0 THEN 1 ELSE 0 END)' ELSE '' END),
   '1 = (CASE WHEN ISNULL(OpportunityBizDocs.numOppBizDocsId,0) > 0 THEN (CASE WHEN OpportunityBizDocItems.numOppItemID > 0 THEN 1 ELSE 0 END) ELSE 1 END)');

   v_vcGroupBy  TEXT DEFAULT(CASE 
   WHEN v_bitGroupByOrder = true 
   THEN 'GROUP BY OpportunityMaster.numOppId
																	,OpportunityMaster.dtExpectedDate
																	,DivisionMaster.numDivisionID
																	,DivisionMaster.numTerID
																	,DivisionMaster.intShippingCompany
																	,DivisionMaster.numDefaultShippingServiceID
																	,OpportunityBizDocs.numOppBizDocsId
																	,OpportunityBizDocs.vcBizDocID
																	,OpportunityBizDocs.monDealAmount
																	,OpportunityBizDocs.monAmountPaid'
   ELSE '' 
   END);

   v_vcOrderBy  TEXT DEFAULT CONCAT(' ORDER BY ',(CASE v_vcSortColumn
   WHEN 'CompanyInfo.vcCompanyName' THEN 'CompanyInfo.vcCompanyName' 
   WHEN 'Item.charItemType' THEN '(CASE 
															WHEN Item.charItemType=''P''  THEN ''Inventory Item''
															WHEN Item.charItemType=''N'' THEN ''Non Inventory Item'' 
															WHEN Item.charItemType=''S'' THEN ''Service''
															WHEN Item.charItemType=''A'' THEN ''Accessory''
														END)'
   WHEN 'Item.numBarCodeId' THEN 'Item.numBarCodeId'
   WHEN 'Item.numItemClassification' THEN 'dbo.GetListIemName(Item.numItemClassification)'
   WHEN 'Item.numItemGroup' THEN 'ISNULL(ItemGroups.vcItemGroup,'''')'
   WHEN 'Item.vcSKU' THEN 'Item.vcSKU'
   WHEN 'OpportunityItems.vcItemName' THEN 'Item.vcItemName'
   WHEN 'OpportunityMaster.numStatus' THEN 'dbo.GetListIemName(OpportunityMaster.numStatus)'
   WHEN 'OpportunityMaster.vcPoppName' THEN 'OpportunityMaster.vcPoppName'
   WHEN 'OpportunityMaster.numAssignedTo' THEN 'dbo.fn_GetContactName(OpportunityMaster.numAssignedTo)'
   WHEN 'OpportunityMaster.numRecOwner' THEN 'dbo.fn_GetContactName(OpportunityMaster.numRecOwner)'
   WHEN 'WareHouseItems.vcLocation' THEN 'WarehouseLocation.vcLocation'
   WHEN 'OpportunityBizDocs.monAmountPaid' THEN 'TempPaid.monAmountPaid'
   WHEN 'OpportunityBizDocs.vcBizDocID' THEN 'LEN(OpportunityBizDocs.vcBizDocID) DESC, OpportunityBizDocs.vcBizDocID'
   WHEN 'OpportunityItems.numQtyPacked' THEN 'ISNULL((SELECT SUM(numUnitHour) FROM OpportunityBizDocs OBD INNER JOIN OpportunityBizDocItems OBDI ON OBD.numOppBizDocsId=OBDI.numOppBizDocID  WHERE OBD.numOppId=OpportunityMaster.numOppId AND OBD.numBizDocId=29397 AND OBDI.numOppItemID=OpportunityItems.numoppitemtCode),0)'
   WHEN 'OpportunityItems.numQtyPicked' THEN 'OpportunityItems.numQtyPicked'
   WHEN 'OpportunityItems.numQtyShipped' THEN 'OpportunityItems.numQtyShipped'
   WHEN 'OpportunityItems.numRemainingQty' THEN '(CASE 
																			WHEN @numViewID = 1
																			THEN ISNULL(OpportunityItems.numUnitHour,0) - ISNULL(OpportunityItems.numQtyPicked,0)
																			WHEN @numViewID = 2
																			THEN ISNULL(OpportunityItems.numUnitHour,0) - ISNULL((SELECT SUM(numUnitHour) FROM OpportunityBizDocs OBD INNER JOIN OpportunityBizDocItems OBDI ON OBD.numOppBizDocsId=OBDI.numOppBizDocID  WHERE OBD.numOppId=OpportunityMaster.numOppId AND OBD.numBizDocId=29397 AND OBDI.numOppItemID=OpportunityItems.numoppitemtCode),0)
																			ELSE 0
																		END)'
   WHEN 'OpportunityItems.numUnitHour' THEN 'OpportunityItems.numUnitHour'
   WHEN 'OpportunityItems.vcInvoiced' THEN 'ISNULL((SELECT SUM(numUnitHour) FROM OpportunityBizDocs OBD INNER JOIN OpportunityBizDocItems OBDI ON OBD.numOppBizDocsId=OBDI.numOppBizDocID  WHERE OBD.numOppId=OpportunityMaster.numOppId AND OBD.numBizDocId=287 AND OBDI.numOppItemID=OpportunityItems.numoppitemtCode),0)'
   WHEN 'OpportunityMaster.dtExpectedDate' THEN 'OpportunityMaster.dtExpectedDate'
   WHEN 'OpportunityItems.vcItemReleaseDate' THEN 'OpportunityItems.ItemReleaseDate'
   WHEN 'OpportunityMaster.bintCreatedDate' THEN 'OpportunityMaster.bintCreatedDate'
   WHEN 'OpportunityMaster.numAge' THEN 'OpportunityMaster.bintCreatedDate'
   WHEN 'OpportunityMaster.bitPaidInFull' THEN '(CASE WHEN ISNULL(TempPaid.monAmountPaid,0) >= ISNULL(OpportunityMaster.monDealAmount,0) THEN 1 ELSE 0 END)'
   WHEN 'OpportunityMaster.dtItemReceivedDate' THEN '(SELECT MAX(OMInner.dtItemReceivedDate) FROM OpportunityMaster OMInner INNER JOIN OpportunityItems OIInner ON OMInner.numOppID=OIInner.numOppID WHERE OMInner.numDomainID=@numDomainID AND OMInner.tintOppType=2 AND OMInner.tintOppStatus=1 AND OIInner.numItemCode=OpportunityItems.numItemCode)'
   ELSE 'OpportunityMaster.bintCreatedDate'
   END),' ',(CASE WHEN coalesce(v_vcSortOrder,'') <> '' THEN v_vcSortOrder ELSE 'ASC' END),
   ' OFFSET (@numPageIndex-1) * 200 ROWS FETCH NEXT 200 ROWS ONLY');

   v_j  INTEGER DEFAULT 0;
   v_jCount  INTEGER;
   v_numFieldId  NUMERIC(18,0);
   v_vcListItemType  VARCHAR(10);
   v_vcFieldName  VARCHAR(50);                                                                                                
   v_vcAssociatedControlType  VARCHAR(10);  
   v_vcLookBackTableName  VARCHAR(100);
   v_vcDbColumnName  VARCHAR(100);
   v_Grp_id  INTEGER;                                               
   v_bitCustomField  BOOLEAN;      
   v_bitIsNumeric  BOOLEAN;
   v_vcFinal  TEXT DEFAULT CONCAT(v_vcSelect,v_vcFrom,v_vcWhere,v_vcGroupBy,v_vcOrderBy);
BEGIN
   IF v_numViewID = 4 OR v_numViewID = 5 OR v_numViewID = 6 then
	
      v_bitGroupByOrder := true;
   end if;
	
   v_vcCustomSearchValue := REPLACE(v_vcCustomSearchValue,'OpportunityItems.vcItemReleaseDate','OpportunityItems.ItemReleaseDate');
   v_vcCustomSearchValue := REPLACE(v_vcCustomSearchValue,'OpportunityMaster.dtItemReceivedDate','(SELECT MAX(OMInner.dtItemReceivedDate) FROM OpportunityMaster OMInner INNER JOIN OpportunityItems OIInner ON OMInner.numOppID=OIInner.numOppID WHERE OMInner.numDomainID=@numDomainID AND OMInner.tintOppType=2 AND OMInner.tintOppStatus=1 AND OIInner.numItemCode=OpportunityItems.numItemCode)');

   If v_numViewID = 6 AND POSITION('OpportunityMaster.bintCreatedDate' IN v_vcCustomSearchValue) > 0 then
	
      v_vcCustomSearchValue := REPLACE(v_vcCustomSearchValue,'OpportunityMaster.bintCreatedDate','DATEADD(MINUTE,@ClientTimeZoneOffset * -1,OpportunityBizDocs.dtCreatedDate)');
   ELSEIF POSITION('OpportunityMaster.bintCreatedDate' IN v_vcCustomSearchValue) > 0
   then
	
      v_vcCustomSearchValue := REPLACE(v_vcCustomSearchValue,'OpportunityMaster.bintCreatedDate','DATEADD(MINUTE,@ClientTimeZoneOffset * -1,OpportunityMaster.bintCreatedDate)');
   end if;

   IF v_bitGroupByOrder = true then
	
      IF v_vcSortColumn NOT IN('CompanyInfo.vcCompanyName','OpportunityBizDocs.monAmountPaid','OpportunityBizDocs.vcBizDocID',
      'OpportunityMaster.bintCreatedDate','OpportunityMaster.bitPaidInFull',
      'OpportunityMaster.numAge','OpportunityMaster.numStatus',
      'OpportunityMaster.vcPoppName','OpportunityMaster.dtItemReceivedDate',
      'OpportunityMaster.numAssignedTo','OpportunityMaster.numRecOwner') then
		
         v_vcSortColumn := 'OpportunityMaster.bintCreatedDate';
         v_vcSortOrder := 'DESC';
      end if;
   ELSE
      IF v_vcSortColumn NOT IN('CompanyInfo.vcCompanyName','Item.charItemType','Item.numBarCodeId','Item.numItemClassification',
      'Item.numItemGroup','Item.vcSKU','OpportunityBizDocs.monAmountPaid',
      'OpportunityMaster.dtExpectedDate','OpportunityItems.numQtyPacked',
      'OpportunityItems.numQtyPicked','OpportunityItems.numQtyShipped',
      'OpportunityItems.numRemainingQty','OpportunityItems.numUnitHour',
      'OpportunityItems.vcInvoiced','OpportunityItems.vcItemName',
      'OpportunityItems.vcItemReleaseDate','OpportunityMaster.bintCreatedDate',
      'OpportunityMaster.bitPaidInFull','OpportunityMaster.numAge','OpportunityMaster.numStatus',
      'OpportunityMaster.vcPoppName','WareHouseItems.vcLocation',
      'OpportunityMaster.dtItemReceivedDate','OpportunityBizDocs.vcBizDocID') then
		
         v_vcSortColumn := 'OpportunityMaster.bintCreatedDate';
         v_vcSortOrder := 'DESC';
      end if;
   end if;

   select   coalesce(tintCommitAllocation,1), coalesce(numShippingServiceItemID,0), coalesce(numDefaultSalesShippingDoc,0) INTO v_tintCommitAllocation,v_numShippingServiceItemID,v_numDefaultSalesShippingDoc FROM
   Domain WHERE
   numDomainId = v_numDomainID; 

   IF v_numViewID = 2 AND v_tintPackingViewMode = 3 AND v_numDefaultSalesShippingDoc = 0 then
	
      RAISE NOTICE 'DEFAULT_SHIPPING_BIZDOC_NOT_CONFIGURED';
      RETURN;
   end if;

   select   tintPackingMode, tintInvoicingType, bitEnablePickListMapping, bitEnableFulfillmentBizDocMapping INTO v_tintPackingMode,v_tintInvoicingType,v_bitEnablePickListMapping,v_bitEnableFulfillmentBizDocMapping FROM
   MassSalesFulfillmentConfiguration WHERE
   numDomainID = v_numDomainID; 

   BEGIN
      CREATE TEMP SEQUENCE tt_TempFieldsLeft_seq INCREMENT BY 1 START WITH 1;
      EXCEPTION WHEN OTHERS THEN
         NULL;
   END;
   DROP TABLE IF EXISTS tt_TEMPFIELDSLEFT CASCADE;
   CREATE TEMPORARY TABLE tt_TEMPFIELDSLEFT
   (
      numPKID INTEGER GENERATED ALWAYS AS IDENTITY(START 1 INCREMENT 1),
      ID VARCHAR(50),
      numFieldID NUMERIC(18,0),
      vcFieldName VARCHAR(100),
      vcOrigDbColumnName VARCHAR(100),
      vcListItemType VARCHAR(10),
      numListID NUMERIC(18,0),
      bitAllowSorting BOOLEAN,
      bitAllowFiltering BOOLEAN,
      vcAssociatedControlType VARCHAR(20),
      vcLookBackTableName VARCHAR(100),
      bitCustomField BOOLEAN,
      intRowNum INTEGER,
      intColumnWidth DOUBLE PRECISION,
      bitIsRequired BOOLEAN,
      bitIsEmail BOOLEAN,
      bitIsAlphaNumeric BOOLEAN,
      bitIsNumeric BOOLEAN,
      bitIsLengthValidation BOOLEAN,
      intMaxLength INTEGER,
      intMinLength INTEGER,
      bitFieldMessage BOOLEAN,
      vcFieldMessage TEXT,
      Grp_id INTEGER
   );

   INSERT INTO tt_TEMPFIELDSLEFT(ID
		,numFieldID
		,vcFieldName
		,vcOrigDbColumnName
		,vcListItemType
		,numListID
		,bitAllowSorting
		,bitAllowFiltering
		,vcAssociatedControlType
		,vcLookBackTableName
		,bitCustomField
		,intRowNum
		,intColumnWidth
		,bitIsRequired
		,bitIsEmail
		,bitIsAlphaNumeric
		,bitIsNumeric
		,bitIsLengthValidation
		,intMaxLength
		,intMinLength
		,bitFieldMessage
		,vcFieldMessage
		,Grp_id)
   SELECT
   CONCAT(141,'~',DFM.numFieldID,'~',0)
		,DFM.numFieldID
		,CAST((CASE WHEN v_numViewID = 6 AND DFM.numFieldID = 248 THEN 'BizDoc' ELSE DFFM.vcFieldName END) AS VARCHAR(100))
		,DFM.vcOrigDbCOlumnName
		,coalesce(DFM.vcListItemType,'')
		,coalesce(DFM.numListID,0)
		,DFFM.bitAllowSorting
		,DFFM.bitAllowFiltering
		,DFFM.vcAssociatedControlType
		,DFM.vcLookBackTableName
		,CAST(0 AS BOOLEAN)
		,DFCD.intRowNum
		,coalesce(DFCD.intColumnWidth,0)
		,CAST(0 AS BOOLEAN)
		,CAST(0 AS BOOLEAN)
		,CAST(0 AS BOOLEAN)
		,CAST((CASE WHEN vcFieldDataType IN('N','M') THEN 1 ELSE 0 END) AS BOOLEAN)
		,CAST(0 AS BOOLEAN)
		,CAST(0 AS INTEGER)
		,CAST(0 AS INTEGER)
		,CAST(0 AS BOOLEAN)
		,CAST('' AS TEXT)
		,0
   FROM
   DycFormConfigurationDetails DFCD
   INNER JOIN
   DycFormField_Mapping DFFM
   ON
   DFCD.numFormID = DFFM.numFormID
   AND DFCD.numFieldID = DFFM.numFieldID
   INNER JOIN
   DycFieldMaster DFM
   ON
   DFFM.numFieldID = DFM.numFieldID
   WHERE
   DFCD.numDomainID = v_numDomainID
   AND DFCD.numUserCntID = v_numUserCntID
   AND DFCD.numFormID = 141
   AND DFCD.numViewId = v_numViewID
   AND coalesce(DFFM.bitDeleted,0) = 0
   AND coalesce(DFFM.bitSettingField,0) = 1
   AND coalesce(DFCD.bitCustom,0) = 0
   UNION
   SELECT
   CONCAT(141,'~',CFM.Fld_id,'~',0)
		,CFM.Fld_id
		,CFM.FLd_label
		,CONCAT('CUST',CFM.Fld_id)
		,CAST('' AS VARCHAR(10))
		,coalesce(CFM.numlistid,0)
		,CAST(0 AS BOOLEAN)
		,CAST(0 AS BOOLEAN)
		,CFM.fld_type
		,CAST('' AS VARCHAR(100))
		,CAST(1 AS BOOLEAN)
		,DFCD.intRowNum
		,coalesce(DFCD.intColumnWidth,0)
		,coalesce(bitIsRequired,false)
		,coalesce(bitIsEmail,false)
		,coalesce(bitIsAlphaNumeric,false)
		,coalesce(bitIsNumeric,false)
		,coalesce(bitIsLengthValidation,false)
		,coalesce(intMaxLength,0)
		,coalesce(intMinLength,0)
		,coalesce(bitFieldMessage,false)
		,coalesce(vcFieldMessage,'')
		,CFM.Grp_id
   FROM
   DycFormConfigurationDetails DFCD
   INNER JOIN
   CFW_Fld_Master CFM
   ON
   DFCD.numFieldID = CFM.Fld_id
   LEFT JOIN
   CFW_Validation CV
   ON
   CFM.Fld_id = CV.numFieldId
   WHERE
   DFCD.numDomainID = v_numDomainID
   AND DFCD.numUserCntID = v_numUserCntID
   AND DFCD.numFormID = 141
   AND DFCD.numViewId = v_numViewID
   AND coalesce(DFCD.bitCustom,0) = 1;

   IF(SELECT COUNT(*) FROM tt_TEMPFIELDSLEFT) = 0 then
	
      INSERT INTO tt_TEMPFIELDSLEFT(ID
			,numFieldID
			,vcFieldName
			,vcOrigDbColumnName
			,vcListItemType
			,numListID
			,bitAllowSorting
			,bitAllowFiltering
			,vcAssociatedControlType
			,vcLookBackTableName
			,bitCustomField
			,intRowNum
			,intColumnWidth)
      SELECT
      CONCAT(141,'~',DFM.numFieldID,'~',0)
			,DFM.numFieldID
			,DFFM.vcFieldName
			,DFM.vcOrigDbCOlumnName
			,coalesce(DFM.vcListItemType,'')
			,coalesce(DFM.numListID,0)
			,DFFM.bitAllowSorting
			,DFFM.bitAllowFiltering
			,DFFM.vcAssociatedControlType
			,DFM.vcLookBackTableName
			,CAST(0 AS BOOLEAN)
			,DFFM."order"
			,100
      FROM
      DycFormField_Mapping DFFM
      INNER JOIN
      DycFieldMaster DFM
      ON
      DFFM.numFieldID = DFM.numFieldID
      WHERE
      DFFM.numFormID = 141
      AND coalesce(DFFM.bitDeleted,0) = 0
      AND coalesce(DFFM.bitSettingField,0) = 1
      AND coalesce(DFFM.bitDefault,0) = 1;
   end if;

   v_vcWhere := CONCAT(v_vcWhere,' ',v_vcCustomSearchValue);
	
   IF EXISTS(SELECT ID FROM tt_TEMPFIELDSLEFT WHERE vcOrigDbColumnName = 'bitPaidInFull') OR v_vcSortColumn = 'OpportunityMaster.bitPaidInFull' then
	
      v_vcFrom := CONCAT(v_vcFrom,' OUTER APPLY
										(
											SELECT 
												SUM(OBInner.monAmountPaid) monAmountPaid
											FROM 
												OpportunityBizDocs OBInner 
											WHERE 
												OBInner.numOppId=OpportunityMaster.numOppId 
												AND OBInner.numBizDocId=287
										) TempPaid');
   end if;

   IF EXISTS(SELECT ID FROM tt_TEMPFIELDSLEFT WHERE vcOrigDbColumnName = 'numQtyPacked') OR v_vcSortColumn = 'OpportunityItems.numQtyPacked' then
	
      v_vcFrom := CONCAT(v_vcFrom,' OUTER APPLY
										(
											SELECT 
												SUM(OpportunityBizDocItems.numUnitHour) numPackedQty
											FROM 
												OpportunityBizDocs
											INNER JOIN
												OpportunityBizDocItems
											ON
												OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
											WHERE 
												OpportunityBizDocs.numOppId = OpportunityMaster.numOppID
												AND OpportunityBizDocs.numBizDocId=@numDefaultSalesShippingDoc
												AND OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtcode
										) TempPacked');
   end if;

   SELECT COUNT(*) INTO v_jCount FROM tt_TEMPFIELDSLEFT;


   WHILE v_j <= v_jCount LOOP
      select   numFieldID, vcFieldName, vcAssociatedControlType, (CASE WHEN coalesce(bitCustomField,false) = true THEN CONCAT('Cust',numFieldID) ELSE vcOrigDbColumnName END), vcLookBackTableName, coalesce(bitCustomField,false), coalesce(Grp_id,0), coalesce(bitIsNumeric,false), coalesce(vcListItemType,'') INTO v_numFieldId,v_vcFieldName,v_vcAssociatedControlType,v_vcDbColumnName,
      v_vcLookBackTableName,v_bitCustomField,v_Grp_id,v_bitIsNumeric,v_vcListItemType FROM
      tt_TEMPFIELDSLEFT WHERE
      numPKID = v_j;
      IF v_vcDbColumnName NOT IN('vcBizDocID','numRemainingQty','dtAnticipatedDelivery','vcShipStatus',
      'vcPaymentStatus','numShipRate') then
		
         IF coalesce(v_bitCustomField,false) = false then
			
            IF (v_vcLookBackTableName = 'Item' OR v_vcLookBackTableName = 'WareHouseItems') AND coalesce(v_bitGroupByOrder,false) = true then
				
               v_vcSelect := CONCAT(v_vcSelect,',',(CASE WHEN v_bitIsNumeric = true THEN '0' ELSE '' END),
               ' [',v_vcDbColumnName,']');
            ELSE
               IF v_vcDbColumnName = 'numAge' then
					
                  v_vcSelect := CONCAT(v_vcSelect,'CONCAT(''<span style="color:'',(CASE
																							WHEN (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE())/24) = 0 AND (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE()) % 24) < 4  
																							THEN ''#00ff00''
																							WHEN (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE())/24) = 0 AND ((DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE()) % 24) >= 4 AND (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE()) % 24) < 8)
																							THEN ''#00b050''
																							WHEN (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE())/24) = 0 AND ((DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE()) % 24) >= 8 AND (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE()) % 24) < 12)
																							THEN ''#3399ff''
																							WHEN (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE())/24) = 0 AND ((DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE()) % 24) >= 12 AND (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE()) % 24) < 24)
																							THEN ''#ff9900''
																							WHEN (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE())/24) BETWEEN 1 AND 2  
																							THEN ''#9b3596''
																							ELSE ''#ff0000''
																						END),''">'',CONCAT(DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE())/24,''d '',DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE()) % 24,''h''),''</span>'')',' [',v_vcDbColumnName,']');
               ELSEIF v_vcDbColumnName = 'dtItemReceivedDate'
               then
					
                  v_vcSelect := CONCAT(v_vcSelect,',dbo.FormatedDateFromDate((SELECT MAX(OMInner.dtItemReceivedDate) FROM OpportunityMaster OMInner INNER JOIN OpportunityItems OIInner ON OMInner.numOppID=OIInner.numOppID WHERE OMInner.numDomainID=@numDomainID AND OMInner.tintOppType=2 AND OMInner.tintOppStatus=1 AND OIInner.numItemCode=OpportunityItems.numItemCode),@numDomainID) [',v_vcDbColumnName,
                  ']');
               ELSEIF v_vcDbColumnName = 'bitPaidInFull'
               then
					
                  v_vcSelect := CONCAT(v_vcSelect,',(CASE WHEN ISNULL(TempPaid.monAmountPaid,0) >= ISNULL(OpportunityMaster.monDealAmount,0) THEN 1 ELSE 0 END) [',v_vcDbColumnName,
                  ']');
               ELSEIF v_vcDbColumnName = 'dtExpectedDate'
               then
					
                  v_vcSelect := CONCAT(v_vcSelect,',dbo.FormatedDateFromDate(OpportunityMaster.dtExpectedDate,@numDomainID) [',v_vcDbColumnName,']');
               ELSEIF v_vcDbColumnName = 'dtCreatedDate'
               then
					
                  v_vcSelect := CONCAT(v_vcSelect,',(CASE WHEN @numViewID = 6 THEN dbo.FormatedDateFromDate(DATEADD(MINUTE,@ClientTimeZoneOffset * -1,OpportunityBizDocs.dtCreatedDate),@numDomainID) ELSE dbo.FormatedDateFromDate(DATEADD(MINUTE,@ClientTimeZoneOffset * -1,OpportunityMaster.bintCreatedDate),@numDomainID) END) [',
                  v_vcDbColumnName,']');
               ELSEIF v_vcDbColumnName = 'numQtyPacked'
               then
					
                  v_vcSelect := CONCAT(v_vcSelect,',ISNULL(TempPacked.numPackedQty,0) [',v_vcDbColumnName,']');
               ELSEIF v_vcDbColumnName = 'vcInvoiced'
               then
					
                  v_vcSelect := CONCAT(v_vcSelect,',ISNULL((SELECT SUM(numUnitHour) FROM OpportunityBizDocs OBD INNER JOIN OpportunityBizDocItems OBDI ON OBD.numOppBizDocsId=OBDI.numOppBizDocID  WHERE OBD.numOppId=OpportunityMaster.numOppId AND OBD.numBizDocId=287 AND OBDI.numOppItemID=OpportunityItems.numoppitemtCode),0) [',
                  v_vcDbColumnName,']');
               ELSEIF v_vcDbColumnName = 'vcLocation'
               then
					
                  v_vcSelect := CONCAT(v_vcSelect,',ISNULL(WarehouseLocation.vcLocation,'''') [',v_vcDbColumnName,
                  ']');
               ELSEIF v_vcDbColumnName = 'charItemType'
               then
					
                  v_vcSelect := CONCAT(v_vcSelect,',(CASE 
																	WHEN Item.charItemType=''P'' 
																	THEN 
																		CASE 
																			WHEN ISNULL(Item.bitAssembly,0) = 1 THEN ''Assembly''
																			WHEN ISNULL(Item.bitKitParent,0) = 1 THEN ''Kit''
																			WHEN ISNULL(Item.bitAsset,0) = 1 AND ISNULL(Item.bitRental,0)=0 THEN ''Asset''
																			WHEN ISNULL(Item.bitAsset,0) = 1 AND ISNULL(Item.bitRental,0)=1 THEN ''Rental Asset''
																			WHEN ISNULL(Item.bitSerialized,0) = 1 AND ISNULL(Item.bitAsset,0) = 0  THEN ''Serialized''
																			WHEN ISNULL(Item.bitSerialized,0) = 1 AND ISNULL(Item.bitAsset,0) = 1 AND ISNULL(Item.bitRental,0)=0 THEN ''Serialized Asset''
																			WHEN ISNULL(Item.bitSerialized,0) = 1 AND ISNULL(Item.bitAsset,0) = 1 AND ISNULL(Item.bitRental,0)=1 THEN ''Serialized Rental Asset''
																			WHEN ISNULL(Item.bitLotNo,0)=1 THEN ''Lot #''
																			ELSE ''Inventory Item'' 
																		END
																	WHEN Item.charItemType=''N'' THEN ''Non Inventory Item'' 
																	WHEN Item.charItemType=''S'' THEN ''Service'' 
																	WHEN Item.charItemType=''A'' THEN ''Accessory'' 
																END)',' [',v_vcDbColumnName,']');
               ELSEIF v_vcAssociatedControlType = 'SelectBox'
               then
					
                  IF v_vcListItemType = 'LI' then
						
                     v_vcSelect := CONCAT(v_vcSelect,',L',SUBSTR(CAST(v_j AS VARCHAR(3)),1,3),'.vcData',' [',v_vcDbColumnName,
                     ']');
                     v_vcFrom := CONCAT(v_vcFrom,' LEFT JOIN ListDetails L',v_j,' ON L',v_j,'.numListItemID=',
                     v_vcLookBackTableName,'.',v_vcDbColumnName);
                  ELSEIF v_vcListItemType = 'IG'
                  then
						
                     v_vcSelect := CONCAT(v_vcSelect,',ISNULL((SELECT vcItemGroup FROM ItemGroups WHERE numItemGroupID = I.numItemGroup),'''')',' [',v_vcDbColumnName,']');
                  ELSEIF v_vcListItemType = 'U'
                  then
						
                     v_vcSelect := CONCAT(v_vcSelect,',dbo.fn_GetContactName(',v_vcLookBackTableName,'.',v_vcDbColumnName,
                     ') [',v_vcDbColumnName,']');
                  ELSE
                     v_vcSelect := CONCAT(v_vcSelect,',',(CASE WHEN v_bitIsNumeric = true THEN '0' ELSE '' END),
                     ' [',v_vcDbColumnName,']');
                  end if;
               ELSEIF v_vcAssociatedControlType = 'DateField'
               then
					
                  v_vcSelect := CONCAT(v_vcSelect,',dbo.FormatedDateFromDate(DATEADD(MINUTE,',-v_ClientTimeZoneOffset,
                  ',',v_vcLookBackTableName,'.',(CASE WHEN v_vcDbColumnName = '' THEN 'ItemReleaseDate' ELSE v_vcDbColumnName END),'),',v_numDomainID,
                  ') [',v_vcDbColumnName,']');
               ELSEIF v_vcAssociatedControlType = 'TextBox'
               then
					
                  v_vcSelect := CONCAT(v_vcSelect,',ISNULL(,',v_vcLookBackTableName,'.',v_vcDbColumnName,(CASE WHEN v_bitIsNumeric = true THEN '0' ELSE '' END),
                  ') [',v_vcDbColumnName,
                  ']');
               ELSEIF v_vcAssociatedControlType = 'TextArea'
               then
					
                  v_vcSelect := CONCAT(v_vcSelect,',ISNULL(,',v_vcLookBackTableName,'.',v_vcDbColumnName,(CASE WHEN v_bitIsNumeric = true THEN '0' ELSE '' END),
                  ') [',v_vcDbColumnName,
                  ']');
               ELSEIF v_vcAssociatedControlType = 'Label'
               then
					
                  v_vcSelect := CONCAT(v_vcSelect,',ISNULL(,',v_vcLookBackTableName,'.',v_vcDbColumnName,(CASE WHEN v_bitIsNumeric = true THEN '0' ELSE '' END),
                  ') [',v_vcDbColumnName,
                  ']');
               ELSE
                  v_vcSelect := CONCAT(v_vcSelect,',ISNULL(,',v_vcLookBackTableName,'.',v_vcDbColumnName,(CASE WHEN v_bitIsNumeric = true THEN '0' ELSE '' END),
                  ') [',v_vcDbColumnName,
                  ']');
               end if;
            end if;
         ELSEIF v_bitCustomField = true
         then
			
            IF v_Grp_id = 5 then
				
               IF coalesce(v_bitGroupByOrder,false) = true then
					
                  v_vcSelect := CONCAT(v_vcSelect,',''''',' [',v_vcDbColumnName,']');
               ELSE
                  v_vcSelect := CONCAT(v_vcSelect,CONCAT(',dbo.GetCustFldValueOppItems(',v_numFieldId,',OpportunityItems.numoppitemtCode,Item.numItemCode)'),' [',v_vcDbColumnName,
                  ']');
               end if;
            ELSE
               IF v_vcAssociatedControlType = 'TextBox' OR v_vcAssociatedControlType = 'TextArea' then
					
                  v_vcSelect := CONCAT(v_vcSelect,',CFW',v_numFieldId,'.Fld_Value  [',v_vcDbColumnName,']');
                  v_vcFrom := CONCAT(v_vcFrom,' left Join CFW_FLD_Values_Opp CFW',v_numFieldId,' ON CFW',v_numFieldId, 
                  '.Fld_Id=',v_numFieldId,' and CFW',v_numFieldId,'.RecId=OpportunityMaster.numOppID');
               ELSEIF v_vcAssociatedControlType = 'CheckBox'
               then
					
                  v_vcSelect := CONCAT(v_vcSelect,',case when isnull(CFW',v_numFieldId,'.Fld_Value,0)=0 then 0 when isnull(CFW', 
                  v_numFieldId,'.Fld_Value,0)=1 then 1 end   [',v_vcDbColumnName, 
                  ']');
                  v_vcFrom := CONCAT(v_vcFrom,' left Join @vcFrom CFW',v_numFieldId,' ON CFW',v_numFieldId,
                  '.Fld_Id=',v_numFieldId,' and CFW',v_numFieldId,'.RecId=OpportunityMaster.numOppID   ');
               ELSEIF v_vcAssociatedControlType = 'DateField'
               then
					
                  v_vcSelect := CONCAT(v_vcSelect,',dbo.FormatedDateFromDate(CFW',v_numFieldId,'.Fld_Value,', 
                  v_numDomainID,')  [',v_vcDbColumnName,']');
                  v_vcFrom := CONCAT(v_vcFrom,' left Join CFW_FLD_Values_Opp CFW',v_numFieldId,' on CFW',v_numFieldId, 
                  '.Fld_Id=',v_numFieldId,' and CFW',v_numFieldId,'.RecId=OpportunityMaster.numOppID   ');
               ELSEIF v_vcAssociatedControlType = 'SelectBox'
               then
					
                  v_vcSelect := CONCAT(v_vcSelect,',L',v_numFieldId,'.vcData',' [',v_vcDbColumnName,']');
                  v_vcFrom := CONCAT(v_vcFrom,' left Join CFW_FLD_Values_Opp CFW',v_numFieldId,' on CFW',v_numFieldId,
                  '.Fld_Id=',v_numFieldId,' and CFW',v_numFieldId,'.RecId=OpportunityMaster.numOppID    ');
                  v_vcFrom := CONCAT(v_vcFrom,' left Join ListDetails L',v_numFieldId,' on L',v_numFieldId, 
                  '.numListItemID=CFW',v_numFieldId,'.Fld_Value');
               ELSE
                  v_vcSelect := CONCAT(v_vcSelect,CONCAT(',dbo.GetCustFldValueOpp(',v_numFieldId,',OpportunityMaster.numOppID)'),' [',v_vcDbColumnName,']');
               end if;
            end if;
         end if;
      end if;
      v_j := v_j::bigint+1;
   END LOOP;


   EXECUTE format(v_vcFinal,v_numDomainID v_vcOrderStauts v_vcFilterValue v_tintFilterBy v_numViewID v_tintPackingViewMode v_numShippingServiceItemID v_numWarehouseID v_numShippingZone v_numDefaultSalesShippingDoc v_tintInvoicingType v_tintPackingMode v_bitRemoveBO v_ClientTimeZoneOffset v_numBatchID v_tintPrintBizDocViewMode v_tintPendingCloseFilter v_bitIncludeSearch);

   open SWV_RefCur for
   SELECT * FROM tt_TEMPFIELDSLEFT ORDER BY intRowNum;
END; $$;


