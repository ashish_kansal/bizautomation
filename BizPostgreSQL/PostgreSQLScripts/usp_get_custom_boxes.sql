CREATE OR REPLACE FUNCTION USP_GET_CUSTOM_BOXES
(
	v_numPackageTypeID	NUMERIC(18,0)
	,INOUT SWV_RefCur refcursor
)
LANGUAGE plpgsql
AS $$
	DECLARE v_strSQL TEXT;
BEGIN
	v_strSQL := 'SELECT 
					numCustomPackageID,
					numPackageTypeID,
					vcPackageName,
					fltWidth,
					fltHeight,
					fltLength,
					fltTotalWeight,
					(CASE WHEN numPackageTypeID > 100 THEN 1 ELSE 0 END) AS IsUpdatable
               FROM 
					CustomPackages CP 
				WHERE
					(' || coalesce(v_numPackageTypeID,0) || '= 0 OR CP.numPackageTypeID = '|| v_numPackageTypeID || ')
				ORDER BY numPackageTypeID';

	OPEN SWV_RefCur FOR EXECUTE v_strSQL;
	RETURN;
END; $$;

	


