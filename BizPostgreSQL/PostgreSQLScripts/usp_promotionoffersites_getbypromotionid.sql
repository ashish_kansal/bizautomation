-- Stored procedure definition script USP_PromotionOfferSites_GetByPromotionID for PostgreSQL
CREATE OR REPLACE FUNCTION USP_PromotionOfferSites_GetByPromotionID(v_numProId NUMERIC(18,0),
	v_numDomainID NUMERIC(18,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
   open SWV_RefCur for SELECT
		Sites.numSiteID ,
		vcSiteName,
		(CASE WHEN coalesce(PromotionOfferSites.numSiteID,0) > 0 THEN true ELSE false END) AS bitSelcted
   FROM
   Sites
   LEFT JOIN
   PromotionOfferSites
   ON
   Sites.numSiteID = PromotionOfferSites.numSiteID
   AND numPromotionID = v_numProId
   WHERE
   numDOmainID = v_numDomainID;
END; $$; 












