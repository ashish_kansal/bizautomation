-- Stored procedure definition script USP_ManageEmbeddedCostClass for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
-- USP_ManageEmbeddedCostClass 1,'2,5,6'
CREATE OR REPLACE FUNCTION USP_ManageEmbeddedCostClass(v_numDomainID NUMERIC,
v_vcClass VARCHAR(4000))
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   DELETE FROM EmbeddedCostConfig WHERE numDomainID = v_numDomainID;
   IF LTRIM(RTRIM(v_vcClass)) <> '' then
		
      INSERT INTO EmbeddedCostConfig(numDomainID,
			numItemClassification)
      SELECT v_numDomainID,Id FROM SplitIDs(v_vcClass,',');
   end if;
   RETURN;
END; $$;



