-- Stored procedure definition script USP_RecurrenceConfiguration_Insert for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
  
-- =============================================  
-- Author:  Sandeep
-- Create date: 9 Oct 2014
-- Description: Inserts recurrence configuration
-- =============================================  
Create or replace FUNCTION USP_RecurrenceConfiguration_Insert(v_numDomainID NUMERIC(18,0),
	v_numUserCntID NUMERIC(18,0),
	v_numOppID NUMERIC(18,0),
	v_numOppBizDocID NUMERIC(18,0) DEFAULT NULL,
	v_dtStartDate DATE DEFAULT NULL,
	v_dtEndDate DATE DEFAULT NULL,
	v_numType SMALLINT DEFAULT NULL,
	v_vcType VARCHAR(20) DEFAULT NULL,
	v_vcFrequency VARCHAR(20) DEFAULT NULL,
	v_numFrequency SMALLINT DEFAULT NULL)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
  
 
   IF v_numType = 1 then
 
      IF EXISTS(SELECT * FROM RecurrenceConfiguration WHERE numOppID = v_numOppID AND numDomainID = v_numDomainID) then
         RETURN;
      end if;
   end if;

   IF v_numType = 2 then
 
      IF EXISTS(SELECT * FROM RecurrenceConfiguration WHERE numOppID = v_numOppID AND numOppBizDocID = v_numOppBizDocID AND numDomainID = v_numDomainID) then
         RETURN;
      end if;
   end if;

   INSERT INTO RecurrenceConfiguration(numDomainID,
	numOppID,
	numOppBizDocID,
	dtStartDate,
	dtEndDate,
	numType,
	vcType,
	numFrequency,
	vcFrequency,
	dtNextRecurrenceDate,
	numCreatedBy,
	dtCreatedDate)
VALUES(v_numDomainID,
	v_numOppID,
	v_numOppBizDocID,
	v_dtStartDate,
	v_dtEndDate,
	v_numType,
	v_vcType,
	v_numFrequency,
	v_vcFrequency,
	v_dtStartDate,
	--(CASE @numFrequency
	--	WHEN 1 THEN DATEADD(D,1,@dtStartDate) --Daily
	--	WHEN 2 THEN DATEADD(D,7,@dtStartDate) --Weekly
	--	WHEN 3 THEN DATEADD(M,1,@dtStartDate) --Monthly
	--	WHEN 4 THEN DATEADD(M,4,@dtStartDate) --Quarterly
	--END),
	v_numUserCntID,
	LOCALTIMESTAMP);
END; $$;  


