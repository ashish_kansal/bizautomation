-- Stored procedure definition script USP_CustomQueryReport_LogEmailNotification for PostgreSQL
-- Timestamp: Sat Apr 10 10:56:43 2021
CREATE OR REPLACE FUNCTION USP_CustomQueryReport_LogEmailNotification(v_numReportID NUMERIC(18,0),
	v_tintEmailFrequency SMALLINT,
	v_dtDateToSend DATE)
RETURNS VOID LANGUAGE plpgsql
   AS $$
BEGIN
   INSERT INTO CustomQueryReportEmail(numReportID,
		tintEmailFrequency,
		dtDateToSend,
		dtSentDate)
	VALUES(v_numReportID,
		v_tintEmailFrequency,
		v_dtDateToSend,
		CAST(LOCALTIMESTAMP  AS DATE));
RETURN;
END; $$;

