-- Stored procedure definition script USP_ReportListMaster_TopCustomersByPortionOfTotalSales for PostgreSQL
CREATE OR REPLACE FUNCTION USP_ReportListMaster_TopCustomersByPortionOfTotalSales(v_numDomainID NUMERIC(18,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_TotalSalesOrderAmount  DECIMAL(20,5);
BEGIN
   select DISTINCT  SUM(monDealAmount) INTO v_TotalSalesOrderAmount FROM
   OpportunityMaster WHERE
   numDomainId = v_numDomainID
   AND tintopptype = 1
   AND tintoppstatus = 1; 

   open SWV_RefCur for SELECT 
   DM.numDivisionID
		,vcCompanyName
		,(SUM(monDealAmount)*100)/(CASE WHEN v_TotalSalesOrderAmount = 0 THEN 1 ELSE v_TotalSalesOrderAmount END) AS TotalSalesPercent
   FROM
   OpportunityMaster OM
   INNER JOIN
   DivisionMaster DM
   ON
   OM.numDivisionId = DM.numDivisionID
   INNER JOIN
   CompanyInfo CI
   ON
   DM.numCompanyID = CI.numCompanyId
   WHERE
   OM.numDomainId = v_numDomainID
   AND DM.numDomainID = v_numDomainID
   AND CI.numDomainID = v_numDomainID
   AND tintopptype = 1
   AND tintoppstatus = 1
   GROUP BY
   DM.numDivisionID,vcCompanyName
   HAVING
   SUM(monDealAmount) > 0
   ORDER BY
   SUM(monDealAmount) DESC LIMIT 10;
END; $$;












