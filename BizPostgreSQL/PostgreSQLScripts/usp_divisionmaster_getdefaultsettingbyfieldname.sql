CREATE OR REPLACE FUNCTION USP_DivisionMaster_GetDefaultSettingByFieldName(v_numDivisionID NUMERIC(18,0)
	,v_vcDbColumnName VARCHAR(300), INOUT SWV_RefCur refcursor)
LANGUAGE plpgsql
   AS $$
BEGIN
   IF v_vcDbColumnName = 'bitAllocateInventoryOnPickList' then
      open SWV_RefCur for
      SELECT coalesce(bitAllocateInventoryOnPickList,false) AS bitAllocateInventoryOnPickList FROM DivisionMaster WHERE numDivisionID = v_numDivisionID;
	ELSEIF v_vcDbColumnName = 'numAnnualRevID' THEN
		open SWV_RefCur for SELECT coalesce(LD.vcData,'') AS numAnnualRevID FROM DivisionMaster DM INNER JOIN CompanyInfo CI ON DM.numCompanyID=CI.numCompanyId LEFT JOIN ListDetails LD ON LD.numListID=6 AND LD.numListItemID=CI.numAnnualRevID WHERE DM.numDivisionID=v_numDivisionID;
	ELSEIF v_vcDbColumnName = 'numNoOfEmployeesId' THEN
		open SWV_RefCur for SELECT coalesce(LD.vcData,'') AS numNoOfEmployeesId FROM DivisionMaster DM INNER JOIN CompanyInfo CI ON DM.numCompanyID=CI.numCompanyId LEFT JOIN ListDetails LD ON LD.numListID=7 AND LD.numListItemID=CI.numNoOfEmployeesId WHERE DM.numDivisionID=v_numDivisionID;
   ELSEIF v_vcDbColumnName = 'numPrimaryContact'
   then
	
      open SWV_RefCur for
      SELECT
      coalesce(AdditionalContactsInformation.numContactId,0) AS numContactId
      FROM
      DivisionMaster
      LEFT JOIN
      AdditionalContactsInformation
      ON
      DivisionMaster.numDivisionID = AdditionalContactsInformation.numDivisionId
      WHERE
      DivisionMaster.numDivisionID = v_numDivisionID
      AND coalesce(AdditionalContactsInformation.bitPrimaryContact,false) = true;
   end if;
   RETURN;
END; $$;


