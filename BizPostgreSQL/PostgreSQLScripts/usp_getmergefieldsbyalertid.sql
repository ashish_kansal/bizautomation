-- Stored procedure definition script USP_GetMergeFieldsByAlertId for PostgreSQL
CREATE OR REPLACE FUNCTION USP_GetMergeFieldsByAlertId(v_AlertDTLID NUMERIC(18,0),INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
BEGIN
	open SWV_RefCur for 
	SELECT 
		vcMergeField
		,vcMergeFieldValue
	FROM 
		AlertEmailMergeFlds
	join 
		EmailMergeFields 
	on 
		intMergeFldID = intEmailMergeFields
	where 
		numEmailAlertDTLID = v_AlertDTLID;
END; $$;












