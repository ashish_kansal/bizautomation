-- Stored procedure definition script USP_ItemDetails_ValidateKitSelection for PostgreSQL
CREATE OR REPLACE FUNCTION USP_ItemDetails_ValidateKitSelection(v_numDomainID NUMERIC(18,0)
	,v_numItemCode NUMERIC(18,0)
	,v_vcKitChildItems TEXT,INOUT SWV_RefCur refcursor DEFAULT NULL)
LANGUAGE plpgsql
   AS $$
   DECLARE
   v_bitValidKitSelection  BOOLEAN DEFAULT true;

	--TODO: Prepare validation logic

BEGIN
   open SWV_RefCur for SELECT v_bitValidKitSelection;
END; $$;












