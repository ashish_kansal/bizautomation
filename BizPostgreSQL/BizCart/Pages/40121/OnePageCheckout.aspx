﻿<%@ Page Language="C#" Inherits="BizCart.BizPage"  MaintainScrollPositionOnPostback="true"  %>
<%@ Register TagPrefix="url" Namespace="Intelligencia.UrlRewriter" Assembly="Intelligencia.UrlRewriter" %>
<!DOCTYPE html>
<script runat="server"> 
</script> 
<html xmlns="http://www.w3.org/1999/xhtml"> 
<head runat="server"><title>OnePage Checkout</title> <meta name="keywords" content=""> <meta name="description" content=""><link rel="stylesheet" type="text/css" href="/PagingDarkStyle.css" /><link rel="stylesheet" type="text/css" href="/default.css" /><link rel="stylesheet" type="text/css" href="/smart_wizard.css" /><link rel="stylesheet" type="text/css" href="/SliderStyle.css" /><link rel="stylesheet" type="text/css" href="/bootstrap.min.css" /><script type="text/javascript" src="/Js/Common.js" ></script><script type="text/javascript" src="/Js/jquery-1.9.1.min.js" ></script><script type="text/javascript" src="/Js/jquery.MetaData.js" ></script><script type="text/javascript" src="/Js/jquery.rating.js" ></script><script type="text/javascript" src="/Js/jquery.jqzoom-core.js" ></script><script type="text/javascript" src="/Js/jquery.ae.image.resize.min.js" ></script><script type="text/javascript" src="/Js/jquery.review.js" ></script><script type="text/javascript" src="/Js/jquery.smartWizard-2.0.js" ></script><script type="text/javascript" src="/Js/bootstrap.min.js" ></script><script type="text/javascript" src="/Js/jquery-loader.js" ></script></head> 
<body> 
    <url:form runat="server" id="form1">
<title>test1234:</title>
<center>
<table cellpadding="0" cellspacing="3" class="MainPanel" border="0">
    <tbody>
        <tr>
            <td valign="top">
            <!--header starts-->
            <table width="100%" border="0" cellpadding="0" cellspacing="3">
    <tbody>
        <tr>
            <td id="header">
            <h1>
            test1234<sup>beta</sup></h1>
            <h2>
            by bizautomation</h2>
            </td>
            <td align="right" style="width: 458px;"> <%@ Register src="~/UserControls/LogInStatus.ascx" tagname="LoginStatus" tagprefix="uc1" %> <uc1:LoginStatus ID="LoginStatus6" runat="server"  LogOutLandingPageURL="" HtmlCustomize="{##LogoutSection##}
Welcome: ##CustomerName## | &lt;a class=&quot;LoginLink&quot; href=&quot;/Account.aspx&quot;&gt;My Account&lt;/a&gt;| &lt;a id=&quot;LogoutLink&quot; class=&quot;LogOutLink&quot; href=&quot;javascript:__doPostBack(&#39;LogoutPostback&#39;,&#39;&#39;)&quot;&gt;Log out&lt;/a&gt; 
{/##LogoutSection##} 
{##LoginSection##}
&lt;a class=&quot;LoginLink&quot; href=&quot;/Login.aspx&quot;&gt;Login&lt;/a&gt; | &lt;a class=&quot;SingupLink&quot; href=&quot;/SignUp.aspx&quot;&gt;Create Account&lt;/a&gt;
{/##LoginSection##}
&lt;br /&gt;
&lt;b&gt;&lt;a href=&quot;/Cart.aspx&quot; class=&quot;dropdown-toggle&quot; aria-controls=&quot;divMiniCart&quot; data-toggle=&quot;dropdown&quot;&gt;##ItemCount## Item(s)&lt;/a&gt; (##TotalAmount##)&lt;/b&gt;"   />  <%@ Register src="~/UserControls/MiniCart.ascx" tagname="MiniCart" tagprefix="uc1" %> <uc1:MiniCart ID="MiniCart8" runat="server"  HtmlCustomize="&lt;link rel=&quot;stylesheet&quot; href=&quot;https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css&quot;&gt;
  &lt;script src=&quot;https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js&quot;&gt;&lt;/script&gt;
  &lt;script src=&quot;https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js&quot;&gt;&lt;/script&gt;
&lt;style type=&quot;text/css&quot;&gt;
    #tblBasket tr th {
        text-align:center !important;
    }

    #tblBasket tr th:first-child {
        width:85px;
    }

    #tblBasket tr td:nth-child(3) {
        width:90px;
        text-align:center;
    }

     #tblBasket tr td:nth-child(4) {
        width:130px;
        text-align:center;
    }

     #tblBasket tr td:nth-child(5) {
        width:130px;
        text-align:center;
    }

    #tblBasket tr td {
        vertical-align:middle;
    }
&lt;/style&gt;
&lt;script type=&quot;text/javascript&quot;&gt;
    function ShowMiniCart() {
        $(&quot;#divMiniCart&quot;).show();
    }
&lt;/script&gt;

&lt;div id=&quot;divMiniCart&quot; class=&quot;dropdown-menu&quot;&gt;
    &lt;div class=&quot;modal-dialog modal-dialog-minicart&quot; style=&quot;width:70%&quot;&gt;
        &lt;!-- Modal content--&gt;
        &lt;div class=&quot;modal-content modal-content-minicart&quot;&gt;
            &lt;div class=&quot;modal-header modal-content-minicart&quot;&gt;
                &lt;div class=&quot;pull-left&quot;&gt;
                    &lt;h4 class=&quot;modal-title modal-title-minicart&quot;&gt;
                        &lt;img id=&quot;imgShopping&quot; src=&quot;/images/shopping_cart.gif&quot; align=&quot;absmiddle&quot; style=&quot;border-width: 0px;&quot;&gt;
                        Your Cart
                    &lt;/h4&gt;
                &lt;/div&gt;
                &lt;button type=&quot;button&quot; class=&quot;close&quot; data-dismiss=&quot;modal&quot;&gt;&#215;&lt;/button&gt;
            &lt;/div&gt;
            &lt;div class=&quot;modal-body modal-body-minicart&quot;&gt;
                &lt;div class=&quot;row&quot;&gt;
                    &lt;div class=&quot;col-xs-12&quot;&gt;
                        &lt;div class=&quot;table-responsive&quot;&gt;
                            &lt;table class=&quot;table table-bordered table-striped&quot; id=&quot;tblBasket&quot;&gt;
                                &lt;thead&gt;
                                    &lt;tr&gt;
                                        &lt;th&gt;&lt;/th&gt;
                                        &lt;th&gt;Item&lt;/th&gt;
                                        &lt;th&gt;Qty&lt;/th&gt;
                                        &lt;th&gt;Unit Price&lt;/th&gt;
                                        &lt;th&gt;Total&lt;/th&gt;
                                    &lt;/tr&gt;
                                &lt;/thead&gt;
                                &lt;##FOR(ItemClass=YourClassName;AlternatingItemClass=YourClassName;)##&gt;
                                &lt;tr class=&quot;##RowClass##&quot;&gt;
                                    &lt;td&gt;
                                        &lt;img src=&quot;##ItemImage##&quot; alt=&quot;&quot; style=&quot;width:80px;&quot; class=&quot;img-responsive&quot; /&gt;
                                    &lt;/td&gt;
                                    &lt;td&gt;
                                        &lt;div class=&quot;pull-left&quot;&gt;
                                            &lt;a href=&quot;##ItemLink##&quot;&gt;##ItemName##&lt;/a&gt;
                                        &lt;/div&gt;
                                        &lt;div class=&quot;pull-right&quot;&gt;
                                            ##ItemAttributes##
                                        &lt;/div&gt;
                                    &lt;/td&gt;
                                    &lt;td&gt;##ItemUnits##
                                    &lt;/td&gt;
                                    &lt;td&gt;##CurrencySymbol## ##ItemPricePerUnit##
                                    &lt;/td&gt;
                                    &lt;td&gt;##CurrencySymbol## ##TotalAmount##
                                    &lt;/td&gt;
                                &lt;/tr&gt;
                                &lt;##ENDFOR##&gt;
                            &lt;/table&gt;
                        &lt;/div&gt;
                    &lt;/div&gt;

                &lt;/div&gt;
                &lt;div class=&quot;row&quot;&gt;
                    &lt;div class=&quot;col-xs-12 pull-right&quot;&gt;
                        &lt;b&gt;SUBTOTAL: ##CurrencySymbol## ##SubTotal##&lt;/b&gt;
                    &lt;/div&gt;
                &lt;/div&gt;
            &lt;/div&gt;
            &lt;div class=&quot;modal-footer modal-footer-minicart&quot;&gt;
                &lt;a id=&quot;hplCart&quot; class=&quot;btn btn-primary pull-left&quot; href=&quot;/Cart.aspx&quot;&gt;View Cart&lt;/a&gt;
                &lt;a id=&quot;hplCheckout&quot; class=&quot;btn btn-primary pull-right&quot; href=&quot;##CheckOutURL##&quot;&gt;Check Out&lt;/a&gt;
            &lt;/div&gt;
        &lt;/div&gt;
    &lt;/div&gt;
&lt;/div&gt;"   /> <img alt="" src="/images/a10.jpg" />
            </td>
        </tr>
        <tr>
            <td colspan="2">
             <%@ Register src="~/UserControls/SiteMenu.ascx" tagname="Menu" tagprefix="uc1" %> <uc1:Menu ID="Menu7" runat="server"  MenuDirection="Horizontal" Skin="Default" PopulateULLIbasedMenu="True" HtmlCustomize="&lt;!--Remove DefaultMenu , if you are planning to use custom menu--&gt;
{##CustomMenu##}
&lt;div id=&quot;vertSiteMenu&quot;&gt;
    &lt;ul class=&quot;TreeNode&quot;&gt;
        &lt;##FOR##&gt;
        &lt;li&gt;&lt;a href=&quot;##MenuItemLink##&quot;&gt;##MenuItemName##&lt;/a&gt;&lt;/li&gt;
        &lt;##ENDFOR##&gt;
    &lt;/ul&gt;
&lt;/div&gt;
{/##CustomMenu##}"   /> 
            </td>
        </tr>
        <tr>
            <td colspan="2">
             <%@ Register src="~/UserControls/ShippingPromotion.ascx" tagname="ShippingPromotion" tagprefix="uc1" %> <uc1:ShippingPromotion ID="ShippingPromotion13" runat="server"  HtmlCustomize=""   /> &nbsp; <%@ Register src="~/UserControls/OrderPromotion.ascx" tagname="OrderPromotion" tagprefix="uc1" %> <uc1:OrderPromotion ID="OrderPromotion10" runat="server"  HtmlCustomize=""   /> </td>
        </tr>
        <tr>
            <td colspan="2">
             <%@ Register src="~/UserControls/BreadCrumb.ascx" tagname="BreadCrumb" tagprefix="uc1" %> <uc1:BreadCrumb ID="BreadCrumb1" runat="server"    /> 
            </td>
        </tr>
    </tbody>
</table>
            <!--header ends-->
            </td>
        </tr>
        <tr>
            <td valign="top">
            <table width="100%">
                <tbody>
                    <tr>
                        <td valign="top" style="width: 260px;">
                        <!--left panel starts-->
                         <%@ Register src="~/UserControls/Categories.ascx" tagname="Categories" tagprefix="uc1" %> <uc1:Categories ID="Categories2" runat="server"  ShowOnlyParentCategoriesinTree="False" PreserveStateofTree="False" HtmlCustomize="&lt;div class=&quot;boxheader&quot; id=&quot;divHeader&quot;&gt;
    Browse Categories
&lt;/div&gt;
&lt;div class=&quot;boxcontent&quot; id=&quot;divContent&quot;&gt;
    ##Menu##
    &lt;br /&gt;
    &lt;div class=&quot;left-category&quot;&gt;
        &lt;div class=&quot;catgry-hed&quot;&gt;Categories&lt;/div&gt;
        &lt;div class=&quot;category-list css-treeview&quot;&gt;
            &lt;div class=&quot;css-treeview&quot;&gt;
                &lt;##TreeViewCustom##&gt;
                &lt;ul class=&quot;TreeNodeParent&quot;&gt;
                    &lt;##FOREACH##&gt;
        		&lt;li&gt;
                    &lt;input type=&quot;checkbox&quot; id=&quot;##ITEMID##&quot;&gt;&lt;/input&gt;&lt;label for=&quot;##ITEMID##&quot;&gt;&lt;a href=&quot;/SubCategory/##CategoryName##/##CategoryID##&quot;&gt;##CategoryName##&lt;/a&gt;&lt;/label&gt;
                    &lt;##IFCHILDAVAILABLE##&gt;
        				&lt;ul class=&quot;TreeNodeChild&quot;&gt;
                            &lt;##FOREACHCHILD##&gt;
        				&lt;li&gt;
                            &lt;input type=&quot;checkbox&quot; id=&quot;Checkbox1&quot; /&gt;&lt;label for=&quot;##ITEMID##&quot;&gt;&lt;a href=&quot;/SubCategory/##CategoryName##/##CategoryID##&quot;&gt;##CategoryName##&lt;/a&gt;&lt;/label&gt;
                            &lt;##INSERTCHILDHERE##&gt;        			
                        &lt;/li&gt;
                            &lt;/##FOREACHCHILD##&gt;
                        &lt;/ul&gt;
                    &lt;/##IFCHILDAVAILABLE##&gt;
                &lt;/li&gt;
                    &lt;/##FOREACH##&gt;
                &lt;/ul&gt;
                &lt;/##TreeViewCustom##&gt;
            &lt;/div&gt;
        &lt;/div&gt;
    &lt;/div&gt;
&lt;/div&gt;" PopulateULLIBasedTree="False"   /> <br />
 <%@ Register src="~/UserControls/Search.ascx" tagname="Search" tagprefix="uc1" %> <uc1:Search ID="Search12" runat="server"  HtmlCustomize="&lt;div class=&quot;boxheader&quot;&gt;
    Search&lt;/div&gt;
&lt;div class=&quot;boxcontent&quot;&gt;
    &#160; &lt;span&gt;##SearchTextBox## &#160; ##SearchButton## &lt;/span&gt;
&lt;/div&gt;"   /> 
                        <!--left panel ends-->
                        </td>
                        <td valign="top" style="padding-left: 15px;">
                        <!--Content panel starts-->
                         <%@ Register src="~/UserControls/Error.ascx" tagname="Error" tagprefix="uc1" %> <uc1:Error ID="Error4" runat="server"    /> <br />
                        CheckOut<br />
                         <%@ Register src="~/UserControls/OnePageCheckout.ascx" tagname="OnePageCheckout" tagprefix="uc1" %> <uc1:OnePageCheckout ID="OnePageCheckout9" runat="server"  HtmlCustomize="&lt;script type=&quot;text/javascript&quot; src=&quot;js/jquery-1.4.2.min.js&quot;&gt;&lt;/script&gt;
&lt;script type=&quot;text/javascript&quot; src=&quot;js/jquery.smartWizard-2.0.js&quot;&gt;&lt;/script&gt;
&lt;script type=&quot;text/javascript&quot;&gt;
    $(document).ready(function () {
        // Smart Wizard 	
        $(&#39;#wizard&#39;).smartWizard();
        $(&quot;.summaryheader&gt;a&quot;).hide();
        function onFinishCallback() {
            $(&#39;#wizard&#39;).smartWizard(&#39;showMessage&#39;, &#39;Finish Clicked&#39;);
        }
    });
&lt;/script&gt;
&lt;table align=&quot;center&quot; border=&quot;0&quot; cellpadding=&quot;0&quot; cellspacing=&quot;0&quot;&gt;
    &lt;tbody&gt;
        &lt;tr&gt;
            &lt;td&gt;
                &lt;div id=&quot;wizard&quot; class=&quot;swMain&quot;&gt;
                    &lt;ul&gt;
                        &lt;li&gt;&lt;a href=&quot;#step-1&quot;&gt;
                            &lt;label class=&quot;stepNumber&quot;&gt;
                                1&lt;/label&gt;
                            &lt;span class=&quot;stepDesc&quot;&gt;Register&lt;br /&gt;
                                &lt;small&gt;Login or Register&lt;/small&gt; &lt;/span&gt;&lt;/a&gt;&lt;/li&gt;
                        &lt;li&gt;&lt;a href=&quot;#step-2&quot;&gt;
                            &lt;label class=&quot;stepNumber&quot;&gt;
                                2&lt;/label&gt;
                            &lt;span class=&quot;stepDesc&quot;&gt;Address&lt;br /&gt;
                                &lt;small&gt;Billing &amp; Shipping Address&lt;/small&gt; &lt;/span&gt;&lt;/a&gt;&lt;/li&gt;
                        &lt;li&gt;&lt;a href=&quot;#step-3&quot;&gt;
                            &lt;label class=&quot;stepNumber&quot;&gt;
                                3&lt;/label&gt;
                            &lt;span class=&quot;stepDesc&quot;&gt;Order Summary&lt;br /&gt;
                                &lt;small&gt;Review and Place Order&lt;/small&gt; &lt;/span&gt;&lt;/a&gt;&lt;/li&gt;
                    &lt;/ul&gt;
                    &lt;div id=&quot;step-1&quot;&gt;
                        &lt;!-- &lt;h2 class=&quot;StepTitle&quot;&gt;
                            Step 1 Content&lt;/h2&gt;--&gt;
                        &lt;p&gt;
                            {#Login#}&lt;/p&gt;
                    &lt;/div&gt;
                    &lt;div id=&quot;step-2&quot;&gt;
                        &lt;!--&lt;h2 class=&quot;StepTitle&quot;&gt;
                            Step 2 Content&lt;/h2&gt;--&gt;
                        &lt;p&gt;
                            {#ConfirmAddress#}
                        &lt;/p&gt;
                    &lt;/div&gt;
                    &lt;div id=&quot;step-3&quot;&gt;
                        &lt;!--&lt;h2 class=&quot;StepTitle&quot;&gt;
                            Step 3 Content&lt;/h2&gt;--&gt;
                        &lt;p&gt;
                            
                        &lt;/p&gt;
                    &lt;/div&gt;
                &lt;/div&gt;
            &lt;/td&gt;
        &lt;/tr&gt;
    &lt;/tbody&gt;
&lt;/table&gt;"   /> <br />
                        <script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>
<script type="text/javascript" src="js/jquery.smartWizard-2.0.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        // Smart Wizard 	
        $('#wizard').smartWizard();
        $(".summaryheader>a").hide();
        function onFinishCallback() {
            $('#wizard').smartWizard('showMessage', 'Finish Clicked');
        }
    });
</script>
<table align="center" border="0" cellpadding="0" cellspacing="0">
    <tbody>
        <tr>
            <td>
                <div id="wizard" class="swMain">
                    <ul>
                        <li><a href="#step-1">
                            <label class="stepNumber">
                                1</label>
                            <span class="stepDesc">Register<br />
                                <small>Login or Register</small> </span></a></li>
                        <li><a href="#step-2">
                            <label class="stepNumber">
                                2</label>
                            <span class="stepDesc">Address<br />
                                <small>Billing & Shipping Address</small> </span></a></li>
                        <li><a href="#step-3">
                            <label class="stepNumber">
                                3</label>
                            <span class="stepDesc">Order Summary<br />
                                <small>Review and Place Order</small> </span></a></li>
                    </ul>
                    <div id="step-1">
                        <!-- <h2 class="StepTitle">
                            Step 1 Content</h2>-->
                        <p>
                             <%@ Register src="~/UserControls/Login.ascx" tagname="Login" tagprefix="uc1" %> <uc1:Login ID="Login5" runat="server"  RedirecttoURLAfterSucessfullLogin="" HtmlCustomize="&lt;table&gt;
    &lt;tr&gt;
        &lt;td class=&quot;normal4&quot;&gt;
            ##ErrorMessage##
        &lt;/td&gt;
    &lt;/tr&gt;
&lt;/table&gt;
&lt;table id=&quot;tblLogin&quot; runat=&quot;server&quot; border=&quot;0&quot; width=&quot;100%&quot; cellpadding=&quot;0&quot; cellspacing=&quot;2&quot;&gt;
    &lt;tr&gt;
        &lt;td colspan=&quot;2&quot;&gt;
            &lt;div class=&quot;sectionheader&quot;&gt;
                Login
            &lt;/div&gt;
        &lt;/td&gt;
    &lt;/tr&gt;
    &lt;tr&gt;
        &lt;td class=&quot;LabelColumn&quot;&gt;
            Email Address
        &lt;/td&gt;
        &lt;td class=&quot;ControlCell&quot;&gt;
            ##EmaillAdd##
        &lt;/td&gt;
    &lt;/tr&gt;
    &lt;tr&gt;
        &lt;td class=&quot;LabelColumn&quot;&gt;
            Password
        &lt;/td&gt;
        &lt;td class=&quot;ControlCell&quot;&gt;
            ##Password##
        &lt;/td&gt;
    &lt;/tr&gt;
    &lt;tr&gt;
        &lt;td&gt;
        &lt;/td&gt;
        &lt;td class=&quot;ControlCell&quot;&gt;
            ##LoginButton## ##ForgotPasswordButton## 
        &lt;/td&gt;
    &lt;/tr&gt;
    &lt;tr&gt;
        &lt;td&gt;
        &lt;/td&gt;
        &lt;td class=&quot;ControlCell&quot;&gt;
            ##SignUpButton## &lt;a class=&quot;btn&quot; href=&quot;/GuestCheckout.aspx&quot;&gt;Guest Checkout&lt;/a&gt;
        &lt;/td&gt;
    &lt;/tr&gt;
&lt;/table&gt;"   /> </p>
                    </div>
                    <div id="step-2">
                        <!--<h2 class="StepTitle">
                            Step 2 Content</h2>-->
                        <p>
                             <%@ Register src="~/UserControls/CustomerDetail.ascx" tagname="ConfirmAddress" tagprefix="uc1" %> <uc1:ConfirmAddress ID="ConfirmAddress3" runat="server"  HtmlCustomize=""   /> 
                        </p>
                    </div>
                    <div id="step-3">
                        <!--<h2 class="StepTitle">
                            Step 3 Content</h2>-->
                        <p>
                             <%@ Register src="~/UserControls/OrderSummary.ascx" tagname="OrderSummary" tagprefix="uc1" %> <uc1:OrderSummary ID="OrderSummary11" runat="server"  HtmlCustomize="&lt;div id=&quot;UpdateProgress&quot; style=&quot;display: none;&quot; role=&quot;status&quot; aria-hidden=&quot;true&quot;&gt;
    &lt;div class=&quot;overlay&quot;&gt;
        &lt;div class=&quot;overlayContent&quot; style=&quot;color: #000;text-align: center;width: 660px;padding: 20px;background-color: #FFF;&quot;&gt;
            &lt;i class=&quot;fa fa-2x fa-refresh fa-spin&quot;&gt;&lt;/i&gt;
            &lt;h4 id=&quot;hProccessing&quot;&gt;Please wait a few seconds while your order is being processed.&lt;/h4&gt;
        &lt;/div&gt;
    &lt;/div&gt;
&lt;/div&gt;
&lt;div class=&quot;cart-bg&quot;&gt;
    &lt;div class=&quot;basket_header&quot;&gt;
        &lt;div class=&quot;row&quot;&gt;
            &lt;div class=&quot;col-md-8 col-sm-8 col-xs-8&quot;&gt;
             &lt;h1&gt;Order Summary&lt;/h1&gt;
            &lt;/div&gt;
            &lt;div class=&quot;col-md-4 col-sm-4 col-xs-4 right&quot;&gt;
           
            &lt;/div&gt;
      &lt;/div&gt;
    &lt;/div&gt;
    &lt;div class=&quot;row row-fluid&quot;&gt;
        &lt;div class=&quot;col-md-8 col-sm-8 col-xs-8&quot;&gt;
             &lt;h4&gt;Customer Information:&lt;/h4&gt; 
			 &lt;b&gt;##FirstName## ##LastName##&#160; ##Email## ##Phone##&lt;/b&gt;
        &lt;/div&gt;
		&lt;div class=&quot;col-md-4 col-sm-4 col-xs-4 right&quot;  style=&quot;text-align: right; margin-top: 15px;&quot;&gt;
             &lt;i&gt;##CustomerInformationLink## &lt;/i&gt;
        &lt;/div&gt;
    &lt;/div&gt;
	&lt;div class=&quot;row row-fluid&quot;&gt;
        &lt;div class=&quot;col-md-8 col-sm-8 col-xs-8&quot;&gt;
             &lt;h4&gt;Address:&lt;/h4&gt;
			 &lt;b&gt;&lt;strong&gt;Billing Address&lt;/strong&gt;: ##BillingAddress##&lt;/b&gt;
			 &lt;b&gt;&lt;strong&gt;Shipping Address&lt;/strong&gt;: ##ShippingAddress##&lt;/b&gt;
        &lt;/div&gt;
		&lt;div class=&quot;col-md-4 col-sm-4 col-xs-4 right&quot; style=&quot;text-align: right; margin-top: 15px;&quot;&gt;
             &lt;i&gt;##AddressLink##&lt;/i&gt;
        &lt;/div&gt;
    &lt;/div&gt;
	&lt;div class=&quot;row row-fluid&quot;&gt;
        &lt;div class=&quot;col-md-8 col-sm-8 col-xs-8&quot;&gt;
             &lt;h4&gt;Shipping Information:&lt;/h4&gt;
        &lt;/div&gt;
		&lt;div class=&quot;col-md-4 col-sm-4 col-xs-4 right&quot; style=&quot;text-align: right; margin-top: 15px;&quot;&gt;
             &lt;i&gt;##CartLink##&lt;/i&gt;
        &lt;/div&gt;
    &lt;/div&gt;
	
	
    &lt;div class=&quot;prod-row&quot; id=&quot;no-more-tables&quot;&gt;
        &lt;div class=&quot;component_row&quot;&gt;
        &lt;/div&gt;
        &lt;div class=&quot;component_row&quot;&gt;
        &lt;/div&gt;
        &lt;table class=&quot;table&quot;&gt;
          &lt;thead&gt;
              &lt;tr&gt;
                &lt;th&gt;Item&lt;/th&gt;
                &lt;th class=&quot;text-center&quot;&gt;Attributes&lt;/th&gt;
                &lt;th class=&quot;text-center&quot;&gt;Units&lt;/th&gt;
                &lt;th class=&quot;text-center&quot;&gt;Price/Unit&lt;/th&gt;
				&lt;th class=&quot;text-center&quot;&gt;Discount&lt;/th&gt;
				&lt;th class=&quot;text-center&quot;&gt;Total&lt;/th&gt;
				
				&lt;th class=&quot;text-center&quot; style=&quot;display:none&quot;&gt;Item Name&lt;/th&gt;
				&lt;th class=&quot;text-center&quot; style=&quot;display:none&quot;&gt;Model ID&lt;/th&gt;
				&lt;th class=&quot;text-center&quot; style=&quot;display:none&quot;&gt;Item ID&lt;/th&gt;
				&lt;th class=&quot;text-center&quot; style=&quot;display:none&quot;&gt;Price&lt;/th&gt;
               
              &lt;/tr&gt;
          &lt;/thead&gt;
          &lt;tbody&gt;
            &lt;##FOR(ItemClass=GridItem;AlternatingItemClass=GridAltItem;)##&gt;		  
            &lt;tr class=&quot;##RowClass##&quot;&gt;
                &lt;td data-title=&quot;Item&quot;&gt;##ItemName##&lt;/td&gt;
				&lt;td class=&quot;text-center&quot; data-title=&quot;Attributes&quot;&gt;##ItemAttributes## ##SKU##&lt;/td&gt;
                &lt;td class=&quot;text-center&quot; data-title=&quot;Units&quot;&gt;##ItemUnits##&lt;/td&gt;
                &lt;td class=&quot;text-center&quot; data-title=&quot;Price/Unit&quot;&gt;&lt;strong&gt;##ItemPricePerUnit##&lt;/strong&gt;&lt;/td&gt;
				&lt;td class=&quot;text-center&quot; data-title=&quot;Discount&quot;&gt;&lt;strong&gt;##Discount##&lt;/strong&gt;&lt;/td&gt;
				&lt;td class=&quot;text-center&quot; data-title=&quot;Total&quot;&gt;&lt;strong&gt;##TotalAmount##&lt;/strong&gt;&lt;/td&gt;
				&lt;td class=&quot;text-center&quot; data-title=&quot;Item Name&quot; style=&quot;display:none&quot;&gt;&lt;strong&gt;&lt;a href=&quot;##ItemURL##&quot;&gt;##ItemName##&lt;/a&gt;&lt;/strong&gt;&lt;/td&gt;
				&lt;td class=&quot;text-center&quot; data-title=&quot;Model ID&quot; style=&quot;display:none&quot;&gt;&lt;strong&gt;##ItemModelID##&lt;/strong&gt;&lt;/td&gt;
				&lt;td class=&quot;text-center&quot; data-title=&quot;Item ID&quot; style=&quot;display:none&quot;&gt;&lt;strong&gt;##ItemID##&lt;/strong&gt;&lt;/td&gt;
				&lt;td class=&quot;text-center&quot; data-title=&quot;Price&quot; style=&quot;display:none&quot;&gt;&lt;strong&gt;##ItemPricePerUnit##&lt;/strong&gt;&lt;/td&gt;
            &lt;/tr&gt;
			&lt;##ENDFOR##&gt;
           &lt;/tbody&gt;
      &lt;/table&gt;

&lt;/div&gt;
&lt;div class=&quot;basket_total&quot;&gt;
      &lt;div class=&quot;row row-fluid&quot;&gt;
         &lt;div class=&quot;col-md-4 col-sm-6 col-xs-6&quot;&gt;
            
         &lt;/div&gt;
         &lt;div class=&quot;col-md-8 col-sm-6 col-xs-6 right&quot;&gt;
           &lt;h4 class=&quot;blue_txt&quot;&gt;SubTotal: ##SubTotalLabel##&lt;/h4&gt;
           &lt;h4 class=&quot;blue_txt&quot;&gt;Discount: ##DiscountLabel##&lt;/h4&gt;
		   &lt;h4 class=&quot;blue_txt&quot;&gt;Tax: ##TaxLabel##&lt;/h4&gt;
		   &lt;h4 class=&quot;blue_txt&quot;&gt;##ShowShippingAmountWithMethod## ##ShippingMethodDropdown## Shipping Charges: ##ShippingChargeLabel##&lt;/h4&gt;
           &lt;h3 class=&quot;blue_txt&quot;&gt;Total: ##TotalAmountLabel##&lt;/h3&gt;
           
         &lt;/div&gt;
      &lt;/div&gt;
&lt;/div&gt;
&lt;div class=&quot;proceed_row&quot;&gt;
    &lt;div class=&quot;row1&quot; style=&quot;width: inherit;&quot;&gt;
        &lt;div class=&quot;col-md-8 col-sm-6&quot;&gt;
			&lt;div class=&quot;radio&quot;&gt;
				&lt;input id=&quot;Checkout_radBillMe&quot; name=&quot;PayOption&quot; value=&quot;BillMe&quot; ##IsBillMeChecked## type=&quot;radio&quot; onclick=&quot;javascript: GetPaymentMethod(&#39;BillMe&#39;);&quot; /&gt;
				&lt;label for=&quot;Checkout_radBillMe&quot;&gt;&#160;&lt;b&gt;Bill Me&lt;/b&gt; &#160;(If you or your company has terms with us.)&lt;/label&gt;&lt;br /&gt;
			&lt;/div&gt;	 
			&lt;form class=&quot;form-horizontal&quot; id =&quot;BillMeSection&quot; style =&quot;display:none&quot; role=&quot;form&quot;&gt;
				&lt;div class=&quot;form-group&quot;&gt;
					&lt;label for=&quot;inputtoatalbal&quot; class=&quot;col-sm-4 control-label&quot;&gt;Total Balance Due :&lt;/label&gt;
						&lt;div class=&quot;col-sm-8&quot; style=&quot;margin-top: 6px;padding-left: 6px;font-weight: bold;&quot;&gt;
						##TotalBalanceDue##
						&lt;/div&gt;
				&lt;/div&gt;
				
				&lt;div class=&quot;form-group&quot;&gt;
					&lt;label for=&quot;inputtoatalcredit&quot; class=&quot;col-sm-4 control-label&quot;&gt;Total Remaining Credit :&lt;/label&gt;
					&lt;div class=&quot;col-sm-8&quot; style=&quot;margin-top: 6px;padding-left: 6px;font-weight: bold;&quot;&gt;
					##TotalRemaniningCredit##
					&lt;/div&gt;
				&lt;/div&gt;
				&lt;div class=&quot;form-group&quot;&gt;
					&lt;label for=&quot;inputtoatalpastdue&quot; class=&quot;col-sm-4 control-label&quot;&gt;Total Amount Past Due :&lt;/label&gt;
					&lt;div class=&quot;col-sm-8&quot; style=&quot;margin-top: 6px;padding-left: 6px;font-weight: bold;&quot;&gt;
					##TotalAmountPastDue##
					&lt;/div&gt;
				&lt;/div&gt;
			&lt;/form&gt;
			
			&lt;div class=&quot;radio&quot;&gt;	
				&lt;input id=&quot;Checkout_radPay&quot; name=&quot;PayOption&quot; value=&quot;CreditCard&quot; ##IsPayChecked## onclick=&quot;javascript: GetPaymentMethod(&#39;CreditCard&#39;);&quot; type=&quot;radio&quot; /&gt;
				&lt;label for=&quot;Checkout_radPay&quot;&gt;&#160;&lt;b&gt;Pay by Credit Card&lt;/b&gt;&lt;/label&gt;
			&lt;/div&gt;	
			&lt;div id=&quot;CreditCardSection&quot; style=&quot;display:none;&quot;&gt;
                 &lt;table id=&quot;tblCustomerInfo&quot; width=&quot;90%&quot;&gt;
                &lt;tr&gt;
                    &lt;td width=&quot;50%&quot; &gt;
                        &lt;table cellspacing=&quot;2&quot; cellpadding=&quot;5&quot; style=&quot;padding-left:20px;&quot;&gt;
                            &lt;tr&gt;
                                &lt;th style=&quot;text-wrap:none; text-align:left&quot; colspan=&quot;4&quot;&gt;
                                    Credit Card Information
                                &lt;/th&gt;
                            &lt;/tr&gt;
                            &lt;tr&gt;
                                &lt;td class=&quot;LabelColumn&quot;&gt;
                                    Card Type:
                                &lt;/td&gt;
                                &lt;td class=&quot;ControlCell&quot;&gt;
                                    ##CardTypeDropDownList##
                                &lt;/td&gt;
                                &lt;td class=&quot;LabelColumn&quot;&gt;
                                    Card Number:
                                &lt;/td&gt;
                                &lt;td class=&quot;ControlCell&quot;&gt;
                                    ##CardNumberTextBox##
                                &lt;/td&gt;
                            &lt;/tr&gt;
                            &lt;tr&gt;
                                &lt;td class=&quot;LabelColumn&quot;&gt;
                                Card Holder Name: 
                                &lt;/td&gt;
                                &lt;td class=&quot;ControlCell&quot;&gt;
                                    ##CardHolderTextBox##
                                &lt;/td&gt;
                                &lt;td class=&quot;LabelColumn&quot; &gt;
                                    CVV2 Data:
                                &lt;/td&gt;
                                &lt;td class=&quot;ControlCell&quot;&gt;
                                    ##CVV2TextBox##
                                &lt;/td&gt;
                            &lt;/tr&gt;
                            &lt;tr&gt;
                                &lt;td class=&quot;LabelColumn&quot;&gt;
                                    Exp Date (MM/YY):
                                &lt;/td&gt;
                                &lt;td class=&quot;ControlCell&quot;&gt;
                                    ##CardExpMonthDropDownList## &#160;/&#160; ##CardExpYearDropDownList##
                                &lt;/td&gt;
                                &lt;td nowrap align=&quot;right&quot; &gt;
                                &lt;/td&gt;
                                &lt;td&gt;
                                &lt;/td&gt;
                            &lt;/tr&gt;
                        &lt;/table&gt;
                    &lt;/td&gt;
                &lt;/tr&gt;
            &lt;/table&gt;

            &lt;/div&gt;
			
			&lt;div class=&quot;radio&quot;&gt;
				&lt;input id=&quot;Checkout_radGoogleCheckOut&quot; name=&quot;PayOption&quot; value=&quot;GoogleCheckout&quot; ##IsGoogleCheckoutChecked## type=&quot;radio&quot; onclick=&quot;javascript: GetPaymentMethod(&#39;GoogleCheckout&#39;);&quot; /&gt;
				&lt;label for=&quot;Checkout_radGoogleCheckOut&quot;&gt;&#160;&lt;b&gt;Google Checkout&lt;/b&gt; &lt;/label&gt;&lt;br /&gt;
			&lt;/div&gt;
			
			&lt;div class=&quot;radio&quot;&gt;
				&lt;input id=&quot;Checkout_radPaypalCheckOut&quot; name=&quot;PayOption&quot; value=&quot;PaypalCheckout&quot; ##IsPaypalCheckoutChecked##
                type=&quot;radio&quot; onclick=&quot;javascript: GetPaymentMethod(&#39;Paypal&#39;);&quot; /&gt;
				&lt;label for=&quot;Checkout_radPaypalCheckOut&quot;&gt;&#160;&lt;b&gt;Pay by Paypal&lt;/b&gt; &lt;/label&gt;
			&lt;/div&gt;		
			&lt;form class=&quot;form-horizontal&quot; id=&quot;PaypalSection&quot; style=&quot;display:none;&quot; role=&quot;form&quot;&gt;
				&lt;div id=&quot;tblPaypal&quot;&gt; 
					&lt;h5 style=&quot;margin-left: 50px;font-weight: bold;&quot;&gt;Pay By Paypal&lt;/h5&gt;
						&lt;div class=&quot;form-group&quot;&gt;
							&lt;label for=&quot;inputtoatalcredit&quot; class=&quot;col-sm-4 control-label&quot;&gt;Paypal Email :&lt;/label&gt;
							&lt;div class=&quot;col-sm-8&quot; style=&quot;margin-top: -5px;padding-left: 6px;&quot;&gt;
							##PaypalEmail##
							&lt;/div&gt;
						&lt;/div&gt; 
						&lt;!-- &lt;div class=&quot;form-group&quot;&gt;
							&lt;label for=&quot;inputtoatalcredit&quot; class=&quot;col-sm-4 control-label&quot;&gt;Password:&lt;/label&gt;
								&lt;div class=&quot;col-sm-8&quot; style=&quot;margin-top: 8px;padding-left: 6px;&quot;&gt;
								##PaypalPassword##
								&lt;/div&gt;
						&lt;/div&gt;  --&gt;
						&lt;div class=&quot;form-group&quot;&gt;
							&lt;label for=&quot;inputtoatalcredit&quot; class=&quot;col-sm-4 control-label&quot;&gt;Total Amount :&lt;/label&gt;
							&lt;div class=&quot;col-sm-8&quot; style=&quot;margin-top: 6px;padding-left: 6px;font-weight: bold&quot;&gt;
							##TotalAmount##
							&lt;/div&gt;
						&lt;/div&gt; 
				&lt;/div&gt;
			&lt;/form&gt;
        &lt;/div&gt;
        &lt;table align=&quot;left&quot; width=&quot;100%&quot;&gt;
                {##EnterCouponSection##}
                &lt;tr id=&quot;trApplyCouponCode&quot;&gt;
                    &lt;td class=&quot;text_bold&quot;&gt;
                        &lt;br /&gt;
                        Apply Coupon/Discount Code: ##CouponCodeTextBox## ##ApplyCouponCodeButton##
                    &lt;/td&gt;
                &lt;/tr&gt;
                {/##EnterCouponSection##} {##UsedCouponSection##}
                &lt;tr id=&quot;trUsedCouponCode&quot;&gt;
                    &lt;td class=&quot;text_bold&quot;&gt;
                        &lt;br /&gt;
                        Coupon/Discount Code: ##CouponCodeLabel## ##RemoveCouponCodeLink##
                    &lt;/td&gt;
                &lt;/tr&gt;
                {/##UsedCouponSection##}
            &lt;/table&gt;
            
##ShipVia##&lt;br/&gt;
##CustomerPONumber##
##TestDropDown_C##
        &lt;div class=&quot;col-md-4 col-sm-6 right&quot;&gt;
           &lt;h3 class=&quot;blue_txt&quot;&gt;Total Amount: ##TotalAmount##&lt;/h3&gt;
		   &lt;h4&gt; if you have any Notes regarding order,type them here:&lt;/h4&gt;  ##Comments##
		   ##SubmitButton## &#160; ##ViewPastOrdersButton##
        &lt;/div&gt;
    &lt;/div&gt;
 &lt;/div&gt;
&lt;/div&gt;
&lt;div id=&quot;##ShippingMethodModelID##&quot; class=&quot;modal fade modal-shippingmethod&quot; role=&quot;dialog&quot;&gt;
    &lt;div class=&quot;modal-dialog modal-dialog-shippingmethod&quot; style=&quot;width: 70%&quot;&gt;
        &lt;!-- Modal content--&gt;
        &lt;div class=&quot;modal-content modal-content-shippingmethod&quot;&gt;
            &lt;div class=&quot;modal-header modal-content-shippingmethod&quot;&gt;
                &lt;div class=&quot;pull-left&quot;&gt;
                    Shipping Service(s)
                &lt;/div&gt;
                &lt;div class=&quot;pull-right&quot;&gt;
                    &lt;button type=&quot;button&quot; class=&quot;btn btn-primary&quot; onclick=&quot;ShippingRateChanged();&quot;&gt;Use selected rate instead&lt;/button&gt;
                    &lt;button type=&quot;button&quot; data-dismiss=&quot;modal&quot; class=&quot;btn btn-primary&quot;&gt;Keep my existing rate&lt;/button&gt;
                &lt;/div&gt;
            &lt;/div&gt;
            &lt;div class=&quot;modal-body modal-body-shippingmethod&quot;&gt;
                &lt;div class=&quot;row&quot;&gt;
                    &lt;div class=&quot;col-xs-12&quot;&gt;
                        &lt;div class=&quot;alert alert-info&quot;&gt;
                          Keep your existing shipping rate or replace it with one of the following carriers / services.
                        &lt;/div&gt;
                    &lt;/div&gt;
                    &lt;div class=&quot;col-xs-12&quot; id=&quot;##divShippingRates##&quot;&gt;
                        
                    &lt;/div&gt;
                &lt;/div&gt;
            &lt;/div&gt;
        &lt;/div&gt;
    &lt;/div&gt;
&lt;/div&gt;"   /> 
                        </p>
                    </div>
                </div>
            </td>
        </tr>
    </tbody>
</table>
<br />
                        </td>
                    </tr>
                </tbody>
            </table>
            </td>
        </tr>
        <tr>
            <td>
            <!--footer starts-->
            <div id="footer">
<a href="#">Temrs &amp; Conditions</a>&nbsp;&nbsp;&nbsp;<a href="#">Newsletters</a>&nbsp;&nbsp;&nbsp;<a href="#">Sitemap</a><br />
&copy; test1234. All rights reserved. Powered by <a href="http://www.BizAutomation.com">
BizAutomation</a>.
</div>
<script src="/Js/jquery-loader.js" type="text/javascript"></script>
            <!--footer ends-->
            </td>
        </tr>
    </tbody>
</table>
</center></url:form> 
</body> 
</html>