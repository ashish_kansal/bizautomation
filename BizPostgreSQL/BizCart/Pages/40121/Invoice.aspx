﻿<%@ Page Language="C#" Inherits="BizCart.BizPage"  %>
<%@ Register TagPrefix="url" Namespace="Intelligencia.UrlRewriter" Assembly="Intelligencia.UrlRewriter" %>
<!DOCTYPE html>
<script runat="server"> 
</script> 
<html xmlns="http://www.w3.org/1999/xhtml"> 
<head runat="server"><title>Invoice</title> <meta name="keywords" content=""> <meta name="description" content=""><link rel="stylesheet" type="text/css" href="/PagingDarkStyle.css" /><link rel="stylesheet" type="text/css" href="/default.css" /><link rel="stylesheet" type="text/css" href="/smart_wizard.css" /><link rel="stylesheet" type="text/css" href="/SliderStyle.css" /><script type="text/javascript" src="/Js/Common.js" ></script><script type="text/javascript" src="/Js/jquery-ui.min.js" ></script><script type="text/javascript" src="/Js/jquery.js" ></script><script type="text/javascript" src="/Js/jquery.MetaData.js" ></script><script type="text/javascript" src="/Js/jquery.rating.js" ></script><script type="text/javascript" src="/Js/jquery.jqzoom-core.js" ></script><script type="text/javascript" src="/Js/jquery.ae.image.resize.min.js" ></script><script type="text/javascript" src="/Js/jquery.review.js" ></script><script type="text/javascript" src="/Js/jquery.smartWizard-2.0.js" ></script></head> 
<body> 
    <url:form runat="server" id="form1">
    <table cellpadding="0" cellspacing="3" class="MainPanel" border="0">
        <tr>
            <td valign="top">
                <table width="100%">
                    <tr>
                        <td valign="top" style="padding-left: 15px;">
                            <!--Content panel starts-->
                             <%@ Register src="~/UserControls/Error.ascx" tagname="Error" tagprefix="uc1" %> <uc1:Error ID="Error1" runat="server"    /> <br>
                             <%@ Register src="~/UserControls/Invoice.ascx" tagname="Invoice" tagprefix="uc1" %> <uc1:Invoice ID="Invoice2" runat="server"    /> 
                            <!--Content panel ends-->
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</url:form> 
</body> 
</html>