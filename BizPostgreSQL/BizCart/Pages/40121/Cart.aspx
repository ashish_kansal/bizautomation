﻿<%@ Page Language="C#" Inherits="BizCart.BizPage"  MaintainScrollPositionOnPostback="true"  %>
<%@ Register TagPrefix="url" Namespace="Intelligencia.UrlRewriter" Assembly="Intelligencia.UrlRewriter" %>
<!DOCTYPE html>
<script runat="server"> 
</script> 
<html xmlns="http://www.w3.org/1999/xhtml"> 
<head runat="server"><title>View & Update Cart</title> <meta name="keywords" content=""> <meta name="description" content=""><link rel="stylesheet" type="text/css" href="/PagingDarkStyle.css" /><link rel="stylesheet" type="text/css" href="/default.css" /><link rel="stylesheet" type="text/css" href="/smart_wizard.css" /><link rel="stylesheet" type="text/css" href="/SliderStyle.css" /><script type="text/javascript" src="/Js/Common.js" ></script><script type="text/javascript" src="/Js/jquery-1.9.1.min.js" ></script><script type="text/javascript" src="/Js/jquery-ui.min.js" ></script><script type="text/javascript" src="/Js/jquery.js" ></script><script type="text/javascript" src="/Js/jquery.MetaData.js" ></script><script type="text/javascript" src="/Js/jquery.rating.js" ></script><script type="text/javascript" src="/Js/jquery.jqzoom-core.js" ></script><script type="text/javascript" src="/Js/jquery.ae.image.resize.min.js" ></script><script type="text/javascript" src="/Js/jquery.review.js" ></script><script type="text/javascript" src="/Js/jquery.smartWizard-2.0.js" ></script><script type="text/javascript" src="/Js/jquery-loader.js" ></script></head> 
<body> 
    <url:form runat="server" id="form1">
<title>test1234:</title>
<center>
    <table cellpadding="0" cellspacing="3" class="MainPanel" border="0">
        <tr>
            <td valign="top">
                <!--header starts-->
                <table width="100%" border="0" cellpadding="0" cellspacing="3">
    <tbody>
        <tr>
            <td id="header">
            <h1>
            test1234<sup>beta</sup></h1>
            <h2>
            by bizautomation</h2>
            </td>
            <td align="right" style="width: 458px;"> <%@ Register src="~/UserControls/LogInStatus.ascx" tagname="LoginStatus" tagprefix="uc1" %> <uc1:LoginStatus ID="LoginStatus5" runat="server"  LogOutLandingPageURL="" HtmlCustomize="{##LogoutSection##}
Welcome: ##CustomerName## | &lt;a class=&quot;LoginLink&quot; href=&quot;/Account.aspx&quot;&gt;My Account&lt;/a&gt;| &lt;a id=&quot;LogoutLink&quot; class=&quot;LogOutLink&quot; href=&quot;javascript:__doPostBack(&#39;LogoutPostback&#39;,&#39;&#39;)&quot;&gt;Log out&lt;/a&gt; 
{/##LogoutSection##} 
{##LoginSection##}
&lt;a class=&quot;LoginLink&quot; href=&quot;/Login.aspx&quot;&gt;Login&lt;/a&gt; | &lt;a class=&quot;SingupLink&quot; href=&quot;/SignUp.aspx&quot;&gt;Create Account&lt;/a&gt;
{/##LoginSection##}
&lt;br /&gt;
&lt;b&gt;&lt;a href=&quot;/Cart.aspx&quot; class=&quot;dropdown-toggle&quot; aria-controls=&quot;divMiniCart&quot; data-toggle=&quot;dropdown&quot;&gt;##ItemCount## Item(s)&lt;/a&gt; (##TotalAmount##)&lt;/b&gt;"   />  <%@ Register src="~/UserControls/MiniCart.ascx" tagname="MiniCart" tagprefix="uc1" %> <uc1:MiniCart ID="MiniCart7" runat="server"  HtmlCustomize="&lt;link rel=&quot;stylesheet&quot; href=&quot;https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css&quot;&gt;
  &lt;script src=&quot;https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js&quot;&gt;&lt;/script&gt;
  &lt;script src=&quot;https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js&quot;&gt;&lt;/script&gt;
&lt;style type=&quot;text/css&quot;&gt;
    #tblBasket tr th {
        text-align:center !important;
    }

    #tblBasket tr th:first-child {
        width:85px;
    }

    #tblBasket tr td:nth-child(3) {
        width:90px;
        text-align:center;
    }

     #tblBasket tr td:nth-child(4) {
        width:130px;
        text-align:center;
    }

     #tblBasket tr td:nth-child(5) {
        width:130px;
        text-align:center;
    }

    #tblBasket tr td {
        vertical-align:middle;
    }
&lt;/style&gt;
&lt;script type=&quot;text/javascript&quot;&gt;
    function ShowMiniCart() {
        $(&quot;#divMiniCart&quot;).show();
    }
&lt;/script&gt;

&lt;div id=&quot;divMiniCart&quot; class=&quot;dropdown-menu&quot;&gt;
    &lt;div class=&quot;modal-dialog modal-dialog-minicart&quot; style=&quot;width:70%&quot;&gt;
        &lt;!-- Modal content--&gt;
        &lt;div class=&quot;modal-content modal-content-minicart&quot;&gt;
            &lt;div class=&quot;modal-header modal-content-minicart&quot;&gt;
                &lt;div class=&quot;pull-left&quot;&gt;
                    &lt;h4 class=&quot;modal-title modal-title-minicart&quot;&gt;
                        &lt;img id=&quot;imgShopping&quot; src=&quot;/images/shopping_cart.gif&quot; align=&quot;absmiddle&quot; style=&quot;border-width: 0px;&quot;&gt;
                        Your Cart
                    &lt;/h4&gt;
                &lt;/div&gt;
                &lt;button type=&quot;button&quot; class=&quot;close&quot; data-dismiss=&quot;modal&quot;&gt;&#215;&lt;/button&gt;
            &lt;/div&gt;
            &lt;div class=&quot;modal-body modal-body-minicart&quot;&gt;
                &lt;div class=&quot;row&quot;&gt;
                    &lt;div class=&quot;col-xs-12&quot;&gt;
                        &lt;div class=&quot;table-responsive&quot;&gt;
                            &lt;table class=&quot;table table-bordered table-striped&quot; id=&quot;tblBasket&quot;&gt;
                                &lt;thead&gt;
                                    &lt;tr&gt;
                                        &lt;th&gt;&lt;/th&gt;
                                        &lt;th&gt;Item&lt;/th&gt;
                                        &lt;th&gt;Qty&lt;/th&gt;
                                        &lt;th&gt;Unit Price&lt;/th&gt;
                                        &lt;th&gt;Total&lt;/th&gt;
                                    &lt;/tr&gt;
                                &lt;/thead&gt;
                                &lt;##FOR(ItemClass=YourClassName;AlternatingItemClass=YourClassName;)##&gt;
                                &lt;tr class=&quot;##RowClass##&quot;&gt;
                                    &lt;td&gt;
                                        &lt;img src=&quot;##ItemImage##&quot; alt=&quot;&quot; style=&quot;width:80px;&quot; class=&quot;img-responsive&quot; /&gt;
                                    &lt;/td&gt;
                                    &lt;td&gt;
                                        &lt;div class=&quot;pull-left&quot;&gt;
                                            &lt;a href=&quot;##ItemLink##&quot;&gt;##ItemName##&lt;/a&gt;
                                        &lt;/div&gt;
                                        &lt;div class=&quot;pull-right&quot;&gt;
                                            ##ItemAttributes##
                                        &lt;/div&gt;
                                    &lt;/td&gt;
                                    &lt;td&gt;##ItemUnits##
                                    &lt;/td&gt;
                                    &lt;td&gt;##CurrencySymbol## ##ItemPricePerUnit##
                                    &lt;/td&gt;
                                    &lt;td&gt;##CurrencySymbol## ##TotalAmount##
                                    &lt;/td&gt;
                                &lt;/tr&gt;
                                &lt;##ENDFOR##&gt;
                            &lt;/table&gt;
                        &lt;/div&gt;
                    &lt;/div&gt;

                &lt;/div&gt;
                &lt;div class=&quot;row&quot;&gt;
                    &lt;div class=&quot;col-xs-12 pull-right&quot;&gt;
                        &lt;b&gt;SUBTOTAL: ##CurrencySymbol## ##SubTotal##&lt;/b&gt;
                    &lt;/div&gt;
                &lt;/div&gt;
            &lt;/div&gt;
            &lt;div class=&quot;modal-footer modal-footer-minicart&quot;&gt;
                &lt;a id=&quot;hplCart&quot; class=&quot;btn btn-primary pull-left&quot; href=&quot;/Cart.aspx&quot;&gt;View Cart&lt;/a&gt;
                &lt;a id=&quot;hplCheckout&quot; class=&quot;btn btn-primary pull-right&quot; href=&quot;##CheckOutURL##&quot;&gt;Check Out&lt;/a&gt;
            &lt;/div&gt;
        &lt;/div&gt;
    &lt;/div&gt;
&lt;/div&gt;"   /> <img alt="" src="/images/a10.jpg" />
            </td>
        </tr>
        <tr>
            <td colspan="2">
             <%@ Register src="~/UserControls/SiteMenu.ascx" tagname="Menu" tagprefix="uc1" %> <uc1:Menu ID="Menu6" runat="server"  MenuDirection="Horizontal" Skin="Default" PopulateULLIbasedMenu="True" HtmlCustomize="&lt;!--Remove DefaultMenu , if you are planning to use custom menu--&gt;
{##CustomMenu##}
&lt;div id=&quot;vertSiteMenu&quot;&gt;
    &lt;ul class=&quot;TreeNode&quot;&gt;
        &lt;##FOR##&gt;
        &lt;li&gt;&lt;a href=&quot;##MenuItemLink##&quot;&gt;##MenuItemName##&lt;/a&gt;&lt;/li&gt;
        &lt;##ENDFOR##&gt;
    &lt;/ul&gt;
&lt;/div&gt;
{/##CustomMenu##}"   /> 
            </td>
        </tr>
        <tr>
            <td colspan="2">
             <%@ Register src="~/UserControls/ShippingPromotion.ascx" tagname="ShippingPromotion" tagprefix="uc1" %> <uc1:ShippingPromotion ID="ShippingPromotion10" runat="server"  HtmlCustomize=""   /> &nbsp; <%@ Register src="~/UserControls/OrderPromotion.ascx" tagname="OrderPromotion" tagprefix="uc1" %> <uc1:OrderPromotion ID="OrderPromotion8" runat="server"  HtmlCustomize=""   /> </td>
        </tr>
        <tr>
            <td colspan="2">
             <%@ Register src="~/UserControls/BreadCrumb.ascx" tagname="BreadCrumb" tagprefix="uc1" %> <uc1:BreadCrumb ID="BreadCrumb1" runat="server"    /> 
            </td>
        </tr>
    </tbody>
</table>
                <!--header ends-->
            </td>
        </tr>
        <tr>
            <td valign="top">
                <table width="100%">
                    <tr>
                        <td width="260px" valign="top">
                            <!--left panel starts-->
                             <%@ Register src="~/UserControls/Categories.ascx" tagname="Categories" tagprefix="uc1" %> <uc1:Categories ID="Categories3" runat="server"  ShowOnlyParentCategoriesinTree="False" PreserveStateofTree="False" HtmlCustomize="&lt;div class=&quot;boxheader&quot; id=&quot;divHeader&quot;&gt;
    Browse Categories
&lt;/div&gt;
&lt;div class=&quot;boxcontent&quot; id=&quot;divContent&quot;&gt;
    ##Menu##
    &lt;br /&gt;
    &lt;div class=&quot;left-category&quot;&gt;
        &lt;div class=&quot;catgry-hed&quot;&gt;Categories&lt;/div&gt;
        &lt;div class=&quot;category-list css-treeview&quot;&gt;
            &lt;div class=&quot;css-treeview&quot;&gt;
                &lt;##TreeViewCustom##&gt;
                &lt;ul class=&quot;TreeNodeParent&quot;&gt;
                    &lt;##FOREACH##&gt;
        		&lt;li&gt;
                    &lt;input type=&quot;checkbox&quot; id=&quot;##ITEMID##&quot;&gt;&lt;/input&gt;&lt;label for=&quot;##ITEMID##&quot;&gt;&lt;a href=&quot;/SubCategory/##CategoryName##/##CategoryID##&quot;&gt;##CategoryName##&lt;/a&gt;&lt;/label&gt;
                    &lt;##IFCHILDAVAILABLE##&gt;
        				&lt;ul class=&quot;TreeNodeChild&quot;&gt;
                            &lt;##FOREACHCHILD##&gt;
        				&lt;li&gt;
                            &lt;input type=&quot;checkbox&quot; id=&quot;Checkbox1&quot; /&gt;&lt;label for=&quot;##ITEMID##&quot;&gt;&lt;a href=&quot;/SubCategory/##CategoryName##/##CategoryID##&quot;&gt;##CategoryName##&lt;/a&gt;&lt;/label&gt;
                            &lt;##INSERTCHILDHERE##&gt;        			
                        &lt;/li&gt;
                            &lt;/##FOREACHCHILD##&gt;
                        &lt;/ul&gt;
                    &lt;/##IFCHILDAVAILABLE##&gt;
                &lt;/li&gt;
                    &lt;/##FOREACH##&gt;
                &lt;/ul&gt;
                &lt;/##TreeViewCustom##&gt;
            &lt;/div&gt;
        &lt;/div&gt;
    &lt;/div&gt;
&lt;/div&gt;" PopulateULLIBasedTree="False"   /> <br />
 <%@ Register src="~/UserControls/Search.ascx" tagname="Search" tagprefix="uc1" %> <uc1:Search ID="Search9" runat="server"  HtmlCustomize="&lt;div class=&quot;boxheader&quot;&gt;
    Search&lt;/div&gt;
&lt;div class=&quot;boxcontent&quot;&gt;
    &#160; &lt;span&gt;##SearchTextBox## &#160; ##SearchButton## &lt;/span&gt;
&lt;/div&gt;"   /> 
                            <!--left panel ends-->
                        </td>
                        <td valign="top" style="padding-left: 15px;">
                            <!--Content panel starts-->
							 <%@ Register src="~/UserControls/Error.ascx" tagname="Error" tagprefix="uc1" %> <uc1:Error ID="Error4" runat="server"    /> <br>
                             <%@ Register src="~/UserControls/Cart.ascx" tagname="Cart" tagprefix="uc1" %> <uc1:Cart ID="Cart2" runat="server"  HtmlCustomize="&lt;script src=&quot;https://maps.googleapis.com/maps/api/js?v=3.exp&amp;signed_in=true&amp;libraries=places&quot; type=&quot;text/javascript&quot;&gt;&lt;/script&gt;
&lt;script src=&quot;../Js/GooglePlace.js&quot; type=&quot;text/javascript&quot;&gt;&lt;/script&gt;
&lt;script type=&quot;text/javascript&quot;&gt;
$(document).ready(function(){

initializeaddress(&quot;txtPostal&quot;, &quot;Cart2_hdnStateLong&quot;, &quot;Cart2_hdnStateShort&quot;, &quot;txtPostal&quot;, &quot;ddlCountry&quot;);

});
&lt;/script&gt;
&lt;div id=&quot;UpdateProgress&quot; style=&quot;display:none;&quot; role=&quot;status&quot; aria-hidden=&quot;true&quot;&gt;
	
            &lt;div class=&quot;overlay&quot;&gt;
                &lt;div class=&quot;overlayContent&quot; style=&quot;color: #000; text-align: center; width: 280px; padding: 20px&quot;&gt;
                    &lt;i class=&quot;fa fa-2x fa-refresh fa-spin&quot;&gt;&lt;/i&gt;
                    &lt;h3&gt;Getting Shipping Quotes&lt;/h3&gt;
                &lt;/div&gt;
            &lt;/div&gt;
        
&lt;/div&gt;
&lt;table cellspacing=&quot;1&quot; cellpadding=&quot;2&quot; width=&quot;100%&quot;&gt;
    &lt;tr&gt;
        &lt;td&gt;
            &lt;div class=&quot;sectionheader&quot;&gt;
                Items to be shipped
            &lt;/div&gt;
        &lt;/td&gt;
    &lt;/tr&gt;
    &lt;tr&gt;
        &lt;td&gt;
            &lt;table class=&quot;Grid&quot; id=&quot;tblCart&quot; border=&quot;1&quot; cellspacing=&quot;0&quot; cellpadding=&quot;0&quot; width=&quot;100%&quot;&gt;
                &lt;tbody&gt;
                    &lt;tr class=&quot;GridHeader&quot;&gt;
                        &lt;td&gt;
                            Item
                        &lt;/td&gt;
                        &lt;td&gt;
                            Inclusion Details
                        &lt;/td&gt;
                        &lt;td&gt;
                            Attributes
                        &lt;/td&gt;
                        &lt;td&gt;
                            Units
                        &lt;/td&gt;
                        &lt;td&gt;
                            Price/Unit
                        &lt;/td&gt;
                        &lt;td&gt;
                            Discount
                        &lt;/td&gt;
                        &lt;td&gt;
                            Total
                        &lt;/td&gt;
                         &lt;td style=&quot;display:none&quot;&gt;
                            Item Name
                        &lt;/td&gt;
                        &lt;td style=&quot;display:none&quot;&gt;
                            Model ID
                        &lt;/td&gt;
                        &lt;td style=&quot;display:none&quot;&gt;
                            Item ID
                        &lt;/td&gt;
                        &lt;td style=&quot;display:none&quot;&gt;
                            Price
                        &lt;/td&gt;
                        &lt;td&gt;
                        &lt;/td&gt;
                    &lt;/tr&gt;
                    &lt;##FOR(ItemClass=GridItem;AlternatingItemClass=GridAltItem;)##&gt;
                    &lt;tr class=&quot;##RowClass##&quot;&gt;
                        &lt;td&gt;
                            &lt;a href=&quot;##ItemURL##&quot;&gt;##ItemName##&lt;/a&gt;
                        &lt;/td&gt;
                        &lt;td&gt;
                            ##ItemAttributes##
                        &lt;/td&gt;
                        &lt;td&gt;
                            ##InclusionDetails##
                        &lt;/td&gt;
                        &lt;td&gt;
                            ##ItemUnitsTextBox##
                        &lt;/td&gt;
                        &lt;td&gt;
                            ##ItemPricePerUnit##
                        &lt;/td&gt;
                        &lt;td&gt;
                            ##Discount##
                        &lt;/td&gt;
                        &lt;td&gt;
                            ##TotalAmount##
                        &lt;/td&gt;
                        &lt;td style=&quot;display:none&quot;&gt;
                            &lt;a href=&quot;##ItemURL##&quot;&gt;##ItemName##&lt;/a&gt;
                        &lt;/td&gt;
                        &lt;td style=&quot;display:none&quot;&gt;
                            ##ItemModelID##
                        &lt;/td&gt;
                        &lt;td style=&quot;display:none&quot;&gt;
                            ##ItemID##
                        &lt;/td&gt;
                        &lt;td style=&quot;display:none&quot;&gt;
                            ##ItemPricePerUnit##
                        &lt;/td&gt;    
                        &lt;td&gt;
                            ##DeleteItemButton##
                        &lt;/td&gt;
                    &lt;/tr&gt;
                    &lt;##ENDFOR##&gt;
                &lt;/tbody&gt;
            &lt;/table&gt;
        &lt;/td&gt;
    &lt;/tr&gt;
    &lt;tr&gt;
        &lt;td&gt;
            &lt;table width=&quot;100%&quot;&gt;
                &lt;tr&gt;
                    &lt;td valign=&quot;top&quot;&gt;
                        &lt;table align=&quot;left&quot; width=&quot;100%&quot;&gt;
                            &lt;tr&gt;
                                &lt;td&gt;
                                    &lt;br /&gt;
                                    &lt;input type=&quot;button&quot; name=&quot;btnContinue&quot; value=&quot;Continue Shopping&quot; class=&quot;button&quot; onclick=&quot;javascript: __doPostBack(&#39;btnContinue&#39;, &#39;&#39;);&quot; /&gt;
                                    &lt;input type=&quot;button&quot; name=&quot;btnUpdateCart&quot; value=&quot;Update Cart&quot; class=&quot;button&quot; onclick=&quot;javascript: __doPostBack(&#39;btnUpdateCart&#39;, &#39;&#39;);&quot; /&gt;
                                    &lt;input type=&quot;button&quot; name=&quot;btnCheckOut&quot; value=&quot;Checkout&quot; class=&quot;button&quot; onclick=&quot;javascript: __doPostBack(&#39;btnCheckOut&#39;, &#39;&#39;);&quot; /&gt;
                                &lt;/td&gt;
                            &lt;/tr&gt;
                            {##EnterCouponSection##}
                            &lt;tr id=&quot;trApplyCouponCode&quot;&gt;
                                &lt;td class=&quot;text_bold&quot;&gt;
                                    &lt;br /&gt;
                                    Apply Coupon/Discount Code: ##CouponCodeTextBox## ##ApplyCouponCodeButton##
                                &lt;/td&gt;
                            &lt;/tr&gt;
                            {/##EnterCouponSection##} {##UsedCouponSection##}
                            &lt;tr id=&quot;trUsedCouponCode&quot;&gt;
                                &lt;td class=&quot;text_bold&quot;&gt;
                                    &lt;br /&gt;
                                    Coupon/Discount Code: ##CouponCodeLabel## ##RemoveCouponCodeLink##
                                &lt;/td&gt;
                            &lt;/tr&gt;
                            {/##UsedCouponSection##}
                        &lt;/table&gt;
                    &lt;/td&gt;
                    &lt;td&gt;
                        &lt;table align=&quot;right&quot;&gt;
                            &lt;tr&gt;
                                    &lt;td&gt;
                                        &lt;div style=&quot;height:180px&quot;&gt;
                                               {##ShippingAddress##}
                                    &lt;table align=&quot;right&quot;&gt;
                                        &lt;tr&gt;
                                            &lt;td colspan=&quot;2&quot; align=&quot;center&quot;&gt;
                                                &lt;b&gt;Calculate Shipping Rates :&lt;/b&gt;
                                            &lt;/td&gt;
                                        &lt;/tr&gt;
                                        &lt;tr&gt;
                                            &lt;td align=&quot;right&quot;&gt;
                                                &lt;b&gt;Country &lt;font color=&quot;red&quot;&gt;*&lt;/font&gt; : &lt;/b&gt;
                                            &lt;/td&gt;
                                            &lt;td align=&quot;left&quot;&gt;
                                                  ##CountryDropDown##   
                                            &lt;/td&gt;
                                        &lt;/tr&gt;
                                        &lt;tr&gt;
                                            &lt;td align=&quot;right&quot;&gt;
                                                &lt;b&gt;State &lt;font color=&quot;red&quot;&gt;*&lt;/font&gt; : &lt;/b&gt;
                                            &lt;/td&gt;
                                            &lt;td align=&quot;left&quot;&gt;
                                               ##StateDropDown##   
                                            &lt;/td&gt;
                                        &lt;/tr&gt;
                                        &lt;tr&gt;
                                            &lt;td align=&quot;right&quot;&gt;
                                                &lt;b&gt;Zip / Postal Code &lt;font color=&quot;red&quot;&gt;*&lt;/font&gt; : &lt;/b&gt;
                                            &lt;/td&gt;
                                            &lt;td align=&quot;left&quot;&gt;
                                               ##ZipPostalCode##
                                            &lt;/td&gt;
                                        &lt;/tr&gt;
                                        &lt;tr&gt;
                                         &lt;td align=&quot;right&quot;&gt;
                                                
                                            &lt;/td&gt;
                                        &lt;td colspan = &quot;2&quot; style = &quot;padding:3px 0px 3px 0px&quot; align=&quot;left&quot; &gt;
                                        &lt;div id=&quot;UpdatePanelMain&quot;&gt;
                                           ##ReCalculate##
                                           &lt;/div&gt;
                                        &lt;/td&gt;
                                        &lt;/tr&gt;
                                    &lt;/table&gt;
                                    {/##ShippingAddress##} {##ShippingRates##}
                                    &lt;table align=&quot;right&quot;&gt;
                                        &lt;tr&gt;
                                            &lt;td&gt;
                                                &lt;b&gt;Shipping Rates&lt;/b&gt; &lt;a onclick=&quot;javascript:__doPostBack(&#39;shippingAddress&#39;,&#39;&#39;);&quot;&gt;Change My Address &lt;/a&gt;
                                            &lt;/td&gt;
                                        &lt;/tr&gt;
                                        &lt;tr&gt;
                                            &lt;td&gt;
                                                ##ShippingMethodName## ##ShowShippingAmountWithMethod##
                                            &lt;/td&gt;
                                        &lt;/tr&gt;
                                        &lt;tr&gt;
                                            &lt;td&gt;
                                                ##lblCountry## , ##lblState## ##lblZipPostalCode##
                                            &lt;/td&gt;
                                        &lt;/tr&gt;
                                    &lt;/table&gt;
                                    {/##ShippingRates##}   
                                   &lt;/div&gt;
                                &lt;/td&gt;
                            &lt;/tr&gt;
                            &lt;tr&gt;
                                &lt;td&gt;
                                    &lt;table align=&quot;right&quot;&gt;
                                        &lt;tr&gt;
                                            &lt;td class=&quot;text_bold&quot; align=&quot;right&quot;&gt;
                                                Subtotal:
                                            &lt;/td&gt;
                                            &lt;td align=&quot;right&quot;&gt;
                                                ##SubTotalLabel##
                                            &lt;/td&gt;
                                        &lt;/tr&gt;
                                        &lt;tr&gt;
                                            &lt;td class=&quot;text_bold&quot; align=&quot;right&quot;&gt;
                                                Discount:
                                            &lt;/td&gt;
                                            &lt;td align=&quot;right&quot;&gt;
                                                ##DiscountLabel##
                                            &lt;/td&gt;
                                        &lt;/tr&gt;
                                        &lt;tr&gt;
                                            &lt;td class=&quot;text_bold&quot; align=&quot;right&quot;&gt;
                                                Tax:
                                            &lt;/td&gt;
                                            &lt;td align=&quot;right&quot;&gt;
                                                ##TaxLabel##
                                            &lt;/td&gt;
                                        &lt;/tr&gt;
                                        &lt;tr&gt;
                                            &lt;td class=&quot;text_bold&quot; align=&quot;right&quot;&gt;
                                                Shipping Charges:
                                            &lt;/td&gt;
                                            &lt;td align=&quot;right&quot;&gt;
                                                ##ShippingChargeLabel##
                                            &lt;/td&gt;
                                        &lt;/tr&gt;
                                        &lt;tr&gt;
                                            &lt;td class=&quot;text_bold&quot; align=&quot;right&quot;&gt;
                                                Total:
                                            &lt;/td&gt;
                                            &lt;td align=&quot;right&quot;&gt;
                                                ##TotalAmountLabel##
                                            &lt;/td&gt;
                                        &lt;/tr&gt;
                                    &lt;/table&gt;
                                &lt;/td&gt;
                            &lt;/tr&gt;
                        &lt;/table&gt;
                    &lt;/td&gt;
                &lt;/tr&gt;
            &lt;/table&gt;
        &lt;/td&gt;
    &lt;/tr&gt;
&lt;/table&gt;" ApplyPriceBookPrice="True"   /> 
                            <!--Content panel ends-->
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <!--footer starts-->
                <div id="footer">
<a href="#">Temrs &amp; Conditions</a>&nbsp;&nbsp;&nbsp;<a href="#">Newsletters</a>&nbsp;&nbsp;&nbsp;<a href="#">Sitemap</a><br />
&copy; test1234. All rights reserved. Powered by <a href="http://www.BizAutomation.com">
BizAutomation</a>.
</div>
<script src="/Js/jquery-loader.js" type="text/javascript"></script>
                <!--footer ends-->
            </td>
        </tr>
    </table>
</center>
</url:form> 
</body> 
</html>