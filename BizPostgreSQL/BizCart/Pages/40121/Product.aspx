﻿<%@ Page Language="C#" Inherits="BizCart.BizPage"  MaintainScrollPositionOnPostback="true"  %>
<%@ Register TagPrefix="url" Namespace="Intelligencia.UrlRewriter" Assembly="Intelligencia.UrlRewriter" %>
<!DOCTYPE html>
<script runat="server"> 
</script> 
<html xmlns="http://www.w3.org/1999/xhtml"> 
<head runat="server"><title>View Detail of selcted Product</title><link rel="stylesheet" type="text/css" href="/PagingDarkStyle.css" /><link rel="stylesheet" type="text/css" href="/default.css" /><link rel="stylesheet" type="text/css" href="/smart_wizard.css" /><link rel="stylesheet" type="text/css" href="/SliderStyle.css" /><script type="text/javascript" src="/Js/Common.js" ></script><script type="text/javascript" src="/Js/jquery-1.9.1.min.js" ></script><script type="text/javascript" src="/Js/jquery.js" ></script><script type="text/javascript" src="/Js/jquery.jqzoom-core.js" ></script><script type="text/javascript" src="/Js/jquery.smartWizard-2.0.js" ></script></head> 
<body> 
    <url:form runat="server" id="form1">
<title>test1234:</title>
<center>
<table cellpadding="0" cellspacing="3" class="MainPanel" border="0">
    <tbody>
        <tr>
            <td valign="top">
            <!--header starts-->
            <table width="100%" border="0" cellpadding="0" cellspacing="3">
    <tbody>
        <tr>
            <td id="header">
            <h1>
            test1234<sup>beta</sup></h1>
            <h2>
            by bizautomation</h2>
            </td>
            <td align="right" style="width: 458px;"> <%@ Register src="~/UserControls/LogInStatus.ascx" tagname="LoginStatus" tagprefix="uc1" %> <uc1:LoginStatus ID="LoginStatus5" runat="server"  LogOutLandingPageURL="" HtmlCustomize="{##LogoutSection##}
Welcome: ##CustomerName## | &lt;a class=&quot;LoginLink&quot; href=&quot;/Account.aspx&quot;&gt;My Account&lt;/a&gt;| &lt;a id=&quot;LogoutLink&quot; class=&quot;LogOutLink&quot; href=&quot;javascript:__doPostBack(&#39;LogoutPostback&#39;,&#39;&#39;)&quot;&gt;Log out&lt;/a&gt; 
{/##LogoutSection##} 
{##LoginSection##}
&lt;a class=&quot;LoginLink&quot; href=&quot;/Login.aspx&quot;&gt;Login&lt;/a&gt; | &lt;a class=&quot;SingupLink&quot; href=&quot;/SignUp.aspx&quot;&gt;Create Account&lt;/a&gt;
{/##LoginSection##}
&lt;br /&gt;
&lt;b&gt;&lt;a href=&quot;/Cart.aspx&quot; class=&quot;dropdown-toggle&quot; aria-controls=&quot;divMiniCart&quot; data-toggle=&quot;dropdown&quot;&gt;##ItemCount## Item(s)&lt;/a&gt; (##TotalAmount##)&lt;/b&gt;"   />  <%@ Register src="~/UserControls/MiniCart.ascx" tagname="MiniCart" tagprefix="uc1" %> <uc1:MiniCart ID="MiniCart7" runat="server"  HtmlCustomize="&lt;link rel=&quot;stylesheet&quot; href=&quot;https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css&quot;&gt;
  &lt;script src=&quot;https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js&quot;&gt;&lt;/script&gt;
  &lt;script src=&quot;https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js&quot;&gt;&lt;/script&gt;
&lt;style type=&quot;text/css&quot;&gt;
    #tblBasket tr th {
        text-align:center !important;
    }

    #tblBasket tr th:first-child {
        width:85px;
    }

    #tblBasket tr td:nth-child(3) {
        width:90px;
        text-align:center;
    }

     #tblBasket tr td:nth-child(4) {
        width:130px;
        text-align:center;
    }

     #tblBasket tr td:nth-child(5) {
        width:130px;
        text-align:center;
    }

    #tblBasket tr td {
        vertical-align:middle;
    }
&lt;/style&gt;
&lt;script type=&quot;text/javascript&quot;&gt;
    function ShowMiniCart() {
        $(&quot;#divMiniCart&quot;).show();
    }
&lt;/script&gt;

&lt;div id=&quot;divMiniCart&quot; class=&quot;dropdown-menu&quot;&gt;
    &lt;div class=&quot;modal-dialog modal-dialog-minicart&quot; style=&quot;width:70%&quot;&gt;
        &lt;!-- Modal content--&gt;
        &lt;div class=&quot;modal-content modal-content-minicart&quot;&gt;
            &lt;div class=&quot;modal-header modal-content-minicart&quot;&gt;
                &lt;div class=&quot;pull-left&quot;&gt;
                    &lt;h4 class=&quot;modal-title modal-title-minicart&quot;&gt;
                        &lt;img id=&quot;imgShopping&quot; src=&quot;/images/shopping_cart.gif&quot; align=&quot;absmiddle&quot; style=&quot;border-width: 0px;&quot;&gt;
                        Your Cart
                    &lt;/h4&gt;
                &lt;/div&gt;
                &lt;button type=&quot;button&quot; class=&quot;close&quot; data-dismiss=&quot;modal&quot;&gt;&#215;&lt;/button&gt;
            &lt;/div&gt;
            &lt;div class=&quot;modal-body modal-body-minicart&quot;&gt;
                &lt;div class=&quot;row&quot;&gt;
                    &lt;div class=&quot;col-xs-12&quot;&gt;
                        &lt;div class=&quot;table-responsive&quot;&gt;
                            &lt;table class=&quot;table table-bordered table-striped&quot; id=&quot;tblBasket&quot;&gt;
                                &lt;thead&gt;
                                    &lt;tr&gt;
                                        &lt;th&gt;&lt;/th&gt;
                                        &lt;th&gt;Item&lt;/th&gt;
                                        &lt;th&gt;Qty&lt;/th&gt;
                                        &lt;th&gt;Unit Price&lt;/th&gt;
                                        &lt;th&gt;Total&lt;/th&gt;
                                    &lt;/tr&gt;
                                &lt;/thead&gt;
                                &lt;##FOR(ItemClass=YourClassName;AlternatingItemClass=YourClassName;)##&gt;
                                &lt;tr class=&quot;##RowClass##&quot;&gt;
                                    &lt;td&gt;
                                        &lt;img src=&quot;##ItemImage##&quot; alt=&quot;&quot; style=&quot;width:80px;&quot; class=&quot;img-responsive&quot; /&gt;
                                    &lt;/td&gt;
                                    &lt;td&gt;
                                        &lt;div class=&quot;pull-left&quot;&gt;
                                            &lt;a href=&quot;##ItemLink##&quot;&gt;##ItemName##&lt;/a&gt;
                                        &lt;/div&gt;
                                        &lt;div class=&quot;pull-right&quot;&gt;
                                            ##ItemAttributes##
                                        &lt;/div&gt;
                                    &lt;/td&gt;
                                    &lt;td&gt;##ItemUnits##
                                    &lt;/td&gt;
                                    &lt;td&gt;##CurrencySymbol## ##ItemPricePerUnit##
                                    &lt;/td&gt;
                                    &lt;td&gt;##CurrencySymbol## ##TotalAmount##
                                    &lt;/td&gt;
                                &lt;/tr&gt;
                                &lt;##ENDFOR##&gt;
                            &lt;/table&gt;
                        &lt;/div&gt;
                    &lt;/div&gt;

                &lt;/div&gt;
                &lt;div class=&quot;row&quot;&gt;
                    &lt;div class=&quot;col-xs-12 pull-right&quot;&gt;
                        &lt;b&gt;SUBTOTAL: ##CurrencySymbol## ##SubTotal##&lt;/b&gt;
                    &lt;/div&gt;
                &lt;/div&gt;
            &lt;/div&gt;
            &lt;div class=&quot;modal-footer modal-footer-minicart&quot;&gt;
                &lt;a id=&quot;hplCart&quot; class=&quot;btn btn-primary pull-left&quot; href=&quot;/Cart.aspx&quot;&gt;View Cart&lt;/a&gt;
                &lt;a id=&quot;hplCheckout&quot; class=&quot;btn btn-primary pull-right&quot; href=&quot;##CheckOutURL##&quot;&gt;Check Out&lt;/a&gt;
            &lt;/div&gt;
        &lt;/div&gt;
    &lt;/div&gt;
&lt;/div&gt;"   /> <img alt="" src="/images/a10.jpg" />
            </td>
        </tr>
        <tr>
            <td colspan="2">
             <%@ Register src="~/UserControls/SiteMenu.ascx" tagname="Menu" tagprefix="uc1" %> <uc1:Menu ID="Menu6" runat="server"  MenuDirection="Horizontal" Skin="Default" PopulateULLIbasedMenu="True" HtmlCustomize="&lt;!--Remove DefaultMenu , if you are planning to use custom menu--&gt;
{##CustomMenu##}
&lt;div id=&quot;vertSiteMenu&quot;&gt;
    &lt;ul class=&quot;TreeNode&quot;&gt;
        &lt;##FOR##&gt;
        &lt;li&gt;&lt;a href=&quot;##MenuItemLink##&quot;&gt;##MenuItemName##&lt;/a&gt;&lt;/li&gt;
        &lt;##ENDFOR##&gt;
    &lt;/ul&gt;
&lt;/div&gt;
{/##CustomMenu##}"   /> 
            </td>
        </tr>
        <tr>
            <td colspan="2">
             <%@ Register src="~/UserControls/ShippingPromotion.ascx" tagname="ShippingPromotion" tagprefix="uc1" %> <uc1:ShippingPromotion ID="ShippingPromotion11" runat="server"  HtmlCustomize=""   /> &nbsp; <%@ Register src="~/UserControls/OrderPromotion.ascx" tagname="OrderPromotion" tagprefix="uc1" %> <uc1:OrderPromotion ID="OrderPromotion8" runat="server"  HtmlCustomize=""   /> </td>
        </tr>
        <tr>
            <td colspan="2">
             <%@ Register src="~/UserControls/BreadCrumb.ascx" tagname="BreadCrumb" tagprefix="uc1" %> <uc1:BreadCrumb ID="BreadCrumb1" runat="server"    /> 
            </td>
        </tr>
    </tbody>
</table>
            <!--header ends-->
            </td>
        </tr>
        <tr>
            <td valign="top">
            <table width="100%">
                <tbody>
                    <tr>
                        <td valign="top" style="width: 260px;">
                        <!--left panel starts-->
                         <%@ Register src="~/UserControls/Categories.ascx" tagname="Categories" tagprefix="uc1" %> <uc1:Categories ID="Categories2" runat="server"  ShowOnlyParentCategoriesinTree="False" PreserveStateofTree="False" HtmlCustomize="&lt;div class=&quot;boxheader&quot; id=&quot;divHeader&quot;&gt;
    Browse Categories
&lt;/div&gt;
&lt;div class=&quot;boxcontent&quot; id=&quot;divContent&quot;&gt;
    ##Menu##
    &lt;br /&gt;
    &lt;div class=&quot;left-category&quot;&gt;
        &lt;div class=&quot;catgry-hed&quot;&gt;Categories&lt;/div&gt;
        &lt;div class=&quot;category-list css-treeview&quot;&gt;
            &lt;div class=&quot;css-treeview&quot;&gt;
                &lt;##TreeViewCustom##&gt;
                &lt;ul class=&quot;TreeNodeParent&quot;&gt;
                    &lt;##FOREACH##&gt;
        		&lt;li&gt;
                    &lt;input type=&quot;checkbox&quot; id=&quot;##ITEMID##&quot;&gt;&lt;/input&gt;&lt;label for=&quot;##ITEMID##&quot;&gt;&lt;a href=&quot;/SubCategory/##CategoryName##/##CategoryID##&quot;&gt;##CategoryName##&lt;/a&gt;&lt;/label&gt;
                    &lt;##IFCHILDAVAILABLE##&gt;
        				&lt;ul class=&quot;TreeNodeChild&quot;&gt;
                            &lt;##FOREACHCHILD##&gt;
        				&lt;li&gt;
                            &lt;input type=&quot;checkbox&quot; id=&quot;Checkbox1&quot; /&gt;&lt;label for=&quot;##ITEMID##&quot;&gt;&lt;a href=&quot;/SubCategory/##CategoryName##/##CategoryID##&quot;&gt;##CategoryName##&lt;/a&gt;&lt;/label&gt;
                            &lt;##INSERTCHILDHERE##&gt;        			
                        &lt;/li&gt;
                            &lt;/##FOREACHCHILD##&gt;
                        &lt;/ul&gt;
                    &lt;/##IFCHILDAVAILABLE##&gt;
                &lt;/li&gt;
                    &lt;/##FOREACH##&gt;
                &lt;/ul&gt;
                &lt;/##TreeViewCustom##&gt;
            &lt;/div&gt;
        &lt;/div&gt;
    &lt;/div&gt;
&lt;/div&gt;" PopulateULLIBasedTree="False"   /> <br />
 <%@ Register src="~/UserControls/Search.ascx" tagname="Search" tagprefix="uc1" %> <uc1:Search ID="Search10" runat="server"  HtmlCustomize="&lt;div class=&quot;boxheader&quot;&gt;
    Search&lt;/div&gt;
&lt;div class=&quot;boxcontent&quot;&gt;
    &#160; &lt;span&gt;##SearchTextBox## &#160; ##SearchButton## &lt;/span&gt;
&lt;/div&gt;"   /> 
                        <!--left panel ends-->
                        </td>
                        <td valign="top" style="padding-left: 15px;">
                        <!--Content panel starts-->
                         <%@ Register src="~/UserControls/Error.ascx" tagname="Error" tagprefix="uc1" %> <uc1:Error ID="Error3" runat="server"    /> <br />
                         <%@ Register src="~/UserControls/Product.ascx" tagname="Product" tagprefix="uc1" %> <uc1:Product ID="Product9" runat="server"  AttributeControlType="DropDown List" ApplyPriceBookPrice="True" HtmlCustomize="&lt;table width=&quot;100%&quot; cellspacing=&quot;0&quot; cellpadding=&quot;0&quot; border=&quot;0&quot;&gt;
    &lt;tbody&gt;
        &lt;tr&gt;
            &lt;td colspan=&quot;2&quot;&gt;
                &lt;h1 class=&quot;sectionheader&quot;&gt;
                    ##Item##
                    &lt;div style=&quot;float: right; font-size: 12px;&quot;&gt;
                        &lt;div class=&quot;AvgRating&quot; style=&quot;float: left;&quot;&gt;
                            ##AverageRating##
                        &lt;/div&gt;
                        &lt;div style=&quot;float: right;&quot;&gt;
                            &lt;div class=&quot;show_rating_count&quot; style=&quot;float: left;&quot;&gt;
                                ##RatingCount## &#160;
                            &lt;/div&gt;
                            &lt;div style=&quot;float: right;&quot;&gt;
                                ##RatingLabel## ##Separator## ##ReviewCount##
                            &lt;/div&gt;
                        &lt;/div&gt;
                    &lt;/div&gt;
                &lt;/h1&gt;
            &lt;/td&gt;
        &lt;/tr&gt;
        &lt;tr&gt;
            &lt;td&gt;
            &lt;/td&gt;
        &lt;/tr&gt;
        &lt;tr&gt;
            &lt;td&gt;
                &lt;table&gt;
                    &lt;tr&gt;
                        &lt;td&gt;
                            {##ProductImage##}
                                &lt;div class=&quot;clearfix&quot; id=&quot;content&quot;&gt;
                                &lt;div class=&quot;clearfix&quot;&gt;
                                    &lt;a href=&quot;##LargeImagePath##&quot; class=&quot;jqzoom&quot; style=&quot;display: block; margin: auto;&quot;
                                        rel=&#39;gal1&#39; title=&quot;##Item##&quot;&gt;
                                        &lt;img src=&quot;##ThumbImagePath##&quot; class=&quot;resizeme&quot; title=&quot;Zoom&quot;&gt;
                                    &lt;/a&gt;
                                &lt;/div&gt;
                                &lt;br /&gt;
                                &lt;div class=&quot;clearfixthumb&quot;&gt;
                                    &lt;ul id=&quot;thumblist&quot; class=&quot;clearfixthumb&quot;&gt;
                                        &lt;##FOR##&gt;
                                        &lt;li class=&quot;imagePadding&quot;&gt;&lt;a class=&quot;##ZoomActive##&quot; href=&#39;javascript:void(0);&#39; rel=&quot;{gallery: &#39;gal1&#39;, smallimage: &#39;##ThumbImagePath##&#39;,largeimage: &#39;##LargeImagePath##&#39;}&quot;&gt;
                                            &lt;img src=&#39;##ThumbImagePath##&#39; class=&quot;resizethumb&quot;&gt;
                                        &lt;/a&gt;&lt;/li&gt;
                                        &lt;##ENDFOR##&gt;
                                    &lt;/ul&gt;
                                &lt;/div&gt;
                            &lt;/div&gt;
                            {/##ProductImage##}
                            {##ProductImageNotAvailable##}
                                &lt;div&gt;
                                   &lt;img src =&quot;/Images/DefaultProduct.png&quot; alt = &quot;Image Not Fount&quot; /&gt;
                                &lt;/div&gt;
                            {/##ProductImageNotAvailable##}
                        &lt;/td&gt;
                    &lt;/tr&gt;
                    &lt;tr&gt;
                        &lt;td align=&quot;center&quot;&gt;
                            &lt;table width=&quot;100%&quot; cellspacing=&quot;0&quot; cellpadding=&quot;0&quot; border=&quot;0&quot;&gt;
                                &lt;tr&gt;
                                    &lt;td&gt;
                                        &lt;div style=&quot;float: right&quot;&gt;
                                            ##RateLabel##
                                        &lt;/div&gt;
                                    &lt;/td&gt;
                                    &lt;td&gt;
                                        &lt;div style=&quot;float: left&quot;&gt;
                                            ##WriteRating##
                                        &lt;/div&gt;
                                    &lt;/td&gt;
                                &lt;/tr&gt;
                            &lt;/table&gt;
                        &lt;/td&gt;
                    &lt;/tr&gt;
                    &lt;tr&gt;
                        &lt;td align=&quot;center&quot;&gt;
                            &lt;a href=&quot;/WriteReview.aspx?ItemId=##ItemId##&amp;CategoryName=##CategoryName##&amp;ProductName=##ProductName##&quot;&gt;
                                Write a review&lt;/a&gt;
                        &lt;/td&gt;
                    &lt;/tr&gt;
                    &lt;tr&gt;
                    &lt;td&gt;##PromotionDescription##&lt;/td&gt;
                    &lt;/tr&gt;
                &lt;/table&gt;
            &lt;/td&gt;
            &lt;td valign=&quot;top&quot;&gt;
                &lt;table&gt;
                    &lt;tbody&gt;
                        &lt;tr id=&quot;trPrice&quot; runat=&quot;server&quot;&gt;
                            &lt;td class=&quot;Product_Label&quot;&gt;
                                Price :
                            &lt;/td&gt;
                            &lt;td class=&quot;Product_Value&quot;&gt;
                                ##MSRP## ##OfferPrice## ##ItemPrice##
                            &lt;/td&gt;
                        &lt;/tr&gt;
                        &lt;tr id=&quot;tr1&quot; runat=&quot;server&quot;&gt;
                            &lt;td class=&quot;Product_Label&quot;&gt;
                                SKU :
                            &lt;/td&gt;
                            &lt;td class=&quot;Product_Value&quot;&gt;
                                ##SKU##
                            &lt;/td&gt;
                        &lt;/tr&gt;
                        &lt;tr id=&quot;tr3&quot; runat=&quot;server&quot;&gt;
                            &lt;td class=&quot;Product_Label&quot;&gt;
                                Manufacturer :
                            &lt;/td&gt;
                            &lt;td class=&quot;Product_Value&quot;&gt;
                                ##Manufacturer##
                            &lt;/td&gt;
                        &lt;/tr&gt;
                        &lt;tr id=&quot;tr3&quot; runat=&quot;server&quot;&gt;
                            &lt;td class=&quot;Product_Label&quot;&gt;
                                UPC :
                            &lt;/td&gt;
                            &lt;td class=&quot;Product_Value&quot;&gt;
                                ##UPCCode##
                            &lt;/td&gt;
                        &lt;/tr&gt;
                        
                        &lt;tr id=&quot;tr4&quot; runat=&quot;server&quot;&gt;
                            &lt;td class=&quot;Product_Label&quot;&gt;
                                Shipping Method :
                            &lt;/td&gt;
                            &lt;td class=&quot;Product_Value&quot;&gt;
                                ##Shipping##
                            &lt;/td&gt;
                        &lt;/tr&gt;
                        &lt;tr id=&quot;tr5&quot; runat=&quot;server&quot;&gt;
                            &lt;td class=&quot;Product_Label&quot;&gt;
                                Availability :
                            &lt;/td&gt;
                            &lt;td class=&quot;Product_Value&quot;&gt;
                                ##Availability##
                            &lt;/td&gt;
                        &lt;/tr&gt;
                        &lt;tr id=&quot;tr6&quot; runat=&quot;server&quot;&gt;
                            &lt;td class=&quot;Product_Label&quot;&gt;
                                Current Stock :
                            &lt;/td&gt;
                            &lt;td class=&quot;Product_Value&quot;&gt;
                                ##CurrentStock##
                            &lt;/td&gt;
                        &lt;/tr&gt;
                        &lt;tr&gt;
                            &lt;td class=&quot;Product_Label&quot;&gt;
                                Quantity:
                            &lt;/td&gt;
                            &lt;td&gt;
                                ##Quantity## ##ItemUOM##
                            &lt;/td&gt;
                        &lt;/tr&gt;
                        &lt;tr&gt;
                            &lt;td colspan=&quot;2&quot;&gt;
                                ##ChildKits##
                            &lt;/td&gt;
                        &lt;/tr&gt;
             
                        &lt;tr&gt;
                            &lt;td&gt;
                            &lt;/td&gt;
                            &lt;td&gt;
                                ##AddToCartLink##
                                
&lt;a href=&quot;#&quot; onclick=&quot;javascript:__doPostBack(&#39;RedirectToLogin&#39;,&#39;&#39;);&quot; id=&quot;loginDealerPrice&quot; style=&quot;display:none&quot; class=&quot;btn btn-primary&quot;&gt;Login for Dealer Price&lt;/a&gt;
                            &lt;/td&gt;
                        &lt;/tr&gt;
                        &lt;tr id=&quot;trList&quot; runat=&quot;server&quot; visible=&quot;false&quot;&gt;
                            &lt;td class=&quot;Product_Label&quot;&gt;
                            &lt;/td&gt;
                            &lt;td&gt;
                                &lt;a onclick=&quot;javascript:__doPostBack(&#39;AddToList&#39;,&#39;&#39;);&quot;&gt;Add to List &lt;/a&gt;
                            &lt;/td&gt;
                        &lt;/tr&gt;
                    &lt;/tbody&gt;
                &lt;/table&gt;
            &lt;/td&gt;
        &lt;/tr&gt;
        &lt;tr&gt;
            &lt;td colspan=&quot;2&quot;&gt;
                ##ShortDescription##
            &lt;/td&gt;
        &lt;/tr&gt;
        &lt;tr&gt;
            &lt;td colspan=&quot;2&quot;&gt;
                ##LongDescription##
                {##AvailabilityGrid##}
                &lt;table class=&quot;table table-responsive table-bordered&quot;&gt;
                    &lt;tr&gt;
                        &lt;th&gt;
                            Location
                        &lt;/th&gt;
                        &lt;th&gt;
                            Qty on hand
                        &lt;/th&gt;
                        &lt;th&gt;
                            Qty on order
                        &lt;/th&gt;
                        &lt;th&gt;
                            Order Date
                        &lt;/th&gt;
                        &lt;th&gt;
                            Lead Time (Days)
                        &lt;/th&gt;
                        &lt;th&gt;
                            Expected Delivery
                        &lt;/th&gt;
                    &lt;/tr&gt;
                    &lt;##FOR_AVAILABLEGRID##&gt;
                        &lt;tr&gt;
                            &lt;td&gt;
                                ##Avail_Location##
                            &lt;/td&gt;
                            &lt;td&gt;
                                ##Avail_QtyOnHand##
                            &lt;/td&gt;
                            &lt;td&gt;
                                ##Avail_QtyOnOrder##
                            &lt;/td&gt;
                            &lt;td&gt;
                                ##Avail_OrderDate##
                            &lt;/td&gt;
                            &lt;td&gt;
                                ##Avail_LeadTime##
                            &lt;/td&gt;
                            &lt;td&gt;
                                ##Avail_ExpectedDelivery##
                            &lt;/td&gt;
                        &lt;/tr&gt;
                   &lt;##ENDFOR##&gt;

                &lt;/table&gt;
                {/##AvailabilityGrid##}

            &lt;/td&gt;
        &lt;/tr&gt;
        &lt;tr&gt;
            &lt;td colspan=&quot;2&quot;&gt;
                   {##ProductDocument##}
                                &lt;div&gt;
                                    &lt;ul style=&quot;list-style:none;&quot;&gt;
                                        &lt;##FOR_Document##&gt;
                                        &lt;li style=&quot; background:url(&#39;##DocumentImage##&#39;) no-repeat;line-height: 32px;vertical-align: middle;padding-left:36px;&quot; &gt;
                                            &lt;a href =&quot;##DocumentPath##&quot;  target=&quot;_blank&quot; &gt;##DocumentName##&lt;/a&gt;
                                        &lt;/li&gt;
                                        &lt;##ENDFOR##&gt;
                                    &lt;/ul&gt;
                                &lt;/div&gt;
                   {/##ProductDocument##}
            &lt;/td&gt;
        &lt;/tr&gt;
        &lt;tr&gt;
            &lt;td colspan=&quot;2&quot;&gt;
            ##PriceLevelTable##
            &lt;/td&gt;
        &lt;/tr&gt;
    &lt;/tbody&gt;
&lt;/table&gt;
&lt;table&gt;&lt;tr&gt;&lt;td&gt;##Size_11796##&lt;/td&gt;&lt;td&gt;##Color_11798##&lt;/td&gt;&lt;td&gt;##FeaturedItem_C##&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;
&lt;br /&gt;
{##OptionsSection##}
&lt;table id=&quot;tblOptSelection&quot;&gt;
    &lt;tr&gt;
        &lt;td colspan=&quot;2&quot;&gt;
            &lt;b&gt;Options And Accessories&lt;/b&gt;
            &lt;br /&gt;
            &lt;br /&gt;
        &lt;/td&gt;
    &lt;/tr&gt;
    &lt;tr&gt;
        &lt;td class=&quot;LabelColumn&quot; rowspan=&quot;1&quot;&gt;
            &lt;img alt=&quot;&quot; borderwidth=&quot;1&quot; width=&quot;100&quot; bordercolor=&quot;black&quot; height=&quot;100&quot; src=&quot;##Opt_ItemPath##&quot;
                onclick=&quot;return OpenImage(&#39;##Opt_ItemCode##&#39;);&quot; style=&quot;cursor: pointer;&quot; /&gt;
        &lt;/td&gt;
        &lt;td&gt;
            &lt;table id=&quot;Table2&quot; runat=&quot;server&quot;&gt;
                &lt;tr&gt;
                    &lt;td class=&quot;LabelColumn&quot; valign=&quot;top&quot;&gt;
                        Item ##Opt_Item##
                    &lt;/td&gt;
                    &lt;td style=&quot;color: orange&quot;&gt;
                        ##Opt_Stock##
                    &lt;/td&gt;
                &lt;/tr&gt;
                &lt;tr&gt;
                    &lt;td colspan=&quot;2&quot;&gt;
                        Price ##Opt_ItemPrice##
                    &lt;/td&gt;
                &lt;/tr&gt;
                &lt;tr&gt;
                    &lt;td colspan=&quot;2&quot; style=&quot;white-space: nowrap&quot;&gt;
                        ##Opt_ItemAttr##
                    &lt;/td&gt;
                &lt;/tr&gt;
                &lt;tr&gt;
                    &lt;td colspan=&quot;2&quot;&gt;
                        Units ##Opt_Quantity## ##Opt_AddToCartLink##
                    &lt;/td&gt;
                &lt;/tr&gt;
                &lt;tr&gt;
                    &lt;td colspan=&quot;2&quot;&gt;
                        ##Opt_Description##
                    &lt;/td&gt;
                &lt;/tr&gt;
            &lt;/table&gt;
        &lt;/td&gt;
    &lt;/tr&gt;
&lt;/table&gt;
{/##OptionsSection##}" AfterAddingItemShowCartPage="False"   /> <br />
                         <%@ Register src="~/UserControls/ItemPromotionPopup.ascx" tagname="ItemPromotionPopup" tagprefix="uc1" %> <uc1:ItemPromotionPopup ID="ItemPromotionPopup4" runat="server"  HtmlCustomize="&lt;!-- IMPORTANT: Do not change id of element or onclick function --&gt;

&lt;div id=&quot;divItemPromoPopup&quot; class=&quot;modal fade modal-itempromopopup&quot; role=&quot;dialog&quot;&gt;
    &lt;div class=&quot;modal-dialog modal-dialog-itempromopopup&quot; style=&quot;width:70%&quot;&gt;
        &lt;div class=&quot;modal-content modal-content-itempromopopup&quot;&gt;
            &lt;div class=&quot;modal-header modal-content-itempromopopup&quot;&gt;
                &lt;div style=&quot;float: left;&quot;&gt;
                    &lt;img src=&quot;/images/item-promotion.png&quot; height=&quot;30&quot; alt=&quot;&quot; /&gt;
                    &lt;b&gt;Item Promotion(s)&lt;/b&gt;
                &lt;/div&gt;
                &lt;div class=&quot;pull-right&quot;&gt;
                    &lt;button onclick=&quot;return SaveItemPromoSelection();&quot; class=&quot;btn btn-primary&quot;&gt;Save &amp; Close&lt;/button&gt;
                &lt;/div&gt;
            &lt;/div&gt;
            &lt;div class=&quot;modal-body modal-body-itempromopopup&quot;&gt;
                &lt;div class=&quot;row&quot;&gt;
                    &lt;div class=&quot;col-xs-12&quot; id=&quot;divItemPromoList&quot;&gt;

                    &lt;/div&gt;
                &lt;/div&gt;
            &lt;/div&gt;
        &lt;/div&gt;
    &lt;/div&gt;
&lt;/div&gt;"   /> 
                        </td>
                    </tr>
                </tbody>
            </table>
            </td>
        </tr>
        <tr>
            <td>
            <!--footer starts-->
            <div id="footer">
<a href="#">Temrs &amp; Conditions</a>&nbsp;&nbsp;&nbsp;<a href="#">Newsletters</a>&nbsp;&nbsp;&nbsp;<a href="#">Sitemap</a><br />
&copy; test1234. All rights reserved. Powered by <a href="http://www.BizAutomation.com">
BizAutomation</a>.
</div>
<script src="/Js/jquery-loader.js" type="text/javascript"></script>
            <!--footer ends-->
            </td>
        </tr>
    </tbody>
</table>
</center></url:form> 
</body> 
</html>