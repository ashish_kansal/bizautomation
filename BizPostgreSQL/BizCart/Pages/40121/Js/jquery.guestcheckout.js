﻿$(document).ready(function () {

    initializebilladdress("txtBillStreet", "txtBillStreet", "txtBillCity", "txtBillCode", "ddlBillCountry");
    initializeshipaddress("txtShipStreet", "txtShipStreet", "txtShipCity", "txtShipCode", "ddlShipCountry");

    $('#ddlBillAddressName').bind("change", function () {
        FillAddress($(this).val());
    });

    $('#ddlShipAddressName').bind("change", function () {
        FillAddress($(this).val());
    });

    $('#ddlBillCountry').bind("change", function () {
        var billCountry = parseInt($(this).val());

        $.when(FillState($(this).val(), $('#ddlBillState'))).then(function (data, textStatus, jqXHR) {
            if ($("#hdnTaxBasedOn").val() == "1" && $('#txtBillCity').val() != '' && $('#txtBillCode').val() != '' && parseInt($(this).val()) > 0 && parseInt($('#ddlBillState').val()) > 0) {
                GetTax($('#txtBillCity').val(), $('#txtBillCode').val(), billCountry, parseInt($('#ddlBillState').val()));
            }
        });
    });

    $('#ddlShipCountry').bind("change", function () {
        var shipCountry = parseInt($(this).val());

        $.when(FillState($(this).val(), $('#ddlShipState'))).then(function (data, textStatus, jqXHR) {
            LoadOverlay('body', 'Please wait a few seconds while we reach out to our carriers to get you the best shipping rates possible');
            GetShippingMethod($('#txtShipCode').val(), shipCountry, parseInt($('#ddlShipState').val() || 0));

            if ($("#hdnTaxBasedOn").val() == "2" && $('#txtShipCity').val() != '' && $('#txtShipCode').val() != '' && parseInt($(this).val()) > 0 && parseInt($('#ddlShipState').val()) > 0) {
                GetTax($('#txtShipCity').val(), $('#txtShipCode').val(), shipCountry, parseInt($('#ddlShipState').val()));
            }
        });
    });

    $('#btnCharge').bind("click", function () {
        Save();
    });

    $('#ddlBillState').bind("change", function () {
        $("#hdnBillState").val($(this).val());

        if ($("#hdnTaxBasedOn").val() == "1" && $('#txtBillCity').val() != '' && $('#txtBillCode').val() != '' && parseInt($('#ddlBillCountry').val()) > 0 && parseInt($(this).val()) > 0) {
            GetTax($('#txtBillCity').val(), $('#txtBillCode').val(), parseInt($('#ddlBillCountry').val()), parseInt($(this).val()));
        }
    });

    $('#ddlShipState').bind("change", function () {
        $("#hdnShipState").val($(this).val());

        LoadOverlay('body', 'Please wait a few seconds while we reach out to our carriers to get you the best shipping rates possible');
        GetShippingMethod($('#txtShipCode').val(), parseInt($('#ddlShipCountry').val()), parseInt($(this).val()));

        if ($("#hdnTaxBasedOn").val() == "2" && $('#txtShipCity').val() != '' && $('#txtShipCode').val() != '' && parseInt($('#ddlShipCountry').val()) > 0 && parseInt($(this).val()) > 0) {
            GetTax($('#txtShipCity').val(), $('#txtShipCode').val(), parseInt($('#ddlShipCountry').val()), parseInt($(this).val()));
        }
    });

    $('#txtShipCity').bind("blur", function () {
        if ($("#hdnTaxBasedOn").val() == "2" && $(this).val() != '' && $('#txtShipCode').val() != '' && parseInt($('#ddlShipCountry').val()) > 0 && parseInt($('#ddlShipState').val()) > 0) {
            GetTax($(this).val(), $('#txtShipCode').val(), parseInt($('#ddlShipCountry').val()), parseInt($('#ddlShipState').val()));
        }
    });

    $('#txtShipCode').bind("blur", function () {
        LoadOverlay('body', 'Please wait a few seconds while we reach out to our carriers to get you the best shipping rates possible');
        GetShippingMethod($(this).val(), parseInt($('#ddlShipCountry').val()), parseInt($('#ddlShipState').val() || 0));

        if ($("#hdnTaxBasedOn").val() == "2" && $(this).val() != '' && $('#txtShipCode').val() != '' && parseInt($('#ddlShipCountry').val()) > 0 && parseInt($('#ddlShipState').val()) > 0) {
            GetTax($('#txtShipCity').val(), $(this).val(), parseInt($('#ddlShipCountry').val()), parseInt($('#ddlShipState').val()));
        }
    });

    $('#txtBillCity').bind("blur", function () {
        if ($("#hdnTaxBasedOn").val() == "1" && $(this).val() != '' && $('#txtBillCode').val() != '' && parseInt($('#ddlBillCountry').val()) > 0 && parseInt($('#ddlBillState').val()) > 0) {
            GetTax($(this).val(), $('#txtBillCode').val(), parseInt($('#ddlBillCountry').val()), parseInt($('#ddlBillState').val()));
        }
    });

    $('#txtBillCode').bind("blur", function () {
        if ($("#hdnTaxBasedOn").val() == "1" && $(this).val() != '' && $('#txtBillCity').val() != '' && parseInt($('#ddlBillCountry').val()) > 0 && parseInt($('#ddlBillState').val()) > 0) {
            GetTax($('#txtBillCity').val(), $(this).val(), parseInt($('#ddlBillCountry').val()), parseInt($('#ddlBillState').val()));
        }
    });

    $('#chkResidential').bind("change", function () {
        if ($('#txtShipCode').val() != '' && parseInt($('#ddlShipCountry').val()) > 0 && parseInt($('#ddlShipState').val()) > 0) {
            LoadOverlay('body', 'Please wait a few seconds while we reach out to our carriers to get you the best shipping rates possible');
            GetShippingMethod($('#txtShipCode').val(), parseInt($('#ddlShipCountry').val()), parseInt($('#ddlShipState').val() || 0));
        }
    });

    $('#chkShippingAdd').bind("change", function () {
        ChkShippingAdd();
    });

    if (parseInt($("#ddlShipCountry").val()) > 0 && $("#ddlShipState option").length == 0) {
        $.when(FillState($("#ddlShipCountry").val(), $('#ddlShipState'))).then(function (data, textStatus, jqXHR) {
            if ($('#chkShippingAdd').is(":checked")) {
                if ($(this).is(":Checked")) {
                    $(".requiredField").css("display", "none");
                    $("#txtBillStreet").attr("disabled", "disabled");
                    $("#txtBillCity").attr("disabled", "disabled");
                    $("#txtBillCode").attr("disabled", "disabled");
                    $("#ddlBillCountry").attr("disabled", "disabled");
                    $("#ddlBillState").attr("disabled", "disabled");
                } else {
                    $(".requiredField").css("display", "");
                    $("#txtBillStreet").removeAttr("disabled");
                    $("#txtBillCity").removeAttr("disabled");
                    $("#txtBillCode").removeAttr("disabled");
                    $("#ddlBillCountry").removeAttr("disabled");
                    $("#ddlBillState").removeAttr("disabled");
                }


                $('#txtBillStreet').val($('#txtShipStreet').val());
                $('#txtBillCity').val($('#txtShipCity').val());
                $('#txtBillCode').val($('#txtShipCode').val());

                $('#ddlBillCountry').val($('#ddlShipCountry').val());
                FillState($('#ddlBillCountry').val(), $('#ddlBillState'));
                $("#hdnBillState").val($("#ddlShipState").val());

                if ($("#hdnTaxBasedOn").val() == "1" && $('#txtBillCity').val() != '' && $('#txtBillCode').val() != '' && parseInt($('#ddlBillCountry').val()) > 0 && parseInt($('#ddlBillState').val()) > 0) {
                    GetTax($('#txtBillCity').val(), $('#txtBillCode').val(), parseInt($('#ddlBillCountry').val()), parseInt($('#ddlBillState').val()));
                }
            } else if (parseInt($("#ddlBillCountry").val()) > 0 && $("#ddlBillState option").length == 0) {
                FillState($("#ddlBillCountry").val(), $('#ddlBillState'))
            }
        });
    }
});

function LoadOverlay(target, OverlayTitle) {
    var OverlayTitle;
    if (OverlayTitle == "")
        OverlayTitle = "Getting Shipping Quotes....";

    $data = {
        autoCheck: 32,
        size: 32,
        bgColor: '#FFF',
        bgOpacity: 0.8,
        fontColor: false,
        title: OverlayTitle, //文字
        isOnly: true
    };
    //console.log("$(this).data('target'):" + $(this).data('target'));
    switch (target) {
        case 'body':
            $.loader.open($data);
            break;
        case 'close':
            $.loader.close(true);

    }
    $.loader.open($data);
}

function isEmail(email) {
    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return regex.test(email);
}

function validateAddress() {
    if ($("#txtEmail") == null || $("#txtEmail").val() == "") {
        alert("Email is required");
        $("#txtEmail").focus();
        return false;
    } else if (!isEmail($("#txtEmail").val())) {
        alert("Enter valid Email");
        $("#txtEmail").focus();
        return false;
    } else if ($("#txtPhone") == null || $("#txtPhone").val() == "") {
        alert("Phone is required");
        $("#txtPhone").focus();
        return false;
    } else if ($("#txtFirstName") == null || $("#txtFirstName").val() == "") {
        alert("First Name is required");
        $("#txtFirstName").focus();
        return false;
    } else if ($("#txtLastName") == null || $("#txtLastName").val() == "") {
        alert("Last Name is required");
        $("#txtLastName").focus();
        return false;
    }

    if ($("#chkShippingAdd") != null && !$("#chkShippingAdd").is(":checked")) {
        if ($("#txtBillStreet").val() == "") {
            alert("Please enter Bill To Street");
            $("#txtBillStreet").focus();
            return false;
        }
        else if ($("#txtBillCity").val() == "") {
            alert("Please enter Bill To City");
            $("#txtBillCity").focus();
            return false;
        }
        else if ($("#txtBillCode").val() == "") {
            alert("Please enter Bill To Zip Code");
            $("#txtBillCode").focus();
            return false;
        }
        else if ($("#ddlBillCountry option:selected").val() == "0") {
            alert("Please select Bill To County");
            $("#ddlBillCountry").focus();
            return false;
        }
        else if ($("#ddlBillState option:selected").val() == "0") {
            alert("Please enter Bill To State");
            $("#ddlBillState").focus();
            return false;
        }
    }

    if ($("#txtBillAddressName") != null && $("#txtBillAddressName").val() == "") {
        alert("Please enter Bill To Name");
        $("#txtBillAddressName").focus();
        return false;
    }
        //Shipping Adress
    else if ($("#txtShipAddressName") != null && $("#txtShipAddressName").val() == "") {
        alert("Please enter Ship To Name");
        $("#txtShipAddressName").focus();
        return false;
    }
    else if ($("#txtShipStreet").val() == "") {
        alert("Please enter shipping street");
        $("#txtShipStreet").focus();
        return false;
    }
    else if ($("#txtShipCity").val() == "") {
        alert("Please enter shipping city");
        $("#txtShipCity").focus();
        return false;
    }
    else if ($("#txtShipCode").val() == "") {
        alert("Please enter shipping zip code");
        $("#txtShipCode").focus();
        return false;
    }
    else if ($("#ddlShipCountry option:selected").val() == "0") {
        alert("Please select shipping country");
        $("#ddlShipCountry").focus();
        return false;
    }
    else if ($("#ddlShipState option:selected").val() == "0") {
        alert("Please enter shipping state");
        $("#ddlShipState").focus();
        return false;
    }
}

function GetShippingMethod(ZipCode, CountryID, StateID) {
    $.ajax({
        type: "POST",
        url: "/js/OrderServices.asmx/GetStaticShippingCharge",
        data: '{ZipCode: "' + ZipCode + '" , CountryID: ' + CountryID + ' , StateID: ' + StateID + ' , UId:0}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            try {
                var jsonData = $.parseJSON(response.d);

                if (jsonData.isStaticShipping) {
                    $("[id$=hdnIsStaticShippingCharge]").val("1");
                    $("[id$=hdnStaticShippingCharge").val(jsonData.staticShippingCharge || 0);
                    $("[id$=hdnSelectedShippingCharge]").val("0");
                    UpdateTotal();
                } else {
                    $("[id$=hdnIsStaticShippingCharge]").val("0");
                    $("[id$=hdnStaticShippingCharge").val("0");
                    $("[id$=hdnSelectedShippingCharge]").val("-1");
                }

                $.ajax({
                    type: "POST",
                    url: "/js/OrderServices.asmx/GetShippingMethod",
                    data: '{ZipCode: "' + ZipCode + '" , CountryID: ' + CountryID + ' , StateID: ' + StateID + ' , UId:0}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {
                        try {
                            var listItems = "";

                            if ($("[id$=hdnIsStaticShippingCharge]").val() == "1") {
                                listItems += "<input type='radio' name='shippingmethod' value='0' checked='true'> Existing shipping rate - " + $("[id$=hdnStaticShippingCharge").val() + "<br>";
                            }

                            var jsonData = $.parseJSON(response.d);

                            if (jsonData != null && jsonData != 'undefined' && jsonData != "") {
                                for (var i = 0; i < jsonData.length; i++) {
                                    if ($("#chkResidential").is(":checked")) {
                                        if (!jsonData[i].vcServiceTypeID1.match("^3702~")) {
                                            listItems += "<input type='radio' name='shippingmethod' value='" + jsonData[i].vcServiceTypeID1 + "'> " + jsonData[i].vcServiceName + "<br>";
                                        }
                                    } else {
                                        listItems += "<input type='radio' name='shippingmethod' value='" + jsonData[i].vcServiceTypeID1 + "'> " + jsonData[i].vcServiceName + "<br>";
                                    }
                                }
                            } else if ($("[id$=hdnIsStaticShippingCharge]").val() == "0") {
                                listItems += "<input type='radio' name='shippingmethod' value='0~0.00'> NA - $0<br>";
                            }

                            $('#divShippingRates').html(listItems);
                            $("#divShippingMethod").modal('show');
                        } catch (err) {
                            alert("No shipping methods are found for this shipping address. Your option is to correct address and try again.!");
                            $.loader.close();
                        }
                    },
                    error: function () {
                        alert("No shipping methods are found for this shipping address. Your option is to correct address and try again.!");
                    }
                });

                $.loader.close();
            } catch (err) {
                alert("No shipping methods are found for this shipping address. Your option is to correct address and try again.!");
                $.loader.close();
            }
        },
        error: function () {
            alert("No shipping methods are found for this shipping address. Your option is to correct address and try again.!");
            $.loader.close();
        }
    });
}
function GetTax(City, ZipCode, CountryID, StateID) {
    $.ajax({
        type: "POST",
        url: "/js/OrderServices.asmx/GetTax",
        data: '{City:"' + City + '", ZipCode: "' + ZipCode + '" , CountryID: ' + CountryID + ' , StateID: ' + StateID + '}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            var jsonData = response.d;
            if (jsonData != null && jsonData != 'undefined' && jsonData != "") {
                $('#lblTax').html(jsonData);
                UpdateTotal();
            }
        },
        error: function () {
            alert("Error ocurred while calculating Tax.");
        }
    });
}
function FillState(CountryID, select) {
    return $.ajax({
        type: "POST",
        url: "/js/OrderServices.asmx/FillState",
        data: '{CountryID: "' + CountryID + '" }',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            //console.log(response.d);
            var listItems = "";
            var jsonData = $.parseJSON(response.d);
            if (jsonData != null && jsonData != 'undefined' && jsonData != "") {
                for (var i = 0; i < jsonData.length; i++) {
                    //console.log(jsonData[i]);
                    listItems += "<option value='" + jsonData[i].numStateID + "'>" + jsonData[i].vcState + "</option>";
                }
                $(select).html(listItems);
                //$('#ddlShipState').val($('#ddlBillState').val());
                if ($(select).attr('id') == 'ddlBillState' && $("#chkShippingAdd").is(":checked")) {
                    $('#ddlBillState').val($('#ddlShipState').val());
                    $("#hdnBillState").val($("#ddlShipState").val());
                } else if ($(select).attr('id') == 'ddlBillState' && parseInt($("#hdnBillState").val()) > 0) {
                    $('#ddlBillState').val($('#hdnBillState').val());
                }

                if ($(select).attr('id') == 'ddlShipState' && parseInt($("#hdnShipState").val()) > 0) {
                    $('#ddlShipState').val($('#hdnShipState').val());
                }
            }
        },
        failure: function (response) {
            console.log(response.d);
        }
    });
}

function FillAddress(AddressId) {

    $.ajax({
        type: "POST",
        url: "/js/OrderServices.asmx/GetAddressDetails",
        data: '{AddressId: "' + AddressId + '" }',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: OnSuccess,
        failure: function (response) {
            //console.log(response.d);
        }
    });
}

function OnSuccess(response) {
    //console.log(response.d);
    var data = $.parseJSON(response.d);
    //console.log(data);
    if (data != null && data != 'undefined' && data != "") {

        var vcStreet;
        var numAddressID;
        var vcAddressName;
        var vcStreet;
        var vcCity;
        var vcPostalCode;
        var numState;
        var numCountry;
        var bitIsPrimary;
        var tintAddressOf;
        var tintAddressType;

        if (data[0].vcStreet != 'undefined' && data[0].vcStreet != null)
            vcStreet = data[0].vcStreet;
        else
            vcStreet = "";

        if (data[0].vcCity != 'undefined' && data[0].vcCity != null)
            vcCity = data[0].vcCity;
        else
            vcCity = "";

        if (data[0].vcPostalCode != 'undefined' && data[0].vcPostalCode != null)
            vcPostalCode = data[0].vcPostalCode;
        else
            vcPostalCode = "";

        if (data[0].numState != 'undefined' && data[0].numState != null)
            numState = data[0].numState;
        else
            numState = 0;

        if (data[0].numCountry != 'undefined' && data[0].numCountry != null)
            numCountry = data[0].numCountry;
        else
            numCountry = 0;

        if (data[0].vcAddressName != 'undefined' && data[0].vcAddressName != null)
            vcAddressName = data[0].vcAddressName;
        else
            vcAddressName = "";

        // Set Bill To Address Details
        if (data[0].tintAddressType == 1) {
            $('#txtBillStreet').val(vcStreet);
            $('#txtBillCity').val(vcCity);
            $('#txtBillCode').val(vcPostalCode);
            $('#ddlBillState').val(numState);
            $('#ddlBillCountry').val(numCountry);
        }
            // Set Ship To Address Details
        else if (data[0].tintAddressType == 2) {
            $('#txtShipStreet').val(vcStreet);
            $('#txtShipCity').val(vcCity);
            $('#txtShipCode').val(vcPostalCode);
            $('#ddlShipState').val(numState);
            $('#ddlShipCountry').val(numCountry);
        }
    }
    else {
        alert("Address details are not found.")
    }
}

function OpenBizInvoice(a, b) {
    //Apply changes in code behind aswell
    window.open('Invoice.aspx?Show=True&OpID=' + a + '&OppBizId=' + b, '', 'toolbar=no,menubar=yes,titlebar=no,left=100,width=850,height=800,scrollbars=yes,resizable=yes');
}

function GetPaymentMethod(PaymentMethod) {
    document.getElementById('hdnPaymentMethod').value = PaymentMethod;

    if (PaymentMethod == "BillMe") {
        $('#BillMeSection').css("display", "block");
        $('#CreditCardSection').css("display", "none");
        $('#PaypalSection').css("display", "none");
        $('.stepContainer').css("height", $('#step-3').height() + 12 + 'px');
    }
    else if (PaymentMethod == "CreditCard") {
        $('#BillMeSection').css("display", "none");
        $('#CreditCardSection').css("display", "block");
        $('#PaypalSection').css("display", "none");
        $('.stepContainer').css("height", $('#step-3').height() + 12 + 'px');
    }
    else if (PaymentMethod == "GoogleCheckout") {
        $('#BillMeSection').css("display", "none");
        $('#CreditCardSection').css("display", "none");
        $('#PaypalSection').css("display", "none");
        $('.stepContainer').css("height", $('#step-3').height() + 12 + 'px');
    }
    else if (PaymentMethod == "Paypal") {
        $('#BillMeSection').css("display", "none");
        $('#CreditCardSection').css("display", "none");
        $('#PaypalSection').css("display", "block");
        $('.stepContainer').css("height", $('#step-3').height() + 12 + 'px');
    }
    else if (PaymentMethod == "SalesInquiry") {
        $('#BillMeSection').css("display", "none");
        $('#CreditCardSection').css("display", "none");
        $('#PaypalSection').css("display", "none");
        $('.stepContainer').css("height", $('#step-3').height() + 12 + 'px');
        //$("id*=[btnCharge]").removeClass();
    }

    var biggestHeight = "0";

    //$(".content").each(function () {

    //    if ($(this).height() > biggestHeight)
    //        biggestHeight = $(this).height();
    //});

}

function Save() {
    if (validateAddress() == false) {
        return false;
    }
    else if ($("[id$=hdnSelectedShippingCharge]").val() == "-1") {
        alert("Select shipping method to place order.")
        return false;
    }
    else if (!$("#Checkout_radPay").is(":checked") && !$("#Checkout_radGoogleCheckOut").is(":checked") && !$("#Checkout_radPaypalCheckOut").is(":checked") && !$("#Checkout_radBillMe").is(":checked")) {
        alert("Select payment method.")
        return false;
    }
    else if ($("#Checkout_radPay").is(":checked") && $("[id$=txtCHName]").val() == "") {
        alert("Card Holder Name is required")
        $("[id$=txtCHName]").focus();
        return false;
    } else if ($("#Checkout_radPay").is(":checked") && $("[id$=txtCardNumber]").val() == "") {
        alert("	Card Number is required")
        $("[id$=txtCardNumber]").focus();
        return false;
    } else if ($("#Checkout_radPay").is(":checked") && $("[id$=txtCardCVV2]").val() == "") {
        alert("CVV2 is required")
        $("[id$=txtCardCVV2]").focus();
        return false;
    }
    else {
        LoadOverlay('body', 'Please wait a few seconds while your order is being processed');
        __doPostBack('btnCharge', '');
    }
    return true;
}

function CheckNumber(cint, e) {
    var k;
    document.all ? k = e.keyCode : k = e.which;
    if (cint == 1) {
        if (!(k > 47 && k < 58 || k == 44 || k == 46 || k == 8 || k == 37 || k == 39 || k == 16)) {
            if (e.preventDefault) {
                e.preventDefault();
            }
            else
                e.returnValue = false;
            return false;
        }
    }
    if (cint == 2) {
        if (!(k > 47 && k < 58 || k == 8 || k == 37 || k == 39 || k == 16)) {
            if (e.preventDefault) {
                e.preventDefault();
            }
            else
                e.returnValue = false;
            return false;
        }
    }
}

function Mask(obj, evt) {
    evt = (evt) ? evt : window.event
    var charCode = (evt.which) ? evt.which : evt.keyCode
    return true
}

function UpdateTotal() {
    if ($("[id$=hdnSelectedShippingCharge]").val() != -1) {

        if ($("[id$=hdnSelectedShippingCharge]").val() == "0" && $("[id$=hdnIsStaticShippingCharge]").val() == "1") {
            $('#lblShippingCharge').html($("[id$=hdnStaticShippingCharge]").val());
        } else {
            var charge = $("[id$=hdnSelectedShippingCharge]").val().split('~');

            if (charge.length > 1) {
                $('#lblShippingCharge').html(parseFloat(charge[1]));
            }
            else {
                $('#lblShippingCharge').html(0);
            }
        }
    } else {
        $('#lblShippingCharge').html(0);
    }

    var Total;
    var SubTotal, Tax, ShippingCharge, Discount;

    if (parseFloat($('#lblSubTotal').html()) == 'undefined' || parseFloat($('#lblSubTotal').html()) == null || parseFloat($('#lblSubTotal').html()) == '')
        SubTotal = 0;
    else
        SubTotal = parseFloat($('#lblSubTotal').html().replace(',', ''));

    if (parseFloat($('#lblTax').html()) == 'undefined' || parseFloat($('#lblTax').html()) == null || parseFloat($('#lblTax').html()) == '')
        Tax = 0;
    else
        Tax = parseFloat($('#lblTax').html().replace(',', ''));

    if (parseFloat($('#lblShippingCharge').html()) == 'undefined' || parseFloat($('#lblShippingCharge').html()) == null || parseFloat($('#lblShippingCharge').html()) == '')
        ShippingCharge = 0;
    else
        ShippingCharge = parseFloat($('#lblShippingCharge').html().replace(',', ''));

    if (parseFloat($('#lblDiscount').html()) == 'undefined' || parseFloat($('#lblDiscount').html()) == null || parseFloat($('#lblDiscount').html()) == '')
        Discount = 0;
    else
        Discount = parseFloat($('#lblDiscount').html().replace(',', ''));

    Total = SubTotal + Tax + ShippingCharge - Discount;
    $("span#lblTotal").html(Total.toFixed(2));
}

function ChkShippingAdd() {
    if ($('#chkShippingAdd').is(":Checked")) {
        $(".requiredField").css("display", "none");
        $("#txtBillStreet").attr("disabled", "disabled");
        $("#txtBillCity").attr("disabled", "disabled");
        $("#txtBillCode").attr("disabled", "disabled");
        $("#ddlBillCountry").attr("disabled", "disabled");
        $("#ddlBillState").attr("disabled", "disabled");
    } else {
        $(".requiredField").css("display", "");
        $("#txtBillStreet").removeAttr("disabled");
        $("#txtBillCity").removeAttr("disabled");
        $("#txtBillCode").removeAttr("disabled");
        $("#ddlBillCountry").removeAttr("disabled");
        $("#ddlBillState").removeAttr("disabled");
    }

    $('#txtBillStreet').val($('#txtShipStreet').val());
    $('#txtBillCity').val($('#txtShipCity').val());
    $('#txtBillCode').val($('#txtShipCode').val());

    $('#ddlBillCountry').val($('#ddlShipCountry').val());
    $.when(FillState($('#ddlBillCountry').val(), $('#ddlBillState'))).then(function (data, textStatus, jqXHR) {
        if ($("#hdnTaxBasedOn").val() == "1" && $('#txtBillCity').val() != '' && $('#txtBillCode').val() != '' && parseInt($('#ddlBillCountry').val()) > 0 && parseInt($('#ddlBillState').val()) > 0) {
            GetTax($('#txtBillCity').val(), $('#txtBillCode').val(), parseInt($('#ddlBillCountry').val()), parseInt($('#ddlBillState').val()));
        }
    });
}