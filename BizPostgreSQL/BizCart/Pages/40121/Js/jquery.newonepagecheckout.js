﻿$(document).ready(function () {
    $('#ddlBillAddressName').bind("change", function () {
        FillAddress($(this).val());
    });

    $('#ddlShipAddressName').bind("change", function () {
        FillAddress($(this).val());
    });

    $('#ddlBillCountry').bind("change", function () {
        FillState($(this).val(), $('#ddlBillState'));
    });

    $('#ddlShipCountry').bind("change", function () {
        FillState($(this).val(), $('#ddlShipState'));
        GetShippingMethod($('#txtShipCode').val(), parseInt($(this).val()), parseInt($('#ddlShipState').val()));
    });

    $('#btnCharge').bind("click", function () {
        Save();
        LoadOverlay($(this).data('target'), 'Processing your order...!');
    });

    $('#ddlBillState').bind("change", function () {
        $("#hdnBillState").val($(this).val());
    });

    $('#ddlShipState').bind("change", function () {
        LoadOverlay($(this).data('target'), '');
        $("#hdnShipState").val($(this).val());
        GetShippingMethod($('#txtShipCode').val(), parseInt($('#ddlShipCountry').val()), parseInt($(this).val()));
    });

    $('#txtShipCode').bind("blur", function () {
        LoadOverlay($(this).data('target'), '');
        GetShippingMethod($(this).val(), parseInt($('#ddlShipCountry').val()), parseInt($('#ddlShipState').val()));
    });

    $('#chkResidential').bind("change", function () {
        LoadOverlay($(this).data('target'), '');
        GetShippingMethod($('#txtShipCode').val(), parseInt($('#ddlShipCountry').val()), parseInt($('#ddlShipState').val()));
    });

    $('#chkShippingAdd').bind("change", function () {
        LoadOverlay($(this).data('target'), '');

        $('#txtShipStreet').val($('#txtBillStreet').val());
        $('#txtShipCity').val($('#txtBillCity').val());
        $('#txtShipCode').val($('#txtBillCode').val());

        $('#ddlShipCountry').val($('#ddlBillCountry').val());
        FillState($('#ddlShipCountry').val(), $('#ddlShipState'));

        $("#hdnShipState").val($('#ddlBillState').val());
        $('#ddlShipState').val($('#ddlBillState').val());


        GetShippingMethod($('#txtShipCode').val(), parseInt($('#ddlShipCountry').val()), parseInt($('#ddlShipState').val()));
    });

    $('#ddlShippingMethod').bind("change", function () {
        //console.log($(this).val());
        //<%#isShippingMethodNeeded = false %>;
        $('#hdnShippingMethod').val($(this).val());
        //<%#Session["ShippingMethodValue"] = hdnShippingMethod.Value %>;

        var charge;
        charge = $(this).val().split('~');
        //console.log(charge);
        if (charge.length > 1) {
            $('#lblShippingCharge').html(parseFloat(charge[1]));
        }
        else {
            $('#lblShippingCharge').html(0);
        }

        //SubTotal + Tax + Sites.ToDecimal(strShippingCharge) - Discount;
        var Total;
        var SubTotal, Tax, ShippingCharge, Discount;

        if (parseFloat($('#lblSubTotal').html()) == 'undefined' || parseFloat($('#lblSubTotal').html()) == null || parseFloat($('#lblSubTotal').html()) == '')
            SubTotal = 0;
        else
            SubTotal = parseFloat($('#lblSubTotal').html().replace(',', ''));

        if (parseFloat($('#lblTax').html()) == 'undefined' || parseFloat($('#lblTax').html()) == null || parseFloat($('#lblTax').html()) == '')
            Tax = 0;
        else
            Tax = parseFloat($('#lblTax').html().replace(',', ''));

        if (parseFloat($('#lblShippingCharge').html()) == 'undefined' || parseFloat($('#lblShippingCharge').html()) == null || parseFloat($('#lblShippingCharge').html()) == '')
            ShippingCharge = 0;
        else
            ShippingCharge = parseFloat($('#lblShippingCharge').html().replace(',', ''));

        if (parseFloat($('#lblDiscount').html()) == 'undefined' || parseFloat($('#lblDiscount').html()) == null || parseFloat($('#lblDiscount').html()) == '')
            Discount = 0;
        else
            Discount = parseFloat($('#lblDiscount').html().replace(',', ''));

        Total = SubTotal + Tax + ShippingCharge - Discount;
        $("span#lblTotal").html(Total.toFixed(2));
    });

});

function LoadOverlay(target, OverlayTitle) {
    var OverlayTitle;
    if (OverlayTitle == "")
        OverlayTitle = "Getting Shipping Quotes....";

    $data = {
        autoCheck: 32,
        size: 32,
        bgColor: '#FFF',
        bgOpacity: 0.8,
        fontColor: false,
        title: OverlayTitle, //文字
        isOnly: true
    };
    //console.log("$(this).data('target'):" + $(this).data('target'));
    switch (target) {
        case 'body':
            $.loader.open($data);
            break;
        case 'close':
            $.loader.close(true);

    }
    $.loader.open($data);
}
function validateAddress() {
    if ($("#txtBillStreet").val() == "") {
        alert("Please enter billing street");
        $("#txtBillStreet").focus();
        return false;
    }
    else if ($("#txtBillCity").val() == "") {
        alert("Please enter billing city");
        $("#txtBillCity").focus();
        return false;
    }
    else if ($("#txtBillCode").val() == "") {
        alert("Please enter billing zip code");
        $("#txtBillCode").focus();
        return false;
    }
    else if ($("#ddlBillCountry option:selected").val() == "0") {
        alert("Please select billing country");
        $("#ddlBillCountry").focus();
        return false;
    }
    else if ($("#ddlBillState option:selected").val() == "0") {
        alert("Please enter billing state");
        $("#ddlBillState").focus();
        return false;
    }

    //Shipping Adress
    else if ($("#txtShipStreet").val() == "") {
        alert("Please enter shipping street");
        $("#txtShipStreet").focus();
        return false;
    }
    else if ($("#txtShipCity").val() == "") {
        alert("Please enter shipping city");
        $("#txtShipCity").focus();
        return false;
    }
    else if ($("#txtShipCode").val() == "") {
        alert("Please enter shipping zip code");
        $("#txtShipCode").focus();
        return false;
    }
    else if ($("#ddlShipCountry option:selected").val() == "0") {
        alert("Please select shipping country");
        $("#ddlShipCountry").focus();
        return false;
    }
    else if ($("#ddlShipState option:selected").val() == "0") {
        alert("Please enter shipping state");
        $("#ddlShipState").focus();
        return false;
    }
}
function GetStaticShippingCharge(ZipCode, CountryID, StateID) {
    $.ajax({
        type: "POST",
        url: "/js/OrderServices.asmx/GetStaticShippingCharge",
        data: '{ZipCode: "' + ZipCode + '" , CountryID: ' + CountryID + ' , StateID: ' + StateID + ' , UId:0}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            var jsonData = $.parseJSON(response.d);

            if (jsonData.isStaticShipping) {
                $("[id$=hdnIsStaticShippingCharge]").val("1");
                $("[id$=hdnStaticShippingCharge").val(jsonData.staticShippingCharge || 0);
                $("[id$=lblSubTotal]").val(jsonData.staticShippingCharge || 0);
            } else {
                $("[id$=hdnIsStaticShippingCharge]").val("0");
                $("[id$=hdnStaticShippingCharge").val(0);
            }

            $.loader.close();
        },
        error: function () {
            alert("No shipping methods are found for this shipping address. Your option is to correct address and try again.!");
            $('#ddlShippingMethod').html('');
            var listItems = "";
            listItems += "<option value='" + '"0~0.00~0"' + "'>" + '--Select One--' + "</option>";
            $('#ddlShippingMethod').html(listItems);
            $.loader.close();
        }
    });
}

function GetShippingMethod(ZipCode, CountryID, StateID) {
    $.ajax({
        type: "POST",
        url: "/js/OrderServices.asmx/GetShippingMethod",
        data: '{ZipCode: "' + ZipCode + '" , CountryID: ' + CountryID + ' , StateID: ' + StateID + ' , UId:0}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            var listItems = "";

            if ($("[id$=hdnIsStaticShippingCharge]").val() == "1") {
                listItems += "<input type='radio' name='shippingmethod' value='0'> Existing shipping rate - " + $("[id$=hdnStaticShippingCharge").val() + "<br>";
            }

            var jsonData = $.parseJSON(response.d);

            if (jsonData != null && jsonData != 'undefined' && jsonData != "") {
                for (var i = 0; i < jsonData.length; i++) {
                    if ($("#chkResidential").is(":checked")) {
                        if (!jsonData[i].vcServiceTypeID1.match("^3702~")) {
                            listItems += "<input type='radio' name='shippingmethod' value='" + jsonData[i].vcServiceTypeID1 + "'> " + jsonData[i].vcServiceName + "<br>";
                        }
                    } else {
                        listItems += "<input type='radio' name='shippingmethod' value='" + jsonData[i].vcServiceTypeID1 + "'> " + jsonData[i].vcServiceName + "<br>";
                    }
                }
            }
            $('#rblShippingMethod').html(listItems);
            $.loader.close();
        },
        error: function () {
            alert("No shipping methods are found for this shipping address. Your option is to correct address and try again.!");
            $('#ddlShippingMethod').html('');
            var listItems = "";
            listItems += "<option value='" + '"0~0.00~0"' + "'>" + '--Select One--' + "</option>";
            $('#ddlShippingMethod').html(listItems);
            $.loader.close();
        }
    });
}

function FillState(CountryID, select) {
    $.ajax({
        type: "POST",
        url: "/js/OrderServices.asmx/FillState",
        data: '{CountryID: "' + CountryID + '" }',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            //console.log(response.d);
            var listItems = "";
            var jsonData = $.parseJSON(response.d);
            if (jsonData != null && jsonData != 'undefined' && jsonData != "") {
                for (var i = 0; i < jsonData.length; i++) {
                    //console.log(jsonData[i]);
                    listItems += "<option value='" + jsonData[i].numStateID + "'>" + jsonData[i].vcState + "</option>";
                }
                $(select).html(listItems);
                //$('#ddlShipState').val($('#ddlBillState').val());
                if ($(select).attr('id') == 'ddlShipState') {
                    $('#ddlShipState').val($('#ddlBillState').val());
                }
            }
        },
        failure: function (response) {
            console.log(response.d);
        }
    });
}

function FillAddress(AddressId) {

    $.ajax({
        type: "POST",
        url: "/js/OrderServices.asmx/GetAddressDetails",
        data: '{AddressId: "' + AddressId + '" }',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: OnSuccess,
        failure: function (response) {
            //console.log(response.d);
        }
    });
}

function OnSuccess(response) {
    //console.log(response.d);
    var data = $.parseJSON(response.d);
    //console.log(data);
    if (data != null && data != 'undefined' && data != "") {

        var vcStreet;
        var numAddressID;
        var vcAddressName;
        var vcStreet;
        var vcCity;
        var vcPostalCode;
        var numState;
        var numCountry;
        var bitIsPrimary;
        var tintAddressOf;
        var tintAddressType;

        if (data[0].vcStreet != 'undefined' && data[0].vcStreet != null)
            vcStreet = data[0].vcStreet;
        else
            vcStreet = "";

        if (data[0].vcCity != 'undefined' && data[0].vcCity != null)
            vcCity = data[0].vcCity;
        else
            vcCity = "";

        if (data[0].vcPostalCode != 'undefined' && data[0].vcPostalCode != null)
            vcPostalCode = data[0].vcPostalCode;
        else
            vcPostalCode = "";

        if (data[0].numState != 'undefined' && data[0].numState != null)
            numState = data[0].numState;
        else
            numState = 0;

        if (data[0].numCountry != 'undefined' && data[0].numCountry != null)
            numCountry = data[0].numCountry;
        else
            numCountry = 0;

        if (data[0].vcAddressName != 'undefined' && data[0].vcAddressName != null)
            vcAddressName = data[0].vcAddressName;
        else
            vcAddressName = "";

        // Set Bill To Address Details
        if (data[0].tintAddressType == 1) {
            $('#txtBillStreet').val(vcStreet);
            $('#txtBillCity').val(vcCity);
            $('#txtBillCode').val(vcPostalCode);
            $('#ddlBillState').val(numState);
            $('#ddlBillCountry').val(numCountry);
        }
            // Set Ship To Address Details
        else if (data[0].tintAddressType == 2) {
            $('#txtShipStreet').val(vcStreet);
            $('#txtShipCity').val(vcCity);
            $('#txtShipCode').val(vcPostalCode);
            $('#ddlShipState').val(numState);
            $('#ddlShipCountry').val(numCountry);
        }
    }
    else {
        alert("Address details are not found.")
    }
}

function OpenBizInvoice(a, b) {
    //Apply changes in code behind aswell
    window.open('Invoice.aspx?Show=True&OpID=' + a + '&OppBizId=' + b, '', 'toolbar=no,menubar=yes,titlebar=no,left=100,width=850,height=800,scrollbars=yes,resizable=yes');
}

function GetPaymentMethod(PaymentMethod) {
    document.getElementById('hdnPaymentMethod').value = PaymentMethod;

    if (PaymentMethod == "BillMe") {
        $('#BillMeSection').css("display", "block");
        $('#CreditCardSection').css("display", "none");
        $('#PaypalSection').css("display", "none");
        $('.stepContainer').css("height", $('#step-3').height() + 12 + 'px');
    }
    else if (PaymentMethod == "CreditCard") {
        $('#BillMeSection').css("display", "none");
        $('#CreditCardSection').css("display", "block");
        $('#PaypalSection').css("display", "none");
        $('.stepContainer').css("height", $('#step-3').height() + 12 + 'px');
    }
    else if (PaymentMethod == "GoogleCheckout") {
        $('#BillMeSection').css("display", "none");
        $('#CreditCardSection').css("display", "none");
        $('#PaypalSection').css("display", "none");
        $('.stepContainer').css("height", $('#step-3').height() + 12 + 'px');
    }
    else if (PaymentMethod == "Paypal") {
        $('#BillMeSection').css("display", "none");
        $('#CreditCardSection').css("display", "none");
        $('#PaypalSection').css("display", "block");
        $('.stepContainer').css("height", $('#step-3').height() + 12 + 'px');
    }
    else if (PaymentMethod == "SalesInquiry") {
        $('#BillMeSection').css("display", "none");
        $('#CreditCardSection').css("display", "none");
        $('#PaypalSection').css("display", "none");
        $('.stepContainer').css("height", $('#step-3').height() + 12 + 'px');
        //$("id*=[btnCharge]").removeClass();
    }

    var biggestHeight = "0";

    //$(".content").each(function () {

    //    if ($(this).height() > biggestHeight)
    //        biggestHeight = $(this).height();
    //});

}

function Save() {
    if (validateAddress() == false) {
        return false;
    }
    else if (document.getElementById('ddlShippingMethod').value == "0~0.00~0" && document.getElementById('hdnDiscountShipping').value != "1") {
        alert("Select shipping method to place order.")
        document.getElementById('ddlShippingMethod').focus();
        return false;
    }
    else {
        __doPostBack('btnCharge', '');
    }
    return true;
}

function CheckNumber(cint, e) {
    var k;
    document.all ? k = e.keyCode : k = e.which;
    if (cint == 1) {
        if (!(k > 47 && k < 58 || k == 44 || k == 46 || k == 8 || k == 37 || k == 39 || k == 16)) {
            if (e.preventDefault) {
                e.preventDefault();
            }
            else
                e.returnValue = false;
            return false;
        }
    }
    if (cint == 2) {
        if (!(k > 47 && k < 58 || k == 8 || k == 37 || k == 39 || k == 16)) {
            if (e.preventDefault) {
                e.preventDefault();
            }
            else
                e.returnValue = false;
            return false;
        }
    }
}

function Mask(obj, evt) {
    evt = (evt) ? evt : window.event
    var charCode = (evt.which) ? evt.which : evt.keyCode
    return true
}