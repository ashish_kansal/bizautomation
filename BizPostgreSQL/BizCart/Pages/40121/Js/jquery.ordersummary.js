﻿function GetShippingMethod(ZipCode, CountryID, StateID) {
    $.ajax({
        type: "POST",
        url: "/js/OrderServices.asmx/GetShippingMethod",
        data: '{ZipCode: "' + ZipCode + '" , CountryID: ' + CountryID + ' , StateID: ' + StateID + ' , UId:0}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            var listItems = "";

            if ($("[id$=hdnIsStaticShippingCharge]").val() == "1") {
                listItems += "<input type='radio' name='shippingmethod' value='0' checked='true'> Existing shipping rate - " + $("[id$=hdnStaticShippingCharge").val() + "<br>";
            }

            var jsonData = $.parseJSON(response.d);

            if (jsonData != null && jsonData != 'undefined' && jsonData != "") {
                for (var i = 0; i < jsonData.length; i++) {
                    if ($("#chkResidential").is(":checked")) {
                        if (!jsonData[i].vcServiceTypeID1.match("^3702~")) {
                            listItems += "<input type='radio' name='shippingmethod' value='" + jsonData[i].vcServiceTypeID1 + "'> " + jsonData[i].vcServiceName + "<br>";
                        }
                    } else {
                        listItems += "<input type='radio' name='shippingmethod' value='" + jsonData[i].vcServiceTypeID1 + "'> " + jsonData[i].vcServiceName + "<br>";
                    }
                }
            } else if ($("[id$=hdnIsStaticShippingCharge]").val() == "0") {
                listItems += "<input type='radio' name='shippingmethod' value='0~0.00'> NA - $0<br>";
            }

            $('#divShippingRates').html(listItems);

            if($("[id$=UpdateProgress]").length > 0)
            {
                $("[id$=UpdateProgress]").hide();
            }
            
            $("#divShippingMethod").modal('show');
        },
        error: function () {
            alert("No shipping methods are found for this shipping address. Your option is to correct address and try again.!");
            $.loader.close();
        }
    });
}