﻿<%@ Page Language="C#" Inherits="BizCart.BizPage"  %>
<%@ Register TagPrefix="url" Namespace="Intelligencia.UrlRewriter" Assembly="Intelligencia.UrlRewriter" %>
<!DOCTYPE html>
<script runat="server"> 
</script> 
<html xmlns="http://www.w3.org/1999/xhtml"> 
<head runat="server"><title>Categorywise View Products</title> <meta name="keywords" content=""> <meta name="description" content=""><link rel="stylesheet" type="text/css" href="/default.css" /><link rel="stylesheet" type="text/css" href="/bootstrap.min.css" /><script type="text/javascript" src="/Js/jquery-1.9.1.min.js" ></script><script type="text/javascript" src="/Js/bootstrap.min.js" ></script></head> 
<body> 
    <url:form runat="server" id="form1">
<title>test1234:</title>
<center>
<table cellpadding="0" cellspacing="3" class="MainPanel" border="0">
    <tbody>
        <tr>
            <td valign="top">
            <!--header starts-->
            <table width="100%" border="0" cellpadding="0" cellspacing="3">
    <tbody>
        <tr>
            <td id="header">
            <h1>
            test1234<sup>beta</sup></h1>
            <h2>
            by bizautomation</h2>
            </td>
            <td align="right" style="width: 458px;"> <%@ Register src="~/UserControls/LogInStatus.ascx" tagname="LoginStatus" tagprefix="uc1" %> <uc1:LoginStatus ID="LoginStatus6" runat="server"  LogOutLandingPageURL="" HtmlCustomize="{##LogoutSection##}
Welcome: ##CustomerName## | &lt;a class=&quot;LoginLink&quot; href=&quot;/Account.aspx&quot;&gt;My Account&lt;/a&gt;| &lt;a id=&quot;LogoutLink&quot; class=&quot;LogOutLink&quot; href=&quot;javascript:__doPostBack(&#39;LogoutPostback&#39;,&#39;&#39;)&quot;&gt;Log out&lt;/a&gt; 
{/##LogoutSection##} 
{##LoginSection##}
&lt;a class=&quot;LoginLink&quot; href=&quot;/Login.aspx&quot;&gt;Login&lt;/a&gt; | &lt;a class=&quot;SingupLink&quot; href=&quot;/SignUp.aspx&quot;&gt;Create Account&lt;/a&gt;
{/##LoginSection##}
&lt;br /&gt;
&lt;b&gt;&lt;a href=&quot;/Cart.aspx&quot; class=&quot;dropdown-toggle&quot; aria-controls=&quot;divMiniCart&quot; data-toggle=&quot;dropdown&quot;&gt;##ItemCount## Item(s)&lt;/a&gt; (##TotalAmount##)&lt;/b&gt;"   />  <%@ Register src="~/UserControls/MiniCart.ascx" tagname="MiniCart" tagprefix="uc1" %> <uc1:MiniCart ID="MiniCart8" runat="server"  HtmlCustomize="&lt;link rel=&quot;stylesheet&quot; href=&quot;https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css&quot;&gt;
  &lt;script src=&quot;https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js&quot;&gt;&lt;/script&gt;
  &lt;script src=&quot;https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js&quot;&gt;&lt;/script&gt;
&lt;style type=&quot;text/css&quot;&gt;
    #tblBasket tr th {
        text-align:center !important;
    }

    #tblBasket tr th:first-child {
        width:85px;
    }

    #tblBasket tr td:nth-child(3) {
        width:90px;
        text-align:center;
    }

     #tblBasket tr td:nth-child(4) {
        width:130px;
        text-align:center;
    }

     #tblBasket tr td:nth-child(5) {
        width:130px;
        text-align:center;
    }

    #tblBasket tr td {
        vertical-align:middle;
    }
&lt;/style&gt;
&lt;script type=&quot;text/javascript&quot;&gt;
    function ShowMiniCart() {
        $(&quot;#divMiniCart&quot;).show();
    }
&lt;/script&gt;

&lt;div id=&quot;divMiniCart&quot; class=&quot;dropdown-menu&quot;&gt;
    &lt;div class=&quot;modal-dialog modal-dialog-minicart&quot; style=&quot;width:70%&quot;&gt;
        &lt;!-- Modal content--&gt;
        &lt;div class=&quot;modal-content modal-content-minicart&quot;&gt;
            &lt;div class=&quot;modal-header modal-content-minicart&quot;&gt;
                &lt;div class=&quot;pull-left&quot;&gt;
                    &lt;h4 class=&quot;modal-title modal-title-minicart&quot;&gt;
                        &lt;img id=&quot;imgShopping&quot; src=&quot;/images/shopping_cart.gif&quot; align=&quot;absmiddle&quot; style=&quot;border-width: 0px;&quot;&gt;
                        Your Cart
                    &lt;/h4&gt;
                &lt;/div&gt;
                &lt;button type=&quot;button&quot; class=&quot;close&quot; data-dismiss=&quot;modal&quot;&gt;&#215;&lt;/button&gt;
            &lt;/div&gt;
            &lt;div class=&quot;modal-body modal-body-minicart&quot;&gt;
                &lt;div class=&quot;row&quot;&gt;
                    &lt;div class=&quot;col-xs-12&quot;&gt;
                        &lt;div class=&quot;table-responsive&quot;&gt;
                            &lt;table class=&quot;table table-bordered table-striped&quot; id=&quot;tblBasket&quot;&gt;
                                &lt;thead&gt;
                                    &lt;tr&gt;
                                        &lt;th&gt;&lt;/th&gt;
                                        &lt;th&gt;Item&lt;/th&gt;
                                        &lt;th&gt;Qty&lt;/th&gt;
                                        &lt;th&gt;Unit Price&lt;/th&gt;
                                        &lt;th&gt;Total&lt;/th&gt;
                                    &lt;/tr&gt;
                                &lt;/thead&gt;
                                &lt;##FOR(ItemClass=YourClassName;AlternatingItemClass=YourClassName;)##&gt;
                                &lt;tr class=&quot;##RowClass##&quot;&gt;
                                    &lt;td&gt;
                                        &lt;img src=&quot;##ItemImage##&quot; alt=&quot;&quot; style=&quot;width:80px;&quot; class=&quot;img-responsive&quot; /&gt;
                                    &lt;/td&gt;
                                    &lt;td&gt;
                                        &lt;div class=&quot;pull-left&quot;&gt;
                                            &lt;a href=&quot;##ItemLink##&quot;&gt;##ItemName##&lt;/a&gt;
                                        &lt;/div&gt;
                                        &lt;div class=&quot;pull-right&quot;&gt;
                                            ##ItemAttributes##
                                        &lt;/div&gt;
                                    &lt;/td&gt;
                                    &lt;td&gt;##ItemUnits##
                                    &lt;/td&gt;
                                    &lt;td&gt;##CurrencySymbol## ##ItemPricePerUnit##
                                    &lt;/td&gt;
                                    &lt;td&gt;##CurrencySymbol## ##TotalAmount##
                                    &lt;/td&gt;
                                &lt;/tr&gt;
                                &lt;##ENDFOR##&gt;
                            &lt;/table&gt;
                        &lt;/div&gt;
                    &lt;/div&gt;

                &lt;/div&gt;
                &lt;div class=&quot;row&quot;&gt;
                    &lt;div class=&quot;col-xs-12 pull-right&quot;&gt;
                        &lt;b&gt;SUBTOTAL: ##CurrencySymbol## ##SubTotal##&lt;/b&gt;
                    &lt;/div&gt;
                &lt;/div&gt;
            &lt;/div&gt;
            &lt;div class=&quot;modal-footer modal-footer-minicart&quot;&gt;
                &lt;a id=&quot;hplCart&quot; class=&quot;btn btn-primary pull-left&quot; href=&quot;/Cart.aspx&quot;&gt;View Cart&lt;/a&gt;
                &lt;a id=&quot;hplCheckout&quot; class=&quot;btn btn-primary pull-right&quot; href=&quot;##CheckOutURL##&quot;&gt;Check Out&lt;/a&gt;
            &lt;/div&gt;
        &lt;/div&gt;
    &lt;/div&gt;
&lt;/div&gt;"   /> <img alt="" src="/images/a10.jpg" />
            </td>
        </tr>
        <tr>
            <td colspan="2">
             <%@ Register src="~/UserControls/SiteMenu.ascx" tagname="Menu" tagprefix="uc1" %> <uc1:Menu ID="Menu7" runat="server"  MenuDirection="Horizontal" Skin="Default" PopulateULLIbasedMenu="True" HtmlCustomize="&lt;!--Remove DefaultMenu , if you are planning to use custom menu--&gt;
{##CustomMenu##}
&lt;div id=&quot;vertSiteMenu&quot;&gt;
    &lt;ul class=&quot;TreeNode&quot;&gt;
        &lt;##FOR##&gt;
        &lt;li&gt;&lt;a href=&quot;##MenuItemLink##&quot;&gt;##MenuItemName##&lt;/a&gt;&lt;/li&gt;
        &lt;##ENDFOR##&gt;
    &lt;/ul&gt;
&lt;/div&gt;
{/##CustomMenu##}"   /> 
            </td>
        </tr>
        <tr>
            <td colspan="2">
             <%@ Register src="~/UserControls/ShippingPromotion.ascx" tagname="ShippingPromotion" tagprefix="uc1" %> <uc1:ShippingPromotion ID="ShippingPromotion12" runat="server"  HtmlCustomize=""   /> &nbsp; <%@ Register src="~/UserControls/OrderPromotion.ascx" tagname="OrderPromotion" tagprefix="uc1" %> <uc1:OrderPromotion ID="OrderPromotion9" runat="server"  HtmlCustomize=""   /> </td>
        </tr>
        <tr>
            <td colspan="2">
             <%@ Register src="~/UserControls/BreadCrumb.ascx" tagname="BreadCrumb" tagprefix="uc1" %> <uc1:BreadCrumb ID="BreadCrumb1" runat="server"    /> 
            </td>
        </tr>
    </tbody>
</table>
            <!--header ends-->
            </td>
        </tr>
        <tr>
            <td valign="top">
            <table width="100%">
                <tbody>
                    <tr>
                        <td valign="top" style="width: 260px;">
                        <!--left panel starts-->
                         <%@ Register src="~/UserControls/Categories.ascx" tagname="Categories" tagprefix="uc1" %> <uc1:Categories ID="Categories2" runat="server"  ShowOnlyParentCategoriesinTree="False" PreserveStateofTree="False" HtmlCustomize="&lt;div class=&quot;boxheader&quot; id=&quot;divHeader&quot;&gt;
    Browse Categories
&lt;/div&gt;
&lt;div class=&quot;boxcontent&quot; id=&quot;divContent&quot;&gt;
    ##Menu##
    &lt;br /&gt;
    &lt;div class=&quot;left-category&quot;&gt;
        &lt;div class=&quot;catgry-hed&quot;&gt;Categories&lt;/div&gt;
        &lt;div class=&quot;category-list css-treeview&quot;&gt;
            &lt;div class=&quot;css-treeview&quot;&gt;
                &lt;##TreeViewCustom##&gt;
                &lt;ul class=&quot;TreeNodeParent&quot;&gt;
                    &lt;##FOREACH##&gt;
        		&lt;li&gt;
                    &lt;input type=&quot;checkbox&quot; id=&quot;##ITEMID##&quot;&gt;&lt;/input&gt;&lt;label for=&quot;##ITEMID##&quot;&gt;&lt;a href=&quot;/SubCategory/##CategoryName##/##CategoryID##&quot;&gt;##CategoryName##&lt;/a&gt;&lt;/label&gt;
                    &lt;##IFCHILDAVAILABLE##&gt;
        				&lt;ul class=&quot;TreeNodeChild&quot;&gt;
                            &lt;##FOREACHCHILD##&gt;
        				&lt;li&gt;
                            &lt;input type=&quot;checkbox&quot; id=&quot;Checkbox1&quot; /&gt;&lt;label for=&quot;##ITEMID##&quot;&gt;&lt;a href=&quot;/SubCategory/##CategoryName##/##CategoryID##&quot;&gt;##CategoryName##&lt;/a&gt;&lt;/label&gt;
                            &lt;##INSERTCHILDHERE##&gt;        			
                        &lt;/li&gt;
                            &lt;/##FOREACHCHILD##&gt;
                        &lt;/ul&gt;
                    &lt;/##IFCHILDAVAILABLE##&gt;
                &lt;/li&gt;
                    &lt;/##FOREACH##&gt;
                &lt;/ul&gt;
                &lt;/##TreeViewCustom##&gt;
            &lt;/div&gt;
        &lt;/div&gt;
    &lt;/div&gt;
&lt;/div&gt;" PopulateULLIBasedTree="False"   /> <br />
 <%@ Register src="~/UserControls/Search.ascx" tagname="Search" tagprefix="uc1" %> <uc1:Search ID="Search11" runat="server"  HtmlCustomize="&lt;div class=&quot;boxheader&quot;&gt;
    Search&lt;/div&gt;
&lt;div class=&quot;boxcontent&quot;&gt;
    &#160; &lt;span&gt;##SearchTextBox## &#160; ##SearchButton## &lt;/span&gt;
&lt;/div&gt;"   /> 
                        <br />
                        <!--left panel ends-->
                        </td>
                        <td valign="top" style="padding-left: 15px;">
                        <!--Content panel starts-->
                         <%@ Register src="~/UserControls/Error.ascx" tagname="Error" tagprefix="uc1" %> <uc1:Error ID="Error3" runat="server"    /> <br />
                         <%@ Register src="~/UserControls/ProductFilters.ascx" tagname="ProductFilters" tagprefix="uc1" %> <uc1:ProductFilters ID="ProductFilters10" runat="server"  HtmlCustomize="&lt;script src=&quot;/Js/select2.js&quot; type=&quot;text/javascript&quot;&gt;&lt;/script&gt;
&lt;link rel=&quot;stylesheet&quot; type=&quot;text/css&quot; href=&quot;/select2.css&quot; /&gt;
&lt;script type=&quot;text/javascript&quot;&gt;
    function setFilter(filter) {
        if (document.location.href.toLowerCase().indexOf(&quot;f~&quot;) &gt; 0 || document.location.href.toLowerCase().indexOf(&quot;c~&quot;) &gt; 0) {
            $(&quot;#hdnFilter&quot;).val($(&quot;#hdnFilter&quot;).val() + filter);
            document.location.href = document.location.href + $(&quot;#hdnFilter&quot;).val();
        }
        else {
            $(&quot;#hdnFilter&quot;).val($(&quot;#hdnFilter&quot;).val() + filter);
            document.location.href = document.location.href + &quot;/&quot; + $(&quot;#hdnFilter&quot;).val();
        }
    }

    function setFilterPrice() {
        var FilterQueryPrice = &quot;f~&quot; + $(&quot;#slider-range&quot;).slider(&quot;values&quot;, 0) + &quot;~Min&quot; + &quot;^&quot; + $(&quot;#slider-range&quot;).slider(&quot;values&quot;, 1) + &quot;~Max&quot; + &quot;;&quot;;

        var txt = decodeURIComponent(document.location.href)
        var re1 = &#39;(f)&#39;;
        var re2 = &#39;(~)&#39;;
        var re3 = &#39;(\\d+)&#39;;
        var re4 = &#39;(~)&#39;;
        var re5 = &#39;(Min)&#39;;
        var re6 = &#39;(\\^)&#39;;
        var re7 = &#39;(\\d+)&#39;;
        var re8 = &#39;(~)&#39;;
        var re9 = &#39;(Max)&#39;;
        var re10 = &#39;(;)&#39;;
        var p = new RegExp(re1 + re2 + re3 + re4 + re5 + re6 + re7 + re8 + re9 + re10, [&quot;i&quot;]);
        var m = p.exec(txt);

        if (m != null) {
            document.location.href = (decodeURIComponent(document.location.href).replace(m[0], FilterQueryPrice));
        }
        else {
            setFilter(FilterQueryPrice);
        }
    }

    function removeFilter(filter) {
        document.location.href = rtrim(String(decodeURIComponent(document.location.href)).replace(filter + &quot;;&quot;, &quot;&quot;), &#39;/&#39;);
    }

    function trim(str, chars) {
        return ltrim(rtrim(str, chars), chars);
    }

    function ltrim(str, chars) {
        chars = chars || &quot;\\s&quot;;
        return str.replace(new RegExp(&quot;^[&quot; + chars + &quot;]+&quot;, &quot;g&quot;), &quot;&quot;);
    }

    function rtrim(str, chars) {
        chars = chars || &quot;\\s&quot;;
        return str.replace(new RegExp(&quot;[&quot; + chars + &quot;]+$&quot;, &quot;g&quot;), &quot;&quot;);
    }

    function setSliderRange() {

        $(&quot;#slider-range&quot;).slider(
            {
                range: true,

                min: Number($(&quot;#hdnMin&quot;).val()),
                max: Number($(&quot;#hdnMax&quot;).val()),
                values: [Number($(&quot;#hdnSelectMin&quot;).val()), Number($(&quot;#hdnSelectMax&quot;).val())],
                slide: function (event, ui) {
                    $(&quot;#amount&quot;).val(&quot;$&quot; + ui.values[0] + &quot; - $&quot; + ui.values[1]);
                }
            });
        $(&quot;#amount&quot;).val(&quot;Rs.&quot; + $(&quot;#slider-range&quot;).slider(&quot;values&quot;, 0) +
            &quot; - Rs.&quot; + $(&quot;#slider-range&quot;).slider(&quot;values&quot;, 1));
    }
&lt;/script&gt;
&lt;style&gt;
.divProductFiltersContainer .select2-selection{
    border-top-left-radius: 0;
    border-bottom-left-radius: 0;
}
&lt;/style&gt;
&lt;div class=&quot;row&quot;&gt;
&lt;##FOR##&gt;
        &lt;div class=&quot;col-xs-12 col-md-3&quot;&gt;
        &lt;div class=&quot;input-group&quot;&gt;
  &lt;span class=&quot;input-group-addon&quot;&gt;##FilterLabel##&lt;/span&gt;
  ##FilterControl##
&lt;/div&gt;
        
        &lt;/div&gt;
&lt;##ENDFOR##&gt;
&lt;/div&gt;"   /> <br />
                         <%@ Register src="~/UserControls/ItemList.ascx" tagname="ItemList" tagprefix="uc1" %> <uc1:ItemList ID="ItemList4" runat="server"  ApplyPriceBookPrice="True" TrimDescriptionbyfollowingnoofcharacters="" TrimItemNamebyfollowingnoofcharacters="" HtmlCustomize="&lt;table border=&quot;0&quot; width=&quot;100%&quot;&gt;

    &lt;tr&gt;
        &lt;td&gt;
            ##ManufacturerLogo##
        &lt;/td&gt;
        &lt;td colspan=&quot;2&quot;&gt;
            ##ManufacturerName##
            ##ManufacturerDescription##
        &lt;/td&gt;
    &lt;/tr&gt;
   &lt;tr&gt;
       &lt;td colspan=&quot;3&quot;&gt;
               {##CategoryImageSection##}
                &lt;div&gt;  
                       &lt;img src=&#39;##CategoryImage##&#39; alt=&#39;##CategoryName##&#39; title=&#39;##CategoryName##&#39;  /&gt;
                &lt;/div&gt;
                {/##CategoryImageSection##}
       &lt;/td&gt;
   &lt;/tr&gt;
    &lt;tr&gt;
        &lt;td colspan=&quot;3&quot;&gt;
            &lt;div class=&quot;sectionheader&quot;&gt;
                ##CategoryName##
            &lt;/div&gt;
            &lt;div class=&quot;Desc&quot;&gt;
                ##CategoryDesc##
            &lt;/div&gt;
        &lt;/td&gt;
    &lt;/tr&gt;
    &lt;tr&gt;
       &lt;td&gt;
             &lt;div&gt;      
                 ##FILTERBY##
                &lt;##FOR_FILTER##&gt;
                    ##FilterAttribute## &lt;font color=&quot;red&quot;&gt;&lt;a href=&quot;javascript:void(0)&quot;  onclick =&quot;javascript:removeFilter(&#39;##FilterValue##&#39;);&quot;&gt;X&lt;/a&gt;&lt;/font&gt; 
                &lt;##ENDFOR_FILTER##&gt; 
              &lt;/div&gt;
       &lt;/td&gt;
    &lt;/tr&gt;
    &lt;tr&gt;
        &lt;td colspan=&quot;3&quot;&gt;
            &lt;table width=&quot;100%&quot;&gt;
                &lt;tr&gt;
                    &lt;td&gt;
                    &lt;/td&gt;
                    &lt;td width=&quot;50&quot; align=&quot;right&quot;&gt;
                        Sort by
                    &lt;/td&gt;
                    &lt;td align=&quot;left&quot; width=&quot;150&quot; style=&quot;vertical-align: bottom&quot;&gt;
                        ##SortBy##
                    &lt;/td&gt;
                    &lt;td align=&quot;right&quot; width=&quot;85&quot;&gt;
                        Items per page
                    &lt;/td&gt;
                    &lt;td align=&quot;left&quot; width=&quot;50&quot;&gt;
                        ##PageSize##
                    &lt;/td&gt;
                &lt;/tr&gt;
            &lt;/table&gt;
        &lt;/td&gt;
    &lt;/tr&gt;
    &lt;tr&gt;
        &lt;td align=&quot;right&quot; align=&quot;left&quot;&gt;
            ##ItemCount##
        &lt;/td&gt;
        &lt;td colspan=&quot;3&quot; align=&quot;right&quot;&gt;
            ##PageControl##
        &lt;/td&gt;
        &lt;td&gt;
            &#160;##SelectAllCheckBox##
            ##AddToMyItems##
            ##AddToCartSelected##
        &lt;/td&gt;
    &lt;/tr&gt;
    &lt;tr&gt;
        &lt;td valign=&quot;top&quot; colspan=&quot;3&quot;&gt;
           &lt;div class=&quot;table-responsive&quot;&gt;
            &lt;table class=&quot;table table-bordered&quot;&gt;
                &lt;thead class=&quot;table-light&quot;&gt;
                    &lt;tr class=&quot;d-none d-md-table-row d-lg-table-row&quot;&gt;
                        &lt;th class=&quot;text-nowrap&quot;&gt;Image&lt;/th&gt;
                        &lt;th&gt;Item Name&lt;/th&gt;
                        &lt;##FOR_ATTRIBUTE_HEADER##&gt;
	                &lt;th class=&quot;text-nowrap&quot;&gt;##ATTRIBUTE_NAME##&lt;/th&gt;
	                &lt;##ENDFOR_ATTRIBUTE_HEADR##&gt; 
                        &lt;th class=&quot;text-nowrap&quot;&gt;Available&lt;/th&gt;
                        &lt;th class=&quot;text-nowrap&quot;&gt;In-Transit&lt;/th&gt;
                        &lt;th class=&quot;text-nowrap&quot;&gt;Price&lt;/th&gt;
                        &lt;th class=&quot;text-nowrap&quot; style=&quot;width: 110px;&quot;&gt;Qty&lt;/th&gt;
                        &lt;th class=&quot;text-nowrap&quot; style=&quot;width: 180px;&quot;&gt;&lt;/th&gt;
                    &lt;/tr&gt;
                &lt;/thead&gt;
                &lt;tbody&gt;
                    &lt;##FOR##&gt;
                    &lt;tr class=&quot;d-xs-block d-sm-block d-md-none d-lg-none&quot;&gt;
                        &lt;td&gt;
                            &lt;div class=&quot;container&quot;&gt;
                                &lt;div class=&quot;row g-0 mb-3&quot;&gt;
                                    &lt;div class=&quot;col-12 text-center mb-3&quot;&gt;
                                        &lt;img src=&#39;##ItemImagePathName##&#39; alt=&#39;##ItemName##&#39; title=&#39;##ItemName##&#39; height=&quot;50&quot; /&gt;
                                    &lt;/div&gt;
                                    &lt;div class=&quot;col-6 p-1 text-end&quot;&gt;&lt;b&gt;Item&lt;/b&gt;&lt;/div&gt;
                                    &lt;div class=&quot;col-6 p-1&quot;&gt;
                                        &lt;div class=&quot;product_title&quot; title=&#39;##ItemName##&#39;&gt;&lt;a href=&#39;##ItemLink##&#39; target=&quot;_blank&quot;&gt;##ItemShortName##&lt;/a&gt;&lt;/div&gt;
                                    &lt;/div&gt;
                                    &lt;div class=&quot;col-6 p-1 text-end&quot;&gt;&lt;b&gt;Price&lt;/b&gt;&lt;/div&gt;
                                    &lt;div class=&quot;col-6 p-1&quot;&gt;##OfferPrice## ##ProductPrice##&lt;/div&gt;
                                    &lt;div class=&quot;col-6 p-1 text-end&quot;&gt;&lt;b&gt;Available&lt;/b&gt;&lt;/div&gt;
                                    &lt;div class=&quot;col-6 p-1&quot;&gt;##WarehouseAvailability##&lt;/div&gt;
                                    &lt;div class=&quot;col-6 p-1 text-end&quot;&gt;&lt;b&gt;In-Transit&lt;/b&gt;&lt;/div&gt;
                                    &lt;div class=&quot;col-6 p-1&quot;&gt;##WarehouseInTransit##&lt;/div&gt;
                                    &lt;##FOR_ATTRIBUTE_DATA##&gt;
                                    &lt;div class=&quot;col-6 p-1 text-end&quot;&gt;&lt;b&gt;##ATTRIBUTE_NAME##&lt;/b&gt;&lt;/div&gt;
                                    &lt;div class=&quot;col-6 p-1&quot;&gt;##ATTRIBUTE_VALUE_PLACEHOLDER##&lt;/div&gt;
                                    &lt;##ENDFOR_ATTRIBUTE_DATA##&gt;
                                &lt;/div&gt;
                                &lt;div class=&quot;row g-0&quot;&gt;
                                    &lt;div class=&quot;col text-center&quot;&gt;
                                        &lt;ul class=&quot;list-inline&quot;&gt;
                                            &lt;li class=&quot;list-inline-item&quot; style=&quot;width:100px;&quot;&gt;##Quantity##&lt;/li&gt;
                                            &lt;li class=&quot;list-inline-item&quot;&gt;&lt;a href=&#39;##ItemLink##&#39; class=&quot;btn btn-primary&quot; target=&quot;_blank&quot;&gt;&lt;i class=&quot;fas fa-external-link-alt&quot;&gt;&lt;/i&gt;&lt;/a&gt;&lt;/li&gt;
                                            &lt;li class=&quot;list-inline-item&quot;&gt;##AddToCartLink##&lt;/li&gt;
                                        &lt;/ul&gt;
                                    &lt;/div&gt;
                                &lt;/div&gt;
                            &lt;/div&gt;
                        &lt;/td&gt;
                    &lt;/tr&gt;
                    &lt;tr class=&quot;d-none d-md-table-row d-lg-table-row&quot;&gt;
                        &lt;td&gt;
                            &lt;img src=&#39;##ItemImagePathName##&#39; alt=&#39;##ItemName##&#39; title=&#39;##ItemName##&#39; height=&quot;50&quot; /&gt;
                        &lt;/td&gt;
                        &lt;td&gt;
                            &lt;div class=&quot;product_title&quot; title=&#39;##ItemName##&#39;&gt;
                                &lt;a href=&#39;##ItemLink##&#39; target=&quot;_blank&quot;&gt;##ItemShortName##&lt;/a&gt;
                            &lt;/div&gt;
                        &lt;/td&gt;
                        &lt;##FOR_ATTRIBUTE_DATA##&gt;
	                    &lt;td class=&quot;text-nowrap&quot;&gt;
	                        ##ATTRIBUTE_VALUE_PLACEHOLDER##
	                    &lt;/td&gt;
                        &lt;##ENDFOR_ATTRIBUTE_DATA##&gt;
                        &lt;td class=&quot;text-nowrap text-end&quot;&gt;##WarehouseAvailability##&lt;/td&gt;
                        &lt;td class=&quot;text-nowrap text-end&quot;&gt;##WarehouseInTransit##&lt;/td&gt;
                        &lt;td class=&quot;text-nowrap&quot;&gt;##OfferPrice## ##ProductPrice##&lt;/td&gt;
                        &lt;td class=&quot;text-nowrap&quot;&gt;##Quantity##&lt;/td&gt;
                        &lt;td class=&quot;text-nowrap&quot;&gt;
                            &lt;a href=&#39;##ItemLink##&#39; class=&quot;btn btn-primary&quot; target=&quot;_blank&quot;&gt;&lt;i class=&quot;fas fa-external-link-alt&quot;&gt;&lt;/i&gt;&lt;/a&gt;
                            ##AddToCartLink##
                        &lt;/td&gt;
                    &lt;/tr&gt;
                    &lt;##ENDFOR##&gt;
                &lt;/tbody&gt;
            &lt;/table&gt;
        &lt;/div&gt;
            
        &lt;/td&gt;
    &lt;/tr&gt;
&lt;/table&gt;" AfterAddingItemShowCartPage="False"   /> 
                        <br />
                         <%@ Register src="~/UserControls/ItemPromotionPopup.ascx" tagname="ItemPromotionPopup" tagprefix="uc1" %> <uc1:ItemPromotionPopup ID="ItemPromotionPopup5" runat="server"  HtmlCustomize="&lt;!-- IMPORTANT: Do not change id of element or onclick function --&gt;

&lt;div id=&quot;divItemPromoPopup&quot; class=&quot;modal fade modal-itempromopopup&quot; role=&quot;dialog&quot;&gt;
    &lt;div class=&quot;modal-dialog modal-dialog-itempromopopup&quot; style=&quot;width:70%&quot;&gt;
        &lt;div class=&quot;modal-content modal-content-itempromopopup&quot;&gt;
            &lt;div class=&quot;modal-header modal-content-itempromopopup&quot;&gt;
                &lt;div style=&quot;float: left;&quot;&gt;
                    &lt;img src=&quot;/images/item-promotion.png&quot; height=&quot;30&quot; alt=&quot;&quot; /&gt;
                    &lt;b&gt;Item Promotion(s)&lt;/b&gt;
                &lt;/div&gt;
                &lt;div class=&quot;pull-right&quot;&gt;
                    &lt;button onclick=&quot;return SaveItemPromoSelection();&quot; class=&quot;btn btn-primary&quot;&gt;Save &amp; Close&lt;/button&gt;
                &lt;/div&gt;
            &lt;/div&gt;
            &lt;div class=&quot;modal-body modal-body-itempromopopup&quot;&gt;
                &lt;div class=&quot;row&quot;&gt;
                    &lt;div class=&quot;col-xs-12&quot; id=&quot;divItemPromoList&quot;&gt;

                    &lt;/div&gt;
                &lt;/div&gt;
            &lt;/div&gt;
        &lt;/div&gt;
    &lt;/div&gt;
&lt;/div&gt;"   /> </td>
                    </tr>
                </tbody>
            </table>
            </td>
        </tr>
        <tr>
            <td>
            <!--footer starts-->
            <div id="footer">
<a href="#">Temrs &amp; Conditions</a>&nbsp;&nbsp;&nbsp;<a href="#">Newsletters</a>&nbsp;&nbsp;&nbsp;<a href="#">Sitemap</a><br />
&copy; test1234. All rights reserved. Powered by <a href="http://www.BizAutomation.com">
BizAutomation</a>.
</div>
<script src="/Js/jquery-loader.js" type="text/javascript"></script>
            <!--footer ends-->
            </td>
        </tr>
    </tbody>
</table>
</center></url:form> 
</body> 
</html>