﻿<%@ Page Language="C#" Inherits="BizCart.BizPage"  MaintainScrollPositionOnPostback="true"  %>
<%@ Register TagPrefix="url" Namespace="Intelligencia.UrlRewriter" Assembly="Intelligencia.UrlRewriter" %>
<!DOCTYPE html>
<script runat="server"> 
</script> 
<html xmlns="http://www.w3.org/1999/xhtml"> 
<head runat="server"><title>Test - Guest Checkout</title> <meta name="keywords" content=""> <meta name="description" content=""><link rel="stylesheet" type="text/css" href="/default.css" /><script type="text/javascript" src="/Js/Common.js" ></script></head> 
<body> 
    <url:form runat="server" id="form1">
<title>test1234:</title>
<center>
<table cellpadding="0" cellspacing="3" class="MainPanel" border="0">
    <tbody>
        <tr>
            <td valign="top">
            <!--header starts-->
            <table width="100%" border="0" cellpadding="0" cellspacing="3">
    <tbody>
        <tr>
            <td id="header">
            <h1>
            test1234<sup>beta</sup></h1>
            <h2>
            by bizautomation</h2>
            </td>
            <td align="right" style="width: 458px;"> <%@ Register src="~/UserControls/LogInStatus.ascx" tagname="LoginStatus" tagprefix="uc1" %> <uc1:LoginStatus ID="LoginStatus4" runat="server"  LogOutLandingPageURL="" HtmlCustomize="{##LogoutSection##}
Welcome: ##CustomerName## | &lt;a class=&quot;LoginLink&quot; href=&quot;/Account.aspx&quot;&gt;My Account&lt;/a&gt;| &lt;a id=&quot;LogoutLink&quot; class=&quot;LogOutLink&quot; href=&quot;javascript:__doPostBack(&#39;LogoutPostback&#39;,&#39;&#39;)&quot;&gt;Log out&lt;/a&gt; 
{/##LogoutSection##} 
{##LoginSection##}
&lt;a class=&quot;LoginLink&quot; href=&quot;/Login.aspx&quot;&gt;Login&lt;/a&gt; | &lt;a class=&quot;SingupLink&quot; href=&quot;/SignUp.aspx&quot;&gt;Create Account&lt;/a&gt;
{/##LoginSection##}
&lt;br /&gt;
&lt;b&gt;&lt;a href=&quot;/Cart.aspx&quot; class=&quot;dropdown-toggle&quot; aria-controls=&quot;divMiniCart&quot; data-toggle=&quot;dropdown&quot;&gt;##ItemCount## Item(s)&lt;/a&gt; (##TotalAmount##)&lt;/b&gt;"   />  <%@ Register src="~/UserControls/MiniCart.ascx" tagname="MiniCart" tagprefix="uc1" %> <uc1:MiniCart ID="MiniCart6" runat="server"  HtmlCustomize="&lt;link rel=&quot;stylesheet&quot; href=&quot;https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css&quot;&gt;
  &lt;script src=&quot;https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js&quot;&gt;&lt;/script&gt;
  &lt;script src=&quot;https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js&quot;&gt;&lt;/script&gt;
&lt;style type=&quot;text/css&quot;&gt;
    #tblBasket tr th {
        text-align:center !important;
    }

    #tblBasket tr th:first-child {
        width:85px;
    }

    #tblBasket tr td:nth-child(3) {
        width:90px;
        text-align:center;
    }

     #tblBasket tr td:nth-child(4) {
        width:130px;
        text-align:center;
    }

     #tblBasket tr td:nth-child(5) {
        width:130px;
        text-align:center;
    }

    #tblBasket tr td {
        vertical-align:middle;
    }
&lt;/style&gt;
&lt;script type=&quot;text/javascript&quot;&gt;
    function ShowMiniCart() {
        $(&quot;#divMiniCart&quot;).show();
    }
&lt;/script&gt;

&lt;div id=&quot;divMiniCart&quot; class=&quot;dropdown-menu&quot;&gt;
    &lt;div class=&quot;modal-dialog modal-dialog-minicart&quot; style=&quot;width:70%&quot;&gt;
        &lt;!-- Modal content--&gt;
        &lt;div class=&quot;modal-content modal-content-minicart&quot;&gt;
            &lt;div class=&quot;modal-header modal-content-minicart&quot;&gt;
                &lt;div class=&quot;pull-left&quot;&gt;
                    &lt;h4 class=&quot;modal-title modal-title-minicart&quot;&gt;
                        &lt;img id=&quot;imgShopping&quot; src=&quot;/images/shopping_cart.gif&quot; align=&quot;absmiddle&quot; style=&quot;border-width: 0px;&quot;&gt;
                        Your Cart
                    &lt;/h4&gt;
                &lt;/div&gt;
                &lt;button type=&quot;button&quot; class=&quot;close&quot; data-dismiss=&quot;modal&quot;&gt;&#215;&lt;/button&gt;
            &lt;/div&gt;
            &lt;div class=&quot;modal-body modal-body-minicart&quot;&gt;
                &lt;div class=&quot;row&quot;&gt;
                    &lt;div class=&quot;col-xs-12&quot;&gt;
                        &lt;div class=&quot;table-responsive&quot;&gt;
                            &lt;table class=&quot;table table-bordered table-striped&quot; id=&quot;tblBasket&quot;&gt;
                                &lt;thead&gt;
                                    &lt;tr&gt;
                                        &lt;th&gt;&lt;/th&gt;
                                        &lt;th&gt;Item&lt;/th&gt;
                                        &lt;th&gt;Qty&lt;/th&gt;
                                        &lt;th&gt;Unit Price&lt;/th&gt;
                                        &lt;th&gt;Total&lt;/th&gt;
                                    &lt;/tr&gt;
                                &lt;/thead&gt;
                                &lt;##FOR(ItemClass=YourClassName;AlternatingItemClass=YourClassName;)##&gt;
                                &lt;tr class=&quot;##RowClass##&quot;&gt;
                                    &lt;td&gt;
                                        &lt;img src=&quot;##ItemImage##&quot; alt=&quot;&quot; style=&quot;width:80px;&quot; class=&quot;img-responsive&quot; /&gt;
                                    &lt;/td&gt;
                                    &lt;td&gt;
                                        &lt;div class=&quot;pull-left&quot;&gt;
                                            &lt;a href=&quot;##ItemLink##&quot;&gt;##ItemName##&lt;/a&gt;
                                        &lt;/div&gt;
                                        &lt;div class=&quot;pull-right&quot;&gt;
                                            ##ItemAttributes##
                                        &lt;/div&gt;
                                    &lt;/td&gt;
                                    &lt;td&gt;##ItemUnits##
                                    &lt;/td&gt;
                                    &lt;td&gt;##CurrencySymbol## ##ItemPricePerUnit##
                                    &lt;/td&gt;
                                    &lt;td&gt;##CurrencySymbol## ##TotalAmount##
                                    &lt;/td&gt;
                                &lt;/tr&gt;
                                &lt;##ENDFOR##&gt;
                            &lt;/table&gt;
                        &lt;/div&gt;
                    &lt;/div&gt;

                &lt;/div&gt;
                &lt;div class=&quot;row&quot;&gt;
                    &lt;div class=&quot;col-xs-12 pull-right&quot;&gt;
                        &lt;b&gt;SUBTOTAL: ##CurrencySymbol## ##SubTotal##&lt;/b&gt;
                    &lt;/div&gt;
                &lt;/div&gt;
            &lt;/div&gt;
            &lt;div class=&quot;modal-footer modal-footer-minicart&quot;&gt;
                &lt;a id=&quot;hplCart&quot; class=&quot;btn btn-primary pull-left&quot; href=&quot;/Cart.aspx&quot;&gt;View Cart&lt;/a&gt;
                &lt;a id=&quot;hplCheckout&quot; class=&quot;btn btn-primary pull-right&quot; href=&quot;##CheckOutURL##&quot;&gt;Check Out&lt;/a&gt;
            &lt;/div&gt;
        &lt;/div&gt;
    &lt;/div&gt;
&lt;/div&gt;"   /> <img alt="" src="/images/a10.jpg" />
            </td>
        </tr>
        <tr>
            <td colspan="2">
             <%@ Register src="~/UserControls/SiteMenu.ascx" tagname="Menu" tagprefix="uc1" %> <uc1:Menu ID="Menu5" runat="server"  MenuDirection="Horizontal" Skin="Default" PopulateULLIbasedMenu="True" HtmlCustomize="&lt;!--Remove DefaultMenu , if you are planning to use custom menu--&gt;
{##CustomMenu##}
&lt;div id=&quot;vertSiteMenu&quot;&gt;
    &lt;ul class=&quot;TreeNode&quot;&gt;
        &lt;##FOR##&gt;
        &lt;li&gt;&lt;a href=&quot;##MenuItemLink##&quot;&gt;##MenuItemName##&lt;/a&gt;&lt;/li&gt;
        &lt;##ENDFOR##&gt;
    &lt;/ul&gt;
&lt;/div&gt;
{/##CustomMenu##}"   /> 
            </td>
        </tr>
        <tr>
            <td colspan="2">
             <%@ Register src="~/UserControls/ShippingPromotion.ascx" tagname="ShippingPromotion" tagprefix="uc1" %> <uc1:ShippingPromotion ID="ShippingPromotion8" runat="server"  HtmlCustomize=""   /> &nbsp; <%@ Register src="~/UserControls/OrderPromotion.ascx" tagname="OrderPromotion" tagprefix="uc1" %> <uc1:OrderPromotion ID="OrderPromotion7" runat="server"  HtmlCustomize=""   /> </td>
        </tr>
        <tr>
            <td colspan="2">
             <%@ Register src="~/UserControls/BreadCrumb.ascx" tagname="BreadCrumb" tagprefix="uc1" %> <uc1:BreadCrumb ID="BreadCrumb1" runat="server"    /> 
            </td>
        </tr>
    </tbody>
</table>
            <!--header ends-->
            </td>
        </tr>
        <tr>
            <td valign="top">
            <table width="100%">
                <tbody>
                    <tr>
                        <td valign="top" style="padding-left: 15px;">
                        <!--Content panel starts-->
                         <%@ Register src="~/UserControls/Error.ascx" tagname="Error" tagprefix="uc1" %> <uc1:Error ID="Error2" runat="server"    /> <br />
                        CheckOut<br />
                         <%@ Register src="~/UserControls/GuestCheckOut.ascx" tagname="GuestCheckout" tagprefix="uc1" %> <uc1:GuestCheckout ID="GuestCheckout3" runat="server"  HtmlCustomize="&lt;link rel=&quot;stylesheet&quot; href=&quot;https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css&quot;&gt;
&lt;link href=&quot;https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css&quot; rel=&quot;stylesheet&quot; integrity=&quot;sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN&quot; crossorigin=&quot;anonymous&quot;&gt;
&lt;script src=&quot;https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js&quot;&gt;&lt;/script&gt;
&lt;script src=&quot;https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js&quot;&gt;&lt;/script&gt;
&lt;script src=&quot;../Js/jquery-loader.js&quot;&gt;&lt;/script&gt;
&lt;div id=&quot;UpdateProgress&quot; style=&quot;display: none;&quot; role=&quot;status&quot; aria-hidden=&quot;true&quot;&gt;
    &lt;div class=&quot;overlay&quot;&gt;
        &lt;div class=&quot;overlayContent&quot; style=&quot;color: #000;text-align: center;width: 660px;padding: 20px;background-color: #FFF;&quot;&gt;
            &lt;i class=&quot;fa fa-2x fa-refresh fa-spin&quot;&gt;&lt;/i&gt;
            &lt;h4&gt;Please wait a few seconds while your order is being processed.&lt;/h4&gt;
        &lt;/div&gt;
    &lt;/div&gt;
&lt;/div&gt;
&lt;div class=&quot;row&quot;&gt;
    &lt;div class=&quot;col-xs-12 text-center&quot;&gt;
        &lt;h4&gt;Checkout&lt;/h4&gt;
    &lt;/div&gt;
&lt;/div&gt;
&lt;div class=&quot;sectionheader&quot;&gt;
    Address
&lt;/div&gt;
&lt;div class=&quot;row&quot;&gt;
    &lt;div class=&quot;col-xs-12 col-md-6&quot;&gt;
        &lt;div class=&quot;form-group&quot;&gt;
            &lt;label&gt;Email&lt;span style=&quot;color: red;&quot;&gt;&lt;i class=&quot;fa fa-asterisk&quot;&gt;&lt;/i&gt;&lt;/span&gt;&lt;/label&gt;
            ##Email##
        &lt;/div&gt;
    &lt;/div&gt;
    &lt;div class=&quot;col-xs-12 col-md-6&quot;&gt;
        &lt;div class=&quot;form-group&quot;&gt;
            &lt;label&gt;Phone&lt;span style=&quot;color: red;&quot;&gt;&lt;i class=&quot;fa fa-asterisk&quot;&gt;&lt;/i&gt;&lt;/span&gt;&lt;/label&gt;
            ##Phone##
        &lt;/div&gt;
    &lt;/div&gt;
    &lt;div class=&quot;col-xs-12 col-md-6&quot;&gt;
                        &lt;div class=&quot;form-group&quot;&gt;
                            &lt;label&gt;First Name&lt;span style=&quot;color: red;&quot;&gt;&lt;i class=&quot;fa fa-asterisk&quot;&gt;&lt;/i&gt;&lt;/span&gt;&lt;/label&gt;
                            ##FirstName##
                        &lt;/div&gt;
                     &lt;/div&gt;
    &lt;div class=&quot;col-xs-12 col-md-6&quot;&gt;
                        &lt;div class=&quot;form-group&quot;&gt;
                        &lt;label&gt;Last Name&lt;span style=&quot;color: red;&quot;&gt;&lt;i class=&quot;fa fa-asterisk&quot;&gt;&lt;/i&gt;&lt;/span&gt;&lt;/label&gt;
                        ##LastName##
                        &lt;/div&gt;
                    &lt;/div&gt;
&lt;/div&gt;

&lt;div class=&quot;row&quot;&gt;
    &lt;div class=&quot;col-xs-12 col-md-6&quot;&gt;
        &lt;div class=&quot;panel panel-default&quot;&gt;
            &lt;div class=&quot;panel-heading&quot;&gt;Shipping Address&lt;/div&gt;
            &lt;div class=&quot;panel-body&quot;&gt;
                &lt;div class=&quot;row&quot;&gt;
                    &lt;div class=&quot;col-xs-12&quot;&gt;
                         &lt;div class=&quot;form-group&quot;&gt;
                            &lt;label&gt;Ship To Name&lt;span style=&quot;color: red;&quot;&gt;&lt;i class=&quot;fa fa-asterisk&quot;&gt;&lt;/i&gt;&lt;/span&gt;&lt;/label&gt;
                            ##ShippingAddressName##
                        &lt;/div&gt;
                    &lt;/div&gt;
                &lt;/div&gt;
                &lt;div class=&quot;row&quot;&gt;
                    &lt;div class=&quot;col-xs-12&quot;&gt;
                         &lt;div class=&quot;form-group&quot;&gt;
                            &lt;label&gt;Ship To Street&lt;span style=&quot;color: red;&quot;&gt;&lt;i class=&quot;fa fa-asterisk&quot;&gt;&lt;/i&gt;&lt;/span&gt;&lt;/label&gt;
                            ##ShippingStreet##
                        &lt;/div&gt;
                    &lt;/div&gt;
                &lt;/div&gt;
                &lt;div class=&quot;row&quot;&gt;
                    &lt;div class=&quot;col-xs-12 col-md-6&quot;&gt;
                        &lt;div class=&quot;form-group&quot;&gt;
                            &lt;label&gt;Ship To County&lt;span style=&quot;color: red;&quot;&gt;&lt;i class=&quot;fa fa-asterisk&quot;&gt;&lt;/i&gt;&lt;/span&gt;&lt;/label&gt;
                            ##ShippingCountryDropDown##
                        &lt;/div&gt;
                    &lt;/div&gt;
                    &lt;div class=&quot;col-xs-12 col-md-6&quot;&gt;
                         &lt;div class=&quot;form-group&quot;&gt;
                            &lt;label&gt;Ship To State&lt;span style=&quot;color: red;&quot;&gt;&lt;i class=&quot;fa fa-asterisk&quot;&gt;&lt;/i&gt;&lt;/span&gt;&lt;/label&gt;
                            ##ShippingStateDropDown##
                        &lt;/div&gt;
                    &lt;/div&gt;
                &lt;/div&gt;
                &lt;div class=&quot;row&quot;&gt;
                    &lt;div class=&quot;col-xs-12 col-md-6&quot;&gt;
                         &lt;div class=&quot;form-group&quot;&gt;
                            &lt;label&gt;Ship To City&lt;span style=&quot;color: red;&quot;&gt;&lt;i class=&quot;fa fa-asterisk&quot;&gt;&lt;/i&gt;&lt;/span&gt;&lt;/label&gt;
                            ##ShippingCity##
                        &lt;/div&gt;
                    &lt;/div&gt;
                    &lt;div class=&quot;col-xs-12 col-md-6&quot;&gt;
                        &lt;div class=&quot;form-group&quot;&gt;
                            &lt;label&gt;Ship To Zip Code&lt;span style=&quot;color: red;&quot;&gt;&lt;i class=&quot;fa fa-asterisk&quot;&gt;&lt;/i&gt;&lt;/span&gt;&lt;/label&gt;
                            ##ShippingZipcode##
                        &lt;/div&gt;
                    &lt;/div&gt;
                &lt;/div&gt;
            &lt;/div&gt;
        &lt;/div&gt;
    &lt;/div&gt;
    &lt;div class=&quot;col-xs-12 col-md-6&quot;&gt;
        &lt;div class=&quot;panel panel-default&quot;&gt;
            &lt;div class=&quot;panel-heading&quot;&gt;Billing Address&lt;/div&gt;
            &lt;div class=&quot;panel-body&quot;&gt;
                &lt;div class=&quot;row&quot;&gt;
                    &lt;div class=&quot;col-xs-12 col-md-6&quot;&gt;
                        &lt;div class=&quot;form-group&quot;&gt;
                            &lt;label&gt;&lt;/label&gt;
                            &lt;div class=&quot;CheckBox&quot;&gt;
                                ##SameAsBillingCheckBox##
                                ##IsResidential##
                            &lt;/div&gt;
                        &lt;/div&gt;
                    &lt;/div&gt;
                    &lt;div class=&quot;col-xs-12 col-md-6&quot;&gt;
                        &lt;div class=&quot;form-group&quot;&gt;
                            &lt;label&gt;Bill To Name&lt;span style=&quot;color: red;&quot;&gt;&lt;i class=&quot;fa fa-asterisk&quot;&gt;&lt;/i&gt;&lt;/span&gt;&lt;/label&gt;
                            ##BillingAddressName##
                        &lt;/div&gt;
                    &lt;/div&gt;
                &lt;/div&gt;
                &lt;div class=&quot;row&quot;&gt;
                    &lt;div class=&quot;col-xs-12&quot;&gt;
                         &lt;div class=&quot;form-group&quot;&gt;
                            &lt;label&gt;Bill To Street&lt;span style=&quot;color: red;&quot; class=&quot;requiredField&quot;&gt;&lt;i class=&quot;fa fa-asterisk&quot;&gt;&lt;/i&gt;&lt;/span&gt;&lt;/label&gt;
                            ##BillingStreet##
                        &lt;/div&gt;
                    &lt;/div&gt;
                &lt;/div&gt;
                &lt;div class=&quot;row&quot;&gt;
                    &lt;div class=&quot;col-xs-12 col-md-6&quot;&gt;
                        &lt;div class=&quot;form-group&quot;&gt;
                            &lt;label&gt;Bill To County&lt;span style=&quot;color: red;&quot; class=&quot;requiredField&quot;&gt;&lt;i class=&quot;fa fa-asterisk&quot;&gt;&lt;/i&gt;&lt;/span&gt;&lt;/label&gt;
                            ##BillingCountryDropDown##
                        &lt;/div&gt;
                    &lt;/div&gt;
                    &lt;div class=&quot;col-xs-12 col-md-6&quot;&gt;
                         &lt;div class=&quot;form-group&quot;&gt;
                            &lt;label&gt;Bill To State&lt;span style=&quot;color: red;&quot; class=&quot;requiredField&quot;&gt;&lt;i class=&quot;fa fa-asterisk&quot;&gt;&lt;/i&gt;&lt;/span&gt;&lt;/label&gt;
                            ##BillingStateDropDown##
                        &lt;/div&gt;
                    &lt;/div&gt;
                &lt;/div&gt;
                &lt;div class=&quot;row&quot;&gt;
                    &lt;div class=&quot;col-xs-12 col-md-6&quot;&gt;
                         &lt;div class=&quot;form-group&quot;&gt;
                            &lt;label&gt;Bill To City&lt;span style=&quot;color: red;&quot; class=&quot;requiredField&quot;&gt;&lt;i class=&quot;fa fa-asterisk&quot;&gt;&lt;/i&gt;&lt;/span&gt;&lt;/label&gt;
                            ##BillingCity##
                        &lt;/div&gt;
                    &lt;/div&gt;
                    &lt;div class=&quot;col-xs-12 col-md-6&quot;&gt;
                        &lt;div class=&quot;form-group&quot;&gt;
                            &lt;label&gt;Bill To Zip Code&lt;span style=&quot;color: red;&quot; class=&quot;requiredField&quot;&gt;&lt;i class=&quot;fa fa-asterisk&quot;&gt;&lt;/i&gt;&lt;/span&gt;&lt;/label&gt;
                            ##BillingZipcode##
                        &lt;/div&gt;
                    &lt;/div&gt;
                &lt;/div&gt;
            &lt;/div&gt;
        &lt;/div&gt;
    &lt;/div&gt;
&lt;/div&gt;
&lt;div class=&quot;sectionheader&quot;&gt;
    &lt;h4&gt;Order Summary&lt;/h4&gt;
&lt;/div&gt;
&lt;div class=&quot;row&quot;&gt;
    &lt;div class=&quot;col-xs-12&quot;&gt;
        &lt;table cellspacing=&quot;0&quot; cellpadding=&quot;0&quot; class=&quot;summarytable&quot;&gt;
                    &lt;tr&gt;
                        &lt;td nowrap&gt;
                            &lt;div class=&quot;summarycontent&quot;&gt;
                                &lt;table id=&quot;tblCart&quot; border=&quot;1&quot; cellspacing=&quot;0&quot; class=&quot;Grid&quot; width=&quot;100%&quot;&gt;
                                    &lt;tr class=&quot;GridHeader&quot;&gt;
                                        &lt;td&gt;
                                            Item
                                        &lt;/td&gt;
                                        &lt;td&gt;
                                            Attributes
                                        &lt;/td&gt;
                                        &lt;td&gt;
                                            Units
                                        &lt;/td&gt;
                                        &lt;td&gt;
                                            Price/Unit
                                        &lt;/td&gt;
                                        &lt;td&gt;
                                            Discount
                                        &lt;/td&gt;
                                        &lt;td&gt;
                                            Total
                                        &lt;/td&gt;
                                            &lt;td style=&quot;display:none&quot;&gt;
                                            Item Name
                                        &lt;/td&gt;
                                        &lt;td style=&quot;display:none&quot;&gt;
                                            Model ID
                                        &lt;/td&gt;
                                        &lt;td style=&quot;display:none&quot;&gt;
                                            Item ID
                                        &lt;/td&gt;
                                        &lt;td style=&quot;display:none&quot;&gt;
                                            Price
                                        &lt;/td&gt;
                                        &lt;td&gt;
                                        &lt;/td&gt;
                                    &lt;/tr&gt;
                                    &lt;##FOR(ItemClass=GridItem;AlternatingItemClass=GridAltItem;)##&gt;
                                    &lt;tr class=&quot;##RowClass##&quot;&gt;
                                        &lt;td&gt;
                                            ##ItemName##
                                        &lt;/td&gt;
                                        &lt;td&gt;
                                            ##ItemAttributes##
                                        &lt;/td&gt;
                                        &lt;td&gt;
                                            ##ItemUnits##
                                        &lt;/td&gt;
                                        &lt;td&gt;
                                            ##CurrencySymbol## ##ItemPricePerUnit##
                                        &lt;/td&gt;
                                        &lt;td&gt;
                                            ##CurrencySymbol## ##Discount##
                                        &lt;/td&gt;
                                        &lt;td&gt;
                                            ##CurrencySymbol## ##TotalAmount##
                                        &lt;/td&gt;
                                        &lt;td style=&quot;display:none&quot;&gt;
                                            &lt;a href=&quot;##ItemURL##&quot;&gt;##ItemName##&lt;/a&gt;
                                        &lt;/td&gt;
                                        &lt;td style=&quot;display:none&quot;&gt;
                                            ##ItemModelID##
                                        &lt;/td&gt;
                                        &lt;td style=&quot;display:none&quot;&gt;
                                            ##ItemID##
                                        &lt;/td&gt;
                                        &lt;td style=&quot;display:none&quot;&gt;
                                            ##CurrencySymbol## ##ItemPricePerUnit##
                                        &lt;/td&gt;   
                                    &lt;/tr&gt;
                                    &lt;##ENDFOR##&gt;
                                &lt;/table&gt;
                            &lt;/div&gt;
                        &lt;/td&gt;
                    &lt;/tr&gt;
                    &lt;tr&gt;
                        &lt;td align=&quot;right&quot;&gt;
                            &lt;table align=&quot;right&quot;&gt;
                                &lt;tr&gt;
                                    &lt;td class=&quot;text_bold&quot; align=&quot;right&quot;&gt;
                                        Subtotal: &#160;
                                    &lt;/td&gt;
                                    &lt;td align=&quot;right&quot;&gt;
                                        ##CurrencySymbol## ##SubTotalLabel##
                                    &lt;/td&gt;
                                &lt;/tr&gt;
                                &lt;tr&gt;
                                    &lt;td class=&quot;text_bold&quot; align=&quot;right&quot;&gt;
                                        Discount: &#160;
                                    &lt;/td&gt;
                                    &lt;td align=&quot;right&quot;&gt;
                                        ##CurrencySymbol## ##DiscountLabel##
                                    &lt;/td&gt;
                                &lt;/tr&gt;
                                &lt;tr&gt;
                                    &lt;td class=&quot;text_bold&quot; align=&quot;right&quot;&gt;
                                        Tax: &#160;
                                    &lt;/td&gt;
                                    &lt;td align=&quot;right&quot;&gt;
                                        ##CurrencySymbol## ##TaxLabel##
                                    &lt;/td&gt;
                                &lt;/tr&gt;
                                &lt;tr&gt;
                                    &lt;td class=&quot;text_bold&quot; align=&quot;right&quot;&gt;
                                            ##ShowShippingAmountWithMethod##
                                            ##GetShippingRatesButton##
                                        Shipping Charges: &#160;
                                    &lt;/td&gt;
                                    &lt;td align=&quot;right&quot;&gt;
                                        ##CurrencySymbol## ##ShippingChargeLabel##
                                    &lt;/td&gt;
                                &lt;/tr&gt;
                                &lt;tr&gt;
                                    &lt;td class=&quot;text_bold&quot; align=&quot;right&quot;&gt;
                                        Total: &#160;
                                    &lt;/td&gt;
                                    &lt;td align=&quot;right&quot;&gt;
                                        ##CurrencySymbol## ##TotalAmountLabel##
                                    &lt;/td&gt;
                                &lt;/tr&gt;
                            &lt;/table&gt;
                        &lt;/td&gt;
                    &lt;/tr&gt;
                    &lt;tr&gt;
                        &lt;td nowrap&gt;
                            &lt;div class=&quot;summaryheader&quot;&gt;
                                Payment Information 
                            &lt;/div&gt;
                        &lt;/td&gt;
                    &lt;/tr&gt;
                    &lt;tr&gt;
                        &lt;td nowrap colspan=&quot;2&quot;&gt;
                        &lt;table width=&quot;100%&quot; align=&quot;center&quot; border=&quot;0&quot; cellpadding=&quot;5&quot; cellspacing=&quot;5&quot;&gt;
                            &lt;tr&gt;
                                &lt;td align=&quot;left&quot; colspan=&quot;2&quot;&gt;
                                    &lt;input id=&quot;Checkout_radPay&quot; name=&quot;PayOption&quot; value=&quot;CreditCard&quot; ##IsPayChecked## onclick=&quot;javascript: GetPaymentMethod(&#39;CreditCard&#39;);&quot;
                                        type=&quot;radio&quot;&gt;&lt;label for=&quot;Checkout_radPay&quot;&gt;&#160;&lt;b&gt;Pay by Credit Card&lt;/b&gt;
                                &lt;/td&gt;
                            &lt;/tr&gt;
                            &lt;tr&gt;
                                &lt;td colspan=&quot;2&quot; align=&quot;left&quot; style=&quot;display:block;&quot;&gt;
                                    &lt;div id=&quot;CreditCardSection&quot; style=&quot;display:none;&quot;&gt;
                                            &lt;table id=&quot;Table1&quot; width=&quot;90%&quot;&gt;
                                        &lt;tr&gt;
                                            &lt;td width=&quot;50%&quot; &gt;
                                                &lt;table cellspacing=&quot;2&quot; cellpadding=&quot;5&quot; style=&quot;padding-left:20px;&quot;&gt;
                                                    &lt;tr&gt;
                                                        &lt;th nowrap colspan=&quot;4&quot; align=&quot;left&quot;&gt;
                                                            Credit Card Information
                                                        &lt;/th&gt;
                                                    &lt;/tr&gt;
                                                    &lt;tr&gt;
                                                        &lt;td class=&quot;LabelColumn&quot;&gt;
                                                            Card Type:
                                                        &lt;/td&gt;
                                                        &lt;td class=&quot;ControlCell&quot;&gt;
                                                            ##CardTypeDropDownList##
                                                        &lt;/td&gt;
                                                        &lt;td class=&quot;LabelColumn&quot;&gt;
                                                            Card Number:
                                                        &lt;/td&gt;
                                                        &lt;td class=&quot;ControlCell&quot;&gt;
                                                            ##CardNumberTextBox##
                                                        &lt;/td&gt;
                                                    &lt;/tr&gt;
                                                    &lt;tr&gt;
                                                        &lt;td class=&quot;LabelColumn&quot;&gt;
                                                        Card Holder Name: 
                                                        &lt;/td&gt;
                                                        &lt;td class=&quot;ControlCell&quot;&gt;
                                                            ##CardHolderTextBox##
                                                        &lt;/td&gt;
                                                        &lt;td class=&quot;LabelColumn&quot; &gt;
                                                            CVV2 Data:
                                                        &lt;/td&gt;
                                                        &lt;td class=&quot;ControlCell&quot;&gt;
                                                            ##CVV2TextBox##
                                                        &lt;/td&gt;
                                                    &lt;/tr&gt;
                                                    &lt;tr&gt;
                                                        &lt;td class=&quot;LabelColumn&quot;&gt;
                                                            Exp Date (MM/YY):
                                                        &lt;/td&gt;
                                                        &lt;td class=&quot;ControlCell&quot;&gt;
                                                            ##CardExpMonthDropDownList## &#160;/&#160; ##CardExpYearDropDownList##
                                                        &lt;/td&gt;
                                                        &lt;td nowrap align=&quot;right&quot; &gt;
                                                        &lt;/td&gt;
                                                        &lt;td&gt;
                                                        &lt;/td&gt;
                                                    &lt;/tr&gt;
                                                &lt;/table&gt;
                                            &lt;/td&gt;
                                        &lt;/tr&gt;
                                    &lt;/table&gt;
                                    &lt;/div&gt;
                                &lt;/td&gt;
                            &lt;/tr&gt;
                            &lt;tr&gt;
                                &lt;td align=&quot;left&quot; colspan=&quot;2&quot;&gt;
                                    &lt;input id=&quot;Checkout_radGoogleCheckOut&quot; name=&quot;PayOption&quot; value=&quot;GoogleCheckout&quot; ##IsGoogleCheckoutChecked##
                                        type=&quot;radio&quot; onclick=&quot;javascript: GetPaymentMethod(&#39;GoogleCheckout&#39;);&quot;&gt;&lt;label for=&quot;Checkout_radGoogleCheckOut&quot;&gt;&#160;&lt;b&gt;Google
                                            Checkout&lt;/b&gt; &lt;/label&gt;
                                &lt;/td&gt;
                            &lt;/tr&gt;
                            &lt;tr&gt;
                            &lt;td align=&quot;left&quot; colspan=&quot;2&quot;&gt;
				&lt;input id=&quot;Checkout_radBillMe&quot; name=&quot;PayOption&quot; value=&quot;BillMe&quot; ##IsBillMeChecked## type=&quot;radio&quot; onclick=&quot;javascript: GetPaymentMethod(&#39;BillMe&#39;);&quot; /&gt;
				&lt;label for=&quot;Checkout_radBillMe&quot;&gt;&#160;&lt;b&gt;Bill Me&lt;/b&gt; &#160;(If you or your company has terms with us.)&lt;/label&gt;&lt;br /&gt;
			&lt;/td&gt;
                            &lt;/tr&gt;
                            &lt;tr&gt;
                                &lt;td align=&quot;left&quot; colspan=&quot;2&quot;&gt;
                                    &lt;input id=&quot;Checkout_radPaypalCheckOut&quot; name=&quot;PayOption&quot; value=&quot;PaypalCheckout&quot; ##IsPaypalCheckoutChecked##
                                        type=&quot;radio&quot; onclick=&quot;javascript: GetPaymentMethod(&#39;Paypal&#39;);&quot;&gt;&lt;label for=&quot;Checkout_radPaypalCheckOut&quot;&gt;&#160;&lt;b&gt;Pay by Paypal
                                            &lt;/b&gt; &lt;/label&gt;
                                &lt;/td&gt;
                            &lt;/tr&gt;
                            &lt;tr&gt;
                                &lt;td colspan=&quot;2&quot; align=&quot;left&quot; style=&quot;display:block;&quot;&gt;
                                    &lt;div id=&quot;PaypalSection&quot; style=&quot;display:none;&quot;&gt;
                                            &lt;table id=&quot;tblPaypal&quot; width=&quot;90%&quot;&gt;
                                            &lt;tr&gt;
                                            &lt;td width=&quot;50%&quot; &gt;
                                                &lt;table cellspacing=&quot;2&quot; cellpadding=&quot;5&quot; style=&quot;padding-left:20px;&quot;&gt;
                                                    &lt;tr&gt;
                                                        &lt;th nowrap colspan=&quot;4&quot; align=&quot;left&quot;&gt;
                                                            Pay By Paypal
                                                        &lt;/th&gt;
                                                    &lt;/tr&gt;
                                                    &lt;tr&gt;
                                                        &lt;td class=&quot;LabelColumn&quot;&gt;
                                                            Paypal Email:
                                                        &lt;/td&gt;
                                                        &lt;td class=&quot;ControlCell&quot;&gt;
                                                            ##PaypalEmail##
                                                        &lt;/td&gt;
                                                    &lt;/tr&gt;
                                                &lt;/table&gt;
                                            &lt;/td&gt;
                                        &lt;/tr&gt;
                                            &lt;/table&gt;
                                    &lt;/div&gt;  
                                &lt;/td&gt;
                            &lt;/tr&gt;
                            &lt;tr&gt;
                                &lt;td colspan=&quot;2&quot; align=&quot;left&quot;&gt;
                                    &lt;div id=&quot;tdTotalAmount&quot;&gt;
                                        Total Amount:&lt;b&gt;##CurrencySymbol## ##TotalAmountLabel##&lt;/b&gt;
                                    &lt;/div&gt;
                                &lt;/td&gt;
                            &lt;/tr&gt;
                        &lt;/table&gt;
                        &lt;/td&gt;
                    &lt;/tr&gt;
                    &lt;tr&gt;
                        &lt;td colspan=&quot;2&quot;&gt;
                            &lt;div class=&quot;summarycontent&quot;&gt;
                                &lt;table border=&quot;0&quot; cellpadding=&quot;0&quot; cellspacing=&quot;5&quot;&gt;
                                    &lt;tr&gt;
                                        &lt;td align=&quot;right&quot;&gt;
                                            Total Amount:
                                        &lt;/td&gt;
                                        &lt;td align=&quot;left&quot;&gt;
                                            &lt;b&gt;##CurrencySymbol## ##TotalAmountLabel##&lt;/b&gt;
                                        &lt;/td&gt;
                                    &lt;/tr&gt;
                                    {##EnterCouponSection##}
                                &lt;tr id=&quot;trApplyCouponCode&quot;&gt;
                                    &lt;td class=&quot;text_bold&quot;&gt;
                                        &lt;br /&gt;
                                        Apply Coupon/Discount Code: ##CouponCodeTextBox## ##ApplyCouponCodeButton##
                                    &lt;/td&gt;
                                &lt;/tr&gt;
                                {/##EnterCouponSection##} {##UsedCouponSection##}
                                &lt;tr id=&quot;trUsedCouponCode&quot;&gt;
                                    &lt;td class=&quot;text_bold&quot;&gt;
                                        &lt;br /&gt;
                                        Coupon/Discount Code: ##CouponCodeLabel## ##RemoveCouponCodeLink##
                                    &lt;/td&gt;
                                &lt;/tr&gt;
                                {/##UsedCouponSection##}
                                    &lt;tr&gt;
                                        &lt;td align=&quot;right&quot;&gt;
                                            if you have any Notes regarding order,type them here:
                                        &lt;/td&gt;
                                        &lt;td align=&quot;left&quot;&gt;
                                            ##Comments##
                                        &lt;/td&gt;
                                    &lt;/tr&gt;
                                    &lt;tr&gt;
                                        &lt;td align=&quot;right&quot;&gt;Attach File:&lt;/td&gt;
                                        &lt;td align=&quot;left&quot;&gt;##FileUpload##&lt;/td&gt;
                                    &lt;/tr&gt;
                                    &lt;tr&gt;
                                        &lt;td align=&quot;right&quot;&gt;
                                        &lt;/td&gt;
                                        &lt;td align=&quot;left&quot;&gt;
                                            &lt;input type=&quot;button&quot; name=&quot;btnCharge&quot; class=&quot;button&quot; data-target=&quot;body&quot; onclick=&quot;return Save();&quot; value=&quot;Place Order&quot; /&gt;
                                        &lt;/td&gt;
                                    &lt;/tr&gt;
                                &lt;/table&gt;
                            &lt;/div&gt;
                        &lt;/td&gt;
                    &lt;/tr&gt;
                &lt;/table&gt;
    &lt;/div&gt;
&lt;/div&gt;
&lt;div id=&quot;##ShippingMethodModelID##&quot; class=&quot;modal fade modal-shippingmethod&quot; role=&quot;dialog&quot;&gt;
    &lt;div class=&quot;modal-dialog modal-dialog-shippingmethod&quot; style=&quot;width: 70%&quot;&gt;
        &lt;!-- Modal content--&gt;
        &lt;div class=&quot;modal-content modal-content-shippingmethod&quot;&gt;
            &lt;div class=&quot;modal-header modal-content-shippingmethod&quot;&gt;
                &lt;div class=&quot;pull-left&quot;&gt;
                    Shipping Service(s)
                &lt;/div&gt;
                &lt;div class=&quot;pull-right&quot;&gt;
                    &lt;button type=&quot;button&quot; class=&quot;btn btn-primary&quot; onclick=&quot;ShippingRateChanged();&quot;&gt;Use selected rate instead&lt;/button&gt;
                    &lt;button type=&quot;button&quot; data-dismiss=&quot;modal&quot; class=&quot;btn btn-primary&quot;&gt;Keep my existing rate&lt;/button&gt;
                &lt;/div&gt;
            &lt;/div&gt;
            &lt;div class=&quot;modal-body modal-body-shippingmethod&quot;&gt;
                &lt;div class=&quot;row&quot;&gt;
                    &lt;div class=&quot;col-xs-12&quot;&gt;
                        &lt;div class=&quot;alert alert-info&quot;&gt;
                          Keep your existing shipping rate or replace it with one of the following carriers / services.
                        &lt;/div&gt;
                    &lt;/div&gt;
                    &lt;div class=&quot;col-xs-12&quot; id=&quot;##divShippingRates##&quot;&gt;
                        
                    &lt;/div&gt;
                &lt;/div&gt;
            &lt;/div&gt;
        &lt;/div&gt;
    &lt;/div&gt;
&lt;/div&gt;"   /> <br />
                        </td>
                    </tr>
                </tbody>
            </table>
            </td>
        </tr>
        <tr>
            <td>
            <!--footer starts-->
            <div id="footer">
<a href="#">Temrs &amp; Conditions</a>&nbsp;&nbsp;&nbsp;<a href="#">Newsletters</a>&nbsp;&nbsp;&nbsp;<a href="#">Sitemap</a><br />
&copy; test1234. All rights reserved. Powered by <a href="http://www.BizAutomation.com">
BizAutomation</a>.
</div>
<script src="/Js/jquery-loader.js" type="text/javascript"></script>
            <!--footer ends-->
            </td>
        </tr>
    </tbody>
</table>
</center></url:form> 
</body> 
</html>