﻿<%@ Page Language="C#" Inherits="BizCart.BizPage"  %>
<%@ Register TagPrefix="url" Namespace="Intelligencia.UrlRewriter" Assembly="Intelligencia.UrlRewriter" %>
<!DOCTYPE html>
<script runat="server"> 
</script> 
<html xmlns="http://www.w3.org/1999/xhtml"> 
<head runat="server"><title>File Not Found</title> <meta name="keywords" content=""> <meta name="description" content=""><link rel="stylesheet" type="text/css" href="/PagingDarkStyle.css" /><link rel="stylesheet" type="text/css" href="/default.css" /><link rel="stylesheet" type="text/css" href="/smart_wizard.css" /><link rel="stylesheet" type="text/css" href="/SliderStyle.css" /><script type="text/javascript" src="/Js/Common.js" ></script><script type="text/javascript" src="/Js/jquery-ui.min.js" ></script><script type="text/javascript" src="/Js/jquery.js" ></script><script type="text/javascript" src="/Js/jquery.MetaData.js" ></script><script type="text/javascript" src="/Js/jquery.rating.js" ></script><script type="text/javascript" src="/Js/jquery.jqzoom-core.js" ></script><script type="text/javascript" src="/Js/jquery.ae.image.resize.min.js" ></script><script type="text/javascript" src="/Js/jquery.review.js" ></script><script type="text/javascript" src="/Js/jquery.smartWizard-2.0.js" ></script></head> 
<body> 
    <url:form runat="server" id="form1">
<center>
    <table cellpadding="0" cellspacing="3" class="MainPanel" border="0">
        <tr>
            <td valign="top">
                <table width="100%">
                    <tr>
                        <td valign="top" style="padding-left: 15px;">
                            <!--Content panel starts-->
                            <img src="/images/PageNotFound.jpg" border="0" />
                            <!--Content panel ends-->
                        </td>
                        <td>
                            Ah, the infamous 404 error. We hate to say it, but the page or file you were looking
                            for isn't here. Furthermore, it appears that the link you followed on the previous
                            page is broken.
                            <br />
                            <br />
                            There is good news though; you have options.
                            <ul>
                                <li>send us mail on support@mysite.com and let us know of the broken link,</li>
                                <li>click the <b>Back</b> button in your browser to return to the previous page,</li>
                                <li><a href="/Home.aspx"><b>start over</b></a> by visiting our home page,</li>
                            </ul>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</center>
</url:form> 
</body> 
</html>