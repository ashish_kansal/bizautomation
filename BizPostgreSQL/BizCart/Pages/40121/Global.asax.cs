﻿using System;
using System.Collections;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;
using BACRM.BusinessLogic.ShioppingCart;
using System.Data;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using BACRM.BusinessLogic.Common;

namespace BizCart
{
    public class Global : System.Web.HttpApplication
    {

        protected void Application_Start(object sender, EventArgs e)
        {
           
        }

        protected void Session_Start(object sender, EventArgs e)
        {
            Sites.InitialiseSession();
            //if (Request.Cookies["SiteID"] != null)
            //    Session["SiteID"] = Sites.ToString(Request.Cookies["SiteID"].Value);
            //if (Request.Cookies["DomainID"] != null)
            //    Session["DomainID"] = Sites.ToString(Request.Cookies["DomainID"].Value);
        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {
            //  http://chintan.dyndns.org/MailBee/default.aspx
            //http://chintan.is-a-geek.com/MailBee/Default.aspx
            //string lowercaseURL = (Request.Url.Scheme + "://" + HttpContext.Current.Request.Url.Authority + HttpContext.Current.Request.Url.AbsolutePath);
            //if (Regex.IsMatch(lowercaseURL, @"[A-Z]"))
            //{
            //    lowercaseURL = lowercaseURL.ToLower() + HttpContext.Current.Request.Url.Query;
            //    Response.Clear();
            //    Response.Status = "301 Moved Permanently";
            //    Response.AddHeader("Location", lowercaseURL);
            //    Response.End();
            //}

            Dictionary<string, string> Redirects = new Dictionary<string, string>();
            string ActualURL = "";
            string CompareURL = "";
            ActualURL = HttpContext.Current.Request.Url.AbsoluteUri;
            CompareURL = ActualURL.ToLower();
            long lngSiteID;
            string[] strArr = HttpContext.Current.Request.PhysicalApplicationPath.Split(new string[] { "\\" }, StringSplitOptions.RemoveEmptyEntries);
            lngSiteID = Sites.ToLong(strArr[strArr.Length - 1]);
            DataTable dtRedirects = new DataTable();
            Sites objSites = new Sites();
            objSites.RedirectConfigID = 0;
            objSites.DomainID = 0;
            objSites.SiteID = lngSiteID;
            dtRedirects = objSites.GetRedirectConfig();
            foreach (DataRow dr in dtRedirects.Rows)
            {
                Redirects.Add(CCommon.ToString(dr["vcOldUrl"]).ToLower(), CCommon.ToString(dr["vcNewUrl"]).ToLower());
            }
            if (Redirects.ContainsKey(CompareURL))
            {
                HttpContext.Current.Response.Status = "301 Moved Permanently";
                HttpContext.Current.Response.AddHeader("Location", Request.Url.ToString().ToLower().Replace(ActualURL, Redirects[CompareURL]));
            }


            //string host = string.Empty;
            //host = Request.ServerVariables["HTTP_HOST"].ToString();
            //string path = Request.ServerVariables["PATH_INFO"].ToString();
            //if (Request.Cookies["SiteID"] == null && Request.Cookies["DomainID"] == null)
            //{
            //    if (!path.ToLower().Contains("showpage.aspx"))
            //    {
            //        DataTable dtSites;
            //        if (Application["SiteCollection"] != null)
            //        {
            //            dtSites = (DataTable)Application["SiteCollection"];
            //        }
            //        else
            //        {
            //            dtSites = Sites.GetSitesAll();
            //            Application["SiteCollection"] = dtSites;
            //        }
            //        bool hostExist = false;
            //        foreach (DataRow dr in dtSites.Rows)
            //        {
            //            if (host.ToLower() == dr["vcHostName"].ToString().ToLower())
            //            {
            //                string strResolvedURL = Request.ApplicationPath + "/ShowPage.aspx";//((System.Web.UI.Control)Context.Handler).ResolveUrl("~/ShowPage.aspx");
            //                hostExist = true;
            //                Response.Redirect(strResolvedURL + "?SiteID=" + dr["numSiteID"].ToString() + "&DomainID=" + dr["numDomainID"].ToString() + "&RedirectURL=" + Request.Url.AbsoluteUri, true);
            //            }
            //        }
            //        // When No host matched 
            //        if (hostExist == false)
            //        {
            //            Response.Cookies["SiteID"].Expires = DateTime.Now.AddDays(-1);
            //            Response.Cookies["DomainID"].Expires = DateTime.Now.AddDays(-1);
            //        }
            //    }
            //}
            //else
            //{
            //    HttpContext.Current.RewritePath("~/Pages/" + Request.Cookies["SiteID"].Value + "/" + Request.Url.Segments[Request.Url.Segments.Length - 1]);
            //}
        }


        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {

        }

        protected void Application_Error(object sender, EventArgs e)
        {

        }

        protected void Session_End(object sender, EventArgs e)
        {

        }

        protected void Application_End(object sender, EventArgs e)
        {

        }
    }
}