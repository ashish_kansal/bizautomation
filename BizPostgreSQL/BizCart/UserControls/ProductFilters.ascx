﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProductFilters.ascx.cs" Inherits="BizCart.UserControls.ProductFilters" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>

<script type="text/javascript">
    var objFilters = [];

    $(document).ready(function () {
        LoadProductFiltersTemplate();
    });

    function LoadProductFiltersTemplate() {
        if (parseInt($("[id$=hdnProductFilterCategoryID]").val()) > 0) {
            $.ajax({
                type: "POST",
                url: '/WebServices/CommonService.svc/GetUserControlTemplate',
                contentType: "application/json",
                dataType: "json",
                data: JSON.stringify({
                    "elementID": 38
                    , "elementName": "ProductFilters"
                }),
                success: function (data) {
                    try {
                        var filtersHtml = "";
                        var html = data.GetUserControlTemplateResult;

                        try {
                            var loopTags = html.match("<##FOR##>([\\s\\S]*?)<##ENDFOR##>");
  
                            if (loopTags != null && loopTags.length > 0) {
                                var loopHtml = loopTags[0].replace("<##FOR##>", "").replace("<##ENDFOR##>", "").trim();
                                var tempHtml = "";
                                $.when(GetGetCategoryFilters()).then(function () {
                                    if (objFilters != null && objFilters.length > 0) {
                                        objFilters.forEach(function (objFilter) {
                                            tempHtml = loopHtml;
                                            tempHtml = tempHtml.replace("##FilterLabel##", objFilter.Fld_label.toString());
                                            tempHtml = tempHtml.replace("##FilterControl##", "<select class='product-filters form-control' id='productFilter" + objFilter.Fld_id.toString() + "' placeholder='select " + objFilter.Fld_label.toString().toLowerCase() + "' listID='" + (objFilter.numlistid || 0).toString() + "'></select>");
                                            filtersHtml = filtersHtml + tempHtml;
                                        });
                                    } 

                                    html = html.replace(loopTags[0], filtersHtml);
                                    $("div.divProductFiltersContainer").html(html || "");
                                    $("div.divProductFiltersContainer").show();

                                    if (objFilters == null || objFilters.length == 0) {
                                        $(".btnClearAll").hide();
                                    }

                                    GenerateFiltersAutocomplete();
                                });
                            }
                        } catch (e) {
                            $("div.divProductFiltersContainer").html("");
                            $("div.divProductFiltersContainer").show();
                            $(".btnClearAll").hide();
                        }
                    } catch (err) {
                        alert("Unknown error ocurred while showing product filters template");
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    if (IsJsonString(jqXHR.responseText)) {
                        var objError = $.parseJSON(jqXHR.responseText)
                        alert("Error occurred while fetching product filters template: " + replaceNull(objError.ErrorMessage));
                    } else {
                        alert("Unknown error ocurred while fetching product filters template");
                    }
                }
            });
        }
    }

    function GetGetCategoryFilters() {
        return $.ajax({
            type: "POST",
            url: '/WebServices/CommonService.svc/GetCategoryFilters',
            contentType: "application/json",
            dataType: "json",
            data: JSON.stringify({
                "categoryID": parseInt($("[id$=hdnProductFilterCategoryID]").val())
            }),
            success: function (data) {
                objFilters = $.parseJSON(data.GetCategoryFiltersResult);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                if (IsJsonString(jqXHR.responseText)) {
                    var objError = $.parseJSON(jqXHR.responseText)
                    alert("Error occurred while fetching product filters: " + replaceNull(objError.ErrorMessage));
                } else {
                    alert("Unknown error ocurred while fetching product filters");
                }
            }
        });
    }

    function GenerateFiltersAutocomplete() {
        $("div.divProductFiltersContainer select.product-filters").each(function () {
            try {
                $(this).select2(
                {
                    placeholder: $(this).attr("placeholder"),
                    multiple: true,
                    width: "100%",
                    dropdownCssClass: 'bigdrop',
                    allowClear: true,
                    ajax: {
                        url: '/Js/OrderServices.asmx/SearchMasterListDetails',
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        delay: 250,
                        type: 'POST',
                        data: function (params) {
                            return JSON.stringify({
                                listID: parseInt($(this).attr("listID")),
                                searchText: (params.term || ""),
                                pageIndex: ((params.page || 1) - 1) * 20,
                                pageSize: 20
                            });
                        },
                        processResults: function (data, params) {
                            params.page = params.page || 1;

                            if (data.hasOwnProperty("d")) {
                                if (data.d == "Session Expired") {
                                    alert("Session expired.");
                                } else {
                                    data = $.parseJSON(data.d)
                                }
                            }
                            else
                                data = $.parseJSON(data);

                            return { results: $.parseJSON(data.results), pagination: {
                                more: (params.page * 20) < data.Total
                            }};
                        },
                        error: function (jqXHR, status, error) {
                            if (status != "abort") {
                                alert(error + ": " + jqXHR.responseText);
                            }
                            return {
                                results: [], pagination: {
                                    more: false
                                }
                            }; // Return dataset to load after error
                        }
                    }
                }).on("change", function (e) {
                    UpdateItemList();
                });
            } catch (e) {
                alert("Unknown error ocurred while generating product filters autocomplete");
            }
        });

        if ($("[id$=hdnSelectedProductFilters]").val() != "") {
            var selectedProductFilters = JSON.parse($("[id$=hdnSelectedProductFilters]").val());

            if (selectedProductFilters != null && selectedProductFilters.length > 0) {
                selectedProductFilters.forEach(function (objFilter) {
                    var option = new Option(objFilter.select2Text, objFilter.select2Value, true, true);
                    $("#" + objFilter.ControlID).append(option);//.trigger('change');
                });
            }
        }
    }

    function UpdateItemList() {
        var selectedItems = [];

        $("div.divProductFiltersContainer select.product-filters").each(function () {
            var controlID = $(this).attr("id");
            var fieldID = parseInt($(this).attr("id").replace("productFilter", ""));

            $(this).select2('data').forEach(function (objFilter) {
                var obj = {};
                obj.ControlID = controlID;
                obj.FieldID = fieldID;
                obj.select2Text = objFilter.text;
                obj.select2Value = objFilter.id;
                selectedItems.push(obj);
            });
        });

        //Both controls are in ItemList.ascx
        $("[id$=hdnSelectedProductFilters]").val(JSON.stringify(selectedItems));        
        $("[id$=btnProductFiltersChanged]").click();
    }

    function ClearProductFilters() {
        try {
            $("div.divProductFiltersContainer select.product-filters").each(function () {
                $(this).val(null).trigger('change.select2');
            });

            UpdateItemList();
        } catch (e) {
            alert("Unknown error occurred.");
        }

        return false;
    }

    function IsJsonString(str) {
        try {
            JSON.parse(str);
        } catch (e) {
            return false;
        }
        return true;
    }
</script>


<div class="divProductFiltersContainer">
</div>

<asp:HiddenField ID="hdnProductFilterCategoryID" runat="server" ClientIDMode="Static" />


