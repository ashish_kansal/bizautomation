﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BACRM.BusinessLogic.Item;
using BACRM.BusinessLogic.ShioppingCart;
using System.Data;
using System.Text.RegularExpressions;
using System.Collections;
using System.Text;
using BACRM.BusinessLogic.Common;

namespace BizCart.UserControls
{
    public partial class FeaturedItems : BizUserControl
    {
        #region Global Declaration
        public int TrimNameLength = 0;
        public string TrimDescriptionbyfollowingnoofcharacters { get; set; }
        public string TrimItemNamebyfollowingnoofcharacters { get; set; }
        public string Reports { get; set; }
        public string AfterAddingItemShowCartPage { get; set; }
        #endregion

        #region Events
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
               
                if (!IsPostBack)
                {
                    TrimNameLength = Sites.ToInteger(TrimItemNamebyfollowingnoofcharacters) > 1 ? Sites.ToInteger(TrimItemNamebyfollowingnoofcharacters) : 22;
                    bindHtml();

                }
                if (IsPostBack)
                {
                    if (Sites.ToString(Request["__EVENTTARGET"]) == "AddToCart")
                    {
                        long lngItemCode = Sites.ToLong(Sites.ToString(Request["__EVENTARGUMENT"]).Split(char.Parse("~"))[0]);
                        CCommon objCommon = new CCommon();
                        objCommon.DomainID = Sites.ToLong(Session["DomainID"]);
                        objCommon.Mode = 10;
                        objCommon.Str = Sites.ToString(Request["__EVENTARGUMENT"]).Split(char.Parse("~"))[1];
                        string strCategoryName = Sites.ToString(objCommon.GetSingleFieldValue());

                        string prodid = Request.Form["ddlProfileItem" + lngItemCode.ToString()];
                        long productid = 0;
                        if (Sites.ToString(Request.Form["ddlProfileItem" + lngItemCode.ToString()]) != "")
                        {
                            string[] strContainerItemCode = Convert.ToString(prodid).Split('~');
                            if (strContainerItemCode != null && strContainerItemCode.Length == 2)
                            {
                                productid = Sites.ToInteger(strContainerItemCode[1]);
                            }
                            else
                            {
                                productid = lngItemCode;
                            }
                        }
                        else
                        {
                            productid = lngItemCode;
                        }

                        AddToCartFromEcomm(strCategoryName, productid, 0, "", false);
                        TrimNameLength = Sites.ToInteger(TrimItemNamebyfollowingnoofcharacters) > 1 ? Sites.ToInteger(TrimItemNamebyfollowingnoofcharacters) : 22;
                    }
                    bindHtml();
                }
            }
            catch (Exception ex)
            {
                ExceptionModule.ExceptionPublish(ex, Sites.ToLong(Session["DomainID"]), Sites.ToLong(Session["UserContactID"]), Sites.ToLong(Session["SiteId"]), Request);
                ShowMessage(ex.ToString(), 1);
            }
        }
        #endregion

        #region Methods
        void bindHtml()
        {
            try
            {
                BACRM.BusinessLogic.Item.CItems objItem = new BACRM.BusinessLogic.Item.CItems();
                objItem.DomainID = Sites.ToLong(Session["DomainID"]);
                objItem.numReportId = Sites.ToInteger(Reports); //9;
                objItem.numSiteId = Sites.ToLong(Session["SiteId"]);
                Session["ExchangeRate"] = 1;
                DataTable dtItem = new DataTable();
                dtItem = objItem.GetFeaturedItems();
                string strUI = GetUserControlTemplate("FeaturedItems.htm");
                string pattern = "<##FOR(?<Property>(\\b[^>]*?))##>(?<Content>([\\s\\S]*?))<##ENDFOR##>";

                if (Regex.IsMatch(strUI, pattern))
                {
                    MatchCollection matches = Regex.Matches(strUI, pattern);
                    if ((matches.Count > 0))
                    {
                        Hashtable htProperties = GetPropertiesAsHastable(matches[0].Groups["Property"].Value.Replace("(", "").Replace(")", ""));
                        int RowsCount = 0;
                        string Content = matches[0].Groups["Content"].Value;
                        Sites objSite = new Sites();
                        CItems objItems = new CItems();
                        string Value = string.Empty;
                        if (dtItem.Rows.Count > 0)
                        {

                            StringBuilder sb = new StringBuilder();

                            string strTemp = string.Empty;
                            int j = 0;
                            int columncount = 0;
                            foreach (DataRow dr in dtItem.Rows)
                            {
                                strTemp = Content;

                                if (Sites.ToBool(dr["bitHasChildKits"]))
                                {
                                    strTemp = strTemp.Replace("##ConfigureKitLink##", "<a onclick=\"javascript: document.location.href = '" + ResolveUrl("~/configurator.aspx") + "?ItemID=##ItemID##&CategoryID=##CategoryID##';\" class=\"configurekitlink\">Configure</a>");
                                    strTemp = strTemp.Replace("##AddToCartLink##", "");
                                }
                                else
                                {
                                    strTemp = strTemp.Replace("##ConfigureKitLink##", "");
                                    strTemp = strTemp.Replace("##AddToCartLink##", "<a onclick=\"javascript: __doPostBack('AddToCart', '##ItemID##~##CategoryID##'); \" class=\"addtocart\">Add to Cart</a>");
                                }

                                if (htProperties.ContainsKey("NoOfColumn") && Int32.TryParse(Convert.ToString(htProperties["NoOfColumn"]), out columncount))
                                {
                                    if (columncount > 0)
                                    {
                                        if (j == 0)
                                        {
                                            if (htProperties.ContainsKey("RowClass"))
                                            {
                                                sb.AppendLine("<div class=\"" + CCommon.ToString(htProperties["RowClass"]) + "\">");
                                            }
                                            else
                                            {
                                                sb.AppendLine("<div class=\"row\">");
                                            }
                                        }
                                    }
                                }

                               
                                string url = GetProductURL(dr["vcCategoryName"].ToString(), dr["vcItemName"].ToString(), dr["numItemCode"].ToString());

                                string ItemImagePath = "";
                                if (CCommon.ToString(dr["vcPathForTImage"]) != "")
                                {
                                     ItemImagePath = GetImagePath(CCommon.ToString(dr["vcPathForTImage"]));
                                }
                                else
                                {
                                    ItemImagePath = "/Images/DefaultProduct.png";//GetImagePath("Images/DefaultProduct.png");
                                }
                                
                                strTemp = strTemp.Replace("##ItemLink##", url);
                                strTemp = strTemp.Replace("##ItemName##", dr["vcItemName"].ToString());
                                strTemp = strTemp.Replace("##ItemShortName##", dr["vcItemName"].ToString().Length > TrimNameLength ? dr["vcItemName"].ToString().Substring(0, TrimNameLength) + ".." : dr["vcItemName"].ToString());

                                strTemp = strTemp.Replace("##ItemImagePathName##", ItemImagePath);
                                objItems.byteMode = 2;
                                objItems.ItemCode = Sites.ToInteger(dr["numItemCode"]);

                                CItems objItemsAssembly = new CItems();
                                DataSet ds = new DataSet();
                                objItemsAssembly.ItemCode = Sites.ToInteger(dr["numItemCode"].ToString());
                                objItemsAssembly.DomainID = Sites.ToLong(Session["DomainID"]);
                                ds = objItemsAssembly.GetProfileItems();
                                if (ds.Tables.Count > 0)
                                {
                                    if (ds.Tables[0].Rows.Count > 0)
                                    {
                                        DropDownList ddlItemAttribute = new DropDownList();
                                        ddlItemAttribute.ID = "ddlProfileItem" + dr["numItemCode"].ToString();
                                        ddlItemAttribute.DataSource = ds.Tables[0];
                                        ddlItemAttribute.DataTextField = "vcData";
                                        ddlItemAttribute.DataValueField = "numOppAccAttrID";
                                        ddlItemAttribute.DataBind();
                                        strTemp = strTemp.Replace("##ColorDropDown##", CCommon.RenderControl(ddlItemAttribute));
                                    }
                                    else
                                    {
                                        strTemp = strTemp.Replace("##ColorDropDown##", "");
                                    }
                                }
                                else
                                {
                                    strTemp = strTemp.Replace("##ColorDropDown##", "");
                                }

                                DataTable dt = new DataTable();
                                dt = objItems.ViewItemExtendedDesc();
                                if (dt.Rows.Count > 0)
                                {
                                    strTemp = strTemp.Replace("##LongDescription##", Convert.ToString(dt.Rows[0]["txtDesc"]));
                                    strTemp = strTemp.Replace("##ShortHtmlDescription##", Convert.ToString(dt.Rows[0]["txtShortDesc"]));
                                }
                                else
                                {
                                    strTemp = strTemp.Replace("##LongDescription##", "");
                                    strTemp = strTemp.Replace("##ShortHtmlDescription##", "");
                                }
                                
                                

                                Label ProductPrice = new Label();
                                ProductPrice.CssClass = "prod_price";

                                Label OfferPrice = new Label();
                                Label ProductDesc = new Label();

                                double dProductPrice = 0, dOfferPrice = 0;
                                if (!(dr["monListPrice"] == System.DBNull.Value))
                                {
                                    dProductPrice = Sites.ToDouble(Sites.ToDecimal(dr["monListPrice"]) / Sites.ToDecimal(Session["ExchangeRate"]));
                                    ProductPrice.Text = Sites.ToString(Session["CurrSymbol"]) + " " + string.Format("{0:#,##0.00}", dProductPrice);
                                }
                                //Hide prices for site visitors until they log in as a registered user.
                                //Hide prices for site registered site visitors AFTER they log in as a registered user, with the following Relationship &  Profile
                                if (Sites.ToBool(Session["HidePrice"]) == true)
                                    OfferPrice.Visible = ProductPrice.Visible = false;
                                //if (Sites.ToBool(ShowItemDescription) == true)
                                //{
                                //Trim description
                                ProductDesc.Text = dr["txtItemDesc"].ToString();

                                if (Sites.ToInteger(TrimDescriptionbyfollowingnoofcharacters) > 0 && ProductDesc.Text.Length > Sites.ToInteger(TrimDescriptionbyfollowingnoofcharacters))
                                    ProductDesc.Text = ProductDesc.Text.Substring(0, Sites.ToInteger(TrimDescriptionbyfollowingnoofcharacters)) + "..";
                                //}

                                /*Apply pricebook rule in Item list */
                                if (Sites.ToBool(ApplyPriceBookPrice) == true && Sites.ToBool(Session["HidePrice"]) == false)
                                {
                                    PriceBookRule objPbook = new PriceBookRule();
                                    objPbook.ItemID = Sites.ToLong(dr["numItemCode"]);
                                    objPbook.QntyofItems = Sites.ToInteger(1 * Sites.ToDecimal(dr["UOMConversionFactor"]));
                                    objPbook.DomainID = Sites.ToLong(Session["DomainID"]);
                                    objPbook.DivisionID = Sites.ToLong(Session["DivId"]);
                                    objPbook.WarehouseID = Sites.ToLong(Session["WareHouseID"]);
                                    objPbook.SiteID = Sites.ToLong(HttpContext.Current.Session["SiteId"]);
                                    DataTable dtCalPrice = default(DataTable);
                                    dtCalPrice = objPbook.GetPriceBasedonPriceBook();
                                    if (dtCalPrice.Rows.Count > 0)
                                    {
                                        dOfferPrice = Sites.ToDouble(Sites.ToDecimal(dtCalPrice.Rows[0]["ListPrice"]) * Sites.ToDecimal(dr["UOMConversionFactor"]) / Sites.ToDecimal(Session["ExchangeRate"].ToString()));
                                        OfferPrice.Text = Sites.ToString(Session["CurrSymbol"]) + " " + dOfferPrice.ToString();

                                    }

                                    if (dOfferPrice != dProductPrice && dOfferPrice > 0)
                                    {
                                        OfferPrice.Text = Sites.ToString(Session["CurrSymbol"]) + " " + string.Format("{0:#,##0.00}", dOfferPrice);
                                        ProductPrice.CssClass = "prod_strick";
                                        OfferPrice.CssClass = "prod_price";
                                    }
                                    else
                                    {
                                        OfferPrice.Visible = false;
                                    }
                                }

                                strTemp = strTemp.Replace("##OfferPrice##", CCommon.RenderControl(OfferPrice));
                                strTemp = strTemp.Replace("##ProductPrice##", CCommon.RenderControl(ProductPrice));
                                strTemp = strTemp.Replace("##ItemDesc##", CCommon.RenderControl(ProductDesc));
                                strTemp = strTemp.Replace("##ModelID##", dr["vcModelID"].ToString());
                               
                                if (RowsCount % 2 == 0)
                                    strTemp = strTemp.Replace("##RowClass##", Sites.ToString(htProperties["AlternatingItemClass"]));
                                else
                                    strTemp = strTemp.Replace("##RowClass##", Sites.ToString(htProperties["ItemClass"]));

                                strTemp = strTemp.Replace("##ItemID##", Sites.ToString(dr["numItemCode"]));
                                strTemp = strTemp.Replace("##CategoryID##", Sites.ToString(dr["numCategoryID"]));

                                if (strTemp.Contains("##Quantity##"))
                                {
                                    TextBox txtQuantity = new TextBox();
                                    txtQuantity.ID = "txtQuantity";
                                    txtQuantity.Text = "1";
                                    txtQuantity.CssClass = "textbox";
                                    txtQuantity.Width = 25;

                                    if (Sites.ToBool(Session["HideAddtoCartBeforeLogin"]) == true && Sites.ToLong(Session["UserContactID"]) == 0)
                                    {
                                        strTemp = strTemp.Replace("##Quantity##", "");
                                    }
                                    else
                                    {
                                        strTemp = strTemp.Replace("##Quantity##", CCommon.RenderControl(txtQuantity));
                                    }
                                }
                                if (Sites.ToBool(Session["HideAddtoCartBeforeLogin"]) == true && Sites.ToLong(Session["UserContactID"]) == 0)
                                {
                                    strTemp = strTemp.Replace("##HidePriceBeforeLogin##", "hideAddtoCart");
                                }
                                else
                                {
                                    strTemp = strTemp.Replace("##HidePriceBeforeLogin##", "showAddtoCart");
                                }

                                sb.AppendLine(strTemp);

                                if (htProperties.ContainsKey("NoOfColumn") && Int32.TryParse(Convert.ToString(htProperties["NoOfColumn"]), out columncount))
                                {
                                    if (columncount > 0)
                                    {
                                        if (j == columncount - 1)
                                        {
                                            sb.AppendLine("</div>");
                                            j = 0;
                                        }
                                        else
                                        {
                                            j++;
                                        }
                                    }
                                }

                                RowsCount++;

                            }

                            strUI = Regex.Replace(strUI, pattern, sb.ToString());
                            lblCustomizeHtml.Text = strUI;

                        }

                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Extra Code
        // strTemp = strTemp.Replace("##SKU##", CCommon.ToString(dr["vcSKU"]));
        //strTemp = strTemp.Replace("##Availability##", dr["InStock"].ToString());
        //strTemp = strTemp.Replace("##SKU##", CCommon.ToString(dr["vcSKU"]));
        //strTemp = strTemp.Replace("##Manufacturer##", CCommon.ToString(dr["vcManufacturer"]));
        #endregion
    }
}