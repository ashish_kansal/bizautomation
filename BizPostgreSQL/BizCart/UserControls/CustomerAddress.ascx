﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CustomerAddress.ascx.cs"
    Inherits="BizCart.UserControls.CustomerAddress" ClientIDMode="Static" %>
<asp:Panel ID="pnlCutomizeHtml" runat="server">
    <table border="0" cellpadding="0" cellspacing="5" width="100%">
        <tr>
            <td class="normal4" align="center" colspan="2">
                <asp:Label ID="litMessage" runat="server" EnableViewState="False" ForeColor="Red"></asp:Label>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <div class="sectionheader">
                    <asp:Label ID="lblAddressTitle" runat="server" Text="Billing Address"></asp:Label></div>
            </td>
        </tr>
        <tr id="trAddress" runat="server">
            <td align="right">
                Address
            </td>
            <td>
                <asp:DropDownList runat="server" ID="ddlAddressName" CssClass="signup" AutoPostBack="true"
                    OnSelectedIndexChanged="ddlAddressName_SelectedIndexChanged">
                </asp:DropDownList>
                &nbsp;<asp:LinkButton Text="Delete" ID="lbBillDelete" runat="server" CssClass="signup"
                    OnClick="lbBillDelete_Click" />
            </td>
        </tr>
        <tr>
            <td align="right">
                Alias
            </td>
            <td>
                <asp:TextBox ID="txtAddressName" runat="server" CssClass="signup" TabIndex="1"></asp:TextBox>
                i.e. Home Address
            </td>
        </tr>
        <tr>
            <td align="right">
                Contact Name
            </td>
            <td>
                <asp:TextBox ID="txtContactName" runat="server" CssClass="signup" TabIndex="2"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td align="right">
                Street
            </td>
            <td>
                <asp:TextBox ID="txtStreet" runat="server" CssClass="signup" TextMode="MultiLine"
                    TabIndex="3"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td align="right">
                City
            </td>
            <td>
                <asp:TextBox ID="txtCity" TabIndex="4" runat="server" CssClass="signup"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td align="right">
                State
            </td>
            <td>
                <asp:DropDownList ID="ddlState" runat="server" TabIndex="5" CssClass="signup">
                    <asp:ListItem Value="0">--Select One--</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td align="right">
                Zip Code
            </td>
            <td>
                <asp:TextBox ID="txtPostal" TabIndex="6" runat="server" CssClass="signup"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td align="right">
                Country
            </td>
            <td>
                <asp:DropDownList ID="ddlCountry" TabIndex="7" AutoPostBack="True" runat="server"
                    CssClass="signup" OnSelectedIndexChanged="ddlCountry_SelectedIndexChanged">
                    <asp:ListItem Value="0">--Select One--</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td align="right">
                <label for="chkIsPrimary">
                    Set as default address?</label>
            </td>
            <td>
                <asp:CheckBox Text=""  runat="server" ID="chkIsPrimary" TabIndex="8"  />
            </td>
        </tr>
         <tr>
            <td align="right">
                <label for="chkShippingSameAsBilling">
                    Shipping Address same as Billing Address</label>
            </td>
            <td>
                <asp:CheckBox Text=""  runat="server" ID="chkShippingSameAsBilling" TabIndex="8"  />
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td>
                <asp:Button ID="btnSubmit" runat="server" Text="Save" CssClass="button" OnClick="btnSubmit_Click" />
                <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="button" OnClick="btnCancel_Click" />
            </td>
        </tr>
    </table>
</asp:Panel>
<asp:Literal ID="litCutomizeHtml" runat="server"></asp:Literal>