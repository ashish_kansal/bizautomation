﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BACRM.BusinessLogic.Admin;
using BACRM.BusinessLogic.Common;
using BACRM.BusinessLogic.ShioppingCart;
using System.Data;
using BACRM.BusinessLogic.Documents;
namespace BizCart.UserControls
{
    public partial class ForgotPassword : BizUserControl
    {
        public string FromEmailAddress { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (FromEmailAddress == null)
                {
                    FromEmailAddress = Sites.ToString(Session["DomainAdminEmail"]);
                }
                lblMessage.Text = "";
                txtEmail.Attributes.Add("onkeydown", "if(event.which || event.keyCode){if ((event.which == 13) || (event.keyCode == 13)) {document.getElementById('" + btnSendPassword.ClientID + "').click();return false;}} else {return true}; ");

                if (!IsPostBack)
                {
                    bindHtml();
                }
            }
            catch (Exception ex)
            {
                ExceptionModule.ExceptionPublish(ex, Sites.ToInteger(Session["DomainID"]), Sites.ToInteger(Session["UserContactID"]), Sites.ToInteger(Session["SiteId"]), Request);
                ShowMessage(ex.ToString(), 1);
            }
        }

        void bindHtml()
        {
            try
            {
                string strUI = GetUserControlTemplate("ForgotPassword.htm");

                strUI = strUI.Replace("##Message##", CCommon.RenderControl(lblMessage));
                strUI = strUI.Replace("##Email##", CCommon.RenderControl(txtEmail));
                strUI = strUI.Replace("##SendPassword##", CCommon.RenderControl(btnSendPassword));
                pnlCutomizeHtml.Visible = false;
                litCutomizeHtml.Text = strUI;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void btnSendPassword_Click(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(txtEmail.Text.Trim()))
                {
                    Session["SMTPServerIntegration"] = "True";
                    UserAccess objUserAccess = new UserAccess();
                    objUserAccess.Email = txtEmail.Text.Trim();
                    objUserAccess.boolPortal = true;
                    objUserAccess.DomainID = Sites.ToLong(Session["DomainID"]);
                    objUserAccess.SendPassoword();
                    if (objUserAccess.NoOfRows == 1)
                    {
                        QueryStringValues objEncryption = new QueryStringValues();

                        Email objSendEmail = new Email();

                        objSendEmail.DomainID = Sites.ToLong(Session["DomainID"]);
                        objSendEmail.TemplateCode = "#SYS#ECOMMERCE_FORGOT_PASSWORD";
                        objSendEmail.ModuleID = 1;
                        objSendEmail.RecordIds = Sites.ToString(objUserAccess.ContactID);
                        objSendEmail.AdditionalMergeFields.Add("ContactEcommercepassword", "<a href='" + Request.Url.Scheme + "://" + Request.Url.Authority + "/ResetPassword.aspx?resetId=" + objEncryption.Encrypt(objUserAccess.Password) + "'>Click Here</a>");
                        objSendEmail.AdditionalMergeFields.Add("PortalPassword", "<a href='" + Request.Url.Scheme + "://" + Request.Url.Authority + "/ResetPassword.aspx?resetId=" + objEncryption.Encrypt(objUserAccess.Password) + "'>Click Here</a>");
                        objSendEmail.AdditionalMergeFields.Add("LoggedInUser", "");
                        objSendEmail.FromEmail = Sites.ToString(Session["DomainAdminEmail"]);
                        objSendEmail.ToEmail = txtEmail.Text.Trim();
                        objSendEmail.SendEmailTemplate();

                        //Log was placed to check link
                        //ExceptionPublisher objExceptionPublisher = new ExceptionPublisher();
                        //objExceptionPublisher.DomainID = Sites.ToInteger(Session["DomainID"]);
                        //objExceptionPublisher.Message = String.Join("<br>",Request.Url.Scheme,Request.Url.Authority,("<a href='" + Request.Url.Scheme + "://" + Request.Url.Authority + "/ResetPassword.aspx?resetId=" + objEncryption.Encrypt(objUserAccess.Password) + "'>Click Here</a>"));
                        //objExceptionPublisher.URL = "";
                        //objExceptionPublisher.Referrer = "";
                        //objExceptionPublisher.UserCntID = Sites.ToInteger(Session["UserContactID"]);
                        //objExceptionPublisher.ManageException();

                        lblMessage.Text = "Please check your inbox for an email we just sent to reset password.";
                    }
                    else if (objUserAccess.NoOfRows == 0)
                    {
                        lblMessage.Text = "Check Your Email Id and try Again !";
                    }
                    else if (objUserAccess.NoOfRows > 1)
                    {
                        lblMessage.Text = "Found Multiple Records.Contact Administrator !";
                    }
                }
                else
                {
                    lblMessage.Text = "Please enter email address !";
                }
                
                bindHtml();
            }
            catch (Exception ex)
            {
                ExceptionModule.ExceptionPublish(ex, Sites.ToInteger(Session["DomainID"]), Sites.ToInteger(Session["UserContactID"]), Sites.ToInteger(Session["SiteId"]), Request);
                ShowMessage(ex.ToString(), 1);
            }
        }
    }
}