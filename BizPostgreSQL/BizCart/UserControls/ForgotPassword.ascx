﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ForgotPassword.ascx.cs"
    Inherits="BizCart.UserControls.ForgotPassword" %>
<script type="text/javascript" language="javascript">
    function SendPassword() {
        var txtEmail = document.getElementById('<%= txtEmail.ClientID %>');

        if (txtEmail.value == "") {
            alert("Please enter email address.");
            txtEmail.focus();
            return false;
        }

        return true;
    }
</script>
<asp:Panel ID="pnlCutomizeHtml" runat="server">
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td colspan="2">
                <div class="sectionheader">
                    <asp:Label ID="lblTitle" runat="server" Text="Forgot Password"></asp:Label>
                </div>
            </td>
        </tr>
        <tr>
            <td class="LabelColumn">
                <asp:Label ID="lblEmail" runat="server" Text="Email "></asp:Label>
            </td>
            <td class="ControlCell">
                <asp:TextBox ID="txtEmail" runat="server" CssClass="textbox" autocomplete="off"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td align="left">
                <asp:Button ID="btnSendPassword" runat="server" Text="Reset Password" OnClick="btnSendPassword_Click"
                    CssClass="button" OnClientClick="javascript:return SendPassword();" />
            </td>
        </tr>
    </table>
    <table>
        <tr>
            <td colspan="2">
                <asp:Label ID="lblMessage" runat="server" Text="" Style="color: Red;"></asp:Label>
            </td>
        </tr>
    </table>
</asp:Panel>
<asp:Literal ID="litCutomizeHtml" runat="server"></asp:Literal>