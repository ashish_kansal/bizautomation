﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BACRM.BusinessLogic.Admin;
using BACRM.BusinessLogic.Common;
using BACRM.BusinessLogic.ShioppingCart;
namespace BizCart.UserControls
{
    public partial class ChnagePassword : BizUserControl
    {
        #region Events
        
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (Session["DomainID"] == null || Session["UserEmail"] == null || Session["UserContactID"] == null)
                {
                    Response.Redirect(Session["SitePath"].ToString() + "/Login.aspx");
                }

                bindHtml();
            }
            catch (Exception ex)
            {
                ExceptionModule.ExceptionPublish(ex, Sites.ToInteger(Session["DomainID"]), Sites.ToInteger(Session["UserContactID"]), Sites.ToInteger(Session["SiteId"]), Request);
                ShowMessage(ex.ToString(), 1);
            }
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (Session["DomainID"] != null && Session["UserEmail"] != null && Session["UserContactID"] != null)
                {
                    CCommon objCommon = new CCommon();

                    UserAccess objUserAccess = new UserAccess();
                    objUserAccess.Email = Session["UserEmail"].ToString();
                    objUserAccess.Password = objCommon.Encrypt(txtOldPasword.Text.Trim());
                    objUserAccess.NewPassword = objCommon.Encrypt(txtNewPassword.Text.Trim());
                    objUserAccess.ContactID = Sites.ToLong(Session["UserContactID"]);
                    objUserAccess.DivisionID = Sites.ToLong(Session["DivId"]);
                    objUserAccess.DomainID = Sites.ToLong(Session["DomainID"]);

                    if (objUserAccess.ChangePassword() == 1)
                    {
                        ShowMessage(GetErrorMessage("ERR012"), 0); //Password has been successfully changed 
                    }
                    else
                    {
                        ShowMessage(GetErrorMessage("ERR013"), 1);//You are not authorized to change the password !
                    }
                }
                else
                {
                    Response.Redirect(Session["SitePath"].ToString() + "/Login.aspx");
                }
            }
            catch (Exception ex)
            {
                ExceptionModule.ExceptionPublish(ex, Sites.ToInteger(Session["DomainID"]), Sites.ToInteger(Session["UserContactID"]), Sites.ToInteger(Session["SiteId"]), Request);
                ShowMessage(ex.ToString(), 1);
            }
        }
        
        #endregion

        #region Methods
        
        private void bindHtml()
        {
            try
            {
                string strUI = GetUserControlTemplate("ChangePassword.htm");
                strUI = strUI.Replace("##OldPassword##", CCommon.RenderControl(txtOldPasword));
                strUI = strUI.Replace("##NewPassword##", CCommon.RenderControl(txtNewPassword));
                strUI = strUI.Replace("##NewPasswordConfirm##", CCommon.RenderControl(txtNewPasswordConfirm));
                strUI = strUI.Replace("##SaveButton##", CCommon.RenderControl(btnSave));
                pnlCutomizeHtml.Visible = false;
                litCutomizeHtml.Text = strUI;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        
        #endregion

        #region ExtraCode
        //public string LoginPageUrl { get; set; }
        #endregion
    }
}