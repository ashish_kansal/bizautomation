﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Error.ascx.cs" Inherits="BizCart.UserControls.Error" %>
<script>
    function Show(elm)
    { document.getElementById(elm).style.display = ""; }
    function Hide(elm)
    { document.getElementById(elm).style.display = "none"; }
    function Highlight(elm, Message, Success) {
        var elmnt = document.getElementById(elm);
        var ItemClass = elmnt.className;
        elmnt.focus();
        elmnt.innerHTML = Message;
        Show(elm);
        if (Success == 0) //success
        {
            document.getElementById(elm).className = "WarningMessage";
        }
        if (Success == 1) //failure
        {
            document.getElementById(elm).className = "ErrorMessage";
        }
       
        var offset = { x: 0, y: 0 };
        while (elmnt) {
            offset.x += elmnt.offsetLeft;
            offset.y += elmnt.offsetTop;
            elmnt = elmnt.offsetParent;
        }
        window.scrollTo(0, offset.y - 60);
    }
</script>
<div align="center">
    <div id="MessageBox">
    </div>
</div>
