﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="OrderSummary.ascx.cs" Inherits="BizCart.UserControls.OrderSummary" %>
<%@ Import Namespace="BACRM.BusinessLogic.Common" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>

<style>
    .itemstylePadding {
        padding-left: 5px;
    }

    .headerstyleAlignment {
        text-align: center;
    }

    .gridViewPager td {
        padding-left: 4px;
        padding-right: 4px;
        padding-top: 1px;
        padding-bottom: 2px;
    }

    .Hide {
        display: none;
    }

    .RadWindow .rwTable
    {
        margin: 200px 8px 58px 100px !important; 
    }
    .RadWindow .rwPopupButton
    {
        margin: 60px 8px 58px 100px !important; /* 55px moves the OK button to the left edge of the window, increase or decrease the value to move left or right */
    }
</style>

<script type="text/javascript">
    $(document).ready(function () {
        $('#ddlShippingMethod').bind("change", function () {
            if ($(this).val() == "0~0.00~0" && $("[id$=hdnIsStaticShippingCharge]").val() == "1") {
                    $('#lblShippingCharge').html($("[id$=hdnStaticShippingCharge]").val());
            } else {
                $('#hdnShippingMethod').val($(this).val());
                
                var charge = $(this).val().split('~');
          
                if (charge.length > 1) {
                    $('#lblShippingCharge').html(parseFloat(charge[1]));
                }
                else {
                    $('#lblShippingCharge').html(0);
                }

                if (charge.length >= 4) {
                    GetTax(parseFloat(charge[3]) == 92 ? true : false);
                }
            }
        });

        UpdateTotal();
    });

    function UpdateTotal() {
        var Total;
        var SubTotal, Tax, ShippingCharge, Discount;

        if ($('#lblSubTotal').length > 0) {
            if (parseFloat($('#lblSubTotal').html()) == 'undefined' || parseFloat($('#lblSubTotal').html()) == null || parseFloat($('#lblSubTotal').html()) == '')
                SubTotal = 0;
            else
                SubTotal = parseFloat($('#lblSubTotal').html().replace(',', ''));
        } else {
            SubTotal = 0;
        }

        if ($('#lblTax').length > 0) {
            if (parseFloat($('#lblTax').html()) == 'undefined' || parseFloat($('#lblTax').html()) == null || parseFloat($('#lblTax').html()) == '')
                Tax = 0;
            else
                Tax = parseFloat($('#lblTax').html().replace(',', ''));
        } else {
            Tax = 0;
        }

        if ($('#lblShippingCharge').length > 0) {
            if (parseFloat($('#lblShippingCharge').html()) == 'undefined' || parseFloat($('#lblShippingCharge').html()) == null || parseFloat($('#lblShippingCharge').html()) == '')
                ShippingCharge = 0;
            else
                ShippingCharge = parseFloat($('#lblShippingCharge').html().replace(',', ''));
        } else {
            ShippingCharge = 0;
        }
        
        if ($('#lblDiscount').length > 0) {
            if (parseFloat($('#lblDiscount').html()) == 'undefined' || parseFloat($('#lblDiscount').html()) == null || parseFloat($('#lblDiscount').html()) == '')
                Discount = 0;
            else
                Discount = parseFloat($('#lblDiscount').html().replace(',', ''));
        } else {
            Discount = 0;
        }

        Total = SubTotal + Tax + ShippingCharge - Discount;
        if ($("span#lblTotal").length > 0) {
            $("span#lblTotal").html(Total.toFixed(2));
        }
    }

    function OpenBizInvoice(a, b) {
        //Apply changes in code behind aswell
        window.open('Invoice.aspx?Show=True&OpID=' + a + '&OppBizId=' + b, '', 'toolbar=no,menubar=yes,titlebar=no,left=100,width=850,height=800,scrollbars=yes,resizable=yes');
    }

    function GetPaymentMethod(PaymentMethod) {
        document.getElementById('hdnPaymentMethod').value = PaymentMethod;

        if (PaymentMethod == "BillMe") {
            $('#BillMeSection').css("display", "block");
            $('#CreditCardSection').css("display", "none");
            $('#PaypalSection').css("display", "none");
            $('.stepContainer').css("height", $('#step-3').height() + 12 + 'px');
        }
        else if (PaymentMethod == "CreditCard") {
            $('#BillMeSection').css("display", "none");
            $('#CreditCardSection').css("display", "block");
            $('#PaypalSection').css("display", "none");
            $('.stepContainer').css("height", $('#step-3').height() + 12 + 'px');
        }
        else if (PaymentMethod == "GoogleCheckout") {
            $('#BillMeSection').css("display", "none");
            $('#CreditCardSection').css("display", "none");
            $('#PaypalSection').css("display", "none");
            $('.stepContainer').css("height", $('#step-3').height() + 12 + 'px');
        }
        else if (PaymentMethod == "Paypal") {
            $('#BillMeSection').css("display", "none");
            $('#CreditCardSection').css("display", "none");
            $('#PaypalSection').css("display", "block");
            $('.stepContainer').css("height", $('#step-3').height() + 12 + 'px');
        }
        else if (PaymentMethod == "SalesInquiry") {
            $('#BillMeSection').css("display", "none");
            $('#CreditCardSection').css("display", "none");
            $('#PaypalSection').css("display", "none");
            $('.stepContainer').css("height", $('#step-3').height() + 12 + 'px');
            //$("id*=[btnCharge]").removeClass();
        }

        var biggestHeight = "0";

        //$(".content").each(function () {

        //    if ($(this).height() > biggestHeight)
        //        biggestHeight = $(this).height();
        //});

    }

    function Save() {
        if (document.getElementById('ddlShippingMethod').value == "0~0.00~0" && document.getElementById('hdnIsStaticShippingCharge').value != "1") {
            alert("Select shipping method to place order.")
            document.getElementById('ddlShippingMethod').focus();
            return false;
        }
        return true;
    }
    function CheckNumber(cint, e) {
        var k;
        document.all ? k = e.keyCode : k = e.which;
        if (cint == 1) {
            if (!(k > 47 && k < 58 || k == 44 || k == 46 || k == 8 || k == 37 || k == 39 || k == 16)) {
                if (e.preventDefault) {
                    e.preventDefault();
                }
                else
                    e.returnValue = false;
                return false;
            }
        }
        if (cint == 2) {
            if (!(k > 47 && k < 58 || k == 8 || k == 37 || k == 39 || k == 16)) {
                if (e.preventDefault) {
                    e.preventDefault();
                }
                else
                    e.returnValue = false;
                return false;
            }
        }
    }

    function Mask(obj, evt) {
        evt = (evt) ? evt : window.event
        var charCode = (evt.which) ? evt.which : evt.keyCode
        return true;
    }

    var Page;

    function endRequest(sender, args) {
        if ($("[id$=UpdateProgress]") != null) {
            $("[id$=UpdateProgress]").hide();
        }
    }

    function placeOrder() {
        if (document.getElementById('ddlShippingMethod').value == "0~0.00~0" && document.getElementById('hdnIsStaticShippingCharge').value != "1") {
            alert("Select shipping method to place order.")
            document.getElementById('ddlShippingMethod').focus();
            return false;
        }

        if ($("[id$=UpdateProgress]") != null) {
            Page = Sys.WebForms.PageRequestManager.getInstance();
            Page.add_endRequest(endRequest);
            $("[id$=UpdateProgress]").show();
        }

        return true;
    }

    function ShowRelatedPostSellItems() {
        $("#divRelatedPostSellItems").modal('show');
    }

    function ShowFreePostSellItems() {
        $("#divFreePostSellItems").modal('show');
    }

    function checkAll(objRef) {
        var GridView = objRef.parentNode.parentNode.parentNode;
        var inputList = GridView.getElementsByTagName("input");
        for (var i = 0; i < inputList.length; i++) {
            //Get the Cell To find out ColumnIndex
            var row = inputList[i].parentNode.parentNode;
            if (inputList[i].type == "checkbox" && objRef != inputList[i]) {
                if (objRef.checked) {
                    //If the header checkbox is checked
                    //check all checkboxes
                    //and highlight all rows
                    inputList[i].checked = true;
                }
                else {
                    //If the header checkbox is checked
                    //uncheck all checkboxes
                    inputList[i].checked = false;
                }
            }
        }
    }

    function Confirm() {
        if ($('#<%=gvPromotionPostCheckOut.ClientID%> input:checkbox:checked').length <= 0) {
            alert('Please select atleast one item.');
            return false;
        }
        else {
            return true;
        }
    }

    function GetTax(isWillCall) {
        $.ajax({
            type: "POST",
            url: "/js/OrderServices.asmx/GetTaxOrderSummary",
            data: '{isWillCall:' + isWillCall + '}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                var jsonData = response.d;
                if (jsonData != null && jsonData != 'undefined' && jsonData != "") {
                    $('#lblTax').html(jsonData);
                }

                UpdateTotal();
            },
            error: function () {
                alert("Error ocurred while calculating Tax.");
            }
        });
    }
</script>

<telerik:RadWindowManager RenderMode="Lightweight" ID="RadWindowManager1" runat="server"></telerik:RadWindowManager>  

<div id="divRelatedPostSellItems" class="modal fade modal-minicart" role="dialog">
    <div class="modal-dialog modal-dialog-minicart" style="width: 90%">
        <div class="modal-content modal-content-minicart">
            <div id="dialogSellPopup" runat="server" visible="false">
                <div>
                    <div class="modal-header modal-content-minicart">
                        <%--<div class="col-1-1">--%>
                        <div class="pull-left">
                            <h4 class="modal-title modal-title-minicart" style="font-weight: bold;">Last chance deal 
                            </h4>
                        </div>
                        <%-- <div style="float: left; padding-left: 5px; line-height: 1; display: inline; padding-top: 8px; font-weight: bold;font-size:small;">
                        Last chance deal
                    </div>--%>
                        <%--<div style="float: left; line-height: 1; display: inline; padding-left: 50px;">
                            <img src="../images/Admin-Price-Control.png" height="30" alt="" />
                        </div>--%>
                        <div style="float: left; line-height: 1; display: inline; padding-left: 50px;" id="divPromotion" runat="server" visible="false">
                            <img src="/Images/Admin-Price-Control.png" height="30" alt="" />
                            <asp:Label ID="lblVcPromotion" runat="server"></asp:Label>
                        </div>
                        <%-- <div style="float: left; line-height: 1; display: inline; padding-top: 8px; font-size: small;">
                            Because of the items ordered, you qualify for an additional discount on the following items: 
                        </div>--%>
                        <div style="float: right; padding: 7px;">
                            <asp:Button ID="btnAdd" OnClick="btnAdd_Click" CssClass="button btn btn-primary" Text="Add" runat="server" OnClientClick="return Confirm()"></asp:Button>
                            <asp:Button ID="btnNoThanks" OnClick="btnNoThanks_Click" runat="server" CssClass="button btn btn-primary" Text="No Thanks" />
                            <%--<asp:Button ID="btnFinished" OnClick="btnFinished_Click" runat="server" CssClass="button" Text="Finished adding items to order" />--%>
                        </div>
                        <%-- </div>--%>
                    </div>
                    <div class="modal-body modal-body-minicart">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="table-responsive" style="align-self: center;">
                                    <asp:GridView ID="gvPromotionPostCheckOut" runat="server" AllowPaging="true" DataKeyNames="numItemCode,numParentItemCode" AutoGenerateColumns="false" PagerStyle-Font-Italic="true"
                                        PagerStyle-CssClass="gridViewPager" PagerStyle-Font-Underline="true" PageSize="8" PagerStyle-HorizontalAlign="Right" OnPageIndexChanging="OnPageIndexChanging" Width="98%" CellPadding="3" CellSpacing="3" BorderColor="#f0f0f0" OnRowDataBound="gvPromotionPostCheckOut_RowDataBound">
                                        <RowStyle BorderColor="#CACACA" BorderStyle="Solid" BorderWidth="1" />
                                        <HeaderStyle HorizontalAlign="Center" Font-Bold="true" Font-Size="10" BackColor="#e8e8e8" BorderColor="#CACACA" BorderStyle="Solid" BorderWidth="1" />
                                        <Columns>
                                            <asp:TemplateField>
                                                <%-- <ItemTemplate>
                                                    <%#CCommon.GetImageHTML(Eval("vcPathForTImage"), 1, 100, 56)%>
                                                </ItemTemplate>--%>
                                                <ItemTemplate>
                                                    <asp:Image ID="imgPath" runat="server" ImageUrl='<%# Eval("vcPathForTImage")%>' Width="80" />
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Center" Width="80px" Height="56px" />
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="vcItemName" HeaderText="Item" HeaderStyle-CssClass="headerstyleAlignment" HeaderStyle-Width="100" ItemStyle-CssClass="itemstylePadding" />
                                            <asp:BoundField DataField="txtItemDesc" HeaderText="Description" HeaderStyle-CssClass="headerstyleAlignment" ItemStyle-CssClass="itemstylePadding" />
                                            <asp:BoundField DataField="vcRelationship" HeaderText="Relation to selected item(s)" HeaderStyle-CssClass="headerstyleAlignment" ItemStyle-CssClass="itemstylePadding" HeaderStyle-Width="200" />
                                            <asp:TemplateField HeaderText="Qty" ItemStyle-HorizontalAlign="Center" ItemStyle-Wrap="false" HeaderStyle-CssClass="headerstyleAlignment" ItemStyle-CssClass="itemstylePadding" HeaderStyle-Width="100">
                                                <ItemTemplate>
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <asp:TextBox ID="txtQty" runat="server" Width="50" Text="1"></asp:TextBox></td>
                                                            <td>/ </td>
                                                            <td><%# Eval("vcUOMName")%></td>
                                                        </tr>
                                                    </table>
                                                    <asp:HiddenField ID="hdnWarehouseID" runat="server" Value='<%# Eval("numWareHouseItemID")%>' />
                                                    <asp:HiddenField ID="hdnPrice" runat="server" Value='<%# Eval("monListPrice")%>' />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="monListPrice" HeaderText="Unit Price" ItemStyle-HorizontalAlign="left" HeaderStyle-CssClass="headerstyleAlignment" HeaderStyle-Width="100" ItemStyle-CssClass="itemstylePadding" />
                                            <%--<asp:BoundField DataField="vcProName" HeaderText="Promotion Name" ItemStyle-HorizontalAlign="left" HeaderStyle-Width="200" HeaderStyle-CssClass="headerstyleAlignment" ItemStyle-CssClass="itemstylePadding" />
                                            <asp:BoundField DataField="vcPromotionRule" HeaderText="Promotion Details" ItemStyle-HorizontalAlign="left" HeaderStyle-CssClass="headerstyleAlignment" ItemStyle-CssClass="itemstylePadding" />--%>
                                            <asp:TemplateField HeaderText="Total" ItemStyle-HorizontalAlign="left" ItemStyle-Wrap="false" HeaderStyle-CssClass="headerstyleAlignment" ItemStyle-CssClass="itemstylePadding">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblTotal" runat="server" Width="100" Text="1"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField  ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="headerstyleAlignment">
                                                <HeaderTemplate>
                                                    <asp:CheckBox ID="checkAll" runat="server" Text="Select All" onclick="checkAll(this);" />
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chkAdd" runat="server" />
                                                    <%--<asp:Button ID="btnAdd" CommandName="Add" CssClass="button" Text="Add" runat="server" OnClientClick="Confirm()" CommandArgument='<%# Eval("numItemCode") %>'></asp:Button>--%>
                                                    <asp:Label ID="lblOutOfStock" runat="server" Width="85" Text="Out of stock" Visible="false"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="numOnHand" Visible="false" HeaderStyle-HorizontalAlign="Center" ItemStyle-CssClass="itemstylePadding" />
                                        </Columns>
                                    </asp:GridView>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="divFreePostSellItems" class="modal fade modal-minicart" role="dialog">
    <div class="modal-dialog modal-dialog-minicart" style="width: 50%">
        <div class="modal-content modal-content-minicart">
            <div id="flashPage" runat="server" visible="false">
                <div>
                    <div class="modal-header modal-content-minicart">
                        <div class="text-center">
                            <h4 class="modal-title modal-title-minicart" style="font-weight: bold;">Your order qualifies you to receive the following offer:  
                            </h4>
                        </div>
                    </div>
                    <div class="modal-body modal-body-minicart">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="table-responsive" style="align-self: center;">
                                    <asp:GridView ID="gvItemsForRule" CssClass="table table-bordered table-striped" runat="server" AutoGenerateColumns="false" CellPadding="3" CellSpacing="3" BorderColor="#f0f0f0">
                                        <RowStyle BorderColor="#CACACA" BorderStyle="Solid" BorderWidth="1" />
                                        <HeaderStyle HorizontalAlign="Center" Font-Bold="true" Font-Size="10" BackColor="#e8e8e8" BorderColor="#CACACA" BorderStyle="Solid" BorderWidth="1" />
                                        <Columns>
                                            <asp:BoundField DataField="numItemCode" ItemStyle-CssClass="Hide" HeaderStyle-CssClass="Hide" />
                                            <asp:BoundField DataField="monListPrice" ItemStyle-CssClass="Hide" HeaderStyle-CssClass="Hide" />
                                            <asp:BoundField DataField="vcItemName" HeaderText="Item Name" ItemStyle-Width="600" ItemStyle-Wrap="true" />
                                            <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Units">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblUnit" runat="server" Width="85" Text="1"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Your Price">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblFree" runat="server" Width="85" Text="Free"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer modal-content-minicart">
                        <div style="float: left; line-height: 1; display: inline; padding-top: 8px; font-size: small;">
                            <asp:Button ID="btnYesThanks" OnClick="btnYesThanks_Click" runat="server" CssClass="button btn btn-primary" Text="Yes, add it to my order thanks!" />
                        </div>
                        <div style="float: right; line-height: 1; display: inline; padding-top: 8px; font-size: small;">
                            <asp:Button ID="btnNo" OnClick="btnNo_Click" runat="server" CssClass="button btn btn-primary" Text="Thanks but not this time" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<asp:Panel ID="pnlCutomizeHtml" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <asp:TextBox runat="server" ID="txtCustomerPoNumber" CssClass="textbox" MaxLength="100" />
    <asp:TextBox runat="server" TextMode="MultiLine" ID="txtComments" CssClass="textarea" MaxLength="1500" />

    <asp:DropDownList ID="ddlShippingMethod" ClientIDMode="Static" runat="server">
    </asp:DropDownList>

    <%--<asp:DropDownList ID="ddlShippingMethod" ClientIDMode="Static" runat="server"
        AutoPostBack="True"
        OnSelectedIndexChanged="ddlShippingMethod_SelectedIndexChanged">
    </asp:DropDownList>--%>

    <asp:LinkButton ID="lbtnEdit" Text="Edit" runat="server" OnClick="lbtnEdit_Click"></asp:LinkButton>
    <asp:LinkButton ID="lbtnCustomerInformationEdit" Text="Edit" runat="server" OnClick="lbtnCustomerInformationEdit_Click"></asp:LinkButton>
    <asp:LinkButton ID="lbtnCartEdit" Text="Edit" runat="server" OnClick="lbtnCartEdit_Click"></asp:LinkButton>
    <asp:Button ID="btnCharge" runat="server" CssClass="button" Text="Place Order" OnClientClick="javascript:return placeOrder();" OnClick="btnCharge_Click"></asp:Button>
    <asp:Button ID="btnOrders" runat="server" Visible="false" CssClass="button" Text="View Past Orders" OnClick="btnOrders_Click"></asp:Button>
    <table width="100%" align="center" border="0" cellpadding="5" cellspacing="5">
        <tr>
            <td colspan="2">
                <div class="sectionheader">
                </div>
            </td>
        </tr>
        <tr>
            <td></td>
            <td align="right" class="EditLink">
                <a id="hplEditCart" href="/Cart.aspx">Edit Shopping Cart</a> <a id="hplEditAddres"
                    href="/ConfirmAddress.aspx">Edit Shipping Details</a> <a id="hplEditPayment" href="/AddPayment.aspx">Edit Cards Details</a>
            </td>
        </tr>
        <tr>
            <td align="left" colspan="2">
                <%--<asp:RadioButton ID="radBillMe" runat="server" GroupName="rad" Text='&nbsp;<b>Bill Me</b> &nbsp;(If you or your company has terms with us.)'
                    AutoPostBack="true" Visible="false">
                </asp:RadioButton>--%>
            </td>
        </tr>
        <tr>
            <td colspan="2" align="left">
                <table align="left" runat="server" id="tblBillme" visible="false">
                    <tr>
                        <td class="LabelColumn">Total Balance Due :
                        </td>
                        <td class="ControlCell">
                            <asp:Label ID="lblBalDue" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="LabelColumn">Total Remaining Credit :
                        </td>
                        <td class="ControlCell">
                            <asp:Label ID="lblRemCredit" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="LabelColumn">Total Amount Past Due :
                        </td>
                        <td class="ControlCell">
                            <asp:Label ID="lblAmtPastDue" runat="server"></asp:Label>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>

        <tr>
            <td align="left" colspan="2">

                <%--  <asp:RadioButton ID="radPay" Checked="true" AutoPostBack="true" runat="server" GroupName="rad"
                    Text='&nbsp;<b>Pay by Credit Card</b>'>
                </asp:RadioButton>--%>
            </td>
            <td valign="top">
                <table align="left" width="100%">
                    <tr>
                        <td class="text_bold">
                            <br />
                            Apply Coupon/Discount Code:
                                        <asp:TextBox ID="txtCouponCode" runat="server" CssClass="textbox" MaxLength="20"></asp:TextBox>&nbsp;<asp:Button
                                            ID="btnCouponCode" runat="server" Text="Apply" CssClass="button" OnClick="btnCouponCode_Click" />
                        </td>
                    </tr>
                    <tr>
                        <td class="text_bold">
                            <br />
                            Coupon/Discount Code:
                                        <asp:Label ID="lblCouponCode" runat="server"></asp:Label>&nbsp;<asp:LinkButton ID="lbCouponCodeRemove"
                                            runat="server" OnClick="lbCouponCodeRemove_Click">Remove</asp:LinkButton>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:DropDownList ID="ddlShipVia" runat="server"></asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td colspan="2" align="left">
                <table id="tblCustomerInfo" width="90%" runat="server">
                    <tr>
                        <td width="50%">
                            <table cellspacing="2" cellpadding="5">
                                <tr>
                                    <th nowrap colspan="4">Credit Card Information
                                    </th>
                                </tr>
                                <tr>
                                    <td nowrap align="right">Card Type:
                                    </td>
                                    <td>
                                        <asp:DropDownList runat="server" ID="ddlCardType" CssClass="dropdown">
                                            <%--   <asp:ListItem Value="1">Visa </asp:ListItem>
                                        <asp:ListItem Value="2">MasterCard</asp:ListItem>
                                        <asp:ListItem Value="3">AMEX</asp:ListItem>
                                        <asp:ListItem Value="4">Discover</asp:ListItem>
                                        <asp:ListItem Value="5">Diners </asp:ListItem>
                                        <asp:ListItem Value="6">JCB</asp:ListItem>
                                        <asp:ListItem Value="7">BankCard</asp:ListItem>--%>
                                        </asp:DropDownList>
                                    </td>
                                    <td nowrap align="right">Card Number:
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtCardNumber" AutoCompleteType="None" CssClass="textbox" runat="server" Width="180px" MaxLength="16"></asp:TextBox>

                                    </td>
                                </tr>
                                <tr>
                                    <td nowrap align="right">Card Holder Name:
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtCHName" Width="180" runat="server" CssClass="signup" MaxLength="100"></asp:TextBox>
                                    </td>
                                    <td nowrap align="right">CVV Data:
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtCardCVV2" CssClass="textbox" runat="server" Width="117px"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" nowrap>Exp Date (MM/YY):
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddlCardExpMonth" CssClass="dropdown" runat="server">
                                            <asp:ListItem Value="01">01</asp:ListItem>
                                            <asp:ListItem Value="02">02</asp:ListItem>
                                            <asp:ListItem Value="03">03</asp:ListItem>
                                            <asp:ListItem Value="04">04</asp:ListItem>
                                            <asp:ListItem Value="05">05</asp:ListItem>
                                            <asp:ListItem Value="06">06</asp:ListItem>
                                            <asp:ListItem Value="07">07</asp:ListItem>
                                            <asp:ListItem Value="08">08</asp:ListItem>
                                            <asp:ListItem Value="09">09</asp:ListItem>
                                            <asp:ListItem Value="10">10</asp:ListItem>
                                            <asp:ListItem Value="11">11</asp:ListItem>
                                            <asp:ListItem Value="12">12</asp:ListItem>
                                        </asp:DropDownList>
                                        &nbsp;/&nbsp;
                                        <asp:DropDownList ID="ddlCardExpYear" CssClass="dropdown" runat="server">
                                        </asp:DropDownList>
                                    </td>
                                    <td align="right" nowrap>&nbsp;
                                    </td>
                                    <td>&nbsp;
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="left" colspan="2"></td>
        </tr>
        <tr>
            <td colspan="2" align="left">
                <table id="tblGoogleCheckout" runat="server" width="90%">
                    <tr>
                        <td width="50%">Google Checkout
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="left" colspan="2"></td>
        </tr>
        <tr>
            <td colspan="2" align="left">
                <table id="tblPayPal" runat="server" width="90%">
                    <tr>
                        <td width="50%">PayPal
                        </td>
                    </tr>
                    <tr>

                        <td nowrap align="right">PayPal Email :
                        </td>
                        <td>
                            <asp:TextBox ID="txtPayPalEmail" AutoCompleteType="None" CssClass="textbox" runat="server" Width="180px"></asp:TextBox>

                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="left" colspan="2"></td>
        </tr>
        <tr>
            <td colspan="2" align="left">
                <table id="tblSalesInquiry" runat="server" width="90%">
                    <tr>
                        <td width="50%">Sales Inquiry
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="2" align="left">
                <div id="tdTotalAmount">
                    Total Amount:
                </div>

            </td>
        </tr>
        <tr>
            <td></td>
            <td align="left"></td>
        </tr>

    </table>

    <asp:Label runat="server" ID="lblSubTotal" ClientIDMode="Static" />
    <asp:Label runat="server" ID="lblDiscount" ClientIDMode="Static" />
    <asp:Label runat="server" ID="lblTax" ClientIDMode="Static" />
    <asp:Label runat="server" ID="lblShippingCharge" ClientIDMode="Static" />
    <asp:Label runat="server" ID="lblTotal" ClientIDMode="Static" />
    <asp:Label runat="server" ID="lblCurrencySymbol" ClientIDMode="Static" />

    <asp:PlaceHolder ID="plhFormControls" runat="server"></asp:PlaceHolder>
    <asp:Literal runat="server" ID="litApplyCouponCode" />
    <asp:Literal runat="server" ID="litUsedCouponCode" Visible="false" />
</asp:Panel>
<asp:HiddenField ID="hdnShippingMethod" Value="" ClientIDMode="Static" runat="server" />
<asp:HiddenField ID="hdnCardNumber" Value="" ClientIDMode="Static" runat="server" />
<asp:HiddenField ID="hdnPaymentMethod" Value="" ClientIDMode="Static" runat="server" />
<asp:Literal ID="litCutomizeHtml" runat="server"></asp:Literal>
<asp:TextBox ID="txtCredit" runat="server" Style="display: none"></asp:TextBox>
<asp:TextBox ID="txtDueAmount" runat="server" Style="display: none"></asp:TextBox>
<asp:HiddenField ID="hdnDiscountShipping" runat="server" ClientIDMode="Static" />
<asp:HiddenField ID="hdnCategory" runat="server" />
<asp:HiddenField ID="hdnIsFinishedPostCheckout" runat="server" />
<asp:HiddenField ID="hdnIsStaticShippingCharge" runat="server" />
<asp:HiddenField ID="hdnStaticShippingCharge" runat="server" />
<asp:HiddenField ID="hdnOrderPromotionID" runat="server" />
<asp:HiddenField ID="hdnDiscountCodeID" runat="server" />
<asp:TextBox ID="hdnSQty" CssClass="hdnSQty" Style="display: none;" value="1" runat="server"></asp:TextBox>
<span style="display: none">
    <asp:FileUpload ID="FluDocument" runat="server" />
</span>