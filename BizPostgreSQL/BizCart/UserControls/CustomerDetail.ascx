﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CustomerDetail.ascx.cs"
    Inherits="BizCart.UserControls.CustomerDetail" ClientIDMode="Static" %>
<asp:Panel ID="pnlCutomizeHtml" runat="server">
    <table border="0" width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td colspan="4">
                <div class="sectionheader">
                </div>
            </td>
        </tr>
        <tr id="trEditAddress" runat="server">
            <td colspan="4">
                <table border="0" width="100%" cellpadding="0" cellspacing="0">
                    <tr>
                        <td class="LabelColumn">Billing Address:
                        </td>
                        <td class="ControlCell">
                            <asp:DropDownList runat="server" ID="ddlBillAddressName" CssClass="signup" AutoPostBack="true"
                                OnSelectedIndexChanged="ddlBillAddressName_SelectedIndexChanged">
                            </asp:DropDownList>
                            &nbsp;<asp:LinkButton Text="Add New" ID="lbBillAddNew" runat="server" CssClass="signup"
                                OnClick="lbBillAddNew_Click" />
                        </td>
                        <td class="LabelColumn">Shipping Address:
                        </td>
                        <td class="ControlCell">
                            <asp:DropDownList runat="server" ID="ddlShipAddressName" CssClass="signup" AutoPostBack="true"
                                OnSelectedIndexChanged="ddlShipAddressName_SelectedIndexChanged">
                            </asp:DropDownList>
                            &nbsp;<asp:LinkButton Text="Add New" ID="lbShipAddNew" runat="server" CssClass="signup"
                                OnClick="lbShipAddNew_Click" />
                        </td>
                    </tr>
                    <tr>
                        <td class="LabelColumn" valign="top">Address
                            <asp:LinkButton Text="(Edit)" ID="lbBillEdit" runat="server" CssClass="signup" Visible="false"
                                OnClick="lbBillEdit_Click" />:
                        </td>
                        <td class="ControlCell">
                            <asp:Label ID="lblBillAddress" runat="server"></asp:Label>
                        </td>
                        <td class="LabelColumn" valign="top">Address:
                            <asp:LinkButton Text="(Edit)" ID="lbShipEdit" runat="server" CssClass="signup" Visible="false"
                                OnClick="lbShipEdit_Click" />:
                        </td>
                        <td class="ControlCell">
                            <asp:Label ID="lblShipAddress" runat="server"></asp:Label>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr id="trFirstTime" runat="server">
            <td colspan="4">
                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <th colspan="2">
                            <h3>Billing Address:</h3>
                        </th>
                        <th colspan="2">
                            <h3>Shipping Address:</h3>
                        </th>
                    </tr>
                    <tr>
                        <td colspan="3"></td>
                        <td>
                            <asp:CheckBox ID="chkShippingAdd" runat="server" Text=" Same as Billing Address" onclick="javascript:__doPostBack('chkShippingAdd',this.checked)" />
                        </td>
                    </tr>
                    <tr>
                        <td class="LabelColumn">Street:
                        </td>
                        <td class="ControlCell">
                            <asp:TextBox ID="txtBillStreet" CssClass="textbox" runat="server" Width="181px"></asp:TextBox>
                        </td>
                        <td class="LabelColumn">Street:
                        </td>
                        <td class="ControlCell">
                            <asp:TextBox ID="txtShipStreet" CssClass="textbox" runat="server" Width="181px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="LabelColumn">City:
                        </td>
                        <td class="ControlCell">
                            <asp:TextBox ID="txtBillCity" CssClass="textbox" runat="server" Width="181px"></asp:TextBox>
                        </td>
                        <td class="LabelColumn">City:
                        </td>
                        <td class="ControlCell">
                            <asp:TextBox ID="txtShipCity" CssClass="textbox" runat="server" Width="181px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="LabelColumn">Zip Code:
                        </td>
                        <td class="ControlCell">
                            <asp:TextBox ID="txtBillCode" CssClass="textbox" runat="server" Width="182px"></asp:TextBox>
                        </td>
                        <td class="LabelColumn">Zip Code:
                        </td>
                        <td class="ControlCell">
                            <asp:TextBox ID="txtShipCode" CssClass="textbox" runat="server" Width="182px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="LabelColumn">Country:
                        </td>
                        <td class="ControlCell">
                            <asp:DropDownList runat="server" ID="ddlBillCountry" AutoPostBack="true" CssClass="dropdown"
                                OnSelectedIndexChanged="ddlBillCountry_SelectedIndexChanged">
                            </asp:DropDownList>
                        </td>
                        <td class="LabelColumn">Country:
                        </td>
                        <td class="ControlCell">
                            <asp:DropDownList runat="server" ID="ddlShipCountry" AutoPostBack="true" CssClass="dropdown"
                                OnSelectedIndexChanged="ddlShipCountry_SelectedIndexChanged">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="LabelColumn">State:
                        </td>
                        <td class="ControlCell">
                            <asp:DropDownList runat="server" ID="ddlBillState" CssClass="dropdown">
                            </asp:DropDownList>
                        </td>
                        <td class="LabelColumn">State:
                        </td>
                        <td class="ControlCell">
                            <asp:DropDownList runat="server" ID="ddlShipState" CssClass="dropdown">
                            </asp:DropDownList>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="4" align="center">
                <asp:Button ID="btnSubmit" runat="server" Text="Confirm & Save" CssClass="button"
                    OnClick="btnSubmit_Click" />
            </td>
        </tr>
    </table>
</asp:Panel>
<asp:Literal ID="litCutomizeHtml" runat="server"></asp:Literal>
<asp:HiddenField ID="hfFirstTime" runat="server" />
<asp:HiddenField ID="hfEditAddress" runat="server" />
