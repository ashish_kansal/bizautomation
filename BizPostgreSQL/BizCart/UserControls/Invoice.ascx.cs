﻿using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using BACRM.BusinessLogic.Common;
using BACRM.BusinessLogic.Admin;
using BACRM.BusinessLogic.Opportunities;
using BACRM.BusinessLogic.Documents;
using BACRM.BusinessLogic.Alerts;
using BACRM.BusinessLogic.ShioppingCart;
using System.Web.UI.HtmlControls;
using Microsoft.VisualBasic;
namespace BizCart.UserControls
{
    public partial class Invoice : BizUserControl
    {
        #region Global Declaration
        DataTable dtOppBiDocItems = new DataTable();
        long sintRefType, lngReferenceID, lngBizDocId, lngOppId;
        CCommon objCommon = new CCommon();
        string InvoiceType = "";
        #endregion

        #region Events
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                lngReferenceID = CCommon.ToLong(Request.QueryString["OppID"]);
                sintRefType = CCommon.ToLong(Request.QueryString["RefType"]);
                InvoiceType = CCommon.ToString(Request.QueryString["InvoiceType"]);
                lngBizDocId = CCommon.ToLong(Request.QueryString["BizDocId"]);
                lngOppId = CCommon.ToLong(Request.QueryString["OppID"]);
                bool isForPrint = CCommon.ToBool(Request.QueryString["Print"]);

                //SetWebControlAttributes()
                if (!IsPostBack)
                {
                    btnClose.Attributes.Add("onclick", "return Close()");
                    btnPrint.Attributes.Add("onclick", "return PrintIt('" + tblButtons.ClientID + "');");

                    if (!isForPrint)
                    {
                        tblButtons.Visible = false;
                    }


                    getAddDetails();
                    getBizDocDetails();
                }
            }
            catch (Exception ex)
            {
                ExceptionModule.ExceptionPublish(ex, Sites.ToLong(Session["DomainID"]), Sites.ToLong(Session["UserContactID"]), Sites.ToLong(Session["SiteId"]), Request);
                ShowMessage(ex.ToString(), 1);
            }
        }
        #endregion

        #region Methods
        private void getAddDetails()
        {
            try
            {
                DataTable dtOppBizAddDtl = default(DataTable);
                OppBizDocs objOppBizDocs = new OppBizDocs();
                objOppBizDocs.OppBizDocId = lngBizDocId;
                objOppBizDocs.OppId = lngOppId;
                objOppBizDocs.DomainID = CCommon.ToLong(Session["DomainID"]);
                dtOppBizAddDtl = objOppBizDocs.GetOppAddressDetail();
                if (dtOppBizAddDtl.Rows.Count != 0)
                {
                    //lblAddress.Text = IIf(IsDBNull(dtOppBizAddDtl.Rows(0).Item("CompName")), "", dtOppBizAddDtl.Rows(0).Item("CompName"))
                    lblBillTo.Text = CCommon.ToString(dtOppBizAddDtl.Rows[0]["BillAdd"]);
                    lblShipTo.Text = CCommon.ToString(dtOppBizAddDtl.Rows[0]["ShipAdd"]);

                    lblBillToAddressName.Text = CCommon.ToString(dtOppBizAddDtl.Rows[0]["BillToAddressName"]);
                    lblShipToAddressName.Text = CCommon.ToString(dtOppBizAddDtl.Rows[0]["ShipToAddressName"]);

                    lblBillToCompanyName.Text = CCommon.ToString(dtOppBizAddDtl.Rows[0]["BillToCompanyName"]);
                    lblShipToCompanyName.Text = CCommon.ToString(dtOppBizAddDtl.Rows[0]["ShipToCompanyName"]);

                    hplOppID.Text = CCommon.ToString(dtOppBizAddDtl.Rows[0]["OppName"]);
                    lblBizDoc.Text = CCommon.ToString(dtOppBizAddDtl.Rows[0]["BizDcocName"]);
                    lblBizDocIDLabel.Text = lblBizDoc.Text + "#";
                    txtBizDoc.Text = CCommon.ToString(dtOppBizAddDtl.Rows[0]["BizDoc"]);
                    txtConEmail.Text = CCommon.ToString(dtOppBizAddDtl.Rows[0]["vcEmail"]);
                    txtOppOwner.Text = CCommon.ToString(dtOppBizAddDtl.Rows[0]["Owner"]);
                    txtCompName.Text = CCommon.ToString(dtOppBizAddDtl.Rows[0]["CompName"]);
                    txtConID.Text = CCommon.ToString(dtOppBizAddDtl.Rows[0]["numContactID"]);
                    txtBizDocRecOwner.Text = CCommon.ToString(dtOppBizAddDtl.Rows[0]["BizDocOwner"]);

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        private void getBizDocDetails()
        {
            try
            {
                DataTable dtOppBiDocDtl = default(DataTable);
                DataTable dtOppBizAddDtl = default(DataTable);
                OppBizDocs objOppBizDocs = new OppBizDocs();
                objOppBizDocs.ReferenceType = CCommon.ToShort(sintRefType);
                objOppBizDocs.ReferenceID = lngReferenceID;
                objOppBizDocs.DomainID = CCommon.ToLong(Session["DomainID"]);
                objOppBizDocs.UserCntID = CCommon.ToLong(Session["UserContactID"]);
                objOppBizDocs.ClientTimeZoneOffset = CCommon.ToInteger(Session["ClientMachineUTCTimeOffset"]);

                DataSet ds1 = new DataSet();
                if (InvoiceType == "1")
                {
                    objOppBizDocs.ReferenceID = lngBizDocId;
                    objOppBizDocs.OppBizDocId = lngBizDocId;
                    objOppBizDocs.OppId = lngOppId;
                }
                else
                {
                }
                ds1 = objOppBizDocs.GetMirrorBizDocDtls();
                if (InvoiceType != "1")
                {
                    dtOppBiDocDtl = ds1.Tables[0];
                }
                else
                {
                    dtOppBiDocDtl = objOppBizDocs.GetOppBizDocDtl();
                }
                objOppBizDocs.ReferenceID = lngBizDocId;
                objOppBizDocs.OppBizDocId = lngBizDocId;
                objOppBizDocs.OppId = lngOppId;
                dtOppBizAddDtl = objOppBizDocs.GetOppAddressDetail();

                if (dtOppBiDocDtl.Rows.Count == 0)
                    return;

                hplOppID.Text = CCommon.ToString(dtOppBizAddDtl.Rows[0]["OppName"]);
                if (sintRefType == 11)
                {
                    sintRefType = 1;
                }
                lblBizDoc.Text = CCommon.ToString(dtOppBizAddDtl.Rows[0]["BizDcocName"]);
                lblBizDocIDLabel.Text = lblBizDoc.Text + "#";
                txtBizDoc.Text = CCommon.ToString(dtOppBizAddDtl.Rows[0]["BizDoc"]);
                txtOppOwner.Text = CCommon.ToString(dtOppBizAddDtl.Rows[0]["Owner"]);
                txtCompName.Text = CCommon.ToString(dtOppBizAddDtl.Rows[0]["CompName"]);
                txtConID.Text = CCommon.ToString(dtOppBizAddDtl.Rows[0]["numContactID"]);
                txtBizDocRecOwner.Text = CCommon.ToString(dtOppBizAddDtl.Rows[0]["BizDocOwner"]);
                txtConEmail.Text = CCommon.ToString(dtOppBizAddDtl.Rows[0]["vcEmail"]);

                //lblRefOrderNo.Text = IIf(IsDBNull(dtOppBiDocDtl.Rows[0]["vcRefOrderNo")), "", dtOppBiDocDtl.Rows[0]["vcRefOrderNo"))
                if (!Convert.IsDBNull(dtOppBiDocDtl.Rows[0]["monAmountPaid"]))
                {
                    lblAmountPaid.Text = String.Format("{0:#,##0.00}", dtOppBiDocDtl.Rows[0]["monAmountPaid"]);
                }
                else
                {
                    lblAmountPaid.Text = "0.00";
                }

                if (CCommon.ToInteger(dtOppBiDocDtl.Rows[0]["tintDeferred"]) == 1)
                {
                    hplAmountPaid.Text = "Amount Paid: (Deferred)";
                }

                if (CCommon.ToInteger(dtOppBiDocDtl.Rows[0]["tintOppType"]) == 1)
                {
                    if (!Convert.IsDBNull(dtOppBiDocDtl.Rows[0]["BizdocFooter"]))
                    {
                        imgFooter.Visible = true;
                        imgFooter.ImageUrl = "/PortalDocs/" + Session["DomainID"] + "/" + dtOppBiDocDtl.Rows[0]["BizdocFooter"];
                    }
                    else
                    {
                        imgFooter.Visible = false;
                    }
                    lblPONo.Text = "P.O";

                }
                else
                {
                    if (!Convert.IsDBNull(dtOppBiDocDtl.Rows[0]["PurBizdocFooter"]))
                    {
                        imgFooter.Visible = true;
                        imgFooter.ImageUrl = CCommon.GetDocumentPath(CCommon.ToLong(Session["DomainID"])) + dtOppBiDocDtl.Rows[0]["PurBizdocFooter"];
                    }
                    else
                    {
                        imgFooter.Visible = false;
                    }
                    lblPONo.Text = "Invoice";

                    hplAmountPaid.Attributes.Add("onclick", "#");
                }

                if (!Convert.IsDBNull(dtOppBiDocDtl.Rows[0]["vcBizDocImagePath"]))
                {
                    imgLogo.Visible = true;
                    imgLogo.ImageUrl = "/PortalDocs/" + Session["DomainID"] + "/" + dtOppBiDocDtl.Rows[0]["vcBizDocImagePath"];
                }
                else
                {
                    imgLogo.Visible = false;
                }

                lblBizDocIDValue.Text = (Convert.IsDBNull(dtOppBiDocDtl.Rows[0]["vcBizDocID"]) ? "" : CCommon.ToString(dtOppBiDocDtl.Rows[0]["vcBizDocID"]));
                //lblcreated.Text = (Convert.IsDBNull(dtOppBiDocDtl.Rows[0]["numCreatedby"]) ? "" : dtOppBiDocDtl.Rows[0]["numCreatedby"]) + "  &nbsp;" + (Convert.IsDBNull(dtOppBiDocDtl.Rows[0]["dtCreatedDate"]) ? "" : dtOppBiDocDtl.Rows[0]["dtCreatedDate"]);
                //lblModifiedby.Text = (Convert.IsDBNull(dtOppBiDocDtl.Rows[0]["numModifiedBy"]) ? "" : dtOppBiDocDtl.Rows[0]["numModifiedBy"]) + "  &nbsp;" + (Convert.IsDBNull(dtOppBiDocDtl.Rows[0]["dtModifiedDate"]) ? "" : dtOppBiDocDtl.Rows[0]["dtModifiedDate"]);
                lblDate.Text = FormattedDateFromDate(Convert.ToDateTime(dtOppBiDocDtl.Rows[0]["dtCreatedDate"]), CCommon.ToString(Session["DateFormat"]));
                lblOrganizationName.Text = CCommon.ToString(dtOppBiDocDtl.Rows[0]["OrganizationName"]);
                lblOrganizationContactName.Text = CCommon.ToString(dtOppBiDocDtl.Rows[0]["OrgContactName"]);
                hdnDivID.Value = CCommon.ToString(dtOppBiDocDtl.Rows[0]["numDivisionID"]);
                //SetWebControlAttributes();
                System.DateTime strDate = default(System.DateTime);

                if (!Convert.IsDBNull(dtOppBiDocDtl.Rows[0]["dtFromDate"]))
                {
                    strDate = DateAndTime.DateAdd(DateInterval.Day, CCommon.ToDouble(dtOppBiDocDtl.Rows[0]["numBillingDaysName"]), Convert.ToDateTime(dtOppBiDocDtl.Rows[0]["dtFromDate"]));
                    lblDuedate.Text = FormattedDateFromDate(Convert.ToDateTime(strDate), CCommon.ToString(Session["DateFormat"]));
                }
                if (CCommon.ToBool(dtOppBiDocDtl.Rows[0]["tintBillingTerms"]) == true)
                {
                    lblBillingTerms.Text = "Net " + CCommon.ToString(dtOppBiDocDtl.Rows[0]["numBillingDaysName"]);
                    lblBillingTermsName.Text = CCommon.ToString(dtOppBiDocDtl.Rows[0]["vcBillingTermsName"]);
                }
                else
                {
                    lblBillingTerms.Text = "-";
                    lblBillingTermsName.Text = "-";
                }

                if (CCommon.ToInteger(dtOppBiDocDtl.Rows[0]["fltDiscount"]) > 0)
                {

                    if (CCommon.ToBool(dtOppBiDocDtl.Rows[0]["bitDiscountType"]) == false)
                    {
                        lblDiscount.Text = CCommon.ToString(dtOppBiDocDtl.Rows[0]["fltDiscount"]) + " %";
                    }
                    else
                    {
                        lblDiscount.Text = CCommon.ToString(dtOppBiDocDtl.Rows[0]["fltDiscount"]);
                    }
                }

                lblStatus.Text = CCommon.ToString(dtOppBiDocDtl.Rows[0]["BizDocStatus"]);
                lblComments.Text = "<pre class='normal1'>" + CCommon.ToString(dtOppBiDocDtl.Rows[0]["vcComments"]) + "</pre>";
                if (InvoiceType != "1")
                {
                    lblNo.Text = "";
                }
                else
                {
                    lblNo.Text = CCommon.ToString(dtOppBiDocDtl.Rows[0]["vcRefOrderNo"]);
                }

                hdShipAmt.Value = CCommon.GetDecimalFormat(dtOppBiDocDtl.Rows[0]["monShipCost"]);

                string strTrackingNo = "";

                if (dtOppBiDocDtl.Rows[0]["vcTrackingNo"].ToString().Trim().Length > 0)
                {
                    string[] strTrackingNoList = dtOppBiDocDtl.Rows[0]["vcTrackingNo"].ToString().Split(new string[] { "$^$" }, StringSplitOptions.None);
                    string[] strIDValue = null;

                    if (strTrackingNoList.Length > 0)
                    {
                        for (int k = 0; k <= strTrackingNoList.Length - 1; k++)
                        {
                            strIDValue = strTrackingNoList[k].Split(new string[] { "#^#" }, StringSplitOptions.None);
                            if (strIDValue.Length == 2)
                            {
                                strTrackingNo += string.Format("<a href='{0}' target='_blank'>{1}</a>&nbsp;&nbsp;", (strIDValue[0].Length == 0 ? "javascript:void(0)" : strIDValue[0].ToString() + strIDValue[1].ToString()), strIDValue[1].ToString());
                            }
                        }
                    }
                }
                lblTrackingNumbers.Text = "<div style='width: 95%;word-break: break-all;'>" + strTrackingNo + "</div>";

                //Show capture amount button
                //If dtOppBiDocDtl.Rows[0]["bitAuthoritativeBizDocs") = 1 Then btnCaptureAmount.Visible = IsCardAuthorized()

                DataSet ds = new DataSet();
                DataTable dtOppBiDocItems = default(DataTable);
                objOppBizDocs = new OppBizDocs();
                objOppBizDocs.DomainID = CCommon.ToLong(Session["DomainID"]);
                objOppBizDocs.ReferenceType = CCommon.ToShort(sintRefType);
                objOppBizDocs.ReferenceID = lngReferenceID;
                if (InvoiceType != "1")
                {
                    ds = objOppBizDocs.GetMirrorBizDocItems();
                }
                else
                {
                    objOppBizDocs.OppBizDocId = lngBizDocId;
                    objOppBizDocs.OppId = lngOppId;
                    ds = objOppBizDocs.GetBizDocItemsWithKitChilds();
                }

                dtOppBiDocItems = ds.Tables[0];

                if (dtOppBiDocItems != null && dtOppBiDocItems.Columns.Contains("txtItemDesc") && dtOppBiDocItems.Rows.Count > 0)
                {
                    foreach (DataRow dr in dtOppBiDocItems.Rows)
                    {
                        dr["txtItemDesc"] = CCommon.ToString(dr["txtItemDesc"]).Replace("\n", "<br>");
                    }
                }

                CalculateDealAmount objCalculateDealAmount = new CalculateDealAmount();
                //objCalculateDealAmount.CalculateDealAmount(lngReferenceID, sintRefType, CShort(dtOppBiDocDtl.Rows[0]["tintOppType")), Session["DomainID"], dtOppBiDocItems, FromBizInvoice:=True)
                //kishan
                objCalculateDealAmount.CalculateDealAmountForMirrorBizDoc(lngReferenceID, CCommon.ToShort(sintRefType), CCommon.ToShort(dtOppBiDocDtl.Rows[0]["tintOppType"]), CCommon.ToLong(Session["DomainID"]), dtOppBiDocItems, dtOppBiDocDtl, FromBizInvoice: true);

                hdLateCharge.Value = CCommon.GetDecimalFormat(objCalculateDealAmount.TotalLateCharges);

                //Commented by chintan, reason: line item discount is display purpose only. take aggregate discount from BizDoc edit ->Discount Field
                hdDisc.Value = CCommon.GetDecimalFormat(objCalculateDealAmount.TotalDiscount);
                // + IIf(dtOppBiDocItems.Rows.Count > 0, dtOppBiDocItems.Compute("SUM(DiscAmt)", ""), 0)

                hdSubTotal.Value = CCommon.GetDecimalFormat(objCalculateDealAmount.TotalAmount);

                hdGrandTotal.Value = CCommon.GetDecimalFormat(objCalculateDealAmount.GrandTotal - objCalculateDealAmount.CreditAmount);

                hdTaxAmt.Value = CCommon.GetDecimalFormat(objCalculateDealAmount.TotalTaxAmount - objCalculateDealAmount.TotalCRVTaxAmount);

                hdCRVTxtAmt.Value = CCommon.GetDecimalFormat(objCalculateDealAmount.TotalCRVTaxAmount);

                hdnCreditAmount.Value = CCommon.GetDecimalFormat((objCalculateDealAmount.CreditAmount > 0 ? objCalculateDealAmount.CreditAmount * -1 : 0));

                Session["Itemslist"] = dtOppBiDocItems;

                ///Add columns to datagrid
                int i = 0;
                DataTable dtdgColumns = default(DataTable);
                if (InvoiceType != "1")
                {
                    dtdgColumns = ds.Tables[1];
                }
                else
                {
                    dtdgColumns = ds.Tables[2];
                }
                BoundColumn bColumn = default(BoundColumn);
                for (i = 0; i <= dtdgColumns.Rows.Count - 1; i++)
                {
                    bColumn = new BoundColumn();
                    bColumn.HeaderText = CCommon.ToString(dtdgColumns.Rows[i]["vcFormFieldName"]);
                    bColumn.DataField = CCommon.ToString(dtdgColumns.Rows[i]["vcDbColumnName"]);
                    if (CCommon.ToString(dtdgColumns.Rows[i]["vcDbColumnName"]) == "SerialLotNo")
                    {
                        bColumn.ItemStyle.CssClass = "WordWrapSerialNo";
                    }
                    if (CCommon.ToString(dtdgColumns.Rows[i]["vcFieldDataType"]) == "M")
                        bColumn.DataFormatString = "{0:#,##0.00}";

                    dgBizDocs.Columns.Add(bColumn);
                }
                BindItems();
                FormConfigWizard objConfigWizard = new FormConfigWizard();
                if (CCommon.ToInteger(dtOppBiDocDtl.Rows[0]["tintOppType"]) == 1)
                {
                    //ibtnFooter.Attributes.Add("onclick", "return Openfooter(1)");
                    objConfigWizard.FormID = 7;
                }
                else
                {
                    objConfigWizard.FormID = 7;
                    //ibtnFooter.Attributes.Add("onclick", "return Openfooter(2)");
                }
                hdnOppType.Value = CCommon.ToString(dtOppBiDocDtl.Rows[0]["tintOppType"]);
                hdnBizDocId.Value = CCommon.ToString(dtOppBiDocDtl.Rows[0]["numBizDocId"]);

                //BindBizDocsTemplate();

                objConfigWizard.DomainID = CCommon.ToLong(Session["DomainID"]);
                objConfigWizard.BizDocID = CCommon.ToLong(dtOppBiDocDtl.Rows[0]["numBizDocId"]);
                objConfigWizard.BizDocTemplateID = CCommon.ToLong(dtOppBiDocDtl.Rows[0]["numBizDocTempID"]);

                objOppBizDocs.BizDocId = CCommon.ToLong(dtOppBiDocDtl.Rows[0]["numBizDocId"]);
                objOppBizDocs.DomainID = CCommon.ToLong(Session["DomainID"]);
                objOppBizDocs.BizDocTemplateID = CCommon.ToLong(dtOppBiDocDtl.Rows[0]["numBizDocTempID"]);

                DataSet dsNew = default(DataSet);
                DataTable dtTable = default(DataTable);
                dsNew = objConfigWizard.GetFieldFormListForBizDocsSumm();
                dtTable = dsNew.Tables[1];

                HtmlTableRow tblrow = default(HtmlTableRow);
                HtmlTableCell tblCell = default(HtmlTableCell);
                string strLabelCellID = null;


                foreach (DataRow dr in dtTable.Rows)
                {
                    tblrow = new HtmlTableRow();
                    tblCell = new HtmlTableCell();
                    tblCell.Attributes.Add("class", "normal1");
                    tblCell.InnerText = CCommon.ToString(dr["vcFormFieldName"]) + ": ";
                    tblrow.Cells.Add(tblCell);

                    tblCell = new HtmlTableCell();
                    tblCell.Attributes.Add("class", "normal1");
                    if (CCommon.ToString(dr["vcDbColumnName"]) == "SubTotal")
                    {
                        tblCell.InnerText = CCommon.ToString(dtOppBiDocDtl.Rows[0]["varCurrSymbol"]) + " " + String.Format("{0:#,##0.00}", hdSubTotal.Value);
                    }
                    else if (CCommon.ToString(dr["vcDbColumnName"]) == "ShippingAmount")
                    {
                        tblCell.InnerText = CCommon.ToString(dtOppBiDocDtl.Rows[0]["varCurrSymbol"]) + " " + String.Format("{0:#,##0.00}", hdShipAmt.Value);
                    }
                    else if (CCommon.ToString(dr["vcDbColumnName"]) == "TotalSalesTax")
                    {
                        tblCell.InnerText = CCommon.ToString(dtOppBiDocDtl.Rows[0]["varCurrSymbol"]) + " " + String.Format("{0:#,##0.00}", hdTaxAmt.Value);
                    }
                    else if (CCommon.ToString(dr["vcDbColumnName"]) == "TotalCRVTax")
                    {
                        tblCell.InnerText = CCommon.ToString(dtOppBiDocDtl.Rows[0]["varCurrSymbol"]) + " " + String.Format("{0:#,##0.00}", hdCRVTxtAmt.Value);
                    }
                    else if (CCommon.ToString(dr["vcDbColumnName"]) == "LateCharge")
                    {
                        tblCell.InnerText = CCommon.ToString(dtOppBiDocDtl.Rows[0]["varCurrSymbol"]) + " " + String.Format("{0:#,##0.00}", hdLateCharge.Value);
                    }
                    else if (CCommon.ToString(dr["vcDbColumnName"]) == "Discount")
                    {
                        tblCell.InnerText = CCommon.ToString(dtOppBiDocDtl.Rows[0]["varCurrSymbol"]) + " " + String.Format("{0:#,##0.00}", hdDisc.Value);
                    }
                    else if (CCommon.ToString(dr["vcDbColumnName"]) == "CreditApplied")
                    {
                        tblCell.InnerText = CCommon.ToString(dtOppBiDocDtl.Rows[0]["varCurrSymbol"]) + " " + String.Format("{0:#,##0.00}", hdnCreditAmount.Value);
                    }
                    else if (CCommon.ToString(dr["vcDbColumnName"]) == "GrandTotal")
                    {
                        tblCell.ID = "tdGradTotal";
                        strLabelCellID = tblCell.ID;
                        //tblCell.InnerText = hdGrandTotal.Value
                    }
                    else
                    {
                        if (CCommon.ToInteger(dtOppBiDocDtl.Rows[0]["tintOppType"]) == 1)
                        {
                            decimal taxAmt = default(decimal);
                            taxAmt = (Convert.IsDBNull(dtOppBiDocItems.Compute("SUM([" + CCommon.ToString(dr["vcFormFieldName"]) + "])", "[" + CCommon.ToString(dr["vcFormFieldName"]) + "]>0")) ? 0 : CCommon.ToDecimal(dtOppBiDocItems.Compute("SUM([" + CCommon.ToString(dr["vcFormFieldName"]) + "])", "[" + CCommon.ToString(dr["vcFormFieldName"]) + "]>0")));
                            tblCell.InnerText = CCommon.ToString(dtOppBiDocDtl.Rows[0]["varCurrSymbol"]) + " " + String.Format("{0:#,##0.00}", taxAmt);
                        }
                    }
                    tblrow.Cells.Add(tblCell);
                    tblBizDocSumm.Rows.Add(tblrow);
                }
                if (!string.IsNullOrEmpty(strLabelCellID))
                {
                    ((HtmlTableCell)tblBizDocSumm.FindControl(strLabelCellID)).InnerText = CCommon.ToString(dtOppBiDocDtl.Rows[0]["varCurrSymbol"]) + " " + String.Format("{0:#,##0.00}", hdGrandTotal.Value);
                }

                lblBalance.Text = String.Format("{0:#,##0.00}", CCommon.ToDecimal(hdGrandTotal.Value) - CCommon.ToDecimal(lblAmountPaid.Text));
                hdnBalance.Value = lblBalance.Text;
                lblAmountPaidCurrency.Text = CCommon.ToString(dtOppBiDocDtl.Rows[0]["varCurrSymbol"]);
                lblBalanceDueCurrency.Text = CCommon.ToString(dtOppBiDocDtl.Rows[0]["varCurrSymbol"]);
                if (InvoiceType != "1")
                {
                    hdnOrientation.Value = CCommon.ToString("");
                    hdnKeepFooterBottom.Value = CCommon.ToString("");
                }
                else
                {
                    hdnOrientation.Value = CCommon.ToString(dtOppBiDocDtl.Rows[0]["numOrientation"]);
                    hdnKeepFooterBottom.Value = CCommon.ToString(dtOppBiDocDtl.Rows[0]["bitKeepFooterBottom"]);
                }

                if (CCommon.ToBool(dtOppBiDocDtl.Rows[0]["bitEnabled"]) == true & CCommon.ToString(dtOppBiDocDtl.Rows[0]["txtBizDocTemplate"]).Length > 0)
                {
                    //BizDoc UI modification 
                    string strCss = "<style>" + Server.HtmlDecode(CCommon.ToString(dtOppBiDocDtl.Rows[0]["txtCSS"])) + "</style>";
                    string strBizDocUI = strCss + HttpUtility.HtmlDecode(CCommon.ToString(dtOppBiDocDtl.Rows[0]["txtBizDocTemplate"]));
                    if (strBizDocUI.Length > 0)
                    {
                        strBizDocUI = strBizDocUI.Replace("#Logo#", CCommon.RenderControl(imgLogo));
                        //strBizDocUI = strBizDocUI.Replace("#Signature#", CCommon.RenderControl(imgSignature))
                        strBizDocUI = strBizDocUI.Replace("#FooterImage#", CCommon.RenderControl(imgFooter));
                        strBizDocUI = strBizDocUI.Replace("#BizDocType#", CCommon.RenderControl(lblBizDoc));

                        //''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                        //'Customer/Vendor Information
                        strBizDocUI = strBizDocUI.Replace("#Customer/VendorOrganizationName#", CCommon.ToString(dtOppBiDocDtl.Rows[0]["OrganizationName"]));
                        strBizDocUI = strBizDocUI.Replace("#Customer/VendorOrganizationPhone#", CCommon.ToString(dtOppBiDocDtl.Rows[0]["OrganizationPhone"]));
                        DataSet dsAddress = new DataSet();
                        objOppBizDocs.OppId = lngOppId;
                        objOppBizDocs.DomainID = CCommon.ToLong(Session["DomainID"]);
                        objOppBizDocs.OppBizDocId = lngBizDocId;
                        dsAddress = objOppBizDocs.GetOPPGetOppAddressDetails();

                        if (dsAddress.Tables[0].Rows.Count == 0)
                        {
                            strBizDocUI = strBizDocUI.Replace("#OppOrderBillToCompanyName#", "");
                            strBizDocUI = strBizDocUI.Replace("#OppOrderBillToAddress#", "");
                            strBizDocUI = strBizDocUI.Replace("#OppOrderBillToStreet#", "");
                            strBizDocUI = strBizDocUI.Replace("#OppOrderBillToCity#", "");
                            strBizDocUI = strBizDocUI.Replace("#OppOrderBillToPostal#", "");
                            strBizDocUI = strBizDocUI.Replace("#OppOrderBillToState#", "");
                            strBizDocUI = strBizDocUI.Replace("#OppOrderBillToCountry#", "");
                            strBizDocUI = strBizDocUI.Replace("#OppOrderBillToAddressName#", "");
                            strBizDocUI = strBizDocUI.Replace("#BillingContact#", "");
                        }
                        else
                        {
                            strBizDocUI = strBizDocUI.Replace("#OppOrderBillToCompanyName#", CCommon.ToString(dsAddress.Tables[0].Rows[0]["vcCompanyName"]));
                            strBizDocUI = strBizDocUI.Replace("#OppOrderBillToAddress#", CCommon.ToString(dsAddress.Tables[0].Rows[0]["vcFullAddress"]));
                            strBizDocUI = strBizDocUI.Replace("#OppOrderBillToStreet#", CCommon.ToString(dsAddress.Tables[0].Rows[0]["vcStreet"]));
                            strBizDocUI = strBizDocUI.Replace("#OppOrderBillToCity#", CCommon.ToString(dsAddress.Tables[0].Rows[0]["vcCity"]));
                            strBizDocUI = strBizDocUI.Replace("#OppOrderBillToPostal#", CCommon.ToString(dsAddress.Tables[0].Rows[0]["vcPostalCode"]));
                            strBizDocUI = strBizDocUI.Replace("#OppOrderBillToState#", CCommon.ToString(dsAddress.Tables[0].Rows[0]["vcState"]));
                            strBizDocUI = strBizDocUI.Replace("#OppOrderBillToCountry#", CCommon.ToString(dsAddress.Tables[0].Rows[0]["vcCountry"]));
                            strBizDocUI = strBizDocUI.Replace("#OppOrderBillToAddressName#", CCommon.ToString(dsAddress.Tables[0].Rows[0]["vcAddressName"]));
                            strBizDocUI = strBizDocUI.Replace("#BillingContact#", CCommon.ToString(dsAddress.Tables[0].Rows[0]["vcContact"]));
                        }

                        if (dsAddress != null && dsAddress.Tables.Count > 4 && dsAddress.Tables[4].Rows.Count > 0)
                        {
                            strBizDocUI = strBizDocUI.Replace("#Customer/VendorBillToCompanyName#", CCommon.ToString(dsAddress.Tables[4].Rows[0]["vcCompanyName"]));
                            strBizDocUI = strBizDocUI.Replace("#Customer/VendorBillToAddress#", CCommon.ToString(dsAddress.Tables[4].Rows[0]["vcFullAddress"]));
                            strBizDocUI = strBizDocUI.Replace("#Customer/VendorBillToStreet#", CCommon.ToString(dsAddress.Tables[4].Rows[0]["vcStreet"]));
                            strBizDocUI = strBizDocUI.Replace("#Customer/VendorBillToCity#", CCommon.ToString(dsAddress.Tables[4].Rows[0]["vcCity"]));
                            strBizDocUI = strBizDocUI.Replace("#Customer/VendorBillToPostal#", CCommon.ToString(dsAddress.Tables[4].Rows[0]["vcPostalCode"]));
                            strBizDocUI = strBizDocUI.Replace("#Customer/VendorBillToState#", CCommon.ToString(dsAddress.Tables[4].Rows[0]["vcState"]));
                            strBizDocUI = strBizDocUI.Replace("#Customer/VendorBillToCountry#", CCommon.ToString(dsAddress.Tables[4].Rows[0]["vcCountry"]));
                            strBizDocUI = strBizDocUI.Replace("#Customer/VendorBillToAddressName#", CCommon.ToString(dsAddress.Tables[4].Rows[0]["vcAddressName"]));
                        }
                        else
                        {
                            strBizDocUI = strBizDocUI.Replace("#Customer/VendorBillToCompanyName#", "");
                            strBizDocUI = strBizDocUI.Replace("#Customer/VendorBillToAddress#", "");
                            strBizDocUI = strBizDocUI.Replace("#Customer/VendorBillToStreet#", "");
                            strBizDocUI = strBizDocUI.Replace("#Customer/VendorBillToCity#", "");
                            strBizDocUI = strBizDocUI.Replace("#Customer/VendorBillToPostal#", "");
                            strBizDocUI = strBizDocUI.Replace("#Customer/VendorBillToState#", "");
                            strBizDocUI = strBizDocUI.Replace("#Customer/VendorBillToCountry#", "");
                            strBizDocUI = strBizDocUI.Replace("#Customer/VendorBillToAddressName#", "");
                        }

                        if (dsAddress.Tables[1].Rows.Count == 0)
                        {
                            strBizDocUI = strBizDocUI.Replace("#OppOrderShipToCompanyName#", "");
                            strBizDocUI = strBizDocUI.Replace("#OppOrderShipToAddress#", "");
                            strBizDocUI = strBizDocUI.Replace("#OppOrderShipToStreet#", "");
                            strBizDocUI = strBizDocUI.Replace("#OppOrderShipToCity#", "");
                            strBizDocUI = strBizDocUI.Replace("#OppOrderShipToPostal#", "");
                            strBizDocUI = strBizDocUI.Replace("#OppOrderShipToState#", "");
                            strBizDocUI = strBizDocUI.Replace("#OppOrderShipToCountry#", "");
                            strBizDocUI = strBizDocUI.Replace("#OppOrderShipToAddressName#", "");
                            strBizDocUI = strBizDocUI.Replace("#ShippingContact#", "");
                        }
                        else
                        {
                            strBizDocUI = strBizDocUI.Replace("#OppOrderShipToCompanyName#", CCommon.ToString(dsAddress.Tables[1].Rows[0]["vcCompanyName"]));
                            strBizDocUI = strBizDocUI.Replace("#OppOrderShipToAddress#", CCommon.ToString(dsAddress.Tables[1].Rows[0]["vcFullAddress"]));
                            strBizDocUI = strBizDocUI.Replace("#OppOrderShipToStreet#", CCommon.ToString(dsAddress.Tables[1].Rows[0]["vcStreet"]));
                            strBizDocUI = strBizDocUI.Replace("#OppOrderShipToCity#", CCommon.ToString(dsAddress.Tables[1].Rows[0]["vcCity"]));
                            strBizDocUI = strBizDocUI.Replace("#OppOrderShipToPostal#", CCommon.ToString(dsAddress.Tables[1].Rows[0]["vcPostalCode"]));
                            strBizDocUI = strBizDocUI.Replace("#OppOrderShipToState#", CCommon.ToString(dsAddress.Tables[1].Rows[0]["vcState"]));
                            strBizDocUI = strBizDocUI.Replace("#OppOrderShipToCountry#", CCommon.ToString(dsAddress.Tables[1].Rows[0]["vcCountry"]));
                            strBizDocUI = strBizDocUI.Replace("#OppOrderShipToAddressName#", CCommon.ToString(dsAddress.Tables[1].Rows[0]["vcAddressName"]));
                            strBizDocUI = strBizDocUI.Replace("#ShippingContact#", CCommon.ToString(dsAddress.Tables[1].Rows[0]["vcContact"]));
                        }

                        if (dsAddress != null && dsAddress.Tables.Count > 5 && dsAddress.Tables[5].Rows.Count > 0)
                        {
                            strBizDocUI = strBizDocUI.Replace("#Customer/VendorShipToCompanyName#", CCommon.ToString(dsAddress.Tables[5].Rows[0]["vcCompanyName"]));
                            strBizDocUI = strBizDocUI.Replace("#Customer/VendorShipToAddress#", CCommon.ToString(dsAddress.Tables[5].Rows[0]["vcFullAddress"]));
                            strBizDocUI = strBizDocUI.Replace("#Customer/VendorShipToStreet#", CCommon.ToString(dsAddress.Tables[5].Rows[0]["vcStreet"]));
                            strBizDocUI = strBizDocUI.Replace("#Customer/VendorShipToCity#", CCommon.ToString(dsAddress.Tables[5].Rows[0]["vcCity"]));
                            strBizDocUI = strBizDocUI.Replace("#Customer/VendorShipToPostal#", CCommon.ToString(dsAddress.Tables[5].Rows[0]["vcPostalCode"]));
                            strBizDocUI = strBizDocUI.Replace("#Customer/VendorShipToState#", CCommon.ToString(dsAddress.Tables[5].Rows[0]["vcState"]));
                            strBizDocUI = strBizDocUI.Replace("#Customer/VendorShipToCountry#", CCommon.ToString(dsAddress.Tables[5].Rows[0]["vcCountry"]));
                            strBizDocUI = strBizDocUI.Replace("#Customer/VendorShipToAddressName#", CCommon.ToString(dsAddress.Tables[5].Rows[0]["vcAddressName"]));
                        }
                        else
                        {
                            strBizDocUI = strBizDocUI.Replace("#Customer/VendorShipToCompanyName#", "");
                            strBizDocUI = strBizDocUI.Replace("#Customer/VendorShipToAddress#", "");
                            strBizDocUI = strBizDocUI.Replace("#Customer/VendorShipToStreet#", "");
                            strBizDocUI = strBizDocUI.Replace("#Customer/VendorShipToCity#", "");
                            strBizDocUI = strBizDocUI.Replace("#Customer/VendorShipToPostal#", "");
                            strBizDocUI = strBizDocUI.Replace("#Customer/VendorShipToState#", "");
                            strBizDocUI = strBizDocUI.Replace("#Customer/VendorShipToCountry#", "");
                            strBizDocUI = strBizDocUI.Replace("#Customer/VendorShipToAddressName#", "");
                        }

                        strBizDocUI = strBizDocUI.Replace("#OppOrderChangeShipToHeader#", "Ship To");
                        strBizDocUI = strBizDocUI.Replace("#OppOrderChangeBillToHeader#", "Bill To");

                        strBizDocUI = strBizDocUI.Replace("#Customer/VendorOrganizationComments#", CCommon.ToString(dtOppBiDocDtl.Rows[0]["vcOrganizationComments"]));

                        strBizDocUI = strBizDocUI.Replace("#Customer/VendorOrganizationContactName#", CCommon.RenderControl(lblOrganizationContactName));
                        strBizDocUI = strBizDocUI.Replace("#Customer/VendorOrganizationContactEmail#", CCommon.ToString(dtOppBiDocDtl.Rows[0]["OrgContactEmail"]));
                        strBizDocUI = strBizDocUI.Replace("#Customer/VendorOrganizationContactPhone#", CCommon.ToString(dtOppBiDocDtl.Rows[0]["OrgContactPhone"]));
                        //''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

                        //'Employer Information
                        strBizDocUI = strBizDocUI.Replace("#EmployerOrganizationName#", CCommon.ToString(dtOppBiDocDtl.Rows[0]["EmployerOrganizationName"]));
                        strBizDocUI = strBizDocUI.Replace("#EmployerOrganizationPhone#", CCommon.ToString(dtOppBiDocDtl.Rows[0]["EmployerOrganizationPhone"]));

                        if (dsAddress.Tables[2].Rows.Count == 0)
                        {
                            strBizDocUI = strBizDocUI.Replace("#EmployerBillToCompanyName#", "");
                            strBizDocUI = strBizDocUI.Replace("#EmployerBillToAddress#", "");
                            strBizDocUI = strBizDocUI.Replace("#EmployerBillToStreet#", "");
                            strBizDocUI = strBizDocUI.Replace("#EmployerBillToCity#", "");
                            strBizDocUI = strBizDocUI.Replace("#EmployerBillToPostal#", "");
                            strBizDocUI = strBizDocUI.Replace("#EmployerBillToState#", "");
                            strBizDocUI = strBizDocUI.Replace("#EmployerBillToCountry#", "");
                            strBizDocUI = strBizDocUI.Replace("#EmployerBillToAddressName#", "");
                        }
                        else
                        {
                            strBizDocUI = strBizDocUI.Replace("#EmployerBillToCompanyName#", CCommon.ToString(dsAddress.Tables[2].Rows[0]["vcCompanyName"]));
                            strBizDocUI = strBizDocUI.Replace("#EmployerBillToAddress#", CCommon.ToString(dsAddress.Tables[2].Rows[0]["vcFullAddress"]));
                            strBizDocUI = strBizDocUI.Replace("#EmployerBillToStreet#", CCommon.ToString(dsAddress.Tables[2].Rows[0]["vcStreet"]));
                            strBizDocUI = strBizDocUI.Replace("#EmployerBillToCity#", CCommon.ToString(dsAddress.Tables[2].Rows[0]["vcCity"]));
                            strBizDocUI = strBizDocUI.Replace("#EmployerBillToPostal#", CCommon.ToString(dsAddress.Tables[2].Rows[0]["vcPostalCode"]));
                            strBizDocUI = strBizDocUI.Replace("#EmployerBillToState#", CCommon.ToString(dsAddress.Tables[2].Rows[0]["vcState"]));
                            strBizDocUI = strBizDocUI.Replace("#EmployerBillToCountry#", CCommon.ToString(dsAddress.Tables[2].Rows[0]["vcCountry"]));
                            strBizDocUI = strBizDocUI.Replace("#EmployerBillToAddressName#", CCommon.ToString(dsAddress.Tables[2].Rows[0]["vcAddressName"]));
                        }

                        if (dsAddress.Tables[3].Rows.Count == 0)
                        {
                            strBizDocUI = strBizDocUI.Replace("#EmployerShipToCompanyName#", "");
                            strBizDocUI = strBizDocUI.Replace("#EmployerShipToAddress#", "");
                            strBizDocUI = strBizDocUI.Replace("#EmployerShipToStreet#", "");
                            strBizDocUI = strBizDocUI.Replace("#EmployerShipToCity#", "");
                            strBizDocUI = strBizDocUI.Replace("#EmployerShipToPostal#", "");
                            strBizDocUI = strBizDocUI.Replace("#EmployerShipToState#", "");
                            strBizDocUI = strBizDocUI.Replace("#EmployerShipToCountry#", "");
                            strBizDocUI = strBizDocUI.Replace("#EmployerShipToAddressName#", "");
                        }
                        else
                        {
                            strBizDocUI = strBizDocUI.Replace("#EmployerShipToCompanyName#", CCommon.ToString(dsAddress.Tables[3].Rows[0]["vcCompanyName"]));
                            strBizDocUI = strBizDocUI.Replace("#EmployerShipToAddress#", CCommon.ToString(dsAddress.Tables[3].Rows[0]["vcFullAddress"]));
                            strBizDocUI = strBizDocUI.Replace("#EmployerShipToStreet#", CCommon.ToString(dsAddress.Tables[3].Rows[0]["vcStreet"]));
                            strBizDocUI = strBizDocUI.Replace("#EmployerShipToCity#", CCommon.ToString(dsAddress.Tables[3].Rows[0]["vcCity"]));
                            strBizDocUI = strBizDocUI.Replace("#EmployerShipToPostal#", CCommon.ToString(dsAddress.Tables[3].Rows[0]["vcPostalCode"]));
                            strBizDocUI = strBizDocUI.Replace("#EmployerShipToState#", CCommon.ToString(dsAddress.Tables[3].Rows[0]["vcState"]));
                            strBizDocUI = strBizDocUI.Replace("#EmployerShipToCountry#", CCommon.ToString(dsAddress.Tables[3].Rows[0]["vcCountry"]));
                            strBizDocUI = strBizDocUI.Replace("#EmployerShipToAddressName#", CCommon.ToString(dsAddress.Tables[3].Rows[0]["vcAddressName"]));
                        }

                        strBizDocUI = strBizDocUI.Replace("#EmployerChangeShipToHeader#", "Ship To");
                        strBizDocUI = strBizDocUI.Replace("#EmployerChangeBillToHeader#", "Bill To");

                        //''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

                        strBizDocUI = strBizDocUI.Replace("#BizDocStatus#", lblStatus.Text);
                        strBizDocUI = strBizDocUI.Replace("#Currency#", lblBalanceDueCurrency.Text);
                        strBizDocUI = strBizDocUI.Replace("#AmountPaid#", lblAmountPaid.Text);
                        strBizDocUI = strBizDocUI.Replace("#BalanceDue#", CCommon.RenderControl(lblBalance));
                        //used by amt paid js
                        strBizDocUI = strBizDocUI.Replace("#Discount#", lblDiscount.Text);
                        strBizDocUI = strBizDocUI.Replace("#BillingTerms#", lblBillingTerms.Text);
                        if (CCommon.ToString(dtOppBiDocDtl.Rows[0]["dtFromDate"]).Length > 3)
                        {
                            strBizDocUI = strBizDocUI.Replace("#BillingTermFromDate#", FormattedDateFromDate(Convert.ToDateTime(dtOppBiDocDtl.Rows[0]["dtFromDate"]), CCommon.ToString(Session["DateFormat"])));
                        }
                        strBizDocUI = strBizDocUI.Replace("#BillingTermsName#", lblBillingTermsName.Text);

                        strBizDocUI = strBizDocUI.Replace("#DueDate#", lblDuedate.Text);
                        strBizDocUI = strBizDocUI.Replace("#BizDocID#", CCommon.RenderControl(lblBizDocIDValue));
                        strBizDocUI = strBizDocUI.Replace("#P.O.NO#", lblNo.Text);
                        strBizDocUI = strBizDocUI.Replace("#OrderID#", CCommon.RenderControl(hplOppID));
                        strBizDocUI = strBizDocUI.Replace("#Comments#", lblComments.Text);
                        strBizDocUI = strBizDocUI.Replace("#Products#", CCommon.RenderControl(dgBizDocs));
                        strBizDocUI = strBizDocUI.Replace("#BizDocSummary#", CCommon.RenderControl(tblBizDocSumm));
                        strBizDocUI = strBizDocUI.Replace("#BizDocCreatedDate#", lblDate.Text);

                        strBizDocUI = strBizDocUI.Replace("#AmountPaidPopUp#", CCommon.RenderControl(hplAmountPaid));
                        strBizDocUI = strBizDocUI.Replace("#ChangeDueDate#", CCommon.RenderControl(hplDueDate));
                        strBizDocUI = strBizDocUI.Replace("#AssigneeName#", CCommon.ToString(dtOppBiDocDtl.Rows[0]["AssigneeName"]));
                        strBizDocUI = strBizDocUI.Replace("#AssigneeEmail#", CCommon.ToString(dtOppBiDocDtl.Rows[0]["AssigneeEmail"]));
                        strBizDocUI = strBizDocUI.Replace("#AssigneePhoneNo#", CCommon.ToString(dtOppBiDocDtl.Rows[0]["AssigneePhone"]));
                        strBizDocUI = strBizDocUI.Replace("#PartnerSource#", CCommon.ToString(dtOppBiDocDtl.Rows[0]["vcPartner"]));
                        strBizDocUI = strBizDocUI.Replace("#ReleaseDate#", CCommon.ToString(dtOppBiDocDtl.Rows[0]["vcReleaseDate"]));
                        strBizDocUI = strBizDocUI.Replace("#RequiredDate#", CCommon.ToString(dtOppBiDocDtl.Rows[0]["vcRequiredDate"]));
                        strBizDocUI = strBizDocUI.Replace("#InventoryStatus#", CCommon.ToString(dtOppBiDocDtl.Rows[0]["vcInventoryStatus"]));
                        //strBizDocUI = strBizDocUI.Replace("#OrganizationName#", CCommon.RenderControl(lblOrganizationName));
                        //strBizDocUI = strBizDocUI.Replace("#OrganizationContactName#", CCommon.RenderControl(lblOrganizationContactName));
                        //strBizDocUI = strBizDocUI.Replace("#OrganizationContactPhone#", CCommon.ToString(dtOppBiDocDtl.Rows[0]["OrgContactPhone"]));
                        //strBizDocUI = strBizDocUI.Replace("#OrganizationContactEmail#", CCommon.ToString(dtOppBiDocDtl.Rows[0]["OrgContactEmail"]));
                        //strBizDocUI = strBizDocUI.Replace("#OrganizationBillingAddress#", CCommon.ToString(dtOppBiDocDtl.Rows[0]["CompanyBillingAddress"]));
                        //strBizDocUI = strBizDocUI.Replace("#OrganizationPhone#", CCommon.ToString(dtOppBiDocDtl.Rows[0]["OrganizationPhone"]));
                        strBizDocUI = strBizDocUI.Replace("#OrderRecOwner#", CCommon.ToString(dtOppBiDocDtl.Rows[0]["OnlyOrderRecOwner"]));
                        strBizDocUI = strBizDocUI.Replace("#CustomerPO##", CCommon.ToString(dtOppBiDocDtl.Rows[0]["vcCustomerPO#"]));
                        strBizDocUI = strBizDocUI.Replace("#SOComments#", CCommon.ToString(dtOppBiDocDtl.Rows[0]["vcSOComments"]));
                        strBizDocUI = strBizDocUI.Replace("#ParcelShippingAccount#", CCommon.ToString(dtOppBiDocDtl.Rows[0]["vcShippersAccountNo"]));
                        strBizDocUI = strBizDocUI.Replace("#OrderCreatedDate#", CCommon.ToString(dtOppBiDocDtl.Rows[0]["OrderCreatedDate"]));
                        strBizDocUI = strBizDocUI.Replace("#VendorInvoice#", CCommon.ToString(dtOppBiDocDtl.Rows[0]["vcVendorInvoice"]));
                        strBizDocUI = strBizDocUI.Replace("#ShippingCompany#", CCommon.ToString(dtOppBiDocDtl.Rows[0]["ShipVia"]));
                        strBizDocUI = strBizDocUI.Replace("#ShippingService#", CCommon.ToString(dtOppBiDocDtl.Rows[0]["vcShippingService"]));
                        strBizDocUI = strBizDocUI.Replace("#PackingSlipID#", CCommon.ToString(dtOppBiDocDtl.Rows[0]["vcPackingSlip"]));
                        strBizDocUI = strBizDocUI.Replace("#BizDocTemplateName#", CCommon.ToString(dtOppBiDocDtl.Rows[0]["vcTemplateName"]));
                        if(dtOppBiDocItems != null && dtOppBiDocItems.Rows.Count > 0){
                            if(dtOppBiDocItems.Select("numItemCode <>" + CCommon.ToDouble(Session["DiscountServiceItem"]) + " AND numItemCode <> " + CCommon.ToDouble(Session["ShippingServiceItem"])).Length > 0){
                                strBizDocUI = strBizDocUI.Replace("#TotalQuantity#", String.Format("{0:#,##0.##}", CCommon.ToDecimal(dtOppBiDocItems.Select("numItemCode <>" + CCommon.ToDouble(Session["DiscountServiceItem"]) + " AND numItemCode <> " + CCommon.ToDouble(Session["ShippingServiceItem"])).CopyToDataTable().Compute("sum(numUnitHour)", ""))));
                            } else {
                                strBizDocUI = strBizDocUI.Replace("#TotalQuantity#", String.Format("{0:#,##0.##}", 0));
                            }
                        } else {
                            strBizDocUI = strBizDocUI.Replace("#TotalQuantity#", String.Format("{0:#,##0.##}", 0));
                        }
                        strBizDocUI = strBizDocUI.Replace("#TrackingNo#", CCommon.RenderControl(lblTrackingNumbers));

                        NumericToWord lNumericToWord = new NumericToWord();

                        strBizDocUI = strBizDocUI.Replace("#GrandTotalinWords#", CCommon.ToString(lNumericToWord.SpellNumber(Strings.Replace(hdGrandTotal.Value, ",", ""))));

                        if (dtOppBiDocDtl.Columns.Contains("vcTotalQtybyUOM"))
                        {
                            strBizDocUI = strBizDocUI.Replace("#TotalQytbyUOM#", CCommon.ToString(dtOppBiDocDtl.Rows[0]["vcTotalQtybyUOM"]));
                        }
                        else
                        {
                            strBizDocUI = strBizDocUI.Replace("#TotalQytbyUOM#", "");
                        }

                        //Replace custom field values
                        CustomFields objCustomFields = new CustomFields();
                        if (CCommon.ToInteger(dtOppBiDocDtl.Rows[0]["tintOppType"]) == 1)
                        {
                            objCustomFields.locId = 2;
                        }
                        else
                        {
                            objCustomFields.locId = 6;
                        }
                        objCustomFields.RelId = 0;
                        objCustomFields.DomainID = CCommon.ToLong(Session["DomainID"]);
                        objCustomFields.RecordId = lngReferenceID;
                        ds = objCustomFields.GetCustFlds();
                        foreach (DataRow drow in ds.Tables[0].Rows)
                        {
                            if (CCommon.ToLong(drow["numListID"]) > 0)
                            {
                                strBizDocUI = strBizDocUI.Replace("#" + CCommon.ToString(drow["fld_label"]).Trim().Replace(" ", "") + "#", objCommon.GetListItemValue(CCommon.ToString(drow["Value"]), CCommon.ToLong(Session["DomainID"])));
                            }
                            else
                            {
                                strBizDocUI = strBizDocUI.Replace("#" + CCommon.ToString(drow["fld_label"]).Trim().Replace(" ", "") + "#", CCommon.ToString(drow["Value"]));
                            }
                        }
                        litBizDocTemplate.Text = strBizDocUI;
                        tblOriginalBizDoc.Visible = false;
                        tblFormattedBizDoc.Visible = true;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private void BindItems()
        {
            try
            {
                DataTable dtItems = new DataTable();
                dtItems = (DataTable)Session["Itemslist"];
                dgBizDocs.DataSource = dtItems;
                dgBizDocs.DataBind();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private string FormattedDateFromDate(System.DateTime dtDate, string strFormat)
        {
            try
            {
                string str4Yr = CCommon.ToString(Convert.ToDateTime(dtDate).Year);
                //Set the Year in 2 Digits to a Variable
                string str2Yr = Strings.Mid(CCommon.ToString(Convert.ToDateTime(dtDate).Year), 3, 2);
                //Set the Month in 2 Digits to a Variable
                string strIntMonth = CCommon.ToString(dtDate.Month);

                if (Convert.ToInt32(strIntMonth) <= 9)
                {
                    strIntMonth = "0" + strIntMonth;
                }
                //Set the Date in 2 Digits to a Variable
                string strDate = CCommon.ToString(dtDate.Day);

                if (Convert.ToInt32(strDate) <= 9)
                {
                    strDate = "0" + strDate;
                }
                //Set the Abbrivated Month Name to a Variable

                string str3Month = GetMonthName(CCommon.ToInteger(strIntMonth), true); //Convert.ToDateTime(strIntMonth).ToString("MMM");
                //Set the Full Month Name to a Variable
                string strFullMonth = GetMonthName(CCommon.ToInteger(strIntMonth), true);// Convert.ToInt32(strIntMonth).ToString("MMM");
                //Set the Nos of Hrs to a Variable
                string strHrs = CCommon.ToString(dtDate.Hour);
                //Set the Nos of Mins to a Variable
                string strMins = CCommon.ToString(dtDate.Minute);

                //As the Date Format will be one of the above mentioned formats,
                //we need to replace the required string to get the formatted date.
                strFormat = Strings.Replace(strFormat, "DD", strDate);
                strFormat = Strings.Replace(strFormat, "YYYY", str4Yr);
                strFormat = Strings.Replace(strFormat, "YY", str2Yr);
                strFormat = Strings.Replace(strFormat, "MM", strIntMonth);
                strFormat = Strings.Replace(strFormat, "MONTH", strFullMonth);
                strFormat = Strings.Replace(strFormat, "MON", str3Month);

                return strFormat;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private static string GetMonthName(int month, bool abbrev)
        {
            try
            {
                DateTime date = new DateTime(1900, month, 1);
                if (abbrev) return date.ToString("MMM");
                return date.ToString("MMMM");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
        protected void btnExportPDF_Click(object sender, EventArgs e)
        {
            try
            {
                hdnBizDocHTML.Value = "<html><head><link rel='stylesheet' href='" + Server.MapPath("~/CSS/master.css") + "' type='text/css' /></head><body>" + hdnBizDocHTML.Value + "</body></html>";

                OppBizDocs objBizDocs = new OppBizDocs();
                string strFileName = "";
                string strFilePhysicalLocation = "";

                HTMLToPDF objHTMLToPDF = new HTMLToPDF();
                strFileName = objHTMLToPDF.ConvertHTML2PDF(hdnBizDocHTML.Value, CCommon.ToLong(Session["DomainID"]), numOrientation: (hdnOrientation.Value == "2" ? 2 : 1), keepFooterAtBottom: CCommon.ToBool(hdnKeepFooterBottom.Value));
                strFilePhysicalLocation = CCommon.GetDocumentPhysicalPath(CCommon.ToLong(Session["DomainID"])) + strFileName;

                Response.Clear();
                Response.ClearContent();
                Response.ContentType = "application/pdf";
                Response.AddHeader("content-disposition", "attachment; filename=" + (lblBizDocIDValue.Text.Trim().Length > 0 ? lblBizDocIDValue.Text.Trim().Replace(" ", "_").Replace(",", " ") + ".pdf" : strFileName.Replace(",", " ")));
                //Response.Write(Session["Attachements"])

                Response.WriteFile(strFilePhysicalLocation);

                Response.End();
            }
            catch (Exception ex)
            {
                Response.Write(ex);
            }

        }

        #region ExtraCode
        //private void SetWebControlAttributes()
        //{
        //    try
        //    {
        //        //  ibtnLogo.Attributes.Add("onclick", "return OpenLogoPage();");
        //        //hplAmountPaid.Attributes.Add("onclick", "return OpenAmtPaid(" & sintRefType & "," & lngReferenceID.ToString & " ," & hdnDivID.Value & ");")
        //        // ibtnPrint.Attributes.Add("onclick", "return PrintIt();");
        //        // ibtnExport.Attributes.Add("onclick", "OpenExport(" + sintRefType.ToString + "," + lngReferenceID.ToString + "); return false;");
        //        // hplOppID.Attributes.Add("onclick", "return Close1(" + lngReferenceID.ToString + ")");
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}



        //public void BindBizDocsTemplate()
        //{
        //    try
        //    {
        //        //OppBizDocs objOppBizDoc = new OppBizDocs();
        //        //objOppBizDoc.DomainID = Session("DomainID");
        //        //objOppBizDoc.BizDocId = hdnBizDocId.Value;
        //        //objOppBizDoc.OppType = hdnOppType.Value;

        //        //DataTable dtBizDocTemplate = objOppBizDoc.GetBizDocTemplateList();

        //        //ddlBizDocTemplate.DataSource = dtBizDocTemplate;
        //        //ddlBizDocTemplate.DataTextField = "vcTemplateName";
        //        //ddlBizDocTemplate.DataValueField = "numBizDocTempID";
        //        //ddlBizDocTemplate.DataBind();

        //        //ddlBizDocTemplate.Items.Insert(0, new ListItem("--Select--", 0));
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        //if (sintRefType == 1 | sintRefType == 2)
        //{
        //    hplBillto.Attributes.Add("onclick", "return openeditAddress('Bill'," + lngReferenceID + ",0)");
        //    hplShipTo.Attributes.Add("onclick", "return openeditAddress('Ship'," + lngReferenceID + ",0)");
        //}
        //else
        //{
        //    hplBillto.Attributes.Add("onclick", "return openeditAddress('Bill',0," + lngReferenceID + ")");
        //    hplShipTo.Attributes.Add("onclick", "return openeditAddress('Ship',0," + lngReferenceID + ")");
        //}

        //if (CCommon.ToInteger(dtOppBiDocDtl.Rows[0]["bitAuthoritativeBizDocs"]) == 1)
        //{
        //    m_aryRightsForAuthoritativ = GetUserRightsForPage_Other(10, 28);

        //    if (m_aryRightsForAuthoritativ(RIGHTSTYPE.VIEW) == 0)
        //    {
        //        Response.Redirect("../admin/authentication.aspx?mesg=AC");
        //    }
        //}
        //else
        //{
        //    m_aryRightsForNonAuthoritativ = GetUserRightsForPage_Other(10, 29);

        //    if (m_aryRightsForNonAuthoritativ(RIGHTSTYPE.VIEW) == 0)
        //    {
        //        Response.Redirect("../admin/authentication.aspx?mesg=AC");
        //    }

        //}

        #endregion

    }
}