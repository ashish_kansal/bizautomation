﻿using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BACRM.BusinessLogic.Opportunities;
using BACRM.BusinessLogic.Common;
using BACRM.BusinessLogic.Item;
using System.Data;
using BACRM.BusinessLogic.ShioppingCart;
using System.Text.RegularExpressions;
using System.Text;
namespace BizCart.UserControls
{
    public partial class OrderOverview : BizUserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                Sites.InitialiseSession();
                if (!IsPostBack)
                {
                    bindHtml();
                }
            }
            catch (Exception ex)
            {
                ExceptionModule.ExceptionPublish(ex, Sites.ToLong(Session["DomainID"]), Sites.ToLong(Session["UserContactID"]), Sites.ToLong(Session["SiteId"]), Request);
                ShowMessage(ex.ToString(), 1);
            }
        }
        void bindHtml()
        {
            try
            {
                string strUI = GetUserControlTemplate("OrderOverview.htm");

     

                //Credit Term Replacement
                CCommon objCommon = new CCommon();
                CItems objItems = new CItems();
                objItems.DivisionID = Sites.ToLong(Session["DivId"]);
                DataTable dtTable = default(DataTable);
                string lblCreditLimit = "";
                dtTable = objItems.GetAmountDue();
                lblBalDue.Text = Sites.ToString(Session["CurrSymbol"]) + " " + string.Format("{0:#,##0.00}", dtTable.Rows[0]["AmountDueSO"]);
                decimal RemainIngCredit = Sites.ToDecimal(dtTable.Rows[0]["RemainingCredit"]);
                if (RemainIngCredit < 0)
                {
                    RemainIngCredit = 0;
                }
                lblRemCredit.Text = Sites.ToString(Session["CurrSymbol"]) + " " + string.Format("{0:#,##0.00}", RemainIngCredit);
                lblAmtPastDue.Text = Sites.ToString(Session["CurrSymbol"]) + " " + string.Format("{0:#,##0.00}", dtTable.Rows[0]["AmountPastDuePO"]);
                lblCreditLimit = Sites.ToString(Session["CurrSymbol"]) + " " + string.Format("{0:#,##0.00}", dtTable.Rows[0]["CreditLimit"]);
                

                strUI = strUI.Replace("##TotalBalanceDueLabel##", lblBalDue.Text);
                strUI = strUI.Replace("##TotalRemainingCreditLabel##", lblRemCredit.Text);
                strUI = strUI.Replace("##TotalAmountPastDueLabel##", lblAmtPastDue.Text);
                strUI = strUI.Replace("##CreditLimit##", lblCreditLimit);

                pnlCutomizeHtml.Visible = false;
                litCutomizeHtml.Text = strUI;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

    }
}