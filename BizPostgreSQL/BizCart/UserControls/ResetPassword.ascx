﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ResetPassword.ascx.cs" Inherits="BizCart.UserControls.ResetPassword" %>
<script type="text/javascript" language="javascript">
    function Save() {
        var txtNewPassword = document.getElementById('<%= txtNewPassword.ClientID %>');
        var txtNewPasswordConfirm = document.getElementById('<%= txtNewPasswordConfirm.ClientID %>');

        if (txtNewPassword.value == "") {
            alert("Please enter new password.");
            txtNewPassword.focus();
            return false;
        }
        else if (txtNewPasswordConfirm.value == "") {
            alert("Please confirm new password.");
            txtNewPasswordConfirm.focus();
            return false;
        }
        else if (txtNewPassword.value != txtNewPasswordConfirm.value) {
            alert("New and Confirm passwords are not the same.");
            return false;
        }
        return true;
    }
</script>
<asp:Panel ID="pnlCutomizeHtml" runat="server">
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td colspan="2">
                <div class="sectionheader">
                    <asp:Label ID="lblTitle" runat="server" Text="Reset Password"></asp:Label>
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="lblNewPassword" runat="server" Text="New Password"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txtNewPassword" runat="server" TextMode="Password" CssClass="textbox"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="lblNewPasswordConfirm" runat="server" Text="Confirm Password"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txtNewPasswordConfirm" runat="server" TextMode="Password" CssClass="textbox"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td>
                <asp:Button ID="btnSave" runat="server" Text="Save" OnClientClick="javascript:return Save();"
                    CssClass="button" OnClick="btnSave_Click" />
            </td>
        </tr>
    </table>
</asp:Panel>
<asp:Literal ID="litCutomizeHtml" runat="server"></asp:Literal>