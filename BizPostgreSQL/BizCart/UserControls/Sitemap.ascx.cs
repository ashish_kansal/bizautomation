﻿using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using BACRM.BusinessLogic.Item;
using System.Data;
using BACRM.BusinessLogic.Admin;
using BACRM.BusinessLogic.Common;
using BACRM.BusinessLogic.ShioppingCart;
using Telerik.Web.UI;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Configuration;

namespace BizCart.UserControls
{
    public partial class Sitemap : BizUserControl
    {
        #region Global Declaration

        public string PreserveStateofTree { get; set; }
        public string ShowOnlyParentCategoriesinTree { get; set; }
        StringBuilder strBuilMain = new StringBuilder();
        HtmlGenericControl div = new HtmlGenericControl("div");

        #endregion

        #region Member Variables

        private string strUI = "";

        private string childHtml;
        private string childContainer;
        private string parentHtml;
        private string parentContainer;

        private string parentContainerStart;
        private string parentContainerEnd;
        private string childContainerStart;
        private string childContainerEnd;
        private string parentCategoryStart;
        private string parentCategoryEnd;
        private string childCategoryStart;
        private string childCategoryEnd;

        private string strCategory;
        #endregion
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                Sites.InitialiseSession();
                strUI = GetUserControlTemplate("Sitemap.htm");
                //if (Session["TreeViewState"] != null)
                //    TreeViewCategory.ExpandDepth = -1;
                //PopulateCategories();

                if (strUI.Contains("<##TreeViewCustom##>"))
                {
                    //Replace default implementation
                    strUI = strUI.Replace("##TreeView##", "");
                    LoadCustomTreeView();
                }
                else
                {
                    LoadCategories();
                }

                bindHtml();

            }
            catch (Exception ex)
            {
                ExceptionModule.ExceptionPublish(ex, Sites.ToLong(Session["DomainID"]), Sites.ToLong(Session["UserContactID"]), Sites.ToLong(Session["SiteId"]), Request);
                ShowMessage(ex.ToString(), 1);
            }
        }


        #region HTML
        private void bindHtml()
        {
            try
            {
                Sites objSite = new Sites();
                DataTable dtPages = new DataTable();
                objSite.PageID = 0;
                objSite.DomainID = Sites.ToLong(Session["DomainID"]);
                objSite.SiteID = Sites.ToLong(Session["SiteID"]);
                dtPages = objSite.GetPages().Tables[0];
                string pageSitemap = "<ul class=\"pageSitemap\">";
                foreach (DataRow dr in dtPages.Rows)
                {
                    pageSitemap = pageSitemap + "<li><a href=\"" + dr["vcPageURL"] + "\">" + dr["vcPageTitle"] + "</a></li>";
                }
                pageSitemap = pageSitemap + "</ul>";
                strUI = strUI.Replace("##Menu##", CCommon.RenderControl(phMenu));
                strUI = strUI.Replace("##PageSitemap##", pageSitemap);

                if (strUI.Contains("##TreeViewCustom##"))
                {
                    strUI = strUI.Replace("##TreeViewCustom##", strCategory);
                }
                else
                {
                    strUI = strUI.Replace("##TreeView##", CCommon.RenderControl(div));
                }
               
                litCutomizeHtml.Text = strUI;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void LoadCategories()
        {
            try
            {
                CItems objItems = new CItems();
                DataTable dt = null;
                objItems.byteMode = 13;
                objItems.SiteID = Sites.ToLong(Session["SiteID"]);

                dt = objItems.SeleDelCategory();

                if (dt.Rows.Count > 0)
                {
                    string EndUL = "</ul>";
                    string StartLi = "<li>";
                    string EndLi = "</li>";
                    string startSpanText = "<span>text";
                    string EndSpanText = "</span>";
                    string startSpanValue = "<span id=\"value\">";
                    string EndSpanValue = "</span>";

                    int intCnt = 0;

                    DataRow[] drLevel = dt.Select("category = 0");

                    string strLink = "";

                    foreach (DataRow dr in drLevel)
                    {
                        intCnt = intCnt + 1;
                        HtmlGenericControl li = new HtmlGenericControl("li");

                        if (intCnt == 1)
                        {
                            strBuilMain.Append("<ul id=\"treeData\" >");
                        }

                        DataRow[] drSubCategories = dt.Select("Category = " + CCommon.ToLong(dr["numCategoryID"]));

                        if (dr["Type"].ToString() == "0")
                        {
                            strLink = "/SubCategory/" + Sites.GenerateSEOFriendlyURL(dr["vcCategoryName"].ToString()) + "/" + dr["numCategoryID"].ToString();
                        }
                        else if (dr["Type"].ToString() == "1")
                        {
                            strLink = dr["vcLink"].ToString();
                        }
                        else if (dr["Type"].ToString() == "2")
                        {
                            strLink = "/Product.aspx?ItemID=" + dr["numCategoryID"].ToString();
                        }

                        if (drSubCategories.Length > 0)
                        {
                            bool IsParent = true;

                            strBuilMain.Append(StartLi + startSpanText.Replace("text", "<a onclick=\"openLink('" + strLink + "')\">" + dr["vcCategoryName"].ToString() + "</a>") + EndSpanText + startSpanValue.Replace("value", dr["numCategoryID"].ToString()) + EndSpanValue);

                            PopulateNode(drSubCategories, IsParent, dt);

                            strBuilMain.Append(EndUL);
                            strBuilMain.Append(EndLi);

                        }
                        else
                        {
                            if (dr["Type"].ToString() == "0")
                            {
                                strLink = "/SubCategory/" + Sites.GenerateSEOFriendlyURL(dr["vcCategoryName"].ToString()) + "/" + dr["numCategoryID"].ToString();
                            }
                            else if (dr["Type"].ToString() == "1")
                            {
                                strLink = dr["vcLink"].ToString();
                            }
                            else if (dr["Type"].ToString() == "2")
                            {
                                strLink = "/Product.aspx?ItemID=" + dr["numCategoryID"].ToString();
                            }

                            strBuilMain.Append(StartLi + startSpanText.Replace("text", "<a onclick=\"openLink('" + strLink + "')\">" + dr["vcCategoryName"].ToString() + "</a>") + EndSpanText + startSpanValue.Replace("value", dr["numCategoryID"].ToString()) + EndSpanValue + EndLi);
                        }
                    }
                    strBuilMain.Append(EndUL);

                    Literal litULCollection = new Literal();
                    litULCollection.Text = CCommon.ToString(strBuilMain);
                    div.ID = "tree";
                    div.Controls.Add(litULCollection);
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        private void PopulateNode(DataRow[] dr, bool IsParent, DataTable dtMain)
        {

            try
            {
                string StartUL = "<ul>";
                string EndUL = "</ul>";
                string StartLi = "<li>";
                string EndLi = "</li>";
                string startSpanText = "<span>text";
                string EndSpanText = "</span>";
                string startSpanValue = "<span id=\"value\">";
                string EndSpanValue = "</span>";
                bool IsFromInnerMethod = false;

                string strLink = "";

                foreach (DataRow drSub in dr)
                {

                    if (dtMain.Select("Category = " + CCommon.ToLong(drSub["numCategoryID"])).Length > 0)
                    {
                        if (drSub["Type"].ToString() == "0")
                        {
                            strLink = "/SubCategory/" + Sites.GenerateSEOFriendlyURL(drSub["vcCategoryName"].ToString()) + "/" + drSub["numCategoryID"].ToString();
                        }
                        else if (drSub["Type"].ToString() == "1")
                        {
                            strLink = drSub["vcLink"].ToString();
                        }
                        else if (drSub["Type"].ToString() == "2")
                        {
                            strLink = "/Product.aspx?ItemID=" + drSub["numCategoryID"].ToString();
                        }

                        if (IsParent == true)
                        {
                            strBuilMain.Append(StartUL);
                        }

                        strBuilMain.Append(StartLi + startSpanText.Replace("text", "<a onclick=\"openLink('" + strLink + "')\">" + drSub["vcCategoryName"].ToString() + " </a>") + EndSpanText + startSpanValue.Replace("value", drSub["numCategoryID"].ToString()) + EndSpanValue);

                        DataRow[] drSubSub = dtMain.Select("Category = " + CCommon.ToLong(drSub["numCategoryID"]));

                        if (drSubSub.Length > 0)
                        {
                            IsParent = true;
                        }
                        else
                        {
                            IsParent = false;
                        }

                        IsFromInnerMethod = true;
                        PopulateNode(drSubSub, IsParent, dtMain);

                        if (IsParent == true)
                        {
                            strBuilMain.Append(EndUL);
                            strBuilMain.Append(EndLi);
                        }


                    }
                    else
                    {
                        if (drSub["Type"].ToString() == "0")
                        {
                            strLink = "/SubCategory/" + Sites.GenerateSEOFriendlyURL(drSub["vcCategoryName"].ToString()) + "/" + drSub["numCategoryID"].ToString();
                        }
                        else if (drSub["Type"].ToString() == "1")
                        {
                            strLink = drSub["vcLink"].ToString();
                        }
                        else if (drSub["Type"].ToString() == "2")
                        {
                            strLink = "/Product.aspx?ItemID=" + drSub["numCategoryID"].ToString();
                        }

                        if (IsParent == true && IsFromInnerMethod == false)
                        {
                            strBuilMain.Append(StartUL);
                        }

                        //"<a onclick=\"openLink('" + Conversion.Str(1) + "')\">"
                        strBuilMain.Append(StartLi + startSpanText.Replace("text", "<a onclick=\"openLink('" + strLink + "')\">" + drSub["vcCategoryName"].ToString() + "</a>") + EndSpanText + startSpanValue.Replace("value", drSub["numCategoryID"].ToString()) + EndSpanValue + EndLi);

                        if (dtMain.Select("Category = " + CCommon.ToLong(drSub["numCategoryID"])).Length > 0)
                        {
                            IsParent = true;
                        }
                        else
                        {
                            IsParent = false;
                        }

                    }

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void LoadCustomTreeView()
        {
            try
            {
                string treeViewPattern = "<##TreeViewCustom##>(?<Content>([\\s\\S]*?))</##TreeViewCustom##>";
                string parentForLoopPattern = "<##FOREACH##>(?<Content>([\\s\\S]*?))</##FOREACH##>";
                string ifChildAvailablePattern = "<##IFCHILDAVAILABLE##>(?<Content>([\\s\\S]*?))</##IFCHILDAVAILABLE##>";
                string childForLoopPattern = "<##FOREACHCHILD##>(?<Content>([\\s\\S]*?))</##FOREACHCHILD##>";

                string categoryLinkStartPattern = "<##FOREACH##>(?<Content>([\\s\\S]*?))<##IFCHILDAVAILABLE##>";
                string categoryLinkEndPattern = "</##IFCHILDAVAILABLE##>(?<Content>([\\s\\S]*?))</##FOREACH##>";


                //GET PARENT CONTAINER HTML START AND END TAG
                string htmlParentContainer = Regex.Match(strUI, treeViewPattern, RegexOptions.None).Value;
                htmlParentContainer = htmlParentContainer.Replace("<##TreeViewCustom##>", "").Replace("</##TreeViewCustom##>", "");
                string parentForEach = Regex.Match(htmlParentContainer, "<##FOREACH##>(?<Content>([\\s\\S]*?))</##FOREACH##>", RegexOptions.None).Value;
                htmlParentContainer = htmlParentContainer.Replace(parentForEach, "##SEPERATOR##");

                parentContainerStart = htmlParentContainer.Substring(0, htmlParentContainer.IndexOf("##SEPERATOR##")).Trim();
                parentContainerEnd = htmlParentContainer.Substring(htmlParentContainer.IndexOf("##SEPERATOR##") + 13).Trim();


                //GET START AND END TAG OF CHILD CONTAINER
                string htmlChildContainer = Regex.Match(strUI, ifChildAvailablePattern, RegexOptions.None).Value;
                htmlChildContainer = htmlChildContainer.Replace("<##IFCHILDAVAILABLE##>", "").Replace("</##IFCHILDAVAILABLE##>", "");

                string childForEach = Regex.Match(htmlChildContainer, "<##FOREACHCHILD##>(?<Content>([\\s\\S]*?))</##FOREACHCHILD##>", RegexOptions.None).Value;
                htmlChildContainer = htmlChildContainer.Replace(childForEach, "##SEPERATOR##");

                childContainerStart = htmlChildContainer.Substring(0, htmlChildContainer.IndexOf("##SEPERATOR##")).Trim();
                childContainerEnd = htmlChildContainer.Substring(htmlChildContainer.IndexOf("##SEPERATOR##") + 13).Trim();

                //GET PARENT CATEGORY START AND END TAG OF CATEGORY LINK
                parentCategoryStart = Regex.Match(strUI, categoryLinkStartPattern, RegexOptions.None).Value;
                parentCategoryStart = parentCategoryStart.Replace("<##FOREACH##>", "").Replace("<##IFCHILDAVAILABLE##>", "").Trim();
                parentCategoryStart = Regex.Replace(parentCategoryStart, @">\s+</", "></");

                parentCategoryEnd = Regex.Match(strUI, categoryLinkEndPattern, RegexOptions.None).Value;
                parentCategoryEnd = parentCategoryEnd.Replace("</##IFCHILDAVAILABLE##>", "").Replace("</##FOREACH##>", "").Trim();
                parentCategoryEnd = Regex.Replace(parentCategoryEnd, @">\s+</", "></");

                //1. Extract html for child element
                string childForLoop = Regex.Match(strUI, childForLoopPattern, RegexOptions.None).Value;
                childHtml = childForLoop.Replace("<##FOREACHCHILD##>", "").Replace("</##FOREACHCHILD##>", "").Trim();
                childHtml = Regex.Replace(childHtml, @">\s+</", "></");
                //Remove extracted information because it is not required now
                strUI = strUI.Replace(childForLoop, "");

                //GET CHILD CATEGORY START AND END TAG OF CATEGORY LINK
                childCategoryStart = childHtml.Substring(0, childHtml.IndexOf("<##INSERTCHILDHERE##>")).Trim();
                childCategoryEnd = childHtml.Substring(childHtml.IndexOf("<##INSERTCHILDHERE##>") + 21).Trim();

                //2. Extract html for container of child element
                string ifChildAvailable = Regex.Match(strUI, ifChildAvailablePattern, RegexOptions.None).Value;
                childContainer = ifChildAvailable.Replace("<##IFCHILDAVAILABLE##>", "").Replace("</##IFCHILDAVAILABLE##>", "").Trim();
                childContainer = Regex.Replace(childContainer, @">\s+</", "></");
                //Remove extracted information because it is not required now
                strUI = strUI.Replace(ifChildAvailable, "");

                //3. Extract parent html
                string parentForLoop = Regex.Match(strUI, parentForLoopPattern, RegexOptions.None).Value;
                parentHtml = parentForLoop.Replace("<##FOREACH##>", "").Replace("</##FOREACH##>", "").Trim();
                parentHtml = Regex.Replace(parentHtml, @">\s+</", "></");
                //Remove extracted information because it is not required now
                strUI = strUI.Replace(parentForLoop, "");

                //4. Extract html for container of parent element
                string treeView = Regex.Match(strUI, treeViewPattern, RegexOptions.None).Value;
                parentContainer = treeView.Replace("<##TreeViewCustom##>", "").Replace("</##TreeViewCustom##>", "").Trim();
                parentContainer = Regex.Replace(parentContainer, @">\s+</", "></");
                //Remove extracted information because it is not required now
                strUI = Regex.Replace(strUI, treeViewPattern, "##TreeViewCustom##");

                strCategory = parentContainerStart;

                CItems objItems = new CItems();
                DataTable dt = null;
                objItems.byteMode = 13;
                objItems.SiteID = Sites.ToLong(Session["SiteID"]);

                dt = objItems.SeleDelCategory();

                if (dt.Rows.Count > 0)
                {
                    int parentID = 0;
                    DataRow[] arrDataRow = dt.Select("category = 0");

                    foreach (DataRow drCatgeory in arrDataRow)
                    {
                        string parentCategory = HttpUtility.HtmlDecode(parentCategoryStart.Replace("##CategoryName##", CCommon.ToString(drCatgeory["vcCategoryName"])).Replace("##CategoryID##", CCommon.ToString(drCatgeory["numCategoryID"])).Replace("##ITEMID##", "item-" + parentID.ToString()));
                        DataRow[] arrSubCategory = dt.Select("Category=" + CCommon.ToString(drCatgeory["numCategoryID"]));

                        if (arrSubCategory.Length > 0)
                        {
                            LoadSubCategory(dt, drCatgeory, "item-" + parentID.ToString(), ref parentCategory);
                        }

                        parentID = parentID + 1;
                        strCategory = strCategory + parentCategory + parentCategoryEnd;
                    }
                }

                strCategory = strCategory + parentContainerEnd;
            }
            catch
            {
                throw;
            }
        }

        public int LoadSubCategory(DataTable dt, DataRow drParentCategory, string id, ref string parentCategory)
        {
            DataRow[] arrSubCategory = dt.Select("Category=" + CCommon.ToString(drParentCategory["numCategoryID"]));

            if (arrSubCategory != null && arrSubCategory.Length > 0)
            {
                int parentID = 0;
                parentCategory = parentCategory + childContainerStart;

                foreach (DataRow drSubCategory in arrSubCategory)
                {
                    parentCategory = parentCategory + HttpUtility.HtmlDecode(childCategoryStart.Replace("##CategoryName##", CCommon.ToString(drSubCategory["vcCategoryName"])).Replace("##CategoryID##", CCommon.ToString(drSubCategory["numCategoryID"])).Replace("##ITEMID##", id + "-" + parentID.ToString()));

                    DataRow[] arrChildCategory = dt.Select("Category=" + CCommon.ToString(drSubCategory["numCategoryID"]));

                    if (arrChildCategory != null && arrChildCategory.Length > 0)
                    {
                        LoadSubCategory(dt, drSubCategory, id + "-" + parentID.ToString(), ref parentCategory);
                    }

                    parentID = parentID + 1;
                    parentCategory = parentCategory + childCategoryEnd;
                }


                parentCategory = parentCategory + childContainerEnd;
            }

            return 0;
        }

        

        #endregion
        public string GetUserControlTemplate(string DefaultTemplateFileName)
        {
            try
            {
                if (HtmlCustomize != null && HtmlCustomize.Length > 0)
                {
                    return HttpUtility.HtmlDecode(HtmlCustomize);
                }
                else
                {
                    return Server.HtmlDecode(Sites.ReadFile(ConfigurationManager.AppSettings["CartLocation"].ToString() + "\\Default\\Elements\\" + DefaultTemplateFileName));
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}