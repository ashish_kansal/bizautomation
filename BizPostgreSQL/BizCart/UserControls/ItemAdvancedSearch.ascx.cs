﻿using System;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BACRM.BusinessLogic.Alerts;
using BACRM.BusinessLogic.Documents;
using BACRM.BusinessLogic.Common;
using BACRM.BusinessLogic.Prospects;
using BACRM.BusinessLogic.Contacts;
using BACRM.BusinessLogic.Leads;
using BACRM.BusinessLogic.Admin;
using BACRM.BusinessLogic.ShioppingCart;
using BACRM.BusinessLogic;
using System.Configuration;

namespace BizCart.UserControls
{
    public partial class ItemAdvancedSearch : BizUserControl
    {
        long numDomainId = 0;
        DataTable dtGenericFormConfig = default(DataTable);

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                Sites.InitialiseSession();
                numDomainId = Sites.ToLong(Session["DomainID"]);
                callFuncForFormGeneration();
            }
            catch (Exception ex)
            {
                ExceptionModule.ExceptionPublish(ex, Sites.ToLong(Session["DomainID"]), Sites.ToLong(Session["UserContactID"]), Sites.ToLong(Session["SiteId"]), Request);
                ShowMessage(ex.ToString(), 1);
            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                //validate if index exist? 
                if (!System.IO.Directory.Exists(GetIndexPath()))
                {
                    ShowMessage(GetErrorMessage("ERR042"), 1);//Search is not configured, Please configure it from e-commerce settings.
                    return;
                }

                string ActualSearchString = "";
                ActualSearchString = SearchWithingIndex(true);

                if (CCommon.ToString(Session["AdvancedSearch"]).Trim().Length == 0)
                    ActualSearchString = SearchWithingIndex(false);

                if (CCommon.ToString(System.Configuration.ConfigurationManager.AppSettings["IsDebugMode"]).ToLower() == "true")
                    Response.Write(ActualSearchString);

                Response.Redirect(Sites.ToString(Session["SitePath"]) + "/AdvancedSearch/1");
            }
            catch (Exception ex)
            {
                ExceptionModule.ExceptionPublish(ex, Sites.ToLong(Session["DomainID"]), Sites.ToLong(Session["UserContactID"]), Sites.ToLong(Session["SiteId"]), Request);
                ShowMessage(ex.ToString(), 1);
            }
        }

        private string SearchWithingIndex(bool SearchExactMatch)
        {
            try
            {
                if (!dtGenericFormConfig.Columns.Contains("vcDbColumnText"))
                    dtGenericFormConfig.Columns.Add("vcDbColumnText");
                string vcAssociatedControlType = null;
                string vcFieldType = null;
                string vcFieldID = null;

                string search = "";
                foreach (DataRow dr in dtGenericFormConfig.Rows)
                {
                    vcFieldType = dr["vcFieldType"].ToString();
                    vcFieldID = dr["numFormFieldID"].ToString().Replace("C", "").Replace("D", "").Replace("R", "");

                    vcAssociatedControlType = dr["vcAssociatedControlType"].ToString();

                    switch (vcAssociatedControlType)
                    {

                        case "TextBox":
                        case "TextArea":
                            dr["vcDbColumnText"] = SearchLucene.LuceneEscapeSpecialChar(((TextBox)plhFormControls.FindControl(dr["vcDbColumnName"].ToString())).Text.Trim(), SearchExactMatch);
                            dr["vcFormFieldName"] = dr["vcFormFieldName"].ToString().Replace(" ", "");
                            if (Sites.ToString(dr["vcDbColumnText"]).Trim().Length > 1)
                            {
                                if (vcFieldType == "R")
                                {
                                    search = search + dr["vcFormFieldName"].ToString() + ":(" + dr["vcDbColumnText"].ToString() + ") ";
                                }
                                else if (vcFieldType == "C")
                                {
                                    search = search + dr["vcFormFieldName"].ToString() + ":(" + dr["vcDbColumnText"].ToString() + ") ";
                                }
                            }

                            break;
                        case "SelectBox":
                            DropDownList ddl = ((DropDownList)plhFormControls.FindControl(dr["vcDbColumnName"].ToString()));
                            if (ddl.SelectedIndex > 0)
                            {
                                dr["vcDbColumnText"] = SearchLucene.LuceneEscapeSpecialChar(ddl.SelectedItem.Text, SearchExactMatch);
                                dr["vcFormFieldName"] = dr["vcFormFieldName"].ToString().Replace(" ", "");
                                if (!string.IsNullOrEmpty(dr["vcDbColumnText"].ToString()))
                                {
                                    if (vcFieldType == "R")
                                    {
                                        search = search + dr["vcFormFieldName"].ToString() + ":(" + dr["vcDbColumnText"].ToString() + ") ";
                                    }
                                    else if (vcFieldType == "C")
                                    {
                                        search = search + dr["vcFormFieldName"].ToString() + ":(" + dr["vcDbColumnText"].ToString() + ") ";
                                    }
                                }
                            }
                            break;
                    }
                }
                if (search.Length > 0)
                {
                    string ActualSearchString = "";
                    Session["AdvancedSearch"] = SearchLucene.SearchIndex(GetIndexPath(), search, IndexType.Items, true, ref ActualSearchString);
                    return ActualSearchString;
                }
                return "";
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void callFuncForFormGeneration()
        {
            try
            {
                GenerateGenericFormControls.FormID = 30;
                //set the form id to Order Form
                GenerateGenericFormControls.DomainID = numDomainId;
                //Set the domain id
                GenerateGenericFormControls.sXMLFilePath = ConfigurationManager.AppSettings["PortalLocation"] + "\\Documents\\Docs\\";
                //set the file path
                GenerateGenericFormControls.boolAOIField = 0;
                //Set the AOI flag to non AOI
                dtGenericFormConfig = GenerateGenericFormControls.getFormControlConfig();
                //Declare and pupulate a Datatable to store the form config
                if (dtGenericFormConfig.Rows.Count == 0)
                {
                    //if the xml for form fields has not been configured
                    btnAdvanceSearch.Enabled = false;
                    //Disable any attempt to save/ submit
                    plhFormControls.Controls.Add(new LiteralControl("The screen is not created. Please use BizForm Wizard to create this screen."));
                    //calls to create the form controls and add it to the form
                    //and exit the flow
                    return;
                }
                plhFormControls.Controls.Add(GenerateGenericFormControls.createFormControls(dtGenericFormConfig));
                //calls to create the form controls and add it to the form

                foreach (DataRow dr in dtGenericFormConfig.Rows)
                {
                    if (plhFormControls.FindControl(CCommon.ToString(dr["vcDbColumnName"])) != null)
                    {
                        ((WebControl)plhFormControls.FindControl(CCommon.ToString(dr["vcDbColumnName"]))).Attributes.Add("onkeydown", "SearchOnKeyDown(event);");
                    }
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}