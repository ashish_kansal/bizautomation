﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ShippingStatus.ascx.cs" Inherits="BizCart.UserControls.ShippingStatus"  %>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" crossorigin="anonymous">
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" crossorigin="anonymous"></script>
<script>
    function OpenShippingReport(shippingreportId, bizdocId, OppID) {
        this.window.close();
        window.open('fulfilment.aspx?shipmentreportId=' + shippingreportId + '&OppBizDocID=' + bizdocId + '&OppID=' + OppID + '', '', 'toolbar=no,titlebar=no,top=10,left=100,width=1000,height=800,scrollbars=yes,resizable=yes')
        return false;
    }
</script>
<asp:literal runat="server" id="lblHtmlControl"></asp:literal>
<asp:panel runat="server" id="pnlCustomize">
<div class="container" style="margin-top:10px">
  <div class="panel panel-default">
    <div class="panel-heading">Tracking Information</div>
    <div class="panel-body">
        <table class="table table-responsive" style="border:0px;">
            <tr>
                <td>STATUS</td>
                <td>
                    <asp:label runat="server" ID="lblStatus" text="Delivered"></asp:label>
                </td>
            </tr>
            <tr>
                <td>LOCATION</td>
                <td>
                    <asp:label runat="server" ID="lblLocation" text="Delivered"></asp:label>
                </td>
            </tr>
            <tr>
                <td>SHIP CARRIER</td>
                <td>
                    <asp:label runat="server" ID="lblShipCarrier" text="Delivered"></asp:label>
                </td>
            </tr>
            <tr>
                <td>TRACKING ID</td>
                <td>
                    <asp:label runat="server" ID="lblTrackingId" text="Delivered"></asp:label>
                </td>
            </tr>
            <tr>
                <td>LATEST EVENT</td>
                <td>
                    <asp:label runat="server" ID="lblLatestEvent" text="Delivered"></asp:label>
                </td>
            </tr>
            <tr>
                <td>SHIPMENT UPDATES VIA TEXT</td>
                <td>
                    <asp:label runat="server" ID="lblShipmentText" text="Delivered"></asp:label>
                </td>
            </tr>
            
        </table>
    </div>
  </div>
</div>
    </asp:panel>