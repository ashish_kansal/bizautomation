﻿using System;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BACRM.BusinessLogic.Item;
using BACRM.BusinessLogic.ShioppingCart;
using BACRM.BusinessLogic.Common;
using System.Text.RegularExpressions;
using System.Text;
using System.Collections;

namespace BizCart.UserControls
{
    public partial class ReviewList : BizUserControl
    {
        #region Global Declaration
        QueryStringValues objEncryption = new QueryStringValues();
        #endregion

        #region events
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                Sites.InitialiseSession();
                if (!IsPostBack)
                {
                    hfContactId.Value = objEncryption.Encrypt(CCommon.ToString(Session["ContactId"]));

                    if (CCommon.ToLong(Request.QueryString["ItemId"]) > 0)
                    {
                        CItems objItems = new CItems();
                        objItems.DomainID = CCommon.ToLong(Session["DomainId"]);
                        objItems.ItemCode = CCommon.ToInteger(Request.QueryString["ItemId"]);
                        DataTable dt = objItems.ValidateItemDomain();

                        if (dt != null && dt.Rows.Count > 0 && CCommon.ToBool(dt.Rows[0]["IsItemInDomain"]))
                        {
                            bindHtml();
                        }
                        else
                        {
                            pnlCutomizeHtml.Visible = false;
                            litCutomizeHtml.Text = "";
                        }
                    }
                    else
                    {
                        pnlCutomizeHtml.Visible = false;
                        litCutomizeHtml.Text = "";
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionModule.ExceptionPublish(ex, Sites.ToLong(Session["DomainID"]), Sites.ToLong(Session["UserContactID"]), Sites.ToLong(Session["SiteId"]), Request);
                ShowMessage(ex.ToString(), 1);
            }
        }
        protected void ddlSortReview_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                bindHtml();
            }
            catch (Exception ex)
            {
                ExceptionModule.ExceptionPublish(ex, Sites.ToLong(Session["DomainID"]), Sites.ToLong(Session["UserContactID"]), Sites.ToLong(Session["SiteId"]), Request);
                ShowMessage(ex.ToString(), 1);
            }
        }

        #endregion

        #region Methods

        #region HTML
        
        private void bindHtml()
        {
            try
            {
                //No Review found html

                string strUI = ""; 
               // string strDelete = CCommon.ToString(Request.QueryString["ItemId"]);

                DataTable dtReview = null ;
                
                Review objReview = new Review();
                objReview.ReviewId = 0; 
                objReview.SiteId = CCommon.ToLong(Session["SiteId"]);
                objReview.DomainID = CCommon.ToLong(Session["DomainId"]);
                objReview.ReferenceId = CCommon.ToString(Request.QueryString["ItemId"]);
                objReview.TypeId = 2;
                objReview.Mode = 1;
                objReview.SearchMode = CCommon.ToInteger(ddlSortReview.SelectedValue);
	            
                objReview.ContactId = CCommon.ToLong(Session["ContactId"]);
                dtReview =  objReview.GetReviews();

                if (dtReview.Rows.Count > 0)
                {
                    strUI = GetUserControlTemplate("ReviewList.htm");
                    strUI = strUI.Replace("##ReadReviews##","<a name=\"read-reviews\"></a>");
                    strUI = strUI.Replace("##ReadReviews##", "");
                    StringBuilder sb = new StringBuilder();
                    string pattern = "<##FOR##>(?<Content>([\\s\\S]*?))<##ENDFOR##>";
                    string flag_template = "";
                    string template = "";
                    if (Regex.IsMatch(strUI, pattern))
                    {
                        MatchCollection matches = Regex.Matches(strUI, pattern);
                        flag_template = matches[0].Groups["Content"].Value;
                    }

                    if (dtReview.Rows.Count > 0)
                    {
                        foreach (DataRow drReview in dtReview.Rows)
                        {
                            template = flag_template;
                            template = template.Replace("##Title##", CCommon.ToString(drReview["vcReviewTitle"]));
                            template = template.Replace("##Review##", CCommon.ToString(drReview["vcReviewComment"]));
                            template = template.Replace("##UserName##", CCommon.ToString(drReview["vcContactName"]));
                            template = template.Replace("##CreatedDate##", CCommon.ToString(Convert.ToDateTime(drReview["dtCreatedDate"]).ToShortDateString()));
                            template = template.Replace("##ReviewId##",objEncryption.Encrypt(CCommon.ToString(Convert.ToString(drReview["numReviewId"]))));
                            template = template.Replace("##TotalVote##",  CCommon.ToString(drReview["vcTotalVote"]));
                            template = template.Replace("##HelpfulVote##", CCommon.ToString(drReview["vcHelpfulVote"]));


                            template = template.Replace("##Permalink##", "<a href=\"/PermaLink.aspx?ReviewId=" + objEncryption.Encrypt(CCommon.ToString(drReview["numReviewId"])) + "\">Permalink</a>");

                                if ((CheckForOwnReview(Convert.ToInt32(drReview["numContactId"])) == true) || Sites.ToLong(Session["UserContactID"]) == 0)
                                {
                                    template = template.Replace("##ReportAbuse##", "");
                                    template = template.Replace("##Saperator##", "");
                                    template = template.Replace("##ReviewHelpfulLabel##", "");
                                    template = template.Replace("##HelpfulYes##", "");
                                    template = template.Replace("##HelpfulSaperator##", "");
                                    template = template.Replace("##HelpfulNo##", "");
                                }
                                else
                                {
                                    template = template.Replace("##ReportAbuse##", "<a id = \"ReportAbuse\" rid=" + objEncryption.Encrypt(CCommon.ToString(drReview["numReviewId"])) + ">Report Abuse</a>");
                                    template = template.Replace("##Saperator##", "|");
                                    template = template.Replace("##ReviewHelpfulLabel##", "was this review helpful ?");
                                    template = template.Replace("##HelpfulYes##", "<a id = \"HelpfulYes\" rid=" + objEncryption.Encrypt(CCommon.ToString(drReview["numReviewId"])) + "><font color=\"navy\"><b>Yes</b></font> </a>");
                                    template = template.Replace("##HelpfulSaperator##", "/");
                                    template = template.Replace("##HelpfulNo##", "<a id = \"HelpfulNo\" rid=" + objEncryption.Encrypt(CCommon.ToString(drReview["numReviewId"])) + "><font color=\"navy\"><b>No</b></font> </a>");
                                    //template = template.Replace("##HelpfulNo##", "<a onclick=\"javascript: ManageReviewDetail('" + Convert.ToString(drReview["numReviewId"]) + "','" + CCommon.ToString(Session["ContactId"]) + "','1', 'false');\"><font color=\"navy\"><b>No</b></font> </a>");
                                }
                            sb.Append(template);
                        }
                    }
                    //<a><font color="navy"><b>Yes</b></font> </a> /<a><font color="navy"><b>No</b></font> </a> </b>
                    strUI = Regex.Replace(strUI, pattern, sb.ToString());
                    strUI = strUI.Replace("##SortReview##", CCommon.RenderControl(ddlSortReview));     
                    
                   
                     
                    
                }
                pnlCutomizeHtml.Visible = false;
                litCutomizeHtml.Text = strUI;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        
        #endregion

        Boolean CheckForOwnReview(long ContactId)
        {
            try
            {
                long LogInContactId = CCommon.ToLong(Session["ContactId"]);
                if (LogInContactId == ContactId)
                {
                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        
        #endregion

        #region Properties
        //public Boolean reFillDropdown
        //{
        //    get
        //    {
        //        object o = ViewState["reFillDropdown"];
        //        if (o != null)
        //        {
        //            return (Boolean)o;
        //        }
        //        else
        //        {
        //            return true;
        //        }
        //    }
        //    set
        //    {
        //        ViewState["reFillDropdown"] = value;
        //    }
        //}
        #endregion

    }
}