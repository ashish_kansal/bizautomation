﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Account.ascx.cs" Inherits="BizCart.UserControls.Account" %>
<asp:Literal ID="litCutomizeHtml" runat="server"></asp:Literal>
<asp:Panel ID="pnlCutomizeHtml" runat="server">
     <table>
            <tr>
                <td>
                    <b>Orders</b>
                    <br />
                    See & Modify Recent Orders
                    <br />
                    <img src="../Default/images/orders.png" style="height: 108px; width: 133px" />
                </td>
                <td valign="top">
                    <table width="100%">
                        <tr>
                            <td>
                                View,Modify,Track or Cancel an Order
                                <br />
                                <center>
                                </center>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top" class="style1">
                               <b> Order History</b>
                                <br />
                             <a href="/Orders.aspx?flag=true">View Open Orders</a><br />
                             <a href="/Orders.aspx?flag=false">View Order History</a><br />
                            </td>
                            <td valign="top"><b>Wish List</b><br />
                            <a href="/Lists.aspx">View List</a><br />
                            <a href="/lists.aspx?action=add">Create List</a>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <b>Payments</b>
                    <br />
                    Credit Cards & Gift Cards<br />
                    <img src="../Default/images/payment.png" style="height: 108px; width: 133px" />
                </td>
                <td valign="top">
                     <table width="100%">
                        <tr>
                            <td valign="top" class="style2">
                                <b>Payment Methods</b><br />
                             <a href="/AddPayment.aspx">Add Credit or Debit Cart</a><br />
                             &nbsp;
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                   <b>Settings</b><br />
                   Password,Prime & Email<br />
                   <img src="../Default/images/settings.png" style="height: 108px; width: 133px" />
                   </td>
                <td valign="top">
                <table width="100%">
                <tr>
                <td valign="top">
                <b>Account Settings</b><br />
                 <a href="/ChangePassword.aspx">Change Password</a><br />
                <a href="/ConfirmAddress.aspx">Update Profile</a>
                </td>
                <td valign="top">
                <b>Address Book</b><br />
               <a href="/CustomerAddress.aspx?AType=Bill">Add Billing Address</a> <br /> 
               <a href="/CustomerAddress.aspx?AType=Ship">Add Shipping Address</a><br />
                </td>
                <td valign="top">
                <b>E-mail from </b><br />
              <a href="/Unsubscribe.aspx">Email Preference Notification</a> <br />
                
                </td>
                </tr>
                </table>
                    </td>
            </tr>
        </table>
</asp:Panel>

