﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EstimateShipping.ascx.cs" ClientIDMode="Static"
    Inherits="BizCart.UserControls.EstimateShipping" %>
<script type="text/javascript">
    function EstimateShipping() {
        if (document.getElementById('ddlCountry').value == "0") {
            alert("Please select country.");
            return false;
        }
        if (document.getElementById('ddlState').value == "0") {
            alert("Please select state.");
            return false;
        }
        if (document.getElementById('txtZipCode').value == "") {
            alert("Please enter zipcode.");
            return false;
        }
        return true;
    }
</script>
<asp:Panel ID="pnlCutomizeHtml" runat="server">
    <div id="dvShipping">
        <table border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td>
                    * Country:
                </td>
                <td>
                    <asp:DropDownList ID="ddlCountry" runat="server" AutoPostBack="True" CssClass="signup"
                        Width="200" onselectedindexchanged="ddlCountry_SelectedIndexChanged">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    * State/Province:
                </td>
                <td>
                    <asp:DropDownList ID="ddlState" runat="server" Width="200" CssClass="signup">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    Zip/Postcode:
                </td>
                <td>
                    <asp:TextBox runat="server" ID="txtZipCode" CssClass="signup" />
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td>
                    <asp:Button Text="Estimate Shipping" runat="server" ID="btnEstimate" CssClass="button"
                        OnClick="btnEstimate_Click" OnClientClick="return EstimateShipping();" />
                </td>
            </tr>
            <tr id="trShippingMethod" runat="server" visible="false">
                <td>
                    Shipping Method:
                </td>
                <td>
                    <asp:DropDownList runat="server" ID="ddlShippingMethod" CssClass="signup">
                    </asp:DropDownList>
                    Note:Actuall shipping rate will be calculated during checkout
                </td>
            </tr>
        </table>
    </div>
</asp:Panel>
<asp:Literal ID="litCutomizeHtml" runat="server"></asp:Literal>
<asp:TextBox ID="txtWeight" runat="server" Style="display: none"></asp:TextBox>
<asp:TextBox ID="txtHeight" runat="server" Style="display: none"></asp:TextBox>
<asp:TextBox ID="txtWidth" runat="server" Style="display: none"></asp:TextBox>
<asp:TextBox ID="txtLength" runat="server" Style="display: none"></asp:TextBox>