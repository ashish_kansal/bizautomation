﻿using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BACRM.BusinessLogic.ShioppingCart;
using BACRM.BusinessLogic.Item;
using BACRM.BusinessLogic.Common;
using System.Data;

namespace BizCart
{
    public partial class ItemImage : BizUserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {


                if (!IsPostBack)
                {
                    bindHtml();

                }
                Page.ClientScript.RegisterStartupScript(this.GetType(), "resize", "windowResize();", true);
            }
            catch (Exception ex)
            {
                Response.Write(ex);
            }
        }

        void bindHtml()
        {
            try
            {
                string strUI = GetUserControlTemplate("ItemImage.htm");
                long lngItemCode = 0;
                lngItemCode = Sites.ToInteger(Request["ItemCode"]);
                CItems objItems = new CItems();
                DataTable dtItemDetails = default(DataTable);
                objItems.ItemCode = (int)lngItemCode;
                dtItemDetails = objItems.ItemDetails();
                if (dtItemDetails.Rows.Count > 0)
                {
                    if (!(dtItemDetails.Rows[0]["vcPathForImage"] == System.DBNull.Value))
                    {
                        if (Sites.ToString(dtItemDetails.Rows[0]["vcPathForImage"]) != "")
                        {
                            strUI = strUI.Replace("##ItemImagePath##", GetImagePath(dtItemDetails.Rows[0]["vcPathForImage"].ToString()));
                        }
                        else
                        {
                           strUI = strUI.Replace("##ItemImagePath##","");
                        }
                    }
                    else
                    {
                        strUI = strUI.Replace("##ItemImagePath##", "");
                    }
                }
                pnlCutomizeHtml.Visible = false;
                litCutomizeHtml.Text = strUI;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}