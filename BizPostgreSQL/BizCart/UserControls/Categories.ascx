﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Categories.ascx.cs"
    Inherits="BizCart.UserControls.Categories" EnableViewState="false" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<script type="text/javascript">
    function setCookie(c_name, value, exdays) {
        var exdate = new Date();
        exdate.setDate(exdate.getDate() + exdays);
        var c_value = escape(value) + ((exdays == null) ? "" : "; expires=" + exdate.toUTCString());
        document.cookie = c_name + "=" + c_value;
    }
    function getCookie(c_name) {
        var i, x, y, ARRcookies = document.cookie.split(";");
        for (i = 0; i < ARRcookies.length; i++) {
            x = ARRcookies[i].substr(0, ARRcookies[i].indexOf("="));
            y = ARRcookies[i].substr(ARRcookies[i].indexOf("=") + 1);
            x = x.replace(/^\s+|\s+$/g, "");
            if (x == c_name) {
                return unescape(y);
            }
        }
    }
</script>
<style type="text/css">
    #TreeViewCategory TD Div {
        height: 20px !important;
    }
</style>
<asp:Panel ID="pnlCutomizeHtml" runat="server">
    <div class="boxheader" id="divHeader" runat="server">
        <asp:Literal ID="litTitle" runat="server" Text="Browse Categories"></asp:Literal>
    </div>
    <div class="boxcontent" id="divContent" runat="server">
        <asp:PlaceHolder ID="phMenu" runat="server"></asp:PlaceHolder>
        <asp:TreeView ID="TreeViewCategory" runat="server" ShowLines="true" ClientIDMode="Static">
        </asp:TreeView>
        <%--OnTreeNodeCollapsed="TreeViewCategory_TreeNodeCollapsed"
        OnTreeNodeExpanded="TreeViewCategory_TreeNodeExpanded"--%>
    </div>
</asp:Panel>
<asp:Literal ID="litCutomizeHtml" runat="server"></asp:Literal>
