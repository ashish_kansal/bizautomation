﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ItemAdvancedSearch.ascx.cs"
    Inherits="BizCart.UserControls.ItemAdvancedSearch" ClientIDMode="Static" %>
<script type="text/javascript">
    function SearchOnKeyDown(event) {
        if (event.which || event.keyCode) {
            if ((event.which == 13) || (event.keyCode == 13)) {
                document.getElementById('btnAdvanceSearch').click(); 
            } 
        } else { return true };
    }
</script>
<table width="100%" cellpadding="0" cellspacing="0">
    <tr>
        <td>
            <div class="sectionheader">
                <asp:Label ID="lblTitle" runat="server" Text="Advanced Search"></asp:Label>
            </div>
        </td>
    </tr>
    <tr>
        <td>
            <asp:PlaceHolder ID="plhFormControls" runat="server"></asp:PlaceHolder>
        </td>
    </tr>
    <tr>
        <td >
            <asp:Button ID="btnAdvanceSearch" Text="Search" runat="server" CssClass="button"
                OnClick="btnSearch_Click"></asp:Button>
        </td>
    </tr>
</table>
