﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BACRM.BusinessLogic.Common;
using BACRM.BusinessLogic.ShioppingCart;
using System.Text.RegularExpressions;
namespace BizCart.UserControls
{
    public partial class LogInStatus : BizUserControl
    {

        public string LogOutLandingPageUrl { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                string url = Request.Url.ToString();
                string path = url.Substring(0, url.LastIndexOf("/") + 1);


                if (IsPostBack == true)
                {
                    if (Sites.ToString(Request["__EVENTTARGET"]) == "LogoutPostback")
                    {
                        LogOut_Click();
                    }
                    bindHtml();
                }
                if (!IsPostBack)
                    bindHtml();
            }
            catch (Exception ex)
            {
                ExceptionModule.ExceptionPublish(ex, Sites.ToInteger(Session["DomainID"]), Sites.ToInteger(Session["UserContactID"]), Sites.ToInteger(Session["SiteId"]), Request);
                ShowMessage(ex.ToString(), 1);
            }
        }



        protected void LogOut_Click()
        {
            try
            {
                string url = Request.Url.ToString();
                string path = url.Substring(0, url.LastIndexOf("/") + 1);
                System.Web.Security.FormsAuthentication.SignOut();
                Session.Abandon();
                //Toggle();
                if (LogOutLandingPageUrl.Length > 0)
                    Response.Redirect(path + LogOutLandingPageUrl, false);
                else
                    Response.Redirect("/Login.aspx", false);
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }
            catch (Exception ex)
            {
                ExceptionModule.ExceptionPublish(ex, Sites.ToInteger(Session["DomainID"]), Sites.ToInteger(Session["UserContactID"]), Sites.ToInteger(Session["SiteId"]), Request);
                ShowMessage(ex.ToString(), 1);
            }
        }


        void bindHtml()
        {
            try
            {
                string strUI = GetUserControlTemplate("LoginStatus.htm");


                string LoginPattern, LogOutPattern;
                LoginPattern = "{##LoginSection##}(?<Content>([\\s\\S]*?)){/##LoginSection##}";
                LogOutPattern = "{##LogoutSection##}(?<Content>([\\s\\S]*?)){/##LogoutSection##}";
                if (System.Web.HttpContext.Current.User.Identity.IsAuthenticated && CCommon.ToLong(Session["DomainID"]) > 0 && CCommon.ToLong(Session["UserContactID"]) > 0)
                {
                    //Hide Other Template
                    strUI = Sites.RemoveMatchedPattern(strUI, LoginPattern);
                    //Replace disply template ##Tags## with content value
                    strUI = Sites.ReplaceMatchedPatternWithContent(strUI, LogOutPattern);
                }
                else
                {
                    strUI = Sites.RemoveMatchedPattern(strUI, LogOutPattern);
                    strUI = Sites.ReplaceMatchedPatternWithContent(strUI, LoginPattern);
                }
                strUI = strUI.Replace("##CustomerName##", Sites.ToString(Session["ContactName"]));
                //strUI = strUI.Replace("##NoOfItemsInCart##", GetItems().Rows.Count.ToString());

                if (Session["gblItemCount"] == null || Session["gblTotalAmount"] == null || Session["IsLoadPriceAfterLogin"] != null)
                {
                    //Get Item From Table
                    DataSet ds;
                    BACRM.BusinessLogic.ShioppingCart.Cart objcart;
                    objcart = new BACRM.BusinessLogic.ShioppingCart.Cart();
                    string[] arr = new string[1];
                    ds = GetCartItem();
                    arr = objcart.GetItemTotal(ds);
                    Session["gblItemCount"] = arr[0];
                    Session["gblTotalAmount"] = arr[1];

                    strUI = strUI.Replace("##ItemCount##", Sites.ToString(Session["gblItemCount"]));

                    //if (CCommon.ToDecimal(Session["gblTotalAmount"]) > 0 )
                    //   strUI = strUI.Replace("##TotalAmount##", Sites.ToString(Session["CurrSymbol"]) + " " + string.Format("{0:#,##0.00}", CCommon.ToDecimal(Session["gblTotalAmount"])/Sites.ToDecimal(Session["ExchangeRate"])));
                    //else
                    strUI = strUI.Replace("##TotalAmount##", Sites.ToString(Session["CurrSymbol"]) + " " + string.Format("{0:#,##0.00}", CCommon.ToDecimal((Session["gblTotalAmount"]))));

                    if (HttpContext.Current.User.Identity.IsAuthenticated && Sites.ToInteger(Session["UserContactID"]) > 0)
                    {
                        Session["IsLoadPriceAfterLogin"] = null;
                    }
                }
                else
                {
                    strUI = strUI.Replace("##ItemCount##", Sites.ToString(Session["gblItemCount"]));
                    strUI = strUI.Replace("##TotalAmount##", Sites.ToString(Session["CurrSymbol"]) + " " + string.Format("{0:#,##0.00}", CCommon.ToDecimal(Session["gblTotalAmount"])));
                    //strUI = strUI.Replace("##TotalAmount##", Sites.ToString(Session["CurrSymbol"]) + " " + string.Format("{0:#,##0.00}", CCommon.ToDecimal(Session["gblTotalAmount"]) / Sites.ToDecimal(Session["ExchangeRate"])));
                }
                strUI = strUI.Replace(LoginPattern, CCommon.RenderControl(ensurePostback));


                pnlCutomizeHtml.Visible = false;
                litCutomizeHtml.Text = strUI;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            try
            {
                bindHtml();
            }
            catch (Exception ex)
            {
                ExceptionModule.ExceptionPublish(ex, Sites.ToInteger(Session["DomainID"]), Sites.ToInteger(Session["UserContactID"]), Sites.ToInteger(Session["SiteId"]), Request);
                ShowMessage(ex.ToString(), 1);
            }
        }

    }

}