﻿using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BACRM.BusinessLogic.Admin;
using BACRM.BusinessLogic.Common;
using System.Data;
using BACRM.BusinessLogic.ShioppingCart;
namespace BizCart.UserControls
{
    public partial class Login : BizUserControl
    {

        #region Global Declaration

        public string RedirecttoURLAfterSucessfullLogin { get; set; }

        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                int ErrorCode = 0;
                Sites.InitialiseSession();

                if (Session["UserContactID"] == null) Session["UserContactID"] = 0;
                if (Session["DomainID"] == null) Session["DomainID"] = 0;
                hplForgotPassword.NavigateUrl = CCommon.ToString(Session["SitePath"]) + "/ForgotPassword.aspx";
                hplSignUp.NavigateUrl = CCommon.ToString(Session["SitePath"]) + "/SignUp.aspx?ReturnURL=" + Request["ReturnURL"];

                btnLogin.Attributes.Add("onclick", "return Login('" + txtEmaillAdd.ClientID + "','" + txtPassword.ClientID + "');");
                txtPassword.Attributes.Add("onkeydown", "if(event.which || event.keyCode){if ((event.which == 13) || (event.keyCode == 13)) {document.getElementById('" + btnLogin.ClientID + "').click();return false;}} else {return true}; ");
                txtEmaillAdd.Attributes.Add("onkeydown", "if(event.which || event.keyCode){if ((event.which == 13) || (event.keyCode == 13)) {document.getElementById('" + btnLogin.ClientID + "').click();return false;}} else {return true}; ");
                
                if (!IsPostBack)
                {
                    bindHtml();
                }

                if (Sites.ToString(Request["LoggedIn"]) == "1")
                {
                    litMessage.Text = GetErrorMessage("ERR058");//You are successfully logged in.
                    tblLogin.Visible = false;
                }
                if (Request.QueryString["errcode"] != null)
                {
                    ErrorCode = CCommon.ToInteger(Request.QueryString["errcode"]);
                    if (ErrorCode == 5)
                    {
                        ShowMessage("Please Login to Continue..", 0);
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionModule.ExceptionPublish(ex, Sites.ToLong(Session["DomainID"]), Sites.ToLong(Session["UserContactID"]), Sites.ToLong(Session["SiteId"]), Request);
                ShowMessage(ex.ToString(), 1);
            }
        }
        protected void btnLogin_Click1(object sender, EventArgs e)
        {
            try
            {
                UserAccess objUserAccess = new UserAccess();
                CCommon objCommon = new CCommon();
                DataTable dttable = default(DataTable);
                objUserAccess.Email = txtEmaillAdd.Text.Trim();
                objUserAccess.Password = objCommon.Encrypt(txtPassword.Text.Trim());
                objUserAccess.DomainID = Sites.ToLong(Session["DomainID"]);
                dttable = objUserAccess.ExtranetLogin();
                Sites.InitialiseSession();
                if (dttable.Rows[0][0].ToString() == "1")
                {
                    //System.Web.Security.FormsAuthentication.SetAuthCookie(CCommon.ToString(dttable.Rows[0][13]), true);

                    System.Web.Security.FormsAuthenticationTicket tkt = new System.Web.Security.FormsAuthenticationTicket(1, CCommon.ToString(dttable.Rows[0][13]), DateTime.Now, DateTime.Now.AddMinutes(30), false, "");
                    string cookiestr = System.Web.Security.FormsAuthentication.Encrypt(tkt);
                    HttpCookie ck = new HttpCookie(System.Web.Security.FormsAuthentication.FormsCookieName, cookiestr);
                    //ck.Expires = tkt.Expiration;
                    ck.Path = System.Web.Security.FormsAuthentication.FormsCookiePath;
                    Response.Cookies.Add(ck);

                    Session["CompID"] = dttable.Rows[0][3];
                    Session["UserContactID"] = dttable.Rows[0][1];
                    Session["ContactId"] = dttable.Rows[0][1];
                    Session["DivId"] = dttable.Rows[0][2];
                    Session["GroupId"] = dttable.Rows[0][4];
                    Session["CRMType"] = dttable.Rows[0][5];
                    //Session["DomainID"] = dttable.Rows[0][6];
                    Session["CompName"] = dttable.Rows[0][7];
                    Session["RecOwner"] = dttable.Rows[0][8];
                    Session["RecOwnerName"] = dttable.Rows[0][9];
                    Session["PartnerGroup"] = dttable.Rows[0][10];
                    Session["RelationShip"] = dttable.Rows[0][11];
                    Session["Profile"] = dttable.Rows[0]["numProfileID"];
                    //Session["HomePage"] = dttable.Rows[0][12];
                    Session["UserEmail"] = dttable.Rows[0][13];
                    Session["DateFormat"] = dttable.Rows[0]["vcDateFormat"];
                    Session["ContactName"] = dttable.Rows[0][14];
                    Session["PartnerAccess"] = dttable.Rows[0][15];
                    Session["PagingRows"] = dttable.Rows[0]["tintCustomPagingRows"];
                    Session["DefCountry"] = dttable.Rows[0]["numDefCountry"];
                    Session["PopulateUserCriteria"] = dttable.Rows[0]["tintAssignToCriteria"];
                    Session["EnableIntMedPage"] = (bool.Parse(dttable.Rows[0]["bitIntmedPage"].ToString()) == true ? 1 : 0);
                    Session["DomainName"] = dttable.Rows[0]["vcDomainName"];
                    Session["FirstName"] = dttable.Rows[0]["vcFirstName"];
                    Session["LastName"] = dttable.Rows[0]["vcLastName"];
                    //' End date
                    Session["Internal"] = 1;
                    Session["Username"] = objUserAccess.Email;
                    if(objCommon == null){
                        objCommon = new CCommon();
                    }

                    Session["Password"] = objCommon.Encrypt(objUserAccess.Password);
                    Sites objSite = new Sites();
                    objSite.UpdateDefaultWarehouseForLoggedInUser(Sites.ToLong(Session["DivId"]));
                    Session["PortalURL"] = "http://" + Sites.ToString(dttable.Rows[0]["vcPortalName"]) + ".bizautomation.com/Login.aspx";




                    if (Request.Cookies["Cart"] != null && Sites.ToLong(Session["OppID"]) == 0)
                    {
                        //REMOVE OLD ITEMS ADDED TO CART
                        HttpCookie cookie = Request.Cookies["Cart"];
                        BACRM.BusinessLogic.ShioppingCart.Cart objcart;
                        objcart = new BACRM.BusinessLogic.ShioppingCart.Cart();
                        objcart.DomainID = CCommon.ToLong(Session["DomainID"]);
                        objcart.ContactId = CCommon.ToLong(Session["UserContactID"]);
                        objcart.CookieId = CCommon.ToString((cookie != null) ? cookie["KeyId"] : "");
                        objcart.ItemCode = 0;
                        objcart.SiteID = Sites.ToLong(Session["SiteId"]);
                        objcart.bitDeleteAll = true;
                        objcart.RemoveItemFromCart();

                        objCommon.Mode = 17;
                        objCommon.Comments = CCommon.ToString((cookie["KeyId"] != null) ? cookie["KeyId"] : "");
                        objCommon.UpdateValueID = CCommon.ToLong(Session["UserContactID"]);
                        objCommon.UpdateSingleFieldValue();
                    }

                    Session["gblItemCount"] = null;
                    Session["gblTotalAmount"] = null;
                    Session["IsLoadPriceAfterLogin"] = true;
                    if (Session["PreviousProductPage"] != null && !string.IsNullOrEmpty(Convert.ToString(Session["PreviousProductPage"])))
                    {
                        Response.Redirect(Session["PreviousProductPage"].ToString(),true);
                    } else if (Sites.ToString(Request["ReturnURL"]).Length > 2)
                    {
                        Response.Redirect(Server.UrlDecode(Sites.ToString(Request["ReturnURL"])),false);
                    }
                    if (Sites.ToInteger(Request["hdnOnePageCheckout"]) == 1)
                    {
                        if (Sites.ToBool(Session["SkipStep2"]) == true && IsOnlySalesInquiry() == true)
                        {
                            Session["SelectedStep"] = Sites.ToInteger(Session["SelectedStep"]) + 2;
                        }
                        else
                        {
                            Session["SelectedStep"] = Sites.ToInteger(Session["SelectedStep"]) + 1;
                        }
                        Response.Redirect("/OnePageCheckout.aspx", true);
                    }
                    if (Sites.ToString(RedirecttoURLAfterSucessfullLogin).Length > 2)
                    {
                        Response.Redirect(RedirecttoURLAfterSucessfullLogin);
                    }

                    Response.Redirect("Account.aspx",false);
                    //Response.Redirect(Request.Url.OriginalString + "?LoggedIn=1");

                }
                else if (int.Parse(dttable.Rows[0][0].ToString()) > 1)
                {
                    Session["CompID"] = null;
                    Session["UserContactID"] = null;
                    Session["DivId"] = null;
                    Session["GroupId"] = null;
                    litMessage.Text = GetErrorMessage("ERR059"); //"Found Multiple Email Address in the system. Please Contact Administrator !";
                    bindHtml();
                }
                else
                {
                    Session["CompID"] = null;
                    Session["UserContactID"] = null;
                    Session["DivId"] = null;
                    Session["GroupId"] = null;
                    litMessage.Text = GetErrorMessage("ERR060"); //"You have failed to enter registered email address. Try again, or contact your representative for assistance !";
                    bindHtml();
                }

            }
            catch (Exception ex)
            {
                ExceptionModule.ExceptionPublish(ex, Sites.ToLong(Session["DomainID"]), Sites.ToLong(Session["UserContactID"]), Sites.ToLong(Session["SiteId"]), Request);
                ShowMessage(ex.ToString(), 1);
            }
        }

        #endregion

        #region Methods

        #region HTML
        private void bindHtml()
        {
            try
            {
                string strUI = GetUserControlTemplate("login.htm");

                strUI = strUI.Replace("##ErrorMessage##", litMessage.Text);
                strUI = strUI.Replace("##EmaillAdd##", CCommon.RenderControl(txtEmaillAdd));
                strUI = strUI.Replace("##Password##", CCommon.RenderControl(txtPassword));
                strUI = strUI.Replace("##LoginButton##", CCommon.RenderControl(btnLogin));
                strUI = strUI.Replace("##ForgotPasswordButton##", CCommon.RenderControl(hplForgotPassword));
                strUI = strUI.Replace("##SignUpButton##", CCommon.RenderControl(hplSignUp));
                pnlCutomizeHtml.Visible = false;
                litCutomizeHtml.Text = strUI;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Check Payment method
        private bool IsOnlySalesInquiry()
        {
            UserAccess objUserAccess = new UserAccess();
            DataTable dtPaymentGateWay = new DataTable();
            objUserAccess.DomainID = Sites.ToLong(Session["DomainID"]);
            objUserAccess.byteMode = 1;
            dtPaymentGateWay = objUserAccess.ManageEComPaymentConfig(Sites.ToLong(Session["SiteId"]));
            if (dtPaymentGateWay != null && dtPaymentGateWay.Rows.Count > 0 && dtPaymentGateWay.Select("numPaymentMethodId = " + CCommon.ToInteger(PaymentMethod.SalesInquiry) + " AND bitEnable = 1").Length == 1 && dtPaymentGateWay.Select("bitEnable = 1").Length == 1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        #endregion

        #endregion

        #region Extra Code

        //public string Title { get; set; }
        //public string ForgotPassword { get; set; }
        //public string ForgotPasswordUrl { get; set; }

        #endregion
    }
}