﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BACRM.BusinessLogic.Admin;
using BACRM.BusinessLogic.Common;
using BACRM.BusinessLogic.ShioppingCart;
using System.Text.RegularExpressions;
using System.Data;
using BACRM.BusinessLogic.Opportunities;
using System.Text;

namespace BizCart.UserControls
{
    public partial class ShippingPromotion : BizUserControl
    {
        #region Member Variables

        DataTable dtShippingException;

        #endregion

        #region Page Events

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                Sites.InitialiseSession();

                if (!Page.IsPostBack)
                {
                    hdnIsChangeZip.Value = "0";
                    BindCountryAndState();
                }
            }
            catch (Exception ex)
            {
                ExceptionModule.ExceptionPublish(ex, Sites.ToInteger(Session["DomainID"]), Sites.ToInteger(Session["UserContactID"]), Sites.ToInteger(Session["SiteId"]), Request);
                ShowMessage(ex.ToString(), 1);
            }
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            if (hdnIsChangeZip.Value == "0")
            {
                GetShippingPromotionAndException(!Page.IsPostBack);
            }
            bindHtml();
        }


        #endregion

        #region Private Methods

        private void bindHtml()
        {
            try
            {
                string strUI = GetUserControlTemplate("ShippingPromotion.htm");

                string strPatternShippingPromotionDescription = "{##ShippingPromotionDescription##}(?<Content>([\\s\\S]*?)){/##ShippingPromotionDescription##}";
                string strPatternShippingPromotionChangeZip = "{##ShippingPromotionChangeZip##}(?<Content>([\\s\\S]*?)){/##ShippingPromotionChangeZip##}";

                if (Regex.IsMatch(strUI, strPatternShippingPromotionDescription))
                {
                    MatchCollection matches = Regex.Matches(strUI, strPatternShippingPromotionDescription);
                    string shippingPromotionDescriptiontemplate = matches[0].Groups["Content"].Value;

                    shippingPromotionDescriptiontemplate = shippingPromotionDescriptiontemplate.Replace("##ShippingRuleDesc##", CCommon.RenderControl(lblSPDescription));
                    shippingPromotionDescriptiontemplate = shippingPromotionDescriptiontemplate.Replace("##ShippingExceptionLink##", CCommon.RenderControl(hplSPException));
                    shippingPromotionDescriptiontemplate = shippingPromotionDescriptiontemplate.Replace("##ChangeShipToButton##", CCommon.RenderControl(btnSPChangeShipTo));

                    plhShippingPromotion.Controls.Add(new Literal() { Text = shippingPromotionDescriptiontemplate});
       
                    strUI = Regex.Replace(strUI, strPatternShippingPromotionDescription, CCommon.RenderControl(plhShippingPromotion));
                }
                else
                {
                    strUI = Sites.RemoveMatchedPattern(strUI, strPatternShippingPromotionDescription);
                }

                if (Regex.IsMatch(strUI, strPatternShippingPromotionChangeZip))
                {
                    MatchCollection matches = Regex.Matches(strUI, strPatternShippingPromotionChangeZip);
                    string shippingPromotionChangeZiptemplate = matches[0].Groups["Content"].Value;

                    shippingPromotionChangeZiptemplate = shippingPromotionChangeZiptemplate.Replace("##ZipCodeTextBox##", CCommon.RenderControl(txtSPZipCode));
                    shippingPromotionChangeZiptemplate = shippingPromotionChangeZiptemplate.Replace("##CountryDropDown##", CCommon.RenderControl(ddlSPCountry));
                    shippingPromotionChangeZiptemplate = shippingPromotionChangeZiptemplate.Replace("##StateDropDown##", CCommon.RenderControl(ddlSPState));
                    shippingPromotionChangeZiptemplate = shippingPromotionChangeZiptemplate.Replace("##SetZipButton##", CCommon.RenderControl(btnSPSetZip));

                    plhShippingPromotionChangeZip.Controls.Add(new Literal() { Text = shippingPromotionChangeZiptemplate });

                    strUI = Regex.Replace(strUI, strPatternShippingPromotionChangeZip, CCommon.RenderControl(plhShippingPromotionChangeZip));
                }
                else
                {
                    strUI = Sites.RemoveMatchedPattern(strUI, strPatternShippingPromotionChangeZip);
                }

                strUI = strUI.Replace("##ShippingExceptionModelID##","divSPException");
                string strPatternShippingException = "<##FORSPEXCEPTION##>(?<Content>([\\s\\S]*?))<##FORSPEXCEPTION##>";

                if (Regex.IsMatch(strUI, strPatternShippingException))
                {
                    MatchCollection matches = Regex.Matches(strUI, strPatternShippingException);
                    string shippingExceptionTemplate = matches[0].Groups["Content"].Value;

                    if (dtShippingException != null && dtShippingException.Rows.Count > 0)
                    {
                        string strTemp = string.Empty;
                        StringBuilder sb = new StringBuilder();

                        foreach (DataRow dr in dtShippingException.Rows)
                        {
                            strTemp = shippingExceptionTemplate;
                            strTemp = strTemp.Replace("##ItemName##", Sites.ToString(dr["vcItemName"]));
                            strTemp = strTemp.Replace("##ShippingCharge##", Sites.ToString(Session["CurrSymbol"]) + " " + string.Format("{0:#,##0.00}", CCommon.ToDouble(dr["FlatAmt"])));

                            sb.Append(strTemp);
                        }

                        strUI = Regex.Replace(strUI, strPatternShippingException, sb.ToString());
                    }
                    else
                    {
                        strUI = Regex.Replace(strUI, strPatternShippingException, "");
                    }
                }
                else
                {
                    strUI = Sites.RemoveMatchedPattern(strUI, strPatternShippingException);
                }

                pnlCutomizeHtml.Visible = false;
                litCutomizeHtml.Text = strUI;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void BindCountryAndState() 
        {
            try
            {
                ddlSPState.ClearSelection();
                ddlSPState.Items.Clear();

                CCommon objCommon = new CCommon();
                objCommon.sb_FillComboFromDBwithSel(ref ddlSPCountry, 40, Sites.ToLong(Session["DomainID"]));

                if (Sites.ToLong(ddlSPCountry.SelectedValue) > 0)
                {
                    FillState(ddlSPState, Sites.ToLong(ddlSPCountry.SelectedValue), Sites.ToLong(Session["DomainID"]));
                }
                else
                {
                    ddlSPState.Items.Insert(0,new ListItem("--Select One--", "0"));
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        private void GetShippingPromotionAndException(bool isFromPageLoad)
        {
            var strItemCodes = "";

            DataSet ds = new DataSet();
            ds = GetCartItem();

            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    strItemCodes = strItemCodes + (strItemCodes.Length > 0 ? "," + CCommon.ToString(dr["numItemCode"]) : CCommon.ToString(dr["numItemCode"]));
                }
            }

            ShippingRule objShippingRule = new ShippingRule();
            objShippingRule.DomainID = Sites.ToLong(Session["DomainID"]);
            objShippingRule.SiteID = Sites.ToInteger(Session["SiteId"]);
            Tuple<bool, string, Double, DataTable> tupleShippingPromotion = objShippingRule.GetShippingRule(Sites.ToString(Session["UserIPZipcode"]), Sites.ToLong(Session["UserIPStateID"]), Sites.ToLong(Session["UserIPCountryID"]), CCommon.ToInteger(Session["DivId"]), CCommon.ToLong(Session["WareHouseID"]), GetCartSubTotal(), strItemCodes);
            dtShippingException = tupleShippingPromotion.Item4;

            if (tupleShippingPromotion.Item1)
            {
                lblSPDescription.Text = tupleShippingPromotion.Item2;

                if(isFromPageLoad)
                {
                    plhShippingPromotion.Visible = true;
                    plhShippingPromotionChangeZip.Visible = false;
                }
            }
            else
            {
                if (!string.IsNullOrEmpty(Sites.ToString(Session["UserIPZipcode"])) && Sites.ToLong(Session["UserIPStateID"]) > 0)
                {
                    lblSPDescription.Text = GetErrorMessage("ERR079");
                    plhShippingPromotion.Visible = true;
                    plhShippingPromotionChangeZip.Visible = false;
                }
                else
                {
                    if (isFromPageLoad)
                    {
                        plhShippingPromotion.Visible = false;
                        plhShippingPromotionChangeZip.Visible = true;
                    }
                    else
                    {
                        lblSPDescription.Text = GetErrorMessage("ERR079");
                    }
                }
            }

            if(tupleShippingPromotion.Item4 != null && tupleShippingPromotion.Item4.Rows.Count > 0)
            {
                hplSPException.Visible = true;
            }
            else
            {
                hplSPException.Visible = false;
            }
        }
        
        #endregion

        #region Event Handlers

        protected void ddlSPCountry_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ddlSPState.ClearSelection();
                ddlSPState.Items.Clear();


                if (Sites.ToLong(ddlSPCountry.SelectedValue) > 0)
                {
                    FillState(ddlSPState, Sites.ToLong(ddlSPCountry.SelectedValue), Sites.ToLong(Session["DomainID"]));
                }
                else
                {
                    ddlSPState.Items.Insert(0, new ListItem("--Select One--", "0"));
                }
            }
            catch (Exception ex)
            {
                ExceptionModule.ExceptionPublish(ex, Sites.ToInteger(Session["DomainID"]), Sites.ToInteger(Session["UserContactID"]), Sites.ToInteger(Session["SiteId"]), Request);
                ShowMessage(ex.ToString(), 1);
            }
        }

        protected void btnSPSetZip_Click(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(txtSPZipCode.Text))
                {
                    ScriptManager.RegisterClientScriptBlock(this, this.Page.GetType(), "ChangeZipValidation", "alert('Please enter zip/postal code');", true);
                    plhShippingPromotion.Visible = false;
                    plhShippingPromotionChangeZip.Visible = true;
                }
                else if (Sites.ToLong(ddlSPCountry.SelectedValue) == 0)
                {
                    ScriptManager.RegisterClientScriptBlock(this, this.Page.GetType(), "ChangeZipValidation", "alert('Please select country');", true);
                    plhShippingPromotion.Visible = false;
                    plhShippingPromotionChangeZip.Visible = true;
                }
                else if (Sites.ToLong(ddlSPState.SelectedValue) == 0)
                {
                    ScriptManager.RegisterClientScriptBlock(this, this.Page.GetType(), "ChangeZipValidation", "alert('Please select State/Province');", true);
                    plhShippingPromotion.Visible = false;
                    plhShippingPromotionChangeZip.Visible = true;
                }
                else
                {
                    Session["UserIPZipcode"] = txtSPZipCode.Text.Trim();
                    Session["UserIPCountryID"] = Sites.ToLong(ddlSPCountry.SelectedValue);
                    Session["UserIPStateID"] = Sites.ToLong(ddlSPState.SelectedValue);

                    GetShippingPromotionAndException(false);

                    plhShippingPromotion.Visible = true;
                    plhShippingPromotionChangeZip.Visible = false;
                }
            }
            catch (Exception ex)
            {
                ExceptionModule.ExceptionPublish(ex, Sites.ToInteger(Session["DomainID"]), Sites.ToInteger(Session["UserContactID"]), Sites.ToInteger(Session["SiteId"]), Request);
                ShowMessage(ex.ToString(), 1);
            }
        }

        protected void btnSPChangeShipTo_Click(object sender, EventArgs e)
        {
            try
            {
                txtSPZipCode.Text = Sites.ToString(Session["UserIPZipcode"]);
                if (ddlSPCountry.Items.FindByValue(CCommon.ToString(Session["UserIPCountryID"])) != null)
                {
                    ddlSPCountry.SelectedValue = CCommon.ToString(Session["UserIPCountryID"]);
                }
                ddlSPState.ClearSelection();
                ddlSPState.Items.Clear();


                if (Sites.ToLong(ddlSPCountry.SelectedValue) > 0)
                {
                    FillState(ddlSPState, Sites.ToLong(ddlSPCountry.SelectedValue), Sites.ToLong(Session["DomainID"]));
                    if (ddlSPState.Items.FindByValue(CCommon.ToString(Session["UserIPStateID"])) != null)
                    {
                        ddlSPState.SelectedValue = CCommon.ToString(Session["UserIPStateID"]);
                    }
                }
                else
                {
                    ddlSPState.Items.Insert(0, new ListItem("--Select One--", "0"));
                }


                plhShippingPromotion.Visible = false;
                plhShippingPromotionChangeZip.Visible = true;
                hdnIsChangeZip.Value = "1";
            }
            catch (Exception ex)
            {
                ExceptionModule.ExceptionPublish(ex, Sites.ToInteger(Session["DomainID"]), Sites.ToInteger(Session["UserContactID"]), Sites.ToInteger(Session["SiteId"]), Request);
                ShowMessage(ex.ToString(), 1);
            }
        }

        #endregion
    }
}