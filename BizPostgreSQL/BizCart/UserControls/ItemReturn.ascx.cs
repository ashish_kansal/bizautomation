﻿using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BACRM.BusinessLogic.Opportunities;
using BACRM.BusinessLogic.Common;
using BACRM.BusinessLogic.Item;
using System.Data;
using BACRM.BusinessLogic.ShioppingCart;
using System.Text.RegularExpressions;
using System.Text;
namespace BizCart.UserControls
{
    public partial class ItemReturn : BizUserControl
    {
        string strColumn = string.Empty;
        int PageSize = 10;
        int PageIndex = 0;
        string DasboardTabs = "";
        string actionitem = "";
        string orderId = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                Sites.InitialiseSession();
                if (Sites.ToLong(Session["UserContactID"]) == 0 || Sites.ToLong(Session["DivId"]) == 0)
                {
                    Response.Redirect("Login.aspx?ReturnURL=OpenRMA.aspx", true);
                }

                actionitem = Convert.ToString(Request.QueryString["actionitem"]);
                orderId = Convert.ToString(Request.QueryString["OrderId"]);


                if (!IsPostBack)
                {
                    CCommon objCommon = new CCommon();
                    objCommon.sb_FillComboFromDB(ref ddlReturnReason, Sites.ToLong(48), Sites.ToLong(Session["DomainID"]));
                    bindHtml();
                }

                //if (!string.IsNullOrEmpty(txtSortChar.Text))
                //{
                //    ViewState["SortChar"] = txtSortChar.Text;
                //    ViewState["Column"] = "BizDocName";
                //    Session["Asc"] = 0;
                //    BindDatagrid();
                //    txtSortChar.Text = "";
                //}
            }
            catch (Exception ex)
            {
                ExceptionModule.ExceptionPublish(ex, Sites.ToLong(Session["DomainID"]), Sites.ToLong(Session["UserContactID"]), Sites.ToLong(Session["SiteId"]), Request);
                ShowMessage(ex.ToString(), 1);
            }
        }


        public DataTable BindDatagrid()
        {
            try
            {

                DataTable dtReturnDetails = new DataTable();
                if (Request.QueryString["ReturnHeaderID"] != null)
                {
                    ReturnHeader objReturn = new ReturnHeader();
                    objReturn.ReturnHeaderID = Sites.ToLong(Request.QueryString["ReturnHeaderID"]);
                    objReturn.DomainID = Sites.ToLong(Session["DomainID"]);

                    dtReturnDetails = objReturn.GetReturnHeader();
                }
                else if (Request.QueryString["OrderId"] != null)
                {
                    MOpportunity objOpportunity = new MOpportunity();
                    objOpportunity.DomainID = Sites.ToLong(Session["DomainID"]);
                    objOpportunity.OppItemCode = Sites.ToLong(0);
                    objOpportunity.OpportunityId = Sites.ToLong(Request.QueryString["OrderID"]);
                    objOpportunity.Mode = 6;
                    objOpportunity.ClientTimeZoneOffset = 0;
                    objOpportunity.strItems = "";
                    dtReturnDetails = objOpportunity.GetOrderItems().Tables[0];
                    Session["SoDetails"] = dtReturnDetails;
                }


                return dtReturnDetails;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string ReturnName(System.DateTime bintCreatedDate)
        {
            try
            {
                string strCreateDate = null;
                strCreateDate = modBacrm.FormattedDateFromDate(bintCreatedDate, Sites.ToString(Session["DateFormat"]));
                return strCreateDate;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string ReturnDateTime(Object CloseDate)
        {
            try
            {
                string strTargetResolveDate = "";
                string temp = "";

                if (!(CloseDate == System.DBNull.Value))
                {
                    DateTime dtCloseDate = DateTime.Parse(CloseDate.ToString());
                    strTargetResolveDate = modBacrm.FormattedDateFromDate(dtCloseDate, Sites.ToString(Session["DateFormat"]));

                    string timePart = dtCloseDate.ToShortTimeString().Substring(0, dtCloseDate.ToShortTimeString().Length - 1);
                    // remove gaps
                    if (timePart.Split(' ').Length >= 2) timePart = timePart.Split(' ').GetValue(0).ToString() + timePart.Split(' ').GetValue(1).ToString();

                    // check Today date components [ Date , Month , Year ] with Parameter CloseDate components [ Date , Month , Year ] 
                    // if both are same it is today
                    if ((dtCloseDate.Date == DateTime.Now.Date & dtCloseDate.Month == DateTime.Now.Month & dtCloseDate.Year == DateTime.Now.Year))
                    {
                        strTargetResolveDate = "<font color=red><b>Today</b></font>";

                        return strTargetResolveDate;
                    }
                    // check [ TodayDate + 1 ] Date.... components [ Date , Month , Year ] with Parameter dtCloseDate components [ Date , Month , Year ] 
                    // if both are same it was Yesterday
                    else if ((dtCloseDate.Date.AddDays(1).Date == DateTime.Now.Date & dtCloseDate.AddDays(1).Month == DateTime.Now.Month & dtCloseDate.AddDays(1).Year == DateTime.Now.Year))
                    {
                        strTargetResolveDate = "<font color=purple><b>Yesterday</b></font>";

                        return strTargetResolveDate;
                    }
                    // check TodayDate .... components [ Date , Month , Year ] with Parameter [ dtCloseDate + 1 ] Date....  components [ Date , Month , Year ] 
                    // if both are same it will Tomorrow
                    else if ((dtCloseDate.Date == DateTime.Now.AddDays(1).Date & dtCloseDate.Month == DateTime.Now.AddDays(1).Month & dtCloseDate.Year == DateTime.Now.AddDays(1).Year))
                    {
                        temp = dtCloseDate.Hour.ToString() + ":" + dtCloseDate.Minute.ToString();
                        strTargetResolveDate = "<font color=orange><b>Tomorrow</b></font>";

                        return strTargetResolveDate;
                    }
                    // display day name for next 4 days from DateTime.Now

                    else if (dtCloseDate.ToString("yyyyMMdd") == DateTime.Now.AddDays(2).ToString("yyyyMMdd") | dtCloseDate.ToString("yyyyMMdd") == DateTime.Now.AddDays(3).ToString("yyyyMMdd") | dtCloseDate.ToString("yyyyMMdd") == DateTime.Now.AddDays(4).ToString("yyyyMMdd") | dtCloseDate.ToString("yyyyMMdd") == DateTime.Now.AddDays(5).ToString("yyyyMMdd"))
                    {
                        strTargetResolveDate = "<b>" + dtCloseDate.DayOfWeek.ToString() + "</b>";
                        return strTargetResolveDate;
                    }
                    else
                    {
                        //strTargetResolveDate = strTargetResolveDate;
                        return strTargetResolveDate;
                    }
                }
                return strTargetResolveDate;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public object ReturnMoney(System.Object Money)
        {
            try
            {
                return Math.Round(Sites.ToDecimal(Money.ToString()), 2);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        private void btnGo_Click(object sender, EventArgs e)
        {
            try
            {
                bindHtml();
            }
            catch (Exception ex)
            {
                ExceptionModule.ExceptionPublish(ex, Sites.ToLong(Session["DomainID"]), Sites.ToLong(Session["UserContactID"]), Sites.ToLong(Session["SiteId"]), Request);
                ShowMessage(ex.ToString(), 1);
            }
        }

        void bindHtml()
        {
            try
            {
                string strUI = GetUserControlTemplate("ItemReturn.htm");

                string pattern = "<##FOR(?<Property>(\\b[^>]*?))##>(?<Content>([\\s\\S]*?))<##ENDFOR##>";
                string CssClass = "", AltCssClass = "", RepeteTemplate = "";

                if (Regex.IsMatch(strUI, pattern))
                {
                    MatchCollection matches = Regex.Matches(strUI, pattern);
                    if ((matches.Count > 0))
                    {
                        string[] Property = matches[0].Groups["Property"].Value.Replace("(", "").Replace(")", "").Split(';');
                        RepeteTemplate = matches[0].Groups["Content"].Value;



                        if (Property.Length > 0)
                        {
                            for (int i = 0; i < Property.Length; i++)
                            {
                                string[] PropertyValue = Property[i].Split('=');

                                if (PropertyValue.Length == 2)
                                {
                                    switch (PropertyValue[0])
                                    {
                                        case "ItemClass":
                                            CssClass = PropertyValue[1].Trim();
                                            break;
                                        case "AlternatingItemClass":
                                            AltCssClass = PropertyValue[1].Trim();
                                            break;
                                    }
                                }
                            }
                        }
                        StringBuilder sb = new StringBuilder();
                        DataTable dt = BindDatagrid();
                        if (dt.Rows.Count > 0)
                        {


                            string strTemp = string.Empty;
                            int RowsCount = 0;

                            foreach (DataRow dr in dt.Rows)
                            {
                                strTemp = RepeteTemplate;
                                strTemp = strTemp.Replace("##ItemName##", CCommon.ToString(dr["vcItemName"]));
                                strTemp = strTemp.Replace("##OrderId##", CCommon.ToString(Request.QueryString["OrderId"]));
                                strTemp = strTemp.Replace("##hdnReturnItem##", CCommon.RenderControl(hdnReturnItem));
                                if (CCommon.ToString(dr["bitSerialized"]) == "True" || CCommon.ToString(dr["bitLotNo"]) == "True")
                                {
                                    if (CCommon.ToString(Request.QueryString["OrderId"]) != "")
                                    {
                                        strTemp = strTemp.Replace("##SerialNo##", "<a href=\"javascript:void(0)\" id=\"href" + CCommon.ToString(dr["numItemCode"]) + "\" onclick=\"openPopup('" + CCommon.ToString(dr["numItemCode"]) + "', '" + Sites.ToLong(Request.QueryString["ReturnHeaderID"]) + "')\">Added (0)</a>");
                                    }
                                    else
                                    {
                                        strTemp = strTemp.Replace("##SerialNo##", "<a href=\"javascript:void(0)\" id=\"href" + CCommon.ToString(dr["numItemCode"]) + "\" onclick=\"openPopup('" + CCommon.ToString(dr["numReturnItemID"]) + "', '" + Sites.ToLong(Request.QueryString["ReturnHeaderID"]) + "')\">Added (" + dr["SerialLotNo"] + ")</a>");
                                    }
                                }
                                else
                                {
                                    strTemp = strTemp.Replace("##SerialNo##", "");
                                }
                                    strTemp = strTemp.Replace("##ItemDesc##", CCommon.ToString(dr["vcItemDesc"]));
                                
                                if (Sites.ToLong(Request.QueryString["ReturnHeaderID"]) > 0)
                                {
                                    strTemp = strTemp.Replace("##QtytoReturnTextBox##", "");
                                    strTemp = strTemp.Replace("##QtytoReturnLabel##", CCommon.ToString(Sites.ToDouble(dr["numUnitHour"])-Sites.ToDouble(dr["numUnitHourReceived"])));
                                }
                                else
                                {
                                    strTemp = strTemp.Replace("##QtytoReturnLabel##","");
                                    strTemp = strTemp.Replace("##QtytoReturnTextBox##", "<input type=\"text\" class=\"form-control\" id=\"txtItem" + CCommon.ToString(dr["numItemCode"]) + "\" attrd=\"" + CCommon.ToString(dr["numItemCode"]) + "\" value=\"0\" onChange=\"validateQty('" + CCommon.ToString(dr["numUnitHour"]) + "','" + CCommon.ToString(dr["numItemCode"]) + "')\"/>");
                                }
                                
                                strTemp = strTemp.Replace("##ItemCode##", CCommon.ToString(dr["numItemCode"]));
                                strTemp = strTemp.Replace("##ItemCodeDefaultQty##", CCommon.ToString(dr["numUnitHour"]));
                                if (RowsCount % 2 == 0)
                                    strTemp = strTemp.Replace("##RowClass##", AltCssClass);
                                else
                                    strTemp = strTemp.Replace("##RowClass##", CssClass);

                                sb.Append(strTemp);

                                RowsCount++;
                            }
                        }
                        //strUI = strUI.Replace("##NoOfOrdersLabel##", lblRecordCount.Text);
                        //strUI = strUI.Replace("##PagerControl##", CCommon.RenderControl(AspNetPager1));
                        strUI = Regex.Replace(strUI, pattern, sb.ToString());
                    }
                }

                //Credit Term Replacement
                string SummaryPattern = "{##SummaryTable##}(?<Content>([\\s\\S]*?)){/##SummaryTable##}";
                CCommon objCommon = new CCommon();
                CItems objItems = new CItems();
                objItems.DivisionID = Sites.ToLong(Session["DivId"]);
                DataTable dtTable = default(DataTable);
                string lblCreditLimit = "";
                dtTable = objItems.GetAmountDue();

                decimal RemainIngCredit = Sites.ToDecimal(dtTable.Rows[0]["RemainingCredit"]);
                if (RemainIngCredit < 0)
                {
                    RemainIngCredit = 0;
                }

                lblCreditLimit = Sites.ToString(Session["CurrSymbol"]) + " " + string.Format("{0:#,##0.00}", dtTable.Rows[0]["CreditLimit"]);
                strUI = Sites.ReplaceMatchedPatternWithContent(strUI, SummaryPattern);

                //strUI = strUI.Replace("##TotalAmountPastDueLabel##", lblAmtPastDue.Text);
                strUI = strUI.Replace(" ##CreditLimit##", lblCreditLimit);
                strUI = strUI.Replace("##ARStatement##", "");
               

                strUI = BindTopDashboard(strUI);

                strUI = strUI.Replace("##CustomerName##", Sites.ToString(Session["ContactName"]));
                if (Sites.ToLong(Request.QueryString["ReturnHeaderID"]) > 0)
                {
                    strUI = strUI.Replace("##RMAReason##", "");
                    strUI = strUI.Replace("##RMARequestButton##", "");
                }
                else
                {
                    strUI = strUI.Replace("##RMAReason##", CCommon.RenderControl(ddlReturnReason));
                    strUI = strUI.Replace("##RMARequestButton##", CCommon.RenderControl(btnReturnItem));
                }

                pnlCutomizeHtml.Visible = false;
                litCutomizeHtml.Text = strUI;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        protected void AspNetPager1_PageChanged(object sender, EventArgs e)
        {
            try
            {
                bindHtml();
            }
            catch (Exception ex)
            {
                ExceptionModule.ExceptionPublish(ex, Sites.ToLong(Session["DomainID"]), Sites.ToLong(Session["UserContactID"]), Sites.ToLong(Session["SiteId"]), Request);
                ShowMessage(ex.ToString(), 1);
            }
        }
        private string BindTopDashboard(string strUI)
        {
            StringBuilder sbTopMenu = new StringBuilder();
            if (Sites.ToBool(Session["bitSalesOrderTabs"]) == true)
            {
                sbTopMenu.Append("<li class=\"SalesOrderTabs\"><a id=\"SalesOrderTabs\" href=\"Orders.aspx?flag=true\">" + Convert.ToString(Session["vcSalesOrderTabs"]) + "</a></li>");
            }
            if (Sites.ToBool(Session["bitSalesQuotesTabs"]) == true)
            {
                sbTopMenu.Append("<li class=\"SalesOpportunityTabs\"><a id=\"SalesOpportunityTabs\" href=\"SalesOpportunity.aspx\">" + Convert.ToString(Session["vcSalesQuotesTabs"]) + "</a></li>");
            }
            if (Sites.ToBool(Session["bitItemPurchaseHistoryTabs"]) == true)
            {
                sbTopMenu.Append("<li class=\"ItemPurchasedTabs\"><a id=\"ItemPurchasedTabs\" href=\"ItemPurchasedHistory.aspx\">" + Convert.ToString(Session["vcItemPurchaseHistoryTabs"]) + "</a></li>");
            }
            if (Sites.ToBool(Session["bitItemsFrequentlyPurchasedTabs"]) == true)
            {
                sbTopMenu.Append("<li class=\"WishListsTabs\"><a id=\"WishListsTabs\" href=\"Lists.aspx\">" + Convert.ToString(Session["vcItemsFrequentlyPurchasedTabs"]) + "</a></li>");
            }
            if (Sites.ToBool(Session["bitOpenCasesTabs"]) == true)
            {
                sbTopMenu.Append("<li class=\"OpenCasesTabs \"><a id=\"OpenCasesTabs\" href=\"OpenCases.aspx\">" + Convert.ToString(Session["vcOpenCasesTabs"]) + "</a></li>");
            }
            if (Sites.ToBool(Session["bitOpenRMATabs"]) == true)
            {
                sbTopMenu.Append("<li class=\"OpenRMATabs active\"><a id=\"OpenRMATabs\" href=\"OpenRMA.aspx\">" + Convert.ToString(Session["vcOpenRMATabs"]) + "</a></li>");
            }
            if (Sites.ToBool(Session["bitSupportTabs"]) == true)
            {
                sbTopMenu.Append("<li class=\"SupportTabs\"><a id=\"SupportTabs\" href=\"support.aspx\">" + Convert.ToString(Session["vcSupportTabs"]) + "</a></li>");
            }
            strUI = strUI.Replace("##TopMenuDasboard##", Convert.ToString(sbTopMenu));
            return strUI;
        }

        protected void btnReturnItem_Click(object sender, EventArgs e)
        {
            try
            {
                ReturnHeader objReturnHeader = new ReturnHeader();
                objReturnHeader.ReturnHeaderID = 0;
                objReturnHeader.Rma = "";
                objReturnHeader.BizDocName = "";
                objReturnHeader.DomainID = Sites.ToLong(Session["DomainID"]);
                objReturnHeader.DivisionId = Sites.ToLong(Session["DivId"]);
                objReturnHeader.ContactId = Sites.ToLong(Session["UserContactID"]);
                objReturnHeader.OppId = Sites.ToLong(Request.QueryString["OrderId"]);
                objReturnHeader.ReturnType = 1;
                objReturnHeader.ReturnReason = CCommon.ToLong(ddlReturnReason.SelectedValue);
                objReturnHeader.ReturnStatus = CCommon.ToLong(enmReturnStatus.Pending);
               
                objReturnHeader.Comments = "";
                objReturnHeader.UserCntID = Sites.ToLong(Session["UserContactID"]);
                objReturnHeader.BillToAddressID = 0;
                objReturnHeader.ShipToAddressID = 0;


                DataTable dtItems = new DataTable();
                DataSet dsItems = new DataSet();
                dtItems.TableName = "Item";
                dtItems.Columns.Add("numReturnItemID");
                dtItems.Columns.Add("numItemCode");
                dtItems.Columns.Add("numUnitHour");
                dtItems.Columns.Add("numUnitHourReceived");
                dtItems.Columns.Add("monPrice");
                dtItems.Columns.Add("bitDiscountType");
                dtItems.Columns.Add("fltDiscount");
                dtItems.Columns.Add("monTotAmount");
                dtItems.Columns.Add("vcItemDesc");
                dtItems.Columns.Add("numWareHouseItemID");
                dtItems.Columns.Add("vcModelID");
                dtItems.Columns.Add("vcManufacturer");
                dtItems.Columns.Add("numUOMId");
                dtItems.Columns.Add("numOppItemID");
                objReturnHeader.ReceiveType = 0;

                DataTable dtSODetails = new DataTable();
                dtSODetails = (DataTable)Session["SoDetails"];
                string ReturnData = hdnReturnItem.Value.Substring(0,hdnReturnItem.Value.Length-1);
                string[] ReturnItems = ReturnData.Split('~');
                decimal Amount = 0;
                decimal TotalTax = 0;
                decimal TotalDiscount = 0;
                foreach (string Items in ReturnItems)
                {
                    if (Items.Length > 0)
                    {
                        string[] ItemDetails = Items.Split('#');
                        DataRow[] dr = dtSODetails.Select("numItemCode=" + ItemDetails[0] + "");
                        DataRow drItem;
                        foreach (DataRow drDetails in dr)
                        {
                            double units = Sites.ToLong(ItemDetails[1]);
                            decimal price = Sites.ToDecimal(drDetails["monPrice"]);
                            drItem = dtItems.NewRow();
                            drItem["numReturnItemID"] = 0;
                            drItem["numItemCode"] = Sites.ToLong(drDetails["numItemCode"]);
                            drItem["numUnitHour"] = units;
                            drItem["numUnitHourReceived"] = Sites.ToDouble(drDetails["numUnitHour"]);
                            drItem["monPrice"] = Sites.ToDecimal(drDetails["monPrice"]);
                            drItem["bitDiscountType"] = Sites.ToBool(drDetails["bitDiscountType"]);
                            drItem["fltDiscount"] = Sites.ToDecimal(drDetails["fltDiscount"]);
                            drItem["monTotAmount"] = Sites.ToDecimal(drDetails["monTotAmount"]);
                            if (CCommon.ToDecimal(drItem["fltDiscount"]) > 0)
                            {
                                decimal orgQty = Sites.ToDecimal(drDetails["numOrigUnitHour"]);
                                decimal monPrice = 0;
                                decimal discount = CCommon.ToDecimal(drDetails["fltDiscount"]);

                                //Discount in amount

                                if (CCommon.ToBool(drDetails["bitDiscountType"]))
                                {
                                    if (orgQty > 0)
                                    {
                                        monPrice = CCommon.ToDecimal(price - (discount / orgQty));
                                    }
                                    else
                                    {
                                        monPrice = 0;
                                    }

                                    drItem["monTotAmount"] = Sites.ToDecimal(units) * monPrice;
                                    Amount = Amount + Sites.ToDecimal(units) * monPrice;
                                    TotalDiscount = TotalDiscount + discount;
                                    //Discount in percent
                                }
                                else
                                {
                                    drItem["monTotAmount"] = Sites.ToDecimal(Sites.ToDecimal(units) * price) - (Sites.ToDecimal(Sites.ToDecimal(units) * price) * (discount / 100));
                                    Amount = Amount + Sites.ToDecimal(Sites.ToDecimal(units) * price) - (Sites.ToDecimal(Sites.ToDecimal(units) * price) * (discount / 100));
                                }
                            }
                            else
                            {
                                drItem["monTotAmount"] = CCommon.ToDecimal(drDetails["monPrice"]) * Sites.ToDecimal(units);
                                Amount = Amount + CCommon.ToDecimal(drDetails["monPrice"]) * Sites.ToDecimal(units);
                            }
                            drItem["vcItemDesc"] = Sites.ToString(drDetails["vcItemDesc"]);
                            drItem["numWareHouseItemID"] = CCommon.ToLong(drDetails["numWareHouseItmsID"]);
                            drItem["vcModelID"] = CCommon.ToString(drDetails["vcModelID"]);
                            drItem["vcManufacturer"] = "";
                            drItem["numUOMId"] = CCommon.ToLong(drDetails["numUOM"]);
                            drItem["numOppItemID"] = CCommon.ToLong(drDetails["numoppitemtCode"]);
                            dtItems.Rows.Add(drItem);
                        }
                    }
                    objReturnHeader.Amount = Amount;
                    objReturnHeader.TotalTax = 0;
                    objReturnHeader.TotalDiscount = TotalDiscount;
                    dsItems.Tables.Add(dtItems);
                    objReturnHeader.strItems = dsItems.GetXml();
                    long lngReturnHeaderID = 0;
                    lngReturnHeaderID = objReturnHeader.ManageReturnHeader();
                    Response.Redirect("/ItemReturn.aspx?ReturnHeaderID=" + lngReturnHeaderID + "");
                }
            }
            catch
            {

            }
        }


    }
}