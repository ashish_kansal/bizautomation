﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ShippingOptions.ascx.cs"
    Inherits="BizCart.UserControls.ShippingOptions" %>
<script>
    function DeleteRecord(a) {
        if (confirm('Are you sure, you want to delete the selected record?')) {
            __doPostBack('btnDelete', a);
            return false;
        }
        else {
            return false;
        }
    }
</script>
<asp:Panel ID="pnlCutomizeHtml" runat="server">
<table cellspacing="1" cellpadding="2" width="100%">
    <tr>
        <td>
            <div class="sectionheader">
                Choose a shipping speed
            </div>
        </td>
    </tr>
    <tr>
        <td>
            <table width="100%">
                <tr>
                    <td valign="top">
                        <table align="left" width="100%">
                            <tr>
                                <td>
                                    <br />
                                    <asp:Button ID="btnSave" runat="server" Text="Save Shipping Options" CssClass="button"
                                        OnClick="btnSave_Click" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
</asp:Panel>
<asp:Literal ID="litCutomizeHtml" runat="server"></asp:Literal>
<asp:HiddenField ID="hdnReffereURL" runat="server" />
