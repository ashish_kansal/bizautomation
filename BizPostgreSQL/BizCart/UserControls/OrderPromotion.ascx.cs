﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BACRM.BusinessLogic.Admin;
using BACRM.BusinessLogic.Common;
using BACRM.BusinessLogic.ShioppingCart;
using System.Text.RegularExpressions;
using System.Data;
using BACRM.BusinessLogic.Opportunities;

namespace BizCart.UserControls
{
    public partial class OrderPromotion : BizUserControl
    {
        #region Page Events

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                Sites.InitialiseSession();
            }
            catch (Exception ex)
            {
                ExceptionModule.ExceptionPublish(ex, Sites.ToInteger(Session["DomainID"]), Sites.ToInteger(Session["UserContactID"]), Sites.ToInteger(Session["SiteId"]), Request);
                ShowMessage(ex.ToString(), 1);
            }
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            try
            {
                bindHtml();
            }
            catch (Exception ex)
            {
                ExceptionModule.ExceptionPublish(ex, Sites.ToInteger(Session["DomainID"]), Sites.ToInteger(Session["UserContactID"]), Sites.ToInteger(Session["SiteId"]), Request);
                ShowMessage(ex.ToString(), 1);
            }
        }

        #endregion

        #region Private Methods

        private void bindHtml()
        {
            try
            {
                string strUI = GetUserControlTemplate("OrderPromotion.htm");
                strUI = strUI.Replace("##OrderPromotionImage##", "<img src='/images/Deal_icon.png' />");

                double numSubTotal = GetCartSubTotal();

                COpportunities objOpportunity = new COpportunities();
                objOpportunity.DomainID = Sites.ToLong(Session["DomainID"]);
                objOpportunity.DivisionID = Sites.ToInteger(Session["DivId"]);
                DataSet dsOrderProm = objOpportunity.GetPromotiondRuleForOrder(numSubTotal, siteID:Sites.ToLong(Session["SiteId"]));

                if (dsOrderProm != null && dsOrderProm.Tables.Count >= 2)
                {
                    DataTable dtOrderPro = dsOrderProm.Tables[0];
                    DataTable dtOrderDiscounts = dsOrderProm.Tables[1];

                    if (dsOrderProm.Tables[0].Rows.Count > 0)
                    {
                        string StrRuleDesc = "";
                        int RowCount = dtOrderDiscounts.Rows.Count;
                        
                        foreach (DataRow dr in dtOrderDiscounts.Rows)
                        {
                            if (numSubTotal == 0 && CCommon.ToInteger(dr["RowNum"]) == 1)
                            {
                                if (Sites.ToString(dtOrderPro.Rows[0]["tintDiscountType"]) == "1")
                                {
                                    StrRuleDesc = String.Concat(StrRuleDesc,"Spend $",(CCommon.ToDouble(dr["numOrderAmount"]) - numSubTotal)," and get ",CCommon.ToString(dr["fltDiscountValue"]),"% off ENTIRE ORDER. ");
                                }
                                else if (Sites.ToString(dtOrderPro.Rows[0]["tintDiscountType"]) == "2")
                                {
                                    StrRuleDesc = String.Concat(StrRuleDesc,"Spend $",(CCommon.ToDouble(dr["numOrderAmount"]) - numSubTotal)," and get $", CCommon.ToString(dr["fltDiscountValue"])," off ENTIRE ORDER. ");
                                }
                            }
                            else if (numSubTotal < CCommon.ToDouble(dr["numOrderAmount"]))
                            {
                                if (CCommon.ToString(dtOrderPro.Rows[0]["tintDiscountType"]) == "1")
                                {
                                    StrRuleDesc = String.Concat(StrRuleDesc,"Spend $",(CCommon.ToDouble(dr["numOrderAmount"]) - numSubTotal)," more to make it ",CCommon.ToString(dr["fltDiscountValue"]),"%. ");
                                }
                                else if (CCommon.ToString(dtOrderPro.Rows[0]["tintDiscountType"]) == "2")
                                {
                                    StrRuleDesc = String.Concat(StrRuleDesc,"Spend $",(CCommon.ToDouble(dr["numOrderAmount"]) - numSubTotal)," more to make it $",CCommon.ToString(dr["fltDiscountValue"]) + ". ");
                                }
                            }
                            else if (numSubTotal >= CCommon.ToDouble(dr["numOrderAmount"]) && (CCommon.ToInteger(dr["RowNum"]) != RowCount))
                            {
                                if (CCommon.ToString(dtOrderPro.Rows[0]["tintDiscountType"]) == "1")
                                {
                                    StrRuleDesc = String.Concat("Order has qualified for ",CCommon.ToString(dr["fltDiscountValue"]),"% discount.");
                                }
                                else if (CCommon.ToString(dtOrderPro.Rows[0]["tintDiscountType"]) == "2")
                                {
                                    StrRuleDesc = String.Concat("Order has qualified for $",CCommon.ToString(dr["fltDiscountValue"])," discount.");
                                }
                            }
                            else if ((numSubTotal >= CCommon.ToDouble(dr["numOrderAmount"])) && (CCommon.ToInteger(dr["RowNum"]) == RowCount))
                            {
                                if (CCommon.ToString(dtOrderPro.Rows[0]["tintDiscountType"]) == "1")
                                {
                                    StrRuleDesc = String.Concat("Congratulations, this order qualifies for a ",CCommon.ToString(dr["fltDiscountValue"]),"% discount (to be applied when order is placed).");
                                }
                                else if (CCommon.ToString(dtOrderPro.Rows[0]["tintDiscountType"]) == "2")
                                {
                                    StrRuleDesc = String.Concat("Congratulations, this order qualifies for a $",CCommon.ToString(dr["fltDiscountValue"])," discount (to be applied when order is placed).");
                                }
                            }
                        }

                        if ((CCommon.ToLong(dtOrderPro.Rows[0]["bitRequireCouponCode"]) == 1))
                        {
                            StrRuleDesc = String.Concat(StrRuleDesc,"<i> (Coupon code required) </i>");
                        }

                        strUI = strUI.Replace("##OrderPromotionDescriptiob##", StrRuleDesc);
                    }
                    else
                    {
                        strUI = "";
                    }
                }
                else
                {
                    strUI = "";
                }


                pnlCutomizeHtml.Visible = false;
                litCutomizeHtml.Text = strUI;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion
    }
}