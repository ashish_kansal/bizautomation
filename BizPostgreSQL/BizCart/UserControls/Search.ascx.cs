﻿using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BACRM.BusinessLogic.ShioppingCart;
using BACRM.BusinessLogic.Common;
using System.ComponentModel;
namespace BizCart.UserControls
{
    public partial class Search : BizUserControl
    {
        #region Events
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {

                Sites.InitialiseSession();
                if (!IsPostBack)
                {
                    btnSearch.ImageUrl = Sites.ToString(Session["SitePath"]) + "/images/icon_find.gif";
                    txtSearch.Attributes.Add("onkeydown", "if(event.which || event.keyCode){if ((event.which == 13) || (event.keyCode == 13)) {document.getElementById('" + btnSearch.ClientID + "').click();return false;}} else {return true}; ");
                    bindHtml();
                }
                if (IsPostBack)
                {
                    if (Sites.ToString(Request["__EVENTTARGET"]) == "btnSearch")
                    {
                        SearchProduct();
                    }
                    if (Sites.ToString(Request["__EVENTTARGET"]) == "QuickAddItems")
                    {
                        quickAddItems();
                    }
                    bindHtml();
                }
            }
            catch (Exception ex)
            {
                ExceptionModule.ExceptionPublish(ex, Sites.ToLong(Session["DomainID"]), Sites.ToLong(Session["UserContactID"]), Sites.ToLong(Session["SiteId"]), Request);
                ShowMessage(ex.ToString(), 1);
            }
        }
        #endregion

        #region Methods
        private void bindHtml()
        {
            try
            {
                string strUI = GetUserControlTemplate("Search.htm");

                strUI = strUI.Replace("##QuickAddedItemsHiddenField##", CCommon.RenderControl(hdnQuickAddedItems));
                strUI = strUI.Replace("##SearchTextBox##", CCommon.RenderControl(txtSearch));
                strUI = strUI.Replace("##SearchButton##", CCommon.RenderControl(btnSearch));

                pnlCutomizeHtml.Visible = false;
                litCutomizeHtml.Text = strUI;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private void quickAddItems()
        {
            try
            {
                string itemDetails = "";
                if (hdnQuickAddedItems != null)
                {
                    itemDetails = hdnQuickAddedItems.Value;
                    var lstProducts = itemDetails.Split(',');
                    foreach (var d in lstProducts)
                    {
                        long productid = 0;
                        int qty = 0;
                        productid = Sites.ToLong(d.Split('#')[0]);
                        qty = Sites.ToInteger(d.Split('#')[1]);
                        Tuple<bool, string> objResult = AddToCartFromEcomm("", productid, 0, "", false, qty);
                        bool isItemAdded = objResult.Item1;
                    }
                }
            }
            catch (Exception ex) { }
        }
        private void SearchProduct()
        {
            try
            {
                if (txtSearch.Text.Trim().Length > 0)
                {
                    //Response.Write(Sites.ToString(Session["SearchFields"]).Replace("#SearchText#", txtSearch.Text.Trim()).Trim() + "<br>");

                    Response.Redirect(Sites.ToString(Session["SitePath"]) + "/Search/1/" + HttpUtility.UrlEncode(txtSearch.Text.Trim()), false);
                }
                else
                    ShowMessage(GetErrorMessage("ERR050"), 0);//Please enter search keyword!
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region ExtraCode

        //protected void btnSearch_Click(object sender, ImageClickEventArgs e)
        //{
        //    try
        //    {
        //        SearchProduct();
        //    }
        //    catch (Exception ex)
        //    {
        //        ExceptionModule.ExceptionPublish(ex, Sites.ToLong(Session["DomainID"]), Sites.ToLong(Session["UserContactID"]), Sites.ToLong(Session["SiteId"]), Request);
        //        ShowMessage(ex.ToString(), 1);
        //    }
        //}

        #endregion

    }
}