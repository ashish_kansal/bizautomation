﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="NewOnePageCheckout.ascx.cs" Inherits="BizCart.UserControls.NewOnePageCheckout" %>
<%@ Import Namespace="BACRM.BusinessLogic.Common" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>

<script src="../Js/jquery.newonepagecheckout.js"></script>
<style>

    .itemstylePadding {
        padding-left: 5px;
    }

    .headerstyleAlignment {
        text-align: center;
    }

    .gridViewPager td {
        padding-left: 4px;
        padding-right: 4px;
        padding-top: 1px;
        padding-bottom: 2px;
    }

    .Hide {
        display: none;
    }

    .RadWindow .rwTable
    {
        margin: 400px 8px 58px 100px !important; 
    }
    .RadWindow .rwPopupButton
    {
        margin: 60px 8px 58px 100px !important; /* 55px moves the OK button to the left edge of the window, increase or decrease the value to move left or right */
    }
</style>

<script type="text/javascript">
    function ShowRelatedPostSellItems() {
        $("#divRelatedPostSellItems").modal('show');
    }

    function ShowFreePostSellItems() {
        $("#divFreePostSellItems").modal('show');
    }

    function checkAll(objRef) {
        var GridView = objRef.parentNode.parentNode.parentNode;
        var inputList = GridView.getElementsByTagName("input");
        for (var i = 0; i < inputList.length; i++) {
            //Get the Cell To find out ColumnIndex
            var row = inputList[i].parentNode.parentNode;
            if (inputList[i].type == "checkbox" && objRef != inputList[i]) {
                if (objRef.checked) {
                    //If the header checkbox is checked
                    //check all checkboxes
                    //and highlight all rows
                    inputList[i].checked = true;
                }
                else {
                    //If the header checkbox is checked
                    //uncheck all checkboxes
                    inputList[i].checked = false;
                }
            }
        }
    }

    function Confirm() {
        if ($('#<%=gvPromotionPostCheckOut.ClientID%> input:checkbox:checked').length <= 0) {
            alert('Please select atleast one item.');
            return false;
        }
        else {
            return true;
        }
    }

    function ShowShippingChargeWindow() {
        LoadOverlay('body', 'Please wait a few seconds while we reach out to our carriers to get you the best shipping rates possible');
        GetShippingMethod($('#txtShipCode').val(), parseInt($('#ddlShipCountry').val()), parseInt($('#ddlShipState').val()));
        return false;
    }

    function ShippingRateChanged() {
        var selectedShippingCharge = $("input:radio[name='shippingmethod']:checked").val();

        if (selectedShippingCharge == null || selectedShippingCharge == "") {
            alert("Select shipping service.");
            return false;
        } else if (selectedShippingCharge == "0" && $("[id$=hdnIsStaticShippingCharge]").val() == "1") {
            $("[id$=hdnSelectedShippingCharge]").val("0");
        } else if (selectedShippingCharge != "0~0.00~0") {
            $("[id$=hdnSelectedShippingCharge]").val(selectedShippingCharge);
            $("[id$=hdnIsStaticShippingCharge]").val("0");
        } else {
            $("[id$=hdnSelectedShippingCharge]").val("-1");
        }

        UpdateTotal();

        $("#divShippingMethod").modal('hide');
        return false;
    }
</script>
<asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
<telerik:RadWindowManager RenderMode="Lightweight" ID="RadWindowManager1" runat="server"></telerik:RadWindowManager>  
<%--<div class="overlaypop" id="dialogSellPopup" runat="server" visible="false">
    <div class="overlaycontentpop">
        <div class="dialog-header">
            <div class="col-1-1">
                <div class="col-9-12" id="divPromotionDetailRI" runat="server" style="float: left; line-height: 1; display: inline;">
                    <div style="float: left; padding-left: 5px; line-height: 1; display: inline; padding-top: 8px; font-weight: bold;">
                        Last chance deal
                    </div>
                    <div style="float: left; line-height: 1; display: inline; padding-left: 50px;">
                        <img src="../images/Admin-Price-Control.png" height="30" alt="" />
                    </div>
                    <div style="float: left; line-height: 1; display: inline; padding-top: 8px; font-weight: bold;">
                        Because of the items ordered, you qualify for an additional discount on the following items: 
                    </div>
                </div>
                <div style="float: right; padding: 7px;">
                    <asp:button id="btnNoThanks" onclick="btnNoThanks_Click" runat="server" cssclass="button" text="No Thanks" />
                    <asp:button id="btnFinished" onclick="btnFinished_Click" runat="server" cssclass="button" text="Finished adding items to order" />
                </div>
            </div>
        </div>
        <div class="dialog-body">
            <asp:gridview id="gvPromotionPostCheckOut" runat="server" datakeynames="numItemCode,numParentItemCode" autogeneratecolumns="false" width="1100px" cellpadding="3" cellspacing="3" bordercolor="#f0f0f0" onrowcommand="gvPromotionPostCheckOut_RowCommand" onrowdatabound="gvPromotionPostCheckOut_RowDataBound">
                <RowStyle BorderColor="#CACACA" BorderStyle="Solid" BorderWidth="1" />
                <HeaderStyle HorizontalAlign="Center" Font-Bold="true" Font-Size="10" BackColor="#e8e8e8" BorderColor="#CACACA" BorderStyle="Solid" BorderWidth="1" />
                <Columns>
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:Image ID="imgPath" runat="server" ImageUrl='<%# Eval("vcPathForTImage")%>' Width="80"/>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Center" Width="100px" Height="56px" />
                    </asp:TemplateField>
                    <asp:BoundField DataField="vcItemName" HeaderText="Item" />
                    <asp:BoundField DataField="txtItemDesc" HeaderText="Description" />
                    <asp:BoundField DataField="vcRelationship" HeaderText="Relation to selected item(s)" />
                    <asp:TemplateField HeaderText="Qty" ItemStyle-HorizontalAlign="Center" ItemStyle-Wrap="false">
                        <ItemTemplate>
                            <table>
                                <tr>
                                    <td>
                                        <asp:TextBox ID="txtQty" runat="server" Width="50" Text="1"></asp:TextBox></td>
                                    <td>/ </td>
                                    <td><%# Eval("vcUOMName")%></td>
                                </tr>
                            </table>
                            <asp:HiddenField ID="hdnWarehouseID" runat="server" Value='<%# Eval("numWareHouseItemID")%>' />
                            <asp:HiddenField ID="hdnPrice" runat="server" Value='<%# Eval("monListPrice")%>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="monListPrice" HeaderText="Unit List" ItemStyle-HorizontalAlign="Center" />
                    <asp:BoundField DataField="vcProName" HeaderText="Promotion Name" ItemStyle-HorizontalAlign="Center" />
                    <asp:BoundField DataField="vcPromotionRule" HeaderText="Promotion Details" ItemStyle-HorizontalAlign="Center" />
                    <asp:TemplateField HeaderText="Total" ItemStyle-HorizontalAlign="Center" ItemStyle-Wrap="false">
                        <ItemTemplate>
                            <asp:Label ID="lblTotal" runat="server" Width="50" Text="1"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField ItemStyle-HorizontalAlign="Center" ItemStyle-Width="25">
                        <ItemTemplate>
                            <asp:Button ID="btnAdd" CommandName="Add" CssClass="button" Text="Add" runat="server" OnClientClick="Confirm()" CommandArgument='<%# Eval("numItemCode") %>'></asp:Button>
                            <asp:Label ID="lblOutOfStock" runat="server" Width="100" Text="Out of stock" Visible="false"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="numOnHand" Visible="false" />
                </Columns>
            </asp:gridview>
        </div>
    </div>
</div>--%>

<div id="divRelatedPostSellItems" class="modal fade modal-minicart" role="dialog">
    <div class="modal-dialog modal-dialog-minicart" style="width: 90%">
        <div class="modal-content modal-content-minicart">
            <div id="dialogSellPopup" runat="server" visible="false">
                <div>
                    <div class="modal-header modal-content-minicart">
                        <%--<div class="col-1-1">--%>
                        <div class="pull-left">
                            <h4 class="modal-title modal-title-minicart" style="font-weight: bold;">Last chance deal 
                            </h4>
                        </div>
                        <%-- <div style="float: left; padding-left: 5px; line-height: 1; display: inline; padding-top: 8px; font-weight: bold;font-size:small;">
                        Last chance deal
                    </div>--%>
                        <%--<div style="float: left; line-height: 1; display: inline; padding-left: 50px;">
                            <img src="../images/Admin-Price-Control.png" height="30" alt="" />
                        </div>--%>
                        <div style="float: left; line-height: 1; display: inline; padding-left: 50px;" id="divPromotion" runat="server" visible="false">
                            <img src="/Images/Admin-Price-Control.png" height="30" alt="" />
                            <asp:Label ID="lblVcPromotion" runat="server"></asp:Label>
                        </div>
                        <%--<div style="float: left; line-height: 1; display: inline; padding-top: 8px; font-size: small;">
                            Because of the items ordered, you qualify for an additional discount on the following items: 
                        </div>--%>
                        <div style="float: right; padding: 7px;">
                            <asp:Button ID="btnAdd" OnClick="btnAdd_Click" CssClass="button btn btn-primary" Text="Add" runat="server" OnClientClick="return Confirm()"></asp:Button>
                            <asp:Button ID="btnNoThanks" OnClick="btnNoThanks_Click" runat="server" CssClass="button btn btn-primary" Text="No Thanks" />
                            <%--<asp:Button ID="btnFinished" OnClick="btnFinished_Click" runat="server" CssClass="button" Text="Finished adding items to order" />--%>
                        </div>
                        <%-- </div>--%>
                    </div>
                    <div class="modal-body modal-body-minicart">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="table-responsive" style="align-self: center;">
                                    <asp:GridView ID="gvPromotionPostCheckOut" runat="server" AllowPaging="true" DataKeyNames="numItemCode,numParentItemCode" AutoGenerateColumns="false" PagerStyle-Font-Italic="true"
                                        PagerStyle-CssClass="gridViewPager" PagerStyle-Font-Underline="true" PageSize="8" PagerStyle-HorizontalAlign="Right" OnPageIndexChanging="OnPageIndexChanging" Width="98%" CellPadding="3" CellSpacing="3" BorderColor="#f0f0f0" OnRowDataBound="gvPromotionPostCheckOut_RowDataBound">
                                        <RowStyle BorderColor="#CACACA" BorderStyle="Solid" BorderWidth="1" />
                                        <HeaderStyle HorizontalAlign="Center" Font-Bold="true" Font-Size="10" BackColor="#e8e8e8" BorderColor="#CACACA" BorderStyle="Solid" BorderWidth="1" />
                                        <Columns>
                                            <asp:TemplateField>
                                                <%-- <ItemTemplate>
                                                    <%#CCommon.GetImageHTML(Eval("vcPathForTImage"), 1, 100, 56)%>
                                                </ItemTemplate>--%>
                                                <ItemTemplate>
                                                    <asp:Image ID="imgPath" runat="server" ImageUrl='<%# Eval("vcPathForTImage")%>' Width="80" />
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Center" Width="80px" Height="56px" />
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="vcItemName" HeaderText="Item" HeaderStyle-CssClass="headerstyleAlignment" HeaderStyle-Width="100" ItemStyle-CssClass="itemstylePadding" />
                                            <asp:BoundField DataField="txtItemDesc" HeaderText="Description" HeaderStyle-CssClass="headerstyleAlignment" ItemStyle-CssClass="itemstylePadding" />
                                            <asp:BoundField DataField="vcRelationship" HeaderText="Relation to selected item(s)" HeaderStyle-CssClass="headerstyleAlignment" ItemStyle-CssClass="itemstylePadding" HeaderStyle-Width="200" />
                                            <asp:TemplateField HeaderText="Qty" ItemStyle-HorizontalAlign="Center" ItemStyle-Wrap="false" HeaderStyle-CssClass="headerstyleAlignment" ItemStyle-CssClass="itemstylePadding" HeaderStyle-Width="100">
                                                <ItemTemplate>
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <asp:TextBox ID="txtQty" runat="server" Width="50" Text="1"></asp:TextBox></td>
                                                            <td>/ </td>
                                                            <td><%# Eval("vcUOMName")%></td>
                                                        </tr>
                                                    </table>
                                                    <asp:HiddenField ID="hdnWarehouseID" runat="server" Value='<%# Eval("numWareHouseItemID")%>' />
                                                    <asp:HiddenField ID="hdnPrice" runat="server" Value='<%# Eval("monListPrice")%>' />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="monListPrice" HeaderText="Unit Price" ItemStyle-HorizontalAlign="left" HeaderStyle-CssClass="headerstyleAlignment" HeaderStyle-Width="100" ItemStyle-CssClass="itemstylePadding" />
                                            <%--<asp:BoundField DataField="vcProName" HeaderText="Promotion Name" ItemStyle-HorizontalAlign="left" HeaderStyle-Width="200" HeaderStyle-CssClass="headerstyleAlignment" ItemStyle-CssClass="itemstylePadding" />
                                            <asp:BoundField DataField="vcPromotionRule" HeaderText="Promotion Details" ItemStyle-HorizontalAlign="left" HeaderStyle-CssClass="headerstyleAlignment" ItemStyle-CssClass="itemstylePadding" />--%>
                                            <asp:TemplateField HeaderText="Total" ItemStyle-HorizontalAlign="left" ItemStyle-Wrap="false" HeaderStyle-CssClass="headerstyleAlignment" ItemStyle-CssClass="itemstylePadding">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblTotal" runat="server" Width="100" Text="1"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="headerstyleAlignment">
                                                <HeaderTemplate>
                                                    <asp:CheckBox ID="checkAll" runat="server" Text="Select All" onclick="checkAll(this);" />
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chkAdd" runat="server" />
                                                    <%--<asp:Button ID="btnAdd" CommandName="Add" CssClass="button" Text="Add" runat="server" OnClientClick="Confirm()" CommandArgument='<%# Eval("numItemCode") %>'></asp:Button>--%>
                                                    <asp:Label ID="lblOutOfStock" runat="server" Width="85" Text="Out of stock" Visible="false"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="numOnHand" Visible="false" HeaderStyle-HorizontalAlign="Center" ItemStyle-CssClass="itemstylePadding" />
                                        </Columns>
                                    </asp:GridView>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="divFreePostSellItems" class="modal fade modal-minicart" role="dialog">
    <div class="modal-dialog modal-dialog-minicart" style="width: 50%">
        <div class="modal-content modal-content-minicart">
            <div id="flashPage" runat="server" visible="false">
                <div>
                    <div class="modal-header modal-content-minicart">
                        <div class="text-center">
                            <h4 class="modal-title modal-title-minicart" style="font-weight: bold;">
                            Your order qualifies you to receive the following offer:  
                            </h4>
                        </div>
                    </div>
                    <div class="modal-body modal-body-minicart">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="table-responsive" style="align-self: center;">
                                    <asp:GridView ID="gvItemsForRule" CssClass="table table-bordered table-striped" runat="server" AutoGenerateColumns="false" CellPadding="3" CellSpacing="3" BorderColor="#f0f0f0">
                                        <RowStyle BorderColor="#CACACA" BorderStyle="Solid" BorderWidth="1" />
                                        <HeaderStyle HorizontalAlign="Center" Font-Bold="true" Font-Size="10" BackColor="#e8e8e8" BorderColor="#CACACA" BorderStyle="Solid" BorderWidth="1" />
                                        <Columns>
                                            <asp:BoundField DataField="numItemCode" ItemStyle-CssClass="Hide" HeaderStyle-CssClass="Hide"/>
                                            <asp:BoundField DataField="monListPrice"   ItemStyle-CssClass="Hide" HeaderStyle-CssClass="Hide"/>
                                            <asp:BoundField DataField="vcItemName" HeaderText="Item Name" ItemStyle-Width="600" ItemStyle-Wrap="true" />
                                            <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Units">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblUnit" runat="server" Width="85" Text="1" ></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Your Price">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblFree" runat="server" Width="85" Text="Free" ></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer modal-content-minicart">
                        <div style="float: left; line-height: 1; display: inline; padding-top: 8px; font-size: small;">
                            <asp:Button ID="btnYesThanks" OnClick="btnYesThanks_Click" runat="server" CssClass="button btn btn-primary" Text="Yes, add it to my order thanks!" />
                        </div>
                        <div style="float: right; line-height: 1; display: inline; padding-top: 8px; font-size: small;">
                            <asp:Button ID="btnNo" OnClick="btnNo_Click" runat="server" CssClass="button btn btn-primary" Text="Thanks but not this time" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<asp:panel id="pnlCutomizeHtml" runat="server">
    <table border="0" width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td colspan="4">
                <div class="sectionheader">
                </div>
            </td>
        </tr>
        <tr id="trEditAddress" runat="server">
            <td class="LabelColumn">Billing Address:
            </td>
            <td class="ControlCell">
                <asp:DropDownList runat="server" ID="ddlBillAddressName" ClientIDMode="Static" CssClass="signup">
                </asp:DropDownList>
                <asp:CheckBox ID="chkBillPrimary" runat="server" Visible="false" />
            </td>
            <td class="LabelColumn">Shipping Address:
            </td>
            <td class="ControlCell">
                <asp:DropDownList runat="server" ID="ddlShipAddressName" ClientIDMode="Static" CssClass="signup">
                </asp:DropDownList>
                <asp:CheckBox ID="chkShipPrimary" runat="server" Visible="false" />
            </td>
        </tr>
        <tr>
            <td class="LabelColumn">First Name:
            </td>
            <td class="ControlCell">
                <asp:Label ID="lblBillingFirstName" ClientIDMode="Static" runat="server" Width="181px"></asp:Label>
            </td>
            <td class="LabelColumn">First Name:
            </td>
            <td class="ControlCell">
                <asp:Label ID="lblShippingFirstName" ClientIDMode="Static" runat="server" Width="181px"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="LabelColumn">Last Name:
            </td>
            <td class="ControlCell">
                <asp:Label ID="lblBillingLastName" ClientIDMode="Static" runat="server" Width="181px"></asp:Label>
            </td>
            <td class="LabelColumn">Last Name:
            </td>
            <td class="ControlCell">
                <asp:Label ID="lblShippingLastName" ClientIDMode="Static" runat="server" Width="181px"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="LabelColumn"></td>
            <td class="ControlCell"></td>
            <td class="LabelColumn">Phone:
            </td>
            <td class="ControlCell">
                <asp:TextBox ID="txtPhone" runat="server" CssClass="textbox" ClientIDMode="Static" Width="181px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="LabelColumn" valign="top">Address
                            <%--<asp:LinkButton Text="(Edit)" ID="lbBillEdit" runat="server" CssClass="signup" Visible="false" />:--%>
            </td>
            <%--<td class="ControlCell">
                <asp:Label ID="lblBillAddress" runat="server"></asp:Label>
            </td>--%>
            <td class="LabelColumn" valign="top">Address:
                            <%--<asp:LinkButton Text="(Edit)" ID="lbShipEdit" runat="server" CssClass="signup" Visible="false" />:--%>
            </td>
            <%--            <td class="ControlCell">
                <asp:Label ID="lblShipAddress" runat="server"></asp:Label>
            </td>--%>
        </tr>
        <tr id="trFirstTime" runat="server">
            <td colspan="4">
                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <%--<tr>
                        <th colspan="2">
                            <h3>Billing Address:</h3>
                        </th>
                        <th colspan="2">
                            <h3>Shipping Address:</h3>
                        </th>
                    </tr>--%>
                    <tr>
                        <td colspan="3"></td>
                        <td>
                            <asp:CheckBox ID="chkShippingAdd" ClientIDMode="Static" runat="server" Text=" Same as Billing Address" />
                            <asp:CheckBox ID="chkResidential" ClientIDMode="Static" runat="server" Text=" Residential" />
                        </td>
                    </tr>
                    <tr>
                        <td class="LabelColumn">Street:
                        </td>
                        <td class="ControlCell">
                            <asp:TextBox ID="txtBillStreet" ClientIDMode="Static" CssClass="textbox" runat="server" Width="181px"></asp:TextBox>
                        </td>
                        <td class="LabelColumn">Street:
                        </td>
                        <td class="ControlCell">
                            <asp:TextBox ID="txtShipStreet" ClientIDMode="Static" CssClass="textbox" runat="server" Width="181px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="LabelColumn">City:
                        </td>
                        <td class="ControlCell">
                            <asp:TextBox ID="txtBillCity" ClientIDMode="Static" CssClass="textbox" runat="server" Width="181px"></asp:TextBox>
                        </td>
                        <td class="LabelColumn">City:
                        </td>
                        <td class="ControlCell">
                            <asp:TextBox ID="txtShipCity" ClientIDMode="Static" CssClass="textbox" runat="server" Width="181px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="LabelColumn">Zip Code:
                        </td>
                        <td class="ControlCell">
                            <asp:TextBox ID="txtBillCode" ClientIDMode="Static" CssClass="textbox" runat="server" Width="182px"></asp:TextBox>
                        </td>
                        <td class="LabelColumn">Zip Code:
                        </td>
                        <td class="ControlCell">
                            <asp:TextBox ID="txtShipCode" ClientIDMode="Static" CssClass="textbox" runat="server" Width="182px" data-target="body"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="LabelColumn">Country:
                        </td>
                        <td class="ControlCell">
                            <asp:DropDownList runat="server" ClientIDMode="Static" ID="ddlBillCountry" CssClass="dropdown">
                            </asp:DropDownList>
                        </td>
                        <td class="LabelColumn">Country:
                        </td>
                        <td class="ControlCell">
                            <asp:DropDownList runat="server" ClientIDMode="Static" ID="ddlShipCountry" CssClass="dropdown">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="LabelColumn">State:
                        </td>
                        <td class="ControlCell">
                            <asp:DropDownList runat="server" ClientIDMode="Static" ID="ddlBillState" CssClass="dropdown">
                            </asp:DropDownList>
                        </td>
                        <td class="LabelColumn">State:
                        </td>
                        <td class="ControlCell">
                            <asp:DropDownList runat="server" ClientIDMode="Static" ID="ddlShipState" CssClass="dropdown" data-target="body">
                            </asp:DropDownList>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <%--<tr>
            <td colspan="4" align="center">
                <asp:Button ID="btnSubmit" runat="server"  ClientIDMode="Static" Text="Confirm & Save" CssClass="button" />
            </td>
        </tr>--%>
    </table>
    <asp:TextBox runat="server" ID="txtCustomerPoNumber" ClientIDMode="Static" CssClass="textbox" MaxLength="100" />
    <asp:TextBox runat="server" TextMode="MultiLine" ClientIDMode="Static" ID="txtComments" CssClass="textarea"
        MaxLength="1500" />

    <asp:Button runat="server" ID="btnGetShippingRates" CssClass="btn btn-primary btn-getshippingrates" Text="Get Shipping Rates" OnClientClick="return ShowShippingChargeWindow();" />

    <asp:LinkButton ID="lbtnEdit" Text="Edit" ClientIDMode="Static" runat="server" OnClick="lbtnEdit_Click">
    </asp:LinkButton>
    <asp:LinkButton ID="lbtnCustomerInformationEdit" ClientIDMode="Static" Text="Edit" runat="server">
    </asp:LinkButton>
    <asp:LinkButton ID="lbtnCartEdit" ClientIDMode="Static" Text="Edit" runat="server">
    </asp:LinkButton>
    <asp:Button ID="btnOrders" ClientIDMode="Static" runat="server" Visible="false" CssClass="button" Text="View Past Orders"
        OnClick="btnOrders_Click"></asp:Button>
    <asp:HiddenField ID="hdnShippingMethod" ClientIDMode="Static" runat="server" />

    <asp:Label runat="server" ID="lblSubTotal" ClientIDMode="Static" />
    <asp:Label runat="server" ID="lblDiscount" ClientIDMode="Static" />
    <asp:Label runat="server" ID="lblTax" ClientIDMode="Static" />
    <asp:Label runat="server" ID="lblShippingCharge" ClientIDMode="Static" />
    <asp:Label runat="server" ID="lblTotal" ClientIDMode="Static" />
    <asp:Label runat="server" ID="lblCurrencySymbol" ClientIDMode="Static" />

    <table width="100%" align="center" border="0" cellpadding="5" cellspacing="5">
        <tr>
            <td colspan="2">
                <div class="sectionheader">
                </div>
            </td>
        </tr>
        <tr>
            <td></td>
            <td align="right" class="EditLink">
                <a id="hplEditCart" href="/Cart.aspx">Edit Shopping Cart</a> <a id="hplEditAddres"
                    href="/ConfirmAddress.aspx">Edit Shipping Details</a> <a id="hplEditPayment" href="/AddPayment.aspx">Edit Cards Details</a>
            </td>
        </tr>
        <tr>
            <td align="left" colspan="2"></td>
        </tr>
        <tr>
            <td colspan="2" align="left">
                <table align="left" runat="server" id="tblBillme" visible="false">
                    <tr>
                        <td class="LabelColumn">Total Balance Due :
                        </td>
                        <td class="ControlCell">
                            <asp:Label ID="lblBalDue" ClientIDMode="Static" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="LabelColumn">Total Remaining Credit :
                        </td>
                        <td class="ControlCell">
                            <asp:Label ID="lblRemCredit" ClientIDMode="Static" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="LabelColumn">Total Amount Past Due :
                        </td>
                        <td class="ControlCell">
                            <asp:Label ID="lblAmtPastDue" ClientIDMode="Static" runat="server"></asp:Label>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="left" colspan="2">
                <%--  <asp:RadioButton ID="radPay" Checked="true" AutoPostBack="true" runat="server" GroupName="rad"
                    Text='&nbsp;<b>Pay by Credit Card</b>'>
                </asp:RadioButton>--%>
            </td>
        </tr>
        <tr>
            <td colspan="2" align="left">
                <table id="tblCustomerInfo" width="90%" runat="server">
                    <tr>
                        <td width="50%">
                            <table cellspacing="2" cellpadding="5">
                                <tr>
                                    <th nowrap colspan="4">Credit Card Information
                                    </th>
                                </tr>
                                <tr>
                                    <td nowrap align="right">Card Type:
                                    </td>
                                    <td>
                                        <asp:DropDownList runat="server" ID="ddlCardType" CssClass="dropdown">
                                            <%--   <asp:ListItem Value="1">Visa </asp:ListItem>
                                        <asp:ListItem Value="2">MasterCard</asp:ListItem>
                                        <asp:ListItem Value="3">AMEX</asp:ListItem>
                                        <asp:ListItem Value="4">Discover</asp:ListItem>
                                        <asp:ListItem Value="5">Diners </asp:ListItem>
                                        <asp:ListItem Value="6">JCB</asp:ListItem>
                                        <asp:ListItem Value="7">BankCard</asp:ListItem>--%>
                                        </asp:DropDownList>
                                    </td>
                                    <td nowrap align="right">Card Number:
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtCardNumber" AutoCompleteType="None" CssClass="textbox" runat="server" Width="180px" MaxLength="16"></asp:TextBox>

                                    </td>
                                </tr>
                                <tr>
                                    <td nowrap align="right">Card Holder Name:
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtCHName" Width="180" runat="server" CssClass="signup" MaxLength="100"></asp:TextBox>
                                    </td>
                                    <td nowrap align="right">CVV Data:
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtCardCVV2" CssClass="textbox" runat="server" Width="117px" TextMode="Password"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" nowrap>Exp Date (MM/YY):
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddlCardExpMonth" CssClass="dropdown" runat="server">
                                            <asp:ListItem Value="01">01</asp:ListItem>
                                            <asp:ListItem Value="02">02</asp:ListItem>
                                            <asp:ListItem Value="03">03</asp:ListItem>
                                            <asp:ListItem Value="04">04</asp:ListItem>
                                            <asp:ListItem Value="05">05</asp:ListItem>
                                            <asp:ListItem Value="06">06</asp:ListItem>
                                            <asp:ListItem Value="07">07</asp:ListItem>
                                            <asp:ListItem Value="08">08</asp:ListItem>
                                            <asp:ListItem Value="09">09</asp:ListItem>
                                            <asp:ListItem Value="10">10</asp:ListItem>
                                            <asp:ListItem Value="11">11</asp:ListItem>
                                            <asp:ListItem Value="12">12</asp:ListItem>
                                        </asp:DropDownList>
                                        &nbsp;/&nbsp;
                                        <asp:DropDownList ID="ddlCardExpYear" CssClass="dropdown" runat="server">
                                        </asp:DropDownList>
                                    </td>
                                    <td align="right" nowrap>&nbsp;
                                    </td>
                                    <td>&nbsp;
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="left" colspan="2"></td>
            <td valign="top">
                <table align="left" width="100%">
                    <tr id="trApplyCouponCode" runat="server">
                        <td class="text_bold">
                            <br />
                            Apply Coupon/Discount Code:
                                        <asp:TextBox ID="txtCouponCode" runat="server" CssClass="textbox" MaxLength="20"></asp:TextBox>&nbsp;<asp:Button
                                            ID="btnCouponCode" runat="server" Text="Apply" CssClass="button" OnClick="btnCouponCode_Click" />
                        </td>
                    </tr>
                    <tr id="trUsedCouponCode" runat="server" visible="false">
                        <td class="text_bold">
                            <br />
                            Coupon/Discount Code:
                                        <asp:Label ID="lblCouponCode" runat="server"></asp:Label>&nbsp;<asp:LinkButton ID="lbCouponCodeRemove"
                                            runat="server" OnClick="lbCouponCodeRemove_Click">Remove</asp:LinkButton>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:DropDownList ID="ddlShipVia" runat="server"></asp:DropDownList>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="2" align="left">
                <table id="tblGoogleCheckout" runat="server" width="90%">
                    <tr>
                        <td width="50%">Google Checkout
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="left" colspan="2"></td>
        </tr>
        <tr>
            <td colspan="2" align="left">
                <table id="tblPayPal" runat="server" width="90%">
                    <tr>
                        <td width="50%">PayPal
                        </td>
                    </tr>
                    <tr>

                        <td nowrap align="right">PayPal Email :
                        </td>
                        <td>
                            <asp:TextBox ID="txtPayPalEmail" AutoCompleteType="None" CssClass="textbox" runat="server" Width="180px"></asp:TextBox>

                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="left" colspan="2"></td>
        </tr>
        <tr>
            <td colspan="2" align="left">
                <table id="tblSalesInquiry" runat="server" width="90%">
                    <tr>
                        <td width="50%">Sales Inquiry
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="2" align="left">
                <div id="tdTotalAmount">
                    Total Amount:
                </div>
            </td>
        </tr>
        <tr>
            <td></td>
            <td align="left"></td>
        </tr>
    </table>
    <asp:PlaceHolder ID="plhFormControls" runat="server"></asp:PlaceHolder>

</asp:panel>


<asp:hiddenfield id="hdnCardNumber" value="" clientidmode="Static" runat="server" />
<asp:hiddenfield id="hdnPaymentMethod" value="" clientidmode="Static" runat="server" />
<asp:hiddenfield id="hdnShippingService" value="" clientidmode="Static" runat="server" />
<asp:hiddenfield id="hdnBillState" value="" clientidmode="Static" runat="server" />
<asp:hiddenfield id="hdnShipState" value="" clientidmode="Static" runat="server" />
<asp:literal id="litCutomizeHtml" runat="server"></asp:literal>
<asp:textbox id="txtCredit" runat="server" style="display: none"></asp:textbox>
<asp:textbox id="txtDueAmount" runat="server" style="display: none"></asp:textbox>
<asp:hiddenfield id="hfFirstTime" runat="server" />
<asp:hiddenfield id="hfEditAddress" runat="server" />
<asp:hiddenfield id="hdnUId" clientidmode="Static" runat="server" />
<asp:hiddenfield id="hdnDiscountShipping" runat="server" clientidmode="Static" />
<asp:hiddenfield id="hdnCategory" runat="server" />
<asp:hiddenfield id="hdnIsFinishedPostCheckout" runat="server" />
<asp:textbox id="hdnSQty" cssclass="hdnSQty" style="display: none;" value="1" runat="server"></asp:textbox>
<span style="display: none">
    <asp:fileupload id="FluDocument" runat="server" />
</span>
<asp:HiddenField ID="hdnIsStaticShippingCharge" runat="server" />
<asp:HiddenField ID="hdnStaticShippingCharge" runat="server" />
<asp:HiddenField ID="hdnSelectedShippingCharge" runat="server" />
<asp:HiddenField ID="hdnOrderPromotionID" runat="server" />
<asp:HiddenField ID="hdnDiscountCodeID" runat="server" />