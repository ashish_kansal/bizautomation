﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BACRM.BusinessLogic.Admin;
using BACRM.BusinessLogic.Common;
using BACRM.BusinessLogic.ShioppingCart;

namespace BizCart.UserControls
{
    public partial class ResetPassword : BizUserControl
    {
        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!Page.IsPostBack)
                {
                    Sites.InitialiseSession();
                }

                if (Request.QueryString["resetId"] != null && !string.IsNullOrEmpty(Convert.ToString(Request.QueryString["resetId"])))
                {
                    UserAccess objUserAccess = new UserAccess();
                    objUserAccess.DomainID = Sites.ToLong(Session["DomainID"]);

                    QueryStringValues objEncryption = new QueryStringValues();

                    objUserAccess.Password = objEncryption.Decrypt(Convert.ToString(Request.QueryString["resetId"]));
                    if (!objUserAccess.IsValidResetPasswordLink())
                    {
                        ShowMessage(GetErrorMessage("ERR078"), 1);
                        btnSave.Visible = false;
                    }
                }
                else
                {
                    Response.Redirect(Session["SitePath"].ToString() + "/Login.aspx");
                }

                bindHtml();
            }
            catch (Exception ex)
            {
                ExceptionModule.ExceptionPublish(ex, Sites.ToInteger(Session["DomainID"]), Sites.ToInteger(Session["UserContactID"]), Sites.ToInteger(Session["SiteId"]), Request);
                ShowMessage(ex.ToString(), 1);
            }
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (Request.QueryString["resetId"] != null && !string.IsNullOrEmpty(Convert.ToString(Request.QueryString["resetId"])))
                {
                    QueryStringValues objEncryption = new QueryStringValues();
                    UserAccess objUserAccess = new UserAccess();
                    CCommon objCommon = new CCommon();
                    objUserAccess.DomainID = Sites.ToLong(Session["DomainID"]);
                    objUserAccess.Password = objEncryption.Decrypt(Convert.ToString(Request.QueryString["resetId"]));
                    objUserAccess.NewPassword = objCommon.Encrypt(txtNewPassword.Text.Trim());

                    try
                    {
                        objUserAccess.ResetPassword();
                        ShowMessage(GetErrorMessage("ERR012"), 0); //Password has been successfully changed
                    }
                    catch (Exception ex)
                    {
                        if (ex.Message.Contains("INVALID_RESET_LINK"))
                        {
                            ShowMessage(GetErrorMessage("ERR078"), 1);
                        }
                        else
                        {
                            throw;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionModule.ExceptionPublish(ex, Sites.ToInteger(Session["DomainID"]), Sites.ToInteger(Session["UserContactID"]), Sites.ToInteger(Session["SiteId"]), Request);
                ShowMessage(ex.ToString(), 1);
            }
        }

        #endregion

        #region Methods

        private void bindHtml()
        {
            try
            {
                string strUI = GetUserControlTemplate("ResetPassword.htm");
                strUI = strUI.Replace("##NewPassword##", CCommon.RenderControl(txtNewPassword));
                strUI = strUI.Replace("##NewPasswordConfirm##", CCommon.RenderControl(txtNewPasswordConfirm));
                strUI = strUI.Replace("##SaveButton##", CCommon.RenderControl(btnSave));
                pnlCutomizeHtml.Visible = false;
                litCutomizeHtml.Text = strUI;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion
    }
}