﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="WriteReview.ascx.cs"
    Inherits="BizCart.UserControls.WriteReview" %>
<%@ Register TagPrefix="captcha" Namespace="WebControlCaptcha" Assembly="WebControlCaptcha" %>
<%@ Import namespace ="BACRM.BusinessLogic.Common"%>

<%--<script type="text/javascript" src="../Js/jquery.xdomainajax.js"></script>--%>
<script language="javascript" type="text/javascript">
    $(function () {
        $('.auto-submit-star').rating({

            callback: function (value, link) {
                var SiteId = "";
                var DomainId = "";
                var ContactId = "";
                var IpAddress = "";
                var TypeId = "";
                var ReferenceId = 0;
                var RatingCount = 0;

                SiteId = $('#hfSiteId').val();
                DomainId = $('#hfDomainId').val();
                ContactId = $('#hfContactId').val();
                IpAddress = $('#hfIpAddress').val(); ;
                TypeId = $('#hfTypeId').val();
                ReferenceId = $('#hfItemId').val();
                RatingCount = value;

                $.ajax({
                    type: "POST",

                    url: "http://portal.bizautomation.com/Common/Common.asmx/ManageRatings",
                    data: '{ TypeId:"' + TypeId + '" , ReferenceId:"' + ReferenceId + '", RatingCount:"' + RatingCount + '", ContactId: "' + ContactId + '", IpAddress: "' + IpAddress + '", SiteId: "' + SiteId + '", DomainId: "' + DomainId + '"}', //1'" + RatingId + "'  + "', TypeId:'" + TypeId + "' , ReferenceId:'" + ReferenceId + "', RatingCount:'" + RatingCount + "', ContactId:'" + ContactId + "', IpAddress:'" + IpAddress + "' , SiteId:'" + SiteId + "', DomainId:'" + DomainId + "'}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (msg) {

                    }
                });
            }
        });
    });

</script>
<asp:Panel ID="pnlCutomizeHtml" runat="server">
    <table>
        <tr>
            <td>
                <asp:TextBox ID="txtTitle" Width="500px" MaxLength="50" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                <asp:TextBox ID="txtReview" Width="500px" MaxLength="50" Height="170px" runat="server"
                    TextMode="MultiLine"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                <asp:CheckBox ID="cbReviewEmail" runat="server" />
            </td>
        </tr>
        <tr>
            <td>
                <captcha:CaptchaControl ID="CaptchaControl1" runat="server" CaptchaBackgroundNoise="Extreme"
                    CaptchaFontWarping="Medium"></captcha:CaptchaControl>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Button ID="btnSubmit" runat="server" CssClass="button" Text="Submit" OnClick="btnSubmit_Click">
                </asp:Button>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Literal ID="ltrlMsg" runat="server"></asp:Literal>
            </td>
        </tr>
    </table>
</asp:Panel>
<asp:HiddenField ID="hfRatingValue" runat="server" Value="" />
<asp:HiddenField ID="hfSiteId" ClientIDMode ="Static" runat="server" />
<asp:HiddenField ID="hfDomainId" ClientIDMode ="Static" runat="server" />
<asp:HiddenField ID="hfContactId" ClientIDMode ="Static" runat="server" />
<asp:HiddenField ID="hfIpAddress" ClientIDMode ="Static"  runat="server" Value="" />
<asp:HiddenField ID="hfTypeId" runat="server" ClientIDMode ="Static" Value="" />
<asp:HiddenField ID="hfItemId" runat="server" Value="" ClientIDMode ="Static"  />



<asp:Literal ID="litCutomizeHtml" runat="server"></asp:Literal>
<div style="display: none;">
</div>
