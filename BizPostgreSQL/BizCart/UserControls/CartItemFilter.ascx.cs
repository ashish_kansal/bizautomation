﻿using System;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BACRM.BusinessLogic.Item;
using BACRM.BusinessLogic.ShioppingCart;
using BACRM.BusinessLogic.Common;
using System.Text.RegularExpressions;
using System.Text;
using System.Collections;
using BACRM.BusinessLogic.Admin;
using BACRM.BusinessLogic.Opportunities;


namespace BizCart.UserControls
{
    public partial class CartItemFilter : BizUserControl
    {

        public Boolean IsControlGenerated
        {
            get
            {
                object o = ViewState["IsControlGenerated"];
                if (o != null)
                {
                    return (Boolean)o;
                }
                else
                {
                    return false;
                }
            }
            set
            {
                ViewState["IsControlGenerated"] = value;
            }
        }
        #region Events
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                Sites.InitialiseSession();

                if (IsPostBack)
                {
                    if (Sites.ToString(Request["__EVENTTARGET"]) == "btnFilter")
                    {
                        //DeleteItem();
                        btnFilter_Click(btnFilter, null);
                    }

                }
            }
            catch (Exception ex)
            {
                ExceptionModule.ExceptionPublish(ex, Sites.ToLong(Session["DomainID"]), Sites.ToLong(Session["UserContactID"]), Sites.ToLong(Session["SiteId"]), Request);
                ShowMessage(ex.ToString(), 1);
            }
        }

        protected void btnFilter_Click(object sender, EventArgs e)
        {
            try
            {
                //((CheckBoxList)tblMain.Rows[01].Cells[1].FindControl("273~numItemClassification~False"))

                CItems objItem = new CItems();
                DataSet ds = new DataSet();
                objItem.DomainID = CCommon.ToInteger(Session["domainId"]);
                objItem.FormID = 77;
                objItem.SiteID = Sites.ToLong(Session["SiteId"]);
                ds = objItem.GetCartFilters();

                string strQuery = "";
                //string[] strCustomQuery = { "" };
                string[] strCustomWhere = { "" };
                string strCustomFieldIDs = "";
                int iCnt = 0;
                int iCustomRowCnt = 0;
                string strAlias = "";
                bool IsElasticSearchEnable = CCommon.ToBool(Sites.GetSettingFieldValue(CCommon.ToLong(Session["DomainID"]), CCommon.ToLong(Session["SiteID"]), "bitElasticSearch"));

                if (IsElasticSearchEnable == false && Session["OverrideElasticSearch"] != null && CCommon.ToBool(Session["OverrideElasticSearch"]) == true)
                {
                    IsElasticSearchEnable = true;
                }

                //strCustomQuery = new string[Sites.ToInteger(ds.Tables[0].Select("vcAssociatedControlType = 'SelectBox' AND bitCustomField = true").Length)];
                strCustomWhere = new string[Sites.ToInteger(ds.Tables[0].Select("vcAssociatedControlType = 'SelectBox' AND bitCustomField = true").Length)];

                foreach (DataRow dr in ds.Tables[0].Select("vcAssociatedControlType = 'SelectBox'"))
                {
                    if (Sites.ToString(dr["vcLookBackTableName"]).ToLower() == "item")
                    {
                        strAlias = "I";
                    }
                    else if (Sites.ToString(dr["vcLookBackTableName"]).ToLower() == "category")
                    {
                        strAlias = "IC";
                    }

                    if (tblMain.Rows.Count >= iCnt)
                    {
                        int iTableRowCnt = 0;
                        CheckBoxList chkList = new CheckBoxList();
                        for (iTableRowCnt = 0; iTableRowCnt <= tblMain.Rows.Count - 1; iTableRowCnt++)
                        {
                            if (tblMain.Rows[iTableRowCnt].Cells.Count <= 1)
                            {
                                continue;
                            }
                            chkList = (CheckBoxList)tblMain.Rows[iTableRowCnt].FindControl(CCommon.ToString(dr["numFieldID"]) + "~" + CCommon.ToString(dr["vcOrigDbColumnName"]) + "~" + CCommon.ToString(dr["bitCustomField"]));

                            if (chkList != null)
                            {
                                string strValues = "";
                                foreach (ListItem item in chkList.Items)
                                {
                                    if (item.Selected == true)
                                    {
                                        if (IsElasticSearchEnable)
                                        {
                                            strValues = strValues + "," + CCommon.ToString(item);
                                        }
                                        else
                                        {
                                            strValues = strValues + "," + CCommon.ToString(item.Value);
                                        }
                                    }
                                }

                                strValues = strValues.Trim(',');
                                if (CCommon.ToBool(dr["bitCustomField"]) == true)
                                {
                                    if (strValues.Length > 0)
                                    {
                                        //strCustomQuery[iCustomRowCnt] = " (t1.FLD_ID = " + CCommon.ToString(dr["numFieldID"]) + " AND t2.Fld_Value IN (" + strValues + "))";
                                        strCustomWhere[iCustomRowCnt] = "[" + CCommon.ToString(dr["numFieldID"]) + "]" + " IN (" + strValues + ")";

                                        strCustomFieldIDs = strCustomFieldIDs + "," + "[" + CCommon.ToString(dr["numFieldID"]) + "]";
                                    }
                                    else
                                    {
                                        //strCustomQuery[iCustomRowCnt] = " 1=1 ";
                                        strCustomWhere[iCustomRowCnt] = " 1=1 ";
                                    }
                                    iCustomRowCnt = iCustomRowCnt + 1;
                                    break;
                                }
                                else
                                {
                                    if (strValues.Length > 0)
                                    {
                                        strQuery = strQuery + " AND " + strAlias + "." + CCommon.ToString(dr["vcOrigDbColumnName"]) + " IN (" + strValues + ") ";
                                    }
                                    else
                                    {
                                        strQuery = strQuery + " AND 1=1 ";
                                    }
                                    break;
                                }

                            }
                        }
                    }
                    iCnt = iCnt + 1;
                }

                if (string.IsNullOrEmpty(strCustomFieldIDs.Trim(',')) != true)
                {
                    //Session["fcc"] = string.Join(" AND ", strCustomQuery);
                    Session["fcwhere"] = string.Join(" AND ", strCustomWhere);
                    Session["fcfields"] = strCustomFieldIDs.Trim(',');
                }
                else
                {
                    //Session["fcc"] = null;
                    Session["fcwhere"] = null;
                    Session["fcfields"] = null;
                }
                if (strQuery.Length > 0)
                {
                    Session["frc"] = strQuery;
                }
                else
                {
                    Session["frc"] = null;
                }

                ViewState["IsControlGenerated"] = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        protected override void OnInit(EventArgs e)
        {
            try
            {
                if (Sites.ToBool(ViewState["IsControlGenerated"]) == false)
                {
                    GenerateFilters();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #region Methods
        void GenerateFilters()
        {
            try
            {
                //string strUI = GetUserControlTemplate("CartItemFilters.htm");
                CItems objItem = new CItems();
                DataSet ds = new DataSet();

                objItem.DomainID = CCommon.ToInteger(Session["domainId"]);
                objItem.FormID = 77;
                objItem.SiteID = Sites.ToLong(Session["SiteId"]);
                ds = objItem.GetCartFilters();

                //StringBuilder sb = new StringBuilder();
                //string pattern = "<##FOR_PARENT##>(?<Content>([\\s\\S]*?))<##ENDFOR_PARENT##>";

                #region mainIf
                //if (Regex.IsMatch(strUI, pattern))
                //{
                //MatchCollection matches = Regex.Matches(strUI, pattern);
                //if ((matches.Count > 0))
                //{
                //string Template;

                string strHeader = "";
                int iCnt = 0;
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    if (strHeader != CCommon.ToString(dr["vcFieldName"]))
                    {
                        strHeader = CCommon.ToString(dr["vcFieldName"]);
                        //Template = matches[0].Groups["Content"].Value;
                        DataRow[] drMain = ds.Tables[1].Select("numFieldID = " + CCommon.ToString(dr["numFieldID"]));
                        if (drMain.Length > 0)
                        {
                            //chkCheckBoxList = new CheckBoxList[drMain.Length];
                            iCnt = iCnt + 1;
                            //Template = Template.Replace("##FilterName##", strHeader);

                            DataTable dtList = new DataTable();
                            dtList = ds.Tables[1].Clone();
                            foreach (DataRow drRow in drMain)
                            {
                                dtList.ImportRow(drRow);
                                dtList.AcceptChanges();
                            }

                            CheckBoxList chkList = new CheckBoxList();
                            chkList.CellPadding = 2;
                            chkList.CellSpacing = 2;
                            chkList.ClientIDMode = ClientIDMode.AutoID;
                            chkList.EnableViewState = true;
                            chkList.ViewStateMode = System.Web.UI.ViewStateMode.Enabled;
                            chkList.Attributes.Add("onclick", "DoFilterPostBack()");
                            chkList.ID = CCommon.ToString(dr["numFieldID"]) + "~" + CCommon.ToString(dr["vcOrigDbColumnName"]) + "~" + CCommon.ToString(dr["bitCustomField"]);
                            chkList.DataTextField = "vcText";
                            chkList.DataValueField = "vcValue";
                            chkList.DataSource = dtList;
                            chkList.DataBind();

                            Panel plcCheckBoxList = new Panel();
                            plcCheckBoxList.ID = CCommon.ToString(dr["vcOrigDbColumnName"]);
                            plcCheckBoxList.CssClass = "productfilter";
                            plcCheckBoxList.Controls.Add(chkList);

                            TableHeaderRow tblrow1 = default(TableHeaderRow);
                            TableCell tblcell1 = default(TableCell);
                            tblrow1 = new TableHeaderRow();
                            tblcell1 = new TableCell();
                            tblcell1.ColumnSpan = 2;
                            tblcell1.Text = "<span class=" + '"' + "productfilterlabel" + '"' + ">" + strHeader + "</span>";
                            tblrow1.Cells.Add(tblcell1);
                            tblMain.Rows.Add(tblrow1);

                            TableRow tblrow2 = default(TableRow);
                            TableCell tblcell2 = default(TableCell);
                            tblrow2 = new TableRow();
                            tblcell2 = new TableCell();
                            tblcell2.ColumnSpan = 2;
                            tblcell2.Controls.Add(plcCheckBoxList);
                            tblrow2.Cells.Add(tblcell2);
                            tblMain.Rows.Add(tblrow2);

                            TableRow tblrow3 = default(TableRow);
                            TableCell tblcell3 = default(TableCell);
                            tblrow3 = new TableRow();
                            tblcell3 = new TableCell();
                            tblcell3.Text = "<br/>";
                            tblcell3.ColumnSpan = 2;
                            tblrow3.Cells.Add(tblcell3);
                            tblMain.Rows.Add(tblrow3);
                            //Template = Template.Replace("##FilterControl##", CCommon.RenderControl(plcCheckBoxList));
                            //sb.Append(Template);

                            //Label lblCheckBox = new Label();
                            //lblCheckBox.ID = CCommon.ToString(dr["vcOrigDbColumnName"]) + "~" + CCommon.ToString(dr["bitCustomField"]);
                            //lblCheckBox.Text = "<table id=" + '"' + "tbl" + Sites.ToInteger(iCnt) + '"' + ">";
                            //foreach (DataRow drRow in drMain)
                            //{
                            //    lblCheckBox.Text = lblCheckBox.Text + "<tr><td><input id=" + '"' + CCommon.ToString(dr["vcOrigDbColumnName"]) + "_" + CCommon.ToString(drRow["vcValue"]) + '"' + "type=" + '"' + "checkbox" + '"' + " runat=" + '"' + "server" + '"' + " /><label for=" + '"' + CCommon.ToString(dr["vcOrigDbColumnName"]) + "_" + CCommon.ToString(drRow["vcValue"]) + '"' + ">" + CCommon.ToString(drRow["vcText"]) + "</label></td></tr>";
                            //}
                            //lblCheckBox.Text = lblCheckBox.Text + "</table>";
                            //Template = Template.Replace("##FilterControl##", lblCheckBox.Text);
                            //sb.Append(Template);
                        }
                    }
                }
                //}
                //strUI = Regex.Replace(strUI, pattern, sb.ToString());

                //}
                #endregion

                //pnlCutomizeHtml.Visible = false;
                //litCutomizeHtml.Text = strUI;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

    }
}