﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PermaLink.ascx.cs" Inherits="BizCart.UserControls.PermaLink" %>
<script language="javascript" type ="text/javascript">
    $(document).ready(function () {
        ReferenceId = $('#hfItemId').val();
        var ContactId1 = $('#hfContactId').val();

        $("a#HelpfulYes").bind('click', function () {
            var ReviewId = $(this).attr("rid");

            ManageReviewDetail(ReviewId, ContactId1, 1, true);
        });

        $("a#HelpfulNo").bind('click', function () {
            var ReviewId = $(this).attr("rid");
            ManageReviewDetail(ReviewId, ContactId1, 1, false);
        });

        $("a#ReportAbuse").bind('click', function () {
            var ReviewId = $(this).attr("rid");
            ManageReviewDetail(ReviewId, ContactId1, 0, true);

        });

    });

    function ManageReviewDetail(ReviewId, ContactId, Mode, ReportValue) {
        $.ajax({
            type: "POST",
            url: "http://portal.bizautomation.com/Common/Common.asmx/ManageReviewDetail",
            data: '{ ReviewId :"' + ReviewId + '",ContactId :"' + ContactId + '",Mode :' + Mode + ',ReportValue :' + ReportValue + '}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (msg) {

                if (Mode == 0) {
                    $(".mainReviewDiv:[reviewid=" + ReviewId + "]").find("div.Helpful_Vote").empty();
                    $(".mainReviewDiv:[reviewid=" + ReviewId + "]").find("div.Helpful_Vote").html(msg.d);

                    $('div.tempMsg').append($('#msgReport'));
                    $('div.tempMsg').append($('#msgVote'));
                    $("div.MsgThankForReporting[rid=" + ReviewId + "]").append($('#msgVote'));

                    $(".mainReviewDiv").unbind('mouseenter').unbind('mouseleave');

                }
                else if (Mode == 1) {

                    $(".mainReviewDiv:[reviewid=" + ReviewId + "]").find("div.Helpful_Vote").empty();
                    $(".mainReviewDiv:[reviewid=" + ReviewId + "]").find("div.Helpful_Vote").html(msg.d);

                    $('div.tempMsg').append($('#msgReport'));
                    $('div.tempMsg').append($('#msgVote'));
                    $("div.MsgThankForHelp[rid=" + ReviewId + "]").append($('#msgReport'));

                    $(".mainReviewDiv").unbind('mouseenter').unbind('mouseleave');

                }
            }
        });

    }

</script>
<asp:Panel ID="pnlCutomizeHtml" runat="server">
 
</asp:Panel>
<asp:Literal ID="litCutomizeHtml" runat="server"></asp:Literal>
<asp:HiddenField ID="hfContactId" runat="server" ClientIDMode="Static"   />