﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Subscribe.ascx.cs" Inherits="BizCart.UserControls.Subscrption" ClientIDMode="Static" %>
<script type="text/javascript" language="javascript">
    function validateEmail() {
        var emailRegEx = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        str = document.getElementById('txtSubscriberEmail').value;
        if (str.match(emailRegEx)) {
            return true;
        }
        else {
            alert('Please enter a valid email address.');
            return false;
        }
    }
</script>
<asp:Panel ID="pnlCutomizeHtml" runat="server">
    <div class="boxheader">
        Subscribe Newsletter</div>
    <div class="boxcontent">
        <table>
            <tr>
                <td colspan="3" align="left">
                    <asp:RadioButtonList ID="rblSubscription" runat="server">
                        <asp:ListItem Text="Subscribe" Value="1" Selected="True" />
                        <asp:ListItem Text="Unsubscribe" Value="0" />
                    </asp:RadioButtonList>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblSubscribe" runat="server" Text="Email"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtSubscriberEmail" runat="server" CssClass="textbox"></asp:TextBox>
                </td>
                <td>
                    <asp:Button ID="btnSubscribe" runat="server" Text="Go" OnClick="btnSubscribe_Click"
                        CssClass="button" OnClientClick="javascript:return validateEmail();" />
                </td>
            </tr>
        </table>
    </div>
</asp:Panel>
<asp:Literal ID="litCutomizeHtml" runat="server"></asp:Literal>
