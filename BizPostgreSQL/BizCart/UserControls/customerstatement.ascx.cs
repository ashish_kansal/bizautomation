﻿using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using BACRM.BusinessLogic.Item;
using System.Data;
using BACRM.BusinessLogic.Admin;
using BACRM.BusinessLogic.Common;
using BACRM.BusinessLogic.ShioppingCart;
using Telerik.Web.UI;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Configuration;
using BACRM.BusinessLogic.Accounting;
using BACRM.BusinessLogic.Opportunities;

namespace BizCart.UserControls
{
    public partial class customermanagement : BizUserControl
    {
        #region Global Declaration

        public string PreserveStateofTree { get; set; }
        public string ShowOnlyParentCategoriesinTree { get; set; }
        StringBuilder strBuilMain = new StringBuilder();
        HtmlGenericControl div = new HtmlGenericControl("div");
        decimal decTotalAmount = 0;
        decimal decAmountPaid = 0;
        decimal decBalanceDue = 0;
        #endregion

        #region Member Variables

        private string strUI = "";

        #endregion
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                Sites.InitialiseSession();
                bindHtml();

            }
            catch (Exception ex)
            {
                ExceptionModule.ExceptionPublish(ex, Sites.ToLong(Session["DomainID"]), Sites.ToLong(Session["UserContactID"]), Sites.ToLong(Session["SiteId"]), Request);
                ShowMessage(ex.ToString(), 1);
            }
        }


        #region HTML
        void bindHtml()
        {
            try
            {
                string strUI = GetUserControlTemplate("customermanagement.htm");

                string pattern = "<##FOR(?<Property>(\\b[^>]*?))##>(?<Content>([\\s\\S]*?))<##ENDFOR##>";
                string CssClass = "", AltCssClass = "", RepeteTemplate = "";

                if (Regex.IsMatch(strUI, pattern))
                {
                    MatchCollection matches = Regex.Matches(strUI, pattern);
                    if ((matches.Count > 0))
                    {
                        string[] Property = matches[0].Groups["Property"].Value.Replace("(", "").Replace(")", "").Split(';');
                        RepeteTemplate = matches[0].Groups["Content"].Value;

                        StringBuilder sb = new StringBuilder();
                        VendorPayment objVendorPayment = new VendorPayment();
                        DataTable dtARAging = new DataTable();
                        DataTable dtARAgingInvoice = new DataTable();
                        DataTable dtAddress = new DataTable();
                        GeneralLedger mobjGeneralLedger = new GeneralLedger();
                        mobjGeneralLedger.DomainID = Sites.ToLong(Session["DomainID"]);
                        mobjGeneralLedger.Year = System.DateTime.Now.Year;
                        objVendorPayment.DomainID = Sites.ToLong(Session["DomainID"]);
                        objVendorPayment.CompanyID = Sites.ToInteger(Session["DivId"]);
                        objVendorPayment.UserCntID = Sites.ToLong(Session["UserContactID"]);
                        objVendorPayment.Flag = "0";
                        objVendorPayment.dtFromDate = Convert.ToDateTime("1990-01-01");
                        objVendorPayment.dtTodate = System.DateTime.Now;

                        dtARAging = objVendorPayment.GetAccountReceivableAging();
                        dtARAgingInvoice = objVendorPayment.GetAccountReceivableAging_Invoice();

                        if (dtARAging.Rows.Count > 0)
                        {
                            string strTemp = string.Empty;
                            int RowsCount = 0;
                          

                            foreach (DataRow dr in dtARAgingInvoice.Rows)
                            {
                                string InvoiceId = "";
                                if (Convert.ToString(dr["numOppId"]) == "-1" || Convert.ToString(dr["numOppId"]) == "0") {
                                    InvoiceId = Convert.ToString(dr["numReturnHeaderID"]);
                                }
                                else
                                {
                                    InvoiceId = Convert.ToString(dr["vcBizDocID"]);
                                }
                                strTemp = RepeteTemplate;
                                strTemp = strTemp.Replace("##OrderID##", CCommon.ToString(dr["vcPOppName"]));
                                strTemp = strTemp.Replace("##RefType##", "1");
                                strTemp = strTemp.Replace("##PrintMode##", "0");
                                strTemp = strTemp.Replace("##OppID##", CCommon.ToString(dr["numOppId"]));
                                strTemp = strTemp.Replace("##InvoiceID##", CCommon.ToString(dr["vcBizDocID"]));
                                strTemp = strTemp.Replace("##TotalOrderAmount##", Sites.ToString(Session["CurrSymbol"]) + " " + string.Format("{0:#,##0.00}", (dr["TotalAmount"])));
                                strTemp = strTemp.Replace("##BalanceDue##", Sites.ToString(Session["CurrSymbol"]) + " " + string.Format("{0:#,##0.00}", (dr["BalanceDue"])));
                                strTemp = strTemp.Replace("##BillingDate##", Convert.ToString(dr["dtFromDate"]));
                                strTemp = strTemp.Replace("##DueDate##", Convert.ToString(dr["DueDate"]));
                                sb.Append(strTemp);

                                RowsCount++;
                            }
                        }
                        strUI = Regex.Replace(strUI, pattern, sb.ToString());
                        if (dtARAgingInvoice.Rows.Count > 0)
                        {
                            decTotalAmount = Convert.ToDecimal(dtARAgingInvoice.Compute("Sum(TotalAmount)", String.Empty));
                            decAmountPaid = Convert.ToDecimal(dtARAgingInvoice.Compute("Sum(AmountPaid)", String.Empty));
                            decBalanceDue = Convert.ToDecimal(dtARAgingInvoice.Compute("Sum(BalanceDue)", String.Empty));
                        }
                        strUI = strUI.Replace("##FooterTotalOrderAmount##", Convert.ToString(decTotalAmount));
                        strUI = strUI.Replace("##FooterBalanceDue##", Convert.ToString(decBalanceDue));
                        


                        COpportunities oCOpportunities = new COpportunities();
                        oCOpportunities.DomainID = Sites.ToLong(Session["DomainID"]);
                        dtAddress = oCOpportunities.GetExistingAddress(Sites.ToInteger(Session["UserContactID"]), Sites.ToInteger(Session["DivId"]));
                        string[] sAddress = CCommon.ToString(dtAddress.Rows[0]["vcBillAddress"]).ToString().Split(new string[] { "||" }, StringSplitOptions.None);
                        string sFinalAddress = "";
                        if (sAddress.Length > 0)
                        {
                            sFinalAddress = sAddress[0] + ", " + sAddress[1];
                        }

                        strUI = strUI.Replace("##BillingAddress##", sFinalAddress);
                        strUI = strUI.Replace("##CompanyNameAddress##", Convert.ToString(dtAddress.Rows[0]["vcCompanyName"]));
                        dtAddress = oCOpportunities.GetExistingAddress(Sites.ToInteger(Session["UserContactID"]), Sites.ToInteger(Session["DivisionID"]));
                        sAddress = CCommon.ToString(dtAddress.Rows[0]["vcBillAddress"]).ToString().Split(new string[] { "||" }, StringSplitOptions.None);
                        sFinalAddress = "";
                        if (sAddress.Length > 0)
                        {
                            sFinalAddress = sAddress[0] + ", " + sAddress[1];
                        }
                        strUI = strUI.Replace("##VendorBillingAddress##", sFinalAddress);
                        strUI = strUI.Replace("##VendorCompanyNameAddress##", Convert.ToString(dtAddress.Rows[0]["vcCompanyName"]));

                        strUI = strUI.Replace("##StatementDate##", System.DateTime.Now.ToString("dd/MM/yyyy"));

                        strUI = strUI.Replace("##thirtyField1##", Convert.ToString(dtARAging.Rows[0]["numThirtyDays"]));
                        strUI = strUI.Replace("##SixtyField1##", Convert.ToString(dtARAging.Rows[0]["numSixtyDays"]));
                        strUI = strUI.Replace("##NintyField1##", Convert.ToString(dtARAging.Rows[0]["numNinetyDays"]));
                        strUI = strUI.Replace("##TwentyField1##", Convert.ToString(dtARAging.Rows[0]["numOverNinetyDays"]));
                        strUI = strUI.Replace("##thirtyField2##", Convert.ToString(dtARAging.Rows[0]["numThirtyDaysOverDue"]));
                        strUI = strUI.Replace("##SixtyField2##", Convert.ToString(dtARAging.Rows[0]["numSixtyDaysOverDue"]));
                        strUI = strUI.Replace("##NintyField2##", Convert.ToString(dtARAging.Rows[0]["numNinetyDaysOverDue"]));
                        strUI = strUI.Replace("##TwentyField2##", Convert.ToString(dtARAging.Rows[0]["numOverNinetyDaysOverDue"]));
                    }
                }

                pnlCutomizeHtml.Visible = false;
                litCutomizeHtml.Text = strUI;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        #endregion
       
    }
}