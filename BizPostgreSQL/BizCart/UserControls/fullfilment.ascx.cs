﻿using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using BACRM.BusinessLogic.Common;
using BACRM.BusinessLogic.Admin;
using BACRM.BusinessLogic.Opportunities;
using BACRM.BusinessLogic.Documents;
using BACRM.BusinessLogic.Alerts;
using BACRM.BusinessLogic.ShioppingCart;
using System.Web.UI.HtmlControls;
using Microsoft.VisualBasic;
using System.Text.RegularExpressions;
using System.Text;
using nsoftware.InShip;
namespace BizCart.UserControls
{
    public partial class fullfilment : BizUserControl
    {
        #region Global Declaration
        DataTable dtOppBiDocItems = new DataTable();
        long sintRefType, lngReferenceID, lngBizDocId, lngOppId;
        CCommon objCommon = new CCommon();
        string InvoiceType = "";
        string transitTime = "-";
        nsoftware.InShip.Fedextrack _FedexTrack = new Fedextrack();
        nsoftware.InShip.Upstrack _UpsTrack = new Upstrack();
        long lngShippingCompanyId = 0;
        string PackageDeliveryDate = "-";
        #endregion

        #region Events
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                lngReferenceID = CCommon.ToLong(Request.QueryString["OppID"]);
                sintRefType = CCommon.ToLong(Request.QueryString["RefType"]);
                InvoiceType = CCommon.ToString(Request.QueryString["InvoiceType"]);
                lngBizDocId = CCommon.ToLong(Request.QueryString["OppBizDocID"]);
                lngOppId = CCommon.ToLong(Request.QueryString["OppID"]);
                //SetWebControlAttributes()
                if (!IsPostBack)
                {
                    btnClose.Attributes.Add("onclick", "return Close()");
                    btnPrint.Attributes.Add("onclick", "return PrintIt('" + tblButtons.ClientID + "');");
                    getAddDetails();
                    getBizDocDetails();
                }
            }
            catch (Exception ex)
            {
                ExceptionModule.ExceptionPublish(ex, Sites.ToLong(Session["DomainID"]), Sites.ToLong(Session["UserContactID"]), Sites.ToLong(Session["SiteId"]), Request);
                ShowMessage(ex.ToString(), 1);
            }
        }
        #endregion

        #region Methods
        private void getAddDetails()
        {
            try
            {
                DataTable dtOppBizAddDtl = default(DataTable);
                OppBizDocs objOppBizDocs = new OppBizDocs();
                objOppBizDocs.OppBizDocId = lngBizDocId;
                objOppBizDocs.OppId = lngOppId;
                objOppBizDocs.DomainID = CCommon.ToLong(Session["DomainID"]);
                dtOppBizAddDtl = objOppBizDocs.GetOppAddressDetail();
                if (dtOppBizAddDtl.Rows.Count != 0)
                {
                    //lblAddress.Text = IIf(IsDBNull(dtOppBizAddDtl.Rows(0).Item("CompName")), "", dtOppBizAddDtl.Rows(0).Item("CompName"))
                    lblBillTo.Text = CCommon.ToString(dtOppBizAddDtl.Rows[0]["BillAdd"]);
                    lblShipTo.Text = CCommon.ToString(dtOppBizAddDtl.Rows[0]["ShipAdd"]);

                    lblBillToAddressName.Text = CCommon.ToString(dtOppBizAddDtl.Rows[0]["BillToAddressName"]);
                    lblShipToAddressName.Text = CCommon.ToString(dtOppBizAddDtl.Rows[0]["ShipToAddressName"]);

                    lblBillToCompanyName.Text = CCommon.ToString(dtOppBizAddDtl.Rows[0]["BillToCompanyName"]);
                    lblShipToCompanyName.Text = CCommon.ToString(dtOppBizAddDtl.Rows[0]["ShipToCompanyName"]);

                    hplOppID.Text = CCommon.ToString(dtOppBizAddDtl.Rows[0]["OppName"]);
                    lblBizDoc.Text = CCommon.ToString(dtOppBizAddDtl.Rows[0]["BizDcocName"]);
                    lblBizDocIDLabel.Text = lblBizDoc.Text + "#";
                    txtBizDoc.Text = CCommon.ToString(dtOppBizAddDtl.Rows[0]["BizDoc"]);
                    txtConEmail.Text = CCommon.ToString(dtOppBizAddDtl.Rows[0]["vcEmail"]);
                    txtOppOwner.Text = CCommon.ToString(dtOppBizAddDtl.Rows[0]["Owner"]);
                    txtCompName.Text = CCommon.ToString(dtOppBizAddDtl.Rows[0]["CompName"]);
                    txtConID.Text = CCommon.ToString(dtOppBizAddDtl.Rows[0]["numContactID"]);
                    txtBizDocRecOwner.Text = CCommon.ToString(dtOppBizAddDtl.Rows[0]["BizDocOwner"]);

                }
                dtOppBizAddDtl = objOppBizDocs.GetOppBizDocDtl();
                imgLogo.ImageUrl = "/PortalDocs/" + Session["DomainID"] + "/" + dtOppBizAddDtl.Rows[0]["vcBizDocImagePath"];
                lblShipVia.Text =CCommon.ToString(dtOppBizAddDtl.Rows[0]["ShipVia"]);
                lblOrderReleaseDate.Text = CCommon.ToString(dtOppBizAddDtl.Rows[0]["dtReleaseDate"]);
                if (CCommon.ToString(dtOppBizAddDtl.Rows[0]["vcFulFillment"]) != "")
                {
                    lblBizDoc.Text = CCommon.ToString(dtOppBizAddDtl.Rows[0]["vcFulFillment"]);
                }
                else
                {
                    lblBizDoc.Text = CCommon.ToString(dtOppBizAddDtl.Rows[0]["vcBizDocID"]);
                }
                lblCustomPO.Text = CCommon.ToString(dtOppBizAddDtl.Rows[0]["vcPOName"]);
                
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        private void getBizDocDetails()
        {
            try
            {
                string strUI = GetUserControlTemplate("fulfilment.htm");

                string pattern = "<##FOR(?<Property>(\\b[^>]*?))##>(?<Content>([\\s\\S]*?))<##ENDFOR##>";
                string CssClass = "", AltCssClass = "", RepeteTemplate = "", RepeteSubTemplate = "";
                if (Regex.IsMatch(strUI, pattern))
                {
                    MatchCollection matches = Regex.Matches(strUI, pattern);
                    if ((matches.Count > 0))
                    {
                        string[] Property = matches[0].Groups["Property"].Value.Replace("(", "").Replace(")", "").Split(';');
                        RepeteTemplate = matches[0].Groups["Content"].Value;



                        if (Property.Length > 0)
                        {
                            for (int i = 0; i < Property.Length; i++)
                            {
                                string[] PropertyValue = Property[i].Split('=');

                                if (PropertyValue.Length == 2)
                                {
                                    switch (PropertyValue[0])
                                    {
                                        case "ItemClass":
                                            CssClass = PropertyValue[1].Trim();
                                            break;
                                        case "AlternatingItemClass":
                                            AltCssClass = PropertyValue[1].Trim();
                                            break;
                                    }
                                }
                            }
                        }
                        StringBuilder sb = new StringBuilder();
                        StringBuilder SubItemsb = new StringBuilder();
                        DataSet ds = BindDatagrid();
                        DataTable dt = ds.Tables[0];
                        if (dt.Rows.Count > 0)
                        {

                            string strTemp = string.Empty;
                            string strSubTemp = string.Empty;
                            int RowsCount = 0;

                            foreach (DataRow dr in dt.Rows)
                            {

                                PackageDeliveryDate = GetDeliveryDate(CCommon.ToLong(dr["numShipCompany"]), CCommon.ToString(dr["vcTrackingNumber"]),_FedexTrack,_UpsTrack);
                                if (PackageDeliveryDate != "" && PackageDeliveryDate != "-")
                                {
                                    PackageDeliveryDate = FormattedDateFromDate(Convert.ToDateTime(PackageDeliveryDate), CCommon.ToString(Session["DateFormat"]));
                                }
                                strTemp = RepeteTemplate;
                                strTemp = strTemp.Replace("#Box#", CCommon.ToString(dr["vcBoxName"]));
                                strTemp = strTemp.Replace("#Dimension#", CCommon.ToString(dr["fltHeight"]) + " X " + CCommon.ToString(dr["fltLength"]) + " X " + CCommon.ToString(dr["fltWidth"]));
                                strTemp = strTemp.Replace("#TotalWeight#", CCommon.ToString(dr["fltTotalWeight"]));
                                strTemp = strTemp.Replace("#ServiceType#", TranslateServiceType(CCommon.ToInteger(dr["tintServiceType"])));
                                strTemp = strTemp.Replace("#PackageType#", CCommon.ToString(dr["vcPackageName"]));
                                if (CCommon.ToString(dr["numShipCompany"]) == "91")
                                {
                                    strTemp = strTemp.Replace("#TrackingUrl#", "http://www.fedex.com/Tracking?tracknumbers=" + CCommon.ToString(dr["vcTrackingNumber"]) + "");
                                }
                                else if (CCommon.ToString(dr["numShipCompany"]) == "88")
                                {
                                    strTemp = strTemp.Replace("#TrackingUrl#", "http://wwwapps.ups.com/WebTracking/track?track=yes&trackNums=" + CCommon.ToString(dr["vcTrackingNumber"]) + "");
                                }
                                strTemp = strTemp.Replace("#TrackingNumber#", CCommon.ToString(dr["vcTrackingNumber"]));
                            
                                strSubTemp = "";
                               
                                    DataRow[] dtSubItems = ds.Tables[1].Select("numBoxID=" + CCommon.ToString(dr["numBoxID"]) + "");
                                    if (dtSubItems.Length > 0)
                                    {
                                        strSubTemp = strSubTemp + "<table class=\"table table-responsive tblitems\" width=\"100%\"><tr><th  class=\"RowHeader\">Item</th><th  class=\"RowHeader\">Model #</th><th  class=\"RowHeader\">SKU</th><th  class=\"RowHeader\">Description</th><th  class=\"RowHeader\">Unit Weight (lbs)</th><th  class=\"RowHeader\">Qty</th><th  class=\"RowHeader\">Dim. (H x L x W)</th></tr>";
                                        foreach (DataRow drSubItems in dtSubItems)
                                        {
                                            strSubTemp = strSubTemp + "<tr>";
                                            strSubTemp = strSubTemp + "<td>" + CCommon.ToString(drSubItems["vcItemName"]) + "</td>";
                                            strSubTemp = strSubTemp + "<td>" + CCommon.ToString(drSubItems["vcModelID"]) + "</td>";
                                            strSubTemp = strSubTemp + "<td>" + CCommon.ToString(drSubItems["vcModelID"]) + "</td>";
                                            strSubTemp = strSubTemp + "<td>" + CCommon.ToString(drSubItems["vcItemDesc"]) + "</td>";
                                            strSubTemp = strSubTemp + "<td>" + CCommon.ToString(drSubItems["fltTotalWeight"]) + "</td>";
                                            strSubTemp = strSubTemp + "<td>" + CCommon.ToString(drSubItems["intBoxQty"]) + "</td>";
                                            strSubTemp = strSubTemp + "<td>" + CCommon.ToString(drSubItems["fltHeight"]) + " X " + CCommon.ToString(drSubItems["fltLength"]) + " X " + CCommon.ToString(drSubItems["fltWidth"]) + "</td>";
                                            strSubTemp = strSubTemp + "</tr>";
                                        }
                                        strSubTemp = strSubTemp + "</table>";
                                    }
                                strTemp = strTemp.Replace("#ItemSummary#", strSubTemp);
                                sb.Append(strTemp);
                            }
                        }
                        if (lblOrderReleaseDate.Text != "" && lblOrderReleaseDate.Text != "-")
                        {
                            lblOrderReleaseDate.Text = FormattedDateFromDate(Convert.ToDateTime(lblOrderReleaseDate.Text), CCommon.ToString(Session["DateFormat"]));
                        }
                        else
                        {
                            lblOrderReleaseDate.Text = "-";
                        }
                        string address = lblShipToCompanyName.Text + "<br/>" + lblShipToAddressName.Text;
                        strUI = Regex.Replace(strUI, pattern, sb.ToString());
                        strUI = strUI.Replace("#ShipAddress#", address);
                        strUI = strUI.Replace("#ShipVia#", CCommon.RenderControl(lblShipVia));
                        strUI = strUI.Replace("#DaysinTransit#", transitTime);
                        strUI = strUI.Replace("#DeliveryBy#", PackageDeliveryDate);
                        strUI = strUI.Replace("#OrderReleaseDate#", CCommon.RenderControl(lblOrderReleaseDate));
                        strUI = strUI.Replace("#Logo#", CCommon.RenderControl(imgLogo));
                        strUI = strUI.Replace("#FOName#", CCommon.RenderControl(lblBizDoc));
                        strUI = strUI.Replace("#CustomPOName#", CCommon.RenderControl(lblCustomPO));
                    }
                }
                ltrlhtml.Text = strUI;
                pnlApprove.Visible = false;
                pnlBizDoc.Visible = false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public string TranslateServiceType(int tintServiceType)
        {
            try
            {
                string service = "";

                switch (tintServiceType)
                {
                    case 0:
                        service = "Unspecified";

                        break;
                    case 10:
                        service = "Priority Overnight";
                        break;
                    case 11:
                        service = "Standard Overnight";
                        break;
                    case 12:
                        service = "First Overnight";
                        break;
                    case 13:
                        service = "FedEx 2nd Day";
                        break;
                    case 14:
                        service = "FedEx Express Saver";
                        break;
                    case 15:
                        service = "FedEx Ground";
                        break;
                    case 16:
                        service = "FedEx Ground Home Delivery";
                        break;
                    case 17:
                        service = "FedEx 1Day Freight";
                        break;
                    case 18:
                        service = "FedEx 2Day Freight";
                        break;
                    case 19:
                        service = "FedEx 3Day Freight";
                        break;
                    case 20:
                        service = "International Priority";
                        break;
                    case 21:
                        service = "International Priority Distribution";
                        break;
                    case 22:
                        service = "International Economy";
                        break;
                    case 23:
                        service = "International Economy Distribution";
                        break;
                    case 24:
                        service = "International First";
                        break;
                    case 25:
                        service = "International Priority Freight";
                        break;
                    case 26:
                        service = "International Economy Freight";
                        break;
                    case 27:
                        service = "International Distribution Freight";
                        break;
                    case 28:
                        service = "Europe International Priority";

                        break;
                    case 70:
                        service = "USPS Express";
                        break;
                    case 71:
                        service = "USPS First Class";
                        break;
                    case 72:
                        service = "USPS Priority";
                        break;
                    case 73:
                        service = "USPS Parcel Post";
                        break;
                    case 74:
                        service = "USPS Bound Printed Matter";
                        break;
                    case 75:
                        service = "USPS Media";
                        break;
                    case 76:
                        service = "USPS Library";

                        break;
                    case 40:
                        service = "UPS Next Day Air";
                        break;
                    case 42:
                        service = "UPS 2nd Day Air";
                        break;
                    case 43:
                        service = "UPS Ground";
                        break;
                    case 48:
                        service = "UPS 3Day Select";
                        break;
                    case 49:
                        service = "UPS Next Day Air Saver";
                        break;
                    case 50:
                        service = "UPS Saver";
                        break;
                    case 51:
                        service = "UPS Next Day Air Early A.M.";
                        break;
                    case 55:
                        service = "UPS 2nd Day Air AM";
                        break;
                }
                return service;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private DataSet BindDatagrid()
        {
            DataSet ds = new DataSet();
            OppBizDocs objOppBizDocs = new OppBizDocs();
            try
            {
                long lngShippingReportId = CCommon.ToLong(Request.QueryString["shipmentreportId"]);
                long lngOppBizDocID = CCommon.ToLong(Request.QueryString["OppBizDocID"]);
                objOppBizDocs.DomainID = CCommon.ToLong(Session["DomainID"]);
                objOppBizDocs.OppBizDocId = lngOppBizDocID;
                objOppBizDocs.ShippingReportId = lngShippingReportId;
                ds = objOppBizDocs.GetShippingBoxes();
                transitTime=GetDeliveryDateandTransit(lngShippingReportId, lngBizDocId);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ds;
        }
        
        private string FormattedDateFromDate(System.DateTime dtDate, string strFormat)
        {
            try
            {
                string str4Yr = CCommon.ToString(Convert.ToDateTime(dtDate).Year);
                //Set the Year in 2 Digits to a Variable
                string str2Yr = Strings.Mid(CCommon.ToString(Convert.ToDateTime(dtDate).Year), 3, 2);
                //Set the Month in 2 Digits to a Variable
                string strIntMonth = CCommon.ToString(dtDate.Month);

                if (Convert.ToInt32(strIntMonth) <= 9)
                {
                    strIntMonth = "0" + strIntMonth;
                }
                //Set the Date in 2 Digits to a Variable
                string strDate = CCommon.ToString(dtDate.Day);

                if (Convert.ToInt32(strDate) <= 9)
                {
                    strDate = "0" + strDate;
                }
                //Set the Abbrivated Month Name to a Variable

                string str3Month = GetMonthName(CCommon.ToInteger(strIntMonth), true); //Convert.ToDateTime(strIntMonth).ToString("MMM");
                //Set the Full Month Name to a Variable
                string strFullMonth = GetMonthName(CCommon.ToInteger(strIntMonth), true);// Convert.ToInt32(strIntMonth).ToString("MMM");
                //Set the Nos of Hrs to a Variable
                string strHrs = CCommon.ToString(dtDate.Hour);
                //Set the Nos of Mins to a Variable
                string strMins = CCommon.ToString(dtDate.Minute);

                //As the Date Format will be one of the above mentioned formats,
                //we need to replace the required string to get the formatted date.
                strFormat = Strings.Replace(strFormat, "DD", strDate);
                strFormat = Strings.Replace(strFormat, "YYYY", str4Yr);
                strFormat = Strings.Replace(strFormat, "YY", str2Yr);
                strFormat = Strings.Replace(strFormat, "MM", strIntMonth);
                strFormat = Strings.Replace(strFormat, "MONTH", strFullMonth);
                strFormat = Strings.Replace(strFormat, "MON", str3Month);

                return strFormat;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private static string GetMonthName(int month, bool abbrev)
        {
            try
            {
                DateTime date = new DateTime(1900, month, 1);
                if (abbrev) return date.ToString("MMM");
                return date.ToString("MMMM");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
        protected void btnExportPDF_Click(object sender, EventArgs e)
        {
            try
            {
                hdnBizDocHTML.Value = "<html><head><link rel='stylesheet' href='" + Server.MapPath("~/CSS/master.css") + "' type='text/css' /></head><body>" + hdnBizDocHTML.Value + "</body></html>";

                OppBizDocs objBizDocs = new OppBizDocs();
                string strFileName = "";
                string strFilePhysicalLocation = "";

                HTMLToPDF objHTMLToPDF = new HTMLToPDF();
                strFileName = objHTMLToPDF.ConvertHTML2PDF(hdnBizDocHTML.Value, CCommon.ToLong(Session["DomainID"]), numOrientation: (hdnOrientation.Value == "2" ? 2 : 1), keepFooterAtBottom: CCommon.ToBool(hdnKeepFooterBottom.Value));
                strFilePhysicalLocation = CCommon.GetDocumentPhysicalPath(CCommon.ToLong(Session["DomainID"])) + strFileName;

                Response.Clear();
                Response.ClearContent();
                Response.ContentType = "application/pdf";
                Response.AddHeader("content-disposition", "attachment; filename=" + (lblBizDocIDValue.Text.Trim().Length > 0 ? lblBizDocIDValue.Text.Trim().Replace(" ", "_").Replace(",", " ") + ".pdf" : strFileName.Replace(",", " ")));
                //Response.Write(Session["Attachements"])

                Response.WriteFile(strFilePhysicalLocation);

                Response.End();
            }
            catch (Exception ex)
            {
                Response.Write(ex);
            }

        }

        #region ExtraCode
        //private void SetWebControlAttributes()
        //{
        //    try
        //    {
        //        //  ibtnLogo.Attributes.Add("onclick", "return OpenLogoPage();");
        //        //hplAmountPaid.Attributes.Add("onclick", "return OpenAmtPaid(" & sintRefType & "," & lngReferenceID.ToString & " ," & hdnDivID.Value & ");")
        //        // ibtnPrint.Attributes.Add("onclick", "return PrintIt();");
        //        // ibtnExport.Attributes.Add("onclick", "OpenExport(" + sintRefType.ToString + "," + lngReferenceID.ToString + "); return false;");
        //        // hplOppID.Attributes.Add("onclick", "return Close1(" + lngReferenceID.ToString + ")");
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}



        //public void BindBizDocsTemplate()
        //{
        //    try
        //    {
        //        //OppBizDocs objOppBizDoc = new OppBizDocs();
        //        //objOppBizDoc.DomainID = Session("DomainID");
        //        //objOppBizDoc.BizDocId = hdnBizDocId.Value;
        //        //objOppBizDoc.OppType = hdnOppType.Value;

        //        //DataTable dtBizDocTemplate = objOppBizDoc.GetBizDocTemplateList();

        //        //ddlBizDocTemplate.DataSource = dtBizDocTemplate;
        //        //ddlBizDocTemplate.DataTextField = "vcTemplateName";
        //        //ddlBizDocTemplate.DataValueField = "numBizDocTempID";
        //        //ddlBizDocTemplate.DataBind();

        //        //ddlBizDocTemplate.Items.Insert(0, new ListItem("--Select--", 0));
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        //if (sintRefType == 1 | sintRefType == 2)
        //{
        //    hplBillto.Attributes.Add("onclick", "return openeditAddress('Bill'," + lngReferenceID + ",0)");
        //    hplShipTo.Attributes.Add("onclick", "return openeditAddress('Ship'," + lngReferenceID + ",0)");
        //}
        //else
        //{
        //    hplBillto.Attributes.Add("onclick", "return openeditAddress('Bill',0," + lngReferenceID + ")");
        //    hplShipTo.Attributes.Add("onclick", "return openeditAddress('Ship',0," + lngReferenceID + ")");
        //}

        //if (CCommon.ToInteger(dtOppBiDocDtl.Rows[0]["bitAuthoritativeBizDocs"]) == 1)
        //{
        //    m_aryRightsForAuthoritativ = GetUserRightsForPage_Other(10, 28);

        //    if (m_aryRightsForAuthoritativ(RIGHTSTYPE.VIEW) == 0)
        //    {
        //        Response.Redirect("../admin/authentication.aspx?mesg=AC");
        //    }
        //}
        //else
        //{
        //    m_aryRightsForNonAuthoritativ = GetUserRightsForPage_Other(10, 29);

        //    if (m_aryRightsForNonAuthoritativ(RIGHTSTYPE.VIEW) == 0)
        //    {
        //        Response.Redirect("../admin/authentication.aspx?mesg=AC");
        //    }

        //}

        #endregion

    }
}