﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BACRM.BusinessLogic.Common;
using BACRM.BusinessLogic.ShioppingCart;
using BACRM.BusinessLogic.Promotion;
using System.Data;

namespace BizCart.UserControls
{
    public partial class ItemPromotionPopup : BizUserControl
    {
        #region Page Events

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                Sites.InitialiseSession();

            }
            catch (Exception ex)
            {
                ExceptionModule.ExceptionPublish(ex, Sites.ToLong(Session["DomainID"]), Sites.ToLong(Session["UserContactID"]), Sites.ToLong(Session["SiteId"]), Request);
                ShowMessage(ex.ToString(), 1);
            }
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            try
            {
                bindHtml();
            }
            catch (Exception ex)
            {
                ExceptionModule.ExceptionPublish(ex, Sites.ToLong(Session["DomainID"]), Sites.ToLong(Session["UserContactID"]), Sites.ToLong(Session["SiteId"]), Request);
                ShowMessage(ex.ToString(), 1);
            }
        }

        #endregion

        #region Private Methods

        private void bindHtml()
        {
            try
            {
                string strUI = GetUserControlTemplate("ItemPromotionPopup.htm");
                litCutomizeHtml.Text = strUI;
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion

    }
}