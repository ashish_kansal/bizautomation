﻿using System;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BACRM.BusinessLogic.Item;
using BACRM.BusinessLogic.ShioppingCart;
using BACRM.BusinessLogic.Common;
using BACRM.BusinessLogic.Contacts;
using System.Collections;
using System.Text.RegularExpressions;
using System.Text;
namespace BizCart.UserControls
{
    public partial class ShippingOptions : BizUserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                Sites.InitialiseSession();
                if (CheckEmptyCart() == false) { btnSave.Visible = false; return; }
                if (!IsPostBack)
                {
                    if (!System.Web.HttpContext.Current.User.Identity.IsAuthenticated || CCommon.ToLong(Session["DomainID"]) == 0 || CCommon.ToLong(Session["UserContactID"]) == 0)
                    {
                        Response.Redirect("Login.aspx?ReturnURL=" + (Sites.ToString(Session["SiteLiveURL"]).Trim().Length > 2 ? "https://" : "http://") + Request.Url.Host + "/" + CheckoutPageName(), true);
                    }
                    if ((Sites.ToString(Session["AddressConfirm"]) != "True" || CCommon.ToLong(Session["ShipAddress"]) == 0) && Sites.ToInteger(Session["OnePageCheckout"]) == 0)
                    {
                        Response.Redirect("ConfirmAddress.aspx", true);
                    }
                    if (Sites.ToLong(Session["DivId"]) == 0) { return; }
                    bindHtml();
                    hdnReffereURL.Value = Request.UrlReferrer == null ? "" : Request.UrlReferrer.OriginalString;
                }
                if (IsPostBack)
                {
                    if (Sites.ToString(Request["__EVENTTARGET"]) == "btnDelete")
                    {
                        DeleteItem();
                        bindHtml();
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionModule.ExceptionPublish(ex, Sites.ToLong(Session["DomainID"]), Sites.ToLong(Session["UserContactID"]), Sites.ToLong(Session["SiteId"]), Request);
                ShowMessage(ex.ToString(), 1);
            }
        }

        private string GetXmlAsString(DataSet ds)
        {
            string strXML = "";
            try
            {
                DataTable dt = new DataTable();
                dt.Columns.Add("numDomainId");
                dt.Columns.Add("numUserCntId");
                dt.Columns.Add("vcCookieId");
                dt.Columns.Add("numItemCode");
                dt.Columns.Add("monPrice");
                dt.Columns.Add("numUnitHour");
                dt.Columns.Add("monTotAmtBefDiscount");
                dt.Columns.Add("monTotAmount");
                dt.Columns.Add("bitDiscountType");
                dt.Columns.Add("fltDiscount");
                dt.Columns.Add("vcShippingMethod");
                dt.Columns.Add("numServiceTypeID");
                dt.Columns.Add("decShippingCharge");
                dt.Columns.Add("tintServiceType");
                dt.Columns.Add("numShippingCompany");
                dt.Columns.Add("dtDeliveryDate");

                DataRow dr;
                HttpCookie cookie = GetCookie();
                if (ds != null && ds.Tables.Count > 0)
                {
                    for (int i = 0; i <= ds.Tables[0].Rows.Count - 1; i++)
                    {
                        dr = dt.NewRow();
                        dr["numDomainId"] = CCommon.ToLong(Session["DomainId"]);
                        dr["numUserCntId"] = CCommon.ToLong(Session["UserContactID"]);
                        dr["vcCookieId"] = (cookie["KeyId"] != null) ? cookie["KeyId"] : "";
                        dr["numItemCode"] = ds.Tables[0].Rows[i]["numItemCode"];
                        dr["monPrice"] = ds.Tables[0].Rows[i]["monPrice"];
                        dr["numUnitHour"] = ds.Tables[0].Rows[i]["numUnitHour"];
                        dr["monTotAmtBefDiscount"] = ds.Tables[0].Rows[i]["monTotAmtBefDiscount"];
                        dr["monTotAmount"] = ds.Tables[0].Rows[i]["monTotAmount"];
                        dr["bitDiscountType"] = ds.Tables[0].Rows[i]["bitDiscountType"];
                        dr["fltDiscount"] = ds.Tables[0].Rows[i]["fltDiscount"];
                        dr["vcShippingMethod"] = ds.Tables[0].Rows[i]["vcShippingMethod"];
                        dr["numServiceTypeID"] = ds.Tables[0].Rows[i]["numServiceTypeID"];
                        dr["decShippingCharge"] = ds.Tables[0].Rows[i]["decShippingCharge"];
                        dr["tintServiceType"] = ds.Tables[0].Rows[i]["tintServiceType"];
                        dr["numShippingCompany"] = ds.Tables[0].Rows[i]["numShippingCompany"];
                        dr["dtDeliveryDate"] = ds.Tables[0].Rows[i]["dtDeliveryDate"];
                        dt.Rows.Add(dr);
                        dt.AcceptChanges();
                    }
                }
                DataSet tempds = new DataSet();
                tempds.Tables.Add(dt);
                strXML = tempds.GetXml();
                return strXML;
            }
            catch (Exception ex)
            {
                ExceptionModule.ExceptionPublish(ex, Sites.ToLong(Session["DomainID"]), Sites.ToLong(Session["UserContactID"]), Sites.ToLong(Session["SiteId"]), Request);
                ShowMessage(ex.ToString(), 1);
                return strXML;
            }
        }


        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                DataSet ds = default(DataSet);
                ds = GetCartItem(); //(DataSet)Session["Data"];

                //foreach (GridViewRow gvRow in gvBasket.Rows)
                //{
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    if (Sites.ToString(dr["bitFreeShipping"]) == "True")
                    {
                        //if (dr["numOppItemCode"].ToString() == ((Label)gvRow.FindControl("lblOppItemCode")).Text)
                        //{
                        dr["vcShippingMethod"] = "Free Shipping";
                        dr["numServiceTypeID"] = 0;
                        dr["decShippingCharge"] = 0;
                        dr["tintServiceType"] = 0;
                        dr["numShippingCompany"] = 0;
                        dr["dtDeliveryDate"] = System.DBNull.Value;
                        //}
                    }
                    else
                    {
                        //if (dr["numOppItemCode"].ToString() == ((Label)gvRow.FindControl("lblOppItemCode")).Text)
                        //{
                        string strSelectedMethod = "";
                        strSelectedMethod = Request.Form["ddlShippingMethod_" + CCommon.ToString(dr["numItemCode"])];

                        if (strSelectedMethod != "")
                        {
                            string[] strArray;

                            strArray = CCommon.ToString(strSelectedMethod).Split(Char.Parse("~"));
                            //else
                            //{
                            //    strArray = new string[] { "", "", "" };
                            //}
                            if (Sites.ToInteger(strArray[2]) == 0)
                            {
                                dr["vcShippingMethod"] = strArray[5];
                                dr["numServiceTypeID"] = strArray[0];
                                dr["decShippingCharge"] = strArray[1];
                                dr["tintServiceType"] = 0;
                                dr["numShippingCompany"] = 0;
                                dr["dtDeliveryDate"] = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.000");
                            }
                            else
                            {
                                dr["vcShippingMethod"] = strArray[5];
                                dr["numServiceTypeID"] = strArray[0];
                                dr["decShippingCharge"] = strArray[1];
                                dr["tintServiceType"] = strArray[2];
                                dr["numShippingCompany"] = strArray[3];
                                try
                                {
                                    if (CCommon.ToString(strArray[4]).Trim() == "")
                                        dr["dtDeliveryDate"] = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.000");
                                    else
                                        dr["dtDeliveryDate"] = DateTime.Parse(strArray[4]).ToString("yyyy-MM-dd HH:mm:ss.000");
                                }
                                catch (Exception ex)
                                {
                                    dr["dtDeliveryDate"] = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.000");
                                    throw ex;
                                }

                            }
                        }
                        //}
                    }

                    //}
                }
                BACRM.BusinessLogic.ShioppingCart.Cart objcart;
                objcart = new BACRM.BusinessLogic.ShioppingCart.Cart();
                HttpCookie cookie = GetCookie();
                objcart.DomainID = CCommon.ToLong(Session["DomainID"]);
                objcart.ContactId = CCommon.ToLong(Session["UserContactID"]);
                objcart.CookieId = (cookie != null) ? cookie["KeyId"] : "";
                objcart.strXML = GetXmlAsString(ds);
                objcart.SiteID = Sites.ToLong(Session["SiteId"]);
                if (Session["PreferredItemPromotions"] != null)
                {
                    objcart.PreferredPromotion = ((DataSet)Session["PreferredItemPromotions"]).GetXml();
                }
                objcart.UpdateCartItem();
                Session["ShippingMethodConfirmed"] = "True";
                if (Sites.ToInteger(Request["hdnOnePageCheckout"]) == 1)
                {
                    decimal Tax = default(decimal);
                    decimal SubTotal = default(decimal);
                    decimal Discount = default(decimal);
                    decimal decShippingCharge = 0;
                    DataTable dtItems = ds.Tables[0];
                    if (dtItems.Rows.Count > 0)
                    {
                        SubTotal = Sites.ToDecimal(dtItems.Compute("SUM(monTotAmtBefDiscount)", ""));
                        Discount = Sites.ToDecimal(dtItems.Compute("SUM(fltDiscount)", ""));
                        decShippingCharge = Sites.ToDecimal(dtItems.Compute("SUM(decShippingCharge)", ""));
                    }
                    if (Session["TotalDiscount"] != null)
                    {
                        Discount = Sites.ToDecimal(Session["TotalDiscount"]);
                    }
                    Session["ShippingCost"] = decShippingCharge;
                    Session["TotalAmount"] = SubTotal + Tax + decShippingCharge - Discount;

                    Session["SelectedStep"] = Sites.ToInteger(Session["SelectedStep"]) + 1;
                    return;
                }
                if (Sites.ToString(Request["Return"]).Length > 0)
                {
                    Response.Redirect(Sites.ToString(Request["Return"]), true);
                }
                Response.Redirect((Sites.ToString(Session["SiteLiveURL"]).Trim().Length > 2 ? "https://" : "http://") + Request.Url.Host + "/Cart.aspx");
            }
            catch (Exception ex)
            {
                ExceptionModule.ExceptionPublish(ex, Sites.ToLong(Session["DomainID"]), Sites.ToLong(Session["UserContactID"]), Sites.ToLong(Session["SiteId"]), Request);
                ShowMessage(ex.ToString(), 1);
            }
        }


        void bindHtml()
        {
            try
            {
                string strUI = GetUserControlTemplate("ShippingOptions.htm");

                string pattern = "<##FOR(?<Property>(\\b[^>]*?))##>(?<Content>([\\s\\S]*?))<##ENDFOR##>";
                string RepeteTemplate = "";
                Hashtable htProperties;
                if (Regex.IsMatch(strUI, pattern))
                {
                    MatchCollection matches = Regex.Matches(strUI, pattern);
                    if ((matches.Count > 0))
                    {

                        RepeteTemplate = matches[0].Groups["Content"].Value;
                        htProperties = GetPropertiesAsHastable(matches[0].Groups["Property"].Value.Replace("(", "").Replace(")", ""));

                        StringBuilder sb = new StringBuilder();
                        DataSet ds = new DataSet();
                        ds = GetCartItem();

                        DataTable dt;
                        if (ds != null && ds.Tables.Count > 0)
                        {
                            dt = ds.Tables[0];
                            if (dt.Rows.Count > 0)
                            {


                                StringBuilder strTemp = new StringBuilder();
                                int RowsCount = 0;
                                Button btnDelete;
                                DropDownList ddlShippingMethod;
                                bool ShowShippingAmount = false;

                                if (RepeteTemplate.Contains("##ShippingAmount##")) { ShowShippingAmount = true; }
                                foreach (DataRow dr in dt.Rows)
                                {
                                    strTemp.Clear();
                                    strTemp.Append(RepeteTemplate);
                                    strTemp.Replace("##ItemURL##", CCommon.ToString(dr["ItemURL"]));
                                    strTemp.Replace("##ItemName##", CCommon.ToString(dr["vcItemName"]));
                                    strTemp.Replace("##ItemUnits##", CCommon.ToString(dr["numUnitHour"]));

                                    if (Sites.ToBool(Session["HidePrice"]) == false)
                                    {
                                        strTemp.Replace("##ItemPricePerUnit##", string.Format("{0:#,##0.00}", dr["monPrice"]));
                                        strTemp.Replace("##TotalAmount##", string.Format("{0:#,##0.00}", dr["monTotAmount"]));
                                    }
                                    else
                                    {
                                        strTemp.Replace("##ItemPricePerUnit##", "NA");
                                        strTemp.Replace("##TotalAmount##", "NA");
                                    }
                                    strTemp.Replace("##Discount##", string.Format("{0:#,##0.00}", dr["fltDiscount"]));


                                    //freeshipping
                                    if (Sites.ToString(dr["bitFreeShipping"]) == "True")
                                    {
                                        strTemp.Replace("##ShippingMethodName##", "Free Shipping");
                                        strTemp.Replace("##ShippingAmount##", "");
                                    }
                                    else // shipping based on shipping rule 
                                    {
                                        ddlShippingMethod = new DropDownList();
                                        ddlShippingMethod.ID = "ddlShippingMethod_" + CCommon.ToString(dr["numItemCode"]);
                                        ddlShippingMethod.CssClass = "dropdown";

                                        DataTable dtTable = null;
                                        CContacts objContact = new CContacts();
                                        objContact.DomainID = Sites.ToLong(Session["DomainID"]);
                                        objContact.AddressID = Sites.ToLong(Session["ShipAddress"]);
                                        objContact.byteMode = 1;
                                        dtTable = objContact.GetAddressDetail();

                                        ShippingRule objRule = new ShippingRule();
                                        objRule.DomainID = CCommon.ToLong(Session["DomainID"]);
                                        objRule.SiteID = CCommon.ToLong(Session["SiteID"]);
                                        objRule.ItemID = CCommon.ToLong(dr["numItemCode"]);
                                        objRule.CategoryID = CCommon.ToLong(dr["numCategoryId"]);
                                        objRule.DivisionID = Sites.ToLong(Session["DivisionID"]);
                                        objRule.RelationshipID = Sites.ToLong(Session["RelationShip"]);
                                        objRule.ProfileID = Sites.ToLong(Session["Profile"]);
                                        objRule.Height = Sites.ToDouble(dr["numHeight"]);
                                        objRule.Width = Sites.ToDouble(dr["numWidth"]);
                                        objRule.Length = Sites.ToDouble(dr["numLength"]);
                                        objRule.Weight = Sites.ToDouble(dr["numWeight"]);
                                        if (dtTable.Rows.Count > 0)
                                        {
                                            objRule.ZipCode = Sites.ToString(dtTable.Rows[0]["vcPostalCode"]);
                                            objRule.Country = Sites.ToLong(dtTable.Rows[0]["numCountry"]);
                                            objRule.State = Sites.ToLong(dtTable.Rows[0]["numState"]);
                                        }
                                        string strMessage = objRule.GetShippingMethodForItem(ddlShippingMethod, ShowShippingAmount);
                                        if (strMessage.Length > 0)
                                            ShowMessage(strMessage, 1);

                                        strTemp.Replace("##ShippingMethodName##", CCommon.RenderControl(ddlShippingMethod));
                                        strTemp.Replace("##ShippingAmount##", "");
                                    }



                                    btnDelete = new Button();
                                    btnDelete.Text = "X";
                                    btnDelete.CssClass = "button";
                                    btnDelete.Attributes.Add("onclick", "return DeleteRecord('" + CCommon.ToString(dr["numItemCode"]) + "');");
                                    strTemp.Replace("##DeleteItemButton##", CCommon.RenderControl(btnDelete));


                                    if (RowsCount % 2 == 0)
                                        strTemp.Replace("##RowClass##", Sites.ToString(htProperties["AlternatingItemClass"]));
                                    else
                                        strTemp.Replace("##RowClass##", Sites.ToString(htProperties["ItemClass"]));

                                    sb.Append(strTemp.ToString());

                                    RowsCount++;
                                }
                            }
                            strUI = Regex.Replace(strUI, pattern, sb.ToString());
                        }
                    }
                }


                strUI = strUI.Replace("##SaveShippingOptionsButton##", CCommon.RenderControl(btnSave));

                pnlCutomizeHtml.Visible = false;
                litCutomizeHtml.Text = strUI;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        private void DeleteItem()
        {
            try
            {
                BACRM.BusinessLogic.ShioppingCart.Cart objcart;
                objcart = new BACRM.BusinessLogic.ShioppingCart.Cart();
                HttpCookie cookie = Request.Cookies["Cart"];
                //DataSet ds = default(DataSet);
                objcart.DomainID = CCommon.ToLong(Session["DomainID"]);
                objcart.ContactId = CCommon.ToLong(Session["UserContactID"]);
                objcart.CookieId = (cookie != null) ? cookie["KeyId"] : "";
                objcart.ItemCode = CCommon.ToLong(Request["__EVENTARGUMENT"]);
                objcart.bitDeleteAll = false;
                objcart.SiteID = Sites.ToLong(Session["SiteId"]);
                objcart.RemoveItemFromCart();
                this.Page.ClientScript.RegisterStartupScript(this.GetType(), "UpdateMiniCart", "if(document.getElementById('btnRefresh')!=null){ __doPostBack('btnRefresh','OnClick');}", true);
                this.Page.ClientScript.RegisterStartupScript(this.GetType(), "UpdateCartTotal", "__doPostBack('RefreshCartTotal','')", true);
                return;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}