﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DisplayList.ascx.cs"
    Inherits="BizCart.UserControls.DisplayList" %>
<script>
    function DeleteRecord(a) {
        if (confirm('Are you sure, you want to delete the selected record?')) {
            __doPostBack('lnkDeleteListItem', a);
            return false;
        }
        else {
            return false;
        }
    }

    document.addEventListener("DOMContentLoaded", function (event) {
        var rbl = document.getElementById("<%=rblLists.ClientID%>");
        var radioButtonListArray = rbl.getElementsByTagName('input');
        for (var x = 0; x < radioButtonListArray.length; x++) {
            radioButtonListArray[x].addEventListener('click', function () {
                document.getElementById("<%=rdbNewCategory.ClientID%>").checked = false;
            });
        }
    });

    function NewCategoryClicked() {
        var myRadio = document.getElementById("<%=rdbNewCategory.ClientID%>");
        if (myRadio.checked) {
            var rbl = document.getElementById("<%=rblLists.ClientID%>");
            var radioButtonListArray = rbl.getElementsByTagName('input');
            for (var x = 0; x < radioButtonListArray.length; x++) {
                radioButtonListArray[x].checked = false;
            }
        } else {
            var rbl = document.getElementById("<%=rblLists.ClientID%>");
            var radioButtonListArray = rbl.getElementsByTagName('input');
            if (radioButtonListArray.length > 0) {
                radioButtonListRef[0].checked = true;
            }
        }
    }
</script>
<asp:Panel ID="pnlCutomizeHtml" runat="server">
    <asp:Panel runat="server" ID="pnlAdd">
        <table width="100%" align="center" border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td>
                    <div class="sectionheader">
                        <asp:Label ID="lblTitle" runat="server" Text="Select a Wishlist"></asp:Label>
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    <asp:RadioButtonList ID="rblLists" runat="server" CssClass="radio" RepeatDirection="Vertical">
                    </asp:RadioButtonList>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:RadioButton ID="rdbNewCategory" CssClass="radioNewCategory" onchange="NewCategoryClicked();" runat="server" Text="Create New Category" /><asp:TextBox CssClass="textNewCategory" ID="txtCategoryName" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Button runat="server" ID="btnSave" CssClass="button" Text="Add to list" OnClientClick="return Save();"
                        OnClick="btnSave_Click" />
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlViewList">
        <table width="100%" align="center" border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td colspan="2">
                    <div class="sectionheader">
                        ##ListName##
                    </div>
                </td>
            </tr>
            <tr>
                <td class="EmptyMessage">
                    ##EmptyMessage##
                </td>
                <td align="right">
                    ##ViewAllLink##
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <%--  <asp:DataList ID="dlItems" runat="server" RepeatDirection="Horizontal" Width="" RepeatColumns="3"
                    OnItemCommand="dlItems_ItemCommand">
                    <ItemStyle CssClass="ListItemBox" />
                    <ItemTemplate>
                        <asp:Label ID="lblItemID" runat="server" Visible="true" Text='<%# Eval("numItemCode").ToString()%>'
                            Style="display: none;"></asp:Label>
                        <div class="product_image">
                            <img src="<%# GetImagePath(Eval("vcPathForTImage").ToString()) %>" height="150" width="130" />
                        </div>
                        <div class="product_title" title="<%# Eval("vcItemName").ToString()%>">
                            <a href="<%# GetProductURL(Eval("vcCategoryName").ToString(),Eval("vcItemName").ToString(),Eval("numItemCode").ToString() ) %>">
                                <%# Eval("vcItemName").ToString().Length > 22 ? Eval("vcItemName").ToString().Substring(0,22) + ".." : Eval("vcItemName").ToString()%></a>
                        </div>
                        <br />
                        <asp:LinkButton runat="server" ID="lnkRemove" Text="Remove item" CommandArgument='<%# Eval("numSalesTemplateItemID").ToString() +":"+Eval("numSalesTemplateID").ToString() %>'
                            CommandName="Remove"></asp:LinkButton>
                    </ItemTemplate>
                </asp:DataList>--%>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Panel>
<asp:Literal ID="litCutomizeHtml" runat="server"></asp:Literal>