﻿using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BACRM.BusinessLogic.Item;
using System.Data;
using BACRM.BusinessLogic.Admin;
using BACRM.BusinessLogic.Common;
using BACRM.BusinessLogic.ShioppingCart;
using System.Text.RegularExpressions;
using System.Text;
using System.Collections;
using System.Web.UI.HtmlControls;
namespace BizCart.UserControls
{
    public partial class CategoryList : BizUserControl
    {
        #region Events
        
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {

                Sites.InitialiseSession();
                //if (!IsPostBack)
                //    BindCategory();
                //if (Title.Length > 0) lblTitle.Text = Title;

                bindHtml();

                BreadCrumb.Clear();
                BreadCrumb.OverWriteBreadCrumb(1, "Home.aspx", "Home", "Home.aspx");
                BreadCrumb.OverWriteBreadCrumb(2, "Categories.aspx", "Category", "Categories.aspx");

                if (Request["Cat"] != null)
                    BreadCrumb.OverWriteBreadCrumb(3, "Categories.aspx", Sites.ToString(Request["CatName"]), "Categories.aspx");

                if (!IsPostBack && Sites.ToInteger(Request["Cat"]) > 0)
                {
                    CItems objItems = default(CItems);
                    objItems = new CItems();
                    objItems.byteMode = 17;
                    objItems.DomainID = Sites.ToLong(Session["DomainID"]);
                    objItems.CategoryID = Sites.ToInteger(Request["Cat"]);
                    DataTable dtCategory = objItems.SeleDelCategory();

                    if (dtCategory != null && dtCategory.Rows.Count > 0)
                    {
                        if (CCommon.ToString(dtCategory.Rows[0]["vcMetaTitle"]) != "")
                        {
                            this.Page.Title = CCommon.ToString(dtCategory.Rows[0]["vcMetaTitle"]);
                        }
                        if (CCommon.ToString(dtCategory.Rows[0]["vcMetaDescription"]) != "")
                        {
                            this.Page.MetaDescription = CCommon.ToString(dtCategory.Rows[0]["vcMetaDescription"]);
                        }
                        if (CCommon.ToString(dtCategory.Rows[0]["vcMetaKeywords"]) != "")
                        {
                            this.Page.MetaKeywords = CCommon.ToString(dtCategory.Rows[0]["vcMetaKeywords"]);
                        }
                    }           
                }
                
            }
            catch (Exception ex)
            {
                ExceptionModule.ExceptionPublish(ex, Sites.ToLong(Session["DomainID"]), Sites.ToLong(Session["UserContactID"]), Sites.ToLong(Session["SiteId"]), Request);
                ShowMessage(ex.ToString(), 1);
            }

        }
        
        #endregion

        #region Methods

        #region HTML
        private  void bindHtml()
        {
            try
            {
                string strUI = GetUserControlTemplate("CategoryList.htm");

                string pattern = "<##FOR(?<Property>(\\b[^>]*?))##>(?<Content>([\\s\\S]*?))<##ENDFOR##>";

                if (Regex.IsMatch(strUI, pattern))
                {
                    MatchCollection matches = Regex.Matches(strUI, pattern);
                    if ((matches.Count > 0))
                    {
                        Hashtable htProperties = GetPropertiesAsHastable(matches[0].Groups["Property"].Value.Replace("(", "").Replace(")", ""));
                        int RowsCount = 0;
                        string Content = matches[0].Groups["Content"].Value;


                        CItems objItems = new CItems();
                        DataTable dt = default(DataTable);
                        if (Request["Cat"] != null)
                        {
                            objItems.byteMode = 15;
                            objItems.CategoryID = Sites.ToInteger(Request["Cat"]);
                        }
                        else
                        {
                            objItems.byteMode = 14;
                        }
                        objItems.SiteID = Sites.ToLong(Session["SiteID"]);
                        dt = objItems.SeleDelCategory();

                        if (dt.Rows.Count > 0)
                        {
                            StringBuilder sb = new StringBuilder();

                            string strTemp = string.Empty;

                            foreach (DataRow dr in dt.Rows)
                            {
                                strTemp = Content;
                                string url = GetURL(dr["vcCategoryName"].ToString(), dr["numCategoryID"].ToString());

                                string CategoryImagePath = "";

                                if (!(dr["vcPathForCategoryImage"] == System.DBNull.Value))
                                {
                                    if (!string.IsNullOrEmpty(dr["vcPathForCategoryImage"].ToString()))
                                    {
                                        CategoryImagePath = Session["ItemImagePath"].ToString() + "/" + dr["vcPathForCategoryImage"].ToString();
                                    }
                                }

                                strTemp = strTemp.Replace("##CategoryLink##", url);
                                strTemp = strTemp.Replace("##CategoryID##", dr["numCategoryID"].ToString());
                                strTemp = strTemp.Replace("##CategoryName##", dr["vcCategoryName"].ToString());
                                strTemp = strTemp.Replace("##CategoryImagePath##", CategoryImagePath);
                                strTemp = strTemp.Replace("##CategoryDesc##", dr["vcDescription"].ToString());

                                if (RowsCount % 2 == 0)
                                    strTemp = strTemp.Replace("##RowClass##", Sites.ToString(htProperties["AlternatingItemClass"]));
                                else
                                    strTemp = strTemp.Replace("##RowClass##", Sites.ToString(htProperties["ItemClass"]));

                                sb.AppendLine(strTemp);
                                RowsCount++;
                            }
                            strUI = Regex.Replace(strUI, pattern, sb.ToString());
                        }
                        else if (Request["Cat"] != null)
                        {
                            string redirect = "/Category/" + Sites.GenerateSEOFriendlyURL(Request["CatName"]) + "/1/" + Request["Cat"];
                            Response.RedirectPermanent(redirect, false);
                        }
                        else
                        {
                            strUI = Regex.Replace(strUI, pattern, " ");

                            litMessage.Text = "No categories found.Please check 'Show in E-Commerce' from Administration";
                        }
                    }
                }

                string DefaultTitlePattern;
                DefaultTitlePattern = "{##DefaultTitle##}(?<Content>([\\s\\S]*?)){/##DefaultTitle##}";
                if (Sites.ToString(Request["CatName"]).Length > 0)
                {
                    strUI = Sites.RemoveMatchedPattern(strUI, DefaultTitlePattern);
                    strUI = strUI.Replace("##ParentCategoryName##", Sites.ToString(Request["CatName"]));
                }
                else
                {
                    strUI = Sites.ReplaceMatchedPatternWithContent(strUI, DefaultTitlePattern);
                    strUI = strUI.Replace("##ParentCategoryName##", "");
                }

                strUI = strUI.Replace("##Message##", CCommon.RenderControl(litMessage));

                pnlCutomizeHtml.Visible = false;
                litCutomizeHtml.Text = strUI;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
        
        private string GetURL(string CategoryName, string CategoryID)
        {
            try
            {
                string strURL = Sites.ToString(Session["SitePath"]) + "/SubCategory/" + Sites.GenerateSEOFriendlyURL(CategoryName) + "/" + CategoryID;
                return strURL;

                //return "Category/" + Sites.GenerateSEOFriendlyURL(CategoryName) + "/1/" + CategoryID;
                //return "SubCategory/" + Sites.GenerateSEOFriendlyURL(CategoryName) + "/" + CategoryID;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        
        #endregion

        #region ExtraCode
        //public string Title { get; set; }

        //public string ImageHeight { get; set; }
        //public string ImageWidth { get; set; }
        //public string ShowCategoryImage { get; set; }

        #endregion
    }
}