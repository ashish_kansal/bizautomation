﻿using System;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BACRM.BusinessLogic.Alerts;
using BACRM.BusinessLogic.Documents;
using BACRM.BusinessLogic.Common;
using BACRM.BusinessLogic.Prospects;
using BACRM.BusinessLogic.Contacts;
using BACRM.BusinessLogic.Leads;
using BACRM.BusinessLogic.Admin;
using BACRM.BusinessLogic.ShioppingCart;
using BACRM.BusinessLogic;

namespace BizCart.UserControls
{
    public partial class MultiCurrencyCountry : BizUserControl
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                Sites.InitialiseSession();
                if (!IsPostBack)
                    BindCurrencyDropDown();
                if (IsPostBack)
                {
                    if (Sites.ToString(Request["__EVENTTARGET"]) == "ClearCart")
                    {
                        ClearCartItems();
                        Response.Redirect("/Home.aspx",false);  
                    }
                    if (Sites.ToString(Request["__EVENTTARGET"]) == "Cancel")
                    {
                        if (ddlCurrency.Items.FindByValue(CCommon.ToString(Session["CurrencyID"]) + ":" + CCommon.ToString(Session["CurrSymbol"]) + ":" + CCommon.ToString(Session["ExchangeRate"])) != null)
                        {
                            ddlCurrency.ClearSelection();
                            ddlCurrency.Items.FindByValue(CCommon.ToString(Session["CurrencyID"]) + ":" + CCommon.ToString(Session["CurrSymbol"]) + ":" + CCommon.ToString(Session["ExchangeRate"])).Selected = true;
                        }
                    }
                    if (ddlCurrency.SelectedIndex > 0)
                    {
                        Session["CurrencyID"] = ddlCurrency.SelectedValue.Split(':')[0];
                        Session["CurrSymbol"] = ddlCurrency.SelectedValue.Split(':')[1];
                        Session["ExchangeRate"] = ddlCurrency.SelectedValue.Split(':')[2];
                    }
                }

                bindHtml();
            }
            catch (Exception ex)
            {
                ExceptionModule.ExceptionPublish(ex, Sites.ToLong(Session["DomainID"]), Sites.ToLong(Session["UserContactID"]), Sites.ToLong(Session["SiteId"]), Request);
                ShowMessage(ex.ToString(), 1);
            }

        }
        private void BindCurrencyDropDown()
        {
            try
            {
                CurrencyRates objCurrency = new CurrencyRates();
                objCurrency.DomainID = Sites.ToLong(Session["DomainID"]);
                ddlCurrency.DataSource = objCurrency.GetCurrencyWithCountry();
                ddlCurrency.DataTextField = "vcCurrencyDesc";
                ddlCurrency.DataValueField = "vcCurrencDetail";
                ddlCurrency.DataBind();
                ddlCurrency.Items.Insert(0, "--Select One--");
                ddlCurrency.Items.FindByText("--Select One--").Value = "0";

                if (Session["CurrencyID"] != null)
                {
                    if (ddlCurrency.Items.FindByValue(CCommon.ToString(Session["CurrencyID"]) + ":" + CCommon.ToString(Session["CurrSymbol"]) + ":" + CCommon.ToString(Session["ExchangeRate"])) != null)
                    {
                        ddlCurrency.ClearSelection();
                        ddlCurrency.Items.FindByValue(CCommon.ToString(Session["CurrencyID"]) + ":" + CCommon.ToString(Session["CurrSymbol"]) + ":" + CCommon.ToString(Session["ExchangeRate"])).Selected = true;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private void bindHtml()
        {
            try
            {
                string strUI = GetUserControlTemplate("MultiCurrencyCountry.htm");

                strUI = strUI.Replace("##CurrencyDropDownList##", CCommon.RenderControl(ddlCurrency));

                pnlCutomizeHtml.Visible = false;
                litCutomizeHtml.Text = strUI;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //protected void ddlCurrency_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        ClearCartItems();
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }

        //}

    }
}