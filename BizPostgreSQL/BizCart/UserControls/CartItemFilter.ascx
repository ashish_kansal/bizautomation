﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CartItemFilter.ascx.cs" Inherits="BizCart.UserControls.CartItemFilter" %>

<script type="text/javascript" lang="javascript">
    function DoFilterPostBack() {
        __doPostBack('btnFilter');
    }
</script>

<%--<script src="../JavaScript/jquery.min.js" type="text/javascript"></script>
<script src="../JavaScript/jquery-ui.min.js" type="text/javascript"></script>
<script type="text/javascript" lang="javascript">
    //function setFilterQuery() {
    //    var strQuery;
    //    strQuery = "";

    //    var strId;
    //    var strFieldName;
    //    var strFieldValue;
    //    var strValues;

    //    strId = "";
    //    strFieldName = "";
    //    strFieldValue = "";
    //    strValues = "";
    //    strQuery = "";

    //    var iCnt;
    //    iCnt = 0;

    //    $('.boxcontent').each(function () {
    //        //console.log($(this).is(':checked'));
    //        //1 != 1 ? 'y' : 'n' --> result is 'n'

    //        //find("[type=checkbox]")
    //        $(this).find("table").each(function () {

    //            $(this).find("[type=checkbox]").each(function () {

    //                //console.log($(this).find("[type=checkbox]"));
    //                strId = $(this).attr("id");
    //                strFieldName = strId.split("_")[0];
    //                if ($(this).is(':checked') == true) {
    //                    strFieldValue = strId.split("_")[1];
    //                    strValues = strValues + "," + strFieldValue;
    //                }
    //            });

    //            //console.log(strValues);
    //            if (strValues != "") {
    //                strQuery = strQuery + "|" + strFieldName + " IN (" + strValues.substring(1, strValues.length) + ")";
    //                strValues = "";
    //            }
    //        });
    //    });

    //    console.log(strQuery);
    //    $('#hdnQuery').val(strQuery);
    //}
    $('#btnCheckAll')
    $('#CartItemFilters9_pnlCutomizeHtml').find("[type=checkbox]").each(function () { $(this).attr("checked", true); });
</script>--%>
<asp:Panel ID="pnlCutomizeHtml" runat="server" style="float:left;">
    <table cellspacing="1" cellpadding="2" width="100%" style="float:left;">
        <tr>
            <td>
                <%--<asp:Literal ID="litCutomizeHtml" runat="server">
                </asp:Literal>
                <asp:HiddenField ID="hdnFilter" runat="server" ClientIDMode="Static" />
                <asp:HiddenField ID="hdnQuery" runat="server" ClientIDMode="Static" />--%>
                <asp:Table ID="tblMain" runat="server">
                    <asp:TableHeaderRow>
                        <asp:TableCell ColumnSpan="2">
                            <br />
                        </asp:TableCell>
                    </asp:TableHeaderRow>
                    <asp:TableHeaderRow style="display:none">
                        <asp:TableCell>
                            <asp:Button ID="btnFilter" runat="server" Text="Filter" CssClass="button" OnClick="btnFilter_Click"  />
                        </asp:TableCell>
                        <asp:TableCell>
                            <%--<asp:Button ID="btnCheckAll" runat="server" Text="Check All" CssClass="button" UseSubmitBehavior="false" />--%>
                        </asp:TableCell>
                    </asp:TableHeaderRow>
                    <asp:TableRow>
                        <asp:TableCell ColumnSpan="2">
                            <br />
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </td>
        </tr>
    </table>
</asp:Panel>
