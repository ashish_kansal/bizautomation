﻿using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BACRM.BusinessLogic.Opportunities;
using BACRM.BusinessLogic.Common;
using BACRM.BusinessLogic.Item;
using System.Data;
using BACRM.BusinessLogic.ShioppingCart;
using System.Text.RegularExpressions;
using System.Text;
using BACRM.BusinessLogic.Case;
namespace BizCart.UserControls
{
    public partial class Support : BizUserControl
    {
        string strColumn = string.Empty;
        int PageSize = 10;
        int PageIndex = 0;
        string DasboardTabs = "";
        string actionitem = "";
        string orderId = "";
        CCommon objCommon = new CCommon();
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                Sites.InitialiseSession();
                if (!System.Web.HttpContext.Current.User.Identity.IsAuthenticated || CCommon.ToLong(Session["DomainID"]) == 0 || CCommon.ToLong(Session["UserContactID"]) == 0)
                {
                    Response.Redirect("Login.aspx?ReturnURL=support.aspx", true);
                }
                PageIndex = Sites.ToInteger(Request["Page"]);
                actionitem = Convert.ToString(Request.QueryString["actionitem"]);
                orderId = Convert.ToString(Request.QueryString["OrderId"]);
                
                if (!IsPostBack)
                {
                    objCommon = new CCommon();
                    objCommon.sb_FillComboFromDBwithSel(ref ddlCategory, 34, Sites.ToLong(Session["DomainID"]));
                    bindHtml();
                }
               
            }
            catch (Exception ex)
            {
                ExceptionModule.ExceptionPublish(ex, Sites.ToLong(Session["DomainID"]), Sites.ToLong(Session["UserContactID"]), Sites.ToLong(Session["SiteId"]), Request);
                ShowMessage(ex.ToString(), 1);
            }
        }
        
     
        public DataTable BindDatagrid()
        {
            try
            {
                DataTable dtSolution = default(DataTable);

                Solution objSolution = new Solution();

                objSolution.Category =Sites.ToLong(ddlCategory.SelectedItem.Value);
                objSolution.KeyWord = txtSupportSearch.Text;
                objSolution.SortCharacter = '0';
                objSolution.DomainID =Sites.ToLong(Session["DomainID"]);
                objSolution.DeleteSolution();



                if (!string.IsNullOrEmpty(strColumn))
                    objSolution.columnName = strColumn;
                dtSolution = objSolution.GetKnowledgeBases();

                return dtSolution;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string ReturnName(System.DateTime bintCreatedDate)
        {
            try
            {
                string strCreateDate = null;
                strCreateDate = modBacrm.FormattedDateFromDate(bintCreatedDate, Sites.ToString(Session["DateFormat"]));
                return strCreateDate;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string ReturnDateTime(Object CloseDate)
        {
            try
            {
                string strTargetResolveDate = "";
                string temp = "";

                if (!(CloseDate == System.DBNull.Value))
                {
                    DateTime dtCloseDate = DateTime.Parse(CloseDate.ToString());
                    strTargetResolveDate = modBacrm.FormattedDateFromDate(dtCloseDate, Sites.ToString(Session["DateFormat"]));

                    string timePart = dtCloseDate.ToShortTimeString().Substring(0, dtCloseDate.ToShortTimeString().Length - 1);
                    // remove gaps
                    if (timePart.Split(' ').Length >= 2) timePart = timePart.Split(' ').GetValue(0).ToString() + timePart.Split(' ').GetValue(1).ToString();

                    // check Today date components [ Date , Month , Year ] with Parameter CloseDate components [ Date , Month , Year ] 
                    // if both are same it is today
                    if ((dtCloseDate.Date == DateTime.Now.Date & dtCloseDate.Month == DateTime.Now.Month & dtCloseDate.Year == DateTime.Now.Year))
                    {
                        strTargetResolveDate = "<font color=red><b>Today</b></font>";

                        return strTargetResolveDate;
                    }
                    // check [ TodayDate + 1 ] Date.... components [ Date , Month , Year ] with Parameter dtCloseDate components [ Date , Month , Year ] 
                    // if both are same it was Yesterday
                    else if ((dtCloseDate.Date.AddDays(1).Date == DateTime.Now.Date & dtCloseDate.AddDays(1).Month == DateTime.Now.Month & dtCloseDate.AddDays(1).Year == DateTime.Now.Year))
                    {
                        strTargetResolveDate = "<font color=purple><b>Yesterday</b></font>";

                        return strTargetResolveDate;
                    }
                    // check TodayDate .... components [ Date , Month , Year ] with Parameter [ dtCloseDate + 1 ] Date....  components [ Date , Month , Year ] 
                    // if both are same it will Tomorrow
                    else if ((dtCloseDate.Date == DateTime.Now.AddDays(1).Date & dtCloseDate.Month == DateTime.Now.AddDays(1).Month & dtCloseDate.Year == DateTime.Now.AddDays(1).Year))
                    {
                        temp = dtCloseDate.Hour.ToString() + ":" + dtCloseDate.Minute.ToString();
                        strTargetResolveDate = "<font color=orange><b>Tomorrow</b></font>";

                        return strTargetResolveDate;
                    }
                    // display day name for next 4 days from DateTime.Now

                    else if (dtCloseDate.ToString("yyyyMMdd") == DateTime.Now.AddDays(2).ToString("yyyyMMdd") | dtCloseDate.ToString("yyyyMMdd") == DateTime.Now.AddDays(3).ToString("yyyyMMdd") | dtCloseDate.ToString("yyyyMMdd") == DateTime.Now.AddDays(4).ToString("yyyyMMdd") | dtCloseDate.ToString("yyyyMMdd") == DateTime.Now.AddDays(5).ToString("yyyyMMdd"))
                    {
                        strTargetResolveDate = "<b>" + dtCloseDate.DayOfWeek.ToString() + "</b>";
                        return strTargetResolveDate;
                    }
                    else
                    {
                        //strTargetResolveDate = strTargetResolveDate;
                        return strTargetResolveDate;
                    }
                }
                return strTargetResolveDate;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public object ReturnMoney(System.Object Money)
        {
            try
            {
                return Math.Round(Sites.ToDecimal(Money.ToString()), 2);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        private void btnGo_Click(object sender, EventArgs e)
        {
            try
            {
                bindHtml();
            }
            catch (Exception ex)
            {
                ExceptionModule.ExceptionPublish(ex, Sites.ToLong(Session["DomainID"]), Sites.ToLong(Session["UserContactID"]), Sites.ToLong(Session["SiteId"]), Request);
                ShowMessage(ex.ToString(), 1);
            }
        }

        void bindHtml()
        {
            try
            {
                string strUI = GetUserControlTemplate("Support.htm");

                string pattern = "<##FOR(?<Property>(\\b[^>]*?))##>(?<Content>([\\s\\S]*?))<##ENDFOR##>";
                string CssClass = "", AltCssClass = "", RepeteTemplate = "";
                int k = 0;
                if (Regex.IsMatch(strUI, pattern))
                {
                    MatchCollection matches = Regex.Matches(strUI, pattern);
                    if ((matches.Count > 0))
                    {
                        string[] Property = matches[0].Groups["Property"].Value.Replace("(", "").Replace(")", "").Split(';');
                        RepeteTemplate = matches[0].Groups["Content"].Value;



                        if (Property.Length > 0)
                        {
                            for (int i = 0; i < Property.Length; i++)
                            {
                                string[] PropertyValue = Property[i].Split('=');

                                if (PropertyValue.Length == 2)
                                {
                                    switch (PropertyValue[0])
                                    {
                                        case "ItemClass":
                                            CssClass = PropertyValue[1].Trim();
                                            break;
                                        case "AlternatingItemClass":
                                            AltCssClass = PropertyValue[1].Trim();
                                            break;
                                    }
                                }
                            }
                        }
                        StringBuilder sb = new StringBuilder();
                        DataTable dt = BindDatagrid();
                        if (dt.Rows.Count > 0)
                        {
                            
                            string strTemp = string.Empty;
                            int RowsCount = 0;

                            foreach (DataRow dr in dt.Rows)
                            {
                                
                                strTemp = RepeteTemplate;
                                strTemp = strTemp.Replace("##SupportTitle##", CCommon.ToString(dr["vcSolnTitle"]));
                                strTemp = strTemp.Replace("##SupportDesc##", CCommon.ToString(dr["txtSolution"]));
                                strTemp = strTemp.Replace("##RecordNo##", CCommon.ToString(RowsCount));
                               
                                //strTemp = strTemp.Replace("##InvoiceID##", CCommon.ToString(dr["vcBizDocID"]));

                                if (RowsCount % 2 == 0)
                                    strTemp = strTemp.Replace("##RowClass##", AltCssClass);
                                else
                                    strTemp = strTemp.Replace("##RowClass##", CssClass);

                                sb.Append(strTemp);

                                RowsCount++;
                            }
                        }
                        
                        strUI = Regex.Replace(strUI, pattern, sb.ToString());
                        strUI = strUI.Replace("##supportcategory##", CCommon.RenderControl(ddlCategory));
                        strUI = strUI.Replace("##supportSearchTextbox##", CCommon.RenderControl(txtSupportSearch));
                        strUI = strUI.Replace("##supportSearchButton##", CCommon.RenderControl(btnSupportSearch));
                    }
                }

                //Credit Term Replacement
                string SummaryPattern = "{##SummaryTable##}(?<Content>([\\s\\S]*?)){/##SummaryTable##}";
                CCommon objCommon = new CCommon();
                CItems objItems = new CItems();
                objItems.DivisionID = Sites.ToLong(Session["DivId"]);
                DataTable dtTable = default(DataTable);
                string lblCreditLimit = "";
                dtTable = objItems.GetAmountDue();
                lblBalDue.Text = Sites.ToString(Session["CurrSymbol"]) + " " + string.Format("{0:#,##0.00}", dtTable.Rows[0]["AmountDueSO"]);
                decimal RemainIngCredit = Sites.ToDecimal(dtTable.Rows[0]["RemainingCredit"]);
                if (RemainIngCredit < 0)
                {
                    RemainIngCredit = 0;
                }
                lblRemCredit.Text = Sites.ToString(Session["CurrSymbol"]) + " " + string.Format("{0:#,##0.00}", RemainIngCredit);
                lblAmtPastDue.Text = Sites.ToString(Session["CurrSymbol"]) + " " + string.Format("{0:#,##0.00}", dtTable.Rows[0]["AmountPastDuePO"]);
                lblCreditLimit = Sites.ToString(Session["CurrSymbol"]) + " " + string.Format("{0:#,##0.00}", dtTable.Rows[0]["CreditLimit"]);
                tblCreditTerm.Visible = true;
                strUI = Sites.ReplaceMatchedPatternWithContent(strUI, SummaryPattern);
                //double CreditLimit = Sites.ToDouble(objItems.GetCreditStatusofCompany());
                //if (CreditLimit > 0 && CreditLimit != 10000000)
                //{
                //    DataTable dtTable = default(DataTable);
                //    dtTable = objItems.GetAmountDue();
                //    lblBalDue.Text = Sites.ToString(Session["CurrSymbol"]) + " " + string.Format("{0:#,##0.00}", dtTable.Rows[0]["AmountDueSO"]);
                //    lblRemCredit.Text = Sites.ToString(Session["CurrSymbol"]) + " " + string.Format("{0:#,##0.00}", dtTable.Rows[0]["RemainingCredit"]);
                //    lblAmtPastDue.Text = Sites.ToString(Session["CurrSymbol"]) + " " + string.Format("{0:#,##0.00}", dtTable.Rows[0]["AmountPastDuePO"]);
                //    lblCreditLimit.Text = Sites.ToString(Session["CurrSymbol"]) + " " + string.Format("{0:#,##0.00}", dtTable.Rows[0]["CreditLimit"]);
                //    tblCreditTerm.Visible = true;

                //    //Replace disply template ##Tags## with content value
                //    strUI = Sites.ReplaceMatchedPatternWithContent(strUI, SummaryPattern);

                //}
                //else
                //{
                //    tblCreditTerm.Visible = false;
                //    //Hide Other Template
                //    strUI = Sites.RemoveMatchedPattern(strUI, SummaryPattern);
                //}

                strUI = strUI.Replace("##TotalBalanceDueLabel##", lblBalDue.Text);
                strUI = strUI.Replace("##TotalRemainingCreditLabel##", lblRemCredit.Text);
                strUI = strUI.Replace("##TotalAmountPastDueLabel##", lblAmtPastDue.Text);
                strUI = strUI.Replace(" ##CreditLimit##", lblCreditLimit);
                strUI = strUI.Replace("##ARStatement##", "");
               
                strUI = BindTopDashboard(strUI);

                strUI = strUI.Replace("##CustomerName##", Sites.ToString(Session["ContactName"]));

                pnlCutomizeHtml.Visible = false;
                litCutomizeHtml.Text = strUI;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        protected void btnSupportSearch_Click(object sender, EventArgs e)
        {
            BindDatagrid();
            bindHtml();
        }
        private string BindTopDashboard(string strUI)
        {
            StringBuilder sbTopMenu = new StringBuilder();
            if (Sites.ToBool(Session["bitSalesOrderTabs"]) == true)
            {
                sbTopMenu.Append("<li class=\"SalesOrderTabs \"><a id=\"SalesOrderTabs\" href=\"Orders.aspx?flag=true\">" + Convert.ToString(Session["vcSalesOrderTabs"]) + "</a></li>");
            }
            if (Sites.ToBool(Session["bitSalesQuotesTabs"]) == true)
            {
                sbTopMenu.Append("<li class=\"SalesOpportunityTabs\"><a id=\"SalesOpportunityTabs\" href=\"SalesOpportunity.aspx\">" + Convert.ToString(Session["vcSalesQuotesTabs"]) + "</a></li>");
            }
            if (Sites.ToBool(Session["bitItemPurchaseHistoryTabs"]) == true)
            {
                sbTopMenu.Append("<li class=\"ItemPurchasedTabs\"><a id=\"ItemPurchasedTabs\" href=\"ItemPurchasedHistory.aspx\">" + Convert.ToString(Session["vcItemPurchaseHistoryTabs"]) + "</a></li>");
            }
            if (Sites.ToBool(Session["bitItemsFrequentlyPurchasedTabs"]) == true)
            {
                sbTopMenu.Append("<li class=\"WishListsTabs\"><a id=\"WishListsTabs\" href=\"Lists.aspx\">" + Convert.ToString(Session["vcItemsFrequentlyPurchasedTabs"]) + "</a></li>");
            }
            if (Sites.ToBool(Session["bitOpenCasesTabs"]) == true)
            {
                sbTopMenu.Append("<li class=\"OpenCasesTabs\"><a id=\"OpenCasesTabs\" href=\"OpenCases.aspx\">" + Convert.ToString(Session["vcOpenCasesTabs"]) + "</a></li>");
            }
            if (Sites.ToBool(Session["bitOpenRMATabs"]) == true)
            {
                sbTopMenu.Append("<li class=\"OpenRMATabs\"><a id=\"OpenRMATabs\" href=\"OpenRMA.aspx\">" + Convert.ToString(Session["vcOpenRMATabs"]) + "</a></li>");
            }
            if (Sites.ToBool(Session["bitSupportTabs"]) == true)
            {
                sbTopMenu.Append("<li class=\"SupportTabs active\"><a id=\"SupportTabs\" href=\"support.aspx\">" + Convert.ToString(Session["vcSupportTabs"]) + "</a></li>");
            }
            strUI = strUI.Replace("##TopMenuDasboard##", Convert.ToString(sbTopMenu));
            return strUI;
        }



        protected void ddlCategory_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindDatagrid();
            bindHtml();
        }
}
}