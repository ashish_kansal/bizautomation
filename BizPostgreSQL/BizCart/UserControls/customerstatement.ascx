﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="customerstatement.ascx.cs" Inherits="BizCart.UserControls.customermanagement" %>
<script language="javascript">
    function OpenBizInvoice(a, b, c) {
        window.open('Invoice.aspx?Show=True&OppID=' + a + '&RefType=' + b + '&PrintMode=' + c, '', 'toolbar=0,titlebar=0,menubar=0,location=1,left=100,top=50,width=750,height=800,scrollbars=yes,resizable=yes');
    }
    function OpenCustomerStatement() {
        window.open('customerstatement.aspx', '', 'toolbar=no,titlebar=no,top=10,left=100,width=1000,height=300,scrollbars=yes,resizable=yes')
        return false;
    }
</script>

<asp:Panel ID="pnlCutomizeHtml" runat="server">
<table width="100%">
       
        <tr>
            <td>
                <div id="divHtml">
                    <table cellpadding="2" cellspacing="2" border="0" width="100%">
                        <tr>
                            <td align="center" style="font-weight: bold; font-size: larger" colspan="2">
                                <span>Customer Statement</span>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <table cellpadding="2" cellspacing="2" border="0" width="100%">
                                    <tr>
                                        <td class="normal" style="font-weight: bold" align="center" colspan="2"><u>Customer Information</u>
                                        </td>
                                        <td class="normal" style="font-weight: bold" align="center" colspan="2"><u>Vendor Information</u>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="normal" width="25%" style="font-weight: bold" align="right">Company Name :
                                        </td>
                                        <td width="25%">
                                           
                                        </td>
                                        <td class="normal" width="25%" style="font-weight: bold" align="right">Company Name :
                                        </td>
                                        <td width="25%">
                                          
                                        </td>

                                    </tr>
                                    <tr>
                                        <td class="normal" style="font-weight: bold" align="right" valign="top">Billing Address :
                                        </td>
                                        <td align="left">
                                           
                                        </td>
                                        <td class="normal" style="font-weight: bold" align="right" valign="top">Billing Address :
                                        </td>
                                        <td align="left">
                                           
                                        </td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td class="normal" width="25%" style="font-weight: bold" align="right">Statement Date :
                                        </td>
                                        <td width="25%">
                                           
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td style="font-weight: bold" align="center">Current Amount Due
                            </td>
                            <td style="font-weight: bold" align="center">Past Due
                            </td>
                        </tr>
                        <tr>
                            <td width="50%" align="center">
                                <table width="100%" cellpadding="2" cellspacing="2">
                                    <tr>
                                        <td width="60%" align="right">Today-30 :
                                        </td>
                                        <td>
                                           
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="60%" align="right">31-60 :
                                        </td>
                                        <td>
                                            
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="60%" align="right">61-90 :
                                        </td>
                                        <td>
                                            
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="60%" align="right">Over 91 :
                                        </td>
                                        <td>
                                            
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td width="50%" align="center">
                                <table width="100%" cellpadding="2" cellspacing="2">
                                    <tr>
                                        <td width="60%" align="right">Today-30 :
                                        </td>
                                        <td>
                                            
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="60%" align="right">31-60 :
                                        </td>
                                        <td>
                                            
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="60%" align="right">61-90 :
                                        </td>
                                        <td>
                                            
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="60%" align="right">Over 91 :
                                        </td>
                                        <td>
                                           
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <table cellpadding="2" cellspacing="2" border="0" width="100%">
                                    <tr>
                                        <td>
                                            
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </div>
            </td>
        </tr>
    </table>
    </asp:Panel>
<asp:Literal ID="litCutomizeHtml" runat="server"></asp:Literal>