﻿using System;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BACRM.BusinessLogic.Item;
using BACRM.BusinessLogic.ShioppingCart;
using BACRM.BusinessLogic.Common;
using System.Text.RegularExpressions;
using System.Text;
using System.Collections;

namespace BizCart.UserControls
{
    public partial class PermaLink : BizUserControl
    {
        #region Global Declaration
     
        QueryStringValues objEncryption = new QueryStringValues();
        
        #endregion

        #region Events
        
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                Sites.InitialiseSession();
                if (!IsPostBack)
                {
                    hfContactId.Value = objEncryption.Encrypt(CCommon.ToString(Session["ContactId"]));
                    bindHtml();
                }
               
            }
            catch (Exception ex)
            {
                ExceptionModule.ExceptionPublish(ex, Sites.ToLong(Session["DomainID"]), Sites.ToLong(Session["UserContactID"]), Sites.ToLong(Session["SiteId"]), Request);
                ShowMessage(ex.ToString(), 1);
            }
        }
        
        #endregion

        #region Methods

        #region HTML
        
        private void bindHtml()
        {
            try
            {
                string strUI="";
                if (this.ReviewId > 0)
                {
                    strUI = "";
                    DataTable dtReview = null;

                    Review objReview = new Review();

                    objReview.ReviewId = this.ReviewId;
                    objReview.Mode = 6;

                    dtReview = objReview.GetReviews();

                    if (dtReview.Rows.Count > 0)
                    {
                        strUI = GetUserControlTemplate("PermaLink.htm");

                        if (dtReview.Rows.Count > 0)
                        {

                            strUI = strUI.Replace("##Title##", CCommon.ToString(dtReview.Rows[0]["vcReviewTitle"]));
                            strUI = strUI.Replace("##Review##", CCommon.ToString(dtReview.Rows[0]["vcReviewComment"]));
                            strUI = strUI.Replace("##UserName##", CCommon.ToString(dtReview.Rows[0]["vcContactName"]));
                            strUI = strUI.Replace("##CreatedDate##", CCommon.ToString(Convert.ToDateTime(dtReview.Rows[0]["dtCreatedDate"]).ToShortDateString()));
                            strUI = strUI.Replace("##ReviewId##", objEncryption.Encrypt(CCommon.ToString(dtReview.Rows[0]["numReviewId"])));
                            strUI = strUI.Replace("##TotalVote##", CCommon.ToString(dtReview.Rows[0]["vcTotalVote"]));
                            strUI = strUI.Replace("##HelpfulVote##", CCommon.ToString(dtReview.Rows[0]["vcHelpfulVote"]));

                            if ((CheckForOwnReview(Convert.ToInt32(dtReview.Rows[0]["numContactId"])) == true) || Sites.ToLong(Session["UserContactID"]) == 0)
                            {
                                strUI = strUI.Replace("##ReportAbuse##", "");
                                strUI = strUI.Replace("##ReviewHelpfulLabel##", "");
                                strUI = strUI.Replace("##HelpfulYes##", "");
                                strUI = strUI.Replace("##HelpfulSaperator##", "");
                                strUI = strUI.Replace("##HelpfulNo##", "");

                            }
                            else
                            {
                                strUI = strUI.Replace("##ReportAbuse##", "<a id = \"ReportAbuse\" rid=" + objEncryption.Encrypt(CCommon.ToString(dtReview.Rows[0]["numReviewId"])) + ">Report Abuse</a>");
                                strUI = strUI.Replace("##ReviewHelpfulLabel##", "was this review helpful ?");
                                strUI = strUI.Replace("##HelpfulYes##", "<a id = \"HelpfulYes\" rid=" + objEncryption.Encrypt(CCommon.ToString(dtReview.Rows[0]["numReviewId"])) + "><font color=\"navy\"><b>Yes</b></font> </a>");
                                strUI = strUI.Replace("##HelpfulSaperator##", "/");
                                strUI = strUI.Replace("##HelpfulNo##", "<a id = \"HelpfulNo\" rid=" + objEncryption.Encrypt(CCommon.ToString(dtReview.Rows[0]["numReviewId"])) + "><font color=\"navy\"><b>No</b></font> </a>");
                            }
                        }

                    }
                }

             
                pnlCutomizeHtml.Visible = false;
                litCutomizeHtml.Text = strUI;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        
        #endregion
        
        Boolean CheckForOwnReview(long ContactId)
        {
            try
            {
                long LogInContactId = CCommon.ToLong(Session["ContactId"]);
                if (LogInContactId == ContactId)
                {
                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
       
        #endregion
        
        #region Custom Property
        
        public long ReviewId
        {
            get
            {
                object o = ViewState["ReviewId"];
                if (o != null)
                {
                    return CCommon.ToLong(objEncryption.Decrypt(CCommon.ToString(o)));
                }
                else
                {
                    if (CCommon.ToString(Request.QueryString["ReviewId"]) != "")
                    {
                        ViewState["ReviewId"] = Request.QueryString["ReviewId"];
                        return CCommon.ToLong(objEncryption.Decrypt(CCommon.ToString(ViewState["ReviewId"])));
                    }
                    else
                    {
                        return 0;
                    }
                }
            }
        }
        
        #endregion        
    }
}