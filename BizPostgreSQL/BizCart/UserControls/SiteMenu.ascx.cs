﻿using System;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using BACRM.BusinessLogic.ShioppingCart;
using BACRM.BusinessLogic.Common;
using System.Text;
using System.Text.RegularExpressions;
namespace BizCart.UserControls
{
    public partial class SiteMenu : BizUserControl
    {
        public string MenuDirection { get; set; }
        public string Skin { get; set; }
        public string PopulateULLIbasedMenu { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                Sites.InitialiseSession();
                if (Convert.ToString(Request.QueryString["promotioncode"]) != "")
                {

                }
                bindHtml();
            }
            catch (Exception ex)
            {
                ExceptionModule.ExceptionPublish(ex, Sites.ToLong(Session["DomainID"]), Sites.ToLong(Session["UserContactID"]), Sites.ToLong(Session["SiteId"]), Request);
                ShowMessage(ex.ToString(), 1);
            }
        }

        //private void populateMenu()
        //{
        //    try
        //    {
        //        Sites objSite = new Sites();
        //        objSite.SiteID = Sites.ToLong(Session["SiteID"]);
        //        DataTable dt = objSite.GetSiteMenu();
        //        String PageURL = string.Empty;

        //        if (string.IsNullOrEmpty(PopulateULLIbasedMenu))
        //            PopulateULLIbasedMenu = "False";

        //        if (PopulateULLIbasedMenu.Contains("True"))
        //        {
        //            RadMenu1.Visible = false;

        //            Panel pnl = new Panel();
        //            pnl.ID = "vertSiteMenu";
        //            pnl.ClientIDMode = ClientIDMode.Static;
        //            HtmlGenericControl ul = new HtmlGenericControl("ul");
        //            ul.Attributes["class"] += "TreeNode";
        //            HyperLink hpCategory;
        //            HtmlGenericControl li;

        //            foreach (DataRow dr in dt.Rows)
        //            {
        //                if (Sites.ToBool(dr["bitStatus"]))
        //                {
        //                    if (dr["vcNavigationURL"].ToString().Contains("http"))
        //                        PageURL = dr["vcNavigationURL"].ToString();
        //                    else
        //                        PageURL = /*Session["SitePath"].ToString() + "/" + */"../" + dr["vcNavigationURL"].ToString();

        //                    if (PageURL.ToLower().Contains("bizportal"))
        //                    {
        //                        if (Sites.ToString(Session["Username"]).Length > 2 && Sites.ToString(Session["Password"]).Length > 2)
        //                        {
        //                            CCommon objCommon = new CCommon();
        //                            PageURL = Sites.ToString(Session["PortalURL"]) + "?&from=bizcart&u=" + Sites.ToString(Session["Username"]) + "&p=" + Sites.ToString(Session["Password"]);
        //                        }
        //                        else
        //                            continue;
        //                    }

        //                    hpCategory = new HyperLink();
        //                    li = new HtmlGenericControl("li");

        //                    hpCategory.Text = dr["vcTitle"].ToString();

        //                    hpCategory.NavigateUrl = PageURL;

        //                    li.Controls.Add(hpCategory);
        //                    ul.Controls.Add(li);
        //                }
        //            }

        //            pnl.Controls.Add(ul);
        //        }
        //        else
        //        {
        //            RadMenu1.Items.Clear();
        //            foreach (DataRow dr in dt.Rows)
        //            {
        //                if (Sites.ToBool(dr["bitStatus"]))
        //                {
        //                    if (dr["vcNavigationURL"].ToString().Contains("http"))
        //                        PageURL = dr["vcNavigationURL"].ToString();
        //                    else
        //                        PageURL = /*Session["SitePath"].ToString() + "/" + */"../" + dr["vcNavigationURL"].ToString();
        //                    RadMenuItem Item = new RadMenuItem(dr["vcTitle"].ToString(), PageURL);

        //                    if (PageURL.ToLower().Contains("bizportal"))
        //                    {
        //                        if (Sites.ToString(Session["Username"]).Length > 2 && Sites.ToString(Session["Password"]).Length > 2)
        //                        {
        //                            CCommon objCommon = new CCommon();
        //                            PageURL = Sites.ToString(Session["PortalURL"]) + "?&from=bizcart&u=" + Sites.ToString(Session["Username"]) + "&p=" + Sites.ToString(Session["Password"]);
        //                            Item.NavigateUrl = PageURL;
        //                            Item.Visible = true;
        //                        }
        //                        else
        //                            Item.Visible = false;
        //                    }
        //                    RadMenu1.Items.Add(Item);
        //                }
        //            }
        //            if (MenuDirection.ToLower().Contains("horizontal") || MenuDirection.ToLower().Contains("vertical"))
        //            {
        //                RadMenu1.Flow = (ItemFlow)Enum.Parse(typeof(ItemFlow), MenuDirection, true);
        //            }
        //            if (!string.IsNullOrEmpty(Skin))
        //                if (Skin.Contains("Black") || Skin.Contains("Default") || Skin.Contains("Forest")
        //                    || Skin.Contains("Hay") || Skin.Contains("Office2007") || Skin.Contains("Simple")
        //                    || Skin.Contains("Outlook") || Skin.Contains("Sunset") || Skin.Contains("Telerik")
        //                    || Skin.Contains("Vista") || Skin.Contains("Web20") || Skin.Contains("WebBlue"))
        //                {
        //                    RadMenu1.Skin = Skin;
        //                }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}



        // Possible Skin values (values are case sensetive)
        //1 Black
        //2 Default
        //3 Forest
        //4 Hay
        //5 Office2007
        //6 Simple
        //7 Outlook
        //8 Sunset
        //9 Telerik
        //10 Vista
        //11 Web20
        //12 WebBlue



        void bindHtml()
        {
            try
            {
                string strUI = GetUserControlTemplate("Menu.htm");
                DataTable dt;
                Sites objSite = new Sites();
                objSite.SiteID = Sites.ToLong(Session["SiteID"]);
                dt = objSite.GetSiteMenu();
                String PageURL = string.Empty;


                string CustomMenuPattern;
                string ForLoopPattern;
                CustomMenuPattern = "{##CustomMenu##}(?<Content>([\\s\\S]*?)){/##CustomMenu##}";
                ForLoopPattern = "<##FOR##>(?<Content>([\\s\\S]*?))<##ENDFOR##>";

                if (PopulateULLIbasedMenu != null)
                {
                    if (PopulateULLIbasedMenu.Contains("True"))
                    {
                        strUI = strUI.Replace("##DefaultMenu##", "");

                        StringBuilder sb = new StringBuilder();
                        if (Regex.IsMatch(strUI, CustomMenuPattern))
                        {
                            strUI = Sites.ReplaceMatchedPatternWithContent(strUI, CustomMenuPattern);
                            MatchCollection matches = Regex.Matches(strUI, ForLoopPattern);
                            if ((matches.Count > 0))
                            {
                                string Template;


                                foreach (DataRow dr in dt.Rows)
                                {
                                    if (Sites.ToBool(dr["bitStatus"]))
                                    {
                                        Template = matches[0].Groups["Content"].Value;


                                        if (dr["vcNavigationURL"].ToString().Contains("http"))
                                            PageURL = dr["vcNavigationURL"].ToString();
                                        else
                                            PageURL = "/" + dr["vcNavigationURL"].ToString();

                                        if (PageURL.ToLower().Contains("bizportal"))
                                        {
                                            if (Sites.ToString(Session["Username"]).Length > 2 && Sites.ToString(Session["Password"]).Length > 2)
                                            {
                                                CCommon objCommon = new CCommon();
                                                PageURL = Sites.ToString(Session["PortalURL"]) + "?&from=bizcart&u=" + Sites.ToString(Session["Username"]) + "&p=" + Sites.ToString(Session["Password"]);
                                            }
                                            else
                                                continue;
                                        }


                                        Template = Template.Replace("##MenuItemLink##", PageURL);
                                        Template = Template.Replace("##MenuItemName##", dr["vcTitle"].ToString());

                                        sb.Append(Template);
                                    }
                                }
                                strUI = Regex.Replace(strUI, ForLoopPattern, sb.ToString());

                            }
                        }

                    }
                    else
                    {
                        strUI = Sites.RemoveMatchedPattern(strUI, CustomMenuPattern);
                        RadMenu RadMenu1 = new RadMenu();
                        RadMenu1.ID = "RadMenu1";
                        RadMenu1.Skin = "Forest";
                        RadMenu1.CssClass = "qsfexMenu";

                        RadMenu1.Items.Clear();
                        foreach (DataRow dr in dt.Rows)
                        {
                            if (Sites.ToBool(dr["bitStatus"]))
                            {
                                if (dr["vcNavigationURL"].ToString().Contains("http"))
                                    PageURL = dr["vcNavigationURL"].ToString();
                                else
                                    PageURL = "/" + dr["vcNavigationURL"].ToString();
                                RadMenuItem Item = new RadMenuItem(dr["vcTitle"].ToString(), PageURL);

                                if (PageURL.ToLower().Contains("bizportal"))
                                {
                                    if (Sites.ToString(Session["Username"]).Length > 2 && Sites.ToString(Session["Password"]).Length > 2)
                                    {
                                        CCommon objCommon = new CCommon();
                                        PageURL = Sites.ToString(Session["PortalURL"]) + "?&from=bizcart&u=" + Sites.ToString(Session["Username"]) + "&p=" + Sites.ToString(Session["Password"]);
                                        Item.NavigateUrl = PageURL;
                                        Item.Visible = true;
                                    }
                                    else
                                        Item.Visible = false;
                                }
                                RadMenu1.Items.Add(Item);
                            }
                        }
                    }
                }
                else
                {
                    strUI = Sites.RemoveMatchedPattern(strUI, CustomMenuPattern);
                    RadMenu RadMenu1 = new RadMenu();
                    RadMenu1.ID = "RadMenu1";
                    RadMenu1.Skin = "Forest";
                    RadMenu1.CssClass = "qsfexMenu";

                    RadMenu1.Items.Clear();
                    foreach (DataRow dr in dt.Rows)
                    {
                        if (Sites.ToBool(dr["bitStatus"]))
                        {
                            if (dr["vcNavigationURL"].ToString().Contains("http"))
                                PageURL = dr["vcNavigationURL"].ToString();
                            else
                                PageURL = "/" + dr["vcNavigationURL"].ToString();
                            RadMenuItem Item = new RadMenuItem(dr["vcTitle"].ToString(), PageURL);

                            if (PageURL.ToLower().Contains("bizportal"))
                            {
                                if (Sites.ToString(Session["Username"]).Length > 2 && Sites.ToString(Session["Password"]).Length > 2)
                                {
                                    CCommon objCommon = new CCommon();
                                    PageURL = Sites.ToString(Session["PortalURL"]) + "?&from=bizcart&u=" + Sites.ToString(Session["Username"]) + "&p=" + Sites.ToString(Session["Password"]);
                                    Item.NavigateUrl = PageURL;
                                    Item.Visible = true;
                                }
                                else
                                    Item.Visible = false;
                            }
                            RadMenu1.Items.Add(Item);
                        }
                    }
                    if (MenuDirection.ToLower().Contains("horizontal") || MenuDirection.ToLower().Contains("vertical"))
                    {
                        RadMenu1.Flow = (ItemFlow)Enum.Parse(typeof(ItemFlow), MenuDirection, true);
                    }
                    if (!string.IsNullOrEmpty(Skin))
                        if (Skin.Contains("Black") || Skin.Contains("Default") || Skin.Contains("Forest")
                            || Skin.Contains("Hay") || Skin.Contains("Office2007") || Skin.Contains("Simple")
                            || Skin.Contains("Outlook") || Skin.Contains("Sunset") || Skin.Contains("Telerik")
                            || Skin.Contains("Vista") || Skin.Contains("Web20") || Skin.Contains("WebBlue"))
                        {
                            RadMenu1.Skin = Skin;
                        }

                    StringBuilder sb = new StringBuilder();
                    System.IO.StringWriter stWriter = new System.IO.StringWriter(sb);
                    HtmlTextWriter htmlWriter = new HtmlTextWriter(stWriter);

                    Page newPage = new Page();
                    HtmlForm newForm = new HtmlForm();

                    RadMenu1.Page = newPage;
                    RadMenu1.RegisterWithScriptManager = false;

                    newForm.Controls.Add(RadMenu1);
                    newPage.Controls.Add(newForm);
                    RadMenu1.RenderControl(htmlWriter);
                    RadMenu1.RegisterWithScriptManager = false;
                    strUI = strUI.Replace("##DefaultMenu##", sb.ToString());

                }


                pnlCutomizeHtml.Visible = false;
                litCutomizeHtml.Text = strUI;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}