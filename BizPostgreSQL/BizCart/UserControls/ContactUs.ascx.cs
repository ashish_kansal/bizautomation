﻿using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BACRM.BusinessLogic.ShioppingCart;
using System.Data;
using BACRM.BusinessLogic.Contacts;
using BACRM.BusinessLogic.Common;

namespace BizCart.UserControls
{
    public partial class ContactUs : BizUserControl
    {
        #region Events
   
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                Sites.InitialiseSession();
                if (!IsPostBack)
                {
                    BindContactAddress();
                    bindHtml();
                }
            }
            catch (Exception ex)
            {
                ExceptionModule.ExceptionPublish(ex, Sites.ToLong(Session["DomainID"]), Sites.ToLong(Session["UserContactID"]), Sites.ToLong(Session["SiteId"]), Request);
                ShowMessage(ex.ToString(), 1);
            }

        }
        
        #endregion

        #region Methods

        #region HTML
        private void bindHtml()
        {
            try
            {
                string strUI = GetUserControlTemplate("ContactUs.htm");
                strUI = strUI.Replace("##EmployerBillingAddress##", lblAddress.Text);
                strUI = strUI.Replace("##EmployerPhoneNumber##", lblPhone.Text);

                pnlCutomizeHtml.Visible = false;
                litCutomizeHtml.Text = strUI;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        private void BindContactAddress()
        {
            try
            {
                Sites objSite = new Sites();
                objSite.SetSessionsForCurrentSite(Sites.ToLong(Request["SiteID"]));
                DataTable dtTable = default(DataTable);
                CContacts objContacts = new CContacts();
                objContacts.DivisionID = Sites.ToLong(Session["DivisionID"]);
                objContacts.DomainID = Sites.ToLong(Session["DomainID"]);
                dtTable = objContacts.GetBillOrgorContAdd();
                lblAddress.Text = Sites.ToString(dtTable.Rows[0]["vcBillstreet"]) + "<br>" + Sites.ToString(dtTable.Rows[0]["vcBillCity"]) + "<br>" + Sites.ToString(dtTable.Rows[0]["BillState"]) + "&nbsp;&nbsp;" + Sites.ToString(dtTable.Rows[0]["BillCountry"]) + "-" + Sites.ToString(dtTable.Rows[0]["vcBillPostCode"]);
                lblPhone.Text = Sites.ToString(dtTable.Rows[0]["vcComPhone"]);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        
        #endregion
    }
}