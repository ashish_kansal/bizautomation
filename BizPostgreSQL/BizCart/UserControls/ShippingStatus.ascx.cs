﻿using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using BACRM.BusinessLogic.Common;
using BACRM.BusinessLogic.Admin;
using BACRM.BusinessLogic.Opportunities;
using BACRM.BusinessLogic.Documents;
using BACRM.BusinessLogic.Alerts;
using BACRM.BusinessLogic.ShioppingCart;
using System.Web.UI.HtmlControls;
using Microsoft.VisualBasic;
using nsoftware.InShip;
namespace BizCart.UserControls
{
    public partial class ShippingStatus : BizUserControl
    {
        nsoftware.InShip.Fedextrack _FedexTrack = new Fedextrack();
        nsoftware.InShip.Upstrack _UpsTrack = new Upstrack();
        long lngShippingCompanyId = 0;
        string trackingno = "";
        string errorMessage = "";

        string ismoretrackingNo = "";
        string oppid = "";
        string oppBizDocId = "";
        string ShippingReportId = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            lngShippingCompanyId = CCommon.ToInteger(Request.QueryString["companyId"]);
            trackingno = CCommon.ToString(Request.QueryString["trackingno"]);

            ismoretrackingNo = CCommon.ToString(Request.QueryString["ismoretrackingNo"]);
            oppid = CCommon.ToString(Request.QueryString["oppid"]);
            oppBizDocId = CCommon.ToString(Request.QueryString["oppBizDocId"]);
            ShippingReportId = CCommon.ToString(Request.QueryString["ShippingReportId"]);

            _UpsTrack.IdentifierType = UpstrackIdentifierTypes.uitMasterTrackingNumber;
            _UpsTrack.ShipDateStart = "20010219"; //yyyMMdd
            _UpsTrack.ShipDateEnd = "20190228"; //yyyMMdd
            NSAPIDETAILS(lngShippingCompanyId, _FedexTrack, _UpsTrack);
            
            //_FedexTrack.TrackShipment("956473415129602");
            if (lngShippingCompanyId == 88)
            {
                try
                {
                    _UpsTrack.TrackShipment(trackingno);
                    string CarrierCodeDescription = _UpsTrack.PackageReferences;
                    string ShipDate = _UpsTrack.ShipDate;
                    string PackageTrackingNumber = _UpsTrack.PackageTrackingNumber;
                    string PackageServiceTypeDescription = _UpsTrack.ServiceTypeDescription;
                    string PackagePackagingType = _UpsTrack.ServiceTypeDescription;
                    string PackageDeliveryStatus = "-";
                    string PackageDeliveryEstimate = "-";
                    string PackageDeliveryDate = "-";
                    string PackageDeliveryLocation = "-";
                    if (_UpsTrack.TrackEvents.Count > 0)
                    {
                        int count = 0;
                        PackageDeliveryStatus = _UpsTrack.TrackEvents[count].Status;
                        string date = Convert.ToString(_UpsTrack.TrackEvents[count].Date);
                        //if (date != "")
                        //{
                        //    date = ReturnDateTime(Convert.ToDateTime(date));
                        //}
                        PackageDeliveryEstimate = date + " " + Convert.ToString(_UpsTrack.TrackEvents[count].Time) + " " + PackageDeliveryStatus;
                        PackageDeliveryDate = _UpsTrack.TrackEvents[count].Date;
                        PackageDeliveryLocation = _UpsTrack.TrackEvents[count].Location + " " + _UpsTrack.TrackEvents[count].City + " " + _UpsTrack.TrackEvents[count].State + " " + _UpsTrack.TrackEvents[count].CountryCode;

                    }
                    string PackageSignedBy = _UpsTrack.PackageSignedBy;
                    lblStatus.Text = PackageDeliveryStatus;
                    lblLocation.Text = PackageDeliveryLocation;
                    lblShipCarrier.Text = PackagePackagingType;
                    lblTrackingId.Text = PackageTrackingNumber;
                    lblLatestEvent.Text = PackageDeliveryEstimate;
                    lblShipmentText.Text = PackageDeliveryStatus;
                }
                catch
                {
                    errorMessage = "Tracking No is Invalid";
                    lblStatus.Text = "-";
                lblLocation.Text = "-";
                lblShipCarrier.Text = "-";
                lblTrackingId.Text = "-";
                lblLatestEvent.Text = "-";
                lblShipmentText.Text = "-";
                }
            }
            else
            {
                try
                {
                    _FedexTrack.TrackShipment(trackingno);
                    string CarrierCodeDescription = _FedexTrack.CarrierCodeDescription;
                    string ShipDate = _FedexTrack.ShipDate;
                    string PackageTrackingNumber = _FedexTrack.PackageTrackingNumber;
                    string PackageServiceTypeDescription = _FedexTrack.PackageServiceTypeDescription;

                    string PackageDeliveryStatus = _FedexTrack.PackageDeliveryStatus;
                    string PackageDeliveryEstimate = _FedexTrack.PackageDeliveryEstimate;
                    string PackageDeliveryDate = _FedexTrack.PackageDeliveryDate;
                    string PackageDeliveryLocation = "-";
                    string PackagePackagingType = "-";
                    string PackageSignedBy = _FedexTrack.PackageSignedBy;
                    string latestUpdateOn = "";
                    if (_FedexTrack.TrackEvents.Count > 0)
                    {
                        int count = 0;
                        PackageDeliveryLocation = _FedexTrack.TrackEvents[count].City + " " + _FedexTrack.TrackEvents[count].CountryCode;
                        PackagePackagingType = _FedexTrack.TrackEvents[count].Status;
                        latestUpdateOn = _FedexTrack.TrackEvents[count].Date;
                    }
                    if (PackageDeliveryEstimate != "")
                    {
                        PackageDeliveryEstimate = ReturnDateTime(Convert.ToDateTime(PackageDeliveryEstimate));
                    }
                    if (PackageDeliveryDate != "")
                    {
                        PackageDeliveryDate = ReturnDateTime(Convert.ToDateTime(PackageDeliveryDate));
                    }
                    lblStatus.Text = PackageDeliveryStatus;
                    lblLocation.Text = PackageDeliveryLocation;
                    lblShipCarrier.Text = PackageServiceTypeDescription;
                    lblTrackingId.Text = PackageTrackingNumber;
                    lblLatestEvent.Text = PackagePackagingType;
                    lblShipmentText.Text = latestUpdateOn + "," + PackagePackagingType + "," + PackageDeliveryLocation;
                }
                catch
                {
                    errorMessage = "Tracking No is Invalid";
                    lblStatus.Text = "-";
                    lblLocation.Text = "-";
                    lblShipCarrier.Text = "-";
                    lblTrackingId.Text = "-";
                    lblLatestEvent.Text = "-";
                    lblShipmentText.Text = "-";
                }
            }
            bindHtml();
        }

        public string ReturnDateTime(Object CloseDate)
        {
            try
            {
                string strTargetResolveDate = "";
                string temp = "";

                if (!(CloseDate == System.DBNull.Value))
                {
                    DateTime dtCloseDate = DateTime.Parse(CloseDate.ToString());
                    strTargetResolveDate = modBacrm.FormattedDateFromDate(dtCloseDate, Sites.ToString(Session["DateFormat"]));

                    string timePart = dtCloseDate.ToShortTimeString().Substring(0, dtCloseDate.ToShortTimeString().Length - 1);
                    // remove gaps
                    if (timePart.Split(' ').Length >= 2) timePart = timePart.Split(' ').GetValue(0).ToString() + timePart.Split(' ').GetValue(1).ToString();

                    // check Today date components [ Date , Month , Year ] with Parameter CloseDate components [ Date , Month , Year ] 
                    // if both are same it is today
                    //if ((dtCloseDate.Date == DateTime.Now.Date & dtCloseDate.Month == DateTime.Now.Month & dtCloseDate.Year == DateTime.Now.Year))
                    //{
                    //    strTargetResolveDate = "<font color=red><b>Today</b></font>";

                    //    return strTargetResolveDate;
                    //}
                    // check [ TodayDate + 1 ] Date.... components [ Date , Month , Year ] with Parameter dtCloseDate components [ Date , Month , Year ] 
                    // if both are same it was Yesterday
                    //else if ((dtCloseDate.Date.AddDays(1).Date == DateTime.Now.Date & dtCloseDate.AddDays(1).Month == DateTime.Now.Month & dtCloseDate.AddDays(1).Year == DateTime.Now.Year))
                    //{
                    //    strTargetResolveDate = "<font color=purple><b>Yesterday</b></font>";

                    //    return strTargetResolveDate;
                    //}
                    // check TodayDate .... components [ Date , Month , Year ] with Parameter [ dtCloseDate + 1 ] Date....  components [ Date , Month , Year ] 
                    // if both are same it will Tomorrow
                    //else if ((dtCloseDate.Date == DateTime.Now.AddDays(1).Date & dtCloseDate.Month == DateTime.Now.AddDays(1).Month & dtCloseDate.Year == DateTime.Now.AddDays(1).Year))
                    //{
                    //    temp = dtCloseDate.Hour.ToString() + ":" + dtCloseDate.Minute.ToString();
                    //    strTargetResolveDate = "<font color=orange><b>Tomorrow</b></font>";

                    //    return strTargetResolveDate;
                    //}
                    // display day name for next 4 days from DateTime.Now

                    //else if (dtCloseDate.ToString("yyyyMMdd") == DateTime.Now.AddDays(2).ToString("yyyyMMdd") | dtCloseDate.ToString("yyyyMMdd") == DateTime.Now.AddDays(3).ToString("yyyyMMdd") | dtCloseDate.ToString("yyyyMMdd") == DateTime.Now.AddDays(4).ToString("yyyyMMdd") | dtCloseDate.ToString("yyyyMMdd") == DateTime.Now.AddDays(5).ToString("yyyyMMdd"))
                    //{
                    //    strTargetResolveDate = "<b>" + dtCloseDate.DayOfWeek.ToString() + "</b>";
                    //    return strTargetResolveDate;
                    //}
                    //else
                    //{
                    //    //strTargetResolveDate = strTargetResolveDate;
                    //    return strTargetResolveDate;
                    //}
                }
                return strTargetResolveDate;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void bindHtml()
        {
            string strUI = GetUserControlTemplate("ShippingStatus.htm");
            strUI = strUI.Replace("##Status##", CCommon.RenderControl(lblStatus));
            strUI = strUI.Replace("##Location##", CCommon.RenderControl(lblLocation));
            strUI = strUI.Replace("##ShipCarrier##", CCommon.RenderControl(lblShipCarrier));
            strUI = strUI.Replace("##TrackingId##", CCommon.RenderControl(lblTrackingId));
            strUI = strUI.Replace("##LastEvent##", CCommon.RenderControl(lblLatestEvent));
            strUI = strUI.Replace("##ShippingStatusUpdateViaText##", CCommon.RenderControl(lblShipmentText));
            strUI = strUI.Replace("##ErrorMessage##", errorMessage);
            if(ismoretrackingNo=="0"){
                strUI=strUI.Replace("##moretrackingno##","");
            }else{
            strUI=strUI.Replace("##moretrackingno##","<a href=\"javascript:void(0)\" onclick=\"OpenShippingReport('"+ShippingReportId+"','"+oppBizDocId+"','"+oppid+"')\">You have more than one tracking no</a>");
            }
           lblHtmlControl.Text = strUI;
            pnlCustomize.Visible = false;
        }
        
    }
}