﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ItemImage.ascx.cs" Inherits="BizCart.ItemImage" %>
<script type="text/javascript">
    function windowResize() {
        if (document.getElementById("resizeDiv") != null) {
            var width = document.getElementById("resizeDiv").offsetWidth;
            var height = document.getElementById("resizeDiv").offsetHeight;
            window.resizeTo(width + 35, height + 150);
        }
    }
</script>
<div id="resizeDiv" style="float: left;">
    <asp:Panel ID="pnlCutomizeHtml" runat="server">
        <table align="center">
            <tr>
                <td align="center">
                    <asp:Image ID="imgItem" runat="server" BorderWidth="1" BorderColor="1"></asp:Image>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Literal ID="litCutomizeHtml" runat="server"></asp:Literal>
</div>
