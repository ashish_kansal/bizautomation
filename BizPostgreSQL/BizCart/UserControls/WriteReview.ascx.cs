﻿using System;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BACRM.BusinessLogic.Item;
using BACRM.BusinessLogic.ShioppingCart;
using BACRM.BusinessLogic.Common;
using System.Text.RegularExpressions;
using System.Text;
using System.Collections;



namespace BizCart.UserControls
{
    public partial class WriteReview : BizUserControl
    {
        #region Global
        QueryStringValues objEncryption = new QueryStringValues();
        #endregion

        #region Events
        protected void Page_Load(object sender, EventArgs e)
        {
            // Page.DataBind();

            try
            {
                Sites.InitialiseSession();
                if (!IsPostBack)
                {
                    if (Sites.ToLong(Session["UserContactID"]) == 0 && Sites.ToInteger(Session["OnePageCheckout"]) == 0)
                    {
                        Response.Redirect("Login.aspx?ReturnURL=" + (Sites.ToString(Session["SiteLiveURL"]).Trim().Length > 2 ? "https://" : "http://") + Request.Url.Host + "/WriteReview.aspx?ItemId=" + this.ItemId + "%26CategoryName=" + this.CategoryName + "%26ProductName=" + this.ProductName, false);
                    }

                    hfSiteId.Value = objEncryption.Encrypt(CCommon.ToString(Session["SiteId"]));
                    hfDomainId.Value = objEncryption.Encrypt(CCommon.ToString(Session["DomainId"]));
                    hfTypeId.Value = objEncryption.Encrypt("2");
                    hfItemId.Value = objEncryption.Encrypt(CCommon.ToString(this.ItemId));
                    hfIpAddress.Value = objEncryption.Encrypt(GetIpAddress());
                    hfContactId.Value = objEncryption.Encrypt(CCommon.ToString(Session["ContactId"]));

                    bindHtml();
                }
            }
            catch (Exception ex)
            {
                ExceptionModule.ExceptionPublish(ex, Sites.ToLong(Session["DomainID"]), Sites.ToLong(Session["UserContactID"]), Sites.ToLong(Session["SiteId"]), Request);
                ShowMessage(ex.ToString(), 1);
            }
        }
        protected override void Render(HtmlTextWriter writer)
        {
            pnlCutomizeHtml.Visible = false;
            base.Render(writer);
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtTitle.Text != "")
                {
                    if (txtReview.Text != "")
                    {
                        if (txtReview.Text.Length > 100)
                        {
                            if (CaptchaControl1.IsValid)
                            {
                                Review objReview = new Review();
                                objReview.TypeId = 2;
                                objReview.ReferenceId = this.ItemId;
                                objReview.ReviewTitle = txtTitle.Text;
                                objReview.ReviewComment = txtReview.Text;
                                objReview.EmailMe = cbReviewEmail.Checked;
                                objReview.IpAddress = objEncryption.Decrypt(hfIpAddress.Value);
                                objReview.ContactId = CCommon.ToLong(Session["ContactId"]);
                                objReview.SiteId = CCommon.ToLong(Session["SiteId"]);
                                objReview.DomainID = CCommon.ToLong(Session["DomainId"]);
                                objReview.CreatedDate = Convert.ToString(System.DateTime.Now);
                                objReview.Hide = false;
                                //Here as per configuration set in admin side we pass default value for Approved
                                objReview.Approved = true;

                                if (objReview.ManageReview())
                                {
                                    Response.Redirect("/WriteReview.aspx?ReviewId=" + objReview.ReviewId + "&ItemID=" + this.ItemId + "&CategoryName=" + this.CategoryName + "&ProductName=" + this.ProductName);
                                }
                            }
                            else
                            {
                                this.ShowCaptchaMessage = true;
                                ShowMessage(GetErrorMessage("ERR065"), 1);
                            }
                        }
                        else
                        {
                            ShowMessage(GetErrorMessage("ERR064"), 1);
                        }
                    }
                    else
                    {
                        ShowMessage(GetErrorMessage("ERR063"), 1);
                    }

                }
                else
                {
                    ShowMessage(GetErrorMessage("ERR062"), 1);
                }

                bindHtml();

            }
            catch (Exception ex)
            {
                ExceptionModule.ExceptionPublish(ex, Sites.ToLong(Session["DomainID"]), Sites.ToLong(Session["UserContactID"]), Sites.ToLong(Session["SiteId"]), Request);
                ShowMessage(ex.ToString(), 1);
            }

        }
        #endregion

        #region Methods
        void bindHtml()
        {
            try
            {
                string strUI = GetUserControlTemplate("WriteReview.htm");

                string WriteReviewPattern = "{##WriteReviewSection##}(?<Content>([\\s\\S]*?)){/##WriteReviewSection##}";
                string ThankYouForReviewPattern = "{##ThankYouForReviewSection##}(?<Content>([\\s\\S]*?)){/##ThankYouForReviewSection##}";

                if (this.ReviewId != "")
                {
                    strUI = Sites.RemoveMatchedPattern(strUI, WriteReviewPattern);
                    strUI = strUI.Replace("##ProductUrl##", GetProductURL(this.CategoryName, this.ProductName, this.ItemId));
                    strUI = Sites.ReplaceMatchedPatternWithContent(strUI, ThankYouForReviewPattern);
                }
                else
                {

                    strUI = Sites.RemoveMatchedPattern(strUI, ThankYouForReviewPattern);
                    strUI = Sites.ReplaceMatchedPatternWithContent(strUI, WriteReviewPattern);
                    strUI = strUI.Replace("##ProductName##", this.ProductName);
                    strUI = strUI.Replace("##ReviewTitle##", CCommon.RenderControl(txtTitle));
                    strUI = strUI.Replace("##Review##", CCommon.RenderControl(txtReview));
                    strUI = strUI.Replace("##ReviewEmail##", CCommon.RenderControl(cbReviewEmail));
                    //strUI = strUI.Replace("##CaptchaText##", CCommon.RenderControl(txtCaptcha));
                    //strUI = strUI.Replace("##Captcha##", CCommon.RenderControl(img));
                    strUI = strUI.Replace("##Submit##", CCommon.RenderControl(btnSubmit));
                    strUI = strUI.Replace("##Captcha##", CCommon.RenderControl(CaptchaControl1));
                    strUI = strUI.Replace("##ItemId##", this.ItemId);

                    if (this.ShowCaptchaMessage)
                    {
                        strUI = strUI.Replace("##CaptchaMessage##", CCommon.RenderControl(ltrlMsg));
                    }
                    else
                    {
                        strUI = strUI.Replace("##CaptchaMessage##", "");
                    }
                    int RatingCount = 0;
                    RatingCount = GetRatings();

                    string[] title = { "Very Poor", "Poor", "Ok", "Good", "very Good" };
                    string strRatingControl = GetRatingContol("WriteItemRating", false, false, RatingCount, false, 0, "auto-submit-star", true, title);

                    strUI = strUI.Replace("##WriteRating##", strRatingControl);

                    int ReviewCount = 0;
                    ReviewCount = GetReviewCount();

                    if (ReviewCount > 0)
                    {
                        strUI = strUI.Replace("##ReadReviewLable##", "Read Reviews");
                        strUI = strUI.Replace("##ReadReviewCount##", "(" + ReviewCount + ")");
                    }
                    else
                    {
                        strUI = strUI.Replace("##ReadReviewLable##", "");
                        strUI = strUI.Replace("##ReadReviewCount##", "");
                    }
                }
                this.ShowCaptchaMessage = false;
                litCutomizeHtml.Text = strUI;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        public int GetRatings()
        {
            try
            {
                DataTable dtRating = new DataTable();
                Review objReview = new Review();

                objReview.ReferenceId = CCommon.ToString(this.ItemId);
                objReview.ContactId = CCommon.ToLong(Session["ContactId"]);
                objReview.DomainID = CCommon.ToLong(Session["DomainId"]);
                objReview.SiteId = CCommon.ToLong(Session["SiteId"]);
                objReview.TypeId = 2;
                objReview.Mode = 1;

                dtRating = objReview.GetRatings();
                if (dtRating.Rows.Count > 0)
                {
                    return CCommon.ToInteger(dtRating.Rows[0]["intRatingCount"]);
                }
                return 0;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public int GetReviewCount()
        {
            try
            {
                DataTable dtRating = new DataTable();
                Review objReview = new Review();

                objReview.ReferenceId = CCommon.ToString(this.ItemId);
                objReview.DomainID = CCommon.ToLong(Session["DomainId"]);
                objReview.SiteId = CCommon.ToLong(Session["SiteId"]);
                objReview.TypeId = 2;
                objReview.Mode = 5;

                dtRating = objReview.GetReviews();
                if (dtRating.Rows.Count > 0)
                {
                    return CCommon.ToInteger(dtRating.Rows[0]["intReviewCount"]);
                }
                return 0;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        #endregion

        #region Custom Properties
        public String ItemId
        {
            get
            {
                object o = ViewState["ItemId"];
                if (o != null)
                {
                    return (string)o;
                }
                else
                {
                    if (CCommon.ToString(Request.QueryString["ItemId"]) != "")
                    {
                        ViewState["ItemId"] = Request.QueryString["ItemId"];
                        return (string)ViewState["ItemId"];
                    }
                    else
                    {
                        return "";
                    }
                }
            }
        }
        public String ReviewId
        {
            get
            {
                object o = ViewState["ReviewId"];
                if (o != null)
                {
                    return (string)o;
                }
                else
                {
                    if (CCommon.ToString(Request.QueryString["ReviewId"]) != "")
                    {
                        ViewState["ReviewId"] = Request.QueryString["ReviewId"];
                        return (string)ViewState["ReviewId"];
                    }
                    else
                    {
                        return "";
                    }
                }
            }
        }
        public String CategoryName
        {
            get
            {
                object o = ViewState["CategoryName"];
                if (o != null)
                {
                    return (string)o;
                }
                else
                {
                    if (CCommon.ToString(Request.QueryString["CategoryName"]) != "")
                    {
                        ViewState["CategoryName"] = Request.QueryString["CategoryName"];
                        return (string)ViewState["CategoryName"];
                    }
                    else
                    {
                        return "";
                    }
                }
            }
        }
        public String ProductName
        {
            get
            {
                object o = ViewState["ProductName"];
                if (o != null)
                {
                    return (string)o;
                }
                else
                {
                    if (CCommon.ToString(Request.QueryString["ProductName"]) != "")
                    {
                        ViewState["ProductName"] = Request.QueryString["ProductName"];
                        return (string)ViewState["ProductName"];
                    }
                    else
                    {
                        return "";
                    }
                }
            }
        }
        public Boolean ShowCaptchaMessage
        {
            get
            {
                object o = ViewState["ShowCaptchaMessage"];
                if (o != null)
                {
                    return (Boolean)o;
                }
                return false;
            }
            set
            {
                ViewState["ShowCaptchaMessage"] = value;
            }
        }
        #endregion

        #region ExtraCode
        //string  strCaptchaError = "";
        //strCaptchaError = strCaptchaError + "<div id =\"review_help_message_captcha\" style=\"display:block;color:#B00;padding:5px 10px;background:#F6E0E0;border:1px solid #B00;\">";
        //strCaptchaError = strCaptchaError + "<font size=\"2\">The following errors have occurred:</font>";            
        //strCaptchaError = strCaptchaError + "<ul  style=\"list-style:none;\">";
        //strCaptchaError = strCaptchaError + "<li style =\"margin:5px 20px;\">";
        //strCaptchaError = strCaptchaError + "Incorrect Captcha Input.";
        //strCaptchaError = strCaptchaError + "</li></ul></div>";                                
        //ltrlMsg.Text = strCaptchaError;
        #endregion
    }
}