﻿using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using BACRM.BusinessLogic.Item;
using System.Data;
using BACRM.BusinessLogic.Admin;
using BACRM.BusinessLogic.Common;
using BACRM.BusinessLogic.ShioppingCart;
using Telerik.Web.UI;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace BizCart.UserControls
{
    public partial class Categories : BizUserControl
    {
        #region Global Declaration

        public string PreserveStateofTree { get; set; }
        public string ShowOnlyParentCategoriesinTree { get; set; }
        StringBuilder strBuilMain = new StringBuilder();
        HtmlGenericControl div = new HtmlGenericControl("div");

        #endregion

        #region Member Variables

        private string strUI = "";

        private string childHtml;
        private string childContainer;
        private string parentHtml;
        private string parentContainer;

        private string parentContainerStart;
        private string parentContainerEnd;
        private string childContainerStart;
        private string childContainerEnd;
        private string parentCategoryStart;
        private string parentCategoryEnd;
        private string childCategoryStart;
        private string childCategoryEnd;

        private string strCategory;
        #endregion

        #region Events
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                Sites.InitialiseSession();
                strUI = GetUserControlTemplate("Categories.htm");
                //if (Session["TreeViewState"] != null)
                //    TreeViewCategory.ExpandDepth = -1;
                //PopulateCategories();

                if (strUI.Contains("<##TreeViewCustom##>"))
                {
                    //Replace default implementation
                    strUI = strUI.Replace("##TreeView##", "");
                    LoadCustomTreeView();
                }
                else
                {
                    LoadCategories();
                }

                bindHtml();

            }
            catch (Exception ex)
            {
                ExceptionModule.ExceptionPublish(ex, Sites.ToLong(Session["DomainID"]), Sites.ToLong(Session["UserContactID"]), Sites.ToLong(Session["SiteId"]), Request);
                ShowMessage(ex.ToString(), 1);
            }

        }

        ////protected void TreeViewCategory_TreeNodeCollapsed(object sender, TreeNodeEventArgs e)
        ////{
        ////    if (IsPostBack)
        ////    {
        ////        AddCookieIfNotExist();

        ////        if (Request.Cookies["CatID"].Value.Contains(e.Node.Value) == true)
        ////            Response.Cookies["CatID"].Value = Request.Cookies["CatID"].Value.Replace(e.Node.Value + ",", "");
        ////    }
        ////}
        ////protected void TreeViewCategory_TreeNodeExpanded(object sender, TreeNodeEventArgs e)
        ////{
        ////    if (IsPostBack)
        ////    {
        ////        AddCookieIfNotExist();

        ////        if (Request.Cookies["CatID"] != null)
        ////            if (Request.Cookies["CatID"].Value.Contains(e.Node.Value) == false)
        ////                Response.Cookies["CatID"].Value = Request.Cookies["CatID"].Value + e.Node.Value + ",";
        ////    }
        ////}

        #endregion

        #region Methods
        #region HTML
        private void bindHtml()
        {
            try
            {
                strUI = strUI.Replace("##Menu##", CCommon.RenderControl(phMenu));

                if (strUI.Contains("##TreeViewCustom##"))
                {
                    strUI = strUI.Replace("##TreeViewCustom##", strCategory);
                }
                else
                {
                    strUI = strUI.Replace("##TreeView##", CCommon.RenderControl(div));
                }

                pnlCutomizeHtml.Visible = false;
                litCutomizeHtml.Text = strUI;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void LoadCategories()
        {
            try
            {
                CItems objItems = new CItems();
                DataTable dt = null;
                objItems.byteMode = 13;
                objItems.SiteID = Sites.ToLong(Session["SiteID"]);

                dt = objItems.SeleDelCategory();

                if (dt.Rows.Count > 0)
                {
                    string EndUL = "</ul>";
                    string StartLi = "<li>";
                    string EndLi = "</li>";
                    string startSpanText = "<span>text";
                    string EndSpanText = "</span>";
                    string startSpanValue = "<span id=\"value\">";
                    string EndSpanValue = "</span>";

                    int intCnt = 0;

                    DataRow[] drLevel = dt.Select("category = 0");

                    string strLink = "";

                    foreach (DataRow dr in drLevel)
                    {
                        intCnt = intCnt + 1;
                        HtmlGenericControl li = new HtmlGenericControl("li");

                        if (intCnt == 1)
                        {
                            strBuilMain.Append("<ul id=\"treeData\" >");
                        }

                        DataRow[] drSubCategories = dt.Select("Category = " + CCommon.ToLong(dr["numCategoryID"]));

                        if (dr["Type"].ToString() == "0")
                        {
                            strLink = "/SubCategory/" + Sites.GenerateSEOFriendlyURL(dr["vcCategoryName"].ToString()) + "/" + dr["numCategoryID"].ToString();
                        }
                        else if (dr["Type"].ToString() == "1")
                        {
                            strLink = dr["vcLink"].ToString();
                        }
                        else if (dr["Type"].ToString() == "2")
                        {
                            strLink = "/Product.aspx?ItemID=" + dr["numCategoryID"].ToString();
                        }

                        if (drSubCategories.Length > 0)
                        {
                            bool IsParent = true;

                            strBuilMain.Append(StartLi + startSpanText.Replace("text", "<a onclick=\"openLink('" + strLink + "')\">" + dr["vcCategoryName"].ToString() + "</a>") + EndSpanText + startSpanValue.Replace("value", dr["numCategoryID"].ToString()) + EndSpanValue);

                            PopulateNode(drSubCategories, IsParent, dt);

                            strBuilMain.Append(EndUL);
                            strBuilMain.Append(EndLi);

                        }
                        else
                        {
                            if (dr["Type"].ToString() == "0")
                            {
                                strLink = "/SubCategory/" + Sites.GenerateSEOFriendlyURL(dr["vcCategoryName"].ToString()) + "/" + dr["numCategoryID"].ToString();
                            }
                            else if (dr["Type"].ToString() == "1")
                            {
                                strLink = dr["vcLink"].ToString();
                            }
                            else if (dr["Type"].ToString() == "2")
                            {
                                strLink = "/Product.aspx?ItemID=" + dr["numCategoryID"].ToString();
                            }

                            strBuilMain.Append(StartLi + startSpanText.Replace("text", "<a onclick=\"openLink('" + strLink + "')\">" + dr["vcCategoryName"].ToString() + "</a>") + EndSpanText + startSpanValue.Replace("value", dr["numCategoryID"].ToString()) + EndSpanValue + EndLi);
                        }
                    }
                    strBuilMain.Append(EndUL);

                    Literal litULCollection = new Literal();
                    litULCollection.Text = CCommon.ToString(strBuilMain);
                    div.ID = "tree";
                    div.Controls.Add(litULCollection);
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        private void PopulateNode(DataRow[] dr, bool IsParent, DataTable dtMain)
        {

            try
            {
                string StartUL = "<ul>";
                string EndUL = "</ul>";
                string StartLi = "<li>";
                string EndLi = "</li>";
                string startSpanText = "<span>text";
                string EndSpanText = "</span>";
                string startSpanValue = "<span id=\"value\">";
                string EndSpanValue = "</span>";
                bool IsFromInnerMethod = false;

                string strLink = "";

                foreach (DataRow drSub in dr)
                {

                    if (dtMain.Select("Category = " + CCommon.ToLong(drSub["numCategoryID"])).Length > 0)
                    {
                        if (drSub["Type"].ToString() == "0")
                        {
                            strLink = "/SubCategory/" + Sites.GenerateSEOFriendlyURL(drSub["vcCategoryName"].ToString()) + "/" + drSub["numCategoryID"].ToString();
                        }
                        else if (drSub["Type"].ToString() == "1")
                        {
                            strLink = drSub["vcLink"].ToString();
                        }
                        else if (drSub["Type"].ToString() == "2")
                        {
                            strLink = "/Product.aspx?ItemID=" + drSub["numCategoryID"].ToString();
                        }

                        if (IsParent == true)
                        {
                            strBuilMain.Append(StartUL);
                        }

                        strBuilMain.Append(StartLi + startSpanText.Replace("text", "<a onclick=\"openLink('" + strLink + "')\">" + drSub["vcCategoryName"].ToString() + " </a>") + EndSpanText + startSpanValue.Replace("value", drSub["numCategoryID"].ToString()) + EndSpanValue);

                        DataRow[] drSubSub = dtMain.Select("Category = " + CCommon.ToLong(drSub["numCategoryID"]));

                        if (drSubSub.Length > 0)
                        {
                            IsParent = true;
                        }
                        else
                        {
                            IsParent = false;
                        }

                        IsFromInnerMethod = true;
                        PopulateNode(drSubSub, IsParent, dtMain);

                        if (IsParent == true)
                        {
                            strBuilMain.Append(EndUL);
                            strBuilMain.Append(EndLi);
                        }


                    }
                    else
                    {
                        if (drSub["Type"].ToString() == "0")
                        {
                            strLink = "/SubCategory/" + Sites.GenerateSEOFriendlyURL(drSub["vcCategoryName"].ToString()) + "/" + drSub["numCategoryID"].ToString();
                        }
                        else if (drSub["Type"].ToString() == "1")
                        {
                            strLink = drSub["vcLink"].ToString();
                        }
                        else if (drSub["Type"].ToString() == "2")
                        {
                            strLink = "/Product.aspx?ItemID=" + drSub["numCategoryID"].ToString();
                        }

                        if (IsParent == true && IsFromInnerMethod == false)
                        {
                            strBuilMain.Append(StartUL);
                        }

                        //"<a onclick=\"openLink('" + Conversion.Str(1) + "')\">"
                        strBuilMain.Append(StartLi + startSpanText.Replace("text", "<a onclick=\"openLink('" + strLink + "')\">" + drSub["vcCategoryName"].ToString() + "</a>") + EndSpanText + startSpanValue.Replace("value", drSub["numCategoryID"].ToString()) + EndSpanValue + EndLi);

                        if (dtMain.Select("Category = " + CCommon.ToLong(drSub["numCategoryID"])).Length > 0)
                        {
                            IsParent = true;
                        }
                        else
                        {
                            IsParent = false;
                        }

                    }

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void LoadCustomTreeView()
        {
            try
            {
                string treeViewPattern = "<##TreeViewCustom##>(?<Content>([\\s\\S]*?))</##TreeViewCustom##>";
                string parentForLoopPattern = "<##FOREACH##>(?<Content>([\\s\\S]*?))</##FOREACH##>";
                string ifChildAvailablePattern = "<##IFCHILDAVAILABLE##>(?<Content>([\\s\\S]*?))</##IFCHILDAVAILABLE##>";
                string childForLoopPattern = "<##FOREACHCHILD##>(?<Content>([\\s\\S]*?))</##FOREACHCHILD##>";

                string categoryLinkStartPattern = "<##FOREACH##>(?<Content>([\\s\\S]*?))<##IFCHILDAVAILABLE##>";
                string categoryLinkEndPattern = "</##IFCHILDAVAILABLE##>(?<Content>([\\s\\S]*?))</##FOREACH##>";


                //GET PARENT CONTAINER HTML START AND END TAG
                string htmlParentContainer = Regex.Match(strUI, treeViewPattern, RegexOptions.None).Value; 
                htmlParentContainer = htmlParentContainer.Replace("<##TreeViewCustom##>", "").Replace("</##TreeViewCustom##>", "");
                string parentForEach = Regex.Match(htmlParentContainer, "<##FOREACH##>(?<Content>([\\s\\S]*?))</##FOREACH##>", RegexOptions.None).Value;
                htmlParentContainer = htmlParentContainer.Replace(parentForEach, "##SEPERATOR##");

                parentContainerStart = htmlParentContainer.Substring(0, htmlParentContainer.IndexOf("##SEPERATOR##")).Trim();
                parentContainerEnd = htmlParentContainer.Substring(htmlParentContainer.IndexOf("##SEPERATOR##") + 13).Trim();


                //GET START AND END TAG OF CHILD CONTAINER
                string htmlChildContainer = Regex.Match(strUI, ifChildAvailablePattern, RegexOptions.None).Value;
                htmlChildContainer = htmlChildContainer.Replace("<##IFCHILDAVAILABLE##>", "").Replace("</##IFCHILDAVAILABLE##>", "");

                string childForEach = Regex.Match(htmlChildContainer, "<##FOREACHCHILD##>(?<Content>([\\s\\S]*?))</##FOREACHCHILD##>", RegexOptions.None).Value;
                htmlChildContainer = htmlChildContainer.Replace(childForEach, "##SEPERATOR##");
  
                childContainerStart = htmlChildContainer.Substring(0, htmlChildContainer.IndexOf("##SEPERATOR##")).Trim();
                childContainerEnd = htmlChildContainer.Substring(htmlChildContainer.IndexOf("##SEPERATOR##") + 13).Trim();

                //GET PARENT CATEGORY START AND END TAG OF CATEGORY LINK
                parentCategoryStart = Regex.Match(strUI, categoryLinkStartPattern, RegexOptions.None).Value;
                parentCategoryStart = parentCategoryStart.Replace("<##FOREACH##>", "").Replace("<##IFCHILDAVAILABLE##>", "").Trim();
                parentCategoryStart = Regex.Replace(parentCategoryStart, @">\s+</", "></");

                parentCategoryEnd = Regex.Match(strUI, categoryLinkEndPattern, RegexOptions.None).Value;
                parentCategoryEnd = parentCategoryEnd.Replace("</##IFCHILDAVAILABLE##>", "").Replace("</##FOREACH##>", "").Trim();
                parentCategoryEnd = Regex.Replace(parentCategoryEnd, @">\s+</", "></");

                //1. Extract html for child element
                string childForLoop = Regex.Match(strUI, childForLoopPattern, RegexOptions.None).Value;
                childHtml = childForLoop.Replace("<##FOREACHCHILD##>", "").Replace("</##FOREACHCHILD##>", "").Trim();
                childHtml = Regex.Replace(childHtml, @">\s+</", "></");
                //Remove extracted information because it is not required now
                strUI = strUI.Replace(childForLoop, "");

                //GET CHILD CATEGORY START AND END TAG OF CATEGORY LINK
                childCategoryStart = childHtml.Substring(0, childHtml.IndexOf("<##INSERTCHILDHERE##>")).Trim();
                childCategoryEnd = childHtml.Substring(childHtml.IndexOf("<##INSERTCHILDHERE##>") + 21).Trim();

                //2. Extract html for container of child element
                string ifChildAvailable = Regex.Match(strUI, ifChildAvailablePattern, RegexOptions.None).Value;
                childContainer = ifChildAvailable.Replace("<##IFCHILDAVAILABLE##>", "").Replace("</##IFCHILDAVAILABLE##>", "").Trim();
                childContainer = Regex.Replace(childContainer, @">\s+</", "></");
                //Remove extracted information because it is not required now
                strUI = strUI.Replace(ifChildAvailable, "");

                //3. Extract parent html
                string parentForLoop = Regex.Match(strUI, parentForLoopPattern, RegexOptions.None).Value;
                parentHtml = parentForLoop.Replace("<##FOREACH##>", "").Replace("</##FOREACH##>", "").Trim();
                parentHtml = Regex.Replace(parentHtml, @">\s+</", "></");
                //Remove extracted information because it is not required now
                strUI = strUI.Replace(parentForLoop, "");

                //4. Extract html for container of parent element
                string treeView = Regex.Match(strUI, treeViewPattern, RegexOptions.None).Value;
                parentContainer = treeView.Replace("<##TreeViewCustom##>", "").Replace("</##TreeViewCustom##>", "").Trim();
                parentContainer = Regex.Replace(parentContainer, @">\s+</", "></");
                //Remove extracted information because it is not required now
                strUI = Regex.Replace(strUI, treeViewPattern, "##TreeViewCustom##");

                strCategory = parentContainerStart;

                CItems objItems = new CItems();
                DataTable dt = null;
                objItems.byteMode = 13;
                objItems.SiteID = Sites.ToLong(Session["SiteID"]);

                dt = objItems.SeleDelCategory();

                if (dt.Rows.Count > 0)
                {
                    int parentID = 0;
                    DataRow[] arrDataRow = dt.Select("category = 0");

                    foreach (DataRow drCatgeory in arrDataRow)
                    {
                        string parentCategory = HttpUtility.HtmlDecode(parentCategoryStart.Replace("##CategoryName##", CCommon.ToString(drCatgeory["vcCategoryName"])).Replace("##CategoryID##", CCommon.ToString(drCatgeory["numCategoryID"])).Replace("##ITEMID##", "item-" + parentID.ToString()));
                        DataRow[] arrSubCategory = dt.Select("Category=" + CCommon.ToString(drCatgeory["numCategoryID"]));

                        if (arrSubCategory.Length > 0)
                        {
                            LoadSubCategory(dt, drCatgeory, "item-" + parentID.ToString(), ref parentCategory);
                        }

                        parentID = parentID + 1;
                        strCategory = strCategory + parentCategory + parentCategoryEnd;
                    }
                }

                strCategory = strCategory + parentContainerEnd;
            }
            catch
            {
                throw;
            }
        }

        public int LoadSubCategory(DataTable dt, DataRow drParentCategory,string id, ref string parentCategory)
        {
            DataRow[] arrSubCategory = dt.Select("Category=" + CCommon.ToString(drParentCategory["numCategoryID"]));

            if (arrSubCategory != null && arrSubCategory.Length > 0)
            {
                int parentID = 0;
                parentCategory = parentCategory + childContainerStart;

                foreach (DataRow drSubCategory in arrSubCategory)
                {
                    parentCategory = parentCategory + HttpUtility.HtmlDecode(childCategoryStart.Replace("##CategoryName##", CCommon.ToString(drSubCategory["vcCategoryName"])).Replace("##CategoryID##", CCommon.ToString(drSubCategory["numCategoryID"])).Replace("##ITEMID##", id + "-" + parentID.ToString()));

                    DataRow[] arrChildCategory = dt.Select("Category=" + CCommon.ToString(drSubCategory["numCategoryID"]));

                    if (arrChildCategory != null && arrChildCategory.Length > 0)
                    {
                        LoadSubCategory(dt, drSubCategory, id + "-" + parentID.ToString(), ref parentCategory);
                    }

                    parentID = parentID + 1;
                    parentCategory = parentCategory + childCategoryEnd;
                }


                parentCategory = parentCategory + childContainerEnd;
            }

            return 0;
        }

        #region Sandeep - New Implementation

        //protected void LoadCategories1()
        //{
        //    CItems objItems = new CItems();
        //    DataTable dt = null;
        //    objItems.byteMode = 13;
        //    objItems.SiteID = Sites.ToLong(Session["SiteID"]);

        //    dt = objItems.SeleDelCategory();

        //    if (dt.Rows.Count > 0)
        //    {
        //        int parentID = 0;

        //        DataRow[] arrDataRow = dt.Select("category = 0");

        //        foreach (DataRow drCatgeory in arrDataRow)
        //        {
        //            string inputID = "";
        //            HtmlGenericControl li = new HtmlGenericControl("li");

        //            HtmlGenericControl label = new HtmlGenericControl("label");
        //            label.Attributes.Add("for", "item-" + parentID.ToString());

        //            string strLink = "";

        //            if (drCatgeory["Type"].ToString() == "0")
        //            {
        //                strLink = "/SubCategory/" + Sites.GenerateSEOFriendlyURL(drCatgeory["vcCategoryName"].ToString()) + "/" + drCatgeory["numCategoryID"].ToString();
        //            }
        //            else if (drCatgeory["Type"].ToString() == "1")
        //            {
        //                strLink = drCatgeory["vcLink"].ToString();
        //            }
        //            else if (drCatgeory["Type"].ToString() == "2")
        //            {
        //                strLink = "/Product.aspx?ItemID=" + drCatgeory["numCategoryID"].ToString();
        //            }

        //            HtmlGenericControl a = new HtmlGenericControl("a");
        //            a.Attributes.Add("onclick", "openLink('" + strLink + "')");
        //            a.Attributes.Add("href", "");
        //            a.InnerHtml = CCommon.ToString(drCatgeory["vcCategoryName"]);


        //            DataRow[] arrSubCategory = dt.Select("Category=" + CCommon.ToString(drCatgeory["numCategoryID"]));

        //            if (arrSubCategory.Length > 0)
        //            {
        //                HtmlGenericControl input = new HtmlGenericControl("input");
        //                input.Attributes.Add("type", "checkbox");
        //                input.Attributes.Add("id", "item-" + parentID.ToString());
        //                li.Controls.Add(input);
        //                inputID = "item-" + parentID.ToString();
        //                parentID = parentID + 1;

        //                label.Controls.Add(a);
        //                li.Controls.Add(label);

        //                LoadSubCategory(dt, drCatgeory, inputID, li);
        //            }
        //            else
        //            {
        //                parentID = parentID + 1;
        //                li.Controls.Add(a);

        //                //label.Controls.Add(a);
        //                //li.Controls.Add(label);
        //            }

        //            ul.Controls.Add(li);
        //        }
        //    }
        //}

        //public int LoadSubCategory1(DataTable dt, DataRow drParentCategory, string id, HtmlGenericControl control)
        //{
        //    DataRow[] arrSubCategory = dt.Select("Category=" + CCommon.ToString(drParentCategory["numCategoryID"]));

        //    if (arrSubCategory != null && arrSubCategory.Length > 0)
        //    {
        //        int parentID = 0;
        //        HtmlGenericControl ul = new HtmlGenericControl("ul");

        //        foreach (DataRow drSubCategory in arrSubCategory)
        //        {
        //            HtmlGenericControl li = new HtmlGenericControl("li");

        //            HtmlGenericControl label = new HtmlGenericControl("label");
        //            label.Attributes.Add("for", id + "-" + parentID.ToString());

        //            string strLink = "";

        //            if (drSubCategory["Type"].ToString() == "0")
        //            {
        //                strLink = "/SubCategory/" + Sites.GenerateSEOFriendlyURL(drSubCategory["vcCategoryName"].ToString()) + "/" + drSubCategory["numCategoryID"].ToString();
        //            }
        //            else if (drSubCategory["Type"].ToString() == "1")
        //            {
        //                strLink = drSubCategory["vcLink"].ToString();
        //            }
        //            else if (drSubCategory["Type"].ToString() == "2")
        //            {
        //                strLink = "/Product.aspx?ItemID=" + drSubCategory["numCategoryID"].ToString();
        //            }

        //            HtmlGenericControl a = new HtmlGenericControl("a");
        //            a.Attributes.Add("onclick", "openLink('" + strLink + "')");
        //            a.Attributes.Add("href", "");
        //            a.InnerText = CCommon.ToString(drSubCategory["vcCategoryName"]);

        //            DataRow[] arrChildCategory = dt.Select("Category=" + CCommon.ToString(drSubCategory["numCategoryID"]));

        //            if (arrChildCategory != null && arrChildCategory.Length > 0)
        //            {
        //                HtmlGenericControl input = new HtmlGenericControl("input");
        //                input.Attributes.Add("type", "checkbox");
        //                input.Attributes.Add("id", id + "-" + parentID.ToString());
        //                li.Controls.Add(input);
        //                id = id + "-" + parentID.ToString();
        //                parentID = parentID + 1;

        //                label.Controls.Add(a);
        //                li.Controls.Add(label);

        //                LoadSubCategory(dt, drSubCategory, id, li);
        //            }
        //            else
        //            {
        //                parentID = parentID + 1;
        //                li.Controls.Add(a);

        //                //label.Controls.Add(a);
        //                //li.Controls.Add(label);
        //            }

        //            ul.Controls.Add(li);
        //        }

        //        control.Controls.Add(ul);
        //    }

        //    return 0;
        //}

        #endregion

        #endregion

        //private void PopulateCategories()
        //{
        //    try
        //    {
        //        CItems objItems = new CItems();
        //        DataTable dt = default(DataTable);
        //        objItems.byteMode = 13;
        //        objItems.SiteID = Sites.ToLong(Session["SiteID"]);

        //        dt = objItems.SeleDelCategory();

        //        //if (HideOuterBox.Contains("True"))
        //        //{
        //        //    divHeader.Visible = false;
        //        //    divContent.Attributes.Remove("class");
        //        //}

        //        //Show UL LI based Single Level Tree
        //        if (Sites.ToBool(ShowOnlyParentCategoriesinTree) == true)
        //        {
        //            Panel pnl = new Panel();
        //            pnl.ID = "vertmenu";
        //            pnl.ClientIDMode = ClientIDMode.Static;
        //            HtmlGenericControl ul = new HtmlGenericControl("ul");
        //            ul.Attributes["class"] += "TreeNode";
        //            HyperLink hpCategory;
        //            HtmlGenericControl li;

        //            foreach (DataRow dr in dt.Rows)
        //            {
        //                hpCategory = new HyperLink();
        //                li = new HtmlGenericControl("li");

        //                hpCategory.Text = dr["vcCategoryName"].ToString();

        //                hpCategory.NavigateUrl = Session["SitePath"].ToString() + "/SubCategory/" + Sites.GenerateSEOFriendlyURL(dr["vcCategoryName"].ToString()) + "/" + dr["numCategoryID"].ToString(); ;

        //                li.Controls.Add(hpCategory);
        //                ul.Controls.Add(li);
        //            }
        //            pnl.Controls.Add(ul);
        //            phMenu.Controls.Add(pnl);
        //        }
        //        else // Show treeview
        //        {
        //            PopulateMenuitem(dt, TreeViewCategory.Nodes);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}
        //private void PopulateMenuitem(DataTable dt, TreeNodeCollection TreeViewnode)
        //{
        //    try
        //    {

        //        //Infragistics.WebUI.UltraWebNavigator.Node tnParent = default(Infragistics.WebUI.UltraWebNavigator.Node);
        //        foreach (DataRow dr in dt.Rows)
        //        {
        //            TreeNode tn = new TreeNode();
        //            tn.Text = dr["vcCategoryName"].ToString();
        //            tn.Value = dr["numCategoryID"].ToString(); // + (dr["Type"].ToString() != "2" ? "C" : "");

        //            if (dr["Type"].ToString() == "0")
        //            {
        //                //tn.NavigateUrl = "Category/" + Sites.GenerateSEOFriendlyURL(dr["vcCategoryName"].ToString()) + "/1/" + dr["numCategoryID"].ToString();//"ProductList.aspx?Page=1&Cat=" + dr["numCategoryID"].ToString();
        //                tn.NavigateUrl = "/SubCategory/" + Sites.GenerateSEOFriendlyURL(dr["vcCategoryName"].ToString()) + "/" + dr["numCategoryID"].ToString();//"ProductList.aspx?Page=1&Cat=" + dr["numCategoryID"].ToString();
        //            }
        //            else if (dr["Type"].ToString() == "1")
        //            {
        //                tn.NavigateUrl = dr["vcLink"].ToString();
        //            }
        //            else if (dr["Type"].ToString() == "2")
        //            {
        //                tn.NavigateUrl = "/Product.aspx?ItemID=" + dr["numCategoryID"].ToString();
        //            }


        //            if (Sites.ToBool(PreserveStateofTree) == true)
        //            {
        //                tn.Expanded = false;
        //                TreeViewCategory.TreeNodeExpanded += new TreeNodeEventHandler(TreeViewCategory_TreeNodeExpanded);
        //                TreeViewCategory.TreeNodeCollapsed += new TreeNodeEventHandler(TreeViewCategory_TreeNodeCollapsed);
        //            }
        //            else
        //                tn.Expanded = true;

        //            //Check if Category Cookie Exist, 
        //            AddCookieIfNotExist();
        //            //when Found then Set Node as expanded
        //            if (Request.Cookies.Get("CatID") != null)
        //                if (Request.Cookies.Get("CatID").Value.Contains(tn.Value))
        //                    tn.Expanded = true;


        //            if (dr["Type"].ToString() == "2")
        //            {
        //                if ((TreeViewCategory.FindNode(dr["Category"].ToString()) != null))
        //                {
        //                    TreeViewCategory.FindNode(dr["Category"].ToString()).ChildNodes.Add(tn);
        //                }
        //            }
        //            else
        //            {
        //                if (dr["Category"].ToString() == "0")
        //                {
        //                    TreeViewCategory.Nodes.Add(tn);
        //                }
        //                else
        //                {
        //                    AddSubCategory(TreeViewCategory.Nodes, tn, CCommon.ToString(dr["Category"]));
        //                }
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        //private void AddSubCategory(TreeNodeCollection treeNodeCollection, TreeNode treeNode, string Category)
        //{
        //    try
        //    {
        //        if (treeNodeCollection.Count > 0)
        //        {
        //            foreach (TreeNode item in treeNodeCollection)
        //            {
        //                if (item.Value == Category)
        //                {
        //                    item.ChildNodes.Add(treeNode);
        //                }
        //                else
        //                {
        //                    if (item.ChildNodes.Count > 0)
        //                    {
        //                        AddSubCategory(item.ChildNodes, treeNode, Category);
        //                    }
        //                }
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        //private void PopulateSubMenuLevel(int parentid, TreeNode parentNode)
        //{
        //    try
        //    {
        //        CItems objItems = new CItems();
        //        DataTable dt = default(DataTable);
        //        objItems.byteMode = 7;
        //        objItems.CategoryID = parentid;
        //        objItems.DomainID = Sites.ToLong(Request.Cookies["id_site"].Value);
        //        dt = objItems.SeleDelCategory();
        //        PopulateMenuitem(dt, parentNode.ChildNodes);
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        //private void AddCookieIfNotExist()
        //{
        //    if (Request.Cookies["CatID"] == null)
        //    {
        //        HttpCookie aCookie = new HttpCookie("CatID", "");
        //        aCookie.Expires = DateTime.Now.AddDays(1);
        //        Response.Cookies.Add(aCookie);
        //    }
        //}

        #endregion
    }
}