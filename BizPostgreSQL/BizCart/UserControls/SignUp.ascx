﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SignUp.ascx.cs" Inherits="BizCart.UserControls.SignUp" ClientIDMode="Static" %>

<script type="text/javascript" src="../../Js/Common.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDKZ2Kph5NQXTnPc1xEzM4Bq0nhzXjONS0&libraries=places&signed_in=true" type="text/javascript"></script>
<script type="text/javascript" src="../Js/GooglePlace.js"></script>
<script src="../Js/smartystreetsAPI/VerifyAddress.js"></script>
<style>
      .textDesign {
    display: inline;
    padding: .2em .6em .3em;
    font-size: 13px;
    font-weight: 500;
    line-height: 1;
    color: #fff;
    text-align: center;
    white-space: nowrap;
    vertical-align: baseline;
    border-radius: .25em;
}
        .text-default {
    background-color: #0278de4f;
    color: #000;
}.text-success {
    background-color: #26d22652;
    color: #000;
}.text-danger {
    background-color: #cc201b99;
    color: #000;
}
</style>
<script type="text/javascript">
    function RetrievePassword() {
        $("[id$=btnRetrieve]").click();
    }
    function VerifyAddress(type) {
        //var smartystreetsKey = "3359218178746496";
        var smartystreetsKey = document.getElementById("hdnSmartyStreetsAPIKeys").value;
        var selectBillCountryoption = document.getElementById("ddlContactCountry");
        var selectBillStateoption = document.getElementById("ddlContactState");

        getVerifyAddress(selectBillCountryoption.options[selectBillCountryoption.selectedIndex].text, document.getElementById("txtContactStreet").value, document.getElementById("txtContactPostalCode").value,document.getElementById("txtContactCity").value, selectBillStateoption.options[selectBillStateoption.selectedIndex].text, type, smartystreetsKey);

    }
    //$(document).ready(function () {
    //    initializebilladdress("txtContactStreet", "txtContactStreet", "txtContactCity", "hdnContactState", "txtContactPostalCode", "ddlContactCountry");
    //    initializebilladdress("txtBillStreet", "txtBillStreet", "txtBillCity", "hdnBillToState", "txtBillPostCode", "ddlBillCountry");
    //    initializeshipaddress("txtShipToStreet", "txtShipToStreet", "txtShipToCity", "hdnShipToState", "txtShiptoPostalCode", "ddlShipCountry");
    //});
</script>
<asp:Panel ID="pnlCutomizeHtml" runat="server">
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td>
                <div class="sectionheader">
                    <asp:Label ID="lblTitle" runat="server" Text="Signup"></asp:Label>
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <asp:PlaceHolder ID="plhFormControls" runat="server"></asp:PlaceHolder>
            </td>
        </tr>
        <tr>
            <td>
                <table width="100%" border="0" cellpadding="0" cellspacing="0" align="center">
                    <tr>
                        <td class="LabelColumn">
                            First Name
                        </td>
                        <td class="ControlCell">
                            <asp:TextBox ID="txtFirstName" runat="server" MaxLength="50" CssClass="textbox" ></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="LabelColumn">
                            Last Name
                        </td>
                        <td class="ControlCell">
                            <asp:TextBox ID="txtLastName" runat="server" MaxLength="50" CssClass="textbox" ></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="LabelColumn">
                            Contact Email
                        </td>
                        <td class="ControlCell">
                            <asp:TextBox ID="txtEmail" runat="server" MaxLength="50" CssClass="textbox" ></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="LabelColumn">
                            Ship To State
                        </td>
                        <td class="ControlCell">
                            <asp:DropDownList ID="ddlShipState" runat="server" CssClass="dropdown">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="LabelColumn">
                            Bill To City
                        </td>
                        <td class="ControlCell">
                            <asp:TextBox ID="txtBillCity" runat="server" MaxLength="50" CssClass="textbox" ></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="LabelColumn">
                            Bill To Country
                        </td>
                        <td class="ControlCell">
                            <asp:DropDownList ID="ddlBillCountry" runat="server" CssClass="dropdown">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="LabelColumn">
                            Bill to Postal Code
                        </td>
                        <td class="ControlCell">
                            <asp:TextBox ID="txtBillPostCode" runat="server" MaxLength="50" CssClass="textbox"
                                ></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="LabelColumn">
                            Bill To State
                        </td>
                        <td class="ControlCell">
                            <asp:DropDownList ID="ddlBillState" runat="server" CssClass="dropdown">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="LabelColumn">
                            Bill To Street
                        </td>
                        <td class="ControlCell">
                            <asp:TextBox ID="txtBillStreet" runat="server" MaxLength="50" CssClass="textbox"
                                ></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="LabelColumn">
                            Contact Category
                        </td>
                        <td class="ControlCell">
                            <asp:DropDownList ID="ddlContactCategory" runat="server" CssClass="dropdown">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="LabelColumn">
                            Contact Cell Phone
                        </td>
                        <td class="ControlCell">
                            <asp:TextBox ID="txtContactCellPhone" runat="server" MaxLength="50" CssClass="textbox"
                                ></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="LabelColumn">
                            Contact City
                        </td>
                        <td class="ControlCell">
                            <asp:TextBox ID="txtContactCity" runat="server" MaxLength="50" CssClass="textbox"
                                ></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="LabelColumn">
                            Contact Country
                        </td>
                        <td class="ControlCell">
                            <asp:DropDownList ID="ddlContactCountry" runat="server" CssClass="dropdown">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="LabelColumn">
                            Contact Fax
                        </td>
                        <td class="ControlCell">
                            <asp:TextBox ID="txtContactFax" runat="server" MaxLength="50" CssClass="textbox"
                                ></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="LabelColumn">
                            Contact Home Phone
                        </td>
                        <td class="ControlCell">
                            <asp:TextBox ID="txtContactHomePhone" runat="server" MaxLength="50" CssClass="textbox"
                                ></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="LabelColumn">
                            Contact Phone
                        </td>
                        <td class="ControlCell">
                            <asp:TextBox ID="txtContactPhone" runat="server" MaxLength="50" CssClass="textbox"
                                ></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="LabelColumn">
                            Contact Phone Ext
                        </td>
                        <td class="ControlCell">
                            <asp:TextBox ID="txtContactPhoneExt" runat="server" MaxLength="50" CssClass="textbox"
                                ></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="LabelColumn">
                            Contact Position
                        </td>
                        <td class="ControlCell">
                            <asp:DropDownList ID="ddlConatctPosition" runat="server" CssClass="dropdown">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="LabelColumn">
                            Contact Postal Code
                        </td>
                        <td class="ControlCell">
                            <asp:TextBox ID="txtContactPostalCode" runat="server" MaxLength="50" CssClass="textbox"
                                ></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="LabelColumn">
                            Contact State
                        </td>
                        <td class="ControlCell">
                            <asp:DropDownList ID="ddlContactState" runat="server" CssClass="dropdown">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="LabelColumn">
                            Contact Status
                        </td>
                        <td class="ControlCell">
                            <asp:DropDownList ID="ddlContactStatus" runat="server" CssClass="dropdown">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="LabelColumn">
                            Contact Street
                        </td>
                        <td class="ControlCell">
                            <asp:TextBox ID="txtContactStreet" runat="server" MaxLength="50" CssClass="textbox"
                                ></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="LabelColumn">
                            Contact Team
                        </td>
                        <td class="ControlCell">
                            <asp:DropDownList ID="ddlContactTeam" runat="server" CssClass="dropdown">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="LabelColumn">
                            Department Name
                        </td>
                        <td class="ControlCell">
                            <asp:DropDownList ID="ddlDepartmentName" runat="server" CssClass="dropdown">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="LabelColumn">
                            Follow-Up Status
                        </td>
                        <td class="ControlCell">
                            <asp:DropDownList ID="ddlFollowUpStatus" runat="server" CssClass="dropdown">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="LabelColumn">
                            Industry
                        </td>
                        <td class="ControlCell">
                            <asp:DropDownList ID="ddlIndustry" runat="server" CssClass="dropdown">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="LabelColumn">
                            How did you hear about us?
                        </td>
                        <td class="ControlCell">
                            <asp:DropDownList ID="ddlInfoSource" runat="server" CssClass="dropdown">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="LabelColumn">
                            No of Employees
                        </td>
                        <td class="ControlCell">
                            <asp:DropDownList ID="ddlNoOfEmployees" runat="server" CssClass="dropdown">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="LabelColumn">
                            Organization Annual Revenue
                        </td>
                        <td class="ControlCell">
                            <asp:DropDownList ID="ddlOrganizationAnnualRevenue" runat="server" CssClass="dropdown">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="LabelColumn">
                            Organization Campaign
                        </td>
                        <td class="ControlCell">
                            <asp:DropDownList ID="ddlOrganizationCampaign" runat="server" CssClass="dropdown">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="LabelColumn">
                            Organization Comments
                        </td>
                        <td class="ControlCell">
                            <asp:TextBox ID="txtOrganizationComments" runat="server" MaxLength="100" CssClass="textbox"
                                ></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="LabelColumn">
                            Organization Credit Limit
                        </td>
                        <td class="ControlCell">
                            <asp:DropDownList ID="ddlOrganizationCreditLimit" runat="server" CssClass="dropdown">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="LabelColumn">
                            Organization Fax
                        </td>
                        <td class="ControlCell">
                            <asp:TextBox ID="txtOrganizationFax" runat="server" MaxLength="50" CssClass="textbox"
                                ></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="LabelColumn">
                            Organization Name
                        </td>
                        <td class="ControlCell">
                            <asp:TextBox ID="txtOrganizationName" runat="server" MaxLength="50" CssClass="textbox"
                                ></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="LabelColumn">
                            Organization Phone
                        </td>
                        <td class="ControlCell">
                            <asp:TextBox ID="txtOrganizationPhone" runat="server" MaxLength="50" CssClass="textbox"
                                ></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="LabelColumn">
                            Organization Profile
                        </td>
                        <td class="ControlCell">
                            <asp:DropDownList ID="ddlOrganizationProfile" runat="server" CssClass="dropdown">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="LabelColumn">
                            Organization Rating
                        </td>
                        <td class="ControlCell">
                            <asp:DropDownList ID="ddlOrganizationRating" runat="server" CssClass="dropdown">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="LabelColumn">
                            Organization Status
                        </td>
                        <td class="ControlCell">
                            <asp:DropDownList ID="ddlOrganizationStatus" runat="server" CssClass="dropdown">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="LabelColumn">
                            Organization Website
                        </td>
                        <td class="ControlCell">
                            <asp:TextBox ID="txtOrganizationWebsite" runat="server" MaxLength="50" CssClass="textbox"
                                ></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="LabelColumn">
                            Relationship
                        </td>
                        <td class="ControlCell">
                            <asp:DropDownList ID="ddlRelationship" runat="server" CssClass="dropdown">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="LabelColumn">
                            Ship To City
                        </td>
                        <td class="ControlCell">
                            <asp:TextBox ID="txtShipToCity" runat="server" MaxLength="50" CssClass="textbox"
                                ></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="LabelColumn">
                            Ship To Country
                        </td>
                        <td class="ControlCell">
                            <asp:DropDownList ID="ddlShipCountry" runat="server" CssClass="dropdown">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="LabelColumn">
                            Ship to Postal Code
                        </td>
                        <td class="ControlCell">
                            <asp:TextBox ID="txtShiptoPostalCode" runat="server" MaxLength="50" CssClass="textbox"
                                ></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="LabelColumn">
                            Ship To Street
                        </td>
                        <td class="ControlCell">
                            <asp:TextBox ID="txtShipToStreet" runat="server" MaxLength="50" CssClass="textbox"
                                ></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="LabelColumn">
                            Shipping Address same as Billing Address
                        </td>
                        <td class="ControlCell">
                            <asp:CheckBox ID="chkShippingAddresssameasBillingAddress" runat="server" CssClass="text" />
                        </td>
                    </tr>
                    <tr>
                        <td class="LabelColumn">
                            Territory
                        </td>
                        <td class="ControlCell">
                            <asp:DropDownList ID="ddlTerritory" runat="server" CssClass="dropdown">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="LabelColumn">
                            Password
                        </td>
                        <td class="ControlCell">
                            <asp:TextBox ID="txtPassword" runat="server" MaxLength="20" CssClass="textbox"
                                 TextMode="Password"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="LabelColumn">
                          Password Confirm
                        </td>
                        <td class="ControlCell">
                            <asp:TextBox ID="txtPasswordConfirm" runat="server" MaxLength="20" CssClass="textbox"
                                 TextMode="Password"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="LabelColumn">Password for future orders (Optional)</td>
                        <td class="ControlCell"><asp:TextBox ID="txtFuturePassword" runat="server" MaxLength="20" CssClass="textbox"
                                 TextMode="Password"></asp:TextBox></td>
                    </tr>
                     <tr>
                        <td class="LabelColumn">
                            Subscribe Newsletter
                        </td>
                        <td class="ControlCell">
                            <asp:CheckBox ID="chkbitOptOut" runat="server" CssClass="text" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                
                <asp:Button ID="btnSubmit" Text="Submit" runat="server" CssClass="button" OnClick="btnSubmit_Click"
                    CausesValidation="false" OnClientClick="return ValidateInput();"></asp:Button>
                <asp:HyperLink ID="hplLogin" runat="server">Login</asp:HyperLink>
            </td>
        </tr>
    </table>
</asp:Panel>
<asp:Literal ID="litCutomizeHtml" runat="server"></asp:Literal>
<%--<asp:ValidationSummary ID="ValidationSummary" runat="server" HeaderText="Please check the following value(s)"
    ShowSummary="false" ShowMessageBox="True" DisplayMode="List"></asp:ValidationSummary>--%>
<input id="hdXMLString" type="hidden" name="hdXMLString" runat="server">
<input id="hdGroupId" type="hidden" name="hdGroupId" runat="server"><!---Store the Group ID--->
<input id="hdEmail" type="hidden" name="hdEmail" runat="server"><!---Store the Email Id--->
<input id="hdCompanyType" type="hidden" name="hdCompanyType" runat="server"><!---Store the Company Type --->
<input id="hdnBillToState" type="hidden" name="hdnBillToState" runat="server"><!---Store the Company Type --->
<input id="hdnShipToState" type="hidden" name="hdnShipToState" runat="server"><!---Store the Company Type --->
<input id="hdnContactState" type="hidden" name="hdnContactState" runat="server" />
<asp:Literal ID="litClientScript" runat="server"></asp:Literal>
<asp:Button ID="btnRetrieve" runat="server" Text="" style="display:none" OnClick="btnRetrieve_Click" />
 <asp:HiddenField ID="hdnSmartyStreetsAPIKeys" runat="server" />
<asp:HiddenField ID="hdnIsSmartyStreetsEnabled" runat="server" />