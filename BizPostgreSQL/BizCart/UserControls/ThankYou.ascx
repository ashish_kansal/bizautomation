﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ThankYou.ascx.cs" Inherits="BizCart.UserControls.ThankYou" %>
<style>
    .overlaypop {
    position: fixed;
    z-index: 121;
    top: 0px;
    left: 0px;
    right: 0px;
    bottom: 0px;
    background-color: rgba(170, 170, 170, 0.5);
    filter: alpha(opacity=80);
}
    .overlaycontentpop
    {
    color: #000;
    background-color: #FFF;
    min-width: 635px;
    max-height:300px;
    overflow-y:scroll;
    padding: 10px;
    border: 2px solid black;
    top: 20%;
    left: 30%;
    position: absolute;
    max-width: 100%;
}
</style>
<script type="text/javascript">
    function OpenBizInvoice(a, b , c) {
        //Apply changes in code behind aswell
        window.open('Invoice.aspx?Show=True&OppID=' + a + '&RefType='+ b +'&Print=' + c  , '', 'toolbar=no,menubar=yes,titlebar=no,left=100,width=850,height=800,scrollbars=yes,resizable=yes');
    }

    function AddToCartPostSell(numItemCode, fltDiscount, PromotionID, PromotionDesc) {
        var z = $('#txtUnits_' + numItemCode).val();
        __doPostBack('AddToCart', numItemCode + ',' + z + ',' + fltDiscount + ',' + PromotionID + ',' + PromotionDesc)
    }
</script>

<asp:Panel ID="pnlCutomizeHtml" runat="server">
  
</asp:Panel>
<asp:Literal ID="litCutomizeHtml" runat="server"></asp:Literal>