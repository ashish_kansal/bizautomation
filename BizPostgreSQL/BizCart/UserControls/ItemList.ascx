﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ItemList.ascx.cs" Inherits="BizCart.UserControls.ItemList" ClientIDMode="Static" %>
<%@ Register Assembly="AspNetPager, Version=7.4.4.0, Culture=neutral, PublicKeyToken=fb0a0fe055d40fd4" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>
<%@ Import Namespace="BACRM.BusinessLogic.Common" %>

<script type="text/javascript">
    function openOrderSummary() {
        window.location.replace("/OrderSummary.aspx");
        return false;
    }
</script>
<asp:panel id="pnlCutomizeHtml" runat="server" >
    <table border="0" width="100%">
        <tr>
            <td colspan="3">
                <div class="sectionheader">
                    <asp:Label ID="lblManufacturerName" runat="server" Text="" EnableViewState="false"></asp:Label>
                    <asp:Label ID="lblCategoryName" runat="server" Text="" EnableViewState="false"></asp:Label>
                </div>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <table width="100%">
                    <tr>
                        <td></td>
                        <td width="50" align="right">Sort by</td>
                        <td align="left" width="150" style="vertical-align: bottom">
                            <asp:DropDownList ID="ddlSortBy" runat="server" AutoPostBack="True" CssClass="dropdown" OnSelectedIndexChanged="ddlSortBy_SelectedIndexChanged"></asp:DropDownList>
                        </td>
                        <td align="right" width="100">Items per page
                        </td>
                        <td align="left" width="50">
                            <asp:DropDownList ID="ddlPageSize" runat="server" AutoPostBack="True" CssClass="dropdown" OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged"></asp:DropDownList>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="right" align="left">
                <asp:Label ID="lblItemCount" runat="server" Text="Item(s): 1-15 of 200" EnableViewState="false"></asp:Label>
            </td>
            <td align="right">
                <webdiyer:aspnetpager id="AspNetPager1" runat="server"
                    pagingbuttonspacing="0"
                    pagingbuttonlayouttype="UnorderedList"
                    currentpagebuttonclass="active"
                    firstpagetext="First"
                    lastpagetext="Last"
                    nextpagetext="Next"
                    prevpagetext="Prev"
                    onpagechanged="AspNetPager1_PageChanged"
                    width="100%"
                    showpageindexbox="Never">
                </webdiyer:aspnetpager>
            </td>
            <td>&nbsp;</td>
        </tr>
    </table>
</asp:panel>
<asp:literal id="litCutomizeHtml" runat="server" EnableViewState="false"></asp:literal>

<asp:HiddenField ID="hdnSelectedProductFilters" runat="server" />
<asp:Button ID="btnProductFiltersChanged" runat="server" style="display:none" OnClick="btnProductFiltersChanged_Click" />
<asp:hiddenfield id="hdnPath" runat="server" EnableViewState="false" />
<asp:textbox id="hdnSQty" cssclass="hdnSQty" style="display: none;" value="1" runat="server" EnableViewState="false"></asp:textbox>

<div id="divMiniCartPreSell" class="modal fade modal-minicart" role="dialog">

    <div class="modal-dialog modal-dialog-minicart" style="width: 70%">
        <!-- Modal content-->
        <div class="modal-content modal-content-minicart">
            <div id="dialogSellPopup" runat="server" visible="false" enableviewstate="false">
                <div>
                    <div class="modal-header modal-content-minicart">
                        <div class="pull-left">
                            <h4 class="modal-title modal-title-minicart" style="font-weight: bold;">Add to your order   
                            </h4>
                        </div>
                        <%-- <div style="float: left; padding-left: 50px; line-height: 1; display: inline;" id="divPromotionDetailRI" visible="false" runat="server">
                            <img src="/Images/Admin-Price-Control.png" height="30" alt="" />
                            <asp:Label ID="Label1" Text="Based on your selection you might want some of the following items, which also qualify for a promotion!" runat="server"></asp:Label>
                        </div>--%>
                        <div id="divPromotionDesription" runat="server" enableviewstate="false" visible="false" style="float: left; padding-left: 50px; line-height: 1; display: inline;">
                            <div style="float: left; padding-right: 5px">
                                <img src="/Images/Admin-Price-Control.png" height="30" alt="" />
                                <%--<asp:Label ID="lblPromotionDescription" runat="server" Text=""></asp:Label>--%>
                                <asp:label id="Label1" text="Based on your selection you might want some of the following items, which also qualify for a promotion!" runat="server" EnableViewState="false"></asp:label>
                            </div>
                            <%-- <div>
                                    <b>Promotion:</b><asp:Label ID="lblPromotionName" runat="server" Text="">
                                    </asp:Label><asp:Label ID="lblExpiration" runat="server" Text=""></asp:Label><br />
                                    <b>Details:</b><asp:Label ID="lblPromotionDescription" runat="server" Text=""></asp:Label><br />

                                </div>--%>
                        </div>
                    </div>
                    <div class="modal-body modal-body-minicart">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="table-responsive">
                                    <asp:gridview id="gvRelatedItems" EnableViewState="false" cssclass="table table-bordered table-striped" datakeynames="numItemCode,numParentItemCode" runat="server" autogeneratecolumns="false" width="100%" cellpadding="3" cellspacing="3" bordercolor="#f0f0f0" onrowcommand="gvRelatedItems_RowCommand" onrowdatabound="gvRelatedItems_RowDataBound">
                                        <RowStyle BorderColor="#CACACA" BorderStyle="Solid" BorderWidth="1" />
                                        <HeaderStyle Font-Bold="true" Font-Size="10" BackColor="#e8e8e8" BorderColor="#CACACA" BorderStyle="Solid" BorderWidth="1" />
                                        <Columns>
                                           <%-- <asp:TemplateField>
                                                <ItemTemplate>
                                                    <%#CCommon.GetImageHTML(Eval("vcPathForTImage"), 1, 100, 56)%>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Center" Width="100px" Height="56px" />
                                            </asp:TemplateField>--%>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:Image ID="imgPath" runat="server" EnableViewState="false" ImageUrl='<%# Eval("vcPathForTImage")%>' alt="" style="width:80px;" class="img-responsive"/>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Center" />
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="vcItemName" HeaderText="Item" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Left" ItemStyle-Wrap="false"/>
                                            <asp:BoundField DataField="txtItemDesc" HeaderText="Description" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Left" ItemStyle-Wrap="true"/>
                                            <asp:BoundField DataField="vcRelationship" HeaderText="Relation to selected item(s)" ItemStyle-HorizontalAlign="Left" HeaderStyle-Wrap="true" HeaderStyle-CssClass="text-center"/>
                                            <asp:TemplateField HeaderText="Qty" ItemStyle-HorizontalAlign="Left" ItemStyle-Wrap="false" HeaderStyle-CssClass="text-center">
                                                <ItemTemplate>
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <asp:TextBox ID="txtQty" runat="server" EnableViewState="false" Width="50" Text="1"></asp:TextBox></td>
                                                            <td>/ </td>
                                                            <td><%# Eval("vcUOMName")%></td>
                                                        </tr>
                                                    </table>
                                                    <asp:HiddenField ID="hdnWarehouseID" runat="server" EnableViewState="false" Value='<%# Eval("numWareHouseItemID")%>' />
                                                    <asp:HiddenField ID="hdnPrice" runat="server" EnableViewState="false" Value='<%# Eval("monListPrice")%>' />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="monListPrice" HeaderText="Unit Price" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center" DataFormatString="USD {0:#,##0.00###}" ItemStyle-Width="150px"/>
                                            <asp:TemplateField HeaderText="Total" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center" ItemStyle-Wrap="false">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblTotal" runat="server" EnableViewState="false" Width="100" Text="1"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField ItemStyle-HorizontalAlign="Center" ItemStyle-Width="25">
                                                <ItemTemplate>
                                                    <asp:Button ID="btnAdd" CommandName="Add" class="btn btn-primary" Text="Add" runat="server" EnableViewState="false" OnClientClick="Confirm()" CommandArgument='<%# Eval("numItemCode") %>'></asp:Button>
                                                    <asp:Label ID="lblOutOfStock" runat="server" EnableViewState="false" Width="100" Text="Out of stock" Visible="false"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="numOnHand" Visible="false"/>
                                            <asp:BoundField DataField="numItemCode" Visible="false" />
                                        </Columns>
                                    </asp:gridview>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <asp:hiddenfield id="hdnCategory" runat="server" EnableViewState="false" />
            </div>
            <div class="row">
                <div class="pull-right" style="padding-right: 20px;" id="divShipping" runat="server" enableviewstate="false" visible="false">
                    <img src="/Images/TruckGreen.png" height="30" alt="" />
                    <asp:label id="lblShipping" runat="server" EnableViewState="false"></asp:label>
                    <%--You qualify for free shipping !--%>
                </div>
            </div>
            <div class="row">
                <div class="pull-right" style="font-weight: bold; padding-right: 30px;">
                    <asp:label id="lblSubTotal" runat="server" EnableViewState="false"></asp:label>
                </div>
            </div>
            <div class="modal-header modal-content-minicart">
                <div class="pull-left">
                    <h4 class="modal-title modal-title-minicart" style="font-weight: bold;">
                        <img id="imgShopping" src="/Images/shopping_cart.gif" align="absmiddle" style="border-width: 0px;">
                        Your Cart
                    </h4>
                </div>
                <div style="float: left; padding-left: 100px; line-height: 1; display: inline;" id="divPromotion" runat="server" EnableViewState="false" visible="false">
                    <img src="/Images/Admin-Price-Control.png" height="30" alt="" />
                    <asp:label id="lblVcPromotion" runat="server" EnableViewState="false"></asp:label>
                </div>
                <div class="pull-right">
                    <a id="hplCart" class="btn btn-primary" href="/Cart.aspx">View Cart</a>
                    <asp:linkbutton id="hplCheckout" class="btn btn-primary" runat="server" EnableViewState="false" text="Check Out" onclientclick="openOrderSummary()" />
                    <%--<asp:LinkButton ID="hplCheckout" class="btn btn-primary" runat="server" OnClick="hplCheckout_Click" Text="Check Out"></asp:LinkButton>--%>
                    <button type="button" data-dismiss="modal" class="btn btn-primary">Close</button>
                </div>
            </div>
            <div class="modal-body modal-body-minicart">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="table-responsive">
                            <%-- ShowFooter="true" OnRowDataBound="gvMiniCart_RowDataBound" --%>
                            <asp:gridview id="gvMiniCart" cssclass="table table-bordered table-striped" onrowdatabound="gvMiniCart_RowDataBound" runat="server" autogeneratecolumns="false" width="100%" cellpadding="3" cellspacing="3" bordercolor="#f0f0f0">
                                <RowStyle BorderColor="#CACACA" BorderStyle="Solid" BorderWidth="1" />
                                <HeaderStyle HorizontalAlign="Center" Font-Bold="true" Font-Size="10" BackColor="#e8e8e8" BorderColor="#CACACA" BorderStyle="Solid" BorderWidth="1" />
                                <Columns>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:Image ID="imgPath" runat="server" EnableViewState="false" ImageUrl='<%# Eval("vcPathForTImage")%>' alt="" style="width:80px;" class="img-responsive"/>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Item" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center" ItemStyle-Width="300px">
                                        <ItemTemplate>
                                            <a href="<%# Eval("ItemURL")%>"><%# Eval("vcItemName")%></a>
                                            <%# Eval("vcAttributes")%>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="txtItemDesc" HeaderText="Description" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Left" ItemStyle-Wrap="true"/>
                                    <asp:BoundField DataField="numUnitHour" HeaderText="Qty" ItemStyle-Width="50" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center" />
                                    <asp:BoundField DataField="monPrice" HeaderText="Unit Price" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center" DataFormatString="USD {0:#,##0.00###}" />
                                    <asp:BoundField DataField="monTotAmount" HeaderText="Total"  ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center" DataFormatString="USD {0:#,##0.00###}" />
                                </Columns>
                            </asp:gridview>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">

    document.getElementById('hdnPath').value = window.location;
    function removeQty(x) {
        var x = '#' + x;
        var qty = $(x).val();
        if (parseInt(qty) > 1) {
            qty = parseInt(qty) - 1;
            $(x).val(qty);
        }
    }
    function addQty(x) {
        var x = '#' + x;
        var qty = $(x).val();
        qty = parseInt(qty) + 1;
        $(x).val(qty);
    }
    function addtocartmngqty(x, y) {
        var z = '#' + y;
        $(".hdnSQty").val($(z).val());
        __doPostBack('AddToCart', y)
    }

    <%--Added by Neelam on 10/11/2017 - Added function to open minicart modal pop up--%>
    function ShowMiniPreSellCart() {
        $("#divMiniCartPreSell").modal('show');
    }

    function AddItemToCart(itemCode, txtQtyID) {
        var qty = 1;

        $("[id='" + txtQtyID + "']").each(function () {
            if ($(this).is(":visible") && parseFloat($(this).val().replace(",", "")) > 0) {
                qty = parseFloat($(this).val().replace(",", ""));
            }
        });

        $("[id$=hdnSQty]").val(qty);

        __doPostBack('AddToCart', itemCode);
    }

    function SelectAllRecords(chkSelectAll) {
        if ($(chkSelectAll).is(":checked")) {
            $("input.chkSelect").prop("checked", true);
        } else {
            $("input.chkSelect").prop("checked", false);
        }
    }

    function AddToCartSelected() {
        try {
            var selectedRecords = [];

            $("input.chkSelect:checked").each(function () {
                var rowIndex = parseInt($(this).attr("id").split("~")[0]);
                var itemCode = parseInt($(this).attr("id").split("~")[1]);
                var quantity = 1;

                $("[id='txtQuantity" + rowIndex + "']").each(function () {
                    if ($(this).is(":visible") && parseFloat($(this).val().replace(",", "")) > 0) {
                        quantity = parseFloat($(this).val().replace(",", ""));
                    }
                });

                var record = {};
                record.ItemCode = itemCode;
                record.Quantity = quantity;
                selectedRecords.push(record);
            });

            if (selectedRecords.length > 0) {
                $.ajax({
                    type: "POST",
                    url: '/WebServices/CommonService.svc/AddToCart',
                    contentType: "application/json",
                    dataType: "json",
                    data: JSON.stringify({
                        "items": JSON.stringify(selectedRecords)
                    }),
                    beforeSend: function () {
                        if ($("[id$=UpdateProgress]") != null) {
                            $("[id$=UpdateProgress]").show();
                        }
                    },
                    complete: function () {
                        if ($("[id$=UpdateProgress]") != null) {
                            $("[id$=UpdateProgress]").hide();
                        }
                    },
                    success: function (data) {
                        var objResult = $.parseJSON(data.AddToCartResult);

                        if (!objResult.IsSuccess) {
                            alert("Error occurred while adding some items to cart.");
                        } else {
                            document.location.href = document.location.href;
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        alert("Error occurred while adding items to cart.");
                    }
                });
            } else {
                alert("Select at least one item.");
            }
        } catch (e) {
            alert("Error occurred while adding selected items to cart.");
        }
    }

    function ItemQuantityChanged(txtQuantity,itemCode,uomConversionRate) {
        try {
            var quantity = parseInt($(txtQuantity).val());

            $.ajax({
                type: "POST",
                url: '/WebServices/CommonService.svc/GetItemPrice',
                contentType: "application/json",
                dataType: "json",
                data: JSON.stringify({
                    "itemCode": itemCode
                    , "quantity": ((quantity || 1) <= 0 ? 1 : (quantity || 1))
                    , "uomConversionRate": uomConversionRate
                }),
                beforeSend: function () {
                    if ($("[id$=UpdateProgress]") != null) {
                        $("[id$=UpdateProgress]").show();
                    }
                },
                complete: function () {
                    if ($("[id$=UpdateProgress]") != null) {
                        $("[id$=UpdateProgress]").hide();
                    }
                },
                success: function (data) {
                    var objResult = $.parseJSON(data.GetItemPriceResult);

                    if (objResult.IsOfferPriceAvailable) {
                        $("#lblProductPrice" + itemCode).removeClass("prod_price").addClass("prod_strick");
                        $("#lblOfferPrice" + itemCode).html(objResult.OfferPrice);
                        $("#lblOfferPrice" + itemCode).addClass("prod_price");
                        $("#lblOfferPrice" + itemCode).show();
                    } else {
                        $("#lblProductPrice" + itemCode).removeClass("prod_strick").addClass("prod_price");
                        $("#lblOfferPrice" + itemCode).html("");
                        $("#lblOfferPrice" + itemCode).removeClass("prod_price");
                        $("#lblOfferPrice" + itemCode).hide();
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {

                }
            });
        } catch (e) {

        }
    }
</script>
