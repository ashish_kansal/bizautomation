﻿using BACRM.BusinessLogic.Accounting;
using BACRM.BusinessLogic.Admin;
using BACRM.BusinessLogic.Alerts;
using BACRM.BusinessLogic.Common;
using BACRM.BusinessLogic.Contacts;
using BACRM.BusinessLogic.Documents;
using BACRM.BusinessLogic.Item;
using BACRM.BusinessLogic.Opportunities;
using BACRM.BusinessLogic.ShioppingCart;
using BACRM.BusinessLogic.Workflow;
using nsoftware.InPayPal;
using System;
using System.Collections;
using System.Data;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Linq;
using BACRM.BusinessLogic.Promotion;

namespace BizCart.UserControls
{
    public partial class GuestCheckOut : BizUserControl
    {
        #region Global Declaration

        long lngDivID = 0;
        long lngContId = 0;
        long lngOppBizDocID = 0;
        long lngCardTypeID = 0;
        long lngTransactionHistoryID = 0;
        long UndepositedFundAccountID = 0;
        string strQueryString = "";
        
        OppotunitiesIP objOpportunity = new OppotunitiesIP();
        PageControls objPageControls = new PageControls();
        CCommon objCommon = new CCommon();
        DataTable dtCustFld = default(DataTable);
        Boolean IsGoogleCheckoutConfigured = true;
        Boolean IsPaypalConfigured = true;
        string PaypalServerUrl = "";
        string PaypalRedirectServerUrl = "";
        Expresscheckout expresscheckout1 = new Expresscheckout();
        public string AfterAddingItemShowCartPage { get; set; }
        decimal dcTotalTax = 0;

        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                #region Check for Execution

                Sites.InitialiseSession();

                if (!IsPostBack)
                {
                    if (CCommon.ToLong(Session["OrderPromotionID"]) > 0 && CCommon.ToLong(Session["DiscountCodeID"]) > 0 && !string.IsNullOrEmpty(CCommon.ToString(Session["CouponCode"])))
                    {
                        hdnOrderPromotionID.Value = CCommon.ToString(Session["OrderPromotionID"]);
                        hdnDiscountCodeID.Value = CCommon.ToString(Session["DiscountCodeID"]);
                        lblCouponCode.Text = CCommon.ToString(Session["CouponCode"]);
                    }

                    Session["CouponCode"] = null;
                    Session["OrderPromotionID"] = null;
                    Session["DiscountCodeID"] = null;

                    AddOrderPromotionDiscountIfAvailable();

                    hdnIsFinishedPostCheckout.Value = "False";
                    if (Sites.ToInteger(Session["BaseTaxOn"]) == 1)
                    {
                        hdnTaxBasedOn.Value = "1";
                    }
                    else
                    {
                        hdnTaxBasedOn.Value = "2";
                    }

                    Stream RequestStream = Request.InputStream;

                    //Relationship Mapping Validation
                    OppBizDocs objOpp = new OppBizDocs();
                    if (objOpp.ValidateARAP(0, Sites.ToLong(Session["DefaultRelationship"]), Sites.ToLong(Session["DomainID"])) == false)
                    {
                        Page.MaintainScrollPositionOnPostBack = false;
                        ShowMessage(GetErrorMessage("ERR014"), 1);//Please Set AR and AP Relationship from Administration->Master List Admin-> Accounting/RelationShip Mapping To Save
                        return;
                    }

                    #region
                    txtCardNumber.Attributes.Add("onkeypress", "return CheckNumber(2,event);");

                    int i = 0;
                    ArrayList Years = new ArrayList();
                    for (i = 1; i <= 20; i++)
                    {
                        Years.Add(DateTime.Now.AddYears(i - 1).Date.ToString("yy"));
                    }
                    ddlCardExpYear.DataSource = Years;
                    ddlCardExpYear.DataBind();
                    ddlCardExpYear.Items.FindByValue(DateTime.Now.Year.ToString("yy"));
                    ddlCardExpMonth.SelectedIndex = (DateTime.Now.Month - 1);

                    BindCardType();

                    if (Request.QueryString["paypalpaymentstatus"] == "paid")
                    {
                        lngDivID = Sites.ToLong(Session["DivId"]);
                        lngContId = Sites.ToLong(Session["UserContactID"]);
                        PaypalCompletePayment(Request.QueryString["token"]);
                    }
                    else
                    {
                        ClearPastSession();
                        Session["OppID"] = 0;
                    }

                    #endregion

                    //Fill Dropdowns
                    CCommon objCommon = new CCommon();
                    objCommon.sb_FillComboFromDBwithSel(ref ddlBillCountry, 40, Sites.ToLong(Session["DomainID"]));
                    objCommon.sb_FillComboFromDBwithSel(ref ddlShipCountry, 40, Sites.ToLong(Session["DomainID"]));

                    if (CCommon.ToLong(Session["UserIPCountryID"]) > 0 && ddlShipCountry.Items.FindByValue(CCommon.ToString(Session["UserIPCountryID"])) != null)
                    {
                        ddlShipCountry.Items.FindByValue(CCommon.ToString(Session["UserIPCountryID"])).Selected = true;
                        FillState(ddlShipState, Sites.ToLong(ddlShipCountry.SelectedValue), Sites.ToLong(Session["DomainID"]));

                        if (CCommon.ToLong(Session["UserIPStateID"]) > 0 && ddlShipState.Items.FindByValue(CCommon.ToString(Session["UserIPStateID"])) != null)
                        {
                            ddlShipState.Items.FindByValue(CCommon.ToString(Session["UserIPStateID"])).Selected = true;
                            hdnShipState.Value = CCommon.ToString(Session["UserIPStateID"]);
                        }
                    }

                    txtShipCode.Text = CCommon.ToString(Session["UserIPZipcode"]);
                }
                else
                {
                    if (CCommon.ToLong(hdnShipState.Value) > 0)
                    {
                        FillState(ddlShipState, Sites.ToLong(ddlShipCountry.SelectedValue), Sites.ToLong(Session["DomainID"]));

                        if (ddlShipState.Items.FindByValue(hdnShipState.Value) != null)
                        {
                            ddlShipState.Items.FindByValue(hdnShipState.Value).Selected = true;
                        }
                    }

                    if (CCommon.ToLong(hdnBillState.Value) > 0)
                    {
                        FillState(ddlBillState, Sites.ToLong(ddlBillCountry.SelectedValue), Sites.ToLong(Session["DomainID"]));

                        if (ddlBillState.Items.FindByValue(hdnBillState.Value) != null)
                        {
                            ddlBillState.Items.FindByValue(hdnBillState.Value).Selected = true;
                        }
                    }

                    if (chkShippingAdd.Checked)
                    {
                        string radalertscript = "<script language='javascript'>function f(){ChkShippingAdd(); Sys.Application.remove_load(f);}; Sys.Application.add_load(f);</script>";
                        Page.ClientScript.RegisterStartupScript(this.GetType(), "radalert", radalertscript);
                    }
                    
                }

                if (CheckEmptyCart(isFromGuestCheckout: true) == false)
                {
                    Response.Redirect("Home.aspx");
                }

                if (IsPostBack)
                {

                    if (CheckEmptyCart(isFromGuestCheckout: true) == true)
                    {
                        if (hdnPaymentMethod.Value != "")
                        {
                            if (Sites.ToString(Session["PayOption"]) != hdnPaymentMethod.Value)
                            {
                                PaymentModeChanged();
                            }
                        }
                    }
                    else
                    {
                        Response.Redirect("Home.aspx");
                    }

                    if (Sites.ToString(Request["__EVENTTARGET"]) == "btnCharge")
                    {
                        btnCharge_Click(((object)(Request["__EVENTTARGET"])), null);
                    }
                }
            }
                #endregion

            catch (Exception ex)
            {
                ExceptionModule.ExceptionPublish(ex, Sites.ToLong(Session["DomainID"]), 0, Sites.ToLong(Session["SiteId"]), Request);
                Page.MaintainScrollPositionOnPostBack = false;
                ShowMessage(ex.ToString(), 1);
            }
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            try
            {
                if (Request.QueryString["paypalpaymentstatus"] == "cancelled" && Sites.ToString(Request.QueryString["token"]) == Sites.ToString(Session["newtoken"]))
                {
                    PaypalPaymentCancel();
                }

                bindHtml();
            }
            catch (Exception ex)
            {
                ExceptionModule.ExceptionPublish(ex, Sites.ToInteger(Session["DomainID"]), Sites.ToInteger(0), Sites.ToInteger(Session["SiteId"]), Request);
                Page.MaintainScrollPositionOnPostBack = false;
                ShowMessage(ex.ToString(), 1);
            }
        }

        protected void btnCharge_Click(object sender, EventArgs e)
        {
            try
            {
                if (Convert.ToString(HttpContext.Current.Session["PostUpSell"]) == "True" && hdnIsFinishedPostCheckout.Value == "False")
                {
                    CItems objItem = new CItems();
                    objItem.DomainID = Sites.ToLong(Session["DomainID"]);
                    HttpCookie cookie = GetCookie();
                    objItem.UserCntID = CCommon.ToLong(Session["UserContactID"]);
                    objItem.vcCookieId = (cookie != null) ? cookie["KeyId"] : "";
                    objItem.byteMode = 7;
                    objItem.SiteID = CCommon.ToLong(Session["SiteID"]);
                    DataTable dtItemPostCheckOutPopup = new DataTable();
                    //DataTable dtCart = (DataTable)Session["CartData"];
                    DataSet ds = GetCartItem();
                    DataTable dtCart = ds.Tables[0];

                    if (dtCart != null && dtCart.Rows.Count > 0 && hdnIsFinishedPostCheckout.Value == "False")
                    {
                        bindPostCheckout(dtCart, objItem);
                    }
                }
                else
                {
                    if (string.IsNullOrEmpty(txtEmail.Text))
                    {
                        ShowMessage("Email is required", 1);
                        return;
                    }
                    else if (!Regex.IsMatch(txtEmail.Text.Trim(), @"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z", RegexOptions.IgnoreCase))
                    {
                        ShowMessage("Enter valid Email", 1);
                        return;
                    }
                    else if (string.IsNullOrEmpty(txtPhone.Text))
                    {
                        ShowMessage("Phone is required", 1);
                        return;
                    }
                    else if (string.IsNullOrEmpty(txtFirstName.Text))
                    {
                        ShowMessage("First Name is required", 1);
                        return;
                    }
                    else if (string.IsNullOrEmpty(txtLastName.Text))
                    {
                        ShowMessage("Last Name is required", 1);
                        return;
                    }
                    else if (string.IsNullOrEmpty(txtBillAddressName.Text))
                    {
                        ShowMessage("Bill To Name is required", 1);
                        return;
                    }
                    else if (string.IsNullOrEmpty(txtShipAddressName.Text))
                    {
                        ShowMessage("Ship To Name is required", 1);
                        return;
                    }

                    if (!chkShippingAdd.Checked)
                    {
                        if (string.IsNullOrEmpty(txtBillStreet.Text))
                        {
                            ShowMessage("Bill To Street is required", 1);
                            return;
                        }
                        else if (string.IsNullOrEmpty(txtBillCity.Text))
                        {
                            ShowMessage("Bill To City is required", 1);
                            return;
                        }
                        else if (string.IsNullOrEmpty(txtBillCode.Text))
                        {
                            ShowMessage("Bill To Zip Code is required", 1);
                            return;
                        }
                        else if (!(CCommon.ToLong(ddlBillCountry.SelectedValue) > 0))
                        {
                            ShowMessage("Bill To County is required", 1);
                            return;
                        }
                        else if (!(CCommon.ToLong(hdnBillState.Value) > 0))
                        {
                            ShowMessage("Bill To State is required", 1);
                            return;
                        }
                    }

                    if (string.IsNullOrEmpty(txtShipStreet.Text))
                    {
                        ShowMessage("Ship To Street is required", 1);
                        return;
                    }
                    else if (string.IsNullOrEmpty(txtShipCity.Text))
                    {
                        ShowMessage("Ship To City is required", 1);
                        return;
                    }
                    else if (string.IsNullOrEmpty(txtShipCode.Text))
                    {
                        ShowMessage("Ship To Zip Code is required", 1);
                        return;
                    }
                    else if (!(CCommon.ToLong(ddlShipCountry.SelectedValue) > 0))
                    {
                        ShowMessage("Ship To County is required", 1);
                        return;
                    }
                    else if (!(CCommon.ToLong(hdnShipState.Value) > 0))
                    {
                        ShowMessage("Ship To State is required", 1);
                        return;
                    }


                    #region If PayOption Selected

                    #region dropdown shippingMethod
                    if (CheckEmptyCart(isFromGuestCheckout: true) == false) { return; }

                    #region Check for User Information

                    Boolean ProceedForCheckout = true; //it is taken for checking if payment method is bill me and credit amount is less than amount due then it will not proceed

                    if (ProceedForCheckout)
                    {
                        #region Check for Shipping Service as a Line Item
                        if (Sites.ToLong(Session["ShippingServiceItemID"]) > 0)
                        {
                            #region Check for Shipping Service

                            if (Sites.ToLong(Session["DiscountServiceItemID"]) > 0)
                            {
                                UseExistingUserOrCreateNew();
                                if (hdnIsEnforcedSubTotalMeets.Value == "False")
                                { return; }


                                DeleteItemFromCart(Sites.ToLong(Session["ShippingServiceItemID"]));

                                #region check for Coupen Service

                                try
                                {
                                    if (IsOnlySalesInquiry() != 1 && !isShippingServiceAdded)
                                    {
                                        AddToCartFromEcomm("", Sites.ToLong(Session["ShippingServiceItemID"]), Sites.ToDouble(GetShippingCharge()), GetShippingMethodName(), true, isShippingCharge: true);
                                    }
                                }
                                catch (Exception ex)
                                {
                                    throw ex;
                                }

                                Session["TotalAmount"] = GetCartTotalAmount();

                                if (Sites.ToString(Session["PayOption"]) != "" || CCommon.ToDecimal(Session["TotalAmount"]) == 0)
                                {

                                    SaveOpportunity();
                                    SaveCusField();

                                    if (Sites.ToDouble(Session["OppID"]) > 0)
                                    {
                                        strQueryString = "OppID=" + Convert.ToString(Session["OppID"]);
                                        string[] strFName = null;
                                        string strFilePath = null;
                                        string strFileName = null;
                                        string strFileType = null;

                                        if ((FluDocument.HasFile))
                                        {
                                            strFileName = Path.GetFileName(FluDocument.PostedFile.FileName);
                                            //Getting the File Name
                                            // If Folder Does not exists create New Folder.
                                            if (Directory.Exists(CCommon.GetDocumentPhysicalPath(Sites.ToLong(Session["DomainID"]))) == false)
                                            {
                                                Directory.CreateDirectory(CCommon.GetDocumentPhysicalPath(Sites.ToLong(Session["DomainID"])));
                                            }
                                            strFilePath = CCommon.GetDocumentPhysicalPath(Sites.ToLong(Session["DomainID"])) + strFileName;
                                            strFName = strFileName.Split('.');
                                            strFileType = "." + Path.GetExtension(FluDocument.PostedFile.FileName);
                                            //Getting the Extension of the File
                                            FluDocument.PostedFile.SaveAs(strFilePath);

                                            UploadFile("L", strFileType, strFileName, strFileName, "", 7983, "370");
                                        }
                                    }

                                    //Set strQueryString for Redirecting on Thank You page and it is also useful for  Google Checkout Return URL 
                                    if (Sites.ToDouble(Session["OppID"]) > 0)
                                    {
                                        strQueryString = "OppID=" + Convert.ToString(Session["OppID"]);
                                        Session["CartItemsForAffliate"] = GetCartItem(isFromGuestCheckout: true);
                                        Session["TotalAffliateAmount"] = GetCartTotalAmount();
                                        Session["TOppId"] = Session["OppID"];
                                        Session["TOppName"] = Session["OppName"];
                                    }

                                    if (CCommon.ToDecimal(Session["TotalAmount"]) > 0)
                                    {
                                        if (Sites.ToDouble(Session["OppID"]) > 0 && Sites.ToString(Session["PayOption"]) == "CreditCard")
                                        {
                                            #region Credit Card Process 1

                                            if (!txtCardNumber.Text.Contains("############"))
                                            {
                                                Session["CardNumber"] = txtCardNumber.Text;
                                            }

                                            if (Sites.ToLong(ddlCardType.SelectedValue) == 0)
                                            { ShowMessage(GetErrorMessage("ERR016"), 1); return; }//Please select valid card type.

                                            SaveCustomerCreditCardInfo();

                                            string responseCode = "";
                                            string ResponseMessage = "";
                                            string ReturnTransactionID = "";
                                            PaymentGateway objPaymentGateway = new PaymentGateway();
                                            objPaymentGateway.DomainID = Sites.ToLong(Session["DomainID"]);

                                            QueryStringValues objEncryption = new QueryStringValues();
                                            OppInvoice objoppinvoice = new OppInvoice();
                                            objoppinvoice.bitflag = true;
                                            objoppinvoice.DomainID = Sites.ToInteger(Session["DomainId"]);
                                            objoppinvoice.IsDefault = true;
                                            objoppinvoice.UserCntID = Sites.ToLong(Session["UserContactID"]);
                                            DataSet ds1 = objoppinvoice.GetCustomerCreditCardInfo();
                                            if (ds1 != null && ds1.Tables.Count > 0)
                                            {
                                                if (ds1.Tables[0].Rows.Count > 0)
                                                {
                                                    string strCardNo = objEncryption.Decrypt(Sites.ToString(ds1.Tables[0].Rows[0]["vcCreditCardNo"])).Trim();//txtCHName.Text.Trim();
                                                    string strCardHolder = objEncryption.Decrypt(Sites.ToString(ds1.Tables[0].Rows[0]["vcCardHolder"])).Trim();//txtCHName.Text.Trim();
                                                    string strCardCvv = objEncryption.Decrypt(Sites.ToString(ds1.Tables[0].Rows[0]["vcCVV2"])).Trim();//txtCardCVV2.Text.Trim();

                                                    bool CreditCardStatus = false;
                                                    long eCommCCTransactionLogID = 0;
                                                    ECommerceCCTransactionLog objECommerceCCTransactionLog = new ECommerceCCTransactionLog();
                                                    try
                                                    {
                                                        objECommerceCCTransactionLog.DomainID = Sites.ToLong(Session["DomainID"]);
                                                        objECommerceCCTransactionLog.OppID = Sites.ToLong(Session["OppID"]);
                                                        objECommerceCCTransactionLog.Amount = Sites.ToDecimal(Session["TotalAmount"]);
                                                        eCommCCTransactionLogID = objECommerceCCTransactionLog.CallStarted();

                                                        CreditCardStatus = objPaymentGateway.GatewayTransaction(Sites.ToDecimal(Session["TotalAmount"]), strCardHolder, strCardNo, strCardCvv, ddlCardExpMonth.SelectedValue.PadLeft(2, '0'), Sites.ToString(ddlCardExpYear.SelectedValue), lngContId, ref ResponseMessage, ref ReturnTransactionID, ref responseCode, Sites.ToBool(Application["CreditCardAuthOnly"]), "", Sites.ToString(Session["OppName"]), Sites.ToInteger(Session["SiteId"]));


                                                        objECommerceCCTransactionLog.ID = eCommCCTransactionLogID;
                                                        objECommerceCCTransactionLog.IsSuccess = CreditCardStatus;
                                                        objECommerceCCTransactionLog.Message = ResponseMessage;
                                                        objECommerceCCTransactionLog.ExceptionMessage = "";
                                                        objECommerceCCTransactionLog.StackStrace = "";
                                                        objECommerceCCTransactionLog.CallCompleted();
                                                    }
                                                    catch (Exception ex)
                                                    {
                                                        objECommerceCCTransactionLog.ID = eCommCCTransactionLogID;
                                                        objECommerceCCTransactionLog.IsSuccess = false;
                                                        objECommerceCCTransactionLog.Message = ResponseMessage;
                                                        objECommerceCCTransactionLog.ExceptionMessage = ex.Message;
                                                        objECommerceCCTransactionLog.StackStrace = ex.StackTrace;
                                                        objECommerceCCTransactionLog.CallCompleted();

                                                        CreditCardStatus = false;

                                                        #region Delete Service Items
                                                        if (this.isShippingServiceAdded)
                                                        {
                                                            if (Sites.ToLong(Session["ShippingServiceItemID"]) > 0)
                                                            {
                                                                DeleteItemFromCart(Sites.ToLong(Session["ShippingServiceItemID"]));
                                                            }
                                                        }

                                                        if (this.isDiscountServiceAdded)
                                                        {
                                                            if (Sites.ToLong(Session["DiscountServiceItemID"]) > 0)
                                                            {
                                                                DeleteItemFromCart(Sites.ToLong(Session["DiscountServiceItemID"]));
                                                            }
                                                        }
                                                        #endregion

                                                        this.isDiscountServiceAdded = false;

                                                        throw ex;
                                                    }
                                                    #region processing of credit card

                                                    if (CreditCardStatus)
                                                    {
                                                        //TODO:Insert Transaction details into TransactionHistory table
                                                        //1 capture 2 Authorise
                                                        TransactionHistory objTransactionHistory = new TransactionHistory();
                                                        objTransactionHistory.TransHistoryID = 0;
                                                        objTransactionHistory.DivisionID = lngDivID;
                                                        objTransactionHistory.ContactID = lngContId;
                                                        objTransactionHistory.OppID = Sites.ToLong(Session["OppID"]);
                                                        objTransactionHistory.OppBizDocsID = lngOppBizDocID;
                                                        objTransactionHistory.TransactionID = Sites.ToString(ReturnTransactionID);
                                                        objTransactionHistory.TransactionStatus = (Sites.ToBool(Application["CreditCardAuthOnly"]) == true ? CCommon.ToShort(1) : CCommon.ToShort(2));
                                                        objTransactionHistory.PGResponse = ResponseMessage;
                                                        objTransactionHistory.Type = 1;//1 is for Authorative and 2 for Captured
                                                        if (Sites.ToBool(Application["CreditCardAuthOnly"]))
                                                        {
                                                            objTransactionHistory.AuthorizedAmt = Sites.ToDecimal(Session["TotalAmount"]);
                                                        }
                                                        else
                                                        {
                                                            objTransactionHistory.CapturedAmt = Sites.ToDecimal(Session["TotalAmount"]);
                                                        }
                                                        objTransactionHistory.RefundAmt = 0;
                                                        objTransactionHistory.CardHolder = objCommon.Encrypt(strCardHolder);
                                                        objTransactionHistory.CreditCardNo = objCommon.Encrypt(strCardNo.Trim().Contains("*") ? Sites.ToString(Session["CCNO"]) : strCardNo.Trim());
                                                        objTransactionHistory.Cvv2 = objCommon.Encrypt(strCardCvv);
                                                        objTransactionHistory.ValidMonth = CCommon.ToShort(Sites.ToString(ds1.Tables[0].Rows[0]["tintValidMonth"]).PadLeft(2, '0'));
                                                        objTransactionHistory.ValidYear = CCommon.ToShort(ds1.Tables[0].Rows[0]["intValidYear"]);
                                                        objTransactionHistory.SignatureFile = "";
                                                        objTransactionHistory.UserCntID = Sites.ToLong(Session["UserContactID"]);
                                                        objTransactionHistory.DomainID = Sites.ToLong(Session["DomainID"]);
                                                        objTransactionHistory.CardType = Sites.ToLong(ds1.Tables[0].Rows[0]["numCardTypeId"]);
                                                        objTransactionHistory.ResponseCode = responseCode;
                                                        lngTransactionHistoryID = objTransactionHistory.ManageTransactionHistory();

                                                        objECommerceCCTransactionLog.ID = eCommCCTransactionLogID;
                                                        objECommerceCCTransactionLog.TransactionHistoryID = lngTransactionHistoryID;
                                                        objECommerceCCTransactionLog.UpdateTransactionHistoryID();

                                                        MakeDepositeEntries();
                                                        CreateShippingReport();//create shipping report

                                                        ClearCartItems();//Clear existing cart
                                                        Session["OppID"] = 0;
                                                        //btnCharge.Visible = false;
                                                    }
                                                    else
                                                    {
                                                        #region Delete Service Items
                                                        if (this.isShippingServiceAdded)
                                                        {
                                                            if (Sites.ToLong(Session["ShippingServiceItemID"]) > 0)
                                                            {
                                                                DeleteItemFromCart(Sites.ToLong(Session["ShippingServiceItemID"]));
                                                            }
                                                        }

                                                        if (this.isDiscountServiceAdded)
                                                        {
                                                            if (Sites.ToLong(Session["DiscountServiceItemID"]) > 0)
                                                            {
                                                                DeleteItemFromCart(Sites.ToLong(Session["DiscountServiceItemID"]));
                                                            }
                                                        }
                                                        #endregion

                                                        this.isDiscountServiceAdded = false;

                                                        #region Update Opportunity Status Failed .
                                                        CCommon objCommon = new CCommon();
                                                        objCommon.Mode = 37;
                                                        objCommon.UpdateRecordID = Sites.ToLong(Session["OppID"]);
                                                        objCommon.DomainID = Sites.ToLong(Session["DomainID"]);
                                                        objCommon.UpdateSingleFieldValue();

                                                        Page.MaintainScrollPositionOnPostBack = false;
                                                        ShowMessage(ResponseMessage, 0);
                                                        return;

                                                        #endregion
                                                    }
                                                    #endregion
                                                }
                                            }
                                            #endregion
                                        }
                                        else if (Sites.ToDouble(Session["OppID"]) > 0 && Sites.ToString(Session["PayOption"]) == "GoogleCheckout")
                                        {
                                            #region Check for Google Checkout Configuration
                                            UserAccess objUserAccess = new UserAccess();
                                            objUserAccess.DomainID = Sites.ToLong(Session["DomainID"]);
                                            objUserAccess.SiteID = Sites.ToLong(Session["SiteId"]);
                                            string strGoogleMerchantID = "";
                                            string strGoogleMerchantKey = "";
                                            Boolean IsSandbox = true;
                                            DataTable dtECommerceDetail = objUserAccess.GetECommerceDetails();

                                            if (dtECommerceDetail != null)
                                            {
                                                strGoogleMerchantID = Sites.ToString(dtECommerceDetail.Rows[0]["vcGoogleMerchantID"]);
                                                strGoogleMerchantKey = Sites.ToString(dtECommerceDetail.Rows[0]["vcGoogleMerchantKey"]);
                                                IsSandbox = Sites.ToBool(dtECommerceDetail.Rows[0]["IsSandbox"]);
                                            }
                                            else
                                            {
                                                strGoogleMerchantID = "";
                                                strGoogleMerchantKey = "";
                                            }

                                            if (strGoogleMerchantID == "" || strGoogleMerchantKey == "")
                                            {
                                                IsGoogleCheckoutConfigured = false;
                                                ShowMessage(GetErrorMessage("ERR061"), 1);
                                                return;
                                            }
                                            #endregion

                                            if (IsGoogleCheckoutConfigured)
                                            {
                                                #region Google Checkout Process 31488
                                                BACRM.BusinessLogic.ShioppingCart.GoogleCheckout objGoogleCheckout = new GoogleCheckout();

                                                DataSet dsCart = GetCartItem(isFromGuestCheckout: true);
                                                if (dsCart != null)
                                                {
                                                    //set strQueryString variable to redirect to Thank You Page with Querystring. After Creation of Bizdoc we get lngBizDocID
                                                    MakeDepositeEntries();
                                                    CreateShippingReport();

                                                    if (dsCart.Tables[0].Rows.Count > 0)
                                                    {
                                                        decimal Tax = default(decimal);
                                                        Tax = GetTax();

                                                        string strGoogleRedirect = "";
                                                        strGoogleRedirect = objGoogleCheckout.GoogleCheckoutRequest(strGoogleMerchantID, strGoogleMerchantKey, IsSandbox, Sites.ToString(Session["OppID"]), Sites.ToLong(Session["DomainID"]), strQueryString, Tax, dsCart);

                                                        if (strGoogleRedirect != "")
                                                        {

                                                            ClearCartItems();
                                                            Session["OppID"] = 0;
                                                            strQueryString = ""; //This should be null otherwise it will redirect to Thank You Page.
                                                            Response.Redirect(strGoogleRedirect, false);
                                                        }
                                                        else
                                                        {
                                                            Page.MaintainScrollPositionOnPostBack = false;
                                                            ShowMessage(GetErrorMessage("ERR070"), 1);//There is a problem to redirect to Google Checkout.
                                                        }
                                                    }
                                                }
                                                #endregion
                                            }
                                            else
                                            {
                                                ShowMessage(GetErrorMessage("ERR061"), 1);
                                                return;
                                            }

                                        }
                                        else if (Sites.ToDouble(Session["OppID"]) > 0 && Sites.ToString(Session["PayOption"]) == "Paypal")
                                        {
                                            #region Check for Paypal Configuration
                                            UserAccess objUserAccess = new UserAccess();
                                            objUserAccess.DomainID = Sites.ToLong(Session["DomainID"]);
                                            objUserAccess.SiteID = Sites.ToLong(Session["SiteId"]);
                                            string strPaypalUserName = "";
                                            string strPaypalPassword = "";
                                            string strPaypalSignature = "";
                                            bool IsPaypalSandbox = true;


                                            DataTable dtECommerceDetail = objUserAccess.GetECommerceDetails();

                                            if (dtECommerceDetail != null)
                                            {
                                                strPaypalUserName = Sites.ToString(dtECommerceDetail.Rows[0]["vcPaypalUserName"]);
                                                strPaypalPassword = Sites.ToString(dtECommerceDetail.Rows[0]["vcPaypalPassword"]);
                                                strPaypalSignature = Sites.ToString(dtECommerceDetail.Rows[0]["vcPaypalSignature"]);
                                                IsPaypalSandbox = Sites.ToBool(dtECommerceDetail.Rows[0]["IsPaypalSandbox"]);
                                            }

                                            if (strPaypalUserName == "" || strPaypalPassword == "" || strPaypalSignature == "")
                                            {
                                                IsPaypalConfigured = false;
                                                ShowMessage(GetErrorMessage("ERR073"), 1);
                                                return;
                                            }
                                            #endregion

                                            if (IsPaypalConfigured)
                                            {
                                                #region Pay by Paypal Process 35141
                                                //BACRM.BusinessLogic.ShioppingCart.GoogleCheckout objGoogleCheckout = new GoogleCheckout();
                                                decimal OrderTotal = 0;
                                                OrderTotal = GetCartTotalAmount();
                                                string strPaypalRedirect = "";

                                                DataSet dsCart = GetCartItem(isFromGuestCheckout: true);
                                                if (dsCart != null)
                                                {
                                                    if (dsCart.Tables[0].Rows.Count > 0)
                                                    {

                                                        if (IsPaypalSandbox)
                                                        {
                                                            PaypalServerUrl = Sites.ToString(System.Configuration.ConfigurationManager.AppSettings["PaypalSandboxUrl"]);
                                                            // expresscheckout1.URL = "https://api-3t.sandbox.paypal.com/nvp"; //Test server URL
                                                        }
                                                        else
                                                        {
                                                            PaypalServerUrl = Sites.ToString(System.Configuration.ConfigurationManager.AppSettings["PaypalServerUrl"]);
                                                            // expresscheckout1.URL =  "https://www.paypal.com/cgi-bin/webscr"; //Paypal server URL
                                                        }
                                                        expresscheckout1.URL = PaypalServerUrl;
                                                        expresscheckout1.User = strPaypalUserName;
                                                        expresscheckout1.Password = strPaypalPassword;
                                                        expresscheckout1.Signature = strPaypalSignature;
                                                        expresscheckout1.Config("SSLEnabledProtocols=3072");

                                                        //expresscheckout1.User = "jxavier.mca-facilitator_api1.gmail.com";
                                                        //expresscheckout1.Password = "1371651348";
                                                        //expresscheckout1.Signature = "AFcWxV21C7fd0v3bYYYRCpSSRl31A9aGptwdTWtuPS0gGenvixMLuh4Y";

                                                        expresscheckout1.OrderTotal = Sites.ToString(Math.Round(OrderTotal, 2));

                                                        expresscheckout1.TaxTotal = Sites.ToString(Math.Round(dcTotalTax, 2));
                                                        string currentpath = "http://" + Request.ServerVariables["SERVER_NAME"] + Request.ServerVariables["PATH_INFO"];
                                                        expresscheckout1.ReturnURL = currentpath + "?paypalpaymentstatus=paid";
                                                        expresscheckout1.CancelURL = currentpath + "?paypalpaymentstatus=cancelled";
                                                        expresscheckout1.PaymentAction = ExpresscheckoutPaymentActions.aSale;
                                                        expresscheckout1.BuyerEmail = txtPayPalEmail.Text;

                                                        for (int intCnt = 0; intCnt <= dsCart.Tables[0].Rows.Count - 1; intCnt++)
                                                        {
                                                            PaymentItem PayItem = new PaymentItem();
                                                            PayItem.Name = Sites.ToString(dsCart.Tables[0].Rows[intCnt]["vcItemName"]);
                                                            PayItem.Amount = String.Format("{0:N2}", Math.Round(Sites.ToDouble(dsCart.Tables[0].Rows[intCnt]["monPrice"]), 2));
                                                            PayItem.Quantity = Sites.ToInteger(dsCart.Tables[0].Rows[intCnt]["numUnitHour"]);
                                                            PayItem.Description = Sites.ToString(dsCart.Tables[0].Rows[intCnt]["vcItemDesc"]);
                                                            expresscheckout1.Items.Insert(intCnt, PayItem);
                                                            //expresscheckout1.OrderTotal = Sites.ToString(Sites.ToDecimal(expresscheckout1.OrderTotal) + Sites.ToDecimal(Math.Round(Sites.ToDecimal(PayItem.Amount), 2)));
                                                        }

                                                        try
                                                        {
                                                            expresscheckout1.SetCheckout();
                                                        }
                                                        catch (Exception ex)
                                                        {
                                                            //ShowMessage(GetErrorMessage("ERR074"), 1); //Invalid Paypal User Email Or Invalid Merchant Paypal Account Configuration
                                                            ShowMessage(ex.Message.ToString(), 1);

                                                            #region Delete Service Items
                                                            if (this.isShippingServiceAdded)
                                                            {
                                                                if (Sites.ToLong(Session["ShippingServiceItemID"]) > 0)
                                                                {
                                                                    DeleteItemFromCart(Sites.ToLong(Session["ShippingServiceItemID"]));
                                                                }
                                                            }

                                                            if (this.isDiscountServiceAdded)
                                                            {
                                                                if (Sites.ToLong(Session["DiscountServiceItemID"]) > 0)
                                                                {
                                                                    DeleteItemFromCart(Sites.ToLong(Session["DiscountServiceItemID"]));
                                                                }
                                                            }
                                                            #endregion
                                                            return;
                                                        }

                                                        string responsetoken = expresscheckout1.ResponseToken;
                                                        Session["newtoken"] = responsetoken;
                                                        if (IsPaypalSandbox)
                                                        {
                                                            PaypalRedirectServerUrl = Sites.ToString(System.Configuration.ConfigurationManager.AppSettings["PaypalRedirectSandboxUrl"]);
                                                            //strPaypalRedirect = "https://www.sandbox.paypal.com/cgi-bin/webscr?cmd=_express-checkout&token=" + responsetoken;
                                                        }
                                                        else
                                                        {
                                                            PaypalRedirectServerUrl = Sites.ToString(System.Configuration.ConfigurationManager.AppSettings["PaypalRedirectServerUrl"]);
                                                            //strPaypalRedirect = "https://www.paypal.com/cgi-bin/webscr?cmd=_express-checkout&token=" + responsetoken;
                                                        }
                                                        strPaypalRedirect = PaypalRedirectServerUrl + responsetoken;
                                                        if (responsetoken != "")
                                                        {
                                                            //ClearCartItems();
                                                            strQueryString = ""; //This should be null otherwise it will redirect to Thank You Page.
                                                            Response.Redirect(strPaypalRedirect, false);
                                                        }
                                                        else
                                                        {
                                                            strQueryString = "Sorry!., There is a problem in redirecting to Paypal..";
                                                            Page.MaintainScrollPositionOnPostBack = false;
                                                            ShowMessage(GetErrorMessage("ERR075"), 1);//There is a problem with Paypal Buyer Email Id account.
                                                            #region Delete Service Items
                                                            if (this.isShippingServiceAdded)
                                                            {
                                                                if (Sites.ToLong(Session["ShippingServiceItemID"]) > 0)
                                                                {
                                                                    DeleteItemFromCart(Sites.ToLong(Session["ShippingServiceItemID"]));
                                                                }
                                                            }

                                                            if (this.isDiscountServiceAdded)
                                                            {
                                                                if (Sites.ToLong(Session["DiscountServiceItemID"]) > 0)
                                                                {
                                                                    DeleteItemFromCart(Sites.ToLong(Session["DiscountServiceItemID"]));
                                                                }
                                                            }
                                                            #endregion

                                                            return;
                                                        }
                                                    }
                                                }
                                                #endregion
                                            }
                                            else
                                            {
                                                ShowMessage(GetErrorMessage("ERR073"), 1); // Merchant Paypal Account Details Not Configured
                                                #region Delete Service Items
                                                if (this.isShippingServiceAdded)
                                                {
                                                    if (Sites.ToLong(Session["ShippingServiceItemID"]) > 0)
                                                    {
                                                        DeleteItemFromCart(Sites.ToLong(Session["ShippingServiceItemID"]));
                                                    }
                                                }

                                                if (this.isDiscountServiceAdded)
                                                {
                                                    if (Sites.ToLong(Session["DiscountServiceItemID"]) > 0)
                                                    {
                                                        DeleteItemFromCart(Sites.ToLong(Session["DiscountServiceItemID"]));
                                                    }
                                                }
                                                #endregion

                                                return;
                                            }
                                        }
                                        else if (Sites.ToDouble(Session["OppID"]) > 0 && Sites.ToString(Session["PayOption"]) == "SalesInquiry")
                                        {
                                            MakeDepositeEntries();

                                            ClearCartItems();//Clear existing cart
                                            Session["OppID"] = 0;
                                            Session["ShippingMethodValue"] = null;
                                            Session["PayOption"] = null;//PayOptio null after placing order.
                                            if (Sites.ToString(Session["vcRedirectThankYouUrl"]) != "")
                                            {

                                                Response.Redirect("thankyou", false);
                                            }
                                            else
                                            {
                                                Response.Redirect("ThankYou.aspx?" + strQueryString + "&rt=3", false);
                                            }
                                        }
                                        else if (Sites.ToDouble(Session["OppID"]) > 0 && Sites.ToString(Session["PayOption"]) == "BillMe")
                                        {
                                            MakeDepositeEntries();

                                            ClearCartItems();//Clear existing cart
                                            Session["OppID"] = 0;
                                            Session["ShippingMethodValue"] = null;
                                            Session["PayOption"] = null;//PayOptio null after placing order.
                                            if (Sites.ToString(Session["vcRedirectThankYouUrl"]) != "")
                                            {

                                                Response.Redirect("thankyou", false);
                                            }
                                            else
                                            {
                                                Response.Redirect("ThankYou.aspx?" + strQueryString + "&rt=3", false);
                                            }
                                        }
                                        else
                                        {
                                            //If in Case session Session["PayOption"] != "" and not from above Payment Options .
                                            Page.MaintainScrollPositionOnPostBack = false;
                                            //Page.ClientScript.RegisterStartupScript(Page.ClientScript.GetType(), Page.ClientID, "resetScrollPosition();", true);
                                            ShowMessage(GetErrorMessage("ERR018"), 1);//Please select a payment option
                                            bindHtml();
                                            return;
                                        }
                                    }
                                    else
                                    {
                                        ClearCartItems();//Clear existing cart
                                    }

                                    
                                    if (strQueryString != "")
                                    {
                                        Session["OppID"] = 0;
                                        Session["ShippingMethodValue"] = null;
                                        Session["PayOption"] = null;//PayOptio null after placing order.
                                        if (Sites.ToString(Session["vcRedirectThankYouUrl"]) != "")
                                        {
                                            Response.Redirect("thankyou", false);
                                        }
                                        else
                                        {
                                            Response.Redirect("ThankYou.aspx?" + strQueryString + "&rt=0", false);
                                        }
                                    }
                                #endregion
                                }
                                else
                                {
                                    Page.MaintainScrollPositionOnPostBack = false;
                                    //Page.ClientScript.RegisterStartupScript(Page.ClientScript.GetType(), Page.ClientID, "resetScrollPosition();", true);
                                    ShowMessage(GetErrorMessage("ERR018"), 1);
                                    return;
                                }
                            }
                            else
                            {
                                Page.MaintainScrollPositionOnPostBack = false;
                                ShowMessage(GetErrorMessage("ERR072"), 1);
                                //Error message for configuring coupon service in domain detail
                            }
                            #endregion
                        }
                        else
                        {
                            Page.MaintainScrollPositionOnPostBack = false;
                            //Page.ClientScript.RegisterStartupScript(Page.ClientScript.GetType(), Page.ClientID, "resetScrollPosition();", true);
                            ShowMessage(GetErrorMessage("ERR067"), 1);
                        }
                        #endregion
                    }

                    #endregion
                    #endregion

                    #endregion

                }
            }
            catch (Exception ex)
            {
                if (CCommon.ToLong(hdnOrderPromotionID.Value) > 0)
                {
                    BACRM.BusinessLogic.ShioppingCart.Cart objcart = new BACRM.BusinessLogic.ShioppingCart.Cart();
                    HttpCookie cookie = Request.Cookies["Cart"];
                    objcart.DomainID = CCommon.ToLong(Session["DomainID"]);
                    objcart.ContactId = CCommon.ToLong(Session["UserContactID"]);
                    objcart.CookieId = (cookie != null) ? cookie["KeyId"] : "";
                    objcart.PromotionID = CCommon.ToLong(hdnOrderPromotionID.Value);
                    objcart.ClearCouponCodePromotion();
                }


                DeleteItemFromCart(Sites.ToLong(Session["ShippingServiceItemID"]));
                DeleteItemFromCart(Sites.ToLong(Session["DiscountServiceItemID"]));
                hdnOrderPromotionID.Value = "";
                hdnDiscountCodeID.Value = "";
                lblCouponCode.Text = "";
                txtCouponCode.Text = "";
                trApplyCouponCode.Visible = true;
                trUsedCouponCode.Visible = false;

                if (ex.Message.Contains("ITEM_PROMOTION_EXPIRED"))
                {
                    Page.MaintainScrollPositionOnPostBack = false;
                    ShowMessage(GetErrorMessage("ERR006"), 1);
                }
                else if (ex.Message.Contains("INVALID_COUPON_CODE_ITEM_PROMOTION"))
                {
                    Page.MaintainScrollPositionOnPostBack = false;
                    ShowMessage(GetErrorMessage("ERR011"), 1);
                }
                else if (ex.Message.Contains("ITEM_PROMOTION_COUPON_CODE_USAGE_LIMIT_EXCEEDED"))
                {
                    Page.MaintainScrollPositionOnPostBack = false;
                    ShowMessage(GetErrorMessage("ERR008"), 1);
                }
                else if (ex.Message.Contains("COUPON_CODE_REQUIRED_ITEM_PROMOTION"))
                {
                    Page.MaintainScrollPositionOnPostBack = false;
                    ShowMessage("Coupon code is required to use promotion.", 1);
                }
                else if (ex.Message.Contains("ORDER_PROMOTION_EXPIRED"))
                {
                    Page.MaintainScrollPositionOnPostBack = false;
                    ShowMessage(GetErrorMessage("ERR006"), 1);
                }
                else if (ex.Message.Contains("INVALID_COUPON_CODE_ORDER_PROMOTION"))
                {
                    Page.MaintainScrollPositionOnPostBack = false;
                    ShowMessage(GetErrorMessage("ERR011"), 1);
                }
                else if (ex.Message.Contains("ORDER_PROMOTION_COUPON_CODE_USAGE_LIMIT_EXCEEDED"))
                {
                    Page.MaintainScrollPositionOnPostBack = false;
                    ShowMessage(GetErrorMessage("ERR008"), 1);
                }
                else if (ex.Message.Contains("COUPON_CODE_REQUIRED_ORDER_PROMOTION"))
                {
                    Page.MaintainScrollPositionOnPostBack = false;
                    ShowMessage(GetErrorMessage("Coupon code is required to use promotion."), 1);
                }
                else
                {
                    ExceptionModule.ExceptionPublish(ex, Sites.ToLong(Session["DomainID"]), Sites.ToLong(Session["UserContactID"]), Sites.ToLong(Session["SiteId"]), Request);
                    Page.MaintainScrollPositionOnPostBack = false;
                    //Page.ClientScript.RegisterStartupScript(Page.ClientScript.GetType(), Page.ClientID, "resetScrollPosition();", true);
                    ShowMessage(ex.ToString(), 1);
                }
            }

        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            foreach (GridViewRow row in gvPromotionPostCheckOut.Rows)
            {
                int RowIndex = row.RowIndex;

                CheckBox chkAdd = (CheckBox)row.FindControl("chkAdd");
                if (chkAdd.Checked == true)
                {
                    long _itemCode = CCommon.ToLong(gvPromotionPostCheckOut.DataKeys[RowIndex].Values["numItemCode"]);
                    long parentItemCode = CCommon.ToLong(gvPromotionPostCheckOut.DataKeys[RowIndex].Values["numParentItemCode"]);

                    TextBox txtQty = (TextBox)row.FindControl("txtQty");
                    int _qty = CCommon.ToInteger(txtQty.Text);

                    AddToCartFromEcomm("", _itemCode, 0, "", false, _qty, parentItemCode: parentItemCode);
                }
            }
            hdnIsFinishedPostCheckout.Value = "True";
            dialogSellPopup.Visible = false;
        }

        private DataTable RelatedItemsPromotion(int numItemCode, long numWarehouseItemID)
        {
            CItems objItems = new CItems();
            objItems.ItemCode = numItemCode;
            objItems.DomainID = CCommon.ToLong(Session["DomainID"]);
            objItems.WareHouseItemID = numWarehouseItemID;
            objItems.numSiteId = CCommon.ToLong(HttpContext.Current.Session["SiteID"]);
            DataTable dt = objItems.GetPromotionsForSimilarItems();
            return dt;
        }

        /*protected void btnFinished_Click(object sender, EventArgs e)
        {
            hdnIsFinishedPostCheckout.Value = "True";
            dialogSellPopup.Visible = false;
            btnCharge_Click(null, null);
        }*/

        protected void btnNoThanks_Click(object sender, EventArgs e)
        {
            hdnIsFinishedPostCheckout.Value = "True";
            dialogSellPopup.Visible = false;
            //btnCharge_Click(null, null);
        }

        public void UpdateAddress()
        {
            try
            {

                CContacts objContacts = new CContacts();
                objContacts.RecordID = Sites.ToLong(Session["DivId"]);
                objContacts.DomainID = Sites.ToLong(Session["DomainID"]);


                if (chkShippingAdd.Checked)
                {
                    objContacts.AddressName = txtBillAddressName.Text;
                    objContacts.Street = txtShipStreet.Text.Trim();
                    objContacts.City = txtShipCity.Text.Trim();
                    objContacts.Country = Sites.ToLong(ddlShipCountry.SelectedValue);
                    objContacts.PostalCode = txtShipCode.Text.Trim();
                    objContacts.State = Sites.ToLong(hdnShipState.Value);
                    objContacts.AddresOf = CContacts.enmAddressOf.Organization;
                    objContacts.AddressType = CContacts.enmAddressType.BillTo;
                    objContacts.IsAltContact = true;
                    objContacts.AltContact = Sites.ToString(txtBillAddressName.Text.Trim());
                }
                else
                {
                    objContacts.AddressID = 0; //Clear AddressID
                    objContacts.AddressName = txtBillAddressName.Text;
                    objContacts.Street = txtBillStreet.Text.Trim();
                    objContacts.City = txtBillCity.Text.Trim();
                    objContacts.Country = Sites.ToLong(ddlBillCountry.SelectedValue);
                    objContacts.PostalCode = txtBillCode.Text.Trim();
                    objContacts.State = Sites.ToLong(hdnBillState.Value);
                    objContacts.AddresOf = CContacts.enmAddressOf.Organization;
                    objContacts.AddressType = CContacts.enmAddressType.BillTo;
                    objContacts.IsAltContact = true;
                    objContacts.AltContact = Sites.ToString(txtBillAddressName.Text.Trim());
                }
                objContacts.ManageAddress();
                Session["BillAddress"] = objContacts.AddressID;

                objContacts.AddressID = 0; //Clear AddressID
                objContacts.AddressName = txtShipAddressName.Text;
                objContacts.Street = txtShipStreet.Text.Trim();
                objContacts.City = txtShipCity.Text.Trim();
                objContacts.Country = Sites.ToLong(ddlShipCountry.SelectedValue);
                objContacts.PostalCode = txtShipCode.Text.Trim();
                objContacts.State = Sites.ToLong(hdnShipState.Value);
                objContacts.AddresOf = CContacts.enmAddressOf.Organization;
                objContacts.AddressType = CContacts.enmAddressType.ShipTo;
                objContacts.IsResidential = chkResidential.Checked;
                objContacts.ContactPhone = txtPhone.Text;
                objContacts.IsFromECommerce = true;
                objContacts.IsAltContact = true;
                objContacts.AltContact = Sites.ToString(txtShipAddressName.Text.Trim());
                objContacts.ManageAddress();
                Session["ShipAddress"] = objContacts.AddressID;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void UploadFile(string URLType, string strFileType, string strFileName, string DocName, string DocDesc, long DocumentStatus, string DocCategory)
        {
            try
            {
                string[] arrOutPut = null;
                DocumentList objDocuments = new DocumentList();

                objDocuments.DomainID = Sites.ToInteger(Session["DomainID"]);
                objDocuments.UserCntID = Sites.ToLong(Session["UserContactID"]);
                objDocuments.UrlType = URLType;
                objDocuments.DocumentStatus = DocumentStatus;
                objDocuments.DocCategory = 370;
                objDocuments.FileType = strFileType;
                objDocuments.DocName = DocName;
                objDocuments.DocDesc = DocDesc;
                objDocuments.FileName = strFileName;
                objDocuments.DocumentSection = "O";
                int lngRecID = Sites.ToInteger(Session["OppID"]);
                objDocuments.RecID = lngRecID;
                objDocuments.DocumentType = 1;
                //1=generic,2=specific
                arrOutPut = objDocuments.SaveDocuments();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void PaypalCompletePayment(string token)
        {
            #region Check for Paypal Configuration
            try
            {
                UserAccess objUserAccess = new UserAccess();
                objUserAccess.DomainID = Sites.ToLong(Session["DomainID"]);
                objUserAccess.SiteID = Sites.ToLong(Session["SiteID"]);
                string strPaypalUserName = "";
                string strPaypalPassword = "";
                string strPaypalSignature = "";
                bool IsPaypalSandbox = true;

                DataTable dtECommerceDetail = objUserAccess.GetECommerceDetails();

                if (dtECommerceDetail != null)
                {
                    strPaypalUserName = Sites.ToString(dtECommerceDetail.Rows[0]["vcPaypalUserName"]);
                    strPaypalPassword = Sites.ToString(dtECommerceDetail.Rows[0]["vcPaypalPassword"]);
                    strPaypalSignature = Sites.ToString(dtECommerceDetail.Rows[0]["vcPaypalSignature"]);
                    IsPaypalSandbox = Sites.ToBool(dtECommerceDetail.Rows[0]["IsPaypalSandbox"]);
                }

                if (strPaypalUserName == "" || strPaypalPassword == "" || strPaypalSignature == "")
                {
                    IsPaypalConfigured = false;
                    ShowMessage(GetErrorMessage("ERR073"), 1); // Paypal Merchant Account not Configured
                    return;
                }
            #endregion

                if (IsPaypalConfigured)
                {
                    decimal OrderTotal = GetCartTotalAmount();
                    if (IsPaypalSandbox)
                    {
                        PaypalServerUrl = Sites.ToString(System.Configuration.ConfigurationManager.AppSettings["PaypalSandboxUrl"]);
                    }
                    else
                    {
                        PaypalServerUrl = Sites.ToString(System.Configuration.ConfigurationManager.AppSettings["PaypalServerUrl"]);
                    }
                    expresscheckout1.URL = PaypalServerUrl;
                    expresscheckout1.User = strPaypalUserName;
                    expresscheckout1.Password = strPaypalPassword;
                    expresscheckout1.Signature = strPaypalSignature;
                    expresscheckout1.Config("SSLEnabledProtocols=3072");
                    expresscheckout1.OrderTotal = Sites.ToString(Math.Round(OrderTotal, 2));
                    expresscheckout1.Token = token;
                    expresscheckout1.GetCheckoutDetails();

                    if (expresscheckout1.Ack.Equals("Success"))
                    {
                        //capture the payment
                        expresscheckout1.PaymentAction = ExpresscheckoutPaymentActions.aSale;
                        expresscheckout1.Token = token;
                        expresscheckout1.OrderTotal = Sites.ToString(Math.Round(OrderTotal, 2));
                        expresscheckout1.CheckoutPayment();

                        strQueryString = ""; //This should be null otherwise it will redirect to Thank You Page.
                        DataSet dsCart = GetCartItem(isFromGuestCheckout: true);
                        if (dsCart != null)
                        {
                            MakeDepositeEntries();
                            CreateShippingReport();
                            strQueryString = "OppID=" + Convert.ToString(Session["OppID"]);
                            Session["CartItemsForAffliate"] = GetCartItem(isFromGuestCheckout: true);
                            Session["TotalAffliateAmount"] = GetCartTotalAmount();
                            Session["TOppId"] = Session["OppID"];
                            Session["TOppName"] = Session["OppName"];
                            ClearCartItems();
                            Session["OppID"] = 0;
                        }
                    }
                    else
                    {
                        ShowMessage(GetErrorMessage("ERR076"), 1); // Paypal Payment Failure.
                        strQueryString = "PayPal: " + expresscheckout1.Ack + ", Payment Status: " + expresscheckout1.Payment.Status;
                        #region Delete Service Items
                        if (this.isShippingServiceAdded)
                        {
                            if (Sites.ToLong(Session["ShippingServiceItemID"]) > 0)
                            {
                                DeleteItemFromCart(Sites.ToLong(Session["ShippingServiceItemID"]));
                            }
                        }

                        if (this.isDiscountServiceAdded)
                        {
                            if (Sites.ToLong(Session["DiscountServiceItemID"]) > 0)
                            {
                                DeleteItemFromCart(Sites.ToLong(Session["DiscountServiceItemID"]));

                            }
                        }
                        return;

                        #endregion
                    }
                }

                Session["ShippingMethodValue"] = null;
                Session["PayOption"] = null;//PayOptio null after placing order.
                if (Sites.ToString(Session["vcRedirectThankYouUrl"]) != "")
                {
                    Response.Redirect("thankyou", true);
                }
                else
                {
                    Response.Redirect("ThankYou.aspx?" + strQueryString, true);
                }
            }
            catch (Exception ex)
            {

                ShowMessage(GetErrorMessage("ERR076"), 1); // Paypal Payment Failure.
                #region Delete Service Items
                if (this.isShippingServiceAdded)
                {
                    if (Sites.ToLong(Session["ShippingServiceItemID"]) > 0)
                    {
                        DeleteItemFromCart(Sites.ToLong(Session["ShippingServiceItemID"]));
                    }
                }

                if (this.isDiscountServiceAdded)
                {
                    if (Sites.ToLong(Session["DiscountServiceItemID"]) > 0)
                    {
                        DeleteItemFromCart(Sites.ToLong(Session["DiscountServiceItemID"]));
                    }
                }
                return;

                #endregion
            }
        }

        private void PaypalPaymentCancel()
        {
            ShowMessage(GetErrorMessage("ERR076"), 1); // Paypal Payment Failure.

            #region Delete Service Items
            //if (this.isShippingServiceAdded)
            //{
            if (Sites.ToLong(Session["ShippingServiceItemID"]) > 0)
            {
                DeleteItemFromCart(Sites.ToLong(Session["ShippingServiceItemID"]));
            }
            //}

            if (this.isDiscountServiceAdded)
            {
                if (Sites.ToLong(Session["DiscountServiceItemID"]) > 0)
                {
                    DeleteItemFromCart(Sites.ToLong(Session["DiscountServiceItemID"]));
                }
            }

            #endregion
        }

        protected void btnOrders_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect("Orders.aspx");
            }
            catch (Exception ex)
            {
                ExceptionModule.ExceptionPublish(ex, Sites.ToLong(Session["DomainID"]), Sites.ToLong(Session["UserContactID"]), Sites.ToLong(Session["SiteId"]), Request);
                Page.MaintainScrollPositionOnPostBack = false;
                //Page.ClientScript.RegisterStartupScript(Page.ClientScript.GetType(), Page.ClientID, "resetScrollPosition();", true);
                ShowMessage(ex.ToString(), 1);
            }
        }

        protected void lbtnEdit_Click(object sender, EventArgs e)
        {
            if (Sites.ToBool(Session["IsOnePageCheckout"]))
            {
                Session["SelectedStep"] = 1;
                Response.Redirect("/OnePageCheckout.aspx", true);
            }
            else
            {
                Response.Redirect("/ConfirmAddress.aspx?Return=OrderSummary.aspx", true);
            }
        }

        protected void lbtnCustomerInformationEdit_Click(object sender, EventArgs e)
        {
            if (Sites.ToBool(Session["IsOnePageCheckout"]))
            {
                //<a href="/CustomerInformation.aspx?Return=OrderSummary.aspx">edit</a>
                Session["SelectedStep"] = 1;
                Response.Redirect("/CustomerInformation.aspx?Return=OnePageCheckout.aspx", true);
            }
            else
            {
                Response.Redirect("/CustomerInformation.aspx?Return=OrderSummary.aspx", true);
            }
        }

        protected void lbtnCartEdit_Click(object sender, EventArgs e)
        {
            if (Sites.ToBool(Session["IsOnePageCheckout"]))
            {
                //<a href="/Cart.aspx?Return=OrderSummary.aspx">edit</a>
                Session["SelectedStep"] = 1;
                Response.Redirect("/Cart.aspx?Return=OnePageCheckout.aspx", true);
            }
            else
            {
                Response.Redirect("/Cart.aspx?Return=OrderSummary.aspx", true);
            }
        }

        protected void gvPromotionPostCheckOut_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    //Button btnAdd = e.Row.FindControl("btnAdd") as Button;
                    CheckBox chkAdd = e.Row.FindControl("chkAdd") as CheckBox;
                    Label lblOutOfStock = e.Row.FindControl("lblOutOfStock") as Label;

                    DataRowView drv = e.Row.DataItem as DataRowView;
                    if (drv != null)
                    {
                        TextBox txtQty = (TextBox)e.Row.FindControl("txtQty");
                        int qty = CCommon.ToInteger(txtQty.Text);
                        decimal price = CCommon.ToDecimal(drv.Row["monListPrice"]);
                        decimal total = CCommon.ToDecimal(qty * price);
                        Label lblTotal = (Label)e.Row.FindControl("lblTotal");
                        lblTotal.Text = CCommon.ToString(Session["CurrSymbol"]) + " " + CCommon.ToString(Math.Round(total, 2));

                        TableCell monListPriceCell = e.Row.Cells[5];
                        monListPriceCell.Text = CCommon.ToString(Session["CurrSymbol"]) + " " + CCommon.ToString(Math.Round(price, 2));

                        //hdnCategory.Value = CCommon.ToString(drv.Row["vcCategoryName"]);

                        int _numOnHand = CCommon.ToInteger(drv.Row["numOnHand"]);
                        if (_numOnHand == 0)
                        {
                            //btnAdd.Visible = false;
                            chkAdd.Visible = false;
                            lblOutOfStock.Visible = true;
                        }

                        Image img = (Image)e.Row.FindControl("imgPath");
                        string imgPath = img.ImageUrl;
                        if (!string.IsNullOrEmpty(imgPath))
                        {
                            imgPath = GetImagePath(CCommon.ToString(imgPath));
                            img.ImageUrl = imgPath;
                        }
                        else
                        {
                            img.ImageUrl = "../Images/DefaultProduct.png";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionModule.ExceptionPublish(ex, Sites.ToLong(Session["DomainID"]), Sites.ToLong(Session["UserContactID"]), Sites.ToLong(Session["SiteId"]), Request);
                ShowMessage(ex.ToString(), 1);
            }
        }

        #endregion

        #region Methods

        #region HTML

        private void bindHtml()
        {
            try
            {
                string strUI = GetUserControlTemplate("GuestCheckout.htm");
                /*-------------Contact Information------------------- */

                strUI = strUI.Replace("##SameAsBillingCheckBox##", CCommon.RenderControl(chkShippingAdd));
                strUI = strUI.Replace("##IsResidential##", CCommon.RenderControl(chkResidential));
                strUI = strUI.Replace("##BillingStreet##", CCommon.RenderControl(txtBillStreet));
                strUI = strUI.Replace("##ShippingStreet##", CCommon.RenderControl(txtShipStreet));
                strUI = strUI.Replace("##BillingCity##", CCommon.RenderControl(txtBillCity));
                strUI = strUI.Replace("##ShippingCity##", CCommon.RenderControl(txtShipCity));
                strUI = strUI.Replace("##BillingZipcode##", CCommon.RenderControl(txtBillCode));
                strUI = strUI.Replace("##ShippingZipcode##", CCommon.RenderControl(txtShipCode));
                strUI = strUI.Replace("##BillingCountryDropDown##", CCommon.RenderControl(ddlBillCountry));
                strUI = strUI.Replace("##ShippingCountryDropDown##", CCommon.RenderControl(ddlShipCountry));
                strUI = strUI.Replace("##BillingStateDropDown##", CCommon.RenderControl(ddlBillState));
                strUI = strUI.Replace("##ShippingStateDropDown##", CCommon.RenderControl(ddlShipState));

                if (CCommon.ToLong(hdnOrderPromotionID.Value) > 0 && CCommon.ToLong(hdnDiscountCodeID.Value) > 0)
                {
                    trApplyCouponCode.Visible = false;
                    trUsedCouponCode.Visible = true;
                }
                else
                {
                    trApplyCouponCode.Visible = true;
                    trUsedCouponCode.Visible = false;
                }

                strUI = strUI.Replace("##CouponCodeTextBox##", CCommon.RenderControl(txtCouponCode));
                strUI = strUI.Replace("##ApplyCouponCodeButton##", CCommon.RenderControl(btnCouponCode));
                strUI = strUI.Replace("##RemoveCouponCodeLink##", CCommon.RenderControl(lbCouponCodeRemove));
                strUI = strUI.Replace("##CouponCodeLabel##", lblCouponCode.Text);

                decimal TotalWeight = 0;
                decimal TotalCartAmount = 0;
                int CartItemsCount = 0;
                bool IsAllCartItemsFreeShipping = true;

                strUI = strUI.Replace("##FirstName##", CCommon.RenderControl(txtFirstName));
                strUI = strUI.Replace("##LastName##", CCommon.RenderControl(txtLastName));
                strUI = strUI.Replace("##Phone##", CCommon.RenderControl(txtPhone));
                strUI = strUI.Replace("##Email##", CCommon.RenderControl(txtEmail));
                strUI = strUI.Replace("##FileUpload##", CCommon.RenderControl(FluDocument));
                strUI = strUI.Replace("##BillingAddressName##", CCommon.RenderControl(txtBillAddressName));
                strUI = strUI.Replace("##ShippingAddressName##", CCommon.RenderControl(txtShipAddressName));

                /*-------------Shipping Method------------------- */
                string pattern = "<##FOR(?<Property>(\\b[^>]*?))##>(?<Content>([\\s\\S]*?))<##ENDFOR##>";
                string RepeteTemplate = "";
                Hashtable htProperties;
                DataTable dtItems = default(DataTable);
                if (Regex.IsMatch(strUI, pattern))
                {
                    MatchCollection matches = Regex.Matches(strUI, pattern);
                    if ((matches.Count > 0))
                    {

                        RepeteTemplate = matches[0].Groups["Content"].Value;
                        htProperties = GetPropertiesAsHastable(matches[0].Groups["Property"].Value.Replace("(", "").Replace(")", ""));

                        StringBuilder sb = new StringBuilder();
                        DataSet ds = new DataSet();
                        ds = GetCartItem(isFromGuestCheckout: true);


                        if (ds != null && ds.Tables.Count > 0)
                        {
                            dtItems = ds.Tables[0];
                            if (dtItems.Rows.Count > 0)
                            {
                                string strTemp = string.Empty;
                                int RowsCount = 0;

                                foreach (DataRow dr in dtItems.Rows)
                                {
                                    strTemp = RepeteTemplate;
                                    if (!string.IsNullOrEmpty(CCommon.ToString(dr["vcPathForTImage"])))
                                    {
                                        strTemp = strTemp.Replace("##ItemImage##", GetImagePath(CCommon.ToString(dr["vcPathForTImage"])));
                                    }
                                    else
                                    {
                                        strTemp = strTemp.Replace("##ItemImage##", "../Images/DefaultProduct.png");
                                    }
                                    if (Sites.ToString(dr["vcItemName"]).Length > 100)
                                    {
                                        strTemp = strTemp.Replace("##ItemName##", Sites.ToString(dr["vcItemName"]).Substring(0, Sites.ToInteger(Sites.ToString(dr["vcItemName"]).Length / 2)));
                                    }
                                    else
                                    {
                                        strTemp = strTemp.Replace("##ItemName##", Sites.ToString(dr["vcItemName"]).Substring(0, Sites.ToString(dr["vcItemName"]).Length));
                                    }

                                    strTemp = strTemp.Replace("##ItemUnits##", Sites.ToString(dr["numUnitHour"]));

                                    if (Sites.ToBool(Session["HidePrice"]) == false)
                                    {
                                        strTemp = strTemp.Replace("##ItemPricePerUnit##", string.Format("{0:#,##0.00###}", dr["monPrice"]));
                                        strTemp = strTemp.Replace("##TotalAmount##", string.Format("{0:#,##0.00}", dr["monTotAmount"]));
                                    }
                                    else
                                    {
                                        strTemp = strTemp.Replace("##ItemPricePerUnit##", "NA");
                                        strTemp = strTemp.Replace("##TotalAmount##", "NA");
                                    }

                                    if (Sites.ToBool(dr["bitDiscountType"]))
                                    {
                                        //strTemp = strTemp.Replace("##Discount##", Sites.ToString(Session["CurrSymbol"]) + " " + string.Format("{0:#,##0.00}", dr["fltDiscount"]));
                                        strTemp = strTemp.Replace("##Discount##", string.Format("{0:#,##0.00}", dr["fltDiscount"]));
                                    }
                                    else
                                    {
                                        //strTemp = strTemp.Replace("##Discount##", Sites.ToString(Session["CurrSymbol"]) + " " + string.Format("{0:#,##0.00}", Sites.ToDouble(dr["monTotAmtBefDiscount"]) - Sites.ToDouble(dr["monTotAmount"])));
                                        strTemp = strTemp.Replace("##Discount##", string.Format("{0:#,##0.00}", Sites.ToDouble(dr["monTotAmtBefDiscount"]) - Sites.ToDouble(dr["monTotAmount"])));
                                    }

                                    strTemp = strTemp.Replace("##ItemModelID##", CCommon.ToString(dr["vcModelID"]));
                                    strTemp = strTemp.Replace("##ItemID##", CCommon.ToString(dr["numItemCode"]));
                                    strTemp = strTemp.Replace("##ItemAttributes##", CCommon.ToString(dr["vcAttributes"]));


                                    // strTemp = strTemp.Replace("##ShippingMethod##", Sites.ToString(dr["vcShippingMethod"]));
                                    if (Sites.ToString(dr["bitFreeShipping"]) == "True")
                                    {
                                        TotalWeight = TotalWeight + 0;
                                        TotalCartAmount = TotalCartAmount + 0;
                                        CartItemsCount = CartItemsCount + 0;
                                    }
                                    else // shipping based on shipping rule 
                                    {
                                        TotalWeight = TotalWeight + Sites.ToDecimal(dr["numWeight"]);
                                        TotalCartAmount = TotalCartAmount + Sites.ToDecimal(dr["monTotAmount"]);
                                        CartItemsCount = CartItemsCount + Sites.ToInteger(dr["numUnitHour"]);
                                        IsAllCartItemsFreeShipping = false;
                                    }

                                    if (RowsCount % 2 == 0)
                                        strTemp = strTemp.Replace("##RowClass##", Sites.ToString(htProperties["AlternatingItemClass"]));
                                    else
                                        strTemp = strTemp.Replace("##RowClass##", Sites.ToString(htProperties["ItemClass"]));

                                    sb.Append(strTemp);

                                    RowsCount++;
                                    dtItems.AcceptChanges();
                                }
                            }
                            strUI = Regex.Replace(strUI, pattern, sb.ToString());
                        }
                    }
                }

                string EnterCouponPattern, UsedCouponPattern;
                EnterCouponPattern = "{##EnterCouponSection##}(?<Content>([\\s\\S]*?)){/##EnterCouponSection##}";
                UsedCouponPattern = "{##UsedCouponSection##}(?<Content>([\\s\\S]*?)){/##UsedCouponSection##}";

                if (CCommon.ToLong(hdnOrderPromotionID.Value) > 0 && CCommon.ToLong(hdnDiscountCodeID.Value) > 0)
                {
                    strUI = Sites.RemoveMatchedPattern(strUI, EnterCouponPattern);
                    strUI = Sites.ReplaceMatchedPatternWithContent(strUI, UsedCouponPattern);
                }
                else
                {
                    strUI = Sites.RemoveMatchedPattern(strUI, UsedCouponPattern);
                    strUI = Sites.ReplaceMatchedPatternWithContent(strUI, EnterCouponPattern);
                }


                if (IsOnlySalesInquiry() != 1)
                {
                    if (hdnSelectedShippingCharge.Value == "-1")
                    {
                        bool ShowShippingAmount = false;
                        if (strUI.Contains("##ShowShippingAmountWithMethod##"))
                        {
                            ShowShippingAmount = true;
                            strUI = strUI.Replace("##ShowShippingAmountWithMethod##", "");
                        }

                        GetShippingMethod(ShowShippingAmount, TotalWeight, TotalCartAmount, CartItemsCount, IsAllCartItemsFreeShipping);
                    }
                    else
                    {
                        if (strUI.Contains("##ShowShippingAmountWithMethod##"))
                        {
                            strUI = strUI.Replace("##ShowShippingAmountWithMethod##", "");
                        }
                    }
                }

                decimal Tax = default(decimal);
                decimal SubTotal = default(decimal);
                decimal Discount = default(decimal);

                if (dtItems.Rows.Count > 0)
                {
                    foreach (DataRow dr in dtItems.Rows)
                    {
                        if (Sites.ToLong(dr["numItemCode"]) != Sites.ToLong(Session["ShippingServiceItemID"]) && Sites.ToLong(dr["numItemCode"]) != Sites.ToLong(Session["DiscountServiceItemID"]))
                        {
                            SubTotal += Sites.ToDecimal(dr["monTotAmtBefDiscount"]);
                        }
                    }

                    Discount = Sites.ToDecimal(dtItems.Compute("SUM(monTotalDiscountAmount)", ""));
                    if (dtItems.Select("numItemCode=" + CCommon.ToString(Session["DiscountServiceItemID"])).Length > 0)
                    {
                        Discount = Discount + Sites.ToDecimal(dtItems.Select("numItemCode=" + CCommon.ToString(Session["DiscountServiceItemID"]))[0]["monTotAmount"]) * -1;
                    }

                    Session["TotalDiscount"] = Discount;
                }


                if (Sites.ToBool(Session["HidePrice"]) == false)
                {
                    string strShippingCharge = GetShippingCharge();
                    if (strShippingCharge == "")
                    {
                        strShippingCharge = "0.00";
                    }

                    lblCurrencySymbol.Text = Sites.ToString(Session["CurrSymbol"]);
                    lblSubTotal.Text = string.Format("{0:#,##0.00}", SubTotal);
                    lblDiscount.Text = string.Format("{0:#,##0.00}", Discount);
                    lblTax.Text = string.Format("{0:#,##0.00}", Tax);
                    lblShippingCharge.Text = string.Format("{0:#,##0.00}", strShippingCharge);
                    lblTotal.Text = string.Format("{0:#,##0.00}", SubTotal + Tax - Discount + Convert.ToDecimal(strShippingCharge));

                    Session["TotalAmount"] = SubTotal + Tax + Sites.ToDecimal(strShippingCharge) - Discount;
                    strUI = strUI.Replace("##ShippingMethodModelID##", "divShippingMethod");
                    strUI = strUI.Replace("##GetShippingRatesButton##", CCommon.RenderControl(btnGetShippingRates));
                    strUI = strUI.Replace("##divShippingRates##", "divShippingRates");
                    strUI = strUI.Replace("##CurrencySymbol##", CCommon.RenderControl(lblCurrencySymbol));
                    strUI = strUI.Replace("##SubTotalLabel##", CCommon.RenderControl(lblSubTotal));
                    strUI = strUI.Replace("##DiscountLabel##", CCommon.RenderControl(lblDiscount));
                    strUI = strUI.Replace("##TaxLabel##", CCommon.RenderControl(lblTax));
                    strUI = strUI.Replace("##ShippingChargeLabel##", CCommon.RenderControl(lblShippingCharge));
                    strUI = strUI.Replace("##TotalAmountLabel##", CCommon.RenderControl(lblTotal));

                }
                else
                {
                    strUI = strUI.Replace("##SubTotalLabel##", "NA");
                    strUI = strUI.Replace("##DiscountLabel##", "NA");
                    strUI = strUI.Replace("##TaxLabel##", "NA");
                    strUI = strUI.Replace("##ShippingChargeLabel##", "NA");
                    strUI = strUI.Replace("##TotalAmountLabel##", "NA");
                }

                /*-------------Payment Method------------------- */

                strUI = strUI.Replace("##AddressLink##", CCommon.RenderControl(lbtnEdit));
                strUI = strUI.Replace("##CustomerInformationLink##", CCommon.RenderControl(lbtnCustomerInformationEdit));
                strUI = strUI.Replace("##CartLink##", CCommon.RenderControl(lbtnCartEdit));


                strUI = strUI.Replace("##TotalAmount##", CCommon.RenderControl(lblTotal));
                strUI = strUI.Replace("##Comments##", CCommon.RenderControl(txtComments));

                #region Checkout
                strUI = strUI.Replace("##TotalBalanceDue##", lblBalDue.Text);
                strUI = strUI.Replace("##TotalRemaniningCredit##", lblRemCredit.Text);
                strUI = strUI.Replace("##TotalAmountPastDue##", lblAmtPastDue.Text);

                strUI = strUI.Replace("##CardHolderTextBox##", CCommon.RenderControl(txtCHName));
                strUI = strUI.Replace("##CardNumberTextBox##", CCommon.RenderControl(txtCardNumber));
                strUI = strUI.Replace("##CardTypeDropDownList##", CCommon.RenderControl(ddlCardType));
                strUI = strUI.Replace("##CardExpMonthDropDownList##", CCommon.RenderControl(ddlCardExpMonth));
                strUI = strUI.Replace("##CardExpYearDropDownList##", CCommon.RenderControl(ddlCardExpYear));
                strUI = strUI.Replace("##CVV2TextBox##", CCommon.RenderControl(txtCardCVV2));
                strUI = strUI.Replace("##TotalAmount##", string.Format("{0:#,##0.00}", Sites.ToDecimal(Session["TotalAmount"])));
                strUI = strUI.Replace("##showpaybycreditcard##", Sites.ToString(Session["PayOption"]) == "CreditCard" ? "" : "display:none");

                strUI = strUI.Replace("##PaypalEmail##", CCommon.RenderControl(txtPayPalEmail));
                strUI = strUI.Replace("##IsGoogleCheckoutChecked##", Sites.ToString(Session["PayOption"]) == "GoogleCheckout" ? "checked=\"checked\"" : "");
                strUI = strUI.Replace("##IsPayChecked##", Sites.ToString(Session["PayOption"]) == "CreditCard" ? "checked=\"checked\"" : "");
                strUI = strUI.Replace("##IsPaypalCheckoutChecked##", Sites.ToString(Session["PayOption"]) == "Paypal" ? "checked=\"checked\"" : "");

                #endregion

                BindCustomField(ref strUI);
                pnlCutomizeHtml.Visible = false;
                litCutomizeHtml.Text = strUI;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void AddOrderPromotionDiscountIfAvailable()
        {
            try
            {
                DeleteItemFromCart(Sites.ToLong(Session["DiscountServiceItemID"]));
                double numSubTotal = GetCartSubTotal();

                //ORDER PROMOTION DISCOUNT
                double lngDiscountAmt = 0;
                DataSet dsOrderProm = null;

                //If user has already used coupon code than get promotion based on used coupon code else try to find promotion based on order subtotal
                if (CCommon.ToLong(hdnOrderPromotionID.Value) > 0 && CCommon.ToLong(hdnDiscountCodeID.Value) > 0)
                {
                    PromotionOfferOrder objPromotionOfferOrder = new PromotionOfferOrder();
                    objPromotionOfferOrder.DomainID = Sites.ToLong(Session["DomainID"]);
                    objPromotionOfferOrder.PromotionID = Sites.ToLong(hdnOrderPromotionID.Value);
                    dsOrderProm = objPromotionOfferOrder.GetPromotionOfferOrderByPromotionID();
                }
                else
                {
                    COpportunities objOpportunity = new COpportunities();
                    objOpportunity.DomainID = Sites.ToLong(Session["DomainID"]);
                    objOpportunity.DivisionID = Sites.ToInteger(Session["DivId"]);
                    dsOrderProm = objOpportunity.GetPromotiondRuleForOrder(numSubTotal, siteID: Sites.ToLong(Session["SiteId"]));
                }

                if (dsOrderProm != null && dsOrderProm.Tables.Count >= 2 && dsOrderProm.Tables[0].Rows.Count > 0)
                {
                    int RowCount = dsOrderProm.Tables[1].Rows.Count;
                    long promotionID = CCommon.ToLong(dsOrderProm.Tables[0].Rows[0]["numProID"]);
                    bool isCouponCodeRequired = CCommon.ToBool(dsOrderProm.Tables[0].Rows[0]["bitRequireCouponCode"]);
                    bool isOrderPromotionDisocuntInPercent = CCommon.ToShort(dsOrderProm.Tables[0].Rows[0]["tintDiscountType"]) == 1 ? true : false;

                    hdnOrderPromotionID.Value = CCommon.ToString(promotionID);

                    foreach (DataRow dr in dsOrderProm.Tables[1].Rows)
                    {
                        if (numSubTotal >= CCommon.ToDouble(dr["numOrderAmount"]) && (CCommon.ToInteger(dr["RowNum"]) != RowCount))
                        {
                            lngDiscountAmt = CCommon.ToDouble(dr["fltDiscountValue"].ToString());
                        }
                        else if ((numSubTotal >= CCommon.ToDouble(dr["numOrderAmount"])) && (CCommon.ToInteger(dr["RowNum"]) == RowCount))
                        {
                            lngDiscountAmt = CCommon.ToDouble(dr["fltDiscountValue"].ToString());
                        }
                    }

                    if (lngDiscountAmt > 0)
                    {
                        bool isValidCouponCode = false;

                        if (isCouponCodeRequired)
                        {
                            if (dsOrderProm.Tables.Count >= 3 && dsOrderProm.Tables[2].Rows.Count > 0 && dsOrderProm.Tables[2].Select("numDiscountID=" + hdnDiscountCodeID.Value).Length > 0)
                            {
                                isValidCouponCode = true;
                            }
                            else
                            {
                                isValidCouponCode = false;
                            }
                        }
                        else
                        {
                            isValidCouponCode = true;
                        }

                        if (isValidCouponCode)
                        {
                            if (isOrderPromotionDisocuntInPercent)
                            {
                                lngDiscountAmt = (numSubTotal * lngDiscountAmt) / 100;
                            }

                            AddToCartFromEcomm("", Sites.ToLong(Session["DiscountServiceItemID"]), -lngDiscountAmt, "", true);
                        }
                        else
                        {
                            hdnOrderPromotionID.Value = "0";
                            hdnDiscountCodeID.Value = "0";
                            txtCouponCode.Text = "";
                            lblCouponCode.Text = "";
                            trApplyCouponCode.Visible = true;
                            trUsedCouponCode.Visible = false;
                        }
                    }
                    else
                    {
                        hdnOrderPromotionID.Value = "0";
                        hdnDiscountCodeID.Value = "0";
                        txtCouponCode.Text = "";
                        lblCouponCode.Text = "";
                        trApplyCouponCode.Visible = true;
                        trUsedCouponCode.Visible = false;
                    }
                }
                else
                {
                    hdnOrderPromotionID.Value = "0";
                    hdnDiscountCodeID.Value = "0";
                    txtCouponCode.Text = "";
                    lblCouponCode.Text = "";
                    trApplyCouponCode.Visible = true;
                    trUsedCouponCode.Visible = false;
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        #endregion

        #region Place Order Bill Me , Credit Card , Google Checkout

        private void SaveOpportunity()
        {
            try
            {
                if (Sites.ToLong(Session["OppID"]) == 0)
                {
                    DataTable dteCommercePaymentConfig = new DataTable();
                    CItems objItems = new CItems();

                    objItems.OppId = Sites.ToLong(Session["OppID"]);
                    objItems.DomainID = Sites.ToLong(Session["DomainID"]);
                    objItems.DivisionID = lngDivID;
                    objItems.ContactID = lngContId;
                    objItems.OppName = "-SO-" + DateTime.Now.ToString("MMMM");
                    objItems.Source = "http://" + Request.ServerVariables["SERVER_NAME"].ToString();
                    objItems.CurrencyID = Sites.ToLong(Session["CurrencyID"].ToString());
                    objItems.ShipAddressId = Sites.ToLong(Session["ShipAddress"]);
                    objItems.BillAddressId = Sites.ToLong(Session["BillAddress"]);

                    //As discount added as a line item there is no need to add it in discount column.
                    //objItems.Discount = Sites.ToDecimal(Session["TotalDiscount"]);
                    objItems.SiteID = Sites.ToLong(Session["SiteId"]);
                    objItems.ProID = Sites.ToLong(Session["ProID"]);
                    objItems.DiscountType = 1;

                    if (Sites.ToString(Session["PayOption"]).ToLower() == "salesinquiry")
                    {
                        objItems.OppStatus = 0;
                    }
                    else
                    {
                        objItems.OppStatus = 1;
                    }

                    //here shipping cost pass 0 as it is added as a line Item in cart itself Sites.ToDecimal(Session["ShippingCost"]);
                    objItems.ShipCost = 0;
                    objItems.tintSource = Sites.ToInteger(Session["SiteId"]);
                    objItems.SourceType = 2;
                    objItems.ShippingService = Sites.ToLong(hdnShippingService.Value);
                    string strPaymentMethod = Sites.ToString(Session["PayOption"]).ToLower();
                    switch (strPaymentMethod)
                    {
                        case "creditcard":
                            objItems.PaymentMethod = Sites.ToLong(PaymentMethod.CreditCard);
                            break;
                        case "googlecheckout":
                            objItems.PaymentMethod = Sites.ToLong(PaymentMethod.GoogleCheckout);
                            break;
                        case "paypal":
                            objItems.PaymentMethod = Sites.ToLong(PaymentMethod.Paypal);
                            break;
                        case "salesinquiry":
                            objItems.PaymentMethod = Sites.ToLong(PaymentMethod.SalesInquiry);
                            break;
                        default:
                            break;
                    }
                    objItems.numPartner = Sites.ToLong(Session["partnercode"]);
                    objItems.vcPartenerContact = Sites.ToString(Session["partnercontact"]);
                    objItems.Comments = txtComments.Text.Trim();

                    CAdmin objAdmin = new CAdmin();
                    objAdmin.DivisionID = lngDivID;
                    DataTable dtBillingTerms = objAdmin.GetBillingTerms();

                    if (dtBillingTerms != null && dtBillingTerms.Rows.Count > 0)
                    {
                        objItems.boolBillingTerms = Sites.ToInteger(dtBillingTerms.Rows[0]["tintBillingTerms"]) == 1 ? true : false;
                        objItems.BillingDays = Sites.ToInteger(dtBillingTerms.Rows[0]["numBillingDays"]);
                        objItems.boolInterestType = CCommon.ToInteger(dtBillingTerms.Rows[0]["tintInterestType"]) == 1 ? true : false;
                        objItems.Interest = Convert.ToDecimal(dtBillingTerms.Rows[0]["fltInterest"]);
                    }
                    objItems.OrderPromotionID = CCommon.ToLong(hdnOrderPromotionID.Value);
                    objItems.OrderPromotionDiscountID = CCommon.ToLong(hdnDiscountCodeID.Value);

                    using (var objTransaction = new System.Transactions.TransactionScope(System.Transactions.TransactionScopeOption.Required, new System.Transactions.TransactionOptions() { IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted, Timeout = System.Transactions.TransactionManager.MaximumTimeout }))
                    {
                        objItems.CreateOrderForECommerce();
                        Workflow objWfA = new Workflow();
                        objWfA.DomainID = Sites.ToLong(Session["DomainID"]);
                        objWfA.UserCntID = Sites.ToLong(Session["UserContactID"]);
                        objWfA.RecordID = objItems.OppId;
                        objWfA.SaveWFOrderQueue();
                        Session["OppID"] = objItems.OppId;
                        Session["OppName"] = objItems.OppName; //TO be used in payment gateway for reference invoice id

                        Session["TotalAmount"] = GetCartTotalAmount();


                        Sites objSites = new Sites();
                        objSites.SiteID = Sites.ToLong(Session["SiteId"]);
                        objSites.DomainID = Sites.ToLong(Session["DomainID"]);
                        dteCommercePaymentConfig = objSites.GeteCommercePaymentConfig(objItems.PaymentMethod);

                        objItems.OppId = Sites.ToLong(Session["OppID"]);

                        if (dteCommercePaymentConfig != null && dteCommercePaymentConfig.Rows.Count > 0 && Sites.ToLong(dteCommercePaymentConfig.Rows[0]["numBizDocId"]) > 0)
                        {
                            OppBizDocs objOppBizDocs = new OppBizDocs();
                            objOppBizDocs.OppId = Sites.ToLong(Session["OppID"]);
                            objOppBizDocs.DomainID = Sites.ToLong(Session["DomainID"]);
                            objOppBizDocs.bitPartialShipment = true;
                            objOppBizDocs.OppType = 1;
                            objOppBizDocs.UserCntID = Sites.ToLong(Session["UserContactID"]);
                            objOppBizDocs.vcPONo = "";
                            objOppBizDocs.vcComments = "";

                            if (dteCommercePaymentConfig.Rows.Count > 0)
                            {
                                objOppBizDocs.BizDocStatus = Sites.ToLong(dteCommercePaymentConfig.Rows[0]["numBizDocStatus"]);
                            }

                            objOppBizDocs.ClientTimeZoneOffset = Sites.ToInteger(Session["ClientMachineUTCTimeOffset"]);
                            objOppBizDocs.BizDocId = Sites.ToLong(dteCommercePaymentConfig.Rows[0]["numBizDocId"]);
                            objOppBizDocs.OppBizDocId = 0;

                            CCommon objCommon = new CCommon();
                            objCommon.DomainID = Sites.ToLong(Session["DomainID"]);
                            objCommon.Mode = 33;
                            objCommon.Str = Sites.ToString(dteCommercePaymentConfig.Rows[0]["numBizDocId"]);
                            objOppBizDocs.SequenceId = Sites.ToString(objCommon.GetSingleFieldValue());

                            lngOppBizDocID = objOppBizDocs.SaveBizDoc();

                            if (objOppBizDocs.BizDocStatus > 0 && Sites.ToLong(dteCommercePaymentConfig.Rows[0]["numBizDocId"]) != Sites.ToLong(Session["numSalesInquiryBizDocID"]))
                            {
                                objOppBizDocs.tintMode = 0;
                                objOppBizDocs.OpportunityBizDocStatusChange();
                            }

                            objItems.DomainID = Sites.ToLong(Session["DomainID"]);

                            if (Sites.ToLong(dteCommercePaymentConfig.Rows[0]["numBizDocId"]) == Sites.ToLong(Session["AuthSalesBizDoc"]))
                            {
                                //Create Accounting Journal entry only when set bizdoc is authoritative bizdoc 
                                CreateJournalEntries(objItems);
                            }

                            Workflow objWfA1 = new Workflow();
                            objWfA1.DomainID = Sites.ToLong(Session["DomainID"]);
                            objWfA1.UserCntID = Sites.ToLong(Session["UserContactID"]);
                            objWfA1.RecordID = lngOppBizDocID;
                            objWfA1.SaveWFBizDocQueue();
                        }

                        objItems.OppId = Sites.ToLong(Session["OppID"]);
                        objItems.byteMode = 1;

                        //Here shipping pass as 0 as it is passed as a line item
                        objItems.ShippingCost = 0;
                        objItems.DomainID = Sites.ToLong(Session["DomainID"]);
                        objItems.UserCntID = Sites.ToLong(Session["UserContactID"]);
                        objItems.SiteID = Sites.ToLong(Session["SiteId"]);
                        objItems.ShipAddressId = 2;

                        if (dteCommercePaymentConfig != null && dteCommercePaymentConfig.Rows.Count > 0 && Sites.ToLong(dteCommercePaymentConfig.Rows[0]["numBizDocId"]) > 0)
                        {
                            if (Sites.ToLong(dteCommercePaymentConfig.Rows[0]["numBizDocId"]) > 0 && Sites.ToLong(dteCommercePaymentConfig.Rows[0]["numBizDocId"]) != Sites.ToLong(Session["numSalesInquiryBizDocID"]))
                                objItems.UpdateDealStatus1();
                        }

                        objTransaction.Complete();
                    }

                    btnOrders.Visible = true;

                    SendEmail();
                    if (lngOppBizDocID > 0 && dteCommercePaymentConfig != null && dteCommercePaymentConfig.Rows.Count > 0 && Sites.ToLong(dteCommercePaymentConfig.Rows[0]["numBizDocId"]) > 0)
                    {
                        CAlerts objAlert = new CAlerts();
                        objAlert.SendBizDocAlerts(Sites.ToLong(Session["OppID"]), lngOppBizDocID, Sites.ToLong(dteCommercePaymentConfig.Rows[0]["numBizDocId"]), Sites.ToLong(Session["DomainID"]), CAlerts.enmBizDoc.IsCreated);
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionModule.ExceptionPublish(ex, Sites.ToLong(Session["DomainID"]), Sites.ToLong(Session["UserContactID"]), Sites.ToLong(Session["SiteId"]), Request);
                throw;
            }
        }

        private void SaveCusField()
        {
            try
            {
                //'Saving CustomFields
                DataView dsViews = new DataView(dtCustFld);
                dsViews.RowFilter = "vcFieldType='C'";
                int i = 0;
                DataTable dtCusTable = new DataTable();
                dtCusTable.Columns.Add("FldDTLID");
                dtCusTable.Columns.Add("fld_id");
                dtCusTable.Columns.Add("Value");
                DataRow dRow;
                string strdetails = null;

                if (dsViews.Count > 0)
                {
                    for (i = 0; i <= dsViews.Count - 1; i++)
                    {
                        if (plhFormControls.FindControl(dsViews[i]["vcFormFieldName"].ToString().Trim().Replace(" ", "") + "_" + dsViews[i]["vcFieldType"].ToString()) != null)
                        {
                            dRow = dtCusTable.NewRow();
                            dRow["FldDTLID"] = 0;
                            dRow["fld_id"] = dsViews[i]["numFormFieldId"].ToString().Replace("C", "");
                            dRow["Value"] = GetCustFldValue(dsViews[i]["vcAssociatedControlType"].ToString(), dsViews[i]["vcFormFieldName"].ToString().Trim().Replace(" ", "") + "_" + dsViews[i]["vcFieldType"].ToString());
                            dtCusTable.Rows.Add(dRow);
                        }
                    }
                    dtCusTable.TableName = "Table";
                    DataSet ds = new DataSet();
                    ds.Tables.Add(dtCusTable.Copy());
                    strdetails = ds.GetXml();
                    ds.Tables.Remove(ds.Tables[0]);

                    CustomFields ObjCusfld = new CustomFields();
                    ObjCusfld.strDetails = strdetails;
                    ObjCusfld.RecordId = Sites.ToLong(Session["OppID"]);
                    ObjCusfld.locId = 2;
                    ObjCusfld.SaveCustomFldsByRecId();
                }


            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void MakeDepositEntry(long DivisionId, decimal Amount, DateTime OrderDate, long lngTransactionHistoryID, string Reference, long PaymentMethod, bool IsAuthoritative)
        {
            try
            {
                UndepositedFundAccountID = ChartOfAccounting.GetDefaultAccount("UF", Sites.ToLong(Session["DomainID"])); //Undeposited Fund
                long lngDepositeID, lngJournalID;
                string Description = "";
                MakeDeposit objMakeDeposit = new MakeDeposit();

                objMakeDeposit.DivisionId = Sites.ToInteger(lngDivID);
                objMakeDeposit.Entry_Date = System.DateTime.Now;
                objMakeDeposit.TransactionHistoryID = lngTransactionHistoryID;
                objMakeDeposit.Reference = Sites.ToString(Session["OppName"]);
                objMakeDeposit.Memo = Sites.ToString(Session["OppName"]);
                objMakeDeposit.PaymentMethod = PaymentMethod;
                objMakeDeposit.DepositeToType = 2;// (radDepositeTo.Checked ? 1 : 2);
                //'.numAmount = Replace(lblDepositTotalAmount.Text, ",", "")
                objMakeDeposit.numAmount = Sites.ToDecimal(Sites.ToString(Session["TotalAmount"]).Replace(",", ""));
                objMakeDeposit.AccountId = Sites.ToInteger(UndepositedFundAccountID);

                objMakeDeposit.RecurringId = 0;
                objMakeDeposit.DepositId = 0;
                objMakeDeposit.UserCntID = Sites.ToLong(Session["UserContactID"]);

                objMakeDeposit.DomainID = Sites.ToLong(Session["DomainId"]);

                if (IsAuthoritative)//if (lngOppBizDocID > 0 && objItems.BizDocID == Sites.ToLong(Session["AuthSalesBizDoc"]))
                {
                    DataSet ds = new DataSet();
                    DataTable dt = new DataTable();
                    DataTable dtDeposit = new DataTable();
                    CCommon.AddColumnsToDataTable(ref dtDeposit, "numDepositeDetailID,numOppBizDocsID,numOppID,monAmountPaid");
                    DataRow dr = dtDeposit.NewRow();
                    dr["numDepositeDetailID"] = 0;
                    dr["numOppBizDocsID"] = lngOppBizDocID;
                    dr["numOppID"] = Sites.ToLong(Session["OppID"]);
                    dr["monAmountPaid"] = Amount;
                    dtDeposit.Rows.Add(dr);
                    ds.Tables.Add(dtDeposit.Copy());
                    ds.Tables[0].TableName = "Item";
                    objMakeDeposit.StrItems = ds.GetXml();
                }
                else
                {
                    objMakeDeposit.StrItems = "";
                }
                objMakeDeposit.Mode = 0;
                objMakeDeposit.DepositePage = 2;
                lngDepositeID = 0;
                lngDepositeID = objMakeDeposit.SaveDataToMakeDepositDetails();

                lngJournalID = SaveDataToHeader(Sites.ToLong(Session["DomainID"]), Sites.ToLong(Session["UserContactID"]), OrderDate, Description, Amount, Sites.ToLong(Session["OppID"]), UndepositedFundAccountID, lngDepositeID, 0);

                SaveDataToGeneralJournalDetailsForCashAndChecks(Sites.ToLong(Session["DomainID"]), lngDepositeID, lngJournalID, Amount, DivisionId, UndepositedFundAccountID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void SaveDataToGeneralJournalDetailsForCashAndChecks(long DomainID, long lngDepositeID, long lngJournalID, decimal p_Amount, long DivisionId, long UndepositedFundAccountID)
        {

            try
            {
                JournalEntryCollection objJEList = new JournalEntryCollection();
                JournalEntryNew objJE = new JournalEntryNew();
                //Debit : Undeposit Fund (Amount)
                objJE = new JournalEntryNew();

                objJE.TransactionId = 0;
                objJE.DebitAmt = p_Amount;
                objJE.CreditAmt = 0;
                objJE.ChartAcntId = UndepositedFundAccountID;
                objJE.Description = "Amount Paid (" + p_Amount.ToString() + ")  To Undeposited Fund";
                objJE.CustomerId = DivisionId;
                objJE.MainDeposit = Sites.ToBool(1);
                objJE.MainCheck = Sites.ToBool(0);
                objJE.MainCashCredit = Sites.ToBool(0);
                objJE.OppitemtCode = 0;
                objJE.BizDocItems = "";
                objJE.Reference = "";
                objJE.PaymentMethod = 0;
                objJE.Reconcile = false;
                objJE.CurrencyID = 0;
                objJE.FltExchangeRate = 0;
                objJE.TaxItemID = 0;
                objJE.BizDocsPaymentDetailsId = 0;
                objJE.ContactID = 0;
                objJE.ItemID = 0;
                objJE.ProjectID = 0;
                objJE.ClassID = 0;
                objJE.CommissionID = 0;
                objJE.ReconcileID = 0;
                objJE.Cleared = Sites.ToBool(0);
                objJE.ReferenceType = CCommon.ToShort(enmReferenceType.DepositHeader);
                objJE.ReferenceID = lngDepositeID;

                objJEList.Add(objJE);

                //For Each dr As DataRow In dtItems.Rows
                //Credit: Customer A/R With (Amount)
                OppBizDocs objOppBizDocs = new OppBizDocs();

                long lngCustomerARAccount = objOppBizDocs.ValidateCustomerAR_APAccounts("AR", DomainID, DivisionId);

                objJE = new JournalEntryNew();

                objJE.TransactionId = 0;
                objJE.DebitAmt = 0;
                objJE.CreditAmt = p_Amount;
                objJE.ChartAcntId = lngCustomerARAccount;
                objJE.Description = "Credit Customer's AR account";
                objJE.CustomerId = DivisionId;
                objJE.MainDeposit = Sites.ToBool(0);
                objJE.MainCheck = Sites.ToBool(0);
                objJE.MainCashCredit = Sites.ToBool(0);
                objJE.OppitemtCode = 0;
                objJE.BizDocItems = "";
                objJE.Reference = "";
                objJE.PaymentMethod = 0;
                objJE.Reconcile = false;
                objJE.CurrencyID = 0;
                objJE.FltExchangeRate = 0;
                objJE.TaxItemID = 0;
                objJE.BizDocsPaymentDetailsId = 0;
                objJE.ContactID = 0;
                objJE.ItemID = 0;
                objJE.ProjectID = 0;
                objJE.ClassID = 0;
                objJE.CommissionID = 0;
                objJE.ReconcileID = 0;
                objJE.Cleared = Sites.ToBool(0);
                objJE.ReferenceType = CCommon.ToShort(enmReferenceType.DepositDetail);
                objJE.ReferenceID = 0;

                objJEList.Add(objJE);

                objJEList.Save(JournalEntryCollection.JournalMode.DeleteUpdateInsert, lngJournalID, DomainID);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private long SaveDataToHeader(long DomainID, long UserContactID, DateTime OrderDate, string Description, decimal Amount, long OppID, long UndepositedFundAccountID, long lngDepositeID, long lngOppBizDocIDParameter)
        {
            try
            {
                JournalEntryHeader objJEHeader = new JournalEntryHeader();
                long lngJournalID = 0;
                {
                    objJEHeader.JournalId = 0;
                    objJEHeader.RecurringId = 0;
                    objJEHeader.EntryDate = OrderDate;
                    objJEHeader.Description = Description;
                    objJEHeader.Amount = Amount;
                    objJEHeader.CheckId = 0;
                    objJEHeader.CashCreditCardId = 0;
                    objJEHeader.ChartAcntId = 0;
                    objJEHeader.OppId = OppID;
                    objJEHeader.OppBizDocsId = lngOppBizDocIDParameter;
                    objJEHeader.DepositId = lngDepositeID;
                    objJEHeader.BizDocsPaymentDetId = 0;
                    objJEHeader.IsOpeningBalance = Sites.ToBool(0);
                    objJEHeader.LastRecurringDate = System.DateTime.Now;
                    objJEHeader.NoTransactions = 0;
                    objJEHeader.CategoryHDRID = 0;
                    objJEHeader.ReturnID = 0;
                    objJEHeader.CheckHeaderID = 0;
                    objJEHeader.BillID = 0;
                    objJEHeader.BillPaymentID = 0;
                    objJEHeader.UserCntID = UserContactID;
                    objJEHeader.DomainID = DomainID;
                }
                lngJournalID = objJEHeader.Save();
                return lngJournalID;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void CreateJournalEntries(CItems objItems)
        {
            try
            {
                UndepositedFundAccountID = ChartOfAccounting.GetDefaultAccount("UF", Sites.ToLong(Session["DomainID"])); //Undeposited Fund

                DataSet ds = new DataSet();
                DataTable dtOppBiDocItems;
                OppBizDocs objOppBizDocs = new OppBizDocs();
                objOppBizDocs.OppId = objItems.OppId;
                objOppBizDocs.OppBizDocId = lngOppBizDocID;
                objOppBizDocs.DomainID = Sites.ToLong(Session["DomainID"]);
                objOppBizDocs.UserCntID = lngContId;
                ds = objOppBizDocs.GetOppInItemsForAuthorizativeAccounting();
                dtOppBiDocItems = ds.Tables[0];

                CalculateDealAmount objCalculateDealAmount = new CalculateDealAmount();
                objCalculateDealAmount.CalculateDealAmount(objItems.OppId, lngOppBizDocID, 1, Sites.ToLong(Session["DomainID"]), dtOppBiDocItems, false);


                //'---------------------------------------------------------------------------------
                long JournalId = SaveDataToHeader(objItems.DomainID, objItems.UserCntID, System.DateTime.Now, objItems.Description, objItems.Amount, objItems.OppId, UndepositedFundAccountID, 0, lngOppBizDocID);
                //SaveDataToHeader(objCalculateDealAmount.GrandTotal);

                JournalEntry objJournalEntries = new JournalEntry();
                objJournalEntries.SaveJournalEntriesSalesNew(objItems.OppId, Sites.ToLong(Session["DomainID"]), dtOppBiDocItems, JournalId, lngOppBizDocID, objCalculateDealAmount.GrandTotal, objCalculateDealAmount.TotalDiscount, objCalculateDealAmount.ShippingAmount, objCalculateDealAmount.TotalLateCharges, lngDivID, Sites.ToLong(ds.Tables[1].Rows[0]["numCurrencyID"]), Sites.ToDouble(ds.Tables[1].Rows[0]["fltExchangeRate"]), 0, 0);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region Shipping and Tax Calculations

        private string GetShippingCharge()
        {
            try
            {
                if (hdnSelectedShippingCharge.Value == "0" && hdnIsStaticShippingCharge.Value == "1")
                {
                    return string.Format("{0:#,##0.00}", Sites.ToDecimal(hdnStaticShippingCharge.Value));
                }
                else if (hdnSelectedShippingCharge.Value != "-1")
                {
                    string[] strShippingCharge = hdnSelectedShippingCharge.Value.Split('~');
                    if (strShippingCharge.Length > 1)
                    {
                        return string.Format("{0:#,##0.00}", Sites.ToDecimal(strShippingCharge[1]));
                    }
                }

                return "0.00";
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        private string GetShippingMethodName()
        {
            try
            {
                if (hdnSelectedShippingCharge.Value == "0" && hdnIsStaticShippingCharge.Value == "1")
                {
                    return "Shipping Charge";
                }
                else if (hdnSelectedShippingCharge.Value != "-1")
                {
                    string[] strShippingCharge = Sites.ToString(hdnSelectedShippingCharge.Value).Split('~');
                    if (strShippingCharge.Length >= 5)
                    {
                        if (strShippingCharge.Length == 6)
                        {
                            hdnShippingService.Value = Sites.ToString(Sites.ToLong(strShippingCharge[5]));
                        }

                        string[] strShippingName = strShippingCharge[4].Split('-');
                        if (strShippingName.Length == 2)
                        {
                            return strShippingName[0];
                        }
                    }
                }

                return "";
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        private void GetShippingMethod(Boolean ShowShippingAmount, decimal TotalWeight, decimal TotalCartAmount, int CartItemsCount, bool IsAllCartItemsFreeShipping)
        {
            try
            {
                string zipCode = txtShipCode.Text.Trim();
                long countryID = Sites.ToLong(ddlShipCountry.SelectedValue);
                long stateID = Sites.ToLong(hdnShipState.Value);

                string strItemCodes = "";


                DataSet ds = new DataSet();
                ds = GetCartItem();

                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        strItemCodes = strItemCodes + (strItemCodes.Length > 0 ? "," + CCommon.ToString(dr["numItemCode"]) : CCommon.ToString(dr["numItemCode"]));
                    }
                }

                ShippingRule objShippingRule = new ShippingRule();
                objShippingRule.DomainID = Sites.ToLong(Session["DomainID"]);
                objShippingRule.SiteID = Sites.ToInteger(Session["SiteId"]);
                Tuple<bool, string, double, DataTable> tupleShippingPromotion = objShippingRule.GetShippingRule(zipCode, stateID, countryID, CCommon.ToInteger(Session["DivId"]), CCommon.ToLong(Session["WareHouseID"]), GetCartSubTotal(), strItemCodes);

                if (tupleShippingPromotion.Item1)
                {
                    hdnIsStaticShippingCharge.Value = "1";
                    hdnStaticShippingCharge.Value = CCommon.ToString(tupleShippingPromotion.Item3);
                    hdnSelectedShippingCharge.Value = "0";
                }
                else
                {
                    hdnIsStaticShippingCharge.Value = "0";
                    hdnStaticShippingCharge.Value = "0";
                    hdnSelectedShippingCharge.Value = "-1";
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private int GetTaxModeForShippingRule()
        {
            try
            {
                DataTable dtTable = null;
                CContacts objContact = new CContacts();
                objContact.DomainID = Sites.ToLong(Session["DomainID"]);
                objContact.AddressID = Sites.ToLong(Session["ShipAddress"]);
                objContact.byteMode = 2;
                dtTable = objContact.GetAddressDetail();

                ShippingRule objRule = new ShippingRule();
                objRule.ItemID = 3;
                objRule.DomainID = Sites.ToLong(Session["DomainID"]);
                objRule.SiteID = Sites.ToLong(Session["SiteID"]);
                objRule.DivisionID = Sites.ToLong(Session["DivisionID"]);
                //objRule.RelationshipID = Sites.ToLong(Session["RelationShip"]);
                //objRule.ProfileID = Sites.ToLong(Session["Profile"]);


                if (dtTable.Rows.Count > 0)
                {
                    objRule.ZipCode = Sites.ToString(dtTable.Rows[0]["vcPostalCode"]);
                    objRule.CountryID = Sites.ToLong(dtTable.Rows[0]["numCountry"]);
                    objRule.StateID = Sites.ToLong(dtTable.Rows[0]["numState"]);
                }
                //Get  Shipping Rule Data By numRuleID ...Byte mode 2 is for getting single record by numRuleID
                DataTable dtShippingMethod = new DataTable();
                dtShippingMethod = objRule.GetShippingMethodForItem1();

                if (dtShippingMethod.Rows.Count > 0)
                {
                    return Sites.ToInteger(dtShippingMethod.Rows[0]["tintTaxMode"]);
                }

                return 0;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        private void CreateShippingReport()
        {
            try
            {
                DataTable dt = default(DataTable);
                DataSet ds;
                string strServiceType = "";
                long lngShipingCompany = 0;

                ds = GetCartItem(isFromGuestCheckout: true); //(DataSet)Session["Data"];
                dt = ds.Tables[0];
                OppBizDocs objOppBizDocs = new OppBizDocs();

                DataTable dtFields = new DataTable();
                dtFields.Columns.Add("numItemCode");
                dtFields.Columns.Add("tintServiceType");
                dtFields.Columns.Add("dtDeliveryDate");
                dtFields.Columns.Add("monShippingRate");
                dtFields.Columns.Add("fltTotalWeight");
                dtFields.Columns.Add("intNoOfBox");
                dtFields.Columns.Add("fltHeight");
                dtFields.Columns.Add("fltWidth");
                dtFields.Columns.Add("fltLength");
                dtFields.Columns.Add("numOppBizDocItemID");
                dtFields.TableName = "Items";

                //Create shipping report
                foreach (DataRow dr in dt.Rows)
                {
                    if (Sites.ToLong(dr["numServiceTypeID"]) > 0)
                    {
                        DataRow dtRow = dtFields.NewRow();
                        dtRow["numItemCode"] = dr["numItemCode"].ToString();
                        dtRow["tintServiceType"] = dr["tintServiceType"].ToString();
                        dtRow["dtDeliveryDate"] = DateTime.Parse(dr["dtDeliveryDate"].ToString()).ToString("yyyy-MM-dd HH:mm:ss.000");
                        dtRow["monShippingRate"] = dr["decShippingCharge"].ToString();
                        dtRow["fltTotalWeight"] = dr["numWeight"].ToString();
                        dtRow["intNoOfBox"] = dr["numUnitHour"].ToString();
                        dtRow["fltHeight"] = dr["numHeight"].ToString();
                        dtRow["fltWidth"] = dr["numWidth"].ToString();
                        dtRow["fltLength"] = dr["numLength"].ToString();
                        dtRow["numOppBizDocItemID"] = dr["numOppItemCode"].ToString();

                        dtFields.Rows.Add(dtRow);
                        if (Sites.ToInteger(dr["tintServiceType"]) > 0)
                            strServiceType = Sites.ToString(dr["tintServiceType"]);
                        lngShipingCompany = Sites.ToLong(dr["numShippingCompany"]);
                    }
                }

                //Need to put some mechanism so that custom shipping method also can be tracked through shipping report 
                if (Sites.ToInteger(strServiceType) == 0)
                    return;

                DataTable dtTable = null;
                CContacts objContact = new CContacts();
                objContact.DomainID = Sites.ToLong(Session["DomainID"]);
                objContact.AddressID = Sites.ToLong(Session["ShipAddress"]);
                objContact.byteMode = 1;
                dtTable = objContact.GetAddressDetail();


                //Get default warehouse address
                CItems objItem = new CItems();
                DataTable dtWarehouse = null;
                objItem.WarehouseID = Sites.ToLong(HttpContext.Current.Session["DefaultWareHouseID"]);
                objItem.DomainID = Sites.ToLong(HttpContext.Current.Session["DomainID"]);
                dtWarehouse = objItem.GetWareHouses();


                objOppBizDocs.DomainID = Sites.ToLong(Session["DomainID"]);
                objOppBizDocs.OppBizDocId = lngOppBizDocID;
                objOppBizDocs.ShipCompany = lngShipingCompany;
                objOppBizDocs.Value2 = strServiceType;
                objOppBizDocs.FromState = Sites.ToString(dtWarehouse.Rows[0]["numWState"]);
                objOppBizDocs.FromZip = Sites.ToString(dtWarehouse.Rows[0]["vcWPincode"]);
                objOppBizDocs.FromCountry = Sites.ToString(dtWarehouse.Rows[0]["numWCountry"]);
                objOppBizDocs.ToState = Sites.ToString(dtTable.Rows[0]["numState"]);
                objOppBizDocs.ToZip = Sites.ToString(dtTable.Rows[0]["vcPostalCode"]); ;
                objOppBizDocs.ToCountry = Sites.ToString(dtTable.Rows[0]["numCountry"]);


                objOppBizDocs.UserCntID = lngContId;
                DataSet ds1 = new DataSet();
                ds1.Tables.Add(dtFields.Copy());
                objOppBizDocs.strText = ds1.GetXml();
                objOppBizDocs.byteMode = 1;//to update numOppBizDocItemID
                objOppBizDocs.ShippingReportId = objOppBizDocs.ManageShippingReport();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //Get cart total amount after deducting tax amount . kishan
        private decimal GetCartTotalAmount()
        {
            try
            {
                DataSet ds;
                ds = GetCartItem(isFromGuestCheckout: true); //(DataSet)Session["Data"];
                decimal TotalAmount = 0;
                decimal Tax = 0;

                if (ds.Tables.Count > 0)
                {
                    DataTable dt = new DataTable();
                    dt = ds.Tables[0];

                    if (dt.Rows.Count > 0)
                    {
                        TotalAmount = Sites.ToDecimal(dt.Compute("SUM(monTotAmount)", ""));
                    }
                }

                if (TotalAmount > 0)
                {
                    Tax = GetTax();
                }

                if (Tax > 0)
                {
                    TotalAmount = TotalAmount + Tax;
                }
                return TotalAmount;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private decimal GetTax()
        {
            try
            {
                DataTable dtTable = null;
                CContacts objContact = new CContacts();
                objContact.DomainID = Sites.ToLong(Session["DomainID"]);
                if (CCommon.ToShort(Session["BaseTaxOn"]) == 1)
                {
                    objContact.AddressID = Sites.ToLong(Session["BillAddress"]);
                }
                else if (CCommon.ToShort(Session["BaseTaxOn"]) == 2)
                {
                    objContact.AddressID = Sites.ToLong(Session["ShipAddress"]);
                }
                else
                {
                    objContact.AddressID = 0;
                }

                objContact.byteMode = 1;
                dtTable = objContact.GetAddressDetail();

                decimal Tax = default(decimal);
                if (dtTable.Rows.Count > 0 && dcTotalTax <= 0)//Check because in on one page check out it throws Error ...
                {
                    Tax = GetTaxForCartItems(Sites.ToLong(Session["DomainID"]), 0, Sites.ToInteger(Session["BaseTaxCalcOn"]), Convert.ToInt16(Session["BaseTaxOnArea"]), Sites.ToInteger(dtTable.Rows[0]["numCountry"]), Sites.ToInteger(dtTable.Rows[0]["numState"]), Sites.ToString(dtTable.Rows[0]["vcCity"]), Sites.ToString(dtTable.Rows[0]["vcPostalCode"]), Sites.ToLong(Session["UserContactID"]));
                }
                else if (dcTotalTax > 0)
                {
                    Tax = dcTotalTax;
                }
                else
                {
                    Tax = 0;
                }

                dcTotalTax = Tax;
                return Tax;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        #endregion

        #region BizDoc and Custom Fields

        private void MakeDepositeEntries()
        {
            try
            {
                CItems objItems = new CItems();

                string strPaymentMethod = Sites.ToString(Session["PayOption"]).ToLower();
                switch (strPaymentMethod)
                {
                    case "creditcard":
                        objItems.PaymentMethod = Sites.ToLong(PaymentMethod.CreditCard);
                        break;
                    case "googlecheckout":
                        objItems.PaymentMethod = Sites.ToLong(PaymentMethod.GoogleCheckout);
                        break;
                    case "paypal":
                        objItems.PaymentMethod = Sites.ToLong(PaymentMethod.Paypal);
                        break;
                    case "salesinquiry":
                        objItems.PaymentMethod = Sites.ToLong(PaymentMethod.SalesInquiry);
                        break;
                    default:
                        break;
                }

                Sites objSites = new Sites();
                objSites.SiteID = Sites.ToLong(Session["SiteId"]);
                objSites.DomainID = Sites.ToLong(Session["DomainID"]);
                DataTable dteCommercePaymentConfig = objSites.GeteCommercePaymentConfig(objItems.PaymentMethod);

                if (dteCommercePaymentConfig != null && dteCommercePaymentConfig.Rows.Count > 0 && Sites.ToLong(dteCommercePaymentConfig.Rows[0]["numBizDocId"]) > 0)
                {
                    if (Sites.ToLong(dteCommercePaymentConfig.Rows[0]["numBizDocId"]) == Sites.ToLong(Session["AuthSalesBizDoc"]))
                    {
                        if (objItems.PaymentMethod == Sites.ToLong(PaymentMethod.CreditCard) || objItems.PaymentMethod == Sites.ToLong(PaymentMethod.GoogleCheckout) || objItems.PaymentMethod == Sites.ToLong(PaymentMethod.Paypal))
                            MakeDepositEntry(lngDivID, Sites.ToDecimal(Sites.ToString(Session["TotalAmount"]).Replace(",", "")), System.DateTime.Now, lngTransactionHistoryID, Sites.ToString(Session["OppName"]), objItems.PaymentMethod, true);
                    }
                    else
                    {
                        if (objItems.PaymentMethod == Sites.ToLong(PaymentMethod.CreditCard) || objItems.PaymentMethod == Sites.ToLong(PaymentMethod.GoogleCheckout) || objItems.PaymentMethod == Sites.ToLong(PaymentMethod.Paypal))
                            MakeDepositEntry(lngDivID, Sites.ToDecimal(Sites.ToString(Session["TotalAmount"]).Replace(",", "")), System.DateTime.Now, lngTransactionHistoryID, Sites.ToString(Session["OppName"]), objItems.PaymentMethod, false);
                    }
                }
                else
                {
                    if (objItems.PaymentMethod == Sites.ToLong(PaymentMethod.CreditCard) || objItems.PaymentMethod == Sites.ToLong(PaymentMethod.GoogleCheckout) || objItems.PaymentMethod == Sites.ToLong(PaymentMethod.Paypal))
                        MakeDepositEntry(lngDivID, Sites.ToDecimal(Sites.ToString(Session["TotalAmount"]).Replace(",", "")), System.DateTime.Now, lngTransactionHistoryID, Sites.ToString(Session["OppName"]), objItems.PaymentMethod, false);
                }
            }
            catch (Exception ex)
            {
                ExceptionModule.ExceptionPublish(ex, Sites.ToLong(Session["DomainID"]), Sites.ToLong(Session["UserContactID"]), Sites.ToLong(Session["SiteId"]), Request);
                throw;
            }
        }

        private void UpdateAmountPaid(string ReturnTransactionID)
        {
            if (Sites.ToDecimal(Session["TotalAmount"]) > 0)
            {
                try
                {
                    OppInvoice objOppInvoice = new OppInvoice();
                    objOppInvoice.AmtPaid = Sites.ToDecimal(Session["TotalAmount"]);
                    objOppInvoice.UserCntID = Sites.ToLong(Session["UserContactID"]);
                    objOppInvoice.OppBizDocId = lngOppBizDocID;
                    objOppInvoice.OppId = Sites.ToLong(Session["OppID"]);
                    objOppInvoice.PaymentMethod = 1;//credit card
                    objOppInvoice.BizDocsPaymentDetId = 0;
                    objOppInvoice.CardTypeID = lngCardTypeID;
                    objOppInvoice.Reference = ReturnTransactionID;
                    objOppInvoice.IntegratedToAcnt = false;
                    objOppInvoice.Memo = "";
                    objOppInvoice.DomainID = Sites.ToInteger(Session["DomainID"]);
                    objOppInvoice.DeferredIncomeStartDate = System.DateTime.UtcNow;
                    objOppInvoice.IsCardAutorized = Sites.ToBool(Application["CreditCardAuthOnly"]) == true ? true : false;
                    objOppInvoice.IsAmountCaptured = false;
                    objOppInvoice.UpdateAmountPaid();
                }
                catch (Exception ex)
                {
                    Response.Write(ex);
                }
            }
        }

        private void BindCustomField(ref string strUI)
        {
            try
            {
                FormConfigWizard objFormWizard = new FormConfigWizard();
                objFormWizard.DomainID = Sites.ToLong(Session["DomainID"]);
                objFormWizard.LocationIds = "2"; //sales opportunity 
                dtCustFld = objFormWizard.GetCustomFormFields();
                GenerateGenericFormControls.DomainID = Sites.ToLong(Session["DomainID"]);

                string FldName = "";
                foreach (DataRow row in dtCustFld.Rows)
                {
                    FldName = "##" + Sites.ToString(row["vcFormFieldName"]).Trim().Replace(" ", "") + "_" + Sites.ToString(row["vcFieldType"]) + "##";
                    if (strUI.Contains(FldName))
                    {
                        Control c = null;
                        c = (Control)GenerateGenericFormControls.getDynamicControlAndData(Sites.ToInteger(Sites.ToString(row["numFormFieldId"]).Replace("R", "").Replace("C", "").Replace("D", "")), row["vcFormFieldName"].ToString().Trim().Replace(" ", "") + "_" + row["vcFieldType"].ToString(), row["vcListItemType"].ToString(), row["vcAssociatedControlType"].ToString(), Sites.ToInteger(row["numListID"]), 0, 0);
                        plhFormControls.Controls.Add(c);
                        strUI = strUI.Replace(FldName, CCommon.RenderControl(c));
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private string GetCustFldValue(string fldType, string fld_Id)
        {
            try
            {
                if (fldType == "TextBox")
                {
                    TextBox txt = default(TextBox);
                    txt = (TextBox)plhFormControls.FindControl(fld_Id);
                    return txt.Text;
                }
                else if (fldType == "SelectBox")
                {
                    DropDownList ddl = default(DropDownList);
                    ddl = (DropDownList)plhFormControls.FindControl(fld_Id);
                    return (string)(string.IsNullOrEmpty(ddl.SelectedItem.Value) ? "0" : ddl.SelectedItem.Value);
                }
                else if (fldType == "TextArea")
                {
                    TextBox txt = default(TextBox);
                    txt = (TextBox)plhFormControls.FindControl(fld_Id);
                    return txt.Text;
                }
                else if (fldType == "CheckBox")
                {
                    CheckBox chk = default(CheckBox);
                    chk = (CheckBox)plhFormControls.FindControl(fld_Id);
                    return Sites.ToString(chk.Checked);
                }
                return "";
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region Payment Method Selection

        private void SaveCustomerCreditCardInfo()
        {
            try
            {
                //if (CCommon.ToBool(Session["SaveCreditCardInfo"]))
                //{
                QueryStringValues objEncryption = new QueryStringValues();
                OppInvoice objOppInvoice = new OppInvoice();

                objOppInvoice.ContactID = Sites.ToLong(Session["UserContactID"]);
                objOppInvoice.CardHolder = objEncryption.Encrypt(txtCHName.Text.Trim());
                objOppInvoice.CardTypeID = Sites.ToLong(ddlCardType.SelectedValue);
                //objOppInvoice.CreditCardNumber = objEncryption.Encrypt(hdnCardNumber.Value.Trim());
                objOppInvoice.CreditCardNumber = objEncryption.Encrypt(Sites.ToString(Session["CardNumber"]));
                objOppInvoice.CVV2 = objEncryption.Encrypt(txtCardCVV2.Text.Trim());
                objOppInvoice.ValidMonth = short.Parse(ddlCardExpMonth.SelectedValue);
                objOppInvoice.ValidYear = short.Parse(ddlCardExpYear.SelectedValue);
                objOppInvoice.UserCntID = Sites.ToLong(Session["UserContactID"]);
                objOppInvoice.IsDefault = true;
                objOppInvoice.AddCustomerCreditCardInfo();
                //}
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void PaymentModeChanged()
        {
            try
            {
                if (hdnPaymentMethod.Value == "CreditCard")
                {
                    //radPay.Visible = true;
                    Session["PayOption"] = "CreditCard";
                }
                else if (hdnPaymentMethod.Value == "GoogleCheckout")
                {
                    Session["PayOption"] = "GoogleCheckout";
                }
                else if (hdnPaymentMethod.Value == "Paypal")
                {
                    Session["PayOption"] = "Paypal";
                }
                else if (hdnPaymentMethod.Value == "SalesInquiry")
                {
                    Session["PayOption"] = "SalesInquiry";
                }
                else if (hdnPaymentMethod.Value == "BillMe")
                {
                    Session["PayOption"] = "BillMe";
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void BindCardType()
        {
            try
            {

                CCommon objCommon = new CCommon();
                objCommon.sb_FillComboFromDB(ref ddlCardType, 120, Sites.ToLong(Session["DomainID"]));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region E-mail

        private void SendEmail()
        {
            try
            {
                Session["SMTPServerIntegration"] = true;
                //Send invoice by mail
                if (Sites.ToString(Session["UserEmail"]).Length > 4)
                {
                    /*Create PDF of Invoice */
                    System.IO.StringWriter sw = new System.IO.StringWriter();
                    Server.Execute("/Invoice.aspx?Show=True&OppID=" + Sites.ToString(Session["OppID"]) + "&RefType=1&Print=0", sw);
                    string htmlCodeToConvert = sw.GetStringBuilder().ToString();
                    sw.Close();

                    string newHtml = "";
                    try
                    {
                        newHtml = htmlCodeToConvert.Substring(htmlCodeToConvert.IndexOf("<body>"), htmlCodeToConvert.IndexOf("</body>") - htmlCodeToConvert.IndexOf("<body>") + 7);
                        newHtml = "<html><head></head>" + newHtml + "</html>";
                        htmlCodeToConvert = newHtml;
                    }
                    catch
                    {

                    }

                    HTMLToPDF objpdf = new HTMLToPDF();
                    string strFileName = objpdf.ConvertHTML2PDF(htmlCodeToConvert, Sites.ToLong(Session["DomainID"]), "http://" + Request.ServerVariables["SERVER_NAME"], false, 1, false);//HttpContext.Current.Request.Url.AbsoluteUri
                    DataRow dr;
                    DataTable dtTable = new DataTable();
                    dtTable.Columns.Add("Filename");
                    dtTable.Columns.Add("FileLocation");
                    dr = dtTable.NewRow();
                    if (Sites.ToString(Session["OppName"]).Length == 0)
                        Session["OppName"] = "Order-" + Sites.ToString(Session["OppID"]);

                    dr["Filename"] = Sites.ToString(Session["OppName"]) + ".pdf";
                    dr["FileLocation"] = CCommon.GetDocumentPhysicalPath(Sites.ToLong(Session["DomainID"])) + strFileName;
                    dtTable.Rows.Add(dr);
                    Session["Attachements"] = dtTable;
                    Email objSendEmail = new Email();

                    objSendEmail.DomainID = Sites.ToLong(Session["DomainID"]);
                    objSendEmail.TemplateCode = "#SYS#ECOMMERCE_BIZDOC_ITEMS";
                    objSendEmail.GetEmailTemplateByCode(objSendEmail.TemplateCode, objSendEmail.DomainID);

                    if (objSendEmail.TemplateBody.Contains("forloop=\"yes\""))
                    {
                        string pattern = "<([A-Za-z][A-Z0-9a-z]*)\\b[^>]*forloop=\"yes\"[^>]*?>[\\s\\S]*?</\\1>";
                        MatchCollection matches = Regex.Matches(objSendEmail.TemplateBody, pattern);

                        if ((matches.Count > 0))
                        {
                            DataTable dt = default(DataTable);
                            DataSet ds;
                            ds = GetCartItem(isFromGuestCheckout: true); //(DataSet)Session["Data"];
                            dt = ds.Tables[0];

                            string strReplace = matches[0].Value;
                            StringBuilder sb = new StringBuilder();
                            string strTemp = string.Empty;
                            foreach (DataRow dr1 in dt.Rows)
                            {
                                strTemp = strReplace;
                                if (Sites.ToString(dr1["vcItemName"]).Length > 100)
                                {
                                    strTemp = strTemp.Replace("##ItemName##", Sites.ToString(dr1["vcItemName"]).Substring(0, Sites.ToInteger(Sites.ToString(dr1["vcItemName"]).Length / 2)));
                                }
                                else
                                {
                                    strTemp = strTemp.Replace("##ItemName##", Sites.ToString(dr1["vcItemName"]).Substring(0, Sites.ToString(dr1["vcItemName"]).Length));
                                }
                                //strTemp = strTemp.Replace("##ItemName##", dr1["vcItemName"].ToString().Substring(0, 30));
                                strTemp = strTemp.Replace("##Price##", dr1["monPrice"].ToString());
                                strTemp = strTemp.Replace("##Unit##", dr1["numUnitHour"].ToString());
                                sb.AppendLine(strTemp);
                            }
                            objSendEmail.TemplateBody = objSendEmail.TemplateBody.Replace(strReplace, sb.ToString());
                        }
                    }

                    objSendEmail.DomainID = Sites.ToLong(Session["DomainID"]);
                    objSendEmail.TemplateCode = "#SYS#ECOMMERCE_SHOPPING_COMPLETED";

                    objSendEmail.ModuleID = 9;
                    objSendEmail.RecordIds = Sites.ToString(Session["OppID"]);
                    objSendEmail.FromEmail = Sites.ToString(Session["DomainAdminEmail"]);
                    objSendEmail.ToEmail = Sites.ToString(Session["UserEmail"]);
                    objSendEmail.IsAttachmentDelete = true;
                    objSendEmail.SendEmailTemplate(objSendEmail.TemplateBody);
                    Session["Attachements"] = null;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region Check Payment method

        private short IsOnlySalesInquiry()
        {
            UserAccess objUserAccess = new UserAccess();
            DataTable dtPaymentGateWay = new DataTable();
            objUserAccess.DomainID = Sites.ToLong(Session["DomainID"]);
            objUserAccess.byteMode = 1;
            dtPaymentGateWay = objUserAccess.ManageEComPaymentConfig(Sites.ToLong(Session["SiteId"]));
            if (dtPaymentGateWay != null && dtPaymentGateWay.Rows.Count > 0 && dtPaymentGateWay.Select("numPaymentMethodId = " + CCommon.ToInteger(PaymentMethod.SalesInquiry) + " AND bitEnable = 1").Length == 1 && dtPaymentGateWay.Select("bitEnable = 1").Length == 1)
            {
                return 1;
            }
            else if (dtPaymentGateWay != null && dtPaymentGateWay.Rows.Count > 0 && dtPaymentGateWay.Select("numPaymentMethodId = " + CCommon.ToInteger(PaymentMethod.SalesInquiry) + " AND bitEnable = 1").Length == 0 && dtPaymentGateWay.Select("bitEnable = 1").Length > 0)
            {
                return 2;
            }
            else
            {
                return 0;
            }
        }

        #endregion

        #endregion

        #region Custom Properties

        public Boolean isShippingServiceAdded
        {
            get
            {
                DataSet ds = GetCartItem();
                DataTable dtCart = ds.Tables[0];

                if (dtCart == null || dtCart.Rows.Count == 0 || dtCart.Select("numItemCode=" + Sites.ToString(Session["ShippingServiceItemID"])).Length == 0)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
        }

        public Boolean isDiscountServiceAdded
        {
            get
            {
                object o = ViewState["isDiscountServiceAdded"];
                if (o != null)
                {
                    return (Boolean)o;
                }
                else
                {
                    ViewState["isDiscountServiceAdded"] = true;
                    return true;
                }
            }
            set
            {
                ViewState["isDiscountServiceAdded"] = value;
            }
        }

        #endregion

        public void UseExistingUserOrCreateNew()
        {
            BACRM.BusinessLogic.Leads.CLeads objLeads = new BACRM.BusinessLogic.Leads.CLeads();
            CCommon objCommon = new CCommon();
            lngDivID = 0;
            lngContId = 0;
            long lngCMPID = 0;
            DataRow dRow = default(DataRow);
            DataSet ds = new DataSet();
            try
            {
                objLeads.GroupID = 1;
                objLeads.ContactType = 70;
                objLeads.PrimaryContact = true;
                objLeads.DivisionName = "-";
                objLeads.FirstName = txtFirstName.Text;
                objLeads.LastName = txtLastName.Text;
                objLeads.Email = txtEmail.Text;
                objLeads.ComPhone = txtPhone.Text;
                objLeads.ContactPhone = txtPhone.Text;
                objLeads.SStreet = txtShipStreet.Text;
                objLeads.SCity = txtShipCity.Text;
                objLeads.SPostalCode = txtShipCode.Text;
                objLeads.SState = Sites.ToLong(hdnShipState.Value);
                objLeads.SCountry = Sites.ToLong(ddlShipCountry.SelectedValue);

                if (chkShippingAdd.Checked)
                {
                    objLeads.Street = txtShipStreet.Text;
                    objLeads.City = txtShipCity.Text;
                    objLeads.PostalCode = txtShipCode.Text;
                    objLeads.State = Sites.ToLong(hdnShipState.Value);
                    objLeads.Country = Sites.ToLong(ddlShipCountry.SelectedValue);
                }
                else
                {
                    objLeads.Street = txtBillStreet.Text;
                    objLeads.City = txtBillCity.Text;
                    objLeads.PostalCode = txtBillCode.Text;
                    objLeads.State = Sites.ToLong(hdnBillState.Value);
                    objLeads.Country = Sites.ToLong(ddlBillCountry.SelectedValue);
                }

                objLeads.DomainID = Sites.ToLong(Session["DomainID"]);


                objLeads.GetConIDCompIDDivIDFromEmail();
                if (objLeads.ContactID > 0 && objLeads.DivisionID > 0 && objLeads.CompanyID > 0)
                {
                    hdnIsEnforcedSubTotalMeets.Value = "True";
                    Session["TotalAmount"] = GetCartTotalAmount();
                    decimal totalAmount = Convert.ToDecimal(Session["TotalAmount"]);

                    DataTable dtEnforcedMinSubTotal = CheckIfMinOrderAmountRuleMeets(totalAmount, objLeads.DivisionID);
                    if (dtEnforcedMinSubTotal != null && dtEnforcedMinSubTotal.Rows.Count > 0 && CCommon.ToInteger(dtEnforcedMinSubTotal.Rows[0]["returnVal"]) == 0)
                    {
                        decimal subTotal = Convert.ToDecimal(dtEnforcedMinSubTotal.Rows[0]["MinOrderAmount"]);
                        //ScriptManager.RegisterClientScriptBlock(this.Page, Page.GetType(), "MinOrderAmountRuleMeets", "alert('You must spend a minimum of $" + subTotal + " to complete this order.');", true);
                        string radalertscript = "<script language='javascript'>function f(){radalert('You must spend a minimum of $" + subTotal + " to complete this order.', 450, 180); Sys.Application.remove_load(f);}; Sys.Application.add_load(f);</script>";
                        Page.ClientScript.RegisterStartupScript(this.GetType(), "radalert", radalertscript);

                        hdnIsEnforcedSubTotalMeets.Value = "False";
                        return;
                    }
                    UserAccess objNewUserAccess = new UserAccess();
                    DataTable dtUser = default(DataTable);
                    objNewUserAccess.Email = objLeads.Email;
                    objNewUserAccess.Password = "EXISTINGUSERGUESTLOGIN";
                    objNewUserAccess.DomainID = Sites.ToLong(Session["DomainID"]);
                    dtUser = objNewUserAccess.ExtranetLogin();

                    if (dtUser == null || dtUser.Rows.Count == 0 || dtUser.Columns.Count < 14)
                    {
                        lngCMPID = objLeads.CompanyID;
                        lngDivID = objLeads.DivisionID;
                        lngContId = objLeads.ContactID;
                        Session["CompID"] = lngCMPID;
                        Session["DivId"] = lngDivID;
                        Session["UserContactID"] = lngContId;
                        Session["FirstName"] = txtFirstName.Text.Trim();
                        Session["LastName"] = txtLastName.Text.Trim();

                        bool CookieEnabled = false;
                        CookieEnabled = Context.Request.Browser.Cookies;
                        if (CookieEnabled == true)
                        {
                            CCommon objcommon = new CCommon();
                            HttpCookie cookie = default(HttpCookie);
                            cookie = new HttpCookie("companyid", lngDivID.ToString());
                            cookie.Expires = DateTime.Now.AddYears(1);
                            Context.Response.Cookies.Set(cookie);

                            cookie = new HttpCookie("usercontactid", lngContId.ToString());
                            cookie.Expires = DateTime.Now.AddYears(1);
                            Context.Response.Cookies.Set(cookie);
                        }

                        if(objCommon == null){
                            objCommon = new CCommon();
                        }

                        objLeads.Password = objCommon.Encrypt(PasswordGenerator.Generate(10, 10));

                        UserGroups objUserGroups = new UserGroups();
                        objUserGroups.ContactID = lngContId;
                        objUserGroups.DomainID = Sites.ToLong(Session["DomainID"]);
                        objUserGroups.Password = objLeads.Password;
                        objUserGroups.AccessAllowed = 0;
                        objUserGroups.CreateExtranetAccount();
                    }
                    else
                    {
                        lngCMPID = objLeads.CompanyID;
                        lngDivID = objLeads.DivisionID;
                        lngContId = objLeads.ContactID;
                        Session["CompID"] = lngCMPID;
                        Session["DivId"] = lngDivID;
                        Session["UserContactID"] = lngContId;
                        Session["FirstName"] = txtFirstName.Text.Trim();
                        Session["LastName"] = txtLastName.Text.Trim();
                        objLeads.Password = "EXISTINGUSERGUESTLOGIN";
                    }

                    UpdateAddress();
                }
                else
                {
                    Session["TotalAmount"] = GetCartTotalAmount();
                    decimal totalAmount = Convert.ToDecimal(Session["TotalAmount"]);
                    hdnIsEnforcedSubTotalMeets.Value = "True";
                    DataTable dtEnforcedMinSubTotal = CheckIfMinOrderAmountRuleMeets(totalAmount, lngDivID);
                    if (dtEnforcedMinSubTotal != null && dtEnforcedMinSubTotal.Rows.Count > 0 && CCommon.ToInteger(dtEnforcedMinSubTotal.Rows[0]["returnVal"]) == 0)
                    {
                        decimal subTotal = Convert.ToDecimal(dtEnforcedMinSubTotal.Rows[0]["MinOrderAmount"]);
                        //ScriptManager.RegisterClientScriptBlock(this.Page, Page.GetType(), "MinOrderAmountRuleMeets", "alert('You must spend a minimum of $" + subTotal + " to complete this order.');", true);
                        string radalertscript = "<script language='javascript'>function f(){radalert('You must spend a minimum of $" + subTotal + " to complete this order.', 450, 180); Sys.Application.remove_load(f);}; Sys.Application.add_load(f);</script>";
                        Page.ClientScript.RegisterStartupScript(this.GetType(), "radalert", radalertscript);

                        hdnIsEnforcedSubTotalMeets.Value = "False";
                        return;
                    }

                    AutoRoutingRules objAutoRoutRles = new AutoRoutingRules();
                    objAutoRoutRles.DomainID = Sites.ToLong(Session["DomainID"]);
                    objAutoRoutRles.strValues = "";
                    objLeads.UserCntID = objAutoRoutRles.GetRecordOwner();
                    objLeads.CRMType = 1;
                    objLeads.CompanyType = Sites.ToLong(Session["DefaultRelationship"]); //Sites.ToLong(hdCompanyType.Value);
                    objLeads.Profile = Sites.ToLong(Session["DefaultProfile"]);//Sites.ToLong(hdGroupId.Value);
                    objLeads.AccountClass = Sites.ToLong(Session["DefaultClass"]);
                    lngCMPID = objLeads.CreateRecordCompanyInfo();
                    objLeads.CompanyID = lngCMPID;
                    lngDivID = objLeads.CreateRecordDivisionsInfo();
                    objLeads.DivisionID = lngDivID;

                    lngContId = objLeads.CreateRecordAddContactInfo();

                    Session["CompID"] = lngCMPID;
                    //Set the Company Id in a session
                    Session["DivId"] = lngDivID;
                    Session["UserContactID"] = lngContId;
                    Session["FirstName"] = txtFirstName.Text.Trim();
                    Session["LastName"] = txtLastName.Text.Trim();

                    BACRM.BusinessLogic.Prospects.CProspects objProspects = new BACRM.BusinessLogic.Prospects.CProspects();
                    objProspects.DivisionID = lngDivID;
                    objProspects.DomainID = Sites.ToLong(Session["DomainID"]);
                    DataTable dsCmp = objProspects.GetCompanyInfoForEdit();

                    if (dsCmp != null && dsCmp.Rows.Count > 0)
                    {
                        Session["BillAddress"] = CCommon.ToLong(dsCmp.Rows[0]["numBillAddressID"]);
                        Session["ShipAddress"] = CCommon.ToLong(dsCmp.Rows[0]["numShipAddressID"]);

                        if (CCommon.ToLong(Session["BillAddress"]) == 0 || CCommon.ToLong(Session["ShipAddress"]) == 0)
                        {
                            throw new Exception("Billing or Shipping address not found");
                        }
                    }
                    else
                    {
                        throw new Exception("Company not found");
                    }



                    bool CookieEnabled = false;
                    CookieEnabled = Context.Request.Browser.Cookies;
                    if (CookieEnabled == true)
                    {
                        CCommon objcommon = new CCommon();
                        HttpCookie cookie = default(HttpCookie);
                        cookie = new HttpCookie("companyid", lngDivID.ToString());
                        cookie.Expires = DateTime.Now.AddYears(1);
                        Context.Response.Cookies.Set(cookie);

                        cookie = new HttpCookie("usercontactid", lngContId.ToString());
                        cookie.Expires = DateTime.Now.AddYears(1);
                        Context.Response.Cookies.Set(cookie);
                    }

                    if (objCommon == null)
                    {
                        objCommon = new CCommon();
                    }

                    objLeads.Password = objCommon.Encrypt(PasswordGenerator.Generate(10, 10));

                    UserGroups objUserGroups = new UserGroups();
                    objUserGroups.ContactID = lngContId;
                    objUserGroups.DomainID = Sites.ToLong(Session["DomainID"]);
                    objUserGroups.Password = objLeads.Password;
                    objUserGroups.AccessAllowed = 0;
                    objUserGroups.CreateExtranetAccount();
                }




                //if (CCommon.ToBool(Session["EmailStatus"]) == true)
                //{
                //    if (!string.IsNullOrEmpty(objLeads.Email))
                //    {
                //        try
                //        {
                //            //We are explicity setting it true so mails will be sent through SMTP details provided in Domain Details
                //            Session["SMTPServerIntegration"] = "True";

                //            Email objSendEmail = new Email();

                //            objSendEmail.DomainID = Sites.ToLong(Session["DomainID"]);
                //            objSendEmail.TemplateCode = "#SYS#ECOMMERCE_SIGNUP";
                //            objSendEmail.ModuleID = 1;
                //            objSendEmail.RecordIds = Sites.ToString(lnCntID);
                //            objSendEmail.AdditionalMergeFields.Add("PortalPassword", objLeads.Password);
                //            objSendEmail.FromEmail = Sites.ToString(Session["DomainAdminEmail"]);
                //            objSendEmail.ToEmail = "##ContactFirstName## <##ContactEmail##>";
                //            objSendEmail.SendEmailTemplate();

                //        }
                //        catch (Exception ex)
                //        {
                //            throw ex;
                //        }
                //    }
                //}
                //Set all default session after signup as it throws error in invoice page when DateFormat is not found in session
                UserAccess objUserAccess = new UserAccess();
                DataTable dttable = default(DataTable);
                objUserAccess.Email = objLeads.Email;
                objUserAccess.Password = objLeads.Password;
                objUserAccess.DomainID = Sites.ToLong(Session["DomainID"]);
                dttable = objUserAccess.ExtranetLogin();
                if (dttable.Rows[0][0].ToString() == "1")
                {
                    Session["CompID"] = dttable.Rows[0][3];
                    Session["UserContactID"] = dttable.Rows[0][1];
                    Session["ContactId"] = dttable.Rows[0][2];
                    Session["DivId"] = dttable.Rows[0][2];
                    Session["GroupId"] = dttable.Rows[0][4];
                    Session["CRMType"] = dttable.Rows[0][5];
                    Session["DomainID"] = dttable.Rows[0][6];
                    Session["CompName"] = dttable.Rows[0][7];
                    Session["RecOwner"] = dttable.Rows[0][8];
                    Session["RecOwnerName"] = dttable.Rows[0][9];
                    Session["PartnerGroup"] = dttable.Rows[0][10];
                    Session["RelationShip"] = dttable.Rows[0][11];
                    //Session["HomePage"] = dttable.Rows[0][12];
                    Session["UserEmail"] = dttable.Rows[0][13];
                    Session["DateFormat"] = dttable.Rows[0]["vcDateFormat"];
                    Session["ContactName"] = dttable.Rows[0][14];
                    Session["PartnerAccess"] = dttable.Rows[0][15];
                    Session["PagingRows"] = dttable.Rows[0]["tintCustomPagingRows"];
                    Session["DefCountry"] = dttable.Rows[0]["numDefCountry"];
                    Session["PopulateUserCriteria"] = dttable.Rows[0]["tintAssignToCriteria"];
                    Session["EnableIntMedPage"] = (bool.Parse(dttable.Rows[0]["bitIntmedPage"].ToString()) == true ? 1 : 0);
                    Session["DomainName"] = dttable.Rows[0]["vcDomainName"];
                    //' End date
                    Session["Internal"] = 1;
                    Session["Username"] = objUserAccess.Email;

                    Session["Password"] = objCommon.Encrypt(objUserAccess.Password);
                    Sites objSite = new Sites();
                    objSite.UpdateDefaultWarehouseForLoggedInUser(Sites.ToLong(Session["DivId"]));
                }

                if (Request.Cookies["Cart"] != null && Sites.ToLong(Session["OppID"]) == 0)
                {
                    //REMOVE OLD ITEMS ADDED TO CART
                    HttpCookie cookie = Request.Cookies["Cart"];
                    BACRM.BusinessLogic.ShioppingCart.Cart objcart;
                    objcart = new BACRM.BusinessLogic.ShioppingCart.Cart();
                    objcart.DomainID = CCommon.ToLong(Session["DomainID"]);
                    objcart.ContactId = CCommon.ToLong(Session["UserContactID"]);
                    objcart.CookieId = CCommon.ToString((cookie != null) ? cookie["KeyId"] : "");
                    objcart.ItemCode = 0;
                    objcart.SiteID = Sites.ToLong(Session["SiteId"]);
                    objcart.bitDeleteAll = true;
                    objcart.RemoveItemFromCart();

                    objCommon.Mode = 17;
                    objCommon.Comments = CCommon.ToString((cookie["KeyId"] != null) ? cookie["KeyId"] : "");
                    objCommon.UpdateValueID = CCommon.ToLong(Session["UserContactID"]);
                    objCommon.UpdateSingleFieldValue();
                }

            }
            catch (Exception ex)
            {
                throw;
            }
        }

        protected void btnCouponCode_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtCouponCode.Text.Length > 0)
                {
                    DiscountCodes objDiscountCodes = new DiscountCodes();
                    objDiscountCodes.DomainID = Sites.ToLong(Session["DomainID"]);
                    objDiscountCodes.DivisionID = Sites.ToInteger(Session["DivId"]);
                    objDiscountCodes.DiscountCode = txtCouponCode.Text.Trim();
                    objDiscountCodes.SiteID = Sites.ToLong(Session["SiteId"]);
                    DataTable dtDiscount = objDiscountCodes.ValidateDiscountCode();

                    if (dtDiscount != null && dtDiscount.Rows.Count > 0)
                    {
                        hdnOrderPromotionID.Value = CCommon.ToString(dtDiscount.Rows[0]["numPromotionID"]);
                        hdnDiscountCodeID.Value = CCommon.ToString(dtDiscount.Rows[0]["numDisocuntID"]);

                        trApplyCouponCode.Visible = false;
                        txtCouponCode.Text = "";
                        lblCouponCode.Text = CCommon.ToString(dtDiscount.Rows[0]["vcCouponCode"]);
                        trUsedCouponCode.Visible = true;

                        if (!CCommon.ToBool(dtDiscount.Rows[0]["bitUseForCouponManagement"]))
                        {
                            AddOrderPromotionDiscountIfAvailable();
                        }

                        BACRM.BusinessLogic.ShioppingCart.Cart objcart = new BACRM.BusinessLogic.ShioppingCart.Cart();
                        HttpCookie cookie = Request.Cookies["Cart"];
                        objcart.DomainID = CCommon.ToLong(Session["DomainID"]);
                        objcart.ContactId = CCommon.ToLong(Session["UserContactID"]);
                        objcart.CookieId = (cookie != null) ? cookie["KeyId"] : "";
                        objcart.vcCoupon = CCommon.ToString(dtDiscount.Rows[0]["vcCouponCode"]);
                        objcart.SiteID = Sites.ToLong(Session["SiteId"]);
                        objcart.ApplyCouponCodePromotion();
                    }

                    hdnSelectedShippingCharge.Value = "-1";
                    bindHtml();
                }
                else
                {
                    Page.MaintainScrollPositionOnPostBack = false;
                    ShowMessage(GetErrorMessage("ERR005"), 1);//Enter Coupon Code
                }
            }
            catch (Exception ex)
            {
                hdnOrderPromotionID.Value = "";
                hdnDiscountCodeID.Value = "";
                lblCouponCode.Text = "";
                txtCouponCode.Text = "";
                trApplyCouponCode.Visible = true;
                trUsedCouponCode.Visible = false;

                if (ex.Message.Contains("INVALID_COUPON_CODE"))
                {
                    Page.MaintainScrollPositionOnPostBack = false;
                    ShowMessage(GetErrorMessage("ERR011"), 1);
                }
                else if (ex.Message.Contains("COUPON_CODE_EXPIRED"))
                {
                    Page.MaintainScrollPositionOnPostBack = false;
                    ShowMessage(GetErrorMessage("ERR006"), 1);
                }
                else if (ex.Message.Contains("COUPON_USAGE_LIMIT_EXCEEDED"))
                {
                    Page.MaintainScrollPositionOnPostBack = false;
                    ShowMessage(GetErrorMessage("ERR008"), 1);
                }
                else
                {
                    ExceptionModule.ExceptionPublish(ex, Sites.ToLong(Session["DomainID"]), Sites.ToLong(Session["UserContactID"]), Sites.ToLong(Session["SiteId"]), Request);
                    Page.MaintainScrollPositionOnPostBack = false;
                    ShowMessage(ex.ToString(), 1);
                }
            }
        }

        protected void lbCouponCodeRemove_Click(object sender, EventArgs e)
        {
            try
            {
                if (CCommon.ToLong(hdnOrderPromotionID.Value) > 0)
                {
                    BACRM.BusinessLogic.ShioppingCart.Cart objcart = new BACRM.BusinessLogic.ShioppingCart.Cart();
                    HttpCookie cookie = Request.Cookies["Cart"];
                    objcart.DomainID = CCommon.ToLong(Session["DomainID"]);
                    objcart.ContactId = CCommon.ToLong(Session["UserContactID"]);
                    objcart.CookieId = (cookie != null) ? cookie["KeyId"] : "";
                    objcart.PromotionID = CCommon.ToLong(hdnOrderPromotionID.Value);
                    objcart.ClearCouponCodePromotion();
                }

                DeleteItemFromCart(Sites.ToLong(Session["DiscountServiceItemID"]));

                hdnOrderPromotionID.Value = "";
                hdnDiscountCodeID.Value = "";
                lblCouponCode.Text = "";
                txtCouponCode.Text = "";

                trApplyCouponCode.Visible = true;
                trUsedCouponCode.Visible = false;

                hdnSelectedShippingCharge.Value = "-1";
                bindHtml();
            }
            catch (Exception ex)
            {
                ExceptionModule.ExceptionPublish(ex, Sites.ToLong(Session["DomainID"]), Sites.ToLong(Session["UserContactID"]), Sites.ToLong(Session["SiteId"]), Request);
                Page.MaintainScrollPositionOnPostBack = false;
                ////Page.ClientScript.RegisterStartupScript(Page.ClientScript.GetType(), Page.ClientID, "resetScrollPosition();", true);
                ShowMessage(ex.ToString(), 1);
            }
        }

        public void ClearPastSession()
        {
            Session.Remove("CompID");
            Session.Remove("UserContactID");
            Session.Remove("ContactId");
            Session.Remove("DivId");
            Session.Remove("GroupId");
            Session.Remove("CRMType");
            Session.Remove("CompName");
            Session.Remove("RecOwner");
            Session.Remove("RecOwnerName");
            Session.Remove("PartnerGroup");
            Session.Remove("RelationShip");
            Session.Remove("UserEmail");
            Session.Remove("ContactName");
            Session.Remove("Username");
            Session.Remove("Password");
        }

        private DataTable CheckIfMinOrderAmountRuleMeets(decimal _totalAmount, long _divID)
        {
            try
            {
                MOpportunity objOpportunity = new MOpportunity();
                objOpportunity.DomainID = CCommon.ToLong(Session["DomainID"]);
                objOpportunity.DivisionID = _divID;
                return objOpportunity.CheckIfMinOrderAmountRuleMeets(_totalAmount);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void OnPageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                DataTable dt = new DataTable();
                gvPromotionPostCheckOut.PageIndex = e.NewPageIndex;
                if (Session["dtItemPostCheckOutPopup"] != null)
                {
                    dt = (DataTable)Session["dtItemPostCheckOutPopup"];
                }
                gvPromotionPostCheckOut.DataSource = dt;
                gvPromotionPostCheckOut.DataBind();
                dialogSellPopup.Visible = true;
                this.Page.ClientScript.RegisterStartupScript(this.GetType(), "ShowMiniPostSellCart", "ShowMiniPostSellCart();", true);
            }
            catch (Exception ex)
            {
                ExceptionModule.ExceptionPublish(ex, Sites.ToLong(Session["DomainID"]), Sites.ToLong(Session["UserContactID"]), Sites.ToLong(Session["SiteId"]), Request);
                Page.MaintainScrollPositionOnPostBack = false;
                ////Page.ClientScript.RegisterStartupScript(Page.ClientScript.GetType(), Page.ClientID, "resetScrollPosition();", true);
                ShowMessage(ex.ToString(), 1);
            }
        }

        protected void btnYesThanks_Click(object sender, EventArgs e)
        {
            DataTable dt = new DataTable();
            for (int i = 0; i < gvItemsForRule.Columns.Count; i++)
            {
                dt.Columns.Add("column" + i.ToString());
            }
            foreach (GridViewRow row in gvItemsForRule.Rows)
            {
                DataRow dr = dt.NewRow();
                for (int j = 0; j < gvItemsForRule.Columns.Count; j++)
                {
                    dr["column" + j.ToString()] = row.Cells[j].Text;
                }

                dt.Rows.Add(dr);
            }
            decimal _discount = 0;
            foreach (DataRow rw in dt.Rows)
            {
                _discount = _discount + CCommon.ToDecimal(rw["column1"]);
                AddToCartFromEcomm("", CCommon.ToLong(rw["column0"]), 0, "", false, 1, CCommon.ToDecimal(rw["column1"]));
            }
            if (_discount > 0)
            {
                decimal _newDiscount = CCommon.ToDecimal(Session["TotalDiscount"]);
                _newDiscount = _newDiscount + _discount;
                Session["TotalDiscount"] = _newDiscount;
            }
            hdnIsFinishedPostCheckout.Value = "True";
            dialogSellPopup.Visible = false;
        }

        protected void btnNo_Click(object sender, EventArgs e)
        {
            hdnIsFinishedPostCheckout.Value = "True";
            dialogSellPopup.Visible = false;
        }

        private void bindPostCheckout(DataTable dtCart, CItems objItem)
        {
            string strNumItemCode = string.Empty;
            DataTable dtIncentiveSaleItems = new DataTable();
            if (dtCart != null && dtCart.Rows.Count > 0)
            {
                decimal _cartTotalAmount = GetCartTotalAmount();
                foreach (DataRow r in dtCart.Rows)
                {
                    strNumItemCode = strNumItemCode + "," + CCommon.ToString(r["numItemCode"]);
                }
                strNumItemCode = strNumItemCode.Substring(1);
                if (_cartTotalAmount > 0 && !string.IsNullOrEmpty(strNumItemCode))
                {
                    dtIncentiveSaleItems = objItem.GetPromotionOfferForIncentiveSaleItems(_cartTotalAmount, strNumItemCode);
                    decimal returnValue = CCommon.ToInteger(dtIncentiveSaleItems.Rows[0]["SubTotal"]);

                    if (dtIncentiveSaleItems != null && dtIncentiveSaleItems.Rows.Count > 0 && returnValue > 0)
                    {
                        LoadFlashDivGrid(CCommon.ToLong(dtIncentiveSaleItems.Rows[0]["PromotionID"]), returnValue);
                        flashPage.Visible = true;
                        dialogSellPopup.Visible = false;
                        this.Page.ClientScript.RegisterStartupScript(this.GetType(), "ShowFreePostSellItems", "ShowFreePostSellItems();", true);
                    }
                    else if (returnValue == 0 || returnValue == -1)
                    {
                        var distinctRows = (from DataRow dRow in dtCart.Rows
                                            where CCommon.ToString(dRow["numParentItemCode"]) == "" || dRow["numParentItemCode"] == null
                                            select new { col1 = dRow["numItemCode"], col2 = dRow["numWarehouseid"] }).Distinct();

                        DataTable dtItemPostCheckOutPopup = new DataTable();
                        DataTable dtTemp = new DataTable();
                        if (distinctRows.Count() > 0)
                        {
                            foreach (var row in distinctRows)
                            {
                                long _numParentItemCode = CCommon.ToLong(row.col1);
                                long _numWarehouseId = CCommon.ToInteger(row.col2);

                                if (_numParentItemCode != 0 && _numWarehouseId != 0)
                                {
                                    DataTable dtPromotions = BindPromotions(CCommon.ToInteger(_numParentItemCode), CCommon.ToLong(_numWarehouseId));
                                    if (dtPromotions != null && dtPromotions.Rows.Count > 0)
                                    {
                                        divPromotion.Visible = true;
                                        lblVcPromotion.Text = CCommon.ToString(dtPromotions.Rows[0]["vcPromotionDescription"]);
                                    }
                                }

                                objItem.ParentItemCode = _numParentItemCode;
                                dtTemp = objItem.GetSimilarItem();
                                if (dtCart.Rows.Count > 0 && dtTemp.Rows.Count > 0)
                                {
                                    DataRow[] findRows = dtCart.Select("numItemCode = " + CCommon.ToLong(dtTemp.Rows[0]["numItemCode"]));
                                    if (findRows.Count() <= 0)
                                    {
                                        if (dtTemp.Rows.Count > 0)
                                        {
                                            if (dtItemPostCheckOutPopup.Rows.Count > 0)
                                            {
                                                dtItemPostCheckOutPopup.Merge(dtTemp);
                                            }
                                            else
                                            {
                                                dtItemPostCheckOutPopup = dtTemp.Copy();
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        if (dtItemPostCheckOutPopup != null && dtItemPostCheckOutPopup.Rows.Count > 0)
                        {
                            #region Commented
                            /*DataTable dtPromotion = new DataTable();
                            foreach (DataRow dr in dtItemPostCheckOutPopup.Rows)
                            {
                                DataTable dt = RelatedItemsPromotion(CCommon.ToInteger(dr["numItemCode"]), CCommon.ToLong(dr["numWareHouseItemID"]));
                                if (dt != null && dt.Rows.Count > 0)
                                {
                                    dtPromotion = dt.Copy();
                                }
                            }

                            if (dtPromotion != null && dtPromotion.Rows.Count > 0)
                            {
                                DataTable dtItemPost = new DataTable();
                                var _proId = (from DataRow dRow in dtPromotion.Rows
                                              select new { p1 = dRow["numProId"] }).Distinct();

                                if (_proId.Count() > 0)
                                {
                                    foreach (var row in _proId)
                                    {
                                        long _numProId = CCommon.ToLong(row.p1);

                                        dtItemPost = (from DataRow dr in dtItemPostCheckOutPopup.Rows
                                                      where CCommon.ToLong(dr["numProId"]) == _numProId
                                                      select dr).CopyToDataTable();
                                    }
                                }
                                Session["dtItemPost"] = dtItemPost;
                                gvPromotionPostCheckOut.DataSource = dtItemPost;
                                gvPromotionPostCheckOut.DataBind();
                                dialogSellPopup.Visible = true;
                            }
                            else
                            {
                                hdnIsFinishedPostCheckout.Value = "True";
                                dialogSellPopup.Visible = false;
                                btnCharge_Click(null, null);
                            }*/
                            #endregion
                            Session["dtItemPostCheckOutPopup"] = dtItemPostCheckOutPopup;
                            gvPromotionPostCheckOut.DataSource = dtItemPostCheckOutPopup;
                            gvPromotionPostCheckOut.DataBind();
                            dialogSellPopup.Visible = true;
                            this.Page.ClientScript.RegisterStartupScript(this.GetType(), "ShowRelatedPostSellItems", "ShowRelatedPostSellItems();", true);
                        }
                        else
                        {
                            hdnIsFinishedPostCheckout.Value = "True";
                            dialogSellPopup.Visible = false;
                            btnCharge_Click(null, null);
                        }
                    }
                }
                else
                {
                    hdnIsFinishedPostCheckout.Value = "True";
                    btnCharge_Click(null, null);
                }
            }
        }

        /*Function added by Neelam on 10/11/2017 - Added functionality to bind Promotions*/
        private DataTable BindPromotions(int numItemCode, long numWarehouseItemID)
        {
            CItems objItems = new CItems();
            objItems.ItemCode = numItemCode;
            objItems.DomainID = CCommon.ToLong(Session["DomainID"]);
            objItems.WareHouseItemID = numWarehouseItemID;
            objItems.SiteID = CCommon.ToLong(HttpContext.Current.Session["SiteID"]);
            DataTable dt = objItems.GetPromotionsForSimilarItems();
            return dt;
        }

        private void LoadFlashDivGrid(long _promotionID, decimal _subTotalReturned)
        {
            try
            {
                PromotionOffer objPromotionOffer = new PromotionOffer();
                objPromotionOffer.PromotionID = _promotionID;
                objPromotionOffer.DomainID = Sites.ToLong(Session["DomainID"]);
                objPromotionOffer.SiteID = CCommon.ToLong(HttpContext.Current.Session["SiteID"]);
                DataSet ds = objPromotionOffer.GetPromotionOfferOrderBasedItems();

                DataTable dtItems = ds.Tables[2];

                if (dtItems != null && dtItems.Rows.Count > 0)
                {
                    DataTable dtClone = new DataTable();

                    dtClone = (from DataRow dr in dtItems.Rows
                               where CCommon.ToDecimal(dr["fltSubTotal"]) == _subTotalReturned
                               select dr).CopyToDataTable();

                    gvItemsForRule.DataSource = dtClone;
                    gvItemsForRule.DataBind();
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }
    }
}