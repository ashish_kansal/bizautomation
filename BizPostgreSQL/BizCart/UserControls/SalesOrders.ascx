﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SalesOrders.ascx.cs"
    Inherits="BizCart.UserControls.SalesOrders" %>
<%@ Register Assembly="AspNetPager, Version=7.4.4.0, Culture=neutral, PublicKeyToken=fb0a0fe055d40fd4" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>
<script language="javascript">
    function OpenBizInvoice(a, b , c,d,e) {
        window.open('Invoice.aspx?Show=True&OppID=' + a + '&RefType=' + b + '&PrintMode=' + c+'&InvoiceType=' + d + '&BizDocId='+e,'', 'toolbar=0,titlebar=0,menubar=0,location=1,left=100,top=50,width=750,height=800,scrollbars=yes,resizable=yes');
    }
    function OpenCustomerStatement() {
        window.open('customerstatement.aspx', '', 'toolbar=no,titlebar=no,top=10,left=100,width=1000,height=800,scrollbars=yes,resizable=yes')
        return false;
    }
    function OpenShippingStatus(companyId, trackingno, ismoretrackingNo, oppid, oppBizDocId, ShippingReportId) {
        if (companyId != "" || trackingno != "") {
            window.open('ShippingStatus.aspx?companyId=' + companyId + '&trackingno=' + trackingno + '&ismoretrackingNo=' + ismoretrackingNo + '&oppid=' + oppid + '&oppBizDocId=' + oppBizDocId + '&ShippingReportId=' + ShippingReportId + '', '', 'toolbar=no,titlebar=no,top=10,left=100,width=550,height=400,scrollbars=yes,resizable=yes')
        }
        return false;
    }
    function OpenShippingReport(shippingreportId, bizdocId, OppID) {
        window.open('fulfilment.aspx?shipmentreportId=' + shippingreportId + '&OppBizDocID=' + bizdocId + '&OppID=' + OppID + '', '', 'toolbar=no,titlebar=no,top=10,left=100,width=1000,height=800,scrollbars=yes,resizable=yes')
        return false;
    }
</script>

<asp:Panel ID="pnlCutomizeHtml" runat="server">
    <table align="right" runat="server" id="tblCreditTerm">
        <tr>
            <td class="LabelColumn">
                Total Balance Due :
            </td>
            <td class="ControlCell">
                <asp:Label ID="lblBalDue" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="LabelColumn">
                Total Remaining Credit :
            </td>
            <td class="ControlCell">
                <asp:Label ID="lblRemCredit" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="LabelColumn">
                Total Amount Past Due :
            </td>
            <td class="ControlCell">
                <asp:Label ID="lblAmtPastDue" runat="server"></asp:Label>
            </td>
        </tr>
        
    </table>
    <table cellspacing="0" cellpadding="0" width="100%" border="0">
        <tr>
            <td>
                <div class="sectionheader">
                    Orders
                </div>
            </td>
        </tr>
         <tr>
            <td class="LabelColumn" align="right">
                <asp:RadioButtonList ID="rdnOrdersType" AutoPostBack="true" RepeatLayout="Flow" RepeatDirection="Horizontal" OnSelectedIndexChanged="rdnOrdersType_SelectedIndexChanged" runat="server">
                    <asp:ListItem Value="0">Open Order</asp:ListItem>
                    <asp:ListItem Value="1">Closed Order</asp:ListItem>
                </asp:RadioButtonList>
            </td>
        </tr>
        <tr>
            <td class="LabelColumn" align="right">
                No of Records:<asp:Label ID="lblRecordCount" runat="server"></asp:Label>
            </td>
        </tr>
          <tr>
            <td align="right" align="left">
                <asp:Label ID="lblItemCount" runat="server" Text="Item(s): 1-15 of 200"></asp:Label>

            </td>
            <td align="right">
                <webdiyer:AspNetPager ID="AspNetPager1" runat="server"
                    PagingButtonSpacing="0"
                    PagingButtonLayoutType="UnorderedList" 
                    CurrentPageButtonClass="active" 
                    FirstPageText="First" 
                    LastPageText="Last" 
                    NextPageText="Next" 
                    PrevPageText="Prev"
                    OnPageChanged="AspNetPager1_PageChanged"
                    width="100%" 
                    ShowPageIndexBox="Never"></webdiyer:AspNetPager>

            </td>
            <td>&nbsp;
            </td>
        </tr>
        <tr>
            <td class="Grid1">
                <%-- <asp:DataGrid ID="dgOpportunity" runat="server" CssClass="Grid" AllowSorting="false"
                    AutoGenerateColumns="False">
                    <AlternatingItemStyle CssClass="GridAltItem"></AlternatingItemStyle>
                    <ItemStyle CssClass="GridItem"></ItemStyle>
                    <HeaderStyle CssClass="GridHeader"></HeaderStyle>
                    <Columns>
                        <asp:BoundColumn Visible="False" DataField="numOppId"></asp:BoundColumn>
                        <asp:BoundColumn Visible="False" DataField="numOppBizDocsId"></asp:BoundColumn>
                        <asp:TemplateColumn HeaderText="Invoice #">
                            <ItemTemplate>
                                <a href='javascript:OpenBizInvoice(<%# Eval("numOppId") +"," + Eval("numOppBizDocsId") %>)'>
                                    <%# Eval("vcBizDocID")%></a>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:BoundColumn DataField="Credit" SortExpression="Credit" HeaderText="Credit terms">
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="CreatedName" SortExpression="CreatedName" HeaderText="Created By">
                        </asp:BoundColumn>
                        <asp:TemplateColumn HeaderText="Date Created" SortExpression="dtCreatedDate">
                            <ItemTemplate>
                                <%#ReturnDateTime(DataBinder.Eval(Container.DataItem, "numCreatedDate"))%>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:BoundColumn DataFormatString="{0:#,##0.00}" DataField="TotalAmt" HeaderText="Grand Total">
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="monAmountPaid" SortExpression="monAmountPaid" DataFormatString="{0:#,##0.00}"
                            HeaderText="Amount Paid"></asp:BoundColumn>
                        <asp:BoundColumn DataField="BalanceDue" DataFormatString="{0:#,##0.00}" SortExpression=""
                            HeaderText="Balance Due"></asp:BoundColumn>
                    </Columns>
                    <PagerStyle Visible="False" ForeColor="#000066" BackColor="White" Mode="NumericPages">
                    </PagerStyle>
                </asp:DataGrid>--%>
            </td>
        </tr>
    </table>
</asp:Panel>
<asp:Literal ID="litCutomizeHtml" runat="server"></asp:Literal>
<asp:TextBox ID="txtTotalPage" runat="server" Style="display: none"></asp:TextBox>
<asp:TextBox ID="txtTotalRecords" runat="server" Style="display: none"></asp:TextBox>
<asp:TextBox ID="txtSortChar" runat="server" Style="display: none"></asp:TextBox>
<asp:Button runat="server" ID="Button1" Text="" Style="display: none"></asp:Button>
