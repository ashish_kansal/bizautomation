﻿using BACRM.BusinessLogic.Admin;
using BACRM.BusinessLogic.Common;
using BACRM.BusinessLogic.Contacts;
using BACRM.BusinessLogic.Item;
using BACRM.BusinessLogic.Opportunities;
using BACRM.BusinessLogic.Promotion;
using BACRM.BusinessLogic.ShioppingCart;
using System;
using System.Collections;
using System.Data;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BizCart.UserControls
{
    public partial class Cart : BizUserControl
    {
        #region Global Declaration

        long lngItemCode;
        long cartID;
        string qualifiedshipping = "";

        #endregion

        #region Custom Properties

        public Boolean reCalculation
        {
            get
            {
                object o = ViewState["reCalculation"];
                if (o != null)
                {
                    return (Boolean)o;
                }
                else
                {
                    return false;
                }
            }
            set
            {
                ViewState["reCalculation"] = value;
            }
        }

        public Boolean isShippingMethodNeeded
        {
            get
            {
                object o = ViewState["isShippingMethodNeeded"];
                if (o != null)
                {
                    return (Boolean)o;
                }
                else
                {
                    ViewState["isShippingMethodNeeded"] = true;
                    return true;
                }
            }
            set
            {
                ViewState["isShippingMethodNeeded"] = value;
            }
        }

        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                Sites.InitialiseSession();

                if (!IsPostBack)
                {
                    #region bind Dropdown

                    CCommon objCommon = new CCommon();
                    objCommon.sb_FillComboFromDBwithSel(ref ddlCountry, 40, Sites.ToLong(Session["DomainID"]));
                    SetDefaultCountry();
                    if (ddlCountry.SelectedIndex != 0)
                    {
                        FillState(ddlState, Sites.ToLong(ddlCountry.SelectedValue), Sites.ToLong(Session["DomainID"]));
                    }
                    if (lblPartnerCode != null)
                    {
                        if (Convert.ToString(Session["partnerurlcode"]) == "")
                        {
                            lblPartnerCode.Text = "-";
                        }
                        else
                        {
                            lblPartnerCode.Text = Convert.ToString(Session["partnerurlcode"]);
                        }
                    }
                    #endregion

                    #region Bind Address (Primary or nonPrimary(Session))

                    DataTable dtTable = null;
                    // Bind address as per Session variable or primary Address ...
                    dtTable = GetShippingAddress();
                    if (dtTable.Rows.Count > 0)
                    {
                        if (CCommon.ToDecimal(dtTable.Rows[0]["numCountry"]) != 0)
                        {
                            if (ddlCountry.Items.Count > 0)
                            {
                                if (ddlCountry.Items.FindByValue(CCommon.ToString(dtTable.Rows[0]["numCountry"])) != null)
                                {
                                    ddlCountry.ClearSelection();
                                    ddlCountry.Items.FindByValue(CCommon.ToString(dtTable.Rows[0]["numCountry"])).Selected = true;
                                    Session["CalculateShippingCountry"] = CCommon.ToString(dtTable.Rows[0]["numCountry"]);
                                }
                            }
                        }
                        if (CCommon.ToDecimal(dtTable.Rows[0]["numState"]) != 0)
                        {
                            if (ddlState.Items.Count > 0)
                            {
                                if (ddlState.Items.FindByValue(CCommon.ToString(dtTable.Rows[0]["numState"])) != null)
                                {
                                    ddlState.ClearSelection();
                                    ddlState.Items.FindByValue(CCommon.ToString(dtTable.Rows[0]["numState"])).Selected = true;
                                    Session["CalculateShippingState"] = CCommon.ToString(dtTable.Rows[0]["numState"]);
                                }
                            }
                        }
                        if (CCommon.ToString(dtTable.Rows[0]["vcPostalCode"]) != "")
                        {
                            txtPostal.Text = CCommon.ToString(dtTable.Rows[0]["vcPostalCode"]);
                            Session["CalculateShippingZipCode"] = CCommon.ToString(dtTable.Rows[0]["vcPostalCode"]);
                        }
                    }
                    GetLocationByIP();

                    #endregion

                    //BindCart();
                    //bindHtml();

                    hdnReffereURL.Value = Request.UrlReferrer == null ? "" : Request.UrlReferrer.OriginalString;

                }

                if (IsPostBack)
                {
                    if (Sites.ToString(Request["__EVENTTARGET"]) == "btnDelete")
                    {
                        DeleteItem();
                    }
                    if (Sites.ToString(Request["__EVENTTARGET"]) == "shippingAddress")
                    {
                        ViewRateCalculation();
                    }

                    if (Sites.ToString(Request["__EVENTTARGET"]) == "btnContinue")
                    {
                        btnContinue_Click(((object)(Request["__EVENTTARGET"])), null);
                    }

                    if (Sites.ToString(Request["__EVENTTARGET"]) == "btnUpdateCart")
                    {
                        btnUpdateCart_Click(((object)(Request["__EVENTTARGET"])), null);
                        //After updating cart we just want new Shipping rule .
                        //Thats why i just set below given variable to get fresh rule ...
                        this.isShippingMethodNeeded = true;
                        Session["ShippingMethodValue"] = null;
                    }

                    if (Sites.ToString(Request["__EVENTTARGET"]) == "btnCheckOut")
                    {
                        btnCheckOut_Click(((object)(Request["__EVENTTARGET"])), null);
                    }
                    //bindHtml();
                }

            }
            catch (Exception ex)
            {
                ExceptionModule.ExceptionPublish(ex, Sites.ToLong(Session["DomainID"]), Sites.ToLong(Session["UserContactID"]), Sites.ToLong(Session["SiteId"]), Request);
                Page.MaintainScrollPositionOnPostBack = false;
                ////Page.ClientScript.RegisterStartupScript(Page.ClientScript.GetType(), Page.ClientID, "resetScrollPosition();", true);
                ShowMessage(ex.ToString(), 1);
            }
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            try
            {
                BindCart();
                bindHtml();
            }
            catch (Exception ex)
            {
                ExceptionModule.ExceptionPublish(ex, Sites.ToInteger(Session["DomainID"]), Sites.ToInteger(Session["UserContactID"]), Sites.ToInteger(Session["SiteId"]), Request);
                Page.MaintainScrollPositionOnPostBack = false;
                ////Page.ClientScript.RegisterStartupScript(Page.ClientScript.GetType(), Page.ClientID, "resetScrollPosition();", true);
                ShowMessage(ex.ToString(), 1);
            }
        }

        protected void btnContinue_Click(object sender, EventArgs e)
        {
            try
            {
                if (hdnReffereURL.Value.Length > 5)
                    Response.Redirect(hdnReffereURL.Value);
                else
                    Response.Redirect("Home.aspx", false);
            }
            catch (Exception ex)
            {
                ExceptionModule.ExceptionPublish(ex, Sites.ToLong(Session["DomainID"]), Sites.ToLong(Session["UserContactID"]), Sites.ToLong(Session["SiteId"]), Request);
                Page.MaintainScrollPositionOnPostBack = false;
                ////Page.ClientScript.RegisterStartupScript(Page.ClientScript.GetType(), Page.ClientID, "resetScrollPosition();", true);
                ShowMessage(ex.ToString(), 1);
            }
        }

        protected void btnUpdateCart_Click(object sender, EventArgs e)
        {
            try
            {
                DataTable dttable = default(DataTable);
                DataSet ds = default(DataSet);
                ds = GetCartItem();
                if (ds != null)
                {
                    //ds = GetCartItem(); //(DataSet)Session["Data"];
                    dttable = ds.Tables[0];
                    int intUnits;
                    decimal decConversionFactor;
                    PriceBookRule objPbook = new PriceBookRule();
                    DataTable dtCalPrice = default(DataTable);
                    string[] strArr;

                    foreach (string str in Request.Form)
                    {
                        if (str.IndexOf("txtUnits_") >= 0)
                        {
                            if (Request.Form[str] != "")
                            {
                                strArr = str.Split(char.Parse("_"));
                                intUnits = Math.Abs(Sites.ToInteger(Request.Form[str]));
                                cartID = Sites.ToLong(strArr[1]);
                                lngItemCode = Sites.ToLong(strArr[2]);

                                foreach (DataRow dr in dttable.Rows)
                                {
                                    if (cartID == Sites.ToLong(dr["numCartID"]))
                                    {
                                        if (Sites.ToBool(ApplyPriceBookPrice) == true && Sites.ToBool(Session["HidePrice"]) == false)
                                        {
                                            decConversionFactor = Sites.ToDecimal(dr["decUOMConversionFactor"]);

                                            objPbook.ItemID = lngItemCode;
                                            objPbook.QntyofItems = Sites.ToInteger(intUnits * decConversionFactor);
                                            objPbook.DomainID = Sites.ToLong(Session["DomainID"]);
                                            objPbook.DivisionID = Sites.ToLong(Session["DivId"]);
                                            objPbook.WarehouseID = Sites.ToLong(Session["WareHouseID"]);
                                            objPbook.KitSelectedChild = CCommon.ToString(dr["vcChildKitItemSelection"]);
                                            objPbook.SiteID = Sites.ToLong(HttpContext.Current.Session["SiteId"]);
                                            dtCalPrice = objPbook.GetPriceBasedonPriceBook();
                                            if (dtCalPrice.Rows.Count > 0)
                                            {
                                                dr["monPrice"] = Sites.ToDouble(Sites.ToDecimal(dtCalPrice.Rows[0]["ListPrice"]) * decConversionFactor / Sites.ToDecimal(Session["ExchangeRate"].ToString()));
                                            }
                                        }

                                        dr["numUnitHour"] = intUnits;
                                        dr["monTotAmtBefDiscount"] = string.Format("{0:#,##0.00}", Sites.ToDecimal(dr["numUnitHour"].ToString()) * Sites.ToDecimal(dr["monPrice"].ToString()));
                                        dr["monTotAmount"] = dr["monTotAmtBefDiscount"];
                                    }
                                }
                            }
                        }
                    }
                    ds.AcceptChanges();
                    BACRM.BusinessLogic.ShioppingCart.Cart objcart;
                    objcart = new BACRM.BusinessLogic.ShioppingCart.Cart();
                    HttpCookie cookie = GetCookie();
                    objcart.DomainID = CCommon.ToLong(Session["DomainID"]);
                    objcart.ContactId = CCommon.ToLong(Session["UserContactID"]);
                    objcart.CookieId = (cookie != null) ? cookie["KeyId"] : "";
                    objcart.strXML = GetCartItemsXML(ds);
                    objcart.SiteID = Sites.ToLong(Session["SiteId"]);
                    if (Session["PreferredItemPromotions"] != null)
                    {
                        objcart.PreferredPromotion = ((DataSet)Session["PreferredItemPromotions"]).GetXml();
                    }
                    objcart.UpdateCartItem();
                    Session["gblItemCount"] = null;
                    Session["gblTotalAmount"] = null;

                    //Update Minicart
                    this.Page.ClientScript.RegisterStartupScript(this.GetType(), "UpdateMiniCart", "if(document.getElementById('btnRefresh')!=null){ __doPostBack('btnRefresh','OnClick');}", true);
                    this.Page.ClientScript.RegisterStartupScript(this.GetType(), "UpdateCartTotal", "__doPostBack('RefreshCartTotal','')", true);
                    Session["ShippingMethodConfirmed"] = "False";//Reset confirmation.. so user have to choose shipping method again.
                    Session["SelectedStep"] = 0;
                }
            }
            catch (Exception ex)
            {
                ExceptionModule.ExceptionPublish(ex, Sites.ToLong(Session["DomainID"]), Sites.ToLong(Session["UserContactID"]), Sites.ToLong(Session["SiteId"]), Request);
                Page.MaintainScrollPositionOnPostBack = false;
                ////Page.ClientScript.RegisterStartupScript(Page.ClientScript.GetType(), Page.ClientID, "resetScrollPosition();", true);
                ShowMessage(ex.ToString(), 1);
            }
        }

        protected void btnCheckOut_Click(object sender, EventArgs e)
        {
            try
            {
                DataSet ds = new DataSet();
                ds = GetCartItem();
                if (ds.Tables[0].Rows.Count > 0)
                {
                    int ErrorCode = 0;
                    if (!Convert.ToBoolean(Session["IsOnePageCheckout"]))
                    {
                        ErrorCode = CheckForUserDetail();
                    }
                    if (ErrorCode > 0)
                    {
                        if (ErrorCode == 5)
                        {
                            Response.Redirect("Login.aspx?ReturnURL=" + (Sites.ToString(Session["SiteLiveURL"]).Trim().Length > 2 ? "https://" : "http://") + Request.Url.Host + "/" + CheckoutPageName() + "&errcode=" + ErrorCode, true);
                        }
                        else if (ErrorCode < 5)
                        {
                            Response.Redirect("CustomerInformation.aspx?ReturnURL=" + (Sites.ToString(Session["SiteLiveURL"]).Trim().Length > 2 ? "https://" : "http://") + Request.Url.Host + "/" + CheckoutPageName() + "&errcode=" + ErrorCode, true);
                        }
                        else
                        {
                            Response.Redirect("ConfirmAddress.aspx?ReturnURL=" + (Sites.ToString(Session["SiteLiveURL"]).Trim().Length > 2 ? "https://" : "http://") + Request.Url.Host + "/" + CheckoutPageName() + "&errcode=" + ErrorCode, true);
                        }
                    }
                    else
                    {
                        //Note : This for confirm shipping Method and go to Checkout Page
                        Session["ShippingMethodConfirmed"] = true;
                        string strShippingCharge = GetShippingCharge();
                        //Session["ShippingCost"] = strShippingCharge; //change 
                        lblShippingCost.Text = string.Format("{0:#,##0.00}", strShippingCharge); //change;
                        Session["TotalAmount"] = CCommon.ToDecimal(lblTotal.Text) + CCommon.ToDecimal(strShippingCharge); //change

                        Response.Redirect((Sites.ToString(Session["SiteLiveURL"]).Trim().Length > 2 ? "https://" : "http://") + Request.Url.Host + "/" + CheckoutPageName(), false);
                    }
                }
                else
                {
                    Page.MaintainScrollPositionOnPostBack = false;
                    ////Page.ClientScript.RegisterStartupScript(Page.ClientScript.GetType(), Page.ClientID, "resetScrollPosition();", true);
                    ShowMessage(GetErrorMessage("ERR001"), 1);//Cart is empty
                    BindCart();
                    bindHtml();
                }

            }
            catch (Exception ex)
            {
                ExceptionModule.ExceptionPublish(ex, Sites.ToLong(Session["DomainID"]), Sites.ToLong(Session["UserContactID"]), Sites.ToLong(Session["SiteId"]), Request);
                Page.MaintainScrollPositionOnPostBack = false;
                ////Page.ClientScript.RegisterStartupScript(Page.ClientScript.GetType(), Page.ClientID, "resetScrollPosition();", true);
                ShowMessage(ex.ToString(), 1);
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                //After Click on Recalculate buttom we may get different value of Country Dropdown , State  Dropdown 
                //and postal Code.Thats why i just set below given variable to get fresh rule ...

                this.reCalculation = true;
                Session["ShippingMethodValue"] = null;
                Session["CalculateShippingCountry"] = ddlCountry.SelectedValue;
                Session["CalculateShippingState"] = ddlState.SelectedValue;
                Session["CalculateShippingZipCode"] = txtPostal.Text;
                Session["CalculateShippingResidential"] = chkResidential.Checked;
                this.isShippingMethodNeeded = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        protected void btnCouponCode_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtCouponCode.Text.Length > 0)
                {
                    DiscountCodes objDiscountCodes = new DiscountCodes();
                    objDiscountCodes.DomainID = Sites.ToLong(Session["DomainID"]);
                    objDiscountCodes.DivisionID = Sites.ToInteger(Session["DivId"]);
                    objDiscountCodes.DiscountCode = txtCouponCode.Text.Trim();
                    objDiscountCodes.SiteID = Sites.ToLong(Session["SiteId"]);
                    DataTable dtDiscount = objDiscountCodes.ValidateDiscountCode();

                    if (dtDiscount != null && dtDiscount.Rows.Count > 0)
                    {
                        Session["CouponCode"] = CCommon.ToString(dtDiscount.Rows[0]["vcCouponCode"]);
                        Session["OrderPromotionID"] = CCommon.ToString(dtDiscount.Rows[0]["numPromotionID"]);
                        Session["DiscountCodeID"] = CCommon.ToString(dtDiscount.Rows[0]["numDisocuntID"]);

                        txtCouponCode.Text = "";
                        lblCouponCode.Text = CCommon.ToString(dtDiscount.Rows[0]["vcCouponCode"]);
                    }
                }
                else
                {
                    Session["CouponCode"] = null;
                    Session["OrderPromotionID"] = null;
                    Session["DiscountCodeID"] = null;

                    Page.MaintainScrollPositionOnPostBack = false;
                    ShowMessage(GetErrorMessage("ERR005"), 1);//Enter Coupon Code
                }
            }
            catch (Exception ex)
            {
                Session["CouponCode"] = null;
                Session["OrderPromotionID"] = null;
                Session["DiscountCodeID"] = null;

                if (ex.Message.Contains("INVALID_COUPON_CODE"))
                {
                    Page.MaintainScrollPositionOnPostBack = false;
                    ShowMessage(GetErrorMessage("ERR011"), 1);
                    lblCouponCode.Text = "";
                    txtCouponCode.Text = "";
                }
                else if (ex.Message.Contains("COUPON_CODE_EXPIRED"))
                {
                    Page.MaintainScrollPositionOnPostBack = false;
                    ShowMessage(GetErrorMessage("ERR006"), 1);
                    lblCouponCode.Text = "";
                    txtCouponCode.Text = "";
                }
                else if (ex.Message.Contains("COUPON_USAGE_LIMIT_EXCEEDED"))
                {
                    Page.MaintainScrollPositionOnPostBack = false;
                    ShowMessage(GetErrorMessage("ERR008"), 1);
                    lblCouponCode.Text = "";
                    txtCouponCode.Text = "";
                }
                else
                {
                    ExceptionModule.ExceptionPublish(ex, Sites.ToLong(Session["DomainID"]), Sites.ToLong(Session["UserContactID"]), Sites.ToLong(Session["SiteId"]), Request);
                    Page.MaintainScrollPositionOnPostBack = false;
                    ShowMessage(ex.ToString(), 1);
                }
            }
        }

        protected void lbCouponCodeRemove_Click(object sender, EventArgs e)
        {
            try
            {
                if (CCommon.ToLong(Session["OrderPromotionID"]) > 0)
                {
                    BACRM.BusinessLogic.ShioppingCart.Cart objcart = new BACRM.BusinessLogic.ShioppingCart.Cart();
                    HttpCookie cookie = Request.Cookies["Cart"];
                    objcart.DomainID = CCommon.ToLong(Session["DomainID"]);
                    objcart.ContactId = CCommon.ToLong(Session["UserContactID"]);
                    objcart.CookieId = (cookie != null) ? cookie["KeyId"] : "";
                    objcart.PromotionID = CCommon.ToLong(Session["OrderPromotionID"]);
                    objcart.ClearCouponCodePromotion();
                }

                Session["CouponCode"] = null;
                Session["OrderPromotionID"] = null;
                Session["DiscountCodeID"] = null;
                lblCouponCode.Text = "";
                txtCouponCode.Text = "";

                this.isShippingMethodNeeded = true;
                bindHtml();
            }
            catch (Exception ex)
            {
                ExceptionModule.ExceptionPublish(ex, Sites.ToLong(Session["DomainID"]), Sites.ToLong(Session["UserContactID"]), Sites.ToLong(Session["SiteId"]), Request);
                Page.MaintainScrollPositionOnPostBack = false;

                ShowMessage(ex.ToString(), 1);
            }
        }

        protected void ddlShippingSpeedChoise_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {

                //After changing Shipping method in dropdown there is no need to 
                //refill dropdown again .Thats why i set below given variable...
                if (Sites.ToDouble(GetShippingCharge()) == -1)
                {
                    ddlShippingMethod.ClearSelection();
                    ddlShippingMethod.SelectedIndex = 0;

                }

                this.isShippingMethodNeeded = false;

                if (ddlShippingMethod.SelectedIndex != 0)
                {
                    Session["ShippingMethodText"] = ddlShippingMethod.SelectedItem.Text;
                    Session["ShippingMethodValue"] = ddlShippingMethod.SelectedItem.Value;
                }
                else
                {
                    Session["ShippingMethodText"] = "";
                    Session["ShippingMethodValue"] = "";
                }
            }
            catch (Exception ex)
            {
                ExceptionModule.ExceptionPublish(ex, Sites.ToLong(Session["DomainID"]), Sites.ToLong(Session["UserContactID"]), Sites.ToLong(Session["SiteId"]), Request);
                Page.MaintainScrollPositionOnPostBack = false;
                ////Page.ClientScript.RegisterStartupScript(Page.ClientScript.GetType(), Page.ClientID, "resetScrollPosition();", true);
                ShowMessage(ex.ToString(), 1);
            }

        }

        protected void ddlCountry_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {

                if (ddlCountry.SelectedIndex > 0)
                {
                    Session["CalculateShippingCountry"] = ddlCountry.SelectedValue;
                    ddlState.ClearSelection();
                    ddlState.Items.Clear();
                    FillState(ddlState, Sites.ToLong(ddlCountry.SelectedValue), Sites.ToLong(Session["DomainID"]));

                    if (!String.IsNullOrEmpty(hdnStateLong.Value))
                    {
                        if (ddlState.Items.FindByText(hdnStateLong.Value) != null)
                        {

                            ddlState.ClearSelection();
                            ddlState.Items.FindByText(hdnStateLong.Value).Selected = true;
                        }
                        else if (!String.IsNullOrEmpty(hdnStateShort.Value))
                        {
                            if (ddlState.Items.FindByText(hdnStateShort.Value) != null)
                            {

                                ddlState.ClearSelection();
                                ddlState.Items.FindByText(hdnStateShort.Value).Selected = true;
                            }
                        }
                    }
                    else if (!String.IsNullOrEmpty(hdnStateShort.Value))
                    {
                        if (ddlState.Items.FindByText(hdnStateShort.Value) != null)
                        {

                            ddlState.ClearSelection();
                            ddlState.Items.FindByText(hdnStateShort.Value).Selected = true;
                        }
                    }

                    // To Refill new shipping Method
                    this.isShippingMethodNeeded = true;

                }
                BindCart();
                bindHtml();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void ddlState_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Session["CalculateShippingState"] = ddlState.SelectedValue;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void chkResidential_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                Session["CalculateShippingResidential"] = chkResidential.Checked;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void txtPostal_TextChanged(object sender, EventArgs e)
        {
            try
            {
                Session["CalculateShippingZipCode"] = txtPostal.Text;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region Methods

        public void GetLocationByIP()
        {
            try
            {
                if (Sites.ToString(HttpContext.Current.Session["UserIPCountryID"]) == "" && ddlState.SelectedValue == "0")
                {
                    string ipAddress = "";
                    try
                    {
                        ipAddress = Request.UserHostAddress;
                        ExceptionModule.ExceptionPublish("Client IP KEY :"+ ipAddress, Sites.ToLong(Session["DomainID"]), Sites.ToLong(Session["UserContactID"]), Sites.ToLong(Session["SiteId"]), Request);
                    }
                    catch(Exception ex) {
                        ExceptionModule.ExceptionPublish(ex, Sites.ToLong(Session["DomainID"]), Sites.ToLong(Session["UserContactID"]), Sites.ToLong(Session["SiteId"]), Request);
                    }
                    //ipAddress = "74.125.45.100";
                    string APIKey = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["IPStackAccessKey"]);
                    string url = string.Format("http://api.ipstack.com/{0}?access_key={1}", ipAddress, APIKey);
                    using (WebClient client = new WebClient())
                    {
                        string json = client.DownloadString(url);
                        Location location = new JavaScriptSerializer().Deserialize<Location>(json); 
                        ExceptionModule.ExceptionPublish("Location Country Name :" + location.country_name + " , Region :" + location.region_name + " , ZIP Code :"+location.zip, Sites.ToLong(Session["DomainID"]), Sites.ToLong(Session["UserContactID"]), Sites.ToLong(Session["SiteId"]), Request);

                        if (location.country_name != "")
                        {

                            if (ddlCountry.Items.FindByText(location.country_name) != null)
                            {
                                ddlCountry.ClearSelection();
                                ddlCountry.Items.FindByText(location.country_name).Selected = true;
                                ddlCountry_SelectedIndexChanged(null, null);
                                DataTable dtTable = new DataTable();
                                UserAccess objUserAccess = new UserAccess();
                                objUserAccess.DomainID = Sites.ToLong(Session["DomainID"]);
                                objUserAccess.Country = Sites.ToLong(ddlCountry.SelectedValue);
                                objUserAccess.vcAbbreviations = location.region_name;
                                dtTable = objUserAccess.SelState();
                                if (dtTable.Rows.Count > 0)
                                {
                                    if (ddlState.Items.FindByValue(Sites.ToString(dtTable.Rows[0]["numStateID"])) != null)
                                    {
                                        ddlState.ClearSelection();
                                        ddlState.Items.FindByValue(Sites.ToString(dtTable.Rows[0]["numStateID"])).Selected = true;
                                        ddlState_SelectedIndexChanged(null, null);
                                    }
                                }
                                txtPostal.Text = location.zip;
                                txtPostal_TextChanged(null, null);
                                btnSubmit_Click(null, null);
                                HttpContext.Current.Session["UserIPCountryID"] = ddlCountry.SelectedValue;
                                HttpContext.Current.Session["UserIPStateID"] = ddlState.SelectedValue;
                                HttpContext.Current.Session["UserIPZipcode"] = txtPostal.Text;
                            }
                        }
                        else if (ddlCountry.SelectedValue != "0" && ddlState.SelectedValue != "0" && txtPostal.Text != "")
                        {
                            btnSubmit_Click(null, null);
                        }
                    }
                }
                else
                {
                    if (ddlState.SelectedIndex == 0 && ddlCountry.Items.FindByValue(Sites.ToString(HttpContext.Current.Session["UserIPCountryID"])) != null)
                    {
                        if (ddlCountry.Items.FindByValue(Sites.ToString(HttpContext.Current.Session["UserIPCountryID"])) != null)
                        {
                            ddlCountry.ClearSelection();
                            ddlCountry.Items.FindByValue(Sites.ToString(HttpContext.Current.Session["UserIPCountryID"])).Selected = true;
                            ddlCountry_SelectedIndexChanged(null, null);
                        }
                        if (ddlState.Items.FindByValue(Sites.ToString(HttpContext.Current.Session["UserIPStateID"])) != null)
                        {
                            ddlState.ClearSelection();
                            ddlState.Items.FindByValue(Sites.ToString(HttpContext.Current.Session["UserIPStateID"])).Selected = true;
                        }
                        txtPostal.Text = Sites.ToString(HttpContext.Current.Session["UserIPZipcode"]);
                        btnSubmit_Click(null, null);
                    }
                }
                
            }
            catch (Exception ex)
            {
                ExceptionModule.ExceptionPublish(ex, Sites.ToLong(Session["DomainID"]), Sites.ToLong(Session["UserContactID"]), Sites.ToLong(Session["SiteId"]), Request);
            }
        }

        void bindShippingPromotionOffer()
        {
            BACRM.BusinessLogic.ShioppingCart.Cart _cart = new BACRM.BusinessLogic.ShioppingCart.Cart();
            string uomid = Convert.ToString(0);
            DataTable dsPromotion = new DataTable();
            _cart.DomainID = Sites.ToLong(Session["DomainID"]);
            _cart.ItemCode = 0;
            _cart.UserCntID = CCommon.ToLong(Session["UserContactID"]);
            HttpCookie cookie = Request.Cookies["Cart"];
            if (Request.Cookies["Cart"] != null)
            {
                _cart.CookieId = (cookie["KeyId"] != null) ? cookie["KeyId"] : "";
            }
            else
            {
                _cart.CookieId = "";
            }
            dsPromotion = _cart.GetCartItemPromotionDetail(CCommon.ToLong(uomid), Convert.ToString(0), CCommon.ToLong(Session["DefCountry"]));
            if (dsPromotion.Rows.Count > 0)
            {
                qualifiedshipping = Convert.ToString(dsPromotion.Rows[0]["vcQualifiedShipping"]);
            }
        }

        #region HTML , Cart

        private void bindHtml()
        {
            try
            {
                bindShippingPromotionOffer();
                string strUI = GetUserControlTemplate("Cart.htm");

                string pattern = "<##FOR(?<Property>(\\b[^>]*?))##>(?<Content>([\\s\\S]*?))<##ENDFOR##>";
                string ShippingAddressPattern = "{##ShippingAddress##}(?<Content>([\\s\\S]*?)){/##ShippingAddress##}";
                string ShippingRatePattern = "{##ShippingRates##}(?<Content>([\\s\\S]*?)){/##ShippingRates##}";

                string RepeteTemplate = "";
                Hashtable htProperties;

                decimal TotalWeight = 0;
                decimal TotalCartAmount = 0;
                int CartItemsCount = 0;
                bool IsAllCartItemsFreeShipping = true;

                DataSet ds = new DataSet();
                ds = GetCartItem();

                if (Regex.IsMatch(strUI, pattern))
                {
                    MatchCollection matches = Regex.Matches(strUI, pattern);
                    if ((matches.Count > 0))
                    {

                        RepeteTemplate = matches[0].Groups["Content"].Value;
                        htProperties = GetPropertiesAsHastable(matches[0].Groups["Property"].Value.Replace("(", "").Replace(")", ""));

                        StringBuilder sb = new StringBuilder();


                        DataTable dt;
                        if (ds != null && ds.Tables.Count > 0)
                        {
                            dt = ds.Tables[0];
                            if (dt.Rows.Count > 0)
                            {


                                string strTemp = string.Empty;
                                int RowsCount = 0;
                                TextBox txtUnits;
                                Button btnDelete;
                                foreach (DataRow dr in dt.Rows)
                                {
                                    strTemp = RepeteTemplate;

                                    if (!string.IsNullOrEmpty(CCommon.ToString(dr["vcPathForTImage"])))
                                    {
                                        strTemp = strTemp.Replace("##ItemImage##", GetImagePath(CCommon.ToString(dr["vcPathForTImage"])));
                                    }
                                    else
                                    {
                                        strTemp = strTemp.Replace("##ItemImage##", "../Images/DefaultProduct.png");
                                    }
                                    strTemp = strTemp.Replace("##ItemURL##", CCommon.ToString(dr["ItemURL"]));
                                    strTemp = strTemp.Replace("##ItemName##", CCommon.ToString(dr["vcItemName"]));
                                    txtUnits = new TextBox();
                                    txtUnits.Attributes.Add("onkeypress", "javascript:CheckNumber(2,event);");
                                    txtUnits.ID = "txtUnits_" + CCommon.ToString(dr["numCartID"]) + "_" + CCommon.ToString(dr["numItemCode"]);
                                    txtUnits.Text = CCommon.ToString(dr["numUnitHour"]);
                                    txtUnits.Width = Unit.Pixel(20);
                                    txtUnits.CssClass = "textbox";
                                    strTemp = strTemp.Replace("##ItemUnitsTextBox##", CCommon.RenderControl(txtUnits));
                                    if (Sites.ToBool(Session["HidePrice"]) == false)
                                    {
                                        strTemp = strTemp.Replace("##ItemPricePerUnit##", Sites.ToString(Session["CurrSymbol"]) + " " + string.Format("{0:#,##0.00}", dr["monPrice"]));
                                        //  strTemp = strTemp.Replace("##TotalAmount##", string.Format("{0:#,##0.00}", (CCommon.ToDecimal(dr["monTotAmount"]) == 0 ? CCommon.ToDecimal(dr["monTotAmount"]) : CCommon.ToDecimal(dr["monTotAmount"]) / CCommon.ToDecimal(Session["ExchangeRate"]))));
                                        strTemp = strTemp.Replace("##TotalAmount##", Sites.ToString(Session["CurrSymbol"]) + " " + string.Format("{0:#,##0.00}", (CCommon.ToDecimal(dr["monTotAmount"]))));
                                    }
                                    else
                                    {
                                        strTemp = strTemp.Replace("##ItemPricePerUnit##", "NA");
                                        strTemp = strTemp.Replace("##TotalAmount##", "NA");
                                    }

                                    if (Sites.ToBool(dr["bitDiscountType"]))
                                    {
                                        strTemp = strTemp.Replace("##Discount##", Sites.ToString(Session["CurrSymbol"]) + " " + string.Format("{0:#,##0.00}", dr["fltDiscount"]));
                                    }
                                    else
                                    {
                                        strTemp = strTemp.Replace("##Discount##", Sites.ToString(Session["CurrSymbol"]) + " " + string.Format("{0:#,##0.00}", Sites.ToDouble(dr["monTotAmtBefDiscount"]) - Sites.ToDouble(dr["monTotAmount"])));
                                    }


                                    strTemp = strTemp.Replace("##PromotionDesc##", Sites.ToString(dr["PromotionDesc"]));
                                    strTemp = strTemp.Replace("##ItemModelID##", CCommon.ToString(dr["vcModelID"]));
                                    strTemp = strTemp.Replace("##ItemID##", CCommon.ToString(dr["numItemCode"]));
                                    strTemp = strTemp.Replace("##ItemAttributes##", CCommon.ToString(dr["vcAttributes"]));
                                    strTemp = strTemp.Replace("##InclusionDetails##", CCommon.ToString(dr["vcInclusionDetails"]));
                                    strTemp = strTemp.Replace("##SKU##", CCommon.ToString(dr["vcSKU"]));

                                    //strTemp = strTemp.Replace("##ShippingMethod##", CCommon.ToString(dr["vcShippingMethod"]));

                                    btnDelete = new Button();
                                    btnDelete.Text = "X";
                                    btnDelete.CssClass = "button";
                                    btnDelete.Attributes.Add("onclick", "return DeleteRecord('" + CCommon.ToString(dr["numCartId"]) + "');");
                                    strTemp = strTemp.Replace("##DeleteItemButton##", CCommon.RenderControl(btnDelete));


                                    if (RowsCount % 2 == 0)
                                        strTemp = strTemp.Replace("##RowClass##", Sites.ToString(htProperties["AlternatingItemClass"]));
                                    else
                                        strTemp = strTemp.Replace("##RowClass##", Sites.ToString(htProperties["ItemClass"]));


                                    //freeshipping
                                    if (Sites.ToString(dr["bitFreeShipping"]) == "True")
                                    {
                                        TotalWeight = TotalWeight + 0;
                                        TotalCartAmount = TotalCartAmount + 0;
                                        CartItemsCount = CartItemsCount + 0;
                                    }
                                    else // shipping based on shipping rule 
                                    {
                                        TotalWeight = TotalWeight + Sites.ToDecimal(dr["numWeight"]);
                                        TotalCartAmount = TotalCartAmount + Sites.ToDecimal(dr["monTotAmount"]);
                                        CartItemsCount = CartItemsCount + CCommon.ToInteger(dr["numUnitHour"]); ;
                                        IsAllCartItemsFreeShipping = false;
                                    }
                                    sb.Append(strTemp);

                                    RowsCount++;
                                }
                            }
                            strUI = Regex.Replace(strUI, pattern, sb.ToString());
                        }
                    }
                }

                //strUI = strUI.Replace("##ContinueShoppingButton##", CCommon.RenderControl(btnContinue));
                //strUI = strUI.Replace("##UpdateCartButton##", CCommon.RenderControl(btnUpdateCart));
                //strUI = strUI.Replace("##CheckoutButton##", CCommon.RenderControl(btnCheckOut));
                strUI = strUI.Replace("##CouponCodeTextBox##", CCommon.RenderControl(txtCouponCode));
                strUI = strUI.Replace("##ApplyCouponCodeButton##", CCommon.RenderControl(btnCouponCode));
                strUI = strUI.Replace("##RemoveCouponCodeLink##", CCommon.RenderControl(lbCouponCodeRemove));
                strUI = strUI.Replace("##CouponCodeLabel##", lblCouponCode.Text);
                if (Sites.ToString(qualifiedshipping) != "")
                {
                    strUI = strUI.Replace("##PromotionShippingDescription##", "<img height=\"35px\"  src=\"" + Sites.ToString(Session["SitePath"]) + "/images/TruckGreen.png\" />" + Sites.ToString(qualifiedshipping));
                }
                else
                {
                    strUI = strUI.Replace("##PromotionShippingDescription##", qualifiedshipping);
                }
                #region Shipping Calculation And Bind Shipping Fropdown
                string strShippingCharge = "0.00";

                if (this.reCalculation)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        if (isShippingMethodNeeded)
                        {

                            bool ShowShippingAmount = false;
                            if (strUI.Contains("##ShowShippingAmountWithMethod##"))
                            {
                                ShowShippingAmount = true;
                                strUI = strUI.Replace("##ShowShippingAmountWithMethod##", "");
                            }
                            GetShippingMethod(ShowShippingAmount, TotalWeight, TotalCartAmount, CartItemsCount, IsAllCartItemsFreeShipping);
                        }
                        else
                        {

                            if (strUI.Contains("##ShowShippingAmountWithMethod##"))
                            {
                                strUI = strUI.Replace("##ShowShippingAmountWithMethod##", "");
                            }

                        }
                    }
                    else
                    {
                        ddlShippingMethod.Items.Clear();
                        ddlShippingMethod.Items.Insert(0, new ListItem("--Select--", "0~0.00~0"));

                        if (strUI.Contains("##ShowShippingAmountWithMethod##"))
                        {
                            strUI = strUI.Replace("##ShowShippingAmountWithMethod##", "");
                        }
                    }
                    strUI = Sites.ReplaceMatchedPatternWithContent(strUI, ShippingRatePattern);
                    strUI = Sites.RemoveMatchedPattern(strUI, ShippingAddressPattern);
                    strUI = strUI.Replace("##lblCountry##", ddlCountry.SelectedItem.Text);
                    strUI = strUI.Replace("##lblState##", ddlState.SelectedItem.Text);
                    strUI = strUI.Replace("##lblZipPostalCode##", txtPostal.Text);
                    strUI = strUI.Replace("##ShippingMethodName##", CCommon.RenderControl(ddlShippingMethod));

                    if (Sites.ToBool(Session["HidePrice"]) == false)
                    {
                        strShippingCharge = GetShippingCharge();
                        if (strShippingCharge == "")
                        {
                            strShippingCharge = "0.00";
                        }
                        strUI = strUI.Replace("##ShippingChargeLabel##", Sites.ToString(Session["CurrSymbol"]) + " " + string.Format("{0:#,##0.00}", strShippingCharge));//Delete this label lblShippingCost.Text
                    }
                    else
                    {
                        strUI = strUI.Replace("##ShippingChargeLabel##", "NA");
                    }
                }
                else
                {
                    if (Sites.ToBool(Session["HidePrice"]) == false)
                    {
                        strUI = strUI.Replace("##ShippingChargeLabel##", Sites.ToString(Session["CurrSymbol"]) + " 0.00");
                    }
                    else
                    {
                        strUI = strUI.Replace("##ShippingChargeLabel##", "NA");
                    }


                    strUI = Sites.ReplaceMatchedPatternWithContent(strUI, ShippingAddressPattern);
                    strUI = Sites.RemoveMatchedPattern(strUI, ShippingRatePattern);
                    strUI = strUI.Replace("##lblPartnerCode##", (lblPartnerCode != null ? CCommon.RenderControl(lblPartnerCode) : ""));
                    strUI = strUI.Replace("##CountryDropDown##", CCommon.RenderControl(ddlCountry));
                    strUI = strUI.Replace("##StateDropDown##", CCommon.RenderControl(ddlState));
                    strUI = strUI.Replace("##ZipPostalCode##", CCommon.RenderControl(txtPostal));
                    strUI = strUI.Replace("##IsResidential##", CCommon.RenderControl(chkResidential));
                    strUI = strUI.Replace("##ReCalculate##", CCommon.RenderControl(btnSubmit));

                }
                #endregion


                if (Sites.ToBool(Session["HidePrice"]) == false)
                {
                    strUI = strUI.Replace("##SubTotalLabel##", Sites.ToString(Session["CurrSymbol"]) + " " + lblSubTotal.Text);
                    strUI = strUI.Replace("##DiscountLabel##", Sites.ToString(Session["CurrSymbol"]) + " " + lblDiscount.Text);
                    strUI = strUI.Replace("##TaxLabel##", Sites.ToString(Session["CurrSymbol"]) + " " + lblTax.Text);
                    strUI = strUI.Replace("##TotalAmountLabel##", Sites.ToString(Session["CurrSymbol"]) + " " + string.Format("{0:#,##0.00}", Convert.ToDecimal(Convert.ToDecimal(lblTotal.Text) + Convert.ToDecimal(strShippingCharge))));
                }
                else
                {
                    strUI = strUI.Replace("##SubTotalLabel##", "NA");
                    strUI = strUI.Replace("##DiscountLabel##", "NA");
                    strUI = strUI.Replace("##TaxLabel##", "NA");
                    strUI = strUI.Replace("##TotalAmountLabel##", "NA");
                }
                string EnterCouponPattern, UsedCouponPattern;
                EnterCouponPattern = "{##EnterCouponSection##}(?<Content>([\\s\\S]*?)){/##EnterCouponSection##}";
                UsedCouponPattern = "{##UsedCouponSection##}(?<Content>([\\s\\S]*?)){/##UsedCouponSection##}";
                if (Sites.ToString(Session["CouponCode"]).Length > 2)
                {
                    //Hide Other Template
                    strUI = Sites.RemoveMatchedPattern(strUI, EnterCouponPattern);
                    //Replace disply template ##Tags## with content value
                    strUI = Sites.ReplaceMatchedPatternWithContent(strUI, UsedCouponPattern);
                }
                else
                {
                    //Hide Other Template
                    strUI = Sites.RemoveMatchedPattern(strUI, UsedCouponPattern);
                    //Replace disply template ##Tags## with content value
                    strUI = Sites.ReplaceMatchedPatternWithContent(strUI, EnterCouponPattern);
                }

                if (IsOnlySalesInquiry() == 1 && Sites.ToBool(Session["SkipStep2"]) == true)
                {
                    strUI = strUI.Replace("id=" + '"' + "trApplyCouponCode" + '"', "id=" + '"' + "trApplyCouponCode" + '"' + " style=" + '"' + "display:none;" + '"');
                    strUI = strUI.Replace("id=" + '"' + "trUsedCouponCode" + '"', "id=" + '"' + "trUsedCouponCode" + '"' + " style=" + '"' + "display:none;" + '"');
                    strUI = strUI.Replace("id=" + '"' + "tblShippingAddress" + '"', "id=" + '"' + "tblShippingAddress" + '"' + " style=" + '"' + "display:none;" + '"');
                }

                //Final Binding of html ...
                pnlCutomizeHtml.Visible = false;
                litCutomizeHtml.Text = strUI;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void BindCart()
        {
            try
            {
                //DataTable dttable = default(DataTable);
                DataSet dsItems = default(DataSet);
                dsItems = GetCartItem();
                if (dsItems.Tables[0].Rows.Count > 0)
                {
                    Session["TotalDiscount"] = null;
                    Session["ProID"] = null;
                    lblCouponCode.Text = "";



                    DataTable dttable = default(DataTable);
                    DataTable dtTableShippingAddress = default(DataTable);

                    dttable = dsItems.Tables[0];

                    decimal Tax = default(decimal);
                    decimal SubTotal = default(decimal);
                    decimal Discount = default(decimal);

                    dtTableShippingAddress = GetShippingAddress();

                    dttable = dsItems.Tables[0];
                    if (dttable.Rows.Count > 0)
                    {
                        foreach (DataRow dr in dttable.Rows)
                        {
                            if (Sites.ToLong(dr["numItemCode"]) != Sites.ToLong(Session["ShippingServiceItemID"]) && Sites.ToLong(dr["numItemCode"]) != Sites.ToLong(Session["DiscountServiceItemID"]))
                            {
                                SubTotal += Sites.ToDecimal(dr["monTotAmtBefDiscount"]);
                            }
                        }

                        Discount = Sites.ToDecimal(dttable.Compute("SUM(monTotalDiscountAmount)", ""));
                        if (dttable.Select("numItemCode=" + CCommon.ToString(Session["DiscountServiceItemID"])).Length > 0)
                        {
                            Discount = Discount + Sites.ToDecimal(dttable.Select("numItemCode=" + CCommon.ToString(Session["DiscountServiceItemID"]))[0]["monTotAmount"]) * -1;
                        }

                        if (dtTableShippingAddress.Rows.Count > 0)
                        {
                            Tax = GetTaxForCartItems(CCommon.ToLong(Session["DomainID"]), CCommon.ToLong(Session["DivId"]), CCommon.ToInteger(Session["BaseTaxCalcOn"]), CCommon.ToInteger(Session["BaseTaxCalcOn"]), CCommon.ToInteger(dtTableShippingAddress.Rows[0]["numCountry"]), CCommon.ToInteger(dtTableShippingAddress.Rows[0]["numState"]), CCommon.ToString(dtTableShippingAddress.Rows[0]["vcCity"]), txtPostal.Text, Sites.ToLong(Session["UserContactID"]));
                        }
                    }

                    lblSubTotal.Text = string.Format("{0:#,##0.00}", SubTotal);
                    lblDiscount.Text = string.Format("{0:#,##0.00}", Discount);
                    lblTax.Text = string.Format("{0:#,##0.00}", Tax);
                    lblTotal.Text = string.Format("{0:#,##0.00}", SubTotal + Tax - Discount);
                    //lblShippingCost.Text = string.Format("{0:#,##0.00}", decShippingCharge); //change;

                    if (Session["CouponCode"] != null)
                    {
                        trApplyCouponCode.Visible = false;
                        trUsedCouponCode.Visible = true;
                        lblCouponCode.Text = Session["CouponCode"].ToString();
                    }
                    else
                    {
                        trApplyCouponCode.Visible = true;
                        trUsedCouponCode.Visible = false;
                    }
                }
                else
                {
                    Session["ShippingMethodValue"] = null;
                    lblTotal.Text = "0.00";
                    lblShippingCost.Text = "0.00";
                    lblDiscount.Text = "0.00";
                    lblTax.Text = "0.00";
                    lblCouponCode.Text = "0.00";
                    lblSubTotal.Text = "0.00";

                }
                Session["gblItemCount"] = null;
                Session["gblTotalAmount"] = null;


            }
            catch (Exception ex)
            {
                throw ex;
            }
            //GetShippingRates()
        }

        #endregion

        #region Shipping
        private DataTable GetShippingAddress()
        {
            try
            {
                CContacts objContact = new CContacts();
                DataTable dtTable;
                objContact.AddressType = CContacts.enmAddressType.ShipTo;
                objContact.DomainID = Sites.ToLong(Session["DomainID"]);

                objContact.AddresOf = CContacts.enmAddressOf.Organization;
                if (CCommon.ToString(Session["ShipAddress"]) != "")
                {
                    objContact.byteMode = 1;
                    objContact.AddressID = CCommon.ToLong(Session["ShipAddress"]);
                }
                else
                {
                    objContact.byteMode = 3;
                    objContact.RecordID = Sites.ToLong(Session["DivId"]);
                }
                dtTable = objContact.GetAddressDetail();
                if (dtTable.Rows.Count > 0)
                {
                    return dtTable;
                }
                else
                {
                    return new DataTable();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private string GetShippingCharge()
        {
            try
            {
                double ShippingCharge = GetShippingChargebyPromotion();
                if (ShippingCharge > 0)
                {
                    return string.Format("{0:#,##0.00}", Sites.ToDecimal(ShippingCharge));
                }
                else if (ddlShippingMethod.SelectedIndex != 0)
                {
                    string[] strShippingCharge = ddlShippingMethod.SelectedValue.Split('~');
                    if (strShippingCharge.Length > 1)
                    {
                        return string.Format("{0:#,##0.00}", CCommon.ToDecimal(strShippingCharge[1]));
                    }
                }
                return "0.00";
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        private void GetShippingMethod(Boolean ShowShippingAmount, decimal TotalWeight, decimal TotalCartAmount, int CartItemsCount, bool IsAllCartItemsFreeShipping)
        {
            try
            {
                DataTable dtShippingMethod = new DataTable();
                DataSet dsShoppingCart = new DataSet();
                string strMessage = "";

                if (!IsAllCartItemsFreeShipping)
                {
                    #region Get Shipping Rule



                    DataTable dtTable = null;
                    CContacts objContact = new CContacts();
                    objContact.DomainID = Sites.ToLong(Session["DomainID"]);
                    objContact.AddressID = Sites.ToLong(Session["ShipAddress"]);
                    objContact.byteMode = 1;
                    dtTable = objContact.GetAddressDetail();

                    ShippingRule objRule = new ShippingRule();
                    objRule.ItemID = 3;
                    objRule.DomainID = CCommon.ToLong(Session["DomainID"]);
                    objRule.SiteID = CCommon.ToLong(Session["SiteID"]);
                    objRule.DivisionID = Sites.ToLong(Session["DivisionID"]);
                    objRule.RelationshipID = Sites.ToLong(Session["RelationShip"]);
                    objRule.ProfileID = Sites.ToLong(Session["Profile"]);
                    objRule.Weight = CCommon.ToDouble(TotalWeight);

                    objRule.ZipCode = Sites.ToString(txtPostal.Text);   //Sites.ToString(dtTable.Rows[0]["vcPostalCode"]);
                    if (ddlCountry.SelectedIndex != 0 && ddlState.SelectedIndex != 0 && txtPostal.Text != "")
                    {
                        objRule.CountryID = Sites.ToLong(ddlCountry.SelectedValue);
                        objRule.StateID = Sites.ToLong(ddlState.SelectedValue);
                        objRule.ZipCode = Sites.ToString(txtPostal.Text);
                    }

                    //Get Shipping Method and Shipping Rule Data ...

                    //objRule.GetShippingMethodForItem(ddlShippingMethod, ShowShippingAmount);
                    dtShippingMethod = objRule.GetShippingMethodForItem1();

                    //To get itemcount for Flat Rate Per Item method ...
                    dsShoppingCart = GetCartItem();

                    #endregion

                    #region Get Shipping Method only when one Of Items in Cart having Free Shipping = false
                    if (dtShippingMethod.Rows.Count > 0)
                    {
                        //Default Column Add
                        System.Data.DataColumn newColumn = new System.Data.DataColumn("IsShippingRuleValid", typeof(System.Boolean));
                        newColumn.DefaultValue = true;
                        dtShippingMethod.Columns.Add(newColumn);


                        //Add this column to store it as a value field in the dropdown of ddlShippingMethod
                        CCommon.AddColumnsToDataTable(ref dtShippingMethod, "vcServiceTypeID1");

                        #region foreach start

                        decimal subtotalItemClassificationAmount = 0;
                        foreach (DataRow dr in dtShippingMethod.Rows)
                        {
                            Decimal decShipingAmount = 0;
                            bool IsValid = false;
                            if (Sites.ToInteger(dr["intNsoftEnum"]) == 0)
                            {
                                if (Sites.ToLong(dr["numSiteID"]) == CCommon.ToLong(Session["SiteID"]))
                                {
                                    ShippingRule _objShippingRule = new ShippingRule();
                                    _objShippingRule.RuleID = CCommon.ToLong(dr["numItemClassificationRuleID"]);
                                    _objShippingRule.DomainID = CCommon.ToLong(Session["DomainID"]);
                                    DataTable dtShippingRuleStates = new DataTable();
                                    dtShippingRuleStates = _objShippingRule.GetShippingRuleStates();
                                    var countryData = dtShippingRuleStates.Select(" numCountryID = " + Sites.ToDecimal(ddlCountry.SelectedValue) + " AND (numStateID = " + Sites.ToDecimal(ddlState.SelectedValue) + " OR numStateID = 0)").Length;
                                    //var countryData = (from myRow in dtShippingRuleStates.AsEnumerable()
                                    //                  where myRow.Field<decimal>("numCountryID") == Sites.ToDecimal(ddlCountry.SelectedValue)
                                    //                  select myRow);
                                    if (countryData> 0)
                                    {
                                        if (Convert.ToBoolean(dr["bitItemClassification"]) == true)
                                        {
                                            decimal totalItemClassificationAmount = 0;
                                            var ItemClassifications = Sites.ToString(dr["vcItemClassification"]).Split(',');
                                            foreach (var d in ItemClassifications)
                                            {
                                                if (d != "")
                                                {
                                                    totalItemClassificationAmount = Sites.ToDecimal(dsShoppingCart.Tables[0].Compute("SUM(monTotAmount)", "numItemClassification = '" + d + "' "));
                                                    //if (d == Sites.ToString(drItems["numItemClassification"]))
                                                    //{
                                                    //    totalItemClassificationAmount = totalItemClassificationAmount + Sites.ToDecimal(drItems["monTotAmount"]);
                                                    //}
                                                    if (totalItemClassificationAmount > 0)
                                                    {
                                                        if (totalItemClassificationAmount >= CCommon.ToDecimal(dr["intFrom"]) && totalItemClassificationAmount <= CCommon.ToDecimal(dr["intTo"]))
                                                        {
                                                            IsValid = false;
                                                            decShipingAmount = Sites.ToDecimal(dr["monRate"]) / Sites.ToDecimal(HttpContext.Current.Session["ExchangeRate"].ToString());
                                                            dr["vcServiceTypeID1"] = Sites.ToString(dr["numServiceTypeID"]) + "~" + CCommon.ToString(decShipingAmount) + "~0~0~0~" + CCommon.ToString(dr["vcServiceName"]);
                                                            if (ShowShippingAmount)
                                                            {
                                                                dr["vcServiceName"] = Sites.ToString(dr["vcServiceName"]) + " - " + Sites.ToString(HttpContext.Current.Session["CurrSymbol"]) + " " + String.Format("{0:#,##0.00}", decShipingAmount);
                                                            }
                                                            else
                                                            {
                                                                dr["vcServiceName"] = Sites.ToString(dr["vcServiceName"]);
                                                            }
                                                            subtotalItemClassificationAmount = subtotalItemClassificationAmount + decShipingAmount;
                                                        }
                                                    }
                                                    else
                                                    {
                                                        IsValid = false;
                                                    }
                                                }
                                            }
                                           
                                        }
                                        else
                                        {
                                            if (TotalCartAmount >= CCommon.ToDecimal(dr["intFrom"]) && TotalCartAmount <= CCommon.ToDecimal(dr["intTo"]))
                                            {
                                                IsValid = true;
                                                decShipingAmount = Sites.ToDecimal(dr["monRate"]) / Sites.ToDecimal(HttpContext.Current.Session["ExchangeRate"].ToString());
                                                dr["vcServiceTypeID1"] = Sites.ToString(dr["numServiceTypeID"]) + "~" + CCommon.ToString(decShipingAmount) + "~0~0~0~" + CCommon.ToString(dr["vcServiceName"]);
                                                if (ShowShippingAmount)
                                                {
                                                    dr["vcServiceName"] = Sites.ToString(dr["vcServiceName"]) + " - " + Sites.ToString(HttpContext.Current.Session["CurrSymbol"]) + " " + String.Format("{0:#,##0.00}", decShipingAmount);
                                                }
                                                else
                                                {
                                                    dr["vcServiceName"] = Sites.ToString(dr["vcServiceName"]);
                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        IsValid = false;
                                    }
                                }
                                else
                                {
                                    IsValid = false;
                                }

                                dr["IsShippingRuleValid"] = IsValid;
                            }
                            else
                            {
                                #region Real Time Shipping Quotes
                                //dsShoppingCart.Tables.Add(GetCartItem().Tables[0].Select("bitFreeShipping=false").CopyToDataTable());

                                if (!IsAllCartItemsFreeShipping && TotalWeight > 0)
                                {
                                    #region "If All Items are not free shipping"

                                    #region Get Warehouse
                                    CItems objItem = new CItems();
                                    DataTable dtTableWarehouse = null;
                                    objItem.WarehouseID = Sites.ToLong(HttpContext.Current.Session["DefaultWareHouseID"]);
                                    objItem.DomainID = Sites.ToLong(HttpContext.Current.Session["DomainID"]);
                                    dtTableWarehouse = objItem.GetWareHouses();
                                    #endregion

                                    #region Warehouse Checking
                                    if (dtTableWarehouse.Rows.Count > 0)
                                    {
                                        if (Sites.ToLong(dtTableWarehouse.Rows[0]["numWCountry"]) > 0 && Sites.ToLong(dtTableWarehouse.Rows[0]["numWState"]) > 0)
                                        {
                                            #region Get Shipping Rates when Warehouse Configured

                                            #region Get BizDocs
                                            OppBizDocs objOppBizDocs = new OppBizDocs();
                                            objOppBizDocs.ShipCompany = Sites.ToInteger(dr["numShippingCompanyID"]);


                                            objOppBizDocs.ShipFromCountry = Sites.ToLong(dtTableWarehouse.Rows[0]["numWCountry"]);
                                            objOppBizDocs.ShipFromState = Sites.ToLong(dtTableWarehouse.Rows[0]["numWState"]);
                                            objOppBizDocs.ShipToCountry = Sites.ToLong(ddlCountry.SelectedValue);
                                            objOppBizDocs.ShipToState = Sites.ToLong(ddlState.SelectedValue);
                                            objOppBizDocs.strShipFromCountry = "";
                                            objOppBizDocs.strShipFromState = "";
                                            objOppBizDocs.strShipToCountry = "";
                                            objOppBizDocs.strShipToState = "";
                                            objOppBizDocs.GetShippingAbbreviation();
                                            #endregion

                                            if (TotalWeight >= CCommon.ToDecimal(dr["intFrom"]) && TotalWeight <= CCommon.ToDecimal(dr["intTo"]))
                                            {
                                                IsValid = true;
                                            }

                                            if (IsValid)
                                            {
                                                #region Get Shipping Rates
                                                Shipping objShipping = new Shipping();
                                                objShipping.DomainID = CCommon.ToLong(HttpContext.Current.Session["DomainID"]);
                                                objShipping.WeightInLbs = CCommon.ToDouble(TotalWeight);
                                                objShipping.NoOfPackages = 1;
                                                objShipping.UseDimentions = Sites.ToInteger(dr["numShippingCompanyID"]) == 91 ? true : false;
                                                //for fedex dimentions are must
                                                objShipping.GroupCount = 1;
                                                if (objShipping.UseDimentions)
                                                {
                                                    objShipping.Height = objRule.Height;
                                                    objShipping.Width = objRule.Width;
                                                    objShipping.Length = objRule.Length;
                                                }
                                                objShipping.SenderState = objOppBizDocs.strShipFromState;
                                                objShipping.SenderZipCode = Sites.ToString(dtTableWarehouse.Rows[0]["vcWPinCode"]);
                                                objShipping.SenderCountryCode = objOppBizDocs.strShipFromCountry;

                                                objShipping.RecepientState = objOppBizDocs.strShipToState;
                                                objShipping.RecepientZipCode = objRule.ZipCode;
                                                objShipping.RecepientCountryCode = objOppBizDocs.strShipToCountry;

                                                objShipping.ServiceType = short.Parse(CCommon.ToString(dr["intNsoftEnum"]));
                                                objShipping.PackagingType = 21;
                                                //ptYourPackaging 
                                                objShipping.ItemCode = objRule.ItemID;
                                                objShipping.OppBizDocItemID = 0;
                                                objShipping.ID = 9999;
                                                objShipping.Provider = CCommon.ToInteger(dr["numShippingCompanyID"]);

                                                DataTable dtShipRates = new DataTable();
                                                if (string.Compare(objShipping.SenderCountryCode, objShipping.RecepientCountryCode) == 0)
                                                {
                                                    dtShipRates = objShipping.GetRates(false);//new DataTable(); 
                                                }
                                                else
                                                {
                                                    dtShipRates = objShipping.GetRates(true);//new DataTable(); 
                                                }

                                                //objShipping.ErrorMsg = "abcd";//Delete
                                                if (!string.IsNullOrEmpty(objShipping.ErrorMsg))
                                                {
                                                    //log for error Message strMessage = objShipping.ErrorMsg;
                                                    if (objShipping.ErrorMsg.Contains("612") || objShipping.ErrorMsg.Contains("Weight below minimum requirement of 151 LB.") || objShipping.ErrorMsg.Contains(" The selected service type is not valid"))
                                                    {
                                                        dr["vcServiceName"] = CCommon.ToString(dr["vcServiceName"]) + " - " + "Not Supported";
                                                        dr["vcServiceTypeID1"] = CCommon.ToString(dr["numServiceTypeID"]) + "~" + "-1" + "~" + CCommon.ToString(dr["numRuleID"]) + "~" + CCommon.ToString(dr["numShippingCompanyID"]) + "~" + CCommon.ToString(dr["vcServiceName"]);
                                                    }
                                                    else if (objShipping.ErrorMsg.Contains("836") || objShipping.ErrorMsg.Contains("Destination Postal-State Mismatch."))
                                                    {
                                                        //ShowMessage(GetErrorMessage("ERR071"), 1);
                                                        ShowMessage("Please enter a valid postal code in the address.", 1);
                                                        dtShippingMethod.Clear();
                                                        this.reCalculation = false;
                                                        break;
                                                    }
                                                }
                                                else
                                                {
                                                    #region Calculation of shipping Rates

                                                    if (dtShipRates.Rows.Count > 0)
                                                    {
                                                        if (Sites.ToDecimal(dtShipRates.Rows[0]["Rate"]) > 0)
                                                        {
                                                            //Currency conversion of shipping rate
                                                            decShipingAmount = Sites.ToDecimal(dtShipRates.Rows[0]["Rate"]) / Sites.ToDecimal(HttpContext.Current.Session["ExchangeRate"].ToString());

                                                            if (Sites.ToBool(dr["bitMarkupType"]) == true && Sites.ToDecimal(dr["fltMarkup"]) > 0)
                                                            {
                                                                dr["fltMarkup"] = decShipingAmount / CCommon.ToDecimal(dr["fltMarkup"]);
                                                            }
                                                            //Percentage
                                                            decShipingAmount = decShipingAmount + CCommon.ToDecimal(dr["fltMarkup"]);

                                                            if (ShowShippingAmount)
                                                            {
                                                                dr["vcServiceName"] = CCommon.ToString(dr["vcServiceName"]) + " - " + CCommon.ToString(Session["CurrSymbol"]) + " " + string.Format("{0:#,##0.00}", decShipingAmount);
                                                            }
                                                            dr["vcServiceTypeID1"] = CCommon.ToString(dr["numServiceTypeID"]) + "~" + CCommon.ToString(decShipingAmount) + "~" + CCommon.ToString(dr["numRuleID"]) + "~" + CCommon.ToString(dr["numShippingCompanyID"]) + "~" + CCommon.ToString(dr["vcServiceName"]);
                                                        }
                                                    }
                                                    else
                                                    {
                                                        dr["IsShippingRuleValid"] = false;
                                                    }

                                                    #endregion
                                                }
                                                #endregion
                                            }
                                            else
                                            {
                                                dr["IsShippingRuleValid"] = false;
                                            }
                                            #endregion
                                        }
                                        else
                                        {
                                            Page.MaintainScrollPositionOnPostBack = false;
                                            //Page.ClientScript.RegisterStartupScript(Page.ClientScript.GetType(), Page.ClientID, "resetScrollPosition();", true);
                                            ShowMessage(GetErrorMessage("ERR069"), 1);// your merchant need to provide valid ship from warehouse address to calculate shipping amount,please contact site administrator.
                                        }
                                    }
                                    else
                                    {
                                        Page.MaintainScrollPositionOnPostBack = false;
                                        //Page.ClientScript.RegisterStartupScript(Page.ClientScript.GetType(), Page.ClientID, "resetScrollPosition();", true);
                                        ShowMessage(GetErrorMessage("ERR069"), 1);// your merchant need to provide valid ship from warehouse address to calculate shipping amount,please contact site administrator.
                                    }
                                    #endregion
                                    #endregion
                                }
                                else
                                {
                                    //Set for not including it as rule if weight is 0 in the case of real Time Shipping Quotes .
                                    dr["IsShippingRuleValid"] = false;
                                }
                                #endregion
                            }
                        }
                        if (subtotalItemClassificationAmount > 0)
                        {
                            DataRow drClassificationRow;
                            drClassificationRow = dtShippingMethod.NewRow();
                            drClassificationRow["vcServiceTypeID1"] = 0 + "~" + CCommon.ToString(subtotalItemClassificationAmount) + "~0~0~0~" + "-";
                            if (ShowShippingAmount)
                            {
                                drClassificationRow["vcServiceName"] = " - "+ " - " + Sites.ToString(HttpContext.Current.Session["CurrSymbol"]) + " " + String.Format("{0:#,##0.00}", subtotalItemClassificationAmount);
                            }
                            else
                            {
                                drClassificationRow["vcServiceName"] = "-";
                            }
                            drClassificationRow["IsShippingRuleValid"] = true;
                            dtShippingMethod.Rows.Add(drClassificationRow);
                        }
                        #endregion

                        #region Select only those Shipping Rules whose IsShippingRuleValid(New Added Column) = true
                        DataRow[] datarows = dtShippingMethod.Select("IsShippingRuleValid=true");
                        if (datarows.Length > 0)
                            dtShippingMethod = datarows.CopyToDataTable();
                        else
                            dtShippingMethod = new DataTable();
                        #endregion

                    }
                    #endregion
                }

                #region Bind Shipping Cost
                if (dtShippingMethod.Rows.Count == 0)
                {
                    ddlShippingMethod.Items.Clear();  // It is necessary otherwise it will show dublicate records .

                    ListItem list = new ListItem();
                    list.Selected = true;
                    list.Text = "NA - $0";
                    list.Value = "0~0.00";
                    ddlShippingMethod.Items.Add(list);
                    ddlShippingMethod.ClearSelection();
                }
                else
                {
                    ddlShippingMethod.Items.Clear();

                    ddlShippingMethod.DataTextField = "vcServiceName";
                    ddlShippingMethod.DataValueField = "vcServiceTypeID1";
                    ddlShippingMethod.DataSource = dtShippingMethod;
                    ddlShippingMethod.DataBind();

                    if (chkResidential.Checked)
                    {
                        foreach (ListItem item in ddlShippingMethod.Items)
                        {
                            if (item.Value.StartsWith("3702~"))
                            {
                                ddlShippingMethod.Items.Remove(item);
                                break;
                            }
                        }
                    }
                }
                ddlShippingMethod.Items.Insert(0, new ListItem("--Select One--", "0~0.00~0"));
                #endregion

                #region Select Default Shipping Rule
                if (ddlShippingMethod.Items.Count == 2)
                {
                    ddlShippingMethod.SelectedIndex = 1;
                    if (Sites.ToDouble(GetShippingCharge()) == -1)
                    {
                        ddlShippingMethod.ClearSelection();
                        ddlShippingMethod.SelectedIndex = 0;
                    }
                }
                else
                {
                    if (CCommon.ToString(Session["ShippingMethodValue"]) != "")
                    {
                        ddlShippingMethod.ClearSelection();
                        if (ddlShippingMethod.Items.FindByValue(CCommon.ToString(Session["ShippingMethodValue"])) != null)
                        {
                            ddlShippingMethod.Items.FindByValue(CCommon.ToString(Session["ShippingMethodValue"])).Selected = true;
                        }
                        else
                        {
                            ddlShippingMethod.SelectedIndex = 0;
                        }
                    }
                    else
                    {
                        ddlShippingMethod.SelectedIndex = 0;
                    }
                }
                #endregion

                if (strMessage.Length > 0)
                {
                    Page.MaintainScrollPositionOnPostBack = false;
                    //Page.ClientScript.RegisterStartupScript(Page.ClientScript.GetType(), Page.ClientID, "resetScrollPosition();", true);
                    ShowMessage(strMessage, 1);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void ViewRateCalculation()
        {
            try
            {
                this.reCalculation = false;
                ddlShippingMethod.Items.Clear();
                Session["ShippingMethodValue"] = null;
                bindHtml();
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        #endregion

        private void SetDefaultCountry()
        {
            try
            {
                DataTable dtTable = default(DataTable);
                UserAccess objUserAccess = new UserAccess();
                objUserAccess.DomainID = Sites.ToLong(Session["DomainID"]);
                dtTable = objUserAccess.GetDomainDetails();
                if ((ddlCountry.Items.FindByValue(dtTable.Rows[0]["numDefCountry"].ToString()) != null))
                {
                    ddlCountry.ClearSelection();
                    ddlCountry.Items.FindByValue(dtTable.Rows[0]["numDefCountry"].ToString()).Selected = true;
                    Session["CalculateShippingCountry"] = CCommon.ToString(dtTable.Rows[0]["numDefCountry"]);

                    if (ddlCountry.SelectedIndex > 0)
                    {
                        ddlState.ClearSelection();
                        //ddlState.Items.Clear();
                        FillState(ddlState, Sites.ToLong(ddlCountry.SelectedValue), Sites.ToLong(Session["DomainID"]));
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void DeleteItem()
        {
            try
            {
                BACRM.BusinessLogic.ShioppingCart.Cart objcart;
                objcart = new BACRM.BusinessLogic.ShioppingCart.Cart();
                HttpCookie cookie = Request.Cookies["Cart"];
                //DataSet ds = default(DataSet);
                objcart.DomainID = CCommon.ToLong(Session["DomainID"]);
                objcart.ContactId = CCommon.ToLong(Session["UserContactID"]);
                objcart.CookieId = (cookie != null) ? cookie["KeyId"] : "";
                objcart.CartId = CCommon.ToLong(Request["__EVENTARGUMENT"]);
                objcart.bitDeleteAll = false;
                objcart.SiteID = Sites.ToLong(Session["SiteId"]);
                objcart.RemoveItemFromCart();
                //BindCart();

                //to Refill Shipping Method in Dropdown
                this.isShippingMethodNeeded = true;
                Session["ShippingMethodValue"] = null;
                //bindHtml();

                this.Page.ClientScript.RegisterStartupScript(this.GetType(), "UpdateMiniCart", "if(document.getElementById('btnRefresh')!=null){ __doPostBack('btnRefresh','OnClick');}", true);
                this.Page.ClientScript.RegisterStartupScript(this.GetType(), "UpdateCartTotal", "__doPostBack('RefreshCartTotal','')", true);
                return;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region Validate User Detail

        private int CheckForUserDetail()
        {
            string IsValid = "";
            int ErrorCode = 0;
            long ContactId = Sites.ToLong(Session["UserContactID"]);
            try
            {

                if (ContactId > 0)
                {
                    CContacts objContacts = new CContacts();
                    DataTable dtTable1 = default(DataTable);
                    objContacts.ContactID = Sites.ToLong(Session["UserContactID"]);
                    objContacts.DomainID = Sites.ToLong(Session["DomainID"]);
                    dtTable1 = objContacts.GetBillOrgorContAdd();

                    if (Sites.ToBool(Session["SkipStep2"]) == true && IsOnlySalesInquiry() == 1)
                    {
                        ErrorCode = 0;
                    }
                    else if (CCommon.ToString(dtTable1.Rows[0]["vcFirstname"]) == "-")
                    {
                        IsValid = "First name cannot be empty!.,";
                        ErrorCode = 1;
                    }
                    else if (CCommon.ToString(dtTable1.Rows[0]["vcLastname"]) == "-")
                    {
                        IsValid = "Last name cannot be empty!.,";
                        ErrorCode = 2;
                    }
                    else if (CCommon.ToString(dtTable1.Rows[0]["vcFirstname"]).Length < 2)
                    {
                        IsValid = "First name cannot be just a single charatcer!.,";
                        ErrorCode = 3;
                    }
                    else if (CCommon.ToString(dtTable1.Rows[0]["vcLastName"]).Length < 2)
                    {
                        IsValid = "Last  name cannot be just a single charatcer!.,";
                        ErrorCode = 4;
                    }
                    else if (CCommon.ToString(dtTable1.Rows[0]["vcShipCity"]).Length <= 0)
                    {
                        IsValid = "Ship To City is not provided!.";
                        ErrorCode = 6;
                    }
                    else if (CCommon.ToLong(dtTable1.Rows[0]["vcShipState"]) <= 0)
                    {
                        IsValid = "Ship To State is not provided!.";
                        ErrorCode = 7;
                    }
                    else if (CCommon.ToString(dtTable1.Rows[0]["vcShipPostCode"]).Length <= 0)
                    {
                        IsValid = "Ship To Postal Code is not provided!.";
                        ErrorCode = 8;
                    }
                    else if (CCommon.ToLong(dtTable1.Rows[0]["vcShipCountry"]) <= 0)
                    {
                        IsValid = "Ship To Country is not provided!.";
                        ErrorCode = 9;
                    }

                    //if (CCommon.ToString(dtTable1.Rows[0]["vcFirstname"]) == "-" || CCommon.ToString(dtTable1.Rows[0]["vcLastname"]) == "-" || CCommon.ToString(dtTable1.Rows[0]["vcFirstname"]).Length < 2 || CCommon.ToString(dtTable1.Rows[0]["vcLastName"]).Length < 2)
                    //{
                    //    return false;    
                    //}
                    //return true;
                }
                else
                {
                    ErrorCode = 5;
                    //Response.Redirect("Login.aspx?ReturnURL=" + (Sites.ToString(Session["SiteLiveURL"]).Trim().Length > 2 ? "https://" : "http://") + Request.Url.Host + "/" + CheckoutPageName() + "&errcode=" + ErrorCode, true);
                    //throw new Exception("Please Login to Continue..");
                    ShowMessage("Please Login to Continue..", 0);
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ErrorCode;
        }

        #endregion

        #region Check Payment method

        private short IsOnlySalesInquiry()
        {
            UserAccess objUserAccess = new UserAccess();
            DataTable dtPaymentGateWay = new DataTable();
            objUserAccess.DomainID = Sites.ToLong(Session["DomainID"]);
            objUserAccess.byteMode = 1;
            dtPaymentGateWay = objUserAccess.ManageEComPaymentConfig(Sites.ToLong(Session["SiteId"]));
            if (dtPaymentGateWay != null && dtPaymentGateWay.Rows.Count > 0 && dtPaymentGateWay.Select("numPaymentMethodId = " + CCommon.ToInteger(PaymentMethod.SalesInquiry) + " AND bitEnable = 1").Length == 1 && dtPaymentGateWay.Select("bitEnable = 1").Length == 1)
            {
                return 1;
            }
            else if (dtPaymentGateWay != null && dtPaymentGateWay.Rows.Count > 0 && dtPaymentGateWay.Select("numPaymentMethodId = " + CCommon.ToInteger(PaymentMethod.SalesInquiry) + " AND bitEnable = 1").Length == 0 && dtPaymentGateWay.Select("bitEnable = 1").Length > 0)
            {
                return 2;
            }
            else
            {
                return 0;
            }
        }

        #endregion

        #region Previous code

        //throw new CustomException(objShipping.ErrorMsg);
        // if (ShowShippingAmount)
        // {
        //}

        //catch (Exception ex)
        //                                            {
        //                                                //strMessage = GetErrorMessage("ERR068") + "<font color = \"WhiteSmoke\">" + ex.ToString().Replace("red", "WhiteSmoke") + "</font>";
        //                                                //ExceptionModule.ExceptionPublish(ex, Sites.ToLong(Session["DomainID"]), Sites.ToLong(Session["UserContactID"]), Sites.ToLong(Session["SiteId"]), Request);
        //                                            }

        //protected void ddlState_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        this.isShippingMethodNeeded = true;
        //        BindCart();
        //        bindHtml();
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}



        //  dtShippingMethod.Columns.Add("vcServiceNameAndPrice", typeof(String), "vcFromName +'@'+ vcDomainName");
        //ddlEmailTemplate.DataSource = dtTableConfig
        //ddlEmailTemplate.DataTextField = "FromAddress"
        //ddlEmailTemplate.DataValueField = "numConfigurationId"
        //ddlEmailTemplate.DataBind()



        //protected void gvBasket_RowCommand(object sender, GridViewCommandEventArgs e)
        //{
        //    try
        //    {
        //        if (e.CommandName == "del")
        //        {
        //            DeleteItem();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        ExceptionModule.ExceptionPublish(ex, Sites.ToLong(Session["DomainID"]), Sites.ToLong(Session["UserContactID"]), Sites.ToLong(Session["SiteId"]), Request);
        //        ShowMessage(ex.ToString(), 1);
        //    }
        //}

        //protected void gvBasket_RowDataBound(object sender, GridViewRowEventArgs e)
        //{
        //    try
        //    {
        //        if (e.Row.RowType == DataControlRowType.DataRow)
        //        {
        //            Button btnDelete;
        //            btnDelete = (Button)e.Row.FindControl("btnDelete");
        //            btnDelete.Attributes.Add("onclick", "return DeleteRecord()");

        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        ExceptionModule.ExceptionPublish(ex, Sites.ToLong(Session["DomainID"]), Sites.ToLong(Session["UserContactID"]), Sites.ToLong(Session["SiteId"]), Request);
        //        ShowMessage(ex.ToString(), 1);
        //    }
        //}


        //This method  Set TotalTaxAmount Properties ...



        //decShippingCharge = Sites.ToDecimal(dttable.Compute("SUM(decShippingCharge)", ""));// change
        //for (i = 0; i <= dttable.Rows.Count - 1; i++)
        //{
        //    if (Sites.ToLong(dttable.Rows[i]["Tax"]) > 0)
        //    {
        //        Tax = Tax + (Sites.ToDecimal(dttable.Rows[i]["Amount"].ToString()) * Sites.ToDecimal(dttable.Rows[i]["Tax"].ToString()) / 100);
        //    }
        //}
        //discount sesson add

        #endregion
    }

    public class Location
    {
        public string ip { get; set; }
        public string country_name { get; set; }
        public string country_code { get; set; }
        public string city { get; set; }
        public string region_name { get; set; }
        public string zip { get; set; }
        public string latitude { get; set; }
        public string longitude { get; set; }
    }
}
