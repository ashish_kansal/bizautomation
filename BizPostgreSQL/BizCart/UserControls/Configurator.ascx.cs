﻿using BACRM.BusinessLogic.Common;
using BACRM.BusinessLogic.Item;
using BACRM.BusinessLogic.ShioppingCart;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Data;
using System.Web.UI.WebControls;
using System.Xml;


namespace BizCart.UserControls
{
    public partial class Configurator : BizUserControl
    {
        #region Global Declaration

        CItems objItems = null;
        CCommon objCommon = null;
        QueryStringValues objEncryption = null;

        #endregion

        #region Custom Properties

        public long ItemId
        {
            get
            {
                object o = ViewState["ItemID"];
                if (o != null)
                {
                    return Sites.ToLong(o);
                }
                else
                {
                    if (CCommon.ToString(Request.QueryString["ItemID"]) != "")
                    {
                        ViewState["ItemID"] = Request.QueryString["ItemID"];
                        return CCommon.ToLong(ViewState["ItemID"]);
                    }
                    else
                    {
                        return 0;
                    }
                }
            }
        }

        public long CategoryID
        {
            get
            {
                object o = ViewState["CategoryID"];
                if (o != null)
                {
                    return Sites.ToLong(o);
                }
                else
                {
                    if (CCommon.ToString(Request.QueryString["CategoryID"]) != "")
                    {
                        ViewState["CategoryID"] = Request.QueryString["CategoryID"];
                        return Sites.ToLong(ViewState["CategoryID"]);
                    }
                    else
                    {
                        return 0;
                    }
                }
            }
        }

        public Boolean IsMetatagAdded
        {
            get
            {
                object o = ViewState["IsMetatagAdded"];
                if (o != null)
                {
                    return (Boolean)o;
                }
                else
                {
                    return false;
                }
            }
            set
            {
                ViewState["IsMetatagAdded"] = value;
            }
        }

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                Sites.InitialiseSession();

                if (!Page.IsPostBack)
                {
                    objEncryption = new QueryStringValues();

                    if (this.ItemId > 0)
                    {
                        objItems = new CItems();
                        objItems.DomainID = CCommon.ToLong(Session["DomainId"]);
                        objItems.ItemCode = CCommon.ToInteger(this.ItemId);
                        DataTable dt = objItems.ValidateItemDomain();

                        if (dt == null || dt.Rows.Count == 0 || !CCommon.ToBool(dt.Rows[0]["IsItemInDomain"]))
                        {
                            throw new HttpException(404, "Item not found");
                        }
                    }
                    else
                    {
                        throw new HttpException(404, "Item not found");
                    }

                    lblProductPriceCurrency.Text = CCommon.ToString(Session["CurrSymbol"]);
                    lblProductPrice.Text = "0.00";
                    hdnKitItemCode.Value = this.ItemId.ToString();
                    hdnPortalURL.Value = Sites.ToString(HttpContext.Current.Session["ItemImagePath"]) + "/";
                    bindHtml();
                }

                if (IsPostBack)
                {
                    if (Sites.ToString(Request["__EVENTTARGET"]) == "AddToCart")
                    {
                        CCommon objCommon = new CCommon();
                        objCommon.DomainID = Sites.ToLong(Session["DomainID"]);
                        objCommon.Mode = 10;
                        objCommon.Str = this.CategoryID.ToString();
                        string strCategoryName = Sites.ToString(objCommon.GetSingleFieldValue());                    

                        AddToCartFromEcomm(strCategoryName, this.ItemId, 0, "", false, qty: Convert.ToInt32(txtUnits.Text), childKitSelectedValues: hdnKitChildItems.Value);

                        Response.Redirect("~/ProductList.aspx?Cat=" + this.CategoryID.ToString());
                    }

                    bindHtml();
                }
            }
            catch (Exception ex)
            {
                ExceptionModule.ExceptionPublish(ex, Sites.ToLong(Session["DomainID"]), Sites.ToLong(Session["UserContactID"]), Sites.ToLong(Session["SiteId"]), Request);
                ShowMessage(ex.ToString(), 1);
            }
        }

        private void bindHtml()
        {
            try
            {
                objItems = new CItems();

                if (this.ItemId > 0)
                {
                    string strUI = GetUserControlTemplate("KitConfigurator.htm");

                    DataTable dtItemDetails = default(DataTable);
                    DataTable dtItemColumn = default(DataTable);

                    DataSet ds = default(DataSet);
                    DataTable dtTable = default(DataTable);
                    objItems.ItemCode = Sites.ToInteger(this.ItemId);
                    objItems.WarehouseID = Sites.ToLong(Session["WareHouseID"]);
                    objItems.DomainID = Sites.ToLong(Session["DomainID"]);
                    objItems.SiteID = Sites.ToLong(Session["SiteId"]);
                    HttpCookie cookie = Request.Cookies["Cart"];
                    if (Request.Cookies["Cart"] != null)
                    {
                        objItems.vcCookieId = (cookie["KeyId"] != null) ? cookie["KeyId"] : "";
                    }
                    else
                    {
                        objItems.vcCookieId = "";
                    }
                    objItems.UserCntID = Sites.ToLong(Session["UserContactID"]);
                    if ((!System.Web.HttpContext.Current.User.Identity.IsAuthenticated || CCommon.ToLong(Session["DomainID"]) == 0 || CCommon.ToLong(Session["UserContactID"]) == 0) && Sites.ToInteger(Session["OnePageCheckout"]) == 0)
                    {
                        //GUEST USER
                        objItems.DivisionID = 0;
                    }
                    else
                    {
                        objItems.DivisionID = Sites.ToLong(Session["DivId"]);
                    }
                    DataSet dsAvailabilityItemGrid = new DataSet();
                    dsAvailabilityItemGrid = objItems.AvailabilityItemStock();
                    ds = objItems.ItemDetailsForEcomm();

                    if (ds.Tables.Count == 2)
                    {
                        dtItemDetails = ds.Tables[0];
                        dtItemColumn = ds.Tables[1];

                        if (dtItemDetails.Rows.Count > 0)
                        {
                            if (CCommon.ToString(dtItemDetails.Rows[0]["vcPageTitle"]) != "")
                            {
                                this.Page.Title = CCommon.ToString(dtItemDetails.Rows[0]["vcPageTitle"]);
                            }
                            else
                            {
                                this.Page.Title = Sites.ToString(dtItemDetails.Rows[0]["vcItemName"]).Trim();
                            }

                            if (Sites.ToBool(Session["HideAddtoCartBeforeLogin"]) == true && Sites.ToLong(Session["UserContactID"]) == 0)
                            {
                                strUI = strUI.Replace("##AddToCartLink##", "");
                                strUI = strUI.Replace("##Quantity##", "");
                                strUI = strUI.Replace("##HidePriceBeforeLogin##", "hideAddtoCart");
                            }
                            
                            if (Sites.ToBool(Session["HidePrice"]) == false)
                            {
                                strUI = strUI.Replace("##ItemPrice##", CCommon.RenderControl(spanProductPrice));
                            }
                            else
                            {
                                strUI = strUI.Replace("##ItemPrice##", "NA");
                            }


                            strUI = strUI.Replace("##AddToCartLink##", CCommon.RenderControl(btnSaveCloseKit));
                            strUI = strUI.Replace("##ChildKits##", "<div class='panel-group' id='divChildKitContainer'></div>");
                            strUI = strUI.Replace("##ItemCode##", Sites.ToString(dtItemDetails.Rows[0]["numItemCode"]));
                            strUI = strUI.Replace("##UPCCode##", Sites.ToString(dtItemDetails.Rows[0]["numBarCodeId"]));
                            strUI = strUI.Replace("##Item##", Sites.ToString(dtItemDetails.Rows[0]["vcItemName"]));
                            strUI = strUI.Replace("##Quantity##", (btnDecrease != null ? CCommon.RenderControl(btnDecrease) : "") + " " + CCommon.RenderControl(txtUnits) + " " + (btnIncrease != null ? CCommon.RenderControl(btnIncrease) : ""));


                            //Sites objSites = new Sites();
                            //objSites.DomainID = Sites.ToLong(Session["DomainID"]);
                            //objSites.CategoryID = this.CategoryID;
                            //DataTable dtCategory = objSites.GetCategoryByID();
                            //string categoryNmae = "";

                            //if (dtCategory != null && dtCategory.Rows.Count > 0)
                            //{
                            //    categoryNmae = Sites.ToString(dtCategory.Rows[0]["vcCategoryName"]);
                            //}

                            //string url = GetProductURL(categoryNmae, Sites.ToString(dtItemDetails.Rows[0]["vcItemName"]), Sites.ToString(dtItemDetails.Rows[0]["numItemCode"]));
                            strUI = strUI.Replace("##CancelLink##", "<a href='javascript:history.go(-1)'>Cancel</a>");


                            
                    
                            if (!(dtItemDetails.Rows[0]["vcImages"] == System.DBNull.Value))
                            {
                                if (!string.IsNullOrEmpty(dtItemDetails.Rows[0]["vcImages"].ToString()))
                                {

                                    XmlDocument xmlItemImages = new XmlDocument();
                                    xmlItemImages.LoadXml(dtItemDetails.Rows[0]["vcImages"].ToString());

                                    XmlNodeList ItemImages = xmlItemImages.SelectNodes("Images/ItemImages");
                                    
                                    foreach (XmlNode Image in ItemImages)
                                    {
                                        if (CCommon.ToString(Image.Attributes["bitDefault"].Value) == "1")
                                        {
                                            strUI = strUI.Replace("##ThumbImagePath##", GetImagePath(CCommon.ToString(Image.Attributes["vcPathForTImage"].Value)));
                                        }
                                    }
                                }
                            }

                            strUI = strUI.Replace("##ThumbImagePath##", "");


                            objItems.byteMode = 2;
                            objItems.ItemCode = Sites.ToInteger(CCommon.ToString(this.ItemId));
                            DataTable dt = new DataTable();
                            dt = objItems.ViewItemExtendedDesc();
                            if (dt.Rows.Count > 0)
                            {
                                strUI = strUI.Replace("##LongDescription##", Convert.ToString(dt.Rows[0]["txtDesc"]));
                                strUI = strUI.Replace("##ShortHtmlDescription##", Convert.ToString(dt.Rows[0]["txtShortDesc"]));
                            }
                            else
                            {
                                strUI = strUI.Replace("##LongDescription##", "");
                                strUI = strUI.Replace("##ShortHtmlDescription##", "");
                            }

                            if (!CCommon.ToBool(Session["IsShowPromoDetailsLink"]) && CCommon.ToInteger(dtItemDetails.Rows[0]["numTotalPromotions"]) == 1)
                            {
                                strUI = strUI.Replace("##PromotionDescription##", "<img src='/images/item-promotion.png' class='imgitempromo' />" + " " + CCommon.ToString(dtItemDetails.Rows[0]["vcPromoDesc"]));
                            }
                            else if (CCommon.ToInteger(dtItemDetails.Rows[0]["numTotalPromotions"]) > 0)
                            {
                                strUI = strUI.Replace("##PromotionDescription##", "<a href='javascript:LoadItemPromoPopup(" + Sites.ToString(dtItemDetails.Rows[0]["numItemCode"]) + ");' class='linkitempromo'><img src='/images/item-promotion.png' class='imgitempromo' /> Click for details</a>");
                            }
                            else
                            {
                                strUI = strUI.Replace("##PromotionDescription##", "");
                            }
                        }
                        else
                        {
                            throw new HttpException(404, "Item not found");
                        }
                        
                        if (this.IsMetatagAdded == false)
                        {
                            if (CCommon.ToString(dtItemDetails.Rows[0]["vcMetaDescription"]) != "")
                            {
                                Page.Header.Controls.Add(GetMetaTag("description", CCommon.ToString(dtItemDetails.Rows[0]["vcMetaDescription"])));
                            }
                            if (CCommon.ToString(dtItemDetails.Rows[0]["vcMetaKeywords"]) != "")
                            {
                                Page.Header.Controls.Add(GetMetaTag("keywords", CCommon.ToString(dtItemDetails.Rows[0]["vcMetaKeywords"])));
                            }
                            this.IsMetatagAdded = true;
                        }

                        pnlCutomizeHtml.Visible = false;
                        litCutomizeHtml.Text = strUI;

                    }
                    else //Item does not exist
                    {
                        pnlCutomizeHtml.Visible = false;
                        litCutomizeHtml.Text = "";
                        ShowMessage(GetErrorMessage("ERR048"), 1);
                    }

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}