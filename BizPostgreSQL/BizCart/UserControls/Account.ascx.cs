﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BACRM.BusinessLogic.Common;
using BACRM.BusinessLogic.ShioppingCart;
namespace BizCart.UserControls
{
    public partial class Account : BizUserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                Sites.InitialiseSession();
                if (!System.Web.HttpContext.Current.User.Identity.IsAuthenticated || CCommon.ToLong(Session["DomainID"]) == 0 || CCommon.ToLong(Session["UserContactID"]) == 0)
                {
                    Response.Redirect("Login.aspx?ReturnURL=Account.aspx", true);
                }
                if (!IsPostBack)
                {
                    bindDashboard();
                    bindHtml();
                }
            }
            catch (Exception ex)
            {
                ExceptionModule.ExceptionPublish(ex, Sites.ToLong(Session["DomainID"]), Sites.ToLong(Session["UserContactID"]), Sites.ToLong(Session["SiteId"]), Request);
                ShowMessage(ex.ToString(), 1);
            }
        }
        void bindDashboard()
        {
            
            if (Sites.ToBool(Session["bitDefaultProfileURL"]) == true)
            {
                Response.Redirect("CustomerInformation.aspx");
            }
            else if (Sites.ToBool(Session["bitSalesOrderTabs"]) == true)
            {
                Response.Redirect("Orders.aspx?flag=true");
            }
            else if (Sites.ToBool(Session["bitSalesQuotesTabs"]) == true)
            {
                Response.Redirect("SalesOpportunity.aspx");
            }
            else if (Sites.ToBool(Session["bitItemPurchaseHistoryTabs"]) == true)
            {
                Response.Redirect("ItemPurchased.aspx");
            }
            else if (Sites.ToBool(Session["bitItemsFrequentlyPurchasedTabs"]) == true)
            {
                Response.Redirect("Lists.aspx");
            }
            else if (Sites.ToBool(Session["bitOpenCasesTabs"]) == true)
            {
                Response.Redirect("OpenCases.aspx");
            }
            else if (Sites.ToBool(Session["bitOpenRMATabs"]) == true)
            {
                Response.Redirect("OpenRMA.aspx");
            }
        }
        void bindHtml()
        {
            try
            {
                string strUI = GetUserControlTemplate("Account.htm");
                strUI = strUI.Replace("##CustomerName##", Sites.ToString(Session["ContactName"]));
                if (strUI.Contains("##AnnualRevenue##"))
                {
                    BACRM.BusinessLogic.Account.CAccounts objAccount = new BACRM.BusinessLogic.Account.CAccounts();
                    objAccount.DivisionID = Convert.ToInt64(HttpContext.Current.Session["DivId"]);
                    System.Data.DataTable dtSetting = objAccount.GetDefaultSettingValue("numAnnualRevID");

                    if (dtSetting != null && dtSetting.Rows.Count > 0)
                    {
                        strUI = strUI.Replace("##AnnualRevenue##", Convert.ToString(dtSetting.Rows[0]["numAnnualRevID"]));
                    }
                    else
                    {
                        strUI = strUI.Replace("##AnnualRevenue##", "");
                    }
                }
                if (strUI.Contains("##NoOfEmployees##"))
                {
                    BACRM.BusinessLogic.Account.CAccounts objAccount = new BACRM.BusinessLogic.Account.CAccounts();
                    objAccount.DivisionID = Convert.ToInt64(HttpContext.Current.Session["DivId"]);
                    System.Data.DataTable dtSetting = objAccount.GetDefaultSettingValue("numNoOfEmployeesId");

                    if (dtSetting != null && dtSetting.Rows.Count > 0)
                    {
                        strUI = strUI.Replace("##NoOfEmployees##", Convert.ToString(dtSetting.Rows[0]["numNoOfEmployeesId"]));
                    }
                    else
                    {
                        strUI = strUI.Replace("##NoOfEmployees##", "");
                    }
                }
                litCutomizeHtml.Text = strUI;
                pnlCutomizeHtml.Visible = false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}