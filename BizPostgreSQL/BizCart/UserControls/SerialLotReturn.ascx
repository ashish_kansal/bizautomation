﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SerialLotReturn.ascx.cs"  Inherits="BizCart.UserControls.SerialLotReturn" %>
<%@ Register Assembly="AspNetPager, Version=7.4.4.0, Culture=neutral, PublicKeyToken=fb0a0fe055d40fd4" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>
<script language="javascript">
    function OpenBizInvoice(a, b , c,d,e) {
        window.open('Invoice.aspx?Show=True&OppID=' + a + '&RefType=' + b + '&PrintMode=' + c+'&InvoiceType=' + d + '&BizDocId='+e,'', 'toolbar=0,titlebar=0,menubar=0,location=1,left=100,top=50,width=750,height=800,scrollbars=yes,resizable=yes');
    }
    function OpenCustomerStatement() {
        window.open('customerstatement.aspx', '', 'toolbar=no,titlebar=no,top=10,left=100,width=1000,height=300,scrollbars=yes,resizable=yes')
        return false;
    }
    function validateQty(qty,itemCode) {
        var rcvdQty = parseInt($("#txtItem" + itemCode + "").val());
        if (rcvdQty > parseInt(qty)) {
            alert("You can not return more than " + qty + " Qty.");
            $("#txtItem" + itemCode + "").val(0);
        }
    }
    function openPopup(ItemCode, ReturnHeaderId) {
        if (ReturnHeaderId == "0") {
            alert("Please generate the RMA Request first then after you can add Serial/#Lot No");
        }
    }
    function SaveRMA() {
        debugger;
        var data = '';
        var WareHouseItemId = 0;
        $("#tblOrders .txtReturnQty").each(function () {
            if ($(this).is('[type=text]') ) {
                if (parseInt($(this).val()) > 0) {
                    WareHouseItemId = $(this).attr("numWareHouseItemID");
                    data = data + $(this).attr("numWareHouseItmsDTLID") + " (" + $(this).val() + ")"+",";
                }
            }
        });
        data = WareHouseItemId + "#" + data;
        $("#<%=hdnReturnItem.ClientID%>").val(data);
        return false;
    }
    function CloseRMA() {
        window.close();
    }
</script>

<asp:Panel ID="pnlCutomizeHtml" runat="server">
   
    <table cellspacing="0" cellpadding="0" width="100%" border="0">
        <tr>
            <td>
                <div class="sectionheader">
                    <asp:HiddenField ID="hdnReturnItem" runat="server" />
                    <asp:Button ID="btnSave" OnClientClick="SaveRMA()" OnClick="btnSave_Click" runat="server" Text="Save" />
                    <asp:Button ID="btnClose" OnClientClick="CloseRMA()" runat="server" Text="Close" />
                </div>
            </td>
        </tr>
        <tr>
            <td class="LabelColumn" align="right">
            </td>
        </tr>
        
    </table>
</asp:Panel>
<asp:Literal ID="litCutomizeHtml" runat="server"></asp:Literal>
<asp:TextBox ID="txtTotalPage" runat="server" Style="display: none"></asp:TextBox>
<asp:TextBox ID="txtTotalRecords" runat="server" Style="display: none"></asp:TextBox>
<asp:TextBox ID="txtSortChar" runat="server" Style="display: none"></asp:TextBox>

<asp:Button runat="server" ID="Button1" Text="" Style="display: none"></asp:Button>