﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BACRM.BusinessLogic.Item;
using BACRM.BusinessLogic.ShioppingCart;
using System.Data;
using System.Text.RegularExpressions;
using System.Collections;
using System.Text;
using BACRM.BusinessLogic.Common;
namespace BizCart.UserControls
{
    public partial class RelatedItems : BizUserControl
    {
        public int TrimNameLength = 0;

        //public string ApplyPriceBookPrice { get; set; }
        public string TrimDescriptionbyfollowingnoofcharacters { get; set; }
        public string TrimItemNamebyfollowingnoofcharacters { get; set; }
        long lngItemCode;
        TextBox hdnSQty = new TextBox();
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                hdnSQty.ID = "hdnSQty";
                hdnSQty.Text = "1";
                hdnSQty.CssClass = "hdnSQty";
                //lblRelatedItems.Text = "Related Items";
                TrimNameLength = Sites.ToInteger(TrimItemNamebyfollowingnoofcharacters) > 1 ? Sites.ToInteger(TrimItemNamebyfollowingnoofcharacters) : 22;
                lngItemCode = Sites.ToLong(Request["ItemID"]);
                bindHtml();
                string strCategoryName = Request.RawUrl.ToString();
                if (strCategoryName.Split(new string[] { "/" }, StringSplitOptions.RemoveEmptyEntries).Length > 1)
                {
                    strCategoryName = strCategoryName.Split(new string[] { "/" }, StringSplitOptions.RemoveEmptyEntries)[1];
                }

                lblCategoryName.Text = strCategoryName.Replace('-', ' ');

            }
            if (IsPostBack)
            {
                if (Sites.ToString(Request["__EVENTTARGET"]) == "AddToCart")
                {
                    string items = Convert.ToString(Request["__EVENTARGUMENT"]);
                    string[] data = items.Split(',');
                    int qty = 1;
                    int n;
                    bool isNumeric = int.TryParse(data[1], out n);
                    if (isNumeric == true)
                    {
                        qty = Convert.ToInt32(data[1]);
                    }
                    AddToCartFromEcomm(lblCategoryName.Text, Sites.ToLong(data[0]), 0, "", false, Convert.ToInt32(qty));
                }
                bindHtml();
            }

        }
        void bindHtml()
        {
            try
            {
                BACRM.BusinessLogic.Item.CItems objItem = new BACRM.BusinessLogic.Item.CItems();
                objItem.DomainID = Sites.ToLong(Session["DomainID"]);
                objItem.ParentItemCode = lngItemCode; //9;
                objItem.byteMode = 3;
                HttpCookie cookie = Request.Cookies["Cart"];
                if (Request.Cookies["Cart"] != null)
                {
                    objItem.vcCookieId = (cookie["KeyId"] != null) ? cookie["KeyId"] : "";
                }
                else
                {
                    objItem.vcCookieId = "";
                }
                objItem.UserCntID = Sites.ToLong(Session["UserContactID"]);
                DataTable dtItem = new DataTable();
                objItem.SiteID = Sites.ToLong(Session["SiteId"]);
                dtItem = objItem.GetSimilarItem();
                string strUI = GetUserControlTemplate("RelatedItems.htm");
                string pattern = "<##FOR(?<Property>(\\b[^>]*?))##>(?<Content>([\\s\\S]*?))<##ENDFOR##>";

                if (Regex.IsMatch(strUI, pattern))
                {
                    MatchCollection matches = Regex.Matches(strUI, pattern);
                    if ((matches.Count > 0))
                    {
                        Hashtable htProperties = GetPropertiesAsHastable(matches[0].Groups["Property"].Value.Replace("(", "").Replace(")", ""));
                        int RowsCount = 0;
                        string Content = matches[0].Groups["Content"].Value;
                        Sites objSite = new Sites();
                        CItems objItems = new CItems();
                        string Value = string.Empty;
                        if (dtItem.Rows.Count > 0)
                        {

                            //this.Page.Title = Sites.ToString(dtItem.Rows[0]["vcCategoryName"]);
                            //lblRelatedItems.Text = "Related Items";


                            StringBuilder sb = new StringBuilder();

                            string strTemp = string.Empty;

                            foreach (DataRow dr in dtItem.Rows)
                            {
                                strTemp = Content;
                                string url = GetProductURL(dr["vcCategoryName"].ToString(), dr["vcItemName"].ToString(), dr["numItemCode"].ToString());

                                string ItemImagePath = GetImagePath(dr["vcPathForTImage"].ToString());

                                strTemp = strTemp.Replace("##ItemLink##", url);
                                strTemp = strTemp.Replace("##ItemName##", dr["vcItemName"].ToString());
                                strTemp = strTemp.Replace("##ItemShortName##", dr["vcItemName"].ToString().Length > TrimNameLength ? dr["vcItemName"].ToString().Substring(0, TrimNameLength) + ".." : dr["vcItemName"].ToString());
                                strTemp = strTemp.Replace("##ItemImagePathName##", ItemImagePath);

                                Label ProductPrice = new Label();
                                ProductPrice.CssClass = "prod_price";

                                Label OfferPrice = new Label();
                                Label ProductDesc = new Label();

                                double dProductPrice = 0, dOfferPrice = 0;
                                if (!(dr["monListPrice"] == System.DBNull.Value))
                                {
                                    dProductPrice = Sites.ToDouble((Sites.ToDecimal(dr["monListPrice"]) * Sites.ToDecimal(dr["UOMConversionFactor"])) / Sites.ToDecimal(Session["ExchangeRate"]));
                                    ProductPrice.Text = Sites.ToString(Session["CurrSymbol"]) + " " + string.Format("{0:#,##0.00###}", dProductPrice);
                                }
                                //Hide prices for site visitors until they log in as a registered user.
                                //Hide prices for site registered site visitors AFTER they log in as a registered user, with the following Relationship &  Profile
                                if (Sites.ToBool(Session["HidePrice"]) == true)
                                    OfferPrice.Visible = ProductPrice.Visible = false;
                                //if (Sites.ToBool(ShowItemDescription) == true)
                                //{
                                //Trim description
                                ProductDesc.Text = dr["txtItemDesc"].ToString();

                                if (Sites.ToInteger(TrimDescriptionbyfollowingnoofcharacters) > 0 && ProductDesc.Text.Length > Sites.ToInteger(TrimDescriptionbyfollowingnoofcharacters))
                                    ProductDesc.Text = ProductDesc.Text.Substring(0, Sites.ToInteger(TrimDescriptionbyfollowingnoofcharacters)) + "..";
                                //}

                                /*Apply pricebook rule in Item list */
                                if (Sites.ToBool(ApplyPriceBookPrice) == true && Sites.ToBool(Session["HidePrice"]) == false)
                                {
                                    PriceBookRule objPbook = new PriceBookRule();
                                    objPbook.ItemID = Sites.ToLong(dr["numItemCode"]);
                                    objPbook.QntyofItems = Sites.ToInteger(1 * Sites.ToDecimal(dr["UOMConversionFactor"]));
                                    objPbook.DomainID = Sites.ToLong(Session["DomainID"]);
                                    objPbook.DivisionID = Sites.ToLong(Session["DivId"]);
                                    objPbook.WarehouseID = Sites.ToLong(Session["WareHouseID"]);
                                    objPbook.SiteID = Sites.ToLong(HttpContext.Current.Session["SiteId"]);
                                    DataTable dtCalPrice = default(DataTable);
                                    dtCalPrice = objPbook.GetPriceBasedonPriceBook();
                                    if (dtCalPrice.Rows.Count > 0)
                                    {
                                        dOfferPrice = Sites.ToDouble(Sites.ToDecimal(dtCalPrice.Rows[0]["ListPrice"]) * Sites.ToDecimal(dr["UOMConversionFactor"]) / Sites.ToDecimal(Session["ExchangeRate"].ToString()));
                                        OfferPrice.Text = Sites.ToString(Session["CurrSymbol"]) + " " + dOfferPrice.ToString();

                                    }

                                    if (dOfferPrice != dProductPrice && dOfferPrice > 0)
                                    {
                                        OfferPrice.Text = Sites.ToString(Session["CurrSymbol"]) + " " + string.Format("{0:#,##0.00###}", dOfferPrice);
                                        ProductPrice.CssClass = "prod_strick";
                                        OfferPrice.CssClass = "prod_price";
                                    }
                                    else
                                    {
                                        OfferPrice.Visible = false;
                                    }
                                }

                                strTemp = strTemp.Replace("##OfferPrice##", CCommon.RenderControl(OfferPrice));
                                strTemp = strTemp.Replace("##ProductPrice##",CCommon.RenderControl(ProductPrice));
                                strTemp = strTemp.Replace("##ItemDesc##", CCommon.RenderControl(ProductDesc));
                                strTemp = strTemp.Replace("##ItemUpSellDesc##",Convert.ToString(dr["txtUpSellItemDesc"]));
                                
                                strTemp = strTemp.Replace("##SKU##", CCommon.ToString(dr["vcSKU"]));
                                strTemp = strTemp.Replace("##Manufacturer##", CCommon.ToString(dr["vcManufacturer"]));
                                //strTemp = strTemp.Replace("##Availability##", dr["InStock"].ToString());
                                //if (Sites.ToBool(ApplyPriceBookPrice) == true && hdPriceBookId.Value.Length > 0)
                                //{
                                //    Table t = createPriceBookRuleTable();
                                //    strTemp = strTemp.Replace("##PriceBookRule##", CCommon.RenderControl(t));
                                //    //strTemp = strTemp + CCommon.RenderControl(t);

                                //}
                                if (RowsCount % 2 == 0)
                                    strTemp = strTemp.Replace("##RowClass##", Sites.ToString(htProperties["AlternatingItemClass"]));
                                else
                                    strTemp = strTemp.Replace("##RowClass##", Sites.ToString(htProperties["ItemClass"]));

                                strTemp = strTemp.Replace("##ItemID##", dr["numItemCode"].ToString());

                                if (strTemp.Contains("##Quantity##"))
                                {
                                    strTemp = strTemp.Replace("##Quantity##", "<input type=\"button\" value=\"-\" name=\'removenQty\' onclick=\"removeQty('" + Sites.ToLong(dr["numItemCode"]) + "')\"/>" + "<input type=\"text\" value=\"1\" name=\"txtnSQty\" id='" + Sites.ToLong(dr["numItemCode"]) + "' class=\"txtSQty\"/>" + "<input type=\"button\" value=\"+\" name=\'addnQty\' onClick=\"addQty('" + Sites.ToLong(dr["numItemCode"]) + "')\" class=\"addqty\"/>" );
                                }

                                sb.AppendLine(strTemp);
                                RowsCount++;

                            }
                            strUI = Regex.Replace(strUI, pattern, sb.ToString());
                            //strUI = strUI.Replace("##CategoryDesc##", Sites.ToString(dtItem.Rows[0]["CategoryDesc"]));
                            //pnlCutomizeHtml.Visible = false;
                            lblCustomizeHtml.Text = strUI;
                            // litCutomizeHtml.Text = strUI;
                        }

                    }
                }


            }
            catch (Exception ex)
            {
                throw ex;
            }

        }




    }

}