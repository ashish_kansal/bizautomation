﻿using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BACRM.BusinessLogic.Opportunities;
using BACRM.BusinessLogic.Common;
using BACRM.BusinessLogic.Item;
using System.Data;
using BACRM.BusinessLogic.ShioppingCart;
using System.Text.RegularExpressions;
using System.Text;
namespace BizCart.UserControls
{
    public partial class SalesOpportunity : BizUserControl
    {
        string strColumn = string.Empty;
        int PageSize = 10;
        int PageIndex = 0;
        string DasboardTabs = "";
        string actionitem = "";
        string orderId = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                Sites.InitialiseSession();
                if (Sites.ToLong(Session["UserContactID"]) == 0 || Sites.ToLong(Session["DivId"]) == 0)
                {
                    Response.Redirect("Login.aspx?ReturnURL=Orders.aspx", true);
                }
                PageIndex = Sites.ToInteger(Request["Page"]);
                actionitem = Convert.ToString(Request.QueryString["actionitem"]);
                orderId = Convert.ToString(Request.QueryString["OrderId"]);
                
                PageIndex = PageIndex == 0 ? 1 : PageIndex;//set default to 1
                AspNetPager1.PageSize = Sites.ToInteger(PageSize);
                AspNetPager1.CurrentPageIndex = PageIndex;
                if (!IsPostBack)
                {

                    bindHtml();
                }
                if (actionitem == "PayNow")
                {

                    AddToCart(Sites.ToLong(orderId));
                }
                if (!string.IsNullOrEmpty(txtSortChar.Text))
                {
                    ViewState["SortChar"] = txtSortChar.Text;
                    ViewState["Column"] = "BizDocName";
                    Session["Asc"] = 0;
                    BindDatagrid();
                    txtSortChar.Text = "";
                }
            }
            catch (Exception ex)
            {
                ExceptionModule.ExceptionPublish(ex, Sites.ToLong(Session["DomainID"]), Sites.ToLong(Session["UserContactID"]), Sites.ToLong(Session["SiteId"]), Request);
                ShowMessage(ex.ToString(), 1);
            }
        }
        private void AddToCart(long lngOrderID)
        {
            try
            {
                COpportunities objOpp = new COpportunities();
                DataTable dtTemplate;
                objOpp.DomainID = Sites.ToLong(Session["DomainID"]);
                objOpp.SalesTemplateID = lngOrderID;
                dtTemplate = objOpp.GetSalesOrderTemplateItems();
                if (dtTemplate.Rows.Count > 0)
                {
                   
                    DataSet dsTemp = GetCartItem(); //(DataSet)Session["Data"];
                    DataTable dtItem = default(DataTable);
                    dtItem = dsTemp.Tables[0];
                    DataRow dr = default(DataRow);
                    //Add items to table and store it to session
                    foreach (DataRow drow in dtTemplate.Rows)
                    {
                        Tuple<bool,string> objResult = AddToCartFromEcomm(Sites.ToString(drow["vcCategoryName"]), Sites.ToLong(drow["numItemCode"]), 0, "", false, Sites.ToInteger(drow["numUnitHour"]));
                        bool isItemAdded = objResult.Item1;
                    }
                    if (actionitem == "PayNow")
                    {
                        Response.Redirect("cart.aspx", true);
                    }
                }
                else
                {
                    ShowMessage(GetErrorMessage("ERR045"), 1);//No items found in list!
                    bindHtml();
                }
            }
            catch (Exception ex)
            {
                ExceptionModule.ExceptionPublish(ex, Sites.ToLong(Session["DomainID"]), Sites.ToLong(Session["UserContactID"]), Sites.ToLong(Session["SiteId"]), Request);
                ShowMessage(ex.ToString(), 1);
            }
        }
     
        public DataTable BindDatagrid()
        {
            try
            {
                DataTable dtOpportunity = default(DataTable);
                COpportunities objOpportunity = new COpportunities();
                char SortChar = '\0';
                if (Sites.ToString(ViewState["SortChar"]) != "")
                {
                    SortChar = Convert.ToChar(ViewState["SortChar"]);
                }
                else
                {
                    SortChar = '0';
                }
                if (Request.QueryString["flag"] != null)
                {
                    objOpportunity.bitflag = Sites.ToBool(Request.QueryString["flag"]);
                   
                }
                else
                {
                    objOpportunity.bitflag = true;
                }
                objOpportunity.Shipped = 0;
                objOpportunity.UserCntID = Sites.ToInteger(Session["UserContactID"]);
                objOpportunity.DomainID = Sites.ToLong(Session["DomainID"]);
                objOpportunity.SortCharacter = SortChar;
                objOpportunity.DivisionID = Sites.ToInteger(Session["DivID"]);
                objOpportunity.CurrentPage =Convert.ToInt32(AspNetPager1.CurrentPageIndex.ToString());
                objOpportunity.PageSize = PageSize;//Sites.ToInteger(Session["PagingRows"].ToString());
                objOpportunity.TotalRecords = 0;
                objOpportunity.OppType = 1;
                if (Sites.ToString(ViewState["Column"]) != "")
                {
                    objOpportunity.columnName = Sites.ToString(ViewState["Column"]);
                }
                else
                {
                    objOpportunity.columnName = "dtCreatedDate";
                }
                if (Sites.ToString(Session["Asc"]) == "1")
                {
                    objOpportunity.columnSortOrder = "Asc";
                }
                else
                {
                    objOpportunity.columnSortOrder = "Desc";
                }
                DataSet ds = new DataSet();
                objOpportunity.OppStatus = 0;
                ds = objOpportunity.GetOrderListInEcommerce();
                dtOpportunity=ds.Tables[0];
                lblRecordCount.Text = Sites.ToString(ds.Tables[1].Rows[0][0]);
                string[] strTotalPage = null;
                decimal decTotalPage = default(decimal);
                AspNetPager1.RecordCount = Sites.ToInteger(lblRecordCount.Text);
                decTotalPage = Sites.ToInteger(lblRecordCount.Text) / PageSize;//Session["PagingRows"]
                decTotalPage = Math.Round(decTotalPage, 2);
                strTotalPage = decTotalPage.ToString().Split('.');
                if (Sites.ToInteger(lblRecordCount.Text) % PageSize == 0)
                {
                    txtTotalPage.Text = strTotalPage[0];
                }
                else
                {
                    txtTotalPage.Text = strTotalPage[0] + 1;
                }
                txtTotalRecords.Text = lblRecordCount.Text;
                //dgOpportunity.DataSource = dtOpportunity;
                //dgOpportunity.DataBind();

                return dtOpportunity;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string ReturnName(System.DateTime bintCreatedDate)
        {
            try
            {
                string strCreateDate = null;
                strCreateDate = modBacrm.FormattedDateFromDate(bintCreatedDate, Sites.ToString(Session["DateFormat"]));
                return strCreateDate;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string ReturnDateTime(Object CloseDate)
        {
            try
            {
                string strTargetResolveDate = "";
                string temp = "";

                if (!(CloseDate == System.DBNull.Value))
                {
                    DateTime dtCloseDate = DateTime.Parse(CloseDate.ToString());
                    strTargetResolveDate = modBacrm.FormattedDateFromDate(dtCloseDate, Sites.ToString(Session["DateFormat"]));

                    string timePart = dtCloseDate.ToShortTimeString().Substring(0, dtCloseDate.ToShortTimeString().Length - 1);
                    // remove gaps
                    if (timePart.Split(' ').Length >= 2) timePart = timePart.Split(' ').GetValue(0).ToString() + timePart.Split(' ').GetValue(1).ToString();

                    // check Today date components [ Date , Month , Year ] with Parameter CloseDate components [ Date , Month , Year ] 
                    // if both are same it is today
                    if ((dtCloseDate.Date == DateTime.Now.Date & dtCloseDate.Month == DateTime.Now.Month & dtCloseDate.Year == DateTime.Now.Year))
                    {
                        strTargetResolveDate = "<font color=red><b>Today</b></font>";

                        return strTargetResolveDate;
                    }
                    // check [ TodayDate + 1 ] Date.... components [ Date , Month , Year ] with Parameter dtCloseDate components [ Date , Month , Year ] 
                    // if both are same it was Yesterday
                    else if ((dtCloseDate.Date.AddDays(1).Date == DateTime.Now.Date & dtCloseDate.AddDays(1).Month == DateTime.Now.Month & dtCloseDate.AddDays(1).Year == DateTime.Now.Year))
                    {
                        strTargetResolveDate = "<font color=purple><b>Yesterday</b></font>";

                        return strTargetResolveDate;
                    }
                    // check TodayDate .... components [ Date , Month , Year ] with Parameter [ dtCloseDate + 1 ] Date....  components [ Date , Month , Year ] 
                    // if both are same it will Tomorrow
                    else if ((dtCloseDate.Date == DateTime.Now.AddDays(1).Date & dtCloseDate.Month == DateTime.Now.AddDays(1).Month & dtCloseDate.Year == DateTime.Now.AddDays(1).Year))
                    {
                        temp = dtCloseDate.Hour.ToString() + ":" + dtCloseDate.Minute.ToString();
                        strTargetResolveDate = "<font color=orange><b>Tomorrow</b></font>";

                        return strTargetResolveDate;
                    }
                    // display day name for next 4 days from DateTime.Now

                    else if (dtCloseDate.ToString("yyyyMMdd") == DateTime.Now.AddDays(2).ToString("yyyyMMdd") | dtCloseDate.ToString("yyyyMMdd") == DateTime.Now.AddDays(3).ToString("yyyyMMdd") | dtCloseDate.ToString("yyyyMMdd") == DateTime.Now.AddDays(4).ToString("yyyyMMdd") | dtCloseDate.ToString("yyyyMMdd") == DateTime.Now.AddDays(5).ToString("yyyyMMdd"))
                    {
                        strTargetResolveDate = "<b>" + dtCloseDate.DayOfWeek.ToString() + "</b>";
                        return strTargetResolveDate;
                    }
                    else
                    {
                        //strTargetResolveDate = strTargetResolveDate;
                        return strTargetResolveDate;
                    }
                }
                return strTargetResolveDate;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public object ReturnMoney(System.Object Money)
        {
            try
            {
                return Math.Round(Sites.ToDecimal(Money.ToString()), 2);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        private void btnGo_Click(object sender, EventArgs e)
        {
            try
            {
                bindHtml();
            }
            catch (Exception ex)
            {
                ExceptionModule.ExceptionPublish(ex, Sites.ToLong(Session["DomainID"]), Sites.ToLong(Session["UserContactID"]), Sites.ToLong(Session["SiteId"]), Request);
                ShowMessage(ex.ToString(), 1);
            }
        }

        void bindHtml()
        {
            try
            {
                string strUI = GetUserControlTemplate("SalesOpportunity.htm");

                string pattern = "<##FOR(?<Property>(\\b[^>]*?))##>(?<Content>([\\s\\S]*?))<##ENDFOR##>";
                string CssClass = "", AltCssClass = "", RepeteTemplate = "";

                if (Regex.IsMatch(strUI, pattern))
                {
                    MatchCollection matches = Regex.Matches(strUI, pattern);
                    if ((matches.Count > 0))
                    {
                        string[] Property = matches[0].Groups["Property"].Value.Replace("(", "").Replace(")", "").Split(';');
                        RepeteTemplate = matches[0].Groups["Content"].Value;



                        if (Property.Length > 0)
                        {
                            for (int i = 0; i < Property.Length; i++)
                            {
                                string[] PropertyValue = Property[i].Split('=');

                                if (PropertyValue.Length == 2)
                                {
                                    switch (PropertyValue[0])
                                    {
                                        case "ItemClass":
                                            CssClass = PropertyValue[1].Trim();
                                            break;
                                        case "AlternatingItemClass":
                                            AltCssClass = PropertyValue[1].Trim();
                                            break;
                                    }
                                }
                            }
                        }
                        StringBuilder sb = new StringBuilder();
                        DataTable dt = BindDatagrid();
                        if (dt.Rows.Count > 0)
                        {
                            

                            string strTemp = string.Empty;
                            int RowsCount = 0;

                            foreach (DataRow dr in dt.Rows)
                            {
                                string refType = CCommon.ToString(dr["tintOppStatus"]) == "0" ? "3" : "1";
                                string FinalBizDoc = "";
                                string FinalTracking = "";
                                string[] BizDocList = CCommon.ToString(dr["vcBizDoc"]).Split(new[] { "$^$" }, StringSplitOptions.None);
                                string[] TrackingList = CCommon.ToString(dr["vcShipping"]).Split(new[] { "$^$" }, StringSplitOptions.None);
                                foreach (string IndivBizDoc in BizDocList)
                                {
                                    string[] BizDocDetails = IndivBizDoc.Split(new[] { "#^#" }, StringSplitOptions.None);
                                    if (BizDocDetails.Length-1 > 0)
                                    {
                                        FinalBizDoc = FinalBizDoc + "<a href=\"javascript:OpenBizInvoice(" + CCommon.ToString(dr["numOppId"]) + ",11," + 0 + "," + 1 + "," + BizDocDetails[0] + ")\">" + BizDocDetails[1] + "</a>"+",";
                                    }
                                }
                                foreach (string IndivTracking in TrackingList)
                                {
                                    string[] TrackingDetails = IndivTracking.Split(new[] { "#^#" }, StringSplitOptions.None);
                                    if (TrackingDetails.Length-1 > 0)
                                    {
                                        FinalTracking = FinalTracking + "<a target=\"_blank\" href=\"" + TrackingDetails[1] + "\">" + TrackingDetails[0] + "</a>" + ",";
                                    }
                                }
                                strTemp = RepeteTemplate;
                                strTemp = strTemp.Replace("##OppID##", CCommon.ToString(dr["numOppId"]));
                                strTemp = strTemp.Replace("##RefType##", refType);
                                strTemp = strTemp.Replace("##PrintMode##", "0");
                                strTemp = strTemp.Replace("##BillingTerms##", CCommon.ToString(dr["Credit"]));
                                strTemp = strTemp.Replace("##OrderName##", CCommon.ToString(dr["vcPOppName"]));
                                strTemp = strTemp.Replace("##ReleaseDate##", CCommon.ToString(dr["ReleaseDate"]));
                                strTemp = strTemp.Replace("##PaymentMethod##", CCommon.ToString(dr["PaymentMethod"]));
                                strTemp = strTemp.Replace("##DueDate##", CCommon.ToString(dr["DueDate"]));
                                strTemp = strTemp.Replace("##BizDocs##", FinalBizDoc);
                                strTemp = strTemp.Replace("##TrackingIds##", FinalTracking);
                                strTemp = strTemp.Replace("##OrderStatus##", CCommon.ToString(dr["vcOrderStatus"]));
                                strTemp = strTemp.Replace("##CreatedBy##", CCommon.ToString(dr["CreatedName"]));
                                strTemp = strTemp.Replace("##CreatedDate##", ReturnDateTime(dr["numCreatedDate"]));
                                strTemp = strTemp.Replace("##GrandTotal##", Sites.ToString(Session["CurrSymbol"]) + " " + string.Format("{0:#,##0.00}", (dr["TotalAmt"])));
                                strTemp = strTemp.Replace("##AmountPaid##", Sites.ToString(Session["CurrSymbol"]) + " " + string.Format("{0:#,##0.00}", (dr["monAmountPaid"])));
                                if (Sites.ToDecimal(dr["BalanceDue"]) == 0)
                                {
                                    strTemp = strTemp.Replace("##BalanceDue##", "<img src=\"../images/amountPaid.png\"/>");
                                }
                                else
                                {
                                    strTemp = strTemp.Replace("##BalanceDue##", Sites.ToString(Session["CurrSymbol"]) + " " + string.Format("{0:#,##0.00}", (dr["BalanceDue"])));
                                }
                                strTemp = strTemp.Replace("##PayNow##", "<a class=\"listBtn\" href=\"SalesOpportunity.aspx?flag=true&actionitem=PayNow&OrderId=" + CCommon.ToString(dr["numOppId"]) + "\">Checkout</a>");
                                //strTemp = strTemp.Replace("##InvoiceID##", CCommon.ToString(dr["vcBizDocID"]));

                                if (RowsCount % 2 == 0)
                                    strTemp = strTemp.Replace("##RowClass##", AltCssClass);
                                else
                                    strTemp = strTemp.Replace("##RowClass##", CssClass);

                                sb.Append(strTemp);

                                RowsCount++;
                            }
                        }
                        strUI = strUI.Replace("##NoOfOrdersLabel##", lblRecordCount.Text);
                        strUI = strUI.Replace("##PagerControl##", CCommon.RenderControl(AspNetPager1));
                        strUI = Regex.Replace(strUI, pattern, sb.ToString());
                    }
                }

                //Credit Term Replacement
                string SummaryPattern = "{##SummaryTable##}(?<Content>([\\s\\S]*?)){/##SummaryTable##}";
                CCommon objCommon = new CCommon();
                CItems objItems = new CItems();
                objItems.DivisionID = Sites.ToLong(Session["DivId"]);
                DataTable dtTable = default(DataTable);
                string lblCreditLimit = "";
                dtTable = objItems.GetAmountDue();
                lblBalDue.Text = Sites.ToString(Session["CurrSymbol"]) + " " + string.Format("{0:#,##0.00}", dtTable.Rows[0]["AmountDueSO"]);
                decimal RemainIngCredit = Sites.ToDecimal(dtTable.Rows[0]["RemainingCredit"]);
                if (RemainIngCredit < 0)
                {
                    RemainIngCredit = 0;
                }
                lblRemCredit.Text = Sites.ToString(Session["CurrSymbol"]) + " " + string.Format("{0:#,##0.00}", RemainIngCredit);
                lblAmtPastDue.Text = Sites.ToString(Session["CurrSymbol"]) + " " + string.Format("{0:#,##0.00}", dtTable.Rows[0]["AmountPastDuePO"]);
                lblCreditLimit = Sites.ToString(Session["CurrSymbol"]) + " " + string.Format("{0:#,##0.00}", dtTable.Rows[0]["CreditLimit"]);
                tblCreditTerm.Visible = true;
                strUI = Sites.ReplaceMatchedPatternWithContent(strUI, SummaryPattern);
                //double CreditLimit = Sites.ToDouble(objItems.GetCreditStatusofCompany());
                //if (CreditLimit > 0 && CreditLimit != 10000000)
                //{
                //    DataTable dtTable = default(DataTable);
                //    dtTable = objItems.GetAmountDue();
                //    lblBalDue.Text = Sites.ToString(Session["CurrSymbol"]) + " " + string.Format("{0:#,##0.00}", dtTable.Rows[0]["AmountDueSO"]);
                //    lblRemCredit.Text = Sites.ToString(Session["CurrSymbol"]) + " " + string.Format("{0:#,##0.00}", dtTable.Rows[0]["RemainingCredit"]);
                //    lblAmtPastDue.Text = Sites.ToString(Session["CurrSymbol"]) + " " + string.Format("{0:#,##0.00}", dtTable.Rows[0]["AmountPastDuePO"]);
                //    lblCreditLimit.Text = Sites.ToString(Session["CurrSymbol"]) + " " + string.Format("{0:#,##0.00}", dtTable.Rows[0]["CreditLimit"]);
                //    tblCreditTerm.Visible = true;

                //    //Replace disply template ##Tags## with content value
                //    strUI = Sites.ReplaceMatchedPatternWithContent(strUI, SummaryPattern);

                //}
                //else
                //{
                //    tblCreditTerm.Visible = false;
                //    //Hide Other Template
                //    strUI = Sites.RemoveMatchedPattern(strUI, SummaryPattern);
                //}

                strUI = strUI.Replace("##TotalBalanceDueLabel##", lblBalDue.Text);
                strUI = strUI.Replace("##TotalRemainingCreditLabel##", lblRemCredit.Text);
                strUI = strUI.Replace("##TotalAmountPastDueLabel##", lblAmtPastDue.Text);
                strUI = strUI.Replace(" ##CreditLimit##", lblCreditLimit);
                strUI = strUI.Replace("##ARStatement##", "");
               
                strUI = BindTopDashboard(strUI);

                strUI = strUI.Replace("##CustomerName##", Sites.ToString(Session["ContactName"]));

                pnlCutomizeHtml.Visible = false;
                litCutomizeHtml.Text = strUI;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        protected void AspNetPager1_PageChanged(object sender, EventArgs e)
        {
            try
            {
                bindHtml();
            }
            catch (Exception ex)
            {
                ExceptionModule.ExceptionPublish(ex, Sites.ToLong(Session["DomainID"]), Sites.ToLong(Session["UserContactID"]), Sites.ToLong(Session["SiteId"]), Request);
                ShowMessage(ex.ToString(), 1);
            }
        }
        private string BindTopDashboard(string strUI)
        {
            StringBuilder sbTopMenu = new StringBuilder();
            if (Sites.ToBool(Session["bitSalesOrderTabs"]) == true)
            {
                sbTopMenu.Append("<li class=\"SalesOrderTabs\"><a id=\"SalesOrderTabs\" href=\"Orders.aspx?flag=true\">" + Convert.ToString(Session["vcSalesOrderTabs"]) + "</a></li>");
            }
            if (Sites.ToBool(Session["bitSalesQuotesTabs"]) == true)
            {
                sbTopMenu.Append("<li class=\"SalesOpportunityTabs active\"><a id=\"SalesOpportunityTabs\" href=\"SalesOpportunity.aspx\">" + Convert.ToString(Session["vcSalesQuotesTabs"]) + "</a></li>");
            }
            if (Sites.ToBool(Session["bitItemPurchaseHistoryTabs"]) == true)
            {
                sbTopMenu.Append("<li class=\"ItemPurchasedTabs\"><a id=\"ItemPurchasedTabs\" href=\"ItemPurchasedHistory.aspx\">" + Convert.ToString(Session["vcItemPurchaseHistoryTabs"]) + "</a></li>");
            }
            if (Sites.ToBool(Session["bitItemsFrequentlyPurchasedTabs"]) == true)
            {
                sbTopMenu.Append("<li class=\"WishListsTabs\"><a id=\"WishListsTabs\" href=\"Lists.aspx\">" + Convert.ToString(Session["vcItemsFrequentlyPurchasedTabs"]) + "</a></li>");
            }
            if (Sites.ToBool(Session["bitOpenCasesTabs"]) == true)
            {
                sbTopMenu.Append("<li class=\"OpenCasesTabs\"><a id=\"OpenCasesTabs\" href=\"OpenCases.aspx\">" + Convert.ToString(Session["vcOpenCasesTabs"]) + "</a></li>");
            }
            if (Sites.ToBool(Session["bitOpenRMATabs"]) == true)
            {
                sbTopMenu.Append("<li class=\"OpenRMATabs\"><a id=\"OpenRMATabs\" href=\"OpenRMA.aspx\">" + Convert.ToString(Session["vcOpenRMATabs"]) + "</a></li>");
            }
            if (Sites.ToBool(Session["bitSupportTabs"]) == true)
            {
                sbTopMenu.Append("<li class=\"SupportTabs\"><a id=\"SupportTabs\" href=\"support.aspx\">" + Convert.ToString(Session["vcSupportTabs"]) + "</a></li>");
            }
            strUI = strUI.Replace("##TopMenuDasboard##", Convert.ToString(sbTopMenu));
            return strUI;
        }


    }
}