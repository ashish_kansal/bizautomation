﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BACRM.BusinessLogic.ShioppingCart;
using BACRM.BusinessLogic.Common;
using BACRM.BusinessLogic.Admin;
using System.Data;
namespace BizCart.UserControls
{
    public partial class OnePageCheckout : BizUserControl
    {

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            Sites.InitialiseSession();
            if (Sites.ToInteger(Session["SelectedStep"]) == 0 && Sites.ToInteger(hdnSelectedStep.Value) == 0 && Sites.ToLong(Session["DomainID"]) > 0 && Sites.ToString(Session["UserEmail"]).Length > 3 && Sites.ToLong(Session["UserContactID"]) > 0)
            { // User is already loged in Skip step 0 and show step 1 directly
                hdnSelectedStep.Value = "1";
            }
            if (!IsPostBack)
            {
                if ((!System.Web.HttpContext.Current.User.Identity.IsAuthenticated || CCommon.ToLong(Session["DomainID"]) == 0 || CCommon.ToLong(Session["UserContactID"]) == 0) && Sites.ToInteger(Session["OnePageCheckout"]) == 0)
                {
                    Response.Redirect("Login.aspx?ReturnURL=" + (Sites.ToString(Session["SiteLiveURL"]).Trim().Length > 2 ? "https://" : "http://") + Request.Url.Host + "/" + CheckoutPageName(), true);
                }

                Session["OnePageCheckout"] = 1;
                if (Sites.ToInteger(Session["SelectedStep"]) > 0)
                {
                    hdnSelectedStep.Value = Sites.ToString(Session["SelectedStep"]);
                }
            }
            Session["SelectedStep"] = Sites.ToInteger(hdnSelectedStep.Value);
        }
        protected void Page_PreRender(object sender, EventArgs e)
        {
            try
            {
                if (Sites.ToBool(Session["SkipStep2"]) == true && Sites.ToInteger(Session["SelectedStep"]) == 1 && IsOnlySalesInquiry() == true)
                {
                    Session["SelectedStep"] = Sites.ToInteger(Session["SelectedStep"]) + 1;
                }
                else
                {
                    Session["SelectedStep"] = Sites.ToInteger(Session["SelectedStep"]);
                }
                hdnSelectedStep.Value = Sites.ToString(Session["SelectedStep"]);
            }
            catch (Exception ex)
            {
                ExceptionModule.ExceptionPublish(ex, Sites.ToInteger(Session["DomainID"]), Sites.ToInteger(Session["UserContactID"]), Sites.ToInteger(Session["SiteId"]), Request);
                ShowMessage(ex.ToString(), 1);
            }
        }

        #region Check Payment method
        private bool IsOnlySalesInquiry()
        {
            UserAccess objUserAccess = new UserAccess();
            DataTable dtPaymentGateWay = new DataTable();
            objUserAccess.DomainID = Sites.ToLong(Session["DomainID"]);
            objUserAccess.byteMode = 1;
            dtPaymentGateWay = objUserAccess.ManageEComPaymentConfig(Sites.ToLong(Session["SiteId"]));
            if (dtPaymentGateWay != null && dtPaymentGateWay.Rows.Count > 0 && dtPaymentGateWay.Select("numPaymentMethodId = " + CCommon.ToInteger(PaymentMethod.SalesInquiry) + " AND bitEnable = 1").Length == 1 && dtPaymentGateWay.Select("bitEnable = 1").Length == 1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        #endregion

        #endregion
    }
}