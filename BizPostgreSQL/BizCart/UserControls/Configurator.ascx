﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Configurator.ascx.cs" Inherits="BizCart.UserControls.Configurator" %>
<script type="text/javascript" src="../Js/slick.min.js"></script>
<link rel="stylesheet" href="../slick.css" />
<link rel="stylesheet" href="../slick-theme.css" />
<script type="text/javascript" src="../Js/biz.kitswithchildkits.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        LoadChildKits($("[id$=hdnKitItemCode]").val());
    });
</script>
<asp:Panel ID="pnlCutomizeHtml" runat="server">
    <asp:Button ID="btnDecrease" runat="server" Text="-" />&nbsp;
    <asp:TextBox ID="txtUnits" runat="server" Width="25" CssClass="textbox" Text="1"></asp:TextBox>
    &nbsp;
    <asp:Button ID="btnIncrease" runat="server" Text="+" />&nbsp;
    
    <span runat="server" ID="spanProductPrice">
        <asp:Label ID="lblProductPriceCurrency" runat="server"></asp:Label>
        <asp:Label ID="lblProductPrice" runat="server"></asp:Label>
    </span>

    <asp:Button ID="btnSaveCloseKit" runat="server" CssClass="btn btn-primary addtocart" ForeColor="White" Text="Add To Cart" OnClientClick="return GetChildKitItemSelection();" />
</asp:Panel>
<asp:Literal ID="litCutomizeHtml" runat="server"></asp:Literal>



<asp:HiddenField ID="hdnSelectedText" runat="server"></asp:HiddenField>
<asp:HiddenField ID="hdnKitItemCode" runat="server"></asp:HiddenField>
<asp:HiddenField ID="hdnKitChildItems" runat="server"></asp:HiddenField>
<asp:HiddenField ID="hdnPortalURL" runat="server"></asp:HiddenField>

