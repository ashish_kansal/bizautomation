﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MultiCurrencyCountry.ascx.cs"
    Inherits="BizCart.UserControls.MultiCurrencyCountry" %>
<script type ="text/javascript" language="javascript">
    function DeleteCartItems(index) {
      
        if (confirm('After changing currency, All items from your shopping cart will be removed. Do you want to continue ?')) {
            __doPostBack('ClearCart', index);
            return false;
        }
        else {
            __doPostBack('Cancel', index);
            return false;
        }
    }
 </script>
<asp:Panel ID="pnlCutomizeHtml" runat="server">
    <table>
        <tr>
            <td>
                <asp:DropDownList ID="ddlCurrency" runat="server" CssClass="dropdown" 
                    AutoPostBack="false"  onchange="javascript:DeleteCartItems(this.selectedIndex);" >
                </asp:DropDownList>
            </td>
        </tr>
    </table>
</asp:Panel>
<asp:Literal ID="litCutomizeHtml" runat="server"></asp:Literal>