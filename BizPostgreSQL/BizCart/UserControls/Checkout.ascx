﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Checkout.ascx.cs" Inherits="BizCart.UserControls.Checkout" %>
<script language="javascript" type="text/javascript">
    function CheckNumber(cint, e) {
        var k;
        document.all ? k = e.keyCode : k = e.which;
        if (cint == 1) {
            if (!(k > 47 && k < 58 || k == 44 || k == 46 || k == 8 || k == 37 || k == 39 || k == 16)) {
                if (e.preventDefault) {
                    e.preventDefault();
                }
                else
                    e.returnValue = false;
                return false;
            }
        }
        if (cint == 2) {
            if (!(k > 47 && k < 58 || k == 8 || k == 37 || k == 39 || k == 16)) {
                if (e.preventDefault) {
                    e.preventDefault();
                }
                else
                    e.returnValue = false;
                return false;
            }
        }
    }
    function Mask(obj, evt) {
        evt = (evt) ? evt : window.event
        var charCode = (evt.which) ? evt.which : evt.keyCode
        var txtValue = document.getElementById("<%=txtCardNumber.ClientID %>");
            var hdnValue = document.getElementById("<%=hdnCardNumber.ClientID %>");

         //    if (charCode == 8) {
         //        if (hdnValue.value.length > txtValue.value.length) {
         //            hdnValue.value = hdnValue.value.substr(0, txtValue.value.length);
         //        }
         //    }
         //    else {
         //        hdnValue.value = hdnValue.value + String.fromCharCode(charCode);
         //    }

         //if (txtValue.value.length > 12) {
         //    txtValue.value = "############" + txtValue.value.substr(12, txtValue.value.length);
         //}
         //else {
         //    var mask = "";
         //    for (i = 0; i < txtValue.value.length; i++) mask += "#";
         //    txtValue.value = mask;
         //}
         hdnValue.value = hdnValue.value + String.fromCharCode(charCode);
         return true
     }
</script>
<asp:Panel ID="pnlCutomizeHtml" runat="server">
    <table width="100%" align="center" border="0" cellpadding="5" cellspacing="5">
        <tr>
            <td colspan="2">
                <div class="sectionheader">
                </div>
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td align="right" class="EditLink">
                <a id="hplEditCart" href="/Cart.aspx">Edit Shopping Cart</a> <a id="hplEditAddres"
                    href="/ConfirmAddress.aspx">Edit Shipping Details</a> <a id="hplEditPayment" href="/AddPayment.aspx">
                        Edit Cards Details</a>
            </td>
        </tr>
        <tr>
            <td align="left" colspan="2">
                 <%--<asp:RadioButton ID="radBillMe" runat="server" GroupName="rad" Text='&nbsp;<b>Bill Me</b> &nbsp;(If you or your company has terms with us.)'
                    AutoPostBack="true" Visible="false">
                </asp:RadioButton>--%>
            </td>
        </tr>
        <tr>
            <td colspan="2" align="left">
                <table align="left" runat="server" id="tblBillme" visible="false">
                    <tr>
                        <td class="LabelColumn">
                            Total Balance Due :
                        </td>
                        <td class="ControlCell">
                            <asp:Label ID="lblBalDue" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="LabelColumn">
                            Total Remaining Credit :
                        </td>
                        <td class="ControlCell">
                            <asp:Label ID="lblRemCredit" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="LabelColumn">
                            Total Amount Past Due :
                        </td>
                        <td class="ControlCell">
                            <asp:Label ID="lblAmtPastDue" runat="server"></asp:Label>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="left" colspan="2">
                <%--  <asp:RadioButton ID="radPay" Checked="true" AutoPostBack="true" runat="server" GroupName="rad"
                    Text='&nbsp;<b>Pay by Credit Card</b>'>
                </asp:RadioButton>--%>
            </td>
        </tr>
        <tr>
            <td colspan="2" align="left">
                <table id="tblCustomerInfo" runat="server" width="90%">
                    <tr>
                        <td width="50%">
                            <table cellspacing="2" cellpadding="5">
                                <tr>
                                    <th nowrap colspan="4">
                                        Credit Card Information
                                    </th>
                                </tr>
                                <tr>
                                    <td nowrap align="right">
                                        Card Type:
                                    </td>
                                    <td>
                                        <asp:DropDownList runat="server" ID="ddlCardType" CssClass="dropdown">
                                            <%--   <asp:ListItem Value="1">Visa </asp:ListItem>
                                        <asp:ListItem Value="2">MasterCard</asp:ListItem>
                                        <asp:ListItem Value="3">AMEX</asp:ListItem>
                                        <asp:ListItem Value="4">Discover</asp:ListItem>
                                        <asp:ListItem Value="5">Diners </asp:ListItem>
                                        <asp:ListItem Value="6">JCB</asp:ListItem>
                                        <asp:ListItem Value="7">BankCard</asp:ListItem>--%>
                                        </asp:DropDownList>
                                    </td>
                                    <td nowrap align="right">
                                        Card Number:
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtCardNumber" CssClass="textbox" runat="server" Width="180px" MaxLength="16"></asp:TextBox>
                                       
                                    </td>
                                </tr>
                                <tr>
                                    <td nowrap align="right">
                                        Card Holder Name:
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtCHName" Width="180" runat="server" CssClass="signup" MaxLength="100"></asp:TextBox>
                                    </td>
                                    <td nowrap align="right">
                                        CVV Data:
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtCardCVV2" CssClass="textbox" TextMode="Password" runat="server" Width="117px"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" nowrap>
                                        Exp Date (MM/YY):
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddlCardExpMonth" CssClass="dropdown" runat="server">
                                            <asp:ListItem Value="01">01</asp:ListItem>
                                            <asp:ListItem Value="02">02</asp:ListItem>
                                            <asp:ListItem Value="03">03</asp:ListItem>
                                            <asp:ListItem Value="04">04</asp:ListItem>
                                            <asp:ListItem Value="05">05</asp:ListItem>
                                            <asp:ListItem Value="06">06</asp:ListItem>
                                            <asp:ListItem Value="07">07</asp:ListItem>
                                            <asp:ListItem Value="08">08</asp:ListItem>
                                            <asp:ListItem Value="09">09</asp:ListItem>
                                            <asp:ListItem Value="10">10</asp:ListItem>
                                            <asp:ListItem Value="11">11</asp:ListItem>
                                            <asp:ListItem Value="12">12</asp:ListItem>
                                        </asp:DropDownList>
                                        &nbsp;/&nbsp;
                                        <asp:DropDownList ID="ddlCardExpYear" CssClass="dropdown" runat="server">
                                        </asp:DropDownList>
                                    </td>
                                    <td align="right" nowrap>
                                        &nbsp;
                                    </td>
                                    <td>
                                        &nbsp;
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
         <tr>
            <td align="left" colspan="2">
                <%--  <asp:RadioButton ID="radPay" Checked="true" AutoPostBack="true" runat="server" GroupName="rad"
                    Text='&nbsp;<b>Pay by Credit Card</b>'>
                </asp:RadioButton>--%>
            </td>
        </tr>
        <tr>
            <td colspan="2" align="left">
                <table id="tblGoogleCheckout" runat="server" width="90%">
                    <tr>
                        <td width="50%">
                           Google Checkout
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="2" align="left">
                <div id="tdTotalAmount">
                    Total Amount:
                </div>
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td align="left">
                <asp:Button ID="bCharge" runat="server" CssClass="button" Text="Submit" OnClick="bCharge_Click">
                </asp:Button>
            </td>
        </tr>
    </table>
</asp:Panel>
<asp:HiddenField ID ="hdnCardNumber" Value ="" ClientIDMode="Static" runat="server"/> 
<asp:Literal ID="litCutomizeHtml" runat="server"></asp:Literal>
<asp:TextBox ID="txtCredit" runat="server" Style="display: none"></asp:TextBox>
<asp:TextBox ID="txtDueAmount" runat="server" Style="display: none"></asp:TextBox>