﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="OrderOverview.ascx.cs" Inherits="BizCart.UserControls.OrderOverview" %>
<asp:Panel ID="pnlCutomizeHtml" runat="server">
    <table align="right" runat="server" id="tblCreditTerm">
        <tr>
            <td class="LabelColumn">
                Total Balance Due :
            </td>
            <td class="ControlCell">
                <asp:Label ID="lblBalDue" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="LabelColumn">
                Total Remaining Credit :
            </td>
            <td class="ControlCell">
                <asp:Label ID="lblRemCredit" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="LabelColumn">
                Total Amount Past Due :
            </td>
            <td class="ControlCell">
                <asp:Label ID="lblAmtPastDue" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="LabelColumn">
                Total Amount Past Due :
            </td>
            <td class="ControlCell">
                <asp:Label ID="lblCreditLimit" runat="server"></asp:Label>
            </td>
        </tr>
    </table>
</asp:Panel>

<asp:Literal ID="litCutomizeHtml" runat="server"></asp:Literal>