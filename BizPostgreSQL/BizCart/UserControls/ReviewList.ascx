﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ReviewList.ascx.cs"
    Inherits="BizCart.UserControls.ReviewList" %>
 
<asp:Panel ID="pnlCutomizeHtml" runat="server">
    <asp:DropDownList ID="ddlSortReview" runat="server" AutoPostBack="True" 
        onselectedindexchanged="ddlSortReview_SelectedIndexChanged">
        <asp:ListItem Text="Most Recent" Value="0"></asp:ListItem>
        <asp:ListItem Text="Most Helpful" Value="1"></asp:ListItem>
    </asp:DropDownList>
</asp:Panel>

<asp:Literal ID="litCutomizeHtml" runat="server"></asp:Literal>
<asp:HiddenField ID = "hfContactId" runat ="server" ClientIDMode ="Static"  />