﻿using System;
using System.Web;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using BACRM.BusinessLogic.Common;
using BACRM.BusinessLogic.Contacts;
using BACRM.BusinessLogic.Admin;
using BACRM.BusinessLogic.ShioppingCart;
using BACRM.BusinessLogic.Item;

namespace BizCart.UserControls
{
    public partial class CustomerAddress : BizUserControl
    {
        #region Events
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                Sites.InitialiseSession();
                if (Sites.ToLong(Session["UserContactID"]) == 0 && Sites.ToInteger(Session["OnePageCheckout"]) == 0)
                {
                    Response.Redirect("Login.aspx?ReturnURL=ConfirmAddress.aspx", true);
                }

                if (!IsPostBack)
                {
                    CCommon objCommon = new CCommon();
                    objCommon.sb_FillComboFromDBwithSel(ref ddlCountry, 40, Sites.ToLong(Session["DomainID"]));
                    //bug id 961 #2
                    SetDefaultCountry();
                    BindAddressName();
                    if (Request.QueryString["AId"] != null && Sites.ToLong(Request.QueryString["AId"]) > 0)
                    {
                        
                        if ((ddlAddressName.Items.FindByValue(Request.QueryString["AId"]) != null))
                        {
                            ddlAddressName.Items.FindByValue(Request.QueryString["AId"]).Selected = true;
                        }
                    }
                    else
                    {
                        //ddlAddressName.ClearSelection();
                        //ddlAddressName.SelectedIndex = 0;
                        //ddlAddressName.Visible = false;
                        //lbBillDelete.Visible = false;
                    }

                    if (Request.QueryString["AType"].ToString().ToLower() == "bill")
                        lblAddressTitle.Text = "Billing Address";
                    else if (Request.QueryString["AType"].ToLower() == "ship")
                        lblAddressTitle.Text = "Shipping Address";

                    if (ddlAddressName.SelectedIndex > 0)
                        LoadAddress();


                    Session["ShipAddress"] = null;
                    Session["BillAddress"] = null;

                    Session["AddressConfirm"] = null;

                    bindHtml();
                }

            }
            catch (Exception ex)
            {
                ExceptionModule.ExceptionPublish(ex, Sites.ToLong(Session["DomainID"]), Sites.ToLong(Session["UserContactID"]), Sites.ToLong(Session["SiteId"]), Request);
                ShowMessage(ex.ToString(), 1);
            }
        }
        protected void ddlAddressName_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                LoadAddress();
                if (Sites.ToLong(ddlAddressName.SelectedValue) == 0)
                    SetDefaultCountry();
                bindHtml();
            }
            catch (Exception ex)
            {
                ExceptionModule.ExceptionPublish(ex, Sites.ToLong(Session["DomainID"]), Sites.ToLong(Session["UserContactID"]), Sites.ToLong(Session["SiteId"]), Request);
                ShowMessage(ex.ToString(), 1);
            }
        }
        protected void lbBillDelete_Click(object sender, EventArgs e)
        {
            try
            {
                if (Sites.ToLong(ddlAddressName.SelectedValue) > 0)
                {
                    CContacts objContact = new CContacts();
                    objContact.DomainID = Sites.ToLong(Session["DomainID"]);
                    objContact.AddressID = Sites.ToLong(ddlAddressName.SelectedValue);
                    try
                    {
                        objContact.DeleteAddress();
                    }
                    catch (Exception ex)
                    {
                        if (ex.Message == "PRIMARY")
                        {
                            litMessage.Text = "Can not delete Default Address!!";
                        }
                        else
                        {
                            throw ex;
                        }
                    }
                    BindAddressName();
                    LoadAddress();
                    SetDefaultCountry();

                }
                bindHtml();
            }
            catch (Exception ex)
            {
                ExceptionModule.ExceptionPublish(ex, Sites.ToLong(Session["DomainID"]), Sites.ToLong(Session["UserContactID"]), Sites.ToLong(Session["SiteId"]), Request);
                ShowMessage(ex.ToString(), 1);
            }
        }
        protected void btnCancel_Click(object sender, EventArgs e)
        {
           try
           {
               if (Sites.ToString(Request["Return"]).Length > 0)
               {
                   Response.Redirect(Sites.ToString(Request["Return"]), true);
               }
               else
               {
                   if (CCommon.ToBool(Session["IsOnePageCheckout"]))
                   {
                       Session["SelectedStep"] = 1;
                       Response.Redirect("/OnePageCheckout.aspx", true);
                   }
                   else
                   {
                       Response.Redirect("ConfirmAddress.aspx", true);
                   }

               }

           }
           catch (Exception ex)
           {
               ExceptionModule.ExceptionPublish(ex, Sites.ToLong(Session["DomainID"]), Sites.ToLong(Session["UserContactID"]), Sites.ToLong(Session["SiteId"]), Request);
               ShowMessage(ex.ToString(), 1);
           }
        }
        protected void ddlCountry_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (ddlCountry.SelectedIndex > 0)
                {
                    ddlState.ClearSelection();
                    ddlState.Items.Clear();
                    FillState(ddlState, Sites.ToLong(ddlCountry.SelectedValue), Sites.ToLong(Session["DomainID"]));
                }
                bindHtml();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        protected void btnSubmit_Click(object sender, EventArgs e)
        {
           if (CheckPageValidate())
           {
               try
               {
                   save();
                   //BindAddressName();
                   //LoadAddress();
                   if (Sites.ToString(Request["Return"]).Length > 0)
                   {
                       Response.Redirect(Sites.ToString(Request["Return"]), true);
                   }
                   else
                   {
                       if (CCommon.ToBool(Session["IsOnePageCheckout"]))
                       {
                           Session["SelectedStep"] = 1;
                           Response.Redirect("/OnePageCheckout.aspx", true);
                       }
                       else
                       {
                           Response.Redirect("ConfirmAddress.aspx" + (!string.IsNullOrEmpty(Request["ReturnURL"]) ? "?ReturnURL=" + Request["ReturnURL"] : ""), true);
                       }
                   }
               }
               catch (Exception ex)
               {
                   ExceptionModule.ExceptionPublish(ex, Sites.ToLong(Session["DomainID"]), Sites.ToLong(Session["UserContactID"]), Sites.ToLong(Session["SiteId"]), Request);
                   ShowMessage(ex.ToString(), 1);
               }
           }
        }
        #endregion

        #region Methods
        public bool CheckPageValidate()
        {
            try
            {
                if (Request.QueryString["AId"] != null && Sites.ToLong(Request.QueryString["AId"]) > 0)
                {
                    if ((ddlAddressName.SelectedIndex == 0))
                    {
                        ShowMessage(GetErrorMessage("ERR019"), 1);//Select Address
                        return false;
                    }
                    else if (txtAddressName.Text.Trim().Length == 0)
                    {
                        ShowMessage(GetErrorMessage("ERR020"), 1);//Invalid Item.
                        return false;
                    }
                    else if (txtStreet.Text.Trim().Length == 0)
                    {
                        ShowMessage(GetErrorMessage("ERR021"), 1);//Enter Street
                        return false;
                    }
                    else if (txtCity.Text.Trim().Length == 0)
                    {
                        ShowMessage(GetErrorMessage("ERR022"), 1);//Enter City
                        return false;
                    }
                    else if (ddlCountry.SelectedIndex == 0)
                    {
                        ShowMessage(GetErrorMessage("ERR023"), 1);//Select Country
                        return false;
                    }
                    else if (ddlState.SelectedIndex == 0)
                    {
                        ShowMessage(GetErrorMessage("ERR024"), 1);//Select State
                        return false;
                    }
                    else if (txtPostal.Text.Trim().Length == 0)
                    {
                        ShowMessage(GetErrorMessage("ERR025"), 1);//Enter Zip Code
                        return false;
                    }
                }
                else if (txtAddressName.Text.Trim().Length == 0)
                {
                    ShowMessage(GetErrorMessage("ERR020"), 1);//Invalid Item.
                    return false;
                }
                else if (txtStreet.Text.Trim().Length == 0)
                {
                    ShowMessage(GetErrorMessage("ERR021"), 1);//Enter Street
                    return false;
                }
                else if (txtCity.Text.Trim().Length == 0)
                {
                    ShowMessage(GetErrorMessage("ERR022"), 1);//Enter City
                    return false;
                }
                else if (ddlCountry.SelectedIndex == 0)
                {
                    ShowMessage(GetErrorMessage("ERR023"), 1);//Select Country
                    return false;
                }
                else if (ddlState.SelectedIndex == 0)
                {
                    ShowMessage(GetErrorMessage("ERR024"), 1);//Select State
                    return false;
                }
                else if (txtPostal.Text.Trim().Length == 0)
                {
                    ShowMessage(GetErrorMessage("ERR025"), 1);//Enter Zip Code
                    return false;
                }
                return true;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        public void save()
        {
            try
            {
                CContacts objContacts = new CContacts();
                if (txtAddressName.Text.Trim().Length > 1)
                {
                    objContacts.AddressID = Sites.ToLong(ddlAddressName.SelectedValue);
                    objContacts.AddressName = txtAddressName.Text.Trim();

                    if (!string.IsNullOrEmpty(txtContactName.Text.Trim()))
                    {
                        objContacts.IsAltContact = true;
                        objContacts.AltContact = Sites.ToString(txtContactName.Text.Trim());
                    }
                    objContacts.Street = txtStreet.Text.Trim();
                    objContacts.City = txtCity.Text.Trim();
                    objContacts.Country = Sites.ToLong(ddlCountry.SelectedItem.Value);
                    objContacts.PostalCode = txtPostal.Text.Trim();
                    objContacts.State = Sites.ToLong(ddlState.SelectedItem.Value);
                    objContacts.AddresOf = CContacts.enmAddressOf.Organization;

                    if (Request.QueryString["AType"].ToString().ToLower() == "bill")
                        objContacts.AddressType = CContacts.enmAddressType.BillTo;
                    else if (Request.QueryString["AType"].ToString().ToLower() == "ship")
                        objContacts.AddressType = CContacts.enmAddressType.ShipTo;

                    objContacts.IsPrimaryAddress = chkIsPrimary.Checked;
                    objContacts.RecordID = Sites.ToLong(Session["DivId"]);
                    objContacts.DomainID = Sites.ToLong(Session["DomainID"]);
                    objContacts.ManageAddress();
                    if (chkShippingSameAsBilling != null)
                    {
                        if (Request.QueryString["AType"].ToString().ToLower() == "bill")
                        {
                            if (chkShippingSameAsBilling.Checked)
                            {
                                objContacts.AddressID = 0;
                                objContacts.AddressType = CContacts.enmAddressType.ShipTo;
                                objContacts.ManageAddress();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        void bindHtml()
        {
            try
            {
                string strUI = GetUserControlTemplate("CustomerAddress.htm");

                strUI = strUI.Replace("##AddressType##", CCommon.RenderControl(lblAddressTitle));
                strUI = strUI.Replace("##ErrorMessage##", litMessage.Text);
                strUI = strUI.Replace("##AddressNameDropDown##", CCommon.RenderControl(ddlAddressName));
                strUI = strUI.Replace("##DeleteAddressLink##", CCommon.RenderControl(lbBillDelete));
                strUI = strUI.Replace("##AddressName##", CCommon.RenderControl(txtAddressName));
                strUI = strUI.Replace("##ContactName##", CCommon.RenderControl(txtContactName));
                strUI = strUI.Replace("##Street##", CCommon.RenderControl(txtStreet));
                strUI = strUI.Replace("##City##", CCommon.RenderControl(txtCity));
                strUI = strUI.Replace("##State##", CCommon.RenderControl(ddlState));
                strUI = strUI.Replace("##ZipCode##", CCommon.RenderControl(txtPostal));
                strUI = strUI.Replace("##Country##", CCommon.RenderControl(ddlCountry));
                strUI = strUI.Replace("##IsPrimaryCheckBox##", CCommon.RenderControl(chkIsPrimary));
                if (Request.QueryString["AType"].ToString().ToLower() == "bill")
                {
                    strUI = strUI.Replace("##IsShippingSameAsBillingCheckBox##", CCommon.RenderControl(chkShippingSameAsBilling));
                }
                strUI = strUI.Replace("##SaveButton##", CCommon.RenderControl(btnSubmit));
                strUI = strUI.Replace("##CancelButton##", CCommon.RenderControl(btnCancel));
                //strUI = strUI.Replace("##isvisible##", Sites.ToLong(Request.QueryString["AId"]) > 0 ? "" : "display:None");
                strUI = strUI.Replace("##isvisible##", "");


                pnlCutomizeHtml.Visible = false;
                litCutomizeHtml.Text = strUI;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private void SetDefaultCountry()
        {
            try
            {
                DataTable dtTable = default(DataTable);
                UserAccess objUserAccess = new UserAccess();
                objUserAccess.DomainID = Sites.ToLong(Session["DomainID"]);
                dtTable = objUserAccess.GetDomainDetails();
                if ((ddlCountry.Items.FindByValue(dtTable.Rows[0]["numDefCountry"].ToString()) != null))
                {
                    ddlCountry.ClearSelection();
                    ddlCountry.Items.FindByValue(dtTable.Rows[0]["numDefCountry"].ToString()).Selected = true;

                    if (ddlCountry.SelectedIndex > 0)
                    {
                        ddlState.ClearSelection();
                        //ddlState.Items.Clear();
                        FillState(ddlState, Sites.ToLong(ddlCountry.SelectedValue), Sites.ToLong(Session["DomainID"]));
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private void BindAddressName()
        {
            try
            {
                CContacts objContact = new CContacts();

                if (Request.QueryString["AType"].ToString().ToLower() == "bill")
                    objContact.AddressType = CContacts.enmAddressType.BillTo;
                else if (Request.QueryString["AType"].ToLower() == "ship")
                    objContact.AddressType = CContacts.enmAddressType.ShipTo;

                objContact.DomainID = Sites.ToLong(Session["DomainID"]);
                objContact.RecordID = Sites.ToLong(Session["DivId"]);
                objContact.AddresOf = CContacts.enmAddressOf.Organization;
                objContact.byteMode = 2;
                DataTable dt = new DataTable();
                dt = objContact.GetAddressDetail();
                ddlAddressName.DataValueField = "numAddressID";
                ddlAddressName.DataTextField = "vcAddressName";
                ddlAddressName.DataSource = dt;
                ddlAddressName.DataBind();
                ddlAddressName.Items.Insert(0, new ListItem("Add new address", "0"));
                if (dt.Rows.Count > 0)
                {
                    DataRow[] dr = dt.Select("bitIsPrimary = true");
                    if (dr.Length > 0)
                    {
                        string addressId = "0";
                        addressId = Convert.ToString(dr[0]["numAddressID"]);
                        if (ddlAddressName.Items.FindByValue(addressId) != null)
                        {
                            ddlAddressName.Items.FindByValue(addressId).Selected = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void LoadAddress()
        {
            try
            {
                DataTable dtTable = null;
                CContacts objContact = new CContacts();
                objContact.DomainID = Sites.ToLong(Session["DomainID"]);
                objContact.AddressID = Sites.ToLong(ddlAddressName.SelectedValue);
                objContact.byteMode = 1;
                dtTable = objContact.GetAddressDetail();

                if (dtTable.Rows.Count > 0)
                {
                    txtAddressName.Text = CCommon.ToString(dtTable.Rows[0]["vcAddressName"]);

                    if (Sites.ToBool(dtTable.Rows[0]["bitAltContact"]))
                    {
                        txtContactName.Text = Sites.ToString(dtTable.Rows[0]["vcAltContact"]);
                    }

                    txtStreet.Text = (dtTable.Rows[0]["vcStreet"] == System.DBNull.Value ? "" : dtTable.Rows[0]["vcStreet"].ToString());
                    txtCity.Text = (dtTable.Rows[0]["vcCity"] == System.DBNull.Value ? "" : dtTable.Rows[0]["vcCity"].ToString());

                    if (!(dtTable.Rows[0]["numCountry"] == System.DBNull.Value))
                    {
                        if ((ddlCountry.Items.FindByValue(dtTable.Rows[0]["numCountry"].ToString()) != null))
                        {
                            ddlCountry.ClearSelection();
                            ddlCountry.Items.FindByValue(dtTable.Rows[0]["numCountry"].ToString()).Selected = true;
                        }
                    }
                    if (ddlCountry.SelectedIndex > 0)
                    {
                        FillState(ddlState, Sites.ToLong(ddlCountry.SelectedValue), Sites.ToLong(Session["DomainID"]));
                        if (!(dtTable.Rows[0]["numState"] == System.DBNull.Value))
                        {
                            if ((ddlState.Items.FindByValue(dtTable.Rows[0]["numState"].ToString()) != null))
                            {
                                ddlState.ClearSelection();
                                ddlState.Items.FindByValue(dtTable.Rows[0]["numState"].ToString()).Selected = true;
                            }
                        }
                    }

                    txtPostal.Text = (dtTable.Rows[0]["vcPostalCode"] == System.DBNull.Value ? "" : dtTable.Rows[0]["vcPostalCode"].ToString());
                    chkIsPrimary.Checked = CCommon.ToBool(dtTable.Rows[0]["bitIsPrimary"]);
                }
                else
                {
                    txtAddressName.Text = "";
                    txtContactName.Text = "";
                    txtStreet.Text = "";
                    txtPostal.Text = "";
                    txtCity.Text = "";
                    ddlState.ClearSelection();
                    ddlState.SelectedValue = "0";
                    ddlCountry.ClearSelection();
                    ddlCountry.SelectedValue = "0";
                    chkIsPrimary.Checked = false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
    }
}