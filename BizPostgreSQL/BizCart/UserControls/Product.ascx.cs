﻿using BACRM.BusinessLogic.Common;
using BACRM.BusinessLogic.Item;
using BACRM.BusinessLogic.Promotion;
using BACRM.BusinessLogic.ShioppingCart;
using System;
using System.Data;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;

namespace BizCart.UserControls
{
    public partial class Product : BizUserControl
    {
        #region Global Declaration

        long lngCategoryID = 0;
        public int tintOfferTriggerValueType = 0;
        public double fltOfferTriggerValue = 0;

        string strCategoryName = string.Empty;
        string strProductName = string.Empty;
        string strValues = string.Empty;
        string qualifiedshipping = string.Empty;

        public string PromotionForProduct = string.Empty;
        public string CouponCode = string.Empty;
        public string ShippingForOrder = string.Empty;

        public string bitRequireCouponCode = "0";

        DataSet dsTemp = null;
        DataTable dtOppAtributes = null;
        DataTable dtOppOptAttributes = null;

        CItems objItems = null;
        CCommon objCommon = null;
        QueryStringValues objEncryption = null;

        #endregion

        #region Properties

        public string AttributeControlType { get; set; }
        public string AfterAddingItemShowCartPage { get; set; }

        #region Custom Properties

        public long ItemId
        {
            get
            {
                object o = ViewState["ItemId"];
                if (o != null)
                {
                    return CCommon.ToLong(o);
                }
                else
                {
                    if (CCommon.ToString(Request.QueryString["ItemId"]) != "")
                    {
                        ViewState["ItemId"] = Request.QueryString["ItemId"];
                        return CCommon.ToLong(ViewState["ItemId"]);
                    }
                    else
                    {
                        return 0;
                    }
                }
            }
        }

        public String CategoryName
        {
            get
            {
                object o = ViewState["CategoryName"];
                if (o != null)
                {
                    return (string)o;
                }
                else
                {
                    if (CCommon.ToString(Request.QueryString["CategoryName"]) != "")
                    {
                        ViewState["CategoryName"] = Request.QueryString["CategoryName"];
                        return (string)ViewState["CategoryName"];
                    }
                    else
                    {
                        return "";
                    }
                }
            }
        }

        public String ProductName
        {
            get
            {
                object o = ViewState["ProductName"];
                if (o != null)
                {
                    return (string)o;
                }
                else
                {
                    if (CCommon.ToString(Request.QueryString["ProductName"]) != "")
                    {
                        ViewState["ProductName"] = Request.QueryString["ProductName"];
                        return (string)ViewState["ProductName"];
                    }
                    else
                    {
                        return "";
                    }
                }
            }
        }

        public Boolean IsMetatagAdded
        {
            get
            {
                object o = ViewState["IsMetatagAdded"];
                if (o != null)
                {
                    return (Boolean)o;
                }
                else
                {
                    return false;
                }
            }
            set
            {
                ViewState["IsMetatagAdded"] = value;
            }
        }

        #endregion

        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                Sites.InitialiseSession();

                string strGetUrlValue = Request.RawUrl.ToString();
                if (strGetUrlValue.Split(new string[] { "/" }, StringSplitOptions.RemoveEmptyEntries).Length >= 2)
                {
                    strCategoryName = strGetUrlValue.Split(new string[] { "/" }, StringSplitOptions.RemoveEmptyEntries)[1];
                    strProductName = strGetUrlValue.Split(new string[] { "/" }, StringSplitOptions.RemoveEmptyEntries)[2];
                }
                objItems = new CItems();

                lnkOptItemAdd.Attributes.Add("onclick", "return AddOption('" + txtOptUnits.ClientID + "','" + txtHidOptValue.ClientID + "','" + txtAddedItems.ClientID + "','" + ddlOptItem.ClientID + "','')");
                lnkAddToCart.Attributes.Add("onclick", "return Add('" + txtUnits.ClientID + "','" + txtHidValue.ClientID + "','" + txtAddedItems.ClientID + "','" + txtWareHouseItemID.ClientID + "')");
                hplCheckout.Attributes.Add("OnClick", "javascript:return false;");

                if (!IsPostBack)
                {
                    objEncryption = new QueryStringValues();

                    if (this.ItemId > 0)
                    {
                        objItems = new CItems();
                        objItems.DomainID = CCommon.ToLong(Session["DomainId"]);
                        objItems.ItemCode = CCommon.ToInteger(this.ItemId);
                        DataTable dt = objItems.ValidateItemDomain();

                        if (dt == null || dt.Rows.Count == 0 || !CCommon.ToBool(dt.Rows[0]["IsItemInDomain"]))
                        {
                            throw new HttpException(404, "Item not found");
                        }
                    }
                    else
                    {
                        throw new HttpException(404, "Item not found");
                    }

                    hfSiteId.Value = objEncryption.Encrypt(CCommon.ToString(Session["SiteId"]));
                    hfDomainId.Value = objEncryption.Encrypt(CCommon.ToString(Session["DomainId"]));
                    hfTypeId.Value = objEncryption.Encrypt("2");
                    hfItemId.Value = objEncryption.Encrypt(CCommon.ToString(this.ItemId));
                    hfIpAddress.Value = objEncryption.Encrypt(GetIpAddress());
                    hfContactId.Value = objEncryption.Encrypt(CCommon.ToString(Session["ContactId"]));
                    lblOfferPriceCurrency.Text = CCommon.ToString(Session["CurrSymbol"]);
                    lblProductPriceCurrency.Text = CCommon.ToString(Session["CurrSymbol"]);
                    lblMSRPPriceCurrency.Text = CCommon.ToString(Session["CurrSymbol"]);

                    trPrice.Visible = !Boolean.Parse(Session["HidePrice"].ToString());

                    bindCombineAssemblies();
                    bindShippingPromotionOffer();
                    //Get items Added cart
                    // createSet();
                    DataTable dtTable = default(DataTable);
                    dsTemp = GetCartItem(); //((DataSet)Session["Data"]);
                    dtTable = dsTemp.Tables[0];
                    txtAddedItems.Text = "";

                    foreach (DataRow dr in dtTable.Rows)
                    {
                        txtAddedItems.Text = txtAddedItems.Text + dr["numWarehouseItmsID"] + ",";
                    }
                    txtAddedItems.Text = txtAddedItems.Text.Trim(',');

                    if (Session["WareHouseID"] == null)
                    {
                        if (Sites.ToString(Session["DivisionID"]) != "")
                        {
                            objItems.DivisionID = Sites.ToLong(Session["DivisionID"]);
                            objItems.DomainID = Sites.ToLong(Session["DomainID"]);
                            Session["WareHouseID"] = objItems.GetWarehouseFromDivID();
                        }
                        else
                        {
                            objItems.DomainID = Sites.ToLong(Session["DomainID"]);
                            Session["WareHouseID"] = objItems.GetWarehouseFromDivID();
                        }
                    }
                    bindHtml();
                }

                if (this.ItemId > 0 & txtHidValue.Text == "False")
                {
                    CreateAttributes();
                    bindHtml();
                }

                if (ddlOptItem.SelectedIndex > 0 & !string.IsNullOrEmpty(txtHidOptValue.Text)) CreateOptAttributes();

                BreadCrumb.Clear();

                BreadCrumb.OverWriteBreadCrumb(1, "Home.aspx", "Home", "Home.aspx");

                BreadCrumb.OverWriteBreadCrumb(2, "Categories.aspx", "Category", "Categories.aspx");


                objItems.CategoryName = strCategoryName.Replace('-', ' ');
                objItems.ItemCode = (int)this.ItemId;
                objItems.SiteID = Sites.ToLong(Session["SiteId"]);

                lngCategoryID = objItems.GetCategoryID();

                Sites objSites = new Sites();
                objSites.DomainID = Sites.ToLong(Session["DomainID"]);
                objSites.CategoryID = lngCategoryID;
                DataTable dtCategories = objSites.GetParentCategories();

                short index = 2;

                if (dtCategories != null && dtCategories.Rows.Count > 0)
                {
                    foreach (DataRow dr in dtCategories.Rows)
                    {
                        index += 1;
                        BreadCrumb.OverWriteBreadCrumb(index, "SubCategory/" + Sites.ToString(dr["vcCategoryName"]) + "/1/" + Sites.ToString(dr["numCategoryID"]), Sites.ToString(dr["vcCategoryName"]), "Categories");
                    }
                }

                index += 1;
                BreadCrumb.OverWriteBreadCrumb(index, "Product/" + strCategoryName + "/" + lblProductName.Text.Replace(' ', '-') + "/" + CCommon.ToString(this.ItemId), lblProductName.Text, "Product");

                if (Page.IsPostBack)
                {
                    if (Sites.ToString(Request["__EVENTTARGET"]) == "AddToList")
                    {
                        lnkAddToFav_Click();   // lnkAddToFav_Click
                    }
                    else if (Sites.ToString(Request["__EVENTTARGET"]) == "RedirectToLogin")
                    {
                        Session["PreviousProductPage"] = (Sites.ToString(Session["SiteLiveURL"]).Trim().Length > 2 ? "https://" : "http://") + Request.Url.Host + "/" + Request.RawUrl;
                        Response.Redirect("/Login.aspx", false);
                    }
                    if (Sites.ToString(Request["__EVENTTARGET"]) == "AddToCart")
                    {
                        string itemid = Convert.ToString(Request["__EVENTARGUMENT"]);
                        AddToCartFromEcomm("", Sites.ToLong(Request["__EVENTARGUMENT"]), 0, "", false, Sites.ToInteger(hdnSQty.Text), childKitSelectedValues: Sites.ToString(hdnChildKitSelection.Value));
                        bindShippingPromotionOffer();
                        if (Convert.ToString(HttpContext.Current.Session["PreUpSell"]) == "True")
                        {
                            CItems objItem = new CItems();
                            objItem.DomainID = Sites.ToLong(Session["DomainID"]);
                            objItem.ParentItemCode = Sites.ToLong(Request["__EVENTARGUMENT"]); //9;
                            //objItem.byteMode = 4;
                            objItem.byteMode = 8;
                            DataTable dtItemPrepopup = new DataTable();
                            objItem.SiteID = Sites.ToLong(Session["SiteId"]);
                            dtItemPrepopup = objItem.GetSimilarItem();

                            DataSet ds = GetCartItem();
                            DataTable dtCart = ds.Tables[0];

                            DataTable duplicateRows = new DataTable();
                            if (dtCart.Rows.Count > 0 && dtItemPrepopup.Rows.Count > 0)
                            {
                                bindMiniCart();
                                /*duplicateRows = (from a in dtCart.AsEnumerable()
                                                           join b in dtItemPrepopup.AsEnumerable()
                                                           on a["numItemCode"].ToString()
                                                           equals b["numItemCode"].ToString() into g
                                                           where g.Count() > 0
                                                           select a).CopyToDataTable();*/

                                var rows = from a in dtCart.AsEnumerable()
                                           join b in dtItemPrepopup.AsEnumerable()
                                           on a["numItemCode"].ToString() equals b["numItemCode"].ToString()
                                           into g
                                           where g.Count() > 0
                                           select a;

                                if (rows.Any())
                                    duplicateRows = rows.CopyToDataTable();
                            }
                            if (duplicateRows != null && duplicateRows.Rows.Count <= 0)
                            {
                                if (dtItemPrepopup.Rows.Count > 0)
                                {
                                    gvRelatedItems.DataSource = dtItemPrepopup;
                                    gvRelatedItems.DataBind();

                                    dialogSellPopup.Visible = true;
                                    this.Page.ClientScript.RegisterStartupScript(this.GetType(), "ShowMiniPreSellCart", "ShowMiniPreSellCart();", true);
                                    /*foreach (DataRow dr in dtItemPrepopup.Rows)
                                    {
                                        RelatedItemsPromotion(CCommon.ToInteger(dr["numItemCode"]), CCommon.ToLong(dr["numWareHouseItemID"]));
                                    }*/

                                    #region Commented
                                    /*lblpopuptitle.Text = Convert.ToString(Convert.ToString(Session["vcPreSellUp"]));
                                    StringBuilder strbui = new StringBuilder();
                                    strbui.Append("<table class='upsellgrid'><tr><th></th><th>Item Name</th><th>Description</th><th>Quantity</th><th></th></tr>");
                                    foreach (DataRow dr in dtItemPrepopup.Rows)
                                    {
                                        string url = GetProductURL(dr["vcCategoryName"].ToString(), dr["vcItemName"].ToString(), dr["numItemCode"].ToString());

                                        string ItemImagePath = GetImagePath(dr["vcPathForTImage"].ToString());
                                        strbui.Append("<tr><td><img src=" + ItemImagePath + " alt=" + dr["vcItemName"].ToString() + " title=" + dr["vcItemName"].ToString() + " height=\"150\" width=\"130\" /></td><td><a href=" + url + ">" + dr["vcItemName"].ToString() + "</a></td><td>" + dr["txtItemDesc"].ToString() + "<br/>" + dr["txtUpSellItemDesc"].ToString() + "</td><td><input type=\"button\" value=\"-\" name=\'removenQty\' onclick=\"removeQty('" + Sites.ToLong(dr["numItemCode"]) + "')\"/>" + "<input type=\"text\" value=\"1\" name=\"txtnSQty\" id='" + Sites.ToLong(dr["numItemCode"]) + "' class=\"txtSQty\"/>" + "<input type=\"button\" value=\"+\" name=\'addnQty\' onClick=\"addQty('" + Sites.ToLong(dr["numItemCode"]) + "')\" class=\"addqty\"/></td><td> <input id=\"AddToCartLink\" name=\"AddToCart\" type=\"button\" onclick=\"addtocartmngqty('AddToCart', " + dr["numItemCode"].ToString() + ");\" class=\"addtocart\" style=\"border: 0;\" /></td></tr>");
                                    }
                                    strbui.Append("</table");
                                    ltrlpopuplist.Text = strbui.ToString();*/
                                    #endregion
                                }
                                else
                                {
                                    dialogSellPopup.Visible = false;
                                    if (CCommon.ToBool(AfterAddingItemShowCartPage))
                                    {
                                        Response.Redirect("/Cart.aspx", false);
                                    }
                                }
                            }
                            else
                            {
                                gvRelatedItems.Columns[8].Visible = true;

                                foreach (DataRow dr in duplicateRows.Rows)
                                {
                                    long _numitemCode = CCommon.ToLong(dr["numItemCode"]);

                                    if (dtItemPrepopup.Rows.Count > 0)
                                    {
                                        gvRelatedItems.DataSource = dtItemPrepopup;
                                        gvRelatedItems.DataBind();
                                        long _gvnumitemCode = 0;
                                        dialogSellPopup.Visible = true;

                                        this.Page.ClientScript.RegisterStartupScript(this.GetType(), "ShowMiniPreSellCart", "ShowMiniPreSellCart();", true);
                                        /*foreach (DataRow drPre in dtItemPrepopup.Rows)
                                        {
                                            RelatedItemsPromotion(CCommon.ToInteger(drPre["numItemCode"]), CCommon.ToLong(drPre["numWareHouseItemID"]));
                                        }*/

                                        for (int i = 0; i < gvRelatedItems.Rows.Count; i++)
                                        {
                                            _gvnumitemCode = CCommon.ToLong(gvRelatedItems.Rows[i].Cells[8].Text);
                                            Button btnAdd = (Button)(gvRelatedItems.Rows[i].Cells[6].FindControl("btnAdd"));
                                            if (_gvnumitemCode == _numitemCode)
                                            {
                                                btnAdd.Enabled = false;
                                                btnAdd.Text = "Already Added";
                                            }
                                        }
                                    }
                                }
                                gvRelatedItems.Columns[8].Visible = false;
                            }
                        }
                        else
                        {
                            dialogSellPopup.Visible = false;
                            if (CCommon.ToBool(AfterAddingItemShowCartPage))
                            {
                                Response.Redirect("/Cart.aspx", false);
                            }
                        }
                    }
                }
                bindShippingPromotionOffer();

                if (Page.IsPostBack)
                {
                    bindHtml();
                }
            }
            catch (HttpException ex)
            {
                if (ex.GetHttpCode() == 404)
                {
                    throw;
                }
                else
                {
                    ExceptionModule.ExceptionPublish(ex, Sites.ToLong(Session["DomainID"]), Sites.ToLong(Session["UserContactID"]), Sites.ToLong(Session["SiteId"]), Request);
                    ShowMessage(ex.ToString(), 1);
                }
            }
            catch (Exception ex)
            {
                ExceptionModule.ExceptionPublish(ex, Sites.ToLong(Session["DomainID"]), Sites.ToLong(Session["UserContactID"]), Sites.ToLong(Session["SiteId"]), Request);
                ShowMessage(ex.ToString(), 1);
            }
        }

        /*Added by Neelam on 10/11/2017 - Added event to navigate on checkout*/
        /*protected void hplCheckout_Click(object sender, EventArgs e)
        {
            string url = "/" + CheckoutPageName();
            ////window.location.replace("http://stackoverflow.com");
            //hplCheckout.PostBackUrl = "/" + CheckoutPageName();
            string str = "<script language='javascript'>e.preventDefault(); window.location.replace(" + HttpUtility.JavaScriptStringEncode(url) + "); return false;</script>";
            this.Page.ClientScript.RegisterStartupScript(this.GetType(), "RedirectToCheckout", "str", true);
            //Dim str As String
            //str = "<script language='javascript'>window.open('../contact/frmComposeWindow.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&PickAtch=1&Lsemail=" & HttpUtility.JavaScriptStringEncode(strConEmail) & "&pqwRT=" & lngDivID & "','','toolbar=no,titlebar=no,top=100,left=100,width=850,height=550,scrollbars=yes,resizable=yes')</script>"
            //    Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "Email", str, False)
        }*/

        protected void btnApplyCoupon_Click(object sender, EventArgs e)
        {
            DataSet dsTemp;
            //Get items Added cart
            //createSet();
            dsTemp = GetCartItem(); //(DataSet)Session["Data"];
            DataTable dtItem = default(DataTable);
            dtItem = dsTemp.Tables[0];
            DataRow[] dr = dtItem.Select("numItemCode=" + this.ItemId + "");
            int units = 0;
            double price = 0;
            foreach (DataRow drd in dr)
            {
                units = Sites.ToInteger(drd["numUnitHour"]);
                price = Sites.ToDouble(drd["monTotAmtBefDiscount"]);
            }
            if (Convert.ToString(hdnPromotionCoupon.Value).Length > 0)
            {
                BACRM.BusinessLogic.ShioppingCart.Cart _cart = new BACRM.BusinessLogic.ShioppingCart.Cart();
                string uomid = Convert.ToString(hfUOM.Value);
                DataTable dsPromotion = new DataTable();
                _cart.DomainID = Sites.ToLong(Session["DomainID"]);
                string[] str = ddlItemAttribute != null ? ddlItemAttribute.SelectedValue.Split('~') : null;
                if (str != null && str.Length == 2)
                {
                    _cart.ItemCode = Convert.ToInt32(str[1]);
                }
                else
                {
                    _cart.ItemCode = (int)this.ItemId;
                }
                _cart.UserCntID = CCommon.ToLong(Session["UserContactID"]);
                _cart.SiteID = Sites.ToLong(Session["SiteId"]);
                HttpCookie cookie = Request.Cookies["Cart"];
                if (Request.Cookies["Cart"] != null)
                {
                    _cart.CookieId = (cookie["KeyId"] != null) ? cookie["KeyId"] : "";
                }
                else
                {
                    _cart.CookieId = "";
                }
                string deleteOutput = _cart.RemoveCouponFromCart(hdnPromotionCoupon.Value);
                btnApplyCoupon.Text = "Apply Coupon";
                txtCouponCode.Visible = true;
                ltrlcouponcode.Visible = false;
                hdnPromotionCoupon.Value = "";
                bindShippingPromotionOffer();
            }
            else
            {
                string[] strCoupon = CouponCode.Split(',');
                int countcoupon = 0;
                foreach (string res in strCoupon)
                {
                    if (res == txtCouponCode.Text)
                    {
                        countcoupon = 1;
                    }
                }
                if (txtCouponCode.Text != "" && countcoupon > 0)
                {
                    BACRM.BusinessLogic.ShioppingCart.Cart _cart = new BACRM.BusinessLogic.ShioppingCart.Cart();
                    string uomid = Convert.ToString(hfUOM.Value);
                    DataTable dsPromotion = new DataTable();
                    _cart.DomainID = Sites.ToLong(Session["DomainID"]);
                    dsPromotion = _cart.GetCartItemPromotionDetail(CCommon.ToLong(uomid), Convert.ToString(this.ItemId), CCommon.ToLong(Session["DefCountry"]), txtCouponCode.Text);
                    if (dsPromotion.Rows.Count > 0)
                    {
                        tintOfferTriggerValueType = Sites.ToInteger(dsPromotion.Rows[0]["tintOfferTriggerValueType"]);
                        fltOfferTriggerValue = Sites.ToDouble(dsPromotion.Rows[0]["fltOfferTriggerValue"]);
                    }
                    if (tintOfferTriggerValueType == 1)
                    {
                        if ((Convert.ToInt32(txtUnits.Text) + Convert.ToInt32(units)) >= fltOfferTriggerValue)
                        {
                            ltrlcouponcode.Text = "<span class=\"success-label couponspan\">Coupon Applied Sucessfully</span>";
                            hdnPromotionCoupon.Value = txtCouponCode.Text;
                            btnApplyCoupon.Text = "Remove Coupon";
                            txtCouponCode.Visible = false;
                            if (dsPromotion.Rows.Count > 0)
                            {
                                PromotionForProduct = Sites.ToString(dsPromotion.Rows[0]["vPromotionDesc"]);
                            }
                        }
                        else
                        {
                            ltrlcouponcode.Text = "<span class=\"danger-label couponspan\">Buy " + fltOfferTriggerValue + " to avail promotion offer </span>";
                            hdnPromotionCoupon.Value = "";
                        }
                    }
                    if (tintOfferTriggerValueType == 2)
                    {
                        if (((Convert.ToDouble(hdnItemPrice.Value) * Convert.ToDouble(txtUnits.Text)) + Convert.ToDouble(price)) >= fltOfferTriggerValue && CouponCode == txtCouponCode.Text)
                        {
                            ltrlcouponcode.Text = "<span class=\"success-label couponspan\">Coupon Applied Sucessfully</span>";
                            hdnPromotionCoupon.Value = txtCouponCode.Text;
                            btnApplyCoupon.Text = "Remove Coupon";
                            txtCouponCode.Visible = false;
                            if (dsPromotion.Rows.Count > 0)
                            {
                                PromotionForProduct = Sites.ToString(dsPromotion.Rows[0]["vPromotionDesc"]);
                            }
                        }
                        else
                        {
                            ltrlcouponcode.Text = "<span class=\"danger-label couponspan\">Buy " + Sites.ToString(Session["CurrSymbol"]) + fltOfferTriggerValue + " of current item to avail promotion offer</span>";
                            hdnPromotionCoupon.Value = "";
                        }
                    }
                }
                else
                {
                    ltrlcouponcode.Text = "<span class=\"danger-label couponspan\">Invalid Coupon !!</span>";
                    hdnPromotionCoupon.Value = "";
                }
            }
            bindHtml();
        }

        protected void gvRelatedItems_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "Add")
                {
                    GridViewRow gvr = (GridViewRow)(((Button)e.CommandSource).NamingContainer);
                    int RowIndex = gvr.RowIndex;
                    int index = Convert.ToInt32(e.CommandArgument);
                    int parentItemCode = Convert.ToInt32(gvRelatedItems.DataKeys[RowIndex].Values["numParentItemCode"]);

                    Button btnAdd = (Button)gvRelatedItems.Rows[RowIndex].FindControl("btnAdd");
                    TextBox txtQty = (TextBox)gvRelatedItems.Rows[RowIndex].FindControl("txtQty");

                    if (btnAdd.Text != "Added")
                    {
                        AddToCartFromEcomm(hdnCategory.Value, index, 0, "", false, Sites.ToInteger(txtQty.Text), parentItemCode: parentItemCode);

                        btnAdd.Text = "Added";
                        btnAdd.Enabled = false;

                        bindMiniCart();
                        this.Page.ClientScript.RegisterStartupScript(this.GetType(), "ShowMiniPreSellCart", "ShowMiniPreSellCart();", true);
                    }

                    int numVisible = 0;
                    foreach (GridViewRow row in gvRelatedItems.Rows)
                    {
                        if (row.Visible == true)
                        {
                            numVisible += 1;
                        }
                    }

                    if (numVisible == 0)
                    {
                        dialogSellPopup.Visible = false;
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionModule.ExceptionPublish(ex, Sites.ToLong(Session["DomainID"]), Sites.ToLong(Session["UserContactID"]), Sites.ToLong(Session["SiteId"]), Request);
                ShowMessage(ex.ToString(), 1);
            }
        }

        protected void gvRelatedItems_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Button btnAdd = e.Row.FindControl("btnAdd") as Button;
                    Label lblOutOfStock = e.Row.FindControl("lblOutOfStock") as Label;

                    DataRowView drv = e.Row.DataItem as DataRowView;
                    if (drv != null)
                    {
                        TextBox txtQty = (TextBox)e.Row.FindControl("txtQty");
                        int qty = CCommon.ToInteger(txtQty.Text);
                        decimal price = CCommon.ToDecimal(drv.Row["monListPrice"]);
                        decimal total = CCommon.ToDecimal(qty * price);
                        Label lblTotal = (Label)e.Row.FindControl("lblTotal");
                        lblTotal.Text = CCommon.ToString(Session["CurrSymbol"]) + " " + CCommon.ToString(Math.Round(total, 2));

                        TableCell monListPriceCell = e.Row.Cells[5];
                        monListPriceCell.Text = CCommon.ToString(Session["CurrSymbol"]) + " " + CCommon.ToString(Math.Round(price, 2));

                        //hdnProductId.Value = CCommon.ToString(drv.Row["numItemCode"]);
                        //hdnCategory.Value = CCommon.ToString(Session["CurrSymbol"]) + " " + CCommon.ToString(drv.Row["vcCategoryName"]);

                        if (Sites.ToBool(drv.Row["bitHasChildKits"]))
                        {
                            string url = GetProductURL(Sites.ToString(drv.Row["vcCategoryName"]), Sites.ToString(drv.Row["vcItemName"]), Sites.ToString(drv.Row["numItemCode"]));
                            btnAdd.Attributes.Add("onclick", "document.location.href='" + url + "';");
                        }
                        else
                        {
                            int _numOnHand = CCommon.ToInteger(drv.Row["numOnHand"]);
                            if (_numOnHand == 0)
                            {
                                btnAdd.Visible = false;
                                lblOutOfStock.Visible = true;
                            }
                        }

                        Image img = (Image)e.Row.FindControl("imgPath");
                        string imgPath = img.ImageUrl;
                        if (!string.IsNullOrEmpty(imgPath))
                        {
                            imgPath = GetImagePath(CCommon.ToString(imgPath));
                            img.ImageUrl = imgPath;
                        }
                        else
                        {
                            img.ImageUrl = "/Images/DefaultProduct.png";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionModule.ExceptionPublish(ex, Sites.ToLong(Session["DomainID"]), Sites.ToLong(Session["UserContactID"]), Sites.ToLong(Session["SiteId"]), Request);
                ShowMessage(ex.ToString(), 1);
            }
        }

        protected void gvMiniCart_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    DataRowView drv = e.Row.DataItem as DataRowView;
                    if (drv != null)
                    {
                        Image img = (Image)e.Row.FindControl("imgPath");
                        string imgPath = img.ImageUrl;
                        if (!string.IsNullOrEmpty(imgPath))
                        {
                            imgPath = GetImagePath(CCommon.ToString(imgPath));
                            img.ImageUrl = imgPath;
                        }
                        else
                        {
                            img.ImageUrl = "/Images/DefaultProduct.png";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionModule.ExceptionPublish(ex, Sites.ToLong(Session["DomainID"]), Sites.ToLong(Session["UserContactID"]), Sites.ToLong(Session["SiteId"]), Request);
                ShowMessage(ex.ToString(), 1);
            }
        }

        protected void lnkAddToCart_Click1(object sender, EventArgs e)
        {
            try
            {
                BACRM.BusinessLogic.ShioppingCart.Cart objcart;
                objcart = new BACRM.BusinessLogic.ShioppingCart.Cart();
                string[] str = ddlItemAttribute != null ? ddlItemAttribute.SelectedValue.Split('~') : null;
                if (str != null && str.Length == 2)
                {
                    objItems = new CItems();
                    objItems.ItemCode = Convert.ToInt32(str[1]);
                    objItems.DomainID = Sites.ToLong(Session["DomainID"]);
                    objItems.WarehouseID = Sites.ToLong(Session["WareHouseID"]);
                    objItems.SiteID = Sites.ToLong(Session["SiteId"]);
                    if ((!System.Web.HttpContext.Current.User.Identity.IsAuthenticated || CCommon.ToLong(Session["DomainID"]) == 0 || CCommon.ToLong(Session["UserContactID"]) == 0) && Sites.ToInteger(Session["OnePageCheckout"]) == 0)
                    {
                        //GUEST USER
                        objItems.DivisionID = 0;
                    }
                    else
                    {
                        objItems.DivisionID = Sites.ToLong(Session["DivId"]);
                    }
                    DataSet dsItemEcomm = new DataSet();
                    dsItemEcomm = objItems.ItemDetailsForEcomm();
                    if (dsItemEcomm.Tables.Count > 0)
                    {
                        if (dsItemEcomm.Tables[0].Rows.Count > 0)
                        {
                            txtOnHand.Text = Convert.ToString(Sites.ToDecimal(dsItemEcomm.Tables[0].Rows[0]["numOnHand"]));
                            txtBackOrder.Text = Convert.ToString(Sites.ToBool(dsItemEcomm.Tables[0].Rows[0]["bitAllowBackOrder"]));
                            txtItemType.Text = Convert.ToString(Sites.ToString(dsItemEcomm.Tables[0].Rows[0]["charItemType"]));
                        }
                    }
                }
                string[] arr = new string[1];
                if (Sites.ToDouble(txtUnits.Text) > Sites.ToDouble(txtOnHand.Text) && txtItemType.Text.ToUpper() == "P" && hdnIsKit.Value == "0")
                {
                    if (txtBackOrder.Text == "False")
                    {
                        //litMessage.Text = "Sufficient Quantity is not present in the inventory";
                        ShowMessage(GetErrorMessage("ERR003"), 1);
                        return;
                    }
                }

                if (hdnHasChildKits.Value == "1")
                {
                    objItems = new CItems();
                    objItems.ItemCode = Convert.ToInt32(this.ItemId);
                    objItems.DomainID = Sites.ToLong(Session["DomainID"]);

                    System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
                    System.Collections.Generic.List<ChildKitSelection> childKitSelection = serializer.Deserialize<System.Collections.Generic.List<ChildKitSelection>>(hdnChildKitSelection.Value);

                    var childKitSelectedItems = "";

                    if (childKitSelection != null)
                    {
                        foreach (ChildKitSelection objChildKitSelection in childKitSelection)
                        {
                            foreach (string selectedItems in objChildKitSelection.selectedItems)
                            {
                                childKitSelectedItems += String.IsNullOrEmpty(childKitSelectedItems) ? (objChildKitSelection.numItemCode + "-" + selectedItems) : ("," + (objChildKitSelection.numItemCode + "-" + selectedItems));
                            }
                        }
                    }

                    objItems.vcKitChildItems = childKitSelectedItems;

                    if (!objItems.IsValidKitSelection())
                    {
                        ShowMessage(GetErrorMessage("ERR077"), 1);
                        return;
                    }
                }


                AddToCart(false);
                DataSet ds = GetCartItem();
                arr = objcart.GetItemTotal(ds);
                Session["gblItemCount"] = arr[0];
                Session["gblTotalAmount"] = arr[1];
                bindShippingPromotionOffer();
                string CartCount = " <a href=" + Sites.ToString(Session["SitePath"]) + "/cart.aspx>View Cart (" + CCommon.ToString(((DataSet)ds).Tables[0].Rows.Count) + ")</a>";
                //string CartCount = " <a href=" + Sites.ToString(Session["SitePath"]) + "/cart.aspx>View Cart </a> (" + ((DataSet)ds).Tables[0].Rows.Count.ToString() + ")";
                string strFinalWithItemCount = GetErrorMessage("ERR004").Replace("##ItemCount##", CartCount); //Items added to cart successfully .
                if (Convert.ToString(HttpContext.Current.Session["PreUpSell"]) == "True")
                {
                    CItems objItem = new CItems();
                    objItem.DomainID = Sites.ToLong(Session["DomainID"]);
                    objItem.ParentItemCode = Convert.ToInt32(this.ItemId); //9;
                    //objItem.byteMode = 4;
                    objItem.byteMode = 8;
                    HttpCookie cookie = Request.Cookies["Cart"];
                    if (Request.Cookies["Cart"] != null)
                    {
                        objItem.vcCookieId = (cookie["KeyId"] != null) ? cookie["KeyId"] : "";
                    }
                    else
                    {
                        objItem.vcCookieId = "";
                    }
                    objItem.UserCntID = Sites.ToLong(Session["UserContactID"]);
                    DataTable dtItemPrepopup = new DataTable();
                    objItem.SiteID = Sites.ToLong(Session["SiteId"]);
                    dtItemPrepopup = objItem.GetSimilarItem();

                    DataTable dtCart = ds.Tables[0];
                    DataTable duplicateRows = new DataTable();

                    if (dtCart.Rows.Count > 0 && dtItemPrepopup.Rows.Count > 0)
                    {
                        bindMiniCart();
                        var rows = from a in dtCart.AsEnumerable()
                                   join b in dtItemPrepopup.AsEnumerable()
                                   on a["numItemCode"].ToString() equals b["numItemCode"].ToString()
                                   into g
                                   where g.Count() > 0
                                   select a;

                        if (rows.Any())
                            duplicateRows = rows.CopyToDataTable();
                    }
                    if (duplicateRows != null && duplicateRows.Rows.Count <= 0)
                    {
                        if (dtItemPrepopup.Rows.Count > 0)
                        {
                            gvRelatedItems.DataSource = dtItemPrepopup;
                            gvRelatedItems.DataBind();

                            dialogSellPopup.Visible = true;

                            this.Page.ClientScript.RegisterStartupScript(this.GetType(), "ShowMiniPreSellCart", "ShowMiniPreSellCart();", true);
                        }
                        else
                        {
                            this.Page.ClientScript.RegisterStartupScript(this.GetType(), "UpdateMiniCart", "if(document.getElementById('btnRefresh')!=null){ __doPostBack('btnRefresh','OnClick');}", true);

                            dialogSellPopup.Visible = false;
                            if (CCommon.ToBool(AfterAddingItemShowCartPage))
                            {
                                Response.Redirect("/Cart.aspx", false);
                            }
                            else
                            {
                                this.Page.ClientScript.RegisterStartupScript(this.GetType(), "ShowMiniCart", "ShowMiniCart();", true);
                                //ShowMessage(strFinalWithItemCount, 0);
                            }
                        }
                    }
                    else
                    {
                        gvRelatedItems.Columns[8].Visible = true;

                        foreach (DataRow dr in duplicateRows.Rows)
                        {
                            long _numitemCode = CCommon.ToLong(dr["numItemCode"]);

                            if (dtItemPrepopup.Rows.Count > 0)
                            {
                                gvRelatedItems.DataSource = dtItemPrepopup;
                                gvRelatedItems.DataBind();
                                long _gvnumitemCode = 0;
                                dialogSellPopup.Visible = true;

                                this.Page.ClientScript.RegisterStartupScript(this.GetType(), "ShowMiniPreSellCart", "ShowMiniPreSellCart();", true);
                                /*foreach (DataRow drPre in dtItemPrepopup.Rows)
                                {
                                    RelatedItemsPromotion(CCommon.ToInteger(drPre["numItemCode"]), CCommon.ToLong(drPre["numWareHouseItemID"]));
                                }*/

                                for (int i = 0; i < gvRelatedItems.Rows.Count; i++)
                                {
                                    _gvnumitemCode = CCommon.ToLong(gvRelatedItems.Rows[i].Cells[8].Text);
                                    Button btnAdd = (Button)(gvRelatedItems.Rows[i].Cells[6].FindControl("btnAdd"));
                                    if (_gvnumitemCode == _numitemCode)
                                    {
                                        btnAdd.Enabled = false;
                                        btnAdd.Text = "Already Added";
                                    }
                                }
                            }
                        }
                        gvRelatedItems.Columns[8].Visible = false;
                    }
                }
                else
                {
                    this.Page.ClientScript.RegisterStartupScript(this.GetType(), "UpdateMiniCart", "if(document.getElementById('btnRefresh')!=null){ __doPostBack('btnRefresh','OnClick');}", true);
                    //Update Minicart
                    if (CCommon.ToBool(AfterAddingItemShowCartPage))
                    {
                        Response.Redirect("/Cart.aspx", false);
                    }
                    else
                    {
                        this.Page.ClientScript.RegisterStartupScript(this.GetType(), "ShowMiniCart", "ShowMiniCart();", true);
                    }
                }
                //this.Page.ClientScript.RegisterStartupScript(this.GetType(), "ShowMiniCart", "ShowMiniCart();", true);
            }
            catch (Exception ex)
            {
                ExceptionModule.ExceptionPublish(ex, Sites.ToLong(Session["DomainID"]), Sites.ToLong(Session["UserContactID"]), Sites.ToLong(Session["SiteId"]), Request);
                ShowMessage(ex.ToString(), 1);
                return;
            }
            //Response.Redirect(hdnPath.Value);
        }

        protected void ddlOptItem_SelectedIndexChanged1(object sender, EventArgs e)
        {
            try
            {
                LoadOptItemDetails();
                if (ddlOptItem.SelectedIndex > 0 & !string.IsNullOrEmpty(txtHidOptValue.Text))
                {
                    tblOptItemAttr.Controls.Clear();
                    CreateOptAttributes();
                }
                else
                {
                    lnkOptItemAdd.Attributes.Add("onclick", "return AddOption('" + txtOptUnits.ClientID + "','" + txtHidOptValue.ClientID + "','" + txtAddedItems.ClientID + "','" + ddlOptItem.ClientID + "','')");
                }

                bindHtml();
            }
            catch (Exception ex)
            {
                ExceptionModule.ExceptionPublish(ex, Sites.ToLong(Session["DomainID"]), Sites.ToLong(Session["UserContactID"]), Sites.ToLong(Session["SiteId"]), Request);
                ShowMessage(ex.ToString(), 1);
            }
        }

        protected void lnkOptItemAdd_Click1(object sender, EventArgs e)
        {
            try
            {
                objCommon = new CCommon();
                if (Sites.ToDouble(txtOptUnits.Text) > Sites.ToDouble(txtOptOnHand.Text))
                {
                    if (txtOptBackOrder.Text == "False")
                    {
                        ShowMessage(GetErrorMessage("ERR003"), 1);
                        return;
                    }
                }
                if (Sites.ToDecimal(lblOptPrice.Text) == 0)
                {
                    ShowMessage(GetErrorMessage("ERR048"), 1);
                    return;
                }

                dsTemp = GetCartItem(); //(DataSet)Session["Data"];
                DataTable dtItem = default(DataTable);
                dtItem = dsTemp.Tables[0];
                DataRow dr = default(DataRow);
                dr = dtItem.NewRow();
                dr["numOppItemCode"] = Sites.ToInteger(dtItem.Compute("MAX(numOppItemCode)", "")) + 1;
                dr["numItemCode"] = ddlOptItem.SelectedValue;
                dr["numUnitHour"] = txtOptUnits.Text;

                dr["numUOM"] = hfUOM.Value;
                dr["vcUOMName"] = lblUnit.Text;
                dr["decUOMConversionFactor"] = hfConversionFactor.Value;
                dr["bitFreeShipping"] = txtFreeShipping.Text;
                if (txtFreeShipping.Text != "True")
                {
                    dr["numWeight"] = txtWeight.Text;
                    dr["numHeight"] = txtHeight.Text;
                    dr["numWidth"] = txtWidth.Text;
                    dr["numLength"] = txtLength.Text;
                }

                if (Sites.ToLong(txtOptWareHouseItemID.Text) > 0)
                {
                    dr["numWarehouseItmsID"] = txtOptWareHouseItemID.Text;
                }

                PriceBookRule objPbook = new PriceBookRule();
                objPbook.ItemID = Sites.ToLong(ddlOptItem.SelectedValue);
                objPbook.QntyofItems = Sites.ToInteger(Sites.ToInteger(dr["numUnitHour"].ToString()) * Sites.ToDecimal(hfConversionFactor.Value));
                objPbook.DomainID = Sites.ToLong(Session["DomainID"]);
                objPbook.DivisionID = Sites.ToLong(Session["DivId"]);
                objPbook.WarehouseID = Sites.ToLong(Session["WareHouseID"]);
                objPbook.SiteID = Sites.ToLong(HttpContext.Current.Session["SiteId"]);
                DataTable dtCalPrice = default(DataTable);
                dtCalPrice = objPbook.GetPriceBasedonPriceBook();

                decimal ExchangeRate;
                ExchangeRate = Sites.ToDecimal(Session["ExchangeRate"].ToString()) == 0 ? 1 : Sites.ToDecimal(Session["ExchangeRate"].ToString());

                if (dtCalPrice.Rows.Count > 0)
                    dr["monPrice"] = Sites.ToDouble(Sites.ToDecimal(dtCalPrice.Rows[0]["ListPrice"]) * Sites.ToDecimal(hfConversionFactor.Value) / ExchangeRate); //Sites.ToDouble(lblOptPrice.Text) + 
                else
                    dr["monPrice"] = lblOptPrice.Text;

                dr["bitDiscountType"] = 0;
                dr["fltDiscount"] = 0;
                dr["vcCoupon"] = hdnPromotionCoupon.Value;
                dr["monTotAmtBefDiscount"] = Sites.ToInteger(dr["numUnitHour"]) * Sites.ToDouble(dr["monPrice"]);
                dr["monTotAmount"] = dr["monTotAmtBefDiscount"];
                dr["vcItemDesc"] = lblOptDesc.Text;
                dr["numWarehouseID"] = Sites.ToLong(Session["WareHouseID"]);
                dr["vcItemName"] = ddlOptItem.SelectedItem.Text;

                dr["vcItemType"] = "Options & Accessories";
                dr["vcAttributes"] = objCommon.GetAttributesForWarehouseItem(Sites.ToLong(dr["numWarehouseItmsID"]), false);
                dr["ItemURL"] = hdnPath.Value;
                dtItem.Rows.Add(dr);
                dsTemp.AcceptChanges();
                //ddlOptItem.SelectedIndex = 0;
                tblOptItemAttr.Controls.Clear();
                //lblOptDesc.Text = "";
                //txtOptUnits.Text = "";
                InsertToCart(dr);
                dsTemp = GetCartItem();  //(DataSet)Session["Data"];
                DataTable dtTable = default(DataTable);
                dtTable = dsTemp.Tables[0];
                txtAddedItems.Text = "";
                foreach (DataRow dr1 in dtTable.Rows)
                {
                    txtAddedItems.Text = txtAddedItems.Text + dr1["numWarehouseItmsID"].ToString() + ",";
                }
                txtAddedItems.Text = txtAddedItems.Text.Trim(',');

                string CartCount = " <a href=" + Sites.ToString(Session["SitePath"]) + "/cart.aspx>(" + ((DataSet)dsTemp).Tables[0].Rows.Count.ToString() + ")</a>";
                this.Page.ClientScript.RegisterStartupScript(this.GetType(), "ShowMiniCart", "ShowMiniCart();", true);
                //string strFinalWithItemCount = GetErrorMessage("ERR004").Replace("##ItemCount##", CartCount);
                //ShowMessage(strFinalWithItemCount, 0);
            }
            catch (Exception ex)
            {
                ExceptionModule.ExceptionPublish(ex, Sites.ToLong(Session["DomainID"]), Sites.ToLong(Session["UserContactID"]), Sites.ToLong(Session["SiteId"]), Request);
                ShowMessage(ex.ToString(), 1);
            }
            //Response.Redirect(hdnPath.Value);
        }

        #endregion

        #region Methods

        void bindShippingPromotionOffer()
        {

            BACRM.BusinessLogic.ShioppingCart.Cart _cart = new BACRM.BusinessLogic.ShioppingCart.Cart();
            string uomid = Convert.ToString(hfUOM.Value);
            DataTable dsPromotion = new DataTable();
            _cart.DomainID = Sites.ToLong(Session["DomainID"]);
            string[] str = ddlItemAttribute != null ? ddlItemAttribute.SelectedValue.Split('~') : null;
            if (str != null && str.Length == 2)
            {
                _cart.ItemCode = Convert.ToInt32(str[1]);
            }
            else
            {
                _cart.ItemCode = (int)this.ItemId;
            }
            _cart.UserCntID = CCommon.ToLong(Session["UserContactID"]);
            HttpCookie cookie = Request.Cookies["Cart"];
            if (Request.Cookies["Cart"] != null)
            {
                _cart.CookieId = (cookie["KeyId"] != null) ? cookie["KeyId"] : "";
            }
            else
            {
                _cart.CookieId = "";
            }
            dsPromotion = _cart.GetCartItemPromotionDetail(CCommon.ToLong(uomid), Convert.ToString(this.ItemId), CCommon.ToLong(Session["DefCountry"]));
            if (dsPromotion.Rows.Count > 0)
            {
                PromotionForProduct = Sites.ToString(dsPromotion.Rows[0]["vcPromotionDescription"]);
                ShippingForOrder = Sites.ToString(dsPromotion.Rows[0]["vcShippingDescription"]);
                qualifiedshipping = Sites.ToString(dsPromotion.Rows[0]["vcQualifiedShipping"]);
                CouponCode = Sites.ToString(dsPromotion.Rows[0]["vcCouponCode"]);
                bitRequireCouponCode = Sites.ToString(dsPromotion.Rows[0]["bitRequireCouponCode"]);
                tintOfferTriggerValueType = Sites.ToInteger(dsPromotion.Rows[0]["tintOfferTriggerValueType"]);
                fltOfferTriggerValue = Sites.ToDouble(dsPromotion.Rows[0]["fltOfferTriggerValue"]);
            }
        }

        void bindCombineAssemblies()
        {
            if (ddlItemAttribute != null)
            {
                objItems = new CItems();

                DataSet ds = new DataSet();
                objItems.ItemCode = CCommon.ToInteger(this.ItemId);
                objItems.DomainID = Sites.ToLong(Session["DomainID"]);
                ds = objItems.GetProfileItems();
                if (ds.Tables.Count > 0)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        ddlItemAttribute.Visible = true;
                        ddlItemAttribute.DataSource = ds.Tables[0];
                        ddlItemAttribute.DataTextField = "vcData";
                        ddlItemAttribute.DataValueField = "numOppAccAttrID";
                        ddlItemAttribute.DataBind();
                    }
                    else
                    {
                        ddlItemAttribute.Visible = false;
                    }
                }
                else
                {
                    ddlItemAttribute.Visible = false;
                }
            }
        }

        void bindHtml()
        {
            try
            {
                objItems = new CItems();

                if (this.ItemId > 0)
                {
                    string strUI = GetUserControlTemplate("Product.htm");

                    DataTable dtItemDetails = default(DataTable);
                    DataTable dtItemColumn = default(DataTable);

                    DataSet ds = default(DataSet);
                    DataTable dtTable = default(DataTable);
                    objItems.ItemCode = Sites.ToInteger(this.ItemId);
                    objItems.WarehouseID = Sites.ToLong(Session["WareHouseID"]);
                    objItems.DomainID = Sites.ToLong(Session["DomainID"]);
                    objItems.SiteID = Sites.ToLong(Session["SiteId"]);
                    HttpCookie cookie = Request.Cookies["Cart"];
                    if (Request.Cookies["Cart"] != null)
                    {
                        objItems.vcCookieId = (cookie["KeyId"] != null) ? cookie["KeyId"] : "";
                    }
                    else
                    {
                        objItems.vcCookieId = "";
                    }
                    objItems.UserCntID = Sites.ToLong(Session["UserContactID"]);
                    if ((!System.Web.HttpContext.Current.User.Identity.IsAuthenticated || CCommon.ToLong(Session["DomainID"]) == 0 || CCommon.ToLong(Session["UserContactID"]) == 0) && Sites.ToInteger(Session["OnePageCheckout"]) == 0)
                    {
                        //GUEST USER
                        objItems.DivisionID = 0;
                    }
                    else
                    {
                        objItems.DivisionID = Sites.ToLong(Session["DivId"]);
                    }
                    DataSet dsAvailabilityItemGrid = new DataSet();
                    dsAvailabilityItemGrid = objItems.AvailabilityItemStock();
                    ds = objItems.ItemDetailsForEcomm();

                    if (ds.Tables.Count == 2)
                    {
                        dtItemDetails = ds.Tables[0];
                        dtItemColumn = ds.Tables[1];

                        if (dtItemDetails.Rows.Count > 0)
                        {
                            if (CCommon.ToString(dtItemDetails.Rows[0]["vcPageTitle"]) != "")
                            {
                                this.Page.Title = CCommon.ToString(dtItemDetails.Rows[0]["vcPageTitle"]);
                            }
                            else
                            {
                                this.Page.Title = Sites.ToString(dtItemDetails.Rows[0]["vcItemName"]).Trim();
                            }

                            hdnHasChildKits.Value = Sites.ToInteger(dtItemDetails.Rows[0]["bitHasChildKits"]).ToString();

                            if (Sites.ToString(hdnHasChildKits.Value) == "1")
                            {
                                strUI = strUI.Replace("##ConfigureKitLink##", "<a onclick=\"javascript: document.location.href = '" + ResolveUrl("~/configurator.aspx") + "?ItemID=" + Sites.ToString(this.ItemId) + "&CategoryID=" + lngCategoryID.ToString() + "';\" class=\"configurekitlink\">Configure</a>");
                                strUI = strUI.Replace("##AddToCartLink##", "");
                            }
                            else
                            {
                                strUI = strUI.Replace("##ConfigureKitLink##", "");
                                strUI = strUI.Replace("##AddToCartLink##", CCommon.RenderControl(lnkAddToCart));
                            }

                            hfConversionFactor.Value = dtItemDetails.Rows[0]["UOMConversionFactor"].ToString();
                            hfUOM.Value = dtItemDetails.Rows[0]["numUOM"].ToString();
                            txtMatrix.Text = CCommon.ToBool(dtItemDetails.Rows[0]["bitMatrix"]).ToString();
                            double dProductPrice = 0, dOfferPrice = 0, dMSRPPrice = 0;

                            if (!Page.IsPostBack)
                            {
                                if (!(dtItemDetails.Rows[0]["monListPrice"] == System.DBNull.Value))
                                {
                                    decimal ExchangeRate;
                                    ExchangeRate = Sites.ToDecimal(Session["ExchangeRate"].ToString()) == 0 ? 1 : Sites.ToDecimal(Session["ExchangeRate"].ToString());

                                    dProductPrice = Sites.ToDouble(Sites.ToDecimal(dtItemDetails.Rows[0]["monListPrice"]) / ExchangeRate);
                                    dMSRPPrice = Sites.ToDouble(Sites.ToDecimal(dtItemDetails.Rows[0]["monMSRP"]) / ExchangeRate);

                                    lblProductPrice.Text = string.Format("{0:#,##0.00###}", dProductPrice);
                                    lblMSRPPrice.Text = string.Format("{0:#,##0.00###}", dMSRPPrice);
                                    hdnItemPrice.Value = dProductPrice.ToString();
                                }

                                /*Apply pricebook rule here */
                                hdPriceBookId.Value = "";
                                if (Sites.ToBool(ApplyPriceBookPrice) == true && Sites.ToBool(Session["HidePrice"]) == false)
                                {
                                    PriceBookRule objPbook = new PriceBookRule();
                                    objPbook.ItemID = this.ItemId;
                                    objPbook.QntyofItems = Sites.ToInteger(1 * Sites.ToDecimal(hfConversionFactor.Value));
                                    objPbook.DomainID = Sites.ToLong(Session["DomainID"]);
                                    objPbook.DivisionID = Sites.ToLong(Session["DivId"]);
                                    objPbook.WarehouseID = Sites.ToLong(Session["WareHouseID"]);
                                    objPbook.SiteID = Sites.ToLong(HttpContext.Current.Session["SiteId"]);
                                    DataTable dtCalPrice = default(DataTable);
                                    dtCalPrice = objPbook.GetPriceBasedonPriceBook();


                                    if (dtCalPrice.Rows.Count > 0)
                                    {
                                        decimal ExchangeRate;
                                        ExchangeRate = Sites.ToDecimal(Session["ExchangeRate"].ToString()) == 0 ? 1 : Sites.ToDecimal(Session["ExchangeRate"].ToString());
                                        dOfferPrice = Sites.ToDouble(Sites.ToDecimal(dtCalPrice.Rows[0]["ListPrice"]) * Sites.ToDecimal(hfConversionFactor.Value) / ExchangeRate);
                                        lblOfferPrice.Text = string.Format("{0:#,##0.00###}", dOfferPrice);

                                        if (dtCalPrice.Rows[0]["tintPricingMethod"].ToString() == "1")
                                            hdPriceBookId.Value = dtCalPrice.Rows[0]["numPricRuleID"].ToString();
                                    }

                                    if (dOfferPrice > 0 && (Sites.ToDouble(lblProductPrice.Text) != dOfferPrice))
                                    {
                                        lblOfferPrice.Text = string.Format("{0:#,##0.00###}", dOfferPrice);
                                        spanProductPrice.Attributes.Add("class", "prod_strick");
                                        spanMSRPPrice.Attributes.Add("class", "prod_strick");
                                        spanOfferPrice.Attributes.Remove("class");
                                    }
                                    else
                                    {
                                        spanOfferPrice.Visible = false;
                                        spanProductPrice.Attributes.Remove("class");
                                        spanMSRPPrice.Attributes.Remove("class");
                                    }
                                }
                                else
                                {
                                    spanOfferPrice.Visible = false;
                                    spanProductPrice.Attributes.Remove("class");
                                    spanMSRPPrice.Attributes.Remove("class");
                                }

                                txtItemType.Text = dtItemDetails.Rows[0]["charItemType"].ToString();
                                hdnIsKit.Value = Sites.ToInteger(dtItemDetails.Rows[0]["bitKitParent"]).ToString();
                                hdnCalAmtBasedonDepItems.Value = Sites.ToInteger(dtItemDetails.Rows[0]["bitCalAmtBasedonDepItems"]).ToString();
                                txtFreeShipping.Text = dtItemDetails.Rows[0]["bitFreeShipping"].ToString() == "1" ? "True" : "False";
                                lblDescription.Text = CCommon.ToString(dtItemDetails.Rows[0]["txtItemDesc"]);
                                if (dtItemDetails.Rows[0]["charItemType"].ToString() == "P" | dtItemDetails.Rows[0]["charItemType"].ToString() == "A")
                                {
                                    if (bool.Parse(dtItemDetails.Rows[0]["bitSerialized"].ToString()) == true)
                                    {
                                        txtHidValue.Text = "True";
                                        txtWareHouseItemID.Text = dtItemDetails.Rows[0]["numWareHouseItemID"].ToString();
                                    }
                                    else
                                    {
                                        txtHidValue.Text = "False";
                                        if (dtItemDetails.Rows.Count >= 1)
                                        {
                                            txtWareHouseItemID.Text = dtItemDetails.Rows[0]["numWareHouseItemID"].ToString();
                                        }
                                    }

                                    txtWeight.Text = dtItemDetails.Rows[0]["fltWeight"].ToString();
                                    txtWidth.Text = dtItemDetails.Rows[0]["fltWidth"].ToString();
                                    txtLength.Text = dtItemDetails.Rows[0]["fltLength"].ToString();
                                    txtHeight.Text = dtItemDetails.Rows[0]["fltHeight"].ToString();
                                    txtOnHand.Text = dtItemDetails.Rows[0]["numOnHand"].ToString();
                                    txtBackOrder.Text = dtItemDetails.Rows[0]["bitAllowBackOrder"].ToString();
                                    lnkShipping.Attributes.Add("onclick", "return EstimateShipping(" + CCommon.ToString(this.ItemId) + "," + lngCategoryID.ToString() + ");");
                                }
                                else if (dtItemDetails.Rows[0]["charItemType"].ToString() == "S")
                                {
                                    txtOnHand.Text = "10";// When Service Item make defult quantity to 10 so it will not throw error of insufficient Quantity Error When added to cart.
                                    txtHidValue.Text = "False";
                                }
                                else
                                    txtHidValue.Text = "False";
                            }


                            strUI = strUI.Replace("##ItemCode##", Sites.ToString(dtItemDetails.Rows[0]["numItemCode"]));
                            strUI = strUI.Replace("##UPCCode##", Sites.ToString(dtItemDetails.Rows[0]["numBarCodeId"]));
                            strUI = strUI.Replace("##Item##", Sites.ToString(dtItemDetails.Rows[0]["vcItemName"]));
                            if (bitRequireCouponCode == "1")
                            {
                                strUI = strUI.Replace("##CouponCode##", (txtCouponCode != null ? CCommon.RenderControl(txtCouponCode) : "") + (btnApplyCoupon != null ? CCommon.RenderControl(btnApplyCoupon) : "") + (ltrlcouponcode != null ? CCommon.RenderControl(ltrlcouponcode) : null));
                                txtCouponCode.Visible = true;
                                btnApplyCoupon.Visible = true;
                                ltrlcouponcode.Visible = true;
                            }
                            else if (bitRequireCouponCode == "APPLIED")
                            {
                                strUI = strUI.Replace("##CouponCode##", (txtCouponCode != null ? CCommon.RenderControl(txtCouponCode) : "") + (btnApplyCoupon != null ? CCommon.RenderControl(btnApplyCoupon) : "") + (ltrlcouponcode != null ? CCommon.RenderControl(ltrlcouponcode) : null));
                                txtCouponCode.Visible = false;
                                btnApplyCoupon.Visible = true;
                                ltrlcouponcode.Visible = true;
                                btnApplyCoupon.Text = "Remove Coupon";
                                hdnPromotionCoupon.Value = CouponCode;
                                ltrlcouponcode.Text = "<span class=\"success-label couponspan\">" + CouponCode + " Already Applied</span>";
                            }
                            else
                            {
                                strUI = strUI.Replace("##CouponCode##", "");
                                if (txtCouponCode != null)
                                {
                                    txtCouponCode.Visible = false;
                                    btnApplyCoupon.Visible = false;
                                    ltrlcouponcode.Visible = false;
                                }
                            }
                            lblProductName.Text = Sites.ToString(dtItemDetails.Rows[0]["vcItemName"]);
                            if (Sites.ToString(PromotionForProduct) != "")
                            {
                                strUI = strUI.Replace("##PromotionDesc##", "<img height=\"35px\" src=\"" + Sites.ToString(Session["SitePath"]) + "/images/Admin-Price-Control.png\" />" + Sites.ToString(PromotionForProduct));
                            }
                            else
                            {
                                strUI = strUI.Replace("##PromotionDesc##", "");
                            }
                            if (Sites.ToString(ShippingForOrder) != "")
                            {
                                strUI = strUI.Replace("##ShippingDesc##", "<img height=\"35px\"  src=\"" + Sites.ToString(Session["SitePath"]) + "/images/TruckGreen.png\" />" + Sites.ToString(ShippingForOrder));
                            }
                            else
                            {
                                if (Sites.ToString(qualifiedshipping) != "")
                                {
                                    strUI = strUI.Replace("##ShippingDesc##", "<img height=\"35px\"  src=\"" + Sites.ToString(Session["SitePath"]) + "/images/TruckGreen.png\" />" + Sites.ToString(qualifiedshipping));
                                }
                                else
                                {
                                    strUI = strUI.Replace("##ShippingDesc##", "");
                                }
                            }
                            strUI = strUI.Replace("##ItemUOM##", Sites.ToString(dtItemDetails.Rows[0]["vcUOMName"]) == "" ? "Units" : Sites.ToString(dtItemDetails.Rows[0]["vcUOMName"]));

                            strUI = strUI.Replace("##AddToListLink##", CCommon.RenderControl(lnkAddToList));
                            if (Sites.ToBool(Session["HideAddtoCartBeforeLogin"]) == true && Sites.ToLong(Session["UserContactID"]) == 0)
                            {
                                strUI = strUI.Replace("##AddToCartLink##", "");
                                strUI = strUI.Replace("##Quantity##", "");
                                strUI = strUI.Replace("##HidePriceBeforeLogin##", "hideAddtoCart");
                            }
                            else
                            {
                                if (Sites.ToBool(dtItemDetails.Rows[0]["bitInStock"]))
                                {
                                    strUI = strUI.Replace("##AddToCartLink##", CCommon.RenderControl(lnkAddToCart));
                                }
                                else
                                {
                                    strUI = strUI.Replace("##AddToCartLink##", CCommon.RenderControl(lblCallToOrder));
                                }

                                strUI = strUI.Replace("##Quantity##", (btnDecrease != null ? CCommon.RenderControl(btnDecrease) : "") + " " + CCommon.RenderControl(txtUnits) + " " + (btnIncrease != null ? CCommon.RenderControl(btnIncrease) : "") + " " + (ddlItemAttribute != null ? CCommon.RenderControl(ddlItemAttribute) : ""));
                            }
                            if (Sites.ToBool(Session["HidePrice"]) == false)
                            {

                                strUI = strUI.Replace("##ItemPrice##", CCommon.RenderControl(spanProductPrice));
                                strUI = strUI.Replace("##MSRP##", CCommon.RenderControl(spanMSRPPrice));
                                strUI = strUI.Replace("##OfferPrice##", CCommon.RenderControl(spanOfferPrice));
                            }
                            else
                            {
                                strUI = strUI.Replace("##ItemPrice##", "NA");
                                strUI = strUI.Replace("##MSRP##", "NA");
                                strUI = strUI.Replace("##OfferPrice##", "NA");
                            }
                            strUI = strUI.Replace("##Width##", dtItemDetails.Rows[0]["fltWidth"].ToString());
                            strUI = strUI.Replace("##Height##", dtItemDetails.Rows[0]["fltHeight"].ToString());
                            strUI = strUI.Replace("##Weight##", dtItemDetails.Rows[0]["fltWeight"].ToString());
                            strUI = strUI.Replace("##Length##", dtItemDetails.Rows[0]["fltLength"].ToString());
                            strUI = strUI.Replace("##Shipping##", dtItemDetails.Rows[0]["Shipping"].ToString());


                            string strPatternProductImageAvailable = "{##ProductImage##}(?<Content>([\\s\\S]*?)){/##ProductImage##}";
                            string strPatternProductImageNotAvailable = "{##ProductImageNotAvailable##}(?<Content>([\\s\\S]*?)){/##ProductImageNotAvailable##}";

                            string strPatternAvailableItemGridAvailable = "{##AvailabilityGrid##}(?<Content>([\\s\\S]*?)){/##AvailabilityGrid##}";
                            string AGpattern = "<##FOR_AVAILABLEGRID##>(?<Content>([\\s\\S]*?))<##ENDFOR##>";
                            string AvailableGrid_Template = "";
                            string tempAvailableGrid_Template = "";
                            StringBuilder sbAvailableGrid = new StringBuilder();
                            if (dsAvailabilityItemGrid.Tables.Count > 0)
                            {
                                if (dsAvailabilityItemGrid.Tables[0].Rows.Count > 0)
                                {
                                    if (Regex.IsMatch(strUI, AGpattern))
                                    {
                                        MatchCollection matches = Regex.Matches(strUI, AGpattern);
                                        AvailableGrid_Template = matches[0].Groups["Content"].Value;
                                    }
                                    foreach (DataRow dr in dsAvailabilityItemGrid.Tables[0].Rows)
                                    {
                                        tempAvailableGrid_Template = AvailableGrid_Template;
                                        tempAvailableGrid_Template = tempAvailableGrid_Template.Replace("##Avail_Location##", CCommon.ToString(dr["vcWareHouse"]));
                                        tempAvailableGrid_Template = tempAvailableGrid_Template.Replace("##Avail_QtyOnHand##", CCommon.ToString(dr["numOnHand"]));
                                        tempAvailableGrid_Template = tempAvailableGrid_Template.Replace("##Avail_QtyOnOrder##", CCommon.ToString(dr["numOnOrder"]));
                                        tempAvailableGrid_Template = tempAvailableGrid_Template.Replace("##Avail_OrderDate##", CCommon.ToString(dr["bintCreatedDate"]));
                                        tempAvailableGrid_Template = tempAvailableGrid_Template.Replace("##Avail_LeadTime##", CCommon.ToString(dr["LeadDays"]));
                                        tempAvailableGrid_Template = tempAvailableGrid_Template.Replace("##Avail_ExpectedDelivery##", CCommon.ToString(dr["dtDeliveryDate"]));
                                        sbAvailableGrid.Append(tempAvailableGrid_Template);
                                    }
                                    strUI = Regex.Replace(strUI, AGpattern, sbAvailableGrid.ToString());
                                }
                                strUI = Sites.ReplaceMatchedPatternWithContent(strUI, strPatternAvailableItemGridAvailable);
                            }
                            else
                            {
                                strUI = Sites.RemoveMatchedPattern(strUI, strPatternAvailableItemGridAvailable);
                            }



                            if (!(dtItemDetails.Rows[0]["vcImages"] == System.DBNull.Value))
                            {
                                if (!string.IsNullOrEmpty(dtItemDetails.Rows[0]["vcImages"].ToString()))
                                {

                                    XmlDocument xmlItemImages = new XmlDocument();
                                    xmlItemImages.LoadXml(dtItemDetails.Rows[0]["vcImages"].ToString());

                                    XmlNodeList ItemImages = xmlItemImages.SelectNodes("Images/ItemImages");
                                    StringBuilder sb = new StringBuilder();

                                    string pattern = "<##FOR##>(?<Content>([\\s\\S]*?))<##ENDFOR##>";

                                    string thumb_template = "";

                                    string template = "";
                                    if (Regex.IsMatch(strUI, pattern))
                                    {
                                        MatchCollection matches = Regex.Matches(strUI, pattern);
                                        thumb_template = matches[0].Groups["Content"].Value;


                                    }
                                    foreach (XmlNode Image in ItemImages)
                                    {

                                        string strTemp = CCommon.ToString(Image.Attributes[4]);
                                        if (CCommon.ToString(Image.Attributes["bitDefault"].Value) == "1")
                                        {
                                            strUI = strUI.Replace("##LargeImagePath##", GetImagePath(CCommon.ToString(Image.Attributes["vcPathForImage"].Value)));
                                            strUI = strUI.Replace("##ThumbImagePath##", GetImagePath(CCommon.ToString(Image.Attributes["vcPathForTImage"].Value)));
                                            template = thumb_template;
                                            template = template.Replace("##ThumbImagePath##", GetImagePath(CCommon.ToString(Image.Attributes["vcPathForTImage"].Value)));
                                            template = template.Replace("##LargeImagePath##", GetImagePath(CCommon.ToString(Image.Attributes["vcPathForImage"].Value)));
                                            template = template.Replace("##ZoomActive##", "zoomThumbActive");

                                            sb.Append(template);
                                        }
                                        else
                                        {
                                            // Other Images 
                                            template = thumb_template;
                                            template = template.Replace("##ThumbImagePath##", GetImagePath(CCommon.ToString(Image.Attributes["vcPathForTImage"].Value)));
                                            template = template.Replace("##LargeImagePath##", GetImagePath(CCommon.ToString(Image.Attributes["vcPathForImage"].Value)));
                                            template = template.Replace("##ZoomActive##", "");

                                            sb.Append(template);
                                        }
                                    }
                                    strUI = Regex.Replace(strUI, pattern, sb.ToString());
                                }
                                strUI = Sites.RemoveMatchedPattern(strUI, strPatternProductImageNotAvailable);
                                strUI = Sites.ReplaceMatchedPatternWithContent(strUI, strPatternProductImageAvailable);
                            }
                            else
                            {
                                strUI = Sites.RemoveMatchedPattern(strUI, strPatternProductImageAvailable);
                                strUI = Sites.ReplaceMatchedPatternWithContent(strUI, strPatternProductImageNotAvailable);
                            }

                            string strPatternProductDocument = "{##ProductDocument##}(?<Content>([\\s\\S]*?)){/##ProductDocument##}";
                            if (!(dtItemDetails.Rows[0]["vcDocuments"] == System.DBNull.Value))
                            {
                                string document_template = "";
                                string patternDocument = "<##FOR_Document##>(?<Content>([\\s\\S]*?))<##ENDFOR##>";

                                //This is for showing document .
                                if (Regex.IsMatch(strUI, patternDocument))
                                {
                                    MatchCollection matches = Regex.Matches(strUI, patternDocument);
                                    document_template = matches[0].Groups["Content"].Value;
                                }
                                if (document_template != "")
                                {

                                    if (!string.IsNullOrEmpty(CCommon.ToString(dtItemDetails.Rows[0]["vcDocuments"])))
                                    {

                                        XmlDocument xmlItemDocuments = new XmlDocument();
                                        xmlItemDocuments.LoadXml(CCommon.ToString(dtItemDetails.Rows[0]["vcDocuments"]));

                                        XmlNodeList ItemDocuments = xmlItemDocuments.SelectNodes("Documents/ItemImages");
                                        StringBuilder sb = new StringBuilder();
                                        string templateDocument = "";

                                        foreach (XmlNode Document in ItemDocuments)
                                        {
                                            templateDocument = document_template;
                                            templateDocument = templateDocument.Replace("##DocumentName##", CCommon.ToString(Document.Attributes["vcPathForImage"].Value));
                                            templateDocument = templateDocument.Replace("##DocumentPath##", GetImagePath(CCommon.ToString(Document.Attributes["vcPathForImage"].Value)));
                                            templateDocument = templateDocument.Replace("##DocumentImage##", "/images/" + CCommon.GetDocumentImage(CCommon.ToString(Document.Attributes["vcPathForImage"].Value)));// + CCommon.ToString(Document.Attributes["vcPathForImage"].Value));
                                            sb.Append(templateDocument);
                                        }
                                        strUI = Regex.Replace(strUI, patternDocument, sb.ToString());
                                    }
                                }
                                strUI = Sites.ReplaceMatchedPatternWithContent(strUI, strPatternProductDocument);
                            }
                            else
                            {
                                strUI = Sites.RemoveMatchedPattern(strUI, strPatternProductDocument);
                            }
                            //strUI = strUI.Replace("##ItemImagePath##", ItemImagePath);
                            strUI = strUI.Replace("##CurrentStock##", string.Format("{0:#,##0.####}", Sites.ToDouble(dtItemDetails.Rows[0]["numOnHand"])));
                            strUI = strUI.Replace("##OnOrder##", dtItemDetails.Rows[0]["numOnOrder"].ToString());
                            strUI = strUI.Replace("##ModelID##", dtItemDetails.Rows[0]["vcModelID"].ToString());
                            strUI = strUI.Replace("##Manufacturer##", dtItemDetails.Rows[0]["vcManufacturer"].ToString());
                            strUI = strUI.Replace("##SKU##", dtItemDetails.Rows[0]["vcSKU"].ToString());
                            strUI = strUI.Replace("##Availability##", dtItemDetails.Rows[0]["InStock"].ToString());
                            strUI = strUI.Replace("##ShortDescription##", dtItemDetails.Rows[0]["txtItemDesc"].ToString());

                            objItems.byteMode = 2;
                            objItems.ItemCode = Sites.ToInteger(CCommon.ToString(this.ItemId));
                            DataTable dt = new DataTable();
                            dt = objItems.ViewItemExtendedDesc();
                            if (dt.Rows.Count > 0)
                            {
                                strUI = strUI.Replace("##LongDescription##", Convert.ToString(dt.Rows[0]["txtDesc"]));
                                strUI = strUI.Replace("##ShortHtmlDescription##", Convert.ToString(dt.Rows[0]["txtShortDesc"]));
                            }
                            else
                            {
                                strUI = strUI.Replace("##LongDescription##", "");
                                strUI = strUI.Replace("##ShortHtmlDescription##", "");
                            }



                            if (Sites.ToBool(ApplyPriceBookPrice) == true && hdPriceBookId.Value.Length > 0)
                            {
                                createPriceBookRuleTable(ref tblPriceBookRule);
                            }

                            strUI = strUI.Replace("##PriceBookRuleTable##", CCommon.RenderControl(tblPriceBookRule));

                            if (strUI.Contains("##PriceLevelTable##"))
                            {
                                string priceLevels = objItems.GetPriceLevelEcommHtml();
                                strUI = strUI.Replace("##PriceLevelTable##", priceLevels);
                            }

                            strUI = strUI.Replace("##ItemAttributes##", CCommon.RenderControl(tblItemAttr));
                            strUI = strUI.Replace("##EstimateShippingChargeLink##", "return EstimateShipping(" + CCommon.ToString(this.ItemId) + "," + lngCategoryID.ToString() + ");");
                            strUI = strUI.Replace("##ChildKits##", "<div id='divChildKits' class='row'></div>");

                            if (!CCommon.ToBool(Session["IsShowPromoDetailsLink"]) && CCommon.ToInteger(dtItemDetails.Rows[0]["numTotalPromotions"]) == 1)
                            {
                                strUI = strUI.Replace("##PromotionDescription##", "<img src='/images/item-promotion.png' class='imgitempromo' />" + " " + CCommon.ToString(dtItemDetails.Rows[0]["vcPromoDesc"]));
                            }
                            else if (CCommon.ToInteger(dtItemDetails.Rows[0]["numTotalPromotions"]) > 0)
                            {
                                strUI = strUI.Replace("##PromotionDescription##", "<a href='javascript:LoadItemPromoPopup(" + Sites.ToString(dtItemDetails.Rows[0]["numItemCode"]) + ");' class='linkitempromo'><img src='/images/item-promotion.png' class='imgitempromo' /> Click for details</a>");
                            }
                            else
                            {
                                strUI = strUI.Replace("##PromotionDescription##", "");
                            }

                            if (dtItemColumn.Rows.Count > 0)
                            {
                                int i = 0;

                                for (i = 0; i <= dtItemColumn.Rows.Count - 1; i++)
                                {
                                    string FieldName = string.Format("{0}_{1}", dtItemColumn.Rows[i]["vcFormFieldName"].ToString(), dtItemColumn.Rows[i]["vcFieldType"].ToString());
                                    string attributeName = string.Format("{0}_{1}", dtItemColumn.Rows[i]["vcFormFieldName"].ToString(), dtItemColumn.Rows[i]["numFormFieldId"].ToString());
                                    strUI = strUI.Replace("##" + FieldName + "##", dtItemColumn.Rows[i]["vcItemValue"].ToString());
                                    strUI = strUI.Replace("##" + attributeName + "##", dtItemColumn.Rows[i]["vcItemValue"].ToString());
                                }
                            }
                        }
                        else
                        {
                            throw new HttpException(404, "Item not found");
                        }

                        if (!Page.IsPostBack)
                        {
                            objItems.ItemCode = Sites.ToInteger(CCommon.ToString(this.ItemId));
                            dtTable = objItems.GetOptAccWareHouses();
                            if (dtTable.Rows.Count > 0)
                            {
                                ddlOptItem.DataSource = dtTable;
                                ddlOptItem.DataTextField = "vcItemName";
                                ddlOptItem.DataValueField = "numItemCode";
                                ddlOptItem.DataBind();
                                ddlOptItem.Items.Insert(0, "--Select One--");
                                ddlOptItem.Items.FindByText("--Select One--").Value = "0";
                            }
                        }
                        string OptionsPattern;
                        OptionsPattern = "{##OptionsSection##}(?<Content>([\\s\\S]*?)){/##OptionsSection##}";
                        if (ddlOptItem.Items.Count > 0)
                        {
                            //Replace disply template ##Tags## with content value
                            strUI = Sites.ReplaceMatchedPatternWithContent(strUI, OptionsPattern);
                        }
                        else
                        {
                            //Hide Template
                            strUI = Sites.RemoveMatchedPattern(strUI, OptionsPattern);
                        }

                        strUI = strUI.Replace("##Opt_ItemCode##", CCommon.ToString(ddlOptItem.SelectedValue));
                        strUI = strUI.Replace("##Opt_Item##", CCommon.RenderControl(ddlOptItem));
                        strUI = strUI.Replace("##Opt_ItemPath##", imgOptItem.ImageUrl);
                        strUI = strUI.Replace("##Opt_Stock##", CCommon.RenderControl(lblOptStock));
                        strUI = strUI.Replace("##Opt_ItemPrice##", CCommon.RenderControl(lblOptPrice));
                        strUI = strUI.Replace("##Opt_ItemAttr##", CCommon.RenderControl(tblOptItemAttr));
                        strUI = strUI.Replace("##Opt_Quantity##", CCommon.RenderControl(txtOptUnits));
                        strUI = strUI.Replace("##Opt_AddToCartLink##", CCommon.RenderControl(lnkOptItemAdd));
                        strUI = strUI.Replace("##Opt_Description##", CCommon.RenderControl(lblOptDesc));

                        //Review

                        if (CCommon.ToLong(dtItemDetails.Rows[0]["ReviewCount"]) > 0)
                        {

                            strUI = strUI.Replace("##Separator##", "|");
                            strUI = strUI.Replace("##ReviewCount##", "<a href=\"/ReadReview.aspx?ItemID=" + this.ItemId + "\">" + CCommon.ToString(dtItemDetails.Rows[0]["ReviewCount"]) + "&nbsp;&nbsp;Review(s)</a>");

                            strUI = strUI.Replace("##ReviewCount##", CCommon.ToString(dtItemDetails.Rows[0]["ReviewCount"]));
                        }
                        else
                        {
                            strUI = strUI.Replace("##Separator##", "");
                            strUI = strUI.Replace("##ReviewCount##", "");
                            //strUI = strUI.Replace("##ReviewLabel##", "");

                            //strUI = strUI.Replace("##ReviewCount##", "");
                        }
                        strUI = strUI.Replace("##ItemId##", CCommon.ToString(this.ItemId));
                        strUI = strUI.Replace("##ProductName##", strProductName);
                        strUI = strUI.Replace("##CategoryName##", strCategoryName);


                        //Rating 

                        if (CCommon.ToLong(dtItemDetails.Rows[0]["RatingCount"]) == 1 || CCommon.ToLong(dtItemDetails.Rows[0]["RatingCount"]) == 0)
                        {
                            strUI = strUI.Replace("##RatingLabel##", "Rating");
                        }
                        else
                        {
                            strUI = strUI.Replace("##RatingLabel##", "Ratings");
                        }
                        strUI = strUI.Replace("##RatingCount##", CCommon.ToString(dtItemDetails.Rows[0]["RatingCount"]));

                        string[] title = { "Very Poor", "Poor", "Ok", "Good", "very Good" };
                        if (Sites.ToLong(Session["UserContactID"]) == 0)
                        {
                            strUI = strUI.Replace("##RateLabel##", "");
                            strUI = strUI.Replace("##WriteRating##", "");
                        }
                        else
                        {
                            strUI = strUI.Replace("##RateLabel##", "Rate this Product :");

                            int Rating = 0;
                            Rating = GetRatings();
                            string strRatingControl = GetRatingContol("WriteItemRating", false, false, Rating, false, 0, "auto-submit-star", true, title);
                            strUI = strUI.Replace("##WriteRating##", strRatingControl);
                        }


                        //for (int i = 1; i <=5 ; i++)
                        //{
                        //    if (i == Rating)
                        //    {
                        //        strUI = strUI.Replace("##Select_" + CCommon.ToString(i) + "##", "checked=\"checked\"");
                        //    }
                        //    else
                        //    {
                        //        strUI = strUI.Replace("##Select_"+CCommon.ToString(i)+"##", "");
                        //    }
                        //}

                        //Avg Rating 
                        double AvgRating = 0;
                        AvgRating = GetAvgRatings();
                        string strAvgRatingControl = GetRatingContol("ItemAverageRating", true, true, AvgRating, true, 2, "star", false, title);
                        strUI = strUI.Replace("##AverageRating##", strAvgRatingControl);

                        if (this.IsMetatagAdded == false)
                        {
                            if (CCommon.ToString(dtItemDetails.Rows[0]["vcMetaDescription"]) != "")
                            {
                                Page.Header.Controls.Add(GetMetaTag("description", CCommon.ToString(dtItemDetails.Rows[0]["vcMetaDescription"])));
                            }
                            if (CCommon.ToString(dtItemDetails.Rows[0]["vcMetaKeywords"]) != "")
                            {
                                Page.Header.Controls.Add(GetMetaTag("keywords", CCommon.ToString(dtItemDetails.Rows[0]["vcMetaKeywords"])));
                            }
                            this.IsMetatagAdded = true;
                        }

                        pnlCutomizeHtml.Visible = false;
                        litCutomizeHtml.Text = strUI;

                    }
                    else //Item does not exist
                    {
                        pnlCutomizeHtml.Visible = false;
                        litCutomizeHtml.Text = "";
                        ShowMessage(GetErrorMessage("ERR048"), 1);
                    }

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /*Function added by Neelam on 10/11/2017 - Added functionality to bind MiniCart pop up*/
        private void bindMiniCart()
        {
            try
            {
                DataSet ds = new DataSet();
                ds = GetCartItem();
                DataTable dt;

                if (ds != null && ds.Tables.Count > 0)
                {
                    dt = ds.Tables[0];
                    if (dt.Rows.Count > 0)
                    {
                        decimal total = Sites.ToDecimal(dt.Compute("SUM(monTotAmtBefDiscount)", ""));
                        string strUI = CCommon.ToString(Math.Round(total, 2));
                        strUI = strUI.Replace(strUI, string.Format("{0:#,##0.00###}", Sites.ToDecimal(dt.Compute("SUM(monTotAmtBefDiscount)", ""))));

                        if (Sites.ToBool(Session["HidePrice"]) == false)
                            lblSubTotal.Text = "SUBTOTAL: USD " + strUI;
                        else
                            lblSubTotal.Text = strUI.Replace(strUI, "NA");

                        gvMiniCart.DataSource = dt;
                        gvMiniCart.DataBind();

                        DataTable dtPromotions;
                        DataTable dtShippingPromotions;

                        if (ds != null && ds.Tables.Count > 0)
                        {
                            dt = ds.Tables[0];
                            if (dt.Rows.Count > 0)
                            {
                                var distinctRows = (from DataRow dRow in dt.Rows
                                                    where CCommon.ToString(dRow["numParentItemCode"]) == "" || dRow["numParentItemCode"] == null
                                                    select new { col1 = dRow["numItemCode"], col2 = dRow["numWarehouseid"] }).Distinct();

                                int _numParentItemCode = 0;
                                long _numWarehouseId = 0;

                                if (distinctRows.Count() > 0)
                                {
                                    foreach (var row in distinctRows)
                                    {
                                        _numParentItemCode = CCommon.ToInteger(row.col1);
                                        _numWarehouseId = CCommon.ToInteger(row.col2);

                                        if (_numParentItemCode != 0 && _numWarehouseId != 0)
                                        {
                                            dtPromotions = BindPromotions(CCommon.ToInteger(_numParentItemCode), CCommon.ToLong(_numWarehouseId));
                                            if (dtPromotions != null && dtPromotions.Rows.Count > 0)
                                            {
                                                divPromotion.Visible = true;
                                                lblVcPromotion.Text = CCommon.ToString(dtPromotions.Rows[0]["vcPromotionDescription"]);
                                                divPromotionDetailRI.Visible = true;
                                            }
                                        }
                                    }
                                }
                                HttpCookie cookie = Request.Cookies["Cart"];
                                cookie = GetCookie();

                                PromotionOffer objPromotionOffer = new PromotionOffer();
                                objPromotionOffer.DomainID = CCommon.ToLong(Session["DomainID"]);
                                objPromotionOffer.UserCntID = CCommon.ToLong(Session["UserContactID"]);
                                objPromotionOffer.Mode = 1;
                                objPromotionOffer.CookieId = CCommon.ToString(cookie["KeyId"]);

                                dtShippingPromotions = objPromotionOffer.GetShippingPromotions(CCommon.ToLong(Session["DefCountry"]));

                                if (dtShippingPromotions != null && dtShippingPromotions.Rows.Count > 0)
                                {
                                    divShipping.Visible = true;
                                    if (!string.IsNullOrEmpty(CCommon.ToString(dtShippingPromotions.Rows[0]["ShippingDescription"])))
                                    {
                                        lblShipping.Text = CCommon.ToString(dtShippingPromotions.Rows[0]["ShippingDescription"]);
                                    }
                                    else if (!string.IsNullOrEmpty(CCommon.ToString(dtShippingPromotions.Rows[0]["QualifiedShipping"])))
                                    {
                                        lblShipping.Text = CCommon.ToString(dtShippingPromotions.Rows[0]["QualifiedShipping"]);
                                    }
                                    else
                                    { divShipping.Visible = false; }
                                }
                                else { divShipping.Visible = false; }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void createPriceBookRuleTable(ref Table tblPriceRule)
        {
            PriceBookRule objPriceBookRule = default(PriceBookRule);
            DataTable dtPriceRule = default(DataTable);
            if (objPriceBookRule == null)
            {
                objPriceBookRule = new PriceBookRule();
            }
            objPriceBookRule.RuleID = Sites.ToLong(hdPriceBookId.Value);
            objPriceBookRule.CurrencyID = 0;
            dtPriceRule = objPriceBookRule.GetPriceTable();

            if (dtPriceRule.Rows.Count > 0)
            {
                Label lbl = default(Label);

                TableRow tbPRlrow = default(TableRow);
                TableCell tblPRcell = default(TableCell);
                tblPriceRule = new Table();
                tblPriceRule.CssClass = "PriceTable";
                tbPRlrow = new TableRow();

                tbPRlrow = new TableRow();
                tblPRcell = new TableCell();
                tblPRcell.CssClass = "PriceTable_Header";
                lbl = new Label();
                lbl.Text = "Qty From";
                tblPRcell.Controls.Add(lbl);
                tbPRlrow.Cells.Add(tblPRcell);

                tblPRcell = new TableCell();
                tblPRcell.CssClass = "PriceTable_Header";
                lbl = new Label();
                lbl.Text = "Qty To";
                tblPRcell.Controls.Add(lbl);
                tbPRlrow.Cells.Add(tblPRcell);

                tblPRcell = new TableCell();
                tblPRcell.CssClass = "PriceTable_Header";
                lbl = new Label();
                lbl.Text = "Amount";
                tblPRcell.Controls.Add(lbl);
                tbPRlrow.Cells.Add(tblPRcell);

                tblPriceRule.Rows.Add(tbPRlrow);

                for (int j = 0; j <= dtPriceRule.Rows.Count - 1; j++)
                {
                    tbPRlrow = new TableRow();

                    tblPRcell = new TableCell();
                    tblPRcell.CssClass = "PriceTable_Value";
                    lbl = new Label();
                    lbl.Text = dtPriceRule.Rows[j]["intFromQty"].ToString();
                    tblPRcell.Controls.Add(lbl);
                    tbPRlrow.Cells.Add(tblPRcell);

                    tblPRcell = new TableCell();
                    tblPRcell.CssClass = "PriceTable_Value";
                    lbl = new Label();
                    lbl.Text = dtPriceRule.Rows[j]["intToQty"].ToString();
                    tblPRcell.Controls.Add(lbl);
                    tbPRlrow.Cells.Add(tblPRcell);

                    tblPRcell = new TableCell();
                    tblPRcell.CssClass = "PriceTable_Value";
                    lbl = new Label();

                    double dPRPrice = 0, dPRDiscount = 0, dPRProductPrice = 0;

                    dPRDiscount = Sites.ToDouble(dtPriceRule.Rows[j]["decDiscount"]);
                    dPRProductPrice = Sites.ToDouble(lblProductPrice.Text);
                    if (Sites.ToInteger(dtPriceRule.Rows[j]["tintDiscountType"]) == 1)
                    {
                        dPRPrice = dPRProductPrice - (dPRProductPrice * (dPRDiscount / 100));
                    }
                    else if (Sites.ToInteger(dtPriceRule.Rows[j]["tintDiscountType"]) == 2)
                    {
                        dPRPrice = dPRProductPrice - dPRDiscount;
                    }

                    lbl.Text = string.Format("{0:#,##0.00###}", dPRPrice);
                    tblPRcell.Controls.Add(lbl);
                    tbPRlrow.Cells.Add(tblPRcell);

                    tblPriceRule.Rows.Add(tbPRlrow);
                }

            }

        }

        public void CreateAttributes()
        {
            try
            {
                objCommon = new CCommon();
                objItems = new CItems();

                string strAttributes = "";
                strValues = "";
                bool boolLoadNextdropdown = false;
                objItems.ItemCode = Sites.ToInteger(CCommon.ToString(this.ItemId));
                objItems.WarehouseID = Sites.ToLong(Session["WareHouseID"]);
                dtOppAtributes = objItems.GetOppItemAttributesForEcomm();
                if (dtOppAtributes.Rows.Count > 0)
                {
                    int i = 0;
                    Label lbl = default(Label);
                    TableRow tblrow = default(TableRow);
                    TableCell tblcell = default(TableCell);

                    if (AttributeControlType.Contains("RadioButton List"))
                    {
                        RadioButtonList ddl = default(RadioButtonList);
                        for (i = 0; i <= dtOppAtributes.Rows.Count - 1; i++)
                        {
                            tblrow = new TableRow();
                            tblcell = new TableCell();
                            tblcell.CssClass = "Product_Label";
                            tblcell.HorizontalAlign = HorizontalAlign.Left;
                            lbl = new Label();
                            lbl.Text = dtOppAtributes.Rows[i]["Fld_label"] + (txtHidValue.Text == "True" ? "" : "<font color='red'>*</font>");
                            //lbl.Font.Bold = true;
                            tblcell.Controls.Add(lbl);
                            tblrow.Cells.Add(tblcell);
                            tblItemAttr.Rows.Add(tblrow);
                            tblrow = new TableRow();
                            if (dtOppAtributes.Rows[i]["fld_type"].ToString() == "SelectBox")
                            {
                                tblcell = new TableCell();
                                tblcell.CssClass = "Product_Value";
                                ddl = new RadioButtonList();
                                ddl.CssClass = "dropdown";
                                ddl.ID = "Attr" + dtOppAtributes.Rows[i]["fld_id"];
                                ddl.Attributes.Add("AttributeName", dtOppAtributes.Rows[i]["Fld_label"].ToString());
                                tblcell.Controls.Add(ddl);
                                ddl.AutoPostBack = true;
                                ddl.SelectedIndexChanged += new EventHandler(this.LoadItemsonChangeofAttr);
                                tblrow.Cells.Add(tblcell);
                                strAttributes = strAttributes + ddl.ID + "~" + dtOppAtributes.Rows[i]["Fld_label"] + ",";
                                if (CCommon.ToLong(ddl.SelectedValue) > 0) strValues = strValues + ddl.SelectedValue + ",";

                                if (i == 0)
                                {
                                    objCommon.sb_FillAttibuesForEcommFromDB(ref ddl, Sites.ToLong(dtOppAtributes.Rows[i]["numlistid"].ToString()), this.ItemId, "", Session["WareHouseID"].ToString());
                                }
                                else
                                {
                                    objCommon.sb_FillAttibuesForEcommFromDB(ref ddl, (boolLoadNextdropdown == true || CCommon.ToBool(txtMatrix.Text) ? Sites.ToLong(dtOppAtributes.Rows[i]["numlistid"].ToString()) : 0), this.ItemId, strValues.TrimEnd(','), Session["WareHouseID"].ToString());
                                }

                                if (CCommon.ToLong(dtOppAtributes.Rows[i]["Fld_Value"]) > 0 && ddl.Items.FindByValue(CCommon.ToString(dtOppAtributes.Rows[i]["Fld_Value"])) != null)
                                {
                                    ddl.Items.FindByValue(CCommon.ToString(dtOppAtributes.Rows[i]["Fld_Value"])).Selected = true;
                                }

                                if (CCommon.ToLong(ddl.SelectedValue) > 0) boolLoadNextdropdown = true;
                            }
                            tblItemAttr.Rows.Add(tblrow);
                        }
                    }
                    else //if(AttributeControlType.Contains("DropDown List"))
                    {
                        DropDownList ddl = default(DropDownList);

                        for (i = 0; i <= dtOppAtributes.Rows.Count - 1; i++)
                        {
                            tblrow = new TableRow();
                            tblcell = new TableCell();
                            tblcell.CssClass = "Product_Label";
                            tblcell.HorizontalAlign = HorizontalAlign.Right;
                            lbl = new Label();
                            lbl.Text = dtOppAtributes.Rows[i]["Fld_label"] + (txtHidValue.Text == "True" ? "" : "<font color='red'>*</font>");
                            lbl.Font.Bold = true;
                            tblcell.Controls.Add(lbl);
                            tblrow.Cells.Add(tblcell);
                            if (dtOppAtributes.Rows[i]["fld_type"].ToString() == "SelectBox")
                            {
                                tblcell = new TableCell();
                                tblcell.CssClass = "Product_Value";
                                ddl = new DropDownList();// new DropDownList();
                                ddl.CssClass = "dropdown";
                                ddl.ID = "Attr" + dtOppAtributes.Rows[i]["fld_id"];
                                ddl.Attributes.Add("AttributeName", dtOppAtributes.Rows[i]["Fld_label"].ToString());
                                tblcell.Controls.Add(ddl);
                                ddl.AutoPostBack = true;
                                ddl.SelectedIndexChanged += new EventHandler(this.LoadItemsonChangeofAttr);
                                tblrow.Cells.Add(tblcell);
                                strAttributes = strAttributes + ddl.ID + "~" + dtOppAtributes.Rows[i]["Fld_label"] + ",";
                                if (CCommon.ToLong(ddl.SelectedValue) > 0) strValues = strValues + ddl.SelectedValue + ",";
                                if (i == 0)
                                {
                                    objCommon.sb_FillAttibuesForEcommFromDB(ref ddl, Sites.ToLong(dtOppAtributes.Rows[i]["numlistid"].ToString()), this.ItemId, "", Session["WareHouseID"].ToString());
                                }
                                else
                                {
                                    objCommon.sb_FillAttibuesForEcommFromDB(ref ddl, (boolLoadNextdropdown == true || CCommon.ToBool(txtMatrix.Text) ? Sites.ToLong(dtOppAtributes.Rows[i]["numlistid"].ToString()) : 0), this.ItemId, strValues.TrimEnd(','), Session["WareHouseID"].ToString());
                                }

                                if (CCommon.ToBool(txtMatrix.Text) && ddl.Items.Count > 0)
                                {
                                    ddl.Items.RemoveAt(0);
                                }

                                if (CCommon.ToLong(dtOppAtributes.Rows[i]["Fld_Value"]) > 0 && ddl.Items.FindByValue(CCommon.ToString(dtOppAtributes.Rows[i]["Fld_Value"])) != null)
                                {
                                    ddl.Items.FindByValue(CCommon.ToString(dtOppAtributes.Rows[i]["Fld_Value"])).Selected = true;
                                }

                                if (CCommon.ToLong(ddl.SelectedValue) > 0) boolLoadNextdropdown = true;
                            }
                            tblItemAttr.Rows.Add(tblrow);
                        }

                    }
                    strAttributes = strAttributes.TrimEnd(',');

                    if (CCommon.ToBool(txtMatrix.Text))
                    {
                        lnkAddToCart.Attributes.Add("onclick", "return Add('" + txtUnits.ClientID + "','" + txtHidValue.ClientID + "','" + txtAddedItems.ClientID + "','')");
                    }
                    else
                    {
                        lnkAddToCart.Attributes.Add("onclick", "return Add('" + txtUnits.ClientID + "','" + txtHidValue.ClientID + "','" + txtAddedItems.ClientID + "','" + strAttributes + "')");
                    }
                }
                else
                {
                    lnkAddToCart.Attributes.Add("onclick", "return Add('" + txtUnits.ClientID + "','" + txtHidValue.ClientID + "','" + txtAddedItems.ClientID + "','')");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void LoadItemsonChangeofAttr(object sender, EventArgs e)
        {
            try
            {
                objCommon = new CCommon();
                strValues = "";
                int i = 0;
                bool boolLoadnextDropdown = false;
                string senderID = null;
                string controlID = null;
                if (AttributeControlType.Contains("RadioButton List"))
                {
                    RadioButtonList Control;
                    Control = sender as RadioButtonList;//DropDownList;//default(DropDownList);
                    if (Control != null) senderID = Control.ID;
                    for (i = 0; i <= dtOppAtributes.Rows.Count - 1; i++)
                    {
                        controlID = "Attr" + dtOppAtributes.Rows[i]["fld_id"].ToString();
                        Control = (RadioButtonList)tblItemAttr.FindControl(controlID);
                        if (Sites.ToLong(Control.SelectedValue) > 0 & boolLoadnextDropdown == false) strValues = strValues + Control.SelectedValue + ",";
                        if (boolLoadnextDropdown == true)
                        {
                            objCommon.sb_FillAttibuesForEcommFromDB(ref Control, Sites.ToLong(dtOppAtributes.Rows[i]["numlistid"].ToString()), this.ItemId, strValues.TrimEnd(','), Sites.ToString(Session["WareHouseID"]));
                            boolLoadnextDropdown = false;
                        }
                        if (senderID == controlID)
                        {
                            boolLoadnextDropdown = true;
                            if (i == dtOppAtributes.Rows.Count - 1)
                            {
                                DataTable dtTable = default(DataTable);
                                dtTable = objCommon.GetWarehouseOnAttrSelEcomm(this.ItemId, strValues.TrimEnd(','), Sites.ToLong(Session["WareHouseID"].ToString()));
                                if (CCommon.ToBool(txtMatrix.Text))
                                {
                                    if (dtTable != null && dtTable.Rows.Count > 0)
                                    {
                                        if (CCommon.ToLong(dtTable.Rows[0]["numItemID"]) > 0)
                                        {
                                            string strGetUrlValue = Request.RawUrl.ToString();
                                            Response.Redirect(Request.UrlReferrer.ToString().Replace(strGetUrlValue.Split(new string[] { "/" }, StringSplitOptions.RemoveEmptyEntries)[3].ToString(), dtTable.Rows[0]["numItemID"].ToString()));
                                        }
                                        else
                                        {
                                            ShowMessage(GetErrorMessage("ERR080"), 1);
                                        }
                                    }
                                    else
                                    {
                                        ShowMessage(GetErrorMessage("ERR080"), 1);
                                    }
                                }
                            }
                        }
                    }
                }
                else //if (AttributeControlType.Contains("DropDown List"))
                {
                    DropDownList Control;
                    Control = sender as DropDownList;//DropDownList;//default(DropDownList);
                    if (Control != null) senderID = Control.ID;
                    for (i = 0; i <= dtOppAtributes.Rows.Count - 1; i++)
                    {
                        controlID = "Attr" + dtOppAtributes.Rows[i]["fld_id"].ToString();
                        Control = (DropDownList)tblItemAttr.FindControl(controlID);
                        if (CCommon.ToLong(Control.SelectedValue) > 0 & boolLoadnextDropdown == false) strValues = strValues + Control.SelectedValue + ",";
                        if (boolLoadnextDropdown == true)
                        {
                            objCommon.sb_FillAttibuesForEcommFromDB(ref Control, Sites.ToLong(dtOppAtributes.Rows[i]["numlistid"].ToString()), this.ItemId, strValues.TrimEnd(','), Sites.ToString(Session["WareHouseID"]));
                            boolLoadnextDropdown = false;
                        }
                        if (senderID == controlID)
                        {
                            boolLoadnextDropdown = true;
                            if (i == dtOppAtributes.Rows.Count - 1)
                            {
                                DataTable dtTable = default(DataTable);
                                dtTable = objCommon.GetWarehouseOnAttrSelEcomm(this.ItemId, strValues.TrimEnd(','), Sites.ToLong(Session["WareHouseID"].ToString()));

                                if (CCommon.ToBool(txtMatrix.Text))
                                {
                                    if (dtTable != null && dtTable.Rows.Count > 0)
                                    {
                                        if (CCommon.ToLong(dtTable.Rows[0]["numItemID"]) > 0)
                                        {
                                            string strGetUrlValue = Request.RawUrl.ToString();
                                            Response.Redirect(Request.UrlReferrer.ToString().Replace(strGetUrlValue.Split(new string[] { "/" }, StringSplitOptions.RemoveEmptyEntries)[3].ToString(), dtTable.Rows[0]["numItemID"].ToString()));
                                        }
                                        else
                                        {
                                            ShowMessage(GetErrorMessage("ERR080"), 1);
                                        }
                                    }
                                    else
                                    {
                                        ShowMessage(GetErrorMessage("ERR080"), 1);
                                    }
                                }

                                txtWareHouseItemID.Text = dtTable.Rows[0]["numWareHouseItemId"].ToString();
                                txtOnHand.Text = dtTable.Rows[0]["numOnHand"].ToString();
                                //lblStock.Text = dtTable.Rows[0]["InStock"].ToString();
                                decimal ExchangeRate;
                                ExchangeRate = Sites.ToDecimal(Session["ExchangeRate"].ToString()) == 0 ? 1 : Sites.ToDecimal(Session["ExchangeRate"].ToString());

                                lblProductPrice.Text = string.Format("{0:#,##0.00###}", Sites.ToDecimal(dtTable.Rows[0]["monWListPrice"]) / ExchangeRate);
                            }
                        }
                    }
                }

                if (Sites.ToBool(ApplyPriceBookPrice) == true && hdPriceBookId.Value.Length > 0)
                {
                    if (tblItemDetail.FindControl("tdPriceBookRule") != null)
                    {
                        //TableCell tblcell = new TableCell();
                        //tblcell = (TableCell)tblItemDetail.FindControl("tdPriceBookRule");
                        createPriceBookRuleTable(ref tblPriceBookRule);
                    }
                }

                bindHtml();
            }
            catch (Exception ex)
            {
                ExceptionModule.ExceptionPublish(ex, Sites.ToLong(Session["DomainID"]), Sites.ToLong(Session["UserContactID"]), Sites.ToLong(Session["SiteId"]), Request);
                ShowMessage(ex.ToString(), 1);
            }
        }

        public void LoadOptItemDetails()
        {
            try
            {
                if (ddlOptItem.SelectedIndex > 0)
                {
                    objItems = new CItems();

                    DataTable dtTable = default(DataTable);
                    DataSet ds = default(DataSet);
                    objItems.ItemCode = Sites.ToInteger(ddlOptItem.SelectedValue);//this.ItemId.ToString());
                    objItems.WarehouseID = Sites.ToLong(Session["WareHouseID"].ToString());
                    objItems.DomainID = Sites.ToLong(Session["DomainID"]);
                    objItems.SiteID = Sites.ToLong(Session["SiteId"]);
                    if ((!System.Web.HttpContext.Current.User.Identity.IsAuthenticated || CCommon.ToLong(Session["DomainID"]) == 0 || CCommon.ToLong(Session["UserContactID"]) == 0) && Sites.ToInteger(Session["OnePageCheckout"]) == 0)
                    {
                        //GUEST USER
                        objItems.DivisionID = 0;
                    }
                    else
                    {
                        objItems.DivisionID = Sites.ToLong(Session["DivId"]);
                    }
                    ds = objItems.ItemDetailsForEcomm();
                    dtTable = ds.Tables[0];
                    if (dtTable.Rows.Count == 1)
                    {
                        decimal ExchangeRate;
                        ExchangeRate = Sites.ToDecimal(Session["ExchangeRate"].ToString()) == 0 ? 1 : Sites.ToDecimal(Session["ExchangeRate"].ToString());

                        lblOptPrice.Text = string.Format("{0:#,##0.00###}", Sites.ToDecimal(dtTable.Rows[0]["monListPrice"]) / ExchangeRate);
                        lblOptDesc.Text = dtTable.Rows[0]["txtItemDesc"].ToString();
                        txtMatrix.Text = CCommon.ToBool(dtTable.Rows[0]["bitMatrix"]).ToString();
                        if (!(dtTable.Rows[0]["vcPathForImage"] == System.DBNull.Value))
                        {
                            if (!string.IsNullOrEmpty(dtTable.Rows[0]["vcPathForImage"].ToString()))
                            {
                                imgOptItem.Visible = true;
                                hplOptImage.Visible = true;
                                imgOptItem.ImageUrl = GetImagePath(Sites.ToString(dtTable.Rows[0]["vcPathForImage"]));
                                hplOptImage.Attributes.Add("onclick", "return OpenImage('" + Request.ApplicationPath + "','" + ddlOptItem.SelectedValue + "')");
                            }
                            else
                            {
                                imgOptItem.Visible = false;
                                hplOptImage.Visible = false;
                            }
                        }
                        else
                        {
                            imgOptItem.Visible = false;
                            hplOptImage.Visible = false;
                        }
                        //txtOptItemType.Text = dtTable.Rows[0]["charItemType"].ToString();
                        if (dtTable.Rows[0]["charItemType"].ToString() == "P" | dtTable.Rows[0]["charItemType"].ToString() == "A")
                        {
                            txtOptUnits.Visible = true;
                            txtHidOptValue.Text = "False";
                            if (dtTable.Rows.Count == 1)
                            {
                                txtOptWareHouseItemID.Text = dtTable.Rows[0]["numWareHouseItemID"].ToString();
                            }
                            txtOptWeight.Text = dtTable.Rows[0]["fltWeight"].ToString();
                            txtOptFreeShipping.Text = dtTable.Rows[0]["bitFreeShipping"].ToString();
                            txtOptOnHand.Text = ((Double)dtTable.Compute("SUM(numOnHand)", "")).ToString();
                            txtOptBackOrder.Text = dtTable.Rows[0]["bitAllowBackOrder"].ToString();
                            lblOptStock.Text = dtTable.Rows[0]["InStock"].ToString();
                        }
                        else
                        {
                            txtHidOptValue.Text = "";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void CreateOptAttributes()
        {
            try
            {
                objCommon = new CCommon();
                objItems = new CItems();

                string strAttributes = "";
                strValues = "";
                bool boolLoadNextdropdown = false;
                objItems.ItemCode = Sites.ToInteger(ddlOptItem.SelectedValue);
                dtOppOptAttributes = objItems.GetOppItemAttributes();
                if (dtOppOptAttributes.Rows.Count > 0)
                {
                    int i = 0;
                    Label lbl = default(Label);
                    TableRow tblrow = default(TableRow);
                    TableCell tblcell = default(TableCell);
                    DropDownList ddl = default(DropDownList);
                    tblrow = new TableRow();
                    for (i = 0; i <= dtOppOptAttributes.Rows.Count - 1; i++)
                    {
                        tblcell = new TableCell();
                        tblcell.CssClass = "LabelColumn";
                        tblcell.HorizontalAlign = HorizontalAlign.Right;
                        lbl = new Label();
                        lbl.Text = dtOppOptAttributes.Rows[i]["Fld_label"] + (txtHidOptValue.Text == "True" ? "" : "<font color='red'>*</font>");
                        tblcell.Controls.Add(lbl);
                        tblrow.Cells.Add(tblcell);

                        if (dtOppOptAttributes.Rows[i]["fld_type"].ToString() == "SelectBox")
                        {
                            tblcell = new TableCell();
                            tblcell.CssClass = "ControlCell";
                            ddl = new DropDownList();
                            ddl.CssClass = "dropdown";
                            ddl.ID = "AttrOpp" + dtOppOptAttributes.Rows[i]["fld_id"].ToString();

                            tblcell.Controls.Add(ddl);
                            ddl.AutoPostBack = true;
                            ddl.SelectedIndexChanged += new EventHandler(this.LoadOptItemsonChangeofAttr);
                            tblrow.Cells.Add(tblcell);
                            strAttributes = strAttributes + ddl.ID + "~" + dtOppOptAttributes.Rows[i]["Fld_label"] + ",";
                            if (CCommon.ToLong(ddl.SelectedValue) > 0) strValues = strValues + ddl.SelectedValue + ",";
                            if (i == 0)
                            {
                                objCommon.sb_FillAttibuesForEcommFromDB(ref ddl, Sites.ToLong(dtOppOptAttributes.Rows[i]["numlistid"].ToString()), Sites.ToLong(ddlOptItem.SelectedValue), "", Session["WareHouseID"].ToString());
                            }
                            else
                            {
                                objCommon.sb_FillAttibuesForEcommFromDB(ref ddl, (boolLoadNextdropdown == true ? Sites.ToLong(dtOppOptAttributes.Rows[i]["numlistid"].ToString()) : 0), Sites.ToLong(ddlOptItem.SelectedValue), strValues.TrimEnd(','), Session["WareHouseID"].ToString());
                            }
                            if (CCommon.ToLong(ddl.SelectedValue) > 0) boolLoadNextdropdown = true;
                        }
                    }
                    strAttributes = strAttributes.TrimEnd(',');
                    lnkOptItemAdd.Attributes.Add("onclick", "return AddOption('" + txtOptUnits.ClientID + "','" + txtHidOptValue.ClientID + "','" + txtAddedItems.ClientID + "','" + ddlOptItem.ClientID + "','" + strAttributes + "')");
                    tblOptItemAttr.Rows.Add(tblrow);
                }
                lnkOptItemAdd.Attributes.Add("onclick", "return AddOption('" + txtOptUnits.ClientID + "','" + txtHidOptValue.ClientID + "','" + txtAddedItems.ClientID + "','" + ddlOptItem.ClientID + "','')");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void LoadOptItemsonChangeofAttr(object sender, System.EventArgs e)
        {
            try
            {
                objCommon = new CCommon();
                strValues = "";
                int i = 0;
                DropDownList ddl = sender as DropDownList;
                bool boolLoadnextDropdown = false;
                string senderID = null;
                string controlID = null;
                if (ddl != null) senderID = ddl.ID;
                for (i = 0; i <= dtOppOptAttributes.Rows.Count - 1; i++)
                {
                    controlID = "AttrOpp" + dtOppOptAttributes.Rows[i]["fld_id"].ToString();
                    ddl = (DropDownList)tblOptItemAttr.FindControl(controlID);
                    if (CCommon.ToLong(ddl.SelectedValue) > 0 & boolLoadnextDropdown == false) strValues = strValues + ddl.SelectedValue + ",";
                    if (boolLoadnextDropdown == true)
                    {
                        objCommon.sb_FillAttibuesForEcommFromDB(ref ddl, Sites.ToLong(dtOppOptAttributes.Rows[i]["numlistid"].ToString()), Sites.ToLong(ddlOptItem.SelectedValue), strValues.TrimEnd(','), Session["WareHouseID"].ToString());
                        boolLoadnextDropdown = false;
                    }
                    if (senderID == controlID)
                    {
                        boolLoadnextDropdown = true;
                        if (i == dtOppOptAttributes.Rows.Count - 1)
                        {
                            DataTable dtTable = default(DataTable);
                            dtTable = objCommon.GetWarehouseOnAttrSelEcomm(Sites.ToInteger(ddlOptItem.SelectedValue), strValues, Sites.ToLong(Session["WareHouseID"].ToString()));
                            txtOptWareHouseItemID.Text = dtTable.Rows[0]["numWareHouseItemId"].ToString();
                            txtOptOnHand.Text = dtTable.Rows[0]["numOnHand"].ToString();
                            lblOptStock.Text = dtTable.Rows[0]["InStock"].ToString();

                            decimal ExchangeRate;
                            ExchangeRate = Sites.ToDecimal(Session["ExchangeRate"].ToString()) == 0 ? 1 : Sites.ToDecimal(Session["ExchangeRate"].ToString());
                            lblOptPrice.Text = string.Format("{0:#,##0.00###}", Sites.ToDecimal(dtTable.Rows[0]["monWListPrice"]) / ExchangeRate);
                        }
                    }
                }

                bindHtml();
            }
            catch (Exception ex)
            {
                ExceptionModule.ExceptionPublish(ex, Sites.ToLong(Session["DomainID"]), Sites.ToLong(Session["UserContactID"]), Sites.ToLong(Session["SiteId"]), Request);
                ShowMessage(ex.ToString(), 1);
            }
        }

        public static string ReplaceAllSpaces(string str)
        {
            return Regex.Replace(str, @"\s+", "%20");
        }

        private void AddToCart(bool IsAddToList)
        {
            try
            {
                dsTemp = GetCartItem(); //(DataSet)Session["Data"];
                DataTable dtItem = default(DataTable);
                dtItem = dsTemp.Tables[0];

                HttpCookie cookie = Request.Cookies["Cart"];
                if (cookie == null)
                {
                    SetCookie(cookie);
                }

                DataRow dr = default(DataRow);
                dr = dtItem.NewRow();
                dr["numOppItemCode"] = (dtItem.Compute("MAX(numOppItemCode)", "") == System.DBNull.Value ? 0 : Sites.ToInteger(dtItem.Compute("MAX(numOppItemCode)", "").ToString())) + 1;
                string[] str = ddlItemAttribute != null ? ddlItemAttribute.SelectedValue.Split('~') : null;
                if (str != null && str.Length == 2)
                {
                    dr["numItemCode"] = str[1];
                }
                else
                {
                    dr["numItemCode"] = this.ItemId;
                }

                dr["numUnitHour"] = Math.Abs(Sites.ToInteger(txtUnits.Text));


                dr["numUOM"] = hfUOM.Value;
                dr["vcUOMName"] = lblUnit.Text;
                dr["decUOMConversionFactor"] = hfConversionFactor.Value;

                dr["bitFreeShipping"] = Sites.ToBool(txtFreeShipping.Text);
                if (txtFreeShipping.Text != "True")
                {
                    dr["numWeight"] = Sites.ToDouble(txtWeight.Text);
                    dr["numHeight"] = Sites.ToDouble(txtHeight.Text);
                    dr["numWidth"] = Sites.ToDouble(txtWidth.Text);
                    dr["numLength"] = Sites.ToDouble(txtLength.Text);
                }

                var childKitSelectedItems = "";

                if (Sites.ToString(hdnHasChildKits.Value) == "1")
                {
                    System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
                    System.Collections.Generic.List<ChildKitSelection> childKitSelection = serializer.Deserialize<System.Collections.Generic.List<ChildKitSelection>>(hdnChildKitSelection.Value);

                    if (childKitSelection != null)
                    {
                        foreach (ChildKitSelection objChildKitSelection in childKitSelection)
                        {
                            foreach (string selectedItems in objChildKitSelection.selectedItems)
                            {
                                childKitSelectedItems += String.IsNullOrEmpty(childKitSelectedItems) ? (objChildKitSelection.numItemCode + "-" + selectedItems) : ("," + (objChildKitSelection.numItemCode + "-" + selectedItems));
                            }
                        }
                    }
                }

                //KEEP IT BELOW ABOVE LOGIC
                dr["vcChildKitItemSelection"] = childKitSelectedItems;

                //KEEP IT BELOW ABOVE LOGIC
                Double finalItemPrice = 0;

                if (Sites.ToString(hdnHasChildKits.Value) == "1")
                {
                    CItems objItem = new CItems();
                    objItem.DomainID = Sites.ToLong(HttpContext.Current.Session["DomainID"]);
                    objItem.SiteID = Sites.ToLong(HttpContext.Current.Session["SiteID"]);
                    objItem.DivisionID = Sites.ToLong(Session["DivId"]);
                    objItem.Quantity = Sites.ToInteger(txtUnits.Text);
                    objItem.ItemCode = Convert.ToInt32(this.ItemId);
                    objItem.KitSelectedChild = childKitSelectedItems;
                    DataTable dtCalculatedPrice = objItem.GetItemCalculatedPrice();

                    if (dtCalculatedPrice != null && dtCalculatedPrice.Rows.Count > 0)
                    {
                        finalItemPrice = Sites.ToDouble(dtCalculatedPrice.Rows[0]["monPrice"]) / Sites.ToDouble(Session["ExchangeRate"].ToString());
                    }
                }
                else
                {
                    finalItemPrice = Sites.ToDouble(lblProductPrice.Text);
                }

                /*Apply pricebook rule */
                if (Sites.ToBool(ApplyPriceBookPrice) == true)
                {
                    PriceBookRule objPbook = new PriceBookRule();
                    objPbook.ItemID = this.ItemId;
                    objPbook.QntyofItems = Sites.ToInteger(Sites.ToInteger(dr["numUnitHour"].ToString()) * Sites.ToDecimal(hfConversionFactor.Value));
                    objPbook.DomainID = Sites.ToLong(Session["DomainID"]);
                    objPbook.DivisionID = Sites.ToLong(Session["DivId"]);
                    objPbook.WarehouseID = Sites.ToLong(Session["WareHouseID"]);
                    objPbook.WarehouseItemID = Sites.ToLong(txtWareHouseItemID.Text);
                    objPbook.KitSelectedChild = childKitSelectedItems;
                    objPbook.SiteID = Sites.ToLong(HttpContext.Current.Session["SiteId"]);
                    DataTable dtCalPrice = default(DataTable);
                    dtCalPrice = objPbook.GetPriceBasedonPriceBook();

                    decimal ExchangeRate;
                    ExchangeRate = Sites.ToDecimal(Session["ExchangeRate"].ToString()) == 0 ? 1 : Sites.ToDecimal(Session["ExchangeRate"].ToString());
                    if (dtCalPrice.Rows.Count > 0)
                    {
                        finalItemPrice = Sites.ToDouble(Sites.ToDecimal(dtCalPrice.Rows[0]["ListPrice"]) * Sites.ToDecimal(hfConversionFactor.Value) / ExchangeRate);
                    }
                }

                dr["monPrice"] = finalItemPrice;
                dr["bitDiscountType"] = 0;
                dr["fltDiscount"] = 0;
                dr["monTotAmtBefDiscount"] = Sites.ToDouble(dr["numUnitHour"]) * Sites.ToDouble(dr["monPrice"]);
                dr["monTotAmount"] = Sites.ToDouble(dr["monTotAmtBefDiscount"]);
                if (Sites.ToString(hdnPromotionCoupon.Value) != "")
                {
                    dr["vcCoupon"] = hdnPromotionCoupon.Value;
                }
                else
                {
                    dr["vcCoupon"] = Session["CouponCode"];
                }

                dr["vcItemDesc"] = lblDescription.Text;
                dr["numWarehouseID"] = Sites.ToLong(Session["WareHouseID"]);
                if (str != null && str.Length == 2)
                {
                    dr["vcItemName"] = str[0];
                }
                else
                {
                    dr["vcItemName"] = lblProductName.Text;
                }

                if (txtHidValue.Text == "False")
                {
                    //dr["Warehouse") = ddlWarehouse.SelectedItem.Text
                    dr["numWarehouseItmsID"] = Sites.ToLong(txtWareHouseItemID.Text);
                    if (txtHidValue.Text == "False")
                    {
                        objCommon = new CCommon();
                        dr["vcAttributes"] = objCommon.GetAttributesForWarehouseItem(Sites.ToLong(dr["numWarehouseItmsID"].ToString()), false);
                    }
                    int i = 0;
                    string controlID = null;
                    if (AttributeControlType.Contains("RadioButton List"))
                    {
                        RadioButtonList rbl = default(RadioButtonList);
                        for (i = 0; i <= dtOppAtributes.Rows.Count - 1; i++)
                        {
                            controlID = "Attr" + Sites.ToString(dtOppAtributes.Rows[i]["fld_id"]).ToString();
                            rbl = (RadioButtonList)tblItemAttr.FindControl(controlID);
                            if (Sites.ToLong(rbl.SelectedValue) > 0) strValues = strValues + dtOppAtributes.Rows[i]["fld_id"] + ":" + rbl.SelectedValue + ",";

                            if (Sites.ToLong(dr["numWarehouseItmsID"]) == 0 && rbl.SelectedItem != null && Sites.ToLong(rbl.SelectedValue) > 0)
                            {
                                dr["vcAttributes"] = (string.IsNullOrEmpty(Sites.ToString(dr["vcAttributes"])) ? "" : dr["vcAttributes"] + ", ") + Sites.ToString(dtOppAtributes.Rows[i]["Fld_label"]) + ":" + rbl.SelectedItem.Text;
                            }
                        }

                        dr["vcAttrValues"] = strValues.TrimEnd(',');
                    }
                    else
                    {
                        DropDownList ddl = default(DropDownList);
                        for (i = 0; i <= dtOppAtributes.Rows.Count - 1; i++)
                        {
                            controlID = "Attr" + dtOppAtributes.Rows[i]["fld_id"].ToString();
                            ddl = (DropDownList)tblItemAttr.FindControl(controlID);
                            if (CCommon.ToLong(ddl.SelectedValue) > 0) strValues = strValues + dtOppAtributes.Rows[i]["fld_id"] + ":" + ddl.SelectedValue + ",";

                            if (Sites.ToLong(dr["numWarehouseItmsID"]) == 0 && ddl.SelectedItem != null && Sites.ToLong(ddl.SelectedValue) > 0)
                            {
                                dr["vcAttributes"] = (string.IsNullOrEmpty(Sites.ToString(dr["vcAttributes"])) ? "" : dr["vcAttributes"] + ", ") + Sites.ToString(dtOppAtributes.Rows[i]["Fld_label"]) + ":" + ddl.SelectedItem.Text;
                            }
                        }
                        dr["vcAttrValues"] = strValues.TrimEnd(',');
                    }
                }
                if (txtItemType.Text == "P")
                    dr["vcItemType"] = "Inventory Item";
                else if (txtItemType.Text == "S")
                    dr["vcItemType"] = "Service Item";
                else if (txtItemType.Text == "N")
                    dr["vcItemType"] = "Non-Inventory Item";
                else if (txtItemType.Text == "A")
                    dr["vcItemType"] = "Accessory";

                dr["tintOpFlag"] = 1;
                dr["ItemURL"] = hdnPath.Value;

                if (!IsAddToList)
                {
                    /*Check if item already exist with same attributes, if yes then increment no of units to buy*/
                    foreach (DataRow item in dtItem.Rows)
                    {
                        if (Sites.ToString(item["numItemCode"]) == Sites.ToString(dr["numItemCode"]) && Sites.ToString(item["vcAttrValues"]) == Sites.ToString(dr["vcAttrValues"]) && Sites.ToString(item["vcChildKitItemSelection"]) == Sites.ToString(dr["vcChildKitItemSelection"]))
                        {
                            item["numUnitHour"] = CCommon.ToInteger(item["numUnitHour"]) + Math.Abs(Sites.ToInteger(txtUnits.Text));
                            item["monTotAmount"] = Sites.ToDouble(item["numUnitHour"]) * Sites.ToDouble(dr["monPrice"]);
                            item["monTotAmtBefDiscount"] = Sites.ToDouble(item["numUnitHour"]) * Sites.ToDouble(item["monPrice"]);
                            item["vcCoupon"] = hdnPromotionCoupon.Value;
                            dsTemp.AcceptChanges();
                            UpdateCart(dsTemp);
                            return;//exit from function
                        }
                    }


                    dtItem.Rows.Add(dr);
                    dsTemp.AcceptChanges();
                    InsertToCart(dr);
                    DataTable dtTable = default(DataTable);
                    dtTable = dsTemp.Tables[0];
                    txtAddedItems.Text = "";
                    foreach (DataRow dr1 in dtTable.Rows)
                    {
                        txtAddedItems.Text = txtAddedItems.Text + Sites.ToString(dr1["numWarehouseItmsID"]).ToString() + ",";
                    }
                    txtAddedItems.Text = txtAddedItems.Text.Trim(',');

                    //Update Total Amount
                    if (dtTable.Rows.Count > 0)
                        Session["TotalAmount"] = Sites.ToDecimal(dtTable.Compute("SUM(monTotAmount)", ""));
                }
                else
                {
                    DataTable dtLists = new DataTable();
                    dtLists = dtItem.Clone();
                    DataRow dr1 = dtLists.NewRow();
                    dr1.ItemArray = dr.ItemArray;
                    dtLists.Rows.Add(dr1);
                    Session["ListData"] = dtLists;
                }
                Session["ShippingMethodConfirmed"] = "False";//Reset confirmation.. so user have to choose shipping method again.
                Session["SelectedStep"] = 0;
                //BindItem();

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void lnkAddToFav_Click()
        {
            try
            {
                AddToCart(true);
                if (Session["ListData"] != null)
                    Response.Redirect("/addtolist.aspx");
                else
                    ShowMessage(GetErrorMessage("ERR049"), 1);
            }
            catch (Exception ex)
            {
                ExceptionModule.ExceptionPublish(ex, Sites.ToLong(Session["DomainID"]), Sites.ToLong(Session["UserContactID"]), Sites.ToLong(Session["SiteId"]), Request);
                ShowMessage(ex.ToString(), 1);
            }
        }

        private int GetRatings()
        {
            try
            {
                DataTable dtRating = new DataTable();
                Review objReview = new Review();

                objReview.ReferenceId = CCommon.ToString(this.ItemId);
                objReview.ContactId = CCommon.ToLong(Session["ContactId"]);
                objReview.DomainID = CCommon.ToLong(Session["DomainId"]);
                objReview.SiteId = CCommon.ToLong(Session["SiteId"]);
                objReview.TypeId = 2;
                objReview.Mode = 1;
                dtRating = objReview.GetRatings();
                if (dtRating.Rows.Count > 0)
                {
                    return CCommon.ToInteger(dtRating.Rows[0]["intRatingCount"]);
                }
                return 0;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private double GetAvgRatings()
        {
            try
            {
                DataTable dtRating = new DataTable();
                Review objReview = new Review();

                objReview.ReferenceId = CCommon.ToString(this.ItemId);
                objReview.DomainID = CCommon.ToLong(Session["DomainId"]);
                objReview.SiteId = CCommon.ToLong(Session["SiteId"]);
                objReview.TypeId = 2;
                objReview.Mode = 2;
                dtRating = objReview.GetRatings();
                if (dtRating.Rows.Count > 0)
                {
                    return CCommon.ToDouble(dtRating.Rows[0]["AvgRatings"]);
                }
                return 0;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /*Function added by Neelam on 10/11/2017 - Added functionality to check Promotions for similar items*/
        /*private void RelatedItemsPromotion(int numItemCode, long numWarehouseItemID)
        {
            CItems objItems = new CItems();
            objItems.ItemCode = numItemCode;
            objItems.DomainID = CCommon.ToLong(Session["DomainID"]);
            objItems.WareHouseItemID = numWarehouseItemID;
            DataTable dt = objItems.GetPromotionsForSimilarItems();
            if (dt != null && dt.Rows.Count > 0)
            {
                divPromotionDetailRI.Visible = true;
            }
            else
            {
                divPromotionDetailRI.Visible = false;
            }
        }*/

        /*Function added by Neelam on 10/11/2017 - Added functionality to bind Promotions*/
        private DataTable BindPromotions(int numItemCode, long numWarehouseItemID)
        {
            CItems objItems = new CItems();
            objItems.ItemCode = numItemCode;
            objItems.DomainID = CCommon.ToLong(Session["DomainID"]);
            objItems.WareHouseItemID = numWarehouseItemID;
            objItems.SiteID = CCommon.ToLong(HttpContext.Current.Session["SiteID"]);
            DataTable dt = objItems.GetPromotionsForSimilarItems();
            return dt;
        }

        private string RenderHTML(Control ctrl)
        {
            StringBuilder sb = new StringBuilder();
            HtmlTextWriter myWriter = new HtmlTextWriter(new System.IO.StringWriter(sb, System.Globalization.CultureInfo.InvariantCulture));

            ctrl.RenderControl(myWriter);

            myWriter.Close();
            return sb.ToString();
        }

        #endregion
    }

    class ChildKitSelection
    {
        public long numItemCode { get; set; }
        public bool bitKitSingleSelect { get; set; }
        public string[] selectedItems { get; set; }
    }
}

