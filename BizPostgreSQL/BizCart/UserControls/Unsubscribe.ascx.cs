﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BACRM.BusinessLogic.ShioppingCart;
using BACRM.BusinessLogic.Contacts;
using BACRM.BusinessLogic.Common;
using System.Data;

namespace BizCart.UserControls
{
    public partial class Unsuscribe : BizUserControl
    {
        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    if (!System.Web.HttpContext.Current.User.Identity.IsAuthenticated || CCommon.ToLong(Session["DomainID"]) == 0 || CCommon.ToLong(Session["UserContactID"]) == 0)
                    {

                        Response.Redirect("Login.aspx?ReturnURL=" + (Sites.ToString(Session["SiteLiveURL"]).Trim().Length > 2 ? "https://" : "http://") + Request.Url.Host + "/Unsubscribe.aspx", true);
                    }
                    BindData();
                }
            }
            catch (Exception ex)
            {
                ExceptionModule.ExceptionPublish(ex, Sites.ToLong(Session["DomainID"]), Sites.ToLong(Session["UserContactID"]), Sites.ToLong(Session["SiteId"]), Request);
                ShowMessage(ex.ToString(), 1);
            }
        }
        protected void btnUnsubscribe_Click(object sender, EventArgs e)
        {
            try
            {
                CContacts objContact = new CContacts();
                objContact.ContactID = CCommon.ToLong(Session["UserContactID"]);
                objContact.Email = CCommon.ToString(Session["UserEmail"]); //txtEmailId.Text.Trim();
                objContact.OptOut = chkSubscribe.Checked;
                objContact.UpdateOptOut();
                ShowMessage(GetErrorMessage("ERR057"), 0);//Your subscription preferences have been saved.
                //lblmsg.Text = "Your subscription preferences have been saved.";
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region Methods

        #region HTML

        private void bindHtml()
        {
            try
            {
                string strUI = GetUserControlTemplate("AddPayment.htm");
                strUI = strUI.Replace("##chkSubscribe##", CCommon.RenderControl(chkSubscribe));
                strUI = strUI.Replace("##btnUnbscribe##", CCommon.RenderControl(btnUnsubscribe));
                strUI = strUI.Replace("##lblmessage##", CCommon.RenderControl(lblmsg));
                pnlCutomizeHtml.Visible = false;
                litCutomizeHtml.Text = strUI;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        private void BindData()
        {
            try
            {
                DataTable dtContactInfo;
                CContacts objContact = new CContacts();
                objContact.ContactID = Sites.ToLong(Session["UserContactID"]);
                objContact.DomainID = Sites.ToLong(Session["DomainID"]);
                objContact.ClientTimeZoneOffset = Sites.ToInteger(Session["ClientMachineUTCTimeOffset"]);
                dtContactInfo = objContact.GetCntInfoForEdit1();
                if (dtContactInfo.Rows.Count > 0)
                {
                    chkSubscribe.Checked = Sites.ToBool(dtContactInfo.Rows[0]["bitOptOut"]);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion
    }
}