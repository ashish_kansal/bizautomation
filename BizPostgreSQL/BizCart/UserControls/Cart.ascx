﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Cart.ascx.cs" Inherits="BizCart.UserControls.Cart" %>

<script type="text/javascript" language="javascript">
    var Page;

    function endRequest(sender, args) {
        if ($("[id$=UpdateProgress]") != null) {
            $("[id$=UpdateProgress]").hide();
        }
    }

    function DeleteRecord(a) {
        if (confirm('Are you sure, you want to delete the selected record?')) {
            __doPostBack('btnDelete', a);
            return false;
        }
        else {
            return false;
        }
    }
    function Save() {
        if (document.getElementById('ddlCountry').value == 0) {
            alert("Select Country.")
            document.getElementById('ddlCountry').focus();
            return false;
        }
        if (document.getElementById('ddlState').value == 0) {
            alert("Select State.")
            document.getElementById('ddlState').focus();
            return false;
        }
        if (document.getElementById('txtPostal').value == "") {
            alert("Enter Zip/Postal Code.")
            document.getElementById('txtPostal').focus()
            return false;
        }

        if ($("[id$=UpdateProgress]") != null) {
            Page = Sys.WebForms.PageRequestManager.getInstance();
            Page.add_endRequest(endRequest);
            $("[id$=UpdateProgress]").show();
        }

        return true;
    }

    function resetScrollPosition() {
        var position = $("#MessageBox").offset();
        window.scrollTo(0, position.top - 60);
    }


    function CheckNumber(cint, e) {
        var k;
        document.all ? k = e.keyCode : k = e.which;
        if (cint == 1) {
            if (!(k > 47 && k < 58 || k == 44 || k == 46 || k == 8 || k == 37 || k == 39 || k == 16)) {
                if (e.preventDefault) {
                    e.preventDefault();
                }
                else
                    e.returnValue = false;
                return false;
            }
        }
        if (cint == 2) {
            if (!(k > 47 && k < 58 || k == 8 || k == 37 || k == 39 || k == 16)) {
                if (e.preventDefault) {
                    e.preventDefault();
                }
                else
                    e.returnValue = false;
                return false;
            }
        }
    }

</script>
<style type="text/css">
    .overlay
    {
        position: fixed;
        z-index: 98;
        top: 0px;
        left: 0px;
        right: 0px;
        bottom: 0px;
        background-color: rgba(170, 170, 170, 0.5);
        filter: alpha(opacity=80);
    }
    
    .overlayContent
    {
        z-index: 99;
        margin: 250px auto;
    }
</style>
<asp:Panel ID="pnlCutomizeHtml" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <table cellspacing="1" cellpadding="2" width="100%">
        <tr>
            <td>
                <div class="sectionheader">
                    Items to be shipped
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <table border="0" cellpadding="0" cellspacing="5" width="100%">
                    <tr>
                        <td align="right">Partner Code
                        </td>
                        <td>
                            <asp:Label ID="lblPartnerCode" runat="server" Text="Label"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">Country
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlCountry" TabIndex="6" ClientIDMode="Static" AutoPostBack="True" runat="server"
                                Width="200" CssClass="signup" OnSelectedIndexChanged="ddlCountry_SelectedIndexChanged">
                                <asp:ListItem Value="0">--Select One--</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">State
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlState" ClientIDMode="Static" runat="server"
                                TabIndex="4" Width="200" CssClass="signup" AutoPostBack="true" OnSelectedIndexChanged="ddlState_SelectedIndexChanged">
                                <asp:ListItem Value="0">--Select One--</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">Zip Code
                        </td>
                        <td>
                            <asp:TextBox ID="txtPostal" TabIndex="5" runat="server" ClientIDMode="Static" CssClass="signup" AutoPostBack="true" Width="200" OnTextChanged="txtPostal_TextChanged"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <asp:CheckBox ID="chkResidential" AutoPostBack="true" ClientIDMode="Static" runat="server" Text=" Residential" OnCheckedChanged="chkResidential_CheckedChanged" />
                        </td>
                    </tr>
                    <tr>
                        <td align="right">Shipping Rates
                        </td>
                        <td>
                            <asp:ListBox ID="ddlShippingMethod" runat="server" TabIndex="4" Width="100%" Height="250"
                                CssClass="signup" AutoPostBack="True" OnSelectedIndexChanged="ddlShippingSpeedChoise_SelectedIndexChanged" runat="server"></asp:ListBox>
                        </td>
                    </tr>

                    <tr>
                        <td colspan="2">
                            <asp:Button ID="btnSubmit" runat="server" Text="Calculate" OnClientClick="javascript:return Save();" CssClass="button" OnClick="btnSubmit_Click" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <table width="100%">
                    <tr>
                        <td valign="top">
                            <table align="left" width="100%">
                                <tr id="trApplyCouponCode" runat="server">
                                    <td class="text_bold">
                                        <br />
                                        Apply Coupon/Discount Code:
                                        <asp:TextBox ID="txtCouponCode" runat="server" CssClass="textbox" MaxLength="20"></asp:TextBox>&nbsp;<asp:Button
                                            ID="btnCouponCode" runat="server" Text="Apply" CssClass="button" OnClick="btnCouponCode_Click" />
                                    </td>
                                </tr>
                                <tr id="trUsedCouponCode" runat="server" visible="false">
                                    <td class="text_bold">
                                        <br />
                                        Coupon/Discount Code:
                                        <asp:Label ID="lblCouponCode" runat="server"></asp:Label>&nbsp;<asp:LinkButton ID="lbCouponCodeRemove"
                                            runat="server" OnClick="lbCouponCodeRemove_Click">Remove</asp:LinkButton>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td>
                            <table align="right" id="tblShippingAddress" runat="server">
                                <tr>
                                    <td class="text_bold" align="right">Subtotal: &nbsp;
                                    </td>
                                    <td align="right">
                                        <asp:Label ID="lblSubTotal" runat="server" CssClass="text"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text_bold" align="right">Discount: &nbsp;
                                    </td>
                                    <td align="right">
                                        <asp:Label ID="lblDiscount" runat="server" CssClass="text"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text_bold" align="right">Tax: &nbsp;
                                    </td>
                                    <td align="right">
                                        <asp:Label ID="lblTax" runat="server" CssClass="text"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text_bold" align="right">Shipping Charges: &nbsp;
                                    </td>
                                    <td align="right">
                                        <asp:Label ID="lblShippingCost" runat="server" CssClass="text"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text_bold" align="right">Total: &nbsp;
                                    </td>
                                    <td align="right">
                                        <asp:Label ID="lblTotal" runat="server" CssClass="text"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Panel>
<asp:Literal ID="litCutomizeHtml" runat="server"></asp:Literal>
<asp:HiddenField ID="hdnReffereURL" runat="server" />
<asp:HiddenField ID="hdnStateLong" runat="server" />
<asp:HiddenField ID="hdnStateShort" runat="server" />
<asp:HiddenField ID="hdnScriptLoaded" runat="server" />
