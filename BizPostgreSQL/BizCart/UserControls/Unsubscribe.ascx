﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Unsubscribe.ascx.cs" Inherits="BizCart.UserControls.Unsuscribe" %>
 <div class="sectionheader">
 Unsubscribe Mailing List
 </div>
 Please confirm that you want to unsubscribe. Keep in mind that if you unsubscribe,
 you will not receive information about holiday / celebration discounts and other
 event-related great deals.
<asp:Panel ID="pnlCutomizeHtml" runat="server">
<table>
<tr>
<td><asp:CheckBox ID="chkSubscribe" runat="server" Text=" I want to subscribe news letter." /></td>
</tr>
<tr>
<td><asp:Button ID="btnUnsubscribe"  runat="server" Text="Update" 
        onclick="btnUnsubscribe_Click" CssClass="button" /></td>
</tr>
</table>
<asp:Label ID="lblmsg" runat="server"></asp:Label>
</asp:Panel>
<asp:Literal ID="litCutomizeHtml" runat="server"></asp:Literal>