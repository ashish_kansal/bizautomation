﻿using BACRM.BusinessLogic.Accounting;
using BACRM.BusinessLogic.Admin;
using BACRM.BusinessLogic.Alerts;
using BACRM.BusinessLogic.Common;
using BACRM.BusinessLogic.Contacts;
using BACRM.BusinessLogic.Documents;
using BACRM.BusinessLogic.Item;
using BACRM.BusinessLogic.Opportunities;
using BACRM.BusinessLogic.ShioppingCart;
using BACRM.BusinessLogic.Workflow;
using nsoftware.InPayPal;
using System;
using System.Collections;
using System.Data;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Linq;
using BACRM.BusinessLogic.Promotion;

namespace BizCart.UserControls
{
    public partial class OrderSummary : BizUserControl
    {
        #region Global Declaration

        long lngDivID = 0; public string AfterAddingItemShowCartPage { get; set; }
        long lngContId = 0;
        long lngOppBizDocID = 0;
        long lngCardTypeID = 0;
        long lngTransactionHistoryID = 0;
        long UndepositedFundAccountID = 0;
        string strQueryString = "";
        Boolean isShippingMethodNeeded = true;
        OppotunitiesIP objOpportunity = new OppotunitiesIP();
        PageControls objPageControls = new PageControls();
        CCommon objCommon = new CCommon();
        DataTable dtCustFld = default(DataTable);
        Boolean IsGoogleCheckoutConfigured = true;
        Boolean IsPaypalConfigured = true;
        string PaypalServerUrl = "";
        string PaypalRedirectServerUrl = "";
        Expresscheckout expresscheckout1 = new Expresscheckout();
        decimal dcTotalTax = 0;

        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {

            try
            {

                #region Check for Execution
                Sites.InitialiseSession();
                lngDivID = Sites.ToLong(Session["DivId"]);
                lngContId = Sites.ToLong(Session["UserContactID"]);


                if (CheckEmptyCart() == false)
                {
                    btnCharge.Visible = false;
                    Response.Redirect("Cart.aspx");
                    return;
                }

                if (!IsPostBack)
                {
                    DeleteItemFromCart(Sites.ToLong(Session["ShippingServiceItemID"]));

                    if (CCommon.ToLong(Session["OrderPromotionID"]) > 0 && CCommon.ToLong(Session["DiscountCodeID"]) > 0 && !string.IsNullOrEmpty(CCommon.ToString(Session["CouponCode"])))
                    {
                        hdnOrderPromotionID.Value = CCommon.ToString(Session["OrderPromotionID"]);
                        hdnDiscountCodeID.Value = CCommon.ToString(Session["DiscountCodeID"]);
                        lblCouponCode.Text = CCommon.ToString(Session["CouponCode"]);
                    }

                    Session["CouponCode"] = null;
                    Session["OrderPromotionID"] = null;
                    Session["DiscountCodeID"] = null;

                    AddOrderPromotionDiscountIfAvailable();

                    hdnIsFinishedPostCheckout.Value = "False";
                    if ((!System.Web.HttpContext.Current.User.Identity.IsAuthenticated || CCommon.ToLong(Session["DomainID"]) == 0 || CCommon.ToLong(Session["UserContactID"]) == 0) && Sites.ToInteger(Session["OnePageCheckout"]) == 0)
                    {
                        if (Sites.ToBool(System.Configuration.ConfigurationManager.AppSettings["IsDebugMode"]) == true)
                            Response.Redirect("Login.aspx?ReturnURL=" + "http://" + Request.Url.Host + "/" + CheckoutPageName(), true);
                        else
                            Response.Redirect("Login.aspx?ReturnURL=" + (Sites.ToString(Session["SiteLiveURL"]).Trim().Length > 2 ? "https://" : "http://") + Request.Url.Host + "/" + CheckoutPageName(), true);
                    }
                    if (Sites.ToString(Session["AddressConfirm"]) != "True" && Sites.ToInteger(Session["OnePageCheckout"]) == 0)
                    {


                        if (Sites.ToBool(Session["SkipStep2"]) == true && IsOnlySalesInquiry() == 1)
                        {
                            //Response.Redirect("OrderSummary.aspx", true);
                        }
                        else if (Sites.ToBool(Session["bitConfirmAddressRequired"]) == true)
                        {

                            DataTable dtTable = null;
                            CContacts objContact = new CContacts();
                            objContact.DomainID = Sites.ToLong(Session["DomainID"]);
                            objContact.RecordID = Sites.ToLong(Session["DivId"]);
                            objContact.byteMode = 5;
                            objContact.AddressType = CContacts.enmAddressType.BillTo;
                            dtTable = objContact.GetAddressDetail();
                            bool IsBillingAddressAvailable = false;
                            bool IsShippingAddressAvailable = false;

                            if (dtTable.Rows.Count > 0)
                            {
                                Session["BillAddress"] = dtTable.Rows[0]["numAddressID"];
                                IsBillingAddressAvailable = true;
                            }

                            objContact.DomainID = Sites.ToLong(Session["DomainID"]);
                            objContact.byteMode = 5;
                            objContact.RecordID = Sites.ToLong(Session["DivId"]);
                            objContact.AddressType = CContacts.enmAddressType.ShipTo;
                            dtTable = objContact.GetAddressDetail();
                            if (dtTable.Rows.Count > 0)
                            {
                                Session["ShipAddress"] = dtTable.Rows[0]["numAddressID"];
                                IsShippingAddressAvailable = true;
                            }
                            if (IsBillingAddressAvailable == false && IsShippingAddressAvailable == false)
                            {
                                Response.Redirect("CustomerAddress.aspx?AType=Bill&ReturnURL=" + (Sites.ToString(Session["SiteLiveURL"]).Trim().Length > 2 ? "https://" : "http://") + Request.Url.Host + "/" + CheckoutPageName(), true);
                            }
                            else
                            {

                                Session["AddressConfirm"] = "True";
                            }
                        }
                        else
                        {
                            Response.Redirect("ConfirmAddress.aspx?ReturnURL=" + (Sites.ToString(Session["SiteLiveURL"]).Trim().Length > 2 ? "https://" : "http://") + Request.Url.Host + "/" + CheckoutPageName(), true);
                        }

                    }
                    else
                    {
                        if (Sites.ToBool(Session["bitConfirmAddressRequired"]) == true)
                        {
                            DataTable dtTable = null;
                            CContacts objContact = new CContacts();
                            objContact.DomainID = Sites.ToLong(Session["DomainID"]);
                            objContact.RecordID = Sites.ToLong(Session["DivId"]);
                            objContact.byteMode = 5;
                            objContact.AddressType = CContacts.enmAddressType.BillTo;
                            dtTable = objContact.GetAddressDetail();
                            bool IsBillingAddressAvailable = false;
                            bool IsShippingAddressAvailable = false;

                            if (dtTable.Rows.Count > 0)
                            {
                                Session["BillAddress"] = dtTable.Rows[0]["numAddressID"];
                                IsBillingAddressAvailable = true;
                            }

                            objContact.DomainID = Sites.ToLong(Session["DomainID"]);
                            objContact.byteMode = 5;
                            objContact.RecordID = Sites.ToLong(Session["DivId"]);
                            objContact.AddressType = CContacts.enmAddressType.ShipTo;
                            dtTable = objContact.GetAddressDetail();
                            if (dtTable.Rows.Count > 0)
                            {
                                Session["ShipAddress"] = dtTable.Rows[0]["numAddressID"];
                                IsShippingAddressAvailable = true;
                            }
                            if (IsBillingAddressAvailable == false && IsShippingAddressAvailable == false)
                            {
                                Response.Redirect("CustomerAddress.aspx?AType=Bill&ReturnURL=" + (Sites.ToString(Session["SiteLiveURL"]).Trim().Length > 2 ? "https://" : "http://") + Request.Url.Host + "/" + CheckoutPageName(), true);
                            }
                            else
                            {

                                Session["AddressConfirm"] = "True";
                            }
                        }
                    }
                    if (Sites.ToString(Session["ShippingMethodConfirmed"]) != "True" && Sites.ToInteger(Session["OnePageCheckout"]) == 0)
                    {
                        //if (Sites.ToBool(Session["SkipStep2"]) == true && IsOnlySalesInquiry() == 1)
                        //{
                        //    //Response.Redirect("OrderSummary.aspx", true);
                        //}
                        //else
                        //{
                        //    Response.Redirect("ShippingOptions.aspx", true);
                        //}
                    }
                    if (lngDivID == 0) { return; }

                    Stream RequestStream = Request.InputStream;
                    //Relationship Mapping Validation
                    OppBizDocs objOpp = new OppBizDocs();
                    if (objOpp.ValidateARAP(lngDivID, 0, Sites.ToLong(Session["DomainID"])) == false)
                    {
                        Page.MaintainScrollPositionOnPostBack = false;
                        //Page.ClientScript.RegisterStartupScript(Page.ClientScript.GetType(), Page.ClientID, "resetScrollPosition();", true);
                        ShowMessage(GetErrorMessage("ERR014"), 1);//Please Set AR and AP Relationship from Administration->Master List Admin-> Accounting/RelationShip Mapping To Save
                        btnCharge.Visible = false;
                        return;
                    }
                    bindShipVia();

                    #region
                    txtCardNumber.Attributes.Add("onkeypress", "return CheckNumber(2,event);");
                    CheckCreditStatus();
                    //if (Sites.ToBool(Session["EnableCreditCart"]) == false)
                    //{
                    //    tblCustomerInfo.Visible = false;
                    //}
                    int i = 0;
                    ArrayList Years = new ArrayList();
                    for (i = 1; i <= 20; i++)
                    {
                        Years.Add(DateTime.Now.AddYears(i - 1).Date.ToString("yy"));
                    }
                    ddlCardExpYear.DataSource = Years;
                    ddlCardExpYear.DataBind();
                    ddlCardExpYear.Items.FindByValue(DateTime.Now.Year.ToString("yy"));
                    ddlCardExpMonth.SelectedIndex = (DateTime.Now.Month - 1);

                    BindCardType();
                    LoadDefaultCard();

                    if (Request.QueryString["paypalpaymentstatus"] == "paid")
                    {
                        PaypalCompletePayment(Request.QueryString["token"]);
                    }
                    //else if (Request.QueryString["paypalpaymentstatus"] == "cancelled")
                    //{
                    //    PaypalPaymentCancel();
                    //}

                    #endregion

                    // this.txtCardNumber.Attributes.Add("onkeyup", "javascript:Mask(this)");
                }
                if (IsPostBack)
                {
                    if (CheckEmptyCart() == true)
                    {
                        if (hdnPaymentMethod.Value != "")
                        {
                            if (Sites.ToString(Session["PayOption"]) != hdnPaymentMethod.Value)
                            {
                                PaymentModeChanged();
                            }
                        }
                        // bindHtml();
                    }
                    else
                    {
                        Response.Redirect("Home.aspx");
                    }
                    if (Sites.ToBool(Session["bitConfirmAddressRequired"]) == true)
                    {
                        DataTable dtTable = null;
                        CContacts objContact = new CContacts();
                        objContact.DomainID = Sites.ToLong(Session["DomainID"]);
                        objContact.RecordID = Sites.ToLong(Session["DivId"]);
                        objContact.byteMode = 5;
                        objContact.AddressType = CContacts.enmAddressType.BillTo;
                        dtTable = objContact.GetAddressDetail();
                        bool IsBillingAddressAvailable = false;
                        bool IsShippingAddressAvailable = false;

                        if (dtTable.Rows.Count > 0)
                        {
                            Session["BillAddress"] = dtTable.Rows[0]["numAddressID"];
                            IsBillingAddressAvailable = true;
                        }

                        objContact.DomainID = Sites.ToLong(Session["DomainID"]);
                        objContact.byteMode = 5;
                        objContact.RecordID = Sites.ToLong(Session["DivId"]);
                        objContact.AddressType = CContacts.enmAddressType.ShipTo;
                        dtTable = objContact.GetAddressDetail();
                        if (dtTable.Rows.Count > 0)
                        {
                            Session["ShipAddress"] = dtTable.Rows[0]["numAddressID"];
                            IsShippingAddressAvailable = true;
                        }
                        if (IsBillingAddressAvailable == false && IsShippingAddressAvailable == false)
                        {
                            Response.Redirect("CustomerAddress.aspx?AType=Bill&ReturnURL=" + (Sites.ToString(Session["SiteLiveURL"]).Trim().Length > 2 ? "https://" : "http://") + Request.Url.Host + "/" + CheckoutPageName(), true);
                        }
                        else
                        {

                            Session["AddressConfirm"] = "True";
                        }
                    }
                    if (Sites.ToString(Request["__EVENTTARGET"]) == "btnCharge")
                    {
                        btnCharge_Click(((object)(Request["__EVENTTARGET"])), null);
                    }
                }
            }
                #endregion

            catch (Exception ex)
            {
                ExceptionModule.ExceptionPublish(ex, Sites.ToLong(Session["DomainID"]), Sites.ToLong(Session["UserContactID"]), Sites.ToLong(Session["SiteId"]), Request);
                Page.MaintainScrollPositionOnPostBack = false;
                //Page.ClientScript.RegisterStartupScript(Page.ClientScript.GetType(), Page.ClientID, "resetScrollPosition();", true);
                ShowMessage(ex.ToString(), 1);
            }
        }

        void bindShipVia()
        {
            try
            {
                objCommon = new CCommon();
                ddlShipVia.DataSource = objCommon.GetMasterListItems(82, CCommon.ToLong(Session["DomainID"]));
                ddlShipVia.DataTextField = "vcData";
                ddlShipVia.DataValueField = "numListItemID";
                ddlShipVia.DataBind();
                ListItem listItem = new ListItem();
                listItem.Text = "-- Select One --";
                listItem.Value = "0";
                ddlShipVia.Items.Insert(0, listItem);
            }
            catch
            {

            }
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            try
            {
                if (CheckForExecution(2))
                {
                    if (Request.QueryString["paypalpaymentstatus"] == "cancelled" && Sites.ToString(Request.QueryString["token"]) == Sites.ToString(Session["newtoken"]))
                    {
                        PaypalPaymentCancel();
                    }
                    bindHtml();
                }
                else
                    pnlCutomizeHtml.Visible = false;

            }
            catch (Exception ex)
            {
                ExceptionModule.ExceptionPublish(ex, Sites.ToInteger(Session["DomainID"]), Sites.ToInteger(Session["UserContactID"]), Sites.ToInteger(Session["SiteId"]), Request);
                Page.MaintainScrollPositionOnPostBack = false;
                //Page.ClientScript.RegisterStartupScript(Page.ClientScript.GetType(), Page.ClientID, "resetScrollPosition();", true);
                ShowMessage(ex.ToString(), 1);
            }
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            try
            {
                FormConfigWizard objFormWizard = new FormConfigWizard();
                objFormWizard.DomainID = Sites.ToLong(Session["DomainID"]);
                objFormWizard.LocationIds = "2"; //sales opportunity 
                dtCustFld = objFormWizard.GetCustomFormFields();

                string FldName = "";
                string strUI = GetUserControlTemplate("OrderSummary.htm");
                foreach (DataRow row in dtCustFld.Rows)
                {
                    FldName = "##" + Sites.ToString(row["vcFormFieldName"]).Trim().Replace(" ", "") + "_" + Sites.ToString(row["vcFieldType"]) + "##";
                    if (strUI.Contains(FldName))
                    {
                        Control c = null;
                        c = (Control)GenerateGenericFormControls.getDynamicControlAndData(Sites.ToInteger(Sites.ToString(row["numFormFieldId"]).Replace("R", "").Replace("C", "").Replace("D", "")), row["vcFormFieldName"].ToString().Trim().Replace(" ", "") + "_" + row["vcFieldType"].ToString(), row["vcListItemType"].ToString(), row["vcAssociatedControlType"].ToString(), Sites.ToInteger(row["numListID"]), 0, 0);
                        plhFormControls.Controls.Add(c);
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionModule.ExceptionPublish(ex, Sites.ToInteger(Session["DomainID"]), Sites.ToInteger(Session["UserContactID"]), Sites.ToInteger(Session["SiteId"]), Request);
                Page.MaintainScrollPositionOnPostBack = false;
                ShowMessage(ex.ToString(), 1);
            }
        }

        //public static void Show(string message)
        //{
        //    // Cleans the message to allow single quotation marks
        //    string cleanMessage = message.Replace("'", "\\'");
        //    string script = "<script type=\"text/javascript\">alert('" + cleanMessage + "');</script>";

        //    // Gets the executing web page
        //    Page page = HttpContext.Current.CurrentHandler as Page;

        //    // Checks if the handler is a Page and that the script isn't allready on the Page
        //    if (page != null && !page.ClientScript.IsClientScriptBlockRegistered("alert"))
        //    {
        //        page.ClientScript.RegisterClientScriptBlock(typeof(Alert), "alert", script);
        //    }
        //}

        protected void btnCharge_Click(object sender, EventArgs e)
        {
            try
            {
                if (Convert.ToString(HttpContext.Current.Session["PostUpSell"]) == "True" && hdnIsFinishedPostCheckout.Value == "False")
                {
                    CItems objItem = new CItems();
                    objItem.DomainID = Sites.ToLong(Session["DomainID"]);
                    HttpCookie cookie = GetCookie();
                    objItem.UserCntID = CCommon.ToLong(Session["UserContactID"]);
                    objItem.vcCookieId = (cookie != null) ? cookie["KeyId"] : "";
                    objItem.byteMode = 7;
                    objItem.SiteID = CCommon.ToLong(Session["SiteID"]);

                    //DataTable dtCart = (DataTable)Session["CartData"];
                    DataSet ds = GetCartItem();
                    DataTable dtCart = ds.Tables[0];

                    if (dtCart != null && dtCart.Rows.Count > 0 && hdnIsFinishedPostCheckout.Value == "False")
                    {
                        bindPostCheckout(dtCart, objItem);
                    }
                }
                else
                {
                    Session["TotalAmount"] = GetCartTotalAmount();
                    decimal totalAmount = Convert.ToDecimal(Session["TotalAmount"]);

                    if (Sites.ToString(Session["PayOption"]) != "" || totalAmount == 0)
                    {
                        #region If PayOption Selected
                        if (ddlShippingMethod.SelectedIndex != 0 || hdnIsStaticShippingCharge.Value == "1")
                        {
                            #region dropdown shippingMethod
                            if (CheckEmptyCart() == false) { return; }

                            #region Check for User Information
                            int ErrorCode = 0;
                            ErrorCode = CheckForUserDetail();

                            if (ErrorCode > 0)
                            {
                                if (ErrorCode == 5)
                                {
                                    Response.Redirect("Login.aspx?ReturnURL=" + (Sites.ToString(Session["SiteLiveURL"]).Trim().Length > 2 ? "https://" : "http://") + Request.Url.Host + "/" + CheckoutPageName() + "&errcode=" + ErrorCode, true);
                                }
                                else if (ErrorCode < 5)
                                {
                                    Response.Redirect("CustomerInformation.aspx?ReturnURL=" + (Sites.ToString(Session["SiteLiveURL"]).Trim().Length > 2 ? "https://" : "http://") + Request.Url.Host + "/" + CheckoutPageName() + "&errcode=" + ErrorCode, true);
                                }
                                else
                                {
                                    Response.Redirect("ConfirmAddress.aspx?ReturnURL=" + (Sites.ToString(Session["SiteLiveURL"]).Trim().Length > 2 ? "https://" : "http://") + Request.Url.Host + "/" + CheckoutPageName() + "&errcode=" + ErrorCode, true);
                                }
                            }
                            else
                            {
                                Boolean ProceedForCheckout = true; //it is taken for checking if payment method is bill me and credit amount is less than amount due then it will not proceed


                                DataTable dtEnforcedMinSubTotal = CheckIfMinOrderAmountRuleMeets(totalAmount);
                                if (dtEnforcedMinSubTotal != null && dtEnforcedMinSubTotal.Rows.Count > 0 && CCommon.ToInteger(dtEnforcedMinSubTotal.Rows[0]["returnVal"]) == 0)
                                {
                                    decimal subTotal = Convert.ToDecimal(dtEnforcedMinSubTotal.Rows[0]["MinOrderAmount"]);
                                    //ScriptManager.RegisterClientScriptBlock(this.Page, Page.GetType(), "MinOrderAmountRuleMeets", "alert('You must spend a minimum of $" + subTotal + " to complete this order.');", true);

                                    string radalertscript = "<script language='javascript'>function f(){radalert('You must spend a minimum of $" + subTotal + " to complete this order.', 450, 180); Sys.Application.remove_load(f);}; Sys.Application.add_load(f);</script>";
                                    Page.ClientScript.RegisterStartupScript(this.GetType(), "radalert", radalertscript);
                                    return;
                                }
                                //}
                                if (Sites.ToString(Session["PayOption"]) == "BillMe")
                                {
                                    decimal totalDue = default(decimal);
                                    totalDue = Sites.ToDecimal(txtDueAmount.Text) + Sites.ToDecimal(Session["TotalAmount"]);
                                    if (totalDue > Sites.ToDecimal(txtCredit.Text))
                                    {
                                        ShowMessage(GetErrorMessage("ERR017"), 0);//You have exceeded the maximum amount you can place on a bill-to account by 'whatever the amount is' Please reduce your total amount to compensate, or if you prefer, execute the order by paying with a check.
                                        return;
                                    }
                                }
                                if (ProceedForCheckout)
                                {
                                    #region Check for Shipping Service as a Line Item
                                    if (Sites.ToLong(Session["ShippingServiceItemID"]) > 0)
                                    {
                                        #region Check for Shipping Service

                                        if (Sites.ToLong(Session["DiscountServiceItemID"]) > 0)
                                        {
                                            DeleteItemFromCart(Sites.ToLong(Session["ShippingServiceItemID"]));
                                            #region check for Coupen Service

                                            try
                                            {
                                                if (IsOnlySalesInquiry() != 1)
                                                {
                                                    AddToCartFromEcomm("", Sites.ToLong(Session["ShippingServiceItemID"]), Sites.ToDouble(GetShippingCharge()), GetShippingMethodName(), true);
                                                }
                                            }
                                            catch (Exception ex)
                                            {
                                                throw;
                                            }

                                            SaveOpportunity();
                                            SaveCusField();

                                            Session["TotalAmount"] = GetCartTotalAmount();

                                            //Set strQueryString for Redirecting on Thank You page and it is also useful for  Google Checkout Return URL 
                                            if (Sites.ToDouble(Session["OppID"]) > 0)
                                            {
                                                strQueryString = "OppID=" + Convert.ToString(Session["OppID"]);
                                                Session["TOppId"] = Session["OppID"];
                                                Session["TOppName"] = Session["OppName"];
                                                Session["TotalAffliateAmount"] = GetCartTotalAmount();
                                                Session["CartItemsForAffliate"] = GetCartItem();
                                                string[] strFName = null;
                                                string strFilePath = null;
                                                string strFileName = null;
                                                string strFileType = null;

                                                if ((FluDocument.HasFile))
                                                {
                                                    strFileName = Path.GetFileName(FluDocument.PostedFile.FileName);
                                                    //Getting the File Name
                                                    // If Folder Does not exists create New Folder.
                                                    if (Directory.Exists(CCommon.GetDocumentPhysicalPath(Sites.ToLong(Session["DomainID"]))) == false)
                                                    {
                                                        Directory.CreateDirectory(CCommon.GetDocumentPhysicalPath(Sites.ToLong(Session["DomainID"])));
                                                    }
                                                    strFilePath = CCommon.GetDocumentPhysicalPath(Sites.ToLong(Session["DomainID"])) + strFileName;
                                                    strFName = strFileName.Split('.');
                                                    strFileType = "." + Path.GetExtension(FluDocument.PostedFile.FileName);
                                                    //Getting the Extension of the File
                                                    FluDocument.PostedFile.SaveAs(strFilePath);

                                                    UploadFile("L", strFileType, strFileName, strFileName, "", 7983, "370");
                                                }
                                            }

                                            if (totalAmount > 0)
                                            {
                                                if (Sites.ToString(Session["PayOption"]) == "CreditCard")
                                                {
                                                    #region Credit Card Process 1

                                                    if (!txtCardNumber.Text.Contains("############"))
                                                    {
                                                        Session["CardNumber"] = txtCardNumber.Text;
                                                    }

                                                    if (Sites.ToLong(ddlCardType.SelectedValue) == 0)
                                                    { ShowMessage(GetErrorMessage("ERR016"), 1); return; }//Please select valid card type.

                                                    SaveCustomerCreditCardInfo();

                                                    string responseCode = "";
                                                    string ResponseMessage = "";
                                                    string ReturnTransactionID = "";
                                                    PaymentGateway objPaymentGateway = new PaymentGateway();
                                                    objPaymentGateway.DomainID = Sites.ToLong(Session["DomainID"]);

                                                    QueryStringValues objEncryption = new QueryStringValues();
                                                    OppInvoice objoppinvoice = new OppInvoice();
                                                    objoppinvoice.bitflag = true;
                                                    objoppinvoice.DomainID = Sites.ToInteger(Session["DomainId"]);
                                                    objoppinvoice.IsDefault = true;
                                                    objoppinvoice.UserCntID = Sites.ToLong(Session["UserContactID"]);
                                                    DataSet ds1 = objoppinvoice.GetCustomerCreditCardInfo();
                                                    if (ds1 != null && ds1.Tables.Count > 0)
                                                    {
                                                        if (ds1.Tables[0].Rows.Count > 0)
                                                        {
                                                            string strCardNo = objEncryption.Decrypt(Sites.ToString(ds1.Tables[0].Rows[0]["vcCreditCardNo"])).Trim();//txtCHName.Text.Trim();
                                                            string strCardHolder = objEncryption.Decrypt(Sites.ToString(ds1.Tables[0].Rows[0]["vcCardHolder"])).Trim();//txtCHName.Text.Trim();
                                                            string strCardCvv = objEncryption.Decrypt(Sites.ToString(ds1.Tables[0].Rows[0]["vcCVV2"])).Trim();//txtCardCVV2.Text.Trim();

                                                            bool CreditCardStatus = false;
                                                            long eCommCCTransactionLogID = 0;
                                                            ECommerceCCTransactionLog objECommerceCCTransactionLog = new ECommerceCCTransactionLog();
                                                            try
                                                            {
                                                                objECommerceCCTransactionLog.DomainID = Sites.ToLong(Session["DomainID"]);
                                                                objECommerceCCTransactionLog.OppID = Sites.ToLong(Session["OppID"]);
                                                                objECommerceCCTransactionLog.Amount = Sites.ToDecimal(Session["TotalAmount"]);
                                                                eCommCCTransactionLogID = objECommerceCCTransactionLog.CallStarted();

                                                                CreditCardStatus = objPaymentGateway.GatewayTransaction(Sites.ToDecimal(Session["TotalAmount"]), strCardHolder, strCardNo, strCardCvv, ddlCardExpMonth.SelectedValue.PadLeft(2, '0'), Sites.ToString(ddlCardExpYear.SelectedValue), lngContId, ref ResponseMessage, ref ReturnTransactionID, ref responseCode, Sites.ToBool(Application["CreditCardAuthOnly"]), "", Sites.ToString(Session["OppName"]), Sites.ToInteger(Session["SiteId"]));

                                                                objECommerceCCTransactionLog.ID = eCommCCTransactionLogID;
                                                                objECommerceCCTransactionLog.IsSuccess = CreditCardStatus;
                                                                objECommerceCCTransactionLog.Message = ResponseMessage;
                                                                objECommerceCCTransactionLog.ExceptionMessage = "";
                                                                objECommerceCCTransactionLog.StackStrace = "";
                                                                objECommerceCCTransactionLog.CallCompleted();
                                                            }
                                                            catch (Exception ex)
                                                            {
                                                                objECommerceCCTransactionLog.ID = eCommCCTransactionLogID;
                                                                objECommerceCCTransactionLog.IsSuccess = false;
                                                                objECommerceCCTransactionLog.Message = ResponseMessage;
                                                                objECommerceCCTransactionLog.ExceptionMessage = ex.Message;
                                                                objECommerceCCTransactionLog.StackStrace = ex.StackTrace;
                                                                objECommerceCCTransactionLog.CallCompleted();

                                                                CreditCardStatus = false;
                                                                throw;
                                                            }
                                                            #region processing of credit card

                                                            if (CreditCardStatus)
                                                            {
                                                                //TODO:Insert Transaction details into TransactionHistory table
                                                                //1 capture 2 Authorise
                                                                TransactionHistory objTransactionHistory = new TransactionHistory();
                                                                objTransactionHistory.TransHistoryID = 0;
                                                                objTransactionHistory.DivisionID = lngDivID;
                                                                objTransactionHistory.ContactID = lngContId;
                                                                objTransactionHistory.OppID = Sites.ToLong(Session["OppID"]);
                                                                objTransactionHistory.OppBizDocsID = Sites.ToLong(Session["OppBizDocID"]);
                                                                objTransactionHistory.TransactionID = Sites.ToString(ReturnTransactionID);
                                                                objTransactionHistory.TransactionStatus = (Sites.ToBool(Application["CreditCardAuthOnly"]) == true ? CCommon.ToShort(1) : CCommon.ToShort(2));
                                                                objTransactionHistory.PGResponse = ResponseMessage;
                                                                objTransactionHistory.Type = 1;//1 is for Authorative and 2 for Captured
                                                                if (Sites.ToBool(Application["CreditCardAuthOnly"]))
                                                                {
                                                                    objTransactionHistory.AuthorizedAmt = Sites.ToDecimal(Session["TotalAmount"]);
                                                                }
                                                                else
                                                                {
                                                                    objTransactionHistory.CapturedAmt = Sites.ToDecimal(Session["TotalAmount"]);
                                                                }
                                                                objTransactionHistory.RefundAmt = 0;
                                                                objTransactionHistory.CardHolder = objCommon.Encrypt(strCardHolder);
                                                                objTransactionHistory.CreditCardNo = objCommon.Encrypt(strCardNo.Trim().Contains("*") ? Sites.ToString(Session["CCNO"]) : strCardNo.Trim());
                                                                objTransactionHistory.Cvv2 = objCommon.Encrypt(strCardCvv);
                                                                objTransactionHistory.ValidMonth = CCommon.ToShort(Sites.ToString(ds1.Tables[0].Rows[0]["tintValidMonth"]).PadLeft(2, '0'));
                                                                objTransactionHistory.ValidYear = CCommon.ToShort(ds1.Tables[0].Rows[0]["intValidYear"]);
                                                                objTransactionHistory.SignatureFile = "";
                                                                objTransactionHistory.UserCntID = Sites.ToLong(Session["UserContactID"]);
                                                                objTransactionHistory.DomainID = Sites.ToLong(Session["DomainID"]);
                                                                objTransactionHistory.CardType = Sites.ToLong(ds1.Tables[0].Rows[0]["numCardTypeId"]);
                                                                objTransactionHistory.ResponseCode = responseCode;
                                                                lngTransactionHistoryID = objTransactionHistory.ManageTransactionHistory();

                                                                objECommerceCCTransactionLog.ID = eCommCCTransactionLogID;
                                                                objECommerceCCTransactionLog.TransactionHistoryID = lngTransactionHistoryID;
                                                                objECommerceCCTransactionLog.UpdateTransactionHistoryID();

                                                                MakeDepositeEntries();
                                                                CreateShippingReport();//create shipping report

                                                                ClearCartItems();//Clear existing cart
                                                            }
                                                            else
                                                            {
                                                                #region Update Opportunity Status Failed .
                                                                CCommon objCommon = new CCommon();
                                                                objCommon.Mode = 37;
                                                                objCommon.UpdateRecordID = Sites.ToLong(Session["OppID"]);
                                                                objCommon.DomainID = Sites.ToLong(Session["DomainID"]);
                                                                objCommon.UpdateSingleFieldValue();

                                                                Page.MaintainScrollPositionOnPostBack = false;
                                                                ShowMessage(ResponseMessage, 0);
                                                                return;

                                                                #endregion
                                                            }
                                                            #endregion
                                                        }
                                                    }
                                                    #endregion
                                                }
                                                else if (Sites.ToString(Session["PayOption"]) == "BillMe")
                                                {
                                                    #region Bill Me Process 27261
                                                    MakeDepositeEntries();
                                                    CreateShippingReport(); //create shipping report
                                                    ClearCartItems();//Clear existing cart
                                                    #endregion
                                                }
                                                else if (Sites.ToString(Session["PayOption"]) == "GoogleCheckout")
                                                {
                                                    #region Check for Google Checkout Configuration
                                                    UserAccess objUserAccess = new UserAccess();
                                                    objUserAccess.DomainID = Sites.ToLong(Session["DomainID"]);
                                                    objUserAccess.SiteID = Sites.ToLong(Session["SiteId"]);
                                                    string strGoogleMerchantID = "";
                                                    string strGoogleMerchantKey = "";
                                                    Boolean IsSandbox = true;
                                                    DataTable dtECommerceDetail = objUserAccess.GetECommerceDetails();

                                                    if (dtECommerceDetail != null)
                                                    {
                                                        strGoogleMerchantID = Sites.ToString(dtECommerceDetail.Rows[0]["vcGoogleMerchantID"]);
                                                        strGoogleMerchantKey = Sites.ToString(dtECommerceDetail.Rows[0]["vcGoogleMerchantKey"]);
                                                        IsSandbox = Sites.ToBool(dtECommerceDetail.Rows[0]["IsSandbox"]);
                                                    }
                                                    else
                                                    {
                                                        strGoogleMerchantID = "";
                                                        strGoogleMerchantKey = "";
                                                    }

                                                    if (strGoogleMerchantID == "" || strGoogleMerchantKey == "")
                                                    {
                                                        IsGoogleCheckoutConfigured = false;
                                                        ShowMessage(GetErrorMessage("ERR061"), 1);
                                                        return;
                                                    }
                                                    #endregion

                                                    if (IsGoogleCheckoutConfigured)
                                                    {
                                                        #region Google Checkout Process 31488
                                                        BACRM.BusinessLogic.ShioppingCart.GoogleCheckout objGoogleCheckout = new GoogleCheckout();

                                                        DataSet dsCart = GetCartItem();
                                                        if (dsCart != null)
                                                        {
                                                            //set strQueryString variable to redirect to Thank You Page with Querystring. After Creation of Bizdoc we get lngBizDocID
                                                            MakeDepositeEntries();
                                                            CreateShippingReport();

                                                            if (dsCart.Tables[0].Rows.Count > 0)
                                                            {
                                                                //DataTable dtTable = null;
                                                                //CContacts objContact = new CContacts();
                                                                //objContact.DomainID = Sites.ToLong(Session["DomainID"]);

                                                                //if (CCommon.ToShort(Session["BaseTaxOn"]) == 1)
                                                                //{
                                                                //    objContact.AddressID = Sites.ToLong(Session["BillAddress"]);
                                                                //}
                                                                //else if (CCommon.ToShort(Session["BaseTaxOn"]) == 2)
                                                                //{
                                                                //    objContact.AddressID = Sites.ToLong(Session["ShipAddress"]);
                                                                //}
                                                                //else
                                                                //{
                                                                //    objContact.AddressID = 0;
                                                                //}

                                                                //objContact.byteMode = 1;
                                                                //dtTable = objContact.GetAddressDetail();

                                                                decimal Tax = default(decimal);
                                                                Tax = GetTax();

                                                                string strGoogleRedirect = "";
                                                                strGoogleRedirect = objGoogleCheckout.GoogleCheckoutRequest(strGoogleMerchantID, strGoogleMerchantKey, IsSandbox, Sites.ToString(Session["OppID"]), Sites.ToLong(Session["DomainID"]), strQueryString, Tax, dsCart);

                                                                if (strGoogleRedirect != "")
                                                                {
                                                                    strQueryString = ""; //This should be null otherwise it will redirect to Thank You Page.
                                                                    Response.Redirect(strGoogleRedirect, false);
                                                                }
                                                                else
                                                                {
                                                                    Page.MaintainScrollPositionOnPostBack = false;
                                                                    //Page.ClientScript.RegisterStartupScript(Page.ClientScript.GetType(), Page.ClientID, "resetScrollPosition();", true);
                                                                    ShowMessage(GetErrorMessage("ERR070"), 1);//There is a problem to redirect to Google Checkout.
                                                                }
                                                            }
                                                        }
                                                        #endregion
                                                    }
                                                    else
                                                    {
                                                        ShowMessage(GetErrorMessage("ERR061"), 1);
                                                        return;
                                                    }

                                                }
                                                else if (Sites.ToString(Session["PayOption"]) == "Paypal")
                                                {
                                                    #region Check for Paypal Configuration
                                                    UserAccess objUserAccess = new UserAccess();
                                                    objUserAccess.DomainID = Sites.ToLong(Session["DomainID"]);
                                                    objUserAccess.SiteID = Sites.ToLong(Session["SiteId"]);
                                                    string strPaypalUserName = "";
                                                    string strPaypalPassword = "";
                                                    string strPaypalSignature = "";
                                                    bool IsPaypalSandbox = true;


                                                    DataTable dtECommerceDetail = objUserAccess.GetECommerceDetails();

                                                    if (dtECommerceDetail != null)
                                                    {
                                                        strPaypalUserName = Sites.ToString(dtECommerceDetail.Rows[0]["vcPaypalUserName"]);
                                                        strPaypalPassword = Sites.ToString(dtECommerceDetail.Rows[0]["vcPaypalPassword"]);
                                                        strPaypalSignature = Sites.ToString(dtECommerceDetail.Rows[0]["vcPaypalSignature"]);
                                                        IsPaypalSandbox = Sites.ToBool(dtECommerceDetail.Rows[0]["IsPaypalSandbox"]);
                                                    }

                                                    if (strPaypalUserName == "" || strPaypalPassword == "" || strPaypalSignature == "")
                                                    {
                                                        IsPaypalConfigured = false;
                                                        ShowMessage(GetErrorMessage("ERR073"), 1);
                                                        return;
                                                    }
                                                    #endregion

                                                    if (IsPaypalConfigured)
                                                    {
                                                        #region Pay by Paypal Process 35141
                                                        //BACRM.BusinessLogic.ShioppingCart.GoogleCheckout objGoogleCheckout = new GoogleCheckout();
                                                        decimal OrderTotal = 0;
                                                        OrderTotal = GetCartTotalAmount();
                                                        string strPaypalRedirect = "";

                                                        DataSet dsCart = GetCartItem();
                                                        if (dsCart != null)
                                                        {
                                                            if (dsCart.Tables[0].Rows.Count > 0)
                                                            {

                                                                if (IsPaypalSandbox)
                                                                {
                                                                    PaypalServerUrl = Sites.ToString(System.Configuration.ConfigurationManager.AppSettings["PaypalSandboxUrl"]);
                                                                    // expresscheckout1.URL = "https://api-3t.sandbox.paypal.com/nvp"; //Test server URL

                                                                }
                                                                else
                                                                {
                                                                    PaypalServerUrl = Sites.ToString(System.Configuration.ConfigurationManager.AppSettings["PaypalServerUrl"]);
                                                                    // expresscheckout1.URL =  "https://www.paypal.com/cgi-bin/webscr"; //Paypal server URL
                                                                }
                                                                expresscheckout1.URL = PaypalServerUrl;
                                                                expresscheckout1.User = strPaypalUserName;
                                                                expresscheckout1.Password = strPaypalPassword;
                                                                expresscheckout1.Signature = strPaypalSignature;

                                                                //expresscheckout1.User = "jxavier.mca-facilitator_api1.gmail.com";
                                                                //expresscheckout1.Password = "1371651348";
                                                                //expresscheckout1.Signature = "AFcWxV21C7fd0v3bYYYRCpSSRl31A9aGptwdTWtuPS0gGenvixMLuh4Y";

                                                                expresscheckout1.OrderTotal = Sites.ToString(Math.Round(OrderTotal, 2));

                                                                expresscheckout1.TaxTotal = Sites.ToString(Math.Round(dcTotalTax, 2));
                                                                string currentpath = "http://" + Request.ServerVariables["SERVER_NAME"] + Request.ServerVariables["PATH_INFO"];
                                                                expresscheckout1.ReturnURL = currentpath + "?paypalpaymentstatus=paid";
                                                                expresscheckout1.CancelURL = currentpath + "?paypalpaymentstatus=cancelled";
                                                                expresscheckout1.PaymentAction = ExpresscheckoutPaymentActions.aSale;
                                                                expresscheckout1.BuyerEmail = txtPayPalEmail.Text;

                                                                for (int intCnt = 0; intCnt <= dsCart.Tables[0].Rows.Count - 1; intCnt++)
                                                                {
                                                                    PaymentItem PayItem = new PaymentItem();
                                                                    PayItem.Name = Sites.ToString(dsCart.Tables[0].Rows[intCnt]["vcItemName"]);
                                                                    PayItem.Amount = String.Format("{0:N2}", Math.Round(Sites.ToDouble(dsCart.Tables[0].Rows[intCnt]["monPrice"]), 2));
                                                                    PayItem.Quantity = Sites.ToInteger(dsCart.Tables[0].Rows[intCnt]["numUnitHour"]);
                                                                    PayItem.Description = Sites.ToString(dsCart.Tables[0].Rows[intCnt]["vcItemDesc"]);
                                                                    expresscheckout1.Items.Insert(intCnt, PayItem);
                                                                    //expresscheckout1.OrderTotal = Sites.ToString(Sites.ToDecimal(expresscheckout1.OrderTotal) + Sites.ToDecimal(Math.Round(Sites.ToDecimal(PayItem.Amount), 2)));
                                                                }

                                                                try
                                                                {
                                                                    expresscheckout1.SetCheckout();
                                                                }
                                                                catch (Exception ex)
                                                                {
                                                                    //ShowMessage(GetErrorMessage("ERR074"), 1); //Invalid Paypal User Email Or Invalid Merchant Paypal Account Configuration
                                                                    ShowMessage(ex.Message.ToString(), 1);
                                                                    return;
                                                                }

                                                                string responsetoken = expresscheckout1.ResponseToken;
                                                                Session["newtoken"] = responsetoken;
                                                                if (IsPaypalSandbox)
                                                                {
                                                                    PaypalRedirectServerUrl = Sites.ToString(System.Configuration.ConfigurationManager.AppSettings["PaypalRedirectSandboxUrl"]);
                                                                    //strPaypalRedirect = "https://www.sandbox.paypal.com/cgi-bin/webscr?cmd=_express-checkout&token=" + responsetoken;
                                                                }
                                                                else
                                                                {
                                                                    PaypalRedirectServerUrl = Sites.ToString(System.Configuration.ConfigurationManager.AppSettings["PaypalRedirectServerUrl"]);
                                                                    //strPaypalRedirect = "https://www.paypal.com/cgi-bin/webscr?cmd=_express-checkout&token=" + responsetoken;
                                                                }
                                                                strPaypalRedirect = PaypalRedirectServerUrl + responsetoken;
                                                                if (responsetoken != "")
                                                                {
                                                                    strQueryString = ""; //This should be null otherwise it will redirect to Thank You Page.
                                                                    Response.Redirect(strPaypalRedirect, false);
                                                                }
                                                                else
                                                                {
                                                                    strQueryString = "Sorry!., There is a problem in redirecting to Paypal..";
                                                                    Page.MaintainScrollPositionOnPostBack = false;
                                                                    ShowMessage(GetErrorMessage("ERR075"), 1);//There is a problem with Paypal Buyer Email Id account.
                                                                    return;
                                                                }
                                                            }
                                                        }
                                                        #endregion
                                                    }
                                                    else
                                                    {
                                                        ShowMessage(GetErrorMessage("ERR073"), 1); // Merchant Paypal Account Details Not Configured
                                                        return;
                                                    }
                                                }
                                                else if (Sites.ToString(Session["PayOption"]) == "SalesInquiry")
                                                {
                                                    MakeDepositeEntries();

                                                    ClearCartItems();//Clear existing cart
                                                    Session["ShippingMethodValue"] = null;
                                                    Session["PayOption"] = null;//PayOptio null after placing order.

                                                    if (Sites.ToString(Session["vcRedirectThankYouUrl"]) != "")
                                                    {
                                                        Response.Redirect("thankyou", false);
                                                    }
                                                    else
                                                    {
                                                        Response.Redirect("ThankYou.aspx?" + strQueryString + "&rt=3", false);
                                                    }
                                                }
                                                else
                                                {
                                                    //If in Case session Session["PayOption"] != "" and not from above Payment Options .
                                                    Page.MaintainScrollPositionOnPostBack = false;
                                                    //Page.ClientScript.RegisterStartupScript(Page.ClientScript.GetType(), Page.ClientID, "resetScrollPosition();", true);
                                                    ShowMessage(GetErrorMessage("ERR018"), 1);//Please select a payment option
                                                    bindHtml();
                                                    return;
                                                }
                                            }
                                            else
                                            {
                                                ClearCartItems();
                                            }



                                            if (strQueryString != "")
                                            {
                                                Session["ShippingMethodValue"] = null;
                                                Session["PayOption"] = null;//PayOptio null after placing order.
                                                if (Sites.ToString(Session["vcRedirectThankYouUrl"]) != "")
                                                {
                                                    Response.Redirect("thankyou", false);
                                                }
                                                else
                                                {
                                                    Response.Redirect("ThankYou.aspx?" + strQueryString + "&rt=0", false);
                                                }
                                            }
                                            #endregion
                                        }
                                        else
                                        {
                                            Page.MaintainScrollPositionOnPostBack = false;
                                            ShowMessage(GetErrorMessage("ERR072"), 1);
                                            //Error message for configuring coupon service in domain detail
                                        }
                                        #endregion
                                    }
                                    else
                                    {
                                        Page.MaintainScrollPositionOnPostBack = false;
                                        //Page.ClientScript.RegisterStartupScript(Page.ClientScript.GetType(), Page.ClientID, "resetScrollPosition();", true);
                                        ShowMessage(GetErrorMessage("ERR067"), 1);
                                    }
                                    #endregion
                                }
                            }
                            #endregion
                            #endregion
                        }
                        else
                        {
                            Page.MaintainScrollPositionOnPostBack = false;
                            //Page.ClientScript.RegisterStartupScript(Page.ClientScript.GetType(), Page.ClientID, "resetScrollPosition();", true);
                            ShowMessage("Select shipping method to place order.", 1);//Check for Shipping Service as a Line Item
                        }

                        #endregion
                    }
                    else
                    {
                        Page.MaintainScrollPositionOnPostBack = false;
                        //Page.ClientScript.RegisterStartupScript(Page.ClientScript.GetType(), Page.ClientID, "resetScrollPosition();", true);
                        ShowMessage(GetErrorMessage("ERR018"), 1);
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                if (CCommon.ToLong(hdnOrderPromotionID.Value) > 0)
                {
                    BACRM.BusinessLogic.ShioppingCart.Cart objcart = new BACRM.BusinessLogic.ShioppingCart.Cart();
                    HttpCookie cookie = Request.Cookies["Cart"];
                    objcart.DomainID = CCommon.ToLong(Session["DomainID"]);
                    objcart.ContactId = CCommon.ToLong(Session["UserContactID"]);
                    objcart.CookieId = (cookie != null) ? cookie["KeyId"] : "";
                    objcart.PromotionID = CCommon.ToLong(hdnOrderPromotionID.Value);
                    objcart.ClearCouponCodePromotion();
                }

                DeleteItemFromCart(Sites.ToLong(Session["ShippingServiceItemID"]));
                DeleteItemFromCart(Sites.ToLong(Session["DiscountServiceItemID"]));
                hdnOrderPromotionID.Value = "";
                hdnDiscountCodeID.Value = "";
                lblCouponCode.Text = "";
                txtCouponCode.Text = "";
                litApplyCouponCode.Visible = true;
                litUsedCouponCode.Visible = false;

                if (ex.Message.Contains("ITEM_PROMOTION_EXPIRED"))
                {
                    Page.MaintainScrollPositionOnPostBack = false;
                    ShowMessage(GetErrorMessage("ERR006"), 1);
                }
                else if (ex.Message.Contains("INVALID_COUPON_CODE_ITEM_PROMOTION"))
                {
                    Page.MaintainScrollPositionOnPostBack = false;
                    ShowMessage(GetErrorMessage("ERR011"), 1);
                }
                else if (ex.Message.Contains("ITEM_PROMOTION_COUPON_CODE_USAGE_LIMIT_EXCEEDED"))
                {
                    Page.MaintainScrollPositionOnPostBack = false;
                    ShowMessage(GetErrorMessage("ERR008"), 1);
                }
                else if (ex.Message.Contains("COUPON_CODE_REQUIRED_ITEM_PROMOTION"))
                {
                    Page.MaintainScrollPositionOnPostBack = false;
                    ShowMessage("Coupon code is required to use promotion.", 1);
                }
                else if (ex.Message.Contains("ORDER_PROMOTION_EXPIRED"))
                {
                    Page.MaintainScrollPositionOnPostBack = false;
                    ShowMessage(GetErrorMessage("ERR006"), 1);
                }
                else if (ex.Message.Contains("INVALID_COUPON_CODE_ORDER_PROMOTION"))
                {
                    Page.MaintainScrollPositionOnPostBack = false;
                    ShowMessage(GetErrorMessage("ERR011"), 1);
                }
                else if (ex.Message.Contains("ORDER_PROMOTION_COUPON_CODE_USAGE_LIMIT_EXCEEDED"))
                {
                    Page.MaintainScrollPositionOnPostBack = false;
                    ShowMessage(GetErrorMessage("ERR008"), 1);
                }
                else if (ex.Message.Contains("COUPON_CODE_REQUIRED_ORDER_PROMOTION"))
                {
                    Page.MaintainScrollPositionOnPostBack = false;
                    ShowMessage(GetErrorMessage("Coupon code is required to use promotion."), 1);
                }
                else
                {
                    ExceptionModule.ExceptionPublish(ex, Sites.ToLong(Session["DomainID"]), Sites.ToLong(Session["UserContactID"]), Sites.ToLong(Session["SiteId"]), Request);
                    Page.MaintainScrollPositionOnPostBack = false;
                    //Page.ClientScript.RegisterStartupScript(Page.ClientScript.GetType(), Page.ClientID, "resetScrollPosition();", true);
                    ShowMessage(ex.ToString(), 1);
                }
            }
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            foreach (GridViewRow row in gvPromotionPostCheckOut.Rows)
            {
                int RowIndex = row.RowIndex;

                CheckBox chkAdd = (CheckBox)row.FindControl("chkAdd");
                if (chkAdd.Checked == true)
                {
                    long _itemCode = CCommon.ToLong(gvPromotionPostCheckOut.DataKeys[RowIndex].Values["numItemCode"]);
                    long parentItemCode = CCommon.ToLong(gvPromotionPostCheckOut.DataKeys[RowIndex].Values["numParentItemCode"]);

                    TextBox txtQty = (TextBox)row.FindControl("txtQty");
                    int _qty = CCommon.ToInteger(txtQty.Text);

                    AddToCartFromEcomm("", _itemCode, 0, "", false, _qty, parentItemCode: parentItemCode);
                }
            }

            hdnIsFinishedPostCheckout.Value = "True";
            dialogSellPopup.Visible = false;
        }

        protected void btnNoThanks_Click(object sender, EventArgs e)
        {
            hdnIsFinishedPostCheckout.Value = "True";
            dialogSellPopup.Visible = false;
        }

        protected void btnOrders_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect("Orders.aspx");
            }
            catch (Exception ex)
            {
                ExceptionModule.ExceptionPublish(ex, Sites.ToLong(Session["DomainID"]), Sites.ToLong(Session["UserContactID"]), Sites.ToLong(Session["SiteId"]), Request);
                Page.MaintainScrollPositionOnPostBack = false;
                //Page.ClientScript.RegisterStartupScript(Page.ClientScript.GetType(), Page.ClientID, "resetScrollPosition();", true);
                ShowMessage(ex.ToString(), 1);
            }
        }

        protected void ddlShippingMethod_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                DataSet dsTemp = GetCartItem();
                if (Sites.ToLong(Session["OppID"]) > 0)
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, Page.GetType(), "AlertShipping", "alert('You can not change shipping method because order is already created.');", true);
                }
                else
                {
                    isShippingMethodNeeded = false;

                    if (Sites.ToDouble(GetShippingCharge()) == -1)
                    {
                        ddlShippingMethod.ClearSelection();
                        ddlShippingMethod.SelectedIndex = 0;
                        //Session["ShippingMethodValue"] = ddlShippingMethod.SelectedItem.Value;
                    }
                    else
                    {
                        Session["ShippingMethodValue"] = ddlShippingMethod.SelectedItem.Value;
                        DeleteItemFromCart(Sites.ToLong(Session["ShippingServiceItemID"]));
                    }
                }
                bindHtml();
            }
            catch (Exception ex)
            {
                ExceptionModule.ExceptionPublish(ex, Sites.ToLong(Session["DomainID"]), Sites.ToLong(Session["UserContactID"]), Sites.ToLong(Session["SiteId"]), Request);
                Page.MaintainScrollPositionOnPostBack = false;
                //Page.ClientScript.RegisterStartupScript(Page.ClientScript.GetType(), Page.ClientID, "resetScrollPosition();", true);
                ShowMessage(ex.ToString(), 1);
            }
        }

        protected void lbtnEdit_Click(object sender, EventArgs e)
        {
            if (Sites.ToBool(Session["IsOnePageCheckout"]))
            {
                Session["SelectedStep"] = 1;
                Response.Redirect("/OnePageCheckout.aspx", true);
            }
            else
            {
                Response.Redirect("/ConfirmAddress.aspx?Return=OrderSummary.aspx", true);
            }
        }

        protected void lbtnCustomerInformationEdit_Click(object sender, EventArgs e)
        {
            if (Sites.ToBool(Session["IsOnePageCheckout"]))
            {
                //<a href="/CustomerInformation.aspx?Return=OrderSummary.aspx">edit</a>
                Session["SelectedStep"] = 1;
                Response.Redirect("/CustomerInformation.aspx?Return=OnePageCheckout.aspx", true);
            }
            else
            {
                Response.Redirect("/CustomerInformation.aspx?Return=OrderSummary.aspx", true);
            }
        }

        protected void lbtnCartEdit_Click(object sender, EventArgs e)
        {
            if (Sites.ToBool(Session["IsOnePageCheckout"]))
            {
                //<a href="/Cart.aspx?Return=OrderSummary.aspx">edit</a>
                Session["SelectedStep"] = 1;
                Response.Redirect("/Cart.aspx?Return=OnePageCheckout.aspx", true);
            }
            else
            {
                Response.Redirect("/Cart.aspx?Return=OrderSummary.aspx", true);
            }
        }

        /*protected void gvPromotionPostCheckOut_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "Add")
                {
                    int index = Convert.ToInt32(e.CommandArgument);

                    AddToCartFromEcomm(hdnCategory.Value, index, 0, "", false, Sites.ToInteger(hdnSQty.Text));
                    GridViewRow gvr = (GridViewRow)(((Button)e.CommandSource).NamingContainer);
                    int RowIndex = gvr.RowIndex;
                    Button btnAdd = (Button)gvPromotionPostCheckOut.Rows[RowIndex].FindControl("btnAdd");
                    int parentItemCode = Convert.ToInt32(gvPromotionPostCheckOut.DataKeys[RowIndex].Values["numParentItemCode"]);

                    if (btnAdd.Text != "Added")
                    {
                        AddToCartFromEcomm(hdnCategory.Value, index, 0, "", false, Sites.ToInteger(hdnSQty.Text), parentItemCode: parentItemCode);

                        btnAdd.Text = "Added";
                        btnAdd.Enabled = false;
                    }
                    //gvPromotionPostCheckOut.Rows[RowIndex].Visible = false;
                    //int numVisible = 0;
                    //foreach (GridViewRow row in gvPromotionPostCheckOut.Rows)
                    //{
                    //    if (row.Visible == true)
                    //    {
                    //        numVisible += 1;
                    //    }
                    //}

                    //if (numVisible == 0)
                    //{
                    //    dialogSellPopup.Visible = false;
                    //}
                }
            }
            catch (Exception ex)
            {
                ExceptionModule.ExceptionPublish(ex, Sites.ToLong(Session["DomainID"]), Sites.ToLong(Session["UserContactID"]), Sites.ToLong(Session["SiteId"]), Request);
                ShowMessage(ex.ToString(), 1);
            }
        }*/

        protected void gvPromotionPostCheckOut_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    //Button btnAdd = e.Row.FindControl("btnAdd") as Button;
                    CheckBox chkAdd = e.Row.FindControl("chkAdd") as CheckBox;
                    Label lblOutOfStock = e.Row.FindControl("lblOutOfStock") as Label;

                    DataRowView drv = e.Row.DataItem as DataRowView;
                    if (drv != null)
                    {
                        TextBox txtQty = (TextBox)e.Row.FindControl("txtQty");
                        int qty = CCommon.ToInteger(txtQty.Text);
                        decimal price = CCommon.ToDecimal(drv.Row["monListPrice"]);
                        decimal total = CCommon.ToDecimal(qty * price);
                        Label lblTotal = (Label)e.Row.FindControl("lblTotal");
                        lblTotal.Text = CCommon.ToString(Session["CurrSymbol"]) + " " + CCommon.ToString(Math.Round(total, 2));

                        TableCell monListPriceCell = e.Row.Cells[5];
                        monListPriceCell.Text = CCommon.ToString(Session["CurrSymbol"]) + " " + CCommon.ToString(Math.Round(price, 2));

                        //hdnCategory.Value = CCommon.ToString(drv.Row["vcCategoryName"]);

                        int _numOnHand = CCommon.ToInteger(drv.Row["numOnHand"]);
                        if (_numOnHand == 0)
                        {
                            //btnAdd.Visible = false;
                            chkAdd.Visible = false;
                            lblOutOfStock.Visible = true;
                        }

                        Image img = (Image)e.Row.FindControl("imgPath");
                        string imgPath = img.ImageUrl;
                        if (!string.IsNullOrEmpty(imgPath))
                        {
                            imgPath = GetImagePath(CCommon.ToString(imgPath));
                            img.ImageUrl = imgPath;
                        }
                        else
                        {
                            img.ImageUrl = "../Images/DefaultProduct.png";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionModule.ExceptionPublish(ex, Sites.ToLong(Session["DomainID"]), Sites.ToLong(Session["UserContactID"]), Sites.ToLong(Session["SiteId"]), Request);
                ShowMessage(ex.ToString(), 1);
            }
        }

        protected void btnCouponCode_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtCouponCode.Text.Length > 0)
                {
                    DiscountCodes objDiscountCodes = new DiscountCodes();
                    objDiscountCodes.DomainID = Sites.ToLong(Session["DomainID"]);
                    objDiscountCodes.DivisionID = Sites.ToInteger(Session["DivId"]);
                    objDiscountCodes.DiscountCode = txtCouponCode.Text.Trim();
                    DataTable dtDiscount = objDiscountCodes.ValidateDiscountCode();

                    if (dtDiscount != null && dtDiscount.Rows.Count > 0)
                    {
                        hdnOrderPromotionID.Value = CCommon.ToString(dtDiscount.Rows[0]["numPromotionID"]);
                        hdnDiscountCodeID.Value = CCommon.ToString(dtDiscount.Rows[0]["numDisocuntID"]);

                        litApplyCouponCode.Visible = false;
                        txtCouponCode.Text = "";
                        lblCouponCode.Text = CCommon.ToString(dtDiscount.Rows[0]["vcCouponCode"]);
                        litUsedCouponCode.Visible = true;

                        if (!CCommon.ToBool(dtDiscount.Rows[0]["bitUseForCouponManagement"]))
                        {
                            AddOrderPromotionDiscountIfAvailable();
                        }

                        BACRM.BusinessLogic.ShioppingCart.Cart objcart = new BACRM.BusinessLogic.ShioppingCart.Cart();
                        HttpCookie cookie = Request.Cookies["Cart"];
                        objcart.DomainID = CCommon.ToLong(Session["DomainID"]);
                        objcart.ContactId = CCommon.ToLong(Session["UserContactID"]);
                        objcart.CookieId = (cookie != null) ? cookie["KeyId"] : "";
                        objcart.vcCoupon = CCommon.ToString(dtDiscount.Rows[0]["vcCouponCode"]);
                        objcart.ApplyCouponCodePromotion();
                    }
                }
                else
                {
                    Page.MaintainScrollPositionOnPostBack = false;
                    ShowMessage(GetErrorMessage("ERR005"), 1);//Enter Coupon Code
                }
            }
            catch (Exception ex)
            {
                hdnOrderPromotionID.Value = "";
                hdnDiscountCodeID.Value = "";
                lblCouponCode.Text = "";
                txtCouponCode.Text = "";
                litApplyCouponCode.Visible = true;
                litUsedCouponCode.Visible = false;

                if (ex.Message.Contains("INVALID_COUPON_CODE"))
                {
                    Page.MaintainScrollPositionOnPostBack = false;
                    ShowMessage(GetErrorMessage("ERR011"), 1);
                }
                else if (ex.Message.Contains("COUPON_CODE_EXPIRED"))
                {
                    Page.MaintainScrollPositionOnPostBack = false;
                    ShowMessage(GetErrorMessage("ERR006"), 1);
                }
                else if (ex.Message.Contains("COUPON_USAGE_LIMIT_EXCEEDED"))
                {
                    Page.MaintainScrollPositionOnPostBack = false;
                    ShowMessage(GetErrorMessage("ERR008"), 1);
                }
                else
                {
                    ExceptionModule.ExceptionPublish(ex, Sites.ToLong(Session["DomainID"]), Sites.ToLong(Session["UserContactID"]), Sites.ToLong(Session["SiteId"]), Request);
                    Page.MaintainScrollPositionOnPostBack = false;
                    ShowMessage(ex.ToString(), 1);
                }
            }
        }

        protected void lbCouponCodeRemove_Click(object sender, EventArgs e)
        {
            try
            {
                if (CCommon.ToLong(hdnOrderPromotionID.Value) > 0)
                {
                    BACRM.BusinessLogic.ShioppingCart.Cart objcart = new BACRM.BusinessLogic.ShioppingCart.Cart();
                    HttpCookie cookie = Request.Cookies["Cart"];
                    objcart.DomainID = CCommon.ToLong(Session["DomainID"]);
                    objcart.ContactId = CCommon.ToLong(Session["UserContactID"]);
                    objcart.CookieId = (cookie != null) ? cookie["KeyId"] : "";
                    objcart.PromotionID = CCommon.ToLong(hdnOrderPromotionID.Value);
                    objcart.ClearCouponCodePromotion();
                }

                DeleteItemFromCart(Sites.ToLong(Session["DiscountServiceItemID"]));

                hdnOrderPromotionID.Value = "";
                hdnDiscountCodeID.Value = "";
                lblCouponCode.Text = "";
                txtCouponCode.Text = "";

                litApplyCouponCode.Visible = true;
                litUsedCouponCode.Visible = false;



                this.isShippingMethodNeeded = true;
                bindHtml();
            }
            catch (Exception ex)
            {
                ExceptionModule.ExceptionPublish(ex, Sites.ToLong(Session["DomainID"]), Sites.ToLong(Session["UserContactID"]), Sites.ToLong(Session["SiteId"]), Request);
                Page.MaintainScrollPositionOnPostBack = false;
                ////Page.ClientScript.RegisterStartupScript(Page.ClientScript.GetType(), Page.ClientID, "resetScrollPosition();", true);
                ShowMessage(ex.ToString(), 1);
            }
        }

        protected void OnPageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                DataTable dt = new DataTable();
                gvPromotionPostCheckOut.PageIndex = e.NewPageIndex;
                if (Session["dtItemPostCheckOutPopup"] != null)
                {
                    dt = (DataTable)Session["dtItemPostCheckOutPopup"];
                }
                gvPromotionPostCheckOut.DataSource = dt;
                gvPromotionPostCheckOut.DataBind();
                dialogSellPopup.Visible = true;
                this.Page.ClientScript.RegisterStartupScript(this.GetType(), "ShowMiniPostSellCart", "ShowMiniPostSellCart();", true);
            }
            catch (Exception ex)
            {
                ExceptionModule.ExceptionPublish(ex, Sites.ToLong(Session["DomainID"]), Sites.ToLong(Session["UserContactID"]), Sites.ToLong(Session["SiteId"]), Request);
                Page.MaintainScrollPositionOnPostBack = false;
                ////Page.ClientScript.RegisterStartupScript(Page.ClientScript.GetType(), Page.ClientID, "resetScrollPosition();", true);
                ShowMessage(ex.ToString(), 1);
            }
        }

        protected void btnYesThanks_Click(object sender, EventArgs e)
        {
            DataTable dt = new DataTable();
            for (int i = 0; i < gvItemsForRule.Columns.Count; i++)
            {
                dt.Columns.Add("column" + i.ToString());
            }
            foreach (GridViewRow row in gvItemsForRule.Rows)
            {
                DataRow dr = dt.NewRow();
                for (int j = 0; j < gvItemsForRule.Columns.Count; j++)
                {
                    dr["column" + j.ToString()] = row.Cells[j].Text;
                }

                dt.Rows.Add(dr);
            }
            decimal _discount = 0;
            foreach (DataRow rw in dt.Rows)
            {
                _discount = _discount + CCommon.ToDecimal(rw["column1"]);
                AddToCartFromEcomm("", CCommon.ToLong(rw["column0"]), 0, "", false, 1, CCommon.ToDecimal(rw["column1"]));
            }
            if (_discount > 0)
            {
                decimal _newDiscount = CCommon.ToDecimal(Session["TotalDiscount"]);
                _newDiscount = _newDiscount + _discount;
                Session["TotalDiscount"] = _newDiscount;
            }
            hdnIsFinishedPostCheckout.Value = "True";
            dialogSellPopup.Visible = false;
        }

        protected void btnNo_Click(object sender, EventArgs e)
        {
            hdnIsFinishedPostCheckout.Value = "True";
            dialogSellPopup.Visible = false;
        }
        #endregion

        #region Methods

        private DataTable CheckIfMinOrderAmountRuleMeets(decimal _totalAmount)
        {
            try
            {
                MOpportunity objOpportunity = new MOpportunity();
                objOpportunity.DomainID = CCommon.ToLong(Session["DomainID"]);
                objOpportunity.DivisionID = CCommon.ToLong(lngDivID);
                return objOpportunity.CheckIfMinOrderAmountRuleMeets(_totalAmount);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        /*private DataTable RelatedItemsPromotion(int numItemCode, long numWarehouseItemID)
        {
            CItems objItems = new CItems();
            objItems.ItemCode = numItemCode;
            objItems.DomainID = CCommon.ToLong(Session["DomainID"]);
            objItems.WareHouseItemID = numWarehouseItemID;
            objItems.numSiteId = CCommon.ToLong(HttpContext.Current.Session["SiteID"]);
            DataTable dt = objItems.GetPromotionsForSimilarItems();
            return dt;
        }*/

        private void bindPostCheckout(DataTable dtCart, CItems objItem)
        {
            string strNumItemCode = string.Empty;
            DataTable dtIncentiveSaleItems = new DataTable();
            if (dtCart != null && dtCart.Rows.Count > 0)
            {
                decimal _cartTotalAmount = GetCartTotalAmount();
                foreach (DataRow r in dtCart.Rows)
                {
                    strNumItemCode = strNumItemCode + "," + CCommon.ToString(r["numItemCode"]);
                }
                strNumItemCode = strNumItemCode.Substring(1);
                if (_cartTotalAmount > 0 && !string.IsNullOrEmpty(strNumItemCode))
                {
                    dtIncentiveSaleItems = objItem.GetPromotionOfferForIncentiveSaleItems(_cartTotalAmount, strNumItemCode);
                    decimal returnValue = CCommon.ToInteger(dtIncentiveSaleItems.Rows[0]["SubTotal"]);

                    if (dtIncentiveSaleItems != null && dtIncentiveSaleItems.Rows.Count > 0 && returnValue > 0)
                    {
                        LoadFlashDivGrid(CCommon.ToLong(dtIncentiveSaleItems.Rows[0]["PromotionID"]), returnValue);
                        flashPage.Visible = true;
                        dialogSellPopup.Visible = false;
                        this.Page.ClientScript.RegisterStartupScript(this.GetType(), "ShowFreePostSellItems", "ShowFreePostSellItems();", true);
                    }
                    else if (returnValue == 0 || returnValue == -1)
                    {
                        var distinctRows = (from DataRow dRow in dtCart.Rows
                                            where CCommon.ToString(dRow["numParentItemCode"]) == "" || dRow["numParentItemCode"] == null
                                            select new { col1 = dRow["numItemCode"], col2 = dRow["numWarehouseid"] }).Distinct();

                        DataTable dtItemPostCheckOutPopup = new DataTable();
                        DataTable dtTemp = new DataTable();
                        if (distinctRows.Count() > 0)
                        {
                            foreach (var row in distinctRows)
                            {
                                long _numParentItemCode = CCommon.ToLong(row.col1);
                                long _numWarehouseId = CCommon.ToInteger(row.col2);

                                if (_numParentItemCode != 0 && _numWarehouseId != 0)
                                {
                                    DataTable dtPromotions = BindPromotions(CCommon.ToInteger(_numParentItemCode), CCommon.ToLong(_numWarehouseId));
                                    if (dtPromotions != null && dtPromotions.Rows.Count > 0)
                                    {
                                        divPromotion.Visible = true;
                                        lblVcPromotion.Text = CCommon.ToString(dtPromotions.Rows[0]["vcPromotionDescription"]);
                                    }
                                }

                                objItem.ParentItemCode = _numParentItemCode;
                                dtTemp = objItem.GetSimilarItem();
                                if (dtCart.Rows.Count > 0 && dtTemp.Rows.Count > 0)
                                {
                                    DataRow[] findRows = dtCart.Select("numItemCode = " + CCommon.ToLong(dtTemp.Rows[0]["numItemCode"]));
                                    if (findRows.Count() <= 0)
                                    {
                                        if (dtTemp.Rows.Count > 0)
                                        {
                                            if (dtItemPostCheckOutPopup.Rows.Count > 0)
                                            {
                                                dtItemPostCheckOutPopup.Merge(dtTemp);
                                            }
                                            else
                                            {
                                                dtItemPostCheckOutPopup = dtTemp.Copy();
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        if (dtItemPostCheckOutPopup != null && dtItemPostCheckOutPopup.Rows.Count > 0)
                        {
                            #region Commented
                            /*DataTable dtPromotion = new DataTable();
                            foreach (DataRow dr in dtItemPostCheckOutPopup.Rows)
                            {
                                DataTable dt = RelatedItemsPromotion(CCommon.ToInteger(dr["numItemCode"]), CCommon.ToLong(dr["numWareHouseItemID"]));
                                if (dt != null && dt.Rows.Count > 0)
                                {
                                    dtPromotion = dt.Copy();
                                }
                            }

                            if (dtPromotion != null && dtPromotion.Rows.Count > 0)
                            {
                                DataTable dtItemPost = new DataTable();
                                var _proId = (from DataRow dRow in dtPromotion.Rows
                                              select new { p1 = dRow["numProId"] }).Distinct();

                                if (_proId.Count() > 0)
                                {
                                    foreach (var row in _proId)
                                    {
                                        long _numProId = CCommon.ToLong(row.p1);

                                        dtItemPost = (from DataRow dr in dtItemPostCheckOutPopup.Rows
                                                      where CCommon.ToLong(dr["numProId"]) == _numProId
                                                      select dr).CopyToDataTable();
                                    }
                                }
                                Session["dtItemPost"] = dtItemPost;
                                gvPromotionPostCheckOut.DataSource = dtItemPost;
                                gvPromotionPostCheckOut.DataBind();
                                dialogSellPopup.Visible = true;
                            }
                            else
                            {
                                hdnIsFinishedPostCheckout.Value = "True";
                                dialogSellPopup.Visible = false;
                                btnCharge_Click(null, null);
                            }*/
                            #endregion
                            Session["dtItemPostCheckOutPopup"] = dtItemPostCheckOutPopup;
                            gvPromotionPostCheckOut.DataSource = dtItemPostCheckOutPopup;
                            gvPromotionPostCheckOut.DataBind();
                            dialogSellPopup.Visible = true;
                            this.Page.ClientScript.RegisterStartupScript(this.GetType(), "ShowRelatedPostSellItems", "ShowRelatedPostSellItems();", true);
                        }
                        else
                        {
                            hdnIsFinishedPostCheckout.Value = "True";
                            dialogSellPopup.Visible = false;
                            btnCharge_Click(null, null);
                        }
                    }
                }
                else
                {
                    hdnIsFinishedPostCheckout.Value = "True";
                    btnCharge_Click(null, null);
                }
            }
        }

        /*Function added by Neelam on 10/11/2017 - Added functionality to bind Promotions*/
        private DataTable BindPromotions(int numItemCode, long numWarehouseItemID)
        {
            CItems objItems = new CItems();
            objItems.ItemCode = numItemCode;
            objItems.DomainID = CCommon.ToLong(Session["DomainID"]);
            objItems.WareHouseItemID = numWarehouseItemID;
            objItems.SiteID = CCommon.ToLong(HttpContext.Current.Session["SiteID"]);
            DataTable dt = objItems.GetPromotionsForSimilarItems();
            return dt;
        }

        private void LoadFlashDivGrid(long _promotionID, decimal _subTotalReturned)
        {
            try
            {
                PromotionOffer objPromotionOffer = new PromotionOffer();
                objPromotionOffer.PromotionID = _promotionID;
                objPromotionOffer.DomainID = Sites.ToLong(Session["DomainID"]);
                objPromotionOffer.SiteID = CCommon.ToLong(HttpContext.Current.Session["SiteID"]);
                DataSet ds = objPromotionOffer.GetPromotionOfferOrderBasedItems();

                DataTable dtItems = ds.Tables[2];

                if (dtItems != null && dtItems.Rows.Count > 0)
                {
                    DataTable dtClone = new DataTable();

                    dtClone = (from DataRow dr in dtItems.Rows
                               where CCommon.ToDecimal(dr["fltSubTotal"]) == _subTotalReturned
                               select dr).CopyToDataTable();

                    gvItemsForRule.DataSource = dtClone;
                    gvItemsForRule.DataBind();
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        #region HTML

        private void bindHtml()
        {
            try
            {
                string strUI = GetUserControlTemplate("OrderSummary.htm");


                if (string.IsNullOrEmpty(strUI))
                {
                    throw new Exception("Not able to retrive html");
                }


                if (Sites.ToLong(Session["UserContactID"]) == 0) return;

                /*-------------Contact Information------------------- */
                CContacts objContacts = new CContacts();
                DataTable dtTable1 = default(DataTable);
                objContacts.ContactID = Sites.ToLong(Session["UserContactID"]);
                objContacts.DomainID = Sites.ToLong(Session["DomainID"]);
                dtTable1 = objContacts.GetBillOrgorContAdd();

                decimal TotalWeight = 0;
                decimal TotalCartAmount = 0;
                int CartItemsCount = 0;
                bool IsAllCartItemsFreeShipping = true;

                strUI = strUI.Replace("##FirstName##", Sites.ToString(dtTable1.Rows[0]["vcFirstname"]));
                strUI = strUI.Replace("##LastName##", Sites.ToString(dtTable1.Rows[0]["vcLastname"]));
                strUI = strUI.Replace("##Phone##", Sites.ToString(dtTable1.Rows[0]["vcPhone"]));
                strUI = strUI.Replace("##Email##", Sites.ToString(dtTable1.Rows[0]["vcEmail"]));
                strUI = strUI.Replace("##FileUpload##", CCommon.RenderControl(FluDocument));

                strUI = strUI.Replace("##ShipVia##", CCommon.RenderControl(ddlShipVia));

                strUI = strUI.Replace("##CouponCodeTextBox##", CCommon.RenderControl(txtCouponCode));
                strUI = strUI.Replace("##ApplyCouponCodeButton##", CCommon.RenderControl(btnCouponCode));
                strUI = strUI.Replace("##RemoveCouponCodeLink##", CCommon.RenderControl(lbCouponCodeRemove));
                strUI = strUI.Replace("##CouponCodeLabel##", lblCouponCode.Text);
                /*-------------Address Information------------------- */
                DataTable dtTable = null;
                CContacts objContact = new CContacts();
                objContact.DomainID = Sites.ToLong(Session["DomainID"]);
                objContact.AddressID = Sites.ToLong(Session["BillAddress"]);
                objContact.byteMode = 1;
                dtTable = objContact.GetAddressDetail();


                if (dtTable.Rows.Count > 0)
                    strUI = strUI.Replace("##BillingAddress##", Sites.ToString(dtTable.Rows[0]["vcFullAddress"]).Replace("<pre>", "").Replace("</pre>", "").Replace("<br>", ""));
                else
                    strUI = strUI.Replace("##BillingAddress##", "");

                objContact.DomainID = Sites.ToLong(Session["DomainID"]);
                objContact.AddressID = Sites.ToLong(Session["ShipAddress"]);
                objContact.byteMode = 1;
                dtTable = objContact.GetAddressDetail();

                if (dtTable.Rows.Count > 0)
                    strUI = strUI.Replace("##ShippingAddress##", Sites.ToString(dtTable.Rows[0]["vcFullAddress"]).Replace("<pre>", "").Replace("</pre>", "").Replace("<br>", ""));
                else
                    strUI = strUI.Replace("##ShippingAddress##", "");

                /*-------------Shipping Method------------------- */
                string pattern = "<##FOR(?<Property>(\\b[^>]*?))##>(?<Content>([\\s\\S]*?))<##ENDFOR##>";
                string RepeteTemplate = "";
                Hashtable htProperties;
                DataTable dtItems = default(DataTable);
                if (Regex.IsMatch(strUI, pattern))
                {
                    MatchCollection matches = Regex.Matches(strUI, pattern);
                    if ((matches.Count > 0))
                    {

                        RepeteTemplate = matches[0].Groups["Content"].Value;
                        htProperties = GetPropertiesAsHastable(matches[0].Groups["Property"].Value.Replace("(", "").Replace(")", ""));

                        StringBuilder sb = new StringBuilder();
                        DataSet ds = new DataSet();
                        ds = GetCartItem();


                        if (ds != null && ds.Tables.Count > 0)
                        {
                            dtItems = ds.Tables[0];
                            if (dtItems.Rows.Count > 0)
                            {
                                string strTemp = string.Empty;
                                int RowsCount = 0;

                                foreach (DataRow dr in dtItems.Rows)
                                {
                                    strTemp = RepeteTemplate;
                                    if (!string.IsNullOrEmpty(CCommon.ToString(dr["vcPathForTImage"])))
                                    {
                                        strTemp = strTemp.Replace("##ImageURL##", GetImagePath(CCommon.ToString(dr["vcPathForTImage"])));
                                    }
                                    else
                                    {
                                        strTemp = strTemp.Replace("##ImageURL##", "../Images/DefaultProduct.png");
                                    }
                                    strTemp = strTemp.Replace("##ItemURL##", Sites.ToString(dr["ItemURL"]));
                                    if (Sites.ToString(dr["vcItemName"]).Length > 100)
                                    {
                                        strTemp = strTemp.Replace("##ItemName##", Sites.ToString(dr["vcItemName"]).Substring(0, Sites.ToInteger(Sites.ToString(dr["vcItemName"]).Length / 2)));
                                    }
                                    else
                                    {
                                        strTemp = strTemp.Replace("##ItemName##", Sites.ToString(dr["vcItemName"]).Substring(0, Sites.ToString(dr["vcItemName"]).Length));
                                    }

                                    strTemp = strTemp.Replace("##ItemUnits##", Sites.ToString(dr["numUnitHour"]));

                                    if (Sites.ToBool(Session["HidePrice"]) == false)
                                    {
                                        strTemp = strTemp.Replace("##ItemPricePerUnit##", Sites.ToString(Session["CurrSymbol"]) + " " + string.Format("{0:#,##0.00###}", dr["monPrice"]));
                                        strTemp = strTemp.Replace("##TotalAmount##", Sites.ToString(Session["CurrSymbol"]) + " " + string.Format("{0:#,##0.00}", dr["monTotAmount"]));
                                    }
                                    else
                                    {
                                        strTemp = strTemp.Replace("##ItemPricePerUnit##", "NA");
                                        strTemp = strTemp.Replace("##TotalAmount##", "NA");
                                    }

                                    if (Sites.ToBool(dr["bitDiscountType"]))
                                    {
                                        strTemp = strTemp.Replace("##Discount##", Sites.ToString(Session["CurrSymbol"]) + " " + string.Format("{0:#,##0.00}", dr["fltDiscount"]));
                                    }
                                    else
                                    {
                                        strTemp = strTemp.Replace("##Discount##", Sites.ToString(Session["CurrSymbol"]) + " " + string.Format("{0:#,##0.00}", Sites.ToDouble(dr["monTotAmtBefDiscount"]) - Sites.ToDouble(dr["monTotAmount"])));
                                    }
                                    strTemp = strTemp.Replace("##ItemModelID##", CCommon.ToString(dr["vcModelID"]));
                                    strTemp = strTemp.Replace("##ItemID##", CCommon.ToString(dr["numItemCode"]));
                                    strTemp = strTemp.Replace("##ItemAttributes##", CCommon.ToString(dr["vcAttributes"]));
                                    strTemp = strTemp.Replace("##SKU##", CCommon.ToString(dr["vcSKU"]));

                                    // strTemp = strTemp.Replace("##ShippingMethod##", Sites.ToString(dr["vcShippingMethod"]));
                                    if (Sites.ToString(dr["bitFreeShipping"]) == "True")
                                    {
                                        TotalWeight = TotalWeight + 0;
                                        TotalCartAmount = TotalCartAmount + 0;
                                        CartItemsCount = CartItemsCount + 0;
                                    }
                                    else // shipping based on shipping rule 
                                    {
                                        TotalWeight = TotalWeight + Sites.ToDecimal(dr["numWeight"]);
                                        TotalCartAmount = TotalCartAmount + Sites.ToDecimal(dr["monTotAmount"]);
                                        CartItemsCount = CartItemsCount + Sites.ToInteger(dr["numUnitHour"]);
                                        IsAllCartItemsFreeShipping = false;
                                    }

                                    if (RowsCount % 2 == 0)
                                        strTemp = strTemp.Replace("##RowClass##", Sites.ToString(htProperties["AlternatingItemClass"]));
                                    else
                                        strTemp = strTemp.Replace("##RowClass##", Sites.ToString(htProperties["ItemClass"]));

                                    sb.Append(strTemp);

                                    RowsCount++;
                                    dtItems.AcceptChanges();
                                }
                            }
                            strUI = Regex.Replace(strUI, pattern, sb.ToString());
                        }
                    }
                }

                if (CCommon.ToLong(hdnOrderPromotionID.Value) > 0 && CCommon.ToLong(hdnDiscountCodeID.Value) > 0)
                {
                    litUsedCouponCode.Visible = true;
                    litApplyCouponCode.Visible = false;
                }
                else
                {
                    litUsedCouponCode.Visible = false;
                    litApplyCouponCode.Visible = true;
                }

                string EnterCouponPattern, UsedCouponPattern;
                EnterCouponPattern = "{##EnterCouponSection##}(?<Content>([\\s\\S]*?)){/##EnterCouponSection##}";
                UsedCouponPattern = "{##UsedCouponSection##}(?<Content>([\\s\\S]*?)){/##UsedCouponSection##}";

                if (Regex.IsMatch(strUI, EnterCouponPattern))
                {
                    MatchCollection matches = Regex.Matches(strUI, EnterCouponPattern);
                    string applyCoupconCodeTemplate = matches[0].Groups["Content"].Value;

                    applyCoupconCodeTemplate = applyCoupconCodeTemplate.Replace("##CouponCodeTextBox##", CCommon.RenderControl(txtCouponCode));
                    applyCoupconCodeTemplate = applyCoupconCodeTemplate.Replace("##ApplyCouponCodeButton##", CCommon.RenderControl(btnCouponCode));

                    litApplyCouponCode.Text = applyCoupconCodeTemplate;

                    strUI = Regex.Replace(strUI, EnterCouponPattern, CCommon.RenderControl(litApplyCouponCode));
                }
                else
                {
                    strUI = Sites.RemoveMatchedPattern(strUI, EnterCouponPattern);
                }

                if (Regex.IsMatch(strUI, UsedCouponPattern))
                {
                    MatchCollection matches = Regex.Matches(strUI, UsedCouponPattern);
                    string usedCoupconCodeTemplate = matches[0].Groups["Content"].Value;

                    usedCoupconCodeTemplate = usedCoupconCodeTemplate.Replace("##CouponCodeLabel##", CCommon.RenderControl(lblCouponCode));
                    usedCoupconCodeTemplate = usedCoupconCodeTemplate.Replace("##RemoveCouponCodeLink##", CCommon.RenderControl(lbCouponCodeRemove));

                    litUsedCouponCode.Text = usedCoupconCodeTemplate;

                    strUI = Regex.Replace(strUI, UsedCouponPattern, CCommon.RenderControl(litUsedCouponCode));
                }
                else
                {
                    strUI = Sites.RemoveMatchedPattern(strUI, UsedCouponPattern);
                }




                if (IsOnlySalesInquiry() != 1)
                {
                    if (isShippingMethodNeeded)
                    {
                        bool ShowShippingAmount = false;
                        if (strUI.Contains("##ShowShippingAmountWithMethod##"))
                        {
                            ShowShippingAmount = true;
                            strUI = strUI.Replace("##ShowShippingAmountWithMethod##", "");
                        }
                        GetShippingMethod(ShowShippingAmount, TotalWeight, TotalCartAmount, CartItemsCount, IsAllCartItemsFreeShipping);
                    }
                    else
                    {
                        if (strUI.Contains("##ShowShippingAmountWithMethod##"))
                        {
                            strUI = strUI.Replace("##ShowShippingAmountWithMethod##", "");
                        }
                    }
                }

                decimal Tax = default(decimal);
                decimal SubTotal = default(decimal);
                decimal Discount = default(decimal);

                if (dtItems.Rows.Count > 0)
                {
                    foreach (DataRow dr in dtItems.Rows)
                    {
                        if (Sites.ToLong(dr["numItemCode"]) != Sites.ToLong(Session["ShippingServiceItemID"]) && Sites.ToLong(dr["numItemCode"]) != Sites.ToLong(Session["DiscountServiceItemID"]))
                        {
                            SubTotal += Sites.ToDecimal(dr["monTotAmtBefDiscount"]);
                        }
                    }

                    if (dtTable.Rows.Count > 0)//Check because in on one page check out it throws Error ...
                    {
                        Tax = GetTax(); //GetTaxForCartItems(Sites.ToLong(Session["DomainID"]), Sites.ToLong(Session["DivId"]), Sites.ToInteger(Session["BaseTaxCalcOn"]), Convert.ToInt16(Session["BaseTaxOnArea"]), Sites.ToInteger(dtTable.Rows[0]["numCountry"]), Sites.ToInteger(dtTable.Rows[0]["numState"]), Sites.ToString(dtTable.Rows[0]["vcCity"]), Sites.ToString(dtTable.Rows[0]["vcPostalCode"]), Sites.ToLong(Session["UserContactID"]));
                    }
                    else
                    {
                        Tax = 0;
                    }
                    dcTotalTax = Tax;

                    Discount = Sites.ToDecimal(dtItems.Compute("SUM(monTotalDiscountAmount)", ""));
                    if (dtItems.Select("numItemCode=" + CCommon.ToString(Session["DiscountServiceItemID"])).Length > 0)
                    {
                        Discount = Discount + Sites.ToDecimal(dtItems.Select("numItemCode=" + CCommon.ToString(Session["DiscountServiceItemID"]))[0]["monTotAmount"]) * -1;
                    }

                    Session["TotalDiscount"] = Discount;
                }


                if (Sites.ToBool(Session["HidePrice"]) == false)
                {
                    string strShippingCharge = GetShippingCharge();//string.Format("{0:#,##0.00}", Sites.ToDecimal(ddlShippingMethod.SelectedValue));
                    if (strShippingCharge == "")
                    {
                        strShippingCharge = "0.00";
                    }

                    lblCurrencySymbol.Text = Sites.ToString(Session["CurrSymbol"]);
                    lblSubTotal.Text = string.Format("{0:#,##0.00}", SubTotal);
                    lblDiscount.Text = string.Format("{0:#,##0.00}", Discount);
                    lblTax.Text = string.Format("{0:#,##0.00}", Tax);
                    lblShippingCharge.Text = string.Format("{0:#,##0.00}", strShippingCharge);
                    lblTotal.Text = string.Format("{0:#,##0.00}", SubTotal + Tax - Discount + Convert.ToDecimal(strShippingCharge));

                    Session["TotalAmount"] = SubTotal + Tax + Sites.ToDecimal(strShippingCharge) - Discount;
                    strUI = strUI.Replace("##ShippingMethodDropdown##", CCommon.RenderControl(ddlShippingMethod) + CCommon.RenderControl(hdnDiscountShipping));
                    strUI = strUI.Replace("##CurrencySymbol##", CCommon.RenderControl(lblCurrencySymbol));
                    strUI = strUI.Replace("##SubTotalLabel##", CCommon.RenderControl(lblSubTotal));
                    strUI = strUI.Replace("##DiscountLabel##", CCommon.RenderControl(lblDiscount));
                    strUI = strUI.Replace("##TaxLabel##", CCommon.RenderControl(lblTax));
                    strUI = strUI.Replace("##ShippingChargeLabel##", CCommon.RenderControl(lblShippingCharge));
                    strUI = strUI.Replace("##TotalAmountLabel##", CCommon.RenderControl(lblTotal));
                }
                else
                {
                    strUI = strUI.Replace("##SubTotalLabel##", "NA");
                    strUI = strUI.Replace("##DiscountLabel##", "NA");
                    strUI = strUI.Replace("##TaxLabel##", "NA");
                    strUI = strUI.Replace("##ShippingChargeLabel##", "NA");
                    strUI = strUI.Replace("##TotalAmountLabel##", "NA");
                }

                /*-------------Payment Method------------------- */

                strUI = strUI.Replace("##AddressLink##", CCommon.RenderControl(lbtnEdit));
                strUI = strUI.Replace("##CustomerInformationLink##", CCommon.RenderControl(lbtnCustomerInformationEdit));
                strUI = strUI.Replace("##CartLink##", CCommon.RenderControl(lbtnCartEdit));

                strUI = strUI.Replace("##TotalAmount##", CCommon.RenderControl(lblTotal));
                //strUI = strUI.Replace("##TotalAmount##", Sites.ToString(Session["CurrSymbol"]) + " " + string.Format("{0:#,##0.00}", Sites.ToDecimal(Session["TotalAmount"])));
                strUI = strUI.Replace("##CustomerPONumber##", CCommon.RenderControl(txtCustomerPoNumber));
                strUI = strUI.Replace("##Comments##", CCommon.RenderControl(txtComments));


                #region Checkout
                strUI = strUI.Replace("##TotalBalanceDue##", lblBalDue.Text);
                strUI = strUI.Replace("##TotalRemaniningCredit##", lblRemCredit.Text);
                strUI = strUI.Replace("##TotalAmountPastDue##", lblAmtPastDue.Text);

                strUI = strUI.Replace("##CardHolderTextBox##", CCommon.RenderControl(txtCHName));
                strUI = strUI.Replace("##CardNumberTextBox##", CCommon.RenderControl(txtCardNumber));
                strUI = strUI.Replace("##CardTypeDropDownList##", CCommon.RenderControl(ddlCardType));
                strUI = strUI.Replace("##CardExpMonthDropDownList##", CCommon.RenderControl(ddlCardExpMonth));
                strUI = strUI.Replace("##CardExpYearDropDownList##", CCommon.RenderControl(ddlCardExpYear));
                strUI = strUI.Replace("##CVV2TextBox##", CCommon.RenderControl(txtCardCVV2));
                strUI = strUI.Replace("##TotalAmount##", Sites.ToString(Session["CurrSymbol"]) + " " + string.Format("{0:#,##0.00}", Sites.ToDecimal(Session["TotalAmount"])));

                strUI = strUI.Replace("##showbillme##", Sites.ToString(Session["PayOption"]) == "BillMe" ? "" : "display:none");
                strUI = strUI.Replace("##showpaybycreditcard##", Sites.ToString(Session["PayOption"]) == "CreditCard" ? "" : "display:none");

                strUI = strUI.Replace("##PaypalEmail##", CCommon.RenderControl(txtPayPalEmail));
                //strUI = strUI.Replace("##PaypalPassword##", CCommon.RenderControl(txtPassword));

                strUI = strUI.Replace("##IsBillMeChecked##", Sites.ToString(Session["PayOption"]) == "BillMe" ? "checked=\"checked\"" : "");
                strUI = strUI.Replace("##IsGoogleCheckoutChecked##", Sites.ToString(Session["PayOption"]) == "GoogleCheckout" ? "checked=\"checked\"" : "");
                strUI = strUI.Replace("##IsPayChecked##", Sites.ToString(Session["PayOption"]) == "CreditCard" ? "checked=\"checked\"" : "");
                strUI = strUI.Replace("##IsPaypalCheckoutChecked##", Sites.ToString(Session["PayOption"]) == "Paypal" ? "checked=\"checked\"" : "");

                #endregion

                strUI = strUI.Replace("##SubmitButton##", CCommon.RenderControl(btnCharge));
                strUI = strUI.Replace("##ViewPastOrdersButton##", CCommon.RenderControl(btnOrders));

                /*Hide Payment Methods only if Sales Inquiry is selected as only payment method within E-Commerce Settings*/
                if (IsOnlySalesInquiry() == 1 && Sites.ToBool(Session["SkipStep2"]) == true)
                {
                    strUI = strUI.Replace("id=" + '"' + "Checkout_radBillMe" + '"', "id=" + '"' + "Checkout_radBillMe" + '"' + " style=" + '"' + "display:none;" + '"');
                    strUI = strUI.Replace("label for=" + '"' + "Checkout_radBillMe" + '"', "label for=" + '"' + "Checkout_radBillMe" + '"' + " style=" + '"' + "display:none;" + '"');

                    strUI = strUI.Replace("id=" + '"' + "Checkout_radPay" + '"', "id=" + '"' + "Checkout_radPay" + '"' + " style=" + '"' + "display:none;" + '"');
                    strUI = strUI.Replace("label for=" + '"' + "Checkout_radPay" + '"', "label for=" + '"' + "Checkout_radPay" + '"' + " style=" + '"' + "display:none;" + '"');

                    strUI = strUI.Replace("id=" + '"' + "Checkout_radGoogleCheckOut" + '"', "id=" + '"' + "Checkout_radGoogleCheckOut" + '"' + " style=" + '"' + "display:none;" + '"');
                    strUI = strUI.Replace("label for=" + '"' + "Checkout_radGoogleCheckOut" + '"', "label for=" + '"' + "Checkout_radGoogleCheckOut" + '"' + " style=" + '"' + "display:none;" + '"');

                    strUI = strUI.Replace("id=" + '"' + "Checkout_radPaypalCheckOut" + '"', "id=" + '"' + "Checkout_radPaypalCheckOut" + '"' + " style=" + '"' + "display:none;" + '"');
                    strUI = strUI.Replace("label for=" + '"' + "Checkout_radPaypalCheckOut" + '"', "label for=" + '"' + "Checkout_radPaypalCheckOut" + '"' + " style=" + '"' + "display:none;" + '"');

                    strUI = strUI.Replace("id=" + '"' + "tblBillme" + '"', "id=" + '"' + "tblBillme" + '"' + " style=" + '"' + "display:none;" + '"');
                    strUI = strUI.Replace("id=" + '"' + "tblCustomerInfo" + '"' + " width=" + '"' + "90%" + '"', "id=" + '"' + "tblCustomerInfo" + '"' + " width=" + '"' + "90%" + '"' + " style=" + '"' + "display:none;" + '"');
                    strUI = strUI.Replace("id=" + '"' + "tblGoogleCheckout" + '"', "id=" + '"' + "tblGoogleCheckout" + '"' + " style=" + '"' + "display:none;" + '"');
                    strUI = strUI.Replace("id=" + '"' + "tblPayPal" + '"', "id=" + '"' + "tblPayPal" + '"' + " style=" + '"' + "display:none;" + '"');
                }
                else if (IsOnlySalesInquiry() == 2)
                {
                    strUI = strUI.Replace("id=" + '"' + "Checkout_radSalesInquiryCheckOut" + '"', "id=" + '"' + "Checkout_radSalesInquiryCheckOut" + '"' + " style=" + '"' + "display:none;" + '"');
                    strUI = strUI.Replace("label for=" + '"' + "Checkout_radSalesInquiryCheckOut" + '"', "label for=" + '"' + "Checkout_radSalesInquiryCheckOut" + '"' + " style=" + '"' + "display:none;" + '"');
                }

                BindCustomField(ref strUI);
                pnlCutomizeHtml.Visible = false;
                litCutomizeHtml.Text = strUI;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        private void AddOrderPromotionDiscountIfAvailable()
        {
            try
            {
                DeleteItemFromCart(Sites.ToLong(Session["DiscountServiceItemID"]));
                double numSubTotal = GetCartSubTotal();

                //ORDER PROMOTION DISCOUNT
                double lngDiscountAmt = 0;
                DataSet dsOrderProm = null;

                //If user has already used coupon code than get promotion based on used coupon code else try to find promotion based on order subtotal
                if (CCommon.ToLong(hdnOrderPromotionID.Value) > 0 && CCommon.ToLong(hdnDiscountCodeID.Value) > 0)
                {
                    PromotionOfferOrder objPromotionOfferOrder = new PromotionOfferOrder();
                    objPromotionOfferOrder.DomainID = Sites.ToLong(Session["DomainID"]);
                    objPromotionOfferOrder.PromotionID = Sites.ToLong(hdnOrderPromotionID.Value);
                    dsOrderProm = objPromotionOfferOrder.GetPromotionOfferOrderByPromotionID();
                }
                else
                {
                    COpportunities objOpportunity = new COpportunities();
                    objOpportunity.DomainID = Sites.ToLong(Session["DomainID"]);
                    objOpportunity.DivisionID = Sites.ToInteger(Session["DivId"]);
                    dsOrderProm = objOpportunity.GetPromotiondRuleForOrder(numSubTotal, siteID: Sites.ToLong(Session["SiteId"]));
                }

                if (dsOrderProm != null && dsOrderProm.Tables.Count >= 2 && dsOrderProm.Tables[0].Rows.Count > 0)
                {
                    int RowCount = dsOrderProm.Tables[1].Rows.Count;
                    long promotionID = CCommon.ToLong(dsOrderProm.Tables[0].Rows[0]["numProID"]);
                    bool isCouponCodeRequired = CCommon.ToBool(dsOrderProm.Tables[0].Rows[0]["bitRequireCouponCode"]);
                    bool isOrderPromotionDisocuntInPercent = CCommon.ToShort(dsOrderProm.Tables[0].Rows[0]["tintDiscountType"]) == 1 ? true : false;

                    hdnOrderPromotionID.Value = CCommon.ToString(promotionID);

                    foreach (DataRow dr in dsOrderProm.Tables[1].Rows)
                    {
                        if (numSubTotal >= CCommon.ToDouble(dr["numOrderAmount"]) && (CCommon.ToInteger(dr["RowNum"]) != RowCount))
                        {
                            lngDiscountAmt = CCommon.ToDouble(dr["fltDiscountValue"].ToString());
                        }
                        else if ((numSubTotal >= CCommon.ToDouble(dr["numOrderAmount"])) && (CCommon.ToInteger(dr["RowNum"]) == RowCount))
                        {
                            lngDiscountAmt = CCommon.ToDouble(dr["fltDiscountValue"].ToString());
                        }
                    }

                    if (lngDiscountAmt > 0)
                    {
                        bool isValidCouponCode = false;

                        if (isCouponCodeRequired)
                        {
                            if (dsOrderProm.Tables.Count >= 3 && dsOrderProm.Tables[2].Rows.Count > 0 && dsOrderProm.Tables[2].Select("numDiscountID=" + hdnDiscountCodeID.Value).Length > 0)
                            {
                                isValidCouponCode = true;
                            }
                            else
                            {
                                isValidCouponCode = false;
                            }
                        }
                        else
                        {
                            isValidCouponCode = true;
                        }

                        if (isValidCouponCode)
                        {
                            if (isOrderPromotionDisocuntInPercent)
                            {
                                lngDiscountAmt = (numSubTotal * lngDiscountAmt) / 100;
                            }

                            AddToCartFromEcomm("", Sites.ToLong(Session["DiscountServiceItemID"]), -lngDiscountAmt, "", true);
                        }
                        else
                        {
                            hdnOrderPromotionID.Value = "0";
                            hdnDiscountCodeID.Value = "0";
                            txtCouponCode.Text = "";
                            lblCouponCode.Text = "";
                            litApplyCouponCode.Visible = true;
                            litUsedCouponCode.Visible = false;
                        }
                    }
                    else
                    {
                        hdnOrderPromotionID.Value = "0";
                        hdnDiscountCodeID.Value = "0";
                        txtCouponCode.Text = "";
                        lblCouponCode.Text = "";
                        litApplyCouponCode.Visible = true;
                        litUsedCouponCode.Visible = false;
                    }
                }
                else
                {
                    hdnOrderPromotionID.Value = "0";
                    hdnDiscountCodeID.Value = "0";
                    txtCouponCode.Text = "";
                    lblCouponCode.Text = "";
                    litApplyCouponCode.Visible = true;
                    litUsedCouponCode.Visible = false;
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        #endregion

        #region Place Order Bill Me , Credit Card , Google Checkout

        private void SaveOpportunity()
        {
            try
            {
                if (Sites.ToLong(Session["OppID"]) == 0)
                {
                    DataTable dteCommercePaymentConfig = new DataTable();
                    CItems objItems = new CItems();
                    objItems.DomainID = Sites.ToLong(Session["DomainID"]);
                    objItems.DivisionID = lngDivID;
                    objItems.ContactID = lngContId;
                    objItems.OppName = "-SO-" + DateTime.Now.ToString("MMMM");
                    objItems.Source = "http://" + Request.ServerVariables["SERVER_NAME"].ToString();
                    objItems.CurrencyID = Sites.ToLong(Session["CurrencyID"].ToString());
                    objItems.ShipAddressId = Sites.ToLong(Session["ShipAddress"]);

                    //Change Shipping Address if will call is selected
                    if (ddlShippingMethod.SelectedItem != null)
                    {
                        string[] strShippingCharge = Sites.ToString(ddlShippingMethod.SelectedValue).Split('~');


                        if (strShippingCharge.Length >= 4)
                        {
                            if (Sites.ToInteger(strShippingCharge[3]) == 92)
                            {
                                CItems objItem = new CItems();
                                objItem.WarehouseID = Sites.ToLong(HttpContext.Current.Session["DefaultWareHouseID"]);
                                objItem.DomainID = Sites.ToLong(HttpContext.Current.Session["DomainID"]);
                                DataTable dtTableWarehouse = objItem.GetWareHouses();

                                if (dtTableWarehouse != null && dtTableWarehouse.Rows.Count > 0)
                                {
                                    objItems.ShipAddressId = Sites.ToLong(dtTableWarehouse.Rows[0]["numAddressID"]);
                                }
                            }
                        }
                    }

                    objItems.BillAddressId = Sites.ToLong(Session["BillAddress"]);

                    //As discount added as a line item there is no need to add it in discount column.
                    //objItems.Discount = Sites.ToDecimal(Session["TotalDiscount"]);
                    objItems.SiteID = Sites.ToLong(Session["SiteId"]);
                    objItems.ProID = Sites.ToLong(Session["ProID"]);
                    objItems.DiscountType = 1;

                    if (Sites.ToString(Session["PayOption"]).ToLower() == "salesinquiry")
                    {
                        objItems.OppStatus = 0;
                    }
                    else
                    {
                        objItems.OppStatus = 1;
                    }

                    //here shipping cost pass 0 as it is added as a line Item in cart itself Sites.ToDecimal(Session["ShippingCost"]);
                    objItems.ShipCost = 0;
                    objItems.tintSource = Sites.ToInteger(Session["SiteId"]);
                    objItems.SourceType = 2;
                    string strPaymentMethod = Sites.ToString(Session["PayOption"]).ToLower();
                    switch (strPaymentMethod)
                    {
                        case "billme":
                            objItems.PaymentMethod = Sites.ToLong(PaymentMethod.BillMe);
                            break;
                        case "creditcard":
                            objItems.PaymentMethod = Sites.ToLong(PaymentMethod.CreditCard);
                            break;
                        case "googlecheckout":
                            objItems.PaymentMethod = Sites.ToLong(PaymentMethod.GoogleCheckout);
                            break;
                        case "paypal":
                            objItems.PaymentMethod = Sites.ToLong(PaymentMethod.Paypal);
                            break;
                        case "salesinquiry":
                            objItems.PaymentMethod = Sites.ToLong(PaymentMethod.SalesInquiry);
                            break;
                        default:
                            break;
                    }
                    objItems.numPartner = Sites.ToLong(Session["partnercode"]);
                    objItems.vcPartenerContact = Sites.ToString(Session["partnercontact"]);
                    objItems.Comments = txtComments.Text.Trim();
                    objItems.numShipmentMethod = Sites.ToLong(ddlShipVia.SelectedValue);
                    objItems.CustomerPONumber = CCommon.ToString(txtCustomerPoNumber.Text);

                    CAdmin objAdmin = new CAdmin();
                    objAdmin.DivisionID = lngDivID;
                    DataTable dtBillingTerms = objAdmin.GetBillingTerms();

                    if (dtBillingTerms != null && dtBillingTerms.Rows.Count > 0)
                    {
                        objItems.boolBillingTerms = Sites.ToInteger(dtBillingTerms.Rows[0]["tintBillingTerms"]) == 1 ? true : false;
                        objItems.BillingDays = Sites.ToInteger(dtBillingTerms.Rows[0]["numBillingDays"]);
                        objItems.boolInterestType = CCommon.ToInteger(dtBillingTerms.Rows[0]["tintInterestType"]) == 1 ? true : false;
                        objItems.Interest = Convert.ToDecimal(dtBillingTerms.Rows[0]["fltInterest"]);
                    }

                    if (ddlShippingMethod.SelectedItem != null)
                    {
                        string[] strShippingCharge = Sites.ToString(ddlShippingMethod.SelectedValue).Split('~');

                        if (strShippingCharge.Length >= 1)
                        {
                            objItems.ShippingService = Sites.ToLong(strShippingCharge[0]);
                        }

                        if (strShippingCharge.Length >= 4)
                        {
                            objItems.numShipmentMethod = Sites.ToInteger(strShippingCharge[3]);
                        }
                    }

                    objItems.OrderPromotionID = CCommon.ToLong(hdnOrderPromotionID.Value);
                    objItems.OrderPromotionDiscountID = CCommon.ToLong(hdnDiscountCodeID.Value);

                    using (var objTransaction = new System.Transactions.TransactionScope(System.Transactions.TransactionScopeOption.Required, new System.Transactions.TransactionOptions() { IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted, Timeout = System.Transactions.TransactionManager.MaximumTimeout }))
                    {
                        objItems.CreateOrderForECommerce();

                        Workflow objWfA = new Workflow();
                        objWfA.DomainID = Sites.ToLong(Session["DomainID"]);
                        objWfA.UserCntID = Sites.ToLong(Session["UserContactID"]);
                        objWfA.RecordID = objItems.OppId;
                        objWfA.SaveWFOrderQueue();

                        Session["OppID"] = objItems.OppId;
                        Session["OppName"] = objItems.OppName; //TO be used in payment gateway for reference invoice id
                        Session["TotalAmount"] = GetCartTotalAmount();

                        Sites objSites = new Sites();
                        objSites.SiteID = Sites.ToLong(Session["SiteId"]);
                        objSites.DomainID = Sites.ToLong(Session["DomainID"]);
                        dteCommercePaymentConfig = objSites.GeteCommercePaymentConfig(objItems.PaymentMethod);

                        objItems.OppId = Sites.ToLong(Session["OppID"]);

                        if (dteCommercePaymentConfig != null && dteCommercePaymentConfig.Rows.Count > 0 && Sites.ToLong(dteCommercePaymentConfig.Rows[0]["numBizDocId"]) > 0)
                        {
                            OppBizDocs objOppBizDocs = new OppBizDocs();
                            objOppBizDocs.OppId = Sites.ToLong(Session["OppID"]);
                            objOppBizDocs.DomainID = Sites.ToLong(Session["DomainID"]);
                            objOppBizDocs.bitPartialShipment = true;
                            objOppBizDocs.OppType = 1;
                            objOppBizDocs.UserCntID = Sites.ToLong(Session["UserContactID"]);
                            objOppBizDocs.vcPONo = "";
                            objOppBizDocs.vcComments = "";

                            if (dteCommercePaymentConfig.Rows.Count > 0)
                            {
                                objOppBizDocs.BizDocStatus = Sites.ToLong(dteCommercePaymentConfig.Rows[0]["numBizDocStatus"]);
                            }

                            objOppBizDocs.ClientTimeZoneOffset = Sites.ToInteger(Session["ClientMachineUTCTimeOffset"]);
                            objOppBizDocs.BizDocId = Sites.ToLong(dteCommercePaymentConfig.Rows[0]["numBizDocId"]);
                            objOppBizDocs.OppBizDocId = 0;

                            CCommon objCommon = new CCommon();
                            objCommon.DomainID = Sites.ToLong(Session["DomainID"]);
                            objCommon.Mode = 33;
                            objCommon.Str = Sites.ToString(dteCommercePaymentConfig.Rows[0]["numBizDocId"]);
                            objOppBizDocs.SequenceId = Sites.ToString(objCommon.GetSingleFieldValue());

                            lngOppBizDocID = objOppBizDocs.SaveBizDoc();
                            Session["OppBizDocID"] = lngOppBizDocID;

                            if (objOppBizDocs.BizDocStatus > 0 && Sites.ToLong(dteCommercePaymentConfig.Rows[0]["numBizDocId"]) != Sites.ToLong(Session["numSalesInquiryBizDocID"]))
                            {
                                objOppBizDocs.tintMode = 0;
                                objOppBizDocs.OpportunityBizDocStatusChange();
                            }

                            objItems.DomainID = Sites.ToLong(Session["DomainID"]);

                            if (Sites.ToLong(dteCommercePaymentConfig.Rows[0]["numBizDocId"]) == Sites.ToLong(Session["AuthSalesBizDoc"]))
                            {
                                //Create Accounting Journal entry only when set bizdoc is authoritative bizdoc 
                                CreateJournalEntries(objItems);
                            }

                            Workflow objWfA1 = new Workflow();
                            objWfA1.DomainID = Sites.ToLong(Session["DomainID"]);
                            objWfA1.UserCntID = Sites.ToLong(Session["UserContactID"]);
                            objWfA1.RecordID = Sites.ToLong(Session["OppBizDocID"]);
                            objWfA1.SaveWFBizDocQueue();
                        }

                        objItems.OppId = Sites.ToLong(Session["OppID"]);
                        objItems.byteMode = 1;

                        objItems.ShippingCost = 0;//Sites.ToDecimal(Session["ShippingCost"]);//lblShippingCost.Text;
                        objItems.DomainID = Sites.ToLong(Session["DomainID"]);
                        objItems.UserCntID = Sites.ToLong(Session["UserContactID"]);
                        objItems.SiteID = Sites.ToLong(Session["SiteId"]);
                        objItems.ShipAddressId = 2;

                        if (dteCommercePaymentConfig != null && dteCommercePaymentConfig.Rows.Count > 0 && Sites.ToLong(dteCommercePaymentConfig.Rows[0]["numBizDocId"]) > 0)
                        {
                            if (Sites.ToLong(dteCommercePaymentConfig.Rows[0]["numBizDocId"]) > 0 && Sites.ToLong(dteCommercePaymentConfig.Rows[0]["numBizDocId"]) != Sites.ToLong(Session["numSalesInquiryBizDocID"]))
                                objItems.UpdateDealStatus1();
                        }


                        btnOrders.Visible = true;

                        objTransaction.Complete();
                    }

                    /*Send email to customer*/
                    SendEmail();
                    if (Sites.ToLong(Session["OppBizDocID"]) > 0 && dteCommercePaymentConfig != null && dteCommercePaymentConfig.Rows.Count > 0 && Sites.ToLong(dteCommercePaymentConfig.Rows[0]["numBizDocId"]) > 0)
                    {
                        //Send BizDoc Alerts - Notify record owners, their supervisors, and your trading partners when a BizDoc is created, modified, or approved.
                        CAlerts objAlert = new CAlerts();
                        objAlert.SendBizDocAlerts(Sites.ToLong(Session["OppID"]), Sites.ToLong(Session["OppBizDocID"]), Sites.ToLong(dteCommercePaymentConfig.Rows[0]["numBizDocId"]), Sites.ToLong(Session["DomainID"]), CAlerts.enmBizDoc.IsCreated);
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        private void SaveCusField()
        {
            try
            {
                //'Saving CustomFields
                FormConfigWizard objFormWizard = new FormConfigWizard();
                objFormWizard.DomainID = Sites.ToLong(Session["DomainID"]);
                objFormWizard.LocationIds = "2"; //sales opportunity 
                dtCustFld = objFormWizard.GetCustomFormFields();
                DataView dsViews = new DataView(dtCustFld);
                dsViews.RowFilter = "vcFieldType='C'";
                int i = 0;
                DataTable dtCusTable = new DataTable();
                dtCusTable.Columns.Add("FldDTLID");
                dtCusTable.Columns.Add("fld_id");
                dtCusTable.Columns.Add("Value");
                DataRow dRow;
                string strdetails = null;

                if (dsViews.Count > 0)
                {
                    for (i = 0; i <= dsViews.Count - 1; i++)
                    {
                        if (plhFormControls.FindControl(dsViews[i]["vcFormFieldName"].ToString().Trim().Replace(" ", "") + "_" + dsViews[i]["vcFieldType"].ToString()) != null)
                        {
                            dRow = dtCusTable.NewRow();
                            dRow["FldDTLID"] = 0;
                            dRow["fld_id"] = dsViews[i]["numFormFieldId"].ToString().Replace("C", "");
                            dRow["Value"] = GetCustFldValue(dsViews[i]["vcAssociatedControlType"].ToString(), dsViews[i]["vcFormFieldName"].ToString().Trim().Replace(" ", "") + "_" + dsViews[i]["vcFieldType"].ToString());
                            dtCusTable.Rows.Add(dRow);
                        }
                    }
                    dtCusTable.TableName = "Table";
                    DataSet ds = new DataSet();
                    ds.Tables.Add(dtCusTable.Copy());
                    strdetails = ds.GetXml();
                    ds.Tables.Remove(ds.Tables[0]);

                    CustomFields ObjCusfld = new CustomFields();
                    ObjCusfld.strDetails = strdetails;
                    ObjCusfld.RecordId = Sites.ToLong(Session["OppID"]);
                    ObjCusfld.locId = 2;
                    ObjCusfld.SaveCustomFldsByRecId();
                }


            }
            catch (Exception ex)
            {
                throw;
            }
        }

        private void MakeDepositEntry(long DivisionId, decimal Amount, DateTime OrderDate, long lngTransactionHistoryID, string Reference, string PaymentMethod, bool IsAuthoritative)
        {
            try
            {
                UndepositedFundAccountID = ChartOfAccounting.GetDefaultAccount("UF", Sites.ToLong(Session["DomainID"])); //Undeposited Fund
                long lngDepositeID, lngJournalID;
                string Description = "";
                MakeDeposit objMakeDeposit = new MakeDeposit();

                objMakeDeposit.DivisionId = Sites.ToInteger(lngDivID);
                objMakeDeposit.Entry_Date = System.DateTime.Now;
                objMakeDeposit.TransactionHistoryID = lngTransactionHistoryID;
                objMakeDeposit.Reference = Sites.ToString(Session["OppName"]);
                objMakeDeposit.Memo = Sites.ToString(Session["OppName"]);
                objMakeDeposit.PaymentMethod = Sites.ToLong(PaymentMethod);
                objMakeDeposit.DepositeToType = 2;// (radDepositeTo.Checked ? 1 : 2);
                //'.numAmount = Replace(lblDepositTotalAmount.Text, ",", "")
                objMakeDeposit.numAmount = Sites.ToDecimal(Sites.ToString(Session["TotalAmount"]).Replace(",", ""));
                objMakeDeposit.AccountId = Sites.ToInteger(UndepositedFundAccountID);

                objMakeDeposit.RecurringId = 0;
                objMakeDeposit.DepositId = 0;
                objMakeDeposit.UserCntID = Sites.ToLong(Session["UserContactID"]);

                objMakeDeposit.DomainID = Sites.ToLong(Session["DomainId"]);

                if (IsAuthoritative)//if (lngOppBizDocID > 0 && objItems.BizDocID == Sites.ToLong(Session["AuthSalesBizDoc"]))
                {
                    DataSet ds = new DataSet();
                    DataTable dt = new DataTable();
                    DataTable dtDeposit = new DataTable();
                    CCommon.AddColumnsToDataTable(ref dtDeposit, "numDepositeDetailID,numOppBizDocsID,numOppID,monAmountPaid");
                    DataRow dr = dtDeposit.NewRow();
                    dr["numDepositeDetailID"] = 0;
                    dr["numOppBizDocsID"] = Sites.ToLong(Session["OppBizDocID"]);
                    dr["numOppID"] = Sites.ToLong(Session["OppID"]);
                    dr["monAmountPaid"] = Amount;
                    dtDeposit.Rows.Add(dr);
                    ds.Tables.Add(dtDeposit.Copy());
                    ds.Tables[0].TableName = "Item";
                    objMakeDeposit.StrItems = ds.GetXml();
                }
                else
                {
                    objMakeDeposit.StrItems = "";
                }
                objMakeDeposit.Mode = 0;
                objMakeDeposit.DepositePage = 2;
                lngDepositeID = 0;
                lngDepositeID = objMakeDeposit.SaveDataToMakeDepositDetails();

                lngJournalID = SaveDataToHeader(Sites.ToLong(Session["DomainID"]), Sites.ToLong(Session["UserContactID"]), OrderDate, Description, Amount, Sites.ToLong(Session["OppID"]), UndepositedFundAccountID, lngDepositeID, 0);

                SaveDataToGeneralJournalDetailsForCashAndChecks(Sites.ToLong(Session["DomainID"]), lngDepositeID, lngJournalID, Amount, DivisionId, UndepositedFundAccountID);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        private void SaveDataToGeneralJournalDetailsForCashAndChecks(long DomainID, long lngDepositeID, long lngJournalID, decimal p_Amount, long DivisionId, long UndepositedFundAccountID)
        {

            try
            {
                JournalEntryCollection objJEList = new JournalEntryCollection();
                JournalEntryNew objJE = new JournalEntryNew();
                //Debit : Undeposit Fund (Amount)
                objJE = new JournalEntryNew();

                objJE.TransactionId = 0;
                objJE.DebitAmt = p_Amount;
                objJE.CreditAmt = 0;
                objJE.ChartAcntId = UndepositedFundAccountID;
                objJE.Description = "Amount Paid (" + p_Amount.ToString() + ")  To Undeposited Fund";
                objJE.CustomerId = DivisionId;
                objJE.MainDeposit = Sites.ToBool(1);
                objJE.MainCheck = Sites.ToBool(0);
                objJE.MainCashCredit = Sites.ToBool(0);
                objJE.OppitemtCode = 0;
                objJE.BizDocItems = "";
                objJE.Reference = "";
                objJE.PaymentMethod = 0;
                objJE.Reconcile = false;
                objJE.CurrencyID = 0;
                objJE.FltExchangeRate = 0;
                objJE.TaxItemID = 0;
                objJE.BizDocsPaymentDetailsId = 0;
                objJE.ContactID = 0;
                objJE.ItemID = 0;
                objJE.ProjectID = 0;
                objJE.ClassID = 0;
                objJE.CommissionID = 0;
                objJE.ReconcileID = 0;
                objJE.Cleared = Sites.ToBool(0);
                objJE.ReferenceType = CCommon.ToShort(enmReferenceType.DepositHeader);
                objJE.ReferenceID = lngDepositeID;

                objJEList.Add(objJE);

                //For Each dr As DataRow In dtItems.Rows
                //Credit: Customer A/R With (Amount)
                OppBizDocs objOppBizDocs = new OppBizDocs();

                long lngCustomerARAccount = objOppBizDocs.ValidateCustomerAR_APAccounts("AR", DomainID, DivisionId);

                objJE = new JournalEntryNew();

                objJE.TransactionId = 0;
                objJE.DebitAmt = 0;
                objJE.CreditAmt = p_Amount;
                objJE.ChartAcntId = lngCustomerARAccount;
                objJE.Description = "Credit Customer's AR account";
                objJE.CustomerId = DivisionId;
                objJE.MainDeposit = Sites.ToBool(0);
                objJE.MainCheck = Sites.ToBool(0);
                objJE.MainCashCredit = Sites.ToBool(0);
                objJE.OppitemtCode = 0;
                objJE.BizDocItems = "";
                objJE.Reference = "";
                objJE.PaymentMethod = 0;
                objJE.Reconcile = false;
                objJE.CurrencyID = 0;
                objJE.FltExchangeRate = 0;
                objJE.TaxItemID = 0;
                objJE.BizDocsPaymentDetailsId = 0;
                objJE.ContactID = 0;
                objJE.ItemID = 0;
                objJE.ProjectID = 0;
                objJE.ClassID = 0;
                objJE.CommissionID = 0;
                objJE.ReconcileID = 0;
                objJE.Cleared = Sites.ToBool(0);
                objJE.ReferenceType = CCommon.ToShort(enmReferenceType.DepositDetail);
                objJE.ReferenceID = 0;

                objJEList.Add(objJE);

                objJEList.Save(JournalEntryCollection.JournalMode.DeleteUpdateInsert, lngJournalID, DomainID);

            }
            catch (Exception ex)
            {
                throw;
            }
        }

        private long SaveDataToHeader(long DomainID, long UserContactID, DateTime OrderDate, string Description, decimal Amount, long OppID, long UndepositedFundAccountID, long lngDepositeID, long lngOppBizDocIDParameter)
        {
            try
            {
                JournalEntryHeader objJEHeader = new JournalEntryHeader();
                long lngJournalID = 0;
                {
                    objJEHeader.JournalId = 0;
                    objJEHeader.RecurringId = 0;
                    objJEHeader.EntryDate = OrderDate;
                    objJEHeader.Description = Description;
                    objJEHeader.Amount = Amount;
                    objJEHeader.CheckId = 0;
                    objJEHeader.CashCreditCardId = 0;
                    objJEHeader.ChartAcntId = 0;
                    objJEHeader.OppId = OppID;
                    objJEHeader.OppBizDocsId = lngOppBizDocIDParameter;
                    objJEHeader.DepositId = lngDepositeID;
                    objJEHeader.BizDocsPaymentDetId = 0;
                    objJEHeader.IsOpeningBalance = Sites.ToBool(0);
                    objJEHeader.LastRecurringDate = System.DateTime.Now;
                    objJEHeader.NoTransactions = 0;
                    objJEHeader.CategoryHDRID = 0;
                    objJEHeader.ReturnID = 0;
                    objJEHeader.CheckHeaderID = 0;
                    objJEHeader.BillID = 0;
                    objJEHeader.BillPaymentID = 0;
                    objJEHeader.UserCntID = UserContactID;
                    objJEHeader.DomainID = DomainID;
                }
                lngJournalID = objJEHeader.Save();
                return lngJournalID;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        private void CreateJournalEntries(CItems objItems)
        {
            try
            {
                UndepositedFundAccountID = ChartOfAccounting.GetDefaultAccount("UF", Sites.ToLong(Session["DomainID"])); //Undeposited Fund

                DataSet ds = new DataSet();
                DataTable dtOppBiDocItems;
                OppBizDocs objOppBizDocs = new OppBizDocs();
                objOppBizDocs.OppId = objItems.OppId;
                objOppBizDocs.OppBizDocId = Sites.ToLong(Session["OppBizDocID"]);
                objOppBizDocs.DomainID = Sites.ToLong(Session["DomainID"]);
                objOppBizDocs.UserCntID = lngContId;
                ds = objOppBizDocs.GetOppInItemsForAuthorizativeAccounting();
                dtOppBiDocItems = ds.Tables[0];

                CalculateDealAmount objCalculateDealAmount = new CalculateDealAmount();
                objCalculateDealAmount.CalculateDealAmount(objItems.OppId, Sites.ToLong(Session["OppBizDocID"]), 1, Sites.ToLong(Session["DomainID"]), dtOppBiDocItems, false);


                //'---------------------------------------------------------------------------------
                long JournalId = SaveDataToHeader(objItems.DomainID, objItems.UserCntID, System.DateTime.Now, objItems.Description, objItems.Amount, objItems.OppId, UndepositedFundAccountID, 0, Sites.ToLong(Session["OppBizDocID"]));
                //SaveDataToHeader(objCalculateDealAmount.GrandTotal);

                JournalEntry objJournalEntries = new JournalEntry();
                objJournalEntries.SaveJournalEntriesSalesNew(objItems.OppId, Sites.ToLong(Session["DomainID"]), dtOppBiDocItems, JournalId, Sites.ToLong(Session["OppBizDocID"]), objCalculateDealAmount.GrandTotal, objCalculateDealAmount.TotalDiscount, objCalculateDealAmount.ShippingAmount, objCalculateDealAmount.TotalLateCharges, lngDivID, Sites.ToLong(ds.Tables[1].Rows[0]["numCurrencyID"]), Sites.ToDouble(ds.Tables[1].Rows[0]["fltExchangeRate"]), 0, 0);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        #endregion

        #region Shipping and Tax Calculations

        private string GetShippingCharge()
        {
            try
            {
                if (ddlShippingMethod.SelectedIndex != 0)
                {
                    string[] strShippingCharge = ddlShippingMethod.SelectedValue.Split('~');
                    if (strShippingCharge.Length > 1)
                    {
                        return string.Format("{0:#,##0.00}", CCommon.ToDecimal(Sites.ToDecimal(strShippingCharge[1])));
                    }
                }
                else if (hdnIsStaticShippingCharge.Value == "1")
                {
                    return string.Format("{0:#,##0.00}", Sites.ToDecimal(hdnStaticShippingCharge.Value));
                }

                return "0.00";
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        private string GetShippingMethodName()
        {
            try
            {
                if (ddlShippingMethod.SelectedIndex != 0)
                {
                    string[] strShippingCharge = Sites.ToString(ddlShippingMethod.SelectedValue).Split('~');
                    if (strShippingCharge.Length == 5)
                    {
                        string[] strShippingName = strShippingCharge[4].Split('-');
                        if (strShippingName.Length == 2)
                        {
                            return strShippingName[0];
                        }
                    }
                }
                else if (hdnIsStaticShippingCharge.Value == "1")
                {
                    return "Shipping Charge";
                }

                return "";
            }
            catch (Exception ex)
            {
                throw;
            }

        }

        private void GetShippingMethod(Boolean ShowShippingAmount, decimal TotalWeight, decimal TotalCartAmount, int CartItemsCount, bool IsAllCartItemsFreeShipping)
        {
            try
            {
                string zipCode = "";
                string strItemCodes = "";
                long stateID = 0;
                long countryID = 0;

                DataTable dtTable = null;
                CContacts objContact = new CContacts();
                objContact.DomainID = Sites.ToLong(Session["DomainID"]);
                objContact.AddressID = Sites.ToLong(Session["ShipAddress"]);
                objContact.byteMode = 1;
                dtTable = objContact.GetAddressDetail();

                if (dtTable.Rows.Count > 0)
                {
                    zipCode = Sites.ToString(dtTable.Rows[0]["vcPostalCode"]);
                    stateID = Sites.ToLong(dtTable.Rows[0]["numState"]);
                    countryID = Sites.ToLong(dtTable.Rows[0]["numCountry"]);
                }

                DataSet ds = new DataSet();
                ds = GetCartItem();

                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        strItemCodes = strItemCodes + (strItemCodes.Length > 0 ? "," + CCommon.ToString(dr["numItemCode"]) : CCommon.ToString(dr["numItemCode"]));
                    }
                }

                ShippingRule objShippingRule = new ShippingRule();
                objShippingRule.DomainID = Sites.ToLong(Session["DomainID"]);
                objShippingRule.SiteID = Sites.ToInteger(Session["SiteId"]);
                Tuple<bool, string, double, DataTable> tupleShippingPromotion = objShippingRule.GetShippingRule(zipCode, stateID, Sites.ToLong(Session["UserIPCountryID"]), CCommon.ToInteger(Session["DivId"]), CCommon.ToLong(Session["WareHouseID"]), GetCartSubTotal(), strItemCodes);

                if (tupleShippingPromotion.Item1)
                {
                    hdnIsStaticShippingCharge.Value = "1";
                    hdnStaticShippingCharge.Value = CCommon.ToString(tupleShippingPromotion.Item3);
                }
                else
                {
                    hdnIsStaticShippingCharge.Value = "0";
                    hdnStaticShippingCharge.Value = "0";
                }

                DataTable dtShippingMethod = new DataTable();
                string strMessage = "";

                #region Get Warehouse

                CItems objItem = new CItems();
                DataTable dtTableWarehouse = null;
                objItem.WarehouseID = Sites.ToLong(HttpContext.Current.Session["DefaultWareHouseID"]);
                objItem.DomainID = Sites.ToLong(HttpContext.Current.Session["DomainID"]);
                dtTableWarehouse = objItem.GetWareHouses();

                #endregion

                //Find real time shipping methods selected
                dtShippingMethod = objShippingRule.GetShippingMethodForEcommerce();


                if (dtShippingMethod != null && dtShippingMethod.Rows.Count > 0)
                {
                    System.Data.DataColumn newColumn = new System.Data.DataColumn("IsShippingRuleValid", typeof(System.Boolean));
                    newColumn.DefaultValue = true;
                    dtShippingMethod.Columns.Add(newColumn);
                    //Add this column to store it as a value field in the dropdown of ddlShippingMethod
                    CCommon.AddColumnsToDataTable(ref dtShippingMethod, "vcServiceTypeID1");

                    #region foreach start
                    decimal subtotalItemClassificationAmount = 0;
                    foreach (DataRow dr in dtShippingMethod.Rows)
                    {
                        Decimal decShipingAmount = 0;
                        bool IsValid = false;
                        if (Sites.ToInteger(dr["intNsoftEnum"]) == 0)
                        {
                            if (Sites.ToLong(dr["numSiteID"]) == CCommon.ToLong(Session["SiteID"]))
                            {
                                ShippingRule _objShippingRule = new ShippingRule();
                                _objShippingRule.RuleID = CCommon.ToLong(dr["numItemClassificationRuleID"]);
                                _objShippingRule.DomainID = CCommon.ToLong(Session["DomainID"]);
                                DataTable dtShippingRuleStates = new DataTable();
                                dtShippingRuleStates = _objShippingRule.GetShippingRuleStates();
                                var countryData = dtShippingRuleStates.Select(" numCountryID = " + Sites.ToDecimal(countryID) + " AND (numStateID = " + Sites.ToDecimal(stateID) + " OR numStateID = 0)").Length;
                                //var countryData = (from myRow in dtShippingRuleStates.AsEnumerable()
                                //                  where myRow.Field<decimal>("numCountryID") == Sites.ToDecimal(ddlCountry.SelectedValue)
                                //                  select myRow);
                                if (countryData > 0)
                                {
                                    if (Convert.ToBoolean(dr["bitItemClassification"]) == true)
                                    {
                                        decimal totalItemClassificationAmount = 0;
                                        var ItemClassifications = Sites.ToString(dr["vcItemClassification"]).Split(',');
                                        foreach (var d in ItemClassifications)
                                        {
                                            if (d != "")
                                            {
                                                totalItemClassificationAmount = Sites.ToDecimal(ds.Tables[0].Compute("SUM(monTotAmount)", "numItemClassification = '" + d + "' "));
                                                //if (d == Sites.ToString(drItems["numItemClassification"]))
                                                //{
                                                //    totalItemClassificationAmount = totalItemClassificationAmount + Sites.ToDecimal(drItems["monTotAmount"]);
                                                //}
                                                if (totalItemClassificationAmount > 0)
                                                {
                                                    if (totalItemClassificationAmount >= CCommon.ToDecimal(dr["intFrom"]) && totalItemClassificationAmount <= CCommon.ToDecimal(dr["intTo"]))
                                                    {
                                                        IsValid = false;
                                                        decShipingAmount = Sites.ToDecimal(dr["monRate"]) / Sites.ToDecimal(HttpContext.Current.Session["ExchangeRate"].ToString());
                                                        dr["vcServiceTypeID1"] = Sites.ToString(dr["numServiceTypeID"]) + "~" + CCommon.ToString(decShipingAmount) + "~0~0~0~" + CCommon.ToString(dr["vcServiceName"]);
                                                        if (ShowShippingAmount)
                                                        {
                                                            dr["vcServiceName"] = Sites.ToString(dr["vcServiceName"]) + " - " + Sites.ToString(HttpContext.Current.Session["CurrSymbol"]) + " " + String.Format("{0:#,##0.00}", decShipingAmount);
                                                        }
                                                        else
                                                        {
                                                            dr["vcServiceName"] = Sites.ToString(dr["vcServiceName"]);
                                                        }
                                                        subtotalItemClassificationAmount = subtotalItemClassificationAmount + decShipingAmount;
                                                    }
                                                }
                                                else
                                                {
                                                    IsValid = false;
                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (TotalCartAmount >= CCommon.ToDecimal(dr["intFrom"]) && TotalCartAmount <= CCommon.ToDecimal(dr["intTo"]))
                                        {
                                            IsValid = true;
                                            decShipingAmount = Sites.ToDecimal(dr["monRate"]) / Sites.ToDecimal(HttpContext.Current.Session["ExchangeRate"].ToString());
                                            dr["vcServiceTypeID1"] = Sites.ToString(dr["numServiceTypeID"]) + "~" + CCommon.ToString(decShipingAmount) + "~0~0~0~" + CCommon.ToString(dr["vcServiceName"]);
                                            if (ShowShippingAmount)
                                            {
                                                dr["vcServiceName"] = Sites.ToString(dr["vcServiceName"]) + " - " + Sites.ToString(HttpContext.Current.Session["CurrSymbol"]) + " " + String.Format("{0:#,##0.00}", decShipingAmount);
                                            }
                                            else
                                            {
                                                dr["vcServiceName"] = Sites.ToString(dr["vcServiceName"]);
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    IsValid = false;
                                }
                            }
                            else
                            {
                                IsValid = false;
                            }

                            dr["IsShippingRuleValid"] = IsValid;
                           
                        }
                        else
                        {
                            #region Warehouse Checking
                            if (dtTableWarehouse.Rows.Count > 0)
                            {
                                if (Sites.ToLong(dtTableWarehouse.Rows[0]["numWCountry"]) > 0 && Sites.ToLong(dtTableWarehouse.Rows[0]["numWState"]) > 0)
                                {
                                    if (Sites.ToLong(dr["numShippingCompanyID"]) == 92)
                                    {
                                        if (ShowShippingAmount)
                                        {
                                            dr["vcServiceName"] = Sites.ToString(dr["vcServiceName"]) + " - " + Sites.ToString(Session["CurrSymbol"]) + " " + string.Format("{0:#,##0.00}", 0);
                                        }

                                        dr["vcServiceTypeID1"] = Sites.ToString(dr["numServiceTypeID"]) + "~0~0~" + Sites.ToString(dr["numShippingCompanyID"]) + "~" + Sites.ToString(dr["vcServiceName"]);
                                    }
                                    else if (TotalWeight >= Sites.ToDecimal(dr["intFrom"]) && TotalWeight <= Sites.ToDecimal(dr["intTo"]))
                                    {
                                        #region Get BizDocs

                                        OppBizDocs objOppBizDocs = new OppBizDocs();
                                        objOppBizDocs.ShipCompany = Sites.ToInteger(dr["numShippingCompanyID"]);


                                        objOppBizDocs.ShipFromCountry = Sites.ToLong(dtTableWarehouse.Rows[0]["numWCountry"]);
                                        objOppBizDocs.ShipFromState = Sites.ToLong(dtTableWarehouse.Rows[0]["numWState"]);
                                        objOppBizDocs.ShipToCountry = countryID;
                                        objOppBizDocs.ShipToState = stateID;
                                        objOppBizDocs.strShipFromCountry = "";
                                        objOppBizDocs.strShipFromState = "";
                                        objOppBizDocs.strShipToCountry = "";
                                        objOppBizDocs.strShipToState = "";
                                        objOppBizDocs.GetShippingAbbreviation();

                                        #endregion

                                        Shipping objShipping = new Shipping();
                                        objShipping.DomainID = Sites.ToLong(HttpContext.Current.Session["DomainID"]);
                                        objShipping.WeightInLbs = Sites.ToDouble(TotalWeight);
                                        objShipping.NoOfPackages = 1;
                                        objShipping.UseDimentions = Sites.ToInteger(dr["numShippingCompanyID"]) == 91 ? true : false;
                                        //for fedex dimentions are must
                                        objShipping.GroupCount = 1;

                                        objShipping.SenderState = objOppBizDocs.strShipFromState;
                                        objShipping.SenderZipCode = Sites.ToString(dtTableWarehouse.Rows[0]["vcWPinCode"]);
                                        objShipping.SenderCountryCode = objOppBizDocs.strShipFromCountry;

                                        objShipping.RecepientState = objOppBizDocs.strShipToState;
                                        objShipping.RecepientZipCode = zipCode;
                                        objShipping.RecepientCountryCode = objOppBizDocs.strShipToCountry;

                                        objShipping.ServiceType = short.Parse(Sites.ToString(dr["intNsoftEnum"]));
                                        objShipping.PackagingType = 21;
                                        //ptYourPackaging 
                                        objShipping.ItemCode = 3;
                                        objShipping.OppBizDocItemID = 0;
                                        objShipping.ID = 9999;
                                        objShipping.Provider = Sites.ToInteger(dr["numShippingCompanyID"]);

                                        DataTable dtShipRates = new DataTable();
                                        if (string.Compare(objShipping.SenderCountryCode, objShipping.RecepientCountryCode) == 0 || string.IsNullOrEmpty(objShipping.SenderCountryCode) == true || string.IsNullOrEmpty(objShipping.RecepientCountryCode) == true)
                                        {
                                            dtShipRates = objShipping.GetRates(false);
                                        }
                                        else
                                        {
                                            dtShipRates = objShipping.GetRates(true);
                                        }

                                        if (!string.IsNullOrEmpty(objShipping.ErrorMsg))
                                        {
                                            if (objShipping.ErrorMsg.Contains("612") || objShipping.ErrorMsg.Contains("Weight below minimum requirement of 151 LB.") || objShipping.ErrorMsg.Contains("The selected service type is not valid"))
                                            {
                                                dr["vcServiceName"] = Sites.ToString(dr["vcServiceName"]) + " - " + "Not Supported";
                                                dr["vcServiceTypeID1"] = Sites.ToString(dr["numServiceTypeID"]) + "~" + "-1" + "~" + Sites.ToString(dr["numRuleID"]) + "~" + Sites.ToString(dr["numShippingCompanyID"]) + "~" + Sites.ToString(dr["vcServiceName"]);
                                            }
                                            else if (objShipping.ErrorMsg.Contains("836") || objShipping.ErrorMsg.Contains("Destination Postal-State Mismatch."))
                                            {
                                                ShowMessage("Please enter a valid postal code in the address.", 1);
                                                dtShippingMethod.Clear();
                                                return;
                                            }
                                        }


                                        if (dtShipRates.Rows.Count > 0)
                                        {
                                            if (Sites.ToDecimal(dtShipRates.Rows[0]["Rate"]) > 0)
                                            {
                                                //Currency conversion of shipping rate
                                                decShipingAmount = Sites.ToDecimal(dtShipRates.Rows[0]["Rate"]) / Sites.ToDecimal(HttpContext.Current.Session["ExchangeRate"].ToString());

                                                if (Sites.ToBool(dr["bitMarkupType"]) == true && Sites.ToDecimal(dr["fltMarkup"]) > 0)
                                                {
                                                    dr["fltMarkup"] = decShipingAmount / Sites.ToDecimal(dr["fltMarkup"]);
                                                }
                                                //Percentage
                                                decShipingAmount = decShipingAmount + Sites.ToDecimal(dr["fltMarkup"]);

                                                if (ShowShippingAmount)
                                                {
                                                    dr["vcServiceName"] = Sites.ToString(dr["vcServiceName"]) + " - " + Sites.ToString(Session["CurrSymbol"]) + " " + string.Format("{0:#,##0.00}", decShipingAmount);
                                                }
                                                dr["vcServiceTypeID1"] = Sites.ToString(dr["numServiceTypeID"]) + "~" + Sites.ToString(decShipingAmount) + "~" + Sites.ToString(dr["numRuleID"]) + "~" + Sites.ToString(dr["numShippingCompanyID"]) + "~" + Sites.ToString(dr["vcServiceName"]);
                                            }
                                        }
                                        else
                                        {
                                            dr["IsShippingRuleValid"] = false;
                                        }
                                    }
                                    else
                                    {
                                        dr["IsShippingRuleValid"] = false;
                                    }
                                }
                                else
                                {
                                    Page.MaintainScrollPositionOnPostBack = false;
                                    ShowMessage(GetErrorMessage("ERR069"), 1);// your merchant need to provide valid ship from warehouse address to calculate shipping amount,please contact site administrator.
                                }
                            }
                            else
                            {
                                Page.MaintainScrollPositionOnPostBack = false;
                                ShowMessage(GetErrorMessage("ERR069"), 1);// your merchant need to provide valid ship from warehouse address to calculate shipping amount,please contact site administrator.
                            }
                            #endregion
                        }
                    }
                    #endregion
                    if (subtotalItemClassificationAmount > 0)
                    {
                        DataRow drClassificationRow;
                        drClassificationRow = dtShippingMethod.NewRow();
                        drClassificationRow["vcServiceTypeID1"] = 0 + "~" + CCommon.ToString(subtotalItemClassificationAmount) + "~0~0~0~" + "-";
                        if (ShowShippingAmount)
                        {
                            drClassificationRow["vcServiceName"] = " - " + " - " + Sites.ToString(HttpContext.Current.Session["CurrSymbol"]) + " " + String.Format("{0:#,##0.00}", subtotalItemClassificationAmount);
                        }
                        else
                        {
                            drClassificationRow["vcServiceName"] = "-";
                        }
                        drClassificationRow["IsShippingRuleValid"] = true;
                        dtShippingMethod.Rows.Add(drClassificationRow);
                    }
                    //Select only those record whose IsShippingRuleValid(New Added Column) = true
                    DataRow[] datarows = dtShippingMethod.Select("IsShippingRuleValid=true");

                    if (datarows.Length > 0)
                    {
                        dtShippingMethod = datarows.CopyToDataTable();
                    }
                }


                if (dtShippingMethod == null || dtShippingMethod.Rows.Count == 0)
                {
                    ddlShippingMethod.Items.Clear();  // It is necessary otherwise it will show dublicate records .

                    ListItem list = new ListItem();
                    list.Selected = true;
                    list.Text = "NA - $0";
                    list.Value = "0~0.00";
                    ddlShippingMethod.Items.Add(list);
                    ddlShippingMethod.ClearSelection();
                }
                else
                {
                    ddlShippingMethod.Items.Clear();

                    ddlShippingMethod.DataTextField = "vcServiceName";
                    ddlShippingMethod.DataValueField = "vcServiceTypeID1";
                    ddlShippingMethod.DataSource = dtShippingMethod;
                    ddlShippingMethod.DataBind();
                }
                ddlShippingMethod.Items.Insert(0, new ListItem("--Select One--", "0~0.00~0"));


                if (ddlShippingMethod.Items.Count > 1 && hdnIsStaticShippingCharge.Value == "0")
                {
                    if (hdnShippingMethod.Value != "" && ddlShippingMethod.Items.FindByValue(hdnShippingMethod.Value) != null)
                    {
                        ddlShippingMethod.Items.FindByValue(hdnShippingMethod.Value).Selected = true;
                    }
                    else if (CCommon.ToString(Session["ShippingMethodValue"]) != "" && ddlShippingMethod.Items.FindByValue(CCommon.ToString(Session["ShippingMethodValue"])) != null)
                    {
                        ddlShippingMethod.Items.FindByValue(CCommon.ToString(Session["ShippingMethodValue"])).Selected = true;
                    }
                    else if (hdnShippingMethod.Value != "" || CCommon.ToString(Session["ShippingMethodValue"]) != "")
                    {
                        ddlShippingMethod.SelectedIndex = 0;
                    }
                    else
                    {
                        ddlShippingMethod.SelectedIndex = 1;
                    }
                }

                if (strMessage.Length > 0)
                {
                    Page.MaintainScrollPositionOnPostBack = false;
                    ShowMessage(strMessage, 1);
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        private void CreateShippingReport()
        {
            try
            {
                DataTable dt = default(DataTable);
                DataSet ds;
                string strServiceType = "";
                long lngShipingCompany = 0;

                ds = GetCartItem(); //(DataSet)Session["Data"];
                dt = ds.Tables[0];
                OppBizDocs objOppBizDocs = new OppBizDocs();

                DataTable dtFields = new DataTable();
                dtFields.Columns.Add("numItemCode");
                dtFields.Columns.Add("tintServiceType");
                dtFields.Columns.Add("dtDeliveryDate");
                dtFields.Columns.Add("monShippingRate");
                dtFields.Columns.Add("fltTotalWeight");
                dtFields.Columns.Add("intNoOfBox");
                dtFields.Columns.Add("fltHeight");
                dtFields.Columns.Add("fltWidth");
                dtFields.Columns.Add("fltLength");
                dtFields.Columns.Add("numOppBizDocItemID");
                dtFields.TableName = "Items";

                //Create shipping report
                foreach (DataRow dr in dt.Rows)
                {
                    if (Sites.ToLong(dr["numServiceTypeID"]) > 0)
                    {
                        DataRow dtRow = dtFields.NewRow();
                        dtRow["numItemCode"] = dr["numItemCode"].ToString();
                        dtRow["tintServiceType"] = dr["tintServiceType"].ToString();
                        dtRow["dtDeliveryDate"] = DateTime.Parse(dr["dtDeliveryDate"].ToString()).ToString("yyyy-MM-dd HH:mm:ss.000");
                        dtRow["monShippingRate"] = dr["decShippingCharge"].ToString();
                        dtRow["fltTotalWeight"] = dr["numWeight"].ToString();
                        dtRow["intNoOfBox"] = dr["numUnitHour"].ToString();
                        dtRow["fltHeight"] = dr["numHeight"].ToString();
                        dtRow["fltWidth"] = dr["numWidth"].ToString();
                        dtRow["fltLength"] = dr["numLength"].ToString();
                        dtRow["numOppBizDocItemID"] = dr["numOppItemCode"].ToString();

                        dtFields.Rows.Add(dtRow);
                        if (Sites.ToInteger(dr["tintServiceType"]) > 0)
                            strServiceType = Sites.ToString(dr["tintServiceType"]);
                        lngShipingCompany = Sites.ToLong(dr["numShippingCompany"]);
                    }
                }

                //Need to put some mechanism so that custom shipping method also can be tracked through shipping report 
                if (Sites.ToInteger(strServiceType) == 0)
                    return;

                DataTable dtTable = null;
                CContacts objContact = new CContacts();
                objContact.DomainID = Sites.ToLong(Session["DomainID"]);
                objContact.AddressID = Sites.ToLong(Session["ShipAddress"]);
                objContact.byteMode = 1;
                dtTable = objContact.GetAddressDetail();


                //Get default warehouse address
                CItems objItem = new CItems();
                DataTable dtWarehouse = null;
                objItem.WarehouseID = Sites.ToLong(HttpContext.Current.Session["DefaultWareHouseID"]);
                objItem.DomainID = Sites.ToLong(HttpContext.Current.Session["DomainID"]);
                dtWarehouse = objItem.GetWareHouses();


                objOppBizDocs.DomainID = Sites.ToLong(Session["DomainID"]);
                objOppBizDocs.OppBizDocId = Sites.ToLong(Session["OppBizDocID"]);
                objOppBizDocs.ShipCompany = lngShipingCompany;
                objOppBizDocs.Value2 = strServiceType;
                objOppBizDocs.FromState = Sites.ToString(dtWarehouse.Rows[0]["numWState"]);
                objOppBizDocs.FromZip = Sites.ToString(dtWarehouse.Rows[0]["vcWPincode"]);
                objOppBizDocs.FromCountry = Sites.ToString(dtWarehouse.Rows[0]["numWCountry"]);
                objOppBizDocs.ToState = Sites.ToString(dtTable.Rows[0]["numState"]);
                objOppBizDocs.ToZip = Sites.ToString(dtTable.Rows[0]["vcPostalCode"]); ;
                objOppBizDocs.ToCountry = Sites.ToString(dtTable.Rows[0]["numCountry"]);


                objOppBizDocs.UserCntID = lngContId;
                DataSet ds1 = new DataSet();
                ds1.Tables.Add(dtFields.Copy());
                objOppBizDocs.strText = ds1.GetXml();
                objOppBizDocs.byteMode = 1;//to update numOppBizDocItemID
                objOppBizDocs.ShippingReportId = objOppBizDocs.ManageShippingReport();
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        //Get cart total amount after deducting tax amount . kishan
        private decimal GetCartTotalAmount()
        {
            try
            {
                DataSet ds;
                ds = GetCartItem(); //(DataSet)Session["Data"];
                decimal TotalAmount = 0;
                decimal Tax = 0;

                if (ds.Tables.Count > 0)
                {
                    DataTable dt = new DataTable();
                    dt = ds.Tables[0];
                    //DataTable dtTotal = new DataTable();
                    //DataView dv = new DataView();
                    //dv = dt.DefaultView;
                    //dv.RowFilter = "vcItemType NOT LIKE '%service%'";
                    //dtTotal = dv.ToTable();

                    if (dt.Rows.Count > 0)
                    {
                        TotalAmount = Sites.ToDecimal(dt.Compute("SUM(monTotAmount)", ""));
                    }
                }

                if (TotalAmount > 0)
                {
                    Tax = GetTax();
                }

                if (Tax > 0)
                {
                    TotalAmount = TotalAmount + Tax;
                }
                return TotalAmount;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        private decimal GetTax()
        {
            try
            {
                DataTable dtTable = null;
                CContacts objContact = new CContacts();
                objContact.DomainID = Sites.ToLong(Session["DomainID"]);
                if (CCommon.ToShort(Session["BaseTaxOn"]) == 1)
                {
                    objContact.AddressID = Sites.ToLong(Session["BillAddress"]);
                }
                else if (CCommon.ToShort(Session["BaseTaxOn"]) == 2)
                {
                    if (ddlShippingMethod.SelectedItem != null)
                    {
                        string[] strShippingCharge = Sites.ToString(ddlShippingMethod.SelectedValue).Split('~');


                        if (strShippingCharge.Length >= 4)
                        {
                            if (Sites.ToInteger(strShippingCharge[3]) == 92)
                            {
                                CItems objItem = new CItems();
                                objItem.WarehouseID = Sites.ToLong(HttpContext.Current.Session["DefaultWareHouseID"]);
                                objItem.DomainID = Sites.ToLong(HttpContext.Current.Session["DomainID"]);
                                DataTable dtTableWarehouse = objItem.GetWareHouses();

                                if (dtTableWarehouse != null && dtTableWarehouse.Rows.Count > 0)
                                {
                                    objContact.AddressID = Sites.ToLong(dtTableWarehouse.Rows[0]["numAddressID"]);
                                }
                                else
                                {
                                    objContact.AddressID = 0;
                                }
                            }
                        }
                        else
                        {
                            objContact.AddressID = Sites.ToLong(Session["ShipAddress"]);
                        }
                    }
                    else
                    {
                        objContact.AddressID = Sites.ToLong(Session["ShipAddress"]);
                    }
                }
                else
                {
                    objContact.AddressID = 0;
                }

                objContact.byteMode = 1;
                dtTable = objContact.GetAddressDetail();

                decimal Tax = default(decimal);
                if (dtTable.Rows.Count > 0 && dcTotalTax <= 0)//Check because in on one page check out it throws Error ...
                {
                    Tax = GetTaxForCartItems(Sites.ToLong(Session["DomainID"]), Sites.ToLong(Session["DivId"]), Sites.ToInteger(Session["BaseTaxCalcOn"]), Convert.ToInt16(Session["BaseTaxOnArea"]), Sites.ToInteger(dtTable.Rows[0]["numCountry"]), Sites.ToInteger(dtTable.Rows[0]["numState"]), Sites.ToString(dtTable.Rows[0]["vcCity"]), Sites.ToString(dtTable.Rows[0]["vcPostalCode"]), Sites.ToLong(Session["UserContactID"]));
                }
                else if (dcTotalTax > 0)
                {
                    Tax = dcTotalTax;
                }
                else
                {
                    Tax = 0;
                }

                dcTotalTax = Tax;
                return Tax;
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        #endregion

        #region BizDoc and Custom Fields

        private void MakeDepositeEntries()
        {
            try
            {
                CItems objItems = new CItems();

                string strPaymentMethod = Sites.ToString(Session["PayOption"]).ToLower();
                switch (strPaymentMethod)
                {
                    case "billme":
                        objItems.PaymentMethod = Sites.ToLong(PaymentMethod.BillMe);
                        break;
                    case "creditcard":
                        objItems.PaymentMethod = Sites.ToLong(PaymentMethod.CreditCard);
                        break;
                    case "googlecheckout":
                        objItems.PaymentMethod = Sites.ToLong(PaymentMethod.GoogleCheckout);
                        break;
                    case "paypal":
                        objItems.PaymentMethod = Sites.ToLong(PaymentMethod.Paypal);
                        break;
                    case "salesinquiry":
                        objItems.PaymentMethod = Sites.ToLong(PaymentMethod.SalesInquiry);
                        break;
                    default:
                        break;
                }


                Sites objSites = new Sites();
                objSites.SiteID = Sites.ToLong(Session["SiteId"]);
                objSites.DomainID = Sites.ToLong(Session["DomainID"]);
                DataTable dteCommercePaymentConfig = objSites.GeteCommercePaymentConfig(objItems.PaymentMethod);

                if (dteCommercePaymentConfig != null && dteCommercePaymentConfig.Rows.Count > 0 && Sites.ToLong(dteCommercePaymentConfig.Rows[0]["numBizDocId"]) > 0)
                {
                    if (Sites.ToLong(dteCommercePaymentConfig.Rows[0]["numBizDocId"]) == Sites.ToLong(Session["AuthSalesBizDoc"]))
                    {
                        if (objItems.PaymentMethod == Sites.ToLong(PaymentMethod.CreditCard) || objItems.PaymentMethod == Sites.ToLong(PaymentMethod.GoogleCheckout) || objItems.PaymentMethod == Sites.ToLong(PaymentMethod.Paypal))
                            MakeDepositEntry(lngDivID, Sites.ToDecimal(Sites.ToString(Session["TotalAmount"]).Replace(",", "")), System.DateTime.Now, lngTransactionHistoryID, Sites.ToString(Session["OppName"]), Sites.ToString(objItems.PaymentMethod), true);
                    }
                    else
                    {
                        if (objItems.PaymentMethod == Sites.ToLong(PaymentMethod.CreditCard) || objItems.PaymentMethod == Sites.ToLong(PaymentMethod.GoogleCheckout) || objItems.PaymentMethod == Sites.ToLong(PaymentMethod.Paypal))
                            MakeDepositEntry(lngDivID, Sites.ToDecimal(Sites.ToString(Session["TotalAmount"]).Replace(",", "")), System.DateTime.Now, lngTransactionHistoryID, Sites.ToString(Session["OppName"]), Sites.ToString(objItems.PaymentMethod), false);
                    }
                }
                else
                {
                    if (objItems.PaymentMethod == Sites.ToLong(PaymentMethod.CreditCard) || objItems.PaymentMethod == Sites.ToLong(PaymentMethod.GoogleCheckout) || objItems.PaymentMethod == Sites.ToLong(PaymentMethod.Paypal))
                        MakeDepositEntry(lngDivID, Sites.ToDecimal(Sites.ToString(Session["TotalAmount"]).Replace(",", "")), System.DateTime.Now, lngTransactionHistoryID, Sites.ToString(Session["OppName"]), Sites.ToString(objItems.PaymentMethod), false);
                }
            }
            catch (Exception ex)
            {
                ExceptionModule.ExceptionPublish(ex, Sites.ToLong(Session["DomainID"]), Sites.ToLong(Session["UserContactID"]), Sites.ToLong(Session["SiteId"]), Request);
                throw;
            }
        }

        public void UploadFile(string URLType, string strFileType, string strFileName, string DocName, string DocDesc, long DocumentStatus, string DocCategory)
        {
            try
            {
                string[] arrOutPut = null;
                DocumentList objDocuments = new DocumentList();

                objDocuments.DomainID = Sites.ToInteger(Session["DomainID"]);
                objDocuments.UserCntID = Sites.ToLong(Session["UserContactID"]);
                objDocuments.UrlType = URLType;
                objDocuments.DocumentStatus = DocumentStatus;
                objDocuments.DocCategory = 370;
                objDocuments.FileType = strFileType;
                objDocuments.DocName = DocName;
                objDocuments.DocDesc = DocDesc;
                objDocuments.FileName = strFileName;
                objDocuments.DocumentSection = "O";
                int lngRecID = Sites.ToInteger(Session["OppID"]);
                objDocuments.RecID = lngRecID;
                objDocuments.DocumentType = 1;
                //1=generic,2=specific
                arrOutPut = objDocuments.SaveDocuments();
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        private void UpdateAmountPaid(string ReturnTransactionID)
        {
            if (Sites.ToDecimal(Session["TotalAmount"]) > 0)
            {
                try
                {
                    OppInvoice objOppInvoice = new OppInvoice();
                    objOppInvoice.AmtPaid = Sites.ToDecimal(Session["TotalAmount"]);
                    objOppInvoice.UserCntID = Sites.ToLong(Session["UserContactID"]);
                    objOppInvoice.OppBizDocId = Sites.ToLong(Session["OppBizDocID"]);
                    objOppInvoice.OppId = Sites.ToLong(Session["OppID"]);
                    objOppInvoice.PaymentMethod = 1;//credit card
                    objOppInvoice.BizDocsPaymentDetId = 0;
                    objOppInvoice.CardTypeID = lngCardTypeID;
                    objOppInvoice.Reference = ReturnTransactionID;
                    objOppInvoice.IntegratedToAcnt = false;
                    objOppInvoice.Memo = "";
                    objOppInvoice.DomainID = Sites.ToInteger(Session["DomainID"]);
                    objOppInvoice.DeferredIncomeStartDate = System.DateTime.UtcNow;
                    objOppInvoice.IsCardAutorized = Sites.ToBool(Application["CreditCardAuthOnly"]) == true ? true : false;
                    objOppInvoice.IsAmountCaptured = false;
                    objOppInvoice.UpdateAmountPaid();
                }
                catch (Exception ex)
                {
                    Response.Write(ex);
                }
            }
        }

        private void BindCustomField(ref string strUI)
        {
            try
            {
                FormConfigWizard objFormWizard = new FormConfigWizard();
                objFormWizard.DomainID = Sites.ToLong(Session["DomainID"]);
                objFormWizard.LocationIds = "2"; //sales opportunity 
                dtCustFld = objFormWizard.GetCustomFormFields();
                GenerateGenericFormControls.DomainID = Sites.ToLong(Session["DomainID"]);

                string FldName = "";
                foreach (DataRow row in dtCustFld.Rows)
                {
                    FldName = "##" + Sites.ToString(row["vcFormFieldName"]).Trim().Replace(" ", "") + "_" + Sites.ToString(row["vcFieldType"]) + "##";
                    if (strUI.Contains(FldName))
                    {
                        Control c = null;
                        c = (Control)GenerateGenericFormControls.getDynamicControlAndData(Sites.ToInteger(Sites.ToString(row["numFormFieldId"]).Replace("R", "").Replace("C", "").Replace("D", "")), row["vcFormFieldName"].ToString().Trim().Replace(" ", "") + "_" + row["vcFieldType"].ToString(), row["vcListItemType"].ToString(), row["vcAssociatedControlType"].ToString(), Sites.ToInteger(row["numListID"]), 0, 0);

                        if (plhFormControls.FindControl(c.ID) == null)
                        {
                            plhFormControls.Controls.Add(c);
                        }
                        else
                        {
                            c = plhFormControls.FindControl(c.ID);
                        }
                        strUI = strUI.Replace(FldName, CCommon.RenderControl(c));
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        private string GetCustFldValue(string fldType, string fld_Id)
        {
            try
            {
                if (fldType == "TextBox")
                {
                    TextBox txt = default(TextBox);
                    txt = (TextBox)plhFormControls.FindControl(fld_Id);
                    return txt.Text;
                }
                else if (fldType == "SelectBox")
                {
                    DropDownList ddl = default(DropDownList);
                    ddl = (DropDownList)plhFormControls.FindControl(fld_Id);
                    return (string)(string.IsNullOrEmpty(ddl.SelectedItem.Value) ? "0" : ddl.SelectedItem.Value);
                }
                else if (fldType == "TextArea")
                {
                    TextBox txt = default(TextBox);
                    txt = (TextBox)plhFormControls.FindControl(fld_Id);
                    return txt.Text;
                }
                else if (fldType == "CheckBox")
                {
                    CheckBox chk = default(CheckBox);
                    chk = (CheckBox)plhFormControls.FindControl(fld_Id);
                    return Sites.ToString(chk.Checked);
                }
                return "";
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        #endregion

        #region Payment Method Selection

        private void SaveCustomerCreditCardInfo()
        {
            try
            {
                //if (CCommon.ToBool(Session["SaveCreditCardInfo"]))
                //{
                QueryStringValues objEncryption = new QueryStringValues();
                OppInvoice objOppInvoice = new OppInvoice();

                objOppInvoice.ContactID = Sites.ToLong(Session["UserContactID"]);
                objOppInvoice.CardHolder = objEncryption.Encrypt(txtCHName.Text.Trim());
                objOppInvoice.CardTypeID = Sites.ToLong(ddlCardType.SelectedValue);
                //objOppInvoice.CreditCardNumber = objEncryption.Encrypt(hdnCardNumber.Value.Trim());
                objOppInvoice.CreditCardNumber = objEncryption.Encrypt(Sites.ToString(Session["CardNumber"]));
                objOppInvoice.CVV2 = objEncryption.Encrypt(txtCardCVV2.Text.Trim());
                objOppInvoice.ValidMonth = short.Parse(ddlCardExpMonth.SelectedValue);
                objOppInvoice.ValidYear = short.Parse(ddlCardExpYear.SelectedValue);
                objOppInvoice.UserCntID = Sites.ToLong(Session["UserContactID"]);
                objOppInvoice.IsDefault = true;
                objOppInvoice.AddCustomerCreditCardInfo();
                //}
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        private void PaymentModeChanged()
        {
            try
            {
                if (hdnPaymentMethod.Value == "BillMe")
                {
                    //radBillMe.Visible = true;
                    Session["PayOption"] = "BillMe";
                }
                else if (hdnPaymentMethod.Value == "CreditCard")
                {
                    //radPay.Visible = true;
                    Session["PayOption"] = "CreditCard";
                }
                else if (hdnPaymentMethod.Value == "GoogleCheckout")
                {
                    Session["PayOption"] = "GoogleCheckout";
                }
                else if (hdnPaymentMethod.Value == "Paypal")
                {
                    Session["PayOption"] = "Paypal";
                }
                else if (hdnPaymentMethod.Value == "SalesInquiry")
                {
                    Session["PayOption"] = "SalesInquiry";
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        private void CheckCreditStatus()
        {
            try
            {
                CItems objItems = new CItems();
                double dblCredit = 0;
                double dblRemainingCredit = 0;
                objItems.DivisionID = lngDivID;
                dblCredit = objItems.GetCreditStatusofCompany();

                DataTable dtTable = default(DataTable);
                dtTable = objItems.GetAmountDue();
                lblBalDue.Text = Sites.ToString(Session["CurrSymbol"]) + " " + string.Format("{0:#,##0.00}", Sites.ToDecimal(Sites.ToString(Sites.ToDecimal(dtTable.Rows[0]["AmountDueSO"]) / Sites.ToDecimal(Sites.ToString(Session["ExchangeRate"])))));
                lblRemCredit.Text = Sites.ToString(Session["CurrSymbol"]) + " " + string.Format("{0:#,##0.00}", Sites.ToDecimal(Sites.ToString(Sites.ToDecimal(dtTable.Rows[0]["RemainingCredit"]) < 0 ? 0 : Sites.ToDecimal(dtTable.Rows[0]["RemainingCredit"]) / Sites.ToDecimal(Sites.ToString(Session["ExchangeRate"])))));// string.Format("{0:#,##0.00}", dtTable.Rows[0]["RemainingCredit"]);
                lblAmtPastDue.Text = Sites.ToString(Session["CurrSymbol"]) + " " + string.Format("{0:#,##0.00}", Sites.ToDecimal(Sites.ToString(Sites.ToDecimal(dtTable.Rows[0]["AmountPastDueSO"]) / Sites.ToDecimal(Sites.ToString(Session["ExchangeRate"])))));

                dblRemainingCredit = Sites.ToDouble(dtTable.Rows[0]["RemainingCredit"]) < 0 ? 0 : Sites.ToDouble(dtTable.Rows[0]["RemainingCredit"]) / Sites.ToDouble(Sites.ToString(Session["ExchangeRate"]));

                txtCredit.Text = Sites.ToString(dblCredit); //Sites.ToString(dblRemainingCredit);  
                txtDueAmount.Text = dtTable.Rows[0]["AmountDueSO"].ToString();
                //}
                PaymentModeChanged();
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        private void BindCardType()
        {
            try
            {

                CCommon objCommon = new CCommon();
                objCommon.sb_FillComboFromDB(ref ddlCardType, 120, Sites.ToLong(Session["DomainID"]));
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        private void LoadDefaultCard()
        {
            try
            {
                QueryStringValues objEncryption = new QueryStringValues();
                OppInvoice objoppinvoice = new OppInvoice();
                objoppinvoice.bitflag = true;
                objoppinvoice.DomainID = Sites.ToInteger(Session["DomainId"]);
                objoppinvoice.IsDefault = true;
                objoppinvoice.UserCntID = Sites.ToLong(Session["UserContactID"]);
                DataSet ds = objoppinvoice.GetCustomerCreditCardInfo();
                if (ds != null && ds.Tables.Count > 0)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        txtCHName.Text = objEncryption.Decrypt(Sites.ToString(ds.Tables[0].Rows[0]["vcCardHolder"]));
                        hdnCardNumber.Value = objEncryption.Decrypt(Sites.ToString(ds.Tables[0].Rows[0]["vcCreditCardNo"]));
                        Session["CardNumber"] = objEncryption.Decrypt(Sites.ToString(ds.Tables[0].Rows[0]["vcCreditCardNo"]));

                        if (Sites.ToString(Session["CardNumber"]).Length > 12)
                        {
                            txtCardNumber.Text = "############" + Sites.ToString(Session["CardNumber"]).Substring(12, Sites.ToString(Session["CardNumber"]).Length - 12); // objEncryption.Decrypt(Sites.ToString(ds.Tables[0].Rows[0]["vcCreditCardNo"]));

                        }
                        //if (hdnCardNumber.Value.Length > 12)
                        //{
                        //    txtCardNumber.Text = "############" + Session["CardNumber"].Substring(12, hdnCardNumber.Value.Length - 12); // objEncryption.Decrypt(Sites.ToString(ds.Tables[0].Rows[0]["vcCreditCardNo"]));
                        //}

                        //if (hdnCardNumber.Value.Length > 12)
                        //{
                        //    txtCardNumber.Text = "############" + hdnCardNumber.Value.Substring(12, hdnCardNumber.Value.Length-12); // objEncryption.Decrypt(Sites.ToString(ds.Tables[0].Rows[0]["vcCreditCardNo"]));
                        //}

                        //txtCardCVV2.Text = objEncryption.Decrypt(Sites.ToString(ds.Tables[0].Rows[0]["vcCVV2"]));
                        ddlCardType.ClearSelection();
                        if (ddlCardType.Items.FindByValue(Sites.ToString(ds.Tables[0].Rows[0]["numCardTypeId"])) != null)
                        {
                            ddlCardType.Items.FindByValue(Sites.ToString(ds.Tables[0].Rows[0]["numCardTypeId"])).Selected = true;
                        }
                        ddlCardExpYear.ClearSelection();
                        ddlCardExpMonth.ClearSelection();
                        if (ddlCardExpMonth.Items.FindByValue(Sites.ToString(ds.Tables[0].Rows[0]["tintValidMonth"]).PadLeft(2, '0')) != null)
                        {
                            ddlCardExpMonth.Items.FindByValue(Sites.ToString(ds.Tables[0].Rows[0]["tintValidMonth"]).PadLeft(2, '0')).Selected = true;
                        }
                        if (ddlCardExpYear.Items.FindByValue(Sites.ToString(ds.Tables[0].Rows[0]["intValidYear"])) != null)
                        {
                            ddlCardExpYear.Items.FindByValue(Sites.ToString(ds.Tables[0].Rows[0]["intValidYear"])).Selected = true;
                        }
                        //chkDefault.Checked = Sites.ToBool(ds.Tables[0].Rows[0]["bitIsDefault"]);
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }


        }

        #endregion

        #region E-mail

        private void SendEmail()
        {
            try
            {
                Session["SMTPServerIntegration"] = true;
                //Send invoice by mail
                if (Sites.ToString(Session["UserEmail"]).Length > 4)
                {
                    /*Create PDF of Invoice */
                    System.IO.StringWriter sw = new System.IO.StringWriter();
                    Server.Execute("/Invoice.aspx?Show=True&OppID=" + Sites.ToString(Session["OppID"]) + "&RefType=1&Print=0", sw);
                    string htmlCodeToConvert = sw.GetStringBuilder().ToString();
                    string newHtml = "";

                    try
                    {
                        newHtml = htmlCodeToConvert.Substring(htmlCodeToConvert.IndexOf("<body>"), htmlCodeToConvert.IndexOf("</body>") - htmlCodeToConvert.IndexOf("<body>") + 7);
                        newHtml = "<html><head></head>" + newHtml + "</html>";
                        htmlCodeToConvert = newHtml;
                    }
                    catch
                    {

                    }

                    sw.Close();
                    HTMLToPDF objpdf = new HTMLToPDF();
                    string strFileName = objpdf.ConvertHTML2PDF(htmlCodeToConvert, Sites.ToLong(Session["DomainID"]), "http://" + Request.ServerVariables["SERVER_NAME"], false, 1, false);//HttpContext.Current.Request.Url.AbsoluteUri
                    DataRow dr;
                    DataTable dtTable = new DataTable();
                    dtTable.Columns.Add("Filename");
                    dtTable.Columns.Add("FileLocation");
                    dr = dtTable.NewRow();
                    if (Sites.ToString(Session["OppName"]).Length == 0)
                        Session["OppName"] = "Order-" + Sites.ToString(Session["OppID"]);

                    dr["Filename"] = Sites.ToString(Session["OppName"]) + ".pdf";
                    dr["FileLocation"] = CCommon.GetDocumentPhysicalPath(Sites.ToLong(Session["DomainID"])) + strFileName;
                    dtTable.Rows.Add(dr);
                    Session["Attachements"] = dtTable;
                    Email objSendEmail = new Email();

                    objSendEmail.DomainID = Sites.ToLong(Session["DomainID"]);
                    objSendEmail.TemplateCode = "#SYS#ECOMMERCE_BIZDOC_ITEMS";
                    objSendEmail.GetEmailTemplateByCode(objSendEmail.TemplateCode, objSendEmail.DomainID);

                    if (objSendEmail.TemplateBody.Contains("forloop=\"yes\""))
                    {
                        string pattern = "<([A-Za-z][A-Z0-9a-z]*)\\b[^>]*forloop=\"yes\"[^>]*?>[\\s\\S]*?</\\1>";
                        MatchCollection matches = Regex.Matches(objSendEmail.TemplateBody, pattern);

                        if ((matches.Count > 0))
                        {
                            DataTable dt = default(DataTable);
                            DataSet ds;
                            ds = GetCartItem(); //(DataSet)Session["Data"];
                            dt = ds.Tables[0];

                            string strReplace = matches[0].Value;
                            StringBuilder sb = new StringBuilder();
                            string strTemp = string.Empty;
                            foreach (DataRow dr1 in dt.Rows)
                            {
                                strTemp = strReplace;
                                if (Sites.ToString(dr1["vcItemName"]).Length > 100)
                                {
                                    strTemp = strTemp.Replace("##ItemName##", Sites.ToString(dr1["vcItemName"]).Substring(0, Sites.ToInteger(Sites.ToString(dr1["vcItemName"]).Length / 2)));
                                }
                                else
                                {
                                    strTemp = strTemp.Replace("##ItemName##", Sites.ToString(dr1["vcItemName"]).Substring(0, Sites.ToString(dr1["vcItemName"]).Length));
                                }
                                //strTemp = strTemp.Replace("##ItemName##", dr1["vcItemName"].ToString().Substring(0, 30));
                                strTemp = strTemp.Replace("##Price##", dr1["monPrice"].ToString());
                                strTemp = strTemp.Replace("##Unit##", dr1["numUnitHour"].ToString());
                                sb.AppendLine(strTemp);
                            }
                            objSendEmail.TemplateBody = objSendEmail.TemplateBody.Replace(strReplace, sb.ToString());
                        }
                    }

                    objSendEmail.DomainID = Sites.ToLong(Session["DomainID"]);
                    objSendEmail.TemplateCode = "#SYS#ECOMMERCE_SHOPPING_COMPLETED";

                    objSendEmail.ModuleID = 9;
                    objSendEmail.RecordIds = Sites.ToString(Session["OppID"]);
                    objSendEmail.FromEmail = Sites.ToString(Session["DomainAdminEmail"]);
                    objSendEmail.ToEmail = Sites.ToString(Session["UserEmail"]);
                    objSendEmail.IsAttachmentDelete = true;
                    objSendEmail.SendEmailTemplate(objSendEmail.TemplateBody);
                    Session["Attachements"] = null;
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        #endregion

        #region User Detail

        private int CheckForUserDetail()
        {
            string IsValid = "";
            int ErrorCode = 0;
            try
            {
                CContacts objContacts = new CContacts();
                DataTable dtTable1 = default(DataTable);
                objContacts.ContactID = Sites.ToLong(Session["UserContactID"]);
                objContacts.DomainID = Sites.ToLong(Session["DomainID"]);
                dtTable1 = objContacts.GetBillOrgorContAdd();
                if (Sites.ToBool(Session["SkipStep2"]) == true && IsOnlySalesInquiry() == 1)
                {
                    ErrorCode = 0;
                }
                else if (Sites.ToString(dtTable1.Rows[0]["vcFirstname"]) == "-")
                {
                    IsValid = "First name cannot be empty!.,";
                    ErrorCode = 1;
                }
                else if (Sites.ToString(dtTable1.Rows[0]["vcLastname"]) == "-")
                {
                    IsValid = "Last name cannot be empty!.,";
                    ErrorCode = 2;
                }
                else if (Sites.ToString(dtTable1.Rows[0]["vcFirstname"]).Length < 2)
                {
                    IsValid = "First name cannot be just a single charatcer!.,";
                    ErrorCode = 3;
                }
                else if (Sites.ToString(dtTable1.Rows[0]["vcLastName"]).Length < 2)
                {
                    IsValid = "Last  name cannot be just a single charatcer!.,";
                    ErrorCode = 4;
                }
                else if (CCommon.ToString(dtTable1.Rows[0]["vcShipCity"]).Length <= 0)
                {
                    IsValid = "Ship To City is not provided!.";
                    ErrorCode = 6;
                }
                else if (CCommon.ToLong(dtTable1.Rows[0]["vcShipState"]) <= 0)
                {
                    IsValid = "Ship To State is not provided!.";
                    ErrorCode = 7;
                }
                else if (CCommon.ToString(dtTable1.Rows[0]["vcShipPostCode"]).Length <= 0)
                {
                    IsValid = "Ship To Postal Code is not provided!.";
                    ErrorCode = 8;
                }
                else if (CCommon.ToLong(dtTable1.Rows[0]["vcShipCountry"]) <= 0)
                {
                    IsValid = "Ship To Country is not provided!.";
                    ErrorCode = 9;
                }
                //else
                //{
                //    ErrorCode = 5;
                //    //Response.Redirect("Login.aspx?ReturnURL=" + (Sites.ToString(Session["SiteLiveURL"]).Trim().Length > 2 ? "https://" : "http://") + Request.Url.Host + "/" + CheckoutPageName() + "&errcode=" + ErrorCode, true);
                //    //throw new Exception("Please Login to Continue..");
                //    ShowMessage("Please Login to Continue..", 0);
                //}
            }

            catch (Exception ex)
            {
                throw;
            }
            return ErrorCode;
        }

        #endregion

        #region Check Payment method

        private short IsOnlySalesInquiry()
        {
            UserAccess objUserAccess = new UserAccess();
            DataTable dtPaymentGateWay = new DataTable();
            objUserAccess.DomainID = Sites.ToLong(Session["DomainID"]);
            objUserAccess.byteMode = 1;
            dtPaymentGateWay = objUserAccess.ManageEComPaymentConfig(Sites.ToLong(Session["SiteId"]));
            if (dtPaymentGateWay != null && dtPaymentGateWay.Rows.Count > 0 && dtPaymentGateWay.Select("numPaymentMethodId = " + CCommon.ToInteger(PaymentMethod.SalesInquiry) + " AND bitEnable = 1").Length == 1 && dtPaymentGateWay.Select("bitEnable = 1").Length == 1)
            {
                return 1;
            }
            else if (dtPaymentGateWay != null && dtPaymentGateWay.Rows.Count > 0 && dtPaymentGateWay.Select("numPaymentMethodId = " + CCommon.ToInteger(PaymentMethod.SalesInquiry) + " AND bitEnable = 1").Length == 0 && dtPaymentGateWay.Select("bitEnable = 1").Length > 0)
            {
                return 2;
            }
            else
            {
                return 0;
            }
        }

        private void PaypalCompletePayment(string token)
        {
            #region Check for Paypal Configuration
            try
            {
                UserAccess objUserAccess = new UserAccess();
                objUserAccess.DomainID = Sites.ToLong(Session["DomainID"]);
                objUserAccess.SiteID = Sites.ToLong(Session["SiteId"]);
                string strPaypalUserName = "";
                string strPaypalPassword = "";
                string strPaypalSignature = "";
                bool IsPaypalSandbox = true;

                DataTable dtECommerceDetail = objUserAccess.GetECommerceDetails();

                if (dtECommerceDetail != null)
                {
                    strPaypalUserName = Sites.ToString(dtECommerceDetail.Rows[0]["vcPaypalUserName"]);
                    strPaypalPassword = Sites.ToString(dtECommerceDetail.Rows[0]["vcPaypalPassword"]);
                    strPaypalSignature = Sites.ToString(dtECommerceDetail.Rows[0]["vcPaypalSignature"]);
                    IsPaypalSandbox = Sites.ToBool(dtECommerceDetail.Rows[0]["IsPaypalSandbox"]);
                }

                if (strPaypalUserName == "" || strPaypalPassword == "" || strPaypalSignature == "")
                {
                    IsPaypalConfigured = false;
                    ShowMessage(GetErrorMessage("ERR073"), 1); // Paypal Merchant Account not Configured
                    return;
                }
            #endregion

                if (IsPaypalConfigured)
                {
                    decimal OrderTotal = GetCartTotalAmount();
                    if (IsPaypalSandbox)
                    {
                        PaypalServerUrl = Sites.ToString(System.Configuration.ConfigurationManager.AppSettings["PaypalSandboxUrl"]);
                        // expresscheckout1.URL = "https://api-3t.sandbox.paypal.com/nvp"; //Test server URL

                    }
                    else
                    {
                        PaypalServerUrl = Sites.ToString(System.Configuration.ConfigurationManager.AppSettings["PaypalServerUrl"]);
                        // expresscheckout1.URL =  "https://www.paypal.com/cgi-bin/webscr"; //Paypal server URL
                    }
                    expresscheckout1.URL = PaypalServerUrl;
                    expresscheckout1.User = strPaypalUserName;
                    expresscheckout1.Password = strPaypalPassword;
                    expresscheckout1.Signature = strPaypalSignature;
                    //expresscheckout1.User = "jxavier.mca-facilitator_api1.gmail.com";
                    //expresscheckout1.Password = "1371651348";
                    //expresscheckout1.Signature = "AFcWxV21C7fd0v3bYYYRCpSSRl31A9aGptwdTWtuPS0gGenvixMLuh4Y";
                    expresscheckout1.OrderTotal = Sites.ToString(Math.Round(OrderTotal, 2));
                    expresscheckout1.Token = token;
                    expresscheckout1.GetCheckoutDetails();

                    if (expresscheckout1.Ack.Equals("Success"))
                    {

                        ////The "Payer" properties contain information about the payer
                        //lblPayerName.Text = expresscheckout1.Payer.FirstName + " " + expresscheckout1.Payer.LastName;
                        //lblPayerEmail.Text = expresscheckout1.Payer.Email;

                        //capture the payment
                        expresscheckout1.PaymentAction = ExpresscheckoutPaymentActions.aSale;
                        expresscheckout1.Token = token;
                        expresscheckout1.OrderTotal = Sites.ToString(Math.Round(OrderTotal, 2));
                        expresscheckout1.CheckoutPayment();

                        ////The "Payment" properties contain information about the payment
                        //lblDate.Text = expresscheckout1.Payment.Date;
                        //lblGrossAmount.Text = expresscheckout1.Payment.GrossAmount;
                        //lblFee.Text = expresscheckout1.Payment.FeeAmount;

                        strQueryString = ""; //This should be null otherwise it will redirect to Thank You Page.
                        DataSet dsCart = GetCartItem();
                        if (dsCart != null)
                        {
                            MakeDepositeEntries();
                            CreateShippingReport();
                            strQueryString = "OppID=" + Convert.ToString(Session["OppID"]);
                            Session["TOppId"] = Session["OppID"];
                            Session["TOppName"] = Session["OppName"];
                            Session["TotalAffliateAmount"] = GetCartTotalAmount();
                            Session["CartItemsForAffliate"] = GetCartItem();
                            ClearCartItems();
                        }
                    }
                    else
                    {
                        ShowMessage(GetErrorMessage("ERR076"), 1); // Paypal Payment Failure.
                        strQueryString = "PayPal: " + expresscheckout1.Ack + ", Payment Status: " + expresscheckout1.Payment.Status;
                        return;
                    }
                }

                Session["ShippingMethodValue"] = null;
                Session["PayOption"] = null;//PayOptio null after placing order.

                //Add code to open Post-Up Sell Dialog with Items
                if (Sites.ToBool(Session["PostUpSell"]) == true)
                {
                    ClientScriptManager cs = Page.ClientScript;
                    cs.RegisterStartupScript(this.GetType(), "PlaceOrder", "javascript:__doPostBack('PlaceOrder','');", true);
                }
                else
                {
                    if (Sites.ToString(Session["vcRedirectThankYouUrl"]) != "")
                    {
                        Response.Redirect("thankyou", false);
                    }
                    else
                    {
                        Response.Redirect("ThankYou.aspx?" + strQueryString + "&rt=3", false);
                    }
                }

            }
            catch (Exception ex)
            {

                ShowMessage(GetErrorMessage("ERR076"), 1); // Paypal Payment Failure.
                return;
            }
        }

        private void PaypalPaymentCancel()
        {
            ShowMessage(GetErrorMessage("ERR076"), 1); // Paypal Payment Failure.
        }

        #endregion

        #endregion
    }
}
