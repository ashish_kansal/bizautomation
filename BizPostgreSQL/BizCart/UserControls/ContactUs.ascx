﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ContactUs.ascx.cs" Inherits="BizCart.UserControls.ContactUs" %>
<asp:Panel ID="pnlCutomizeHtml" runat="server">
    <table width="100%">
        <tr>
            <td>
                <div class="sectionheader">
                    <asp:Label ID="lblTitle" runat="server" Text="Contact Us"></asp:Label>
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="lblAddress" runat="server" Text=""></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                Contact Number:
                <asp:Label ID="lblPhone" runat="server" Text=""></asp:Label>
            </td>
        </tr>
    </table>
</asp:Panel>
<asp:Literal ID="litCutomizeHtml" runat="server"></asp:Literal>
