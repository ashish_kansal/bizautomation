﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BACRM.BusinessLogic.Accounting;
using BACRM.BusinessLogic.Admin;
using BACRM.BusinessLogic.Alerts;
using BACRM.BusinessLogic.Common;
using BACRM.BusinessLogic.Contacts;
using BACRM.BusinessLogic.Documents;
using BACRM.BusinessLogic.Item;
using BACRM.BusinessLogic.Opportunities;
using BACRM.BusinessLogic.ShioppingCart;
using System.Collections;
using System.Data;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Linq;
using BACRM.BusinessLogic.Workflow;

namespace BizCart.UserControls
{
    public partial class Blog : BizUserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                Sites.InitialiseSession();
                if (!IsPostBack)
                {
                    bindBlogs();
                }
            }
            catch (Exception ex)
            {
                ExceptionModule.ExceptionPublish(ex, Sites.ToLong(Session["DomainID"]), Sites.ToLong(Session["UserContactID"]), Sites.ToLong(Session["SiteId"]), Request);
                ShowMessage(ex.ToString(), 1);
            }
        }
        public void bindBlogs()
        {
            ContentManagement objCM = new ContentManagement();
            objCM.numContentID = 0;
            objCM.numSiteId = Sites.ToLong(Session["SiteId"]);
            objCM.numDomainID = Sites.ToLong(Session["DomainID"]);
            objCM.intMode = 0;
            objCM.intType = 2;
            DataTable dtCM = new DataTable();
            dtCM = objCM.GetEcommercePages();

            string pattern = "<##FOR(?<Property>(\\b[^>]*?))##>(?<Content>([\\s\\S]*?))<##ENDFOR##>";
            string RepeteTemplate = "";
            Hashtable htProperties;
            string strUI = GetUserControlTemplate("Blog.htm");
            CCommon objCommon = new CCommon();
            DataTable dt = new DataTable();
            objCommon.DomainID = Sites.ToLong(Session["DomainID"]);
            objCommon.ListID = Sites.ToLong(1344);
            dt = objCommon.GetMasterListItemsWithRights();
            string parentCategoryRelationship = "";
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    parentCategoryRelationship = parentCategoryRelationship + Sites.ToString(dr["vcListItemGroupName"]) + "#" + Sites.ToString(dr["numListItemID"]) + "#" + Sites.ToString(dr["vcData"]);
                }
            }
            strUI = strUI.Replace("##ParentChildCategoryName##", parentCategoryRelationship);
            string strTemp = "";
            StringBuilder sb = new StringBuilder();
            if (Regex.IsMatch(strUI, pattern))
            {
                MatchCollection matches = Regex.Matches(strUI, pattern);
                if ((matches.Count > 0))
                {
                    RepeteTemplate = matches[0].Groups["Content"].Value;
                    htProperties = GetPropertiesAsHastable(matches[0].Groups["Property"].Value.Replace("(", "").Replace(")", ""));

                    if (dtCM.Rows.Count > 0)
                    {
                        foreach (DataRow dr in dtCM.Rows)
                        {
                            strTemp = RepeteTemplate;
                            strTemp = strTemp.Replace("##BlogUrl##", Sites.ToString(dr["vcUrl"]));
                            strTemp = strTemp.Replace("##BlogTitle##", Sites.ToString(dr["vcPageTitle"]));
                            strTemp = strTemp.Replace("##CategoryName##", Sites.ToString(dr["vcCategoryName"]));
                            strTemp = strTemp.Replace("##CategoryId##", Sites.ToString(dr["numCategoryId"]));
                            strTemp = strTemp.Replace("##UpdatedBy##", Sites.ToString(dr["vcUpdatedBy"]));
                            strTemp = strTemp.Replace("##UpdatedOn##", Sites.ToString(dr["dtmUpdatedOn"]));
                            if (@Regex.Replace(Sites.ToString(dr["vcContent"]), @"<[^>]*>", "").Length > 500)
                            {
                                strTemp = strTemp.Replace("##BlogShortDescription##", @Regex.Replace(Sites.ToString(dr["vcContent"]), @"<[^>]*>", "").Substring(0, 499));
                            }
                            else
                            {
                                strTemp = strTemp.Replace("##BlogShortDescription##", @Regex.Replace(Sites.ToString(dr["vcContent"]), @"<[^>]*>", ""));
                            }
                            strTemp = strTemp.Replace("##BlogLongDescription##", Sites.ToString(dr["vcContent"]));
                            sb.Append(strTemp);
                        }
                        strUI = Regex.Replace(strUI, pattern, sb.ToString());
                    }
                }
            }

            
            litCutomizeHtml.Text = strUI;
        }

    }
}