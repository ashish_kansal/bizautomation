﻿using System;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BACRM.BusinessLogic.Common;
using BACRM.BusinessLogic.ShioppingCart;
using BACRM.BusinessLogic.Opportunities;
using System.Text;
using System.Text.RegularExpressions;

namespace BizCart.UserControls
{
    public partial class DisplayList : BizUserControl
    {
        public string Title { get; set; }
        //private long lngItemCode;
        //private int intUnits;
        private long lngSalesTemplateID;
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                Sites.InitialiseSession();
                if (!System.Web.HttpContext.Current.User.Identity.IsAuthenticated || CCommon.ToLong(Session["DomainID"]) == 0 || CCommon.ToLong(Session["UserContactID"]) == 0)
                {
                    Response.Redirect("Login.aspx?ReturnURL=" + Request.Url.Segments[Request.Url.Segments.Length - 1] + "?" + Server.UrlEncode(Request.QueryString.ToString()), true);
                }
                lngSalesTemplateID = Sites.ToLong(Request["ID"]);

                if (!IsPostBack)
                {
                    if (lngSalesTemplateID > 0)
                    {
                        BindListItems();
                    }
                    else if (Session["ListData"] != null)
                    {
                        BindWishListRadio();
                    }
                    bindHtml();
                }
                if (IsPostBack)
                {
                    if (Sites.ToString(Request["__EVENTTARGET"]) == "lnkDeleteListItem")
                    {
                        DeleteItem();
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionModule.ExceptionPublish(ex, Sites.ToLong(Session["DomainID"]), Sites.ToLong(Session["UserContactID"]), Sites.ToLong(Session["SiteId"]), Request);
                ShowMessage(ex.ToString(), 1);
            }
        }
        private void BindWishListRadio()
        {
            try
            {
                COpportunities objOpp = new COpportunities();
                DataTable dtTemplate;
                objOpp.DomainID = Sites.ToLong(Session["DomainID"]);
                objOpp.DivisionID = Sites.ToInteger(Session["DivId"]);
                objOpp.SalesTemplateID = 0;
                objOpp.OpportID = 0;
                objOpp.byteMode = 1;
                dtTemplate = objOpp.GetSalesTemplate();
                rblLists.DataTextField = "vcTemplateName";
                rblLists.DataValueField = "numSalesTemplateID";
                rblLists.DataSource = dtTemplate;
                rblLists.DataBind();
                if (rblLists.Items.Count > 0)
                    rblLists.Items[0].Selected = true;
                else
                    Response.Redirect("lists.aspx?action=addnew");

                pnlAdd.Visible = true;
                pnlViewList.Visible = false;

            }
            catch (Exception ex)
            {
                ExceptionModule.ExceptionPublish(ex, Sites.ToLong(Session["DomainID"]), Sites.ToLong(Session["UserContactID"]), Sites.ToLong(Session["SiteId"]), Request);
                ShowMessage(ex.ToString(), 1);
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (rdbNewCategory.Checked && txtCategoryName.Text.Trim().Length == 0)
                {
                    ShowMessage("Enter category name.", 1);
                    return;
                } else if (Sites.ToLong(rblLists.SelectedValue) == 0)
                {
                    ShowMessage(GetErrorMessage("ERR038"), 1);//Please select one list name
                    return;
                }
               
                if (Session["ListData"] != null)
                {

                    DataSet ds = new DataSet();
                    ds.Tables.Add(((DataTable)Session["ListData"]).Copy());

                    COpportunities objOpp = new COpportunities();
                    objOpp.DomainID = Sites.ToLong(Session["DomainID"]);
                    if (rdbNewCategory.Checked)
                    {
                        objOpp.DivisionID = Sites.ToInteger(Session["DivId"]);
                        objOpp.SalesTemplateID = 0;
                        objOpp.TemplateName = txtCategoryName.Text.Trim();
                        objOpp.TemplateType = 0;//company specific
                        objOpp.OpportID = 0;
                        objOpp.UserCntID = Sites.ToInteger(Session["UserContactID"]);
                        objOpp.ManageSalesTemplate();
                    }
                    else
                    {
                        objOpp.SalesTemplateID = Sites.ToLong(rblLists.SelectedValue);
                    }
                    objOpp.strItemDtls = ds.GetXml();
                    objOpp.ManageSalesTemplateItems();
                    Session["ListData"] = null;
                    lngSalesTemplateID = objOpp.SalesTemplateID;
                    BindListItems();
                    ShowMessage(GetErrorMessage("ERR039"), 0);//Item Added to list sucessfully
                    bindHtml();
                }
            }
            catch (Exception ex)
            {
                ExceptionModule.ExceptionPublish(ex, Sites.ToLong(Session["DomainID"]), Sites.ToLong(Session["UserContactID"]), Sites.ToLong(Session["SiteId"]), Request);
                ShowMessage(ex.ToString(), 1);
            }
        }
        private void BindListItems()
        {
            try
            {
                pnlAdd.Visible = false;
                pnlViewList.Visible = true;
            }
            catch (Exception ex)
            {
                ExceptionModule.ExceptionPublish(ex, Sites.ToLong(Session["DomainID"]), Sites.ToLong(Session["UserContactID"]), Sites.ToLong(Session["SiteId"]), Request);
                ShowMessage(ex.ToString(), 1);
            }
        }



        private void DeleteItem()
        {
            try
            {
                COpportunities objOpp = new COpportunities();
                objOpp.SalesTemplateItemID = Sites.ToLong(Sites.ToString(Request["__EVENTARGUMENT"]).Split(':')[0]);
                objOpp.DomainID = Sites.ToLong(Session["DomainID"]);
                objOpp.DeleteSalesTemplateItem();
                lngSalesTemplateID = Sites.ToLong(Sites.ToString(Request["__EVENTARGUMENT"]).Split(':')[1]);
                BindListItems();
                ShowMessage(GetErrorMessage("ERR040"), 0);//Item removed sucessfully
                bindHtml();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        void bindHtml()
        {
            try
            {

                string strUI = GetUserControlTemplate("AddToList.htm");

                string AddToListPattern, ViewListPattern;
                AddToListPattern = "{##AddToListSection##}(?<Content>([\\s\\S]*?)){/##AddToListSection##}";
                ViewListPattern = "{##ViewListSection##}(?<Content>([\\s\\S]*?)){/##ViewListSection##}";
                if (lngSalesTemplateID == 0)
                {
                    //Hide Other Template
                    strUI = Sites.RemoveMatchedPattern(strUI, ViewListPattern);
                    //Replace disply template ##Tags## with content value
                    strUI = Sites.ReplaceMatchedPatternWithContent(strUI, AddToListPattern);

                    strUI = strUI.Replace("##ChooseAListRadioButtonList##", CCommon.RenderControl(rblLists));
                    strUI = strUI.Replace("##NewListRadioButton##", CCommon.RenderControl(rdbNewCategory));
                    strUI = strUI.Replace("##NewListTextBox##", CCommon.RenderControl(txtCategoryName));
                    strUI = strUI.Replace("##AddToListButton##", CCommon.RenderControl(btnSave));

                }
                else if (lngSalesTemplateID > 0)
                {
                    strUI = Sites.RemoveMatchedPattern(strUI, AddToListPattern);
                    strUI = Sites.ReplaceMatchedPatternWithContent(strUI, ViewListPattern);

                    COpportunities objOpp = new COpportunities();
                    objOpp.DomainID = Sites.ToLong(Session["DomainID"]);
                    objOpp.SalesTemplateID = lngSalesTemplateID;
                    DataTable dtItems = objOpp.GetSalesTemplateItems();
                    objOpp.OpportID = 0;
                    objOpp.byteMode = 2;
                    DataTable dtTemplate = objOpp.GetSalesTemplate();
                    if (dtTemplate.Rows.Count > 0)
                    {
                        strUI = strUI.Replace("##ListName##", Sites.ToString(dtTemplate.Rows[0]["vcTemplateName"]));
                        strUI = strUI.Replace("##EmptyListMessage##", "");
                    }
                    else if (dtTemplate.Rows.Count == 0)
                        strUI = strUI.Replace("##EmptyListMessage##", "No items found"); ;


                    StringBuilder sb = new StringBuilder();
                    string pattern = "<##FOR##>(?<Content>([\\s\\S]*?))<##ENDFOR##>";
                    if (Regex.IsMatch(strUI, pattern))
                    {
                        MatchCollection matches = Regex.Matches(strUI, pattern);
                        if ((matches.Count > 0))
                        {

                            string Template, url, ItemImagePath;
                            foreach (DataRow dr in dtItems.Rows)
                            {
                                Template = matches[0].Groups["Content"].Value;
                                url = GetProductURL(dr["vcCategoryName"].ToString(), dr["vcItemName"].ToString(), dr["numItemCode"].ToString());
                                ItemImagePath = GetImagePath(dr["vcPathForTImage"].ToString());

                                Template = Template.Replace("##ItemLink##", url);
                                Template = Template.Replace("##ItemName##", dr["vcItemName"].ToString());
                                Template = Template.Replace("##ItemShortName##", dr["vcItemName"].ToString().Length > 22 ? dr["vcItemName"].ToString().Substring(0, 22) + ".." : dr["vcItemName"].ToString());
                                Template = Template.Replace("##ItemImagePath##", ItemImagePath);
                                Template = Template.Replace("##RecordID##", Sites.ToString(dr["numSalesTemplateItemID"]) + ":" + Sites.ToString(dr["numSalesTemplateID"]));
                                sb.Append(Template);
                            }
                            strUI = Regex.Replace(strUI, pattern, sb.ToString());

                        }
                    }

                }
                pnlCutomizeHtml.Visible = false;
                litCutomizeHtml.Text = strUI;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}