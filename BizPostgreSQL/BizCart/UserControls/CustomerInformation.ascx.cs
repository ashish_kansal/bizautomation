﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BACRM.BusinessLogic.ShioppingCart;
using BACRM.BusinessLogic.Common;
using BACRM.BusinessLogic.Contacts;
using BACRM.BusinessLogic.Leads;
using BACRM.BusinessLogic.Admin;
using System.Data;

namespace BizCart.UserControls
{
    public partial class CustomerInformation : BizUserControl
    {
        #region Events
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (Sites.ToLong(Session["UserContactID"]) == 0 || Sites.ToLong(Session["DivId"]) == 0)
                {
                    Response.Redirect("/Login.aspx?ReturnURL=CustomerInformation.aspx", true);
                }
                if (!IsPostBack)
                {
                    int ErrorCode = 0;
                    if (Request.QueryString["errcode"] != null)
                    {
                        ErrorCode = CCommon.ToInteger(Request.QueryString["errcode"]);
                        if (ErrorCode > 0)
                        {
                            CustomErrorMessage(ErrorCode);
                        }
                    }
                    LoadContactDetails();
                }
                bindHtml();
            }
            catch (Exception ex)
            {
                ExceptionModule.ExceptionPublish(ex, Sites.ToLong(Session["DomainID"]), Sites.ToLong(Session["UserContactID"]), Sites.ToLong(Session["SiteId"]), Request);
                ShowMessage(ex.ToString(), 1);
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                CLeads objLead = new CLeads();
                objLead.DomainID = Sites.ToLong(Session["DomainID"]);
                objLead.ContactID = Sites.ToLong(Session["UserContactID"]);
                objLead.FirstName = txtCustomerFirstName.Text.Trim();
                objLead.LastName = txtCustomerLastName.Text.Trim();
                objLead.Email = txtCustomerEmail.Text.Trim();
                objLead.ContactPhone = txtCustomerPhone.Text.Trim();
                objLead.UpdateCustomerInformation();

                Session["ContactName"] = objLead.FirstName + " " + objLead.LastName;

                try
                {
                    if (txtOldPasword != null && txtNewPassword.Text != null && txtNewPasswordConfirm.Text != null)
                    {
                        if (txtOldPasword.Text != "" || txtNewPassword.Text != "")
                        {
                            if (Session["DomainID"] != null && Session["UserEmail"] != null && Session["UserContactID"] != null)
                            {
                                CCommon objCommon = new CCommon();

                                UserAccess objUserAccess = new UserAccess();
                                objUserAccess.Email = Session["UserEmail"].ToString();
                                objUserAccess.Password = objCommon.Encrypt(txtOldPasword.Text.Trim());
                                objUserAccess.NewPassword = objCommon.Encrypt(txtNewPassword.Text.Trim());
                                objUserAccess.ContactID = Sites.ToLong(Session["UserContactID"]);
                                objUserAccess.DivisionID = Sites.ToLong(Session["DivId"]);
                                objUserAccess.DomainID = Sites.ToLong(Session["DomainID"]);

                                if (objUserAccess.ChangePassword() == 1)
                                {
                                    ShowMessage(GetErrorMessage("ERR012"), 0); //Password has been successfully changed 
                                }
                                else
                                {
                                    ShowMessage(GetErrorMessage("ERR013"), 1);//You are not authorized to change the password !
                                }
                            }
                            else
                            {
                                Response.Redirect(Session["SitePath"].ToString() + "/Login.aspx");
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    ExceptionModule.ExceptionPublish(ex, Sites.ToInteger(Session["DomainID"]), Sites.ToInteger(Session["UserContactID"]), Sites.ToInteger(Session["SiteId"]), Request);
                    ShowMessage(ex.ToString(), 1);
                }

                if (Sites.ToString(Request["Return"]).Length > 0)
                {
                    Response.Redirect(Sites.ToString(Request["Return"]), true);
                }
                Response.Redirect("/Account.aspx", true);
            }
            catch (Exception ex)
            {
                ExceptionModule.ExceptionPublish(ex, Sites.ToLong(Session["DomainID"]), Sites.ToLong(Session["UserContactID"]), Sites.ToLong(Session["SiteId"]), Request);
                ShowMessage(ex.ToString(), 1);
            }
        }
        #endregion

        #region Methods

        private void CustomErrorMessage(int ErrorCode)
        {
            switch (ErrorCode)
            {
                case 1:
                    ShowMessage("First name cannot be empty!., Please update First Name.", 1);
                    break;

                case 2:
                    ShowMessage("Last name cannot be empty!., Please update Last Name.", 1);
                    break;

                case 3:
                    ShowMessage("First name cannot be just a single charatcer!.,., Please update First Name.", 1);
                    break;

                case 4:
                    ShowMessage("Last name cannot be just a single charatcer!., Please update Last Name.", 1);
                    break;

                default:
                    ShowMessage("Please update Customer Information", 1);
                    break;
            }
        }

        private void LoadContactDetails()
        {
            try
            {
                CContacts objContacts = new CContacts();
                DataTable dtTable1 = default(DataTable);
                objContacts.ContactID = Sites.ToLong(Session["UserContactID"]);
                objContacts.DomainID = Sites.ToLong(Session["DomainID"]);
                dtTable1 = objContacts.GetBillOrgorContAdd();
                txtCustomerFirstName.Text = Sites.ToString(dtTable1.Rows[0]["vcFirstname"]);
                txtCustomerLastName.Text = Sites.ToString(dtTable1.Rows[0]["vcLastname"]);
                txtCustomerEmail.Text = Sites.ToString(dtTable1.Rows[0]["vcEmail"]);
                txtCustomerPhone.Text = Sites.ToString(dtTable1.Rows[0]["vcPhone"]);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private void bindHtml()
        {
            try
            {
                string strUI = GetUserControlTemplate("CustomerInformation.htm");

                strUI = strUI.Replace("##CustomerFirstName##", CCommon.RenderControl(txtCustomerFirstName));
                strUI = strUI.Replace("##CustomerLastName##", CCommon.RenderControl(txtCustomerLastName));
                strUI = strUI.Replace("##CustomerPhone##", CCommon.RenderControl(txtCustomerPhone));
                strUI = strUI.Replace("##CustomerEmail##", CCommon.RenderControl(txtCustomerEmail));
                try
                {
                    if (txtOldPasword != null)
                    {
                        strUI = strUI.Replace("##OldPassword##", CCommon.RenderControl(txtOldPasword));
                    }
                    if (txtNewPassword != null)
                    {
                        strUI = strUI.Replace("##NewPassword##", CCommon.RenderControl(txtNewPassword));
                    }
                    if (txtNewPasswordConfirm != null)
                    {
                        strUI = strUI.Replace("##NewPasswordConfirm##", CCommon.RenderControl(txtNewPasswordConfirm));
                    }
                }
                catch
                {

                }

                strUI = strUI.Replace("##SaveButton##", CCommon.RenderControl(btnSubmit));

                pnlCutomizeHtml.Visible = false;
                litCutomizeHtml.Text = strUI;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Properties

        #endregion

    }
}