﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CustomerInformation.ascx.cs"
    Inherits="BizCart.UserControls.CustomerInformation" ClientIDMode="Static" %>
<asp:Panel ID="pnlCutomizeHtml" runat="server">
    <script>
        function Save() {
            var txtOldPassword = document.getElementById('<%= txtOldPasword.ClientID %>');
             var txtNewPassword = document.getElementById('<%= txtNewPassword.ClientID %>');
             var txtNewPasswordConfirm = document.getElementById('<%= txtNewPasswordConfirm.ClientID %>');
            if (txtOldPassword.value !== "" || txtNewPassword.value !== "") {
                if (txtOldPassword.value == "") {
                    alert("Please enter old password.");
                    txtOldPassword.focus();
                    return false;
                }
                else if (txtNewPassword.value == "") {
                    alert("Please enter new password.");
                    txtNewPassword.focus();
                    return false;
                }
                else if (txtNewPasswordConfirm.value == "") {
                    alert("Please confirm new password.");
                    txtNewPasswordConfirm.focus();
                    return false;
                }
                else if (txtNewPassword.value != txtNewPasswordConfirm.value) {
                    alert("New and Confirm passwords are not the same.");
                    return false;
                }
            }
             return true;
         }
    </script>
    <table border="0" width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td colspan="4">
                <div class="sectionheader">
                    Customer Information
                </div>
            </td>
        </tr>
        <tr>
            <td class="LabelColumn">
                First Name:
            </td>
            <td class="ControlCell">
                <asp:TextBox ID="txtCustomerFirstName" CssClass="textbox" runat="server" Width="182px"></asp:TextBox>
            </td>
            <td class="LabelColumn">
                Last Name:
            </td>
            <td class="ControlCell">
                <asp:TextBox ID="txtCustomerLastName" CssClass="textbox" runat="server" Width="182px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="LabelColumn">
                Phone:
            </td>
            <td class="ControlCell">
                <asp:TextBox ID="txtCustomerPhone" CssClass="textbox" runat="server" Width="182px"></asp:TextBox>
            </td>
            <td class="LabelColumn">
                Email:
            </td>
            <td class="ControlCell">
                <asp:TextBox ID="txtCustomerEmail" CssClass="textbox" runat="server" Width="182px"
                    Enabled="false"></asp:TextBox>
            </td>
        </tr>
         <tr>
            <td>
                <asp:Label ID="lblOldPassword" runat="server" Text="Old Password"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txtOldPasword" TextMode="Password" CssClass="textbox" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="lblNewPassword" runat="server" Text="New Password"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txtNewPassword" runat="server" TextMode="Password" CssClass="textbox"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="lblNewPasswordConfirm" runat="server" Text="Confirm Password"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txtNewPasswordConfirm" runat="server" TextMode="Password" CssClass="textbox"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td colspan="4" align="center">
                <asp:Button ID="btnSubmit" runat="server" Text="Confirm & Save" OnClientClick="javascript:return Save();" CssClass="button"
                    OnClick="btnSubmit_Click" />
            </td>
        </tr>
         
    </table>
</asp:Panel>
<asp:Literal ID="litCutomizeHtml" runat="server"></asp:Literal>