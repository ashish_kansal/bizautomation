﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Lists.ascx.cs" Inherits="BizCart.UserControls.Lists" ClientIDMode="Static" %>
<script>
    var Imageurl = '<%=Session["ItemImagePath"] %>';
    function DeleteRecord() {
        if (confirm('Are you sure, you want to delete the selected record?')) {
            return true;
        }
        else {
            return false;
        }
    }

    function DeleteWishList() {
        if (document.getElementById('ddlWishListName').value == "") {
            alert("Select wish list.");
            return false;
        } else {
            if (confirm('Are you sure, you want to delete the selected wish list?')) {
                return true;
            }
            else {
                return false;
            }
        }
    }

    function OpenCustomerStatement() {
        window.open('customerstatement.aspx', '', 'toolbar=no,titlebar=no,top=10,left=100,width=1000,height=300,scrollbars=yes,resizable=yes')
        return false;
    }
    function Save() {
        if (document.getElementById('txtListName').value == "") {
            alert("Enter name.");
            document.getElementById('txtListName').focus();
            return false;
        }
        return true;
    }
    $(document).click(function () {
        if (this != $(".autocompletediv")[0]) {
            $(".autocompletediv").hide();
        }
    });
    function ItemAutoSuggestDetails(i) {
        $('.autocompletediv').css("display", "block");
        if (i.length > 0) {
            var q = i;
            $(".ItemNamesList").html("<li><a attr='0' href='javascript:void(0)'>Searching...</a></li>");
            $.ajax({
                type: "POST",
                url: "/js/OrderServices.asmx/GetAllEcommerceItems",
                data: '{q: "' + i + '"}',
                //data: '{q: "' + q + '" }',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    console.log(response.d);
                    var jsonData = JSON.parse(response.d);

                    if (jsonData.length > 0) {
                        $(".ItemNamesList").html("");
                        $.each(jsonData, function (index, item) {
                            var dataAppend = "<table><tr><td><img class='imageThumb' src=" + Imageurl + '/' + item.vcPathForImage + " ></td><td><b>Item : </b>" + item.vcItemName + "</br><b>Model : </b>" + item.vcModelID + "</td><td><b>SKU : </b>" + item.vcSKU + "</br><b>Item Price : </b>" + item.monListPrice.toFixed(2) + "</td></table>"
                            //$(".LocationName").append("<li><a onclick=\"SetIndustry('" + item.IndustryName + "')\"  href='javascript:void(0)'>" + item.IndustryName + "</a></li>")
                            $(".ItemNamesList").append("<li><a onclick=\"loadItem('" + item.numItemCode + "','" + encodeURI(item.vcItemName) + "')\" attr='" + item.numItemCode + "' href='javascript:void(0)'>" + dataAppend + "</a></li>")
                        });
                    } else {
                        $(".ItemNamesList").html("");
                        $(".ItemNamesList").append("<li><a attr='0' href='javascript:void(0)'>No item found</a></li>")
                    }
                }
            });
        } else {
            $(".ItemNamesList").html("<li><a attr='0' href='javascript:void(0)'>Please enter one more character</a></li>");
        }
    }
    function loadItem(a, b) {
        $("#ItemAutoSuggest").val(decodeURI(b));
        $("#hdnAutoSuggestItemId").val(a);
    }
</script>
<asp:Panel ID="pnlCutomizeHtml" runat="server">
    <asp:Panel runat="server" ID="pnlNew">

        <table width="100%" align="center" border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td colspan="2">
                    <div class="sectionheader">
                        <asp:Label ID="lblTitle" runat="server" Text=""></asp:Label>
                    </div>
                </td>
            </tr>
            <tr>
                <td class="LabelColumn">Total Balance Due :
                </td>
                <td class="ControlCell">
                    <asp:Label ID="lblBalDue" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="LabelColumn">Total Remaining Credit :
                </td>
                <td class="ControlCell">
                    <asp:Label ID="lblRemCredit" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="LabelColumn">Total Amount Past Due :
                </td>
                <td class="ControlCell">
                    <asp:Label ID="lblAmtPastDue" runat="server"></asp:Label>
                </td>
            </tr>

            <tr>
                <td colspan="2">&nbsp;
                </td>
            </tr>
            <tr>
                <td align="right">
                    <asp:Label ID="lblListLabel" runat="server"></asp:Label>
                </td>
                <td align="left">
                    <asp:TextBox ID="txtListName" runat="server" CssClass="textbox form-control"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td></td>
                <td align="left">
                    <asp:Button runat="server" ID="btnSave" CssClass="button btn btn-primary" Text="Save" OnClick="btnSave_Click" OnClientClick="return Save();" />
                    <a href="/lists.aspx">Cancel</a>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlView">
        <table width="100%">
            <tr>
                <td>
                    <div class="sectionheader">
                        <asp:Label ID="lblLists" runat="server" Text=""></asp:Label>
                    </div>
                </td>
            </tr>
            <tr>
                <td align="right">
                    <asp:LinkButton runat="server" ID="lnkAddNew" CssClass="btn btn-primary" Text="Add New" OnClick="lnkAddNew_Click" />
                    <asp:LinkButton runat="server" ID="lnkDelete" CssClass="btn btn-danger" Text="Delete" OnClientClick="return DeleteWishList();" OnClick="lnkDelete_Click" />
                    <asp:DropDownList ID="ddlWishListName" CssClass="form-control" OnSelectedIndexChanged="ddlWishListName_SelectedIndexChanged" AutoPostBack="true" runat="server"></asp:DropDownList>
                    <asp:Button ID="btnAddItemtoList" OnClick="btnAddItemtoList_Click" CssClass="btn btn-primary btnAddItemtoList" runat="server" Text="Add" />
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Panel>
<asp:Literal ID="litCutomizeHtml" runat="server"></asp:Literal>
<asp:HiddenField ID="hdnSalesTemplateID" runat="server" />
<asp:HiddenField ID="hdnMode" runat="server" />
<asp:HiddenField ID="hdnAutoSuggestItemId" runat="server" />
