﻿using BACRM.BusinessLogic.Admin;
using BACRM.BusinessLogic.Common;
using BACRM.BusinessLogic.Contacts;
using BACRM.BusinessLogic.Item;
using BACRM.BusinessLogic.Promotion;
using BACRM.BusinessLogic.ShioppingCart;
using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI.WebControls;
using Nest;
using Newtonsoft.Json;
using Elasticsearch.Net;
using Newtonsoft.Json.Linq;
using System.Reflection;
using System.Web.UI;

namespace BizCart.UserControls
{
    public partial class ItemList : BizUserControl
    {
        #region Global Declaration

        int PageIndex = 0;
        string SearchText = string.Empty;
        string AdvancedSearchText = string.Empty;
        long CategoryID = 0;
        long ManufacturerID = 0;
        public int TrimNameLength = 0;

        #endregion

        #region Events

        #region Custom Properties

        public string TrimDescriptionbyfollowingnoofcharacters { get; set; }
        public string AfterAddingItemShowCartPage { get; set; }
        public string TrimItemNamebyfollowingnoofcharacters { get; set; }

        public string filter
        {
            get
            {
                object o = ViewState["filter"];
                if (o != null)
                {
                    return (string)o;
                }
                else
                {
                    ViewState["filter"] = CCommon.ToString(Request["filter"]);
                    object obj = ViewState["filter"];
                    if (obj != null)
                    {
                        return CCommon.ToString(ViewState["filter"]);
                    }
                    else
                    {
                        return "";
                    }
                }
            }
        }

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            DateTime startbindHtml = DateTime.Now;
            try
            {
                Sites.InitialiseSession();
                hplCheckout.Attributes.Add("OnClick", "javascript:return false;");

                if (!IsPostBack)
                {
                    Session["frc"] = null;
                    Session["fcfields"] = null;
                    Session["fcwhere"] = null;
    
                    if (Session["OverrideElasticSearch"] == null && Request.RawUrl.Contains("OverrideElasticSearch"))
                    {
                        Session["OverrideElasticSearch"] = true;
                    }

                    /// IsDDTimeDiffrent : for Time Diffrent
                    /// IsDDConsoleESValues : Console for Elastic Item and condition
                    /// IsDDCacheCategory : Cache Category
                    /// IsDDCacheECommerceSettings : Cache ECommerceSettings
                    /// IsDDEnableESItemDynamicData : Enable ES Item Dynamic Data

                    if (Request.RawUrl.Contains("IsDDTimeDiffrent"))
                    {
                        Session["IsDDTimeDiffrent"] = Sites.ToBool(GetParamValueFromQueryString("IsDDTimeDiffrent"));
                    }
                    if (Request.RawUrl.Contains("IsDDConsoleESValues"))
                    {
                        Session["IsDDConsoleESValues"] = Sites.ToBool(GetParamValueFromQueryString("IsDDConsoleESValues"));
                    }
                    if (Request.RawUrl.Contains("IsDDCacheCategory"))
                    {
                        Session["IsDDCacheCategory"] = Sites.ToBool(GetParamValueFromQueryString("IsDDCacheCategory"));
                    }
                    if (Request.RawUrl.Contains("IsDDCacheECommerceSettings"))
                    {
                        Session["IsDDCacheECommerceSettings"] = Sites.ToBool(GetParamValueFromQueryString("IsDDCacheECommerceSettings"));
                    }
                    if (Request.RawUrl.Contains("IsDDDisableESItemDynamicData"))
                    {
                        Session["IsDDDisableESItemDynamicData"] = Sites.ToBool(GetParamValueFromQueryString("IsDDDisableESItemDynamicData"));
                    }

                    AddSortItems();

                    /*if (Request["Cat"] != null)
                    {
                        string redirect = "/Category/" + Sites.GenerateSEOFriendlyURL(Request["CatName"]) + "/1/" + Request["Cat"];
                        Response.Redirect(redirect, false);
                    }*/

                    BindPagingDDL();
                }

                PageIndex = AspNetPager1.CurrentPageIndex == 0 ? 1 : AspNetPager1.CurrentPageIndex;//set default to 1
                SearchText = HttpUtility.UrlDecode(Sites.ToString(Request["ST"]));

                AdvancedSearchText = Sites.ToString(Session["AdvancedSearch"]);
                ManufacturerID = Sites.ToInteger(Sites.ToString(Request.QueryString["Manufacturer"]).Trim('/'));
                if (lblManufacturerName != null)
                {
                    lblManufacturerName.Text = Sites.ToString(Request.QueryString["ManufacturerName"]).Trim('/');
                }
                CategoryID = Sites.ToInteger(Sites.ToString(Request["Cat"]).Trim('/'));
                lblCategoryName.Text = Request["CatName"] != null ? CCommon.ToString(Request["CatName"]) : "";

                if (CategoryID == 0 && Request["Category"] != null)
                {
                    Sites objSites = new Sites();
                    objSites.DomainID = Sites.ToLong(Session["DomainID"]);
                    DataTable dt = null;

                    if (Session["IsDDCacheCategory"] != null && CCommon.ToBool(Session["IsDDCacheCategory"]) == true)
                    {
                        string CacheNameForCategory = "CacheCategoryFromName_" + CCommon.ToString(Request["Category"]);
                        if (Cache[CacheNameForCategory] == null)
                        {
                            Cache.Insert(CacheNameForCategory, objSites.GetCategoryFromName(CCommon.ToString(Request["Category"])));
                        }
                        dt = (DataTable)Cache[CacheNameForCategory];
                    }
                    else
                    {
                        dt = objSites.GetCategoryFromName(CCommon.ToString(Request["Category"]));
                    }


                    if (dt != null && dt.Rows.Count > 0)
                    {
                        CategoryID = Sites.ToLong(dt.Rows[0]["numCategoryID"]);
                        lblCategoryName.Text = dt.Rows[0]["vcCategoryName"] != DBNull.Value ? CCommon.ToString(dt.Rows[0]["vcCategoryName"]) : "";
                    }
                    else
                    {
                        Response.Redirect("Home.aspx", false);
                    }
                }
                if (!IsPostBack && CategoryID > 0)
                {
                    CItems objItems = default(CItems);
                    objItems = new CItems();
                    objItems.byteMode = 17;
                    objItems.DomainID = Sites.ToLong(Session["DomainID"]);
                    objItems.CategoryID = this.CategoryID;
                    //DataTable dtCategory = objItems.SeleDelCategory();

                    DataTable dtCategory = null;

                    if (Session["IsDDCacheCategory"] != null && CCommon.ToBool(Session["IsDDCacheCategory"]) == true)
                    {
                        string CacheNameForCategory = string.Format("CacheSeleDelCategory_{0}_{1}_{2}", objItems.byteMode, objItems.SiteID, objItems.CategoryID);
                        if (Cache[CacheNameForCategory] == null)
                        {
                            Cache.Insert(CacheNameForCategory, objItems.SeleDelCategory());
                        }
                        dtCategory = (DataTable)Cache[CacheNameForCategory];
                    }
                    else
                    {
                        dtCategory = objItems.SeleDelCategory();
                    }

                    if (dtCategory != null && dtCategory.Rows.Count > 0)
                    {
                        if (CCommon.ToString(dtCategory.Rows[0]["vcMetaTitle"]) != "")
                        {
                            this.Page.Title = CCommon.ToString(dtCategory.Rows[0]["vcMetaTitle"]);
                        }
                        if (CCommon.ToString(dtCategory.Rows[0]["vcMetaDescription"]) != "")
                        {
                            this.Page.MetaDescription = CCommon.ToString(dtCategory.Rows[0]["vcMetaDescription"]);
                        }
                        if (CCommon.ToString(dtCategory.Rows[0]["vcMetaKeywords"]) != "")
                        {
                            this.Page.MetaKeywords = CCommon.ToString(dtCategory.Rows[0]["vcMetaKeywords"]);
                        }
                    }
                    else
                    {
                        throw new HttpException(404, "Category not found");
                    }
                }

                AspNetPager1.PageSize = Sites.ToInteger(ddlPageSize.SelectedValue);
                AspNetPager1.CurrentPageIndex = PageIndex;
                TrimNameLength = Sites.ToInteger(TrimItemNamebyfollowingnoofcharacters) > 1 ? Sites.ToInteger(TrimItemNamebyfollowingnoofcharacters) : 22;
                if (IsPostBack)
                {
                    if (Sites.ToString(Request["__EVENTTARGET"]) == "AddToCart")
                    {
                        string itemid = Convert.ToString(Request["__EVENTARGUMENT"]);
                        string prodid = Request.Form["ddlProfileItem" + itemid.ToString()];
                        long productid = 0;
                        if (Sites.ToString(Request.Form["ddlProfileItem" + itemid.ToString()]) != "")
                        {
                            string[] strContainerItemCode = Convert.ToString(prodid).Split('~');
                            if (strContainerItemCode != null && strContainerItemCode.Length == 2)
                            {
                                productid = Sites.ToInteger(strContainerItemCode[1]);
                            }
                            else
                            {
                                productid = Sites.ToLong(Request["__EVENTARGUMENT"]);
                            }
                        }
                        else
                        {
                            productid = Sites.ToLong(Request["__EVENTARGUMENT"]);
                        }
                         
                        Tuple<bool,string> objResult = AddToCartFromEcomm(lblCategoryName.Text, productid, 0, "", false, Sites.ToInteger(hdnSQty.Text));
                        bool isItemAdded = objResult.Item1;

                        if (isItemAdded)
                        {
                            if (Convert.ToString(HttpContext.Current.Session["PreUpSell"]) == "True")
                            {
                                /*Modified by Neelam on 10/11/2017 - Added functionality to open Child PreUpSell window in modal pop up*/
                                CItems objItem = new CItems();
                                objItem.DomainID = Sites.ToLong(Session["DomainID"]);
                                objItem.ParentItemCode = Sites.ToLong(Request["__EVENTARGUMENT"]); //9;
                                //objItem.byteMode = 4;
                                objItem.byteMode = 8;
                                HttpCookie cookie = Request.Cookies["Cart"];
                                if (Request.Cookies["Cart"] != null)
                                {
                                    objItem.vcCookieId = (cookie["KeyId"] != null) ? cookie["KeyId"] : "";
                                }
                                else
                                {
                                    objItem.vcCookieId = "";
                                }
                                objItem.UserCntID = Sites.ToLong(Session["UserContactID"]);
                                DataTable dtItemPrepopup = new DataTable();
                                objItem.SiteID = Sites.ToLong(Session["SiteId"]);
                                dtItemPrepopup = objItem.GetSimilarItem();

                                DataSet ds = GetCartItem();
                                DataTable dtCart = ds.Tables[0];

                                DataTable duplicateRows = new DataTable();
                                if (dtCart.Rows.Count > 0 && dtItemPrepopup.Rows.Count > 0)
                                {
                                    bindMiniCart();
                                    /*duplicateRows = (from a in dtCart.AsEnumerable()
                                                               join b in dtItemPrepopup.AsEnumerable()
                                                               on a["numItemCode"].ToString()
                                                               equals b["numItemCode"].ToString() into g
                                                               where g.Count() > 0
                                                               select a).CopyToDataTable();*/

                                    var rows = from a in dtCart.AsEnumerable()
                                               join b in dtItemPrepopup.AsEnumerable()
                                               on a["numItemCode"].ToString() equals b["numItemCode"].ToString()
                                               into g
                                               where g.Count() > 0
                                               select a;

                                    if (rows != null && rows.Count() > 0)
                                        duplicateRows = rows.CopyToDataTable();
                                }
                                if (duplicateRows != null && duplicateRows.Rows.Count <= 0)
                                {
                                    if (dtItemPrepopup.Rows.Count > 0)
                                    {
                                        gvRelatedItems.DataSource = dtItemPrepopup;
                                        gvRelatedItems.DataBind();

                                        dialogSellPopup.Visible = true;
                                        this.Page.ClientScript.RegisterStartupScript(this.GetType(), "ShowMiniPreSellCart", "ShowMiniPreSellCart();", true);
                                        /*foreach (DataRow dr in dtItemPrepopup.Rows)
                                        {
                                            RelatedItemsPromotion(CCommon.ToInteger(dr["numItemCode"]), CCommon.ToLong(dr["numWareHouseItemID"]));
                                        }
                                        RelatedItemsPromotion(CCommon.ToInteger(Sites.ToLong(Request["__EVENTARGUMENT"])), CCommon.ToLong(Sites.ToLong(Session["WareHouseID"])));*/
                                    }
                                    else
                                    {
                                        this.Page.ClientScript.RegisterStartupScript(this.GetType(), "UpdateMiniCart", "if(document.getElementById('btnRefresh')!=null){ __doPostBack('btnRefresh','OnClick');}", true);

                                        dialogSellPopup.Visible = false;
                                        if (CCommon.ToBool(AfterAddingItemShowCartPage))
                                        {
                                            Response.Redirect("/Cart.aspx", false);
                                        }
                                        else
                                        {
                                            this.Page.ClientScript.RegisterStartupScript(this.GetType(), "ShowMiniCart", "ShowMiniCart();", true);
                                        }
                                    }
                                }
                                else
                                {
                                    gvRelatedItems.Columns[8].Visible = true;

                                    foreach (DataRow dr in duplicateRows.Rows)
                                    {
                                        long _numitemCode = CCommon.ToLong(dr["numItemCode"]);

                                        if (dtItemPrepopup.Rows.Count > 0)
                                        {
                                            gvRelatedItems.DataSource = dtItemPrepopup;
                                            gvRelatedItems.DataBind();
                                            long _gvnumitemCode = 0;
                                            dialogSellPopup.Visible = true;
                                            this.Page.ClientScript.RegisterStartupScript(this.GetType(), "ShowMiniPreSellCart", "ShowMiniPreSellCart();", true);
                                            /*foreach (DataRow drPre in dtItemPrepopup.Rows)
                                            {
                                                RelatedItemsPromotion(CCommon.ToInteger(drPre["numItemCode"]), CCommon.ToLong(drPre["numWareHouseItemID"]));
                                            }
                                            RelatedItemsPromotion(CCommon.ToInteger(Sites.ToLong(Request["__EVENTARGUMENT"])), CCommon.ToLong(Sites.ToLong(Session["WareHouseID"])));*/

                                            for (int i = 0; i < gvRelatedItems.Rows.Count; i++)
                                            {
                                                _gvnumitemCode = CCommon.ToLong(gvRelatedItems.Rows[i].Cells[8].Text);
                                                Button btnAdd = (Button)(gvRelatedItems.Rows[i].Cells[6].FindControl("btnAdd"));
                                                if (_gvnumitemCode == _numitemCode)
                                                {
                                                    btnAdd.Enabled = false;
                                                    btnAdd.Text = "Added";
                                                }
                                            }
                                        }
                                    }
                                    gvRelatedItems.Columns[8].Visible = false;
                                }
                            }
                            else
                            {
                                this.Page.ClientScript.RegisterStartupScript(this.GetType(), "UpdateMiniCart", "if(document.getElementById('btnRefresh')!=null){ __doPostBack('btnRefresh','OnClick');}", true);
                                //Update Minicart

                                dialogSellPopup.Visible = false;
                                if (CCommon.ToBool(AfterAddingItemShowCartPage))
                                {
                                    Response.Redirect("/Cart.aspx", false);
                                }
                                else
                                {
                                    this.Page.ClientScript.RegisterStartupScript(this.GetType(), "ShowMiniCart", "ShowMiniCart();", true);
                                }
                            }
                        }
                    }
                }

                BreadCrumb.Clear();
                BreadCrumb.OverWriteBreadCrumb(1, "Home.aspx", "Home", "Home.aspx");

                if (ManufacturerID > 0)
                {
                    BreadCrumb.OverWriteBreadCrumb(2, "Manufacturer.aspx", "Manufacturer", "Manufacturer.aspx");
                    BreadCrumb.OverWriteBreadCrumb(3, "ProductList.aspx?ManufacturerID=" + ManufacturerID.ToString(), lblManufacturerName.Text, "Manufacturer");
                }
                else
                {
                    BreadCrumb.OverWriteBreadCrumb(2, "Categories.aspx", "Category", "Categories.aspx");

                    Sites objSites = new Sites();
                    objSites.DomainID = Sites.ToLong(Session["DomainID"]);
                    objSites.CategoryID = Sites.ToLong(CategoryID);
                    DataTable dtCategories = objSites.GetParentCategories();

                    short index = 2;

                    if (dtCategories != null && dtCategories.Rows.Count > 0)
                    {
                        foreach (DataRow dr in dtCategories.Rows)
                        {
                            index += 1;
                            BreadCrumb.OverWriteBreadCrumb(index, "SubCategory/" + Sites.ToString(dr["vcCategoryName"]) + "/1/" + Sites.ToString(dr["numCategoryID"]), Sites.ToString(dr["vcCategoryName"]), "Categories");
                        }
                    }
                }
            }
            catch (HttpException ex)
            {
                if (ex.GetHttpCode() == 404)
                {
                    throw;
                }
                else
                {
                    ExceptionModule.ExceptionPublish(ex, Sites.ToLong(Session["DomainID"]), Sites.ToLong(Session["UserContactID"]), Sites.ToLong(Session["SiteId"]), Request);
                    ShowMessage(ex.ToString(), 1);
                }
            }
            catch (Exception ex)
            {
                ExceptionModule.ExceptionPublish(ex, Sites.ToLong(Session["DomainID"]), Sites.ToLong(Session["UserContactID"]), Sites.ToLong(Session["SiteId"]), Request);
                ShowMessage(ex.ToString(), 1);
            }
            TimeSpan BindtimeDiff = DateTime.Now - startbindHtml;

            if (Session["IsDDTimeDiffrent"] != null && CCommon.ToBool(Session["IsDDTimeDiffrent"]) == true)
            {
                ScriptManager.RegisterClientScriptBlock(this.Page, Page.GetType(), "DDTimeDiffrent_Page_Load", "console.log('DDTimeDiffrent_Page_Load','" + BindtimeDiff.TotalMilliseconds + "');", true);
            }
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack || Sites.ToString(Request["__EVENTTARGET"]) == "AddToCart" || Sites.ToString(Request["__EVENTTARGET"]) == "btnFilter" || Sites.ToString(Request["__EVENTTARGET"]) == "btnSearch")
                    bindHtml();
            }
            catch (Exception ex)
            {
                ExceptionModule.ExceptionPublish(ex, Sites.ToLong(Session["DomainID"]), Sites.ToLong(Session["UserContactID"]), Sites.ToLong(Session["SiteId"]), Request);
                ShowMessage(ex.ToString(), 1);
            }
        }

        protected string GetParamValueFromQueryString(string Key)
        {
            int StartInd = Request.RawUrl.LastIndexOf(Key) + Key.Length + 1;
            int length = (Request.RawUrl.IndexOf("&", StartInd) != -1 ? Request.RawUrl.IndexOf("&", StartInd) : Request.RawUrl.Length) - StartInd;
            return Request.RawUrl.Substring(StartInd, length);
        }

        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                HttpContext.Current.Session["PageSize"] = ddlPageSize.SelectedValue;
                AspNetPager1.PageSize = Sites.ToInteger(ddlPageSize.SelectedValue);
                bindHtml();
            }
            catch (Exception ex)
            {
                ExceptionModule.ExceptionPublish(ex, Sites.ToLong(Session["DomainID"]), Sites.ToLong(Session["UserContactID"]), Sites.ToLong(Session["SiteId"]), Request);
                ShowMessage(ex.ToString(), 1);
            }
        }

        protected void ddlSortBy_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                HttpContext.Current.Session["SortBy"] = ddlSortBy.SelectedValue;
                bindHtml();
            }
            catch (Exception ex)
            {
                ExceptionModule.ExceptionPublish(ex, Sites.ToLong(Session["DomainID"]), Sites.ToLong(Session["UserContactID"]), Sites.ToLong(Session["SiteId"]), Request);
                ShowMessage(ex.ToString(), 1);
            }
        }

        protected void AspNetPager1_PageChanged(object sender, EventArgs e)
        {
            try
            {
                bindHtml();
            }
            catch (Exception ex)
            {
                ExceptionModule.ExceptionPublish(ex, Sites.ToLong(Session["DomainID"]), Sites.ToLong(Session["UserContactID"]), Sites.ToLong(Session["SiteId"]), Request);
                ShowMessage(ex.ToString(), 1);
            }
        }

        /*Added by Neelam on 10/11/2017 - Added event to navigate on checkout*/
        /*protected void hplCheckout_Click(object sender, EventArgs e)
        {
            hplCheckout.PostBackUrl = "/" + CheckoutPageName();
        }*/

        protected void gvRelatedItems_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "Add")
                {
                    int index = Convert.ToInt32(e.CommandArgument);
                    GridViewRow gvr = (GridViewRow)(((Button)e.CommandSource).NamingContainer);
                    int RowIndex = gvr.RowIndex;
                    int parentItemCode = Convert.ToInt32(gvRelatedItems.DataKeys[RowIndex].Values["numParentItemCode"]);
                    Button btnAdd = (Button)gvRelatedItems.Rows[RowIndex].FindControl("btnAdd");
                    TextBox txtQty = (TextBox)gvRelatedItems.Rows[RowIndex].FindControl("txtQty");
                    if (btnAdd.Text != "Added")
                    {
                        AddToCartFromEcomm(hdnCategory.Value, index, 0, "", false, Sites.ToInteger(txtQty.Text), parentItemCode: parentItemCode);

                        btnAdd.Text = "Added";
                        btnAdd.Enabled = false;

                        bindMiniCart();
                        this.Page.ClientScript.RegisterStartupScript(this.GetType(), "ShowMiniPreSellCart", "ShowMiniPreSellCart();", true);
                    }

                    int numVisible = 0;
                    foreach (GridViewRow row in gvRelatedItems.Rows)
                    {
                        if (row.Visible == true)
                        {
                            numVisible += 1;
                        }
                    }

                    if (numVisible == 0)
                    {
                        dialogSellPopup.Visible = false;
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionModule.ExceptionPublish(ex, Sites.ToLong(Session["DomainID"]), Sites.ToLong(Session["UserContactID"]), Sites.ToLong(Session["SiteId"]), Request);
                ShowMessage(ex.ToString(), 1);
            }
        }

        protected void gvRelatedItems_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Button btnAdd = e.Row.FindControl("btnAdd") as Button;
                    Label lblOutOfStock = e.Row.FindControl("lblOutOfStock") as Label;
                    //ScriptManager.GetCurrent(this).RegisterAsyncPostBackControl(btnAdd);
                    //ScriptManager.RegisterAsyncPostBackControl(btnAdd);

                    DataRowView drv = e.Row.DataItem as DataRowView;
                    if (drv != null)
                    {
                        TextBox txtQty = (TextBox)e.Row.FindControl("txtQty");
                        int qty = CCommon.ToInteger(txtQty.Text);
                        decimal price = CCommon.ToDecimal(drv.Row["monListPrice"]);
                        decimal total = CCommon.ToDecimal(qty * price);
                        Label lblTotal = (Label)e.Row.FindControl("lblTotal");
                        lblTotal.Text = CCommon.ToString(Session["CurrSymbol"]) + " " + CCommon.ToString(Math.Round(total, 2));

                        TableCell monListPriceCell = e.Row.Cells[5];
                        monListPriceCell.Text = CCommon.ToString(Session["CurrSymbol"]) + " " + CCommon.ToString(Math.Round(price, 2));

                        //hdnProductId.Value = CCommon.ToString(drv.Row["numItemCode"]);
                        //hdnCategory.Value = CCommon.ToString(drv.Row["vcCategoryName"]);

                        int _numOnHand = CCommon.ToInteger(drv.Row["numOnHand"]);
                        if (_numOnHand == 0)
                        {
                            btnAdd.Visible = false;
                            lblOutOfStock.Visible = true;
                        }

                        Image img = (Image)e.Row.FindControl("imgPath");
                        string imgPath = img.ImageUrl;
                        if (!string.IsNullOrEmpty(imgPath))
                        {
                            imgPath = GetImagePath(CCommon.ToString(imgPath));
                            img.ImageUrl = imgPath;
                        }
                        else
                        {
                            img.ImageUrl = "/Images/DefaultProduct.png";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionModule.ExceptionPublish(ex, Sites.ToLong(Session["DomainID"]), Sites.ToLong(Session["UserContactID"]), Sites.ToLong(Session["SiteId"]), Request);
                ShowMessage(ex.ToString(), 1);
            }
        }

        protected void gvMiniCart_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    DataRowView drv = e.Row.DataItem as DataRowView;
                    if (drv != null)
                    {
                        Image img = (Image)e.Row.FindControl("imgPath");
                        string imgPath = img.ImageUrl;
                        if (!string.IsNullOrEmpty(imgPath))
                        {
                            imgPath = GetImagePath(CCommon.ToString(imgPath));
                            img.ImageUrl = imgPath;
                        }
                        else
                        {
                            img.ImageUrl = "/Images/DefaultProduct.png";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionModule.ExceptionPublish(ex, Sites.ToLong(Session["DomainID"]), Sites.ToLong(Session["UserContactID"]), Sites.ToLong(Session["SiteId"]), Request);
                ShowMessage(ex.ToString(), 1);
            }
        }

        #endregion

        #region Methods

        #region HTML

        private void bindHtml()
        {
            DateTime startbindHtml = DateTime.Now;
            try
            {
                bool IsElasticSearchEnable = false;
                bool bitAutoSelectWarehouse = false;
                bool tintDisplayCategory = false;
                short warehouseAvailability = 0;
                bool isDisplayQtyAvailable = false;
                DataTable dtTable;
                UserAccess objUserAccess = new UserAccess();
                objUserAccess.DomainID = Sites.ToLong(Session["DomainID"]);
                objUserAccess.SiteID = Sites.ToLong(Session["SiteId"]);
                //dtTable = objUserAccess.GetECommerceDetails();

                if (Session["IsDDCacheECommerceSettings"] != null && CCommon.ToBool(Session["IsDDCacheECommerceSettings"]) == true)
                {
                    string CacheName = string.Format("CacheGetECommerceDetails_{0}_{1}", objUserAccess.DomainID, objUserAccess.SiteID);
                    if (Cache[CacheName] == null)
                    {
                        Cache.Insert(CacheName, objUserAccess.GetECommerceDetails());
                    }
                    dtTable = (DataTable)Cache[CacheName];
                }
                else
                {
                    dtTable = objUserAccess.GetECommerceDetails();
                }

                if (dtTable.Rows.Count > 0)
                {
                    IsElasticSearchEnable = CCommon.ToBool(dtTable.Rows[0]["bitElasticSearch"]);
                    bitAutoSelectWarehouse = CCommon.ToBool(dtTable.Rows[0]["bitAutoSelectWarehouse"]);
                    tintDisplayCategory = CCommon.ToBool(dtTable.Rows[0]["bitDisplayCategory"]);
                    warehouseAvailability = CCommon.ToShort(dtTable.Rows[0]["tintWarehouseAvailability"]);
                    isDisplayQtyAvailable = CCommon.ToBool(dtTable.Rows[0]["bitDisplayQtyAvailable"]);

                }
                if (IsElasticSearchEnable == false && Session["OverrideElasticSearch"] != null && CCommon.ToBool(Session["OverrideElasticSearch"]) == true)
                {
                    IsElasticSearchEnable = true;
                }

                string strUI = GetUserControlTemplate("ItemList.htm");
                string strPatternCategoryImageSaction = "{##CategoryImageSection##}(?<Content>([\\s\\S]*?)){/##CategoryImageSection##}";

                string patternAttributeHeader = "<##FOR_ATTRIBUTE_HEADER##>(?<Content>([\\s\\S]*?))<##ENDFOR_ATTRIBUTE_HEADR##>";
                string patternAttributeData = "<##FOR_ATTRIBUTE_DATA##>(?<Content>([\\s\\S]*?))<##ENDFOR_ATTRIBUTE_DATA##>";

                Sites objSites = new Sites();
                objSites.DomainID = Sites.ToLong(Session["DomainID"]);
                objSites.CategoryID = CategoryID;
                DataTable dtAttributes = objSites.GetCategoryFilters();

                if (Regex.IsMatch(strUI, patternAttributeHeader) && dtAttributes != null && dtAttributes.Rows.Count > 0)
                {
                    MatchCollection mFilter = Regex.Matches(strUI, patternAttributeHeader);

                    if (mFilter.Count > 0)
                    {
                        foreach (Match objMatch in mFilter)
                        {
                            string filterHeaderHtml = "";
                            string tempTemplate = "";

                            foreach (DataRow dr in dtAttributes.Rows)
                            {
                                tempTemplate = objMatch.Groups["Content"].Value;
                                tempTemplate = tempTemplate.Replace("##ATTRIBUTE_NAME##", CCommon.ToString(dr["Fld_label"]));
                                filterHeaderHtml += tempTemplate;
                            }

                            strUI = Regex.Replace(strUI, "<##FOR_ATTRIBUTE_HEADER##>" + objMatch.Groups["Content"].Value + "<##ENDFOR_ATTRIBUTE_HEADR##>", filterHeaderHtml);
                        }
                    }
                }
                else
                {
                    strUI = Regex.Replace(strUI, patternAttributeHeader, "");
                }

                if (Regex.IsMatch(strUI, patternAttributeData) && dtAttributes != null && dtAttributes.Rows.Count > 0)
                {
                    MatchCollection mFilter = Regex.Matches(strUI, patternAttributeData);

                    if (mFilter.Count > 0)
                    {
                        foreach (Match objMatch in mFilter)
                        {
                            string filterDataHtml = "";
                            string tempTemplate = "";

                            foreach (DataRow dr in dtAttributes.Rows)
                            {
                                tempTemplate = objMatch.Groups["Content"].Value;
                                tempTemplate = tempTemplate.Replace("##ATTRIBUTE_NAME##", CCommon.ToString(dr["Fld_label"]));
                                tempTemplate = tempTemplate.Replace("##ATTRIBUTE_VALUE_PLACEHOLDER##", "##" + CCommon.ToString(dr["Fld_label"]).Replace(" ", "") + "_" + CCommon.ToString(dr["Fld_id"]) + "##");

                                filterDataHtml += tempTemplate;
                            }

                            strUI = Regex.Replace(strUI, "<##FOR_ATTRIBUTE_DATA##>" + objMatch.Groups["Content"].Value + "<##ENDFOR_ATTRIBUTE_DATA##>", filterDataHtml);
                        }
                    }
                }
                else
                {
                    strUI = Regex.Replace(strUI, patternAttributeData, "");
                }

                string pattern = "<##FOR(?<Property>(\\b[^>]*?))##>(?<Content>([\\s\\S]*?))<##ENDFOR##>";

                string strFilter = CCommon.ToString(Request["filter"]);
                string patternFilter = "<##FOR_FILTER##>(?<Content>([\\s\\S]*?))<##ENDFOR_FILTER##>";

                if (strFilter != "")
                {
                    string[] Filters = strFilter.Split(';');


                    string allFilter = "";

                    for (int i = 0; i < Filters.Length - 1; i++)
                    {
                        if (Regex.IsMatch(strUI, patternFilter))
                        {
                            MatchCollection mFilter = Regex.Matches(strUI, patternFilter);

                            if (mFilter.Count > 0)
                            {
                                string Template = "";

                                Template = mFilter[0].Groups["Content"].Value;
                                Template = Template.Replace("##FilterValue##", Filters[i]);

                                if (Filters[i].ToLower().Contains("min") && Filters[i].ToLower().Contains("max"))
                                {
                                    Template = Template.Replace("##FilterAttribute##", "Rs. " + Filters[i].Split('~', '^')[1] + "-" + "Rs. " + Filters[i].Split('~', '^')[3]);
                                }
                                else
                                {
                                    Template = Template.Replace("##FilterAttribute##", Filters[i].Split('~', '^')[1] + "-" + Filters[i].Split('~', '^')[3]);
                                }
                                allFilter = allFilter + Template;
                            }
                        }
                    }
                    strUI = Regex.Replace(strUI, patternFilter, allFilter);
                    strUI = strUI.Replace("##FILTERBY##", "Filter By : ");
                }
                else
                {
                    strUI = Regex.Replace(strUI, patternFilter, "");
                    strUI = strUI.Replace("##FILTERBY##", "");
                }

                if (Regex.IsMatch(strUI, pattern))
                {
                    MatchCollection matches = Regex.Matches(strUI, pattern);
                    if ((matches.Count > 0))
                    {
                        Hashtable htProperties = GetPropertiesAsHastable(matches[0].Groups["Property"].Value.Replace("(", "").Replace(")", ""));
                        int RowsCount = 0;
                        string Content = matches[0].Groups["Content"].Value;

                        Sites objSite = new Sites();
                        CItems objItems = new CItems();
                        DataTable dtTableCategory = new DataTable();

                        objItems.CategoryID = CategoryID;
                        objItems.DomainID = Sites.ToLong(Session["DomainID"]);


                        if (SearchText.Length > 0)
                        {
                            this.Page.Title = "Search results for \"" + SearchText + "\"";
                            lblCategoryName.Text = "Search results for \"" + SearchText + "\"";
                            //strUI = strUI.Replace("##CategoryName##", lblCategoryName.Text);
                            strUI = Sites.RemoveMatchedPattern(strUI, strPatternCategoryImageSaction);
                            strUI = strUI.Replace("##CategoryDesc##", "");
                            strUI = strUI.Replace("##ManufacturerLogo##", "");
                            strUI = strUI.Replace("##ManufacturerName##", "");
                            strUI = strUI.Replace("##ManufacturerDescription##", "");
                        }
                        else
                        {
                            if (ManufacturerID > 0)
                            {
                                CItems objManufacturer = new CItems();
                                objManufacturer.DomainID = Sites.ToLong(Session["DomainID"]);
                                objManufacturer.DivisionID = 0;
                                objItems.numShowOnWebFieldId = 0;
                                string strTemp = string.Empty;
                                DataSet dsManufacturer = new DataSet();
                                objItems.ManufacturerID = ManufacturerID;
                                dsManufacturer = objItems.GetManufactuerersForECommerce();

                                if (dsManufacturer.Tables.Count > 0)
                                {
                                    if (dsManufacturer.Tables[0].Rows.Count > 0)
                                    {
                                        string strFileName = "/PortalDocs/" + Session["DomainID"] + "/" + CCommon.ToString(dsManufacturer.Tables[0].Rows[0]["VcFileName"]);
                                        strUI = strUI.Replace("##ManufacturerLogo##", strFileName);
                                        strUI = strUI.Replace("##ManufacturerName##", CCommon.ToString(dsManufacturer.Tables[0].Rows[0]["vcCompanyName"]));
                                        lblManufacturerName.Text = CCommon.ToString(dsManufacturer.Tables[0].Rows[0]["vcCompanyName"]);
                                        strUI = strUI.Replace("##ManufacturerDescription##", CCommon.ToString(dsManufacturer.Tables[0].Rows[0]["txtComments"]));
                                    }
                                    else
                                    {
                                        strUI = strUI.Replace("##ManufacturerLogo##", "");
                                        strUI = strUI.Replace("##ManufacturerName##", "");
                                        strUI = strUI.Replace("##ManufacturerDescription##", "");
                                    }
                                }
                                else
                                {
                                    strUI = strUI.Replace("##ManufacturerLogo##", "");
                                    strUI = strUI.Replace("##ManufacturerName##", "");
                                    strUI = strUI.Replace("##ManufacturerDescription##", "");
                                }
                                strUI = Sites.RemoveMatchedPattern(strUI, strPatternCategoryImageSaction);
                                strUI = strUI.Replace("##CategoryDesc##", "");
                                lblCategoryName.Text = Sites.GenerateSEOFriendlyURL(Request["CatName"]);
                                strUI = strUI.Replace("##CategoryName##", CCommon.RenderControl(lblCategoryName));
                            }
                            else
                            {
                                objItems.byteMode = 17;
                                //dtTableCategory = objItems.SeleDelCategory();

                                if (Session["IsDDCacheCategory"] != null && CCommon.ToBool(Session["IsDDCacheCategory"]) == true)
                                {
                                    string CacheNameForCategory = string.Format("CacheSeleDelCategory_{0}_{1}_{2}", objItems.byteMode, objItems.SiteID, objItems.CategoryID);
                                    if (Cache[CacheNameForCategory] == null)
                                    {
                                        Cache.Insert(CacheNameForCategory, objItems.SeleDelCategory());
                                    }
                                    dtTableCategory = (DataTable)Cache[CacheNameForCategory];
                                }
                                else
                                {
                                    dtTableCategory = objItems.SeleDelCategory();
                                }

                                strUI = strUI.Replace("##ManufacturerLogo##", "");
                                strUI = strUI.Replace("##ManufacturerName##", "");
                                strUI = strUI.Replace("##ManufacturerDescription##", "");
                                if (dtTableCategory.Rows.Count > 0)
                                {
                                    if (CCommon.ToString(dtTableCategory.Rows[0]["vcPathForCategoryImage"]) != "")
                                    {
                                        strUI = Sites.ReplaceMatchedPatternWithContent(strUI, strPatternCategoryImageSaction);
                                        strUI = strUI.Replace("##CategoryImage##", GetImagePath(CCommon.ToString(dtTableCategory.Rows[0]["vcPathForCategoryImage"])));
                                    }
                                    else
                                        strUI = Sites.RemoveMatchedPattern(strUI, strPatternCategoryImageSaction);

                                    lblCategoryName.Text = Sites.ToString(dtTableCategory.Rows[0]["vcCategoryName"]);

                                    strUI = strUI.Replace("##CategoryName##", lblCategoryName.Text);
                                    strUI = strUI.Replace("##CategoryDesc##", CCommon.ToString(dtTableCategory.Rows[0]["vcDescription"]));
                                    strUI = strUI.Replace("##CategoryImage##", "");
                                }
                                else
                                {
                                    strUI = Sites.RemoveMatchedPattern(strUI, strPatternCategoryImageSaction);
                                    strUI = strUI.Replace("##CategoryDesc##", "");
                                    lblCategoryName.Text = Sites.GenerateSEOFriendlyURL(Request["CatName"]);
                                    strUI = strUI.Replace("##CategoryName##", CCommon.RenderControl(lblCategoryName));
                                }
                            }
                        }

                        objItems.SiteID = Sites.ToLong(Session["SiteID"]);

                        objItems.WarehouseID = Sites.ToLong(Session["WareHouseID"]);
                        objItems.CurrentPage = AspNetPager1.CurrentPageIndex;//PageIndex.ToString();
                        objItems.PageSize =Convert.ToInt32(ddlPageSize.SelectedValue);
                        objItems.TotalRecords = 0;
                        string ActualSearchString = "";

                        if (IsElasticSearchEnable)
                        {
                            if (SearchText.Length > 0)
                            {
                                objItems.SearchText = SearchText;
                            }
                        }
                        else
                        {
                            if (SearchText.Length > 0)
                            {
                                //validate if index exist? 
                                if (!System.IO.Directory.Exists(GetIndexPath()))
                                {
                                    ShowMessage(GetErrorMessage("ERR042"), 1); //Search is not configured, Please configure it from e-commerce settings.
                                    return;
                                }

                                //Search for exact match
                                objItems.SearchText = SearchLucene.SearchIndex(GetIndexPath(), SearchLucene.LuceneEscapeSpecialChar(SearchText.Trim()).Trim(), IndexType.Items, ActuallSearchString: ref ActualSearchString);

                                if (objItems.SearchText.Trim().Length > 0)
                                {
                                    if (CCommon.ToString(System.Configuration.ConfigurationManager.AppSettings["IsDebugMode"]).ToLower() == "true")
                                        Response.Write(ActualSearchString);
                                    AddRelevanceListItem();
                                }
                                //When Exact match not found split string and search.
                                else
                                {
                                    objItems.SearchText = SearchLucene.SearchIndex(GetIndexPath(), SearchLucene.LuceneEscapeSpecialChar(SearchText.Trim(), false).Trim(), IndexType.Items, ActuallSearchString: ref ActualSearchString);

                                    if (objItems.SearchText.Trim().Length > 0)
                                    {
                                        if (CCommon.ToString(System.Configuration.ConfigurationManager.AppSettings["IsDebugMode"]).ToLower() == "true")
                                            Response.Write(ActualSearchString);
                                        AddRelevanceListItem();
                                    } else
                                    {
                                        objItems.SearchText = "0";
                                        AddRelevanceListItem();
                                    }
                                }
                            }
                            if (AdvancedSearchText.Length > 0)
                            {
                                if (CCommon.ToString(System.Configuration.ConfigurationManager.AppSettings["IsDebugMode"]).ToLower() == "true")
                                    Response.Write(ActualSearchString);
                                objItems.SearchText = AdvancedSearchText;
                                //objItems.AdvancedSearchText = AdvancedSearchText;
                            }
                        }

                        //objItems.SortBy = short.Parse(ddlSortBy.SelectedValue);

                        //Creating Sorting String for Item Cart
                        if (CCommon.ToString(ddlSortBy.SelectedValue) == "507")//Title:A-Z
                        {
                            objItems.SortByString = " vcItemName Asc ";
                        }
                        else if (CCommon.ToString(ddlSortBy.SelectedValue) == "508")//Title:Z-A
                        {
                            objItems.SortByString = " vcItemName Desc ";
                        }
                        else if (CCommon.ToString(ddlSortBy.SelectedValue) == "509")//Price:Low to High
                        {
                            objItems.SortByString = " monListPrice Asc ";
                        }
                        else if (CCommon.ToString(ddlSortBy.SelectedValue) == "510")//Price:High to Low
                        {
                            objItems.SortByString = " monListPrice Desc ";
                        }
                        else if (CCommon.ToString(ddlSortBy.SelectedValue) == "511")//New Arrival
                        {
                            objItems.SortByString = " bintCreatedDate Desc ";
                        }
                        else if (CCommon.ToString(ddlSortBy.SelectedValue) == "512")//Oldest
                        {
                            objItems.SortByString = " bintCreatedDate ASC ";
                        }
                        else if (CCommon.ToString(ddlSortBy.SelectedValue) == "7")
                        {
                            objItems.SortByString = " SearchOrder ASC ";
                        }
                        else
                        {
                            string[] strItemValue = { "" };
                            long lngFieldID = 0;
                            string strDbColumnName, SortOrder;
                            short sCustom;

                            strItemValue = CCommon.ToString(ddlSortBy.SelectedValue).Split('~');
                            if (strItemValue.Length > 2)
                            {
                                lngFieldID = CCommon.ToLong(strItemValue[0]);
                                sCustom = CCommon.ToShort(strItemValue[1]);
                                strDbColumnName = CCommon.ToString(strItemValue[2]);
                                SortOrder = CCommon.ToString(strItemValue[3]);

                                if (sCustom == 0)
                                {
                                    objItems.SortByString = strDbColumnName + " " + SortOrder;
                                }
                                else
                                {
                                    if (SortOrder.ToLower() == "asc")
                                    {
                                        objItems.SortByString = strDbColumnName + "~0";
                                    }
                                    else
                                    {
                                        objItems.SortByString = strDbColumnName + "~1";
                                    }
                                }
                            }
                        }

                        DataSet dsItems = default(DataSet);
                        DataTable dtItemColumn = default(DataTable);

                        string filterQuery = "";
                        if (this.filter != "")
                        {
                            string[] filterEach = this.filter.Split(';');
                            for (int i = 0; i < filterEach.Length - 1; i++)
                            {
                                string[] filterFinal;

                                if (filterEach[i].ToLower().StartsWith("f"))
                                {
                                    #region f Querystring
                                    if (filterEach[i].ToLower().Contains("min") && filterEach[i].ToLower().Contains("max"))
                                    {
                                        filterFinal = filterEach[i].Split('~', '^');
                                        filterQuery = filterQuery + "f>-2=" + (filterFinal[1] + "~" + filterFinal[3]) + ";";
                                    }
                                    else if (filterEach[i].ToLower().Contains("manufacturer"))
                                    {

                                        filterFinal = filterEach[i].Split('~', '^');
                                        filterQuery = filterQuery + "f>" + filterFinal[2] + "=" + filterFinal[3] + ";";
                                    }
                                    else
                                    {
                                        filterFinal = filterEach[i].Split('~', '^');
                                        filterQuery = filterQuery + "f>" + filterFinal[2] + "=" + filterFinal[4] + ";";
                                    }
                                    #endregion
                                }
                                else if (filterEach[i].ToLower().StartsWith("c"))
                                {
                                    filterFinal = filterEach[i].Split('~', '^');
                                    filterQuery = filterQuery + "c>" + filterFinal[2] + "=" + filterFinal[4] + ";";
                                }
                            }
                            if (filterQuery.Length > 1)
                            {
                                objItems.FilterQuery = filterQuery.Substring(0, filterQuery.Length - 1);
                            }
                        }

                        objItems.FilterRegularCondition = Sites.ToString(Session["frc"]);
                        objItems.FilterCustomFields = Sites.ToString(Session["fcfields"]);
                        objItems.FilterCustomWhere = Sites.ToString(Session["fcwhere"]);

                        if (!string.IsNullOrEmpty(hdnSelectedProductFilters.Value))
                        {
                            DataTable dtFilters = JsonConvert.DeserializeObject<DataTable>(hdnSelectedProductFilters.Value);

                            if (dtFilters != null && dtFilters.Rows.Count > 0)
                            {
                                int filterIndex = 0;
                                string productFilters = "";
                                

                                DataTable dtFilterFields = dtFilters.DefaultView.ToTable(true, "FieldID");

                                foreach (DataRow drFilterField in dtFilterFields.Rows)
                                {
                                    productFilters = productFilters + " INNER JOIN ItemAttributes IA" + filterIndex.ToString() + " ON IA" + filterIndex.ToString() + ".numItemCode=I.numItemCode AND (IA" + filterIndex.ToString() + ".Fld_ID = " + CCommon.ToString(drFilterField["FieldID"]) + " AND (";

                                    string filterConditions = "";

                                    foreach (DataRow drFilter in dtFilters.Rows)
                                    {
                                        if (Sites.ToLong(drFilterField["FieldID"]) == Sites.ToLong(drFilter["FieldID"]))
                                        {
                                            filterConditions += (filterConditions != "" ? " OR " : "") + "IA" + filterIndex.ToString() + ".Fld_Value = " + CCommon.ToString(drFilter["select2Value"]);
                                        }
                                    }

                                    productFilters = productFilters + filterConditions + "))";

                                    filterIndex += 1;
                                }

                                objItems.FilterItemAttributes = productFilters;
                            }                            
                        }

                        
                        if ((!System.Web.HttpContext.Current.User.Identity.IsAuthenticated || CCommon.ToLong(Session["DomainID"]) == 0 || CCommon.ToLong(Session["UserContactID"]) == 0) && Sites.ToInteger(Session["OnePageCheckout"]) == 0)
                        {
                            //GUEST USER
                            objItems.DivisionID = 0;
                        }
                        else
                        {
                            objItems.DivisionID = Sites.ToLong(Session["DivId"]);
                        }
                        objItems.ManufacturerID = ManufacturerID;

                        if (IsElasticSearchEnable)
                        {
                            DateTime start = DateTime.Now;
                            bool IsAvaiablityTokenCheck = (Content != null && Content.Contains("##Availability##"));
                            dsItems = GetItemForECommerceIndexFromES(objItems, tintDisplayCategory, bitAutoSelectWarehouse, IsAvaiablityTokenCheck);

                            TimeSpan timeDiff = DateTime.Now - start;

                            if (Session["IsDDTimeDiffrent"] != null && CCommon.ToBool(Session["IsDDTimeDiffrent"]) == true)
                            {
                                ScriptManager.RegisterClientScriptBlock(this.Page, Page.GetType(), "DDTimeDiffrent_GetItemForECommerceIndexFromES", "console.log('DDTimeDiffrent_GetItemForECommerceIndexFromES','" + timeDiff.TotalMilliseconds + "');", true);
                            }
                        }
                        else
                        {
                            dsItems = objItems.GetItemsForECommerce();
                        }


                        Session["AdvancedSearch"] = null;
                        //Session["frc"] = null;
                        //Session["fcfields"] = null;
                        //Session["fcwhere"] = null;

                        DateTime DataTablestart = DateTime.Now;
                        if (dsItems.Tables.Count > 0)
                        {
                            DataTable dtItems = dsItems.Tables[0];
                            dtItemColumn = dsItems.Tables[1];

                            int PageSize = Sites.ToInteger(ddlPageSize.SelectedValue);
                            string Value = string.Empty;
                            PageIndex = AspNetPager1.CurrentPageIndex;
                            if ((PageSize * PageIndex) > objItems.TotalRecords)
                                Value = objItems.TotalRecords.ToString();
                            else
                                Value = (PageSize * PageIndex).ToString();
                            lblItemCount.Text = "Item(s): " + (objItems.TotalRecords == 0 ? "0" : (((PageSize * PageIndex) + 1) - PageSize).ToString()) + "-" + Value + " of " + objItems.TotalRecords.ToString();
                            AspNetPager1.RecordCount = objItems.TotalRecords;
                            if (dtItems.Rows.Count > 0)
                            {
                                CItems objItemAvailability = new CItems();
                                objItemAvailability.DomainID = CCommon.ToLong(Session["DomainID"]);

                                StringBuilder sb = new StringBuilder();

                                string strTemp = string.Empty;
                                int j = 0;
                                int rowIndex = 0;
                                int columncount = 0;
                                int clearFix1 = 0;
                                int clearFix2 = 0;
                                int clearFix3 = 0;
                                foreach (DataRow dr in dtItems.Rows)
                                {
                                    rowIndex+=1;

                                    if (htProperties.ContainsKey("NoOfColumn") && Int32.TryParse(Convert.ToString(htProperties["NoOfColumn"]), out columncount))
                                    {
                                        if (columncount > 0)
                                        {
                                            if (j == 0)
                                            {
                                                if (htProperties.ContainsKey("RowClass"))
                                                {
                                                    sb.AppendLine("<div class=\"" + CCommon.ToString(htProperties["RowClass"]) + "\">");
                                                }
                                                else
                                                {
                                                    sb.AppendLine("<div class=\"row\">");
                                                }
                                            }
                                            else if (Int32.TryParse(Sites.ToString(htProperties["ClearFix1"]), out clearFix1) && j == clearFix1)
                                            {
                                                sb.AppendLine("<div class='clearfix visible-md'></div>");
                                            }
                                            else if (Int32.TryParse(Sites.ToString(htProperties["ClearFix2"]), out clearFix2) && j == clearFix2)
                                            {
                                                sb.AppendLine("<div class='clearfix visible-sm'></div>");
                                            }
                                            else if (Int32.TryParse(Sites.ToString(htProperties["ClearFix3"]), out clearFix3) && j == clearFix3)
                                            {
                                                sb.AppendLine("<div class='clearfix visible-xs'></div>");
                                            }
                                        }
                                    }

                                    strTemp = Content;
                                    string url = GetProductURL(dr["vcCategoryName"].ToString(), dr["vcItemName"].ToString(), dr["numItemCode"].ToString());

                                    string ItemImagePath = "/Images/DefaultProduct.png";
                                    if (!string.IsNullOrEmpty(dr["vcPathForTImage"].ToString()))
                                    {
                                        ItemImagePath = GetImagePath(ReplaceAllSpaces(dr["vcPathForTImage"].ToString()));
                                    }
                                    if (IsElasticSearchEnable)
                                    {
                                        strTemp = strTemp.Replace("##ColorDropDown##", "");
                                    }
                                    else
                                    {
                                        CItems objItemsAssembly = new CItems();
                                        DataSet ds = new DataSet();
                                        objItemsAssembly.ItemCode = Sites.ToInteger(dr["numItemCode"].ToString());
                                        objItemsAssembly.DomainID = Sites.ToLong(Session["DomainID"]);
                                        ds = objItemsAssembly.GetProfileItems();
                                        if (ds.Tables.Count > 0)
                                        {
                                            if (ds.Tables[0].Rows.Count > 0)
                                            {
                                                DropDownList ddlItemAttribute = new DropDownList();
                                                ddlItemAttribute.ID = "ddlProfileItem" + dr["numItemCode"].ToString();
                                                ddlItemAttribute.DataSource = ds.Tables[0];
                                                ddlItemAttribute.DataTextField = "vcData";
                                                ddlItemAttribute.DataValueField = "numOppAccAttrID";
                                                ddlItemAttribute.DataBind();
                                                strTemp = strTemp.Replace("##ColorDropDown##", CCommon.RenderControl(ddlItemAttribute));
                                            }
                                            else
                                            {
                                                strTemp = strTemp.Replace("##ColorDropDown##", "");
                                            }
                                        }
                                        else
                                        {
                                            strTemp = strTemp.Replace("##ColorDropDown##", "");
                                        }
                                    }

                                    if (Sites.ToInteger(dr["bitHasChildKits"]).ToString() == "1")
                                    {
                                        strTemp = strTemp.Replace("##ConfigureKitLink##", "<a onclick=\"javascript: document.location.href = '" + ResolveUrl("~/configurator.aspx") + "?ItemID=" + Sites.ToString(dr["numItemCode"]) + "&CategoryID=" + CategoryID.ToString() + "';\" class=\"configurekitlink\">Configure</a>");
                                        strTemp = strTemp.Replace("##AddToCartLink##", "");
                                    }
                                    else
                                    {
                                        strTemp = strTemp.Replace("##ConfigureKitLink##", "");
                                    }

                                    strTemp = strTemp.Replace("##ItemLink##", url);
                                    strTemp = strTemp.Replace("##ItemName##", dr["vcItemName"].ToString());
                                    strTemp = strTemp.Replace("##ItemShortName##", dr["vcItemName"].ToString().Length > TrimNameLength ? dr["vcItemName"].ToString().Substring(0, TrimNameLength) + ".." : dr["vcItemName"].ToString());
                                    strTemp = strTemp.Replace("##ItemImagePathName##", ItemImagePath);
                                    strTemp = strTemp.Replace("##FirstPriceLevelValue##", CCommon.ToString(dr["monFirstPriceLevelPrice"]));
                                    strTemp = strTemp.Replace("##FirstPriceLevelDiscount##", CCommon.ToString(dr["fltFirstPriceLevelDiscount"]));
                                    strTemp = strTemp.Replace("##ItemAttributes##", Convert.ToString(dr["vcAttributes"]));
                                    
                                    objItems.byteMode = 2;
                                    objItems.ItemCode = Sites.ToInteger(dr["numItemCode"]);

                                    if (IsElasticSearchEnable)
                                    {
                                        strTemp = strTemp.Replace("##LongDescription##", CCommon.ToString(dr["txtDesc"]));
                                        strTemp = strTemp.Replace("##ShortHtmlDescription##", CCommon.ToString(dr["txtShortDesc"]));
                                    }
                                    else
                                    {
                                        DataTable dt = new DataTable();
                                        dt = objItems.ViewItemExtendedDesc();
                                        if (dt.Rows.Count > 0)
                                        {
                                            strTemp = strTemp.Replace("##LongDescription##", Convert.ToString(dt.Rows[0]["txtDesc"]));
                                            strTemp = strTemp.Replace("##ShortHtmlDescription##", Convert.ToString(dt.Rows[0]["txtShortDesc"]));
                                        }
                                        else
                                        {
                                            strTemp = strTemp.Replace("##LongDescription##", "");
                                            strTemp = strTemp.Replace("##ShortHtmlDescription##", "");
                                        }
                                    }

                                    if (!CCommon.ToBool(Session["IsShowPromoDetailsLink"]) && CCommon.ToInteger(dr["numTotalPromotions"]) == 1)
                                    {
                                        strTemp = strTemp.Replace("##PromotionDescription##", "<img src='/images/item-promotion.png' class='imgitempromo' />" + " " + CCommon.ToString(dr["vcPromoDesc"]));
                                    }
                                    else if (CCommon.ToInteger(dr["numTotalPromotions"]) > 0)
                                    {
                                        strTemp = strTemp.Replace("##PromotionDescription##", "<a href='javascript:LoadItemPromoPopup(" + Sites.ToString(dr["numItemCode"]) + ");' class='linkitempromo'><img src='/images/item-promotion.png' class='imgitempromo' /> Click for details</a>");
                                    }
                                    else
                                    {
                                        strTemp = strTemp.Replace("##PromotionDescription##", "");
                                    }

                                    Label ProductPrice = new Label();
                                    ProductPrice.ID = "lblProductPrice" + Sites.ToString(dr["numItemCode"]);
                                    ProductPrice.CssClass = "prod_price";

                                    Label MSRPPrice = new Label();
                                    MSRPPrice.ID = "lblMSRPPrice" + Sites.ToString(dr["numItemCode"]);
                                    MSRPPrice.CssClass = "prod_price";

                                    Label ProductFirstPriceLevelValue = new Label();
                                    ProductFirstPriceLevelValue.CssClass = "prod_price";

                                    Label FirstPriceLevelDiscount = new Label();

                                    Label OfferPrice = new Label();
                                    OfferPrice.ID = "lblOfferPrice" + Sites.ToString(dr["numItemCode"]);
                                    Label ProductDesc = new Label();

                                    double dProductPrice = 0, dOfferPrice = 0, dMSRPPrice = 0;
                                    //double dFirstPriceLevelValue = 0, dFirstPriceLevelDiscount = 0;
                                    decimal ExchangeRate;
                                    ExchangeRate = Sites.ToDecimal(Session["ExchangeRate"]) == 0 ? 1 : Sites.ToDecimal(Session["ExchangeRate"]);

                                    dProductPrice = Sites.ToDouble(Sites.ToDecimal(dr["monListPrice"]) / ExchangeRate);
                                    dMSRPPrice = Sites.ToDouble(Sites.ToDecimal(dr["monMSRP"]) / ExchangeRate);
                                    ProductPrice.Text = Sites.ToString(Session["CurrSymbol"]) + " " + string.Format("{0:#,##0.00###}", dProductPrice);
                                    MSRPPrice.Text = Sites.ToString(Session["CurrSymbol"]) + " " + string.Format("{0:#,##0.00###}", Sites.ToDouble(Sites.ToDecimal(dr["monMSRP"]) / ExchangeRate));


                                    //Hide prices for site visitors until they log in as a registered user.
                                    //Hide prices for site registered site visitors AFTER they log in as a registered user, with the following Relationship &  Profile
                                    if (Sites.ToBool(Session["HidePrice"]) == true)
                                        OfferPrice.Visible = ProductPrice.Visible = MSRPPrice.Visible = false;

                                    ProductDesc.Text = dr["txtItemDesc"].ToString();

                                    if (Sites.ToInteger(TrimDescriptionbyfollowingnoofcharacters) > 0 && ProductDesc.Text.Length > Sites.ToInteger(TrimDescriptionbyfollowingnoofcharacters))
                                        ProductDesc.Text = ProductDesc.Text.Substring(0, Sites.ToInteger(TrimDescriptionbyfollowingnoofcharacters)) + "..";
                                    //}

                                    // Replacing Custom fields
                                    if (dtItemColumn.Rows.Count > 0)
                                    {
                                        foreach (DataRow drCustom in dtItemColumn.Rows)
                                        {
                                            string FieldName = string.Format("{0}_{1}", drCustom["vcFormFieldName"].ToString(), drCustom["vcFieldType"].ToString());
                                            strTemp = strTemp.Replace("##" + FieldName + "##", dr[FieldName].ToString());
                                        }
                                    }

                                    /*Apply pricebook rule in Item list */
                                    if (Sites.ToBool(ApplyPriceBookPrice) == true && Sites.ToBool(Session["HidePrice"]) == false)
                                    {
                                        PriceBookRule objPbook = new PriceBookRule();
                                        objPbook.ItemID = Sites.ToLong(dr["numItemCode"]);
                                        objPbook.QntyofItems = Sites.ToInteger(1 * Sites.ToDecimal(dr["UOMConversionFactor"]));
                                        objPbook.DomainID = Sites.ToLong(Session["DomainID"]);
                                        objPbook.DivisionID = Sites.ToLong(Session["DivId"]);
                                        objPbook.WarehouseID = Sites.ToLong(Session["WareHouseID"]);
                                        objPbook.SiteID = Sites.ToLong(HttpContext.Current.Session["SiteId"]);
                                        DataTable dtCalPrice = default(DataTable);
                                        dtCalPrice = objPbook.GetPriceBasedonPriceBook();
                                        if (dtCalPrice.Rows.Count > 0)
                                        {
                                            dOfferPrice = Sites.ToDouble(Sites.ToDecimal(dtCalPrice.Rows[0]["ListPrice"]) * Sites.ToDecimal(dr["UOMConversionFactor"]) / Sites.ToDecimal(Session["ExchangeRate"].ToString()));
                                            OfferPrice.Text = Sites.ToString(Session["CurrSymbol"]) + " " + CCommon.ToString(dOfferPrice);
                                        }

                                        if (dOfferPrice != dProductPrice && dOfferPrice > 0)
                                        {
                                            OfferPrice.Text = Sites.ToString(Session["CurrSymbol"]) + " " + string.Format("{0:#,##0.00###}", dOfferPrice);
                                            ProductPrice.CssClass = "prod_strick";
                                            MSRPPrice.CssClass = "prod_strick";
                                            OfferPrice.CssClass = "prod_price";
                                        }
                                        else
                                        {
                                            OfferPrice.Style.Add("display","none");
                                        }
                                    }
                                    else if (!(!System.Web.HttpContext.Current.User.Identity.IsAuthenticated || CCommon.ToLong(Session["DomainID"]) == 0 || CCommon.ToLong(Session["UserContactID"]) == 0) && dProductPrice != dMSRPPrice)
                                    {
                                        ProductPrice.CssClass = "prod_price";
                                        MSRPPrice.CssClass = "prod_strick";
                                        OfferPrice.Style.Add("display", "none");
                                    }
                                    else
                                    {
                                        MSRPPrice.Style.Add("display", "none");
                                        OfferPrice.Style.Add("display", "none");
                                    }

                                    strTemp = strTemp.Replace("##OfferPrice##", CCommon.RenderControl(OfferPrice));
                                    strTemp = strTemp.Replace("##ProductPrice##", CCommon.RenderControl(ProductPrice));
                                    strTemp = strTemp.Replace("##MSRP##", CCommon.RenderControl(MSRPPrice));
                                    strTemp = strTemp.Replace("##ItemDesc##", CCommon.RenderControl(ProductDesc));
                                    strTemp = strTemp.Replace("##Availability##", dr["InStock"].ToString());
                                    if (strTemp.Contains("##PriceLevelTable##"))
                                    {
                                        string priceLevels = objItems.GetPriceLevelEcommHtml();
                                        strTemp = strTemp.Replace("##PriceLevelTable##", priceLevels);
                                    }

                                    bool isSelectCheckboxVisible = true;

                                    if (Sites.ToBool(Session["HideAddtoCartBeforeLogin"]) == true && Sites.ToLong(Session["UserContactID"]) == 0)
                                    {
                                        strUI = strUI.Replace("##AddToCartLink##", "");
                                        strTemp = strTemp.Replace("##AddToCartLink##", "");
                                        isSelectCheckboxVisible = false;
                                    }
                                    else
                                    {
                                        if (Sites.ToBool(dr["bitHasChildKits"]))
                                        {
                                            strTemp = strTemp.Replace("##AddToCartLink##", "<input id=\"AddToCartLink\" name=\"AddToCartLink\" type=\"button\" onclick=\"document.location.href='" + url + "';\" class=\"addtocart btn btn-primary\" value=\"Add to Cart\">");
                                            isSelectCheckboxVisible = false;
                                        }
                                        else
                                        {
                                            if (Sites.ToBool(dr["bitInStock"]))
                                            {
                                                strTemp = strTemp.Replace("##AddToCartLink##", "<input id=\"AddToCartLink\" name=\"AddToCartLink\" type=\"button\" onclick=\"AddItemToCart(" + Sites.ToString(dr["numItemCode"]) + ",'" + "txtQuantity" + rowIndex.ToString() + "');\" class=\"addtocart btn btn-primary\" value=\"Add to Cart\">");
                                            }
                                            else
                                            {
                                                strTemp = strTemp.Replace("##AddToCartLink##", "<label class='calltoorder'>Call to Order</lable>");
                                                isSelectCheckboxVisible = false;
                                            }
                                        }
                                    }

                                    if (isSelectCheckboxVisible)
                                    {
                                        strTemp = strTemp.Replace("##SelectCheckBox##", "<input type='checkbox' class='chkSelect form-check-input' id='" + rowIndex.ToString() + "~" + CCommon.ToInteger(dr["numItemCode"]).ToString() + "'>");                                        
                                    }
                                    else
                                    {
                                        strTemp = strTemp.Replace("##SelectCheckBox##", "");
                                    }
                                    
                                    strTemp = strTemp.Replace("##SKU##", dr["vcSKU"].ToString());
                                    strTemp = strTemp.Replace("##ModelID##", dr["vcModelID"].ToString());

                                    if (RowsCount % 2 == 0)
                                        strTemp = strTemp.Replace("##RowClass##", Sites.ToString(htProperties["AlternatingItemClass"]));
                                    else
                                        strTemp = strTemp.Replace("##RowClass##", Sites.ToString(htProperties["ItemClass"]));

                                    strTemp = strTemp.Replace("##ItemID##", dr["numItemCode"].ToString());
                                    strTemp = strTemp.Replace("##WarehouseItemID##", dr["numWareHouseItemID"].ToString());

                                    if (strTemp.Contains("##WarehouseAvailability##"))
                                    {
                                        objItemAvailability.ItemCode = CCommon.ToInteger(dr["numItemCode"]);
                                        strTemp = strTemp.Replace("##WarehouseAvailability##", CCommon.ToString(objItemAvailability.GetEcommerceWarehouseAvailability(warehouseAvailability, isDisplayQtyAvailable)));
                                    }

                                    if (strTemp.Contains("##WarehouseInTransit##"))
                                    {
                                        objItemAvailability.ItemCode = CCommon.ToInteger(dr["numItemCode"]);
                                        strTemp = strTemp.Replace("##WarehouseInTransit##", CCommon.ToString(objItemAvailability.GetEcommerceWarehouseInTransit()));
                                    }

                                    if (strTemp.Contains("##Quantity##"))
                                    {
                                        TextBox txtQuantity = new TextBox();
                                        txtQuantity.ID = "txtQuantity" + rowIndex.ToString(); //Id is used in javascript
                                        txtQuantity.Text = "1";
                                        txtQuantity.CssClass = "txtQuantity form-control";

                                        if (Sites.ToBool(ApplyPriceBookPrice) == true && Sites.ToBool(Session["HidePrice"]) == false)
                                        {
                                            txtQuantity.Attributes.Add("onchange", "ItemQuantityChanged(this," + Sites.ToLong(dr["numItemCode"]) + "," + Sites.ToDecimal(dr["UOMConversionFactor"]) + ");");
                                        }

                                        if (Sites.ToBool(Session["HideAddtoCartBeforeLogin"]) == true && Sites.ToLong(Session["UserContactID"]) == 0)
                                        {
                                            strTemp = strTemp.Replace("##Quantity##", "");
                                        }
                                        else
                                        {
                                            strTemp = strTemp.Replace("##Quantity##", CCommon.RenderControl(txtQuantity));
                                        }
                                    }

                                    if (Sites.ToBool(Session["HideAddtoCartBeforeLogin"]) == true && Sites.ToLong(Session["UserContactID"]) == 0)
                                    {
                                        strTemp = strTemp.Replace("##HidePriceBeforeLogin##", "hideAddtoCart");
                                    }
                                    else
                                    {
                                        strTemp = strTemp.Replace("##HidePriceBeforeLogin##", "showAddtoCart");
                                    }

                                    sb.AppendLine(strTemp);

                                    if (htProperties.ContainsKey("NoOfColumn") && Int32.TryParse(Convert.ToString(htProperties["NoOfColumn"]), out columncount))
                                    {
                                        if (columncount > 0)
                                        {
                                            if (j == columncount - 1)
                                            {
                                                sb.AppendLine("</div>");
                                                j = 0;
                                            }
                                            else
                                            {
                                                j++;
                                            }
                                        }
                                    }
                                    RowsCount++;
                                }
                                if (htProperties.ContainsKey("NoOfColumn") && Int32.TryParse(Convert.ToString(htProperties["NoOfColumn"]), out columncount))
                                {
                                    if (columncount > 0 && j != 0)
                                    {
                                        sb.AppendLine("</div>");
                                    }
                                }

                                strUI = Regex.Replace(strUI, pattern, sb.ToString());
                                //strUI = strUI.Replace("##CategoryDesc##", Sites.ToString(dtItems.Rows[0]["CategoryDesc"]));
                            }
                            else
                            {
                                strUI = Regex.Replace(strUI, pattern, " ");
                                //strUI = strUI.Replace("##CategoryDesc##", "");
                                lblCategoryName.Text = Sites.GenerateSEOFriendlyURL(Request["CatName"]);
                                strUI = Sites.RemoveMatchedPattern(strUI, strPatternCategoryImageSaction);
                                strUI = strUI.Replace("##CategoryDesc##", "");

                                if (SearchText.Length > 0)
                                {
                                    lblCategoryName.Text = "Your search \"" + SearchText + "\" did not match any products.";
                                }
                            }
                        }
                        TimeSpan DataTabletimeDiff = DateTime.Now - DataTablestart;

                        if (Session["IsDDTimeDiffrent"] != null && CCommon.ToBool(Session["IsDDTimeDiffrent"]) == true)
                        {
                            ScriptManager.RegisterClientScriptBlock(this.Page, Page.GetType(), "DDTimeDiffrent_ItemDataTableBindingForUI", "console.log('DDTimeDiffrent_ItemDataTableBindingForUI','" + DataTabletimeDiff.TotalMilliseconds + "');", true);
                        }

                        strUI = strUI.Replace("##CategoryName##", CCommon.RenderControl(lblCategoryName));
                        strUI = strUI.Replace("##SortBy##", CCommon.RenderControl(ddlSortBy));
                        strUI = strUI.Replace("##PageSize##", CCommon.RenderControl(ddlPageSize));
                        strUI = strUI.Replace("##ItemCount##", CCommon.RenderControl(lblItemCount));
                        strUI = strUI.Replace("##PageControl##", CCommon.RenderControl(AspNetPager1));
                        strUI = strUI.Replace("##SelectAllCheckBox##", "<input type='checkbox' class='chkSelectAll form-check-input' onchange=\"SelectAllRecords(this);\">");

                        strUI = strUI.Replace("##AddToMyItems##","<button type=\"button\" class=\"btn btn-primary\" onClick=\"return AddToMyItems();\">Add to My Items</button>");
                        strUI = strUI.Replace("##AddToCartSelected##", "<button type=\"button\" class=\"btn btn-primary\" onClick=\"return AddToCartSelected();\">Add to Cart</button>");

                        pnlCutomizeHtml.Visible = false;
                        litCutomizeHtml.Text = strUI;
                    }
                }
            }
            catch (Exception ex)
            {
                if (ex.Message.Contains("no segments* file found in Lucene.Net.Store.SimpleFSDirectory"))
                {
                    ShowMessage(GetErrorMessage("ERR066"), 1);
                }
                else
                {
                    throw ex;
                }
            }
            TimeSpan BindtimeDiff = DateTime.Now - startbindHtml;

            if (Session["IsDDTimeDiffrent"] != null && CCommon.ToBool(Session["IsDDTimeDiffrent"]) == true)
            {
                ScriptManager.RegisterClientScriptBlock(this.Page, Page.GetType(), "DDTimeDiffrent_bindHtml", "console.log('DDTimeDiffrent_bindHtml','" + BindtimeDiff.TotalMilliseconds + "');", true);
            }
        }

        /*Function added by Neelam on 10/11/2017 - Added functionality to bind MiniCart pop up*/
        private void bindMiniCart()
        {
            try
            {
                DataSet ds = new DataSet();
                ds = GetCartItem();
                DataTable dt;

                if (ds != null && ds.Tables.Count > 0)
                {
                    dt = ds.Tables[0];
                    if (dt.Rows.Count > 0)
                    {
                        gvMiniCart.DataSource = dt;
                        gvMiniCart.DataBind();

                        DataTable dtPromotions;
                        DataTable dtShippingPromotions;

                        if (ds != null && ds.Tables.Count > 0)
                        {
                            dt = ds.Tables[0];
                            if (dt.Rows.Count > 0)
                            {
                                decimal total = Sites.ToDecimal(dt.Compute("SUM(monTotAmtBefDiscount)", ""));
                                string strUI = CCommon.ToString(Math.Round(total, 2));
                                strUI = strUI.Replace(strUI, string.Format("{0:#,##0.00###}", Sites.ToDecimal(dt.Compute("SUM(monTotAmtBefDiscount)", ""))));

                                if (Sites.ToBool(Session["HidePrice"]) == false)
                                    lblSubTotal.Text = "SUBTOTAL: USD " + strUI;
                                else
                                    lblSubTotal.Text = strUI.Replace(strUI, "NA");

                                var distinctRows = (from DataRow dRow in dt.Rows
                                                    where CCommon.ToString(dRow["numParentItemCode"]) == "" || dRow["numParentItemCode"] == null
                                                    select new { col1 = dRow["numItemCode"], col2 = dRow["numWarehouseid"] }).Distinct();

                                int _numParentItemCode = 0;
                                long _numWarehouseId = 0;

                                if (distinctRows.Count() > 0)
                                {
                                    foreach (var row in distinctRows)
                                    {
                                        _numParentItemCode = CCommon.ToInteger(row.col1);
                                        _numWarehouseId = CCommon.ToInteger(row.col2);

                                        if (_numParentItemCode != 0 && _numWarehouseId != 0)
                                        {
                                            dtPromotions = BindPromotions(CCommon.ToInteger(_numParentItemCode), CCommon.ToLong(_numWarehouseId));
                                            if (dtPromotions != null && dtPromotions.Rows.Count > 0)
                                            {
                                                divPromotion.Visible = true;
                                                lblVcPromotion.Text = CCommon.ToString(dtPromotions.Rows[0]["vcPromotionDescription"]);
                                                divPromotionDesription.Visible = true;
                                            }
                                            else
                                            {
                                                divPromotion.Visible = false;
                                                lblVcPromotion.Text = string.Empty;
                                                divPromotionDesription.Visible = false;
                                            }
                                        }
                                    }
                                }
                                HttpCookie cookie = Request.Cookies["Cart"];
                                cookie = GetCookie();
                                PromotionOffer objPromotionOffer = new PromotionOffer();
                                objPromotionOffer.DomainID = CCommon.ToLong(Session["DomainID"]);
                                objPromotionOffer.UserCntID = CCommon.ToLong(Session["UserContactID"]);
                                objPromotionOffer.Mode = 1;
                                objPromotionOffer.CookieId = CCommon.ToString(cookie["KeyId"]);
                                dtShippingPromotions = objPromotionOffer.GetShippingPromotions(CCommon.ToLong(Session["DefCountry"]));

                                if (dtShippingPromotions != null && dtShippingPromotions.Rows.Count > 0)
                                {
                                    divShipping.Visible = true;
                                    if (!string.IsNullOrEmpty(CCommon.ToString(dtShippingPromotions.Rows[0]["ShippingDescription"])))
                                    {
                                        lblShipping.Text = CCommon.ToString(dtShippingPromotions.Rows[0]["ShippingDescription"]);
                                    }
                                    else if (!string.IsNullOrEmpty(CCommon.ToString(dtShippingPromotions.Rows[0]["QualifiedShipping"])))
                                    {
                                        lblShipping.Text = CCommon.ToString(dtShippingPromotions.Rows[0]["QualifiedShipping"]);
                                    }
                                    else
                                    { divShipping.Visible = false; }
                                }
                                else
                                { divShipping.Visible = false; }
                            }

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        private void AddRelevanceListItem()
        {
            try
            {
                if (!IsPostBack)
                {
                    ddlSortBy.Items.Add(new ListItem("Relevance", "7"));
                    ddlSortBy.ClearSelection();
                    ddlSortBy.SelectedValue = "7";
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void AddSortItems()
        {
            try
            {
                ddlSortBy.Width = 150;
                DataSet ds = new DataSet();
                CContacts objContact = new CContacts();
                objContact.FormId = 85;
                objContact.ContactType = 0;
                objContact.DomainID = CCommon.ToLong(Session["DomainID"]);
                ds = objContact.GetColumnConfiguration();
                DataTable dtItems = new DataTable();
                dtItems = ds.Tables[1];
                ddlSortBy.Items.Clear();
                foreach (DataRow drItem in dtItems.Rows)
                {
                    //if (CCommon.ToString(drItem["vcFieldDataType"]) == "TextBox" || CCommon.ToString(drItem["vcFieldDataType"]) == "V")
                    //{

                    //Author:Sachin Sadhu||Date:20-Jan-2013||Boneta
                    //Purpose:Dynamically bind Sort By DropDown-So Client can Change default Sort Order
                    if (CCommon.ToString(drItem["FieldId"]) == "507" || CCommon.ToString(drItem["FieldId"]) == "508" || CCommon.ToString(drItem["FieldId"]) == "509" || CCommon.ToString(drItem["FieldId"]) == "510" || CCommon.ToString(drItem["FieldId"]) == "511" || CCommon.ToString(drItem["FieldId"]) == "512")
                    {
                        string[] strID = CCommon.ToString(drItem["numFieldID"]).Split('~');
                        ddlSortBy.Items.Add(new ListItem(CCommon.ToString(drItem["vcFieldName"]), strID[0]));
                    }
                    else
                    {
                        ddlSortBy.Items.Add(new ListItem(CCommon.ToString(drItem["vcFieldName"]) + " : A-Z ", CCommon.ToString(drItem["numFieldID"]) + "~" + CCommon.ToString(drItem["vcDbColumnName"]) + "~Asc"));
                        ddlSortBy.Items.Add(new ListItem(CCommon.ToString(drItem["vcFieldName"]) + " : Z-A ", CCommon.ToString(drItem["numFieldID"]) + "~" + CCommon.ToString(drItem["vcDbColumnName"]) + "~Desc"));
                    }
                    //End of Code by Sachin
                    //}
                    //else if (CCommon.ToString(drItem["vcFieldDataType"]) == "SelectBox")
                    //{
                    //    ddlSortBy.Items.Add(new ListItem(CCommon.ToString(drItem["vcFieldName"]) + " : A-Z ", CCommon.ToString(drItem["numFieldID"])));
                    //    ddlSortBy.Items.Add(new ListItem(CCommon.ToString(drItem["vcFieldName"]) + " : Z-A ", CCommon.ToString(drItem["numFieldID"])));
                    //}
                }
                if (HttpContext.Current.Session["SortBy"] != null)
                {
                    ddlSortBy.SelectedValue = CCommon.ToString(HttpContext.Current.Session["SortBy"]);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void BindPagingDDL()
        {
            try
            {
                DataTable dtTable;
                UserAccess objUserAccess = new UserAccess();
                objUserAccess.DomainID = Sites.ToLong(Session["DomainID"]);
                objUserAccess.SiteID = Sites.ToLong(Session["SiteId"]);
                //dtTable = objUserAccess.GetECommerceDetails();

                if (Session["IsDDCacheECommerceSettings"] != null && CCommon.ToBool(Session["IsDDCacheECommerceSettings"]) == true)
                {
                    string CacheName = string.Format("CacheGetECommerceDetails_{0}_{1}", objUserAccess.DomainID, objUserAccess.SiteID);
                    if (Cache[CacheName] == null)
                    {
                        Cache.Insert(CacheName, objUserAccess.GetECommerceDetails());
                    }
                    dtTable = (DataTable)Cache[CacheName];
                }
                else
                {
                    dtTable = objUserAccess.GetECommerceDetails();
                }

                ddlPageSize.Items.Clear();
                if (dtTable.Rows.Count > 0)
                {
                    int iInitialise = CCommon.ToInteger(dtTable.Rows[0]["numPageSize"]);

                    int iCount = iInitialise * CCommon.ToInteger(dtTable.Rows[0]["numPageVariant"]);

                    for (int i = iInitialise; i <= iCount; i += iInitialise)
                    {
                        ddlPageSize.Items.Add(new ListItem(CCommon.ToString(i), CCommon.ToString(i)));
                    }
                    if (HttpContext.Current.Session["PageSize"] != null)
                    {
                        ddlPageSize.SelectedValue = CCommon.ToString(HttpContext.Current.Session["PageSize"]);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static string ReplaceAllSpaces(string str)
        {
            return Regex.Replace(str, @"\s+", "%20");
        }

        /*Function added by Neelam on 10/11/2017 - Added functionality to check Promotions for similar items*/
        /*private void RelatedItemsPromotion(int numItemCode, long numWarehouseItemID)
        {
            CItems objItems = new CItems();
            objItems.ItemCode = numItemCode;
            objItems.DomainID = CCommon.ToLong(Session["DomainID"]);
            objItems.WareHouseItemID = numWarehouseItemID;
            DataTable dt = objItems.GetPromotionsForSimilarItems();
            if (dt != null && dt.Rows.Count > 0)
            {
                divPromotionDesription.Visible = true;
                //lblPromotionName.Text = CCommon.ToString(dt.Rows[0]["vcProName"]);
                //lblExpiration.Text = CCommon.ToString(dt.Rows[0]["dtExpire"]);
                lblPromotionDescription.Text = CCommon.ToString(dt.Rows[0]["vcPromotionDescription"]);
            }
            else
            {
                divPromotionDesription.Visible = false;
            }
        }*/

        /*Function added by Neelam on 10/11/2017 - Added functionality to bind Promotions*/
        private DataTable BindPromotions(int numItemCode, long numWarehouseItemID)
        {
            CItems objItems = new CItems();
            objItems.ItemCode = numItemCode;
            objItems.DomainID = CCommon.ToLong(Session["DomainID"]);
            objItems.WareHouseItemID = numWarehouseItemID;
            objItems.numSiteId = CCommon.ToLong(HttpContext.Current.Session["SiteID"]);
            DataTable dt = objItems.GetPromotionsForSimilarItems();
            return dt;
        }

        #endregion

        #region Elastic Search
        public DataSet GetItemForECommerceIndexFromES(CItems objItems, bool tintDisplayCategory, bool bitAutoSelectWarehouse, bool IsAvaiablityTokenCheck)
        {
            DataSet ds = new DataSet();
            try
            {
                long domainID = objItems.DomainID;
                long SiteID = objItems.SiteID;
                DataTable dt = new DataTable();
                string _index = string.Concat("biz_cart_", domainID, "_item");

                Nest.ConnectionSettings settings = new Nest.ConnectionSettings(new Uri(ConfigurationManager.AppSettings["ElasticSearchURL"]));
                settings.DisableDirectStreaming(true);
                ElasticClient client = new ElasticClient(settings);

                DataTable dtCustomFields = new DataTable();

                IExistsResponse result = client.IndexExists(new IndexExistsRequest(Indices.Parse(_index + "_fields")));
                if (result != null && result.Exists)
                {
                    var resp = client.Search<object>(s => s.Index(_index + "_fields").From(0).Size(200));

                    var ResponseData = JsonConvert.SerializeObject(resp.Documents, Formatting.None);

                    if (Session["IsDDConsoleESValues"] != null && CCommon.ToBool(Session["IsDDConsoleESValues"]) == true)
                    {
                        ScriptManager.RegisterClientScriptBlock(this.Page, Page.GetType(), "GetIndexFieldResponse", "console.log('GetIndexFieldResponse','" + ResponseData + "');", true);
                    }
                    dtCustomFields = JsonConvert.DeserializeObject<DataTable>(ResponseData);
                }

                result = client.IndexExists(new IndexExistsRequest(Indices.Parse(_index)));

                if (result != null && result.Exists)
                {
                    var GetMappingResponse = client.GetMapping<object>(s => s.Index(_index));

                    if (Session["IsDDConsoleESValues"] != null && CCommon.ToBool(Session["IsDDConsoleESValues"]) == true)
                    {
                        ScriptManager.RegisterClientScriptBlock(this.Page, Page.GetType(), "GetMappingResponse", "console.log('GetMappingResponse','" + JsonConvert.SerializeObject(GetMappingResponse, new JsonSerializerSettings() { ContractResolver = new ElasticContractResolver(settings, new List<Func<Type, JsonConverter>>()) }) + "');", true);
                    }

                    string searchText = objItems.SearchText;
                    if (!string.IsNullOrWhiteSpace(searchText))
                    {
                        searchText = "*" + searchText + "*";
                    }

                    string SortAsc = "";
                    string SortDesc = "";
                    ElasticSearchSortingField(objItems, GetMappingResponse, ref SortAsc, ref SortDesc);

                    int pageIndex = Sites.ToInteger(objItems.CurrentPage);
                    int pageSize = Sites.ToInteger(objItems.PageSize);

                    var func = new List<Func<Nest.QueryContainerDescriptor<object>, Nest.QueryContainer>>();
                    var funcMust = new List<Func<Nest.QueryContainerDescriptor<object>, Nest.QueryContainer>>();
                    var funcMustNot = new List<Func<Nest.QueryContainerDescriptor<object>, Nest.QueryContainer>>();
                    ElasticSearchFilterSet(objItems, dtCustomFields, funcMust, tintDisplayCategory, bitAutoSelectWarehouse);

                    List<string> SearchingField = new List<string>() { "Search_*" };
                    ElasticSearch_SearchIntoSpecificField(objItems, domainID, dtCustomFields, GetMappingResponse, SearchingField);

                    if (!string.IsNullOrEmpty(hdnSelectedProductFilters.Value))
                    {
                        DataTable dtFilters = JsonConvert.DeserializeObject<DataTable>(hdnSelectedProductFilters.Value);

                        if (dtFilters != null && dtFilters.Rows.Count > 0)
                        {
                            DataTable dtFilterFields = dtFilters.DefaultView.ToTable(true, "FieldID");

                            foreach (DataRow drFilterField in dtFilterFields.Rows)
                            {
                                List<string> listAttributes = new List<string>();
                     
                                foreach (DataRow drFilter in dtFilters.Rows)
                                {
                                    if (Sites.ToLong(drFilterField["FieldID"]) == Sites.ToLong(drFilter["FieldID"]))
                                    {
                                        listAttributes.Add(CCommon.ToLong(drFilter["select2Value"]).ToString());
                                    }
                                }

                                if (listAttributes != null && listAttributes.Count > 0)
                                {
                                    funcMust.Add(qry => qry.Match(m => m.Field("vcAttributeValues.FieldID").Query(CCommon.ToString(drFilterField["FieldID"]))) && qry.Terms(t => t.Field("vcAttributeValues.FieldVaue").Terms(listAttributes)));
                                }
                            }
                        }
                    }

                    var resp = client.Search<object>(s => s.Index(_index).IgnoreUnavailable(true)
                            .Query(qry => qry.QueryString(qs => qs.Fields(SearchingField.ToArray()).Query(searchText).DefaultOperator(Nest.Operator.And)))                            
                            .PostFilter(pf => pf.Bool(b => b.Must(funcMust).MustNot(funcMustNot).Should(func)))
                            .From((pageIndex - 1) * pageSize).Take(pageSize)
                            .Sort(ss => ss.Ascending(SortAsc).Descending(SortDesc))
                            );


                    if (Session["IsDDConsoleESValues"] != null && CCommon.ToBool(Session["IsDDConsoleESValues"]) == true)
                    {
                        ScriptManager.RegisterClientScriptBlock(this.Page, Page.GetType(), "ElasticIndexResponse", "console.log('ElasticIndexResp','" + JsonConvert.SerializeObject(resp, new JsonSerializerSettings() { ContractResolver = new ElasticContractResolver(settings, new List<Func<Type, JsonConverter>>()) }) + "');", true);
                        ScriptManager.RegisterClientScriptBlock(this.Page, Page.GetType(), "ElasticIndexResponseDocuments", "console.log('ElasticIndexResponseDocuments','" + JsonConvert.SerializeObject(resp.Documents, Formatting.None) + "');", true);
                        ScriptManager.RegisterClientScriptBlock(this.Page, Page.GetType(), "ElasticIndexRequestBody", "console.log('ElasticIndexRequestBody','" + JsonConvert.SerializeObject(Encoding.UTF8.GetString(resp.ApiCall.RequestBodyInBytes), new JsonSerializerSettings() { ContractResolver = new ElasticContractResolver(settings, new List<Func<Type, JsonConverter>>()) }) + "');", true);
                        ScriptManager.RegisterClientScriptBlock(this.Page, Page.GetType(), "ElasticIndexResponseBody", "console.log('ElasticIndexResponseBody','" + JsonConvert.SerializeObject(Encoding.UTF8.GetString(resp.ApiCall.ResponseBodyInBytes), new JsonSerializerSettings() { ContractResolver = new ElasticContractResolver(settings, new List<Func<Type, JsonConverter>>()) }) + "');", true);
                    }


                    objItems.TotalRecords = Sites.ToInteger(resp.Total);
                    var ResponseData = JsonConvert.SerializeObject(resp.Documents, Formatting.None);
                    dt = ConvertJsonToDatatableLinq(ResponseData);
                    //dt = JsonConvert.DeserializeObject<DataTable>(ResponseData, new JsonSerializerSettings
                    //{
                    //    MissingMemberHandling = MissingMemberHandling.Ignore
                    //});

                    if (Session["IsDDDisableESItemDynamicData"] == null || CCommon.ToBool(Session["IsDDDisableESItemDynamicData"]) == false)
                    {
                        if (dt != null && dt.Rows.Count > 0)
                        {
                            DataSet dsItemPromotion = new DataSet();

                            DataTable dtItem_UOMConversion = new DataTable("dtItem_UOMConversion");
                            dtItem_UOMConversion.Columns.Add(new DataColumn("numItemCode", typeof(int)));
                            dtItem_UOMConversion.Columns.Add(new DataColumn("UOM", typeof(float)));
                            dtItem_UOMConversion.Columns.Add(new DataColumn("UOMPurchase", typeof(float)));

                            for (int i = 0; i < dt.Rows.Count; i++)
                            {
                                dtItem_UOMConversion.Rows.Add(dt.Rows[i]["numItemCode"], dt.Rows[i]["UOMConversionFactor"], dt.Rows[i]["UOMPurchaseConversionFactor"]);
                            }

                            //var ItemCodes = dt.AsEnumerable().Select(x => x.Field<string>("numItemCode").ToString());
                            //string vcItemCodes = string.Join(",", ItemCodes);
                            dsItemPromotion = objItems.GetItemsForECommerceDynamicValues(IsAvaiablityTokenCheck, dtItem_UOMConversion);

                            var objItemList = new List<ESExtractItemField>();
                            objItemList = JsonConvert.DeserializeObject<List<ESExtractItemField>>(ResponseData);

                            if (dt.Columns.Contains("WarehouseDetails") == false)
                            {
                                dt.Columns.Add("WarehouseDetails", typeof(string));
                            }

                            for (int i = 0; i < dt.Rows.Count; i++)
                            {
                                string ItemId = dt.Rows[i]["numItemCode"].ToString();
                                if (dsItemPromotion != null && dsItemPromotion.Tables.Count > 0 && dsItemPromotion.Tables[0].Select("numItemCode = " + ItemId + "").Count() > 0)
                                {
                                    DataRow dr = dsItemPromotion.Tables[0].Select("numItemCode = " + ItemId + "").FirstOrDefault();

                                    dt.Rows[i]["numTotalPromotions"] = dr["numTotalPromotions"];
                                    dt.Rows[i]["vcPromoDesc"] = dr["vcPromoDesc"];
                                    dt.Rows[i]["bitRequireCouponCode"] = dr["bitRequireCouponCode"];
                                    dt.Rows[i]["bitInStock"] = dr["bitInStock"];
                                    dt.Rows[i]["InStock"] = dr["InStock"];
                                    dt.Rows[i]["monFirstPriceLevelPrice"] = dr["monFirstPriceLevelPrice"];
                                    dt.Rows[i]["fltFirstPriceLevelDiscount"] = dr["fltFirstPriceLevelDiscount"];
                                    dt.Rows[i]["numWareHouseItemID"] = dr["numWareHouseItemID"];
                                    dt.Rows[i]["monListPrice"] = dr["monListPrice"];
                                }
                                if (objItemList != null && objItemList.Any(x => x.numItemCode == ItemId))
                                {
                                    var obj = objItemList.FirstOrDefault(x => x.numItemCode == ItemId);
                                    dt.Rows[i]["WarehouseDetails"] = obj.WarehouseDetails;
                                    if (obj.lstJsonItemCategories != null && obj.lstJsonItemCategories.Any(x => x.numCategoryID == objItems.CategoryID))
                                    {
                                        dt.Rows[i]["vcCategoryName"] = obj.lstJsonItemCategories.FirstOrDefault(x => x.numCategoryID == objItems.CategoryID).vcCategoryName;
                                        dt.Rows[i]["CategoryDesc"] = obj.lstJsonItemCategories.FirstOrDefault(x => x.numCategoryID == objItems.CategoryID).CategoryDesc;
                                    }
                                }
                            }
                        }
                    }


                    ds.Tables.Add(dt);
                }

                ds.Tables.Add(dtCustomFields);
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterClientScriptBlock(this.Page, Page.GetType(), "ElasticSearchError", "console.log('ElasticSearchError','" + GetExceptionDetails(ex) + "');", true);
                //throw;
            }

            return ds;
        }
        public DataTable ConvertJsonToDatatableLinq(string jsonString)
        {
            JArray jsonArray = JArray.Parse(jsonString);

            foreach (JObject row in jsonArray.Children<JObject>())
            {
                foreach (JProperty column in row.Properties())
                {
                    // Only include JValue types
                    if (column.Value.Type == JTokenType.Array)
                    {
                        column.Value = JsonConvert.SerializeObject(column.Value);
                    }
                }
            }

            return JsonConvert.DeserializeObject<DataTable>(jsonArray.ToString());
        }

        public string GetExceptionDetails(Exception exception)
        {
            PropertyInfo[] properties = exception.GetType()
                                    .GetProperties();
            List<string> fields = new List<string>();
            foreach (PropertyInfo property in properties)
            {
                object value = property.GetValue(exception, null);
                fields.Add(String.Format(
                                 "{0} = {1}",
                                 property.Name,
                                 value != null ? value.ToString() : String.Empty
                ));
            }
            return String.Join("\n", fields.ToArray());
        }

        private static void ElasticSearch_SearchIntoSpecificField(CItems objItems, long domainID, DataTable dtCustomFields, IGetMappingResponse GetMappingResponse, List<string> SearchingField)
        {
            if (!string.IsNullOrWhiteSpace(objItems.SearchText))
            {
                DataSet dsSimpleSearch = new DataSet();
                var objContact = new CContacts();
                objContact.DomainID = domainID;
                objContact.FormId = 30;
                objContact.UserCntID = 0;
                objContact.ContactType = 0;
                objContact.ViewID = 0;

                dsSimpleSearch = objContact.GetColumnConfiguration();
                if (dsSimpleSearch != null && dsSimpleSearch.Tables != null && dsSimpleSearch.Tables.Count > 1)
                {
                    for (int i = 0; i < dsSimpleSearch.Tables[1].Rows.Count; i++)
                    {
                        string DbColumnName = "";
                        if (Sites.ToBool(dsSimpleSearch.Tables[1].Rows[i]["Custom"]))
                        {
                            int CustomFieldID = Sites.ToInteger(dsSimpleSearch.Tables[1].Rows[i]["FieldId"]);

                            if (dtCustomFields != null && dtCustomFields.Select("numFormFieldId = " + CustomFieldID + "").Count() > 0)
                            {
                                DbColumnName = (from DataRow dr in dtCustomFields.Rows
                                                where Sites.ToInteger(dr["numFormFieldId"]) == CustomFieldID
                                                select string.Format("{0}_{1}", Sites.ToString(dr["vcFormFieldName"]), Sites.ToString(dr["vcFieldType"]))).FirstOrDefault();
                            }
                        }
                        else
                        {
                            DbColumnName = Sites.ToString(dsSimpleSearch.Tables[1].Rows[i]["vcDbColumnName"]);
                        }

                        if (!string.IsNullOrWhiteSpace(DbColumnName))
                        {
                            if (GetMappingResponse != null && GetMappingResponse.Mapping != null && GetMappingResponse.Mapping.Properties != null && GetMappingResponse.Mapping.Properties.Any(x => x.Key.Name == DbColumnName))
                            {
                                var objProperty = GetMappingResponse.Mapping.Properties.FirstOrDefault(x => x.Key.Name == DbColumnName);
                                if (objProperty.Value.Type.Name != "text")
                                {
                                    DbColumnName += ".keyword";
                                }
                            }
                            SearchingField.Add(DbColumnName);
                        }
                    }
                }
            }
        }

        private void ElasticSearchFilterSet(CItems objItems, DataTable dtCustomFields, List<Func<QueryContainerDescriptor<object>, QueryContainer>> funcMust, bool tintDisplayCategory, bool bitAutoSelectWarehouse)
        {
            funcMust.Add(sh => sh.QueryString(x => x.Fields("IsArchieve").Query("false")));
            //if (objItems.WarehouseID != null && objItems.WarehouseID > 0)
            //{
            //    string strWarehouseID = Sites.ToString(objItems.WarehouseID);
            //    funcMust.Add(sh =>
            //        sh.QueryString(x => x.Fields("numWareHouseID").Query(strWarehouseID))
            //        || sh.QueryString(x => x.Fields("numWareHouseID").Query("0"))
            //    );
            //}
            if (objItems.ManufacturerID != null && objItems.ManufacturerID > 0)
            {
                string strManufacturerID = Sites.ToString(objItems.ManufacturerID);
                funcMust.Add(sh =>
                    sh.QueryString(x => x.Fields("numManufacturerID").Query(strManufacturerID))
                    || sh.QueryString(x => x.Fields("numManufacturerID").Query("0"))
                );
            }

            if (tintDisplayCategory)
            {
                if (objItems.CategoryID != null && objItems.CategoryID > 0)
                {
                    CItems objCategoryItems = new CItems();
                    objCategoryItems.byteMode = 13;
                    objCategoryItems.SiteID = objItems.SiteID;
                    objCategoryItems.CategoryID = objItems.CategoryID;

                    DataTable dt = null;

                    if (Session["IsDDCacheCategory"] != null && CCommon.ToBool(Session["IsDDCacheCategory"]) == true)
                    {
                        string CacheNameForCategory = string.Format("CacheSeleDelCategory_{0}_{1}_{2}", objCategoryItems.byteMode, objCategoryItems.SiteID, objCategoryItems.CategoryID);
                        if (Cache[CacheNameForCategory] == null)
                        {
                            Cache.Insert(CacheNameForCategory, objCategoryItems.SeleDelCategory());
                        }
                        dt = (DataTable)Cache[CacheNameForCategory];
                    }
                    else
                    {
                        dt = objCategoryItems.SeleDelCategory();
                    }


                    DataTable filterDT = getSubCategories(dt, objCategoryItems.CategoryID);
                    if (filterDT != null && filterDT.Rows.Count > 0)
                    {
                        List<string> lstCategories = filterDT.AsEnumerable().Select(x => Sites.ToString(x.Field<decimal>("numCategoryID"))).ToList();
                        if (lstCategories != null && lstCategories.Count() > 0)
                        {
                            funcMust.Add(sh =>
                                sh.Nested(n => n.Path("lstJsonItemCategories").Query(q =>
                                    q.Bool(b => b.Must(m => m.Terms(t => t.Field("lstJsonItemCategories.numCategoryID").Terms(lstCategories.Distinct())))))
                                )
                            );
                        }
                    }
                }
                else
                {
                    //funcMust.Add(sh => sh.QueryString(x => x.Fields("lstJsonItemCategories.numCategoryID").Query(Sites.ToString(objItems.CategoryID))));
                    funcMust.Add(sh =>
                                sh.Nested(n => n.Path("lstJsonItemCategories").Query(q =>
                                    q.Range(b => b.Field("lstJsonItemCategories.numCategoryID").GreaterThan(0))
                                ))
                            );
                }
                List<Func<QueryContainerDescriptor<object>, QueryContainer>> funcNestedMust = new List<Func<QueryContainerDescriptor<object>, QueryContainer>>();

                //HAVING 1 = (CASE WHEN (I.charItemType=''P'' AND ISNULL(I.bitKitParent,0) = 0) THEN CASE WHEN (ISNULL(SUM(numOnHand), 0) > 0 OR ISNULL(SUM(numAllocation), 0) > 0) THEN 1 ELSE 0 END ELSE 1 END)
                string warehouseid = Sites.ToString(objItems.WarehouseID);
                if (bitAutoSelectWarehouse)
                {
                    funcNestedMust.Add(sh =>
                        sh.Range(m => m.Field("lstJsonWarehouseDetails.numOnHand").GreaterThan(0)) ||
                        sh.Range(m => m.Field("lstJsonWarehouseDetails.numAllocation").GreaterThan(0))
                    );
                }
                else
                {
                    funcNestedMust.Add(sh =>
                        sh.QueryString(m => m.Fields("lstJsonWarehouseDetails.numWareHouseID").Query(warehouseid)) &&
                        (
                            sh.Range(m => m.Field("lstJsonWarehouseDetails.numOnHand").GreaterThan(0)) ||
                            sh.Range(m => m.Field("lstJsonWarehouseDetails.numAllocation").GreaterThan(0))
                        )
                    );
                }

                funcMust.Add(sh =>
                    sh.QueryString(x => x.Fields("bitKitParent").Query("true")) ||
                    !sh.QueryString(x => x.Fields("charItemType").Query("P")) ||
                    (sh.QueryString(x => x.Fields("charItemType").Query("P")) && sh.QueryString(x => x.Fields("bitKitParent").Query("false"))
                        && sh.Nested(n => n.Path("lstJsonWarehouseDetails")
                            .Query(q => q.Bool(b => b.Must(funcNestedMust))
                        )
                    )
                ));

            }
            else
            {
                if (objItems.CategoryID != null && objItems.CategoryID > 0)
                {
                    CItems objCategoryItems = new CItems();
                    objCategoryItems.byteMode = 13;
                    objCategoryItems.SiteID = objItems.SiteID;
                    objCategoryItems.CategoryID = objItems.CategoryID;

                    DataTable dt = null;

                    if (Session["IsDDCacheCategory"] != null && CCommon.ToBool(Session["IsDDCacheCategory"]) == true)
                    {
                        string CacheNameForCategory = string.Format("CacheSeleDelCategory_{0}_{1}_{2}", objCategoryItems.byteMode, objCategoryItems.SiteID, objCategoryItems.CategoryID);
                        if (Cache[CacheNameForCategory] == null)
                        {
                            Cache.Insert(CacheNameForCategory, objCategoryItems.SeleDelCategory());
                        }
                        dt = (DataTable)Cache[CacheNameForCategory];
                    }
                    else
                    {
                        dt = objCategoryItems.SeleDelCategory();
                    }

                    DataTable filterDT = getSubCategories(dt, objCategoryItems.CategoryID);
                    if (filterDT != null && filterDT.Rows.Count > 0)
                    {
                        List<string> lstCategories = filterDT.AsEnumerable().Select(x => Sites.ToString(x.Field<decimal>("numCategoryID"))).ToList();
                        if (lstCategories != null && lstCategories.Count() > 0)
                        {
                            funcMust.Add(sh =>
                                sh.Nested(n => n.Path("lstJsonItemCategories").Query(q =>
                                    q.Bool(b => b.Must(m => m.Terms(t => t.Field("lstJsonItemCategories.numCategoryID").Terms(lstCategories.Distinct())))))
                                )
                            );
                        }
                    }
                }
            }

            if (!string.IsNullOrWhiteSpace(objItems.FilterCustomWhere) && !string.IsNullOrWhiteSpace(objItems.FilterCustomFields))
            {
                string CustomFieldId = Sites.ToString(objItems.FilterCustomFields).Replace("[", "").Replace("]", "");
                if (dtCustomFields != null && dtCustomFields.Select("numFormFieldId = " + CustomFieldId + "").Count() > 0)
                {
                    string CustomFieldFullName = (from DataRow dr in dtCustomFields.Rows
                                                  where Sites.ToString(dr["numFormFieldId"]) == CustomFieldId
                                                  select string.Format("{0}_{1}", Sites.ToString(dr["vcFormFieldName"]), Sites.ToString(dr["vcFieldType"]))).FirstOrDefault();

                    string CustomFieldValue = Sites.ToString(objItems.FilterCustomWhere).Replace(string.Format("{0} IN (", objItems.FilterCustomFields), "").Replace(")", "");
                    string[] CustomFieldValues = CustomFieldValue.Split(',');
                    if (CustomFieldValues != null && CustomFieldValues.Count() > 0)
                    {
                        funcMust.Add(sh => sh.Terms(t => t.Field(CustomFieldFullName + ".keyword").Terms(CustomFieldValues)));
                    }
                }
            }
        }

        private static DataTable getSubCategories(DataTable dt, long CategoryID)
        {
            DataTable newdt = null;

            if (dt != null)
            {
                newdt = dt.Select("numCategoryID = " + CategoryID).CopyToDataTable();

                if (dt.Select("Category = " + CategoryID).Count() > 0)
                {
                    DataTable SubDT = dt.Select("Category = " + CategoryID).CopyToDataTable();
                    if (SubDT != null)
                    {
                        for (int i = 0; i < SubDT.Rows.Count; i++)
                        {
                            long subCategoryID = Sites.ToLong(SubDT.Rows[i]["numCategoryID"]);
                            DataTable SubDTResult = getSubCategories(dt, subCategoryID);
                            if (SubDTResult != null && SubDTResult.Rows.Count > 0)
                            {
                                newdt.Merge(SubDTResult, false, MissingSchemaAction.Add);
                            }

                        }
                    }
                }
            }

            return newdt;
        }

        private static void ElasticSearchSortingField(CItems objItems, IGetMappingResponse GetMappingResponse, ref string SortAsc, ref string SortDesc)
        {
            if (objItems != null && !string.IsNullOrWhiteSpace(objItems.SortByString))
            {
                string[] Sort = objItems.SortByString.ToString().Trim().Split(' ');
                if (Sort != null && Sort.Count() > 0)
                {
                    string SortFieldName = Sort[0];

                    if (GetMappingResponse != null && GetMappingResponse.Mapping != null && GetMappingResponse.Mapping.Properties != null && GetMappingResponse.Mapping.Properties.Any(x => x.Key.Name == SortFieldName))
                    {
                        var objProperty = GetMappingResponse.Mapping.Properties.FirstOrDefault(x => x.Key.Name == SortFieldName);
                        if (objProperty.Value != null && objProperty.Value.Type != null && objProperty.Value.Type.Name == "text")
                        {
                            SortFieldName += ".keyword";
                        }
                    }

                    if (Sort.Count() == 2)
                    {
                        string SortOrder = Sort[1];
                        if (SortOrder.ToLower() == "asc")
                        {
                            SortAsc = SortFieldName;
                        }
                        else
                        {
                            SortDesc = SortFieldName;
                        }
                    }
                }
            }
            else
            {//Default Sorting
                SortAsc = "vcItemName.keyword";
            }
        }

        #endregion

        protected void btnProductFiltersChanged_Click(object sender, EventArgs e)
        {
            try
            {
                AspNetPager1.CurrentPageIndex = 1;
                bindHtml();
            }
            catch (Exception ex)
            {
                ExceptionModule.ExceptionPublish(ex, Sites.ToLong(Session["DomainID"]), Sites.ToLong(Session["UserContactID"]), Sites.ToLong(Session["SiteId"]), Request);
                ShowMessage(ex.ToString(), 1);
            }
        }
    }

    public class ESExtractItemField
    {
        public string numItemCode { get; set; }
        public JArray lstJsonWarehouseDetails { get; set; }
        public List<ESItemCategoryDetails> lstJsonItemCategories { get; set; }
        public string WarehouseDetails { get { return lstJsonWarehouseDetails != null ? JsonConvert.SerializeObject(lstJsonWarehouseDetails) : ""; } }
    }
}