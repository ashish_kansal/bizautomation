﻿using BACRM.BusinessLogic.Common;
using BACRM.BusinessLogic.Item;
using BACRM.BusinessLogic.ShioppingCart;
using BACRM.BusinessLogic.Promotion;
using System;
using System.Collections;
using System.Data;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI.WebControls;

namespace BizCart.UserControls
{
    public partial class MiniCart : BizUserControl
    {

        #region Events
        
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                Sites.InitialiseSession();
                if (IsPostBack)
                {
                    if (Sites.ToString(Request["__EVENTTARGET"]) == "btnDeleteMiniCart")
                    {
                        DeleteItem();
                    }
                }
             
            }
            catch (Exception ex)
            {
                ExceptionModule.ExceptionPublish(ex, Sites.ToLong(Session["DomainID"]), Sites.ToLong(Session["UserContactID"]), Sites.ToLong(Session["SiteId"]), Request);
                ShowMessage(ex.ToString(), 1);
            }
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            try
            {
                bindHtml();
            }
            catch (Exception ex)
            {
                ExceptionModule.ExceptionPublish(ex, Sites.ToInteger(Session["DomainID"]), Sites.ToInteger(Session["UserContactID"]), Sites.ToInteger(Session["SiteId"]), Request);
                ShowMessage(ex.ToString(), 1);
            }
        }

        #endregion
        
        #region Methods

        #region HTML
        private void bindHtml()
        {
            try
            {
                string strUI = GetUserControlTemplate("MiniCart.htm");
                string pattern = "<##FOR(?<Property>(\\b[^>]*?))##>(?<Content>([\\s\\S]*?))<##ENDFOR##>";
                string RepeteTemplate = "";
                Hashtable htProperties;

                DataSet ds = new DataSet();
                ds = GetCartItem();

                DataTable dt;


                if (Regex.IsMatch(strUI, pattern))
                {
                    MatchCollection matches = Regex.Matches(strUI, pattern);
                    if ((matches.Count > 0))
                    {

                        RepeteTemplate = matches[0].Groups["Content"].Value;
                        htProperties = GetPropertiesAsHastable(matches[0].Groups["Property"].Value.Replace("(", "").Replace(")", ""));

                        StringBuilder sb = new StringBuilder();
                        //DataSet ds = new DataSet();
                        //ds = GetCartItem();

                        //DataTable dt;
                        //DataTable dtPromotions;
                        //DataTable dtShippingPromotions;

                        if (ds != null && ds.Tables.Count > 0)
                        {
                            dt = ds.Tables[0];
                            if (dt.Rows.Count > 0)
                            {
                                string strTemp = string.Empty;
                                int RowsCount = 0;
                                Button btnDelete;

                                foreach (DataRow dr in dt.Rows)
                                {
                                    strTemp = RepeteTemplate;

                                    if (!string.IsNullOrEmpty(CCommon.ToString(dr["vcPathForTImage"])))
                                    {
                                        strTemp = strTemp.Replace("##ItemImage##", GetImagePath(CCommon.ToString(dr["vcPathForTImage"])));
                                    }
                                    else
                                    {
                                        strTemp = strTemp.Replace("##ItemImage##", "../Images/DefaultProduct.png");
                                    }

                                    strTemp = strTemp.Replace("##ItemID##", CCommon.ToString(dr["numItemCode"]));
                                    strTemp = strTemp.Replace("##ItemLink##", CCommon.ToString(dr["ItemURL"]));
                                    strTemp = strTemp.Replace("##ItemName##", CCommon.ToString(dr["vcItemName"]));
                                    strTemp = strTemp.Replace("##ItemUnits##", CCommon.ToString(dr["numUnitHour"]));
                                    strTemp = strTemp.Replace("##ItemPricePerUnit##", string.Format("{0:#,##0.00}", CCommon.ToDouble(dr["monPrice"])));
                                    strTemp = strTemp.Replace("##TotalAmount##", string.Format("{0:#,##0.00}", dr["monTotAmount"]));
                                    strTemp = strTemp.Replace("##CurrencySymbol##", Sites.ToString(Session["CurrSymbol"]));
                                    strTemp = strTemp.Replace("##ItemAttributes##", CCommon.ToString(dr["vcAttributes"]));

                                    btnDelete = new Button();
                                    btnDelete.Text = "X";
                                    btnDelete.CssClass = "button";
                                    btnDelete.Attributes.Add("onclick", "return DeleteRecord('" + CCommon.ToString(dr["numCartId"]) + "');");
                                    strTemp = strTemp.Replace("##DeleteItemButton##", CCommon.RenderControl(btnDelete));


                                    if (RowsCount % 2 == 0)
                                        strTemp = strTemp.Replace("##RowClass##", Sites.ToString(htProperties["AlternatingItemClass"]));
                                    else
                                        strTemp = strTemp.Replace("##RowClass##", Sites.ToString(htProperties["ItemClass"]));

                                    sb.Append(strTemp);
                                    RowsCount++;
                                }
                                if (Sites.ToBool(Session["HidePrice"]) == false)
                                    strUI = strUI.Replace("##SubTotal##", string.Format("{0:#,##0.00}", Sites.ToDecimal(dt.Compute("SUM(monTotAmtBefDiscount)", ""))));
                                else
                                    strUI = strUI.Replace("##SubTotal##", "NA");
                            }
                            else
                            {
                                if (Sites.ToBool(Session["HidePrice"]) == false)
                                    strUI = strUI.Replace("##SubTotal##", string.Format("{0:#,##0.00}", 0.00));
                                else
                                    strUI = strUI.Replace("##SubTotal##", "NA");
                            }

                            strUI = strUI.Replace("##CurrencySymbol##", Sites.ToString(Session["CurrSymbol"]));
                            strUI = strUI.Replace("##CheckOutURL##", "/" + CheckoutPageName()); 
                            strUI = strUI.Replace("##SubTotal##", "");
                            strUI = Regex.Replace(strUI, pattern, sb.ToString());
                        }
                    }
                }

                //string strUIPromotions = GetUserControlTemplate("MiniCart.htm");
                string patternPromotions = "<##ShowPromotionSection##>(?<Content>([\\s\\S]*?))<##EndShowPromotionSection##>";
                //strUIPromotions = Sites.ReplaceMatchedPatternWithContent(strUIPromotions, patternPromotions);
                if (Regex.IsMatch(strUI, patternPromotions))
                {
                    MatchCollection matchesPromotions = Regex.Matches(strUI, patternPromotions);
                    if ((matchesPromotions.Count > 0))
                    {
                        DataTable dtPromotions;
                        DataTable dtShippingPromotions;
                        StringBuilder sbPromotions = new StringBuilder();

                        if (ds != null && ds.Tables.Count > 0)
                        {
                            dt = ds.Tables[0];
                            if (dt.Rows.Count > 0)
                            {
                                string strTempPromotions = string.Empty;

                                strTempPromotions = matchesPromotions[0].Groups["Content"].Value;

                                var distinctRows = (from DataRow dRow in dt.Rows
                                                    where CCommon.ToString(dRow["numParentItemCode"]) == "" || dRow["numParentItemCode"] == null
                                                    select new { col1 = dRow["numItemCode"], col2 = dRow["numWarehouseid"] }).Distinct();

                                int _numParentItemCode = 0;
                                long _numWarehouseId = 0;

                                if (distinctRows.Count() > 0)
                                {
                                    foreach (var row in distinctRows)
                                    {
                                        _numParentItemCode = CCommon.ToInteger(row.col1);
                                        _numWarehouseId = CCommon.ToInteger(row.col2);

                                        if (_numParentItemCode != 0 && _numWarehouseId != 0)
                                        {
                                            dtPromotions = BindPromotions(CCommon.ToInteger(_numParentItemCode), CCommon.ToLong(_numWarehouseId));
                                            if (dtPromotions != null && dtPromotions.Rows.Count > 0)
                                            {
                                                strTempPromotions = strTempPromotions.Replace("##PromotionOffer##", CCommon.ToString(dtPromotions.Rows[0]["vcPromotionDescription"]));
                                                strTempPromotions = strTempPromotions.Replace("##PromotionImage##", "/Images/Admin-Price-Control.png");
                                            }
                                        }
                                    }
                                }

                                HttpCookie cookie = Request.Cookies["Cart"];
                                cookie = GetCookie();

                                PromotionOffer objPromotionOffer = new PromotionOffer();
                                objPromotionOffer.DomainID = CCommon.ToLong(Session["DomainID"]);
                                objPromotionOffer.UserCntID = CCommon.ToLong(Session["UserContactID"]);
                                objPromotionOffer.Mode = 1;
                                objPromotionOffer.CookieId = CCommon.ToString(cookie["KeyId"]);

                                dtShippingPromotions = objPromotionOffer.GetShippingPromotions(CCommon.ToLong(Session["DefCountry"]));

                                if (dtShippingPromotions != null && dtShippingPromotions.Rows.Count > 0)
                                {
                                    strTempPromotions = strTempPromotions.Replace("##ShippingImage##", "/Images/TruckGreen.png");
                                    //strTempPromotions = strTempPromotions.Replace("##ShippingPromotions##", "You qualify for free shipping !");
                                    if (!string.IsNullOrEmpty(CCommon.ToString(dtShippingPromotions.Rows[0]["ShippingDescription"])))
                                    {
                                        strTempPromotions = strTempPromotions.Replace("##ShippingPromotions##", CCommon.ToString(dtShippingPromotions.Rows[0]["ShippingDescription"]));
                                    }
                                    else
                                    {
                                        strTempPromotions = strTempPromotions.Replace("##ShippingPromotions##", CCommon.ToString(dtShippingPromotions.Rows[0]["QualifiedShipping"]));
                                    }
                                }
                                sbPromotions.Append(strTempPromotions);
                                strUI = Regex.Replace(strUI, patternPromotions, sbPromotions.ToString());
                            }

                        }
                    }
                }

                // IF NOT REPLACED THAN REPLACE WITH EMPTY STRING
                strUI = strUI.Replace("##PromotionOffer##", "");
                strUI = strUI.Replace("##PromotionImage##", "");
                strUI = strUI.Replace("##ShippingImage##", "");
                strUI = strUI.Replace("##ShippingPromotions##", "");

                pnlCutomizeHtml.Visible = false;
                litCutomizeHtml.Text = strUI;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        #endregion

        private DataTable BindPromotions(int numItemCode, long numWarehouseItemID)
        {
            CItems objItems = new CItems();
            objItems.ItemCode = numItemCode;
            objItems.DomainID = CCommon.ToLong(Session["DomainID"]);
            objItems.WareHouseItemID = numWarehouseItemID;
            objItems.numSiteId = CCommon.ToLong(HttpContext.Current.Session["SiteID"]);
            DataTable dt = objItems.GetPromotionsForSimilarItems();
            return dt;
            //if (dt != null && dt.Rows.Count > 0)
            //{
            //    divPromotionDetailRI.Visible = true;
            //}
            //else
            //{
            //    divPromotionDetailRI.Visible = false;
            //}
        }

        private void DeleteItem()
        {
            try
            {
                BACRM.BusinessLogic.ShioppingCart.Cart objcart;
                objcart = new BACRM.BusinessLogic.ShioppingCart.Cart();
                HttpCookie cookie = Request.Cookies["Cart"];
                objcart.DomainID = CCommon.ToLong(Session["DomainID"]);
                objcart.ContactId = CCommon.ToLong(Session["UserContactID"]);
                objcart.CookieId = (cookie != null) ? cookie["KeyId"] : "";
                objcart.ItemCode = CCommon.ToLong(Request["__EVENTARGUMENT"]);
                objcart.bitDeleteAll = false;
                objcart.SiteID = Sites.ToLong(Session["SiteId"]);
                objcart.RemoveItemFromCart();
                Session["gblItemCount"] = null;
                Session["gblTotalAmount"] = null;
                return;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        
        #endregion
    }

}