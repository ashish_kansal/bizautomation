﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BACRM.BusinessLogic.ShioppingCart;
using BACRM.BusinessLogic.Common;
using System.Collections;
using BACRM.BusinessLogic.Opportunities;
using System.Data;
using System.Text;
using System.Text.RegularExpressions;

namespace BizCart.UserControls
{
    public partial class AddPayment : BizUserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                Sites.InitialiseSession();
                if (!IsPostBack)
                {
                    if (!System.Web.HttpContext.Current.User.Identity.IsAuthenticated || CCommon.ToLong(Session["DomainID"]) == 0 || CCommon.ToLong(Session["UserContactID"]) == 0)
                    {
                        Response.Redirect("Login.aspx?ReturnURL=" + (Sites.ToString(Session["SiteLiveURL"]).Trim().Length > 2 ? "https://" : "http://") + Request.Url.Host + "/AddPayment.aspx", true);
                    }
                    if (Request.QueryString["flag"] != null && Request.QueryString["PaymentId"] != null && CCommon.ToString(Request.QueryString["flag"]) == "edit")
                    {
                        btnSubmit.Text = "Update";
                    }
                    if (Request.QueryString["flag"] != null && Request.QueryString["PaymentId"] != null && CCommon.ToString(Request.QueryString["flag"]) == "delete")
                    {
                        DeleteCustomerCreditData();
                    }
                    BindCardType();
                    ArrayList Years = new ArrayList();
                    for (int i = 1; i <= 20; i++)
                    {
                        Years.Add(DateTime.Now.AddYears(i - 1).Date.ToString("yy"));
                    }
                    ddlCardExpYear.DataSource = Years;
                    ddlCardExpYear.DataBind();
                    ddlCardExpYear.Items.FindByValue(DateTime.Now.Year.ToString("yy"));

                    if (Request.QueryString["flag"] != null && Request.QueryString["PaymentId"] != null && CCommon.ToString(Request.QueryString["flag"]) == "edit")
                    {
                        BindCardsData();
                    }
                    bindHtml();
                }
                //if (IsPostBack)
                //{
                //    if (Sites.ToString(Request["__EVENTTARGET"]) == "chkDefault")
                //    {
                //        chkDefault.Checked = Sites.ToBool(Request["__EVENTARGUMENT"]);
                //        bindHtml();
                //    }
                //}
            }
            catch (Exception ex)
            {
                ExceptionModule.ExceptionPublish(ex, Sites.ToLong(Session["DomainID"]), Sites.ToLong(Session["UserContactID"]), Sites.ToLong(Session["SiteId"]), Request);
                ShowMessage(ex.ToString(), 1);
            }
        }
        private void bindHtml()
        {
            try
            {
                QueryStringValues objEncryption = new QueryStringValues();
                string strUI = GetUserControlTemplate("AddPayment.htm");
                string pattern = "<##FOR(?<Property>(\\b[^>]*?))##>(?<Content>([\\s\\S]*?))<##ENDFOR##>";
                string CssClass = "", AltCssClass = "", RepeteTemplate = "";
                DataTable dt = GetCardData();
                DataTable dtCardData = new DataTable();
                dtCardData.Columns.Add("CardNumber", typeof(string));
                dtCardData.Columns.Add("numCCInfoID", typeof(string));
                foreach (DataRow dr in dt.Rows)
                {
                    string strCCNo = objEncryption.Decrypt(CCommon.ToString(dr["vcCreditCardNo"]));
                    if (strCCNo.Length > 4)
                    {
                        strCCNo = "************" + strCCNo.Substring(strCCNo.Length - 4, 4);
                    }
                    else
                    {
                        strCCNo = "";
                    }
                    dtCardData.Rows.Add(strCCNo, CCommon.ToString(dr["numCCInfoID"]));
                }
                if (Regex.IsMatch(strUI, pattern))
                {
                    MatchCollection matches = Regex.Matches(strUI, pattern);
                    if ((matches.Count > 0))
                    {
                        string[] Property = matches[0].Groups["Property"].Value.Replace("(", "").Replace(")", "").Split(';');
                        RepeteTemplate = matches[0].Groups["Content"].Value;



                        if (Property.Length > 0)
                        {
                            for (int i = 0; i < Property.Length; i++)
                            {
                                string[] PropertyValue = Property[i].Split('=');

                                if (PropertyValue.Length == 2)
                                {
                                    switch (PropertyValue[0])
                                    {
                                        case "ItemClass":
                                            CssClass = PropertyValue[1].Trim();
                                            break;
                                        case "AlternatingItemClass":
                                            AltCssClass = PropertyValue[1].Trim();
                                            break;
                                    }
                                }
                            }
                        }


                        StringBuilder sb = new StringBuilder();

                        if (dt.Rows.Count > 0)
                        {
                            string strTemp = string.Empty;
                            int RowsCount = 0;
                            foreach (DataRow dr in dt.Rows)
                            {
                                strTemp = RepeteTemplate;
                                strTemp = strTemp.Replace("##CardName##", objEncryption.Decrypt(CCommon.ToString(dr["vcCardHolder"])));
                                string strCCNo = objEncryption.Decrypt(CCommon.ToString(dr["vcCreditCardNo"]));
                                if (strCCNo.Length > 4)
                                {
                                    strCCNo = "************" + strCCNo.Substring(strCCNo.Length - 4, 4);
                                }
                                else
                                {
                                    strCCNo = "";
                                }

                                strTemp = strTemp.Replace("##CardNumber##", strCCNo);
                                strTemp = strTemp.Replace("##CardType##", CCommon.ToString(dr["vcData"]));
                                strTemp = strTemp.Replace("##ExpDate##", CCommon.ToString(dr["tintValidMonth"]) + "/" + CCommon.ToString(dr["intValidYear"]));
                                strTemp = strTemp.Replace(" ##CVV2##", objEncryption.Decrypt(CCommon.ToString(dr["vcCVV2"])));
                                strTemp = strTemp.Replace(" ##btnEdit##", "<a href=/Addpayment.aspx?flag=edit&PaymentId=" + CCommon.ToString(dr["numCCInfoID"]) + ">Edit</a>");
                                strTemp = strTemp.Replace(" ##btnDelete##", "<a onclick='DeleteRecord(" + CCommon.ToString(dr["numCCInfoID"]) + ")'>Delete</a>");
                                if (RowsCount % 2 == 0)
                                    strTemp = strTemp.Replace("##RowClass##", AltCssClass);
                                else
                                    strTemp = strTemp.Replace("##RowClass##", CssClass);

                                sb.Append(strTemp);
                                
                                RowsCount++;
                            }
                        }
                        strUI = Regex.Replace(strUI, pattern, sb.ToString());

                    }
                }
                try
                {
                    if (ddlCardProfiles != null)
                    {
                        ddlCardProfiles.DataSource = dtCardData;
                        ddlCardProfiles.DataTextField = "CardNumber";
                        ddlCardProfiles.DataValueField = "numCCInfoID";
                        ddlCardProfiles.DataBind();
                        ddlCardProfiles.Items.Insert(0, "--Select One--");
                        if (Request.QueryString["flag"] != null && Request.QueryString["PaymentId"] != null && CCommon.ToString(Request.QueryString["flag"]) == "edit")
                        {
                            if (ddlCardProfiles.Items.FindByValue(Request.QueryString["PaymentId"]) != null)
                            {
                                ddlCardProfiles.Items.FindByValue(Request.QueryString["PaymentId"]).Selected = true;
                            }
                        }
                        else
                        {
                            ddlCardProfiles.Items.FindByText("--Select One--").Value = "0";
                        }
                        strUI = strUI.Replace("##CardProfiles##", CCommon.RenderControl(ddlCardProfiles));
                    }
                    if (btnDelete != null)
                    {
                        strUI = strUI.Replace("##DeleteButton##", CCommon.RenderControl(btnDelete));
                    }
                }
                catch
                {

                }
                strUI = strUI.Replace("##CardHolderNameTextBox##", CCommon.RenderControl(txtCardHolderName));
                strUI = strUI.Replace("##CardNumberTextBox##", CCommon.RenderControl(txtCardNumber));
                strUI = strUI.Replace("##CardTypeDropDownList##", CCommon.RenderControl(ddlCardType));
                strUI = strUI.Replace("##CardExpMonthDropDownList##", CCommon.RenderControl(ddlCardExpMonth));
                strUI = strUI.Replace("##CardExpYearDropDownList##", CCommon.RenderControl(ddlCardExpYear));
                strUI = strUI.Replace("##CVV2TextBox##", CCommon.RenderControl(txtCardCVV2));
                strUI = strUI.Replace("##SetDefaultCard##", CCommon.RenderControl(chkDefault));
                strUI = strUI.Replace("##SubmitButton##", CCommon.RenderControl(btnSubmit));
                pnlCutomizeHtml.Visible = false;
                litCutomizeHtml.Text = strUI;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void DeleteCustomerCreditData()
        {
            try
            {
                OppInvoice objoppinvoice = new OppInvoice();
                objoppinvoice.numCCInfoID = CCommon.ToLong(Request.QueryString["PaymentId"]);
                objoppinvoice.DeleteCustomerCreditCardInfo();
                Response.Redirect("/AddPayment.aspx");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        protected void bsubmit_Click(object sender, EventArgs e)
        {
            try
            {
                //validate if card length and number
                if (CCommon.ToString(System.Configuration.ConfigurationManager.AppSettings["IsDebugMode"]).ToLower() != "true")
                {
                    BACRM.BusinessLogic.Admin.PaymentGateway objPG = new BACRM.BusinessLogic.Admin.PaymentGateway();
                    string strMessage = objPG.ValidateCard(txtCardNumber.Text.Trim(), ddlCardExpMonth.SelectedValue, ddlCardExpYear.SelectedValue);
                    if (strMessage.Length > 0)
                    {
                        ShowMessage(strMessage, 1);
                        return;
                    }
                }


                QueryStringValues objEncryption = new QueryStringValues();
                OppInvoice objoppinvoice = new OppInvoice();
                objoppinvoice.ContactID = CCommon.ToLong(Session["UserContactID"]);
                objoppinvoice.CardHolder = objEncryption.Encrypt(txtCardHolderName.Text);
                objoppinvoice.CreditCardNumber = objEncryption.Encrypt(txtCardNumber.Text);
                objoppinvoice.CVV2 = objEncryption.Encrypt(txtCardCVV2.Text);
                objoppinvoice.CardTypeID = CCommon.ToLong(ddlCardType.SelectedValue);
                objoppinvoice.ValidMonth = short.Parse(ddlCardExpMonth.SelectedValue);
                objoppinvoice.ValidYear = short.Parse(ddlCardExpYear.SelectedValue);
                objoppinvoice.UserCntID = CCommon.ToLong(Session["UserContactID"]);
                objoppinvoice.numCCInfoID = CCommon.ToLong(Request.QueryString["PaymentId"]);
                objoppinvoice.IsDefault = chkDefault.Checked;
                objoppinvoice.ManageCustomerCreditCardInfo();

                //bindHtml();
                Response.Redirect("/AddPayment.aspx");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private DataTable GetCardData()
        {
            try
            {
                DataSet ds = new DataSet();
                OppInvoice objoppinvoice = new OppInvoice();
                objoppinvoice.bitflag = true;
                objoppinvoice.UserCntID = CCommon.ToLong(Session["UserContactID"]);
                objoppinvoice.DomainID = CCommon.ToInteger(Session["DomainID"]);
                ds = objoppinvoice.GetCustomerCreditCardInfo();
                return ds.Tables[0];
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private void BindCardType()
        {
            try
            {
                CCommon objCommon = new CCommon();
                objCommon.sb_FillComboFromDB(ref ddlCardType, 120, Sites.ToLong(Session["DomainID"]));
                //CCommon objCommon = new CCommon();
                //objCommon.DomainID = Sites.ToLong(Session["DomainID"]);
                //objCommon.tinyOrder = 0;
                //ddlCardType.DataTextField = "vcData";
                //ddlCardType.DataValueField = "numListItemID";
                //ddlCardType.DataSource = objCommon.GetCardTypes();
                //ddlCardType.DataBind();
                //ddlCardType.Items.Insert(0, "--Select One--");
                //ddlCardType.Items.FindByText("--Select One--").Value = "0";
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void BindCardsData()
        {
            try
            {
                QueryStringValues objEncryption = new QueryStringValues();
                OppInvoice objoppinvoice = new OppInvoice();
                if (Request.QueryString["flag"] != null && Request.QueryString["PaymentId"] != null && CCommon.ToString(Request.QueryString["flag"]) == "edit")
                {
                    objoppinvoice.bitflag = true;
                    objoppinvoice.DomainID = CCommon.ToInteger(Session["DomainId"]);
                    objoppinvoice.numCCInfoID = CCommon.ToLong(Request.QueryString["PaymentId"]);
                    DataSet ds = objoppinvoice.GetCustomerCreditCardInfo();
                    if (ds != null && ds.Tables.Count > 0)
                    {
                        txtCardHolderName.Text = objEncryption.Decrypt(CCommon.ToString(ds.Tables[0].Rows[0]["vcCardHolder"]));
                        txtCardNumber.Text = objEncryption.Decrypt(CCommon.ToString(ds.Tables[0].Rows[0]["vcCreditCardNo"]));
                        txtCardCVV2.Text = objEncryption.Decrypt(CCommon.ToString(ds.Tables[0].Rows[0]["vcCVV2"]));
                        ddlCardType.ClearSelection();
                        if (ddlCardType.Items.FindByValue(CCommon.ToString(ds.Tables[0].Rows[0]["numCardTypeId"])) != null)
                        {
                            ddlCardType.Items.FindByValue(CCommon.ToString(ds.Tables[0].Rows[0]["numCardTypeId"])).Selected = true;
                        }
                        ddlCardExpYear.ClearSelection();
                        ddlCardExpMonth.ClearSelection();
                        if (ddlCardExpMonth.Items.FindByValue(CCommon.ToString(ds.Tables[0].Rows[0]["tintValidMonth"]).PadLeft(2, '0')) != null)
                        {
                            ddlCardExpMonth.Items.FindByValue(CCommon.ToString(ds.Tables[0].Rows[0]["tintValidMonth"]).PadLeft(2, '0')).Selected = true;
                        }
                        if (ddlCardExpYear.Items.FindByValue(CCommon.ToString(ds.Tables[0].Rows[0]["intValidYear"])) != null)
                        {
                            ddlCardExpYear.Items.FindByValue(CCommon.ToString(ds.Tables[0].Rows[0]["intValidYear"])).Selected = true;
                        }
                        chkDefault.Checked = CCommon.ToBool(ds.Tables[0].Rows[0]["bitIsDefault"]);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        protected void ddlCardProfiles_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (Sites.ToLong(ddlCardProfiles.SelectedValue) > 0)
                {
                    Response.Redirect("/AddPayment.aspx?flag=edit&PaymentId=" + ddlCardProfiles.SelectedValue);
                }
                else
                {
                    Response.Redirect("/AddPayment.aspx");
                }
                bindHtml();
            }
            catch (Exception ex)
            {

            }
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            if (Sites.ToLong(ddlCardProfiles.SelectedValue) > 0)
            {
                Response.Redirect("/AddPayment.aspx?flag=delete&PaymentId=" + ddlCardProfiles.SelectedValue);
            }
            else
            {
                Response.Redirect("/AddPayment.aspx");
            }
            bindHtml();
        }
    }
}