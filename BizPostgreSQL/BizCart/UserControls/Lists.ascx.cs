﻿using System;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BACRM.BusinessLogic.Common;
using BACRM.BusinessLogic.ShioppingCart;
using BACRM.BusinessLogic.Opportunities;
using BACRM.BusinessLogic.Item;
using System.Text;
using System.Text.RegularExpressions;
using System.Collections;
namespace BizCart.UserControls
{
    public partial class Lists : BizUserControl
    {
        public string ListLabel { get; set; }
        //public string Title { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                Sites.InitialiseSession();
                if (!System.Web.HttpContext.Current.User.Identity.IsAuthenticated || CCommon.ToLong(Session["DomainID"]) == 0 || CCommon.ToLong(Session["UserContactID"]) == 0)
                {
                    Response.Redirect("Login.aspx?ReturnURL=" + Request.Url.Segments[Request.Url.Segments.Length - 1], true);
                }

                if (!IsPostBack)
                {
                    if (Sites.ToString(Request["action"]) == "add" || Sites.ToString(Request["action"]) == "addnew")
                        AddNew();
                    else if (Sites.ToString(Request["action"]) == "edit")
                        LoadList(Sites.ToLong(Request["id"]));
                    else if (Sites.ToString(Request["action"]) == "del")
                        DeleteList();
                    else if (Sites.ToString(Request["action"]) == "addtocart")
                        AddToCart(Sites.ToLong(Request["id"]), Sites.ToLong(Request["Itemid"]), false);
                    else
                        BindList();
                    bindListDropDown();

                    bindHtml();

                }
            }
            catch (Exception ex)
            {
                ExceptionModule.ExceptionPublish(ex, Sites.ToLong(Session["DomainID"]), Sites.ToLong(Session["UserContactID"]), Sites.ToLong(Session["SiteId"]), Request);
                ShowMessage(ex.ToString(), 1);
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtListName.Text.Trim().Length > 0)
                {
                    COpportunities objOpp = new COpportunities();
                    objOpp.DomainID = Sites.ToLong(Session["DomainID"]);
                    objOpp.DivisionID = Sites.ToInteger(Session["DivId"]);
                    objOpp.SalesTemplateID = Sites.ToLong(hdnSalesTemplateID.Value);
                    objOpp.TemplateName = txtListName.Text;
                    objOpp.TemplateType = 0;//company specific
                    objOpp.OpportID = 0;
                    objOpp.UserCntID = Sites.ToInteger(Session["UserContactID"]);
                    if (objOpp.ManageSalesTemplate())
                    {
                        if (Sites.ToString(Request["action"]) == "addnew")
                            Response.Redirect("addtolist.aspx");
                        string strFinalList = GetErrorMessage("ERR043").Replace("##WishList##", ListLabel);//##Wishlist##  saved sucessfully
                        ShowMessage(strFinalList, 0);
                        BindList();
                        bindHtml();
                    }
                }
                else
                {
                    ShowMessage("Please enter " + ListLabel.ToLower() + " name", 1);
                }

            }
            catch (Exception ex)
            {
                ExceptionModule.ExceptionPublish(ex, Sites.ToLong(Session["DomainID"]), Sites.ToLong(Session["UserContactID"]), Sites.ToLong(Session["SiteId"]), Request);
                ShowMessage(ex.ToString(), 1);
            }
        }

        private void DeleteList()
        {
            try
            {
                COpportunities objOpp = new COpportunities();
                objOpp.SalesTemplateID = Sites.ToLong(Request["id"]);
                objOpp.SalesTemplateItemID = Sites.ToLong(Request["Itemid"]);
                objOpp.DomainID = Sites.ToLong(Session["DomainID"]);
                objOpp.DeleteSalesTemplateItem();
                BindList();

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void LoadList(long lngSalesTemplateID)
        {
            try
            {
                COpportunities objOpp = new COpportunities();
                DataTable dtTemplate;
                objOpp.DomainID = Sites.ToLong(Session["DomainID"]);
                objOpp.SalesTemplateID = lngSalesTemplateID;
                objOpp.OpportID = 0;
                objOpp.byteMode = 2;
                dtTemplate = objOpp.GetSalesTemplate();
                if (dtTemplate.Rows.Count > 0)
                {
                    hdnSalesTemplateID.Value = dtTemplate.Rows[0]["numSalesTemplateID"].ToString();
                    txtListName.Text = dtTemplate.Rows[0]["vcTemplateName"].ToString();
                }
                btnSave.Text = "Update";
                hdnMode.Value = "Add";
            }
            catch (Exception ex)
            {
                ExceptionModule.ExceptionPublish(ex, Sites.ToLong(Session["DomainID"]), Sites.ToLong(Session["UserContactID"]), Sites.ToLong(Session["SiteId"]), Request);
                ShowMessage(ex.ToString(), 1);
            }
        }


        private void BindList()
        {
            try
            {

                hdnMode.Value = "View";
                pnlNew.Visible = false;
                pnlView.Visible = true;
            }
            catch (Exception ex)
            {
                ExceptionModule.ExceptionPublish(ex, Sites.ToLong(Session["DomainID"]), Sites.ToLong(Session["UserContactID"]), Sites.ToLong(Session["SiteId"]), Request);
                ShowMessage(ex.ToString(), 1);
            }
        }

        protected void lnkAddNew_Click(object sender, EventArgs e)
        {
            try
            {
                AddNew();
            }
            catch (Exception ex)
            {
                ExceptionModule.ExceptionPublish(ex, Sites.ToLong(Session["DomainID"]), Sites.ToLong(Session["UserContactID"]), Sites.ToLong(Session["SiteId"]), Request);
                ShowMessage(ex.ToString(), 1);
            }
        }

        private void AddNew()
        {
            try
            {
                hdnMode.Value = "Add";
                pnlNew.Visible = true;
                pnlView.Visible = false;
                btnSave.Text = "Save";
                hdnSalesTemplateID.Value = "0";
                txtListName.Text = "";
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public void ItemAddtoWishList(long ItemId)
        {
            DataTable dtItemDetails = default(DataTable);
            DataTable dtItemColumn = default(DataTable);

            DataSet ds = default(DataSet);
            DataTable dtTable = default(DataTable);
            CItems objItems = new CItems();
            objItems.ItemCode = Sites.ToInteger(ItemId);
            objItems.WarehouseID = Sites.ToLong(Session["WareHouseID"]);
            objItems.DomainID = Sites.ToLong(Session["DomainID"]);
            objItems.SiteID = Sites.ToLong(Session["SiteId"]);
            HttpCookie cookie = Request.Cookies["Cart"];
            if (Request.Cookies["Cart"] != null)
            {
                objItems.vcCookieId = (cookie["KeyId"] != null) ? cookie["KeyId"] : "";
            }
            else
            {
                objItems.vcCookieId = "";
            }
            objItems.UserCntID = Sites.ToLong(Session["UserContactID"]);
            ds = objItems.ItemDetailsForEcomm();
            //dtresult

        }


        private void AddToCart(long lngSalesTemplateID, long ItemCode = 0, bool IsItemtoList = false)
        {
            try
            {
                if (!IsItemtoList)
                {
                    Tuple<bool,string> objResult = AddToCartFromEcomm("", ItemCode, 0, "", false, 1);
                    bool result = objResult.Item1;

                    if (result == true)
                    {
                        bindHtml();
                        Response.Redirect("cart.aspx", false);
                    }
                    else
                    {
                        bindHtml();
                        Session["ShippingMethodConfirmed"] = "False";//Reset confirmation.. so user have to choose shipping method again.
                        Session["SelectedStep"] = 0;
                    }

                }
                else
                {
                    COpportunities objOpp = new COpportunities();
                    DataTable dtTemplate;
                    objOpp.DomainID = Sites.ToLong(Session["DomainID"]);
                    objOpp.SalesTemplateID = lngSalesTemplateID;
                    objOpp.ItemCode = ItemCode;
                    dtTemplate = objOpp.GetSalesTemplateItemsByItemId();
                    if (dtTemplate.Rows.Count > 0)
                    {
                        DataSet dsTemp = GetCartItem(); //(DataSet)Session["Data"];
                        DataTable dtItem = default(DataTable);
                        dtItem = dsTemp.Tables[0];
                        DataRow dr = default(DataRow);
                        HttpCookie cookie = Request.Cookies["Cart"];
                        if (cookie == null)
                        {
                            SetCookie(cookie);
                        }
                        //Add items to table and store it to session
                        foreach (DataRow drow in dtTemplate.Rows)
                        {
                            dr = dtItem.NewRow();
                            dr["numOppItemCode"] = (dtItem.Compute("MAX(numOppItemCode)", "") == System.DBNull.Value ? 0 : Sites.ToInteger(dtItem.Compute("MAX(numOppItemCode)", "").ToString())) + 1;
                            dr["numItemCode"] = drow["numItemCode"];
                            dr["numUnitHour"] = drow["numUnitHour"];
                            dr["numWeight"] = drow["Weight"];
                            dr["monPrice"] = drow["monPrice"];
                            dr["bitDiscountType"] = drow["bitDiscountType"];
                            dr["fltDiscount"] = drow["fltDiscount"];
                            dr["monTotAmtBefDiscount"] = drow["monTotAmtBefDiscount"];
                            dr["monTotAmount"] = drow["monTotAmount"];
                            dr["vcItemDesc"] = drow["vcItemDesc"];
                            dr["numWarehouseID"] = Sites.ToLong(Session["WareHouseID"]);
                            dr["vcItemName"] = drow["vcItemName"];
                            dr["numWarehouseItmsID"] = drow["numWarehouseItmsID"];
                            dr["vcAttributes"] = drow["Attributes"];
                            dr["vcAttrValues"] = drow["AttrValues"];
                            dr["bitFreeShipping"] = drow["FreeShipping"];
                            dr["vcItemType"] = drow["ItemType"];
                            dr["tintOpFlag"] = drow["Op_Flag"];
                            dr["ItemURL"] = drow["ItemURL"];
                            dr["numUOM"] = drow["numUOM"];
                            dr["vcUOMName"] = drow["vcUOMName"];
                            dr["decUOMConversionFactor"] = drow["UOMConversionFactor"];
                            foreach (DataRow item in dtItem.Rows)
                            {
                                if (Sites.ToString(item["numItemCode"]) == Sites.ToString(dr["numItemCode"]) && Sites.ToString(item["vcAttrValues"]) == Sites.ToString(dr["vcAttrValues"]))
                                {
                                    item["numUnitHour"] = CCommon.ToInteger(item["numUnitHour"]) + 1;
                                    item["monTotAmount"] = Sites.ToDouble(item["numUnitHour"]) * Sites.ToDouble(dr["monPrice"]);
                                    item["monTotAmtBefDiscount"] = Sites.ToDouble(item["numUnitHour"]) * Sites.ToDouble(item["monPrice"]);
                                    dsTemp.AcceptChanges();
                                    UpdateCart(dsTemp);
                                }
                            }
                            if (!IsItemtoList)
                            {

                            }
                        }
                        if (!IsItemtoList)
                        {
                            AddToCartFromEcomm("", ItemCode, 0, "", false, 1);
                            Session["ShippingMethodConfirmed"] = "False";//Reset confirmation.. so user have to choose shipping method again.
                            Session["SelectedStep"] = 0;
                            Response.Redirect("cart.aspx", false);
                        }
                        else
                        {
                            DataTable dtLists = new DataTable();
                            dtLists = dtItem.Clone();
                            DataRow dr1 = dtLists.NewRow();
                            dr1.ItemArray = dr.ItemArray;
                            dtLists.Rows.Add(dr1);
                            Session["ListData"] = dtLists;
                            DataSet ds = new DataSet();
                            ds.Tables.Add(((DataTable)Session["ListData"]).Copy());

                            objOpp = new COpportunities();
                            objOpp.DomainID = Sites.ToLong(Session["DomainID"]);
                            objOpp.SalesTemplateID = Sites.ToLong(ddlWishListName.SelectedValue);
                            objOpp.strItemDtls = ds.GetXml();
                            objOpp.ManageSalesTemplateItems();
                            Session["ListData"] = null;
                            bindHtml();
                        }
                    }
                    else
                    {
                        ShowMessage(GetErrorMessage("ERR045"), 1);//No items found in list!
                        hdnMode.Value = "View";
                        bindHtml();
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionModule.ExceptionPublish(ex, Sites.ToLong(Session["DomainID"]), Sites.ToLong(Session["UserContactID"]), Sites.ToLong(Session["SiteId"]), Request);
                ShowMessage(ex.ToString(), 1);
            }
        }
        private void SetCookie(HttpCookie cookie)
        {
            Guid g;
            g = Guid.NewGuid();
            cookie = new HttpCookie("Cart");
            cookie["KeyId"] = CCommon.ToString(g);//CCommon.ToString(rand.Next());
            cookie.Expires = DateTime.Now.AddDays(50);
            Response.Cookies.Add(cookie);
        }

        private void UpdateCart(DataSet ds)
        {
            BACRM.BusinessLogic.ShioppingCart.Cart objcart;
            objcart = new BACRM.BusinessLogic.ShioppingCart.Cart();
            HttpCookie cookie = Request.Cookies["Cart"];

            try
            {
                ds.Tables[0].TableName = "Table1";
                objcart.ContactId = CCommon.ToInteger(Session["UserContactID"]);
                objcart.DomainID = CCommon.ToInteger(Session["DomainID"]);
                objcart.CookieId = (cookie != null) ? cookie["KeyId"] : "";
                objcart.strXML = GetCartItemsXML(ds);
                objcart.SiteID = Sites.ToLong(Session["SiteId"]);
                if (Session["PreferredItemPromotions"] != null)
                {
                    objcart.PreferredPromotion = ((DataSet)Session["PreferredItemPromotions"]).GetXml();
                }
                objcart.UpdateCartItem();

            }
            catch (Exception ex)
            {
                ExceptionModule.ExceptionPublish(ex, Sites.ToLong(Session["DomainID"]), Sites.ToLong(Session["UserContactID"]), Sites.ToLong(Session["SiteId"]), Request);
                ShowMessage(ex.ToString(), 1);
                return;
            }
            finally
            {
                objcart = null;
            }

        }
        void bindHtml()
        {
            try
            {
                string strUI = GetUserControlTemplate("Lists.htm");

                string AddNewPattern, ViewListPattern;
                AddNewPattern = "{##NewListSection##}(?<Content>([\\s\\S]*?)){/##NewListSection##}";
                ViewListPattern = "{##ViewListSection##}(?<Content>([\\s\\S]*?)){/##ViewListSection##}";
                if (hdnMode.Value == "Add")
                {
                    //Hide Other Template
                    strUI = Sites.RemoveMatchedPattern(strUI, ViewListPattern);
                    //Replace disply template ##Tags## with content value
                    strUI = Sites.ReplaceMatchedPatternWithContent(strUI, AddNewPattern);

                    strUI = strUI.Replace("##ListNameTextBox##", CCommon.RenderControl(txtListName));
                    strUI = strUI.Replace("##SaveButton##", CCommon.RenderControl(btnSave));

                }
                else if (hdnMode.Value == "View")
                {
                    strUI = Sites.RemoveMatchedPattern(strUI, AddNewPattern);
                    strUI = Sites.ReplaceMatchedPatternWithContent(strUI, ViewListPattern);


                    //Credit Term Replacement
                    string SummaryPattern = "{##SummaryTable##}(?<Content>([\\s\\S]*?)){/##SummaryTable##}";
                    CCommon objCommon = new CCommon();
                    CItems objItems = new CItems();
                    objItems.DivisionID = Sites.ToLong(Session["DivId"]);
                    DataTable dtTable = default(DataTable);
                    string lblCreditLimit = "";
                    dtTable = objItems.GetAmountDue();
                    lblBalDue.Text = Sites.ToString(Session["CurrSymbol"]) + " " + string.Format("{0:#,##0.00}", dtTable.Rows[0]["AmountDueSO"]);
                    decimal RemainIngCredit = Sites.ToDecimal(dtTable.Rows[0]["RemainingCredit"]);
                    if (RemainIngCredit < 0)
                    {
                        RemainIngCredit = 0;
                    }
                    lblRemCredit.Text = Sites.ToString(Session["CurrSymbol"]) + " " + string.Format("{0:#,##0.00}", RemainIngCredit);
                    lblAmtPastDue.Text = Sites.ToString(Session["CurrSymbol"]) + " " + string.Format("{0:#,##0.00}", dtTable.Rows[0]["AmountPastDuePO"]);
                    lblCreditLimit = Sites.ToString(Session["CurrSymbol"]) + " " + string.Format("{0:#,##0.00}", dtTable.Rows[0]["CreditLimit"]);
                    strUI = Sites.ReplaceMatchedPatternWithContent(strUI, SummaryPattern);

                    strUI = strUI.Replace("##TotalBalanceDueLabel##", lblBalDue.Text);
                    strUI = strUI.Replace("##TotalRemainingCreditLabel##", lblRemCredit.Text);
                    strUI = strUI.Replace("##TotalAmountPastDueLabel##", lblAmtPastDue.Text);
                    strUI = strUI.Replace("##CreditLimit##", lblCreditLimit);
                    strUI = strUI.Replace("##ARStatement##", "");

                    strUI = BindTopDashboard(strUI);

                    DataTable dtWishlist;

                    StringBuilder sb = new StringBuilder();
                    string pattern = "<##FOR(?<Property>(\\b[^>]*?))##>(?<Content>([\\s\\S]*?))<##ENDFOR##>";
                    if (Regex.IsMatch(strUI, pattern))
                    {
                        MatchCollection matches = Regex.Matches(strUI, pattern);
                        if ((matches.Count > 0))
                        {
                            Hashtable htProperties = GetPropertiesAsHastable(matches[0].Groups["Property"].Value.Replace("(", "").Replace(")", ""));

                            string Template;
                            int RowsCount = 0;
                            COpportunities objOpp = new COpportunities();
                            objOpp.DomainID = Sites.ToLong(Session["DomainID"]);
                            objOpp.DivisionID = Sites.ToInteger(Session["DivId"]);
                            objOpp.SalesTemplateID = 0;
                            objOpp.OpportID = 0;
                            objOpp.byteMode = 1;
                            objOpp.SalesTemplateID = Sites.ToInteger(ddlWishListName.SelectedValue);
                            dtWishlist = objOpp.GetSalesTemplateItems();
                            strUI = strUI.Replace("##WishListDropDown##", CCommon.RenderControl(ddlWishListName));
                            strUI = strUI.Replace("##DeleteWishList##", CCommon.RenderControl(lnkDelete));
                            strUI = strUI.Replace("##addItemtoList##", CCommon.RenderControl(btnAddItemtoList));
                            foreach (DataRow dr in dtWishlist.Rows)
                            {
                                Template = matches[0].Groups["Content"].Value;

                                //Template = Template.Replace("##WishListLink##", "/addtolist.aspx?id=" + Sites.ToString(dr["numSalesTemplateID"]));
                                //Template = Template.Replace("##WishListName##", dr["vcTemplateName"].ToString());
                                //Template = Template.Replace("##NoOfItems##", dr["ItemCount"].ToString());
                                Template = Template.Replace("##ItemName##", dr["vcItemName"].ToString());
                                Template = Template.Replace("##ItemDescription##", dr["vcItemDesc"].ToString());
                                Template = Template.Replace("##AddToCartLink##", "/lists.aspx?action=addtocart&id=" + Sites.ToString(dr["numSalesTemplateID"]) + "&Itemid=" + Sites.ToString(dr["numItemCode"]));

                                //Template = Template.Replace("##EditWishListLink##", "/lists.aspx?action=edit&id=" + Sites.ToString(dr["numSalesTemplateID"]));
                                Template = Template.Replace("##DeleteWishListLink##", "/lists.aspx?action=del&id=" + Sites.ToString(dr["numSalesTemplateID"]) + "&Itemid=" + Sites.ToString(dr["numSalesTemplateItemID"]));

                                if (RowsCount % 2 == 0)
                                    Template = Template.Replace("##RowClass##", Sites.ToString(htProperties["AlternatingItemClass"]));
                                else
                                    Template = Template.Replace("##RowClass##", Sites.ToString(htProperties["ItemClass"]));

                                sb.Append(Template);
                                RowsCount++;
                            }
                            strUI = Regex.Replace(strUI, pattern, sb.ToString());

                        }
                    }

                }
                pnlCutomizeHtml.Visible = false;
                litCutomizeHtml.Text = strUI;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private string BindTopDashboard(string strUI)
        {
            StringBuilder sbTopMenu = new StringBuilder();
            if (Sites.ToBool(Session["bitSalesOrderTabs"]) == true)
            {
                sbTopMenu.Append("<li class=\"SalesOrderTabs\"><a id=\"SalesOrderTabs\" href=\"Orders.aspx?flag=true\">" + Convert.ToString(Session["vcSalesOrderTabs"]) + "</a></li>");
            }
            if (Sites.ToBool(Session["bitSalesQuotesTabs"]) == true)
            {
                sbTopMenu.Append("<li class=\"SalesOpportunityTabs\"><a id=\"SalesOpportunityTabs\" href=\"SalesOpportunity.aspx\">" + Convert.ToString(Session["vcSalesQuotesTabs"]) + "</a></li>");
            }
            if (Sites.ToBool(Session["bitItemPurchaseHistoryTabs"]) == true)
            {
                sbTopMenu.Append("<li class=\"ItemPurchasedTabs\"><a id=\"ItemPurchasedTabs\" href=\"ItemPurchasedHistory.aspx\">" + Convert.ToString(Session["vcItemPurchaseHistoryTabs"]) + "</a></li>");
            }
            if (Sites.ToBool(Session["bitItemsFrequentlyPurchasedTabs"]) == true)
            {
                sbTopMenu.Append("<li class=\"WishListsTabs active\"><a id=\"WishListsTabs\" href=\"Lists.aspx\">" + Convert.ToString(Session["vcItemsFrequentlyPurchasedTabs"]) + "</a></li>");
            }
            if (Sites.ToBool(Session["bitOpenCasesTabs"]) == true)
            {
                sbTopMenu.Append("<li class=\"OpenCasesTabs\"><a id=\"OpenCasesTabs\" href=\"OpenCases.aspx\">" + Convert.ToString(Session["vcOpenCasesTabs"]) + "</a></li>");
            }
            if (Sites.ToBool(Session["bitOpenRMATabs"]) == true)
            {
                sbTopMenu.Append("<li class=\"OpenRMATabs\"><a id=\"OpenRMATabs\" href=\"OpenRMA.aspx\">" + Convert.ToString(Session["vcOpenRMATabs"]) + "</a></li>");
            }
            if (Sites.ToBool(Session["bitSupportTabs"]) == true)
            {
                sbTopMenu.Append("<li class=\"SupportTabs\"><a id=\"SupportTabs\" href=\"support.aspx\">" + Convert.ToString(Session["vcSupportTabs"]) + "</a></li>");
            }
            strUI = strUI.Replace("##TopMenuDasboard##", Convert.ToString(sbTopMenu));
            return strUI;
        }
        public void bindListDropDown()
        {
            COpportunities objOpp = new COpportunities();
            DataTable dtWishlist = new DataTable();
            objOpp.DomainID = Sites.ToLong(Session["DomainID"]);
            objOpp.DivisionID = Sites.ToInteger(Session["DivId"]);
            objOpp.SalesTemplateID = 0;
            objOpp.OpportID = 0;
            objOpp.byteMode = 1;
            objOpp.SiteID = Sites.ToLong(Session["SiteID"]);
            dtWishlist = objOpp.GetSalesTemplate();
            ddlWishListName.DataSource = dtWishlist;
            ddlWishListName.DataTextField = "vcTemplateName";
            ddlWishListName.DataValueField = "numSalesTemplateID";
            ddlWishListName.DataBind();
        }

        protected void ddlWishListName_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                bindHtml();
            }
            catch
            {

            }
        }

        protected void btnAddItemtoList_Click(object sender, EventArgs e)
        {
            AddToCart(0, Sites.ToLong(hdnAutoSuggestItemId.Value), true);
        }

        protected void lnkDelete_Click(object sender, EventArgs e)
        {
            try
            {
                if (Sites.ToLong(ddlWishListName.SelectedValue) > 0)
                {
                    COpportunities objOpp = new COpportunities();
                    objOpp.SalesTemplateID = Sites.ToLong(ddlWishListName.SelectedValue);
                    objOpp.DomainID = Sites.ToLong(Session["DomainID"]);
                    objOpp.DeleteSalesTemplate();

                    bindListDropDown();
                    bindHtml();
                }
            }
            catch (Exception ex)
            {
                ExceptionModule.ExceptionPublish(ex, Sites.ToLong(Session["DomainID"]), Sites.ToLong(Session["UserContactID"]), Sites.ToLong(Session["SiteId"]), Request);
                ShowMessage("Error ocurred while deleting wish list!!", 1);
            }
        }
    }
}