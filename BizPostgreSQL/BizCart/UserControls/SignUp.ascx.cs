﻿using System;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BACRM.BusinessLogic.Alerts;
using BACRM.BusinessLogic.Documents;
using BACRM.BusinessLogic.Common;
using BACRM.BusinessLogic.Prospects;
using BACRM.BusinessLogic.Contacts;
using BACRM.BusinessLogic.Leads;
using BACRM.BusinessLogic.Admin;
using BACRM.BusinessLogic.ShioppingCart;
using BACRM.BusinessLogic;
using System.Configuration;

namespace BizCart.UserControls
{
    public partial class SignUp : BizUserControl
    {
        #region  Global Declaration

        long numDomainId = 0;
        bool IsPasswordFieldAddedToForm = false;
        DataTable dtGenericFormConfig = default(DataTable);
        DataTable dtCustFld = default(DataTable);
        //public string Title { get; set; }
        public string RedirectAfterSigningUp { get; set; }

        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                DataTable dtTable = default(DataTable);
                UserAccess objUserAccess = new UserAccess();
                Sites.InitialiseSession();
                hplLogin.NavigateUrl = Sites.ToString(Session["SitePath"]) + "/Login.aspx";
                numDomainId = Sites.ToLong(Session["DomainID"]);
                //if (Title.Length > 0) lblTitle.Text = Title;
                //Put user code to initialize the page here
                if (!IsPostBack)
                {
                    //The form loads for the first time
                    //ViewState("referrer") = Request.UrlReferrer.AbsoluteUri
                    DataTable dtGenericFormHeaderConfig = default(DataTable);
                    //declare a datatable
                    FormGenericFormContents objConfigWizard = new FormGenericFormContents();
                    //Create an object of class which encaptulates the functionaltiy
                    objConfigWizard.FormID = 4;
                    //Set the property of formId
                    objConfigWizard.DomainID = numDomainId;
                    //Set the Domain Id
                    dtGenericFormHeaderConfig = objConfigWizard.getFormHeaderDetails();
                    //call to get the form header details
                    if (dtGenericFormHeaderConfig.Rows.Count == 0)
                    {
                        //if the form is not registered in the database
                        btnSubmit.Enabled = false;
                        //Disable any attempt to save/ submit
                        //Exit the flow
                        return;
                    }
                    ImplementLeadsAutoRoutingRules objLeadBoxData = new ImplementLeadsAutoRoutingRules();
                    //Create an object of class which encaptulates the functionaltiy
                    objLeadBoxData.DomainID = numDomainId;
                    //set the domain id
                    int numRoutId = objLeadBoxData.checkForDefaultAutoRule();
                    //Get the Default Rule Id
                    if (numRoutId == 0)
                    {
                        //if the xml for form fields has not been configured
                        btnSubmit.Enabled = false;
                        //Disable any attempt to save/ submit
                        plhFormControls.Controls.Add(new LiteralControl("Auto Rules are not created, please contact the Administrator."));
                        //calls to create the form controls and add it to the form
                        //and exit the flow
                        return;
                    }
                    hdGroupId.Value = dtGenericFormHeaderConfig.Rows[0]["numGrpId"].ToString();
                    //Store the Group Id
                    //Store the Company type/ relationship
                    hdCompanyType.Value = dtGenericFormHeaderConfig.Rows[0]["vcAdditionalParam"].ToString();
                    try
                    {

                        objUserAccess.DomainID = numDomainId;
                        dtTable = objUserAccess.GetDomainDetails();
                        hdnSmartyStreetsAPIKeys.Value = Sites.ToString(dtTable.Rows[0]["vcSmartyStreetsAPIKeys"]);
                        if (Convert.ToBoolean(dtTable.Rows[0]["bitEnableSmartyStreets"])==true)
                        {
                            hdnIsSmartyStreetsEnabled.Value = "1";
                        }
                        else
                        {
                            hdnIsSmartyStreetsEnabled.Value = "0";
                        }
                    }
                    catch
                    {

                    }

                }

                try
                {
                   
                    objUserAccess.DomainID = numDomainId;
                    dtTable = objUserAccess.GetDomainDetails();
                    hdnSmartyStreetsAPIKeys.Value = Sites.ToString(dtTable.Rows[0]["vcSmartyStreetsAPIKeys"]);
                    if (Convert.ToBoolean(dtTable.Rows[0]["bitEnableSmartyStreets"]) == true)
                    {
                        hdnIsSmartyStreetsEnabled.Value = "1";
                    }
                    else
                    {
                        hdnIsSmartyStreetsEnabled.Value = "0";
                    }
                }
                catch
                {

                }
                bindHtml();
            }
            catch (Exception ex)
            {
                ExceptionModule.ExceptionPublish(ex, Sites.ToLong(Session["DomainID"]), Sites.ToLong(Session["UserContactID"]), Sites.ToLong(Session["SiteId"]), Request);
                ShowMessage(ex.ToString(), 1);
            }
        }
        /// -----------------------------------------------------------------------------
        /// <summary>
        ///     This method is used to handle saving of the configuration after the 
        ///     "Submit" button is clicked
        /// </summary>
        /// <param name="sender">Represents the sender object.</param>
        /// <param name="e">Represents the EventArgs.</param>
        /// <remarks>
        /// </remarks>
        /// <history>
        /// 	[Debasish Tapan Nag]	09/20/2005	Created
        /// </history>
        /// -----------------------------------------------------------------------------
        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            Page.Validate();
            if (!Page.IsValid)
            {
                Page.ClientScript.RegisterClientScriptBlock(Page.GetType(), "Script", "alert('Please enter required fields!!')", true);
                return;
            }
            CLeads objLeads = new CLeads();
            CCommon objCommon = new CCommon();
            long lnDivID = 0;
            long lngCMPID = 0;
            long lnCntID = 0;
            DataRow dRow = default(DataRow);
            DataSet ds = new DataSet();
            try
            {
                objLeads.GroupID = 1;
                objLeads.ContactType = 70;
                objLeads.PrimaryContact = true;
                objLeads.DivisionName = "-";
                objLeads.FirstName = txtFirstName.Text;
                objLeads.LastName = txtLastName.Text;
                objLeads.Email = txtEmail.Text;
                objLeads.ComPhone = txtOrganizationPhone.Text;
                objLeads.PStreet = txtContactStreet.Text;
                objLeads.PCity = txtContactCity.Text;
                objLeads.PPostalCode = txtContactPostalCode.Text;
                objLeads.ContactPhone = txtContactPhone.Text;
                objLeads.PhoneExt = txtContactPhoneExt.Text;
                objLeads.WebSite = txtOrganizationWebsite.Text;
                objLeads.Cell = txtContactCellPhone.Text;
                objLeads.HomePhone = txtContactHomePhone.Text;
                objLeads.Comments = txtOrganizationComments.Text;
                objLeads.CompanyName = txtOrganizationName.Text;
                objLeads.Fax = txtContactFax.Text;

                objLeads.ComFax = txtOrganizationFax.Text;
                objLeads.Street = txtBillStreet.Text;
                objLeads.City = txtBillCity.Text;
                objLeads.PostalCode = txtBillPostCode.Text;

                objLeads.Category = Sites.ToLong(ddlContactCategory.SelectedValue);
                objLeads.PState = Sites.ToLong(ddlContactState.SelectedValue);
                objLeads.PCountry = Sites.ToLong(ddlContactCountry.SelectedValue);
                objLeads.Position = Sites.ToLong(ddlConatctPosition.SelectedValue);
                objLeads.Team = Sites.ToLong(ddlContactTeam.SelectedValue);
                objLeads.AnnualRevenue = Sites.ToLong(ddlOrganizationAnnualRevenue.SelectedValue);
                objLeads.NumOfEmp = Sites.ToLong(ddlNoOfEmployees.SelectedValue);
                objLeads.InfoSource = Sites.ToLong(ddlInfoSource.SelectedValue);
                objLeads.Profile = Sites.ToLong(ddlOrganizationProfile.SelectedValue);
                objLeads.EmpStatus = Sites.ToLong(ddlContactStatus.SelectedValue);
                objLeads.CompanyIndustry = Sites.ToLong(ddlIndustry.SelectedValue);
                objLeads.CompanyType = Sites.ToLong(ddlRelationship.SelectedValue);
                objLeads.CompanyRating = Sites.ToLong(ddlOrganizationRating.SelectedValue);
                objLeads.StatusID = Sites.ToLong(ddlOrganizationStatus.SelectedValue);
                objLeads.TerritoryID = Sites.ToLong(ddlTerritory.SelectedValue);
                objLeads.CampaignID = Sites.ToLong(ddlOrganizationCampaign.SelectedValue);
                objLeads.CompanyCredit = Sites.ToLong(ddlOrganizationCreditLimit.SelectedValue);
                objLeads.Department = Sites.ToLong(ddlDepartmentName.SelectedValue);
                objLeads.FollowUpStatus = Sites.ToLong(ddlFollowUpStatus.SelectedValue);
                objLeads.State = Sites.ToLong(ddlBillState.SelectedValue);
                objLeads.Country = Sites.ToLong(ddlBillCountry.SelectedValue);
                objLeads.OptOut = !(chkbitOptOut.Checked);

                if (chkShippingAddresssameasBillingAddress.Checked)
                {
                    objLeads.SStreet = txtBillStreet.Text;
                    objLeads.SCity = txtBillCity.Text;
                    objLeads.SPostalCode = txtBillPostCode.Text;
                    objLeads.SState = Sites.ToLong(ddlBillState.SelectedValue);
                    objLeads.SCountry = Sites.ToLong(ddlBillCountry.SelectedValue);
                }
                else
                {
                    objLeads.SStreet = txtShipToStreet.Text;
                    objLeads.SCity = txtShipToCity.Text;
                    objLeads.SPostalCode = txtShiptoPostalCode.Text;
                    objLeads.SState = Sites.ToLong(ddlShipState.SelectedValue);
                    objLeads.SCountry = Sites.ToLong(ddlShipCountry.SelectedValue);
                }

                objLeads.Password = txtPassword.Text.Trim();
                objLeads.PasswordConfirm = txtPasswordConfirm.Text.Trim();

                objLeads.DomainID = Sites.ToLong(Session["DomainID"]);
                if (objLeads.Email.Length > 2)
                {
                    objLeads.GetConIDCompIDDivIDFromEmail();
                    if (objLeads.ContactID > 0 && objLeads.DivisionID > 0)
                    {
                        ShowMessage(GetErrorMessage("ERR051") + " <a href=\"#\" onclick=\"return RetrievePassword();\">Click Here</a> to retrieve the password from your email address", 1);//The email address already exists. Your options are to change the email address, or contact someone at the store you are buying from to resolve this for you
                        return;
                    }
                }
                // Password Validation
                if (IsPasswordFieldAddedToForm == true)
                {
                    if (Sites.ToString(objLeads.Password).Length < 8 || Sites.ToString(objLeads.PasswordConfirm).Length < 8)
                    {
                        ShowMessage(GetErrorMessage("ERR052"), 1);
                        return;
                    }

                    System.Text.RegularExpressions.Regex passwordPattern = new System.Text.RegularExpressions.Regex("^(?=.*[A-Z])(?=.*[!@#$&*])(?=.*[0-9])(?=.*[a-z]).{8,}$");
                    if (!passwordPattern.IsMatch(objLeads.Password.Trim()))
                    {
                        ShowMessage(GetErrorMessage("ERR082"), 1);
                        return;
                    }

                    if (objLeads.Password != objLeads.PasswordConfirm)
                    {
                        ShowMessage(GetErrorMessage("ERR053"), 1);
                        return;
                    }
                }
                else if (!string.IsNullOrEmpty(txtFuturePassword.Text))
                {
                    if (Sites.ToString(txtFuturePassword.Text.Trim()).Length < 8)
                    {
                        ShowMessage(GetErrorMessage("ERR052"), 1);//Length of password must be greater than 6 characters
                        return;
                    }
                }

                AutoRoutingRules objAutoRoutRles = new AutoRoutingRules();
                objAutoRoutRles.DomainID = Sites.ToLong(Session["DomainID"]);
                dtGenericFormConfig.TableName = "Table";
                ds.Tables.Add(dtGenericFormConfig.Copy());
                objAutoRoutRles.strValues = ds.GetXml();
                ds.Tables.Remove(ds.Tables[0]);

                objLeads.UserCntID = objAutoRoutRles.GetRecordOwner();
                objLeads.CRMType = 1;
                objLeads.CompanyType = Sites.ToLong(Session["DefaultRelationship"]); //Sites.ToLong(hdCompanyType.Value);
                objLeads.Profile = Sites.ToLong(Session["DefaultProfile"]);//Sites.ToLong(hdGroupId.Value);
                objLeads.AccountClass = Sites.ToLong(Session["DefaultClass"]);
                lngCMPID = objLeads.CreateRecordCompanyInfo();
                objLeads.CompanyID = lngCMPID;
                lnDivID = objLeads.CreateRecordDivisionsInfo();
                objLeads.DivisionID = lnDivID;

                lnCntID = objLeads.CreateRecordAddContactInfo();
                Session["CompID"] = lngCMPID;
                //Set the Company Id in a session
                Session["DivId"] = lnDivID;
                Session["UserContactID"] = lnCntID;
                Session["FirstName"] = txtFirstName.Text.Trim();
                Session["LastName"] = txtLastName.Text.Trim();
                //'Saving CustomFields
                DataView dsViews = new DataView(dtCustFld);
                dsViews.RowFilter = "vcFieldType='C'";
                int i = 0;
                DataTable dtCusTable = new DataTable();
                dtCusTable.Columns.Add("FldDTLID");
                dtCusTable.Columns.Add("fld_id");
                dtCusTable.Columns.Add("Value");
                string strdetails = null;

                if (dsViews.Count > 0)
                {
                    for (i = 0; i <= dsViews.Count - 1; i++)
                    {
                        if (plhFormControls.FindControl(dsViews[i]["vcFormFieldName"].ToString().Trim().Replace(" ", "") + "_" + dsViews[i]["vcFieldType"].ToString()) != null)
                        {
                            dRow = dtCusTable.NewRow();
                            dRow["FldDTLID"] = 0;
                            dRow["fld_id"] = dsViews[i]["numFormFieldId"].ToString().Replace("C", "");
                            dRow["Value"] = GetCustFldValue(dsViews[i]["vcAssociatedControlType"].ToString(), dsViews[i]["vcFormFieldName"].ToString().Trim().Replace(" ", "") + "_" + dsViews[i]["vcFieldType"].ToString());
                            dtCusTable.Rows.Add(dRow);
                        }
                    }
                    dtCusTable.TableName = "Table";
                    ds.Tables.Add(dtCusTable.Copy());
                    strdetails = ds.GetXml();
                    ds.Tables.Remove(ds.Tables[0]);

                    CustomFields ObjCusfld = new CustomFields();
                    ObjCusfld.strDetails = strdetails;
                    ObjCusfld.RecordId = lnCntID;
                    ObjCusfld.locId = 4;
                    ObjCusfld.SaveCustomFldsByRecId();
                }

                dsViews.RowFilter = "vcFieldType='D'";
                dtCusTable.Rows.Clear();
                if (dsViews.Count > 0)
                {
                    for (i = 0; i <= dsViews.Count - 1; i++)
                    {
                        if (plhFormControls.FindControl(dsViews[i]["vcFormFieldName"].ToString().Trim().Replace(" ", "") + "_" + dsViews[i]["vcFieldType"].ToString()) != null)
                        {
                            dRow = dtCusTable.NewRow();
                            dRow["FldDTLID"] = 0;
                            dRow["fld_id"] = dsViews[i]["numFormFieldId"].ToString().Replace("C", "");
                            dRow["Value"] = GetCustFldValue(dsViews[i]["vcAssociatedControlType"].ToString(), dsViews[i]["vcFormFieldName"].ToString().Trim().Replace(" ", "") + "_" + dsViews[i]["vcFieldType"].ToString());
                            dtCusTable.Rows.Add(dRow);
                        }
                    }
                    dtCusTable.TableName = "Table";
                    ds.Tables.Add(dtCusTable.Copy());
                    strdetails = ds.GetXml();
                    ds.Tables.Remove(ds.Tables[0]);

                    CustomFields ObjCusfld = new CustomFields();
                    ObjCusfld.strDetails = strdetails;
                    ObjCusfld.RecordId = lnDivID;
                    ObjCusfld.locId = 1;
                    ObjCusfld.SaveCustomFldsByRecId();
                }

                try
                {
                    CAlerts objAlerts = new CAlerts();
                    DataTable dtDetails = default(DataTable);
                    objAlerts.AlertDTLID = 6;
                    //Alert DTL ID for sending alerts in opportunities
                    objAlerts.DomainID = Sites.ToLong(Session["DomainID"]);
                    dtDetails = objAlerts.GetIndAlertDTL();
                    if (dtDetails.Rows.Count > 0)
                    {
                        if (dtDetails.Rows[0]["tintAlertOn"].ToString() == "1")
                        {
                            DataTable dtEmailTemplate = default(DataTable);
                            DocumentList objDocuments = new DocumentList();
                            objDocuments.GenDocID = Sites.ToLong(dtDetails.Rows[0]["numEmailTemplate"]);
                            objDocuments.DomainID = Sites.ToLong(Session["DomainID"]);
                            dtEmailTemplate = objDocuments.GetDocByGenDocID();
                            if (dtEmailTemplate.Rows.Count > 0)
                            {
                                CProspects objProspects = new CProspects();
                                DataTable dtComInfo = default(DataTable);
                                objProspects.DivisionID = lnDivID;
                                objProspects.DomainID = Sites.ToLong(Session["DomainID"]);
                                objProspects.ClientTimeZoneOffset = Sites.ToInteger(Session["ClientMachineUTCTimeOffset"]);
                                dtComInfo = objProspects.GetCompanyInfoForEdit();

                                Email objSendMail = new Email();
                                DataTable dtMergeFields = new DataTable();
                                DataRow drNew = default(DataRow);
                                dtMergeFields.Columns.Add("NoofEmp");
                                dtMergeFields.Columns.Add("Organization");
                                dtMergeFields.Columns.Add("Assignee");
                                drNew = dtMergeFields.NewRow();
                                drNew["NoofEmp"] = dtComInfo.Rows[0]["NoofEmp"];
                                drNew["Organization"] = dtComInfo.Rows[0]["vcCompanyName"];
                                drNew["Assignee"] = dtComInfo.Rows[0]["vcCreatedBy"];
                                dtMergeFields.Rows.Add(drNew);
                                objCommon.byteMode = 1;
                                objCommon.UserCntID = objLeads.UserCntID;
                                objSendMail.SendEmail(dtEmailTemplate.Rows[0]["vcSubject"].ToString(), dtEmailTemplate.Rows[0]["vcDocdesc"].ToString(), (dtDetails.Rows[0]["tintCCManager"].ToString() == "1" ? objCommon.GetManagerEmail() : ""), Sites.ToString(Session["DomainAdminEmail"]), objCommon.GetContactsEmail(), ref dtMergeFields, "", "", 0);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    ExceptionModule.ExceptionPublish(ex, Sites.ToLong(Session["DomainID"]), Sites.ToLong(Session["UserContactID"]), Sites.ToLong(Session["SiteId"]), Request);
                }
                bool CookieEnabled = false;
                CookieEnabled = Context.Request.Browser.Cookies;
                if (CookieEnabled == true)
                {
                    CCommon objcommon = new CCommon();
                    HttpCookie cookie = default(HttpCookie);
                    cookie = new HttpCookie("companyid", lnDivID.ToString());
                    cookie.Expires = DateTime.Now.AddYears(1);
                    Context.Response.Cookies.Set(cookie);

                    cookie = new HttpCookie("usercontactid", lnCntID.ToString());
                    cookie.Expires = DateTime.Now.AddYears(1);
                    Context.Response.Cookies.Set(cookie);
                }
                Session["CompID"] = lngCMPID;
                //Set the Company Id in a session
                Session["DivId"] = lnDivID;
                //Set the Contact Id in a session
                Session["UserContactID"] = lnCntID;

                if (!IsPasswordFieldAddedToForm)
                {
                    if (!string.IsNullOrEmpty(txtFuturePassword.Text))
                    {
                        if (objCommon == null)
                        {
                            objCommon = new CCommon();
                        }

                        objLeads.Password = objCommon.Encrypt(txtFuturePassword.Text.Trim());
                    }
                    else
                    {
                        if (objCommon == null)
                        {
                            objCommon = new CCommon();
                        }

                        objLeads.Password = objCommon.Encrypt(PasswordGenerator.Generate(10, 10));
                    }
                }

                UserGroups objUserGroups = new UserGroups();
                objUserGroups.ContactID = lnCntID;
                objUserGroups.DomainID = numDomainId;
                objUserGroups.Password = objCommon.Encrypt(objLeads.Password);
                objUserGroups.AccessAllowed = 1;
                objUserGroups.CreateExtranetAccount();

                if (CCommon.ToBool(Session["EmailStatus"]) == true)
                {
                    if (!string.IsNullOrEmpty(objLeads.Email))
                    {
                        try
                        {
                            //We are explicity setting it true so mails will be sent through SMTP details provided in Domain Details
                            Session["SMTPServerIntegration"] = "True";

                            Email objSendEmail = new Email();

                            objSendEmail.DomainID = Sites.ToLong(Session["DomainID"]);
                            objSendEmail.TemplateCode = "#SYS#ECOMMERCE_SIGNUP";
                            objSendEmail.ModuleID = 1;
                            objSendEmail.RecordIds = Sites.ToString(lnCntID);
                            objSendEmail.AdditionalMergeFields.Add("PortalPassword", objLeads.Password);
                            objSendEmail.FromEmail = Sites.ToString(Session["DomainAdminEmail"]);
                            objSendEmail.ToEmail = "##ContactFirstName## <##ContactEmail##>";
                            objSendEmail.SendEmailTemplate();

                        }
                        catch (Exception ex)
                        {
                            throw ex;
                        }
                    }
                }
                //Set all default session after signup as it throws error in invoice page when DateFormat is not found in session
                UserAccess objUserAccess = new UserAccess();
                DataTable dttable = default(DataTable);
                objUserAccess.Email = objLeads.Email;
                objUserAccess.Password = objCommon.Encrypt(objLeads.Password);
                objUserAccess.DomainID = Sites.ToLong(Session["DomainID"]);
                dttable = objUserAccess.ExtranetLogin();
                if (dttable.Rows[0][0].ToString() == "1")
                {
					System.Web.Security.FormsAuthenticationTicket tkt = new System.Web.Security.FormsAuthenticationTicket(1, CCommon.ToString(dttable.Rows[0][13]), DateTime.Now, DateTime.Now.AddMinutes(30), false, "");
                    string cookiestr = System.Web.Security.FormsAuthentication.Encrypt(tkt);
                    HttpCookie ck = new HttpCookie(System.Web.Security.FormsAuthentication.FormsCookieName, cookiestr);
                    //ck.Expires = tkt.Expiration;
                    ck.Path = System.Web.Security.FormsAuthentication.FormsCookiePath;
                    Response.Cookies.Add(ck);

                    Session["CompID"] = dttable.Rows[0][3];
                    Session["UserContactID"] = dttable.Rows[0][1];
                    Session["ContactId"] = dttable.Rows[0][1];
                    Session["DivId"] = dttable.Rows[0][2];
                    Session["GroupId"] = dttable.Rows[0][4];
                    Session["CRMType"] = dttable.Rows[0][5];
                    Session["DomainID"] = dttable.Rows[0][6];
                    Session["CompName"] = dttable.Rows[0][7];
                    Session["RecOwner"] = dttable.Rows[0][8];
                    Session["RecOwnerName"] = dttable.Rows[0][9];
                    Session["PartnerGroup"] = dttable.Rows[0][10];
                    Session["RelationShip"] = dttable.Rows[0][11];
                    //Session["HomePage"] = dttable.Rows[0][12];
                    Session["UserEmail"] = dttable.Rows[0][13];
                    Session["DateFormat"] = dttable.Rows[0]["vcDateFormat"];
                    Session["ContactName"] = dttable.Rows[0][14];
                    Session["PartnerAccess"] = dttable.Rows[0][15];
                    Session["PagingRows"] = dttable.Rows[0]["tintCustomPagingRows"];
                    Session["DefCountry"] = dttable.Rows[0]["numDefCountry"];
                    Session["PopulateUserCriteria"] = dttable.Rows[0]["tintAssignToCriteria"];
                    Session["EnableIntMedPage"] = (bool.Parse(dttable.Rows[0]["bitIntmedPage"].ToString()) == true ? 1 : 0);
                    Session["DomainName"] = dttable.Rows[0]["vcDomainName"];
                    //' End date
                    Session["Internal"] = 1;
                    Session["Username"] = objUserAccess.Email;

                    Session["Password"] = objCommon.Encrypt(objUserAccess.Password);
                    Sites objSite = new Sites();
                    objSite.UpdateDefaultWarehouseForLoggedInUser(Sites.ToLong(Session["DivId"]));
                }

                if (Request.Cookies["Cart"] != null)
                {
                    HttpCookie cookie = Request.Cookies["Cart"];
                    objCommon.Mode = 17;
                    objCommon.Comments = CCommon.ToString((cookie["KeyId"] != null) ? cookie["KeyId"] : "");
                    objCommon.UpdateValueID = CCommon.ToLong(Session["UserContactID"]);
                    objCommon.UpdateSingleFieldValue();
                }

                HttpCookie cartCookie = Request.Cookies["Cart"];
                BACRM.BusinessLogic.ShioppingCart.Cart objcart;
                objcart = new BACRM.BusinessLogic.ShioppingCart.Cart();
                objcart.DomainID = CCommon.ToLong(Session["DomainID"]);
                objcart.ContactId = CCommon.ToLong(Session["UserContactID"]);
                objcart.CookieId = CCommon.ToString((cartCookie != null) ? cartCookie["KeyId"] : "");
                objcart.ItemCode = 0;
                objcart.SiteID = Sites.ToLong(Session["SiteId"]);
                objcart.RemoveItemFromCart();

                if (Sites.ToString(RedirectAfterSigningUp).Length > 2)
                    Response.Redirect(Sites.ToString(RedirectAfterSigningUp), false);
                else
                    Response.Redirect((Sites.ToString(Session["SiteLiveURL"]).Trim().Length > 2 ? "https://" : "http://") + Request.Url.Host + "/" + CheckoutPageName(), false);

            }
            catch (Exception ex)
            {
                ExceptionModule.ExceptionPublish(ex, Sites.ToLong(Session["DomainID"]), Sites.ToLong(Session["UserContactID"]), Sites.ToLong(Session["SiteId"]), Request);
                ShowMessage(ex.ToString(), 1);
            }
        }

        #endregion

        #region Methods

        #region HTML

        private void bindHtml()
        {
            try
            {
                string strUI = GetUserControlTemplate("SignUp.htm");

                dtGenericFormConfig = new DataTable();
                dtGenericFormConfig.Columns.Add("vcDbColumnName");
                dtGenericFormConfig.Columns.Add("vcDbColumnText");


                if (strUI.Contains("##FirstName##")) { AddToTable("vcFirstName", txtFirstName.Text.Trim()); strUI = strUI.Replace("##FirstName##", CCommon.RenderControl(txtFirstName)); }
                if (strUI.Contains("##LastName##")) { AddToTable("vcLastName", txtLastName.Text.Trim()); strUI = strUI.Replace("##LastName##", CCommon.RenderControl(txtLastName)); }
                if (strUI.Contains("##ContactEmail##")) { AddToTable("vcEmail", txtEmail.Text.Trim()); strUI = strUI.Replace("##ContactEmail##", CCommon.RenderControl(txtEmail)); }
                if (strUI.Contains("##BillToCity##")) { AddToTable("vcBillCity", txtBillCity.Text.Trim()); strUI = strUI.Replace("##BillToCity##", CCommon.RenderControl(txtBillCity)); }
                if (strUI.Contains("##BilltoPostalCode##")) { AddToTable("vcBillPostCode", txtBillPostCode.Text.Trim()); strUI = strUI.Replace("##BilltoPostalCode##", CCommon.RenderControl(txtBillPostCode)); }
                if (strUI.Contains("##BillToStreet##")) { AddToTable("vcBillStreet", txtBillStreet.Text.Trim()); strUI = strUI.Replace("##BillToStreet##", CCommon.RenderControl(txtBillStreet)); }
                if (strUI.Contains("##ContactCellPhone##")) { AddToTable("numCell", txtContactCellPhone.Text.Trim()); strUI = strUI.Replace("##ContactCellPhone##", CCommon.RenderControl(txtContactCellPhone)); }
                if (strUI.Contains("##ContactCity##")) { AddToTable("vcPCity", txtContactCity.Text.Trim()); strUI = strUI.Replace("##ContactCity##", CCommon.RenderControl(txtContactCity)); }
                if (strUI.Contains("##ContactFax##")) { AddToTable("vcFax", txtContactFax.Text.Trim()); strUI = strUI.Replace("##ContactFax##", CCommon.RenderControl(txtContactFax)); }
                if (strUI.Contains("##ContactHomePhone##")) { AddToTable("numHomePhone", txtContactHomePhone.Text.Trim()); strUI = strUI.Replace("##ContactHomePhone##", CCommon.RenderControl(txtContactHomePhone)); }
                if (strUI.Contains("##ContactPhone##")) { AddToTable("numPhone", txtContactPhone.Text.Trim()); strUI = strUI.Replace("##ContactPhone##", CCommon.RenderControl(txtContactPhone)); }
                if (strUI.Contains("##ContactPhoneExt##")) { AddToTable("numPhoneExtension", txtContactPhoneExt.Text.Trim()); strUI = strUI.Replace("##ContactPhoneExt##", CCommon.RenderControl(txtContactPhoneExt)); }
                if (strUI.Contains("##ContactPostalCode##")) { AddToTable("vcPPostalCode", txtContactPostalCode.Text.Trim()); strUI = strUI.Replace("##ContactPostalCode##", CCommon.RenderControl(txtContactPostalCode)); }
                if (strUI.Contains("##ContactStreet##")) { AddToTable("vcPStreet", txtContactStreet.Text.Trim()); strUI = strUI.Replace("##ContactStreet##", CCommon.RenderControl(txtContactStreet)); }
                if (strUI.Contains("##OrganizationFax##")) { AddToTable("vcComFax", txtOrganizationFax.Text.Trim()); strUI = strUI.Replace("##OrganizationFax##", CCommon.RenderControl(txtOrganizationFax)); }
                if (strUI.Contains("##OrganizationName##")) { AddToTable("vcCompanyName", txtOrganizationName.Text.Trim()); strUI = strUI.Replace("##OrganizationName##", CCommon.RenderControl(txtOrganizationName)); }
                if (strUI.Contains("##OrganizationPhone##")) { AddToTable("vcComPhone", txtOrganizationPhone.Text.Trim()); strUI = strUI.Replace("##OrganizationPhone##", CCommon.RenderControl(txtOrganizationPhone)); }
                if (strUI.Contains("##OrganizationWebsite##")) { AddToTable("vcWebSite", txtOrganizationWebsite.Text.Trim()); strUI = strUI.Replace("##OrganizationWebsite##", CCommon.RenderControl(txtOrganizationWebsite)); }
                if (strUI.Contains("##ShipToCity##")) { AddToTable("vcShipCity", txtShipToCity.Text.Trim()); strUI = strUI.Replace("##ShipToCity##", CCommon.RenderControl(txtShipToCity)); }
                if (strUI.Contains("##ShiptoPostalCode##")) { AddToTable("vcShipPostCode", txtShiptoPostalCode.Text.Trim()); strUI = strUI.Replace("##ShiptoPostalCode##", CCommon.RenderControl(txtShiptoPostalCode)); }
                if (strUI.Contains("##ShipToStreet##")) { AddToTable("vcShipStreet", txtShipToStreet.Text.Trim()); strUI = strUI.Replace("##ShipToStreet##", CCommon.RenderControl(txtShipToStreet)); }
                if (strUI.Contains("##OrganizationComments##")) { AddToTable("txtComments", txtOrganizationComments.Text.Trim()); strUI = strUI.Replace("##OrganizationComments##", CCommon.RenderControl(txtOrganizationComments)); }

                if (strUI.Contains("##Password##")) { IsPasswordFieldAddedToForm = true; AddToTable("vcPassword", txtPassword.Text.Trim()); strUI = strUI.Replace("##Password##", CCommon.RenderControl(txtPassword)); }
                if (strUI.Contains("##PasswordConfirm##")) { AddToTable("vcPasswordConfirm", txtPasswordConfirm.Text.Trim()); strUI = strUI.Replace("##PasswordConfirm##", CCommon.RenderControl(txtPasswordConfirm)); }
                
                if (!IsPasswordFieldAddedToForm && strUI.Contains("##FuturePassword##"))
                {
                    AddToTable("vcFuturePassword", txtFuturePassword.Text.Trim());
                    strUI = strUI.Replace("##FuturePassword##", CCommon.RenderControl(txtFuturePassword));
                }
                else
                {
                    strUI = strUI.Replace("##FuturePassword##", "");
                }

                if (strUI.Contains("##ShippingAddresssameasBillingAddress##")) { strUI = strUI.Replace("##ShippingAddresssameasBillingAddress##", CCommon.RenderControl(chkShippingAddresssameasBillingAddress)); }
                if (strUI.Contains("##bitOptOut##")) { AddToTable("bitOptOut", chkbitOptOut.Checked.ToString()); strUI = strUI.Replace("##bitOptOut##", CCommon.RenderControl(chkbitOptOut)); }

                if (strUI.Contains("##ContactCountry##"))
                {
                    AddToTable("vcPCountry", ddlContactCountry.SelectedValue);
                    BindCountryState(ddlContactCountry, strUI);
                }

                if (strUI.Contains("##ContactState##"))
                {
                    AddToTable("vcPState", ddlContactState.SelectedValue);
                    if (!IsPostBack)
                    {
                        BindDropDown(ddlContactState, "S", 0);
                        modBacrm.FillState(ddlContactState, Sites.ToLong(ddlContactCountry.SelectedItem.Value), numDomainId);
                    }
                }

                if (strUI.Contains("##ShipCountry##"))
                {
                    AddToTable("vcShipCountry", ddlShipCountry.SelectedValue);
                    BindCountryState(ddlShipCountry, strUI);

                    if (ddlShipState != null && hdnShipToState != null && !string.IsNullOrEmpty(hdnShipToState.Value) && ddlShipState.SelectedItem.Text != hdnShipToState.Value)
                    {
                        if (ddlShipState.Items.FindByText(hdnShipToState.Value) != null)
                        {
                            ddlShipState.ClearSelection();
                            ddlShipState.Items.FindByText(hdnShipToState.Value).Selected = true;
                        }
                    }
                }

                if (strUI.Contains("##ShipState##"))
                {
                    AddToTable("vcShipState", ddlShipState.SelectedValue);
                    if (!IsPostBack)
                    {
                        BindDropDown(ddlShipState, "S", 0);
                        modBacrm.FillState(ddlShipState, Sites.ToLong(ddlShipCountry.SelectedItem.Value), numDomainId);
                    }

                }

                if (strUI.Contains("##BillCountry##"))
                {
                    AddToTable("vcBillCountry", ddlBillCountry.SelectedValue);
                    BindCountryState(ddlBillCountry, strUI);

                    if (ddlBillState != null && hdnBillToState != null && !string.IsNullOrEmpty(hdnBillToState.Value) && ddlBillState.SelectedItem.Text != hdnBillToState.Value)
                    {
                        if (ddlBillState.Items.FindByText(hdnBillToState.Value) != null)
                        {
                            ddlBillState.ClearSelection();
                            ddlBillState.Items.FindByText(hdnBillToState.Value).Selected = true;
                        }
                    }
                }

                if (strUI.Contains("##BillState##"))
                {
                    AddToTable("vcBilState", ddlBillState.SelectedValue);
                    if (!IsPostBack)
                    {
                        BindDropDown(ddlBillState, "S", 0);
                        modBacrm.FillState(ddlBillState, Sites.ToLong(ddlBillCountry.SelectedItem.Value), numDomainId);
                    }
                }

                strUI = strUI.Replace("##ContactCountry##", CCommon.RenderControl(ddlContactCountry));
                strUI = strUI.Replace("##ContactState##", CCommon.RenderControl(ddlContactState));

                strUI = strUI.Replace("##ShipCountry##", CCommon.RenderControl(ddlShipCountry));
                strUI = strUI.Replace("##ShipState##", CCommon.RenderControl(ddlShipState));

                strUI = strUI.Replace("##BillCountry##", CCommon.RenderControl(ddlBillCountry));
                strUI = strUI.Replace("##BillState##", CCommon.RenderControl(ddlBillState));

                if (strUI.Contains("##ContactCategory##")) { if (!IsPostBack) { BindDropDown(ddlContactCategory, "LI", 25); } AddToTable("vcCategory", ddlContactCategory.SelectedValue); strUI = strUI.Replace("##ContactCategory##", CCommon.RenderControl(ddlContactCategory)); }

                if (strUI.Contains("##ContactPosition##")) { if (!IsPostBack) { BindDropDown(ddlConatctPosition, "LI", 41); } AddToTable("vcPosition", ddlConatctPosition.SelectedValue); strUI = strUI.Replace("##ContactPosition##", CCommon.RenderControl(ddlConatctPosition)); }

                if (strUI.Contains("##ContactTeam##")) { if (!IsPostBack) { BindDropDown(ddlContactTeam, "LI", 35); } AddToTable("numTeam", ddlContactTeam.SelectedValue); strUI = strUI.Replace("##ContactTeam##", CCommon.RenderControl(ddlContactTeam)); }

                if (strUI.Contains("##OrganizationAnnualRevenue##")) { if (!IsPostBack) { BindDropDown(ddlOrganizationAnnualRevenue, "LI", 6); } AddToTable("numAnnualRevID", ddlOrganizationAnnualRevenue.SelectedValue); strUI = strUI.Replace("##OrganizationAnnualRevenue##", CCommon.RenderControl(ddlOrganizationAnnualRevenue)); }

                if (strUI.Contains("##NoofEmployees##")) { if (!IsPostBack) { BindDropDown(ddlNoOfEmployees, "LI", 7); } AddToTable("numNoOfEmployeesId", ddlNoOfEmployees.SelectedValue); strUI = strUI.Replace("##NoofEmployees##", CCommon.RenderControl(ddlNoOfEmployees)); }

                if (strUI.Contains("##InfoSource##")) { if (!IsPostBack) { BindDropDown(ddlInfoSource, "LI", 18); } AddToTable("vcHow", ddlInfoSource.SelectedValue); strUI = strUI.Replace("##InfoSource##", CCommon.RenderControl(ddlInfoSource)); }

                if (strUI.Contains("##OrganizationProfile##")) { if (!IsPostBack) { BindDropDown(ddlOrganizationProfile, "LI", 21); } AddToTable("vcProfile", ddlOrganizationProfile.SelectedValue); strUI = strUI.Replace("##OrganizationProfile##", CCommon.RenderControl(ddlOrganizationProfile)); }

                if (strUI.Contains("##ContactStatus##")) { if (!IsPostBack) { BindDropDown(ddlContactStatus, "LI", 44); } AddToTable("numEmpStatus", ddlContactStatus.SelectedValue); strUI = strUI.Replace("##ContactStatus##", CCommon.RenderControl(ddlContactStatus)); }

                if (strUI.Contains("##Relationship##")) { if (!IsPostBack) { BindDropDown(ddlRelationship, "LI", 5); } AddToTable("numCompanyType", ddlRelationship.SelectedValue); strUI = strUI.Replace("##Relationship##", CCommon.RenderControl(ddlRelationship)); }

                if (strUI.Contains("##Industry##")) { if (!IsPostBack) { BindDropDown(ddlIndustry, "LI", 4); } AddToTable("numCompanyIndustry", ddlIndustry.SelectedValue); strUI = strUI.Replace("##Industry##", CCommon.RenderControl(ddlIndustry)); }

                if (strUI.Contains("##OrganizationRating##")) { if (!IsPostBack) { BindDropDown(ddlOrganizationRating, "LI", 2); } AddToTable("numCompanyRating", ddlOrganizationRating.SelectedValue); strUI = strUI.Replace("##OrganizationRating##", CCommon.RenderControl(ddlOrganizationRating)); }

                if (strUI.Contains("##OrganizationStatus##")) { if (!IsPostBack) { BindDropDown(ddlOrganizationStatus, "LI", 1); } AddToTable("numStatusID", ddlOrganizationStatus.SelectedValue); strUI = strUI.Replace("##OrganizationStatus##", CCommon.RenderControl(ddlOrganizationStatus)); }

                if (strUI.Contains("##Territory##")) { if (!IsPostBack) { BindDropDown(ddlTerritory, "T", 36); } AddToTable("numTerID", ddlTerritory.SelectedValue); strUI = strUI.Replace("##Territory##", CCommon.RenderControl(ddlTerritory)); }

                if (strUI.Contains("##OrganizationCampaign##")) { if (!IsPostBack) { BindDropDown(ddlOrganizationCampaign, "C", 24); } AddToTable("numCampaignID", ddlOrganizationCampaign.SelectedValue); strUI = strUI.Replace("##OrganizationCampaign##", CCommon.RenderControl(ddlOrganizationCampaign)); }

                if (strUI.Contains("##OrganizationCreditLimit##")) { if (!IsPostBack) { BindDropDown(ddlOrganizationCreditLimit, "LI", 3); } AddToTable("numCompanyCredit", ddlOrganizationCreditLimit.SelectedValue); strUI = strUI.Replace("##OrganizationCreditLimit##", CCommon.RenderControl(ddlOrganizationCreditLimit)); }

                if (strUI.Contains("##DepartmentName##")) { if (!IsPostBack) { BindDropDown(ddlDepartmentName, "LI", 19); } AddToTable("vcDepartment", ddlDepartmentName.SelectedValue); strUI = strUI.Replace("##DepartmentName##", CCommon.RenderControl(ddlDepartmentName)); }

                if (strUI.Contains("##Follow-UpStatus##")) { if (!IsPostBack) { BindDropDown(ddlFollowUpStatus, "LI", 30); } AddToTable("numFollowUpStatus", ddlFollowUpStatus.SelectedValue); strUI = strUI.Replace("##Follow-UpStatus##", CCommon.RenderControl(ddlFollowUpStatus)); }

                strUI = strUI.Replace("##SubmitButton##", CCommon.RenderControl(btnSubmit));
                strUI = strUI.Replace("##LoginButton##", CCommon.RenderControl(hplLogin));

                BindCustomField(ref strUI);

                pnlCutomizeHtml.Visible = false;
                litCutomizeHtml.Text = strUI;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        private void BindCustomField(ref string strUI)
        {
            FormConfigWizard objFormWizard = new FormConfigWizard();

            objFormWizard.DomainID = Sites.ToLong(Session["DomainID"]);

            objFormWizard.LocationIds = "1,4"; //1=Lead/Prospect/Account , 4 =Contact Details

            dtCustFld = objFormWizard.GetCustomFormFields();

            GenerateGenericFormControls.DomainID = numDomainId;

            string FldName = "";
            foreach (DataRow row in dtCustFld.Rows)
            {
                FldName = "##" + row["vcFormFieldName"].ToString().Trim().Replace(" ", "") + "_" + row["vcFieldType"].ToString() + "##";
                if (strUI.Contains(FldName))
                {
                    Control c = null;
                    c = (Control)GenerateGenericFormControls.getDynamicControlAndData(Sites.ToInteger(row["numFormFieldId"].ToString().Replace("R", "").Replace("C", "").Replace("D", "")), row["vcFormFieldName"].ToString().Trim().Replace(" ", "") + "_" + row["vcFieldType"].ToString(), row["vcListItemType"].ToString(), row["vcAssociatedControlType"].ToString(), Sites.ToInteger(row["numListID"]), 0, 0);

                    plhFormControls.Controls.Add(c);
                    strUI = strUI.Replace(FldName, CCommon.RenderControl(plhFormControls.FindControl(c.ID)));
                }
            }
        }
        private void AddToTable(string ColumnName, string ColumnText)
        {
            DataRow dRow = null;

            dRow = dtGenericFormConfig.NewRow();
            dRow["vcDbColumnName"] = ColumnName;
            dRow["vcDbColumnText"] = ColumnText;
            dtGenericFormConfig.Rows.Add(dRow);
        }
        private void BindCountryState(DropDownList oControlBox, string strUI)
        {
            if (strUI.Contains("##" + oControlBox.ID.Replace("ddl", "").Replace("Country", "State") + "##"))
            {
                oControlBox.AutoPostBack = true;
                oControlBox.SelectedIndexChanged += FillStateDyn;
            }

            if (!IsPostBack)
            {
                BindDropDown(oControlBox, "LI", 40);

                DataTable dtTable = default(DataTable);
                UserAccess objUserAccess = new UserAccess();
                objUserAccess.DomainID = numDomainId;
                dtTable = objUserAccess.GetDomainDetails();

                if ((oControlBox.Items.FindByValue(dtTable.Rows[0]["numDefCountry"].ToString()) != null))
                {
                    oControlBox.Items.FindByValue(dtTable.Rows[0]["numDefCountry"].ToString()).Selected = true;
                }
            }
        }
        private void BindDropDown(DropDownList oControlBox, string vcListItemType, int numListID)
        {
            FormGenericFormContents objConfigWizard = new FormGenericFormContents();
            //Create an object of class which encaptulates the functionaltiy
            objConfigWizard.DomainID = Sites.ToLong(Session["DomainID"]);
            //set the domain id
            objConfigWizard.ListItemType = vcListItemType;
            //set the listitem type

            DataTable dtTable = null;
            //declare a datatable
            dtTable = objConfigWizard.GetMasterListByListId(numListID);
            //call function to fill the datatable
            DataRow dRow = null;
            //declare a datarow
            dRow = dtTable.NewRow();
            //get a new row object
            dtTable.Rows.InsertAt(dRow, 0);
            //insert the row
            dtTable.Rows[0]["numItemID"] = DBNull.Value;
            //set the values for the first entry
            dtTable.Rows[0]["vcItemName"] = "---Select One---";
            //set the group name
            oControlBox.DataSource = dtTable;
            //set the datasource of the drop down
            oControlBox.DataTextField = "vcItemName";
            //Set the Text property of the drop down
            oControlBox.DataValueField = "numItemID";
            //Set the value attribute of the textbox
            oControlBox.DataBind();
            //Databind the drop down
        }
        private void FillStateDyn(object sender, EventArgs e)
        {
            try
            {
                DropDownList ddl = (DropDownList)sender;
                modBacrm.FillState((DropDownList)this.FindControl(ddl.ID.ToString().Replace("Country", "State")), Sites.ToLong(ddl.SelectedItem.Value), numDomainId);
                if (ddl.ID == "ddlContactCountry" && !string.IsNullOrEmpty(hdnContactState.Value.Trim()) && this.FindControl(ddl.ID.ToString().Replace("Country", "State")) != null && ((DropDownList)this.FindControl(ddl.ID.ToString().Replace("Country", "State"))).Items.FindByText(hdnContactState.Value) != null)
                {
                    ((DropDownList)this.FindControl(ddl.ID.ToString().Replace("Country", "State"))).Items.FindByText(hdnContactState.Value).Selected = true;
                }
                else if (ddl.ID == "ddlBillCountry" && !string.IsNullOrEmpty(hdnBillToState.Value.Trim()) && this.FindControl(ddl.ID.ToString().Replace("Country", "State")) != null && ((DropDownList)this.FindControl(ddl.ID.ToString().Replace("Country", "State"))).Items.FindByText(hdnBillToState.Value) != null)
                {
                    ((DropDownList)this.FindControl(ddl.ID.ToString().Replace("Country", "State"))).Items.FindByText(hdnBillToState.Value).Selected = true;
                }
                else if (ddl.ID == "ddlShipCountry" && !string.IsNullOrEmpty(hdnShipToState.Value.Trim()) && this.FindControl(ddl.ID.ToString().Replace("Country", "State")) != null && ((DropDownList)this.FindControl(ddl.ID.ToString().Replace("Country", "State"))).Items.FindByText(hdnShipToState.Value) != null)
                {
                    ((DropDownList)this.FindControl(ddl.ID.ToString().Replace("Country", "State"))).Items.FindByText(hdnShipToState.Value).Selected = true;
                }
                bindHtml();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        /// -----------------------------------------------------------------------------
        /// <summary>
        ///     This function is called to initiate the generation of the dynamic form.
        /// </summary>
        /// <remarks>
        /// </remarks>
        /// <history>
        /// 	[Debasish Tapan Nag]	08/20/2005	Created
        /// </history>
        ///-----------------------------------------------------------------------------
        private void callFuncForFormGenerationNonAOI()
        {
            try
            {
                GenerateGenericFormControls.FormID = 4;
                //set the form id to Order Form
                GenerateGenericFormControls.DomainID = numDomainId;
                //Set the domain id
                GenerateGenericFormControls.sXMLFilePath = ConfigurationManager.AppSettings["PortalLocation"] + "\\Documents\\Docs\\";
                //set the file path
                GenerateGenericFormControls.boolAOIField = 0;
                //Set the AOI flag to non AOI
                dtGenericFormConfig = GenerateGenericFormControls.getFormControlConfig();
                //Declare and pupulate a Datatable to store the form config
                if (dtGenericFormConfig.Rows.Count == 0)
                {
                    //if the xml for form fields has not been configured
                    btnSubmit.Enabled = false;
                    //Disable any attempt to save/ submit
                    plhFormControls.Controls.Add(new LiteralControl("The screen is not created. Please use BizForm Wizard to create this screen."));
                    //calls to create the form controls and add it to the form
                    //and exit the flow
                    return;
                }
                plhFormControls.Controls.Add(GenerateGenericFormControls.createFormControls(dtGenericFormConfig));
                //calls to create the form controls and add it to the form
                //Create teh javascript array and store
                litClientScript.Text = GenerateGenericFormControls.getJavascriptArray(dtGenericFormConfig);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private string GetCustFldValue(string fldType, string fld_Id)
        {
            try
            {
                if (fldType == "TextBox")
                {
                    TextBox txt = default(TextBox);
                    txt = (TextBox)plhFormControls.FindControl(fld_Id);
                    return txt.Text;
                }
                else if (fldType == "SelectBox")
                {
                    DropDownList ddl = default(DropDownList);
                    ddl = (DropDownList)plhFormControls.FindControl(fld_Id);
                    return (string)(string.IsNullOrEmpty(ddl.SelectedItem.Value) ? "0" : ddl.SelectedItem.Value);
                }
                else if (fldType == "TextArea")
                {
                    TextBox txt = default(TextBox);
                    txt = (TextBox)plhFormControls.FindControl(fld_Id);
                    return txt.Text;
                }
                else if (fldType == "CheckBox")
                {
                    CheckBox chk = default(CheckBox);
                    chk = (CheckBox)plhFormControls.FindControl(fld_Id);
                    return chk.Checked.ToString();
                }
                return "";
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        protected void btnRetrieve_Click(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(txtEmail.Text.Trim()))
                {
                    Session["SMTPServerIntegration"] = "True";
                    UserAccess objUserAccess = new UserAccess();
                    objUserAccess.Email = txtEmail.Text.Trim();
                    objUserAccess.boolPortal = true;
                    objUserAccess.DomainID = Sites.ToLong(Session["DomainID"]);
                    objUserAccess.SendPassoword();
                    if (objUserAccess.NoOfRows == 1)
                    {
                        QueryStringValues objEncryption = new QueryStringValues();

                        Email objSendEmail = new Email();
                        objSendEmail.DomainID = Sites.ToLong(Session["DomainID"]);
                        objSendEmail.TemplateCode = "#SYS#ECOMMERCE_FORGOT_PASSWORD";
                        objSendEmail.ModuleID = 1;
                        objSendEmail.RecordIds = Sites.ToString(objUserAccess.ContactID);
                        objSendEmail.AdditionalMergeFields.Add("ContactEcommercepassword", "<a href='" + Request.Url.Scheme + "://" + Request.Url.Authority + "/ResetPassword.aspx?resetId=" + objEncryption.Encrypt(objUserAccess.Password) + "'>Click Here</a>");
                        objSendEmail.AdditionalMergeFields.Add("PortalPassword", "<a href='" + Request.Url.Scheme + "://" + Request.Url.Authority + "/ResetPassword.aspx?resetId=" + objEncryption.Encrypt(objUserAccess.Password) + "'>Click Here</a>");
                        objSendEmail.AdditionalMergeFields.Add("LoggedInUser", "");
                        objSendEmail.FromEmail = Sites.ToString(Session["DomainAdminEmail"]);
                        objSendEmail.ToEmail = txtEmail.Text.Trim();
                        objSendEmail.SendEmailTemplate();

                        ShowMessage("Please check your inbox for an email to reset password.", 0);
                    }
                    else if (objUserAccess.NoOfRows == 0)
                    {
                        ShowMessage("Check Your Email Id and try Again !", 1);
                    }
                    else if (objUserAccess.NoOfRows > 1)
                    {
                        ShowMessage("Found Multiple Records.Contact Administrator !", 1);
                    }
                }
                else
                {
                    ShowMessage("Please enter email address !", 1);
                }
            }
            catch (Exception ex)
            {
                ExceptionModule.ExceptionPublish(ex, Sites.ToInteger(Session["DomainID"]), Sites.ToInteger(Session["UserContactID"]), Sites.ToInteger(Session["SiteId"]), Request);
                ShowMessage(ex.ToString(), 1);
            }
        }
    }
}