﻿using BACRM.BusinessLogic.Common;
using BACRM.BusinessLogic.Item;
using BACRM.BusinessLogic.ShioppingCart;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using Telerik.Web.UI;

namespace BizCart.UserControls
{
    public partial class ProductFilters : BizUserControl
    {
        #region Events

        protected void Page_Init(object sender, EventArgs e)
        {
            Sites.InitialiseSession();
            hdnProductFilterCategoryID.Value = CCommon.ToLong(Request["Cat"].Trim('/')).ToString();
        }

        #endregion
    }
}