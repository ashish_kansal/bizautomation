﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Login.ascx.cs" Inherits="BizCart.UserControls.Login" %>
<script language="javascript" type="text/javascript">
    function Login(a, b) {
        if (document.getElementById(a).value == "") {
            alert("Enter Email Address")
            document.getElementById(a).focus();
            return false;
        }
        if (document.getElementById(b).value == "") {
            alert("Enter Password");
            document.getElementById(b).focus();
            return false;
        }
    }
</script>
<asp:Panel ID="pnlCutomizeHtml" runat="server">
    <table>
        <tr>
            <td class="normal4">
                <asp:Literal ID="litMessage" runat="server"></asp:Literal>
            </td>
        </tr>
    </table>
    <table id="tblLogin" runat="server" border="0" width="100%">
        <tr>
            <td colspan="2">
                <div class="sectionheader">
                    <asp:Label ID="lblTitle" runat="server" Text="Login"></asp:Label>
                </div>
            </td>
        </tr>
        <tr>
            <td class="LabelColumn">
                Email Address
            </td>
            <td class="ControlCell">
                <asp:TextBox ID="txtEmaillAdd" CssClass="textbox" runat="server" autocomplete="on"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="LabelColumn">
                Password
            </td>
            <td class="ControlCell">
                <asp:TextBox ID="txtPassword" CssClass="textbox" TextMode="Password" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td>
                <asp:Button ID="btnLogin" CssClass="button" runat="server" Text="Login" UseSubmitBehavior="true"
                    OnClick="btnLogin_Click1"></asp:Button>
                <asp:HyperLink ID="hplForgotPassword" runat="server" Text="Forgot Password ?"></asp:HyperLink>
            </td>
        </tr>
        <tr>
            <td align="center" colspan="2">
                <div runat="server" id="dvNewUsers">
                    <asp:HyperLink ID="hplSignUp" runat="server" Text="New Users - Sign up here"></asp:HyperLink>
                </div>
            </td>
        </tr>
    </table>
</asp:Panel>
<asp:Literal ID="litCutomizeHtml" runat="server"></asp:Literal>
<%--<asp:Button ID="btnPDF" runat="server" Text="PDF" Width="50" CssClass="button" 
                onclick="btnPDF_Click">
            </asp:Button>--%>
<asp:HiddenField runat="server" ID="hdnLoginSucess" ClientIDMode="Static" />
