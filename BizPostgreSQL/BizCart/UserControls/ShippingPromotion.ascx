﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ShippingPromotion.ascx.cs" Inherits="BizCart.UserControls.ShippingPromotion"
    ClientIDMode="Static" %>

<%--<script type="text/javascript" src="https://maps.google.com/maps/api/js?sensor=false&key=AIzaSyDKZ2Kph5NQXTnPc1xEzM4Bq0nhzXjONS0"></script>--%>
<script type="text/javascript">
    //function getLocation(txt) {
    //    getAddressInfoByZip(txt.value);
    //}

    //function getAddressInfoByZip(zip) {
    //    if (zip.length >= 5 && typeof google != 'undefined') {
    //        var addr = {};
    //        var geocoder = new google.maps.Geocoder();
    //        geocoder.geocode({ 'address': zip }, function (results, status) {
    //            if (status == google.maps.GeocoderStatus.OK) {
    //                if (results.length >= 1) {
    //                    for (var ii = 0; ii < results[0].address_components.length; ii++) {
    //                        var types = results[0].address_components[ii].types.join(",");
    //                        if (types == "administrative_area_level_1,political") {
    //                            addr.state = results[0].address_components[ii].long_name;
    //                        }

    //                        if (types == "country,political") {
    //                            addr.country = results[0].address_components[ii].long_name;
    //                        }
    //                    }

    //                    //try to get biz state and country id from street found from google gecode api
    //                    addr.success = true;
    //                    for (name in addr) {
    //                        if (name == "state") {
    //                            document.getElementById('lblChangeState').innerText = addr[name];
    //                            document.getElementById("hdnChangeState").value = addr[name];
    //                        }
    //                    }
    //                    response(addr);
    //                }
    //            }
    //        });
    //    }
    //}

    function ShowShippingException() {
        $("#divSPException").modal('show');
        return false;
    }

    function ValidateChangeZipForm() {
        if ($("[id$=txtSPZipCode]").val() == "") {
            alert("Please enter zip/postal code");
            $("[id$=txtSPZipCode]").focus();
            return false;
        } else if ($("[id$=ddlSPCountry]").val() == "0") {
            alert("Please select country");
            $("[id$=ddlSPCountry]").focus();
            return false;
        } else if ($("[id$=ddlSPState]").val() == "0") {
            alert("Please select State/Province");
            $("[id$=ddlSPState]").focus();
            return false;
        }

        return true;
    }
</script>
<asp:Panel ID="pnlCutomizeHtml" runat="server">
    <div id="plhShippingPromotion" runat="server"></div>
    <div id="plhShippingPromotionChangeZip" runat="server"></div>
    <asp:Label runat="server" ID="lblSPDescription" CssClass="sp-description"></asp:Label>
    <asp:HyperLink ID="hplSPException" CssClass="sp-exception" runat="server" Text="exceptions apply" onclick="return ShowShippingException();"></asp:HyperLink>
    <asp:Button ID="btnSPChangeShipTo" runat="server" CssClass="btn btn-primary sp-changeshipto" Text="Change Ship-to" OnClick="btnSPChangeShipTo_Click" />
    <asp:TextBox runat="server" ID="txtSPZipCode" CssClass="form-control sp-zipcode"></asp:TextBox>
    <asp:DropDownList runat="server" ID="ddlSPState" CssClass="form-control sp-state"></asp:DropDownList>
    <asp:DropDownList runat="server" ID="ddlSPCountry" CssClass="form-control sp-country" OnSelectedIndexChanged="ddlSPCountry_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
    <asp:Button ID="btnSPSetZip" runat="server" CssClass="btn btn-primary sp-setzip" Text="Set" OnClientClick="return ValidateChangeZipForm();" OnClick="btnSPSetZip_Click" />
</asp:Panel>
<asp:Literal ID="litCutomizeHtml" runat="server"></asp:Literal>
<asp:HiddenField ID="hdnIsChangeZip" runat="server" />


