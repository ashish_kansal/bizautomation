﻿using System;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BACRM.BusinessLogic.Item;
using BACRM.BusinessLogic.Common;
using BACRM.BusinessLogic.ShioppingCart;
//using BACRM.BusinessLogic.Opportunities;

namespace BizCart.UserControls
{
    public partial class EstimateShipping : BizUserControl
    {
        long lngItemCode, lngCategoryID;
        protected void Page_Load(object sender, EventArgs e)
        {
            lngItemCode = Sites.ToLong(Request["ItemID"]);
            lngCategoryID = Sites.ToLong(Request["CatID"]);
            if (!IsPostBack)
            {
                BindShippingCalculator();
            }


        }
        protected void btnEstimate_Click(object sender, EventArgs e)
        {
            try
            {
                trShippingMethod.Visible = true;

                ShippingRule objRule = new ShippingRule();
                objRule.DomainID = CCommon.ToLong(Session["DomainID"]);
                objRule.SiteID = CCommon.ToLong(Session["SiteID"]);
                objRule.ItemID = lngItemCode;
                objRule.CategoryID = lngCategoryID;
                objRule.DivisionID = Sites.ToLong(Session["DivisionID"]);
                objRule.RelationshipID = Sites.ToLong(Session["RelationShip"]);
                objRule.ProfileID = Sites.ToLong(Session["Profile"]);
                objRule.Height = Sites.ToDouble(txtHeight.Text);
                objRule.Width = Sites.ToDouble(txtWidth.Text);
                objRule.Length = Sites.ToDouble(txtLength.Text);
                objRule.Weight = Sites.ToDouble(txtWeight.Text);
                objRule.ZipCode = txtZipCode.Text.Trim();
                objRule.Country = Sites.ToLong(ddlCountry.SelectedValue);
                objRule.State = Sites.ToLong(ddlState.SelectedValue);
                string strMessage = objRule.GetShippingMethodForItem(ddlShippingMethod,true);
                if (strMessage.Length > 0)
                    ShowMessage(strMessage, 1);

                if (Sites.ToLong(ddlState.SelectedValue) > 0) { Session["DefState"] = ddlState.SelectedValue; }

                bindHtml();

            }
            catch (Exception ex)
            {
                ExceptionModule.ExceptionPublish(ex, Sites.ToLong(Session["DomainID"]), Sites.ToLong(Session["UserContactID"]), Sites.ToLong(Session["SiteId"]), Request);
                ShowMessage(ex.ToString(), 1);
            }
        }
        private void BindShippingCalculator()
        {
            try
            {
                //Estimate shipping
                if (ddlState.Items.Count == 0)
                {
                    CCommon objCommon = new CCommon();
                    objCommon.sb_FillComboFromDBwithSel(ref ddlCountry, 40, Sites.ToLong(Session["DomainID"]));
                }
                if (CCommon.ToLong(Session["DefCountry"]) > 0 && ddlState.Items.Count == 0)
                {
                    if (ddlCountry.Items.FindByValue(CCommon.ToString(Session["DefCountry"])) != null)
                    {
                        ddlCountry.Items.FindByValue(CCommon.ToString(Session["DefCountry"])).Selected = true;

                        FillState(ddlState, Sites.ToLong(ddlCountry.SelectedValue), Sites.ToLong(Session["DomainID"]));

                        if (Sites.ToLong(Session["DefState"]) > 0)
                        {
                            if (ddlState.Items.FindByValue(CCommon.ToString(Session["DefState"])) != null)
                            {
                                ddlState.Items.FindByValue(CCommon.ToString(Session["DefState"])).Selected = true;
                            }
                        }
                    }
                }
                CItems objItems = new CItems();
                DataSet ds = default(DataSet);
                DataTable dtItemDetails = default(DataTable);
                objItems.ItemCode = Sites.ToInteger(lngItemCode.ToString());
                objItems.WarehouseID = Sites.ToLong(Session["WareHouseID"]);
                objItems.DomainID = Sites.ToLong(Session["DomainID"]);
                objItems.SiteID = Sites.ToLong(Session["SiteId"]);
                if ((!System.Web.HttpContext.Current.User.Identity.IsAuthenticated || CCommon.ToLong(Session["DomainID"]) == 0 || CCommon.ToLong(Session["UserContactID"]) == 0) && Sites.ToInteger(Session["OnePageCheckout"]) == 0)
                {
                    //GUEST USER
                    objItems.DivisionID = 0;
                }
                else
                {
                    objItems.DivisionID = Sites.ToLong(Session["DivId"]);
                }
                ds = objItems.ItemDetailsForEcomm();
                if (ds.Tables.Count == 2)
                {
                    dtItemDetails = ds.Tables[0];
                    txtWeight.Text = dtItemDetails.Rows[0]["fltWeight"].ToString();
                    txtWidth.Text = dtItemDetails.Rows[0]["fltWidth"].ToString();
                    txtLength.Text = dtItemDetails.Rows[0]["fltLength"].ToString();
                    txtHeight.Text = dtItemDetails.Rows[0]["fltHeight"].ToString();
                }
                
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        void bindHtml()
        {
            try
            {
                string strUI = GetUserControlTemplate("EstimateShipping.htm");

                strUI = strUI.Replace("##Country##", CCommon.RenderControl(ddlCountry));
                strUI = strUI.Replace("##State##", CCommon.RenderControl(ddlState));

                strUI = strUI.Replace("##ZipCode##", CCommon.RenderControl(txtZipCode));
                strUI = strUI.Replace("##EstimateShippingButton##", CCommon.RenderControl(btnEstimate));
                strUI = strUI.Replace("##showshippingmethod##", trShippingMethod.Visible == true ? "" : "display:none;");
                strUI = strUI.Replace("##ShippingMethodDropDownList##", CCommon.RenderControl(ddlShippingMethod));

                pnlCutomizeHtml.Visible = false;
                litCutomizeHtml.Text = strUI;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void ddlCountry_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                FillState(ddlState, Sites.ToLong(ddlCountry.SelectedValue), Sites.ToLong(Session["DomainID"]));
                bindHtml();
            }
            catch (Exception ex)
            {
                ExceptionModule.ExceptionPublish(ex, Sites.ToLong(Session["DomainID"]), Sites.ToLong(Session["UserContactID"]), Sites.ToLong(Session["SiteId"]), Request);
                ShowMessage(ex.ToString(), 1);
            }
        }
    }
}