﻿using System;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BACRM.BusinessLogic.ShioppingCart;
using BACRM.BusinessLogic.Item;
using BACRM.BusinessLogic.Common;
using System.Text.RegularExpressions;
using System.Text;
using System.Collections;
using BACRM.BusinessLogic.Contacts;
using BACRM.BusinessLogic.Admin;

namespace BizCart.UserControls
{
    public partial class ManufacturerList : BizUserControl
    {
        #region Global Declaration

        int PageIndex = 0;
        string SearchText = string.Empty;
        string AdvancedSearchText = string.Empty;
        long CategoryID = 0;
        long ManufacturerID = 0;
        long RelationShipId = 0;
        long ManufacturerLogoFieldId = 0;
        long ManufacturerDescriptionFieldId = 0;
        long ManufacturerDisplayFieldId = 0;
        public int TrimNameLength = 0;
        

        public string TrimDescriptionbyfollowingnoofcharacters { get; set; }
        public string AfterAddingItemShowCartPage { get; set; }
        public string TrimItemNamebyfollowingnoofcharacters { get; set; }

        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                Sites.InitialiseSession();

                if (!IsPostBack)
                {
                    string querystring = Request.QueryString["enc"];
                    querystring = encrypt(querystring);
                    querystring = decrypt(querystring);
                    string[] strArray = querystring.Split(',');
                    if (strArray.Length == 2)
                    {
                        RelationShipId = Convert.ToInt64(strArray[0]);
                        ManufacturerDisplayFieldId = Convert.ToInt64(strArray[1]);
                        bindHtml();
                    }
                }

                BreadCrumb.Clear();

                BreadCrumb.OverWriteBreadCrumb(1, "Home.aspx", "Home", "Home.aspx");


                BreadCrumb.OverWriteBreadCrumb(2, "Manufacturer.aspx", "Manufacturer", "Manufacturer.aspx");

            }
            catch (Exception ex)
            {
                ExceptionModule.ExceptionPublish(ex, Sites.ToLong(Session["DomainID"]), Sites.ToLong(Session["UserContactID"]), Sites.ToLong(Session["SiteId"]), Request);
                ShowMessage(ex.ToString(), 1);
            }
        }
        public static string ReplaceAllSpaces(string str)
        {
            return Regex.Replace(str, @"\s+", "%20");
        }
        #endregion
        public  string encrypt(string ToEncrypt)
        {
            return Convert.ToBase64String(Encoding.ASCII.GetBytes(ToEncrypt));
        }
        public  string decrypt(string cypherString)
        {
            return Encoding.ASCII.GetString(Convert.FromBase64String(cypherString));
        }
        #region Methods

        #region HTML
        private void bindHtml()
        {
            try
            {
                string strUI = GetUserControlTemplate("ManufacturerList.htm");
                StringBuilder sb = new StringBuilder();
                string pattern = "<##FOR(?<Property>(\\b[^>]*?))##>(?<Content>([\\s\\S]*?))<##ENDFOR##>";
                if (Regex.IsMatch(strUI, pattern))
                {
                    MatchCollection matches = Regex.Matches(strUI, pattern);
                    if ((matches.Count > 0))
                    {
                        Hashtable htProperties = GetPropertiesAsHastable(matches[0].Groups["Property"].Value.Replace("(", "").Replace(")", ""));
                        int RowsCount = 0;
                        string Content = matches[0].Groups["Content"].Value;

                        Sites objSite = new Sites();
                        CItems objItems = new CItems();
                        DataTable dtTableCategory = new DataTable();

                        objItems.CategoryID = CategoryID;
                        objItems.DomainID = Sites.ToLong(Session["DomainID"]);
                        objItems.DivisionID = RelationShipId;
                        objItems.numShowOnWebFieldId = ManufacturerDisplayFieldId;
                        string strTemp = string.Empty;

                        DataSet dsItems = default(DataSet);
                        DataTable dtItemColumn = default(DataTable);

                        objItems.ManufacturerID = 0;
                        dsItems = objItems.GetManufactuerersForECommerce();

                        if (dsItems.Tables.Count > 0)
                        {
                            DataTable dtItems = dsItems.Tables[0];
                            dtItemColumn = dsItems.Tables[0];
                            string strFileName="";
                            foreach (DataRow dr in dtItemColumn.Rows)
                            {
                                strTemp = Content;
                                strFileName = "/PortalDocs/" + Session["DomainID"] + "/" +CCommon.ToString(dr["VcFileName"]);
                                strTemp = strTemp.Replace("##ManufacturerLogo##", strFileName);
                                strTemp = strTemp.Replace("##ManufacturerName##", CCommon.ToString(dr["vcCompanyName"]));
                                strTemp = strTemp.Replace("##ManufacturerLogo##", CCommon.ToString(dr["txtComments"]));
                                strTemp = strTemp.Replace("##ManufacturerURLParameter##", CCommon.ToString(dr["numDivisionID"]));
                                sb.Append(strTemp);
                            }
                        }
                        strUI = Regex.Replace(strUI, pattern, sb.ToString());

                        pnlCutomizeHtml.Visible = false;
                        litCutomizeHtml.Text = strUI;
                    }
                }
            }
            catch (Exception ex)
            {
                if (ex.Message.Contains("no segments* file found in Lucene.Net.Store.SimpleFSDirectory"))
                {
                    ShowMessage(GetErrorMessage("ERR066"), 1);
                }
                else
                {
                    throw ex;
                }

            }
        }
        #endregion

      
           
        #endregion

    }
}