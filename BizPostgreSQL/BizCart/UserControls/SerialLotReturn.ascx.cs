﻿using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BACRM.BusinessLogic.Opportunities;
using BACRM.BusinessLogic.Common;
using BACRM.BusinessLogic.Item;
using System.Data;
using BACRM.BusinessLogic.ShioppingCart;
using System.Text.RegularExpressions;
using System.Text;
namespace BizCart.UserControls
{
    public partial class SerialLotReturn : BizUserControl
    {
        string strColumn = string.Empty;
        int PageSize = 10;
        int PageIndex = 0;
        string DasboardTabs = "";
        long lngReturnHeaderID = 0;
        long lngReturnItemID = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                Sites.InitialiseSession();

                lngReturnHeaderID = Sites.ToLong(Request.QueryString["returnHeaderId"]);
                lngReturnItemID = Sites.ToLong(Request.QueryString["returnItemId"]);


                if (!IsPostBack)
                {
                    bindHtml();
                }

                //if (!string.IsNullOrEmpty(txtSortChar.Text))
                //{
                //    ViewState["SortChar"] = txtSortChar.Text;
                //    ViewState["Column"] = "BizDocName";
                //    Session["Asc"] = 0;
                //    BindDatagrid();
                //    txtSortChar.Text = "";
                //}
            }
            catch (Exception ex)
            {
                ExceptionModule.ExceptionPublish(ex, Sites.ToLong(Session["DomainID"]), Sites.ToLong(Session["UserContactID"]), Sites.ToLong(Session["SiteId"]), Request);
                ShowMessage(ex.ToString(), 1);
            }
        }


        public DataTable BindDatagrid()
        {
            try
            {
                DataSet ds = new DataSet();
                DataTable dtReturnDetails = new DataTable();
                ReturnHeader objOpp = new ReturnHeader();
                objOpp.DomainID = Sites.ToLong(Session["DomainID"]);
                objOpp.ReturnHeaderID = lngReturnHeaderID;
                objOpp.ReturnItemID = lngReturnItemID;

                ds = objOpp.GetRMASerializedIndItems();
                return ds.Tables[1];
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string ReturnName(System.DateTime bintCreatedDate)
        {
            try
            {
                string strCreateDate = null;
                strCreateDate = modBacrm.FormattedDateFromDate(bintCreatedDate, Sites.ToString(Session["DateFormat"]));
                return strCreateDate;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string ReturnDateTime(Object CloseDate)
        {
            try
            {
                string strTargetResolveDate = "";
                string temp = "";

                if (!(CloseDate == System.DBNull.Value))
                {
                    DateTime dtCloseDate = DateTime.Parse(CloseDate.ToString());
                    strTargetResolveDate = modBacrm.FormattedDateFromDate(dtCloseDate, Sites.ToString(Session["DateFormat"]));

                    string timePart = dtCloseDate.ToShortTimeString().Substring(0, dtCloseDate.ToShortTimeString().Length - 1);
                    // remove gaps
                    if (timePart.Split(' ').Length >= 2) timePart = timePart.Split(' ').GetValue(0).ToString() + timePart.Split(' ').GetValue(1).ToString();

                    // check Today date components [ Date , Month , Year ] with Parameter CloseDate components [ Date , Month , Year ] 
                    // if both are same it is today
                    if ((dtCloseDate.Date == DateTime.Now.Date & dtCloseDate.Month == DateTime.Now.Month & dtCloseDate.Year == DateTime.Now.Year))
                    {
                        strTargetResolveDate = "<font color=red><b>Today</b></font>";

                        return strTargetResolveDate;
                    }
                    // check [ TodayDate + 1 ] Date.... components [ Date , Month , Year ] with Parameter dtCloseDate components [ Date , Month , Year ] 
                    // if both are same it was Yesterday
                    else if ((dtCloseDate.Date.AddDays(1).Date == DateTime.Now.Date & dtCloseDate.AddDays(1).Month == DateTime.Now.Month & dtCloseDate.AddDays(1).Year == DateTime.Now.Year))
                    {
                        strTargetResolveDate = "<font color=purple><b>Yesterday</b></font>";

                        return strTargetResolveDate;
                    }
                    // check TodayDate .... components [ Date , Month , Year ] with Parameter [ dtCloseDate + 1 ] Date....  components [ Date , Month , Year ] 
                    // if both are same it will Tomorrow
                    else if ((dtCloseDate.Date == DateTime.Now.AddDays(1).Date & dtCloseDate.Month == DateTime.Now.AddDays(1).Month & dtCloseDate.Year == DateTime.Now.AddDays(1).Year))
                    {
                        temp = dtCloseDate.Hour.ToString() + ":" + dtCloseDate.Minute.ToString();
                        strTargetResolveDate = "<font color=orange><b>Tomorrow</b></font>";

                        return strTargetResolveDate;
                    }
                    // display day name for next 4 days from DateTime.Now

                    else if (dtCloseDate.ToString("yyyyMMdd") == DateTime.Now.AddDays(2).ToString("yyyyMMdd") | dtCloseDate.ToString("yyyyMMdd") == DateTime.Now.AddDays(3).ToString("yyyyMMdd") | dtCloseDate.ToString("yyyyMMdd") == DateTime.Now.AddDays(4).ToString("yyyyMMdd") | dtCloseDate.ToString("yyyyMMdd") == DateTime.Now.AddDays(5).ToString("yyyyMMdd"))
                    {
                        strTargetResolveDate = "<b>" + dtCloseDate.DayOfWeek.ToString() + "</b>";
                        return strTargetResolveDate;
                    }
                    else
                    {
                        //strTargetResolveDate = strTargetResolveDate;
                        return strTargetResolveDate;
                    }
                }
                return strTargetResolveDate;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public object ReturnMoney(System.Object Money)
        {
            try
            {
                return Math.Round(Sites.ToDecimal(Money.ToString()), 2);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        private void btnGo_Click(object sender, EventArgs e)
        {
            try
            {
                bindHtml();
            }
            catch (Exception ex)
            {
                ExceptionModule.ExceptionPublish(ex, Sites.ToLong(Session["DomainID"]), Sites.ToLong(Session["UserContactID"]), Sites.ToLong(Session["SiteId"]), Request);
                ShowMessage(ex.ToString(), 1);
            }
        }

        void bindHtml()
        {
            try
            {
                string strUI = GetUserControlTemplate("SerialLotReturn.htm");

                string pattern = "<##FOR(?<Property>(\\b[^>]*?))##>(?<Content>([\\s\\S]*?))<##ENDFOR##>";
                string CssClass = "", AltCssClass = "", RepeteTemplate = "";

                if (Regex.IsMatch(strUI, pattern))
                {
                    MatchCollection matches = Regex.Matches(strUI, pattern);
                    if ((matches.Count > 0))
                    {
                        string[] Property = matches[0].Groups["Property"].Value.Replace("(", "").Replace(")", "").Split(';');
                        RepeteTemplate = matches[0].Groups["Content"].Value;



                        if (Property.Length > 0)
                        {
                            for (int i = 0; i < Property.Length; i++)
                            {
                                string[] PropertyValue = Property[i].Split('=');

                                if (PropertyValue.Length == 2)
                                {
                                    switch (PropertyValue[0])
                                    {
                                        case "ItemClass":
                                            CssClass = PropertyValue[1].Trim();
                                            break;
                                        case "AlternatingItemClass":
                                            AltCssClass = PropertyValue[1].Trim();
                                            break;
                                    }
                                }
                            }
                        }
                        StringBuilder sb = new StringBuilder();
                        DataTable dt = BindDatagrid();
                        if (dt.Rows.Count > 0)
                        {


                            string strTemp = string.Empty;
                            int RowsCount = 0;

                            foreach (DataRow dr in dt.Rows)
                            {
                                strTemp = RepeteTemplate;
                                strTemp = strTemp.Replace("##LotName##", CCommon.ToString(dr["vcSerialNo"]));
                                strTemp = strTemp.Replace("##AvailableQty##", CCommon.ToString(dr["TotalQty"]));
                                strTemp = strTemp.Replace("##UsedQty##", "<input type=\"text\" class=\"form-control txtReturnQty\" numWareHouseItmsDTLID='" + CCommon.ToString(dr["numWareHouseItmsDTLID"]) + "' numWareHouseItemID='" + CCommon.ToString(dr["numWareHouseItemID"]) + "' onChange=\"valid('" + CCommon.ToString(dr["vcSerialNo"]) + "','" + CCommon.ToString(dr["TotalQty"]) + "')\" id=\"txtItem" + CCommon.ToString(dr["vcSerialNo"]) + "\" value=\"" + CCommon.ToString(dr["UsedQty"]) + "\" />");
                                if (RowsCount % 2 == 0)
                                    strTemp = strTemp.Replace("##RowClass##", AltCssClass);
                                else
                                    strTemp = strTemp.Replace("##RowClass##", CssClass);

                                sb.Append(strTemp);

                                RowsCount++;
                            }
                        }
                        strUI = strUI.Replace("##hiddenItemstoReturn##", CCommon.RenderControl(hdnReturnItem));
                        strUI = strUI.Replace("##RMASaveButton##", CCommon.RenderControl(btnSave));
                        strUI = strUI.Replace("##RMACancelButton##", CCommon.RenderControl(btnClose));
                        //strUI = strUI.Replace("##NoOfOrdersLabel##", lblRecordCount.Text);
                        //strUI = strUI.Replace("##PagerControl##", CCommon.RenderControl(AspNetPager1));
                        strUI = Regex.Replace(strUI, pattern, sb.ToString());
                    }
                }

                //Credit Term Replacement
                string SummaryPattern = "{##SummaryTable##}(?<Content>([\\s\\S]*?)){/##SummaryTable##}";


                pnlCutomizeHtml.Visible = false;
                litCutomizeHtml.Text = strUI;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        protected void AspNetPager1_PageChanged(object sender, EventArgs e)
        {
            try
            {
                bindHtml();
            }
            catch (Exception ex)
            {
                ExceptionModule.ExceptionPublish(ex, Sites.ToLong(Session["DomainID"]), Sites.ToLong(Session["UserContactID"]), Sites.ToLong(Session["SiteId"]), Request);
                ShowMessage(ex.ToString(), 1);
            }
        }
        private string BindTopDashboard(string strUI)
        {
            StringBuilder sbTopMenu = new StringBuilder();
            if (Sites.ToBool(Session["bitSalesOrderTabs"]) == true)
            {
                sbTopMenu.Append("<li class=\"SalesOrderTabs\"><a id=\"SalesOrderTabs\" href=\"Orders.aspx?flag=true\">" + Convert.ToString(Session["vcSalesOrderTabs"]) + "</a></li>");
            }
            if (Sites.ToBool(Session["bitSalesQuotesTabs"]) == true)
            {
                sbTopMenu.Append("<li class=\"SalesOpportunityTabs\"><a id=\"SalesOpportunityTabs\" href=\"SalesOpportunity.aspx\">" + Convert.ToString(Session["vcSalesQuotesTabs"]) + "</a></li>");
            }
            if (Sites.ToBool(Session["bitItemPurchaseHistoryTabs"]) == true)
            {
                sbTopMenu.Append("<li class=\"ItemPurchasedTabs\"><a id=\"ItemPurchasedTabs\" href=\"ItemPurchasedHistory.aspx\">" + Convert.ToString(Session["vcItemPurchaseHistoryTabs"]) + "</a></li>");
            }
            if (Sites.ToBool(Session["bitItemsFrequentlyPurchasedTabs"]) == true)
            {
                sbTopMenu.Append("<li class=\"WishListsTabs\"><a id=\"WishListsTabs\" href=\"Lists.aspx\">" + Convert.ToString(Session["vcItemsFrequentlyPurchasedTabs"]) + "</a></li>");
            }
            if (Sites.ToBool(Session["bitOpenCasesTabs"]) == true)
            {
                sbTopMenu.Append("<li class=\"OpenCasesTabs \"><a id=\"OpenCasesTabs\" href=\"OpenCases.aspx\">" + Convert.ToString(Session["vcOpenCasesTabs"]) + "</a></li>");
            }
            if (Sites.ToBool(Session["bitOpenRMATabs"]) == true)
            {
                sbTopMenu.Append("<li class=\"OpenRMATabs active\"><a id=\"OpenRMATabs\" href=\"OpenRMA.aspx\">" + Convert.ToString(Session["vcOpenRMATabs"]) + "</a></li>");
            }
            if (Sites.ToBool(Session["bitSupportTabs"]) == true)
            {
                sbTopMenu.Append("<li class=\"SupportTabs\"><a id=\"SupportTabs\" href=\"support.aspx\">" + Convert.ToString(Session["vcSupportTabs"]) + "</a></li>");
            }
            strUI = strUI.Replace("##TopMenuDasboard##", Convert.ToString(sbTopMenu));
            return strUI;
        }

        protected void btnReturnItem_Click(object sender, EventArgs e)
        {
           
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                string data = hdnReturnItem.Value;
                if (data.Length > 0)
                {
                    string[] dataValues = data.Split('#');
                    if (dataValues.Length > 0)
                    {
                        ReturnHeader objReturn = new ReturnHeader();
                        objReturn.DomainID = Sites.ToLong(Session["DomainId"]);
                        objReturn.ReturnHeaderID = lngReturnHeaderID;
                        objReturn.WareHouseItemID = Sites.ToLong(dataValues[0]);
                        objReturn.ReturnItemID = lngReturnItemID;
                        objReturn.Mode = 1;
                        objReturn.strItems = Sites.ToString(dataValues[1]);

                        objReturn.ManageRMALotSerial();
                        Response.Write("<script>window.opener.location.reload(true);window.close();</script>");
                    }
                }
            }
            catch
            {

            }
        }


    }
}