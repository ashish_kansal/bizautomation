﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using BACRM.BusinessLogic.Item;
using System.Data;
using BACRM.BusinessLogic.Opportunities;
using System.Collections;
using BACRM.BusinessLogic.Common;
using BACRM.BusinessLogic.Contacts;
using BACRM.BusinessLogic.Admin;
using BACRM.BusinessLogic.ShioppingCart;
using BACRM.BusinessLogic.Accounting;
using BACRM.BusinessLogic.Alerts;
using BACRM.BusinessLogic.Documents;
using System.Text.RegularExpressions;
using System.Text;

namespace BizCart.UserControls
{
    public partial class Checkout : BizUserControl
    {
        #region Global Declaration

        long lngDivID = 0;
        Boolean IsGoogleCheckoutConfigured = true;

        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                Sites.InitialiseSession();
                lngDivID = Sites.ToLong(Session["DivId"]);

                if (!IsPostBack)
                {
                    if (Sites.ToLong(Session["UserContactID"]) == 0 && Sites.ToInteger(Session["OnePageCheckout"]) == 0)
                    {
                        Response.Redirect("Login.aspx?ReturnURL=" + (Sites.ToString(Session["SiteLiveURL"]).Trim().Length > 2 ? "https://" : "http://") + Request.Url.Host + "/" + CheckoutPageName(), true);
                        // Response.Redirect("Login.aspx?ReturnURL=" + (Sites.ToString(Session["SiteLiveURL"]).Trim().Length > 2 ? "https://www." : "http://www.") + Request.Url.Host.ToLower().Replace("www.", "") + "/" + CheckoutPageName(), true);
                    }
                    if (Sites.ToString(Session["AddressConfirm"]) != "True" && Sites.ToInteger(Session["OnePageCheckout"]) == 0)
                    {
                        Response.Redirect("ConfirmAddress.aspx", true);
                    }
                    if (Sites.ToString(Session["ShippingMethodConfirmed"]) != "True" && Sites.ToInteger(Session["OnePageCheckout"]) == 0)
                    {
                        Response.Redirect("Cart.aspx", true);
                    }

                    if (CheckEmptyCart() == false) { return; }
                    if (lngDivID == 0) { return; }
                    //Relationship Mapping Validation
                    OppBizDocs objOpp = new OppBizDocs();
                    if (objOpp.ValidateARAP(lngDivID, 0, Sites.ToLong(Session["DomainID"])) == false)
                    {
                        ShowMessage(GetErrorMessage("ERR014"), 1);//Please Set AR and AP Relationship from Administration->Master List Admin-> Accounting/RelationShip Mapping To Save
                        bCharge.Visible = false;
                        return;
                    }
                    if (!IsPostBack)
                    {
                        txtCardNumber.Attributes.Add("onkeypress", "return CheckNumber(2,event);");
                        txtCardNumber.Attributes.Add("onkeyup", "javascript:Mask(this)");
                        CheckCreditStatus();
                        //if (Sites.ToBool(Session["EnableCreditCart"]) == false)
                        //{
                        //    tblCustomerInfo.Visible = false;
                        //}
                        int i = 0;
                        ArrayList Years = new ArrayList();
                        for (i = 1; i <= 20; i++)
                        {
                            Years.Add(DateTime.Now.AddYears(i - 1).Date.ToString("yy"));
                        }
                        ddlCardExpYear.DataSource = Years;
                        ddlCardExpYear.DataBind();
                        ddlCardExpYear.Items.FindByValue(DateTime.Now.Year.ToString("yy"));
                        ddlCardExpMonth.SelectedIndex = (DateTime.Now.Month - 1);

                        BindCardType();
                        LoadDefaultCard();

                        bindHtml();
                    }
                }
                if (IsPostBack)
                {
                    if (CheckEmptyCart() == true)
                    {
                        if (Sites.ToString(Request["__EVENTTARGET"]) == "BillMe" || Sites.ToString(Request["__EVENTTARGET"]) == "PayByCard" || Sites.ToString(Request["__EVENTTARGET"]) == "GoogleCheckout")
                        {
                            PaymentModeChanged();
                        }
                        bindHtml();
                    }
                    else 
                    {
                        Response.Redirect("Home.aspx");  
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionModule.ExceptionPublish(ex, Sites.ToLong(Session["DomainID"]), Sites.ToLong(Session["UserContactID"]), Sites.ToLong(Session["SiteId"]), Request);
                ShowMessage(ex.ToString(), 1);
            }
        }
        protected void bCharge_Click(object sender, EventArgs e)
        {
            try
            {
                if (CheckEmptyCart() == false) { return; }

                if (Sites.ToString(Session["PayOption"]) == "CreditCard") //1
                {
                    //if (Sites.ToLong(Session["CreditCardBizDocID"]) == 0)
                    //{ ShowMessage(GetErrorMessage("ERR015"), 1); return; }//Can not checkout, selected payment method is not associated with any bizdoc.

                    if (Sites.ToLong(ddlCardType.SelectedValue) == 0)
                    { ShowMessage(GetErrorMessage("ERR016"), 1); return; }//Please select valid card type.

                    SaveCustomerCreditCardInfo();

                    Session["PayOption"] = "CreditCard";
                    if (Sites.ToInteger(Request["hdnOnePageCheckout"]) == 1)
                    {
                        Session["SelectedStep"] = Sites.ToInteger(Session["SelectedStep"]) + 1;
                        return;
                    }
                    else
                    {
                        Response.Redirect("/OrderSummary.aspx");
                    }
                }
                else if (Sites.ToString(Session["PayOption"]) == "BillMe")
                {
                    //if (Sites.ToLong(Session["CreditTermBizDocID"]) == 0)
                    //{ ShowMessage(GetErrorMessage("ERR015"), 1); return; }//Can not checkout, selected payment method is not associated with any bizdoc.

                    decimal totalDue = default(decimal);
                    totalDue = Sites.ToDecimal(txtDueAmount.Text) + Sites.ToDecimal(Session["TotalAmount"]);
                    if (totalDue > Sites.ToDecimal(txtCredit.Text))
                    {
                        ShowMessage(GetErrorMessage("ERR017"), 0);//You have exceeded the maximum amount you can place on a bill-to account by 'whatever the amount is' Please reduce your total amount to compensate, or if you prefer, execute the order by paying with a check.
                    }
                    else
                    {
                        Session["PayOption"] = "BillMe";
                        if (Sites.ToInteger(Request["hdnOnePageCheckout"]) == 1)
                        {
                            Session["SelectedStep"] = Sites.ToInteger(Session["SelectedStep"]) + 1;
                            return;
                        }
                        else
                            Response.Redirect("/OrderSummary.aspx");
                    }
                }
                else if (Sites.ToString(Session["PayOption"]) == "GoogleCheckout")
                {
                    UserAccess objUserAccess = new UserAccess();
                    objUserAccess.DomainID = Sites.ToLong(Session["DomainID"]);
                    objUserAccess.SiteID = Sites.ToLong(Session["SiteId"]);
                    string strGoogleMerchantID = "";
                    string strGoogleMerchantKey = "";
                    DataTable dtECommerceDetail = objUserAccess.GetECommerceDetails();

                    if (dtECommerceDetail != null)
                    {
                        strGoogleMerchantID = CCommon.ToString(dtECommerceDetail.Rows[0]["vcGoogleMerchantID"]);
                        strGoogleMerchantKey = CCommon.ToString(dtECommerceDetail.Rows[0]["vcGoogleMerchantKey"]);
                    }
                    else
                    {
                        strGoogleMerchantID = "";
                        strGoogleMerchantKey = "";
                    }

                    if (strGoogleMerchantID != "" && strGoogleMerchantKey != "")
                    {
                        //if (Sites.ToLong(Session["CreditTermBizDocID"]) == 0)
                        //{ ShowMessage(GetErrorMessage("ERR015"), 1); return; }//Can not checkout, selected payment method is not associated with any bizdoc.

                        Session["PayOption"] = "GoogleCheckout";
                        if (Sites.ToInteger(Request["hdnOnePageCheckout"]) == 1)
                        {
                            Session["SelectedStep"] = Sites.ToInteger(Session["SelectedStep"]) + 1;
                            return;
                        }
                        else
                            Response.Redirect("/OrderSummary.aspx");

                    }
                    else
                    {
                        IsGoogleCheckoutConfigured = false;
                        ShowMessage(GetErrorMessage("ERR061"), 1);
                    }
                }
                else
                {
                    Page.MaintainScrollPositionOnPostBack = false;
                    //Page.ClientScript.RegisterStartupScript(Page.ClientScript.GetType(), Page.ClientID, "resetScrollPosition();", true);
                    ShowMessage(GetErrorMessage("ERR018"), 1);
                }

                bindHtml();
            }
            catch (Exception ex)
            {
                ExceptionModule.ExceptionPublish(ex, Sites.ToLong(Session["DomainID"]), Sites.ToLong(Session["UserContactID"]), Sites.ToLong(Session["SiteId"]), Request);
                ShowMessage(ex.ToString(), 1);
            }
        }
        protected void Page_PreRender(object sender, EventArgs e)
        {
            try
            {
                if(CheckEmptyCart())
                {
                    bindHtml();    
                }
                
            }
            catch (Exception ex)
            {
                ExceptionModule.ExceptionPublish(ex, Sites.ToInteger(Session["DomainID"]), Sites.ToInteger(Session["UserContactID"]), Sites.ToInteger(Session["SiteId"]), Request);
                ShowMessage(ex.ToString(), 1);
            }
        }

        #endregion

        #region Methods

        #region HTML
        private void bindHtml()
        {
            try
            {
                string strUI = GetUserControlTemplate("Checkout.htm");

                strUI = strUI.Replace("##TotalBalanceDue##", lblBalDue.Text);
                strUI = strUI.Replace("##TotalRemaniningCredit##", lblRemCredit.Text);
                strUI = strUI.Replace("##TotalAmountPastDue##", lblAmtPastDue.Text);

                strUI = strUI.Replace("##CardHolderTextBox##", CCommon.RenderControl(txtCHName));
                strUI = strUI.Replace("##CardNumberTextBox##", CCommon.RenderControl(txtCardNumber));
                strUI = strUI.Replace("##CardTypeDropDownList##", CCommon.RenderControl(ddlCardType));
                strUI = strUI.Replace("##CardExpMonthDropDownList##", CCommon.RenderControl(ddlCardExpMonth));
                strUI = strUI.Replace("##CardExpYearDropDownList##", CCommon.RenderControl(ddlCardExpYear));
                strUI = strUI.Replace("##CVV2TextBox##", CCommon.RenderControl(txtCardCVV2));
                strUI = strUI.Replace("##TotalAmount##", Sites.ToString(Session["CurrSymbol"]) +" "+  string.Format("{0:#,##0.00}", Sites.ToDecimal(Session["TotalAmount"])));


                strUI = strUI.Replace("##SubmitButton##", CCommon.RenderControl(bCharge));

                strUI = strUI.Replace("##IsBillMeChecked##", Sites.ToString(Session["PayOption"]) == "BillMe" ? "checked=\"checked\"" : "");
                strUI = strUI.Replace("##showbillme##", Sites.ToString(Session["PayOption"]) == "BillMe" ? "" : "display:none");
                strUI = strUI.Replace("##IsPayChecked##", Sites.ToString(Session["PayOption"]) == "CreditCard" ? "checked=\"checked\"" : "");
                strUI = strUI.Replace("##showpaybycreditcard##", Sites.ToString(Session["PayOption"]) == "CreditCard" ? "" : "display:none");
                if (IsGoogleCheckoutConfigured)
                {
                    strUI = strUI.Replace("##IsGoogleCheckoutChecked##", Sites.ToString(Session["PayOption"]) == "GoogleCheckout" ? "checked=\"checked\"" : "");
                }
                else
                {
                    strUI = strUI.Replace("##IsGoogleCheckoutChecked##", "");
                }

                pnlCutomizeHtml.Visible = false;
                litCutomizeHtml.Text = strUI;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        private void PaymentModeChanged()
        {
            try
            {
                if (Sites.ToString(Request["PayOption"]) == "BillMe")
                {
                    //radBillMe.Visible = true;
                    Session["PayOption"] = "BillMe"; 
                    tblCustomerInfo.Visible = false;
                    tblBillme.Visible = true;
                }
                else if (Sites.ToString(Request["PayOption"]) == "CreditCard")
                {
                    //radPay.Visible = true;
                    Session["PayOption"] = "CreditCard";
                    tblCustomerInfo.Visible = true;
                    tblBillme.Visible = false;
                }
                else if (Sites.ToString(Request["PayOption"]) == "GoogleCheckout")
                {
                    Session["PayOption"] = "GoogleCheckout";

                }
                else if (Sites.ToString(Request["PayOption"]) == "SaleInquiry")
                {
                    Session["PayOption"] = "SaleInquiry";
                    tblCustomerInfo.Visible = false;
                    tblBillme.Visible = false;
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private void CheckCreditStatus()
        {
            try
            {
                CItems objItems = new CItems();
                double dblCredit = 0;
                double dblRemainingCredit = 0;
                objItems.DivisionID = lngDivID;
                dblCredit = objItems.GetCreditStatusofCompany();

                DataTable dtTable = default(DataTable);
                dtTable = objItems.GetAmountDue();
                lblBalDue.Text = Sites.ToString(Session["CurrSymbol"]) + " " + string.Format("{0:#,##0.00}",CCommon.ToDecimal(CCommon.ToString(CCommon.ToDecimal(dtTable.Rows[0]["AmountDueSO"]) / CCommon.ToDecimal(CCommon.ToString(Session["ExchangeRate"]))))); 
                lblRemCredit.Text = Sites.ToString(Session["CurrSymbol"]) + " " + string.Format("{0:#,##0.00}", CCommon.ToDecimal(CCommon.ToString(CCommon.ToDecimal(dtTable.Rows[0]["RemainingCredit"]) / CCommon.ToDecimal(CCommon.ToString(Session["ExchangeRate"])))));// string.Format("{0:#,##0.00}", dtTable.Rows[0]["RemainingCredit"]);
                lblAmtPastDue.Text = Sites.ToString(Session["CurrSymbol"]) + " " + string.Format("{0:#,##0.00}", CCommon.ToDecimal(CCommon.ToString(CCommon.ToDecimal(dtTable.Rows[0]["AmountPastDueSO"]) / CCommon.ToDecimal(CCommon.ToString(Session["ExchangeRate"]))))); 

                dblRemainingCredit = Sites.ToDouble(dtTable.Rows[0]["RemainingCredit"]);

                txtCredit.Text = dblCredit.ToString();
                txtDueAmount.Text = dtTable.Rows[0]["AmountDueSO"].ToString();
                //}
                PaymentModeChanged();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private void BindCardType()
        {
            try
            {

                CCommon objCommon = new CCommon();
                objCommon.sb_FillComboFromDB(ref ddlCardType, 120, Sites.ToLong(Session["DomainID"]));
                //objCommon.DomainID = Sites.ToLong(Session["DomainID"]);
                //objCommon.tinyOrder = 0;
                //ddlCardType.DataTextField = "vcData";
                //ddlCardType.DataValueField = "numListItemID";
                //ddlCardType.DataSource = objCommon.GetCardTypes();
                //ddlCardType.DataBind();
                //ddlCardType.Items.Insert(0, "--Select One--");
                //ddlCardType.Items.FindByText("--Select One--").Value = "0";
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private void LoadDefaultCard()
        {
            try
            {
                QueryStringValues objEncryption = new QueryStringValues();
                OppInvoice objoppinvoice = new OppInvoice();
                objoppinvoice.bitflag = true;
                objoppinvoice.DomainID = CCommon.ToInteger(Session["DomainId"]);
                objoppinvoice.IsDefault = true;
                objoppinvoice.UserCntID = CCommon.ToLong(Session["UserContactID"]);
                DataSet ds = objoppinvoice.GetCustomerCreditCardInfo();
                if (ds != null && ds.Tables.Count > 0)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        txtCHName.Text = objEncryption.Decrypt(CCommon.ToString(ds.Tables[0].Rows[0]["vcCardHolder"]));
                        hdnCardNumber.Value = objEncryption.Decrypt(CCommon.ToString(ds.Tables[0].Rows[0]["vcCreditCardNo"]));
                        if (hdnCardNumber.Value.Length > 12)
                        {
                            txtCardNumber.Text = "############" + hdnCardNumber.Value.Substring(12, hdnCardNumber.Value.Length - 12); // objEncryption.Decrypt(CCommon.ToString(ds.Tables[0].Rows[0]["vcCreditCardNo"]));
                        }
                        //txtCardCVV2.Text = objEncryption.Decrypt(CCommon.ToString(ds.Tables[0].Rows[0]["vcCVV2"]));
                        ddlCardType.ClearSelection();
                        if (ddlCardType.Items.FindByValue(CCommon.ToString(ds.Tables[0].Rows[0]["numCardTypeId"])) != null)
                        {
                            ddlCardType.Items.FindByValue(CCommon.ToString(ds.Tables[0].Rows[0]["numCardTypeId"])).Selected = true;
                        }
                        ddlCardExpYear.ClearSelection();
                        ddlCardExpMonth.ClearSelection();
                        if (ddlCardExpMonth.Items.FindByValue(CCommon.ToString(ds.Tables[0].Rows[0]["tintValidMonth"]).PadLeft(2, '0')) != null)
                        {
                            ddlCardExpMonth.Items.FindByValue(CCommon.ToString(ds.Tables[0].Rows[0]["tintValidMonth"]).PadLeft(2, '0')).Selected = true;
                        }
                        if (ddlCardExpYear.Items.FindByValue(CCommon.ToString(ds.Tables[0].Rows[0]["intValidYear"])) != null)
                        {
                            ddlCardExpYear.Items.FindByValue(CCommon.ToString(ds.Tables[0].Rows[0]["intValidYear"])).Selected = true;
                        }
                        //chkDefault.Checked = CCommon.ToBool(ds.Tables[0].Rows[0]["bitIsDefault"]);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }


        }
        private void SaveCustomerCreditCardInfo()
        {
            try
            {
                //if (CCommon.ToBool(Session["SaveCreditCardInfo"]))
                //{
                QueryStringValues objEncryption = new QueryStringValues();
                OppInvoice objOppInvoice = new OppInvoice();

                objOppInvoice.ContactID = Sites.ToLong(Session["UserContactID"]);
                objOppInvoice.CardHolder = objEncryption.Encrypt(txtCHName.Text.Trim());
                objOppInvoice.CardTypeID = CCommon.ToLong(ddlCardType.SelectedValue);
                objOppInvoice.CreditCardNumber = objEncryption.Encrypt(hdnCardNumber.Value.Trim());
                objOppInvoice.CVV2 = objEncryption.Encrypt(txtCardCVV2.Text.Trim());
                objOppInvoice.ValidMonth = short.Parse(ddlCardExpMonth.SelectedValue);
                objOppInvoice.ValidYear = short.Parse(ddlCardExpYear.SelectedValue);
                objOppInvoice.UserCntID = Sites.ToLong(Session["UserContactID"]);
                objOppInvoice.IsDefault = true;
                objOppInvoice.AddCustomerCreditCardInfo();
                //}
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion
    }
}

