﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="support.ascx.cs"
    Inherits="BizCart.UserControls.Support" %>
<%@ Register Assembly="AspNetPager, Version=7.4.4.0, Culture=neutral, PublicKeyToken=fb0a0fe055d40fd4" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>
<script language="javascript">
    function OpenBizInvoice(a, b , c,d,e) {
        window.open('Invoice.aspx?Show=True&OppID=' + a + '&RefType=' + b + '&PrintMode=' + c+'&InvoiceType=' + d + '&BizDocId='+e,'', 'toolbar=0,titlebar=0,menubar=0,location=1,left=100,top=50,width=750,height=800,scrollbars=yes,resizable=yes');
    }
    function OpenCustomerStatement() {
        window.open('customerstatement.aspx', '', 'toolbar=no,titlebar=no,top=10,left=100,width=1000,height=800,scrollbars=yes,resizable=yes')
        return false;
    }
    function OpenShippingStatus(companyId,trackingno) {
        window.open('ShippingStatus.aspx?companyId=' + companyId + '&trackingno=' + trackingno + '', '', 'toolbar=no,titlebar=no,top=10,left=100,width=300,height=400,scrollbars=yes,resizable=yes')
        return false;
    }
</script>

<asp:Panel ID="pnlCutomizeHtml" runat="server">
    <table align="right" runat="server" id="tblCreditTerm">
        <tr>
            <td class="LabelColumn">
                Total Balance Due :
            </td>
            <td class="ControlCell">
                <asp:Label ID="lblBalDue" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="LabelColumn">
                Total Remaining Credit :
            </td>
            <td class="ControlCell">
                <asp:Label ID="lblRemCredit" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="LabelColumn">
                Total Amount Past Due :
            </td>
            <td class="ControlCell">
                <asp:Label ID="lblAmtPastDue" runat="server"></asp:Label>
            </td>
        </tr>
        
    </table>
    <table cellspacing="0" cellpadding="0" width="100%" border="0">
        <tr>
            <td>
                <div class="sectionheader">
                    Support
                </div>
            </td>
        </tr>
         <tr>
            <td class="LabelColumn" align="right">

            </td>
        </tr>
        <tr>
            <td class="LabelColumn" align="right">
          
            </td>
        </tr>
          <tr>
            <td align="right" align="left">

            </td>
            <td align="right">
               
                <asp:TextBox runat="server" ID="txtSupportSearch"></asp:TextBox>
                <asp:Button runat="server" Text="Search" OnClick="btnSupportSearch_Click" ID="btnSupportSearch"></asp:Button>
            </td>
            <td>&nbsp;
            </td>
        </tr>
        <tr>
            <td class="Grid1">
                 <asp:DropDownList id="ddlCategory" OnSelectedIndexChanged="ddlCategory_SelectedIndexChanged" AutoPostBack="true" runat="server"></asp:DropDownList>
            </td>
        </tr>
    </table>
</asp:Panel>
<asp:Literal ID="litCutomizeHtml" runat="server"></asp:Literal>
<asp:TextBox ID="txtTotalPage" runat="server" Style="display: none"></asp:TextBox>
<asp:TextBox ID="txtTotalRecords" runat="server" Style="display: none"></asp:TextBox>
<asp:TextBox ID="txtSortChar" runat="server" Style="display: none"></asp:TextBox>
<asp:Button runat="server" ID="Button1" Text="" Style="display: none"></asp:Button>
