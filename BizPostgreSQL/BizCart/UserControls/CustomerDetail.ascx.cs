﻿using System;
using System.Web;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using BACRM.BusinessLogic.Common;
using BACRM.BusinessLogic.Contacts;
using BACRM.BusinessLogic.Admin;
using BACRM.BusinessLogic.ShioppingCart;
using BACRM.BusinessLogic.Item;
namespace BizCart.UserControls
{
    public partial class CustomerDetail : BizUserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                Sites.InitialiseSession();
                if (Sites.ToLong(Session["UserContactID"]) == 0 && Sites.ToInteger(Session["OnePageCheckout"]) == 0)
                {
                    Response.Redirect("Login.aspx?ReturnURL=ConfirmAddress.aspx", true);
                }
                if (!IsPostBack)
                {
                    int ErrorCode = 0;
                    if (Request.QueryString["errcode"] != null)
                    {
                        ErrorCode = CCommon.ToInteger(Request.QueryString["errcode"]);
                        if (ErrorCode > 0)
                        {
                            CustomErrorMessage(ErrorCode);
                        }
                    }

                    CCommon objCommon = new CCommon();
                    objCommon.sb_FillComboFromDBwithSel(ref ddlBillCountry, 40, Sites.ToLong(Session["DomainID"]));
                    objCommon.sb_FillComboFromDBwithSel(ref ddlShipCountry, 40, Sites.ToLong(Session["DomainID"]));

                    BindAddressName();
                    bindHtml();
                    
                }
                if (IsPostBack)
                {
                    if (Sites.ToString(Request["__EVENTTARGET"]) == "chkShippingAdd")
                    {
                        chkShippingAdd.Checked = Sites.ToBool(Request["__EVENTARGUMENT"]);
                        chkShippingAdd_CheckedChanged();
                        bindHtml();
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionModule.ExceptionPublish(ex, Sites.ToLong(Session["DomainID"]), Sites.ToLong(Session["UserContactID"]), Sites.ToLong(Session["SiteId"]), Request);
                ShowMessage(ex.ToString(), 1);
            }
        }
        /// <summary>
        /// Will be called dynamically from one page checkout
        /// </summary>
        //public void PageLoad()
        //{
        //    Sites.InitialiseSession();
        //    if (Sites.ToLong(Session["UserContactID"]) == 0 && Sites.ToInteger(Session["OnePageCheckout"]) == 0)
        //    {
        //        Response.Redirect("Login.aspx?ReturnURL=ConfirmAddress.aspx", true);
        //    }
        //    if (!IsPostBack)
        //    {
        //        CCommon objCommon = new CCommon();
        //        objCommon.sb_FillComboFromDBwithSel(ref ddlBillCountry, 40, Sites.ToLong(Session["DomainID"]));
        //        objCommon.sb_FillComboFromDBwithSel(ref ddlShipCountry, 40, Sites.ToLong(Session["DomainID"]));

        //        BindAddressName();
        //        bindHtml();

        //    }
        //    if (IsPostBack)
        //    {
        //        if (Sites.ToString(Request["__EVENTTARGET"]) == "chkShippingAdd")
        //        {
        //            chkShippingAdd.Checked = Sites.ToBool(Request["__EVENTARGUMENT"]);
        //            chkShippingAdd_CheckedChanged();
        //            bindHtml();
        //        }
        //    }
        //}

        private void CustomErrorMessage(int ErrorCode)
        {
            switch (ErrorCode)
            {
                case 6:
                    ShowMessage("Ship To City is not provided!., Please update Ship To City.", 1);
                    break;

                case 7:
                    ShowMessage("Ship To State is not provided!., Please update Ship To State.", 1);
                    break;

                case 8:
                    ShowMessage("Ship To Postal Code is not provided!., Please update Ship To Postal Code.", 1);
                    break;

                case 9:
                    ShowMessage("Ship To Country is not provided!., Please update Ship To Country.", 1);
                    break;

                case 10:
                    ShowMessage("Bill To City is not provided!., Please update Bill To City.", 1);
                    break;

                case 11:
                    ShowMessage("Bill To State is not provided!., Please update Bill To State.", 1);
                    break;

                case 12:
                    ShowMessage("Bill To Postal Code is not provided!., Please update Bill To Postal Code.", 1);
                    break;

                case 13:
                    ShowMessage("Bill To Country is not provided!., Please update Bill To Country.", 1);
                    break;

                default:
                    ShowMessage("Please update Customer Address Details.", 1);
                    break;
            }
        }

        private void BindAddressName()
        {
            try
            {
                CContacts objContact = new CContacts();
                objContact.AddressType = CContacts.enmAddressType.BillTo;
                objContact.DomainID = Sites.ToLong(Session["DomainID"]);
                objContact.RecordID = Sites.ToLong(Session["DivId"]);
                objContact.AddresOf = CContacts.enmAddressOf.Organization;
                objContact.byteMode = 2;
                ddlBillAddressName.DataValueField = "numAddressID";
                ddlBillAddressName.DataTextField = "vcAddressName";
                ddlBillAddressName.DataSource = objContact.GetAddressDetail();
                ddlBillAddressName.DataBind();
                ddlBillAddressName.Items.Insert(0, new ListItem("--Select One--", "0"));

                if (ddlBillAddressName.Items.Count > 1) { ddlBillAddressName.SelectedIndex = 1; }

                objContact.AddressType = CContacts.enmAddressType.ShipTo;
                objContact.DomainID = Sites.ToLong(Session["DomainID"]);
                objContact.RecordID = Sites.ToLong(Session["DivId"]);
                objContact.AddresOf = CContacts.enmAddressOf.Organization;
                objContact.byteMode = 2;
                ddlShipAddressName.DataValueField = "numAddressID";
                ddlShipAddressName.DataTextField = "vcAddressName";
                ddlShipAddressName.DataSource = objContact.GetAddressDetail();
                ddlShipAddressName.DataBind();
                ddlShipAddressName.Items.Insert(0, new ListItem("--Select One--", "0"));

                if (ddlShipAddressName.Items.Count > 1) { ddlShipAddressName.SelectedIndex = 1; }
                /*Show Add new address for first time.. second time show edit address*/
                DataTable dtTable;
                objContact.DomainID = Sites.ToLong(Session["DomainID"]);
                objContact.AddressID = Sites.ToLong(ddlBillAddressName.SelectedValue);
                objContact.byteMode = 1;
                dtTable = objContact.GetAddressDetail();

                if (dtTable.Rows.Count > 0)
                {
                    if (Sites.ToString(dtTable.Rows[0]["vcStreet"]).Length < 2)
                    {
                        trFirstTime.Visible = true;
                        trEditAddress.Visible = false;
                    }
                    else
                    {
                        trFirstTime.Visible = false;
                        trEditAddress.Visible = true;
                    }
                }
                else
                {
                    trFirstTime.Visible = true;
                    trEditAddress.Visible = false;
                }
                hfEditAddress.Value = trEditAddress.Visible.ToString();
                hfFirstTime.Value = trFirstTime.Visible.ToString();
                if (trEditAddress.Visible == true) { LoadAddress(); }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
       
        public void LoadAddress()
        {
            try
            {
                DataTable dtTable = null;
                CContacts objContact = new CContacts();
                objContact.DomainID = Sites.ToLong(Session["DomainID"]);
                objContact.AddressID = Sites.ToLong(ddlBillAddressName.SelectedValue);
                objContact.byteMode = 1;
                dtTable = objContact.GetAddressDetail();

                if (dtTable.Rows.Count > 0)
                {
                    lblBillAddress.Text = Sites.ToString(dtTable.Rows[0]["vcFullAddress"]);
                    lbBillEdit.Visible = true;
                }
                else
                {
                    lblBillAddress.Text = "";
                    lbBillEdit.Visible = false;
                }

                objContact.DomainID = Sites.ToLong(Session["DomainID"]);
                objContact.AddressID = Sites.ToLong(ddlShipAddressName.SelectedValue);
                objContact.byteMode = 1;
                dtTable = objContact.GetAddressDetail();

                if (dtTable.Rows.Count > 0)
                {
                    lblShipAddress.Text = Sites.ToString(dtTable.Rows[0]["vcFullAddress"]);
                    lbShipEdit.Visible = true;

                    Session["UserIPZipcode"] = CCommon.ToString(dtTable.Rows[0]["vcPostalCode"]);
                    Session["UserIPCountryID"] = Sites.ToLong(dtTable.Rows[0]["numCountry"]);
                    Session["UserIPStateID"] = Sites.ToLong(dtTable.Rows[0]["numState"]);
                }
                else
                {
                    lblShipAddress.Text = "";
                    lbShipEdit.Visible = false;

                    Session["UserIPZipcode"] = "";
                    Session["UserIPCountryID"] = "";
                    Session["UserIPStateID"] = "";
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            if (CheckPageValidate())
            {
                try
                {
                    save();
                    Session["ShipAddress"] = ddlShipAddressName.SelectedValue;
                    Session["BillAddress"] = ddlBillAddressName.SelectedValue;

                    Session["AddressConfirm"] = "True";

                    if (Sites.ToInteger(Request["hdnOnePageCheckout"]) == 1)
                    {
                        Session["SelectedStep"] = Sites.ToInteger(Session["SelectedStep"]) + 1;
                        return;
                    }
                    if (Sites.ToString(Request["Return"]).Length > 0)
                    {
                        Response.Redirect(Sites.ToString(Request["Return"]), true);
                    }
                    else if (Sites.ToString(Request["ReturnURL"]).Length > 0)
                    {
                        Response.Redirect(Sites.ToString(Request["ReturnURL"]), true);
                    }
                    
                    Response.Redirect((Sites.ToString(Session["SiteLiveURL"]).Trim().Length > 2 ? "https://" : "http://") + Request.Url.Host + "/Cart.aspx");
                }
                catch (Exception ex)
                {
                    ExceptionModule.ExceptionPublish(ex, Sites.ToLong(Session["DomainID"]), Sites.ToLong(Session["UserContactID"]), Sites.ToLong(Session["SiteId"]), Request);
                    ShowMessage(ex.ToString(), 1);
                }

            }
            bindHtml();
        }

        public void save()
        {
            try
            {

                if (hfFirstTime.Value == "True")
                {
                    CContacts objContacts = new CContacts();
                    objContacts.AddressID = Sites.ToLong(ddlBillAddressName.SelectedValue);
                    objContacts.AddressName = "Billing Address";
                    objContacts.Street = txtBillStreet.Text.Trim();
                    objContacts.City = txtBillCity.Text.Trim();
                    objContacts.Country = Sites.ToLong(ddlBillCountry.SelectedItem.Value);
                    objContacts.PostalCode = txtBillCode.Text.Trim();
                    objContacts.State = Sites.ToLong(ddlBillState.SelectedItem.Value);
                    objContacts.AddresOf = CContacts.enmAddressOf.Organization;
                    objContacts.AddressType = CContacts.enmAddressType.BillTo;
                    objContacts.IsPrimaryAddress = true;
                    objContacts.RecordID = Sites.ToLong(Session["DivId"]);
                    objContacts.DomainID = Sites.ToLong(Session["DomainID"]);
                    objContacts.ManageAddress();

                    objContacts.AddressID = Sites.ToLong(ddlShipAddressName.SelectedValue);
                    objContacts.AddressName = "Shipping Address";
                    objContacts.Street = txtShipStreet.Text.Trim();
                    objContacts.City = txtShipCity.Text.Trim();
                    objContacts.Country = Sites.ToLong(ddlShipCountry.SelectedItem.Value);
                    objContacts.PostalCode = txtShipCode.Text.Trim();
                    objContacts.State = Sites.ToLong(ddlShipState.SelectedItem.Value);
                    objContacts.AddresOf = CContacts.enmAddressOf.Organization;
                    objContacts.AddressType = CContacts.enmAddressType.ShipTo;
                    objContacts.IsPrimaryAddress = true;
                    objContacts.RecordID = Sites.ToLong(Session["DivId"]);
                    objContacts.DomainID = Sites.ToLong(Session["DomainID"]);
                    objContacts.ManageAddress();

                    Session["UserIPZipcode"] = txtShipCode.Text.Trim();
                    Session["UserIPCountryID"] = Sites.ToLong(ddlShipCountry.SelectedValue);
                    Session["UserIPStateID"] = Sites.ToLong(ddlShipState.SelectedValue);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool CheckPageValidate()
        {
            if (hfFirstTime.Value == "False")
            {
                if (ddlBillAddressName.SelectedIndex == 0)
                {
                    ShowMessage(GetErrorMessage("ERR026"), 1);//Select Billing Address
                    return false;
                }
                else if (ddlShipAddressName.SelectedIndex == 0)
                {
                    ShowMessage(GetErrorMessage("ERR027"), 1);//Select Shipping Address
                    return false;
                }

                DataTable dtTable = null;
                CContacts objContact = new CContacts();
                objContact.DomainID = Sites.ToLong(Session["DomainID"]);
                objContact.AddressID = Sites.ToLong(ddlBillAddressName.SelectedValue);
                objContact.byteMode = 1;
                dtTable = objContact.GetAddressDetail();

                if (dtTable.Rows.Count > 0)
                {
                    if (String.IsNullOrEmpty(Sites.ToString(dtTable.Rows[0]["vcCity"])) == true)
                    {
                        CustomErrorMessage(10);
                        return false;
                    }
                    else if (String.IsNullOrEmpty(Sites.ToString(dtTable.Rows[0]["numState"])) == true)
                    {
                        CustomErrorMessage(11);
                        return false;
                    }
                    else if (String.IsNullOrEmpty(Sites.ToString(dtTable.Rows[0]["vcPostalCode"])) == true)
                    {
                        CustomErrorMessage(12);
                        return false;
                    }
                    else if (String.IsNullOrEmpty(Sites.ToString(dtTable.Rows[0]["numCountry"])) == true)
                    {
                        CustomErrorMessage(13);
                        return false;
                    }
                }
                else
                {
                    CustomErrorMessage(0);
                }

                objContact.DomainID = Sites.ToLong(Session["DomainID"]);
                objContact.AddressID = Sites.ToLong(ddlShipAddressName.SelectedValue);
                objContact.byteMode = 1;
                dtTable = objContact.GetAddressDetail();

                if (dtTable.Rows.Count > 0)
                {
                    if (String.IsNullOrEmpty(Sites.ToString(dtTable.Rows[0]["vcCity"])) == true)
                    {
                        CustomErrorMessage(6);
                        return false;
                    }
                    else if (Sites.ToLong(dtTable.Rows[0]["numState"]) <=0 )
                    {
                        CustomErrorMessage(7);
                        return false;
                    }
                    else if (String.IsNullOrEmpty(Sites.ToString(dtTable.Rows[0]["vcPostalCode"])) == true)
                    {
                        CustomErrorMessage(8);
                        return false;
                    }
                    else if (Sites.ToLong(dtTable.Rows[0]["numCountry"]) <= 0)
                    {
                        CustomErrorMessage(9);
                        return false;
                    }
                }
                else
                {
                    CustomErrorMessage(0);
                }

            }
            else
            {
                if (txtBillStreet.Text.Trim().Length <= 2)
                {
                    ShowMessage(GetErrorMessage("ERR028"), 1);//Enter Bill to Street
                    return false;
                }
                else if (txtBillCity.Text.Trim().Length == 0)
                {
                    ShowMessage(GetErrorMessage("ERR029"), 1);//Enter Bill to City
                    return false;
                }
                else if (txtBillCode.Text.Trim().Length == 0)
                {
                    ShowMessage(GetErrorMessage("ERR030"), 1);//Enter Bill to Zip Code
                    return false;
                }
                else if (ddlBillCountry.SelectedIndex == 0)
                {
                    ShowMessage(GetErrorMessage("ERR031"), 1);//Select Bill to Country
                    return false;
                }
                else if (ddlBillState.SelectedIndex == 0)
                {
                    ShowMessage(GetErrorMessage("ERR032"), 1);//Select Bill to State
                    return false;
                }

                //shiping validation
                if (txtShipStreet.Text.Trim().Length <= 2)
                {
                    ShowMessage(GetErrorMessage("ERR033"), 1);//Enter Ship to Street
                    return false;
                }
                else if (txtShipCity.Text.Trim().Length == 0)
                {
                    ShowMessage(GetErrorMessage("ERR034"), 1);//Enter Ship to City
                    return false;
                }
                else if (txtShipCode.Text.Trim().Length == 0)
                {
                    ShowMessage(GetErrorMessage("ERR035"), 1);//Enter Ship to Zip Code
                    return false;
                }
                else if (ddlShipCountry.SelectedIndex == 0)
                {
                    ShowMessage(GetErrorMessage("ERR036"), 1);//Select Ship to Country
                    return false;
                }
                else if (ddlShipState.SelectedIndex == 0)
                {
                    ShowMessage(GetErrorMessage("ERR037"), 1);//Select Ship to State
                    return false;
                }

            }
            return true;
        }

        protected void ddlBillAddressName_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                LoadAddress();
                bindHtml();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void ddlShipAddressName_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                LoadAddress();
                bindHtml();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void lbBillEdit_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect("CustomerAddress.aspx?AType=Bill&AId=" + ddlBillAddressName.SelectedValue + (!string.IsNullOrEmpty(Request["ReturnURL"]) ? "&ReturnURL=" + Request["ReturnURL"] : ""));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void lbShipEdit_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect("CustomerAddress.aspx?AType=Ship&AId=" + ddlShipAddressName.SelectedValue + (!string.IsNullOrEmpty(Request["ReturnURL"]) ? "&ReturnURL=" + Request["ReturnURL"] : ""));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void lbBillAddNew_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect("CustomerAddress.aspx?AType=Bill" + (!string.IsNullOrEmpty(Request["ReturnURL"]) ? "&ReturnURL=" + Request["ReturnURL"] : ""));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void lbShipAddNew_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect("CustomerAddress.aspx?AType=Ship" + (!string.IsNullOrEmpty(Request["ReturnURL"]) ? "&ReturnURL=" + Request["ReturnURL"] : ""));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void chkShippingAdd_CheckedChanged()
        {
            try
            {
                if (chkShippingAdd.Checked)
                {
                    txtShipStreet.Text = txtBillStreet.Text;
                    txtShipCity.Text = txtBillCity.Text;
                    txtShipCode.Text = txtBillCode.Text;
                    ddlShipCountry.SelectedValue = ddlBillCountry.SelectedValue;
                    if (ddlShipCountry.SelectedIndex > 0)
                    {
                        FillState(ddlShipState, Sites.ToLong(ddlShipCountry.SelectedValue), Sites.ToLong(Session["DomainID"]));
                    }
                    ddlShipState.SelectedValue = ddlBillState.SelectedValue;
                }
                else
                {
                    txtShipStreet.Text = "";
                    txtShipCity.Text = "";
                    txtShipCode.Text = "";
                    ddlShipCountry.SelectedIndex = 0;
                    ddlShipState.Items.Clear();
                    //ddlShipState.SelectedIndex = 0;
                }
                // bindHtml();
            }
            catch (Exception ex)
            {
                ExceptionModule.ExceptionPublish(ex, Sites.ToLong(Session["DomainID"]), Sites.ToLong(Session["UserContactID"]), Sites.ToLong(Session["SiteId"]), Request);
                ShowMessage(ex.ToString(), 1);
            }
        }

        protected void ddlBillCountry_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (ddlBillCountry.SelectedIndex > 0)
                {
                    FillState(ddlBillState, Sites.ToLong(ddlBillCountry.SelectedValue), Sites.ToLong(Session["DomainID"]));
                }
                bindHtml();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void ddlShipCountry_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (ddlShipCountry.SelectedIndex > 0)
                {
                    FillState(ddlShipState, Sites.ToLong(ddlShipCountry.SelectedValue), Sites.ToLong(Session["DomainID"]));
                }
                bindHtml();
            }
            catch (Exception ex)
            {
                ExceptionModule.ExceptionPublish(ex, Sites.ToLong(Session["DomainID"]), Sites.ToLong(Session["UserContactID"]), Sites.ToLong(Session["SiteId"]), Request);
                ShowMessage(ex.ToString(), 1);
            }
        }

        void bindHtml()
        {
            try
            {
                string strUI = GetUserControlTemplate("ConfirmAddress.htm");

                strUI = strUI.Replace("##BillingAddressDropdown##", CCommon.RenderControl(ddlBillAddressName));

                strUI = strUI.Replace("##AddNewBillingAddressLink##", CCommon.RenderControl(lbBillAddNew));
                strUI = strUI.Replace("##ShippingAddressDropdown##", CCommon.RenderControl(ddlShipAddressName));
                strUI = strUI.Replace("##AddNewShippingAddressLink##", CCommon.RenderControl(lbShipAddNew));
                strUI = strUI.Replace("##EditBillingAddressLink##", CCommon.RenderControl(lbBillEdit));
                strUI = strUI.Replace("##BillingAddressFull##", CCommon.RenderControl(lblBillAddress));
                strUI = strUI.Replace("##EditShippingAddressLink##", CCommon.RenderControl(lbShipEdit));
                strUI = strUI.Replace("##ShippingAddressFull##", CCommon.RenderControl(lblShipAddress));
                strUI = strUI.Replace("##SameAsBillingCheckBox##", CCommon.RenderControl(chkShippingAdd));
                strUI = strUI.Replace("##BillingStreet##", CCommon.RenderControl(txtBillStreet));
                strUI = strUI.Replace("##ShippingStreet##", CCommon.RenderControl(txtShipStreet));
                strUI = strUI.Replace("##BillingCity##", CCommon.RenderControl(txtBillCity));
                strUI = strUI.Replace("##ShippingCity##", CCommon.RenderControl(txtShipCity));
                strUI = strUI.Replace("##BillingZipcode##", CCommon.RenderControl(txtBillCode));
                strUI = strUI.Replace("##ShippingZipcode##", CCommon.RenderControl(txtShipCode));
                strUI = strUI.Replace("##BillingCountryDropDown##", CCommon.RenderControl(ddlBillCountry));
                strUI = strUI.Replace("##ShippingCountryDropDown##", CCommon.RenderControl(ddlShipCountry));
                strUI = strUI.Replace("##BillingStateDropDown##", CCommon.RenderControl(ddlBillState));
                strUI = strUI.Replace("##ShippingStateDropDown##", CCommon.RenderControl(ddlShipState));
                strUI = strUI.Replace("##SaveButton##", CCommon.RenderControl(btnSubmit));


                strUI = strUI.Replace("##showenteraddess##", hfFirstTime.Value == "True" ? "" : "display:None");
                strUI = strUI.Replace("##showeditaddess##", hfEditAddress.Value == "True" ? "" : "display:None");

                pnlCutomizeHtml.Visible = false;
                litCutomizeHtml.Text = strUI;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}