﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;
using System.Text;
using BACRM.BusinessLogic.Common;
using BACRM.BusinessLogic.ShioppingCart;
using System.Data;

namespace BizCart.UserControls
{
    public partial class BreadCrumb : BizUserControl
    {
        public struct PageCrumb
        {
            private short _level;
            private string _url;
            private string _linkName;
            private string _pageFileName;

            public PageCrumb(short level, string url, string linkName, string PageName)
            {
                _level = level;
                _url = url;
                _linkName = linkName;
                _pageFileName = PageName;
            }

            public short Level
            {
                get { return _level; }
            }

            public string Url
            {
                get { return _url; }
            }

            public string LinkName
            {
                get { return _linkName; }
            }

            public string PageFileName
            {
                get { return _pageFileName; }
            }
        }

        public string DisplayName { get; set; }
        public short Level { get; set; }

        private SortedList CrumbList;
        private PageCrumb PageCrumbs;
        private string PageFileName;

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                Sites.InitialiseSession();
                Sites objBreadCrumbs = new Sites();
                DataTable dtBreadCrumb = new DataTable();
                if (!IsPostBack)
                {
                    PageFileName = Request.AppRelativeCurrentExecutionFilePath;
                    PageFileName = PageFileName.Substring(PageFileName.LastIndexOf("/") + 1);

                    if (Session["BreadCrumbDetails"] == null)
                    {
                        objBreadCrumbs.SiteID = Sites.ToLong(Session["SiteId"]);
                        objBreadCrumbs.DomainID = Sites.ToLong(Session["DomainID"]);
                        dtBreadCrumb = objBreadCrumbs.GetBreadCrumb();
                        Session["BreadCrumbDetails"] = dtBreadCrumb;
                    }
                    else
                    {
                        dtBreadCrumb = (DataTable)Session["BreadCrumbDetails"];
                    }

                    DataRow[] drSelectPage = dtBreadCrumb.Select("vcPageURL = '" + PageFileName + "'");

                    if (drSelectPage.Length == 1)
                    {
                        Level = Convert.ToInt16(drSelectPage[0]["tintLevel"].ToString());

                        if (drSelectPage[0]["vcDisplayName"] != System.DBNull.Value)
                        {
                            DisplayName = drSelectPage[0]["vcDisplayName"].ToString();
                        }
                        else
                        {
                            DisplayName = drSelectPage[0]["vcPageName"].ToString();
                        }

                    }

                    if (Session["BreadCrumbs"] == null)
                    {
                        CrumbList = new SortedList();
                        Session["BreadCrumbs"] = CrumbList;
                    }
                    else
                    {
                        CrumbList = (SortedList)Session["BreadCrumbs"];
                    }


                    PageCrumbs = new PageCrumb(Level, Request.RawUrl, DisplayName, PageFileName);

                    ModifyList();
                }
            }
            catch (Exception ex)
            {
                ExceptionModule.ExceptionPublish(ex, Sites.ToInteger(Session["DomainID"]), Sites.ToInteger(Session["UserContactID"]), Sites.ToInteger(Session["SiteId"]), Request);
                ShowMessage(ex.ToString(), 1);
            }
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            try
            {
                if (Session["BreadCrumbs"] != null)
                {
                    CrumbList = (SortedList)Session["BreadCrumbs"];

                    if (CrumbList.Count > 1)
                    {
                        PutBreadCrumbs();
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionModule.ExceptionPublish(ex, Sites.ToInteger(Session["DomainID"]), Sites.ToInteger(Session["UserContactID"]), Sites.ToInteger(Session["SiteId"]), Request);
                ShowMessage(ex.ToString(), 1);
            }
        }

        private void ModifyList()
        {
            try
            {
                RemoveLowerLevelCrumbs();

                CrumbList.Add(Level, PageCrumbs);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void RemoveLowerLevelCrumbs()
        {
            try
            {
                short forRemoval = 0;

                if (Level > 0)
                {
                    forRemoval = Level;
                }
                else
                {
                    foreach (short level in CrumbList.Keys)
                    {
                        PageCrumb pcCrumb = (PageCrumb)CrumbList[level];

                        if ((pcCrumb.PageFileName.ToLower().Equals(PageFileName.ToLower())))
                        {
                            forRemoval = level;
                            break;
                        }
                    }
                }

                short index = (short)CrumbList.IndexOfKey(forRemoval);

                while (index < CrumbList.Count && index != -1)
                {
                    CrumbList.RemoveAt(index);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void PutBreadCrumbs()
        {
            try
            {
                StringBuilder linkString = new StringBuilder();

                PageCrumb pageCrumb = new PageCrumb();
                int index = 0;
                linkString.Append("<ul class=\"breadcrumb\">");

                for (index = 0; index <= CrumbList.Count - 2; index++)
                {
                    pageCrumb = (PageCrumb)CrumbList.GetByIndex(index);
                    linkString.Append("<li class=\"breadcrumb-item\">");
                    linkString.Append(string.Format("<a href=\"{0}\">{1}</a>", pageCrumb.Url, pageCrumb.LinkName));
                    linkString.Append("</li>");
                }

                pageCrumb = (PageCrumb)CrumbList.GetByIndex(index);
                linkString.Append("<li class=\"breadcrumb-item active\">");
                linkString.Append(pageCrumb.LinkName);
                linkString.Append("</li>");
                linkString.Append("</ul>");
                litTrail.Text = linkString.ToString();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static void OverWriteBreadCrumb(string SpecificName)
        {
            try
            {
                SortedList pageCrumbList = (SortedList)HttpContext.Current.Session["BreadCrumbs"];

                if (pageCrumbList != null)
                {
                    short level = -1;
                    string relativeAppPath = HttpContext.Current.Request.AppRelativeCurrentExecutionFilePath;
                    relativeAppPath = relativeAppPath.Substring(relativeAppPath.LastIndexOf("/") + 1);

                    foreach (short eachlevel in pageCrumbList.Keys)
                    {
                        PageCrumb pclSpecific = (PageCrumb)pageCrumbList[eachlevel];

                        if (pclSpecific.PageFileName.Equals(relativeAppPath))
                        {
                            level = eachlevel;
                            break;
                        }
                    }

                    if (level > -1)
                    {
                        BreadCrumb.PageCrumb pc = new BreadCrumb.PageCrumb(level, HttpContext.Current.Request.RawUrl, SpecificName, relativeAppPath);

                        pageCrumbList[level] = pc;
                        HttpContext.Current.Session["BreadCrumbs"] = pageCrumbList;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static void OverWriteBreadCrumb(short level, string url, string CrumbDisplayName, string pageFileName)
        {
            try
            {
                SortedList pageCrumbList = (SortedList)HttpContext.Current.Session["BreadCrumbs"];

                string actualUrl = HttpContext.Current.Session["SitePath"].ToString();//"http://localhost/BizCart/Pages/15/";

                url = actualUrl + "/" +url;

                //actualUrl = actualUrl.Substring(actualUrl.IndexOf("/") + 2);
                //actualUrl = actualUrl.Substring(actualUrl.IndexOf("/") + 1);
                //actualUrl = actualUrl.Substring(actualUrl.IndexOf("/"));

                pageFileName = "~" + actualUrl + pageFileName;

                PageCrumb pcList = new PageCrumb(level, url, CrumbDisplayName, pageFileName);

                if (pageCrumbList != null)
                {
                    pageCrumbList.Add(level, pcList);
                }
                else
                {
                    pageCrumbList = new SortedList();

                    pageCrumbList.Add(level, pcList);

                }

                HttpContext.Current.Session["BreadCrumbs"] = pageCrumbList;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static void Clear()
        {
            HttpContext.Current.Session.Remove("BreadCrumbs");
        }

        private string GetURL(string CategoryName, string CategoryID)
        {
            try
            {
                string strURL = Sites.ToString(Session["SitePath"]) + "/SubCategory/" + Sites.GenerateSEOFriendlyURL(CategoryName) + "/" + CategoryID;
                return strURL;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}