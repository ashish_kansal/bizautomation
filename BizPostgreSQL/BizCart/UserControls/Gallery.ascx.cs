﻿using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using BACRM.BusinessLogic.Item;
using System.Data;
using BACRM.BusinessLogic.Admin;
using BACRM.BusinessLogic.Common;
using BACRM.BusinessLogic.ShioppingCart;
using Telerik.Web.UI;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Configuration;
using System.IO;

namespace BizCart.UserControls
{
    public partial class Gallery : BizUserControl
    {
        #region Global Declaration

        public string PreserveStateofTree { get; set; }
        public string ShowOnlyParentCategoriesinTree { get; set; }
        StringBuilder strBuilMain = new StringBuilder();
        HtmlGenericControl div = new HtmlGenericControl("div");

        #endregion

        #region Member Variables

        private string strUI = "";

        private string childHtml;
        private string childContainer;
        private string parentHtml;
        private string parentContainer;

        private string parentContainerStart;
        private string parentContainerEnd;
        private string childContainerStart;
        private string childContainerEnd;
        private string parentCategoryStart;
        private string parentCategoryEnd;
        private string childCategoryStart;
        private string childCategoryEnd;

        private string strCategory;
        #endregion
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!System.Web.HttpContext.Current.User.Identity.IsAuthenticated || CCommon.ToLong(Session["DomainID"]) == 0 || CCommon.ToLong(Session["UserContactID"]) == 0)
                {
                    Response.Redirect("Login.aspx?ReturnURL=" + (Sites.ToString(Session["SiteLiveURL"]).Trim().Length > 2 ? "https://" : "http://") + Request.Url.Host + "/Gallery.aspx", true);
                }
                Sites.InitialiseSession();
                strUI = GetUserControlTemplate("Gallery.htm");
                string pattern = "<##FOR##>(?<Content>([\\s\\S]*?))<##ENDFOR##>";
                string GalleryContent = "";
                string template = "";
                StringBuilder sb = new StringBuilder();
                if (Regex.IsMatch(strUI, pattern))
                {
                    MatchCollection matches = Regex.Matches(strUI, pattern);
                    if ((matches.Count > 0))
                    {
                        GalleryContent = matches[0].Groups["Content"].Value;
                        if (!Directory.Exists(Server.MapPath("/Gallery/")))
                        {
                            Directory.CreateDirectory(Server.MapPath("/Gallery/"));
                        }
                        string[] filePaths = Directory.GetFiles(Server.MapPath("/Gallery/"));
                        List<ListItem> files = new List<ListItem>();
                        foreach (string filePath in filePaths)
                        {
                            template = GalleryContent;
                            template = template.Replace("##ImagePath##", "/Gallery/" + Path.GetFileName(filePath));
                            template = template.Replace("##ImageName##", Path.GetFileName(filePath));
                            sb.Append(template);
                        }
                    }
                }
                strUI = Regex.Replace(strUI, pattern, sb.ToString());
                litCutomizeHtml.Text = strUI;
            }
            catch (Exception ex)
            {
                ExceptionModule.ExceptionPublish(ex, Sites.ToLong(Session["DomainID"]), Sites.ToLong(Session["UserContactID"]), Sites.ToLong(Session["SiteId"]), Request);
                ShowMessage(ex.ToString(), 1);
            }
        }



        public string GetUserControlTemplate(string DefaultTemplateFileName)
        {
            try
            {
                if (HtmlCustomize != null && HtmlCustomize.Length > 0)
                {
                    return HttpUtility.HtmlDecode(HtmlCustomize);
                }
                else
                {
                    return Server.HtmlDecode(Sites.ReadFile(ConfigurationManager.AppSettings["CartLocation"].ToString() + "\\Default\\Elements\\" + DefaultTemplateFileName));
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}