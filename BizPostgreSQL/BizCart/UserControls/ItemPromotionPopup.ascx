﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ItemPromotionPopup.ascx.cs" Inherits="BizCart.UserControls.ItemPromotionPopup" %>
<script type="text/javascript">
    function LoadItemPromoPopup(itemCode) {
        try {
            $('#divItemPromoList').html("");

            $data = {
                autoCheck: 32,
                size: 32,
                bgColor: '#FFF',
                bgOpacity: 0.8,
                fontColor: false,
                title: "Please wait...",
                isOnly: true
            };

            if ($.loader != null) {
                $.loader.open($data);
            }

            $.ajax({
                type: "POST",
                url: "/js/OrderServices.asmx/GetPromotionApplicableToItem",
                data: '{itemCode: ' + itemCode + '}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    try {
                        var jsonData = $.parseJSON(response.d);

                        if (jsonData != null && jsonData != 'undefined' && jsonData != "") {
                            var itemPromotions = $.parseJSON(jsonData.itemPromotions);
                            var preferredPromotionID = parseInt($.parseJSON(jsonData.preferredPromotionID));

                            var listItems = '';
                            for (var i = 0; i < itemPromotions.length; i++) {
                                if ((preferredPromotionID > 0 && parseInt(itemPromotions[i].numProId) == preferredPromotionID) || (preferredPromotionID == 0 && i == 0)) {
                                    listItems += "<input type='radio' name='itempromo' checked='true' value='" + itemPromotions[i].numProId + "'> " + itemPromotions[i].vcShortDesc + "<br>";
                                }
                                else {
                                    listItems += "<input type='radio' name='itempromo' value='" + itemPromotions[i].numProId + "'> " + itemPromotions[i].vcShortDesc + "<br>";
                                }

                            }

                            $("[id$=hdnShowPromoForItem]").val(itemCode);
                            $('#divItemPromoList').html(listItems);
                            $("#divItemPromoPopup").modal('show');
                        } else {
                            $('#divItemPromoPopup').modal('hide');
                        }
                    } catch (e) {
                        alert("Error occured while fetching promotion details.!");
                    }
                    
                    if ($.loader != null) {
                        $.loader.close();
                    }
                },
                error: function () {
                    alert("Error occured while fetching promotion details.!");
                    if ($.loader != null) {
                        $.loader.close();
                    }
                }
            });
        } catch (err) {
            alert("Error occured while fetching promotion details.!");

            if ($.loader != null) {
                $.loader.close();
            }
        }
        
        return false;
    }

    function SaveItemPromoSelection() {
        try {
            var itemCode = $("[id$=hdnShowPromoForItem]").val();
            var promotionID = $("input:radio[name='itempromo']:checked").val();

            $.ajax({
                type: "POST",
                url: "/js/OrderServices.asmx/SavePreferredItemPromotion",
                data: '{itemCode: ' + itemCode + ',promotionID:' + promotionID + '}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    $("#divItemPromoPopup").modal('hide');

                    if ($.loader != null) {
                        $.loader.close();
                    }
                },
                error: function () {
                    alert("Error occured while saving promotion selection.!");

                    $('#divItemPromoPopup').modal('hide');

                    if ($.loader != null) {
                        $.loader.close();
                    }
                }
            });
        } catch (e) {
            alert("Error occured while saving promotion selection.!");

            $('#divItemPromoPopup').modal('hide');

            if ($.loader != null) {
                $.loader.close();
            }
        }
        

        return false;
    }
</script>
<asp:Literal ID="litCutomizeHtml" runat="server"></asp:Literal>
<asp:HiddenField ID="hdnShowPromoForItem" runat="server" />



