﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BACRM.BusinessLogic.ShioppingCart;
using BACRM.BusinessLogic.Common;
using BACRM.BusinessLogic.Leads;

namespace BizCart.UserControls
{
    public partial class Subscrption : BizUserControl
    {
        public string SubscribeMessage { get; set; }

        public string UnsubscribeMessage { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                Sites.InitialiseSession();
                txtSubscriberEmail.Attributes.Add("onkeydown", "if(event.which || event.keyCode){if ((event.which == 13) || (event.keyCode == 13)) {document.getElementById('" + btnSubscribe.ClientID + "').click();return false;}} else {return true}; ");
                if (!IsPostBack)
                    bindHtml();
            }
            catch (Exception ex)
            {
                ExceptionModule.ExceptionPublish(ex, Sites.ToInteger(Session["DomainID"]), Sites.ToInteger(Session["UserContactID"]), Sites.ToInteger(Session["SiteId"]), Request);
                ShowMessage(ex.ToString(), 1);
            }
        }

        protected void btnSubscribe_Click(object sender, EventArgs e)
        {
            try
            {
                if (Session["DomainID"] != null && Session["SiteId"] != null)
                {
                    Sites objSites = new Sites();
                    string email = txtSubscriberEmail.Text.Trim();
                    string emailName = email.Substring(0, email.LastIndexOf('@'));

                    objSites.DomainID = Sites.ToLong(Session["DomainID"]);
                    objSites.SiteID = Sites.ToLong(Session["SiteId"]);
                    objSites.Email = email;

                    if (rblSubscription.SelectedValue.Equals("1"))
                    {

                        if (!objSites.CheckDuplicateSiteSubscription())
                        {
                            CLeads leadSubscribe = new CLeads();

                            leadSubscribe.DomainID = Sites.ToLong(Session["DomainID"]);

                            leadSubscribe.Email = email;
                            leadSubscribe.CompanyName = emailName;

                            long lngCompanyID = leadSubscribe.CreateRecordCompanyInfo();

                            leadSubscribe.DivisionName = emailName;
                            leadSubscribe.CompanyID = lngCompanyID;

                            long lngDivisionID = leadSubscribe.CreateRecordDivisionsInfo();

                            leadSubscribe.DivisionID = lngDivisionID;

                            long lngContactID = leadSubscribe.CreateRecordAddContactInfo();

                            objSites.IsSubscribe = true;
                            objSites.ContactID = lngContactID;

                            if (objSites.ManageSiteSubscription())
                            {
                                if (SubscribeMessage == null || SubscribeMessage == string.Empty)
                                {
                                    ShowMessage(GetErrorMessage("ERR054"), 0);//You have successfully subscribed.
                                }
                                else
                                {
                                    ShowMessage(SubscribeMessage, 0);
                                }
                            }
                        }
                        else
                        {
                            ShowMessage(GetErrorMessage("ERR055"), 1);//You are already Subscribed.
                        }
                    }
                    else
                    {
                        objSites.IsSubscribe = false;

                        if (objSites.ManageSiteSubscription())
                        {
                            if (SubscribeMessage == null || SubscribeMessage == string.Empty)
                            {
                                ShowMessage(GetErrorMessage("ERR056"), 0);//You are unsubscribed.
                            }
                            else
                            {
                                ShowMessage(UnsubscribeMessage, 0);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionModule.ExceptionPublish(ex, Sites.ToInteger(Session["DomainID"]), Sites.ToInteger(Session["UserContactID"]), Sites.ToInteger(Session["SiteId"]), Request);
                ShowMessage(ex.ToString(), 1);
            }
        }

        void bindHtml()
        {
            try
            {
                string strUI = GetUserControlTemplate("Subscribe.htm");

                strUI = strUI.Replace("##SubscribeRadioButtonList##", CCommon.RenderControl(rblSubscription));
                strUI = strUI.Replace("##EmailTextBox##", CCommon.RenderControl(txtSubscriberEmail));
                strUI = strUI.Replace("##SubmitButton##", CCommon.RenderControl(btnSubscribe));

                pnlCutomizeHtml.Visible = false;
                litCutomizeHtml.Text = strUI;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}