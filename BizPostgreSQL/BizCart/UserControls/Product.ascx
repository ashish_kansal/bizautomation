﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Product.ascx.cs" Inherits="BizCart.UserControls.Product" ClientIDMode="Static" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Import Namespace="BACRM.BusinessLogic.Common" %>

<% QueryStringValues objEncryption = new QueryStringValues();%>

<style>
    .itemstylePadding {
        padding-left: 5px;
    }
</style>
<script src="/Js/jquery-loader.js"></script>
<script type="text/javascript">

    var SiteId = "";
    var DomainId = "";
    var TypeId = "";
    var ContactId = "";
    var ReferenceId = 0;
    var kitSelection = [];

    $(document).ready(function () {

        SiteId = $('#hfSiteId').val();
        DomainId = $('#hfDomainId').val();
        TypeId = $('#hfTypeId').val();
        ContactId = $('#hfContactId').val();
        ReferenceId = $('#hfItemId').val();
        $("#btnIncrease").click(function () {
            var units = $("#txtUnits").val();
            $("#txtUnits").val(parseInt(units) + 1);
            return false;
        })
        $("#btnDecrease").click(function () {
            var units = $("#txtUnits").val();
            if (units > 1) {
                $("#txtUnits").val(units - 1);
            }
            return false;
        })

        //LoadChildKits(ReferenceId);
    });

    function UpdateAvaregeRating() {
        $.ajax({
            type: "POST",
            url: "http://portal.bizautomation.com/Common/Common.asmx/UpdateAverageRating",
            data: '{ReferenceId:"' + ReferenceId + '",SiteId: "' + SiteId + '", DomainId:"' + DomainId + '"}', //1'" + RatingId + "'  + "', TypeId:'" + TypeId + "' , ReferenceId:'" + ReferenceId + "', RatingCount:'" + RatingCount + "', ContactId:'" + ContactId + "', IpAddress:'" + IpAddress + "' , SiteId:'" + SiteId + "', DomainId:'" + DomainId + "'}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (msg) {
                $('.AvgRating').empty();
                $('.AvgRating').html(msg.d);
                $(function () {
                    $('.star').rating({

                    });
                });
            }
        });
    }

    function UpdateRatingCount() {
        $.ajax({
            type: "POST",
            url: "http://portal.bizautomation.com/Common/Common.asmx/UpdateRatingCount",
            data: '{ReferenceId:"' + ReferenceId + '",TypeId: "' + TypeId + '", SiteId: "' + SiteId + '", DomainId:"' + DomainId + '"}', //1'" + RatingId + "'  + "', TypeId:'" + TypeId + "' , ReferenceId:'" + ReferenceId + "', RatingCount:'" + RatingCount + "', ContactId:'" + ContactId + "', IpAddress:'" + IpAddress + "' , SiteId:'" + SiteId + "', DomainId:'" + DomainId + "'}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (msg) {
                $('.show_rating_count').empty();
                $('.show_rating_count').html(msg.d + "&nbsp;");
            }
        });
    }

    function openOrderSummary() {
        window.location.replace("/OrderSummary.aspx");
        return false;
    }

    //function LoadChildKits(itemCode) {
    //    LoadOverlay('body', 'Please wait...');

    //    try {
    //        $.ajax({
    //            type: "POST",
    //            url: "/js/OrderServices.asmx/LoadChildKits",
    //            data: '{itemCode: "' + itemCode + '"}',
    //            contentType: "application/json; charset=utf-8",
    //            dataType: "json",
    //            success: function (data) {
    //                var childKits = $.parseJSON(data.d);
    //                childKits.forEach(function (e) {
    //                    var html = "";
    //                    kitSelection.push({ "numItemCode": e.numItemCode, "bitKitSingleSelect": e.bitKitSingleSelect, "selectedItems": [] });

    //                    if (e.vcParentKits == "") {
    //                        html += "<div class='col-xs-12' id='divKit" + e.numItemCode + "'>";
    //                    } else {
    //                        html += "<div class='col-xs-12' id='divKit" + e.numItemCode + "' style='display:none'>";
    //                    }

    //                    if (e.bitKitSingleSelect) {
    //                        html += "<div class='form-group'><label>" + e.vcItemName + "<span style='color:red'> *</span></label>";
    //                        html += "<select id='control" + e.numItemCode + "' class='form-control' onchange=\"return KitSelectionChanged('" + itemCode + "'," + e.numItemCode + ",this,'" + e.vcDependedKits + "');\"></select>";
    //                    } else {
    //                        html += "<div class='form-group'><label>" + e.vcItemName + "</label>";
    //                        html += "<div><ul id='control" + e.numItemCode + "' class='list-unstyled'></ul></div>";
    //                    }

    //                    html += "</div>";
    //                    html += "</div>";

    //                    $("#divChildKits").append(html);

    //                    if (e.vcParentKits == "") {
    //                        $.ajax({
    //                            type: "POST",
    //                            url: "/js/OrderServices.asmx/LoadKitChildItems",
    //                            data: '{itemCode: ' + e.numItemCode + '}',
    //                            contentType: "application/json; charset=utf-8",
    //                            dataType: "json",
    //                            success: function (data) {
    //                                var childItems = $.parseJSON(data.d);

    //                                if (childItems.length > 0) {
    //                                    if (e.bitKitSingleSelect) {
    //                                        $("#control" + childItems[0].numKitItemCode).append("<option value='0'>-- Select One --</option>")

    //                                        childItems.forEach(function (e) {
    //                                            $("#control" + e.numKitItemCode).append("<option value='" + e.numItemCode + "'>" + e.vcItemName + "</option>")
    //                                        });
    //                                    } else {
    //                                        childItems.forEach(function (e) {
    //                                            $("#control" + e.numKitItemCode).append("<li><input type='checkbox' id='chk' value='" + e.numItemCode + "' onchange=\"return KitSelectionChanged('" + itemCode + "'," + e.numKitItemCode + ",this,'" + e.vcDependedKits + "');\"> " + e.vcItemName + "</li>")
    //                                        });
    //                                    }
    //                                }
    //                            },
    //                            error: function (jqXHR, textStatus, errorThrown) {
    //                                alert(textStatus);
    //                            }
    //                        });
    //                    }
    //                });

    //                $.loader.close();
    //            },
    //            error: function (jqXHR, textStatus, errorThrown) {
    //                alert(textStatus);
    //            },
    //        });
    //    }
    //    catch (err) {
    //        $.loader.close();
    //        alert("Unknown error occurred while loading kit item(s).");
    //    }
    //}

    //function KitSelectionChanged(mianItemCode, childKitItemCode, ctl, vcDependedItems) {
    //    LoadOverlay('body', 'Please wait...');

    //    try {
    //        var selectedValue = "";

    //        kitSelection.forEach(function (e) {
    //            if (e.numItemCode == childKitItemCode) {
    //                if ($(ctl).is("select")) {
    //                    e.selectedItems = [];
    //                    if ($(ctl).val() != "0") {
    //                        e.selectedItems.push($(ctl).val());
    //                    }
    //                } else if ($(ctl).is("input")) {
    //                    if (e.selectedItems != null && e.selectedItems.length > 0) {
    //                        e.selectedItems = jQuery.grep(e.selectedItems, function (value) {
    //                            return value != $(ctl).attr("value");
    //                        });
    //                    }

    //                    if ($(ctl).is(":checked")) {
    //                        e.selectedItems.push($(ctl).attr("value"));
    //                    }
    //                }

    //                selectedValue = e.selectedItems.join(",");
    //            }
    //        });

    //        if (vcDependedItems != "") {
    //            $.ajax({
    //                type: "POST",
    //                url: "/js/OrderServices.asmx/LoadDependedKitChilds",
    //                data: '{MainKitIemCode: "' + mianItemCode + '",ChildKitIemCode:' + childKitItemCode + ',SelectedValue:"' + selectedValue + '"}',
    //                contentType: "application/json; charset=utf-8",
    //                dataType: "json",
    //                success: function (data) {
    //                    var obj = $.parseJSON(data.d);

    //                    var kits = $.parseJSON(obj.kits);
    //                    var kitChilds = $.parseJSON(obj.kitChilds);
    //                    var i = 0;
    //                    var field;

    //                    kits.forEach(function (e) {
    //                        //clear stored selection value
    //                        kitSelection.forEach(function (t) {
    //                            if (t.numItemCode == e.numChildKitItemCode) {
    //                                t.selectedItems = [];
    //                            }
    //                        });

    //                        var control = $("#control" + e.numChildKitItemCode);
    //                        $(control).empty();

    //                        var childItems = kitChilds.filter(function (obj) {
    //                            return obj.numChildKitItemCode === e.numChildKitItemCode;
    //                        });

    //                        if (childItems != null && childItems.length > 0) {
    //                            if (control.is("ul")) {
    //                                childItems.forEach(function (f) {
    //                                    $(control).append("<li><input type='checkbox' id='chk'  value='" + f.numItemCode + "' onchange=\"return KitSelectionChanged('" + mianItemCode + "'," + e.numChildKitItemCode + ",this,'" + e.vcDependedKits + "');\"> " + f.vcItemName + "</li>")
    //                                });
    //                            } else {
    //                                $(control).append("<option value='0'>-- Select One --</option>")

    //                                childItems.forEach(function (f) {
    //                                    $(control).append("<option value='" + f.numItemCode + "'>" + f.vcItemName + "</option>")
    //                                });
    //                            }

    //                            $("#divKit" + e.numChildKitItemCode).show();
    //                        } else {
    //                            $("#divKit" + e.numChildKitItemCode).hide();
    //                        }
    //                    });

    //                    $("[id$=hdnChildKitSelection]").val(JSON.stringify(kitSelection));
    //                },
    //                error: function (jqXHR, textStatus, errorThrown) {
    //                    alert(textStatus);
    //                }
    //            });
    //        } else {
    //            $("[id$=hdnChildKitSelection]").val(JSON.stringify(kitSelection));
    //        }

    //        var selectedItems = ""

    //        kitSelection.forEach(function (e) {
    //            if (e.selectedItems != null && e.selectedItems.length > 0) {
    //                e.selectedItems.forEach(function (t) {
    //                    selectedItems += ((selectedItems.length > 0 ? "," : "") + e.numItemCode + "-" + t)
    //                });
    //            }
    //        });

    //        if ($("[id$=hdnCalAmtBasedonDepItems]").val() == "1") {
    //            $.ajax({
    //                type: "POST",
    //                url: "/js/OrderServices.asmx/GetItemCalculatedPrice",
    //                data: '{MainKitIemCode: "' + mianItemCode + '", quantity:' + ($("[id$=txtUnits]").val() || 1) + ',selectedItems:"' + selectedItems + '"}',
    //                contentType: "application/json; charset=utf-8",
    //                dataType: "json",
    //                success: function (data) {
    //                    var obj = $.parseJSON(data.d);

    //                    var price = $.parseJSON(obj.price);
    //                    var msrp = $.parseJSON(obj.msrp);

    //                    $("#lblMSRPPrice").text(formatCurrency(msrp));
    //                    if ($("#lblOfferPrice").text() != "") {
    //                        $("#lblOfferPrice").text(formatCurrency(price));
    //                    }
    //                    $("#lblProductPrice").text(formatCurrency(price));
    //                },
    //                error: function (jqXHR, textStatus, errorThrown) {
    //                    alert(textStatus);
    //                }
    //            });
    //        }

    //        $.loader.close();
    //    }
    //    catch (err) {
    //        $.loader.close();
    //        alert("Unknown error occurred while loading kit child(s).");
    //    }

    //    return false;
    //}

    function formatCurrency(num) {
        num = num.toString().replace(/\$|\,/g, '');
        if (isNaN(num))
            num = "0";
        sign = (num == (num = Math.abs(num)));
        num = Math.floor(num * 100 + 0.50000000001);
        cents = num % 100;
        num = Math.floor(num / 100).toString();
        if (cents < 10)
            cents = "0" + cents;
        for (var i = 0; i < Math.floor((num.length - (1 + i)) / 3) ; i++)
            num = num.substring(0, num.length - (4 * i + 3)) + ',' +
num.substring(num.length - (4 * i + 3));
        return (((sign) ? '' : '-') + num + '.' + cents);
    }

    function LoadOverlay(target, OverlayTitle) {
        var OverlayTitle;
        if (OverlayTitle == "")
            OverlayTitle = "Please wait...";

        $data = {
            autoCheck: 32,
            size: 32,
            bgColor: '#FFF',
            bgOpacity: 0.8,
            fontColor: false,
            title: OverlayTitle, //文字
            isOnly: true
        };
        //console.log("$(this).data('target'):" + $(this).data('target'));
        switch (target) {
            case 'body':
                $.loader.open($data);
                break;
            case 'close':
                $.loader.close(true);

        }

        $.loader.open($data);
    }
</script>

<asp:Panel ID="pnlCutomizeHtml" runat="server">
    <asp:ScriptManager runat="server" ID="ScriptManager1"></asp:ScriptManager>
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td colspan="2">
                <h1>
                    <asp:Label ID="lblProductName" CssClass="sectionheader" runat="server"></asp:Label>
                </h1>
            </td>
        </tr>
        <tr>
            <td></td>
        </tr>
        <tr>
            <td>
                <asp:Image ID="imgProduct" runat="server" />
            </td>
            <td valign="top">
                <table>
                    <tr id="trPrice" runat="server">
                        <td class="Product_Label">
                            <asp:Label ID="lblPriceLabel" runat="server"></asp:Label>:
                        </td>
                        <td class="Product_Value">
                            <span runat="server" ID="spanOfferPrice">
                                <asp:Label ID="lblOfferPriceCurrency" runat="server"></asp:Label>
                                <asp:Label ID="lblOfferPrice" runat="server"></asp:Label>
                            </span>
                            <span runat="server" ID="spanProductPrice">
                                <asp:Label ID="lblProductPriceCurrency" runat="server"></asp:Label>
                                <asp:Label ID="lblProductPrice" runat="server"></asp:Label>
                            </span>
                            <span runat="server" ID="spanMSRPPrice">
                                <asp:Label ID="lblMSRPPriceCurrency" runat="server"></asp:Label>
                                <asp:Label ID="lblMSRPPrice" runat="server"></asp:Label>
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <asp:Table ID="tblItemDetail" runat="server">
                            </asp:Table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <asp:Table ID="tblPriceBookRule" runat="server">
                            </asp:Table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <asp:Table ID="tblItemAttr" runat="server">
                            </asp:Table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <asp:UpdatePanel ID="uplChildKits" runat="server" ChildrenAsTriggers="true" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <asp:PlaceHolder ID="plhChilKits" runat="server"></asp:PlaceHolder>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                    <tr id="DtrColorItems" runat="server">
                        <td class="Product_Label">Select Item:
                        </td>
                        <td></td>
                    </tr>
                    <tr>
                        <td class="Product_Label">Quantity:
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlItemAttribute" runat="server"></asp:DropDownList>&nbsp;
                            <asp:Button ID="btnDecrease" runat="server" Text="-" />&nbsp;
                            <asp:TextBox ID="txtUnits" runat="server" Width="25" CssClass="textbox" Text="1"></asp:TextBox>
                            &nbsp;
                            <asp:Button ID="btnIncrease" runat="server" Text="+" />&nbsp;
                            <asp:Label ID="lblUnit" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>Coupon Code</td>
                        <td>
                            <asp:TextBox ID="txtCouponCode" Visible="false" runat="server"></asp:TextBox>
                            <asp:Button ID="btnApplyCoupon" runat="server" OnClick="btnApplyCoupon_Click" Visible="false" Text="Apply Coupon" /><br />
                            <asp:Literal ID="ltrlcouponcode" Visible="false" runat="server"></asp:Literal>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>
                            <asp:LinkButton ID="lnkAddToCart" runat="server" CssClass="" CausesValidation="false" OnClick="lnkAddToCart_Click1">Add to Cart</asp:LinkButton>
                            <asp:Label ID="lblCallToOrder" CssClass="calltoorder" runat="server" Text="Call to Order"></asp:Label>
                        </td>
                    </tr>
                    <tr id="trList" runat="server">
                        <td class="Product_Label"></td>
                        <td>
                            <asp:LinkButton ID="lnkAddToList" runat="server" CssClass="" Text=""><div class="addtoFav"></div></asp:LinkButton>
                        </td>
                    </tr>
                    <tr id="tr1" runat="server">
                        <td align="right" colspan="2">
                            <a id="lnkShipping" runat="server" href="#" onclick=""><i>Estimate Shipping charge</i></a>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr id="trShortDescription" runat="server">
            <td colspan="2">
                <asp:Label ID="lblDescription" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:Label ID="lblText" runat="server"></asp:Label>
            </td>
        </tr>
    </table>
    <br />
    <table id="tblOptSelection" runat="server" visible="false">
        <tr>
            <td colspan="2">
                <b>Options And Accessories</b>
                <br />
                <br />
            </td>
        </tr>
        <tr>
            <td class="LabelColumn" rowspan="1">
                <asp:Image ID="imgOptItem" runat="server" BorderWidth="1" Width="100" BorderColor="black"
                    Height="100"></asp:Image><br>
                <asp:HyperLink ID="hplOptImage" runat="server" CssClass="hyperlink" NavigateUrl="">View Full Image</asp:HyperLink>
            </td>
            <td>
                <table id="Table2" runat="server">
                    <tr>
                        <td class="LabelColumn" valign="top">Item
                            <asp:DropDownList ID="ddlOptItem" runat="server" CssClass="dropdown" AutoPostBack="true" OnSelectedIndexChanged="ddlOptItem_SelectedIndexChanged1"></asp:DropDownList>
                        </td>
                        <td style="color: orange">&nbsp;<asp:Label ID="lblOptStock" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">Price
                            <asp:Label ID="lblOptPrice" runat="server" Font-Bold="True"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" style="white-space: nowrap">
                            <asp:Table ID="tblOptItemAttr" runat="server"></asp:Table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">Units
                            <asp:TextBox ID="txtOptUnits" runat="server" CssClass="textbox" Width="25px" Text="1"></asp:TextBox>
                            <asp:LinkButton ID="lnkOptItemAdd" runat="server" CssClass="addtocartlink" OnClick="lnkOptItemAdd_Click1">Add To Cart</asp:LinkButton>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <asp:Label ID="lblOptDesc" runat="server"></asp:Label>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Panel>
<asp:Literal ID="litCutomizeHtml" runat="server"></asp:Literal>
<asp:Literal ID="litTemplate" runat="server"></asp:Literal>
<asp:TextBox ID="txtHidValue" runat="server" Style="display: none"></asp:TextBox>
<asp:TextBox ID="txtHidEditOppItem" runat="server" Style="display: none"></asp:TextBox>
<asp:TextBox ID="txtHidOptValue" runat="server" Style="display: none"></asp:TextBox>
<asp:TextBox ID="txtAddedItems" runat="server" Style="display: none"></asp:TextBox>
<asp:TextBox ID="txtWareHouseItemID" runat="server" Style="display: none"></asp:TextBox>
<asp:TextBox ID="txtOptWareHouseItemID" runat="server" Style="display: none"></asp:TextBox>
<asp:TextBox ID="txtMatrix" runat="server" Style="display: none"></asp:TextBox>
<asp:TextBox ID="txtFreeShipping" runat="server" Style="display: none"></asp:TextBox>
<asp:TextBox ID="txtBackOrder" runat="server" Style="display: none"></asp:TextBox>
<asp:TextBox ID="txtOnHand" runat="server" Style="display: none"></asp:TextBox>
<asp:TextBox ID="txtWeight" runat="server" Style="display: none"></asp:TextBox>
<asp:TextBox ID="txtHeight" runat="server" Style="display: none"></asp:TextBox>
<asp:TextBox ID="txtWidth" runat="server" Style="display: none"></asp:TextBox>
<asp:TextBox ID="txtLength" runat="server" Style="display: none"></asp:TextBox>
<asp:TextBox ID="txtOptFreeShipping" runat="server" Style="display: none"></asp:TextBox>
<asp:TextBox ID="txtOptBackOrder" runat="server" Style="display: none"></asp:TextBox>
<asp:TextBox ID="txtOptOnHand" runat="server" Style="display: none"></asp:TextBox>
<asp:TextBox ID="txtOptWeight" runat="server" Style="display: none"></asp:TextBox>
<asp:HiddenField ID="hdnPath" runat="server" />
<asp:HiddenField ID="hdPriceBookId" runat="server" />
<asp:HiddenField ID="hdnItemPrice" runat="server" />
<asp:TextBox ID="txtItemType" runat="server" Style="display: none"></asp:TextBox>
<asp:HiddenField ID="hdnIsKit" runat="server" />
<asp:HiddenField ID="hdnCalAmtBasedonDepItems" runat="server" />
<asp:HiddenField ID="hfConversionFactor" runat="server" />
<asp:HiddenField ID="hfUOM" runat="server" />
<asp:HiddenField ID="hfSiteId" runat="server" />
<asp:HiddenField ID="hfDomainId" runat="server" />
<asp:HiddenField ID="hfTypeId" runat="server" />
<asp:HiddenField ID="hfItemId" runat="server" />
<asp:HiddenField ID="hfIpAddress" runat="server" />
<asp:HiddenField ID="hdnPromotionCoupon" runat="server" />
<asp:HiddenField ID="hfContactId" runat="server" />
<asp:TextBox ID="hdnSQty" CssClass="hdnSQty" Style="display: none" value="1" runat="server"></asp:TextBox>
<asp:HiddenField ID="hdnHasChildKits" runat="server" />
<asp:HiddenField ID="hdnChildKitSelection" runat="server" />
<div id="divMiniCartPreSell" class="modal fade modal-minicart" role="dialog">

    <div class="modal-dialog modal-dialog-minicart" style="width: 70%">
        <!-- Modal content-->
        <div class="modal-content modal-content-minicart">
            <div id="dialogSellPopup" runat="server" visible="false">
                <div>
                    <div class="modal-header modal-content-minicart">
                        <div class="pull-left">
                            <h4 class="modal-title modal-title-minicart" style="font-weight: bold;">Add to your order   
                            </h4>
                        </div>
                        <div style="float: left; padding-left: 50px; line-height: 1; display: inline;" id="divPromotionDetailRI" visible="false" runat="server">
                            <img src="/Images/Admin-Price-Control.png" height="30" alt="" />
                            <asp:Label ID="Label1" Text="Based on your selection you might want some of the following items, which also qualify for a promotion!" runat="server"></asp:Label>
                        </div>
                    </div>
                    <div class="modal-body modal-body-minicart">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="table-responsive">
                                    <asp:GridView ID="gvRelatedItems" CssClass="table table-bordered table-striped" runat="server" DataKeyNames="numItemCode,numParentItemCode" AutoGenerateColumns="false" Width="100%" CellPadding="3" CellSpacing="3" BorderColor="#f0f0f0" OnRowCommand="gvRelatedItems_RowCommand" OnRowDataBound="gvRelatedItems_RowDataBound">
                                        <RowStyle BorderColor="#CACACA" BorderStyle="Solid" BorderWidth="1" />
                                        <HeaderStyle Font-Bold="true" Font-Size="10" BackColor="#e8e8e8" BorderColor="#CACACA" BorderStyle="Solid" BorderWidth="1" />
                                        <Columns>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:Image ID="imgPath" runat="server" ImageUrl='<%# Eval("vcPathForTImage")%>' alt="" Style="width: 80px;" class="img-responsive" />
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Center" />
                                            </asp:TemplateField>
                                            <%-- <asp:TemplateField HeaderText="Item" HeaderStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <a href="<%# Eval("ItemURL")%>"><%# Eval("vcItemName")%></a>
                                                    <%# Eval("vcAttributes")%>
                                                </ItemTemplate>
                                            </asp:TemplateField>--%>
                                            <asp:BoundField DataField="vcItemName" HeaderText="Item" HeaderStyle-CssClass="text-center" />
                                            <asp:BoundField DataField="txtItemDesc" HeaderText="Description" HeaderStyle-CssClass="text-center" ItemStyle-CssClass="itemstylePadding" ItemStyle-HorizontalAlign="Left" ItemStyle-Wrap="true" />
                                            <asp:BoundField DataField="vcRelationship" HeaderText="Relation to selected item(s)" HeaderStyle-CssClass="text-center" ItemStyle-CssClass="itemstylePadding" ItemStyle-HorizontalAlign="Left" HeaderStyle-Wrap="true" />
                                            <asp:TemplateField HeaderText="Qty" ItemStyle-Wrap="false" HeaderStyle-CssClass="text-center" ItemStyle-CssClass="itemstylePadding" ItemStyle-HorizontalAlign="Left">
                                                <ItemTemplate>
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <asp:TextBox ID="txtQty" runat="server" Width="50" Text="1"></asp:TextBox></td>
                                                            <td>/ </td>
                                                            <td><%# Eval("vcUOMName")%></td>
                                                        </tr>
                                                    </table>
                                                    <asp:HiddenField ID="hdnWarehouseID" runat="server" Value='<%# Eval("numWareHouseItemID")%>' />
                                                    <asp:HiddenField ID="hdnPrice" runat="server" Value='<%# Eval("monListPrice")%>' />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="monListPrice" HeaderText="Unit Price" ItemStyle-CssClass="itemstylePadding" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center" DataFormatString="USD {0:#,##0.00###}" />
                                            <asp:TemplateField HeaderText="Total" ItemStyle-CssClass="itemstylePadding" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center" ItemStyle-Wrap="false">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblTotal" runat="server" Width="100" Text="1"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField ItemStyle-HorizontalAlign="Center" ItemStyle-Width="25">
                                                <ItemTemplate>
                                                    <asp:Button ID="btnAdd" CommandName="Add" class="btn btn-primary" Text="Add" runat="server" OnClientClick="Confirm()" CommandArgument='<%# Eval("numItemCode") %>'></asp:Button>
                                                    <asp:Label ID="lblOutOfStock" runat="server" Width="100" Text="Out of stock" Visible="false"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="numOnHand" Visible="false" />
                                            <asp:BoundField DataField="numItemCode" Visible="false" />
                                        </Columns>
                                    </asp:GridView>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <asp:HiddenField ID="hdnCategory" runat="server" />
            </div>
            <div class="row">
                <div class="pull-right" style="padding-right: 20px;" id="divShipping" runat="server" visible="false">
                    <img src="/Images/TruckGreen.png" height="30" alt="" />
                    <%-- You qualify for free shipping !--%>
                    <asp:Label ID="lblShipping" runat="server"></asp:Label>
                </div>
            </div>
            <div class="row">
                <div class="pull-right" style="font-weight: bold; padding-right: 20px;">
                    <%-- <div class="pull-right" style="font-weight:bold;padding-right:20px;">--%>
                    <asp:Label ID="lblSubTotal" runat="server"></asp:Label>
                </div>
            </div>
            <div class="modal-header modal-content-minicart">
                <div class="pull-left">
                    <h4 class="modal-title modal-title-minicart" style="font-weight: bold;">
                        <img id="imgShopping" src="/Images/shopping_cart.gif" align="absmiddle" style="border-width: 0px;">
                        Your Cart
                    </h4>
                </div>
                <div style="float: left; padding-left: 100px; line-height: 1; display: inline;" id="divPromotion" runat="server" visible="false">
                    <img src="/Images/Admin-Price-Control.png" height="30" alt="" />
                    <asp:Label ID="lblVcPromotion" runat="server"></asp:Label>
                </div>
                <%--  <div style="float: left; padding-left: 100px; line-height: 1; display: inline;" id="divShipping" runat="server" visible="false">
                    <img src="Images/TruckGreen.png" height="30" alt="" />
                    You qualify for free shipping !
                </div>--%>
                <div class="pull-right">
                    <a id="hplCart" class="btn btn-primary" href="/Cart.aspx">View Cart</a>
                    <%--<asp:LinkButton ID="hplCheckout" class="btn btn-primary" runat="server" OnClick="hplCheckout_Click" Text="Check Out"></asp:LinkButton>--%>
                    <asp:LinkButton ID="hplCheckout" class="btn btn-primary" runat="server" Text="Check Out" OnClientClick="openOrderSummary()" />
                    <button type="button" data-dismiss="modal" class="btn btn-primary">Close</button>
                </div>
            </div>
            <div class="modal-body modal-body-minicart">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="table-responsive">
                            <%--OnRowDataBound="gvMiniCart_RowDataBound" --%>
                            <asp:GridView ID="gvMiniCart" CssClass="table table-bordered table-striped" OnRowDataBound="gvMiniCart_RowDataBound" runat="server" ShowFooter="false" AutoGenerateColumns="false" Width="100%" CellPadding="3" CellSpacing="3" BorderColor="#f0f0f0">
                                <RowStyle BorderColor="#CACACA" BorderStyle="Solid" BorderWidth="1" />
                                <HeaderStyle HorizontalAlign="Center" Font-Bold="true" Font-Size="10" BackColor="#e8e8e8" BorderColor="#CACACA" BorderStyle="Solid" BorderWidth="1" />
                                <Columns>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:Image ID="imgPath" runat="server" ImageUrl='<%# Eval("vcPathForTImage")%>' alt="" Style="width: 80px;" class="img-responsive" />
                                            <%--<asp:Image ID="Image1" runat="server" ImageUrl='<%# !string.IsNullOrEmpty(Eval("vcPathForTImage").ToString()) ? "/Images/" + Eval("vcPathForTImage") : "/Images/DefaultProduct.png" %>' />--%>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Item" ItemStyle-CssClass="itemstylePadding" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center" ItemStyle-Width="300px">
                                        <ItemTemplate>
                                            <a href="<%# Eval("ItemURL")%>"><%# Eval("vcItemName")%></a>
                                            <%# Eval("vcAttributes")%>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="txtItemDesc" HeaderText="Description" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Left" ItemStyle-Wrap="true" />
                                    <asp:BoundField DataField="numUnitHour" HeaderText="Qty" ItemStyle-CssClass="itemstylePadding" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center" />
                                    <asp:BoundField DataField="monPrice" HeaderText="Unit Price" ItemStyle-CssClass="itemstylePadding" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center" DataFormatString="USD {0:#,##0.00###}" />
                                    <asp:BoundField DataField="monTotAmount" HeaderText="Total" ItemStyle-CssClass="itemstylePadding" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center" DataFormatString="USD {0:#,##0.00###}" />
                                </Columns>
                            </asp:GridView>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<br />
<br />
<br />
<script type="text/javascript">

    document.getElementById('hdnPath').value = window.location;

    function removeQty(x) {
        var x = '#' + x;
        var qty = $(x).val();
        if (parseInt(qty) > 1) {
            qty = parseInt(qty) - 1;
            $(x).val(qty);
        }
    }

    function addQty(x) {
        var x = '#' + x;
        var qty = $(x).val();
        qty = parseInt(qty) + 1;
        $(x).val(qty);
    }

    function addtocartmngqty(x, y) {
        var z = '#' + y;
        $(".hdnSQty").val($(z).val());
        __doPostBack('AddToCart', y)
    }

    function Add(a, c, d, e) {
        if ($('#' + a).val().length == 0) {
            alert("Please enter Units.");
            return false;
        }
        else if (isInteger($('#' + a).val()) == false) {
            alert("Invalid Unit Value!");
            return false;
        } else if (parseInt($('#' + a).val()) <= 0) {
            alert("Invalid Unit Value!");
            return false;
        } else if ($("[id*=divKit]").length > 0) {
            var isValid = true;

            $("[id*=divKit]").each(function (index) {
                if ($(this).is(":visible")) {
                    if ($(this).find("select").length > 0 && $($(this).find("select")[0]).val() == "0") {
                        alert($($(this).find("label")[0]).text().replace("*", "") + 'is required.');
                        $($(this).find("select")[0]).focus();
                        isValid = false;
                        return false;
                    }
                }
            });

            if (!isValid) {
                return false;
            }
        }

        if ($('#' + c).val() == "False") {
            var controlType = '<%= AttributeControlType %>';
            if (controlType == 'RadioButton List')
                var DynamicElements = $("#tblItemAttr").find("select"); // getElementsByAttribute($('tblItemAttr'), 'input', 'type', 'radio');
            else
                var DynamicElements = $("#tblItemAttr").find("select");


            for (i = 0; i < DynamicElements.length; i++) {
                if (controlType == 'RadioButton List') {
                    if ($(DynamicElements[i]).checked == false) {
                        alert("Select " + $(DynamicElements[i]).attr("attributename"));
                        DynamicElements[i].focus();
                        return false;
                    }
                }
                else {
                    if ($(DynamicElements[i]).val() == 0) {
                        alert("Select " + $(DynamicElements[i]).attr("attributename"));
                        DynamicElements[i].focus();
                        return false;
                    }
                }

            }
        }
        if ($('#' + c).val() != "") {
            if ($('#' + e).val() == 0 || $('#' + e).val() == '') {
                alert("InSufficient Quantity")
                return false;
            }
        }

        /*if ($('#' + c).val() != "") {
            if ($('#' + d).val() != '') {
                var ddlIDs = $('#' + d).val().split(",");
                for (i = 0; i < ddlIDs.length; i++) {
                    if (ddlIDs[i] == $('#' + e).val()) {
                        alert("This Item is already added to cart. Please update cart.")
                        return false;
                    }
                }
            }
        }*/

        if ($('#' + a).val() == "") {
            alert("Enter Units")
            $('#' + a).focus();
            return false;
        }

    }

    function AddOption(a, c, d, e, f) {

        if ($('#' + e).val() == 0) {
            alert("Select Option Item")
            $('#' + e).focus();
            return false;
        }
        if ($('#' + a).val().length == 0) {
            alert("Please enter Units.");
            return false;
        }
        else if (isInteger($('#' + a).val()) == false) {
            alert("Invalid Unit Value!");
            return false;
        }
        if ($('#' + a).val() < 1) {
            alert('Units must be greater than 0');
        }
        if ($('#' + c).val() != "") {
            if ($('#' + 'ctl00_CphPage_txtOptWareHouseItemID').val() == 0 || $('#' + 'ctl00_CphPage_txtOptWareHouseItemID').val() == '') {
                alert("Sufficient Quantity is not present in the inventory")
                return false;
            }
        }
        if ($('#' + c).val() == "False") {
            if (f != '') {
                var ddlIDs = f.split(",");
                for (i = 0; i < ddlIDs.length; i++) {
                    if ($('#' + 'ctl00_CphPage_' + ddlIDs[i].split("~")[0]).val() == "0") {
                        alert("Select " + ddlIDs[i].split("~")[1])
                        $('#' + 'ctl00_CphPage_' + ddlIDs[i].split("~")[0]).focus();
                        return false;
                    }
                }
            }

        }
        if ($('#' + c).val() != "") {
            if ($('#' + b) != null) {
                if ($('#' + b).val() == 0) {
                    alert("Item is not present in inventory")
                    $('#' + b).focus();
                    return false;
                }
            }

        }
        /*if ($('#' + c).val() != "") {
            if ($('#' + d).val() != '') {
                var ddlIDs = $('#' + d).val().split(",");
                for (i = 0; i < ddlIDs.length; i++) {
                    if (ddlIDs[i] == $('#' + 'ctl00_CphPage_txtOptWareHouseItemID').val()) {
                        alert("This Item is already added to opportunity. Please Edit the details")
                        return false;
                    }
                }
            }
        }*/

        if ($('#' + a).val() == "") {
            alert("Enter Units")
            $('#' + a).focus();
            return false;
        }
    }

    function isInteger(val) {
        for (var i = 0; i < val.length; i++) {
            var ch = val.charAt(i)
            if (i == 0 && ch == "-") {
                continue
            }
            if (ch < "0" || ch > "9") {
                return false
            }
        }
        return true
    }

    function OpenImage(b) {
        window.open('/ItemImage.aspx?ItemCode=' + b, '', 'toolbar=no,titlebar=no,left=300, top=100,width=600,height=500,scrollbars=yes,resizable=yes');
        return false;
    }

    function EstimateShipping(a, b) {
        window.open('/EstimateShipping.aspx?ItemID=' + a + '&CatID=' + b, '', 'toolbar=no,titlebar=no,left=300, top=100,width=600,height=300,scrollbars=yes,resizable=yes');
        return false;
    }

    function getElementsByAttribute(oElm, strTagName, strAttributeName, strAttributeValue) {
        var arrElements = (strTagName == "*" && oElm.all) ? oElm.all : oElm.getElementsByTagName(strTagName);
        var arrReturnElements = new Array();
        var oAttributeValue = (typeof strAttributeValue != "undefined") ? new RegExp("(^|\\s)" + strAttributeValue + "(\\s|$)", "i") : null;
        var oCurrent;
        var oAttribute;
        for (var i = 0; i < arrElements.length; i++) {
            oCurrent = arrElements[i];
            oAttribute = oCurrent.getAttribute && oCurrent.getAttribute(strAttributeName);
            if (typeof oAttribute == "string" && oAttribute.length > 0) {
                if (typeof strAttributeValue == "undefined" || (oAttributeValue && oAttributeValue.test(oAttribute))) {
                    arrReturnElements.push(oCurrent);
                }
            }
        }
        return arrReturnElements;
    }

    <%--Added by Neelam on 10/11/2017 - Added function to open minicart modal pop up--%>
    function ShowMiniPreSellCart() {
        $("#divMiniCartPreSell").modal('show');
    }
</script>
