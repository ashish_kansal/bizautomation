﻿using System;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BACRM.BusinessLogic.Item;
using BACRM.BusinessLogic.ShioppingCart;
using BACRM.BusinessLogic.Common;
using System.Text.RegularExpressions;
using System.Text;
using System.Collections;

namespace BizCart.UserControls
{
    public partial class ThankYou : BizUserControl
    {
        #region Global Variable
        int refType;
        #endregion

        #region Events
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                Sites.InitialiseSession();
                refType = Sites.ToInteger(Request.QueryString["rt"]);
                refType = refType == 0 ? 1 : refType;
                if (!IsPostBack)
                {
                    bindHtml();
                }

                if (Sites.ToString(Request["__EVENTTARGET"]) == "AddToCart")
                {
                    string[] arrItems = Convert.ToString(Request["__EVENTARGUMENT"]).Split(',');
                    long itemid = 0;
                    int qty = 1;
                    decimal discount = 0;
                    long promotionID = 0;
                    string promotionDescription = "";
                    if (arrItems.Length == 5)
                    {
                        itemid = Sites.ToLong(arrItems[0]);
                        qty = Sites.ToInteger(arrItems[1]);
                        discount = Sites.ToDecimal(arrItems[2]);
                        promotionID = Sites.ToLong(arrItems[3]);
                        promotionDescription = Sites.ToString(arrItems[4]);

                        if (qty > 0)
                        {
                            AddToCartFromEcomm("", itemid, 0, "", false, qty, discount, promotionID,promotionDescription);
                        }
                    }
                    
                    Page.MaintainScrollPositionOnPostBack = false;
                    //ScriptManager.RegisterStartupScript(this, this.GetType(), "OpenPostSellPopup", "$('#dialogSellPopup').show();", true);
                    return;
                }

            }
            catch (Exception ex)
            {
                ExceptionModule.ExceptionPublish(ex, Sites.ToLong(Session["DomainID"]), Sites.ToLong(Session["UserContactID"]), Sites.ToLong(Session["SiteId"]), Request);
                // Page.MaintainScrollPositionOnPostBack = false;
                ShowMessage(ex.ToString(), 1);
            }
        }
        #endregion

        #region Methods
        #region HTML
        void bindHtml()
        {
            try
            {
                bool showPostSellPopUp = false;
                string strUI = GetUserControlTemplate("ThankYou.htm");
                if (Sites.ToString(Session["TOppId"]) != "")
                {
                    strUI = strUI.Replace("##OppId##", Sites.ToString(Session["TOppId"]));
                }
                else
                {
                    strUI = strUI.Replace("##OppId##", Sites.ToString(Request["OppID"]));  //"Thank you for submitting your order! <a href=\"javascript:void(0);\" onclick=\"OpenBizInvoice(\'" + Sites.ToString(Session["OppID"]) + "\',\'" + lngOppBizDocID + "\')\" >Click here to view your order</a>."; 
                }
                if (Session["CartItemsForAffliate"] != null)
                {
                    string items = "";
                    DataSet ds = new DataSet();
                    ds = (DataSet)Session["CartItemsForAffliate"];
                    if (ds.Tables.Count > 0)
                    {
                        foreach (DataRow dr in ds.Tables[0].Rows)
                        {
                            items = items + Sites.ToString(dr["vcItemName"]) + "|" + Sites.ToString(dr["numUnitHour"]) + "|" + Sites.ToString(dr["monPrice"]) + ",";
                        }
                    }
                    items = items.TrimEnd(',');
                    strUI = strUI.Replace("##Items##", items);
                }
                else
                {
                    strUI = strUI.Replace("##Items##", "");
                }
                if (Sites.ToString(Session["TotalAffliateAmount"]) != "")
                {
                    strUI = strUI.Replace("##TotalAmount##", Sites.ToString(Session["TotalAffliateAmount"]));
                }
                else
                {
                    strUI = strUI.Replace("##TotalAmount##", "0");
                }

                strUI = strUI.Replace("##OppName##", Sites.ToString(Session["TOppName"]));
                strUI = strUI.Replace("##RefType##", CCommon.ToString(refType));  //"Thank you for submitting your order! <a href=\"javascript:void(0);\" onclick=\"OpenBizInvoice(\'" + Sites.ToString(Session["OppID"]) + "\',\'" + lngOppBizDocID + "\')\" >Click here to view your order</a>."; 
                strUI = strUI.Replace("##PrintMode##", "0");  //"Thank you for submitting your order! <a href=\"javascript:void(0);\" onclick=\"OpenBizInvoice(\'" + Sites.ToString(Session["OppID"]) + "\',\'" + lngOppBizDocID + "\')\" >Click here to view your order</a>."; 

                strUI = strUI.Replace("##Email##", CCommon.ToString(Session["UserEmail"]));
                strUI = strUI.Replace("##FirstName##", CCommon.ToString(Session["FirstName"]));
                strUI = strUI.Replace("##LastName##", CCommon.ToString(Session["LastName"]));

                long oppID = 0;

                if (Sites.ToString(Session["TOppId"]) != "")
                {
                    oppID = Sites.ToLong(Session["TOppId"]);
                }
                else
                {
                    oppID = Sites.ToLong(Request["OppID"]);
                }

                string pattern = "<##FOR(?<Property>(\\b[^>]*?))##>(?<Content>([\\s\\S]*?))<##ENDFOR##>";
                StringBuilder sb = new StringBuilder();
                /*if (Convert.ToString(HttpContext.Current.Session["PostUpSell"]) == "True" && oppID > 0)
                {
                    if (Regex.IsMatch(strUI, pattern))
                    {
                        MatchCollection matches = Regex.Matches(strUI, pattern);
                        if ((matches.Count > 0))
                        {
                            string RepeteTemplate = matches[0].Groups["Content"].Value;
                            Hashtable htProperties = GetPropertiesAsHastable(matches[0].Groups["Property"].Value.Replace("(", "").Replace(")", ""));

                            BACRM.BusinessLogic.Item.CItems objItem = new BACRM.BusinessLogic.Item.CItems();
                            objItem.DomainID = Sites.ToLong(Session["DomainID"]);
                            HttpCookie cookie = GetCookie();
                            objItem.UserCntID = CCommon.ToLong(Session["UserContactID"]);
                            objItem.vcCookieId = (cookie != null) ? cookie["KeyId"] : "";
                            objItem.byteMode = 5;
                            objItem.SiteID = CCommon.ToLong(Session["SiteID"]);
                            DataTable dtItemPrepopup = new DataTable();
                            objItem.SiteID = Sites.ToLong(Session["SiteId"]);
                            objItem.OppId = oppID;
                            dtItemPrepopup = objItem.GetSimilarItem();

                            if (dtItemPrepopup.Rows.Count > 0)
                            {
                                string strTemp = string.Empty;
                                int RowsCount = 0;
                                TextBox txtUnits;
                                Button btnAddToCart;
                                foreach (DataRow dr in dtItemPrepopup.Rows)
                                {
                                    strTemp = RepeteTemplate;

                                    if (!string.IsNullOrEmpty(CCommon.ToString(dr["vcPathForTImage"])))
                                    {
                                        strTemp = strTemp.Replace("##ItemImage##", GetImagePath(CCommon.ToString(dr["vcPathForTImage"])));
                                    }
                                    else
                                    {
                                        strTemp = strTemp.Replace("##ItemImage##", "/Images/DefaultProduct.png");
                                    }
                                    string url = GetProductURL(dr["vcCategoryName"].ToString(), dr["vcItemName"].ToString(), dr["numItemCode"].ToString());
                                    strTemp = strTemp.Replace("##ItemURL##", url);
                                    strTemp = strTemp.Replace("##ItemName##", CCommon.ToString(dr["vcItemName"]));
                                    txtUnits = new TextBox();
                                    txtUnits.Attributes.Add("onkeypress", "javascript:CheckNumber(2,event);");
                                    txtUnits.ID = "txtUnits_" + CCommon.ToString(dr["numItemCode"]);
                                    txtUnits.Text = CCommon.ToString(dr["numUnitHour"]);
                                    txtUnits.Width = Unit.Pixel(20);
                                    txtUnits.CssClass = "textbox";
                                    strTemp = strTemp.Replace("##ItemUnitsTextBox##", CCommon.RenderControl(txtUnits));

                                    strTemp = strTemp.Replace("##ItemPricePerUnit##", Sites.ToString(Session["CurrSymbol"]) + " " + string.Format("{0:#,##0.00}", dr["monListPrice"]));
                                    strTemp = strTemp.Replace("##PriceForYou##", Sites.ToString(Session["CurrSymbol"]) + " " + string.Format("{0:#,##0.00}", (CCommon.ToDecimal(dr["monOfferListPrice"]))));


                                    strTemp = strTemp.Replace("##ItemModelID##", CCommon.ToString(dr["vcModelID"]));
                                    strTemp = strTemp.Replace("##ItemID##", CCommon.ToString(dr["numItemCode"]));

                                    if(CCommon.ToLong(dr["fltDiscount"]) > 0)
                                    {
                                        strUI = strUI.Replace("##PostSellDiscount##", "Based on the items purchased, you’re eligible for a " + CCommon.ToString(dr["fltDiscount"]) + "% discount off list, for the following items");
                                    }
                                    else{
                                        strUI = strUI.Replace("##PostSellDiscount##", "");
                                    }

                                    
                                    btnAddToCart = new Button();
                                    btnAddToCart.Text = "Add to Cart";
                                    btnAddToCart.CssClass = "AddToCart";
                                    btnAddToCart.Attributes.Add("onclick", "return AddToCartPostSell(" + CCommon.ToString(dr["numItemCode"]) + "," + CCommon.ToString(dr["fltDiscount"]) + "," + CCommon.ToString(dr["numPromotionID"]) + ",'" + CCommon.ToString(dr["vcPromotionDescription"]) + "');");
                                    strTemp = strTemp.Replace("##AddToCartButton##", CCommon.RenderControl(btnAddToCart));


                                    if (RowsCount % 2 == 0)
                                        strTemp = strTemp.Replace("##RowClass##", Sites.ToString(htProperties["AlternatingItemClass"]));
                                    else
                                        strTemp = strTemp.Replace("##RowClass##", Sites.ToString(htProperties["ItemClass"]));

                                    sb.Append(strTemp);

                                    RowsCount++;
                                }

                                showPostSellPopUp = true;
                            }
                        }
                    }
                }*/
                strUI = Regex.Replace(strUI, pattern, sb.ToString());
                pnlCutomizeHtml.Visible = false;
                litCutomizeHtml.Text = strUI;

                if (showPostSellPopUp)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "OpenPostSellPopup", "$('#dialogSellPopup').show();", true);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        #endregion


 
        #endregion
    }
}