﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AddPayment.ascx.cs"
    Inherits="BizCart.UserControls.AddPayment" %>
<script language="javascript" src="../../Js/Common.js"></script>
<script type="text/javascript">
    function DeleteRecord(a) {
        if (confirm('Are you sure, you want to delete the selected record?')) {
            document.location.href = '/Addpayment.aspx?flag=delete&PaymentId=' + a;
            return false;
        }
        else {
            return false;
        }
    }
</script>
<asp:Panel ID="pnlCutomizeHtml" runat="server">
    <table>
        <tr>
            <th nowrap colspan="4" align="left">
                <div class="sectionheader">
                    Credit Card Information
                </div>
            </th>
        </tr>
         <tr>
            <td nowrap align="right">
                Card Profiles:
            </td>
            <td>
                <asp:DropDownList ID="ddlCardProfiles" AutoPostBack="true" CssClass="dropdown" OnSelectedIndexChanged="ddlCardProfiles_SelectedIndexChanged" runat="server"></asp:DropDownList>
            </td>
            <td nowrap align="right">
                &nbsp;
            </td>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td nowrap align="right">
                Card Holder Name:
            </td>
            <td>
                <asp:TextBox ID="txtCardHolderName" runat="server" CssClass="textbox" MaxLength="100" ></asp:TextBox>
            </td>
            <td nowrap align="right">
                &nbsp;
            </td>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td align="right" nowrap>
                Card Number:
            </td>
            <td>
                <asp:TextBox ID="txtCardNumber" runat="server" CssClass="textbox" MaxLength="18"></asp:TextBox>
            </td>
            <td align="right" nowrap>
                Card Type:
            </td>
            <td>
                <asp:DropDownList ID="ddlCardType" runat="server" CssClass="dropdown">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td nowrap align="right">
                Exp Date (MM/YY):
            </td>
            <td>
                <asp:DropDownList ID="ddlCardExpMonth" CssClass="dropdown creditcarmy" runat="server" Width="70">
                    <asp:ListItem Value="01">01</asp:ListItem>
                    <asp:ListItem Value="02">02</asp:ListItem>
                    <asp:ListItem Value="03">03</asp:ListItem>
                    <asp:ListItem Value="04">04</asp:ListItem>
                    <asp:ListItem Value="05">05</asp:ListItem>
                    <asp:ListItem Value="06">06</asp:ListItem>
                    <asp:ListItem Value="07">07</asp:ListItem>
                    <asp:ListItem Value="08">08</asp:ListItem>
                    <asp:ListItem Value="09">09</asp:ListItem>
                    <asp:ListItem Value="10">10</asp:ListItem>
                    <asp:ListItem Value="11">11</asp:ListItem>
                    <asp:ListItem Value="12">12</asp:ListItem>
                </asp:DropDownList>
                &nbsp;/&nbsp;
                <asp:DropDownList ID="ddlCardExpYear" CssClass="dropdown creditcarmy" runat="server" Width="70">
                </asp:DropDownList>
            </td>
            <td nowrap align="right" height="31">
                CVV2 Data:
            </td>
            <td height="31">
                <asp:TextBox ID="txtCardCVV2" CssClass="textbox" runat="server" MaxLength="4" TextMode="Password"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td>
                <asp:CheckBox ID="chkDefault" runat="server" Text=" Is Default" />
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td>
                <asp:Button ID="btnSubmit" runat="server" CssClass="button" Text="Save" OnClientClick="return ValidateInput();" OnClick="bsubmit_Click"></asp:Button>
                <asp:Button ID="btnDelete" runat="server" CssClass="button" OnClick="btnDelete_Click" Text="Remove" />
            </td>
            <td colspan="2">
            </td>
        </tr>
        <tr>
            <td colspan="4" class="Grid1">
            </td>
        </tr>
    </table>
</asp:Panel>
<asp:Literal ID="litCutomizeHtml" runat="server"></asp:Literal>
<asp:Literal ID="Literal1" runat="server"></asp:Literal>
