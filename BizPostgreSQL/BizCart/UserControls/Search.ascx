﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Search.ascx.cs" Inherits="BizCart.UserControls.Search" %>
<asp:Panel ID="pnlCutomizeHtml" runat="server">
    <div class="boxheader">
        Search</div>
    <div class="boxcontent">
        &nbsp; <span>
            <asp:HiddenField ID="hdnQuickAddedItems" ClientIDMode="Static" runat="server" />
            <asp:TextBox ID="txtSearch" runat="server" CssClass="searchbox"></asp:TextBox>&nbsp;
            <asp:ImageButton BorderWidth="0" ID="btnSearch" runat="server" class="search-btn" OnClientClick="javascript:__doPostBack('btnSearch', '');" />
        </span>
    </div>
</asp:Panel>
<asp:Literal ID="litCutomizeHtml" runat="server"></asp:Literal>