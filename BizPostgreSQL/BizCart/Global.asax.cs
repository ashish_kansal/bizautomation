﻿using System;
using System.Collections;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;
using BACRM.BusinessLogic.ShioppingCart;
using System.Data;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using BACRM.BusinessLogic.Common;
using BACRM.BusinessLogic.Leads;

namespace BizCart
{
    public class Global : System.Web.HttpApplication
    {

        public static Dictionary<string, string> Redirects;

        protected void Application_Start(object sender, EventArgs e)
        {
           
        }

        protected void Session_Start(object sender, EventArgs e)
        {
            Sites.InitialiseSession();
        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {
            string ActualURL = "";
            string CompareURL = "";
            ActualURL = HttpContext.Current.Request.Url.AbsoluteUri;
            long lngSiteID;
            string[] strArr = HttpContext.Current.Request.PhysicalApplicationPath.Split(new string[] { "\\" }, StringSplitOptions.RemoveEmptyEntries);
            lngSiteID = Sites.ToLong(strArr[strArr.Length - 1]);
            if (ActualURL.Contains("partnercode=")) {
                string[] partnerUrl = ActualURL.Split('&');
                if (partnerUrl.Length > 0)
                {
                    string partnercode = partnerUrl[0].Substring((partnerUrl[0].IndexOf("partnercode=", 0)), partnerUrl[0].Length - (partnerUrl[0].IndexOf("partnercode=", 0)));
                    string[] dataPart = partnercode.Split('=');
                    CLeads _verfiyPartnerCode = new CLeads();
                    _verfiyPartnerCode.DomainID = lngSiteID;
                    _verfiyPartnerCode.vcPartnerCode = dataPart[1];
                    DataTable dt = new DataTable();
                    dt = _verfiyPartnerCode.VerifyPartnerCode();
                    if (dt.Rows.Count > 0)
                    {
                        HttpContext.Current.Items["__partnercode"] = Convert.ToString(dt.Rows[0][0]);
                        HttpContext.Current.Items["__partnerurlcode"] = Convert.ToString(dataPart[1]);
                    }
                    else
                    {
                        HttpContext.Current.Items["__partnercode"] = "0";
                        HttpContext.Current.Items["__partnerurlcode"] = "Not Available";
                    }
                }
            }
            if (ActualURL.Contains("partnercontact="))
            {
                string[] partnerContactUrl = ActualURL.Split('&');
                if (partnerContactUrl.Length > 0)
                {
                    string partnercontact = partnerContactUrl[1].Substring((partnerContactUrl[1].IndexOf("partnercontact=", 0)), partnerContactUrl[1].Length - (partnerContactUrl[1].IndexOf("partnercontact=", 0)));
                    string[] dataPartContact = partnercontact.Split('=');
                    HttpContext.Current.Items["__partnercontact"] = dataPartContact[1];
                }
            }
            CompareURL = ActualURL.ToLower();


            if (Redirects == null)
            {
                Redirects = new Dictionary<string,string>();

                DataTable dtRedirects = new DataTable();
                Sites objSites = new Sites();
                objSites.RedirectConfigID = 0;
                objSites.DomainID = 0;
                objSites.SiteID = lngSiteID;
                dtRedirects = objSites.GetRedirectConfig();
                foreach (DataRow dr in dtRedirects.Rows)
                {
                    Redirects.Add(CCommon.ToString(dr["vcOldUrl"]).ToLower(), CCommon.ToString(dr["vcNewUrl"]).ToLower());
                }
            }
           
            if (Redirects.ContainsKey(CompareURL))
            {
                HttpContext.Current.Response.Status = "301 Moved Permanently";
                HttpContext.Current.Response.AddHeader("Location", Request.Url.ToString().ToLower().Replace(ActualURL, Redirects[CompareURL]));
            }
        }


        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {

        }

        protected void Application_AcquireRequestState(object sender, EventArgs e)
        {
            HttpContext context = HttpContext.Current;
            if (context != null && context.Session != null)
            {
                if (Convert.ToString(HttpContext.Current.Items["__partnercode"]) != "" || HttpContext.Current.Items["__partnercode"]!=null)
                {
                    context.Session["partnercode"] = HttpContext.Current.Items["__partnercode"];
                    
                }
                if (Convert.ToString(HttpContext.Current.Items["__partnerurlcode"]) != "" || HttpContext.Current.Items["__partnerurlcode"]!=null)
                {
                    context.Session["partnerurlcode"] = HttpContext.Current.Items["__partnerurlcode"];
                }
                if (Convert.ToString(HttpContext.Current.Items["__partnercontact"]) != "" || HttpContext.Current.Items["__partnercontact"] != null)
                {
                    context.Session["partnercontact"] = HttpContext.Current.Items["__partnercontact"];
                }
                
            }
        }

        protected void Application_Error(object sender, EventArgs e)
        {

        }

        protected void Session_End(object sender, EventArgs e)
        {

        }

        protected void Application_End(object sender, EventArgs e)
        {

        }
    }
}