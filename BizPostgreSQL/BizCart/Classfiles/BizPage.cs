﻿using System;
using System.Web;
using BACRM.BusinessLogic.ShioppingCart;
namespace BizCart
{
    public class BizPage : System.Web.UI.Page
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="Message"></param>
        /// <param name="Success">1=true(its exception),0=false(its warning)</param>
        //public void ShowMessage(string Message, int IsError)
        //{
        //    //Message = HttpUtility.HtmlEncode(Message).Replace("\r", "").Replace("\n", "<br>").Replace("&#39;", "\\'");
        //    //Message = HttpUtility.HtmlDecode(Message);
        //    this.Page.ClientScript.RegisterStartupScript(this.GetType(), "Msg", "<script>Highlight('MessageBox','" + HttpUtility.JavaScriptStringEncode(Message) + "','" + IsError.ToString() + "');</script>");
        //}

        public override void VerifyRenderingInServerForm(System.Web.UI.Control control)
        { /* Do nothing */ }

        public override bool EnableEventValidation
        {
            get { return false; }
            set { /* Do nothing */}
        }

        //protected override void Render(System.Web.UI.HtmlTextWriter writer)
        //{ /* Do nothing */ }
        public string GetIndexPath()
        {
            return BACRM.BusinessLogic.Common.CCommon.GetLuceneIndexPath(Sites.ToLong(Session["DomainID"]), Sites.ToLong(Session["SiteID"]));
            //return BACRM.BusinessLogic.Common.CCommon.GetDocumentPhysicalPath(Sites.ToLong(Session["DomainID"])) + Sites.ToString(Session["SiteID"]) + "\\ItemIndex";
        }
    }
}
