﻿using System;
using System.Web;
using BACRM.BusinessLogic.ShioppingCart;
using System.Data;
using System.Configuration;
using BACRM.BusinessLogic.Common;
using BACRM.BusinessLogic.Item;
using System.Web.UI.WebControls;
using BACRM.BusinessLogic.Admin;
using System.Web.UI.HtmlControls;
using System.Web.UI;
using nsoftware.InShip;
using BACRM.BusinessLogic.Opportunities;

namespace BizCart
{
    class ChildKitSelection
    {
        public long numItemCode { get; set; }
        public bool bitKitSingleSelect { get; set; }
        public string[] selectedItems { get; set; }
    }

    public class BizUserControl : System.Web.UI.UserControl
    {
        #region Properties
        /// <summary>
        /// Contains HTML template for usercontrol
        /// </summary>
        public string HtmlCustomize { get; set; }
        public string ApplyPriceBookPrice { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="Message"></param>
        /// <param name="IsError">1=true(its exception),0=false(its warning)</param>
        #endregion

        #region Enumerations
        public enum PaymentMethod
        {
            GoogleCheckout = 31488,
            BillMe = 27261,
            CreditCard = 1,
            Paypal = 35141,
            SalesInquiry = 84
        }
        #endregion

        #region Methods

        #region ShippingTrack
        public void NSAPIDETAILS(long lngShippingCompanyId, nsoftware.InShip.Fedextrack _FedexTrack, nsoftware.InShip.Upstrack _UpsTrack)
        {
            try
            {
                BACRM.BusinessLogic.Item.CItems objItem = new BACRM.BusinessLogic.Item.CItems();
                DataSet dsShippingDtl = default(DataSet);
                objItem.DomainID = CCommon.ToLong(HttpContext.Current.Session["DomainID"]);
                objItem.ShippingCMPID = lngShippingCompanyId;
                dsShippingDtl = objItem.ShippingDtls();
                foreach (DataRow dr in dsShippingDtl.Tables[0].Rows)
                {
                    switch (dr["vcFieldName"].ToString())
                    {

                        //Case "Rate Type"
                        //    If Rates1.Provider = EzratesProviders.pFedEx Then
                        //        Rates1.Config("RateType=" & dr("vcShipFieldValue").ToString())
                        //    End If


                        case "Developer Key":
                            _FedexTrack.FedExAccount.DeveloperKey = dr["vcShipFieldValue"].ToString();

                            break;
                        case "Account Number":
                            _FedexTrack.FedExAccount.AccountNumber = dr["vcShipFieldValue"].ToString();

                            break;
                        case "Meter Number":
                            _FedexTrack.FedExAccount.MeterNumber = dr["vcShipFieldValue"].ToString();
                            break;
                            //case "Weight Unit":
                            //    if (dr["vcShipFieldValue"].ToString().ToLower().Equals("lbs"))
                            //    {
                            //        Rates1.Config("WeightUnit=LB");
                            //    }
                            //    else
                            //    {
                            //        Rates1.Config("WeightUnit=KG");
                            //    }

                            break;
                        //Common fields
                        case "Password":
                            _FedexTrack.FedExAccount.Password = dr["vcShipFieldValue"].ToString();
                            _UpsTrack.UPSAccount.Password = dr["vcShipFieldValue"].ToString();
                            break;
                        case "Server URL":
                            _FedexTrack.FedExAccount.Server = dr["vcShipFieldValue"].ToString();

                            break;
                        case "Tracking URL":
                            _UpsTrack.UPSAccount.Server = dr["vcShipFieldValue"].ToString();
                            break;
                        //Fields specific to UPS
                        case "AccessKey":
                            //_FedexTrack.FedExAccount.AccessKey = dr["vcShipFieldValue"].ToString();
                            _UpsTrack.UPSAccount.AccessKey = dr["vcShipFieldValue"].ToString();
                            break;
                        case "UserID":
                            //_FedexTrack.FedExAccount.UserId = dr["vcShipFieldValue"].ToString();
                            _UpsTrack.UPSAccount.UserId = dr["vcShipFieldValue"].ToString();
                            break;
                        case "ShipperNo":
                            _FedexTrack.FedExAccount.AccountNumber = dr["vcShipFieldValue"].ToString();
                            _UpsTrack.UPSAccount.AccountNumber = dr["vcShipFieldValue"].ToString();
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public string GetDeliveryDate(long companyId, string ShipmentNo, nsoftware.InShip.Fedextrack _FedexTrack, nsoftware.InShip.Upstrack _UpsTrack)
        {
            string PackageDeliveryDate = "-";
            try
            {
                long lngShippingCompanyId = companyId;
                _UpsTrack.IdentifierType = UpstrackIdentifierTypes.uitPackageTrackingNumber;
                _UpsTrack.ShipDateStart = "20010219"; //yyyMMdd
                _UpsTrack.ShipDateEnd = "20010228"; //yyyMMdd.
                NSAPIDETAILS(lngShippingCompanyId, _FedexTrack, _UpsTrack);

                //_FedexTrack.TrackShipment("956473415129602");
                if (lngShippingCompanyId == 88)
                {
                    _UpsTrack.TrackShipment(ShipmentNo);
                    PackageDeliveryDate = _UpsTrack.ShipDate;
                }
                else
                {
                    _FedexTrack.TrackShipment(ShipmentNo);
                    if (_FedexTrack.PackageDeliveryDate == "")
                    {
                        PackageDeliveryDate = _FedexTrack.PackageDeliveryEstimate;
                    }
                    else
                    {
                        PackageDeliveryDate = _FedexTrack.PackageDeliveryDate;
                    }
                }
            }
            catch { }
            return PackageDeliveryDate;
        }

        public string GetDeliveryDateandTransit(long ShippingReportId, long ShippingBizDocId)
        {
            string transitTime = "-";
            DataSet ds = new DataSet();
            OppBizDocs objOppBizDocs = new OppBizDocs();
            try
            {
                objOppBizDocs.DomainID = CCommon.ToLong(HttpContext.Current.Session["DomainID"]);
                objOppBizDocs.OppBizDocId = ShippingBizDocId;
                objOppBizDocs.ShippingReportId = ShippingReportId;
                ds = objOppBizDocs.GetShippingReport();

                Shipping objShipping = new Shipping();
                objShipping.DomainID = CCommon.ToLong(HttpContext.Current.Session["DomainID"]);
                objShipping.WeightInLbs = CCommon.ToDouble(ds.Tables[0].Rows[0]["fltTotalDimensionalWeight"]);
                objShipping.NoOfPackages = 1;
                objShipping.UseDimentions = CCommon.ToInteger(ds.Tables[0].Rows[0]["numShippingCompany"]) == 91 ? true : false;
                //for fedex dimentions are must
                objShipping.GroupCount = 1;
                //if (objShipping.UseDimentions)
                //{
                //    objShipping.Height = CCommon.ToDouble(ds.Tables[0].Rows[0]["fltHeight"]);
                //    objShipping.Width = CCommon.ToDouble(ds.Tables[0].Rows[0]["fltWidth"]);
                //    objShipping.Length = CCommon.ToDouble(ds.Tables[0].Rows[0]["fltLength"]);
                //}

                objShipping.SenderState = CCommon.ToString(ds.Tables[0].Rows[0]["vcFromState"]);
                objShipping.SenderZipCode = CCommon.ToString(ds.Tables[0].Rows[0]["vcFromZip"]);
                //CCommon.ToString(dtTable.Rows(0)("vcWPinCode"))
                objShipping.SenderCountryCode = CCommon.ToString(ds.Tables[0].Rows[0]["vcFromCountry"]);

                objShipping.RecepientState = CCommon.ToString(ds.Tables[0].Rows[0]["vcToState"]);
                objShipping.RecepientZipCode = CCommon.ToString(ds.Tables[0].Rows[0]["vcToZip"]);
                objShipping.RecepientCountryCode = CCommon.ToString(ds.Tables[0].Rows[0]["vcToCountry"]);

                objShipping.ServiceType = CCommon.ToShort(ds.Tables[0].Rows[0]["numServiceTypeID"]);
                objShipping.PackagingType = 21;
                //ptYourPackaging 
                objShipping.ItemCode = 1;
                objShipping.OppBizDocItemID = 0;
                objShipping.ID = 9999;
                objShipping.Provider = CCommon.ToInteger(ds.Tables[0].Rows[0]["numShippingCompany"]);

                DataTable dtShipRates = objShipping.GetRates();
                if (dtShipRates.Rows.Count > 0)
                {
                    transitTime = CCommon.ToString(dtShipRates.Rows[0]["TransitTIme"]);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return transitTime;
        }
        #endregion

        #region Cookie
        public void SetCookie(HttpCookie cookie)
        {
            Guid g;
            g = Guid.NewGuid();
            cookie = new HttpCookie("Cart");
            cookie["KeyId"] = CCommon.ToString(g);//CCommon.ToString(rand.Next());
            cookie.Expires = DateTime.Now.AddDays(50);
            HttpContext.Current.Response.Cookies.Add(cookie);
        }
        public HttpCookie GetCookie()
        {
            HttpCookie cookie = HttpContext.Current.Request.Cookies["Cart"];
            if (cookie == null)
            {
                SetCookie(cookie);
            }
            return cookie;
        }

        #endregion

        #region Cart
        public DataSet GetCartItem(bool isFromGuestCheckout = false)
        {
            BACRM.BusinessLogic.ShioppingCart.Cart objcart;
            objcart = new BACRM.BusinessLogic.ShioppingCart.Cart();
            HttpCookie cookie = HttpContext.Current.Request.Cookies["Cart"];
            DataSet ds = new DataSet();
            DataTable dt;
            try
            {
                if (isFromGuestCheckout)
                {
                    objcart.ContactId = CCommon.ToInteger(HttpContext.Current.Session["UserContactID"]);
                }
                else
                {
                    if (!System.Web.HttpContext.Current.User.Identity.IsAuthenticated || CCommon.ToLong(HttpContext.Current.Session["DomainID"]) == 0 || CCommon.ToLong(HttpContext.Current.Session["UserContactID"]) == 0)
                    {
                        //GUEST USER
                        objcart.ContactId = 0;
                    }
                    else
                    {
                        objcart.ContactId = CCommon.ToInteger(HttpContext.Current.Session["UserContactID"]);
                    }
                }

                objcart.DomainID = CCommon.ToInteger(HttpContext.Current.Session["DomainID"]);
                if (HttpContext.Current.Request.Cookies["Cart"] != null)
                {
                    objcart.CookieId = (cookie["KeyId"] != null) ? cookie["KeyId"] : "";
                }
                else
                {
                    objcart.CookieId = "";
                }
                dt = objcart.GetCartItems();
                HttpContext.Current.Session["CartItems"] = dt;
                ds.Tables.Add((DataTable)(dt.Copy()));
                return ds;
            }
            catch (Exception ex)
            {
                ExceptionModule.ExceptionPublish(ex, Sites.ToLong(HttpContext.Current.Session["DomainID"]), Sites.ToLong(HttpContext.Current.Session["UserContactID"]), Sites.ToLong(HttpContext.Current.Session["SiteId"]), HttpContext.Current.Request);
                ShowMessage(ex.ToString(), 1);
                return ds;
            }
            finally
            {
                objcart = null;

            }
        }
        public void ClearCartItems()
        {
            try
            {
                BACRM.BusinessLogic.ShioppingCart.Cart objcart;
                objcart = new BACRM.BusinessLogic.ShioppingCart.Cart();
                HttpCookie cookie = HttpContext.Current.Request.Cookies["Cart"];
                //DataSet ds = default(DataSet);
                objcart.DomainID = CCommon.ToLong(HttpContext.Current.Session["DomainID"]);
                objcart.ContactId = CCommon.ToLong(HttpContext.Current.Session["UserContactID"]);
                objcart.CookieId = (cookie != null) ? cookie["KeyId"] : "";
                objcart.ItemCode = 0;
                objcart.bitDeleteAll = true;
                objcart.SiteID = Sites.ToLong(HttpContext.Current.Session["SiteId"]);
                objcart.RemoveItemFromCart();
                HttpContext.Current.Session["CouponCode"] = null;
                HttpContext.Current.Session["OppID"] = null;
                HttpContext.Current.Session["OppBizDocID"] = null;
                HttpContext.Current.Session["gblItemCount"] = null;
                HttpContext.Current.Session["PayOption"] = null;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void InsertToCart(DataRow dr, int applypostupdiscount = 0, bool isShippingCharge = false, long parentItemCode = 0)
        {
            BACRM.BusinessLogic.ShioppingCart.Cart objcart;
            objcart = new BACRM.BusinessLogic.ShioppingCart.Cart();
            HttpCookie cookie = HttpContext.Current.Request.Cookies["Cart"];
            try
            {
                cookie = GetCookie();

                if (isShippingCharge)
                {
                    objcart.ContactId = CCommon.ToInteger(HttpContext.Current.Session["UserContactID"]);
                }
                else
                {
                    if (!System.Web.HttpContext.Current.User.Identity.IsAuthenticated || CCommon.ToLong(HttpContext.Current.Session["DomainID"]) == 0 || CCommon.ToLong(HttpContext.Current.Session["UserContactID"]) == 0)
                    {
                        //GUEST USER
                        objcart.ContactId = 0;
                    }
                    else
                    {
                        objcart.ContactId = CCommon.ToInteger(HttpContext.Current.Session["UserContactID"]);
                    }
                }
                objcart.DomainID = CCommon.ToInteger(HttpContext.Current.Session["DomainID"]);
                objcart.CookieId = CCommon.ToString(cookie["KeyId"]);
                objcart.OppItemCode = CCommon.ToLong(dr["numOppItemCode"]);
                objcart.ItemCode = CCommon.ToLong(dr["numItemCode"]);
                objcart.ParentItemCode = parentItemCode;
                objcart.UnitHour = CCommon.ToLong(dr["numUnitHour"]);
                objcart.Price = CCommon.ToDecimal(dr["monPrice"]);
                objcart.SourceId = 0;
                objcart.ItemDesc = CCommon.ToString(dr["vcItemDesc"]);
                objcart.WarehouseId = CCommon.ToLong(dr["numWarehouseID"]);
                objcart.ItemName = CCommon.ToString(dr["vcItemName"]);
                objcart.Warehouse = "";
                objcart.WarehouseItemsId = CCommon.ToLong(dr["numWareHouseItmsID"]);
                objcart.ItemType = CCommon.ToString(dr["vcItemType"]);
                objcart.Attributes = CCommon.ToString(dr["vcAttributes"]);
                objcart.AttributesValue = CCommon.ToString(dr["vcAttrValues"]);
                objcart.FreeShipping = CCommon.ToBool(dr["bitFreeShipping"]);
                objcart.Weight = CCommon.ToDecimal(dr["numWeight"]);
                objcart.OPFlag = (short)CCommon.ToInteger(dr["tintOpFlag"]);
                objcart.DiscountType = CCommon.ToBool(dr["bitDiscountType"]);
                objcart.Discount = CCommon.ToDouble(dr["fltDiscount"]);
                objcart.TotAmtBeforeDisc = CCommon.ToDecimal(dr["monTotAmtBefDiscount"]);
                objcart.ItemURL = CCommon.ToString(dr["ItemURL"]);
                objcart.UOM = CCommon.ToLong(dr["numUOM"]);
                objcart.UOMName = CCommon.ToString(dr["vcUOMName"]);
                objcart.UOMConversionFactor = CCommon.ToDecimal(dr["decUOMConversionFactor"]);
                objcart.Height = CCommon.ToLong(dr["numHeight"]);
                objcart.Length = CCommon.ToLong(dr["numLength"]);
                objcart.Width = CCommon.ToLong(dr["numWidth"]);
                objcart.ShippingMethod = "";
                objcart.ServiceTypeId = 0;
                objcart.ShippingCompany = 0;
                objcart.ServiceType = 0;
                objcart.DeliveryDate = DateTime.Now.Date;
                objcart.TotAmount = CCommon.ToDecimal(dr["monTotAmount"]);
                objcart.ItemDesc = CCommon.ToString(dr["vcItemDesc"]);
                objcart.vcCoupon = CCommon.ToString(dr["vcCoupon"]);
                objcart.intpostselldiscount = applypostupdiscount;
                objcart.SiteID = Sites.ToLong(HttpContext.Current.Session["SiteId"]);
                objcart.PromotionID = CCommon.ToLong(dr["PromotionID"]);
                objcart.PromotionDescription = CCommon.ToString(dr["PromotionDesc"]);
                objcart.ChildKitItemSelection = CCommon.ToString(dr["vcChildKitItemSelection"]);
                if (HttpContext.Current.Session["PreferredItemPromotions"] != null)
                {
                    objcart.PreferredPromotion = ((DataSet)HttpContext.Current.Session["PreferredItemPromotions"]).GetXml();
                }

                bool insert = objcart.InsertCartItems();
                if (insert == true)
                {
                    DataTable dtNumItemCode = objcart.GetParentToAddRequiredItem();
                    string SearchText = HttpUtility.UrlDecode(Sites.ToString(HttpContext.Current.Request["ST"]));

                    string lblCategoryName = string.Empty;
                    long productid = 0;
                    if (dtNumItemCode.Rows.Count > 0)
                    {
                        foreach (DataRow dritem in dtNumItemCode.Rows)
                        {
                            if (SearchText.Length > 0)
                            {
                                lblCategoryName = CCommon.ToString(dritem["vcItemName"]);
                            }
                            productid = CCommon.ToLong(dritem["numItemCode"]);

                            AddToCartFromEcomm(lblCategoryName, productid, 0, "", false, 1, parentItemCode: objcart.ItemCode);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionModule.ExceptionPublish(ex, Sites.ToLong(HttpContext.Current.Session["DomainID"]), Sites.ToLong(HttpContext.Current.Session["UserContactID"]), Sites.ToLong(HttpContext.Current.Session["SiteId"]), HttpContext.Current.Request);
                ShowMessage(ex.ToString(), 1);
                return;
            }
            finally
            {
                objcart = null;
            }
        }
        public void UpdateCart(DataSet ds, int postselldiscount = 0)
        {
            Cart objcart;
            objcart = new Cart();
            HttpCookie cookie = HttpContext.Current.Request.Cookies["Cart"];

            try
            {
                ds.Tables[0].TableName = "Table1";
                objcart.ContactId = CCommon.ToInteger(HttpContext.Current.Session["UserContactID"]);
                objcart.DomainID = CCommon.ToInteger(HttpContext.Current.Session["DomainID"]);
                objcart.CookieId = CCommon.ToString(cookie["KeyId"]);
                objcart.intpostselldiscount = postselldiscount;
                objcart.strXML = GetCartItemsXML(ds);
                objcart.SiteID = Sites.ToLong(HttpContext.Current.Session["SiteId"]);
                if (HttpContext.Current.Session["PreferredItemPromotions"] != null)
                {
                    objcart.PreferredPromotion = ((DataSet)HttpContext.Current.Session["PreferredItemPromotions"]).GetXml();
                }
                objcart.UpdateCartItem();

            }
            catch (Exception ex)
            {
                ExceptionModule.ExceptionPublish(ex, Sites.ToLong(HttpContext.Current.Session["DomainID"]), Sites.ToLong(HttpContext.Current.Session["UserContactID"]), Sites.ToLong(HttpContext.Current.Session["SiteId"]), HttpContext.Current.Request);
                ShowMessage(ex.ToString(), 1);
                return;
            }
            finally
            {
                objcart = null;
            }

        }
        public Tuple<bool, string> AddToCartFromEcomm(string strCategoryName, long lngItemCode, double ShippingCharge, string ShippingDescription, bool IsLineItem, int qty = 1, decimal discount = 0, long promotionID = 0, string promotionDesc = "", bool isShippingCharge = false, long parentItemCode = 0, string childKitSelectedValues = "")
        {
            try
            {
                int applypostupdiscount = 0;
                if (discount > 0)
                {
                    applypostupdiscount = 1;
                }
                if (qty <= 0)
                {
                    qty = 1;
                }
                if (lngItemCode == 0)
                {
                    ShowMessage(GetErrorMessage("ERR002"), 1);//Invalid Item.
                    return new Tuple<bool, string>(false, CCommon.ToString(GetErrorMessage("ERR002")));
                }
                //IStart Pinkal Patel Date : 07-11-2011 
                HttpCookie cookie = HttpContext.Current.Request.Cookies["Cart"];
                if (cookie == null)
                {
                    SetCookie(cookie);
                }
                //IEnd Pinkal Patel Date : 07-11-2011 
                ;
                DataSet ds = default(DataSet);
                DataTable dtItemDetails = default(DataTable);
                CItems objItems = new CItems();
                objItems.ItemCode = Sites.ToInteger(lngItemCode.ToString());
                objItems.DomainID = Sites.ToLong(HttpContext.Current.Session["DomainID"]);
                ds = objItems.GetProfileItems();
                if (ds.Tables.Count > 0)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        string[] strContainerItemCode = Convert.ToString(ds.Tables[0].Rows[0]["numOppAccAttrID"]).Split('~');
                        if (strContainerItemCode != null && strContainerItemCode.Length == 2)
                        {
                            objItems.ItemCode = Convert.ToInt32(strContainerItemCode[1]);
                        }
                    }
                }
                objItems.WarehouseID = Sites.ToLong(HttpContext.Current.Session["WareHouseID"]);

                objItems.SiteID = Sites.ToLong(HttpContext.Current.Session["SiteId"]);

                if ((!System.Web.HttpContext.Current.User.Identity.IsAuthenticated || CCommon.ToLong(HttpContext.Current.Session["DomainID"]) == 0 || CCommon.ToLong(HttpContext.Current.Session["UserContactID"]) == 0) && Sites.ToInteger(HttpContext.Current.Session["OnePageCheckout"]) == 0)
                {
                    //GUEST USER
                    objItems.DivisionID = 0;
                }
                else
                {
                    objItems.DivisionID = Sites.ToLong(HttpContext.Current.Session["DivId"]);
                }
                ds = objItems.ItemDetailsForEcomm();
                dtItemDetails = ds.Tables[0];

                if (dtItemDetails.Rows.Count > 0)
                {
                    var childKitSelectedItems = "";

                    if (1 > Sites.ToDecimal(dtItemDetails.Rows[0]["numOnHand"]) && CCommon.ToString(dtItemDetails.Rows[0]["charItemType"]).ToUpper() == "P" && !Sites.ToBool(dtItemDetails.Rows[0]["bitKitParent"]))
                    {
                        if (Sites.ToBool(dtItemDetails.Rows[0]["bitAllowBackOrder"]) == false)
                        {
                            ShowMessage(GetErrorMessage("ERR003"), 1);//Sufficient Quantity is not present in the inventory
                            return new Tuple<bool, string>(false, CCommon.ToString(GetErrorMessage("ERR003")));
                        }
                    }
                    else if (Sites.ToBool(dtItemDetails.Rows[0]["bitHasChildKits"]))
                    {
                        objItems = new CItems();
                        objItems.ItemCode = Convert.ToInt32(lngItemCode);
                        objItems.DomainID = Sites.ToLong(HttpContext.Current.Session["DomainID"]);

                        System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
                        System.Collections.Generic.List<ChildKitSelection> childKitSelection = serializer.Deserialize<System.Collections.Generic.List<ChildKitSelection>>(childKitSelectedValues);

                        if (childKitSelection != null)
                        {
                            foreach (ChildKitSelection objChildKitSelection in childKitSelection)
                            {
                                foreach (string selectedItems in objChildKitSelection.selectedItems)
                                {
                                    childKitSelectedItems += String.IsNullOrEmpty(childKitSelectedItems) ? (objChildKitSelection.numItemCode + "-" + selectedItems) : ("," + (objChildKitSelection.numItemCode + "-" + selectedItems));
                                }
                            }
                        }

                        objItems.vcKitChildItems = childKitSelectedItems;

                        if (!objItems.IsValidKitSelection())
                        {
                            ShowMessage(GetErrorMessage("ERR077"), 1);
                            return new Tuple<bool, string>(false, CCommon.ToString(GetErrorMessage("ERR077")));
                        }
                    }


                    DataSet dsTemp;
                    //Get items Added cart
                    //createSet();
                    dsTemp = GetCartItem(); //(DataSet)HttpContext.Current.Session["Data"];
                    DataTable dtItem = default(DataTable);
                    dtItem = dsTemp.Tables[0];
                    DataRow dr = default(DataRow);
                    dr = dtItem.NewRow();
                    dr["numOppItemCode"] = (dtItem.Compute("MAX(numOppItemCode)", "") == System.DBNull.Value ? 0 : Sites.ToInteger(dtItem.Compute("MAX(numOppItemCode)", "").ToString())) + 1;
                    dr["numItemCode"] = lngItemCode;
                    dr["numUnitHour"] = qty;
                    dr["numUOM"] = dtItemDetails.Rows[0]["numUOM"].ToString();
                    dr["vcUOMName"] = Sites.ToString(dtItemDetails.Rows[0]["vcUOMName"]) == "" ? "Units" : Sites.ToString(dtItemDetails.Rows[0]["vcUOMName"]);
                    decimal dConversionFactor = Sites.ToDecimal(dtItemDetails.Rows[0]["UOMConversionFactor"].ToString());
                    dr["decUOMConversionFactor"] = dConversionFactor;
                    string FreeShipping = dtItemDetails.Rows[0]["bitFreeShipping"].ToString() == "1" ? "True" : "False";
                    dr["bitFreeShipping"] = FreeShipping;
                    if (FreeShipping != "True")
                    {
                        dr["numWeight"] = dtItemDetails.Rows[0]["fltWeight"].ToString();
                        dr["numHeight"] = dtItemDetails.Rows[0]["fltHeight"].ToString();
                        dr["numWidth"] = dtItemDetails.Rows[0]["fltWidth"].ToString();
                        dr["numLength"] = dtItemDetails.Rows[0]["fltLength"].ToString();
                    }

                    if (IsLineItem)
                    {
                        dr["monPrice"] = ShippingCharge;
                        dr["vcItemDesc"] = ShippingDescription;
                    }
                    else
                    {
                        Double finalItemPrice = 0;

                        if (Sites.ToBool(dtItemDetails.Rows[0]["bitHasChildKits"]))
                        {
                            CItems objItem = new CItems();
                            objItem.DomainID = Sites.ToLong(HttpContext.Current.Session["DomainID"]);
                            objItem.SiteID = Sites.ToLong(HttpContext.Current.Session["SiteID"]);
                            objItem.DivisionID = Sites.ToLong(HttpContext.Current.Session["DivId"]);
                            objItem.ItemCode = Convert.ToInt32(lngItemCode);
                            objItem.Quantity = qty;
                            objItem.KitSelectedChild = childKitSelectedItems;

                            DataTable dtCalculatedPrice = objItem.GetItemCalculatedPrice();

                            if (dtCalculatedPrice != null && dtCalculatedPrice.Rows.Count > 0)
                            {
                                finalItemPrice = Sites.ToDouble(dtCalculatedPrice.Rows[0]["monPrice"]) / Sites.ToDouble(HttpContext.Current.Session["ExchangeRate"].ToString());
                            }


                        }
                        else
                        {
                            finalItemPrice = Sites.ToDouble(Sites.ToDecimal(dtItemDetails.Rows[0]["monListPrice"]) / Sites.ToDecimal(HttpContext.Current.Session["ExchangeRate"].ToString()));
                        }
                        /*Apply pricebook rule */
                        if (Sites.ToBool(ApplyPriceBookPrice) == true && Sites.ToBool(HttpContext.Current.Session["HidePrice"]) == false)
                        {
                            PriceBookRule objPbook = new PriceBookRule();
                            objPbook.ItemID = lngItemCode;
                            objPbook.QntyofItems = Sites.ToInteger(Sites.ToInteger(dr["numUnitHour"].ToString()) * Sites.ToDecimal(dConversionFactor));
                            objPbook.DomainID = Sites.ToLong(HttpContext.Current.Session["DomainID"]);
                            objPbook.DivisionID = Sites.ToLong(HttpContext.Current.Session["DivId"]);
                            objPbook.WarehouseID = Sites.ToLong(HttpContext.Current.Session["WareHouseID"]);
                            objPbook.SiteID = Sites.ToLong(HttpContext.Current.Session["SiteID"]);
                            objPbook.KitSelectedChild = childKitSelectedItems;
                            DataTable dtCalPrice = default(DataTable);
                            dtCalPrice = objPbook.GetPriceBasedonPriceBook();
                            if (dtCalPrice.Rows.Count > 0)
                            {
                                finalItemPrice = Sites.ToDouble(Sites.ToDecimal(dtCalPrice.Rows[0]["ListPrice"]) * Sites.ToDecimal(dConversionFactor) / Sites.ToDecimal(HttpContext.Current.Session["ExchangeRate"].ToString()));
                            }
                        }

                        dr["monPrice"] = finalItemPrice;
                        dr["vcItemDesc"] = CCommon.ToString(dtItemDetails.Rows[0]["txtItemDesc"]);
                    }
                    double percentagediscount = 0;
                    if (Sites.ToDouble(discount) > 0)
                    {
                        percentagediscount = (Sites.ToDouble(discount) / Sites.ToDouble(dr["monPrice"])) * 100;
                    }
                    dr["bitDiscountType"] = 0;
                    dr["fltDiscount"] = percentagediscount;
                    dr["monTotAmtBefDiscount"] = Sites.ToDouble(dr["numUnitHour"]) * Sites.ToDouble(dr["monPrice"]);
                    dr["monTotAmount"] = Sites.ToDouble(dr["numUnitHour"]) * Sites.ToDouble(dr["monPrice"]) - ((Sites.ToDouble(dr["numUnitHour"]) * Sites.ToDouble(dr["monPrice"])) * percentagediscount) / 100;
                    dr["numWarehouseID"] = Sites.ToLong(HttpContext.Current.Session["WareHouseID"]);
                    dr["vcItemName"] = Sites.ToString(dtItemDetails.Rows[0]["vcItemName"]);
                    dr["numWareHouseItmsID"] = Sites.ToLong(dtItemDetails.Rows[0]["numWareHouseItemID"]);


                    CCommon objCommon = new CCommon();
                    dr["vcAttributes"] = objCommon.GetAttributesForWarehouseItem(Sites.ToLong(dtItemDetails.Rows[0]["numWareHouseItemID"]), false);
                    dr["vcAttrValues"] = objCommon.GetAttributesIdsForWarehouseItem(Sites.ToLong(dtItemDetails.Rows[0]["numWareHouseItemID"]));
                    dr["PromotionID"] = promotionID;
                    dr["PromotionDesc"] = promotionDesc;
                    dr["vcChildKitItemSelection"] = childKitSelectedItems;

                    if (dtItemDetails.Rows[0]["charItemType"].ToString() == "P")
                        dr["vcItemType"] = "Inventory Item";
                    else if (dtItemDetails.Rows[0]["charItemType"].ToString() == "S")
                        dr["vcItemType"] = "Service Item";
                    else if (dtItemDetails.Rows[0]["charItemType"].ToString() == "N")
                        dr["vcItemType"] = "Non-Inventory Item";
                    else if (dtItemDetails.Rows[0]["charItemType"].ToString() == "A")
                        dr["vcItemType"] = "Accessory";

                    dr["tintOpFlag"] = 1;
                    dr["ItemURL"] = GetProductURL(strCategoryName, CCommon.ToString(dtItemDetails.Rows[0]["vcItemName"]), CCommon.ToString(dtItemDetails.Rows[0]["numItemCode"]));

                    /*Check if item already exist with same attributes, if yes then increment no of units to buy*/
                    foreach (DataRow item in dtItem.Rows)
                    {
                        //if (item["numItemCode"].ToString() == "123")
                        //    dataTable2.Rows.Add(dr.ItemArray);
                        //lngItemCode = CCommon.ToLong(item["numItemCode"]);
                        if (Sites.ToString(item["numItemCode"]) == Sites.ToString(dr["numItemCode"]) && Sites.ToString(item["vcAttrValues"]) == Sites.ToString(dr["vcAttrValues"]) && Sites.ToString(item["vcChildKitItemSelection"]) == Sites.ToString(dr["vcChildKitItemSelection"]))
                        {
                            //if shipping item then increase quantity than do not increase quantity
                            if (lngItemCode == Sites.ToLong(HttpContext.Current.Session["ShippingServiceItemID"]))
                            {
                                item["numUnitHour"] = "1";
                                item["monPrice"] = ShippingCharge;
                                item["vcItemDesc"] = ShippingDescription;
                                item["monTotAmtBefDiscount"] = Sites.ToDouble(dr["numUnitHour"]) * Sites.ToDouble(dr["monPrice"]);
                                item["monTotAmount"] = dr["monTotAmtBefDiscount"];
                            }
                            else //if(CCommon.ToLong(item["numItemCode"]) == lngItemCode || CCommon.ToLong(item["numParentItemCode"]) == lngItemCode)
                            {
                                long tempNumItemCode = 0;
                                long tempNumParentItemCode = 0;
                                long numItemCode = CCommon.ToLong(item["numItemCode"]);

                                foreach (DataRow drtemp in dtItem.Rows)
                                {
                                    tempNumItemCode = CCommon.ToLong(drtemp["numItemCode"]);
                                    tempNumParentItemCode = CCommon.ToLong(drtemp["numParentItemCode"]);

                                    if (tempNumItemCode == numItemCode || tempNumParentItemCode == numItemCode)
                                    {
                                        drtemp["numUnitHour"] = CCommon.ToInteger(drtemp["numUnitHour"]) + qty;

                                        if (percentagediscount > 0)
                                        {
                                            drtemp["monTotAmtBefDiscount"] = Sites.ToDouble(drtemp["numUnitHour"]) * Sites.ToDouble(dr["monPrice"]);
                                            drtemp["monTotAmount"] = Sites.ToDouble(drtemp["numUnitHour"]) * Sites.ToDouble(dr["monPrice"]) - ((Sites.ToDouble(drtemp["numUnitHour"]) * Sites.ToDouble(drtemp["monPrice"])) * percentagediscount) / 100;
                                        }
                                        else
                                        {
                                            drtemp["monTotAmount"] = Sites.ToDouble(drtemp["numUnitHour"]) * Sites.ToDouble(dr["monPrice"]);
                                            drtemp["monTotAmtBefDiscount"] = Sites.ToDouble(drtemp["numUnitHour"]) * Sites.ToDouble(drtemp["monPrice"]);
                                        }
                                    }
                                }
                            }
                            dsTemp.AcceptChanges();
                            UpdateCart(dsTemp, applypostupdiscount);
                            UpdateTotal(dsTemp, IsLineItem);
                            return new Tuple<bool, string>(true, "");
                        }
                    }
                    dtItem.Rows.Add(dr);
                    dsTemp.AcceptChanges();
                    //Add New Item Into The Database.
                    InsertToCart(dr, applypostupdiscount, isShippingCharge, parentItemCode: parentItemCode);
                    UpdateTotal(dsTemp, IsLineItem);
                }

                return new Tuple<bool, string>(true, "");
            }
            catch (Exception ex)
            {
                ExceptionModule.ExceptionPublish(ex, Sites.ToLong(HttpContext.Current.Session["DomainID"]), Sites.ToLong(HttpContext.Current.Session["UserContactID"]), Sites.ToLong(HttpContext.Current.Session["SiteId"]), HttpContext.Current.Request);
                ShowMessage(ex.ToString(), 1);
                return new Tuple<bool, string>(false, ex.ToString());
            }
        }

        public double GetCartSubTotal(bool isFromGuestCheckOut = false)
        {
            try
            {
                DataSet ds = new DataSet();
                ds = GetCartItem(isFromGuestCheckout: isFromGuestCheckOut);

                double subTotal = 0;

                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        if (Sites.ToLong(dr["numItemCode"]) != Sites.ToLong(HttpContext.Current.Session["ShippingServiceItemID"]) && Sites.ToLong(dr["numItemCode"]) != Sites.ToLong(HttpContext.Current.Session["DiscountServiceItemID"]))
                        {
                            subTotal += Sites.ToDouble(dr["monTotAmount"]);
                        }

                    }
                }

                return subTotal;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void DeleteItemFromCart(long ItemCode)
        {
            try
            {
                BACRM.BusinessLogic.ShioppingCart.Cart objcart;
                objcart = new BACRM.BusinessLogic.ShioppingCart.Cart();
                HttpCookie cookie = HttpContext.Current.Request.Cookies["Cart"];

                objcart.DomainID = CCommon.ToLong(HttpContext.Current.Session["DomainID"]);
                objcart.ContactId = CCommon.ToLong(HttpContext.Current.Session["UserContactID"]);
                objcart.CookieId = (cookie != null) ? cookie["KeyId"] : "";
                objcart.ItemCode = ItemCode;
                objcart.bitDeleteAll = false;
                objcart.SiteID = Sites.ToLong(HttpContext.Current.Session["SiteId"]);
                objcart.RemoveItemFromCart();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool CheckEmptyCart(bool isFromGuestCheckout = false)
        {
            //Cart empty Validation
            DataTable dt = GetItems(isFromGuestCheckout: isFromGuestCheckout);
            if (dt.Rows.Count == 0)
            {
                ShowMessage(GetErrorMessage("ERR001"), 1);//Cart is empty
                HttpContext.Current.Session["SelectedStep"] = Sites.ToLong(HttpContext.Current.Session["DivId"]) == 0 ? 0 : 1;
                //bCharge.Visible = false;
                //tblBillme.Visible = false;
                //tblCustomerInfo.Visible = false;
                return false;
            }
            HttpContext.Current.Session["CartItems"] = dt;
            return true;
        }
        public bool CheckForExecution(int TabIndex)
        {
            try
            {
                bool AllowForExecution = false;
                if (Sites.ToBool(HttpContext.Current.Session["IsOnePageCheckout"]))
                {
                    if (Sites.ToInteger(HttpContext.Current.Session["SelectedStep"]) >= TabIndex)
                        AllowForExecution = true;
                }
                else
                    AllowForExecution = true;

                return AllowForExecution;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string GetCartItemsXML(DataSet ds)
        {
            string strXML = "";

            try
            {
                DataTable dt = new DataTable("Table1");
                dt.Columns.Add("numCartID");
                dt.Columns.Add("numDomainId");
                dt.Columns.Add("numUserCntId");
                dt.Columns.Add("vcCookieId");
                dt.Columns.Add("numItemCode");
                dt.Columns.Add("monPrice");
                dt.Columns.Add("numUnitHour");
                dt.Columns.Add("bitDiscountType");
                dt.Columns.Add("monTotAmtBefDiscount");
                dt.Columns.Add("fltDiscount");
                dt.Columns.Add("monTotAmount");
                dt.Columns.Add("vcShippingMethod");
                dt.Columns.Add("numServiceTypeID");
                dt.Columns.Add("decShippingCharge");
                dt.Columns.Add("tintServiceType");
                dt.Columns.Add("numShippingCompany");
                dt.Columns.Add("dtDeliveryDate");

                DataRow dr;
                HttpCookie cookie = GetCookie();
                if (ds != null && ds.Tables.Count > 0)
                {
                    for (int i = 0; i <= ds.Tables[0].Rows.Count - 1; i++)
                    {
                        dr = dt.NewRow();
                        dr["numDomainId"] = CCommon.ToLong(HttpContext.Current.Session["DomainId"]);
                        dr["numUserCntId"] = CCommon.ToLong(HttpContext.Current.Session["UserContactID"]);
                        dr["vcCookieId"] = (cookie["KeyId"] != null) ? cookie["KeyId"] : "";
                        dr["numCartID"] = ds.Tables[0].Rows[i]["numCartId"];
                        dr["numItemCode"] = ds.Tables[0].Rows[i]["numItemCode"];
                        dr["monPrice"] = ds.Tables[0].Rows[i]["monPrice"];
                        dr["numUnitHour"] = ds.Tables[0].Rows[i]["numUnitHour"];
                        dr["monTotAmtBefDiscount"] = ds.Tables[0].Rows[i]["monTotAmtBefDiscount"];
                        dr["monTotAmount"] = ds.Tables[0].Rows[i]["monTotAmount"];
                        dr["fltDiscount"] = ds.Tables[0].Rows[i]["fltDiscount"];
                        dr["bitDiscountType"] = ds.Tables[0].Rows[i]["bitDiscountType"];
                        dr["vcShippingMethod"] = ds.Tables[0].Rows[i]["vcShippingMethod"];
                        dr["numServiceTypeID"] = ds.Tables[0].Rows[i]["numServiceTypeID"];
                        dr["decShippingCharge"] = ds.Tables[0].Rows[i]["decShippingCharge"];
                        dr["tintServiceType"] = ds.Tables[0].Rows[i]["tintServiceType"];
                        dr["numShippingCompany"] = ds.Tables[0].Rows[i]["numShippingCompany"];
                        dr["dtDeliveryDate"] = ds.Tables[0].Rows[i]["dtDeliveryDate"];
                        dt.Rows.Add(dr);
                        dt.AcceptChanges();
                    }
                }

                DataSet tempds = new DataSet();
                tempds.Tables.Add(dt);
                strXML = tempds.GetXml();
                return strXML;
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion

        #region ErrorMessage
        public string GetErrorMessage(string ErrorCode)
        {
            try
            {
                ErrorMessage e1 = new ErrorMessage();
                e1.CultureID = 0;
                e1.DomainID = CCommon.ToInteger(HttpContext.Current.Session["DomainId"]);
                e1.SiteID = CCommon.ToInteger(HttpContext.Current.Session["SiteId"]);
                e1.Application = CCommon.ToInteger(ErrorMessage.GetApplication.ECommerce);
                e1.ErrorCode = ErrorCode;
                return e1.GetErrorMessages();
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        public void ShowMessage(string Message, int IsError)
        {
            this.Page.ClientScript.RegisterStartupScript(this.GetType(), "Msg", "<script>Highlight('MessageBox','" + HttpUtility.JavaScriptStringEncode(Message) + "','" + IsError.ToString() + "');</script>");
        }
        #endregion

        #region Product , Items , Images  , Cobtrols ,State ,Checkout
        public string GetImagePath(string ImageFileName)
        {
            try
            {
                if (ImageFileName.StartsWith("http"))
                {
                    return ImageFileName;
                }
                else
                {
                    string path = Sites.ToString(HttpContext.Current.Session["ItemImagePath"]) + "/" + ImageFileName;
                    if (HttpContext.Current.Request.IsSecureConnection)
                    {
                        return path.ReplaceFirst("http", "https");
                    }
                    else
                    {
                        return path;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public string GetProductURL(string CategoryName, string ProductName, string ItemCode)
        {
            try
            {
                string strURL = Sites.ToString(HttpContext.Current.Session["SitePath"]) + "/Product/" + Sites.GenerateSEOFriendlyURL(CategoryName) + "/" + Sites.GenerateSEOFriendlyURL(ProductName) + "/" + ItemCode;
                return strURL;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        //Will be used from Product.ascx and list.ascx
        //public void createSet()
        //{
        //    try
        //    {
        //        if (HttpContext.Current.Session["Data"] == null)
        //        {
        //            DataSet dsTemp = new DataSet();
        //            DataTable dtItem = new DataTable();
        //            dtItem.Columns.Add("numoppitemtCode", typeof(int));
        //            dtItem.Columns.Add("numItemCode");
        //            dtItem.Columns.Add("numUnitHour");
        //            dtItem.Columns.Add("monPrice", typeof(decimal));
        //            dtItem.Columns.Add("monTotAmount", typeof(decimal));
        //            dtItem.Columns.Add("numSourceID");
        //            dtItem.Columns.Add("vcItemDesc");
        //            dtItem.Columns.Add("numWarehouseID");
        //            dtItem.Columns.Add("vcItemName");
        //            dtItem.Columns.Add("Warehouse");
        //            dtItem.Columns.Add("numWarehouseItmsID");
        //            dtItem.Columns.Add("ItemType");
        //            dtItem.Columns.Add("Attributes");
        //            dtItem.Columns.Add("AttrValues");
        //            dtItem.Columns.Add("FreeShipping");
        //            dtItem.Columns.Add("Weight");
        //            dtItem.Columns.Add("Op_Flag");
        //            dtItem.Columns.Add("bitDiscountType", typeof(bool));
        //            dtItem.Columns.Add("fltDiscount", typeof(decimal));
        //            dtItem.Columns.Add("monTotAmtBefDiscount", typeof(decimal));
        //            dtItem.Columns.Add("ItemURL", typeof(string));//this url will be used in Cart Grid to show link

        //            dtItem.Columns.Add("numUOM");
        //            dtItem.Columns.Add("vcUOMName");
        //            dtItem.Columns.Add("UOMConversionFactor");
        //            dtItem.Columns.Add("Height");
        //            dtItem.Columns.Add("Length");
        //            dtItem.Columns.Add("Width");
        //            dtItem.Columns.Add("vcShippingMethod");
        //            dtItem.Columns.Add("numServiceTypeID", typeof(string));
        //            dtItem.Columns.Add("ShippingCharge", typeof(decimal));
        //            dtItem.Columns.Add("numShippingCompany");
        //            dtItem.Columns.Add("tintServiceType");//i.e Nsoft Enum which represents shipping method
        //            dtItem.Columns.Add("DeliveryDate");

        //            dtItem.TableName = "Item";
        //            dsTemp.Tables.Add(dtItem);
        //            dtItem.PrimaryKey = new DataColumn[] { dtItem.Columns["numoppitemtCode"] };
        //            HttpContext.Current.Session["Data"] = dsTemp;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        public double GetShippingChargebyPromotion(decimal shippingamt = 0)
        {
            BACRM.BusinessLogic.ShioppingCart.Cart _cart = new BACRM.BusinessLogic.ShioppingCart.Cart();
            _cart.DomainID = Sites.ToLong(HttpContext.Current.Session["DomainID"]);
            _cart.CountryID = Sites.ToInteger(HttpContext.Current.Session["DefCountry"]);
            //_cart.CountryID = Sites.ToInteger(HttpContext.Current.Session["DefCountry"]);
            _cart.UserCntID = CCommon.ToLong(HttpContext.Current.Session["UserContactID"]);
            _cart.ShippingCharge = shippingamt;
            HttpCookie cookie = HttpContext.Current.Request.Cookies["Cart"];
            if (HttpContext.Current.Request.Cookies["Cart"] != null)
            {
                _cart.CookieId = (cookie["KeyId"] != null) ? cookie["KeyId"] : "";
            }
            else
            {
                _cart.CookieId = "";
            }
            DataTable dt = new DataTable();
            dt = _cart.GetShippingChargebyPromotion();
            if (dt.Rows.Count > 0)
            {
                return Sites.ToDouble(dt.Rows[0][0]);
            }
            else
            {
                return 0;
            }
        }
        public DataTable GetItems(bool isFromGuestCheckout = false)
        {
            try
            {


                DataSet dsItems = GetCartItem(isFromGuestCheckout: isFromGuestCheckout);
                DataTable dt;
                if (dsItems.Tables.Count > 0)
                {
                    dt = dsItems.Tables[0];
                }
                else
                    dt = new DataTable();
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void FillState(DropDownList ddl, long lngCountry, long lngDomainID)
        {
            try
            {
                DataTable dtTable = default(DataTable);
                UserAccess objUserAccess = new UserAccess();
                objUserAccess.DomainID = lngDomainID;
                objUserAccess.Country = lngCountry;
                dtTable = objUserAccess.SelState();
                DataRow dr;
                dr = dtTable.NewRow();
                dr["vcState"] = "--Select One--";
                dr["numStateID"] = 0;
                dtTable.Rows.InsertAt(dr, 0);
                ddl.DataSource = dtTable;
                ddl.DataTextField = "vcState";
                ddl.DataValueField = "numStateID";
                ddl.DataBind();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public string GetUserControlTemplate(string DefaultTemplateFileName)
        {
            try
            {
                if (HtmlCustomize != null && HtmlCustomize.Length > 0)
                {
                    return HttpUtility.HtmlDecode(HtmlCustomize);
                }
                else
                {
                    return Server.HtmlDecode(Sites.ReadFile(ConfigurationManager.AppSettings["CartLocation"].ToString() + "\\Default\\Elements\\" + DefaultTemplateFileName));
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public string CheckoutPageName()
        {
            if (Sites.ToBool(HttpContext.Current.Session["IsOnePageCheckout"]))
                return "onepagecheckout.aspx";
            else
                return "OrderSummary.aspx";
        }
        protected T GetControl<T>(string ControlName, object sender) where T : System.Web.UI.Control
        {
            return (T)((System.Web.UI.WebControls.Button)sender).NamingContainer.FindControl(ControlName);
        }

        #endregion

        #region Review and Ratings
        public string GetRatingContol(string Name, bool IsDisabled, bool IsAverage, double CheckCount, bool IsSplit, int SplitCount, string CssClass, bool IsRequired, string[] title)
        {
            try
            {
                string strRating = "";
                string strCssClass = CssClass;
                string strChecked = "";
                string strDisabled = "";
                int titleCounter = 0;
                int RatingLimit = 5;
                int FinalCheckCount = 0;
                double Remainder = 0;
                if (IsAverage)
                {
                    Remainder = CheckCount % 1;
                    FinalCheckCount = ((int)CheckCount) * SplitCount;

                    if (Remainder > 0)
                    {
                        if (Remainder > 0.5)
                        {
                            FinalCheckCount = FinalCheckCount + 2;
                        }
                        else
                        {
                            FinalCheckCount = FinalCheckCount + 1;
                        }
                    }
                }
                else
                {
                    FinalCheckCount = (int)CheckCount;
                }

                if (IsSplit)
                {
                    strCssClass = strCssClass + " {split:" + SplitCount + "} ";
                    RatingLimit = RatingLimit * SplitCount;
                }


                if (IsDisabled)
                {
                    strDisabled = "disabled= \"disabled\"";
                }
                else
                {
                    strDisabled = "";
                }

                string strTempCssClass = "";
                for (int i = 1; i <= RatingLimit; i++)
                {
                    if (i == FinalCheckCount)
                    {
                        strChecked = "checked = \"checked\"";
                    }
                    else
                    {
                        strChecked = "";
                    }


                    if (IsRequired)
                    {
                        if (i == 1)
                        {
                            strTempCssClass = strCssClass;
                            strCssClass = strCssClass + " required";
                        }
                        else
                        {
                            strCssClass = strTempCssClass;
                        }
                    }
                    //Name + "_" + CCommon.ToString(i)
                    //"test-1-rating-5"
                    strRating = strRating + "<input class=\"" + strCssClass + "\" type= \"radio\"  name= \"" + Name + "\" value= \"" + i + "\" title = \"" + title[titleCounter] + "\"  " + strDisabled + " " + strChecked + " />";
                    if (IsSplit != true)
                    {
                        titleCounter++;
                    }
                    else
                    {
                        if (i % SplitCount == 0)
                        {
                            titleCounter++;
                        }
                    }
                }

                return strRating;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        public string GetIpAddress()
        {
            try
            {
                string strIpAddress;
                strIpAddress = HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
                if (strIpAddress == null)
                {
                    strIpAddress = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];
                }
                else
                {
                    string[] ipRange = strIpAddress.Split(',');
                    int le = ipRange.Length - 1;
                    string trueIP = ipRange[le];
                }

                return strIpAddress;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        #endregion

        #region Tax Calculations
        public decimal GetTaxForCartItems(long DomainId, long DivisionID, int BaseTaxCalcOn, int BaseTaxOnArea, int CountryId, int StateId, string City, string PostalCode, long ContactID)
        {
            try
            {
                BACRM.BusinessLogic.ShioppingCart.Cart objCartItemTax = new BACRM.BusinessLogic.ShioppingCart.Cart();
                HttpCookie cookie = HttpContext.Current.Request.Cookies["Cart"];
                if (HttpContext.Current.Request.Cookies["Cart"] != null)
                {
                    objCartItemTax.CookieId = (cookie["KeyId"] != null) ? cookie["KeyId"] : "";
                }
                else
                {
                    objCartItemTax.CookieId = "";
                }

                objCartItemTax.DomainID = DomainId;
                objCartItemTax.DivisionID = DivisionID;
                objCartItemTax.BaseTaxOn = BaseTaxCalcOn;
                objCartItemTax.CountryID = CountryId;
                objCartItemTax.StateID = StateId;
                objCartItemTax.City = City;
                objCartItemTax.ZipPostalCode = PostalCode;
                objCartItemTax.TotalTaxAmount = 0;
                objCartItemTax.ContactId = ContactID;

                //This method  Set TotalTaxAmount Properties ...
                objCartItemTax.GetTaxOfCartItems();

                return CCommon.ToDecimal(objCartItemTax.TotalTaxAmount);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region
        public HtmlMeta GetMetaTag(string key, string value)
        {
            try
            {
                HtmlMeta tag = new HtmlMeta();
                tag.Name = key;
                tag.Content = value;

                return tag;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Others
        //Used for ItemList
        /// <summary>
        /// 
        /// </summary>
        /// <param name="strInput">i.e.ItemClass=GridItem;AlternatingItemClass=GridAltItem;</param>
        /// <returns></returns>
        public System.Collections.Hashtable GetPropertiesAsHastable(string strInput)
        {
            try
            {
                System.Collections.Hashtable ht = new System.Collections.Hashtable();
                string[] strKeyValuePair;
                strKeyValuePair = strInput.Split(char.Parse(";"));
                for (int i = 0; i < strKeyValuePair.Length; i++)
                {
                    if (strKeyValuePair[i].Contains("="))
                        ht.Add(strKeyValuePair[i].Split(char.Parse("="))[0], strKeyValuePair[i].Split(char.Parse("="))[1]);
                }
                return ht;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public string GetIndexPath()
        {
            return BACRM.BusinessLogic.Common.CCommon.GetLuceneIndexPath(Sites.ToLong(HttpContext.Current.Session["DomainID"]), Sites.ToLong(HttpContext.Current.Session["SiteID"]));
            //return BACRM.BusinessLogic.Common.CCommon.GetDocumentPhysicalPath(Sites.ToLong(HttpContext.Current.Session["DomainID"])) + Sites.ToString(HttpContext.Current.Session["SiteID"]) + "\\ItemIndex";
        }
        public void UpdateTotal(DataSet dsTemp, bool IsLineItem)
        {
            try
            {
                DataTable dtTable = default(DataTable);
                dtTable = dsTemp.Tables[0];
                //Update Total Amount

                //string CartCount = " <a href=" + Sites.ToString(HttpContext.Current.Session["SitePath"]) + "/cart.aspx>(" + dtTable.Rows.Count.ToString() + ")</a>";
                //string strFinalWithItemCount = GetErrorMessage("ERR004").Replace("##ItemCount##", CartCount); //Item added to cart successfully.##ItemCount##
                if (!IsLineItem)
                {
                    if (dtTable.Rows.Count > 0)
                        HttpContext.Current.Session["TotalAmount"] = Sites.ToDecimal(dtTable.Compute("SUM(monTotAmount)", ""));
                    HttpContext.Current.Session["ShippingMethodConfirmed"] = "False";


                    HttpContext.Current.Session["SelectedStep"] = 0;
                    string CartCount = " <a href=" + Sites.ToString(HttpContext.Current.Session["SitePath"]) + "/cart.aspx>View Cart (" + CCommon.ToString(dtTable.Rows.Count) + ")</a>";

                    //this.Page.ClientScript.RegisterStartupScript(this.GetType(), "ShowMiniCart", "ShowMiniCart();", true);

                    //string strFinalWithItemCount = GetErrorMessage("ERR004").Replace("##ItemCount##", CartCount); //Items added to cart successfully .
                    //ShowMessage(strFinalWithItemCount, 0);
                }
                HttpContext.Current.Session["gblItemCount"] = null;
                HttpContext.Current.Session["gblTotalAmount"] = null;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
        #endregion
    }

    public class CustomException : Exception
    {
        public CustomException()
            : base() { }

        public CustomException(string message)
            : base(message) { }

        public CustomException(string format, params object[] args)
            : base(string.Format(format, args)) { }

        public CustomException(string message, Exception innerException)
            : base(message, innerException) { }

        public CustomException(string format, Exception innerException, params object[] args)
            : base(string.Format(format, args), innerException) { }
    }

    public static class StringExtension
    {
        public static string ReplaceFirst(this string text, string search, string replace)
        {
            int pos = text.IndexOf(search);
            if (pos < 0)
            {
                return text;
            }
            return text.Substring(0, pos) + replace + text.Substring(pos + search.Length);
        }
    }
}
