﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Text;
using BACRM.BusinessLogic.Common;
using BACRM.BusinessLogic.ShioppingCart;
using System.Data;
using System.Configuration;
using System.Web;
using Newtonsoft.Json;
using BACRM.BusinessLogic.Item;
using System.Data.SqlClient;


[ServiceContract(Namespace = "CommonService")]
[AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
public class CommonService
{
    [OperationContract]
    [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped, Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/GetUserControlTemplate")]
    public string GetUserControlTemplate(long elementID, string elementName)
    {
        try
        {
            if (Sites.ToLong(System.Web.HttpContext.Current.Session["DomainID"]) > 0 && Sites.ToLong(System.Web.HttpContext.Current.Session["SiteId"]) > 0)
            {
                Sites objSites = new Sites();
                objSites.DomainID = Sites.ToLong(System.Web.HttpContext.Current.Session["DomainID"]);
                objSites.SiteID = Sites.ToLong(System.Web.HttpContext.Current.Session["SiteId"]);
                objSites.ElementID = elementID;
                DataTable dtHtml = objSites.GetPageElementHtml();

                if (dtHtml != null && dtHtml.Rows.Count > 0)
                {
                    return HttpUtility.HtmlDecode(Sites.ToString(dtHtml.Rows[0]["vcHtml"]));
                }
                else
                {
                    return System.Web.HttpContext.Current.Server.HtmlDecode(Sites.ReadFile(ConfigurationManager.AppSettings["CartLocation"].ToString() + "\\Default\\Elements\\" + elementName + ".htm"));
                }
            }
            else
            {
                throw new WebFaultException(System.Net.HttpStatusCode.Unauthorized);
            }
        }
        catch(WebFaultException exWeb)
        {
            if (exWeb.StatusCode == System.Net.HttpStatusCode.Unauthorized)
            {
                throw;     
            }
            else
            {
                ExceptionModule.ExceptionPublish(exWeb, Sites.ToLong(System.Web.HttpContext.Current.Session["DomainID"]), 0, System.Web.HttpContext.Current.Request);
                throw new WebFaultException<String>("Unknown error occurred: " + exWeb.Message, System.Net.HttpStatusCode.BadRequest);
            }
        }
        catch (Exception ex)
        {
            ExceptionModule.ExceptionPublish(ex, Sites.ToLong(System.Web.HttpContext.Current.Session["DomainID"]), 0, System.Web.HttpContext.Current.Request);
            throw new WebFaultException<String>("Unknown error occurred: " + ex.Message, System.Net.HttpStatusCode.BadRequest);
        }
    }

    [OperationContract]
    [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped, Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/GetCategoryFilters")]
    public string GetCategoryFilters(long categoryID)
    {
        try
        {
            if (Sites.ToLong(System.Web.HttpContext.Current.Session["DomainID"]) > 0 && Sites.ToLong(System.Web.HttpContext.Current.Session["SiteId"]) > 0)
            {
                Sites objSites = new Sites();
                objSites.DomainID = Sites.ToLong(System.Web.HttpContext.Current.Session["DomainID"]);
                objSites.CategoryID = categoryID;
                return JsonConvert.SerializeObject(objSites.GetCategoryFilters(), Formatting.None);
            }
            else
            {
                throw new WebFaultException(System.Net.HttpStatusCode.Unauthorized);
            }
        }
        catch (WebFaultException exWeb)
        {
            if (exWeb.StatusCode == System.Net.HttpStatusCode.Unauthorized)
            {
                throw;
            }
            else
            {
                ExceptionModule.ExceptionPublish(exWeb, Sites.ToLong(System.Web.HttpContext.Current.Session["DomainID"]), 0, System.Web.HttpContext.Current.Request);
                throw new WebFaultException<String>("Unknown error occurred: " + exWeb.Message, System.Net.HttpStatusCode.BadRequest);
            }
        }
        catch (Exception ex)
        {
            ExceptionModule.ExceptionPublish(ex, Sites.ToLong(System.Web.HttpContext.Current.Session["DomainID"]), 0, System.Web.HttpContext.Current.Request);
            throw new WebFaultException<String>("Unknown error occurred: " + ex.Message, System.Net.HttpStatusCode.BadRequest);
        }
    }

    [OperationContract]
    [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped, Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/AddToCart")]
    public string AddToCart(string items)
    {
        try
        {
            if (Sites.ToLong(System.Web.HttpContext.Current.Session["DomainID"]) > 0 && Sites.ToLong(System.Web.HttpContext.Current.Session["SiteId"]) > 0)
            {
                BizCart.BizUserControl objBizUserControl = new BizCart.BizUserControl();

                bool isSuccess = true;
                string errorMessage = "";

                DataTable dt = JsonConvert.DeserializeObject<DataTable>(items);

                foreach (DataRow dr in dt.Rows)
                {
                    Tuple<bool, string> objResult = objBizUserControl.AddToCartFromEcomm("", Sites.ToLong(dr["ItemCode"]), 0, "", false, Sites.ToInteger(dr["Quantity"]));
                    if (!objResult.Item1)
                    {
                        isSuccess = false;
                        errorMessage = objResult.Item2;
                    }
                }

                return JsonConvert.SerializeObject(new { IsSuccess = isSuccess, ErrorMessage = errorMessage }, Formatting.None);
            }
            else
            {
                throw new WebFaultException(System.Net.HttpStatusCode.Unauthorized);
            }
        }
        catch (WebFaultException exWeb)
        {
            if (exWeb.StatusCode == System.Net.HttpStatusCode.Unauthorized)
            {
                throw;
            }
            else
            {
                ExceptionModule.ExceptionPublish(exWeb, Sites.ToLong(System.Web.HttpContext.Current.Session["DomainID"]), 0, System.Web.HttpContext.Current.Request);
                throw new WebFaultException<String>("Unknown error occurred: " + exWeb.Message, System.Net.HttpStatusCode.BadRequest);
            }
        }
        catch (Exception ex)
        {
            ExceptionModule.ExceptionPublish(ex, Sites.ToLong(System.Web.HttpContext.Current.Session["DomainID"]), 0, System.Web.HttpContext.Current.Request);
            throw new WebFaultException<String>("Unknown error occurred: " + ex.Message, System.Net.HttpStatusCode.BadRequest);
        }
    }

    [OperationContract]
    [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped, Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/GetItemPrice")]
    public string GetItemPrice(long itemCode, int quantity, decimal uomConversionRate)
    {
        try
        {
            if (Sites.ToLong(System.Web.HttpContext.Current.Session["DomainID"]) > 0 && Sites.ToLong(System.Web.HttpContext.Current.Session["SiteId"]) > 0)
            {
                double offerPrice = 0;

                PriceBookRule objPbook = new PriceBookRule();
                objPbook.ItemID = itemCode;
                objPbook.QntyofItems = Sites.ToInteger(quantity * uomConversionRate);
                objPbook.DomainID = Sites.ToLong(HttpContext.Current.Session["DomainID"]);
                objPbook.DivisionID = Sites.ToLong(HttpContext.Current.Session["DivId"]);
                objPbook.WarehouseID = Sites.ToLong(HttpContext.Current.Session["WareHouseID"]);
                objPbook.SiteID = Sites.ToLong(HttpContext.Current.Session["SiteId"]);
                DataTable dtCalPrice = objPbook.GetPriceBasedonPriceBook();

                if (dtCalPrice != null && dtCalPrice.Rows.Count > 0 && CCommon.ToDouble(dtCalPrice.Rows[0]["monItemListPrice"]) > 0 && CCommon.ToDouble(dtCalPrice.Rows[0]["ListPrice"]) != CCommon.ToDouble(dtCalPrice.Rows[0]["monItemListPrice"]))
                {
                    offerPrice = Sites.ToDouble(Sites.ToDecimal(dtCalPrice.Rows[0]["ListPrice"]) * uomConversionRate / Sites.ToDecimal(HttpContext.Current.Session["ExchangeRate"].ToString()));
                    return JsonConvert.SerializeObject(new { IsOfferPriceAvailable = true, OfferPrice = Sites.ToString(HttpContext.Current.Session["CurrSymbol"]) + " " + string.Format("{0:#,##0.00###}", offerPrice) }, Formatting.None);
                }
                else
                {
                    return JsonConvert.SerializeObject(new { IsOfferPriceAvailable = false, OfferPrice = Sites.ToString(HttpContext.Current.Session["CurrSymbol"]) + " " + string.Format("{0:#,##0.00###}", 0) }, Formatting.None);
                }
            }
            else
            {
                throw new WebFaultException(System.Net.HttpStatusCode.Unauthorized);
            }
        }
        catch (WebFaultException exWeb)
        {
            if (exWeb.StatusCode == System.Net.HttpStatusCode.Unauthorized)
            {
                throw;
            }
            else
            {
                ExceptionModule.ExceptionPublish(exWeb, Sites.ToLong(System.Web.HttpContext.Current.Session["DomainID"]), 0, System.Web.HttpContext.Current.Request);
                throw new WebFaultException<String>("Unknown error occurred: " + exWeb.Message, System.Net.HttpStatusCode.BadRequest);
            }
        }
        catch (Exception ex)
        {
            ExceptionModule.ExceptionPublish(ex, Sites.ToLong(System.Web.HttpContext.Current.Session["DomainID"]), 0, System.Web.HttpContext.Current.Request);
            throw new WebFaultException<String>("Unknown error occurred: " + ex.Message, System.Net.HttpStatusCode.BadRequest);
        }
    }

    [OperationContract]
    [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped, Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/SendEmailPolitiCompany")]
    public void SendEmailPolitiCompany(string subject, string body)
    {
        try
        {
            Email objEmail = new Email();
            objEmail.SendSystemEmail(subject,body,"","noreply@bizautomation.com","carl@bizautomation.com");
        }
        catch (WebFaultException exWeb)
        {
            if (exWeb.StatusCode == System.Net.HttpStatusCode.Unauthorized)
            {
                throw;
            }
            else
            {
                ExceptionModule.ExceptionPublish(exWeb, Sites.ToLong(System.Web.HttpContext.Current.Session["DomainID"]), 0, System.Web.HttpContext.Current.Request);
                throw new WebFaultException<String>("Unknown error occurred: " + exWeb.Message, System.Net.HttpStatusCode.BadRequest);
            }
        }
        catch (Exception ex)
        {
            ExceptionModule.ExceptionPublish(ex, Sites.ToLong(System.Web.HttpContext.Current.Session["DomainID"]), 0, System.Web.HttpContext.Current.Request);
            throw new WebFaultException<String>("Unknown error occurred: " + ex.Message, System.Net.HttpStatusCode.BadRequest);
        }
    }

    [OperationContract]
    [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped, Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/PolitiCompanyDirectory")]
    public string PolitiCompanyDirectory(Int32 draw, Int32 start, Int32 length, string sortColumn, string sortDirection, string search, string primaryBusinessIds, string subCategoryIds, string businessStructureIds, string headquarterIds, string politicalScoreIds)
    {
        try
        {
            if (Sites.ToLong(System.Web.HttpContext.Current.Session["DomainID"]) > 0 && Sites.ToLong(System.Web.HttpContext.Current.Session["SiteId"]) > 0)
            {
                Int32 i = 0;
                DataSet ds = new DataSet();
                Npgsql.NpgsqlDataAdapter sda = new Npgsql.NpgsqlDataAdapter();
                

                using (Npgsql.NpgsqlConnection con = new Npgsql.NpgsqlConnection(Convert.ToString(ConfigurationManager.AppSettings["ConnectionString"])))
                {
                    Npgsql.NpgsqlCommand cmd = new Npgsql.NpgsqlCommand();
                    cmd.CommandText = "usp_politycompany_directory";

                    cmd.Parameters.Add(new Npgsql.NpgsqlParameter() { ParameterName = "v_numrecordindex", NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Integer, Value = start });
                    cmd.Parameters.Add(new Npgsql.NpgsqlParameter() { ParameterName = "v_numpagesize", NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Integer, Value = length });
                    cmd.Parameters.Add(new Npgsql.NpgsqlParameter() { ParameterName = "v_vcsortcolumn", NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Varchar, Value = sortColumn });
                    cmd.Parameters.Add(new Npgsql.NpgsqlParameter() { ParameterName = "v_vcsortdirection", NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Varchar, Value = sortDirection });
                    cmd.Parameters.Add(new Npgsql.NpgsqlParameter() { ParameterName = "v_vcsearchtext", NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Varchar, Value = search });
                    cmd.Parameters.Add(new Npgsql.NpgsqlParameter() { ParameterName = "v_vcprimarybusinessids", NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Varchar, Value = primaryBusinessIds });
                    cmd.Parameters.Add(new Npgsql.NpgsqlParameter() { ParameterName = "v_vcsubcategoryids", NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Varchar, Value = subCategoryIds });
                    cmd.Parameters.Add(new Npgsql.NpgsqlParameter() { ParameterName = "v_vcbusinessstructureids", NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Varchar, Value = businessStructureIds });
                    cmd.Parameters.Add(new Npgsql.NpgsqlParameter() { ParameterName = "v_vcheadquarterids", NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Varchar, Value = headquarterIds });
                    cmd.Parameters.Add(new Npgsql.NpgsqlParameter() { ParameterName = "v_vcpoliticalscoreids", NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Varchar, Value = politicalScoreIds });
                    cmd.Parameters.Add(new Npgsql.NpgsqlParameter() { ParameterName = "swv_refcur", Direction = ParameterDirection.InputOutput, NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Refcursor, Value = null});
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = con;

                    con.Open();
                    using (Npgsql.NpgsqlTransaction objTransaction = con.BeginTransaction())
                    {
                        cmd.ExecuteNonQuery();

                        foreach (Npgsql.NpgsqlParameter parm in cmd.Parameters)
                        {
                            if (parm.NpgsqlDbType == NpgsqlTypes.NpgsqlDbType.Refcursor)
                            {
                                if (!string.IsNullOrEmpty(Convert.ToString(parm.Value)))
                                {
                                    string parm_val = string.Format("FETCH ALL IN \"{0}\"", parm.Value.ToString());
                                    sda = new Npgsql.NpgsqlDataAdapter(parm_val.Trim().ToString(), con);
                                    ds.Tables.Add(parm.Value.ToString());
                                    sda.Fill(ds.Tables[i]);
                                    i += 1;
                                }
                            }
                        }

                        objTransaction.Commit();
                    }
                    con.Close();
                }

                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    return JsonConvert.SerializeObject(new { draw = draw, recordsTotal = Convert.ToInt32(ds.Tables[0].Rows[0]["TotalReords"]), recordsFiltered = Convert.ToInt32(ds.Tables[0].Rows[0]["TotalReords"]), data = JsonConvert.SerializeObject(ds.Tables[0], Formatting.None) }, Formatting.None);
                }
                else
                {
                    return JsonConvert.SerializeObject(new { draw = draw, recordsTotal = 0, recordsFiltered = 0, data = ""}, Formatting.None);
                }
            }
            else
            {
                throw new WebFaultException(System.Net.HttpStatusCode.Unauthorized);
            }
        }
        catch (WebFaultException exWeb)
        {
            if (exWeb.StatusCode == System.Net.HttpStatusCode.Unauthorized)
            {
                throw;
            }
            else
            {
                ExceptionModule.ExceptionPublish(exWeb, Sites.ToLong(System.Web.HttpContext.Current.Session["DomainID"]), 0, System.Web.HttpContext.Current.Request);
                throw new WebFaultException<String>("Unknown error occurred: " + exWeb.Message, System.Net.HttpStatusCode.BadRequest);
            }
        }
        catch (Exception ex)
        {
            ExceptionModule.ExceptionPublish(ex, Sites.ToLong(System.Web.HttpContext.Current.Session["DomainID"]), 0, System.Web.HttpContext.Current.Request);
            throw new WebFaultException<String>("Unknown error occurred: " + ex.Message, System.Net.HttpStatusCode.BadRequest);
        }
    }

    [OperationContract()]
    [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped, Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/GetChildKitsOfKit")]
    public string GetChildKitsOfKit(long numItemCode)
    {
        try
        {
            if (CCommon.ToLong(HttpContext.Current.Session["DomainID"]) > 0 && CCommon.ToLong(HttpContext.Current.Session["SiteId"]) > 0)
            {
                CItems objItem = new CItems();
                objItem.DomainID = CCommon.ToLong(HttpContext.Current.Session["DomainID"]);
                objItem.ItemCode = Sites.ToInteger(numItemCode);
                DataSet ds = objItem.GetChildKitsOfKit();

                return JsonConvert.SerializeObject(new { Table1 = JsonConvert.SerializeObject(ds.Tables[0], Formatting.None), Table2 = JsonConvert.SerializeObject(ds.Tables[1], Formatting.None) }, Formatting.None);
            }
            else
                throw new WebFaultException(System.Net.HttpStatusCode.Unauthorized);
        }
        catch (WebFaultException exWeb) when (exWeb.StatusCode == System.Net.HttpStatusCode.Unauthorized)
        {
            throw;
        }
        catch (Exception ex)
        {
            ExceptionModule.ExceptionPublish(ex, Sites.ToLong(HttpContext.Current.Session["DomainID"]), 0, HttpContext.Current.Request);
            throw new WebFaultException<string>("Unknown error occurred.", System.Net.HttpStatusCode.BadRequest);
        }
    }

    [OperationContract()]
    [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped, Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/GetKitChildItems")]
    public string GetKitChildItems(long numMainKitItemCode, long numChildKitItemCode)
    {
        try
        {
            if (CCommon.ToLong(HttpContext.Current.Session["DomainID"]) > 0 && CCommon.ToLong(HttpContext.Current.Session["SiteId"]) > 0)
            {
                CItems objItem = new CItems();
                objItem.DomainID = CCommon.ToLong(HttpContext.Current.Session["DomainID"]);
                objItem.ItemCode = Sites.ToInteger(numMainKitItemCode);
                objItem.ChildItemID = numChildKitItemCode;
                return JsonConvert.SerializeObject(objItem.GetKitChildItems(), Formatting.None);
            }
            else
                throw new WebFaultException(System.Net.HttpStatusCode.Unauthorized);
        }
        catch (WebFaultException exWeb) when (exWeb.StatusCode == System.Net.HttpStatusCode.Unauthorized)
        {
            throw;
        }
        catch (Exception ex)
        {
            ExceptionModule.ExceptionPublish(ex, Sites.ToLong(HttpContext.Current.Session["DomainID"]), 0, HttpContext.Current.Request);
            throw new WebFaultException<string>("Unknown error occurred.", System.Net.HttpStatusCode.BadRequest);
        }
    }

    [OperationContract()]
    [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped, Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/GetDependedKitChilds")]
    public string GetDependedKitChilds(long numMainKitItemCode, long numChildKitItemCode, string vcCurrentKitConfiguration)
    {
        try
        {
            if (CCommon.ToLong(HttpContext.Current.Session["DomainID"]) > 0 && CCommon.ToLong(HttpContext.Current.Session["SiteId"]) > 0)
            {
                CItems objItem = new CItems();
                objItem.DomainID = CCommon.ToLong(HttpContext.Current.Session["DomainID"]);
                objItem.ItemCode = Sites.ToInteger(numMainKitItemCode);
                objItem.ChildItemID = numChildKitItemCode;
                objItem.KitSelectedChild = vcCurrentKitConfiguration;
                DataSet ds = objItem.GetDependedKitChilds();

                return JsonConvert.SerializeObject(new { childKits = (ds != null && ds.Tables.Count > 0 ? ds.Tables[0] : null), childKitItems = (ds != null && ds.Tables.Count > 1 ? ds.Tables[1] : null)}, Formatting.None);
            }
            else
                throw new WebFaultException(System.Net.HttpStatusCode.Unauthorized);
        }
        catch (WebFaultException exWeb) when (exWeb.StatusCode == System.Net.HttpStatusCode.Unauthorized)
        {
            throw;
        }
        catch (Exception ex)
        {
            ExceptionModule.ExceptionPublish(ex, Sites.ToLong(HttpContext.Current.Session["DomainID"]), 0, HttpContext.Current.Request);
            throw new WebFaultException<string>("Unknown error occurred.", System.Net.HttpStatusCode.BadRequest);
        }
    }

    [OperationContract()]
    [WebInvoke(BodyStyle = WebMessageBodyStyle.Wrapped, Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "/GetItemCalculatedPrice")]
    public string GetItemCalculatedPrice(long itemCode, double qty, string vcCurrentKitConfiguration)
    {
        try
        {
            if (CCommon.ToLong(HttpContext.Current.Session["DomainID"]) > 0 && CCommon.ToLong(HttpContext.Current.Session["SiteId"]) > 0)
            {
                CItems objItem = new CItems();
                objItem.DomainID = CCommon.ToLong(HttpContext.Current.Session["DomainID"]);
                objItem.DivisionID = 0;
                objItem.Quantity = Convert.ToInt32(qty);
                objItem.SiteID = 0;
                objItem.ItemCode = Convert.ToInt32(itemCode);
                objItem.KitSelectedChild = vcCurrentKitConfiguration;
                DataTable dtCalculatedPrice = objItem.GetItemCalculatedPrice();

                return JsonConvert.SerializeObject(new { price = CCommon.ToDouble(dtCalculatedPrice.Rows[0]["monPrice"]), msrp = CCommon.ToDouble(dtCalculatedPrice.Rows[0]["monMSRP"]) }, Formatting.None);
            }
            else
                throw new WebFaultException(System.Net.HttpStatusCode.Unauthorized);
        }
        catch (WebFaultException exWeb) when (exWeb.StatusCode == System.Net.HttpStatusCode.Unauthorized)
        {
            throw;
        }
        catch (Exception ex)
        {
            ExceptionModule.ExceptionPublish(ex, Sites.ToLong(HttpContext.Current.Session["DomainID"]), 0, HttpContext.Current.Request);
            throw new WebFaultException<string>("Unknown error occurred.", System.Net.HttpStatusCode.BadRequest);
        }
    }
}

