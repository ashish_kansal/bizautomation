﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Data;
using System.ComponentModel;
using BACRM.BusinessLogic.ShioppingCart;
using BACRM.BusinessLogic.Common;
using BACRM.BusinessLogic.Contacts;
using BACRM.BusinessLogic.Admin;
using BACRM.BusinessLogic.Opportunities;
using BACRM.BusinessLogic.Item;
using BACRM.BusinessLogic.Promotion;
using Newtonsoft.Json;
using Elasticsearch.Net;
using Newtonsoft.Json.Linq;
using Nest;
using System.Configuration;

namespace BizCart.Utilities
{
    /// <summary>
    /// Summary description for OrderServices
    /// </summary>
    [WebService(Namespace = "BizCart")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class OrderServices : System.Web.Services.WebService
    {

        [WebMethod]
        public string HelloWorld()
        {
            return "Hello World";
        }

        #region  Web Methods
        [WebMethod(EnableSession = true)]
        public string GetSubCategoryByCategoryId(long CategoryId)
        {
            DataTable dt = default(DataTable); string strResult = "";
            try
            {
                CItems objItems = new CItems();
                objItems.byteMode = 15;
                objItems.CategoryID = Sites.ToInteger(CategoryId);
                objItems.SiteID = Sites.ToLong(Session["SiteID"]);
                dt = objItems.SeleDelCategory();
                strResult = CCommon.ConvertDataTabletoJSONString(dt);
            }
            catch (Exception ex)
            {
                strResult = "";
                ExceptionModule.ExceptionPublish(ex, Sites.ToLong(Session["DomainID"]), CCommon.ToInteger(Session["UserContactID"]), CCommon.ToInteger(Session["SiteId"]), HttpContext.Current.Request);
                throw ex;
            }
            return strResult;
        }
        [WebMethod(EnableSession = true)]
        public string GetStaticShippingCharge(string ZipCode, long CountryID, long StateID, long UId)
        {
            string strResult = "";

            try
            {
                string strItemCodes = "";

                BACRM.BusinessLogic.ShioppingCart.Cart objcart = new BACRM.BusinessLogic.ShioppingCart.Cart();
                objcart.DomainID = CCommon.ToInteger(Session["DomainID"]);
                objcart.ContactId = CCommon.ToInteger(Session["UserContactID"]);

                if (objcart.ContactId == 0)
                {
                    objcart.ContactId = UId;
                }

                if (HttpContext.Current.Request.Cookies["Cart"] != null)
                {
                    HttpCookie cookie = HttpContext.Current.Request.Cookies["Cart"];
                    objcart.CookieId = (cookie["KeyId"] != null) ? cookie["KeyId"] : "";
                }
                else
                {
                    objcart.CookieId = "";
                }

                double subTotal = 0;
                DataTable dt = objcart.GetCartItems();


                if (dt != null && dt.Rows.Count > 0)
                {
                    subTotal = Sites.ToDouble(dt.Compute("SUM(monTotAmtBefDiscount)", ""));

                    foreach (DataRow dr in dt.Rows)
                    {
                        strItemCodes = strItemCodes + (strItemCodes.Length > 0 ? "," + CCommon.ToString(dr["numItemCode"]) : CCommon.ToString(dr["numItemCode"]));
                    }
                }

                ShippingRule objShippingRule = new ShippingRule();
                objShippingRule.DomainID = Sites.ToLong(Session["DomainID"]);
                objShippingRule.SiteID = Sites.ToInteger(Session["SiteId"]);
                Tuple<bool, string, double, DataTable> tupleShippingPromotion = objShippingRule.GetShippingRule(ZipCode, StateID, CountryID, CCommon.ToInteger(Session["DivId"]), CCommon.ToLong(Session["WareHouseID"]), subTotal, strItemCodes);

                System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
                strResult = serializer.Serialize(new { isStaticShipping = tupleShippingPromotion.Item1, staticShippingCharge = tupleShippingPromotion.Item3 });
            }
            catch (Exception ex)
            {
                System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
                strResult = serializer.Serialize(new { isStaticShipping = false, staticShippingCharge = 0 }); ;
                throw ex;
            }
            return strResult;
        }

        [WebMethod(EnableSession = true)]
        public string GetShippingMethod(string ZipCode, long CountryID, long StateID, long UId)
        {
            string strResult = "";
            DataTable dtShippingMethod = new DataTable();
            try
            {
                BACRM.BusinessLogic.ShioppingCart.Cart objcart = new BACRM.BusinessLogic.ShioppingCart.Cart();
                objcart.DomainID = CCommon.ToInteger(Session["DomainID"]);
                objcart.ContactId = CCommon.ToInteger(Session["UserContactID"]);

                if (objcart.ContactId == 0)
                {
                    objcart.ContactId = UId;
                }

                if (HttpContext.Current.Request.Cookies["Cart"] != null)
                {
                    HttpCookie cookie = HttpContext.Current.Request.Cookies["Cart"];
                    objcart.CookieId = (cookie["KeyId"] != null) ? cookie["KeyId"] : "";
                }
                else
                {
                    objcart.CookieId = "";
                }

                double subTotal = 0;
                double TotalWeight = 0;
                DataTable dt = objcart.GetCartItems();


                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        TotalWeight = TotalWeight + Sites.ToDouble(dr["numWeight"]);
                    }
                }

                #region Get Warehouse

                CItems objItem = new CItems();
                DataTable dtTableWarehouse = null;
                objItem.WarehouseID = Sites.ToLong(HttpContext.Current.Session["DefaultWareHouseID"]);
                objItem.DomainID = Sites.ToLong(HttpContext.Current.Session["DomainID"]);
                dtTableWarehouse = objItem.GetWareHouses();

                #endregion

                //Find real time shipping methods selected
                ShippingRule objShippingRule = new ShippingRule();
                objShippingRule.DomainID = Sites.ToLong(Session["DomainID"]);
                objShippingRule.SiteID = Sites.ToInteger(Session["SiteId"]);
                dtShippingMethod = objShippingRule.GetShippingMethodForEcommerce();

                if (dtShippingMethod != null && dtShippingMethod.Rows.Count > 0)
                {
                    System.Data.DataColumn newColumn = new System.Data.DataColumn("IsShippingRuleValid", typeof(System.Boolean));
                    newColumn.DefaultValue = true;
                    dtShippingMethod.Columns.Add(newColumn);
                    //Add this column to store it as a value field in the dropdown of ddlShippingMethod
                    CCommon.AddColumnsToDataTable(ref dtShippingMethod, "vcServiceTypeID1");

                    #region foreach start
                    foreach (DataRow dr in dtShippingMethod.Rows)
                    {
                        Decimal decShipingAmount = 0;

                        #region Warehouse Checking
                        if (dtTableWarehouse.Rows.Count > 0)
                        {
                            if (Sites.ToLong(dtTableWarehouse.Rows[0]["numWCountry"]) > 0 && Sites.ToLong(dtTableWarehouse.Rows[0]["numWState"]) > 0)
                            {
                                if (Sites.ToLong(dr["numShippingCompanyID"]) == 92)
                                {
                                    dr["vcServiceName"] = Sites.ToString(dr["vcServiceName"]) + " - " + Sites.ToString(Session["CurrSymbol"]) + " " + string.Format("{0:#,##0.00}", 0);
                                    dr["vcServiceTypeID1"] = Sites.ToString(dr["numServiceTypeID"]) + "~0~0~" + Sites.ToString(dr["numShippingCompanyID"]) + "~" + Sites.ToString(dr["vcServiceName"]);
                                }
                                else if (TotalWeight >= Sites.ToDouble(dr["intFrom"]) && TotalWeight <= Sites.ToDouble(dr["intTo"]))
                                {
                                    #region Get BizDocs

                                    OppBizDocs objOppBizDocs = new OppBizDocs();
                                    objOppBizDocs.ShipCompany = Sites.ToInteger(dr["numShippingCompanyID"]);


                                    objOppBizDocs.ShipFromCountry = Sites.ToLong(dtTableWarehouse.Rows[0]["numWCountry"]);
                                    objOppBizDocs.ShipFromState = Sites.ToLong(dtTableWarehouse.Rows[0]["numWState"]);
                                    objOppBizDocs.ShipToCountry = CountryID;
                                    objOppBizDocs.ShipToState = StateID;
                                    objOppBizDocs.strShipFromCountry = "";
                                    objOppBizDocs.strShipFromState = "";
                                    objOppBizDocs.strShipToCountry = "";
                                    objOppBizDocs.strShipToState = "";
                                    objOppBizDocs.GetShippingAbbreviation();

                                    #endregion

                                    Shipping objShipping = new Shipping();
                                    objShipping.DomainID = Sites.ToLong(HttpContext.Current.Session["DomainID"]);
                                    objShipping.WeightInLbs = Sites.ToDouble(TotalWeight);
                                    objShipping.NoOfPackages = 1;
                                    objShipping.UseDimentions = Sites.ToInteger(dr["numShippingCompanyID"]) == 91 ? true : false;
                                    //for fedex dimentions are must
                                    objShipping.GroupCount = 1;

                                    objShipping.SenderState = objOppBizDocs.strShipFromState;
                                    objShipping.SenderZipCode = Sites.ToString(dtTableWarehouse.Rows[0]["vcWPinCode"]);
                                    objShipping.SenderCountryCode = objOppBizDocs.strShipFromCountry;

                                    objShipping.RecepientState = objOppBizDocs.strShipToState;
                                    objShipping.RecepientZipCode = ZipCode;
                                    objShipping.RecepientCountryCode = objOppBizDocs.strShipToCountry;

                                    objShipping.ServiceType = short.Parse(Sites.ToString(dr["intNsoftEnum"]));
                                    objShipping.PackagingType = 21;
                                    //ptYourPackaging 
                                    objShipping.ItemCode = 3;
                                    objShipping.OppBizDocItemID = 0;
                                    objShipping.ID = 9999;
                                    objShipping.Provider = Sites.ToInteger(dr["numShippingCompanyID"]);

                                    DataTable dtShipRates = new DataTable();
                                    if (string.Compare(objShipping.SenderCountryCode, objShipping.RecepientCountryCode) == 0 || string.IsNullOrEmpty(objShipping.SenderCountryCode) == true || string.IsNullOrEmpty(objShipping.RecepientCountryCode) == true)
                                    {
                                        dtShipRates = objShipping.GetRates(false);
                                    }
                                    else
                                    {
                                        dtShipRates = objShipping.GetRates(true);
                                    }

                                    if (dtShipRates.Rows.Count > 0)
                                    {
                                        if (Sites.ToDecimal(dtShipRates.Rows[0]["Rate"]) > 0)
                                        {
                                            //Currency conversion of shipping rate
                                            decShipingAmount = Sites.ToDecimal(dtShipRates.Rows[0]["Rate"]) / Sites.ToDecimal(HttpContext.Current.Session["ExchangeRate"].ToString());

                                            if (Sites.ToBool(dr["bitMarkupType"]) == true && Sites.ToDecimal(dr["fltMarkup"]) > 0)
                                            {
                                                dr["fltMarkup"] = decShipingAmount / Sites.ToDecimal(dr["fltMarkup"]);
                                            }
                                            //Percentage
                                            decShipingAmount = decShipingAmount + Sites.ToDecimal(dr["fltMarkup"]);

                                            dr["vcServiceName"] = Sites.ToString(dr["vcServiceName"]) + " - " + Sites.ToString(Session["CurrSymbol"]) + " " + string.Format("{0:#,##0.00}", decShipingAmount);
                                            dr["vcServiceTypeID1"] = Sites.ToString(dr["numServiceTypeID"]) + "~" + Sites.ToString(decShipingAmount) + "~" + Sites.ToString(dr["numRuleID"]) + "~" + Sites.ToString(dr["numShippingCompanyID"]) + "~" + Sites.ToString(dr["vcServiceName"]);
                                        }
                                    }
                                    else
                                    {
                                        dr["IsShippingRuleValid"] = false;
                                    }
                                }
                                else
                                {
                                    dr["IsShippingRuleValid"] = false;
                                }
                            }
                        }
                        #endregion
                    }
                    #endregion

                    //Select only those record whose IsShippingRuleValid(New Added Column) = true
                    DataRow[] datarows = dtShippingMethod.Select("IsShippingRuleValid=true");

                    if (datarows.Length > 0)
                    {
                        dtShippingMethod = datarows.CopyToDataTable();
                    }
                }

                strResult = CCommon.ConvertDataTabletoJSONString(dtShippingMethod);
            }
            catch (Exception ex)
            {
                strResult = "";
                ExceptionModule.ExceptionPublish(ex, Sites.ToLong(Session["DomainID"]), CCommon.ToInteger(Session["UserContactID"]), CCommon.ToInteger(Session["SiteId"]), HttpContext.Current.Request);
                throw ex;
            }
            return strResult;
        }

        [WebMethod(EnableSession = true)]
        public string GetTax(string City, string ZipCode, int CountryID, int StateID)
        {
            string strResult = "";

            try
            {
                decimal Tax = default(decimal);

                BACRM.BusinessLogic.ShioppingCart.Cart objCartItemTax = new BACRM.BusinessLogic.ShioppingCart.Cart();
                HttpCookie cookie = HttpContext.Current.Request.Cookies["Cart"];
                if (HttpContext.Current.Request.Cookies["Cart"] != null)
                {
                    objCartItemTax.CookieId = (cookie["KeyId"] != null) ? cookie["KeyId"] : "";
                }
                else
                {
                    objCartItemTax.CookieId = "";
                }

                objCartItemTax.DomainID = CCommon.ToInteger(Session["DomainID"]);
                objCartItemTax.DivisionID = 0;
                objCartItemTax.BaseTaxOn = CCommon.ToInteger(Session["BaseTaxCalcOn"]);
                objCartItemTax.CountryID = CountryID;
                objCartItemTax.StateID = StateID;
                objCartItemTax.City = City;
                objCartItemTax.ZipPostalCode = ZipCode;
                objCartItemTax.TotalTaxAmount = 0;
                objCartItemTax.ContactId = 0;

                //This method  Set TotalTaxAmount Properties ...
                objCartItemTax.GetTaxOfCartItems();

                Tax = CCommon.ToDecimal(objCartItemTax.TotalTaxAmount);

                strResult = string.Format("{0:#,##0.00}", Tax);
            }
            catch (Exception ex)
            {
                strResult = null;
                throw ex;
            }

            return strResult;
        }

        [WebMethod(EnableSession = true)]
        public string GetTaxOrderSummary(bool isWillCall)
        {
            string strResult = "";

            try
            {
                string City = "";
                string ZipCode = "";
                int CountryID = 0;
                int StateID = 0;
                decimal Tax = default(decimal);

                if (isWillCall)
                {
                    CItems objItem = new CItems();
                    objItem.WarehouseID = Sites.ToLong(HttpContext.Current.Session["DefaultWareHouseID"]);
                    objItem.DomainID = Sites.ToLong(HttpContext.Current.Session["DomainID"]);
                    DataTable dtTableWarehouse = objItem.GetWareHouses();

                    if (dtTableWarehouse != null && dtTableWarehouse.Rows.Count > 0)
                    {
                        City = Sites.ToString(dtTableWarehouse.Rows[0]["vcWCity"]);
                        ZipCode = Sites.ToString(dtTableWarehouse.Rows[0]["vcWPinCode"]);
                        CountryID = Sites.ToInteger(dtTableWarehouse.Rows[0]["numWCountry"]);
                        StateID = Sites.ToInteger(dtTableWarehouse.Rows[0]["numWState"]);
                    }
                }
                else
                {
                    CContacts objContact = new CContacts();
                    objContact.DomainID = Sites.ToLong(Session["DomainID"]);
                    if (CCommon.ToShort(Session["BaseTaxOn"]) == 1)
                    {
                        objContact.AddressID = Sites.ToLong(Session["BillAddress"]);
                    }
                    else if (CCommon.ToShort(Session["BaseTaxOn"]) == 2)
                    {
                        objContact.AddressID = Sites.ToLong(Session["ShipAddress"]);
                    }
                    else
                    {
                        objContact.AddressID = 0;
                    }

                    objContact.byteMode = 1;
                    DataTable dtTable = objContact.GetAddressDetail();

                    if (dtTable != null && dtTable.Rows.Count > 0)
                    {
                        City = Sites.ToString(dtTable.Rows[0]["vcCity"]);
                        ZipCode = Sites.ToString(dtTable.Rows[0]["vcPostalCode"]);
                        CountryID = Sites.ToInteger(dtTable.Rows[0]["numCountry"]);
                        StateID = Sites.ToInteger(dtTable.Rows[0]["numState"]);
                    }
                }

                BACRM.BusinessLogic.ShioppingCart.Cart objCartItemTax = new BACRM.BusinessLogic.ShioppingCart.Cart();
                HttpCookie cookie = HttpContext.Current.Request.Cookies["Cart"];
                if (HttpContext.Current.Request.Cookies["Cart"] != null)
                {
                    objCartItemTax.CookieId = (cookie["KeyId"] != null) ? cookie["KeyId"] : "";
                }
                else
                {
                    objCartItemTax.CookieId = "";
                }

                objCartItemTax.DomainID = CCommon.ToInteger(Session["DomainID"]);
                objCartItemTax.DivisionID = Sites.ToLong(Session["DivId"]);
                objCartItemTax.BaseTaxOn = CCommon.ToInteger(Session["BaseTaxCalcOn"]);
                objCartItemTax.CountryID = CountryID;
                objCartItemTax.StateID = StateID;
                objCartItemTax.City = City;
                objCartItemTax.ZipPostalCode = ZipCode;
                objCartItemTax.TotalTaxAmount = 0;
                objCartItemTax.ContactId = Sites.ToLong(Session["UserContactID"]);

                //This method  Set TotalTaxAmount Properties ...
                objCartItemTax.GetTaxOfCartItems();

                Tax = CCommon.ToDecimal(objCartItemTax.TotalTaxAmount);

                strResult = string.Format("{0:#,##0.00}", Tax);
            }
            catch (Exception ex)
            {
                strResult = null;
                throw ex;
            }

            return strResult;
        }

        [WebMethod(EnableSession = true)]
        public string GetAddressDetails(long AddressId)
        {
            string strResult = "";

            try
            {
                Sites.InitialiseSession();
                DataTable dtTable = new DataTable();
                CContacts objContact = new CContacts();
                objContact.DomainID = Sites.ToLong(HttpContext.Current.Session["DomainID"]);
                objContact.AddressID = AddressId;
                objContact.byteMode = 1;
                dtTable = objContact.GetAddressDetail();
                strResult = CCommon.ConvertDataTabletoJSONString(dtTable);
            }
            catch (Exception ex)
            {
                strResult = null;
                throw ex;
            }

            return strResult;
        }

        [WebMethod(EnableSession = true)]
        public string FillState(long CountryID)
        {
            string strResult = "";

            try
            {
                Sites.InitialiseSession();
                DataTable dtTable = default(DataTable);
                UserAccess objUserAccess = new UserAccess();
                objUserAccess.DomainID = Sites.ToLong(HttpContext.Current.Session["DomainID"]);
                objUserAccess.Country = CountryID;
                dtTable = objUserAccess.SelState();
                DataRow dr;
                dr = dtTable.NewRow();
                dr["vcState"] = "--Select One--";
                dr["numStateID"] = 0;
                dtTable.Rows.InsertAt(dr, 0);
                dtTable.AcceptChanges();

                strResult = CCommon.ConvertDataTabletoJSONString(dtTable);
            }
            catch (Exception ex)
            {
                strResult = null;
                throw ex;
            }

            return strResult;
        }

        [WebMethod(EnableSession = true)]
        public string GetAllEcommerceItems(string q)
        {
            string strResult = "";
            DataTable dtresult = new DataTable();
            try
            {
                Sites.InitialiseSession();
                dtresult = new DataTable();
                CItems objItems = new CItems();
                objItems.DomainID = Sites.ToLong(HttpContext.Current.Session["DomainID"]);
                objItems.SiteID = Sites.ToLong(HttpContext.Current.Session["SiteID"]);
                objItems.WarehouseID = Sites.ToLong(HttpContext.Current.Session["WareHouseID"]);
                objItems.strCondition = q;
                DataSet ds = new DataSet();
                ds = objItems.ItemDetailsForEcommAutoSuggest();
                dtresult = ds.Tables[0];
                strResult = CCommon.ConvertDataTabletoJSONString(dtresult);
            }
            catch (Exception ex)
            {
                strResult = "";
                throw ex;
            }
            return strResult;
        }

        [WebMethod(EnableSession = true)]
        public string SearchMasterListDetails(long listID, string searchText, int pageIndex, int pageSize)
        {
            string json = String.Empty;

            try
            {
                if (Sites.ToLong(Session["DomainID"]) > 0)
                {
                    System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();

                    CCommon objCommon = new CCommon();
                    objCommon.DomainID = Sites.ToLong(Session["DomainID"]);
                    objCommon.ListID = listID;
                    DataTable dt = objCommon.SearchListDetails(searchText.Trim(), pageIndex, pageSize);

                    json = serializer.Serialize(new { results = GetJson(dt), Total = objCommon.TotalRecords });
                }
                else
                {
                    throw new HttpException(401, "Authentication Failed");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return json;
        }

        private string GetJson(DataTable dt)
        {
            System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
            Dictionary<string, object> row = null;

            dt.Columns.Add("id"); dt.Columns.Add("text");

            foreach (DataRow dr in dt.Rows)
            {
                row = new Dictionary<string, object>();
                row.Add("id", CCommon.ToString(dr["numListItemID"]));
                row.Add("text", CCommon.ToString(dr["vcData"]));
                rows.Add(row);
            }

            return serializer.Serialize(rows);
        }

        [WebMethod(EnableSession = true)]
        public string LoadChildKits(string itemCode)
        {
            string json = String.Empty;

            try
            {
                QueryStringValues objEncryption = new QueryStringValues();

                CItems objItem = new CItems();
                objItem.DomainID = Sites.ToLong(HttpContext.Current.Session["DomainID"]);
                objItem.ItemCode = Sites.ToInteger(objEncryption.Decrypt(itemCode));
                DataSet ds = objItem.GetChildKitsOfKit();

                if (ds != null && ds.Tables.Count > 1)
                {
                    json = CCommon.ConvertDataTabletoJSONString(ds.Tables[1]);
                }

                return json;
            }
            catch (Exception ex)
            {
                if (Sites.ToLong(HttpContext.Current.Session["DomainID"]) == 0)
                {
                    Context.Response.StatusCode = 404;
                    return "Session Expired";
                }
                else
                {
                    throw ex;
                }

            }
        }

        [WebMethod(EnableSession = true)]
        public string LoadKitChildItems(int itemCode)
        {
            string json = String.Empty;

            try
            {
                QueryStringValues objEncryption = new QueryStringValues();

                CItems objItem = new CItems();
                objItem.DomainID = Sites.ToLong(HttpContext.Current.Session["DomainID"]);
                objItem.ItemCode = itemCode;
                DataTable dt = objItem.GetKitChildItems();

                json = CCommon.ConvertDataTabletoJSONString(dt);

                return json;
            }
            catch (Exception ex)
            {
                if (Sites.ToLong(HttpContext.Current.Session["DomainID"]) == 0)
                {
                    Context.Response.StatusCode = 404;
                    return "Session Expired";
                }
                else
                {
                    throw ex;
                }
            }
        }

        [WebMethod(EnableSession = true)]
        public string LoadDependedKitChilds(string MainKitIemCode, long ChildKitIemCode, string SelectedValue)
        {
            try
            {
                QueryStringValues objEncryption = new QueryStringValues();

                CItems objItem = new CItems();
                objItem.DomainID = Sites.ToLong(HttpContext.Current.Session["DomainID"]);
                objItem.ItemCode = Sites.ToInteger(objEncryption.Decrypt(MainKitIemCode));
                objItem.ChildItemID = ChildKitIemCode;
                objItem.KitSelectedChild = SelectedValue;
                DataSet ds = objItem.GetDependedKitChilds();

                System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
                return serializer.Serialize(new { kits = CCommon.ConvertDataTabletoJSONString(ds.Tables[0]), kitChilds = CCommon.ConvertDataTabletoJSONString(ds.Tables[1]) });
            }
            catch (Exception ex)
            {
                if (Sites.ToLong(HttpContext.Current.Session["DomainID"]) == 0)
                {
                    Context.Response.StatusCode = 404;
                    return "Session Expired";
                }
                else
                {
                    throw ex;
                }
            }
        }

        [WebMethod(EnableSession = true)]
        public string GetItemCalculatedPrice(string MainKitIemCode, int quantity, string selectedItems)
        {
            try
            {
                QueryStringValues objEncryption = new QueryStringValues();

                CItems objItem = new CItems();
                objItem.DomainID = Sites.ToLong(HttpContext.Current.Session["DomainID"]);
                objItem.DivisionID = Sites.ToLong(Session["DivId"]);
                objItem.Quantity = quantity;
                objItem.SiteID = Sites.ToLong(HttpContext.Current.Session["SiteID"]);
                objItem.ItemCode = Sites.ToInteger(objEncryption.Decrypt(MainKitIemCode));
                objItem.KitSelectedChild = selectedItems;
                DataTable dtCalculatedPrice = objItem.GetItemCalculatedPrice();

                System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
                return serializer.Serialize(new { price = CCommon.ToDouble(dtCalculatedPrice.Rows[0]["monPrice"]), msrp = CCommon.ToDouble(dtCalculatedPrice.Rows[0]["monMSRP"]) });
            }
            catch (Exception ex)
            {
                if (Sites.ToLong(HttpContext.Current.Session["DomainID"]) == 0)
                {
                    Context.Response.StatusCode = 404;
                    return "Session Expired";
                }
                else
                {
                    throw ex;
                }
            }
        }

        [WebMethod(EnableSession = true)]
        public string GetPromotionApplicableToItem(long itemCode)
        {
            long selectedPromotion = 0;
            string json = String.Empty;

            try
            {
                PromotionOffer objPromotionOffer = new PromotionOffer();
                objPromotionOffer.DomainID = Sites.ToLong(Session["DomainID"]);
                objPromotionOffer.DivisionID = Sites.ToLong(Session["DivId"]);
                objPromotionOffer.SiteID = Sites.ToLong(Session["SiteId"]);
                objPromotionOffer.numItemCode = itemCode;
                DataTable dt = objPromotionOffer.GetPromotionApplicableToItem();

                if (dt != null && dt.Rows.Count > 0)
                {
                    if (Session["PreferredItemPromotions"] != null)
                    {
                        DataSet dsPreferredItemPromotions = (DataSet)Session["PreferredItemPromotions"];

                        if (dsPreferredItemPromotions != null && dsPreferredItemPromotions.Tables.Count > 0 && dsPreferredItemPromotions.Tables[0].Rows.Count > 0)
                        {
                            if (dsPreferredItemPromotions.Tables[0].Select("numItemCode=" + itemCode.ToString()).Length > 0)
                            {
                                DataRow dr = dsPreferredItemPromotions.Tables[0].Select("numItemCode=" + itemCode.ToString()).First();
                                selectedPromotion = CCommon.ToLong(dr["numPromotionID"]);
                            }
                        }
                    }

                    System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
                    return serializer.Serialize(new { itemPromotions = CCommon.ConvertDataTabletoJSONString(dt), preferredPromotionID = selectedPromotion });
                }
                else
                {
                    json = "";
                }

                return json;
            }
            catch (Exception ex)
            {
                if (Sites.ToLong(HttpContext.Current.Session["DomainID"]) == 0)
                {
                    Context.Response.StatusCode = 404;
                    return "Session Expired";
                }
                else
                {
                    throw ex;
                }
            }
        }

        [WebMethod(EnableSession = true)]
        public string SavePreferredItemPromotion(long itemCode, long promotionID)
        {
            string json = String.Empty;

            try
            {
                DataSet dsPreferredItemPromotions = null;

                if (Session["PreferredItemPromotions"] != null)
                {
                    dsPreferredItemPromotions = (DataSet)Session["PreferredItemPromotions"];
                }
                else
                {
                    dsPreferredItemPromotions = new DataSet();
                    DataTable dt = new DataTable();
                    dt.Columns.Add("numItemCode", typeof(long));
                    dt.Columns.Add("numPromotionID", typeof(long));
                    dsPreferredItemPromotions.Tables.Add(dt);
                }

                if (dsPreferredItemPromotions.Tables[0].Select("numItemCode=" + itemCode.ToString()).Length > 0)
                {
                    DataRow dr = dsPreferredItemPromotions.Tables[0].Select("numItemCode=" + itemCode.ToString()).First();
                    dr["numPromotionID"] = promotionID;
                }
                else
                {
                    DataRow dr = dsPreferredItemPromotions.Tables[0].NewRow();
                    dr["numItemCode"] = itemCode;
                    dr["numPromotionID"] = promotionID;
                    dsPreferredItemPromotions.Tables[0].Rows.Add(dr);
                }
                dsPreferredItemPromotions.AcceptChanges();
                Session["PreferredItemPromotions"] = dsPreferredItemPromotions;

                return json;
            }
            catch (Exception ex)
            {
                if (Sites.ToLong(HttpContext.Current.Session["DomainID"]) == 0)
                {
                    Context.Response.StatusCode = 404;
                    return "Session Expired";
                }
                else
                {
                    throw ex;
                }
            }
        }


        [WebMethod(EnableSession = true)]
        public string QuickAddItemsToCart(string itemDetails)
        {
            List<string> lstProducts = new List<string>();
            BizUserControl bizUserControl = new BizUserControl();
            lstProducts = itemDetails.Split(',').ToList();
            foreach (var d in lstProducts)
            {
                long productid = 0;
                int qty = 0;
                productid = Sites.ToLong(d.Split('#')[0]);
                qty = Sites.ToInteger(d.Split('#')[1]);
                Tuple<bool, string> objResult = bizUserControl.AddToCartFromEcomm("", productid, 0, "", false, qty);
                bool isItemAdded = objResult.Item1;
            }
            return "1";
        }
        [WebMethod(EnableSession = true)]
        public string GetItemLookupElasticSearch(string searchText)
        {
            try
            {
                bool IsElasticSearchEnable = false;
                bool bitAutoSelectWarehouse = false;
                bool tintDisplayCategory = false;
                short warehouseAvailability = 0;
                bool isDisplayQtyAvailable = false;
                DataTable dtTable;
                DataSet dsItems = new DataSet();
                CItems objItems = new CItems();
                UserAccess objUserAccess = new UserAccess();
                objUserAccess.DomainID = Sites.ToLong(Session["DomainID"]);
                objUserAccess.SiteID = Sites.ToLong(Session["SiteId"]);
                objItems.SearchText = searchText;
                dtTable = objUserAccess.GetECommerceDetails();

                if (dtTable.Rows.Count > 0)
                {
                    IsElasticSearchEnable = CCommon.ToBool(dtTable.Rows[0]["bitElasticSearch"]);
                    bitAutoSelectWarehouse = CCommon.ToBool(dtTable.Rows[0]["bitAutoSelectWarehouse"]);
                    tintDisplayCategory = CCommon.ToBool(dtTable.Rows[0]["bitDisplayCategory"]);
                    warehouseAvailability = CCommon.ToShort(dtTable.Rows[0]["tintWarehouseAvailability"]);
                    isDisplayQtyAvailable = CCommon.ToBool(dtTable.Rows[0]["bitDisplayQtyAvailable"]);

                }
                if (IsElasticSearchEnable == false && Session["OverrideElasticSearch"] != null && CCommon.ToBool(Session["OverrideElasticSearch"]) == true)
                {
                    IsElasticSearchEnable = true;
                }

                objItems.DomainID = Sites.ToLong(Session["DomainID"]);
                objItems.CurrentPage = 1;
                objItems.PageSize = 100;
                objItems.TotalRecords = 0;
                objItems.SiteID = Sites.ToLong(Session["SiteID"]);

                objItems.WarehouseID = Sites.ToLong(Session["WareHouseID"]);
                objItems.SearchText = searchText;
                objItems.SortByString = " vcItemName Asc ";
                string ActualSearchString = "";
                if ((!System.Web.HttpContext.Current.User.Identity.IsAuthenticated || CCommon.ToLong(Session["DomainID"]) == 0 || CCommon.ToLong(Session["UserContactID"]) == 0) && Sites.ToInteger(Session["OnePageCheckout"]) == 0)
                {
                    //GUEST USER
                    objItems.DivisionID = 0;
                }
                else
                {
                    objItems.DivisionID = Sites.ToLong(Session["DivId"]);
                }
                if (IsElasticSearchEnable)
                {
                    DateTime start = DateTime.Now;
                    bool IsAvaiablityTokenCheck = false;
                    dsItems = GetItemForECommerceIndexFromES(objItems, tintDisplayCategory, bitAutoSelectWarehouse, IsAvaiablityTokenCheck);
                }
                else
                {
                    if (searchText.Length > 0)
                    {
                        objItems.SearchText = SearchLucene.SearchIndex(GetIndexPath(), SearchLucene.LuceneEscapeSpecialChar(searchText.Trim()).Trim(), IndexType.Items, ActuallSearchString: ref ActualSearchString);
                    }
                    dsItems = objItems.GetItemsForECommerce();
                }
                System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
                return JsonConvert.SerializeObject(dsItems.Tables[0], Formatting.None,
                        new JsonSerializerSettings
                        {
                            ReferenceLoopHandling =
                                              ReferenceLoopHandling.Ignore
                        });
            }
            catch (Exception ex)
            {
                ExceptionModule.ExceptionPublish(ex, Sites.ToLong(Session["DomainID"]), CCommon.ToInteger(Session["UserContactID"]), CCommon.ToInteger(Session["SiteId"]), HttpContext.Current.Request);
                throw;
            }
        }

        #region "Elastic Search results"
        public string GetIndexPath()
        {
            return BACRM.BusinessLogic.Common.CCommon.GetLuceneIndexPath(Sites.ToLong(HttpContext.Current.Session["DomainID"]), Sites.ToLong(HttpContext.Current.Session["SiteID"]));
            //return BACRM.BusinessLogic.Common.CCommon.GetDocumentPhysicalPath(Sites.ToLong(HttpContext.Current.Session["DomainID"])) + Sites.ToString(HttpContext.Current.Session["SiteID"]) + "\\ItemIndex";
        }
        public DataSet GetItemForECommerceIndexFromES(CItems objItems, bool tintDisplayCategory, bool bitAutoSelectWarehouse, bool IsAvaiablityTokenCheck)
        {
            DataSet ds = new DataSet();
            try
            {
                long domainID = objItems.DomainID;
                long SiteID = objItems.SiteID;
                DataTable dt = new DataTable();
                string _index = string.Concat("biz_cart_", domainID, "_item");

                Nest.ConnectionSettings settings = new Nest.ConnectionSettings(new Uri(ConfigurationManager.AppSettings["ElasticSearchURL"]));
                settings.DisableDirectStreaming(true);
                ElasticClient client = new ElasticClient(settings);

                DataTable dtCustomFields = new DataTable();

                IExistsResponse result = client.IndexExists(new IndexExistsRequest(Indices.Parse(_index + "_fields")));
                if (result != null && result.Exists)
                {
                    var resp = client.Search<object>(s => s.Index(_index + "_fields").From(0).Size(200));

                    var ResponseData = JsonConvert.SerializeObject(resp.Documents, Formatting.None);

                    //if (Session["IsDDConsoleESValues"] != null && CCommon.ToBool(Session["IsDDConsoleESValues"]) == true)
                    //{
                    //    ScriptManager.RegisterClientScriptBlock(this.Page, Page.GetType(), "GetIndexFieldResponse", "console.log('GetIndexFieldResponse','" + ResponseData + "');", true);
                    //}
                    dtCustomFields = JsonConvert.DeserializeObject<DataTable>(ResponseData);
                }

                result = client.IndexExists(new IndexExistsRequest(Indices.Parse(_index)));

                if (result != null && result.Exists)
                {
                    var GetMappingResponse = client.GetMapping<object>(s => s.Index(_index));



                    string searchText = objItems.SearchText;
                    if (!string.IsNullOrWhiteSpace(searchText))
                    {
                        searchText = "*" + searchText + "*";
                    }

                    string SortAsc = "";
                    string SortDesc = "";
                    ElasticSearchSortingField(objItems, GetMappingResponse, ref SortAsc, ref SortDesc);

                    int pageIndex = Sites.ToInteger(objItems.CurrentPage);
                    int pageSize = Sites.ToInteger(objItems.PageSize);

                    var func = new List<Func<Nest.QueryContainerDescriptor<object>, Nest.QueryContainer>>();
                    var funcMust = new List<Func<Nest.QueryContainerDescriptor<object>, Nest.QueryContainer>>();
                    var funcMustNot = new List<Func<Nest.QueryContainerDescriptor<object>, Nest.QueryContainer>>();
                    ElasticSearchFilterSet(objItems, dtCustomFields, funcMust, tintDisplayCategory, bitAutoSelectWarehouse);

                    List<string> SearchingField = new List<string>() { "Search_*" };
                    ElasticSearch_SearchIntoSpecificField(objItems, domainID, dtCustomFields, GetMappingResponse, SearchingField);



                    var resp = client.Search<object>(s => s.Index(_index).IgnoreUnavailable(true)
                            .Query(qry => qry.QueryString(qs => qs.Fields(SearchingField.ToArray()).Query(searchText).DefaultOperator(Nest.Operator.And)))
                            .PostFilter(pf => pf.Bool(b => b.Must(funcMust).MustNot(funcMustNot).Should(func)))
                            .From((pageIndex - 1) * pageSize).Take(pageSize)
                            .Sort(ss => ss.Ascending(SortAsc).Descending(SortDesc))
                            );


                    //if (Session["IsDDConsoleESValues"] != null && CCommon.ToBool(Session["IsDDConsoleESValues"]) == true)
                    //{
                    //    ScriptManager.RegisterClientScriptBlock(this.Page, Page.GetType(), "ElasticIndexResponse", "console.log('ElasticIndexResp','" + JsonConvert.SerializeObject(resp, new JsonSerializerSettings() { ContractResolver = new ElasticContractResolver(settings, new List<Func<Type, JsonConverter>>()) }) + "');", true);
                    //    ScriptManager.RegisterClientScriptBlock(this.Page, Page.GetType(), "ElasticIndexResponseDocuments", "console.log('ElasticIndexResponseDocuments','" + JsonConvert.SerializeObject(resp.Documents, Formatting.None) + "');", true);
                    //    ScriptManager.RegisterClientScriptBlock(this.Page, Page.GetType(), "ElasticIndexRequestBody", "console.log('ElasticIndexRequestBody','" + JsonConvert.SerializeObject(Encoding.UTF8.GetString(resp.ApiCall.RequestBodyInBytes), new JsonSerializerSettings() { ContractResolver = new ElasticContractResolver(settings, new List<Func<Type, JsonConverter>>()) }) + "');", true);
                    //    ScriptManager.RegisterClientScriptBlock(this.Page, Page.GetType(), "ElasticIndexResponseBody", "console.log('ElasticIndexResponseBody','" + JsonConvert.SerializeObject(Encoding.UTF8.GetString(resp.ApiCall.ResponseBodyInBytes), new JsonSerializerSettings() { ContractResolver = new ElasticContractResolver(settings, new List<Func<Type, JsonConverter>>()) }) + "');", true);
                    //}


                    objItems.TotalRecords = Sites.ToInteger(resp.Total);
                    var ResponseData = JsonConvert.SerializeObject(resp.Documents, Formatting.None);
                    dt = ConvertJsonToDatatableLinq(ResponseData);
                    //dt = JsonConvert.DeserializeObject<DataTable>(ResponseData, new JsonSerializerSettings
                    //{
                    //    MissingMemberHandling = MissingMemberHandling.Ignore
                    //});

                    if (Session["IsDDDisableESItemDynamicData"] == null || CCommon.ToBool(Session["IsDDDisableESItemDynamicData"]) == false)
                    {
                        if (dt != null && dt.Rows.Count > 0)
                        {
                            DataSet dsItemPromotion = new DataSet();

                            DataTable dtItem_UOMConversion = new DataTable("dtItem_UOMConversion");
                            dtItem_UOMConversion.Columns.Add(new DataColumn("numItemCode", typeof(int)));
                            dtItem_UOMConversion.Columns.Add(new DataColumn("UOM", typeof(float)));
                            dtItem_UOMConversion.Columns.Add(new DataColumn("UOMPurchase", typeof(float)));

                            for (int i = 0; i < dt.Rows.Count; i++)
                            {
                                dtItem_UOMConversion.Rows.Add(dt.Rows[i]["numItemCode"], dt.Rows[i]["UOMConversionFactor"], dt.Rows[i]["UOMPurchaseConversionFactor"]);
                            }

                            //var ItemCodes = dt.AsEnumerable().Select(x => x.Field<string>("numItemCode").ToString());
                            //string vcItemCodes = string.Join(",", ItemCodes);
                            dsItemPromotion = objItems.GetItemsForECommerceDynamicValues(IsAvaiablityTokenCheck, dtItem_UOMConversion);

                            var objItemList = new List<ESExtractItemField>();
                            objItemList = JsonConvert.DeserializeObject<List<ESExtractItemField>>(ResponseData);

                            if (dt.Columns.Contains("WarehouseDetails") == false)
                            {
                                dt.Columns.Add("WarehouseDetails", typeof(string));
                            }

                            for (int i = 0; i < dt.Rows.Count; i++)
                            {
                                string ItemId = dt.Rows[i]["numItemCode"].ToString();
                                if (dsItemPromotion != null && dsItemPromotion.Tables.Count > 0 && dsItemPromotion.Tables[0].Select("numItemCode = " + ItemId + "").Count() > 0)
                                {
                                    DataRow dr = dsItemPromotion.Tables[0].Select("numItemCode = " + ItemId + "").FirstOrDefault();

                                    dt.Rows[i]["numTotalPromotions"] = dr["numTotalPromotions"];
                                    dt.Rows[i]["vcPromoDesc"] = dr["vcPromoDesc"];
                                    dt.Rows[i]["bitRequireCouponCode"] = dr["bitRequireCouponCode"];
                                    dt.Rows[i]["bitInStock"] = dr["bitInStock"];
                                    dt.Rows[i]["InStock"] = dr["InStock"];
                                    dt.Rows[i]["monFirstPriceLevelPrice"] = dr["monFirstPriceLevelPrice"];
                                    dt.Rows[i]["fltFirstPriceLevelDiscount"] = dr["fltFirstPriceLevelDiscount"];
                                    dt.Rows[i]["numWareHouseItemID"] = dr["numWareHouseItemID"];
                                    //dt.Rows[i]["monWListPrice"] = dr["monWListPrice"];
                                }
                                if (objItemList != null && objItemList.Any(x => x.numItemCode == ItemId))
                                {
                                    var obj = objItemList.FirstOrDefault(x => x.numItemCode == ItemId);
                                    dt.Rows[i]["WarehouseDetails"] = obj.WarehouseDetails;
                                    if (obj.lstJsonItemCategories != null && obj.lstJsonItemCategories.Any(x => x.numCategoryID == objItems.CategoryID))
                                    {
                                        dt.Rows[i]["vcCategoryName"] = obj.lstJsonItemCategories.FirstOrDefault(x => x.numCategoryID == objItems.CategoryID).vcCategoryName;
                                        dt.Rows[i]["CategoryDesc"] = obj.lstJsonItemCategories.FirstOrDefault(x => x.numCategoryID == objItems.CategoryID).CategoryDesc;
                                    }
                                }
                            }
                        }
                    }


                    ds.Tables.Add(dt);
                }

                ds.Tables.Add(dtCustomFields);
            }
            catch (Exception ex)
            {
                //ScriptManager.RegisterClientScriptBlock(this.Page, Page.GetType(), "ElasticSearchError", "console.log('ElasticSearchError','" + GetExceptionDetails(ex) + "');", true);
                throw;
            }

            return ds;
        }

        private DataTable ConvertJsonToDatatableLinq(string jsonString)
        {
            JArray jsonArray = JArray.Parse(jsonString);

            foreach (JObject row in jsonArray.Children<JObject>())
            {
                foreach (JProperty column in row.Properties())
                {
                    // Only include JValue types
                    if (column.Value.Type == JTokenType.Array)
                    {
                        column.Value = JsonConvert.SerializeObject(column.Value);
                    }
                }
            }

            return JsonConvert.DeserializeObject<DataTable>(jsonArray.ToString());
        }

        private static void ElasticSearch_SearchIntoSpecificField(CItems objItems, long domainID, DataTable dtCustomFields, IGetMappingResponse GetMappingResponse, List<string> SearchingField)
        {
            if (!string.IsNullOrWhiteSpace(objItems.SearchText))
            {
                DataSet dsSimpleSearch = new DataSet();
                var objContact = new CContacts();
                objContact.DomainID = domainID;
                objContact.FormId = 30;
                objContact.UserCntID = 0;
                objContact.ContactType = 0;
                objContact.ViewID = 0;

                dsSimpleSearch = objContact.GetColumnConfiguration();
                if (dsSimpleSearch != null && dsSimpleSearch.Tables != null && dsSimpleSearch.Tables.Count > 1)
                {
                    for (int i = 0; i < dsSimpleSearch.Tables[1].Rows.Count; i++)
                    {
                        string DbColumnName = "";
                        if (Sites.ToBool(dsSimpleSearch.Tables[1].Rows[i]["Custom"]))
                        {
                            int CustomFieldID = Sites.ToInteger(dsSimpleSearch.Tables[1].Rows[i]["FieldId"]);

                            if (dtCustomFields != null && dtCustomFields.Select("numFormFieldId = " + CustomFieldID + "").Count() > 0)
                            {
                                DbColumnName = (from DataRow dr in dtCustomFields.Rows
                                                where Sites.ToInteger(dr["numFormFieldId"]) == CustomFieldID
                                                select string.Format("{0}_{1}", Sites.ToString(dr["vcFormFieldName"]), Sites.ToString(dr["vcFieldType"]))).FirstOrDefault();
                            }
                        }
                        else
                        {
                            DbColumnName = Sites.ToString(dsSimpleSearch.Tables[1].Rows[i]["vcDbColumnName"]);
                        }

                        if (!string.IsNullOrWhiteSpace(DbColumnName))
                        {
                            if (GetMappingResponse != null && GetMappingResponse.Mapping != null && GetMappingResponse.Mapping.Properties != null && GetMappingResponse.Mapping.Properties.Any(x => x.Key.Name == DbColumnName))
                            {
                                var objProperty = GetMappingResponse.Mapping.Properties.FirstOrDefault(x => x.Key.Name == DbColumnName);
                                if (objProperty.Value.Type.Name != "text")
                                {
                                    DbColumnName += ".keyword";
                                }
                            }
                            SearchingField.Add(DbColumnName);
                        }
                    }
                }
            }
        }

        private void ElasticSearchFilterSet(CItems objItems, DataTable dtCustomFields, List<Func<QueryContainerDescriptor<object>, QueryContainer>> funcMust, bool tintDisplayCategory, bool bitAutoSelectWarehouse)
        {
            funcMust.Add(sh => sh.QueryString(x => x.Fields("IsArchieve").Query("false")));
            //if (objItems.WarehouseID != null && objItems.WarehouseID > 0)
            //{
            //    string strWarehouseID = Sites.ToString(objItems.WarehouseID);
            //    funcMust.Add(sh =>
            //        sh.QueryString(x => x.Fields("numWareHouseID").Query(strWarehouseID))
            //        || sh.QueryString(x => x.Fields("numWareHouseID").Query("0"))
            //    );
            //}
            if (objItems.ManufacturerID != null && objItems.ManufacturerID > 0)
            {
                string strManufacturerID = Sites.ToString(objItems.ManufacturerID);
                funcMust.Add(sh =>
                    sh.QueryString(x => x.Fields("numManufacturerID").Query(strManufacturerID))
                    || sh.QueryString(x => x.Fields("numManufacturerID").Query("0"))
                );
            }

            if (tintDisplayCategory)
            {
                if (objItems.CategoryID != null && objItems.CategoryID > 0)
                {
                    CItems objCategoryItems = new CItems();
                    objCategoryItems.byteMode = 13;
                    objCategoryItems.SiteID = objItems.SiteID;
                    objCategoryItems.CategoryID = objItems.CategoryID;

                    DataTable dt = null;

                    if (Session["IsDDCacheCategory"] != null && CCommon.ToBool(Session["IsDDCacheCategory"]) == true)
                    {
                        string CacheNameForCategory = string.Format("CacheSeleDelCategory_{0}_{1}_{2}", objCategoryItems.byteMode, objCategoryItems.SiteID, objCategoryItems.CategoryID);
                        //if (Cache[CacheNameForCategory] == null)
                        //{
                        //    Cache.Insert(CacheNameForCategory, objCategoryItems.SeleDelCategory());
                        //}
                        //dt = (DataTable)Cache[CacheNameForCategory];
                    }
                    else
                    {
                        dt = objCategoryItems.SeleDelCategory();
                    }


                    DataTable filterDT = getSubCategories(dt, objCategoryItems.CategoryID);
                    if (filterDT != null && filterDT.Rows.Count > 0)
                    {
                        List<string> lstCategories = filterDT.AsEnumerable().Select(x => Sites.ToString(x.Field<decimal>("numCategoryID"))).ToList();
                        if (lstCategories != null && lstCategories.Any())
                        {
                            funcMust.Add(sh =>
                                sh.Nested(n => n.Path("lstJsonItemCategories").Query(q =>
                                    q.Bool(b => b.Must(m => m.Terms(t => t.Field("lstJsonItemCategories.numCategoryID").Terms(lstCategories.Distinct())))))
                                )
                            );
                        }
                    }
                }
                else
                {
                    //funcMust.Add(sh => sh.QueryString(x => x.Fields("lstJsonItemCategories.numCategoryID").Query(Sites.ToString(objItems.CategoryID))));
                    funcMust.Add(sh =>
                                sh.Nested(n => n.Path("lstJsonItemCategories").Query(q =>
                                    q.Range(b => b.Field("lstJsonItemCategories.numCategoryID").GreaterThan(0))
                                ))
                            );
                }
                List<Func<QueryContainerDescriptor<object>, QueryContainer>> funcNestedMust = new List<Func<QueryContainerDescriptor<object>, QueryContainer>>();

                //HAVING 1 = (CASE WHEN (I.charItemType=''P'' AND ISNULL(I.bitKitParent,0) = 0) THEN CASE WHEN (ISNULL(SUM(numOnHand), 0) > 0 OR ISNULL(SUM(numAllocation), 0) > 0) THEN 1 ELSE 0 END ELSE 1 END)
                string warehouseid = Sites.ToString(objItems.WarehouseID);
                if (bitAutoSelectWarehouse)
                {
                    funcNestedMust.Add(sh =>
                        sh.Range(m => m.Field("lstJsonWarehouseDetails.numOnHand").GreaterThan(0)) ||
                        sh.Range(m => m.Field("lstJsonWarehouseDetails.numAllocation").GreaterThan(0))
                    );
                }
                else
                {
                    funcNestedMust.Add(sh =>
                        sh.QueryString(m => m.Fields("lstJsonWarehouseDetails.numWareHouseID").Query(warehouseid)) &&
                        (
                            sh.Range(m => m.Field("lstJsonWarehouseDetails.numOnHand").GreaterThan(0)) ||
                            sh.Range(m => m.Field("lstJsonWarehouseDetails.numAllocation").GreaterThan(0))
                        )
                    );
                }

                funcMust.Add(sh =>
                    sh.QueryString(x => x.Fields("bitKitParent").Query("true")) ||
                    !sh.QueryString(x => x.Fields("charItemType").Query("P")) ||
                    (sh.QueryString(x => x.Fields("charItemType").Query("P")) && sh.QueryString(x => x.Fields("bitKitParent").Query("false"))
                        && sh.Nested(n => n.Path("lstJsonWarehouseDetails")
                            .Query(q => q.Bool(b => b.Must(funcNestedMust))
                        )
                    )
                ));

            }
            else
            {
                if (objItems.CategoryID != null && objItems.CategoryID > 0)
                {
                    CItems objCategoryItems = new CItems();
                    objCategoryItems.byteMode = 13;
                    objCategoryItems.SiteID = objItems.SiteID;
                    objCategoryItems.CategoryID = objItems.CategoryID;

                    DataTable dt = null;

                    dt = objCategoryItems.SeleDelCategory();

                    DataTable filterDT = getSubCategories(dt, objCategoryItems.CategoryID);
                    if (filterDT != null && filterDT.Rows.Count > 0)
                    {
                        List<string> lstCategories = filterDT.AsEnumerable().Select(x => Sites.ToString(x.Field<decimal>("numCategoryID"))).ToList();
                        if (lstCategories != null && lstCategories.Any())
                        {
                            funcMust.Add(sh =>
                                sh.Nested(n => n.Path("lstJsonItemCategories").Query(q =>
                                    q.Bool(b => b.Must(m => m.Terms(t => t.Field("lstJsonItemCategories.numCategoryID").Terms(lstCategories.Distinct())))))
                                )
                            );
                        }
                    }
                }
            }

            if (!string.IsNullOrWhiteSpace(objItems.FilterCustomWhere) && !string.IsNullOrWhiteSpace(objItems.FilterCustomFields))
            {
                string CustomFieldId = Sites.ToString(objItems.FilterCustomFields).Replace("[", "").Replace("]", "");
                if (dtCustomFields != null && dtCustomFields.Select("numFormFieldId = " + CustomFieldId + "").Count() > 0)
                {
                    string CustomFieldFullName = (from DataRow dr in dtCustomFields.Rows
                                                  where Sites.ToString(dr["numFormFieldId"]) == CustomFieldId
                                                  select string.Format("{0}_{1}", Sites.ToString(dr["vcFormFieldName"]), Sites.ToString(dr["vcFieldType"]))).FirstOrDefault();

                    string CustomFieldValue = Sites.ToString(objItems.FilterCustomWhere).Replace(string.Format("{0} IN (", objItems.FilterCustomFields), "").Replace(")", "");
                    string[] CustomFieldValues = CustomFieldValue.Split(',');
                    if (CustomFieldValues != null && CustomFieldValues.Count() > 0)
                    {
                        funcMust.Add(sh => sh.Terms(t => t.Field(CustomFieldFullName + ".keyword").Terms(CustomFieldValues)));
                    }
                }
            }
        }
        private static DataTable getSubCategories(DataTable dt, long CategoryID)
        {
            DataTable newdt = null;

            if (dt != null)
            {
                newdt = dt.Select("numCategoryID = " + CategoryID).CopyToDataTable();

                if (dt.Select("Category = " + CategoryID).Count() > 0)
                {
                    DataTable SubDT = dt.Select("Category = " + CategoryID).CopyToDataTable();
                    if (SubDT != null)
                    {
                        for (int i = 0; i < SubDT.Rows.Count; i++)
                        {
                            long subCategoryID = Sites.ToLong(SubDT.Rows[i]["numCategoryID"]);
                            DataTable SubDTResult = getSubCategories(dt, subCategoryID);
                            if (SubDTResult != null && SubDTResult.Rows.Count > 0)
                            {
                                newdt.Merge(SubDTResult, false, MissingSchemaAction.Add);
                            }

                        }
                    }
                }
            }

            return newdt;
        }

        private static void ElasticSearchSortingField(CItems objItems, IGetMappingResponse GetMappingResponse, ref string SortAsc, ref string SortDesc)
        {
            if (objItems != null && !string.IsNullOrWhiteSpace(objItems.SortByString))
            {
                string[] Sort = objItems.SortByString.ToString().Trim().Split(' ');
                if (Sort != null && Sort.Count() > 0)
                {
                    string SortFieldName = Sort[0];

                    if (GetMappingResponse != null && GetMappingResponse.Mapping != null && GetMappingResponse.Mapping.Properties != null && GetMappingResponse.Mapping.Properties.Any(x => x.Key.Name == SortFieldName))
                    {
                        var objProperty = GetMappingResponse.Mapping.Properties.FirstOrDefault(x => x.Key.Name == SortFieldName);
                        if (objProperty.Value != null && objProperty.Value.Type != null && objProperty.Value.Type.Name == "text")
                        {
                            SortFieldName += ".keyword";
                        }
                    }

                    if (Sort.Count() == 2)
                    {
                        string SortOrder = Sort[1];
                        if (SortOrder.ToLower() == "asc")
                        {
                            SortAsc = SortFieldName;
                        }
                        else
                        {
                            SortDesc = SortFieldName;
                        }
                    }
                }
            }
            else
            {//Default Sorting
                SortAsc = "vcItemName.keyword";
            }
        }

        #endregion
        #endregion

    }
    public class ESExtractItemField
    {
        public string numItemCode { get; set; }
        public JArray lstJsonWarehouseDetails { get; set; }
        public List<ESItemCategoryDetails> lstJsonItemCategories { get; set; }
        public string WarehouseDetails { get { return lstJsonWarehouseDetails != null ? JsonConvert.SerializeObject(lstJsonWarehouseDetails) : ""; } }
    }
}
