﻿var itemDataSource = [];
function itemLookUp(searchText) {

    $.ajax({
        type: "POST",
        url: "/js/OrderServices.asmx/GetItemLookupElasticSearch",
        data: '{searchText: "' + searchText + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            $("#itemLookUpList").css("display", "inline");
            $("#itemLookUpList").html("");
            var arrData = JSON.parse(response.d);
            var templateId = $("#tempItemLookup").html();
            itemDataSource = arrData;
            //$.each(arrData, function (index, value) {
            //    var temptemplateId = templateId;
            //    temptemplateId = temptemplateId.replace("##numItemCode##", value.numItemCode);
            //    temptemplateId = temptemplateId.replace("##vcItemName##", value.vcItemName);
            //    $("#itemLookUpList").append(temptemplateId);
            //})
            console.log(response);
        }
    })
}

//$(".searchbox ").autocomplete({
//    source: function (request, response) {
//        $.ajax({
//            type: "POST",
//            url: "/js/OrderServices.asmx/GetItemLookupElasticSearch",
//            data: '{searchText: "' + request.term + '"}',
//            contentType: "application/json; charset=utf-8",
//            dataType: "json",
//            success: function (responseD) {
//                debugger;
//                var arrData = JSON.parse(responseD.d);
//                var list = arrData.map(function (item) { return { label: item.vcItemName, value: item.vcItemName, id: item.numItemCode, categoryName: item.vcCategoryName }; });
//                response(list);
//            }
//        });
//    },
//    minLength: 2,
//    select: function (event, ui) {
//        debugger;
//        window.location.href = "/Product/" + ui.item.categoryName + "/" + ui.item.value + "/" + ui.item.id;
//        console.log("Selected: " + ui.item.value + " aka " + ui.item.id);
//    }
//});