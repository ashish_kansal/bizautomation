﻿
function GetSubCategoryByCategoryId(CategoryId) {
    $.ajax({
        type: "POST",
        url: "/js/OrderServices.asmx/GetSubCategoryByCategoryId",
        data: '{CategoryId: "' + CategoryId + '" }',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async : true,
        success: function (response) {
            try {
                var jsonData = JSON.parse(response.d);
                var menuData = "";
                var templateData = $("#templateSubCategorySubMenu").html();
                if (jsonData.length > 0) {
                    $.each(jsonData, function (index, value) {
                        var temptemplateData = templateData;
                        temptemplateData = temptemplateData.replace("##SubCategoryLink##", '/SubCategory/' + value.vcCategoryName + '/' + value.numCategoryID + '')
                        temptemplateData = temptemplateData.replace("##SubCategoryName##", value.vcCategoryName)
                        menuData = menuData + temptemplateData;
                    });
                    $("#category_list_submenu_" + jsonData[0].Category + "").append(menuData);
                }
               // $.loader.close();
            } catch (err) {
                //alert("No shipping methods are found for this shipping address. Your option is to correct address and try again.!");
               // $.loader.close();
            }
        },
        error: function () {
            //alert("No shipping methods are found for this shipping address. Your option is to correct address and try again.!");
            //$.loader.close();
        }
    });
}