﻿var autocompleteadress, autocompletebilladress, autocompleteshipaddress;



function initializeaddress(autoCompleteID, hdnStateLong, hdnStateShort, ctlPostal, ctlCountry) {

    var hdnGoogleStateLongID = hdnStateLong;
    var hdnGoogleStateShortID = hdnStateShort;
    var txtPostalID = ctlPostal;
    var ddlCountryID = ctlCountry;

    // Create the autocomplete object, restricting the search
    // to geographical location types.
    autocompleteadress = new google.maps.places.Autocomplete(
        /** @type {HTMLInputElement} */(document.getElementById(autoCompleteID)),
        { types: ['geocode'] });
    // When the user selects an address from the dropdown,
    // populate the address fields in the form.
    google.maps.event.addListener(autocompleteadress, 'place_changed', function () {
        // Get the place details from the autocomplete object.
        var place = autocompleteadress.getPlace();

        var street = "", city = "", stateLong = "", stateShort="", country = "", postalcode = "";

        // Get each component of the address from the place details
        // and fill the corresponding field on the form.

        for (var i = 0; i < place.address_components.length; i++) {
            var addressType = place.address_components[i].types[0];

            if (addressType == "administrative_area_level_1") {
                stateLong = place.address_components[i]["long_name"];
                stateShort = place.address_components[i]["short_name"];
            } else if (addressType == "country") {
                country = place.address_components[i]["long_name"];
            } else if (addressType == "postal_code") {
                postalcode = place.address_components[i]["long_name"];
            }
        }

        document.getElementById(hdnGoogleStateLongID).value = stateLong;
        document.getElementById(hdnGoogleStateShortID).value = stateShort;
        document.getElementById(txtPostalID).value = postalcode;

        var ddlCountry = document.getElementById(ddlCountryID);
        for (var i = 0; i < ddlCountry.options.length; i++) {
            if (ddlCountry.options[i].text === country) {
                ddlCountry.selectedIndex = i;
                break;
            }
        }
        __doPostBack(ddlCountryID, '');
    });
}


function initializebilladdress(autoCompleteID, ctlStreet, ctlCity, hdnState, ctlPostal, ctlCountry) {

    var txtStreetID = ctlStreet;
    var txtCityID = ctlCity;
    var hdnGoogleStateID = hdnState;
    var txtPostalID = ctlPostal;
    var ddlCountryID = ctlCountry;

    // Create the autocomplete object, restricting the search
    // to geographical location types.
    autocompletebilladress = new google.maps.places.Autocomplete(
        /** @type {HTMLInputElement} */(document.getElementById(autoCompleteID)),
        { types: ['geocode'] });
    // When the user selects an address from the dropdown,
    // populate the address fields in the form.
    google.maps.event.addListener(autocompletebilladress, 'place_changed', function () {
        // Get the place details from the autocomplete object.
        var place = autocompletebilladress.getPlace();

        var street = "", city = "", state = "", country = "", postalcode = "";

        // Get each component of the address from the place details
        // and fill the corresponding field on the form.

        for (var i = 0; i < place.address_components.length; i++) {
            var addressType = place.address_components[i].types[0];

            if (addressType == "street_number") {
                street = place.address_components[i]["long_name"];
            } else if (addressType == "route") {
                street = street == "" ? place.address_components[i]["long_name"] : street + " " + place.address_components[i]["long_name"];
            } else if (addressType == "locality") {
                city = place.address_components[i]["long_name"];
            } else if (addressType == "administrative_area_level_1") {
                state = place.address_components[i]["long_name"];
            } else if (addressType == "country") {
                country = place.address_components[i]["long_name"];
            } else if (addressType == "postal_code") {
                postalcode = place.address_components[i]["long_name"];
            }
        }

        document.getElementById(txtStreetID).value = street;
        document.getElementById(txtCityID).value = city;
        document.getElementById(hdnGoogleStateID).value = state;
        document.getElementById(txtPostalID).value = postalcode;

        var ddlCountry = document.getElementById(ddlCountryID);
        for (var i = 0; i < ddlCountry.options.length; i++) {
            if (ddlCountry.options[i].text === country) {
                ddlCountry.selectedIndex = i;
                break;
            }
        }
        __doPostBack(ddlCountryID, '');
    });
}


function initializeshipaddress(autoCompleteID, ctlStreet, ctlCity, hdnState, ctlPostal, ctlCountry) {

    var txtStreetID = ctlStreet;
    var txtCityID = ctlCity;
    var hdnGoogleStateID = hdnState;
    var txtPostalID = ctlPostal;
    var ddlCountryID = ctlCountry;

    // Create the autocomplete object, restricting the search
    // to geographical location types.
    autocompleteshipaddress = new google.maps.places.Autocomplete(
        /** @type {HTMLInputElement} */(document.getElementById(autoCompleteID)),
        { types: ['geocode'] });
    // When the user selects an address from the dropdown,
    // populate the address fields in the form.
    google.maps.event.addListener(autocompleteshipaddress, 'place_changed', function () {
        // Get the place details from the autocomplete object.
        var place = autocompleteshipaddress.getPlace();

        //Clear hiddenfield value
        document.getElementById(hdnGoogleStateID).value = "";


        var street = "", city = "", state = "", country = "", postalcode = "";

        // Get each component of the address from the place details
        // and fill the corresponding field on the form.
        if (place.address_components.length > 0) {
            for (var i = 0; i < place.address_components.length; i++) {
                var addressType = place.address_components[i].types[0];

                if (addressType == "street_number") {
                    street = place.address_components[i]["long_name"];
                } else if (addressType == "route") {
                    street = street == "" ? place.address_components[i]["long_name"] : street + " " + place.address_components[i]["long_name"];
                } else if (addressType == "locality") {
                    city = place.address_components[i]["long_name"];
                } else if (addressType == "administrative_area_level_1") {
                    state = place.address_components[i]["long_name"];
                } else if (addressType == "country") {
                    country = place.address_components[i]["long_name"];
                } else if (addressType == "postal_code") {
                    postalcode = place.address_components[i]["long_name"];
                }
            }

            document.getElementById(txtStreetID).value = street;
            document.getElementById(txtCityID).value = city;
            document.getElementById(hdnGoogleStateID).value = state;
            document.getElementById(txtPostalID).value = postalcode;

            var ddlCountry = document.getElementById(ddlCountryID);
            for (var i = 0; i < ddlCountry.options.length; i++) {
                if (ddlCountry.options[i].text === country) {
                    ddlCountry.selectedIndex = i;
                    break;
                }
            }

            __doPostBack(ddlCountryID, '');
        }
    });
}




