﻿var autocompleteadress, autocompletebilladress, autocompleteshipaddress;

$(document).ready(function () {
    initializebilladdress("txtBillStreet", "txtBillStreet", "txtBillCity", "txtBillCode", "ddlBillCountry");
    initializeshipaddress("txtShipStreet", "txtShipStreet", "txtShipCity", "txtShipCode", "ddlShipCountry");
});

function initializebilladdress(autoCompleteID, ctlStreet, ctlCity, ctlPostal, ctlCountry) {

    var txtStreetID = ctlStreet;
    var txtCityID = ctlCity;
    var txtPostalID = ctlPostal;
    var ddlCountryID = ctlCountry;

    // Create the autocomplete object, restricting the search
    // to geographical location types.
    autocompletebilladress = new google.maps.places.Autocomplete(
        /** @type {HTMLInputElement} */(document.getElementById(autoCompleteID)),
        { types: ['geocode'] });
    // When the user selects an address from the dropdown,
    // populate the address fields in the form.
    google.maps.event.addListener(autocompletebilladress, 'place_changed', function () {
        // Get the place details from the autocomplete object.
        var place = autocompletebilladress.getPlace();

        var street = "", city = "", state = "", country = "", postalcode = "";

        // Get each component of the address from the place details
        // and fill the corresponding field on the form.

        for (var i = 0; i < place.address_components.length; i++) {
            var addressType = place.address_components[i].types[0];

            if (addressType == "street_number") {
                street = place.address_components[i]["long_name"];
            } else if (addressType == "route") {
                street = street == "" ? place.address_components[i]["long_name"] : street + " " + place.address_components[i]["long_name"];
            } else if (addressType == "locality") {
                city = place.address_components[i]["long_name"];
            } else if (addressType == "administrative_area_level_1") {
                state = place.address_components[i]["long_name"];
            } else if (addressType == "country") {
                country = place.address_components[i]["long_name"];
            } else if (addressType == "postal_code") {
                postalcode = place.address_components[i]["long_name"];
            }
        }

        document.getElementById(txtStreetID).value = street;
        document.getElementById(txtCityID).value = city;
        document.getElementById(txtPostalID).value = postalcode;

        var ddlCountry = document.getElementById(ddlCountryID);
        for (var i = 0; i < ddlCountry.options.length; i++) {
            if (ddlCountry.options[i].text === country) {
                ddlCountry.selectedIndex = i;
                break;
            }
        }

        $.when(FillState(ddlCountry.value, $("#ddlBillState"))).done(function (a1) {
            var matchingValue = $('#ddlBillState option').filter(function () {
                return this.text.toLowerCase() == state.toLowerCase();
            }).attr('value');

            if (matchingValue != null) {
                $("#ddBillState").val(matchingValue);
                $('#hdnBillState').val(matchingValue);
            }

            if ($("#hdnTaxBasedOn").val() == "1" && parseInt($('#ddlBillCountry').val()) > 0 && parseInt($('#ddlBillState').val()) > 0) {
                GetTax($('#txtBillCity').val(), $('#txtBillCode').val(), parseInt($('#ddlBillCountry').val()), parseInt($('#ddlBillState').val()));
            }
        });
    });
}


function initializeshipaddress(autoCompleteID, ctlStreet, ctlCity, ctlPostal, ctlCountry) {

    var txtStreetID = ctlStreet;
    var txtCityID = ctlCity;
    var txtPostalID = ctlPostal;
    var ddlCountryID = ctlCountry;

    // Create the autocomplete object, restricting the search
    // to geographical location types.
    autocompleteshipaddress = new google.maps.places.Autocomplete(
        /** @type {HTMLInputElement} */(document.getElementById(autoCompleteID)),
        { types: ['geocode'] });
    // When the user selects an address from the dropdown,
    // populate the address fields in the form.
    google.maps.event.addListener(autocompleteshipaddress, 'place_changed', function () {
        // Get the place details from the autocomplete object.
        var place = autocompleteshipaddress.getPlace();

        var street = "", city = "", state = "", country = "", postalcode = "";

        // Get each component of the address from the place details
        // and fill the corresponding field on the form.
        if (place.address_components.length > 0) {
            for (var i = 0; i < place.address_components.length; i++) {
                var addressType = place.address_components[i].types[0];

                if (addressType == "street_number") {
                    street = place.address_components[i]["long_name"];
                } else if (addressType == "route") {
                    street = street == "" ? place.address_components[i]["long_name"] : street + " " + place.address_components[i]["long_name"];
                } else if (addressType == "locality") {
                    city = place.address_components[i]["long_name"];
                } else if (addressType == "administrative_area_level_1") {
                    state = place.address_components[i]["long_name"];
                } else if (addressType == "country") {
                    country = place.address_components[i]["long_name"];
                } else if (addressType == "postal_code") {
                    postalcode = place.address_components[i]["long_name"];
                }
            }

            document.getElementById(txtStreetID).value = street;
            document.getElementById(txtCityID).value = city;
            document.getElementById(txtPostalID).value = postalcode;

            var ddlCountry = document.getElementById(ddlCountryID);
            for (var i = 0; i < ddlCountry.options.length; i++) {
                if (ddlCountry.options[i].text === country) {
                    ddlCountry.selectedIndex = i;
                    break;
                }
            }

            $.when(FillState(ddlCountry.value, $("#ddlShipState"))).done(function (a1) {
                var matchingValue = $('#ddlShipState option').filter(function () {
                    return this.text.toLowerCase() == state.toLowerCase();
                }).attr('value');

                if (matchingValue != null) {
                    $("#ddlShipState").val(matchingValue);
                    $('#hdnShipState').val(matchingValue);
                }

                if ($("#hdnTaxBasedOn").val() == "2" && parseInt($('#ddlShipCountry').val()) > 0 && parseInt($('#ddlShipState').val()) > 0) {
                    GetTax($('#txtShipCity').val(), $('#txtShipCode').val(), parseInt($('#ddlShipCountry').val()), parseInt($('#ddlShipState').val()));
                }

                if ($('#txtShipCode').val() != '' && parseInt($('#ddlShipCountry').val()) > 0 && parseInt($('#ddlShipState').val()) > 0) {
                    LoadOverlay('body', 'Please wait a few seconds while we reach out to our carriers to get you the best shipping rates possible');
                    GetShippingMethod($('#txtShipCode').val(), parseInt($('#ddlShipCountry').val()), parseInt($('#ddlShipState').val()));
                }
            });


        }
    });
}




