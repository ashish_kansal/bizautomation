////////////////////////////////////////////////////////////////
///////////////////////  COUNTRIES AND STATES //////////////////
////////////////////////////////////////////////////////////////
/*
Purpose:	Display the States belonging to the selected Country
Created By: Debasish Tapan Nag
Parameter:	1) oCountryDdl: The drop down object of Country
Return		1) None
*/
function filterStates4Country(oCountryDdl)
{
	var sCountryDdlName = oCountryDdl.name;
	var sCorrespondingStateDdlName;
	if (sCountryDdlName=='vcBillCountry')
	{
	    sCorrespondingStateDdlName='vcBilState'
	}
	else 
	{
	    sCorrespondingStateDdlName = sCountryDdlName.replace('Country','State');
	}
	//alert(sCorrespondingStateDdlName)
	var oStateDdl = document.getElementById(sCorrespondingStateDdlName);
	if(oStateDdl)
	{
		var numCountryId;
		numCountryId = oCountryDdl.value;
		var sStateNamesList;
		var sStateIdsList;
		try{		
			sStateNamesList = eval('arrStatesNamesInCountry_'+numCountryId);
			sStateIdsList = eval('arrStateIdsInCountry_'+numCountryId);
			if(sStateNamesList.length > 0)
			{
				oStateDdl.length = 1;
				for(var iStateIndex = 0;iStateIndex < sStateNamesList.length;iStateIndex++)
				{
					var oStateOption = new Option();
					oStateOption.value = sStateIdsList[iStateIndex];
					oStateOption.text = sStateNamesList[iStateIndex];
					oStateDdl.options[oStateDdl.options.length] = oStateOption;
				}
			}else{
				oStateDdl.length = 1;
			}
		}catch(ex){
			oStateDdl.length = 1;
		}
	}
}