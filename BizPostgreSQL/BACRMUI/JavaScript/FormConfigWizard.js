/*
Purpose:	Redirects to the Admin Main Screen
Created By: Debasish Tapan Nag
Parameter:	1) None
Return		1) None
*/

function Back() {
    document.location.href = 'frmAdminSection.aspx';
}
/*
Purpose:	Redirects to the Admin Main Screen
Created By: Debasish Tapan Nag
Parameter:	1) None
Return		1) None
*/

function openSimpleSearchConfig() {
    window.open('frmSimpleSearchConfiguration.aspx', '', 'toolbar=no,titlebar=no,left=200, top=200,width=500,height=300,scrollbars=no,resizable=yes');
    return false;
}
/*
Purpose:	Opens the Custom Field Selector Window
Created By: Debasish Tapan Nag
Parameter:	1) None
Return		1) None
*/

function openCustomFieldSelector(numFormId) {
    var numAuthenticationGroupId;
    if (document.getElementById('objAuthenticationGroupControl')) {
        var objDdlGroup = document.getElementById('objAuthenticationGroupControl');
        numAuthenticationGroupId = objDdlGroup.options[objDdlGroup.selectedIndex].value;
    }
    else {
        var objHdAuthGroup = document.getElementById('hdAuthenticationGroupId');
        numAuthenticationGroupId = objHdAuthGroup.value;
    }
    window.open('frmBizFormCustomFieldAreasSelector.aspx?numFormId=' + numFormId + '&numAuthenticationGroupId=' + numAuthenticationGroupId, '', 'toolbar=no,titlebar=no,left=200, top=200,width=400,height=180,scrollbars=no,resizable=yes');
}
/*
Purpose:	Closes the window
Created By: Debasish Tapan Nag
Parameter:	1) None
Return		1) None
*/
function CloseWin() {
    this.close();
}

/*
Purpose:	Class for storing the form config parameters for Custom Areas
Created By: Debasish Tapan Nag
Parameter:	1) numLocId: The Location Id for which this config settings are
3) vcFormFieldName: The form field text
Return		1) Nothing
*/
function classCustomAreas(numLocId, vcFormFieldName) {
    this.numLocId = numLocId;
    this.vcFormFieldName = vcFormFieldName;
}
/*
Purpose:	Provides client side script to the custom validatior for the checkbox "Billing Address same as Shipment Address"
Created By: Debasish Tapan Nag
Parameter:	1) source: Source of the event
2) args: Arguments for the event
Return		1) None
*/
function validateBusinessAndShipAddress(source, args) {
    var boolCBSelected;
    boolCBSelected = true;
    ddlAvailableFields = document.getElementById('lstAvailablefld');
    ddlFormList = document.getElementById('ddlFormList')
    if (ddlFormList.value == 3) {
        for (var jIndex = 0; jIndex <= ddlAvailableFields.options.length - 1; jIndex++) {
            if (ddlAvailableFields.options[jIndex].value == '144R') {
                boolCBSelected = false;
                break;
            }
        }
        if (boolCBSelected) {
            var arrRelatedFields = new Array('135R', '139R', '137R', '142R', '143R', '149R', '150R', '151R', '152R', '153R');
            for (var iIndex = 0; iIndex <= ddlAvailableFields.options.length - 1; iIndex++) {
                for (var jIndex = 0; jIndex <= arrRelatedFields.length - 1; jIndex++) {
                    if (arrRelatedFields[jIndex] == ddlAvailableFields.options[iIndex].value) {
                        args.IsValid = false;
                        return;
                    }
                }
            }
        }
        args.IsValid = true;

    }
    else if (ddlFormList.value == 4) {
        for (var jIndex = 0; jIndex <= ddlAvailableFields.options.length - 1; jIndex++) {
            if (ddlAvailableFields.options[jIndex].value == '195R') {
                boolCBSelected = false;
                break;
            }
        }
        if (boolCBSelected) {
            var arrRelatedFields = new Array('186R', '188R', '190R', '193R', '194R', '200R', '201R', '202R', '203R', '204R');
            for (var iIndex = 0; iIndex <= ddlAvailableFields.options.length - 1; iIndex++) {
                for (var jIndex = 0; jIndex <= arrRelatedFields.length - 1; jIndex++) {
                    if (arrRelatedFields[jIndex] == ddlAvailableFields.options[iIndex].value) {
                        args.IsValid = false;
                        return;
                    }
                }
            }
        }
        args.IsValid = true;

    }
    else if (ddlFormList.value == 5) {
        for (var jIndex = 0; jIndex <= ddlAvailableFields.options.length - 1; jIndex++) {
            if (ddlAvailableFields.options[jIndex].value == '246R') {
                boolCBSelected = false;
                break;
            }
        }
        if (boolCBSelected) {
            var arrRelatedFields = new Array('237R', '239R', '241R', '244R', '245R', '251R', '252R', '253R', '254R', '255R');
            for (var iIndex = 0; iIndex <= ddlAvailableFields.options.length - 1; iIndex++) {
                for (var jIndex = 0; jIndex <= arrRelatedFields.length - 1; jIndex++) {
                    if (arrRelatedFields[jIndex] == ddlAvailableFields.options[iIndex].value) {
                        args.IsValid = false;
                        return;
                    }
                }
            }
        }
        args.IsValid = true;

    }
}
/*
Purpose:	Validates the selection of Custom Field Areas
Created By: Debasish Tapan Nag
Parameter:	1) None
Return		1) None
*/
function validateFieldAreasData() {
    var sCustomAreasXML = '<FormCustomAreas>' + '\n';
    for (var iIndex = 0; iIndex < arrCustomAreas.length; iIndex++) {
        if (document.getElementById('cbCustomFieldAreas_' + iIndex).checked) {
            sCustomAreasXML += '<FormCustomArea>' + '\n';
            sCustomAreasXML += '<Loc_Id>' + arrCustomAreas[iIndex].numLocId + '</Loc_Id>' + '\n';
            sCustomAreasXML += '<Loc_Name>' + encodeMyHtml(arrCustomAreas[iIndex].vcFormFieldName) + '</Loc_Name>' + '\n';
            sCustomAreasXML += '</FormCustomArea>' + '\n';
        }
    }
    sCustomAreasXML += '</FormCustomAreas>' + '\n';
    document.getElementById('hdCustomFieldXMLString').value = sCustomAreasXML;
}
/*
Purpose:	encodes the string
Created By: Debasish Tapan Nag
Parameter:	1) fBoxString: The source string
Return		1) The htmlencoded string
*/
function encodeMyHtml(fBoxString) {
    encodedHtml = unescape(fBoxString);
    encodedHtml = encodedHtml.replace(/&/g, "\&amp;");
    encodedHtml = encodedHtml.replace(/\>/g, "&gt;");
    encodedHtml = encodedHtml.replace(/\</g, "&lt; ");
    encodedHtml = encodedHtml.replace(/'/g, "\&apos;");
    encodedHtml = encodedHtml.replace(/"/g, "\&quot;");
    encodedHtml = encodedHtml.replace('(*)', '');
    return encodedHtml;
}
/*
Purpose:	Pulls the text from the drop dwon into the edit box
Created By: Debasish Tapan Nag
Parameter:	1) fbox: The source list box
2) tbox: the Target edit box
Return		1) Nothing
*/
RequiredField = 0; //1: Required; 0: Not-Required

function editLable(fbox, tbox) {
    for (var i = 0; i < fbox.options.length; i++) {
        if (fbox.options[i].selected && fbox.options[i].value != "") {
            SelectedText = fbox.options[i].text;
            SelectedValue = fbox.options[i].value;
            if (SelectedText.substring(0, 4) == '(*) ') {
                SelectedFilteredText = SelectedText.substring(4, SelectedText.length);
                RequiredField = 1;
            } else {
                SelectedFilteredText = SelectedText;
                RequiredField = 0;
            }
            if (tbox != null) {
                tbox.value = SelectedFilteredText;
            }
        }
    }
}

/*
Purpose:	puts the text from the edit box into the drop down
Created By: Debasish Tapan Nag
Parameter:	1) fbox: The Target edit box
2) tbox: The source list box
Return		1) Nothing
*/
function SaveEditedField(fbox, tbox) {
    SelectedText = fbox.value
    if (RequiredField == 1) {
        SelectedText = SelectedText.replace('(*)', '')
        SelectedFilteredText = '(*) ' + SelectedText;
    } else {
        SelectedFilteredText = SelectedText;
    }
    for (var i = 0; i < tbox.options.length; i++) {
        if (tbox.options[i].selected && tbox.options[i].value != "") {
            tbox.options[i].text = SelectedFilteredText;
            SelectedValue = tbox.options[i].value;
        }
    }
    for (var iIndex = 0; iIndex < arrFieldConfig.length; iIndex++) {
        if (arrFieldConfig[iIndex].numFormFieldId == SelectedValue) {
            arrFieldConfig[iIndex].vcNewFormFieldName = SelectedText;
            break;
        }
    }
}
/*
Purpose:	Class for storing the form config parameters
Created By: Debasish Tapan Nag
Parameter:	1) numFormId: The Form Id for which this config settings are
2) numFormFieldId: The form field id for the form
3) vcFormFieldName: The form field text
4) vcDbColumnName: The table columns where the data entered in these fields will be entered
5) vcAssociatedControlType: The html controls which will be associated with these fields
6) intColumnNo: The column no in which the this field will appear
7) intRowNum: The row number in which this control will appear
8) boolRequired: The flag which indicates if the particular field is mandatory or not
9) numListID: The link for the list items,in case the control is a listbox/ combo
10) boolAOIField: Indicates if the field is related to AOI or not
11) bitDefaultMandatory: Represent if the field should always be mandatory or not
Return		1) Nothing
*/
function classFormFieldConfig(numFormId, numFormFieldId, vcFormFieldName, vcNewFormFieldName, vcDbColumnName, vcListItemType, vcAssociatedControlType, vcFieldType, vcFieldDataType, intColumnNum, intRowNum, boolRequired, numListID, boolAOIField, bitDefaultMandatory, vcLookBackTableName) {
    this.numFormId = numFormId;
    this.numFormFieldId = numFormFieldId;
    this.vcFormFieldName = vcFormFieldName;
    this.vcNewFormFieldName = vcNewFormFieldName;
    this.vcDbColumnName = vcDbColumnName;
    this.vcListItemType = vcListItemType;
    this.vcAssociatedControlType = vcAssociatedControlType;
    this.vcFieldType = vcFieldType;
    this.vcFieldDataType = vcFieldDataType;
    this.intColumnNum = intColumnNum;
    this.intRowNum = intRowNum;
    this.boolRequired = boolRequired;
    this.numListID = numListID;
    this.boolAOIField = boolAOIField;
    this.bitDefaultMandatory = bitDefaultMandatory;
    this.vcLookBackTableName = vcLookBackTableName;
}
/*
Purpose:	Class for storing the form's mandatory config parameters
Created By: Debasish Tapan Nag
Parameter:	1) numFormFieldId: The form field id for the form
2) vcFormFieldName: The form field text
Return		1) Nothing
*/
function classMandatoryFormFieldConfig(numFormFieldId, vcFormFieldName) {
    this.numFormFieldId = numFormFieldId;
    this.vcFormFieldName = vcFormFieldName;
}
/*
Purpose:	To swap the array elements
Created By: Debasish Tapan Nag
Parameter:	1) iFieldValue:The from list box value
2) iColNo: The column of the new element
2) iRowNo: The row of the new element
			
Return		1) False
*/
function swapColumnPosition(iFieldValue, iColNo, iRowNo, aoiFlag) {
    for (var iIndex = 0; iIndex < arrFieldConfig.length; iIndex++) {
        if (arrFieldConfig[iIndex].numFormFieldId == iFieldValue && arrFieldConfig[iIndex].boolAOIField == aoiFlag) {
            arrFieldConfig[iIndex].intColumnNum = iColNo;
            arrFieldConfig[iIndex].intRowNum = iRowNo;
            break;
        }
    }
}
/*
Purpose:	To moveup/down the array elements
Created By: Debasish Tapan Nag
Parameter:	1) iFieldValue: The field value
2) iFirstRow: The first index in the list box (max)
3) iSecondFieldValue: the secone field value
4) iSecondRow: The 2nd index in the list box	(max - 1)
Return		1) False
*/
function swapRowPosition(iFieldValue, iFirstRow, iSecondFieldValue, iSecondRow, aoiFlag) {
    for (var iIndex = 0; iIndex < arrFieldConfig.length; iIndex++) {
        if (arrFieldConfig[iIndex].numFormFieldId == iFieldValue && arrFieldConfig[iIndex].boolAOIField == aoiFlag) {
            arrFieldConfig[iIndex].intRowNum = iSecondRow;
        }
        if (arrFieldConfig[iIndex].numFormFieldId == iSecondFieldValue && arrFieldConfig[iIndex].boolAOIField == aoiFlag) {
            arrFieldConfig[iIndex].intRowNum = iFirstRow;
        }
    }
}
/*
Purpose:	Reassigns the row values
Created By: Debasish Tapan Nag
Parameter:	1) iFieldValue: The field value
2) aoiFlag: AOI flag
Return		1) False
*/
function reAssignRowPosition(iIndex, SelectedDdlName, numFormFieldId) {
    for (var jIndex = 0; jIndex <= document.getElementById(SelectedDdlName).options.length - 1; jIndex++) {
        if (document.getElementById(SelectedDdlName).options[jIndex].value == numFormFieldId) {
            arrFieldConfig[iIndex].intRowNum = jIndex;
            break;
        }
    }
}

/*
Purpose:	Reconfirm the values
Created By: Debasish Tapan Nag
Parameter:	1) iFieldValue: The field value
2) aoiFlag: AOI flag
Return		1) False
*/
function doubleCheckRowPosition() {
    for (var iIndex = 0; iIndex < arrFieldConfig.length; iIndex++) {
        if (arrFieldConfig[iIndex].intColumnNum > 0) {
            if (arrFieldConfig[iIndex].boolAOIField == "0") {
                switch (arrFieldConfig[iIndex].intColumnNum) {
                    case 1:
                        try {
                            if (document.getElementById('lstSelectedfldOne').options[arrFieldConfig[iIndex].intRowNum].value != arrFieldConfig[iIndex].numFormFieldId)
                                reAssignRowPosition(iIndex, 'lstSelectedfldOne', arrFieldConfig[iIndex].numFormFieldId)
                        } catch (e) {
                            reAssignRowPosition(iIndex, 'lstSelectedfldOne', arrFieldConfig[iIndex].numFormFieldId)
                        }
                    case 2:
                        if (document.getElementById('lstSelectedfldTwo')) {
                            try {
                                if (document.getElementById('lstSelectedfldTwo').options[arrFieldConfig[iIndex].intRowNum].value != arrFieldConfig[iIndex].numFormFieldId)
                                    reAssignRowPosition(iIndex, 'lstSelectedfldTwo', arrFieldConfig[iIndex].numFormFieldId)
                            } catch (e) {
                                reAssignRowPosition(iIndex, 'lstSelectedfldTwo', arrFieldConfig[iIndex].numFormFieldId)
                            }
                        }
                    case 3:
                        if (document.getElementById('lstSelectedfldThree')) {
                            try {
                                if (document.getElementById('lstSelectedfldThree').options[arrFieldConfig[iIndex].intRowNum].value != arrFieldConfig[iIndex].numFormFieldId)
                                    reAssignRowPosition(iIndex, 'lstSelectedfldThree', arrFieldConfig[iIndex].numFormFieldId)
                            } catch (e) {
                                reAssignRowPosition(iIndex, 'lstSelectedfldThree', arrFieldConfig[iIndex].numFormFieldId)
                            }
                        }
                }
            } else {
                if (arrFieldConfig[iIndex].intColumnNum == 1) {
                    try {
                        if (document.getElementById('lstSelectedfldAOI').options[arrFieldConfig[iIndex].intRowNum].value != arrFieldConfig[iIndex].numFormFieldId)
                            reAssignRowPosition(iIndex, 'lstSelectedfldAOI', arrFieldConfig[iIndex].numFormFieldId)
                    } catch (e) {
                        reAssignRowPosition(iIndex, 'lstSelectedfldAOI', arrFieldConfig[iIndex].numFormFieldId)
                    }
                }
            }
        }
    }
}
/*
Purpose:	The processing required before the form is saved
Created By: Debasish Tapan Nag
Parameter:	1) None
Return		1) None
*/
var sMissingMandatoryFields;
function PreSaveProcess(frmForm) {
    if (frmForm.lstSelectedfldAOI != null) {
        var str = '';

        for (var i = 0; i < frmForm.lstSelectedfldAOI.options.length; i++) {
            var SelectedValue;
            SelectedValue = frmForm.lstSelectedfldAOI.options[i].value;
            str = str + SelectedValue + ','
        }
        frmForm.hdnAOI.value = str;
    }

    doubleCheckRowPosition();
    sMissingMandatoryFields = '';
    var sXMLString = '<FormFields>' + '\n';
    //Create xml for available fields
    for (iIndex = 0; iIndex < arrFieldConfig.length; iIndex++) {
        if (arrFieldConfig[iIndex].intColumnNum > 0) {
            if (arrFieldConfig[iIndex].bitDefaultMandatory == "True" && arrFieldConfig[iIndex].boolRequired == 0) {
                sMissingMandatoryFields = sMissingMandatoryFields + arrFieldConfig[iIndex].vcFormFieldName + ', ';
            }
            sXMLString += '<FormField>' + '\n';
            sXMLString += '<numFormId>' + arrFieldConfig[iIndex].numFormId + '</numFormId>' + '\n';
            sXMLString += '<numFormFieldId>' + arrFieldConfig[iIndex].numFormFieldId + '</numFormFieldId>' + '\n';
            sXMLString += '<vcFormFieldName>' + encodeMyHtml(arrFieldConfig[iIndex].vcFormFieldName) + '</vcFormFieldName>' + '\n';

            sXMLString += '<vcNewFormFieldName>' + encodeMyHtml(arrFieldConfig[iIndex].vcNewFormFieldName) + '</vcNewFormFieldName>' + '\n';
            if (arrFieldConfig[iIndex].boolAOIField == "1") {
                sXMLString += '<vcDbColumnName>' + encodeMyHtml(arrFieldConfig[iIndex].vcDbColumnName) + parseInt(arrFieldConfig[iIndex].intRowNum + 1) + '</vcDbColumnName>' + '\n';
            } else {
                sXMLString += '<vcDbColumnName>' + encodeMyHtml(arrFieldConfig[iIndex].vcDbColumnName) + '</vcDbColumnName>' + '\n';
            }
            sXMLString += '<vcListItemType>' + arrFieldConfig[iIndex].vcListItemType + '</vcListItemType>' + '\n';
            sXMLString += '<vcAssociatedControlType>' + arrFieldConfig[iIndex].vcAssociatedControlType + '</vcAssociatedControlType>' + '\n';
            sXMLString += '<vcFieldType>' + arrFieldConfig[iIndex].vcFieldType + '</vcFieldType>' + '\n';
            sXMLString += '<vcFieldDataType>' + arrFieldConfig[iIndex].vcFieldDataType + '</vcFieldDataType>' + '\n';
            sXMLString += '<intColumnNum>' + arrFieldConfig[iIndex].intColumnNum + '</intColumnNum>' + '\n';
            sXMLString += '<intRowNum>' + arrFieldConfig[iIndex].intRowNum + '</intRowNum>' + '\n';
            sXMLString += '<boolRequired>' + arrFieldConfig[iIndex].boolRequired + '</boolRequired>' + '\n';
            sXMLString += '<numListID>' + arrFieldConfig[iIndex].numListID + '</numListID>' + '\n';
            sXMLString += '<boolAOIField>' + arrFieldConfig[iIndex].boolAOIField + '</boolAOIField>' + '\n';
            sXMLString += '<vcLookBackTableName>' + arrFieldConfig[iIndex].vcLookBackTableName + '</vcLookBackTableName>' + '\n';

            sXMLString += '</FormField>' + '\n';
        }
        if (arrFieldConfig[iIndex].intColumnNum == 0) {
            if (arrFieldConfig[iIndex].bitDefaultMandatory == "True") {
                sMissingMandatoryFields = sMissingMandatoryFields + arrFieldConfig[iIndex].vcFormFieldName + ', ';
            }
        }
    }
    sXMLString += '</FormFields>' + '\n';
    frmForm.hdXMLString.value = sXMLString;
}
/*
Purpose:	To validate the form data
Created By: Debasish Tapan Nag
Parameter:	1) None
Return		1) False/ True
*/
function validateFormData(frmForm) {
    if (frmForm.ddlFormList.value == 3) {
        if (frmForm.ddlGroup.value == 0) {
            alert("You must select group");
            return false;
        }
        for (var i = 0; i < frmForm.lstAvailablefld.options.length; i++) {
            var SelectedValue;
            SelectedValue = frmForm.lstAvailablefld.options[i].value;
            if (SelectedValue == "6R")
            {
                alert("You must add 'Relationship' field from list of available fields to one of columns.");
                return false;
            }
        }
        
    }


    Page_ClientValidate(); 						//explcitly call for validation
    if (!Page_IsValid) {
        return false;
    } else {

        if (frmForm.ddlFormList.value == 7 || frmForm.ddlFormList.value == 8) {
            var str = '';
            for (var i = 0; i < frmForm.lstSummaryAddedFlds.options.length; i++) {
                var SelectedValue;
                SelectedValue = frmForm.lstSummaryAddedFlds.options[i].value;
                str = str + SelectedValue + ','
            }
            frmForm.hdBizDocSumm.value = str;
        }



        if (frmForm.lstSelectedfldOne.options.length > 0) {
            PreSaveProcess(frmForm);
            sMissingMandatoryFields = ''; 							//Added on request from Carl on 09/03/2005 to make no fields mandatory
            if (sMissingMandatoryFields != '') {
                sMissingMandatoryFields = sMissingMandatoryFields.substring(0, sMissingMandatoryFields.length - 2);
                alert(sMissingMandatoryFields + ' must be selected as mandatory fields.');
                return false;
            }
            frmForm.hdSaveConfigurationStatus.value = "Yes";
            if (frmForm.txtobjAdditionalParam)
                frmForm.hdFormAdditionalParam.value = frmForm.txtobjAdditionalParam.value;
            else if (frmForm.ddlobjAdditionalParam)
                frmForm.hdFormAdditionalParam.value = frmForm.ddlobjAdditionalParam.options[frmForm.ddlobjAdditionalParam.selectedIndex].value;


            return true;
        }
        var sColumnIndex;
        sColumnIndex = '';
        if (frmForm.lstSelectedfldOne.options.length == 0)
            sColumnIndex = '1st' + ',';

        if (document.getElementById('lstSelectedfldThree') && document.getElementById('lstSelectedfldTwo')) {
            if (frmForm.lstSelectedfldThree.options.length != 0 && frmForm.lstSelectedfldTwo.options.length == 0)
                sColumnIndex += '2nd' + ',';
        }
        sColumnIndex = sColumnIndex.substring(0, sColumnIndex.length - 1);
        alert('Please select fields for the ' + sColumnIndex + ' column(s).');
        return false;
    }
}
/*
Purpose:	To associate the option with a (*) to indicate mandatory field
Created By: Debasish Tapan Nag
Parameter:	1) tbox: The target list box
Return		1) False
*/
function AssociateMandatory(tbox) {
    var SelectedText, SelectedValue;
    for (var i = 0; i < tbox.options.length; i++) {
        if (tbox.options[i].selected && tbox.options[i].value != "" && tbox.options[i].text.substring(0, 4) != '(*) ') {
            SelectedText = tbox.options[i].text;
            SelectedValue = tbox.options[i].value;
            SelectedText = SelectedText.replace('(*)', '')
            SelectedText = '(*) ' + SelectedText
            tbox.options[i].text = SelectedText;
            RequiredField = 1;
        } else if (tbox.options[i].selected && tbox.options[i].value != "" && tbox.options[i].text.substring(0, 4) == '(*) ') {
            SelectedText = tbox.options[i].text;
            SelectedValue = tbox.options[i].value;
            SelectedText = SelectedText.substring(4, SelectedText.length);
            tbox.options[i].text = SelectedText;
            RequiredField = 0;
        }
    }

    for (var iIndex = 0; iIndex < arrFieldConfig.length; iIndex++) {
        if (arrFieldConfig[iIndex].numFormFieldId == SelectedValue) {
            arrFieldConfig[iIndex].boolRequired = RequiredField;
            break;
        }
    }
    return false;
}
/*
Purpose:	To set the current auth group id in the hidden variable before submitting the form
Created By: Debasish Tapan Nag
Parameter:	1) None
Return		1) None
*/
function setAuthenticationGroupId(formHandle) {
    formHandle.hdAuthenticationGroupId.value = formHandle.objAuthenticationGroupControl.value;
}
/*
Purpose:	To move the options upwards from one position in the listbox 
to another position in the same listbox
Created By: Debasish Tapan Nag
Parameter:	1) tbox: The target list box
Return		1) False
*/
function MoveUp(tbox, aoiFlag) {
    for (var i = 1; i < tbox.options.length; i++) {
        if (tbox.options[i].selected && tbox.options[i].value != "") {
            var SelectedText, SelectedValue, PrevSelectedValue;
            SelectedValue = tbox.options[i].value;
            SelectedText = tbox.options[i].text;
            PrevSelectedValue = tbox.options[i - 1].value;
            //calls function to swap the array
            swapRowPosition(SelectedValue, i, PrevSelectedValue, i - 1, aoiFlag);
            tbox.options[i].value = tbox.options[i - 1].value;
            tbox.options[i].text = tbox.options[i - 1].text;
            tbox.options[i - 1].value = SelectedValue;
            tbox.options[i - 1].text = SelectedText;
            tbox.options[i - 1].selected = true;
        }
    }
    return false;
}
/*
Purpose:	To move the options downwards from one position in the listbox 
to another position in the same listbox
Created By: Debasish Tapan Nag
Parameter:	1) tbox: The target list box
Return		1) False
*/
function MoveDown(tbox, aoiFlag) {
    var SelectedText, SelectedValue, NextSelectedValue, Opt;
    for (var i = 0; i < tbox.options.length - 1; i++) {

        if (tbox.options[i].selected && tbox.options[i].value != "") {


            SelectedValue = tbox.options[i].value;
            SelectedText = tbox.options[i].text;
            NextSelectedValue = tbox.options[i].value;
            //calls function to swap the array
            //swapRowPosition(SelectedValue,i,NextSelectedValue,i+2,aoiFlag);
            Opt = i + 1
            tbox.options[i].value = tbox.options[i + 1].value;
            tbox.options[i].text = tbox.options[i + 1].text;
            tbox.options[i + 1].value = SelectedValue;
            tbox.options[i + 1].text = SelectedText;

        }
    }
    if (Opt <= tbox.options.length) {
        tbox.options[Opt].selected = true;
    }
    return false;
}

sortitems = 0;  // 0-False , 1-True
/*
Purpose:	To move the options from one listbox to another
Created By: Debasish Tapan Nag
Parameter:	1) fbox:The from list box
2) tbox: The target list box
3) colno: The column no to which element is shifted
4) aoiflag: aoi flag
Return		1) False
*/
function move(fbox, tbox, colno, aoiflag) {
    fBoxSelectedIndex = -1;
    if (fbox != null) {
        for (var i = 0; i < fbox.options.length; i++) {
            if (fbox.options[i].selected && fbox.options[i].value != "") {
                /// to check for duplicates 
                for (var j = 0; j < tbox.options.length; j++) {
                    if (tbox.options[j].value == fbox.options[i].value) {
                        alert("Item is already selected");
                        return false;
                    }
                }
                fBoxSelectedIndex = fbox.options[i].value;
                var no = new Option();
                no.value = fbox.options[i].value;
                no.text = fbox.options[i].text;
                tbox.options[tbox.options.length] = no;
                fbox.options[i].value = "";
                fbox.options[i].text = "";
            }
        }
        BumpUp(fbox, aoiflag);
        if (sortitems) SortD(tbox);
        //calls function to swap the array
        swapColumnPosition(fBoxSelectedIndex, colno, tbox.options.length - 1, aoiflag)
    }

    return false;
}
/*
Purpose:	To remove the options from one listbox to another
Created By: Debasish Tapan Nag
Parameter:	1) fbox:The from list box
2) tbox: The target list box
3) aoiflag: aoi flag
Return		1) False
*/
/*renamed function from remove to remove1 since it was cerating issue in chrome. which deletes button itself who calls this function.*/
function remove1(fbox, tbox, aoiflag) {
    var fboxSelectedElementName;
    fBoxSelectedIndex = -1;
    for (var i = 0; i < fbox.options.length; i++) {
        if (fbox.options[i].selected && fbox.options[i].value != "") {
            fBoxSelectedIndex = fbox.options[i].value;
            fboxSelectedElementName = getFieldName(fBoxSelectedIndex, aoiflag)
            //if(!boolReferencesAreClear(fboxSelectedElementName))
            //{			
            //calls function to swap the array
            swapColumnPosition(fBoxSelectedIndex, 0, 0, aoiflag);

            /// to check for duplicates 
            for (var j = 0; j < tbox.options.length; j++) {
                if (tbox.options[j].value == fbox.options[i].value) {
                    fbox.options[i].value = "";
                    fbox.options[i].text = "";
                    BumpUp(fbox, aoiflag);
                    if (sortitems) SortD(tbox);
                    return false;


                    //alert("Item is already selected");
                    //return false;
                }
            }
            var no = new Option();
            no.value = fbox.options[i].value;
            if (fbox.options[i].text.substring(0, 4) != '(*) ')
                no.text = fbox.options[i].text;
            else
                no.text = fbox.options[i].text.substring(4, fbox.options[i].text.length);
            tbox.options[tbox.options.length] = no;
            fbox.options[i].value = "";
            fbox.options[i].text = "";
            //}else{
            //alert('The selected field is referenced in Auto Rules and cannot be removed unless the Rule is changed.');
            //return false;
            //}	
        }
    }
    BumpUp(fbox, aoiflag);
    if (sortitems) SortD(tbox);
    return false;
}
/*
Purpose:	To shift the option upwards after one is removed from the middle
Created By: Debasish Tapan Nag
Parameter:	1) box:The list box under consideration
Return		1) None
*/
function BumpUp(box, aoiflag) {
    for (var i = 0; i < box.options.length; i++) {
        if (box.options[i].value == "") {
            for (var j = i; j < box.options.length - 1; j++) {
                PrevSelectedValue = box.options[j].value;
                SelectedValue = box.options[j + 1].value;
                box.options[j].value = box.options[j + 1].value;
                box.options[j].text = box.options[j + 1].text;
                swapRowPosition(SelectedValue, parseInt(j) + 1, PrevSelectedValue, j, aoiflag);
            }
            var ln = i;
            break;
        }
    }
    if (ln < box.options.length) {
        box.options.length -= 1;
        BumpUp(box, aoiflag);
    }
}
/*
Purpose:	To sort the options in the drop down
Created By: Debasish Tapan Nag
Parameter:	1) box:The list box under consideration
Return		1) None
*/
function SortD(box) {
    var temp_opts = new Array();
    var temp = new Object();
    for (var i = 0; i < box.options.length; i++) {
        temp_opts[i] = box.options[i];
    }
    for (var x = 0; x < temp_opts.length - 1; x++) {
        for (var y = (x + 1); y < temp_opts.length; y++) {
            if (temp_opts[x].text > temp_opts[y].text) {
                temp = temp_opts[x].text;
                temp_opts[x].text = temp_opts[y].text;
                temp_opts[y].text = temp;
                temp = temp_opts[x].value;
                temp_opts[x].value = temp_opts[y].value;
                temp_opts[y].value = temp;
            }
        }
    }
    for (var i = 0; i < box.options.length; i++) {
        box.options[i].value = temp_opts[i].value;
        box.options[i].text = temp_opts[i].text;
    }
}
/*
Purpose:	Checks if the field is referenced in a Auto Rule
Created By: Debasish Tapan Nag
Parameter:	1) thisFieldName: The field which is being checked for reference
Return		1) true: Reference Exists/ false: Reference does not exist
*/
function boolReferencesAreClear(thisFieldName) {
    var arrReferencedFields = new Array();
    arrReferencedFields = sUsedFieldsInAutoRules.split(',');
    for (iArrIndex = 0; iArrIndex < arrReferencedFields.length; iArrIndex++) {
        if (thisFieldName == arrReferencedFields[iArrIndex]) { return true; }
    }
    return false;
}
/*
Purpose:	Returns
Created By: Debasish Tapan Nag
Parameter:	1) strURL: Opens the URL in a new window
Return		1) false
*/
function getFieldName(fBoxSelectedValue, aoiFlag) {
    for (var iIndex = 0; iIndex < arrFieldConfig.length; iIndex++) {
        if (arrFieldConfig[iIndex].numFormFieldId == fBoxSelectedValue && arrFieldConfig[iIndex].boolAOIField == aoiFlag) {
            if (aoiFlag == 1)
                return arrFieldConfig[iIndex].vcDbColumnName + arrFieldConfig[iIndex].numFormFieldId.substring(0, arrFieldConfig[iIndex].numFormFieldId.length - 1);
            else
                return arrFieldConfig[iIndex].vcDbColumnName;
        }
    }
}
/*
Purpose:	Opens the URL
Created By: Debasish Tapan Nag
Parameter:	1) strURL: Opens the URL in a new window
Return		1) false
*/
function OpenForm(strURL) {
    window.open(strURL)
    return false;
}