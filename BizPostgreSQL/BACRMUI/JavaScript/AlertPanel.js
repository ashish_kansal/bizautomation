﻿//Organization Module -1
//Case Module - 2
//Purchase Opportunity - 3
//Sales Opportunity - 4
//Purchase Order - 5
//Sales Order - 6
//Project - 7
//Email - 8
//Tickler - 9
//Process - 10
//var rootUrl = "http://localhost/BACRMUI";
//var rootUrl = "http://localhost:3113";
var rootUrl = "https://login.bizautomation.com";
function DataLoaded() {
    $("#orgMenu").empty();
    $.ajax({
        type: 'POST',
        async: false,
        contentType: "application/json; charset=utf-8",
        url: '' + rootUrl + '/Common/TicklerDisplay.aspx/OrgnizationAlert',
        data: "",
        success: function (data) {
            $(".orgazCount").empty();
            if (parseInt(data.d.length) >= 25) {
                $(".orgazCount").html("25+");
            } else {
                $(".orgazCount").html(data.d.length);
            }
            $.each(data.d, function (index, value) {
                $("#orgMenu").append("<li><a href=\"#\" onclick=DeleteAlertPanel(1," + value.recordId + "); id=" + value.recordId + " module=1 class=\"btnDelete\"><span><span>" + value.CRMType + "</span><span class=\"time\">" + value.createdDate + "</span></span><span class=\"message\"><b>" + value.companyName + "</b> Assigned By " + value.createdby + "</span></a><span id=" + value.recordId + " module=1 class=\"close btnDelete\" onclick=DeleteAlertPanelClose(1," + value.recordId + ");><i class=\"fa fa-close\"></i></span></li>");
            })
        }
    });
}
function CaseDataLoaded() {
    $("#caseMenu").empty();
    $.ajax({
        type: 'POST',
        async: false,
        contentType: "application/json; charset=utf-8",
        url: '' + rootUrl + '/Common/TicklerDisplay.aspx/CaseNAlert',
        data: "",
        success: function (data) {
            $(".caseCount").empty();
            if (parseInt(data.d.length) >= 25) {
                $(".caseCount").html("25+");
            } else {
                $(".caseCount").html(data.d.length);
            }
            $.each(data.d, function (index, value) {
                $("#caseMenu").append("<li><a href=\"#\" onclick=DeleteAlertPanel(2," + value.caseId + "); id=" + value.caseId + " module=1 class=\"btnDelete\"><span><span>" + value.CreatedBy + "</span><span class=\"time\">" + value.CreatedDate + "</span></span><span class=\"message\">New Case from <b>" + value.OrganizationName + "</b> By " + value.ContactName + "</span></a><span id=" + value.caseId + " module=2 class=\"close btnDelete\" onclick=DeleteAlertPanelClose(2," + value.caseId + ");><i class=\"fa fa-close\"></i></span></li>");
            })
        }
    });
}
function OpportunityDataLoaded() {
    $("#opportunityMenu").empty();
    $("#orderMenu").empty();
    var order = 0;
    var opportunity = 0;
    $.ajax({
        type: 'POST',
        async: false,
        contentType: "application/json; charset=utf-8",
        url: '' + rootUrl + '/Common/TicklerDisplay.aspx/POSAlert',
        data: "",
        success: function (data) {

            $.each(data.d, function (index, value) {
                if (value.OppType == "2" && value.OppStatus == "0") {
                    opportunity = opportunity + 1;
                    $("#opportunityMenu").append("<li><a href=\"#\" onclick=DeleteAlertPanel(3," + value.caseId + "); id=" + value.caseId + " module=1 class=\"btnDelete\"><span><span>Purchase Opportunity</span><span class=\"time\">" + value.CreatedDate + "</span></span><span class=\"message\"><b>" + value.CaseName + "</b> from <b>" + value.OrganizationName + "</b> By " + value.ContactName + "</span></a><span id=" + value.caseId + " module=2 class=\"close btnDelete\" onclick=DeleteAlertPanelClose(3," + value.caseId + ");><i class=\"fa fa-close\"></i></span></li>");
                }
                if (value.OppType == "1" && value.OppStatus == "0") {
                    opportunity = opportunity + 1;
                    $("#opportunityMenu").append("<li><a href=\"#\" onclick=DeleteAlertPanel(4," + value.caseId + "); id=" + value.caseId + " module=1 class=\"btnDelete\"><span><span>Sales Opportunity</span><span class=\"time\">" + value.CreatedDate + "</span></span><span class=\"message\"><b>" + value.CaseName + "</b> from <b>" + value.OrganizationName + "</b> By " + value.ContactName + "</span></a><span id=" + value.caseId + " module=2 class=\"close btnDelete\" onclick=DeleteAlertPanelClose(4," + value.caseId + ");><i class=\"fa fa-close\"></i></span></li>");
                }
                if (value.OppType == "2" && value.OppStatus == "1") {
                    order = order + 1;
                    $("#orderMenu").append("<li><a href=\"#\" onclick=DeleteAlertPanel(5," + value.caseId + "); id=" + value.caseId + " module=1 class=\"btnDelete\"><span><span>Purchase Order</span><span class=\"time\">" + value.CreatedDate + "</span></span><span class=\"message\"><b>" + value.CaseName + "</b> from <b>" + value.OrganizationName + "</b> By " + value.ContactName + "</span></a><span id=" + value.caseId + " module=2 class=\"close btnDelete\" onclick=DeleteAlertPanelClose(5," + value.caseId + ");><i class=\"fa fa-close\"></i></span></li>");
                }
                if (value.OppType == "1" && value.OppStatus == "1") {
                    order = order + 1;
                    $("#orderMenu").append("<li><a href=\"#\" onclick=DeleteAlertPanel(6," + value.caseId + "); id=" + value.caseId + " module=1 class=\"btnDelete\"><span><span>Sales Order</span><span class=\"time\">" + value.CreatedDate + "</span></span><span class=\"message\"><b>" + value.CaseName + "</b> from <b>" + value.OrganizationName + "</b> By " + value.ContactName + "</span></a><span id=" + value.caseId + " module=2 class=\"close btnDelete\" onclick=DeleteAlertPanelClose(6," + value.caseId + ");><i class=\"fa fa-close\"></i></span></li>");
                }
            })
            $(".opportunityCount").empty();
            if (parseInt(opportunity) >= 25) {
                $(".opportunityCount").html("25+");
            } else {
                $(".opportunityCount").html(opportunity);
            }
            $(".orderCount").empty();
            if (parseInt(order) >= 25) {
                $(".orderCount").html("25+");
            } else {
                $(".orderCount").html(order);
            }
        }
    });
}
function ProjectDataLoaded() {
    $("#projectMenu").empty();
    $(".projectCount").empty();
    var order = 0;
    var opportunity = 0;
    $.ajax({
        type: 'POST',
        async: false,
        contentType: "application/json; charset=utf-8",
        url: '' + rootUrl + '/Common/TicklerDisplay.aspx/ProjectAlert',
        data: "",
        success: function (data) {
            if (parseInt(data.d.length) >= 25) {
                $(".projectCount").html("25+");
            } else {
                $(".projectCount").html(data.d.length);
            }
            $.each(data.d, function (index, value) {
                $("#projectMenu").append("<li><a href=\"#\" onclick=DeleteAlertPanel(7," + value.caseId + "); id=" + value.caseId + " module=1 class=\"btnDelete\"><span><span>" + value.CreatedBy + "</span><span class=\"time\">" + value.CreatedDate + "</span></span><span class=\"message\"><b>" + value.CaseName + "</b> from <b>" + value.OrganizationName + "</b> Assigned By " + value.CreatedBy + "</span></a><span id=" + value.caseId + " module=2 class=\"close btnDelete\" onclick=DeleteAlertPanelClose(7," + value.caseId + ");><i class=\"fa fa-close\"></i></span></li>");
            })
        }
    });
}
function EmailDataLoaded() {
    $("#EmailMenu").empty();
    $(".EmailCount").empty();

    var order = 0;
    var opportunity = 0;
    $.ajax({
        type: 'POST',
        async: false,
        contentType: "application/json; charset=utf-8",
        url: '' + rootUrl + '/Common/TicklerDisplay.aspx/EmailAlert',
        data: "",
        success: function (data) {
            if (parseInt(data.d.length) >= 25) {
                $(".EmailCount").html("25+");
            } else {
                $(".EmailCount").html(data.d.length);
            }
            $.each(data.d, function (index, value) {
                $("#EmailMenu").append("<li><a href=\"#\" onclick=DeleteAlertPanel(8," + value.caseId + "); id=" + value.caseId + " module=1 class=\"btnDelete\"><span><span>" + value.CreatedBy + "</span><span class=\"time\">" + value.CreatedDate + "</span></span><span class=\"message\"><b>" + value.CaseName + "</b> from <b>" + value.CreatedBy + "</b> </span></a><span id=" + value.caseId + " module=2 class=\"close btnDelete\" onclick=DeleteAlertPanelClose(8," + value.caseId + ");><i class=\"fa fa-close\"></i></span></li>");
            })
        }
    });
}
function TicklerDataLoaded() {
    $("#TicklerMenu").empty();
    $(".TicklerCount").empty();
    $.ajax({
        type: 'POST',
        async: false,
        contentType: "application/json; charset=utf-8",
        url: '' + rootUrl + '/Common/TicklerDisplay.aspx/TicklerAlert',
        data: "",
        success: function (data) {
            if (parseInt(data.d.length) >= 25) {
                $(".TicklerCount").html("25+");
            } else {
                $(".TicklerCount").html(data.d.length);
            }
            $.each(data.d, function (index, value) {
                $("#TicklerMenu").append("<li><a href=\"#\" onclick=DeleteAlertPanel(9," + value.caseId + "); id=" + value.caseId + " module=1 class=\"btnDelete\"><span><span>" + value.CreatedBy + "</span><span class=\"time\">" + value.CreatedDate + "</span></span><span class=\"message\">A new Tickler from <b>" + value.CaseName + "</b> through <b>" + value.OppStatus + "</b> </span></a><span id=" + value.caseId + " module=2 class=\"close btnDelete\" onclick=DeleteAlertPanelClose(9," + value.caseId + ");><i class=\"fa fa-close\"></i></span></li>");
            })
        }
    });
}
function ProcessDataLoaded() {
    $("#ProcessMenu").empty();
    $(".ProcessCount").empty();

    var order = 0;
    var opportunity = 0;
    $.ajax({
        type: 'POST',
        async: false,
        contentType: "application/json; charset=utf-8",
        url: '' + rootUrl + '/Common/TicklerDisplay.aspx/ProcessAlert',
        data: "",
        success: function (data) {
            if (parseInt(data.d.length) >= 25) {
                $(".ProcessCount").html("25+");
            } else {
                $(".ProcessCount").html(data.d.length);
            }
            $.each(data.d, function (index, value) {
                $("#ProcessMenu").append("<li><a href=\"#\" onclick=DeleteAlertPanel(10," + value.caseId + "," + value.OppType + "," + value.OppStatus + "," + value.OrganizationName + "); id=" + value.caseId + " module=1 class=\"btnDelete\"><span><span>" + value.CreatedBy + "</span><span class=\"time\">" + value.CreatedDate + "</span></span><span class=\"message\"><b>" + value.CaseName + "</b> from <b>" + value.OrganizationName + "</b> Assigned By " + value.CreatedBy + "</span></a><span id=" + value.caseId + " module=2 class=\"close btnDelete\" onclick=DeleteAlertPanelClose(10," + value.caseId + ");><i class=\"fa fa-close\"></i></span></li>");
            })
        }
    });
}
function countDataLoaded() {
    $("#orgMenu").empty();
    $.ajax({
        type: 'POST',
        async: false,
        contentType: "application/json; charset=utf-8",
        url: '' + rootUrl + '/Common/TicklerDisplay.aspx/OrgnizationAlertcount',
        data: "",
        success: function (data) {
            $(".orgazCount").empty();
            if (parseInt(data.d) > 25) {
                $(".orgazCount").html("25+");
            } else {
                $(".orgazCount").html(data.d);
            }
            //$.each(data.d, function (index, value) {
            //    $("#orgMenu").append("<li><a href=\"#\" onclick=DeleteAlertPanel(1," + value.recordId + "); id=" + value.recordId + " module=1 class=\"btnDelete\"><span><span>" + value.CRMType + "</span><span class=\"time\">" + value.createdDate + "</span></span><span class=\"message\"><b>" + value.companyName + "</b> Assigned By " + value.createdby + "</span></a><span id=" + value.recordId + " module=1 class=\"close btnDelete\" onclick=DeleteAlertPanelClose(1," + value.recordId + ");><i class=\"fa fa-close\"></i></span></li>");
            //})
        }
    });
}
function countCaseDataLoaded() {
    $("#caseMenu").empty();
    $.ajax({
        type: 'POST',
        async: false,
        contentType: "application/json; charset=utf-8",
        url: '' + rootUrl + '/Common/TicklerDisplay.aspx/CaseNAlertcount',
        data: "",
        success: function (data) {
            $(".caseCount").empty();
            if (parseInt(data.d) > 25) {
                $(".caseCount").html("25+");
            } else {
                $(".caseCount").html(data.d);
            }
            //$.each(data.d, function (index, value) {
            //    $("#caseMenu").append("<li><a href=\"#\" onclick=DeleteAlertPanel(2," + value.caseId + "); id=" + value.caseId + " module=1 class=\"btnDelete\"><span><span>" + value.CreatedBy + "</span><span class=\"time\">" + value.CreatedDate + "</span></span><span class=\"message\">New Case from <b>" + value.OrganizationName + "</b> By " + value.ContactName + "</span></a><span id=" + value.caseId + " module=2 class=\"close btnDelete\" onclick=DeleteAlertPanelClose(2," + value.caseId + ");><i class=\"fa fa-close\"></i></span></li>");
            //})
        }
    });
}
function countOpportunityDataLoaded(type) {
    $("#opportunityMenu").empty();
    $("#orderMenu").empty();
    var order = 0;
    var opportunity = 0;
    $.ajax({
        type: 'POST',
        async: false,
        contentType: "application/json; charset=utf-8",
        url: '' + rootUrl + '/Common/TicklerDisplay.aspx/POSAlertcount',
        data: JSON.stringify({ "type": type }),
        success: function (data) {
            if (type == 1) {
                $(".opportunityCount").empty();
                if (parseInt(data.d) > 25) {
                    $(".opportunityCount").html("25+");
                } else {
                    $(".opportunityCount").html(data.d);
                }
            } else {
                $(".orderCount").empty();
                if (parseInt(data.d) > 25) {
                    $(".orderCount").html("25+");
                } else {
                    $(".orderCount").html(data.d);
                }
            }
        }
    });
}
function countProjectDataLoaded() {
    $("#projectMenu").empty();
    $(".projectCount").empty();
    var order = 0;
    var opportunity = 0;
    $.ajax({
        type: 'POST',
        async: false,
        contentType: "application/json; charset=utf-8",
        url: '' + rootUrl + '/Common/TicklerDisplay.aspx/ProjectAlertcount',
        data: "",
        success: function (data) {
            if (parseInt(data.d) > 25) {
                $(".projectCount").html("25+");
            } else {
                $(".projectCount").html(data.d);
            }
        }
    });
}
function countEmailDataLoaded() {
    $("#EmailMenu").empty();
    $(".EmailCount").empty();

    var order = 0;
    var opportunity = 0;
    $.ajax({
        type: 'POST',
        async: false,
        contentType: "application/json; charset=utf-8",
        url: '' + rootUrl + '/Common/TicklerDisplay.aspx/EmailAlertcount',
        data: "",
        success: function (data) {
            if (parseInt(data.d) > 25) {
                $(".EmailCount").html("25+");
            } else {
                $(".EmailCount").html(data.d);
            }
        }
    });
}
function countTicklerDataLoaded() {
    $("#TicklerMenu").empty();
    $(".TicklerCount").empty();
    $.ajax({
        type: 'POST',
        async: false,
        contentType: "application/json; charset=utf-8",
        url: '' + rootUrl + '/Common/TicklerDisplay.aspx/TicklerAlertcount',
        data: "",
        success: function (data) {
            if (parseInt(data.d) > 25) {
                $(".TicklerCount").html("25+");
            } else {
                $(".TicklerCount").html(data.d);
            }
        }
    });
}
function countProcessDataLoaded() {
    $("#ProcessMenu").empty();
    $(".ProcessCount").empty();

    var order = 0;
    var opportunity = 0;
    $.ajax({
        type: 'POST',
        async: false,
        contentType: "application/json; charset=utf-8",
        url: '' + rootUrl + '/Common/TicklerDisplay.aspx/ProcessAlertcount',
        data: "",
        success: function (data) {
            if (parseInt(data.d) > 25) {
                $(".ProcessCount").html("25+");
            } else {
                $(".ProcessCount").html(data.d);
            }
        }
    });
}


//Delete Records
function DeleteAlertPanelClose(mod, id) {
    var recordid = id;
    var module = mod;
    DeleteAlertPanelRecord(recordid, module);
    if (module == 1) {
        countDataLoaded();
    }
    if (module == 2) {
        countCaseDataLoaded();
    }
    if (module == 3) {
        countOpportunityDataLoaded(1);
    }
    if (module == 4) {
        countOpportunityDataLoaded(1);
    }
    if (module == 5) {
        countOpportunityDataLoaded(2);
    }
    if (module == 6) {
        countOpportunityDataLoaded(2);
    }
    if (module == 7) {
        countProjectDataLoaded();
    }
    if (module == 8) {
        countEmailDataLoaded();
    }
    if (module == 9) {
        countTicklerDataLoaded();
    }
    if (module == 10) {
        countProcessDataLoaded();
    }
}

//Delete View Records
function DeleteAlertPanel(mod, id, opptype, oppstatus, OppID) {
    var recordid = id;
    var module = mod;
    DeleteAlertPanelRecord(recordid, module);
    if (module == 1) {
        countDataLoaded();
        window.location.href = '' + rootUrl + '/Leads/frmLeads.aspx?DivID=' + recordid + '';
    }
    if (module == 2) {
        countCaseDataLoaded();
        window.location.href = '' + rootUrl + '/cases/frmCases.aspx?CaseID=' + recordid + '';
    }
    if (module == 3) {
        countOpportunityDataLoaded(1);
        window.location.href = '' + rootUrl + '/opportunity/frmOpportunities.aspx?opId=' + recordid + '';
    }
    if (module == 4) {
        countOpportunityDataLoaded(1);
        window.location.href = '' + rootUrl + '/opportunity/frmOpportunities.aspx?opId=' + recordid + '';
    }
    if (module == 5) {
        countOpportunityDataLoaded(2);
        window.location.href = '' + rootUrl + '/opportunity/frmOpportunities.aspx?opId=' + recordid + '';
    }
    if (module == 6) {
        countOpportunityDataLoaded(2);
        window.location.href = '' + rootUrl + '/opportunity/frmOpportunities.aspx?opId=' + recordid + '';
    }
    if (module == 7) {
        countProjectDataLoaded();
        window.location.href = '' + rootUrl + '/projects/frmProjects.aspx?ProId=' + recordid + '';
    }
    if (module == 8) {
        countEmailDataLoaded();
        window.location.href = '' + rootUrl + '/projects/frmProjects.aspx?ProId=' + recordid + '';
    }
    if (module == 9) {
        countTicklerDataLoaded();
        window.location.href = '' + rootUrl + '/admin/ActionItemDetailsOld.aspx?CommId=' + recordid + '&CaseId=0&CaseTimeId=0&CaseExpId=0';
    }
    if (module == 10) {
        countProcessDataLoaded();
        if (opptype == -1 && oppstatus == -1) {
            window.location.href = '' + rootUrl + '/projects/frmProjects.aspx?ProId=' + OppID + '';
        } else {
            window.location.href = ''+rootUrl+'/opportunity/frmOpportunities.aspx?opId=' + OppID + '';
        }
    }
}
function DeleteAlertPanelRecord(RecordId, ModuleId) {
    $("#newData").empty();
    $.ajax({
        type: 'POST',
        async: false,
        contentType: "application/json; charset=utf-8",
        url: '' + rootUrl + '/Common/TicklerDisplay.aspx/DeleteRecord',
        data: JSON.stringify({ "ModuleId": ModuleId, "RecordId": RecordId }),
        success: function (data) {
            if (data.d == "1") {
            }
        }
    });
}

//$(document).ready(function () {
//        $("#orgMenu").empty();
//        $.ajax({
//            type: 'POST',
//            async: false,
//            contentType: "application/json; charset=utf-8",
//            url: '' + rootUrl + '/Common/TicklerDisplay.aspx/AllAlertcount',
//            data: "",
//            success: function (data) {
//                $(".orgazCount").empty();
//                var object = data.d;
//                if (parseInt(object[0]) >= 25) {
//                    $(".orgazCount").html("25+");
//                } else {
//                    $(".orgazCount").html(object[0]);
//                }
//                $(".caseCount").empty();
//                if (parseInt(object[1]) > 25) {
//                    $(".caseCount").html("25+");
//                } else {
//                    $(".caseCount").html(object[1]);
//                }
//                $(".opportunityCount").empty();
//                if (parseInt(object[2]) > 25) {
//                    $(".opportunityCount").html("25+");
//                } else {
//                    $(".opportunityCount").html(object[2]);
//                }
//                $(".orderCount").empty();
//                if (parseInt(object[3]) > 25) {
//                    $(".orderCount").html("25+");
//                } else {
//                    $(".orderCount").html(object[3]);
//                }
//                $(".projectCount").empty();
//                if (parseInt(object[4]) > 25) {
//                    $(".projectCount").html("25+");
//                } else {
//                    $(".projectCount").html(object[4]);
//                }
//                $(".EmailCount").empty();
//                if (parseInt(object[5]) > 25) {
//                    $(".EmailCount").html("25+");
//                } else {
//                    $(".EmailCount").html(object[5]);
//                }
//                $(".TicklerCount").empty();
//                if (parseInt(object[6]) > 25) {
//                    $(".TicklerCount").html("25+");
//                } else {
//                    $(".TicklerCount").html(object[6]);
//                }
//                $(".ProcessCount").empty();
//                if (parseInt(object[7]) > 25) {
//                    $(".ProcessCount").html("25+");
//                } else {
//                    $(".ProcessCount").html(object[7]);
//                }
//            }
//        });
   
//})


$(function () {

    //Current Counts Status
    var COrg = $(".orgazCount").html();
    var CCase = $(".caseCount").html();
    var COpp = $(".opportunityCount").html();
    var COrder = $(".orderCount").html();
    var CProject = $(".projectCount").html();
    var CEmail = $(".EmailCount").html();
    var CTickler = $(".TicklerCount").html();
    var CProcess = $(".ProcessCount").html();

    //New Notifcation Status
    var NOrga = 0;
    var NCase = 0;
    var NOpportunity = 0;
    var NOrder = 0;
    var NProject = 0;
    var NEmail = 0;
    var NTickler = 0;
    var NProcess = 0;
    //var notify = $.connection.notificationsHub;
    $("#caseLi").click(function () {
        CaseDataLoaded();
        NCase = 0;
    })
    $("#orgLi").click(function () {
        DataLoaded();
        NOrga = 0;
    })
    $("#oppLi").click(function () {
        OpportunityDataLoaded();
        NOpportunity = 0;
    })
    $("#orderLi").click(function () {
        OpportunityDataLoaded();
        NOrder = 0;
    })
    $("#projectLi").click(function () {
        ProjectDataLoaded();
        NProject = 0;
    })
    $("#emailLi").click(function () {
        EmailDataLoaded();
        NEmail = 0;
    })
    $("#ticklerLi").click(function () {
        TicklerDataLoaded();
        NTickler = 0;
    })
    $("#processLi").click(function () {
        ProcessDataLoaded();
        NProcess = 0;
    })
    //$.connection.notificationsHub.client.displayNotification = function () {
    //    var data = "";
    //    countDataLoaded();
    //    if (COrg != $(".orgazCount").html()) {
    //        NOrga = 1;
    //        COrg = $(".orgazCount").html();
    //    }
    //    var OrganizationIntrvlId = setInterval(function () {
    //        if (NOrga == "1") {
    //            $(".iorgazCount").fadeTo('slow', 0.5).fadeTo('slow', 1.0);
    //        } else {
    //            $(".iorgazCount").stop().fadeTo('slow', 1);
    //            $(".iorgazCount").css('opacity', '1');
    //        }
    //    }, 2);
    //};
    //$.connection.caseHub.client.displayCaseNotification = function (msg) {
    //    var data = "";
    //    countCaseDataLoaded();
    //    if (CCase != $(".caseCount").html()) {
    //        NCase = 1;
    //        CCase = $(".caseCount").html();
    //    }
    //    var CaseIntrvlId = setInterval(function () {
    //        if (NCase == "1") {
    //            $(".icaseCount").fadeTo('slow', 0.5).fadeTo('slow', 1.0);
    //        } else {
    //            $(".icaseCount").stop().fadeTo('slow', 1);
    //            $(".icaseCount").css('opacity', '1');
    //        }
    //    }, 2);
    //};
    //$.connection.opportunityHub.client.displayOpportunityNotification = function (msg) {
    //    var data = "";
    //    countOpportunityDataLoaded(1);
    //    countOpportunityDataLoaded(2);
    //    if (COpp != $(".opportunityCount").html()) {
    //        NOpportunity = 1;
    //        COpp = $(".opportunityCount").html();
    //    }
    //    var OppIntrvlId = setInterval(function () {
    //        if (NOpportunity == "1") {
    //            $(".iopportunityCount").fadeTo('slow', 0.5).fadeTo('slow', 1.0);
    //        } else {
    //            $(".iopportunityCount").stop().fadeTo('slow', 1);
    //            $(".iopportunityCount").css('opacity', '1');
    //        }
    //    }, 2);
    //    if (COrder != $(".orderCount").html()) {
    //        NOrder = 1;
    //        COrder = $(".orderCount").html();
    //    }
    //    var OrderIntrvlId = setInterval(function () {
    //        if (NOrder == "1") {
    //            $(".iorderCount").fadeTo('slow', 0.5).fadeTo('slow', 1.0);
    //        } else {
    //            $(".iorderCount").stop().fadeTo('slow', 1);
    //            $(".iorderCount").css('opacity', '1');
    //        }
    //    }, 2);
    //};
    //$.connection.projectHub.client.displayProjectNotification = function (msg) {
    //    var data = "";
    //    countProjectDataLoaded();
    //    if (CProject != $(".projectCount").html()) {
    //        NProject = 1;
    //        CProject = $(".projectCount").html();
    //    }
    //    var ProjectIntrvlId = setInterval(function () {
    //        if (NProject == "1") {
    //            $(".iprojectCount").fadeTo('slow', 0.5).fadeTo('slow', 1.0);
    //        } else {
    //            $(".iprojectCount").stop().fadeTo('slow', 1);
    //            $(".iprojectCount").css('opacity', '1');
    //        }
    //    }, 2);
    //};
    //$.connection.emailHub.client.displayEmailNotification = function (msg) {
    //    var data = "";
    //    countEmailDataLoaded();
    //    if (CEmail != $(".EmailCount").html()) {
    //        NEmail = 1;
    //        CEmail = $(".EmailCount").html();
    //    }
    //    var EmailIntrvlId = setInterval(function () {
    //        if (NEmail == "1") {
    //            $(".iEmailCount").fadeTo('slow', 0.5).fadeTo('slow', 1.0);
    //        } else {
    //            $(".iEmailCount").stop().fadeTo('slow', 1);
    //            $(".iEmailCount").css('opacity', '1');
    //        }
    //    }, 2);
    //};
    //$.connection.ticklerHub.client.displayTicklerNotification = function (msg) {
    //    var data = "";
    //    countTicklerDataLoaded();
    //    if (CTickler != $(".TicklerCount").html()) {
    //        NTickler = 1;
    //        CTickler = $(".TicklerCount").html();
    //    }
    //    var TicklerIntrvlId = setInterval(function () {
    //        if (NTickler == "1") {
    //            $(".iTicklerCount").fadeTo('slow', 0.5).fadeTo('slow', 1.0);
    //        } else {
    //            $(".iTicklerCount").stop().fadeTo('slow', 1)
    //            $(".iTicklerCount").css('opacity', '1');
    //        }
    //    }, 2);
    //};
    //$.connection.processHub.client.displayProcessNotification = function (msg) {
    //    var data = "";
    //    countProcessDataLoaded();
    //    if (CProcess != $(".ProcessCount").html()) {
    //        NProcess = 1;
    //        CProcess = $(".ProcessCount").html();
    //    }
    //    var CaseIntrvlId = setInterval(function () {
    //        if (NProcess == "1") {
    //            $(".iProcessCount").fadeTo('slow', 0.5).fadeTo('slow', 1.0);
    //        } else {
    //            $(".iProcessCount").stop().fadeTo('slow', 1)
    //            $(".iProcessCount").css('opacity', '1');
    //        }
    //    }, 2);
    //};
    //$.connection.hub.start().done(function () {
    //    //alert("Connection Started");
    //});
});