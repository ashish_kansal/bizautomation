﻿var isCalAmtBasedonDepItems = false;
var currency = "";
var kiteItemIDBizKitsWithChildKitsJS = 0;
var isPreventOrphanedParents = true;


function LoadChildKits(itemCode) {
    try {
        kiteItemIDBizKitsWithChildKitsJS = itemCode;

        if ($("[id$=divKitChild]").hasClass("modal")) {
            $("[id$=divKitChild]").modal('show');
        } else {
            $("[id$=divKitChild]").show();
        }

        $("#divLoader").show();

        $.ajax({
            type: "POST",
            url: '../WebServices/CommonService.svc/GetChildKitsOfKit',
            contentType: "application/json",
            dataType: "json",
            data: JSON.stringify({
                "numItemCode": itemCode
            }),
            success: function (data) {
                try {
                    var obj = $.parseJSON(data.GetChildKitsOfKitResult);
                    var itemDetail = $.parseJSON(obj.Table1);
                    isCalAmtBasedonDepItems = JSON.parse(itemDetail[0].bitCalAmtBasedonDepItems);
                    isPreventOrphanedParents = JSON.parse(itemDetail[0].bitPreventOrphanedParents);
                    currency = (itemDetail[0].vcCurrency || "$");
                    $("#lblKitItem").text(itemDetail[0].vcItemName);
                    $("#lblKitDescription").text(itemDetail[0].txtItemDesc);
                    if (!isCalAmtBasedonDepItems) {
                        $("#lblKitCalculaedPrice").text(currency + " " + (itemDetail[0].monListPrice || 0).toLocaleString("en"));
                    }
                    if (itemDetail[0].vcPathForTImage != "") {
                        $("#imgKit").attr("src", $("[id$=hdnPortalURL]").val() + itemDetail[0].vcPathForTImage);
                    }

                    var childKits = $.parseJSON(obj.Table2);

                    if (childKits != null) {
                        var promises = [];
                        var html = "";
                        var slickHorizontalDisplayKits = [];

                        childKits.forEach(function (n, index) {
                            if ((n.vcParentKits || "") == "") {

                                $.ajax({
                                    type: "POST",
                                    url: '../WebServices/CommonService.svc/GetKitChildItems',
                                    contentType: "application/json",
                                    dataType: "json",
                                    async: false,
                                    data: JSON.stringify({
                                        "numMainKitItemCode": itemCode
                                        , "numChildKitItemCode": parseInt(n.numItemCode)
                                    }),
                                    success: function (data) {
                                        var kitChildItems = $.parseJSON(data.GetKitChildItemsResult);
                                        try {
                                            html += "<div class='panel panel-primary panelchildKit' MainKitItemCode='" + itemCode + "'ChildKitItemCode='" + n.numItemCode + "' DependedItems='" + (n.vcDependedKits || "") + "' IsKitSingleSelect='" + JSON.parse(n.bitKitSingleSelect) + "' id='childKit" + n.numItemCode + "'>";
                                            html += "<div class='panel-heading'>";
                                            html += "<div class='panel-title' style='font-size:14px;'>"
                                            html += "<a data-toggle='collapse' href='#" + n.numItemCode + "' data-parent='#divChildKitContainer'><b>" + (n.vcItemName || "") + "</b></a>";
                                            html += "&nbsp;&nbsp;<i class='selectedChilds'></i>";
                                            html += "</div>";
                                            html += "</div>";
                                            html += "<div id='" + n.numItemCode + "' style='padding-left:30px; padding-right:30px' class='panel-body panel-collapse collapse " + (index == 0 ? "in" : "") + "'>";
                                            if (parseInt(n.tintView) == 2) {
                                                html += "<div class='slick-container' style='width:100%'>";
                                            }
                                            html += GetChildKitItemsHtml(n.numItemCode, JSON.parse(n.bitKitSingleSelect), parseInt(n.tintView), kitChildItems, JSON.parse(n.bitOrderEditable));
                                            if (parseInt(n.tintView) == 2) {
                                                html += "</div>";
                                            }
                                            html += "</div>";
                                            html += "</div>";

                                            $("#divLoader").hide();
                                        } catch (err) {
                                            alert("Unknown error occurred while loading kit childs.");
                                            $("#divLoader").hide();
                                        }
                                    },
                                    error: function (jqXHR, textStatus, errorThrown) {
                                        $("#divLoader").hide();
                                        alert("Unknown error occurred while loading kit childs.");
                                    }
                                });
                            } else {
                                html += "<div class='panel panel-primary panelchildKit' MainKitItemCode='" + itemCode + "'ChildKitItemCode='" + n.numItemCode + "' DependedItems='" + (n.vcDependedKits || "") + "' id='childKit" + n.numItemCode + "' style='display:none'>";
                                html += "<div class='panel-heading'>";
                                html += "<div class='panel-title' style='font-size:14px;'>"
                                html += "<a data-toggle='collapse' href='#" + n.numItemCode + "' data-parent='#divChildKitContainer'><b>" + (n.vcItemName || "") + "</b></a>";
                                html += "&nbsp;&nbsp;<i class='selectedChilds'></i>";
                                html += "</div>";
                                html += "</div>";
                                html += "<div id='" + n.numItemCode + "' style='padding-left:30px; padding-right:30px' class='panel-body panel-collapse collapse'>";
                                if (parseInt(n.tintView) == 2) {
                                    html += "<div class='slick-container' style='width:100%'></div>";
                                }
                                html += "</div>";
                                html += "</div>";
                            }

                            if (parseInt(n.tintView) == 2) {
                                slickHorizontalDisplayKits.push(n.numItemCode);
                            }
                        });

                        $("#divChildKitContainer").append(html);

                        AutoSelectIfOnlyOneOption();

                        slickHorizontalDisplayKits.forEach(function (numItemCode) {
                            LoadSlickSlider(numItemCode);

                            //$("#divKitChild #" + numItemCode).on('shown.bs.collapse', function () {
                            //    $("#divKitChild #" + numItemCode).find('div.slick-container').slick("refresh");
                            //});
                        });
                    }
                } catch (e) {
                    alert("Unknown error occurred while loading kit childs.");
                    $("#divLoader").hide();
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                $("#divLoader").hide();
                alert("Unknown error occurred while loading kit childs.");
            }
        });
    } catch (e) {
        alert("Unknown error occurred while loading kit childs.");
        $("#divLoader").hide();
    }
}

function GetCalculatedPrice() {
    try {
        var vcCurrentKitConfiguration = "";

        $("div.panelchildKit").each(function (index, kit) {
            if ($(kit).is(":visible")) {
                $(kit).find("input.kititem").each(function () {
                    if ($(this).is(":checked")) {
                        vcCurrentKitConfiguration += ((vcCurrentKitConfiguration == "" ? "" : ",") + (parseInt($(kit).attr("ChildKitItemCode")) + "-" + $(this).val() + ($(this).parent().find(".txtQtyKit").length > 0 ? ("-" + parseFloat($(this).parent().find(".txtQtyKit").val()) * parseFloat($(this).parent().find(".txtQtyKit").attr("uom"))) : ("-" + parseFloat($(this).attr("qty"))))));
                    }
                });
            }
        });

        $.ajax({
            type: "POST",
            url: '../WebServices/CommonService.svc/GetItemCalculatedPrice',
            contentType: "application/json",
            dataType: "json",
            async: false,
            data: JSON.stringify({
                "itemCode": kiteItemIDBizKitsWithChildKitsJS
                , "qty": 1
                , "vcCurrentKitConfiguration": vcCurrentKitConfiguration
            }),
            success: function (data) {
                var objResult = $.parseJSON(data.GetItemCalculatedPriceResult);
                try {
                    $("#lblKitCalculaedPrice").text(currency + " " + (objResult.price || 0).toLocaleString("en"));
                } catch (err) {

                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
            }
        });
    } catch (e) {
        //DO NOT THROW ERROR
    }
}

function KitChildSelectionChanged(control, isCheckbox) {
    try {
        if ($(control).closest("div.panelchildKit").length > 0) {
            var panel = $(control).closest("div.panelchildKit");
            var mainKitItemCode = parseInt($(panel).attr("MainKitItemCode"));
            var childKitItemCode = parseInt($(panel).attr("ChildKitItemCode"));
            var dependedItems = $(panel).attr("DependedItems");

            var vcSelectedChildItems = "";
            $(control).closest("div.panelchildKit").find("input.kititem").each(function () {
                if ($(this).is(":checked")) {
                    vcSelectedChildItems += ((vcSelectedChildItems == "" ? "" : ", ") + $(this).attr("itemName"));
                }
            });

            if (vcSelectedChildItems != "") {
                vcSelectedChildItems = "(" + vcSelectedChildItems + ")";
            }

            $(control).closest("div.panelchildKit").find(".panel-heading i.selectedChilds").text(vcSelectedChildItems);


            if (dependedItems != "") {
                var vcCurrentKitConfiguration = "";

                $("div.panelchildKit").each(function (index, kit) {
                    $(kit).find("input.kititem").each(function () {
                        if ($(this).is(":checked")) {
                            vcCurrentKitConfiguration = (vcCurrentKitConfiguration == "" ? (parseInt($(kit).attr("ChildKitItemCode")) + "-" + $(this).val()) : (vcCurrentKitConfiguration + "," + (parseInt($(kit).attr("ChildKitItemCode")) + "-" + $(this).val())));
                            vcSelectedChildItems += ((vcSelectedChildItems == "" ? "" : ", ") + $(this).attr("itemName"));
                        }
                    });
                });

                $("#divLoader").show();

                $.ajax({
                    type: "POST",
                    url: '../WebServices/CommonService.svc/GetDependedKitChilds',
                    contentType: "application/json",
                    dataType: "json",
                    async: false,
                    data: JSON.stringify({
                        "numMainKitItemCode": mainKitItemCode
                        , "numChildKitItemCode": childKitItemCode
                        , "vcCurrentKitConfiguration": vcCurrentKitConfiguration
                    }),
                    success: function (data) {
                        var objResult = $.parseJSON(data.GetDependedKitChildsResult);
                        try {
                            if (objResult != null) {
                                var childKits = objResult.childKits;
                                var childKitItems = objResult.childKitItems;

                                if (childKits != null) {
                                    childKits.forEach(function (objChildKit) {
                                        if ($("#divKitChild #" + objChildKit.numChildKitItemCode).length > 0) {
                                            $("#divKitChild #childKit" + objChildKit.numChildKitItemCode).find(".panel-heading i.selectedChilds").text("");
                                            if (parseInt(objChildKit.tintView) == 2) {
                                                $("#divKitChild #" + objChildKit.numChildKitItemCode).find("div.slick-container").html("");
                                            } else {
                                                $("#divKitChild #" + objChildKit.numChildKitItemCode).html("");
                                            }

                                            if (objChildKit.bitDoNotShow) {
                                                $("#divKitChild #" + objChildKit.numChildKitItemCode).html("");
                                                $("#divKitChild #" + objChildKit.numChildKitItemCode).closest("div.panelchildKit").hide();
                                            } else {
                                                $("#divKitChild #" + objChildKit.numChildKitItemCode).closest("div.panelchildKit").show();

                                                if (childKitItems != null) {
                                                    var items = $.grep(childKitItems, function (objChildKitItem) {
                                                        return objChildKitItem.numChildKitItemCode == objChildKit.numChildKitItemCode;
                                                    });

                                                    if (items.length > 0) {
                                                        if (parseInt(objChildKit.tintView) == 2) {
                                                            $("#divKitChild #" + objChildKit.numChildKitItemCode).find("div.slick-container").slick('unslick');
                                                            $("#divKitChild #" + objChildKit.numChildKitItemCode).find("div.slick-container").html(GetChildKitItemsHtml(objChildKit.numChildKitItemCode, JSON.parse(objChildKit.bitKitSingleSelect), parseInt(objChildKit.tintView), items, JSON.parse(objChildKit.bitOrderEditable)));
                                                            LoadSlickSlider(objChildKit.numChildKitItemCode);
                                                        } else {
                                                            $("#divKitChild #" + objChildKit.numChildKitItemCode).html(GetChildKitItemsHtml(objChildKit.numChildKitItemCode, JSON.parse(objChildKit.bitKitSingleSelect), parseInt(objChildKit.tintView), items, JSON.parse(objChildKit.bitOrderEditable)));
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    });
                                }
                            }

                            AutoSelectIfOnlyOneOption();

                            if (!isCheckbox) {
                                FocusNextKit($(control).closest("div.panelchildKit"));
                            }

                            $("#divLoader").hide();
                        } catch (err) {
                            $("#divLoader").hide();
                            alert("Unknown error occurred while loading kit selection changes.");
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        $("#divLoader").hide();
                        alert("Unknown error occurred while loading kit selection changes.");
                    }
                });
            } else {
                if (!isCheckbox) {
                    FocusNextKit($(control).closest("div.panelchildKit"));
                }
            }
        }

        if (isCalAmtBasedonDepItems) {
            GetCalculatedPrice();
        }
    } catch (e) {
        alert("Unknown error occurred while loading kit selection changes.");
    }
}

function GetChildKitItemsHtml(numItemCode, bitKitSingleSelect, tintView, kitChildItems, bitOrderEditable) {
    try {
        var html = "";

        if (kitChildItems != null) {
            if (bitKitSingleSelect) {
                if (tintView == 2) {
                    kitChildItems.forEach(function (objChildItem) {
                        html += "<div class='slick-item' onclick='SlickItemSelected(this, false);'>";
                        html += "<input type='radio' itemName='" + objChildItem.vcItemName.replace("'", "").replace("\"", "") + "' unitName='" + objChildItem.UnitName.replace("'", "").replace("\"", "") + "' qty='" + objChildItem.Qty + "' style='display:none' class='kititem' name='kitChildItem" + numItemCode + "' value='" + objChildItem.numItemCode + "' />";
                        html += ReplaceImagePlaceHolder(objChildItem.vcDisplayText || "");
                        if (bitOrderEditable) {
                            html += "<input type='text' style='width:90px;display:inline-block;' class='form-control txtQtyKit' uom='" + objChildItem.fltUOMConversion + "' value='" + objChildItem.Qty + "' />" + objChildItem.vcSaleUOMName;
                        }
                        html += "</div>";
                    });
                } else {
                    kitChildItems.forEach(function (objChildItem) {
                        html += "<ul class='list-inline'>";
                        html += "<li>";
                        html += "<input type='radio' itemName='" + objChildItem.vcItemName.replace("'", "").replace("\"", "") + "' unitName='" + objChildItem.UnitName.replace("'", "").replace("\"", "") + "' qty='" + objChildItem.Qty + "' class='kititem' name='kitChildItem" + numItemCode + "' value='" + objChildItem.numItemCode + "' onChange='KitChildSelectionChanged(this,false)' />";
                        html += "</li>"
                        if (bitOrderEditable) {
                            html += "<li>";
                            html += "<input type='text' style='width:90px;display:inline-block;' class='form-control txtQtyKit' uom='" + objChildItem.fltUOMConversion + "' value='" + objChildItem.Qty + "' />" + objChildItem.vcSaleUOMName;
                            html += "</li>";
                        }
                        html += "<li>";
                        html += ReplaceImagePlaceHolder(objChildItem.vcDisplayText || "");
                        html += "</li>";
                        html += "</ul>";
                    });
                }
            } else {
                if (tintView == 2) {
                    kitChildItems.forEach(function (objChildItem) {
                        html += "<div class='slick-item' onclick='SlickItemSelected(this, true);'>";
                        html += "<input itemName='" + objChildItem.vcItemName.replace("'", "").replace("\"", "") + "' unitName='" + objChildItem.UnitName.replace("'", "").replace("\"", "") + "' qty='" + objChildItem.Qty + "' style='display:none' type='checkbox' class='kititem' value='" + objChildItem.numItemCode + "' />";
                        html += ReplaceImagePlaceHolder(objChildItem.vcDisplayText || "");
                        if (bitOrderEditable) {
                            html += "<input type='text' style='width:90px;display:inline-block;' class='form-control txtQtyKit' uom='" + objChildItem.fltUOMConversion + "' value='" + objChildItem.Qty + "' />" + objChildItem.vcSaleUOMName;
                        }
                        html += "</div>";
                    });
                } else {
                    kitChildItems.forEach(function (objChildItem) {
                        html += "<ul class='list-inline'>";
                        html += "<li>";
                        html += "<input itemName='" + objChildItem.vcItemName.replace("'", "").replace("\"", "") + "' unitName='" + objChildItem.UnitName.replace("'", "").replace("\"", "") + "' qty='" + objChildItem.Qty + "' type='checkbox' class='kititem' value='" + objChildItem.numItemCode + "' onChange='KitChildSelectionChanged(this,true)' />";
                        html += "</li>"
                        if (bitOrderEditable) {
                            html += "<li>";
                            html += "<input type='text' style='width:90px;display:inline-block;' class='form-control txtQtyKit' uom='" + objChildItem.fltUOMConversion + "' value='" + objChildItem.Qty + "' />" + objChildItem.vcSaleUOMName;
                            html += "</li>";
                        }
                        html += "<li>";
                        html += ReplaceImagePlaceHolder(objChildItem.vcDisplayText || "");
                        html += "</li>";
                        html += "</ul>";
                    });
                }
            }
        }

        return html;
    } catch (e) {
        throw e;
    }
}

function SlickItemSelected(slickItem, isCheckBox) {
    try {
        if (window.event.target == null || !$(window.event.target).hasClass("txtQtyKit")) {
            if (isCheckBox) {
                if ($(slickItem).find("input.kititem").is(":checked")) {
                    $(slickItem).removeClass("active");
                    $(slickItem).find("input.kititem").prop("checked", false);
                } else {
                    $(slickItem).addClass("active");
                    $(slickItem).find("input.kititem").prop("checked", true);
                }
            } else {
                $(slickItem).closest(".panel-body").find(".slick-item").removeClass("active");
                $(slickItem).closest(".panel-body").find(".slick-item").find("input.kititem").prop("checked", false);

                if ($(slickItem).find("input.kititem").is(":checked")) {
                    $(slickItem).removeClass("active");
                    $(slickItem).find("input.kititem").prop("checked", false);
                } else {
                    $(slickItem).addClass("active");
                    $(slickItem).find("input.kititem").prop("checked", true);
                }
            }

            KitChildSelectionChanged($(slickItem).find("input.kititem")[0], isCheckBox)
        }
    } catch (e) {
        alert("Unknown error occurred while loading kit selection changes.");
    }
}

function LoadSlickSlider(numItemCode) {
    $("#divKitChild #" + numItemCode + " div.slick-container").slick({
        slidesToShow: 5,
        slidesToScroll: 5,
        infinite: false,
        dots: false,
        adaptiveHeight: true,
        variableWidth: true,
        arrows: true,
        responsive: [
            {
                breakpoint: 1200,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3
                }
            },
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 4,
                    slidesToScroll: 4
                }
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    variableWidth: true,
                    arrows: false
                }
            }
        ]
    });

    //var childrenWidth = 0;
    //$("#divKitChild #" + numItemCode + " div.slick-container").find('.slick-track').children().each(function () {
    //    childrenWidth += $(this).width();
    //});
    //var outerContainerWidth = $("#divKitChild #" + numItemCode + " div.slick-container").width();

    //if (childrenWidth < outerContainerWidth) {
    //    var nextArrow = $("#divKitChild #" + numItemCode + " div.slick-container").find('.slick-next');
    //    nextArrow.hide();

    //    var prevArrow = $("#divKitChild #" + numItemCode + " div.slick-container").find('.slick-prev');
    //    prevArrow.hide();
    //}
    //else {
    //    var nextArrow = $("#divKitChild #" + numItemCode + " div.slick-container").find('.slick-next');
    //    nextArrow.show();

    //    var prevArrow = $("#divKitChild #" + numItemCode + " div.slick-container").find('.slick-prev');
    //    prevArrow.show();
    //}
}

function GetChildKitItemSelection() {
    try {
        const [isItemsSelected, vcCurrentKitConfiguration, selectedKitItemstoDisplay] = GetChildKitItemSelectionString();

        if (isItemsSelected) {
            $("[id$=hdnKitChildItems]").val(vcCurrentKitConfiguration);
            $("[id$=hdnSelectedText]").val(selectedKitItemstoDisplay);

            return true;
        } else {
            return false;
        }
    } catch (e) {
        alert("Unknown error occurred while getting selected kit values.");
        return false;
    }
}

function GetChildKitItemSelectionString() {
    var isItemsSelected = false;
    var selectedKitItemstoDisplay = "";
    var vcCurrentKitConfiguration = "";

    $("div.panelchildKit").each(function (index, kit) {
        if ($(kit).is(":visible")) {
            isItemsSelected = false;

            $(kit).find("input.kititem").each(function () {
                if ($(this).is(":checked")) {
                    vcCurrentKitConfiguration += ((vcCurrentKitConfiguration == "" ? "" : ",") + (parseInt($(kit).attr("ChildKitItemCode")) + "-" + $(this).val() + ($(this).parent().find(".txtQtyKit").length > 0 ? ("-" + parseFloat($(this).parent().find(".txtQtyKit").val()) * parseFloat($(this).parent().find(".txtQtyKit").attr("uom"))) : ("-" + parseFloat($(this).attr("qty"))))));
                    selectedKitItemstoDisplay = (selectedKitItemstoDisplay + (selectedKitItemstoDisplay == "" ? "" : ", ") + $(this).attr("itemName") + " (" + ($(this).parent().find(".txtQtyKit").length > 0 ? (parseFloat($(this).parent().find(".txtQtyKit").val()) * parseFloat($(this).parent().find(".txtQtyKit").attr("uom"))) : $(this).attr("qty")) + " " + $(this).attr("unitName") + " )");
                    isItemsSelected = true;
                }
            });

            if (!isItemsSelected && isPreventOrphanedParents) {
                alert("No items are selected for kit item: " + $(kit).find(".panel-heading a").text());
                $(kit).focus();
                return [false, "", ""];
            } else {
                isItemsSelected = true;
            }
        }
    });

    if (isItemsSelected) {
        return [true, vcCurrentKitConfiguration, selectedKitItemstoDisplay];
    } else {
        return [false, "", ""];
    }
}

function ReplaceImagePlaceHolder(displayText) {
    return displayText.replace("##URL##/", $("[id$=hdnPortalURL]").val())
}

function FocusNextKit(control) {
    $(control).find(".panel-body").collapse('hide');

    if ($(control).next("div.panelchildKit").length > 0) {
        control = $(control).next("div.panelchildKit");

        if ($(control).is(":visible") && $(control).find(".panel-heading i.selectedChilds").text() == "") {
            $(control).find(".panel-body").collapse('show');
        } else {
            FocusNextKit(control);
        }
    }
}

function AutoSelectIfOnlyOneOption() {
    $("div.panelchildKit").each(function (index, kit) {
        if ($(kit).find(".panel-heading i.selectedChilds").text() == "") {
            if ($(kit).find("input.kititem").is(':radio') && $(kit).find("input.kititem").length == 1) {
                $(kit).find("input.kititem").prop("checked", true);

                if ($(kit).find("input.kititem").closest("div.slick-item").length > 0) {
                    SlickItemSelected($(kit).find("input.kititem").closest("div.slick-item"), !$(kit).find("input.kititem").is(':radio'));
                    //$(kit).find("input.kititem").closest("div.slick-item").click();
                } else {
                    KitChildSelectionChanged($(kit).find("input.kititem"), !$(kit).find("input.kititem").is(':radio'));
                }
            }
        }
    });
}

function ResetKitConfiguration() {
    LoadChildKits(kiteItemIDBizKitsWithChildKitsJS);
}

function CreateNewItem(isAssembly, isKitParent, isNewOrder) {
    try {
        $("#divLoader").show();
        const [isItemsSelected, vcCurrentKitConfiguration, selectedKitItemstoDisplay] = GetChildKitItemSelectionString();

        if (isItemsSelected) {
            $.ajax({
                type: "POST",
                url: '../WebServices/CommonService.svc/CreateNewItemFromKitConfiguration',
                contentType: "application/json",
                dataType: "json",
                async: false,
                data: JSON.stringify({
                    "itemCode": kiteItemIDBizKitsWithChildKitsJS
                    , "vcCurrentKitConfiguration": vcCurrentKitConfiguration
                    , "isAssembly": isAssembly
                    , "isKitParent": isKitParent
                }),
                beforeSend: function () {

                },
                complete: function () {
                    $("#divLoader").hide();
                },
                success: function (data) {
                    var numItemCode = $.parseJSON(data.CreateNewItemFromKitConfigurationResult);
                    if (numItemCode != null && numItemCode > 0) {
                        if (isNewOrder) {
                            try {
                                LoadNewKitItem(numItemCode);
                            } catch (e) {
                                
                            }
                        } else {
                            document.location.href = "../Items/frmKitDetails.aspx?ItemCode=" + numItemCode.toString() + "&amp;frm=All Items";
                        }
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    alert(jqXHR.responseText);
                }
            });

            return true;
        } else {
            $("#divLoader").hide();
        }
    } catch (e) {
        $("#divLoader").hide();
        alert("Unknown error occurred while creating new item");
    } 

    return false;
}