﻿function getVerifyUSAddress(streetAddr, city, state, type, smartystreetsKey) {
    //3359218178746496
    var url = "https://us-street.api.smartystreets.com/street-address?key=" + smartystreetsKey;
    url = url +"&street="+ encodeURIComponent(streetAddr);
    url = url + "&city=" + encodeURIComponent(city);
    url = url + "&state=" + encodeURIComponent(state);
    url = url + "&candidates=10";
    $.ajax({
        url: url, success: function (result) {
            debugger;
            try {
                if (result.length > 0) {
                    if (type == "0") {
                        $("#verifyBillingAdressStatus").text("Verified");
                        $("#verifyBillingAdressStatus").addClass("text-success");
                    } else {
                        $("#verifyShippingAdressStatus").text("Verified");
                        $("#verifyShippingAdressStatus").addClass("text-success");
                    }
                } else {
                    if (type == "0") {
                        $("#verifyBillingAdressStatus").text("Not Verified");
                        $("#verifyBillingAdressStatus").addClass("text-danger");
                    } else {
                        $("#verifyShippingAdressStatus").text("Not Verified");
                        $("#verifyShippingAdressStatus").addClass("text-danger");
                    }
                }
            }
            catch{
                if (type == "0") {
                    $("#verifyBillingAdressStatus").text("Not Verified");
                    $("#verifyBillingAdressStatus").addClass("text-danger");
                } else {
                    $("#verifyShippingAdressStatus").text("Not Verified");
                    $("#verifyShippingAdressStatus").addClass("text-danger");
                }
            }
        }, error: function (result) {
            if (type == "0") {
                $("#verifyBillingAdressStatus").text("Not Verified");
                $("#verifyBillingAdressStatus").addClass("text-danger");
            } else {
                $("#verifyShippingAdressStatus").text("Not Verified");
                $("#verifyShippingAdressStatus").addClass("text-danger");
            }
        }
    });
}

function getVerifyNonUSAddress(country, streetAddr, postal_code, type, smartystreetsKey) {
    var url = "https://international-street.api.smartystreets.com/verify?key=" + smartystreetsKey;
    url = url + "&country=" + encodeURIComponent(country);
    url = url + "&address1=" + encodeURIComponent(streetAddr);
    url = url + "&postal_code=" + encodeURIComponent(postal_code);
    url = url + "&candidates=10";
    $.ajax({
        url: url, success: function (result) {
            debugger;
            try {
                if (result.length > 0) {
                    if (result[0].analysis.verification_status == "Verified") {
                        if (type == "0") {
                            $("#verifyBillingAdressStatus").text("Verified");
                            $("#verifyBillingAdressStatus").addClass("text-success");
                        } else {
                            $("#verifyShippingAdressStatus").text("Verified");
                            $("#verifyShippingAdressStatus").addClass("text-success");
                        }

                    } else {
                        if (type == "0") {
                            $("#verifyBillingAdressStatus").text("Not Verified");
                            $("#verifyBillingAdressStatus").addClass("text-danger");
                        } else {
                            $("#verifyShippingAdressStatus").text("Not Verified");
                            $("#verifyShippingAdressStatus").addClass("text-danger");
                        }

                    }
                } else {
                    if (type == "0") {
                        $("#verifyBillingAdressStatus").text("Not Verified");
                        $("#verifyBillingAdressStatus").addClass("text-danger");
                    } else {
                        $("#verifyShippingAdressStatus").text("Not Verified");
                        $("#verifyShippingAdressStatus").addClass("text-danger");
                    }
                }
            } catch{
                if (type == "0") {
                    $("#verifyBillingAdressStatus").text("Not Verified");
                    $("#verifyBillingAdressStatus").addClass("text-danger");
                } else {
                    $("#verifyShippingAdressStatus").text("Not Verified");
                    $("#verifyShippingAdressStatus").addClass("text-danger");
                }

            }
        }, error: function (result) {
            if (type == "0") {
                $("#verifyBillingAdressStatus").text("Not Verified");
                $("#verifyBillingAdressStatus").addClass("text-danger");
            } else {
                $("#verifyShippingAdressStatus").text("Not Verified");
                $("#verifyShippingAdressStatus").addClass("text-danger");
            }
        }
    });
}
function getVerifyAddress(country, streetAddr, postal_code, city, state, type, smartystreetsKey) {
    if (type == "0") {
        $("#verifyBillingAdressStatus").text("Validating...");

        $("#verifyBillingAdressStatus").removeClass("text-danger");
        $("#verifyBillingAdressStatus").removeClass("text-success");
        $("#verifyBillingAdressStatus").addClass("textDesign text-default");
    } else {
        $("#verifyShippingAdressStatus").text("Validating...");
        $("#verifyShippingAdressStatus").removeClass("text-danger");
        $("#verifyShippingAdressStatus").removeClass("text-success");
        $("#verifyShippingAdressStatus").addClass("textDesign text-default");
    }
    if (country == "United States") {
        getVerifyUSAddress(streetAddr, city, state, type, smartystreetsKey);
    } else {
        getVerifyNonUSAddress(country, streetAddr, postal_code, type, smartystreetsKey);
    }
}