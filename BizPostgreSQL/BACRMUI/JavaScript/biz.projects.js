﻿$(document).ready(function () {
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    prm.add_pageLoaded(pageLoadedProjectJS);
});

function pageLoadedProjectJS() {
    $("label.taskTimerInitial").each(function () {
        var currentTime = new Date();
        var divTaskControlsProject = $(this).closest("div[id='divTaskControlsProject']");


        if (divTaskControlsProject != null) {
            ActivateTimerProject(currentTime, divTaskControlsProject, true);
        }
    });

    $('#divTaskLogProject').on('hidden.bs.modal', function () {
        document.location.href = document.location.href;
    })
}

function ShowTaskTimeLogWindowProject(taskID) {
    $("#tblTaskTimeLogProject > tbody").html("");

    $.ajax({
        type: "POST",
        url: '../WebServices/StagePercentageDetailsTaskTimeLogService.svc/GetTimeEntries',
        contentType: "application/json",
        dataType: "json",
        data: JSON.stringify({
            "taskID": taskID,
            "bitForProject": true
        }),
        beforeSend: function () {
            $("[id$=UpdateProgressProject]").show();
        },
        complete: function () {
            $("[id$=UpdateProgressProject]").hide();
        },
        success: function (data) {
            var obj = $.parseJSON(data.GetTimeEntriesResult);

            $("#divTaskLogProject #spnTimeSpendProject").html(obj.TotalTimeSpendOnTask);
            if (obj.TotalTimeSpendOnTask === "Invalid time sequence") {
                $("#divTaskLogProject #spnTimeSpendProject").attr("class", "badge bg-red");
            }
            var records = $.parseJSON(obj.Records);

            try {
                if (records != null && records.length > 0) {
                    var html = "";

                    records.forEach(function (n) {
                        html = "<tr>";
                        html += "<td>" + replaceNull(n.vcEmployee) + "</td>";
                        html += "<td class='text-center'>" + replaceNull(n.vcAction) + "</td>";
                        html += "<td class='text-center'>" + replaceNull(n.vcActionTime) + "</td>";                                
                        html += "<td>" + replaceNull(n.vcReasonForPause) + "</td>";
                        html += "<td id='tdNotes'>" + replaceNull(n.vcNotes) + "</td>";
                        html += "<td class='text-center'>"
                        if (!JSON.parse(parseInt(n.bitTaskFinished))) {
                            html += "<input type='hidden' class='tintAction' value='" + replaceNull(n.tintAction) + "'>"
                            html += "<input type='hidden' class='dtActionTime' value='" + replaceNull(n.dtActionTime) + "'>"
                            html += "<input type='hidden' class='numReasonForPause' value='" + replaceNull(n.numReasonForPause) + "'>"
                            html += "<button class='btn btn-xs btn-info' onclick='return EditTimeEntryProject(this," + replaceNull(n.ID) + ")'><i class='fa fa-pencil'></i></button>&nbsp;"
                            html += "<button class='btn btn-xs btn-danger' onclick='return DeleteTimeEntryProject(this," + replaceNull(n.ID) + "," + replaceNull(n.numTaskID) + ")'><i class='fa fa-trash-o'></i></button>";
                        }
                        html += "</td>";
                        html += "</tr>";
                        $("#tblTaskTimeLogProject > tbody").append(html);
                    });
                } else {
                    $("#tblTaskTimeLogProject > tbody").append("<tr><td colspan='7'>Not records found</td></tr>");
                }

                $("#hdnTaskLogTaskIDProject").val(taskID);
                $("#divTaskLogProject .btn-start-again").attr("onclick","return StartTaskAgainProject(" + taskID + ")");
                $("#divTaskLogProject").modal("show");
            } catch (err) {
                $("[id$=UpdateProgressProject]").hide();
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            if (IsJsonString(jqXHR.responseText)) {
                var objError = $.parseJSON(jqXHR.responseText)
                if (objError.Message != null) {
                    alert("Error occurred: " + objError.Message);
                } else {
                    alert(objError);
                }
            } else {
                alert("Unknown error ocurred");
            }
        }
    });

    return false;
}

function EditTimeEntryProject(btn, id) {
	try {
		var tr = $(btn).closest("tr");

		if (tr != null) {
			$("#divTaskLogProject #ddlActionProject").val($(tr).find(".tintAction").val());
			var actionTIme = $(tr).find(".dtActionTime").val();


			var rdpTaskStartDateProject = $telerik.findControl(document.documentElement, "rdpTaskStartDateProject");
			var txtStartFromHoursProject = $telerik.findControl(document.documentElement, "txtStartFromHoursProject");
			var txtStartFromMinutesProject = $telerik.findControl(document.documentElement, "txtStartFromMinutesProject");

			rdpTaskStartDateProject.set_selectedDate(new Date(actionTIme));
			txtStartFromHoursProject.set_value(new Date(actionTIme).getHours());
			txtStartFromMinutesProject.set_value(new Date(actionTIme).getMinutes());

			if (new Date(actionTIme).getHours() > 12) {
			    $("#rblStartFromTimeProject_0").prop("checked", false);
			    $("#rblStartFromTimeProject_1").prop("checked", true);
			    txtStartFromHoursProject.set_value(new Date(actionTIme).getHours() - 12);
			} else {
			    $("#rblStartFromTimeProject_0").prop("checked", true);
			    $("#rblStartFromTimeProject_1").prop("checked", false);
			}

			if ($(tr).find(".tintAction").val() === "2") {
				$("#divTaskLogProject [id$=ddlAddReasonForPauseProject]").val($(tr).find(".numReasonForPause").val());
				$("#divTaskLogProject #txtAddNotesProject").val($(tr).find("#tdNotes").html());
				$("#divTaskLogProject [id$=ddlAddReasonForPauseProject]").show();
				$("#divTaskLogProject #txtAddNotesProject").show();
			} else {
				$("#divTaskLogProject [id$=ddlAddReasonForPauseProject]").val("0");
				$("#divTaskLogProject #txtAddNotesProject").val("");
				$("#divTaskLogProject [id$=ddlAddReasonForPauseProject]").hide();
				$("#divTaskLogProject #txtAddNotesProject").hide();
			}
			$('input[name="TimeEntryTypeProject"]').removeAttr('checked');
			$("input[name='TimeEntryTypeProject'][value='2']").prop('checked', true);
			TimeEntryTypeChangedProject();
			$("#btnAddTimeEntryProject").attr("onclick", "return AddTimeEntryProject(" + id + ")");
			$("#btnCancelTimeEntryProject").show();
		}
	} catch (e) {
		alert("Unknown error occurred.");
	}

	return false;
}

function TimeEntryTypeChangedProject() {
	if ($("input[name='TimeEntryTypeProject']:checked").val() == "1") {
		$("#tblTaskTimeLogProject td.tdSingleDay").show();
		$("#tblTaskTimeLogProject td.tdMultiDayProject").hide();
		$("#tblTaskTimeLogProject #btnAddTimeEntryProject").attr("class", "btn btn-sm btn-success");
		$("#tblTaskTimeLogProject #btnAddTimeEntryProject").val("Finish Task");
	} else {
		$("#tblTaskTimeLogProject td.tdSingleDay").hide();
		$("#tblTaskTimeLogProject td.tdMultiDayProject").show();
		$("#tblTaskTimeLogProject #btnAddTimeEntryProject").attr("class", "btn btn-sm btn-primary");
		$("#tblTaskTimeLogProject #btnAddTimeEntryProject").val("Save");
	}
}

function SaveTimeEntryProject(id, taskID, actionType, actionTime, processedQty, reasonForPause, notes, isManualEntry) {
	return $.ajax({
		type: "POST",
		url: '../WebServices/StagePercentageDetailsTaskTimeLogService.svc/Save',
		contentType: "application/json",
		dataType: "json",
		data: JSON.stringify({
			"id": id
			, "taskID": taskID
			, "actionType": actionType
			, "actionTime": "\/Date(" + actionTime.getTime() + ")\/"
			, "processedQty": processedQty
			, "reasonForPause": reasonForPause
			, "notes": notes
			, "isManualEntry": isManualEntry
		}),
		beforeSend: function () {
			$("[id$=UpdateProgressProject]").show();
		},
		complete: function () {
			$("[id$=UpdateProgressProject]").hide();

		}
	});
}

function AddTimeEntryProject(id) {
	try {
	    if ($("input[name='TimeEntryTypeProject']:checked").val() == "1") {
	        var rdpTaskStartDateSingleProject = $telerik.findControl(document.documentElement, "rdpTaskStartDateSingleProject");

	        if (rdpTaskStartDateSingleProject.get_selectedDate() == null) {
				alert("Select date");
				rdpTaskStartDateSingleProject.get_dateInput().focus();

				return false;
			}

	        var pickerStart = $telerik.findControl(document.documentElement, "rtpStartTimeProject");
			var viewStart = pickerStart.get_timeView();

			if (viewStart.getTime() == null) {
				alert("Select Start Time");
				return false;
			}

			var pickerEnd = $telerik.findControl(document.documentElement, "rtpEndTimeProject");
			var viewEnd = pickerEnd.get_timeView();

			if (viewEnd.getTime() == null) {
				alert("Select End Time");
				return false;
			}

			var dt = $telerik.findControl(document.documentElement, "rdpTaskStartDateSingleProject").get_selectedDate();
			var hours = parseInt(viewStart.getTime().getHours());
			var minutes = parseInt(viewStart.getTime().getMinutes());
			var dtStartTime = new Date(dt.getFullYear(), dt.getMonth(), dt.getDate(), hours, minutes, 0);

			dt = $telerik.findControl(document.documentElement, "rdpTaskStartDateSingleProject").get_selectedDate();
			hours = parseInt(viewEnd.getTime().getHours());
			minutes = parseInt(viewEnd.getTime().getMinutes());
			var dtEndTime = new Date(dt.getFullYear(), dt.getMonth(), dt.getDate(), hours, minutes, 0);

			if (dtStartTime > dtEndTime) {
				alert("End Time must be after start time.");
				return false;
			}

			var a1 = SaveTimeEntryProject(id, parseInt($("#divTaskLogProject #hdnTaskLogTaskIDProject").val()), 1, dtStartTime, 0, 0, "", true);

			$.when(a1).then(function (data) {
				var a2 = SaveTimeEntryProject(id, parseInt($("#divTaskLogProject #hdnTaskLogTaskIDProject").val()), 4, dtEndTime, 0, 0, "", true);

				$.when(a2).then(function (data) {
					document.location.href = document.location.href;
				}, function (jqXHR, textStatus, errorThrown) {
					if (IsJsonString(jqXHR.responseText)) {
						var objError = $.parseJSON(jqXHR.responseText)
						if (objError.Message != null) {
							alert("Error occurred: " + objError.Message);
						} else {
							alert(objError);
						}
					} else {
						alert("Unknown error ocurred");
					}
				});
			}, function (jqXHR, textStatus, errorThrown) {
				if (IsJsonString(jqXHR.responseText)) {
					var objError = $.parseJSON(jqXHR.responseText)
					if (objError.Message != null) {
						alert("Error occurred: " + objError.Message);
					} else {
						alert(objError);
					}
				} else {
					alert("Unknown error ocurred");
				}
			});
		} else {
	        if ($telerik.findControl(document.documentElement, "rdpTaskStartDateProject").get_selectedDate() == null) {
				alert("Select date");
				$telerik.findControl(document.documentElement, "rdpTaskStartDateProject").get_dateInput().focus();
				return false;
			}

	        if ($telerik.findControl(document.documentElement, "txtStartFromHoursProject").get_value() === "") {
				alert("Select start hour.");
				$telerik.findControl(document.documentElement, "txtStartFromHoursProject").focus();
				return false;
			}

	        if ($telerik.findControl(document.documentElement, "txtStartFromMinutesProject").get_value() === "") {
				alert("Select start minute.");
				$telerik.findControl(document.documentElement, "txtStartFromMinutesProject").focus();
				return false;
			}

	        var dt = $telerik.findControl(document.documentElement, "rdpTaskStartDateProject").get_selectedDate();
	        var hours = parseInt($telerik.findControl(document.documentElement, "txtStartFromHoursProject").get_value());
			if ($("#rblStartFromTimeProject_1").is(":checked")) {
				hours = hours + 12;
			}
			var minutes = parseInt($telerik.findControl(document.documentElement, "txtStartFromMinutesProject").get_value());
			var dtActionTime = new Date(dt.getFullYear(), dt.getMonth(), dt.getDate(), hours, minutes, 0);
			var numProcessedQty = 0;
			var reasonForPause = 0;
			var notes = "";

			if ($("#divTaskLogProject #ddlActionProject").val() === "2") {
				reasonForPause = parseInt($("#divTaskLogProject [id$=ddlAddReasonForPauseProject]").val());
				notes = $("#divTaskLogProject #txtAddNotesProject").val();
			}

			var a1 = SaveTimeEntryProject(id, parseInt($("#divTaskLogProject #hdnTaskLogTaskIDProject").val()), parseInt($("#divTaskLogProject #ddlActionProject").val()), dtActionTime, 0, reasonForPause, notes, true);

			$.when(a1).then(function (data) {
				CancelTimeEntryProject();
				ShowTaskTimeLogWindowProject(parseInt($("#divTaskLogProject #hdnTaskLogTaskIDProject").val()));
			}, function (jqXHR, textStatus, errorThrown) {
				if (IsJsonString(jqXHR.responseText)) {
					var objError = $.parseJSON(jqXHR.responseText)
					if (objError.Message != null) {
						alert("Error occurred: " + objError.Message);
					} else {
						alert(objError);
					}
				} else {
					alert("Unknown error ocurred");
				}
			});
		}
	} catch (e) {
		alert("Unknown error occurred.");
	}

	return false;
}

function DeleteTimeEntryProject(btn, id, taskID) {
	if (confirm("Time entry will be deleted. Do you want to proceed?")) {
		$.ajax({
			type: "POST",
			url: '../WebServices/StagePercentageDetailsTaskTimeLogService.svc/Delete',
			contentType: "application/json",
			dataType: "json",
			data: JSON.stringify({
				"id": id
				, "taskID": taskID
			}),
			beforeSend: function () {
				$("[id$=UpdateProgressProject]").show();
			},
			complete: function () {
				$("[id$=UpdateProgressProject]").hide();
			},
			success: function (data) {
				$(btn).closest("tr").remove();
			},
			error: function (jqXHR, textStatus, errorThrown) {
				if (IsJsonString(jqXHR.responseText)) {
					var objError = $.parseJSON(jqXHR.responseText)
					if (objError.Message != null) {
						alert("Error occurred: " + objError.Message);
					} else {
						alert(objError);
					}
				} else {
					alert("Unknown error ocurred");
				}
			}
		});
	}

	return false;
}

function ShowTaskPausedWindowProject(btn, taskID) {
	var divTaskControlsProject = $(btn).closest("div[id='divTaskControlsProject']");

	if (divTaskControlsProject != null) {
		var processedQty = 0
		$("#hdnPausedTaskIDProject").val(taskID);
		$("#divReasonForPauseProject").modal("show");
	}

	return false;
}

function TaskStartedProject(btn, taskID, isResumed) {
	var currentTime = new Date();
	var divTaskControlsProject = $(btn).closest("div[id='divTaskControlsProject']");


	if (divTaskControlsProject != null) {
		var a1 = SaveTimeEntryProject(0, taskID, (isResumed ? 3 : 1), currentTime, 0, 0, "", false);

		$.when(a1).then(function (data) {
			var html = "<ul class='list-inline' style='margin-bottom:0px;'>";
			html += "<li style='vertical-align:middle;padding-left:0px;padding-right:0px;'><button class='btn btn-xs btn-info' onclick='return ShowTaskTimeLogWindowProject(" + taskID.toString() + ");'><i class='fa fa-clock-o'></i></button></li>";
			html += "<li style='vertical-align:middle;padding-right:0px;'><button class='btn btn-xs btn-flat btn-warning' onclick='return ShowTaskPausedWindowProject(this," + taskID.toString() + ");'>Pause</button></li>";
			html += "<li style='vertical-align:middle;padding-right:0px;'><button class='btn btn-flat btn-task-finish' onclick='return TaskFinishedProject(this," + taskID.toString() + ",4);'><img src='../images/comflag.png' />&nbsp;Finish</button></li>";
			html += "<li style='vertical-align:middle;padding-right:0px;'><label class='taskTimer'></label></li>";
			html += "</ul>";

			$(divTaskControlsProject).html(html);

			ActivateTimerProject(currentTime, divTaskControlsProject, false);
		}, function (jqXHR, textStatus, errorThrown) {
			if (IsJsonString(jqXHR.responseText)) {
				var objError = $.parseJSON(jqXHR.responseText)
				if (objError.Message != null) {
					alert("Error occurred: " + objError.Message);
				} else {
					alert(objError);
				}
			} else {
				alert("Unknown error ocurred");
			}
		});
	}

	return false;
}

function TaskPausedProject() {
    var a1 = SaveTimeEntryProject(0, parseInt($("#hdnPausedTaskIDProject").val()), 2, new Date(), 0, parseInt($("[id$=ddlPauseReasonProject]").val()), $("#txtPauseNotesProject").val(), false);

	$.when(a1).then(function (data) {
		$("#radPageView_WIP > table > tbody > tr").each(function (index, e) {
			if ($(e).find("input.hdnTaskID").length > 0 && parseInt($(e).find("input.hdnTaskID").val()) === parseInt($("#hdnPausedTaskIDProject").val())) {
			    $(e).find("div[id='divTaskControlsProject']").html("<ul class='list-inline'><li style='vertical-align:middle;padding-left:0px;padding-right:0px;'><button class='btn btn-xs btn-info' onclick='return ShowTaskTimeLogWindowProject(" + $("#hdnPausedTaskIDProject").val() + ");'><i class='fa fa-clock-o'></i></button></li><li style='vertical-align:middle;padding-right:0px;'><button class='btn btn-xs btn-flat bg-purple' onclick='return TaskStartedProject(this," + $("#hdnPausedTaskIDProject").val() + ",1);'>Resume</button></li></ul>");
			}
		});

		$("[id$=ddlPauseReasonProject]").val("0");
		$("#txtPauseNotesProject").val("");
		$("#hdnPausedTaskIDProject").val("");
		$("#divReasonForPauseProject").modal("hide");
		document.location.href = document.location.href;
	}, function (jqXHR, textStatus, errorThrown) {
		if (IsJsonString(jqXHR.responseText)) {
			var objError = $.parseJSON(jqXHR.responseText)
			if (objError.Message != null) {
				alert("Error occurred: " + objError.Message);
			} else {
				alert(objError);
			}
		} else {
			alert("Unknown error ocurred");
		}
	});

	return false;
}

function TaskFinishedProject(btn, taskID, isResumed) {
	var a1 = SaveTimeEntryProject(0, taskID, 4, new Date(), 1, 0, "", false);
	$.when(a1).then(function (data) {
		document.location.href = document.location.href;
	}, function (jqXHR, textStatus, errorThrown) {
		if (IsJsonString(jqXHR.responseText)) {
			var objError = $.parseJSON(jqXHR.responseText)
			if (objError.Message != null) {
				alert("Error occurred: " + objError.Message);
			} else {
				alert(objError);
			}
		} else {
			alert("Unknown error ocurred");
		}
	});
}

function ActivateTimerProject(fromTime, divTaskControlsProject, isInitialLoad) {
	try {
		if (divTaskControlsProject != null) {
			var estimatedTimeInMinutes = 0;
			var timeSpentInMinutes = 0;

			if ($(divTaskControlsProject).closest("tr").find("input.hdnLastStartDate") != null && $(divTaskControlsProject).closest("tr").find("input.hdnLastStartDate").val() != "") {
				fromTime = new Date($(divTaskControlsProject).closest("tr").find("input.hdnLastStartDate").val());
			}

			if ($(divTaskControlsProject).closest("tr").find("input.hdnTaskEstimationInMinutes") != null) {
				estimatedTimeInMinutes = parseInt($(divTaskControlsProject).closest("tr").find("input.hdnTaskEstimationInMinutes").val());
			}

			if ($(divTaskControlsProject).closest("tr").find("input.hdnTimeSpentInMinutes") != null) {
				timeSpentInMinutes = parseInt($(divTaskControlsProject).closest("tr").find("input.hdnTimeSpentInMinutes").val() || 0);
			}


			if (estimatedTimeInMinutes > 0) {
				var countDownDate = fromTime.getTime();

				// Update the count down every 1 second
				var x = setInterval(function () {

					// Get today's date and time
					var now = new Date().getTime();

					// Find the distance between now and the count down date
					var distance = (now - countDownDate) + (timeSpentInMinutes * 60000);

					// Time calculations for days, hours, minutes and seconds
					var days = Math.floor(distance / (1000 * 60 * 60 * 24));
					var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
					var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
					var seconds = Math.floor((distance % (1000 * 60)) / 1000);

					if ($(divTaskControlsProject).find("label." + (isInitialLoad ? "taskTimerInitial" : "taskTimer")) != null) {
						$(divTaskControlsProject).find("label." + (isInitialLoad ? "taskTimerInitial" : "taskTimer")).text((days > 0 ? (days + "d ") : "") + (hours > 0 ? (hours + "h ") : "") + (minutes > 0 ? (minutes + "m ") : "") + seconds + "s ");
					}

					// If the count down is finished, write some text
					if (Math.floor((distance / 1000) / 60) > estimatedTimeInMinutes) {
						clearInterval(x);
						$(divTaskControlsProject).find("label." + (isInitialLoad ? "taskTimerInitial" : "taskTimer")).text("Late");
						$(divTaskControlsProject).find("label." + (isInitialLoad ? "taskTimerInitial" : "taskTimer")).addClass("text-red");
					}
				}, 1000);
			}
		}
	} catch (e) {

	}
}

function TimeEntryActionChangeProject(ddl) {
	if ($(ddl).val() === "2") {
		$("#divTaskLogProject [id$=ddlAddReasonForPauseProject]").show();
		$("#divTaskLogProject #txtAddNotesProject").show();
	} else {
		$("#divTaskLogProject [id$=ddlAddReasonForPauseProject]").hide();
		$("#divTaskLogProject #txtAddNotesProject").hide();
	}

	return false;
}

function CancelTimeEntryProject() {
	$("#btnCancelTimeEntryProject").hide();
	$("#btnAddTimeEntryProject").attr("onclick", "return AddTimeEntryProject(0);");
	$("#ddlActionProject").val("1");
	$("[id$=ddlAddReasonForPauseProject]").val("0");
	$("#txtAddNotesProject").val("");
	$('input[name="TimeEntryTypeProject"]').removeAttr('checked');
	$("input[name='TimeEntryTypeProject'][value='1']").prop('checked', true);
	TimeEntryTypeChangedProject();
	return false;
}

function StartTaskAgainProject(taskID) {
    if (confirm("Existing time entries will be removed and you have to start task again. Do you want to proceed?")) {
        $.ajax({
            type: "POST",
            url: '../WebServices/StagePercentageDetailsTaskTimeLogService.svc/Delete',
            contentType: "application/json",
            dataType: "json",
            data: JSON.stringify({
                "id": -1
                , "taskID": taskID
            }),
            beforeSend: function () {
                $("[id$=UpdateProgressProject]").show();
            },
            complete: function () {
                $("[id$=UpdateProgressProject]").hide();
            },
            success: function (data) {
                document.location.href = document.location.href;
            },
            error: function (jqXHR, textStatus, errorThrown) {
                if (IsJsonString(jqXHR.responseText)) {
                    var objError = $.parseJSON(jqXHR.responseText)
                    if (objError.Message != null) {
                        alert("Error occurred: " + objError.Message);
                    } else {
                        alert(objError);
                    }
                } else {
                    alert("Unknown error ocurred");
                }
            }
        });
    }

    return false;
}