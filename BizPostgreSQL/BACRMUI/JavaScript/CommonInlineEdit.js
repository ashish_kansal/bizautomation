﻿
function upDateCond() {
    $("#confirmBox").show();
   

}

$("#yes").click(function () {
    debugger
    if ($(this).attr('class') == 'yes') {
        alert("Click YES")
        

    } else {
        return false;
    }
});

function InlineEditDropDownSelectionChanged(select) {
    $(select.closest("form")).find(".inlineUpdate").click();
}

function BindInlineEdit() {
    $(".editable_select").editable("../include/SaveData.ashx", {
        loadurl: "../include/JSONSelect.ashx",
        type: "select",
        onsubmit: function (e, i) {
           
            var t = $(i).find("SELECT"),
                r = FindArrayIndex(i);
            r > -1 && "True" == arrFieldConfig[r].bitIsRequired && $(i).attr("requiredfielddropdown", arrFieldConfig[r].vcFieldMessage);
            var a = t.val();
            e.tempValue = $(t).find("[value='" + e.oldValue + "']").text();
            if (r > 0) {
                if (arrFieldConfig[r].vcFieldMessage == "Follow-up campaign") {
                    if (confirm("You are about to assign a new follow-up campaign to this contact. Any existing campaign will be replaced with this one. \n\nAre you sure you want to proceed ?"))
                        return null != $(i).attr("requiredfielddropdown") ? RequiredFieldDropDown_Inline(a, $(i).attr("requiredfielddropdown")) ? !0 : (t.css("background-color", "#c00").css("color", "#fff"), !1) : void 0
                    else
                        return false;
                }
                else if (arrFieldConfig[r].vcFieldMessage == "Deal Status") {
                    debugger;
                    if (a == "2") {
                        try {
                            $("#rbDealLost").attr("checked", true);
                            $("#rbDealWon").removeAttr("checked");
                        } catch{ }
                    } else {
                        try {
                            $("#rbDealLost").removeAttr("checked");
                            $("#rbDealWon").attr("checked", true);
                        } catch{ }
                    } 
                    if (a == "2" || a == "1") {
                        $("#openCloseDealDiv").css("display", "none");
                        $("#modalConfirmDealStatus").modal("show");
                        return false;
                    }
                }
                else
                    return null != $(i).attr("requiredfielddropdown") ? RequiredFieldDropDown_Inline(a, $(i).attr("requiredfielddropdown")) ? !0 : (t.css("background-color", "#c00").css("color", "#fff"), !1) : void 0

            } else {
                return null != $(i).attr("requiredfielddropdown") ? RequiredFieldDropDown_Inline(a, $(i).attr("requiredfielddropdown")) ? !0 : (t.css("background-color", "#c00").css("color", "#fff"), !1) : void 0
            }
        },
        onerror: function (settings, original, xhr) {
            alert(xhr.statusText);
            $(original).html(settings.tempValue);
            original.reset();
        }
    }), $(".editable_CheckBox").editable("../include/SaveData.ashx", {
        type: "CheckBox",
    }), $(".editable_textarea").editable("../include/SaveData.ashx", {
        type: "textarea",
        submitdata: {
            _method: "put"
        },
        select: !0,
        rows: "5",
        width: "100%",
        onsubmit: function (e, i) {
            var t = $(i).find("textarea"),
                r = t.val(),
                a = FindArrayIndex(i);
            return a > -1 && "True" == arrFieldConfig[a].bitIsRequired && $(i).attr("requiredfield", arrFieldConfig[a].vcFieldMessage), null != $(i).attr("requiredfield") ? RequiredField_Inline(r, $(i).attr("requiredfield")) ? !0 : (t.css("background-color", "#c00").css("color", "#fff"), !1) : void 0
        },
        onerror: function (settings, original, xhr) {
            alert(xhr.statusText);
            $(original).html(settings.tempValue);
            original.reset();
        }
    }), $(".editable_Amount").editable("../include/SaveData.ashx", {
        width: "130px",
        type: "Amount",
        onsubmit: function (e, i) {
            var t = $(i).find("input");
            e.oldValue = $.trim(t.val());
            return t.val($.trim(t.val())), validateInlineEdit(i) ? !0 : !1
        },
        onerror: function (settings, original, xhr) {
            alert(xhr.statusText);
            $(original).html(settings.oldValue);
            original.reset();
        }
    }), $(".editable_Number").editable("../include/SaveData.ashx", {
        width: "130px",
        type: "Number",
        onsubmit: function (e, i) {
            var t = $(i).find("input");
            e.oldValue = $.trim(t.val());
            return t.val($.trim(t.val())), validateInlineEdit(i) ? !0 : !1
        },
        onerror: function (settings, original, xhr) {
            alert(xhr.statusText);
            $(original).html(settings.oldValue);
            original.reset();
        }
    }), $(".editable_DateField").editable("../include/SaveData.ashx", {
        type: "DateField",
        width: "100px",
        onsubmit: function (e, i) {
            var t = $(i).find("input");
            e.oldValue = $.trim(t.val());
            return t.val($.trim(t.val())), validateInlineEdit(i) ? !0 : !1
        },
        onerror: function (settings, original, xhr) {
            alert(xhr.statusText);
            $(original).html(settings.oldValue);
            original.reset();
        }
    }), $(".click").editable("../include/SaveData.ashx", {
        width: "130px",
        onsubmit: function (e, i) {
            var t = $(i).find("input");
            return t.val($.trim(t.val())), validateInlineEdit(i) ? !0 : !1
        },
        onerror: function (settings, original, xhr) {
            if (xhr.statusText.indexOf("DUPLICATE_EMAIL") != -1) {
                var ids = original.id.split('~');
                window.open('../contact/frmDuplicateContactEmail.aspx?isFromInlineEdit=1&contactID=' + original.id.split("~")[3] + '&email=' + xhr.statusText.split("_")[2], 'ContactChangeEmail', 'toolbar=no,titlebar=no,top=200,left=200,width=800,height=400,scrollbars=no,resizable=no')
                $(original).html(settings.oldValue);
                original.reset();
            } else {
                alert(xhr.statusText);
                var ids = original.id.split('~');
                $(original).html(settings.oldValue);
                original.reset();
            }
        }
    }), $(".editable_CheckBoxList").editable("../include/SaveData.ashx", {
        loadurl: "../include/JSONSelect.ashx",
        type: "checkboxlist"
    })
}

function validateInlineEdit(e) {
    var i = $(e).find("input"),
        t = i.val(),
        r = FindArrayIndex(e);
    return r > -1 && ("True" == arrFieldConfig[r].bitIsRequired && $(e).attr("requiredfield", arrFieldConfig[r].vcFieldMessage), "True" == arrFieldConfig[r].bitIsNumeric && $(e).attr("isnumeric", arrFieldConfig[r].vcFieldMessage), "True" == arrFieldConfig[r].bitIsAlphaNumeric && $(e).attr("isalphanumeric", arrFieldConfig[r].vcFieldMessage), "True" == arrFieldConfig[r].bitIsEmail && $(e).attr("checkmail", arrFieldConfig[r].vcFieldMessage), "True" == arrFieldConfig[r].bitIsLengthValidation && ($(e).attr("rangevalidator", arrFieldConfig[r].vcFieldMessage), $(e).attr("MinValue", arrFieldConfig[r].intMinLength), $(e).attr("MaxValue", arrFieldConfig[r].intMaxLength))), (null == $(e).attr("requiredfield") || RequiredField_Inline(t, $(e).attr("requiredfield"))) && (null == $(e).attr("checkmail") || CheckMail_Inline(t, $(e).attr("checkmail"))) && (null == $(e).attr("isnumeric") || IsNumeric_Inline(t, $(e).attr("isnumeric"))) && (null == $(e).attr("isalphanumeric") || IsAlphaNumeric_Inline(t, $(e).attr("isalphanumeric"))) && (null == $(e).attr("rangevalidator") || RangeValidator_Inline(t, $(e).attr("MinValue"), $(e).attr("MaxValue"), $(e).attr("rangevalidator"))) && (null == $(e).attr("requiredfielddropdown") || RequiredFieldDropDown_Inline(t, $(e).attr("requiredfielddropdown"))) ? !0 : (i.css("background-color", "#c00").css("color", "#fff"), !1)
}

function FindArrayIndex(e) {
    for (var i = $(e).attr("id"), t = i.split("~"), r = t[1], a = t[2], n = 0; n < arrFieldConfig.length; n++)
        if (arrFieldConfig[n].numFormFieldId == r && arrFieldConfig[n].bitCustomField == a) return n;
    return -1
}

function RequiredField_Inline(e, i) {
    return 0 == trim(e).length ? (alert(i), !1) : !0
}

function RequiredFieldDropDown_Inline(e, i) {
    return "0" == e ? (alert(i), !1) : !0
}

function CheckMail_Inline(e, i) {
    return null != e && e.toString().match(/^\w+[\+\.\w-]*@([\w-]+\.)*\w+[\w-]*\.([a-z]{2,4}|\d+)$/i) ? !0 : (alert(i), !1)
}

function IsNumeric_Inline(e, i) {
    return null != e && e.toString().match(/(^\d+$)|(^\d+\.\d+$)/) ? !0 : (alert(i), !1)
}

function IsAlphaNumeric_Inline(e, i) {
    for (var t = 0; t < e.length; t++) {
        var r = e.charAt(t),
            a = r.charCodeAt(0);
        if (!(a > 47 && 58 > a || a > 64 && 91 > a || a > 96 && 123 > a)) return alert(i), !1
    }
    return !0
}

function RangeValidator_Inline(e, i, t, r) {
    var a;
    return a = parseInt(trim(e)), "" == trim(e) ? !0 : isNaN(a) && 0 == IsNumeric_Inline(e) ? (alert(r), !1) : isNaN(a) || (i = parseInt(i), t = parseInt(t), t >= a && a >= i) ? !0 : (alert(r), !1)
}

function trim(e, i) {
    return ltrim(rtrim(e, i), i)
}

function ltrim(e, i) {
    return i = i || "\\s", e.replace(new RegExp("^[" + i + "]+", "g"), "")
}

function rtrim(e, i) {
    return i = i || "\\s", e.replace(new RegExp("[" + i + "]+$", "g"), "")
}

function InlineEditValidation(e, i, t, r, a, n, l, d, u, o) {
    this.numFormFieldId = e, this.bitCustomField = i, this.bitIsRequired = t, this.bitIsNumeric = r, this.bitIsAlphaNumeric = a, this.bitIsEmail = n, this.bitIsLengthValidation = l, this.intMaxLength = d, this.intMinLength = u, this.vcFieldMessage = o
}

function CheckValidNumber(e, i) {
    var t;
    return t = document.all ? i.keyCode : i.which, (1 != e || t > 47 && 58 > t || 44 == t || 46 == t || 8 == t || 37 == t || 39 == t || 16 == t) && (2 != e || t > 47 && 58 > t || 8 == t || 37 == t || 39 == t || 16 == t) ? void 0 : (i.preventDefault ? i.preventDefault() : i.returnValue = !1, !1)
}

function validateDate(e, i) {
    isDate(e.value, i) || alert("Enter Valid Date")
}

function ValidateSelctedDate(e, i) {
    return isDate(e.value, i) ? void 0 : (e.value = e.defaultValue, alert("Enter Valid Date"), !1)
}

