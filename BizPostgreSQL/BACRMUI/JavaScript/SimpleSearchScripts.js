////////////////////////////////////////////////////////////////////////////
////////////////////SIMPLE SEARCH FIELDS SELECTION////////////////////////
////////////////////////////////////////////////////////////////////////////
/*
Purpose:	Closes the present window
Created By: Debasish Tapan Nag
Parameter:	1) None
Return		1) None
*/
function CloseThisWin()
{
	this.close();
}

/*
Purpose:	encodes the string
Created By: Debasish Tapan Nag
Parameter:	1) fBoxString: The source string
Return		1) The htmlencoded string
*/
function encodeMyHtml(fBoxString)
{
     encodedHtml = unescape(fBoxString);
     encodedHtml = encodedHtml.replace(/&/g,"\&amp;");
     encodedHtml = encodedHtml.replace(/\>/g,"&gt;");
     encodedHtml = encodedHtml.replace(/\</g,"&lt;");
     encodedHtml = encodedHtml.replace(/'/g,"\&apos;");
     encodedHtml = encodedHtml.replace(/"/g,"\&quot;");
     return encodedHtml;
}
/*
Purpose:	The processing required before the form is saved
Created By: Debasish Tapan Nag
Parameter:	1) None
Return		1) None
*/
function PreSaveSearchCustomFields(frmForm)
{
	var sXMLString = '<FormFields>'+'\n';
	//Create xml for available fields
	for(var i=0; i<frmForm.lstSelectedfld.options.length; i++)
	{
		sXMLString += '<FormField>'+'\n';
		sXMLString += '<vcDbColumnName>' + frmForm.lstSelectedfld.options[i].value + '</vcDbColumnName>'+'\n';
		sXMLString += '<vcFormFieldName>' + encodeMyHtml(frmForm.lstSelectedfld.options[i].text) + '</vcFormFieldName>'+'\n';
		sXMLString += '</FormField>'+'\n';
	}
	sXMLString += '</FormFields>'+'\n';
	frmForm.hdXMLString.value = sXMLString;
}
/*
Purpose:	To validate the form data
Created By: Debasish Tapan Nag
Parameter:	1) None
Return		1) False/ True
*/
function validateSelectedSearchFields(frmForm)
{
	if(frmForm.lstSelectedfld.options.length > 0)
	{
		PreSaveSearchCustomFields(frmForm);		
		frmForm.hdSave.value = 'True';
		return true;
	}
	alert('Please select the columns for search result of the selected group.');
	return false;
}
/*
Purpose:	To validate the form data
Created By: Debasish Tapan Nag
Parameter:	1) None
Return		1) False/ True
*/
function checkGroupSelection(frmForm)
{
	if(frmForm.ddlGroup.selectedIndex == 0)
	{
		alert('Please select the user group prior to selecting the columns.');
		return false;
	}
	return true;
}
/*
Purpose:	To move the options upwards from one position in the listbox 
			to another position in the same listbox
Created By: Debasish Tapan Nag
Parameter:	1) tbox: The target list box
Return		1) False
*/
function MoveUp(tbox)
{		
	for(var i=1; i<tbox.options.length; i++)
	{
		if(tbox.options[i].selected && tbox.options[i].value != "") 
		{		
			var SelectedText,SelectedValue,PrevSelectedValue;
			SelectedValue = tbox.options[i].value;
			SelectedText = tbox.options[i].text;
			PrevSelectedValue = tbox.options[i-1].value;
			tbox.options[i].value=tbox.options[i-1].value;
			tbox.options[i].text=tbox.options[i-1].text;
			tbox.options[i-1].value=SelectedValue;
			tbox.options[i-1].text=SelectedText;
			tbox.options[i-1].selected=true;
		}
	}
	return false;
}
/*
Purpose:	To move the options downwards from one position in the listbox 
			to another position in the same listbox
Created By: Debasish Tapan Nag
Parameter:	1) tbox: The target list box
Return		1) False
*/
function MoveDown(tbox)
{
	for(var i=0; i<tbox.options.length-1; i++)
	{
		if(tbox.options[i].selected && tbox.options[i].value != "") 
		{
			var SelectedText,SelectedValue,NextSelectedValue;
			SelectedValue = tbox.options[i].value;
			SelectedText = tbox.options[i].text;
			NextSelectedValue = tbox.options[i+1].value;
			tbox.options[i].value=tbox.options[i+1].value;
			tbox.options[i].text=tbox.options[i+1].text;
			tbox.options[i+1].value=SelectedValue;
			tbox.options[i+1].text=SelectedText;
		}
	}
	return false;
}
/*
Purpose:	Checks the number of fields selected and then disables/ enables the buttons
Created By: Debasish Tapan Nag
Parameter:	1) tbox: The target list box
Return		1) False
*/
function checkMaxFields(tbox)
{
	if(tbox.options.length >= 10)
		document.getElementById('btnAdd').disabled=true;
	else document.getElementById('btnAdd').disabled=false;
}

sortitems = 0;  // 0-False , 1-True
/*
Purpose:	To move the options from one listbox to another
Created By: Debasish Tapan Nag
Parameter:	1) fbox:The from list box
			2) tbox: The target list box
Return		1) False
*/
function move(fbox,tbox)
{
	fBoxSelectedIndex = -1;
	for(var i=0; i<fbox.options.length; i++)
	{
		if(fbox.options[i].selected && fbox.options[i].value != "") 
		{
			/// to check for duplicates 
			for (var j=0;j<tbox.options.length;j++)
			{
				if (tbox.options[j].value == fbox.options[i].value)	
				{
					alert("Item is already selected");
					return false;
				}
			}
			fBoxSelectedIndex = fbox.options[i].value;
			var no = new Option();
			no.value = fbox.options[i].value;
			no.text = fbox.options[i].text;
			tbox.options[tbox.options.length] = no;
			fbox.options[i].value = "";
			fbox.options[i].text = "";
		}
	}
	BumpUp(fbox);
	checkMaxFields(tbox);	//Calls to limit the number of fields selected in a view
	if (sortitems) SortD(tbox);
	return false;
	
}
/*
Purpose:	To remove the options from one listbox to another
Created By: Debasish Tapan Nag
Parameter:	1) fbox:The from list box
			2) tbox: The target list box
			3) aoiflag: aoi flag
Return		1) False
*/
function remove(fbox,tbox)
{
	fBoxSelectedIndex = -1;
	for(var i=0; i<fbox.options.length; i++)
	{
		if(fbox.options[i].selected && fbox.options[i].value != "") 
		{
			fBoxSelectedIndex = fbox.options[i].value;
			/// to check for duplicates 
			for (var j=0;j<tbox.options.length;j++)
			{
				if (tbox.options[j].value == fbox.options[i].value)	
				{
					fbox.options[i].value = "";
					fbox.options[i].text = "";
					BumpUp(fbox);
					if (sortitems) SortD(tbox);
					return false;
					
					
					//alert("Item is already selected");
					//return false;
				}
			}
			var no = new Option();
			no.value = fbox.options[i].value;
			if(fbox.options[i].text.substring(0,4) != '(*) ')
				no.text = fbox.options[i].text;
			else
				no.text = fbox.options[i].text.substring(4,fbox.options[i].text.length);
			tbox.options[tbox.options.length] = no;
			fbox.options[i].value = "";
			fbox.options[i].text = "";		
		}
	}
	BumpUp(fbox);
	checkMaxFields(fbox);	//Calls to limit the number of fields selected in a view
	if (sortitems) SortD(tbox);
	return false;
}
/*
Purpose:	To shift the option upwards after one is removed from the middle
Created By: Debasish Tapan Nag
Parameter:	1) box:The list box under consideration
Return		1) None
*/
function BumpUp(box) 
{
	for(var i=0; i<box.options.length; i++) 
	{
		if(box.options[i].value == "")  
		{
			for(var j=i; j<box.options.length-1; j++)  
			{
				box.options[j].value = box.options[j+1].value;
				box.options[j].text = box.options[j+1].text;
			}
			var ln = i;
			break;
		}
	}
	if(ln < box.options.length) 
	{
		box.options.length -= 1;
		BumpUp(box);
	}
}
/*
Purpose:	To sort the options in the drop down
Created By: Debasish Tapan Nag
Parameter:	1) box:The list box under consideration
Return		1) None
*/
function SortD(box) 
{
	var temp_opts = new Array();
	var temp = new Object();
	for(var i=0; i<box.options.length; i++)  
	{
		temp_opts[i] = box.options[i];
	}
	for(var x=0; x<temp_opts.length-1; x++)  
	{
		for(var y=(x+1); y<temp_opts.length; y++)  
		{
			if(temp_opts[x].text > temp_opts[y].text) 
			{
				temp = temp_opts[x].text;
				temp_opts[x].text = temp_opts[y].text;
				temp_opts[y].text = temp;
				temp = temp_opts[x].value;
				temp_opts[x].value = temp_opts[y].value;
				temp_opts[y].value = temp;
			}
		}
	}
	for(var i=0; i<box.options.length; i++)  
	{
		box.options[i].value = temp_opts[i].value;
		box.options[i].text = temp_opts[i].text;
	}
}