﻿
function RuleModuleFieldList(numFormFieldId, vcDbColumnName, vcFormFieldName, vcAssociatedControlType, vcListItemType, numListID, vcLookBackTableName, vcFieldDataType) {
    this.numFormFieldId = numFormFieldId;
    this.vcDbColumnName = vcDbColumnName;
    this.vcFormFieldName = vcFormFieldName;
    this.vcAssociatedControlType = vcAssociatedControlType;
    this.vcListItemType = vcListItemType;
    this.numListID = numListID;
    this.vcLookBackTableName = vcLookBackTableName;
    this.vcFieldDataType = vcFieldDataType;
}

var rootcondition = '<table><tr><td class="seperator" ><img src="../images/remove.png" alt="Remove" class="remove" /><select class="MainOP"><option value="and">And</option><option value="or">Or</option></select></td>';
rootcondition += '<td><div class="querystmts"></div><div class="newQuery"><img class="add" src="../images/add.png" alt="Add" /> <button class="addroot">+()</button></div>';
rootcondition += '</td></tr><tr><td class="tdAction" colspan="2"><td></tr></table>';

//Condition Statement
var statement = '<div class="condition"><img src="../images/remove.png" alt="Remove" class="remove" />'
statement += '<select class="col">$$ConditionField$$</select>'; //Field List
statement += '<div class="opControl" style="display: inline;"></div>'; //Operator List
statement += '<div class="colValueControl" style="display: inline;"></div>'; //Field Value Control Textbox,DropDown
statement += '</div>';

//Action Statement
var actionStatement = '<div class="Action"><img src="../images/remove.png" alt="Remove" class="removeAction" />'
actionStatement += '<select class="ddlAction">';
actionStatement += '<option value="0">--Select--</option>';
//actionStatement += '<option value="1">Send Email</option>';
actionStatement += '<option value="2">Tickler Action</option>';
//actionStatement += '<option value="3">Calendar Event</option>';
actionStatement += '<option value="4">Field value set</option>';
actionStatement += '<option value="5">BizDoc Approval Request</option>';
actionStatement += '</select>';
actionStatement += '<div class="ActionValueControl" style="display: inline;"><div></div>';

//Action - Field Value Set
var actFVSStatement = '<select class="ddlFVSField">$$ConditionField$$</select>';
actFVSStatement += '<div class="actFVSValue" style="display: inline;"></div>';


//Add Query Root
var addqueryroot = function (sel, isroot) {
    //$(sel).append('<hr />')
    $(sel).append(rootcondition);
    var q = $(sel).find('table');
    var l = q.length;
    var elem = q;
    if (l > 1) {
        elem = $(q[l - 1]);
    }

    // Add the default staement segment to the root condition
    var statement1 = '';
    statement1 += '<option value="0~~0">--Select--</option>';
    for (var iIndex = 0; iIndex < arrFieldConfig.length; iIndex++) {
        statement1 += '<option value="' + arrFieldConfig[iIndex].vcLookBackTableName + '.' + arrFieldConfig[iIndex].vcDbColumnName + '~' + arrFieldConfig[iIndex].vcAssociatedControlType + '~' + arrFieldConfig[iIndex].numFormFieldId + '~' + arrFieldConfig[iIndex].vcFieldDataType + '">' + arrFieldConfig[iIndex].vcFormFieldName + '</option>';
    }

    statement = statement.replace("$$ConditionField$$", statement1);

    elem.find('td >.querystmts').append(statement);

    bindQueryEvent(elem);
};

function bindQueryEvent(elem) {
    //If root element remove the close image
    //    if (isroot) {
    //        elem.find('td >.remove').detach();
    //    }
    //    else {
    elem.find('td >.remove').click(function () {
        // td>tr>tbody>table
        $(this).parent().parent().parent().parent().detach();
    });

    //}
    // Add the head class to the first statement
    //elem.find('td >.querystmts div >.remove').addClass('head');

    // Handle click for adding new statement segment
    // When a new statement is added add a condition to handle remove click.
    elem.find('td div >.add').click(function () {
        $(this).parent().siblings('.querystmts').append(statement);
        var stmts = $(this).parent().siblings('.querystmts').find('div >.remove').filter(':not(.head)');
        stmts.unbind('click');
        stmts.click(function () {
            $(this).parent().detach();
        });

        elem.find('.col').change(function () {
            //$(this).next().append($('<option></option>').val('1').html('1'));
            CreateOperatorControl(this);
        });
    });

    elem.find('div >.remove').filter(':not(.head)').click(function () {
        $(this).parent().detach();
    });

    //    elem.find('td div >.remove').click(function () {
    //        $(this).parent().detach();
    //    });

    // Handle click to add new root condition
    elem.find('td div > .addroot').click(function () {
        addqueryroot($(this).parent(), false);
        return false;
    });

    elem.find('.col').change(function () {
        //console.log($(this).prop('value'));
        CreateOperatorControl(this);
    });

    var rootTable = $('.query > table');
    rootTable.unbind('click');
    rootTable.bind('click', function (event) {
        rootTable.not(this).removeClass("selected");
        //$(this).toggleClass("selected");
        $(this).addClass("selected");
    });
}

var addAction = function (sel) {
    //$(sel).append('<hr />')
    $(sel).append(actionStatement);
    var q = $(sel).find('.Action');
    var l = q.length;
    var elem = q;
    if (l > 1) {
        elem = $(q[l - 1]);
    }

    elem.find('.removeAction').click(function () {
        $(this).parent().detach();
    });

    elem.find('.ddlAction').change(function () {
        //console.log($(this).prop('value'));
        CreateActionControl(this);
    });
};

function CreateActionControl(objField) {
    var currentId = $(objField).prop('value');

    if (currentId == 4) {
        var statement1 = '';
        statement1 += '<option value="0~~0">--Select--</option>';
        for (var iIndex = 0; iIndex < arrFieldConfig.length; iIndex++) {
            statement1 += '<option value="' + arrFieldConfig[iIndex].vcLookBackTableName + '.' + arrFieldConfig[iIndex].vcDbColumnName + '~' + arrFieldConfig[iIndex].vcAssociatedControlType + '~' + arrFieldConfig[iIndex].numFormFieldId + '~' + arrFieldConfig[iIndex].vcFieldDataType + '">' + arrFieldConfig[iIndex].vcFormFieldName + '</option>';
        }

        actFVSStatement = actFVSStatement.replace("$$ConditionField$$", statement1);

        $(objField).siblings('.ActionValueControl').empty().append(actFVSStatement);

        $(objField).parent().find('.ddlFVSField').change(function () {
            //console.log($(this).prop('value'));
            CreateActionValueControl(this);
        });
    }

    else if (currentId == 5) {
        var statement1 = '';
        $.ajax({
            type: "GET",
            async: false,
            url: "../include/AutomationRuleJSONSelect.ashx?id=U",
            success: function (result) {
                var json = result;

                var dddActBAR = '<select class="ddlActBAR">';

                for (var key in json) {
                    dddActBAR += '<option value="' + key + '">' + json[key] + '</option>';
                }

                dddActBAR += '</select>';

                $(objField).siblings('.ActionValueControl').empty().append(dddActBAR);
            }
        });
    }

    else if (currentId == 2) {
        $.ajax({
            type: "GET",
            async: false,
            url: "../include/AutomationRuleJSONSelect.ashx?id=ActionItemTemp",
            success: function (result) {
                var json = result;

                var ddlActItemTemp = '<select class="ddlActItemTemp">';

                for (var key in json) {
                    ddlActItemTemp += '<option value="' + key + '">' + json[key] + '</option>';
                }

                ddlActItemTemp += '</select>';

                $(objField).siblings('.ActionValueControl').empty().append(ddlActItemTemp);
            }
        });

        $.ajax({
            type: "GET",
            async: false,
            url: "../include/AutomationRuleJSONSelect.ashx?id=U",
            success: function (result) {
                var json = result;

                var ddlActItemTempContact = '<select class="ddlActItemTempContact">';

                for (var key in json) {
                    ddlActItemTempContact += '<option value="' + key + '">' + json[key] + '</option>';
                }

                ddlActItemTempContact += '</select>';

                $(objField).siblings('.ActionValueControl').append(ddlActItemTempContact);
            }
        });
    }
}

function CreateActionValueControl(objField) {
    var currentId = $(objField).prop('value');
    var substr = currentId.split('~');
    var vcControlType = substr[1];

    if (vcControlType == 'SelectBox') {
        $.ajax({
            type: "GET",
            async: false,
            url: "../include/AutomationRuleJSONSelect.ashx?id=" + substr[2],
            success: function (result) {
                var json = result;

                var colValueControl = '<select class="ddlactFVSValue">';

                for (var key in json) {
                    colValueControl += '<option value="' + key + '">' + json[key] + '</option>';
                }

                colValueControl += '</select>';

                $(objField).siblings('.actFVSValue').empty().append(colValueControl);
            }
        });
    }
    else if (vcControlType == 'TextBox') {
        $(objField).siblings('.actFVSValue').empty().append('<input type="text" />');
    }
}

function CreateOperatorControl(objField) {
    var currentId = $(objField).prop('value');
    var substr = currentId.split('~');
    var vcControlType = substr[1];
    var opControl = '<select class="op">';

    if (vcControlType == 'SelectBox') {
        opControl += '<option value="=">Equal To</option>';
        opControl += '<option value="!=">Not Equal To</option>';

        $.ajax({
            type: "GET",
            url: "../include/AutomationRuleJSONSelect.ashx?id=" + substr[2],
            async: false,
            success: function (result) {
                var json = result;

                var colValueControl = '<select class="colValue">';

                for (var key in json) {
                    //console.log(key);
                    //console.log(json[key]);
                    colValueControl += '<option value="' + key + '">' + json[key] + '</option>';
                    //colValueControl += $('<option />').val(key).append(json[key]);
                }

                colValueControl += '</select>';

                $(objField).siblings('.colValueControl').empty().append(colValueControl);
            }
        });
    }
    else if (vcControlType == 'TextBox') {
        if (substr[3] == 'V') {
            opControl += '<option value="=">Equal To</option>';
            opControl += '<option value="!=">Not Equal To</option>';
        }
        else if (substr[3] == 'N') {
            opControl += '<option value="=">Equal To</option>';
            opControl += '<option value="!=">Not Equal To</option>';
            opControl += '<option value="<">Less than</option>';
            opControl += '<option value=">">Greater than</option>';
        }

        $(objField).siblings('.colValueControl').empty().append('<input type="text" />');
    }

    opControl += '</select>';

    $(objField).siblings('.opControl').empty().append(opControl);
}



var GetQueryUI = function (sel, JSONobj) {
    var obj = JSONobj;

    //console.log(obj.operator);

    //    $.each(obj.expressions, function () {
    //        console.log(this.colval);
    //    });

    $(sel).append(rootcondition);
    var q = $(sel).find('table');
    var l = q.length;
    var elem = q;
    if (l > 1) {
        elem = $(q[l - 1]);
    }

    var statement1 = '';
    statement1 += '<option value="0~~0">--Select--</option>';
    for (var iIndex = 0; iIndex < arrFieldConfig.length; iIndex++) {
        statement1 += '<option value="' + arrFieldConfig[iIndex].vcLookBackTableName + '.' + arrFieldConfig[iIndex].vcDbColumnName + '~' + arrFieldConfig[iIndex].vcAssociatedControlType + '~' + arrFieldConfig[iIndex].numFormFieldId + '~' + arrFieldConfig[iIndex].vcFieldDataType + '">' + arrFieldConfig[iIndex].vcFormFieldName + '</option>';
    }

    statement = statement.replace("$$ConditionField$$", statement1);

    // Add the default staement segment to the root condition
    for (var i = 0; i < obj.expressions.length; i++)
        elem.find('td >.querystmts').append(statement);

    bindQueryEvent(elem);
    //var elem1 = $(sel).children().children().children();


    $(elem).find(".MainOP option[value='" + obj.operator + "']").prop("selected", "selected");

    var expressionelem = $(elem).find('td >.querystmts .condition');

    for (var i = 0; i < expressionelem.length; i++) {
        var col = $(expressionelem[i]).find('.col');
        col.find("option[value='" + obj.expressions[i].colval + "']").prop("selected", "selected");

        CreateOperatorControl(col);

        var op = $(expressionelem[i]).find('.op');
        op.find("option[value='" + obj.expressions[i].opval + "']").prop("selected", "selected");

        var substr = obj.expressions[i].colval.split('~');
        var vcControlType = substr[1];

        if (vcControlType == 'SelectBox') {
            var colValue = $(expressionelem[i]).find('.colValue');
            colValue.find("option[value='" + obj.expressions[i].val + "']").prop("selected", "selected");
        }
        else {
            $(expressionelem[i]).find(':text').val(obj.expressions[i].val);
        }


        //            var a = elem.find('td div >.add');
        //            $(a).parent().siblings('.querystmts').append(statement);
    }

    var Rows = $(elem).children().children();
    if (Rows.length == 2) {
        var Actionelem = $(Rows[1]).find('.tdAction');
        for (var i = 0; i < obj.actions.length; i++) {
            addAction(Actionelem);
        }

        var ActioneList = $(Actionelem).find('.Action');
        for (var i = 0; i < ActioneList.length; i++) {
            var ddlAction = $(ActioneList[i]).find('.ddlAction');
            ddlAction.find("option[value='" + obj.actions[i].Actval + "']").prop("selected", "selected");

            CreateActionControl(ddlAction);

            if (obj.actions[i].Actval == 4) {
                var ddlFVSField = $(ActioneList[i]).find('.ddlFVSField');
                ddlFVSField.find("option[value='" + obj.actions[i].ActTID + "']").prop("selected", "selected");

                CreateActionValueControl(ddlFVSField);

                var substr = obj.actions[i].ActTID.split('~');
                var vcControlType = substr[1];

                if (vcControlType == 'SelectBox') {
                    var ddlactFVSValue = $(ActioneList[i]).find('.ddlactFVSValue');
                    ddlactFVSValue.find("option[value='" + obj.actions[i].ActFValue + "']").prop("selected", "selected");
                }
                else {
                    $(ActioneList[i]).find(':text').val(obj.actions[i].ActFValue);
                }
            }
            else if (obj.actions[i].Actval == 5) {
                var ddlActBAR = $(ActioneList[i]).find('.ddlActBAR');
                ddlActBAR.find("option[value='" + obj.actions[i].ActFValue + "']").prop("selected", "selected");
            }
            else if (obj.actions[i].Actval == 2) {
                var ddlActItemTemp = $(ActioneList[i]).find('.ddlActItemTemp');
                ddlActItemTemp.find("option[value='" + obj.actions[i].ActTID + "']").prop("selected", "selected");

                var ddlActItemTempContact = $(ActioneList[i]).find('.ddlActItemTempContact');
                ddlActItemTempContact.find("option[value='" + obj.actions[i].ActFValue + "']").prop("selected", "selected");
            }
        }
    }

    //console.log(obj.nestedexpressions.length);
    if (obj.nestedexpressions.length != 0) {
        var b = elem.find('td div > .addroot');

        for (var i = 0; i < obj.nestedexpressions.length; i++) {
            GetQueryUI(b.parent(), obj.nestedexpressions[i]);

            //console.log(obj.nestedexpressions[i]);
        }
    }
    //console.log(elem);
}


//Recursive method to parse the condition and generate the query. Takes the selector for the root condition
var getCondition = function (rootsel) {
    //Get the columns from table (to find a clean way to do it later) //tbody>tr>td
    var elem = $(rootsel).children().children().children();
    //elem 0 is for operator, elem 1 is for expressions

    var q = {};
    var expressions = [];
    var nestedexpressions = [];
    var actions = [];

    var operator = $(elem[0]).find(':selected').val();
    q.operator = operator;

    // Get all the expressions in a condition
    var expressionelem = $(elem[1]).find('> .querystmts .condition');
    for (var i = 0; i < expressionelem.length; i++) {
        var col = $(expressionelem[i]).find('.col :selected');
        var op = $(expressionelem[i]).find('.op :selected');

        if (col.text() != '--Select--') {
            expressions[i] = {};

            expressions[i].colval = col.val();
            expressions[i].coldisp = col.text();
            expressions[i].opval = op.val();
            expressions[i].opdisp = op.text();

            var substr = col.val().split('~');
            var vcControlType = substr[1];

            if (vcControlType == 'SelectBox') {
                expressions[i].val = $(expressionelem[i]).find('.colValue').val();
            }
            else {
                expressions[i].val = $(expressionelem[i]).find(':text').val();
            }
        }
    }
    q.expressions = expressions;
    //console.log(expressions);

    var Rows = $(rootsel).children().children();
    if (Rows.length == 2) {
        var Actionelem = $(Rows[1]).find('.tdAction > .Action');
        //console.log(Actionelem.length);
        for (var i = 0; i < Actionelem.length; i++) {
            actions[i] = {};

            var ddlAction = $(Actionelem[i]).find('.ddlAction :selected');

            actions[i].Actval = ddlAction.val();
            actions[i].Actvaldisp = ddlAction.text();

            if (actions[i].Actval == 4) {
                var ddlFVSField = $(Actionelem[i]).find('.ddlFVSField :selected');
                var substr = ddlFVSField.val().split('~');

                actions[i].ActTID = ddlFVSField.val();

                var vcControlType = substr[1];

                if (vcControlType == 'SelectBox') {
                    actions[i].ActFValue = $(Actionelem[i]).find('.ddlactFVSValue').val();
                }
                else {
                    actions[i].ActFValue = $(Actionelem[i]).find(':text').val();
                }
            }
            else if (actions[i].Actval == 5) {
                actions[i].ActFValue = $(Actionelem[i]).find('.ddlActBAR').val();
            }
            else if (actions[i].Actval == 2) {
                actions[i].ActTID = $(Actionelem[i]).find('.ddlActItemTemp').val();

                actions[i].ActFValue = $(Actionelem[i]).find('.ddlActItemTempContact').val();
            }
            //console.log(ddlAction.val());
        }
    }

    q.actions = actions;

    // Get all the nested expressions
    if ($(elem[1]).find('> .newQuery > table').length != 0) {
        var len = $(elem[1]).find('> .newQuery > table').length;

        for (var k = 0; k < len; k++) {
            nestedexpressions[k] = getCondition($(elem[1]).find('> .newQuery > table')[k]);
        }
    }

    q.nestedexpressions = nestedexpressions;

    return q;
};

//Recursive method to iterate over the condition tree and generate the query
var getQuery = function (condition) {
    var op = [' ', condition.operator, ' '].join('');

    var e = [];
    var elen = condition.expressions.length;
    for (var i = 0; i < elen; i++) {
        var expr = condition.expressions[i];

        var substr = expr.colval.split('~');
        var vcControlType = substr[1];

        if (vcControlType == 'SelectBox' || substr[3] == 'N') {
            e.push(substr[0] + " " + expr.opval + " " + expr.val);
        }
        else {
            e.push(substr[0] + " " + expr.opval + " '" + expr.val + "'");
        }
    }

    var n = [];
    var nlen = condition.nestedexpressions.length;
    for (var k = 0; k < nlen; k++) {
        var nestexpr = condition.nestedexpressions[k];
        var result = getQuery(nestexpr);
        n.push(result);
    }

    var q = [];
    if (e.length > 0)
        q.push(e.join(op));
    if (n.length > 0)
        q.push(n.join(op));

    return ['(', q.join(op), ')'].join(' ');
};