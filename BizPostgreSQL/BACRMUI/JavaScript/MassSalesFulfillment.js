﻿var recordsPerPage = 200;
var arrFieldConfig = new Array();

$(document).ajaxStart(function () {
    $("#divLoader").show();
}).ajaxStop(function () {
    $("#divLoader").hide();
});

$(document).ready(function () {
    $("#divGridRightPick").show();
    $("#liBackOrder").show();
    $("#chkBackOrder").prop("checked","checked");

    if($("[id$=hdnDefaultShippingBizDoc]").val() == 296){
        $("#btnPackPickedItems").hide();
        $("#btnShipPackedItems").hide();
    } else {
        $("#btnPackPickedItems").show();
        $("#btnShipPackedItems").show();
    }

    $("#btnPick").click(function () {
        ResetPanes();
        $("#hdnViewID").val("1");
        $(".liPick").addClass("active");
        $(".liPackShip").removeClass("active");
        $(".liInvoice").removeClass("active");
        $(".liPay").removeClass("active");
        $(".liClose").removeClass("active");
        $("#ms-sidebar").css("border-right", "5px solid #00a65a");
        $("#chkGroupBy").prop("checked", $("#hdnGroupByOrderPick").val() === "true" ? "checked" : "");
        $("#divPackFilters").hide();
        $("#divPackSubFilters").hide();
        $("#divMainGrid").css("height","72vh");
        $("#divExpandCollpaseGroup").show();
        $("#divPickActionsRight").show();
        $("#txtScan").show();
        $("#ms-selected-grid").show();
        $("#liGroupBy").show();
        $("#liBackOrder").show();
        $("#chkBackOrder").prop("checked","checked");
        if ($("#ms-main-grid").hasClass("col-md-12")) {
            $("#ms-main-grid").removeClass("col-md-12").addClass("col-md-10");
        } else if ($("#ms-main-grid").hasClass("col-md-10")) {
            $("#ms-main-grid").removeClass("col-md-10").addClass("col-md-7");
        } else {
            $("#ms-main-grid").removeClass("col-md-9").addClass("col-md-7");
        }
        $("#divShippingRates").hide();
        //$("#divShipOrders").hide();
        $("#btnShipOrders").hide();
        $("#btnInvoiceOrders").hide();
        $("#divPayOptions").hide();
        $("#btnCloseOrders").hide();
        $("#btnCreatePackingSlip").hide();
        $("#divCloseOptions").hide();
        $("#divPrintOrderOpenClosed").hide();
        $("#divGridRightPick").show();
        $("#divGridRightPack").hide();
        $("#btnPrintPickList").show();
        $("#btnPrintBizDoc").hide();
        $("#btnPrintLabel").hide();
        $("#divPrintBizDocSubFilters").hide();
        $("[id$=hdnLastViewMode]").val("1");
        SaveLastVisitedView(1,0)
        ClearFilters();
        ClearRightPaneSelection();
        LoadRecordsWithPagination();
    });

    $("#btnPackShip").click(function () {
        ResetPanes();
        $("#hdnViewID").val("2");
        $(".liPick").removeClass("active");
        $(".liPackShip").addClass("active");
        $(".liInvoice").removeClass("active");
        $(".liPay").removeClass("active");
        $(".liClose").removeClass("active");
        $("#ms-sidebar").css("border-right", "5px solid #0073b7");
        $("#chkGroupBy").prop("checked", $("#hdnGroupByOrderShip").val() === "true" ? "checked" : "");
        $("#divPackFilters").show();
        $("#divPackFilters").css("display", "");
        $("#divPackSubFilters").hide();
        $("#divMainGrid").css("height","72vh");
        $("#divExpandCollpaseGroup").hide();
        $("#divPickActionsRight").hide();
        $("#txtScan").hide();
        $("#divGridRightPick").hide();
        $("#divGridRightPack").show();
        $("#ms-selected-grid").hide();
        $("#liGroupBy").show();
        $("#liBackOrder").show();
        $("#chkBackOrder").prop("checked","checked");
        if ($("#ms-main-grid").hasClass("col-md-9")) {
            $("#ms-main-grid").removeClass("col-md-9").addClass("col-md-12");
        } else {
            $("#ms-main-grid").removeClass("col-md-7").addClass("col-md-10");
        }
        $("#divShippingRates").show();
        //$("#divShipOrders").hide();
        $("#btnShipOrders").hide();
        $("#btnInvoiceOrders").hide();
        $("#divPayOptions").hide();
        $("#btnCloseOrders").hide();
        $("#btnCreatePackingSlip").hide();
        $("#divCloseOptions").hide();
        $("#divPrintOrderOpenClosed").hide();
        $("#divPackFilters .nav li").removeClass("active");
        $("#liPackView2").addClass("active");
        $("#btnPrintPickList").hide();
        $("#btnPrintBizDoc").hide();
        $("#btnPrintLabel").hide();
        $("#divPrintBizDocSubFilters").hide();
        ClearFilters();
        ClearRightPaneSelection();
        $("[id$=hdnLastViewMode]").val("2");
        if ($("[id$=hdnPackLastSubViewMode]").val() == "0"){
            $("[id$=hdnPackLastSubViewMode]").val("2");
        }
        SaveLastVisitedView($("[id$=hdnLastViewMode]").val(), $("[id$=hdnPackLastSubViewMode]").val());
        PackingViewChanged(parseInt($("[id$=hdnPackLastSubViewMode]").val()));
    });

    $("#btnInvoice").click(function () {
        ResetPanes();
        $("#hdnViewID").val("3");
        $(".liPick").removeClass("active");
        $(".liPackShip").removeClass("active");
        $(".liInvoice").addClass("active");
        $(".liPay").removeClass("active");
        $(".liClose").removeClass("active");
        $("#ms-sidebar").css("border-right", "5px solid #ff851b");
        $("#chkGroupBy").prop("checked", $("#hdnGroupByOrderInvoice").val() === "true" ? "checked" : "");
        $("#divPackFilters").hide();
        $("#divPackSubFilters").hide();
        $("#divMainGrid").css("height","72vh");
        $("#divExpandCollpaseGroup").hide();
        $("#divPickActionsRight").hide();
        $("#txtScan").hide();
        $("#divGridRightPick").hide();
        $("#divGridRightPack").hide();
        $("#ms-selected-grid").hide();
        $("#liGroupBy").show();
        $("#liBackOrder").hide();
        $("#chkBackOrder").prop("checked","");
        if ($("#ms-main-grid").hasClass("col-md-9")) {
            $("#ms-main-grid").removeClass("col-md-9").addClass("col-md-12");
        } else {
            $("#ms-main-grid").removeClass("col-md-7").addClass("col-md-10");
        }
        $("#divShippingRates").hide();
        //$("#divShipOrders").hide();
        $("#btnShipOrders").hide();
        $("#btnInvoiceOrders").show();
        $("#divPayOptions").hide();
        $("#btnCloseOrders").hide();
        $("#btnCreatePackingSlip").hide();
        $("#divCloseOptions").hide();
        $("#divPrintOrderOpenClosed").hide();
        $("#btnPrintPickList").hide();
        $("#btnPrintBizDoc").hide();
        $("#btnPrintLabel").hide();
        $("#divPrintBizDocSubFilters").hide();
        $("[id$=hdnLastViewMode]").val("3");
        SaveLastVisitedView(3,0)
        ClearFilters();
        ClearRightPaneSelection();
        LoadRecordsWithPagination();
    });

    $("#btnPay").click(function () {
        ResetPanes();
        $("#hdnViewID").val("4");
        $(".liPick").removeClass("active");
        $(".liPackShip").removeClass("active");
        $(".liInvoice").removeClass("active");
        $(".liPay").addClass("active");
        $(".liClose").removeClass("active");
        $("#ms-sidebar").css("border-right", "5px solid #9900cc");
        $("#chkGroupBy").prop("checked", "");
        $("#divPackFilters").hide();
        $("#divPackSubFilters").hide();
        $("#divMainGrid").css("height","72vh");
        $("#divExpandCollpaseGroup").hide();
        $("#divPickActionsRight").hide();
        $("#txtScan").hide();
        $("#divGridRightPick").hide();
        $("#divGridRightPack").hide();
        $("#ms-selected-grid").hide();
        $("#liGroupBy").hide();
        $("#liBackOrder").hide();
        $("#chkBackOrder").prop("checked","");
        if ($("#ms-main-grid").hasClass("col-md-9")) {
            $("#ms-main-grid").removeClass("col-md-9").addClass("col-md-12");
        } else {
            $("#ms-main-grid").removeClass("col-md-7").addClass("col-md-10");
        }
        $("#divShippingRates").hide();
        //$("#divShipOrders").hide();
        $("#btnShipOrders").hide();
        $("#btnInvoiceOrders").hide();
        $("#divPayOptions").show();
        $("#btnCloseOrders").hide();
        $("#btnCreatePackingSlip").hide();
        $("#divCloseOptions").hide();
        $("#divPrintOrderOpenClosed").hide();
        $("#btnPrintPickList").hide();
        $("#btnPrintBizDoc").hide();
        $("#btnPrintLabel").hide();
        $("#divPrintBizDocSubFilters").hide();
        $("[id$=hdnLastViewMode]").val("4");
        SaveLastVisitedView(4,0)
        ClearFilters();
        ClearRightPaneSelection();
        LoadRecordsWithPagination();
    });

    $("#btnClose").click(function () {
        ResetPanes();
        $("#hdnViewID").val("5");
        $(".liPick").removeClass("active");
        $(".liPackShip").removeClass("active");
        $(".liInvoice").removeClass("active");
        $(".liPay").removeClass("active");
        $(".liClose").addClass("active");
        $("#ms-sidebar").css("border-right", "5px solid #c00000");
        $("#chkGroupBy").prop("checked", "");
        $("#divPackFilters").hide();
        $("#divPackSubFilters").hide();
        $("#divMainGrid").css("height","72vh");
        $("#divExpandCollpaseGroup").hide();
        $("#divPickActionsRight").hide();
        $("#txtScan").hide();
        $("#ms-selected-grid").hide();
        $("#divGridRightPick").hide();
        $("#divGridRightPack").hide();
        $("#liGroupBy").hide();
        $("#liBackOrder").hide();
        $("#chkBackOrder").prop("checked","");
        if ($("#ms-main-grid").hasClass("col-md-9")) {
            $("#ms-main-grid").removeClass("col-md-9").addClass("col-md-12");
        } else {
            $("#ms-main-grid").removeClass("col-md-7").addClass("col-md-10");
        }
        $("#divShippingRates").hide();
        //$("#divShipOrders").hide();
        $("#btnShipOrders").hide();
        $("#btnInvoiceOrders").hide();
        $("#divPayOptions").hide();
        $("#btnCloseOrders").show();
        $("#btnCreatePackingSlip").hide();
        $("#divCloseOptions").show();
        $("#divPrintOrderOpenClosed").hide();
        $("#btnPrintPickList").hide();
        $("#btnPrintBizDoc").hide();
        $("#btnPrintLabel").hide();
        $("#divPrintBizDocSubFilters").hide();
        $("[id$=hdnLastViewMode]").val("5");
        SaveLastVisitedView(5,0)
        ClearFilters();
        ClearRightPaneSelection();
        LoadRecordsWithPagination();
    });

    $("#hplPrintBizDocs").click(function () {
        ResetPanes();
        $("#hdnViewID").val("6");
        $(".liPick").removeClass("active");
        $(".liPackShip").removeClass("active");
        $(".liInvoice").removeClass("active");
        $(".liPay").removeClass("active");
        $(".liClose").removeClass("active");
        $("#ms-sidebar").css("border-right", "5px solid #d81b60");
        $("#chkGroupBy").prop("checked", "");
        $("#divPackFilters").hide();
        $("#divPackSubFilters").hide();
        $("#divMainGrid").css("height","72vh");
        $("#divExpandCollpaseGroup").hide();
        $("#divPickActionsRight").hide();
        $("#txtScan").hide();
        $("#ms-selected-grid").hide();
        $("#divGridRightPick").hide();
        $("#divGridRightPack").hide();
        $("#liGroupBy").hide();
        $("#liBackOrder").hide();
        $("#chkBackOrder").prop("checked","");
        if ($("#ms-main-grid").hasClass("col-md-9")) {
            $("#ms-main-grid").removeClass("col-md-9").addClass("col-md-12");
        } else {
            $("#ms-main-grid").removeClass("col-md-7").addClass("col-md-10");
        }
        $("#divShippingRates").hide();
        //$("#divShipOrders").hide();
        $("#btnShipOrders").hide();
        //$("#btnShipOrders").hide();
        $("#btnInvoiceOrders").hide();
        $("#divPayOptions").hide();
        $("#btnCloseOrders").hide();
        $("#divCloseOptions").hide();
        $("#divPrintOrderOpenClosed").show();
        $("#btnPrintPickList").hide();
        $("#btnCreatePackingSlip").hide();
        $("#btnPrintBizDoc").show();
        if($("[id$=hdnDefaultShippingBizDoc]").val() == "55206"){
            $("#btnPrintLabel").show();
        } else {
            $("#btnPrintLabel").hide();
        }
        $("#divPrintBizDocSubFilters").show();
        $("#divPrintBizDocSubFilters").css("display","");
        $("#divPrintBizDocSubFilters .nav li").removeClass("active");
        $("#liPrintBizDocView1").addClass("active");
        $("#hdnPrintBizDocViewMode").val("1");
        ClearFilters();
        ClearRightPaneSelection();
        
        $("[id$=hdnLastViewMode]").val("6");
        if ($("[id$=hdnPrintLastSubViewMode]").val() == "0"){
            $("[id$=hdnPrintLastSubViewMode]").val("1");
            LoadRecordsWithPagination();
        } else {
            $("#hdnPrintBizDocViewMode").val($("[id$=hdnPrintLastSubViewMode]").val());
            PrintBizDocViewChanged(parseInt($("[id$=hdnPrintLastSubViewMode]").val()));
        }
        SaveLastVisitedView($("[id$=hdnLastViewMode]").val(), $("[id$=hdnPrintLastSubViewMode]").val());        
    });

    $.when(LoadOrderStatus(), LoadShippingZone(), LoadBatches(), LoadWarehouses(), LoadContainers(), LoadMassSalesFulfillmentConfiguration(), LoadCountries()).then(function () {
        $("#chkGroupBy").prop("checked", $("#hdnGroupByOrderPick").val() === "true" ? "checked" : "");

        if ($("[id$=hdnLastViewMode]").val() == "1") {
            $("#btnPick").click();
        } else if ($("[id$=hdnLastViewMode]").val() == "2"){
            $("#btnPackShip").click();
        } else if ($("[id$=hdnLastViewMode]").val() == "3"){
            $("#btnInvoice").click();
        } else if ($("[id$=hdnLastViewMode]").val() == "4"){
            $("#btnPay").click();
        } else if ($("[id$=hdnLastViewMode]").val() == "5"){
            $("#btnClose").click();
        } else if ($("[id$=hdnLastViewMode]").val() == "6"){
            $("#hplPrintBizDocs").click();
        } else {
            LoadRecordsWithPagination();
        }
        
    });
   
    $("#chkGroupBy").change(function () {
        LoadRecordsWithPagination();
    });

    $("#chkBackOrder").change(function () {
        LoadRecordsWithPagination();
    });

    $("#ddlPendingCloseFilter").change(function () {
        LoadRecordsWithPagination();
    });

    $("#btnmssidebar").click(function () {
        if ($("#ms-container").hasClass("ms-sidebar-expanded")) {
            $("#ms-container").removeClass("ms-sidebar-expanded").addClass("ms-sidebar-collapsed");
            $("#btnmssidebar").html("<i class='fa fa-chevron-right'></i>");

            if ($("#ms-main-grid").hasClass("col-md-10")) {
                $("#ms-main-grid").removeClass("col-md-10").addClass("col-md-12");
            } else {
                $("#ms-main-grid").removeClass("col-md-7").addClass("col-md-9");
            }
        } else {
            if ($("#ms-main-grid").hasClass("col-md-12")) {
                $("#ms-main-grid").removeClass("col-md-12").addClass("col-md-10");
            } else if ($("#ms-main-grid").hasClass("col-md-10")) {
                $("#ms-main-grid").removeClass("col-md-10").addClass("col-md-7");
            } else {
                $("#ms-main-grid").removeClass("col-md-9").addClass("col-md-7");
            }
            $("#ms-container").removeClass("ms-sidebar-collapsed").addClass("ms-sidebar-expanded");
            $("#btnmssidebar").html("<i class='fa fa-chevron-left'></i>");
        }
    });

    $("#btncollapsemaingrid").click(function () {
        $("#ms-container").removeClass("ms-sidebar-expanded").addClass("ms-sidebar-collapsed");
        $("#btnmssidebar").html("<i class='fa fa-chevron-right'></i>");
        $("#ms-main-grid").removeClass("col-md-12").removeClass("col-md-10").addClass("col-md-7");

        $("#ms-container").removeClass("ms-maingrid-expanded").addClass("ms-maingrid-collapsed");
        $("#ms-container").removeClass("ms-selectedgrid-collapsed").addClass("ms-selectedgrid-expanded");
        $("#ms-selected-grid").removeClass("col-md-3").addClass("col-md-12");
    });

    $("#btnexpandbothgridright").click(function () {
        ResetPanes();
    });

    $("#btncollapseselectedgrid").click(function () {
        if ($("#ms-main-grid").hasClass("col-md-9")) {
            $("#ms-main-grid").removeClass("col-md-9").addClass("col-md-12");
        } else {
            $("#ms-main-grid").removeClass("col-md-7").addClass("col-md-10");
        }

        $("#ms-container").removeClass("ms-sidebar-collapsed").addClass("ms-sidebar-expanded");
        $("#btnmssidebar").html("<i class='fa fa-chevron-left'></i>");
        $("#ms-container").removeClass("ms-maingrid-collapsed").addClass("ms-maingrid-expanded");
        $("#ms-container").removeClass("ms-selectedgrid-expanded").addClass("ms-selectedgrid-collapsed");
        $("#ms-selected-grid").attr("class","col-sm-12 col-md-3");
    });

    $("#btnexpandbothgrid").click(function () {
        ResetPanes();
    });

    $("#btncolumnconfig").click(function () {
        var h = screen.height;
        var w = screen.width;

        window.open('../opportunity/frmMassSalesFulfillmentConfig.aspx', '', 'toolbar=no,titlebar=no,top=50,left=50,width=' + (w - 100) + ',height=' + (h - 200) + ',scrollbars=yes,resizable=yes');
        return false;
    });

    $("#ddlFiterBy").change(function () {
        var filterBy = parseInt($(this).val());
        $("#divFilterValueOptions").html("");
        $("#dropdownMenuFilter").text("None Selected");

        if ($(this).val() !== "0") {
            $.ajax({
                type: "POST",
                url: '../opportunity/MassSalesFulfillmentService.svc/GetFilterValues',
                contentType: "application/json",
                dataType: "json",
                data: JSON.stringify({
                    "filterBy": filterBy
                }),
                success: function (data) {
                    try {
                        var obj = $.parseJSON(data.GetFilterValuesResult);
                        if (obj != null && obj.length > 0) {
                            if (filterBy === 1) {
                                var strFilterValues = "";

                                obj.forEach(function (e) {
                                    strFilterValues += "<div class='checkbox' onchange='filterSelectionChanged();'><input type='checkbox' id='chkFilterValue" + e.numListItemID.toString() + "' /><label>" + replaceNull(e.vcData) + "</label></div>";
                                });

                                $("#divFilterValueOptions").html(strFilterValues);
                            }
                        }
                    } catch (err) {
                        alert("Unknown error occurred.");
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    if(IsJsonString(jqXHR.responseText)){
                        var objError = $.parseJSON(jqXHR.responseText)
                        alert("Error occurred: " + replaceNull(objError.ErrorMessage));
                    } else {
                        alert("Unknown error ocurred");
                    }
                }
            });
        }
    });

    $("#btnApplyFilters").click(function () {
        LoadRecordsWithPagination();
    });

    $("#btnClearFilters").click(function () {
        ClearFilters();
        PersistHeaderSearchFilters();
        LoadRecordsWithPagination();
    });

    $("#btnRemoveFromBatch").click(function () {
        try {
            var selectedRecords = "";

            $("#divMainGrid").find(".chkSelect").each(function () {
                if ($(this).is(":checked")) {
                    selectedRecords = selectedRecords + (selectedRecords.length > 0 ? "," : "") + ($(this).closest("tr").find("#hdnOppID")[0].value + "-" + $(this).closest("tr").find("#hdnOppItemID")[0].value)
                }
            });

            if (selectedRecords.length > 0) {
                if (parseInt($("#hdnBatchID").val()) > 0) {
                    $.ajax({
                        type: "POST",
                        url: '../opportunity/MassSalesFulfillmentService.svc/RemoveRecordsFromBatch',
                        contentType: "application/json",
                        dataType: "json",
                        data: JSON.stringify({
                            "batchID": parseInt($("#hdnBatchID").val())
                            , "selectedRecords": selectedRecords
                        }),
                        success: function (data) {
                            alert("Rrecords are removed successfully from selected batch.");
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            if(IsJsonString(jqXHR.responseText)){
                                var objError = $.parseJSON(jqXHR.responseText)
                                alert("Error occurred: " + replaceNull(objError.ErrorMessage));
                            } else {
                                alert("Unknown error ocurred");
                            }
                        }
                    });
                } else {
                    alert("Select Batch");
                }
            } else {
                alert("Select records.");
            }
        } catch (e) {
            alert("Unknown error occured while removing record(s) from batch")
        }
    });

    $("#ddlBatchRight").change(function () {
        try {
            $("#hdnSelectedRecordsForRightPanePicking").val("");

            if (parseInt($("#ddlBatchRight").val()) > 0) {
                LoadSelectedRecordsToRightPaneForPicking();
            } else {
                $("#tblRight").html("");
            }
        } catch (e) {
            alert("Unknown error occurred while loading records of selected batch");
        }
    });

    $("#btnPrintPickList").click(function(){
        if($('#tblRight > tbody > tr').length > 0){
            try {
                var oppItems = [];
                $('#tblRight > tbody > tr').each(function(){
                    if(parseInt($(this).find("#hdnOppChildItemID").val()) == 0 && parseInt($(this).find("#hdnOppKitChildItemID").val()) == 0){
                        var objItem = {};
                        objItem.numOppID = $(this).find("#hdnOppID").val();
                        objItem.vcOppName = $(this).find("#hdnOppName").val();
                        objItem.vcImage = $(this).find("#hdnItemImage").val();
                        objItem.vcItemName = $(this).find("#hdnItemName").val();
                        objItem.vcSKU = $(this).find("#hdnSKU").val();
                        objItem.vcUPC = $(this).find("#hdnUPC").val();
                        objItem.vcAttributes = $(this).find("#hdnAttributes").val();
                        objItem.vcInclusionDetail = $(this).find("#hdnInclusionDetail").val();
                        objItem.vcLocation = $(this).find("#hdnLocation").val();
                        objItem.numUnitHour = $(this).find("#hdnRemainingQty").val();

                        oppItems.push(objItem);
                    }
                });


                return $.ajax({
                    type: "POST",
                    url: '../opportunity/MassSalesFulfillmentService.svc/PrintPickList',
                    contentType: "application/json",
                    dataType: "json",
                    data: JSON.stringify({
                        "isPickListByOrder":JSON.parse($("#hdnPickListByOrder").val())
                        ,"batchName": (parseInt($("#ddlBatchRight")) > 0 ? $("#ddlBatchRight").text() : "")
                        ,"oppItems": JSON.stringify(oppItems)
                    }),
                    success: function (data) {
                        try {
                            var sampleArr = base64ToArrayBuffer(data.PrintPickListResult);

                            now = new Date();
                            year = "" + now.getFullYear();
                            month = "" + (now.getMonth() + 1); if (month.length == 1) { month = "0" + month; }
                            day = "" + now.getDate(); if (day.length == 1) { day = "0" + day; }
                            hour = "" + now.getHours(); if (hour.length == 1) { hour = "0" + hour; }
                            minute = "" + now.getMinutes(); if (minute.length == 1) { minute = "0" + minute; }
                            second = "" + now.getSeconds(); if (second.length == 1) { second = "0" + second; }

                            saveByteArray("PickList" + year + month + day + hour + minute + second, sampleArr);
                        } catch (err) {
                            alert("Unknown error occurred while printing pick list.")
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        if(IsJsonString(jqXHR.responseText)){
                            var objError = $.parseJSON(jqXHR.responseText)
                            alert("Error occurred: " + replaceNull(objError.ErrorMessage));
                        } else {
                            alert("Unknown error ocurred");
                        }
                    }
                });                       
            } catch (e) {
                alert("Unknown error occurred while printing pick list.")
            }
        } else {
            alert("No records loaded in right pane to pick");
        }
    });

    $("#btnPrintBizDoc").click(function(){
        try {
            var selectdRecords = [];

            $("#divMainGrid > table > tbody > tr").not(":first").each(function (index, e) {
                if($(e).find(".chkSelect").is(":checked")) {
                    var obj = {};
                    obj.OppID= parseInt($(e).find("#hdnOppID").val());
                    obj.OppBizDocID = parseInt($(e).find("#hdnOppBizDocID").val())
                    obj.BizDocName = $(e).find("#hdnBizDocName").val()
                    selectdRecords.push(obj);
                }
            });

            if (selectdRecords.length > 0) {
                PrintBizDoc(selectdRecords);
            } else {
                alert("Select bizdoc(s).")
            }
        } catch (e) {
            alert("Unknown error occurred while printing bizdoc(s).")
        }
    });

    $("#btnPrintLabel").click(function(){
        try {
            var selectdRecords = [];

            $("#divMainGrid > table > tbody > tr").not(":first").each(function (index, e) {
                if($(e).find(".chkSelect").is(":checked")) {
                    var obj = {};
                    obj.OppID= parseInt($(e).find("#hdnOppID").val());
                    obj.OppBizDocID = parseInt($(e).find("#hdnOppBizDocID").val())
                    obj.IsSucess = false;
                    obj.ErrorMessage = "";
                    selectdRecords.push(obj);
                }
            });

            if(selectdRecords.length > 0){
                $.ajax({
                    type: "POST",
                    url: '../opportunity/MassSalesFulfillmentService.svc/PrintShippingLabel',
                    contentType: "application/json",
                    dataType: "json",
                    data: JSON.stringify({
                        "warehouseID": $("#ddlWarehouse").val()
                        ,"selectdRecords": JSON.stringify(selectdRecords)
                    }),
                    success: function (data) {
                        try {
                            var obj = $.parseJSON(data.PrintShippingLabelResult);
                                          
                            var foundRecordsWithError = $.map(obj, function(e) {
                                if(!e.IsSucess) 
                                    return e;
                            });

                            if(foundRecordsWithError.length > 0){
                                alert("Error occurred in printing shipping label(s) of some order(s). please review records and try again.");

                                obj.forEach(function (lineItem) {
                                    if(lineItem.IsSucess){
                                        $("#divMainGrid > table > tbody > tr").not(":first").each(function (index, e) {
                                            if(lineItem.OppID == $(e).find("#hdnOppID").val() && lineItem.OppBizDocID == $(e).find("#hdnOppBizDocID").val()){
                                                $(e).remove();
                                            }
                                        });
                                    } else {
                                        $("#divMainGrid > table > tbody > tr").not(":first").each(function (index, e) {
                                            if(lineItem.OppID == $(e).find("#hdnOppID").val() && lineItem.OppBizDocID == $(e).find("#hdnOppBizDocID").val()){
                                                $(e).css("background-color","#ffdada");
                                                $(this).find(".chkSelect").closest("td").find(".ship-orders").remove();
                                                $(this).find(".chkSelect").closest("td").append("<i class='fa fa-exclamation-triangle ship-orders' style='color:red' data-toggle='tooltip' title='" + replaceNull(lineItem.ErrorMessage) + "' aria-hidden='true'></i>");
                                            }
                                        });
                                    }
                                });
                            } else {
                                alert("Shipping label(s) printed successfully.");
                            }
                        } catch (err) {
                            alert("Unknown error occurred.");
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        if(IsJsonString(jqXHR.responseText)){
                            var objError = $.parseJSON(jqXHR.responseText)
                            alert("Error occurred: " + replaceNull(objError.ErrorMessage));
                        } else {
                            alert("Unknown error ocurred");
                        }   
                    }
                });            
            } else {
                alert("Select bizdoc(s).")
            }
        } catch (e) {
            alert("Unknown error occurred while printing bizdoc(s).")
        }
    });

    $("#btnPickRight").click(function(){
        try {
            var isValidLocationPick;
            var isValidLot;
            var isSerial;
            var isLot;
            var serialLotQty;

            if($('#tblRight > tbody > tr').length > 0) {
                var itemsPicked = [];

                var i = 1;
                var isDataValid = true;
                $('#tblRight > tbody > tr').each(function(){
                    var pickedQty = 0;
                    var warehouseItemIDs = "";
                    isSerial = JSON.parse($(this).find("#hdnIsSerial").val());
                    isLot = JSON.parse($(this).find("#hdnIsLot").val());

                    if($(this).find(".tblPickLocation").length > 0){
                        isValidLocationPick = true;
                        isValidLot = true;
                        serialLotQty = 0;

                        if(isLot){
                            isValidLot = IsValidSerialLot($(this).find(".txtPicked"));
                        }

                        $(this).find(".tblPickLocation").find("tr").each(function(){
                            if(isSerial || isLot){
                                if($(this).find(".txtPicked").val().trim() != "0"){
                                    serialLotQty = GetSerialLotQty($(this).find(".txtPicked"),isSerial,isLot);
                                    pickedQty += serialLotQty;
                                    warehouseItemIDs += ("<Table1><ID>" + (parseInt($(this).find(".hdnWarehouseItemID").val())) + "</ID><Qty>" + serialLotQty + "</Qty><SerialLotNo>" + $(this).find(".txtPicked").val() + "</SerialLotNo></Table1>");
                                }
                            } else {
                                if(parseFloat($(this).find(".txtPicked").val()) > 0){
                                    if(parseFloat($(this).find(".txtPicked").val()) > parseFloat($(this).find(".hdnAvailable").val())){
                                        if($("#hdnPickWithoutInventoryCheck").val() === "true"){
                                            if(parseInt($(this).find(".hdnWarehouseItemID").val()) > 0 && parseInt($(this).find(".hdnWarehouseLocationID").val()) === 0){
                                                pickedQty +=  parseFloat($(this).find(".txtPicked").val());
                                                warehouseItemIDs += ("<Table1><ID>" + parseInt($(this).find(".hdnWarehouseItemID").val()) + "</ID><Qty>" + parseFloat($(this).find(".txtPicked").val()) + "</Qty><SerialLotNo></SerialLotNo></Table1>");
                                            } else {
                                                isValidLocationPick = false;
                                            }
                                        } else {
                                            isValidLocationPick = false;
                                        }
                                    } else {
                                        pickedQty +=  parseFloat($(this).find(".txtPicked").val());
                                        warehouseItemIDs += ("<Table1><ID>" + parseInt($(this).find(".hdnWarehouseItemID").val()) + "</ID><Qty>" + parseFloat($(this).find(".txtPicked").val()) + "</Qty><SerialLotNo></SerialLotNo></Table1>");
                                    }
                                }
                            }
                        });

                        if(pickedQty > 0){
                            if(pickedQty <= parseFloat($(this).find("#hdnRemainingQty").val()) && isValidLocationPick && isValidLot){
                                var objItem = {};
                                objItem.numPickedQty = pickedQty;
                                objItem.WarehouseItemIDs = "<NewDataSet>" + warehouseItemIDs + "</NewDataSet>";
                                objItem.vcOppItems = $(this).find("#hdnOppItemID").val();
                                objItem.numOppChildItemID = parseFloat( $(this).find("#hdnOppChildItemID").val());
                                objItem.numOppKitChildItemID = parseFloat( $(this).find("#hdnOppKitChildItemID").val());
                                itemsPicked.push(objItem);
                            } else if (!isValidLot){
                                isDataValid = false;                                    
                                return false;
                            } else if (!isValidLocationPick){
                                isDataValid = false;                                    
                                return false;
                            } else {
                                isDataValid = false;                                    
                                return false;
                            }
                        } else if (!isValidLot){
                            isDataValid = false;                                    
                            return false;
                        } else if (!isValidLocationPick){
                            isDataValid = false;                                    
                            return false;
                        }
                    }      
                });

                if(isDataValid){
                    if(itemsPicked.length > 0){
                        return $.ajax({
                            type: "POST",
                            url: '../opportunity/MassSalesFulfillmentService.svc/UpdateItemsPickedQty',
                            contentType: "application/json",
                            dataType: "json",
                            data: JSON.stringify({
                                "oppItems": JSON.stringify(itemsPicked)
                            }),
                            success: function (data) {
                                ClearRightPaneSelection();
                                LoadRecordsWithPagination();

                                var objPickLists = $.parseJSON(data.UpdateItemsPickedQtyResult);

                                if (objPickLists != null) {
                                    var bizDocsToPrint = [];

                                    objPickLists.forEach(function (lineItem) {
                                        var arrPickLists = lineItem.vcpicklists.split(",");

                                        $.each(arrPickLists, function (i) {
                                            var obj = {};
                                            obj.OppID = parseInt(arrPickLists[i].split("-")[0]);
                                            obj.OppBizDocID = parseInt(arrPickLists[i].split("-")[1])
                                            obj.BizDocName = ""
                                            bizDocsToPrint.push(obj);
                                        });
                                    });

                                    if (bizDocsToPrint.length > 0) {
                                        $("#modal-confirmation #hdnBizDocIDs").val(JSON.stringify(bizDocsToPrint));
                                        $("#modal-confirmation .modal-body").html("Pick list(s) added successfully. <br/> Corresponding pick lists have been added to orders.");
                                        $("#btnSaveConfirmation").text("Print Pick List(s)")
                                        $("#modal-confirmation").modal("show");
                                    }
                                } else {
                                    alert("Item(s) picked quantity updated successfully.");
                                }                               
                            },
                            error: function (jqXHR, textStatus, errorThrown) {
                                if(IsJsonString(jqXHR.responseText)){
                                    var objError = $.parseJSON(jqXHR.responseText)

                                    if (objError.ErrorMessage === "PICKED_QTY_IS_GREATER_THEN_REMAINING_QTY") {
                                        alert("Qty picked for some row(s) is greater than quantity left to be picked. Items may be updated by other user(s). Records will be loaded again to reflect changes done by other user(s).");
                                        LoadSelectedRecordsToRightPaneForPicking();
                                    } else {
                                        alert(objError.ErrorMessage);
                                    }
                                } else {
                                    alert("Unknown error ocurred");
                                }                                
                            }
                        });   
                    } else {
                        alert("No record found with picked quantity.");
                    }
                } else {
                    if(!isValidLocationPick){
                        alert("Quantity picked is more than available quantity for row number " + i);
                    } else if(!isValidLot){
                        alert("Lot number are not provided in validate format for row number " + i);
                    } else {
                        alert("Quantity picked is more than remaining quantity for row number " + i);
                    }
                    return false;
                }
            } else {
                alert("No records loaded in right pane to pick");
            }
        } catch (e) {
            alert("Unknown error occurred while marking item(s) as picked.");
        }
                
    });

    $("#btnCommitShippingRate").click(function(){
        try {
            var selectdRecords = [];

            $("#divMainGrid > table > tbody > tr").not(":first").each(function (index, e) {
                if($(e).find(".chkSelect").is(":checked") && $(e).find("#hdnIsShipRateFetched").val() === "true") {
                    var obj = {};
                    obj.OppID= parseInt($(e).find("#hdnOppID").val());
                    obj.WarehouseID= parseInt($("#ddlWarehouse").val());
                    obj.ShipByDate= $(e).find("#hdnShipByDate").val();
                    obj.ShipRate = parseFloat($(e).find("#hdnShipRate").val());
                    obj.Description = $(e).find("#hdnShipDescription").val();

                    selectdRecords.push(obj);
                }
            });

            if(selectdRecords.length > 0){
                $.ajax({
                    type: "POST",
                    url: '../opportunity/MassSalesFulfillmentService.svc/UpdateShippingRate',
                    contentType: "application/json",
                    dataType: "json",
                    data: JSON.stringify({
                        "orders": JSON.stringify(selectdRecords)
                    }),
                    success: function (data) {
                        try {
                            var obj = $.parseJSON(data.UpdateShippingRateResult);
                                          
                            if(obj.isSuccess){
                                ClearFilters();
                                ClearRightPaneSelection();
                                PackingViewChanged(2);
                            } else {
                                alert("Error occurred while adding ship rate some order(s).");
                                PackingViewChanged(1);
                            }

                        } catch (err) {
                            alert("Unknown error occurred.");
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        if(IsJsonString(jqXHR.responseText)){
                            var objError = $.parseJSON(jqXHR.responseText)

                            if (objError.ErrorMessage === "SHIPPING_ITEM_NOT_SET") {
                                alert("Go global settings -> Accounting and select Default Item used for Shipping within Orders.");
                            } else {
                                alert("Error occurred: " + replaceNull(objError.ErrorMessage));
                            }
                        } else {
                            alert("Unknown error ocurred");
                        }
                    }
                });
            } else {
                alert("No records available with ship rate.")
            }
        } catch (e) {
            alert("Unkown error occured while adding ship rate to order(s).");
        }
    });

    $("#btnShipOrders").click(function(){
        try {
            var selectdRecords = [];

            $("#divMainGrid > table > tbody > tr").not(":first").each(function (index, e) {
                if($(e).find(".chkSelect").is(":checked")) {
                    var found = $.map(selectdRecords, function(obj) {
                        if(obj.OppID === parseInt($(e).find("#hdnOppID").val()) && obj.ParentBizDocID === parseInt($(e).find("[id*=hdnParentBizDoc]").val())) 
                            return obj;
                    });

                    if(found.length === 0) {
                        var obj = {};
                        obj.OppID= parseInt($(e).find("#hdnOppID").val());
                        obj.ParentBizDocID = parseInt($(e).find("[id*=hdnParentBizDoc]").val())
                        if(parseInt($(e).find("#hdnOppItemID").val())> 0 ){
                            if($(e).find("#txtRemainingQty").length > 0){
                                obj.OppItemIDs = parseInt($(e).find("#hdnOppItemID").val()).toString() + "-" +  parseFloat($(e).find("#txtRemainingQty").val());
                            } else {
                                obj.OppItemIDs = parseInt($(e).find("#hdnOppItemID").val()).toString() + "-" +  parseFloat($(e).find("#hdnRemainingQty").val());
                            }
                        } else {
                            obj.OppItemIDs = "";
                        }
                        obj.IsSucess = false;
                        obj.ErrorMessage = "";
                        selectdRecords.push(obj);
                    } else {
                        if(parseInt($(e).find("#hdnOppItemID").val())> 0 ){
                            if($(e).find("#txtRemainingQty").length > 0){
                                found[0].OppItemIDs = found[0].OppItemIDs + "," + (parseInt($(e).find("#hdnOppItemID").val()).toString() + "-" +  parseFloat($(e).find("#txtRemainingQty").val()));
                            } else {
                                found[0].OppItemIDs = found[0].OppItemIDs + "," + (parseInt($(e).find("#hdnOppItemID").val()).toString() + "-" +  parseFloat($(e).find("#hdnRemainingQty").val()));
                            }
                        }
                    }
                }
            });

            if(selectdRecords.length > 0){
                if(confirm("You're about to ship all rows selected, are you sure you want to do this?")){
                    $.ajax({
                        type: "POST",
                        url: '../opportunity/MassSalesFulfillmentService.svc/ShipOrders',
                        contentType: "application/json",
                        dataType: "json",
                        data: JSON.stringify({
                            "orders": JSON.stringify(selectdRecords)
                        }),
                        success: function (data) {
                            try {
                                var obj = $.parseJSON(data.ShipOrdersResult);
                                          
                                var foundRecordsWithError = $.map(obj, function(e) {
                                    if(!e.IsSucess) 
                                        return e;
                                });

                                if(foundRecordsWithError.length > 0){
                                    alert("Error occurred in fulfillment of some order(s). please review records and try again.");

                                    obj.forEach(function (lineItem) {
                                        if(lineItem.IsSucess){
                                            $("#divMainGrid > table > tbody > tr").not(":first").each(function (index, e) {
                                                if(lineItem.OppID == $(e).find("#hdnOppID").val()){
                                                    $(e).remove();
                                                }
                                            });
                                        } else {
                                            $("#divMainGrid > table > tbody > tr").not(":first").each(function (index, e) {
                                                if(lineItem.OppID == $(e).find("#hdnOppID").val()){
                                                    $(e).css("background-color","#ffdada");
                                                    $(this).find(".chkSelect").closest("td").find(".ship-orders").remove();
                                                    $(this).find(".chkSelect").closest("td").append("<i class='fa fa-exclamation-triangle ship-orders' style='color:red' data-toggle='tooltip' title='" + replaceNull(lineItem.ErrorMessage) + "' aria-hidden='true'></i>");
                                                }
                                            });
                                        }
                                    });
                                } else {
                                    alert("Selected record(s) shipped successfully.");
                                    ClearFilters();
                                    ClearRightPaneSelection();
                                    PackingViewChanged(2);
                                }

                            } catch (err) {
                                alert("Unknown error occurred.");
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            if(IsJsonString(jqXHR.responseText)){
                                var objError = $.parseJSON(jqXHR.responseText)
                                alert("Error occurred: " + replaceNull(objError.ErrorMessage));
                            } else {
                                alert("Unknown error ocurred");
                            }   
                        }
                    });
                }
            } else {
                alert("Select records.")
            }
        } catch (e) {
            alert("Unknown error occurred while ship orders.");
        }
    });

    $("#btnInvoiceOrders").click(function(){
        try {
            var selectdRecords = [];

            $("#divMainGrid > table > tbody > tr").not(":first").each(function (index, e) {
                if($(e).find(".chkSelect").is(":checked")) {
                    var found = $.map(selectdRecords, function(obj) {
                        if(obj.OppID === parseInt($(e).find("#hdnOppID").val()) && obj.ParentBizDocID === parseInt($(e).find("[id*=hdnParentBizDoc]").val())) 
                            return obj;
                    });

                    if(found.length === 0) {
                        var obj = {};
                        obj.OppID= parseInt($(e).find("#hdnOppID").val());
                        obj.ParentBizDocID = parseInt($(e).find("[id*=hdnParentBizDoc]").val())
                        if(parseInt($(e).find("#hdnOppItemID").val())> 0 ){
                            if($(e).find("#txtRemainingQty").length > 0){
                                obj.OppItemIDs = parseInt($(e).find("#hdnOppItemID").val()).toString() + "-" +  parseFloat($(e).find("#txtRemainingQty").val());
                            } else {
                                obj.OppItemIDs = parseInt($(e).find("#hdnOppItemID").val()).toString() + "-" +  parseFloat($(e).find("#hdnRemainingQty").val());
                            }
                        } else {
                            obj.OppItemIDs = "";
                        }
                        obj.IsSucess = false;
                        obj.ErrorMessage = "";
                        selectdRecords.push(obj);
                    } else {
                        if(parseInt($(e).find("#hdnOppItemID").val())> 0 ){
                            if($(e).find("#txtRemainingQty").length > 0){
                                found[0].OppItemIDs = found[0].OppItemIDs + "," + (parseInt($(e).find("#hdnOppItemID").val()).toString() + "-" +  parseFloat($(e).find("#txtRemainingQty").val()));
                            } else {
                                found[0].OppItemIDs = found[0].OppItemIDs + "," + (parseInt($(e).find("#hdnOppItemID").val()).toString() + "-" +  parseFloat($(e).find("#hdnRemainingQty").val()));
                            }
                        }
                    }
                }
            });

            if(selectdRecords.length > 0){
                if(confirm("You're about to invoice all rows selected, are you sure you want to do this?")){
                    $.ajax({
                        type: "POST",
                        url: '../opportunity/MassSalesFulfillmentService.svc/InvoiceOrders',
                        contentType: "application/json",
                        dataType: "json",
                        data: JSON.stringify({
                            "orders": JSON.stringify(selectdRecords)
                        }),
                        success: function (data) {
                            try {
                                var obj = $.parseJSON(data.InvoiceOrdersResult);
                                          
                                var foundRecordsWithError = $.map(obj, function(e) {
                                    if(!e.IsSucess) 
                                        return e;
                                });

                                if(foundRecordsWithError.length > 0){
                                    alert("Error occurred in invoicing of some order(s). please review records and try again.");

                                    obj.forEach(function (lineItem) {
                                        if(lineItem.IsSucess){
                                            $("#divMainGrid > table > tbody > tr").not(":first").each(function (index, e) {
                                                if(lineItem.OppID == $(e).find("#hdnOppID").val()){
                                                    $(e).remove();
                                                }
                                            });
                                        } else {
                                            $("#divMainGrid > table > tbody > tr").not(":first").each(function (index, e) {
                                                if(lineItem.OppID == $(e).find("#hdnOppID").val()){
                                                    $(e).css("background-color","#ffdada");
                                                    $(this).find(".chkSelect").closest("td").find(".invoice-orders").remove();
                                                    $(this).find(".chkSelect").closest("td").append("<i class='fa fa-exclamation-triangle invoice-orders' style='color:red' data-toggle='tooltip' title='" + replaceNull(lineItem.ErrorMessage) + "' aria-hidden='true'></i>");
                                                }
                                            });
                                        }
                                    });
                                } else {
                                    ClearFilters();
                                    ClearRightPaneSelection();
                                    LoadRecordsWithPagination();
                                    alert("Selected order(s) are invoiced successfully.");
                                }

                            } catch (err) {
                                alert("Unknown error occurred.");
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            if(IsJsonString(jqXHR.responseText)){
                                var objError = $.parseJSON(jqXHR.responseText)
                                alert("Error occurred: " + replaceNull(objError.ErrorMessage));
                            } else {
                                alert("Unknown error ocurred");
                            }   
                        }
                    });
                }
            } else {
                alert("Select records.")
            }
        } catch (e) {
            alert("Unknown error occurred while invoicing orders.");
        }
    });

    $("#btnCreatePackingSlip").click(function(){
        try {
            var selectdRecords = [];

            $("#divMainGrid > table > tbody > tr").not(":first").each(function (index, e) {
                if($(e).find(".chkSelect").is(":checked")) {
                    var found = $.map(selectdRecords, function(obj) {
                        if(obj.OppID === parseInt($(e).find("#hdnOppID").val())) 
                            return obj;
                    });

                    if(found.length === 0) {
                        var obj = {};
                        obj.OppID= parseInt($(e).find("#hdnOppID").val());
                        if(parseInt($(e).find("#hdnOppItemID").val())> 0 ){
                            if($(e).find("#txtRemainingQty").length > 0){
                                obj.OppItemIDs = parseInt($(e).find("#hdnOppItemID").val()).toString() + "-" +  parseFloat($(e).find("#txtRemainingQty").val());
                            } else {
                                obj.OppItemIDs = parseInt($(e).find("#hdnOppItemID").val()).toString() + "-" +  parseFloat($(e).find("#hdnRemainingQty").val());
                            }
                        } else {
                            obj.OppItemIDs = "";
                        }
                        obj.IsSucess = false;
                        obj.ErrorMessage = "";
                        obj.OppBizDocID = 0;
                        selectdRecords.push(obj);
                    } else {
                        if(parseInt($(e).find("#hdnOppItemID").val())> 0 ){
                            if($(e).find("#txtRemainingQty").length > 0){
                                found[0].OppItemIDs = found[0].OppItemIDs + "," + (parseInt($(e).find("#hdnOppItemID").val()).toString() + "-" +  parseFloat($(e).find("#txtRemainingQty").val()));
                            } else {
                                found[0].OppItemIDs = found[0].OppItemIDs + "," + (parseInt($(e).find("#hdnOppItemID").val()).toString() + "-" +  parseFloat($(e).find("#hdnRemainingQty").val()));
                            }
                        }
                    }
                }
            });

            if(selectdRecords.length > 0){
                if(confirm("You're about to create packing slips all rows selected, are you sure you want to do this?")){
                    $.ajax({
                        type: "POST",
                        url: '../opportunity/MassSalesFulfillmentService.svc/PackOrders',
                        contentType: "application/json",
                        dataType: "json",
                        data: JSON.stringify({
                            "orders": JSON.stringify(selectdRecords)
                            , "packingViewMode": parseInt($("[id$=hdnPackLastSubViewMode]").val())
                        }),
                        success: function (data) {
                            try {
                                var obj = $.parseJSON(data.PackOrdersResult);
                                          
                                var foundRecordsWithError = $.map(obj, function(e) {
                                    if(!e.IsSucess) 
                                        return e;
                                });

                                if(foundRecordsWithError.length > 0){
                                    alert("Error occurred while adding shipping bizdoc for some order(s). please review records and try again.");

                                    obj.forEach(function (lineItem) {
                                        if(lineItem.IsSucess){
                                            $("#divMainGrid > table > tbody > tr").not(":first").each(function (index, e) {
                                                if(lineItem.OppID == $(e).find("#hdnOppID").val()){
                                                    $(e).remove();
                                                }
                                            });
                                        } else {
                                            $("#divMainGrid > table > tbody > tr").not(":first").each(function (index, e) {
                                                if(lineItem.OppID == $(e).find("#hdnOppID").val()){
                                                    $(e).css("background-color","#ffdada");
                                                    $(this).find(".chkSelect").closest("td").find(".invoice-orders").remove();
                                                    $(this).find(".chkSelect").closest("td").append("<i class='fa fa-exclamation-triangle invoice-orders' style='color:red' data-toggle='tooltip' title='" + replaceNull(lineItem.ErrorMessage) + "' aria-hidden='true'></i>");
                                                }
                                            });
                                        }
                                    });
                                } else {
                                    ClearFilters();
                                    ClearRightPaneSelection();
                                    PackingViewChanged(2);
                                    alert("Shipping bizdoc added successfully for Selected order(s).");
                                }

                            } catch (err) {
                                alert("Unknown error occurred.");
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            if(IsJsonString(jqXHR.responseText)){
                                var objError = $.parseJSON(jqXHR.responseText)
                                alert("Error occurred: " + replaceNull(objError.ErrorMessage));
                            } else {
                                alert("Unknown error ocurred");
                            }   
                        }
                    });
                }
            } else {
                alert("Select records.")
            }
        } catch (e) {
            alert("Unknown error occurred while invoicing orders.");
        }
    });

    $("#btnPayOrders").click(function(){
        try {
            var selectdRecords = [];

            $("#divMainGrid > table > tbody > tr").not(":first").each(function (index, e) {
                if($(e).find(".chkSelect").is(":checked")) {
                    var found = $.map(selectdRecords, function(obj) {
                        if(obj.OppID === parseInt($(e).find("#hdnOppID").val()) && obj.OppBizDocID === parseInt($(e).find("#hdnOppBizDocID").val())) 
                            return obj;
                    });

                    if(found.length === 0) {
                        var obj = {};
                        obj.OppID= parseInt($(e).find("#hdnOppID").val());
                        obj.OppBizDocID = parseInt($(e).find("#hdnOppBizDocID").val());
                        obj.AmountToPay = parseFloat($(e).find("#hdnAmountToPay").val());
                        obj.IsSucess = false;
                        obj.ErrorMessage = "";
                        selectdRecords.push(obj);
                    }
                }
            });

            if(selectdRecords.length > 0){
                if(confirm("You're about to pay all rows selected, are you sure you want to do this?")){
                    $.ajax({
                        type: "POST",
                        url: '../opportunity/MassSalesFulfillmentService.svc/PayOrders',
                        contentType: "application/json",
                        dataType: "json",
                        data: JSON.stringify({
                            "invoices": JSON.stringify(selectdRecords)
                        }),
                        success: function (data) {
                            try {
                                var obj = $.parseJSON(data.PayOrdersResult);
                                          
                                var foundRecordsWithError = $.map(obj, function(e) {
                                    if(!e.IsSucess) 
                                        return e;
                                });

                                if(foundRecordsWithError.length > 0){
                                    alert("Error occurred while payment processing of some invoice(s). please review records and try again.");

                                    obj.forEach(function (lineItem) {
                                        if(lineItem.IsSucess){
                                            $("#divMainGrid > table > tbody > tr").not(":first").each(function (index, e) {
                                                if(lineItem.OppID == $(e).find("#hdnOppID").val() && lineItem.OppBizDocID == $(e).find("#hdnOppBizDocID").val()){
                                                    $(e).remove();
                                                }
                                            });
                                        } else {
                                            $("#divMainGrid > table > tbody > tr").not(":first").each(function (index, e) {
                                                if(lineItem.OppID == $(e).find("#hdnOppID").val() && lineItem.OppBizDocID == $(e).find("#hdnOppBizDocID").val()){
                                                    $(e).css("background-color","#ffdada");
                                                    $(this).find(".chkSelect").closest("td").find(".pay-orders").remove();
                                                    $(this).find(".chkSelect").closest("td").append("<i class='fa fa-exclamation-triangle pay-orders' style='color:red' data-toggle='tooltip' title='" + replaceNull(lineItem.ErrorMessage) + "' aria-hidden='true'></i>");
                                                }
                                            });
                                        }
                                    });
                                } else {
                                    ClearFilters();
                                    ClearRightPaneSelection();
                                    LoadRecordsWithPagination();
                                    alert("Payment processed successfully for selected invoice(s).");
                                }

                            } catch (err) {
                                alert("Unknown error occurred.");
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            if(IsJsonString(jqXHR.responseText)){
                                var objError = $.parseJSON(jqXHR.responseText)
                                alert("Error occurred: " + replaceNull(objError.ErrorMessage));
                            } else {
                                alert("Unknown error ocurred");
                            }   
                        }
                    });
                }
            } else {
                alert("Select records.")
            }
        } catch (e) {
            alert("Unknown error occured while closing selected order(s).")
        }
    });

    $("#btnCloseOrders").click(function(){
        try {
            var selectdRecords = [];

            $("#divMainGrid > table > tbody > tr").not(":first").each(function (index, e) {
                if($(e).find(".chkSelect").is(":checked")) {
                    var found = $.map(selectdRecords, function(obj) {
                        if(obj.OppID === parseInt($(e).find("#hdnOppID").val())) 
                            return obj;
                    });

                    if(found.length === 0) {
                        var obj = {};
                        obj.OppID= parseInt($(e).find("#hdnOppID").val());
                        obj.IsSucess = false;
                        obj.ErrorMessage = "";
                        selectdRecords.push(obj);
                    }
                }
            });

            if(selectdRecords.length > 0){
                if(confirm("You're about to close all rows selected, are you sure you want to do this?")){
                    $.ajax({
                        type: "POST",
                        url: '../opportunity/MassSalesFulfillmentService.svc/CloseOrders',
                        contentType: "application/json",
                        dataType: "json",
                        data: JSON.stringify({
                            "orders": JSON.stringify(selectdRecords)
                        }),
                        success: function (data) {
                            try {
                                var obj = $.parseJSON(data.CloseOrdersResult);
                                          
                                var foundRecordsWithError = $.map(obj, function(e) {
                                    if(!e.IsSucess) 
                                        return e;
                                });

                                if(foundRecordsWithError.length > 0){
                                    alert("Error occurred while closing some order(s). please review records and try again.");

                                    obj.forEach(function (lineItem) {
                                        if(lineItem.IsSucess){
                                            $("#divMainGrid > table > tbody > tr").not(":first").each(function (index, e) {
                                                if(lineItem.OppID == $(e).find("#hdnOppID").val()){
                                                    $(e).remove();
                                                }
                                            });
                                        } else {
                                            $("#divMainGrid > table > tbody > tr").not(":first").each(function (index, e) {
                                                if(lineItem.OppID == $(e).find("#hdnOppID").val()){
                                                    $(e).css("background-color","#ffdada");
                                                    $(this).find(".chkSelect").closest("td").find(".close-orders").remove();
                                                    $(this).find(".chkSelect").closest("td").append("<i class='fa fa-exclamation-triangle close-orders' style='color:red' data-toggle='tooltip' title='" + replaceNull(lineItem.ErrorMessage) + "' aria-hidden='true'></i>");
                                                }
                                            });
                                        }
                                    });
                                } else {
                                    ClearFilters();
                                    ClearRightPaneSelection();
                                    LoadRecordsWithPagination();
                                    alert("Selected order(s) are closed successfully.");
                                }

                            } catch (err) {
                                alert("Unknown error occurred.");
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            if(IsJsonString(jqXHR.responseText)){
                                var objError = $.parseJSON(jqXHR.responseText)
                                alert("Error occurred: " + replaceNull(objError.ErrorMessage));
                            } else {
                                alert("Unknown error ocurred");
                            }   
                        }
                    });
                }
            } else {
                alert("Select records.")
            }
        } catch (e) {
            alert("Unknown error occured while closing selected order(s).")
        }
    });

    $("#btnCancelShippingRate").click(function(){
        LoadRecordsWithPagination();
    });

    $("#btnGetShippingRate").click(function(){
        try {
            var selectdRecords = [];

            $("#divMainGrid > table > tbody > tr").not(":first").each(function(index, e){
                if($(this).find(".chkSelect").is(":checked")) {
                    var obj = {};
                    obj.OppID= parseInt($(e).find("#hdnOppID").val());
                    obj.WarehouseID = parseInt($("#ddlWarehouse").val());
                    obj.ShipByDate = $(e).find("#hdnShipByDate").val();
                    obj.Rate = 0;
                    obj.TransitTime = 0;
                    obj.Description = "";
                    obj.IsRateFetchedSuccessfully = false;
                    obj.ErrorMessage = "";
                    selectdRecords.push(obj);
                }
            });

            if(selectdRecords.length > 0) {
                $.ajax({
                    type: "POST",
                    url: '../opportunity/MassSalesFulfillmentService.svc/GetShippingRate',
                    contentType: "application/json",
                    dataType: "json",
                    data: JSON.stringify({
                        "selectedRecords": JSON.stringify(selectdRecords)
                    }),
                    success: function (data) {
                        try {
                            var result = $.parseJSON(data.GetShippingRateResult);
                                                        
                            if (result != null && result.length > 0) {
                                $("#divMainGrid > table > tbody > tr").not(":first").each(function (index, e) {
                                    var found = $.map(result, function(item) {
                                        if(item.OppID == $(e).find("#hdnOppID").val() && item.ShipByDate == $(e).find("#hdnShipByDate").val()) 
                                            return item;
                                    });

                                    if(found.length > 0) {
                                        var obj = found[0];

                                        if(obj.IsRateFetchedSuccessfully){
                                            $(e).css("background-color","rgb(229, 255, 225)");
                                            $(e).find("#lblShipRate").html($("[id$=hdnMSCurrencyID]").val() + " " + obj.Rate + " (" + obj.Description + ")");
                                            $(e).find("#hdnShipRate").val(obj.Rate);
                                            $(e).find("#hdnShipDescription").val(obj.Description);
                                            $(e).find("#hdnIsShipRateFetched").val("true");

                                            $(e).find(".chkSelect").closest("td").find(".get-rates").remove();
                                            $(e).find(".chkSelect").closest("td").append("  <i class='fa fa-check-circle get-rates' style='color:green' aria-hidden='true'></i>");

                                            if($(e).find("td.tdDeliverBy").length > 0){
                                                var tdValue = "";

                                                if(parseInt(obj.TransitTime) > 0){
                                                    tdValue = parseInt(obj.TransitTime) + (parseInt(obj.TransitTime) > 1 ? " Days" : " Day");

                                                    if($.datepicker.parseDate("yy-mm-dd", $(e).find("#hdnExpectedDate").val()) != null) {
                                                        var dtExpectedDate = $.datepicker.parseDate("yy-mm-dd", $(e).find("#hdnExpectedDate").val());
                                                        var deliveryDate;

                                                        if(parseInt(obj.TransitTime) > 0){
                                                            var now = new Date();

                                                            if($.datepicker.parseDate("yy-mm-dd", $(e).find("#hdnShipByDate").val()) != null){
                                                                now = $.datepicker.parseDate("yy-mm-dd", $(e).find("#hdnShipByDate").val());
                                                            } 
                                                    
                                                            now.setDate(now.getDate()+ parseInt(obj.TransitTime));
                                                            deliveryDate = now;
                                                        } else if($.datepicker.parseDate("yy-mm-dd", obj.DeliveryDate) != null){
                                                            deliveryDate =$.datepicker.parseDate("yy-mm-dd", obj.DeliveryDate);
                                                        }

                                                        if(deliveryDate <= dtExpectedDate){
                                                            tdValue = tdValue + " <span style='color:green'>(On Time)</span>";                                               
                                                        } else {
                                                            tdValue = tdValue + " <span style='color:red'>(Late)</span>";
                                                        }
                                                    }
                                                } else {
                                                    tdValue = "N/A";
                                                }

                                                $(e).find("td.tdDeliverBy").html(tdValue);
                                            }

                                            $("#btnGetShippingRate").hide();
                                            $("#btnCommitShippingRate").show();
                                            $("#btnCancelShippingRate").show();
                                        } else {
                                            $("#divMainGrid > table > tbody > tr").not(":first").each(function (index, e) {
                                                if(obj.OppID == $(e).find("#hdnOppID").val() && obj.ShipByDate == $(e).find("#hdnShipByDate").val()){
                                                    $(e).css("background-color","#ffdada");
                                                    $(this).find(".chkSelect").closest("td").find(".get-rates").remove();
                                                    $(this).find(".chkSelect").closest("td").append("<i class='fa fa-exclamation-triangle get-rates' style='color:red' data-toggle='tooltip' title='" + replaceNull(obj.ErrorMessage) + "' aria-hidden='true'></i>");
                                                }
                                            });
                                        }
                                    }
                                });
                            } 
                        } catch (err) {
                            alert("Unknown error occurred.");
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        if(IsJsonString(jqXHR.responseText)){
                            var objError = $.parseJSON(jqXHR.responseText)
  
                            $("#divMainGrid > table > tbody > tr").not(":first").each(function (index, e) {
                                if(objError.OppID == $(e).find("#hdnOppID").val() && objError.ShipByDate == $(e).find("#hdnShipByDate").val()){
                                    $(e).css("background-color","#ffdada");
                                    $(this).find(".chkSelect").closest("td").find(".get-rates").remove();
                                    $(this).find(".chkSelect").closest("td").append("<i class='fa fa-exclamation-triangle get-rates' style='color:red' data-toggle='tooltip' title='" + replaceNull(objError.ErrorMessage) + "' aria-hidden='true'></i>");
                                }
                            });

                            $('[data-toggle="tooltip"]').tooltip(); 
                        } else {
                            $("#divMainGrid > table > tbody > tr").not(":first").each(function (index, e) {
                                if(objError.OppID == $(e).find("#hdnOppID").val() && objError.ShipByDate == $(e).find("#hdnShipByDate").val()){
                                    $(e).css("background-color","#ffdada");
                                    $(this).find(".chkSelect").closest("td").find(".get-rates").remove();
                                    $(this).find(".chkSelect").closest("td").append("<i class='fa fa-exclamation-triangle get-rates' style='color:red' data-toggle='tooltip' title='Unknown error ocurred' aria-hidden='true'></i>");
                                }
                            });
                                
                            $('[data-toggle="tooltip"]').tooltip(); 
                        }   
                    }
                });
            } else {
                alert("Select atleast one record.");
            }
        } catch (e) {
            alert("Unknown error occurred while fetching shipping rates.");
        }
                
        return false;
    });

    $("#ddlFromCountry").change(function(){
        LoadStates($("#ddlFromState"), parseInt($("#ddlFromCountry").val()), 0);
    });

    $("#ddlToCountry").change(function(){
        LoadStates($("#ddlToState"), parseInt($("#ddlToCountry").val()), 0);
    });

    $("#ddlPaymentOption").change(function(){
        if($(this).val() === "0"){
            $("#divPaymentOption1").hide();
        } else {
            if($("#hdnShipCompany").val() === "91") {
                $("#divPaymentOption1").show();
                $("#divThirdPartyZipCode").hide();
            } else if($("#hdnShipCompany").val() === "88"){
                $("#divPaymentOption1").show();
                $("#divThirdPartyZipCode").show();
            }
            else {
                $("#divPaymentOption1").hide();
            }
        }
    });

    $("#chkCOD").change(function(){
        if($(this).is(":checked")){
            $("#divShipCOD").show();
        } else {
            $("#divShipCOD").hide();
            $("#ddlCODType").val("0");
            $("#txtCODAmount").val("");
        }
    });

    $("#ddlWarehouse").change(function(){
        ClearRightPaneSelection();
        LoadRecordsWithPagination();
    });

    $("#btnPackPickedItems").click(function () {
        try {
            var isPickNotPackedColumnAdded = false;
            $("#divMainGrid > table > tbody > tr").not(":first").each(function (index, e) {
                if ($(e).find("#lblQtyPickedNotPacked").length > 0) {
                    isPickNotPackedColumnAdded = true;
                }

                return false;
            });

            if (isPickNotPackedColumnAdded) {
                var selectdRecords = [];

                $("#divMainGrid > table > tbody > tr").not(":first").each(function (index, e) {
                    if ($(e).find(".chkSelect").is(":checked") && ($("#chkGroupBy").is(":checked") || parseFloat($(e).find("#lblQtyPickedNotPacked").text()) > 0)) {
                        var found = $.map(selectdRecords, function (obj) {
                            if (obj.OppID === parseInt($(e).find("#hdnOppID").val()))
                                return obj;
                        });

                        if (found.length === 0) {
                            var obj = {};
                            obj.OppID = parseInt($(e).find("#hdnOppID").val());
                            if (parseInt($(e).find("#hdnOppItemID").val()) > 0) {
                                obj.OppItemIDs = parseInt($(e).find("#hdnOppItemID").val()).toString() + "-" + parseFloat($(e).find("#lblQtyPickedNotPacked").text());
                            } else {
                                obj.OppItemIDs = "";
                            }
                            obj.IsSucess = false;
                            obj.ErrorMessage = "";
                            obj.OppBizDocID = 0;
                            selectdRecords.push(obj);
                        } else {
                            if (parseInt($(e).find("#hdnOppItemID").val()) > 0) {
                                found[0].OppItemIDs = found[0].OppItemIDs + "," + (parseInt($(e).find("#hdnOppItemID").val()).toString() + "-" + parseFloat($(e).find("#lblQtyPickedNotPacked").text()));
                            }
                        }
                    }
                });

                if (selectdRecords.length > 0) {
                    if (confirm("You're about to create packing slips all rows selected, are you sure you want to do this?")) {
                        $.ajax({
                            type: "POST",
                            url: '../opportunity/MassSalesFulfillmentService.svc/PackOrders',
                            contentType: "application/json",
                            dataType: "json",
                            data: JSON.stringify({
                                "orders": JSON.stringify(selectdRecords)
                                , "isAddFulfillmentBizDoc": false
                            }),
                            success: function (data) {
                                try {
                                    var obj = $.parseJSON(data.PackOrdersResult);

                                    var foundRecordsWithError = $.map(obj, function (e) {
                                        if (!e.IsSucess)
                                            return e;
                                    });


                                    var bizDocsToPrint = [];

                                    obj.forEach(function (lineItem) {
                                        if (parseInt(lineItem.OppBizDocID) > 0) {
                                            var obj = {};
                                            obj.OppID = lineItem.OppID;
                                            obj.OppBizDocID = lineItem.OppBizDocID
                                            obj.BizDocName = ""
                                            bizDocsToPrint.push(obj);
                                        }
                                    });

                                    if (foundRecordsWithError.length > 0) {
                                        alert("Error occurred while adding shipping bizdoc for some order(s). please review records and try again.");

                                        obj.forEach(function (lineItem) {
                                            if (lineItem.IsSucess) {
                                                $("#divMainGrid > table > tbody > tr").not(":first").each(function (index, e) {
                                                    if (lineItem.OppID == $(e).find("#hdnOppID").val()) {
                                                        $(e).remove();
                                                    }
                                                });
                                            } else {
                                                $("#divMainGrid > table > tbody > tr").not(":first").each(function (index, e) {
                                                    if (lineItem.OppID == $(e).find("#hdnOppID").val()) {
                                                        $(e).css("background-color", "#ffdada");
                                                        $(this).find(".chkSelect").closest("td").find(".invoice-orders").remove();
                                                        $(this).find(".chkSelect").closest("td").append("<i class='fa fa-exclamation-triangle invoice-orders' style='color:red' data-toggle='tooltip' title='" + replaceNull(lineItem.ErrorMessage) + "' aria-hidden='true'></i>");
                                                    }
                                                });
                                            }
                                        });

                                        if (bizDocsToPrint.length > 0) {
                                            $("#modal-confirmation #hdnBizDocIDs").val(JSON.stringify(bizDocsToPrint));
                                            $("#modal-confirmation .modal-body").html("Packing slip(s) added successfully. <br/> Corresponding Packing Slips have been added to orders.");
                                            $("#btnSaveConfirmation").text("Print Packing Slip(s)")
                                            $("#modal-confirmation").modal("show");
                                        }
                                    } else {
                                        ClearFilters();
                                        ClearRightPaneSelection();
                                        PackingViewChanged(2);
                                        if (bizDocsToPrint.length > 0) {
                                            $("#modal-confirmation #hdnBizDocIDs").val(JSON.stringify(bizDocsToPrint));
                                            $("#modal-confirmation .modal-body").html("Packing slip(s) added successfully. <br/> Corresponding Packing Slips have been added to orders.");
                                            $("#btnSaveConfirmation").text("Print Packing Slip(s)")
                                            $("#modal-confirmation").modal("show");
                                        } else {
                                            alert("Packing slip(s) added successfully for Selected order(s).")
                                        }
                                    }

                                } catch (err) {
                                    alert("Unknown error occurred.");
                                }
                            },
                            error: function (jqXHR, textStatus, errorThrown) {
                                if (IsJsonString(jqXHR.responseText)) {
                                    var objError = $.parseJSON(jqXHR.responseText)
                                    alert("Error occurred: " + replaceNull(objError.ErrorMessage));
                                } else {
                                    alert("Unknown error ocurred");
                                }
                            }
                        });
                    }
                } else {
                    alert("Select records with quantity left to be packed.")
                }
            } else {
                alert("Please add \"Picked Not Packed\" column to list");
            }
        } catch (e) {
            alert("Unknown error occurred while adding bizdoc.");
        }

        return false;
    });

    $("#btnShipPackedItems").click(function () {
        try {
            var isPackNotShippedColumnAdded = false;
            $("#divMainGrid > table > tbody > tr").not(":first").each(function (index, e) {
                if ($(e).find("#lblQtyPackedNotShipped").length > 0) {
                    isPackNotShippedColumnAdded = true
                    return false;
                }
            });

            if (isPackNotShippedColumnAdded) {
                var selectdRecords = [];

                $("#divMainGrid > table > tbody > tr").not(":first").each(function (index, e) {
                    if ($(e).find(".chkSelect").is(":checked") && ($("#chkGroupBy").is(":checked") || parseFloat($(e).find("#lblQtyPackedNotShipped").text()) > 0)) {
                        var found = $.map(selectdRecords, function (obj) {
                            if (obj.OppID === parseInt($(e).find("#hdnOppID").val()))
                                return obj;
                        });

                        if (found.length === 0) {
                            var obj = {};
                            obj.OppID = parseInt($(e).find("#hdnOppID").val());
                            obj.ParentBizDocID = parseInt($(e).find("[id*=hdnParentBizDoc]").val());
                            if (parseInt($(e).find("#hdnOppItemID").val()) > 0) {
                                obj.OppItemIDs = parseInt($(e).find("#hdnOppItemID").val()).toString() + "-" + parseFloat($(e).find("#lblQtyPackedNotShipped").text());
                            } else {
                                obj.OppItemIDs = "";
                            }
                            obj.IsSucess = false;
                            obj.ErrorMessage = "";
                            selectdRecords.push(obj);
                        } else {
                            if (parseInt($(e).find("#hdnOppItemID").val()) > 0) {
                                found[0].OppItemIDs = found[0].OppItemIDs + "," + (parseInt($(e).find("#hdnOppItemID").val()).toString() + "-" + parseFloat($(e).find("#lblQtyPackedNotShipped").text()));
                            }
                        }
                    }
                });

                if (selectdRecords.length > 0) {
                    if (confirm("You're about to ship all rows selected, are you sure you want to do this?")) {
                        $.ajax({
                            type: "POST",
                            url: '../opportunity/MassSalesFulfillmentService.svc/ShipOrders',
                            contentType: "application/json",
                            dataType: "json",
                            data: JSON.stringify({
                                "orders": JSON.stringify(selectdRecords)
                            }),
                            success: function (data) {
                                try {
                                    var obj = $.parseJSON(data.ShipOrdersResult);

                                    var foundRecordsWithError = $.map(obj, function (e) {
                                        if (!e.IsSucess)
                                            return e;
                                    });

                                    if (foundRecordsWithError.length > 0) {
                                        alert("Error occurred in fulfillment of some order(s). please review records and try again.");

                                        obj.forEach(function (lineItem) {
                                            if (lineItem.IsSucess) {
                                                $("#divMainGrid > table > tbody > tr").not(":first").each(function (index, e) {
                                                    if (lineItem.OppID == $(e).find("#hdnOppID").val()) {
                                                        $(e).remove();
                                                    }
                                                });
                                            } else {
                                                $("#divMainGrid > table > tbody > tr").not(":first").each(function (index, e) {
                                                    if (lineItem.OppID == $(e).find("#hdnOppID").val()) {
                                                        $(e).css("background-color", "#ffdada");
                                                        $(this).find(".chkSelect").closest("td").find(".ship-orders").remove();
                                                        $(this).find(".chkSelect").closest("td").append("<i class='fa fa-exclamation-triangle ship-orders' style='color:red' data-toggle='tooltip' title='" + replaceNull(lineItem.ErrorMessage) + "' aria-hidden='true'></i>");
                                                    }
                                                });
                                            }
                                        });
                                    } else {
                                        alert("Selected record(s) shipped successfully.");
                                        ClearFilters();
                                        ClearRightPaneSelection();
                                        PackingViewChanged(2);
                                    }

                                } catch (err) {
                                    alert("Unknown error occurred.");
                                }
                            },
                            error: function (jqXHR, textStatus, errorThrown) {
                                if (IsJsonString(jqXHR.responseText)) {
                                    var objError = $.parseJSON(jqXHR.responseText)
                                    alert("Error occurred: " + replaceNull(objError.ErrorMessage));
                                } else {
                                    alert("Unknown error ocurred");
                                }
                            }
                        });
                    }
                } else {
                    alert("Select records with quantity left to be shipped.")
                }
            } else {
                alert("Please add \"Packed Not Shipped\" column to list");
            }
        } catch (e) {
            alert("Unknown error occurred while adding bizdoc.");
        }

        return false;
    });

    $("#btnPackAndShipPickedItems").click(function () {
        try {
            var isPickNotPackedColumnAdded = false;
            $("#divMainGrid > table > tbody > tr").not(":first").each(function (index, e) {
                if ($(e).find("#lblQtyPickedNotPacked").length > 0) {
                    isPickNotPackedColumnAdded = true;
                }

                return false;
            });

            if (isPickNotPackedColumnAdded) {
                var selectdRecords = [];

                $("#divMainGrid > table > tbody > tr").not(":first").each(function (index, e) {
                    if ($(e).find(".chkSelect").is(":checked") && ($("#chkGroupBy").is(":checked") || parseFloat($(e).find("#lblQtyPickedNotPacked").text()) > 0)) {
                        var found = $.map(selectdRecords, function (obj) {
                            if (obj.OppID === parseInt($(e).find("#hdnOppID").val()))
                                return obj;
                        });

                        if (found.length === 0) {
                            var obj = {};
                            obj.OppID = parseInt($(e).find("#hdnOppID").val());
                            if (parseInt($(e).find("#hdnOppItemID").val()) > 0) {
                                obj.OppItemIDs = parseInt($(e).find("#hdnOppItemID").val()).toString() + "-" + parseFloat($(e).find("#lblQtyPickedNotPacked").text());
                            } else {
                                obj.OppItemIDs = "";
                            }
                            obj.IsSucess = false;
                            obj.ErrorMessage = "";
                            obj.OppBizDocID = 0;
                            selectdRecords.push(obj);
                        } else {
                            if (parseInt($(e).find("#hdnOppItemID").val()) > 0) {
                                found[0].OppItemIDs = found[0].OppItemIDs + "," + (parseInt($(e).find("#hdnOppItemID").val()).toString() + "-" + parseFloat($(e).find("#lblQtyPickedNotPacked").text()));
                            }
                        }
                    }
                });

                if (selectdRecords.length > 0) {
                    if (confirm("You're about to create packing slips and ship all rows selected, are you sure you want to do this?")) {
                        $.ajax({
                            type: "POST",
                            url: '../opportunity/MassSalesFulfillmentService.svc/PackOrders',
                            contentType: "application/json",
                            dataType: "json",
                            data: JSON.stringify({
                                "orders": JSON.stringify(selectdRecords)
                                , "isAddFulfillmentBizDoc": true
                            }),
                            success: function (data) {
                                try {
                                    var obj = $.parseJSON(data.PackOrdersResult);

                                    var foundRecordsWithError = $.map(obj, function (e) {
                                        if (!e.IsSucess)
                                            return e;
                                    });

                                    if (foundRecordsWithError.length > 0) {
                                        alert("Error occurred while adding shipping bizdoc for some order(s). please review records and try again.");

                                        obj.forEach(function (lineItem) {
                                            if (lineItem.IsSucess) {
                                                $("#divMainGrid > table > tbody > tr").not(":first").each(function (index, e) {
                                                    if (lineItem.OppID == $(e).find("#hdnOppID").val()) {
                                                        $(e).remove();
                                                    }
                                                });
                                            } else {
                                                $("#divMainGrid > table > tbody > tr").not(":first").each(function (index, e) {
                                                    if (lineItem.OppID == $(e).find("#hdnOppID").val()) {
                                                        $(e).css("background-color", "#ffdada");
                                                        $(this).find(".chkSelect").closest("td").find(".invoice-orders").remove();
                                                        $(this).find(".chkSelect").closest("td").append("<i class='fa fa-exclamation-triangle invoice-orders' style='color:red' data-toggle='tooltip' title='" + replaceNull(lineItem.ErrorMessage) + "' aria-hidden='true'></i>");
                                                    }
                                                });
                                            }
                                        });
                                    } else {
                                        ClearFilters();
                                        ClearRightPaneSelection();
                                        PackingViewChanged(2);
                                        alert("Shipping bizdoc added successfully for Selected order(s).");
                                    }

                                } catch (err) {
                                    alert("Unknown error occurred.");
                                }
                            },
                            error: function (jqXHR, textStatus, errorThrown) {
                                if (IsJsonString(jqXHR.responseText)) {
                                    var objError = $.parseJSON(jqXHR.responseText)
                                    alert("Error occurred: " + replaceNull(objError.ErrorMessage));
                                } else {
                                    alert("Unknown error ocurred");
                                }
                            }
                        });
                    }
                } else {
                    alert("Select records with quantity left to be packed.")
                }
            } else {
                alert("Please add \"Picked Not Packed\" column to list");
            }
        } catch (e) {
            alert("Unknown error occurred while adding bizdoc.");
        }

        return false;
    });

    $("#btnSaveConfirmation").click(function () {
        try {
            if ($("[id$=hdnBizDocIDs]").val() != "") {
                var selectdRecords = JSON.parse($("[id$=hdnBizDocIDs]").val());

                if (selectdRecords.length > 0) {
                    PrintBizDoc(selectdRecords);
                }
            }

            $("#modal-confirmation").modal("hide");
        } catch (e) {
            alert("Unknown error occurred while printing bizdoc(s).");
        }
    });
});


function PrintBizDoc(selectdRecords) {
    try {
        $.ajax({
            type: "POST",
            url: '../opportunity/MassSalesFulfillmentService.svc/PrintBizDoc',
            contentType: "application/json",
            dataType: "json",
            data: JSON.stringify({
                "selectdRecords": JSON.stringify(selectdRecords)
            }),
            success: function (data) {
                try {
                    var obj = $.parseJSON(data.PrintBizDocResult);

                    var sampleArr = base64ToArrayBuffer(obj.bizDocBytes);

                    now = new Date();
                    year = "" + now.getFullYear();
                    month = "" + (now.getMonth() + 1); if (month.length == 1) { month = "0" + month; }
                    day = "" + now.getDate(); if (day.length == 1) { day = "0" + day; }
                    hour = "" + now.getHours(); if (hour.length == 1) { hour = "0" + hour; }
                    minute = "" + now.getMinutes(); if (minute.length == 1) { minute = "0" + minute; }
                    second = "" + now.getSeconds(); if (second.length == 1) { second = "0" + second; }

                    saveByteArray("PrintBizDoc" + year + month + day + hour + minute + second, sampleArr);
                } catch (err) {
                    alert("Unknown error occurred.");
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                if (IsJsonString(jqXHR.responseText)) {
                    var objError = $.parseJSON(jqXHR.responseText)
                    alert("Error occurred: " + replaceNull(objError.ErrorMessage));
                } else {
                    alert("Unknown error ocurred");
                }
            }
        });
    } catch (e) {
        throw e;
    }
}


function ResetPanes(){
    $("#ms-container").removeClass("ms-sidebar-collapsed").addClass("ms-sidebar-expanded");
    $("#ms-container").removeClass("ms-maingrid-collapsed").addClass("ms-maingrid-expanded");
    $("#ms-container").removeClass("ms-selectedgrid-collapsed").addClass("ms-selectedgrid-expanded");

    if ($("#ms-main-grid").hasClass("col-md-12")) {
        $("#ms-main-grid").removeClass("col-md-12").addClass("col-md-9");
    } else {
        $("#ms-main-grid").removeClass("col-md-10").addClass("col-md-7");
    }

    $("#ms-selected-grid").attr("class","col-sm-12 col-md-3");
}

function base64ToArrayBuffer(base64) {
    var binaryString = window.atob(base64);
    var binaryLen = binaryString.length;
    var bytes = new Uint8Array(binaryLen);
    for (var i = 0; i < binaryLen; i++) {
        var ascii = binaryString.charCodeAt(i);
        bytes[i] = ascii;
    }
    return bytes;
}

function saveByteArray(reportName, byte) {
    var blob = new Blob([byte], {type: "application/pdf"});
    var link = document.createElement('a');
    link.href = window.URL.createObjectURL(blob);
    var fileName = reportName;
    link.download = fileName;
    link.click();
};

function ClearFilters() {
    try {
        $("#divOrderStatus input[type='checkbox']").each(function () {
            $(this).prop("checked", "");
        });

        $("#divFilterValueOptions input[type='checkbox']").each(function () {
            $(this).prop("checked", "");
        });

        $("#divMainGrid table tr th input").each(function (index, e) {
            $(this).val("");
        });

        $("#divMainGrid table tr th select").each(function (index, e) {
            $(this).val("0");
        });

        $("#rbInclude").prop("checked", "checked");
        $("#rbExclude").prop("checked", "");
        $("#ddlShippingZone").val("0");
        $("#hdnBatchID").val("0");
        $("#btnBatch").html("Select Batch <span class='caret'></span>");
        $("#btnRemoveFromBatch").hide();

        $("#hdnSortColumn").val("OpportunityMaster.bintCreatedDate");
        $("#hdnSortOrder").val("DESC");

        $("#dropdownMenuFilter").text("None Selected");
        $("#hdnPrintBizDocViewMode").val("1");
    } catch (e) {
        alert("Unknown error occurred while clearing filters.");
    }
}

function ClearRightPaneSelection(){
    $("#tblRight").html("");
    $("#hdnSelectedRecordsForRightPanePicking").val("");
    $("#ddlBatchRight").val("0");
    $("#divGridRightPack").html("");
}

function LoadOrderStatus() {
    return $.ajax({
        type: "POST",
        url: '../opportunity/MassSalesFulfillmentService.svc/GetOrderStatus',
        contentType: "application/json",
        dataType: "json",
        data: JSON.stringify({
            "oppType": 1
        }),
        success: function (data) {
            try {
                var obj = $.parseJSON(data.GetOrderStatusResult);
                if (obj != null && obj.length > 0) {
                    var strOrderStauts = "";

                    obj.forEach(function (e) {
                        strOrderStauts += "<li><div class='checkbox'><input type='checkbox' id='chkOrderStatus" + e.ListItemID + "'><label>" + replaceNull(e.ListItemValue) + " (" + replaceNull(e.StatusCount) + ")</lable></div></li>";
                    });

                    $("#divOrderStatus").html(strOrderStauts);
                }
            } catch (err) {
                alert("Unknown error occurred.");
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            if(IsJsonString(jqXHR.responseText)){
                var objError = $.parseJSON(jqXHR.responseText)
                alert("Error occurred: " + replaceNull(objError.ErrorMessage));
            } else {
                alert("Unknown error ocurred");
            }
        }
    });
}

function LoadShippingZone() {
    return $.ajax({
        type: "GET",
        url: '../opportunity/MassSalesFulfillmentService.svc/GetShippingZone',
        contentType: "application/json",
        success: function (data) {
            try {
                var obj = $.parseJSON(data.GetShippingZoneResult);
                if (obj != null && obj.length > 0) {
                    var strShippingZones = "";

                    obj.forEach(function (e) {
                        strShippingZones += "<option value='" + e.numListItemID.toString() + "'>" + replaceNull(e.vcData) + "</option>";
                    });

                    $("#ddlShippingZone").append(strShippingZones);
                }
            } catch (err) {
                alert("Unknown error occurred.");
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            if(IsJsonString(jqXHR.responseText)){
                var objError = $.parseJSON(jqXHR.responseText)
                alert("Error occurred: " + replaceNull(objError.ErrorMessage));
            } else {
                alert("Unknown error ocurred");
            }
        }
    });
}

function LoadCountries() {
    return $.ajax({
        type: "GET",
        url: '../opportunity/MassSalesFulfillmentService.svc/GetCountries',
        contentType: "application/json",
        success: function (data) {
            try {
                var obj = $.parseJSON(data.GetCountriesResult);
                $("#ddlFromCountry").append("<option value='0'>-- Select One --</option>");
                $("#ddlToCountry").append("<option value='0'>-- Select One --</option>");
                $("#ddlCountry").append("<option value='0'>-- Select One --</option>");

                if (obj != null && obj.length > 0) {
                    obj.forEach(function (e) {
                        $("#ddlFromCountry").append("<option value='" + e.numListItemID.toString() + "'>" + replaceNull(e.vcData) + "</option>");
                        $("#ddlToCountry").append("<option value='" + e.numListItemID.toString() + "'>" + replaceNull(e.vcData) + "</option>");
                        $("#ddlCountry").append("<option value='" + e.numListItemID.toString() + "'>" + replaceNull(e.vcData) + "</option>");
                    });
                }
            } catch (err) {
                alert("Unknown error occurred.");
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            if(IsJsonString(jqXHR.responseText)){
                var objError = $.parseJSON(jqXHR.responseText)
                alert("Error occurred: " + replaceNull(objError.ErrorMessage));
            } else {
                alert("Unknown error ocurred");
            }
        }
    });
}

function LoadStates(control, countryID, selectedValue){
    try {
        $(control).html("<option value='0'>-- Select One --</option>");

        if(countryID > 0){
            $.ajax({
                type: "POST",
                url: '../opportunity/MassSalesFulfillmentService.svc/GetStates',
                contentType: "application/json",
                dataType: "json",
                data: JSON.stringify({
                    "countryID": countryID
                }),
                success: function (data) {
                    try {
                        var obj = $.parseJSON(data.GetStatesResult);
                        if (obj != null && obj.length > 0) {
                            obj.forEach(function (e) {
                                if(selectedValue === parseInt(e.numStateID)){ 
                                    $(control).append("<option value='" + e.numStateID.toString() + "' selected='selected'>" + replaceNull(e.vcState) + "</option>");
                                } else {
                                    $(control).append("<option value='" + e.numStateID.toString() + "'>" + replaceNull(e.vcState) + "</option>");
                                }
                            });
                        }
                    } catch (err) {
                        alert("Unknown error occurred.");
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    if(IsJsonString(jqXHR.responseText)){
                        var objError = $.parseJSON(jqXHR.responseText)
                        alert("Error occurred: " + replaceNull(objError.ErrorMessage));
                    } else {
                        alert("Unknown error ocurred");
                    }
                }
            });
        }                
    } catch (e) {
        alert("Unknown error occurred while loading states.");
    }
}

function LoadWarehouses() {
    return $.ajax({
        type: "GET",
        url: '../opportunity/MassSalesFulfillmentService.svc/GetWarehouses',
        contentType: "application/json",
        success: function (data) {
            try {
                var obj = $.parseJSON(data.GetWarehousesResult);
                if (obj != null && obj.length > 0) {
                    var strWarehouses = "";
                    obj.forEach(function (e) {
                        if (e.numWareHouseID == parseInt($("[id$=hdnMSWarehouseID]").val())) {
                            strWarehouses += "<option value='" + e.numWareHouseID.toString() + "' selected>" + replaceNull(e.vcWareHouse) + "</option>";
                        } else {
                            strWarehouses += "<option value='" + e.numWareHouseID.toString() + "'>" + replaceNull(e.vcWareHouse) + "</option>";
                        }
                    });

                    $("#ddlWarehouse").append(strWarehouses);
                } else {
                    $("#ddlWarehouse").append("<option value='0'>-- Select Warehouse --</option>");
                }
            } catch (err) {
                alert("Unknown error occurred.");
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            if(IsJsonString(jqXHR.responseText)){
                var objError = $.parseJSON(jqXHR.responseText)
                alert("Error occurred: " + replaceNull(objError.ErrorMessage));
            } else {
                alert("Unknown error ocurred");
            }
        }
    });
}

function LoadContainers() {
    return $.ajax({
        type: "GET",
        url: '../opportunity/MassSalesFulfillmentService.svc/GetContainers',
        contentType: "application/json",
        success: function (data) {
            try {
                $("#hdnContainers").val(data.GetContainersResult);
            } catch (err) {
                alert("Unknown error occurred.");
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            if(IsJsonString(jqXHR.responseText)){
                var objError = $.parseJSON(jqXHR.responseText)
                alert("Error occurred: " + replaceNull(objError.ErrorMessage));
            } else {
                alert("Unknown error ocurred");
            }
        }
    });
}

function LoadMassSalesFulfillmentConfiguration() {
    return $.ajax({
        type: "GET",
        url: '../opportunity/MassSalesFulfillmentService.svc/GetMassSalesFulfillmentConfiguration',
        contentType: "application/json",
        success: function (data) {
            try {
                var obj = $.parseJSON(data.GetMassSalesFulfillmentConfigurationResult);
                        
                $("#hdnGroupByOrderPick").val(obj[0].bitGroupByOrderForPick);
                $("#hdnGroupByOrderShip").val(obj[0].bitGroupByOrderForShip);
                $("#hdnGroupByOrderInvoice").val(obj[0].bitGroupByOrderForInvoice);
                $("#hdnGroupByOrderPay").val(obj[0].bitGroupByOrderForPay);
                $("#hdnGroupByOrderClose").val(obj[0].bitGroupByOrderForClose);
                $("#hdnInvoicingType").val(obj[0].tintInvoicingType);
                $("#hdnScanField").val(obj[0].tintScanValue);
                $("#hdnDefaultShippingBizDocType").val(obj[0].numDefaultSalesShippingDoc);
                $("#ddlPendingCloseFilter").val(obj[0].tintPendingCloseFilter);
                $("#hdnPickListByOrder").val(obj[0].bitGeneratePickListByOrder)
                $("#hdnPickWithoutInventoryCheck").val(obj[0].bitPickWithoutInventoryCheck);
                $("#hdnEnablePickListMapping").val(obj[0].bitEnablePickListMapping);
                $("#hdnEnableFulfillmentBizDocMapping").val(obj[0].bitEnableFulfillmentBizDocMapping);

                if (obj[0].tintPackShipButtons == "1") {
                    $("#btnShipPackedItems").hide();
                    $("#btnPackAndShipPickedItems").show();
                } else if (obj[0].tintPackShipButtons == "2") {
                    $("#btnShipPackedItems").show();
                    $("#btnPackAndShipPickedItems").hide();
                }
            } catch (err) {
                alert("Unknown error occurred.");
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            if(IsJsonString(jqXHR.responseText)){
                var objError = $.parseJSON(jqXHR.responseText)
                alert("Error occurred: " + replaceNull(objError.ErrorMessage));
            } else {
                alert("Unknown error ocurred");
            }
        }
    });
}

function LoadBatches() {
    return $.ajax({
        type: "GET",
        url: '../opportunity/MassSalesFulfillmentService.svc/GetBatches',
        contentType: "application/json",
        success: function (data) {
            try {
                var obj = $.parseJSON(data.GetBatchesResult);
                $('#divBatch > ul').find('[id*=batch]').parent("ul").parent("li").remove()
                $("#ddlBatch > option").remove();
                $("#tblRight").html("");
                $("#ddlBatchRight > option").remove();
                $("#ddlBatchRight").append("<option value='0'>-- Select Batch --</option>");

                if (obj != null && obj.length > 0) {
                    var strBatches = "";

                    obj.forEach(function (e) {
                        strBatches += "<option value='" + e.ID.toString() + "'>" + replaceNull(e.vcName) + "</option>";
                        $('#divBatch > ul').find(' > li:nth-last-child(4)').before("<li><ul class='list-inline' style='margin-left:0px;'><li style='width:85%;line-height:28px;' id='batch" + e.ID.toString() + "' onclick='javascript:BatchSelectionChanged(this)'>" + replaceNull(e.vcName) + "</li><li  style='width:15%; font-size:20px;'><a  href='javascript:CloseBatch(" + e.ID.toString() + ")' title='Mark as complete'><i class='fa fa-check-circle'></i></a></li></ul></li>");
                    });

                    $("#ddlBatch").append(strBatches);
                    $("#ddlBatchRight").append(strBatches);
                } else {
                    $('#divBatch > ul').find(' > li:nth-last-child(4)').before("<li><a href='javascript:void'>No Batch Exists</a></li>")
                    $("#ddlBatch").append("<option value='0'>-- Select Batch --</option>");
                }
            } catch (err) {
                alert("Unknown error occurred while loading batche(s).");
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            alert("Unknown error occurred while loading batche(s).");
        }
    });
}

//function LoadShipvia(){
//    return $.ajax({
//        type: "GET",
//        url: '../opportunity/MassSalesFulfillmentService.svc/GetShipVia',
//        contentType: "application/json",
//        success: function (data) {
//            try {
//                var obj = $.parseJSON(data.GetShipViaResult);
//                if (obj != null && obj.length > 0) {
//                    var strShipVia = "<option value='0'>-- Select One --</option>";

//                    obj.forEach(function (e) {
//                        strShipVia += "<option value='" + e.numListItemID.toString() + "'>" + replaceNull(e.vcData) + "</option>";
//                    });

//                    $("#ddlShipVia").append(strShipVia);
//                } else {
//                    $("#ddlShipVia").append("<option value='0'>-- Select One --</option>");
//                }
//            } catch (err) {
//                alert("Unknown error occurred while loading Ship-Via.");
//            }
//        },
//        error: function (jqXHR, textStatus, errorThrown) {
//            if(IsJsonString(jqXHR.responseText)){
//                var objError = $.parseJSON(jqXHR.responseText)
//                alert("Error occurred: " + replaceNull(objError.ErrorMessage));
//            } else {
//                alert("Unknown error ocurred");
//            }
//        }
//    });
//}

function ShiViaChanged(ddlShipVia){
    var id = $(ddlShipVia).attr("id");
    var ddlShipService = $("[id='" + id.replace("ddlShipVia","ddlShipService") + "']");
    LoadShipService(ddlShipService, $(ddlShipVia).val())

    $("[id='" + id.replace("ddlShipVia","divOrder") + "']").find(".package-type").each(function(index,ddlPackageType){
        $(ddlPackageType).html("<option value='0'>-- Package Type --</option>");
        if(parseInt($(ddlShipVia).val()) === 91){
            $(ddlPackageType).append("<option value='2'>FedEx Letter</option>");
            $(ddlPackageType).append("<option value='11'>FedEx Pak</option>");
            $(ddlPackageType).append("<option value='12'>FedEx Box</option>");
            $(ddlPackageType).append("<option value='13'>FedEx 10kg Box</option>");
            $(ddlPackageType).append("<option value='14'>FedEx 25kg Box</option>");
            $(ddlPackageType).append("<option value='28'>FedEx Tube</option>");
            $(ddlPackageType).append("<option value='31' selected='true'>Your Packaging</option>");
        } else if (parseInt($(ddlShipVia).val()) === 88){
            $(ddlPackageType).append("<option value='0'>None</option>");
            $(ddlPackageType).append("<option value='2'>UPS letter</option>");
            $(ddlPackageType).append("<option value='11'>UPS Pak</option>");
            $(ddlPackageType).append("<option value='12'>UPS Box</option>");
            $(ddlPackageType).append("<option value='13'>UPS 10kg Box</option>");
            $(ddlPackageType).append("<option value='14'>UPS 25kg Box</option>");
            $(ddlPackageType).append("<option value='15'>UPS Small Express Box</option>");
            $(ddlPackageType).append("<option value='16'>UPS Medium Express Box</option>");
            $(ddlPackageType).append("<option value='17'>UPS Large Express Box</option>");
            $(ddlPackageType).append("<option value='28'>UPS Tube</option>");
            $(ddlPackageType).append("<option value='30'>UPS Pallet</option>");
            $(ddlPackageType).append("<option value='31' selected='true'>Your Packaging</option>");
            $(ddlPackageType).append("<option value='33'>UPS Flats</option>");
            $(ddlPackageType).append("<option value='34'>UPS Parcels</option>");
            $(ddlPackageType).append("<option value='35'>UPS BPM</option>");
            $(ddlPackageType).append("<option value='36'>UPS First Class</option>");
            $(ddlPackageType).append("<option value='37'>UPS Priority</option>");
            $(ddlPackageType).append("<option value='38'>UPS Machinables</option>");
            $(ddlPackageType).append("<option value='39'>UPS Irregulars</option>");
            $(ddlPackageType).append("<option value='40'>UPS Parcel Post</option>");
            $(ddlPackageType).append("<option value='41'>UPS BPM Parcel</option>");
            $(ddlPackageType).append("<option value='42'>UPS Media Mail</option>");
            $(ddlPackageType).append("<option value='43'>UPS BPM Flat</option>");
            $(ddlPackageType).append("<option value='44'>UPS Standard Flat</option>");
        } else if (parseInt($(ddlShipVia).val()) === 90){
            $(ddlPackageType).append("<option value='1'>USPS Postcards</option>");
            $(ddlPackageType).append("<option value='2'>USPS Letter</option>");
            $(ddlPackageType).append("<option value='3'>USPS Large Envelope</option>");
            $(ddlPackageType).append("<option value='4'>USPS Flat Rate Envelope</option>");
            $(ddlPackageType).append("<option value='5'>USPS Flat Rate Legal Envelope</option>");
            $(ddlPackageType).append("<option value='6'>USPS Flat Rate Padded Envelope</option>");
            $(ddlPackageType).append("<option value='7'>USPS Flat Rate GiftCard Envelope</option>");
            $(ddlPackageType).append("<option value='8'>USPS Flat Rate Window Envelope</option>");
            $(ddlPackageType).append("<option value='9'>USPS Flat Rate Cardboard Envelope</option>");
            $(ddlPackageType).append("<option value='10'>USPS Small Flat Rate Envelope</option>");
            $(ddlPackageType).append("<option value='18'>USPS Flat Rate Box</option>");
            $(ddlPackageType).append("<option value='19'>USPS Small Flat Rate Box</option>");
            $(ddlPackageType).append("<option value='20'>USPS Medium Flat Rate Box</option>");
            $(ddlPackageType).append("<option value='21'>USPS Large Flat Rate Box</option>");
            $(ddlPackageType).append("<option value='22'>USPS DVD Flat Rate Box</option>");
            $(ddlPackageType).append("<option value='23'>USPS Large Video Flat Rate Box</option>");
            $(ddlPackageType).append("<option value='24'>USPS Regional Rate Box A</option>");
            $(ddlPackageType).append("<option value='25'>USPS Regional Rate Box B</option>");
            $(ddlPackageType).append("<option value='26'>USPS Rectangular</option>");
            $(ddlPackageType).append("<option value='27'>USPS Non Rectangular</option>");
            $(ddlPackageType).append("<option value='29'>USPS Matter For The Blind</option>");
            $(ddlPackageType).append("<option value='31' selected='true'>Your Packaging</option>");
            $(ddlPackageType).append("<option value='45'>USPS Regional Rate Box C</option>");
        }
    });
}

function LoadShipService(ddlShipService, shipViaID){
    try {
        $(ddlShipService).find("option").remove();
        var options = "<option value='0'>Ship-service</option>";

        if(shipViaID == 91){
            options += "<option value='10'>FedEx Priority Overnight</option>";
            options += "<option value='11'>FedEx Standard Overnight</option>";
            options += "<option value='12'>FedEx Overnight</option>";
            options += "<option value='13'>FedEx 2nd Day</option>";
            options += "<option value='14'>FedEx Express Saver</option>";
            options += "<option value='15' selected='selected'>FedEx Ground</option>";
            options += "<option value='16'>FedEx Ground Home Delivery</option>";
            options += "<option value='17'>FedEx 1 Day Freight</option>";
            options += "<option value='18'>FedEx 2 Day Freight</option>";
            options += "<option value='19'>FedEx 3 Day Freight</option>";
            options += "<option value='20'>FedEx International Priority</option>";
            options += "<option value='21'>FedEx International Priority Distribution</option>";
            options += "<option value='22'>FedEx International Economy</option>";
            options += "<option value='23'>FedEx International Economy Distribution</option>";
            options += "<option value='24'>FedEx International First</option>";
            options += "<option value='25'>FedEx International Priority Freight</option>";
            options += "<option value='26'>FedEx International Economy Freight</option>";
            options += "<option value='27'>FedEx International Distribution Freight</option>";
            options += "<option value='28'>FedEx Europe International Priority</option>";
        } else if (shipViaID == 88){
            options += "<option value='40'>UPS Next Day Air</option>";
            options += "<option value='42'>UPS 2nd Day Air</option>";
            options += "<option value='43' selected='selected'>UPS Ground</option>";
            options += "<option value='48'>UPS 3Day Select</option>";
            options += "<option value='49'>UPS Next Day Air Saver</option>";
            options += "<option value='50'>UPS Saver</option>";
            options += "<option value='51'>UPS Next Day Air Early A.M.</option>";
            options += "<option value='55'>UPS 2nd Day Air AM</option>";
        } else if (shipViaID == 90){
            options += "<option value='70'>USPS Express</option>";
            options += "<option value='71'>USPS First Class</option>";
            options += "<option value='72' selected='selected'>USPS Priority</option>";
            options += "<option value='73'>USPS Parcel Post</option>";
            options += "<option value='74'>USPS Bound Printed Matter</option>";
            options += "<option value='75'>USPS Media</option>";
            options += "<option value='76'>USPS Library</option>";
        }

        $(ddlShipService).append(options);
    } catch (e) {
        alert('Unknown error occurred while loading Ship-Service');
    }
}

function LoadRecords(pageIndex) {
    GetPersistedHeaderSearchFilters();
    $("#tblRight").html("");
    $("#divGridRightPack").html("");
    $("#btnGetShippingRate").show();
    $("#btnCommitShippingRate").hide();
    $("#btnCancelShippingRate").hide();
    $("#hdnSelectedRecordsForRightPanePicking").val("");
    
    return $.ajax({
        type: "POST",
        url: '../opportunity/MassSalesFulfillmentService.svc/GetRecords',
        contentType: "application/json",
        dataType: "json",
        data: JSON.stringify({
            "viewID": parseInt($("#hdnViewID").val())
            , "warehouseID": parseInt($("#ddlWarehouse").val())
            , "isGroupByOrder": $("#chkGroupBy").is(":checked")
            , "isRemoveBO": $("#chkBackOrder").is(":checked")
            , "shippingZone": parseInt($("#ddlShippingZone").val())
            , "isIncludeSearch": $("#rbInclude").is(":checked")
            , "selectedOrderStatus": GetSelectedOrderStatus()
            , "filterBy": parseInt($("#ddlFiterBy").val())
            , "selectedFilterValue": GetSelectedFilterValues()
            , "customSearchValue": GetHeaderSearchString()
            , "pageIndex": (pageIndex || 1)
            , "sortColumn": $("#hdnSortColumn").val()
            , "sortOrder": $("#hdnSortOrder").val()
            , "batchID": parseInt($("#hdnBatchID").val())
            , "packingViewMode": (parseInt($("#hdnViewID").val()) == 2 ? parseInt($("[id$=hdnPackLastSubViewMode]").val()) : (parseInt($("#hdnViewID").val()) == 6 ? parseInt($("[id$=hdnPrintLastSubViewMode]").val()) : 0))
            , "printBizDocViewMode": parseInt($("#hdnPrintBizDocViewMode").val())
            , "pendingCloseFilter": (parseInt($("#hdnViewID").val()) === 6 ? parseInt($("input:radio[name='rdOpenCloseAll']:checked").val()) : parseInt($("#ddlPendingCloseFilter").val()))
        }),
        success: function (data) {
            try {
                var obj = $.parseJSON(data.GetRecordsResult);
                if (obj.LeftPaneFields.length === 0) {
                    alert("Columns are not configured. Please first configure columns to display.");
                } else {
                    var leftPaneColumns = $.parseJSON(obj.LeftPaneFields);
                    var records = $.parseJSON(obj.Records);
                    var searchConditions = $.parseJSON($("#hdnHeaderSearch").val());
                    var noWrapColumns = [];
                    //var noWrapColumns = [3, 96, 101, 203, 233, 251, 273, 281, 294, 50928];
                    $("#hdnSortColumn").val(obj.SortColumn);
                    $("#hdnSortOrder").val(obj.SortOrder);
                    var data = "<table id='tblMain' class='table table-bordered table-striped'>";

                    //Header Row
                    data += "<tr>";

                    if (leftPaneColumns.length > 0) {
                        data += "<th style='width:25px;'><input type='checkbox' class='chkSelectAll' onChange='SelectAllChanged(this);' /></th>";
                    }

                    var i = 0;
                    arrFieldConfig = new Array();
                    leftPaneColumns.forEach(function (e) {
                        var fieldMessage = e.bitFieldMessage ? replaceNull(e.vcFieldMessage).replace("{$FieldName}", replaceNull(e.vcFieldName)) : replaceNull(e.vcFieldName).replace("'", "\'");
                        arrFieldConfig[i] = new InlineEditValidation(e.numFieldID,e.bitCustomField.toString(),e.bitIsRequired,e.bitIsNumeric,e.bitIsAlphaNumeric,e.bitIsEmail,e.bitIsLengthValidation,e.intMaxLength,e.intMinLength,replaceNull(fieldMessage));
                        i++;

                        if (e.bitAllowFiltering) {
                            if (e.bitAllowSorting) {
                                data += "<th id='" + e.ID + "' style='width:" + (e.intColumnWidth || 100) + "px;'><a href=\"javascript:SortData('" + (replaceNull(e.vcLookBackTableName) + "." + replaceNull(e.vcOrigDbColumnName)) + "');\">" + replaceNull(e.vcFieldName) + ((replaceNull(e.vcLookBackTableName) + "." + replaceNull(e.vcOrigDbColumnName)) == $("#hdnSortColumn").val() ? ($("#hdnSortOrder").val() == "DESC" ? "&nbsp;&nbsp;<i class='fa fa-caret-down'></i>" : "&nbsp;&nbsp;<i class='fa fa-caret-up'></i>") : "") + "</a><br />"
                            } else {
                                data += "<th id='" + e.ID + "' style='width:" + (e.intColumnWidth || 100) + "px;'>" + replaceNull(e.vcFieldName) + "<br />"
                            }
                            var controlID = "hs~" + replaceNull(e.vcLookBackTableName) + "~" + replaceNull(e.vcOrigDbColumnName) + "~" + replaceNull(e.vcAssociatedControlType) + "~" + (e.bitCustomField || "0") + "~" + (e.numListID || 0) + "~" + replaceNull(e.vcListItemType) + "~" + e.numFieldID;
                            if (e.vcAssociatedControlType === "TextBox") {
                                var vcValue = "";

                                if (searchConditions.length > 0 && $.grep(searchConditions, function (f) { return f.id == controlID; }).length > 0) {
                                    vcValue = $.grep(searchConditions, function (f) { return f.id == controlID; })[0].searchValue;
                                }
                                data += "<input type='text' class='form-control' id='" + controlID + "' onkeydown='return SearchRecords(this,false);' value='" + vcValue + "' />"
                            } else if (e.vcAssociatedControlType === "SelectBox") {
                                var vcValue = "";

                                if (searchConditions.length > 0 && $.grep(searchConditions, function (f) { return f.id == controlID; }).length > 0) {
                                    vcValue = $.grep(searchConditions, function (f) { return f.id == controlID; })[0].searchValue;
                                }

                                if(e.numFieldID != 97 && e.numFieldID != 101){
                                    data += "<select class='form-control' id='" + controlID + "' onChange='return SearchRecords(this,true);' selectedValue='" + vcValue + "'>";
                                    data += "<option value='0'>-- Select One --</option>";
                                } else {
                                    data += "<select class='form-control' id='" + controlID + "' onChange='return SearchRecords(this,true);' selectedValue='" + vcValue + "' multiple='multiple'>";
                                }
                                data += "</select>";
                            } else if (e.vcAssociatedControlType === "DateField") {
                                var vcValueFrom = "";
                                var vcValueTo = "";
                                var selectedDateRange = "";

                                if (searchConditions.length > 0 && $.grep(searchConditions, function (f) { return f.id == controlID; }).length > 0) {
                                    vcValueFrom = $.grep(searchConditions, function (f) { return f.id == controlID; })[0].searchValueFrom;
                                    vcValueTo = $.grep(searchConditions, function (f) { return f.id == controlID; })[0].searchValueTo;
                                }

                                if (vcValueFrom != "") {
                                    selectedDateRange += vcValueFrom;
                                }
                                if (vcValueTo != "") {
                                    selectedDateRange += ("#" + vcValueTo);
                                }

                                data += "<input type='text' style='width:105px' class='FromDateRangePicker' id='" + controlID + "~from' value='" + vcValueFrom + "' placeholder='From - To' DateRange='" + selectedDateRange + "' /><br/><input type='text' style='width:105px' class='ToDateRangePicker' id='" + controlID + "~to' value='" + vcValueTo + "' />"
                            }

                            data += "</th>";
                        } else {
                            if (e.bitAllowSorting) {
                                data += "<th id='" + e.ID + "' style='width:" + (e.intColumnWidth || 100) + "px;'><a href=\"javascript:SortData('" + (replaceNull(e.vcLookBackTableName) + "." + replaceNull(e.vcOrigDbColumnName)) + "');\">" + replaceNull(e.vcFieldName) + ((replaceNull(e.vcLookBackTableName) + "." + replaceNull(e.vcOrigDbColumnName)) == $("#hdnSortColumn").val() ? ($("#hdnSortOrder").val() == "DESC" ? "&nbsp;&nbsp;<i class='fa fa-caret-down'></i>" : "&nbsp;&nbsp;<i class='fa fa-caret-up'></i>") : "") + "</a><br />"
                            } else {
                                data += "<th id='" + e.ID + "' style='width:" + (e.intColumnWidth || 100) + "px;'>" + replaceNull(e.vcFieldName) + "<br />"
                            }
                        }
                    });
                    data += "</tr>";

                    //Data Rows
                    var rowIndex  = 0;
                    records.forEach(function (n) {
                        rowIndex += 1;
                        data += "<tr>";
                        data += "<td style='white-space:nowrap'>";
                        if(!JSON.parse(n["bitRequiredWarehouseCorrection"])){
                            data += "<input type='checkbox' class='chkSelect' onChange='RecordSelected(this);' />";
                        }
                        data += "<input type='hidden' id='hdnOppID' value='" + (n.numOppID || 0) + "' />",
                        data += "<input type='hidden' id='hdnOppItemID' value='" + (n.numOppItemID || 0) + "' />";
                        data += "<input type='hidden' id='hdnOppBizDocID' value='" + (n.numOppBizDocID || 0) + "' />";
                        data += "<input type='hidden' id='hdnBizDocName' value='" + replaceNull(n.vcBizDocID) + "' />";
                        data += "<input type='hidden' id='hdnAmountToPay' value='" + (n.monAmountToPay || 0) + "' />";
                        data += "<input type='hidden' id='hdnRemainingQty' value='" + (n.numRemainingQty || 0) + "' />";
                        data += "<input type='hidden' id='hdnOppShipVia' value='" + (n.numOppShipVia || 0) + "' />";
                        data += "<input type='hidden' id='hdnOppShipService' value='" + (n.numOppShipService || 0) + "' />";
                        data += "<input type='hidden' id='hdnShipRate' value='0' />";
                        data += "<input type='hidden' id='hdnShipDescription' value='' />";
                        data += "<input type='hidden' id='hdnExpectedDate' value='" + replaceNull(n.dtExpectedDateOrig).replace("T00:00:00","") + "' />";
                        data += "<input type='hidden' id='hdnIsShipRateFetched' value='false' />";
                        data += "<input type='hidden' id='hdnLinkingBizDocs' value='" + replaceNull(n.vcLinkingBizDocs) + "' />";
                        data += "<input type='hidden' id='hdnParentBizDoc" + rowIndex + "' value='0' />";
                        data += "<input type='hidden' id='hdnPreselectedParentBizDoc" + rowIndex + "' value='0' />";
                        data += "<input type='hidden' id='hdnShipByDate' value='" + replaceNull(n.dtShipByDate).replace("T00:00:00","") + "' />";
                        data += "</td>";
                                
                        leftPaneColumns.forEach(function (e) {
                            if ($("#hdnViewID").val() === "2" && $("[id$=hdnPackLastSubViewMode]").val() === "2" && e.vcOrigDbColumnName === "numQtyPicked") {
                                data += "<td" + ($.inArray(e.numFieldID, noWrapColumns) > -1 ? " class='nowrap'" : "") + ($("#chkGroupBy").is(":checked") ? " style='background-color:" + replaceNull(n[e.vcOrigDbColumnName]) + "'" : "") + "><span id='lblQtyPickedNotPacked'>" + ($("#chkGroupBy").is(":checked") ? "" : replaceNull(n[e.vcOrigDbColumnName])) + "</span></td>";
                            } else if ($("#hdnViewID").val() === "2" && $("[id$=hdnPackLastSubViewMode]").val() === "2" && e.vcOrigDbColumnName === "numQtyPacked") {
                                data += "<td" + ($.inArray(e.numFieldID, noWrapColumns) > -1 ? " class='nowrap'" : "") + ($("#chkGroupBy").is(":checked") ? " style='background-color:" + replaceNull(n[e.vcOrigDbColumnName]) + "'" : "") +"><span id='lblQtyPackedNotShipped'>" + ($("#chkGroupBy").is(":checked") ? "" : replaceNull(n[e.vcOrigDbColumnName])) + "</span></td>";
                            } else if ($("#hdnViewID").val() === "2" && $("[id$=hdnPackLastSubViewMode]").val() === "2" && e.vcOrigDbColumnName === "numQtyShipped") {
                                data += "<td" + ($.inArray(e.numFieldID, noWrapColumns) > -1 ? " class='nowrap'" : "") + ($("#chkGroupBy").is(":checked") ? " style='background-color:" + replaceNull(n[e.vcOrigDbColumnName]) + "'" : "") +  ">" + ($("#chkGroupBy").is(":checked") ? "" : replaceNull(n[e.vcOrigDbColumnName])) + "</td>";
                            } else if (e.vcOrigDbColumnName === "numRemainingQty" && $("#hdnViewID").val() === "3"){
                                data += "<td" + ($.inArray(e.numFieldID, noWrapColumns) > -1 ? " class='nowrap'" : "") + "><input class='form-control' id='txtRemainingQty' value='" + replaceNull(n[e.vcOrigDbColumnName]) + "' /></td>";
                            } else if(e.vcOrigDbColumnName === "numShipRate"){
                                data += "<td" + ($.inArray(e.numFieldID, noWrapColumns) > -1 ? " class='nowrap'" : "") + "><span id='lblShipRate'>" + replaceNull(n[e.vcOrigDbColumnName]) + "</span></td>";
                            } else if(e.vcOrigDbColumnName === "dtAnticipatedDelivery"){
                                data += "<td class='nowrap tdDeliverBy'></td>";
                            } else if(e.vcOrigDbColumnName === "vcPoppName" && (n.numOppID || 0) > 0){
                                data += "<td" + ($.inArray(e.numFieldID, noWrapColumns) > -1 ? " class='nowrap'" : "") + "><a href='../opportunity/frmOpportunities.aspx?opId=" + n.numOppID.toString() + "'>" + replaceNull(n[e.vcOrigDbColumnName]) + "</a>" + (replaceNull(n["vcShippingLabel"]).length > 0 ? (" " + $.parseHTML(replaceNull(n["vcShippingLabel"]).replace(/"/g, "'"))[0].data) : "")  + "</td>";
                            } else if(e.vcOrigDbColumnName === "vcBizDocID" && (n.numOppID || 0) > 0 && (n.numOppBizDocID || 0) > 0){
                                data += "<td" + ($.inArray(e.numFieldID, noWrapColumns) > -1 ? " class='nowrap'" : "") + "><a onclick='return OpenRecentBizInvoice(" + n.numOppID.toString() + "," + n.numOppBizDocID.toString() + ",0)' href='#'>" + replaceNull(n[e.vcOrigDbColumnName]) + "</a></td>";
                            } else if(e.bitCustomField) {
                                var controlClass = "";
                                if (e.vcAssociatedControlType === "TextBox" || e.vcAssociatedControlType === "Website" || e.vcAssociatedControlType === "Email") {
                                    controlClass="click";
                                } else if (e.vcAssociatedControlType === "SelectBox") {
                                    controlClass="editable_select";
                                } else if (e.vcAssociatedControlType === "DateField") {
                                    controlClass="editable_DateField";
                                } else if (e.vcAssociatedControlType === "TextArea") {
                                    controlClass="editable_textarea";
                                } else if (e.vcAssociatedControlType === "CheckBox") {
                                    controlClass="editable_CheckBox";
                                }

                                if((e.Grp_id || 0) == 5){
                                    if((n.numOppItemID || 0) > 0){
                                        data += "<td id='OppItems~" + e.numFieldID + "~" + e.bitCustomField + "~" + (n.numOppItemID || 0) + "~" + (n.numTerID || 0) + "~0~" + (n.numDivisionID || 0) + "'" + ($.inArray(e.numFieldID, noWrapColumns) > -1 ? " class='" + controlClass + " nowrap'" : " class='" + controlClass + "'") + ">" + replaceNull(n["Cust" + e.numFieldID]) + "</td>";
                                    } else {
                                        data += "<td>" + replaceNull(n["Cust" + e.numFieldID]) + "</td>";
                                    }
                                } else {
                                    data += "<td id='Opp~" + e.numFieldID + "~" + e.bitCustomField + "~" + (n.numOppID || 0) + "~" + (n.numTerID || 0) + "~0~" + (n.numDivisionID || 0) + "'" + ($.inArray(e.numFieldID, noWrapColumns) > -1 ? " class='" + controlClass + " nowrap'" : " class='" + controlClass + "'") + ">" + replaceNull(n["Cust" + e.numFieldID]) + "</td>";
                                }
                            } else {
                                if (e.vcOrigDbColumnName === "vcWareHouse") {
                                    var controlClass = "";

                                    if(JSON.parse(n["bitRequiredWarehouseCorrection"])){
                                        controlClass="editable_select";
                                    }
                                    data += "<td id='OppItems~" + e.numFieldID + "~" + e.bitCustomField + "~" + (n.numOppItemID || 0) + "~" + (n.numTerID || 0) + "~0~" + (n.numDivisionID || 0) + "~" + (n.numOppID || 0) + "' class='nowrap" + (controlClass != "" ? (" " + controlClass) : "") + "'" + ">" + replaceNull(n[e.vcOrigDbColumnName]) + "</td>";
                                } else {
                                    data += "<td" + ($.inArray(e.numFieldID, noWrapColumns) > -1 ? " class='nowrap" + (controlClass != "" ? (" " + controlClass) : "") + "'" : "") + ">" + replaceNull(n[e.vcOrigDbColumnName]).replace("T00:00:00","") + "</td>";
                                }

                                
                            }
                        });
                        data += "</tr>";
                    });


                    data += "</table>";

                    $("#divMainGrid").html(data);

                    $(".FromDateRangePicker").each(function (index, object) {
                        var dateRange = $(this).attr("daterange");
                        var dateArray = dateRange.split("#");
                        var start = moment().subtract(29, 'days').format('MM/DD/YYYY');
                        var end = moment().format('MM/DD/YYYY');
                        if (dateArray.length == 2) {
                            start = dateArray[0];
                            end = dateArray[1];
                        }
                        $(this).daterangepicker({
                            "autoApply": true,
                            "showDropdowns": true,
                            "autoUpdateInput": false,
                            "startDate": start,
                            "endDate": end
                        }, DateRangeChanged);
                    });

                    //$("[id$='~from']").datepicker({
                    //    dateFormat: 'mm/dd/yy', onSelect: function (dateText, inst) {
                    //        LoadRecordsWithPagination();
                    //    }
                    //});
                    //$("[id$='~to']").datepicker({
                    //    dateFormat: 'mm/dd/yy', onSelect: function (dateText, inst) {
                    //        LoadRecordsWithPagination();
                    //    }
                    //});
                    
                    $("#tblMain tr:first select").each(function (index, select) {
                        var arrSelectedItems = [];

                        if ($(select).attr("selectedValue") != null && $(select).attr("selectedValue").length > 0) {
                            arrSelectedItems = $(select).attr("selectedValue").split(",");
                        }

                        if($(select).attr("id").split("~").length >= 3 && $(select).attr("id").split("~")[2] === "dtAnticipatedDelivery"){
                            $(select).append("<option value='1'>On Time</option>");
                            $(select).append("<option value='2'>Late</option>");
                        } else if ($(select).attr("id").split("~").length >= 8 && $(select).attr("id").split("~")[7] == 537) {
                            $(select).append("<option value='1' " + (arrSelectedItems.indexOf("1") != -1 ? "selected" : "") + ">Not Applicable</option>");
                            $(select).append("<option value='2' " + (arrSelectedItems.indexOf("2") != -1 ? "selected" : "") + ">Shipped</option>");
                            $(select).append("<option value='3' " + (arrSelectedItems.indexOf("3") != -1 ? "selected" : "") + ">Back Order</option>");
                            $(select).append("<option value='4' " + (arrSelectedItems.indexOf("4") != -1 ? "selected" : "") + ">Shippable</option>");
                        } else {
                            $.ajax({
                                type: "POST",
                                url: '../Common/Common.asmx/GetAdvancedSearchListDetails',
                                contentType: "application/json",
                                dataType: "json",
                                data: JSON.stringify({
                                    "numListID": ($(select).attr("id").split("~")[5] || 0),
                                    "vcListItemType":replaceNull($(select).attr("id").split("~")[6]),
                                    "searchText": "",
                                    "pageIndex": 1,
                                    "pageSize": 1000000
                                }),
                                success: function (data) {
                                    try {
                                        var listItems;

                                        if (data.hasOwnProperty("d")) {
                                            if(IsJsonString(data.d)){
                                                data = $.parseJSON(data.d)
                                            } else {
                                                data = null;
                                            }
                                        }
                                        else{
                                            if(IsJsonString(data.d)){
                                                data = $.parseJSON(data.d)
                                            } else {
                                                data = null;
                                            }
                                        }

                                        if(data != null){
                                            listItems = $.parseJSON(data.results || null);

                                            if(listItems != null) {
                                                listItems.forEach(function(li){
                                                    if(($(select).attr("id").split("~")[7] || 0) == 97){
                                                        if(!li.id.endsWith("~3")){
                                                            $(select).append("<option value='" + li.id + "' " + (arrSelectedItems.indexOf(li.id.toString()) != -1 ? "selected" : "") + ">" + replaceNull(li.text) + "</option>");
                                                        }
                                                    } if(($(select).attr("id").split("~")[7] || 0) == 101){
                                                        $(select).append("<option value='" + li.id + "' " + (arrSelectedItems.indexOf(li.id.toString()) != -1 ? "selected" : "") + ">" + replaceNull(li.text) + "</option>");
                                                    } else {
                                                        $(select).append("<option value='" + li.id + "' " + (li.id.toString() === $(select).attr("selectedValue") ? "selected" : "") + ">" + replaceNull(li.text) + "</option>");
                                                    }                                                
                                                });
                                            }

                                            if(($(select).attr("id").split("~")[7] || 0) == 97 || ($(select).attr("id").split("~")[7] || 0) == 101){
                                                $(select).multiselect({includeSelectAllOption: true});
                                            }
                                        }
                                    } catch (err) {
                                           
                                    }
                                }
                            }); 
                        }                      
                    });

                    BindInlineEdit();

                    $("table[id$=tblMain]").colResizable({
                        //fixed: false,
                        liveDrag: true,
                        gripInnerHtml: "<div class='grip2'></div>",
                        draggingClass: "dragging",
                        resizeMode:'overflow',
                        onResize: onGridColumnResized,
                        postbackSafe:true,
                        partialRefresh: true,
                    });

                    if (records.length > 0) {
                        $("#liRecordDisplay").html("<b>" + ((pageIndex - 1) * recordsPerPage + 1) + " to " + (records.length >= recordsPerPage ? ((recordsPerPage * pageIndex) + " of many") : ((pageIndex > 1 ? ((pageIndex - 1) * recordsPerPage) : 0) + records.length)) + (pageIndex > 1 ? "&nbsp;<a class='btn btn-default' onclick='LoadRecords(" + (pageIndex - 1).toString() + ");'>Previous</a>" : "") + (records.length >= recordsPerPage ? "&nbsp;<a class='btn btn-default' onclick='LoadRecords(" + (pageIndex + 1).toString() + ");'>Next</a>" : "") +  "</b>");
                    } else {
                        if (pageIndex == 1) {
                            $("#liRecordDisplay").html("<b>0 to 0 of 0</b>");
                        } else {
                            $("#liRecordDisplay").html("<b>No more records</b>" + "&nbsp;<a class='btn btn-default' onclick='LoadRecords(" + (pageIndex - 1).toString() + ");'>Previous</a>");
                        }
                    }
                }
            } catch (err) {
                alert("Unknown error occurred.");
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            if(IsJsonString(jqXHR.responseText)){
                var objError = $.parseJSON(jqXHR.responseText)

                if (objError.ErrorMessage === "DEFAULT_SHIPPING_BIZDOC_NOT_CONFIGURED") {
                    alert("Go global settings -> Accounting and Select Sales BizDoc responsible for Shipping Labels & Trackings #.");
                } else {
                    alert("Error occurred: " + replaceNull(objError.ErrorMessage));
                }
            } else {
                alert("Unknown error ocurred");
            }
        }
    });
}

function DateRangeChanged(start, end) {
    document.getElementById("" + this.element[0].id + "").setAttribute("value", start.format('MM/DD/YYYY'))
    var FromId = this.element[0].id;
    FromId = FromId.replace("from", "to");

    document.getElementById("" + FromId + "").setAttribute("value", end.format('MM/DD/YYYY'))
    PersistHeaderSearchFilters();
    LoadRecordsWithPagination();
}

function LoadRecordsWithPagination() {
   LoadRecords(1);
}

function GetSelectedOrderStatus() {
    try {
        var selectedOrderStatus = "";

        $("#divOrderStatus input[type='checkbox']").each(function () {
            if ($(this).is(":checked")) {
                selectedOrderStatus = selectedOrderStatus + (selectedOrderStatus.length > 0, ",", "") + $(this).attr("id").replace("chkOrderStatus", "")
            }
        });

        return selectedOrderStatus;
    } catch (e) {
        alert("Unknown error occurred.");
    }
}

function GetSelectedFilterValues() {
    try {
        var selectedFilterValues = "";

        $("#divFilterValueOptions input[type='checkbox']").each(function () {
            if ($(this).is(":checked")) {
                selectedFilterValues = selectedFilterValues + (selectedFilterValues.length > 0 ? "," : "") + $(this).attr("id").replace("chkFilterValue", "")
            }
        });

        return selectedFilterValues;
    } catch (e) {
        alert("Unknown error occurred.");
    }
}

function filterSelectionChanged() {
    try {
        var selected = 0;

        $("#divFilterValueOptions input[type='checkbox']").each(function () {
            if ($(this).is(":checked")) {
                selected++;
            }
        });

        $("#dropdownMenuFilter").text(selected > 0 ? (selected.toString() + " Selected") : "None Selected");
    } catch (e) {
        alert("Unknown error occurred.");
    }
}

function SearchRecords(control,isSelectChange) {
    try {
        if($(control).attr("id").split("~").length >= 3 && $(control).attr("id").split("~")[2] == "dtAnticipatedDelivery"){
            $("#divMainGrid > table > tbody > tr").not(":first").each(function (index, e) {
                if($(control).val() === "0"){
                    $(e).show();
                } else {
                    if($(e).find("td.tdDeliverBy").length > 0){
                        if($(control).val() === "1"){
                            if($(e).find("td.tdDeliverBy").html().indexOf("(On Time)") != -1){
                                $(e).show();
                            } else {
                                $(e).hide();
                            }
                        } else if ($(control).val() === "2"){
                            if($(e).find("td.tdDeliverBy").html().indexOf("(Late)") != -1){
                                $(e).show();
                            } else {
                                $(e).hide();
                            }
                        }                        
                    } 
                }
                
            });
        } else if (event.keyCode == 13 || isSelectChange) {
            PersistHeaderSearchFilters();
            LoadRecordsWithPagination();
            return false;
        } else {
            return true;
        }
    } catch (e) {
        alert("Unknown error occurred.");
    }
}

function GetHeaderSearchString() {
    try {
        var searchConditions = JSON.parse($("#hdnHeaderSearch").val());

        var searchValue = "";
        if (searchConditions.length > 0) {
            searchConditions.forEach(function (e) {
                if (e.controlType === "TextBox") {
                    searchValue += (" AND " + e.searchField + " ilike '%" + e.searchValue + "%'")
                } else if (e.controlType === "SelectBox") {
                    if (e.id.split("~")[7] == 97) {
                        if (e.searchValue != null && e.searchValue.length > 0) {
                            searchValue += (" AND (")
                            var i = 0;
                            e.searchValue.forEach(function (item) {
                                searchValue += ((i === 0 ? "" : "OR ") + "OpportunityMaster.tintSource = " + item.split("~")[0] + " AND OpportunityMaster.tintSourceType = " + item.split("~")[1]);
                                i += 1;
                            });
                            searchValue += (")")
                        }
                    } else if (e.id.split("~")[7] == 101) {
                        if (e.searchValue != null && e.searchValue.length > 0) {
                            searchValue += (" AND (")
                            var i = 0;
                            e.searchValue.forEach(function (item) {
                                searchValue += ((i === 0 ? "" : "OR ") + e.searchField + " = " + item);
                                i += 1;
                            });
                            searchValue += (")")
                        }
                    } else if (parseInt(e.searchValue) > 0) {
                        searchValue += (" AND " + e.searchField + " = " + e.searchValue)
                    }
                } else if (e.controlType === "DateField") {
                    if (e.searchValueFrom != "" && e.searchValueTo != "") {
                        searchValue += (" AND CAST(" + e.searchField + " AS DATE) BETWEEN '" + e.searchValueFrom + "' AND '" + e.searchValueTo + "'")
                    } else if (e.searchValueFrom != "") {
                        searchValue += (" AND CAST(" + e.searchField + " AS DATE) >= '" + e.searchValueFrom + "'")
                    } else if (e.searchValueTo != "") {
                        searchValue += (" AND CAST(" + e.searchField + "  AS DATE)<= '" + e.searchValueTo + "'")
                    }
                }
            });
        }

        return searchValue;
    } catch (e) {
        alert("Unknown error occurred.");
    }
}


function PersistHeaderSearchFilters() {
    try {
        var searchConditions = [];
        var searchControls = $("#divMainGrid table tr th input[type='text'], #divMainGrid table tr th select")

        searchControls.each(function (index, e) {
            if ($(e).attr("id") != null && $(e).val() != "") {
                if ($(e).attr("id").split("~")[3] === "DateField") {
                    var result = $.grep(searchConditions, function (f) { return f.id == $(e).attr("id").replace("~from", "").replace("~to", ""); });
                    if (result.length == 0) {
                        searchConditions.push({ "id": $(e).attr("id").replace("~from", "").replace("~to", ""), "controlType": $(e).attr("id").split("~")[3], "searchField": ($(e).attr("id").split("~")[1] + "." + $(e).attr("id").split("~")[2]), "searchValue": $(e).val(), "searchValueFrom": $(e).attr("id").endsWith("~from") ? $(e).val() : "", "searchValueTo": $(e).attr("id").endsWith("~to") ? $(e).val() : "" });
                    } else {
                        if ($(e).attr("id").endsWith("~from")) {
                            result[0].searchValueFrom = $(e).val();
                        } else if ($(e).attr("id").endsWith("~to")) {
                            result[0].searchValueTo = $(e).val();
                        }
                    }
                } else {
                    searchConditions.push({ "id": $(e).attr("id"), "controlType": $(e).attr("id").split("~")[3], "searchField": ($(e).attr("id").split("~")[1] + "." + $(e).attr("id").split("~")[2]), "searchValue": $(e).val(), "searchValueFrom": "", "searchValueTo": "" });
                }
            }
        });

        $("#hdnHeaderSearch").val(JSON.stringify(searchConditions))

        $.ajax({
            type: "POST",
            url: '../WebServices/CommonService.svc/SavePersistTable',
            contentType: "application/json",
            dataType: "json",
            async: false,
            data: JSON.stringify({
                "isMasterPage": true
                , "boolOnlyURL": false
                , "strParam": ("masssalesfulfillmentheaderfilter" + $("#hdnViewID").val())
                , "strPageName": ""
                , "values": "[{\"key\": \"HeaderSearchFilters\",\"value\" : \"" + JSON.stringify(searchConditions).replace(/"/g, "\\\"") + "\"}]"
            }),
            error: function (jqXHR, textStatus, errorThrown) {
                alert("Unknown error occurred.");
            }
        });
    } catch (e) {
        throw e;
    }
}

function GetPersistedHeaderSearchFilters() {
    try {
        $.ajax({
            type: "POST",
            url: '../WebServices/CommonService.svc/GetPersistTable',
            contentType: "application/json",
            dataType: "json",
            async: false,
            data: JSON.stringify({
                "isMasterPage": true
                , "boolOnlyURL": false
                , "strParam": ("masssalesfulfillmentheaderfilter" + $("#hdnViewID").val())
                , "strPageName": ""
                , "values": ""
            }),
            success: function (data) {
                try {
                    var obj = $.parseJSON(data.GetPersistTableResult);
                    if (obj.HeaderSearchFilters != undefined) {
                        $("#hdnHeaderSearch").val(JSON.stringify(obj.HeaderSearchFilters));
                    } else {
                        $("#hdnHeaderSearch").val("[]");
                    }
                } catch (err) {
                    alert("Unknown error occurred.");
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                if (IsJsonString(jqXHR.responseText)) {
                    var objError = $.parseJSON(jqXHR.responseText)
                    alert("Error occurred: " + replaceNull(objError.ErrorMessage));
                } else {
                    alert("Unknown error ocurred");
                }
            }
        });
    } catch (e) {
        throw e;
    }
}

function SortData(sortColumn) {
    try {
        if ($("#hdnSortColumn").val() === sortColumn) {
            if ($("#hdnSortOrder").val() === "DESC") {
                $("#hdnSortOrder").val("ASC");
            } else {
                $("#hdnSortOrder").val("DESC");
            }
        } else {
            $("#hdnSortColumn").val(sortColumn);
            $("#hdnSortOrder").val("ASC");
        }

        LoadRecordsWithPagination();
    } catch (e) {
        alert("Unknown error occurred.");
    }
}

function SelectAllChanged(chkSelectAll) {
    $(".chkSelect").prop("checked", $(chkSelectAll).is(":checked"))

    if ($("#hdnViewID").val() === "1" && $("#ddlBatchRight").val() === "0") {
        LoadSelectedRecordsToRightPaneForPicking();
    } else if ($("#hdnViewID").val() === "2" && $("[id$=hdnPackLastSubViewMode]").val() === "2") {
        var isPickNotPackedColumnAdded = false;
        $("#divMainGrid > table > tbody > tr").not(":first").each(function (index, e) {
            if ($(e).find("#lblQtyPickedNotPacked").length > 0) {
                isPickNotPackedColumnAdded = true;
            }

            return false;
        });

        if (isPickNotPackedColumnAdded) {
            $("#divGridRightPack").html("");

            $("#divMainGrid > table > tbody > tr").not(":first").each(function (index, e) {
                if (parseInt($(e).find("#hdnOppBizDocID").val()) > 0) {
                    LoadShippingBizDoc($(e).find(".chkSelect")[0]);
                } else {
                    LoadSelectedRecordsToRightPaneForPacking($(e).find(".chkSelect")[0]);
                }
            });
        } else {
            alert("Please add \"Picked Not Packed\" column to list");
        }
    } 
}

function AddToBatch(mode) {
    try {
        $("#hdnBatchMode").val(mode);
        var selectedRecords = "";

        $("#divMainGrid").find(".chkSelect").each(function () {
            if ($(this).is(":checked")) {
                selectedRecords += 1
            }
        });

        if (selectedRecords > 0) {
            $("#ddlBatch").val("0");
            $("#txtBatchName").val("");

            if (mode === 1) {
                $("#ddlBatch").show();
                $("#divBatchName").hide();
                $("#modal-batch .modal-title").text("Select Batch");
            } else if (mode === 2) {
                $("#ddlBatch").hide();
                $("#divBatchName").show();
                $("#modal-batch .modal-title").text("Create New Batch");
            }

            $("#modal-batch").modal("show");
        } else {
            alert("Select records.");
        }
    } catch (e) {
        alert("Unknown error occurred while opening batch window.")
    }
}

function SaveBatch() {
    try {
        var selectedRecords = "";

        $("#divMainGrid").find(".chkSelect").each(function () {
            if ($(this).is(":checked")) {
                selectedRecords = selectedRecords + (selectedRecords.length > 0 ? "," : "") + ($(this).closest("tr").find("#hdnOppID")[0].value + "-" + $(this).closest("tr").find("#hdnOppItemID")[0].value)
            }
        });

        if (selectedRecords.length > 0) {
            if ($("#hdnBatchMode").val() === "1") {
                if (parseInt($("#ddlBatch").val()) > 0) {
                    $.ajax({
                        type: "POST",
                        url: '../opportunity/MassSalesFulfillmentService.svc/AddRecordsToBatch',
                        contentType: "application/json",
                        dataType: "json",
                        data: JSON.stringify({
                            "batchID": parseInt($("#ddlBatch").val())
                            , "selectedRecords": selectedRecords
                        }),
                        success: function (data) {
                            alert("Rrecords are added successfully to selected batch.");
                            $("#ddlBatch").val("0");
                            $("#txtBatchName").val("");
                            $("#modal-batch").modal("hide");
                            $("#ddlBatchRight").val("0");
                            $("#tblRight").html("");
                            return true;
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            if(IsJsonString(jqXHR.responseText)){
                                var objError = $.parseJSON(jqXHR.responseText)

                                if (objError.ErrorMessage === "INVALID_BATCH") {
                                    alert("Batch doesn't exist.");
                                } else {
                                    alert("Error occurred: " + replaceNull(objError.ErrorMessage));
                                }
                            } else {
                                alert("Unknown error ocurred");
                            }
                        }
                    });
                } else {
                    alert("Select Batch");
                    $("#ddlBatch").focus();
                }
            } else if ($("#hdnBatchMode").val() === "2") {
                if ($("#txtBatchName").val() != "") {
                    $.ajax({
                        type: "POST",
                        url: '../opportunity/MassSalesFulfillmentService.svc/CreateNewBatchAndRecordsToBatch',
                        contentType: "application/json",
                        dataType: "json",
                        data: JSON.stringify({
                            "batchName": $("#txtBatchName").val().trim()
                            , "selectedRecords": selectedRecords
                        }),
                        success: function (data) {
                            alert("New batch with selected records is created successfully.");
                            $("#ddlBatch").val("0");
                            $("#txtBatchName").val("");
                            $("#modal-batch").modal("hide");
                            LoadBatches();
                            return true;
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            if(IsJsonString(jqXHR.responseText)){
                                var objError = $.parseJSON(jqXHR.responseText)

                                if (objError.ErrorMessage === "MAX_ACTIVE_BATCH_LIMIT_EXCEED") {
                                    alert("You can have maximum 20 active batches.");
                                } else if (objError.ErrorMessage === "BATCH_WITH_SAME_NAME_EXISTS") {
                                    alert("Another batch with same name already exists.");
                                } else {
                                    alert("Error occurred: " + replaceNull(objError.ErrorMessage));
                                }
                            } else {
                                alert("Unknown error ocurred");
                            }
                        }
                    }).fail(function (response) {
                        console.log(response);
                    });
                } else {
                    alert("Batch name is required");
                    $("#txtBatchName").focus();
                }

            } else {
                alert("Somethign wrong with batch selection.");
            }

        } else {
            alert("Select records.");
        }
    } catch (e) {
        alert("Unknown error occurred while adding records to batch.")
    }

    return false;
}

function BatchSelectionChanged(li) {
    try {
        ClearFilters();
        var batchId = $(li).attr("id").replace("batch", "");
        var batchName = $(li).text();
        $("#btnBatch").html(batchName + " <span class='caret'></span>");
        $("#hdnBatchID").val(batchId);
        $("#btnRemoveFromBatch").show();

        LoadRecordsWithPagination();
    } catch (e) {
        alert("Unknown error occurred while fetching detail of selected batch")
    }
}

function CloseBatch(batchID) {
    try {
        $.ajax({
            type: "POST",
            url: '../opportunity/MassSalesFulfillmentService.svc/MarkBatchAsComplete',
            contentType: "application/json",
            dataType: "json",
            data: JSON.stringify({
                "batchID": batchID
            }),
            success: function (data) {
                alert("Batch is marked as complete successfully");
                LoadBatches();
            },
            error: function (jqXHR, textStatus, errorThrown) {
                if(IsJsonString(jqXHR.responseText)){
                    var objError = $.parseJSON(jqXHR.responseText)
                    alert("Error occurred: " + replaceNull(objError.ErrorMessage));
                } else {
                    alert("Unknown error ocurred");
                }
            }
        });
    } catch (e) {
        alert("Unknown error occurred while marking batch as complete");
    }
}

function LoadSelectedRecordsToRightPaneForPicking() {
    try {
        var batchID = 0;
        var selectedRecords = "";
        $("#tblRight").html("");

        if (parseInt($("#ddlBatchRight").val()) > 0) {
            batchID = parseInt($("#ddlBatchRight").val());
            selectedRecords = "";
        } else {
            batchID = 0;

            var selectedRecords = $("#hdnSelectedRecordsForRightPanePicking").val();
            var arrSelectedRecords = [];
            if (selectedRecords != "") {
                arrSelectedRecords = selectedRecords.split(",");
            }

            $("#divMainGrid").find(".chkSelect").each(function (e, chkSelect) {
                if ($(this).is(":checked")) {
                    if (arrSelectedRecords.indexOf($(chkSelect).closest("tr").find("#hdnOppID")[0].value + "-" + $(chkSelect).closest("tr").find("#hdnOppItemID")[0].value) === -1) {
                        arrSelectedRecords.push($(chkSelect).closest("tr").find("#hdnOppID")[0].value + "-" + $(chkSelect).closest("tr").find("#hdnOppItemID")[0].value);
                    }
                } else {
                    arrSelectedRecords = arrSelectedRecords.filter(function(value, index, arr){
                        return value != ($(chkSelect).closest("tr").find("#hdnOppID")[0].value + "-" + $(chkSelect).closest("tr").find("#hdnOppItemID")[0].value);
                    });
                }
            });

            $("#hdnSelectedRecordsForRightPanePicking").val(arrSelectedRecords.join(","));
            selectedRecords = arrSelectedRecords.join(",");
        }

        $.ajax({
            type: "POST",
            url: '../opportunity/MassSalesFulfillmentService.svc/GetRecordsForPicking',
            contentType: "application/json",
            dataType: "json",
            data: JSON.stringify({
                "isGroupByOrder": $("#chkGroupBy").is(":checked")
                , "batchID": batchID
                , "selectedRecords": selectedRecords
            }),
            success: function (data) {
                try {
                    var obj = $.parseJSON(data.GetRecordsForPickingResult);
                    if (obj.RightPaneFields.length === 0) {
                        alert("Columns are not configured for pick list right pane. Please first configure columns to display.");
                    } else {
                        var rightPaneColumns = $.parseJSON(obj.RightPaneFields);
                        var records = $.parseJSON(obj.Records);
                        var noWrapColumns = [];
                        //var noWrapColumns = [3, 96, 101, 203, 233, 251, 273, 281, 294];

                        //Header Row
                        data += "<thead><tr>";
                        rightPaneColumns.forEach(function (e) {
                            data += "<th id='" + e.ID + "' style='width:" + (e.intColumnWidth || 100) + "px;'>" + (e.vcOrigDbColumnName === "numQtyPicked" ? "<input type='checkbox' id='chkPickAll' onChange='return PickAllChanged(this);'>" : "") + " " + replaceNull(e.vcFieldName) + "</th>";
                        });
                        data += "</tr></thead><tbody>";

                        //Data Rows
                        if (records.length > 0) {
                            records.forEach(function (n) {
                                data += "<tr>";
                                var i = 1;
                                rightPaneColumns.forEach(function (e) {
                                    if (i === 1) {
                                        data += "<td" + ($.inArray(e.numFieldID, noWrapColumns) > -1 ? " class='nowrap'" : "") + ">"
                                        data += "<input type='hidden' id='hdnOppID' value='" + (n.numOppId || 0) + "' />";
                                        if((n.numOppItemID || 0) > 0)
                                        {
                                            $("#hdnPickListByOrder").val(true);
                                        }
                                        data += "<input type='hidden' id='hdnOppName' value='" + replaceNull(n.vcPoppName) + "' />"
                                        data += "<input type='hidden' id='hdnItemCode' value='" + replaceNull(n.numItemCode) + "' />";
                                        data += "<input type='hidden' id='hdnWareHouseID' value='" + replaceNull(n.numWareHouseID) + "' />";
                                        data += "<input type='hidden' id='hdnWLocationID' value='" + replaceNull(n.numWLocationID) + "' />";
                                        
                                        //data += "<input type='hidden' id='hdnKitChildItems' value='" + replaceNull(n.vcKitChildItems) + "' />";
                                        //data += "<input type='hidden' id='hdnKitChildKitItems' value='" + replaceNull(n.vcKitChildKitItems) + "' />";
                                        
                                        data += "<input type='hidden' id='hdnOppChildItemID' value='" + (n.numOppChildItemID || 0) + "' />";
                                        data += "<input type='hidden' id='hdnOppKitChildItemID' value='" + (n.numOppKitChildItemID || 0) + "' />";
                                        data += "<input type='hidden' id='hdnPickedQty' value='" + replaceNull(n.numQtyPicked) + "' />";
                                        data += "<input type='hidden' id='hdnItemImage' value='" + replaceNull(n.vcPathForTImage) + "' />";
                                        data += "<input type='hidden' id='hdnIsSerial' value='" + (n.bitSerial || false) + "' />";
                                        data += "<input type='hidden' id='hdnIsLot' value='" + (n.bitLot || false) + "' />";
                                        data += "<input type='hidden' id='hdnIsKit' value='" + JSON.parse(n.bitKit) + "' />";
                                        data += "<input type='hidden' id='hdnSKU' value='" + replaceNull(n.vcSKU) + "' />";
                                        data += "<input type='hidden' id='hdnUPC' value='" + replaceNull(n.numBarCodeId) + "' />";
                                        data += "<input type='hidden' id='hdnItemName' value='" + replaceNull(n.vcItemName) + "' />";
                                        data += "<input type='hidden' id='hdnAttributes' value='" + replaceNull(n.vcAttributes) + "' />";
                                        data += "<input type='hidden' id='hdnInclusionDetail' value='" + replaceNull(n.vcInclusionDetails) + "' />";
                                        data += "<input type='hidden' id='hdnLocation' value='" + replaceNull(n.vcLocation) + "' />";
                                        data += "<input type='hidden' id='hdnRemainingQty' value='" + replaceNull(n.numRemainingQty) + "' />";
                                        data += "<input type='hidden' id='hdQtyRequiredKit' value='" + (n.numQtyRequiredKit || 0) + "' />";
                                        
                                        data += "<input type='hidden' id='hdnOppItemID' value='" + replaceNull(n.vcOppItemIDs) + "' />";
                                    } else {
                                        data += "<td" + ($.inArray(e.numFieldID, noWrapColumns) > -1 ? " class='nowrap'" : "") + ">";
                                    }
                                    i = i + 1;

                                    if (e.vcOrigDbColumnName === "vcPathForTImage") {
                                        if (n[e.vcOrigDbColumnName] != null && n[e.vcOrigDbColumnName].indexOf('.') > -1) {
                                            data += "<img height='45' class='img-responsive' src='" + $("[id$=hdnMSImagePath]").val() + n[e.vcOrigDbColumnName] + "' alt='' /></td>"
                                        } else {
                                            data += "<img height='45' class='img-responsive' src='../images/icons/cart_large.png' alt='' /></td>"
                                        }

                                    } else if (e.vcOrigDbColumnName === "numQtyPicked") {
                                        var html = "<table class='tblPickLocation' style='width: 100%;white-space:nowrap;'>";
                                        JSON.parse(n.vcWarehouseLocations.toString().replace(/\n/g, "<br />")).forEach(function (objLocation) {
                                            html += "<tr>";
                                            html += "<td>" + replaceNull(objLocation.Location) + " (" + (objLocation.Available || 0) + ") " + "<input type='hidden' class='hdnWarehouseItemID' value='" + objLocation.WarehouseItemID + "'/><input type='hidden' class='hdnWarehouseLocationID' value='" + objLocation.WarehouseLocationID + "'/><input type='hidden' class='hdnAvailable' value='" + (objLocation.Available || 0) + "'/></td>";
                                            if((n.numOppChildItemID || 0) > 0 || (n.numOppKitChildItemID || 0) > 0){
                                                html += "<td><input type='text' class='txtPicked form-control' onfocus='FocusParentRow(this)' style='padding:1px;width:80px;' value='" + replaceNull(n[e.vcOrigDbColumnName]) + "' disabled /></td>";
                                            } else {
                                                html += "<td><input type='text' class='txtPicked form-control' onfocus='FocusParentRow(this)' style='padding:1px;width:80px;' value='" + replaceNull(n[e.vcOrigDbColumnName]) + "' /></td>";
                                            }
                                            
                                            html += "</tr>";
                                        });
                                        html += "</table>";
                                        data += html;
                                        data += "</td>";
                                    } else if (e.vcOrigDbColumnName === "numRemainingQty") {
                                        data += "<span id='lblRemaining' class='badge bg-red'>" + replaceNull(n[e.vcOrigDbColumnName]) + "</span>";
                                    } else {
                                        data += replaceNull(n[e.vcOrigDbColumnName]) + "</td>";
                                    }
                                });
                                data += "</tr>";
                            });
                        } else {
                            if ($("#tblRight > tbody > tr").not(":first").length === 0) {
                                if(batchID > 0){
                                    data += "<tr><td colspan='" + rightPaneColumns.length + "' class='text-center'>No records left to be picked for selected batch</td></tr>";
                                } else {
                                    data += "<tr><td colspan='" + rightPaneColumns.length + "' class='text-center'>No records found</td></tr>";
                                }
                            }
                        }

                        $("#tblRight").append(data + "</tbody>");

                        $("#tblRight input.txtPicked").keydown(function (e) {
                            if (e.keyCode == 13 || e.keyCode == 9) {
                                e.preventDefault();

                                if($(this).val() === ""){
                                    $(this).val("0");
                                }


                                var remainingQty = parseFloat($(this).closest(".tblPickLocation").closest("tr").find("#hdnRemainingQty").val());
                                var row = $(this).closest("table.tblPickLocation").closest("tr")[0];
                                var pickedQty = 0;
                                
                                $(this).closest(".tblPickLocation").find("tr").each(function(){
                                    if(JSON.parse($(row).find("#hdnIsSerial").val()) || JSON.parse($(row).find("#hdnIsLot").val())){
                                        pickedQty += GetSerialLotQty($(this).find(".txtPicked"),JSON.parse($(row).find("#hdnIsSerial").val()),JSON.parse($(row).find("#hdnIsLot").val()));
                                    } else {
                                        if(parseFloat($(this).find(".txtPicked").val()) > 0){
                                            pickedQty +=  parseFloat($(this).find(".txtPicked").val());
                                        }
                                    }
                                });

                                if(pickedQty > remainingQty){    
                                    alert('Picked qty is more than remaining quantity.');
                                    if(JSON.parse($(row).find("#hdnIsSerial").val()) || JSON.parse($(row).find("#hdnIsLot").val())){
                                        remainingQty = remainingQty - 0;
                                    } else {
                                        remainingQty = remainingQty - parseFloat($(this).val());
                                    }
                                    
                                    $(this).val("0");
                                } else {
                                    if($(this).closest("tr").next("tr").length > 0)
                                    {
                                        var searchInput =  $(this).closest("tr").next("tr").find("input.txtPicked")[0];
                                        
                                        var strLength = $(searchInput).val().length * 2;

                                        $(searchInput).focus();
                                        searchInput.setSelectionRange(strLength, strLength);
                                    }
                                    else 
                                    {
                                        if ($(row).next('tr').length > 0) {
                                            var searchInput =  $(row).next('tr').find("input.txtPicked")[0];
                                        
                                            var strLength = $(searchInput).val().length * 2;

                                            $(searchInput).focus();
                                            searchInput.setSelectionRange(strLength, strLength);
                                        }
                                        else {
                                            if ($("#tblRight > tbody > tr").length > 0) {
                                                var searchInput =  $("#tblRight > tbody > tr:first-child").find("input.txtPicked")[0];

                                                var strLength = $(searchInput).val().length * 2;

                                                $(searchInput).focus();
                                                searchInput.setSelectionRange(strLength, strLength);
                                            }
                                        }
                                    }

                                    remainingQty = parseFloat($(row).find("#hdnRemainingQty").val()) - pickedQty;
                                }

                                $(row).find("#lblRemaining").text(remainingQty);

                                if (remainingQty === 0) {
                                    $(row).find("#lblRemaining").removeClass("bg-red").addClass("bg-green");
                                    $(row).addClass("pickedItem");

                                    animateCSS(row,"slideOutDown",function(){
                                        $(row).appendTo("#tblRight > tbody");
                                        $(row).removeClass("focusedRow");
                                    });
                                } else {
                                    $(row).find("#lblRemaining").removeClass("bg-green").addClass("bg-red");
                                    $(row).removeClass("pickedItem");
                                    $(row).prependTo("#tblRight > tbody");
                                }

                                ChangeKitChildQty($(this),false);
                            } else {
                                var row = $(this).closest("table.tblPickLocation").closest("tr")[0];
                                if(!(JSON.parse($(row).find("#hdnIsSerial").val()) || JSON.parse($(row).find("#hdnIsLot").val()))){
                                    if (e.keyCode != 8 && e.keyCode != 0 && e.keyCode != 190 && e.keyCode != 37 && 	e.keyCode != 38 && 	e.keyCode != 39 && 	e.keyCode != 40 && 	e.keyCode != 46 && (e.keyCode < 48 || e.keyCode > 57) && !(e.keyCode >=96 && e.keyCode <= 105)){
                                        e.preventDefault();
                                    } 
                                } 
                            } 
                        });

                        $("table[id$=tblRight]").colResizable({
                            //fixed: false,
                            liveDrag: true,
                            gripInnerHtml: "<div class='grip2'></div>",
                            draggingClass: "dragging",
                            minWidth: 120,
                            resizeMode:'overflow',
                            onResize: onGridColumnResizedRight,
                            postbackSafe:true,
                            partialRefresh: true
                        });
                    }
                } catch (err) {
                    alert("Unknown error occurred.");
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                if(IsJsonString(jqXHR.responseText)){
                    var objError = $.parseJSON(jqXHR.responseText)
                    alert("Error occurred: " + replaceNull(objError.ErrorMessage));
                } else {
                    alert("Unknown error ocurred");
                }
            }
        });
    } catch (e) {
        alert("Unknown error occurred while loading selecting records for picking");
    }

    $("#txtScan").focus();
}

function ChangeKitChildQty(txt,isCheckAll){
    var row = $(txt).closest("table.tblPickLocation").closest("tr")[0];
    var pickedQty = 0;
    var isKit = JSON.parse($(row).find("#hdnIsKit").val());
    var oppItemID = parseFloat($(row).find("#hdnOppItemID").val()); 

    if(isKit){
        $(txt).closest(".tblPickLocation").find("tr").each(function(){
            if(JSON.parse($(row).find("#hdnIsSerial").val()) || JSON.parse($(row).find("#hdnIsLot").val())){
                pickedQty += GetSerialLotQty($(this).find(".txtPicked"),JSON.parse($(row).find("#hdnIsSerial").val()),JSON.parse($(row).find("#hdnIsLot").val()));
            } else {
                if(parseFloat($(this).find(".txtPicked").val()) > 0){
                    pickedQty +=  parseFloat($(this).find(".txtPicked").val());
                }
            }
        });

        $('#tblRight > tbody > tr').each(function(){
            var row = $(this);

            if(parseInt($(this).find("#hdnOppItemID").val()) == oppItemID && (parseInt($(this).find("#hdnOppChildItemID").val()) > 0 || parseInt($(this).find("#hdnOppKitChildItemID").val()) > 0)){
                var remainingQty = pickedQty * parseFloat($(this).find("#hdQtyRequiredKit").val());

                $(this).find("table.tblPickLocation").find("tr").each(function() {
                    if(parseFloat($(this).find(".hdnAvailable").val()) >= remainingQty){
                        $(this).find(".txtPicked").val(remainingQty);
                        remainingQty = 0
                    } else {
                        $(this).find(".txtPicked").val($(this).find(".hdnAvailable").val());
                        remainingQty = remainingQty - parseFloat($(this).find(".hdnAvailable").val())
                    }
                });

                if(pickedQty == 0){
                    remainingQty = parseFloat($(this).find("#hdnRemainingQty").val())
                }

                if(remainingQty > 0){
                    $(this).find("#lblRemaining").text(remainingQty);
                    $(this).find("#lblRemaining").removeClass("bg-green").addClass("bg-red");
                    $(this).removeClass("pickedItem");

                    if(!isCheckAll){
                        $(this).prependTo("#tblRight > tbody");
                    }
                } else {
                    $(this).find("#lblRemaining").text(0);
                    $(this).find("#lblRemaining").removeClass("bg-red").addClass("bg-green");
                    $(this).addClass("pickedItem");

                    if(!isCheckAll){
                        animateCSS(row[0],"slideOutDown",function(){
                            $(this).appendTo("#tblRight > tbody");
                            $(this).removeClass("focusedRow");
                        });
                    }
                }
            }
        });
    }
}

var onGridColumnResized = function (e) {
    var columns = $(e.currentTarget).find("th");
    var msg = "";
    columns.each(function () {
        var thId = $(this).attr("id");
        var thWidth = $(this).width();
        msg += (thId || "anchor") + ':' + thWidth + ';';
    }
    )

    $.ajax({
        type: "POST",
        url: "../common/Common.asmx/SaveAdvanceSearchGridColumnWidth",
        data: "{str:'" + msg + "', viewID:'" + parseInt($("#hdnViewID").val()) + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json"
    });
};

var onGridColumnResizedRight = function (e) {
    var columns = $(e.currentTarget).find("th");
    var msg = "";
    columns.each(function () {
        var thId = $(this).attr("id");
        var thWidth = $(this).width();
        msg += (thId || "anchor") + ':' + thWidth + ';';
    }
    )

    $.ajax({
        type: "POST",
        url: "../common/Common.asmx/SaveGridColumnWidth",
        data: "{str:'" + msg + "', viewID:'" + parseInt($("#hdnViewID").val()) + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json"
    });
};

function LoadSelectedRecordsToRightPaneForPacking(chkSelect){
    try {
        var oppID = parseInt($(chkSelect).closest("tr").find("#hdnOppID").val());
        var oppItemID = parseInt($(chkSelect).closest("tr").find("#hdnOppItemID").val());
        var oppBizDocID = parseInt($(chkSelect).closest("tr").find("#hdnOppBizDocID").val());
        var remainingQty = parseFloat($(chkSelect).closest("tr").find("#lblQtyPickedNotPacked").text());

        if ($(chkSelect).is(":checked") && ($("#chkGroupBy").is(":checked") || remainingQty > 0)){
            $.ajax({
                type: "POST",
                url: '../opportunity/MassSalesFulfillmentService.svc/GetRecordsForPacking',
                contentType: "application/json",
                dataType: "json",
                data: JSON.stringify({
                    "oppID": oppID
                    , "oppItemID": oppItemID
                    , "oppBizDocID": oppBizDocID
                }),
                success: function (data) {
                    try {
                        var obj = $.parseJSON(data.GetRecordsForPackingResult);
                            
                        obj.forEach(function (e) {
                            var order = "";
                            var boxUI = "";
                            var packedQty = 0;
                            var boxCount = 1;
                            var qtyToPack = e.numTotalQty || 0;

                            if($("#divGridRightPack").find("[id='divOrder" + oppID.toString() + "~" + oppBizDocID.toString() + "']").length > 0){
                                if(e.numoppitemtCode > 0 && $("#divGridRightPack").find("[id='divOrder" + oppID.toString() + "~" + oppBizDocID.toString() + "']").find(".ulBoxedItems li[id='" + e.numoppitemtCode.toString() + "']").length === 0){
                                    if(parseInt(e.numContainer) > 0 && (e.numNoItemIntoContainer || 0) > 0 && (e.numTotalQty || 0) > 0) {
                                        $("#divGridRightPack").find("[id='divOrder" + oppID.toString() + "~" + oppBizDocID.toString() + "']").find("div[id^='box~']").each(function(index,box){
                                            var numContainerID = parseInt($(box).attr("id").split("~")[2]);
                                            var numNumberItemIntoContainer = parseFloat($(box).attr("id").split("~")[3]);

                                            if(parseInt(e.numContainer) === numContainerID && parseFloat(e.numNoItemIntoContainer || 0) === numNumberItemIntoContainer){
                                                var numBoxedQuantity = 0;

                                                $(box).find("li").each(function(index,item){
                                                    numBoxedQuantity += parseFloat($(item).find(".AddedItemQty").text())
                                                });

                                                if(numBoxedQuantity < numNumberItemIntoContainer){
                                                    $(box).find("ul.ulBoxedItems").append("<li id='" + e.numoppitemtCode.toString() + "'>" + replaceNull(e.vcItemName) + "<button class='btn btn-xs btn-danger pull-right' onclick='return DeleteBoxedItem(this)'><i class='fa fa-trash-o'></i></button><span class='AddedItemQty pull-right' style='padding-right:10px;'>" + (e.numTotalQty > (numNumberItemIntoContainer - numBoxedQuantity) ? (numNumberItemIntoContainer - numBoxedQuantity) : e.numTotalQty) + "</span></li>");

                                                    if(e.numTotalQty > (numNumberItemIntoContainer - numBoxedQuantity)){
                                                        e.numTotalQty = e.numTotalQty - (numNumberItemIntoContainer - numBoxedQuantity);
                                                        packedQty+=(numNumberItemIntoContainer - numBoxedQuantity);
                                                    } else {
                                                        packedQty+=e.numTotalQty;
                                                        e.numTotalQty = 0;
                                                    }
                                                }
                                            }

                                            boxCount += 1;
                                        });

                                        while(e.numTotalQty > 0){
                                            var boxID = "box~" + boxCount + "~" + e.numContainer.toString() + "~" + (e.numNoItemIntoContainer || 0).toString();

                                            boxUI += "<li>";
                                            boxUI += "<button id='btnAddToBox" + boxID + "' class='btn btn-block btn-flat' style='background-color: #bf9000;color: #fff;' onClick='return AddItemToBox(this)'>Add to this box</button>";
                                            boxUI += "<div class='box box-default' style='margin-top:5px;' id='" + boxID + "'>";
                                            boxUI += "<div class='box-header with-border' style='padding:5px;'><h3 class='box-title'>Box<span class='spnBoxNumber'>" + boxCount + "</span>: " + replaceNull(e.vcContainer) + "</h3>";
                                            boxUI += "<div class='box-tools pull-right'>";
                                            boxUI += "<div class='form-inline'>";
                                            boxUI += "<button type='button' class='btn btn-xs btn-danger btn-delete-box' onClick='return DeleteShippingBox(\"" + boxID + "\");'><i class='fa fa-trash-o'></i></button>";
                                            boxUI += "</div>";
                                            boxUI += "</div>";
                                            boxUI += "</div>";
                                            boxUI += "<div class='box-body order-box-body' style='padding:5px'>";
                                            boxUI += "<div class='row'>";
                                            boxUI += "<div class='pull-left'>";
                                            boxUI += "<input type='button' class='btn btn-danger btn-flat btnChangeWeight' value='Change' onClick='return ChangeBoxWeight(\"" + boxID + "\")' />";
                                            boxUI += "</div>";
                                            boxUI += "<div class='pull-right'>";
                                            boxUI += PackageTypeUI((e.numShipVia || 0));
                                            boxUI += "</div>";
                                            boxUI += "</div>";
                                            boxUI += "</div>";
                                            boxUI += "<hr style='margin-top: 5px;margin-bottom: 5px;'>";
                                            boxUI += "<div class='form-inline'>";
                                            boxUI += "<div class='form-group'><input type='hidden' class='hdnBoxWeightChanged' value='0' /><input type='textbox' style='width:55px' class='form-control txtBoxWeight' value='" + (e.fltContainerWeight || 0).toString() + "' disabled/> lbs</div>";
                                            boxUI += "<div class='pull-right'><div class='form-group'><label> L</label><input type='text' id='length' value='" + (e.fltContainerLength || 0).toString() + "' class='form-control' style='width:45px; padding:2px' /></div><div class='form-group'><label> x W</label><input type='text' id='width' value='" + (e.fltContainerWidth || 0).toString() + "' class='form-control' style='width:45px; padding:2px' /></div><div class='form-group'><label> x H</label><input type='text' id='height' value='" + (e.fltContainerHeight || 0).toString() + "' class='form-control' style='width:45px; padding:2px' /></div></div>"; 
                                            boxUI += "</div>";
                                            boxUI += "<ul class='list-unstyled ui-sortable ulBoxedItems'>";
                                            boxUI += "<li id='" + e.numoppitemtCode.toString() + "'>" + replaceNull(e.vcItemName) + "<button class='btn btn-xs btn-danger pull-right' onclick='return DeleteBoxedItem(this)'><i class='fa fa-trash-o'></i></button><span class='AddedItemQty pull-right' style='padding-right:10px;'>" + (e.numTotalQty > e.numNoItemIntoContainer ? e.numNoItemIntoContainer : e.numTotalQty) + "</span></li>";
                                            boxUI += "</ul>";
                                            boxUI += "</div>";
                                            boxUI += "</div>";
                                            boxUI += "</li>";
                                            boxCount = boxCount + 1;

                                            if(e.numTotalQty > e.numNoItemIntoContainer){
                                                e.numTotalQty = e.numTotalQty - e.numNoItemIntoContainer;
                                                packedQty+=e.numNoItemIntoContainer;
                                            } else {
                                                packedQty+=e.numTotalQty;
                                                e.numTotalQty = 0;
                                            }
                                        }

                                        $("#divGridRightPack").find("[id='divOrder" + oppID.toString() + "~" + oppBizDocID.toString() + "']").find("ul.ulBoxes").append(boxUI);
                                    }

                                    $("#divGridRightPack").find("[id='divOrder" + oppID.toString() + "~" + oppBizDocID.toString() + "']").find(".ulItems").append("<li id='" + e.numoppitemtCode.toString() + "'><input type='checkbox' class='chkShip' checked='checked' id='chk" + e.numoppitemtCode.toString() + "'/><span class='spnItemName'>" + replaceNull(e.vcItemName) + "</span><input type='hidden' class='hdnQtyToPack' value='" + qtyToPack + "'/><input type='text' class='form-control txtQtyToPack pull-right' style='width:35px;height:25px;padding:0px;' value='" + (e.numTotalQty || 0) + "'/><label class='lblQtyPacked pull-right' style='width:35px;height:25px;padding:0px;'>" + packedQty.toString() + "</label></li>");
                                }
                            } else {
                                if((e.numContainer || 0) > 0){
                                            
                                            
                                    if((e.numNoItemIntoContainer || 0) > 0 && (e.numTotalQty || 0) > 0){
                                        while(e.numTotalQty > 0){
                                            var boxID = "box~" + boxCount + "~" + e.numContainer.toString() + "~" + (e.numNoItemIntoContainer || 0).toString();

                                            boxUI += "<li>";
                                            boxUI += "<button id='btnAddToBox" + boxID + "' class='btn btn-block btn-flat' style='background-color: #bf9000;color: #fff;' onClick='return AddItemToBox(this)'>Add to this box</button>";
                                            boxUI += "<div class='box box-default' style='margin-top:5px;' id='" + boxID + "'>";
                                            boxUI += "<div class='box-header with-border' style='padding:5px;'><h3 class='box-title'>Box<span class='spnBoxNumber'>" + boxCount + "</span>: " + replaceNull(e.vcContainer) + "</h3>";
                                            boxUI += "<div class='box-tools pull-right'>";
                                            boxUI += "<div class='form-inline'>";
                                            boxUI += "<button type='button' class='btn btn-xs btn-danger btn-delete-box' onClick='return DeleteShippingBox(\"" + boxID + "\");'><i class='fa fa-trash-o'></i></button>";
                                            boxUI += "</div>";
                                            boxUI += "</div>";
                                            boxUI += "</div>";
                                            boxUI += "<div class='box-body order-box-body' style='padding:5px'>";
                                            boxUI += "<div class='row'>";
                                            boxUI += "<div class='pull-left'>";
                                            boxUI += "<input type='button' class='btn btn-danger btn-flat btnChangeWeight' value='Change' onClick='return ChangeBoxWeight(\"" + boxID + "\")' />";
                                            boxUI += "</div>";
                                            boxUI += "<div class='pull-right'>";
                                            boxUI += PackageTypeUI((e.numShipVia || 0));
                                            boxUI += "</div>";
                                            boxUI += "</div>";
                                            boxUI += "<hr style='margin-top: 5px;margin-bottom: 5px;'>";
                                            boxUI += "<div class='form-inline'>";
                                            boxUI += "<div class='form-group'><input type='hidden' class='hdnBoxWeightChanged' value='0' /><input type='textbox' style='width:55px' class='form-control txtBoxWeight' value='" + (e.fltContainerWeight || 0).toString() + "' disabled/> lbs</div>";
                                            boxUI += "<div class='pull-right'><div class='form-group'><label> L</label><input type='text' id='length' value='" + (e.fltContainerLength || 0).toString() + "' class='form-control' style='width:45px; padding:2px' /></div><div class='form-group'><label> x W</label><input type='text' id='width' value='" + (e.fltContainerWidth || 0).toString() + "' class='form-control' style='width:45px; padding:2px' /></div><div class='form-group'><label> x H</label><input type='text' id='height' value='" + (e.fltContainerHeight || 0).toString() + "' class='form-control' style='width:45px; padding:2px' /></div></div>"; 
                                            boxUI += "</div>";
                                            boxUI += "<ul class='list-unstyled ui-sortable ulBoxedItems'>";
                                            boxUI += "<li id='" + e.numoppitemtCode.toString() + "'>" + replaceNull(e.vcItemName) + "<button class='btn btn-xs btn-danger pull-right' onclick='return DeleteBoxedItem(this)'><i class='fa fa-trash-o'></i></button><span class='AddedItemQty pull-right' style='padding-right:10px;'>" + (e.numTotalQty > e.numNoItemIntoContainer ? e.numNoItemIntoContainer : e.numTotalQty) + "</span></li>";
                                            boxUI += "</ul>";
                                            boxUI += "</div>";
                                            boxUI += "</div>";
                                            boxUI += "</li>";
                                            boxCount = boxCount + 1;

                                            if(e.numTotalQty > e.numNoItemIntoContainer){
                                                e.numTotalQty = e.numTotalQty - e.numNoItemIntoContainer;
                                                packedQty+=e.numNoItemIntoContainer;
                                            } else {
                                                packedQty+=e.numTotalQty;
                                                e.numTotalQty = 0;
                                            }
                                        }
                                    }
                                }

                                order += "<div class='box box-primary' id='divOrder" + e.numOppId.toString() + "~" + parseInt(e.numOppBizDocsId || 0).toString() + "'>";
                                order += "<div class='box-header with-border'>";
                                order += "<h3 class='box-title'>" + replaceNull(e.vcpOppName) + "</h3>";
                                order += "<div class='box-tools pull-right'>";
                                order += "<ul class='list-inline'><li>Current ship rate:</li><li>" + (parseFloat(e.monCurrentShipRate || 0) > 0 ? ($("[id$=hdnMSCurrencyID]").val() + parseFloat(e.monCurrentShipRate || 0)) : "Needs rate") + "</li>";
                                order += "<li>";
                                order += "<button type='button' class='btn btn-box-tool' data-widget='collapse'><i class='fa fa-minus'></i></button><input type='hidden' id='hdnShippingDetail' />";
                                order += "</li></ul>";
                                order += "</div>";
                                order += "</div>";
                                order += "<div class='box-body order-box-body' style='overflow-y:auto; overflow-x:auto padding:5px'>";
                                order += "<table style='width:100%; white-space:nowrap;'>";
                                order += "<tr>";
                                order += "<td style='padding:5px;'>";
                                order += "<div class='form-inline'>";
                                order += "<div class='form-group'>";
                                order += "<select class='form-control containers' style='width:70px' onChange='return AddShippingBox(\"divOrder" + e.numOppId.toString() + "~" + parseInt(e.numOppBizDocsId || 0).toString() + "\",1,0,\"\",0,0,0,0);'>";
                                order += "<option value='0'>Box</option>";

                                if($("#hdnContainers").val() !== ""){
                                    var containers = $.parseJSON($("#hdnContainers").val());

                                    if (containers != null && containers.length > 0) {
                                        containers.forEach(function (e) {
                                            order += "<option value='" + replaceNull(e.ValueDimension) + "'>" + replaceNull(e.vcPackageName) + "</option>";
                                        });
                                    }
                                }

                                order += "</select>";
                                order += "</div>";
                                order += "<button type='button' style='margin-left:5px' onClick='return AddShippingBox(\"divOrder" + e.numOppId.toString() + "~" + parseInt(e.numOppBizDocsId || 0).toString() + "\",2,0,\"Custom\",0,0,0,0);' id='btnAddCustomBox' class='btn btn-flat btn-default'><img src='../images/package_add.png' title='Add Box' style='cursor: pointer' /></button>";
                                order += "</div>";
                                order += "</td>";
                                order += "<td style='padding:5px;text-align: left;width: 100%;'>";
                                order += "<div class='form-inline'>";
                                order += "<div class='form-group'>";
                                order += "<label></label>";
                                order += "<select class='form-control' id='ddlShipVia" + e.numOppId.toString() + "~" + parseInt(e.numOppBizDocsId || 0).toString() + "' onChange='return ShiViaChanged(this);'>";
                                order += "<option value='0'>Ship-via</option>";
                                order += "<option value='91' " + ((e.numShipVia || 0) === 91 ? "selected='true'" : "")  + ">FedEx</option>";
                                order += "<option value='88' " + ((e.numShipVia || 0) === 88 ? "selected='true'" : "")  + ">UPS</option>";
                                order += "<option value='90' " + ((e.numShipVia || 0) === 90 ? "selected='true'" : "")  + ">USPS</option>";
                                order += "</select>";
                                order += "</div>";
                                order += "<div class='form-group'>";
                                order += "<label></label>";
                                order += "<select class='form-control' style='width:120px;' id='ddlShipService" + e.numOppId.toString() + "~" + parseInt(e.numOppBizDocsId || 0).toString() + "'>";
                                order += "<option value='0'>Ship-service</option>";
                                if((e.numShipVia || 0) == 91){
                                    order += "<option value='10' " + ((e.numShipService || 0) === 10 ? "selected='true'" : "") + ">FedEx Priority Overnight</option>";
                                    order += "<option value='11' " + ((e.numShipService || 0) === 11 ? "selected='true'" : "") + ">FedEx Standard Overnight</option>";
                                    order += "<option value='12' " + ((e.numShipService || 0) === 12 ? "selected='true'" : "") + ">FedEx Overnight</option>";
                                    order += "<option value='13' " + ((e.numShipService || 0) === 13 ? "selected='true'" : "") + ">FedEx 2nd Day</option>";
                                    order += "<option value='14' " + ((e.numShipService || 0) === 14 ? "selected='true'" : "") + ">FedEx Express Saver</option>";
                                    order += "<option value='15' " + ((e.numShipService || 0) === 15 || (e.numShipService || 0) === 0 ? "selected='true'" : "") + ">FedEx Ground</option>";
                                    order += "<option value='16' " + ((e.numShipService || 0) === 16 ? "selected='true'" : "") + ">FedEx Ground Home Delivery</option>";
                                    order += "<option value='17' " + ((e.numShipService || 0) === 17 ? "selected='true'" : "") + ">FedEx 1 Day Freight</option>";
                                    order += "<option value='18' " + ((e.numShipService || 0) === 18 ? "selected='true'" : "") + ">FedEx 2 Day Freight</option>";
                                    order += "<option value='19' " + ((e.numShipService || 0) === 19 ? "selected='true'" : "") + ">FedEx 3 Day Freight</option>";
                                    order += "<option value='20' " + ((e.numShipService || 0) === 20 ? "selected='true'" : "") + ">FedEx International Priority</option>";
                                    order += "<option value='21' " + ((e.numShipService || 0) === 21 ? "selected='true'" : "") + ">FedEx International Priority Distribution</option>";
                                    order += "<option value='22' " + ((e.numShipService || 0) === 22 ? "selected='true'" : "") + ">FedEx International Economy</option>";
                                    order += "<option value='23' " + ((e.numShipService || 0) === 23 ? "selected='true'" : "") + ">FedEx International Economy Distribution</option>";
                                    order += "<option value='24' " + ((e.numShipService || 0) === 24 ? "selected='true'" : "") + ">FedEx International First</option>";
                                    order += "<option value='25' " + ((e.numShipService || 0) === 25 ? "selected='true'" : "") + ">FedEx International Priority Freight</option>";
                                    order += "<option value='26' " + ((e.numShipService || 0) === 26 ? "selected='true'" : "") + ">FedEx International Economy Freight</option>";
                                    order += "<option value='27' " + ((e.numShipService || 0) === 27 ? "selected='true'" : "") + ">FedEx International Distribution Freight</option>";
                                    order += "<option value='28' " + ((e.numShipService || 0) === 28 ? "selected='true'" : "") + ">FedEx Europe International Priority</option>";
                                } else if ((e.numShipVia || 0) == 88){
                                    order += "<option value='40' " + ((e.numShipService || 0) === 40 ? "selected='true'" : "") + ">UPS Next Day Air</option>";
                                    order += "<option value='42' " + ((e.numShipService || 0) === 42 ? "selected='true'" : "") + ">UPS 2nd Day Air</option>";
                                    order += "<option value='43' " + ((e.numShipService || 0) === 43 || (e.numShipService || 0) === 0 ? "selected='true'" : "") + ">UPS Ground</option>";
                                    order += "<option value='48' " + ((e.numShipService || 0) === 48 ? "selected='true'" : "") + ">UPS 3Day Select</option>";
                                    order += "<option value='49' " + ((e.numShipService || 0) === 49 ? "selected='true'" : "") + ">UPS Next Day Air Saver</option>";
                                    order += "<option value='50' " + ((e.numShipService || 0) === 50 ? "selected='true'" : "") + ">UPS Saver</option>";
                                    order += "<option value='51' " + ((e.numShipService || 0) === 51 ? "selected='true'" : "") + ">UPS Next Day Air Early A.M.</option>";
                                    order += "<option value='55' " + ((e.numShipService || 0) === 55 ? "selected='true'" : "") + ">UPS 2nd Day Air AM</option>";
                                } else if ((e.numShipVia || 0) == 90){
                                    order += "<option value='70' " + ((e.numShipService || 0) === 70 ? "selected='true'" : "") + ">USPS Express</option>";
                                    order += "<option value='71' " + ((e.numShipService || 0) === 71 ? "selected='true'" : "") + ">USPS First Class</option>";
                                    order += "<option value='72' " + ((e.numShipService || 0) === 72 || (e.numShipService || 0) === 0 ? "selected='true'" : "") + ">USPS Priority</option>";
                                    order += "<option value='73' " + ((e.numShipService || 0) === 73 ? "selected='true'" : "") + ">USPS Parcel Post</option>";
                                    order += "<option value='74' " + ((e.numShipService || 0) === 74 ? "selected='true'" : "") + ">USPS Bound Printed Matter</option>";
                                    order += "<option value='75' " + ((e.numShipService || 0) === 75 ? "selected='true'" : "") + ">USPS Media</option>";
                                    order += "<option value='76' " + ((e.numShipService || 0) === 76 ? "selected='true'" : "") + ">USPS Library</option>";
                                }
                                order += "</select>";
                                order += "</div>";
                                order += "<button type='button' style='margin-left:5px' onClick='return CreateShippingLabel(" + e.numOppId.toString() + "," +  parseInt(e.numOppBizDocsId || 0).toString() + ",0);' class='btn btn-flat btn-primary'>+ Label/Track#</button>";
                                order += "<a style='margin-left:5px' id='btnShippingDetail' href='javascript:OpenShippingDetail(" + e.numOppId.toString() + "," + parseInt(e.numOppBizDocsId || 0).toString() + ",0);'>?</a>";
                                order += "<div class='form-group divNewShipRate' style='display:none'>";
                                order += "<label class='lblNewShipRate' style='margin-left: 10px;color: #009200;margin-right: 5px;'></label>";
                                order += "<a href='javascript:UpdateNewShippingRate(\"divOrder" + e.numOppId.toString() + "~" + parseInt(e.numOppBizDocsId || 0).toString() + "\")' class='btn btn-primary btn-xs' style='border-radius:50%'><i class='fa fa-plus'></i></a>";
                                order += "</div>";
                                order += "</div>";
                                order += "</td>";
                                order += "</tr>";
                                order += "</table>";
                                order += "<table style='width:100%; white-space:nowrap;'>";
                                order += "<tr>";
                                order += "<td style='min-width: 300px; width:30%'>";
                                order += "<ul class='list-unstyled ulItems'>";
                                order += "<li style='background-color:inherit;border:0px;padding:0px;line-height:15px;color: #000;'><input type='checkbox' class='chkShipAll' checked='checked' onChange='return ItemsToShipSelectAllCahnged(this);'/>Select All<label class='pull-right' style='width:40px'>Rem.</label><label class='pull-right' style='width:50px'>Packed</label></li>"
                                order += "<li id='" + e.numoppitemtCode.toString() + "'><input type='checkbox' class='chkShip' id='chk" + e.numoppitemtCode.toString() + "' checked='checked' /><span class='spnItemName'>" + replaceNull(e.vcItemName) + "</span><input type='hidden' class='hdnQtyToPack' value='" + qtyToPack + "'/><input type='text' class='form-control txtQtyToPack pull-right' style='width:35px;height:25px;padding:0px;' value='" + (e.numTotalQty || 0) + "'/><label class='lblQtyPacked pull-right' style='width:35px;height:25px;padding:0px;'>" + packedQty.toString() + "</label></li>";
                                order += "</ul>";
                                order += "</td>";
                                order += "<td style='min-width: 250px;'>";
                                order += "<ul class='list-inline ulBoxes'>";
                                order += boxUI;
                                order += "</ul>";
                                order += "</td>";
                                order += "</tr>";
                                order += "</table>";

                                order += "</div>";
                                order += "</div>";


                                $("#divGridRightPack").append(order);   
                            }
                                    
                        });

                        enableDragDrop();

                    } catch (err) {
                        alert("Unknown error occurred.");
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    if(IsJsonString(jqXHR.responseText)){
                        var objError = $.parseJSON(jqXHR.responseText)
                        alert("Error occurred: " + replaceNull(objError.ErrorMessage));
                    } else {
                        alert("Unknown error ocurred");
                    }
                }
            });
        } else {
            //Remove order packing
            if(oppID > 0 && oppItemID > 0){
                if($("#divGridRightPack").find("[id='divOrder" + oppID.toString() + "~" + oppBizDocID.toString() + "']").length > 0){
                    $("#divGridRightPack").find("[id='divOrder" + oppID.toString() + "~" + oppBizDocID.toString() + "']").find(".ulItems > li[id='" + oppItemID + "']").remove();

                    $("#divGridRightPack").find("[id='divOrder" + oppID.toString() + "~" + oppBizDocID.toString() + "']").find(".ulBoxes").each(function(index,box){
                        $(box).find(".ulBoxedItems > li").each(function(index,n){
                            if(parseInt($(n).closest("li").attr("id")) === oppItemID){
                                $(n).remove();
                            }
                        });
                    });

                    $("#divGridRightPack").find("[id='divOrder" + oppID.toString() + "~" + oppBizDocID.toString() + "']").find(".ulBoxes > li").each(function(index,box){
                        if($(box).find(".ulBoxedItems > li").length === 0){
                            $(box).remove();
                        }
                    });

                    if($("#divGridRightPack").find("[id='divOrder" + oppID.toString() + "~" + oppBizDocID.toString() + "']").find(".ulItems > li").length === 1){
                        $("#divGridRightPack").find("[id='divOrder" + oppID.toString() + "~" + oppBizDocID.toString() + "']").remove();
                    }
                } 
            } else {
                $("#divGridRightPack").find("[id='divOrder" + oppID.toString() + "~" + oppBizDocID.toString() + "']").remove();
            }
        }
    } catch (e) {
        alert("Unknown error occurred while loading selecting records for packing");
    }
}

function LoadShippingBizDoc(chkSelect){
    try {
        var oppID = parseInt($(chkSelect).closest("tr").find("#hdnOppID").val());
        var oppItemID = parseInt($(chkSelect).closest("tr").find("#hdnOppItemID").val());
        var oppBizDocID = parseInt($(chkSelect).closest("tr").find("#hdnOppBizDocID").val());
        
        if($(chkSelect).is(":checked")){
            $.ajax({
                type: "POST",
                url: '../opportunity/MassSalesFulfillmentService.svc/GetRecordsForPacking',
                contentType: "application/json",
                dataType: "json",
                data: JSON.stringify({
                    "oppID": oppID
                    , "oppItemID": oppItemID
                    , "oppBizDocID": oppBizDocID
                }),
                success: function (data) {
                    try {
                        var obj = $.parseJSON(data.GetRecordsForPackingResult);

                        var boxes = $.parseJSON(obj.Boxes);
                        var boxItems = $.parseJSON(obj.BoxItems);
                        var bizDocItems = $.parseJSON(obj.BizDocItems);
                            
                        if(boxes.length > 0){
                            if($("#divGridRightPack").find("[id$='divOrder" + oppID.toString() + "~" + oppBizDocID.toString() + "']").length === 0){
                                var order = "";
                                var boxUI = "";

                                order += "<div class='box box-primary' id='divOrder" + oppID.toString() + "~" + oppBizDocID.toString() + "'>";
                                order += "<div class='box-header with-border'>";
                                order += "<h3 class='box-title'>" + replaceNull(boxes[0].vcpOppName) + "</h3>";
                                order += "<div class='box-tools pull-right'>";
                                order += "<ul class='list-inline'><li>Current ship rate:</li><li>" + (parseFloat(boxes[0].monCurrentShipRate || 0) > 0 ? ($("[id$=hdnMSCurrencyID]").val() + parseFloat(boxes[0].monCurrentShipRate || 0)) : "Needs rate") + "</li>";
                                order += "<li>";
                                order += "<button type='button' class='btn btn-box-tool' data-widget='collapse'><i class='fa fa-minus'></i></button><input type='hidden' id='hdnShippingDetail' />";
                                order += "</li></ul>";
                                order += "</div>";
                                order += "</div>";
                                order += "<div class='box-body order-box-body' style='overflow-y:auto; overflow-x:auto padding:5px'>";
                                order += "<table style='width:100%; white-space:nowrap;'>";
                                order += "<tr>";
                                order += "<td style='padding:5px;'>";
                                order += "</td>";
                                order += "<td style='padding:5px;text-align: right;'>";
                                order += "<a style='margin-left:5px' id='btnShippingDetail' href='javascript:OpenShippingDetail(" + oppID.toString() + "," + oppBizDocID.toString() + "," + boxes[0].numShippingReportID.toString() + ");'>?</a>";
                                order += "<button type='button' style='margin-left:5px' onClick='return CreateShippingLabel(" + oppID.toString() + "," + oppBizDocID.toString() + "," + boxes[0].numShippingReportID.toString() + ");' class='btn btn-flat btn-primary'>Create Label & Tracking#</button>";
                                order += "</td>";
                                order += "</tr>";
                                order += "</table>";
                                order += "<table style='width:100%; white-space:nowrap;'>";
                                order += "<tr>";
                                order += "<td style='min-width: 300px; width:30%'>";
                                order += "<ul class='list-unstyled ulItems'>";
                                order += "<li style='background-color:inherit;border:0px;padding:0px;line-height:15px;color: #000;'><input type='checkbox' class='chkShipAll' checked='checked' onChange='return ItemsToShipSelectAllCahnged(this);'/>Select All<label class='pull-right' style='width:40px'>Rem.</label><label class='pull-right' style='width:50px'>Packed</label></li>"
                                order += "</ul>";
                                order += "</td>";
                                order += "<td style='min-width: 250px;'>";
                                order += "<ul class='list-inline ulBoxes'>";

                                boxes.forEach(function(e){
                                    var boxID = e.numBoxID.toString();

                                    order += "<li>";
                                    order += "<button id='btnAddToBox" + boxID + "' class='btn btn-block btn-flat' style='background-color: #bf9000;color: #fff;' onClick='return AddItemToBox(this)'>Add to this box</button>";
                                    order += "<div class='box box-default' style='margin-top:5px;' id='" + boxID + "'>";
                                    order += "<div class='box-header with-border' style='padding:5px;'><h3 class='box-title'>" + replaceNull(e.vcBoxName) + "</h3>";
                                    order += "<div class='box-tools pull-right'>";
                                    order += "</div>";
                                    order += "</div>";
                                    order += "<div class='box-body order-box-body' style='padding:5px'>";
                                    order += "<div class='form-inline'>";
                                    order += "<div class='form-group'><input type='hidden' class='hdnBoxWeightChanged' value='0' /><input type='textbox' style='width:55px' class='form-control txtBoxWeight' value='" + (e.fltTotalWeight || 0).toString() + "' disabled/> lbs</div>";
                                    order += "<div class='pull-right'><div class='form-group'><label> L</label><input type='text' id='length' value='" + (e.fltLength || 0).toString() + "' class='form-control' style='width:45px; padding:2px' /></div><div class='form-group'><label> x W</label><input type='text' id='width' value='" + (e.fltWidth || 0).toString() + "' class='form-control' style='width:45px; padding:2px' /></div><div class='form-group'><label> x H</label><input type='text' id='height' value='" + (e.fltHeight || 0).toString() + "' class='form-control' style='width:45px; padding:2px' /></div></div>"; 
                                    order += "</div>";
                                    order += "<ul class='list-unstyled ui-sortable ulBoxedItems'>";

                                    boxItems.forEach(function(boxItem){
                                        if(boxItem.numBoxID === e.numBoxID){
                                            order += "<li id='" + boxItem.numOppBizDocItemID.toString() + "'>" + replaceNull(boxItem.vcItemName) + "<span class='AddedItemQty pull-right' style='padding-right:10px;'>" + (boxItem.intBoxQty || 0) + "</span></li>";
                                        }
                                    });

                                    order += "</ul>";
                                    order += "</div>";
                                    order += "</div>";
                                    order += "</li>";
                                });

                                order += "</ul>";
                                order += "</td>";
                                order += "</tr>";
                                order += "</table>";

                                order += "</div>";
                                order += "</div>";


                                $("#divGridRightPack").append(order);  

                                
                            }
                            
                        } else {
                            bizDocItems.forEach(function (e) {
                                var order = "";
                                var boxUI = "";
                                var packedQty = 0;
                                var boxCount = 1;
                                var qtyToPack = e.numTotalQty || 0;

                                if($("#divGridRightPack").find("[id$='divOrder" + e.numOppID.toString() + "~" + parseInt(e.numOppBizDocsId || 0).toString() + "']").length > 0){
                                    if(e.numoppitemtCode > 0 && $("#divGridRightPack").find("[id$='divOrder" + e.numOppID.toString() + "~" + parseInt(e.numOppBizDocsId || 0).toString() + "']").find(".ulBoxedItems li[id='" + e.numoppitemtCode.toString() + "']").length === 0){
                                        if(parseInt(e.numContainer) > 0 && (e.numNoItemIntoContainer || 0) > 0 && (e.numTotalQty || 0) > 0) {
                                            $("#divGridRightPack").find("[id$='divOrder" + e.numOppID.toString() + "~" + parseInt(e.numOppBizDocsId || 0).toString() + "']").find("div[id^='box~']").each(function(index,box){
                                                var numContainerID = parseInt($(box).attr("id").split("~")[2]);
                                                var numNumberItemIntoContainer = parseFloat($(box).attr("id").split("~")[3]);

                                                if(parseInt(e.numContainer) === numContainerID && parseFloat(e.numNoItemIntoContainer || 0) === numNumberItemIntoContainer){
                                                    var numBoxedQuantity = 0;

                                                    $(box).find("li").each(function(index,item){
                                                        numBoxedQuantity += parseFloat($(item).find(".AddedItemQty").text())
                                                    });

                                                    if(numBoxedQuantity < numNumberItemIntoContainer){
                                                        $(box).find("ul.ulBoxedItems").append("<li id='" + e.numoppitemtCode.toString() + "'>" + replaceNull(e.vcItemName) + "<button class='btn btn-xs btn-danger pull-right' onclick='return DeleteBoxedItem(this)'><i class='fa fa-trash-o'></i></button><span class='AddedItemQty pull-right' style='padding-right:10px;'>" + (e.numTotalQty > (numNumberItemIntoContainer - numBoxedQuantity) ? (numNumberItemIntoContainer - numBoxedQuantity) : e.numTotalQty) + "</span></li>");

                                                        if(e.numTotalQty > (numNumberItemIntoContainer - numBoxedQuantity)){
                                                            e.numTotalQty = e.numTotalQty - (numNumberItemIntoContainer - numBoxedQuantity);
                                                            packedQty+=(numNumberItemIntoContainer - numBoxedQuantity);
                                                        } else {
                                                            packedQty+=e.numTotalQty;
                                                            e.numTotalQty = 0;
                                                        }
                                                    }
                                                }

                                                boxCount += 1;
                                            });

                                            while(e.numTotalQty > 0){
                                                var boxID = "box~" + boxCount + "~" + e.numContainer.toString() + "~" + (e.numNoItemIntoContainer || 0).toString();

                                                boxUI += "<li>";
                                                boxUI += "<button id='btnAddToBox" + boxID + "' class='btn btn-block btn-flat' style='background-color: #bf9000;color: #fff;' onClick='return AddItemToBox(this)'>Add to this box</button>";
                                                boxUI += "<div class='box box-default' style='margin-top:5px;' id='" + boxID + "'>";
                                                boxUI += "<div class='box-header with-border' style='padding:5px;'><h3 class='box-title'>Box<span class='spnBoxNumber'>" + boxCount + "</span>: " + replaceNull(e.vcContainer) + "</h3>";
                                                boxUI += "<div class='box-tools pull-right'>";
                                                boxUI += "<div class='form-inline'>";
                                                boxUI += "<button type='button' class='btn btn-xs btn-danger btn-delete-box' onClick='return DeleteShippingBox(\"" + boxID + "\");'><i class='fa fa-trash-o'></i></button>";
                                                boxUI += "</div>";
                                                boxUI += "</div>";
                                                boxUI += "</div>";
                                                boxUI += "<div class='box-body order-box-body' style='padding:5px'>";
                                                boxUI += "<div class='row'>";
                                                boxUI += "<div class='pull-left'>";
                                                boxUI += "<input type='button' class='btn btn-danger btn-flat btnChangeWeight' value='Change' onClick='return ChangeBoxWeight(\"" + boxID + "\")' />";
                                                boxUI += "</div>";
                                                boxUI += "<div class='pull-right'>";
                                                boxUI += PackageTypeUI((e.numShipVia || 0));
                                                boxUI += "</div>";
                                                boxUI += "</div>";
                                                boxUI += "<hr style='margin-top: 5px;margin-bottom: 5px;'>";
                                                boxUI += "<div class='form-inline'>";
                                                boxUI += "<div class='form-group'><input type='hidden' class='hdnBoxWeightChanged' value='0' /><input type='textbox' style='width:55px' class='form-control txtBoxWeight' value='" + (e.fltContainerWeight || 0).toString() + "' disabled/> lbs</div>";
                                                boxUI += "<div class='pull-right'><div class='form-group'><label> L</label><input type='text' id='length' value='" + (e.fltContainerLength || 0).toString() + "' class='form-control' style='width:45px; padding:2px' /></div><div class='form-group'><label> x W</label><input type='text' id='width' value='" + (e.fltContainerWidth || 0).toString() + "' class='form-control' style='width:45px; padding:2px' /></div><div class='form-group'><label> x H</label><input type='text' id='height' value='" + (e.fltContainerHeight || 0).toString() + "' class='form-control' style='width:45px; padding:2px' /></div></div>"; 
                                                boxUI += "</div>";
                                                boxUI += "<ul class='list-unstyled ui-sortable ulBoxedItems'>";
                                                boxUI += "<li id='" + e.numoppitemtCode.toString() + "'>" + replaceNull(e.vcItemName) + "<button class='btn btn-xs btn-danger pull-right' onclick='return DeleteBoxedItem(this)'><i class='fa fa-trash-o'></i></button><span class='AddedItemQty pull-right' style='padding-right:10px;'>" + (e.numTotalQty > e.numNoItemIntoContainer ? e.numNoItemIntoContainer : e.numTotalQty) + "</span></li>";
                                                boxUI += "</ul>";
                                                boxUI += "</div>";
                                                boxUI += "</div>";
                                                boxUI += "</li>";
                                                boxCount = boxCount + 1;

                                                if(e.numTotalQty > e.numNoItemIntoContainer){
                                                    e.numTotalQty = e.numTotalQty - e.numNoItemIntoContainer;
                                                    packedQty+=e.numNoItemIntoContainer;
                                                } else {
                                                    packedQty+=e.numTotalQty;
                                                    e.numTotalQty = 0;
                                                }
                                            }

                                            $("#divGridRightPack").find("[id$='divOrder" + e.numOppID.toString() + "~" + parseInt(e.numOppBizDocsId || 0).toString() + "']").find("ul.ulBoxes").append(boxUI);
                                        }

                                        $("#divGridRightPack").find("[id$='divOrder" + e.numOppID.toString() + "~" + parseInt(e.numOppBizDocsId || 0).toString() + "']").find(".ulItems").append("<li id='" + e.numoppitemtCode.toString() + "'><input type='checkbox' class='chkShip' checked='checked' id='chk" + e.numoppitemtCode.toString() + "'/><span class='spnItemName'>" + replaceNull(e.vcItemName) + "</span><input type='hidden' class='hdnQtyToPack' value='" + qtyToPack + "'/><input type='text' class='form-control txtQtyToPack pull-right' style='width:35px;height:25px;padding:0px;' value='" + (e.numTotalQty || 0) + "'/><label class='lblQtyPacked pull-right' style='width:35px;height:25px;padding:0px;'>" + packedQty.toString() + "</label></li>");
                                    }
                                } else {
                                    if((e.numContainer || 0) > 0){
                                            
                                            
                                        if((e.numNoItemIntoContainer || 0) > 0 && (e.numTotalQty || 0) > 0){
                                            while(e.numTotalQty > 0){
                                                var boxID = "box~" + boxCount + "~" + e.numContainer.toString() + "~" + (e.numNoItemIntoContainer || 0).toString();

                                                boxUI += "<li>";
                                                boxUI += "<button id='btnAddToBox" + boxID + "' class='btn btn-block btn-flat' style='background-color: #bf9000;color: #fff;' onClick='return AddItemToBox(this)'>Add to this box</button>";
                                                boxUI += "<div class='box box-default' style='margin-top:5px;' id='" + boxID + "'>";
                                                boxUI += "<div class='box-header with-border' style='padding:5px;'><h3 class='box-title'>Box<span class='spnBoxNumber'>" + boxCount + "</span>: " + replaceNull(e.vcContainer) + "</h3>";
                                                boxUI += "<div class='box-tools pull-right'>";
                                                boxUI += "<div class='form-inline'>";
                                                boxUI += "<button type='button' class='btn btn-xs btn-danger btn-delete-box' onClick='return DeleteShippingBox(\"" + boxID + "\");'><i class='fa fa-trash-o'></i></button>";
                                                boxUI += "</div>";
                                                boxUI += "</div>";
                                                boxUI += "</div>";
                                                boxUI += "<div class='box-body order-box-body' style='padding:5px'>";
                                                boxUI += "<div class='row'>";
                                                boxUI += "<div class='pull-left'>";
                                                boxUI += "<input type='button' class='btn btn-danger btn-flat btnChangeWeight' value='Change' onClick='return ChangeBoxWeight(\"" + boxID + "\")' />";
                                                boxUI += "</div>";
                                                boxUI += "<div class='pull-right'>";
                                                boxUI += PackageTypeUI((e.numShipVia || 0));
                                                boxUI += "</div>";
                                                boxUI += "</div>";
                                                boxUI += "<hr style='margin-top: 5px;margin-bottom: 5px;'>";
                                                boxUI += "<div class='form-inline'>";
                                                boxUI += "<div class='form-group'><input type='hidden' class='hdnBoxWeightChanged' value='0' /><input type='textbox' style='width:55px' class='form-control txtBoxWeight' value='" + (e.fltContainerWeight || 0).toString() + "' disabled/> lbs</div>";
                                                boxUI += "<div class='pull-right'><div class='form-group'><label> L</label><input type='text' id='length' value='" + (e.fltContainerLength || 0).toString() + "' class='form-control' style='width:45px; padding:2px' /></div><div class='form-group'><label> x W</label><input type='text' id='width' value='" + (e.fltContainerWidth || 0).toString() + "' class='form-control' style='width:45px; padding:2px' /></div><div class='form-group'><label> x H</label><input type='text' id='height' value='" + (e.fltContainerHeight || 0).toString() + "' class='form-control' style='width:45px; padding:2px' /></div></div>"; 
                                                boxUI += "</div>";
                                                boxUI += "<ul class='list-unstyled ui-sortable ulBoxedItems'>";
                                                boxUI += "<li id='" + e.numoppitemtCode.toString() + "'>" + replaceNull(e.vcItemName) + "<button class='btn btn-xs btn-danger pull-right' onclick='return DeleteBoxedItem(this)'><i class='fa fa-trash-o'></i></button><span class='AddedItemQty pull-right' style='padding-right:10px;'>" + (e.numTotalQty > e.numNoItemIntoContainer ? e.numNoItemIntoContainer : e.numTotalQty) + "</span></li>";
                                                boxUI += "</ul>";
                                                boxUI += "</div>";
                                                boxUI += "</div>";
                                                boxUI += "</li>";
                                                boxCount = boxCount + 1;

                                                if(e.numTotalQty > e.numNoItemIntoContainer){
                                                    e.numTotalQty = e.numTotalQty - e.numNoItemIntoContainer;
                                                    packedQty+=e.numNoItemIntoContainer;
                                                } else {
                                                    packedQty+=e.numTotalQty;
                                                    e.numTotalQty = 0;
                                                }
                                            }
                                        }
                                    }

                                    order += "<div class='box box-primary' id='divOrder" + e.numOppID.toString() + "~" + parseInt(e.numOppBizDocsId || 0).toString() + "'>";
                                    order += "<div class='box-header with-border'>";
                                    order += "<h3 class='box-title'>" + replaceNull(e.vcpOppName) + "</h3>";
                                    order += "<div class='box-tools pull-right'>";
                                    order += "<ul class='list-inline'><li>Current ship rate:</li><li>" + ((e.monCurrentShipRate || 0) > 0 ? ($("[id$=hdnMSCurrencyID]").val() + parseFloat(e.monCurrentShipRate || 0)) : "Needs rate") + "</li>";
                                    order += "<li>";
                                    order += "<button type='button' class='btn btn-box-tool' data-widget='collapse'><i class='fa fa-minus'></i></button><input type='hidden' id='hdnShippingDetail' />";
                                    order += "</li></ul>";
                                    order += "</div>";
                                    order += "</div>";
                                    order += "<div class='box-body order-box-body' style='overflow-y:auto; overflow-x:auto padding:5px'>";
                                    order += "<table style='width:100%; white-space:nowrap;'>";
                                    order += "<tr>";
                                    order += "<td style='padding:5px;'>";
                                    order += "<div class='form-inline'>";
                                    order += "<div class='form-group'>";
                                    order += "<select class='form-control containers' style='width:70px' onChange='return AddShippingBox(\"divOrder" + e.numOppID.toString() + "~" + parseInt(e.numOppBizDocsId || 0).toString() + "\",1,0,\"\",0,0,0,0);'>";
                                    order += "<option value='0'>Box</option>";

                                    if($("#hdnContainers").val() !== ""){
                                        var containers = $.parseJSON($("#hdnContainers").val());

                                        if (containers != null && containers.length > 0) {
                                            containers.forEach(function (e) {
                                                order += "<option value='" + replaceNull(e.ValueDimension) + "'>" + replaceNull(e.vcPackageName) + "</option>";
                                            });
                                        }
                                    }

                                    order += "</select>";
                                    order += "</div>";
                                    order += "<button type='button' style='margin-left:5px' onClick='return AddShippingBox(\"divOrder" + e.numOppID.toString() + "~" + parseInt(e.numOppBizDocsId || 0).toString() + "\",2,0,\"Custom\",0,0,0,0);' id='btnAddCustomBox' class='btn btn-flat btn-default'><img src='../images/package_add.png' title='Add Box' style='cursor: pointer' /></button>";
                                    order += "</div>";
                                    order += "</td>";
                                    order += "<td style='padding:5px;text-align: left;width: 100%;'>";
                                    order += "<div class='form-inline'>";
                                    order += "<div class='form-group'>";
                                    order += "<label></label>";
                                    order += "<select class='form-control' id='ddlShipVia" + e.numOppID.toString() + "~" + e.numOppBizDocsId.toString() + "' onChange='return ShiViaChanged(this);'>";
                                    order += "<option value='0'>Ship-via</option>";
                                    order += "<option value='91' " + ((e.numShipVia || 0) === 91 ? "selected='true'" : "")  + ">FedEx</option>";
                                    order += "<option value='88' " + ((e.numShipVia || 0) === 88 ? "selected='true'" : "")  + ">UPS</option>";
                                    order += "<option value='90' " + ((e.numShipVia || 0) === 90 ? "selected='true'" : "")  + ">USPS</option>";
                                    order += "</select>";
                                    order += "</div>";
                                    order += "<div class='form-group'>";
                                    order += "<label></label>";
                                    order += "<select class='form-control' style='width:120px;' id='ddlShipService" + e.numOppID.toString() + "~" + e.numOppBizDocsId.toString() + "'>";
                                    order += "<option value='0'>Ship-service</option>";
                                    if((e.numShipVia || 0) == 91){
                                        order += "<option value='10'>FedEx Priority Overnight</option>";
                                        order += "<option value='11'>FedEx Standard Overnight</option>";
                                        order += "<option value='12'>FedEx Overnight</option>";
                                        order += "<option value='13'>FedEx 2nd Day</option>";
                                        order += "<option value='14'>FedEx Express Saver</option>";
                                        order += "<option value='15' selected='selected'>FedEx Ground</option>";
                                        order += "<option value='16'>FedEx Ground Home Delivery</option>";
                                        order += "<option value='17'>FedEx 1 Day Freight</option>";
                                        order += "<option value='18'>FedEx 2 Day Freight</option>";
                                        order += "<option value='19'>FedEx 3 Day Freight</option>";
                                        order += "<option value='20'>FedEx International Priority</option>";
                                        order += "<option value='21'>FedEx International Priority Distribution</option>";
                                        order += "<option value='22'>FedEx International Economy</option>";
                                        order += "<option value='23'>FedEx International Economy Distribution</option>";
                                        order += "<option value='24'>FedEx International First</option>";
                                        order += "<option value='25'>FedEx International Priority Freight</option>";
                                        order += "<option value='26'>FedEx International Economy Freight</option>";
                                        order += "<option value='27'>FedEx International Distribution Freight</option>";
                                        order += "<option value='28'>FedEx Europe International Priority</option>";
                                    } else if ((e.numShipVia || 0) == 88){
                                        order += "<option value='40'>UPS Next Day Air</option>";
                                        order += "<option value='42'>UPS 2nd Day Air</option>";
                                        order += "<option value='43' selected='selected'>UPS Ground</option>";
                                        order += "<option value='48'>UPS 3Day Select</option>";
                                        order += "<option value='49'>UPS Next Day Air Saver</option>";
                                        order += "<option value='50'>UPS Saver</option>";
                                        order += "<option value='51'>UPS Next Day Air Early A.M.</option>";
                                        order += "<option value='55'>UPS 2nd Day Air AM</option>";
                                    } else if ((e.numShipVia || 0) == 90){
                                        order += "<option value='70'>USPS Express</option>";
                                        order += "<option value='71'>USPS First Class</option>";
                                        order += "<option value='72' selected='selected'>USPS Priority</option>";
                                        order += "<option value='73'>USPS Parcel Post</option>";
                                        order += "<option value='74'>USPS Bound Printed Matter</option>";
                                        order += "<option value='75'>USPS Media</option>";
                                        order += "<option value='76'>USPS Library</option>";
                                    }
                                    order += "</select>";
                                    order += "</div>";
                                    order += "<button type='button' style='margin-left:5px' onClick='return CreateShippingLabel(" + e.numOppID.toString() + "," + parseInt(e.numOppBizDocsId || 0).toString() + ",0);' class='btn btn-flat btn-primary'>+ Label/Track#</button>";
                                    order += "<a style='margin-left:5px' id='btnShippingDetail' href='javascript:OpenShippingDetail(" + e.numOppID.toString() + "," + parseInt(e.numOppBizDocsId || 0).toString() + ",0);' class='btn btn-flat btn-primary'>?</a>";
                                    order += "<div class='form-group divNewShipRate' style='display:none'>";
                                    order += "<label class='lblNewShipRate' style='margin-left: 10px;color: #009200;margin-right: 5px;'></label>";
                                    order += "<a href='javascript:UpdateNewShippingRate(\"divOrder" + e.numOppID.toString() + "~" + parseInt(e.numOppBizDocsId || 0).toString() + "\")' class='btn btn-primary btn-xs' style='border-radius:50%'><i class='fa fa-plus'></i></a>";
                                    order += "</div>";
                                    order += "</div>";
                                    order += "</td>";
                                    order += "</tr>";
                                    order += "</table>";
                                    order += "<table style='width:100%; white-space:nowrap;'>";
                                    order += "<tr>";
                                    order += "<td style='min-width: 300px; width:30%'>";
                                    order += "<ul class='list-unstyled ulItems'>";
                                    order += "<li style='background-color:inherit;border:0px;padding:0px;line-height:15px;color: #000;'><input type='checkbox' class='chkShipAll' checked='checked' onChange='return ItemsToShipSelectAllCahnged(this);'/>Select All<label class='pull-right' style='width:40px'>Rem.</label><label class='pull-right' style='width:50px'>Packed</label></li>"
                                    order += "<li id='" + e.numoppitemtCode.toString() + "'><input type='checkbox' class='chkShip' id='chk" + e.numoppitemtCode.toString() + "' checked='checked' /><span class='spnItemName'>" + replaceNull(e.vcItemName) + "</span><input type='hidden' class='hdnQtyToPack' value='" + qtyToPack + "'/><input type='text' class='form-control txtQtyToPack pull-right' style='width:35px;height:25px;padding:0px;' value='" + (e.numTotalQty || 0) + "'/><label class='lblQtyPacked pull-right' style='width:35px;height:25px;padding:0px;'>" + packedQty.toString() + "</label></li>";
                                    order += "</ul>";
                                    order += "</td>";
                                    order += "<td style='min-width: 250px;'>";
                                    order += "<ul class='list-inline ulBoxes'>";
                                    order += boxUI;
                                    order += "</ul>";
                                    order += "</td>";
                                    order += "</tr>";
                                    order += "</table>";

                                    order += "</div>";
                                    order += "</div>";


                                    $("#divGridRightPack").append(order);   
                                }
                                    
                            });

                            enableDragDrop();
                        }
                    } catch (err) {
                        alert("Unknown error occurred.");
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    if(IsJsonString(jqXHR.responseText)){
                        var objError = $.parseJSON(jqXHR.responseText)
                        alert("Error occurred: " + replaceNull(objError.ErrorMessage));
                    } else {
                        alert("Unknown error ocurred");
                    }
                }
            });
        } else {
            //Remove order packing
            if(oppID > 0 && oppItemID > 0){
                if($("#divGridRightPack").find("[id='divOrder" + oppID.toString() + "~" + oppBizDocID.toString() + "']").length > 0){
                    $("#divGridRightPack").find("[id='divOrder" + oppID.toString() + "~" + oppBizDocID.toString() + "']").find(".ulItems > li[id='" + oppItemID + "']").remove();

                    $("#divGridRightPack").find("[id='divOrder" + oppID.toString() + "~" + oppBizDocID.toString() + "']").find(".ulBoxes").each(function(index,box){
                        $(box).find(".ulBoxedItems > li").each(function(index,n){
                            if(parseInt($(n).closest("li").attr("id")) === oppItemID){
                                $(n).remove();
                            }
                        });
                    });

                    $("#divGridRightPack").find("[id='divOrder" + oppID.toString() + "~" + oppBizDocID.toString() + "']").find(".ulBoxes > li").each(function(index,box){
                        if($(box).find(".ulBoxedItems > li").length === 0){
                            $(box).remove();
                        }
                    });

                    if($("#divGridRightPack").find("[id='divOrder" + oppID.toString() + "~" + oppBizDocID.toString() + "']").find(".ulItems > li").length === 1){
                        $("#divGridRightPack").find("[id='divOrder" + oppID.toString() + "~" + oppBizDocID.toString() + "']").remove();
                    }
                } 
            } else {
                $("#divGridRightPack").find("[id='divOrder" + oppID.toString() + "~" + oppBizDocID.toString() + "']").remove();
            }
        }
    } catch (e) {
        alert("Unknown error occurred while loading selecting records for packing");
    }
}

function replaceNull(value) {
    return String(value) === "null" || String(value) === "undefined" ? "" : value.toString().replace(/'/g,"&#39;");
}

function RecordSelected(chkSelect) {
    try {
        if ($("#hdnViewID").val() === "1" && $("#ddlBatchRight").val() === "0") {
            LoadSelectedRecordsToRightPaneForPicking();
        } else if ($("#hdnViewID").val() === "3" && JSON.parse($("#hdnEnableFulfillmentBizDocMapping").val()) && $(chkSelect).is(":checked")) {
            var bizdocs = $(chkSelect).closest("tr").find("#hdnLinkingBizDocs").val().trim();
            var hdnParentBizDoc = $(chkSelect).closest("tr").find("[id*=hdnParentBizDoc]")[0];
            var hdnPreselectedParentBizDoc = $(chkSelect).closest("tr").find("[id*=hdnPreselectedParentBizDoc]")[0];

            if (bizdocs != "[]" && $(hdnPreselectedParentBizDoc).val() == "0") {
                var html = "<ul class='list-unstyled'>";

                var arrBizDocs = JSON.parse(bizdocs);

                var rowIndex = 1;
                arrBizDocs.forEach(function (item) {
                    html += "<li><input id='parentBizDoc" + rowIndex + "' type='radio' name='parentBizDoc' " + (parseInt($(hdnParentBizDoc).val()) == item.numOppBizDocsId ? "checked" : "") + " bizDocID='" + (item.numOppBizDocsId) + "'/> <label for='parentBizDoc" + rowIndex + "'>" + replaceNull(item.vcBizDocName) + "</label></li>";
                    rowIndex += 1;
                });

                html += "</ul>";
                html += "</br>";
                html += "<input type='checkbox' id='chkUseSameForAll' /> <label for='chkUseSameForAll'>Use selected bizdoc for all other item(s) of order.</lable>";

                $("#modal-parent-bizdoc .modal-body").html(html);
                $("#modal-parent-bizdoc #hdnRecordID").val($(hdnParentBizDoc).attr("id"));
                $("#modal-parent-bizdoc").modal("show");
            } 
        } else if ($("#hdnViewID").val() === "2" && $("[id$=hdnPackLastSubViewMode]").val() === "2") {
            var isPickNotPackedColumnAdded = false;
            $("#divMainGrid > table > tbody > tr").not(":first").each(function (index, e) {
                if ($(e).find("#lblQtyPickedNotPacked").length > 0) {
                    isPickNotPackedColumnAdded = true;
                }

                return false;
            });

            if (isPickNotPackedColumnAdded) {
                if (JSON.parse($("#hdnEnablePickListMapping").val()) && $(chkSelect).is(":checked")) {
                    var bizdocs = $(chkSelect).closest("tr").find("#hdnLinkingBizDocs").val().trim();
                    var hdnParentBizDoc = $(chkSelect).closest("tr").find("[id*=hdnParentBizDoc]")[0];
                    var hdnPreselectedParentBizDoc = $(chkSelect).closest("tr").find("[id*=hdnPreselectedParentBizDoc]")[0];

                    if (bizdocs != "[]" && $(hdnPreselectedParentBizDoc).val() == "0") {
                        var html = "<ul class='list-unstyled'>";

                        var arrBizDocs = JSON.parse(bizdocs);

                        var rowIndex = 1;
                        arrBizDocs.forEach(function (item) {
                            html += "<li><input id='parentBizDoc" + rowIndex + "' type='radio' name='parentBizDoc' " + (parseInt($(hdnParentBizDoc).val()) == item.numOppBizDocsId ? "checked" : "") + " bizDocID='" + (item.numOppBizDocsId) + "'/> <label for='parentBizDoc" + rowIndex + "'>" + replaceNull(item.vcBizDocName) + "</label></li>";
                            rowIndex += 1;
                        });

                        html += "</ul>";
                        html += "</br>";
                        html += "<input type='checkbox' id='chkUseSameForAll' /> <label for='chkUseSameForAll'>Use selected bizdoc for all other item(s) of order.</lable>";

                        $("#modal-parent-bizdoc .modal-body").html(html);
                        $("#modal-parent-bizdoc #hdnRecordID").val($(hdnParentBizDoc).attr("id"));
                        $("#modal-parent-bizdoc").modal("show");
                    }
                }


                if (parseInt($(chkSelect).closest("tr").find("#hdnOppBizDocID").val()) > 0) {
                    LoadShippingBizDoc(chkSelect);
                } else {
                    LoadSelectedRecordsToRightPaneForPacking(chkSelect);
                }
            } else {
                alert("Please add \"Picked Not Packed\" column to list");
            }
        }
    }
    catch (e) {
        alert("Unknown error occurred while loading selecting records for picking");
    }
}

function SaveParentBizDoc(){
    var parentBizDoc = parseInt($("#modal-parent-bizdoc .modal-body input[name='parentBizDoc']:checked").attr("bizDocID"));
    $("#" + $("#hdnRecordID").val()).val(parentBizDoc);

    if($("#modal-parent-bizdoc #chkUseSameForAll").is(":checked")){
        var oppID = $("#" + $("#hdnRecordID").val()).closest("tr").find("#hdnOppID").val();

        $("#divMainGrid > table > tbody > tr").not(":first").each(function (index, e) {
            if(oppID == parseInt($(e).find("#hdnOppID").val())) {
                $(e).find("[id*=hdnParentBizDoc]").val(parentBizDoc);
                $(e).find("[id*=hdnPreselectedParentBizDoc]").val("1");
            }             
        });
    }
    
    $("#modal-parent-bizdoc").modal("hide");
}

function FocusRow(txtPicked) {
    $('#tblRight > tbody > tr').removeClass("focusedRow");
    $(txtPicked).closest("tr").addClass("focusedRow");
    $(txtPicked).val($(txtPicked).val());
}

function FocusParentRow(txtPicked) {
    $('#tblRight > tbody > tr').removeClass("focusedRow");
    $(txtPicked).closest("table.tblPickLocation").closest("tr").addClass("focusedRow");
    $(txtPicked).val($(txtPicked).val());
}

function ItemPicked(txtScan) {
    try {
        if (event.keyCode == 13) {
            var valueToCompre = "";

            var foundRows = $('#tblRight > tbody > tr').filter(function () {
                if ($("#hdnScanField").val() === "2") {
                    valueToCompre = $(this).find("#hdnUPC").val().trim();
                } else if ($("#hdnScanField").val() === "3") {
                    valueToCompre = $(this).find("#hdnItemName").val().trim();
                } else {
                    valueToCompre = $(this).find("#hdnSKU").val().trim();
                }

                return $(txtScan).val().trim().toLowerCase() === valueToCompre.toLowerCase() && parseFloat($(this).find("#lblRemaining").text()) > 0;
            });

            if (foundRows.length > 0) {
                var firtRow = foundRows[0];

                $('#tblRight > tbody > tr').removeClass("focusedRow");
                $(firtRow).prependTo("#tblRight > tbody");
                $(firtRow).addClass("focusedRow");
                        
                var remainingQty = parseFloat($(firtRow).find("#lblRemaining").text()) - 1;

                if($(firtRow).find("table.tblPickLocation").length > 0){
                    $(firtRow).find(".tblPickLocation").find("tr").each(function(){
                        if((parseFloat($(this).find(".txtPicked").val()) + 1) <= parseFloat($(this).find(".hdnAvailable").val())){
                            $(this).find(".txtPicked").val(parseFloat($(this).find(".txtPicked").val()) + 1);
                            return false;
                        }
                    });
                }

                $(firtRow).find("#lblRemaining").text(remainingQty);
                
                if (parseFloat($(firtRow).find("#lblRemaining").text()) === 0) {
                    $(firtRow).find("#lblRemaining").removeClass("bg-red").addClass("bg-green");
                }

                if (remainingQty === 0) {
                    $(firtRow).addClass("pickedItem");

                    animateCSS(firtRow,"slideOutDown",function(){
                        $(firtRow).appendTo("#tblRight");
                        $(firtRow).removeClass("focusedRow");
                    });
                }
            }

            $(txtScan).val("");
            $(txtScan).focus();
            return false;
        } else {
            return true;
        }
    } catch (e) {
        alert("Unknown error occurred.");
    }

    return false;
}

function animateCSS(element, animationName, callback) {
    const node = element;
    node.classList.add('animated','slower', animationName)

    function handleAnimationEnd() {
        node.classList.remove('animated','slower', animationName)
        node.removeEventListener('animationend', handleAnimationEnd)

        if (typeof callback === 'function') callback()
    }

    node.addEventListener('animationend', handleAnimationEnd)
}

function PackingViewChanged(mode) {
    $("[id$=hdnPackLastSubViewMode]").val(mode);
    SaveLastVisitedView(2, $("[id$=hdnPackLastSubViewMode]").val());
    $("#divPackFilters .nav li").removeClass("active");
    $("#liPackView" + mode).addClass("active");
    $("#divGridRightPack").html("");
    $("#btnCreatePackingSlip").hide(); 

    if (mode === 2) {
        $("#chkGroupBy").prop("checked", $("#hdnGroupByOrderShip").val() === "true" ? "checked" : "");
        $("#liGroupBy").show();
        $("#divPackSubFilters").show();
        $("#divShippingRates").hide();
        $("#divMainGrid").css("height", "72vh");
        $("#divExpandCollpaseGroup").hide();
        $("#divPickActionsRight").hide();
        $("#txtScan").hide();
        $("#ms-selected-grid").show();
        if ($("#ms-main-grid").hasClass("col-md-12")) {
            $("#ms-main-grid").removeClass("col-md-12").addClass("col-md-9");
        } else {
            $("#ms-main-grid").removeClass("col-md-10").addClass("col-md-7");
        }
    } else {
        $("#chkGroupBy").prop("checked","checked");
        $("#liGroupBy").hide();
        $("#divPackSubFilters").hide();
        $("#divShippingRates").show();
        $("#btnShipOrders").hide();
        $("#divMainGrid").css("height", "72vh");
        $("#divExpandCollpaseGroup").hide();
        $("#divPickActionsRight").hide();
        $("#txtScan").hide();
        $("#ms-selected-grid").hide();
        if ($("#ms-main-grid").hasClass("col-md-9")) {
            $("#ms-main-grid").removeClass("col-md-9").addClass("col-md-12");
        } else {
            $("#ms-main-grid").removeClass("col-md-7").addClass("col-md-10");
        }
    }

    LoadRecordsWithPagination();
}

function PrintBizDocViewChanged(mode){
    $("#hdnPrintBizDocViewMode").val(mode);
    $("#divPrintBizDocSubFilters .nav li").removeClass("active");
    $("#liPrintBizDocView" + mode).addClass("active");
    $("[id$=hdnPrintLastSubViewMode]").val(mode);
    SaveLastVisitedView(6, $("[id$=hdnPrintLastSubViewMode]").val());

    if(mode == 1 && $("[id$=hdnDefaultShippingBizDoc]").val() == "55206"){
        $("#btnPrintLabel").show();
    } else if (mode == 2 && $("[id$=hdnDefaultShippingBizDoc]").val() == "29397") {
        $("#btnPrintLabel").show();
    } else if (mode == 3 && $("[id$=hdnDefaultShippingBizDoc]").val() == "296") {
        $("#btnPrintLabel").show();
    } else if (mode == 4 && $("[id$=hdnDefaultShippingBizDoc]").val() == "287") {
        $("#btnPrintLabel").show();
    } else {
        $("#btnPrintLabel").hide();
    }

    LoadRecordsWithPagination();
}

function AddItemToBox(button){
    try {
        var selectedItems = 0;
        var orderID = $(button).closest("div[id^=divOrder]").attr("id");
        var boxID = $(button).attr("id").replace("btnAddToBox","");

        $("[id='" + orderID + "']").find(".ulItems > li").each(function(index,n){
            if($(n).find(".chkShip").is(":checked") && parseFloat($(n).find(".txtQtyToPack").val()) > 0){
                var qtyAddedForPack = parseFloat($(n).find(".txtQtyToPack").val());
                $("[id='" + orderID + "']").find(".ulBoxedItems > li[id=" + $(n).attr("id") + "]").find(".AddedItemQty").each(function(){
                    qtyAddedForPack+=parseFloat($(this).text());
                });

                var qtyToPack = parseFloat($(n).find(".hdnQtyToPack").val());

                if(qtyAddedForPack > qtyToPack){
                    alert("Quantity added is greater than ordered quantity.");
                    selectedItems = -1;
                } else {
                    if($("[id='" + boxID + "']").find(".ulBoxedItems > li[id=" + $(n).attr("id") + "]").length > 0){
                        addedQuantity = parseFloat($("[id='" + boxID + "']").find(".ulBoxedItems > li[id=" + $(n).attr("id") + "]").find(".AddedItemQty").text()) + parseFloat($(n).find(".txtQtyToPack").val());
                        $("[id='" + boxID + "']").find(".ulBoxedItems > li[id=" + $(n).attr("id") + "]").find(".AddedItemQty").text(addedQuantity);
                    } else {
                        $("[id='" + boxID + "']").find(".ulBoxedItems").append("<li id='" + $(n).attr("id") + "'>" + $(n).find(".spnItemName").text() + "<button class='btn btn-xs btn-danger pull-right' onclick='return DeleteBoxedItem(this)'><i class='fa fa-trash-o'></i></button><span class='AddedItemQty pull-right' style='padding-right:10px;'>" + parseFloat($(n).find(".txtQtyToPack").val()).toString() + "</span></li>");
                    }

                    $(n).find(".lblQtyPacked").text(parseFloat($(n).find(".lblQtyPacked").text()) + parseFloat($(n).find(".txtQtyToPack").val()));
                    $(n).find(".txtQtyToPack").val(parseFloat($(n).find(".txtQtyToPack").val()) - parseFloat($(n).find(".txtQtyToPack").val()));

                    selectedItems++;
                }
            }
        });

        if(selectedItems === 0){
            alert("Select atleast one item with remaining quantity greater than 0.");
        }
    } catch (e) {
        alert("Unknown error occurred while adding select item(s) to box.");
    }
            
    return false;
}
        
function DeleteBoxedItem(button){
    try {
        var numOppItemCode = parseInt($(button).closest("li").attr("id"));
        var removedQty = parseFloat($(button).closest("li").find(".AddedItemQty").text());

        $(".ulItems > li[id=" + numOppItemCode + "]").find(".lblQtyPacked").text(parseFloat($(".ulItems > li[id=" + numOppItemCode + "]").find(".lblQtyPacked").text()) - removedQty);
        $(".ulItems > li[id=" + numOppItemCode + "]").find(".txtQtyToPack").val(parseFloat($(".ulItems > li[id=" + numOppItemCode + "]").find(".txtQtyToPack").val()) + removedQty);

        $(button).closest("li").remove();
    } catch (e) {
        alert("Unknown error occured while removing item.");
    }

    return false;
}

function DeleteShippingBox(boxID){
    try {
        var parentUlBoxes = $("[id='"+ boxID + "']").closest("li").closest("ul");

        $("[id='"+ boxID + "']").find(".ulBoxedItems > li").each(function(index,n){
            var numOppItemCode = parseInt($(n).closest("li").attr("id"));
            var removedQty = parseFloat($(n).closest("li").find(".AddedItemQty").text());

            $(".ulItems > li[id=" + numOppItemCode + "]").find(".lblQtyPacked").text(parseFloat($(".ulItems > li[id=" + numOppItemCode + "]").find(".lblQtyPacked").text()) - removedQty);
            $(".ulItems > li[id=" + numOppItemCode + "]").find(".txtQtyToPack").val(parseFloat($(".ulItems > li[id=" + numOppItemCode + "]").find(".txtQtyToPack").val()) + removedQty);

            $(n).remove();
        });     
                
        $("[id='"+ boxID + "']").closest("li").remove();

        $(parentUlBoxes).find("div[id^='box~']").each(function(index,n){
            var newBoxID = "box~" + (index+ 1) + "~" + $(n).attr("id").split("~")[2] + "~" + $(n).attr("id").split("~")[3];
            $(n).closest("li").find("[id^='btnAddToBox']").attr("id","btnAddToBox" + newBoxID);
            $(n).attr("id",newBoxID);
            $(n).find(".spnBoxNumber").text(index + 1);
            $(n).closest("li").find(".btn-delete-box").attr("onClick","return DeleteShippingBox(\"" + newBoxID + "\");"); 
        });
    } catch (e) {
        alert("Unknown error occured while removing box.");
    }

    return false;
}

function AddShippingBox(divOrder, mode, boxID, boxName, weight, length, width, height){
    try {
        if(mode === 1){
            if($("[id='" + divOrder + "']").find(".containers").val() === "0"){
                alert("Select box.");
                return false;
            } else {
                var strValue = $("[id='" + divOrder + "']").find(".containers").val().split(",");
                width = strValue[1];
                height = strValue[2];
                length = strValue[3];
                weight = strValue[4];
                boxID = strValue[5];
                boxName = $("[id='" + divOrder + "']").find(".containers option:selected").text();
            }
        }

        var boxCount = $("div[id^='box~']").length;

        var boxID = "box~" + (boxCount + 1) + "~" + boxID.toString() + "~-1";

        var boxUI = "<li>";
        boxUI += "<button id='btnAddToBox" + boxID + "' class='btn btn-block btn-flat' style='background-color: #bf9000;color: #fff;' onClick='return AddItemToBox(this)'>Add to this box</button>";
        boxUI += "<div class='box box-default' style='margin-top:5px;' id='" + boxID + "'>";
        boxUI += "<div class='box-header with-border' style='padding:5px;'><h3 class='box-title'>Box<span class='spnBoxNumber'>" + (boxCount + 1) + "</span>: " + boxName + "</h3>";
        boxUI += "<div class='box-tools pull-right'>";
        boxUI += "<div class='form-inline'>";
        boxUI += "<button type='button' class='btn btn-xs btn-danger btn-delete-box' onClick='return DeleteShippingBox(\"" + boxID + "\");'><i class='fa fa-trash-o'></i></button>";
        boxUI += "</div>";
        boxUI += "</div>";
        boxUI += "</div>";
        boxUI += "<div class='box-body order-box-body' style='padding:5px'>";
        boxUI += "<div class='row'>";
        boxUI += "<div class='pull-left'>";
        boxUI += "<input type='button' class='btn btn-danger btn-flat btnChangeWeight' value='Change' onClick='return ChangeBoxWeight(\"" + boxID + "\")' />";
        boxUI += "</div>";
        boxUI += "<div class='pull-right'>";
        boxUI += PackageTypeUI(parseInt($("[id='" + divOrder + "']").find("[id^='ddlShipVia']").val()));
        boxUI += "</div>";
        boxUI += "</div>";
        boxUI += "<hr style='margin-top: 5px;margin-bottom: 5px;'>";
        boxUI += "<div class='form-inline'>";
        boxUI += "<div class='form-group'><input type='hidden' class='hdnBoxWeightChanged' value='0' /><input type='textbox' class='form-control txtBoxWeight' style='width:55px' value='" + weight.toString() + "' disabled/> lbs</div>";
        boxUI += "<div class='pull-right'><div class='form-group'><label> L</label><input type='text' id='length' value='" + length.toString() + "' class='form-control' style='width:45px; padding:2px' /></div><div class='form-group'><label> x W</label><input type='text' id='width' value='" + width.toString() + "' class='form-control' style='width:45px; padding:2px' /></div><div class='form-group'><label> x H</label><input type='text' id='height' value='" + height.toString() + "' class='form-control' style='width:45px; padding:2px' /></div></div>"; 
        boxUI += "</div>";
        boxUI += "<ul class='list-unstyled ui-sortable ulBoxedItems'>";
        boxUI += "</ul>";
        boxUI += "</div>";
        boxUI += "</div>";
        boxUI += "</li>";

        $("[id='" + divOrder + "']").find(".ulBoxes").append(boxUI);

        enableDragDrop();
    
        return false;
    } catch (e) {
        alert("Unknown error occured while adding shipping box;");
    }
}

function ItemsToShipSelectAllCahnged(chkShipAll){
    var orderID = $(chkShipAll).closest("div[id^=divOrder]").attr("id");

    $("[id='" + orderID + "'] .chkShip").prop("checked", $(chkShipAll).is(":checked"));
    return false;
}

function enableDragDrop(){
    $(".ulBoxedItems").droppable({
        greedy: true,
        drop: function(ev, ui) {
            if(parseFloat($(ui.draggable).find(".txtQtyToPack").val()) > 0) {
                if($(this).find("li[id=" + $(ui.draggable).attr("id") + "]").length > 0){
                    addedQuantity = parseFloat($(this).find("li[id=" + $(ui.draggable).attr("id") + "]").find(".AddedItemQty").text()) + parseFloat($(ui.draggable).find(".txtQtyToPack").val());
                    $(this).find("li[id=" + $(ui.draggable).attr("id") + "]").find(".AddedItemQty").text(addedQuantity);
                } else {
                    $(this).append("<li id='" + $(ui.draggable).attr("id") + "'>" + $(ui.draggable).find(".spnItemName").text() + "<button class='btn btn-xs btn-danger pull-right' onclick='return DeleteBoxedItem(this)'><i class='fa fa-trash-o'></i></button><span class='AddedItemQty pull-right' style='padding-right:10px;'>" + parseFloat($(ui.draggable).find(".txtQtyToPack").val()).toString() + "</span></li>");
                }

                $(ui.draggable).find(".lblQtyPacked").text(parseFloat($(ui.draggable).find(".lblQtyPacked").text()) + parseFloat($(ui.draggable).find(".txtQtyToPack").val()));
                $(ui.draggable).find(".txtQtyToPack").val(parseFloat($(ui.draggable).find(".txtQtyToPack").val()) - parseFloat($(ui.draggable).find(".txtQtyToPack").val()));

                return true;
            } else {
                alert("Remaining quantity is 0");
                return false;
            }
        }
    });

    $(".ulItems > li:not(:first-child)").draggable({
        connectToSortable: ".ulBoxedItems",
        helper: "clone",
        revert: 'invalid'
    });
}

function CreateShippingLabel(oppID, oppBizDocID, shippingReportID){
    try {
        if(parseInt($("#hdnDefaultShippingBizDocType").val()) > 0){
            var shipVia = 0;
            var shipService = 0;
            var selectdRecords = [];
            var boxes = [];
            var boxItems = [];
            var shippingDetail = "";

            if ($("#divGridRightPack").find("[id='divOrder" + oppID + "~" + oppBizDocID + "']").find("#hdnShippingDetail").length > 0){
                shippingDetail = $("#divGridRightPack").find("[id='divOrder" + oppID + "~" + oppBizDocID + "']").find("#hdnShippingDetail").val();
            }

            if(shippingReportID === 0){
                var divID = "divOrder" + oppID.toString() + "~" + oppBizDocID.toString();

                if(parseInt($("[id='" + divID + "']").find("[id^='ddlShipVia']").val()) === 0){
                    alert("Select Ship-via");
                    $("[id='" + divID + "']").find("[id^='ddlShipVia']").focus();
                    return false;
                } else if (parseInt($("[id='" + divID + "']").find("[id^='ddlShipService']").val()) === 0){
                    alert("Select Ship-service");
                    $("[id='" + divID + "']").find("[id^='ddlShipService']").focus();
                    return false;
                }

                var isPakageTypeSelected = true;

                $("[id='" + divID + "']").find(".package-type").each(function(index,ddlPackageType){
                    if(parseInt($(ddlPackageType).val()) === 0){
                        isPakageTypeSelected = false;
                        $(ddlPackageType).focus();
                        return false;
                    }
                });

                if(!isPakageTypeSelected){
                    alert("Select Package Type");
                    return false;
                }

                $("#divGridRightPack").find("[id='" + divID + "']").find(".ulBoxes > li").each(function(boxIndex,box){
                    $(box).find(".ulBoxedItems > li").each(function(itemIndex,item){
                        var oppItemID = parseInt($(item).attr("id"));
                        var addedQty = parseFloat($(item).find(".AddedItemQty").text());

                        if(addedQty > 0) {
                            var found = $.map(selectdRecords, function(obj) {
                                if(obj.OppItemID === oppItemID) 
                                    return obj;
                            });

                            if(found.length === 0) {
                                var obj = {};
                                obj.OppItemID = oppItemID;
                                obj.Quantity = addedQty;
                                selectdRecords.push(obj);
                            } else {
                                found[0].Quantity += addedQty;
                            }

                            var foundBox = $.map(boxes, function(obj) {
                                if(obj.ID === (boxIndex + 1)) 
                                    return obj;
                            });

                            if(foundBox.length === 0) {
                                var objBox = {};
                                objBox.ID = boxIndex + 1;
                                objBox.Name = $(box).find(".box-title").text();
                                objBox.Height = parseFloat($(box).find("#height").val());
                                objBox.Width = parseFloat($(box).find("#width").val());
                                objBox.Length = parseFloat($(box).find("#length").val());
                                objBox.Weight = parseFloat($(box).find(".txtBoxWeight").val());
                                if($(box).find(".hdnBoxWeightChanged").val() === "1"){
                                    objBox.IsUserProvidedBoxWeight = true;
                                } else {
                                    objBox.IsUserProvidedBoxWeight = false;
                                }
                                objBox.PackageType = parseInt($(box).find(".package-type").val());
                                boxes.push(objBox);
                            }

                            var foundBoxItem = $.map(boxItems, function(obj) {
                                if(obj.BoxID === (boxIndex + 1) && obj.OppItemID === oppItemID) 
                                    return obj;
                            });

                            if(foundBoxItem.length === 0) {
                                var boxItem = {};
                                boxItem.BoxID = boxIndex + 1;
                                boxItem.OppItemID = oppItemID;
                                boxItem.Quantity = addedQty;
                                boxItems.push(boxItem);
                            } else {
                                foundBoxItem[0].Quantity += addedQty;
                            }
                        }
                    });
                });

                shipVia = parseInt($("[id='" + divID + "']").find("[id^='ddlShipVia']").val());
                shipService = parseInt($("[id='" + divID + "']").find("[id^='ddlShipService']").val());
            }

            if(selectdRecords.length > 0 || shippingReportID > 0){
                $.ajax({
                    type: "POST",
                    url: '../opportunity/MassSalesFulfillmentService.svc/GenerateShippingLabel',
                    contentType: "application/json",
                    dataType: "json",
                    data: JSON.stringify({
                        "oppID": oppID
                        ,"oppBizDocID": oppBizDocID
                        ,"shippingReportID": shippingReportID
                        ,"shipViaID": shipVia
                        ,"shipServiceID": shipService
                        ,"shippingDetail": shippingDetail
                        ,"bizDocType" : parseInt($("#hdnDefaultShippingBizDocType").val())
                        ,"items":JSON.stringify(selectdRecords)
                        ,"boxes":JSON.stringify(boxes)
                        ,"boxItems":JSON.stringify(boxItems)
                    }),
                    success: function (data) {
                        try {
                            var obj = $.parseJSON(data.GenerateShippingLabelResult);
                                          
                            if(obj.isSuccess){
                                $("#divGridRightPack").find("[id='divOrder" + oppID.toString() + "~" + oppBizDocID.toString() + "']").remove();

                                $("#divMainGrid > table > tbody > tr").not(":first").each(function (index, e) {
                                    if(oppBizDocID > 0){
                                        if(oppID === parseInt($(e).find("#hdnOppID").val()) && oppBizDocID === parseInt($(e).find("#hdnOppBizDocID").val())){
                                            $(e).remove();
                                        }
                                    } else {
                                        if(oppID === parseInt($(e).find("#hdnOppID").val())){
                                            $(e).remove();
                                        }
                                    }
                                });

                                if(obj.oppID > 0 && obj.oppBizDocID > 0){
                                    if(confirm("Do you want to print packing slip?")){
                                        PrintPackingSlip(obj.oppID, obj.oppBizDocID)
                                    }
                                }
                            } else {
                                alert("Error occurred while generating shipping label.");
                                PackingViewChanged(2);
                            }
                        } catch (err) {
                            alert("Unknown error occurred.");
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        if(IsJsonString(jqXHR.responseText)){
                            var objError = $.parseJSON(jqXHR.responseText)
                            alert("Error occurred: " + replaceNull(objError.ErrorMessage));
                        } else {
                            alert("Unknown error ocurred while creating shipping label");
                        }   
                    }
                });
            } else {
                alert("Please first add item(s) to boxe(s).");
            }
        } else {
            alert("Go global settings -> Accounting and Select Sales BizDoc responsible for Shipping Labels & Trackings #.");
        }
    } catch (e) {
        alert("Unknown error ocurred while creating shipping label");
    }
}

function OpenShippingDetail(oppID, oppBizDocID, shippingReportID){
    try {
        var savedShippingDetail = "";

        if ($("#divGridRightPack").find("[id='divOrder" + oppID.toString() + "~" + oppBizDocID.toString() + "']").find("#hdnShippingDetail").length > 0) {
            savedShippingDetail = $("#divGridRightPack").find("[id='divOrder" + oppID.toString() + "~" + oppBizDocID.toString() + "']").find("#hdnShippingDetail").val();
        }

        $.ajax({
            type: "POST",
            url: '../opportunity/MassSalesFulfillmentService.svc/GetOrderShippingDetail',
            contentType: "application/json",
            dataType: "json",
            data: JSON.stringify({
                "oppID": oppID
                ,"shippingReportID": shippingReportID
            }),
            success: function (data) {
                try {
                    var obj = $.parseJSON(data.GetOrderShippingDetailResult);

                    $("#ddlPaymentOption").val("0");
                    $("#divPaymentOption1").hide();
                    $("#divThirdPartyZipCode").show();
                    $("#txtReferenceNo").val("");
                    $("#txtAccountNumber").val("");
                    $("#txtZipCode").val("");
                    $("#ddlCountry").val("0");
                    $("#divShipDescription").show();
                    $("#liAdditionalHandling").hide();
                    $("#chkAdditionalHandling").removeAttr("checked");
                    $("#liCOD").hide();
                    $("#chkCOD").removeAttr("checked");
                    $("#liHDP").hide();
                    $("#chkHDP").removeAttr("checked");
                    $("#liInsideDelivery").hide();
                    $("#chkInsideDelivery").removeAttr("checked");
                    $("#liInsidePickup").hide();
                    $("#chkInsidePickup").removeAttr("checked");
                    $("#liLargePackage").hide();
                    $("#chkLargePackage").removeAttr("checked");
                    $("#liSaturdayDelivery").hide();
                    $("#chkSaturdayDelivery").removeAttr("checked");
                    $("#liSaturdayPickup").hide();
                    $("#chkSaturdayPickup").removeAttr("checked");
                    $("#ddlSignatureType").val("0");
                    $("#txtDescription").val("");
                    $("#txtInsuredValue").val("");
                    $("#txtCustomValue").val("");
                    $("#divShipCOD").hide();
                    $("#ddlCODType").val("0");
                    $("#txtCODAmount").val("");


                    var savedObj = null;

                    if (savedShippingDetail != "") {
                        savedObj = $.parseJSON(savedShippingDetail);

                        if (savedObj != null && savedObj.length > 0) {
                            savedObj = savedObj[0];
                        }
                    }
                                          
                    if(obj != null && obj.length > 0){
                        $("#hdnShipOppID").val(obj[0].numOppID);
                        $("#hdnShipOppBizDocID").val(obj[0].numOppBizDocID);
                        $("#ddlPaymentOption").val(replaceNull(obj[0].tintPayorType));

                        if(replaceNull(obj[0].tintPayorType) == 2){
                            $("#divPaymentOption1").show();
                            $("#divThirdPartyZipCode").show();
                            $("#txtAccountNumber").val(savedObj != null ? replaceNull(savedObj.AccountNo) : replaceNull(obj[0].vcPayorAccountNo));
                            $("#txtZipCode").val(savedObj != null ? replaceNull(savedObj.ZipCode) : replaceNull(obj[0].vcPayorZip));
                            $("#ddlCountry").val(savedObj != null ? replaceNull(savedObj.Country) : replaceNull(obj[0].numPayorCountry));
                        }                        

                        $("#txtFromContact").val(savedObj != null ? replaceNull(savedObj.FromContact) : replaceNull(obj[0].vcFromName));
                        $("#txtFromPhone").val(savedObj != null ? replaceNull(savedObj.FromPhone) : replaceNull(obj[0].vcFromPhone));
                        $("#txtFromCompany").val(savedObj != null ? replaceNull(savedObj.FromCompany) : replaceNull(obj[0].vcFromCompanyName));
                        $("#txtFromAddress1").val(savedObj != null ? replaceNull(savedObj.FromAddress1) : replaceNull(obj[0].vcFromStreet));
                        $("#ddlFromCountry").val(savedObj != null ? replaceNull(savedObj.FromCountry) : (obj[0].numFromCountry || 0))
                        LoadStates($("#ddlFromState"), parseInt($("#ddlFromCountry").val()), parseInt(savedObj != null ? replaceNull(savedObj.FromState) : (obj[0].numFromState || 0)));
                        $("#txtFromCity").val(savedObj != null ? replaceNull(savedObj.FromCity) : replaceNull(obj[0].vcFromCity));
                        $("#txtFromZip").val(savedObj != null ? replaceNull(savedObj.FromZip) : replaceNull(obj[0].vcFromZipCode));
                        if(obj[0].bitFromResidential){
                            $("#chkFromResidential").attr("checked","checked");
                        } else {
                            $("#chkFromResidential").removeAttr("checked");
                        }

                        $("#txtToContact").val(savedObj != null ? replaceNull(savedObj.ToContact) : replaceNull(obj[0].vcToName));
                        $("#txtToPhone").val(savedObj != null ? replaceNull(savedObj.ToPhone) : replaceNull(obj[0].vcToPhone));
                        $("#txtToCompany").val(savedObj != null ? replaceNull(savedObj.ToCompany) : replaceNull(obj[0].vcToCompanyName));
                        $("#txtToAddress1").val(savedObj != null ? replaceNull(savedObj.ToAddress1) : replaceNull(obj[0].vcToStreet));
                        $("#ddlToCountry").val(savedObj != null ? replaceNull(savedObj.ToCountry) : (obj[0].numToCountry || 0))
                        LoadStates($("#ddlToState"), parseInt($("#ddlToCountry").val()), parseInt(savedObj != null ? replaceNull(savedObj.ToState) : (obj[0].numToState || 0)));
                        $("#txtToCity").val(replaceNull(savedObj != null ? replaceNull(savedObj.ToCity) : obj[0].vcToCity));
                        $("#txtToZip").val(replaceNull(savedObj != null ? replaceNull(savedObj.ToZip) : obj[0].vcToZipCode));
                        if(obj[0].bitToResidential){
                            $("#chkToResidential").attr("checked","checked");
                        } else {
                            $("#chkToResidential").removeAttr("checked");
                        }

                        $("#chkAdditionalHandling").attr("checked", savedObj != null ? replaceNull(savedObj.IsAdditionalHandling) : (obj[0].bitAdditionalHandling ? true : false));
                        $("#chkCOD").attr("checked", savedObj != null ? replaceNull(savedObj.IsCOD) : (obj[0].bitCOD ? true : false));
                        $("#chkHDP").attr("checked", savedObj != null ? replaceNull(savedObj.IsHomeDelivery) : (obj[0].bitHomeDelivery ? true : false));
                        $("#chkInsideDelivery").attr("checked", savedObj != null ? replaceNull(savedObj.IsInsideDelivery) : (obj[0].bitInsideDelevery ? true : false));
                        $("#chkInsidePickup").attr("checked", savedObj != null ? replaceNull(savedObj.IsInsidePickup) : (obj[0].bitIsInsidePickup ? true : false));
                        $("#chkLargePackage").attr("checked", savedObj != null ? replaceNull(savedObj.IsLargePackage) : (obj[0].bitLargePackage ? true : false));
                        $("#chkSaturdayDelivery").attr("checked", savedObj != null ? replaceNull(savedObj.IsSaturdayDelivery) : (obj[0].bitSaturdayDelivery ? true : false));
                        $("#chkSaturdayPickup").attr("checked", savedObj != null ? replaceNull(savedObj.IsSaturdayPickup) : (obj[0].bitIsSaturdayPickup ? true : false));
                        $("#ddlSignatureType").val(savedObj != null ? replaceNull(savedObj.SignatureType) : (obj[0].tintSignatureType || 0));
                        $("#txtDescription").val(savedObj != null ? replaceNull(savedObj.Description) : replaceNull(obj[0].vcDescription));

                        if (savedObj != null) {
                            $("#txtInsuredValue").val(obj.TotalInsuredValue);
                            $("#txtCustomValue").val(obj.TotalCustomsValue);
                            $("#ddlCODType").val(obj.CODType);
                            $("#txtCODAmount").val(obj.CODAmount);
                        }


                        if(parseInt(obj[0].numShippingCompany) === 91){
                            $("#divShipDescription").hide();
                            $("#liCOD").show();
                            $("#liHDP").show();
                            $("#liInsideDelivery").show();
                            $("#liInsidePickup").show();
                            $("#liSaturdayDelivery").show();
                           

                            $("#ddlCODType").html("");
                            $("#ddlCODType").append("<option value='0'>-- Select One --</option>");
                            $("#ddlCODType").append("<option value='Any'>Any</option>");
                            $("#ddlCODType").append("<option value='Cash'>Cash</option>");
                            $("#ddlCODType").append("<option value='Guaranted Funds'>Guaranted Funds</option>");
                        } else if (parseInt(obj[0].numShippingCompany) === 88) {
                            $("#divShipDescription").show();
                            $("#ddlCODType").html("");
                            $("#ddlCODType").append("<option value='0'>-- Select One --</option>");
                            $("#ddlCODType").append("<option value='Any Check'>Any Check</option>");
                            $("#ddlCODType").append("<option value=\"Cashier's Check Or Money Order\">Cashier's Check Or Money Order</option>");
                            $("#ddlCODType").append("<option value='Guaranted Funds'>Guaranted Funds</option>");

                            $("#liAdditionalHandling").show();
                            $("#chkSaturdayDelivery").show();
                            $("#chkSaturdayPickup").show();
                            $("#chkLargePackage").show();
                        }
                    }

                    $("#modal-shipping-detail").modal("show");
                } catch (err) {
                    alert("Unknown error occurred.");
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                if(IsJsonString(jqXHR.responseText)){
                    var objError = $.parseJSON(jqXHR.responseText)
                    alert("Error occurred: " + replaceNull(objError.ErrorMessage));
                } else {
                    alert("Unknown error ocurred");
                }
            }
        });
    } catch (e) {
        alert("Unknown error occurred while loading order shipping detail");
    }
}

function SaveShippingDetail(){
    try {
        if (parseInt($("#ddlFromState").val()) == 0) {
            alert("Please Select From State");
            $("#ddlFromState").focus();
            return false;
        } else if (parseInt($("#ddlToState").val()) == 0) {
            alert("Please Select To State");
            $("#ddlToState").focus();
            return false;
        } else if ($("#txtFromZip").val().trim().length < 5) {
            alert("Please enter valid From Zip");
            $("#txtFromZip").focus();
            return false;
        } else if ($("#txtToZip").val().trim().length < 5) {
            alert("Please enter valid To Zip");
            $("#txtToZip").focus();
            return false;
        } else if (parseInt($("#ddlFromCountry").val()) == 0) {
            alert("Please Select From Country");
            $("#ddlFromCountry").focus();
            return false;
        } else if (parseInt($("#ddlToCountry").val()) == 0) {
            alert("Please Select To Country");
            $("#ddlToCountry").focus();
            return false;
        }
        else{
            var oppID = parseInt($("#hdnShipOppID").val());
            var oppBizDocID = parseInt($("#hdnShipOppBizDocID").val());

            var obj = {};
            obj.FromContact = $("#txtFromContact").val().trim();
            obj.FromPhone = $("#txtFromPhone").val().trim();
            obj.FromCompany = $("#txtFromCompany").val().trim();
            obj.FromAddress1 = $("#txtFromAddress1").val().trim();
            obj.FromAddress2 = $("#txtFromAddress2").val().trim();
            obj.FromCountry = parseInt($("#ddlFromCountry").val());
            obj.FromState = parseInt($("#ddlFromState").val());
            obj.FromCity = $("#txtFromCity").val().trim();
            obj.FromZip = $("#txtFromZip").val().trim();
            obj.IsFromResidential = $("#chkFromResidential").is(":checked");
            obj.ToContact = $("#txtToContact").val().trim();
            obj.ToPhone = $("#txtToPhone").val().trim();
            obj.ToCompany = $("#txtToCompany").val().trim();
            obj.ToAddress1 = $("#txtToAddress1").val().trim();
            obj.ToAddress2 = $("#txtToAddress2").val().trim();
            obj.ToCountry = parseInt($("#ddlToCountry").val());
            obj.ToState = parseInt($("#ddlToState").val());
            obj.ToCity = $("#txtToCity").val().trim();
            obj.ToZip = $("#txtToZip").val().trim();
            obj.IsToResidential = $("#chkToResidential").is(":checked");
            obj.PayerType = parseInt($("#ddlPaymentOption").val());
            obj.ReferenceNo = $("#txtReferenceNo").val().trim();
            obj.AccountNo = $("#txtAccountNumber").val().trim();
            obj.ZipCode = $("#txtZipCode").val().trim();
            obj.Country = parseInt($("#ddlCountry").val());
            obj.IsAdditionalHandling = $("#liAdditionalHandling").is(":visible") && $("#chkAdditionalHandling").is(":checked");
            obj.IsCOD = $("#liCOD").is(":visible") && $("#chkCOD").is(":checked");
            obj.IsHomeDelivery = $("#liHDP").is(":visible") && $("#chkHDP").is(":checked");
            obj.IsInsideDelivery = $("#liInsideDelivery").is(":visible") && $("#chkInsideDelivery").is(":checked");
            obj.IsInsidePickup = $("#liInsidePickup").is(":visible") && $("#chkInsidePickup").is(":checked");
            obj.IsLargePackage = $("#liLargePackage").is(":visible") && $("#chkLargePackage").is(":checked");
            obj.IsSaturdayDelivery = $("#liSaturdayDelivery").is(":visible") && $("#chkSaturdayDelivery").is(":checked");
            obj.IsSaturdayPickup = $("#liSaturdayPickup").is(":visible") && $("#chkSaturdayPickup").is(":checked");
            obj.SignatureType = parseInt($("#ddlSignatureType").val());
            obj.Description = $("#txtDescription").val().trim();
            obj.TotalInsuredValue = parseFloat($("#txtInsuredValue").val().trim());
            obj.TotalCustomsValue = parseFloat($("#txtCustomValue").val().trim());
            obj.CODType = replaceNull($("#ddlCODType").val()).trim();
            obj.CODAmount = parseFloat($("#txtCODAmount").val().trim());

            var selectedRecord = [];
            selectedRecord.push(obj);

            if ($("#divGridRightPack").find("[id='divOrder" + oppID.toString() + "~" + oppBizDocID.toString() + "']").find("#hdnShippingDetail").length > 0){
                $("#divGridRightPack").find("[id='divOrder" + oppID.toString() + "~" + oppBizDocID.toString() +  "']").find("#hdnShippingDetail").val(JSON.stringify(selectedRecord));
            }

            $("#modal-shipping-detail").modal("hide");
        }

        
    } catch (e) {
        alert("Unknown error occurred while updating shipping detail");
    }
}

function PackageTypeUI(shipViaID){
    var packageType = "<select class='form-control package-type'>";
    packageType += "<option value='0'>-- Package Type --</option>";
    if(shipViaID === 91){
        packageType += "<option value='2'>FedEx Letter</option>";
        packageType += "<option value='11'>FedEx Pak</option>";
        packageType += "<option value='12'>FedEx Box</option>";
        packageType += "<option value='13'>FedEx 10kg Box</option>";
        packageType += "<option value='14'>FedEx 25kg Box</option>";
        packageType += "<option value='28'>FedEx Tube</option>";
        packageType += "<option value='31' selected='true'>Your Packaging</option>";
    } else if (shipViaID === 88){
        packageType += "<option value='0'>None</option>";
        packageType += "<option value='2'>UPS letter</option>";
        packageType += "<option value='11'>UPS Pak</option>";
        packageType += "<option value='12'>UPS Box</option>";
        packageType += "<option value='13'>UPS 10kg Box</option>";
        packageType += "<option value='14'>UPS 25kg Box</option>";
        packageType += "<option value='15'>UPS Small Express Box</option>";
        packageType += "<option value='16'>UPS Medium Express Box</option>";
        packageType += "<option value='17'>UPS Large Express Box</option>";
        packageType += "<option value='28'>UPS Tube</option>";
        packageType += "<option value='30'>UPS Pallet</option>";
        packageType += "<option value='31' selected='true'>Your Packaging</option>";
        packageType += "<option value='33'>UPS Flats</option>";
        packageType += "<option value='34'>UPS Parcels</option>";
        packageType += "<option value='35'>UPS BPM</option>";
        packageType += "<option value='36'>UPS First Class</option>";
        packageType += "<option value='37'>UPS Priority</option>";
        packageType += "<option value='38'>UPS Machinables</option>";
        packageType += "<option value='39'>UPS Irregulars</option>";
        packageType += "<option value='40'>UPS Parcel Post</option>";
        packageType += "<option value='41'>UPS BPM Parcel</option>";
        packageType += "<option value='42'>UPS Media Mail</option>";
        packageType += "<option value='43'>UPS BPM Flat</option>";
        packageType += "<option value='44'>UPS Standard Flat</option>";
    } else if (shipViaID === 90){
        packageType += "<option value='1'>USPS Postcards</option>";
        packageType += "<option value='2'>USPS Letter</option>";
        packageType += "<option value='3'>USPS Large Envelope</option>";
        packageType += "<option value='4'>USPS Flat Rate Envelope</option>";
        packageType += "<option value='5'>USPS Flat Rate Legal Envelope</option>";
        packageType += "<option value='6'>USPS Flat Rate Padded Envelope</option>";
        packageType += "<option value='7'>USPS Flat Rate GiftCard Envelope</option>";
        packageType += "<option value='8'>USPS Flat Rate Window Envelope</option>";
        packageType += "<option value='9'>USPS Flat Rate Cardboard Envelope</option>";
        packageType += "<option value='10'>USPS Small Flat Rate Envelope</option>";
        packageType += "<option value='18'>USPS Flat Rate Box</option>";
        packageType += "<option value='19'>USPS Small Flat Rate Box</option>";
        packageType += "<option value='20'>USPS Medium Flat Rate Box</option>";
        packageType += "<option value='21'>USPS Large Flat Rate Box</option>";
        packageType += "<option value='22'>USPS DVD Flat Rate Box</option>";
        packageType += "<option value='23'>USPS Large Video Flat Rate Box</option>";
        packageType += "<option value='24'>USPS Regional Rate Box A</option>";
        packageType += "<option value='25'>USPS Regional Rate Box B</option>";
        packageType += "<option value='26'>USPS Rectangular</option>";
        packageType += "<option value='27'>USPS Non Rectangular</option>";
        packageType += "<option value='29'>USPS Matter For The Blind</option>";
        packageType += "<option value='31' selected='true'>Your Packaging</option>";
        packageType += "<option value='45'>USPS Regional Rate Box C</option>";
    }
    packageType += "</select>";

    return packageType;
}

function IsJsonString(str) {
    try {
        JSON.parse(str);
    } catch (e) {
        return false;
    }
    return true;
}

function PrintPackingSlip(oppID, oppBizDocID){
    try {
        $.ajax({
            type: "POST",
            url: '../opportunity/MassSalesFulfillmentService.svc/PrintPackingSlip',
            contentType: "application/json",
            dataType: "json",
            data: JSON.stringify({
                "oppID":oppID
                ,"oppBizDocID": oppBizDocID
            }),
            success: function (data) {
                try {
                    var sampleArr = base64ToArrayBuffer(data.PrintPackingSlipResult);

                    now = new Date();
                    year = "" + now.getFullYear();
                    month = "" + (now.getMonth() + 1); if (month.length == 1) { month = "0" + month; }
                    day = "" + now.getDate(); if (day.length == 1) { day = "0" + day; }
                    hour = "" + now.getHours(); if (hour.length == 1) { hour = "0" + hour; }
                    minute = "" + now.getMinutes(); if (minute.length == 1) { minute = "0" + minute; }
                    second = "" + now.getSeconds(); if (second.length == 1) { second = "0" + second; }

                    saveByteArray("PickingSlip" + year + month + day + hour + minute + second, sampleArr);
                } catch (err) {
                    alert("Unknown error occurred while printing packing slip.")
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                if(IsJsonString(jqXHR.responseText)){
                    var objError = $.parseJSON(jqXHR.responseText)
                    alert("Error occurred: " + replaceNull(objError.ErrorMessage));
                } else {
                    alert("Unknown error ocurred");
                }
            }
        });   
    } catch (e) {
        alert("Unknown error occurred while printing packing slip")
    }
}

function PickingViewChanged(mode){
    $("#divMobilePick .nav li").removeClass("active");
    $("#liPickView" + mode).addClass("active");

    if(mode === 1){
        $("#ms-main-grid").show();
        $("#ms-selected-grid").hide();
    } else {
        $("#ms-main-grid").hide();
        $("#ms-selected-grid").show();
    } 
}

function PickAllChanged(chkPickAll){
    $("#tblRight > tbody > tr").each(function(index,tr){
        if(parseInt($(this).find("#hdnOppChildItemID").val()) == 0 && parseInt($(this).find("#hdnOppKitChildItemID").val()) == 0){
            var remainingQty = parseFloat($(tr).find("#hdnRemainingQty").val());

            if($(this).find("table.tblPickLocation").length > 0){
                $(this).find("table.tblPickLocation").find("tr").each(function() {
                    if($(chkPickAll).is(":checked")){
                        if(parseFloat($(this).find(".hdnAvailable").val()) >= remainingQty){
                            $(this).find(".txtPicked").val(remainingQty);
                            remainingQty = 0
                        } else {
                            $(this).find(".txtPicked").val($(this).find(".hdnAvailable").val());
                            remainingQty = remainingQty - parseFloat($(this).find(".hdnAvailable").val())
                        }
                    } else {
                        $(this).find(".txtPicked").val("0");
                    }

                    ChangeKitChildQty($(this).find(".txtPicked"),true);
                });

                if($(chkPickAll).is(":checked") && $("#hdnPickWithoutInventoryCheck").val() === "true" && remainingQty > 0){
                    $(this).find("table.tblPickLocation").find("tr").each(function() {
                        if(parseInt($(this).find(".hdnWarehouseItemID").val()) > 0 && parseInt($(this).find(".hdnWarehouseLocationID").val()) === 0){
                            $(this).find(".txtPicked").val(parseFloat($(this).find(".txtPicked").val()) + remainingQty);
                            remainingQty = 0;
                            return false;
                        }
                    });
                }

                if(remainingQty > 0){
                    $(tr).find("#lblRemaining").text(remainingQty);
                    $(tr).find("#lblRemaining").removeClass("bg-green").addClass("bg-red");
                } else {
                    $(tr).find("#lblRemaining").text(0);
                    $(tr).find("#lblRemaining").removeClass("bg-red").addClass("bg-green");
                }
            } else {
                var txtPicked = $(tr).find("[id$=txtPicked]")[0];

                if($(chkPickAll).is(":checked")){
                    $(txtPicked).val(remainingQty);
                    $(txtPicked).closest("tr").find("#lblRemaining").text("0");
                    $(txtPicked).closest("tr").find("#lblRemaining").removeClass("bg-red").addClass("bg-green");
                } else {
                    $(txtPicked).closest("tr").find("#lblRemaining").text(remainingQty + parseFloat($(txtPicked).val()));
                    $(txtPicked).val("0");
                    $(txtPicked).closest("tr").find("#lblRemaining").removeClass("bg-green").addClass("bg-red");
                }

                ChangeKitChildQty(txtPicked,true);
            }
        }        
    });
}

function SaveLastVisitedView(viewID,subViewID){
    try {
        $.ajax({
            global: false,
            type: "POST",
            url: '../opportunity/MassSalesFulfillmentService.svc/SaveLastView',
            contentType: "application/json",
            dataType: "json",
            data: JSON.stringify({
                "viewID":viewID
                ,"subViewID": subViewID
            })
        });   
    } catch (e) {
        //Do not raise error
    }
}

function openReportList(a, b, c) {
    var h = screen.height;
    var w = screen.width;

    if (c > 0) {
        window.open('../opportunity/frmShippingReport.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&OppBizDocId=' + b + '&ShipReportId=' + c + '&OppId=' + a, '', 'toolbar=no,menubar=yes,titlebar=no,top=50,left=50,width=' + (w - 100) + ',height=' + (h - 200) + ',scrollbars=yes,resizable=yes');
        return false;
    }
    else {
        window.open('../opportunity/frmShippingReportList.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&OppId=' + a + '&OppBizDocId=' + b, '', 'toolbar=no,menubar=yes,titlebar=no,left=100,width=800,height=500,scrollbars=yes,resizable=yes');
        return false;
    }
}

function ChangeBoxWeight(boxID){
    $("[id='" + boxID + "']").find(".btnChangeWeight").removeClass("btn-danger").addClass("btn-success");
    $("[id='" + boxID + "']").find(".btnChangeWeight").val("Update");
    $("[id='" + boxID + "']").find(".btnChangeWeight").attr('onClick',"return GetNewShippingRate('" + boxID + "');");

    $("[id='" + boxID + "']").find(".txtBoxWeight").prop('disabled', false);
    $("[id='" + boxID + "']").find(".txtBoxWeight").val(0);
    
    return false;
}

function GetNewShippingRate(boxID){
    try {
        if(parseFloat($("[id='" + boxID + "']").find(".txtBoxWeight").val()) > 0){
            var divID = $("[id='" + boxID + "']").closest("div[id^=divOrder]").attr("id");

            var oppID = parseInt(divID.replace("divOrder","").split("~")[0]);
            var shipVia = parseInt($("[id='" + divID + "']").find("[id^='ddlShipVia']").val());
            var shipService = parseInt($("[id='" + divID + "']").find("[id^='ddlShipService']").val());

            if(shipVia === 0){
                alert("Select Ship-via");
                $("[id='" + divID + "']").find("[id^='ddlShipVia']").focus();
                return false;
            } else if (shipService === 0){
                alert("Select Ship-service");
                $("[id='" + divID + "']").find("[id^='ddlShipService']").focus();
                return false;
            }

            var isPakageTypeSelected = true;

            $("[id='" + divID + "']").find(".package-type").each(function(index,ddlPackageType){
                if(parseInt($(ddlPackageType).val()) === 0){
                    isPakageTypeSelected = false;
                    $(ddlPackageType).focus();
                    return false;
                }
            });

            if(!isPakageTypeSelected){
                alert("Select Package Type");
                return false;
            }
        } else {
            alert("Weight must be greate than 0");
            return false;
        }

        var boxes = [];
        $("#divGridRightPack").find("[id='" + divID + "']").find(".ulBoxes").each(function(boxIndex,box){
            var objBox = {};
            objBox.Name = $(box).find(".box-title").text();
            objBox.Height = parseFloat($(box).find("#height").val());
            objBox.Width = parseFloat($(box).find("#width").val());
            objBox.Length = parseFloat($(box).find("#length").val());
            objBox.Weight = parseFloat($(box).find(".txtBoxWeight").val());
            if($(box).find(".hdnBoxWeightChanged").val() === "1"){
                objBox.IsUserProvidedBoxWeight = true;
            } else {
                objBox.IsUserProvidedBoxWeight = false;
            }
            objBox.PackageType = parseInt($(box).find(".package-type").val());
            boxes.push(objBox);
        });
        
        if(boxes.length > 0) {
            $.ajax({
                type: "GET",
                url: '../opportunity/MassSalesFulfillmentService.svc/GetShippingConfiguration',
                contentType: "application/json",
                success: function (data) {
                    try {
                        var shippingConfiguration = $.parseJSON(data.GetShippingConfigurationResult);
                                        
                        $.ajax({
                            type: "POST",
                            url: '../opportunity/MassSalesFulfillmentService.svc/GetNewShippingRate',
                            contentType: "application/json",
                            dataType: "json",
                            data: JSON.stringify({
                                "oppID": oppID
                                ,"shipVia": shipVia
                                ,"shipService": shipService
                                ,"shippingConfiguration": JSON.stringify(shippingConfiguration)
                                ,"boxes":JSON.stringify(boxes)
                            }),
                            success: function (data) {
                                try {
                                    var obj = $.parseJSON(data.GetNewShippingRateResult);
                                    $("[id='" + divID + "']").find("label.lblNewShipRate").html($("[id$=hdnMSCurrencyID]").val() + " " + obj.Rate);
                                    $("[id='" + divID + "']").find("div.divNewShipRate").show();
                                    $("[id='" + divID + "']").find("input.hdnBoxWeightChanged").val("1");

                                    $("[id='" + boxID + "']").find(".btnChangeWeight").removeClass("btn-success").addClass("btn-danger");
                                    $("[id='" + boxID + "']").find(".btnChangeWeight").val("Change");
                                    $("[id='" + boxID + "']").find(".btnChangeWeight").attr('onClick',"return ChangeBoxWeight('" + boxID + "');");
                                    $("[id='" + boxID + "']").find(".txtBoxWeight").prop('disabled', true);
                                } catch (err) {
                                    alert("Unknown error occurred.");
                                }
                            },
                            error: function (jqXHR, textStatus, errorThrown) {
                                if(IsJsonString(jqXHR.responseText)){
                                    var objError = $.parseJSON(jqXHR.responseText)
  
                                    if (objError.ErrorMessage === "SHIP_FROM_ADDRESS_NOT_AVAILABLE") {
                                        alert("Invalid ship from address.");
                                    } else if(objError.ErrorMessage === "SHIP_TO_FROM_ADDRESS_NOT_AVAILABLE"){
                                        alert("Invalid ship to address.");
                                    } else {
                                        alert(replaceNull(objError.ErrorMessage));
                                    }
                                } else {
                                    alert("Unknown error ocurred");
                                }   
                            }
                        });
                    } catch (err) {
                        alert("Unknown error occurred while fetching shipping configuration.");
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    if(IsJsonString(jqXHR.responseText)){
                        var objError = $.parseJSON(jqXHR.responseText)
                        alert("Error occurred: " + replaceNull(objError.ErrorMessage));
                    } else {
                        alert("Unknown error ocurred");
                    } 
                }
            });
        } else {
            alert("Add item(s) to boxes.");
        }
    } catch (e) {
        alert("Unknown error occurred while getting new shipping rate");
    
    }
    $("[id='" + boxID + "']").find(".btnChangeWeight").removeClass("btn-danger").addClass("btn-success");
    $("[id='" + boxID + "']").find(".btnChangeWeight").val("Update");
    $("[id='" + boxID + "']").find(".txtBoxWeight").prop('disabled', false);
    $("[id='" + boxID + "']").find(".btnChangeWeight").attr('onClick',"return GetNewShippingRate('" + boxID + "');");

    return false;
}

function UpdateNewShippingRate(divID){
    try {
        var selectdRecords = [];
        var obj = {};
        obj.OppID= parseInt(divID.replace("divOrder","").split("~")[0]);
        obj.WarehouseID= parseInt($("#ddlWarehouse").val());
        obj.ShipByDate= "";
        obj.PreferredShipVia= parseInt($("[id='" + divID + "']").find("[id^='ddlShipVia']").val());
        obj.PreferredShipService= parseInt($("[id='" + divID + "']").find("[id^='ddlShipService']").val());
        obj.ShipRate = parseFloat( $("[id='" + divID + "']").find("label.lblNewShipRate").html().replace($("[id$=hdnMSCurrencyID]").val(),"").trim());
        obj.Description = "";
        selectdRecords.push(obj);

        if(selectdRecords.length > 0){
            $.ajax({
                type: "POST",
                url: '../opportunity/MassSalesFulfillmentService.svc/UpdateShippingRate',
                contentType: "application/json",
                dataType: "json",
                data: JSON.stringify({
                    "orders": JSON.stringify(selectdRecords)
                }),
                success: function (data) {
                    try {
                        var obj = $.parseJSON(data.UpdateShippingRateResult);
                                          
                        if(obj.isSuccess){
                            $("[id='" + divID + "']").find("label.lblNewShipRate").html("");
                            $("[id='" + divID + "']").find("div.divNewShipRate").hide();
                        } else {
                            alert("Error occurred while updating ship rate.");
                        }
                    } catch (err) {
                        alert("Unknown error occurred.");
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    if(IsJsonString(jqXHR.responseText)){
                        var objError = $.parseJSON(jqXHR.responseText)

                        if (objError.ErrorMessage === "SHIPPING_ITEM_NOT_SET") {
                            alert("Go global settings -> Accounting and select Default Item used for Shipping within Orders.");
                        } else {
                            alert("Error occurred: " + replaceNull(objError.ErrorMessage));
                        }
                    } else {
                        alert("Unknown error ocurred");
                    }
                }
            });
        } 
    } catch (e) {
        alert("Unkown error occured while updating ship rate to order.");
    }
}

function OpenCloseAllChanged(){
    LoadRecordsWithPagination();
}

function GetSerialLotQty(txtPicked,IsSerial,IsLot){
    if(IsSerial){
        return ($(txtPicked).val().trim() == "" || $(txtPicked).val().trim() == "0") ? 0 : parseInt($(txtPicked).val().split(",").length);
    } else if(IsLot){
        if($(txtPicked).val().trim() == ""){
            return 0;
        } else {
            var qty = 0;
            var arrLot = $(txtPicked).val().trim().replace(/,/g, '').split(",");
            $.each( arrLot, function( index, value ) {
                var startIndex = value.indexOf("(");
                var endIndex = value.indexOf(")");

                if(startIndex != -1 && endIndex != -1 && endIndex > (startIndex + 1)){
                    qty += parseFloat(value.substring(startIndex + 1,endIndex));
                }
            });

            return qty;
        }
    }
}

function IsValidSerialLot(txtPicked){
    var isValidSerialLot = true;
    var arrLot = $(txtPicked).val().trim().replace(/,/g, '').split(",");
    $.each( arrLot, function(index,value) {
        var startIndex = value.indexOf("(");
        var endIndex = value.indexOf(")");

        if(!(startIndex != -1 && endIndex != -1 && endIndex > (startIndex + 1))){
            isValidSerialLot = false
        }
    });

    return isValidSerialLot;
}

function escapeHtml (string) {
    return String(string).replace(/[&<>"'`=\/]/g, function (s) {
        return entityMap[s];
    });
}