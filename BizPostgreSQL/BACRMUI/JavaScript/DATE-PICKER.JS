var weekend = [0,6];
var weekendColor = "#8ca2bd";
var fontface = "Arial";
var fontsize = 2;

var gNow = new Date();
var ggWinCal;
isNav = (navigator.appName.indexOf("Netscape") != -1) ? true : false;
isIE = (navigator.appName.indexOf("Microsoft") != -1) ? true : false;

Calendar.Months = ["January", "February", "March", "April", "May", "June",
"July", "August", "September", "October", "November", "December"];

// Non-Leap year Month days..
Calendar.DOMonth = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
// Leap year Month days..
Calendar.lDOMonth = [31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];

function Calendar(p_item, p_WinCal, p_month, p_year, p_format) {
	if ((p_month == null) && (p_year == null))	return;

	if (p_WinCal == null)
		this.gWinCal = ggWinCal;
	else
		this.gWinCal = p_WinCal;
	
	if (p_month == null) {
		this.gMonthName = null;
		this.gMonth = null;
		this.gYearly = true;
	} else {
		this.gMonthName = Calendar.get_month(p_month);
		this.gMonth = new Number(p_month);
		this.gYearly = false;
	}

	this.gYear = p_year;
	this.gFormat = p_format;
	this.gBGColor = "white";
	this.gFGColor = "black";
	this.gTextColor = "black";
	this.gHeaderColor = "black";
	this.gReturnItem = p_item;
}

Calendar.get_month = Calendar_get_month;
Calendar.get_daysofmonth = Calendar_get_daysofmonth;
Calendar.calc_month_year = Calendar_calc_month_year;
Calendar.print = Calendar_print;

function Calendar_get_month(monthNo) {
	return Calendar.Months[monthNo];
}

function Calendar_get_daysofmonth(monthNo, p_year) {
	/* 
	Check for leap year ..
	1.Years evenly divisible by four are normally leap years, except for... 
	2.Years also evenly divisible by 100 are not leap years, except for... 
	3.Years also evenly divisible by 400 are leap years. 
	*/
	if ((p_year % 4) == 0) {
		if ((p_year % 100) == 0 && (p_year % 400) != 0)
			return Calendar.DOMonth[monthNo];
	
		return Calendar.lDOMonth[monthNo];
	} else
		return Calendar.DOMonth[monthNo];
}

function Calendar_calc_month_year(p_Month, p_Year, incr) {
	/* 
	Will return an 1-D array with 1st element being the calculated month 
	and second being the calculated year 
	after applying the month increment/decrement as specified by 'incr' parameter.
	'incr' will normally have 1/-1 to navigate thru the months.
	*/
	var ret_arr = new Array();
	
	if (incr == -1) {
		// B A C K W A R D
		if (p_Month == 0) {
			ret_arr[0] = 11;
			ret_arr[1] = parseInt(p_Year) - 1;
		}
		else {
			ret_arr[0] = parseInt(p_Month) - 1;
			ret_arr[1] = parseInt(p_Year);
		}
	} else if (incr == 1) {
		// F O R W A R D
		if (p_Month == 11) {
			ret_arr[0] = 0;
			ret_arr[1] = parseInt(p_Year) + 1;
		}
		else {
			ret_arr[0] = parseInt(p_Month) + 1;
			ret_arr[1] = parseInt(p_Year);
		}
	}
	
	return ret_arr;
}

function Calendar_print() {
	ggWinCal.print();
}

function Calendar_calc_month_year(p_Month, p_Year, incr) {
	/* 
	Will return an 1-D array with 1st element being the calculated month 
	and second being the calculated year 
	after applying the month increment/decrement as specified by 'incr' parameter.
	'incr' will normally have 1/-1 to navigate thru the months.
	*/
	var ret_arr = new Array();
	
	if (incr == -1) {
		// B A C K W A R D
		if (p_Month == 0) {
			ret_arr[0] = 11;
			ret_arr[1] = parseInt(p_Year) - 1;
		}
		else {
			ret_arr[0] = parseInt(p_Month) - 1;
			ret_arr[1] = parseInt(p_Year);
		}
	} else if (incr == 1) {
		// F O R W A R D
		if (p_Month == 11) {
			ret_arr[0] = 0;
			ret_arr[1] = parseInt(p_Year) + 1;
		}
		else {
			ret_arr[0] = parseInt(p_Month) + 1;
			ret_arr[1] = parseInt(p_Year);
		}
	}
	
	return ret_arr;
}

// This is for compatibility with Navigator 3, we have to create and discard one object before the prototype object exists.
new Calendar();

Calendar.prototype.getMonthlyCalendarCode = function() {
	var vCode = "";
	var vHeader_Code = "";
	var vData_Code = "";
	
	// Begin Table Drawing code here..
	vCode = vCode + "<TABLE BORDER=1 cellspacing=0 cellpading=1 bordercolor='#52658C' BGCOLOR=\"" + this.gBGColor + "\">";
	
	vHeader_Code = this.cal_header();
	vData_Code = this.cal_data();
	vCode = vCode + vHeader_Code + vData_Code;
	
	vCode = vCode + "</TABLE>";
	
	return vCode;
}

Calendar.prototype.show = function() {
	var vCode = "";
	
	this.gWinCal.document.open();

	// Setup the page...
	this.wwrite("<html>");
	this.wwrite("<head><title>Calendar</title>");
	this.wwrite("<script language =javascript>");
	//this.wwrite("window.captureEvents(Event.BLUR);");
	this.wwrite("window.onBlur = myClickHandler;");
	this.wwrite("function myClickHandler()");
	this.wwrite("{");
	//this.wwrite("alert(window.name);");
	this.wwrite(" window.focus();");
	this.wwrite("}");
	this.wwrite("</script>");
	this.wwrite("</head>");

	this.wwrite("<body onLoad= window.focus(); onblur=myClickHandler(); " + 
		"link=\"" + this.gLinkColor + "\" " + 
		"vlink=\"" + this.gLinkColor + "\" " +
		"alink=\"" + this.gLinkColor + "\" " +
		"text=\"" + this.gTextColor + "\">");
	this.wwriteA("<FONT FACE='" + fontface + "' SIZE=2><B>");
	this.wwriteA(this.gMonthName + " " + this.gYear);
	this.wwriteA("</B><BR>");

	// Show navigation buttons
	var prevMMYYYY = Calendar.calc_month_year(this.gMonth, this.gYear, -1);
	var prevMM = prevMMYYYY[0];
	var prevYYYY = prevMMYYYY[1];

	var nextMMYYYY = Calendar.calc_month_year(this.gMonth, this.gYear, 1);
	var nextMM = nextMMYYYY[0];
	var nextYYYY = nextMMYYYY[1];
	
	this.wwrite("<TABLE WIDTH='100%' BORDER=1 CELLSPACING=0 CELLPADDING=0 bordercolor='52658c' BGCOLOR='#c6d3e7'><TR><TD ALIGN=center>");
	this.wwrite("<b><font size=2>[<A HREF=\"" +
		"javascript:window.opener.Build(" + 
		"'" + this.gReturnItem + "', '" + this.gMonth + "', '" + (parseInt(this.gYear)-1) + "', '" + this.gFormat + "'" +
		");" +
		"\"><<<\/A>]</font></b></TD><TD ALIGN=center>");
	this.wwrite("<b><font size=2>[<A HREF=\"" +
		"javascript:window.opener.Build(" + 
		"'" + this.gReturnItem + "', '" + prevMM + "', '" + prevYYYY + "', '" + this.gFormat + "'" +
		");" +
		"\"><<\/A>]</font></B></TD><TD ALIGN=center>");
	this.wwrite("<b><font size=2>[<A HREF=\"javascript:window.print();\">Print</A>]</font></B></TD><TD ALIGN=center>");
	this.wwrite("<b><font size=2>[<A HREF=\"" +
		"javascript:window.opener.Build(" + 
		"'" + this.gReturnItem + "', '" + nextMM + "', '" + nextYYYY + "', '" + this.gFormat + "'" +
		");" +
		"\">><\/A>]</font></b></TD><TD ALIGN=center>");
	this.wwrite("<b><font size=2>[<A HREF=\"" +
		"javascript:window.opener.Build(" + 
		"'" + this.gReturnItem + "', '" + this.gMonth + "', '" + (parseInt(this.gYear)+1) + "', '" + this.gFormat + "'" +
		");" +
		"\">>><\/A>]</font></b></TD></TR></TABLE><BR>");

	// Get the complete calendar code for the month..
	vCode = this.getMonthlyCalendarCode();
	this.wwrite(vCode);

	this.wwrite("</font></body></html>");
	this.gWinCal.document.close();
}

Calendar.prototype.showY = function() {
	var vCode = "";
	var i;
	var vr, vc, vx, vy;		// Row, Column, X-coord, Y-coord
	var vxf = 285;			// X-Factor
	var vyf = 200;			// Y-Factor
	var vxm = 10;			// X-margin
	var vym;				// Y-margin
	if (isIE)	vym = 75;
	else if (isNav)	vym = 25;
	
	this.gWinCal.document.open();

	this.wwrite("<html>");
	this.wwrite("<head><title>Calendar</title>");
	this.wwrite("<style type='text/css'>\n<!--");
	for (i=0; i<12; i++) {
		vc = i % 3;
		if (i>=0 && i<= 2)	vr = 0;
		if (i>=3 && i<= 5)	vr = 1;
		if (i>=6 && i<= 8)	vr = 2;
		if (i>=9 && i<= 11)	vr = 3;
		
		vx = parseInt(vxf * vc) + vxm;
		vy = parseInt(vyf * vr) + vym;

		this.wwrite(".lclass" + i + " {position:absolute;top:" + vy + ";left:" + vx + ";}");
	}
	this.wwrite("-->\n</style>");
	this.wwrite("</head>");

	this.wwrite("<body " + 
		"link=\"" + this.gLinkColor + "\" " + 
		"vlink=\"" + this.gLinkColor + "\" " +
		"alink=\"" + this.gLinkColor + "\" " +
		"text=\"" + this.gTextColor + "\">");
	this.wwrite("<FONT FACE='" + fontface + "' SIZE=2><B>");
	this.wwrite("Year : " + this.gYear);
	this.wwrite("</B><BR>");

	// Show navigation buttons
	var prevYYYY = parseInt(this.gYear) - 1;
	var nextYYYY = parseInt(this.gYear) + 1;
	
	this.wwrite("<TABLE WIDTH='100%' BORDER=1 CELLSPACING=0 CELLPADDING=0 BGCOLOR='#CCCCCC'><TR><TD ALIGN=center>");
	this.wwrite("[<A HREF=\"" +
		"javascript:window.opener.Build(" + 
		"'" + this.gReturnItem + "', null, '" + prevYYYY + "', '" + this.gFormat + "'" +
		");" +
		"\" alt='Prev Year'><<<\/A>]</TD><TD ALIGN=center>");
	this.wwrite("[<A HREF=\"javascript:window.print();\">Print</A>]</TD><TD ALIGN=center>");
	this.wwrite("[<A HREF=\"" +
		"javascript:window.opener.Build(" + 
		"'" + this.gReturnItem + "', null, '" + nextYYYY + "', '" + this.gFormat + "'" +
		");" +
		"\">>><\/A>]</TD></TR></TABLE><BR>");

	// Get the complete calendar code for each month..
	var j;
	for (i=11; i>=0; i--) {
		if (isIE)
			this.wwrite("<DIV ID=\"layer" + i + "\" CLASS=\"lclass" + i + "\">");
		else if (isNav)
			this.wwrite("<LAYER ID=\"layer" + i + "\" CLASS=\"lclass" + i + "\">");

		this.gMonth = i;
		this.gMonthName = Calendar.get_month(this.gMonth);
		vCode = this.getMonthlyCalendarCode();
		this.wwrite(this.gMonthName + "/" + this.gYear + "<BR>");
		this.wwrite(vCode);

		if (isIE)
			this.wwrite("</DIV>");
		else if (isNav)
			this.wwrite("</LAYER>");
	}

	this.wwrite("</font><BR></body></html>");
	this.gWinCal.document.close();
}

Calendar.prototype.wwrite = function(wtext) {
	this.gWinCal.document.writeln(wtext);
}

Calendar.prototype.wwriteA = function(wtext) {
	this.gWinCal.document.write(wtext);
}

Calendar.prototype.cal_header = function() {
	var vCode = "";
	
	vCode = vCode + "<TR bgcolor='#c6d3e7'>";
	vCode = vCode + "<TD WIDTH='14%'><FONT SIZE='2' FACE='" + fontface + "' COLOR='" + this.gHeaderColor + "'><B>Sun</B></FONT></TD>";
	vCode = vCode + "<TD WIDTH='14%'><FONT SIZE='2' FACE='" + fontface + "' COLOR='" + this.gHeaderColor + "'><B>Mon</B></FONT></TD>";
	vCode = vCode + "<TD WIDTH='14%'><FONT SIZE='2' FACE='" + fontface + "' COLOR='" + this.gHeaderColor + "'><B>Tue</B></FONT></TD>";
	vCode = vCode + "<TD WIDTH='14%'><FONT SIZE='2' FACE='" + fontface + "' COLOR='" + this.gHeaderColor + "'><B>Wed</B></FONT></TD>";
	vCode = vCode + "<TD WIDTH='14%'><FONT SIZE='2' FACE='" + fontface + "' COLOR='" + this.gHeaderColor + "'><B>Thu</B></FONT></TD>";
	vCode = vCode + "<TD WIDTH='14%'><FONT SIZE='2' FACE='" + fontface + "' COLOR='" + this.gHeaderColor + "'><B>Fri</B></FONT></TD>";
	vCode = vCode + "<TD WIDTH='16%'><FONT SIZE='2' FACE='" + fontface + "' COLOR='" + this.gHeaderColor + "'><B>Sat</B></FONT></TD>";
	vCode = vCode + "</TR>";
	
	return vCode;
}

Calendar.prototype.cal_data = function() {
	var vDate = new Date();
	vDate.setDate(1);
	vDate.setMonth(this.gMonth);
	vDate.setFullYear(this.gYear);

	var vFirstDay=vDate.getDay();
	var vDay=1;
	var vLastDay=Calendar.get_daysofmonth(this.gMonth, this.gYear);
	var vOnLastDay=0;
	var vCode = "";

	/*
	Get day for the 1st of the requested month/year..
	Place as many blank cells before the 1st day of the month as necessary. 
	*/

	vCode = vCode + "<TR>";
	for (i=0; i<vFirstDay; i++) {
		vCode = vCode + "<TD WIDTH='14%'" + this.write_weekend_string(i) + "><FONT SIZE='2' FACE='" + fontface + "'> </FONT></TD>";
	}

	// Write rest of the 1st week
	for (j=vFirstDay; j<7; j++) {
		vCode = vCode + "<TD WIDTH='14%'" + this.write_weekend_string(j) + "><FONT SIZE='2' FACE='" + fontface + "'>" + 
			"<b><A HREF='#' " + 
				"onClick=\"self.opener.document." + this.gReturnItem + ".value='" + 
				this.format_data(vDay) + 
				"';window.close();\">" + 
				this.format_day(vDay) + 
			"</A>" + 
			"</b></FONT></TD>";
		vDay=vDay + 1;
	}
	vCode = vCode + "</TR>";

	// Write the rest of the weeks
	for (k=2; k<7; k++) {
		vCode = vCode + "<TR>";

		for (j=0; j<7; j++) {
			vCode = vCode + "<TD WIDTH='14%'" + this.write_weekend_string(j) + "><FONT SIZE='2' FACE='" + fontface + "'>" + 
				"<b><A HREF='#' " + 
					"onClick=\"self.opener.document." + this.gReturnItem + ".value='" + 
					this.format_data(vDay) + 
					"';window.close();\">" + 
				this.format_day(vDay) + 
				"</A></b>" + 
				"</FONT></TD>";
			vDay=vDay + 1;

			if (vDay > vLastDay) {
				vOnLastDay = 1;
				break;
			}
		}

		if (j == 6)
			vCode = vCode + "</TR>";
		if (vOnLastDay == 1)
			break;
	}
	
	// Fill up the rest of last week with proper blanks, so that we get proper square blocks
	for (m=1; m<(7-j); m++) {
		if (this.gYearly)
			vCode = vCode + "<TD WIDTH='14%'" + this.write_weekend_string(j+m) + 
			"><FONT SIZE='2' FACE='" + fontface + "' COLOR='gray'> </FONT></TD>";
		else
			vCode = vCode + "<TD WIDTH='14%'" + this.write_weekend_string(j+m) + 
			"><FONT SIZE='2' FACE='" + fontface + "' COLOR='gray'>" + m + "</FONT></TD>";
	}
	
	return vCode;
}

Calendar.prototype.format_day = function(vday) {
	var vNowDay = gNow.getDate();
	var vNowMonth = gNow.getMonth();
	var vNowYear = gNow.getFullYear();

	if (vday == vNowDay && this.gMonth == vNowMonth && this.gYear == vNowYear)
		return ("<FONT COLOR=\"RED\"><B>" + vday + "</B></FONT>");
	else
		return (vday);
}

Calendar.prototype.write_weekend_string = function(vday) {
	var i;

	// Return special formatting for the weekend day.
	for (i=0; i<weekend.length; i++) {
		if (vday == weekend[i])
			return (" BGCOLOR=\"" + weekendColor + "\"");
	}
	
	return "";
}

Calendar.prototype.format_data = function(p_day) {
	var vData;
	var vMonth = 1 + this.gMonth;
	vMonth = (vMonth.toString().length < 2) ? "0" + vMonth : vMonth;
	//var vMon = Calendar.get_month(this.gMonth).substr(0,3).toUpperCase();
	var vMon = Calendar.get_month(this.gMonth).substr(0,3);
	//var vFMon = Calendar.get_month(this.gMonth).toUpperCase();
	var vFMon = Calendar.get_month(this.gMonth);
	var vY4 = new String(this.gYear);
	var vY2 = new String(this.gYear.substr(2,2));
	var vDD = (p_day.toString().length < 2) ? "0" + p_day : p_day;

	switch (this.gFormat) {
		case "MM\/DD\/YYYY" :
			vData = vMonth + "\/" + vDD + "\/" + vY4;
			break;
		case "MM\/DD\/YY" :
			vData = vMonth + "\/" + vDD + "\/" + vY2;
			break;
		case "MM-DD-YYYY" :
			vData = vMonth + "-" + vDD + "-" + vY4;
			break;
		case "MM-DD-YY" :
			vData = vMonth + "-" + vDD + "-" + vY2;
			break;

		case "DD\/MON\/YYYY" :
			vData = vDD + "\/" + vMon + "\/" + vY4;
			break;
		case "DD\/MON\/YY" :
			vData = vDD + "\/" + vMon + "\/" + vY2;
			break;
		case "DD-MON-YYYY" :
			vData = vDD + "-" + vMon + "-" + vY4;
			break;
		case "DD-MON-YY" :
			vData = vDD + "-" + vMon + "-" + vY2;
			break;

		case "DD\/MONTH\/YYYY" :
			vData = vDD + "\/" + vFMon + "\/" + vY4;
			break;
		case "DD\/MONTH\/YY" :
			vData = vDD + "\/" + vFMon + "\/" + vY2;
			break;
		case "DD-MONTH-YYYY" :
			vData = vDD + "-" + vFMon + "-" + vY4;
			break;
		case "DD-MONTH-YY" :
			vData = vDD + "-" + vFMon + "-" + vY2;
			break;

		case "DD\/MM\/YYYY" :
			vData = vDD + "\/" + vMonth + "\/" + vY4;
			break;
		case "DD\/MM\/YY" :
			vData = vDD + "\/" + vMonth + "\/" + vY2;
			break;
		case "DD-MM-YYYY" :
			vData = vDD + "-" + vMonth + "-" + vY4;
			break;
		case "DD-MM-YY" :
			vData = vDD + "-" + vMonth + "-" + vY2;
			break;

		default :
			vData = vMonth + "\/" + vDD + "\/" + vY4;
	}

	return vData;
}

function Build(p_item, p_month, p_year, p_format) {
	var p_WinCal = ggWinCal;
	gCal = new Calendar(p_item, p_WinCal, p_month, p_year, p_format);

	// Customize your Calendar here..
	gCal.gBGColor="white";
	gCal.gLinkColor="black";
	gCal.gTextColor="black";
	gCal.gHeaderColor="black";

	// Choose appropriate show function
	if (gCal.gYearly)	gCal.showY();
	else	gCal.show();
}




function show_calendar() {
	/* 
		p_month : 0-11 for Jan-Dec; 12 for All Months.
		p_year	: 4-digit year
		p_format: Date format (mm/dd/yyyy, dd/mm/yy, ...)
		p_item	: Return Item.
	*/

	p_item = arguments[0];
	if (arguments[1] == null)
		p_month = new String(gNow.getMonth());
	else
		p_month = arguments[1];
	if (arguments[2] == "" || arguments[2] == null)
		p_year = new String(gNow.getFullYear().toString());
	else
		p_year = arguments[2];
	if (arguments[3] == null)
		p_format = "MM/DD/YYYY";
	else
		p_format = arguments[3];

	vWinCal = window.open("", "Calendar", 
		"width=250,height=225,status=no,resizable=no,top=200,left=200");
	vWinCal.opener = self;
	ggWinCal = vWinCal;

	Build(p_item, p_month, p_year, p_format);
}
/*
Yearly Calendar Code Starts here
*/
function show_yearly_calendar(p_item, p_year, p_format) {
	// Load the defaults..
	if (p_year == null || p_year == "")
		p_year = new String(gNow.getFullYear().toString());
	if (p_format == null || p_format == "")
		p_format = "MM/DD/YYYY";

	var vWinCal = window.open("", "Calendar", "scrollbars=yes");
	vWinCal.opener = self;
	ggWinCal = vWinCal;

	Build(p_item, null, p_year, p_format);
}

function fn_GetGMTDateTime(varMonth, varDate, varYear, varHour, varMinute, varSeconds)
{

	var myDt = new Date();
	myDt.setFullYear(varYear, varMonth, varDate);	//Jan=0 , Dec=11
	myDt.setHours(varHour,varMinute,varSeconds,0);
	//document.forms(0).txtUserDt.value = myDt.toDateString() + ' ' + myDt.toTimeString();
	var strMonthName;
	if(myDt.getUTCMonth() == 0)
		strMonthName = '01';
	else if(myDt.getUTCMonth() == 1)
		strMonthName = '02';
	else if(myDt.getUTCMonth() == 2)
		strMonthName = '03';
	else if(myDt.getUTCMonth() == 3)
		strMonthName = '04';
	else if(myDt.getUTCMonth() == 4)
		strMonthName = '05';
	else if(myDt.getUTCMonth() == 5)
		strMonthName = '06';
	else if(myDt.getUTCMonth() == 6)
		strMonthName = '07';
	else if(myDt.getUTCMonth() == 7)
		strMonthName = '08';
	else if(myDt.getUTCMonth() == 8)
		strMonthName = '09';
	else if(myDt.getUTCMonth() == 9)
		strMonthName = '10';
	else if(myDt.getUTCMonth() == 10)
		strMonthName = '11';
	else if(myDt.getUTCMonth() == 11)
		strMonthName = '12';
	//return myDt.getUTCDate() + '/' + strMonthName + '/' + myDt.getUTCFullYear() + ' ' + myDt.getUTCHours() + ':' + myDt.getUTCMinutes() + ':' + myDt.getUTCSeconds();
	return myDt.getUTCDate() + '/' + strMonthName + '/' + myDt.getUTCFullYear() + ' ' + myDt.getUTCHours() + ':' + myDt.getUTCMinutes() + ':' + '00';
	//Modified the Seconds to return as 00
	/*if (varMinute == '0' || varMinute == '00')
	{
			return myDt.getUTCDate() + '/' + strMonthName + '/' + myDt.getUTCFullYear() + ' ' + myDt.getUTCHours() + ':' + '00' + ':' + '00';
		}
	else
		{
			return myDt.getUTCDate() + '/' + strMonthName + '/' + myDt.getUTCFullYear() + ' ' + myDt.getUTCHours() + ':' + myDt.getUTCMinutes() + ':' + '00';
		}*/
	
}

function fn_GetDateAsNumber(varDate, varFormat)
{
	var strNewFormat = new String();
	
	var strRetString;
	strNewFormat = varDate;
	
	if(varFormat=='MM/DD/YYYY')
		strRetString = strNewFormat.substring(6,10) + strNewFormat.substring(0,2) + strNewFormat.substring(3,5);
	else if(varFormat=='MM/DD/YY')
		strRetString = '20' + strNewFormat.substring(6,8) + strNewFormat.substring(0,2) + strNewFormat.substring(3,5);
	else if(varFormat=='MM-DD-YYYY')
		strRetString = strNewFormat.substring(6,10) + strNewFormat.substring(0,2) + strNewFormat.substring(3,5);
	else if(varFormat=='MM-DD-YY')
		strRetString = '20' + strNewFormat.substring(6,8) + strNewFormat.substring(0,2) + strNewFormat.substring(3,5);
	else if(varFormat=='DD/MON/YYYY')
		strRetString = strNewFormat.substring(7,11) + fn_GetMonthID(strNewFormat.substring(3,6)) + strNewFormat.substring(0,2);
	else if(varFormat=='DD/MON/YY')
		strRetString = '20' + strNewFormat.substring(7,9) + fn_GetMonthID(strNewFormat.substring(3,6)) + strNewFormat.substring(0,2);
	else if(varFormat=='DD-MON-YYYY')
		strRetString = strNewFormat.substring(7,11) + fn_GetMonthID(strNewFormat.substring(3,6)) + strNewFormat.substring(0,2);
	else if(varFormat=='DD-MON-YY')
		strRetString = '20' + strNewFormat.substring(7,9) + fn_GetMonthID(strNewFormat.substring(3,6)) + strNewFormat.substring(0,2);
	else if(varFormat=='DD/MONTH/YYYY')
		strRetString = strNewFormat.substring(strNewFormat.indexOf("/",4)+1,strNewFormat.indexOf("/",4)+ 5) + fn_GetMonthID(strNewFormat.substring(3,strNewFormat.indexOf("/",4))) + strNewFormat.substring(0,2) ;
	else if(varFormat=='DD/MONTH/YY')
		strRetString = '20' + strNewFormat.substring(strNewFormat.indexOf("/",4)+1,strNewFormat.indexOf("/",4)+ 3) + fn_GetMonthID(strNewFormat.substring(3,strNewFormat.indexOf("/",4)))+ strNewFormat.substring(0,2); 
	else if(varFormat=='DD-MONTH-YYYY')
		strRetString = strNewFormat.substring(strNewFormat.indexOf("-",4)+1,strNewFormat.indexOf("-",4)+ 5) + fn_GetMonthID(strNewFormat.substring(3,strNewFormat.indexOf("-",4))) + strNewFormat.substring(0,2) ;
	else if(varFormat=='DD-MONTH-YY')
		strRetString = '20' + strNewFormat.substring(strNewFormat.indexOf("-",4)+1,strNewFormat.indexOf("-",4)+ 3) + fn_GetMonthID(strNewFormat.substring(3,strNewFormat.indexOf("-",4)))+ strNewFormat.substring(0,2); 
	else if(varFormat=='DD/MM/YYYY')
		strRetString = strNewFormat.substring(6,10) + strNewFormat.substring(3,5) + strNewFormat.substring(0,2)
	else if(varFormat=='DD/MM/YY')
		strRetString = '20' + strNewFormat.substring(6,8) + strNewFormat.substring(3,5) + strNewFormat.substring(0,2)
	else if(varFormat=='DD-MM-YYYY')
		strRetString = strNewFormat.substring(6,10) + strNewFormat.substring(3,5) + strNewFormat.substring(0,2)
	else if(varFormat=='DD-MM-YY')
		strRetString = '20' + strNewFormat.substring(6,8) + strNewFormat.substring(3,5) + strNewFormat.substring(0,2)
	
	return strRetString;
}

function fn_GetMonthID(varMonthName)

{
	var strRetVal

	if (varMonthName=='Jan' || varMonthName=='January')
		strRetVal = '01';
	else if (varMonthName=='Feb' || varMonthName=='February')
		strRetVal = '02';
	else if (varMonthName=='Mar' || varMonthName=='March')
		strRetVal = '03';
	else if (varMonthName=='Apr' || varMonthName=='April')
		strRetVal = '04';
	else if (varMonthName=='May' || varMonthName=='May')
		strRetVal = '05';
	else if (varMonthName=='Jun' || varMonthName=='June')
		strRetVal = '06';
	else if (varMonthName=='Jul' || varMonthName=='July')
		strRetVal = '07';
	else if (varMonthName=='Aug' || varMonthName=='August')
		strRetVal = '08';
	else if (varMonthName=='Sep' || varMonthName=='September')
		strRetVal = '09';
	else if (varMonthName=='Oct' || varMonthName=='October')
		strRetVal = '10';
	else if (varMonthName=='Nov' || varMonthName=='November')
		strRetVal = '11';
	else if (varMonthName=='Dec' || varMonthName=='December')
		strRetVal = '12';
	return strRetVal;
}

function fn_GetTime(varstrTime)
{
	var strActualTime=new String();
	var strRetTime;
	strActualTime=varstrTime;
	
	strRetTime=	 strActualTime.split(":");
	return strRetTime;
}

function fn_GetAMPM (varAMPM)  //To return AM PM
{
	var strAMPM;
	if (varAMPM =='AM')
		strAMPM='0';   //Means it is AM
	else if  (varAMPM =='PM')
		strAMPM='1';	  //Means it is PM
		
	return strAMPM;
}



// This is for Date of Birth







var date_Item ;
var varDateFormat;

function show_DateControl() 
{
/*	alert('test');*/
	p_item = arguments[0];
	vWinCal = window.open("", "Calendar", 
		"width=300,height=20,status=no,resizable=no,top=200,left=200");
	vWinCal.opener = self;
	ggWinCal = vWinCal;
	date_Item = arguments[0];
	
	if (arguments[1] == null)
		p_month = new String(gNow.getMonth());
	else
		p_month = arguments[1];
	if (arguments[2] == "" || arguments[2] == null)
		p_year = new String(gNow.getFullYear().toString());
	else
		p_year = arguments[2];
	if (arguments[3] == null)
		varDateFormat = "MM/DD/YYYY";
	else
		varDateFormat = arguments[3];

	
	build_DateControl();
    
}


function build_DateControl(){
var varDate= new Date();

this.ggWinCal.document.open();
this.ggWinCal.document.write("<html><TITLE>Calender</TITLE>")
this.ggWinCal.document.write("<head><script language=javascript> var bFlag='N'; var varDFormat='"+ this.varDateFormat +"';");

this.ggWinCal.document.write("function fn_LoadYears(){");

this.ggWinCal.document.write("if (yy.length<2)  { ");
this.ggWinCal.document.write("for(var i=1900;i<=2000;i++)  {");
this.ggWinCal.document.write("len = yy.length++; ");
this.ggWinCal.document.write("yy.options[len].value = i;");
this.ggWinCal.document.write("yy.options[len].text = i;");

this.ggWinCal.document.write("yy.selectedIndex = len;");
this.ggWinCal.document.write("} }");

this.ggWinCal.document.write("yy.selectedIndex = 0;");
this.ggWinCal.document.write("mm.selectedIndex=0;");

this.ggWinCal.document.write("dd.selectedIndex=0;");
this.ggWinCal.document.write("}");
this.ggWinCal.document.write("function fn_Mod(a, b)  {");
this.ggWinCal.document.write("return a-Math.floor(a/b)*b;");

this.ggWinCal.document.write("}");
this.ggWinCal.document.write("function fn_LoadDay()  {");
this.ggWinCal.document.write("dd.selectedIndex = len;");
this.ggWinCal.document.write("DOMonth = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];");
this.ggWinCal.document.write("lDOMonth = [31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];");
this.ggWinCal.document.write("var monval=mm.selectedIndex-1;");

this.ggWinCal.document.write("var len;");
this.ggWinCal.document.write("if(fn_Mod(yy.value,4)==0)  {");
this.ggWinCal.document.write("for(var j=1;j<=lDOMonth[monval];j++) {");
this.ggWinCal.document.write("len = dd.length++;");
this.ggWinCal.document.write("dd.options[len].value=j;");
this.ggWinCal.document.write("dd.options[len].text=j;");
this.ggWinCal.document.write("}");
this.ggWinCal.document.write("}");


this.ggWinCal.document.write("else  {");
this.ggWinCal.document.write("for(var j=1;j<=DOMonth[monval];j++)  {");
this.ggWinCal.document.write("len = dd.length++;");
this.ggWinCal.document.write("dd.options[len].value=j;");
this.ggWinCal.document.write("dd.options[len].text=j;");
this.ggWinCal.document.write("}");
this.ggWinCal.document.write("}");
this.ggWinCal.document.write("dd.selectedIndex=0;");
this.ggWinCal.document.write("}");
this.ggWinCal.document.write("function makeArray()  {");
this.ggWinCal.document.write("this[0] = makeArray.arguments.length;");
this.ggWinCal.document.write("for (i = 0; i<makeArray.arguments.length; i++)");
this.ggWinCal.document.write("this[i+1] = makeArray.arguments[i];");
this.ggWinCal.document.write("}");
this.ggWinCal.document.write("function fn_LoadMonth()  {");
this.ggWinCal.document.write("var months = new makeArray('January','February','March','April',");
this.ggWinCal.document.write("'May','June','July','August','September','October',");
this.ggWinCal.document.write("'November','December');");
this.ggWinCal.document.write("for(var i=1;i<13;i++) 	{");
this.ggWinCal.document.write("var len = mm.length++;");
this.ggWinCal.document.write("mm.options[len].value=i;");
this.ggWinCal.document.write("mm.options[len].text=months[i];");
this.ggWinCal.document.write("}");
this.ggWinCal.document.write("mm.selectedIndex=0;");
this.ggWinCal.document.write("dd.selectedIndex=0;");
this.ggWinCal.document.write("}");

this.ggWinCal.document.write("function date_Format() {");
this.ggWinCal.document.write("var vDay=dd.options[dd.selectedIndex].value;");
this.ggWinCal.document.write("vDD = (vDay.toString().length < 2) ? '0' + vDay : vDay;");
this.ggWinCal.document.write("var vData;");
this.ggWinCal.document.write("var vMonth = mm.options[mm.selectedIndex].value;");
this.ggWinCal.document.write("vMonth = (vMonth.toString().length < 2) ? '0' + vMonth : vMonth;");
this.ggWinCal.document.write("var vMon = mm.options[mm.selectedIndex].text.substr(0,3);");
this.ggWinCal.document.write("var vFMon = mm.options[mm.selectedIndex].text;");
this.ggWinCal.document.write("var vY4 = new String(yy.options[yy.selectedIndex].text);");
this.ggWinCal.document.write("var vY2 = new String(yy.options[yy.selectedIndex].text.substr(2,2));");
this.ggWinCal.document.write("switch (varDFormat)");
this.ggWinCal.document.write("{");
this.ggWinCal.document.write("case 'MM\/DD\/YYYY' : vData = vMonth + '\/' + vDD + '\/' + vY4;	break;");
this.ggWinCal.document.write("case 'MM\/DD\/YY' : vData = vMonth + '\/' + vDD + '\/' + vY2; break;");
this.ggWinCal.document.write("case 'MM-DD-YYYY' : vData = vMonth + '-' + vDD + '-' + vY4; break;");
this.ggWinCal.document.write("case 'MM-DD-YY' : vData = vMonth + '-' + vDD + '-' + vY2; break;");
this.ggWinCal.document.write("case 'DD\/MON\/YYYY' : vData = vDD + '\/' + vMon + '\/' + vY4; break;");
this.ggWinCal.document.write("case 'DD\/MON\/YY' : vData = vDD + '\/' + vMon + '\/' + vY2; break;");
this.ggWinCal.document.write("case 'DD-MON-YYYY' : vData = vDD + '-' + vMon + '-' + vY4; break;");
this.ggWinCal.document.write("case 'DD-MON-YY' : vData = vDD + '-' + vMon + '-' + vY2; break;");
this.ggWinCal.document.write("case 'DD\/MONTH\/YYYY' : vData = vDD + '\/' + vFMon + '\/' + vY4; break;");
this.ggWinCal.document.write("case 'DD\/MONTH\/YY' : vData = vDD + '\/' + vFMon + '\/' + vY2;	break;");
this.ggWinCal.document.write("case 'DD-MONTH-YYYY' : vData = vDD + '-' + vFMon + '-' + vY4; break;");
this.ggWinCal.document.write("case 'DD-MONTH-YY' : vData = vDD + '-' + vFMon + '-' + vY2; break;");
this.ggWinCal.document.write("case 'DD\/MM\/YYYY' : vData = vDD + '\/' + vMonth + '\/' + vY4; break;");
this.ggWinCal.document.write("case 'DD\/MM\/YY' : vData = vDD + '\/' + vMonth + '\/' + vY2; break;");
this.ggWinCal.document.write("case 'DD-MM-YYYY' : vData = vDD + '-' + vMonth + '-' + vY4; break;");
this.ggWinCal.document.write("case 'DD-MM-YY' : vData = vDD + '-' + vMonth + '-' + vY2; break;");
/*this.ggWinCal.document.write("if mm.options[mm.selectedIndex].text=='--Month--'  {");
this.ggWinCal.document.write("this.document.write('<p class=text_red>Select Month</p>');}");
*/

this.ggWinCal.document.write("default : vData = vMonth + '\/' + vDD + '\/' + vY4;");

this.ggWinCal.document.write("}");
this.ggWinCal.document.write("if (yy.options[yy.selectedIndex].text=='--Year--')  { vData='';bFlag='N';merrorY.innerHTML='select Year';merrorM.innerHTML='';merrorD.innerHTML='';}");
this.ggWinCal.document.write("else if (mm.options[mm.selectedIndex].text=='--Month--')  { vData='';bFlag='N';merrorM.innerHTML='select Month';merrorY.innerHTML='';merrorD.innerHTML='';}");
this.ggWinCal.document.write("else if (dd.options[dd.selectedIndex].text=='--Day--')  { vData='';bFlag='N';merrorD.innerHTML='select Day';merrorM.innerHTML='';merrorY.innerHTML='';} else bFlag='Y';");


/*this.ggWinCal.document.write("alert('');");*/
this.ggWinCal.document.write("return vData;");
this.ggWinCal.document.write("}");
this.ggWinCal.document.write("function closeMe()");
this.ggWinCal.document.write("{ ");
this.ggWinCal.document.write(" if (bFlag=='Y') {this.window.close();}");
this.ggWinCal.document.write("}");
this.ggWinCal.document.write("</script></head> <body  >");
this.ggWinCal.document.write("<select name=yy id=yy class='signup'  onchange=fn_LoadMonth()>");
this.ggWinCal.document.write("<option value="+ i + ">--Year--</option>");
for(var i=1900;i<=varDate.getFullYear() ;i++)
{
	this.ggWinCal.document.write("<option value="+ i + ">"+ i+ "</option>");
}
this.ggWinCal.document.write("</select>");
this.ggWinCal.document.write("<select name=mm id=mm class='signup' onchange=fn_LoadDay() >");
this.ggWinCal.document.write("<option value="+ i + ">--Month--</option>");
this.ggWinCal.document.write("</select>");
this.ggWinCal.document.write("<select name=dd id=dd class='signup'>");
this.ggWinCal.document.write("<option value="+ i + ">--Day--</option>");
this.ggWinCal.document.write("</select>");
/*this.ggWinCal.document.write("<br><center>");*/
this.ggWinCal.document.write("&nbsp&nbsp<input type=image name=OK value=OK id=OK src='../images/ok.gif' onclick=self.opener.document.");
this.ggWinCal.document.write(this.date_Item + ".value=" );
this.ggWinCal.document.write("date_Format();closeMe();>");
this.ggWinCal.document.write("<br>");
this.ggWinCal.document.write("<table width='100%' ><tr><td width='30%'><span id=merrorY align=left style='font-family : Tahoma, Verdana, Arial, Helvetica;font-size : 8pt;");
this.ggWinCal.document.write("font-style : normal;font-variant : normal;font-weight : normal;color: #FF0000' ></span></td>");
this.ggWinCal.document.write("<td width='30%'><span id=merrorM align=left style='font-family : Tahoma, Verdana, Arial, Helvetica;font-size : 8pt;");
this.ggWinCal.document.write("font-style : normal;font-variant : normal;font-weight : normal;color: #FF0000' ></span></td>");
this.ggWinCal.document.write("<td width='30%'><span id=merrorD align=left style='font-family : Tahoma, Verdana, Arial, Helvetica;font-size : 8pt;");
this.ggWinCal.document.write("font-style : normal;font-variant : normal;font-weight : normal;color: #FF0000' ></span></td><td></td></tr></table>");
/*
this.ggWinCal.document.write("<a href='self.opener.document.");
this.ggWinCal.document.write(this.date_Item + ".value=" );
this.ggWinCal.document.write("date_Format();window.close();>");
this.ggWinCal.document.write("ee<IMG height='12' src='../images/ok.gif' width='14' border='0'></A>");
*/

this.ggWinCal.document.write("</body></html>");



}



// This function for DateAdd

function DateAdd(startDate, numDays, numMonths, numYears)
		{
			var returnDate = new Date(startDate.getTime());
			var yearsToAdd = numYears;
			
			var month = returnDate.getMonth()	+ numMonths;
			if (month > 11)
			{
				yearsToAdd = Math.floor((month+1)/12);
				month -= 12*yearsToAdd;
				yearsToAdd += numYears;
			}
			returnDate.setMonth(month);
			returnDate.setFullYear(returnDate.getFullYear()	+ yearsToAdd);
			
			returnDate.setTime(returnDate.getTime()+60000*60*24*numDays);
			
			//var str=returnDate.getFullYear.toString+''+returnDate.getMonth.toString+''+returnDate.getDay.toString
			return returnDate;

		}
	//Added By Anoop Jayaraj For New Calendar Control	
	function fncOpenPopup(page) 
		{
			breedte= window.screen.width;
			hoogte= window.screen.height;
			iks=(breedte-150)/2;
			ij=(hoogte-200)/2;
			var Calendar_window=window.open(page,'Calender','width=210,height=195,menubar=no,screenX='+iks+',screenY='+ij+',top='+ij+',left='+iks+'');
			Calendar_window.focus()
			return false;
		}
		