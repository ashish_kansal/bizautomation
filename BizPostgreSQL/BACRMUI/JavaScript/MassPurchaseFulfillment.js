﻿var recordsPerPage = 200;
var totalRecords = 0;

$(document).ajaxStart(function () {
    $("#divLoader").show();
}).ajaxStop(function () {
    $("#divLoader").hide();
});

function setButtonByHiddenValue() {
    if ($("input[id$=hdnViewID]").val() == "1"){
        $(".liReceive").addClass("active");
        $(".liPutAway").removeClass("active");
        $(".liBill").removeClass("active");
        $(".liClose").removeClass("active");
    } if ($("input[id$=hdnViewID]").val() == "2"){
        $(".liReceive").removeClass("active");
        $(".liPutAway").addClass("active");
        $(".liBill").removeClass("active");
        $(".liClose").removeClass("active");
    } if ($("input[id$=hdnViewID]").val() == "3"){
        $(".liReceive").removeClass("active");
        $(".liPutAway").removeClass("active");
        $(".liBill").addClass("active");
        $(".liClose").removeClass("active");
    }
    if ($("input[id$=hdnViewID]").val() == "4"){
        $(".liReceive").removeClass("active");
        $(".liPutAway").removeClass("active");
        $(".liBill").removeClass("active");
        $(".liClose").addClass("active");
    }
}
$(document).ready(function () {
    $("#dtReceivedDate").val($.datepicker.formatDate('mm/dd/yy', new Date()));
    $("#dtPutAwayDate").val($.datepicker.formatDate('mm/dd/yy', new Date()));

    $("#dtReceivedDate").datepicker({
        dateFormat: 'mm/dd/yy'
        , defaultDate: new Date()
    })

    $("#ms-pagination-left").pagination({
        items: 0,
        itemsOnPage: recordsPerPage,
        displayedPages: 3,
        hrefTextPrefix: "#"
    });

    $.when(LoadOrderStatus(), LoadWarehouses(), LoadMassPurchaseFulfillmentConfiguration()).then(function () {

        /*added this script so that we can default the view as and when needed. this is used in dashboard report .where it redirects to PO fulfillment page*/
        if ($("#hdnViewID").val() == "2") {
            $("#btnPutAway").click();
        }
        else if ($("#hdnViewID").val() == "3") {
            $("#btnBill").click();
        }
        else if ($("#hdnViewID").val() == "4") {
            $("#btnClose").click();
        }


        else {
            LoadRecordsWithPagination();
        }
    });
    setButtonByHiddenValue()
    $("#btncolumnconfig").click(function () {
        var h = screen.height;
        var w = screen.width;

        window.open('../opportunity/frmMassPurchaseFulfillmentConfig.aspx', '', 'toolbar=no,titlebar=no,top=50,left=50,width=' + (w - 100) + ',height=' + (h - 200) + ',scrollbars=yes,resizable=yes');
        return false;
    });

    $("#btnReceive").click(function () {
        ResetPanes();
        $("input[id$=hdnViewID]").val("1");
        $(".liReceive").addClass("active");
        $(".liPutAway").removeClass("active");
        $(".liBill").removeClass("active");
        $(".liClose").removeClass("active");
        $("#divshowrecords").show();
        $(".ShowRecords").show();
        $("#ms-sidebar").css("border-right", "5px solid #00a65a");
        $("#divMainGrid").css("height", "72vh");
        $("#divExpandCollpaseGroup").show();
        $("#ms-selected-grid").show();
        if ($("#ms-main-grid").hasClass("col-md-12")) {
            $("#ms-main-grid").removeClass("col-md-12").addClass("col-md-10");
        } else if ($("#ms-main-grid").hasClass("col-md-10")) {
            $("#ms-main-grid").removeClass("col-md-10").addClass("col-md-7");
        } else {
            $("#ms-main-grid").removeClass("col-md-9").addClass("col-md-7");
        }
        $("#liGroupBy").show();
        $("#chkGroupBy").prop("checked", $("#hdnGroupByOrderReceive").val() === "true" ? "checked" : "");
        $("#btnReceiveOnly").show();
        $("#btnAddBill").hide();
        $("#btnCloseOrders").hide();
        ClearFilters();
        ClearRightPaneSelection();
        LoadRecordsWithPagination();
    });

    $("#btnPutAway").click(function () {
        ResetPanes();
        $("input[id$=hdnViewID]").val("2");
        $(".liReceive").removeClass("active");
        $(".liPutAway").addClass("active");
        $(".liBill").removeClass("active");
        $(".liClose").removeClass("active");
        $("#divshowrecords").hide();
        $(".ShowRecords").hide();
        $("#chkshowrecords").prop('checked', false)
        $("#ms-sidebar").css("border-right", "5px solid #0073b7");
        $("#divMainGrid").css("height", "72vh");
        $("#divExpandCollpaseGroup").show();
        $("#ms-selected-grid").show();
        if ($("#ms-main-grid").hasClass("col-md-12")) {
            $("#ms-main-grid").removeClass("col-md-12").addClass("col-md-10");
        } else if ($("#ms-main-grid").hasClass("col-md-10")) {
            $("#ms-main-grid").removeClass("col-md-10").addClass("col-md-7");
        } else {
            $("#ms-main-grid").removeClass("col-md-9").addClass("col-md-7");
        }
        $("#liGroupBy").show();
        $("#chkGroupBy").prop("checked", $("#hdnGroupByOrderPutAway").val() === "true" ? "checked" : "");
        $("#btnReceiveOnly").hide();
        $("#btnAddBill").hide();
        $("#btnCloseOrders").hide();
        ClearFilters();
        ClearRightPaneSelection();
        LoadRecordsWithPagination();
    });

    $("#btnBill").click(function () {
        ResetPanes();
        $("input[id$=hdnViewID]").val("3");
        $(".liReceive").removeClass("active");
        $(".liPutAway").removeClass("active");
        $(".liBill").addClass("active");
        $(".liClose").removeClass("active");
        $("#divshowrecords").hide();
        $(".ShowRecords").hide();
        $("#chkshowrecords").prop('checked', false)
        $("#ms-sidebar").css("border-right", "5px solid #ff851b");
        $("#divMainGrid").css("height", "72vh");
        $("#divExpandCollpaseGroup").hide();
        $("#ms-selected-grid").hide();
        if ($("#ms-main-grid").hasClass("col-md-9")) {
            $("#ms-main-grid").removeClass("col-md-9").addClass("col-md-12");
        } else {
            $("#ms-main-grid").removeClass("col-md-7").addClass("col-md-10");
        }
        $("#liGroupBy").show();
        $("#chkGroupBy").prop("checked", $("#hdnGroupByOrderBill").val() === "true" ? "checked" : "");
        $("#btnReceiveOnly").hide();
        $("#btnAddBill").show();
        $("#btnCloseOrders").hide();
        ClearFilters();
        ClearRightPaneSelection();
        LoadRecordsWithPagination();
    });

    $("#btnClose").click(function () {
        ResetPanes();
        $("input[id$=hdnViewID]").val("4");
        $(".liReceive").removeClass("active");
        $(".liPutAway").removeClass("active");
        $(".liBill").removeClass("active");
        $(".liClose").addClass("active");
        $("#divshowrecords").hide();
        $(".ShowRecords").hide();
        $("#chkshowrecords").prop('checked', false)
        $("#ms-sidebar").css("border-right", "5px solid #c00000");
        $("#divMainGrid").css("height", "72vh");
        $("#divExpandCollpaseGroup").hide();
        $("#ms-selected-grid").hide();
        $("#liGroupBy").hide();
        if ($("#ms-main-grid").hasClass("col-md-9")) {
            $("#ms-main-grid").removeClass("col-md-9").addClass("col-md-12");
        } else {
            $("#ms-main-grid").removeClass("col-md-7").addClass("col-md-10");
        }
        $("#liGroupBy").hide();
        $("#chkGroupBy").prop("checked", "");
        $("#btnReceiveOnly").hide();
        $("#btnAddBill").hide();
        $("#btnCloseOrders").show();
        ClearFilters();
        ClearRightPaneSelection();
        LoadRecordsWithPagination();
    });

    $("#chkGroupBy").change(function () {
        LoadRecordsWithPagination();
    });

    $("#chkshowrecords").change(function () {
        LoadRecordsWithPagination();
    });

    $("#btnReceiveOnly").click(function () {
        ClearRightPaneSelection();

        if ($("input[id$=hdnViewID]").val() === "1") {
            try {
                var isValidData = true;
                var errorMessage = "";
                var selectdRecords = [];

                $("#divMainGrid > table > tbody > tr").not(":first").each(function (index, e) {
                    if ($(e).find(".chkSelect").is(":checked")) {
                        var obj = {};
                        obj.OppID = parseInt($(e).find("#hdnOppID").val());
                        obj.WOID = parseInt($(e).find("#hdnWOID").val());
                        obj.IsSuccess = true;
                        obj.ErrorMessage = "";

                        if(obj.WOID > 0){
                            obj.OppItemID = 0;

                            if ($(e).find(".txtQtyToReceive").length > 0) {
                                if (parseFloat($(e).find(".txtQtyToReceive").val().replace(/,/g, '')) > 0) {
                                    obj.QtyToReceive = parseFloat($(e).find(".txtQtyToReceive").val().replace(/,/g, ''));
                                } else {
                                    return;
                                }
                            } else {
                                if (parseFloat($(e).find("#hdnRemainingQty").val()) > 0) {
                                    obj.QtyToReceive = parseFloat($(e).find("#hdnRemainingQty").val());
                                } else {
                                    return;
                                }
                            }

                            if (obj.QtyToReceive > obj.RemainingQty) {
                                isValidData = false;
                                errorMessage = "Qty to receive can not be greater then quantity built.";
                                $(e).find(".txtQtyToReceive").focus();
                                return false;
                            }

                            selectdRecords.push(obj);
                        } else if (parseInt($(e).find("#hdnOppItemID").val()) > 0) {
                            obj.OppItemID = parseInt($(e).find("#hdnOppItemID").val());

                            if ($(e).find(".txtQtyToReceive").length > 0) {
                                if (parseFloat($(e).find(".txtQtyToReceive").val().replace(/,/g, '')) > 0) {
                                    obj.QtyToReceive = parseFloat($(e).find(".txtQtyToReceive").val().replace(/,/g, ''));
                                } else {
                                    return;
                                }
                            } else {
                                if (parseFloat($(e).find("#hdnRemainingQty").val()) > 0) {
                                    obj.QtyToReceive = parseFloat($(e).find("#hdnRemainingQty").val());
                                } else {
                                    return;
                                }
                            }

                            if (obj.QtyToReceive > obj.RemainingQty) {
                                isValidData = false;
                                errorMessage = "Qty to receive can not be greater than ordered quantity.";
                                $(e).find(".txtQtyToReceive").focus();
                                return false;
                            }

                            selectdRecords.push(obj);
                        } else {
                            obj.OppItemID = 0;
                            selectdRecords.push(obj);
                        }
                    }
                });

                if (isValidData) {
                    if (selectdRecords.length > 0) {
                        $.ajax({
                            type: "POST",
                            url: '../opportunity/MassPurchaseFulfillmentService.svc/ReceiveItems',
                            contentType: "application/json",
                            dataType: "json",
                            data: JSON.stringify({
                                "selectedRecords": JSON.stringify(selectdRecords)
                            }),
                            success: function (data) {
                                var obj = $.parseJSON(data.ReceiveItemsResult);

                                var foundRecordsWithError = $.map(obj, function (e) {
                                    if (!e.IsSuccess)
                                        return e;
                                });

                                if (foundRecordsWithError.length > 0) {
                                    alert("Error occurred while receiving some item(s). please review records and try again.");

                                    obj.forEach(function (lineItem) {
                                        if (lineItem.IsSuccess) {
                                            $("#divMainGrid > table > tbody > tr").not(":first").each(function (index, e) {
                                                if (lineItem.OppID == $(e).find("#hdnOppID").val() && lineItem.OppItemID == $(e).find("#hdnOppItemID").val()) {
                                                    $(e).remove();
                                                }
                                            });
                                        } else {
                                            $("#divMainGrid > table > tbody > tr").not(":first").each(function (index, e) {
                                                if (lineItem.OppID == $(e).find("#hdnOppID").val() && lineItem.OppItemID == $(e).find("#hdnOppItemID").val()) {
                                                    $(e).css("background-color", "#ffdada");
                                                    $(this).find(".chkSelect").closest("td").find(".receive-items").remove();
                                                    $(this).find(".chkSelect").closest("td").append("<i class='fa fa-exclamation-triangle receive-items' style='color:red' data-toggle='tooltip' title='" + replaceNull(lineItem.ErrorMessage) + "' aria-hidden='true'></i>");
                                                }
                                            });
                                        }
                                    });
                                } else {
                                    alert("Item(s) are received successfully.")
                                    ClearFilters();
                                    ClearRightPaneSelection();
                                    LoadRecordsWithPagination();
                                }
                            },
                            error: function (jqXHR, textStatus, errorThrown) {
                                if (IsJsonString(jqXHR.responseText)) {
                                    var objError = $.parseJSON(jqXHR.responseText)
                                    alert(objError.ErrorMessage);
                                } else {
                                    alert("Unknown error ocurred");
                                }
                            }
                        });
                    } else {
                        alert("No records selected.")
                    }
                } else if (errorMessage != "") {
                    alert(errorMessage);
                }
            } catch (e) {
                alert("Unknown error occurred while receiving item(s)");
            }
        }

        return false;
    });

    $("#btnPutAwayItems").click(function (){
        try {
            if($("input[id$=hdnViewID]").val() === "1" || $("input[id$=hdnViewID]").val() === "2"){
                if ($("#dtPutAwayDate").value == "") {
                    $("#dtPutAwayDate").focus();
                    alert("Enter Received Date");
                } else {
                    if($('#tblRight > tbody > tr').length > 0) {
                        var itemsPutAway = [];


                        var isDataValid = true;
                        $('#tblRight > tbody > tr').each(function(){
                            var objItem = {};
                            objItem.OppID = parseInt($(this).find("#hdnOppID").val());
                            objItem.WOID = parseInt($(this).find("#hdnWOID").val());
                            objItem.OppItemID = parseInt($(this).find("#hdnOppItemID").val());
                            objItem.DivisionID = parseInt($(this).find("#hdnDivisionID").val());
                            objItem.OppName = replaceNull($(this).find("#hdnOppName").val());
                            objItem.IsStockTransfer =  JSON.parse($(this).find("#hdnIsStockTransfer").val());
                            objItem.IsPPVariance = JSON.parse($(this).find("#hdnPPVariance").val());
                            objItem.CurrencyID = parseInt($(this).find("#hdnCurrencyID").val());
                            objItem.ExchangeRate = parseFloat($(this).find("#hdnExchangeRate").val());
                            objItem.ItemCode = $(this).find("#hdnItemCode").val();
                            objItem.ItemName = $(this).find("#hdnItemName").val();
                            objItem.QtyToReceive  = 0;
                            objItem.WarehouseItemIDs = "";
                            objItem.RemainingQty = parseFloat($(this).find("#hdnRemainingQty").val());
                            objItem.IsSerial = JSON.parse($(this).find("#hdnIsSerial").val());
                            objItem.IsLot = JSON.parse($(this).find("#hdnIsLot").val());
                            objItem.IsValidLotNo = true;
                            objItem.CharItemType = replaceNull($(this).find("#hdncharItemType").val());
                            objItem.ItemType = replaceNull($(this).find("#hdnItemType").val());
                            objItem.Price = parseFloat($(this).find("#hdnPrice").val());
                            objItem.DropShip = JSON.parse($(this).find("#hdnDropShip").val());
                            objItem.ProjectID = parseInt($(this).find("#hdnProjectID").val());
                            objItem.ClassID = parseInt($(this).find("#hdnClassID").val());
                            objItem.AssetChartAcntId = parseInt($(this).find("#hdnAssetChartAcntId").val());
                            objItem.COGsChartAcntId = parseInt($(this).find("#hdnCOGsChartAcntId").val());
                            objItem.IncomeChartAcntId = parseInt($(this).find("#hdnIncomeChartAcntId").val());
                            objItem.IsSuccess = true;
                            objItem.ErrorMessage = "";

                            $(this).find(".tblPutAwayLocation").find("tr").each(function(){
                                var serialLotQty = 0;

                                if(objItem.IsLot){
                                    objItem.IsValidLotNo = IsValidSerialLot($(this).find(".txtPutAway"));
                                }

                                if(parseInt($(this).find(".hdnWarehouseItemID").val()) > 0){
                                    if(objItem.IsSerial || objItem.IsLot){
                                        if($(this).find(".txtPutAway").val().trim() != "0"){
                                            serialLotQty = GetSerialLotQty($(this).find(".txtPutAway"),objItem.IsSerial,objItem.IsLot);
                                            objItem.QtyToReceive += serialLotQty;
                                            objItem.WarehouseItemIDs += ("<Table1><bitNewLocation>0</bitNewLocation><ID>" + (parseInt($(this).find(".hdnWarehouseItemID").val())) + "</ID><Qty>" + serialLotQty + "</Qty><SerialLotNo>" + $(this).find(".txtPutAway").val() + "</SerialLotNo></Table1>");
                                        }
                                    } else {
                                        if(parseFloat($(this).find(".txtPutAway").val().replace(/,/g, '')) > 0){
                                            objItem.QtyToReceive +=  parseFloat($(this).find(".txtPutAway").val().replace(/,/g, ''));
                                            objItem.WarehouseItemIDs += ("<Table1><bitNewLocation>0</bitNewLocation><ID>" + (parseInt($(this).find(".hdnWarehouseItemID").val())) + "</ID><Qty>" + parseFloat($(this).find(".txtPutAway").val().replace(/,/g, '')) + "</Qty><SerialLotNo></SerialLotNo></Table1>");
                                        }
                                    }
                                } else if(parseInt($(this).find(".hdnWarehouseLocationID").val()) > 0){
                                    if(objItem.IsSerial || objItem.IsLot){
                                        if($(this).find(".txtPutAway").val().trim() != "0"){
                                            serialLotQty = GetSerialLotQty($(this).find(".txtPutAway"),objItem.IsSerial,objItem.IsLot);
                                            objItem.QtyToReceive += serialLotQty;
                                            objItem.WarehouseItemIDs += ("<Table1><bitNewLocation>0</bitNewLocation><ID>" + (parseInt($(this).find(".hdnWarehouseItemID").val())) + "</ID><Qty>" + serialLotQty + "</Qty><SerialLotNo>" + $(this).find(".txtPutAway").val() + "</SerialLotNo></Table1>");
                                        }
                                    } else {
                                        if(parseFloat($(this).find(".txtPutAway").val().replace(/,/g, '')) > 0){
                                            objItem.QtyToReceive +=  parseFloat($(this).find(".txtPutAway").val().replace(/,/g, ''));
                                            objItem.WarehouseItemIDs += ("<Table1><bitNewLocation>0</bitNewLocation><ID>" + (parseInt($(this).find(".hdnWarehouseLocationID").val())) + "</ID><Qty>" + parseFloat($(this).find(".txtPutAway").val().replace(/,/g, '')) + "</Qty><SerialLotNo></SerialLotNo></Table1>");
                                        }
                                    }
                                }                                
                            });

                            if (objItem.QtyToReceive == 0 || objItem.WarehouseItemIDs == "") {
                                return;
                            } else {
                                objItem.WarehouseItemIDs = "<NewDataSet>" + objItem.WarehouseItemIDs + "</NewDataSet>";
                            }

                            if (objItem.QtyToReceive > objItem.RemainingQty) {
                                isDataValid = false;
                                errorMessage = "Qty to receive can not be greater than ordered quantity.";
                                return false;
                            } else if (objItem.IsLot && !objItem.IsValidLotNo) {
                                isDataValid = false;
                                errorMessage = "Lot#s are invalid (must be in format like abc(100) where lot no is abc and quantity is 100).";
                                return false;
                            }

                            itemsPutAway.push(objItem);
                        });

                        if (isDataValid) {
                            if (itemsPutAway.length > 0) {
                                return $.ajax({
                                    type: "POST",
                                    url: '../opportunity/MassPurchaseFulfillmentService.svc/PutAwayItems',
                                    contentType: "application/json",
                                    dataType: "json",
                                    data: JSON.stringify({
                                        "mode": ($("input[id$=hdnViewID]").val() === "2" ? 3 : 1)
                                        , "receivedDate": "\/Date(" + new Date($("#dtPutAwayDate").val()).getTime() + ")\/"
                                        , "selectedRecords": JSON.stringify(itemsPutAway)
                                    }),
                                    success: function (data) {
                                        var obj = $.parseJSON(data.PutAwayItemsResult);

                                        var foundRecordsWithError = $.map(obj, function (e) {
                                            if (!e.IsSuccess)
                                                return e;
                                        });

                                        if (foundRecordsWithError.length > 0) {
                                            alert("Error occurred while receiving some item(s). please review records and try again.");

                                            obj.forEach(function (lineItem) {
                                                if (lineItem.IsSuccess) {
                                                    $("#divMainGrid > table > tbody > tr").not(":first").each(function (index, e) {
                                                        if (lineItem.OppID == $(e).find("#hdnOppID").val() && lineItem.OppItemID == $(e).find("#hdnOppItemID").val()) {
                                                            $(e).remove();
                                                        }
                                                    });
                                                } else {
                                                    $("#divMainGrid > table > tbody > tr").not(":first").each(function (index, e) {
                                                        if (lineItem.OppID == $(e).find("#hdnOppID").val() && lineItem.OppItemID == $(e).find("#hdnOppItemID").val()) {
                                                            $(e).css("background-color", "#ffdada");
                                                            $(this).find(".chkSelect").closest("td").find(".receive-items").remove();
                                                            $(this).find(".chkSelect").closest("td").append("<i class='fa fa-exclamation-triangle receive-items' style='color:red' data-toggle='tooltip' title='" + replaceNull(lineItem.ErrorMessage) + "' aria-hidden='true'></i>");
                                                        } else if (lineItem.OppID == $(e).find("#hdnOppID").val()) {
                                                            $(e).css("background-color", "#ffdada");
                                                            $(this).find(".chkSelect").closest("td").find(".receive-items").remove();
                                                            $(this).find(".chkSelect").closest("td").append("<i class='fa fa-exclamation-triangle receive-items' style='color:red' data-toggle='tooltip' title='" + replaceNull(lineItem.ErrorMessage) + "' aria-hidden='true'></i>");
                                                        }
                                                    });
                                                }
                                            });
                                        } else {
                                            alert("Item(s) are received successfully.")
                                            ClearFilters();
                                            ClearRightPaneSelection();
                                            LoadRecordsWithPagination();
                                        }
                                    },
                                    error: function (jqXHR, textStatus, errorThrown) {
                                        if (IsJsonString(jqXHR.responseText)) {
                                            var objError = $.parseJSON(jqXHR.responseText)
                                            alert(objError.ErrorMessage);
                                        } else {
                                            alert("Unknown error ocurred");
                                        }                                
                                    }
                                });   
                            } else {
                                alert("No record found with put-away quantity.");
                            }
                        } else {
                            alert(errorMessage);
                            return false;
                        }
                    } else {
                        alert("No records loaded in right pane to put-away");
                    }
                }
            }
        } catch (e) {
            alert("Unknown error occurred while put-away item(s).");
        }
                
    });

    $("#btnAddBill").click(function () {
        try {
            var selectdRecords = [];

            $("#divMainGrid > table > tbody > tr").not(":first").each(function (index, e) {
                if ($(e).find(".chkSelect").is(":checked")) {
                    var found = $.map(selectdRecords, function (obj) {
                        if (obj.OppID === parseInt($(e).find("#hdnOppID").val()))
                            return obj;
                    });

                    if (found.length === 0) {
                        var obj = {};
                        obj.OppID = parseInt($(e).find("#hdnOppID").val());
                        obj.InvoiceNo = "";
                        obj.BillingDate = ""

                        if ($(e).find("#txtInvoiceNo").length > 0) {
                            if ($(e).find("#txtInvoiceNo").val() != "") {
                                obj.InvoiceNo = $(e).find("#txtInvoiceNo").val();
                            }
                        }

                        if ($(e).find(".txtBillingDate").length > 0) {
                            if ($(e).find(".txtBillingDate").val() != "") {
                                obj.BillingDate = $(e).find(".txtBillingDate").val();
                            }
                        }

                        if (parseInt($(e).find("#hdnOppItemID").val()) > 0) {
                            if ($(e).find(".txtQtyToReceive").length > 0) {
                                if (parseFloat($(e).find(".txtQtyToReceive").val().replace(/,/g, '')) > 0) {
                                    obj.OppItemIDs = parseInt($(e).find("#hdnOppItemID").val()).toString() + "-" + parseFloat($(e).find(".txtQtyToReceive").val().replace(/,/g, ''));
                                } else {
                                    return;
                                }
                            } else {
                                if (parseFloat($(e).find("#hdnRemainingQty").val()) > 0) {
                                    obj.OppItemIDs = parseInt($(e).find("#hdnOppItemID").val()).toString() + "-" + parseFloat($(e).find("#hdnRemainingQty").val());
                                } else {
                                    return;
                                }
                            }
                        } else {
                            obj.OppItemIDs = "";
                        }
                        obj.IsSucess = false;
                        obj.ErrorMessage = "";
                        selectdRecords.push(obj);
                    } else {
                        if ($(e).find("#txtInvoiceNo").length > 0) {
                            if ($(e).find("#txtInvoiceNo").val() != "") {
                                obj.InvoiceNo = $(e).find("#txtInvoiceNo").val();
                            }
                        }

                        if (parseInt($(e).find("#hdnOppItemID").val()) > 0) {
                            if ($(e).find(".txtQtyToReceive").length > 0) {
                                if (parseFloat($(e).find(".txtQtyToReceive").val().replace(/,/g, '')) > 0) {
                                    found[0].OppItemIDs = found[0].OppItemIDs + "," + (parseInt($(e).find("#hdnOppItemID").val()).toString() + "-" + parseFloat($(e).find(".txtQtyToReceive").val().replace(/,/g, '')));
                                } else {
                                    return;
                                }
                            } else {
                                if (parseFloat($(e).find("#hdnRemainingQty").val()) > 0) {
                                    found[0].OppItemIDs = found[0].OppItemIDs + "," + (parseInt($(e).find("#hdnOppItemID").val()).toString() + "-" + parseFloat($(e).find("#hdnRemainingQty").val()));
                                } else {
                                    return;
                                }
                            }
                        }
                    }
                }
            });

            if (selectdRecords.length > 0) {
                $.ajax({
                    type: "POST",
                    url: '../opportunity/MassPurchaseFulfillmentService.svc/BillOrders',
                    contentType: "application/json",
                    dataType: "json",
                    data: JSON.stringify({
                        "orders": JSON.stringify(selectdRecords)
                    }),
                    success: function (data) {
                        try {
                            var obj = $.parseJSON(data.BillOrdersResult);

                            var foundRecordsWithError = $.map(obj, function (e) {
                                if (!e.IsSucess)
                                    return e;
                            });

                            if (foundRecordsWithError.length > 0) {
                                alert("Error occurred while adding bill to some order(s). please review records and try again.");

                                obj.forEach(function (lineItem) {
                                    if (lineItem.IsSucess) {
                                        $("#divMainGrid > table > tbody > tr").not(":first").each(function (index, e) {
                                            if (lineItem.OppID == $(e).find("#hdnOppID").val()) {
                                                $(e).remove();
                                            }
                                        });
                                    } else {
                                        $("#divMainGrid > table > tbody > tr").not(":first").each(function (index, e) {
                                            if (lineItem.OppID == $(e).find("#hdnOppID").val()) {
                                                $(e).css("background-color", "#ffdada");
                                                $(this).find(".chkSelect").closest("td").find(".bill-orders").remove();
                                                $(this).find(".chkSelect").closest("td").append("<i class='fa fa-exclamation-triangle bill-orders' style='color:red' data-toggle='tooltip' title='" + replaceNull(lineItem.ErrorMessage) + "' aria-hidden='true'></i>");
                                            }
                                        });
                                    }
                                });
                            } else {
                                ClearFilters();
                                ClearRightPaneSelection();
                                LoadRecordsWithPagination();
                                alert("Bill(s) are added successfully for selected order(s).");
                            }

                        } catch (err) {
                            alert("Unknown error occurred.");
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        if (IsJsonString(jqXHR.responseText)) {
                            var objError = $.parseJSON(jqXHR.responseText)
                            alert("Error occurred: " + replaceNull(objError.ErrorMessage));
                        } else {
                            alert("Unknown error ocurred");
                        }
                    }
                });
            } else {
                alert("Select records.")
            }
        } catch (e) {
            alert("Unknown error occurred while adding bill(s)")
        }

        return false;
    });

    $("#btnCloseOrders").click(function () {
        try {
            var selectdRecords = [];

            $("#divMainGrid > table > tbody > tr").not(":first").each(function (index, e) {
                if ($(e).find(".chkSelect").is(":checked")) {
                    var found = $.map(selectdRecords, function (obj) {
                        if (obj.OppID === parseInt($(e).find("#hdnOppID").val()))
                            return obj;
                    });

                    if (found.length === 0) {
                        var obj = {};
                        obj.OppID = parseInt($(e).find("#hdnOppID").val());
                        obj.IsSucess = false;
                        obj.ErrorMessage = "";
                        selectdRecords.push(obj);
                    }
                }
            });

            if (selectdRecords.length > 0) {
                $.ajax({
                    type: "POST",
                    url: '../opportunity/MassPurchaseFulfillmentService.svc/CloseOrders',
                    contentType: "application/json",
                    dataType: "json",
                    data: JSON.stringify({
                        "orders": JSON.stringify(selectdRecords)
                        ,"isReceiveBillOnClose": $("#hdnReceiveBillOnClose").val()
                    }),
                    success: function (data) {
                        try {
                            var obj = $.parseJSON(data.CloseOrdersResult);

                            var foundRecordsWithError = $.map(obj, function (e) {
                                if (!e.IsSucess)
                                    return e;
                            });

                            if (foundRecordsWithError.length > 0) {
                                alert("Error occurred while closing some order(s). please review records and try again.");

                                obj.forEach(function (lineItem) {
                                    if (lineItem.IsSucess) {
                                        $("#divMainGrid > table > tbody > tr").not(":first").each(function (index, e) {
                                            if (lineItem.OppID == $(e).find("#hdnOppID").val()) {
                                                $(e).remove();
                                            }
                                        });
                                    } else {
                                        $("#divMainGrid > table > tbody > tr").not(":first").each(function (index, e) {
                                            if (lineItem.OppID == $(e).find("#hdnOppID").val()) {
                                                $(e).css("background-color", "#ffdada");
                                                $(this).find(".chkSelect").closest("td").find(".close-orders").remove();
                                                $(this).find(".chkSelect").closest("td").append("<i class='fa fa-exclamation-triangle close-orders' style='color:red' data-toggle='tooltip' title='" + replaceNull(lineItem.ErrorMessage) + "' aria-hidden='true'></i>");
                                            }
                                        });
                                    }
                                });
                            } else {
                                ClearFilters();
                                ClearRightPaneSelection();
                                LoadRecordsWithPagination();
                                alert("Selected order(s) are closed successfully.");
                            }

                        } catch (err) {
                            alert("Unknown error occurred.");
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        if (IsJsonString(jqXHR.responseText)) {
                            var objError = $.parseJSON(jqXHR.responseText)
                            alert("Error occurred: " + replaceNull(objError.ErrorMessage));
                        } else {
                            alert("Unknown error ocurred");
                        }
                    }
                });
            } else {
                alert("Select records.")
            }
        } catch (e) {
            alert("Unknown error occured while closing selected order(s).")
        }
    });

    $("#btncollapseselectedgrid").click(function () {
        if ($("#ms-main-grid").hasClass("col-md-9")) {
            $("#ms-main-grid").removeClass("col-md-9").addClass("col-md-12");
        } else {
            $("#ms-main-grid").removeClass("col-md-7").addClass("col-md-10");
        }

        $("#ms-container").removeClass("ms-sidebar-collapsed").addClass("ms-sidebar-expanded");
        $("#btnmssidebar").html("<i class='fa fa-chevron-left'></i>");
        $("#ms-container").removeClass("ms-maingrid-collapsed").addClass("ms-maingrid-expanded");
        $("#ms-container").removeClass("ms-selectedgrid-expanded").addClass("ms-selectedgrid-collapsed");
        $("#ms-selected-grid").attr("class", "col-sm-12 col-md-3");
    });

    $("#btncollapsemaingrid").click(function () {
        $("#ms-container").removeClass("ms-sidebar-expanded").addClass("ms-sidebar-collapsed");
        $("#btnmssidebar").html("<i class='fa fa-chevron-right'></i>");
        $("#ms-main-grid").removeClass("col-md-12").removeClass("col-md-10").addClass("col-md-7");

        $("#ms-container").removeClass("ms-maingrid-expanded").addClass("ms-maingrid-collapsed");
        $("#ms-container").removeClass("ms-selectedgrid-collapsed").addClass("ms-selectedgrid-expanded");
        $("#ms-selected-grid").removeClass("col-md-3").addClass("col-md-12");
    });

    $("#btnexpandbothgrid").click(function () {
        ResetPanes();
    });

    $("#ddlWarehouse").change(function () {
        ClearRightPaneSelection();
        LoadRecordsWithPagination();
    });

    $("#btnApplyFilters").click(function () {
        LoadRecordsWithPagination();
    });

    $("#btnClearFilters").click(function () {
        ClearFilters();
        LoadRecordsWithPagination();
    });

    $("#btnAddLocation").click(function(){
        var obj = $('.location-dropdown').select2('data');

        if (obj == null) {
            alert("Please select internal location")
            return false;
        }

        if($("#tblRight tr.focusedRow").length > 0){

            var html = "<tr>";
            html += "<td>" + replaceNull(obj.text) + " <input type='hidden' class='hdnWarehouseLocationID' value='" + obj.id + "'/><input type='hidden' class='hdnWarehouseItemID' value='0'/></td>";
            html += "<td><input type='text' class='txtPutAway form-control' onfocus='FocusRow(this)' value='0' /></td>";
            html += "</tr>";

            var tr = $("#tblRight tr.focusedRow")[0];

            $(tr).find("table.tblPutAwayLocation tbody").append(html);

            PutAwatKeyDown();
        } else {
            alert("Please focus row where you want to add internal location. Place cursor in textbox in Put-away column.")
            return false;
        }
    
    });

    $('.location-dropdown').select2({
        placeholder: 'Select Location',
        dropdownCssClass: 'bigdrop',
        allowClear: true,
        maximumSelectionLength: 1,
        ajax: {
            dataType: "json",
            url: '../WebServices/CommonService.svc/GetInternalLocation',
            delay:250,
            dataType: 'json',
            type: 'POST',
            params: {
                contentType: 'application/json; charset=utf-8'
            },
            data: function (term, page) {
                return JSON.stringify({
                    warehouseID: $("#ddlWarehouse").val(),
                    searchText: term,
                    pageIndex: page || 1,
                    pageSize:20
                })
            },
            results: function (data, params) {
                var obj = $.parseJSON(data.GetInternalLocationResult)

                params.page = params.page || 1;

                return {
                    results: $.parseJSON(obj.Records),
                    pagination: {
                        more: (params.page * 20) < obj.TotalRecords
                    }
                };
            }
        }
    });
});

function replaceNull(value) {
    return String(value) === "null" || String(value) === "undefined" ? "" : value.toString().replace(/'/g, "&#39;");
}

function IsJsonString(str) {
    try {
        JSON.parse(str);
    } catch (e) {
        return false;
    }
    return true;
}

function ResetPanes() {
    $("#ms-container").removeClass("ms-maingrid-collapsed").addClass("ms-maingrid-expanded");
    $("#ms-container").removeClass("ms-selectedgrid-collapsed").addClass("ms-selectedgrid-expanded");

    if ($("#ms-main-grid").hasClass("col-md-12")) {
        $("#ms-main-grid").removeClass("col-md-12").addClass("col-md-9");
    } else {
        $("#ms-main-grid").removeClass("col-md-10").addClass("col-md-7");
    }

    $("#ms-selected-grid").attr("class", "col-sm-12 col-md-3");
}

function ClearFilters() {
    try {
        $("#divOrderStatus input[type='checkbox']").each(function () {
            $(this).prop("checked", "");
        });

        $("#divFilterValueOptions input[type='checkbox']").each(function () {
            $(this).prop("checked", "");
        });

        $("#divMainGrid table tr th input").each(function (index, e) {
            $(this).val("");
        });

        $("#divMainGrid table tr th select").each(function (index, e) {
            $(this).val("0");
        });

        $("#rbInclude").prop("checked", "checked");
        $("#rbExclude").prop("checked", "");
        $("#hdnSortColumn").val("OpportunityMaster.bintCreatedDate");
        $("#hdnSortOrder").val("DESC");
    } catch (e) {
        alert("Unknown error occurred while clearing filters.");
    }
}

function LoadOrderStatus() {
    return $.ajax({
        type: "POST",
        url: '../opportunity/MassSalesFulfillmentService.svc/GetOrderStatus',
        contentType: "application/json",
        dataType: "json",
        data: JSON.stringify({
            "oppType": 2
        }),
        success: function (data) {
            try {
                var obj = $.parseJSON(data.GetOrderStatusResult);
                if (obj != null && obj.length > 0) {
                    var strOrderStauts = "";

                    obj.forEach(function (e) {
                        strOrderStauts += "<li><div class='checkbox'><input type='checkbox' id='chkOrderStatus" + e.ListItemID + "'><label>" + replaceNull(e.ListItemValue) + " (" + replaceNull(e.StatusCount) + ")</lable></div></li>";
                    });

                    $("#divOrderStatus").html(strOrderStauts);
                }
            } catch (err) {
                alert("Unknown error occurred.");
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            if (IsJsonString(jqXHR.responseText)) {
                var objError = $.parseJSON(jqXHR.responseText)
                alert("Error occurred: " + replaceNull(objError.ErrorMessage));
            } else {
                alert("Unknown error ocurred");
            }
        }
    });
}

function LoadWarehouses() {
    return $.ajax({
        type: "GET",
        url: '../opportunity/MassSalesFulfillmentService.svc/GetWarehouses',
        contentType: "application/json",
        success: function (data) {
            try {
                var obj = $.parseJSON(data.GetWarehousesResult);
                if (obj != null && obj.length > 0) {
                    var strWarehouses = "";

                    obj.forEach(function (e) {
                        if (e.numWareHouseID == parseInt($("[id$=hdnMSWarehouseID]").val())) {
                            strWarehouses += "<option value='" + e.numWareHouseID.toString() + "' selected='selected'>" + replaceNull(e.vcWareHouse) + "</option>";
                        } else {
                            strWarehouses += "<option value='" + e.numWareHouseID.toString() + "'>" + replaceNull(e.vcWareHouse) + "</option>";
                        }
                    });

                    $("#ddlWarehouse").append(strWarehouses);
                } else {
                    $("#ddlWarehouse").append("<option value='0'>-- Select Warehouse --</option>");
                }
            } catch (err) {
                alert("Unknown error occurred.");
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            if (IsJsonString(jqXHR.responseText)) {
                var objError = $.parseJSON(jqXHR.responseText)
                alert("Error occurred: " + replaceNull(objError.ErrorMessage));
            } else {
                alert("Unknown error ocurred");
            }
        }
    });
}

function LoadMassPurchaseFulfillmentConfiguration() {
    return $.ajax({
        type: "GET",
        url: '../opportunity/MassPurchaseFulfillmentService.svc/GetMassPurchaseFulfillmentConfiguration',
        contentType: "application/json",
        success: function (data) {
            try {
                var obj = $.parseJSON(data.GetMassPurchaseFulfillmentConfigurationResult);

                $("#chkGroupBy").prop("checked", obj[0].bitGroupByOrderForReceive);
                $("#hdnGroupByOrderReceive").val(obj[0].bitGroupByOrderForReceive);
                $("#hdnGroupByOrderPutAway").val(obj[0].bitGroupByOrderForPutAway);
                $("#hdnGroupByOrderBill").val(obj[0].bitGroupByOrderForBill);
                $("#hdnGroupByOrderClose").val(obj[0].bitGroupByOrderForClose);
                $("#hdnScanField").val(obj[0].tintScanValue);
                $("#hdnReceiveBillOnClose").val(obj[0].bitReceiveBillOnClose);

            } catch (err) {
                alert("Unknown error occurred.");
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            if (IsJsonString(jqXHR.responseText)) {
                var objError = $.parseJSON(jqXHR.responseText)
                alert("Error occurred: " + replaceNull(objError.ErrorMessage));
            } else {
                alert("Unknown error ocurred");
            }
        }
    });
}

function LoadRecordsWithPagination() {
    $.when(LoadRecords()).then(function () {
        if (totalRecords > 0) {
            $("#liRecordDisplay").html("<b>Showing records " + 1 + " to " + (totalRecords > recordsPerPage ? recordsPerPage : totalRecords) + " of " + totalRecords + "</b>");
        } else {
            $("#liRecordDisplay").html("<b>Showing records 0 to 0 of 0</b>");
        }

        $("#ms-pagination-left").pagination('destroy');

        $("#ms-pagination-left").pagination({
            items: totalRecords,
            itemsOnPage: recordsPerPage,
            displayedPages: 3,
            hrefTextPrefix: "#",
            onPageClick: function (page) {
                $("#liRecordDisplay").html("<b>Showing records " + ((page - 1) * recordsPerPage + 1) + " to " + (totalRecords > (page * recordsPerPage) ? (page * recordsPerPage) : totalRecords) + " of " + totalRecords + "</b>");
                LoadRecords(page);
            }
        });
    });
}

function LoadRecords(pageIndex) {
    return $.ajax({
        type: "POST",
        url: '../opportunity/MassPurchaseFulfillmentService.svc/GetRecords',
        contentType: "application/json",
        dataType: "json",
        data: JSON.stringify({
            "viewID": parseInt($("input[id$=hdnViewID]").val())
            , "warehouseID": parseInt($("#ddlWarehouse").val())
            , "isGroupByOrder": $("#chkGroupBy").is(":checked")
            , "isIncludeSearch": $("#rbInclude").is(":checked")
            , "selectedOrderStatus": GetSelectedOrderStatus()
            , "customSearchValue": GetHeaderSearchString()
            , "pageIndex": (pageIndex || 1)
            , "sortColumn": $("#hdnSortColumn").val()
            , "sortOrder": $("#hdnSortOrder").val()
            , "tintFlag": ($("#hdnFlag").val() == 2 ? "2" : ($("#chkshowrecords").is(":checked") ? "1" : "0"))
        }),
        success: function (data) {
            try {
                var obj = $.parseJSON(data.GetRecordsResult);
                if (obj.LeftPaneFields.length === 0) {
                    alert("Columns are not configured. Please first configure columns to display.");
                } else {
                    var leftPaneColumns = $.parseJSON(obj.LeftPaneFields);
                    var records = $.parseJSON(obj.Records);
                    var searchConditions = $.parseJSON($("#hdnHeaderSearch").val());
                    var noWrapColumns = [];
                    //var noWrapColumns = [3, 96, 101, 203, 233, 251, 273, 281, 294, 50928];
                    $("#hdnSortColumn").val(obj.SortColumn);
                    $("#hdnSortOrder").val(obj.SortOrder);
                    var data = "<table id='tblMain' class='table table-bordered table-striped'>";

                    //Header Row
                    data += "<tr>";

                    if (leftPaneColumns.length > 0) {
                        data += "<th style='width:25px;'><input type='checkbox' class='chkSelectAll' onChange='SelectAllChanged(this);' /></th>";
                    }

                    var i = 0;
                    arrFieldConfig = new Array();
                    leftPaneColumns.forEach(function (e) {
                        if (replaceNull(e.vcOrigDbColumnName) === "numQtyToShipReceive" && $("input[id$=hdnViewID]").val() === "3"){
                            e.vcFieldName = "Qty to Bill";
                        }

                        var fieldMessage = e.bitFieldMessage ? replaceNull(e.vcFieldMessage).replace("{$FieldName}", replaceNull(e.vcFieldName)) : replaceNull(e.vcFieldName).replace("'", "\'");
                        arrFieldConfig[i] = new InlineEditValidation(e.numFieldID, e.bitCustomField.toString(), e.bitIsRequired, e.bitIsNumeric, e.bitIsAlphaNumeric, e.bitIsEmail, e.bitIsLengthValidation, e.intMaxLength, e.intMinLength, replaceNull(fieldMessage));
                        i++;

                        if (e.bitAllowFiltering) {
                            if (e.bitAllowSorting) {
                                data += "<th id='" + e.ID + "' style='width:" + (e.intColumnWidth || 100) + "px;'><a href=\"javascript:SortData('" + (replaceNull(e.vcLookBackTableName) + "." + replaceNull(e.vcOrigDbColumnName)) + "');\">" + replaceNull(e.vcFieldName) + ((replaceNull(e.vcLookBackTableName) + "." + replaceNull(e.vcOrigDbColumnName)) == $("#hdnSortColumn").val() ? ($("#hdnSortOrder").val() == "DESC" ? "&nbsp;&nbsp;<i class='fa fa-caret-down'></i>" : "&nbsp;&nbsp;<i class='fa fa-caret-up'></i>") : "") + "</a><br />"
                            } else {
                                data += "<th id='" + e.ID + "' style='width:" + (e.intColumnWidth || 100) + "px;'>" + replaceNull(e.vcFieldName) + "<br />"
                            }
                            var controlID = "hs~" + replaceNull(e.vcLookBackTableName) + "~" + replaceNull(e.vcOrigDbColumnName) + "~" + replaceNull(e.vcAssociatedControlType) + "~" + (e.bitCustomField || "0") + "~" + (e.numListID || 0) + "~" + replaceNull(e.vcListItemType);
                            if (e.vcAssociatedControlType === "TextBox") {
                                var vcValue = "";

                                if (searchConditions.length > 0 && $.grep(searchConditions, function (f) { return f.id == controlID; }).length > 0) {
                                    vcValue = $.grep(searchConditions, function (f) { return f.id == controlID; })[0].searchValue;
                                }
                                data += "<input type='text' class='form-control' id='" + controlID + "' onkeydown='return SearchRecords(false);' value='" + vcValue + "' />"
                            } else if (e.vcAssociatedControlType === "SelectBox") {
                                var vcValue = "";

                                if (searchConditions.length > 0 && $.grep(searchConditions, function (f) { return f.id == controlID; }).length > 0) {
                                    vcValue = $.grep(searchConditions, function (f) { return f.id == controlID; })[0].searchValue;
                                }

                                data += "<select class='form-control' id='" + controlID + "' onChange='return SearchRecords(true);' selectedValue='" + vcValue + "'><option value='0'>-- Select One --</option></select>"
                            } else if (e.vcAssociatedControlType === "DateField") {
                                var vcValueFrom = "";
                                var vcValueTo = "";

                                if (searchConditions.length > 0 && $.grep(searchConditions, function (f) { return f.id == controlID; }).length > 0) {
                                    vcValueFrom = $.grep(searchConditions, function (f) { return f.id == controlID; })[0].searchValueFrom;
                                    vcValueTo = $.grep(searchConditions, function (f) { return f.id == controlID; })[0].searchValueTo;
                                }

                                data += "<ul class='list-inline'><li><input type='text' style='width:105px' class='form-control' id='" + controlID + "~from' onkeydown='return SearchRecords(false);' value='" + vcValueFrom + "' placeholder='mm/dd/yyyy' /></li><li><input type='text' style='width:105px' class='form-control' id='" + controlID + "~to' onkeydown='return SearchRecords(false);' value='" + vcValueTo + "' placeholder='mm/dd/yyyy' /></li></ul>"
                            }

                            data += "</th>";
                        } else {
                            data += "<th id='" + e.ID + "' style='width:" + (e.intColumnWidth || 100) + "px;'>" + replaceNull(e.vcFieldName) + "</th>";
                        }
                    });
                    data += "</tr>";

                    //Data Rows
                    records.forEach(function (n) {
                        data += "<tr>";
                        data += "<td style='white-space:nowrap'>";
                        data += "<input type='checkbox' class='chkSelect' onChange='RecordSelected(this);' />";
                        data += "<input type='hidden' id='hdnOppID' value='" + (n.numOppID || 0) + "' />";
                        data += "<input type='hidden' id='hdnWOID' value='" + (n.numWOID || 0) + "' />";
                        data += "<input type='hidden' id='hdnOppItemID' value='" + (n.numOppItemID || 0) + "' />";
                        data += "<input type='hidden' id='hdnRemainingQty' value='" + (n.numRemainingQty || 0) + "' />";;
                        data += "</td>";

                        leftPaneColumns.forEach(function (e) {
                            if (e.vcOrigDbColumnName === "numOnHand" || e.vcOrigDbColumnName === "numOnOrder" || e.vcOrigDbColumnName === "numAllocation" || e.vcOrigDbColumnName === "numBackOrder" || e.vcOrigDbColumnName === "numUnitHour" || e.vcOrigDbColumnName === "numUnitHourReceived" || e.vcOrigDbColumnName === "numRemainingQty") {
                                data += "<td>" + formatNumber((n[e.vcOrigDbColumnName] || 0)) + "</td>";
                            } else if (e.vcOrigDbColumnName === "numQtyToShipReceive" && ((n.numOppItemID || 0) > 0 || (n.numWOID) > 0) && ($("input[id$=hdnViewID]").val() === "1" || $("input[id$=hdnViewID]").val() === "2" || $("input[id$=hdnViewID]").val() === "3")) {
                                data += "<td" + ($.inArray(e.numFieldID, noWrapColumns) > -1 ? " class='nowrap'" : "") + "><input type='text' class='txtQtyToReceive form-control' value='" + formatNumber((n.numRemainingQty || 0)) + "'/></td>";
                            } else if (e.vcOrigDbColumnName === "SerialLotNo" && (n["bitSerialized"] || n["bitLotNo"])) {
                                data += "<td" + ($.inArray(e.numFieldID, noWrapColumns) > -1 ? " class='nowrap'" : "") + "><input type='text' id='txtSerialLot' class='form-control' value='" + replaceNull(n[e.vcOrigDbColumnName]) + "'/></td>";
                            } else if (e.vcOrigDbColumnName === "vcPoppName" && (n.numOppID || 0) > 0) {
                                data += "<td" + ($.inArray(e.numFieldID, noWrapColumns) > -1 ? " class='nowrap'" : "") + "><a href='../opportunity/frmOpportunities.aspx?opId=" + n.numOppID.toString() + "'>" + replaceNull(n[e.vcOrigDbColumnName]) + "</a></td>";
                            } else if (e.vcOrigDbColumnName === "vcPoppName" && (n.numWOID || 0) > 0) {
                                data += "<td" + ($.inArray(e.numFieldID, noWrapColumns) > -1 ? " class='nowrap'" : "") + "><a href='../items/frmWorkOrder.aspx?WOID=" + n.numWOID.toString() + "'>" + replaceNull(n[e.vcOrigDbColumnName]) + "</a></td>";
                            } else if(e.vcOrigDbColumnName === "vcRefOrderNo"){
                                data += "<td><input type='text' id='txtInvoiceNo' class='form-control' /></td>";
                            } else if (e.vcOrigDbColumnName === "dtFromDate") {
                                data += "<td><input type='text' class='txtBillingDate form-control' /></td>";
                            } else if (e.bitCustomField) {
                                var controlClass = "";
                                if (e.vcAssociatedControlType === "TextBox" || e.vcAssociatedControlType === "Website" || e.vcAssociatedControlType === "Email") {
                                    controlClass = "click";
                                } else if (e.vcAssociatedControlType === "SelectBox") {
                                    controlClass = "editable_select";
                                } else if (e.vcAssociatedControlType === "DateField") {
                                    controlClass = "editable_DateField";
                                } else if (e.vcAssociatedControlType === "TextArea") {
                                    controlClass = "editable_textarea";
                                } else if (e.vcAssociatedControlType === "CheckBox") {
                                    controlClass = "editable_CheckBox";
                                }

                                data += "<td id='Opp~" + e.numFieldID + "~" + e.bitCustomField + "~" + (n.numOppID || 0) + "~" + (n.numTerID || 0) + "~0~" + (n.numDivisionID || 0) + "'" + ($.inArray(e.numFieldID, noWrapColumns) > -1 ? " class='" + controlClass + " nowrap'" : " class='" + controlClass + "'") + ">" + replaceNull(n["Cust" + e.numFieldID]) + "</td>";
                            } else {
                                if (e.vcOrigDbColumnName === "monPrice" && parseInt($("input[id$=hdnViewID]").val()) == 3 && (n.numOppItemID || 0) > 0) {
                                    data += "<td id='oppitems~" + e.numFieldID + "~" + e.bitCustomField + "~" + (n.numOppItemID || 0) + "~" + (n.numTerID || 0) + "~0~" + (n.numDivisionID || 0) + "'" + ($.inArray(e.numFieldID, noWrapColumns) > -1 ? " class='nowrap editable_textarea'" : " class='editable_textarea'") + ">" + replaceNull(n[e.vcOrigDbColumnName]) + "</td>";
                                } else {
                                    data += "<td" + ($.inArray(e.numFieldID, noWrapColumns) > -1 ? " class='nowrap'" : "") + ">" + replaceNull(n[e.vcOrigDbColumnName]) + "</td>";
                                }
                            }
                        });
                        data += "</tr>";
                    });


                    data += "</table>";

                    $("#divMainGrid").html(data);

                    $("[id$='~from']").datepicker({
                        dateFormat: 'mm/dd/yy', onSelect: function (dateText, inst) {
                            LoadRecordsWithPagination();
                        }
                    });
                    $("[id$='~to']").datepicker({
                        dateFormat: 'mm/dd/yy', onSelect: function (dateText, inst) {
                            LoadRecordsWithPagination();
                        }
                    });

                    $(".txtBillingDate").datepicker({
                        dateFormat: 'mm/dd/yy'
                    });

                    $("#tblMain tr:first select").each(function (index, select) {
                        $.ajax({
                            type: "POST",
                            url: '../Common/Common.asmx/GetAdvancedSearchListDetails',
                            contentType: "application/json",
                            dataType: "json",
                            data: JSON.stringify({
                                "numListID": ($(select).attr("id").split("~")[5] || 0),
                                "vcListItemType": replaceNull($(select).attr("id").split("~")[6]),
                                "searchText": "",
                                "pageIndex": 1,
                                "pageSize": 1000000
                            }),
                            success: function (data) {
                                try {
                                    var listItems;

                                    if (data.hasOwnProperty("d")) {
                                        if (IsJsonString(data.d)) {
                                            data = $.parseJSON(data.d)
                                        } else {
                                            data = null;
                                        }
                                    }
                                    else {
                                        if (IsJsonString(data.d)) {
                                            data = $.parseJSON(data.d)
                                        } else {
                                            data = null;
                                        }
                                    }

                                    if (data != null) {
                                        listItems = $.parseJSON(data.results || null);

                                        if (listItems != null) {
                                            listItems.forEach(function (li) {
                                                $(select).append("<option value='" + li.id + "' " + (li.id.toString() === $(select).attr("selectedValue") ? "selected" : "") + ">" + replaceNull(li.text) + "</option>");
                                            });
                                        }
                                    }
                                } catch (err) {

                                }
                            }
                        });
                    });

                    BindInlineEdit();

                    $("table[id$=tblMain]").colResizable({
                        //fixed: false,
                        liveDrag: true,
                        gripInnerHtml: "<div class='grip2'></div>",
                        draggingClass: "dragging",
                        minWidth: 30,
                        resizeMode: 'overflow',
                        onResize: onGridColumnResized,
                        postbackSafe: true,
                        partialRefresh: true
                    });

                    totalRecords = obj.TotalRecords;
                }
            } catch (err) {
                alert("Unknown error occurred.");
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            if (IsJsonString(jqXHR.responseText)) {
                var objError = $.parseJSON(jqXHR.responseText)

                if (objError.ErrorMessage === "DEFAULT_SHIPPING_BIZDOC_NOT_CONFIGURED") {
                    alert("Go global settings -> Accounting and Select Sales BizDoc responsible for Shipping Labels & Trackings #.");
                } else {
                    alert("Error occurred: " + replaceNull(objError.ErrorMessage));
                }
            } else {
                alert("Unknown error ocurred");
            }
        }
    });
}

function GetSelectedOrderStatus() {
    try {
        var selectedOrderStatus = "";

        $("#divOrderStatus input[type='checkbox']").each(function () {
            if ($(this).is(":checked")) {
                selectedOrderStatus = selectedOrderStatus + (selectedOrderStatus.length > 0, ",", "") + $(this).attr("id").replace("chkOrderStatus", "")
            }
        });

        return selectedOrderStatus;
    } catch (e) {
        alert("Unknown error occurred.");
    }
}

function GetHeaderSearchString() {
    try {
        var searchConditions = [];
        var searchControls = $("#divMainGrid table tr th input[type='text'], #divMainGrid table tr th select")

        searchControls.each(function (index, e) {
            if ($(e).attr("id") != null && $(e).val() != "") {
                if ($(e).attr("id").split("~")[3] === "DateField") {
                    var result = $.grep(searchConditions, function (f) { return f.id == $(e).attr("id").replace("~from", "").replace("~to", ""); });
                    if (result.length == 0) {
                        searchConditions.push({ "id": $(e).attr("id").replace("~from", "").replace("~to", ""), "controlType": $(e).attr("id").split("~")[3], "searchField": ($(e).attr("id").split("~")[1] + "." + $(e).attr("id").split("~")[2]), "searchValue": $(e).val(), "searchValueFrom": $(e).attr("id").endsWith("~from") ? $(e).val() : "", "searchValueTo": $(e).attr("id").endsWith("~to") ? $(e).val() : "" });
                    } else {
                        if ($(e).attr("id").endsWith("~from")) {
                            result[0].searchValueFrom = $(e).val();
                        } else if ($(e).attr("id").endsWith("~to")) {
                            result[0].searchValueTo = $(e).val();
                        }
                    }
                } else {
                    searchConditions.push({ "id": $(e).attr("id"), "controlType": $(e).attr("id").split("~")[3], "searchField": ($(e).attr("id").split("~")[1] + "." + $(e).attr("id").split("~")[2]), "searchValue": $(e).val(), "searchValueFrom": "", "searchValueTo": "" });
                }
            }
        });

        $("#hdnHeaderSearch").val(JSON.stringify(searchConditions))

        var searchValue = "";
        if (searchConditions.length > 0) {
            searchConditions.forEach(function (e) {
                if (e.controlType === "TextBox") {
                    searchValue += (" AND " + e.searchField + " ilike '%" + e.searchValue + "%'")
                } else if (e.controlType === "SelectBox" && parseInt(e.searchValue) > 0) {
                    searchValue += (" AND " + e.searchField + " = " + e.searchValue)
                } else if (e.controlType === "DateField") {
                    if (e.searchValueFrom != "" && e.searchValueTo != "") {
                        searchValue += (" AND " + e.searchField + " BETWEEN '" + e.searchValueFrom + "' AND '" + e.searchValueTo + "'")
                    } else if (e.searchValueFrom != "") {
                        searchValue += (" AND " + e.searchField + " >= '" + e.searchValueFrom + "'")
                    } else if (e.searchValueTo != "") {
                        searchValue += (" AND " + e.searchField + " <= '" + e.searchValueTo + "'")
                    }
                }
            });
        }

        return searchValue;
    } catch (e) {
        alert("Unknown error occurred.");
    }
}

function SearchRecords(isSelectChange) {
    try {
        if (event.keyCode == 13 || isSelectChange) {
            LoadRecordsWithPagination();
            return false;
        } else {
            return true;
        }
    } catch (e) {
        alert("Unknown error occurred.");
    }
}

function SelectAllChanged(chkSelectAll) {
    $(".chkSelect").prop("checked", $(chkSelectAll).is(":checked"));

    if ($("input[id$=hdnViewID]").val() === "1" || $("input[id$=hdnViewID]").val() === "2") {
        try {
            if ($("input[id$=hdnViewID]").val() === "1") {
                LoadSelectedRecordsToRightPaneForPutAway(1);
            } else if ($("input[id$=hdnViewID]").val() === "2") {
                LoadSelectedRecordsToRightPaneForPutAway(3);
            }
        }
        catch (e) {
            alert("Unknown error occurred while loading selecting records for put-away.");
        }
    }
}

function RecordSelected(chkSelect) {
    try {
        if ($("input[id$=hdnViewID]").val() === "1" || $("input[id$=hdnViewID]").val() === "2") {
            if ($("input[id$=hdnViewID]").val() === "1") {
                LoadSelectedRecordsToRightPaneForPutAway(1);
            } else if ($("input[id$=hdnViewID]").val() === "2") {
                LoadSelectedRecordsToRightPaneForPutAway(3);
            }
        }
    }
    catch (e) {
        alert("Unknown error occurred while loading selecting records for put-away.");
    }
}

var onGridColumnResized = function (e) {
    var columns = $(e.currentTarget).find("th");
    var msg = "";
    columns.each(function () {
        var thId = $(this).attr("id");
        var thWidth = $(this).width();
        msg += (thId || "anchor") + ':' + thWidth + ';';
    }
    )

    $.ajax({
        type: "POST",
        url: "../common/Common.asmx/SaveAdvanceSearchGridColumnWidth",
        data: "{str:'" + msg + "', viewID:'" + parseInt($("input[id$=hdnViewID]").val()) + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json"
    });
};

var onGridColumnResizedRight = function (e) {
    var columns = $(e.currentTarget).find("th");
    var msg = "";
    columns.each(function () {
        var thId = $(this).attr("id");
        var thWidth = $(this).width();
        msg += (thId || "anchor") + ':' + thWidth + ';';
    }
    )

    $.ajax({
        type: "POST",
        url: "../common/Common.asmx/SaveGridColumnWidth",
        data: "{str:'" + msg + "', viewID:'" + parseInt($("input[id$=hdnViewID]").val()) + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json"
    });
};

function LoadSelectedRecordsToRightPaneForPutAway(mode) {
    try {
        $("#tblRight").html("");
        var arrSelectedRecords = [];
        if ($("#hdnSelectedRecordsForPutAway").val() != "") {
            arrSelectedRecords = JSON.parse($("#hdnSelectedRecordsForPutAway").val());
        }

        $("#divMainGrid").find(".chkSelect").each(function (e, chkSelect) {
            var obj = {};
            obj.OppID = parseInt($(chkSelect).closest("tr").find("#hdnOppID").val());
            obj.OppItemID = parseInt($(chkSelect).closest("tr").find("#hdnOppItemID").val());
            obj.WOID = parseInt($(chkSelect).closest("tr").find("#hdnWOID").val());

            if ($(this).is(":checked")) {
                var result = $.grep(arrSelectedRecords, function(e){ return e.OppID == obj.OppID && e.OppItemID == obj.OppItemID && e.WOID == obj.WOID; });

                if (result.length === 0) {
                    arrSelectedRecords.push(obj);
                }
            } else {
                arrSelectedRecords = arrSelectedRecords.filter(function (e, index, arr) {
                    return !(e.OppID == obj.OppID && e.OppItemID == obj.OppItemID && e.WOID == obj.WOID);
                });
            }
        });

        $("#hdnSelectedRecordsForPutAway").val(JSON.stringify(arrSelectedRecords));

        if(arrSelectedRecords.length > 0){
            $.ajax({
                type: "POST",
                url: '../opportunity/MassPurchaseFulfillmentService.svc/GetRecordsForPutAway',
                contentType: "application/json",
                dataType: "json",
                data: JSON.stringify({
                    "mode": mode
                    ,"selectedRecords": JSON.stringify(arrSelectedRecords)
                }),
                success: function (data) {
                    try {
                        var obj = $.parseJSON(data.GetRecordsForPutAwayResult);
                        if (obj.RightPaneFields.length === 0) {
                            alert("Columns are not configured forput-away right pane. Please first configure columns to display.");
                        } else {
                            var rightPaneColumns = $.parseJSON(obj.RightPaneFields);
                            var records = $.parseJSON(obj.Records);

                            //Header Row
                            data += "<thead><tr>";
                            rightPaneColumns.forEach(function (e) {
                                data += "<th id='" + e.ID + "' style='width:" + (e.intColumnWidth || 100) + "px;'>" + (e.vcOrigDbColumnName === "numQtyToShipReceive" ? "<input type='checkbox' id='chkPutAwaykAll' onChange='return PutAwayAllChanged(this);'>" : "") + " " + replaceNull(e.vcFieldName) + "</th>";
                            });
                            data += "</tr></thead><tbody>";

                            //Data Rows
                            if (records.length > 0) {
                                records.forEach(function (n) {
                                    data += "<tr>";
                                    var i = 1;
                                    rightPaneColumns.forEach(function (e) {
                                        if (i === 1) {
                                            data += "<td>";
                                            data += "<input type='hidden' id='hdnOppID' value='" + replaceNull(n.numOppID) + "' />"
                                            data += "<input type='hidden' id='hdnWOID' value='" + replaceNull(n.numWOID) + "' />"
                                            data += "<input type='hidden' id='hdnOppItemID' value='" + replaceNull(n.numOppItemID) + "' />";
                                            data += "<input type='hidden' id='hdnDivisionID' value='" + (n.numDivisionID || 0) + "' />";
                                            data += "<input type='hidden' id='hdnItemCode' value='" + replaceNull(n.numItemCode) + "' />";
                                            data += "<input type='hidden' id='hdnItemImage' value='" + replaceNull(n.vcPathForTImage) + "' />";
                                            data += "<input type='hidden' id='hdnSKU' value='" + replaceNull(n.vcSKU) + "' />";
                                            data += "<input type='hidden' id='hdnUPC' value='" + replaceNull(n.numBarCodeId) + "' />";
                                            data += "<input type='hidden' id='hdnItemName' value='" + replaceNull(n.vcItemName) + "' />";
                                            data += "<input type='hidden' id='hdnAttributes' value='" + replaceNull(n.vcAttributes) + "' />";
                                            data += "<input type='hidden' id='hdnRemainingQty' value='" + replaceNull(n.numRemainingQty) + "' />";
                                            data += "<input type='hidden' id='hdnWarehouseItemID' value='" + (n.numWarehouseItemID || 0) + "' />";
                                            data += "<input type='hidden' id='hdnOppName' value='" + replaceNull(n.vcPoppName) + "' />";
                                            data += "<input type='hidden' id='hdnItemName' value='" + replaceNull(n.vcItemName) + "' />";
                                            data += "<input type='hidden' id='hdnIsStockTransfer' value='" + (n.bitStockTransfer || false) + "' />";
                                            data += "<input type='hidden' id='hdnIsSerial' value='" + (n.bitSerialized || false) + "' />";
                                            data += "<input type='hidden' id='hdnIsLot' value='" + (n.bitLotNo || false) + "' />";
                                            data += "<input type='hidden' id='hdnPPVariance' value='" + (n.bitPPVariance || false) + "' />";
                                            data += "<input type='hidden' id='hdnCurrencyID' value='" + (n.numCurrencyID || 0) + "' />";
                                            data += "<input type='hidden' id='hdnExchangeRate' value='" + (n.fltExchangeRate || 0) + "' />";
                                            data += "<input type='hidden' id='hdncharItemType' value='" + replaceNull(n.charItemType) + "' />";
                                            data += "<input type='hidden' id='hdnItemType' value='" + replaceNull(n.vcItemType) + "' />";
                                            data += "<input type='hidden' id='hdnPrice' value='" + (n.monPrice || 0) + "' />";
                                            data += "<input type='hidden' id='hdnDropShip' value='" + (n.bitDropShip || false) + "' />";
                                            data += "<input type='hidden' id='hdnProjectID' value='" + (n.numProjectID || 0) + "' />";
                                            data += "<input type='hidden' id='hdnClassID' value='" + (n.numClassID || 0) + "' />";
                                            data += "<input type='hidden' id='hdnAssetChartAcntId' value='" + (n.numAssetChartAcntId || 0) + "' />";
                                            data += "<input type='hidden' id='hdnCOGsChartAcntId' value='" + (n.numCOGsChartAcntId || 0) + "' />";
                                            data += "<input type='hidden' id='hdnIncomeChartAcntId' value='" + (n.numIncomeChartAcntId || 0) + "' />";
                                        } else {
                                            data += "<td>";
                                        }
                                        i = i + 1;

                                        if (e.vcOrigDbColumnName === "vcPathForTImage") {
                                            if (n[e.vcOrigDbColumnName] != null && n[e.vcOrigDbColumnName].indexOf('.') > -1) {
                                                data += "<img height='45' class='img-responsive' src='" + $("[id$=hdnMSImagePath]").val() + n[e.vcOrigDbColumnName] + "' alt='' /></td>"
                                            } else {
                                                data += "<img height='45' class='img-responsive' src='../images/icons/cart_large.png' alt='' /></td>"
                                            }

                                        } else if (e.vcOrigDbColumnName === "numQtyToShipReceive") {
                                            var html = "<table class='tblPutAwayLocation' style='width: 100%;white-space:nowrap;'>";
                                            JSON.parse(n.vcWarehouses.toString()).forEach(function (objLocation) {
                                                html += "<tr>";
                                                html += "<td>" + replaceNull(objLocation.Location) + " <input type='hidden' class='hdnWarehouseLocationID' value='0'/><input type='hidden' class='hdnWarehouseItemID' value='" + objLocation.WarehouseItemID + "'/></td>";
                                                html += "<td><input type='text' class='txtPutAway form-control' onfocus='FocusRow(this)' value='" + formatNumber(n[e.vcOrigDbColumnName] || 0) + "' /></td>";
                                                html += "</tr>";
                                            });
                                            html += "</table>";
                                            data += html;
                                            data += "</td>";
                                        } else if (e.vcOrigDbColumnName === "vcLocation") {
                                            data += "</td>";
                                        } else if (e.vcOrigDbColumnName === "SerialLotNo" && (n["bitSerialized"] || n["bitLotNo"])) {
                                            data += "</td>";
                                        } else if (e.vcOrigDbColumnName === "numRemainingQty") {
                                            data += "<span id='lblRemaining' class='badge bg-red'>" + formatNumber((n[e.vcOrigDbColumnName] || 0)) + "</span>";
                                        } else {
                                            data += replaceNull(n[e.vcOrigDbColumnName]) + "</td>";
                                        }
                                    });
                                    data += "</tr>";
                                });
                            } else {
                                if ($("#tblRight > tbody > tr").not(":first").length === 0) {
                                    data += "<tr><td colspan='" + rightPaneColumns.length + "' class='text-center'>No records found</td></tr>";
                                }
                            }

                            $("#tblRight").append(data + "</tbody>");

                            PutAwatKeyDown();

                            $("table[id$=tblRight]").colResizable({
                                //fixed: false,
                                liveDrag: true,
                                gripInnerHtml: "<div class='grip2'></div>",
                                draggingClass: "dragging",
                                minWidth: 30,
                                resizeMode: 'overflow',
                                onResize: onGridColumnResizedRight,
                                postbackSafe: true,
                                partialRefresh: true
                            });
                        }
                    } catch (err) {
                        alert("Unknown error occurred.");
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    if (IsJsonString(jqXHR.responseText)) {
                        var objError = $.parseJSON(jqXHR.responseText)
                        alert("Error occurred: " + replaceNull(objError.ErrorMessage));
                    } else {
                        alert("Unknown error ocurred");
                    }
                }
            });
        }
    } catch (e) {
        alert("Unknown error occurred while loading selecting records for picking");
    }

    $("#txtScan").focus();
}

function PutAwatKeyDown(){
    $("#tblRight input.txtPutAway").keydown(function (e) {
        if (e.keyCode == 13 || e.keyCode == 9) {
            e.preventDefault();

            if ($(this).val() === "") {
                $(this).val("0");
            }

            var row = $(this).closest("table.tblPutAwayLocation").closest("tr")[0];
            var remainingQty = parseFloat($(row).find("#hdnRemainingQty").val())
            var qtyToPutAway = 0;
                                    
            $(this).closest("table.tblPutAwayLocation").find("tr").each(function(){
                if(JSON.parse($(row).find("#hdnIsSerial").val()) || JSON.parse($(row).find("#hdnIsLot").val())){
                    qtyToPutAway += GetSerialLotQty($(this).find(".txtPutAway"),JSON.parse($(row).find("#hdnIsSerial").val()),JSON.parse($(row).find("#hdnIsLot").val()))
                } else {
                    if(parseFloat($(this).find(".txtPutAway").val().replace(/,/g, '')) > 0){
                        qtyToPutAway +=  parseFloat($(this).find(".txtPutAway").val().replace(/,/g, ''));
                    }
                }
            });

            if (qtyToPutAway > parseFloat($(this).closest("table.tblPutAwayLocation").closest("tr").find("#hdnRemainingQty").val())) {
                alert('Put-Away qty is more than remaining quantity.');

                if(JSON.parse($(row).find("#hdnIsSerial").val()) || JSON.parse($(row).find("#hdnIsLot").val())){
                    remainingQty = remainingQty - 0;
                } else {
                    remainingQty = remainingQty - parseFloat($(this).val().replace(/,/g, ''));
                    $(this).val("0");
                }
            } else {
                if($(this).closest("tr").next("tr").length > 0)
                {
                    var tmpStr = $(this).closest("tr").next("tr").find("input.txtPutAway").val();
                    $(this).closest("tr").next("tr").find("input.txtPutAway").val('');
                    $(this).closest("tr").next("tr").find("input.txtPutAway").val(tmpStr);
                }
                else 
                {
                    if ($(row).next('tr').length > 0) {
                        var searchInput;
                        var searchInput =  $(row).next('tr').find("input.txtPutAway")[0];
                                        
                        var strLength = $(searchInput).val().length * 2;

                        $(searchInput).focus();
                        searchInput.setSelectionRange(strLength, strLength);
                    }
                    else {
                        if ($("#tblRight > tbody > tr").length > 0) {
                            var searchInput;

                            if($("#tblRight > tbody > tr:first-child").find("input.txtPutAway").length > 0){
                                var searchInput =  $("#tblRight > tbody > tr:first-child").find("input.txtPutAway")[0];
                                var strLength = $(searchInput).val().length * 2;

                                $(searchInput).focus();
                                searchInput.setSelectionRange(strLength, strLength);
                            } 
                        }
                    }
                }
                                        
                remainingQty = parseFloat($(row).find("#hdnRemainingQty").val()) - qtyToPutAway;
            }

                                   
            $(row).find("#lblRemaining").text(formatNumber(remainingQty));

            if (remainingQty === 0) {
                $(row).find("#lblRemaining").removeClass("bg-red").addClass("bg-green");
                $(row).addClass("putAwayItem");

                animateCSS(row, "slideOutDown", function () {
                    $(row).appendTo("#tblRight");
                    $(row).removeClass("focusedRow");
                });
            } else {
                $(row).find("#lblRemaining").removeClass("bg-green").addClass("bg-red");
                $(row).removeClass("putAwayItem");
                $(row).prependTo("#tblRight > tbody");
            }

            return false;
        } else { 
            var row = $(this).closest("table.tblPutAwayLocation").closest("tr")[0];
            if(!(JSON.parse($(row).find("#hdnIsSerial").val()) || JSON.parse($(row).find("#hdnIsLot").val()))){
                if (e.keyCode != 8 && e.keyCode != 0 && e.keyCode != 190 && e.keyCode != 37 && e.keyCode != 38 && e.keyCode != 39 && e.keyCode != 40 && e.keyCode != 46 && (e.keyCode < 48 || e.keyCode > 57) && !(e.keyCode >=96 && e.keyCode <= 105)) {
                    e.preventDefault();
                }
            } 
        }
    });
}

function PutAwayAllChanged(chkPutAwaykAll) {
    $("#tblRight > tbody > tr").each(function(index,tr){
        var remainingQty = parseFloat($(tr).find("#hdnRemainingQty").val());

        $(this).find("table.tblPutAwayLocation").find("tr").each(function() {
            if($(chkPutAwaykAll).is(":checked")){
                $(this).find(".txtPutAway").val(formatNumber(remainingQty));
                remainingQty = 0;
                $(tr).find("#lblRemaining").text("0");
                $(tr).find("#lblRemaining").removeClass("bg-red").addClass("bg-green");
            } else {
                $(tr).find("#lblRemaining").text(formatNumber(remainingQty + parseFloat($(this).find(".txtPutAway").val().replace(/,/g, ''))));
                $(this).find(".txtPutAway").val("0");
                $(tr).find("#lblRemaining").removeClass("bg-green").addClass("bg-red");
            }
                
        });
    });
}

function ItemPutAway(txtScan) {
    try {
        if (event.keyCode == 13) {
            var valueToCompre = "";

            var foundRows = $('#tblRight > tbody > tr').not(":first").filter(function () {
                if ($("#hdnScanField").val() === "2") {
                    valueToCompre = $(this).find("#hdnUPC").val().trim();
                } else if ($("#hdnScanField").val() === "3") {
                    valueToCompre = $(this).find("#hdnItemName").val().trim();
                } else {
                    valueToCompre = $(this).find("#hdnSKU").val().trim();
                }

                return $(txtScan).val().trim().toLowerCase() === valueToCompre.toLowerCase() && parseFloat($(this).find("#lblRemaining").text().replace(",", "")) > 0;
            });

            if (foundRows.length > 0) {
                var firtRow = foundRows[0];

                $('#tblRight > tbody > tr').removeClass("focusedRow");
                $(firtRow).prependTo("#tblRight > tbody");
                $(firtRow).addClass("focusedRow");

                if($(firtRow).find("table.tblPutAwayLocation").length > 0){
                    $(firtRow).find("table.tblPutAwayLocation").find("tr").each(function(){
                        $(this).find(".txtPutAway").val(formatNumber(parseFloat($(this).find(".txtPutAway").val().replace(/,/g, '')) + 1));
                        var remainingQty = parseFloat($(firtRow).find("#lblRemaining").text().replace(/,/g, '')) - 1;
                        $(firtRow).find("#lblRemaining").text(formatNumber(remainingQty));
                        return false;
                    });
                }

                if (parseFloat($(firtRow).find("#lblRemaining").text().replace(/,/g, '')) === 0) {
                    $(firtRow).find("#lblRemaining").removeClass("bg-red").addClass("bg-green");
                }

                if (remainingQty === 0) {
                    $(firtRow).addClass("putAwayItem");

                    animateCSS(firtRow, "slideOutDown", function () {
                        $(firtRow).appendTo("#tblRight");
                        $(firtRow).removeClass("focusedRow");
                    });
                }
            }

            $(txtScan).val("");
            $(txtScan).focus();
            return false;
        } else {
            return true;
        }
    } catch (e) {
        alert("Unknown error occurred.");
    }

    return false;
}

function FocusRow(txtPutAway) {
    $('#tblRight > tbody > tr').removeClass("focusedRow");
    $(txtPutAway).closest("table.tblPutAwayLocation").closest("tr").addClass("focusedRow");
    $(txtPutAway).val($(txtPutAway).val());
}

function ClearRightPaneSelection() {
    $("#tblRight").html("");
    $('.location-dropdown').select2('data', null);
    $("#hdnSelectedRecordsForPutAway").val("");
}

function animateCSS(element, animationName, callback) {
    const node = element;
    node.classList.add('animated', 'slower', animationName)

    function handleAnimationEnd() {
        node.classList.remove('animated', 'slower', animationName)
        node.removeEventListener('animationend', handleAnimationEnd)

        if (typeof callback === 'function') callback()
    }

    node.addEventListener('animationend', handleAnimationEnd)
}

function PutAwayViewChanged(mode) {
    $("#divMobilePutAway .nav li").removeClass("active");
    $("#liPutAwayView" + mode).addClass("active");

    if (mode === 1) {
        $("#ms-main-grid").show();
        $("#ms-selected-grid").hide();
    } else {
        $("#ms-main-grid").hide();
        $("#ms-selected-grid").show();
    } 
}

function formatNumber(n) {
    return n.toLocaleString("en-US");
    //return n.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")

    //sep = ",";
    //if(n % 1 != 0){
    //    decimals = parseInt($("[id$=hdnDecimalPoints]").val()) || 2; // Default to 2 decimals
    //} else {
    //    decimals = 0;
    //}

    //return n.toLocaleString().split(sep)[0]
    //    + sep
    //    + n.toFixed(decimals).split(sep)[1];
}

function SortData(sortColumn) {
    try {
        if ($("#hdnSortColumn").val() === sortColumn) {
            if ($("#hdnSortOrder").val() === "DESC") {
                $("#hdnSortOrder").val("ASC");
            } else {
                $("#hdnSortOrder").val("DESC");
            }
        } else {
            $("#hdnSortColumn").val(sortColumn);
            $("#hdnSortOrder").val("ASC");
        }

        LoadRecordsWithPagination();
    } catch (e) {
        alert("Unknown error occurred.");
    }
}

function GetSerialLotQty(txtPutAway,IsSerial,IsLot){
    if(IsSerial){
        return ($(txtPutAway).val().trim() == "" || $(txtPutAway).val().trim() == "0") ? 0 : parseInt($(txtPutAway).val().split(",").length);
    } else if(IsLot){
        if($(txtPutAway).val().trim() == ""){
            return 0;
        } else {
            var qty = 0;
            var arrLot = $(txtPutAway).val().trim().replace(/,/g, '').split(",");
            $.each( arrLot, function( index, value ) {
                var startIndex = value.indexOf("(");
                var endIndex = value.indexOf(")");

                if(startIndex != -1 && endIndex != -1 && endIndex > (startIndex + 1)){
                    qty += parseFloat(value.substring(startIndex + 1,endIndex));
                }
            });

            return qty;
        }
    }
}

function IsValidSerialLot(txtPutAway){
    var isValidSerialLot = true;

    if ($(txtPutAway).val().trim() != "0") {
        var arrLot = $(txtPutAway).val().trim().replace(/,/g, '').split(",");
        $.each(arrLot, function (index, value) {
            var startIndex = value.indexOf("(");
            var endIndex = value.indexOf(")");

            if (!(startIndex != -1 && endIndex != -1 && endIndex > (startIndex + 1))) {
                isValidSerialLot = false
            }
        });
    }

    return isValidSerialLot;
}


