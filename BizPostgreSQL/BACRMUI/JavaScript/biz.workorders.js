﻿$(document).ready(function () {
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    prm.add_pageLoaded(pageLoadedWorkOrderJS);
});

function pageLoadedWorkOrderJS() {
    $("label.taskTimerInitial").each(function () {
        var currentTime = new Date();
        var divTaskControlsWorkOrder = $(this).closest("div[id='divTaskControlsWorkOrder']");


        if (divTaskControlsWorkOrder != null) {
            ActivateTimerWorkOrder(currentTime, divTaskControlsWorkOrder, true);
        }
    });

    $('#divTaskLogWorkOrder').on('hidden.bs.modal', function () {
        document.location.href = document.location.href;
    })
}

function TaskStartedWorkOrder(btn, taskID, isResumed) {
    var currentTime = new Date();
    var divTaskControlsWorkOrder = $(btn).closest("div[id='divTaskControlsWorkOrder']");

    if (divTaskControlsWorkOrder != null) {
        var a1 = SaveTimeEntryWorkOrder(0, taskID, (isResumed ? 3 : 1), new Date(), 0, 0, "", false);

        $.when(a1).then(function (data) {
            var html = "<ul class='list-inline' style='margin-bottom:0px;'>";
            html += "<li style='vertical-align:middle;padding-left:0px;padding-right:0px;'><button class='btn btn-xs btn-info' onclick='return ShowTaskTimeLogWindowWorkOrder(" + taskID.toString() + ");'><i class='fa fa-clock-o'></i></button></li>";
            html += "<li style='vertical-align:middle;padding-right:0px;'><button class='btn btn-xs btn-flat btn-warning' onclick='return ShowTaskPausedWindowWorkOrder(this," + taskID.toString() + ");'>Pause</button></li>";
            html += "<li style='vertical-align:middle;padding-right:0px;'><button class='btn btn-flat btn-task-finish' onclick='return TaskFinishedWorkOrder(this," + taskID.toString() + ",4);'><img src='../images/comflag.png' />&nbsp;Finish</button></li>";
            html += "<li style='vertical-align:middle;padding-right:0px;'><input type='text' onkeydown='return ProcessedUnitsChangedWorkOrder(this," + taskID.toString() + ",event);' class='form-control txtUnitsProcessed' style='width:50px;padding:1px 2px 1px 2px;height:23px;' /></li>";
            html += "<li style='vertical-align:middle;padding-right:0px;'><label class='taskTimer'></label></li>";
            html += "</ul>";

            $(divTaskControlsWorkOrder).html(html);
            ActivateTimerWorkOrder(currentTime, divTaskControlsWorkOrder, false);
            $(divTaskControlsWorkOrder).find(".txtUnitsProcessed").focus();
        }, function (jqXHR, textStatus, errorThrown) {
            if (IsJsonString(jqXHR.responseText)) {
                var objError = $.parseJSON(jqXHR.responseText)
                if (objError.Message != null) {
                    alert("Error occurred: " + objError.Message);
                } else {
                    alert(objError);
                }
            } else {
                alert("Unknown error ocurred");
            }
        });
    }

    return false;
}

function ActivateTimerWorkOrder(fromTime, divTaskControlsWorkOrder, isInitialLoad) {
    try {
        if (divTaskControlsWorkOrder != null) {
            var estimatedTimeInMinutes = 0;
            var timeSpentInMinutes = 0;

            if ($(divTaskControlsWorkOrder).closest("tr").find("input.hdnLastStartDate") != null && $(divTaskControlsWorkOrder).closest("tr").find("input.hdnLastStartDate").val() != "") {
                fromTime = new Date($(divTaskControlsWorkOrder).closest("tr").find("input.hdnLastStartDate").val());
            }

            if ($(divTaskControlsWorkOrder).closest("tr").find("input.hdnTaskEstimationInMinutes") != null) {
                estimatedTimeInMinutes = parseInt($(divTaskControlsWorkOrder).closest("tr").find("input.hdnTaskEstimationInMinutes").val() || 0);
            }

            if ($(divTaskControlsWorkOrder).closest("tr").find("input.hdnTimeSpentInMinutes") != null) {
                timeSpentInMinutes = parseInt($(divTaskControlsWorkOrder).closest("tr").find("input.hdnTimeSpentInMinutes").val() || 0);
            }


            if (estimatedTimeInMinutes > 0) {
                var countDownDate = fromTime.getTime();

                // Update the count down every 1 second
                var x = setInterval(function () {

                    // Get today's date and time
                    var now = new Date().getTime();

                    // Find the distance between now and the count down date
                    var distance = (now - countDownDate) + (timeSpentInMinutes * 60000);

                    // Time calculations for days, hours, minutes and seconds
                    var days = Math.floor(distance / (1000 * 60 * 60 * 24));
                    var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
                    var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
                    var seconds = Math.floor((distance % (1000 * 60)) / 1000);

                    if ($(divTaskControlsWorkOrder).find("label." + (isInitialLoad ? "taskTimerInitial" : "taskTimer")) != null) {
                        $(divTaskControlsWorkOrder).find("label." + (isInitialLoad ? "taskTimerInitial" : "taskTimer")).text((days > 0 ? (days + "d ") : "") + (hours > 0 ? (hours + "h ") : "") + (minutes > 0 ? (minutes + "m ") : "") + seconds + "s ");
                    }

                    // If the count down is finished, write some text
                    if (Math.floor((distance / 1000) / 60) > estimatedTimeInMinutes) {
                        clearInterval(x);
                        $(divTaskControlsWorkOrder).find("label." + (isInitialLoad ? "taskTimerInitial" : "taskTimer")).text("Late");
                        $(divTaskControlsWorkOrder).find("label." + (isInitialLoad ? "taskTimerInitial" : "taskTimer")).addClass("text-red");
                    }
                }, 1000);
            }
        }
    } catch (e) {

    }
}

function ShowTaskPausedWindowWorkOrder(btn, taskID) {
    var divTaskControlsWorkOrder = $(btn).closest("div[id='divTaskControlsWorkOrder']");

    if (divTaskControlsWorkOrder != null) {
        var remainingQty = parseFloat($(divTaskControlsWorkOrder).closest("tr").find("[id$=lblRemainingQuantity]").text());
        var processedQty = parseFloat($(divTaskControlsWorkOrder).find(".txtUnitsProcessed").val());


        if ($(divTaskControlsWorkOrder).find(".txtUnitsProcessed").val() === "") {
            alert("Enter processed quantity.")
        } else {
            if (processedQty <= remainingQty) {
                $("#hdnPausedTaskIDWorkOrder").val(taskID);
                $("#hdnProcessedQtyWorkOrder").val(processedQty);
                $("#divReasonForPauseWorkOrder").modal("show");
            } else {
                alert("You can’t exceed the Remaining qty.")
            }
        }
    }

    return false;
}

function TaskPausedWorkOrder() {
    var a1 = SaveTimeEntryWorkOrder(0, parseInt($("#hdnPausedTaskIDWorkOrder").val()), 2, new Date(), parseFloat($("#hdnProcessedQtyWorkOrder").val()), parseInt($("[id$=ddlPauseReasonWorkOrder]").val()), $("#txtPauseNotesWorkOrder").val(), false);

    $.when(a1).then(function (data) {
        $("#tableWIP > tbody > tr").each(function (index, e) {
            if ($(e).find("input.hdnTaskID").length > 0 && parseInt($(e).find("input.hdnTaskID").val()) === parseInt($("#hdnPausedTaskIDWorkOrder").val())) {
                $(e).find("div[id='divTaskControlsWorkOrder']").html("<ul class='list-inline'><li style='vertical-align:middle;padding-left:0px;padding-right:0px;'><button class='btn btn-xs btn-info' onclick='return ShowTaskTimeLogWindowWorkOrder(" + $("#hdnPausedTaskIDWorkOrder").val() + ");'><i class='fa fa-clock-o'></i></button></li><li style='vertical-align:middle;padding-right:0px;'><button class='btn btn-xs btn-flat bg-purple' onclick='return TaskStartedWorkOrder(this," + $("#hdnPausedTaskIDWorkOrder").val() + ",1);'>Resume</button></li></ul>");
            }
        });

        $("[id$=ddlPauseReasonWorkOrder]").val("0");
        $("#txtPauseNotesWorkOrder").val("");
        $("#hdnPausedTaskIDWorkOrder").val("");
        $("#divReasonForPauseWorkOrder").modal("hide");
        document.location.href = document.location.href;
    }, function (jqXHR, textStatus, errorThrown) {
        if (IsJsonString(jqXHR.responseText)) {
            var objError = $.parseJSON(jqXHR.responseText)
            if (objError.Message != null) {
                alert("Error occurred: " + objError.Message);
            } else {
                alert(objError);
            }
        } else {
            alert("Unknown error ocurred");
        }
    });

    return false;
}

function ShowTaskTimeLogWindowWorkOrder(taskID) {
    $("#tblTaskTimeLogWorkOrder > tbody").html("");

    $.ajax({
        type: "POST",
        url: '../WebServices/StagePercentageDetailsTaskTimeLogService.svc/GetTimeEntries',
        contentType: "application/json",
        dataType: "json",
        data: JSON.stringify({
            "taskID": taskID
        }),
        beforeSend: function () {
            $("[id$=UpdateProgress]").show();
        },
        complete: function () {
            $("[id$=UpdateProgress]").hide();
        },
        success: function (data) {
            var obj = $.parseJSON(data.GetTimeEntriesResult);

            $("#divTaskLogWorkOrder #spnTimeSpendWorkOrder").html(obj.TotalTimeSpendOnTask);
            if (obj.TotalTimeSpendOnTask === "Invalid time sequence") {
                $("#divTaskLogWorkOrder #spnTimeSpendWorkOrder").attr("class", "badge bg-red");
            }
            var records = $.parseJSON(obj.Records);

            try {
                if (records != null && records.length > 0) {
                    var html = "";

                    records.forEach(function (n) {
                        html = "<tr>";
                        html += "<td>" + replaceNull(n.vcEmployee) + "</td>";
                        html += "<td class='text-center'>" + replaceNull(n.vcAction) + "</td>";
                        html += "<td class='text-center'>" + replaceNull(n.vcActionTime) + "</td>";
                        html += "<td>" + replaceNull(n.vcProcessedQty) + "</td>";
                        html += "<td>" + replaceNull(n.vcReasonForPause) + "</td>";
                        html += "<td id='tdNotes'>" + replaceNull(n.vcNotes) + "</td>";
                        html += "<td class='text-center'>"
                        if (!JSON.parse(parseInt(n.bitTaskFinished))) {
                            html += "<input type='hidden' class='tintAction' value='" + replaceNull(n.tintAction) + "'>"
                            html += "<input type='hidden' class='dtActionTime' value='" + replaceNull(n.dtActionTime) + "'>"
                            html += "<input type='hidden' class='numProcessedQty' value='" + replaceNull(n.numProcessedQty) + "'>"
                            html += "<input type='hidden' class='numReasonForPause' value='" + replaceNull(n.numReasonForPause) + "'>"
                            html += "<button class='btn btn-xs btn-info' onclick='return EditTimeEntryWorkOrder(this," + replaceNull(n.ID) + ")'><i class='fa fa-pencil'></i></button>&nbsp;"
                            html += "<button class='btn btn-xs btn-danger' onclick='return DeleteTimeEntryWorkOrder(this," + replaceNull(n.ID) + "," + replaceNull(n.numTaskID) + ")'><i class='fa fa-trash-o'></i></button>";
                        }
                        html += "</td>";
                        html += "</tr>";
                        $("#tblTaskTimeLogWorkOrder > tbody").append(html);
                    });
                } else {
                    $("#tblTaskTimeLogWorkOrder > tbody").append("<tr><td colspan='7'>Not records found</td></tr>");
                }

                $("#hdnTaskLogTaskIDWorkOrder").val(taskID);
                $("#divTaskLogWorkOrder .btn-start-again").attr("onclick", "return StartTaskAgainWorkOrder(" + taskID + ")");
                $("#divTaskLogWorkOrder").modal("show");
            } catch (err) {
                $("[id$=UpdateProgress]").hide();
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            if (IsJsonString(jqXHR.responseText)) {
                var objError = $.parseJSON(jqXHR.responseText)
                if (objError.Message != null) {
                    alert("Error occurred: " + objError.Message);
                } else {
                    alert(objError);
                }
            } else {
                alert("Unknown error ocurred");
            }
        }
    });

    return false;
}

function ProcessedUnitsChangedWorkOrder(input, taskID, e) {
    if (e.keyCode == 13) {
        e.preventDefault();

        var divTaskControlsWorkOrder = $(input).closest("div[id='divTaskControlsWorkOrder']");

        if (divTaskControlsWorkOrder != null) {
            var remainingQty = parseFloat($(input).closest("tr").find("[id$=lblRemainingQuantity]").text());
            var processedQty = parseFloat($(input).val());

            if (processedQty === remainingQty) {
                ConfirmTaskFinishWorkOrder(taskID);
            }
        }
    }
}

function ConfirmTaskFinishWorkOrder(taskID) {
    if (confirm("You are about to close out this Task. You’ll no longer be able to edit time or qty. Do you want to proceed?")) {
        var a1 = SaveTimeEntryWorkOrder(0, taskID, 4, new Date(), 0, 0, "", false);

        $.when(a1).then(function (data) {
            document.location.href = document.location.href;
        }, function (jqXHR, textStatus, errorThrown) {
            if (IsJsonString(jqXHR.responseText)) {
                var objError = $.parseJSON(jqXHR.responseText)
                if (objError.Message != null) {
                    alert("Error occurred: " + objError.Message);
                } else {
                    alert(objError);
                }
            } else {
                alert("Unknown error ocurred");
            }
        });
    }
}

function TaskFinishedWorkOrder(btn, taskID, isResumed) {
    var a1 = SaveTimeEntryWorkOrder(0, taskID, 4, new Date(), 0, 0, "", false);

    $.when(a1).then(function (data) {
        document.location.href = document.location.href;
    }, function (jqXHR, textStatus, errorThrown) {
        if (IsJsonString(jqXHR.responseText)) {
            var objError = $.parseJSON(jqXHR.responseText)
            if (objError.Message != null) {
                alert("Error occurred: " + objError.Message);
            } else {
                alert(objError);
            }
        } else {
            alert("Unknown error ocurred");
        }
    });
}

function EditTimeEntryWorkOrder(btn, id) {
    try {
        var tr = $(btn).closest("tr");

        if (tr != null) {
            $("#divTaskLogWorkOrder #ddlActionWorkOrder").val($(tr).find(".tintAction").val());
            var actionTIme = $(tr).find(".dtActionTime").val();

            $telerik.findControl(document.documentElement, "rdpTaskStartDateWorkOrder").set_selectedDate(new Date(actionTIme));
            $telerik.findControl(document.documentElement, "txtStartFromHoursWorkOrder").set_value(new Date(actionTIme).getHours());
            $telerik.findControl(document.documentElement, "txtStartFromMinutesWorkOrder").set_value(new Date(actionTIme).getMinutes());

            if (new Date(actionTIme).getHours() > 12) {
                $("#rblStartFromTimeWorkOrder_0").prop("checked", false);
                $("#rblStartFromTimeWorkOrder_1").prop("checked", true);
                $telerik.findControl(document.documentElement, "txtStartFromHoursWorkOrder").set_value(new Date(actionTIme).getHours() - 12);
            } else {
                $("#rblStartFromTimeWorkOrder_0").prop("checked", true);
                $("#rblStartFromTimeWorkOrder_1").prop("checked", false);
            }

            if ($(tr).find(".tintAction").val() === "2") {
                $("#divTaskLogWorkOrder [id$=ddlAddReasonForPauseWorkOrder]").val($(tr).find(".numReasonForPause").val());
                $("#divTaskLogWorkOrder #txtAddProcessedUnitsWorkOrder").val($(tr).find(".numProcessedQty").val());
                $("#divTaskLogWorkOrder #txtAddNotesWorkOrder").val($(tr).find("#tdNotes").html());
                $("#divTaskLogWorkOrder [id$=ddlAddReasonForPauseWorkOrder]").show();
                $("#divTaskLogWorkOrder #txtAddProcessedUnitsWorkOrder").show();
                $("#divTaskLogWorkOrder #txtAddNotesWorkOrder").show();
            } else {
                $("#divTaskLogWorkOrder [id$=ddlAddReasonForPauseWorkOrder]").val("0");
                $("#divTaskLogWorkOrder #txtAddProcessedUnitsWorkOrder").val("");
                $("#divTaskLogWorkOrder #txtAddNotesWorkOrder").val("");
                $("#divTaskLogWorkOrder [id$=ddlAddReasonForPauseWorkOrder]").hide();
                $("#divTaskLogWorkOrder #txtAddProcessedUnitsWorkOrder").hide();
                $("#divTaskLogWorkOrder #txtAddNotesWorkOrder").hide();
            }

            $('input[name="TimeEntryTypeWorkOrder"]').removeAttr('checked');
            $("input[name='TimeEntryTypeWorkOrder'][value='2']").prop('checked', true);
            TimeEntryTypeChangedWorkOrder();
            $("#btnAddTimeEntryWorkOrder").attr("onclick", "return AddTimeEntryWorkOrder(" + id + ")");
            $("#btnCancelTimeEntryWorkOrder").show();
        }
    } catch (e) {
        alert("Unknown error occurred.");
    }

    return false;
}

function SaveTimeEntryWorkOrder(id, taskID, actionType, actionTime, processedQty, reasonForPause, notes, isManualEntry) {
    return $.ajax({
        type: "POST",
        url: '../WebServices/StagePercentageDetailsTaskTimeLogService.svc/Save',
        contentType: "application/json",
        dataType: "json",
        data: JSON.stringify({
            "id": id
            , "taskID": taskID
            , "actionType": actionType
            , "actionTime": "\/Date(" + actionTime.getTime() + ")\/"
            , "processedQty": processedQty
            , "reasonForPause": reasonForPause
            , "notes": notes
            , "isManualEntry": isManualEntry
        }),
        beforeSend: function () {
            $("[id$=UpdateProgress]").show();
        },
        complete: function () {
            $("[id$=UpdateProgress]").hide();
        }
    });
}

function AddTimeEntryWorkOrder(id) {
    try {
        if ($("input[name='TimeEntryTypeWorkOrder']:checked").val() == "1") {
            if ($telerik.findControl(document.documentElement, "rdpTaskStartDateSingleWorkOrder").get_selectedDate() == null) {
                alert("Select date");
                $telerik.findControl(document.documentElement, "rdpTaskStartDateSingleWorkOrder").get_dateInput().focus();

                return false;
            }

            var pickerStart = $telerik.findControl(document.documentElement, "rtpStartTimeWorkOrder");
            var viewStart = pickerStart.get_timeView();

            if (viewStart.getTime() == null) {
                alert("Select Start Time");
                return false;
            }

            var pickerEnd = $telerik.findControl(document.documentElement, "rtpEndTimeWorkOrder");
            var viewEnd = pickerEnd.get_timeView();

            if (viewEnd.getTime() == null) {
                alert("Select End Time");
                return false;
            }

            var dt = $telerik.findControl(document.documentElement, "rdpTaskStartDateSingleWorkOrder").get_selectedDate();
            var hours = parseInt(viewStart.getTime().getHours());
            var minutes = parseInt(viewStart.getTime().getMinutes());
            var dtStartTime = new Date(dt.getFullYear(), dt.getMonth(), dt.getDate(), hours, minutes, 0);

            dt = $telerik.findControl(document.documentElement, "rdpTaskStartDateSingleWorkOrder").get_selectedDate();
            hours = parseInt(viewEnd.getTime().getHours());
            minutes = parseInt(viewEnd.getTime().getMinutes());
            var dtEndTime = new Date(dt.getFullYear(), dt.getMonth(), dt.getDate(), hours, minutes, 0);

            if (dtStartTime > dtEndTime) {
                alert("End Time must be after start time.");
                return false;
            }

            var a1 = SaveTimeEntryWorkOrder(id, parseInt($("#divTaskLogWorkOrder #hdnTaskLogTaskIDWorkOrder").val()), 1, dtStartTime, 0, 0, "", true);

            $.when(a1).then(function (data) {
                var a2 = SaveTimeEntryWorkOrder(id, parseInt($("#divTaskLogWorkOrder #hdnTaskLogTaskIDWorkOrder").val()), 4, dtEndTime, 0, 0, "", true);

                $.when(a2).then(function (data) {
                    document.location.href = document.location.href;
                }, function (jqXHR, textStatus, errorThrown) {
                    if (IsJsonString(jqXHR.responseText)) {
                        var objError = $.parseJSON(jqXHR.responseText)
                        if (objError.Message != null) {
                            alert("Error occurred: " + objError.Message);
                        } else {
                            alert(objError);
                        }
                    } else {
                        alert("Unknown error ocurred");
                    }
                });
            }, function (jqXHR, textStatus, errorThrown) {
                if (IsJsonString(jqXHR.responseText)) {
                    var objError = $.parseJSON(jqXHR.responseText)
                    if (objError.Message != null) {
                        alert("Error occurred: " + objError.Message);
                    } else {
                        alert(objError);
                    }
                } else {
                    alert("Unknown error ocurred");
                }
            });
        } else {
            if ($telerik.findControl(document.documentElement, "rdpTaskStartDateWorkOrder").get_selectedDate() == null) {
                alert("Select date");
                $telerik.findControl(document.documentElement, "rdpTaskStartDateWorkOrder").get_dateInput().focus();
                return false;
            }

            if ($telerik.findControl(document.documentElement, "txtStartFromHoursWorkOrder").get_value() === "") {
                alert("Select start hour.");
                $telerik.findControl(document.documentElement, "txtStartFromHoursWorkOrder").focus();
                return false;
            }

            if ($telerik.findControl(document.documentElement, "txtStartFromMinutesWorkOrder").get_value() === "") {
                alert("Select start minute.");
                $telerik.findControl(document.documentElement, "txtStartFromMinutesWorkOrder").focus();
                return false;
            }

            if ($("#divTaskLogWorkOrder #ddlActionWorkOrder").val() === "2" && $("#divTaskLogWorkOrder #txtAddProcessedUnitsWorkOrder").val() === "") {
                alert("Processed Qty required.");
                $("#divTaskLogWorkOrder #txtAddProcessedUnitsWorkOrder").focus();

                return false;
            }

            var dt = $telerik.findControl(document.documentElement, "rdpTaskStartDateWorkOrder").get_selectedDate();
            var hours = parseInt($telerik.findControl(document.documentElement, "txtStartFromHoursWorkOrder").get_value());
            if ($("#rblStartFromTimeWorkOrder_1").is(":checked")) {
                hours = hours + 12;
            }
            var minutes = parseInt($telerik.findControl(document.documentElement, "txtStartFromMinutesWorkOrder").get_value());
            var dtActionTime = new Date(dt.getFullYear(), dt.getMonth(), dt.getDate(), hours, minutes, 0);
            var numProcessedQty = 0;
            var reasonForPause = 0;
            var notes = "";

            if ($("#divTaskLogWorkOrder #ddlActionWorkOrder").val() === "2") {
                numProcessedQty = parseFloat($("#divTaskLogWorkOrder #txtAddProcessedUnitsWorkOrder").val());
                reasonForPause = parseInt($("#divTaskLogWorkOrder [id$=ddlAddReasonForPauseWorkOrder]").val());
                notes = $("#divTaskLogWorkOrder #txtAddNotesWorkOrder").val();
            }

            var a1 = SaveTimeEntryWorkOrder(id, parseInt($("#divTaskLogWorkOrder #hdnTaskLogTaskIDWorkOrder").val()), parseInt($("#divTaskLogWorkOrder #ddlActionWorkOrder").val()), dtActionTime, numProcessedQty, reasonForPause, notes, true);

            $.when(a1).then(function (data) {
                CancelTimeEntryWorkOrder();
                ShowTaskTimeLogWindowWorkOrder(parseInt($("#divTaskLogWorkOrder #hdnTaskLogTaskIDWorkOrder").val()));
            }, function (jqXHR, textStatus, errorThrown) {
                if (IsJsonString(jqXHR.responseText)) {
                    var objError = $.parseJSON(jqXHR.responseText)
                    if (objError.Message != null) {
                        alert("Error occurred: " + objError.Message);
                    } else {
                        alert(objError);
                    }
                } else {
                    alert("Unknown error ocurred");
                }
            });
        }
    } catch (e) {
        alert("Unknown error occurred.");
    }

    return false;
}

function TimeEntryTypeChangedWorkOrder() {
    if ($("input[name='TimeEntryTypeWorkOrder']:checked").val() == "1") {
        $("#tblTaskTimeLogWorkOrder td.tdSingleDay").show();
        $("#tblTaskTimeLogWorkOrder td.tdMultiDayWorkOrder").hide();
        $("#tblTaskTimeLogWorkOrder #btnAddTimeEntryWorkOrder").attr("class", "btn btn-sm btn-success");
        $("#tblTaskTimeLogWorkOrder #btnAddTimeEntryWorkOrder").val("Finish Task");
    } else {
        $("#tblTaskTimeLogWorkOrder td.tdSingleDay").hide();
        $("#tblTaskTimeLogWorkOrder td.tdMultiDayWorkOrder").show();
        $("#tblTaskTimeLogWorkOrder #btnAddTimeEntryWorkOrder").attr("class", "btn btn-sm btn-primary");
        $("#tblTaskTimeLogWorkOrder #btnAddTimeEntryWorkOrder").val("Save");
    }
}

function DeleteTimeEntryWorkOrder(btn, id, taskID) {
    if (confirm("Time entry will be deleted. Do you want to proceed?")) {
        $.ajax({
            type: "POST",
            url: '../WebServices/StagePercentageDetailsTaskTimeLogService.svc/Delete',
            contentType: "application/json",
            dataType: "json",
            data: JSON.stringify({
                "id": id
                , "taskID": taskID
            }),
            beforeSend: function () {
                $("[id$=UpdateProgress]").show();
            },
            complete: function () {
                $("[id$=UpdateProgress]").hide();
            },
            success: function (data) {
                $(btn).closest("tr").remove();
            },
            error: function (jqXHR, textStatus, errorThrown) {
                if (IsJsonString(jqXHR.responseText)) {
                    var objError = $.parseJSON(jqXHR.responseText)
                    if (objError.Message != null) {
                        alert("Error occurred: " + objError.Message);
                    } else {
                        alert(objError);
                    }
                } else {
                    alert("Unknown error ocurred");
                }
            }
        });
    }

    return false;
}

function CancelTimeEntryWorkOrder() {
    $("#btnCancelTimeEntryWorkOrder").hide();
    $("#btnAddTimeEntryWorkOrder").attr("onclick", "return AddTimeEntryWorkOrder(0);");
    $("#ddlActionWorkOrder").val("1");
    $("[id$=ddlAddReasonForPauseWorkOrder]").val("0");
    $("#txtAddProcessedUnitsWorkOrder").val("");
    $("#txtAddNotesWorkOrder").val("");
    $('input[name="TimeEntryTypeWorkOrder"]').removeAttr('checked');
    $("input[name='TimeEntryTypeWorkOrder'][value='1']").prop('checked', true);
    TimeEntryTypeChangedWorkOrder();
    return false;
}

function TimeEntryActionChangeWorkOrder(ddl) {
    if ($(ddl).val() === "2") {
        $("#divTaskLogWorkOrder [id$=ddlAddReasonForPauseWorkOrder]").show();
        $("#divTaskLogWorkOrder #txtAddProcessedUnitsWorkOrder").show();
        $("#divTaskLogWorkOrder #txtAddNotesWorkOrder").show();
    } else {
        $("#divTaskLogWorkOrder [id$=ddlAddReasonForPauseWorkOrder]").hide();
        $("#divTaskLogWorkOrder #txtAddProcessedUnitsWorkOrder").hide();
        $("#divTaskLogWorkOrder #txtAddNotesWorkOrder").hide();
    }

    return false;
}

function StartTaskAgainWorkOrder(taskID) {
    if (confirm("Existing time entries will be removed and you have to start task again. Do you want to proceed?")) {
        $.ajax({
            type: "POST",
            url: '../WebServices/StagePercentageDetailsTaskTimeLogService.svc/Delete',
            contentType: "application/json",
            dataType: "json",
            data: JSON.stringify({
                "id": -1
                , "taskID": taskID
            }),
            beforeSend: function () {
                $("[id$=UpdateProgress]").show();
            },
            complete: function () {
                $("[id$=UpdateProgress]").hide();
            },
            success: function (data) {
                document.location.href = document.location.href;
            },
            error: function (jqXHR, textStatus, errorThrown) {
                if (IsJsonString(jqXHR.responseText)) {
                    var objError = $.parseJSON(jqXHR.responseText)
                    if (objError.Message != null) {
                        alert("Error occurred: " + objError.Message);
                    } else {
                        alert(objError);
                    }
                } else {
                    alert("Unknown error ocurred");
                }
            }
        });
    }

    return false;
}