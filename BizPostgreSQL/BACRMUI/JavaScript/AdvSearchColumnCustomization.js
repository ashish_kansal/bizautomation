////////////////////////////////////////////////////////////////////////////
///////////////////////ADVANCE SEARCH MASS UPDATE///////////////////////////
////////////////////////////////////////////////////////////////////////////
/*
Purpose:	Closes the present window
Created By: Debasish Tapan Nag
Parameter:	1) None
Return		1) None
*/
function CloseThisWin()
{
	if(opener.document.getElementById('btnRefresh'))
	{
		opener.document.getElementById('btnRefresh').click();
	}
	this.close();
}

/*
Purpose:	encodes the string
Created By: Debasish Tapan Nag
Parameter:	1) fBoxString: The source string
Return		1) The htmlencoded string
*/
function encodeMyHtml(fBoxString)
{
     encodedHtml = unescape(fBoxString);
     encodedHtml = encodedHtml.replace(/&/g,"\&amp;");
     encodedHtml = encodedHtml.replace(/\>/g,"&gt;");
     encodedHtml = encodedHtml.replace(/\</g,"&lt;");
     encodedHtml = encodedHtml.replace(/'/g,"\&apos;");
     encodedHtml = encodedHtml.replace(/"/g,"\&quot;");
     return encodedHtml;
}
/*
Purpose:	validates the value that is entered tor mass update
Created By: Debasish Tapan Nag
Parameter:	1) thisFormObject: The Form Object
Return		1) None
*/
function validateEnteredValue(thisFormObject)
{
	if(opener.document.getElementById('txtCheckedRecordCompIds'))
	{
		var numSelectedIndex = document.getElementById('lstAvailablefld').selectedIndex;
		if(numSelectedIndex != 0)
		{
			var sValue;
			if (document.getElementById('hdDbAssociatedControl').value == 'TextBox' || document.getElementById('hdDbAssociatedControl').value == 'TextArea')
			{
				sValue = encodeMyHtml(document.getElementById('txtValue').value);
			}else if(document.getElementById('hdDbAssociatedControl').value == 'SelectBox')	
			{
				if(document.getElementById('ddlValue').selectedIndex==0)
				{
					alert('Select from the drop-down before you mass update.');
					return false;
				}
				sValue = document.getElementById('ddlValue').value;
			}else if(document.getElementById('hdDbAssociatedControl').value == 'CheckBox')	
			{
				sValue = document.getElementById('cbValue').checked?1:0;	
			}
			
			document.getElementById('hdColumnValue').value = sValue;
			document.getElementById('hdEntityIdRefList').value = opener.document.getElementById('txtCheckedRecordCompIds').value;
			return true;
		}else{
			alert('Select the field to update.');
			return false;
		}
	}else
	{
			alert('Cannot do a mass update as the main Adv. Search window has been closed.');
			return false;
	}	
}

/*
Purpose:	Class for storing the form data
Created By: Debasish Tapan Nag
Parameter:	1) vcDbColumnName: The table columns where the data entered in these fields will be entered
			2) vcFieldName: The form field text
			3) vcDbColumnValue: The form field value
			4) numRowNum: The row number in which this control will appear
			5) numColumnNum: The column number in which this control will appear
			6) vcAssociatedControlType: The control type (EditBox/ SelectBox/ CheckBox etc)
			7) boolAOIField: Indicates if the field is AOI or not
Return		1) Nothing
*/
function classEditableFieldConfig(vcOrigDbColumnName, vcLookBackTableName, vcFormFieldName, vcListItemType, numListID, vcFieldDataType, vcAssociatedControlType)
{
	this.vcOrigDbColumnName = vcOrigDbColumnName;
	this.vcLookBackTableName = vcLookBackTableName;
	this.vcFormFieldName = vcFormFieldName;
	this.vcListItemType = vcListItemType;
	this.numListID = numListID;
	this.vcFieldDataType = vcFieldDataType;
	this.vcAssociatedControlType = vcAssociatedControlType;
}
/*
Purpose:	validates the value that is selected from the drop down and instantiates the values
Created By: Debasish Tapan Nag
Parameter:	1) None
Return		1) None
*/
function validateAndInitiateSubmission(thisObject)
{
	var numSelectedIndex = thisObject.selectedIndex;
	if(numSelectedIndex != 0)
	{
		document.getElementById('hdDbAssociatedControl').value = arrEditableFieldConfig[numSelectedIndex-1].vcAssociatedControlType;
		document.getElementById('hdLookBackTableName').value = arrEditableFieldConfig[numSelectedIndex-1].vcLookBackTableName;
		document.getElementById('hdOrigDbColumnName').value = arrEditableFieldConfig[numSelectedIndex-1].vcOrigDbColumnName;
		document.getElementById('hdDbColumnListItemType').value = arrEditableFieldConfig[numSelectedIndex-1].vcListItemType;
		document.getElementById('hdFieldDataType').value = arrEditableFieldConfig[numSelectedIndex-1].vcFieldDataType;
		document.getElementById('hdDbColumnListId').value = arrEditableFieldConfig[numSelectedIndex-1].numListID;
		return true;
	}else{
		return false;
	}
}
////////////////////////////////////////////////////////////////////////////
///////////////////////ADVANCE SEARCH SURVEY RATING/////////////////////////
////////////////////////////////////////////////////////////////////////////
/*
Purpose:	Ensures that proper selection of radio buttons is made
Created By: Debasish Tapan Nag
Parameter:	1) None
Return		1) None
*/
function resetSurveyIdEntry()
{
	if(document.getElementById('rbAllSurveys_0').checked)
	{
		document.getElementById('txtSurveys').readOnly=true;
	}else{
		document.getElementById('txtSurveys').readOnly=false;
	}
}
/*
Purpose:	Ensures that proper selection of radio buttons is made
Created By: Debasish Tapan Nag
Parameter:	1) None
Return		1) None
*/
function SaveSurveyRatingFilteration()
{
	Page_ClientValidate();							//explcitly call for validation
	if (Page_IsValid)
	{
		var sSurveyRatingFilter;
		sSurveyRatingFilter = document.getElementById('txtRatingStart').value + '.' + document.getElementById('txtRatingEnd').value + '.' + ((document.getElementById('rbAllSurveys_0').checked)?'*':document.getElementById('txtSurveys').value);
		document.getElementById('hdRatingInSurveyIds').value=sSurveyRatingFilter;
		
	}
	return false;
}
/*
Purpose:	Provides client side script to the custom validatior for Survey Ids
Created By: Debasish Tapan Nag
Parameter:	1) source: Source of the event
			2) args: Arguments for the event
Return		1) None
*/
function validateSurveyIds(source, args)
{
	if(document.getElementById('rbAllSurveys_1').checked && document.getElementById('txtSurveys').value!='')
	{
		var arrSurveyIds = new Array();
		var sSurveyIds = document.getElementById('txtSurveys').value;
		arrSurveyIds = sSurveyIds.split(',');
		for(iIndex=0;iIndex<arrSurveyIds.length;iIndex++)
		{
			if(!IsNumeric(arrSurveyIds[iIndex]))	
			{
				args.IsValid = false;
				return;
			}
		}	
	}
	args.IsValid = true;
}