﻿function $SearchElement(e, r, t) {
    for (var l, r = r || "*", t = t || document, a = "*" == r && t.all ? t.all : t.getElementsByTagName(r), n = [], u = a.length, i = 0; u > i; i++) l = a[i], l.id.indexOf(e) >= 0 && n.push(l);
    return n[0]
}

function CheckMail(e, r, t) {
    var l = $SearchElement(e), a = emailfilter.test(trim(l.value));

    if (l.value === '') {
        return true;
    } else {
        return 0 == a && (0 == t.length ? alert("Please enter a valid " + r + " .") : alert(t), l.select()), a
    }
}

function RequiredField(e, r, t) {
    var l = $SearchElement(e);
    return null != l && 0 == trim(l.value).length ? (0 == t.length ? alert(r + " is required!") : alert(t), l.focus(), !1) : void 0
}

function RequiredFieldDropDown(e, r, t) {
    var l = $SearchElement(e);
    if ($find(e) != null) {
        var comboBox = $find(e);
        var input = comboBox.get_inputDomElement();
        return null != $find(e) && 0 == comboBox.get_value() ? (0 == t.length ? alert(r + " is required!") : alert(t), input.focus(), !1) : void 0
    } else {
        return null != l && 0 == l.value ? (0 == t.length ? alert(r + " is required!") : alert(t), l.focus(), !1) : void 0
    }
}

function IsNumeric(e, r, t) {
    var l, a = $SearchElement(e);
    null != a && (l = trim(a.value));
    if (l === '')
    {
        return true;
    } else {
        var n = /(^\d+$)|(^\d+\.\d+$)/;
        return n.test(l) ? testresult = !0 : (0 == t.length ? alert("Please input a valid number for " + r + "!") : alert(t), a.focus(), testresult = !1), testresult
    }
}

function IsAlphaNumeric(e, r, t) {
    var l, a = $SearchElement(e);
    null != a && (l = trim(a.value));
    if (l === '') {
        return true;
    } else {
        for (var n = 0; n < l.length; n++) {
            var u = l.charAt(n),
                i = u.charCodeAt(0);
            if (!(i > 47 && 58 > i || i > 64 && 91 > i || i > 96 && 123 > i)) return 0 == t.length ? alert("Please enter valid AlphaNumeric value for " + r + " ") : alert(t), a.focus(), !1
        }
        return !0
    }
}

function RangeValidator(e, r, t, l, a) {
    var n, u = $SearchElement(e);
    if (u.value === '') {
        return true;
    } else {
        return null != u && (n = parseInt(trim(u.value))), "" == trim(u.value) ? !0 : isNaN(n) && 0 == IsNumeric(e, r, a) ? !1 : isNaN(n) || (t = parseInt(t), l = parseInt(l), l >= n && n >= t) ? void 0 : (0 == a.length ? alert("Value of " + r + " must be between " + t + " and " + l) : alert(a), u.focus(), !1)
    }
}

function trim(e, r) {
    return ltrim(rtrim(e, r), r)
}

function ltrim(e, r) {
    return r = r || "\\s", e.replace(new RegExp("^[" + r + "]+", "g"), "")
}

function rtrim(e, r) {
    return r = r || "\\s", e.replace(new RegExp("[" + r + "]+$", "g"), "")
}
var emailfilter = /^\w+[\+\.\w-]*@([\w-]+\.)*\w+[\w-]*\.([a-z]{2,4}|\d+)$/i;