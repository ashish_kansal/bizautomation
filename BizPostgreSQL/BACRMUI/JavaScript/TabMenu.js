﻿
function TabMenuLink(id, name, link, tabid, image, bitNew, NewLink) {
    this.id = id;
    this.name = name;
    this.link = link;
    this.tabid = tabid;
    this.image = image;
    this.bitNew = bitNew;
    this.NewLink = NewLink;
}

var TabId = 0;
var vTabSelect = false;
var myTimer = null;
var timerRunning = false;

function onTabMouseOver(sender, args) {
    //alert(args.get_tab().get_value());
    //alert(args.get_tab().get_text());
    //SetTabMenu(args.get_tab().get_value());

    //    var multiPageID = sender.get_multiPageID();
    //    sender.set_multiPageID(null);
    //    args.get_tab().select();
    //    sender.set_multiPageID(multiPageID);

    myTimer = setTimeout("DelaySelectTab('" + args.get_tab().get_value() + "');", 500);
    timerRunning = true;

    //SetTabMenu(args.get_tab().get_value());
}

function DelaySelectTab(tabId) {
    //console.log(tabId);
    SelectTabByValue(tabId);

    SetTabMenu(tabId);
    timerRunning = false;
}

function onTabMouseOut(sender, args) {
    //document.getElementById("tdLink").innerHTML = '&nbsp;';
    if (timerRunning)
        clearTimeout(myTimer);
}

function onTabSelected(sender, args) {
    SetTabMenu(args.get_tab().get_value());
}

function OnTabSelecting(sender, args) {
    if (TabId != args.get_tab().get_value()) {
        SetTabMenu(args.get_tab().get_value());
    }

    TabId = args.get_tab().get_value();
    //console.log(args.get_domEvent().type);

    if (vTabSelect == false) {
        args.set_cancel(true);
    }
    else {
        args.set_cancel(false);
    }
    vTabSelect = false;
}

function onTabDoubleClick(sender, args) {
    vTabSelect = true;
    args.get_tab().select();
    //OnTabSelecting(sender, args);
    //sender._postback(args.get_tab());
    vTabSelect = false;

    OpenInMainFrame(args.get_tab().get_navigateUrl());
    //args.get_tab().set_postBack(true);
}

function ClientTabstripLoad(tabstrip) {
    var selectedTab = tabstrip.get_selectedTab();

    if (selectedTab != null)
        SetTabMenu(selectedTab.get_value());
}

function SetTabMenu(tabid) {
    document.getElementById("divLink").innerHTML = '&nbsp;';

    var content = '';
    for (var iIndex = 0; iIndex < arrTabFieldConfig.length; iIndex++) {
        if (arrTabFieldConfig[iIndex].tabid == tabid) {
            var strContent;
            if (arrTabFieldConfig[iIndex].image.length > 0) {
                strContent = '<a onclick="' + arrTabFieldConfig[iIndex].link + '" class="normal11" style="cursor: pointer;font-size:11.5px;"><img src="' + arrTabFieldConfig[iIndex].image + '" /> ' + arrTabFieldConfig[iIndex].name + '</a>';
            }
            else {
                strContent = '<a onclick="' + arrTabFieldConfig[iIndex].link + '" class="normal11" style="cursor: pointer;font-size:11.5px;" >' + arrTabFieldConfig[iIndex].name + '</a>';
            }

            if (arrTabFieldConfig[iIndex].bitNew == 'True' && arrTabFieldConfig[iIndex].NewLink.length > 0) {
                //console.log(arrTabFieldConfig[iIndex].link.toLowerCase().indexOf("frmcompanylist.aspx"));
                if (arrTabFieldConfig[iIndex].link.toLowerCase().indexOf("frmcompanylist.aspx") != -1) {
                    strContent += '&nbsp;&nbsp;<a onclick="' + arrTabFieldConfig[iIndex].NewLink + '" class="normal11" style="cursor: pointer;font-size:11.5px;" ><img class="alignbottom" src="../images/AddRecord.png" title="New ' + arrTabFieldConfig[iIndex].name.substring(0, arrTabFieldConfig[iIndex].name.length-1) + '" border="0" /></a>';
                } else {
                    strContent += '&nbsp;&nbsp;<a onclick="' + arrTabFieldConfig[iIndex].NewLink + '" class="normal11" style="cursor: pointer;font-size:11.5px;" ><img class="alignbottom" src="../images/AddRecord.png" title="New ' + arrTabFieldConfig[iIndex].name + '" border="0" /></a>';
                }
            }

            content += strContent + '&nbsp;&nbsp;|&nbsp;&nbsp;';
        }
    }
    content += '@@';

    //content = $.trim($(content).text());
    //    content = content.substring(0, content.length - 1);

    //    console.log($(content).text());
    //    console.log(content);
    //content = content.replace(new RegExp('/|&nbsp;&nbsp;@@', 'g'), '');

    content = content.replace(/(\|&nbsp;&nbsp;@@)/g, '');
    content = content.replace(/(@@)/g, '');

    $('#divLink').append(content);
}


/* This script and many more are available free online at
The JavaScript Source!! http://javascript.internet.com
Created by: Mr J | http://www.huntingground.net/ */

scrollStep = 1

timerLeft = ""
timerRight = ""

function toLeft(id) {
    document.getElementById(id).scrollLeft = 0
}

function scrollDivLeft(id) {
    clearTimeout(timerRight)
    document.getElementById(id).scrollLeft += scrollStep
    timerRight = setTimeout("scrollDivLeft('" + id + "')", 10)
}

function scrollDivRight(id) {
    clearTimeout(timerLeft)
    document.getElementById(id).scrollLeft -= scrollStep
    timerLeft = setTimeout("scrollDivRight('" + id + "')", 10)
}

function toRight(id) {
    document.getElementById(id).scrollLeft = document.getElementById(id).scrollWidth
}

function stopMe() {
    clearTimeout(timerRight)
    clearTimeout(timerLeft)
}

