/*
Purpose:	Closes the Window
Created By: Debasish Tapan Nag
Parameter:	1) None
Return		1) None
*/
function Close()
{
	window.close()
	return false;
}
/*
Purpose:	Returns the status of user confirmation about deleting an association
Created By: Debasish Tapan Nag
Parameter:	1) None
Return		1) None
*/
function DeleteRecord()
{
	return confirm('Are you sure, you want to delete the selected association?');
}
/*
Purpose:	Ensures that either Parent or Child checkboxes are checked at a time
Created By: Debasish Tapan Nag
Parameter:	1) None
Return		1) None
*/
function EnsureParentChildUniqueness(thisElement)
{
	if (thisElement.checked)
	{
		if (thisElement.id=='cbParentOrgIndicator')
			document.getElementById('cbChildOrgIndicator').checked = 0;
		else
			document.getElementById('cbParentOrgIndicator').checked = 0;
	}
}
/*
Purpose:	Validates the parameters for Moving contacts and also asks for confirmation
Created By: Debasish Tapan Nag
Parameter:	1) None
Return		1) trye/ false
*/

function validateAndConfirmMove()
{
	Page_ClientValidate();							//explcitly call for validation
	if (Page_IsValid)
	{
		return confirm(sButtomMessage);
	}
	return false;
}