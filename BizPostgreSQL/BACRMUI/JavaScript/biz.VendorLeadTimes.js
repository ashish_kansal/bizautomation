﻿let VendorLeadTimes = (function () {
    let privateProps = new WeakMap();

    class VendorLeadTimes {
        constructor(vendorID, vendorLeadTimesID) {
            //this.vendorID = vendorID; // this is public
            privateProps.set(this, {
                vendorID: vendorID
                , vendorLeadTimesID: vendorLeadTimesID
                , fromQty: null
                , toQty: null
                , itemGroups: null
                , shipVia: null
                , shippingServices: null
                , shipFromLocations: null
                , shipToCountry: null
                , shipToStates: null
                , daysToArrive: null
            }); // this is private
        }

        set fromQty(value) {
            privateProps.get(this).fromQty = value;
        }

        set toQty(value) {
            privateProps.get(this).toQty = value;
        }

        set itemGroups(value) {
            privateProps.get(this).itemGroups = value;
        }

        set shipVia(value) {
            privateProps.get(this).shipVia = value;
        }

        set shippingServices(value) {
            privateProps.get(this).shippingServices = value;
        }

        set shipFromLocations(value) {
            privateProps.get(this).shipFromLocations = value;
        }

        set shipToCountry(value) {
            privateProps.get(this).shipToCountry = value;
        }

        set shipToStates(value) {
            privateProps.get(this).shipToStates = value;
        }

        set daysToArrive(value) {
            privateProps.get(this).daysToArrive = value;
        }

        GetByVendor() {
            var objVendorLeadTimes = this;

            return new Promise(function (resolve, reject) {
                try {
                    return $.ajax({
                        type: "POST",
                        url: '../WebServices/CommonService.svc/VendorLeadTimes/GetByVendor',
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        data: JSON.stringify({
                            "vendorID": parseInt(privateProps.get(objVendorLeadTimes).vendorID)
                        }),
                        success: function (data) {
                            let objRules = null;
                            if (data.VendorLeadTimesGetByVendorResult != null && data.VendorLeadTimesGetByVendorResult != "") {
                                objRules = $.parseJSON(data.VendorLeadTimesGetByVendorResult);
                            }

                            var objResult = new Object();
                            objResult.IsSuccess = true;
                            objResult.ErrorMessage = "";
                            objResult.Data = objRules;
                            resolve(objResult)
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            var objResult = new Object();
                            objResult.IsSuccess = false;

                            if (objVendorLeadTimes.#IsJsonString(jqXHR.responseText)) {
                                var objError = $.parseJSON(jqXHR.responseText)
                                objResult.ErrorMessage = "Error occurred while fetching data: " + objVendorLeadTimes.#ReplaceNull(objError.ErrorMessage);
                            } else {
                                objResult.ErrorMessage = "Unknown error occurred while fetching data.";
                            }

                            reject(objResult);
                        }
                    });
                } catch (e) {
                    var objResult = new Object();
                    objResult.IsSuccess = false;
                    objResult.ErrorMessage = "Unknown error occurred while fetching data.";

                    reject(objResult);
                }
            });
        }

        GetByID() {
            var objVendorLeadTimes = this;

            return new Promise(function (resolve, reject) {
                try {
                    return $.ajax({
                        type: "POST",
                        url: '../WebServices/CommonService.svc/VendorLeadTimes/GetByID',
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        data: JSON.stringify({
                            "vendorLeadTimesID": parseInt(privateProps.get(objVendorLeadTimes).vendorLeadTimesID)
                            , "vendorID": parseInt(privateProps.get(objVendorLeadTimes).vendorID)
                        }),
                        success: function (data) {
                            let objRule = null;
                            if (data.VendorLeadTimesGetByIDResult != null && data.VendorLeadTimesGetByIDResult != "") {
                                objRule = $.parseJSON(data.VendorLeadTimesGetByIDResult);
                            }

                            var objResult = new Object();
                            objResult.IsSuccess = true;
                            objResult.ErrorMessage = "";
                            objResult.Data = objRule;
                            resolve(objResult);
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            var objResult = new Object();
                            objResult.IsSuccess = false;

                            if (objVendorLeadTimes.#IsJsonString(jqXHR.responseText)) {
                                var objError = $.parseJSON(jqXHR.responseText)
                                objResult.ErrorMessage = "Error occurred while fetching data: " + objVendorLeadTimes.#ReplaceNull(objError.ErrorMessage);
                            } else {
                                objResult.ErrorMessage = "Unknown error occurred while fetching data.";
                            }

                            reject(objResult);
                        }
                    });
                } catch (e) {
                    var objResult = new Object();
                    objResult.IsSuccess = false;
                    objResult.ErrorMessage = "Unknown error occurred while fetching data.";

                    reject(objResult);
                }
            });
        }

        Save() {
            var objVendorLeadTimes = this;

            return new Promise(function (resolve, reject) {
                try {
                    var obj = new Object();
                    obj.FromQuantity = parseInt(privateProps.get(objVendorLeadTimes).fromQty);
                    obj.ToQuantity = parseInt(privateProps.get(objVendorLeadTimes).toQty);
                    obj.ItemGroups = privateProps.get(objVendorLeadTimes).itemGroups != null && privateProps.get(objVendorLeadTimes).itemGroups != "" ? privateProps.get(objVendorLeadTimes).itemGroups : [];
                    obj.ShipVia = parseInt(privateProps.get(objVendorLeadTimes).shipVia);
                    obj.ShippingServices = privateProps.get(objVendorLeadTimes).shippingServices != null && privateProps.get(objVendorLeadTimes).shippingServices != "" ? privateProps.get(objVendorLeadTimes).shippingServices : [];
                    obj.ShipFromLocations = privateProps.get(objVendorLeadTimes).shipFromLocations != null && privateProps.get(objVendorLeadTimes).shipFromLocations != "" ? privateProps.get(objVendorLeadTimes).shipFromLocations : [];
                    obj.ShipToCountry = parseInt(privateProps.get(objVendorLeadTimes).shipToCountry);
                    obj.ShipToStates = privateProps.get(objVendorLeadTimes).shipToStates != null && privateProps.get(objVendorLeadTimes).shipToStates != "" ? privateProps.get(objVendorLeadTimes).shipToStates : [];
                    obj.DaysToArrive = parseInt(privateProps.get(objVendorLeadTimes).daysToArrive);

                    return $.ajax({
                        type: "POST",
                        url: '../WebServices/CommonService.svc/VendorLeadTimes/Save',
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        data: JSON.stringify({
                            "vendorLeadTimesID": parseInt(privateProps.get(objVendorLeadTimes).vendorLeadTimesID),
                            "vendorID": parseInt(privateProps.get(objVendorLeadTimes).vendorID),
                            "data": JSON.stringify(obj)
                        }),
                        success: function (data) {
                            var objResult = new Object();
                            objResult.IsSuccess = true;
                            objResult.ErrorMessage = "";
                            resolve(objResult)
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            var objResult = new Object();
                            objResult.IsSuccess = false;

                            if (objVendorLeadTimes.#IsJsonString(jqXHR.responseText)) {
                                var objError = $.parseJSON(jqXHR.responseText)
                                objResult.ErrorMessage = "Error occurred while saving data: " + objVendorLeadTimes.#ReplaceNull(objError.ErrorMessage);
                            } else {
                                objResult.ErrorMessage = "Unknown error occurred while saving data.";
                            }

                            reject(objResult);
                        }
                    });
                } catch (ex) {
                    var objResult = new Object();
                    objResult.IsSuccess = false;
                    objResult.ErrorMessage = "Unknown error occurred while saving data.";

                    reject(objResult);
                }
            });
        }

        Delete(ids) {
            var objVendorLeadTimes = this;

            return new Promise(function (resolve, reject) {
                try {
                    return $.ajax({
                        type: "POST",
                        url: '../WebServices/CommonService.svc/VendorLeadTimes/Delete',
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        data: JSON.stringify({
                            "vendorLeadTimesIDs": ids
                            , "vendorID": parseInt(privateProps.get(objVendorLeadTimes).vendorID)
                        }),
                        success: function (data) {
                            var objResult = new Object();
                            objResult.IsSuccess = true;
                            objResult.ErrorMessage = "";
                            resolve(objResult);
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            var objResult = new Object();
                            objResult.IsSuccess = false;

                            if (objVendorLeadTimes.#IsJsonString(jqXHR.responseText)) {
                                var objError = $.parseJSON(jqXHR.responseText)
                                objResult.ErrorMessage = "Error occurred while deleting data: " + objVendorLeadTimes.#ReplaceNull(objError.ErrorMessage);
                            } else {
                                objResult.ErrorMessage = "Unknown error occurred while deleting data.";
                            }

                            reject(objResult);
                        }
                    });
                } catch (e) {
                    var objResult = new Object();
                    objResult.IsSuccess = false;
                    objResult.ErrorMessage = "Unknown error occurred while deleting data.";

                    reject(objResult);
                }
            });
        }

    #IsJsonString(str) {
        try {
            JSON.parse(str);
        } catch (e) {
            return false;
        }
        return true;
    }

    #ReplaceNull(value) {
        return String(value) === "null" || String(value) === "undefined" ? "" : value.toString().replace(/'/g, "&#39;");
    }
}

    return VendorLeadTimes;
}) ();
