﻿function $ControlFind(element) {
    return document.getElementById(element);
}

function CommaFormatted(nStr) {
    nStr += '';
    x = nStr.split('.');
    x1 = x[0];
    x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + ',' + '$2');
    }
    return x1 + x2;
}

function CheckNumber(cint, e) {
    var k;
    document.all ? k = e.keyCode : k = e.which;

    console.log(k);
    if (cint == 1) {
        if (!(k > 47 && k < 58 || k == 44 || k == 46 || k == 8 || k == 37 || k == 39 || k == 16)) {
            if (e.preventDefault) {
                e.preventDefault();
            }
            else
                e.returnValue = false;
            return false;
        }
    }
    if (cint == 2) {
        if (!(k > 47 && k < 58 || k == 8 || k == 37 || k == 39 || k == 16)) {
            if (e.preventDefault) {
                e.preventDefault();
            }
            else
                e.returnValue = false;
            return false;
        }
    }
    if (cint == 3) {
        if (!(k > 47 && k < 58 || k == 44 || k == 46 || k == 8 || k == 37 || k == 39 || k == 16 || k == 45)) {
            if (e.preventDefault) {
                e.preventDefault();
            }
            else
                e.returnValue = false;
            return false;
        }
    }
}

function TempHideButton() {
    //Commented because we have now diiferent button for Add and Update so no need to hide
    //$ControlFind('btnAdd').style.display = 'none';
    return false;
}

function roundNumber(num, dec) {
    var result = Math.round(num * Math.pow(10, dec)) / Math.pow(10, dec);
    return result;
}

function trim(str, chars) {
    return ltrim(rtrim(str, chars), chars);
}

function ltrim(str, chars) {
    chars = chars || "\\s";
    return str.replace(new RegExp("^[" + chars + "]+", "g"), "");
}

function rtrim(str, chars) {
    chars = chars || "\\s";
    return str.replace(new RegExp("[" + chars + "]+$", "g"), "");
}

function Close() {
    window.close()
    return false;
}

function OpenImage(a) {
    window.open('../Items/frmItemImage.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&ItemCode=' + a, '', 'toolbar=no,titlebar=no,left=300, top=250,width=600,height=500,scrollbars=yes,resizable=yes');
    return false;
}

function AddPrice(a, b, c) {
    $(c).value = a;
}

function DropShip() {
    if ($ControlFind('chkDropShip').checked == true) {
        if ($ControlFind('tdWareHouse') != null) {
            $ControlFind('tdWareHouse').style.display = 'none';
            $ControlFind('tdWareHouseLabel').style.display = 'none';
        }
        //        if ($('trAttributes') != null) {
        //            $('trAttributes').style.display = 'none';
        //        }
    }
    else {
        if ($ControlFind('tdWareHouse') != null) {
            $ControlFind('tdWareHouse').style.display = '';
            $ControlFind('tdWareHouseLabel').style.display = '';
        }
        //        if ($('trAttributes') != null) {
        //            $('trAttributes').style.display = '';
        //        }
    }

}

function getTableCell(id, row, cell) {
    var vRow = $(id).rows[row];
    var i = 0, j = 0;
    for (; i < vRow.childNodes.length; i++) {
        if ("TD" == vRow.childNodes[i].nodeName) {
            if (j == cell) return vRow.childNodes[i];
            j++;
        }
    }
    return null;
}

function blinkIt() {
    if (document.getElementById('hdnmonVal1') != null ) {
        var val1 = parseFloat(document.getElementById('hdnmonVal1').value);
        var val2 = parseFloat(document.getElementById('hdnmonVal2').value);
        if (val1 > val2) {
            s = document.getElementById('lblTotalAmtPastDue');
            if (s != null) {
                s.style.visibility = (s.style.visibility == 'visible') ? 'hidden' : 'visible';
            }
        }
    }
}