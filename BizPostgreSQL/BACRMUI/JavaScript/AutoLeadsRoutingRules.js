/*
Purpose:	Redirects to the Admin Main Screen
Created By: Debasish Tapan Nag
Parameter:	1) None
Return		1) None
*/
function Back()
{
	document.location.href='frmAdminSection.aspx';
}
/*
Purpose:	Validates Data before submit
Created By: Debasish Tapan Nag
Parameter:	1) None
Return		1) None
*/
function ProcessPreSubmitValidation(formName)
{
	if(IsEmpty(formName.txtRuleDesc.value))
	{
		alert('Please enter the Rule Description.');
		return false;
	}
	if(formName.RadioGroup[0].checked || formName.RadioGroup[1].checked || formName.RadioGroup[2].checked)
	{
		if(formName.ddlDistributionList.options.length > 0)
		{
			var sAOIList='';
			var sAOINameList='';
			var sVcDataValue='';
			var sDistributionList='';
			if(formName.RadioGroup[1].checked)
			{
				sVcDataValue = formName.txtIncludedText.value;
				if (sVcDataValue=='')
				{
					alert('Please enter the Included values.');
					return false;
				}
			}
			if(formName.RadioGroup[2].checked)
			{
				for(var iAOIIndex=0;iAOIIndex<formName.ddlAOIList.options.length;iAOIIndex++)
				{
					if(formName.ddlAOIList.options[iAOIIndex].text!="")
					{
						sAOIList += formName.ddlAOIList.options[iAOIIndex].value.split('_')[0]+',';
						sAOINameList += formName.ddlAOIList.options[iAOIIndex].text+',';
					}
				}
				if (sAOIList=='')
				{
					alert('Please select the AOIs.');
					return false;
				}
			}
			for(var iDistributionListIndex=0;iDistributionListIndex<formName.ddlDistributionList.options.length;iDistributionListIndex++)
			{
				if(formName.ddlDistributionList.options[iDistributionListIndex].text!="")
				sDistributionList += formName.ddlDistributionList.options[iDistributionListIndex].value+',';
			}
			formName.hdDistributionList.value = sDistributionList.substring(0,sDistributionList.length-1);
			formName.hdAOIList.value = sAOIList.substring(0,sAOIList.length-1);
			formName.hdAOINameList.value = sAOINameList.substring(0,sAOINameList.length-1);
			formName.hdSave.value = true;
			if(sOrigRuleName != formName.txtRuleDesc.value)
				formName.hdRoutingRuleChangeIndicator.value = 1;
			return true;
		}else
		{
			alert('Please select the Circular Distribution List.');
			return false;
		}
	}else
	{
		alert('Please select the rule parameter.');
		return false;
	}
	return false;
}
/*
Purpose:	Allows editing of an existing Auto Routing Rule in a small iframe called ifRuleDetails
Created By: Debasish Tapan Nag
Parameter:	1) numRoutID: The Routing Rule being edited
Return		1) None
*/function ShowBlankAutoRoutingRules()
{
	//document.frames['ifRuleDetails'].location.href='frmEditRoutingRules.aspx?numRoutID='+document.frmManageAutoRoutingRules.hdNumRoutId.value;
}
/*
Purpose:	Allows editing of an existing Auto Routing Rule in a small iframe called ifRuleDetails
Created By: Debasish Tapan Nag
Parameter:	1) numRoutID: The Routing Rule being edited
Return		1) None
*/function EditAutoRoutingRules(numRoutID)
{
    window.open("../Admin/frmEditRoutingRules.aspx?numRoutID="+numRoutID, "", "width=900,height=400,status=no,scrollbars=yes,left=155,top=160");
	
	//frames['ifRuleDetails'].location.href='frmEditRoutingRules.aspx?numRoutID='+numRoutID;
	return false;
}

/*
Purpose:	Refresh the screen
Created By: Debasish Tapan Nag
Parameter:	1) numRoutID: The Routing Rule being edited
Return		1) None
*/function RefreshAutoRoutingRules(numRoutID)
{
	parent.document.frmManageAutoRoutingRules.hdNumRoutId.value=numRoutID;
	parent.document.frmManageAutoRoutingRules.btnRefresh.click();
}
/*
Purpose:	Raises an click event on the associated button in a iframe called ifRuleDetails
Created By: Debasish Tapan Nag
Parameter:	1) btnName: The name of the button which is clicked
Return		1) None
*/
function  clickAssociatedButton(btnName)
{
	eval("document.frames['ifRuleDetails'].document.forms['frmEditRoutingRules']." + btnName + ".click()");
}
	
/*
Purpose:	Set a hidden variable before the form submits to save the data for default Rule
Created By: Debasish Tapan Nag
Parameter:	1) frmObject: handle to the form object
Return		1) None
*/
function setHiddenAttribute(frmObject)
{
	frmObject.hdNumRoutId.value=-1;
}
/*
Purpose:	displays the requested table row and hides all the others
Created By: Debasish Tapan Nag
Parameter:	1) tr1: TR which has to be displayed
			2) tr2: TR which has to be hidden
			3) tr3: TR which has to be hidden
Return		1) None
*/
function showSelectableOptions(tr1,tr2)
{
    document.getElementById(tr1).style.display='inline';
    document.getElementById(tr2).style.display='none';
}

/*
Purpose:	stores the selected value from the drop down of If Fields (before the form is autoposted)
Created By: Debasish Tapan Nag
Parameter:	1) frmObject: The Form Onject
Return		1) None
*/
function SetSelectedAvailableIFField(frmObject)
{
	frmObject.hdIFFieldSelected.value = frmObject.ddlAvailableFields.options[frmObject.ddlAvailableFields.selectedIndex].value;
}
/*
Purpose:	encodes the string
Created By: Debasish Tapan Nag
Parameter:	1) fBoxString: The source string
Return		1) The htmlencoded string
*/
function encodeMyHtml(fBoxString)
{
     encodedHtml = unescape(fBoxString);
     encodedHtml = encodedHtml.replace(/&/g,"\&amp;");
     encodedHtml = encodedHtml.replace(/\>/g,"&gt;");
     encodedHtml = encodedHtml.replace(/\</g,"&lt;");
     encodedHtml = encodedHtml.replace(/'/g,"\&apos;");
     encodedHtml = encodedHtml.replace(/"/g,"\&quot;");
     return encodedHtml;
}
/*
Purpose:	creates the XML string of the available if fields
Created By: Debasish Tapan Nag
Parameter:	1) None
Return		1) None
*/
function createXMLOfSelectedIfFields(frmElement)
{
	var sXMLOfSelectedIfFields;
	var sXMLOfSelectedIfFieldsValue;
	var sXMLOfSelectedIfFieldsDbName;
	var sXMLOfSelectedIfFieldsItemId;
	var sXMLOfSelectedIfFieldsFormFieldType;
	sXMLOfSelectedIfFields = '<AvailableOptions>';
	for(var iIndex = 0; iIndex < frmElement.ddlAvailableFields.options.length; iIndex++)
	{
		sXMLOfSelectedIfFieldsValue = frmElement.ddlAvailableFields.options[iIndex].value;
		sXMLOfSelectedIfFieldsDbName = sXMLOfSelectedIfFieldsValue.substring(sXMLOfSelectedIfFieldsValue.indexOf('_')+1, sXMLOfSelectedIfFieldsValue.length);
		sXMLOfSelectedIfFieldsItemId = sXMLOfSelectedIfFieldsValue.substring(0 ,sXMLOfSelectedIfFieldsValue.indexOf('_'));
		if (sXMLOfSelectedIfFieldsItemId == '-1')
		{
			sXMLOfSelectedIfFieldsFormFieldType = '';
		}else{
			sXMLOfSelectedIfFieldsFormFieldType = 'L';
		}
		sXMLOfSelectedIfFields += '<Options>';
		sXMLOfSelectedIfFields += '<numFormFieldId> </numFormFieldId>';	
		sXMLOfSelectedIfFields += '<vcFormFieldName>' + encodeMyHtml(frmElement.ddlAvailableFields.options[iIndex].text) + '</vcFormFieldName>';	
		sXMLOfSelectedIfFields += '<vcDataFieldName>' + sXMLOfSelectedIfFieldsDbName + '</vcDataFieldName>';	
		sXMLOfSelectedIfFields += '<vcFormFieldType>' + sXMLOfSelectedIfFieldsFormFieldType + '</vcFormFieldType>';	
		sXMLOfSelectedIfFields += '<numListItemId>' + sXMLOfSelectedIfFieldsItemId + '</numListItemId>';			
		sXMLOfSelectedIfFields += '</Options>';
	}
	sXMLOfSelectedIfFields += '</AvailableOptions>';
	frmElement.hdAvailableIFFieldList.value = sXMLOfSelectedIfFields;
	frmElement.hdSave.value='True';
}
/*
Purpose:	hides the requested table row
Created By: Debasish Tapan Nag
Parameter:	1) tr: TR which has to be hidden
Return		1) None
*/				
function HideTableRow(tr)
{
    document.getElementById(tr).style.display='none';	
}
/*
Purpose:	To move the options upwards from one position in the listbox 
			to another position in the same listbox
Created By: Debasish Tapan Nag
Parameter:	1) tbox: The target list box
Return		1) False
*/
function MoveUp(tbox)
{		
	for(var i=1; i<tbox.options.length; i++)
	{
		if(tbox.options[i].selected && tbox.options[i].value != "") 
		{		
			var SelectedText,SelectedValue;
			SelectedValue = tbox.options[i].value;
			SelectedText = tbox.options[i].text;
			tbox.options[i].value=tbox.options[i-1].value;
			tbox.options[i].text=tbox.options[i-1].text;
			tbox.options[i-1].value=SelectedValue;
			tbox.options[i-1].text=SelectedText;
			tbox.options[i-1].selected=true;
		}
	}
	return false;
}
/*
Purpose:	To move the options downwards from one position in the listbox 
			to another position in the same listbox
Created By: Debasish Tapan Nag
Parameter:	1) tbox: The target list box
Return		1) False
*/
//function MoveDown(tbox,aoiFlag)
//{
//	for(var i=0; i<tbox.options.length-1; i++)
//	{
//		if(tbox.options[i].selected && tbox.options[i].value != "") 
//		{
//			var SelectedText,SelectedValue;
//			SelectedValue = tbox.options[i].value;
//			SelectedText = tbox.options[i].text;
//			tbox.options[i].value=tbox.options[i+1].value;
//			tbox.options[i].text=tbox.options[i+1].text;
//			tbox.options[i+1].value=SelectedValue;
//			tbox.options[i+1].text=SelectedText;
//		}
//	}
//	return false;
//}

function swapRowPosition(iFieldValue, iFirstRow, iSecondFieldValue, iSecondRow, aoiFlag) {
    for (var iIndex = 0; iIndex < arrFieldConfig.length; iIndex++) {
        if (arrFieldConfig[iIndex].numFormFieldId == iFieldValue && arrFieldConfig[iIndex].boolAOIField == aoiFlag) {
            arrFieldConfig[iIndex].intRowNum = iSecondRow;
        }
        if (arrFieldConfig[iIndex].numFormFieldId == iSecondFieldValue && arrFieldConfig[iIndex].boolAOIField == aoiFlag) {
            arrFieldConfig[iIndex].intRowNum = iFirstRow;
        }
    }
}

function MoveDown(tbox, aoiFlag) {
    var SelectedText, SelectedValue, NextSelectedValue, Opt;
    for (var i = 0; i < tbox.options.length - 1; i++) {

        if (tbox.options[i].selected && tbox.options[i].value != "") {


            SelectedValue = tbox.options[i].value;
            SelectedText = tbox.options[i].text;
            NextSelectedValue = tbox.options[i].value;
            //calls function to swap the array
            //swapRowPosition(SelectedValue,i,NextSelectedValue,i+2,aoiFlag);
            Opt = i + 1
            tbox.options[i].value = tbox.options[i + 1].value;
            tbox.options[i].text = tbox.options[i + 1].text;
            tbox.options[i + 1].value = SelectedValue;
            tbox.options[i + 1].text = SelectedText;

        }
    }
    if (Opt <= tbox.options.length) {
        tbox.options[Opt].selected = true;
    }
    return false;
}


sortitems = 0;  // 0-False , 1-True
/*
Purpose:	To move the options from one listbox to another
Created By: Debasish Tapan Nag
Parameter:	1) fbox:The from list box
			2) tbox: The target list box
Return		1) False
*/
function move(fbox,tbox)
{
	for(var i=0; i<fbox.options.length; i++)
	{
		if(fbox.options[i].selected && fbox.options[i].value != "") 
		{
			/// to check for duplicates 
			for (var j=0;j<tbox.options.length;j++)
			{
				if (tbox.options[j].value == fbox.options[i].value)	
				{
					alert("Item is already selected");
					return false;
				}
			}
			var no = new Option();
			no.value = fbox.options[i].value;
			no.text = fbox.options[i].text;
			tbox.options[tbox.options.length] = no;
			fbox.options[i].value = "";
			fbox.options[i].text = "";
		}
	}
	BumpUp(fbox);
	if (sortitems) SortD(tbox);
	return false;
}
/*
Purpose:	To remove the options from one listbox to another
Created By: Debasish Tapan Nag
Parameter:	1) fbox:The from list box
			2) tbox: The target list box
Return		1) False
*/
function remove(fbox,tbox)
{
	for(var i=0; i<fbox.options.length; i++)
	{
		if(fbox.options[i].selected && fbox.options[i].value != "") 
		{
		/// to check for duplicates 
		for (var j=0;j<tbox.options.length;j++)
			{
				if (tbox.options[j].value == fbox.options[i].value)	
				{
					fbox.options[i].value = "";
					fbox.options[i].text = "";
					BumpUp(fbox);
					if (sortitems) SortD(tbox);
					return false;
				}
			}
		
			var no = new Option();
			no.value = fbox.options[i].value;
			no.text = fbox.options[i].text;
			tbox.options[tbox.options.length] = no;
			fbox.options[i].value = "";
			fbox.options[i].text = "";
		}
	}
	BumpUp(fbox);
	if (sortitems) SortD(tbox);
	return false;
}
/*
Purpose:	To shift the option upwards after one is removed from the middle
Created By: Debasish Tapan Nag
Parameter:	1) box:The list box under consideration
Return		1) None
*/
function BumpUp(box) 
{
	for(var i=0; i<box.options.length; i++) 
	{
		if(box.options[i].value == "")  
		{
			for(var j=i; j<box.options.length-1; j++)  
			{
				box.options[j].value = box.options[j+1].value;
				box.options[j].text = box.options[j+1].text;
			}
			var ln = i;
			break;
		}
	}
	if(ln < box.options.length) 
	{
		box.options.length -= 1;
		BumpUp(box);
	}
}
/*
Purpose:	To sort the options in the drop down
Created By: Debasish Tapan Nag
Parameter:	1) box:The list box under consideration
Return		1) None
*/
function SortD(box) 
{
	var temp_opts = new Array();
	var temp = new Object();
	for(var i=0; i<box.options.length; i++)  
	{
		temp_opts[i] = box.options[i];
	}
	for(var x=0; x<temp_opts.length-1; x++)  
	{
		for(var y=(x+1); y<temp_opts.length; y++)  
		{
			if(temp_opts[x].text > temp_opts[y].text) 
			{
				temp = temp_opts[x].text;
				temp_opts[x].text = temp_opts[y].text;
				temp_opts[y].text = temp;
				temp = temp_opts[x].value;
				temp_opts[x].value = temp_opts[y].value;
				temp_opts[y].value = temp;
			}
		}
	}
	for(var i=0; i<box.options.length; i++)  
	{
		box.options[i].value = temp_opts[i].value;
		box.options[i].text = temp_opts[i].text;
	}
}
/*
Purpose:	To set the options in the drop down without removing it from the origional dro dpwn
Created By: Debasish Tapan Nag
Parameter:	1) fbox: The from listbox
			2) tbox: The target list box
Return		1) None
*/
function makeDataTransfer(fbox,tbox,tr)
{
	var iAlreadySelectedIndex = -1;
	if(tbox.selectedIndex > -1)
		iAlreadySelectedIndex = tbox.options[tbox.selectedIndex].value;
	for(var i=0; i<tbox.options.length; i++)
	{
		tbox.options[i].value = "";
		tbox.options[i].text = "";
	}
	BumpUp(tbox);
	for(var i=0; i<fbox.options.length; i++)
	{
		if(fbox.options[i].value != "") 
		{
			var no = new Option();
			no.value = fbox.options[i].value;
			no.text = fbox.options[i].text;
			tbox.options[tbox.options.length] = no;
		}
	}
	if (sortitems) SortD(tbox);
	for(var i=0; i<tbox.options.length; i++)
	{
		if(tbox.options[i].value == iAlreadySelectedIndex)
		{
			tbox.options[i].selected=true;
		}
	}
	HideTableRow(tr)
}
/*
Purpose:	Checks if the field is referenced in a Auto Rule
Created By: Debasish Tapan Nag
Parameter:	1) thisFieldName: The field which is being checked for reference
Return		1) true: Reference Exists/ false: Reference does not exist
*/
function boolReferencesAreClear(fbox)
{
	var sSelectedField = '';
	if(fbox.selectedIndex > -1)
		sSelectedField = fbox.options[fbox.selectedIndex].value;
		
	if (sSelectedField == '-1_vcInterested')
	{
			alert('Cannot remove AOI\'s from available fileds in Auto Rules.');
			return true;
	}
	if(sSelectedField != '')
	{
		var arrReferencedFields = new Array();
		arrReferencedFields = sUsedFieldsInAutoRules.split(',');
		for(iArrIndex=0;iArrIndex<arrReferencedFields.length;iArrIndex++)
		{
			if(sSelectedField.substring(sSelectedField.indexOf('_')+1, sSelectedField.length) == arrReferencedFields[iArrIndex])
			{
				alert('Cannot remove the selected field as it is used in one of the Auto Rules.');
				return true;
			}
		}
	}
	return false;
}