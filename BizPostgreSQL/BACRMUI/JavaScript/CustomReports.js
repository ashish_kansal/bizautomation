////////////////////////////////////////////////////////////////////////////
//////////////////// CUSTOM REPORT DATA TYPE SELECTION//////////////////////
////////////////////////////////////////////////////////////////////////////
/*
Purpose:	Includes/ Excludes selection of the Contact Types
Created By: Debasish Tapan Nag
Parameter:	1) None
Return		1) None
*/
function IncludeExcludeContactTypes()
{
	if(document.getElementById('rbIncludeContacts_1').checked)
	{
		document.getElementById('trContactTypes').style.display='none';
	}else{
		document.getElementById('trContactTypes').style.display='inline';
	}
}
////////////////////////////////////////////////////////////////////////////
///////////////CUSTOM REPORT PARAMETER & CRITERIA SELECTION/////////////////
////////////////////////////////////////////////////////////////////////////
/*
Purpose:	To select all options from a listbox
Created By: Debasish Tapan Nag
Parameter:	1) box:The list box under current context
Return		1) None
*/
function selectAll(box)
{
	for(var i=0; i<box.options.length; i++)
	{
		box.options[i].selected = 1;
	}
}

sortitems = 0;  // 0-False , 1-True
/*
Purpose:	To move the options from one listbox to another
Created By: Debasish Tapan Nag
Parameter:	1) fbox:The from list box
			2) tbox: The target list box
Return		1) False
*/
function move(fbox,tbox)
{
	fBoxSelectedIndex = -1;
	for(var i=0; i<fbox.options.length; i++)
	{
		if(fbox.options[i].selected && fbox.options[i].value != "") 
		{
			/// to check for duplicates 
			for (var j=0;j<tbox.options.length;j++)
			{
				if (tbox.options[j].value == fbox.options[i].value)	
				{
					alert("Item is already selected");
					return false;
				}
			}
			fBoxSelectedIndex = fbox.options[i].value;
			var no = new Option();
			no.value = fbox.options[i].value;
			no.text = fbox.options[i].text;
			tbox.options[tbox.options.length] = no;
			fbox.options[i].value = "";
			fbox.options[i].text = "";
		}
	}
	BumpUp(fbox);
	if (sortitems) SortD(tbox);
	return false;
	
}
/*
Purpose:	To move the options from to the column ordering listbox
Created By: Debasish Tapan Nag
Parameter:	1) fbox:The from list box
			2) tbox: The target list box
Return		1) False
*/
function moveForOrderingColumns(fbox,tbox)
{
	fBoxSelectedIndex = -1;
	for(var i=0; i<fbox.options.length; i++)
	{
		if(fbox.options[i].selected && fbox.options[i].value != "") 
		{
			fBoxSelectedIndex = fbox.options[i].value;
			var no = new Option();
			no.value = fbox.options[i].value;
			no.text = fbox.options[i].text;
			tbox.options[tbox.options.length] = no;
		}
	}
	if (sortitems) SortD(tbox);
	moveForAdvFilter(fbox)
	return false;	
}
/*
Purpose:	To move the options from to the adv filter listbox
Created By: Debasish Tapan Nag
Parameter:	1) fbox:The from list box
			2) tbox: The target list box
Return		1) False
*/
function moveForAdvFilter(fbox)
{
	for(var k=1; k<=4; k++)
	{
		tbox = document.getElementById('ddlField'+k);
		fBoxSelectedIndex = -1;
		for(var i=0; i<fbox.options.length; i++)
		{
			if(fbox.options[i].selected && fbox.options[i].value != "") 
			{
				fBoxSelectedIndex = fbox.options[i].value;
				var no = new Option();
				no.value = fbox.options[i].value;
				no.text = fbox.options[i].text;
				tbox.options[tbox.options.length] = no;
			}
		}
		if (sortitems) SortD(tbox);
	}	
	return false;	
}
/*
Purpose:	To remove the options from the ordering listbox
Created By: Debasish Tapan Nag
Parameter:	1) fbox:The from list box
			2) tbox: The target list box
Return		1) False
*/
function removeFromOrderingColumns(fbox,tbox)
{
	for(var i=0; i<tbox.options.length; i++)
	{
		if(tbox.options[i].selected && tbox.options[i].value != "") 
		{
			for(var j=0; j<fbox.options.length; j++)
			{
				if(fbox.options[j].value == tbox.options[i].value)
				{
					fbox.options[j].value = "";
					fbox.options[j].text = "";		
				}
			}
		}
	}
	BumpUp(fbox);
	removeFromAdvFilter(tbox);
	return false;
}
/*
Purpose:	To remove the options from the adv filter listbox
Created By: Debasish Tapan Nag
Parameter:	1) fbox:The from list box
			2) tbox: The target list box
Return		1) False
*/
function removeFromAdvFilter(tbox)
{
	for(var k=1; k<=4; k++)
	{
		fbox = document.getElementById('ddlField'+k);
		for(var i=0; i<tbox.options.length; i++)
		{
			if(tbox.options[i].selected && tbox.options[i].value != "") 
			{
				for(var j=1; j<fbox.options.length; j++)
				{
					if(fbox.options[j].value == tbox.options[i].value)
					{
						fbox.options[j].value = "";
						fbox.options[j].text = "";		
					}
				}
			}
		}
		BumpUp(fbox);
	}
	return false;
}
/*
Purpose:	To remove the options from one listbox to another
Created By: Debasish Tapan Nag
Parameter:	1) fbox:The from list box
			2) tbox: The target list box
			3) aoiflag: aoi flag
Return		1) False
*/
function remove(fbox,tbox)
{
	fBoxSelectedIndex = -1;
	for(var i=0; i<fbox.options.length; i++)
	{
		if(fbox.options[i].selected && fbox.options[i].value != "") 
		{
			fBoxSelectedIndex = fbox.options[i].value;
			/// to check for duplicates 
			for (var j=0;j<tbox.options.length;j++)
			{
				if (tbox.options[j].value == fbox.options[i].value)	
				{
					fbox.options[i].value = "";
					fbox.options[i].text = "";
					BumpUp(fbox);
					if (sortitems) SortD(tbox);
					return false;
					
					
					//alert("Item is already selected");
					//return false;
				}
			}
			var no = new Option();
			no.value = fbox.options[i].value;
			if(fbox.options[i].text.substring(0,4) != '(*) ')
				no.text = fbox.options[i].text;
			else
				no.text = fbox.options[i].text.substring(4,fbox.options[i].text.length);
			tbox.options[tbox.options.length] = no;
			fbox.options[i].value = "";
			fbox.options[i].text = "";		
		}
	}
	BumpUp(fbox);
	if (sortitems) SortD(tbox);
	return false;
}
/*
Purpose:	To shift the option upwards after one is removed from the middle
Created By: Debasish Tapan Nag
Parameter:	1) box:The list box under consideration
Return		1) None
*/
function BumpUp(box) 
{
	for(var i=0; i<box.options.length; i++) 
	{
		if(box.options[i].value == "")  
		{
			for(var j=i; j<box.options.length-1; j++)  
			{
				box.options[j].value = box.options[j+1].value;
				box.options[j].text = box.options[j+1].text;
			}
			var ln = i;
			break;
		}
	}
	if(ln < box.options.length) 
	{
		box.options.length -= 1;
		BumpUp(box);
	}
}
/*
Purpose:	To sort the options in the drop down
Created By: Debasish Tapan Nag
Parameter:	1) box:The list box under consideration
Return		1) None
*/
function SortD(box) 
{
	var temp_opts = new Array();
	var temp = new Object();
	for(var i=0; i<box.options.length; i++)  
	{
		temp_opts[i] = box.options[i];
	}
	for(var x=0; x<temp_opts.length-1; x++)  
	{
		for(var y=(x+1); y<temp_opts.length; y++)  
		{
			if(temp_opts[x].text > temp_opts[y].text) 
			{
				temp = temp_opts[x].text;
				temp_opts[x].text = temp_opts[y].text;
				temp_opts[y].text = temp;
				temp = temp_opts[x].value;
				temp_opts[x].value = temp_opts[y].value;
				temp_opts[y].value = temp;
			}
		}
	}
	for(var i=0; i<box.options.length; i++)  
	{
		box.options[i].value = temp_opts[i].value;
		box.options[i].text = temp_opts[i].text;
	}
}
/*
Purpose:	To move the options upwards from one position in the listbox 
			to another position in the same listbox
Created By: Debasish Tapan Nag
Parameter:	1) tbox: The target list box
Return		1) False
*/
function MoveUp(tbox)
{		
	for(var i=1; i<tbox.options.length; i++)
	{
		if(tbox.options[i].selected && tbox.options[i].value != "") 
		{		
			var SelectedText,SelectedValue,PrevSelectedValue;
			SelectedValue = tbox.options[i].value;
			SelectedText = tbox.options[i].text;
			PrevSelectedValue = tbox.options[i-1].value;
			tbox.options[i].value=tbox.options[i-1].value;
			tbox.options[i].text=tbox.options[i-1].text;
			tbox.options[i-1].value=SelectedValue;
			tbox.options[i-1].text=SelectedText;
			tbox.options[i-1].selected=true;
		}
	}
	return false;
}
/*
Purpose:	To move the options downwards from one position in the listbox 
			to another position in the same listbox
Created By: Debasish Tapan Nag
Parameter:	1) tbox: The target list box
Return		1) False
*/
function MoveDown(tbox)
{
	for(var i=0; i<tbox.options.length-1; i++)
	{
		if(tbox.options[i].selected && tbox.options[i].value != "") 
		{
			var SelectedText,SelectedValue,NextSelectedValue;
			SelectedValue = tbox.options[i].value;
			SelectedText = tbox.options[i].text;
			tbox.options[i].value=tbox.options[i+1].value;
			tbox.options[i].text=tbox.options[i+1].text;
			tbox.options[i+1].value=SelectedValue;
			tbox.options[i + 1].text = SelectedText;
			tbox.options[i + 1].selected = true;
			break;
		}
	}
	return false;
}
/*
Purpose:	To move the options upwards from one position in the listbox to the extreme top
			to another position in the same listbox
Created By: Debasish Tapan Nag
Parameter:	1) tbox: The target list box
Return		1) False
*/
function moveTop(sourceSelect) {

    if (sourceSelect.length > 1) {
        var options = sourceSelect.options;

        // find which ones are selected...
        var selectedIds = new Array ();
        var index = 0;

        for (var i = 0; i < sourceSelect.length; i++) {
            if (options[i].selected) {
                selectedIds[index] = i;
                index++;
            }
        }

        // Move each selected option up to the topmost available
        // position.  The first one in the selected list gets position 0,
        // second one gets position 1, and so on.
        var selId;
        for (var i = 0; i < selectedIds.length; i++) {
            selId = selectedIds[i];
            // delta is how many positions up to move the selected item
            // to get it into the target position, which is position "i"
            delta = selId-i;
            for (var j = 0 ; j < delta; j++) {         
                privateMoveUp (options, selId-j);
                options[selId-j].selected = false;
                options[(selId-j)-1].selected = true;
                }
        }
        
        sourceSelect.focus ();
    }
}

function moveBottom(sourceSelect) {

    if (sourceSelect.length > 1) {
        var options = sourceSelect.options;

        // find which ones are selected...
        var selectedIds = new Array ();
        var index = 0;

        for (var i = 0; i < sourceSelect.length; i++) {
            if (options[i].selected) {
                selectedIds[index] = i;
                index++;
            }
        }

        // move each selected option down - starting from the end
        // of the selected items array, we'll move each item down to 
        // the next lowest position (i.e., last one in the array ends up at 
        // the very bottom, nth one in the array ends up (array length - n) from
        // the bottom
        // targetPos is position the element is moving to
        var targetPos = sourceSelect.length-1; 
        var selId;
        for (var i = selectedIds.length-1; i >= 0 ; i--) {
            selId = selectedIds[i];
            // delta is how much to move down from the current position to get to the target position
            var delta = targetPos-selId; 
            for (var j = 0 ; j < delta; j++) {
                privateMoveDown (options, selId+j);
                options[selId+j].selected = false;
                options[(selId+j)+1].selected = true;
                }
            targetPos--;
        }
        sourceSelect.focus ();
    }
}


/*
 * Do not call this function directly.
 * As it does NO bounds checking.
 * Please use the moveUp or moveTop calls.
 */
function privateMoveUp (options, index) {
    var newOption = new Option (options[index-1].text, options[index-1].value);
    options[index-1].text = options[index].text;
    options[index-1].value = options[index].value;
    options[index].text = newOption.text;
    options[index].value = newOption.value;
}

/*
 * Do not call this function directly.
 * As it does NO bounds checking.
 * Please use the moveDown or moveBottom calls.
 */
function privateMoveDown (options, index) {
    var newOption = new Option (options[index+1].text, options[index+1].value);
    options[index+1].text = options[index].text;
    options[index+1].value = options[index].value;
    options[index].text = newOption.text;
    options[index].value = newOption.value;
}
/*
Purpose:	Opens the list of teams in a new window
Created By: Debasish Tapan Nag
Parameter:	1) None
Return		1) None
*/
function OpenSelTeam()
{
	window.open('../admin/frmSelectTeams.aspx','','toolbar=no,titlebar=no,left=200, top=300,width=500,height=150,scrollbars=no,resizable=yes')
	return false;
}
/*
Purpose:	Opens the list of territories in a new window
Created By: Debasish Tapan Nag
Parameter:	1) None
Return		1) None
*/
function OpenTerritory()
{
	window.open('../admin/frmSelectTerritories.aspx','','toolbar=no,titlebar=no,left=200, top=300,width=500,height=150,scrollbars=no,resizable=yes')
	return false;
}
////////////////////////////////////////////////////////////////////////////
///////////////////////////////CUSTOM REPORT OPENING////////////////////////
////////////////////////////////////////////////////////////////////////////
/*
Purpose:	Opens the Custom Report Screen in a new popup window
Created By: Debasish Tapan Nag
Parameter:	1) None
Return		1) None
*/
function OpenCustomReport()
{
	window.open('frmGeneratedCustomReport.aspx','','toolbar=no,titlebar=no,left=5, top=300,width=1000,height=400,scrollbars=yes,resizable=yes');
	//return false;
}
////////////////////////////////////////////////////////////////////////////
/////////////////////////////CUSTOM REPORT EXECUTION////////////////////////
////////////////////////////////////////////////////////////////////////////
/*
Purpose:	encodes the string
Created By: Debasish Tapan Nag
Parameter:	1) fBoxString: The source string
Return		1) The htmlencoded string
*/
function encodeMyHtml(fBoxString)
{
     encodedHtml = unescape(fBoxString);
     encodedHtml = encodedHtml.replace(/&/g,"\&amp;");
     encodedHtml = encodedHtml.replace(/\>/g,"&gt;");
     encodedHtml = encodedHtml.replace(/\</g,"&lt;");
     encodedHtml = encodedHtml.replace(/'/g,"\&apos;");
     encodedHtml = encodedHtml.replace(/"/g,"\&quot;");
     return encodedHtml;
}
/*
Purpose:	Defines the class
Created By: Debasish Tapan Nag
Parameter:	1) vcDbFieldName: The field name as represented in the database
			2) vcScrFieldName: The field name as represented in the screen
			3) numFieldGroupID: 1: Organization Details/ 2: Contact Details/ 3: Opportunity Details/ 4: Item Details/ 5: Leads Details
			
*/
function CustomReportConfig(vcDbFieldName,vcScrFieldName,numFieldGroupID,numOrderOfSelection,numOrderOfDisplay,bitSumAggregation,bitAvgAggregation,bitLValueAggregation,bitSValueAggregation,vcAdvFilterOperator,vcAdvFilterValue)
{
	this.vcDbFieldName = vcDbFieldName;
	this.vcScrFieldName = vcScrFieldName;
	this.numFieldGroupID = numFieldGroupID;
	this.numOrderOfSelection = numOrderOfSelection;
	this.numOrderOfDisplay = numOrderOfDisplay;
	this.bitSumAggregation = bitSumAggregation;
	this.bitAvgAggregation = bitAvgAggregation;
	this.bitLValueAggregation = bitLValueAggregation;
	this.bitSValueAggregation = bitSValueAggregation;
	this.vcAdvFilterOperator = vcAdvFilterOperator;
	this.vcAdvFilterValue = vcAdvFilterValue;	
}
/*
Purpose:	To form the columns to summarize
Created By: Debasish Tapan Nag
Parameter:	1) None
Return		1) None
*/
var sCustRptSummationColumns='';
function SetColumnListToSummarize(thisElementObj,numFieldGroupID)
{//Here we do not check if the column is added to summarization or removed (checking is done later)
	if(thisElementObj.value != 'numRecordCount' && sCustRptSummationColumns.indexOf(thisElementObj.value) == -1)
	{
		var sThisElementObjValue = thisElementObj.value + '|' + thisElementObj.name + '|' + numFieldGroupID;
		sCustRptSummationColumns = (sCustRptSummationColumns != '') ? sCustRptSummationColumns + ',' + sThisElementObjValue : sThisElementObjValue;
	}
}
/*
Purpose:	To Reasses the conditions for execution of the Custom Report
Created By: Debasish Tapan Nag
Parameter:	1) None
Return		1) None
*/
function ReAssessReportGenerationCriteria(flagAction)
{	
    //alert('Siva');
	var vcDropDownColumnNames='';//Store the list of columns which are participating in Adv Search
	var iIndexColumns = 0;//Index variable for the elements of adv filter configuration
	for(var iGroupIndex = 1; iGroupIndex <= 4; iGroupIndex++)
	{
		var objDropDownGroup = document.getElementById('ddlField' + iGroupIndex);
		if(objDropDownGroup.selectedIndex > 0)//If the adv filter Drop down selection is made
		{
			if(vcDropDownColumnNames.indexOf(objDropDownGroup.value) > -1)
			{
				alert('The same field cannot be repeated for Advanced Search filteration');
				return false;
			}else{
				vcDropDownColumnNames += ',' + objDropDownGroup.value;
			}
		}
	}

	ReAssessReportColumns();//Call to create a list of columns for the Report
	ReAssessReportColumnOrder();//Call to order the columns according to the display
	SetColumnToSummarize();//Call to set the columns which msut be summarized
	GetAdvFilterCriteria();//Call to Get the Advanced Filters
	SetCustomReportColumnsAndAdvFilters()//Call to create the Custom Report Columns and Adv Filters
	SetCustomReportLayoutAndStdFilters()//Call to create the Custom Report Layout and Standard Filters
	SetCustomReportMasterConfig()//Call to create the Custom Report Master Config
	OpenCustomReport()
	if (vcDbColumnsArray.length == 0)
	{
		alert('Please make selection of fields for Custom Report');
		return false;
	}
	if (flagAction == 'OpenReport')//Requested Opening the Report in a new window
	{
		document.frames['frmCustomReportInitiator'].document.getElementById('hdCustomReportConfig').value = '<CustRptConfig>' + '\n' + vcCustomFilterXMLString + '\n' + '</CustRptConfig>';
		document.frames['frmCustomReportInitiator'].document.getElementById('btnOpenReport').click();
		//DisplayProgressBar();
		return false;
	}else if (flagAction == 'SaveReport')//Save the Report and add to My Reports
	{
		document.getElementById('hdCustomReportConfig').value = '<CustRptConfig>' + '\n' + vcCustomFilterXMLString + '\n' + '</CustRptConfig>';
		return true;
	}
	
}
/*
Purpose:	To Reasses accesses the Columns for saving the Custom Report
Created By: Debasish Tapan Nag
Parameter:	1) None
Return		1) None
*/
var vcDbColumnsArray = new Array();
function ReAssessReportColumns()
{
	var objDropDownGroup;//The object instance of the group drop down
	var iIndexColumns = 0;//Index variable for the elements of column configuration
	vcDbColumnsArray.length = 0;//Reset
	for(var iGroupIndex = 1; iGroupIndex <= 5; iGroupIndex++)
	{
		if(document.getElementById('ddlReportColumnGroup' + iGroupIndex + 'Sel'))//If the Group Drop down is present
		{
			var objDropDownGroup = document.getElementById('ddlReportColumnGroup' + iGroupIndex + 'Sel');
			for (var iIndexGroupColumns = 0; iIndexGroupColumns <= objDropDownGroup.length-1; iIndexGroupColumns++)
			{
				vcDbColumnsArray[iIndexColumns] = new CustomReportConfig(objDropDownGroup.options[iIndexGroupColumns].value,objDropDownGroup.options[iIndexGroupColumns].text,iGroupIndex,iIndexGroupColumns,0,0,0,0,0,0,0);
				iIndexColumns++;
			}
		}
	}
}
/*
Purpose:	To Reasses the order of appearance of columns for the Custom Report
Created By: Debasish Tapan Nag
Parameter:	1) None
Return		1) None
*/
function ReAssessReportColumnOrder()
{
	var objDropDownGroup;//The object instance of the column order drop down
	var objDropDownGroup = document.getElementById('lstReportColumnsOrder');//The columns which are ordered
	for (var iIndexOrderColumns = 0; iIndexOrderColumns <= objDropDownGroup.length-1; iIndexOrderColumns++)
	{
		for (var iIndexGroupColumns = vcDbColumnsArray.length-1; iIndexGroupColumns >= 0; iIndexGroupColumns--)
		{
			if(objDropDownGroup.options[iIndexOrderColumns].value == vcDbColumnsArray[iIndexGroupColumns].vcDbFieldName)
			{
				//alert(vcDbColumnsArray[iIndexGroupColumns].vcDbFieldName + '--' + iIndexGroupColumns)
				vcDbColumnsArray[iIndexGroupColumns].numOrderOfDisplay = iIndexOrderColumns;
				break;
			}
		}
	}
}
/*
Purpose:	To Reasses the summation of columns for the Custom Report
Created By: Debasish Tapan Nag
Parameter:	1) None
Return		1) None
*/
function SetColumnToSummarize()
{
	var boolColumnFound;//Boolean value to indicate if a column has been found or not
	var vcDbSummationColumnsArray = new Array();
	if(sCustRptSummationColumns != '')
	vcDbSummationColumnsArray = sCustRptSummationColumns.split(',');//Create an array of the summation columns
	var vcColumnName = new Array();
	for (var iIndexSummationColumns = vcDbSummationColumnsArray.length-1; iIndexSummationColumns >= 0; iIndexSummationColumns--)
	{
		boolColumnFound = false;
		for (var iIndexGroupColumns = vcDbColumnsArray.length-1; iIndexGroupColumns >= 0; iIndexGroupColumns--)
		{
			vcColumnName = vcDbSummationColumnsArray[iIndexSummationColumns].split('|');
			if(vcDbColumnsArray[iIndexGroupColumns].vcDbFieldName == vcColumnName[0])
			{
				if(document.getElementById('cbSum' + vcColumnName[0]).checked)
					vcDbColumnsArray[iIndexGroupColumns].bitSumAggregation = 1;
				if(document.getElementById('cbAvg' + vcColumnName[0]).checked)
					vcDbColumnsArray[iIndexGroupColumns].bitAvgAggregation = 1;
				if(document.getElementById('cbLValue' + vcColumnName[0]).checked)
					vcDbColumnsArray[iIndexGroupColumns].bitLValueAggregation = 1;
				if(document.getElementById('cbSValue' + vcColumnName[0]).checked)
					vcDbColumnsArray[iIndexGroupColumns].bitSValueAggregation = 1;
				boolColumnFound = true;
				break;
			}
		}
		if(!boolColumnFound)//Column is not there in the existing list of columns
		{
			var bitSumAggregation = 0;
			var bitAvgAggregation = 0;
			var bitLValueAggregation = 0;
			var bitSValueAggregation = 0;
			if(document.getElementById('cbSum' + vcColumnName[0]).checked || document.getElementById('cbAvg' + vcColumnName[0]).checked || document.getElementById('cbLValue' + vcColumnName[0]).checked || document.getElementById('cbSValue' + vcColumnName[0]).checked)//Check if atleast one of the summation is selected
			{
				if(document.getElementById('cbSum' + vcColumnName[0]).checked)
					var bitSumAggregation = 1;
				if(document.getElementById('cbAvg' + vcColumnName[0]).checked)
					var bitAvgAggregation = 1;
				if(document.getElementById('cbLValue' + vcColumnName[0]).checked)
					var bitLValueAggregation = 1;
				if(document.getElementById('cbSValue' + vcColumnName[0]).checked)
					var bitSValueAggregation = 1;
				vcDbColumnsArray[vcDbColumnsArray.length] = new CustomReportConfig(vcColumnName[0],vcColumnName[1],vcColumnName[2],0,vcDbColumnsArray.length-1,bitSumAggregation,bitAvgAggregation,bitLValueAggregation,bitSValueAggregation,0,0);
			}	
		}
	}
}
/*
Purpose:	To get the Advanced Filters for the Custom Report
Created By: Debasish Tapan Nag
Parameter:	1) None
Return		1) None
*/
function GetAdvFilterCriteria()
{
	var objDropDownGroup;//The object instance of the adv filter drop down
	var iIndexColumns = 0;//Index variable for the elements of adv filter configuration
	for(var iGroupIndex = 1; iGroupIndex <= 4; iGroupIndex++)
	{
		var objDropDownGroup = document.getElementById('ddlField' + iGroupIndex);
		if(objDropDownGroup.selectedIndex > 0)//If the adv filter Drop down selection is made
		{
			for (var iIndexGroupColumns = vcDbColumnsArray.length-1; iIndexGroupColumns >= 0; iIndexGroupColumns--)
			{
				if(vcDbColumnsArray[iIndexGroupColumns].vcDbFieldName == objDropDownGroup.value)
				{
					vcDbColumnsArray[iIndexGroupColumns].vcAdvFilterOperator = document.getElementById('ddlCondition' + iGroupIndex).value;
					vcDbColumnsArray[iIndexGroupColumns].vcAdvFilterValue = document.getElementById('txtValue' + iGroupIndex).value;
				}
			}
		}
	}
}
/*
Purpose:	To create an xml string of columns and adv filter criteria for the Custom Report
Created By: Debasish Tapan Nag
Parameter:	1) None
Return		1) None
*/
var vcCustomFilterXMLString;//XML String containing the  Custom Report Criteria
function SetCustomReportColumnsAndAdvFilters()
{
	var vcCustomColXMLString='\n' + '<ColAndAdvFilter>' + '\n';
	for (var iIndexGroupColumns = vcDbColumnsArray.length-1; iIndexGroupColumns >= 0; iIndexGroupColumns--)
	{
		//alert(vcDbColumnsArray[iIndexGroupColumns].vcScrFieldName + '-------' + vcDbColumnsArray[iIndexGroupColumns].numOrderOfDisplay)
		vcCustomColXMLString+='<vcDbRow>' + '\n';
		vcCustomColXMLString+='<vcDbFieldName>' + vcDbColumnsArray[iIndexGroupColumns].vcDbFieldName + '</vcDbFieldName>' + '\n';
		vcCustomColXMLString+='<vcScrFieldName>' + vcDbColumnsArray[iIndexGroupColumns].vcScrFieldName + '</vcScrFieldName>' + '\n';
		vcCustomColXMLString+='<numFieldGroupID>' + vcDbColumnsArray[iIndexGroupColumns].numFieldGroupID + '</numFieldGroupID>' + '\n';
		vcCustomColXMLString+='<numOrderOfSelection>' + vcDbColumnsArray[iIndexGroupColumns].numOrderOfSelection + '</numOrderOfSelection>' + '\n';
		vcCustomColXMLString+='<numOrderOfDisplay>' + vcDbColumnsArray[iIndexGroupColumns].numOrderOfDisplay + '</numOrderOfDisplay>' + '\n';
		vcCustomColXMLString+='<bitSumAggregation>' + vcDbColumnsArray[iIndexGroupColumns].bitSumAggregation + '</bitSumAggregation>' + '\n';
		vcCustomColXMLString+='<bitAvgAggregation>' + vcDbColumnsArray[iIndexGroupColumns].bitAvgAggregation + '</bitAvgAggregation>' + '\n';
		vcCustomColXMLString+='<bitLValueAggregation>' + vcDbColumnsArray[iIndexGroupColumns].bitLValueAggregation + '</bitLValueAggregation>' + '\n';
		vcCustomColXMLString+='<bitSValueAggregation>' + vcDbColumnsArray[iIndexGroupColumns].bitSValueAggregation + '</bitSValueAggregation>' + '\n';
		vcCustomColXMLString+='<vcAdvFilterOperator>' + vcDbColumnsArray[iIndexGroupColumns].vcAdvFilterOperator + '</vcAdvFilterOperator>' + '\n';
		vcCustomColXMLString+='<vcAdvFilterValue>' + encodeMyHtml(vcDbColumnsArray[iIndexGroupColumns].vcAdvFilterValue) + '</vcAdvFilterValue> '+ '\n';
		vcCustomColXMLString+='</vcDbRow>' + '\n';
	}
	vcCustomColXMLString+='</ColAndAdvFilter>' + '\n';
	vcCustomFilterXMLString = vcCustomColXMLString;
}
/*
Purpose:	To create an xml string of layout and standard filters for the Custom Report
Created By: Debasish Tapan Nag
Parameter:	1) None
Return		1) None
*/
function SetCustomReportLayoutAndStdFilters()
{
	var bitStandardFilters, vcFilterOn, bitRecordCountSummation, vcAdvFilterJoining;
	var vcLayoutAndStdFilter='\n' + '<LayoutAndStdFilter>' + '\n';
		vcLayoutAndStdFilter+='<vcReportLayout>Tab</vcReportLayout>' + '\n';
		bitStandardFilters = (document.getElementById('txtFromDateDisplay').value != '' ||  document.getElementById('txtToDateDisplay').value != '') ? 1 : 0;
		vcLayoutAndStdFilter+='<bitStandardFilters>' + bitStandardFilters + '</bitStandardFilters>' + '\n';
		vcLayoutAndStdFilter+='<vcStandardFilterCriteria>' + document.getElementById('ddlStandardFilters').value + '</vcStandardFilterCriteria>' + '\n';
		vcLayoutAndStdFilter+='<vcStandardFilterStartDate>' + document.getElementById('txtFromDateDisplay').value + '</vcStandardFilterStartDate>' + '\n';
		vcLayoutAndStdFilter+='<vcStandardFilterEndDate>' + document.getElementById('txtToDateDisplay').value + '</vcStandardFilterEndDate>' + '\n';
		vcLayoutAndStdFilter+='<vcTeams>' + document.getElementById('hdTeams').value + '</vcTeams>' + '\n';
		vcLayoutAndStdFilter+='<vcTerritories>' + document.getElementById('hdTerritories').value + '</vcTerritories>' + '\n';
		vcFilterOn = (document.getElementById('rdlReportType_0').checked) ? 0 : (document.getElementById('rdlReportType_1').checked) ? 1 : 2;
		vcLayoutAndStdFilter+='<vcFilterOn>' + vcFilterOn + '</vcFilterOn>' + '\n';
		vcAdvFilterJoining = (document.getElementById('rbAdvFilterJoiningCondition_0').checked) ? 'A' : 'R';
		vcLayoutAndStdFilter+='<vcAdvFilterJoining>' + vcAdvFilterJoining + '</vcAdvFilterJoining>' + '\n';
		bitRecordCountSummation = (document.getElementById('cbSumnumRecordCount').checked) ? 1 : 0;
		vcLayoutAndStdFilter+='<bitRecordCountSummation>' + bitRecordCountSummation + '</bitRecordCountSummation>' + '\n';
	vcLayoutAndStdFilter+='</LayoutAndStdFilter>' + '\n';
	vcCustomFilterXMLString = vcLayoutAndStdFilter + vcCustomFilterXMLString;
}
/*
Purpose:	To create an xml string of Report config for the Custom Report
Created By: Debasish Tapan Nag
Parameter:	1) None
Return		1) None
*/
function SetCustomReportMasterConfig()
{
	var vcReportTypeConfig = '<ReportTypeConfig>' + '\n';
	if(document.getElementById('hdReportType').value == 1)
	{
		vcReportTypeConfig += '<ReportType>1</ReportType>' + '\n';
		vcReportTypeConfig += '<ReportName>' + document.getElementById('hdReportName').value + '</ReportName>' + '\n';
		vcReportTypeConfig += '<ReportDesc>' + document.getElementById('hdReportDesc').value + '</ReportDesc>' + '\n';
		var CRMType = (document.getElementById('rbCRMType_0').checked) ? 1 : 2;
		vcReportTypeConfig += '<CRMType>' + CRMType + '</CRMType>' + '\n';
		vcReportTypeConfig += '<RelationShip>' + document.getElementById('ddlRelationShips').value + '</RelationShip>' + '\n';
		var CustomFields = (document.getElementById('cbIncludeCustomFieldsOrg').checked) ? 1 : 0;
		vcReportTypeConfig += '<CustomFields>' + CustomFields + '</CustomFields>' + '\n';
		var IncludeContacts = (document.getElementById('rbIncludeContacts_0').checked) ? 1: 0;
		vcReportTypeConfig += '<IncludeContacts>' + IncludeContacts + '</IncludeContacts>' + '\n';
		ContactType = (IncludeContacts == 1) ? document.getElementById('ddlContactTypes').value : 0;
		vcReportTypeConfig += '<ContactType>' + ContactType + '</ContactType>' + '\n';
	}else if(document.getElementById('hdReportType').value == 2)
	{
		vcReportTypeConfig += '<ReportType>2</ReportType>' + '\n';
		vcReportTypeConfig += '<ReportName>' + document.getElementById('hdReportName').value + '</ReportName>' + '\n';
		vcReportTypeConfig += '<ReportDesc>' + document.getElementById('hdReportDesc').value + '</ReportDesc>' + '\n';
		var CustomFields = (document.getElementById('cbIncludeCustomFieldsOppItems').checked) ? 1 : 0;
		vcReportTypeConfig += '<CustomFields>' + CustomFields + '</CustomFields>' + '\n';
		var OppType = (document.getElementById('rbOppItemType_0').checked) ? 1 : 2;
		vcReportTypeConfig += '<OppType>' + OppType + '</OppType>' + '\n';//1: Sales; 2: Purchase
	}else if(document.getElementById('hdReportType').value == 3)
	{
		vcReportTypeConfig += '<ReportType>3</ReportType>' + '\n';
		vcReportTypeConfig += '<ReportName>' + document.getElementById('hdReportName').value + '</ReportName>' + '\n';
		vcReportTypeConfig += '<ReportDesc>' + document.getElementById('hdReportDesc').value + '</ReportDesc>' + '\n';
	}
	vcReportTypeConfig += '</ReportTypeConfig>' + '\n';
	vcCustomFilterXMLString = vcReportTypeConfig + vcCustomFilterXMLString;
}
////////////////////////////////////////////////////////////////////////////
//////////////////////////////ADD TO MY REPORTS/////////////////////////////
////////////////////////////////////////////////////////////////////////////
/*
Purpose:	Request Name and Description of the Custom Report before its added to My Reports
Created By: Debasish Tapan Nag
Parameter:	1) vcDivName: The id of the div element
Return		1) None
*/
function RequestReportNameAndDesc(vcDivName)
{
	document.getElementById(vcDivName).style.visibility='visible';
	document.getElementById('txtReportName').focus();
	return false;
}
/*
Purpose:	Hides the div element which had asked to name and description of the Report
Created By: Debasish Tapan Nag
Parameter:	1) vcDivName: The id of the div element
Return		1) None
*/
function HideDivElement(vcDivName)
{
	document.getElementById(vcDivName).style.visibility='hidden';
}
/*
Purpose:	Saves the name and the desc of the Csutom report and initiates saving it in the database
Created By: Debasish Tapan Nag
Parameter:	1) vcReportName: The name of the Report
			2) vcReportDesc: The description of the Report
Return		1) None
*/
function makeDataTransfer(vcReportName,vcReportDesc)
{
	if(document.getElementById('txtReportName').value == '')
	{
		alert('Please enter the Report Name before adding it to your \'My Reports\'.');
		return false;
	}
	opener.parent.document.getElementById('hdReportName').value=encodeMyHtml(document.getElementById('txtReportName').value);
	opener.parent.document.getElementById('hdReportDesc').value=encodeMyHtml(document.getElementById('txtReportDescription').value);
	opener.parent.document.getElementById('btnAddToMyReports').click();
	this.close();
}
/*
Purpose:	Opens the interface to display the progress bar
Created By: Debasish Tapan Nag
Parameter:	1) None
Return		1) None
*/
function DisplayProgressBar()
{
	var hndProgressBarWin = window.open('../include/ProgressBar/ProgressBar.aspx','','toolbar=no,titlebar=no,left=400,top=100,width=400,height=150,scrollbars=no,resizable=no');
	hndProgressBarWin.focus();
}