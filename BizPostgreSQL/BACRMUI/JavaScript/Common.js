﻿Date.prototype.getDayName = function (lang) {
    lang = lang && (lang in Date.locale) ? lang : 'en';
    return Date.locale[lang].day_names[this.getDay()];
};

Date.prototype.getDayNameShort = function (lang) {
    lang = lang && (lang in Date.locale) ? lang : 'en';
    return Date.locale[lang].day_names_short[this.getDay()];
};

Date.prototype.getMonthName = function (lang) {
    lang = lang && (lang in Date.locale) ? lang : 'en';
    return Date.locale[lang].month_names[this.getMonth()];
};

Date.prototype.getMonthNameShort = function (lang) {
    lang = lang && (lang in Date.locale) ? lang : 'en';
    return Date.locale[lang].month_names_short[this.getMonth()];
};

Date.locale = {
    en: {
        month_names: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
        month_names_short: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
        day_names: ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
        day_names_short: ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"]
    }
};

function OpenWebsite(e) {
    var t = "";
    if (null != document.getElementById(e) && (t = document.getElementById(e).value), "" != t && "http://" == t.substr(0, 7) && t.length > 7) {
        var n = window.open(t, "", "");
        n.focus()
    }
    return !1
}

function OpenEmail(e, t, n) {
    var o = document.getElementById(e);
    return null != document.getElementById(e) ? 1 == t ? window.open("mailto:" + o.value) : 2 == t && window.open("../contact/frmComposeWindow.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&LsEmail=" + o.value + "&ContID=" + n, "ComposeWindow", "toolbar=no,titlebar=no,top=100,left=100,width=1200,height=645,scrollbars=yes,resizable=yes") : 1 == t ? window.open("mailto:" + e) : 2 == t && window.open("../contact/frmComposeWindow.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&LsEmail=" + e + "&ContID=" + n, "mail", "toolbar=no,titlebar=no,top=100,left=100,width=850,height=550,scrollbars=yes,resizable=yes"), !1
}

function FillAddress() {
    return window.location.reload(!0), !1
}

function formatCurrency(e) {
    e = e.toString().replace(/\$|\,/g, ""), isNaN(e) && (e = "0"), sign = e == (e = Math.abs(e)), e = Math.floor(100 * e + .50000000001), cents = e % 100, e = Math.floor(e / 100).toString(), cents < 10 && (cents = "0" + cents);
    for (var t = 0; t < Math.floor((e.length - (1 + t)) / 3) ; t++) e = e.substring(0, e.length - (4 * t + 3)) + "," + e.substring(e.length - (4 * t + 3));
    return (sign ? "" : "-") + e + "." + cents
}

function currencyToDecimal(e) {
    return e = e.toString().replace(/\$|\,/g, ""), isNaN(e) && (e = "0"), e
}

function toWords(e) {
    if (e = e.toString(), e = e.replace(/[\, ]/g, ""), e != parseFloat(e)) return "not a number";
    var t = e.indexOf(".");
    if (-1 == t && (t = e.length), t > 15) return "too big";
    for (var n = e.split(""), o = "", r = 0, i = 0; t > i; i++) (t - i) % 3 == 2 ? "1" == n[i] ? (o += tn[Number(n[i + 1])] + " ", i++, r = 1) : 0 != n[i] && (o += tw[n[i] - 2] + " ", r = 1) : 0 != n[i] && (o += dg[n[i]] + " ", (t - i) % 3 == 0 && (o += "hundred "), r = 1), (t - i) % 3 == 1 && (r && (o += th[(t - i - 1) / 3] + " "), r = 0);
    if (t != e.length) {
        var l = e.length;
        o += " and ";
        for (var i = t + 1; l > i; i++) o += n[i];
        o += " / 100"
    }
    return o = o.replace(/\s+/g, " "), pad(100, o.charAt(0).toUpperCase() + o.slice(1) + " ", "*")
}

function pad(e, t, n) {
    for (; t.length < e;) t += n;
    return t
}

function CheckIsReconciled() {
    if (1 == document.getElementById("hdnIsReconciled").value) {
        var e;
        return e = "The transaction you are editing has been reconciled. Saving your changes could put you out of balance the next time you try to reconcile. Are you sure you want to modify it?", confirm(e) ? !0 : !1
    }
    return !0
}
var th = ["", "thousand", "million", "billion", "trillion"],
    dg = ["zero", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine"],
    tn = ["ten", "eleven", "twelve", "thirteen", "fourteen", "fifteen", "sixteen", "seventeen", "eighteen", "nineteen"],
    tw = ["twenty", "thirty", "forty", "fifty", "sixty", "seventy", "eighty", "ninety"];