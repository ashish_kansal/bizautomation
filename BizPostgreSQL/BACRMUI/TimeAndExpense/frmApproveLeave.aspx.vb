Imports BACRM.BusinessLogic.Reports
Imports BACRM.BusinessLogic.TimeAndExpense
Imports BACRM.BusinessLogic.Common
Namespace BACRM.UserInterface.Reports
    Partial Public Class frmApproveLeave : Inherits BACRMPage

        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            Try
                If Not IsPostBack Then DisplayRecords()
                
                btnClose.Attributes.Add("onclick", "return Close()")
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Sub DisplayRecords()
            Try
                Dim objPredefinedReports As New PredefinedReports
                objPredefinedReports.DomainID = Session("DomainID")
                objPredefinedReports.UserCntID = GetQueryStringVal( "CntID")
                objPredefinedReports.FromDate = GetQueryStringVal( "fdt")
                objPredefinedReports.ToDate = GetQueryStringVal( "tdt")
                dgLeave.DataSource = objPredefinedReports.GetLeaveDetails
                dgLeave.DataBind()
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Function ReturnName(ByVal SDate) As String
            Try
                Dim strDate As String = ""
                If Not IsDBNull(SDate) Then
                    strDate = FormattedDateFromDate(SDate, Session("DateFormat"))
                    If Format(SDate, "yyyyMMdd") = Format(Now(), "yyyyMMdd") Then
                        strDate = "<font color=red>" & strDate & "</font>"
                    ElseIf Format(SDate, "yyyyMMdd") = Format(DateAdd(DateInterval.Day, 1, Now()), "yyyyMMdd") Then
                        strDate = "<font color=orange>" & strDate & "</font>"
                    End If
                End If
                Return strDate
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Private Sub dgLeave_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgLeave.ItemDataBound
            Try
                If e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.Item Then
                    If e.Item.Cells(3).Text = Session("UserID") Then btnSaveClose.Visible = True
                    CType(e.Item.FindControl("ddlType"), DropDownList).Items.FindByValue(e.Item.Cells(1).Text).Selected = True
                    If e.Item.Cells(2).Text = True Then CType(e.Item.FindControl("chkApprove"), CheckBox).Checked = True
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub btnSaveClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveClose.Click
            Try
                Dim gridrow As DataGridItem
                Dim objTimeExpLeave As New TimeExpenseLeave
                For Each gridrow In dgLeave.Items
                    objTimeExpLeave.CategoryHDRID = gridrow.Cells(0).Text
                    objTimeExpLeave.CategoryType = CType(gridrow.FindControl("ddltype"), DropDownList).SelectedValue
                    objTimeExpLeave.Approved = IIf(CType(gridrow.FindControl("chkApprove"), CheckBox).Checked = True, 1, 0)
                    objTimeExpLeave.UpdateLeaveDtls()
                Next
                Response.Write("<script>self.close();</script>")
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

    End Class
End Namespace