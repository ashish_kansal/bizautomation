<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmAddAmount.aspx.vb" Inherits="BACRM.UserInterface.TimeAndExpense.frmAddAmount" %>
<%@ Register TagPrefix="BizCalendar" TagName="Calendar" src="../include/calandar.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
	<head id="Head1"    runat="server">		
        <link rel="stylesheet" href="~/CSS/master.css" type="text/css" />
       
    <title>Untitled Page</title>
    
    <link rel="stylesheet" type="text/css" href="../CSS/jscal2.css" />
    <link rel="stylesheet" type="text/css" href="../CSS/border-radius.css" />

    <script type="text/javascript" src="../JavaScript/jscal2.js"></script>

    <script type="text/javascript" src="../JavaScript/en.js"></script>  
    <script type="text/javascript" language="javascript" >
        function Close()
        {
            window.opener.form1.btnGo.click()
            window.close()
            return false;
        }
             function Save()
        {
            if (document.form1.ddlUser.value==0)
            {
                alert("Select User");
                document.form1.ddlUser.focus();
                return false;
            }
            if (document.form1.txtAmt.value=='')
            {
                alert("Enter Amount");
                document.form1.txtAmt.focus();
                return false;
            }
             if (document.form1.ddlAmtCat.value==0)
            {
                alert("Select Additional Amount Category");
                document.form1.ddlAmtCat.focus();
                return false;
            }
            if (document.form1.txtDate.value=='')
            {
                alert("Select date");
                return false;
            }
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <br />
    <table width="100%" class="aspTable">
    <tr>
        <td colspan="4" align="right" >
                 <asp:Button ID="btnClose" runat="server" Text="Close" Width="50"  CssClass="button"/>
         </td>
    </tr>
        <tr>
            <td class="normal1" align="right" >
               Select User
            </td>
            <td>
            <asp:DropDownList ID="ddlUser" runat="server" CssClass="signup" Width="150" AutoPostBack="true" ></asp:DropDownList>&nbsp;
            
            
            </td>
             <td class="normal1" align="right">
                 Amount 
            </td>
            <td>
                    <asp:TextBox ID="txtAmt" runat="server"  Width="80" CssClass="signup"></asp:TextBox>
            </td>
            </tr>
            <tr>
            <td class="normal1" align="right">
                Additional Amount Category
            </td>
            <td>
                    <asp:DropDownList ID="ddlAmtCat" runat="server" CssClass="signup" Width="150"></asp:DropDownList>
            </td>
            <td class="normal1" align="right">
            Date
            </td>
            <td>
             <BizCalendar:Calendar ID="cal" runat="server" />
            </td>
        </tr>
      <tr>
              <td align="center"  colspan="4">
                        <asp:Button ID="btnAdd" runat="server" Text="Add" Width="50"  CssClass="button"/>
              </td>
      </tr>
        <tr>
            <td colspan="4">
               <asp:datagrid id="dgAddAmt" runat="server" Width="100%" CssClass="dg" AutoGenerateColumns="False"
																				BorderColor="white">
																				<AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
																				<ItemStyle CssClass="is"></ItemStyle>
																				<HeaderStyle CssClass="hs"></HeaderStyle>
																				<Columns>
																					<asp:BoundColumn DataField="numAddHDRID" Visible="false"></asp:BoundColumn>
																					<asp:BoundColumn DataField="numAmtCategory" Visible="false"></asp:BoundColumn>
																					<asp:BoundColumn HeaderText="Amount Category" DataField="vcData"></asp:BoundColumn>
																					<asp:BoundColumn HeaderText="Amount" DataFormatString="{0:#,##0.00}" DataField="monAmount"></asp:BoundColumn>
																					<asp:TemplateColumn  HeaderText="Date Added">
															                            <ItemTemplate>
																                            <%#Formatdate(DataBinder.Eval(Container.DataItem, "dtAmtAdded"))%>
															                            </ItemTemplate>
														                            </asp:TemplateColumn>
																					<asp:TemplateColumn>
											
												<ItemTemplate>
											
													<asp:Button ID="btnDelete" Runat="server" CssClass="button Delete" Text="X" CommandName="Delete"></asp:Button>
													<asp:LinkButton ID="lnkDelete" Runat="server" Visible="false">
														<font color="#730000">*</font></asp:LinkButton>
												</ItemTemplate>
											</asp:TemplateColumn>
											</Columns>
			 </asp:datagrid>
            </td>
        </tr>
    </table>
    </form>
</body>
</html>

