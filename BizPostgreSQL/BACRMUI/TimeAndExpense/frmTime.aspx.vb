﻿Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Admin
Imports System.Web.Services
Imports BACRM.BusinessLogic.Opportunities
Imports Newtonsoft.Json
Imports BACRM.BusinessLogic.TimeAndExpense
Imports BACRM.BusinessLogic.Accounting

Namespace BACRM.UserInterface.TimeAndExpense

    Public Class frmTime
        Inherits BACRMPage

#Region "Page Events"

        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            Try
                lblError.Text = ""
                divError.Style.Add("display", "none")

                If Not Page.IsPostBack Then
                    GetUserRightsForPage(MODULEID.TimeAndExpense, PAGEID.TIME_AND_EXPENSE)

                    objCommon.sb_FillConEmpFromDBSel(ddlEmployee, Session("DomainID"), 0, 0)

                    If Not ddlEmployee.Items.FindByValue(CCommon.ToLong(Session("UserContactID"))) Is Nothing Then
                        ddlEmployee.Items.FindByValue(CCommon.ToLong(Session("UserContactID"))).Selected = True
                    End If

                    If m_aryRightsForPage(RIGHTSTYPE.ADD) = 0 Then
                        ddlEmployee.Enabled = False
                        btnSave.Visible = False
                        btnSaveClose.Visible = False
                    ElseIf m_aryRightsForPage(RIGHTSTYPE.ADD) = 1 Then
                        ddlEmployee.Enabled = False
                    ElseIf m_aryRightsForPage(RIGHTSTYPE.ADD) = 2 Then
                        ddlEmployee.Enabled = False
                    End If

                    BusinessLogic.Admin.CAdmin.sb_BindClassCombo(ddlClass, Session("DomainID"))

                    LoadServiceItem()
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

#End Region

#Region "Private Methods"

        Private Sub LoadServiceItem()
            Try
                Dim objItem As New BusinessLogic.Item.CItems
                objItem.DomainID = Session("DomainID")
                objItem.Filter = "S"
                objItem.type = 3
                Dim ds As DataSet = objItem.getItemList()

                hdnServiceItemData.Value = JsonConvert.SerializeObject(ds.Tables(0), Formatting.None)
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Shared Function SaveTime(ByVal employeeID As Long, ByVal DivID As Double, ByVal ClassID As Double, ByVal ServiceItemID As Double, ByVal IsBillable As Boolean, ByVal RatePerHour As Double, _
                                       ByVal SOId As Double, ByVal Notes As String, ByVal dtFromDate As Date, ByVal dtToDate As Date) As Boolean
            Try
                Dim objTimeExp As New TimeExpenseLeave
                Dim objOppBizDocs As New OppBizDocs

                Dim CategoryHDRID As Long = 0
                Dim lngProjAccountID As Long = 0
                Dim lngAPAccountID As Long = 0
                Dim lngEmployeeExpenseAccountID As Long = 0

                If IsBillable Then
                    lngAPAccountID = ChartOfAccounting.GetDefaultAccount("AP", HttpContext.Current.Session("DomainID"))
                    If lngAPAccountID = 0 Then
                        Throw New Exception("DEFAULT_AP_NOT_CONFIGURED")
                    End If

                    lngEmployeeExpenseAccountID = ChartOfAccounting.GetDefaultAccount("EP", HttpContext.Current.Session("DomainID")) 'Employee Payeroll Expense
                    If lngEmployeeExpenseAccountID = 0 Then
                        Throw New Exception("DEFAULT_EP_NOT_CONFIGURED")
                    End If
                End If

                objTimeExp = New TimeExpenseLeave
                objTimeExp.CategoryHDRID = CategoryHDRID
                objTimeExp.DivisionID = DivID
                objTimeExp.CategoryID = 1
                objTimeExp.ClassID = ClassID
                objTimeExp.ServiceItemID = ServiceItemID
                objTimeExp.CategoryType = IIf(IsBillable, 1, 2)
                If IsBillable Then
                    objTimeExp.Amount = RatePerHour
                Else
                    objTimeExp.ContractID = 0
                    objTimeExp.Amount = RatePerHour
                End If

                objTimeExp.FromDate = dtFromDate
                objTimeExp.ToDate = dtToDate
                objTimeExp.ProID = 0
                objTimeExp.OppID = SOId
                objTimeExp.Expid = 0
                objTimeExp.StageId = 0
                objTimeExp.CaseID = 0
                objTimeExp.OppBizDocsId = 0
                objTimeExp.Desc = Notes 'IIf(radNonBill.Checked = True, txtNotes.Text, "") '"" '
                objTimeExp.UserCntID = employeeID
                objTimeExp.DomainID = HttpContext.Current.Session("DomainID")
                objTimeExp.ServiceItemID = ServiceItemID
                If SOId > 0 Then
                    objTimeExp.TEType = 1
                Else
                    objTimeExp.TEType = 0
                End If
                Dim ApprovalTrans As New ApprovalConfig
                ApprovalTrans.UserId = employeeID
                If HttpContext.Current.Session("bitApprovalforTImeExpense") = "1" Then
                    If HttpContext.Current.Session("intTimeExpApprovalProcess") = "1" Then
                        objTimeExp.ApprovalStatus = 6
                        ApprovalTrans.chrAction = "CF"
                        ApprovalTrans.ApprovalStatus = 6
                    ElseIf HttpContext.Current.Session("intTimeExpApprovalProcess") = "2" Then
                        objTimeExp.ApprovalStatus = 1
                        ApprovalTrans.chrAction = "CH"
                        ApprovalTrans.ApprovalStatus = 1
                    End If
                    ApprovalTrans.strOutPut = "INPUT"
                    ApprovalTrans.UpdateApprovalTransaction()
                    If ApprovalTrans.strOutPut <> "VALID" Then
                        Throw New Exception("APPROVAL_PROCESS_NOT_CONFIGURED")
                    End If
                Else
                    objTimeExp.ApprovalStatus = 0
                End If

                objTimeExp.CreatedBy = HttpContext.Current.Session("UserContactID")
                objTimeExp.ManageTimeAndExpense()

                If HttpContext.Current.Session("bitApprovalforTImeExpense") <> "1" Then
                    CategoryHDRID = objTimeExp.CategoryHDRID
                    If IsBillable Then
                        Dim objinvoice As New OppBizDocs
                        If ServiceItemID > 0 Then
                            objinvoice.ItemCode = ServiceItemID
                            objinvoice.ItemDesc = Notes
                            objinvoice.Price = RatePerHour
                            objinvoice.UnitHour = DateDiff(DateInterval.Minute, dtFromDate, dtToDate, Microsoft.VisualBasic.FirstDayOfWeek.Monday) / 60
                            objinvoice.OppId = SOId
                            objinvoice.OppBizDocId = 0
                            objinvoice.CategoryHDRID = CategoryHDRID
                            objinvoice.ProID = 0
                            objinvoice.StageId = 0
                            objinvoice.ClassId = ClassID
                            objinvoice.AddItemToExistingInvoice()
                        End If

                        '/******* 23/11/2013 : C.Patel  Don't do any account entries. Manage from Payroll********/
                        If CategoryHDRID > 0 Then
                            'Create Journal Entry
                            Dim totalAmount As Decimal = RatePerHour * DateDiff(DateInterval.Minute, dtFromDate, dtToDate, Microsoft.VisualBasic.FirstDayOfWeek.Monday) / 60
                            Dim payrollType As Short = 1
                            Try
                                Dim objUserAccess As New UserAccess
                                objUserAccess.DomainID = CCommon.ToLong(HttpContext.Current.Session("DomainID"))
                                objUserAccess.UserCntID = CCommon.ToLong(HttpContext.Current.Session("UserContactID"))
                                payrollType = CCommon.ToShort(objUserAccess.GetFieldValue("tintPayrollType"))
                            Catch ex As Exception
                                'DO NOT THROW ERROR 
                            End Try

                            If totalAmount > 0 AndAlso payrollType <> 2 Then
                                Using objTransactionScope As New System.Transactions.TransactionScope(Transactions.TransactionScopeOption.Required, New System.Transactions.TransactionOptions() With {.IsolationLevel = Transactions.IsolationLevel.ReadCommitted, .Timeout = Transactions.TransactionManager.MaximumTimeout})
                                    Dim lngJournalId As Long
                                    Dim objJEHeader As New JournalEntryHeader

                                    With objJEHeader
                                        .JournalId = lngJournalId
                                        .RecurringId = 0
                                        .EntryDate = Date.UtcNow
                                        .Description = "TimeAndExpense"
                                        .Amount = CCommon.ToDecimal(totalAmount)
                                        .CheckId = 0
                                        .CashCreditCardId = 0
                                        .ChartAcntId = 0
                                        .OppId = 0
                                        .OppBizDocsId = 0
                                        .DepositId = 0
                                        .BizDocsPaymentDetId = 0
                                        .IsOpeningBalance = 0
                                        .LastRecurringDate = Date.Now
                                        .NoTransactions = 0
                                        .CategoryHDRID = CategoryHDRID
                                        .ReturnID = 0
                                        .CheckHeaderID = 0
                                        .BillID = 0
                                        .BillPaymentID = 0
                                        .UserCntID = employeeID
                                        .DomainID = HttpContext.Current.Session("DomainID")
                                    End With

                                    lngJournalId = objJEHeader.Save()

                                    'SaveDataToGeneralJournalDetails(lngJournalId)

                                    Dim objJEList As New JournalEntryCollection

                                    Dim objJE As New JournalEntryNew()

                                    objJE.TransactionId = 0
                                    objJE.DebitAmt = totalAmount
                                    objJE.CreditAmt = 0
                                    objJE.ChartAcntId = lngEmployeeExpenseAccountID
                                    objJE.Description = "TimeAndExpense"
                                    objJE.CustomerId = CCommon.ToLong(DivID)
                                    objJE.MainDeposit = 0
                                    objJE.MainCheck = 0
                                    objJE.MainCashCredit = 0
                                    objJE.OppitemtCode = 0
                                    objJE.BizDocItems = ""
                                    objJE.Reference = ""
                                    objJE.PaymentMethod = 0
                                    objJE.Reconcile = False
                                    objJE.CurrencyID = 0
                                    objJE.FltExchangeRate = 0
                                    objJE.TaxItemID = 0
                                    objJE.BizDocsPaymentDetailsId = 0
                                    objJE.ContactID = 0
                                    objJE.ItemID = 0
                                    objJE.ProjectID = 0
                                    objJE.ClassID = CCommon.ToLong(ClassID)
                                    objJE.CommissionID = 0
                                    objJE.ReconcileID = 0
                                    objJE.Cleared = 0
                                    objJE.ReferenceType = 0
                                    objJE.ReferenceID = 0

                                    objJEList.Add(objJE)

                                    objJE = New JournalEntryNew()
                                    objJE.TransactionId = 0
                                    objJE.DebitAmt = 0
                                    objJE.CreditAmt = totalAmount
                                    objJE.ChartAcntId = lngAPAccountID
                                    objJE.Description = "TimeAndExpense"
                                    objJE.CustomerId = CCommon.ToLong(DivID)
                                    objJE.MainDeposit = 0
                                    objJE.MainCheck = 0
                                    objJE.MainCashCredit = 0
                                    objJE.OppitemtCode = 0
                                    objJE.BizDocItems = ""
                                    objJE.Reference = ""
                                    objJE.PaymentMethod = 0
                                    objJE.Reconcile = False
                                    objJE.CurrencyID = 0
                                    objJE.FltExchangeRate = 0
                                    objJE.TaxItemID = 0
                                    objJE.BizDocsPaymentDetailsId = 0
                                    objJE.ContactID = 0
                                    objJE.ItemID = 0
                                    objJE.ProjectID = 0
                                    objJE.ClassID = CCommon.ToLong(ClassID)
                                    objJE.CommissionID = 0
                                    objJE.ReconcileID = 0
                                    objJE.Cleared = 0
                                    objJE.ReferenceType = 0
                                    objJE.ReferenceID = 0
                                    objJEList.Add(objJE)
                                    objJEList.Save(JournalEntryCollection.JournalMode.DeleteUpdateInsert, lngJournalId, HttpContext.Current.Session("DomainID"))
                                    objTransactionScope.Complete()
                                End Using
                            End If
                        End If
                        '/****************************************************/
                    End If
                End If

                Return True
            Catch ex As Exception
                Throw
            End Try
        End Function

        Private Sub DisplayError(ByVal errorMessage As String)
            Try
                lblError.Text = errorMessage
                divError.Style.Add("display", "")
            Catch ex As Exception
                'DO NOT THROW ERROR
            End Try
        End Sub

#End Region

#Region "Web Methods"

        <WebMethod(EnableSession:=True)> _
        Public Shared Function FillHourlyRate(ByVal EmpId As Long) As String
            Try
                Dim userAcess As New UserAccess()
                userAcess.DomainID = HttpContext.Current.Session("DomainID")
                userAcess.UserCntID = EmpId
                Dim dt As DataTable = userAcess.GetHourlyRateForUsers()
                If Not dt Is Nothing AndAlso dt.Rows.Count > 0 Then
                    Return dt.Rows(0)(0)
                Else
                    Return 0
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        <WebMethod(EnableSession:=True)> _
        Public Shared Function FillSalesOrder(ByVal DivID As Long, ByVal DomainID As Long) As String
            Try
                Dim objOpp As New MOpportunity
                Dim dt As DataTable
                objOpp.DomainID = DomainID
                objOpp.DivisionID = DivID
                objOpp.Mode = 3
                dt = objOpp.GetOpenRecords()

                Dim intCount As Integer = 0
                Dim str As New System.Text.StringBuilder
                If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then
                    str.Append("[")

                    For intCount = 0 To dt.Rows.Count - 1
                        str.Append("{")
                        str.Append("""id"" : """ & dt.Rows(intCount)(dt.Columns(0).ColumnName) & """,")
                        str.Append("""value"" : """ & dt.Rows(intCount)(dt.Columns(1).ColumnName) & """")
                        str.Append("},")
                    Next

                    str.Remove(str.ToString.Length - 1, 1)
                    str.Append("]")
                    If dt.Rows.Count <= 0 Then
                        Return ("[]")
                    End If
                    str.Remove(0, 1)
                    str.Remove(str.Length - 1, 1)
                    Return ("[" & str.ToString & "]")

                Else
                    Return ""

                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        <WebMethod(EnableSession:=True)> _
        Public Shared Function SaveEntries(ByVal employeeID As Long, ByVal classID As Long, ByVal timeEntries As String) As String
            Try
                If Not String.IsNullOrEmpty(timeEntries) Then
                    Try
                        Dim dtTimeEntry As DataTable = JsonConvert.DeserializeObject(Of DataTable)(timeEntries)

                        If Not dtTimeEntry Is Nothing AndAlso dtTimeEntry.Rows.Count > 0 Then
                            Using objTransactionScope As New System.Transactions.TransactionScope(Transactions.TransactionScopeOption.Required, New System.Transactions.TransactionOptions() With {.IsolationLevel = Transactions.IsolationLevel.ReadCommitted, .Timeout = Transactions.TransactionManager.MaximumTimeout})
                                For Each dr As DataRow In dtTimeEntry.Rows
                                    Try
                                        Dim entryDate As DateTime = Convert.ToDateTime(dr("Date"))
                                        Dim fromDate As DateTime = New Date(entryDate.Year, entryDate.Month, entryDate.Day, 0, 0, 0)

                                        If CCommon.ToBool(dr("IsAbsent")) Then
                                            Dim toDate As DateTime = fromDate.AddDays(1).AddMilliseconds(-1)

                                            Dim objTimeExp As New TimeExpenseLeave
                                            objTimeExp.DivisionID = 0
                                            objTimeExp.CategoryID = 1
                                            objTimeExp.ClassID = classID
                                            objTimeExp.ServiceItemID = 0
                                            objTimeExp.CategoryType = If(CCommon.ToBool(dr("IsPaidLevel")), 3, 4)
                                            objTimeExp.ContractID = 0
                                            objTimeExp.Amount = 0
                                            objTimeExp.FromDate = fromDate
                                            objTimeExp.ToDate = toDate
                                            objTimeExp.ProID = 0
                                            objTimeExp.OppID = 0
                                            objTimeExp.Expid = 0
                                            objTimeExp.StageId = 0
                                            objTimeExp.CaseID = 0
                                            objTimeExp.OppBizDocsId = 0
                                            objTimeExp.Desc = CCommon.ToString(dr("Notes"))
                                            objTimeExp.UserCntID = employeeID
                                            objTimeExp.DomainID = HttpContext.Current.Session("DomainID")
                                            objTimeExp.ServiceItemID = 0
                                            objTimeExp.TEType = 0

                                            Dim ApprovalTrans As New ApprovalConfig
                                            ApprovalTrans.UserId = employeeID
                                            If HttpContext.Current.Session("bitApprovalforTImeExpense") = "1" Then
                                                If HttpContext.Current.Session("intTimeExpApprovalProcess") = "1" Then
                                                    objTimeExp.ApprovalStatus = 6
                                                    ApprovalTrans.chrAction = "CF"
                                                    ApprovalTrans.ApprovalStatus = 6
                                                ElseIf HttpContext.Current.Session("intTimeExpApprovalProcess") = "2" Then
                                                    objTimeExp.ApprovalStatus = 1
                                                    ApprovalTrans.chrAction = "CH"
                                                    ApprovalTrans.ApprovalStatus = 1
                                                End If
                                                ApprovalTrans.strOutPut = "INPUT"
                                                ApprovalTrans.UpdateApprovalTransaction()
                                                If ApprovalTrans.strOutPut <> "VALID" Then
                                                    Throw New Exception("APPROVAL_PROCESS_NOT_CONFIGURED")
                                                End If
                                            Else
                                                objTimeExp.ApprovalStatus = 0
                                            End If

                                            objTimeExp.CreatedBy = HttpContext.Current.Session("UserContactID")
                                            objTimeExp.ManageTimeAndExpense()
                                        Else
                                            Dim toDate As DateTime = fromDate.AddHours(CCommon.ToDouble(dr("Hours"))).AddMinutes(CCommon.ToDouble(dr("Minutes")))
                                            SaveTime(employeeID, CCommon.ToLong(dr("DivisionID")), classID, CCommon.ToLong(dr("ItemID")), CCommon.ToBool(dr("IsBillable")), CCommon.ToDouble(dr("RatePerHour")), CCommon.ToLong(dr("OppID")), CCommon.ToString(dr("Notes")), fromDate, toDate)
                                        End If
                                    Catch ex As Exception
                                        If ex.Message.Contains("LEAVE_ENTRY_EXISTS") Then
                                            Dim entryDate As DateTime = Convert.ToDateTime(dr("Date"))
                                            Throw New Exception("DAY_IS_MARKED_AS_LEAVE: " & entryDate.AddHours(CCommon.ToInteger(HttpContext.Current.Session("ClientMachineUTCTimeOffset")) * -1).ToString(CCommon.GetDateFormat(), System.Globalization.CultureInfo.InvariantCulture))
                                        Else
                                            Throw
                                        End If
                                    End Try
                                Next

                                objTransactionScope.Complete()
                            End Using
                        End If
                    Catch ex As Exception
                        If ex.Message.Contains("FY_CLOSED") Then
                            Throw New Exception("You can't add time entries once financial year is closed.")
                        ElseIf ex.Message.Contains("DEFAULT_AP_NOT_CONFIGURED") Then
                            Throw New Exception("Please Set Default Accounts Payable Account from ""Administration -> Global Settings -> Accounting ->Default Accounts"".")
                        ElseIf ex.Message.Contains("DEFAULT_EP_NOT_CONFIGURED") Then
                            Throw New Exception("Please Set Default Employee Payroll Expense Account from ""Administration -> Global Settings -> Accounting ->Default Accounts"".")
                        ElseIf ex.Message.Contains("APPROVAL_PROCESS_NOT_CONFIGURED") Then
                            Throw New Exception("Please Set Approval Authority from ""Administration -> Approval Process"".")
                        ElseIf ex.Message.Contains("DAY_IS_MARKED_AS_LEAVE") Then
                            Throw New Exception("You can add time entry once day is marked as level:" & ex.Message.Replace("DAY_IS_MARKED_AS_LEAVE:", ""))
                        Else
                            ExceptionModule.ExceptionPublish(ex, HttpContext.Current.Session("DomainID"), HttpContext.Current.Session("UserContactID"), HttpContext.Current.Request)
                            Throw New Exception(ex.Message)
                        End If
                    End Try
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Function

#End Region

    End Class

End Namespace
