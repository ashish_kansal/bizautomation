Imports BACRM.BusinessLogic.TimeAndExpense
Imports BACRM.BusinessLogic.Common

Namespace BACRM.UserInterface.TimeAndExpense
    Partial Public Class frmAddRoles : Inherits BACRMPage

        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            Try
                If Not IsPostBack Then

                    
                    objCommon.sb_FillComboFromDBwithSel(ddlRole, 26, Session("DomainID"))
                    LoadDetails()
                End If
                btnClose.Attributes.Add("onclick", "return Close()")
                btnAdd.Attributes.Add("onclick", "return Add()")
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Sub LoadDetails()
            Try
                Dim objTimeExp As New TimeExpenseLeave
                objTimeExp.UserCntID = GetQueryStringVal("NameU")
                objTimeExp.byteMode = 2
                dgRoles.DataSource = objTimeExp.ManageUserRoles
                dgRoles.DataBind()
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub dgRoles_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgRoles.ItemCommand
            Try
                If e.CommandName = "Delete" Then
                    Dim objTimeExp As New TimeExpenseLeave
                    objTimeExp.UserCntID = GetQueryStringVal("NameU")
                    objTimeExp.byteMode = 1
                    objTimeExp.RoleID = e.Item.Cells(1).Text
                    objTimeExp.ManageUserRoles()
                    objTimeExp.byteMode = 2
                    dgRoles.DataSource = objTimeExp.ManageUserRoles
                    dgRoles.DataBind()
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
            Try
                Dim objTimeExp As New TimeExpenseLeave
                objTimeExp.UserCntID = GetQueryStringVal("NameU")
                objTimeExp.byteMode = 0
                objTimeExp.RoleID = ddlRole.SelectedValue
                objTimeExp.RolePercentage = txtPercentage.Text.Trim
                objTimeExp.ManageUserRoles()
                objTimeExp.byteMode = 2
                dgRoles.DataSource = objTimeExp.ManageUserRoles
                dgRoles.DataBind()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

    End Class
End Namespace