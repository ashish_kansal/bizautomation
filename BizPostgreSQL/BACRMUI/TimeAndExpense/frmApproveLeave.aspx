<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false"  CodeBehind="frmApproveLeave.aspx.vb" Inherits="BACRM.UserInterface.Reports.frmApproveLeave" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
	<head id="Head1"    runat="server">		
        <link rel="stylesheet" href="~/CSS/master.css" type="text/css" />
       
    <title>Leave Details</title>
    
    <script language="javascript" type="text/javascript" >
    function  Close()
    {
        window.close()
        return false;
    }
    </script>
</head>
<body>
    <form id="form1" runat="server">
     <table width="100%" class="aspTable" style="height:200px">
        <tr>
            <td  valign="top">
    <table width="100%">
        <tr>
            <td align="right">
                <asp:Button ID="btnSaveClose" Visible="false"   runat="server" Text="Save & Close"  CssClass="button"/>
                 <asp:Button ID="btnClose"  runat="server" Text="Close"  CssClass="button" Width="50"/>
            </td>
        </tr>
    </table>
 <asp:DataGrid ID="dgLeave" CssClass="dg" Width="100%" Runat="server" BorderColor="white"
							AutoGenerateColumns="False" AllowSorting="True">
							<AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
							<ItemStyle CssClass="is"></ItemStyle>
							<HeaderStyle CssClass="hs"></HeaderStyle>
							<Columns>
								<asp:BoundColumn DataField="numCategoryHDRID" Visible="false"></asp:BoundColumn>
								<asp:BoundColumn DataField="numType" Visible="false"></asp:BoundColumn>
								<asp:BoundColumn DataField="bitApproved" Visible="false"></asp:BoundColumn>
								<asp:BoundColumn DataField="MGRUserID" Visible="false"></asp:BoundColumn>
								<asp:BoundColumn DataField="Name" SortExpression="Name" HeaderText="<font color=white>Employee</font>"></asp:BoundColumn>
								<asp:TemplateColumn HeaderText="<font color=white>From Date</font>" SortExpression="dtFromDate">
															<ItemTemplate>
																<%#ReturnName(DataBinder.Eval(Container.DataItem, "dtFromDate"))%>
															</ItemTemplate>
														</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="<font color=white>To Date</font>" SortExpression="dtToDate">
															<ItemTemplate>
																<%#ReturnName(DataBinder.Eval(Container.DataItem, "dtToDate"))%>
															</ItemTemplate>
														</asp:TemplateColumn>
								<asp:BoundColumn DataField="NoOfdays"  SortExpression="NoOfdays" HeaderText="<font color=white>No Of Days</font>"></asp:BoundColumn>
								<asp:TemplateColumn HeaderText="Type of Leave">
								    <ItemTemplate>
								        <asp:DropDownList ID="ddlType" runat="server" CssClass="signup" Width="100"> 
								        <asp:ListItem Value="3">Paid </asp:ListItem>
								        <asp:ListItem Value="4">Non-Paid </asp:ListItem>
								        </asp:DropDownList>
								    </ItemTemplate>
								</asp:TemplateColumn>
								<asp:BoundColumn DataField="txtDesc" SortExpression="txtDesc" HeaderText="<font color=white>Description</font>"></asp:BoundColumn>
								<asp:BoundColumn DataField="MGRName" SortExpression="MGRName" HeaderText="<font color=white>Manager</font>"></asp:BoundColumn>
								<asp:TemplateColumn HeaderText="Approved">
								    <ItemTemplate>
								        <asp:CheckBox ID="chkApprove"  runat="server" />
								    </ItemTemplate>
								</asp:TemplateColumn>
							</Columns>
						</asp:DataGrid>
						</td></tr></table>
    </form>
</body>
</html>
