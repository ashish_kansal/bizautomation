Imports BACRM.BusinessLogic.TimeAndExpense
Imports BACRM.BusinessLogic.Common
Imports System.Web.Services
Imports BACRM.BusinessLogic.Opportunities
Imports BACRM.BusinessLogic.Projects
Imports System.Text
Imports BACRM.BusinessLogic.Accounting
Imports BACRM.BusinessLogic.Contacts
Imports BACRM.BusinessLogic.Admin
Imports Telerik.Web.UI

Namespace BACRM.UserInterface.TimeAndExpense
    Partial Public Class frmEmpCal : Inherits BACRMPage

#Region "Memeber Variables"

        Private dtApprovers As DataTable

#End Region

#Region "Page Events"

        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            Try
                'CLEAR ERROR ON RELOAD
                DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ""
                DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "none")

                'CLEAR ALERT MESSAGE
                divMessage.Style.Add("display", "none")
                litMessage.Text = ""

                GetUserRightsForPage(MODULEID.TimeAndExpense, PAGEID.TIME_AND_EXPENSE)
                If Not IsPostBack Then
                    PersistTable.Load(strPageName:="frmEmpCal.aspx")
                    If PersistTable.Count > 0 Then
                        radTimeExpTab.Tabs(CCommon.ToInteger(PersistTable("index"))).Selected = True
                        radTimeExpTab.MultiPage.FindPageViewByID(radTimeExpTab.SelectedTab.PageViewID).Selected = True
                    End If

                    Dim lobjPayroll As New PayrollExpenses
                    lobjPayroll.DomainID = Session("DomainId")
                    hdnPayPeriod.Value = CCommon.ToInteger(lobjPayroll.GetPayPeriod())

                    objCommon.sb_FillComboFromDBwithSel(ddlDepartmentTime, 19, Session("DomainID"))
                    objCommon.sb_FillComboFromDBwithSel(ddlDepartmentExpense, 19, Session("DomainID"))
                    LoadMonth()
                    LoadYear()
                    LoadDateDropDownList(ddlYearTime, ddlMonthTime, ddlDateTime, divPayrollTime, dtFromDateTime, dtToDateTime)
                    LoadDateDropDownList(ddlYearExpense, ddlMonthExpense, ddlDateExpense, divPayrollExpense, dtFromDateExpense, dtToDateExpense)

                    BindDataGrid()
                End If

                If m_aryRightsForPage(RIGHTSTYPE.ADD) = 0 Then
                    btnAddTime.Visible = False
                    btnAddExpense.Visible = False
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

#End Region

#Region "Private Methods"

        Private Sub LoadMonth()
            Try
                Dim Count As Integer
                For Count = 1 To 12
                    ddlMonthTime.Items.Add(New ListItem(MonthName(Count), Count))
                    ddlMonthExpense.Items.Add(New ListItem(MonthName(Count), Count))
                Next
                ddlMonthTime.Items.FindByValue(Month(Now)).Selected = True
                ddlMonthExpense.Items.FindByValue(Month(Now)).Selected = True
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub LoadYear()
            Try
                ddlYearTime.Items.Add(Year(Now()) - 2)
                ddlYearTime.Items.Add(Year(Now()) - 1)
                ddlYearTime.Items.Add(Year(Now()))
                ddlYearTime.Items.Add(Year(Now()) + 1)
                ddlYearTime.Items.Add(Year(Now()) + 2)
                ddlYearTime.Items.Add(Year(Now()) + 3)
                ddlYearTime.Items.Add(Year(Now()) + 4)
                ddlYearTime.Items.FindByValue(Year(Now())).Selected = True

                ddlYearExpense.Items.Add(Year(Now()) - 2)
                ddlYearExpense.Items.Add(Year(Now()) - 1)
                ddlYearExpense.Items.Add(Year(Now()))
                ddlYearExpense.Items.Add(Year(Now()) + 1)
                ddlYearExpense.Items.Add(Year(Now()) + 2)
                ddlYearExpense.Items.Add(Year(Now()) + 3)
                ddlYearExpense.Items.Add(Year(Now()) + 4)
                ddlYearExpense.Items.FindByValue(Year(Now())).Selected = True
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub LoadDateDropDownList(ByRef ddlYear As DropDownList, ByRef ddlMonth As DropDownList, ByRef ddlDate As DropDownList, ByRef divPayroll As HtmlGenericControl, ByRef calFrom As BACRM.Include.calandar, ByRef calTo As BACRM.Include.calandar)
            Try
                Dim lobjPayroll As New PayrollExpenses
                Dim lintPayPeriod As Integer
                Dim strStartDate As String
                Dim strEndDate As String
                Dim dt As New DataTable
                Dim dr As DataRow
                Dim i As Integer
                dt.Columns.Add("Date", GetType(String))
                dt.Columns.Add("DateValue", GetType(String))
                lobjPayroll.DomainID = Session("DomainId")
                lintPayPeriod = CCommon.ToInteger(hdnPayPeriod.Value)
                If lintPayPeriod = 1 Then
                    strStartDate = New Date(ddlYear.SelectedItem.Value, ddlMonth.SelectedItem.Value, 1)
                    strEndDate = New Date(ddlYear.SelectedItem.Value, ddlMonth.SelectedItem.Value, Day(DateAdd("d", -1, DateSerial(ddlYear.SelectedItem.Value, ddlMonth.SelectedItem.Value + 1, 1))))
                    dr = dt.NewRow
                    dr("Date") = strStartDate & " to " & strEndDate
                    dr("DateValue") = strStartDate & " to " & strEndDate
                    dt.Rows.Add(dr)
                    ddlDate.DataSource = dt.DefaultView
                    ddlDate.DataTextField = "DateValue"
                    ddlDate.DataValueField = "Date"
                    ddlDate.DataBind()
                ElseIf lintPayPeriod = 2 Then
                    For i = 0 To 1
                        If i = 0 Then
                            strStartDate = New Date(ddlYear.SelectedItem.Value, ddlMonth.SelectedItem.Value, 1)
                            strEndDate = New Date(ddlYear.SelectedItem.Value, ddlMonth.SelectedItem.Value, 15)
                        Else
                            strStartDate = New Date(ddlYear.SelectedItem.Value, ddlMonth.SelectedItem.Value, 16)
                            strEndDate = New Date(ddlYear.SelectedItem.Value, ddlMonth.SelectedItem.Value, Day(DateAdd("d", -1, DateSerial(ddlYear.SelectedItem.Value, ddlMonth.SelectedItem.Value + 1, 1))))
                        End If
                        dr = dt.NewRow
                        dr("Date") = strStartDate & " to " & strEndDate
                        dr("DateValue") = strStartDate & " to " & strEndDate
                        dt.Rows.Add(dr)
                    Next
                    ddlDate.DataSource = dt.DefaultView
                    ddlDate.DataTextField = "DateValue"
                    ddlDate.DataValueField = "Date"
                    ddlDate.DataBind()
                ElseIf lintPayPeriod = 3 Then
                    For i = 0 To 3
                        If i = 0 Then
                            strStartDate = New Date(ddlYear.SelectedItem.Value, ddlMonth.SelectedItem.Value, 1)
                            Do While CDate(strStartDate).DayOfWeek <> DayOfWeek.Monday
                                strStartDate = CDate(strStartDate).AddDays(1)
                            Loop
                            strEndDate = DateAdd(DateInterval.Day, 6, CDate(strStartDate))
                            dr = dt.NewRow
                            dr("Date") = strStartDate & " to " & strEndDate
                            dr("DateValue") = strStartDate & " to " & strEndDate
                            dt.Rows.Add(dr)
                        Else
                            strStartDate = DateAdd(DateInterval.Day, 1, CDate(strEndDate))
                            strEndDate = DateAdd(DateInterval.Day, 6, CDate(strStartDate))
                            dr = dt.NewRow
                            dr("Date") = strStartDate & " to " & strEndDate
                            dr("DateValue") = strStartDate & " to " & strEndDate
                            dt.Rows.Add(dr)
                        End If
                    Next
                    ddlDate.DataSource = dt.DefaultView
                    ddlDate.DataTextField = "DateValue"
                    ddlDate.DataValueField = "Date"
                    ddlDate.DataBind()
                ElseIf lintPayPeriod = 4 Then
                    For i = 0 To 3
                        If i = 0 Then
                            strStartDate = New Date(ddlYear.SelectedItem.Value, ddlMonth.SelectedItem.Value, 1)
                            Do While CDate(strStartDate).DayOfWeek <> DayOfWeek.Tuesday
                                strStartDate = CDate(strStartDate).AddDays(1)
                            Loop

                            strEndDate = DateAdd(DateInterval.Day, 6, CDate(strStartDate))
                            dr = dt.NewRow
                            dr("Date") = strStartDate & " to " & strEndDate
                            dr("DateValue") = strStartDate & " to " & strEndDate
                            dt.Rows.Add(dr)
                        Else
                            strStartDate = DateAdd(DateInterval.Day, 1, CDate(strEndDate))
                            strEndDate = DateAdd(DateInterval.Day, 6, CDate(strStartDate))
                            dr = dt.NewRow
                            dr("Date") = strStartDate & " to " & strEndDate
                            dr("DateValue") = strStartDate & " to " & strEndDate
                            dt.Rows.Add(dr)
                        End If
                    Next
                    ddlDate.DataSource = dt.DefaultView
                    ddlDate.DataTextField = "DateValue"
                    ddlDate.DataValueField = "Date"
                    ddlDate.DataBind()
                ElseIf lintPayPeriod = 5 Then
                    For i = 0 To 3
                        If i = 0 Then
                            strStartDate = New Date(ddlYear.SelectedItem.Value, ddlMonth.SelectedItem.Value, 1)
                            Do While CDate(strStartDate).DayOfWeek <> DayOfWeek.Wednesday
                                strStartDate = CDate(strStartDate).AddDays(1)
                            Loop

                            strEndDate = DateAdd(DateInterval.Day, 6, CDate(strStartDate))
                            dr = dt.NewRow
                            dr("Date") = strStartDate & " to " & strEndDate
                            dr("DateValue") = strStartDate & " to " & strEndDate
                            dt.Rows.Add(dr)
                        Else
                            strStartDate = DateAdd(DateInterval.Day, 1, CDate(strEndDate))
                            strEndDate = DateAdd(DateInterval.Day, 6, CDate(strStartDate))
                            dr = dt.NewRow
                            dr("Date") = strStartDate & " to " & strEndDate
                            dr("DateValue") = strStartDate & " to " & strEndDate
                            dt.Rows.Add(dr)
                        End If

                    Next
                    ddlDate.DataSource = dt.DefaultView
                    ddlDate.DataTextField = "DateValue"
                    ddlDate.DataValueField = "Date"
                    ddlDate.DataBind()
                ElseIf lintPayPeriod = 6 Then
                    For i = 0 To 3
                        If i = 0 Then
                            strStartDate = New Date(ddlYear.SelectedItem.Value, ddlMonth.SelectedItem.Value, 1)
                            Do While CDate(strStartDate).DayOfWeek <> DayOfWeek.Thursday
                                strStartDate = CDate(strStartDate).AddDays(1)
                            Loop

                            strEndDate = DateAdd(DateInterval.Day, 6, CDate(strStartDate))
                            dr = dt.NewRow
                            dr("Date") = strStartDate & " to " & strEndDate
                            dr("DateValue") = strStartDate & " to " & strEndDate
                            dt.Rows.Add(dr)
                        Else
                            strStartDate = DateAdd(DateInterval.Day, 1, CDate(strEndDate))
                            strEndDate = DateAdd(DateInterval.Day, 6, CDate(strStartDate))
                            dr = dt.NewRow
                            dr("Date") = strStartDate & " to " & strEndDate
                            dr("DateValue") = strStartDate & " to " & strEndDate
                            dt.Rows.Add(dr)
                        End If

                    Next
                    ddlDate.DataSource = dt.DefaultView
                    ddlDate.DataTextField = "DateValue"
                    ddlDate.DataValueField = "Date"
                    ddlDate.DataBind()
                ElseIf lintPayPeriod = 7 Then
                    For i = 0 To 3
                        If i = 0 Then
                            strStartDate = New Date(ddlYear.SelectedItem.Value, ddlMonth.SelectedItem.Value, 1)
                            Do While CDate(strStartDate).DayOfWeek <> DayOfWeek.Friday
                                strStartDate = CDate(strStartDate).AddDays(1)
                            Loop

                            strEndDate = DateAdd(DateInterval.Day, 6, CDate(strStartDate))
                            dr = dt.NewRow
                            dr("Date") = strStartDate & " to " & strEndDate
                            dr("DateValue") = strStartDate & " to " & strEndDate
                            dt.Rows.Add(dr)
                        Else
                            strStartDate = DateAdd(DateInterval.Day, 1, CDate(strEndDate))
                            strEndDate = DateAdd(DateInterval.Day, 6, CDate(strStartDate))
                            dr = dt.NewRow
                            dr("Date") = strStartDate & " to " & strEndDate
                            dr("DateValue") = strStartDate & " to " & strEndDate
                            dt.Rows.Add(dr)
                        End If

                    Next
                    ddlDate.DataSource = dt.DefaultView
                    ddlDate.DataTextField = "DateValue"
                    ddlDate.DataValueField = "Date"
                    ddlDate.DataBind()
                ElseIf lintPayPeriod = 8 Then
                    For i = 0 To 3
                        If i = 0 Then
                            strStartDate = New Date(ddlYear.SelectedItem.Value, ddlMonth.SelectedItem.Value, 1)
                            Do While CDate(strStartDate).DayOfWeek <> DayOfWeek.Saturday
                                strStartDate = CDate(strStartDate).AddDays(1)
                            Loop

                            strEndDate = DateAdd(DateInterval.Day, 6, CDate(strStartDate))
                            dr = dt.NewRow
                            dr("Date") = strStartDate & " to " & strEndDate
                            dr("DateValue") = strStartDate & " to " & strEndDate
                            dt.Rows.Add(dr)
                        Else
                            strStartDate = DateAdd(DateInterval.Day, 1, CDate(strEndDate))
                            strEndDate = DateAdd(DateInterval.Day, 6, CDate(strStartDate))
                            dr = dt.NewRow
                            dr("Date") = strStartDate & " to " & strEndDate
                            dr("DateValue") = strStartDate & " to " & strEndDate
                            dt.Rows.Add(dr)
                        End If

                    Next
                    ddlDate.DataSource = dt.DefaultView
                    ddlDate.DataTextField = "DateValue"
                    ddlDate.DataValueField = "Date"
                    ddlDate.DataBind()
                ElseIf lintPayPeriod = 9 Then
                    For i = 0 To 3
                        If i = 0 Then
                            strStartDate = New Date(ddlYear.SelectedItem.Value, ddlMonth.SelectedItem.Value, 1)
                            Do While CDate(strStartDate).DayOfWeek <> DayOfWeek.Sunday
                                strStartDate = CDate(strStartDate).AddDays(1)
                            Loop

                            strEndDate = DateAdd(DateInterval.Day, 6, CDate(strStartDate))
                            dr = dt.NewRow
                            dr("Date") = strStartDate & " to " & strEndDate
                            dr("DateValue") = strStartDate & " to " & strEndDate
                            dt.Rows.Add(dr)
                        Else
                            strStartDate = DateAdd(DateInterval.Day, 1, CDate(strEndDate))
                            strEndDate = DateAdd(DateInterval.Day, 6, CDate(strStartDate))
                            dr = dt.NewRow
                            dr("Date") = strStartDate & " to " & strEndDate
                            dr("DateValue") = strStartDate & " to " & strEndDate
                            dt.Rows.Add(dr)
                        End If
                    Next
                    ddlDate.DataSource = dt.DefaultView
                    ddlDate.DataTextField = "DateValue"
                    ddlDate.DataValueField = "Date"
                    ddlDate.DataBind()
                ElseIf lintPayPeriod = 10 Then
                    For i = 0 To 1
                        If i = 0 Then
                            strStartDate = New Date(ddlYear.SelectedItem.Value, ddlMonth.SelectedItem.Value, 1)
                            Do While CDate(strStartDate).DayOfWeek <> DayOfWeek.Monday
                                strStartDate = CDate(strStartDate).AddDays(1)
                            Loop

                            strEndDate = DateAdd(DateInterval.Day, 13, CDate(strStartDate))
                            dr = dt.NewRow
                            dr("Date") = strStartDate & " to " & strEndDate
                            dr("DateValue") = strStartDate & " to " & strEndDate
                            dt.Rows.Add(dr)
                        Else
                            strStartDate = DateAdd(DateInterval.Day, 1, CDate(strEndDate))
                            strEndDate = DateAdd(DateInterval.Day, 14, CDate(strStartDate))
                            dr = dt.NewRow
                            dr("Date") = strStartDate & " to " & strEndDate
                            dr("DateValue") = strStartDate & " to " & strEndDate
                            dt.Rows.Add(dr)
                        End If
                    Next
                    ddlDate.DataSource = dt.DefaultView
                    ddlDate.DataTextField = "DateValue"
                    ddlDate.DataValueField = "Date"
                    ddlDate.DataBind()
                ElseIf lintPayPeriod = 11 Then
                    For i = 0 To 1
                        If i = 0 Then
                            strStartDate = New Date(ddlYear.SelectedItem.Value, ddlMonth.SelectedItem.Value, 1)
                            Do While CDate(strStartDate).DayOfWeek <> DayOfWeek.Tuesday
                                strStartDate = CDate(strStartDate).AddDays(1)
                            Loop

                            strEndDate = DateAdd(DateInterval.Day, 13, CDate(strStartDate))
                            dr = dt.NewRow
                            dr("Date") = strStartDate & " to " & strEndDate
                            dr("DateValue") = strStartDate & " to " & strEndDate
                            dt.Rows.Add(dr)
                        Else
                            strStartDate = DateAdd(DateInterval.Day, 1, CDate(strEndDate))
                            strEndDate = DateAdd(DateInterval.Day, 14, CDate(strStartDate))
                            dr = dt.NewRow
                            dr("Date") = strStartDate & " to " & strEndDate
                            dr("DateValue") = strStartDate & " to " & strEndDate
                            dt.Rows.Add(dr)
                        End If
                    Next
                    ddlDate.DataSource = dt.DefaultView
                    ddlDate.DataTextField = "DateValue"
                    ddlDate.DataValueField = "Date"
                    ddlDate.DataBind()
                ElseIf lintPayPeriod = 12 Then
                    For i = 0 To 1
                        If i = 0 Then
                            strStartDate = New Date(ddlYear.SelectedItem.Value, ddlMonth.SelectedItem.Value, 1)
                            Do While CDate(strStartDate).DayOfWeek <> DayOfWeek.Wednesday
                                strStartDate = CDate(strStartDate).AddDays(1)
                            Loop

                            strEndDate = DateAdd(DateInterval.Day, 13, CDate(strStartDate))
                            dr = dt.NewRow
                            dr("Date") = strStartDate & " to " & strEndDate
                            dr("DateValue") = strStartDate & " to " & strEndDate
                            dt.Rows.Add(dr)
                        Else
                            strStartDate = DateAdd(DateInterval.Day, 1, CDate(strEndDate))
                            strEndDate = DateAdd(DateInterval.Day, 14, CDate(strStartDate))
                            dr = dt.NewRow
                            dr("Date") = strStartDate & " to " & strEndDate
                            dr("DateValue") = strStartDate & " to " & strEndDate
                            dt.Rows.Add(dr)
                        End If
                    Next
                    ddlDate.DataSource = dt.DefaultView
                    ddlDate.DataTextField = "DateValue"
                    ddlDate.DataValueField = "Date"
                    ddlDate.DataBind()
                ElseIf lintPayPeriod = 13 Then
                    For i = 0 To 1
                        If i = 0 Then
                            strStartDate = New Date(ddlYear.SelectedItem.Value, ddlMonth.SelectedItem.Value, 1)
                            Do While CDate(strStartDate).DayOfWeek <> DayOfWeek.Thursday
                                strStartDate = CDate(strStartDate).AddDays(1)
                            Loop

                            strEndDate = DateAdd(DateInterval.Day, 13, CDate(strStartDate))
                            dr = dt.NewRow
                            dr("Date") = strStartDate & " to " & strEndDate
                            dr("DateValue") = strStartDate & " to " & strEndDate
                            dt.Rows.Add(dr)
                        Else
                            strStartDate = DateAdd(DateInterval.Day, 1, CDate(strEndDate))
                            strEndDate = DateAdd(DateInterval.Day, 14, CDate(strStartDate))
                            dr = dt.NewRow
                            dr("Date") = strStartDate & " to " & strEndDate
                            dr("DateValue") = strStartDate & " to " & strEndDate
                            dt.Rows.Add(dr)
                        End If
                    Next
                    ddlDate.DataSource = dt.DefaultView
                    ddlDate.DataTextField = "DateValue"
                    ddlDate.DataValueField = "Date"
                    ddlDate.DataBind()
                ElseIf lintPayPeriod = 14 Then
                    For i = 0 To 1
                        If i = 0 Then
                            strStartDate = New Date(ddlYear.SelectedItem.Value, ddlMonth.SelectedItem.Value, 1)
                            Do While CDate(strStartDate).DayOfWeek <> DayOfWeek.Friday
                                strStartDate = CDate(strStartDate).AddDays(1)
                            Loop

                            strEndDate = DateAdd(DateInterval.Day, 13, CDate(strStartDate))
                            dr = dt.NewRow
                            dr("Date") = strStartDate & " to " & strEndDate
                            dr("DateValue") = strStartDate & " to " & strEndDate
                            dt.Rows.Add(dr)
                        Else
                            strStartDate = DateAdd(DateInterval.Day, 1, CDate(strEndDate))
                            strEndDate = DateAdd(DateInterval.Day, 14, CDate(strStartDate))
                            dr = dt.NewRow
                            dr("Date") = strStartDate & " to " & strEndDate
                            dr("DateValue") = strStartDate & " to " & strEndDate
                            dt.Rows.Add(dr)
                        End If
                    Next
                    ddlDate.DataSource = dt.DefaultView
                    ddlDate.DataTextField = "DateValue"
                    ddlDate.DataValueField = "Date"
                    ddlDate.DataBind()
                ElseIf lintPayPeriod = 15 Then
                    For i = 0 To 1
                        If i = 0 Then
                            strStartDate = New Date(ddlYear.SelectedItem.Value, ddlMonth.SelectedItem.Value, 1)
                            Do While CDate(strStartDate).DayOfWeek <> DayOfWeek.Saturday
                                strStartDate = CDate(strStartDate).AddDays(1)
                            Loop

                            strEndDate = DateAdd(DateInterval.Day, 13, CDate(strStartDate))
                            dr = dt.NewRow
                            dr("Date") = strStartDate & " to " & strEndDate
                            dr("DateValue") = strStartDate & " to " & strEndDate
                            dt.Rows.Add(dr)
                        Else
                            strStartDate = DateAdd(DateInterval.Day, 1, CDate(strEndDate))
                            strEndDate = DateAdd(DateInterval.Day, 14, CDate(strStartDate))
                            dr = dt.NewRow
                            dr("Date") = strStartDate & " to " & strEndDate
                            dr("DateValue") = strStartDate & " to " & strEndDate
                            dt.Rows.Add(dr)
                        End If
                    Next
                    ddlDate.DataSource = dt.DefaultView
                    ddlDate.DataTextField = "DateValue"
                    ddlDate.DataValueField = "Date"
                    ddlDate.DataBind()
                ElseIf lintPayPeriod = 16 Then
                    For i = 0 To 1
                        If i = 0 Then
                            strStartDate = New Date(ddlYear.SelectedItem.Value, ddlMonth.SelectedItem.Value, 1)
                            Do While CDate(strStartDate).DayOfWeek <> DayOfWeek.Sunday
                                strStartDate = CDate(strStartDate).AddDays(1)
                            Loop

                            strEndDate = DateAdd(DateInterval.Day, 13, CDate(strStartDate))
                            dr = dt.NewRow
                            dr("Date") = strStartDate & " to " & strEndDate
                            dr("DateValue") = strStartDate & " to " & strEndDate
                            dt.Rows.Add(dr)
                        Else
                            strStartDate = DateAdd(DateInterval.Day, 1, CDate(strEndDate))
                            strEndDate = DateAdd(DateInterval.Day, 14, CDate(strStartDate))
                            dr = dt.NewRow
                            dr("Date") = strStartDate & " to " & strEndDate
                            dr("DateValue") = strStartDate & " to " & strEndDate
                            dt.Rows.Add(dr)
                        End If
                    Next
                    ddlDate.DataSource = dt.DefaultView
                    ddlDate.DataTextField = "DateValue"
                    ddlDate.DataValueField = "Date"
                    ddlDate.DataBind()
                ElseIf lintPayPeriod = 17 Then
                    divPayroll.Visible = False
                    Dim objPayrollExpenses As New PayrollExpenses
                    objPayrollExpenses.DomainID = Session("DomainId")

                    Dim dtDate As DataTable = objPayrollExpenses.GetCommissionDatePayPeriod()
                    If dtDate.Rows.Count > 0 Then
                        calFrom.SelectedDate = CDate(dtDate.Rows(0)("dtFromDate"))
                        calTo.SelectedDate = CDate(dtDate.Rows(0)("dtToDate"))
                    Else
                        calFrom.SelectedDate = Date.Now
                        calTo.SelectedDate = CDate(New DateTime(Date.Now.Year, Date.Now.Month, DateTime.DaysInMonth(Date.Now.Year, Date.Now.Month)))
                    End If
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub BindDataGrid()
            Try
                Dim objTimeAndExpense As New TimeExpenseLeave
                objTimeAndExpense.DomainID = CCommon.ToLong(Session("DomainID"))
                objTimeAndExpense.UserCntID = CCommon.ToLong(Session("UserContactID"))
                objTimeAndExpense.FromDate = CDate(ddlDateTime.SelectedItem.Value.Split("t")(0))
                objTimeAndExpense.ToDate = CDate(ddlDateTime.SelectedItem.Value.Split("t")(1).Substring(1))
                objTimeAndExpense.ClientTimeZoneOffset = CCommon.ToInteger(Session("ClientMachineUTCTimeOffset"))
                objTimeAndExpense.UserRightType = m_aryRightsForPage(RIGHTSTYPE.VIEW)

                If radTimeExpTab.SelectedIndex = 1 Then
                    Dim employeeName As String = "'"
                    Dim teams As String = ""
                    Dim payrollType As Short = 0
                    Dim sortColumnTime As String = ""
                    Dim sortOrderTime As String = ""

                    PersistTable.Load(strPageName:="frmEmpCal.aspx")
                    If PersistTable.Count > 0 Then
                        employeeName = CCommon.ToString(PersistTable("EmployeeNameFilterExpense"))
                        teams = CCommon.ToString(PersistTable("TeamsFilterExpense"))
                        payrollType = CCommon.ToShort(PersistTable("PayrollTypeFilterExpense"))
                        sortColumnTime = CCommon.ToString(PersistTable("index"))
                        sortOrderTime = CCommon.ToString(PersistTable("index"))
                    End If

                    rgExpense.DataSource = objTimeAndExpense.GetExpenseEntries(employeeName, teams, payrollType, sortColumnTime, sortOrderTime)
                    rgExpense.DataBind()
                Else
                    Dim employeeName As String = "'"
                    Dim teams As String = ""
                    Dim payrillType As Short = 0
                    Dim sortColumnTime As String = ""
                    Dim sortOrderTime As String = ""

                    PersistTable.Load(strPageName:="frmEmpCal.aspx")
                    If PersistTable.Count > 0 Then
                        employeeName = CCommon.ToString(PersistTable("EmployeeNameFilterTime"))
                        teams = CCommon.ToString(PersistTable("TeamsFilterTime"))
                        payrillType = CCommon.ToShort(PersistTable("PayrollTypeFilterTime"))
                        sortColumnTime = CCommon.ToString(PersistTable("index"))
                        sortOrderTime = CCommon.ToString(PersistTable("index"))
                    End If


                    rgTime.DataSource = objTimeAndExpense.GetTimeEntries(employeeName, teams, payrillType, sortColumnTime, sortOrderTime)
                    rgTime.DataBind()
                End If

                LoadSummary()
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Private Sub LoadSummary()
            Try
                Dim employeeName As String = "'"
                Dim teams As String = ""
                Dim payrollType As Short = 0
                Dim sortColumnTime As String = ""
                Dim sortOrderTime As String = ""

                Dim objTimeAndExpense As New TimeExpenseLeave
                objTimeAndExpense.DomainID = CCommon.ToLong(Session("DomainID"))
                objTimeAndExpense.FromDate = CDate(ddlDateTime.SelectedItem.Value.Split("t")(0))
                objTimeAndExpense.ToDate = CDate(ddlDateTime.SelectedItem.Value.Split("t")(1).Substring(1))
                objTimeAndExpense.ClientTimeZoneOffset = CCommon.ToInteger(Session("ClientMachineUTCTimeOffset"))

                If radTimeExpTab.SelectedIndex = 1 Then
                    PersistTable.Load(strPageName:="frmEmpCal.aspx")
                    If PersistTable.Count > 0 Then
                        employeeName = CCommon.ToString(PersistTable("EmployeeNameFilterExpense"))
                        teams = CCommon.ToString(PersistTable("TeamsFilterExpense"))
                        payrollType = CCommon.ToShort(PersistTable("PayrollTypeFilterExpense"))
                    End If

                    Dim dt As DataTable = objTimeAndExpense.GetExpenseSummary(employeeName, teams, payrollType)

                    If Not dt Is Nothing AndAlso dt.Rows.Count > 0 Then
                        lblTotalExpense.Text = String.Format(CCommon.GetDataFormatStringWithCurrency(), CCommon.ToDouble(dt.Rows(0)("monTotalExpense")))
                        lblTotalBillableExpense.Text = String.Format(CCommon.GetDataFormatStringWithCurrency(), CCommon.ToDouble(dt.Rows(0)("monTotalBillableExpense")))
                        lblTotalReimbursableExpense.Text = String.Format(CCommon.GetDataFormatStringWithCurrency(), CCommon.ToDouble(dt.Rows(0)("monTotalReimbursableExpense")))
                        lblTotalBillableReimbursableExpense.Text = String.Format(CCommon.GetDataFormatStringWithCurrency(), CCommon.ToDouble(dt.Rows(0)("monTotalBillableReimbursableExpense")))
                    Else
                        lblTotalExpense.Text = String.Format(CCommon.GetDataFormatStringWithCurrency(), 0)
                        lblTotalBillableExpense.Text = String.Format(CCommon.GetDataFormatStringWithCurrency(), 0)
                        lblTotalReimbursableExpense.Text = String.Format(CCommon.GetDataFormatStringWithCurrency(), 0)
                        lblTotalBillableReimbursableExpense.Text = String.Format(CCommon.GetDataFormatStringWithCurrency(), 0)
                    End If
                Else
                    PersistTable.Load(strPageName:="frmEmpCal.aspx")
                    If PersistTable.Count > 0 Then
                        employeeName = CCommon.ToString(PersistTable("EmployeeNameFilterTime"))
                        teams = CCommon.ToString(PersistTable("TeamsFilterTime"))
                        payrollType = CCommon.ToShort(PersistTable("PayrollTypeFilterTime"))
                    End If

                    Dim dt As DataTable = objTimeAndExpense.GetTimeSummary(employeeName, teams, payrollType)

                    If Not dt Is Nothing AndAlso dt.Rows.Count > 0 Then
                        lblTotalHours.Text = CCommon.ToString(dt.Rows(0)("vcTotalTime"))
                        lblTotalBillableHours.Text = CCommon.ToString(dt.Rows(0)("vcTotalBillableTime"))
                        lblTotalNonBillableHours.Text = CCommon.ToString(dt.Rows(0)("vcTotalNonBillableTime"))
                    Else
                        lblTotalHours.Text = "0:00"
                        lblTotalBillableHours.Text = "0:00"
                        lblTotalNonBillableHours.Text = "0:00"
                    End If
                End If
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Private Sub ApproveTimeExpense(ByVal recordD As Long)
            Try
                Dim objApprovalTrans As New ApprovalConfig
                objApprovalTrans.strOutPut = "INPUT"
                objApprovalTrans.numDomainId = Session("DomainID")
                objApprovalTrans.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
                objApprovalTrans.numRecordId = recordD
                objApprovalTrans.numConfigId = recordD
                Dim dt As DataTable = objApprovalTrans.GetTimeExpenseByRecord()

                Dim lngEmpAccountID As Long = 0
                Dim lngEmpAccountIDB As Long = 0
                Dim lngDebtAccountID As Long = 0
                Dim lngProjAccountID As Long = 0
                Dim lngEmpPayrollExpenseAcntID As Long = 0

                Dim SOId As Long = CCommon.ToLong(dt.Rows(0)("numOppId"))
                Dim DivID As Long = CCommon.ToLong(dt.Rows(0)("numDivisionID"))
                Dim ExpID As Long = CCommon.ToLong(dt.Rows(0)("numExpId"))
                Dim ProID As Long = CCommon.ToLong(dt.Rows(0)("numProid"))

                'Save Expense Module
                If CCommon.ToInteger(dt.Rows(0)("numCategory")) = 2 Then
                    If CCommon.ToInteger(dt.Rows(0)("numtype")) = 1 Then
                        If SOId > 0 Then
                            'Validate default Payroll Expense Account
                            lngEmpPayrollExpenseAcntID = ChartOfAccounting.GetDefaultAccount("EP", Session("DomainID")) 'Employee Payeroll Expense
                            lngDebtAccountID = lngEmpPayrollExpenseAcntID
                            lngEmpAccountID = ChartOfAccounting.GetDefaultAccount("AP", Session("DomainID"))
                            If lngDebtAccountID = 0 Then
                                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Alert", "alert('Please Set Default Receiveable Account from ""Administration->Domain Details->Accounting->Default Accounts"".' );", True)
                                Exit Sub
                            End If
                            If lngEmpAccountID = 0 Then
                                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Alert", "alert('Please Set Default Reimbursable Expense Receivable Account from ""Administration->Domain Details->Accounting->Default Accounts"".' );", True)
                                Exit Sub
                            End If
                        End If
                    End If

                    If CCommon.ToInteger(dt.Rows(0)("numtype")) = 2 Then
                        lngEmpPayrollExpenseAcntID = ChartOfAccounting.GetDefaultAccount("RE", Session("DomainID")) 'Employee Payeroll Expense
                        lngDebtAccountID = lngEmpPayrollExpenseAcntID
                        lngEmpAccountID = ChartOfAccounting.GetDefaultAccount("AP", Session("DomainID"))
                        If lngDebtAccountID = 0 Then
                            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Alert", "alert('Please Set Default Reimbursable Expense Receivable Account from ""Administration->Domain Details->Accounting->Default Accounts"".' );", True)
                            Exit Sub
                        End If
                        If lngEmpAccountID = 0 Then
                            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Alert", "alert('Please Set Default Payable Account from ""Administration->Domain Details->Accounting->Default Accounts"".' );", True)
                            Exit Sub
                        End If
                    End If
                    If CCommon.ToInteger(dt.Rows(0)("numtype")) = 6 Then
                        lngEmpPayrollExpenseAcntID = ChartOfAccounting.GetDefaultAccount("RE", Session("DomainID")) 'Employee Payeroll Expense
                        lngDebtAccountID = lngEmpPayrollExpenseAcntID
                        If lngDebtAccountID = 0 Then
                            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Alert", "alert('Please Set Default Receivable Account from ""Administration->Domain Details->Accounting->Default Accounts"".' );", True)
                            Exit Sub
                        End If
                        lngEmpAccountID = ChartOfAccounting.GetDefaultAccount("AP", Session("DomainID"))
                        If lngEmpAccountID = 0 Then
                            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Alert", "alert('Please Set Default Reimbursable Expense Receivable Account from ""Administration->Domain Details->Accounting->Default Accounts"".' );", True)
                            Exit Sub
                        End If
                    End If

                End If

                'Save Authorities for Time Module
                If CCommon.ToInteger(dt.Rows(0)("numCategory")) = 1 Then
                    lngEmpAccountID = ChartOfAccounting.GetDefaultAccount("AP", Session("DomainID"))
                    ' Validate Project Chart of Account 
                    If SOId = 0 AndAlso ProID > 0 Then
                        Dim objProject As New Project
                        objProject.DomainID = Session("DomainID")
                        objProject.ProjectID = ProID
                        objProject.DivisionID = DivID
                        Dim dtRCordProject As DataTable = objProject.GetOpenProject()
                        If dtRCordProject.Rows.Count > 0 Then
                            lngProjAccountID = CCommon.ToLong(dtRCordProject.Rows(0).Item("numAccountId"))
                            lngDebtAccountID = lngProjAccountID
                        End If

                        If lngProjAccountID = 0 Then
                            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "AccountValidation", "alert('Please Set Time & Expense Account for Current Project from ""Administration->Domain Details->Accounting->Accounts for Project""')", True)
                            Exit Sub
                        End If
                    End If

                    If SOId > 0 Then
                        'Validate default Payroll Expense Account
                        lngEmpPayrollExpenseAcntID = ChartOfAccounting.GetDefaultAccount("EP", Session("DomainID")) 'Employee Payeroll Expense
                        lngDebtAccountID = lngEmpPayrollExpenseAcntID
                        If lngEmpPayrollExpenseAcntID = 0 Then
                            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Alert", "alert('Please Set Default Employee Payroll Expense Account from ""Administration->Domain Details->Accounting->Default Accounts"".' );", True)
                            Exit Sub
                        End If
                    End If
                    If ExpID > 0 Then
                        lngDebtAccountID = ExpID
                    End If
                    If lngEmpAccountID = 0 Then
                        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Alert", "alert('Please Set Default Employee Payroll Expense Account from ""Administration->Domain Details->Accounting->Default Accounts"".' );", True)
                        Exit Sub
                    End If
                    If CCommon.ToInteger(dt.Rows(0)("numtype")) = 2 Then
                        lngEmpPayrollExpenseAcntID = ChartOfAccounting.GetDefaultAccount("EP", Session("DomainID")) 'Employee Payeroll Expense
                        lngDebtAccountID = lngEmpPayrollExpenseAcntID
                        If lngEmpPayrollExpenseAcntID = 0 Then
                            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Alert", "alert('Please Set Default Employee Payroll Expense Account from ""Administration->Domain Details->Accounting->Default Accounts"".' );", True)
                            Exit Sub
                        End If
                    End If
                End If
                If Session("bitApprovalforTImeExpense") = "1" Then
                    If Session("intTimeExpApprovalProcess") = "1" Then
                        objApprovalTrans.ApprovalStatus = 0
                        objApprovalTrans.numConfigId = CCommon.ToLong(recordD)
                        objApprovalTrans.chrAction = "UT"
                        objApprovalTrans.UserId = Session("UserContactID")
                        objApprovalTrans.DomainID = Session("DomainID")
                        objApprovalTrans.numModuleId = 1
                        objApprovalTrans.UpdateApprovalTransaction()
                        'Validate Debit and CashCreditCard Account

                        If SOId > 0 Then
                            AddItemtoExistingInvoice(CCommon.ToLong(dt.Rows(0)("numServiceItemID")), CCommon.ToString(dt.Rows(0)("vcItemDesc")), CCommon.ToDouble(dt.Rows(0)("monAmount")), Convert.ToDateTime(dt.Rows(0)("dtFromDate")), Convert.ToDateTime(dt.Rows(0)("dtToDate")), SOId, recordD, ProID, CCommon.ToLong(dt.Rows(0)("numEClassId")))
                        End If
                        SaveExpenseJournal(recordD, CCommon.ToDouble(dt.Rows(0)("monAmount")), Convert.ToDateTime(dt.Rows(0)("dtFromDate")), Convert.ToDateTime(dt.Rows(0)("dtToDate")), CCommon.ToLong(dt.Rows(0)("numUserCntID")), CCommon.ToInteger(dt.Rows(0)("numCategory")), DivID, ProID, CCommon.ToLong(dt.Rows(0)("numEClassId")), lngEmpAccountID, lngDebtAccountID, lngEmpAccountIDB, CCommon.ToInteger(dt.Rows(0)("numtype")))
                        Response.Redirect(HttpContext.Current.Request.Url.ToString(), False)
                    ElseIf Session("intTimeExpApprovalProcess") = "2" Then
                        objApprovalTrans.chrAction = "CH"
                        If Convert.ToInt32(dt.Rows(0)("numApprovalComplete")) = 6 Then
                            objApprovalTrans.ApprovalStatus = 1
                        ElseIf Convert.ToInt32(dt.Rows(0)("numApprovalComplete")) + 1 = 5 Then
                            objApprovalTrans.ApprovalStatus = 0
                        Else
                            objApprovalTrans.ApprovalStatus = Convert.ToInt32(dt.Rows(0)("numApprovalComplete")) + 1
                        End If
                        objApprovalTrans.UpdateApprovalTransaction()
                        If objApprovalTrans.strOutPut = "VALID" Then
                            objApprovalTrans.numConfigId = recordD
                            objApprovalTrans.chrAction = "UT"
                            objApprovalTrans.UserId = Session("UserContactID")
                            objApprovalTrans.DomainID = Session("DomainID")
                            objApprovalTrans.numModuleId = 1
                            objApprovalTrans.UpdateApprovalTransaction()
                        Else
                            objApprovalTrans.numConfigId = recordD
                            objApprovalTrans.ApprovalStatus = 0
                            objApprovalTrans.chrAction = "UT"
                            objApprovalTrans.UserId = Session("UserContactID")
                            objApprovalTrans.DomainID = Session("DomainID")
                            objApprovalTrans.numModuleId = 1
                            objApprovalTrans.UpdateApprovalTransaction()
                            If SOId > 0 Then
                                AddItemtoExistingInvoice(CCommon.ToLong(dt.Rows(0)("numServiceItemID")), CCommon.ToString(dt.Rows(0)("vcItemDesc")), CCommon.ToDouble(dt.Rows(0)("monAmount")), Convert.ToDateTime(dt.Rows(0)("dtFromDate")), Convert.ToDateTime(dt.Rows(0)("dtToDate")), SOId, recordD, ProID, CCommon.ToLong(dt.Rows(0)("numEClassId")))
                            End If
                            SaveExpenseJournal(recordD, CCommon.ToDouble(dt.Rows(0)("monAmount")), Convert.ToDateTime(dt.Rows(0)("dtFromDate")), Convert.ToDateTime(dt.Rows(0)("dtToDate")), CCommon.ToLong(dt.Rows(0)("numUserCntID")), CCommon.ToInteger(dt.Rows(0)("numCategory")), DivID, ProID, CCommon.ToLong(dt.Rows(0)("numEClassId")), lngEmpAccountID, lngDebtAccountID, lngEmpAccountIDB, CCommon.ToInteger(dt.Rows(0)("numtype")))
                        End If
                        Response.Redirect(HttpContext.Current.Request.Url.ToString(), False)
                    End If
                Else
                    objApprovalTrans.ApprovalStatus = 0
                End If
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Private Sub DeclineTimeExpense(ByVal recordD As Long)
            Try
                Dim objApprovalTrans As New ApprovalConfig
                objApprovalTrans.strOutPut = "INPUT"
                objApprovalTrans.ApprovalStatus = -1
                objApprovalTrans.numConfigId = recordD
                objApprovalTrans.chrAction = "UT"
                objApprovalTrans.UserId = Session("UserContactID")
                objApprovalTrans.DomainID = Session("DomainID")
                objApprovalTrans.numModuleId = 1
                objApprovalTrans.UpdateApprovalTransaction()

                Response.Redirect(HttpContext.Current.Request.Url.ToString(), False)
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Private Sub AddItemtoExistingInvoice(ByVal ServiceItemID As Long, ByVal Notes As String, ByVal RatePerHour As Double, ByVal dtFromDate As Date, ByVal dtToDate As Date, ByVal SOId As Long, ByVal CategoryHDRID As Long, ByVal ProID As Long, ByVal ClassID As Long)
            Try
                Dim objinvoice As New OppBizDocs
                If ServiceItemID > 0 Then
                    objinvoice.ItemCode = ServiceItemID
                    objinvoice.ItemDesc = Notes
                    objinvoice.Price = RatePerHour
                    objinvoice.UnitHour = DateDiff(DateInterval.Minute, dtFromDate, dtToDate, Microsoft.VisualBasic.FirstDayOfWeek.Monday) / 60
                    objinvoice.OppId = SOId
                    objinvoice.OppBizDocId = 0
                    objinvoice.CategoryHDRID = CategoryHDRID
                    objinvoice.ProID = ProID
                    objinvoice.StageId = 0
                    objinvoice.ClassId = ClassID
                    objinvoice.AddItemToExistingInvoice()
                End If
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Private Sub SaveExpenseJournal(ByVal CategoryHDRID As Integer, ByVal RatePerHour As Double, ByVal dtFromDate As Date, ByVal dtToDate As Date, ByVal Employee As Long, ByVal category As Long, ByVal DivID As Long, ByVal ProID As Long, ByVal ClassID As Long, ByVal lngEmpAccountID As Long, ByVal lngDebtAccountID As Long, ByVal lngEmpAccountIDB As Long, ByVal Type As Long)
            Try
                If CategoryHDRID > 0 Then
                    Dim totalAmount As Decimal
                    'Create Journal Entry
                    If category = 1 Then
                        totalAmount = RatePerHour * DateDiff(DateInterval.Minute, dtFromDate, dtToDate, Microsoft.VisualBasic.FirstDayOfWeek.Monday) / 60
                    Else
                        totalAmount = RatePerHour
                    End If

                    If totalAmount > 0 Then

                        Using objTransactionScope As New System.Transactions.TransactionScope(Transactions.TransactionScopeOption.Required, New System.Transactions.TransactionOptions() With {.IsolationLevel = Transactions.IsolationLevel.ReadCommitted, .Timeout = Transactions.TransactionManager.MaximumTimeout})
                            Dim lngJournalId As Long
                            Dim objJEHeader As New JournalEntryHeader

                            With objJEHeader
                                .JournalId = lngJournalId
                                .RecurringId = 0
                                .EntryDate = Date.UtcNow
                                .Description = "TimeAndExpense"
                                .Amount = CCommon.ToDecimal(totalAmount)
                                .CheckId = 0
                                .CashCreditCardId = 0
                                .ChartAcntId = 0
                                .OppId = 0
                                .OppBizDocsId = 0
                                .DepositId = 0
                                .BizDocsPaymentDetId = 0
                                .IsOpeningBalance = 0
                                .LastRecurringDate = Date.Now
                                .NoTransactions = 0
                                .CategoryHDRID = CategoryHDRID
                                .ReturnID = 0
                                .CheckHeaderID = 0
                                .BillID = 0
                                .BillPaymentID = 0
                                .UserCntID = CCommon.ToLong(Employee)
                                .DomainID = Session("DomainID")
                            End With

                            lngJournalId = objJEHeader.Save()

                            'SaveDataToGeneralJournalDetails(lngJournalId)

                            Dim objJEList As New JournalEntryCollection

                            Dim objJE As New JournalEntryNew()

                            objJE.TransactionId = 0
                            objJE.DebitAmt = totalAmount
                            objJE.CreditAmt = 0
                            objJE.ChartAcntId = lngDebtAccountID 'If(SOId > 0, lngEmpPayrollExpenseAcntID, lngProjAccountID) If(txtMode.Text = "BD", lngEmpPayrollExpenseAcntID, lngProjAccountID)
                            objJE.Description = "TimeAndExpense"
                            objJE.CustomerId = CCommon.ToLong(DivID)
                            objJE.MainDeposit = 0
                            objJE.MainCheck = 0
                            objJE.MainCashCredit = 0
                            objJE.OppitemtCode = 0
                            objJE.BizDocItems = ""
                            objJE.Reference = ""
                            objJE.PaymentMethod = 0
                            objJE.Reconcile = False
                            objJE.CurrencyID = 0
                            objJE.FltExchangeRate = 0
                            objJE.TaxItemID = 0
                            objJE.BizDocsPaymentDetailsId = 0
                            objJE.ContactID = 0
                            objJE.ItemID = 0
                            objJE.ProjectID = CCommon.ToLong(ProID)
                            objJE.ClassID = CCommon.ToLong(ClassID)
                            objJE.CommissionID = 0
                            objJE.ReconcileID = 0
                            objJE.Cleared = 0
                            objJE.ReferenceType = 0
                            objJE.ReferenceID = 0

                            objJEList.Add(objJE)

                            objJE = New JournalEntryNew()
                            objJE.TransactionId = 0
                            objJE.DebitAmt = 0
                            objJE.CreditAmt = totalAmount
                            objJE.ChartAcntId = lngEmpAccountID
                            objJE.Description = "TimeAndExpense"
                            objJE.CustomerId = CCommon.ToLong(DivID)
                            objJE.MainDeposit = 0
                            objJE.MainCheck = 0
                            objJE.MainCashCredit = 0
                            objJE.OppitemtCode = 0
                            objJE.BizDocItems = ""
                            objJE.Reference = ""
                            objJE.PaymentMethod = 0
                            objJE.Reconcile = False
                            objJE.CurrencyID = 0
                            objJE.FltExchangeRate = 0
                            objJE.TaxItemID = 0
                            objJE.BizDocsPaymentDetailsId = 0
                            objJE.ContactID = 0
                            objJE.ItemID = 0
                            objJE.ProjectID = CCommon.ToLong(ProID)
                            objJE.ClassID = CCommon.ToLong(ClassID)
                            objJE.CommissionID = 0
                            objJE.ReconcileID = 0
                            objJE.Cleared = 0
                            objJE.ReferenceType = 0
                            objJE.ReferenceID = 0
                            objJEList.Add(objJE)


                            objJEList.Save(JournalEntryCollection.JournalMode.DeleteUpdateInsert, lngJournalId, Session("DomainID"))
                            objTransactionScope.Complete()
                        End Using
                    End If
                End If
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Private Sub SavePersistantData(Optional ByVal isClearGridFilter As Boolean = False)
            Try
                PersistTable.Clear()
                PersistTable.Add("index", radTimeExpTab.SelectedIndex)

                If isClearGridFilter Then
                    If radTimeExpTab.SelectedIndex = 0 Then
                        PersistTable.Add("EmployeeNameFilterTime", "")
                        PersistTable.Add("TeamsFilterTime", "")
                        PersistTable.Add("PayrollTypeFilterTime", 0)
                    Else
                        PersistTable.Add("EmployeeNameFilterExpense", "")
                        PersistTable.Add("TeamsFilterExpense", "")
                        PersistTable.Add("PayrollTypeFilterExpense", 0)
                    End If
                Else
                    If rgTime.MasterTableView.GetItems(Telerik.Web.UI.GridItemType.FilteringItem).Length > 0 Then
                        Dim filteringItemTime As Telerik.Web.UI.GridFilteringItem = DirectCast(rgTime.MasterTableView.GetItems(Telerik.Web.UI.GridItemType.FilteringItem)(0), Telerik.Web.UI.GridFilteringItem)

                        If Not filteringItemTime Is Nothing Then
                            PersistTable.Add("EmployeeNameFilterTime", DirectCast(filteringItemTime.FindControl("txtEmployeeHeaderTime"), TextBox).Text)
                            Dim teams As String = ""

                            For Each item As Telerik.Web.UI.RadComboBoxItem In DirectCast(filteringItemTime.FindControl("rcbTeamHeaderTime"), Telerik.Web.UI.RadComboBox).CheckedItems
                                teams = If(teams.Length > 0, ",", "") & item.Value
                            Next

                            PersistTable.Add("TeamsFilterTime", teams)
                            PersistTable.Add("PayrollTypeFilterTime", CCommon.ToShort(DirectCast(filteringItemTime.FindControl("ddlTypeHeaderTime"), DropDownList).SelectedValue))
                        Else
                            PersistTable.Add("EmployeeNameFilterTime", "")
                            PersistTable.Add("TeamsFilterTime", "")
                            PersistTable.Add("PayrollTypeFilterTime", 0)
                        End If
                    End If

                    If rgExpense.MasterTableView.GetItems(Telerik.Web.UI.GridItemType.FilteringItem).Length > 0 Then
                        Dim filteringItemExpense As Telerik.Web.UI.GridFilteringItem = DirectCast(rgExpense.MasterTableView.GetItems(Telerik.Web.UI.GridItemType.FilteringItem)(0), Telerik.Web.UI.GridFilteringItem)
                        If Not filteringItemExpense Is Nothing Then
                            PersistTable.Add("EmployeeNameFilterExpense", DirectCast(filteringItemExpense.FindControl("txtEmployeeHeaderExpense"), TextBox).Text)
                            Dim teams As String = ""

                            For Each item As Telerik.Web.UI.RadComboBoxItem In DirectCast(filteringItemExpense.FindControl("rcbTeamHeaderExpense"), Telerik.Web.UI.RadComboBox).CheckedItems
                                teams = If(teams.Length > 0, ",", "") & item.Value
                            Next

                            PersistTable.Add("TeamsFilterExpense", teams)
                            PersistTable.Add("PayrollTypeFilterExpense", CCommon.ToShort(DirectCast(filteringItemExpense.FindControl("ddlTypeHeaderExpense"), DropDownList).SelectedValue))
                        Else
                            PersistTable.Add("EmployeeNameFilterExpense", "")
                            PersistTable.Add("TeamsFilterExpense", "")
                            PersistTable.Add("PayrollTypeFilterExpense", 0)
                        End If
                    End If
                End If

                PersistTable.Save(strPageName:="frmEmpCal.aspx")
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Private Sub DisplayError(ByVal exception As String)
            Try
                DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = exception
                DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
                DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Focus()

                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "SlectTab", "selectTab('');", True)
            Catch ex As Exception

            End Try
        End Sub

#End Region

#Region "Event Handlers"

        Protected Sub radTimeExpTab_TabClick(sender As Object, e As Telerik.Web.UI.RadTabStripEventArgs)
            Try
                BindDataGrid()
                SavePersistantData()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Protected Sub rgTime_DetailTableDataBind(sender As Object, e As Telerik.Web.UI.GridDetailTableDataBindEventArgs)
            Try
                Dim parentItem As Telerik.Web.UI.GridDataItem = CType(e.DetailTableView.ParentItem, Telerik.Web.UI.GridDataItem)

                Dim objTimeAndExpense As New TimeExpenseLeave
                objTimeAndExpense.DomainID = CCommon.ToLong(Session("DomainID"))
                objTimeAndExpense.UserCntID = CCommon.ToLong(parentItem.GetDataKeyValue("numUserCntID"))
                objTimeAndExpense.FromDate = CDate(ddlDateTime.SelectedItem.Value.Split("t")(0))
                objTimeAndExpense.ToDate = CDate(ddlDateTime.SelectedItem.Value.Split("t")(1).Substring(1))
                objTimeAndExpense.ClientTimeZoneOffset = CCommon.ToInteger(Session("ClientMachineUTCTimeOffset"))
                e.DetailTableView.DataSource = objTimeAndExpense.GetTimeEntriesDetail()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Protected Sub rgTime_ItemDataBound(sender As Object, e As Telerik.Web.UI.GridItemEventArgs)
            Try
                If e.Item.ItemType = GridItemType.Header Then
                    Dim objApprovalConfig As New ApprovalConfig
                    objApprovalConfig.chrAction = ""
                    objApprovalConfig.numDomainId = Session("DomainId")
                    objApprovalConfig.numModuleId = 1
                    dtApprovers = objApprovalConfig.GetAllUsers()
                End If

                If e.Item.ItemType = Telerik.Web.UI.GridItemType.FilteringItem Then
                    If objCommon Is Nothing Then
                        objCommon = New CCommon
                    End If

                    PersistTable.Load(strPageName:="frmEmpCal.aspx")
                    DirectCast(e.Item.FindControl("txtEmployeeHeaderTime"), TextBox).Text = CCommon.ToString(PersistTable("EmployeeNameFilterTime"))
                    DirectCast(e.Item.FindControl("ddlTypeHeaderTime"), DropDownList).SelectedValue = CCommon.ToShort(PersistTable("PayrollTypeFilterTime"))

                    Dim rcbTeamHeaderTime As RadComboBox = DirectCast(e.Item.FindControl("rcbTeamHeaderTime"), RadComboBox)
                    objCommon.sb_FillComboFromDBwithSel(rcbTeamHeaderTime, 35, Session("DomainID"))
                    rcbTeamHeaderTime.Items(0).Remove()
                    For Each item As RadComboBoxItem In rcbTeamHeaderTime.Items
                        If CCommon.ToString(PersistTable("TeamsFilterTime")).Split(",").Contains(item.Value) Then
                            item.Checked = True
                        End If
                    Next
                End If

                If (e.Item.ItemType = Telerik.Web.UI.GridItemType.Item Or e.Item.ItemType = GridItemType.AlternatingItem) AndAlso e.Item.OwnerTableView.Name = "rgTimeDetail" Then
                    Dim drv As DataRowView = DirectCast(e.Item.DataItem, DataRowView)

                    If CCommon.ToShort(drv("tintRecordType")) = 1 Then
                        If Session("bitApprovalforTImeExpense") = "1" Then
                            If CCommon.ToInteger(drv("numApprovalComplete")) = "-1" Then
                                DirectCast(e.Item.FindControl("lblDeclined"), Label).Visible = True
                                DirectCast(e.Item.NamingContainer, GridDataItem).BackColor = Color.Red
                            ElseIf CCommon.ToInteger(drv("numApprovalComplete")) <> 0 Then

                                If Session("intTimeExpApprovalProcess") = "1" Then
                                    Dim filterText As String = "numUserId=" & CCommon.ToLong(drv("numUserCntID")) & " AND (numLevel1Authority=##UserCntID## OR numLevel2Authority=##UserCntID## OR numLevel3Authority=##UserCntID## OR numLevel4Authority=##UserCntID## OR numLevel5Authority=##UserCntID##)"
                                    If Not dtApprovers Is Nothing AndAlso dtApprovers.Select(filterText.Replace("##UserCntID##", CCommon.ToLong(Session("UserContactID")))).Length > 0 Then
                                        DirectCast(e.Item.FindControl("lnkApprove"), LinkButton).Visible = True
                                        DirectCast(e.Item.FindControl("lnkDecline"), LinkButton).Visible = True
                                    Else
                                        DirectCast(e.Item.FindControl("lblPendingApproval"), Label).Visible = True
                                    End If
                                ElseIf Session("intTimeExpApprovalProcess") = "2" Then
                                    Dim numApprovalComplete As Int16 = 0
                                    If CCommon.ToInteger(drv("numApprovalComplete")) < 5 Then
                                        numApprovalComplete = CCommon.ToInteger(drv("numApprovalComplete")) + 1
                                    Else
                                        numApprovalComplete = 1
                                    End If

                                    Dim filterText As String = "numUserId=" & CCommon.ToLong(drv("numUserCntID")) & " AND numLevel" & numApprovalComplete & "Authority=" & CCommon.ToLong(Session("UserContactID"))
                                    If Not dtApprovers Is Nothing AndAlso dtApprovers.Select(filterText).Length > 0 Then
                                        DirectCast(e.Item.FindControl("lnkApprove"), LinkButton).Visible = True
                                        DirectCast(e.Item.FindControl("lnkDecline"), LinkButton).Visible = True
                                    Else
                                        DirectCast(e.Item.FindControl("lblPendingApproval"), Label).Visible = True
                                    End If
                                Else
                                    DirectCast(e.Item.FindControl("lblPendingApproval"), Label).Visible = True
                                End If


                            End If
                        End If

                        DirectCast(e.Item.FindControl("lnkEdit"), LinkButton).Visible = True
                        DirectCast(e.Item.FindControl("lnkDelete"), LinkButton).Visible = True
                    Else
                        DirectCast(e.Item.FindControl("lnkDelete"), LinkButton).Visible = False
                    End If
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Protected Sub rcbTeamHeaderTime_SelectedIndexChanged(sender As Object, e As Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs)
            Try
                SavePersistantData()
                BindDataGrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Protected Sub ddlTypeHeaderTime_SelectedIndexChanged(sender As Object, e As EventArgs)
            Try
                SavePersistantData()
                BindDataGrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Protected Sub rgExpense_DetailTableDataBind(sender As Object, e As GridDetailTableDataBindEventArgs)
            Try
                Dim parentItem As Telerik.Web.UI.GridDataItem = CType(e.DetailTableView.ParentItem, Telerik.Web.UI.GridDataItem)

                Dim objTimeAndExpense As New TimeExpenseLeave
                objTimeAndExpense.DomainID = CCommon.ToLong(Session("DomainID"))
                objTimeAndExpense.UserCntID = CCommon.ToLong(parentItem.GetDataKeyValue("numUserCntID"))
                objTimeAndExpense.FromDate = CDate(ddlDateTime.SelectedItem.Value.Split("t")(0))
                objTimeAndExpense.ToDate = CDate(ddlDateTime.SelectedItem.Value.Split("t")(1).Substring(1))
                objTimeAndExpense.ClientTimeZoneOffset = CCommon.ToInteger(Session("ClientMachineUTCTimeOffset"))
                e.DetailTableView.DataSource = objTimeAndExpense.GetExpenseEntriesDetail()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Protected Sub rgExpense_ItemDataBound(sender As Object, e As GridItemEventArgs)
            Try
                If e.Item.ItemType = GridItemType.Header Then
                    Dim objApprovalConfig As New ApprovalConfig
                    objApprovalConfig.chrAction = ""
                    objApprovalConfig.numDomainId = Session("DomainId")
                    objApprovalConfig.numModuleId = 1
                    dtApprovers = objApprovalConfig.GetAllUsers()
                End If

                If e.Item.ItemType = Telerik.Web.UI.GridItemType.FilteringItem Then
                    If objCommon Is Nothing Then
                        objCommon = New CCommon
                    End If

                    PersistTable.Load(strPageName:="frmEmpCal.aspx")
                    DirectCast(e.Item.FindControl("txtEmployeeHeaderExpense"), TextBox).Text = CCommon.ToString(PersistTable("EmployeeNameFilterExpense"))
                    DirectCast(e.Item.FindControl("ddlTypeHeaderExpense"), DropDownList).SelectedValue = CCommon.ToShort(PersistTable("PayrollTypeFilterExpense"))

                    Dim rcbTeamHeaderExpense As RadComboBox = DirectCast(e.Item.FindControl("rcbTeamHeaderExpense"), RadComboBox)
                    objCommon.sb_FillComboFromDBwithSel(rcbTeamHeaderExpense, 35, Session("DomainID"))
                    rcbTeamHeaderExpense.Items(0).Remove()
                    For Each item As RadComboBoxItem In rcbTeamHeaderExpense.Items
                        If CCommon.ToString(PersistTable("TeamsFilterExpense")).Split(",").Contains(item.Value) Then
                            item.Checked = True
                        End If
                    Next
                End If

                If (e.Item.ItemType = Telerik.Web.UI.GridItemType.Item Or e.Item.ItemType = GridItemType.AlternatingItem) AndAlso e.Item.OwnerTableView.Name = "rgExpenseDetail" Then
                    Dim drv As DataRowView = DirectCast(e.Item.DataItem, DataRowView)

                    If CCommon.ToShort(drv("tintRecordType")) = 1 Then
                        If Session("bitApprovalforTImeExpense") = "1" Then
                            If CCommon.ToInteger(drv("numApprovalComplete")) = "-1" Then
                                DirectCast(e.Item.FindControl("lblDeclined"), Label).Visible = True
                                DirectCast(e.Item.NamingContainer, GridDataItem).BackColor = Color.Red
                            ElseIf CCommon.ToInteger(drv("numApprovalComplete")) <> 0 Then

                                If Session("intTimeExpApprovalProcess") = "1" Then
                                    Dim filterText As String = "numUserId=" & CCommon.ToLong(drv("numUserCntID")) & " AND (numLevel1Authority=##UserCntID## OR numLevel2Authority=##UserCntID## OR numLevel3Authority=##UserCntID## OR numLevel4Authority=##UserCntID## OR numLevel5Authority=##UserCntID##)"
                                    If Not dtApprovers Is Nothing AndAlso dtApprovers.Select(filterText.Replace("##UserCntID##", CCommon.ToLong(Session("UserContactID")))).Length > 0 Then
                                        DirectCast(e.Item.FindControl("lnkApprove"), LinkButton).Visible = True
                                        DirectCast(e.Item.FindControl("lnkDecline"), LinkButton).Visible = True
                                    Else
                                        DirectCast(e.Item.FindControl("lblPendingApproval"), Label).Visible = True
                                    End If
                                ElseIf Session("intTimeExpApprovalProcess") = "2" Then
                                    Dim numApprovalComplete As Int16 = 0
                                    If CCommon.ToInteger(drv("numApprovalComplete")) < 5 Then
                                        numApprovalComplete = CCommon.ToInteger(drv("numApprovalComplete")) + 1
                                    Else
                                        numApprovalComplete = 1
                                    End If

                                    Dim filterText As String = "numUserId=" & CCommon.ToLong(drv("numUserCntID")) & " AND numLevel" & numApprovalComplete & "Authority=" & CCommon.ToLong(Session("UserContactID"))
                                    If Not dtApprovers Is Nothing AndAlso dtApprovers.Select(filterText).Length > 0 Then
                                        DirectCast(e.Item.FindControl("lnkApprove"), LinkButton).Visible = True
                                        DirectCast(e.Item.FindControl("lnkDecline"), LinkButton).Visible = True
                                    Else
                                        DirectCast(e.Item.FindControl("lblPendingApproval"), Label).Visible = True
                                    End If
                                Else
                                    DirectCast(e.Item.FindControl("lblPendingApproval"), Label).Visible = True
                                End If
                            End If
                        End If

                        DirectCast(e.Item.FindControl("lnkEdit"), LinkButton).Visible = True
                        DirectCast(e.Item.FindControl("lnkDelete"), LinkButton).Visible = True
                    Else
                        DirectCast(e.Item.FindControl("lnkDelete"), LinkButton).Visible = False
                    End If
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Protected Sub rcbTeamHeaderExpense_SelectedIndexChanged(sender As Object, e As RadComboBoxSelectedIndexChangedEventArgs)
            Try
                SavePersistantData()
                BindDataGrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Protected Sub ddlTypeHeaderExpense_SelectedIndexChanged(sender As Object, e As EventArgs)
            Try
                SavePersistantData()
                BindDataGrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Protected Sub rgTime_ItemCommand(sender As Object, e As GridCommandEventArgs)
            Try
                If e.CommandName = "Approve" Then
                    ApproveTimeExpense(CCommon.ToLong(e.CommandArgument))
                End If
                If e.CommandName = "Decline" Then
                    DeclineTimeExpense(CCommon.ToLong(e.CommandArgument))
                End If
                If e.CommandName = "Delete" Then
                    Dim objTimeAndExpense As New TimeExpenseLeave
                    objTimeAndExpense.DomainID = CCommon.ToLong(Session("DomainID"))
                    objTimeAndExpense.CategoryHDRID = CCommon.ToLong(e.CommandArgument)
                    objTimeAndExpense.Delete()
                End If
            Catch ex As Exception
                If ex.Message.Contains("ORDER_EXISTS") Then
                    DisplayError("Can't delete time entry. You have to first delete line item added in order for entry")
                ElseIf ex.Message.Contains("BIZDOC_EXISTS") Then
                    DisplayError("Can't delete time entry. You have to first delete line item added in bizdoc for entry")
                ElseIf ex.Message.Contains("JOURNAL_ENTRIES_EXISTS") Then
                    DisplayError("Can't delete time entry. Journal entries are being added for entry")
                Else
                    ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                    DisplayError(CCommon.ToString(ex))
                End If
            End Try
        End Sub

        Protected Sub rgExpense_ItemCommand(sender As Object, e As GridCommandEventArgs)
            Try
                If e.CommandName = "Approve" Then
                    ApproveTimeExpense(CCommon.ToLong(e.CommandArgument))
                End If
                If e.CommandName = "Decline" Then
                    DeclineTimeExpense(CCommon.ToLong(e.CommandArgument))
                End If
                If e.CommandName = "Delete" Then
                    Dim objTimeAndExpense As New TimeExpenseLeave
                    objTimeAndExpense.DomainID = CCommon.ToLong(Session("DomainID"))
                    objTimeAndExpense.CategoryHDRID = CCommon.ToLong(e.CommandArgument)
                    objTimeAndExpense.Delete()
                End If
            Catch ex As Exception
                If ex.Message.Contains("ORDER_EXISTS") Then
                    DisplayError("Can't delete expense entry. You have to first delete line item added in order for entry")
                ElseIf ex.Message.Contains("BIZDOC_EXISTS") Then
                    DisplayError("Can't delete expense entry. You have to first delete line item added in bizdoc for entry")
                ElseIf ex.Message.Contains("JOURNAL_ENTRIES_EXISTS") Then
                    DisplayError("Can't delete expense entry. Journal entries are being added for entry")
                Else
                    ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                    DisplayError(CCommon.ToString(ex))
                End If
            End Try
        End Sub

        Protected Sub btnSearchTime_Click(sender As Object, e As EventArgs)
            Try
                SavePersistantData()
                BindDataGrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Protected Sub lnkClearGridCondition_Click(sender As Object, e As EventArgs)
            Try
                SavePersistantData(isClearGridFilter:=True)
                BindDataGrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

#End Region

    End Class
End Namespace