Imports BACRM.BusinessLogic.TimeAndExpense
Imports BACRM.BusinessLogic.Opportunities
Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Contract
Imports BACRM.BusinessLogic.Accounting
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Contacts
Imports System.Web.Services
Imports BACRM.BusinessLogic.Projects

Namespace BACRM.UserInterface.TimeAndExpense
    Partial Public Class frmEditTimeExp
        Inherits BACRMPage


#Region "Variables"
        Dim dtOppBiDocItems As DataTable
        Dim OppBizDocID As Long

        Dim lngEmployeeAccountID, lngEmpPayrollExpenseAcntID As Long
#End Region
        Dim dtTable As DataTable
        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            Try
                divError.Style.Add("display", "none")
                lblError.Text = ""

                divMessage.Style.Add("display", "none")
                litMessage.Text = ""

                If Not IsPostBack Then
                    If GetQueryStringVal("CatID") = Nothing Then

                    Else
                        LoadInformation()
                    End If
                End If

                btnSaveClose.Attributes.Add("onclick", "return Save()")
                btnCancel.Attributes.Add("onclick", "return Close()")
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub
        <WebMethod(EnableSession:=True)> _
        Public Shared Function FillSalesOrder(ByVal DivID As Long, ByVal DomainID As Long) As String
            Try
                Dim objOpp As New MOpportunity
                Dim dt As DataTable
                objOpp.DomainID = DomainID
                objOpp.DivisionID = DivID
                objOpp.Mode = 3
                dt = objOpp.GetOpenRecords()

                Dim intCount As Integer = 0
                Dim str As New System.Text.StringBuilder
                If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then
                    str.Append("[")

                    str.Append("{")
                    str.Append("""id"" : """ & 0 & """,")
                    str.Append("""value"" : """"")
                    str.Append("},")

                    For intCount = 0 To dt.Rows.Count - 1
                        str.Append("{")
                        str.Append("""id"" : """ & dt.Rows(intCount)(dt.Columns(0).ColumnName) & """,")
                        str.Append("""value"" : """ & dt.Rows(intCount)(dt.Columns(1).ColumnName) & """")
                        str.Append("},")
                    Next

                    str.Remove(str.ToString.Length - 1, 1)
                    str.Append("]")
                    If dt.Rows.Count <= 0 Then
                        Return ("[]")
                    End If
                    str.Remove(0, 1)
                    str.Remove(str.Length - 1, 1)
                    Return ("[" & str.ToString & "]")

                Else
                    Return ""

                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Sub LoadInformation()
            Try
                Dim ApprovalTrans As New ApprovalConfig
                ApprovalTrans.numDomainId = Session("DomainID")
                ApprovalTrans.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
                ApprovalTrans.numRecordId = CCommon.ToLong(GetQueryStringVal("CatID"))
                dtTable = ApprovalTrans.GetTimeExpenseByRecord()
                hdnCntId.Value = dtTable.Rows(0).Item("numUserCntID")
                If dtTable.Rows.Count = 1 Then
                    hdnCategory.Value = Convert.ToString(dtTable.Rows(0).Item("numCategory"))
                    hdntype.Value = Convert.ToString(dtTable.Rows(0).Item("numType"))
                    BindClass(ddlClass)
                    hdnClassId.Value = Convert.ToString(dtTable.Rows(0).Item("numClassID"))
                    If Not ddlClass.Items.FindByValue(Convert.ToString(dtTable.Rows(0).Item("numClassID"))) Is Nothing Then
                        ddlClass.Items.FindByValue(Convert.ToString(dtTable.Rows(0).Item("numClassID"))).Selected = True
                    End If

                    If (dtTable.Rows(0).Item("numCategory") = 1 And dtTable.Rows(0).Item("numType") = 1) Then
                        ddlCategory.SelectedValue = 1
                    ElseIf (dtTable.Rows(0).Item("numCategory") = 1 And (dtTable.Rows(0).Item("numType") = 2 Or dtTable.Rows(0).Item("numType") = 7)) Then
                        ddlCategory.SelectedValue = 2
                    ElseIf (dtTable.Rows(0).Item("numCategory") = 2 And dtTable.Rows(0).Item("numType") = 1) Then
                        ddlCategory.SelectedValue = 3
                    ElseIf (dtTable.Rows(0).Item("numCategory") = 2 And dtTable.Rows(0).Item("numType") = 2) Then
                        ddlCategory.SelectedValue = 4
                    ElseIf (dtTable.Rows(0).Item("numCategory") = 2 And dtTable.Rows(0).Item("numType") = 6) Then
                        ddlCategory.SelectedValue = 5
                    End If
                    If dtTable.Rows(0).Item("numType") = 1 Or dtTable.Rows(0).Item("numType") = 6 Then
                        divCustomer.Visible = True
                        divService.Visible = True
                        divSalesOrder.Visible = True
                        LoadServiceItem(ddlService)
                        hdnServiceItemID.Value = Convert.ToString(dtTable.Rows(0).Item("numServiceItemID"))
                        For i As Integer = 0 To ddlService.Items.Count - 1
                            Dim StrSplitResult As String() = ddlService.Items(i).Value.Split("~")
                            If (StrSplitResult(0) = Convert.ToString(dtTable.Rows(0).Item("numServiceItemID"))) Then
                                ddlService.Items.FindByValue(ddlService.Items(i).Value).Selected = True
                            End If
                        Next
                        hdnServiceOrder.Value = Convert.ToInt32(dtTable.Rows(0).Item("numOppId"))
                        If objCommon Is Nothing Then objCommon = New CCommon
                        Dim strCompany As String
                        objCommon.DivisionID = dtTable.Rows(0).Item("numDivisionID")
                        strCompany = objCommon.GetCompanyName
                        hdnDivName.Value = strCompany
                        hdnDivId.Value = dtTable.Rows(0).Item("numDivisionID")
                        If GetQueryStringVal("type") = "P" Then
                            btnSaveClose.Text = "Update"
                            placeholder.Visible = False
                            ddlService.Visible = False
                            ddlAssignToSalesOrder.Visible = False
                            lblCustomerName.Visible = True
                            lblSalesOrder.Visible = True
                            lblService.Visible = True
                            lblCustomerName.Text = strCompany
                            lblService.Text = ddlService.SelectedItem.Text
                        End If
                    End If
                    If IsDBNull(dtTable.Rows(0).Item("txtDesc")) = False Then
                        txtDesc.Text = dtTable.Rows(0).Item("txtDesc")
                    End If

                    If dtTable.Rows(0).Item("numCategory") = 1 Then
                        lblRateAmount.Text = "Rate/Hour"
                        calFrom.SelectedDate = dtTable.Rows(0).Item("dtFromDate")
                        calto.SelectedDate = dtTable.Rows(0).Item("dtToDate")
                        txtStartHours.Text = If(Convert.ToDateTime(dtTable.Rows(0).Item("dtFromDate")).Hour > 12, Convert.ToDateTime(dtTable.Rows(0).Item("dtFromDate")).Hour - 12, Convert.ToDateTime(dtTable.Rows(0).Item("dtFromDate")).Hour)
                        txtStartMinutes.Text = Convert.ToDateTime(dtTable.Rows(0).Item("dtFromDate")).Minute
              
                        txtEndHours.Text = If(Convert.ToDateTime(dtTable.Rows(0).Item("dtToDate")).Hour > 12, Convert.ToDateTime(dtTable.Rows(0).Item("dtToDate")).Hour - 12, Convert.ToDateTime(dtTable.Rows(0).Item("dtToDate")).Hour)
                        txtEndMinutes.Text = Convert.ToDateTime(dtTable.Rows(0).Item("dtToDate")).Minute
    
                        If Format(dtTable.Rows(0).Item("dtFromDate"), "tt") = "AM" Then
                            rblStartTime.SelectedValue = 0
                        Else : rblStartTime.SelectedValue = 1
                        End If
                        If Format(dtTable.Rows(0).Item("dtToDate"), "tt") = "AM" Then
                            rblEndTime.SelectedValue = 0
                        Else : rblEndTime.SelectedValue = 1
                        End If
                        txtRate.Text = String.Format("{0:#,##0.00}", CCommon.ToDouble(dtTable.Rows(0).Item("monAmount")))
                    ElseIf dtTable.Rows(0).Item("numCategory") = 2 Then
                        lblRateAmount.Text = "Amount"
                        txtRate.Text = CCommon.ToDouble(dtTable.Rows(0).Item("monAmount"))
                        calFrom.SelectedDate = dtTable.Rows(0).Item("dtFromDate")
                        calto.SelectedDate = dtTable.Rows(0).Item("dtToDate")
                        txtStartHours.Text = ""
                        txtStartMinutes.Text = ""
                        txtEndHours.Text = ""
                        txtEndMinutes.Text = ""
                    End If

                End If
                TransactionInfo1.ReferenceType = enmReferenceType.TimeAndExpense
                TransactionInfo1.ReferenceID = CCommon.ToLong(GetQueryStringVal("CatID"))
                TransactionInfo1.getTransactionInfo()

                EnableControls(dtTable.Rows(0).Item("numCategory"))
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub LoadServiceItem(ByVal ddlServiceItem As DropDownList)
            Try
                Dim objItem As New BusinessLogic.Item.CItems
                objItem.DomainID = Session("DomainID")
                objItem.Filter = "S"
                objItem.type = 3
                ddlServiceItem.DataSource = objItem.getItemList()
                ddlServiceItem.DataTextField = "vcItemName"
                ddlServiceItem.DataValueField = "numItemCode"
                ddlServiceItem.DataBind()
                ddlServiceItem.Items.Insert(0, "")
                ddlServiceItem.Items.FindByText("").Value = "0"
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        Sub EnableControls(ByVal categ As String)
            Try
                If categ = "1" Then
                    tdStartDay.Visible = False
                    tdEndDay.Visible = False

                    divStartTime.Visible = True
                    divEndTime.Visible = True
                    lblRateAmount.Text = "Rate/Hour"
                    divRate.Visible = True
                    divDesc.Visible = True
                    divAssignTo.Visible = False
                ElseIf categ = "2" Then
                    divRate.Visible = True
                    lblRateAmount.Text = "Amount"
                    divAssignTo.Visible = False
                    divDesc.Visible = True
                    tdStartDay.Visible = False
                    tdEndDay.Visible = False
                    divStartTime.Visible = False
                    divEndTime.Visible = False
                End If

                EnableRate()
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub EnableRate()
            Try
                txtRate.Enabled = True
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub btnSaveClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveClose.Click
            Try
                Dim strStartTime, strEndTime, strStartDate, strEndDate As String
                strStartDate = calFrom.SelectedDate
                strEndDate = calto.SelectedDate
                Dim strSplitStartTimeDate, strSplitEndTimeDate As Date
                If Not (strStartDate Is Nothing And strEndDate Is Nothing) Then
                    strStartTime = strStartDate.Trim & " " & CCommon.ToInteger(txtStartHours.Text) & ":" & CCommon.ToInteger(txtStartMinutes.Text) & ":00" & IIf(rblStartTime.SelectedValue = 0, " AM", " PM")
                    strEndTime = strEndDate.Trim & " " & CCommon.ToInteger(txtEndHours.Text) & ":" & CCommon.ToInteger(txtEndMinutes.Text) & ":00" & IIf(rblEndTime.SelectedValue = 0, " AM", " PM")

                    strSplitStartTimeDate = CType(strStartTime, DateTime).AddMinutes(Session("ClientMachineUTCTimeOffset"))        'Convert the Start Date and Time to UT
                    strSplitEndTimeDate = CType(strEndTime, DateTime).AddMinutes(Session("ClientMachineUTCTimeOffset"))
                End If


                Dim objTimeExp As New TimeExpenseLeave
                objTimeExp.CategoryHDRID = CCommon.ToLong(GetQueryStringVal("CatID"))
                'objTimeExp.CategoryID = IIf(ddlCategory.SelectedItem.Value = "5", "1", ddlCategory.SelectedItem.Value)
                objTimeExp.Amount = txtRate.Text
                objTimeExp.ContractID = 0
                objTimeExp.ClassID = hdnClassId.Value
                If ddlCategory.SelectedItem.Value = "2" Or ddlCategory.SelectedItem.Value = "1" Then
                    objTimeExp.CategoryID = 1
                    objTimeExp.FromDate = strSplitStartTimeDate
                    objTimeExp.ToDate = strSplitEndTimeDate
                Else
                    objTimeExp.CategoryID = 2
                    objTimeExp.FromDate = strSplitStartTimeDate
                    objTimeExp.ToDate = strSplitStartTimeDate
                End If
                If ddlCategory.SelectedItem.Value = "1" Or ddlCategory.SelectedItem.Value = "3" Or ddlCategory.SelectedItem.Value = "5" Then
                    objTimeExp.ServiceItemID = hdnServiceItemID.Value
                    objTimeExp.OppID = hdnServiceOrder.Value
                    objTimeExp.DivisionID = hdnDivId.Value
                End If
                objTimeExp.Desc = txtDesc.Text.Trim
                objTimeExp.UserCntID = CCommon.ToLong(hdnCntId.Value)
                objTimeExp.DomainID = Session("DomainID")
                objTimeExp.ManageTimeAndExpense()
                If GetQueryStringVal("type") <> "P" Then

                    Dim ApprovalTrans As New ApprovalConfig
                    ApprovalTrans.strOutPut = "INPUT"
                    Dim dt As DataTable
                    ApprovalTrans.numDomainId = Session("DomainID")
                    ApprovalTrans.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
                    ApprovalTrans.numRecordId = CCommon.ToLong(GetQueryStringVal("CatID"))
                    dt = ApprovalTrans.GetTimeExpenseByRecord()
                    hdnApprovalComplete.Value = Convert.ToString(dt.Rows(0)("numApprovalComplete"))
                    hdnItemDesc.Value = Convert.ToString(dt.Rows(0)("vcItemDesc"))
                    hdnUserCntID.Value = Convert.ToString(dt.Rows(0)("numUserCntID"))
                    hdnEmployee.Value = Convert.ToString(dt.Rows(0)("vcEmployee"))
                    hdnAmount.Value = Convert.ToString(dt.Rows(0)("monAmount"))
                    hdntype.Value = Convert.ToString(dt.Rows(0)("numtype"))
                    hdnCategory.Value = Convert.ToString(dt.Rows(0)("numCategory"))
                    hdnDivisionID.Value = Convert.ToString(dt.Rows(0)("numDivisionID"))
                    hdnStageID.Value = Convert.ToString(dt.Rows(0)("numStageID"))
                    hdnProid.Value = Convert.ToString(dt.Rows(0)("numProid"))
                    hdnCaseid.Value = Convert.ToString(dt.Rows(0)("numCaseid"))
                    hdnOppId.Value = Convert.ToString(dt.Rows(0)("numOppId"))
                    hdnServiceItemID.Value = Convert.ToString(dt.Rows(0)("numServiceItemID"))
                    hdnExpId.Value = Convert.ToString(dt.Rows(0)("numExpId"))
                    hdnFromDate.Value = Convert.ToString(dt.Rows(0)("dtFromDate"))
                    hdnToDate.Value = Convert.ToString(dt.Rows(0)("dtToDate"))
                    hdnClassId.Value = Convert.ToString(dt.Rows(0)("numEClassId"))

                    ApprovalTrans.numConfigId = CCommon.ToLong(GetQueryStringVal("CatID"))
                    ApprovalTrans.strOutPut = "INPUT"
                    'Find Current row values 
                    Dim lngEmpAccountID As Long = 0
                    Dim lngEmpAccountIDB As Long = 0
                    Dim lngDebtAccountID As Long = 0
                    Dim SOId As Long
                    Dim DivID As Long
                    Dim ExpID As Long
                    Dim ProID As Long
                    Dim lngProjAccountID As Long
                    Dim lngEmpPayrollExpenseAcntID As Long
                    Dim objProject As New Project

                    SOId = hdnOppId.Value
                    ProID = hdnProid.Value
                    ExpID = hdnExpId.Value
                    SOId = hdnOppId.Value
                    DivID = hdnDivisionID.Value

                    'Save Expense Module
                    If hdnCategory.Value = 2 Then

                        If hdntype.Value = 1 Then
                            If SOId > 0 Then
                                'Validate default Payroll Expense Account
                                lngEmpPayrollExpenseAcntID = ChartOfAccounting.GetDefaultAccount("EP", Session("DomainID")) 'Employee Payeroll Expense
                                lngDebtAccountID = lngEmpPayrollExpenseAcntID
                                lngEmpAccountID = ChartOfAccounting.GetDefaultAccount("AP", Session("DomainID"))
                                If lngDebtAccountID = 0 Then
                                    Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "Alert", "alert('Please Set Default Receiveable Account from ""Administration->Domain Details->Accounting->Default Accounts"".' );", False)
                                    Exit Sub
                                End If
                                If lngEmpAccountID = 0 Then
                                    Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "Alert", "alert('Please Set Default Reimbursable Expense Receivable Account from ""Administration->Domain Details->Accounting->Default Accounts"".' );", False)
                                    Exit Sub
                                End If
                            End If
                        End If

                        If hdntype.Value = 2 Then
                            lngEmpPayrollExpenseAcntID = ChartOfAccounting.GetDefaultAccount("RE", Session("DomainID")) 'Employee Payeroll Expense
                            lngDebtAccountID = lngEmpPayrollExpenseAcntID
                            lngEmpAccountID = ChartOfAccounting.GetDefaultAccount("AP", Session("DomainID"))
                            If lngDebtAccountID = 0 Then
                                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "Alert", "alert('Please Set Default Reimbursable Expense Receivable Account from ""Administration->Domain Details->Accounting->Default Accounts"".' );", False)
                                Exit Sub
                            End If
                            If lngEmpAccountID = 0 Then
                                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "Alert", "alert('Please Set Default Payable Account from ""Administration->Domain Details->Accounting->Default Accounts"".' );", False)
                                Exit Sub
                            End If
                        End If
                        If hdntype.Value = 6 Then
                            lngEmpPayrollExpenseAcntID = ChartOfAccounting.GetDefaultAccount("RE", Session("DomainID")) 'Employee Payeroll Expense
                            lngDebtAccountID = lngEmpPayrollExpenseAcntID
                            If lngDebtAccountID = 0 Then
                                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "Alert", "alert('Please Set Default Receivable Account from ""Administration->Domain Details->Accounting->Default Accounts"".' );", False)
                                Exit Sub
                            End If
                            lngEmpAccountID = ChartOfAccounting.GetDefaultAccount("AP", Session("DomainID"))
                            If lngEmpAccountID = 0 Then
                                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "Alert", "alert('Please Set Default Reimbursable Expense Receivable Account from ""Administration->Domain Details->Accounting->Default Accounts"".' );", False)
                                Exit Sub
                            End If
                        End If

                    End If

                    'Save Authorities for Time Module
                    If hdnCategory.Value = 1 Then
                        lngEmpAccountID = ChartOfAccounting.GetDefaultAccount("AP", Session("DomainID"))
                        ' Validate Project Chart of Account 
                        If SOId = 0 AndAlso ProID > 0 Then
                            Dim dtRCordProject As DataTable
                            objProject.DomainID = Session("DomainID")
                            objProject.ProjectID = ProID
                            objProject.DivisionID = DivID
                            dtRCordProject = objProject.GetOpenProject()
                            If dtRCordProject.Rows.Count > 0 Then
                                lngProjAccountID = CCommon.ToLong(dtRCordProject.Rows(0).Item("numAccountId"))
                                lngDebtAccountID = lngProjAccountID
                            End If

                            If lngProjAccountID = 0 Then
                                Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "AccountValidation", "alert('Please Set Time & Expense Account for Current Project from ""Administration->Domain Details->Accounting->Accounts for Project""')", False)
                                Exit Sub
                            End If
                        End If

                        If SOId > 0 Then
                            'Validate default Payroll Expense Account
                            lngEmpPayrollExpenseAcntID = ChartOfAccounting.GetDefaultAccount("EP", Session("DomainID")) 'Employee Payeroll Expense
                            lngDebtAccountID = lngEmpPayrollExpenseAcntID
                            If lngEmpPayrollExpenseAcntID = 0 Then
                                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "Alert", "alert('Please Set Default Employee Payroll Expense Account from ""Administration->Domain Details->Accounting->Default Accounts"".' );", False)
                                Exit Sub
                            End If
                        End If
                        If ExpID > 0 Then
                            lngDebtAccountID = ExpID
                        End If
                        If lngEmpAccountID = 0 Then
                            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "Alert", "alert('Please Set Default Employee Payroll Expense Account from ""Administration->Domain Details->Accounting->Default Accounts"".' );", False)
                            Exit Sub
                        End If
                        If hdntype.Value = 2 Then
                            lngEmpPayrollExpenseAcntID = ChartOfAccounting.GetDefaultAccount("EP", Session("DomainID")) 'Employee Payeroll Expense
                            lngDebtAccountID = lngEmpPayrollExpenseAcntID
                            If lngEmpPayrollExpenseAcntID = 0 Then
                                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "Alert", "alert('Please Set Default Employee Payroll Expense Account from ""Administration->Domain Details->Accounting->Default Accounts"".' );", False)
                                Exit Sub
                            End If
                        End If
                    End If
                    If Session("bitApprovalforTImeExpense") = "1" Then
                        If Session("intTimeExpApprovalProcess") = "1" Then
                            ApprovalTrans.ApprovalStatus = 0
                            ApprovalTrans.numConfigId = CCommon.ToLong(GetQueryStringVal("CatID"))
                            ApprovalTrans.chrAction = "UT"
                            ApprovalTrans.UserId = Session("UserContactID")
                            ApprovalTrans.DomainID = Session("DomainID")
                            ApprovalTrans.numModuleId = 1
                            ApprovalTrans.UpdateApprovalTransaction()
                            'Validate Debit and CashCreditCard Account

                            If SOId > 0 Then
                                AddItemtoExistingInvoice(hdnServiceItemID.Value, hdnItemDesc.Value, hdnAmount.Value, hdnFromDate.Value, hdnToDate.Value, SOId, CCommon.ToLong(GetQueryStringVal("CatID")), ProID, hdnClassId.Value)
                            End If
                            SaveExpenseJournal(CCommon.ToLong(GetQueryStringVal("CatID")), hdnAmount.Value, hdnFromDate.Value, hdnToDate.Value, hdnUserCntID.Value, hdnCategory.Value, DivID, ProID, hdnClassId.Value, lngEmpAccountID, lngDebtAccountID, lngEmpAccountIDB, hdntype.Value)
                            'Response.Redirect("../Common/frmTicklerdisplay.aspx", False)
                        ElseIf Session("intTimeExpApprovalProcess") = "2" Then
                            ApprovalTrans.chrAction = "CH"
                            If Convert.ToInt32(hdnApprovalComplete.Value) < 5 Then
                                ApprovalTrans.ApprovalStatus = Convert.ToInt32(hdnApprovalComplete.Value) + 1
                            Else
                                ApprovalTrans.ApprovalStatus = 5
                            End If
                            ApprovalTrans.UpdateApprovalTransaction()
                            If ApprovalTrans.strOutPut = "VALID" Then
                                ApprovalTrans.numConfigId = CCommon.ToLong(GetQueryStringVal("CatID"))
                                ApprovalTrans.chrAction = "UT"
                                ApprovalTrans.UserId = Session("UserContactID")
                                ApprovalTrans.DomainID = Session("DomainID")
                                ApprovalTrans.numModuleId = 1
                                ApprovalTrans.UpdateApprovalTransaction()
                            Else
                                ApprovalTrans.numConfigId = CCommon.ToLong(GetQueryStringVal("CatID"))
                                ApprovalTrans.ApprovalStatus = 0
                                ApprovalTrans.chrAction = "UT"
                                ApprovalTrans.UserId = Session("UserContactID")
                                ApprovalTrans.DomainID = Session("DomainID")
                                ApprovalTrans.numModuleId = 1
                                ApprovalTrans.UpdateApprovalTransaction()
                                If SOId > 0 Then
                                    AddItemtoExistingInvoice(hdnServiceItemID.Value, hdnItemDesc.Value, hdnAmount.Value, hdnFromDate.Value, hdnToDate.Value, SOId, CCommon.ToLong(GetQueryStringVal("CatID")), ProID, hdnClassId.Value)
                                End If
                                SaveExpenseJournal(CCommon.ToLong(GetQueryStringVal("CatID")), hdnAmount.Value, hdnFromDate.Value, hdnToDate.Value, hdnUserCntID.Value, hdnCategory.Value, DivID, ProID, hdnClassId.Value, lngEmpAccountID, lngDebtAccountID, lngEmpAccountIDB, hdntype.Value)
                            End If
                        End If
                    Else
                        ApprovalTrans.ApprovalStatus = 0
                    End If

                End If
                Dim strScript As String = "<script language=JavaScript>"
                strScript += "window.opener.location.reload(true); self.close();"
                strScript += "</script>"
                Page.RegisterStartupScript("clientScript", strScript)

            Catch ex As Exception
                If ex.Message = "FY_CLOSED" Then
                    ShowMessage("This transaction can not be posted,Because transactions date belongs to closed financial year.")
                Else
                    ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                    DisplayError(CCommon.ToString(ex))
                End If
            End Try
        End Sub

        Private Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
            Try
              Response.Redirect("../Common/frmTicklerdisplay.aspx", False)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Private Sub SaveExpenseJournal(ByVal CategoryHDRID As Integer, ByVal RatePerHour As Double, ByVal dtFromDate As Date, ByVal dtToDate As Date, ByVal Employee As Long, ByVal category As Long, ByVal DivID As Long, ByVal ProID As Long, ByVal ClassID As Long, ByVal lngEmpAccountID As Long, ByVal lngDebtAccountID As Long, ByVal lngEmpAccountIDB As Long, ByVal Type As Long)
            If CategoryHDRID > 0 Then
                Dim totalAmount As Decimal
                'Create Journal Entry
                If category = 1 Then
                    totalAmount = RatePerHour * DateDiff(DateInterval.Minute, dtFromDate, dtToDate, Microsoft.VisualBasic.FirstDayOfWeek.Monday) / 60
                Else
                    totalAmount = RatePerHour
                End If

                If totalAmount > 0 Then

                    Using objTransactionScope As New System.Transactions.TransactionScope(Transactions.TransactionScopeOption.Required, New System.Transactions.TransactionOptions() With {.IsolationLevel = Transactions.IsolationLevel.ReadCommitted, .Timeout = Transactions.TransactionManager.MaximumTimeout})
                        Dim lngJournalId As Long
                        Dim objJEHeader As New JournalEntryHeader

                        With objJEHeader
                            .JournalId = lngJournalId
                            .RecurringId = 0
                            .EntryDate = Date.UtcNow
                            .Description = "TimeAndExpense"
                            .Amount = CCommon.ToDecimal(totalAmount)
                            .CheckId = 0
                            .CashCreditCardId = 0
                            .ChartAcntId = 0
                            .OppId = 0
                            .OppBizDocsId = 0
                            .DepositId = 0
                            .BizDocsPaymentDetId = 0
                            .IsOpeningBalance = 0
                            .LastRecurringDate = Date.Now
                            .NoTransactions = 0
                            .CategoryHDRID = CategoryHDRID
                            .ReturnID = 0
                            .CheckHeaderID = 0
                            .BillID = 0
                            .BillPaymentID = 0
                            .UserCntID = CCommon.ToLong(Employee)
                            .DomainID = Session("DomainID")
                        End With

                        lngJournalId = objJEHeader.Save()

                        'SaveDataToGeneralJournalDetails(lngJournalId)

                        Dim objJEList As New JournalEntryCollection

                        Dim objJE As New JournalEntryNew()

                        objJE.TransactionId = 0
                        objJE.DebitAmt = totalAmount
                        objJE.CreditAmt = 0
                        objJE.ChartAcntId = lngDebtAccountID 'If(SOId > 0, lngEmpPayrollExpenseAcntID, lngProjAccountID) If(txtMode.Text = "BD", lngEmpPayrollExpenseAcntID, lngProjAccountID)
                        objJE.Description = "TimeAndExpense"
                        objJE.CustomerId = CCommon.ToLong(DivID)
                        objJE.MainDeposit = 0
                        objJE.MainCheck = 0
                        objJE.MainCashCredit = 0
                        objJE.OppitemtCode = 0
                        objJE.BizDocItems = ""
                        objJE.Reference = ""
                        objJE.PaymentMethod = 0
                        objJE.Reconcile = False
                        objJE.CurrencyID = 0
                        objJE.FltExchangeRate = 0
                        objJE.TaxItemID = 0
                        objJE.BizDocsPaymentDetailsId = 0
                        objJE.ContactID = 0
                        objJE.ItemID = 0
                        objJE.ProjectID = CCommon.ToLong(ProID)
                        objJE.ClassID = CCommon.ToLong(ClassID)
                        objJE.CommissionID = 0
                        objJE.ReconcileID = 0
                        objJE.Cleared = 0
                        objJE.ReferenceType = 0
                        objJE.ReferenceID = 0

                        objJEList.Add(objJE)

                        objJE = New JournalEntryNew()
                        objJE.TransactionId = 0
                        objJE.DebitAmt = 0
                        objJE.CreditAmt = totalAmount
                        objJE.ChartAcntId = lngEmpAccountID
                        objJE.Description = "TimeAndExpense"
                        objJE.CustomerId = CCommon.ToLong(DivID)
                        objJE.MainDeposit = 0
                        objJE.MainCheck = 0
                        objJE.MainCashCredit = 0
                        objJE.OppitemtCode = 0
                        objJE.BizDocItems = ""
                        objJE.Reference = ""
                        objJE.PaymentMethod = 0
                        objJE.Reconcile = False
                        objJE.CurrencyID = 0
                        objJE.FltExchangeRate = 0
                        objJE.TaxItemID = 0
                        objJE.BizDocsPaymentDetailsId = 0
                        objJE.ContactID = 0
                        objJE.ItemID = 0
                        objJE.ProjectID = CCommon.ToLong(ProID)
                        objJE.ClassID = CCommon.ToLong(ClassID)
                        objJE.CommissionID = 0
                        objJE.ReconcileID = 0
                        objJE.Cleared = 0
                        objJE.ReferenceType = 0
                        objJE.ReferenceID = 0
                        objJEList.Add(objJE)


                        objJEList.Save(JournalEntryCollection.JournalMode.DeleteUpdateInsert, lngJournalId, Session("DomainID"))
                        objTransactionScope.Complete()
                    End Using
                End If
            End If
        End Sub

        Private Sub AddItemtoExistingInvoice(ByVal ServiceItemID As Long, ByVal Notes As String, ByVal RatePerHour As Double, ByVal dtFromDate As Date, ByVal dtToDate As Date, ByVal SOId As Long, ByVal CategoryHDRID As Long, ByVal ProID As Long, ByVal ClassID As Long)
            Dim objinvoice As New OppBizDocs
            If ServiceItemID > 0 Then
                objinvoice.ItemCode = ServiceItemID
                objinvoice.ItemDesc = Notes
                objinvoice.Price = RatePerHour
                objinvoice.UnitHour = DateDiff(DateInterval.Minute, dtFromDate, dtToDate, Microsoft.VisualBasic.FirstDayOfWeek.Monday) / 60
                objinvoice.OppId = SOId
                objinvoice.OppBizDocId = 0
                objinvoice.CategoryHDRID = CategoryHDRID
                objinvoice.ProID = ProID
                objinvoice.StageId = 0
                objinvoice.ClassId = ClassID
                objinvoice.AddItemToExistingInvoice()
            End If
        End Sub

        Private Sub BindClass(ByVal ddlClass As DropDownList)
            Try
                BusinessLogic.Admin.CAdmin.sb_BindClassCombo(ddlClass, Session("DomainID"))
                If ddlClass.Items.FindByText("--Select One--") IsNot Nothing Then
                    ddlClass.Items.FindByText("--Select One--").Selected = True
                    ddlClass.Items.FindByText("--Select One--").Text = ""
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        Dim dtItems As New DataTable
        Dim dtrow As DataRow
        Dim dblHourlyRate, dblDailyHours As Double


        Private Sub ShowMessage(ByVal message As String)
            Try
                divMessage.Style.Add("display", "")
                litMessage.Text = message
            Catch ex As Exception

            End Try
        End Sub

        Private Sub DisplayError(ByVal errorMessage As String)
            Try
                divError.Style.Add("display", "")
                lblError.Text = errorMessage
                divError.Focus()
            Catch ex As Exception

            End Try
        End Sub
    End Class
End Namespace