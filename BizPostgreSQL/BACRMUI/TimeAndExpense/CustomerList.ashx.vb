﻿Imports System.Web
Imports System.Web.Services
Imports System.Web.Script.Serialization
Imports System.Linq
Imports System.Collections.Generic
Imports System.Text
Imports BACRM.BusinessLogic.Outlook
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Contacts
Imports System.Web.SessionState
Public Class CustomerList
    Implements System.Web.IHttpHandler, IReadOnlySessionState
    Dim intCount As Integer
    Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest

        Dim dtCompanies As DataTable
        Dim items As New List(Of ComboBoxItemData)()
        Dim objCommon As New CCommon
        With objCommon
            .DomainID = context.Session("DomainID")
            .Filter = "%" & context.Request.QueryString("q").ToString & "%"
            .UserCntID = context.Session("UserContactID")
            dtCompanies = objCommon.PopulateOrganization.Tables(0)
        End With

        If dtCompanies IsNot Nothing AndAlso dtCompanies.Rows.Count > 0 Then

            For Each row As DataRow In dtCompanies.Rows
                Dim itemData As New ComboBoxItemData()
                itemData.Text = row("vcCompanyname")
                itemData.Value = row("numDivisionID")
                items.Add(itemData)
            Next

            Dim nameList As New StringBuilder
            nameList.Append("[")
            For intCount = 0 To dtCompanies.Rows.Count - 1
                nameList.Append("{")
                nameList.Append("""id"":""" & dtCompanies.Rows(intCount)("numDivisionID") & """,")
                nameList.Append("""company_name"":""" & dtCompanies.Rows(intCount)("vcCompanyname") & """")
                'nameList.Append("""last_name"":""" & CCommon.ToString(dtContacts.Rows(intCount)("vcLastName")) & " (" & CCommon.ToString(dtContacts.Rows(intCount)("Company")) & ")" & """,")
                'nameList.Append("""email"":""" & dtContacts.Rows(intCount)("vcEmail") & """,")
                'nameList.Append("""url"":""#""")
                nameList.Append("},")
            Next
            nameList.Remove(nameList.ToString.Length - 1, 1)
            nameList.Append("]")
            nameList.Remove(0, 1)
            nameList.Remove(nameList.Length - 1, 1)

            context.Response.ContentType = "application/json"
            context.Response.ContentEncoding = Encoding.UTF8

            'Dim js As New JavaScriptSerializer()
            'Dim strJSON As String = js.Serialize(nameList)
            context.Response.Write("[" & nameList.ToString & "]")

        End If

    End Sub


    ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property

End Class