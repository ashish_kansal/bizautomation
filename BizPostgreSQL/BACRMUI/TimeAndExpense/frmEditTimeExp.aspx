<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" CodeBehind="frmEditTimeExp.aspx.vb"
    Inherits="BACRM.UserInterface.TimeAndExpense.frmEditTimeExp" MasterPageFile="~/common/PopupBootstrap.Master" %>

<%@ Register Src="~/Accounting/TransactionInfo.ascx" TagName="TransactionInfo" TagPrefix="uc1" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register TagPrefix="BizCalendar" TagName="Calendar" Src="../include/calandar.ascx" %>

<asp:Content ContentPlaceHolderID="head" ID="Content1" runat="server">
    <title>Time,Expense & Leave</title>
    <script type="text/javascript" src="../JavaScript/jquery-ui.min.js"></script>
    <script type="text/javascript" src="../JavaScript/jquery.tokeninput.js"></script>
    <link href="../CSS/jquery-ui.css" rel="Stylesheet" type="text/css" />
    <link href="../Styles/token-input.css" rel="Stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="../CSS/jscal2.css" />
    <link rel="stylesheet" type="text/css" href="../CSS/border-radius.css" />
    <script type="text/javascript" src="../JavaScript/jscal2.js"></script>
    <script type="text/javascript" src="../JavaScript/en.js"></script>
    <link href="../Styles/token-input-facebook1.css" rel="Stylesheet" type="text/css" />
    <style>
        a {
            text-decoration: none;
        }

            a:hover {
                text-decoration: none;
            }
    </style>
    <script type="text/javascript">
        function openBizDoc(a, b) {
            window.open('../opportunity/frmBizInvoice.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&OpID=' + a + '&OppBizId=' + b, '', 'toolbar=no,menubar=yes,titlebar=no,left=100,width=680,height=700,scrollbars=yes,resizable=yes');
            return false;
        }
        function OpenRec(a) {
            window.open('../opportunity/frmRecomBillTime.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&DivID=' + a, '', 'toolbar=no,titlebar=no,left=300,top=300,width=400,height=200,scrollbars=yes,resizable=yes')
            return false;
        }

        function FillBox(a, b) {
            document.Form1.txtRate.value = a;
        }
        function Save() {
            if ($("[name='ctl00$Content$calFrom$txtDate']").val() == "") {
                alert("Select From Date")
                return false;
            }
            if ($("[name='ctl00$Content$calto$txtDate']").val() == "") {
                alert("Select To Date")
                return false;
            }
            if ($("#<%=hdntype.ClientID%>").val() == "1" || $("#<%=hdntype.ClientID%>").val() == "6") {
                var DivID;
                DivID = (isNaN($("#<%=hdnDivId.ClientID%>").val()) == true) ? 0 : Number($("#<%=hdnDivId.ClientID%>").val());

                if (DivID == 0) {
                    alert("Please Select Customer")
                    return false;
                }
                if ($("#<%=hdnServiceOrder.ClientID%>").val() <= 0) {
                    alert("Please Select Sales Order")
                    return false;
                }
                if ($("#<%=hdnServiceItemID.ClientID%>").val() <= 0) {
                    alert("Please Select Service Item ID")
                    return false;
                }
            }
            if ($("#<%=hdntype.ClientID%>").val() == "1" || $("#<%=hdnCategory.ClientID%>").val() == "1") {
                var Amt;
                Amt = (isNaN($("#<%=txtRate.ClientID%>").val()) == true) ? 0 : Number($("#<%=txtRate.ClientID%>").val());
                if (Amt <= 0) {
                    alert("Rate Per Hour must be greater than zero")
                    return false;
                }
            }
        }
        function OpenProjectTime() {
            window.open('<%# ResolveUrl("~/projects/frmProTime.aspx")%>' + '?Mode=TE&ProStageID=0&Proid=0&DivId=0&Date=' + document.getElementById('hdnDate').value, '', 'toolbar=no,titlebar=no,left=500, top=300,width=750,height=350,scrollbars=yes,resizable=yes');
            return false;
        }
        function AddAuthBizdoc() {
            window.open('<%# ResolveUrl("~/projects/frmProTime.aspx")%>' + '?Mode=BD&ProStageID=0&Proid=0&DivId=0&Date=' + document.getElementById('hdnDate').value, '', 'toolbar=no,titlebar=no,left=500, top=300,width=750,height=350,scrollbars=yes,resizable=yes');
            return false;
        } var saved_tokens = [];
        // var saved_tokensCC = [];<a href="frmEmpCal.aspx">frmEmpCal.aspx</a>
        var hdnTo = [];
        var token_count = 0;
        $(document).ready(function () {
            if ($("#<%=hdnDivId.ClientID%>").val() != 0) {
                FillSalesOrderDetail($("#<%=hdnDivId.ClientID%>").val());

            }
            $(".tokeninputclass").tokenInput("CustomerList.ashx", {
                txtInput: "company_name",
                tokenLimit: 1,
                theme: "facebook1",
                prePopulate: [{ id: $("#<%=hdnDivId.ClientID%>").val(), company_name: $("#<%=hdnDivName.ClientID%>").val() }],
                propertyToSearch: "company_name",
                preventDuplicates: true,
                needNewTokenInput: true,
                resultsFormatter: function (item) { return "<li><div style='display: inline-block; padding-left: 10px;'><div class='full_name'>" + item.company_name + "</div></li>" },
                tokenFormatter: function (item) { return "<li><p title='" + item.company_name + "'>" + item.company_name + "</p></li>" },
                onAdd: function (item) {

                    if (Number(item.id) > 0) {
                        $("#<%=hdnDivId.ClientID%>").val(item.id)
                        FillSalesOrderDetail(item.id)
                    } else {
                        $("#<%=hdnDivId.ClientID%>").val(0)
                    }
                }
            });

            function FillSalesOrderDetail(data) {
                var pagePath = window.location.pathname + "/FillSalesOrder";
                var dataString = "{DivID:" + data + ",DomainID:" + <%=Session("DomainID") %> + "}";
                FillSalesOrder(pagePath, dataString, "", "");
            }
            $('#<%=ddlService.ClientID%>').on("change", function () {
                var strItemDetail;
                strItemDetail = ($(this).find('option:selected').val()).toString().split("~");
                console.log(strItemDetail);
                if (strItemDetail.length > 0) {

                    $("#<%=hdnServiceItemID.ClientID%>").val(strItemDetail[0]);
                    $("#<%=txtRate.ClientID%>").val(strItemDetail[1]);
                }
                else {
                    $("#<%=hdnServiceItemID.ClientID%>").val(0);
                    $("#<%=txtRate.ClientID%>").val(0);
                }
            });
            $('#<%=ddlAssignToSalesOrder.ClientID%>').on("change", function () {
                $('#<%=hdnServiceOrder.ClientID%>').val($('#<%=ddlAssignToSalesOrder.ClientID%> option:selected').val());
            });
            $('#<%=ddlClass.ClientID%>').on("change", function () {
                $('#<%=hdnClassId.ClientID%>').val($('#<%=ddlClass.ClientID%> option:selected').val());
            });

            function FillSalesOrder(pagePath, dataString, hdnDivId, gridName) {
                $.ajax({
                    type: "POST",
                    url: pagePath,
                    Async: false,
                    data: dataString,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    error:
              function (XMLHttpRequest, textStatus, errorThrown) {
                  console.log("Error");
              },
                    success: function (result) {
                        $('#ddlAssignToSalesOrder').html('');

                        var json;
                        json = null;
                        json = eval(result.d);
                        if (result.d !== '') {
                            for (i = 0; i < json.length; i++) {
                                $('#<%=ddlAssignToSalesOrder.ClientID%>').append("<option value=" + json[i].id + ">" + json[i].value + "</option>")
                                if ($('#<%=hdnServiceOrder.ClientID%>').val() != "") {
                                    if ($('#<%=hdnServiceOrder.ClientID%>').val() == json[i].id) {
                                        $('#<%=lblSalesOrder.ClientID%>').text(json[i].value);
                                    }
                                }
                            }
                            $('#ddlAssignToSalesOrder').val(0);
                            if ($('#<%=hdnServiceOrder.ClientID%>').val() != "") {
                                $('#<%=ddlAssignToSalesOrder.ClientID%>').val($('#<%=hdnServiceOrder.ClientID%>').val());
                                $('#<%=lblSalesOrder.ClientID%>').html($('#<%=ddlAssignToSalesOrder.ClientID%> option:selected').val());
                            }
                        }
                        else {
                            $('#ddlAssignToSalesOrder').html('');
                        }
                        json = null;
                    }
                });
            }
        })

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <div class="row">
        <div class="col-xs-12">
            <div class="pull-right">
                <asp:LinkButton ID="btnSaveClose" OnClientClick="return Save()" runat="server" CssClass="btn btn-primary">Update</asp:LinkButton>
            <asp:LinkButton ID="btnCancel" runat="server" CssClass="btn btn-primary">Close</asp:LinkButton>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageTitle" runat="server" ClientIDMode="Static">
    Edit Time & Expense Details
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="Content" runat="server">
    <asp:ScriptManager runat="server"></asp:ScriptManager>
    <div class="row" id="divError" runat="server" style="display: none">
        <div class="col-sm-12">
            <div class="alert alert-danger">
                <h4><i class="icon fa fa-ban"></i>Error</h4>

                <p>
                    <asp:Label ID="lblError" runat="server"></asp:Label>
                </p>
            </div>
        </div>
    </div>
    <div id="divMessage" runat="server" class="row" style="display: none">
        <div class="col-xs-12">
            <div class="alert alert-warning">
                <h4><i class="icon fa fa-warning"></i>Alert!</h4>
                <asp:Literal ID="litMessage" runat="server" EnableViewState="False"></asp:Literal>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="form-group">
                <label>Category</label>
                <asp:HiddenField ID="hdnCntId" runat="server" />
                <asp:DropDownList ID="ddlCategory" runat="server" Enabled="false" AutoPostBack="True" CssClass="form-control">
                    <asp:ListItem Value="0">--Select One--</asp:ListItem>
                    <asp:ListItem Value="1">Billable Time</asp:ListItem>
                    <asp:ListItem Value="2">Non Billable Time</asp:ListItem>
                    <asp:ListItem Value="3">Billable Expense</asp:ListItem>
                    <asp:ListItem Value="4">Reimbursable Expense</asp:ListItem>
                    <asp:ListItem Value="5">Billable+Reimbursable Expense</asp:ListItem>
                </asp:DropDownList>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="form-group">
                <label>From:</label>
                <BizCalendar:Calendar ID="calFrom" runat="server" />
            </div>
        </div>
    </div>
    <div class="row" id="divStartTime" runat="server">
        <div class="col-xs-12">
            <div class="form-group">
                <label>Strat Time</label>
                <ul class="list-inline">
                    <li style="vertical-align: top">
                        <telerik:RadNumericTextBox ID="txtStartHours" CssClass="form-control" runat="server" MaxValue="12" MinValue="0" ShowSpinButtons="true" NumberFormat-DecimalDigits="0" EmptyMessage="Hours" Width="58"></telerik:RadNumericTextBox></li>
                    <li style="vertical-align: top">
                        <telerik:RadNumericTextBox ID="txtStartMinutes" CssClass="form-control" runat="server" MaxValue="59" MinValue="0" ShowSpinButtons="true" NumberFormat-DecimalDigits="0" EmptyMessage="Minutes" Width="68"></telerik:RadNumericTextBox></li>
                    <li style="vertical-align: top">
                        <asp:RadioButtonList ID="rblStartTime" runat="server" RepeatLayout="Flow" RepeatDirection="Horizontal">
                            <asp:ListItem Text="AM" Value="0" Selected="True"></asp:ListItem>
                            <asp:ListItem Text="PM" Value="1"></asp:ListItem>
                        </asp:RadioButtonList>
                    </li>
                    <li>
                        <div id="tdStartDay" runat="server">
                            <asp:RadioButton ID="radStartFullDay" runat="server" Checked="true" Text="Full Day" GroupName="rad1" />
                            <asp:RadioButton ID="radStartHalfDay" runat="server" Text="Half Day" GroupName="rad1" />
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="form-group">
                <label>To</label>
                <BizCalendar:Calendar ID="calto" runat="server" />
            </div>
        </div>
    </div>
    <div class="row" id="divEndTime" runat="server">
        <div class="col-xs-12">
            <div class="form-group">
                <label>End Time</label>
                <ul class="list-inline">
                    <li style="vertical-align: top">
                        <telerik:RadNumericTextBox ID="txtEndHours" CssClass="form-control" runat="server" MaxValue="12" MinValue="0" ShowSpinButtons="true" NumberFormat-DecimalDigits="0" EmptyMessage="Hours" Width="58"></telerik:RadNumericTextBox></li>
                    <li style="vertical-align: top">
                        <telerik:RadNumericTextBox ID="txtEndMinutes" CssClass="form-control" runat="server" MaxValue="59" MinValue="0" ShowSpinButtons="true" NumberFormat-DecimalDigits="0" EmptyMessage="Minutes" Width="68"></telerik:RadNumericTextBox></li>
                    <li style="vertical-align: top">
                        <asp:RadioButtonList ID="rblEndTime" runat="server" RepeatLayout="Flow" RepeatDirection="Horizontal">
                            <asp:ListItem Text="AM" Value="0" Selected="True"></asp:ListItem>
                            <asp:ListItem Text="PM" Value="1"></asp:ListItem>
                        </asp:RadioButtonList>
                    </li>
                    <li>
                        <div id="tdEndDay" runat="server">
                            <asp:RadioButton ID="radEndFullday" runat="server" Checked="true" Text="Full Day" GroupName="rad2" />
                            <asp:RadioButton ID="radEndHalfDay" runat="server" Text="Half Day" GroupName="rad2" />
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="row" id="divRate" runat="server">
        <div class="col-xs-12">
            <div class="form-group">
                <label>
                    <asp:Label runat="server" ID="lblRateAmount" Text="Rate/Hour"></asp:Label></label>
                <asp:TextBox ID="txtRate" runat="server" CssClass="form-control"></asp:TextBox>
            </div>
        </div>
    </div>
    <div class="row" id="divAssignTo" runat="server" visible="false">
        <div class="col-xs-12">
            <div class="form-group">
                <label>Assign to <span style="color: red">*</span></label>
                <asp:RadioButtonList ID="rblAssignto" runat="server" AutoPostBack="True" Font-Bold="false" RepeatDirection="Horizontal">
                    <asp:ListItem Text="Project" Value="1"></asp:ListItem>
                    <asp:ListItem Text="Sales Order / Invoice" Value="3"></asp:ListItem>
                </asp:RadioButtonList>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="form-group">
                <label>Select Class</label>
                <asp:DropDownList runat="server" ID="ddlClass" CssClass="form-control"></asp:DropDownList>
            </div>
        </div>
    </div>
    <div class="row" id="divCustomer" runat="server" visible="false">
        <div class="col-xs-12">
            <div class="form-group">
                <label>Select Customer<span style="color: red">*</span></label>
                <div>
                    <div class="placeholder" id="placeholder" runat="server">
                        <div class="CodeToCopy">
                            <input type="text" id="demo-input" name="blah" class="tokeninputclass form-control" />

                        </div>
                    </div>
                    <input type="hidden" runat="server" id="hdnDivId" />
                    <input type="hidden" runat="server" id="hdnDivName" />
                    <asp:Label ID="lblCustomerName" Visible="false" runat="server" Text="Label"></asp:Label>
                </div>
            </div>
        </div>
    </div>
    <div class="row" id="divSalesOrder" runat="server" visible="false">
        <div class="col-xs-12">
            <div class="form-group">
                <label>Select Sales Order<span style="color: red">*</span></label>
                <asp:DropDownList runat="server" ID="ddlAssignToSalesOrder" CssClass="form-control"></asp:DropDownList>
                <asp:HiddenField ID="hdnServiceOrder" Value="0" runat="server" />
                <asp:Label ID="lblSalesOrder" Visible="false" runat="server" Text="Label"></asp:Label>
            </div>
        </div>
    </div>
    <div class="row" id="divService" runat="server" visible="false">
        <div class="col-xs-12">
            <div class="form-group">
                <label>Select Service(Item)<span style="color: red">*</span></label>
                <asp:DropDownList runat="server" ID="ddlService" CssClass="form-control"></asp:DropDownList>
                <asp:HiddenField ID="hdnServiceItemID" runat="server" />
                <asp:Label ID="lblService" Visible="false" runat="server" Text="Label"></asp:Label>
            </div>
        </div>
    </div>
    <div class="row" id="divDesc" runat="server">
        <div class="col-xs-12">
            <div class="form-group">
                <label>Notes</label>
                <asp:TextBox ID="txtDesc" runat="server" CssClass="form-control" TextMode="MultiLine"></asp:TextBox>
            </div>
        </div>
    </div>
    <asp:HiddenField ID="hdnApprovalComplete" runat="server" />
    <asp:HiddenField ID="hdnItemDesc" runat="server" />
    <asp:HiddenField ID="hdnUserCntID" runat="server" />
    <asp:HiddenField ID="hdnEmployee" runat="server" />
    <asp:HiddenField ID="hdnAmount" runat="server" />
    <asp:HiddenField ID="hdntype" runat="server" />
    <asp:HiddenField ID="hdnCategory" runat="server" />
    <asp:HiddenField ID="hdnDivisionID" runat="server" />
    <asp:HiddenField ID="hdnStageID" runat="server" />
    <asp:HiddenField ID="hdnProid" runat="server" />
    <asp:HiddenField ID="hdnCaseid" runat="server" />
    <asp:HiddenField ID="hdnOppId" runat="server" />
    <asp:HiddenField ID="hdnExpId" runat="server" />
    <asp:HiddenField ID="hdnFromDate" runat="server" />
    <asp:HiddenField ID="hdnToDate" runat="server" />
    <asp:HiddenField ID="hdnClassId" runat="server" />
    <asp:HiddenField ID="hdnDate" runat="server" />

    <uc1:TransactionInfo ID="TransactionInfo1" runat="server" />
</asp:Content>
