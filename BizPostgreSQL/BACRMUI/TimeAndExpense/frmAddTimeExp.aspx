<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" CodeBehind="frmAddTimeExp.aspx.vb"
    Inherits="BACRM.UserInterface.TimeAndExpense.frmAddTimeExp" MasterPageFile="~/common/BizMaster.Master" %>

<%@ Register Src="~/Accounting/TransactionInfo.ascx" TagName="TransactionInfo" TagPrefix="uc1" %>
<%@ Register TagPrefix="BizCalendar" TagName="Calendar" Src="../include/calandar.ascx" %>

<asp:Content ContentPlaceHolderID="head" runat="server">
    <title>Time,Expense & Leave</title>

    <script type="text/javascript">
        function openBizDoc(a, b) {
            window.open('../opportunity/frmBizInvoice.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&OpID=' + a + '&OppBizId=' + b, '', 'toolbar=no,menubar=yes,titlebar=no,left=100,width=680,height=700,scrollbars=yes,resizable=yes');
            return false;
        }
        function OpenRec(a) {
            window.open('../opportunity/frmRecomBillTime.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&DivID=' + a, '', 'toolbar=no,titlebar=no,left=300,top=300,width=400,height=200,scrollbars=yes,resizable=yes')
            return false;
        }
        function FillBox(a, b) {
            document.Form1.txtRate.value = a;
        }
        function Save() {
            if ($("[id$=ddlCategory]").val() == "0") {
                alert("Select Category")
                $("[id$=ddlCategory]").focus();
                return false;
            }

            if ($("[id$=ddlCategory]").val() == "1") {

                if ($("[id$=calFrom_txtDate]").val() == "") {
                    alert("Select From Date")
                    return false;
                }
                if ($("[id$=calto_txtDate]").val() == "") {
                    alert("Select To Date")
                    return false;
                }

                if ($("[id$=txtRate]").val() == "") {
                    alert("Enter Rate/Hour")
                    return false;
                }
            }

            if ($("[id$=ddlCategory]").val() == "2") {
                if ($("[id$=txtRate]").val() == "") {
                    alert("Enter Rate/Hour")
                    return false;
                }
                if ($("[id$=ddlExpenseAccount]").val() == "0") {
                    alert("Select Expense Account")
                    $("[id$=ddlExpenseAccount]").focus();
                    return false;
                }
            }

            if ($("[id$=ddlCategory]").val() == "3") {
                if ($("[id$=calFrom_txtDate]").val() == "") {
                    alert("Select From Date")
                    return false;
                }
                if ($("[id$=calto_txtDate]").val() == "") {
                    alert("Select To Date")
                    return false;
                }
            }
        }
        function OpenProjectTime() {
            window.open('<%# ResolveUrl("~/projects/frmProTime.aspx")%>' + '?Mode=TE&ProStageID=0&Proid=0&DivId=0&Date=' + document.getElementById('hdnDate').value, '', 'toolbar=no,titlebar=no,left=500, top=300,width=750,height=350,scrollbars=yes,resizable=yes');
            return false;
        }
        function AddAuthBizdoc() {
            window.open('<%# ResolveUrl("~/projects/frmProTime.aspx")%>' + '?Mode=BD&ProStageID=0&Proid=0&DivId=0&Date=' + document.getElementById('hdnDate').value, '', 'toolbar=no,titlebar=no,left=500, top=300,width=750,height=350,scrollbars=yes,resizable=yes');
            return false;
        }
    </script>
</asp:Content>
<asp:Content ContentPlaceHolderID="MainContent" runat="server">
    <div class="row" id="divError" runat="server" style="display: none">
        <div class="col-sm-12">
            <div class="alert alert-danger">
                <h4><i class="icon fa fa-ban"></i>Error</h4>

                <p>
                    <asp:Label ID="lblError" runat="server"></asp:Label>
                </p>
            </div>
        </div>
    </div>
    <div id="divMessage" runat="server" class="row" style="display: none">
        <div class="col-xs-12">
            <div class="alert alert-warning">
                <h4><i class="icon fa fa-warning"></i>Alert!</h4>
                <asp:Literal ID="litMessage" runat="server" EnableViewState="False"></asp:Literal>
            </div>
        </div>
    </div>
    <div class="row padbottom10">
        <div class="col-xs-12 text-right">
            <asp:LinkButton ID="btnSaveClose" runat="server" CssClass="btn btn-primary"><i class="fa fa-floppy-o"></i>&nbsp;&nbsp;Save & Close</asp:LinkButton>
            <asp:LinkButton ID="btnCancel" runat="server" CssClass="btn btn-primary"><i class="fa fa-times"></i>&nbsp;&nbsp;Close</asp:LinkButton>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <div class="box box-default">
                <div class="box-header with-border">
                    <i class="fa fa-bars"></i>

                    <h3 class="box-title">Time And Expense</h3>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="form-group">
                                <label>Category</label>
                                <asp:DropDownList ID="ddlCategory" runat="server" AutoPostBack="True" CssClass="form-control">
                                    <asp:ListItem Value="0">--Select One--</asp:ListItem>
                                    <asp:ListItem Value="1">Billable Time</asp:ListItem>
                                    <asp:ListItem Value="5">Non Billable Time</asp:ListItem>
                                    <asp:ListItem Value="6">Billable Expense</asp:ListItem>
                                    <asp:ListItem Value="2">Reimbursable Expense</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                    </div>
                    <asp:Panel ID="pnlDate" runat="server">
                        <div class="row">
                            <div class="col-xs-12 col-sm-6">
                                <div class="form-group">
                                    <label>From Date</label>
                                    <ul class="list-inline">
                                        <li>
                                            <div class="form-inline">
                                                <BizCalendar:Calendar ID="calFrom" runat="server" />
                                            </div>
                                        </li>
                                        <li id="tdStartTime" runat="server">
                                            <div class="form-inline">
                                                <img src="../images/Clock-16.gif" align="absMiddle">
                                                <asp:DropDownList ID="ddltime" runat="server" CssClass="form-control">
                                                    <asp:ListItem Selected="False" Value="23">12:00</asp:ListItem>
                                                    <asp:ListItem Selected="False" Value="24">12:30</asp:ListItem>
                                                    <asp:ListItem Selected="False" Value="1">1:00</asp:ListItem>
                                                    <asp:ListItem Selected="False" Value="2">1:30</asp:ListItem>
                                                    <asp:ListItem Selected="False" Value="3">2:00</asp:ListItem>
                                                    <asp:ListItem Selected="False" Value="4">2:30</asp:ListItem>
                                                    <asp:ListItem Selected="False" Value="5">3:00</asp:ListItem>
                                                    <asp:ListItem Selected="False" Value="6">3:30</asp:ListItem>
                                                    <asp:ListItem Selected="False" Value="7">4:00</asp:ListItem>
                                                    <asp:ListItem Selected="False" Value="8">4:30</asp:ListItem>
                                                    <asp:ListItem Selected="False" Value="9">5:00</asp:ListItem>
                                                    <asp:ListItem Selected="False" Value="10">5:30</asp:ListItem>
                                                    <asp:ListItem Selected="False" Value="11">6:00</asp:ListItem>
                                                    <asp:ListItem Selected="False" Value="12">6:30</asp:ListItem>
                                                    <asp:ListItem Selected="False" Value="13">7:00</asp:ListItem>
                                                    <asp:ListItem Selected="False" Value="14">7:30</asp:ListItem>
                                                    <asp:ListItem Selected="True" Value="15">8:00</asp:ListItem>
                                                    <asp:ListItem Selected="False" Value="16">8:30</asp:ListItem>
                                                    <asp:ListItem Selected="False" Value="17">9:00</asp:ListItem>
                                                    <asp:ListItem Selected="False" Value="18">9:30</asp:ListItem>
                                                    <asp:ListItem Selected="False" Value="19">10:00</asp:ListItem>
                                                    <asp:ListItem Selected="False" Value="20">10:30</asp:ListItem>
                                                    <asp:ListItem Selected="False" Value="21">11:00</asp:ListItem>
                                                    <asp:ListItem Selected="False" Value="22">11:30</asp:ListItem>
                                                </asp:DropDownList>
                                                <div class="radio">
                                                    <label>
                                                        <input id="chkAM" type="radio" checked value="0" name="AM" runat="server" />
                                                        <b>AM</b>
                                                    </label>
                                                </div>
                                                <div class="radio">
                                                    <label>
                                                        <input id="chkPM" type="radio" value="1" name="AM" runat="server" />
                                                        <b>PM</b>
                                                    </label>
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <div id="tdStartDay" runat="server">
                                                <asp:RadioButton ID="radStartFullDay" runat="server" Checked="true" Text="Full Day" GroupName="rad1" />
                                                <asp:RadioButton ID="radStartHalfDay" runat="server" Text="Half Day" GroupName="rad1" />
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6">
                                <div class="form-group">
                                    <label>To Date</label>
                                    <ul class="list-inline">
                                        <li>
                                            <div class="form-inline">
                                                <BizCalendar:Calendar ID="calto" runat="server" />
                                            </div>
                                        </li>
                                        <li id="tdEndTime" runat="server">
                                            <div class="form-inline">
                                                <img src="../images/Clock-16.gif" align="absMiddle" />
                                                <asp:DropDownList ID="ddlEndTime" runat="server" CssClass="form-control">
                                                    <asp:ListItem Selected="False" Value="23">12:00</asp:ListItem>
                                                    <asp:ListItem Selected="False" Value="24">12:30</asp:ListItem>
                                                    <asp:ListItem Selected="False" Value="1">1:00</asp:ListItem>
                                                    <asp:ListItem Selected="False" Value="2">1:30</asp:ListItem>
                                                    <asp:ListItem Selected="False" Value="3">2:00</asp:ListItem>
                                                    <asp:ListItem Selected="False" Value="4">2:30</asp:ListItem>
                                                    <asp:ListItem Selected="False" Value="5">3:00</asp:ListItem>
                                                    <asp:ListItem Selected="False" Value="6">3:30</asp:ListItem>
                                                    <asp:ListItem Selected="False" Value="7">4:00</asp:ListItem>
                                                    <asp:ListItem Selected="False" Value="8">4:30</asp:ListItem>
                                                    <asp:ListItem Selected="False" Value="9">5:00</asp:ListItem>
                                                    <asp:ListItem Selected="False" Value="10">5:30</asp:ListItem>
                                                    <asp:ListItem Selected="False" Value="11">6:00</asp:ListItem>
                                                    <asp:ListItem Selected="False" Value="12">6:30</asp:ListItem>
                                                    <asp:ListItem Selected="False" Value="13">7:00</asp:ListItem>
                                                    <asp:ListItem Selected="False" Value="14">7:30</asp:ListItem>
                                                    <asp:ListItem Selected="False" Value="15">8:00</asp:ListItem>
                                                    <asp:ListItem Selected="True" Value="16">8:30</asp:ListItem>
                                                    <asp:ListItem Selected="False" Value="17">9:00</asp:ListItem>
                                                    <asp:ListItem Selected="False" Value="18">9:30</asp:ListItem>
                                                    <asp:ListItem Selected="False" Value="19">10:00</asp:ListItem>
                                                    <asp:ListItem Selected="False" Value="20">10:30</asp:ListItem>
                                                    <asp:ListItem Selected="False" Value="21">11:00</asp:ListItem>
                                                    <asp:ListItem Selected="False" Value="22">11:30</asp:ListItem>
                                                </asp:DropDownList>
                                                <div class="radio">
                                                    <label>
                                                        <input id="chkEndAM" type="radio" checked value="0" name="EndAM" runat="server" />
                                                        <b>AM</b>
                                                    </label>
                                                </div>
                                                <div class="radio">
                                                    <label>
                                                        <input id="chkEndPM" type="radio" value="1" name="EndAM" runat="server" />
                                                        <b>PM</b>
                                                    </label>
                                                </div>
                                            </div>
                                        </li>
                                        <li id="tdEndDay" runat="server">
                                            <div>
                                                <asp:RadioButton ID="radEndFullday" runat="server" Checked="true" Text="Full Day"
                                                    GroupName="rad2" />
                                                <asp:RadioButton ID="radEndHalfDay" runat="server" Text="Half Day" GroupName="rad2" />
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </asp:Panel>
                    <div class="row" id="trAmount" runat="server">
                        <div class="col-xs-12 col-sm-6">
                            <div class="form-group">
                                <label>
                                    <asp:HyperLink ID="hplRec" runat="server" Text="Rate/Hour"></asp:HyperLink></label>
                                <asp:TextBox ID="txtRate" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6">
                            <div class="form-group" id="tdExpenseAcc" runat="server">
                                <label>Expense Account</label>
                                <asp:DropDownList ID="ddlExpenseAccount" runat="server" CssClass="form-control"></asp:DropDownList>
                            </div>
                        </div>
                    </div>
                    <div class="row" id="trAssignTo" runat="server" visible="false">
                        <div class="col-xs-12">
                            <div class="form-group">
                                <label>
                                    Assign to
                                    <asp:Label runat="server" ID="lblAssignto" ForeColor="#ff3333" Text="*"></asp:Label></label>

                                <asp:RadioButtonList ID="rblAssignto" runat="server" AutoPostBack="True" Font-Bold="false" RepeatDirection="Horizontal">
                                    <asp:ListItem Text="Project" Value="1"></asp:ListItem>
                                    <asp:ListItem Text="Sales Order / Invoice" Value="3"></asp:ListItem>
                                </asp:RadioButtonList>
                            </div>
                        </div>
                    </div>
                    <div class="row" id="trDesc" runat="server">
                        <div class="col-xs-12">
                            <div class="form-group">
                                <label>Description</label>
                                <asp:TextBox ID="txtDesc" runat="server" CssClass="form-control" TextMode="MultiLine"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <asp:HiddenField ID="hdnDate" runat="server" />
    <uc1:TransactionInfo ID="TransactionInfo1" runat="server" />
</asp:Content>
