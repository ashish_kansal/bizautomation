﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/common/PopupBootstrap.Master" CodeBehind="frmTime.aspx.vb" Inherits="BACRM.UserInterface.TimeAndExpense.frmTime" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../CSS/jquery-ui.css" rel="stylesheet" />
    <link rel="stylesheet" href="../CSS/select2.css" />
    <style type="text/css">
        .hiddenClass {
            visibility: hidden;
        }

        .visibleClass {
            visibility: visible;
        }

        .notextwrap {
            white-space: nowrap;
        }

        a.select2-choice {
            height: 34px !important;
            padding: 3px 0 0 8px !important;
        }

        #tblWeek .spinnerhour {
            width: 57px;
        }

        #tblWeek .spinnerminute {
            width: 57px;
        }

        #tblWeek input[type=number]::-webkit-inner-spin-button,
        #tblWeek input[type=number]::-webkit-outer-spin-button {
            -webkit-appearance: none;
            -moz-appearance: textfield;
            margin: 0;
        }

        #tblWeek .spinner input, #tblWeek .spinner input::-webkit-outer-spin-button, #tblWeek .spinner input::-webkit-inner-spin-button {
            text-align: right;
            -webkit-appearance: none;
            margin: 0;
            -moz-appearance: textfield;
        }

        #tblWeek .input-group-btn-vertical > .btn:focus, #tblWeek .input-group-btn-vertical > .btn:active, #tblWeek .input-group-btn-vertical > .btn:hover {
            color: #333;
            background-color: #e6e6e6;
            border-color: #d2d6de !important;
        }

        #tblWeek .input-group-btn-vertical {
            position: relative;
            white-space: nowrap;
            width: 1%;
            vertical-align: middle;
            display: table-cell;
        }

            #tblWeek .input-group-btn-vertical > .btn {
                display: block;
                float: none;
                width: 100%;
                max-width: 100%;
                padding: 8px;
                margin-left: -1px;
                position: relative;
                border-radius: 0;
            }

                #tblWeek .input-group-btn-vertical > .btn:last-child {
                    margin-top: -2px;
                }

            #tblWeek .input-group-btn-vertical i {
                position: absolute;
                top: 0;
                left: 4px;
            }
    </style>
    <script src="../JavaScript/jquery-ui.min.js" type="text/javascript"></script>
    <script type='text/javascript' src="../JavaScript/select2.js"></script>
    <script type="text/javascript">
        Date.prototype.getDayName = function (lang) {
            lang = lang && (lang in Date.locale) ? lang : 'en';
            return Date.locale[lang].day_names[this.getDay()];
        };

        Date.prototype.getDayNameShort = function (lang) {
            lang = lang && (lang in Date.locale) ? lang : 'en';
            return Date.locale[lang].day_names_short[this.getDay()];
        };

        Date.prototype.getMonthName = function (lang) {
            lang = lang && (lang in Date.locale) ? lang : 'en';
            return Date.locale[lang].month_names[this.getMonth()];
        };

        Date.prototype.getMonthNameShort = function (lang) {
            lang = lang && (lang in Date.locale) ? lang : 'en';
            return Date.locale[lang].month_names_short[this.getMonth()];
        };

        Date.locale = {
            en: {
                month_names: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
                month_names_short: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
                day_names: ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
                day_names_short: ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"]
            }
        };

        $(document).ajaxStart(function () {
            $("#divLoader").show();
        }).ajaxStop(function () {
            $("#divLoader").hide();
        });

        $(document).ready(function () {
            var curr = new Date(); // get current date

            $("#dtWeek").datepicker({
                dateFormat: "mm/dd/yy",
                changeYear: true,
                onSelect: function (selectedDate) {
                    LoadWeek(new Date(selectedDate));
                }
            }).datepicker("setDate", curr);

            $.when(GetEmployeeHourRate()).then(function () {
                LoadWeek(curr);
            });

            $("#btnShowDatePicker").click(function () {
                $("#dtWeek").datepicker("show");
            });
        });

        function LoadWeek(curr) {
            var html = "";

            if (curr != null) {
                var curr = new Date(curr.getFullYear(), curr.getMonth(), curr.getDate(), 0, 0, 0, 0);
                var first = curr.getDate() - curr.getDay() + 1; // First day is the day of the month - the day of the week
                var last = first + 6; // last day is the first day + 6

                var weekStart = new Date(curr.setDate(first));
                var weekEnd = new Date(curr.setDate(last));
                var weekStartUTC = new Date(weekStart.getUTCFullYear(), weekStart.getUTCMonth(), weekStart.getUTCDate(), weekStart.getUTCHours(), weekStart.getUTCMinutes(), weekStart.getUTCSeconds());

                while (weekStart <= weekEnd) {
                    html = html + getRowHtml(weekStart, weekStartUTC, false);
                    weekStart = new Date(weekStart.getFullYear(), weekStart.getMonth(), weekStart.getDate() + 1);
                    weekStartUTC = new Date(weekStart.getUTCFullYear(), weekStart.getUTCMonth(), weekStart.getUTCDate(), weekStart.getUTCHours(), weekStart.getUTCMinutes(), weekStart.getUTCSeconds());
                }
            }

            $("#tblWeek tr:gt(1)").remove();
            $("#tblWeek").append(html);

            LoadCustomerAutoComplete();
        }

        function AddNewEntry(control, weekStart) {
            var html = "";
            weekStart = new Date(weekStart);
            var weekStartUTC = new Date(weekStart.getUTCFullYear(), weekStart.getUTCMonth(), weekStart.getUTCDate(), weekStart.getUTCHours(), weekStart.getUTCMinutes(), weekStart.getUTCSeconds());

            var tr = $(control).closest("tr");
            $(tr).after(getRowHtml(weekStart, weekStartUTC, true));

            $($("#tblWeek tr.tr" + weekStart.getDayNameShort())[0]).find("td:first").attr("rowspan", $("#tblWeek tr.tr" + weekStart.getDayNameShort()).length);

            LoadCustomerAutoComplete();
        }

        function getRowHtml(weekStart, weekStartUTC, isAddNewEntry) {
            var html = ""

            if (weekStart.getDayNameShort() === "Sat" || weekStart.getDayNameShort() === "Sun") {
                html += "<tr class='weekend tr" + weekStart.getDayNameShort() + "' style='display: none'>";
            } else {
                html += "<tr class='tr" + weekStart.getDayNameShort() + "'>";
            }

            if (isAddNewEntry) {
                html += "";
            } else {
                html += "<td class='notextwrap'>";
                html += "<div class='row'>";
                html += "<div class='col-xs-12'>";
                html += "<div class='pull-left'>";
                html += "<b>" + weekStart.getDate() + " " + weekStart.getMonthNameShort() + " " + weekStart.getFullYear() + " (" + weekStart.getDayNameShort() + ")</b>";
                html += "</div>";
                html += "<div class='pull-right'>";
                html += "<input type='hidden' class='hdnLocalDate' value='" + weekStart.toLocaleString() + "' />";
                html += "<a href='javascript:void(0);' onclick='AddNewEntry(this,\"" + weekStart.toLocaleString() + "\")' class='btn btn-xs btn-primary' title='Add new entry'><i class='fa fa-plus' aria-hidden='true'></i></a>";
                html += "</div>";
                html += "</div>";
                html += "</div>";
                html += "</td>";
            }

            html += "<td class='notextwrap'>";
            html += "<div class='input-group spinner spinnerhour'>";
            html += "<input type='number' class='form-control work-hours' min='0' max='24' value='0' />";
            html += "<div class='input-group-btn-vertical'>";
            html += "<button class='btn btn-default' type='button'><i class='fa fa-caret-up'></i></button>";
            html += "<button class='btn btn-default' type='button'><i class='fa fa-caret-down'></i></button>";
            html += "</div>";
            html += "</div>";
            html += "</td>";
            html += "<td class='notextwrap'>";
            html += "<div class='input-group spinner spinnerminute'>";
            html += "<input type='number' class='form-control  work-minutes' min='0' max='59' value='0' />";
            html += "<div class='input-group-btn-vertical'>";
            html += "<button class='btn btn-default' type='button'><i class='fa fa-caret-up'></i></button>";
            html += "<button class='btn btn-default' type='button'><i class='fa fa-caret-down'></i></button>";
            html += "</div>";
            html += "</div>";
            html += "</td>";
            html += "<td>";
            html += "<textarea class='form-control work-notes' rows='3'></textarea>";
            html += "</td>";
            html += "<td class='notextwrap'>";
            html += "<ul class='list-inline'>";
            html += "<li style='vertical-align:middle'>";
            html += "<ul class='list-unstyled'>";
            html += "<li>";
            html += "<input type='checkbox' class='billable' onChange='BillableChanged(this);' />";
            html += "Billable";
            html += "</li>";
            html += "<li>";
            html += "<input type='checkbox' class='absent' onChange='AbsentChanged(this);' />";
            html += "Absent";
            html += "</li>";
            html += "</ul>";
            html += "</li>";
            html += "<li style='vertical-align:middle; display:none;' class='liAbsentDetail'>";
            html += "<input type='checkbox' class='chkPaidLeave' /> Is Paid Leave";
            html += "<li>";
            html += "<li style='vertical-align:middle; display:none;' class='liBillableDetail'>";
            html += "<ul class='list-inline'>";
            html += "<li style='vertical-align:middle'>";
            html += "<input type='number' class='txtRatePerHour form-control' style='width:60px' value='" + $("#hdnEmployeeHourRate").val() + "' />"
            html += "</li>";
            html += "<li style='vertical-align:middle'> / Hour</li>";
            html += "<li style='vertical-align:middle'>";
            html += "<input type='text' class='txtCustomer' />";
            html += "</li>";
            html += "<li style='vertical-align:middle'>";
            html += "<select class='form-control ddlSalesOrder'>";
            html += "<option value='0'>-- Select Order --</option>";
            html += "</select>";
            html += "</li>";
            html += "<li style='vertical-align:middle'>";
            html += "<select class='form-control ddlServiceItem'>";
            html += "<option value='0'>-- Service Item --</option>";

            var obj = $.parseJSON($("[id$=hdnServiceItemData]").val());
            if (obj != null && obj.length > 0) {
                obj.forEach(function (e) {
                    html += "<option value='" + e.numItemCode + "'>" + replaceNull(e.vcItemName) + "</option>";
                });
            }

            html += "</select>";
            html += "</li>";
            html += "</ul>";
            html += "</li>";
            html += "</ul>";
            html += "</td>";
            html += "<td>";
            html += "<a href='javascript:deleteTimeEntry(this);' class='btn btn-xs btn-danger " + (isAddNewEntry ? "" : "disabled") + "'><i class='fa fa-trash-o'></i></a>";
            html += "</td>";
            html += "</tr>";
            return html;
        }

        function DeleteTimeEntry(control, weekStart) {
            weekStart = new Date(weekStart);
            $(control).closest("tr").remove();
            $($("#tblWeek tr.tr" + weekStart.getDayNameShort())[0]).find("td:first").attr("rowspan", $("#tblWeek tr.tr" + weekStart.getDayNameShort()).length);
        }

        function pad(str, max) {
            str = str.toString();
            return str.length < max ? pad("0" + str, max) : str;
        }

        function BillableChanged(chk) {
            if ($(chk).is(":checked")) {
                $(chk).closest("tr").find("input[type='checkbox'].absent").prop("checked", false);
                $(chk).closest("tr").find("li.liBillableDetail").show();
                $(chk).closest("tr").find("li.liAbsentDetail").hide();
            } else {
                $(chk).closest("tr").find("li.liBillableDetail").hide();
                
            }
        }

        function AbsentChanged(chk) {
            if ($(chk).is(":checked")) {
                $(chk).closest("tr").find("input[type='checkbox'].billable").prop("checked", false);
                $(chk).closest("tr").find("li.liBillableDetail").hide();
                $(chk).closest("tr").find("input.work-hours").val("0");
                $(chk).closest("tr").find("nput.work-minutes").val("0");
                $(chk).closest("tr").find("li.liAbsentDetail").show();
            } else {
                $(chk).closest("tr").find("li.liAbsentDetail").hide();
            }         
        }

        function ShowWeekendChanged(chkShowWeekend) {
            if ($(chkShowWeekend).is(":checked")) {
                $("#tblWeek tr.weekend").show();
            } else {
                $("#tblWeek tr.weekend").hide();
            }
        }

        function EmployeeChanged() {
            $.when(GetEmployeeHourRate()).then(function () {
                LoadWeek($.datepicker.parseDate("mm/dd/yy", $("#dtWeek")[0].value));
            });
        }

        function GetEmployeeHourRate() {
            try {
                var dataString = "{EmpId:" + $("#<%=ddlEmployee.ClientID%>").val() + "}";

                return $.ajax({
                    type: "POST",
                    url: window.location.pathname + "/FillHourlyRate",
                    data: dataString,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (result) {
                        $("#hdnEmployeeHourRate").val(parseFloat(result.d).toFixed(2));
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        if (IsJsonString(jqXHR.responseText)) {
                            var objError = $.parseJSON(jqXHR.responseText)
                            alert("Error occurred: " + replaceNull(objError.Message));
                        } else {
                            alert("Unknown error ocurred");
                        }
                    }
                });
            } catch (e) {
                alert("Unknown error occurred while fetching employee hourly rate.");
            }
        }

        function IsJsonString(str) {
            try {
                JSON.parse(str);
            } catch (e) {
                return false;
            }
            return true;
        }


        function replaceNull(value) {
            return String(value) === "null" || String(value) === "undefined" ? "" : value.toString().replace(/'/g, "&#39;");
        }

        function LoadCustomerAutoComplete() {
            $('.txtCustomer').each(function (i, obj) {
                if (!$(obj).data('select2')) {
                    $(obj).select2({
                        placeholder: 'Select customer',
                        multiple: false,
                        width: "180px",
                        dropdownCssClass: 'bigdrop',
                        minimumInputLength: 1,
                        dataType: "json",
                        allowClear: true,
                        ajax: {
                            quietMillis: 500,
                            url: '../common/Common.asmx/GetSearchedCustomers',
                            type: 'POST',
                            params: {
                                contentType: 'application/json; charset=utf-8'
                            },
                            dataType: 'json',
                            data: function (term, page) {
                                return JSON.stringify({
                                    searchText: term,
                                    pageIndex: page,
                                    pageSize: 10
                                });
                            },
                            results: function (data, page) {
                                if (data.hasOwnProperty("d")) {
                                    if (data.d == "Session Expired") {
                                        alert("Session expired.");
                                    } else {
                                        data = $.parseJSON(data.d)
                                    }
                                }
                                else
                                    data = $.parseJSON(data);

                                return { results: $.parseJSON(data.results), more: ((page.page * 10) < data.Total) };
                            }
                        }
                    });

                    $(obj).on("change", function (e) {
                        if (e.added != null) {
                            FillSalesOrder($(this), e.added.id);
                        }
                    });

                    $(obj).on("select2-removed", function (e) {
                        $($(this).closest('tr').find('.ddlSalesOrder').get(0)).find('option').not(':first').remove();
                    });
                }
            });

            $('.spinnerhour .btn:first-of-type').on('click', function () {
                if (parseInt($(this).closest("div.spinnerhour").find('input').val(), 10) + 1 >= 0 && parseInt($(this).closest("div.spinnerhour").find('input').val(), 10) + 1 <= 24) {
                    $(this).closest("div.spinnerhour").find('input').val(parseInt($(this).closest("div.spinnerhour").find('input').val(), 10) + 1);
                }
            });
            $('.spinnerhour .btn:last-of-type').on('click', function () {
                if (parseInt($(this).closest("div.spinnerhour").find('input').val(), 10) - 1 >= 0 && parseInt($(this).closest("div.spinnerhour").find('input').val(), 10) - 1 <= 24) {
                    $(this).closest("div.spinnerhour").find('input').val(parseInt($(this).closest("div.spinnerhour").find('input').val(), 10) - 1);
                }
            });

            $('.spinnerminute .btn:first-of-type').on('click', function () {
                if (parseInt($(this).closest("div.spinnerhour").find('input').val(), 10) + 1 >= 0 && parseInt($(this).closest("div.spinnerhour").find('input').val(), 10) + 1 <= 59) {
                    $(this).closest("div.spinnerhour").find('input').val(parseInt($(this).closest("div.spinnerhour").find('input').val(), 10) + 1);
                }
            });
            $('.spinnerminute .btn:last-of-type').on('click', function () {
                if (parseInt($(this).closest("div.spinnerhour").find('input').val(), 10) - 1 >= 0 && parseInt($(this).closest("div.spinnerhour").find('input').val(), 10) - 1 <= 59) {
                    $(this).closest("div.spinnerhour").find('input').val(parseInt($(this).closest("div.spinnerhour").find('input').val(), 10) - 1);
                }
            });

            $("input[type='number']").change(function () {
                var max = parseInt($(this).attr('max'));
                var min = parseInt($(this).attr('min'));
                if ($(this).val() > max) {
                    $(this).val(max);
                }
                else if ($(this).val() < min) {
                    $(this).val(min);
                }
            });
        }

        function FillSalesOrder(obj, divID) {
            $.ajax({
                type: "POST",
                url: window.location.pathname + "/FillSalesOrder",
                data: "{DivID:" + divID + ",DomainID:" + <%=Session("DomainID") %> + "}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (result) {
                    $($(obj).closest('tr').find('.ddlSalesOrder').get(0)).find('option').not(':first').remove();

                    var json = eval(result.d);
                    if (result.d !== '') {
                        for (i = 0; i < json.length; i++) {
                            $($(obj).closest('tr').find('.ddlSalesOrder').get(0)).append("<option value='" + json[i].id + "'>" + json[i].value + "</option>");
                        }
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    if (IsJsonString(jqXHR.responseText)) {
                        var objError = $.parseJSON(jqXHR.responseText)
                        alert("Error occurred while loading sales order: " + replaceNull(objError.Message));
                    } else {
                        alert("Unknown error ocurred while loading sales order");
                    }
                }
            });
        }

        function Save(isFromSaveClose) {
            if ($('[id$=ddlEmployee] option:selected').val() == 0) {
                alert("Please Select Employee");
                $('[id$=ddlEmployee]').focus();
                return false;
            }

            var timeEntries = [];
            var isValid = true;
            var Message = "";
            

            $("#tblWeek tr:gt(1)").each(function () {
                var objItem = {};
                objItem.EmployeeID = parseInt($('[id$=ddlEmployee]').val());

                // In case rowspan
                var tr = $(this);
                while (tr.find("input.hdnLocalDate").length == 0 && !tr.find('td:first[rowspan]').length > 0) {
                    tr = tr.prev();
                }

                objItem.Date = new Date(tr.find("input.hdnLocalDate").val()).toGMTString();
                objItem.Notes = replaceNull($(this).find("textarea.work-notes").val());
                if ($(this).find("input.absent").is(":checked")) {
                    objItem.IsAbsent = true;
                    objItem.IsPaidLevel = $(this).find("input.chkPaidLeave").is(":checked");
                    objItem.Hours = 0;
                    objItem.Minutes = 0;
                    objItem.RatePerHour = 0;
                    objItem.DivisionID = 0;
                    objItem.OppID = 0;
                    objItem.ItemID = 0;
                } else {
                    objItem.IsAbsent = false;
                    objItem.Hours = parseInt($(this).find("input.work-hours").val());
                    objItem.Minutes = parseInt($(this).find("nput.work-minutes").val());
                }

                if ($(this).find("input.billable").is(":checked")) {
                    objItem.IsBillable = true;
                    objItem.RatePerHour = parseFloat($(this).find("input.txtRatePerHour").val());
                    objItem.DivisionID = parseInt($(this).find(".txtCustomer").val());
                    objItem.CompanyName = replaceNull($(this).find(".txtCustomer").text());
                    objItem.OppID = parseInt($(this).find("select.ddlSalesOrder").val());
                    objItem.ItemID = parseInt($(this).find("select.ddlServiceItem").val().split("~")[0]);
                } else {
                    objItem.IsBillable = false;
                    objItem.RatePerHour = 0;
                    objItem.DivisionID = 0;
                    objItem.OppID = 0;
                    objItem.ItemID = 0;
                }

                if (objItem.IsBillable && objItem.RatePerHour == 0) {
                    errorMessage = "Billable RatePerHour must be greater than Zero";
                    isValid = false;
                    return false;
                }

                if (objItem.IsBillable && objItem.DivisionID == 0) {
                    errorMessage = "Billable time must be assigned to customer";
                    $(this).find(".txtCustomer").focus();
                    isValid = false;
                    return false;
                }

                if (objItem.IsBillable && objItem.OppID == 0) {
                    errorMessage = "Billable time must be assign to sales order";
                    $(this).find("select.ddlSalesOrder").focus();
                    isValid = false;
                    return false;
                }

                if (objItem.IsBillable && objItem.ItemID == 0) {
                    errorMessage = "Billable time must be assign to service item";
                    $(this).find("select.ddlServiceItem").focus();
                    isValid = false;
                    return false;
                }

                if (objItem.IsAbsent || (objItem.Hours > 0 || objItem.Minutes > 0)) {
                    timeEntries.push(objItem);
                }                
            });

            if (!isValid) {
                alert(errorMessage);
                return false;
            } else if (timeEntries.length == 0) {
                alert("Enter at least one time entry for a selected week.");
                return false;
            } else {
                $.ajax({
                    type: "POST",
                    url: window.location.pathname + "/SaveEntries",
                    data: "{employeeID:" + $("[id$=ddlEmployee]").val() + ",classID:" + $("[id$=ddlClass]").val() + ",timeEntries:'" + JSON.stringify(timeEntries) + "'}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (result) {
                        alert("Time entries are saved successfully.");

                        if (isFromSaveClose) {
                            RefereshParentAndClose();
                        } else {
                            EmployeeChanged();
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        if (IsJsonString(jqXHR.responseText)) {
                            var objError = $.parseJSON(jqXHR.responseText)
                            alert("Error occurred while saving time entries: " + replaceNull(objError.Message));
                        } else {
                            alert("Unknown error ocurred while saving time entries.");
                        }
                    }
                });
            }

            return false;
        }

        function RefereshParentAndClose() {
            if (window.opener != null) {
                window.opener.location.href = window.opener.location.href;
            }

            close();
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server">
    <div class="overlay" id="divLoader" style="z-index: 10000">
        <div class="overlayContent" style="color: #000; background-color: #fff; text-align: center; width: 250px; padding: 20px">
            <i class="fa fa-2x fa-refresh fa-spin"></i>
            <h3>Please wait...</h3>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="pull-right">
                <asp:Button ID="btnSave" runat="server" CssClass="btn btn-primary" Text="Save" OnClientClick="return Save(false);" />
                <asp:Button ID="btnSaveClose" runat="server" CssClass="btn btn-primary" Text="Save & Close" OnClientClick="return Save(true);" />
                <asp:Button ID="btnClose" runat="server" CssClass="btn btn-primary" Text="Close" OnClientClick="return Close();" />
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitle" runat="server">
    Time
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Content" runat="server">
    <asp:ScriptManager runat="server"></asp:ScriptManager>
    <div class="row padbottom10" id="divError" runat="server" style="display: none">
        <div class="col-sm-12">
            <div class="alert alert-danger">
                <h4><i class="icon fa fa-ban"></i>Error</h4>
                <p>
                    <asp:Label ID="lblError" runat="server"></asp:Label>
                </p>
            </div>
        </div>
    </div>
    <div class="row padbottom10">
        <div class="col-xs-12">
            <div class="pull-left">
                <div class="form-inline">
                    <div class="form-group">
                        <label>Week</label>
                        <div class="input-group">
                            <input type="text" id="dtWeek" class="form-control" placeholder="MM/DD/YYYY" style="width: 100px;" />
                            <span class="input-group-btn">
                                <button class="btn btn-default" type="button" id="btnShowDatePicker">
                                    <i class="fa fa-calendar" aria-hidden="true"></i>
                                </button>
                            </span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Employee/Vendor<span style="color: red"> *</span></label>
                        <asp:DropDownList runat="server" ID="ddlEmployee" onchange="EmployeeChanged()" CssClass="form-control"></asp:DropDownList>&nbsp;
                    </div>
                    <div class="form-group">
                        <label>Class</label>
                        <asp:DropDownList runat="server" ID="ddlClass" CssClass="form-control"></asp:DropDownList>
                    </div>
                </div>
            </div>
            <div class="pull-right">
                <asp:CheckBox ID="chkShowWeekend" runat="server" onclick="ShowWeekendChanged(this);" />
                <label for="chkShowWeekend">Show weekends</label>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="table-responsive">
                <table class="table table-bordered" id="tblWeek">
                    <tr>
                        <th rowspan="2" style="width: 165px;">Day</th>
                        <th colspan="2">Time</th>
                        <th rowspan="2" style="max-width: 300px;">Notes</th>
                        <th rowspan="2">Billable/Absent</th>
                        <th rowspan="2" style="width: 25px;"></th>
                    </tr>
                    <tr>
                        <th style="width: 74px;">Hours</th>
                        <th style="width: 74px;">Minutes</th>
                    </tr>
                </table>
            </div>
        </div>
    </div>

    <input type="hidden" id="hdnEmployeeHourRate" />
    <asp:HiddenField ID="hdnServiceItemData" runat="server" />
</asp:Content>
