Imports BACRM.BusinessLogic.TimeAndExpense
Imports BACRM.BusinessLogic.Contacts
Imports BACRM.BusinessLogic.Common

Namespace BACRM.UserInterface.TimeAndExpense
    Partial Public Class frmAddAmount : Inherits BACRMPage

        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            Try
                If Not IsPostBack Then

                    
                    objCommon.sb_FillComboFromDBwithSel(ddlAmtCat, 47, Session("DomainID"))
                    Dim objContacts As New CContacts
                    objContacts.DomainID = Session("DomainID")
                    ddlUser.DataSource = objContacts.EmployeeList
                    ddlUser.DataTextField = "vcUserName"
                    ddlUser.DataValueField = "numContactID"
                    ddlUser.DataBind()
                    ddlUser.Items.Insert(0, "--Select Employee--")
                    ddlUser.Items.FindByText("--Select Employee--").Value = 0
                    cal.SelectedDate = Now()
                End If
                btnClose.Attributes.Add("onclick", "return Close()")
                btnAdd.Attributes.Add("onclick", "return Save()")
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Sub LoadDetails()
            Try
                Dim objTimeExp As New TimeExpenseLeave
                objTimeExp.UserCntID = ddlUser.SelectedItem.Value
                objTimeExp.byteMode = 1
                dgAddAmt.DataSource = objTimeExp.ManageAddAmount
                dgAddAmt.DataBind()
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub ddlUser_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlUser.SelectedIndexChanged
            Try
                LoadDetails()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub dgAddAmt_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgAddAmt.ItemCommand
            Try
                If e.CommandName = "Delete" Then
                    Dim objTimeExp As New TimeExpenseLeave
                    objTimeExp.byteMode = 2
                    objTimeExp.AmtAddHDRID = e.Item.Cells(0).Text
                    objTimeExp.ManageAddAmount()
                    objTimeExp.byteMode = 1
                    objTimeExp.UserCntID = ddlUser.SelectedValue
                    dgAddAmt.DataSource = objTimeExp.ManageAddAmount
                    dgAddAmt.DataBind()
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
            Try
                Dim objTimeExp As New TimeExpenseLeave
                objTimeExp.UserCntID = ddlUser.SelectedValue
                objTimeExp.byteMode = 0
                objTimeExp.AmtCategory = ddlAmtCat.SelectedValue
                objTimeExp.dtAmtAdded = cal.SelectedDate
                objTimeExp.Amount = txtAmt.Text.Trim
                objTimeExp.ManageAddAmount()
                objTimeExp.byteMode = 1
                dgAddAmt.DataSource = objTimeExp.ManageAddAmount
                dgAddAmt.DataBind()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Function Formatdate(ByVal CloseDate As Date) As String
            Try
                Dim strTargetResolveDate As String = ""
                strTargetResolveDate = FormattedDateFromDate(CloseDate, Session("DateFormat"))
                Return strTargetResolveDate
            Catch ex As Exception
                Throw ex
            End Try
        End Function

    End Class
End Namespace