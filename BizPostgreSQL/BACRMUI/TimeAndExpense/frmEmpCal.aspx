<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" CodeBehind="frmEmpCal.aspx.vb"
    Inherits="BACRM.UserInterface.TimeAndExpense.frmEmpCal" MasterPageFile="~/common/GridMasterRegular.Master" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register TagPrefix="BizCalendar" TagName="Calendar" Src="../include/calandar.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Time & Expense</title>
    <style type="text/css">
        .RadGrid_Default, .RadGrid_Default .rgMasterTable, .RadGrid_Default .rgDetailTable, .RadGrid_Default .rgGroupPanel table, .RadGrid_Default .rgCommandRow table, .RadGrid_Default .rgEditForm table, .RadGrid_Default .rgPager table, .GridToolTip_Default {
            font:inherit !important;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function ($) {
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_pageLoaded(pageLoaded);

        });

        function pageLoaded() {
            $("div[id$=rgTime]").removeClass("RadGrid_Default");
            $("div[id$=rgExpense]").removeClass("RadGrid_Default");

            $("[id$=ddlYearTime]").change(function () {
                $("[id$=btnSearchTime]").click();
            });

            $("[id$=ddlMonthTime]").change(function () {
                $("[id$=btnSearchTime]").click();
            });

            $("[id$=ddlDateTime]").change(function () {
                $("[id$=btnSearchTime]").click();
            });
        }

        function OpenAddTimeWindow() {
            var h = screen.height;
            var w = screen.width;

            window.open('../TimeAndExpense/frmTime.aspx', '', 'toolbar=no,titlebar=no,top=50,left=50,width=' + (w - 100) + ',height=' + (h - 200) + ',scrollbars=yes,resizable=yes');

            return false;
        }

        function OpenAddExpenseWindow() {
            var h = screen.height;
            var w = screen.width;

            window.open('../TimeAndExpense/frmExpense.aspx', '', 'toolbar=no,titlebar=no,top=50,left=50,width=' + (w - 100) + ',height=' + (h - 200) + ',scrollbars=yes,resizable=yes');

            return false;
        }

        function FilterTimeGrid() {
            if (window.event.keyCode == 13) {
                $("[id$=btnSearchTime]").click();
                return true;
            } else {
                return false;
            }
        }

        function OpenTimeExpenseEdit(recordID) {
            var h = screen.height;
            var w = screen.width;

            window.open('../TimeAndExpense/frmEditTimeExp.aspx?CatID=' + recordID, '', 'toolbar=no,titlebar=no,top=50,left=50,width=500,height=' + (h - 200) + ',scrollbars=yes,resizable=yes');

            return false;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server"
    ClientIDMode="Static">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <div id="divMessage" runat="server" class="row padbottom10" style="display: none">
        <div class="col-xs-12">
            <div class="alert alert-warning">
                <h4><i class="icon fa fa-warning"></i>Alert!</h4>
                <asp:Label ID="litMessage" EnableViewState="false" runat="server"></asp:Label>
            </div>
        </div>
    </div>
    <asp:Button ID="btnClose" runat="server" Style="width: 50px; margin-right: 14px; display: none;" Text="Close" CssClass="button" />
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server" ClientIDMode="Static">
    Time & Expense&nbsp;<a href="#" onclick="return OpenHelpPopUp('timeandexpense/frmempcal.aspx')"><label class="badge bg-yellow">?</label></a>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BizPager" runat="server" ClientIDMode="Static">
    &nbsp;
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="GridPlaceHolder" runat="server" ClientIDMode="Static">
    <telerik:RadTabStrip ID="radTimeExpTab" runat="server" ClickSelectedTab="true" EnableEmbeddedSkins="true" Skin="Default" MultiPageID="radMultiPage_TimeExpTab" OnTabClick="radTimeExpTab_TabClick">
        <Tabs>
            <telerik:RadTab Text="Time Registry" Value="TimeRegistry" PageViewID="radPageView_TimeRegistry" Selected="true">
            </telerik:RadTab>
            <telerik:RadTab Text="Expense Registry" Value="ExpenseRegistry" PageViewID="radPageView_ExpenseRegistry" Selected="false">
            </telerik:RadTab>
        </Tabs>
    </telerik:RadTabStrip>
    <telerik:RadMultiPage ID="radMultiPage_TimeExpTab" runat="server" CssClass="pageView">
        <telerik:RadPageView ID="radPageView_TimeRegistry" runat="server">
            <div class="row padbottom10">
                <div class="col-xs-12">
                    <div class="pull-left">
                        <div class="form-inline">
                            <div class="form-group" id="divPayrollTime" runat="server">
                                <label>Current Pay Period:</label>
                                <asp:DropDownList ID="ddlYearTime" runat="server" CssClass="form-control">
                                </asp:DropDownList>
                                <asp:DropDownList ID="ddlMonthTime" runat="server" CssClass="form-control">
                                </asp:DropDownList>
                                <asp:DropDownList ID="ddlDateTime" runat="server" CssClass="form-control">
                                </asp:DropDownList>
                            </div>
                            <div class="form-group">
                                <label>Department:</label>
                                <asp:DropDownList ID="ddlDepartmentTime" runat="server" CssClass="signup form-control" AutoPostBack="true">
                                </asp:DropDownList>
                            </div>
                        </div>
                    </div>
                    <div class="pull-right">
                        <div class="form-inline">
                            <div class="form-group">
                                <label>From:</label>
                                <BizCalendar:Calendar ID="dtFromDateTime" runat="server" ClientIDMode="Predictable" Width="95" />
                            </div>
                            <div class="form-group">
                                <label>To:</label>
                                <BizCalendar:Calendar ID="dtToDateTime" runat="server" ClientIDMode="Predictable" Width="95" />
                            </div>
                            <asp:Button ID="btnGoTime" runat="server" Text="Go" CssClass="btn btn-primary" />
                            <asp:Button ID="btnAddTime" runat="server" Text="Add Time" CssClass="btn btn-primary" OnClientClick="return OpenAddTimeWindow();" />
                        </div>
                    </div>
                </div>
            </div>
            <div class="row padbottom10">
                <div class="col-xs-12">
                    <div class="table-responsive">
                        <asp:Button id="btnSearchTime" runat="server" style="display:none;" OnClick="btnSearchTime_Click" />
                        <telerik:RadGrid runat="server" ID="rgTime" BorderWidth="0" AutoGenerateColumns="False" Skin="Default" AllowFilteringByColumn="True" OnDetailTableDataBind="rgTime_DetailTableDataBind" OnItemDataBound="rgTime_ItemDataBound" OnItemCommand="rgTime_ItemCommand">
                            <MasterTableView DataKeyNames="numUserCntID" ShowHeadersWhenNoRecords="true" ExpandCollapseColumn-Visible="false" 
                                HierarchyLoadMode="ServerOnDemand" CssClass="table table-bordered table-striped" Width="100%">
                                <Columns>
                                    <telerik:GridTemplateColumn>
                                        <HeaderTemplate>
                                            Employee
                                        </HeaderTemplate>
                                        <FilterTemplate>
                                            <asp:TextBox runat="server" ID="txtEmployeeHeaderTime" CssClass="form-control" onkeypress="FilterTimeGrid();"></asp:TextBox>
                                        </FilterTemplate>
                                        <ItemTemplate>
                                            <%# Eval("vcEmployee")%>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridTemplateColumn HeaderStyle-Width="200">
                                        <HeaderTemplate>
                                            Team
                                        </HeaderTemplate>
                                        <FilterTemplate>
                                            <telerik:RadComboBox runat="server" ID="rcbTeamHeaderTime" AutoPostBack="true" CheckBoxes="true" AllowCustomText="false" EmptyMessage="All" OnSelectedIndexChanged="rcbTeamHeaderTime_SelectedIndexChanged"></telerik:RadComboBox>
                                        </FilterTemplate>
                                        <ItemTemplate>
                                            <%# Eval("vcTeam")%>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridTemplateColumn AllowFiltering="false" HeaderStyle-Width="120">
                                        <HeaderTemplate>
                                            Total Hrs
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <ul class="list-inline">
                                                <li><%# Eval("numTotalHours")%></li>
                                                <%--<li><a href="javascript:void(0)" class="btn btn-xs btn-info" title="edit"><i class="fa fa-pencil"></i></a></li>--%>
                                            </ul>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridTemplateColumn HeaderStyle-Width="120">
                                        <HeaderTemplate>
                                            Payroll Type
                                        </HeaderTemplate>
                                        <FilterTemplate>
                                            <asp:DropDownList runat="server" ID="ddlTypeHeaderTime" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlTypeHeaderTime_SelectedIndexChanged">
                                                <asp:ListItem Value="0" Text="All"></asp:ListItem>
                                                <asp:ListItem Value="1" Text="Hourly"></asp:ListItem>
                                                <asp:ListItem Value="2" Text="Salary"></asp:ListItem>
                                            </asp:DropDownList>
                                        </FilterTemplate>
                                        <ItemTemplate>
                                            <%# Eval("vcPayrollType")%>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                </Columns>
                                <DetailTables>
                                    <telerik:GridTableView Name="rgTimeDetail" runat="server" DataKeyNames="numUserCntID" CssClass="table table-bordered table-hover dataTable" AllowFilteringByColumn="false" on >
                                        <Columns>
                                            <telerik:GridTemplateColumn ItemStyle-Wrap="false" HeaderStyle-Wrap="false">
                                                <HeaderTemplate>Source (Customer or Client)</HeaderTemplate>
                                                <ItemTemplate>
                                                    <%# Eval("vcSource")%>
                                                </ItemTemplate>
                                            </telerik:GridTemplateColumn>
                                            <telerik:GridBoundColumn HeaderText="Date" DataField="vcDate" ItemStyle-HorizontalAlign="Center" ItemStyle-Wrap="false" HeaderStyle-Wrap="false"></telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn HeaderText="Type or Line Item Name" DataField="vcType" ItemStyle-HorizontalAlign="Center" ItemStyle-Wrap="false" HeaderStyle-Wrap="false"></telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn HeaderText="Hours" DataField="vcHours" ItemStyle-HorizontalAlign="Center" ItemStyle-Wrap="false" HeaderStyle-Wrap="false"></telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn HeaderText="Details" DataField="vcDetails" MaxLength="200" ItemStyle-Width="100%" ItemStyle-Wrap="true"></telerik:GridBoundColumn>
                                            <telerik:GridTemplateColumn ItemStyle-Wrap="false" HeaderStyle-Wrap="false">
                                                <HeaderTemplate>
                                                    
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:HiddenField ID="hdnApprovalStaus" runat="server" Value='<%#Eval("numApprovalComplete")%>' />
                                                    <asp:Label ID="lblDeclined" runat="server" Text="Declined" Visible="false" />
                                                    <asp:Label ID="lblPendingApproval" runat="server" Text="Pending Approval" Visible="false" CssClass="badge bg-red" />
                                                    <asp:LinkButton ID="lnkApprove" runat="server" Visible="false" CssClass="btn btn-xs btn-primary" CommandName="Approve" CommandArgument='<%#Eval("numRecordID")%>'><i class="fa fa-thumbs-o-up"></i></asp:LinkButton>
                                                    <asp:LinkButton ID="lnkDecline" runat="server" Visible="false" CssClass="btn btn-xs btn-danger" CommandName="Decline" CommandArgument='<%#Eval("numRecordID")%>'><i class="fa fa-thumbs-o-down"></i></asp:LinkButton>
                                                    <asp:LinkButton ID="lnkEdit" runat="server" CssClass="btn btn-xs btn-info" OnClientClick='<%#"return OpenTimeExpenseEdit(" & Eval("numRecordID") & ");"%>'><i class="fa fa-edit"></i></asp:LinkButton>
                                                    <asp:LinkButton ID="lnkDelete" runat="server" Visible="false" CssClass="btn btn-xs btn-danger" CommandName="Delete" CommandArgument='<%#Eval("numRecordID")%>'><i class="fa fa-trash-o"></i></asp:LinkButton>
                                                </ItemTemplate>
                                            </telerik:GridTemplateColumn>
                                        </Columns>
                                    </telerik:GridTableView>
                                </DetailTables>
                            </MasterTableView>
                        </telerik:RadGrid>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <div class="pull-right">
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <tbody>
                                    <tr>
                                        <th style="width:50%; text-align:right">Total Hours</th>
                                        <td class="text-bold"><asp:Label ID="lblTotalHours" runat="server"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <th style="text-align:right">Total Billable Hours</th>
                                        <td class="text-bold"><asp:Label ID="lblTotalBillableHours" runat="server"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <th style="text-align:right">Total Non-Billable Hours</th>
                                        <td class="text-bold"><asp:Label ID="lblTotalNonBillableHours" runat="server"></asp:Label></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </telerik:RadPageView>
        <telerik:RadPageView ID="radPageView_ExpenseRegistry" runat="server">
            <div class="row padbottom10">
                <div class="col-xs-12">
                    <div class="pull-left">
                        <div class="form-inline">
                            <div class="form-group" id="divPayrollExpense" runat="server">
                                <label>Current Pay Period:</label>
                                <asp:DropDownList ID="ddlYearExpense" runat="server" CssClass="form-control" AutoPostBack="true">
                                </asp:DropDownList>
                                <asp:DropDownList ID="ddlMonthExpense" runat="server" CssClass="form-control" AutoPostBack="true">
                                </asp:DropDownList>
                                <asp:DropDownList ID="ddlDateExpense" runat="server" CssClass="form-control" AutoPostBack="true">
                                </asp:DropDownList>
                            </div>
                            <div class="form-group">
                                <label>Department:</label>
                                <asp:DropDownList ID="ddlDepartmentExpense" runat="server" CssClass="signup form-control" AutoPostBack="true">
                                </asp:DropDownList>
                            </div>
                        </div>
                    </div>
                    <div class="pull-right">
                        <div class="form-inline">
                            <div class="form-group">
                                <label>From:</label>
                                <BizCalendar:Calendar ID="dtFromDateExpense" runat="server" ClientIDMode="Predictable" Width="95" />
                            </div>
                            <div class="form-group">
                                <label>To:</label>
                                <BizCalendar:Calendar ID="dtToDateExpense" runat="server" ClientIDMode="Predictable" Width="95" />
                            </div>
                            <asp:Button ID="btnGoExpense" runat="server" Text="Go" CssClass="btn btn-primary" />
                            <asp:Button ID="btnAddExpense" runat="server" Text="Add Expense" CssClass="btn btn-primary" OnClientClick="return OpenAddExpenseWindow();" />
                        </div>
                    </div>
                </div>
            </div>
            <div class="row padbottom10">
                <div class="col-xs-12">
                    <div class="table-responsive">
                        <telerik:RadGrid runat="server" BorderWidth="0" ID="rgExpense" AutoGenerateColumns="False" AllowFilteringByColumn="True" OnDetailTableDataBind="rgExpense_DetailTableDataBind" OnItemDataBound="rgExpense_ItemDataBound" OnItemCommand="rgExpense_ItemCommand">
                            <MasterTableView DataKeyNames="numUserCntID" ExpandCollapseColumn-Visible="true" HierarchyLoadMode="ServerOnDemand" CssClass="table table-bordered table-striped" Width="100%">
                                <Columns>
                                    <telerik:GridTemplateColumn>
                                        <HeaderTemplate>
                                            Employee
                                        </HeaderTemplate>
                                        <FilterTemplate>
                                            <asp:TextBox runat="server" ID="txtEmployeeHeaderExpense" CssClass="form-control"></asp:TextBox>
                                        </FilterTemplate>
                                        <ItemTemplate>
                                            <%# Eval("vcEmployee")%>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridTemplateColumn>
                                        <HeaderTemplate>
                                            Team
                                        </HeaderTemplate>
                                        <FilterTemplate>
                                            <telerik:RadComboBox runat="server" ID="rcbTeamHeaderExpense" AutoPostBack="true" CheckBoxes="true" EmptyMessage="All" AllowCustomText="false" OnSelectedIndexChanged="rcbTeamHeaderExpense_SelectedIndexChanged"></telerik:RadComboBox>
                                        </FilterTemplate>
                                        <ItemTemplate>
                                            <%# Eval("vcTeam")%>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridTemplateColumn AllowFiltering="false">
                                        <HeaderTemplate>
                                            Expense
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <%# String.Format(BACRM.BusinessLogic.Common.CCommon.GetDataFormatStringWithCurrency(), Eval("monExpense"))%>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridTemplateColumn>
                                        <HeaderTemplate>
                                            Type
                                        </HeaderTemplate>
                                         <FilterTemplate>
                                            <asp:DropDownList runat="server" ID="ddlTypeHeaderExpense" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlTypeHeaderExpense_SelectedIndexChanged">
                                                <asp:ListItem Value="0" Text="All"></asp:ListItem>
                                                <asp:ListItem Value="1" Text="Hourly"></asp:ListItem>
                                                <asp:ListItem Value="2" Text="Salary"></asp:ListItem>
                                            </asp:DropDownList>
                                        </FilterTemplate>
                                        <ItemTemplate>
                                            <%# Eval("vcPayrollType")%>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                </Columns>
                                <DetailTables>
                                    <telerik:GridTableView Name="rgExpenseDetail" runat="server" DataKeyNames="numUserCntID" CssClass="table table-bordered table-hover dataTable" AllowFilteringByColumn="false">
                                        <Columns>
                                            <telerik:GridTemplateColumn ItemStyle-Wrap="false" HeaderStyle-Wrap="false">
                                                <HeaderTemplate>Source (Customer or Client)</HeaderTemplate>
                                                <ItemTemplate>
                                                    <%# Eval("vcSource")%>
                                                </ItemTemplate>
                                            </telerik:GridTemplateColumn>
                                            <telerik:GridBoundColumn HeaderText="Date" DataField="vcDate" ItemStyle-HorizontalAlign="Center" ItemStyle-Wrap="false" HeaderStyle-Wrap="false"></telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn HeaderText="Type or Line Item Name" DataField="vcType" ItemStyle-HorizontalAlign="Center" ItemStyle-Wrap="false" HeaderStyle-Wrap="false"></telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn HeaderText="Expense" DataField="monExpense" ItemStyle-HorizontalAlign="Center" ItemStyle-Wrap="false" HeaderStyle-Wrap="false"></telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn HeaderText="Details" DataField="vcDetails" MaxLength="200"></telerik:GridBoundColumn>
                                            <telerik:GridTemplateColumn>
                                                <HeaderTemplate>Receipts</HeaderTemplate>
                                                <ItemTemplate>
                                                    <ul class="list-inline" runat="server" id="ulReceipts"></ul>
                                                </ItemTemplate>
                                            </telerik:GridTemplateColumn>
                                            <telerik:GridBoundColumn HeaderText="Details" DataField="vcDetails" MaxLength="200" ItemStyle-Width="100%" ItemStyle-Wrap="true"></telerik:GridBoundColumn>
                                            <telerik:GridTemplateColumn ItemStyle-Wrap="false" HeaderStyle-Wrap="false">
                                                <HeaderTemplate>
                                                    
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:HiddenField ID="hdnApprovalStaus" runat="server" Value='<%#Eval("numApprovalComplete")%>' />
                                                    <asp:Label ID="lblDeclined" runat="server" Text="Declined" Visible="false" />
                                                    <asp:Label ID="lblPendingApproval" runat="server" Text="Pending Approval" Visible="false" CssClass="badge bg-red" />
                                                    <asp:LinkButton ID="lnkApprove" runat="server" Visible="false" CssClass="btn btn-xs btn-primary" CommandName="Approve" CommandArgument='<%#Eval("numRecordID")%>'><i class="fa fa-thumbs-o-up"></i></asp:LinkButton>
                                                    <asp:LinkButton ID="lnkDecline" runat="server" Visible="false" CssClass="btn btn-xs btn-danger" CommandName="Decline" CommandArgument='<%#Eval("numRecordID")%>'><i class="fa fa-thumbs-o-down"></i></asp:LinkButton>
                                                    <asp:LinkButton ID="lnkEdit" runat="server" CssClass="btn btn-xs btn-info" OnClientClick='<%#"return OpenTimeExpenseEdit(" & Eval("numRecordID") & ");"%>'><i class="fa fa-edit"></i></asp:LinkButton>
                                                    <asp:LinkButton ID="lnkDelete" runat="server" Visible="false" CssClass="btn btn-xs btn-danger" CommandName="Delete" CommandArgument='<%#Eval("numRecordID")%>'><i class="fa fa-trash-o"></i></asp:LinkButton>
                                                </ItemTemplate>
                                            </telerik:GridTemplateColumn>
                                        </Columns>
                                    </telerik:GridTableView>
                                </DetailTables>
                            </MasterTableView>
                        </telerik:RadGrid>
                    </div>
                </div>
            </div>
             <div class="row">
                <div class="col-xs-12">
                    <div class="pull-right">
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <tbody>
                                    <tr>
                                        <th style="width: 50%;text-align:right">Total Expense</th>
                                        <td class="text-bold"><asp:Label ID="lblTotalExpense" runat="server"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <th style="text-align:right">Total Billable Expense</th>
                                        <td class="text-bold"><asp:Label ID="lblTotalBillableExpense" runat="server"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <th style="text-align:right">Total Reimbursable Expense</th>
                                        <td class="text-bold"><asp:Label ID="lblTotalReimbursableExpense" runat="server"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <th style="text-align:right">Total Billable & Reimbursable Expense</th>
                                        <td class="text-bold"><asp:Label ID="lblTotalBillableReimbursableExpense" runat="server"></asp:Label></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </telerik:RadPageView>
    </telerik:RadMultiPage>
    <asp:HiddenField ID="hdnPayPeriod" runat="server" />
    <asp:UpdateProgress ID="UpdateProgress" runat="server" ClientIDMode="Static">
        <ProgressTemplate>
            <div class="overlay">
                <div class="overlayContent" style="color: #000; text-align: center; width: 250px; padding: 20px">
                    <i class="fa fa-2x fa-refresh fa-spin"></i>
                    <h3>Processing Request</h3>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
</asp:Content>
<asp:Content ID="Content7" runat="server" ContentPlaceHolderID="GridSettingPopup">
    <asp:LinkButton id="lnkClearGridCondition" runat="server" tooltip="Clear All Filters" class="btn-box-tool" OnClick="lnkClearGridCondition_Click"><i class="fa fa-2x fa-filter"></i></asp:LinkButton>
</asp:Content>
