<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" CodeBehind="frmAddRoles.aspx.vb"
    Inherits="BACRM.UserInterface.TimeAndExpense.frmAddRoles" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <link rel="stylesheet" href="~/CSS/master.css" type="text/css" />
    <title>User Roles</title>

    <script type="text/javascript" language="javascript">
        function Close() {
            // window.opener.document.frmUserDetails.all['lblRoles'].innerText=dgRoles.rows.length-1
            if (document.all) {
                window.opener.document.getElementById('lblRoles').innerText = document.getElementById('dgRoles').rows.length - 1
            } else {
                window.opener.document.getElementById('lblRoles').textContent = document.getElementById('dgRoles').rows.length - 1
            }
            window.close()
            return false;
        }
        function Add() {
            if (document.form1.ddlRole.value == 0) {
                alert("Select Role")
                document.form1.ddlRole.focus()
                return false;
            }
            if (document.form1.txtPercentage.value == '') {
                alert("Enter Percentage")
                document.form1.ddlRole.focus()
                return false;
            }
        }
    </script>

</head>
<body>
    <form id="form1" runat="server">
    <br />
    <table width="100%">
        <tr>
            <td class="normal1" align="right">
                Select Role
            </td>
            <td>
                <asp:DropDownList ID="ddlRole" runat="server" CssClass="signup" Width="150">
                </asp:DropDownList>
                &nbsp;
            </td>
            <td>
                <asp:TextBox ID="txtPercentage" runat="server" Width="40" CssClass="signup"></asp:TextBox>&nbsp;&nbsp;
                <asp:Button ID="btnAdd" CssClass="button" runat="server" Text="Add" Width="50" />
                <asp:Button ID="btnClose" CssClass="button" runat="server" Text="Close" Width="50" />
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <asp:DataGrid ID="dgRoles" runat="server" Width="100%" CssClass="dg" AutoGenerateColumns="False"
                    BorderColor="white">
                    <AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
                    <ItemStyle CssClass="is"></ItemStyle>
                    <HeaderStyle CssClass="hs"></HeaderStyle>
                    <Columns>
                        <asp:BoundColumn DataField="numRoleHDRID" Visible="false"></asp:BoundColumn>
                        <asp:BoundColumn DataField="numRole" Visible="false"></asp:BoundColumn>
                        <asp:BoundColumn HeaderText="Role" DataField="vcData"></asp:BoundColumn>
                        <asp:BoundColumn HeaderText="Percentage" DataField="fltPercentage"></asp:BoundColumn>
                        <asp:TemplateColumn>
                            <ItemTemplate>
                                <asp:Button ID="btnDelete" runat="server" CssClass="button Delete" Text="X" CommandName="Delete">
                                </asp:Button>
                                <asp:LinkButton ID="lnkDelete" runat="server" Visible="false">
														<font color="#730000">*</font></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                </asp:DataGrid>
            </td>
        </tr>
        <tr>
            <td colspan="3" class="normal1">
                &nbsp;&nbsp;&nbsp;Note: For updating a role, select the role from the dropdown,
                enter the percentage to associate &nbsp;&nbsp;&nbsp;with the role and then click
                Add.
            </td>
        </tr>
    </table>
    </form>
</body>
</html>
