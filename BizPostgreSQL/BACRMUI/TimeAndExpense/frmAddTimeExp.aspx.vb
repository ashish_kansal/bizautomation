Imports BACRM.BusinessLogic.TimeAndExpense
Imports BACRM.BusinessLogic.Opportunities
Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Contract
Imports BACRM.BusinessLogic.Accounting
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Contacts

Namespace BACRM.UserInterface.TimeAndExpense
    Partial Public Class frmAddTimeExp
        Inherits BACRMPage


#Region "Variables"
        Dim dtOppBiDocItems As DataTable
        Dim OppBizDocID As Long

        Dim lngEmployeeAccountID, lngEmpPayrollExpenseAcntID As Long
#End Region

        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            Try
                divError.Style.Add("display", "none")
                lblError.Text = ""

                divMessage.Style.Add("display", "none")
                litMessage.Text = ""

                If Not IsPostBack Then
                    If GetQueryStringVal("CatID") = Nothing Then
                        pnlDate.Visible = False
                        trAmount.Visible = False
                        calFrom.SelectedDate = GetQueryStringVal("Date")
                        calto.SelectedDate = GetQueryStringVal("Date")
                        hdnDate.Value = GetQueryStringVal("Date")
                        ' CalCreated.SelectedDate = GetQueryStringVal( "Date")
                        If GetQueryStringVal("Mode") = "AddTime" Then
                            ddlCategory.SelectedValue = 1
                            EnableControls()
                        End If
                    Else
                        LoadInformation()
                    End If
                End If

                btnSaveClose.Attributes.Add("onclick", "return Save()")
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Sub LoadInformation()
            Try
                Dim objTimeExp As New TimeExpenseLeave
                Dim dtTable As DataTable
                objTimeExp.TEType = 0
                objTimeExp.UserCntID = CCommon.ToLong(GetQueryStringVal("CntID"))
                objTimeExp.DomainID = Session("DomainID")
                objTimeExp.CategoryHDRID = CCommon.ToLong(GetQueryStringVal("CatID"))
                objTimeExp.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
                dtTable = objTimeExp.GetTimeAndExpDetails
                If dtTable.Rows.Count = 1 Then
                    If Not ddlCategory.Items.FindByValue(dtTable.Rows(0).Item("numCategory")) Is Nothing Then
                        ddlCategory.Items.FindByValue(dtTable.Rows(0).Item("numCategory")).Selected = True
                    End If
                    If IsDBNull(dtTable.Rows(0).Item("txtDesc")) = False Then
                        txtDesc.Text = dtTable.Rows(0).Item("txtDesc")
                    End If
                    If dtTable.Rows(0).Item("numType") = 1 Then
                        If objCommon Is Nothing Then objCommon = New CCommon
                        Dim strCompany As String
                        objCommon.DivisionID = dtTable.Rows(0).Item("numDivisionID")
                        strCompany = objCommon.GetCompanyName
                    ElseIf dtTable.Rows(0).Item("numType") = 2 Then
                        ddlCategory.ClearSelection()
                        If CCommon.ToBool(dtTable.Rows(0).Item("bitReimburse")) = True Then
                            ddlCategory.SelectedValue = 2
                            btnSaveClose.Visible = True
                        Else
                            ddlCategory.SelectedValue = 5
                            btnSaveClose.Visible = False
                        End If
                    End If

                    If dtTable.Rows(0).Item("numCategory") = 1 Then
                        hplRec.Text = "Rate/Hour"
                        calFrom.SelectedDate = dtTable.Rows(0).Item("dtFromDate")
                        calto.SelectedDate = dtTable.Rows(0).Item("dtToDate")
                        ddltime.ClearSelection()
                        If Not ddltime.Items.FindByText(Format(dtTable.Rows(0).Item("dtFromDate"), "h:mm")) Is Nothing Then
                            ddltime.Items.FindByText(Format(dtTable.Rows(0).Item("dtFromDate"), "h:mm")).Selected = True
                        End If
                        ddlEndTime.ClearSelection()
                        If Not ddlEndTime.Items.FindByText(Format(dtTable.Rows(0).Item("dtToDate"), "h:mm")) Is Nothing Then
                            ddlEndTime.Items.FindByText(Format(dtTable.Rows(0).Item("dtToDate"), "h:mm")).Selected = True
                        End If
                        If Format(dtTable.Rows(0).Item("dtFromDate"), "tt") = "AM" Then
                            chkAM.Checked = True
                        Else : chkPM.Checked = True
                        End If
                        If Format(dtTable.Rows(0).Item("dtToDate"), "tt") = "AM" Then
                            chkEndAM.Checked = True
                        Else : chkEndPM.Checked = True
                        End If
                        txtRate.Text = String.Format("{0:#,##0.00}", CCommon.ToDouble(dtTable.Rows(0).Item("monAmount")))
                    ElseIf dtTable.Rows(0).Item("numCategory") = 2 Then
                        BindExpenseAccount()
                        hplRec.Text = "Amount"
                        txtRate.Text = CCommon.ToDouble(dtTable.Rows(0).Item("monAmount"))
                        If Not ddlExpenseAccount.Items.FindByValue(CCommon.ToLong(dtTable.Rows(0).Item("numExpenseAccountID"))) Is Nothing Then
                            ddlExpenseAccount.Items.FindByValue(CCommon.ToLong(dtTable.Rows(0).Item("numExpenseAccountID"))).Selected = True
                        End If
                        'btnSaveClose.Visible = False
                        calFrom.SelectedDate = dtTable.Rows(0).Item("dtFromDate")
                        calto.SelectedDate = dtTable.Rows(0).Item("dtToDate")
                        ddltime.ClearSelection()
                        If Not ddltime.Items.FindByText(Format(dtTable.Rows(0).Item("dtFromDate"), "h:mm")) Is Nothing Then
                            ddltime.Items.FindByText(Format(dtTable.Rows(0).Item("dtFromDate"), "h:mm")).Selected = True
                        End If
                        ddlEndTime.ClearSelection()
                        If Not ddlEndTime.Items.FindByText(Format(dtTable.Rows(0).Item("dtToDate"), "h:mm")) Is Nothing Then
                            ddlEndTime.Items.FindByText(Format(dtTable.Rows(0).Item("dtToDate"), "h:mm")).Selected = True
                        End If

                    ElseIf CCommon.ToInteger(dtTable.Rows(0).Item("numCategory")) = 3 Or CCommon.ToInteger(dtTable.Rows(0).Item("numCategory")) = 4 Then
                        calFrom.SelectedDate = dtTable.Rows(0).Item("dtFromDate")
                        calto.SelectedDate = dtTable.Rows(0).Item("dtToDate")
                        If dtTable.Rows(0).Item("bitFromFullDay") = True Then
                            radStartFullDay.Checked = True
                        Else : radStartHalfDay.Checked = True
                        End If
                        If dtTable.Rows(0).Item("bitToFullDay") = True Then
                            radEndFullday.Checked = True
                        Else : radEndHalfDay.Checked = True
                        End If
                        btnSaveClose.Visible = False
                    End If
                End If

                TransactionInfo1.ReferenceType = enmReferenceType.TimeAndExpense
                TransactionInfo1.ReferenceID = CCommon.ToLong(GetQueryStringVal("CatID"))
                TransactionInfo1.getTransactionInfo()

                EnableControls()
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub ddlCategory_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCategory.SelectedIndexChanged
            Try
                EnableControls()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Sub EnableControls()
            Try
                If ddlCategory.SelectedItem.Value = "1" Then
                    pnlDate.Visible = False
                    tdStartDay.Visible = False
                    tdEndDay.Visible = False

                    tdStartTime.Visible = False
                    tdEndTime.Visible = False
                    hplRec.Text = "Rate/Hour"
                    tdExpenseAcc.Visible = False
                    trAmount.Visible = False
                    trDesc.Visible = False
                    trAssignTo.Visible = True
                ElseIf ddlCategory.SelectedItem.Value = "2" Then
                    pnlDate.Visible = False
                    trAmount.Visible = True
                    hplRec.Text = "Amount"
                    tdExpenseAcc.Visible = True
                    trAssignTo.Visible = False
                    trDesc.Visible = True
                    BindExpenseAccount()
                ElseIf ddlCategory.SelectedItem.Value = "3" Or ddlCategory.SelectedItem.Value = "4" Then
                    trAmount.Visible = False
                    tdStartTime.Visible = False
                    tdEndTime.Visible = False
                    tdStartDay.Visible = True
                    tdEndDay.Visible = True
                    pnlDate.Visible = True
                    tdExpenseAcc.Visible = False
                    trAssignTo.Visible = False
                    trDesc.Visible = True
                ElseIf ddlCategory.SelectedItem.Value = "5" Then
                    pnlDate.Visible = True
                    tdStartDay.Visible = False
                    tdEndDay.Visible = False
                    tdStartTime.Visible = True
                    tdEndTime.Visible = True
                    hplRec.Text = "Rate/Hour"
                    tdExpenseAcc.Visible = False
                    trAmount.Visible = False
                    trDesc.Visible = True
                    trAssignTo.Visible = False
                End If
            
                EnableRate()
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub EnableRate()
            Try
                txtRate.Enabled = True
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub ValidateEmpAccount()
            'Validate Employee's Account ID
            Dim ds As New DataSet
            Dim objCOA As New ChartOfAccounting
            With objCOA
                .MappingID = 0
                .ContactTypeID = CContacts.GetContactType(Session("UserContactID"))
                .DomainID = Session("DomainID")
                ds = .GetContactTypeMapping()
                If ds.Tables(0).Rows.Count > 0 Then
                    lngEmployeeAccountID = CCommon.ToLong(ds.Tables(0).Rows(0).Item("numAccountId"))
                End If
            End With
        End Sub
        Private Sub btnSaveClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveClose.Click
            Try
                Dim strStartTime, strEndTime, strStartDate, strEndDate As String
                strStartDate = calFrom.SelectedDate
                strEndDate = calto.SelectedDate
                Dim strSplitStartTimeDate, strSplitEndTimeDate As Date
                If Not (strStartDate Is Nothing And strEndDate Is Nothing) Then
                    strStartTime = strStartDate.Trim & " " & ddltime.SelectedItem.Text.Trim & ":00" & IIf(chkAM.Checked = True, " AM", " PM")
                    strEndTime = strEndDate.Trim & " " & ddlEndTime.SelectedItem.Text.Trim & ":00" & IIf(chkEndAM.Checked = True, " AM", " PM")

                    strSplitStartTimeDate = CType(strStartTime, DateTime).AddMinutes(Session("ClientMachineUTCTimeOffset"))        'Convert the Start Date and Time to UT
                    strSplitEndTimeDate = CType(strEndTime, DateTime).AddMinutes(Session("ClientMachineUTCTimeOffset"))
                End If

                If ddlCategory.SelectedValue = 2 Then
                    ValidateEmpAccount()
                    If lngEmployeeAccountID = 0 Then
                        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Alert", "alert('Please Set your Account from Administration->Global Settings->Accounting->Contact Type Mapping.' );", True)
                        Exit Sub
                    End If

                ElseIf ddlCategory.SelectedValue = 3 Then

                    ValidateEmpAccount()
                    If lngEmployeeAccountID = 0 Then
                        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Alert", "alert('Please Set your Account from ""Administration->Global Settings->Accounting->Accounts for Contact Type"".' );", True)
                        Exit Sub
                    End If
                    'Validate default Payroll Expense Account
                    lngEmpPayrollExpenseAcntID = ChartOfAccounting.GetDefaultAccount("EP", Session("DomainID")) 'Employee Payeroll Expense
                    If lngEmpPayrollExpenseAcntID = 0 Then
                        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Alert", "alert('Please Set Default Employee Payroll Expense Account from ""Administration->Global Settings->Accounting->Default Accounts"".' );", True)
                        Exit Sub
                    End If
                    'Validate id Employee is Salary based.
                    Dim objPayroll As New PayrollExpenses
                    If objPayroll.IsSalariedEmployee(Session("UserID"), Session("DomainID"), dblHourlyRate, dblDailyHours) = False Then
                        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "employee", "alert('You will not be able to enter leave because you are not set-up as a salaried employee. To enable this setting, go to Accounting-> Employee Payroll & Expenses click your name, and within the detail page, check the section that automatically sets salary hours');Close();", True)
                        Exit Sub
                    End If
                    If dblDailyHours = 0 Or dblHourlyRate = 0 Then
                        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "employee", "alert('You will not be able to enter leave because your hourly rate and/or Daily working Hours is not set-up. To enable this setting, go to Accounting->Employee Payroll & Expenses click your name, and within the detail page you can set it.');Close();", True)
                        Exit Sub
                    End If
                End If

                Dim objTimeExp As New TimeExpenseLeave
                objTimeExp.CategoryHDRID = CCommon.ToLong(GetQueryStringVal("CatID"))
                objTimeExp.CategoryID = IIf(ddlCategory.SelectedItem.Value = "5", "1", ddlCategory.SelectedItem.Value)
                
                objTimeExp.ContractID = 0
             
                If ddlCategory.SelectedItem.Value = "2" Then
                    objTimeExp.Amount = txtRate.Text
                    objTimeExp.FromDate = calFrom.SelectedDate 'GetQueryStringVal("Date")
                    objTimeExp.ToDate = calto.SelectedDate 'GetQueryStringVal("Date")
                    objTimeExp.bitReimburse = True 'chkReimb.Checked
                    objTimeExp.CategoryType = 2

                ElseIf ddlCategory.SelectedItem.Value = "3" Then
                    objTimeExp.FromDate = calFrom.SelectedDate
                    objTimeExp.ToDate = calto.SelectedDate
                    objTimeExp.bitFromFullDay = IIf(radStartFullDay.Checked = True, 1, 0)
                    objTimeExp.bitToFullDay = IIf(radEndFullday.Checked = True, 1, 0)
                    objTimeExp.CategoryType = 3 ' Paid Leave

                ElseIf ddlCategory.SelectedValue = "4" Then
                    objTimeExp.FromDate = calFrom.SelectedDate
                    objTimeExp.ToDate = calto.SelectedDate
                    objTimeExp.bitFromFullDay = IIf(radStartFullDay.Checked = True, 1, 0)
                    objTimeExp.bitToFullDay = IIf(radEndFullday.Checked = True, 1, 0)
                    objTimeExp.CategoryType = 4 ' Non paid Leaves

                ElseIf ddlCategory.SelectedValue = "5" Then
                    objTimeExp.FromDate = strSplitStartTimeDate
                    objTimeExp.ToDate = strSplitEndTimeDate
                    objTimeExp.CategoryType = 2 ' Non Billable time

                End If
                
                objTimeExp.Desc = txtDesc.Text.Trim
                objTimeExp.UserCntID = CCommon.ToLong(GetQueryStringVal("CntID"))
                objTimeExp.DomainID = Session("DomainID")
                objTimeExp.TEType = 0

                Using objTransactionScope As New System.Transactions.TransactionScope(Transactions.TransactionScopeOption.Required, New System.Transactions.TransactionOptions() With {.IsolationLevel = Transactions.IsolationLevel.ReadCommitted, .Timeout = Transactions.TransactionManager.MaximumTimeout})
                    If objTimeExp.ManageTimeAndExpense() = True Then
                        ShowMessage("Leave is  already taken for the selected dates")
                    Else
                        If ddlCategory.SelectedValue = 2 Then
                            'Create journal Entry for Reimbursable Expense
                            'Debit:Expense Account ,Credit:Employee's Account
                            If objTimeExp.CategoryHDRID > 0 And CCommon.ToDouble(txtRate.Text) > 0 Then
                                SaveDataToHeaderAndDetailForExpense(TransactionInfo1.JournalID, objTimeExp.CategoryHDRID)
                            End If
                        End If
                        If ddlCategory.SelectedValue = 3 Then
                            If objTimeExp.CategoryHDRID > 0 Then
                                SaveDataToHeaderAndDetailForLeave(TransactionInfo1.JournalID, objTimeExp.CategoryHDRID)
                            End If
                        End If
                    End If

                    objTransactionScope.Complete()
                End Using

                Dim strScript As String

                If GetQueryStringVal("frm") = "TimeAndExpDets" Then
                    Response.Redirect("../Accounting/frmTimeAndExpensesDetails.aspx?NameU=" & GetQueryStringVal("CntID") & "&StartDate=" & GetQueryStringVal("Date") & "&EndDate=" & GetQueryStringVal("EndDate"))
                ElseIf GetQueryStringVal("frm") = "TransactionDet" Or GetQueryStringVal("frm") = "QuickAccount" Or GetQueryStringVal("frm") = "GeneralLedger" Or GetQueryStringVal("frm") = "BankRegister" Or GetQueryStringVal("frm") = "BankRecon" Then
                    strScript = "<script language=JavaScript>"
                    strScript += "self.close();"
                    strScript += "</script>"
                    If (Not Page.IsStartupScriptRegistered("clientScript")) Then Page.RegisterStartupScript("clientScript", strScript)
                ElseIf GetQueryStringVal("frm") = "EditTE" Then
                    Response.Redirect("~/TimeAndExpense/frmEmpCal.aspx", False)
                Else
                    strScript = ""
                    strScript = "<script language='JavaScript'>"
                    strScript += " window.parent.document.getElementById('hdnDate').value='" + GetQueryStringVal("Date") + "';"
                    strScript += "window.parent.document.getElementById('btnrefresh').click();window.close();</script>"
                    Page.RegisterStartupScript("clientScript", strScript)
                End If
            Catch ex As Exception
                If ex.Message = "FY_CLOSED" Then
                    ShowMessage("This transaction can not be posted,Because transactions date belongs to closed financial year.")
                Else
                    ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                    DisplayError(CCommon.ToString(ex))
                End If
            End Try
        End Sub

        Private Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
            Try
                If GetQueryStringVal("frm") = "TimeAndExpDets" Then
                    Response.Redirect("../Accounting/frmTimeAndExpensesDetails.aspx?NameU=" & GetQueryStringVal("CntID") & "&StartDate=" & GetQueryStringVal("Date") & "&EndDate=" & GetQueryStringVal("EndDate"))
                ElseIf GetQueryStringVal("frm") = "TransactionDet" Or GetQueryStringVal("frm") = "QuickAccount" Or GetQueryStringVal("frm") = "GeneralLedger" Or GetQueryStringVal("frm") = "BankRegister" Or GetQueryStringVal("frm") = "BankRecon" Then
                    Response.Write("<script language=JavaScript>window.close();</script>")
                ElseIf GetQueryStringVal("frm") = "EditTE" Then
                    Response.Redirect("~/TimeAndExpense/frmEmpCal.aspx", False)

                Else : Response.Redirect("../TimeAndExpense/frmDaydetails.aspx?Date=" & GetQueryStringVal("Date") & "&CntID=" & GetQueryStringVal("CntID"))

                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Private Sub rblAssignto_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rblAssignto.SelectedIndexChanged
            Try
                If rblAssignto.SelectedValue = 1 Then
                    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "opentime", "OpenProjectTime();", True)
                ElseIf rblAssignto.SelectedValue = 3 Then
                    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "openBizDoc", "AddAuthBizdoc();", True)
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Private Sub BindExpenseAccount()
            Try
                If ddlExpenseAccount.Items.Count = 0 Then
                    Dim objCOA As New ChartOfAccounting
                    objCOA.DomainID = Session("DomainId")
                    objCOA.AccountCode = "0104"
                    ddlExpenseAccount.DataSource = objCOA.GetParentCategory()
                    ddlExpenseAccount.DataTextField = "vcAccountName1"
                    ddlExpenseAccount.DataValueField = "numAccountId"
                    ddlExpenseAccount.DataBind()
                    ddlExpenseAccount.Items.Insert(0, "--Select One--")
                    ddlExpenseAccount.Items.FindByText("--Select One--").Value = "0"
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        Dim dtItems As New DataTable
        Dim dtrow As DataRow
        Private Function SaveDataToHeaderAndDetailForExpense(lngJournalId As Long, ByVal lngCategoryHDRID As Long) As Integer
            Dim lntJournalId As Integer
            Try
                Dim objJEHeader As New JournalEntryHeader
                With objJEHeader
                    .JournalId = lngJournalId
                    .RecurringId = 0
                    .EntryDate = Date.UtcNow
                    .Description = "TimeAndExpense"
                    .Amount = CCommon.ToDecimal(txtRate.Text)
                    .CheckId = 0
                    .CashCreditCardId = 0
                    .ChartAcntId = ddlExpenseAccount.SelectedValue
                    .OppId = 0
                    .OppBizDocsId = 0
                    .DepositId = 0
                    .BizDocsPaymentDetId = 0
                    .IsOpeningBalance = 0
                    .LastRecurringDate = Date.Now
                    .NoTransactions = 0
                    .CategoryHDRID = lngCategoryHDRID
                    .ReturnID = 0
                    .CheckHeaderID = 0
                    .BillID = 0
                    .BillPaymentID = 0
                    .UserCntID = Session("UserContactID")
                    .DomainID = Session("DomainID")
                End With

                lngJournalId = objJEHeader.Save()


                Dim objJEList As New JournalEntryCollection

                Dim objJE As New JournalEntryNew()

                objJE.TransactionId = 0
                objJE.DebitAmt = CCommon.ToDecimal(txtRate.Text)
                objJE.CreditAmt = 0
                objJE.ChartAcntId = ddlExpenseAccount.SelectedValue
                objJE.Description = "TimeAndExpense"
                objJE.CustomerId = 0
                objJE.MainDeposit = 0
                objJE.MainCheck = 0
                objJE.MainCashCredit = 0
                objJE.OppitemtCode = 0
                objJE.BizDocItems = ""
                objJE.Reference = ""
                objJE.PaymentMethod = 0
                objJE.Reconcile = False
                objJE.CurrencyID = 0
                objJE.FltExchangeRate = 0
                objJE.TaxItemID = 0
                objJE.BizDocsPaymentDetailsId = 0
                objJE.ContactID = 0
                objJE.ItemID = 0
                objJE.ProjectID = 0
                objJE.ClassID = 0
                objJE.CommissionID = 0
                objJE.ReconcileID = 0
                objJE.Cleared = 0
                objJE.ReferenceType = 0
                objJE.ReferenceID = 0

                objJEList.Add(objJE)

                objJE = New JournalEntryNew()

                objJE.TransactionId = 0
                objJE.DebitAmt = 0
                objJE.CreditAmt = CCommon.ToDecimal(txtRate.Text)
                objJE.ChartAcntId = lngEmployeeAccountID
                objJE.Description = "TimeAndExpense"
                objJE.CustomerId = 0
                objJE.MainDeposit = 0
                objJE.MainCheck = 0
                objJE.MainCashCredit = 0
                objJE.OppitemtCode = 0
                objJE.BizDocItems = ""
                objJE.Reference = ""
                objJE.PaymentMethod = 0
                objJE.Reconcile = False
                objJE.CurrencyID = 0
                objJE.FltExchangeRate = 0
                objJE.TaxItemID = 0
                objJE.BizDocsPaymentDetailsId = 0
                objJE.ContactID = 0
                objJE.ItemID = 0
                objJE.ProjectID = 0
                objJE.ClassID = 0
                objJE.CommissionID = 0
                objJE.ReconcileID = 0
                objJE.Cleared = 0
                objJE.ReferenceType = 0
                objJE.ReferenceID = 0

                objJEList.Add(objJE)

                objJEList.Save(JournalEntryCollection.JournalMode.DeleteUpdateInsert, lngJournalId, Session("DomainID"))

                Return lngJournalId
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Dim dblHourlyRate, dblDailyHours As Double
        Private Function SaveDataToHeaderAndDetailForLeave(lngJournalId As Long, ByVal lngCategoryHDRID As Long) As Integer
            Dim lntJournalId As Integer
            Try
                Dim ds As New DataSet
                Dim dblPaidLeaveAmount As Double
                If radStartHalfDay.Checked = True Or radEndHalfDay.Checked = True Then
                    dblPaidLeaveAmount = (dblHourlyRate * dblDailyHours) / 2
                Else
                    dblPaidLeaveAmount = dblHourlyRate * dblDailyHours
                End If

                Dim objJEHeader As New JournalEntryHeader
                With objJEHeader
                    .JournalId = lngJournalId
                    .RecurringId = 0
                    .EntryDate = Date.UtcNow
                    .Description = "TimeAndExpense"
                    .Amount = dblPaidLeaveAmount
                    .CheckId = 0
                    .CashCreditCardId = 0
                    .ChartAcntId = 0
                    .OppId = 0
                    .OppBizDocsId = 0
                    .DepositId = 0
                    .BizDocsPaymentDetId = 0
                    .IsOpeningBalance = 0
                    .LastRecurringDate = Date.Now
                    .NoTransactions = 0
                    .CategoryHDRID = lngCategoryHDRID
                    .ReturnID = 0
                    .CheckHeaderID = 0
                    .BillID = 0
                    .BillPaymentID = 0
                    .UserCntID = Session("UserContactID")
                    .DomainID = Session("DomainID")
                End With

                lngJournalId = objJEHeader.Save()


                Dim objJEList As New JournalEntryCollection

                Dim objJE As New JournalEntryNew()

                objJE.TransactionId = 0
                objJE.DebitAmt = dblPaidLeaveAmount
                objJE.CreditAmt = 0
                objJE.ChartAcntId = lngEmpPayrollExpenseAcntID
                objJE.Description = "TimeAndExpense"
                objJE.CustomerId = 0
                objJE.MainDeposit = 0
                objJE.MainCheck = 0
                objJE.MainCashCredit = 0
                objJE.OppitemtCode = 0
                objJE.BizDocItems = ""
                objJE.Reference = ""
                objJE.PaymentMethod = 0
                objJE.Reconcile = False
                objJE.CurrencyID = 0
                objJE.FltExchangeRate = 0
                objJE.TaxItemID = 0
                objJE.BizDocsPaymentDetailsId = 0
                objJE.ContactID = 0
                objJE.ItemID = 0
                objJE.ProjectID = 0
                objJE.ClassID = 0
                objJE.CommissionID = 0
                objJE.ReconcileID = 0
                objJE.Cleared = 0
                objJE.ReferenceType = 0
                objJE.ReferenceID = 0

                objJEList.Add(objJE)

                objJE = New JournalEntryNew()

                objJE.TransactionId = 0
                objJE.DebitAmt = 0
                objJE.CreditAmt = dblPaidLeaveAmount
                objJE.ChartAcntId = lngEmployeeAccountID
                objJE.Description = "TimeAndExpense"
                objJE.CustomerId = 0
                objJE.MainDeposit = 0
                objJE.MainCheck = 0
                objJE.MainCashCredit = 0
                objJE.OppitemtCode = 0
                objJE.BizDocItems = ""
                objJE.Reference = ""
                objJE.PaymentMethod = 0
                objJE.Reconcile = False
                objJE.CurrencyID = 0
                objJE.FltExchangeRate = 0
                objJE.TaxItemID = 0
                objJE.BizDocsPaymentDetailsId = 0
                objJE.ContactID = 0
                objJE.ItemID = 0
                objJE.ProjectID = 0
                objJE.ClassID = 0
                objJE.CommissionID = 0
                objJE.ReconcileID = 0
                objJE.Cleared = 0
                objJE.ReferenceType = 0
                objJE.ReferenceID = 0

                objJEList.Add(objJE)

                objJEList.Save(JournalEntryCollection.JournalMode.DeleteUpdateInsert, lngJournalId, Session("DomainID"))

                Return lngJournalId
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Private Sub ShowMessage(ByVal message As String)
            Try
                divMessage.Style.Add("display", "")
                litMessage.Text = message
            Catch ex As Exception

            End Try
        End Sub

        Private Sub DisplayError(ByVal errorMessage As String)
            Try
                divError.Style.Add("display", "")
                lblError.Text = errorMessage
                divError.Focus()
            Catch ex As Exception

            End Try
        End Sub
    End Class
End Namespace