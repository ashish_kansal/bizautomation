<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmDaydetails.aspx.vb" Inherits="BACRM.UserInterface.TimeAndExpense.frmDaydetails" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <link rel="stylesheet" href="~/CSS/master.css" type="text/css" />

    <title></title>

</head>
<body>
    <script language="javascript" type="text/javascript">
        function OpenOpp(a, b) {
            if (a > 0) {
                var str;
                str = "../opportunity/frmOpportunities.aspx?frm=OpenBizDoc&OpID=" + a + "&OppType=" + b;

                if (window.opener == null)
                    window.parent.location.href = str;
                else if (window.opener.opener == null)
                    window.opener.location.href = str;
                else
                    window.opener.opener.frames.document.location.href = str;
            }
        }

        function OpenProject(a) {
            var str;
            str = "../projects/frmProjects.aspx?ProId=" + a;

            if (window.opener == null)
                window.parent.location.href = str;
            else if (window.opener.opener == null)
                window.opener.location.href = str;
            else
                window.opener.opener.frames.document.location.href = str;
        }

    </script>
    <form id="form1" runat="server">
        <br />
        <br />
        <table width="100%">
            <tr>
                <td align="center">
                    <asp:Label ID="lbldate" runat="server" CssClass="text"></asp:Label>
                </td>
                <td align="right">
                    <asp:Button ID="btnAdd" runat="Server" Text="Add" Width="50" CssClass="button" Visible="false"/>
                </td>
            </tr>
            <tr>
                <td colspan="2" align="right">
                    <asp:DataGrid ID="dgDetails" runat="server" Width="100%" CssClass="dg" AutoGenerateColumns="False"
                        BorderColor="white">
                        <AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
                        <ItemStyle CssClass="is"></ItemStyle>
                        <HeaderStyle CssClass="hs"></HeaderStyle>
                        <Columns>
                            <asp:TemplateColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" FooterStyle-Width="20" HeaderStyle-Width="20" ItemStyle-Width="20">
                                <ItemTemplate>
                                    <%--<asp:Button CommandName="Edit"  ID="btnEdit" runat="server" Width="35" Text="View" CssClass="button"/>--%>
                                    <asp:ImageButton ID="editImage" runat="server" ImageUrl="~/images/edit.png" ImageAlign="Middle" CommandName="Edit" />
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="numCategoryHDRID" Visible="false"></asp:BoundColumn>
                            <asp:BoundColumn DataField="tintTEType" Visible="false"></asp:BoundColumn>
                            <asp:BoundColumn DataField="numOppId" Visible="false"></asp:BoundColumn>
                            <asp:BoundColumn DataField="numCaseid" Visible="false"></asp:BoundColumn>
                            <asp:BoundColumn DataField="numProid" Visible="false"></asp:BoundColumn>
                            <asp:BoundColumn DataField="numStageID" Visible="false"></asp:BoundColumn>
                            <asp:BoundColumn DataField="numDivisionID" Visible="false"></asp:BoundColumn>
                            <asp:BoundColumn DataField="txtDesc" Visible="false"></asp:BoundColumn>
                            <asp:BoundColumn DataField="Category" HeaderText="Time, Expense and Leave"></asp:BoundColumn>
                            <asp:TemplateColumn HeaderText="Time" ItemStyle-HorizontalAlign="Right">
                                <ItemTemplate>
                                    <%#Eval("TimeValue")%>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Expense" ItemStyle-HorizontalAlign="Right">
                                <ItemTemplate>
                                    <%#Eval("ExpenseValue")%>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="Detail" Visible="false" DataFormatString="{0:#,##0.00}" HeaderText="" ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
                            <asp:BoundColumn DataField="Type" HeaderText="Type"></asp:BoundColumn>
                            <%--<asp:BoundColumn DataField="chrFrom" HeaderText="From"></asp:BoundColumn>--%>
                            <asp:TemplateColumn>
                                <ItemTemplate>
                                    <%#Eval("chrFrom")%>&nbsp;:&nbsp;<a href="<%#If(Eval("numOppId") > 0,"javascript:OpenOpp(" & Eval("numOppId") & ")","javascript:OpenProject(" & Eval("numProID") & ")") %>">
                                        <%#If(Eval("numOppId") > 0, Eval("vcPOppname"), Eval("vcProjectId"))%>
                                    </a>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="numCategory" Visible="false"></asp:BoundColumn>
                            <asp:BoundColumn DataField="numtype" Visible="false"></asp:BoundColumn>
                            <asp:BoundColumn DataField="monAmount" HeaderText="Rate/Hr" Visible="false" ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
                            <asp:BoundColumn DataField="vcEmployee" HeaderText="Employee"></asp:BoundColumn>
                            <asp:BoundColumn DataField="numUserCntID" Visible="false"></asp:BoundColumn>
                            <asp:TemplateColumn>
                                <ItemTemplate>
                                    <asp:Button ID="btnDelete" runat="server" CssClass="button Delete" Text="X" CommandName="Delete"></asp:Button>
                                    <asp:LinkButton ID="lnkDelete" runat="server" Visible="false">
														<font color="#730000">*</font></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <%--<asp:TemplateColumn>
                                <FooterStyle Width="100%" HorizontalAlign="Center" VerticalAlign="Middle" />
                                <FooterTemplate>
                                    <asp:Label runat="server" ID="lblEmptyDataText" Text=""> </asp:Label>
                                </FooterTemplate>
                            </asp:TemplateColumn>--%>
                        </Columns>
                    </asp:DataGrid>
                </td>
            </tr>
            <tr>
                <td colspan="2" align="center">
                    <asp:Label runat="server" ID="lblEmptyDataText" > </asp:Label>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
