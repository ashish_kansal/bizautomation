﻿Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.TimeAndExpense
Imports BACRM.BusinessLogic.Accounting
Imports BACRM.BusinessLogic.Opportunities

Namespace BACRM.UserInterface.TimeAndExpense
    Public Class UCDayDetails
        Inherits BACRMUserControl

#Region "Member Variables"
        Public Type As Short
        Public strDate As DateTime
        Public StartDate As DateTime
        Public EndDate As DateTime
        Public dateFlag As Integer
        Public UserCntID As Long
#End Region

#Region "Page Loads"
        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            Try
                'CLEAR ERROR ON RELOAD
                DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ""
                DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "none")

                GetUserRightsForPage(MODULEID.TimeAndExpense, PAGEID.TIME_AND_EXPENSE)

                If Not IsPostBack Then
                    LoadGrid()
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub
#End Region

#Region "Private Methods"
        Private Sub DisplayError(ByVal exception As String)
            Try
                DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = exception
                DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
                DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Focus()
            Catch ex As Exception

            End Try
        End Sub
#End Region

#Region "Event Handlers"
        Private Sub dgDetails_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgDetails.ItemCommand
            Try
                If e.CommandName = "Edit" Then
                    Dim numCategory As Int16 = e.Item.Cells(14).Text

                    Select Case e.Item.Cells(1).Text
                        Case 0
                            Response.Redirect("~/TimeAndExpense/frmAddTimeExp.aspx?frm=EditTE&type=" & hdnDayDetailType.Value & "&Date=" & hdnDayDetailDate.Value & "&CatID=" & e.Item.Cells(0).Text & "&CntID=" & e.Item.Cells(18).Text)
                        Case 1
                            Dim url As String = Page.ResolveClientUrl("~/projects/frmProTime.aspx") & "?title=1&type=" & hdnDayDetailType.Value & "&ProStageID=" & e.Item.Cells(5).Text & "&Proid=0&DivId=" & e.Item.Cells(6).Text & "&Date=" & hdnDayDetailDate.Value & "&CatID=" & e.Item.Cells(0).Text & "&CntID=" & e.Item.Cells(18).Text & "&CatHdrId=" & e.Item.Cells(1).Text
                            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "ProjectTime", "OpenPopUp('" & url & "');", True)

                            Me.Page.GetType.InvokeMember("ReloadDayDetailsControl", System.Reflection.BindingFlags.InvokeMethod, Nothing, Me.Page, New Object() {})
                        Case 2
                            If numCategory = 1 OrElse numCategory = 2 Then
                                Dim url As String = Page.ResolveClientUrl("~/projects/frmProTime.aspx") & "?title=1&type=" & hdnDayDetailType.Value & "&ProStageID=" & e.Item.Cells(5).Text & "&Proid=" & e.Item.Cells(4).Text & "&DivId=" & e.Item.Cells(6).Text & "&Date=" & hdnDayDetailDate.Value & "&CatID=" & e.Item.Cells(0).Text & "&CntID=" & hdnDayDetailUserCntID.Value & "&CatHdrId=" & e.Item.Cells(0).Text
                                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "ProjectTime", "OpenPopUp('" & url & "');", True)

                                Me.Page.GetType.InvokeMember("ReloadDayDetailsControl", System.Reflection.BindingFlags.InvokeMethod, Nothing, Me.Page, New Object() {})
                            Else : Response.Redirect("~/projects/frmProExpense.aspx?title=1&type=" & hdnDayDetailType.Value & "&frm=TE&ProStageID=" & e.Item.Cells(5).Text & "&Proid=" & e.Item.Cells(4).Text & "&DivId=" & e.Item.Cells(6).Text & "&Date=" & hdnDayDetailDate.Value & "&CatID=" & e.Item.Cells(0).Text & "&CntID=" & hdnDayDetailUserCntID.Value)
                            End If
                        Case 3
                            If numCategory = 1 Then
                                Response.Redirect("~/cases/frmCasetime.aspx?title=1&type=" & hdnDayDetailType.Value & "&frm=TE&CommId=" & e.Item.Cells(5).Text & "&caseId=" & e.Item.Cells(3).Text & "&CaseTimeId=" & e.Item.Cells(0).Text & "&DivId=" & e.Item.Cells(6).Text & "&Date=" & hdnDayDetailDate.Value & "&CatID=" & e.Item.Cells(0).Text & "&CntID=" & hdnDayDetailUserCntID.Value)
                            Else : Response.Redirect("~/cases/frmCaseExpense.aspx?title=1&type=" & hdnDayDetailType.Value & "&frm=TE&CommId=" & e.Item.Cells(5).Text & "&caseId=" & e.Item.Cells(3).Text & "&CaseExpId=" & e.Item.Cells(0).Text & "&DivId=" & e.Item.Cells(6).Text & "&Date=" & hdnDayDetailDate.Value & "&CatID=" & e.Item.Cells(0).Text & "&CntID=" & hdnDayDetailUserCntID.Value)
                            End If
                    End Select
                End If
                If e.CommandName = "Delete" Then
                    Dim objTimeAndExp As New TimeExpenseLeave
                    objTimeAndExp.CategoryHDRID = e.Item.Cells(0).Text
                    objTimeAndExp.DomainID = Session("DomainId")
                    objTimeAndExp.DeleteTimeExpLeave()

                    Me.Page.GetType.InvokeMember("ReloadDayDetailsControl", System.Reflection.BindingFlags.InvokeMethod, Nothing, Me.Page, New Object() {})
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Private Sub dgDetails_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgDetails.ItemDataBound
            Try
                If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
                    If m_aryRightsForPage(RIGHTSTYPE.ADD) = 0 Then
                        Dim editImage As LinkButton
                        editImage = DirectCast(e.Item.FindControl("editImage"), LinkButton)
                        If editImage IsNot Nothing Then
                            editImage.Visible = False
                        End If
                    End If

                    If m_aryRightsForPage(RIGHTSTYPE.DELETE) = 0 Then
                        Dim btnDelete As LinkButton
                        btnDelete = DirectCast(e.Item.FindControl("btnDelete"), LinkButton)
                        If btnDelete IsNot Nothing Then
                            btnDelete.Visible = False
                        End If

                        Dim lnkDelete As LinkButton
                        lnkDelete = DirectCast(e.Item.FindControl("lnkDelete"), LinkButton)
                        If lnkDelete IsNot Nothing Then
                            lnkDelete.Visible = False
                        End If
                    End If
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub
#End Region

#Region "Public Methods"
        Public Sub LoadGrid()
            Try
                hdnDayDetailType.Value = Type
                hdnDayDetailDate.Value = strDate
                If m_aryRightsForPage(RIGHTSTYPE.VIEW) = 3 Then
                    hdnDayDetailUserCntID.Value = UserCntID
                Else
                    hdnDayDetailUserCntID.Value = Convert.ToInt32(Session("UserContactID"))
                End If


                Dim objTimeAndExp As New TimeExpenseLeave
                objTimeAndExp.UserCntID = hdnDayDetailUserCntID.Value
                objTimeAndExp.DomainID = Session("DomainID")
                objTimeAndExp.strDate = strDate
                objTimeAndExp.CategoryID = Type
                objTimeAndExp.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
                Dim dtTime As DataTable
                If dateFlag = 1 Then
                    objTimeAndExp.FromDate = StartDate
                    objTimeAndExp.ToDate = EndDate
                    dtTime = objTimeAndExp.GetTimeAndExpDetailsByDate
                Else
                    dtTime = objTimeAndExp.GetTimeAndExpDetails
                End If


                dgDetails.DataSource = dtTime
                dgDetails.DataBind()
                'If Session("bitApprovalforTImeExpense") = "1" Then
                '    dgDetails.Columns(20).Visible = True
                'End If
                If dtTime.Rows.Count = 0 Then
                    lblEmptyDataText.Text = "No Time And Expense records are found."
                Else
                    lblEmptyDataText.Text = ""
                End If
            Catch ex As Exception
                Throw
            End Try
        End Sub
#End Region

    End Class
End Namespace