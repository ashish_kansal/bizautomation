Imports BACRM.BusinessLogic.TimeAndExpense
Imports BACRM.BusinessLogic.Opportunities
Imports BACRM.BusinessLogic.Projects
Imports BACRM.BusinessLogic.Case
Imports BACRM.BusinessLogic.Common
Namespace BACRM.UserInterface.TimeAndExpense
    Partial Public Class frmDaydetails : Inherits BACRMPage

        Dim type As Short
        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            Try
                GetUserRightsForPage(MODULEID.TimeAndExpense, PAGEID.TIME_AND_EXPENSE)
                type = CCommon.ToShort(GetQueryStringVal("type"))
                If Not IsPostBack Then LoadGrid()

            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Sub LoadGrid()
            Try
                Dim objTimeAndExp As New TimeExpenseLeave
                objTimeAndExp.UserCntID = GetQueryStringVal("CntID")
                objTimeAndExp.DomainID = Session("DomainID")
                objTimeAndExp.strDate = GetQueryStringVal("Date")
                objTimeAndExp.CategoryID = type 'GetQueryStringVal("type")
                objTimeAndExp.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
                Dim dtTime As DataTable = objTimeAndExp.GetTimeAndExpDetails
                dgDetails.DataSource = dtTime
                dgDetails.DataBind()

                If dtTime.Rows.Count = 0 Then
                    'dgDetails.ShowFooter = True
                    'DirectCast(dgDetails.Items(0).FindControl("lblEmptyDataText"), Label).Text = "No Time And Expense data is found."
                    'Response.Redirect("../TimeAndExpense/frmAddTimeExp.aspx?Date=" & GetQueryStringVal("Date") & "&CntID=" & GetQueryStringVal("CntID") & "&Mode=AddTime")
                    lblEmptyDataText.Text = "No Time And Expense records are found."
                Else
                    lblEmptyDataText.Text = ""
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub dgDetails_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgDetails.ItemCommand
            Try
                If e.CommandName = "Edit" Then
                    Dim numCategory As Int16 = e.Item.Cells(15).Text

                    Select Case e.Item.Cells(2).Text
                        Case 0
                            'Response.Redirect("../projects/frmProTime.aspx?type=" & type & "&frm=TE&ProStageID=" & e.Item.Cells(6).Text & "&Proid=0&DivId=" & e.Item.Cells(7).Text & "&Date=" & GetQueryStringVal("Date") & "&CatID=" & e.Item.Cells(1).Text & "&CntID=" & GetQueryStringVal("CntID") & "&CatHdrId=" & e.Item.Cells(1).Text)
                            Response.Redirect("../TimeAndExpense/frmAddTimeExp.aspx?frm=EditTE&type=" & type & "&Date=" & GetQueryStringVal("Date") & "&CatID=" & e.Item.Cells(1).Text & "&CntID=" & e.Item.Cells(19).Text)
                        Case 1
                            Response.Redirect("../projects/frmProTime.aspx?title=1&type=" & type & "&frm=TE&ProStageID=" & e.Item.Cells(6).Text & "&Proid=0&DivId=" & e.Item.Cells(7).Text & "&Date=" & GetQueryStringVal("Date") & "&CatID=" & e.Item.Cells(1).Text & "&CntID=" & e.Item.Cells(19).Text & "&CatHdrId=" & e.Item.Cells(1).Text)
                        Case 2
                            If numCategory = 1 OrElse numCategory = 2 Then
                                Response.Redirect("../projects/frmProTime.aspx?title=1&type=" & type & "&frm=TE&ProStageID=" & e.Item.Cells(6).Text & "&Proid=" & e.Item.Cells(5).Text & "&DivId=" & e.Item.Cells(7).Text & "&Date=" & GetQueryStringVal("Date") & "&CatID=" & e.Item.Cells(1).Text & "&CntID=" & GetQueryStringVal("CntID") & "&CatHdrId=" & e.Item.Cells(1).Text)
                            Else : Response.Redirect("../projects/frmProExpense.aspx?title=1&type=" & type & "&frm=TE&ProStageID=" & e.Item.Cells(6).Text & "&Proid=" & e.Item.Cells(5).Text & "&DivId=" & e.Item.Cells(7).Text & "&Date=" & GetQueryStringVal("Date") & "&CatID=" & e.Item.Cells(1).Text & "&CntID=" & GetQueryStringVal("CntID"))
                            End If
                        Case 3
                            If numCategory = 1 Then
                                Response.Redirect("../cases/frmCasetime.aspx?title=1&type=" & type & "&frm=TE&CommId=" & e.Item.Cells(6).Text & "&caseId=" & e.Item.Cells(4).Text & "&CaseTimeId=" & e.Item.Cells(1).Text & "&DivId=" & e.Item.Cells(7).Text & "&Date=" & GetQueryStringVal("Date") & "&CatID=" & e.Item.Cells(1).Text & "&CntID=" & GetQueryStringVal("CntID"))
                            Else : Response.Redirect("../cases/frmCaseExpense.aspx?title=1&type=" & type & "&frm=TE&CommId=" & e.Item.Cells(6).Text & "&caseId=" & e.Item.Cells(4).Text & "&CaseExpId=" & e.Item.Cells(1).Text & "&DivId=" & e.Item.Cells(7).Text & "&Date=" & GetQueryStringVal("Date") & "&CatID=" & e.Item.Cells(1).Text & "&CntID=" & GetQueryStringVal("CntID"))
                            End If
                    End Select
                End If
                If e.CommandName = "Delete" Then
                    Dim objTimeAndExp As New TimeExpenseLeave
                    objTimeAndExp.CategoryHDRID = e.Item.Cells(1).Text
                    objTimeAndExp.DomainID = Session("DomainId")
                    objTimeAndExp.DeleteTimeExpLeave()
                    LoadGrid()
                    Dim strScript As String
                    strScript = "<script language='JavaScript'>"
                    strScript += " window.parent.document.getElementById('hdnDate').value='" + GetQueryStringVal( "Date") + "';"
                    strScript += "window.parent.document.getElementById('btnrefresh').click();</script>"
                    'ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "msg", strScript, False)
                    Page.RegisterStartupScript("clientScript", strScript)
                    'Response.Redirect("../TimeAndExpense/frmAddTimeExp.aspx?Date=" & GetQueryStringVal( "Date") & "&CatID=" & e.Item.Cells(1).Text & "&CntID=" & GetQueryStringVal( "CntID"))

                   
                    '' LoadGrid()
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub dgDetails_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgDetails.ItemDataBound
            Try
                If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
                    If m_aryRightsForPage(RIGHTSTYPE.ADD) = 0 Then
                        Dim editImage As ImageButton
                        editImage = DirectCast(e.Item.FindControl("editImage"), ImageButton)
                        If editImage IsNot Nothing Then
                            editImage.Visible = False
                        End If
                    End If

                    If m_aryRightsForPage(RIGHTSTYPE.DELETE) = 0 Then
                        Dim btnDelete As Button
                        btnDelete = DirectCast(e.Item.FindControl("btnDelete"), Button)
                        If btnDelete IsNot Nothing Then
                            btnDelete.Visible = False
                        End If

                        Dim lnkDelete As LinkButton
                        lnkDelete = DirectCast(e.Item.FindControl("lnkDelete"), LinkButton)
                        If lnkDelete IsNot Nothing Then
                            lnkDelete.Visible = False
                        End If
                    End If
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        'Private Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        '    Try
        '        Response.Redirect("../TimeAndExpense/frmAddTimeExp.aspx?Date=" & GetQueryStringVal( "Date") & "&CntID=" & GetQueryStringVal( "CntID"))
        '    Catch ex As Exception
        '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
        '        Response.Write(ex)
        '    End Try
        'End Sub

    End Class
End Namespace