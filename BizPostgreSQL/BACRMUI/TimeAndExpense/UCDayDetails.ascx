﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="UCDayDetails.ascx.vb" Inherits="BACRM.UserInterface.TimeAndExpense.UCDayDetails" %>

<script type="text/javascript">
    function OpenOpp(a, b) {
        if (a > 0) {
            var str;
            str = "../opportunity/frmOpportunities.aspx?frm=OpenBizDoc&OpID=" + a + "&OppType=" + b;

            if (window.opener == null)
                window.parent.location.href = str;
            else if (window.opener.opener == null)
                window.opener.location.href = str;
            else
                window.opener.opener.frames.document.location.href = str;
        }
    }

    function OpenProject(a) {
        var str;
        str = "../projects/frmProjects.aspx?ProId=" + a;

        if (window.opener == null)
            window.parent.location.href = str;
        else if (window.opener.opener == null)
            window.opener.location.href = str;
        else
            window.opener.opener.frames.document.location.href = str;
    }

</script>

<asp:Label ID="lbldate" runat="server" CssClass="text"></asp:Label>
<asp:Button ID="btnAdd" runat="Server" Text="Add" Width="50" CssClass="button" Visible="false" />
<asp:DataGrid ID="dgDetails" runat="server" Width="100%" CssClass="table table-bordered table-striped" AutoGenerateColumns="False" UseAccessibleHeader="true">
    <Columns>
        <asp:BoundColumn DataField="numCategoryHDRID" Visible="false"></asp:BoundColumn>
        <asp:BoundColumn DataField="tintTEType" Visible="false"></asp:BoundColumn>
        <asp:BoundColumn DataField="numOppId" Visible="false"></asp:BoundColumn>
        <asp:BoundColumn DataField="numCaseid" Visible="false"></asp:BoundColumn>
        <asp:BoundColumn DataField="numProid" Visible="false"></asp:BoundColumn>
        <asp:BoundColumn DataField="numStageID" Visible="false"></asp:BoundColumn>
        <asp:BoundColumn DataField="numDivisionID" Visible="false"></asp:BoundColumn>
        <asp:BoundColumn DataField="txtDesc" Visible="false"></asp:BoundColumn>
        <asp:BoundColumn DataField="Category" HeaderText="Type"></asp:BoundColumn>
        <asp:BoundColumn DataField="CreatedDate" HeaderText="Entered By, On"></asp:BoundColumn>
        <asp:TemplateColumn HeaderText="Sales Order (Service)">
            <itemtemplate>
                <a href="<%#If(Eval("numOppId") > 0,"javascript:OpenOpp(" & Eval("numOppId") & ")","") %>"><%#Eval("vcPOppname")%></a> <%#Eval("vcItemName") %>
            </itemtemplate>
        </asp:TemplateColumn>
        <asp:TemplateColumn HeaderText="Time" ItemStyle-HorizontalAlign="Right" Visible="false">
            <ItemTemplate>
                <%#Eval("TimeValue")%>
            </ItemTemplate>
        </asp:TemplateColumn>
        <asp:TemplateColumn HeaderText="Expense" ItemStyle-HorizontalAlign="Right" Visible="false">
            <ItemTemplate>
                <%#Eval("ExpenseValue")%>
            </ItemTemplate>
        </asp:TemplateColumn>
        <asp:BoundColumn DataField="Detail" Visible="false" DataFormatString="{0:#,##0.00}" HeaderText="" ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
        <asp:BoundColumn DataField="Type" HeaderText="Type"  Visible="false"></asp:BoundColumn>
        <asp:BoundColumn DataField="ClientCharge" HeaderText="Amt billed to client" ></asp:BoundColumn>
        <asp:BoundColumn DataField="EmployeeCharge" HeaderText="Employee Cost" ></asp:BoundColumn>
        <asp:BoundColumn DataField="vcEmployee" HeaderText="Employee"></asp:BoundColumn>

        <asp:BoundColumn DataField="numCategory" Visible="false"></asp:BoundColumn>
        <asp:BoundColumn DataField="numtype" Visible="false"></asp:BoundColumn>
        <asp:BoundColumn DataField="monAmount" HeaderText="Rate/Hr" Visible="false" ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
        
        <asp:BoundColumn DataField="numUserCntID" Visible="false"></asp:BoundColumn>
        <asp:BoundColumn DataField="vcItemDesc" HeaderText="Notes"></asp:BoundColumn>
        <asp:BoundColumn DataField="ApprovalStatus" HeaderText="Approval Status"></asp:BoundColumn>
        <asp:TemplateColumn>
            <ItemTemplate>
                <div style="white-space: nowrap;">
                    <asp:LinkButton ID="editImage" runat="server" Visible="false" CssClass="btn btn-xs btn-info" CommandName="Edit"><i class="fa fa-pencil"></i></asp:LinkButton>
                    <asp:LinkButton ID="btnDelete" runat="server" Visible="false" CssClass="btn btn-xs btn-danger" CommandName="Delete"><i class="fa fa-trash"></i></asp:LinkButton>
                    <asp:LinkButton ID="lnkDelete" runat="server" Visible="false"><font color="#730000">*</font></asp:LinkButton>
                    <%-- <asp:HiddenField ID="hdnAuthority" Value="<%#Eval("Authority")%>" runat="server" />
                    <asp:LinkButton ID="lnkApprove" runat="server" CssClass="btn btn-xs btn-primary" CommandName="Approve"><i class="fa fa-thumbs-o-up"></i></asp:LinkButton>
                    <asp:LinkButton ID="lnkDecline" runat="server" CssClass="btn btn-xs btn-danger" CommandName="Decline"><i class="fa fa-thumbs-o-down"></i></asp:LinkButton>--%>
                </div>
            </ItemTemplate>
        </asp:TemplateColumn>
    </Columns>
</asp:DataGrid>

<asp:HiddenField ID="hdnDayDetailType" runat="server" />
<asp:HiddenField ID="hdnDayDetailDate" runat="server" />
<asp:HiddenField ID="hdnDayDetailUserCntID" runat="server" />

<asp:Label runat="server" ID="lblEmptyDataText"> </asp:Label>


