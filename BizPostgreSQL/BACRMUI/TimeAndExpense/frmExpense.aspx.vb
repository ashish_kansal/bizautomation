﻿Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Admin
Imports System.Web.Services
Imports BACRM.BusinessLogic.Opportunities
Imports Newtonsoft.Json
Imports BACRM.BusinessLogic.TimeAndExpense
Imports BACRM.BusinessLogic.Accounting
Namespace BACRM.UserInterface.TimeAndExpense

    Public Class frmExpense
        Inherits BACRMPage

#Region "Page Events"

        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            Try
                lblError.Text = ""
                divError.Style.Add("display", "none")

                If Not Page.IsPostBack Then
                    GetUserRightsForPage(MODULEID.TimeAndExpense, PAGEID.TIME_AND_EXPENSE)

                    objCommon.sb_FillConEmpFromDBSel(ddlEmployee, Session("DomainID"), 0, 0)

                    If Not ddlEmployee.Items.FindByValue(CCommon.ToLong(Session("UserContactID"))) Is Nothing Then
                        ddlEmployee.Items.FindByValue(CCommon.ToLong(Session("UserContactID"))).Selected = True
                    End If

                    If m_aryRightsForPage(RIGHTSTYPE.ADD) = 0 Then
                        ddlEmployee.Enabled = False
                        btnSave.Visible = False
                        btnSaveClose.Visible = False
                    ElseIf m_aryRightsForPage(RIGHTSTYPE.ADD) = 1 Then
                        ddlEmployee.Enabled = False
                    ElseIf m_aryRightsForPage(RIGHTSTYPE.ADD) = 2 Then
                        ddlEmployee.Enabled = False
                    End If

                    BusinessLogic.Admin.CAdmin.sb_BindClassCombo(ddlClass, Session("DomainID"))

                    LoadServiceItem()
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

#End Region

#Region "Private Methods"

        Private Sub LoadServiceItem()
            Try
                Dim objItem As New BusinessLogic.Item.CItems
                objItem.DomainID = Session("DomainID")
                objItem.Filter = "S"
                objItem.type = 3
                Dim ds As DataSet = objItem.getItemList()

                hdnServiceItemData.Value = JsonConvert.SerializeObject(ds.Tables(0), Formatting.None)
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Shared Sub SaveExpense(ByVal employeeID As Long, ByVal DivID As Double, ByVal ClassID As Double, ByVal ServiceItemID As Double, ByVal IsBillable As Boolean, ByVal Rates As Double, _
                                ByVal SOId As Double, ByVal Notes As String, ByVal dtFromDate As Date, ByVal dtToDate As Date, ByVal IsReimburse As Boolean)
            Try
                Dim objTimeExp As New TimeExpenseLeave
                Dim lngProjAccountID As Long = 0
                Dim lngEmpPayrollExpenseAcntID As Long = 0
                Dim lngAPAccountID As Long = 0
                Dim lngDebtAccountID As Long = 0

                Dim lngEmpAccountIDB As Long

                If IsReimburse = False And IsBillable = True Then
                    If SOId > 0 Then
                        'Validate default Payroll Expense Account
                        lngEmpPayrollExpenseAcntID = ChartOfAccounting.GetDefaultAccount("EP", HttpContext.Current.Session("DomainID")) 'Employee Payeroll Expense
                        If lngEmpPayrollExpenseAcntID = 0 Then
                            Throw New Exception("DEFAULT_EP_NOT_CONFIGURED")
                        End If

                        lngDebtAccountID = lngEmpPayrollExpenseAcntID
                        If lngDebtAccountID = 0 Then
                            Throw New Exception("DEFAULT_RE_NOT_CONFIGURED")
                        End If

                        lngAPAccountID = ChartOfAccounting.GetDefaultAccount("AP", HttpContext.Current.Session("DomainID"))
                        If lngAPAccountID = 0 Then
                            Throw New Exception("DEFAULT_AP_NOT_CONFIGURED")
                        End If
                    End If
                ElseIf IsReimburse = True And IsBillable = False Then
                    lngEmpPayrollExpenseAcntID = ChartOfAccounting.GetDefaultAccount("RE", HttpContext.Current.Session("DomainID")) 'Employee Payeroll Expense
                    lngDebtAccountID = lngEmpPayrollExpenseAcntID
                    If lngDebtAccountID = 0 Then
                        Throw New Exception("DEFAULT_RE_NOT_CONFIGURED")
                    End If

                    lngAPAccountID = ChartOfAccounting.GetDefaultAccount("AP", HttpContext.Current.Session("DomainID"))
                    If lngAPAccountID = 0 Then
                        Throw New Exception("DEFAULT_AP_NOT_CONFIGURED")
                    End If
                ElseIf IsReimburse = True And IsBillable = True Then
                    lngEmpPayrollExpenseAcntID = ChartOfAccounting.GetDefaultAccount("RE", HttpContext.Current.Session("DomainID")) 'Employee Payeroll Expense
                    lngDebtAccountID = lngEmpPayrollExpenseAcntID
                    If lngDebtAccountID = 0 Then
                        Throw New Exception("DEFAULT_RE_NOT_CONFIGURED")
                    End If

                    lngAPAccountID = ChartOfAccounting.GetDefaultAccount("AP", HttpContext.Current.Session("DomainID"))
                    If lngAPAccountID = 0 Then
                        Throw New Exception("DEFAULT_AP_NOT_CONFIGURED")
                    End If
                End If

                objTimeExp.CategoryHDRID = 0 'CCommon.ToLong(GetQueryStringVal("CatID"))
                objTimeExp.CategoryID = 2
                objTimeExp.ContractID = 0
                objTimeExp.DivisionID = DivID
                objTimeExp.ClassID = ClassID
                objTimeExp.Amount = Rates
                objTimeExp.FromDate = dtFromDate
                objTimeExp.ToDate = dtToDate
                objTimeExp.bitReimburse = IsReimburse
                objTimeExp.ServiceItemID = ServiceItemID
                If IsBillable = True AndAlso IsReimburse = True Then
                    objTimeExp.CategoryType = 6
                ElseIf IsBillable = False AndAlso IsReimburse = True Then
                    objTimeExp.CategoryType = 2
                ElseIf IsBillable = True Then
                    objTimeExp.CategoryType = 1
                End If
                objTimeExp.ProID = 0
                objTimeExp.OppID = SOId
                objTimeExp.Expid = 0
                objTimeExp.StageId = 0
                objTimeExp.CaseID = 0
                objTimeExp.OppBizDocsId = 0
                objTimeExp.Desc = Notes
                objTimeExp.UserCntID = employeeID
                objTimeExp.DomainID = HttpContext.Current.Session("DomainID")
                If SOId > 0 Then
                    objTimeExp.TEType = 1
                Else
                    objTimeExp.TEType = 0
                End If

                Dim ApprovalTrans As New ApprovalConfig
                ApprovalTrans.UserId = employeeID
                If HttpContext.Current.Session("bitApprovalforTImeExpense") = "1" Then
                    If HttpContext.Current.Session("intTimeExpApprovalProcess") = "1" Then
                        objTimeExp.ApprovalStatus = 6
                        ApprovalTrans.chrAction = "CF"
                        ApprovalTrans.ApprovalStatus = 6
                    ElseIf HttpContext.Current.Session("intTimeExpApprovalProcess") = "2" Then
                        objTimeExp.ApprovalStatus = 1
                        ApprovalTrans.chrAction = "CH"
                        ApprovalTrans.ApprovalStatus = 1
                    End If
                    ApprovalTrans.strOutPut = "INPUT"
                    ApprovalTrans.UpdateApprovalTransaction()
                    If ApprovalTrans.strOutPut <> "VALID" Then
                        Throw New Exception("APPROVAL_PROCESS_NOT_CONFIGURED")
                    End If
                Else
                    objTimeExp.ApprovalStatus = 0
                End If

                objTimeExp.CreatedBy = HttpContext.Current.Session("UserContactID")
                objTimeExp.ManageTimeAndExpense()
                If HttpContext.Current.Session("bitApprovalforTImeExpense") <> "1" Then
                    'Create journal Entry for Reimbursable Expense
                    'Debit:Expense Account ,Credit:Employee's Account
                    If objTimeExp.CategoryHDRID > 0 AndAlso CCommon.ToDouble(Rates) > 0 AndAlso ((IsReimburse = True) OrElse (IsBillable = True)) Then
                        If IsBillable = True Then
                            If ServiceItemID > 0 Then
                                Dim objinvoice As New OppBizDocs
                                objinvoice.ItemCode = ServiceItemID
                                objinvoice.ItemDesc = Notes.Trim()
                                objinvoice.Price = Rates
                                objinvoice.UnitHour = 1 'DateDiff(DateInterval.Minute, dtFromDate, dtToDate, Microsoft.VisualBasic.FirstDayOfWeek.Monday) / 60
                                objinvoice.OppId = SOId
                                objinvoice.OppBizDocId = 0
                                objinvoice.CategoryHDRID = objTimeExp.CategoryHDRID
                                objinvoice.ProID = 0
                                objinvoice.StageId = 0
                                objinvoice.ClassId = ClassID
                                objinvoice.AddItemToExistingInvoice()
                            End If
                        End If

                        '/******* 23/11/2013 : C.Patel  Don't do any account entries. Manage from Payroll********/
                        Dim lngJournalId As Integer = 0
                        Try
                            Using objTransactionScope As New System.Transactions.TransactionScope(Transactions.TransactionScopeOption.Required, New System.Transactions.TransactionOptions() With {.IsolationLevel = Transactions.IsolationLevel.ReadCommitted, .Timeout = Transactions.TransactionManager.MaximumTimeout})
                                Dim objJEHeader As New JournalEntryHeader
                                With objJEHeader
                                    .JournalId = lngJournalId
                                    .RecurringId = 0
                                    .EntryDate = dtFromDate 'Date.UtcNow
                                    .Description = "TimeAndExpense"
                                    .Amount = Rates
                                    .CheckId = 0
                                    .CashCreditCardId = 0
                                    .ChartAcntId = 0
                                    .OppId = 0
                                    .OppBizDocsId = 0
                                    .DepositId = 0
                                    .BizDocsPaymentDetId = 0
                                    .IsOpeningBalance = 0
                                    .LastRecurringDate = Date.Now
                                    .NoTransactions = 0
                                    .CategoryHDRID = objTimeExp.CategoryHDRID
                                    .ReturnID = 0
                                    .CheckHeaderID = 0
                                    .BillID = 0
                                    .BillPaymentID = 0
                                    .UserCntID = employeeID
                                    .DomainID = HttpContext.Current.Session("DomainID")
                                End With
                                lngJournalId = objJEHeader.Save()

                                Dim objJEList As New JournalEntryCollection
                                Dim objJE As New JournalEntryNew()
                                objJE.TransactionId = 0
                                objJE.DebitAmt = Rates
                                objJE.CreditAmt = 0
                                objJE.ChartAcntId = lngDebtAccountID
                                objJE.Description = "TimeAndExpense"
                                objJE.CustomerId = 0
                                objJE.MainDeposit = 0
                                objJE.MainCheck = 0
                                objJE.MainCashCredit = 0
                                objJE.OppitemtCode = 0
                                objJE.BizDocItems = ""
                                objJE.Reference = ""
                                objJE.PaymentMethod = 0
                                objJE.Reconcile = False
                                objJE.CurrencyID = 0
                                objJE.FltExchangeRate = 0
                                objJE.TaxItemID = 0
                                objJE.BizDocsPaymentDetailsId = 0
                                objJE.ContactID = 0
                                objJE.ItemID = 0
                                objJE.ProjectID = 0
                                objJE.ClassID = 0
                                objJE.CommissionID = 0
                                objJE.ReconcileID = 0
                                objJE.Cleared = 0
                                objJE.ReferenceType = 0
                                objJE.ReferenceID = 0

                                objJEList.Add(objJE)

                                objJE = New JournalEntryNew()
                                objJE.TransactionId = 0
                                objJE.DebitAmt = 0
                                objJE.CreditAmt = CCommon.ToDecimal(Rates)
                                objJE.ChartAcntId = lngAPAccountID
                                objJE.Description = "TimeAndExpense"
                                objJE.CustomerId = 0
                                objJE.MainDeposit = 0
                                objJE.MainCheck = 0
                                objJE.MainCashCredit = 0
                                objJE.OppitemtCode = 0
                                objJE.BizDocItems = ""
                                objJE.Reference = ""
                                objJE.PaymentMethod = 0
                                objJE.Reconcile = False
                                objJE.CurrencyID = 0
                                objJE.FltExchangeRate = 0
                                objJE.TaxItemID = 0
                                objJE.BizDocsPaymentDetailsId = 0
                                objJE.ContactID = 0
                                objJE.ItemID = 0
                                objJE.ProjectID = 0
                                objJE.ClassID = 0
                                objJE.CommissionID = 0
                                objJE.ReconcileID = 0
                                objJE.Cleared = 0
                                objJE.ReferenceType = 0
                                objJE.ReferenceID = 0

                                objJEList.Add(objJE)


                                objJEList.Save(JournalEntryCollection.JournalMode.DeleteUpdateInsert, lngJournalId, HttpContext.Current.Session("DomainID"))
                                objTransactionScope.Complete()
                            End Using
                        Catch ex As Exception
                            Throw ex
                        End Try
                        '/****************************************************/
                    End If

                End If
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Private Sub DisplayError(ByVal errorMessage As String)
            Try
                lblError.Text = errorMessage
                divError.Style.Add("display", "")
            Catch ex As Exception
                'DO NOT THROW ERROR
            End Try
        End Sub

#End Region

#Region "Web Methods"

        <WebMethod(EnableSession:=True)> _
        Public Shared Function FillSalesOrder(ByVal DivID As Long, ByVal DomainID As Long) As String
            Try
                Dim objOpp As New MOpportunity
                Dim dt As DataTable
                objOpp.DomainID = DomainID
                objOpp.DivisionID = DivID
                objOpp.Mode = 3
                dt = objOpp.GetOpenRecords()

                Dim intCount As Integer = 0
                Dim str As New System.Text.StringBuilder
                If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then
                    str.Append("[")

                    For intCount = 0 To dt.Rows.Count - 1
                        str.Append("{")
                        str.Append("""id"" : """ & dt.Rows(intCount)(dt.Columns(0).ColumnName) & """,")
                        str.Append("""value"" : """ & dt.Rows(intCount)(dt.Columns(1).ColumnName) & """")
                        str.Append("},")
                    Next

                    str.Remove(str.ToString.Length - 1, 1)
                    str.Append("]")
                    If dt.Rows.Count <= 0 Then
                        Return ("[]")
                    End If
                    str.Remove(0, 1)
                    str.Remove(str.Length - 1, 1)
                    Return ("[" & str.ToString & "]")

                Else
                    Return ""

                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        <WebMethod(EnableSession:=True)> _
        Public Shared Function SaveEntries(ByVal employeeID As Long, ByVal classID As Long, ByVal expenseEntries As String) As String
            Try
                If Not String.IsNullOrEmpty(expenseEntries) Then
                    Dim dtTimeEntry As DataTable = JsonConvert.DeserializeObject(Of DataTable)(expenseEntries)

                    If Not dtTimeEntry Is Nothing AndAlso dtTimeEntry.Rows.Count > 0 Then
                        Using objTransactionScope As New System.Transactions.TransactionScope(Transactions.TransactionScopeOption.Required, New System.Transactions.TransactionOptions() With {.IsolationLevel = Transactions.IsolationLevel.ReadCommitted, .Timeout = Transactions.TransactionManager.MaximumTimeout})
                            For Each dr As DataRow In dtTimeEntry.Rows
                                Dim entryDate As DateTime = Convert.ToDateTime(dr("Date"))
                                Dim fromDate As DateTime = New Date(entryDate.Year, entryDate.Month, entryDate.Day, 0, 0, 0)
                                Dim toDate As DateTime = fromDate
                                SaveExpense(employeeID, CCommon.ToLong(dr("DivisionID")), classID, CCommon.ToLong(dr("ItemID")), CCommon.ToBool(dr("IsBillable")), CCommon.ToDouble(dr("Amount")), CCommon.ToLong(dr("OppID")), CCommon.ToString(dr("Notes")), fromDate, toDate, CCommon.ToBool(dr("IsReimbursable")))
                            Next

                            objTransactionScope.Complete()
                        End Using
                    End If
                End If
            Catch ex As Exception
                If ex.Message.Contains("FY_CLOSED") Then
                    Throw New Exception("You can't add expense entries once financial year is closed.")
                ElseIf ex.Message.Contains("DEFAULT_AP_NOT_CONFIGURED") Then
                    Throw New Exception("Please Set Default Accounts Payable Account from ""Administration -> Global Settings -> Accounting ->Default Accounts"".")
                ElseIf ex.Message.Contains("DEFAULT_EP_NOT_CONFIGURED") Then
                    Throw New Exception("Please Set Default Employee Payroll Expense Account from ""Administration -> Global Settings -> Accounting ->Default Accounts"".")
                ElseIf ex.Message.Contains("DEFAULT_RE_NOT_CONFIGURED") Then
                    Throw New Exception("Please Set Default Reimbursable Expense Receivable Account from ""Administration -> Global Settings -> Accounting ->Default Accounts"".")
                ElseIf ex.Message.Contains("APPROVAL_PROCESS_NOT_CONFIGURED") Then
                    Throw New Exception("Please Set Approval Authority from ""Administration -> Approval Process"".")
                Else
                    ExceptionModule.ExceptionPublish(ex, HttpContext.Current.Session("DomainID"), HttpContext.Current.Session("UserContactID"), HttpContext.Current.Request)
                    Throw New Exception(ex.Message)
                End If
            End Try
        End Function

#End Region

    End Class

End Namespace

