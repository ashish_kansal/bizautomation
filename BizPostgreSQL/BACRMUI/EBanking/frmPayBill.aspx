﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmPayBill.aspx.vb"
    MasterPageFile="~/common/Popup.Master" Inherits=".frmPayBill1" %>

<%@ Register TagPrefix="BizCalendar" TagName="Calendar" Src="../include/calandar.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" type="text/css" href="../CSS/jscal2.css" />
    <link rel="stylesheet" type="text/css" href="../CSS/border-radius.css" />
    <script type="text/javascript" src="../JavaScript/jscal2.js"></script>
    <script type="text/javascript" src="../JavaScript/en.js"></script>
    <script type="text/javascript" language="javascript">
        $(document).ready(function () {
            InitializeValidation();
        });

        function PayBill() {

            if (document.getElementById("txtMemo").value == "") {
                alert("Enter Memo text");
                document.getElementById("txtMemo").focus();
                return false;
            }
            if (document.getElementById("txtAmount").value == "") {
                alert("Enter Amount");
                document.getElementById("txtAmount").focus();
                return false;
            }
            
            return true;
        }
    </script>
</asp:Content>

<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="FiltersAndViews1">
    <div class="right-input">
        <div class="input-part">
            <table>
                <tr>
                    <td class="leftBorder" valign="bottom">
                        <a href="#" class="help">&nbsp;</a>
                    </td>
                    <td> <asp:button runat="server" ID="btnClose" CssClass="button" Text="Close" OnClientClick="window.close();return false;" /></td>
                </tr>
            </table>
        </div>
    </div>

</asp:Content>

<asp:Content ID="Content3" runat="server" ContentPlaceHolderID="PageTitle">
    Pay Bill
</asp:Content>

<asp:Content ID="Content4" runat="server" ContentPlaceHolderID="Content">
    <ul id="messagebox" class="errorInfo" style="display: none">
    </ul>
    <div id="dvPayBill" runat="server" style="width:500px">
        <table cellpadding="0" cellspacing="0" align="center" border="0">

            <tr style="height:22px;">
                <td><b>Pay to :</b></td>
                    <td><asp:Label runat="server" ID="lblName"></asp:Label></td>
            </tr>
            <tr>
                <td><asp:Label runat="server" ID="Label1" Text=" Account : " Font-Bold="true"></asp:Label></td>
                <td>
                    <asp:Label runat="server" ID="lblAccount"></asp:Label>
                </td>
            </tr>
            <tr style="height:22px;">
                <td >
                    <b>Address :  </b>
                </td>
                <td colspan="2"><asp:Label runat="server" ID="lblAddressline1"></asp:Label></td>
            </tr>
            <tr style="height:22px;">
                <td></td>
                <td colspan="2">
                    <asp:Label runat="server" ID="lblAddressline2"></asp:Label></td>
            </tr>
            <tr style="height:22px;">
                <td></td>
                <td colspan="2">
                    <asp:Label runat="server" ID="lblAddressline3"></asp:Label>
                </td>
            </tr>

       <%-- </table>
        <table cellpadding="0" cellspacing="0" align="center">--%>
            <tr style="height:22px;">
                <td>
                    <asp:Label runat="server" ID="lblMemo" Text="Memo"></asp:Label></td>
                <td>
                    <asp:TextBox runat="server" CssClass="signup" ID="txtMemo"  Width="180px"></asp:TextBox>
                </td>
            </tr>

            <tr style="height:32px;">
                <td>
                    <asp:Label runat="server" ID="lblAmount" Text="Amount"></asp:Label></td>
                <td>
                    <asp:TextBox runat="server"  ID="txtAmount" CssClass="signup" Width="50px"></asp:TextBox>
                </td>
            </tr>
            <tr style="height:22px;">
                <td>
                    <asp:Label runat="server" ID="lblDueDate" Text="Due Date"></asp:Label></td>
                <td>
                    <BizCalendar:Calendar ID="calDueDate" runat="server" ClientIDMode="AutoID" />
                </td>
            </tr>
            <tr style="height:25px;">
                <td></td>
                <td>
                    <asp:Button runat="server" ID="btnPayBill" Enabled="false" Text="Pay" CssClass="button" />
                </td>
            </tr>

        </table>
    </div>
    <div>
        <asp:HiddenField runat="server" ID="hdnFldBankDetailId" />
        <asp:HiddenField runat="server" ID="hdnFldPayeeDetailId" />

    </div>
</asp:Content>
