﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmTransactionMapping.aspx.vb" MasterPageFile="~/common/GridMasterRegular.Master" Inherits=".frmTransactionMapping" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Untitled Page</title>
    <script language="javascript" type="text/javascript">
        function ShowMessage() {
            alert('We are connecting to your Account.. Please visit again in few Minutes To see your Current Balance.');
        }
    </script>
    <style type="text/css">
        .Chkwidth
        {
            width: 130px;
            float: left;
        }
        .date1
        {
            width: 280px;
            float: none;
        }
        .RightHeading
        {
            font-weight: bold;
            width: 130px;
            float: right;
        }
        .Rightvalue
        {
            width: 280px;
            float: left;
        }
        .tbl1
        {
            margin-left:auto;
            margin-right:auto;
            width:90%;
            background-color:#EEECEC;
        }
        .tblrow
        {
           height:21px;
        }
        .fixpanel {
    width: 430px;
    overflow: hidden;
    display: inline-block;
    white-space: nowrap;
    height=auto;
}
    </style>


</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server" ClientIDMode="Static">
 Mapping Transactions
   
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="GridPlaceHolder" runat="server">
    <ul id="messagebox" class="errorInfo" style="display: none">
   </ul>
    
          
       <div>
       <div style="vertical-align:middle;"> <asp:Button ID="btnClose1" runat="server" CssClass="button" Text="Close"/>  </div>
       <div>
           <table cellspacing="2" cellpadding="0" border="0" align="center" width="100%">
        <tr align="left">
            <td>
                <table>
                    <tr>
                        <td style="font-weight: bold;">
                            Account :
                        </td>
                        <td align="left">
                            <asp:DropDownList ID="ddlCOAccounts" runat="server" CssClass="signup" AutoPostBack="true">
                            </asp:DropDownList>
                        </td>
                        <td><asp:HiddenField runat="server" ID="hdnFldCOAccountId" /></td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
       </div>
      <asp:UpdatePanel runat="server" ID="UpdatePanel1">
      
      <ContentTemplate>
      <div style="vertical-align:middle;font-size:small;width:70%;margin-left:auto;margin-right:auto;">
            <asp:Label id="lblError" runat="server" ForeColor="Crimson" Visible="false">Errors</asp:Label> <br />
       </div> 
           <asp:Repeater ID="RepTransDetails" runat="server">
<HeaderTemplate>
       <table class="tbl">
       <tr>
       <td><table><tr> 
       <td><asp:CheckBox  Width="30px"  runat="server" ID="chkSelected"/></td>
       <td><asp:Label  Width="120" Font-Size="Small" runat="server" ID="lblDateHeader" Visible="true" Text="Date"></asp:Label></td>
       <td><asp:Label  Width="220px" Font-Size="Small" runat="server" ID="Label1" Visible="true" Text="Description"></asp:Label></td>
       <td><asp:Label  Width="220px" Font-Size="Small" runat="server" ID="Label2" Visible="true" Text="Memo"></asp:Label></td>
       <td><asp:Label  Width="90px" Font-Size="Small" runat="server" ID="Label4" Visible="true" Text="Txn Type"></asp:Label></td>
       <td><asp:Label  Width="90px" Font-Size="Small" runat="server" ID="Label3" Visible="true" Text="Amount"></asp:Label></td>
       <td></td>
       <td></td></tr></table></td>
      
       </tr>
</HeaderTemplate>
<ItemTemplate>
<asp:UpdatePanel runat="server" ID="uppnlAccounts">
        <Triggers>
            <asp:AsyncPostBackTrigger controlid="btnSaveMatch" eventname="Click" />
            <asp:AsyncPostBackTrigger controlid="btnClose" eventname="Click" />
        </Triggers>
        <ContentTemplate>

<tr runat="server" id="TransRow">
    <td>
       <asp:LinkButton ID="LinkButton1" runat="server" Text="SingleClick" CommandName="SingleClick" Visible="false"/>
                <asp:LinkButton ID="LinkButton2" runat="server" Text="DoubleClick" CommandName="DoubleClick" Visible="false"/>
        <table class="tbl">
            <tr style="width:1200;">
            <td> <asp:CheckBox  Width="30px" runat="server" ID="chkSelected"/></td>
                <td>
                     <asp:Label  Width="120px" Font-Size="Small" runat="server" ID="lblDate" Text='<%#Eval("dtDatePosted") %>'></asp:Label>
                </td>
                <td>
                     <asp:Label Width="220px"  Font-Size="Small" runat="server" ID="lblDescription" Text='<%#Eval("vcPayeeName") %>'></asp:Label>
                </td>
                <td class="fixpanel" colspan="3">

                 <asp:Panel ID="pnlTrnsDetail" runat="server" Visible="true">
                    <table id="tblTrnsDetail"><tr>
                   <td>
                     <asp:Label Width="220px" Font-Size="Small" runat="server" ID="Label5" Text='<%#Eval("vcMemo") %>'></asp:Label>
                </td>
                <td>
                     <asp:Label Width="90px" Font-Size="Small" runat="server" ID="Label6" Text='<%#Eval("vcTxType") %>'></asp:Label>
                </td>
                <td>
                     <asp:Label Width="120px" Font-Size="Small" runat="server" ID="Label7" Text='<%#Eval("monAmount") %>'></asp:Label>
                </td>
                </tr></table>
                </asp:Panel>

                <asp:Panel ID="pnlMatch" runat="server" Visible="false">
                    <table id="tblMatch"><tr>
                   <td>
                    <asp:DropDownList runat="server" ID="ddlCOAccounts1" Width="120px"></asp:DropDownList>
                </td>
                <td>
                     <asp:DropDownList runat="server" ID="DropDownList1" Width="120px"></asp:DropDownList>
                </td>
                <td>
                     <asp:TextBox runat="server" ID="txtDesc"></asp:TextBox>
                </td>
                </tr>
                <tr><td><asp:Button runat = "server" ID="btnSaveMatch" Text="Accept" CssClass="button"/> </td>
                <td></td>
                <td><asp:Button runat = "server" ID="btnClose" Text="Close" CssClass="button"/> </td>
                </tr>
                </table>
                </asp:Panel>
            
                </td>
             
               <td align="right"><asp:Button Width="50px" ID="btnAccept" CommandName="btnEvent" 
               CommandArgument='<%#Eval("numTransactionId") %>' runat="server" CssClass="button" Text="Accept"/>  </td>
            </tr>
        </table>
    </td>
</tr>
  </ContentTemplate>
       </asp:UpdatePanel>

</ItemTemplate>
<FooterTemplate>
</table>
</FooterTemplate>
</asp:Repeater>
      </ContentTemplate>

      </asp:UpdatePanel>
      

     
       </div>
     
<div>
    <asp:Table id="tblDetails" runat="server" Width="664px" Height="80px"></asp:Table>
</div>
  
</asp:Content>
