﻿Imports BACRM.BusinessLogic.Accounting
Imports BACRM.BusinessLogic.Common

Public Class frmTransactionMapping
    Inherits BACRMPage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then
                BindDropDowns()
                PersistTable.Load()
                If PersistTable.Count > 0 Then
                    If Not ddlCOAccounts.Items.FindByValue(CCommon.ToString(PersistTable(ddlCOAccounts.ID))) Is Nothing Then
                        ddlCOAccounts.Items.FindByValue(CCommon.ToString(PersistTable(ddlCOAccounts.ID))).Selected = True
                        BindTransactions(CCommon.ToLong(PersistTable(ddlCOAccounts.ID)))

                    End If
                End If

                'LoadChartType()
                'BindTransactions()
            End If

        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub ddlCOAccounts_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCOAccounts.SelectedIndexChanged
        Try
            PersistTable.Clear()
            PersistTable.Add(ddlCOAccounts.ID, ddlCOAccounts.SelectedValue)
            PersistTable.Save()

            If ddlCOAccounts.SelectedItem.Value > 0 Then
                hdnFldCOAccountId.Value = ddlCOAccounts.SelectedItem.Value
                BindTransactions(CCommon.ToLong(ddlCOAccounts.SelectedItem.Value))

            Else
                'gvReconcile.DataSource = Nothing
                'gvReconcile.Visible = False

            End If

        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try

    End Sub


    Sub BindTransactions(ByVal COAId As Long)
        Try
            Dim dtTransactions As New DataTable
            Dim objEBanking As New BACRM.BusinessLogic.Accounting.EBanking

            objEBanking.DomainID = Session("DomainId")
            objEBanking.AccountID = COAId
            dtTransactions = objEBanking.GetAllTransactionsOfCOAId()

            If dtTransactions.Rows.Count > 0 Then
                RepTransDetails.Visible = True
                RepTransDetails.DataSource = dtTransactions
                RepTransDetails.DataBind()

            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub RepTransDetails_ItemCommand(ByVal source As Object, ByVal e As RepeaterCommandEventArgs) Handles RepTransDetails.ItemCommand

        Dim objEBanking As New BACRM.BusinessLogic.Accounting.EBanking
        Dim dt As DataTable
        Dim BankDetailID As Long
        Dim lastStatementDate As New DateTime
        Dim strMessage As String = ""
        Try

            If e.CommandName = "btnSaveEvent" Then
                AddSingleClickEvent(e)
                If Not e.Item.FindControl("pnlTrnsDetail") Is Nothing Then
                    e.Item.FindControl("pnlTrnsDetail").Visible = True
                End If

                If Not e.Item.FindControl("pnlMatch") Is Nothing Then
                    e.Item.FindControl("pnlMatch").Visible = False
                End If
                Return
            End If

            If e.CommandName = "btnCloseEvent" Then
                AddSingleClickEvent(e)
                If Not e.Item.FindControl("pnlTrnsDetail") Is Nothing Then
                    e.Item.FindControl("pnlTrnsDetail").Visible = True
                End If

                If Not e.Item.FindControl("pnlMatch") Is Nothing Then
                    e.Item.FindControl("pnlMatch").Visible = False
                End If
                Return
            End If

            If e.CommandName = "SingleClick" Then
                Dim row As HtmlTableRow = e.Item.FindControl("TransRow")
                If Not row Is Nothing Then
                    row.Attributes.Clear()
                End If
                If Not e.Item.FindControl("pnlTrnsDetail") Is Nothing Then
                    e.Item.FindControl("pnlTrnsDetail").Visible = False
                End If

                If Not e.Item.FindControl("pnlMatch") Is Nothing Then
                    e.Item.FindControl("pnlMatch").Visible = True
                End If
                Return

            End If


        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)

        End Try
    End Sub


    Protected Sub RepTransDetails_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles RepTransDetails.ItemDataBound
        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
            Dim _singleClickButton As LinkButton = CType(e.Item.FindControl("LinkButton1"), LinkButton)
            Dim _jsSingle As String = ClientScript.GetPostBackClientHyperlink(_singleClickButton, "")
            _jsSingle = _jsSingle.Insert(11, "setTimeout(""")
            _jsSingle = (_jsSingle + """, 300)")
            Dim row As HtmlTableRow = e.Item.FindControl("TransRow")
            If Not row Is Nothing Then
                row.Attributes("onclick") = _jsSingle
            End If

            Dim bFlag1 As Button = CType(e.Item.FindControl("btnSaveMatch"), Button)
            Dim sArgument As String = CType(e.Item.DataItem, DataRowView).Row.Item("numTransactionId").ToString

            If Not IsNothing(bFlag1) Then
                bFlag1.Attributes.Remove("onclick")
                setFlagCommands(bFlag1, "btnSaveEvent", sArgument)
            End If

            Dim bFlag2 As Button = CType(e.Item.FindControl("btnClose"), Button)
            Dim sArgument2 As String = CType(e.Item.DataItem, DataRowView).Row.Item("numTransactionId").ToString

            If Not IsNothing(bFlag2) Then
                bFlag2.Attributes.Remove("onclick")
                setFlagCommands(bFlag2, "btnCloseEvent", sArgument2)
            End If

        End If

    End Sub

    Private Sub setFlagCommands(ByVal bFlag As Button, ByVal sCommand As String, ByVal sArgument As String)
        bFlag.CommandName = sCommand
        bFlag.CommandArgument = sArgument
    End Sub

    Private Sub AddSingleClickEvent(ByVal e As RepeaterCommandEventArgs)
        Try
            If Not e.Item.FindControl("LinkButton1") Is Nothing Then
                Dim _singleClickButton As LinkButton = CType(e.Item.FindControl("LinkButton1"), LinkButton)
                Dim _jsSingle As String = ClientScript.GetPostBackClientHyperlink(_singleClickButton, "")
                _jsSingle = _jsSingle.Insert(11, "setTimeout(""")
                _jsSingle = (_jsSingle + """, 300)")
                Dim row As HtmlTableRow = e.Item.FindControl("TransRow")
                If Not row Is Nothing Then
                    row.Attributes("onclick") = _jsSingle
                End If

            End If

        Catch ex As Exception
            Throw ex

        End Try
    End Sub
    Sub BindDropDowns()
        Try
            sb_FillCOAFromDBSel(ddlCOAccounts)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Sub sb_FillCOAFromDBSel(ByRef objCombo As DropDownList)
        Try
            Dim objEBanking As New BACRM.BusinessLogic.Accounting.EBanking

            objEBanking.DomainID = Session("DomainID")
            objEBanking.UserContactID = Session("UserContactId")
            objCombo.DataSource = objEBanking.GetConnectedCOABankAccounts()
            objCombo.DataTextField = "vcAccountName"
            objCombo.DataValueField = "numAccountId"
            objCombo.Attributes("OptionGroup") = "Connected Bank Accounts"
            objCombo.DataBind()
            objCombo.Items.Insert(0, "--Select One--")
            objCombo.Items.FindByText("--Select One--").Value = "0"

        Catch ex As Exception
            Throw ex
        End Try

    End Sub



End Class