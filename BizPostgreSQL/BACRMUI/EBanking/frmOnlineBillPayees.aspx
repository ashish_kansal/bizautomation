﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmOnlineBillPayees.aspx.vb"
    MasterPageFile="~/common/GridMasterRegular.Master" Inherits=".frmPayBill" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Untitled Page</title>
        <style type="text/css">
        .info
        {
            color: #00529B;
            background-color: #BDE5F8;
            background-image: url('../images/info.png');
            border: 1px solid;
            margin: 10px 5px 0px 5px;
            padding: 15px 10px 15px 50px;
            background-repeat: no-repeat;
            background-position: 10px center;
        }
    </style>
    <script language="javascript" type="text/javascript">
        function ShowMessage(Message) {
            alert(Message);
        }

        $(document).ready(function () {

            $("#dvAddPayee").hide();

            $("#lnkAddPayee").click(function () {

                $("#dvAddPayee").slideToggle("slow");
            });

            $("#btnGetPayees").click(function () {

                $(this).val('Downloading..');
            });
        })

        function OpenPayBill(PayeeDetailId) {
            window.open("../EBanking/frmPayBill.aspx?PayeeDetailId=" + PayeeDetailId, "", "width=1100,height=600,status=no,scrollbars=yes,left=155,top=160");
            return false;
        }

        function DeleteRecord() {
            return confirm("Are you sure you want to delete selected record? deleting it will remove payee record from bank as well")
        }

        function Save() {

            if (document.getElementById("txtName").value == "") {
                alert("Enter Payee Name");
                document.getElementById("txtName").focus();
                return false;
            }
            if (document.getElementById("txtAddress1").value == "") {
                alert("Enter Address Line 1");
                document.getElementById("txtAddress1").focus();
                return false;
            }
            if (document.getElementById("txtCity").value == "") {
                alert("Enter City name");
                document.getElementById("txtCity").focus();
                return false;
            }
            if (document.getElementById("txtState").value == "") {
                alert("Enter State Code");
                document.getElementById("txtState").focus();
                return false;
            }
            if (document.getElementById("txtPostalCode").value == "") {
                alert("Enter Postal Code");
                document.getElementById("txtPostalCode").focus();
                return false;
            }
            if (document.getElementById("txtCountry").value == "") {
                alert("Enter Country Code");
                document.getElementById("txtCountry").focus();
                return false;
            }
            if (document.getElementById("txtPhone").value == "") {
                alert("Enter Phone Number");
                document.getElementById("txtPhone").focus();
                return false;
            }

            return true;
        }

        function GetPayees() {

            if (document.getElementById("ddlCOAccounts").value == 0) {
                alert("Select Bank");
                document.getElementById("ddlCOAccounts").focus();
                return false;
            }

            return true;
        }
    </script>
    <style type="text/css">
        .Message
        {
            font-size: xx-small;
            font-weight: bold;
            color: gray;
        }
    </style>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server" ClientIDMode="Static">
    Online Bill Payment
</asp:Content>
<asp:Content ID="Contant7" ContentPlaceHolderID="FiltersAndViews" runat="server" ClientIDMode="Static">
    <div>
        <table align="right">
            <tr>
                <td><a href="javascript:void(0);" id="lnkAddPayee">Add Payee</a></td>
                <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
            </tr>

        </table>
    </div>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="GridPlaceHolder" runat="server">
    <ul id="messagebox" class="errorInfo" style="display: none">
    </ul>
     <table id="Table4" height="10" cellspacing="0" cellpadding="0" border="0" width="100%">
        <tr align="center" class="normal1">
            <td class="normal4" valign="middle">
                <asp:Literal ID="litMessage" runat="server" EnableViewState="False"></asp:Literal>
            </td>
        </tr>
    </table>

    <div>
        <table cellspacing="2" cellpadding="0" border="0" align="center" width="100%">
            <tr align="left">
                <td>
                    <table>
                        <tr>
                            <td style="font-weight: bold;">Bank Account :
                            </td>
                            <td align="left">
                                <asp:DropDownList ID="ddlCOAccounts" runat="server" CssClass="signup" Width="180px" AutoPostBack="true">
                                </asp:DropDownList>
                            </td>
                            <td>
                                <asp:Button ID="btnGetPayees" runat="server" Text="Download Payees" CssClass="button"></asp:Button>
                                <asp:HiddenField runat="server" ID="hdnFldCOAccountId" />
                                <asp:Button ID="btnHiddenEvent" runat="server" Style="visibility: hidden"></asp:Button>


                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                       <asp:Panel runat="server" Visible="false" ID="pnlNoBankAccounts" CssClass="info">
        <b>No Bank Accounts Linked</b><br />
      No bank accounts found!., To add one go to Chart Of Accounts and click on 'link your bank account'
    </asp:Panel>

                </td>
            </tr>
        </table>
    </div>
    <div>


        <div id="dvAddPayee" runat="server">
            <fieldset>
                <legend>Add New Payee
                </legend>
                <table>

                    <tr>
                        <td align="right">
                            <asp:Label runat="server" ID="lblName" Text="Name <font color='#ff0000'>*</font>"></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="txtName" CssClass="signup" Width="100px"></asp:TextBox>
                        </td>
                        <td align="left" class="Message">Check will be made on entered Name</td>
                    </tr>
                    <tr>
                        <td align="right">
                            <asp:Label runat="server" ID="lblAccount" Text="Account "></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="txtAccount" CssClass="signup" Width="100px"></asp:TextBox>
                        </td>
                        <td align="left" class="Message">if Payee Account is not provided, Check will be sent to payee's address</td>
                    </tr>

                    <tr>
                        <td align="right">
                            <asp:Label runat="server" ID="lblAddress1" Text="Address 1 <font color='#ff0000'>*</font>"></asp:Label></td>
                        <td colspan="2">
                            <asp:TextBox runat="server" ID="txtAddress1" CssClass="signup" Width="300px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            <asp:Label runat="server" ID="lblAddress2" Text="Address 2 "></asp:Label></td>
                        <td colspan="2">
                            <asp:TextBox runat="server" ID="txtAddress2" CssClass="signup" Width="300px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            <asp:Label runat="server" ID="lblAddress3" Text="Address 3"></asp:Label></td>
                        <td colspan="2">
                            <asp:TextBox runat="server" ID="txtAddress3" CssClass="signup" Width="300px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            <asp:Label runat="server" ID="lblCity" Text="City <font color='#ff0000'>*</font>" ></asp:Label>
                        </td>
                        <td colspan="2">
                            <table cellpadding="0" cellspacing="0">
                                <tr>
                                    <td align="right">
                                        <asp:TextBox runat="server" ID="txtCity" CssClass="signup" Width="100px"></asp:TextBox>

                                    </td>
                                    <td align="left">
                                        <asp:Label runat="server" ID="lblState" Text="State <font color='#ff0000'>*</font>"></asp:Label>
                                    </td>
                                    <td align="right">
                                        <asp:TextBox runat="server" ID="txtState" CssClass="signup" Width="20px"></asp:TextBox>

                                    </td>
                                    <td align="left">
                                        <asp:Label runat="server" ID="lblPostalCode" Text="Postal Code <font color='#ff0000'>*</font>"></asp:Label>

                                    </td>
                                    <td align="right">
                                        <asp:TextBox runat="server" ID="txtPostalCode" CssClass="signup" Width="50px"></asp:TextBox>

                                    </td>
                                </tr>
                            </table>

                        </td>
                    </tr>

                    <tr>
                        <td align="right">
                            <asp:Label runat="server" ID="lblCountry" Text="Country <font color='#ff0000'>*</font>"></asp:Label></td>
                        <td>
                            <asp:TextBox runat="server" ID="txtCountry" CssClass="signup" Width="100px"></asp:TextBox>
                            <%--<asp:DropDownList ID="dldCountry" runat="server" CssClass="signup">
     </asp:DropDownList>--%>
                        </td>
                    </tr>

                    <tr>
                        <td align="right">
                            <asp:Label runat="server" ID="lblPhone" Text="Phone <font color='#ff0000'>*</font>"></asp:Label></td>
                        <td>
                            <asp:TextBox runat="server" ID="txtPhone" CssClass="signup" Width="100px"></asp:TextBox>
                        </td>
                    </tr>

                    <tr>
                        <td></td>
                        <td>
                            <asp:Button runat="server" ID="btnSavePayee" Text="Save" CssClass="button" />
                        </td>
                        <td align="left" class="Message">Please note that majority of banks takes 1-2 working days to approve payee before you can initiate first payment. </td>
                    </tr>

                </table>
            </fieldset>
        </div>
        <asp:GridView ID="gvPayeeList" runat="server" BorderWidth="0" CssClass="dgNHover"
            AutoGenerateColumns="False" Style="margin-right: 0px" Width="100%" ClientIDMode="AutoID">
            <AlternatingRowStyle CssClass="ais" />
            <RowStyle CssClass="is" Height="40px" />
            <HeaderStyle CssClass="hs" />
            <Columns>
                <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="" Visible="false">
                    <ItemTemplate>
                        <asp:Label ID="lblID" runat="server" Text='<%# Eval("numPayeeDetailId") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="" Visible="false">
                    <ItemTemplate>
                        <asp:Label ID="lblFIId" runat="server" Text='<%# Eval("vcPayeeFIId") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="" Visible="false">
                    <ItemTemplate>
                        <asp:Label ID="lblFIListId" runat="server" Text='<%# Eval("vcPayeeFIListID") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderStyle-VerticalAlign="Middle" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle">
                    <ItemTemplate>
                        <asp:Button runat="server" ID="btnDeletePayee" Text="X" CssClass="button Delete" CommandArgument='<%#Eval("vcPayeeFIListID") %>' ToolTip="Delete Payee" OnClientClick="return DeleteRecord()" />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Name">
                    <ItemTemplate>
                        <asp:Label ID="lblPayeeName" runat="server" Text='<%# Eval("vcPayeeName") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Account">
                    <ItemTemplate>
                        <asp:Label ID="lblAccount" runat="server" Text='<%# Eval("vcAccount") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Address">
                    <ItemTemplate>
                        <asp:Label ID="lblAddress1" runat="server" Text='<%# Eval("vcAddress1") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="City">
                    <ItemTemplate>
                        <asp:Label ID="lblCity" runat="server" Text='<%# Eval("vcCity") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="State">
                    <ItemTemplate>
                        <asp:Label ID="lblState" runat="server" Text='<%# Eval("vcState") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="" Visible="false">
                    <ItemTemplate>
                        <asp:Label ID="lblPostalCode" runat="server" Text='<%# Eval("vcPostalCode") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="Country">
                    <ItemTemplate>
                        <asp:Label ID="lblCountry" runat="server" Text='<%# Eval("vcCountry") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Phone">
                    <ItemTemplate>
                        <asp:Label ID="lblPhone" runat="server" Text='<%# Eval("vcPhone") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Pay Bill" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle">
                    <ItemTemplate>
                        <asp:Button runat="server" ID="btnPayBill" Text="Pay Bill" CssClass="button" CommandArgument='<%#Eval("vcPayeeFIListID") %>' />
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
    </div>


</asp:Content>
