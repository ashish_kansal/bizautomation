﻿Imports BACRM.BusinessLogic.Accounting
Imports BACRM.BusinessLogic.Common
Imports System.Text


Public Class MatchingTransLookup
    Inherits BACRMPage

    Public TransactionID As Long
    Dim objEBanking As BACRM.BusinessLogic.Accounting.EBanking
    Dim dtAcceptedTrans As DataTable
    Dim dtMatchedTrans As DataTable
    Dim dAmount As Decimal

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then
                If GetQueryStringVal("ReferenceTypeId") <> "" Then
                    hdnReferenceTypeID.Value = GetQueryStringVal("ReferenceTypeId")

                End If

                If GetQueryStringVal("TransactionID") <> "" Then
                    Dim builder As New StringBuilder
                    hdnTransactionID.Value = GetQueryStringVal("TransactionID")
                    objEBanking = New BACRM.BusinessLogic.Accounting.EBanking
                    objEBanking.TransactionID = CCommon.ToLong(hdnTransactionID.Value)
                    Dim dtTransactionDetail As DataTable = objEBanking.GetTransactionDetails()

                    If (dtTransactionDetail.Rows.Count > 0) Then
                        Dim dtDatePosted As DateTime = Convert.ToDateTime(dtTransactionDetail.Rows(0)("dtDatePosted"))
                        builder.Append(CCommon.ToString(dtTransactionDetail.Rows(0)("vcPayeeName")))
                        builder.Append(" on <b> ")
                        builder.Append(dtDatePosted.ToString("MMM dd"))
                        builder.Append(" </b>  for <b> $")
                        'builder.Append(CCommon.ToString(Math.Abs(CCommon.ToDecimal(dtTransactionDetail.Rows(0)("monAmount")))))
                        builder.Append(CCommon.ToString(Math.Round(Math.Abs(CCommon.ToDecimal(dtTransactionDetail.Rows(0)("monAmount"))), 2)))
                        builder.Append(" </b>")
                        lblTransInfo.Text = builder.ToString
                        'lblTransInfo.Text = CCommon.ToString(dtTransactionDetail.Rows(0)("vcPayeeName")) + " on " + dtDatePosted.ToString("MMM dd") + " for " + CCommon.ToString(Math.Abs(CCommon.ToDecimal(dtTransactionDetail.Rows(0)("monAmount"))))
                        hdnTransAmount.Value = CCommon.ToString(Math.Round(Math.Abs(CCommon.ToDecimal(dtTransactionDetail.Rows(0)("monAmount"))), 2))
                        lblDownTransAmount.Text = ReturnMoney(CCommon.ToString(Math.Round(Math.Abs(CCommon.ToDecimal(dtTransactionDetail.Rows(0)("monAmount"))), 2)))

                        calFromDate.SelectedDate = dtDatePosted.AddMonths(-1)
                        calToDate.SelectedDate = dtDatePosted.AddMonths(1)
                        GetMatchedTransactions()
                    End If

                    btnSave.Enabled = False
                    dAmount = Math.Abs(dAmount)
                    lblTotAmount.Text = ReturnMoney(CCommon.ToString(dAmount))

                End If

            Else
                If Not IsNothing(ViewState("dtAcceptedTrans")) Then
                    dtAcceptedTrans = CType(ViewState("dtAcceptedTrans"), DataTable)
                End If

                If Not IsNothing(ViewState("dtMatchedTrans")) Then
                    dtMatchedTrans = CType(ViewState("dtMatchedTrans"), DataTable)
                End If

                UpdateTotalAmount()

            End If
           
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)

        End Try

    End Sub

    Private Sub btnApply_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnApply.Click
        Try
            GetMatchedTransactions()

        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)

        End Try

    End Sub

    Private Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Try
            Response.Redirect("../Accounting/frmDownloadedTransactions.aspx")

        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)

        End Try

    End Sub

    Private Sub GetMatchedTransactions()
        Try

            Dim dRow As DataRow
            Dim objMakeDeposit As New MakeDeposit
            Dim objChecks As New Checks

            dtAcceptedTrans = Nothing
            dtMatchedTrans = Nothing
            ViewState("dtMatchedTrans") = Nothing
            ViewState("dtAcceptedTrans") = Nothing
            CreateMatchedTransDataTable()
            CreateTransDataTable()

            gvMatches.DataSource = dtMatchedTrans
            gvMatches.DataBind()
            gvAccepted.DataSource = dtAcceptedTrans
            gvAccepted.DataBind()
            Dim strMessage As String

            If Not CCommon.ToLong(hdnReferenceTypeID.Value) = 0 Then

                Select Case (CType(CCommon.ToLong(hdnReferenceTypeID.Value), enmReferenceType))
                    Case enmReferenceType.CheckHeader

                        objChecks.DomainID = CCommon.ToLong(Session("DomainID"))
                        objChecks.dtFromDate = CCommon.ToSqlDate(calFromDate.SelectedDate)
                        objChecks.dtToDate = CCommon.ToSqlDate(calToDate.SelectedDate)
                        objChecks.numAmount = CCommon.ToDecimal(hdnTransAmount.Value)
                        Dim dtMatchingCheckDetails As DataTable = objChecks.GetMatchingCheckDetails()

                        If dtMatchingCheckDetails.Rows.Count > 0 Then

                            For Each dr As DataRow In dtMatchingCheckDetails.Rows
                                dRow = dtMatchedTrans.NewRow
                                dRow("ID") = dr("numCheckDetailID")
                                dRow("ReferenceID") = dr("numCheckHeaderID")
                                dRow("dtPosted") = dr("dtCheckDate")
                                dRow("vcDescription") = dr("vcCompanyname")
                                dRow("monAmount") = CCommon.ToString(Math.Round(CCommon.ToDecimal(dr("monAmount")), 2))
                                dtMatchedTrans.Rows.Add(dRow)

                            Next

                        Else
                            strMessage = "There aren\'t any transactions to match."
                            'ClientScript.RegisterStartupScript(Page.GetType, "alertUser", "alert('" & strMessage & "');", True)

                        End If

                    Case enmReferenceType.DepositHeader
                        objMakeDeposit.DomainID = CCommon.ToLong(Session("DomainID"))
                        objMakeDeposit.dtFromDate = CCommon.ToSqlDate(calFromDate.SelectedDate)
                        objMakeDeposit.dtToDate = CCommon.ToSqlDate(calToDate.SelectedDate)
                        objMakeDeposit.numAmount = CCommon.ToDecimal(hdnTransAmount.Value)
                        Dim dtMatchingDeposits As DataTable = objMakeDeposit.GetMatchingDeposits()

                        If dtMatchingDeposits.Rows.Count > 0 Then

                            For Each dr As DataRow In dtMatchingDeposits.Rows
                                dRow = dtMatchedTrans.NewRow
                                dRow("ID") = dr("numDepositeDetailID")
                                dRow("ReferenceID") = dr("numDepositId")
                                dRow("dtPosted") = dr("dtDepositdate")
                                dRow("vcDescription") = dr("vcCompanyname")
                                dRow("monAmount") = CCommon.ToString(Math.Round(CCommon.ToDecimal(dr("monAmountPaid")), 2))
                                dtMatchedTrans.Rows.Add(dRow)

                            Next

                        Else
                            strMessage = "There aren\'t any transactions to match."
                            'ClientScript.RegisterStartupScript(Me.GetType(), "alertUser1", "alert('" & strMessage & "');", True)

                        End If

                End Select

            End If

            gvMatches.DataSource = dtMatchedTrans
            gvMatches.DataBind()
            ViewState.Add("dtMatchedTrans", dtMatchedTrans)

        Catch ex As Exception
            Throw ex

        End Try


    End Sub

    Private Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        Try
            Dim dRow As DataRow
            Dim ID As Long

            CreateTransDataTable()

            If Not IsNothing(ViewState("dAmount")) Then
                dAmount = CCommon.ToDecimal(ViewState("dAmount"))
            End If

            For Each gvRow As GridViewRow In gvMatches.Rows
                Dim chkSelect As CheckBox = CType(gvRow.FindControl("chkSelect"), CheckBox)
                ID = CCommon.ToLong(CType(gvRow.FindControl("lblID"), Label).Text)
                Dim dtClone As DataTable = dtMatchedTrans.Copy
                Dim rowcount As Integer = dtMatchedTrans.Rows.Count
                Dim dr As DataRow

                If chkSelect.Checked = True Then
                    For i As Integer = 0 To rowcount - 1
                        dr = dtClone.Rows(i)
                        If (CCommon.ToLong(dr("ID")) = ID) Then
                            dRow = dtAcceptedTrans.NewRow
                            dRow("ID") = dr("ID")
                            dRow("ReferenceID") = dr("ReferenceID")
                            dRow("dtPosted") = dr("dtPosted")
                            dRow("vcDescription") = dr("vcDescription")
                            dRow("monAmount") = dr("monAmount")
                            dtAcceptedTrans.Rows.Add(dRow)
                            dAmount += CCommon.ToDecimal(dRow("monAmount"))
                            dtMatchedTrans.Rows.RemoveAt(i)
                        End If
                    Next
                End If

            Next

            If dAmount > CCommon.ToDecimal(hdnTransAmount.Value) Or dAmount < CCommon.ToDecimal(hdnTransAmount.Value) Then
                btnSave.Enabled = False
            Else
                btnSave.Enabled = True
            End If

            lblTotAmount.Text = ReturnMoney(CCommon.ToString(dAmount))
            ViewState.Add("dtMatchedTrans", dtMatchedTrans)
            ViewState.Add("dtAcceptedTrans", dtAcceptedTrans)
            ViewState.Add("dAmount", dAmount)

            gvAccepted.DataSource = dtAcceptedTrans
            gvAccepted.DataBind()
            gvMatches.DataSource = dtMatchedTrans
            gvMatches.DataBind()

        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)

        End Try

    End Sub

    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            If Not IsNothing(ViewState("dtAcceptedTrans")) Then
                dtAcceptedTrans = CType(ViewState("dtAcceptedTrans"), DataTable)
            End If

            If Not CCommon.ToLong(hdnReferenceTypeID.Value) = 0 Then

                For Each dr As DataRow In dtAcceptedTrans.Rows

                    If Not CCommon.ToLong(hdnTransactionID.Value) = 0 Then
                        objEBanking = New BACRM.BusinessLogic.Accounting.EBanking
                        objEBanking.TransMappingId = 0
                        objEBanking.TransactionID = CCommon.ToLong(hdnTransactionID.Value)
                        objEBanking.ReferenceType = CCommon.ToLong(hdnReferenceTypeID.Value)
                        objEBanking.ReferenceID = CCommon.ToLong(dr("ReferenceID"))
                        objEBanking.IsActive = 1
                        objEBanking.MapBankStatementTransactions()
                    End If
                Next


            End If

            dtAcceptedTrans.Rows.Clear()
            ViewState("dtAcceptedTrans") = Nothing
            Response.Redirect("../Accounting/frmDownloadedTransactions.aspx", False)

        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)

        End Try

    End Sub


    Private Sub CreateMatchedTransDataTable()
        Try

            If Not IsNothing(dtMatchedTrans) Then

                If Not IsNothing(ViewState("dtMatchedTrans")) Then
                    dtMatchedTrans = CType(ViewState("dtMatchedTrans"), DataTable)
                End If
            Else

                dtMatchedTrans = New DataTable()
                'If dtMatchedTrans.Columns.Count > 0 Then
                '    dtMatchedTrans.Columns.Clear()
                'End If
                dtMatchedTrans.Columns.Add("ID")
                dtMatchedTrans.Columns.Add("ReferenceID")
                dtMatchedTrans.Columns.Add("dtPosted")
                dtMatchedTrans.Columns.Add("vcDescription")
                dtMatchedTrans.Columns.Add("monAmount")

            End If


        Catch ex As Exception
            Throw ex

        End Try

    End Sub

    Private Sub CreateTransDataTable()
        Try

            If Not IsNothing(dtAcceptedTrans) Then
                If Not IsNothing(ViewState("dtAcceptedTrans")) Then
                    dtAcceptedTrans = CType(ViewState("dtAcceptedTrans"), DataTable)
                End If

            Else
                dtAcceptedTrans = New DataTable()
                'If dtAcceptedTrans.Columns.Count > 0 Then
                '    dtAcceptedTrans.Columns.Clear()
                'End If
                dtAcceptedTrans.Columns.Add("ID")
                dtAcceptedTrans.Columns.Add("ReferenceID")
                dtAcceptedTrans.Columns.Add("dtPosted")
                dtAcceptedTrans.Columns.Add("vcDescription")
                dtAcceptedTrans.Columns.Add("monAmount")

            End If


        Catch ex As Exception
            Throw ex

        End Try

    End Sub

    Private Sub GetMatchingTransaction(ByVal ReferenceTypeID As Integer, ByVal ReferenceNo As Long)
        Try


        Catch ex As Exception
            Throw ex

        End Try

    End Sub

    Protected Sub gvAccepted_RowDataBound(ByVal sender As Object, ByVal e As GridViewRowEventArgs) Handles gvAccepted.RowDataBound
        Try
            If (e.Row.RowType = DataControlRowType.DataRow) Then
                If ((e.Row.RowState = DataControlRowState.Edit) _
                            OrElse (e.Row.RowState = (DataControlRowState.Edit Or DataControlRowState.Alternate))) Then
                    e.Row.Attributes.Clear()
                Else

                End If
                Dim sArgument As String = CType(e.Row.DataItem, DataRowView).Row.Item("ID").ToString

                If Not IsNothing(gvAccepted.HeaderRow) Then

                    Dim btnRemoveAll As Button = CType(gvAccepted.HeaderRow.FindControl("btnRemoveAll"), Button)
                    If Not IsNothing(btnRemoveAll) Then
                        setFlagCommands(btnRemoveAll, "btnRemoveAll", sArgument)

                    End If
                End If

                Dim btnRemove As Button = CType(e.Row.FindControl("btnRemove"), Button)
                If Not IsNothing(btnRemove) Then
                    setFlagCommands(btnRemove, "btnRemove", sArgument)

                End If

            End If


        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)

        End Try

    End Sub

    Protected Sub gvAccepted_RowCommand(ByVal sender As Object, ByVal e As GridViewCommandEventArgs) Handles gvAccepted.RowCommand
        Try
            Dim dRow As DataRow
            Dim gvRow As GridViewRow
            gvRow = CType(CType(e.CommandSource, Control).NamingContainer, GridViewRow)

            ' Get the command name
            Dim _commandName As String = e.CommandName
            Dim _eventArgument As String = Request.Form("__EVENTARGUMENT")
            Select Case (_commandName)
                Case "btnRemove"
                    Dim ID As String = 0
                    ID = CCommon.ToLong(CType(gvRow.FindControl("lblID"), Label).Text)

                    Dim dtClone As DataTable = dtAcceptedTrans.Copy
                    Dim rowcount As Integer = dtAcceptedTrans.Rows.Count
                    Dim dr As DataRow

                    For i As Integer = 0 To rowcount - 1
                        dr = dtClone.Rows(i)
                        If (CCommon.ToLong(dr("ID")) = ID) Then
                            dRow = dtMatchedTrans.NewRow
                            dRow("ID") = dr("ID")
                            dRow("dtPosted") = dr("dtPosted")
                            dRow("vcDescription") = dr("vcDescription")
                            dRow("ReferenceID") = dr("ReferenceID")
                            dRow("monAmount") = dr("monAmount")
                            dtMatchedTrans.Rows.Add(dRow)
                            dtAcceptedTrans.Rows.RemoveAt(i)

                            dAmount -= CCommon.ToDecimal(dr("monAmount"))
                        End If
                    Next
                    ViewState.Add("dAmount", dAmount)
                    UpdateTotalAmount()
                    ViewState.Add("dtMatchedTrans", dtMatchedTrans)
                    ViewState.Add("dtAcceptedTrans", dtAcceptedTrans)

                    gvAccepted.DataSource = dtAcceptedTrans
                    gvAccepted.DataBind()
                    gvMatches.DataSource = dtMatchedTrans
                    gvMatches.DataBind()

                Case "btnRemoveAll"
                    Dim dtClone As DataTable = dtAcceptedTrans.Copy
                    Dim rowcount As Integer = dtAcceptedTrans.Rows.Count
                    Dim dr As DataRow

                    For i As Integer = 0 To rowcount - 1
                        dr = dtClone.Rows(i)
                        dRow = dtMatchedTrans.NewRow
                        dRow("ID") = dr("ID")
                        dRow("dtPosted") = dr("dtPosted")
                        dRow("ReferenceID") = dr("ReferenceID")
                        dRow("vcDescription") = dr("vcDescription")
                        dRow("monAmount") = dr("monAmount")
                        dtMatchedTrans.Rows.Add(dRow)
                        'dtAcceptedTrans.Rows.RemoveAt(i)
                    Next
                    dAmount = 0
                    dtAcceptedTrans.Rows.Clear()
                    ViewState.Add("dAmount", dAmount)
                    UpdateTotalAmount()
                    ViewState.Add("dtMatchedTrans", dtMatchedTrans)
                    ViewState.Add("dtAcceptedTrans", dtAcceptedTrans)

                    gvAccepted.DataSource = dtAcceptedTrans
                    gvAccepted.DataBind()
                    gvMatches.DataSource = dtMatchedTrans
                    gvMatches.DataBind()


            End Select

        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)

        End Try

    End Sub

    Private Sub setFlagCommands(ByVal bFlag As Button, ByVal sCommand As String, ByVal sArgument As String)
        Try
            bFlag.CommandName = sCommand
            bFlag.CommandArgument = sArgument

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub gvMatches_RowDeleted(sender As Object, e As System.Web.UI.WebControls.GridViewDeletedEventArgs) Handles gvMatches.RowDeleted
        Try


        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)

        End Try

    End Sub

    Public Function ReturnMoney(ByVal Money As Object) As String
        Try
            If Not IsDBNull(Money) Then Return String.Format("{0:###0.00}", Money)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Sub UpdateTotalAmount()

        If Not IsNothing(ViewState("dAmount")) Then
            dAmount = CCommon.ToDecimal(ViewState("dAmount"))
        End If
        lblTotAmount.Text = ReturnMoney(CCommon.ToString(dAmount))
        If dAmount > CCommon.ToDecimal(hdnTransAmount.Value) Or dAmount < CCommon.ToDecimal(hdnTransAmount.Value) Then
            btnSave.Enabled = False
        Else
            btnSave.Enabled = True
        End If

    End Sub

End Class