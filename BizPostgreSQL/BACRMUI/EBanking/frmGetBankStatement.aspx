﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmGetBankStatement.aspx.vb"
    MasterPageFile="~/common/GridMasterRegular.Master" Inherits=".frmGetBankStatement" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Untitled Page</title>
    <script language="javascript" type="text/javascript">

        function OpenSetting(URL) {
            window.open(URL, "", "width=800,height=400,status=no,scrollbars=yes,left=155,top=160");
            return false;
        }

        function Validate() {
            if (document.getElementById("ddlAccounts").value == 0) {
                alert("Select an Account");
                document.getElementById("ddlAccounts").focus();
                return false;
            }
            return true;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server"
    ClientIDMode="Static">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server" ClientIDMode="Static">
    Get Bank Statement
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="GridPlaceHolder" runat="server">
    <ul id="messagebox" class="errorInfo" style="display: none">
    </ul>
    <div style="width: 70%; margin-left: auto; margin-right: auto;">
        <asp:Label ID="lblError" runat="server" ForeColor="Crimson" Visible="False">Errors</asp:Label>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="form-inline">
                <div class="form-group">
                    <label>Accounts:</label>
                    <asp:DropDownList ID="ddlAccounts" runat="server" CssClass="form-control">
                    </asp:DropDownList>
                </div>
                <asp:Button ID="btnConnect" runat="server" CssClass="btn btn-primary" Text="Connect" />
                <asp:Button ID="btnClose" runat="server" CssClass="btn btn-default" Text="Close" />
            </div>
        </div>
    </div>
    <div>
        <asp:DataGrid ID="dgTransactions" AutoGenerateColumns="false" runat="server" CssClass="dg"
            Width="100%">
            <AlternatingItemStyle CssClass="ais" />
            <ItemStyle CssClass="is" />
            <HeaderStyle CssClass="hs" />
            <Columns>

                <asp:BoundColumn DataField="FITransactionID" HeaderText="Transaction ID" />
                <asp:BoundColumn DataField="PayeeName" HeaderText="Payee Name" />
                <asp:BoundColumn DataField="Amount" HeaderText="Amount" />
                <asp:BoundColumn DataField="TxType" HeaderText="Transaction Type" />
                <asp:BoundColumn DataField="CheckNumber" HeaderText="Check Number" />
                <asp:BoundColumn DataField="DtDatePosted" HeaderText="Date Posted" />
                <asp:BoundColumn DataField="Memo" HeaderText="Memo" />
            </Columns>
        </asp:DataGrid>
    </div>
    <div>
        <asp:Table ID="tblDetails" runat="server" Width="664px" Height="80px"></asp:Table>
    </div>

</asp:Content>
