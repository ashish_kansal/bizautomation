﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmBankAccounts.aspx.vb" MasterPageFile="~/common/GridMasterRegular.Master" Inherits=".frmBankAccounts" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Untitled Page</title>
     <style type="text/css">
        .info
        {
            color: #00529B;
            background-color: #BDE5F8;
            background-image: url('../images/info.png');
            border: 1px solid;
            margin: 10px 5px 0px 5px;
            padding: 15px 10px 15px 50px;
            background-repeat: no-repeat;
            background-position: 10px center;
        }
    </style>
    <script language="javascript" type="text/javascript">


        function ShowMessage() {
            alert('We are connecting to your Account.. Please visit again in few Minutes To see your Current Balance.');
        }
        function ShowMessage1(message) {
            alert("Enters");
            alert(message);
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server" ClientIDMode="Static">
    
    <asp:Button ID="btnClose" runat="server" CssClass="button" Text="Close"  style="float:right" />
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server" ClientIDMode="Static">
    Bank Accounts 
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="GridPlaceHolder" runat="server">
    <ul id="messagebox" class="errorInfo" style="display: none">
    </ul>
    <asp:UpdatePanel runat="server" ID="UpdatePanel1">

        <ContentTemplate>
            <div style="vertical-align: middle; font-size: small; width: 70%; margin-left: auto; margin-right: auto;">
                <asp:Label ID="lblError" runat="server" ForeColor="Crimson" Visible="false">Errors</asp:Label>
                <br />
            </div>
            <asp:Repeater ID="RepDetails" runat="server">
                <HeaderTemplate>
                    <table>
                </HeaderTemplate>
                <ItemTemplate>
                    <tr>
                        <td>
                            <table style="background-color: #848CAC; width: 700px; height: 35px;">
                                <tr>
                                    <td style="font-size: 15px; font-weight: bold;" align="left">
                                        <asp:Label ForeColor="White" Font-Size="Large" runat="server" ID="lblBankName" Text='<%#Eval("vcFIName") %>'></asp:Label>
                                    </td>

                                     <td align="right">
                                        <asp:LinkButton ForeColor="White" Font-Size="Small" ID="lnkDownloadedTrans" CommandName="lnkDownloadedTrans" CommandArgument='<%#Eval("numAccountID") %>' runat="server" Text="View Downloaded Transactions" />
                                    </td>

                                    <td align="right">
                                        <asp:Button ID="btnRefreshBalance" CommandName="btnEvent" CommandArgument='<%#Eval("numBankDetailID") %>' runat="server" CssClass="button" Text="Refresh" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>


                    <asp:UpdatePanel runat="server" ID="uppnlAccounts">
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="btnRefreshBalance" EventName="Click" />
                        </Triggers>
                        <ContentTemplate>

                            <tr style="background-color: #EBEFF0;">

                                <td colspan="2">
                                    <table style="background-color: #EBEFF0; width: 700px">
                                        <tr>
                                            <td colspan="3">
                                                <div style="vertical-align: middle; font-family: @Adobe Song Std L; font-size: 12px; width: 70%; margin-left: auto; margin-right: auto;">
                                                    <asp:Label ID="lblAccountMessage" runat="server" ForeColor="Crimson" Visible="false">Errors</asp:Label>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="font-size: 13px;" align="left">Account Number
                                                <asp:Label runat="server" ID="lblhdrAccountName"></asp:Label></td>
                                            <td style="font-size: 13px;" align="left">Ledger Balance
                                                <asp:Label runat="server" ID="lblhdrLedgerBalance"></asp:Label></td>
                                            <td style="font-size: 13px;" align="left">Account Balance
                                                <asp:Label runat="server" ID="lblhdrAccountBalance"></asp:Label></td>
                                        </tr>
                                        <tr>
                                            <td style="font-weight: bold; font-size: 15px;" align="left">
                                                <asp:Label runat="server" ID="lblvalAccountName" Text='<%#Eval("vcAccountNumber") %>'></asp:Label></td>
                                            <td style="font-weight: bold; font-size: 15px;" align="left">
                                                <asp:Label runat="server" ID="lblvalLedgerBalance" Text='<%# ReturnMoney(DataBinder.Eval(Container,"DataItem.monLedgerBalance")) %>'></asp:Label></td>
                                            <td style="font-weight: bold; font-size: 15px;" align="left">
                                                <asp:Label runat="server" ID="lblvalAccountBalance" Text='<%# ReturnMoney(DataBinder.Eval(Container,"DataItem.monAvailableBalance")) %>'></asp:Label></td>
                                        </tr>
                                        <tr>
                                            <td colspan="3">
                                                <table width="100%">
                                                    <tr>
                                                        <td>
                                                            <div style="width: 70%; margin-left: auto; margin-right: auto;">
                                                                <div style="font-size: 12px;">Ledger Balance Last Updated on : </div>
                                                                <div style="font-size: 13px; font-weight: bold;">
                                                                    <asp:Label runat="server" ID="lblLedgerBalanceDate" Text='<%#Eval("dtLedgerBalanceDate") %>'></asp:Label>
                                                                </div>
                                                            </div>
                                                        </td>

                                                        <td>
                                                            <div style="width: 70%; margin-left: auto; margin-right: auto;">
                                                                <div style="font-size: 12px;">Account Balance Last Updated on :</div>
                                                                <div style="font-size: 13px; font-weight: bold;">
                                                                    <asp:Label runat="server" ID="lblAvailableBalanceDate" Text='<%#Eval("dtAvailableBalanceDate") %>'></asp:Label>
                                                                </div>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>

                                        </tr>

                                    </table>
                                </td>
                            </tr>

                            <tr style="height: 50px;">
                                <td></td>
                            </tr>

                        </ContentTemplate>
                    </asp:UpdatePanel>



                </ItemTemplate>
                <FooterTemplate>
                    </table>
                </FooterTemplate>
            </asp:Repeater>
        </ContentTemplate>

    </asp:UpdatePanel>


    <div>
        <asp:Table ID="tblDetails" runat="server" Width="664px" Height="80px"></asp:Table>
    </div>
    
     <asp:Panel runat="server" Visible="false" ID="pnlNoBankAccounts" CssClass="info">
        <b>No Bank Accounts Linked</b><br />
      No bank accounts found!., To add one go to Chart Of Accounts and click on 'link your bank account'
    </asp:Panel>
    
     
</asp:Content>
