﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmFindMatchingTransaction.aspx.vb"
    MasterPageFile="~/common/GridMasterRegular.Master" Inherits=".MatchingTransLookup" %>

<%@ Register TagPrefix="BizCalendar" TagName="Calendar" Src="../include/calandar.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Match Transactions</title>
    <script language="javascript" type="text/javascript">
        function ShowMessage(Message) {
            alert(Message);
        }

        function CancelMatchingTransaction() {
            window.location.href = "../Accounting/frmDownloadedTransactions.aspx";
            return false;
        }
    </script>
    <style type="text/css">
        .fixpanel
        {
            /*width: auto;
            overflow: hidden;
            display: inline-block;
            white-space: nowrap;
            height: auto;*/
             border-top: 1px solid #CBCBCB;
            border-left: 1px solid #CBCBCB;
            border-right: 1px solid #CBCBCB;
            border-bottom: 1px solid #CBCBCB;
        }

        .displaynone
        {
            display: none;
        }

        .header
        {
            background: #E5E5E5!important;
            border-bottom: 1px solid #CBCBCB;
            border-top: 1px solid #CBCBCB;
            border-right: 1px solid #CBCBCB;
            border-left: 1px solid #CBCBCB;
            font-weight: bold;
            line-height: 30px;
        }



        .fixpanel .is, .ais
        {
            /*background: #E5E5E5!important;*/
            border-bottom: 1px solid #CBCBCB;
            border-top: 1px solid #CBCBCB;
            border-right: 1px solid #fff;
            border-left: 1px solid #fff;
            /*font-weight: bold;*/
            padding: 0 0 0 10px;
            /*line-height: 50px;*/
        }

            .fixpanel .is td:first-child, .ais td:first-child
            {
                border-left: 1px solid #CBCBCB;
            }

            .fixpanel .is td:last-child, .ais td:last-child
            {
                border-right: 1px solid #CBCBCB;
            }

        #tblAssign .NA td:first-child, td:first-child, td:last-child, td:last-child
        {
            border-width: 0 0 0 0;
        }
    </style>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server" ClientIDMode="Static">
    Match Transactions
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="GridPlaceHolder" runat="server" ClientIDMode="Static">
    <ul id="messagebox" class="errorInfo" style="display: none">
    </ul>
    <div>
        Downloaded Transaction :
        <asp:Label runat="server" ID="lblTransInfo"></asp:Label>
    </div>
    <div>
        <table>
            <tr>
                <td>
                    <div>
                        <div>
                            <asp:HiddenField ID="hdnTransactionID" runat="server" Value="0" />
                            <asp:HiddenField ID="hdnTransAmount" runat="server" Value="0" />
                            <asp:HiddenField ID="hdnReferenceTypeID" runat="server" Value="0" />

                        </div>

                        <table id="tblMain">
                            <tr>
                                <td>
                                    <div style="height: 300px; overflow: hidden;">

                                        <table class="">
                                            <tr>
                                                <td>

                                                    <table id="AvailMatches" class="fixpanel"  border="0" cellpadding="0" cellspacing="0">
                                                        <tr class="header">
                                                            <td>Available Matches from&nbsp;
                                                            </td>
                                                            <td>
                                                                <BizCalendar:Calendar ID="calFromDate" runat="server" ClientIDMode="AutoID" />
                                                            </td>
                                                            <td>&nbsp;to
                                                            </td>
                                                            <td>
                                                                <BizCalendar:Calendar ID="calToDate" runat="server" ClientIDMode="AutoID" />
                                                            </td>
                                                            <td>&nbsp;<asp:Button ID="btnApply" runat="server" Text="Apply" CssClass="button" />

                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="5" width="400">
                                                                <div style="height: 250px; overflow: auto;">
                                                                    <asp:Panel ID="pnlGridView" runat="server" ScrollBars="Auto">

                                                                        <asp:GridView ID="gvMatches" runat="server" BorderWidth="0" CssClass="fixpanel" AutoGenerateColumns="False"
                                                                            Style="margin-right: 0px" Width="100%" ClientIDMode="Static" ShowHeaderWhenEmpty="true" >
                                                                            <AlternatingRowStyle CssClass="ais" />
                                                                            <RowStyle CssClass="is" Height="40px" />
                                                                            <HeaderStyle CssClass="header" />
                                                                            <Columns>
                                                                                <asp:TemplateField ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5%">
                                                                                    <HeaderTemplate>
                                                                                        <asp:CheckBox ID="SelectAllCheckBox" runat="server" onclick="SelectAll('SelectAllCheckBox', 'chkSelected');" />
                                                                                    </HeaderTemplate>
                                                                                    <ItemTemplate>
                                                                                        <asp:CheckBox ID="chkSelect" CssClass="chkSelected" runat="server" />
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:ButtonField Text="SingleClick" CommandName="SingleClick" Visible="false" />
                                                                                <asp:ButtonField Text="DoubleClick" CommandName="DoubleClick" Visible="false" />
                                                                                <asp:BoundField DataField="dtPosted" HeaderText="Date" />
                                                                                <asp:BoundField DataField="vcDescription" HeaderText="Description" />
                                                                                <%--<asp:BoundField DataField="monAmount" HeaderText="Amount"  />--%>

                                                                                <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="" Visible="false">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="lblID" runat="server" Text='<%# Eval("ID") %>'></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="" Visible="false">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="lblDatePosted" EnableViewState="false" runat="server" Text='<%# Eval("dtPosted") %>'></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:TemplateField HeaderText="" Visible="false">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="lblDescription" EnableViewState="false" runat="server" Text='<%# Eval("vcDescription") %>'></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:TemplateField HeaderText=""  ItemStyle-HorizontalAlign="Right">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="lblAmount" EnableViewState="false" runat="server" Text='<%# ReturnMoney(Eval("monAmount"))%>'></asp:Label>
                                                                                        
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                            </Columns>
                                                                            <EmptyDataTemplate>
                                                                                There aren't any transactions to match.
                                                                            </EmptyDataTemplate>
                                                                        </asp:GridView>
                                                                    </asp:Panel>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </table>

                                                </td>
                                                <td rowspan="2" valign="middle" width="100" align="center">
                                                    <div>
                                                        <asp:Button runat="server" ID="btnAdd" Text="Add" CssClass="button" />
                                                    </div>
                                                </td>
                                                <td>

                                                    <table id="sleMatches" class="fixpanel" cellpadding="0" cellspacing="0">
                                                        <tr class="header">
                                                            <td>Selected Matches
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td width="400">
                                                                <div style="height: 250px; overflow: auto;">
                                                                    <asp:Panel ID="Panel1" runat="server" ScrollBars="Auto">

                                                                        <asp:GridView ID="gvAccepted" runat="server" BorderWidth="0" CssClass="fixpanel" AutoGenerateColumns="False"
                                                                            Style="margin-right: 0px" Width="100%" ClientIDMode="AutoID" ShowHeaderWhenEmpty="true">
                                                                            <AlternatingRowStyle CssClass="ais" />
                                                                            <RowStyle CssClass="is" Height="40px" />
                                                                            <HeaderStyle CssClass="header" />
                                                                            <Columns>
                                                                                <asp:TemplateField ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5%">
                                                                                    <HeaderTemplate>
                                                                                        <%--<asp:Button ID="btnRemoveAll" runat="server" Text="X" CssClass="Delete" />--%>
                                                                                    </HeaderTemplate>
                                                                                    <ItemTemplate>
                                                                                        <asp:Button ID="btnRemove" runat="server" Text="x" CssClass="button Delete" />
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="" Visible="false">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="lblID" runat="server" Text='<%# Eval("ID") %>'></asp:Label>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:ButtonField Text="SingleClick" CommandName="SingleClick" Visible="false" />
                                                                                <asp:ButtonField Text="DoubleClick" CommandName="DoubleClick" Visible="false" />

                                                                                <asp:BoundField DataField="dtPosted" HeaderText="Date" />
                                                                                <asp:BoundField DataField="vcDescription" HeaderText="Description" />
                                                                                <asp:BoundField DataField="monAmount" HeaderText="Amount"  ItemStyle-HorizontalAlign="Right"/>



                                                                            </Columns>
                                                                        </asp:GridView>
                                                                    </asp:Panel>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </table>



                                                </td>
                                            </tr>
                                        </table>

                                    </div>
                                </td>

                            </tr>
                            <tr>
                                <td>
                                    <table id="baseline" width="100%">
                                        <tr align="left">
                                            <td align="right">Total Amount :
            <asp:Label ID="lblTotAmount" runat="server" Width="100px" Text="0.00"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right">Downloaded Transaction Amount :
            <asp:Label ID="lblDownTransAmount" runat="server" Width="100px"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right">
                                                <table id="tblSaveOrCancel">
                                                    <tr>
                                                        <td>
                                                            <asp:Button runat="server" ID="btnSave" Text="Save" />
                                                        </td>
                                                        <td>
                                                            <asp:Button runat="server" ID="btnCancel" Text="Cancel"  OnClientClick="CancelMatchingTransaction();return false;"/>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </div>

                </td>
            </tr>

        </table>

    </div>

    <div>
        <asp:Table ID="tblDetails" runat="server" Width="664px" Height="80px">
        </asp:Table>
    </div>
</asp:Content>
