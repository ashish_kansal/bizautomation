﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="~/EBanking/frmBankConnectWizard.aspx"
    MasterPageFile="~/common/DetailPage.Master" Inherits=".frmBankConnectWizard" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        $(document).ready(function () {
            InitializeValidation();
        });
        function OpenSetting(URL) {
            window.open(URL, "", "width=800,height=400,status=no,scrollbars=yes,left=155,top=160");
            return false;
        }

        function SelectSingleRadiobutton(rdBtnID) {
            //process the radio button Being checked.
            var rduser = $(document.getElementById(rdBtnID));
            rduser.checked = true;
            // process all other radio buttons (excluding the the radio button Being checked).
            var list = rduser.closest('table').find("INPUT[type='radio']").not(rduser);
            list.prop('checked', false);
        }

        function ValidateAccountSelection() {
            var counter = 0;
            var obj = document.getElementById("<%=dgBankAccounts.ClientID%>").getElementsByTagName("input");
            for (i = 0; i <= obj.length - 1; i++) {
                if (obj[i].type == "radio" && obj[i].checked == true) {
                    counter++;
                }
            }
            if (counter == 0) {
                alert('Please Select an Account');
                return false
            }
            return true;
        }


        function SetBankDetailId(BankDetailID) {
            //alert(BankDetailID);
            window.opener.document.getElementById('hdnBankDetailID').value = BankDetailID;
            window.opener.document.getElementById('hdnIsConnected').value = true;
            window.opener.document.getElementById('btnHiddenSave').click();
            self.close();
            //alert(window.opener.document.getElementById('hdnBankDetailID').value);
        }
        function Save() {
            if (document.getElementById("ddlFinancialInst").value == 0) {
                alert("Select Bank");
                document.getElementById("ddlFinancialInst").focus();
                return false;
            }

            if (document.getElementById("txtOFXUserName").value == "") {
                alert("Enter User Name");
                document.getElementById("txtOFXUserName").focus();
                return false;
            }
            if (document.getElementById("txtOFXPassword").value == "") {
                alert("Enter User password");
                document.getElementById("txtOFXPassword").focus();
                return false;
            }
            return true;
        }


    </script>
</asp:Content>
<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="RecordActionPanel">
    <div class="row padbottom10">
        <div class="col-xs-12">
            <div class="pull-right">
                <asp:HyperLink ID="hrfBnkStatement" CssClass="btn btn-primary" runat="server" NavigateUrl="~/EBanking/frmGetBankStatement.aspx">Get Bank Statement</asp:HyperLink>

                <asp:HyperLink ID="hrfAccounts" CssClass="btn btn-primary" runat="server" NavigateUrl="~/EBanking/frmBankAccounts.aspx">Accounts</asp:HyperLink>

                <asp:HyperLink ID="hrfTransactionDetail" CssClass="btn btn-primary" runat="server" NavigateUrl="~/EBanking/frmTransactionDetails.aspx?TransactionID=638">Transaction Detail</asp:HyperLink>

                <asp:HyperLink ID="hrfTransMapping" CssClass="btn btn-primary" runat="server" NavigateUrl="~/EBanking/frmTransactionMapping.aspx">Transaction Mapping</asp:HyperLink>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" runat="server" ContentPlaceHolderID="DetailPageTitle">
    E-Banking Integrator
</asp:Content>
<asp:Content ID="Content4" runat="server" ContentPlaceHolderID="TabsPlaceHolder">
    <ul id="messagebox" class="errorInfo" style="display: none">
    </ul>
    <div>
        <asp:Label ID="lblError" runat="server" ForeColor="Crimson" Visible="False">Errors</asp:Label>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="form-inline">
                <div class="form-group">
                    <label>Financial Institution:</label>
                    <asp:DropDownList ID="ddlFinancialInst" runat="server" CssClass="form-control">
                    </asp:DropDownList>
                    <asp:HiddenField runat="server" ID="hdnAccountID" />
                </div>
                &nbsp;&nbsp;&nbsp;&nbsp;
                <div class="form-group">
                    <label>User Name:</label>
                    <asp:TextBox ID="txtOFXUserName" runat="server" CssClass="form-control" autocomplete="OFF"></asp:TextBox>
                </div>
                &nbsp;&nbsp;&nbsp;&nbsp;
                <div class="form-group">
                    <label>Password:</label>
                    <asp:TextBox ID="txtOFXPassword" CssClass="form-control" TextMode="Password" runat="server"></asp:TextBox>
                </div>
                <asp:Button ID="btnConnect" runat="server" CssClass="btn btn-primary" Text="Connect" />
            </div>
        </div>
    </div>

    <asp:Panel ID="pnllstAccounts" runat="server" Visible="false">
        <div>
            <asp:DataGrid ID="dgBankAccounts" AutoGenerateColumns="false" runat="server" CssClass="dg"
                Width="100%" ClientIDMode="AutoID">
                <AlternatingItemStyle CssClass="ais" />
                <ItemStyle CssClass="is" />
                <HeaderStyle CssClass="hs" />
                <Columns>
                    <asp:TemplateColumn>
                        <HeaderStyle Width="25px"></HeaderStyle>
                        <ItemTemplate>
                            <asp:RadioButton runat="server" ID="rdbSelected" OnClick="javascript:SelectSingleRadiobutton(this.id)" />
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:BoundColumn DataField="AccountNo" HeaderText="Account Number" />
                    <asp:BoundColumn DataField="AccType" HeaderText="Account Type" />
                    <asp:BoundColumn DataField="BankId" HeaderText="BankId" Visible="false" />
                    <asp:BoundColumn DataField="Description" HeaderText="Description" Visible="false" />
                    <asp:BoundColumn DataField="Status" HeaderText="Status" Visible="false" />
                    <asp:BoundColumn DataField="IsTransferSource" HeaderText="Is TransferSource" Visible="false" />
                    <asp:BoundColumn DataField="IsTransferDestination" HeaderText="Is TransferDestination" Visible="false" />
                    <asp:TemplateColumn HeaderText="Enabled" Visible="false">
                        <HeaderStyle Width="25px"></HeaderStyle>
                        <ItemTemplate>
                            <asp:Label runat="server" ID="lblIsConnected" Text='<%#Eval("IsConnected") %>' HeaderText="IsConnected"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateColumn>


                </Columns>
            </asp:DataGrid>
        </div>
        <div>
            <table>
                <tr>
                    <td>
                        <asp:Button ID="btnSaveBankDetails" runat="server" CssClass="button" Text="Save & Close" />
                    </td>
                    <td>
                        <asp:HiddenField runat="server" ID="hdnFldBankDetailID" />
                    </td>
                </tr>
            </table>
        </div>
    </asp:Panel>

    <div>
        <table>
            <tr>
                <td>
                    <asp:HiddenField runat="server" ID="hdnFldBankMasterId" />
                </td>
                <td>
                    <asp:HiddenField runat="server" ID="hdnFldUserName" />
                </td>
                <td>
                    <asp:HiddenField runat="server" ID="hdnFldPassword" />
                </td>
            </tr>
        </table>
    </div>
    <div>
        <asp:Table ID="tblDetails" runat="server" Width="664px" Height="80px">
        </asp:Table>
    </div>
</asp:Content>
