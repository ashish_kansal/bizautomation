﻿
Imports nsoftware.InEBank
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Accounting

Partial Public Class frmBankConnectWizard
    Inherits BACRMPage

#Region "Members"

    Dim Bankstatement1 As Bankstatement
    Dim objCcStatement1 As Ccstatement
    Dim dtBankDetail As DataTable

#End Region

#Region "Events"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            'CLEAR ERROR ON RELOAD
            DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ""
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "none")

            If Not IsPostBack Then
                If GetQueryStringVal("AccountID") <> "" Then
                    hdnAccountID.Value = GetQueryStringVal("AccountID")
                End If

                BindDropDowns()
                txtOFXUserName.Text = ""
                txtOFXPassword.Text = ""
                btnSaveBankDetails.Attributes.Add("onclick", "javascript:return ValidateAccountSelection();")
                btnConnect.Attributes.Add("onclick", "javascript:return Save();")

            End If

        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))

        End Try

    End Sub

    Private Sub btnConnect_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnConnect.Click
        Try
            Dim objCCommon As New CCommon
            Dim objEBanking As New EBanking
            Dim dtExistingAccounts As DataTable
            Dim dtBankMasterDetails As DataTable
            Dim objAccount As New Account
            Dim strMessage As String = ""
            hdnFldUserName.Value = objCCommon.Encrypt(txtOFXUserName.Text.Trim)
            hdnFldPassword.Value = objCCommon.Encrypt(txtOFXPassword.Text.Trim)
            hdnFldBankMasterId.Value = ddlFinancialInst.SelectedItem.Value
            objEBanking.TintMode = 1
            objEBanking.BankMasterID = ddlFinancialInst.SelectedItem.Value
            objEBanking.UserName = hdnFldUserName.Value
            objEBanking.Password = hdnFldPassword.Value
            objEBanking.DomainID = Session("DomainID")
            objEBanking.UserContactID = Session("UserContactId")
            dtExistingAccounts = objEBanking.GetBankDetailsList()
            dtBankMasterDetails = objEBanking.GetBankMaster()

            If dtBankMasterDetails.Rows.Count > 0 Then
                objAccount.FIUrl = CCommon.ToString(dtBankMasterDetails.Rows(0)("vcFIOFXServerUrl"))
                objAccount.FIOrganization = dtBankMasterDetails.Rows(0)("vcFIOrganization")
                objAccount.FIId = dtBankMasterDetails.Rows(0)("vcFIId")
                objAccount.OFXAppId = "QWIN"
                objAccount.OFXAppVersion = "1800"
                objAccount.OFXUser = txtOFXUserName.Text.Trim
                objAccount.OFXPassword = txtOFXPassword.Text.Trim

                objAccount.GetInfo()
                CreateBankDetailTable()
                Dim dr As DataRow
                For i As Integer = 0 To objAccount.Accounts.Count - 1
                    dr = dtBankDetail.NewRow()
                    dr("Checked") = "false"
                    dr("AccountNo") = objAccount.Accounts(i).Id.Trim
                    dr("AccType") = objAccount.Accounts(i).AccType.Trim
                    dr("BankId") = objAccount.Accounts(i).BankId.Trim
                    dr("Description") = objAccount.Accounts(i).Description.Trim
                    dr("Status") = objAccount.Accounts(i).Status.Trim
                    dr("IsTransferSource") = CCommon.ToString(objAccount.Accounts(i).IsTransferSource)
                    dr("IsTransferDestination") = CCommon.ToString(objAccount.Accounts(i).IsTransferDestination)
                    dr("IsConnected") = "0"
                    dtBankDetail.Rows.Add(dr)
                Next
                For Each drBankAccount As DataRow In dtBankDetail.Rows
                    For Each drExistingAccount As DataRow In dtExistingAccounts.Rows
                        If drBankAccount("AccountNo") = drExistingAccount("vcAccountNumber") AndAlso objEBanking.GetAccountTypeID(CCommon.ToString(drBankAccount("AccType"))) = drExistingAccount("vcAccountType").ToString.ToUpper Then
                            drBankAccount("IsConnected") = "1"
                        End If
                    Next

                Next

                If dtBankDetail.Rows.Count > 0 Then
                    pnllstAccounts.Visible = True
                    dgBankAccounts.DataSource = dtBankDetail
                    dgBankAccounts.DataBind()
                Else
                    strMessage = "Sorry!., No Accounts found for the Details provided"
                    'ScriptManager.RegisterStartupScript(Page, Page.GetType, "alertUser", "alert('" & strMessage & "');", True)
                    ClientScript.RegisterClientScriptBlock(Page.GetType, "alertUser", "alert('" & strMessage & "');", True)
                End If

            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))

        End Try

    End Sub

    Private Sub dgBankAccounts_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgBankAccounts.ItemDataBound
        Select Case e.Item.ItemType

            Case ListItemType.AlternatingItem, ListItemType.Item, ListItemType.EditItem

                If CType(e.Item.FindControl("lblIsConnected"), Label).Text = "1" Then
                    e.Item.Enabled = False
                End If


        End Select
    End Sub


    Private Sub btnSaveBankDetails_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSaveBankDetails.Click
        Dim item As DataGridItem
        Dim objEBanking As New EBanking
        For Each item In dgBankAccounts.Items
            If CType(item.FindControl("rdbSelected"), RadioButton).Checked Then
                objEBanking.BankDetailID = 0
                objEBanking.BankMasterID = CCommon.ToLong(hdnFldBankMasterId.Value)
                objEBanking.DomainID = CCommon.ToLong(Session("DomainID"))
                objEBanking.UserContactID = CCommon.ToLong(Session("UserContactID"))
                objEBanking.AccountID = CCommon.ToLong(hdnAccountID.Value)
                objEBanking.AccountNumber = item.Cells(1).Text
                objEBanking.AccountType = objEBanking.GetAccountTypeID(item.Cells(2).Text)
                'If objEBanking.AccountType = "4" Then
                '    objEBanking.BankID = ""
                'Else
                '    objEBanking.BankID = item.Cells(3).Text.Trim
                'End If
                objEBanking.BankID = item.Cells(3).Text.Trim
                objEBanking.UserName = hdnFldUserName.Value
                objEBanking.Password = hdnFldPassword.Value
                objEBanking.IsActive = 1
                Dim BankDetailID As Long = objEBanking.ManageBankDetails()
                hdnFldBankDetailID.Value = BankDetailID

                ClientScript.RegisterClientScriptBlock(Page.GetType, "SetbankID", "SetBankDetailId(" & BankDetailID & ");", True)
            End If
        Next
    End Sub

#End Region

#Region "Methods"

    Public Sub CreateBankDetailTable()
        Try
            dtBankDetail = New DataTable
            If dtBankDetail.Columns.Count > 0 Then
                dtBankDetail.Columns.Clear()
            End If
            dtBankDetail.Columns.Add("Checked")
            dtBankDetail.Columns.Add("AccountNo")
            dtBankDetail.Columns.Add("AccType")
            dtBankDetail.Columns.Add("BankId")
            dtBankDetail.Columns.Add("Description")
            dtBankDetail.Columns.Add("Status")
            dtBankDetail.Columns.Add("IsTransferSource")
            dtBankDetail.Columns.Add("IsTransferDestination")
            dtBankDetail.Columns.Add("IsConnected")

        Catch ex As Exception
            Throw ex

        End Try
    End Sub

    Sub BindDropDowns()
        Try
            sb_FillFinancialInstFromDBSel(ddlFinancialInst)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Sub sb_FillFinancialInstFromDBSel(ByRef objCombo As DropDownList)

        Try
            Dim objEbanking As New EBanking
            objCombo.DataSource = objEbanking.GetBankMasterList()
            objCombo.DataTextField = "vcFIName"
            objCombo.DataValueField = "numBankMasterID"
            objCombo.DataBind()
            objCombo.Items.Insert(0, "--Select One--")
            objCombo.Items.FindByText("--Select One--").Value = "0"

        Catch ex As Exception
            Throw ex
        End Try

    End Sub

    Public Sub sb_Fill_COA_AccountTypes(ByRef objCombo As DropDownList)

        Try
            Dim objCOA = New ChartOfAccounting
            objCOA.DomainId = Session("DomainID")
            objCOA.UserCntId = Session("UserContactId")
            objCOA.GetAccountTypes()
            objCombo.DataSource = objCOA.GetAccountTypes()
            objCombo.DataTextField = "vcAccountType"
            objCombo.DataValueField = "numAccountTypeID"
            objCombo.DataBind()
            objCombo.Items.Insert(0, "--Select One--")
            objCombo.Items.FindByText("--Select One--").Value = "0"

        Catch ex As Exception
            Throw ex

        End Try

    End Sub

    Sub Clear()
        Try

        Catch ex As Exception
            Throw ex

        End Try

    End Sub

    Private Sub DisplayError(ByVal exception As String)
        Try
            DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = exception
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Focus()
        Catch ex As Exception

        End Try
    End Sub
#End Region

End Class