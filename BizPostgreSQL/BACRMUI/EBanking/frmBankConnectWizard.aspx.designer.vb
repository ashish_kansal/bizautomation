﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class frmBankConnectWizard

    '''<summary>
    '''hrfBnkStatement control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hrfBnkStatement As Global.System.Web.UI.WebControls.HyperLink

    '''<summary>
    '''hrfAccounts control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hrfAccounts As Global.System.Web.UI.WebControls.HyperLink

    '''<summary>
    '''hrfTransactionDetail control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hrfTransactionDetail As Global.System.Web.UI.WebControls.HyperLink

    '''<summary>
    '''hrfTransMapping control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hrfTransMapping As Global.System.Web.UI.WebControls.HyperLink

    '''<summary>
    '''lblError control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblError As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''ddlFinancialInst control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlFinancialInst As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''hdnAccountID control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hdnAccountID As Global.System.Web.UI.WebControls.HiddenField

    '''<summary>
    '''txtOFXUserName control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtOFXUserName As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtOFXPassword control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtOFXPassword As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''btnConnect control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnConnect As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''pnllstAccounts control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnllstAccounts As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''dgBankAccounts control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents dgBankAccounts As Global.System.Web.UI.WebControls.DataGrid

    '''<summary>
    '''btnSaveBankDetails control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnSaveBankDetails As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''hdnFldBankDetailID control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hdnFldBankDetailID As Global.System.Web.UI.WebControls.HiddenField

    '''<summary>
    '''hdnFldBankMasterId control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hdnFldBankMasterId As Global.System.Web.UI.WebControls.HiddenField

    '''<summary>
    '''hdnFldUserName control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hdnFldUserName As Global.System.Web.UI.WebControls.HiddenField

    '''<summary>
    '''hdnFldPassword control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hdnFldPassword As Global.System.Web.UI.WebControls.HiddenField

    '''<summary>
    '''tblDetails control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents tblDetails As Global.System.Web.UI.WebControls.Table
End Class
