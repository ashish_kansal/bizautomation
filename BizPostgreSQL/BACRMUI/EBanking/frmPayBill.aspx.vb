﻿
Imports nsoftware.InEBank
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Accounting

Public Class frmPayBill1
    Inherits BACRMPage

    Dim BankDetailId As Long
    Dim PayeeDetailId As Long
    Dim WithEvents objBillpayment As New nsoftware.InEBank.Billpayment

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then

                Dim objEBanking As BACRM.BusinessLogic.Accounting.EBanking
                Dim dtPayeeDetail As DataTable

                If GetQueryStringVal("PayeeDetailId") <> "" Then
                    PayeeDetailId = GetQueryStringVal("PayeeDetailId")
                End If
                If PayeeDetailId > 0 Then
                    objEBanking = New BACRM.BusinessLogic.Accounting.EBanking
                    objEBanking.PayeeDetailId = PayeeDetailId
                    dtPayeeDetail = objEBanking.GetOnlineBillPayeeDetail()
                    hdnFldPayeeDetailId.Value = CCommon.ToString(PayeeDetailId)
                    If dtPayeeDetail.Rows.Count > 0 Then
                        FillPayeeDetails(dtPayeeDetail.Rows(0))
                        btnPayBill.Enabled = True

                    End If

                End If
                btnPayBill.Attributes.Add("onclick", "javascript:return PayBill();")

            End If

        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)

        End Try
    End Sub

    Private Sub FillPayeeDetails(ByVal drPayee As DataRow)
        Try
            lblName.Text = CCommon.ToString(drPayee("vcPayeeName"))
            lblAccount.Text = CCommon.ToString(drPayee("vcAccount"))
            lblAddressline1.Text = CCommon.ToString(drPayee("vcAddress1")) & ", " & CCommon.ToString(drPayee("vcAddress2")) & ", " & CCommon.ToString(drPayee("vcAddress3"))
            lblAddressline2.Text = CCommon.ToString(drPayee("vcCity")) & ", " & CCommon.ToString(drPayee("vcState")) & ", PIN : " & CCommon.ToString(drPayee("vcPostalCode"))
            lblAddressline3.Text = CCommon.ToString(drPayee("vcCountry")) & ", Phone : " & CCommon.ToString(drPayee("vcPhone"))
            hdnFldBankDetailId.Value = CCommon.ToString(drPayee("numBankDetailId"))

        Catch ex As Exception
            Throw ex

        End Try

    End Sub

    Private Sub btnPayBill_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPayBill.Click

        Dim objEBanking As BACRM.BusinessLogic.Accounting.EBanking
        Dim dt As DataTable
        Dim dtPayee As DataTable
        Try
            If CCommon.ToDecimal(txtAmount.Text.Trim) > 0 Then
                objEBanking = New BACRM.BusinessLogic.Accounting.EBanking
                objEBanking.TintMode = 0
                objEBanking.BankDetailID = CCommon.ToLong(hdnFldBankDetailId.Value)
                objEBanking.PayeeDetailId = CCommon.ToLong(hdnFldPayeeDetailId.Value)

                objEBanking.Amount = CCommon.ToDecimal(txtAmount.Text.Trim)
                objEBanking.Memo = txtMemo.Text.Trim
                objEBanking.DtPaymentDueDate = calDueDate.SelectedDate
                objEBanking.PayBill()

                'dt = objEBanking.GetBankDetails()

                'If dt.Rows.Count > 0 Then
                '    Dim dRow As DataRow = dt.Rows(0)
                '    objBillpayment.OFXAppId = "QWIN"
                '    objBillpayment.OFXAppVersion = "1700"
                '    objBillpayment.FIUrl = dRow("vcFIOFXServerUrl").ToString
                '    objBillpayment.FIId = dRow("vcFIId").ToString
                '    objBillpayment.FIOrganization = dRow("vcFIOrganization").ToString
                '    objBillpayment.OFXUser = objCommon.Decrypt(dRow("vcUserName").ToString)
                '    objBillpayment.OFXPassword = objCommon.Decrypt(dRow("vcPassword").ToString)
                '    objBillpayment.Payment.FromBankId = dRow("vcBankID").ToString
                '    objBillpayment.Payment.FromAccountId = dRow("vcAccountNumber").ToString
                '    Dim AccountTyp As AccountTypes = DirectCast(System.[Enum].Parse(GetType(AccountTypes), dRow("vcAccountType").ToString), AccountTypes)
                '    objBillpayment.Payment.FromAccountType = AccountTyp
                '    objBillpayment.Payment.Amount = CCommon.ToDecimal(txtAmount.Text.Trim)
                '    objBillpayment.Payment.Memo = txtMemo.Text.Trim
                '    objBillpayment.Payment.DateDue = CCommon.ToString(DateTime.Now.AddDays(7))

                '    dtPayee = objEBanking.GetOnlineBillPayeeDetail()

                '    If dtPayee.Rows.Count > 0 Then
                '        Dim dRowPayee As DataRow = dtPayee.Rows(0)
                '        objBillpayment.Payee.Name = dRowPayee("vcPayeeName").ToString
                '        objBillpayment.Payee.Account = dRowPayee("vcAccount").ToString
                '        objBillpayment.Payee.ListId = dRowPayee("vcPayeeFIListID").ToString
                '        objBillpayment.Payee.Addr1 = dRowPayee("vcAddress1").ToString
                '        objBillpayment.Payee.Addr2 = dRowPayee("vcAddress2").ToString
                '        objBillpayment.Payee.Addr3 = dRowPayee("vcAddress3").ToString
                '        objBillpayment.Payee.City = dRowPayee("vcCity").ToString
                '        objBillpayment.Payee.State = dRowPayee("vcState").ToString
                '        objBillpayment.Payee.Country = dRowPayee("vcCountry").ToString
                '        objBillpayment.Payee.PostalCode = dRowPayee("vcPostalCode").ToString
                '        objBillpayment.Payee.Phone = dRowPayee("vcPhone").ToString
                '        objBillpayment.PayBill()

                '    End If

                'End If
                Dim strMessage As String = "Pay Bill request forwarded to Bank."
                ScriptManager.RegisterStartupScript(Page, Page.GetType, "alertUser", "alert('" & strMessage & "'); window.close();", True)

            End If

        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)

        End Try

    End Sub



End Class