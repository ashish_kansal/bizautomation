﻿Imports BACRM.BusinessLogic.Accounting
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Projects
Imports BACRM.BusinessLogic.Admin

Imports nsoftware.InEBank
'.BillpaymentSyncPayeesEventArgs


Public Class frmPayBill
    Inherits BACRMPage
    Dim payees As New ArrayList()
    Dim WithEvents Billpayment1 As New nsoftware.InEBank.Billpayment

    Dim dtPayee As DataTable
    Dim BankDetailID As Long

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            litMessage.Text = ""
            If Not IsPostBack Then
                pnlNoBankAccounts.Visible = False
                BindDropDowns()

                PersistTable.Load()
                If PersistTable.Count > 0 Then
                    If Not ddlCOAccounts.Items.FindByValue(CCommon.ToString(PersistTable(ddlCOAccounts.ID))) Is Nothing Then
                        ddlCOAccounts.Items.FindByValue(CCommon.ToString(PersistTable(ddlCOAccounts.ID))).Selected = True

                    End If
                End If
                BindPayeeList()
                'dvAddPayee.Visible = False
                btnSavePayee.Attributes.Add("onclick", "javascript:return Save();")
                btnGetPayees.Attributes.Add("onclick", "javascript:return GetPayees();")

            End If

        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub ddlCOAccounts_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCOAccounts.SelectedIndexChanged
        Try
            'Dim dtTransactions As DataTable
            PersistTable.Clear()
            PersistTable.Add(ddlCOAccounts.ID, ddlCOAccounts.SelectedValue)
            PersistTable.Save()
            BankDetailID = ddlCOAccounts.SelectedItem.Value
            If ddlCOAccounts.SelectedItem.Value > 0 Then
                BindPayeeList()
            Else
                gvPayeeList.DataSource = Nothing
                gvPayeeList.Visible = False

            End If

        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try

    End Sub

    Sub BindDropDowns()
        Try
            sb_FillCOAFromDBSel(ddlCOAccounts)

        Catch ex As Exception
            Throw ex

        End Try

    End Sub

    Public Sub sb_FillCOAFromDBSel(ByRef objCombo As DropDownList)
        Try
            Dim dt As DataTable
            Dim objEBanking As New BACRM.BusinessLogic.Accounting.EBanking

            objEBanking.DomainID = Session("DomainID")
            objEBanking.UserContactID = Session("UserContactId")
            objEBanking.TintMode = 1
            dt = objEBanking.GetConnectedCOABankAccounts()
            If dt.Rows.Count > 0 Then
                objCombo.DataSource = objEBanking.GetConnectedCOABankAccounts()
                objCombo.DataTextField = "vcAccountName"
                objCombo.DataValueField = "numBankDetailID"
                objCombo.Attributes("OptionGroup") = "Connected Bank Accounts"
                objCombo.DataBind()
                objCombo.Items.Insert(0, "--Select One--")
                objCombo.Items.FindByText("--Select One--").Value = "0"

            Else
                pnlNoBankAccounts.Visible = True
                objCombo.Items.Insert(0, "--Select One--")
                objCombo.Items.FindByText("--Select One--").Value = "0"

            End If
           
        Catch ex As Exception
            Throw ex
        End Try

    End Sub

    Private Sub btnGetPayees_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGetPayees.Click

        Dim objCommon As New CCommon
        Dim strResult As String

        Try
            BankDetailID = ddlCOAccounts.SelectedItem.Value
            If BankDetailID > 0 Then
                Dim objEBanking As New BACRM.BusinessLogic.Accounting.EBanking
                objEBanking.TintMode = 0
                objEBanking.BankDetailID = BankDetailID
                objEBanking.DomainID = CCommon.ToLong(Session("DomainID"))
                Dim dt As DataTable = objEBanking.GetBankDetails()
                If dt.Rows.Count > 0 Then
                    Dim dRow As DataRow = dt.Rows(0)
                    Billpayment1.OFXAppId = "QWIN"
                    Billpayment1.OFXAppVersion = "1700"
                    Billpayment1.FIUrl = dRow("vcFIOFXServerUrl").ToString
                    Billpayment1.FIId = dRow("vcFIId").ToString
                    Billpayment1.FIOrganization = dRow("vcFIOrganization").ToString
                    Billpayment1.OFXUser = objCommon.Decrypt(dRow("vcUserName").ToString)
                    Billpayment1.OFXPassword = objCommon.Decrypt(dRow("vcPassword").ToString)
                    Billpayment1.Payment.FromBankId = dRow("vcBankID").ToString
                    Billpayment1.Payment.FromAccountId = dRow("vcAccountNumber").ToString
                    Dim AccountTyp As AccountTypes = DirectCast(System.[Enum].Parse(GetType(AccountTypes), dRow("vcAccountType").ToString), AccountTypes)
                    Billpayment1.Payment.FromAccountType = AccountTyp
                    strResult = Billpayment1.SynchronizePayees("REFRESH")
                    'AddPayeeDetails()
                    BindPayeeList()
                    litMessage.Text = "Payees downloaded successfully"
                Else

                End If

            End If

        Catch ex As InEBankBillpaymentException
            litMessage.Text = ex.Message


        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)

        End Try

    End Sub

    Private Sub Billpayment1_OnSyncPayees(ByVal sender As Object, ByVal e As nsoftware.InEBank.BillpaymentSyncPayeesEventArgs) Handles Billpayment1.OnSyncPayees
        Try
           
            Dim objEBanking As New BACRM.BusinessLogic.Accounting.EBanking
            objEBanking.PayeeDetailId = 0
            objEBanking.PayeeFIId = e.PayeeId
            objEBanking.PayeeFIListID = e.PayeeListId
            objEBanking.PayeeName = e.PayeeName
            objEBanking.Account = e.PayeeAccount
            objEBanking.Address1 = e.PayeeAddr1
            objEBanking.Address2 = e.PayeeAddr2
            objEBanking.Address3 = e.PayeeAddr3
            objEBanking.City = e.PayeeCity
            objEBanking.State = e.PayeeState
            objEBanking.PostalCode = e.PayeePostalCode
            objEBanking.Country = e.PayeeCountry
            objEBanking.PayeePhone = e.PayeePhone
            objEBanking.BankDetailID = BankDetailID
            objEBanking.DomainID = Session("DomainID")
            objEBanking.ManageOnlineBillPayeeDetail()

        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)

        End Try

    End Sub

    Private Sub btnSavePayee_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSavePayee.Click
        Dim BankDetailID As Long
        Dim objCommon As New CCommon
        Dim strResult As String

        Try
            BankDetailID = ddlCOAccounts.SelectedItem.Value
            If BankDetailID > 0 Then
                Dim objEBanking As New BACRM.BusinessLogic.Accounting.EBanking
                objEBanking.TintMode = 0
                objEBanking.BankDetailID = BankDetailID
                objEBanking.DomainID = CCommon.ToLong(Session("DomainID"))
                Dim dt As DataTable = objEBanking.GetBankDetails()
                If dt.Rows.Count > 0 Then
                    Dim dRow As DataRow = dt.Rows(0)
                    Billpayment1.OFXAppId = "QWIN"
                    Billpayment1.OFXAppVersion = "1700"
                    Billpayment1.FIUrl = dRow("vcFIOFXServerUrl").ToString
                    Billpayment1.FIId = dRow("vcFIId").ToString
                    Billpayment1.FIOrganization = dRow("vcFIOrganization").ToString
                    Billpayment1.OFXUser = objCommon.Decrypt(dRow("vcUserName").ToString)
                    Billpayment1.OFXPassword = objCommon.Decrypt(dRow("vcPassword").ToString)
                    Billpayment1.Payment.FromBankId = dRow("vcBankID").ToString
                    Billpayment1.Payment.FromAccountId = dRow("vcAccountNumber").ToString
                    Dim AccountTyp As AccountTypes = DirectCast(System.[Enum].Parse(GetType(AccountTypes), dRow("vcAccountType").ToString), AccountTypes)
                    Billpayment1.Payment.FromAccountType = AccountTyp
                    Billpayment1.Payee.Name = txtName.Text.Trim
                    Billpayment1.Payee.Account = txtAccount.Text.Trim
                    Billpayment1.Payee.Addr1 = txtAddress1.Text.Trim
                    Billpayment1.Payee.Addr2 = txtAddress2.Text.Trim
                    Billpayment1.Payee.Addr3 = txtAddress3.Text.Trim
                    Billpayment1.Payee.City = txtCity.Text.Trim
                    Billpayment1.Payee.State = txtState.Text.Trim
                    Billpayment1.Payee.Country = txtCountry.Text.Trim
                    Billpayment1.Payee.PostalCode = txtPostalCode.Text.Trim
                    Billpayment1.Payee.Phone = txtPhone.Text.Trim
                    Billpayment1.AddPayee()

                End If

            End If

        Catch ex As InEBankBillpaymentException
            litMessage.Text = ex.Message

        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)

        End Try

    End Sub

    Private Sub setFlagCommands(ByVal bFlag As Button, ByVal sCommand As String, ByVal sArgument As String)
        Try
            bFlag.CommandName = sCommand
            bFlag.CommandArgument = sArgument

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub gvPayeeList_RowDataBound(ByVal sender As Object, ByVal e As GridViewRowEventArgs) Handles gvPayeeList.RowDataBound
        Try
            If (e.Row.RowType = DataControlRowType.DataRow) Then

                Dim btnPayBill As Button = CType(e.Row.FindControl("btnPayBill"), Button)
                Dim btnDeletePayee As Button = CType(e.Row.FindControl("btnDeletePayee"), Button)
                Dim PayeeListId As String = CType(e.Row.DataItem, DataRowView).Row.Item("vcPayeeFIListID").ToString
                Dim PayeeDetailId As Long = CCommon.ToLong(CType(e.Row.DataItem, DataRowView).Row.Item("numPayeeDetailId").ToString)

                If Not IsNothing(btnPayBill) Then
                    'setFlagCommands(btnPayBill, "btnPayBill", PayeeListId)

                    Dim PayBill As String
                    PayBill = String.Format("return OpenPayBill('" & PayeeDetailId & "');")
                    btnPayBill.Attributes.Add("onclick", PayBill)

                End If

                If Not IsNothing(btnDeletePayee) Then
                    setFlagCommands(btnDeletePayee, "btnDeletePayee", PayeeListId)
                    
                End If

            End If

        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)

        End Try
    End Sub

    Protected Sub gvPayeeList_RowCommand(ByVal sender As Object, ByVal e As GridViewCommandEventArgs) Handles gvPayeeList.RowCommand

        Try
            Dim _gridView As GridView = CType(sender, GridView)
            _gridView.EditIndex = -1

            ' Get the selected index and the command name
            ' Dim _selectedIndex As Integer = Integer.Parse(e.CommandArgument.ToString)
            Dim _commandName As String = e.CommandName
            Dim _eventArgument As String = Request.Form("__EVENTARGUMENT")
            Select Case (_commandName)

                Case "btnDeletePayee"
                    Dim gvRow As GridViewRow
                    gvRow = CType(CType(e.CommandSource, Control).NamingContainer, GridViewRow)
                    Dim PayeeListId As String = "0"
                    Dim PayeeDetailId As Long
                    PayeeListId = CType(gvRow.FindControl("lblFIListId"), Label).Text
                    PayeeDetailId = CCommon.ToLong(CType(gvRow.FindControl("lblID"), Label).Text)
                    BankDetailID = ddlCOAccounts.SelectedItem.Value

                    If BankDetailID > 0 Then
                        Dim objEBanking As New BACRM.BusinessLogic.Accounting.EBanking
                        objEBanking.TintMode = 0
                        objEBanking.BankDetailID = BankDetailID
                        objEBanking.DomainID = CCommon.ToLong(Session("DomainID"))
                        Dim dt As DataTable = objEBanking.GetBankDetails()
                        If dt.Rows.Count > 0 Then
                            Dim dRow As DataRow = dt.Rows(0)
                            Billpayment1.OFXAppId = "QWIN"
                            Billpayment1.OFXAppVersion = "1700"
                            Billpayment1.FIUrl = dRow("vcFIOFXServerUrl").ToString
                            Billpayment1.FIId = dRow("vcFIId").ToString
                            Billpayment1.FIOrganization = dRow("vcFIOrganization").ToString
                            Billpayment1.OFXUser = objCommon.Decrypt(dRow("vcUserName").ToString)
                            Billpayment1.OFXPassword = objCommon.Decrypt(dRow("vcPassword").ToString)
                            Billpayment1.Payment.FromBankId = dRow("vcBankID").ToString
                            Billpayment1.Payment.FromAccountId = dRow("vcAccountNumber").ToString
                            Dim AccountTyp As AccountTypes = DirectCast(System.[Enum].Parse(GetType(AccountTypes), dRow("vcAccountType").ToString), AccountTypes)
                            Billpayment1.Payment.FromAccountType = AccountTyp
                            Billpayment1.DeletePayee(PayeeListId)
                            objEBanking.PayeeFIListID = CCommon.ToLong(PayeeListId)
                            objEBanking.PayeeDetailId = PayeeDetailId
                            objEBanking.DeleteOnlineBillPayeeDetail()

                        End If

                        dvAddPayee.Visible = False
                    End If

            End Select

        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)

        End Try
    End Sub

    Private Sub BindPayeeList()
        Try
            If ddlCOAccounts.Items.Count > 0 AndAlso ddlCOAccounts.SelectedItem IsNot Nothing Then
                BankDetailID = ddlCOAccounts.SelectedItem.Value
                If BankDetailID > 0 Then
                    Dim objEBanking As New BACRM.BusinessLogic.Accounting.EBanking
                    objEBanking.TintMode = 0
                    objEBanking.BankDetailID = BankDetailID
                    objEBanking.DomainID = CCommon.ToLong(Session("DomainID"))
                    Dim dtPayee As DataTable = objEBanking.GetOnlineBillPayeeList()
                    gvPayeeList.Visible = True
                    gvPayeeList.DataSource = dtPayee
                    gvPayeeList.DataBind()
                End If
            End If

        Catch ex As Exception
            Throw ex

        End Try

    End Sub

End Class