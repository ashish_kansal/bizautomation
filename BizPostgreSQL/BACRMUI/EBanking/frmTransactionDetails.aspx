﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmTransactionDetails.aspx.vb" 
 MasterPageFile="~/common/PopUp.Master" Inherits=".frmTransactionDetails" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Untitled Page</title>
    <script language="javascript" type="text/javascript">

        function OpenSetting(URL) {
            window.open(URL, "", "width=800,height=400,status=no,scrollbars=yes,left=155,top=160");
            return false;
        }

        function Validate() {
            if (document.getElementById("ddlAccounts").value == 0) {
                alert("Select an Account");
                document.getElementById("ddlAccounts").focus();
                return false;
            }
            return true;
        }

        function DeleteDisabled() {
            alert("If you want to delete this transaction, please unmatch it first.");
        }
    </script>
    <style type="text/css">
        .leftHeading
        {
            font-weight: bold;
            width: 130px;
            float: right;
        }
        .leftvalue
        {
            width: 280px;
            float: left;
        }
        .RightHeading
        {
            font-weight: bold;
            width: 130px;
            float: right;
        }
        .Rightvalue
        {
            width: 280px;
            float: left;
        }
        .tbl1
        {
            margin-left:auto;
            margin-right:auto;
            width:90%;
            background-color:#EEECEC;
        }
        .tblrow
        {
           height:21px;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <div class="right-input">
        <div class="input-part">
            <table>
                <tr>
                    <td class="leftBorder" valign="bottom">
                        <a href="#" class="help">&nbsp;</a>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <%--<tr>
            <td align="right">
                <asp:Button ID="btnSave" Text="Save" Width="50" runat="server" CssClass="button" />
            </td>
        </tr>--%>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="PageTitle" runat="server" ClientIDMode="Static">
   Transaction Details
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="Content" runat="server">
    <ul id="messagebox" class="errorInfo" style="display: none">
    </ul>
    <div style="width:70%;margin-left:auto;margin-right:auto;">
            <asp:Label id="lblError" runat="server" ForeColor="Crimson" Visible="False">Errors</asp:Label> <br />
			
       </div>
        <div>
        <table class="tbl1">
        <tr class="tblrow">
        <td align="right" style="font-weight:bold;width:50px;">Transaction Date : </td>
        <td align="left" style="width:280px;"><asp:Label runat="server" ID="lblTransactionDate" Text=""></asp:Label></td>
        <td align="right" style="font-weight:bold;width:130px;">Bank : </td>
        <td align="left" style="width:280px;"><asp:Label runat="server" ID="lblBankName" Text=""></asp:Label></td>

        </tr>
          <tr class="tblrow">
        <td align="right" style="font-weight:bold;width:130px;">Transaction Type : </td>
        <td align="left" style="width:280px;"><asp:Label runat="server" ID="lblTransactionType" Text=""></asp:Label></td>
        <td align="right" style="font-weight:bold;width:130px;">Account Number : </td>
        <td align="left" style="width:280px;"><asp:Label runat="server" ID="lblAccountNo" Text=""></asp:Label></td>

        </tr>

         <tr class="tblrow">
        <td align="right" style="font-weight:bold;width:130px;">Reference # : </td>
        <td align="left" style="width:280px;"><asp:Label runat="server" ID="lblRefernceNo" Text=""></asp:Label></td>
        <td align="right" style="font-weight:bold;width:130px;">Update ID : </td>
        <td align="left" style="width:280px;"><asp:Label runat="server" ID="lblUpdateID" Text=""></asp:Label></td>

        </tr>

          <tr class="tblrow">
        <td align="right" style="font-weight:bold;width:130px;">Payee : </td>
        <td align="left" style="width:280px;"><asp:Label runat="server" ID="lblPayee" Text=""></asp:Label></td>
        <td align="right" style="font-weight:bold;width:130px;">Bank Transaction ID : </td>
        <td align="left" style="width:280px;"><asp:Label runat="server" ID="lblFITransactionID" Text=""></asp:Label></td>

        </tr>

         <tr class="tblrow">
        <td align="right" style="font-weight:bold;width:130px;">Amount : </td>
        <td align="left" style="width:280px;"><asp:Label runat="server" ID="lblAmount" Text=""></asp:Label></td>
        </tr>

        <tr class="tblrow">
        <td align="right" style="font-weight:bold;width:130px;">Info : </td>
        <td align="left" style="width:280px;"><asp:Label runat="server" ID="lblInfo" Text=""></asp:Label></td>
        </tr>

        <tr class="tblrow">
        <td align="right" style="font-weight:bold;width:130px;">Status : </td>
        <td align="left" style="width:280px;"><asp:Label runat="server" ID="lblStatus" Text=""></asp:Label></td>
        </tr>
        
         
        </table>
        <table class="tbl1" style="background-color:transparent;">
         <tr class="tblrow">
        <td style="float:left;"><asp:Button runat="server" ID="btnDelete" Text="Delete" CssClass="button"/></td>
        <td><asp:Label runat="server" ID="Label1" Text=""></asp:Label></td>
        <td></td>
         <td style="float:right;"><asp:Button runat="server" ID="btnClose" Text="Close" CssClass="button" OnClientClick="self.close();" /></td>
        </tr>
        </table>
        </div>


        <div>
        <table>
        <tr>
          <td>
               <asp:HiddenField runat="server" ID="hdnTransactionID" />
          </td>
          <td>
               <asp:HiddenField runat="server" ID="hdnFldAccountID" />
          </td>
            <td>
               <asp:HiddenField runat="server" ID="hdnFldReferenceID" />
          </td>
          </tr>
        </table>
        </div>
         <div>
   
          	<asp:Table id="tblDetails" runat="server" Width="664px" Height="80px"></asp:Table>
           <%-- <cc2:bankstatement id="Bankstatement1" runat="server"></cc2:bankstatement>--%>
       </div>
  
</asp:Content>
