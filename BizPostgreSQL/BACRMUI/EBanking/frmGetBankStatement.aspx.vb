﻿
Imports nsoftware.InEBank
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Accounting


Partial Public Class frmGetBankStatement
    Inherits BACRMPage

#Region "Members"

    Dim dtTransactions As DataTable
    Public Enum AccountTypes
        Checking = 0
        Savings = 1
        MoneyMarket = 2
        LineOfCredit = 3
        CreditCard = 4
    End Enum

#End Region

#Region "Events"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            'CLEAR ERROR ON RELOAD
            DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ""
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "none")

            If Not IsPostBack Then
                BindDropDowns()
                btnConnect.Attributes.Add("onclick", "javascript:return Validate()")
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Private Sub btnConnect_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnConnect.Click
        Dim BankDetailID As Long
        Dim lastStatementDate As New DateTime

        Try
            lblError.Visible = False
            Dim objEBanking As New BACRM.BusinessLogic.Accounting.EBanking
            objEBanking.TintMode = 0
            objEBanking.BankDetailID = ddlAccounts.SelectedItem.Value

            Dim dt As DataTable


            If Not objEBanking.BankDetailID < 0 Then
                dt = objEBanking.GetBankDetails()
                If dt.Rows.Count > 0 Then

                    If Not CCommon.ToString(dt.Rows(0)("dtCreatedDate")) = "" Then
                        lastStatementDate = Convert.ToDateTime(CCommon.ToString(dt.Rows(0)("dtCreatedDate")))
                    End If

                    'If CCommon.ToString(dt.Rows(0)("dtCreatedDate")) = "" Or DateTime.Now.Subtract(lastStatementDate).TotalHours >= 24 Then

                    If CCommon.ToString(dt.Rows(0)("vcAccountType")) = "Credit Card" Then
                        GetCreditCardStatement(dt.Rows(0))
                    Else
                        GetBankStatement(dt.Rows(0))
                    End If
                    'Else
                    '    lblError.Visible = True
                    '    lblError.Text = "Sorry!., The Account Balance is already Uptodate"
                    'End If
                Else
                    lblError.Visible = True
                    lblError.Text = "Sorry!., The Account Does seem having active Connection with Bank"
                End If
            End If

        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))

        End Try

    End Sub

    Private Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Response.Redirect("~/EBanking/frmBankConnectWizard.aspx", False)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

#End Region

#Region "Methods"

    Sub BindDropDowns()
        Try
            sb_FillFinancialInstFromDBSel(ddlAccounts)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Sub sb_FillFinancialInstFromDBSel(ByRef objCombo As DropDownList)
        Try
            Dim objEbanking As New BACRM.BusinessLogic.Accounting.EBanking
            objEbanking.TintMode = 0
            objEbanking.BankMasterID = 0
            objEbanking.UserName = ""
            objEbanking.Password = ""
            objEbanking.DomainID = Session("DomainID")
            objEbanking.UserContactID = Session("UserContactId")
            objCombo.DataSource = objEbanking.GetBankDetailsList()
            objCombo.DataTextField = "vcAccountNumber"
            objCombo.DataValueField = "numBankDetailID"
            objCombo.DataBind()
            objCombo.Items.Insert(0, "--Select One--")
            objCombo.Items.FindByText("--Select One--").Value = "0"

        Catch ex As Exception
            Throw ex
        End Try

    End Sub

    Private Sub GetCreditCardStatement(ByVal dRow As DataRow)
        Try

            Dim objCcStatement1 As New Ccstatement
            'objCcStatement1 = New Ccstatement
            Dim objCommon As New CCommon
            objCcStatement1.OFXAppId = "QWIN"
            objCcStatement1.OFXAppVersion = "1800"
            objCcStatement1.FIUrl = dRow("vcFIOFXServerUrl").ToString
            objCcStatement1.FIId = dRow("vcFIId").ToString
            objCcStatement1.FIOrganization = dRow("vcFIOrganization").ToString
            objCcStatement1.CardNumber = dRow("vcAccountNumber").ToString
            objCcStatement1.OFXUser = dRow("vcUserName").ToString
            objCcStatement1.OFXPassword = dRow("vcPassword").ToString

            If CCommon.ToString(dRow("dtCreatedDate")) <> "" Then
                objCcStatement1.StartDate = CCommon.ToString(dRow("dtCreatedDate"))
            Else
                objCcStatement1.StartDate = DateTime.Now.AddDays(-30).ToString
            End If

            objCcStatement1.EndDate = DateTime.Now.ToString()
            objCcStatement1.GetStatement()
            Dim objEbank As New BACRM.BusinessLogic.Accounting.EBanking
            objEbank.StatementID = 0
            objEbank.BankDetailID = CCommon.ToLong(dRow("numBankDetailID"))
            objEbank.FIStatementID = objCcStatement1.FIId
            objEbank.LedgerBalance = CCommon.ToDecimal(objCcStatement1.LedgerBalance)
            objEbank.DtLedgerBalanceDate = Convert.ToDateTime(objCcStatement1.LedgerBalanceDate)

            objEbank.AvailableBalance = CCommon.ToDecimal(objCcStatement1.AvailableBalance)
            objEbank.DtAvailableBalanceDate = Convert.ToDateTime(objCcStatement1.AvailableBalanceDate)
            objEbank.DtStartDate = Convert.ToDateTime(objCcStatement1.StartDate)
            objEbank.DtEndDate = Convert.ToDateTime(objCcStatement1.EndDate)
            objCommon.DomainID = CCommon.ToLong(dRow("numDomainID"))  'Session("DomainID")
            objCommon.Mode = 15
            objCommon.Str = "USD"
            objEbank.CurrencyID = CCommon.ToLong(objCommon.GetSingleFieldValue())

            Dim StatementId As Long = objEbank.ManageBankStatementHeader()
            objEbank.StatementID = StatementId
            Dim dr As DataRow
            ' dtTransactions = New DataTable
            CreateTransactionTable()
            For i As Integer = 0 To objCcStatement1.Transactions.Count - 1
                dr = dtTransactions.NewRow()
                objEbank.TransactionID = 0
                dr("Amount") = objCcStatement1.Transactions(i).Amount
                objEbank.Amount = objCcStatement1.Transactions(i).Amount
                dr("CheckNumber") = objCcStatement1.Transactions(i).CheckNumber
                objEbank.CheckNumber = objCcStatement1.Transactions(i).CheckNumber
                dr("DtDatePosted") = objCcStatement1.Transactions(i).DatePosted
                objEbank.DtDatePosted = Convert.ToDateTime(objCcStatement1.Transactions(i).DatePosted)
                dr("FITransactionID") = objCcStatement1.Transactions(i).FITID
                objEbank.FITransactionID = objCcStatement1.Transactions(i).FITID
                dr("PayeeName") = objCcStatement1.Transactions(i).PayeeName
                objEbank.PayeeName = objCcStatement1.Transactions(i).PayeeName
                dr("Memo") = objCcStatement1.Transactions(i).Memo
                objEbank.Memo = objCcStatement1.Transactions(i).Memo
                dr("TxType") = objCcStatement1.Transactions(i).TxType.ToString
                objEbank.TxType = objCcStatement1.Transactions(i).TxType.ToString
                objEbank.ManageBankStatementTransactions()
                dtTransactions.Rows.Add(dr)
            Next

            dgTransactions.DataSource = dtTransactions
            dgTransactions.DataBind()

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub GetBankStatement(ByVal dRow As DataRow)
        Try

            Dim objBankStatement1 As New Bankstatement
            Dim objCommon As New CCommon
            objBankStatement1.OFXAppId = "QWIN"
            objBankStatement1.OFXAppVersion = "1800"
            objBankStatement1.FIUrl = CCommon.ToString(dRow("vcFIOFXServerUrl"))
            objBankStatement1.FIId = dRow("vcFIId").ToString
            objBankStatement1.FIOrganization = dRow("vcFIOrganization").ToString
            objBankStatement1.AccountId = dRow("vcAccountNumber").ToString
            Dim AccountTyp As AccountTypes = DirectCast(System.[Enum].Parse(GetType(AccountTypes), dRow("vcAccountType").ToString), AccountTypes)
            objBankStatement1.AccountType = AccountTyp
            objBankStatement1.BankId = dRow("vcBankID").ToString
            objBankStatement1.OFXUser = dRow("vcUserName").ToString
            objBankStatement1.OFXPassword = dRow("vcPassword").ToString

            If CCommon.ToString(dRow("dtCreatedDate")) <> "" Then
                objBankStatement1.StartDate = CCommon.ToString(dRow("dtCreatedDate"))
            Else
                objBankStatement1.StartDate = DateTime.Now.AddDays(-30).ToString
            End If

            objBankStatement1.EndDate = DateTime.Now.ToString()
            objBankStatement1.GetStatement()
            Dim objEbank As New BACRM.BusinessLogic.Accounting.EBanking
            objEbank.StatementID = 0
            objEbank.BankDetailID = CCommon.ToLong(dRow("numBankDetailID"))
            objEbank.FIStatementID = objBankStatement1.FIId

            objEbank.LedgerBalance = CCommon.ToDecimal(objBankStatement1.LedgerBalance)

            Try
                If Not objBankStatement1.LedgerBalanceDate = Nothing Then
                    objEbank.DtLedgerBalanceDate = Convert.ToDateTime(objBankStatement1.LedgerBalanceDate)
                Else
                    objEbank.DtLedgerBalanceDate = DateTime.Now

                End If


            Catch ex As Exception
                objEbank.DtLedgerBalanceDate = DateTime.Now
            End Try


            objEbank.AvailableBalance = CCommon.ToDecimal(objBankStatement1.AvailableBalance)

            Try
                If Not objBankStatement1.AvailableBalanceDate = Nothing Then
                    objEbank.DtAvailableBalanceDate = Convert.ToDateTime(objBankStatement1.AvailableBalanceDate)
                Else
                    objEbank.DtAvailableBalanceDate = DateTime.Now

                End If

            Catch ex As Exception
                objEbank.DtAvailableBalanceDate = DateTime.Now
            End Try

            objEbank.DtStartDate = Convert.ToDateTime(objBankStatement1.StartDate)
            objEbank.DtEndDate = Convert.ToDateTime(objBankStatement1.EndDate)
            objCommon.DomainID = CCommon.ToLong(dRow("numDomainID")) 'Session("DomainID")
            objCommon.Mode = 15
            objCommon.Str = "USD"
            objEbank.CurrencyID = CCommon.ToLong(objCommon.GetSingleFieldValue())

            Dim StatementId As Long = objEbank.ManageBankStatementHeader()
            objEbank.StatementID = StatementId
            Dim dr As DataRow
            ' dtTransactions = New DataTable
            CreateTransactionTable()
            For i As Integer = 0 To objBankStatement1.Transactions.Count - 1
                dr = dtTransactions.NewRow()
                objEbank.TransactionID = 0
                dr("Amount") = objBankStatement1.Transactions(i).Amount
                objEbank.Amount = objBankStatement1.Transactions(i).Amount
                dr("CheckNumber") = objBankStatement1.Transactions(i).CheckNumber
                objEbank.CheckNumber = objBankStatement1.Transactions(i).CheckNumber
                dr("DtDatePosted") = objBankStatement1.Transactions(i).DatePosted
                objEbank.DtDatePosted = Convert.ToDateTime(objBankStatement1.Transactions(i).DatePosted)
                dr("FITransactionID") = objBankStatement1.Transactions(i).FITID
                objEbank.FITransactionID = objBankStatement1.Transactions(i).FITID
                dr("PayeeName") = objBankStatement1.Transactions(i).PayeeName
                objEbank.PayeeName = objBankStatement1.Transactions(i).PayeeName
                dr("Memo") = objBankStatement1.Transactions(i).Memo
                objEbank.Memo = objBankStatement1.Transactions(i).Memo
                dr("TxType") = objBankStatement1.Transactions(i).TxType.ToString
                objEbank.TxType = objBankStatement1.Transactions(i).TxType.ToString
                objEbank.ManageBankStatementTransactions()
                dtTransactions.Rows.Add(dr)
            Next
            If dtTransactions.Rows.Count > 0 Then
                dgTransactions.DataSource = dtTransactions
                dgTransactions.DataBind()
            Else
                dgTransactions.Visible = False
            End If


        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub CreateTransactionTable()

        Try
            dtTransactions = New DataTable
            If dtTransactions.Columns.Count > 0 Then
                dtTransactions.Columns.Clear()
            End If
            dtTransactions.Columns.Add("FITransactionID")
            dtTransactions.Columns.Add("PayeeName")
            dtTransactions.Columns.Add("Amount")
            dtTransactions.Columns.Add("TxType")
            dtTransactions.Columns.Add("CheckNumber")
            dtTransactions.Columns.Add("DtDatePosted")
            dtTransactions.Columns.Add("Memo")

        Catch ex As Exception
            Throw ex

        End Try

    End Sub

    Private Sub DisplayError(ByVal exception As String)
        Try
            DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = exception
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Focus()
        Catch ex As Exception

        End Try
    End Sub
#End Region

End Class