﻿Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Accounting

Public Class frmTransactionDetails
    Inherits BACRMPage

    Dim objEBanking As BACRM.BusinessLogic.Accounting.EBanking
    Dim dtTransactionDetail As DataTable

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then

                If GetQueryStringVal("TransactionID") <> "" Then
                    hdnTransactionID.Value = GetQueryStringVal("TransactionID")
                    objEBanking = New BACRM.BusinessLogic.Accounting.EBanking
                    objEBanking.TransactionID = CCommon.ToLong(hdnTransactionID.Value)
                    dtTransactionDetail = objEBanking.GetTransactionDetails()
                    If (dtTransactionDetail.Rows.Count > 0) Then
                        lblTransactionDate.Text = CCommon.ToString(dtTransactionDetail.Rows(0)("dtDatePosted"))
                        lblBankName.Text = CCommon.ToString(dtTransactionDetail.Rows(0)("vcFIName"))
                        lblTransactionType.Text = CCommon.ToString(dtTransactionDetail.Rows(0)("vcTxType"))
                        Dim strAccountNo As String = CCommon.ToString(dtTransactionDetail.Rows(0)("vcAccountNumber"))
                        lblAccountNo.Text = "XXXXX" & strAccountNo.Substring(strAccountNo.Length - 4)
                        lblRefernceNo.Text = CCommon.ToString(dtTransactionDetail.Rows(0)("vcCheckNumber"))
                        lblUpdateID.Text = ""
                        lblPayee.Text = CCommon.ToString(dtTransactionDetail.Rows(0)("vcPayeeName"))
                        lblFITransactionID.Text = CCommon.ToString(dtTransactionDetail.Rows(0)("vcFITransactionID"))
                        lblAmount.Text = ReturnMoney(CCommon.ToString(dtTransactionDetail.Rows(0)("monAmount")))
                        lblInfo.Text = ""
                        lblStatus.Text = CCommon.ToString(dtTransactionDetail.Rows(0)("TransMatchDetails"))
                        hdnFldAccountID.Value = CCommon.ToString(dtTransactionDetail.Rows(0)("numAccountID"))
                        If CCommon.ToLong(dtTransactionDetail.Rows(0)("numReferenceID")) > 0 Then
                            'btnDelete.Enabled = False
                            hdnFldReferenceID.Value = CCommon.ToLong(dtTransactionDetail.Rows(0)("numReferenceID"))
                            lblStatus.Text = CCommon.ToString(dtTransactionDetail.Rows(0)("TransMatchDetails"))
                            btnDelete.Attributes.Add("onclick", "javascript:DeleteDisabled();")
                        End If

                    Else

                    End If
                End If
              
            End If

        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)

        End Try
    End Sub

    Public Function ReturnMoney(ByVal Money As Object) As String
        Dim MoneyVal As String = ""
        Try
            If Not IsDBNull(Money) Then MoneyVal = String.Format("{0:#,##0.00}", Money)
        Catch ex As Exception
            Throw ex
        End Try
        Return MoneyVal
    End Function

    Private Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Response.Redirect("../Accounting/frmDownloadedTransactions.aspx")

        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)

        End Try
    End Sub


    Private Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Try
            Dim objEBanking As New BACRM.BusinessLogic.Accounting.EBanking
         
            Dim strMessage As String = ""
            If Not CCommon.ToLong(hdnFldReferenceID.Value) > 0 Then
                objEBanking.DomainID = Session("DomainID")
                objEBanking.TransactionID = CCommon.ToLong(hdnTransactionID.Value)
                objEBanking.DeleteBankStatementTransactions()
                strMessage = "Transaction detail Deleted successfully."
                ClientScript.RegisterClientScriptBlock(Page.GetType, "Deleted", "alert('" & strMessage & "');", True)
                Response.Redirect("../Accounting/frmDownloadedTransactions.aspx")

            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)

        End Try
    End Sub

End Class