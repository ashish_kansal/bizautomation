﻿Imports nsoftware.InEBank
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Accounting

Partial Public Class frmBankAccounts
    Inherits BACRMPage

#Region "Members"

    Dim objEBanking As BACRM.BusinessLogic.Accounting.EBanking
    Dim dt As DataTable

#End Region

#Region "Events"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then
                GetAccountDetails()

            End If

        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)

        End Try
    End Sub

    Protected Sub RepDetails_ItemCommand(ByVal source As Object, ByVal e As RepeaterCommandEventArgs) Handles RepDetails.ItemCommand

        Dim objEBanking As New BACRM.BusinessLogic.Accounting.EBanking
        Dim dt As DataTable
        Dim BankDetailID As Long
        Dim lastStatementDate As New DateTime
        Dim strMessage As String = ""
        Try

            If e.CommandName = "btnEvent" Then

                Dim lblAccountMessage As Label = DirectCast(e.Item.FindControl("lblAccountMessage"), Label)
                BankDetailID = CCommon.ToLong(e.CommandArgument)
                lblAccountMessage.Visible = False
                lblAccountMessage.Text = ""
                If BankDetailID > 0 Then
                    objEBanking.TintMode = 0
                    objEBanking.DomainID = CCommon.ToLong(Session("DomainID"))
                    objEBanking.BankDetailID = BankDetailID
                    dt = objEBanking.GetBankDetails()
                    If dt.Rows.Count > 0 Then

                        If Not CCommon.ToString(dt.Rows(0)("dtCreatedDate")) = "" Then
                            lastStatementDate = Convert.ToDateTime(CCommon.ToString(dt.Rows(0)("dtCreatedDate")))
                        End If

                        'If CCommon.ToString(dt.Rows(0)("dtCreatedDate")) = "" Or DateTime.Now.Subtract(lastStatementDate).TotalHours >= 24 Then

                        If CCommon.ToString(dt.Rows(0)("vcAccountType")) = "4" Then
                            objEBanking.AsyncGetCreditCardStatement(dt.Rows(0))

                        Else
                            objEBanking.AsyncGetBankStatement(dt.Rows(0))

                        End If
                        strMessage = "We are connecting to your Account.. Please visit again in few Minutes to see your Current Balance."
                        ScriptManager.RegisterStartupScript(Page, Page.GetType, "alertUser", "alert('" & strMessage & "');", True)

                     
                        'Else
                        '    lblAccountMessage.Visible = True
                        '    lblAccountMessage.Text = "Sorry!., The Account Balance is already Uptodate"
                        'End If

                    Else
                        lblAccountMessage.Visible = True
                        lblAccountMessage.Text = "Sorry!., The Account Doesnot seem having active Connection with Bank"
                    End If

                End If

            ElseIf e.CommandName = "lnkDownloadedTrans" Then
                Dim COAccountID As Long = CCommon.ToLong(e.CommandArgument)
                Response.Redirect("../Accounting/frmDownloadedTransactions.aspx?COAccountID=" & COAccountID, False)
            End If

        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)

        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Response.Redirect("../Accounting/frmChartofAccounts.aspx")
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)

        End Try
    End Sub

    'Protected Sub RepDetails_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles RepDetails.ItemDataBound
    '    If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
    '        Dim bFlag1 As Button = CType(e.Item.FindControl("btnRefreshBalance"), Button)
    '        Dim sArgument As String = CType(e.Item.DataItem, DataRowView).Row.Item("numBankDetailID").ToString

    '        If Not IsNothing(bFlag1) Then
    '            setFlagCommands(bFlag1, "btnEvent", sArgument)
    '        End If


    '    End If
    'End Sub

    'Private Sub setFlagCommands(ByVal bFlag As Button, ByVal sCommand As String, ByVal sArgument As String)
    '    bFlag.CommandName = sCommand
    '    bFlag.CommandArgument = sArgument
    'End Sub

#End Region

#Region "Methods"

    Sub GetAccountDetails()
        Try
            Dim strMessage As String

            objEBanking = New BACRM.BusinessLogic.Accounting.EBanking
            objEBanking.UserContactID = CCommon.ToLong(Session("UserContactID"))
            objEBanking.DomainID = CCommon.ToLong(Session("DomainID"))
            dt = objEBanking.GetUserBankAccounts()
            If dt.Rows.Count > 0 Then
                FillAccountDetails(dt)

            Else
                pnlNoBankAccounts.Visible = True
                'strMessage = "No bank accounts found!., To add one go to Chart Of Accounts and click on \'link your bank account\'"
                'ScriptManager.RegisterStartupScript(Page, Page.GetType, "alertUser", "alert('" & strMessage & "');", True)
                ' Response.Redirect("../Accounting/frmChartofAccounts.aspx")

            End If


        Catch ex As Exception
            Throw ex

        End Try
    End Sub

    Sub FillAccountDetails(ByVal dt As DataTable)
        Try

            RepDetails.DataSource = dt
            RepDetails.DataBind()

        Catch ex As Exception
            Throw ex

        End Try
    End Sub

    Public Function ReturnMoney(ByVal Money As Object) As String
        Try
            If Not IsDBNull(Money) Then Return String.Format("{0:#,##0.00}", Money)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

#End Region

End Class