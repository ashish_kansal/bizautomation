﻿Imports BACRM.BusinessLogic.Accounting
Imports BACRM.BusinessLogic.Common
Partial Public Class frmShippingMethodMapping
    Inherits BACRMPage

    Dim lngShippingMappingID As Long
   
    
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            GetUserRightsForPage(13, 1)

            If m_aryRightsForPage(RIGHTSTYPE.VIEW) = 0 Then
                Response.Redirect("../admin/authentication.aspx?mesg=AS")
            ElseIf m_aryRightsForPage(RIGHTSTYPE.ADD) = 0 Then
                btnSave.Visible = False
            End If

            If Not IsPostBack Then
                
                
                'objCommon.sb_FillComboFromDBwithSel(ddlShipCompany, 82, Session("DomainID"))
                If objCommon Is Nothing Then objCommon = New CCommon
                radShipCompany.DataSource = objCommon.GetMasterListItems(82, Session("DomainID"))
                radShipCompany.DataTextField = "vcData"
                radShipCompany.DataValueField = "numListItemID"
                radShipCompany.DataBind()

                Dim listItem As Telerik.Web.UI.RadComboBoxItem
                If radShipCompany.Items.FindItemByText("Fedex") IsNot Nothing Then
                    radShipCompany.Items.Remove(radShipCompany.Items.FindItemByText("Fedex"))
                End If
                If radShipCompany.Items.FindItemByText("UPS") IsNot Nothing Then
                    radShipCompany.Items.Remove(radShipCompany.Items.FindItemByText("UPS"))
                End If
                If radShipCompany.Items.FindItemByText("USPS") IsNot Nothing Then
                    radShipCompany.Items.Remove(radShipCompany.Items.FindItemByText("USPS"))
                End If

                listItem = New Telerik.Web.UI.RadComboBoxItem
                listItem.Text = "-- Select One --"
                listItem.Value = "0"
                radShipCompany.Items.Insert(0, listItem)

                listItem = New Telerik.Web.UI.RadComboBoxItem
                listItem.Text = "Fedex"
                listItem.Value = "91"
                listItem.ImageUrl = "../images/FedEx.png"
                radShipCompany.Items.Insert(1, listItem)

                listItem = New Telerik.Web.UI.RadComboBoxItem
                listItem.Text = "UPS"
                listItem.Value = "88"
                listItem.ImageUrl = "../images/UPS.png"
                radShipCompany.Items.Insert(2, listItem)

                listItem = New Telerik.Web.UI.RadComboBoxItem
                listItem.Text = "USPS"
                listItem.Value = "90"
                listItem.ImageUrl = "../images/USPS.png"
                radShipCompany.Items.Insert(3, listItem)

                BindCOA()
            End If
            If Not ViewState("ShippingMappingID") Is Nothing Then
                lngShippingMappingID = ViewState("ShippingMappingID")
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
    Private Sub BindData()
        Try
            Dim objCOA As New ChartOfAccounting
            Dim ds As DataSet
            With objCOA
                .ShippingMappingID = 0
                .ShippingMethodID = radShipCompany.SelectedValue 'ddlShipCompany.SelectedValue
                .DomainId = Session("DomainID")
                ds = .GetCOAShippingMapping()
            End With
            If ds.Tables(0).Rows.Count > 0 Then
                ViewState("ShippingMappingID") = ds.Tables(0).Rows(0).Item("numShippingMappingID")
                If Not IsDBNull(ds.Tables(0).Rows(0).Item("numIncomeAccountID")) Then
                    If Not ddlIncomeAccount.Items.FindByValue(ds.Tables(0).Rows(0).Item("numIncomeAccountID")) Is Nothing Then
                        ddlIncomeAccount.Items.FindByValue(ds.Tables(0).Rows(0).Item("numIncomeAccountID")).Selected = True
                    End If
                End If
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            If CCommon.ToLong(ddlIncomeAccount.SelectedValue) > 0 Then
                Dim objCOA As New ChartOfAccounting
                With objCOA
                    .ShippingMappingID = lngShippingMappingID
                    .ShippingMethodID = radShipCompany.SelectedValue 'ddlShipCompany.SelectedValue
                    .AccountId = ddlIncomeAccount.SelectedValue
                    .DomainId = Session("DomainID")
                    lngShippingMappingID = .ManageCOAShippingMapping()
                End With
            Else
                litMessage.Text = "Please select income account"
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub radShipCompany_SelectedIndexChanged(sender As Object, e As Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs) Handles radShipCompany.SelectedIndexChanged
        Try
            If CCommon.ToLong(radShipCompany.SelectedValue) > 0 Then
                ddlIncomeAccount.ClearSelection()
                ViewState("ShippingMappingID") = 0
                lngShippingMappingID = 0
                BindData()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub BindCOA()
        Try
            Dim dtChartAcntDetails As DataTable
            Dim objCOA As New ChartOfAccounting
            objCOA.DomainId = Session("DomainId")
            objCOA.AccountCode = "0103"
            dtChartAcntDetails = objCOA.GetParentCategory()
            Dim item As ListItem
            For Each dr As DataRow In dtChartAcntDetails.Rows
                item = New ListItem()
                item.Text = dr("vcAccountName")
                item.Value = dr("numAccountID")
                item.Attributes("OptionGroup") = dr("vcAccountType")
                ddlIncomeAccount.Items.Add(item)
            Next
            ddlIncomeAccount.Items.Insert(0, New ListItem("--Select One --", "0"))
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

End Class