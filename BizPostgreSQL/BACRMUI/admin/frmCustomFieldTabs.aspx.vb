Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Common
Namespace BACRM.UserInterface.Admin
    Public Class frmCustomFieldTabs
        Inherits BACRMPage

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub
        Protected WithEvents btnClose As System.Web.UI.WebControls.Button
        Protected WithEvents txtTabName As System.Web.UI.WebControls.TextBox
        Protected WithEvents ddlLocation As System.Web.UI.WebControls.DropDownList
        Protected WithEvents btnAdd As System.Web.UI.WebControls.Button
        Protected WithEvents dgTabs As System.Web.UI.WebControls.DataGrid
        Dim myDataTable As New DataTable
        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.
        Private designerPlaceholderDeclaration As System.Object

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Try
                lblError.Text = ""
                divError.Style.Add("display", "none")

                btnClose.Attributes.Add("onclick", "return Close()")
                btnAdd.Attributes.Add("onclick", "return AddTab()")
                If Not IsPostBack Then
                    GetUserRightsForPage(13, 25)
                    If m_aryRightsForPage(RIGHTSTYPE.VIEW) = 0 Then
                        Response.Redirect("../admin/authentication.aspx?mesg=AS")
                    ElseIf m_aryRightsForPage(RIGHTSTYPE.UPDATE) = 0 Then
                        btnSave.Visible = False
                        btnAdd.Visible = False
                    End If

                    PopulateGroups()

                    Dim radItem As Telerik.Web.UI.RadComboBoxItem
                    For Each item As ListItem In ddlGroup.Items
                        If item.Value <> "0" AndAlso CCommon.ToLong(item.Value) <> CCommon.ToLong(Session("UserGroupID")) Then
                            radItem = New Telerik.Web.UI.RadComboBoxItem
                            radItem.Text = item.Text
                            radItem.Value = item.Value
                            rcbGroups.Items.Add(radItem)
                        End If
                    Next
                End If

                If Session("UserGroupID") IsNot Nothing Then
                    If ddlGroup.Items.FindByValue(Session("UserGroupID")) IsNot Nothing Then
                        ddlGroup.Items.FindByValue(Session("UserGroupID")).Attributes.Add("style", "color:green")
                    End If
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Sub BindGrid()
            Try
                Dim objCustomFields As New CustomFields
                objCustomFields.locId = ddlLocation.SelectedItem.Value
                objCustomFields.DomainID = Session("DomainID")
                objCustomFields.Type = -1
                objCustomFields.GroupID = ddlGroup.SelectedValue
                myDataTable = objCustomFields.GetCustomFieldSubTabs
                dgTabs.DataSource = myDataTable
                dgTabs.DataBind()

                dgTabs.Visible = True
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub PopulateGroups()
            Try
                Dim objUserGroups As New UserGroups
                objUserGroups.DomainId = Session("DomainID")
                ddlGroup.DataSource = objUserGroups.GetAutorizationGroup
                ddlGroup.DataTextField = "vcGroupName"
                ddlGroup.DataValueField = "numGroupID"
                ddlGroup.DataBind()
                ddlGroup.Items.Insert(0, "--Select One--")
                ddlGroup.Items.FindByText("--Select One--").Value = 0

                If Session("UserGroupID") IsNot Nothing Then
                    If ddlGroup.Items.FindByValue(Session("UserGroupID")) IsNot Nothing Then
                        ddlGroup.ClearSelection()
                        ddlGroup.Items.FindByValue(Session("UserGroupID")).Selected = True
                    End If
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
            Try
                Dim ObjCusFlds As New CustomFields
                If ddlType.SelectedIndex = 0 Then
                    ObjCusFlds.byteMode = 1
                    ObjCusFlds.TabName = txtTabName.Text
                    ObjCusFlds.locId = ddlLocation.SelectedItem.Value
                    ObjCusFlds.DomainID = Session("DomainID")
                    ObjCusFlds.URL = ""
                Else
                    ObjCusFlds.byteMode = 4
                    ObjCusFlds.TabName = txtTabName.Text
                    ObjCusFlds.locId = ddlLocation.SelectedItem.Value
                    ObjCusFlds.DomainID = Session("DomainID")
                    ObjCusFlds.URL = txtUrl.Text
                End If
                ObjCusFlds.ManageTabsInCustomFields()
                txtTabName.Text = ""
                Call BindGrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Private Sub dgTabs_EditCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgTabs.EditCommand
            Try
                dgTabs.EditItemIndex = e.Item.ItemIndex
                Call BindGrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Private Sub dgTabs_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgTabs.ItemCommand
            Try
                If e.CommandName = "Cancel" Then
                    dgTabs.EditItemIndex = e.Item.ItemIndex
                    dgTabs.EditItemIndex = -1
                    Call BindGrid()
                End If

                If e.CommandName = "Delete" Then
                    Dim ObjCusFlds As New CustomFields
                    ObjCusFlds.byteMode = 3
                    ObjCusFlds.TabId = CType(e.Item.FindControl("lblTabID"), Label).Text
                    Try
                        ObjCusFlds.ManageTabsInCustomFields()
                    Catch ex As Exception
                        If ex.Message = "CHILD_RECORD_FOUND" Then
                            DisplayError("You're not allowed to delete sub-tabs that contains a custom field. You need to first remove the custom field.")
                        Else
                            Throw ex
                        End If
                    End Try
                    BindGrid()
                End If
                If e.CommandName = "Update" Then
                    Dim ObjCusFlds As New CustomFields
                    ObjCusFlds.byteMode = 2
                    ObjCusFlds.TabName = CType(e.Item.FindControl("txtETabName"), TextBox).Text
                    ObjCusFlds.TabId = CType(e.Item.FindControl("lblTabID"), Label).Text
                    ObjCusFlds.ManageTabsInCustomFields()
                    dgTabs.EditItemIndex = -1
                    Call BindGrid()
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Private Sub ddlLocation_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlLocation.SelectedIndexChanged
            Try
                BindGrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Private Sub dgTabs_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgTabs.ItemDataBound
            Try
                If e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.Item Then
                    Dim btnDelete As Button
                    btnDelete = e.Item.FindControl("btnDelete")
                    btnDelete.Attributes.Add("onclick", "return DeleteRecord()")
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub ddlType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlType.SelectedIndexChanged
            Try
                If ddlType.SelectedIndex = 0 Then
                    divURL.Visible = False
                Else
                    divURL.Visible = True
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
            Try
                Dim objUserGroups As UserGroups
                Dim ds As New DataSet

                Dim dtTabs As New DataTable
                dtTabs.Columns.Add("TabId")
                dtTabs.Columns.Add("bitallowed")
                dtTabs.Columns.Add("numOrder")

                Dim dr As DataRow
                For i As Integer = 0 To dgTabs.Items.Count - 1
                    dr = dtTabs.NewRow
                    dr("TabId") = CType(dgTabs.Items(i).FindControl("lblTabID"), Label).Text
                    dr("bitallowed") = IIf(CType(dgTabs.Items(i).FindControl("chk"), CheckBox).Checked, 1, 0)
                    dr("numOrder") = i + 1
                    dtTabs.Rows.Add(dr)
                Next

                dtTabs.TableName = "Table"
                ds.Tables.Add(dtTabs.Copy)

                Dim listItemGroup As New System.Collections.Generic.List(Of Long)
                If ddlGroup.SelectedValue <> "0" AndAlso CCommon.ToLong(ddlGroup.SelectedValue.Split("~")(0)) > 0 Then
                    listItemGroup.Add(ddlGroup.SelectedValue.Split("~")(0))
                End If

                For Each item As Telerik.Web.UI.RadComboBoxItem In rcbGroups.CheckedItems
                    listItemGroup.Add(item.Value)
                Next

                For Each groupID As Long In listItemGroup
                    objUserGroups = New UserGroups
                    objUserGroups.GroupId = groupID
                    objUserGroups.strTabs = ds.GetXml()
                    objUserGroups.RelationShip = 0
                    objUserGroups.Type = 1 ' 1 for subtabs , null or 0 for main tabs
                    objUserGroups.LocationID = ddlLocation.SelectedValue
                    objUserGroups.ManageTabsAuthorization()
                Next
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Private Sub ddlGroup_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGroup.SelectedIndexChanged
            Try

                If ddlGroup.SelectedValue > 0 Then
                    BindGrid()
                End If

                rcbGroups.Items.Clear()
                Dim radItem As Telerik.Web.UI.RadComboBoxItem
                For Each item As ListItem In ddlGroup.Items
                    If item.Value <> "0" AndAlso item.Value <> ddlGroup.SelectedValue Then
                        radItem = New Telerik.Web.UI.RadComboBoxItem
                        radItem.Text = item.Text
                        radItem.Value = item.Value
                        rcbGroups.Items.Add(radItem)
                    End If
                Next
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Private Sub DisplayError(ByVal ex As String)
            lblError.Text = ex
            divError.Style.Remove("display")
        End Sub
    End Class
End Namespace

