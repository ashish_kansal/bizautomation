<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmItemSearch.aspx.vb"
    Inherits="BACRM.UserInterface.Admin.frmItemSearch" MasterPageFile="~/common/DetailPage.Master" %>

<%@ Import Namespace="BACRM.BusinessLogic.Admin" %>
<%@ Import Namespace="BACRM.BusinessLogic.Reports" %>
<%@ Register TagPrefix="menu1" TagName="webmenu" Src="../include/webmenu.ascx" %>
<%@ Register TagPrefix="BizCalendar" TagName="Calendar" Src="../include/calandar.ascx" %>
<%@ Register Src="frmAdvSearchCriteria.ascx" TagName="frmAdvSearchCriteria" TagPrefix="uc1" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Import Namespace="BACRM.BusinessLogic.Common" %>
<%@ Register Src="frmMySavedSearch.ascx" TagName="frmMySavedSearch" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Untitled Page</title>
    <link rel="stylesheet" type="text/css" href="../CSS/jscal2.css" />
    <link rel="stylesheet" type="text/css" href="../CSS/border-radius.css" />
    <script type="text/javascript" src="../JavaScript/jscal2.js"></script>
    <script type="text/javascript" src="../JavaScript/en.js"></script>
    <script src="../javascript/AdvSearchScripts.js" type="text/javascript"></script>
    <script src="../javascript/AdvSearchColumnCustomization.js"
        type="text/javascript"></script>
    <script type="text/javascript" language="javascript">
        function CheckNumber(cint, e) {
            var k;
            document.all ? k = e.keyCode : k = e.which;
            if (cint == 1) {
                if (!(k > 47 && k < 58 || k == 44 || k == 46 || k == 8 || k == 37 || k == 39 || k == 16)) {
                    if (e.preventDefault) {
                        e.preventDefault();
                    }
                    else
                        e.returnValue = false;
                    return false;
                }
            }
            if (cint == 2) {

                if (!(k > 47 && k < 58 || k == 8 || k == 37 || k == 39 || k == 16 || k == 9)) {

                    if (e.preventDefault) {
                        e.preventDefault();
                    }
                    else
                        e.returnValue = false;
                    return false;
                }
            }
        }
        function PopupCheck() {
        }
    </script>
    <script language="vb" runat="server">
        Private Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
            Try
                Dim str As String = ""
                Dim sbFriendlyQuery As New StringBuilder
                'User ,Territory and Team Filter
                If rdlReportType.SelectedValue = 1 Then ' Users
                    Dim objRep As New PredefinedReports
                    Dim dtUserSelected As Data.DataTable
                    objRep.UserCntID = Session("UserContactId")
                    objRep.DomainID = Session("DomainID")
                    objRep.ReportType = 6 ' for advance search
                    dtUserSelected = objRep.GetUsersForForRep()
                    If dtUserSelected.Rows.Count > 0 Then
                        Select Case CCommon.ToInteger(dtUserSelected.Rows(0)("tintUserType"))
                            Case 1 'record owner 
                                str = str & " and OppMas.numRecOwner in (SELECT numSelectedUserCntID FROM ForReportsByUser where numcontactid=" & Session("UserContactID") & " and numDomainID=" & Session("DomainID") & " and tintType = 6)"
                            Case 2 ' record assignee
                                str = str & " and OppMas.numAssignedTo in (SELECT numSelectedUserCntID FROM ForReportsByUser where numcontactid=" & Session("UserContactID") & " and numDomainID=" & Session("DomainID") & " and tintType = 6)"
                            Case Else
                                'all
                        End Select
                    End If
                ElseIf rdlReportType.SelectedValue = 2 Then ' Team
                    str = str & " and OppMas.numRecOwner in (select  numContactID from AdditionalContactsInformation where numTeam in(select numTeam from ForReportsByTeam where numUserCntID=" & Session("UserContactID") & " and numDomainID=" & Session("DomainID") & " and tintType =6))"
                ElseIf rdlReportType.SelectedValue = 3 Then 'Territory
                    str = str & " and DM.numTerId in (select numTerritory from ForReportsByTerritory where numUserCntID=" & Session("UserContactID") & " and numDomainID=" & Session("DomainID") & " and tintType =6)"
                End If

                'Opportunity Type filter 
                Select Case ddlOppType.SelectedValue
                    Case 1 'Sales order 
                        str = str & " and OppMas.tintOppType = 1 AND OppMas.tintOppStatus = 1 "
                    Case 2 'purchase order
                        str = str & " and OppMas.tintOppType = 2 AND OppMas.tintOppStatus = 1"
                    Case 3 'saled opp
                        str = str & " and OppMas.tintOppType = 1 AND OppMas.tintOppStatus = 0"
                    Case 4 'purchase opp
                        str = str & " and OppMas.tintOppType = 2 AND tintOppStatus = 0 "
                    Case Else
                End Select
                FormGenericAdvSearch.GenerateUserFriendlySearchString(sbFriendlyQuery, "Order Type", "ddlOppType", ddlOppType.SelectedItem.Text, "", "Equal To")

                'BizDoc Type Filter
                Select Case ddlBizDocType.SelectedValue
                    Case 1  'Only records without BizDocs
                        str = str & " and OppMas.numOppID in (SELECT OM.numOppId FROM OpportunityMaster OM LEFT JOIN OpportunityBizDocs OBD ON OM.numOppId = OBD.numOppId  "
                        str = str & " WHERE OBD.numOppBizDocsId IS NULL AND OM.numDomainId = " & Session("DomainID") & " )"
                    Case 2 'Only records with Non-Authoritative BizDocs
                        str = str & " and OpportunityBizDocs.numOppBizDocsId in ("
                        str = str & "SELECT distinct numOppBizDocsId FROM    OpportunityBizDocs WHERE"
                        str = str & " numBizDocId NOT IN"
                        str = str & " (SELECT  numAuthoritativeSales FROM AuthoritativeBizDocs WHERE   numDomainId = " & Session("DomainID")
                        str = str & " union "
                        str = str & " SELECT  numAuthoritativePurchase FROM AuthoritativeBizDocs WHERE   numDomainId = " & Session("DomainID") & " )"
                        str = str & ") "
                    Case 3 'Only records containing Authoritative BizDocs
                        str = str & " and OpportunityBizDocs.numOppBizDocsId in ("
                        str = str & "SELECT distinct numOppBizDocsId FROM    OpportunityBizDocs WHERE"
                        str = str & " numBizDocId IN"
                        str = str & " (SELECT  numAuthoritativeSales FROM AuthoritativeBizDocs WHERE   numDomainId = " & Session("DomainID")
                        str = str & " union "
                        str = str & " SELECT  numAuthoritativePurchase FROM AuthoritativeBizDocs WHERE   numDomainId = " & Session("DomainID") & " )"
                        str = str & ") "
                    Case 4 'Orders containing invoice(s) for some but not all items
                        str = str & " and OppMas.numOppID in ("
                        str = str & "SELECT distinct X.numOppid FROM("
                        str = str & " SELECT A.ACount,A.numOppId,B.BCount FROM "
                        str = str & " (SELECT OM.numOppId,COUNT(numOppBizDocItemID) ACount FROM OpportunityBizDocItems OBI INNER JOIN OpportunityBizDocs OB "
                        str = str & " ON OBI.numOppBizDocID = OB.numOppBizDocsId INNER JOIN OpportunityMaster OM ON OM.numOppId = OB.numOppId "
                        str = str & " WHERE OB.bitAuthoritativeBizDocs=1 AND OM.numDomainId= " & Session("DomainID")
                        str = str & " GROUP BY OM.numOppId) A, "
                        str = str & " (SELECT OM.numOppId,COUNT(numoppitemtCode) BCount FROM OpportunityItems OI INNER JOIN OpportunityMaster OM "
                        str = str & " ON OM.numOppId=OI.numOppId WHERE OM.numDomainId= " & Session("DomainID") & " GROUP BY OM.numOppId) B "
                        str = str & " WHERE A.numoppID = B.numoppID "
                        str = str & " )X "
                        str = str & " WHERE X.Acount <X.BCount ) "
                    Case Else
                End Select
                FormGenericAdvSearch.GenerateUserFriendlySearchString(sbFriendlyQuery, "BizDocs", "ddlBizDocType", ddlBizDocType.SelectedItem.Text, "", "Equal To")

                'Crete/Mofified Date  filter
                If rbFixedDate.Checked = True Then 'fixed date
                    Session("TimeQuery") = GetDateQuery(calFrom.SelectedDate, calTo.SelectedDate)
                    If chkCreated.Checked Or chkModified.Checked Then
                        FormGenericAdvSearch.GenerateUserFriendlySearchString(sbFriendlyQuery, IIf(chkCreated.Checked, "Created Date ", "") & IIf(chkModified.Checked, "Modified Date ", ""), "rbFixedDate", calFrom.SelectedDate & " and " & calTo.SelectedDate, "", "Between")
                    End If
                ElseIf rbSlidingDate.Checked = True Then 'sliding date
                    Select Case ddlDateSpan.SelectedValue
                        Case 365
                            Session("TimeQuery") = GetDateQuery(CStr(New Date(Now.Year, 1, 1)), CStr(Date.Today))
                        Case Else
                            Session("TimeQuery") = GetDateQuery(Date.Today.AddDays(-ddlDateSpan.SelectedValue), Date.Today)
                    End Select
                    Session("SlidingDays") = ddlDateSpan.SelectedValue
                    If chkCreated.Checked Or chkModified.Checked Then
                        FormGenericAdvSearch.GenerateUserFriendlySearchString(sbFriendlyQuery, IIf(chkCreated.Checked, "Created Date ", "") & IIf(chkModified.Checked, "Modified Date ", ""), "ddlDateSpan", ddlDateSpan.SelectedItem.Text, "", "Equal To")
                    End If
                End If




                'If txtFromAmount.Text <> "" And txtToAmount.Text <> "" Then str = str & " and (OppMas.monDealAmount between " & txtFromAmount.Text & " and " & txtToAmount.Text & ") "
                If txtFromQuantity.Text <> "" And txttoQuantity.Text <> "" Then
                    str = str & " and OppMas.numOppId  in  (select numOppId from OpportunityItems OppItems where OppItems.numUnitHour between " & txtFromQuantity.Text & " and " & txttoQuantity.Text & ") "
                    FormGenericAdvSearch.GenerateUserFriendlySearchString(sbFriendlyQuery, "Quantity ", "txtFromQuantity", txtFromQuantity.Text & " and " & txttoQuantity.Text, "", "Between")
                End If


                If txtFromProgress.Text <> "" And txtToProgress.Text <> "" Then
                    str = str & " and OppMas.numOppId  in  (SELECT DISTINCT PP.numOppId FROM ProjectProgress PP INNER JOIN OpportunityMaster OM ON OM.numOppId = PP.numOppId"
                    str = str & " WHERE PP.numDomainId =" & Session("DomainID") & " and  intTotalProgress between " & txtFromProgress.Text & " and " & txtToProgress.Text & ") "
                    FormGenericAdvSearch.GenerateUserFriendlySearchString(sbFriendlyQuery, "Total Progress ", "txtFromProgress", txtFromProgress.Text & " and " & txtToProgress.Text, "", "Between")
                End If

                If chkAmtSubtotal.Checked Then
                    FormGenericAdvSearch.GenerateUserFriendlySearchString(sbFriendlyQuery, "Apply amount only from sub-total", "chkAmtSubtotal", "Checked", "", "Is")
                End If


                Dim i As Integer
                Dim ControlType, strFieldName, strFieldValue, strFieldID, strFriendlyName, strControlID, strLookBackTable As String
                Dim dr As System.Data.DataRow

                For i = 0 To dtGenericFormConfig.Rows.Count - 1
                    dr = dtGenericFormConfig.Rows(i)
                    strFieldID = dr("numFormFieldID").ToString.Replace("C", "").Replace("D", "").Replace("R", "")
                    ControlType = dr("vcAssociatedControlType").ToString.ToLower.Replace(" ", "") 'replace whitespace with blank 
                    strFieldName = dr("vcDbColumnName").ToString.Trim.Replace(" ", "_")
                    strFriendlyName = dr("vcFormFieldName").ToString
                    strLookBackTable = dr("vcLookBackTableName").ToString
                    strControlID = dr("numFormFieldID").ToString & "_" & dr("vcDbColumnName").ToString.Trim.Replace(" ", "_")

                    Select Case ControlType
                        Case "selectbox"
                            strFieldValue = IIf(CType(tblMain.FindControl(strControlID), DropDownList).SelectedIndex > 0, CType(tblMain.FindControl(strControlID), DropDownList).SelectedValue, 0)
                            If IsNumeric(strFieldValue) AndAlso strFieldValue > 0 Then FormGenericAdvSearch.GenerateUserFriendlySearchString(sbFriendlyQuery, strFriendlyName, strFieldName, CType(tblMain.FindControl(strControlID), DropDownList).SelectedItem.Text, hdnSaveSearchCriteria.Value.Trim, "Equal To")
                        Case "checkboxlist"
                            strFieldValue = ""
                            Dim chkbl As CheckBoxList = CType(tblMain.FindControl(strControlID), CheckBoxList)
                            Dim fldText As String = ""

                            For Each item As ListItem In chkbl.Items
                                If item.Selected Then
                                    strFieldValue = strFieldValue & If(strFieldValue.Length > 0, " OR ", "") & " CONCAT(',',Fld_Value,',') ilike '%," & item.Value & ",%'"
                                    fldText = fldText & item.Text & " or "
                                End If
                            Next

                            strFieldValue = strFieldValue.TrimEnd("OR")
                            If strFieldValue.Length > 1 Then FormGenericAdvSearch.GenerateUserFriendlySearchString(sbFriendlyQuery, strFriendlyName, strFieldName, fldText.Trim.TrimEnd("r").TrimEnd("o"), hdnSaveSearchCriteria.Value.Trim, "Equals To")
                        Case "listbox"
                            'Create Only User Friendly Query here, perform actuall query at bottom
                            strFieldValue = ""
                            Dim lstBox As Telerik.Web.UI.RadComboBox = CType(tblMain.FindControl(strControlID), Telerik.Web.UI.RadComboBox)
                            Dim fldText As String = ""

                            For Each item As RadComboBoxItem In lstBox.CheckedItems
                                strFieldValue = strFieldValue & item.Value & ","
                                fldText = fldText & item.Text & " or "
                            Next

                            strFieldValue = strFieldValue.TrimEnd(",")

                            If strFieldValue.Length > 1 Then FormGenericAdvSearch.GenerateUserFriendlySearchString(sbFriendlyQuery, strFriendlyName, strFieldName, fldText.Trim.TrimEnd("r").TrimEnd("o"), hdnSaveSearchCriteria.Value.Trim, "Equals To")
                        Case "checkbox"
                            'strFieldValue = IIf(CType(tblMain.FindControl(strControlID), CheckBox).Checked, "1", "0")
                            'If strFieldValue = 1 Then FormGenericAdvSearch.GenerateUserFriendlySearchString(sbFriendlyQuery, strFriendlyName, strFieldName, "Checked", hdnSaveSearchCriteria.Value.Trim, "Is")

                            strFieldValue = IIf(CType(tblMain.FindControl(strControlID), DropDownList).SelectedIndex > 0, CType(tblMain.FindControl(strControlID), DropDownList).SelectedValue, -1)
                            If CCommon.ToLong(strFieldValue) > -1 Then FormGenericAdvSearch.GenerateUserFriendlySearchString(sbFriendlyQuery, strFriendlyName, strFieldName, CType(tblMain.FindControl(strControlID), DropDownList).SelectedItem.Text, hdnSaveSearchCriteria.Value.Trim, "Equal To")
                        Case "datefield"
                            strFieldValue = CType(tblMain.FindControl(strControlID), BACRM.Include.calandar).SelectedDate
                            If CType(tblMain.FindControl("hdn_" & strControlID), HiddenField).Value = 11 Then 'between condition is set
                                If strFieldValue <> "" Then FormGenericAdvSearch.GenerateUserFriendlySearchString(sbFriendlyQuery, strFriendlyName, strFieldName, CType(tblMain.FindControl("hdn_" & strControlID & "_value"), HiddenField).Value, hdnSaveSearchCriteria.Value.Trim, "Between")
                            Else
                                If strFieldValue <> "" Then FormGenericAdvSearch.GenerateUserFriendlySearchString(sbFriendlyQuery, strFriendlyName, strFieldName, strFieldValue, hdnSaveSearchCriteria.Value.Trim, "Equal To")
                            End If
                        Case Else
                            ''Added By Sachin Sadhu||Date:12thJune2014
                            ''Purpose:To add Functionaly :Search with Notes Field-OpportunityBizDocItems
                            If strLookBackTable = "OpportunityBizDocItems" And CCommon.ToInteger(ddlBizDocType.SelectedValue) = 1 Then
                                'nothing
                            Else
                                strFieldValue = Replace(CType(tblMain.FindControl(strControlID), TextBox).Text.Trim, "'", "''")
                                If strFieldValue <> "" Then FormGenericAdvSearch.GenerateUserFriendlySearchString(sbFriendlyQuery, strFriendlyName, strFieldName, strFieldValue, hdnSaveSearchCriteria.Value.Trim, IIf(dr("vcFieldDataType").ToString.ToLower = "v", "Contains", "Equal To"))
                            End If
                            'End of Code

                    End Select



                    If ControlType = "selectbox" Or ControlType = "checkbox" Then
                        Dim ddlControl As DropDownList = CType(tblMain.FindControl(strControlID), DropDownList)
                        If dr("vcFieldType") = "R" Then
                            If ddlControl.SelectedIndex > 0 Then 'Or ddlControl.SelectedValue = "0" Then 'added 0 filter for passing order status, when 0 then search for record with value null or 0 
                                ''Based On the Look Back Table Sub Queries 
                                If strFieldName = "numShipCountry" Or strFieldName = "numBillCountry" Then

                                    If strFieldName = "numShipCountry" Then
                                        str = str & " and oppMas.numOppID in "
                                        str = str & " ("
                                        str = str & " SELECT numOppID FROM OpportunityMaster Where COALESCE(tintShipToType,1)=0 and numDomainId= " & Session("DomainId") & " AND numDivisionID in  "
                                        str = str & " (SELECT numRecordID FROM AddressDetails WHERE numDomainID=" & Session("DomainID") & " and tintAddressOf=2 AND tintAddressType=2 AND " & FormGenericAdvSearch.GetSearchCondition(hdnFilterCriteria.Value, "numCountry", strControlID, strFieldValue, "N", TablePrefix:="") & ") "

                                        str = str & " union "

                                        ''WHEN tintShipToType=1
                                        str = str & " SELECT OM.numOppId FROM OpportunityMaster OM INNER JOIN DivisionMaster DM"
                                        str = str & " ON OM.numDivisionId = DM.numDivisionID WHERE COALESCE(OM.tintShipToType,1)=1 AND OM.numDomainId= " & Session("DomainID") & " AND DM.numDivisionID in "
                                        str = str & " (SELECT numRecordID FROM AddressDetails WHERE numDomainID=" & Session("DomainID") & " and tintAddressOf=2 AND tintAddressType=2 "
                                        str = str & " AND " & FormGenericAdvSearch.GetSearchCondition(hdnFilterCriteria.Value, "numCountry", strControlID, strFieldValue, "N", TablePrefix:="") & " ) "

                                        str = str & " union "

                                        'WHEN tintShipToType=2
                                        str = str & " SELECT OM.numOppId FROM OpportunityAddress OA INNER JOIN OpportunityMaster OM ON OA.numOppID = OM.numOppId WHERE COALESCE(OM.tintShipToType,1)=2 AND OM.numDomainId= " & Session("DomainID")
                                        str = str & " AND " & FormGenericAdvSearch.GetSearchCondition(hdnFilterCriteria.Value, "numShipCountry", strControlID, strFieldValue, "N", TablePrefix:="OA.")
                                        str = str & " ) "

                                    ElseIf strFieldName = "numBillCountry" Then
                                        str = str & " and oppMas.numOppID in "
                                        str = str & " ("
                                        str = str & " SELECT numOppID FROM OpportunityMaster Where COALESCE(tintBillToType,1)=0 and numDomainId= " & Session("DomainId") & " AND numDivisionID in  "
                                        str = str & " (SELECT numRecordID FROM AddressDetails WHERE numDomainID=" & Session("DomainID") & " and tintAddressOf=2 AND tintAddressType=1 AND " & FormGenericAdvSearch.GetSearchCondition(hdnFilterCriteria.Value, "numCountry", strControlID, strFieldValue, "N", TablePrefix:="") & ") "

                                        str = str & " union "

                                        ''WHEN tintBillToType=1
                                        str = str & " SELECT OM.numOppId FROM OpportunityMaster OM INNER JOIN DivisionMaster DM"
                                        str = str & " ON OM.numDivisionId = DM.numDivisionID WHERE COALESCE(OM.tintBillToType,1)=1 AND OM.numDomainId= " & Session("DomainID") & " AND DM.numDivisionID in "
                                        str = str & " (SELECT numRecordID FROM AddressDetails WHERE numDomainID=" & Session("DomainID") & " and tintAddressOf=2 AND tintAddressType=1 "
                                        str = str & " AND " & FormGenericAdvSearch.GetSearchCondition(hdnFilterCriteria.Value, "numCountry", strControlID, strFieldValue, "N", TablePrefix:="") & " ) "

                                        str = str & " union "

                                        'WHEN tintBillToType=2
                                        str = str & " SELECT OM.numOppId FROM OpportunityAddress OA INNER JOIN OpportunityMaster OM ON OA.numOppID = OM.numOppId WHERE COALESCE(OM.tintBillToType,1)=2 AND OM.numDomainId= " & Session("DomainID")
                                        str = str & " AND " & FormGenericAdvSearch.GetSearchCondition(hdnFilterCriteria.Value, "numBillCountry", strControlID, strFieldValue, "N", TablePrefix:="OA.")
                                        str = str & " ) "
                                    End If
                                ElseIf dr("vcLookBackTableName") = "Warehouses" Then '
                                    str = str & " and oppMas.numOppId in "
                                    str = str & " (select distinct(OppMas.numOppId)   from WareHouses WareHouse"
                                    str = str & " join WareHouseItems on WareHouseItems.numWareHouseID =WareHouse.numWareHouseID "
                                    str = str & " join Item on Item.numItemCode = WareHouseItems.numItemId"
                                    str = str & " Join OpportunityItems OppItems on Item.numItemCode = OppItems.numItemCode"
                                    str = str & " join OpportunityMaster OppMas on OppItems.numOppId = OppMas.numOppId"
                                    str = str & " where OppMas.numDomainId= " & Session("DomainId")
                                    str = str & " and " & FormGenericAdvSearch.GetSearchCondition(hdnFilterCriteria.Value, strFieldName, strControlID, ddlControl.SelectedValue, dr("vcFieldDataType"))
                                    str = str & " ) "
                                ElseIf dr("vcLookBackTableName") = "WareHouseItmsDTL" Then
                                    str = str & " and oppMas.numOppId in "
                                    str = str & " (select distinct(OppMas.numOppId) from WareHouseItmsDTL WareHouseItemDTL"
                                    str = str & " join WareHouseItems on WareHouseItemDTL.numWareHouseItemID =WareHouseItems.numWareHouseItemID "
                                    str = str & " join Item on Item.numItemCode = WareHouseItems.numItemId"
                                    str = str & " Join OpportunityItems OppItems on Item.numItemCode = OppItems.numItemCode"
                                    str = str & " join OpportunityMaster OppMas on OppItems.numOppId = OppMas.numOppId "
                                    str = str & " join OppWarehouseSerializedItem OppSerItm on (OppSerItm.numOppId = OppMas.numOppId and WareHouseItemDTL.numWarehouseItmsDTLID= OppSerItm.numWarehouseItmsDTLID) "
                                    str = str & " where OppMas.numDomainId= " & Session("DomainId")
                                    str = str & " and " & FormGenericAdvSearch.GetSearchCondition(hdnFilterCriteria.Value, strFieldName, strControlID, ddlControl.SelectedValue, dr("vcFieldDataType"))
                                    str = str & " ) "
                                ElseIf dr("vcLookBackTableName") = "WareHouseItems" Then
                                    str = str & " and oppMas.numOppId in "
                                    str = str & " (select distinct(OppMas.numOppId)    from WareHouseItems"
                                    str = str & " join Item on Item.numItemCode = WareHouseItems.numItemId   "
                                    str = str & " Join OpportunityItems OppItems on Item.numItemCode = OppItems.numItemCode  "
                                    str = str & " join OpportunityMaster OppMas on OppItems.numOppId = OppMas.numOppId "
                                    str = str & " where OppMas.numDomainId= " & Session("DomainId")
                                    str = str & " and " & FormGenericAdvSearch.GetSearchCondition(hdnFilterCriteria.Value, strFieldName, strControlID, ddlControl.SelectedValue, dr("vcFieldDataType"))
                                    str = str & " ) "
                                    'ElseIf dr("vcLookBackTableName") = "OpportunityBizDocItems" Then
                                    '    str = str & " and oppMas.numOppId in "
                                    '    str = str & " (select distinct(OppMas.numOppId)    from WareHouseItems"
                                    '    str = str & " join Item on Item.numItemCode = WareHouseItems.numItemId   "
                                    '    str = str & " Join OpportunityItems OppItems on Item.numItemCode = OppItems.numItemCode  "
                                    '    str = str & " join OpportunityMaster OppMas on OppItems.numOppId = OppMas.numOppId "
                                    '    str = str & " where OppMas.numDomainId= " & Session("DomainId")
                                    '    str = str & " and " & FormGenericAdvSearch.GetSearchCondition(hdnFilterCriteria.Value, strFieldName, strControlID, ddlControl.SelectedValue, dr("vcFieldDataType"))
                                    '    str = str & " ) "
                                ElseIf dr("vcLookBackTableName") = "Item" Then
                                    str = str & " and oppMas.numOppId in "
                                    str = str & " (select distinct(OppItems.numOppId)   from item "
                                    str = str & "Join OpportunityItems OppItems on Item.numItemCode = OppItems.numItemCode "

                                    If strFieldName = "numBaseUnit" Or strFieldName = "numPurchaseUnit" Or strFieldName = "numSaleUnit" Then
                                        str = str & " and item." & FormGenericAdvSearch.GetSearchCondition(hdnFilterCriteria.Value, strFieldName, strControlID, CType(tblMain.FindControl(strControlID), DropDownList).SelectedItem.Value, dr("vcFieldDataType"))
                                        If strFieldValue <> "0" Then FormGenericAdvSearch.GenerateUserFriendlySearchString(sbFriendlyQuery, strFriendlyName, strFieldName, CType(tblMain.FindControl(strControlID), DropDownList).SelectedItem.Text, hdnSaveSearchCriteria.Value.Trim, "Equal To")
                                    ElseIf strFieldName = "charItemType" Then
                                        str = str & " and " & FormGenericAdvSearch.GetSearchCondition(hdnFilterCriteria.Value, strFieldName, strControlID, strFieldValue, dr("vcFieldDataType"), TablePrefix:="Item.")
                                        If strFieldValue <> "0" Then FormGenericAdvSearch.GenerateUserFriendlySearchString(sbFriendlyQuery, strFriendlyName, strFieldName, CType(tblMain.FindControl(strControlID), DropDownList).SelectedItem.Text, hdnSaveSearchCriteria.Value.Trim, "Equal To")
                                    Else
                                        str = str & " and " & FormGenericAdvSearch.GetSearchCondition(hdnFilterCriteria.Value, strFieldName, strControlID, strFieldValue, dr("vcFieldDataType"), TablePrefix:="Item.")
                                    End If
                                    str = str & " ) "
                                ElseIf dr("vcLookBackTableName") = "OpportunityContact" Then
                                    If strFieldName = "numOppContactId" Then
                                        str = str & " AND OppMas.numOppId in ( SELECT OC.numOppId FROM OpportunityContact OC INNER JOIN OpportunityMaster OM ON OC.numOppId = OM.numOppId WHERE numDomainId= " & Session("DomainID") & " AND " & FormGenericAdvSearch.GetSearchCondition(hdnFilterCriteria.Value, "numRole", strControlID, ddlControl.SelectedValue, dr("vcFieldDataType")) & "  ) "
                                    End If
                                ElseIf dr("vcLookBackTableName") = "OpportunityBizDocsDetails" Then
                                    If strFieldName = "numPaymentMethod" Then
                                        str = str & " AND OppMas.numOppId in ( SELECT BD.numOppId FROM OpportunityBizDocsDetails BDD INNER JOIN OpportunityBizDocs BD ON  BDD.numBizDocsId = BD.numOppBizDocsId"
                                        str = str & " WHERE BDD.numDomainId = " & Session("DomainID") & " and " & FormGenericAdvSearch.GetSearchCondition(hdnFilterCriteria.Value, strFieldName, strControlID, ddlControl.SelectedValue, dr("vcFieldDataType")) & "  ) "
                                    End If
                                ElseIf dr("vcLookBackTableName") = "DivisionMaster" Then 'for Bill to ship to country/state
                                    'Don't do anything. is handled seperately
                                Else
                                    Dim Prefix As String = ""
                                    If dr("vcLookBackTableName") = "CompanyInfo" Then
                                        Prefix = "DM."
                                    ElseIf dr("vcLookBackTableName") = "AdditionalContactsInformation" Then
                                        Prefix = "ADC."
                                    ElseIf dr("vcLookBackTableName") = "OpportunityMaster" Then
                                        Prefix = "OppMas."
                                    ElseIf dr("vcLookBackTableName") = "OpportunityBizDocs" Then
                                        Prefix = "OpportunityBizDocs."
                                    End If

                                    If strFieldName = "numSalesOrPurType" Then
                                        str = str & " and " & FormGenericAdvSearch.GetSearchCondition(hdnFilterCriteria.Value, strFieldName, strControlID, ddlControl.SelectedValue, dr("vcFieldDataType"), TablePrefix:=Prefix)
                                    ElseIf strFieldName = "tintSource" Then
                                        str = str & " and " & FormGenericAdvSearch.GetSearchCondition(hdnFilterCriteria.Value, strFieldName, strControlID, ddlControl.SelectedValue.Split("~")(0), dr("vcFieldDataType"), TablePrefix:=Prefix)
                                        str = str & " and " & FormGenericAdvSearch.GetSearchCondition(hdnFilterCriteria.Value, "tintSourceType", strControlID, ddlControl.SelectedValue.Split("~")(1), dr("vcFieldDataType"), TablePrefix:=Prefix)
                                    ElseIf ddlControl.SelectedValue = "0" Then
                                        str = str & " and COALESCE(" & Prefix & strFieldName & ",0) = 0"
                                    Else
                                        str = str & " and " & FormGenericAdvSearch.GetSearchCondition(hdnFilterCriteria.Value, strFieldName, strControlID, ddlControl.SelectedValue, dr("vcFieldDataType"), TablePrefix:=Prefix)
                                    End If

                                End If
                            End If
                        ElseIf dr("vcFieldType") = "O" Then
                            If ddlControl.SelectedIndex > 0 Then
                                str = str & " and OppMas.numOppId in (select RecId from CFW_FLD_Values_Opp where Fld_ID =" & strFieldID & " and Fld_Value = '" & ddlControl.SelectedValue & "' )"
                            End If
                        ElseIf dr("vcFieldType") = "P" Then
                            If ddlControl.SelectedIndex > 0 Then
                                str = str & " and OppMas.numOppId in (select numOppId from OpportunityItems where numItemCode in  (select RecId from CFW_FLD_Values_Item where Fld_ID =" & strFieldID & " and Fld_Value = '" & ddlControl.SelectedValue & "' ))"
                            End If
                        ElseIf dr("vcFieldType") = "I" Then
                            If ddlControl.SelectedIndex > 0 Then
                                str = str & " and OppMas.numOppId in (Select OI.numOppId FROM OpportunityItems OI"
                                str = str & " INNER JOIN Item I ON OI.numItemCode = I.numItemCode"
                                str = str & " INNER JOIN CFW_Fld_Values_Serialized_Items SI ON I.bitSerialized = SI.bitSerialized AND OI.numWarehouseItmsID = SI.RecId"
                                str = str & " WHERE SI.Fld_ID=" & strFieldID & " and OI.numWarehouseItmsID > 0 And numDomainID = " & Session("DomainID") & " "
                                str = str & " and SI.Fld_Value = '" & strFieldValue & "' )"
                            End If
                        End If
                    ElseIf ControlType = "listbox" Then
                        If strFieldValue.Length > 1 Then
                            If strFieldName = "numShipState" Or strFieldName = "numBillState" Then
                                If strFieldName = "numShipState" Then
                                    str = str & " and oppMas.numOppID in "
                                    str = str & " ("
                                    str = str & " SELECT numOppID FROM OpportunityMaster Where COALESCE(tintShipToType,1)=0 and numDomainId= " & Session("DomainId") & " AND numDivisionID in  "
                                    str = str & " (SELECT numRecordID FROM AddressDetails WHERE numDomainID=" & Session("DomainID") & " and tintAddressOf=2 AND tintAddressType=2 and numState in (" & strFieldValue & ")) "

                                    str = str & " union "

                                    ''WHEN tintShipToType=1
                                    str = str & " SELECT OM.numOppId FROM OpportunityMaster OM INNER JOIN DivisionMaster DM"
                                    str = str & " ON OM.numDivisionId = DM.numDivisionID WHERE COALESCE(OM.tintShipToType,1)=1 AND OM.numDomainId= " & Session("DomainID") & " AND DM.numDivisionID in "
                                    str = str & " (SELECT numRecordID FROM AddressDetails WHERE numDomainID=" & Session("DomainID") & " and tintAddressOf=2 AND tintAddressType=2 "
                                    str = str & " and numState in (" & strFieldValue & ")) "

                                    str = str & " union "

                                    'WHEN tintShipToType=2
                                    str = str & " SELECT OM.numOppId FROM OpportunityAddress OA INNER JOIN OpportunityMaster OM ON OA.numOppID = OM.numOppId WHERE COALESCE(OM.tintShipToType,1)=2 AND OM.numDomainId= " & Session("DomainID")
                                    str = str & " AND OA.numShipState in( " & strFieldValue & ") "
                                    str = str & " ) "
                                ElseIf strFieldName = "numBillState" Then
                                    str = str & " AND oppMas.numOppID in (SELECT numOppID FROM OpportunityMaster Where COALESCE(tintBillToType,1)=0 and numDomainId= " & Session("DomainId") & " AND numDivisionID in (SELECT numRecordID FROM AddressDetails WHERE numDomainID=" & Session("DomainID") & " and tintAddressOf=2 AND tintAddressType=1 and numState in (" & strFieldValue & "))"

                                    str = str & " union "

                                    str = str & " SELECT OM.numOppId FROM OpportunityMaster OM INNER JOIN DivisionMaster DM"
                                    str = str & " ON OM.numDivisionId = DM.numDivisionID WHERE COALESCE(OM.tintBillToType,1)=1 AND OM.numDomainId= " & Session("DomainID") & " AND DM.numDivisionID in "
                                    str = str & " (SELECT numRecordID FROM AddressDetails WHERE numDomainID=" & Session("DomainID") & " and tintAddressOf=2 AND tintAddressType=1 "
                                    str = str & " and numState in (" & strFieldValue & ")) "

                                    str = str & " union "

                                    'WHEN tintBillToType=2
                                    str = str & " SELECT OM.numOppId FROM OpportunityAddress OA INNER JOIN OpportunityMaster OM ON OA.numOppID = OM.numOppId WHERE COALESCE(OM.tintBillToType,1)=2 AND OM.numDomainId= " & Session("DomainID")
                                    str = str & " AND OA.numBillState in( " & strFieldValue & ") "
                                    str = str & " ) "
                                End If
                            End If
                        End If
                    ElseIf ControlType = "checkboxlist" Then
                        If Not String.IsNullOrEmpty(strFieldValue) Then
                            If dr("vcFieldType") = "O" Then
                                str = str & " and OppMas.numOppId in (select RecId from CFW_FLD_Values_Opp where Fld_ID =" & strFieldID & " AND (" & strFieldValue & "))"

                            ElseIf dr("vcFieldType") = "P" Then
                                str = str & " and OppMas.numOppId in (select numOppId from OpportunityItems where numItemCode in (select RecId from CFW_FLD_Values_Item where Fld_ID =" & strFieldID & " AND (" & strFieldValue & ")))"

                            End If
                        End If
                    ElseIf ControlType = "datefield" Then
                        If dr("vcFieldType") = "R" Then
                            If CCommon.ToString(strFieldValue).Length > 0 Then
                                If hdnFilterCriteria.Value.ToLower.IndexOf(strFieldName & "~11") >= 0 Then
                                    str = str & " and " & FormGenericAdvSearch.GetSearchCondition(hdnFilterCriteria.Value, strFieldName, strControlID, strFieldValue, dr("vcFieldDataType"))
                                Else
                                    Dim dt As Date = DateFromFormattedDate(strFieldValue, Session("DateFormat"))
                                    str = str & " and " & strFieldName & " = '" & dt.Year.ToString + "-" + dt.Month.ToString + "-" + dt.Day.ToString + "'"
                                End If
                            End If
                        ElseIf dr("vcFieldType") = "O" Then
                            If CCommon.ToString(strFieldValue).Length > 0 Then
                                str = str & " and OppMas.numOppID in (select RecId from CFW_Fld_Values_Opp where Fld_ID =" & strFieldID & " and " & FormGenericAdvSearch.GetSearchCondition(hdnFilterCriteria.Value, strFieldName, strControlID, strFieldValue, dr("vcFieldDataType"), IsCustomField:=True) & ")"
                            End If
                        End If
                    Else 'EditBox
                        If CType(tblMain.FindControl(strControlID), TextBox).Text <> "" Then
                            If dr("vcFieldType") = "R" Then
                                ''Based On the Look Back Table Sub Queries 
                                If dr("vcLookBackTableName") = "WareHouses" Then
                                    str = str & " and oppMas.numOppId in "
                                    str = str & " (select distinct(OppMas.numOppId)   from WareHouses WareHouse"
                                    str = str & " join WareHouseItems on WareHouseItems.numWareHouseID =WareHouse.numWareHouseID "
                                    str = str & " join Item on Item.numItemCode = WareHouseItems.numItemId"
                                    str = str & " Join OpportunityItems OppItems on Item.numItemCode = OppItems.numItemCode"
                                    str = str & " join OpportunityMaster OppMas on OppItems.numOppId = OppMas.numOppId"
                                    str = str & " where OppMas.numDomainId= " & Session("DomainId")
                                    str = str & " and " & FormGenericAdvSearch.GetSearchCondition(hdnFilterCriteria.Value, strFieldName, strControlID, strFieldValue, dr("vcFieldDataType"), )
                                    str = str & " ) "
                                ElseIf dr("vcLookBackTableName") = "WareHouseItmsDTL" Then
                                    str = str & " and oppMas.numOppId in "
                                    str = str & " (select distinct(OppMas.numOppId) from WareHouseItmsDTL WareHouseItemDTL"
                                    str = str & " join WareHouseItems on WareHouseItemDTL.numWareHouseItemID =WareHouseItems.numWareHouseItemID "
                                    str = str & " join Item on Item.numItemCode = WareHouseItems.numItemId"
                                    str = str & " Join OpportunityItems OppItems on Item.numItemCode = OppItems.numItemCode"
                                    str = str & " join OpportunityMaster OppMas on OppItems.numOppId = OppMas.numOppId "
                                    str = str & " join OppWarehouseSerializedItem OppSerItm on (OppSerItm.numOppId = OppMas.numOppId and WareHouseItemDTL.numWarehouseItmsDTLID= OppSerItm.numWarehouseItmsDTLID) "
                                    str = str & " where OppMas.numDomainId= " & Session("DomainId")
                                    str = str & " and " & FormGenericAdvSearch.GetSearchCondition(hdnFilterCriteria.Value, strFieldName, strControlID, strFieldValue, dr("vcFieldDataType"))
                                    str = str & " ) "
                                ElseIf dr("vcLookBackTableName") = "WareHouseItems" Then
                                    str = str & " and oppMas.numOppId in "
                                    str = str & " (select distinct(OppMas.numOppId)    from WareHouseItems"
                                    str = str & " join Item on Item.numItemCode = WareHouseItems.numItemId   "
                                    str = str & " Join OpportunityItems OppItems on Item.numItemCode = OppItems.numItemCode  "
                                    str = str & " join OpportunityMaster OppMas on OppItems.numOppId = OppMas.numOppId "
                                    str = str & " where OppMas.numDomainId= " & Session("DomainId")
                                    str = str & " and " & FormGenericAdvSearch.GetSearchCondition(hdnFilterCriteria.Value, strFieldName, strControlID, strFieldValue, dr("vcFieldDataType"))
                                    str = str & " ) "
                                ElseIf dr("vcLookBackTableName") = "Item" Then
                                    str = str & " and oppMas.numOppId in "
                                    str = str & " (select distinct(OppItems.numOppId)   from item "
                                    str = str & "Join OpportunityItems OppItems on Item.numItemCode = OppItems.numItemCode AND Item.numDomainID = " & Session("DomainID") & " "
                                    If strFieldName = "vcItemName" Or strFieldName = "vcModelID" Or strFieldName = "vcManufacturer" Then
                                        str = str & " and ( " & FormGenericAdvSearch.GetSearchCondition(hdnFilterCriteria.Value, strFieldName, strControlID, strFieldValue, dr("vcFieldDataType"), TablePrefix:="item.") & " or " & FormGenericAdvSearch.GetSearchCondition(hdnFilterCriteria.Value, strFieldName, strControlID, strFieldValue, dr("vcFieldDataType"), TablePrefix:="OppItems.") & " )"
                                    ElseIf strFieldName = "txtItemDesc" Then
                                        str = str & " and ( " & FormGenericAdvSearch.GetSearchCondition(hdnFilterCriteria.Value, strFieldName, strControlID, strFieldValue, dr("vcFieldDataType"), TablePrefix:="item.") & " or " & FormGenericAdvSearch.GetSearchCondition(hdnFilterCriteria.Value, "vcItemDesc", strControlID, strFieldValue, dr("vcFieldDataType"), TablePrefix:="OppItems.") & " )"
                                    ElseIf strFieldName = "monPrice" Then
                                        str = str & " and " & FormGenericAdvSearch.GetSearchCondition(hdnFilterCriteria.Value, strFieldName, strControlID, strFieldValue, dr("vcFieldDataType"), TablePrefix:="OppItems.")
                                    Else
                                        str = str & " and " & FormGenericAdvSearch.GetSearchCondition(hdnFilterCriteria.Value, strFieldName, strControlID, strFieldValue, dr("vcFieldDataType"), TablePrefix:="item.")
                                    End If
                                    str = str & " ) "
                                Else
                                    Dim Prefix As String = ""
                                    If dr("vcLookBackTableName") = "CompanyInfo" Then
                                        Prefix = "C."
                                    ElseIf dr("vcLookBackTableName") = "AdditionalContactsInformation" Then
                                        Prefix = "ADC."
                                    ElseIf dr("vcLookBackTableName") = "OpportunityMaster" Then
                                        Prefix = "OppMas."
                                    ElseIf dr("vcLookBackTableName") = "OpportunityBizDocs" Then
                                        Prefix = "OpportunityBizDocs."
                                        'ElseIf dr("vcLookBackTableName") = "OpportunityBizDocItems" Then
                                        '    Prefix = "OpportunityBizDocItems."
                                    End If
                                    'If strFieldName = "monDealAmount" And chkAmtSubtotal.Checked And dr("vcLookBackTableName") = "OpportunityMaster" Then
                                    '    str = str & " and oppMas.numOppId in "
                                    '    str = str & "( SELECT OM.numOppId FROM OpportunityMaster OM INNER JOIN OpportunityItems OI ON OM.numOppId = OI.numOppId "
                                    '    str = str & " WHERE numDomainId= " & Session("DomainID")
                                    '    str = str & " GROUP BY OM.numOppId  HAVING SUM(OI.numUnitHour * OI.monPrice) " & FormGenericAdvSearch.GetSearchCondition(hdnFilterCriteria.Value, strFieldName, strControlID, strFieldValue, dr("vcFieldDataType"), TablePrefix:=Prefix).Replace("OppMas.monDealAmount", "")
                                    '    str = str & " )"

                                    If strFieldName = "monDealAmount" And dr("vcLookBackTableName") = "OpportunityBizDocs" Then
                                        str = str & " and " & FormGenericAdvSearch.GetSearchCondition(hdnFilterCriteria.Value, "COALESCE((SELECT SUM(COALESCE(monDealAmount,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = OppMas.numOppId AND COALESCE(bitAuthoritativeBizDocs,0)=1),0)", strControlID, strFieldValue, dr("vcFieldDataType"))
                                    ElseIf strFieldName = "monAmountPaid" And dr("vcLookBackTableName") = "OpportunityBizDocs" Then
                                        str = str & " and " & FormGenericAdvSearch.GetSearchCondition(hdnFilterCriteria.Value, "COALESCE((SELECT SUM(COALESCE(monAmountPaid,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = OppMas.numOppId AND COALESCE(bitAuthoritativeBizDocs,0)=1),0)", strControlID, strFieldValue, dr("vcFieldDataType"))
                                    ElseIf strFieldName = "CalAmount" And dr("vcLookBackTableName") = "OpportunityMaster" Then
                                        str = str & " and " & FormGenericAdvSearch.GetSearchCondition(hdnFilterCriteria.Value, "getdealamount(OppMas.numOppId,timezone('utc', now()),0)", strControlID, strFieldValue, dr("vcFieldDataType"))
                                    ElseIf strFieldName = "vcRefOrderNo" And dr("vcLookBackTableName") = "OpportunityBizDocs" Then
                                        str = str & " and (" & FormGenericAdvSearch.GetSearchCondition(hdnFilterCriteria.Value, strFieldName, strControlID, strFieldValue, dr("vcFieldDataType"), TablePrefix:=Prefix)
                                        str = str & " or " & FormGenericAdvSearch.GetSearchCondition(hdnFilterCriteria.Value, "OppMas.vcOppRefOrderNo", strControlID, strFieldValue, dr("vcFieldDataType")) & ")"
                                    Else
                                        ''Added By Sachin Sadhu||Date:12thJune2014
                                        ''Purpose:To add Functionaly :Search with Notes Field-OpportunityBizDocItems

                                        If strLookBackTable = "OpportunityBizDocItems" And CCommon.ToInteger(ddlBizDocType.SelectedValue) = 1 Then
                                            str = str & " and 1=6"
                                        Else
                                            str = str & " and " & FormGenericAdvSearch.GetSearchCondition(hdnFilterCriteria.Value, strFieldName, strControlID, strFieldValue, dr("vcFieldDataType"), TablePrefix:=Prefix)
                                        End If
                                        'End Of Code
                                    End If
                                End If

                            ElseIf dr("vcFieldType") = "O" Then
                                str = str & " and OppMas.numOppId in (select RecId from CFW_FLD_Values_Opp where Fld_ID =" & strFieldID & " and " & FormGenericAdvSearch.GetSearchCondition(hdnFilterCriteria.Value, strFieldName, strControlID, strFieldValue, dr("vcFieldDataType"), IsCustomField:=True) & ")"

                            ElseIf dr("vcFieldType") = "P" Then
                                str = str & " and OppMas.numOppId in (select numOppId from OpportunityItems where numItemCode in (select RecId from CFW_FLD_Values_Item where Fld_ID =" & strFieldID & " and Fld_Value ilike '%" & strFieldValue & "%' ))"

                            End If
                        End If
                    End If
                Next

                FormGenericAdvSearch.SaveSearchCriteria(hdnSaveSearchCriteria.Value, 15)


                Session("WhereContditionOpp") = str
                Session("SavedSearchCondition") = str
                Session("UserFriendlyQuery") = sbFriendlyQuery.ToString.Trim.TrimEnd(New Char() {"d", "n", "a"})

                Response.Redirect("../Admin/frmItemSearchRes.aspx")
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Private Sub DisplayError(ByVal exception As String)
            Try
                DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = exception
                DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
                DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Focus()
            Catch ex As Exception

            End Try
        End Sub

        Function GetListBoxSelectedValues(ByVal lb As ListBox, ByRef fldText1 As String) As String
            Try
                Dim strFieldValue As String = ""
                Dim fldText As String = ""
                For Each Item As ListItem In lb.Items
                    If Item.Selected Then
                        strFieldValue = strFieldValue & Item.Value & ","
                        fldText = fldText & Item.Text & " or "
                    End If
                Next
                strFieldValue = strFieldValue.TrimEnd(",")
                fldText1 = fldText
                Return strFieldValue
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        ''' <summary>
        ''' returns Date query for saved search
        ''' </summary>
        ''' <param name="strFromDate"></param>
        ''' <param name="strToDate"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Function GetDateQuery(ByVal strFromDate As String, ByVal strToDate As String) As String
            Try
                If chkCreated.Checked And chkModified.Checked Then
                    If strFromDate <> "" And strToDate <> "" Then
                        If rbSlidingDate.Checked Then Session("TimeExpression") = " and ( OppMas.bintCreatedDate between '{FROM}'::TIMESTAMP and '{TO}'::TIMESTAMP or OppMas.bintModifiedDate between '{FROM}' and '{TO}' ) "
                        Return " and ( OppMas.bintCreatedDate between  '" & strFromDate & "'::TIMESTAMP and '" & CDate(strToDate & " 23:59:59") & "'::TIMESTAMP" &
                         " or  OppMas.bintModifiedDate between  '" & strFromDate & "'::TIMESTAMP and '" & CDate(strToDate & " 23:59:59") & "'::TIMESTAMP)"
                    End If
                ElseIf chkCreated.Checked Then
                    If strFromDate <> "" And strToDate <> "" Then
                        If rbSlidingDate.Checked Then Session("TimeExpression") = " and OppMas.bintCreatedDate between '{FROM}'::TIMESTAMP and '{TO}'::TIMESTAMP "
                        Return " and OppMas.bintCreatedDate between  '" & strFromDate & "'::TIMESTAMP and '" & CDate(strToDate & " 23:59:59") & "'::TIMESTAMP"
                    End If
                ElseIf chkModified.Checked Then
                    If strFromDate <> "" And strToDate <> "" Then
                        If rbSlidingDate.Checked Then Session("TimeExpression") = " and OppMas.bintModifiedDate between '{FROM}'::TIMESTAMP and '{TO}'::TIMESTAMP "
                        Return " and OppMas.bintModifiedDate between  '" & strFromDate & "'::TIMESTAMP and '" & CDate(strToDate & " 23:59:59") & "'::TIMESTAMP"
                    End If
                End If
                Return ""
            Catch ex As Exception
                Throw ex
            End Try
        End Function
    </script>
    <style type="text/css">
        .form-inline {
            min-height:40px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="RecordActionPanel" runat="server"
    ClientIDMode="Static">
    <asp:Literal ID="litMessage" runat="server" EnableViewState="False"></asp:Literal>
    <div class="row padbottom10">
        <div class="col-xs-12">
            <div class="pull-left">
                <div class="form-inline">
                    <asp:RadioButtonList ID="rdlReportType" AutoPostBack="False" runat="server" CssClass="normal1"
                        RepeatLayout="Flow" RepeatDirection="Horizontal" Style="display: inline-block;">
                        <asp:ListItem Value="1" Selected="True"><a href="javascript:void(0);" onclick="javascript:document.getElementById('rdlReportType_0').checked=true;OpenSelUser(6)">User</a></asp:ListItem>
                        <asp:ListItem Value="2"><a href="javascript:void(0);" onclick="javascript:document.getElementById('rdlReportType_1').checked=true;OpenSelTeam(6)">Team Selected</a></asp:ListItem>
                        <asp:ListItem Value="3"><a href="javascript:void(0);" onclick="javascript:document.getElementById('rdlReportType_2').checked=true;OpenTerritory(6)">Territory Selected</a></asp:ListItem>
                    </asp:RadioButtonList>
                    &nbsp;&nbsp;
                    <b>Apply amount only from sub-total:</b>
                    <asp:CheckBox Text="" runat="server" ID="chkAmtSubtotal" />
                    &nbsp;&nbsp;
                    <label>Total Progress From</label>
                    <asp:TextBox runat="server" CssClass="form-control" Width="60" ID="txtFromProgress" TabIndex="1"></asp:TextBox>
                    &nbsp;&nbsp;<b>To</b>&nbsp;&nbsp;
                <asp:TextBox runat="server" CssClass="form-control" Width="60" ID="txtToProgress" TabIndex="2"></asp:TextBox>
                    &nbsp;&nbsp;
                    <label>Quantity From</label>
                    <asp:TextBox runat="server" CssClass="form-control" Width="60" ID="txtFromQuantity" TabIndex="3"></asp:TextBox>
                    &nbsp;&nbsp;<b>To</b>&nbsp;&nbsp;
                <asp:TextBox runat="server" CssClass="form-control" Width="60" ID="txttoQuantity" TabIndex="4"></asp:TextBox>
                </div>
            </div>
            <div class="pull-right">
                <asp:LinkButton runat="server" ID="btnSearch" CssClass="btn btn-primary" Text="" OnClientClick="SetUserSearchCriteria();"><i class="fa fa-search"></i>&nbsp;&nbsp;Search</asp:LinkButton>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="DetailPageTitle" runat="server"
    ClientIDMode="Static">
    Opp/Order Search
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="TabsPlaceHolder" runat="server"
    ClientIDMode="Static">
    <div class="row">
        <div class="col-xs-12">
            <asp:ValidationSummary runat="server" ID="valsummery" CssClass="normal4" />
        </div>
    </div>
    <asp:UpdateProgress ID="UpdateProgress" runat="server" ClientIDMode="Static">
        <ProgressTemplate>
            <div class="overlay">
                <div class="overlayContent" style="color: #000; text-align: center; width: 250px; padding: 20px">
                    <i class="fa fa-2x fa-refresh fa-spin"></i>
                    <h3>Processing Request</h3>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <div runat="server" id="tblMain">
        <div class="box box-primary box-solid">
            <div class="box-header with-border">
                <h3 class="box-title">General</h3>

                <div class="box-tools pull-right">
                </div>
                <!-- /.box-tools -->
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="row">
                    <div class="col-sm-12 col-md-6">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="form-group">
                                    <label>Order Type</label>
                                    <asp:DropDownList runat="server" ID="ddlOppType" CssClass="form-control">
                                        <asp:ListItem Text="Sales Order" Value="1" />
                                        <asp:ListItem Text="Purchase Order" Value="2" />
                                        <asp:ListItem Text="Sales Opportunity" Value="3" />
                                        <asp:ListItem Text="Purchase Opportunity" Value="4" />
                                        <asp:ListItem Text="All" Value="0" Selected="True" />
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="form-group">
                                    <label>BizDocs</label>
                                    <asp:DropDownList runat="server" ID="ddlBizDocType" CssClass="form-control">
                                        <asp:ListItem Text="All (Including records without BizDocs)" Value="0" Selected="True" />
                                        <asp:ListItem Text="Only records without BizDocs" Value="1" />
                                        <asp:ListItem Text="Only records not containing Authoritative BizDocs" Value="2" />
                                        <asp:ListItem Text="Only records containing Authoritative BizDocs" Value="3" />
                                        <asp:ListItem Text="Orders containing invoice(s) for some but not all items" Value="4" />
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6">
                        <div class="box box-default box-solid">
                            <div class="box-header with-border">
                                <h3 class="box-title">&nbsp;&nbsp;<asp:CheckBox Text="Created" runat="server" ID="chkCreated" CssClass="signup" Checked="true" />
                                    &nbsp;&nbsp;or&nbsp;&nbsp;
                                    <asp:CheckBox Text="Modified" runat="server" ID="chkModified" CssClass="signup" /></h3>

                                <div class="box-tools pull-right">
                                </div>
                                <!-- /.box-tools -->
                            </div>
                            <div class="box-body">
                                <div class="row">
                                    <div class="col-xs-12">
                                        <div class="form-inline">
                                            <label>
                                                <asp:RadioButton Text="" runat="server" ID="rbFixedDate" CssClass="signup" GroupName="dateGroup" Checked="true" />
                                                From</label>
                                            <BizCalendar:Calendar ID="calFrom" runat="server" />
                                            <label>To</label>
                                            <BizCalendar:Calendar ID="calTo" runat="server" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12">
                                        <div class="form-inline">
                                            <label>
                                                <asp:RadioButton Text="" runat="server" ID="rbSlidingDate" CssClass="signup" GroupName="dateGroup" /></label>
                                            <asp:DropDownList runat="server" ID="ddlDateSpan" CssClass="form-control">
                                                <asp:ListItem Text="Today" Value="1" />
                                                <asp:ListItem Text="Week (Last 7 days)" Value="7" />
                                                <asp:ListItem Text="Month (Last 30 days)" Value="30" />
                                                <asp:ListItem Text="Quarter (Last 90 days)" Value="90" />
                                                <asp:ListItem Text="Last 6 months" Value="180" />
                                                <asp:ListItem Text="This Year" Value="365" />
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <asp:PlaceHolder ID="plhControls" runat="server"></asp:PlaceHolder>
    </div>

    <uc1:frmAdvSearchCriteria ID="frmAdvSearchCriteria1" runat="server" />

    <asp:HiddenField ID="hdnFilterCriteria" runat="server" />
    <asp:HiddenField ID="hdnSaveSearchCriteria" runat="server" />
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="UtilityLinksPanel" runat="server">
    <uc2:frmMySavedSearch ID="frmMySavedSearch1" runat="server" FormID="15" />
</asp:Content>
