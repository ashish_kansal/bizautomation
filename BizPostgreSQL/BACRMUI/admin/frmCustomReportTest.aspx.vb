﻿Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Reports
Public Class frmCustomReportTest
    Inherits BACRMPage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then
                bindGrid()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
    Sub bindGrid()
        Dim objReports As New CustomReports

        objReports.DomainID = txtDomainId.Text

        Dim ds As DataSet = objReports.getCustomReportTest

        gvCustomReports.DataSource = ds.Tables(0)
        gvCustomReports.DataBind()
    End Sub

    Private Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Try
            bindGrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
End Class