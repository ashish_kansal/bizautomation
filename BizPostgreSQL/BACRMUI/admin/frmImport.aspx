﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmImport.aspx.vb" Inherits=".frmImport" Async="true"
    ClientIDMode="Predictable" MasterPageFile="~/common/GridMasterRegular.Master" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Import Wizard</title>
    <script type="text/javascript" src="../javascript/comboClientSide.js"></script>
    <script language="javascript" type="text/javascript">
        $(document).ready(function () {
            InitializeValidation();

            $('#txtFile').change(function () {
                //alert(this.files[0].size + " bytes");
                $('#hdnFileSize').val(this.files[0].size);
            });

            $('#litMessage').fadeIn().delay(20000).fadeOut();
        });

        function checkFileExt(fileSize) {
            var data = txtFile.value;
            var extension = data.substr(data.lastIndexOf('.'));
            if (extension == '.csv' || extension == '.CSV') {
                console.log($('#hdnFileSize').val());
                console.log(fileSize);
                if ($('#hdnFileSize').val() > fileSize) {
                    alert("The file size should not exceed " + (fileSize / (1024 * 1024)) + " MB.");
                    return false;
                }
                else {
                    return true;
                }
            }
            else {
                alert("Please Select a .csv File");
                return false;
            }

        }

        function ToggleClass() {
            $('#divStep2').toggle();
        }

        function SingleSelect(current) {
            if (current.id == 'chkSerializedItem') {
                document.getElementById('chkLotNo').checked = false;
            }
            else if (current.id == 'chkLotNo') {
                document.getElementById('chkSerializedItem').checked = false;
            }
        }

        function OpenConf(ImportType, FormID) {
            window.open("../admin/frmImportRecordConf.aspx?ImportType=" + ImportType + '&FormID=' + FormID + '&InsertUpdateType=' + getInsertUpdateType(), '', 'width=800,height=300,status=no,titlebar=no,scrollbar=yes,resizable=yes,top=110,left=150')
            return false;
        }

        function getInsertUpdateType() {
            var radioButtons = $("[id^=rdbupdate_]");
            for (var x = 0; x < radioButtons.length; x++) {
                if (radioButtons[x].checked) {
                    return radioButtons[x].value;
                }
            }
        }

        function SetSerialNo() {
            $.each($(".lblSerial"), function (i, el) {
                $(this).val(i + 1); // Simply couse the first "prototype" is not counted in the list
                $(this).text(i + 1); // Simply couse the first "prototype" is not counted in the list
            })
        }

        function SetLinkingId(linkingID) {
            $("[id$=hdnItemLinkingID]").val(linkingID);
        }

        function RunPreImportAnalysis() {
            $("[id$=btnPreImportAnalysis]").click();
        }

        function CorrectFieldMapping() {
            $('#divStep2Content #tblParent tr[id="trChildHeader1"], #divStep2Content #tblParent tr[id="trChildHeader2"]').each(function (index, trCurrent) {
                var selectedValue = -1;
                if ($(this).css('background-color') == "rgb(213, 255, 215)") {
                    if ($(this).find("[id$=ddlImportField1]").length > 0) {
                        selectedValue = $(this).find("[id$=ddlImportField1]").val();
                    }
                    else if ($(this).find("[id$=ddlImportField2]").length > 0) {
                        selectedValue = $(this).find("[id$=ddlImportField2]").val();
                    }

                    $('#divStep2Content #tblParent tr[id="trChildHeader1"],#divStep2Content #tblParent tr[id="trChildHeader2"]').each(function (index, trOther) {
                        if (trCurrent != trOther) {
                            if ($(trOther).find("[id$=ddlImportField1]").length > 0) {
                                if ($(trOther).find("[id$=ddlImportField1]").find("option[value='" + selectedValue + "']").length > 0) {
                                    if ($(trOther).find("[id$=ddlImportField1]").val() == selectedValue) {
                                        $(trOther).find("[id$=ddlImportField1]").val("0");
                                        $(trOther).css("background-color", "#efefef");
                                    }

                                    $(trOther).find("[id$=ddlImportField1]").find("option[value='" + selectedValue + "']").remove();
                                }                             
                            }
                            else if ($(trOther).find("[id$=ddlImportField2]").length > 0) {
                                if ($(trOther).find("[id$=ddlImportField2]").find("option[value='" + selectedValue + "']").length > 0) {
                                    if ($(trOther).find("[id$=ddlImportField2]").val() == selectedValue) {
                                        $(trOther).find("[id$=ddlImportField2]").val("0");
                                        $(trOther).css("background-color", "#efefef");
                                    }

                                    $(trOther).find("[id$=ddlImportField2]").find("option[value='" + selectedValue + "']").remove();
                                }
                            }
                        }
                    });
                }
            });
        }
    </script>
    <style>
        .table > tbody > tr > td {
            border-top: 0px solid #ddd !important;
        }

        #rdbupdate > tbody > tr > td {
            background-color: #efefef;
            border: 1px solid #c5c5c5;
            padding: 10px;
            text-align: center;
            width: 33.33%;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server"
    ClientIDMode="Static">
    <asp:HiddenField ID="hdnItemLinkingID" runat="server" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <table width="100%">
        <tr>
            <td>
                <ul id="messagebox" class="errorInfo" style="display: none">
                </ul>
            </td>
        </tr>
        <tr>
            <td class="normal4" align="center">
                <asp:Label ID="litMessage" EnableViewState="false" runat="server"></asp:Label>
            </td>
        </tr>
    </table>
    <table width="100%">
        <tr>
            <td align="right" class="normal1">
                <a href="../admin/frmImportList.aspx" class="hyperlink">Back</a> &nbsp;&nbsp;&nbsp;
                <a href="#" onclick="window.open('http://help.bizautomation.com/?pageurl=frmImportFiles.aspx','Help');" title="help">Trouble importing Items [?]</a>&nbsp;
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server" ClientIDMode="Static">
    Import/Update Wizard
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BizPager" runat="server" ClientIDMode="Static">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="GridPlaceHolder" runat="server"
    ClientIDMode="Static">
    <div id="divMain">
        <asp:HiddenField ID="hdnFileSize" runat="server" />
        <asp:Panel ID="pnlStep1" runat="server">
            <div class="row">
                <div class="col-xs-12">
                    <div class="pull-left">
                        <h5>STEP : 1 Update or Import & Upload Data</h5>
                    </div>
                </div>
            </div>
            <hr style="margin-top: 0px" />
            <div id="divStep1Content">
                <table class="table table-responsive" style="width: 100%;">
                    <tr>
                        <td class="normal1" align="right">What do you want to do ?
                        </td>
                        <td>
                            <asp:RadioButtonList ID="rdbupdate" runat="server" RepeatDirection="Horizontal">
                                <asp:ListItem Selected="True" Value="2">
                                    <table style="border-spacing: 8px;border-collapse:initial">
                                        <tr>
                                            <td style="text-align:right"><img src="../images/ImportRecords.png" title="Import" /></td>
                                            <td style="text-align:left"><b>Import new records</b></td>
                                        </tr>
                                        <tr>
                                            <td colspan="2" style="font-weight: normal;text-align:left;">This selection will use row data from your CSV exclusively for creating new records (no existing records will be updated).</td>
                                        </tr>
                                    </table>
                                </asp:ListItem>
                                <asp:ListItem Value="1">
                                    <table style="border-spacing: 8px;border-collapse:initial">
                                        <tr>
                                            <td style="text-align:right"><img src="../images/UpdateRecords.png" title="Update"/></td>
                                            <td style="text-align:left"><b>UPDATE records</b></td>
                                        </tr>
                                        <tr>
                                            <td colspan="2" style="font-weight: normal;text-align:left;">This selection will use row data from your CSV that contain linking ID values, exclusively for updating existing records (no new records will be imported)</td>
                                        </tr>
                                    </table>
                                </asp:ListItem>
                                <asp:ListItem Value="3">
                                    <table style="border-spacing: 8px;border-collapse:initial">
                                        <tr>
                                            <td style="text-align:right"><img src="../images/ImportRecords.png" title="Import" /><img src="../images/UpdateRecords.png" title="Update"/></td>
                                            <td style="text-align:left"><b>Import & Update</b></td>
                                        </tr>
                                        <tr>
                                            <td colspan="2" style="font-weight: normal;text-align:left;">This selection will update records from CSV row data that has a matching linking ID, and import all other rows as new records.</td>
                                        </tr>
                                    </table>
                                </asp:ListItem>
                            </asp:RadioButtonList>
                        </td>
                    </tr>
                    <tr>
                        <td class="normal1" width="15%" align="right">Select Import/Update Type:
                        </td>
                        <td class="normal1" colspan="2">
                            <asp:DropDownList ID="ddlImport" runat="server" CssClass="form-control {required:true, messages:{required:'Select Import Type first!'}}"
                                AutoPostBack="true">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr id="trOrderImport" runat="server" visible="false">
                        <td></td>
                        <td>
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="form-group">
                                        <label>
                                            <b style="color: red">*</b> Select a Field used to pick items for order 
                                        </label>
                                        <br />
                                        <asp:RadioButtonList ID="rblImportOrderItemIdentifier" runat="server" RepeatDirection="Vertical" RepeatLayout="Flow">
                                            <asp:ListItem Text="Item Name" Value="189"></asp:ListItem>
                                            <asp:ListItem Text="SKU" Value="281"></asp:ListItem>
                                            <asp:ListItem Text="UPC" Value="203"></asp:ListItem>
                                            <asp:ListItem Text="Customer Part#" Value="899"></asp:ListItem>
                                            <asp:ListItem Text="Item ID" Value="211" Selected="True"></asp:ListItem>
                                            <asp:ListItem Text="Vendor Part#" Value="291"></asp:ListItem>
                                        </asp:RadioButtonList>
                                    </div>
                                </div>
                            </div>
                            <div class="row padbottom10">
                                <div class="col-xs-12">
                                    <div class="form-inline">
                                        <b style="color: red">* </b><strong>Select</strong>
                                        <asp:DropDownList ID="ddlNoOfOrders" runat="server" CssClass="form-control" AutoPostBack="true">
                                            <asp:ListItem Text="Generate 1 order from CSV" Value="1"></asp:ListItem>
                                            <asp:ListItem Text="Generate multiple orders from CSV" Value="2"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                            <div class="row" id="divSingleOrder" runat="server" visible="true" style="font-weight: bold">
                                <div class="col-xs-12">
                                    <div class="form-inline padbottom10">
                                        <asp:RadioButton runat="server" ID="rbExistingCustomer" GroupName="ImportSingleOrder" Checked="true" />
                                        <b style="color: red">*</b>Existing Organization
                                        <telerik:RadComboBox AccessKey="C" Width="300" ID="radCmbCompany" DropDownWidth="600px"
                                            Skin="Default" runat="server" AutoPostBack="True" AllowCustomText="True" ShowMoreResultsBox="true" EnableLoadOnDemand="True"
                                            OnClientItemsRequested="OnClientItemsRequestedOrganization" ClientIDMode="Static">
                                            <WebServiceSettings Path="../common/Common.asmx" Method="GetCompanies" />
                                        </telerik:RadComboBox>
                                        <b style="color: red">*</b>Contact
                                        <asp:DropDownList ID="ddlContact" runat="server" Width="150" CssClass="form-control"></asp:DropDownList>
                                    </div>
                                    <div class="form-inline">
                                        <asp:RadioButton runat="server" ID="rbNewCustomer" GroupName="ImportSingleOrder" />
                                        <b style="color: red">*</b>New Organization
                                        <asp:TextBox runat="server" ID="txtNewOrganization" placeholder="Enter Org. Name" CssClass="form-control"></asp:TextBox>
                                        <b style="color: red">*</b>Contact First & Last Name 
                                        <asp:TextBox runat="server" ID="txtFirstName" placeholder="Enter First Name" CssClass="form-control" Width="130"></asp:TextBox>
                                        <asp:TextBox runat="server" ID="txtLastName" placeholder="Enter Last Name" CssClass="form-control" Width="130"></asp:TextBox>
                                        Contact Email Address
                                        <asp:TextBox runat="server" ID="txtEmail" placeholder="Enter Email Address" CssClass="form-control"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="row" id="divMultipleOrders" runat="server" visible="false">
                                <div class="col-xs-12">
                                    <b>Attempt to create orders under existing organizations:</b>
                                    <br />
                                    <div class="form-inline padbottom10">
                                        <asp:RadioButton runat="server" ID="rbMatchUsingOrganizationID" GroupName="ImportMultipleOrder" Checked="true" />
                                        Use the BizAutomation Organization ID (Requires the column is mapped from the CSV column to the ID Field in BizAutomation).
                                    </div>
                                    <div class="form-inline">
                                        <asp:RadioButton runat="server" ID="rbMatchUsingField" GroupName="ImportMultipleOrder" />
                                        When CSV values mapped to the organization name and the following field match:
                                        <asp:DropDownList ID="ddlMatchField" runat="server" CssClass="form-control" Width="200">
                                            <asp:ListItem Text="Assigned To" Value="14" />
                                            <asp:ListItem Text="Industry" Value="28" />
                                            <asp:ListItem Text="Organization ID" Value="539" />
                                            <asp:ListItem Text="Organization Name" Value="3" />
                                            <asp:ListItem Text="Organization Phone" Value="10" />
                                            <asp:ListItem Text="Organization Profile" Value="5" />
                                            <asp:ListItem Text="Organization Status" Value="30" />
                                            <asp:ListItem Text="Relationship (e.g. Customer, Vendor, etc…)" Value="6" />
                                            <asp:ListItem Text="Relationship Type (e.g. Leads, Prospect or Accounts)" Value="451" />
                                            <asp:ListItem Text="Territory" Value="21" />
                                            <asp:ListItem Text="Customer PO#" Value="535" />
                                        </asp:DropDownList>
                                        <br />
                                        <i>BizAutomation will create the order for that organization provided there is only 1 record found in BizAutomation that matches both of those values. If no record or a duplicate is found, the order will be added under a new record using the organization name provided from the CSV.</i>
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr runat="server" id="trAddressType" visible="false">
                        <td class="normal1" align="right">Select Organization/Contact :
                        </td>
                        <td>
                            <asp:RadioButtonList ID="rdbList" runat="server" RepeatDirection="Horizontal">
                                <asp:ListItem Selected="True" Value="1">Contact</asp:ListItem>
                                <asp:ListItem Value="2">Organization</asp:ListItem>
                            </asp:RadioButtonList>
                        </td>
                    </tr>
                    <tr runat="server" id="trOptions" visible="false">
                        <td class="normal1" align="right">Select Item Group :
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlItemGroup" runat="server" CssClass="signup form-control">
                            </asp:DropDownList>
                            &nbsp;&nbsp;&nbsp;
                            <asp:CheckBox ID="chkLotNo" runat="server" onclick="SingleSelect(this);" Text="Lot No" />
                            <asp:CheckBox ID="chkSerializedItem" runat="server" onclick="SingleSelect(this);"
                                Text="Serialize" />
                        </td>
                    </tr>
                    <tr>
                        <td class="normal1" align="right">Download Template :
                        </td>
                        <td class="normal1" colspan="2">
                            <asp:UpdatePanel ID="updatedownload1" runat="server">
                                <ContentTemplate>
                                    <asp:LinkButton ID="lbDownloadCSV" Text="Download CSV Template" runat="server" CssClass="hyperlink btn btn-primary" />
                                    <asp:LinkButton ID="lbDownloadCSVWithData" Text="Download CSV Template With Data"
                                        runat="server" Visible="False" CssClass="hyperlink btn btn-primary" />
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <asp:Button ID="btnConfg" runat="server" Text="Configure" CssClass="button btn btn-primary" Visible="false"
                                UseSubmitBehavior="false"></asp:Button>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:PostBackTrigger ControlID="lbDownloadCSV" />
                                    <asp:PostBackTrigger ControlID="lbDownloadCSVWithData" />
                                    <asp:PostBackTrigger ControlID="btnConfg" />
                                </Triggers>
                            </asp:UpdatePanel>

                        </td>
                    </tr>
                    <tr id="trUpload" runat="server">
                        <td class="normal1" align="right">Source :
                        </td>
                        <td class="normal1" colspan="2">
                            <asp:UpdatePanel ID="uploadpanel2" runat="server">
                                <ContentTemplate>
                                    <input runat="server" id="txtFile" type="file" class="{required:true, messages:{required:'Select .csv file first!'}}" /><br />
                                    <asp:Button runat="server" ID="btnUpload" Text="Upload" CssClass="button btn btn-primary"></asp:Button>
                                    (Upload CSV file with Maximum File Size of <%=ConfigurationManager.AppSettings("MaxUploadFileSize") / 1024%> MB & Maximum <%=ConfigurationManager.AppSettings("MaxFileRecords")%> Rows to view before you save.)
                                </ContentTemplate>
                                <Triggers>
                                    <asp:PostBackTrigger ControlID="btnUpload" />
                                </Triggers>
                            </asp:UpdatePanel>

                        </td>
                    </tr>
                    <tr id="trInfo" runat="server" visible="false">
                        <td colspan="2">
                            <asp:Panel runat="server" ID="Panel1" CssClass="info">
                                <br />
                                <ul>
                                    <li>You can not update OnHand quantity of any inventory item though this wizard. its allowed only for first time when inserting new item in warehouse</li>
                                </ul>
                                <br />
                            </asp:Panel>
                        </td>
                    </tr>
                </table>
            </div>
        </asp:Panel>
        <asp:Panel ID="pnlStep2" runat="server">
            <div class="row">
                <div class="col-xs-12">
                    <div class="pull-left">
                        <h5>STEP : 2 Map Fields</h5>
                    </div>
                    <div class="pull-right">
                        <ul class="list-inline" style="margin-top: 10px;">
                            <li>
                                <span class="badge" style="font-size: 13px; background-color: #d5ffd7; padding: 3px 7px; border-radius: 0px; border: 1px solid #95fb9a;">&nbsp;</span>
                                <label>Match</label>
                            </li>
                            <li>
                                <span class="badge" style="font-size: 13px; background-color: #fffdcd; padding: 3px 7px; border-radius: 0px; border: 1px solid #f5f192;">&nbsp;</span>
                                <label>Fuzzy Match (check for accuracy)</label>
                            </li>
                            <li>
                                <span class="badge" style="font-size: 13px; background-color: #ffd9d9; padding: 3px 7px; border-radius: 0px; border: 1px solid #f99999;">&nbsp;</span>
                                <label>Manual mapping required</label>
                            </li>
                            <li>
                                <span class="badge" style="font-size: 13px; background-color: #efefef; padding: 3px 7px; border-radius: 0px; border: 1px solid #dcdada;">&nbsp;</span>
                                <label>Not mapped or required (will not import)</label>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <hr style="margin-top: 0px" />
            <div id="divStep2Content">
                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td>Map the field names with the appropriate column names of the source file that you
                            import.
                            <br />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div id="toggleDiv" style="width: 100%; overflow: scroll;">
                                <asp:Repeater ID="rptrParent" runat="server">
                                    <HeaderTemplate>
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <div class="pull-left">
                                                    <h3>
                                                        <asp:Label ID="lblMainHeader" runat="server"></asp:Label></h3>
                                                </div>
                                                <div class="pull-right">
                                                    <div class="form-inline" style="margin-top: 20px;">
                                                        <div class="form-group" id="divLinking" runat="server" visible="false">
                                                            <label>Linking ID:</label>
                                                            <asp:DropDownList ID="ddlItemLinking" runat="server" CssClass="form-control" Visible="false">
                                                                <asp:ListItem Text="-- Select One --" Value="0"></asp:ListItem>
                                                                <asp:ListItem Text="Item ID" Value="1"></asp:ListItem>
                                                                <asp:ListItem Text="Item Name" Value="7"></asp:ListItem>
                                                                <asp:ListItem Text="SKU" Value="2"></asp:ListItem>
                                                                <asp:ListItem Text="UPC" Value="3"></asp:ListItem>
                                                                <asp:ListItem Text="ModelID" Value="4"></asp:ListItem>
                                                            </asp:DropDownList>
                                                            <asp:DropDownList ID="ddlOrganizationLinking" runat="server" CssClass="form-control" Visible="false">
                                                                <asp:ListItem Text="-- Select One --" Value="0"></asp:ListItem>
                                                                <asp:ListItem Text="Non Biz Company ID" Value="1"></asp:ListItem>
                                                                <asp:ListItem Text="Organization ID" Value="2"></asp:ListItem>
                                                            </asp:DropDownList>
                                                            <asp:DropDownList ID="ddlContactLinking" runat="server" CssClass="form-control" Visible="false">
                                                                <asp:ListItem Text="-- Select One --" Value="0"></asp:ListItem>
                                                                <asp:ListItem Text="Non Biz Contact ID" Value="1"></asp:ListItem>
                                                                <asp:ListItem Text="Contact ID" Value="2"></asp:ListItem>
                                                                <asp:ListItem Text="Non Biz Company ID" Value="3"></asp:ListItem>
                                                                <asp:ListItem Text="Organization ID" Value="4"></asp:ListItem>
                                                                <asp:ListItem Text="Non Biz Company ID & Non Biz Contact ID" Value="5"></asp:ListItem>
                                                                <asp:ListItem Text="Non Biz Company ID & Contact ID" Value="6"></asp:ListItem>
                                                                <asp:ListItem Text="Organization ID & Non Biz Contact ID" Value="7"></asp:ListItem>
                                                                <asp:ListItem Text="Organization ID & Contact ID" Value="8"></asp:ListItem>
                                                                <asp:ListItem Text="Autocreate Organization using contact first & last name" Value="9"></asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                        <asp:UpdatePanel ID="updatepanel3" runat="server" class="form-group">
                                                            <ContentTemplate>
                                                                <asp:Button ID="btnConfirm" Text="Confirm Mapping" CssClass="button btn btn-primary" UseSubmitBehavior="true"
                                                                    CommandName="Confirm" runat="server" />
                                                            </ContentTemplate>
                                                            <Triggers>
                                                                <asp:PostBackTrigger ControlID="btnConfirm" />
                                                            </Triggers>
                                                        </asp:UpdatePanel>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <h4>
                                            <asp:Label ID="lblHeader" runat="server" Text='<%#Eval("Data") %>'></asp:Label></h4>
                                        <asp:HiddenField ID="hdnID" runat="server" Value='<%#Eval("Id") %>' />
                                        <asp:Table ID="tblParent" runat="server" Width="100%" CssClass="table table-responsive table-bordered">
                                            <asp:TableHeaderRow HorizontalAlign="center">
                                                <asp:TableCell VerticalAlign="Top" Width="50%">
                                                    <asp:Repeater ID="rptrChild1" runat="server" OnItemDataBound="rptrChild1_ItemDataBound">
                                                        <HeaderTemplate>
                                                            <asp:Label ID="lblChildHeader1" runat="server"></asp:Label>
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:Table ID="tblChild1" CssClass="table table-responsive table-bordered" runat="server" GridLines="Both" Width="100%">
                                                                <asp:TableHeaderRow ID="trChildHeader1">
                                                                    <asp:TableCell Width="5%" HorizontalAlign="Center">
                                                                        <%--<asp:Label ID="lblSrNo1" runat="server" Text='<%#Eval("SrNo") %>'></asp:Label>--%>
                                                                        <asp:Label ID="lblSrNo1" CssClass="lblSerial" runat="server"></asp:Label>
                                                                    </asp:TableCell>
                                                                    <asp:TableCell Style="font-weight: bold;" HorizontalAlign="right" Width="38%">
                                                                        <asp:HiddenField ID="hdnFormFieldId1" runat="server" Value='<%#Eval("numFormFieldID") %>' />
                                                                        <asp:Label ID="lblFormFieldName1" runat="server" Text='<%#Eval("vcFormFieldName") %>'></asp:Label>
                                                                        <asp:HiddenField ID="hdnDbFieldName1" runat="server" Value='<%#Eval("vcDbColumnName") %>' />
                                                                        <asp:HiddenField ID="hdnIsCustomField1" runat="server" Value='<%#Eval("IsCustomField") %>' />
                                                                        <asp:Label ID="lblStar1" ForeColor="Red" runat="server" Text='<%#Eval("vcReqString") %>'></asp:Label>
                                                                    </asp:TableCell>
                                                                    <asp:TableCell HorizontalAlign="Center" Width="2%">
                                                                        <asp:Label ID="Label1" runat="server" Text=":"></asp:Label>
                                                                    </asp:TableCell>
                                                                    <asp:TableCell Width="55%" HorizontalAlign="Left">
                                                                        <asp:DropDownList ID="ddlImportField1" runat="server" CssClass="form-control">
                                                                        </asp:DropDownList>
                                                                    </asp:TableCell>
                                                                </asp:TableHeaderRow>
                                                            </asp:Table>
                                                        </ItemTemplate>
                                                    </asp:Repeater>
                                                </asp:TableCell>
                                                <asp:TableCell VerticalAlign="Top" Width="50%">
                                                    <asp:Repeater ID="rptrChild2" runat="server" OnItemDataBound="rptrChild2_ItemDataBound">
                                                        <HeaderTemplate>
                                                            <asp:Label ID="lblChildHeader2" runat="server"></asp:Label>
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:Table ID="tblChild2" runat="server" GridLines="Both" Width="100%" CssClass="table table-responsive table-bordered">
                                                                <asp:TableHeaderRow ID="trChildHeader2" HorizontalAlign="Center">
                                                                    <asp:TableCell Width="5%">
                                                                        <%--<asp:Label ID="lblSrNo2" runat="server" Text='<%#Eval("SrNo") %>'></asp:Label>--%>
                                                                        <asp:Label ID="lblSrNo2" CssClass="lblSerial" runat="server"></asp:Label>
                                                                    </asp:TableCell>
                                                                    <asp:TableCell Style="font-weight: bold;" HorizontalAlign="right" Width="38%">
                                                                        <asp:HiddenField ID="hdnFormFieldId2" runat="server" Value='<%#Eval("numFormFieldID") %>' />
                                                                        <asp:Label ID="lblFormFieldName2" runat="server" Text='<%#Eval("vcFormFieldName") %>'></asp:Label>
                                                                        <asp:HiddenField ID="hdnDbFieldName2" runat="server" Value='<%#Eval("vcDbColumnName") %>' />
                                                                        <asp:HiddenField ID="hdnIsCustomField2" runat="server" Value='<%#Eval("IsCustomField") %>' />
                                                                        <asp:Label ID="lblStar2" runat="server" ForeColor="Red" Text='<%#Eval("vcReqString") %>'></asp:Label>
                                                                    </asp:TableCell>
                                                                    <asp:TableCell HorizontalAlign="Center" Width="2%">
                                                                        <asp:Label ID="Label1" runat="server" Text=":"></asp:Label>
                                                                    </asp:TableCell>
                                                                    <asp:TableCell Width="55%" HorizontalAlign="Left">
                                                                        <asp:DropDownList ID="ddlImportField2" runat="server" CssClass="form-control">
                                                                        </asp:DropDownList>
                                                                    </asp:TableCell>
                                                                </asp:TableHeaderRow>
                                                            </asp:Table>
                                                        </ItemTemplate>
                                                    </asp:Repeater>
                                                </asp:TableCell>
                                            </asp:TableHeaderRow>
                                        </asp:Table>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
            <br />
        </asp:Panel>
        <asp:Panel ID="pnlStep3" runat="server" Width="1280px">
            <table width="100%" class="table table-responsive ">
                <tr>
                    <td class="tblrowHeader">STEP : 3 Import Data Preview
                    </td>
                </tr>
                <tr>
                    <td>Displaying first 500 records to confirm mapped fields.
                            <br />
                    </td>
                </tr>
                <tr>
                    <td>
                        <table align="right" width="100%" class="table table-responsive ">
                            <tr>
                                <td align="right">
                                    <asp:Button runat="server" ID="btnContinue" CssClass="button btn btn-primary" Text="Pre-Import Analysis" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <div style="overflow: scroll; height: 300px">
                <table style="width: 100%;">
                    <tr>
                        <td>
                            <asp:GridView ID="gvPreviewData" AutoGenerateColumns="true" CellPadding="2" CellSpacing="2"
                                runat="server" Width="100%" CssClass="tbl table table-responsive table-bordered" EnableViewState="false">
                                <%--<AlternatingRowStyle CssClass="ais" />
                                <RowStyle CssClass="is" />
                                <HeaderStyle CssClass="hs"></HeaderStyle>--%>
                                <PagerTemplate>
                                </PagerTemplate>
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
            </div>
        </asp:Panel>
        <asp:Panel ID="pnlStep4" runat="server">
            <div class="row">
                <div class="col-xs-12 col-sm-6 col-sm-offset-3">
                    <div class="media">
                        <span class="media-left" style="vertical-align:middle;padding-right: 25px;">
                            <img src="../images/ImportAnalyze.png" style="margin-right:10px" />
                        </span>
                        <div class="media-body">
                            <h3>Analyzing your CSV data……<i>please wait</i>&nbsp;&nbsp;<i class="fa fa-x fa-refresh fa-spin"></i></h3>
                            <br />
                            <p style="font-size:16px;font-style: italic;"><b>Note:</b> If data errors are found, the report will only cite the 1st instance of an error, on the 1st row on which it’s found (indicating that all downstream rows probably have the same error).</p>
                            <p style="font-size:16px;font-style: italic;">This will enable you to use the “Find & Replace” function on your spread-sheet app to fix the error(s) so you can run “Pre-import Analysis” again, and proceed to import your data.</p>
                        </div>
                    </div>
                </div>
            </div>
        </asp:Panel>
        <asp:Panel ID="pnlStep5" runat="server">
            <div class="row">
                <div class="col-xs-12 col-sm-10 col-sm-offset-1">
                    <div class="media">
                        <span class="media-left" style="vertical-align:middle;padding-right: 25px;">
                            <i class="fa fa-exclamation-circle" style="font-size: 50px;color:#ffc107;"></i>
                        </span>
                        <div class="media-body">
                            <b style="font-size: 24px;">Analysis Complete</b><span style="font-size:24px">- Action required !</span>
                            <br />
                            <br />
                            <ol type="1" runat="server" id="olError">

                            </ol>
                            <br />
                            <div class="text-center">
                                <asp:Button ID="btnOK" runat="server" Visible="false" Text="Import Without Analysis" CssClass="button btn btn-primary" />
                            </div>
                            <br />
                            <p style="font-size:14px;font-style: italic;"><b>Note:</b> If data errors are found, the report will only cite the 1st instance of an error, on the 1st row on which it’s found (indicating that all downstream rows probably have the same error).</p>
                            <p style="font-size:14px;font-style: italic;">This will enable you to use the “Find & Replace” function on your spread-sheet app to fix the error(s) so you can run “Pre-import Analysis” again, and proceed to import your data.</p>
                        </div>
                    </div>
                </div>
            </div>
        </asp:Panel>
        <asp:Panel ID="pnlStep6" runat="server">
            <div class="row">
                <div class="col-xs-12 col-sm-6 col-sm-offset-3">
                    <div class="media">
                        <span class="media-left" style="vertical-align: middle;padding-right: 25px;">
                            <i class="fa fa-check-circle" style="font-size: 50px;color: green;"></i>
                        </span>
                        <div class="media-body text-center">
                            <b style="font-size: 24px;">Success !</b><span style="font-size:16px"> - Your CSV seems ready to import. What would you like to do ?</span>
                            <br />
                            <br />
                            <asp:Button ID="btnImport" runat="server" CssClass="btn btn-primary" Text="Import CSV data now" OnClick="btnImport_Click" />
                            <asp:Button ID="btnCancelImport" runat="server" CssClass="btn btn-danger" Text="Cancel" OnClick="btnCancelImport_Click" />
                        </div>
                    </div>
                </div>
            </div>
        </asp:Panel>
    </div>
    <asp:Button Id="btnPreImportAnalysis" runat="server" style="display:none" OnClick="btnPreImportAnalysis_Click" />
</asp:Content>
