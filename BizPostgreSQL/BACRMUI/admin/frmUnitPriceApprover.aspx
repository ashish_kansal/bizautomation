﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/common/Popup.Master" CodeBehind="frmUnitPriceApprover.aspx.vb" Inherits=".frmUnitPriceApprover" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        function SetSelectedUnitPriceApprover(ids) {
            opener.document.getElementById("hdnUnitPriceApprover").value = ids;
            self.close();
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server">
    <div class="input-part">
        <div class="right-input" style="width: 100%; text-align: right">
            <asp:Button ID="btnSaveClose" runat="server" CssClass="button" Text="Save &amp; Close"></asp:Button>
            <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Close" OnClientClick="Close();" CausesValidation="false"></asp:Button>
        </div>
    </div>
    <div style="text-align: center; color: red">
        <asp:Literal ID="litMessage" runat="server" EnableViewState="False"></asp:Literal>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitle" runat="server">
    Unit Price Approver
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Content" runat="server">
    <table width="100%" style="max-width: 800px; max-height: 700px; min-width: 500px">
        <tr>
            <td>
                <asp:CheckBoxList
                    ID="chklUnitPriceApprover"
                    runat="server"
                    RepeatLayout="Table"
                    RepeatColumns="3"
                    RepeatDirection="Vertical">
                </asp:CheckBoxList>
                <asp:HiddenField ID="hdnUnitPriceApprover" runat="server" />
            </td>
        </tr>
    </table>
</asp:Content>
