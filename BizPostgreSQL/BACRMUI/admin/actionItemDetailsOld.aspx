﻿<%@ Page Language="vb" AutoEventWireup="false" ValidateRequest="false" EnableEventValidation="false"
    CodeBehind="actionItemDetailsOld.aspx.vb" Inherits="BACRM.UserInterface.Admin.actionItemDetailsOld"
    MasterPageFile="~/common/DetailPage.Master" %>


<%@ Register TagPrefix="BizCalendar" TagName="Calendar" Src="../include/calandar.ascx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="../common/frmCorrespondence.ascx" TagName="frmCorrespondence" TagPrefix="uc1" %>
<%@ Register Src="../common/frmCollaborationUserControl.ascx" TagName="frmCollaborationUserControl" TagPrefix="uc1" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Src="ActionAttendees.ascx" TagName="ActionAttendees" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <title>Action Item Details</title>
    <meta http-equiv="Page-Enter" content="blendTrans(Duration=0.01)">
    <meta http-equiv="Page-Exit" content="blendTrans(Duration=0.01)">
    <style type="text/css">
        .tblTickler {
            width: 98%;
            margin-left: 1%;
        }

        .mainColumns .col-md-3 {
            width: 15%;
            padding: 0px !important;
            text-align: right;
        }

        .mainColumns .clearfix {
            margin-bottom: 10px !important;
            /*border-bottom:1px solid #e1e1e1;
            padding-bottom:10px;*/
        }

        .customPanel {
            margin-left: -8%;
        }

            .customPanel .col-md-6 {
                padding: 0px !important;
                padding: 0px !important;
                text-align: right;
            }

            .customPanel .col-md-5 {
                padding: 0px !important;
            }

            .customPanel .col-md-7 {
                padding: 0px !important;
            }
        /*.customPanel .pull-left label{
            padding-right:10px;
            width:120px;
        }*/
        span.ValuesClass {
            padding-left: 3px;
        }

        .btn-purple {
            background-color: #EE8E00 !important;
            color: #fff !important;
        }

        .tblTickler > tbody > tr {
            height: 40px;
        }

            .tblTickler > tbody > tr > td:nth-child(2n+1) {
                font-weight: bold;
                text-align: right;
                width: 9%;
                white-space: nowrap;
            }

            .tblTickler > tbody > tr > td:nth-child(2n+2) {
                width: 16%;
                padding-left: 5px;
            }


        .rcbInput {
            height: 19px !important;
        }

        .aorg {
            text-decoration: none !important;
        }

        .btn-task-finish {
            background-color: white;
            color: black;
            border: 1px solid black;
            font-weight: bold;
            padding: 4px;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_pageLoaded(pageLoaded);
        });
        function addRemoveTime() {
            debugger;
            var timeInMinutesId = 2;
            if ($("#chkTimeAdded").prop("checked") == false) {
                timeInMinutesId = 1;
            } else {
                timeInMinutesId = 2;
            }
            $.ajax({
                type: "POST",
                url: '../projects/frmProjects.aspx/WebMethodAddRemoveTimeFromContract',
                contentType: "application/json",
                dataType: "json",
                data: JSON.stringify({
                    "DomainID": <%=Session("DomainID")%>,
                    "DivisionId": $("#<%=hdnDivisionID.ClientID%>").val(),
                    "TaskId": $("#<%=hdnCommunicationID.ClientID%>").val(),
                    "numSecounds": $("#<%=hdnTimeUsedinSec.ClientID%>").val(),
                    "Type": timeInMinutesId,
                    "isProjectActivityEmail": 2
                }),
                beforeSend: function () {
                    $("[id$=UpdateProgress]").show();
                },
                complete: function () {
                    $("[id$=UpdateProgress]").hide();

                },
                success: function (data) {
                    $("[id$=UpdateProgress]").hide();
                    var dataArr = $.parseJSON(data.d);
                    debugger;
                    if (parseInt(dataArr[0]) == 1) {
                        alert("You need to add more time to the contract before attempting to deduct time from this activity / task");
                        $("#chkTimeAdded").removeAttr("checked")
                    } else if (parseInt(dataArr[0]) == 2) {
                        if (confirm("Would you like to send task completion statement including the time it took to complete this task, and how much time is left on the contract ?")) {
                            $("#hdnContractBalanceRemaining").val(parseInt(dataArr[1]));
                            $("#btnOpenEmailContract").click();
                        }
                    }
                    var hours = Math.floor(parseInt(dataArr[1]) / 60 / 60);
                    var minutes = Math.floor(parseInt(dataArr[1]) / 60) - (hours * 60); 
                    if (hours < 10) {
                        hours = "0" + hours;
                    }
                    if (minutes < 10) {
                        minutes = "0" + minutes;
                    }
                    $("#lblTimeLeftHrs").text(hours + ":" + minutes);
                }
            });
        }
        function pageLoaded() {
            $("#btnCustomExpandCollapse").click(function () {
                if ($("#divCustomFieldsBody").css('display') == "none") {
                    $("#hdnCustomFieldExpandCollapse").val(0);
                } else {
                    $("#hdnCustomFieldExpandCollapse").val(1);
                }

                $("#btnCustomFieldsExpandCollapse").click();
            });

            $("#btnAttendeesExpandCollapse").click(function () {
                if ($("#divAttendeesBody").css('display') == "none") {
                    $("#hdnAttendeesExpandCollapse").val(0);
                } else {
                    $("#hdnAttendeesExpandCollapse").val(1);
                }

                $("#btnAttendeesPersistExpandCollapse").click();
            });


            if ($("#hdnCustomFieldExpandCollapse").val() == 1) {
                $('#divCustomFieldsBody').hide();
                $('#btnCustomExpandCollapse').html("<i class='fa fa-plus'></i>");
                $('#divCustomFieldsMain').addClass("collapsed-box");
            } else {
                $('#divCustomFieldsBody').show();
                $('#btnCustomExpandCollapse').html("<i class='fa fa-minus'></i>");
                $('#divCustomFieldsMain').removeClass("collapsed-box");
            }

            if ($("#hdnAttendeesExpandCollapse").val() == 1) {
                $('#divAttendeesBody').hide();
                $('#btnAttendeesExpandCollapse').html("<i class='fa fa-plus'></i>");
                $('#divAttendeesMain').addClass("collapsed-box");
            } else {
                $('#divAttendeesBody').show();
                $('#btnAttendeesExpandCollapse').html("<i class='fa fa-minus'></i>");
                $('#divAttendeesMain').removeClass("collapsed-box");
            }

        }

        /*Function added by Neelam on 10/07/2017 - Added functionality to open Child Record window in modal pop up*/
        function OpenDocumentFiles(divID) {
            $find("<%= RadWinDocumentFiles.ClientID%>").show();
        }

        /*Function added by Neelam on 02/14/2018 - Added functionality to close document modal pop up*/
        function CloseDocumentFiles(mode) {
            debugger;
            var radwindow = $find('<%=RadWinDocumentFiles.ClientID %>');
            radwindow.close();

            if (mode == 'C') { return false; }
            else if (mode == 'L') {
                __doPostBack("<%= btnLinkClose.UniqueID%>", "");
            }
        }

        function DeleteDocumentOrLink(fileId) {
            if (confirm('Are you sure, you want to delete the selected item?')) {
                $('#hdnFileId').val(fileId);
                __doPostBack("<%= btnDeleteDocOrLink.UniqueID%>", "");
            }
            else {
                return false;
            }
        }
        $(document).ready(function ($) {
            requestStart = function (target, arguments) {
                if (arguments.get_eventTarget().indexOf("btnAttach") > -1) {
                    arguments.set_enableAjax(false);
                }
                if (arguments.get_eventTarget().indexOf("btnLinkClose") > -1) {
                    arguments.set_enableAjax(false);
                }
            }
        }
        );
        function fn_Mail(txtMailAddr, a) {
            if (txtMailAddr != '') {
                window.open('../contact/frmComposeWindow.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&LsEmail=' + txtMailAddr + '&pqwRT=' + a, 'ComposeWindow', 'toolbar=no,titlebar=no,top=100,left=100,width=1200,height=645,scrollbars=yes,resizable=yes')
            }

            return false;
        }
        function openCase(a) {
            window.location(a)
        }
        function Close() {
            window.close()
            return false;
        }
        function openTime(a, b, c, d) {
            window.open("../cases/frmCasetime.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&caseId=" + a + "&CntId=" + b + "&DivId=" + c + "&CommId=" + d, '', 'toolbar=no,titlebar=no,left=500, top=300,width=650,height=300,scrollbars=yes,resizable=yes');
            return false;
        }

        function openExp(a, b, c, d) {
            window.open("../cases/frmCaseExpense.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&caseId=" + a + "&CntId=" + b + "&DivId=" + c + "&CommId=" + d, '', 'toolbar=no,titlebar=no,left=500, top=300,width=650,height=300,scrollbars=yes,resizable=yes');
            return false;
        }

        function Loaddata() {
            document.getElementById("btnLoadData").click()
            return false;
        }
        function OpenNew() {
            var bln;
            bln = confirm('If you click "Ok" this action item will be closed and sent to history (leaving an audit trail), and a new follow-up action item will be created for this organization. Would you like to continue ?')
            if (bln == true) {
                return true;
            }
            else {
                return false;
            }
        }

        function openEmpAvailability() {
            var strTime, strStart, strEnd;
            if (document.getElementById("chkAM").checked == true) {
                strStart = 'AM'
            }
            else {
                strStart = 'PM'
            }
            if (document.getElementById("chkEndAM").checked == true) {
                strEnd = 'AM'
            }
            else {
                strEnd = 'PM'
            }

            var comboStart = $find("<%= radcmbStartTime.ClientID%>");
            var comboEnd = $find("<%= radcmbEndTime.ClientID%>");

            strTime = comboStart.get_text() + ' ' + strStart + '-' + comboEnd.get_text() + ' ' + strEnd;
            window.open("/ActionItems/frmEmpAvailability.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&Date=" + $find("<%= radDueDate.ClientID %>").get_selectedDate().format("MM/dd/yyyy") + '&Time=' + strTime, '', 'toolbar=no,titlebar=no,top=0,left=100,width=850,height=500,scrollbars=yes,resizable=yes');
            return false;
        }
        function AssignTo(a, b) {
            document.getElementById("ddlUserNames").value = a;
            document.getElementById("ctl00_TabsPlaceHolder_cal_txtDate").value = b;
            return false;
        }
        function ShowLayout(a, b, d) {
            window.open("../pagelayout/frmCustomisePageLayout.aspx?Ctype=" + a + "&type=" + b + "&PType=3&FormId=" + d, '', 'toolbar=no,titlebar=no,width=800,height=500,top=200,scrollbars=yes,resizable=yes');
            return false;
        }

        function IsNumeric(strString)
        //  check for valid numeric strings	
        {
            var strValidChars = "0123456789.-";
            var strChar;
            var blnResult = true;
            var strString;
            if (strString.length == 0) return true;

            //  test strString consists of valid characters listed above
            for (i = 0; i < strString.length && blnResult == true; i++) {
                strChar = strString.charAt(i);
                if (strValidChars.indexOf(strChar) == -1) {
                    return false;
                }
            }
            return blnResult;
        }

        function Save() {

            if ($("#hdnCommunicationID").val() == "" || $("#hdnCommunicationID").val() == "0") {
                if ($("#hdnContactID").val() != "") {

                } else {
                    if ($find('radCmbCompany').get_value() == "") {
                        alert("Select Company")
                        return false;
                    }
                    if (document.getElementById("ddlContact").selectedIndex == -1) {
                        alert("Select Contact")
                        document.getElementById("ddlContact").focus();
                        return false;
                    }
                    if (document.getElementById("ddlContact").value == 0) {
                        alert("Select Contact")
                        document.getElementById("ddlContact").focus();
                        return false;
                    }
                }
                if ($("#chkFollowUpStatus").prop("checked") == true && $("#ddlUpdateFollowUpStatus option:selected").val() == 0) {
                    alert("Please select Follow Up Status");
                    return false;
                }
                if (document.getElementById("ddlType").value == 0) {
                    alert("Select Action Item Type")
                    document.getElementById("ddlType").focus();
                    return false;
                }
            } else {
                var ddlType = $find("<%= ddlType.ClientID%>");
                if (ddlType.value == 0) {
                    alert("Select Action Item Type");
                    return false;
                }
            }
        }

        function openActionItem(a, b, c, d, e, f) {
            if (e == 'Email') {
                window.open("../outlook/frmMailDtl.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&numEmailHstrID=" + a, '', 'titlebar=no,top=100,left=250,width=850,height=550,scrollbars=yes,resizable=yes');
                return false;
            }
            else {
                window.location.href = "../admin/actionitemdetails.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=prospects&CommId=" + a + "&CaseId=" + b + "&CaseTimeId=" + c + "&CaseExpId=" + d;
                return false;
            }

        }

        function duedateChage(format) {
            var selectedDate = $find("<%= radDueDate.ClientID %>").get_selectedDate().format(format);

            if (!isDate(selectedDate, format)) {
                alert("Enter Valid Due date");
                $("#<%= radDueDate.ClientID %>").focus();
                return false;
            }

            var SDate = new Date(getDateFromFormat(selectedDate, format));

            var ddDueDate = document.getElementById('ddDueDate').value;
            var dateFormat = document.getElementById('hfDateFormat').value;

            if (ddDueDate == 'W') {
                startDate = new Date(SDate.getFullYear(), SDate.getMonth(), SDate.getDate() + 7);
            }
            else if (ddDueDate == 'M') {
                startDate = new Date(SDate.getFullYear(), SDate.getMonth() + 1, SDate.getDate());
            }
            else if (ddDueDate == 'Q') {
                startDate = new Date(SDate.getFullYear(), SDate.getMonth() + 3, SDate.getDate());
            }
            else if (ddDueDate == 'Y') {
                startDate = new Date(SDate.getFullYear() + 1, SDate.getMonth(), SDate.getDate());
            }
            else {
                startDate = new Date(SDate.getFullYear(), SDate.getMonth(), SDate.getDate());
            }

            $find("<%= radDueDate.ClientID %>").set_selectedDate(parseDate(formatDate(startDate, format)));
        }

        function openRecord() {
            var lngOpenRecord = document.getElementById("ddlOpenRecord").value;
            var lngAssignTO = document.getElementById("ddlAssignTO1").value;

            if (lngOpenRecord > 0 && lngAssignTO > 0) {
                if (lngAssignTO == 1 || lngAssignTO == 2
                    || lngAssignTO == 3 || lngAssignTO == 4) { //1:Sales Opportunity 2:Purchase Opportunity 3:Sales Order 4:Purchase Order
                    document.location = '../opportunity/frmOpportunities.aspx?OpID=' + lngOpenRecord;
                }
                else if (lngAssignTO == 5) { //Project
                    document.location = '../projects/frmProjects.aspx?ProId=' + lngOpenRecord;
                }
                else if (lngAssignTO == 6) { //Cases
                    document.location = '../cases/frmCases.aspx?CaseID=' + lngOpenRecord;
                }
                else if (lngAssignTO == 7) { //Item
                    document.location = '../Items/frmKitDetails.aspx?ItemCode=' + lngOpenRecord;
                }
            }

            return false;
        }


        function ShowHideElements(IsHide) {
            if (IsHide == true) {
                jQuery('#tdStartTimeLabel').hide();
                jQuery('#tdStartTimeControl').hide();
                jQuery('#tdEndTimeLabel').hide();
                jQuery('#tdEndTimeControl').hide();
                jQuery('#UpdatePanelReminder').hide();
            }
            else {
                jQuery('#tdStartTimeLabel').show();
                jQuery('#tdStartTimeControl').show();
                jQuery('#tdEndTimeLabel').show();
                jQuery('#tdEndTimeControl').show();
                jQuery('#UpdatePanelReminder').show();
            }
        }

        function OpenDropDown(intvalue) {
            if (intvalue == "1") {
                var combo = $find("<%= radcmbStartTime.ClientID%>");
                combo.showDropDown();
            } else {
                var combo = $find("<%= radcmbEndTime.ClientID%>");
                combo.showDropDown();
            }
        }

        jQuery(document).ready(function () {

            if (parseInt($("#hdnLinkedOrg").val()) > 0) {
                $("#trLinkedOrganization").show();
            }

            ShowHideElements(jQuery('#chkFollowUp').is(':checked'));
            jQuery('#chkFollowUp').on("change", function () {
                if (jQuery(this).is(':checked') == true) {
                    ShowHideElements(true);
                }
                else {
                    ShowHideElements(false);
                }
            });



            //$("#chkPM").change(function () {
            //    if ($(this).is(":checked")) {
            //        $("#chkEndPM").prop('checked', true);
            //    } else {
            //        $("#chkEndPM").prop('checked', false);
            //        $("#chkEndAM").prop('checked', true);
            //    }
            //});

            //$("#chkEndPM").change(function () {
            //    if ($(this).is(":checked")) {
            //        $("#chkPM").prop('checked', true);
            //    } else {
            //        $("#chkEndPM").prop('checked', false);
            //        $("#chkAM").prop('checked', true);
            //    }
            //});

            //$("#chkAM").change(function () {
            //    if ($(this).is(":checked")) {
            //        $("#chkEndAM").prop('checked', true);
            //    } else {
            //        $("#chkEndAM").prop('checked', false);
            //        $("#chkEndPM").prop('checked', true);
            //    }
            //});

            //$("#chkEndAM").change(function () {
            //    if ($(this).is(":checked")) {
            //        $("#chkAM").prop('checked', true);
            //    } else {
            //        $("#chkEndAM").prop('checked', false);
            //        $("#chkPM").prop('checked', true);
            //    }
            //});
        });

        function OnClientSelectedIndexChanged(sender, eventArgs) {
            var selectedValue = eventArgs.get_item().get_value();
            var combo = $find("<%= radcmbEndTime.ClientID%>");

            if (parseInt(selectedValue) == 24) {
                var itm = combo.findItemByValue("1");
                itm.select();
            } else {
                var itm = combo.findItemByValue(parseInt(selectedValue) + 1);
                itm.select();
            }

            combo.showDropDown();
        }

        function OpenActionItemWindow() {
            window.open('../ActionItems/frmActionItemTemplate.aspx?ID=' + $("#listActionItemTemplate").val(), '', 'toolbar=no,titlebar=no,top=100,left=250,width=700,height=350,scrollbars=yes,resizable=yes')
            return false;
        }

        function OpenAddOrganizationWindow() {
            var orgID = "", contactID = "", orgName = "";

            if ($("#hdnLinkedOrg").val() != "") {
                orgID = $("#hdnLinkedOrg").val();
                orgName = $("#lblLinkedOrg").text();
                contactID = $("#hdnLinkedOrgContact").val();
            }

            window.open('../admin/actionitemorganization.aspx?orgID=' + orgID + '&cntID=' + contactID + '&orgName=' + orgName, '', 'toolbar=no,titlebar=no,top=100,left=250,width=700,height=350,scrollbars=yes,resizable=yes')
            return false;
        }

        function AddOrganization(organizationID, organizationName, contactID, contactName, followUpID, email, phone) {
            $("#lblLinkedOrg").text(organizationName);
            $("#hdnLinkedOrg").val(organizationID);

            if (followUpID != "0") {
                $("#ddlFollowUpStatus option").each(function () {
                    if (this.value == followUpID) {
                        $("#lblLinkedOrgFolloUp").text(this.innerText);
                    }
                });
            }

            $("#lblLinkedOrgContact").text(contactName);
            $("#hdnLinkedOrgContact").val(contactID);

            $("#lblLinkedOrgPhone").text(phone);

            $("#imbBtnLinkedOrgEmail").attr("title", email);
            $("#imbBtnLinkedOrgEmail").attr("alt", email);
            $("#imbBtnLinkedOrgEmail").attr("onclick", "return fn_Mail('" + email + "','" + contactID + "')");

            $("#trLinkedOrganization").show();
        }

        function RemoveOrganization() {
            $("#lblLinkedOrg").text("");
            $("#hdnLinkedOrg").val("");

            $("#lblLinkedOrgFolloUp").text("");

            $("#lblLinkedOrgContact").text("");
            $("#hdnLinkedOrgContact").val("");

            $("#lblLinkedOrgPhone").text("");

            $("#imbBtnLinkedOrgEmail").attr("title", "");
            $("#imbBtnLinkedOrgEmail").attr("alt", "");
            $("#imbBtnLinkedOrgEmail").attr("onclick", "");

            $("#trLinkedOrganization").hide();
        }

        function ShowLinkedOrganization() {
            $("#trLinkedOrganization").show();
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="RecordInformationPanel" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="RecordActionPanel" runat="server"
    ClientIDMode="Static">
    <div class="row">
        <div class="col-md-3" style="width: 20%;">
            <div id="lblorganisation" runat="server">
                <div class="callout calloutGroup bg-theme">
                    <div>
                        <asp:Label ID="lblCustomerType" CssClass="customerType" runat="server" Text="Customer"></asp:Label>
                        <span>
                            <u>
                                <asp:LinkButton Style="font-size: 16px !important; font-weight: bold !important; font-style: italic !important"
                                    ID="lnkCompany" runat="server" CssClass="aorg" CommandName="Company" Visible="false" Font-Size="Larger"></asp:LinkButton>

                            </u>
                            <asp:Label ID="lblRelationCustomerType" CssClass="customerType" Text="" runat="server"></asp:Label>
                        </span>


                    </div>
                </div>
                <div class="record-small-box record-small-group-box">
                    <strong>
                        <asp:LinkButton ID="lnkContact" runat="server" Style="font-size: 13px; font-style: initial; text-decoration: none;"
                            CssClass="text-color-theme" CommandName="Contact"></asp:LinkButton>
                    </strong>
                    <span class="contact">
                        <asp:Label ID="lblContactPhone" runat="server" Text=""></asp:Label></span>
                    <a id="btnSendEmail" runat="server" href="#">
                        <img src="../images/msg_unread_small.gif" />
                    </a>
                </div>
            </div>
        </div>
        <div class="col-md-5" id="trRecordOwner" runat="server">
            <div class="record-small-box record-small-group-box createdBySection">

                <a href="#">
                    <label>CREATED</label>
                </a>
                <span class="innerCreated">
                    <asp:Label ID="lblCreatedBy" runat="server"></asp:Label>
                </span>
                <a href="#">
                    <label>OWNER</label>
                </a>
                <span class="innerCreated">
                    <asp:Label ID="lblRecordOwner" runat="server"></asp:Label>
                </span>
                <a href="#">
                    <label>MODIFIED</label>
                </a>
                <span class="innerCreated">
                    <asp:Label ID="lblModifiedBy" runat="server"></asp:Label>
                </span>
            </div>

        </div>
        <div class="col-md-4  pull-right" style="width: 38%;">
            <div class="pull-right" id="divReminder" runat="server">
                <asp:UpdatePanel ID="UpdatePanelReminder" runat="server" UpdateMode="Conditional" style="display: inline">
                    <ContentTemplate>
                        <asp:CheckBox ID="chkOutlook" runat="server" CssClass="text" Text="Add to Calendar "></asp:CheckBox>&nbsp;&nbsp;
                        <asp:CheckBox ID="chkPopupRemainder" runat="server" CssClass="text" Text="Have Popup Remind Assignee Prior to Event" Visible="false"></asp:CheckBox>
                        <asp:CheckBox runat="server" Style="vertical-align: middle;" ID="chkRemindMe" /><label for="chkRemindMe">Remind me&nbsp;
                            <asp:DropDownList runat="server" ID="ddlMinutes" Style="width: 40px!important;">
                                <asp:ListItem Text="5" Value="5"></asp:ListItem>
                                <asp:ListItem Text="10" Value="10"></asp:ListItem>
                                <asp:ListItem Text="15" Value="15"></asp:ListItem>
                                <asp:ListItem Text="20" Value="20"></asp:ListItem>
                                <asp:ListItem Text="30" Value="30"></asp:ListItem>
                                <asp:ListItem Text="45" Value="45"></asp:ListItem>
                                <asp:ListItem Text="60" Value="60"></asp:ListItem>
                            </asp:DropDownList>
                            &nbsp;minutes prior to event</label>&nbsp;
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
            <div class="pull-right createdBySection" style="clear: both">
                <asp:UpdatePanel ID="pnlMain" runat="server" UpdateMode="Always" style="display: inline">
                    <ContentTemplate>
                        <asp:TextBox ID="txtfdate" TabIndex="4" runat="server" Width="50px" Visible="False" AutoPostBack="true" Enabled="False" CssClass="signup"></asp:TextBox>
                        <input class="signup" id="hidcompanyname" type="hidden" size="2" name="hidcompanyname" runat="server" />
                        <asp:Label ID="lblsingleq" runat="server" Font-Size="9px" Font-Names="Verdana" Visible="False" ForeColor="Red">Single Quotes are not allowed</asp:Label>
                        <input type="hidden" id="HidStrEml" runat="server" value="0" name="HidStrEml" />
                        <input id="HidStartTime" type="hidden" value="0" runat="server" name="HidStartTime" />
                        <input id="HidEndTime" type="hidden" value="0" runat="server" name="HidEndTime" />
                        <asp:TextBox ID="txtUTCTime" runat="server" Style="display: none"></asp:TextBox>
                        <asp:TextBox ID="TextBox1" runat="server" Style="display: none"></asp:TextBox>
                        <asp:TextBox runat="server" ID="txtActivityID" Style="display: none" Text="0"></asp:TextBox>
                        <asp:HiddenField ID="hdnCorrespondenceID" runat="server" />
                        <asp:HiddenField ID="hdnContactID" runat="server" />
                        <asp:HiddenField ID="hdnCommunicationID" runat="server" />
                        <asp:HiddenField ID="hdnlngType" runat="server" />
                        <asp:Button ID="btnReloadTempateData" runat="server" Text="" Style="display: none" />
                        <asp:HiddenField ID="hdnRedirectID" runat="server" />
                    </ContentTemplate>
                </asp:UpdatePanel>
                <asp:HiddenField ID="hdnDivisionID" runat="server" />
                &nbsp;&nbsp;&nbsp;
                <asp:CheckBox ID="chkClose" runat="server" CssClass="text checkbox-inline" Text="Close"></asp:CheckBox>&nbsp;&nbsp
                <asp:LinkButton ID="btnFollowup" runat="server" CssClass="btn btn-purple">Create Follow-Up</asp:LinkButton>
                <asp:LinkButton ID="btnFinish" runat="server" CssClass="btn btn-task-finish"><img src="../images/comflag.png" />&nbsp;Finish</asp:LinkButton>
                <asp:LinkButton ID="btnActDelete" runat="server" CssClass="btn btn-danger"><i class="fa fa-trash"></i>&nbsp;&nbsp;Delete</asp:LinkButton>

            </div>
        </div>
    </div>
    <div class="row padbottom10">
        <div>
            <div>
                <div>
                    <div class="form-inline">
                        <div class="form-group">

                            <div id="divTemplate" runat="server">
                                <asp:UpdatePanel ID="UpdatePanelActionTemplate" runat="server" ChildrenAsTriggers="true">
                                    <ContentTemplate>
                                        <div class="form-inline">
                                            <span id="divOrganizations" runat="server">
                                                <label>
                                                    Organization&nbsp;<span style="color: red; font-size: 12px">*</span>
                                                </label>
                                                <telerik:RadComboBox AccessKey="C" ID="radCmbCompany" DropDownWidth="600px"
                                                    OnClientItemsRequested="OnClientItemsRequestedOrganization"
                                                    ShowMoreResultsBox="true"
                                                    Skin="Default" runat="server" AutoPostBack="True" AllowCustomText="True" EnableLoadOnDemand="True" ClientIDMode="Static">
                                                    <WebServiceSettings Path="~/common/Common.asmx" Method="GetCompanies" />
                                                </telerik:RadComboBox>
                                                <label>Contact&nbsp;<span style="color: red; font-size: 12px">*</span></label>
                                                <asp:DropDownList ID="ddlContact" CssClass="form-control" runat="server" AutoPostBack="true">
                                                    <asp:ListItem Value="0">--Select One--</asp:ListItem>
                                                </asp:DropDownList>
                                            </span>
                                            <label id="multiContactText" runat="server" visible="false"></label>
                                            <asp:HiddenField ID="hdnContactIds" runat="server" Value="" />
                                            <asp:HiddenField ID="hdnDivisionIds" runat="server" Value="" />
                                            <label>Action Item Templates</label>
                                            <asp:DropDownList ID="listActionItemTemplate" CssClass="form-control" runat="server" AutoPostBack="true">
                                                <asp:ListItem Value="0">--Select One--</asp:ListItem>
                                            </asp:DropDownList>
                                            <a onclick="return OpenActionItemWindow();" class="btn"><i class="fa fa-plus-circle"></i></a>
                                        </div>
                                        <div class="clearfix" style="clear: both"></div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </div>

                        <div class="form-group" style="display: none">
                            <asp:Label ID="lblPhone" runat="server"></asp:Label>&nbsp;
                                <asp:ImageButton runat="server" Style="vertical-align: middle;" ID="imbBtnEmail" ImageUrl="~/images/Email_22x22.png" />
                        </div>
                        <div id="divAddActivity" runat="server" class="clearfix"></div>
                        <div class="col-md-3" style="width: 20%;">
                            <div class="form-group " id="tdStartTimeLabel" runat="server">
                                <label>Start Time&nbsp;<img src="~/images/Clock-16.gif" alt="" runat="server" /></label>
                                <div id="tdStartTimeControl" runat="server">
                                    <telerik:RadComboBox ID="radcmbStartTime" Style="width: 80px;" runat="server" onmouseover="OpenDropDown('1')" OnClientSelectedIndexChanged="OnClientSelectedIndexChanged">
                                        <Items>
                                            <telerik:RadComboBoxItem Selected="False" Value="23" Text="12:00" />
                                            <telerik:RadComboBoxItem Selected="False" Value="25" Text="12:15" />
                                            <telerik:RadComboBoxItem Selected="False" Value="24" Text="12:30" />
                                            <telerik:RadComboBoxItem Selected="False" Value="27" Text="12:45" />
                                            <telerik:RadComboBoxItem Selected="False" Value="1" Text="1:00" />
                                            <telerik:RadComboBoxItem Selected="False" Value="26" Text="1:15" />
                                            <telerik:RadComboBoxItem Selected="False" Value="2" Text="1:30" />
                                            <telerik:RadComboBoxItem Selected="False" Value="28" Text="1:45" />
                                            <telerik:RadComboBoxItem Selected="False" Value="3" Text="2:00" />
                                            <telerik:RadComboBoxItem Selected="False" Value="29" Text="2:15" />
                                            <telerik:RadComboBoxItem Selected="False" Value="4" Text="2:30" />
                                            <telerik:RadComboBoxItem Selected="False" Value="30" Text="2:45" />
                                            <telerik:RadComboBoxItem Selected="False" Value="5" Text="3:00" />
                                            <telerik:RadComboBoxItem Selected="False" Value="31" Text="3:15" />
                                            <telerik:RadComboBoxItem Selected="False" Value="6" Text="3:30" />
                                            <telerik:RadComboBoxItem Selected="False" Value="32" Text="3:45" />
                                            <telerik:RadComboBoxItem Selected="False" Value="7" Text="4:00" />
                                            <telerik:RadComboBoxItem Selected="False" Value="33" Text="4:15" />
                                            <telerik:RadComboBoxItem Selected="False" Value="8" Text="4:30" />
                                            <telerik:RadComboBoxItem Selected="False" Value="34" Text="4:45" />
                                            <telerik:RadComboBoxItem Selected="False" Value="9" Text="5:00" />
                                            <telerik:RadComboBoxItem Selected="False" Value="35" Text="5:15" />
                                            <telerik:RadComboBoxItem Selected="False" Value="10" Text="5:30" />
                                            <telerik:RadComboBoxItem Selected="False" Value="36" Text="5:45" />
                                            <telerik:RadComboBoxItem Selected="False" Value="11" Text="6:00" />
                                            <telerik:RadComboBoxItem Selected="False" Value="37" Text="6:15" />
                                            <telerik:RadComboBoxItem Selected="False" Value="12" Text="6:30" />
                                            <telerik:RadComboBoxItem Selected="False" Value="38" Text="6:45" />
                                            <telerik:RadComboBoxItem Selected="False" Value="13" Text="7:00" />
                                            <telerik:RadComboBoxItem Selected="False" Value="39" Text="7:15" />
                                            <telerik:RadComboBoxItem Selected="False" Value="14" Text="7:30" />
                                            <telerik:RadComboBoxItem Selected="False" Value="40" Text="7:45" />
                                            <telerik:RadComboBoxItem Selected="true" Value="15" Text="8:00" />
                                            <telerik:RadComboBoxItem Selected="False" Value="41" Text="8:15" />
                                            <telerik:RadComboBoxItem Selected="False" Value="16" Text="8:30" />
                                            <telerik:RadComboBoxItem Selected="False" Value="42" Text="8:45" />
                                            <telerik:RadComboBoxItem Selected="False" Value="17" Text="9:00" />
                                            <telerik:RadComboBoxItem Selected="False" Value="43" Text="9:15" />
                                            <telerik:RadComboBoxItem Selected="False" Value="18" Text="9:30" />
                                            <telerik:RadComboBoxItem Selected="False" Value="44" Text="9:45" />
                                            <telerik:RadComboBoxItem Selected="False" Value="19" Text="10:00" />
                                            <telerik:RadComboBoxItem Selected="False" Value="45" Text="10:15" />
                                            <telerik:RadComboBoxItem Selected="False" Value="20" Text="10:30" />
                                            <telerik:RadComboBoxItem Selected="False" Value="46" Text="10:45" />
                                            <telerik:RadComboBoxItem Selected="False" Value="21" Text="11:00" />
                                            <telerik:RadComboBoxItem Selected="False" Value="47" Text="11:15" />
                                            <telerik:RadComboBoxItem Selected="False" Value="22" Text="11:30" />
                                            <telerik:RadComboBoxItem Selected="False" Value="48" Text="11:45" />
                                        </Items>
                                    </telerik:RadComboBox>
                                    &nbsp;
                                <span class="bold_span" style="vertical-align: middle">AM</span><input id="chkAM" type="radio" value="0" name="AM" runat="server" style="vertical-align: middle" />
                                    <span class="bold_span" style="vertical-align: middle">PM</span><input id="chkPM" type="radio" value="1" name="AM" runat="server" style="vertical-align: middle" />
                                </div>
                            </div>
                            <div class="form-group" id="tdEndTimeLabel" runat="server">
                                <label>
                                    <span class="bold_span">End Time :</span>
                                    <img src="~/images/Clock-16.gif" alt="" runat="server" />
                                </label>
                                <div id="tdEndTimeControl">
                                    <telerik:RadComboBox ID="radcmbEndTime" Style="width: 80px;" runat="server" onmouseover="OpenDropDown('2');">
                                        <Items>
                                            <telerik:RadComboBoxItem Selected="False" Value="23" Text="12:00" />
                                            <telerik:RadComboBoxItem Selected="False" Value="25" Text="12:15" />
                                            <telerik:RadComboBoxItem Selected="False" Value="24" Text="12:30" />
                                            <telerik:RadComboBoxItem Selected="False" Value="27" Text="12:45" />
                                            <telerik:RadComboBoxItem Selected="False" Value="1" Text="1:00" />
                                            <telerik:RadComboBoxItem Selected="False" Value="26" Text="1:15" />
                                            <telerik:RadComboBoxItem Selected="False" Value="2" Text="1:30" />
                                            <telerik:RadComboBoxItem Selected="False" Value="28" Text="1:45" />
                                            <telerik:RadComboBoxItem Selected="False" Value="3" Text="2:00" />
                                            <telerik:RadComboBoxItem Selected="False" Value="29" Text="2:15" />
                                            <telerik:RadComboBoxItem Selected="False" Value="4" Text="2:30" />
                                            <telerik:RadComboBoxItem Selected="False" Value="30" Text="2:45" />
                                            <telerik:RadComboBoxItem Selected="False" Value="5" Text="3:00" />
                                            <telerik:RadComboBoxItem Selected="False" Value="31" Text="3:15" />
                                            <telerik:RadComboBoxItem Selected="False" Value="6" Text="3:30" />
                                            <telerik:RadComboBoxItem Selected="False" Value="32" Text="3:45" />
                                            <telerik:RadComboBoxItem Selected="False" Value="7" Text="4:00" />
                                            <telerik:RadComboBoxItem Selected="False" Value="33" Text="4:15" />
                                            <telerik:RadComboBoxItem Selected="False" Value="8" Text="4:30" />
                                            <telerik:RadComboBoxItem Selected="False" Value="34" Text="4:45" />
                                            <telerik:RadComboBoxItem Selected="False" Value="9" Text="5:00" />
                                            <telerik:RadComboBoxItem Selected="False" Value="35" Text="5:15" />
                                            <telerik:RadComboBoxItem Selected="False" Value="10" Text="5:30" />
                                            <telerik:RadComboBoxItem Selected="False" Value="36" Text="5:45" />
                                            <telerik:RadComboBoxItem Selected="False" Value="11" Text="6:00" />
                                            <telerik:RadComboBoxItem Selected="False" Value="37" Text="6:15" />
                                            <telerik:RadComboBoxItem Selected="False" Value="12" Text="6:30" />
                                            <telerik:RadComboBoxItem Selected="False" Value="38" Text="6:45" />
                                            <telerik:RadComboBoxItem Selected="False" Value="13" Text="7:00" />
                                            <telerik:RadComboBoxItem Selected="False" Value="39" Text="7:15" />
                                            <telerik:RadComboBoxItem Selected="False" Value="14" Text="7:30" />
                                            <telerik:RadComboBoxItem Selected="False" Value="40" Text="7:45" />
                                            <telerik:RadComboBoxItem Selected="False" Value="15" Text="8:00" />
                                            <telerik:RadComboBoxItem Selected="true" Value="41" Text="8:15" />
                                            <telerik:RadComboBoxItem Selected="False" Value="16" Text="8:30" />
                                            <telerik:RadComboBoxItem Selected="False" Value="42" Text="8:45" />
                                            <telerik:RadComboBoxItem Selected="False" Value="17" Text="9:00" />
                                            <telerik:RadComboBoxItem Selected="False" Value="43" Text="9:15" />
                                            <telerik:RadComboBoxItem Selected="False" Value="18" Text="9:30" />
                                            <telerik:RadComboBoxItem Selected="False" Value="44" Text="9:45" />
                                            <telerik:RadComboBoxItem Selected="False" Value="19" Text="10:00" />
                                            <telerik:RadComboBoxItem Selected="False" Value="45" Text="10:15" />
                                            <telerik:RadComboBoxItem Selected="False" Value="20" Text="10:30" />
                                            <telerik:RadComboBoxItem Selected="False" Value="46" Text="10:45" />
                                            <telerik:RadComboBoxItem Selected="False" Value="21" Text="11:00" />
                                            <telerik:RadComboBoxItem Selected="False" Value="47" Text="11:15" />
                                            <telerik:RadComboBoxItem Selected="False" Value="22" Text="11:30" />
                                            <telerik:RadComboBoxItem Selected="False" Value="48" Text="11:45" />
                                        </Items>
                                    </telerik:RadComboBox>
                                    &nbsp;
                                <span class="bold_span" style="vertical-align: middle">AM</span><input style="vertical-align: middle" id="chkEndAM" type="radio" value="0" name="EndAM" runat="server">
                                    <span class="bold_span" style="vertical-align: middle">PM</span><input style="vertical-align: middle" id="chkEndPM" type="radio" value="1" name="EndAM" runat="server">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-5">

                            <div class="form-group">
                                <label id="lblFollowUp" runat="server">
                                    <asp:CheckBox ID="chkFollowUp" runat="server" />Follow-up Any Time</label>
                                <div>
                                    <asp:DropDownList ID="ddDueDate" Width="90" runat="server" class="form-control" Style="vertical-align: middle;">
                                        <asp:ListItem Value="0">Select</asp:ListItem>
                                        <asp:ListItem Value="W">Next Week</asp:ListItem>
                                        <asp:ListItem Value="M">Next Month</asp:ListItem>
                                        <asp:ListItem Value="Q">Next Quarter</asp:ListItem>
                                        <asp:ListItem Value="Y">Next Year</asp:ListItem>
                                    </asp:DropDownList>
                                    <asp:HiddenField ID="hfDateFormat" runat="server" />
                                </div>

                            </div>
                            <div class="form-group">
                                <label>
                                    &nbsp;
                                <asp:CheckBox ID="chkFollowUpStatus" Style="float: left" runat="server" />Change Follow-up Status to:</label>
                                <div>
                                    <asp:DropDownList ID="ddlUpdateFollowUpStatus" CssClass="form-control" Style="float: right; width: 195px;" runat="server"></asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4" style="width: 38%;">

                            <div class="pull-right">
                                <asp:LinkButton ID="btnCancel" runat="server" CssClass="btn btn-primary"><i class="fa fa-times"></i>&nbsp;&nbsp;Close</asp:LinkButton>
                                <asp:LinkButton ID="btnSaveOnly" runat="server" CssClass="btn btn-primary"><i class="fa fa-floppy-o"></i>&nbsp;&nbsp;Save</asp:LinkButton>
                                <asp:LinkButton ID="btnSave" runat="server" CssClass="btn btn-primary"><i class="fa fa-floppy-o"></i>&nbsp;&nbsp;Save &amp; Close</asp:LinkButton>
                                <asp:LinkButton ID="btnSaveNext" runat="server" CssClass="btn btn-primary">Save &amp; Next&nbsp;&nbsp;<i class="fa fa-arrow-right"></i></asp:LinkButton>




                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="DetailPageTitle" runat="server"
    ClientIDMode="Static">
    <asp:Label ID="lblTitle" runat="server" Text=""></asp:Label>&nbsp;<a href="#" onclick="return OpenHelpPopUp('admin/actionitemdetailsold.aspx')"><label class="badge bg-yellow">?</label></a>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="TabsPlaceHolder" runat="server"
    ClientIDMode="Static">
    <asp:UpdateProgress ID="UpdateProgress" runat="server" ClientIDMode="Static">
        <ProgressTemplate>
            <div class="overlay">
                <div class="overlayContent" style="color: #000; text-align: center; width: 250px; padding: 20px">
                    <i class="fa fa-2x fa-refresh fa-spin"></i>
                    <h3>Processing Request</h3>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="UpdatePanelCompany" runat="server" ChildrenAsTriggers="true">
        <ContentTemplate>
            <div class="col-md-8 col-md-offset-2 mainColumns">
                <div class="col-md-6 col-sm-6">
                    <label class="col-md-3">Type: </label>
                    <div class="col-md-9">
                        <asp:DropDownList ID="ddlType" runat="server" CssClass="form-control gridInsideControl"></asp:DropDownList></div>
                </div>
                <div class="col-md-6 col-sm-6">
                    <label class="col-md-3">Assigned to:</label>
                    <div class="col-md-9">
                        <asp:DropDownList ID="ddlUserNames" runat="server" CssClass="form-control gridInsideControl">
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="col-md-6 col-sm-6">
                    <label class="col-md-3">Due Date:</label>
                    <div class="col-md-9">
                        <asp:UpdatePanel ID="UpdatePanel1" runat="server" class="form-group" style="margin-bottom: 0px" UpdateMode="Always">
                            <ContentTemplate>
                                <div class="form-inline">
                                    <BizCalendar:Calendar ID="radDueDate" TabIndex="8" runat="server" ClientIDMode="AutoID" />

                                    <span id="txtFromToTime" runat="server"></span>
                                    <img src="../images/timeIconnnnn.png" runat="server" id="imgTimeIconn" style="height: 25px" />
                                    <asp:CheckBox ID="chkTimeAdded" onclick="addRemoveTime()" runat="server" />
                                    <asp:HiddenField ID="hdnEndDateTime" runat="server" />
                                    <asp:HiddenField ID="hdnTimeUsedinSec" runat="server" />
                                    <asp:HiddenField ID="hdnContractBalanceRemaining" runat="server" />
                                    <asp:HiddenField ID="hdnAssignName" runat="server" />
                                    <asp:HiddenField ID="hdnAssignToForContract" runat="server" />
                                    <asp:HiddenField ID="hdnStartDateTime" runat="server" />
                                    
                                    
                                    
                        <asp:Button ID="btnOpenEmailContract" runat="server" Text="OpenEmailContrac" style="display:none" />
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>
                <div class="col-md-6 col-sm-6">
                    <label class="col-md-3">Priority:</label>
                    <div class="col-md-9">
                        <asp:DropDownList ID="ddlStatus" runat="server" CssClass="form-control gridInsideControl">
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="clearfix"></div>

                <div class="col-md-6 col-sm-6">
                    <label class="col-md-3">Comments:</label>
                    <div class="col-md-9">
                        <asp:UpdatePanel ID="UpdatePanelComment" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <asp:TextBox ID="txtcomments" TabIndex="6" runat="server" Height="100" CssClass="form-control gridInsideControl" TextMode="MultiLine" Wrap="true" MaxLength="300"></asp:TextBox>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>
                <div class="col-md-6 col-sm-6">
                    <label class="col-md-3">Activity:</label>
                    <div class="col-md-9">
                        <asp:DropDownList ID="ddlActivity" runat="server" CssClass="form-control gridInsideControl">
                        </asp:DropDownList>
                    </div>
                    <div class="customPanel " id="divCustomFields" runat="server">
                        <asp:PlaceHolder ID="plhCustomFields" runat="server"></asp:PlaceHolder>
                    </div>
                </div>
                <div class="clearfix"></div>

            </div>
            <div class="clearfix"></div>
            <%--<asp:Table ID="tblMain" CellPadding="3" CellSpacing="3" align="center" Width="100%" GridLines="none" border="0" runat="server" CssClass="table table-responsive tblNoBorder">
                <asp:TableRow>
                    <asp:TableCell Width="18%" Style="border: 0px;"></asp:TableCell>
                    <asp:TableCell Width="30%" Style="border: 0px;"></asp:TableCell>
                    <asp:TableCell Width="7%" Style="border: 0px;"></asp:TableCell>
                    <asp:TableCell Width="30%" Style="border: 0px;"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                      <asp:TableCell class="normal1Right" align="right" Style="font-weight: bold;">Priority:</asp:TableCell>
                    <asp:TableCell ID="tdPriority" Visible="false"></asp:TableCell>
                    <asp:TableCell>
                       
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell class="normal1Right" align="right" Style="font-weight: bold;">Activity:</asp:TableCell>
                    <asp:TableCell ID="tdActivity" Visible="false"></asp:TableCell>
                    <asp:TableCell>
                     
                    </asp:TableCell>
                          <asp:TableCell class="normal1Right" align="right" Style="font-weight: bold;">Comments:</asp:TableCell>
                    <asp:TableCell ID="tdComments" Visible="false"></asp:TableCell>
                    <asp:TableCell>
                        
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow Style="border-bottom: 1px solid #e1e1e1;">

              

                    <asp:TableCell ColumnSpan="4" CssClass="customPanel">
                       
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>--%>
            <asp:UpdatePanel ID="UpdatePanelAttendees" runat="server">
                <ContentTemplate>
                    <telerik:RadTabStrip ID="radOppTab" runat="server" UnSelectChildren="True" EnableEmbeddedSkins="true"
                        Skin="Default" ClickSelectedTab="True" OnTabClick="radOppTab_TabClick" AutoPostBack="true" SelectedIndex="0"
                        MultiPageID="radMultiPage_OppTab" ClientIDMode="Static">
                        <Tabs>
                            <telerik:RadTab Text="Associated Contacts" Value="AssociatedContacts" PageViewID="radPageView_AssociatedContacts">
                            </telerik:RadTab>
                            <telerik:RadTab Text="Correspondence" Value="Correspondence" PageViewID="radPageView_Correspondence">
                            </telerik:RadTab>
                            <telerik:RadTab Text="Collaboration" Value="Collaboration" PageViewID="radPageView_Collaboration">
                            </telerik:RadTab>
                        </Tabs>
                    </telerik:RadTabStrip>
                    <telerik:RadMultiPage ID="radMultiPage_OppTab" runat="server" CssClass="pageView">
                        <telerik:RadPageView ID="radPageView_AssociatedContacts" runat="server">

                            <div class="row" id="divAttendeesFields" runat="server">
                                <div class="col-sm-12">
                                    <uc2:ActionAttendees ID="ActionAttendees1" runat="server" AttendeesFor="1" />
                                </div>
                            </div>
                        </telerik:RadPageView>
                        
                        
                        <telerik:RadPageView ID="radPageView_Correspondence" runat="server">
                            <uc1:frmCorrespondence ID="Correspondence1" runat="server" />
                        </telerik:RadPageView>
                        <telerik:RadPageView ID="radPageView_Collaboration" runat="server">
                            <uc1:frmCollaborationUserControl ID="frmCollaborationUserControl" runat="server" />
                        </telerik:RadPageView>
                         
                    </telerik:RadMultiPage>
                </ContentTemplate>
            </asp:UpdatePanel>
            <div class="row" style="display: none">
                <div class="col-sm-6 col-md-2">
                    <div class="form-group">
                        <label>Type&nbsp;<span style="color: red; font-size: 12px">*</span></label>

                    </div>
                </div>
                <div class="col-sm-6 col-md-2">
                    <div class="form-group" id="DivAssignTo" runat="server">
                        <label>Assign To</label>
                        <asp:HyperLink ID="hplEmpAvaliability" runat="server" CssClass="pull-right" NavigateUrl="#">Availability</asp:HyperLink>

                    </div>
                </div>
                <div class="col-sm-6 col-md-2" id="divPriority" runat="server">
                    <div class="form-group">
                        <label>Priority</label>

                    </div>
                    <div class="form-inline">

                        <%-- <div class="form-group">

                            <label>Due Date</label>
                            <div>
                                <telerik:RadDatePicker CssClass="" ID="radDueDate" runat="server"></telerik:RadDatePicker>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>&nbsp;</label>
                            &nbsp;
                                <div>
                                    <asp:DropDownList ID="ddDueDate" Width="90" runat="server" class="form-control" Style="vertical-align: middle;">
                                        <asp:ListItem Value="0">Select</asp:ListItem>
                                        <asp:ListItem Value="W">Next Week</asp:ListItem>
                                        <asp:ListItem Value="M">Next Month</asp:ListItem>
                                        <asp:ListItem Value="Q">Next Quarter</asp:ListItem>
                                        <asp:ListItem Value="Y">Next Year</asp:ListItem>
                                    </asp:DropDownList>
                                    <asp:HiddenField ID="hfDateFormat" runat="server" />
                                </div>
                        </div>--%>
                    </div>
                </div>
                <div class="col-sm-6 col-md-2" id="divActivity" runat="server">
                    <div class="form-group">
                        <label>Activity</label>

                    </div>
                    <%--<div class="form-group">
                        <label>Follow-up Any Time</label>
                        <div>
                            <asp:CheckBox ID="chkFollowUp" runat="server" />
                        </div>
                    </div>--%>
                </div>
                <div class="col-sm-6 col-md-3" id="divFollowupStatus" runat="server" style="display: none">
                    <div class="form-group">
                        <label>Follow-up Status</label>
                        <asp:DropDownList ID="ddlFollowUpStatus" runat="server" CssClass="form-control">
                        </asp:DropDownList>
                    </div>
                </div>


            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <div class="row" id="trLinkedOrganization" style="display: none">
        <div class="col-sm-6 col-md-2">
            <div class="form-group">
                <label>Organization&nbsp;<span style="color: red; font-size: 12px">*</span></label>
                <div>
                    <asp:Label ID="lblLinkedOrg" runat="server"></asp:Label>
                    <asp:HiddenField ID="hdnLinkedOrg" runat="server" />
                    <asp:HiddenField ID="hdnOrgLinked" runat="server" />
                </div>
            </div>
        </div>
        <div class="col-sm-6 col-md-2">
            <div class="form-group">
                <label>Follow-up Status</label>
                <div>
                    <asp:Label ID="lblLinkedOrgFolloUp" runat="server" Text=""></asp:Label>
                </div>
            </div>
        </div>
        <div class="col-sm-6 col-md-3">
            <div class="form-group">
                <label>Contact&nbsp;<span style="color: red; font-size: 12px">*</span></label>
                <div>
                    <asp:Label ID="lblLinkedOrgContact" runat="server"></asp:Label>
                    <asp:HiddenField ID="hdnLinkedOrgContact" runat="server" />
                </div>
            </div>
        </div>
        <div class="col-sm-6 col-md-3">
            <div class="form-group">
                <label>Phone, Email</label>
                <div>
                    <asp:Label ID="lblLinkedOrgPhone" runat="server" CssClass="text"></asp:Label>&nbsp;
                    <asp:ImageButton runat="server" Style="vertical-align: middle;" ID="imbBtnLinkedOrgEmail" CssClass="hyperlink" ImageUrl="~/images/Email_22x22.png" />
                </div>
            </div>
        </div>
    </div>
    <asp:UpdatePanel ID="UpdatePanelTicklerDetail" runat="server" UpdateMode="Conditional" style="display: none">
        <ContentTemplate>
            <div class="row">
                <div class="col-sm-6 col-md-2">
                    <%--<div class="form-group">
                        <label>Priority</label>
                        <asp:DropDownList ID="ddlStatus" runat="server" CssClass="form-control">
                        </asp:DropDownList>
                    </div>--%>
                </div>
                <div class="col-sm-6 col-md-2">
                    <%-- <div class="form-group">
                        <label>Activity</label>
                        <asp:DropDownList ID="ddlActivity" runat="server" CssClass="form-control">
                        </asp:DropDownList>
                    </div>--%>
                </div>
                <div class="col-sm-6 col-md-3">
                    <%--  <div class="form-group" id="tdStartTimeLabel">
                        <label>Start Time&nbsp;<img src="~/images/Clock-16.gif" alt="" runat="server" /></label>
                        <div id="tdStartTimeControl">
                            <telerik:RadComboBox ID="radcmbStartTime" runat="server" onmouseover="OpenDropDown('1')" OnClientSelectedIndexChanged="OnClientSelectedIndexChanged">
                                <Items>
                                    <telerik:RadComboBoxItem Selected="False" Value="23" Text="12:00" />
                                    <telerik:RadComboBoxItem Selected="False" Value="24" Text="12:30" />
                                    <telerik:RadComboBoxItem Selected="False" Value="1" Text="1:00" />
                                    <telerik:RadComboBoxItem Selected="False" Value="2" Text="1:30" />
                                    <telerik:RadComboBoxItem Selected="False" Value="3" Text="2:00" />
                                    <telerik:RadComboBoxItem Selected="False" Value="4" Text="2:30" />
                                    <telerik:RadComboBoxItem Selected="False" Value="5" Text="3:00" />
                                    <telerik:RadComboBoxItem Selected="False" Value="6" Text="3:30" />
                                    <telerik:RadComboBoxItem Selected="False" Value="7" Text="4:00" />
                                    <telerik:RadComboBoxItem Selected="False" Value="8" Text="4:30" />
                                    <telerik:RadComboBoxItem Selected="False" Value="9" Text="5:00" />
                                    <telerik:RadComboBoxItem Selected="False" Value="10" Text="5:30" />
                                    <telerik:RadComboBoxItem Selected="False" Value="11" Text="6:00" />
                                    <telerik:RadComboBoxItem Selected="False" Value="12" Text="6:30" />
                                    <telerik:RadComboBoxItem Selected="False" Value="13" Text="7:00" />
                                    <telerik:RadComboBoxItem Selected="False" Value="14" Text="7:30" />
                                    <telerik:RadComboBoxItem Selected="true" Value="15" Text="8:00" />
                                    <telerik:RadComboBoxItem Selected="False" Value="16" Text="8:30" />
                                    <telerik:RadComboBoxItem Selected="False" Value="17" Text="9:00" />
                                    <telerik:RadComboBoxItem Selected="False" Value="18" Text="9:30" />
                                    <telerik:RadComboBoxItem Selected="False" Value="19" Text="10:00" />
                                    <telerik:RadComboBoxItem Selected="False" Value="20" Text="10:30" />
                                    <telerik:RadComboBoxItem Selected="False" Value="21" Text="11:00" />
                                    <telerik:RadComboBoxItem Selected="False" Value="22" Text="11:30" />
                                </Items>
                            </telerik:RadComboBox>
                            &nbsp;
                                <span class="bold_span" style="vertical-align: middle">AM</span><input id="chkAM" type="radio" value="0" name="AM" runat="server" style="vertical-align: middle" />
                            <span class="bold_span" style="vertical-align: middle">PM</span><input id="chkPM" type="radio" value="1" name="AM" runat="server" style="vertical-align: middle" />
                        </div>
                    </div>--%>
                </div>
                <div class="col-sm-6 col-md-3">
                    <%--<div class="form-group" id="tdEndTimeLabel">
                        <label>
                            <span class="bold_span">End Time :</span>
                            <img src="~/images/Clock-16.gif" alt="" runat="server" />
                        </label>
                        <div id="tdEndTimeControl">
                            <telerik:RadComboBox ID="radcmbEndTime" runat="server" onmouseover="OpenDropDown('2');">
                                <Items>
                                    <telerik:RadComboBoxItem Selected="False" Value="23" Text="12:00" />
                                    <telerik:RadComboBoxItem Selected="False" Value="24" Text="12:30" />
                                    <telerik:RadComboBoxItem Selected="False" Value="1" Text="1:00" />
                                    <telerik:RadComboBoxItem Selected="False" Value="2" Text="1:30" />
                                    <telerik:RadComboBoxItem Selected="False" Value="3" Text="2:00" />
                                    <telerik:RadComboBoxItem Selected="False" Value="4" Text="2:30" />
                                    <telerik:RadComboBoxItem Selected="False" Value="5" Text="3:00" />
                                    <telerik:RadComboBoxItem Selected="False" Value="6" Text="3:30" />
                                    <telerik:RadComboBoxItem Selected="False" Value="7" Text="4:00" />
                                    <telerik:RadComboBoxItem Selected="False" Value="8" Text="4:30" />
                                    <telerik:RadComboBoxItem Selected="False" Value="9" Text="5:00" />
                                    <telerik:RadComboBoxItem Selected="False" Value="10" Text="5:30" />
                                    <telerik:RadComboBoxItem Selected="False" Value="11" Text="6:00" />
                                    <telerik:RadComboBoxItem Selected="False" Value="12" Text="6:30" />
                                    <telerik:RadComboBoxItem Selected="False" Value="13" Text="7:00" />
                                    <telerik:RadComboBoxItem Selected="False" Value="14" Text="7:30" />
                                    <telerik:RadComboBoxItem Selected="False" Value="15" Text="8:00" />
                                    <telerik:RadComboBoxItem Selected="true" Value="16" Text="8:30" />
                                    <telerik:RadComboBoxItem Selected="False" Value="17" Text="9:00" />
                                    <telerik:RadComboBoxItem Selected="False" Value="18" Text="9:30" />
                                    <telerik:RadComboBoxItem Selected="False" Value="19" Text="10:00" />
                                    <telerik:RadComboBoxItem Selected="False" Value="20" Text="10:30" />
                                    <telerik:RadComboBoxItem Selected="False" Value="21" Text="11:00" />
                                    <telerik:RadComboBoxItem Selected="False" Value="22" Text="11:30" />
                                </Items>
                            </telerik:RadComboBox>
                            &nbsp;
                                <span class="bold_span" style="vertical-align: middle">AM</span><input style="vertical-align: middle" id="chkEndAM" type="radio" value="0" name="EndAM" runat="server">
                            <span class="bold_span" style="vertical-align: middle">PM</span><input style="vertical-align: middle" id="chkEndPM" type="radio" value="1" name="EndAM" runat="server">
                        </div>
                    </div>--%>
                </div>
            </div>
            <asp:Button ID="btnCustomFieldsExpandCollapse" runat="server" Style="display: none;" />
            <asp:Button ID="btnAttendeesPersistExpandCollapse" runat="server" Style="display: none;" />
            <asp:HiddenField ID="hdnCustomFieldExpandCollapse" runat="server" />
            <asp:HiddenField ID="hdnAttendeesExpandCollapse" runat="server" />
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel ID="UpdatePanelCustomFields" runat="server" UpdateMode="Always" style="display: none">
        <ContentTemplate>
            <div class="row">
                <div class="col-sm-12 col-md-12">
                    <div class="box box-default box-solid" id="divCustomFieldsMain">
                        <div class="box-header with-border">
                            <h3 class="box-title">Custom Fields</h3>

                            <div class="box-tools pull-right">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse" id="btnCustomExpandCollapse">
                                    <i class="fa fa-minus"></i>
                                </button>
                            </div>
                            <!-- /.box-tools -->
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body" id="divCustomFieldsBody">
                        </div>

                    </div>
                </div>
            </div>

        </ContentTemplate>
    </asp:UpdatePanel>

    <div class="row" style="display: none">
        <div class="col-sm-12 col-md-5">
            <div class="form-group">
                <label>Comments</label>

            </div>
            <div class="form-group">
                <table width="100%" border="0" style="padding-right: 5px;" id="divImageUploadArea" runat="server" visible="false">
                    <tr>
                        <td>
                            <asp:ImageButton ID="imgOpenDocument" runat="server" ImageUrl="~/images/icons/drawer.png" Style="height: 30px;" />
                        </td>
                        <td style="text-align: left;">
                            <asp:HyperLink ID="hplOpenDocument" runat="server" Font-Bold="true" Font-Underline="true" Text="Attach or Link" Style="display: inline-block; vertical-align: top; line-height: normal;"></asp:HyperLink>
                        </td>
                    </tr>
                    <tr>
                        <td style="vertical-align: top;">
                            <img id="imgDocument" runat="server" src="../images/icons/AttachSmall.png" alt="" />
                        </td>
                        <td style="text-align: left;" runat="server" id="tdDocument"></td>
                    </tr>
                    <tr>
                        <td style="vertical-align: top;">
                            <img src="../images/icons/LinkSmall.png" id="imgLink" runat="server">
                        </td>
                        <td style="text-align: left;" runat="server" id="tdLink"></td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="col-sm-12 col-md-7">
            <div class="form-group">
                <label>Recent Correspondence</label>

            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="UtilityLinksPanel" runat="server">
    <i id="lblTimeLeftLabel" runat="server">Time Left</i>
    <label class="lblHeighlight" id="lblTimeLeftHrs" runat="server">0</label>
    <asp:LinkButton ID="btnLayout" runat="server" CssClass="btn btn-default btn-sm" ToolTip="Layout"><i class="fa fa-columns"></i>&nbsp;&nbsp;Layout</asp:LinkButton>
    <telerik:RadWindow RenderMode="Lightweight" runat="server" ID="RadWinDocumentFiles" Title="Documents / Files" RestrictionZoneID="ContentTemplateZone" Modal="true" Behaviors="Resize,Close,Move" Width="800" Height="400">
        <ContentTemplate>
            <asp:UpdatePanel ID="Updatepanel2" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <div>
                        <table border="0" style="width: 100%; text-align: right; padding-right: 5px;">
                            <tr>
                                <td>
                                    <input type="button" id="btnCloseDocumentFiles" onclick="return CloseDocumentFiles('C');" class="btn btn-primary" style="color: white" value="Close" /></td>
                            </tr>
                        </table>
                        <div class="col-xs-12" style="padding-top: 40px;">
                            <div class="form-inline">
                                <table border="0" style="width: 100%;">
                                    <tr>
                                        <td>
                                            <img src="../images/icons/Attach.png"></td>
                                        <td style="font-size: small; font-weight: bold; padding-left: 2px; padding-right: 2px;">Upload a Document from your Desktop</td>
                                        <td colspan="2">
                                            <telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server" ClientEvents-OnRequestStart="requestStart" LoadingPanelID="LoadingPanel1">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <%--<telerik:RadAsyncUpload RenderMode="Lightweight" runat="server" Width="320px" CssClass="async-attachment" ID="rdUploadFile" HideFileInput="true" Localization-Select="Choose File" AutoAddFileInputs="false" />--%>
                                                            <input class="signup" id="fileupload" type="file" name="fileupload" runat="server">
                                                        </td>
                                                        <td>
                                                            <asp:Button ID="btnAttach" OnClick="btnAttach_Click" runat="server" Text="Attach & Close" CssClass="btn btn-primary"></asp:Button>
                                                            <asp:Button ID="btnLinkClose" OnClick="btnLinkClose_Click" runat="server" CssClass="hidden"></asp:Button></td>
                                                    </tr>
                                                </table>
                                            </telerik:RadAjaxPanel>
                                            <telerik:RadAjaxLoadingPanel ID="LoadingPanel1" runat="server" InitialDelayTime="0" Skin="Default">
                                            </telerik:RadAjaxLoadingPanel>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <img src="../images/icons/Link.png">&nbsp;</td>
                                        <td style="font-size: small; font-weight: bold; padding-left: 2px; padding-right: 2px;">Link to a Document</td>
                                        <td>
                                            <telerik:RadTextBox runat="server" ID="txtLinkName" CssClass="inlineFormInput" Width="250px" EmptyMessage="Link Name"></telerik:RadTextBox></td>
                                        <td>
                                            <asp:Button ID="btnLink" runat="server" OnClientClick="return CloseDocumentFiles('L');" Text="Link & Close" CssClass="btn btn-primary"></asp:Button></td>
                                        <td>
                                            <asp:Label ID="Label1" Text="[?]" CssClass="tip" runat="server" ToolTip="Linking to a file on Google Drive or any other file repository is a good way to preserve your BizAutomation storage space. 
To avoid login credentials when clicking the  link, just make it public (e.g. On Google Drive you have to make sure that “Link Sharing” is set to “Public on the web”)." /></td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;&nbsp;</td>
                                    </tr>
                                    <tr style="text-align: left;">
                                        <td colspan="2"></td>
                                        <td colspan="2">
                                            <telerik:RadTextBox RenderMode="Lightweight" runat="server" ID="txtLinkURL" Width="200px" EmptyMessage="Paste link here" TextMode="MultiLine" Height="100px" Resize="None"></telerik:RadTextBox>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </ContentTemplate>
    </telerik:RadWindow>

    <asp:Button ID="btnDeleteDocOrLink" OnClick="btnDeleteDocOrLink_Click" runat="server" CssClass="hidden"></asp:Button>
    <asp:HiddenField runat="server" ID="hdnFileId" />
</asp:Content>
