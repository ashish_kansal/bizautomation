Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Leads
Imports System.IO
Namespace BACRM.UserInterface.Admin

    Partial Public Class frmImportRecordConf
        Inherits BACRMPage

        Dim intFormId As Integer
        Dim sImportModule As Short
        Dim sInsertUpdateType As Short '1: Update, 2:Insert, 3:Insert or Update

        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            Try
                lblException.Text = ""
                DomainID = Session("DomainID")
                UserCntID = Session("UserContactID")

                btnAdd.Attributes.Add("OnClick", "return move(document.getElementById('lstAvailablefld'),document.getElementById('lstAddfld'))")
                btnRemove.Attributes.Add("OnClick", "return remove1(document.getElementById('lstAddfld'),document.getElementById('lstAvailablefld'))")

                btnSave.Attributes.Add("OnClick", "return Save()")
                btnSaveClose.Attributes.Add("OnClick", "return Save()")
                btnClose.Attributes.Add("onclick", "return Close()")

                intFormId = CCommon.ToLong(GetQueryStringVal("FormID"))
                sImportModule = CCommon.ToShort(GetQueryStringVal("ImportType"))
                sInsertUpdateType = CCommon.ToShort(GetQueryStringVal("InsertUpdateType"))
                If Not IsPostBack Then
                    'objCommon.sb_FillComboFromDBwithSel(ddlRelationship, 5, Session("DomainID"))
                    LoadAvailableList()

                    If (intFormId = 20 Or intFormId = 133 Or intFormId = 134) AndAlso (sInsertUpdateType = 1 Or sInsertUpdateType = 3) Then
                        tdLinking.Visible = True
                        ddlLinkingIDs.Items.Add(New ListItem("-- Select One --", 0))
                        If intFormId = 20 Then 'Item
                            ddlLinkingIDs.Items.Add(New ListItem("Item ID", 1))
                            ddlLinkingIDs.Items.Add(New ListItem("Item Name", 7))
                            ddlLinkingIDs.Items.Add(New ListItem("SKU", 2))
                            ddlLinkingIDs.Items.Add(New ListItem("UPC", 3))
                            ddlLinkingIDs.Items.Add(New ListItem("Model ID", 4))
                        ElseIf intFormId = 133 Then 'Organization
                            ddlLinkingIDs.Items.Add(New ListItem("Non Biz Company ID", 1))
                            ddlLinkingIDs.Items.Add(New ListItem("Organization ID", 2))
                        ElseIf intFormId = 134 AndAlso sInsertUpdateType = 1 Then 'Contact Update
                            ddlLinkingIDs.Items.Add(New ListItem("Non Biz Contact ID", 1))
                            ddlLinkingIDs.Items.Add(New ListItem("Contact ID", 2))
                        ElseIf intFormId = 134 AndAlso sInsertUpdateType = 3 Then 'Contact Insert/Update
                            ddlLinkingIDs.Items.Add(New ListItem("Non Biz Company ID & Non Biz Contact ID", 5))
                            ddlLinkingIDs.Items.Add(New ListItem("Non Biz Company ID & Contact ID", 6))
                            ddlLinkingIDs.Items.Add(New ListItem("Organization ID & Non Biz Contact ID", 7))
                            ddlLinkingIDs.Items.Add(New ListItem("Organization ID & Contact ID", 8))
                        End If
                    ElseIf intFormId = 134 AndAlso sInsertUpdateType = 2 Then
                        tdLinking.Visible = True
                        ddlLinkingIDs.Items.Add(New ListItem("-- Select One --", 0))
                        ddlLinkingIDs.Items.Add(New ListItem("Non Biz Company ID", 3))
                        ddlLinkingIDs.Items.Add(New ListItem("Organization ID", 4))
                        ddlLinkingIDs.Items.Add(New ListItem("Autocreate Organization using contact first & last name", 9))
                    End If
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Sub LoadAvailableList_Old()
            Try
                Dim ds As New DataSet
                Dim dtAvailabletable As DataTable
                Dim dtConftable As DataTable
                Dim objImport As New ImportWizard
                objImport.DomainId = Session("DomainID")
                objImport.ImportType = sImportModule
                objImport.Relationship = 0
                ds = objImport.GetConfiguration()
                If ds.Tables.Count > 0 Then
                    dtAvailabletable = ds.Tables(0)
                    dtConftable = ds.Tables(1)
                End If
                lstAvailablefld.DataSource = dtAvailabletable
                lstAvailablefld.DataTextField = "vcFormFieldName"                   'set the text field
                lstAvailablefld.DataValueField = "numFormFieldID"                'set the value attribut
                lstAvailablefld.DataBind()

                lstAddfld.DataSource = dtConftable
                lstAddfld.DataTextField = "vcFormFieldName"                   'set the text field
                lstAddfld.DataValueField = "numFormFieldID"                'set the value attribut
                lstAddfld.DataBind()
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub LoadAvailableList()
            Try
                Dim ds As New DataSet
                Dim dtAvailabletable As DataTable
                Dim dtConftable As DataTable
                Dim objImport As New ImportWizard
                objImport.DomainId = Session("DomainID")
                objImport.ImportMasterID = intFormId
                objImport.ImportType = sImportModule

                objImport.Mode = 2
                objImport.ImportFileID = 0
                objImport.InsertUpdateType = sInsertUpdateType
                'ds = objImport.GetConfiguration()
                ds = objImport.GetImportMasterCategories()
                If ds.Tables.Count > 2 Then
                    dtAvailabletable = ds.Tables(1)
                    dtConftable = ds.Tables(2)

                    Dim dvAvail As DataView
                    dvAvail = dtAvailabletable.DefaultView
                    dvAvail.Sort = "vcFormFieldName ASC"

                    lstAvailablefld.DataSource = dvAvail
                    lstAvailablefld.DataTextField = "vcFormFieldName"             'set the text field
                    lstAvailablefld.DataValueField = "KeyFieldID"             'set the value attribute
                    lstAvailablefld.DataBind()

                    lstAddfld.DataSource = dtConftable
                    lstAddfld.DataTextField = "vcFormFieldName"                   'set the text field
                    lstAddfld.DataValueField = "KeyFieldID"                   'set the value attribute
                    lstAddfld.DataBind()

                End If

            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
            Try
                If Not ValidSelection() Then
                    Exit Sub
                End If

                Save()
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Save", "window.opener.SetLinkingId(" & CCommon.ToLong(ddlLinkingIDs.SelectedValue) & ");", True)
                LoadAvailableList()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Function ValidSelection() As Boolean
            Try
                Dim strVales As String()
                strVales = txthidden.Text.Split(",")

                If strVales Is Nothing Or strVales.Length = 0 Then
                    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "No Items Added", "alert('Please selects columns.');", True)
                    Return False
                End If

                If intFormId = 20 Then
                    If sInsertUpdateType = 1 Then 'Update
                        If CCommon.ToLong(ddlLinkingIDs.SelectedValue) = 0 Then
                            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "InvalidItemColumns", "alert('Linking ID is required for updating item(s).');", True)
                            Return False
                        Else
                            If ddlLinkingIDs.SelectedValue = "1" AndAlso Not strVales.Contains("211~0") Then 'Item ID
                                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "InvalidItemColumns", "alert('Item ID is selected as Linking Id but it is not added in selected fields.');", True)
                                Return False
                            ElseIf ddlLinkingIDs.SelectedValue = "2" AndAlso Not strVales.Contains("281~0") Then 'SKU
                                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "InvalidItemColumns", "alert('SKU is selected as Linking Id but it is not added in selected fields.');", True)
                                Return False
                            ElseIf ddlLinkingIDs.SelectedValue = "3" AndAlso Not strVales.Contains("203~0") Then 'UPC
                                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "InvalidItemColumns", "alert('UPC is selected as Linking Id but it is not added in selected fields.');", True)
                                Return False
                            ElseIf ddlLinkingIDs.SelectedValue = "4" AndAlso Not strVales.Contains("193~0") Then 'Model ID
                                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "InvalidItemColumns", "alert('Model ID is selected as Linking Id but it is not added in selected fields.');", True)
                                Return False
                            ElseIf ddlLinkingIDs.SelectedValue = "7" AndAlso Not strVales.Contains("189~0") Then 'Item Name
                                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "InvalidItemColumns", "alert('Item Name is selected as Linking Id but it is not added in selected fields.');", True)
                                Return False
                            End If
                        End If
                    ElseIf sInsertUpdateType = 2 Then 'Insert
                        '189:Item Name, 294: Item Type, 271:Asset Account, 272:COG Expense Account, 270:Income Account
                        If Not strVales.Contains("189~0") Or _
                            Not strVales.Contains("294~0") Or _
                            Not strVales.Contains("271~0") Or _
                            Not strVales.Contains("272~0") Or _
                            Not strVales.Contains("270~0") Then
                            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "InvalidItemColumns", "alert('Item Name, Item Type, Income Account, Asset Account and COGs/Expense Account are required for importing new item(s).');", True)
                            Return False
                        End If
                    ElseIf sInsertUpdateType = 3 Then 'Insert Or Update
                        '189:Item Name, 294: Item Type, 271:Asset Account, 272:COG Expense Account, 270:Income Account
                        If Not strVales.Contains("189~0") Or _
                            Not strVales.Contains("294~0") Or _
                            Not strVales.Contains("271~0") Or _
                            Not strVales.Contains("272~0") Or _
                            Not strVales.Contains("270~0") Then
                            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "InvalidItemColumns", "alert('Item Name, Item Type, Income Account, Asset Account and COGs/Expense Account are required for import & update item(s).');", True)
                            Return False
                        ElseIf CCommon.ToLong(ddlLinkingIDs.SelectedValue) = 0 Then
                            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "InvalidItemColumns", "alert('Linking ID is required for updating item(s).');", True)
                            Return False
                        Else
                            If ddlLinkingIDs.SelectedValue = "1" AndAlso Not strVales.Contains("211~0") Then 'Item ID
                                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "InvalidItemColumns", "alert('Item ID is selected as Linking Id but it is not added in selected fields.');", True)
                                Return False
                            ElseIf ddlLinkingIDs.SelectedValue = "2" AndAlso Not strVales.Contains("281~0") Then 'SKU
                                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "InvalidItemColumns", "alert('SKU is selected as Linking Id but it is not added in selected fields.');", True)
                                Return False
                            ElseIf ddlLinkingIDs.SelectedValue = "3" AndAlso Not strVales.Contains("203~0") Then 'UPC
                                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "InvalidItemColumns", "alert('UPC is selected as Linking Id but it is not added in selected fields.');", True)
                                Return False
                            ElseIf ddlLinkingIDs.SelectedValue = "4" AndAlso Not strVales.Contains("193~0") Then 'Model ID
                                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "InvalidItemColumns", "alert('Model ID is selected as Linking Id but it is not added in selected fields.');", True)
                                Return False
                            ElseIf ddlLinkingIDs.SelectedValue = "7" AndAlso Not strVales.Contains("189~0") Then 'Item Name
                                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "InvalidItemColumns", "alert('Item Name is selected as Linking Id but it is not added in selected fields.');", True)
                                Return False
                            End If
                        End If
                    End If
                ElseIf intFormId = 133 Then
                    If sInsertUpdateType = 1 Then 'Update
                        If CCommon.ToLong(ddlLinkingIDs.SelectedValue) = 0 Then
                            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "InvalidItemColumns", "alert('Linking ID is required for updating organization(s).');", True)
                            Return False
                        Else
                            If ddlLinkingIDs.SelectedValue = "1" AndAlso Not strVales.Contains("380~0") Then 'Non Biz Comapny ID
                                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "InvalidItemColumns", "alert('Non Biz Company ID is selected as Linking Id but it is not added in selected fields.');", True)
                                Return False
                            ElseIf ddlLinkingIDs.SelectedValue = "2" AndAlso Not strVales.Contains("539~0") Then 'Organization ID
                                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "InvalidItemColumns", "alert('Organization ID is selected as Linking Id but it is not added in selected fields.');", True)
                                Return False
                            End If
                        End If
                    ElseIf sInsertUpdateType = 2 Then 'Insert
                        '380:Non Biz Company ID, 3:Organization Name, 6:Relationship, 451:Relationship Type
                        If Not strVales.Contains("380~0") Or _
                            Not strVales.Contains("3~0") Or _
                            Not strVales.Contains("6~0") Or _
                            Not strVales.Contains("451~0") Then
                            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "InvalidItemColumns", "alert('Non Biz Company ID, Organization Name, Relationship, and Relationship Type are required for importing new organization(s).');", True)
                            Return False
                        End If
                    ElseIf sInsertUpdateType = 3 Then 'Insert Or Update
                        '380:Non Biz Company ID, 3:Organization Name, 6:Relationship, 451:Relationship Type
                        If Not strVales.Contains("380~0") Or _
                            Not strVales.Contains("3~0") Or _
                            Not strVales.Contains("6~0") Or _
                            Not strVales.Contains("451~0") Then
                            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "InvalidItemColumns", "alert('Non Biz Company ID, Organization Name, Relationship and Relationship Type are required for importing new organization(s).');", True)
                            Return False
                        ElseIf CCommon.ToLong(ddlLinkingIDs.SelectedValue) = 0 Then
                            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "InvalidItemColumns", "alert('Linking ID is required for updating organization(s).');", True)
                            Return False
                        Else
                            If ddlLinkingIDs.SelectedValue = "1" AndAlso Not strVales.Contains("380~0") Then 'Non Biz Company ID
                                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "InvalidItemColumns", "alert('Non Biz Company ID is selected as Linking Id but it is not added in selected fields.');", True)
                                Return False
                            ElseIf ddlLinkingIDs.SelectedValue = "2" AndAlso Not strVales.Contains("539~0") Then 'Organization ID
                                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "InvalidItemColumns", "alert('Organization ID is selected as Linking Id but it is not added in selected fields.');", True)
                                Return False
                            End If
                        End If
                    End If
                ElseIf intFormId = 134 Then
                    If sInsertUpdateType = 1 Then 'Update
                        If CCommon.ToLong(ddlLinkingIDs.SelectedValue) = 0 Then
                            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "InvalidItemColumns", "alert('Linking ID is required for updating contact(s).');", True)
                            Return False
                        Else
                            If ddlLinkingIDs.SelectedValue = "1" AndAlso Not strVales.Contains("386~0") Then 'Non Biz Contact ID
                                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "InvalidItemColumns", "alert('Non Biz Contact ID is selected as Linking Id but it is not added in selected fields.');", True)
                                Return False
                            ElseIf ddlLinkingIDs.SelectedValue = "2" AndAlso Not strVales.Contains("860~0") Then 'Contact ID
                                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "InvalidItemColumns", "alert('Contact ID is selected as Linking Id but it is not added in selected fields.');", True)
                                Return False
                            End If
                        End If
                    ElseIf sInsertUpdateType = 2 Then 'Insert
                        '51:Contact First Name, 52:Contact Last Name,386:Non Biz Contact ID
                        If Not strVales.Contains("51~0") Or _
                            Not strVales.Contains("52~0") Or _
                            Not strVales.Contains("386~0") Then
                            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "InvalidItemColumns", "alert('Non Biz Contact ID, Contact First Name and Contact Last Name are required for importing new contact(s).');", True)
                            Return False
                        ElseIf CCommon.ToLong(ddlLinkingIDs.SelectedValue) = 0 Then
                            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "InvalidItemColumns", "alert('Linking ID is required for adding new contact(s) to particular organization.');", True)
                            Return False
                        Else
                            If ddlLinkingIDs.SelectedValue = "3" AndAlso Not strVales.Contains("380~0") Then 'Non Biz Company ID
                                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "InvalidItemColumns", "alert('Non Biz Company ID is selected as Linking Id but it is not added in selected fields.');", True)
                                Return False
                            ElseIf ddlLinkingIDs.SelectedValue = "4" AndAlso Not strVales.Contains("539~0") Then 'Organization ID
                                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "InvalidItemColumns", "alert('Organization ID is selected as Linking Id but it is not added in selected fields.');", True)
                                Return False
                            End If
                        End If
                    ElseIf sInsertUpdateType = 3 Then 'Insert Or Update
                        If Not strVales.Contains("51~0") Or _
                            Not strVales.Contains("52~0") Or _
                            Not strVales.Contains("386~0") Then
                            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "InvalidItemColumns", "alert('Non Biz Contact ID, Contact First Name and Contact Last Name are required for importing new contact(s).');", True)
                            Return False
                        ElseIf CCommon.ToLong(ddlLinkingIDs.SelectedValue) = 0 Then
                            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "InvalidItemColumns", "alert('Linking ID is required for adding new contact(s) to particular organization or Updating existing contact(s).');", True)
                            Return False
                        Else
                            If ddlLinkingIDs.SelectedValue = "5" AndAlso (Not strVales.Contains("380~0") Or Not strVales.Contains("386~0")) Then 'Non Biz Company ID & Non Biz Contact ID
                                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "InvalidItemColumns", "alert('Non Biz Company ID & Non Biz Contact ID is selected as Linking Id but they are not added in selected fields.');", True)
                                Return False
                            ElseIf ddlLinkingIDs.SelectedValue = "6" AndAlso (Not strVales.Contains("380~0") Or Not strVales.Contains("860~0")) Then 'Non Biz Company ID & Contact ID
                                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "InvalidItemColumns", "alert('Non Biz Company ID & Contact ID is selected as Linking Id but they are not added in selected fields.');", True)
                                Return False
                            ElseIf ddlLinkingIDs.SelectedValue = "7" AndAlso (Not strVales.Contains("539~0") Or Not strVales.Contains("386~0")) Then 'Organization ID & Non Biz Contact ID
                                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "InvalidItemColumns", "alert('Organization ID & Non Biz Contact ID is selected as Linking Id but they are not added in selected fields.');", True)
                                Return False
                            ElseIf ddlLinkingIDs.SelectedValue = "8" AndAlso (Not strVales.Contains("539~0") Or Not strVales.Contains("860~0")) Then 'Organization ID & Contact ID
                                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "InvalidItemColumns", "alert('Organization ID & Contact ID is selected as Linking Id but they are not added in selected fields.');", True)
                                Return False
                            End If
                        End If
                    End If
                End If

                Return True
            Catch ex As Exception
                Throw
            End Try
        End Function

        Sub Save()
            Try
                'numFormFieldId,
                'intRowNum,
                'intColumnNum,
                'bitCustom,
                Dim dtTabs As New DataTable
                Dim i As Integer
                Dim dr As DataRow
                dtTabs.Columns.Add("numFormFieldID")
                dtTabs.Columns.Add("intRowNum")
                dtTabs.Columns.Add("intColumnNum")
                dtTabs.Columns.Add("bitCustom")

                Dim strXml As String
                Dim strVales As String()
                strVales = txthidden.Text.Split(",")
                Dim ds As New DataSet
                For i = 0 To strVales.Length - 2
                    Dim strValues As String()
                    strValues = strVales(i).Split("~")
                    dr = dtTabs.NewRow
                    dr("numFormFieldID") = strValues(0)
                    dr("intRowNum") = i + 1
                    dr("intColumnNum") = i + 1
                    dr("bitCustom") = strValues(1)
                    dtTabs.Rows.Add(dr)
                Next
                dtTabs.TableName = "Table"
                ds.Tables.Add(dtTabs.Copy)
                strXml = ds.GetXml
                ds.Tables.Remove(ds.Tables(0))


                Dim objImport As New ImportWizard
                objImport.DomainId = DomainID
                objImport.Relationship = 0
                objImport.StrXml = strXml
                objImport.UserContactID = UserCntID
                objImport.MasterID = intFormId
                objImport.PageType = sImportModule
                objImport.ItemLinkingID = CCommon.ToLong(ddlLinkingIDs.SelectedValue)
                objImport.SaveImportConfg()
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub btnSaveClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveClose.Click
            Try
                If Not ValidSelection() Then
                    Exit Sub
                End If

                Save()
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "SaveClose", "window.opener.SetLinkingId(" & CCommon.ToLong(ddlLinkingIDs.SelectedValue) & "); window.close();", True)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Sub AssignValuesTextBox(ByVal objLeads As CLeads, ByVal vcValue As String, ByVal vcColumnName As String)
            Try
                Select Case vcColumnName
                    Case "txtComments"
                        objLeads.Comments = vcValue
                End Select
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub DisplayError(ByVal errorMessage As String)
            lblException.Text = errorMessage
        End Sub

    End Class
End Namespace

