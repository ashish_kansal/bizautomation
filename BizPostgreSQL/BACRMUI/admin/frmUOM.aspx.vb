﻿Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Common
Public Class frmUOM
    Inherits BACRMPage
    Dim objCommon As CCommon

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then
                If Session("UnitSystem") = "E" Then
                    hdnUnitType.Value = 1
                ElseIf Session("UnitSystem") = "M" Then
                    hdnUnitType.Value = 2
                Else
                    hdnUnitType.Value = 1
                End If

                BindDatagrid()
            End If
            btnCancel.Attributes.Add("onclick", "return Close()")
            litMessage.Text = ""
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
    Sub BindDatagrid()
        Try
            objCommon = New CCommon
            objCommon.DomainID = Session("DomainID")
            objCommon.UnitType = CCommon.ToShort(hdnUnitType.Value)

            Dim dtList As DataTable
            dtList = objCommon.GetUOM()
            gvUOM.DataSource = dtList
            gvUOM.DataBind()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If Page.IsValid Then
            Try
                Try
                    save()
                Catch ex As Exception
                    If ex.Message = "UOM_ALREADY_EXISTS" Then
                        litMessage.Text = "Unit name is already exists."
                    Else
                        Throw ex
                    End If
                End Try

                BindDatagrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End If
    End Sub

    Private Sub btnSaveClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveClose.Click
        If Page.IsValid Then
            Try
                save()
                Dim strScript As String = "<script language=JavaScript>self.close()</script>"
                If (Not Page.IsStartupScriptRegistered("clientScript")) Then Page.RegisterStartupScript("clientScript", strScript)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End If
    End Sub

    Sub save()
        Try
            objCommon = New CCommon

            If hfUnitId.Value > 0 Then
                objCommon.UnitId = hfUnitId.Value
            End If
            objCommon.DomainID = Session("DomainID")
            objCommon.UnitName = txtUnit.Text.Trim
            objCommon.UnitType = CCommon.ToShort(hdnUnitType.Value)
            objCommon.UnitEnable = True

            objCommon.ManageUOM()

            txtUnit.Text = ""
            hfUnitId.Value = 0
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub gvUOM_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvUOM.RowCommand

        Try

            If e.CommandName = "EditUnit" Then
                objCommon = New CCommon

                objCommon.DomainID = Session("DomainID")
                objCommon.UnitId = gvUOM.DataKeys(e.CommandArgument).Value

                Dim dtList As DataTable
                dtList = objCommon.GetUnit()

                If (dtList.Rows.Count > 0) Then
                    hfUnitId.Value = dtList(0)("numUOMId")
                    txtUnit.Text = dtList(0)("vcUnitName")
                End If
                'added by Sachin
            ElseIf e.CommandName = "DeleteUnit" Then
                Try

                    objCommon = New CCommon
                    objCommon.DomainID = Session("DomainID")
                    objCommon.UnitId = CCommon.ToLong(e.CommandArgument)
                    objCommon.DeleteUnit()
                    BindDatagrid()

                Catch ex As Exception

                    litMessage.Text = "Child Record Exists,Can't perform Delete Operation"

                End Try
               
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
End Class