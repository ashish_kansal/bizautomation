﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="CommissionRuleList.aspx.vb"
    Inherits=".CommissionRuleList" MasterPageFile="~/common/GridMasterRegular.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">

    <title>Commission Rule List</title>
    <style>
        .info {
            color: #00529B;
            background-color: #BDE5F8;
            background-image: url('../images/info.png');
            border: 1px solid;
            margin: 10px 5px 0px 5px;
            padding: 15px 10px 15px 50px;
            background-repeat: no-repeat;
            background-position: 10px center;
        }
    </style>
    <script type="text/javascript" language="javascript">
        function New() {
            window.location.href = "CommissionRuleDtl.aspx";
        }
        function DeleteRecord() {
            if (confirm('Are you sure, you want to delete the selected record?')) {
                return true;
            }
            else {
                return false;
            }
        }

        function OpenSetting() {
            window.open('../admin/CommissionContacts.aspx', '', 'toolbar=no,titlebar=no,top=200,left=200,width=700,height=550,scrollbars=yes,resizable=yes')
            return false;
        }
        function OpenHelp() {
            window.open('../Help/Admin-Commission_Rules.html', '', 'toolbar=no,titlebar=no,top=200,left=200,width=550,height=350,scrollbars=yes,resizable=yes')
            return false
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server"
    ClientIDMode="Static">
    <div class="row padbottom10">
        <div class="col-xs-12">
            <div class="pull-right">
                <asp:LinkButton ID="btnNew" runat="server" CssClass="btn btn-primary" OnClientClick="New()"><i class="fa fa-plus-circle"></i>&nbsp;&nbsp;New Commission Rule</asp:LinkButton>
            </div>
        </div>
    </div>
    <asp:UpdateProgress ID="UpdateProgress" runat="server" ClientIDMode="Static">
        <ProgressTemplate>
            <div class="overlay">
                <div class="overlayContent" style="color: #000; text-align: center; width: 250px; padding: 20px">
                    <i class="fa fa-2x fa-refresh fa-spin"></i>
                    <h3>Processing Request</h3>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <asp:Literal ID="litMessage" runat="server" EnableViewState="False"></asp:Literal>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server" ClientIDMode="Static">
    Commission Rules&nbsp;<a href="#" onclick="return OpenHelpPopUp('admin/commissionrulelist.aspx')"><label class="badge bg-yellow">?</label></a>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BizPager" runat="server" ClientIDMode="Static">
    <div class="orderbradcrumb">
        <%--<asp:HyperLink ID="hplCommissionableContacts" runat="server" CssClass="btn btn-default btn-sm">Manage Commissionable Subcontractors</asp:HyperLink>&nbsp;&nbsp;--%>
    </div>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="GridPlaceHolder" runat="server"
    ClientIDMode="Static">
    <div class="row">
        <div class="col-xs-12">
            <div class="table-responsive">
                <asp:DataGrid ID="dgCommissionRule" AllowSorting="True" runat="server" Width="100%"
                    CssClass="table table-striped table-bordered" AutoGenerateColumns="False" UseAccessibleHeader="true">
                    <Columns>
                        <asp:BoundColumn Visible="False" DataField="numComRuleID"></asp:BoundColumn>
                        <asp:ButtonColumn DataTextField="vcCommissionName" CommandName="RuleID" SortExpression="vcRuleName"
                            HeaderText="<font>Rule Name</font>"></asp:ButtonColumn>
                        <asp:BoundColumn HeaderText="Priority" DataField="Priority" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                        <asp:BoundColumn HeaderText="Item it applies to" DataField="ItemList"></asp:BoundColumn>
                        <asp:BoundColumn HeaderText="Customer it applies to" DataField="CustomerList"></asp:BoundColumn>
                        <asp:BoundColumn HeaderText="Contact it applies to" DataField="ContactList"></asp:BoundColumn>
                        <asp:TemplateColumn ItemStyle-Width="25">
                            <ItemTemplate>
                                <asp:LinkButton ID="btnDelete" runat="server" CssClass="btn btn-danger btn-xs" CommandName="Delete"><i class="fa fa-trash"></i></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                </asp:DataGrid>
            </div>
        </div>
    </div>
    <asp:Panel runat="server" ID="pnlSMTPError" CssClass="info">
        <%-- If you don't have your own mail server (e.g. MS Exchange) BizAutomation.com recommends
        using Google Apps for your email service (supports name@yourcompanydomain.com).
        Type "Google Apps" within help for set up details.--%>
        Commission will be paid only for authoritative bizdocs.
    </asp:Panel>
</asp:Content>
