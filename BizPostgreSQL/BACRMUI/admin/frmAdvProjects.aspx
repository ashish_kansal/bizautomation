<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmAdvProjects.aspx.vb"
    Inherits="BACRM.UserInterface.Admin.frmAdvProjects" MasterPageFile="~/common/DetailPage.Master" %>

<%@ Register TagPrefix="menu1" TagName="webmenu" Src="../include/webmenu.ascx" %>
<%@ Register TagPrefix="BizCalendar" TagName="Calendar" Src="../include/calandar.ascx" %>
<%@ Register Src="frmAdvSearchCriteria.ascx" TagName="frmAdvSearchCriteria" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Advance Project Search</title>
    <link rel="stylesheet" type="text/css" href="../CSS/jscal2.css" />
    <link rel="stylesheet" type="text/css" href="../CSS/border-radius.css" />
    <script type="text/javascript" src="../JavaScript/jscal2.js"></script>
    <script type="text/javascript" src="../JavaScript/en.js"></script>
    <script src="../javascript/AdvSearchScripts.js" type="text/javascript"></script>
    <script src="../javascript/AdvSearchColumnCustomization.js"
        type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="RecordActionPanel" runat="server"
    ClientIDMode="Static">
    <div class="row padbottom10">
        <div class="col-xs-12">
            <div class="pull-right">
                <div class="form-inline">
                    <asp:RadioButton Text="Created Date" Checked="true" runat="server" ID="radCreated" GroupName="Date" />
                    &nbsp;&nbsp;&nbsp;
                    <asp:RadioButton Text="Due Date" runat="server" ID="radDue" GroupName="Date" />&nbsp;
                    From
                    <BizCalendar:Calendar ID="calFrom" runat="server" />
                    To
                    <BizCalendar:Calendar ID="calTo" runat="server" />
                    <asp:LinkButton runat="server" ID="btnSearch" CssClass="btn btn-primary"><i class="fa fa-search"></i>&nbsp;&nbsp;Search</asp:LinkButton>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="DetailPageTitle" runat="server"
    ClientIDMode="Static">
    Projects Search
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="TabsPlaceHolder" runat="server"
    ClientIDMode="Static">
    <asp:UpdateProgress ID="UpdateProgress" runat="server" ClientIDMode="Static">
        <ProgressTemplate>
            <div class="overlay">
                <div class="overlayContent" style="color: #000; text-align: center; width: 250px; padding: 20px">
                    <i class="fa fa-2x fa-refresh fa-spin"></i>
                    <h3>Processing Request</h3>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:Literal ID="litMessage" runat="server" EnableViewState="False"></asp:Literal>
    <div runat="server" id="tblMain">
        <asp:PlaceHolder ID="plhControls" runat="server"></asp:PlaceHolder>
    </div>
    <uc1:frmAdvSearchCriteria ID="frmAdvSearchCriteria1" runat="server" />
    <asp:HiddenField ID="hdnFilterCriteria" runat="server" />
</asp:Content>
