Imports BACRM.BusinessLogic.Common

Namespace BACRM.UserInterface.Admin
    Public Class cflwizard5
        Inherits BACRMPage
        Protected WithEvents cfword As System.Web.UI.WebControls.DropDownList
        Protected WithEvents ImageButton3 As System.Web.UI.WebControls.ImageButton
        Protected WithEvents ImageButton1 As System.Web.UI.WebControls.ImageButton

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Try
                If Session("tabord") <> "" Then cfword.Items.FindByText(Session("tabord")).Selected = True
                Session("tabord") = ""
                
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub ImageButton1_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton1.Click
            Try
                If cfword.SelectedItem.Value = "0" Then
                    Response.Write("<script language=javascript>")
                    Response.Write("alert('Choose Order')")
                    Response.Write("</script>")
                    Exit Sub
                End If
                Dim tabord As String
                Session("tabord") = cfword.SelectedItem.Value
                Response.Redirect("../admin/cflwizardsum.aspx")
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub ImageButton2_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs)

        End Sub

        Private Sub ImageButton3_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImageButton3.Click

        End Sub
    End Class
End Namespace