﻿Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Contacts
Imports BACRM.BusinessLogic.Documents
Imports BACRM.BusinessLogic.Common
Imports System.Text
Imports System.IO

Public Class frmGenerateHtmlForm
    Inherits BACRMPage
    Dim iForm As Integer
    Dim iSubForm As Integer
    Dim iEnableLinkedin As Integer
    Dim iopenPopup As Integer
    Dim iGroupID As Integer

    Dim objFormWizard As FormConfigWizard

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            iForm = CCommon.ToInteger(GetQueryStringVal("frmId"))
            iSubForm = CCommon.ToInteger(GetQueryStringVal("SubForm"))
            iEnableLinkedin = CCommon.ToInteger(GetQueryStringVal("EnableLinkedin"))
            iopenPopup = CCommon.ToInteger(GetQueryStringVal("OpenPopup"))

            Dim objConfigWizard As New FormGenericFormContents                                  'Create an object of class which encaptulates the functionaltiy
            Dim dtFormInfo As DataTable
            iGroupID = 0

            objConfigWizard.FormID = iForm                                                          'Set the property of formId
            objConfigWizard.DomainID = Session("DomainID")
            dtFormInfo = objConfigWizard.getFormHeaderDetails()                  'call to get the form header details
            If dtFormInfo.Rows.Count > 0 Then                                    'if the form is not registered in the database
                iGroupID = dtFormInfo.Rows(0).Item("numGrpId")
            End If

            If Not IsPostBack Then
                generateForm()

                BindDatagrid()
            End If
            btnClose.Attributes.Add("onclick", "return Close()")

        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Sub BindDatagrid()
        Try
            objFormWizard = New FormConfigWizard

            objFormWizard.DomainID = Session("DomainID")
            objFormWizard.FormID = iForm
            objFormWizard.AuthenticationGroupID = iGroupID
            objFormWizard.tintType = 2
            objFormWizard.numSubFormId = iSubForm
            Dim dtList As DataTable
            dtList = objFormWizard.ManageHTMLFormURL()
            gvURL.DataSource = dtList
            gvURL.DataBind()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub FillStateDyn(ByVal sender As DropDownList)
        Try
            If sender.ID = "vcBillCountry" Then
                FillState(CType(plhFormControls.FindControl("vcBilState"), DropDownList), sender.SelectedItem.Value, Session("DomainID"))
            Else : FillState(CType(plhFormControls.FindControl(Replace(sender.ID, "Country", "State")), DropDownList), sender.SelectedItem.Value, Session("DomainID"))
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub generateForm()
        Dim dtGenericFormConfig As DataTable

        GenerateGenericFormControls.FormID = iForm                                            'set the form id for Lead Box
        GenerateGenericFormControls.DomainID = Session("DomainID")                                'Set the domain id
        GenerateGenericFormControls.SubFormId = iSubForm
        GenerateGenericFormControls.sXMLFilePath = CCommon.ToString(System.Configuration.ConfigurationManager.AppSettings("PortalLocation")) & "\Documents\Docs\" 'set the file path
        dtGenericFormConfig = GenerateGenericFormControls.getFormControlConfig()          'get the datatable to contain the form config
        If dtGenericFormConfig.Rows.Count = 0 Then                                          'if the xml for form fields has not been configured
            litMessage.Text = "The screen is not created. Please use BizForm Wizard to create this screen." 'calls to create the form controls and add it to the form
            Return
        End If

        Dim strValidation As String = GenerateGenericFormControls.GenerateValidationScript(dtGenericFormConfig)
        'plhFormControls.Controls.Add(New LiteralControl(strValidation))

        callFuncForFormGenerationNonAOI(dtGenericFormConfig)                                'Calls function for form generation and display for non AOI fields

        callFuncForFormGenerationAOI()                                   'Calls function for form generation and display for AOI fields

        plhFormControlsAOI.Controls.Add(New LiteralControl("<!--Do not delete Hidden Field-->"))

        Dim objenc As New QueryStringValues

        Dim oControlBox As HiddenField
        oControlBox = New HiddenField
        oControlBox.ID = "hfForm"
        oControlBox.Value = objenc.Encrypt(iForm)
        oControlBox.ClientIDMode = UI.ClientIDMode.Static
        plhFormControlsAOI.Controls.Add(oControlBox)

        oControlBox = New HiddenField
        oControlBox.ID = "hfSubForm"
        oControlBox.Value = objenc.Encrypt(iSubForm)
        oControlBox.ClientIDMode = UI.ClientIDMode.Static
        plhFormControlsAOI.Controls.Add(oControlBox)

        oControlBox = New HiddenField
        oControlBox.ID = "hfDomain"
        oControlBox.Value = objenc.Encrypt(Session("DomainID"))
        oControlBox.ClientIDMode = UI.ClientIDMode.Static
        plhFormControlsAOI.Controls.Add(oControlBox)

        oControlBox = New HiddenField
        oControlBox.ID = "hfGroup"
        oControlBox.Value = objenc.Encrypt(iGroupID)
        oControlBox.ClientIDMode = UI.ClientIDMode.Static
        plhFormControlsAOI.Controls.Add(oControlBox)

        oControlBox = New HiddenField
        oControlBox.ID = "hfLinkedinId"
        oControlBox.Value = ""
        plhFormControlsAOI.Controls.Add(oControlBox)

        oControlBox = New HiddenField
        oControlBox.ID = "hfLinkedinUrl"
        oControlBox.Value = ""
        oControlBox.ClientIDMode = UI.ClientIDMode.Static
        plhFormControlsAOI.Controls.Add(oControlBox)

        oControlBox = New HiddenField
        oControlBox.ID = "hfEnableLinkedin"
        oControlBox.Value = iEnableLinkedin
        oControlBox.ClientIDMode = UI.ClientIDMode.Static
        plhFormControlsAOI.Controls.Add(oControlBox)

        oControlBox = New HiddenField
        oControlBox.ID = "hfPopupOpen"
        oControlBox.Value = iopenPopup
        oControlBox.ClientIDMode = UI.ClientIDMode.Static
        plhFormControlsAOI.Controls.Add(oControlBox)

        oControlBox = New HiddenField
        oControlBox.ID = "hfPartenrSource"
        oControlBox.Value = 0
        oControlBox.ClientIDMode = UI.ClientIDMode.Static
        plhFormControlsAOI.Controls.Add(oControlBox)

        oControlBox = New HiddenField
        oControlBox.ID = "hfPartenerContact"
        oControlBox.Value = 0
        oControlBox.ClientIDMode = UI.ClientIDMode.Static
        plhFormControlsAOI.Controls.Add(oControlBox)

        If iForm = 128 Then
            RadEditor1.Content = "<html><head><title>Case Form</title>" & _
                                "<script src='http://portal.bizautomation.com/JavaScript/JavaScriptValidation.js' type='text/javascript'></script>" & _
                                "<script src='https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js' type='text/javascript'></script>" & _
                                 "<script language='JavaScript'>" & strValidation & "</script></head> " & _
                               "<body><form name=""frmLead"" method=""post"" action='http://portal.bizautomation.com/Common/HtmlWebCase.aspx'>" & _
                                CCommon.RenderControl(plhFormControls).Replace("ctl00$Content$", "") & CCommon.RenderControl(plhFormControlsAOI).Replace("ctl00$Content$", "") & _
                               "</form></body></html>"
        Else
            RadEditor1.Content = "<html><head><title>Lead Form</title>" & _
                                "<script src='http://portal.bizautomation.com/JavaScript/JavaScriptValidation.js' type='text/javascript'></script>" & _
                                "<script src='https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js' type='text/javascript'></script>" & _
                                 "<script language='JavaScript'>" & strValidation & "</script></head> " & _
                                 "<script type='text/javascript' src='http://platform.linkedin.com/in.js'> api_key: 75xmt0efgvwi1m " & _
                                 "</script>" & _
                                 "<script type='text/javascript' src='http://portal.bizautomation.com/javascript/linkedin.js'></script>" & _
                               "<body><form name=""frmLead"" method=""post"" action='http://portal.bizautomation.com/Common/HtmlWebLead.aspx'>" & _
                                CCommon.RenderControl(plhFormControls).Replace("ctl00$Content$", "") & CCommon.RenderControl(plhFormControlsAOI).Replace("ctl00$Content$", "") & _
                               "</form></body></html>"
        End If


        ' added by chintan since it does not generate lead without replacing it .Replace("ctl00$Content$","")
        plhFormControls.Visible = False
        plhFormControlsAOI.Visible = False

    End Sub
    Public Overrides Sub VerifyRenderingInServerForm(ByVal control As System.Web.UI.Control)

    End Sub
    Sub callFuncForFormGenerationNonAOI(ByVal dtFormConfig As DataTable)
        Try
            GenerateGenericFormControls.boolAOIField = 0                                               'Set the AOI flag to non AOI
            plhFormControls.Controls.Add(GenerateGenericFormControls.createFormControls(dtFormConfig, True)) 'calls to create the form controls and add it to the form
            'Set control's default value from query string ,so one can submit data through query and pre populate form

            Dim dvConfig As DataView
            dvConfig = New DataView(dtFormConfig)
            dvConfig.RowFilter = " vcDbColumnName like '%Country'"
            If dvConfig.Count > 0 Then
                Dim i As Integer
                For i = 0 To dvConfig.Count - 1
                    Dim dlCountry As DropDownList
                    dlCountry = CType(plhFormControls.FindControl(dvConfig(i).Item("vcDbColumnName")), DropDownList)
                    If dlCountry.Items.FindByText("United States") IsNot Nothing Then
                        dlCountry.ClearSelection()
                        dlCountry.Items.FindByText("United States").Selected = True

                        If dvConfig(i).Item("vcDbColumnName") = "vcBillCountry" Then
                            If Not CType(plhFormControls.FindControl("vcBilState"), DropDownList) Is Nothing Then
                                Dim dl As DropDownList
                                dl = CType(plhFormControls.FindControl(dvConfig(i).Item("vcDbColumnName")), DropDownList)
                                FillStateDyn(dlCountry)
                            End If
                        Else
                            If Not CType(plhFormControls.FindControl(Replace(dvConfig(i).Item("vcDbColumnName"), "Country", "State")), DropDownList) Is Nothing Then
                                Dim dl As DropDownList
                                dl = CType(plhFormControls.FindControl(dvConfig(i).Item("vcDbColumnName")), DropDownList)
                                FillStateDyn(dlCountry)
                            End If
                        End If
                    End If
                Next
            End If

            For index As Integer = 0 To dtFormConfig.Rows.Count - 1
                Dim control As Control = plhFormControls.FindControl(dtFormConfig.Rows(index)("vcDbColumnName"))
                If Not control Is Nothing Then
                    Select Case dtFormConfig.Rows(index)("vcAssociatedControlType").ToString
                        Case "CheckBox"
                            CType(control, CheckBox).Checked = CCommon.ToBool(Request(dtFormConfig.Rows(index)("vcDbColumnName")))
                        Case "TextBox"
                            CType(control, TextBox).Text = CCommon.ToString(Request(dtFormConfig.Rows(index)("vcDbColumnName")))
                        Case "TextArea"
                            CType(control, TextBox).Text = CCommon.ToString(Request(dtFormConfig.Rows(index)("vcDbColumnName")))
                        Case "RadioBox"
                            CType(control, RadioButtonList).SelectedValue = CCommon.ToString(Request(dtFormConfig.Rows(index)("vcDbColumnName")))
                        Case "SelectBox"
                            If Not dtFormConfig.Rows(index)("vcDbColumnName").ToString.Contains("Country") Then
                                CType(control, DropDownList).SelectedValue = CCommon.ToString(Request(dtFormConfig.Rows(index)("vcDbColumnName")))
                            End If

                            If dtFormConfig.Rows(index)("vcDbColumnName").ToString.Contains("numCompanyType") Then
                                CType(control, DropDownList).SelectedValue = 46 ' set to customer by default
                                CType(control, DropDownList).Attributes.Add("style", "display:none")
                            End If

                            CType(control, DropDownList).Items(0).Value = 0
                    End Select

                    If CCommon.ToString(dtFormConfig.Rows(index)("numFormFieldId")).Contains("C") AndAlso CCommon.ToString(dtFormConfig.Rows(index)("vcFieldType")) = "D" Then
                        control.ID = dtFormConfig.Rows(index)("vcDbColumnName") & "_" & CCommon.ToString(dtFormConfig.Rows(index)("numFormFieldId")).Replace("C", "D")
                    Else
                        control.ID = dtFormConfig.Rows(index)("vcDbColumnName") & "_" & CCommon.ToString(dtFormConfig.Rows(index)("numFormFieldId"))
                    End If
                End If
            Next
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub callFuncForFormGenerationAOI()
        Try
            Dim dsFormConfig As DataSet
            Dim objGenericAdvSearch As New FormGenericAdvSearch 'Declare a DataTable
            objGenericAdvSearch.AuthenticationGroupID = 0
            objGenericAdvSearch.FormID = iForm
            objGenericAdvSearch.DomainID = Session("DomainID")
            dsFormConfig = objGenericAdvSearch.getAOIList()                                                     'Get the AOIList
            GenerateGenericFormControls.createFormControlsAOIHTML(dsFormConfig, plhFormControlsAOI, iEnableLinkedin)  'calls to create the form controls and add it to the form
            'Return dtFormConfig                                                                                 'Return the datatable which contains the AOI List
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub btnAddEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddEdit.Click
        If Page.IsValid Then
            Try
                save()
                BindDatagrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End If
    End Sub

    Sub save()
        Try
            objFormWizard = New FormConfigWizard

            objFormWizard.tintType = 1

            If hfURLId.Value > 0 Then
                objFormWizard.URLId = hfURLId.Value
            End If
            objFormWizard.DomainID = Session("DomainID")
            objFormWizard.FormID = iForm
            objFormWizard.AuthenticationGroupID = iGroupID
            objFormWizard.vcURL = txtURL.Text.Trim
            objFormWizard.vcSuccessURL = txtSuccessURL.Text.Trim
            objFormWizard.vcFailURL = txtFailURL.Text.Trim
            objFormWizard.numSubFormId = iSubForm
            objFormWizard.ManageHTMLFormURL()

            txtURL.Text = ""
            txtSuccessURL.Text = ""
            txtFailURL.Text = ""
            hfURLId.Value = 0
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub gvURL_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvURL.RowCommand
        If e.CommandName = "EditURL" Then
            objFormWizard = New FormConfigWizard

            objFormWizard.DomainID = Session("DomainID")
            objFormWizard.FormID = iForm
            objFormWizard.AuthenticationGroupID = iGroupID
            objFormWizard.tintType = 4
            objFormWizard.URLId = gvURL.DataKeys(e.CommandArgument).Value
            objFormWizard.numSubFormId = iSubForm
            Dim dtList As DataTable
            dtList = objFormWizard.ManageHTMLFormURL()

            If (dtList.Rows.Count > 0) Then
                hfURLId.Value = dtList(0)("numURLId")
                txtURL.Text = dtList(0)("vcURL")
                txtSuccessURL.Text = dtList(0)("vcSuccessURL")
                txtFailURL.Text = dtList(0)("vcFailURL")
            End If
        ElseIf e.CommandName = "DeleteURL" Then
            objFormWizard = New FormConfigWizard

            objFormWizard.DomainID = Session("DomainID")
            objFormWizard.FormID = iForm
            objFormWizard.AuthenticationGroupID = iGroupID
            objFormWizard.tintType = 3
            objFormWizard.URLId = gvURL.DataKeys(e.CommandArgument).Value
            objFormWizard.numSubFormId = iSubForm
            objFormWizard.ManageHTMLFormURL()
            BindDatagrid()
        End If
    End Sub
End Class