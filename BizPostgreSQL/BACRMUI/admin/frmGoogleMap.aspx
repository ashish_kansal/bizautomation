﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmGoogleMap.aspx.vb"
    Inherits=".frmGoogleMap" MasterPageFile="~/common/Popup.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Google Map</title>
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
    <style type="text/css">
        html
        {
            height: 100%;
        }
        body
        {
            height: 100%;
            margin: 0;
            padding: 0;
        }
        #map_canvas
        {
            height: 100%;
        }
        .formatText
        {
            color: black;
            font-size: 11px;
            font-family: Arial;
            font-weight: bold;
        }
    </style>
    <script src="../JavaScript/GoogleMap.js" type="text/javascript"></script>
    <%--  <script src="https://maps.google.com/maps?file=api&amp;v=2&amp;sensor=false&amp;key=AIzaSyATuFZN6vUCRHxxQoL0gNdGLVRFhv70b54"
        type="text/javascript"></script>--%>
    <script src="https://maps.google.com/maps?file=api&amp;v=2&amp;sensor=false&amp;key=AIzaSyATuFZN6vUCRHxxQoL0gNdGLVRFhv70b54"
        type="text/javascript"></script>
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDKZ2Kph5NQXTnPc1xEzM4Bq0nhzXjONS0&sensor=false">
    </script>
    <script type="text/javascript">
        var map;
        function initialize() {

            if (!arrGoogleMapAddress)
                return false;

            var myLatlng = new google.maps.LatLng(arrGoogleMapAddress[0].Longitude, arrGoogleMapAddress[0].Latitude);
            var myOptions = {
                zoom: 10,
                center: myLatlng,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            }

            map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);

            var sidebar = document.getElementById('sidebar');
            sidebar.innerHTML = '';

            if (arrGoogleMapAddress.length == 0) {
                sidebar.innerHTML = 'No results found.';
                map.setCenter(new GLatLng(40, -100), 4);
                return;
            }

            // Create marker 
            var marker = new google.maps.Marker({
                map: map,
                position: myLatlng,
                title: 'Some location'
            });

            var radiusMiles = document.getElementById("hfRadius").value;
            // Add circle overlay and bind to marker
            var circle = new google.maps.Circle({
                map: map,
                radius: parseFloat(radiusMiles),
                fillColor: '#AA0000'
            });
            circle.bindTo('center', marker, 'position');
            //            var circ = new google.maps.Circle();
            //            circ.setRadius(5000);
            //            circ.setCenter(myLatlng);
            //            map.setCenter(myLatlng);
            //            map.fitBounds(circ.getBounds());

            for (var i = 0; i < arrGoogleMapAddress.length; i++) {
                var location = new google.maps.LatLng(arrGoogleMapAddress[i].Longitude, arrGoogleMapAddress[i].Latitude);
                var marker = new google.maps.Marker({
                    position: location,
                    map: map
                });
                var j = i + 1;
                marker.setTitle(j.toString());
                attachSecretMessage(marker, i);

                var sidebarEntry = createSidebarEntry(marker, arrGoogleMapAddress[i].Name, arrGoogleMapAddress[i].Address, arrGoogleMapAddress[i].Distance);
                sidebar.appendChild(sidebarEntry);
            }
        }

        function createSidebarEntry(marker, name, address, distance) {
            var div = document.createElement('div');
            var html = '<b>' + name + '</b> (' + distance + ')<br/>' + address;
            div.innerHTML = html;
            div.style.cursor = 'pointer';
            div.style.marginBottom = '5px';
            google.maps.event.addDomListener(div, 'click', function () {
                google.maps.event.trigger(marker, 'click');
            });
            google.maps.event.addDomListener(div, 'mouseover', function () {
                div.style.backgroundColor = '#eee';
            });
            google.maps.event.addDomListener(div, 'mouseout', function () {
                div.style.backgroundColor = '#fff';
            });
            return div;
        }
        // The five markers show a secret message when clicked
        // but that message is not within the marker's instance data

        function attachSecretMessage(marker, number) {
            var infowindow = new google.maps.InfoWindow(
      { content: "<span class=formatText >" + arrGoogleMapAddress[number].Name + "<br />" + arrGoogleMapAddress[number].Address + "</span>",
          size: new google.maps.Size(50, 50)
      });
            google.maps.event.addListener(marker, 'click', function () {
                infowindow.open(map, marker);
            });
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <div class="input-part">
        <div class="right-input">
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageTitle" runat="server" ClientIDMode="Static">
    Google Map
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="Content" runat="server" ClientIDMode="Static">
    <table>
        <tbody>
            <tr>
                <td class="normal4" align="center">
                    <asp:Literal ID="litMessage" runat="server" EnableViewState="False"></asp:Literal>
                </td>
            </tr>
            <tr id="cm_mapTR">
                <td>
                    <div style="width: 560px; height: 560px;">
                        <div id="map_canvas" style="width: 100%; height: 100%;">
                        </div>
                    </div>
                </td>
                <td width="200" valign="top">
                    <div id="sidebar" style="overflow: auto; height: 400px; font-size: 11px; color: #000">
                    </div>
                    <asp:HiddenField ID="hfRadius" runat="server" />
                </td>
            </tr>
        </tbody>
    </table>
    <script type="text/javascript">
        initialize();
    </script>
</asp:Content>
