﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmGoogleAnalyticsConfiguration.aspx.vb"
    Inherits=".frmGoogleAnalyticsConfiguration" MasterPageFile="~/common/Popup.Master" ClientIDMode="Predictable" %>

<asp:Content ID="Content3" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>UOM - Unit Of Measurement Conversion</title>
    <script language="javascript" type="text/javascript">
        function Close() {
            window.close();
        }

        function ClientSideClick() {
            var isGrpOneValid = Page_ClientValidate("vgRequired");
            var check = Check();

            var i;
            for (i = 0; i < Page_Validators.length; i++) {
                ValidatorValidate(Page_Validators[i]); //this forces validation in all groups
            }

            if (isGrpOneValid && check) {
                return true;
            }
            else
                return false;
        }

        function Check() {
            if (Number(document.getElementById("hfTotalRow").value) > 0) {
                for (var i = 1; i <= Number(document.getElementById("hfTotalRow").value); i++) {
                    if (document.getElementById("ddlUnit_1_" + i).value == document.getElementById("ddlUnit_2_" + i).value) {
                        alert('Base and Conversion Unit are not same!!!');
                        return false;
                    }
                    for (var j = 1; j <= Number(document.getElementById("hfTotalRow").value); j++) {
                        if (j != i) {
                            if ((document.getElementById("ddlUnit_1_" + i).value == document.getElementById("ddlUnit_1_" + j).value && document.getElementById("ddlUnit_2_" + i).value == document.getElementById("ddlUnit_2_" + j).value)
                                  || (document.getElementById("ddlUnit_1_" + i).value == document.getElementById("ddlUnit_2_" + j).value && document.getElementById("ddlUnit_2_" + i).value == document.getElementById("ddlUnit_1_" + j).value)) {
                                alert('Select distinct base and conversion unit!!!');
                                return false;
                            }
                        }
                    }
                }
            }
            else {
                return false;
            }

            return true;
        }
    </script>
    <style type="text/css">
        .row
        {
            background-color: #DBE5F1;
            text-align: left;
            color: black;
            font-family: Arial;
            font-size: 8pt;
        }
        .arow
        {
            background-color: #fffff;
            text-align: left;
            color: black;
            font-family: Arial;
            font-size: 8pt;
        }

        input[type='checkbox'] {
            position: relative;
            bottom: 1px;
            vertical-align: middle;
        }

        .tooltip {
            top:0px !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <div class="input-part">
        <div class="right-input">
            <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" 
                OnClientClick="return ClientSideClick()"></asp:Button>
            <asp:Button ID="btnSaveClose" runat="server" CssClass="button" Text="Save &amp; Close"
                 OnClientClick="return ClientSideClick()"></asp:Button>
            <asp:Button ID="btnCancel" runat="server" CssClass="button" Width="50" Text="Close">
            </asp:Button>&nbsp;&nbsp;
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="PageTitle" runat="server" ClientIDMode="Static">
    Google Analytics Configurations
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="Content" runat="server" ClientIDMode="Static">
   <table style="min-width:400px">
        <tr>
                               <td class="normal1" align="right">User E-mail :
                                </td>
                                <td  >
                                    <asp:TextBox ID="txtGAUserEmail" CssClass="signup" runat="server" Width="200" title="">
                                    </asp:TextBox>

                                </td> 
            </tr>
       <tr>
            <td class="normal1" align="right">User Password : 
                                </td>
                                <td>
                                    <asp:TextBox ID="txtGAPassword" CssClass="signup" runat="server" Width="200" title="">
                                    </asp:TextBox>

                                </td>
           </tr>
       <tr>
               <td class="normal1" align="right">User Profile Id : 
                                </td>
                                <td>
                                    <asp:TextBox ID="txtGAProfileID" CssClass="signup" runat="server" Width="200" title="">
                                    </asp:TextBox>

                                </td>
                            </tr>
   </table>
</asp:Content>
