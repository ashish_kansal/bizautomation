﻿Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Item
Imports BACRM.BusinessLogic.Opportunities
Imports BACRM.BusinessLogic.Admin

Namespace BACRM.UserInterface.Admin
    Public Class frmAdvSerInvItems
        Inherits BACRMPage

        Dim objCommon As CCommon
        Public objGenericAdvSearch As New FormGenericAdvSearch
        Public dtGenericFormConfig As DataTable
        Public dsGenericFormConfig As DataSet

        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            Try

                'CLEAR ERROR ON RELOAD
                DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ""
                DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "none")

                objCommon = New CCommon
                GetUserRightsForPage(9, 11)

                GetFormFieldList()

                GenericFormControlsGeneration.FormID = 29                                        'set the form id to Advance Search
                GenericFormControlsGeneration.DomainID = Session("DomainID")                    'Set the domain id
                GenericFormControlsGeneration.UserCntID = Session("UserContactID")                        'Set the User id
                GenericFormControlsGeneration.AuthGroupId = Session("UserGroupID")              'Set the User Authentication Group Id

                dtGenericFormConfig = dsGenericFormConfig.Tables(1)

                callFuncForFormGenerationNonAOI(dsGenericFormConfig)                        'Calls function for form generation and display for non AOI fields
                litMessage.Text = GenericFormControlsGeneration.getJavascriptArray(dsGenericFormConfig.Tables(1)) 'Create teh javascript array and store

            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Sub GetFormFieldList()
            Try
                If objGenericAdvSearch Is Nothing Then objGenericAdvSearch = New FormGenericAdvSearch

                objGenericAdvSearch.AuthenticationGroupID = Session("UserGroupID")
                objGenericAdvSearch.FormID = 29
                objGenericAdvSearch.DomainID = Session("DomainID")
                dsGenericFormConfig = objGenericAdvSearch.GetAdvancedSearchFieldList()

                dsGenericFormConfig.Tables(0).TableName = "SectionInfo"                           'give a name to the datatable
                dsGenericFormConfig.Tables(1).TableName = "AdvSearchFields"                           'give a name to the datatable

                dsGenericFormConfig.Tables(1).Columns.Add("vcNewFormFieldName", System.Type.GetType("System.String"), "[vcFormFieldName]") 'This columsn has to be an integer to tryign to manipupate the datatype as Compute(Max) does not work on string datatypes
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Sub callFuncForFormGenerationNonAOI(ByVal dsGenericFormConfig As DataSet)
            Try
                GenericFormControlsGeneration.boolAOIField = 0                                      'Set the AOI flag to non AOI
                GenericFormControlsGeneration.createDynamicFormControlsSectionWise(dsGenericFormConfig, plhControls, 29, Session("DomainID"), Session("UserContactID"))
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Private Sub DisplayError(ByVal exception As String)
            Try
                DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = exception
                DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
                DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Focus()
            Catch ex As Exception

            End Try
        End Sub
    End Class
End Namespace
