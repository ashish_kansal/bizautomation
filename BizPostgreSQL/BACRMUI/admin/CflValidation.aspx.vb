﻿Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Admin

Namespace BACRM.UserInterface.Admin
    Public Class CflValidation
        Inherits BACRMPage
        Dim lngFldID As Long
        Dim strFldType As String
        Public Const strFieldMessage = "Please enter a value of {$FieldName}."
        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            lngFldID = CCommon.ToLong(GetQueryStringVal("fldid"))
            strFldType = GetQueryStringVal( "fldType")
            Try
                If Not IsPostBack Then
                    txtMax.Attributes.Add("onkeypress", "CheckNumber(2,event)")
                    txtMin.Attributes.Add("onkeypress", "CheckNumber(2,event)")
                    BindData()
                    If strFldType = "SelectBox" Or strFldType = "DateField" Then
                        trTextboxValidation.Visible = False
                    End If
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub
        Private Sub BindData()
            Try
                Dim dtFldDetails As DataTable
                Dim objCusFld As New CustomFields
                objCusFld.CusFldId = lngFldID
                dtFldDetails = objCusFld.GetFieldDetails
                If dtFldDetails.Rows.Count > 0 Then
                    chkIsRequired.Checked = CCommon.ToBool(dtFldDetails.Rows(0)("bitIsRequired"))
                    chkFieldMessage.Checked = CCommon.ToBool(dtFldDetails.Rows(0)("bitFieldMessage"))
                    'If chkFieldMessage.Checked Then
                    '    txtFieldMessage.Text = dtFldDetails.Rows(0)("vcFieldMessage")
                    'End If
                    If CCommon.ToString(dtFldDetails.Rows(0)("vcFieldMessage")) = "" Then
                        txtFieldMessage.Text = strFieldMessage
                    Else
                        txtFieldMessage.Text = dtFldDetails.Rows(0)("vcFieldMessage")
                    End If
                    If CCommon.ToBool(dtFldDetails.Rows(0)("bitIsNumeric")) Then
                        rbIsNumeric.Checked = True
                    ElseIf CCommon.ToBool(dtFldDetails.Rows(0)("bitIsAlphaNumeric")) Then
                        rbIsAlphaNumeric.Checked = True
                    ElseIf CCommon.ToBool(dtFldDetails.Rows(0)("bitIsEmail")) Then
                        rbIsEmail.Checked = True
                    ElseIf CCommon.ToBool(dtFldDetails.Rows(0)("bitIsLengthValidation")) Then
                        rbRange.Checked = True
                        txtMin.Text = dtFldDetails.Rows(0)("intMinLength")
                        txtMax.Text = dtFldDetails.Rows(0)("intMaxLength")
                    End If

                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSave.Click
            Try
                Dim strEditedfldlst As String

                Dim dr As DataRow
                Dim dtFieldDetails As New DataTable
                dtFieldDetails.Columns.Add("numFieldId")
                dtFieldDetails.Columns.Add("bitIsRequired")
                dtFieldDetails.Columns.Add("bitIsNumeric")
                dtFieldDetails.Columns.Add("bitIsAlphaNumeric")
                dtFieldDetails.Columns.Add("bitIsEmail")
                dtFieldDetails.Columns.Add("bitIsLengthValidation")
                dtFieldDetails.Columns.Add("intMaxLength")
                dtFieldDetails.Columns.Add("intMinLength")
                dtFieldDetails.Columns.Add("bitFieldMessage")
                dtFieldDetails.Columns.Add("vcFieldMessage")
                dtFieldDetails.TableName = "Table"


                dr = dtFieldDetails.NewRow
                dr("numFieldId") = lngFldID
                dr("bitIsRequired") = IIf(chkIsRequired.Checked, "1", "0")
                dr("bitIsNumeric") = IIf(rbIsNumeric.Checked, "1", "0")
                dr("bitIsAlphaNumeric") = IIf(rbIsAlphaNumeric.Checked, "1", "0")
                dr("bitIsEmail") = IIf(rbIsEmail.Checked, "1", "0")
                dr("bitIsLengthValidation") = IIf(rbRange.Checked, "1", "0")
                dr("intMaxLength") = CCommon.ToInteger(txtMax.Text)
                dr("intMinLength") = CCommon.ToInteger(txtMin.Text)
                dr("bitFieldMessage") = IIf(chkFieldMessage.Checked, "1", "0")
                dr("vcFieldMessage") = CCommon.ToString(txtFieldMessage.Text)
                dtFieldDetails.Rows.Add(dr)


                Dim ds As New DataSet
                ds.Tables.Add(dtFieldDetails)
                strEditedfldlst = ds.GetXml()
                ds.Tables.Remove(dtFieldDetails)

                Dim objCusfld As New CustomFields()
                objCusfld.strEditedFldLst = strEditedfldlst
                objCusfld.CusFldId = lngFldID
                objCusfld.ManageCFValidation()
                Response.Write("<script>self.close();</script>")
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub
    End Class
End Namespace