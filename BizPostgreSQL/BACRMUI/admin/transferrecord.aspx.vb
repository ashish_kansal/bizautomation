Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Projects

Namespace BACRM.UserInterface.Admin
    Public Class transferrecord
        Inherits BACRMPage
        Protected WithEvents Table2 As System.Web.UI.WebControls.Table
        Protected WithEvents ddlEmployee As System.Web.UI.WebControls.DropDownList
        Protected WithEvents btnTransfer As System.Web.UI.WebControls.Button
        Protected WithEvents litMessage As System.Web.UI.WebControls.Literal
        Protected WithEvents btnCancel As System.Web.UI.WebControls.Button

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Try
                If Not IsPostBack Then sb_FillEmpList()

                hdnProjectID.Value = CCommon.ToLong(GetQueryStringVal("tyrCV"))
                hdnProjectName.Value = CCommon.ToString(GetQueryStringVal("ProjName"))
                hdnStageName.Value = CCommon.ToString(GetQueryStringVal("StageName"))

                btnCancel.Attributes.Add("onclick", "return Close()")
                btnTransfer.Attributes.Add("onclick", "return check()")
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub sb_FillEmpList()
            Try
                If GetQueryStringVal("frm") = "Opp" Then
                    
                    If Session("PopulateUserCriteria") = 1 Then
                        objCommon.OppID = CCommon.ToLong(GetQueryStringVal("pluYR"))
                        objCommon.charModule = "O"
                        objCommon.GetCompanySpecificValues1()
                        objCommon.sb_FillConEmpFromTerritories(ddlEmployee, Session("DomainID"), 0, 0, objCommon.TerittoryID)
                    ElseIf Session("PopulateUserCriteria") = 2 Then
                        objCommon.sb_FillConEmpFromDBUTeam(ddlEmployee, Session("DomainID"), Session("UserContactID"))
                    Else : objCommon.sb_FillConEmpFromDBSel(ddlEmployee, Session("DomainID"), 0, 0)
                    End If

                ElseIf GetQueryStringVal("frm") = "Projects" Then
                    
                    If GetQueryStringVal("StageId") <> "" Then
                        lblTab.Text = "Assign Stage"
                        btnTransfer.Text = "Transfer Assignee"
                        hplEmpAvaliability.Visible = True
                    End If
                    If Session("PopulateUserCriteria") = 1 Then
                        objCommon.ProID = CCommon.ToLong(GetQueryStringVal("tyrCV"))
                        objCommon.charModule = "P"
                        objCommon.GetCompanySpecificValues1()
                        objCommon.sb_FillConEmpFromTerritories(ddlEmployee, Session("DomainID"), 0, 0, objCommon.TerittoryID)
                    ElseIf Session("PopulateUserCriteria") = 2 Then
                        objCommon.sb_FillConEmpFromDBUTeam(ddlEmployee, Session("DomainID"), Session("UserContactID"))
                    Else : objCommon.sb_FillConEmpFromDBSel(ddlEmployee, Session("DomainID"), 0, 0)
                    End If


                    'External User for Project
                    Dim dtUserList As DataTable
                    Dim objProject As New Project
                    objProject.DomainID = Session("DomainId")
                    objProject.ProjectID = CCommon.ToLong(GetQueryStringVal("tyrCV"))

                    objProject.bytemode = 2 '1:Internal User 2:External User
                    dtUserList = objProject.GetProjectTeamRights()

                    Dim item As ListItem
                    Dim dr As DataRow
                    For Each dr In dtUserList.Rows
                        item = New ListItem()
                        item.Text = dr("vcCompanyName") & " - " & dr("vcUserName")
                        item.Value = dr("numContactID")
                        ddlEmployee.Items.Add(item)
                    Next
                ElseIf GetQueryStringVal("frm") = "contacts" Then
                    
                    If Session("PopulateUserCriteria") = 1 Then
                        objCommon.ContactID = CCommon.ToLong(GetQueryStringVal("uihTR"))
                        objCommon.charModule = "C"
                        objCommon.GetCompanySpecificValues1()
                        objCommon.sb_FillConEmpFromTerritories(ddlEmployee, Session("DomainID"), 0, 0, objCommon.TerittoryID)
                    ElseIf Session("PopulateUserCriteria") = 2 Then
                        objCommon.sb_FillConEmpFromDBUTeam(ddlEmployee, Session("DomainID"), Session("UserContactID"))
                    Else : objCommon.sb_FillConEmpFromDBSel(ddlEmployee, Session("DomainID"), 0, 0)
                    End If
                ElseIf GetQueryStringVal("frm") = "Cases" Then
                    
                    If Session("PopulateUserCriteria") = 1 Then
                        objCommon.CaseID = CCommon.ToLong(GetQueryStringVal("fghTY"))
                        objCommon.charModule = "S"
                        objCommon.GetCompanySpecificValues1()
                        objCommon.sb_FillConEmpFromTerritories(ddlEmployee, Session("DomainID"), 0, 0, objCommon.TerittoryID)
                    ElseIf Session("PopulateUserCriteria") = 2 Then
                        objCommon.sb_FillConEmpFromDBUTeam(ddlEmployee, Session("DomainID"), Session("UserContactID"))
                    Else : objCommon.sb_FillConEmpFromDBSel(ddlEmployee, Session("DomainID"), 0, 0)
                    End If
                Else
                    
                    If Session("PopulateUserCriteria") = 1 Then
                        objCommon.DivisionID = CCommon.ToLong(GetQueryStringVal("rtyWR"))
                        objCommon.charModule = "D"
                        objCommon.GetCompanySpecificValues1()
                        objCommon.sb_FillConEmpFromTerritories(ddlEmployee, Session("DomainID"), 0, 0, objCommon.TerittoryID)
                    ElseIf Session("PopulateUserCriteria") = 2 Then
                        objCommon.sb_FillConEmpFromDBUTeam(ddlEmployee, Session("DomainID"), Session("UserContactID"))
                    Else : objCommon.sb_FillConEmpFromDBSel(ddlEmployee, Session("DomainID"), 0, 0)
                    End If
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub btnTransfer_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnTransfer.Click
            Try
                Dim objUserAccess As New UserAccess
                objUserAccess.UserCntID = ddlEmployee.SelectedItem.Value
                If GetQueryStringVal("frm") = "Opp" Then
                    objUserAccess.RecID = CCommon.ToLong(GetQueryStringVal("pluYR"))
                    objUserAccess.byteMode = 1
                ElseIf GetQueryStringVal("frm") = "Projects" Then
                    If GetQueryStringVal("StageId") <> "" Then
                        objUserAccess.RecID = CCommon.ToLong(GetQueryStringVal("StageId"))
                        objUserAccess.byteMode = 5
                    Else
                        objUserAccess.RecID = CCommon.ToLong(GetQueryStringVal("tyrCV"))
                        objUserAccess.byteMode = 2
                    End If
                ElseIf GetQueryStringVal("frm") = "contacts" Then
                    objUserAccess.RecID = CCommon.ToLong(GetQueryStringVal("uihTR"))
                    objUserAccess.byteMode = 3
                ElseIf GetQueryStringVal("frm") = "Cases" Then
                    objUserAccess.RecID = CCommon.ToLong(GetQueryStringVal("fghTY"))
                    objUserAccess.byteMode = 4
                Else
                    objUserAccess.RecID = CCommon.ToLong(GetQueryStringVal("rtyWR"))
                    objUserAccess.byteMode = 0
                End If
                objUserAccess.DomainID = Session("DomainID")
                objUserAccess.TransferOwnership()
                litMessage.Text = "Sucessfully transfered"

                If GetQueryStringVal("frm") = "Projects" Then
                    Response.Write("<script language='javascript'>opener.__doPostBack('btnAssignChange', ''); self.close();</script>")
                Else
                    Response.Write("<script language='javascript'>opener.location.reload(true); self.close();</script>")
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

    End Class
End Namespace