﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmAlertConfig.aspx.vb"
    MasterPageFile="~/common/Popup.Master" Inherits=".frmAlertConfig" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .Tab {
            background-image: url(../images/ig_tab_winXPs3.gif);
            background-repeat: no-repeat;
            padding-left: 10px;
            color: white;
            font-family: Arial;
            font-size: 11px;
            font-weight: bold;
            height: 23px;
        }
    </style>
    <script type="text/javascript">
        function CheckNumber(cint, e) {
            var k;
            document.all ? k = e.keyCode : k = e.which;
            if (cint == 1) {
                if (!(k > 47 && k < 58 || k == 44 || k == 46 || k == 8 || k == 37 || k == 39 || k == 16)) {
                    if (e.preventDefault) {
                        e.preventDefault();
                    }
                    else
                        e.returnValue = false;
                    return false;
                }
            }
            if (cint == 2) {
                if (!(k > 47 && k < 58 || k == 8 || k == 37 || k == 39 || k == 16)) {
                    if (e.preventDefault) {
                        e.preventDefault();
                    }
                    else
                        e.returnValue = false;
                    return false;
                }
            }
        }
        function Close() {
            //opener.location.reload(true);
            window.close();
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
    <div>
        <table>
            <tr>
                <td colspan="2" style="font-size: 12px;"><b>These alerts are possible only If the inbox message is tied to contact in BizAutomation</b></td>
            </tr>
            <tr>
                <td colspan="2">
                    <br />
                </td>
            </tr>
            <tr style="height:30px;">
                <td>
                    <asp:CheckBox ID="chkARBalance" runat="server" /></td>
                <td>If the Contact’s Organization belongs to has an A/R Balance display this:&nbsp;<img alt="" src="../images/dollar.png" style="height: 16px; vertical-align: middle; padding-bottom: 2px;" />
                </td>
            </tr>
            <tr style="height:30px;">
                <td>
                    <asp:CheckBox ID="chkOrder" runat="server" Text="" />
                </td>
                <td>If the Contact’s Organization has open Sales Opportunities / Orders display this:&nbsp;<img alt="" src="../images/icons/cart.png" style="height: 16px; vertical-align: middle; padding-bottom: 2px;" />
                </td>
            </tr>
            <tr style="height:30px;">
                <td>
                    <asp:CheckBox ID="chkCase" runat="server" Text="" /></td>
                <td>If the Contact’s Organization has open Cases display this:&nbsp;<img alt="" src="../images/icons/headphone_mic.png" style="height: 16px; vertical-align: middle; padding-bottom: 2px;" /></td>
            </tr>
            <tr style="height:30px;">
                <td>
                    <asp:CheckBox ID="chkProject" runat="server" Text="" /></td>
                <td>If the Contact’s Organization has open Projects display this:&nbsp;<img alt="" src="../images/Compass-16.gif" style="height: 16px; vertical-align: middle; padding-bottom: 2px;" /></td>
            </tr>
            <tr style="height:40px;">
                <td>
                    <asp:CheckBox ID="chkMessages" runat="server" Text="" />
                </td>
                <td>If the Contact the message belongs to has unread Email Messages, show how many with a link similar to the example shown here:&nbsp;<label style="color: blue;">(<span style="text-decoration: underline">2</span>)</label>
                </td>
            </tr>
            <tr style="height:50px;">
                <td>
                    <asp:CheckBox ID="chkCampaign" runat="server" Text="" />
                </td>
                <td>If the Contact the message belongs to has a Follow-up Campaign, display the campaign’s status icon:&nbsp;<img alt="" src="../images/comflag.png" style="height: 16px; vertical-align: middle; padding-bottom: 2px;" />&nbsp;Or&nbsp;<img alt="" src="../images/GreenFlag.png" style="height: 16px; vertical-align: middle; padding-bottom: 2px;" /></td>
            </tr>
            <tr style="height:30px;">
                <td>
                    <asp:CheckBox ID="chkTickler" runat="server" Text="" />
                </td>
                <td>If Contact the message belongs to has an open Tickler display this:&nbsp;<img alt="" src="../images/MasterList-16.gif" style="height: 16px; vertical-align: middle; padding-bottom: 2px;" /></td>
            </tr>
        </table>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitle" runat="server">
    Email Alert Panel Configuration
</asp:Content>
<asp:Content ID="Content4" runat="server" ContentPlaceHolderID="FiltersAndViews1">
    <div class="right-input">
        <div class="input-part">
            <table align="right">
                <tr>
                    <td align="right">
                        <asp:Button ID="btnSave" CssClass="button" runat="server" Text="Save" />
                        &nbsp;
                        <asp:Button ID="btnSaveClose" CssClass="button" runat="server" Text="Save & Close" />
                        &nbsp;
                        <asp:Button ID="btnClose" CssClass="button" runat="server" Text="Close" OnClientClick="close();" />
                        &nbsp;
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>
