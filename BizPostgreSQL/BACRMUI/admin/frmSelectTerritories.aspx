<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" Codebehind="frmSelectTerritories.aspx.vb" Inherits="BACRM.UserInterface.Admin.frmSelectTerritories" ValidateRequest="false" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
	<head  runat="server">
        <link rel="stylesheet" href="~/CSS/master.css" type="text/css" />
		<title>Select Territories</title>
		<script language="javascript" src="../javascript/SelectTerritories.js"></script>
	</HEAD>
	<body >
		<form id="frmSelectTerritories" method="post" runat="server">
			<table cellSpacing="0" cellPadding="0" width="100%" border="0">
				<tr>
					<td colSpan="3" height=7>
					</td>
				</tr>
				<tr>
					<td vAlign="bottom">
						<table class="TabStyle">
							<tr>
								<td>&nbsp;&nbsp;&nbsp;Select 
									Territories&nbsp;&nbsp;&nbsp;
								</td>
							</tr>
						</table>
					</td>
					<td align="right">
						<asp:button id="btnSave" Runat="server" Text="Save &amp; Close" CssClass="button"></asp:button>
						<input type="button" id="btnClose" Runat="server" Value="Close" Class="button" onclick="javascript: CloseThisWin();"></td>
				</tr>
				<tr>
					<td colSpan="3"><asp:table id="tblTeams" Runat="server" BorderColor="black" GridLines="None" Width="100%" CellSpacing="0" CssClass="aspTable"
							CellPadding="0" BorderWidth="1">
							<asp:TableRow>
								<asp:TableCell>
									<table>
										<TR>
											<TD class="normal1" nowrap>
												Available Territories<br>
												<br>
												<asp:listbox id="lstTerritoriesAvail" runat="server" Width="200" Height="80" CssClass="signup" SelectionMode="Multiple"></asp:listbox>
											</TD>
											<td align="center">
												<input type="button" id="btnAdd" Class="button" Value="Add >" onclick="javascript:move(document.frmSelectTerritories.lstTerritoriesAvail,document.frmSelectTerritories.lstTerritoriesSelected)">
												<br>
												<input type="button" id="btnRemove" Class="button" Value="< Remove" onclick="javascript:move(document.frmSelectTerritories.lstTerritoriesSelected,document.frmSelectTerritories.lstTerritoriesAvail)">
											</td>
											<td class="normal1">
												The user will ONLY be able to access
												<br>
												records from the following territories.<br>
												<asp:listbox id="lstTerritoriesSelected" Width="200" Height="80" runat="server" CssClass="signup" SelectionMode="Multiple"></asp:listbox>
											</td>
										</TR>
									</table>
								</asp:TableCell>
							</asp:TableRow>
						</asp:table>
					</td>
				</tr>
			</table>
			<asp:Literal ID="ltClientScript" Runat="server"></asp:Literal>
			<input type="text" name="hdXMLString" ID="hdXMLString" Runat="server" style="DISPLAY:none">
		</form>
	</body>
</HTML>
