﻿Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Common
Imports System.Linq
Imports System.Web.UI.WebControls
Imports System.Collections.Generic

Public Class frmUnitPriceApprover
    Inherits BACRMPage
#Region "Member Variables"
    Private objUserAccess As UserAccess
#End Region

#Region "Page Events"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            litMessage.Text = ""

            If Not Page.IsPostBack Then
                hdnUnitPriceApprover.Value = GetQueryStringVal("ID")
                BindData()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

#End Region

#Region "Private Methods"

    Private Sub BindData()
        Try
            Dim dtUserList As DataTable
            Dim objUserList As New UserGroups


            With objUserList
                .UserId = Session("UserID")
                .DomainID = Session("DomainID")
                .SortCharacter = "0"
                .CurrentPage = 1
                .PageSize = 1000
                .TotalRecords = 0
                .BitStatus = 1
                .ColumnName = "Name"
                .columnSortOrder = "Asc"
            End With

            Dim ds As DataSet
            ds = objUserList.GetUserListAuthorisedApprovers
            dtUserList = ds.Tables(0)

            chklUnitPriceApprover.DataSource = dtUserList
            chklUnitPriceApprover.DataTextField = "Name"
            chklUnitPriceApprover.DataValueField = "numUserID"
            chklUnitPriceApprover.DataBind()

            Dim listSelectedUsers As List(Of String)
            listSelectedUsers = hdnUnitPriceApprover.Value.Split(",").ToList()

            If listSelectedUsers.Count > 0 Then
                For Each var As String In listSelectedUsers
                    If Not chklUnitPriceApprover.Items.FindByValue(var) Is Nothing Then
                        chklUnitPriceApprover.Items.FindByValue(var).Selected = True
                    End If
                Next
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

#End Region

#Region "Event Hanlers"

    Private Sub btnSaveClose_Click(sender As Object, e As EventArgs) Handles btnSaveClose.Click
        Try
            Dim selected As New List(Of ListItem)
            selected = chklUnitPriceApprover.Items.Cast(Of ListItem)().Where(Function(li) li.Selected).ToList()

            If selected.Count > 0 Then
                Dim selectedIds As String
                selectedIds = String.Join(",", selected.Select(Function(li) li.Value).ToArray())

                Dim objAdmin As New CAdmin
                objAdmin.DomainID = CCommon.ToLong(Session("DomainID"))
                objAdmin.SaveUnitPriceApprover(selectedIds)

                ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "SetSelectedIDS", "SetSelectedUnitPriceApprover('" & selectedIds & "');", True)
            Else
                litMessage.Text = "Please select at least one user as approver."
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

#End Region

End Class