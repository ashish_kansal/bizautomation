Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Common
Imports System.IO
Imports QueryStringVal
Imports System.Net
Imports System.Text.RegularExpressions
Imports BACRM.BusinessLogic.Accounting
Imports BACRM.BusinessLogic.ShioppingCart
Imports BACRM.BusinessLogic.Item
Imports Telerik.Web.UI
Imports System.Collections.Generic
Imports Amazon
Imports Amazon.SimpleEmail.Model
Imports Amazon.SimpleEmail

Namespace BACRM.UserInterface.Admin
    Partial Public Class frmDomainDetails
        Inherits BACRMPage

        Private Property chkClassTracking As Object
        Dim objShippingRule As ShippingRule
        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            Try
                Session("GoogleContactId") = 0
                lblError.Text = ""
                DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ""
                DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "none")

                hplSMTP.Attributes.Add("onclick", "return OpenSMTPPopUp(" & Session("DomainId") & ",3)")
                hplSupportIMAP.Attributes.Add("onclick", "return OpenSMTPPopUp(-1,2)")
                hplSupportSMTP.Attributes.Add("onclick", "return OpenSMTPPopUp(-1,4)")

                If Session("UserContactID") <> Session("AdminID") Then 'Logged in User is Admin of Domain? yes then override permission and give him access
                    Dim m_aryRightsForABDocs() As Integer

                    GetUserRightsForPage(MODULEID.Administration, 26)

                    If m_aryRightsForPage(RIGHTSTYPE.UPDATE) = 0 Then
                        btnSave.Visible = False
                    End If
                    m_aryRightsForABDocs = GetUserRightsForPage(MODULEID.Administration, 20)
                    If m_aryRightsForABDocs(RIGHTSTYPE.VIEW) = 0 Then
                        hplAuthoritativeBizDocs.Visible = False
                    End If
                End If
                If ChartOfAccounting.GetDefaultAccount("SI", Session("DomainID")) = 0 Then
                    ClientScript.RegisterStartupScript(Me.GetType, "alert", "alert('Please set default account for shipping from Administration->Global Settings->Accounting->Default Accounts and try again');", True)
                    btnNew.Visible = False
                    Exit Sub
                End If
                'chkPrimaryAddress.Checked = True
                If Not IsPostBack Then
                    BindEmployeeList()
                    BindEmployeeGroup()
                    BindDropDowns()
                    BindRelationshipsProfiles()
                    LoadDetails()
                    FillDepositTocombo()
                    BindBizDoc(1, ddlDefaultSalesBizDoc)
                    ddlDefaultSalesBizDoc.Items.Insert(0, New ListItem("--Select--", 0))
                    BindBizDoc(2, ddlDefaultPurchaseBizDoc)
                    ddlDefaultPurchaseBizDoc.Items.Insert(0, New ListItem("--Select--", 0))
                    LoadDomainDetails()
                    BindShippingCompanyDataGrid()
                    BindServiceTypes()
                    'Added By:Sachin Sadhu||Date:27thAug2014
                    'Purpose : Add PayBill data to Accounting tab(No more required Pay Bill WFA settings separatly)
                    BindData()
                    BindRuleData()
                    'end of code

                    txtWebAly.Text = txtWebAly.Text.Replace("{#TrackID#}", Session("DomainID"))
                End If
                hplDupSettings.Attributes.Add("onclick", "return OpenSetting(1)")
                hplConfigEmbeddedCost.Attributes.Add("onclick", "return OpenSetting(2)")
                hplUnit.Attributes.Add("onclick", "return OpenSetting(3)")
                hpUOMConversion.Attributes.Add("onclick", "return OpenSetting(4)")
                btnSave.Attributes.Add("onclick", "return Save()")
                CCommon.InitializeOrganizationClientSideTemplate(Session("DomainID"), Session("UserContactID"), radCmbCompany)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Sub BindDropDowns()
            Try
                'objCommon.sb_FillComboFromDBwithSel(ddlRentalItemClass, 381, Session("DomainID"))

                For i As Integer = 0 To 100
                    ddlAuthorizePercentage.Items.Add(New ListItem(i, i))
                Next





                'Dim objJournalEntry As New JournalEntry
                Dim dtChartAcntDetails As DataTable
                Dim objCOA As New ChartOfAccounting
                objCOA.DomainID = Session("DomainId")
                objCOA.AccountCode = "0103"
                dtChartAcntDetails = objCOA.GetParentCategory()
                Dim item As ListItem
                For Each dr As DataRow In dtChartAcntDetails.Rows
                    item = New ListItem()
                    item.Text = dr("vcAccountName")
                    item.Value = dr("numAccountID")
                    item.Attributes("OptionGroup") = dr("vcAccountType")
                    ddlIncomeAccount.Items.Add(item)
                Next
                objCOA.AccountCode = "0101"
                dtChartAcntDetails = objCOA.GetParentCategory()
                For Each dr As DataRow In dtChartAcntDetails.Rows
                    item = New ListItem()
                    item.Text = dr("vcAccountName")
                    item.Value = dr("numAccountID")
                    item.Attributes("OptionGroup") = dr("vcAccountType")
                    ddlAssetAccount.Items.Add(item)
                Next
                objCOA.AccountCode = "0106"
                dtChartAcntDetails = objCOA.GetParentCategory()
                For Each dr As DataRow In dtChartAcntDetails.Rows
                    item = New ListItem()
                    item.Text = dr("vcAccountName")
                    item.Value = dr("numAccountID")
                    item.Attributes("OptionGroup") = dr("vcAccountType")
                    ddlCOGS.Items.Add(item)
                Next
                ddlIncomeAccount.Items.Insert(0, New ListItem("--Select One --", "0"))
                ddlAssetAccount.Items.Insert(0, New ListItem("--Select One --", "0"))
                ddlCOGS.Items.Insert(0, New ListItem("--Select One --", "0"))

                Dim objItems As New BACRM.BusinessLogic.Item.CItems
                objItems.str = ""
                objItems.DomainID = CCommon.ToLong(Session("DomainID"))
                Dim dtServiceItem As DataTable = objItems.GetUnArchivedServiceItems

                ddlShippingServiceItem.DataValueField = "numItemCode"
                ddlShippingServiceItem.DataTextField = "vcItemName"
                ddlShippingServiceItem.DataSource = dtServiceItem
                ddlShippingServiceItem.DataBind()
                ddlShippingServiceItem.Items.Insert(0, "--Select One--")
                ddlShippingServiceItem.Items.FindByText("--Select One--").Value = 0

                ddlDiscountServiceItem.DataValueField = "numItemCode"
                ddlDiscountServiceItem.DataTextField = "vcItemName"
                ddlDiscountServiceItem.DataSource = dtServiceItem
                ddlDiscountServiceItem.DataBind()
                ddlDiscountServiceItem.Items.Insert(0, New ListItem("--Select One--", "0"))

                ddlOverheadServiceItem.DataValueField = "numItemCode"
                ddlOverheadServiceItem.DataTextField = "vcItemName"
                ddlOverheadServiceItem.DataSource = dtServiceItem
                ddlOverheadServiceItem.DataBind()
                ddlOverheadServiceItem.Items.Insert(0, New ListItem("--Select One--", "0"))

                ddlUnMappedItem.DataValueField = "numItemCode"
                ddlUnMappedItem.DataTextField = "vcItemName"
                ddlUnMappedItem.DataSource = dtServiceItem
                ddlUnMappedItem.DataBind()
                ddlUnMappedItem.Items.Insert(0, New ListItem("--Select One--", "0"))

                objCommon.sb_FillComboFromDBwithSel(ddlCountry, 40, Session("DomainID"))
                'objCommon.sb_FillComboFromDBwithSel(ddlShipCompany, 82, Session("DomainID"))
                If objCommon Is Nothing Then objCommon = New CCommon
                radShipCompany.DataSource = objCommon.GetMasterListItems(82, Session("DomainID"))
                radShipCompany.DataTextField = "vcData"
                radShipCompany.DataValueField = "numListItemID"
                radShipCompany.DataBind()

                Dim listItem As Telerik.Web.UI.RadComboBoxItem
                If radShipCompany.Items.FindItemByText("Fedex") IsNot Nothing Then
                    radShipCompany.Items.Remove(radShipCompany.Items.FindItemByText("Fedex"))
                End If
                If radShipCompany.Items.FindItemByText("UPS") IsNot Nothing Then
                    radShipCompany.Items.Remove(radShipCompany.Items.FindItemByText("UPS"))
                End If
                If radShipCompany.Items.FindItemByText("USPS") IsNot Nothing Then
                    radShipCompany.Items.Remove(radShipCompany.Items.FindItemByText("USPS"))
                End If

                listItem = New Telerik.Web.UI.RadComboBoxItem
                listItem.Text = "-- Select One --"
                listItem.Value = "0"
                radShipCompany.Items.Insert(0, listItem)

                listItem = New Telerik.Web.UI.RadComboBoxItem
                listItem.Text = "Fedex"
                listItem.Value = "91"
                listItem.ImageUrl = "../images/FedEx.png"
                radShipCompany.Items.Insert(1, listItem)

                listItem = New Telerik.Web.UI.RadComboBoxItem
                listItem.Text = "UPS"
                listItem.Value = "88"
                listItem.ImageUrl = "../images/UPS.png"
                radShipCompany.Items.Insert(2, listItem)

                listItem = New Telerik.Web.UI.RadComboBoxItem
                listItem.Text = "USPS"
                listItem.Value = "90"
                listItem.ImageUrl = "../images/USPS.png"
                radShipCompany.Items.Insert(3, listItem)

                Dim objCurrency As New CurrencyRates
                objCurrency.DomainID = Session("DomainID")
                objCurrency.GetAll = 0
                ddlCurrency.DataSource = objCurrency.GetCurrencyWithRates()
                ddlCurrency.DataTextField = "vcCurrencyDesc"
                ddlCurrency.DataValueField = "numCurrencyID"
                ddlCurrency.DataBind()
                ddlCurrency.Items.Insert(0, "--Select One--")
                ddlCurrency.Items.FindByText("--Select One--").Value = "0"

                BindBizDoc(1, ddlSalesOppBizDoc)
                ddlSalesOppBizDoc.Items.Insert(0, New ListItem("--Select--", 0))

                BindBizDoc(2, ddlPODropShipBizDoc)
                ddlPODropShipBizDoc.Items.Insert(0, New ListItem("--Select--", 0))

                BindBizDocsTemplate()

                Dim dtTable As DataTable
                Dim objSite As New Sites
                objSite.DomainID = CCommon.ToLong(Session("DomainID"))
                dtTable = objSite.GetSites()

                ddlSites.DataSource = dtTable
                ddlSites.DataTextField = "vcSiteName"
                ddlSites.DataValueField = "numSiteID"
                ddlSites.DataBind()
                ddlSites.Items.Insert(0, New ListItem("-- Select One --", 0))

                objCommon.sb_FillComboFromDBwithSel(ddlARContactPosition, 41, Session("DomainID"))

                objCommon.sb_FillComboFromDBwithSel(rcbItemGroupsDynamicCost, 36, Session("DomainID"))
                rcbItemGroupsDynamicCost.FindItemByValue("0").Remove()
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub BindBizDoc(ByVal BizocType As Short, ByVal ddlBizDoc As DropDownList)
            Try
                objCommon.DomainID = Session("DomainID")
                objCommon.BizDocType = BizocType
                Dim dtTable As DataTable
                dtTable = objCommon.GetBizDocType()
                For Each dr As DataRow In dtTable.Rows
                    If dr("numListItemID") = CCommon.ToLong(Session("AuthoritativeSalesBizDoc")) Then
                        dr("vcData") = dr("vcData") & " - Authoritative"
                    End If
                Next
                ddlBizDoc.DataSource = dtTable
                ddlBizDoc.DataTextField = "vcData"
                ddlBizDoc.DataValueField = "numListItemID"
                ddlBizDoc.DataBind()
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub FillDepositTocombo()
            Try
                Dim dtChartAcntDetails As DataTable
                Dim objCOA As New ChartOfAccounting
                objCOA.DomainID = Session("DomainId")
                objCOA.AccountCode = "010101"
                dtChartAcntDetails = objCOA.GetParentCategory()

                'Exclude AR As per discussion with jeff. since it created AR aging summary report balance off.
                Dim drArray() As DataRow = dtChartAcntDetails.Select("vcAccountTypeCode <> '01010105'")
                If drArray.Length > 0 Then
                    dtChartAcntDetails = drArray.CopyToDataTable()
                End If


                Dim item As ListItem
                For Each dr As DataRow In dtChartAcntDetails.Rows
                    item = New ListItem()
                    item.Text = dr("vcAccountName")
                    item.Value = dr("numAccountID")
                    item.Attributes("OptionGroup") = dr("vcAccountType")
                    ddlDepositTo.Items.Add(item)
                Next
                ddlDepositTo.Items.Insert(0, New ListItem("--Select One --", "0"))
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub BindRelationshipsProfiles()
            Try
                If objShippingRule Is Nothing Then
                    objShippingRule = New ShippingRule
                End If

                objShippingRule.DomainID = Session("DomainID")

                ddlRelProfile.DataSource = objShippingRule.GetShippingRelationshipsProfiles
                ddlRelProfile.DataTextField = "vcRelProfile"
                ddlRelProfile.DataValueField = "numRelProfileID"
                ddlRelProfile.DataBind()

                ddlRelProfile.Items.Insert(0, "--All Relationships & Profiles--")
                ddlRelProfile.Items.FindByText("--All Relationships & Profiles--").Value = "0"
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub LoadDetails()
            Try
                If objShippingRule Is Nothing Then
                    objShippingRule = New ShippingRule
                End If
                objShippingRule.RuleID = 0
                objShippingRule.byteMode = 1
                objShippingRule.DomainID = Session("DomainID")
                objShippingRule.RelationshipProfileId = ddlRelProfile.SelectedValue

                Dim ds As DataSet = objShippingRule.GetShippingRule

                Dim dtGlobalValues As DataTable = ds.Tables(0)

                txtminShippingCost.Text = dtGlobalValues.Rows(0)("minShippingCost").ToString()
                chkbxEnableStaticShip.Checked = CCommon.ToBool(dtGlobalValues.Rows(0)("bitEnableStaticShippingRule"))
                chkbxExcludeClassifications.Checked = CCommon.ToBool(dtGlobalValues.Rows(0)("bitEnableShippingExceptions"))

                dgShippingRuleList.DataSource = ds.Tables(1)
                dgShippingRuleList.DataBind()
                'End If

            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        Sub BindEmployeeGroup()
            Dim dtAuthenticationGroups As DataTable                                 'declare a datatable
            Dim objConfigWizard As New FormConfigWizard
            objConfigWizard.AuthenticationGroupID = 0                               'Represents an flag for all groups
            objConfigWizard.DomainID = Session("DomainId")
            dtAuthenticationGroups = objConfigWizard.getAuthenticationGroups()      'call to get the authentication groups
            ddlUserGroup.DataSource = dtAuthenticationGroups.DefaultView 'set the datasource
            ddlUserGroup.DataTextField = "vcGroupName"             'set the text attribute
            ddlUserGroup.DataValueField = "numGroupID"             'set the value attribute
            ddlUserGroup.DataBind()
            ddlUserGroup.Items.Insert(0, New ListItem("-Select-", "0"))
        End Sub
        Sub SetEmployeeByGroupId()
            If ddlUserGroup.SelectedValue > 0 Then
                Dim dtUserBasedOnGroup As DataTable
                dtUserBasedOnGroup = objCommon.ConEmpListByGroupId(Session("DomainID"), ddlUserGroup.SelectedValue)

                rcbPurchaseOrderApproval.ClearCheckedItems()
                rcbARInvoiceDue.ClearCheckedItems()
                rcbAPBillsDue.ClearCheckedItems()
                rcbItemsTOPickPackShip.ClearCheckedItems()
                rcbItemToInvoice.ClearCheckedItems()
                rcbSalesOrderToClose.ClearCheckedItems()
                rcbItemsToPutAway.ClearCheckedItems()
                rcbItemsToBill.ClearCheckedItems()
                rcbPOSToClose.ClearCheckedItems()
                rcbPurchaseorderToClose.ClearCheckedItems()
                rcbWorkOrderBOMS.ClearCheckedItems()
                rcbPriceMarginApproval.ClearCheckedItems()

                rcbPurchaseOrderApproval.ClearSelection()
                rcbARInvoiceDue.ClearSelection()
                rcbAPBillsDue.ClearSelection()
                rcbItemsTOPickPackShip.ClearSelection()
                rcbItemToInvoice.ClearSelection()
                rcbSalesOrderToClose.ClearSelection()
                rcbItemsToPutAway.ClearSelection()
                rcbItemsToBill.ClearSelection()
                rcbPOSToClose.ClearSelection()
                rcbPurchaseorderToClose.ClearSelection()
                rcbWorkOrderBOMS.ClearSelection()
                rcbPriceMarginApproval.ClearSelection()

                rcbPurchaseOrderApproval.ClearSelection()
                rcbPurchaseOrderApproval.ClearCheckedItems()
                rcbPurchaseOrderApproval.SelectedIndex = -1
                For Each dr As DataRow In dtUserBasedOnGroup.Rows
                    SetCheckedItemString(rcbPurchaseOrderApproval, CCommon.ToString(dr("numContactID")))
                    SetCheckedItemString(rcbARInvoiceDue, CCommon.ToString(dr("numContactID")))
                    SetCheckedItemString(rcbAPBillsDue, CCommon.ToString(dr("numContactID")))
                    SetCheckedItemString(rcbItemsTOPickPackShip, CCommon.ToString(dr("numContactID")))
                    SetCheckedItemString(rcbItemToInvoice, CCommon.ToString(dr("numContactID")))
                    SetCheckedItemString(rcbSalesOrderToClose, CCommon.ToString(dr("numContactID")))
                    SetCheckedItemString(rcbItemsToPutAway, CCommon.ToString(dr("numContactID")))
                    SetCheckedItemString(rcbItemsToBill, CCommon.ToString(dr("numContactID")))
                    SetCheckedItemString(rcbPOSToClose, CCommon.ToString(dr("numContactID")))
                    SetCheckedItemString(rcbPurchaseorderToClose, CCommon.ToString(dr("numContactID")))
                    SetCheckedItemString(rcbWorkOrderBOMS, CCommon.ToString(dr("numContactID")))
                    SetCheckedItemString(rcbPriceMarginApproval, CCommon.ToString(dr("numContactID")))
                Next

            End If

        End Sub

        Sub BindEmployeeList()
            Try
                Dim objCommon As New CCommon
                Dim dt As DataTable
                dt = objCommon.ConEmpList(Session("DomainID"), 0, 0)

                rcbPurchaseOrderApproval.DataSource = dt
                rcbPurchaseOrderApproval.DataTextField = "vcUserName"
                rcbPurchaseOrderApproval.DataValueField = "numContactID"
                rcbPurchaseOrderApproval.DataBind()

                rcbARInvoiceDue.DataSource = dt
                rcbARInvoiceDue.DataTextField = "vcUserName"
                rcbARInvoiceDue.DataValueField = "numContactID"
                rcbARInvoiceDue.DataBind()

                rdbEnableUsers.DataSource = dt
                rdbEnableUsers.DataTextField = "vcUserName"
                rdbEnableUsers.DataValueField = "numContactID"
                rdbEnableUsers.DataBind()

                rcbSalesOrderToClose.DataSource = dt
                rcbSalesOrderToClose.DataTextField = "vcUserName"
                rcbSalesOrderToClose.DataValueField = "numContactID"
                rcbSalesOrderToClose.DataBind()


                rcbAPBillsDue.DataSource = dt
                rcbAPBillsDue.DataTextField = "vcUserName"
                rcbAPBillsDue.DataValueField = "numContactID"
                rcbAPBillsDue.DataBind()

                rcbItemsToPutAway.DataSource = dt
                rcbItemsToPutAway.DataTextField = "vcUserName"
                rcbItemsToPutAway.DataValueField = "numContactID"
                rcbItemsToPutAway.DataBind()

                rcbItemsTOPickPackShip.DataSource = dt
                rcbItemsTOPickPackShip.DataTextField = "vcUserName"
                rcbItemsTOPickPackShip.DataValueField = "numContactID"
                rcbItemsTOPickPackShip.DataBind()

                rcbItemsToBill.DataSource = dt
                rcbItemsToBill.DataTextField = "vcUserName"
                rcbItemsToBill.DataValueField = "numContactID"
                rcbItemsToBill.DataBind()

                rcbItemToInvoice.DataSource = dt
                rcbItemToInvoice.DataTextField = "vcUserName"
                rcbItemToInvoice.DataValueField = "numContactID"
                rcbItemToInvoice.DataBind()

                rcbPOSToClose.DataSource = dt
                rcbPOSToClose.DataTextField = "vcUserName"
                rcbPOSToClose.DataValueField = "numContactID"
                rcbPOSToClose.DataBind()

                rcbPriceMarginApproval.DataSource = dt
                rcbPriceMarginApproval.DataTextField = "vcUserName"
                rcbPriceMarginApproval.DataValueField = "numContactID"
                rcbPriceMarginApproval.DataBind()

                rcbPurchaseorderToClose.DataSource = dt
                rcbPurchaseorderToClose.DataTextField = "vcUserName"
                rcbPurchaseorderToClose.DataValueField = "numContactID"
                rcbPurchaseorderToClose.DataBind()

                rcbWorkOrderBOMS.DataSource = dt
                rcbWorkOrderBOMS.DataTextField = "vcUserName"
                rcbWorkOrderBOMS.DataValueField = "numContactID"
                rcbWorkOrderBOMS.DataBind()

            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub
        Private Sub dgShippingRuleList_DeleteCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgShippingRuleList.DeleteCommand
            Try
                objShippingRule = New ShippingRule
                objShippingRule.RuleID = e.Item.Cells(0).Text
                objShippingRule.DomainID = Session("DomainID")
                objShippingRule.DelShippingRule()
                LoadDetails()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub
        Private Sub dgShippingRuleList_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgShippingRuleList.ItemCommand
            Try
                If e.CommandName = "RuleID" Then
                    Response.Redirect("../ECommerce/frmShippingRule.aspx?RuleID=" & e.Item.Cells(0).Text)
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub
        Private Sub dgShippingRuleList_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgShippingRuleList.ItemDataBound
            Try
                If e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.Item Then
                    Dim btnDelete As Button
                    Dim lnkDelete As LinkButton
                    lnkDelete = e.Item.FindControl("lnkDelete")
                    btnDelete = e.Item.FindControl("btnDelete")

                    btnDelete.Attributes.Add("onclick", "return DeleteRecord()")
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Protected Sub ddlRelProfile_SelectedIndexChanged(sender As Object, e As EventArgs)
            LoadDetails()
        End Sub
        Protected Sub btnStaticShippingSave_Click(sender As Object, e As EventArgs)
            If objShippingRule Is Nothing Then
                objShippingRule = New ShippingRule
            End If

            objShippingRule.DomainID = Session("DomainID")
            objShippingRule.minShippingCost = CCommon.ToDouble(txtminShippingCost.Text)
            objShippingRule.bitEnableStaticShipping = chkbxEnableStaticShip.Checked
            objShippingRule.bitEnableShippingExceptions = chkbxExcludeClassifications.Checked

            objShippingRule.ManageShippingGlobalValues()

            LoadDetails()
        End Sub
        Sub LoadDomainDetails()
            Try
                Dim dtTable As DataTable
                Dim objUserAccess As New UserAccess
                objUserAccess.DomainID = Session("DomainID")
                dtTable = objUserAccess.GetDomainDetails()
                Dim strPassword As String
                Dim strDomainId As String = objCommon.Encrypt(Session("DomainId"))


                If dtTable.Rows.Count > 0 Then
                    If CCommon.ToLong(Session("AuthoritativeSalesBizDoc")) > 0 Then
                        ddlSalesOppBizDoc.Items.Remove(ddlSalesOppBizDoc.Items.FindByValue(CCommon.ToString(Session("AuthoritativeSalesBizDoc"))))
                    End If

                    txtDomain.Text = dtTable.Rows(0).Item("vcDomainName")
                    txtDesc.Text = dtTable.Rows(0).Item("vcDomainDesc")

                    If Not ddlAuthorizePercentage.Items.FindByValue(dtTable.Rows(0).Item("numAuthorizePercentage")) Is Nothing Then
                        ddlAuthorizePercentage.Items.FindByValue(dtTable.Rows(0).Item("numAuthorizePercentage")).Selected = True
                    End If
                    If Not ddlDefaultPurchaseBizDoc.Items.FindByValue(dtTable.Rows(0).Item("numDefaultPurchaseShippingDoc")) Is Nothing Then
                        ddlDefaultPurchaseBizDoc.Items.FindByValue(dtTable.Rows(0).Item("numDefaultPurchaseShippingDoc")).Selected = True
                    End If
                    If Not ddlDefaultSalesBizDoc.Items.FindByValue(dtTable.Rows(0).Item("numDefaultSalesShippingDoc")) Is Nothing Then
                        ddlDefaultSalesBizDoc.Items.FindByValue(dtTable.Rows(0).Item("numDefaultSalesShippingDoc")).Selected = True
                    End If
                    If Not ddlPaginRows.Items.FindByValue(dtTable.Rows(0).Item("tintCustomPagingRows")) Is Nothing Then
                        ddlPaginRows.Items.FindByValue(dtTable.Rows(0).Item("tintCustomPagingRows")).Selected = True
                    End If
                    If Not ddlChr.Items.FindByValue(dtTable.Rows(0).Item("tintChrForComSearch")) Is Nothing Then
                        ddlChr.Items.FindByValue(dtTable.Rows(0).Item("tintChrForComSearch")).Selected = True
                    End If
                    If Not ddlChrItemSearch.Items.FindByValue(dtTable.Rows(0).Item("tintChrForItemSearch")) Is Nothing Then
                        ddlChrItemSearch.Items.FindByValue(dtTable.Rows(0).Item("tintChrForItemSearch")).Selected = True
                    End If
                    If Not ddlDateFormat.Items.FindByValue(dtTable.Rows(0).Item("vcDateFormat")) Is Nothing Then
                        ddlDateFormat.Items.FindByValue(dtTable.Rows(0).Item("vcDateFormat")).Selected = True
                    End If
                    If Not ddlCountry.Items.FindByValue(dtTable.Rows(0).Item("numDefCountry")) Is Nothing Then
                        ddlCountry.Items.FindByValue(dtTable.Rows(0).Item("numDefCountry")).Selected = True
                    End If
                    If Not ddlComposeWindow.Items.FindByValue(dtTable.Rows(0).Item("tintComposeWindow")) Is Nothing Then
                        ddlComposeWindow.Items.FindByValue(dtTable.Rows(0).Item("tintComposeWindow")).Selected = True
                    End If
                    If Not ddlPopulateUserCrt.Items.FindByValue(dtTable.Rows(0).Item("tintAssignToCriteria")) Is Nothing Then
                        ddlPopulateUserCrt.Items.FindByValue(dtTable.Rows(0).Item("tintAssignToCriteria")).Selected = True
                    End If
                    If Not ddlFiscalStartMonth.Items.FindByValue(dtTable.Rows(0).Item("tintFiscalStartMonth")) Is Nothing Then
                        ddlFiscalStartMonth.Items.FindByValue(dtTable.Rows(0).Item("tintFiscalStartMonth")).Selected = True
                    End If

                    If Not ddlPayPeriod.Items.FindByValue(dtTable.Rows(0).Item("tintPayPeriod")) Is Nothing Then
                        ddlPayPeriod.Items.FindByValue(dtTable.Rows(0).Item("tintPayPeriod")).Selected = True
                    End If
                    If Not ddlCurrency.Items.FindByValue(dtTable.Rows(0).Item("numCurrencyID")) Is Nothing Then
                        ddlCurrency.Items.FindByValue(dtTable.Rows(0).Item("numCurrencyID")).Selected = True
                    End If
                    If Not ddlSites.Items.FindByValue(dtTable.Rows(0).Item("numDefaultSiteID")) Is Nothing Then
                        ddlSites.Items.FindByValue(dtTable.Rows(0).Item("numDefaultSiteID")).Selected = True
                    End If
                    chkActionItems.Checked = CCommon.ToBool(dtTable.Rows(0).Item("bitUseOnlyActionItems"))
                    hdnUnitSystem.Value = dtTable.Rows(0).Item("charUnitSystem")
                    chkInvoicing.Checked = CCommon.ToBool(dtTable.Rows(0).Item("bitInventoryInvoicing"))
                    If Not IsDBNull(dtTable.Rows(0).Item("numShipCompany")) Then
                        If Not radShipCompany.Items.FindItemByValue(dtTable.Rows(0).Item("numShipCompany")) Is Nothing Then
                            radShipCompany.Items.FindItemByValue(dtTable.Rows(0).Item("numShipCompany")).Selected = True
                        End If
                    End If

                    If CCommon.ToInteger(dtTable.Rows(0).Item("numCost")) = 3 Then
                        rdbPVCost.Checked = True
                    Else
                        rdbAvgCost.Checked = True
                    End If

                    txtSalesOrderTabs.Text = Convert.ToString(dtTable.Rows(0).Item("vcSalesOrderTabs"))
                    txtSalesQuotesTabs.Text = Convert.ToString(dtTable.Rows(0).Item("vcSalesQuotesTabs"))
                    txtItemPurchaseHistoryTabs.Text = Convert.ToString(dtTable.Rows(0).Item("vcItemPurchaseHistoryTabs"))
                    txtItemsFrequentlyPurchasedTabs.Text = Convert.ToString(dtTable.Rows(0).Item("vcItemsFrequentlyPurchasedTabs"))
                    txtOpenCasesTabs.Text = Convert.ToString(dtTable.Rows(0).Item("vcOpenCasesTabs"))
                    txtOpenRMATabs.Text = Convert.ToString(dtTable.Rows(0).Item("vcOpenRMATabs"))
                    txtSupportTabs.Text = Convert.ToString(dtTable.Rows(0).Item("vcSupportTabs"))

                    chkSalesOrderTabs.Checked = Convert.ToBoolean(dtTable.Rows(0).Item("bitSalesOrderTabs"))
                    chkSalesQuotesTabs.Checked = Convert.ToBoolean(dtTable.Rows(0).Item("bitSalesQuotesTabs"))
                    chkItemPurchaseHistoryTabs.Checked = Convert.ToBoolean(dtTable.Rows(0).Item("bitItemPurchaseHistoryTabs"))
                    chkItemsFrequentlyPurchasedTabs.Checked = Convert.ToBoolean(dtTable.Rows(0).Item("bitItemsFrequentlyPurchasedTabs"))
                    chkOpenCasesTabs.Checked = Convert.ToBoolean(dtTable.Rows(0).Item("bitOpenCasesTabs"))
                    chkOpenRMATabs.Checked = Convert.ToBoolean(dtTable.Rows(0).Item("bitOpenRMATabs"))
                    chkSupportTabs.Checked = Convert.ToBoolean(dtTable.Rows(0).Item("bitSupportTabs"))


                    'If Not ddlShipCompany.Items.FindByValue(dtTable.Rows(0).Item("numShipCompany")) Is Nothing Then
                    '    ddlShipCompany.Items.FindByValue(dtTable.Rows(0).Item("numShipCompany")).Selected = True
                    'End If
                    'chkOverRideAssigntoList.Checked = dtTable.Rows(0).Item("bitchkOverRideAssignto")
                    hfPortalLogo.Value = dtTable.Rows(0).Item("vcPortalLogo")
                    txtStartDate.Text = dtTable.Rows(0).Item("sintStartDate")
                    txtInterval.Text = dtTable.Rows(0).Item("sintNoofDaysInterval")
                    'chkInteMedPage.Checked = dtTable.Rows(0).Item("bitIntmedPage")
                    chkMultiCurrency.Checked = dtTable.Rows(0).Item("bitMultiCurrency")
                    'chkMultiCompany.Checked = dtTable.Rows(0).Item("bitMultiCompany")
                    chkNonIExpenseAccount.Checked = dtTable.Rows(0).Item("bitExpenseNonInventoryItem")

                    chkGtoBContact.Checked = dtTable.Rows(0).Item("bitGtoBContact")
                    chkBtoGContact.Checked = dtTable.Rows(0).Item("bitBtoGContact")
                    chkGtoBCalendar.Checked = dtTable.Rows(0).Item("bitGtoBCalendar")
                    chkBtoGCalendar.Checked = dtTable.Rows(0).Item("bitBtoGCalendar")
                    If chkGtoBCalendar.Checked = True OrElse chkBtoGCalendar.Checked = True Then
                        chkEnableCalender.Checked = True
                    Else
                        chkEnableCalender.Checked = False
                    End If

                    'chkInlineEdit.Checked = dtTable.Rows(0).Item("bitInlineEdit")
                    chkRemoveVendorPOValidation.Checked = Not CCommon.ToBool(dtTable.Rows(0).Item("bitRemoveVendorPOValidation"))
                    chkSearchOrderCustomerHistory.Checked = dtTable.Rows(0).Item("bitSearchOrderCustomerHistory")

                    chkAmountPastDue.Checked = True
                    txtAmountPastDue.Text = CCommon.ToDecimal(1)
                    'If dtTable.Rows(0).Item("bitDeferredIncome") = True Then
                    '    ChkDeferredIncome.Checked = True
                    'Else : ChkDeferredIncome.Checked = False
                    'End If
                    If dtTable.Rows(0).Item("bitAutoPopulateAddress") = True Then
                        chkPrimaryAddress.Checked = True
                        If Not ddlAddress.Items.FindByValue(dtTable.Rows(0).Item("tintPoulateAddressTo")) Is Nothing Then
                            ddlAddress.Items.FindByValue(dtTable.Rows(0).Item("tintPoulateAddressTo")).Selected = True
                        End If
                    Else : chkPrimaryAddress.Checked = False
                        ddlAddress.SelectedIndex = -1
                    End If
                    'If dtTable.Rows(0).Item("bitCreateInvoice") = True Then
                    '    chkCreateInvoice.Checked = True
                    'Else
                    '    chkCreateInvoice.Checked = False
                    'End If
                    'chkAutolinkUnappliedPayment.Checked = True
                    If dtTable.Rows(0).Item("bitAutolinkUnappliedPayment") = True Then
                        chkAutolinkUnappliedPayment.Checked = True
                    Else
                        chkAutolinkUnappliedPayment.Checked = False
                    End If

                    If dtTable.Rows(0).Item("bitDiscountOnUnitPrice") = True Then
                        chkDiscountOnUnitPrice.Checked = True
                    Else
                        chkDiscountOnUnitPrice.Checked = False
                    End If

                    'If dtTable.Rows(0).Item("bitAllowPPVariance") = True Then
                    '    chkAllowPPVariance.Checked = True
                    'Else
                    '    chkAllowPPVariance.Checked = False
                    'End If

                    If dtTable.Rows(0).Item("bitSaveCreditCardInfo") = True Then
                        chkKeepCreditCardInfo.Checked = True
                    Else
                        chkKeepCreditCardInfo.Checked = False
                    End If
                    If Not ddlLastViewd.Items.FindByValue(dtTable.Rows(0).Item("intLastViewedRecord")) Is Nothing Then
                        ddlLastViewd.Items.FindByValue(dtTable.Rows(0).Item("intLastViewedRecord")).Selected = True
                    End If

                    If Not rblCommissionType.Items.FindByValue(dtTable.Rows(0).Item("tintCommissionType")) Is Nothing Then
                        rblCommissionType.Items.FindByValue(dtTable.Rows(0).Item("tintCommissionType")).Selected = True
                    End If

                    chkCommisonBasedOn.Checked = CCommon.ToBool(dtTable.Rows(0).Item("bitCommissionBasedOn"))

                    If Not ddlCommissionBasedOn.Items.FindByValue(CCommon.ToShort(dtTable.Rows(0).Item("tintCommissionBasedOn"))) Is Nothing Then
                        ddlCommissionBasedOn.Items.FindByValue(CCommon.ToShort(dtTable.Rows(0).Item("tintCommissionBasedOn"))).Selected = True
                    End If

                    If Not ddlVendorCostType.Items.FindByValue(CCommon.ToShort(dtTable.Rows(0).Item("tintCommissionVendorCostType"))) Is Nothing Then
                        ddlVendorCostType.Items.FindByValue(CCommon.ToShort(dtTable.Rows(0).Item("tintCommissionVendorCostType"))).Selected = True
                    End If

                    If Not rblBaseTax.Items.FindByValue(dtTable.Rows(0).Item("tintBaseTax")) Is Nothing Then
                        rblBaseTax.Items.FindByValue(dtTable.Rows(0).Item("tintBaseTax")).Selected = True
                    End If

                    If Not rblBaseTaxOnArea.Items.FindByValue(dtTable.Rows(0).Item("tintBaseTaxOnArea")) Is Nothing Then
                        rblBaseTaxOnArea.Items.FindByValue(dtTable.Rows(0).Item("tintBaseTaxOnArea")).Selected = True
                    End If

                    If dtTable.Rows(0).Item("bitEmbeddedCost") = True Then
                        chkEmbeddedCost.Checked = True
                    Else
                        chkEmbeddedCost.Checked = False
                    End If
                    If Not ddlSessionTimeout.Items.FindByValue(dtTable.Rows(0).Item("tintSessionTimeOut")) Is Nothing Then
                        ddlSessionTimeout.Items.FindByValue(dtTable.Rows(0).Item("tintSessionTimeOut")).Selected = True
                    End If

                    If Not ddlDecimalPoints.Items.FindByValue(dtTable.Rows(0).Item("tintDecimalPoints")) Is Nothing Then
                        ddlDecimalPoints.Items.FindByValue(dtTable.Rows(0).Item("tintDecimalPoints")).Selected = True
                    End If

                    hplMarketingEmailConfig.Text = "High Volume Outbound <i style='color:#000'>(Relay Server for email broadcasting)</i>: <b style='color:red'>Pending</b>"
                    If (CheckDomainVerificationStatus() = True) Then
                        hplMarketingEmailConfig.Text = "High Volume Outbound <i style='color:#000'>(Relay Server for email broadcasting)</i>: <b style='color:green'>Ready to use</b>"
                    End If
                    If CCommon.ToShort(dtTable.Rows(0)("tintMailProvider")) = 3 Then
                        hplMail.Style.Add("display", "none")
                        hplSMTP.Style.Remove("display")
                        hplSupportIMAP.Style.Remove("display")
                        hplSupportIMAP.Text = "Inbound <i style='color:#000'>(IMAP)</i>: <b style='color:red'>Pending</b>"
                        hplSMTP.Text = "Low Volume Outbound <i style='color:#000'>(SMTP)</i>: <b style='color:red'>Pending</b>"
                        If dtTable.Rows(0).Item("bitImapConfigured") Then
                            hplSupportIMAP.Text = "Inbound <i style='color:#000'>(IMAP)</i>: <b style='color:green'>Ready to use</b>"
                        End If


                        If dtTable.Rows(0).Item("bitPSMTPServer") Then
                            If IIf(IsDBNull(dtTable.Rows(0).Item("vcPSMTPServer")), "", dtTable.Rows(0).Item("vcPSMTPServer")) <> "" Then
                                hplSMTP.Text = "Low Volume Outbound <i style='color:#000'>(SMTP)</i>: <b style='color:green'>Ready to use</b>"
                            End If
                        End If
                    ElseIf CCommon.ToShort(dtTable.Rows(0)("tintMailProvider")) = 2 Then
                        hplSMTP.Style.Add("display", "none")
                        hplSupportIMAP.Style.Add("display", "none")
                        hplMail.Style.Remove("display")
                        hplMail.Attributes.Add("onclick", "return Office365MailAuthenticate()")
                        hplMail.Text = "Allow BizAutomation to access Office365 Mail"
                    Else
                        hplSMTP.Style.Add("display", "none")
                        hplSupportIMAP.Style.Add("display", "none")
                        hplMail.Style.Remove("display")
                        hplMail.Attributes.Add("onclick", "return GoogleMailAuthenticate()")
                        hplMail.Text = "Allow BizAutomation to access Google Mail"
                    End If

                    'If dtTable.Rows(0).Item("bitCustomizePortal") = True Then
                    '    chkEnablePortalCustomization.Checked = True
                    'Else
                    '    chkEnablePortalCustomization.Checked = False
                    'End If
                    If dtTable.Rows(0).Item("tintBillToForPO") > 0 Then
                        If Not ddlBillTOAddress.Items.FindByValue(dtTable.Rows(0).Item("tintBillToForPO")) Is Nothing Then
                            ddlBillTOAddress.Items.FindByValue(dtTable.Rows(0).Item("tintBillToForPO")).Selected = True
                        End If
                    End If

                    If dtTable.Rows(0).Item("tintShipToForPO") > 0 Then
                        If Not ddlShipToAddress.Items.FindByValue(dtTable.Rows(0).Item("tintShipToForPO")) Is Nothing Then
                            ddlShipToAddress.Items.FindByValue(dtTable.Rows(0).Item("tintShipToForPO")).Selected = True
                        End If
                    End If

                    If Not rblOrganizationSearchCriteria.Items.FindByValue(dtTable.Rows(0).Item("tintOrganizationSearchCriteria")) Is Nothing Then
                        rblOrganizationSearchCriteria.Items.FindByValue(dtTable.Rows(0).Item("tintOrganizationSearchCriteria")).Selected = True
                    End If
                    'txtPortalName.Text = dtTable.Rows(0).Item("vcPortalName")
                    hdnOldPortalName.Value = dtTable.Rows(0).Item("vcPortalName")
                    chkEnableDocRep.Checked = dtTable.Rows(0).Item("bitDocumentRepositary")
                    SetPortalLink()



                    chkShowBalance.Checked = dtTable.Rows(0).Item("bitIsShowBalance")

                    chkProjectTracking.Checked = dtTable.Rows(0).Item("IsEnableProjectTracking")
                    chkCampaignTracking.Checked = dtTable.Rows(0).Item("IsEnableCampaignTracking")
                    chkResourceScheduling.Checked = dtTable.Rows(0).Item("IsEnableResourceScheduling")
                    chkEnableDiferredIncome.Checked = CCommon.ToBool(dtTable.Rows(0).Item("IsEnableDeferredIncome"))
                    chkEnableDiferredIncome.Enabled = CCommon.ToBool(dtTable.Rows(0).Item("canChangeDiferredIncomeSelection"))
                    chk3PL.Checked = CCommon.ToBool(dtTable.Rows(0).Item("bit3PL"))
                    chkEDI.Checked = CCommon.ToBool(dtTable.Rows(0).Item("bitEDI"))

                    If Not ddlIncomeAccount.Items.FindByValue(dtTable.Rows(0).Item("numIncomeAccID")) Is Nothing Then
                        ddlIncomeAccount.ClearSelection()
                        ddlIncomeAccount.Items.FindByValue(dtTable.Rows(0).Item("numIncomeAccID")).Selected = True
                    End If
                    If Not ddlCOGS.Items.FindByValue(dtTable.Rows(0).Item("numCOGSAccID")) Is Nothing Then
                        ddlCOGS.ClearSelection()
                        ddlCOGS.Items.FindByValue(dtTable.Rows(0).Item("numCOGSAccID")).Selected = True
                    End If
                    If Not ddlAssetAccount.Items.FindByValue(dtTable.Rows(0).Item("numAssetAccID")) Is Nothing Then
                        ddlAssetAccount.ClearSelection()
                        ddlAssetAccount.Items.FindByValue(dtTable.Rows(0).Item("numAssetAccID")).Selected = True
                    End If



                    If Not ddlCalendarTimeFormat.Items.FindByValue(dtTable.Rows(0).Item("tintCalendarTimeFormat")) Is Nothing Then
                        ddlCalendarTimeFormat.ClearSelection()
                        ddlCalendarTimeFormat.Items.FindByValue(dtTable.Rows(0).Item("tintCalendarTimeFormat")).Selected = True
                    End If

                    If Not rblDefaultClass.Items.FindByValue(dtTable.Rows(0).Item("tintDefaultClassType")) Is Nothing Then
                        rblDefaultClass.ClearSelection()
                        rblDefaultClass.Items.FindByValue(dtTable.Rows(0).Item("tintDefaultClassType")).Selected = True
                    End If

                    If Not rblPriceBookDiscount.Items.FindByValue(dtTable.Rows(0).Item("tintPriceBookDiscount")) Is Nothing Then
                        rblPriceBookDiscount.ClearSelection()
                        rblPriceBookDiscount.Items.FindByValue(dtTable.Rows(0).Item("tintPriceBookDiscount")).Selected = True
                    End If

                    If Not ddlShippingServiceItem.Items.FindByValue(dtTable.Rows(0).Item("numShippingServiceItemID")) Is Nothing Then
                        ddlShippingServiceItem.ClearSelection()
                        ddlShippingServiceItem.Items.FindByValue(dtTable.Rows(0).Item("numShippingServiceItemID")).Selected = True
                    End If

                    If Not ddlDiscountServiceItem.Items.FindByValue(dtTable.Rows(0).Item("numDiscountServiceItemID")) Is Nothing Then
                        ddlDiscountServiceItem.ClearSelection()
                        ddlDiscountServiceItem.Items.FindByValue(dtTable.Rows(0).Item("numDiscountServiceItemID")).Selected = True
                    End If

                    If Not ddlOverheadServiceItem.Items.FindByValue(dtTable.Rows(0).Item("numOverheadServiceItemID")) Is Nothing Then
                        ddlOverheadServiceItem.ClearSelection()
                        ddlOverheadServiceItem.Items.FindByValue(dtTable.Rows(0).Item("numOverheadServiceItemID")).Selected = True
                    End If
                    'Commented by :sachin Sadhu||Date:27thAug14
                    'Reason :Moved to E-Commerce API
                    ' txtShipToPhoneNo.Text = CCommon.ToString(dtTable.Rows(0).Item("vcShipToPhoneNo"))
                    'end of code
                    chkAutoSerialNoAssign.Checked = dtTable.Rows(0).Item("bitAutoSerialNoAssign")
                    chkAllowDuplicateLineItems.Checked = CCommon.ToBool(dtTable.Rows(0).Item("bitAllowDuplicateLineItems"))
                    chkDoNotShowDropshipWindow.Checked = CCommon.ToBool(dtTable.Rows(0).Item("bitDoNotShowDropshipPOWindow"))
                    ddlCommitAllocation.SelectedValue = CCommon.ToShort(dtTable.Rows(0).Item("tintCommitAllocation"))
                    ddlInvoicing.SelectedValue = CCommon.ToShort(dtTable.Rows(0).Item("tintInvoicing"))

                    txtGAUserEmail.Text = dtTable.Rows(0).Item("vcGAUserEMail")
                    txtGAPassword.Text = dtTable.Rows(0).Item("vcGAUserPassword")
                    txtGAProfileID.Text = dtTable.Rows(0).Item("vcGAUserProfileId")

                    txtWidth.Text = CCommon.ToInteger(dtTable.Rows(0).Item("intShippingImageWidth"))
                    txtHeight.Text = CCommon.ToInteger(dtTable.Rows(0).Item("intShippingImageHeight"))
                    txtTotalInsuredValue.Text = CCommon.ToDecimal(dtTable.Rows(0).Item("numTotalInsuredValue"))
                    txtTotalCustomsValue.Text = CCommon.ToDecimal(dtTable.Rows(0).Item("numTotalCustomsValue"))
                    chkUseBizdocAmount.Checked = CCommon.ToBool(dtTable.Rows(0).Item("bitUseBizdocAmount"))
                    chkDefaultRateType.Checked = CCommon.ToBool(dtTable.Rows(0).Item("bitDefaultRateType"))
                    chkIncludeTaxAndShippingInCommission.Checked = CCommon.ToBool(dtTable.Rows(0).Item("bitIncludeTaxAndShippingInCommission"))
                    chkPurchaseTaxCredit.Checked = CCommon.ToBool(dtTable.Rows(0).Item("bitPurchaseTaxCredit"))
                    chkApprovalTimeExp.Checked = CCommon.ToBool(dtTable.Rows(0).Item("bitApprovalforTImeExpense"))
                    hdnPurchaseTaxCredit.Value = ChartOfAccounting.GetDefaultAccount("PT", Session("DomainID"))
                    chkRemoveGlobalWarehouse.Checked = CCommon.ToBool(dtTable.Rows(0).Item("bitRemoveGlobalLocation"))
                    chkLandedCost.Checked = CCommon.ToBool(dtTable.Rows(0).Item("bitLandedCost"))
                    chkUseDeluxeCheckStock.Checked = CCommon.ToBool(dtTable.Rows(0).Item("bitUseDeluxeCheckStock"))
                    If Not rblLandedCost.Items.FindByValue(dtTable.Rows(0).Item("vcLanedCostDefault")) Is Nothing Then
                        rblLandedCost.ClearSelection()
                        rblLandedCost.Items.FindByValue(dtTable.Rows(0).Item("vcLanedCostDefault")).Selected = True
                    End If
                    If dtTable.Rows(0).Item("intTimeExpApprovalProcess") > 0 Then
                        rdnTimeExpProcess.SelectedValue = dtTable.Rows(0).Item("intTimeExpApprovalProcess")
                    End If
                    If dtTable.Rows(0).Item("intOpportunityApprovalProcess") > 0 Then
                        rdnPromotionApproval.SelectedValue = dtTable.Rows(0).Item("intOpportunityApprovalProcess")
                    End If
                    chkMinUnitPrice.Checked = CCommon.ToBool(dtTable.Rows(0).Item("bitMinUnitPriceRule"))
                    If CCommon.ToBool(dtTable.Rows(0).Item("bitMinUnitPriceRule")) = True Then
                        lblPriceMargin.Text = "Enabled"
                    ElseIf CCommon.ToBool(dtTable.Rows(0).Item("bitMinUnitPriceRule")) = False Then
                        lblPriceMargin.Text = "Disabled"
                    End If

                    If CCommon.ToInteger(dtTable.Rows(0).Item("numDefaultSalesPricing")) = 1 Then
                        rdbPriceLevel.Checked = True
                    ElseIf CCommon.ToInteger(dtTable.Rows(0).Item("numDefaultSalesPricing")) = 2 Then
                        rdbPriceRule.Checked = True
                    ElseIf CCommon.ToInteger(dtTable.Rows(0).Item("numDefaultSalesPricing")) = 3 Then
                        rdbLastPrice.Checked = True
                    End If

                    If dtTable.Rows(0).Item("numDefaultSalesOppBizDocId") > 0 Then
                        If Not ddlSalesOppBizDoc.Items.FindByValue(dtTable.Rows(0).Item("numDefaultSalesOppBizDocId")) Is Nothing Then
                            ddlSalesOppBizDoc.ClearSelection()
                            ddlSalesOppBizDoc.Items.FindByValue(dtTable.Rows(0).Item("numDefaultSalesOppBizDocId")).Selected = True
                        End If
                    End If
                    If dtTable.Rows(0).Item("numPODropShipBizDoc") > 0 Then
                        If Not ddlPODropShipBizDoc.Items.FindByValue(dtTable.Rows(0).Item("numPODropShipBizDoc")) Is Nothing Then
                            ddlPODropShipBizDoc.ClearSelection()
                            ddlPODropShipBizDoc.Items.FindByValue(dtTable.Rows(0).Item("numPODropShipBizDoc")).Selected = True
                        End If
                    End If
                    BindBizDocsTemplate()
                    If dtTable.Rows(0).Item("numPODropShipBizDocTemplate") > 0 Then
                        If Not ddlPODropShipBizDocTemplate.Items.FindByValue(dtTable.Rows(0).Item("numPODropShipBizDocTemplate")) Is Nothing Then
                            ddlPODropShipBizDocTemplate.ClearSelection()
                            ddlPODropShipBizDocTemplate.Items.FindByValue(dtTable.Rows(0).Item("numPODropShipBizDocTemplate")).Selected = True
                        End If
                    End If
                    txtPrinterIP.Text = CCommon.ToString(dtTable.Rows(0).Item("vcPrinterIPAddress"))
                    txtPrinterPort.Text = CCommon.ToString(dtTable.Rows(0).Item("vcPrinterPort"))

                    ddlMarkupDiscountOption.SelectedValue = CCommon.ToString(dtTable.Rows(0).Item("tintMarkupDiscountOption"))
                    ddlMarkupDiscountValue.SelectedValue = CCommon.ToString(dtTable.Rows(0).Item("tintMarkupDiscountValue"))

                    If CCommon.ToInteger(dtTable.Rows(0).Item("tintReceivePaymentTo")) = 1 Then
                        radDepositeTo.Checked = True
                        radUnderpositedFund.Checked = False
                    Else
                        radDepositeTo.Checked = False
                        radUnderpositedFund.Checked = True
                    End If

                    If Not ddlDepositTo.Items.FindByValue(CCommon.ToLong(dtTable.Rows(0).Item("numReceivePaymentBankAccount"))) Is Nothing Then
                        ddlDepositTo.Items.FindByValue(CCommon.ToLong(dtTable.Rows(0).Item("numReceivePaymentBankAccount"))).Selected = True
                    End If

                    chkCustomFieldSection.Checked = dtTable.Rows(0).Item("bitDisplayCustomField")
                    chkFollowupAnytime.Checked = dtTable.Rows(0).Item("bitFollowupAnytime")
                    chkTitle.Checked = dtTable.Rows(0).Item("bitpartycalendarTitle")
                    chkLocation.Checked = dtTable.Rows(0).Item("bitpartycalendarLocation")
                    chkDescription.Checked = dtTable.Rows(0).Item("bitpartycalendarDescription")
                    chkPurchaseOrderApproval.Checked = dtTable.Rows(0).Item("bitREQPOApproval")
                    chkARInvoiceDue.Checked = dtTable.Rows(0).Item("bitARInvoiceDue")
                    chkAPBillsDue.Checked = dtTable.Rows(0).Item("bitAPBillsDue")
                    chkItemsTOPickPackShip.Checked = dtTable.Rows(0).Item("bitItemsToPickPackShip")
                    chkItemToInvoice.Checked = dtTable.Rows(0).Item("bitItemsToInvoice")
                    chkSalesOrderToClose.Checked = dtTable.Rows(0).Item("bitSalesOrderToClose")
                    chkItemsToPutAway.Checked = dtTable.Rows(0).Item("bitItemsToPutAway")
                    chkItemsToBill.Checked = dtTable.Rows(0).Item("bitItemsToBill")
                    chkPOSToClose.Checked = dtTable.Rows(0).Item("bitPosToClose")
                    chlPurchaseorderToClose.Checked = dtTable.Rows(0).Item("bitPOToClose")
                    chkWorkOrderBOMS.Checked = dtTable.Rows(0).Item("bitBOMSToPick")
                    SetCheckedItemString(rcbPurchaseOrderApproval, dtTable.Rows(0).Item("vchREQPOApprovalEmp"))
                    SetCheckedItemString(rcbARInvoiceDue, dtTable.Rows(0).Item("vchARInvoiceDue"))
                    SetCheckedItemString(rcbAPBillsDue, dtTable.Rows(0).Item("vchAPBillsDue"))
                    SetCheckedItemString(rcbItemsTOPickPackShip, dtTable.Rows(0).Item("vchItemsToPickPackShip"))
                    SetCheckedItemString(rcbItemToInvoice, dtTable.Rows(0).Item("vchItemsToInvoice"))
                    Dim objCommon As New CCommon()
                    objUserAccess = New UserAccess
                    objUserAccess.DomainID = Session("DomainID")
                    objUserAccess.ListItemId = Nothing
                    Dim dtMarginTable As New DataTable()
                    dtMarginTable = objUserAccess.GetDomainDetailsForPriceMargin()
                    Dim listSelectedUsers As List(Of String)
                    listSelectedUsers = CCommon.ToString(dtMarginTable.Rows(0).Item("vcUnitPriceApprover")).Split(",").ToList()

                    For Each dr As String In listSelectedUsers
                        Dim radComboItem As RadComboBoxItem
                        radComboItem = rcbPriceMarginApproval.FindItemByValue(dr)
                        If radComboItem IsNot Nothing Then
                            radComboItem.Checked = True
                        End If
                    Next
                    SetCheckedItemString(rcbSalesOrderToClose, dtTable.Rows(0).Item("vchSalesOrderToClose"))
                    SetCheckedItemString(rcbItemsToPutAway, dtTable.Rows(0).Item("vchItemsToPutAway"))
                    SetCheckedItemString(rcbItemsToBill, dtTable.Rows(0).Item("vchItemsToBill"))
                    SetCheckedItemString(rcbPOSToClose, dtTable.Rows(0).Item("vchPosToClose"))
                    SetCheckedItemString(rcbPurchaseorderToClose, dtTable.Rows(0).Item("vchPOToClose"))
                    SetCheckedItemString(rcbWorkOrderBOMS, dtTable.Rows(0).Item("vchBOMSToPick"))
                    SetCheckedItemString(rdbEnableUsers, dtTable.Rows(0).Item("vcEmployeeForContractTimeElement"))
                    chkDisplayContract.Checked = dtTable.Rows(0).Item("bitDisplayContractElement")
                    txtDisplayPO.Text = CCommon.ToString(Convert.ToInt16(dtTable.Rows(0).Item("decReqPOMinValue")))

                    LoadTerms()
                    LoadPortalTabs(CCommon.ToString(dtTable.Rows(0).Item("vcHideTabs")))

                    If Not ddlARContactPosition.Items.FindByValue(CCommon.ToLong(dtTable.Rows(0).Item("numARContactPosition"))) Is Nothing Then
                        ddlARContactPosition.Items.FindByValue(CCommon.ToLong(dtTable.Rows(0).Item("numARContactPosition"))).Selected = True
                    End If
                    chkShowCardConnectLink.Checked = CCommon.ToBool(dtTable.Rows(0).Item("bitShowCardConnectLink"))
                    txtBluePayFormName.Text = CCommon.ToString(dtTable.Rows(0).Item("vcBluePayFormName"))
                    txtBluePaySuccessURL.Text = CCommon.ToString(dtTable.Rows(0).Item("vcBluePaySuccessURL"))
                    txtBluePayDeclineURL.Text = CCommon.ToString(dtTable.Rows(0).Item("vcBluePayDeclineURL"))
                    txtSmartystreetsAddrVerificationKeys.Text = CCommon.ToString(dtTable.Rows(0).Item("vcSmartyStreetsAPIKeys"))
                    chkSmartystreetsAddrVerification.Checked = CCommon.ToBool(dtTable.Rows(0).Item("bitEnableSmartyStreets"))
                    chkReceieveOrderWithoutItem.Checked = CCommon.ToBool(dtTable.Rows(0).Item("bitReceiveOrderWithNonMappedItem"))
                    chkUsePredefinedCustomer.Checked = CCommon.ToBool(dtTable.Rows(0).Item("bitUsePredefinedCustomer"))
                    If Not ddlUnMappedItem.Items.FindByValue(CCommon.ToLong(dtTable.Rows(0).Item("numItemToUseForNonMappedItem"))) Is Nothing Then
                        ddlUnMappedItem.Items.FindByValue(CCommon.ToLong(dtTable.Rows(0).Item("numItemToUseForNonMappedItem"))).Selected = True
                    End If
                    chkUsePreviousEmailBizDoc.Checked = CCommon.ToBool(dtTable.Rows(0).Item("bitUsePreviousEmailBizDoc"))
                End If

                Dim str As String
                'txtWebAly.Text = Replace(txtWebAly.Text, "DomainID", strDomainId) 'Removed domain id as encrypted id doesn't load script in IE and DomainID is determined from SubDomain of URL
                txtWebAly.Text = Replace(txtWebAly.Text, "ServerName", hplCusPortal.NavigateUrl.Replace("/Login.aspx", ""))

            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        Private Function CheckDomainVerificationStatus() As Boolean
            Dim isValidCredential As Boolean = False

            Try
                Dim objCampaign As New BACRM.BusinessLogic.Marketing.Campaign()
                objCampaign.DomainID = CCommon.ToLong(Session("DomainID"))
                Dim dt As DataTable = objCampaign.GetBroadcastConfiguration()
                If Not String.IsNullOrEmpty(CCommon.ToString(dt.Rows(0)("vcAWSAccessKey"))) AndAlso
                Not String.IsNullOrEmpty(CCommon.ToString(dt.Rows(0)("vcAWSSecretKey"))) AndAlso
                Not String.IsNullOrEmpty(CCommon.ToString(dt.Rows(0)("vcAWSDomain"))) AndAlso
                Not String.IsNullOrEmpty(CCommon.ToString(dt.Rows(0)("vcFrom"))) Then

                    Dim region As RegionEndpoint = RegionEndpoint.USWest2
                    Dim client As New AmazonSimpleEmailServiceClient(CCommon.ToString(dt.Rows(0)("vcAWSAccessKey")).Trim(), CCommon.ToString(dt.Rows(0)("vcAWSSecretKey")).Trim(), region)

                    'Check Domain Verification Status
                    Dim requestIdentity As New GetIdentityVerificationAttributesRequest()
                    requestIdentity.Identities.Add(CCommon.ToString(dt.Rows(0)("vcAWSDomain")).Trim())

                    Dim responseIdentity As GetIdentityVerificationAttributesResponse = client.GetIdentityVerificationAttributes(requestIdentity)

                    If Not responseIdentity Is Nothing AndAlso responseIdentity.VerificationAttributes.Count > 0 AndAlso responseIdentity.VerificationAttributes.ContainsKey(CCommon.ToString(dt.Rows(0)("vcAWSDomain")).Trim()) Then
                        Dim identityAttriubute As IdentityVerificationAttributes = responseIdentity.VerificationAttributes(CCommon.ToString(dt.Rows(0)("vcAWSDomain")).Trim())

                        If identityAttriubute.VerificationStatus = "Success" Then
                            isValidCredential = True
                            Return isValidCredential
                        Else
                            isValidCredential = False
                            Return isValidCredential
                        End If

                        Dim requestDkimIdentity As GetIdentityDkimAttributesRequest = New GetIdentityDkimAttributesRequest()
                        requestDkimIdentity.Identities.Add(CCommon.ToString(dt.Rows(0)("vcAWSDomain")))

                        Dim responseDkimIdentity As GetIdentityDkimAttributesResponse = client.GetIdentityDkimAttributes(requestDkimIdentity)
                        If responseDkimIdentity Is Nothing Or responseDkimIdentity.DkimAttributes.Count = 0 Or Not responseDkimIdentity.DkimAttributes.ContainsKey(CCommon.ToString(dt.Rows(0)("vcAWSDomain")).Trim()) Or responseDkimIdentity.DkimAttributes(CCommon.ToString(dt.Rows(0)("vcAWSDomain")).Trim()).DkimVerificationStatus <> "Success" Then
                            isValidCredential = False
                            Return isValidCredential
                        Else
                            isValidCredential = True
                            Return isValidCredential
                        End If
                    Else
                        isValidCredential = False
                        Return isValidCredential
                    End If

                End If
            Catch ex As Exception
                isValidCredential = False
            End Try

            Return isValidCredential
        End Function
        Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
            Try
                litMessage.Text = ""
                Dim objUserAccess As New UserAccess
                objUserAccess.CommissionType = rblCommissionType.SelectedValue
                objUserAccess.IsCommissionBasedOn = chkCommisonBasedOn.Checked
                objUserAccess.CommissionBasedOn = CCommon.ToShort(ddlCommissionBasedOn.SelectedValue)
                objUserAccess.CommissionVendorCostType = CCommon.ToShort(ddlVendorCostType.SelectedValue)

                objUserAccess.DomainID = Session("DomainID")
                If chkPurchaseTaxCredit.Checked AndAlso ChartOfAccounting.GetDefaultAccount("PT", Session("DomainID")) = 0 Then
                    ShowMessage("You must first go to Domain Details | Accounting | General | Default Accounts | Purchase Tax Credit, and select a proper account.")
                    Exit Sub
                End If
                'End If
                'End If

                If ddlShippingServiceItem.SelectedValue > 0 And ddlDiscountServiceItem.SelectedValue > 0 And ddlShippingServiceItem.SelectedValue = ddlDiscountServiceItem.SelectedValue Then
                    ShowMessage("Shipping Service Item and Discount Service Item must be different.")
                    Exit Sub
                End If

                SetPortalLink()

                objUserAccess.DomainName = txtDomain.Text
                objUserAccess.DomainDesc = txtDesc.Text
                objUserAccess.CustomPagingRows = ddlPaginRows.SelectedValue
                objUserAccess.ComposeWindow = ddlComposeWindow.SelectedValue
                objUserAccess.DateFormat = ddlDateFormat.SelectedValue
                objUserAccess.DefCountry = ddlCountry.SelectedValue
                objUserAccess.StartDate = IIf(txtStartDate.Text = "", 0, txtStartDate.Text)
                objUserAccess.IntervalDays = IIf(txtInterval.Text = "", 0, txtInterval.Text)
                objUserAccess.PopulateUserCriteria = ddlPopulateUserCrt.SelectedValue
                objUserAccess.bitIntMedPage = True 'bug ID: 2060 #4  chkInteMedPage.Checked
                objUserAccess.FiscalStartMonth = ddlFiscalStartMonth.SelectedValue
                objUserAccess.PayPeriod = ddlPayPeriod.SelectedItem.Value
                objUserAccess.CurrencyID = ddlCurrency.SelectedValue
                'objUserAccess.boolMultiCompany = chkMultiCompany.Checked
                objUserAccess.boolMultiCurrency = chkMultiCurrency.Checked
                objUserAccess.DefaultSite = CCommon.ToLong(ddlSites.SelectedValue)

                objUserAccess.vcSalesOrderTabs = txtSalesOrderTabs.Text
                objUserAccess.vcSalesQuotesTabs = txtSalesQuotesTabs.Text
                objUserAccess.vcItemPurchaseHistoryTabs = txtItemPurchaseHistoryTabs.Text
                objUserAccess.vcItemsFrequentlyPurchasedTabs = txtItemsFrequentlyPurchasedTabs.Text
                objUserAccess.vcOpenCasesTabs = txtOpenCasesTabs.Text
                objUserAccess.vcOpenRMATabs = txtOpenRMATabs.Text
                objUserAccess.vcSupportTabs = txtSupportTabs.Text

                objUserAccess.bitSalesOrderTabs = chkSalesOrderTabs.Checked
                objUserAccess.bitSalesQuotesTabs = chkSalesQuotesTabs.Checked
                objUserAccess.bitItemPurchaseHistoryTabs = chkItemPurchaseHistoryTabs.Checked
                objUserAccess.bitItemsFrequentlyPurchasedTabs = chkItemsFrequentlyPurchasedTabs.Checked
                objUserAccess.bitOpenCasesTabs = chkOpenCasesTabs.Checked
                objUserAccess.bitOpenRMATabs = chkOpenRMATabs.Checked
                objUserAccess.bitSupportTabs = chkSupportTabs.Checked

                objUserAccess.numDefaultSalesShippingDoc = ddlDefaultSalesBizDoc.SelectedValue
                objUserAccess.numDefaultPurchaseShippingDoc = ddlDefaultPurchaseBizDoc.SelectedValue

                objUserAccess.StrDivision = hdnDivisionValue.Text.Trim
                'If ChkDeferredIncome.Checked = True Then
                '    objUserAccess.DeferredIncome = True
                'Else : objUserAccess.DeferredIncome = False
                'End If


                If chkPrimaryAddress.Checked Then
                    objUserAccess.boolAutoPopulateAddress = True
                    objUserAccess.PoulateAddressTo = ddlAddress.SelectedValue
                End If


                objUserAccess.AutolinkUnappliedPayment = chkAutolinkUnappliedPayment.Checked
                objUserAccess.DiscountOnUnitPrice = chkDiscountOnUnitPrice.Checked
                objUserAccess.KeepCreditCardInfo = chkKeepCreditCardInfo.Checked
                objUserAccess.chrUnitSystem = IIf(String.IsNullOrEmpty(hdnUnitSystem.Value), "E", hdnUnitSystem.Value)

                If FUPortalLogo.FileName <> "" Then
                    objUserAccess.PortalLogo = UploadImage()
                Else : objUserAccess.PortalLogo = hfPortalLogo.Value
                End If
                objUserAccess.ChrForComSearch = ddlChr.SelectedValue
                objUserAccess.ChrItemSearch = ddlChrItemSearch.SelectedValue
                objUserAccess.ShipCompany = radShipCompany.SelectedValue ' ddlShipCompany.SelectedValue
                objUserAccess.LastViewedCount = ddlLastViewd.SelectedValue
                objUserAccess.BaseTax = rblBaseTax.SelectedValue

                objUserAccess.BaseTaxOnArea = rblBaseTaxOnArea.SelectedValue

                objUserAccess.EnableEmbeddedCost = chkEmbeddedCost.Checked
                objUserAccess.SessionTimeout = ddlSessionTimeout.SelectedValue
                'objUserAccess.boolCustomizePortal = chkEnablePortalCustomization.Checked
                objUserAccess.BillToDefault = ddlBillTOAddress.SelectedValue
                objUserAccess.ShipToDefault = ddlShipToAddress.SelectedValue
                objUserAccess.OrganizationSearchCriteria = rblOrganizationSearchCriteria.SelectedValue
                objUserAccess.DecimalPoints = ddlDecimalPoints.SelectedValue
                objUserAccess.EnableDocumentRepositary = chkEnableDocRep.Checked
                objUserAccess.boolGtoBContact = chkGtoBContact.Checked
                objUserAccess.boolBtoGContact = chkBtoGContact.Checked
                If chkEnableCalender.Checked = True Then
                    chkGtoBCalendar.Checked = True
                    chkBtoGCalendar.Checked = True
                Else
                    chkGtoBCalendar.Checked = False
                    chkBtoGCalendar.Checked = False
                End If
                objUserAccess.boolGtoBCalendar = chkGtoBCalendar.Checked
                objUserAccess.boolBtoGCalendar = chkBtoGCalendar.Checked
                objUserAccess.EnableNonInventoryItemExpense = chkNonIExpenseAccount.Checked
                objUserAccess.InlineEdit = True 'chkInlineEdit.Checked
                objUserAccess.RemoveVendorPOValidation = Not chkRemoveVendorPOValidation.Checked
                objUserAccess.SearchOrderCustomerHistory = chkSearchOrderCustomerHistory.Checked

                objUserAccess.boolAmountPastDue = chkAmountPastDue.Checked
                If txtAmountPastDue.Text = "" OrElse txtAmountPastDue.Text = "0" Then
                    txtAmountPastDue.Text = "1"
                End If
                objUserAccess.AmountPastDue = CCommon.ToDecimal(txtAmountPastDue.Text.Trim)


                objUserAccess.CalendarTimeFormat = ddlCalendarTimeFormat.SelectedValue
                objUserAccess.DefaultClass = rblDefaultClass.SelectedValue
                objUserAccess.PriceBookDiscount = rblPriceBookDiscount.SelectedValue
                objUserAccess.AutoSerialNoAssign = chkAutoSerialNoAssign.Checked
                objUserAccess.IsAllowDuplicateLineItems = chkAllowDuplicateLineItems.Checked
                objUserAccess.IsDoNotShowDropshipWindow = chkDoNotShowDropshipWindow.Checked
                objUserAccess.CommitAllocation = CCommon.ToShort(ddlCommitAllocation.SelectedValue)
                objUserAccess.InventoryInvoicing = CCommon.ToShort(ddlInvoicing.SelectedValue)
                objUserAccess.IsInventoryInvoicing = chkInvoicing.Checked

                'MODIFIED BY MANISH ANJARA ON : 27th Sept,2012
                objUserAccess.IsShowBalance = chkShowBalance.Checked

                'MODIFIED BY MANISH ANJARA ON : 18th Oct,2012
                objUserAccess.AssetAccountID = CCommon.ToLong(ddlAssetAccount.SelectedValue)
                objUserAccess.IncomeAccountID = CCommon.ToLong(ddlIncomeAccount.SelectedValue)
                objUserAccess.COGSAccountID = CCommon.ToLong(ddlCOGS.SelectedValue)

                objUserAccess.IsEnableProjectTracking = chkProjectTracking.Checked
                objUserAccess.IsEnableCampaignTracking = chkCampaignTracking.Checked
                objUserAccess.IsEnableDeferredIncome = chkEnableDiferredIncome.Checked
                objUserAccess.ShippingServiceItem = CCommon.ToLong(ddlShippingServiceItem.SelectedValue)
                objUserAccess.DiscountServiceItem = CCommon.ToLong(ddlDiscountServiceItem.SelectedValue)
                objUserAccess.OverheadServiceItem = CCommon.ToLong(ddlOverheadServiceItem.SelectedValue)
                'MODIFIED BY MANISH ANJARA ON : 28th Dec,2012
                objUserAccess.IsEnableResourceScheduling = chkResourceScheduling.Checked
                'Commented by :sachin Sadhu||Date:27thAug14
                'Reason :Moved to E-Commerce API
                '  objUserAccess.ShipToPhoneNumber = txtShipToPhoneNo.Text.Trim()
                'end of code
                objUserAccess.GAUserProfileId = txtGAProfileID.Text.Trim()
                objUserAccess.GAUserEmailId = txtGAUserEmail.Text.Trim()
                objUserAccess.GAUserPassword = txtGAPassword.Text.Trim()
                objUserAccess.ImageWidth = CCommon.ToInteger(txtWidth.Text)
                objUserAccess.ImageHeight = CCommon.ToInteger(txtHeight.Text)
                objUserAccess.TotalInsuredValue = CCommon.ToDecimal(txtTotalInsuredValue.Text)
                objUserAccess.TotalCustomsValue = CCommon.ToDecimal(txtTotalCustomsValue.Text)
                objUserAccess.UseBizdocAmount = CCommon.ToBool(chkUseBizdocAmount.Checked)
                objUserAccess.IsDefaultRateType = CCommon.ToBool(chkDefaultRateType.Checked)
                objUserAccess.IsIncludeTaxAndShippingInCommission = CCommon.ToBool(chkIncludeTaxAndShippingInCommission.Checked)
                objUserAccess.IsPurchaseTaxCredit = CCommon.ToBool(chkPurchaseTaxCredit.Checked)
                objUserAccess.IsLandedCost = CCommon.ToBool(chkLandedCost.Checked)
                objUserAccess.IsUseDeluxeCheckStock = chkUseDeluxeCheckStock.Checked
                objUserAccess.LanedCostDefault = rblLandedCost.SelectedValue

                Dim strSelectedItems As String = ""
                For Each lstItem As ListItem In chkLstHideTabs.Items
                    If lstItem.Selected = True Then
                        strSelectedItems = strSelectedItems & "," & lstItem.Value
                    End If
                Next
                strSelectedItems = strSelectedItems.Trim(",").Trim()
                objUserAccess.HideTabs = strSelectedItems

                If rdbPriceLevel.Checked Then
                    objUserAccess.DefaultSalesPricingMethod = 1
                ElseIf rdbPriceRule.Checked Then
                    objUserAccess.DefaultSalesPricingMethod = 2
                ElseIf rdbLastPrice.Checked Then
                    objUserAccess.DefaultSalesPricingMethod = 3
                Else
                    objUserAccess.DefaultSalesPricingMethod = 1
                End If

                objUserAccess.IsMinUnitPriceRule = chkMinUnitPrice.Checked
                objUserAccess.DefaultSalesOppBizDocID = CCommon.ToLong(ddlSalesOppBizDoc.SelectedValue)
                objUserAccess.PODropShipBizDoc = CCommon.ToLong(ddlPODropShipBizDoc.SelectedValue)
                objUserAccess.PODropShipBizDocTemplate = CCommon.ToLong(ddlPODropShipBizDocTemplate.SelectedValue)
                objUserAccess.bitApprovalforTImeExpense = chkApprovalTimeExp.Checked
                objUserAccess.intTimeExpApprovalProcess = rdnTimeExpProcess.SelectedValue
                objUserAccess.intOpportunityApprovalProcess = rdnPromotionApproval.SelectedValue
                objUserAccess.IsRemoveGlobalWarehouse = chkRemoveGlobalWarehouse.Checked
                Dim Cost As Integer
                If rdbAvgCost.Checked Then
                    objUserAccess.numCost = 1
                    Cost = 1
                ElseIf rdbPVCost.Checked Then
                    objUserAccess.numCost = 3
                    Cost = 3
                End If
                'If chkOverRideAssigntoList.Checked Then
                '    objUserAccess.bitchkOverRideAssignto = 1
                'Else
                objUserAccess.bitchkOverRideAssignto = 0
                'End If
                objUserAccess.PrinterIPAddress = txtPrinterIP.Text
                objUserAccess.PrinterPort = txtPrinterPort.Text
                objUserAccess.numAuthorizePercentage = ddlAuthorizePercentage.SelectedValue
                objUserAccess.bitEDI = chkEDI.Checked
                objUserAccess.bit3PL = chk3PL.Checked
                objUserAccess.tintMarkupDiscountOption = ddlMarkupDiscountOption.SelectedValue
                objUserAccess.tintMarkupDiscountValue = ddlMarkupDiscountValue.SelectedValue

                If radDepositeTo.Checked Then
                    objUserAccess.tintReceivePaymentTo = 1
                    objUserAccess.numReceivePaymentBankAccount = CCommon.ToLong(ddlDepositTo.SelectedValue)
                Else
                    objUserAccess.tintReceivePaymentTo = 2
                    objUserAccess.numReceivePaymentBankAccount = 0
                End If

                objUserAccess.bitDisplayCustomField = chkCustomFieldSection.Checked
                objUserAccess.bitFollowupAnytime = chkFollowupAnytime.Checked
                objUserAccess.bitpartycalendarTitle = chkTitle.Checked
                objUserAccess.bitpartycalendarLocation = chkLocation.Checked
                objUserAccess.bitpartycalendarDescription = chkDescription.Checked
                objUserAccess.bitREQPOApproval = chkPurchaseOrderApproval.Checked
                objUserAccess.bitARInvoiceDue = chkARInvoiceDue.Checked
                objUserAccess.bitAPBillsDue = chkAPBillsDue.Checked
                objUserAccess.bitItemsToPickPackShip = chkItemsTOPickPackShip.Checked
                objUserAccess.bitItemsToInvoice = chkItemToInvoice.Checked
                objUserAccess.bitSalesOrderToClose = chkSalesOrderToClose.Checked
                objUserAccess.bitItemsToPutAway = chkItemsToPutAway.Checked
                objUserAccess.bitItemsToBill = chkItemsToBill.Checked
                objUserAccess.bitPosToClose = chkPOSToClose.Checked
                objUserAccess.bitPOToClose = chlPurchaseorderToClose.Checked
                objUserAccess.bitBOMSToPick = chkWorkOrderBOMS.Checked
                objUserAccess.vchREQPOApprovalEmp = GenerateCheckedItemString(rcbPurchaseOrderApproval)
                objUserAccess.vchARInvoiceDue = GenerateCheckedItemString(rcbARInvoiceDue)
                objUserAccess.vchAPBillsDue = GenerateCheckedItemString(rcbAPBillsDue)
                objUserAccess.vchItemsToPickPackShip = GenerateCheckedItemString(rcbItemsTOPickPackShip)
                objUserAccess.vchItemsToInvoice = GenerateCheckedItemString(rcbItemToInvoice)
                objUserAccess.vchPriceMarginApproval = GenerateCheckedItemString(rcbPriceMarginApproval)
                objUserAccess.vchSalesOrderToClose = GenerateCheckedItemString(rcbSalesOrderToClose)
                objUserAccess.vchItemsToPutAway = GenerateCheckedItemString(rcbItemsToPutAway)
                objUserAccess.vchItemsToBill = GenerateCheckedItemString(rcbItemsToBill)
                objUserAccess.vchPosToClose = GenerateCheckedItemString(rcbPOSToClose)
                objUserAccess.vchPOToClose = GenerateCheckedItemString(rcbPurchaseorderToClose)
                objUserAccess.vchBOMSToPick = GenerateCheckedItemString(rcbWorkOrderBOMS)
                objUserAccess.decReqPOMinValue = Convert.ToDecimal(txtDisplayPO.Text)
                objUserAccess.bitUseOnlyActionItems = chkActionItems.Checked
                objUserAccess.bitDisplayContractElement = chkDisplayContract.Checked
                objUserAccess.vcEmployeeForContractTimeElement = GenerateCheckedItemString(rdbEnableUsers)
                objUserAccess.ARContactPosition = CCommon.ToLong(ddlARContactPosition.SelectedValue)
                objUserAccess.IsShowCardConnectLink = chkShowCardConnectLink.Checked
                objUserAccess.BluePayFormName = txtBluePayFormName.Text
                objUserAccess.BluePaySuccessURL = txtBluePaySuccessURL.Text
                objUserAccess.BluePayDeclineURL = txtBluePayDeclineURL.Text
                objUserAccess.IsEnableSmartyStreets = chkSmartystreetsAddrVerification.Checked
                objUserAccess.SmartyStreetsAPIKeys = txtSmartystreetsAddrVerificationKeys.Text
                objUserAccess.IsUsePreviousEmailBizDoc = chkUsePreviousEmailBizDoc.Checked
                objUserAccess.UpdateDomainDetails()

                Session("bitInventoryInvoicing") = chkInvoicing.Checked
                Session("bitUseOnlyActionItems") = chkActionItems.Checked
                Session("numAuthorizePercentage") = ddlAuthorizePercentage.SelectedValue
                Session("bitchkOverRideAssignto") = False
                Session("numDefaultPurchaseShippingDoc") = ddlDefaultPurchaseBizDoc.SelectedValue
                Session("numDefaultSalesShippingDoc") = ddlDefaultSalesBizDoc.SelectedValue
                Session("bitApprovalforTImeExpense") = chkApprovalTimeExp.Checked
                Session("intTimeExpApprovalProcess") = rdnTimeExpProcess.SelectedValue
                Session("intOpportunityApprovalProcess") = rdnPromotionApproval.SelectedValue
                Session("numCost") = Cost
                Session("DefaultSalesOppBizDocID") = CCommon.ToLong(ddlSalesOppBizDoc.SelectedValue)
                Session("PODropShipBizDoc") = CCommon.ToLong(ddlPODropShipBizDoc.SelectedValue)
                Session("PODropShipBizDocTemplate") = CCommon.ToLong(ddlPODropShipBizDocTemplate.SelectedValue)
                Session("ApprovalforTImeExpense") = chkApprovalTimeExp.Checked
                Session("DateFormat") = ddlDateFormat.SelectedValue
                Session("PagingRows") = ddlPaginRows.SelectedValue
                Session("DefCountry") = ddlCountry.SelectedValue
                Session("CompWindow") = ddlComposeWindow.SelectedValue
                Session("StartDate") = DateAdd(DateInterval.Day, -objUserAccess.StartDate, Now)
                Session("EndDate") = DateAdd(DateInterval.Day, objUserAccess.IntervalDays, DateAdd(DateInterval.Day, -objUserAccess.StartDate, Now))
                Session("PopulateUserCriteria") = ddlPopulateUserCrt.SelectedValue
                Session("EnableIntMedPage") = 1 'bug id 2060 #4  'IIf(chkInteMedPage.Checked = True, 1, 0) ' Status of Intermediatory page
                Session("FiscalMonth") = ddlFiscalStartMonth.SelectedValue
                Session("ChrForItemSearch") = ddlChrItemSearch.SelectedValue
                Session("ChrForCompSearch") = ddlChr.SelectedValue
                Session("UnitSystem") = IIf(String.IsNullOrEmpty(hdnUnitSystem.Value), "E", hdnUnitSystem.Value)
                Session("SOBizDocStatus") = objUserAccess.SOBizDocStatus
                Session("AutolinkUnappliedPayment") = objUserAccess.AutolinkUnappliedPayment
                Session("DiscountOnUnitPrice") = objUserAccess.DiscountOnUnitPrice
                'Session("AllowPPVariance") = objUserAccess.AllowPPVariance

                Session("BaseTaxCalcOn") = rblBaseTax.SelectedValue

                Session("BaseTaxOnArea") = rblBaseTaxOnArea.SelectedValue

                Session("DefaultShippingCompany") = radShipCompany.SelectedValue 'ddlShipCompany.SelectedValue
                Session("EnableEmbeddedCost") = chkEmbeddedCost.Checked
                Session("DecimalPoints") = ddlDecimalPoints.SelectedValue
                Session("DefaultBillToForPO") = ddlBillTOAddress.SelectedValue
                Session("DefaultShipToForPO") = ddlShipToAddress.SelectedValue
                Session("OrganizationSearchCriteria") = rblOrganizationSearchCriteria.SelectedValue
                Session("ChrForCompSearch") = ddlChr.SelectedValue
                Session("DocumentRepositaryEnabled") = IIf(chkEnableDocRep.Checked, 1, 0)

                Session("GtoBContact") = chkGtoBContact.Checked
                Session("BtoGContact") = chkBtoGContact.Checked
                Session("GtoBCalendar") = chkGtoBCalendar.Checked
                Session("BtoGCalendar") = chkBtoGCalendar.Checked
                Session("EnableNonInventoryItemExpense") = chkNonIExpenseAccount.Checked

                Session("InlineEdit") = True ' chkInlineEdit.Checked
                Session("RemoveVendorPOValidation") = Not chkRemoveVendorPOValidation.Checked
                Session("SearchOrderCustomerHistory") = chkSearchOrderCustomerHistory.Checked

                Session("bitAmountPastDue") = chkAmountPastDue.Checked
                Session("AmountPastDue") = txtAmountPastDue.Text.Trim
                Session("AutoSerialNoAssign") = chkAutoSerialNoAssign.Checked
                Session("IsAllowDuplicateLineItems") = chkAllowDuplicateLineItems.Checked


                Session("CalendarTimeFormat") = ddlCalendarTimeFormat.SelectedValue
                Session("DefaultClassType") = rblDefaultClass.SelectedValue
                Session("PriceBookDiscount") = rblPriceBookDiscount.SelectedValue
                Session("IsEnableResourceScheduling") = chkResourceScheduling.Checked

                Session("IsEnableProjectTracking") = chkProjectTracking.Checked
                Session("IsEnableDeferredIncome") = chkEnableDiferredIncome.Checked
                Session("IsEnableCampaignTracking") = chkCampaignTracking.Checked
                Session("ShippingServiceItem") = CCommon.ToLong(ddlShippingServiceItem.SelectedValue)
                Session("DiscountServiceItem") = CCommon.ToLong(ddlDiscountServiceItem.SelectedValue)
                Session("CommissionType") = rblCommissionType.SelectedValue
                Session("ShippingImageWidth") = CCommon.ToInteger(txtWidth.Text)
                Session("ShippingImageHeight") = CCommon.ToInteger(txtHeight.Text)
                Session("TotalInsuredValue") = CCommon.ToDecimal(txtTotalInsuredValue.Text)
                Session("TotalCustomsValue") = CCommon.ToDecimal(txtTotalCustomsValue.Text)
                Session("UseBizdocAmount") = CCommon.ToBool(chkUseBizdocAmount.Checked)
                Session("IsDefaultRateType") = CCommon.ToBool(chkDefaultRateType.Checked)
                Session("IsIncludeTaxAndShippingInCommission") = CCommon.ToBool(chkIncludeTaxAndShippingInCommission.Checked)
                Session("BaseCurrencyID") = ddlCurrency.SelectedValue
                Session("MultiCurrency") = chkMultiCurrency.Checked
                Session("IsLandedCost") = CCommon.ToBool(chkLandedCost.Checked)
                Session("LanedCostDefault") = rblLandedCost.SelectedValue
                Session("IsMinUnitPriceRule") = chkMinUnitPrice.Checked

                Session("bitDisplayCustomField") = chkCustomFieldSection.Checked
                Session("bitFollowupAnytime") = chkFollowupAnytime.Checked
                Session("bitpartycalendarTitle") = chkTitle.Checked
                Session("bitpartycalendarLocation") = chkLocation.Checked
                Session("bitpartycalendarDescription") = chkDescription.Checked
                Session("bitREQPOApproval") = chkPurchaseOrderApproval.Checked
                Session("bitARInvoiceDue") = chkARInvoiceDue.Checked
                Session("bitAPBillsDue") = chkAPBillsDue.Checked
                Session("bitItemsToPickPackShip") = chkItemsTOPickPackShip.Checked
                Session("bitItemsToInvoice") = chkItemToInvoice.Checked
                Session("bitSalesOrderToClose") = chkSalesOrderToClose.Checked
                Session("bitItemsToPutAway") = chkItemsToPutAway.Checked
                Session("bitItemsToBill") = chkItemsToBill.Checked
                Session("bitPosToClose") = chkPOSToClose.Checked
                Session("bitPOToClose") = chlPurchaseorderToClose.Checked
                Session("bitBOMSToPick") = chkWorkOrderBOMS.Checked
                Session("vchREQPOApprovalEmp") = GenerateCheckedItemString(rcbPurchaseOrderApproval)
                Session("vchARInvoiceDue") = GenerateCheckedItemString(rcbARInvoiceDue)
                Session("vchAPBillsDue") = GenerateCheckedItemString(rcbAPBillsDue)
                Session("vchItemsToPickPackShip") = GenerateCheckedItemString(rcbItemsTOPickPackShip)
                Session("vchItemsToInvoice") = GenerateCheckedItemString(rcbItemToInvoice)
                Session("vchPriceMarginApproval") = GenerateCheckedItemString(rcbPriceMarginApproval)
                Session("vchSalesOrderToClose") = GenerateCheckedItemString(rcbSalesOrderToClose)
                Session("vchItemsToPutAway") = GenerateCheckedItemString(rcbItemsToPutAway)
                Session("vchItemsToBill") = GenerateCheckedItemString(rcbItemsToBill)
                Session("vchPosToClose") = GenerateCheckedItemString(rcbPOSToClose)
                Session("vchPOToClose") = GenerateCheckedItemString(rcbPurchaseorderToClose)
                Session("vchBOMSToPick") = GenerateCheckedItemString(rcbWorkOrderBOMS)
                Session("decReqPOMinValue") = Convert.ToDecimal(txtDisplayPO.Text)
                Session("bitDisplayContractElement") = chkDisplayContract.Checked
                Session("vcEmployeeForContractTimeElement") = GenerateCheckedItemString(rdbEnableUsers)

                Session("vcSmartyStreetsAPIKeys") = Convert.ToString(txtSmartystreetsAddrVerificationKeys.Text)
                Session("bitEnableSmartyStreets") = chkSmartystreetsAddrVerification.Checked
                'Added By:Sachin Sadhu||Date:27thAug2014
                'Purpose : To Save PayBill WFA data in Account tab
                SavePayBillWFAData()
                'end of code
                LoadDomainDetails()
            Catch ex As Exception
                If ex.Message.Contains("INCOMPLETE_MIN_UNIT_PRICE_CONFIGURATION") Then
                    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "MinUnitPriceConfig", "alert('Before you can select Minimum Price Margin rule, you first have to: 1. Setup a global approval process, and .2 Setup approvers.\n\nTo set the global approval settings, go to Administration | Global Settings | Order Mgt. Select Enable Flat or Hierarchical Approval Process.\n\nTo configure approvers, go to Administration | Workflow Automation | Approval Process, select the Purchase/Sales Order Approval module, and set all approvers. Only approvers set up from here, will be allowed to be selected within the Price Margin Rules.');", True)
                ElseIf ex.Message.Contains("INCOMPLETE_TIMEEXPENSE_APPROVAL") Then
                    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "TimeExpenseApproval", "alert('Before you can enable Approval Process for Time & Expense, you first have to Setup approvers: \n\nGo to Administration | Workflow Automation | Approval Process, select the Time and Expense module, and set atleast one approver for all users.');", True)
                ElseIf ex.Message.Contains("NOT_ALLOWED_CHANGE_COMMIT_ALLOCATION_OPEN_SALES_ORDER") Then
                    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "OpenSalesOrder", "alert('Before you can change Commit allocation to Sales Orders, you have to close all open sales order(s)');", True)
                Else
                    ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                    DisplayError(ex.Message)
                End If
            End Try
        End Sub

        Private Sub DisplayError(ByVal ex As String)
            DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ex
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
        End Sub

        Function UploadImage() As String
            Try
                Dim strFileName As String
                If Directory.Exists(CCommon.GetDocumentPhysicalPath(Session("DomainID"))) = False Then ' If Folder Does not exists create New Folder.
                    Directory.CreateDirectory(CCommon.GetDocumentPhysicalPath(Session("DomainID")))
                End If
                Dim FilePath As String = CCommon.GetDocumentPhysicalPath(Session("DomainID"))
                Dim ArrFilePath, ArrFileExt
                Dim strFileExt As String

                If FUPortalLogo.PostedFile.FileName <> "" Then
                    ArrFileExt = Split(FUPortalLogo.PostedFile.FileName, ".")
                    strFileExt = ArrFileExt(UBound(ArrFileExt))
                    strFileName = "File" & Day(DateTime.Now) & Month(DateTime.Now) & Year(DateTime.Now) & Hour(DateTime.Now) & Minute(DateTime.Now) & Second(DateTime.Now) & ".gif"

                    FilePath = FilePath & strFileName

                    If Not FUPortalLogo.PostedFile Is Nothing Then FUPortalLogo.PostedFile.SaveAs(FilePath)
                    hfPortalLogo.Value = strFileName
                Else : strFileName = hfPortalLogo.Value
                End If
                Return strFileName
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Private Sub SetPortalLink()
            Try
                'txtPortalName.Text = txtPortalName.Text.Trim.ToLower.Replace("http://", "").Replace("https://", "").Replace("www.", "")
                hplCusPortal.NavigateUrl = "http://" + "portal.bizautomation.com" 'ConfigurationManager.AppSettings("PortalURL").ToLower.Replace("http://portal", "") '& "?id_site=" & strDomainId
                hplCusPortal.Text = hplCusPortal.NavigateUrl
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        'Private Sub btnCreatePortal_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCreatePortal.Click
        'Try
        'Update Host header for website
        '    Try
        '        'create Regular Expression Match pattern object
        '        Dim myRegex As New Regex("^[a-zA-Z0-9]{4,20}$") 'minimum length 4
        '        'boolean variable to hold the status
        '        Dim isValid As Boolean = False
        '        isValid = myRegex.IsMatch(txtPortalName.Text.Trim())
        '        'return the results
        '        If isValid = False Then
        '            ClientScript.RegisterClientScriptBlock(Me.GetType, "error", "alert('invalid Portal name entered! (only alphanumeric characters are allowed with minimum length 4 characters to max 20)');", True)
        '            Exit Sub
        '        End If

        '        If BACRM.BusinessLogic.ShioppingCart.Sites.IsHostNameInUse(txtPortalName.Text.Trim(), 0, Session("DomainID")) Then
        '            ClientScript.RegisterClientScriptBlock(Me.GetType, "error", "alert('Portal Name Already in use, your option is to change portal name and try again.');", True)
        '            Exit Sub
        '        End If

        '        Dim serverManager As New Microsoft.Web.Administration.ServerManager
        '        Dim newBinding As String = (txtPortalName.Text + ".bizautomation.com").ToLower 'ConfigurationManager.AppSettings("PortalURL").ToLower.Replace("http://portal", "").Replace("/login.aspx", "").Replace("/bacrmportal", "").Replace("/bizportal", "").TrimEnd("/")
        '        Dim oldBinding As String = (hdnOldPortalName.Value + ".bizautomation.com").ToLower 'ConfigurationManager.AppSettings("PortalURL").ToLower.Replace("http://portal", "").Replace("/login.aspx", "").Replace("/bacrmportal", "").Replace("/bizportal", "").TrimEnd("/")
        '        Dim bindingToDelete As Microsoft.Web.Administration.Binding = Nothing
        '        If Not serverManager.Sites("portal") Is Nothing Then
        '            For Each binding As Microsoft.Web.Administration.Binding In serverManager.Sites("portal").Bindings
        '                If newBinding <> oldBinding Then
        '                    If binding.BindingInformation.ToLower.IndexOf(newBinding) >= 0 Then
        '                        'Duplicate
        '                        ClientScript.RegisterClientScriptBlock(Me.GetType, "error", "alert('Portal Name Already in use, your option is to change portal name and try again.');", True)
        '                        Exit Sub
        '                    End If
        '                    If binding.BindingInformation.ToLower.IndexOf(oldBinding) >= 0 Then
        '                        'Delete Old Binding
        '                        bindingToDelete = binding
        '                    End If
        '                End If
        '            Next
        '            'Delete Old Binding
        '            If Not bindingToDelete Is Nothing Then
        '                serverManager.Sites("portal").Bindings.Remove(bindingToDelete)
        '            End If
        '            'Create New Binding
        '            If newBinding <> oldBinding Then
        '                Dim b As Microsoft.Web.Administration.Binding = serverManager.Sites("portal").Bindings.CreateElement()
        '                b.SetAttributeValue("protocol", "http")
        '                b.SetAttributeValue("bindingInformation", ":80:" + newBinding)
        '                serverManager.Sites("portal").Bindings.Add(b)
        '            End If
        '            serverManager.CommitChanges()
        '            'Update New Host Name
        '            objCommon.Mode = 12
        '            objCommon.UpdateRecordID = Session("DomainID")
        '            objCommon.Comments = txtPortalName.Text.Trim
        '            objCommon.UpdateSingleFieldValue()

        '            ''List all bindings for given site
        '            'For Each b As Microsoft.Web.Administration.Binding In serverManager.Sites("portal").Bindings
        '            '    Response.Write(b.BindingInformation & "<br>")
        '            'Next
        '        End If
        '    Catch ex As Exception
        '        If ConfigurationManager.AppSettings("IsDebugMode") = "true" Then
        '            Throw ex
        '        Else
        '            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
        '        End If
        '        'Throw ex
        '        'do not throw exception , for developement 
        '    End Try

        '    Dim dtTable As DataTable
        '    Dim objUserAccess As New UserAccess
        '    objUserAccess.DomainID = Session("DomainID")
        '    dtTable = objUserAccess.GetDomainDetails()
        '    If dtTable.Rows.Count > 0 Then
        '        Session("PortalURL") = "http://" & CCommon.ToString(dtTable.Rows(0)("vcPortalName")) & ".bizautomation.com/Login.aspx"
        '    End If
        '    SetPortalLink()
        'Catch ex As Exception
        '    ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
        '    Response.Write(ex)
        'End Try
        'End Sub

        Private Sub LoadTerms()
            Try
                Dim objOpp As New BusinessLogic.Opportunities.OppotunitiesIP
                Dim dtTerms As New DataTable
                With objOpp
                    .DomainID = Session("DomainId")
                    .TermsID = 0
                    .ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
                    dtTerms = .GetTerms()
                End With

                If dtTerms IsNot Nothing Then
                    gvTerms.DataSource = dtTerms
                    gvTerms.DataBind()
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub gvTerms_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles gvTerms.RowCommand
            Try
                If e.CommandName = "DeleteRow" Then
                    Dim objOpp As New BusinessLogic.Opportunities.OppotunitiesIP
                    objOpp.DomainID = Session("DomainID")
                    objOpp.TermsID = CCommon.ToLong(e.CommandArgument)
                    litMessage.Text = objOpp.DeleteTerms()
                    LoadTerms()
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Sub gvTerms_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles gvTerms.RowDataBound
            Try
                If e.Row.RowType = DataControlRowType.DataRow Then
                    'Dim lnkBtnEdit As LinkButton
                    'lnkBtnEdit = DirectCast(e.Row.FindControl("lnkBtnEdit"), LinkButton)
                    'If lnkBtnEdit IsNot Nothing Then

                    '    lnkBtnEdit.Text = "Edit"
                    '    lnkBtnEdit.Attributes.Add("onclick", "return OpenTerms(" & CCommon.ToDouble(DataBinder.Eval(e.Row.DataItem, "numTermsID")) & "," & _
                    '                              If(CCommon.ToBool(DataBinder.Eval(e.Row.DataItem, "IsUsed")) = True, 1, 0) & ");")

                    'End If

                    'Allowing all the terms to be edited by Manish : AS per modification suggested by Carl : 16th Feb,2015
                    Dim imgbtnEdit As ImageButton
                    imgbtnEdit = DirectCast(e.Row.FindControl("imgbtnEdit"), ImageButton)
                    If imgbtnEdit IsNot Nothing Then
                        imgbtnEdit.Attributes.Add("onclick", "return OpenTerms(" & CCommon.ToDouble(DataBinder.Eval(e.Row.DataItem, "numTermsID")) & "," &
                                                  If(CCommon.ToBool(DataBinder.Eval(e.Row.DataItem, "IsUsed")) = True, 1, 0) & ");")

                    End If

                    Dim btnDeleteRow As Button
                    btnDeleteRow = DirectCast(e.Row.FindControl("btnDeleteRow"), Button)
                    If btnDeleteRow IsNot Nothing Then
                        If CCommon.ToBool(DataBinder.Eval(e.Row.DataItem, "IsUsed")) = False Then
                            btnDeleteRow.Visible = True
                            DirectCast(e.Row.FindControl("lblDelete"), Label).Visible = False
                        Else
                            btnDeleteRow.Visible = False
                            DirectCast(e.Row.FindControl("lblDelete"), Label).Visible = True
                            DirectCast(e.Row.FindControl("lblDelete"), Label).Text = "<a href="""" style=""text-decoration:underline"" onclick=""return ShowDialog();"" >*</a>"
                        End If

                    End If
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub
        Function GenerateCheckedItemString(ByVal radComboBox As RadComboBox) As String
            Dim strTransactionType As String
            For Each item As RadComboBoxItem In radComboBox.CheckedItems
                If CCommon.ToLong(item.Value) > 0 Then
                    strTransactionType = strTransactionType & item.Value & ","
                End If
            Next
            Return strTransactionType
        End Function
        Function SetCheckedItemString(ByVal radComboBox As RadComboBox, ByVal strValues As String)

            Dim strArr As String()
            strArr = strValues.Split(",")
            For Each indiv As String In strArr
                Dim radComboItem As RadComboBoxItem
                radComboItem = radComboBox.FindItemByValue(indiv)
                If radComboItem IsNot Nothing Then
                    radComboItem.Checked = True
                End If
            Next
        End Function
        Private Sub LoadPortalTabs(ByVal strCheckedItems As String)
            Try
                For Each strItem As String In strCheckedItems.Split(",")
                    If chkLstHideTabs.Items.FindByValue(CCommon.ToShort(strItem)) IsNot Nothing Then
                        chkLstHideTabs.Items.FindByValue(CCommon.ToShort(strItem)).Selected = True
                    End If
                Next
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub btnSaveProfile_Click(sender As Object, e As EventArgs) Handles btnSaveProfile.Click
            Try
                btnSave_Click(sender, e)
                litMessage.Text = "Profile Saved."
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

#Region "Pay Bill WFA"
        Sub BindData()
            Try
                'Bind Bizdoc status related to Bills
                Dim dtBizDocStatus As DataTable = objCommon.GetMasterListItems(11, Session("DomainID"), 644, 0)

                FillBizDocStatus(ddlBizDocStatus15, dtBizDocStatus)
                FillBizDocStatus(ddlBizDocStatus16, dtBizDocStatus)

            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub FillBizDocStatus(ByRef ddlBizDocStatus As DropDownList, ByVal dtBizDocStatus As DataTable)
            Try
                ddlBizDocStatus.DataSource = dtBizDocStatus
                ddlBizDocStatus.DataTextField = "vcData"
                ddlBizDocStatus.DataValueField = "numListItemID"
                ddlBizDocStatus.DataBind()
                ddlBizDocStatus.Items.Insert(0, "--Select One--")
                ddlBizDocStatus.Items.FindByText("--Select One--").Value = "0"
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub BindRuleData()
            Try
                ddlBizDocStatus15.ClearSelection()
                ddlBizDocStatus16.ClearSelection()

                Dim objAdmin As New CAdmin
                objAdmin.DomainID = Session("DomainID")
                objAdmin.byteMode = 0
                objAdmin.OppType = 2
                Dim dtRules As DataTable = objAdmin.ManageOpportunityAutomationRules()

                'Rule 15
                Dim foundRows As DataRow() = dtRules.Select("numRuleID=15")
                If foundRows.Length > 0 Then
                    If Not ddlBizDocStatus15.Items.FindByValue(foundRows(0)("numBizDocStatus1")) Is Nothing Then
                        ddlBizDocStatus15.Items.FindByValue(foundRows(0)("numBizDocStatus1")).Selected = True
                    End If
                End If

                'Rule 16
                foundRows = dtRules.Select("numRuleID=16")
                If foundRows.Length > 0 Then
                    If Not ddlBizDocStatus16.Items.FindByValue(foundRows(0)("numBizDocStatus1")) Is Nothing Then
                        ddlBizDocStatus16.Items.FindByValue(foundRows(0)("numBizDocStatus1")).Selected = True
                    End If
                End If

            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub SavePayBillWFAData()
            Try
                Dim dtTable As New DataTable
                dtTable.TableName = "Table"

                CCommon.AddColumnsToDataTable(dtTable, "numRuleID,numBizDocStatus1,numBizDocStatus2,numOrderStatus")

                Dim dr As DataRow

                'Rule 15
                If ddlBizDocStatus15.SelectedValue > 0 Then
                    dr = dtTable.NewRow
                    dr("numRuleID") = 15
                    dr("numBizDocStatus1") = ddlBizDocStatus15.SelectedValue
                    dtTable.Rows.Add(dr)
                End If

                'Rule 16
                If ddlBizDocStatus16.SelectedValue > 0 Then
                    dr = dtTable.NewRow
                    dr("numRuleID") = 16
                    dr("numBizDocStatus1") = ddlBizDocStatus16.SelectedValue
                    dtTable.Rows.Add(dr)
                End If

                Dim ds As New DataSet
                ds.Tables.Add(dtTable)

                Dim objAdmin As New CAdmin
                objAdmin.DomainID = Session("DomainID")
                objAdmin.strItems = ds.GetXml
                objAdmin.byteMode = 1
                objAdmin.OppType = 2
                objAdmin.ManageOpportunityAutomationRules()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

#End Region

        Sub BindBizDocsTemplate()
            Try
                ddlPODropShipBizDocTemplate.ClearSelection()
                ddlPODropShipBizDocTemplate.Items.Clear()

                If CCommon.ToLong(ddlPODropShipBizDoc.SelectedValue) > 0 Then
                    Dim objOppBizDoc As New BusinessLogic.Opportunities.OppBizDocs
                    objOppBizDoc.DomainID = Session("DomainID")
                    objOppBizDoc.BizDocId = ddlPODropShipBizDoc.SelectedValue
                    objOppBizDoc.OppType = 2

                    Dim dtBizDocTemplate As DataTable = objOppBizDoc.GetBizDocTemplateList()

                    ddlPODropShipBizDocTemplate.DataSource = dtBizDocTemplate
                    ddlPODropShipBizDocTemplate.DataTextField = "vcTemplateName"
                    ddlPODropShipBizDocTemplate.DataValueField = "numBizDocTempID"
                    ddlPODropShipBizDocTemplate.DataBind()
                End If

                ddlPODropShipBizDocTemplate.Items.Insert(0, New ListItem("--Select--", 0))
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub ddlPODropShipBizDoc_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlPODropShipBizDoc.SelectedIndexChanged
            Try
                BindBizDocsTemplate()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Sub lkbOpenOrganizationRating_Click(sender As Object, e As EventArgs) Handles lkbOpenOrganizationRating.Click
            Try
                BindOrganizationRatingDropDown()
                BindOrganizationRatingGrid()
                radORFrom.Value = 0
                radORTo.Value = 0
                ddlPerformance.SelectedValue = "0"
                ddlRating.SelectedValue = "0"
                ddlPerformance.Focus()
                ScriptManager.RegisterClientScriptBlock(upOrganizationRating, upOrganizationRating.GetType(), "OpenORWindow", "$('#divOrganizationRatingRule').modal('show');", True)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Sub lnkLeadSource_Click(sender As Object, e As EventArgs) Handles lnkLeadSource.Click
            Try
                txtSearchDomain.Focus()
                BindLeadSource()
                BindReferringGrid()
                ScriptManager.RegisterClientScriptBlock(upLeadSource, upLeadSource.GetType(), "OpenLSWindow", "$('#divLeadSource').modal('show');", True)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Sub BindLeadSource()
            Try
                objCommon = New CCommon
                Dim dtTable As DataTable
                dtTable = objCommon.GetMasterListItems(18, Session("DomainId"))
                If dtTable IsNot Nothing AndAlso dtTable.Rows.Count > 0 Then
                    ddlLeadSource.DataValueField = "numListItemID"
                    ddlLeadSource.DataTextField = "vcData"
                    ddlLeadSource.DataSource = dtTable
                    ddlLeadSource.DataBind()
                    ddlLeadSource.Items.Insert(0, New ListItem("--Select--", "0"))
                End If
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Private Sub btnAddLeadSource_Click(sender As Object, e As EventArgs) Handles btnAddLeadSource.Click
            Try
                If txtSearchDomain.Text = "" OrElse txtSearchDomain.Text = "http://" Then
                    'litMessage.Text = "Enter domain name."
                    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "DomainName", "alert('Enter domain name.');", True)
                    txtSearchDomain.Focus()
                    Exit Sub
                End If
                If ddlLeadSource.SelectedValue = "0" Then
                    'litMessage.Text = "Select lead source."
                    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "LeadSource", "alert('Select lead source.');", True)
                    ddlLeadSource.Focus()
                    Exit Sub
                End If

                objCommon = New CCommon

                objCommon.ListItemID = ddlLeadSource.SelectedValue
                objCommon.DomainID = Session("DomainId")
                objCommon.ManageWebReferringInfo(txtSearchDomain.Text)
                BindReferringGrid()
            Catch ex As Exception
                If ex.Message = "DUPLICATE_REFERRER" Then
                    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "PopUpAlert", "alert('The value you�re entering on the referring page source is not unique. Change your value and try again.');", True)
                    'ElseIf ex.Message = "DIVISION_NULL" Then
                    '    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "PopUpAlert", "alert('Lead source is not associated with any division.');", True)
                Else
                    ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                    Response.Write(ex)
                End If
            End Try
        End Sub

        Private Sub BindReferringGrid()
            objCommon = New CCommon
            Dim dtTable As DataTable

            objCommon.DomainID = Session("DomainId")
            dtTable = objCommon.GetWebReferringInfo()
            If dtTable.Rows.Count > 0 Then
                gvLeadSource.DataSource = dtTable
                gvLeadSource.DataBind()
            Else
                gvLeadSource.DataSource = Nothing
                gvLeadSource.DataBind()
            End If
        End Sub

        'Private Sub gvLeadSource_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles gvLeadSource.RowCommand
        '    Try
        '        If e.CommandName = "Delete" Then
        '            Dim objOpp As New BusinessLogic.Opportunities.OppotunitiesIP
        '            objOpp.DomainID = Session("DomainID")
        '            objOpp.TrackingID = CCommon.ToLong(e.CommandArgument)
        '            litMessage.Text = objOpp.DeleteLeadSourceReferringURL()
        '            BindReferringGrid()
        '        End If
        '    Catch ex As Exception
        '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
        '        DisplayError(ex.Message)
        '    End Try
        'End Sub

        Private Sub BindOrganizationRatingDropDown()
            Try
                objCommon.sb_FillComboFromDBwithSel(ddlRating, 2, Session("DomainID"))
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Private Sub BindOrganizationRatingGrid()
            Try
                Dim objOrganizationRatingRule As New OrganizationRatingRule
                objOrganizationRatingRule.DomainID = Session("DomainID")
                Dim dt As DataTable = objOrganizationRatingRule.GetByDomainID()

                gvOrganizationRating.DataSource = dt
                gvOrganizationRating.DataBind()

                ViewState("OrganizationRatingRule") = dt
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Private Sub btnAddOrganizationRatingRule_Click(sender As Object, e As EventArgs) Handles btnAddOrganizationRatingRule.Click
            Try
                Dim dt As DataTable = ViewState("OrganizationRatingRule")

                If ddlPerformance.SelectedValue = "0" Then
                    lblError.Text = "Select performance."
                    ddlPerformance.Focus()
                    Exit Sub
                End If

                If ddlRating.SelectedValue = "0" Then
                    lblError.Text = "Select organization rating."
                    ddlRating.Focus()
                    Exit Sub
                End If

                If dt.Select("tintPerformance <> " & ddlPerformance.SelectedValue).Length > 0 Then
                    lblError.Text = "Only one performance option selection is allowed for all ratings."
                    Exit Sub
                End If

                If dt.Select("(" & radORFrom.Value & " >= numFromAmount AND " & radORFrom.Value & " <= numToAmount) OR (" & radORTo.Value & " >= numFromAmount AND " & radORTo.Value & " <= numToAmount)").Length > 0 Then
                    lblError.Text = "An existing rule using a number within this range is already in use.  You�ll need to delete that rule first, or use a different value range for this one."
                    Exit Sub
                End If

                Dim dr As DataRow = dt.NewRow()
                dr("tintPerformance") = ddlPerformance.SelectedValue
                dr("numFromAmount") = radORFrom.Value
                dr("numToAmount") = radORTo.Value
                dr("numOrganizationRatingID") = ddlRating.SelectedValue
                dr("vcOrganizationRatingRule") = "If the performance (Order Sub-total for period selected) amount for " & ddlPerformance.SelectedItem.Text & " is >= " & radORFrom.Value & " and <= " & radORTo.Value & " set Organization Rating to " & ddlRating.SelectedItem.Text
                dt.Rows.Add(dr)

                gvOrganizationRating.DataSource = dt
                gvOrganizationRating.DataBind()

                ViewState("OrganizationRatingRule") = dt
                radORFrom.Focus()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                lblError.Text = ex.Message
            End Try
        End Sub

        Private Sub lkbSaveOrganizationRatingRule_Click(sender As Object, e As EventArgs) Handles lkbSaveOrganizationRatingRule.Click
            Try
                Dim ds As New DataSet
                Dim dt As New DataTable("Rule")
                dt = DirectCast(ViewState("OrganizationRatingRule"), DataTable).Copy()
                ds.Tables.Add(dt)

                Dim objOrganizationRatingRule As New OrganizationRatingRule
                objOrganizationRatingRule.DomainID = Session("DomainID")
                objOrganizationRatingRule.UserCntID = Session("UserContactID")
                objOrganizationRatingRule.Save(ds.GetXml())

                ScriptManager.RegisterClientScriptBlock(upOrganizationRating, upOrganizationRating.GetType(), "OpenORWindow", "$('#divOrganizationRatingRule').modal('hide');", True)
            Catch ex As Exception
                If ex.Message.Contains("DIFFERENT_PERFORMANCE_RATING") Then
                    lblError.Text = "Only one performance option selection is allowed for all ratings."
                ElseIf ex.Message.Contains("RULE_EXISTS_WITH_NUMBER") Then
                    lblError.Text = "An existing rule using a number within this range is already in use.  You�ll need to delete that rule first, or use a different value range for this one."
                Else
                    ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                    lblError.Text = ex.Message
                End If
            End Try
        End Sub

        Private Sub gvOrganizationRating_RowDeleting(sender As Object, e As GridViewDeleteEventArgs) Handles gvOrganizationRating.RowDeleting
            Try
                Dim dt As DataTable = ViewState("OrganizationRatingRule")
                dt.Rows.RemoveAt(e.RowIndex)

                gvOrganizationRating.DataSource = dt
                gvOrganizationRating.DataBind()

                ViewState("OrganizationRatingRule") = dt
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                lblError.Text = ex.Message
            End Try
        End Sub

        Private Sub ShowMessage(ByVal message As String)
            Try
                divMessage.Style.Add("display", "")
                litMessage.Text = message
                divMessage.Focus()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Protected Sub gvLeadSource_RowDeleting(sender As Object, e As GridViewDeleteEventArgs)
            Try
                Dim objOpp As New BusinessLogic.Opportunities.OppotunitiesIP
                Dim _trackingID As Long = CInt(gvLeadSource.DataKeys(e.RowIndex).Value)
                objOpp.DomainID = Session("DomainID")
                objOpp.TrackingID = _trackingID
                litMessage.Text = objOpp.DeleteLeadSourceReferringURL()
                BindReferringGrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Sub BindShippingCompanyDataGrid()
            Try
                objCommon = New CCommon
                Dim dtTable As DataTable
                dtTable = objCommon.GetMasterListItems(82, Session("DomainId"))
                dgShipcmp.DataSource = dtTable
                dgShipcmp.DataBind()
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Private Sub dgShipcmp_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgShipcmp.ItemDataBound
            Try
                Dim objItem As New CItems
                objItem.DomainID = Session("DomainID")

                If e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.Item Then

                    objItem.ShippingCMPID = e.Item.Cells(0).Text
                    Dim dtFields As DataTable = objItem.ShippingDtls.Tables(0)

                    If dtFields.Rows.Count = 0 Then
                        e.Item.FindControl("hplEditData").Visible = False
                    Else
                        e.Item.FindControl("hplEditData").Visible = True
                    End If
                    Dim hplEdit As HyperLink = CType(e.Item.FindControl("hplEditData"), HyperLink)
                    hplEdit.Attributes.Add("onclick", "return OpenComDTL(" & e.Item.Cells(0).Text & ",'" & CType(e.Item.FindControl("hlData"), HyperLink).Text & "')")
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex)
            End Try
        End Sub

        Private Sub BindServiceTypes()
            Try
                Dim dtShippingMethod As New DataTable

                Dim objShippingRule As New ShippingRule
                objShippingRule.DomainID = Session("DomainID")
                objShippingRule.RuleID = 0
                objShippingRule.ServiceTypeID = 0
                objShippingRule.byteMode = 2

                dtShippingMethod = objShippingRule.GetShippingServiceType()

                Dim datarows As DataRow() = dtShippingMethod.Select("numServiceTypeID not in('101','102','103')", "") '.Select("numServiceTypeID <> 102") '.Select("numServiceTypeID <> 103")
                If datarows IsNot Nothing Then
                    If datarows.Length > 0 Then
                        dtShippingMethod = datarows.CopyToDataTable()
                    End If
                Else
                    dtShippingMethod = New DataTable()
                End If

                dgRealTimeShippingQuotes.DataSource = dtShippingMethod
                dgRealTimeShippingQuotes.DataBind()

            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex)
            End Try
        End Sub
        Private Sub DisplayError(ByVal exception As Exception)
            Try
                TryCast(Me.Master, ECommerceMenuMaster).ThrowError(exception)
            Catch ex As Exception

            End Try
        End Sub

        Private Sub dgRealTimeShippingQuotes_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgRealTimeShippingQuotes.ItemDataBound
            Try
                If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
                    Dim lblNSoftEnm As Label = e.Item.FindControl("lblNSoftEnm")
                    If CCommon.ToInteger(lblNSoftEnm.Text) > 0 Then
                        CType(e.Item.FindControl("txtShippingCaption"), TextBox).Visible = False
                    Else
                        CType(e.Item.FindControl("txtRate"), TextBox).Visible = True
                        CType(e.Item.FindControl("chkIsPercentage"), CheckBox).Visible = False
                        e.Item.FindControl("lblIsPercentage").Visible = False
                        CType(e.Item.FindControl("txtMarkup"), TextBox).Visible = False
                        CType(e.Item.FindControl("lblServiceName"), Label).Visible = False
                        CType(e.Item.FindControl("btnDelete"), Button).Visible = True
                    End If
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Protected Sub btnSaveParcelShipping_Click(sender As Object, e As EventArgs)
            Try
                Dim objShippingRule = New ShippingRule()
                If objShippingRule Is Nothing Then objShippingRule = New ShippingRule()
                objShippingRule.DomainID = Session("DomainID")
                objShippingRule.byteMode = 2 'Update

                objShippingRule.ServiceTypeID = 2
                objShippingRule.Str = GetItemsForRealShippingQuotes()
                objShippingRule.ManageShippingServiceTypes()
                BindServiceTypes()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex)
            End Try
        End Sub

        Function GetItemsForRealShippingQuotes() As String
            Try
                Dim ds As New DataSet
                Dim dt As New DataTable
                Dim dr As DataRow
                CCommon.AddColumnsToDataTable(dt, "numServiceTypeID,vcServiceName,intFrom,intTo,fltMarkup,bitMarkupType,monRate,bitEnabled")

                For Each gvr As DataGridItem In dgRealTimeShippingQuotes.Items
                    dr = dt.NewRow()
                    dr("numServiceTypeID") = gvr.Cells(0).Text.Trim
                    dr("vcServiceName") = CType(gvr.FindControl("txtShippingCaption"), TextBox).Text
                    dr("intFrom") = CType(gvr.FindControl("txtFrom"), TextBox).Text
                    dr("intTo") = CType(gvr.FindControl("txtTo"), TextBox).Text
                    dr("fltMarkup") = CType(gvr.FindControl("txtMarkup"), TextBox).Text
                    dr("bitMarkupType") = CType(gvr.FindControl("chkIsPercentage"), CheckBox).Checked
                    dr("monRate") = CType(gvr.FindControl("txtRate"), TextBox).Text
                    dr("bitEnabled") = CType(gvr.FindControl("chk"), CheckBox).Checked
                    dt.Rows.Add(dr)
                Next
                ds.Tables.Add(dt.Copy)
                ds.Tables(0).TableName = "Item"
                Return ds.GetXml()
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Protected Sub ddlUserGroup_SelectedIndexChanged(sender As Object, e As EventArgs)
            SetEmployeeByGroupId()
        End Sub
    End Class
End Namespace