﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/common/DetailPage.Master" CodeBehind="frmParentChildFieldMap.aspx.vb" Inherits="BACRM.UserInterface.Admin.frmParentChildFildMap" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <title>Parent Child Field Mapping</title>

    <script type="text/javascript">
        function Save() {
            if (document.getElementById("ddlParentModule").selectedIndex == 0) {
                alert("Select a Parent Module");
                document.getElementById("ddlParentModule").focus();
                return false;
            }

            if (document.getElementById("ddlParentField").selectedIndex == 0) {
                alert("Select a Parent Field");
                document.getElementById("ddlParentField").focus();
                return false;
            }

            if (document.getElementById("ddlChildModule").selectedIndex == 0) {
                alert("Select a Child Module");
                document.getElementById("ddlChildModule").focus();
                return false;
            }

            if (document.getElementById("ddlChildField").selectedIndex == 0) {
                alert("Select a Child Field");
                document.getElementById("ddlChildField").focus();
                return false;
            }

            return true;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="RecordInformationPanel" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="RecordActionPanel" runat="server">
    <div id="divMessage" runat="server" class="row padbottom10" style="display: none">
        <div class="col-xs-12">
            <div class="alert alert-warning">
                <h4><i class="icon fa fa-warning"></i>Alert!</h4>
                <asp:Literal ID="litMessage" runat="server"></asp:Literal>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="DetailPageTitle" runat="server">
    Parent/Child Field Mapping
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="UtilityLinksPanel" runat="server">
    <asp:LinkButton ID="btnSave" runat="server" class="btn btn-primary" OnClientClick="return Save();"><i class="fa fa-floppy-o"></i>&nbsp;&nbsp;Save</asp:LinkButton>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="TabsPlaceHolder" runat="server">
    <div class="row">
        <div class="col-xs-12 col-sm-6 col-md-3">
            <div class="form-group">
                <label>Parent Module</label>
                <asp:DropDownList ID="ddlParentModule" runat="server" AutoPostBack="true" CssClass="form-control">
                    <asp:ListItem Value="0">--Select--</asp:ListItem>
                    <asp:ListItem Value="1">Leads/Prospects/Accounts</asp:ListItem>
                    <asp:ListItem Value="2">Sales Opportunities / Orders</asp:ListItem>
                </asp:DropDownList>
            </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-3">
            <div class="form-group">
                <label>Parent Field</label>
                <asp:DropDownList ID="ddlParentField" runat="server" CssClass="form-control">
                    <asp:ListItem Value="0">--Select--</asp:ListItem>
                </asp:DropDownList>
            </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-3">
            <div class="form-group">
                <label>Child Module</label>
                <asp:DropDownList ID="ddlChildModule" runat="server" AutoPostBack="true" CssClass="form-control">
                    <asp:ListItem Value="0">--Select--</asp:ListItem>
                    <asp:ListItem Value="2">Sales Opportunities / Orders</asp:ListItem>
                    <asp:ListItem Value="6">Purchase Opportunities / Orders</asp:ListItem>
                    <asp:ListItem Value="3">Case</asp:ListItem>
                    <asp:ListItem Value="11">Projects</asp:ListItem>
                </asp:DropDownList>
            </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-3">
            <div class="form-group">
                <label>Child Field</label>
                <asp:DropDownList ID="ddlChildField" runat="server" CssClass="form-control">
                    <asp:ListItem Value="0">--Select--</asp:ListItem>
                </asp:DropDownList>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary box-solid">
                <div class="box-header with-border">
                    <h3 class="box-title">Parent/Child Field Mapping List</h3>

                    <div class="box-tools pull-right">
                    </div>
                    <!-- /.box-tools -->
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="row padbottom10">
                        <div class="col-xs-12">
                            <div class="form-inline">
                                <label>Child Module:</label>
                                <asp:DropDownList ID="ddlChildModuleSearch" runat="server" CssClass="form-control" AutoPostBack="true" Width="250">
                                    <asp:ListItem Value="0">--All--</asp:ListItem>
                                    <asp:ListItem Value="2">Sales Opportunities / Orders</asp:ListItem>
                                    <asp:ListItem Value="6">Purchase Opportunities / Orders</asp:ListItem>
                                    <asp:ListItem Value="3">Case</asp:ListItem>
                                    <asp:ListItem Value="11">Projects</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="table-responsive">
                                <asp:GridView ID="gvFieldMap" runat="server" EnableViewState="true" AutoGenerateColumns="false"
                                    CssClass="table table-bordered table-striped" Width="100%" ShowHeaderWhenEmpty="true" DataKeyNames="numParentChildFieldID">
                                    <Columns>
                                        <asp:BoundField DataField="vcParentModule" HeaderText="Parent Module" />
                                        <asp:BoundField DataField="vcParentField" HeaderText="Parent Field" />
                                        <asp:BoundField DataField="vcChildModule" HeaderText="Child Module" />
                                        <asp:BoundField DataField="vcChildField" HeaderText="Child Field" />
                                        <asp:ButtonField CommandName="DeleteRow" ButtonType="Button" Text="&#xf1f8;" ItemStyle-Width="25" ControlStyle-Font-Names="FontAwesome" ControlStyle-CssClass="btn btn-xs btn-danger" />
                                    </Columns>
                                    <EmptyDataTemplate>
                                        No records Found.
                                    </EmptyDataTemplate>
                                </asp:GridView>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
