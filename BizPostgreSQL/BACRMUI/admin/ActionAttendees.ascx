﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ActionAttendees.ascx.vb"
    Inherits=".ActionAttendees" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="../common/frmCorrespondence.ascx" TagName="frmCorrespondence" TagPrefix="uc1" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<script language="javascript" type="text/javascript">
    function SaveAttendee() {
        if (document.getElementById("ddlAttendeeUserType").value == 0) {
            alert("Select Attendee User Type")
            document.getElementById("ddlAttendeeUserType").focus();
            return false;
        }

        if (document.getElementById("ddlAttendeeUserType").value == 2) {
            if ($find('<%# radAttendeeCmbCompany.ClientID%>').get_value() == "") {
                alert("Select Attendee Contact Company")
                return false;
            }
        }

        if (document.getElementById("ddlAttendeeUser").value == 0) {
            alert("Select Attendee User")
            document.getElementById("ddlAttendeeUser").focus();
            return false;
        }
    }
</script>
<asp:UpdatePanel ID="UpdatePanelAttendee" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="true">
    <ContentTemplate>
        <div class="form-inline padbottom10">
                    
            <div class="form-group" id="trAttendeeCmbCompany" runat="server" visible="false">
                <label>Customer</label>
                <telerik:RadComboBox AccessKey="C" ID="radAttendeeCmbCompany" DropDownWidth="200px" Width="200"
                    Skin="Default" runat="server" AutoPostBack="True" AllowCustomText="True" EnableLoadOnDemand="True">
                    <WebServiceSettings Path="../common/Common.asmx" Method="GetCompanies" />
                </telerik:RadComboBox>
            </div>
            <div class="form-group">
                <label>Contact</label>
                <asp:DropDownList ID="ddlAttendeeUser" CssClass="form-control" runat="server" Width="200">
                </asp:DropDownList>
                <asp:HyperLink ID="hplEmpAvaliability" runat="server" NavigateUrl="#" Visible="false">Availability</asp:HyperLink>
            </div>
            <div class="form-group">
                <label>User Type</label>
                <asp:DropDownList ID="ddlAttendeeUserType" runat="server" CssClass="form-control" AutoPostBack="true" Width="200">
                    <asp:ListItem Value="0">--Select--</asp:ListItem>
                    <asp:ListItem Value="1">Employee</asp:ListItem>
                    <asp:ListItem Value="2">Contacts</asp:ListItem>
                </asp:DropDownList>
            </div>
            <asp:Button ID="btnAttendeeAdd" Text="Add" CssClass="btn btn-primary" runat="server"></asp:Button>
            <asp:Button ID="btnSendAlert" Text="Notify Selected" CssClass="btn btn-primary" runat="server" Visible="false"></asp:Button>
        </div>
        <asp:GridView ID="gvAttendee" runat="server" Width="100%" AllowSorting="true" CssClass="table table-bordered table-striped" AutoGenerateColumns="False" DataKeyNames="numContactId" OnRowDataBound="gvAttendee_RowDataBound">
            <Columns>
                <asp:BoundField DataField="vcCompanyname" HeaderText="Organization" ItemStyle-HorizontalAlign="Center" />
                <asp:BoundField DataField="vcContact" HeaderText="Contact" ItemStyle-HorizontalAlign="Center" />
                <asp:BoundField DataField="vcPosition" HeaderText="Position" ItemStyle-HorizontalAlign="Center" />
                <asp:BoundField DataField="vcEmail" HeaderText="Email Address" ItemStyle-HorizontalAlign="Center" />
                <asp:BoundField DataField="numPhone" HeaderText="Contact Phone" ItemStyle-HorizontalAlign="Center" />
                <asp:BoundField DataField="numPhoneExtension" HeaderText="Extension" ItemStyle-HorizontalAlign="Center" />
                <asp:BoundField DataField="vcStatus" HeaderText="Status" ItemStyle-HorizontalAlign="Center" />
                <asp:TemplateField ItemStyle-Width="20">
                    <ItemTemplate>
                        <asp:LinkButton ID="lnkDelete" CssClass="btn btn-xs btn-danger" CommandArgument='<%# Eval("numContactId")%>' CommandName="Delete" runat="server" ToolTip="delete"><i class="fa fa-trash"></i></asp:LinkButton>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Notify" Visible="false">
                    <ItemTemplate>
                        <asp:CheckBox ID="chkNotify" runat="server"></asp:CheckBox>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
    </ContentTemplate>
</asp:UpdatePanel>
    