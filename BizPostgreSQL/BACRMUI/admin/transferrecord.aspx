<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" CodeBehind="transferrecord.aspx.vb"
    Inherits="BACRM.UserInterface.Admin.transferrecord" MasterPageFile="~/common/Popup.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Transfer OwnerShip</title>
    <script language="javascript">
        function check() {
            if (document.form1.ddlEmployee.value == 0) {
                alert("Please Select User")
                document.form1.ddlEmployee.focus()
                return false;
            }
        }
        function openEmpAvailability() {
            var ProID;
            var ProjName;
            var StageName;

            ProID = $('#hdnProjectID').val();
            ProjName = $('#hdnProjectName').val();
            StageName = $('#hdnStageName').val();

            window.open("../ActionItems/frmEmpAvailability.aspx?frm=Tickler&OrgName=" + ProjName + "&BProcessName=" + StageName + "&ProID=" + ProID, '', 'toolbar=no,titlebar=no,top=0,left=100,width=850,height=500,scrollbars=yes,resizable=yes')
            return false;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <div class="input-part">
        <div class="right-input">
            <asp:Button ID="btnTransfer" runat="server" Text="Transfer Ownership" CssClass="button"></asp:Button>
            <asp:Button ID="btnCancel" runat="server" Text="Close" CssClass="button"></asp:Button>&nbsp;&nbsp;
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageTitle" runat="server" ClientIDMode="Static">
    <asp:Label ID="lblTab" runat="server" Text="Transfer
                            Record Ownership"></asp:Label>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="Content" runat="server" ClientIDMode="Static">
    <table width="600px">
        <tr>
            <td class="normal4" align="center" colspan="2">
                <asp:Literal ID="litMessage" runat="server" EnableViewState="False"></asp:Literal>
            </td>
        </tr>
        <tr>
            <td class="normal1" align="right">Select User
            </td>
            <td class="normal1">
                <asp:DropDownList ID="ddlEmployee" CssClass="signup" runat="server">
                </asp:DropDownList>
                &nbsp;&nbsp;<asp:HyperLink ID="hplEmpAvaliability" Visible="false" runat="server"
                    NavigateUrl="#" onclick="return openEmpAvailability()"><font color="#180073">Check Availability</font></asp:HyperLink>
                <asp:HiddenField ID="hdnProjectID" runat="server"  />
                <asp:HiddenField ID="hdnProjectName" runat="server"  />
                <asp:HiddenField ID="hdnStageName" runat="server"  />

            </td>
        </tr>
    </table>
</asp:Content>
