'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On

Namespace BACRM.UserInterface.Admin

    Partial Public Class frmAdminBusinessProcess

        '''<summary>
        '''ddlConfiguration control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents ddlConfiguration As Global.System.Web.UI.WebControls.DropDownList

        '''<summary>
        '''ddlProcess control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents ddlProcess As Global.System.Web.UI.WebControls.DropDownList

        '''<summary>
        '''rfvKitWareHouse control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents rfvKitWareHouse As Global.System.Web.UI.WebControls.RequiredFieldValidator

        '''<summary>
        '''ddlProcessList control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents ddlProcessList As Global.System.Web.UI.WebControls.DropDownList

        '''<summary>
        '''txtName control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents txtName As Global.System.Web.UI.WebControls.TextBox

        '''<summary>
        '''ddlStag control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents ddlStag As Global.System.Web.UI.WebControls.DropDownList

        '''<summary>
        '''btnNewProcess control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents btnNewProcess As Global.System.Web.UI.WebControls.LinkButton

        '''<summary>
        '''btnDelete control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents btnDelete As Global.System.Web.UI.HtmlControls.HtmlButton

        '''<summary>
        '''rdnTaskType control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents rdnTaskType As Global.System.Web.UI.WebControls.RadioButtonList

        '''<summary>
        '''lblMessage control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents lblMessage As Global.System.Web.UI.WebControls.Label

        '''<summary>
        '''ValidationSummary1 control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents ValidationSummary1 As Global.System.Web.UI.WebControls.ValidationSummary

        '''<summary>
        '''txtMSMaxStage5 control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents txtMSMaxStage5 As Global.System.Web.UI.WebControls.TextBox

        '''<summary>
        '''txtMSMaxStage20 control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents txtMSMaxStage20 As Global.System.Web.UI.WebControls.TextBox

        '''<summary>
        '''txtMSMaxStage25 control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents txtMSMaxStage25 As Global.System.Web.UI.WebControls.TextBox

        '''<summary>
        '''txtMSMaxStage35 control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents txtMSMaxStage35 As Global.System.Web.UI.WebControls.TextBox

        '''<summary>
        '''txtMSMaxStage40 control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents txtMSMaxStage40 As Global.System.Web.UI.WebControls.TextBox

        '''<summary>
        '''txtMSMaxStage50 control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents txtMSMaxStage50 As Global.System.Web.UI.WebControls.TextBox

        '''<summary>
        '''txtMSMaxStage60 control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents txtMSMaxStage60 As Global.System.Web.UI.WebControls.TextBox

        '''<summary>
        '''txtMSMaxStage65 control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents txtMSMaxStage65 As Global.System.Web.UI.WebControls.TextBox

        '''<summary>
        '''txtMSMaxStage75 control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents txtMSMaxStage75 As Global.System.Web.UI.WebControls.TextBox

        '''<summary>
        '''txtMSMaxStage80 control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents txtMSMaxStage80 As Global.System.Web.UI.WebControls.TextBox

        '''<summary>
        '''txtMSMaxStage95 control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents txtMSMaxStage95 As Global.System.Web.UI.WebControls.TextBox

        '''<summary>
        '''txtAddStage control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents txtAddStage As Global.System.Web.UI.WebControls.TextBox

        '''<summary>
        '''txtDeleteStage control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents txtDeleteStage As Global.System.Web.UI.WebControls.TextBox

        '''<summary>
        '''hdnConfigurationVal control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents hdnConfigurationVal As Global.System.Web.UI.HtmlControls.HtmlInputHidden

        '''<summary>
        '''mode control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents mode As Global.System.Web.UI.HtmlControls.HtmlInputHidden

        '''<summary>
        '''UpdateProgress control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents UpdateProgress As Global.System.Web.UI.UpdateProgress

        '''<summary>
        '''ddlBuildManager control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents ddlBuildManager As Global.System.Web.UI.WebControls.DropDownList

        '''<summary>
        '''chkTimerWindowOpens control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents chkTimerWindowOpens As Global.System.Web.UI.WebControls.CheckBox

        '''<summary>
        '''chkAssignStagetoTeams control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents chkAssignStagetoTeams As Global.System.Web.UI.WebControls.CheckBox

        '''<summary>
        '''divstage5 control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents divstage5 As Global.System.Web.UI.HtmlControls.HtmlGenericControl

        '''<summary>
        '''span5 control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents span5 As Global.System.Web.UI.HtmlControls.HtmlGenericControl

        '''<summary>
        '''btnAddStage5 control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents btnAddStage5 As Global.System.Web.UI.WebControls.Button

        '''<summary>
        '''btnDelStage5 control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents btnDelStage5 As Global.System.Web.UI.WebControls.Button

        '''<summary>
        '''tbl5 control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents tbl5 As Global.System.Web.UI.WebControls.Table

        '''<summary>
        '''divstage20 control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents divstage20 As Global.System.Web.UI.HtmlControls.HtmlGenericControl

        '''<summary>
        '''span20 control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents span20 As Global.System.Web.UI.HtmlControls.HtmlGenericControl

        '''<summary>
        '''btnAddStage20 control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents btnAddStage20 As Global.System.Web.UI.WebControls.Button

        '''<summary>
        '''btnDelStage20 control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents btnDelStage20 As Global.System.Web.UI.WebControls.Button

        '''<summary>
        '''tbl20 control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents tbl20 As Global.System.Web.UI.WebControls.Table

        '''<summary>
        '''divstage25 control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents divstage25 As Global.System.Web.UI.HtmlControls.HtmlGenericControl

        '''<summary>
        '''span25 control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents span25 As Global.System.Web.UI.HtmlControls.HtmlGenericControl

        '''<summary>
        '''btnAddStage25 control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents btnAddStage25 As Global.System.Web.UI.WebControls.Button

        '''<summary>
        '''btnDelStage25 control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents btnDelStage25 As Global.System.Web.UI.WebControls.Button

        '''<summary>
        '''tbl25 control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents tbl25 As Global.System.Web.UI.WebControls.Table

        '''<summary>
        '''divstage35 control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents divstage35 As Global.System.Web.UI.HtmlControls.HtmlGenericControl

        '''<summary>
        '''span35 control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents span35 As Global.System.Web.UI.HtmlControls.HtmlGenericControl

        '''<summary>
        '''btnAddStage35 control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents btnAddStage35 As Global.System.Web.UI.WebControls.Button

        '''<summary>
        '''btnDelstage35 control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents btnDelstage35 As Global.System.Web.UI.WebControls.Button

        '''<summary>
        '''tbl35 control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents tbl35 As Global.System.Web.UI.WebControls.Table

        '''<summary>
        '''divstage40 control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents divstage40 As Global.System.Web.UI.HtmlControls.HtmlGenericControl

        '''<summary>
        '''span40 control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents span40 As Global.System.Web.UI.HtmlControls.HtmlGenericControl

        '''<summary>
        '''btnAddStage40 control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents btnAddStage40 As Global.System.Web.UI.WebControls.Button

        '''<summary>
        '''btnDelStage40 control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents btnDelStage40 As Global.System.Web.UI.WebControls.Button

        '''<summary>
        '''tbl40 control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents tbl40 As Global.System.Web.UI.WebControls.Table

        '''<summary>
        '''divstage50 control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents divstage50 As Global.System.Web.UI.HtmlControls.HtmlGenericControl

        '''<summary>
        '''span50 control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents span50 As Global.System.Web.UI.HtmlControls.HtmlGenericControl

        '''<summary>
        '''btnAddStage50 control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents btnAddStage50 As Global.System.Web.UI.WebControls.Button

        '''<summary>
        '''btnDelStage50 control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents btnDelStage50 As Global.System.Web.UI.WebControls.Button

        '''<summary>
        '''tbl50 control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents tbl50 As Global.System.Web.UI.WebControls.Table

        '''<summary>
        '''divstage60 control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents divstage60 As Global.System.Web.UI.HtmlControls.HtmlGenericControl

        '''<summary>
        '''span60 control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents span60 As Global.System.Web.UI.HtmlControls.HtmlGenericControl

        '''<summary>
        '''Button1 control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents Button1 As Global.System.Web.UI.WebControls.Button

        '''<summary>
        '''Button2 control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents Button2 As Global.System.Web.UI.WebControls.Button

        '''<summary>
        '''tbl60 control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents tbl60 As Global.System.Web.UI.WebControls.Table

        '''<summary>
        '''divstage65 control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents divstage65 As Global.System.Web.UI.HtmlControls.HtmlGenericControl

        '''<summary>
        '''span65 control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents span65 As Global.System.Web.UI.HtmlControls.HtmlGenericControl

        '''<summary>
        '''btnAddStage65 control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents btnAddStage65 As Global.System.Web.UI.WebControls.Button

        '''<summary>
        '''btnDelStage65 control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents btnDelStage65 As Global.System.Web.UI.WebControls.Button

        '''<summary>
        '''tbl65 control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents tbl65 As Global.System.Web.UI.WebControls.Table

        '''<summary>
        '''divstage75 control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents divstage75 As Global.System.Web.UI.HtmlControls.HtmlGenericControl

        '''<summary>
        '''span75 control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents span75 As Global.System.Web.UI.HtmlControls.HtmlGenericControl

        '''<summary>
        '''btnAddStage75 control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents btnAddStage75 As Global.System.Web.UI.WebControls.Button

        '''<summary>
        '''btnDelStage75 control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents btnDelStage75 As Global.System.Web.UI.WebControls.Button

        '''<summary>
        '''tbl75 control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents tbl75 As Global.System.Web.UI.WebControls.Table

        '''<summary>
        '''divstage80 control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents divstage80 As Global.System.Web.UI.HtmlControls.HtmlGenericControl

        '''<summary>
        '''span80 control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents span80 As Global.System.Web.UI.HtmlControls.HtmlGenericControl

        '''<summary>
        '''btnAddStage80 control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents btnAddStage80 As Global.System.Web.UI.WebControls.Button

        '''<summary>
        '''btnDelStage80 control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents btnDelStage80 As Global.System.Web.UI.WebControls.Button

        '''<summary>
        '''tbl80 control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents tbl80 As Global.System.Web.UI.WebControls.Table

        '''<summary>
        '''divstage95 control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents divstage95 As Global.System.Web.UI.HtmlControls.HtmlGenericControl

        '''<summary>
        '''span95 control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents span95 As Global.System.Web.UI.HtmlControls.HtmlGenericControl

        '''<summary>
        '''btnAddStage95 control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents btnAddStage95 As Global.System.Web.UI.WebControls.Button

        '''<summary>
        '''btnDelStage95 control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents btnDelStage95 As Global.System.Web.UI.WebControls.Button

        '''<summary>
        '''tbl95 control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents tbl95 As Global.System.Web.UI.WebControls.Table

        '''<summary>
        '''btnSave control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents btnSave As Global.System.Web.UI.WebControls.LinkButton

        '''<summary>
        '''hdnProcessType control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents hdnProcessType As Global.System.Web.UI.WebControls.HiddenField
    End Class
End Namespace
