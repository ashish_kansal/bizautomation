'Modified By Anoop Jayaraj
Imports BACRM.BusinessLogic.Contacts
Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Marketing
Imports BACRM.BusinessLogic.Outlook
Imports BACRM.BusinessLogic.Projects
Imports BACRM.BusinessLogic.Opportunities
Imports BACRM.BusinessLogic.Case
Imports BACRM.BusinessLogic.Leads
Imports BACRM.BusinessLogic.Alerts
Imports BACRM.BusinessLogic.Workflow
Imports Telerik.Web.UI
Imports System.IO
Imports BACRM.BusinessLogic.Documents

Namespace BACRM.UserInterface.Admin
    Public Class actionitemdetails
        Inherits BACRMPage

        Dim lngCommId As Long
        Dim lngDivId As Long = 0
        Dim lngType As Long
        Dim lngCaseid As Long = 0
        Dim lngCaseTimeId As Long = 0
        Dim lngActivityId As Long = 0
        Dim bError As Boolean = False
        Dim objCampaign As Campaign
        Dim objContacts As CContacts
        Dim objActionItem As ActionItem
        Dim objOutLook As COutlook
        Dim dtTableInfo As DataTable
        Dim boolIntermediatoryPage As Boolean = False
        Dim objPageControls As New PageControls
        Dim objLeads As New LeadsIP
        Dim dsTemp As DataSet
        Dim m_aryRightsForActItem() As Integer
        Dim intRecord As Integer = 0
#Region "Page Events"
        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()

            Try
                CCommon.InitializeOrganizationClientSideTemplate(Session("DomainID"), Session("UserContactID"), radCmbCompany)
                CCommon.UpdateItemRadComboValues("1", radCmbCompany.SelectedValue)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Load
            Try
                'CLEAR ERROR ON RELOAD
                DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ""
                DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "none")

                hplEmpAvaliability.Attributes.Add("onclick", "return openEmpAvailability();")
                btnFollowup.Attributes.Add("onclick", "return OpenNew();")

                If Not IsPostBack Then
                    radDueDate.SelectedDate = DateTime.Now.Date
                    Correspondence1.bPaging = False
                    lngCommId = CCommon.ToDouble(GetQueryStringVal("CommId"))

                    hdnCommunicationID.Value = lngCommId
                    Dim strSelectedContactIdType As String = ""
                    strSelectedContactIdType = CCommon.ToString(GetQueryStringVal("selectedContactId"))
                    If (strSelectedContactIdType = "1") Then
                        If CCommon.ToString(Session("ContIDs")) <> "" Then
                            hdnContactID.Value = CCommon.ToString(Session("ContIDs"))
                            hdnDivisionID.Value = CCommon.ToString(Session("DivisionIDs"))
                            Dim contactCount As Long = hdnContactID.Value.Split(",").Count
                            multiContactText.InnerText = "" & CCommon.ToString(contactCount) & " Organizations & Contacts selected"
                            divOrganizations.Visible = False
                            multiContactText.Visible = True
                        End If
                    End If
                    lngType = CCommon.ToDouble(GetQueryStringVal("lngType"))
                    hdnlngType.Value = lngType
                    sb_FillEmpList()
                    objCommon.sb_FillComboFromDBwithSel(ddlFollowUpStatus, 30, Session("DomainID"))
                    objCommon.sb_FillComboFromDBwithSel(ddlStatus, 447, Session("DomainID"))
                    objCommon.sb_FillComboFromDBwithSel(ddlActivity, 32, Session("DomainID"))
                    objCommon.sb_FillComboFromDBwithSel(ddlType, 73, Session("DomainID"))
                    BindFollowUpStatus()
                    If ddlType.Items.FindByValue("974") IsNot Nothing Then
                        ddlType.Items.Remove(ddlType.Items.FindByValue("974"))
                    End If
                    BindActivityFormDetails()
                    'Edit
                    If lngCommId > 0 Then
                        lblTitle.Text = "Action Item Detail"
                        radCmbCompany.Visible = False
                        ddlContact.Visible = False
                        lnkCompany.Visible = True
                        lnkContact.Visible = True
                        If lngType = 0 Then
                            trRecordOwner.Visible = True
                        Else
                            trRecordOwner.Visible = False
                        End If
                        divTemplate.Visible = False
                        divAddActivity.Visible = False
                        AddToRecentlyViewed(RecetlyViewdRecordType.ActionItem, lngCommId)

                        BindDetail()
                    Else
                        lblTitle.Text = "New Action Item"
                        btnFollowup.Visible = False
                        btnSaveNext.Visible = False
                        btnSaveOnly.Visible = False
                        btnActDelete.Visible = False
                        btnLayout.Visible = False
                        trRecordOwner.Visible = False
                        divTemplate.Visible = True
                        lblorganisation.Visible = False
                        chkAM.Checked = True
                        chkEndAM.Checked = True
                        divAddActivity.Visible = True
                        BindActionItemTemplateData()

                        If GetQueryStringVal("uihTR") <> "" Or GetQueryStringVal("CntID") <> "" Or GetQueryStringVal("DivID") <> "" Or GetQueryStringVal("tyrCV") <> "" Or GetQueryStringVal("pluYR") <> "" Or GetQueryStringVal("fghTY") <> "" Then
                            If objCommon Is Nothing Then objCommon = New CCommon
                            objCommon.UserCntID = Session("UserContactID")
                            If GetQueryStringVal("uihTR") <> "" Then
                                objCommon.ContactID = CCommon.ToLong(GetQueryStringVal("uihTR"))
                                objCommon.charModule = "C"
                                hdnRedirectID.Value = objCommon.ContactID
                            ElseIf GetQueryStringVal("CntID") <> "" Then
                                objCommon.ContactID = CCommon.ToLong(GetQueryStringVal("CntID"))
                                objCommon.charModule = "C"
                                hdnRedirectID.Value = objCommon.ContactID
                            ElseIf GetQueryStringVal("DivID") <> "" Then
                                objCommon.DivisionID = CCommon.ToLong(GetQueryStringVal("DivID"))
                                objCommon.charModule = "D"
                                hdnRedirectID.Value = objCommon.DivisionID
                            ElseIf GetQueryStringVal("tyrCV") <> "" Then
                                objCommon.ProID = CCommon.ToLong(GetQueryStringVal("tyrCV"))
                                objCommon.charModule = "P"
                                hdnRedirectID.Value = objCommon.ProID
                            ElseIf GetQueryStringVal("pluYR") <> "" Then
                                objCommon.OppID = CCommon.ToLong(GetQueryStringVal("pluYR"))
                                objCommon.charModule = "O"
                                hdnRedirectID.Value = objCommon.OppID
                            ElseIf GetQueryStringVal("fghTY") <> "" Then
                                objCommon.CaseID = CCommon.ToLong(GetQueryStringVal("fghTY"))
                                objCommon.charModule = "S"
                                hdnRedirectID.Value = objCommon.CaseID
                            End If
                            objCommon.GetCompanySpecificValues1()
                            Dim strCompanyName As String
                            objContacts = New CContacts
                            objContacts.DivisionID = objCommon.DivisionID
                            strCompanyName = objContacts.GetCompanyName
                            radCmbCompany.Text = strCompanyName
                            radCmbCompany.SelectedValue = objCommon.DivisionID

                            If objCommon.DivisionID Then
                                Correspondence1.lngRecordID = objCommon.DivisionID

                                Correspondence1.getCorrespondance()
                            End If

                            LoadCompanyDetail()
                            LoadContactDetail()

                            If objCommon.ContactID > 0 AndAlso Not ddlContact.Items.FindByValue(objCommon.ContactID) Is Nothing Then
                                ddlContact.Items.FindByValue(objCommon.ContactID).Selected = True

                                Dim objLeads As New BACRM.BusinessLogic.Leads.LeadsIP
                                objLeads.ContactID = objCommon.ContactID
                                objLeads.DomainID = Session("DomainID")
                                objLeads.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
                                objLeads.GetLeadDetails()

                                imbBtnEmail.AlternateText = objLeads.Email
                                imbBtnEmail.ToolTip = objLeads.Email
                                imbBtnEmail.Attributes.Add("onclick", "return fn_Mail('" & objLeads.Email & "','" & objLeads.ContactID & "')")
                                lblPhone.Text = objLeads.ContactPhone
                            End If
                        End If

                    End If

                    Session("AttendeeTable") = Nothing

                    If GetQueryStringVal("Popup") = "True" Then
                        Page.Master.FindControl("webmenu1").Visible = False
                        btnCancel.Attributes.Add("onclick", "return Close()")
                        objActionItem = New ActionItem
                        objActionItem.CommID = lngCommId
                        objActionItem.ChangeSnoozeStatus()
                    End If

                    PersistTable.Load()
                    If PersistTable.Count > 0 Then
                        If CCommon.ToBool(CCommon.ToInteger(PersistTable("CustomFields"))) Then
                            hdnCustomFieldExpandCollapse.Value = "1"
                        Else
                            hdnCustomFieldExpandCollapse.Value = "0"
                        End If

                        If CCommon.ToBool(CCommon.ToInteger(PersistTable("AttendeesFields"))) Then
                            hdnAttendeesExpandCollapse.Value = "1"
                        Else
                            hdnAttendeesExpandCollapse.Value = "0"
                        End If
                    End If
                End If

                LoadControls()

                If ddlType.SelectedItem.Value = "973" Then
                    ddlUserNames.Enabled = False
                    chkClose.Enabled = False
                ElseIf ddlType.SelectedItem.Value = "974" Then
                    ddlUserNames.Enabled = True
                Else
                    ddlUserNames.Enabled = True
                    chkClose.Enabled = True
                End If

                btnSave.Attributes.Add("onclick", "return Save()")
                btnSaveNext.Attributes.Add("onclick", "return Save()")
                btnSaveOnly.Attributes.Add("onclick", "return Save()")
                ddDueDate.Attributes.Add("onchange", "return duedateChage('" & CCommon.GetValidationDateFormat() & "');")

                hfDateFormat.Value = Session("DateFormat").ToString().ToLower().Replace("m", "M").Replace("Month", "MMM")
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Sub BindFollowUpStatus()
            Try
                ddlUpdateFollowUpStatus.Items.Clear()
                Dim dt As New DataTable()
                objCommon.DomainID = CLng(HttpContext.Current.Session("DomainID"))
                objCommon.ListID = CCommon.ToLong(30)
                dt = objCommon.GetMasterListItemsWithRights()
                Dim item As ListItem = New ListItem("-- All --", "0")
                ddlUpdateFollowUpStatus.Items.Add(item)
                ddlUpdateFollowUpStatus.AutoPostBack = False
                For Each dr As DataRow In dt.Rows
                    item = New ListItem(CCommon.ToString(dr("vcData")), CCommon.ToString(dr("numListItemID")))
                    item.Attributes("OptionGroup") = CCommon.ToString(dr("vcListItemGroupName"))
                    item.Attributes("class") = CCommon.ToString(dr("vcColorScheme"))
                    ddlUpdateFollowUpStatus.Items.Add(item)
                Next
            Catch ex As Exception

            End Try
        End Sub
        Sub BindActivityFormDetails()
            Try
                Dim objConfigWizard As New FormConfigWizard
                objConfigWizard.DomainID = Session("DomainID")
                objConfigWizard.AuthenticationGroupID = Session("UserGroupID")
                Dim lngType As Long = CCommon.ToLong(hdnlngType.Value)
                Dim ds As New DataSet
                ds = objConfigWizard.getActivityConfiguration()
                If (ds.Tables(0).Rows.Count > 0) Then

                    If (lngType = 1) Then
                        btnFollowup.Visible = False
                        btnActDelete.Visible = False
                        chkFollowUp.Visible = False
                        lblFollowUp.Visible = False
                        ddDueDate.Visible = False
                        divReminder.Visible = False
                        radDueDate.Enabled = False
                        divCustomFields.Visible = False
                        btnLayout.Visible = False
                    Else
                        If Convert.ToBoolean(ds.Tables(0).Rows(0)("bitFollowupAnytime")) = True Then
                            If lngCommId <= 0 Then
                                chkFollowUp.Checked = True
                            End If
                        Else
                            If lngCommId <= 0 Then
                                chkFollowUp.Checked = False
                            End If
                        End If

                        If Convert.ToBoolean(ds.Tables(0).Rows(0)("bitCustomField")) = True Then
                            divCustomFields.Visible = True
                            btnLayout.Visible = True
                        Else
                            divCustomFields.Visible = False
                            btnLayout.Visible = False
                        End If
                        ddDueDate.Visible = True
                        divReminder.Visible = True
                        radDueDate.Enabled = True
                        radcmbEndTime.Enabled = True
                        radcmbStartTime.Enabled = True
                        btnActDelete.Visible = True
                        btnFollowup.Visible = True
                    End If
                    If Convert.ToBoolean(ds.Tables(0).Rows(0)("bitFollowupStatus")) = True Then
                        divFollowupStatus.Visible = True
                    Else
                        divFollowupStatus.Visible = False
                    End If
                    If Convert.ToBoolean(ds.Tables(0).Rows(0)("bitPriority")) = True Then
                        divPriority.Visible = True
                    Else
                        divPriority.Visible = False
                    End If
                    If Convert.ToBoolean(ds.Tables(0).Rows(0)("bitActivity")) = True Then
                        divActivity.Visible = True
                    Else
                        divActivity.Visible = False
                    End If

                Else
                    divFollowupStatus.Visible = True
                    divPriority.Visible = True
                    divActivity.Visible = True
                    If (lngType = 1) Then
                        chkFollowUp.Visible = False
                        lblFollowUp.Visible = False
                        ddDueDate.Visible = False
                        divReminder.Visible = False
                        divCustomFields.Visible = False
                        btnLayout.Visible = False
                        radDueDate.Enabled = False
                        btnActDelete.Visible = False
                        btnFollowup.Visible = False
                    Else
                        If lngCommId <= 0 Then
                            chkFollowUp.Checked = False
                        Else
                            chkFollowUp.Checked = True
                        End If
                        ddDueDate.Visible = True
                        divReminder.Visible = True
                        divCustomFields.Visible = True
                        btnLayout.Visible = True
                        radDueDate.Enabled = True
                        btnActDelete.Visible = True
                        btnFollowup.Visible = True
                    End If
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
#End Region

#Region "Private Methods"
        Private Sub Save()
            Try
                If hdnlngType.Value = 1 Then
                    UpdateActivityInfo()
                Else
                    If CCommon.ToLong(hdnCommunicationID.Value) = 0 Then
                        InsertCommunication()
                    Else
                        UpdateCommunication()
                    End If
                End If

            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub UpdateActivityInfo()
            If objOutLook Is Nothing Then
                objOutLook = New COutlook
            End If
            objOutLook.Priority = ddlStatus.SelectedValue
            objOutLook.Activity = ddlActivity.SelectedValue
            objOutLook.FollowUpStatus = ddlFollowUpStatus.SelectedValue
            objOutLook.Comments = txtcomments.Text.ToString()
            objOutLook.ActivityID = CInt(txtActivityID.Text)
            objOutLook.ContactID = ViewState("numContactId")
            objOutLook.UpdateActivityFromBiz()


            Dim objAcc As New BACRM.BusinessLogic.Account.CAccounts
            objAcc.ContactID = ViewState("numContactId")
            objAcc.FollowUpStatus = ddlFollowUpStatus.SelectedValue
            objAcc.UpdateFollowUpStatus()
        End Sub
        Private Sub GetDocumentFilesLink()
            Try
                tdDocument.Controls.Clear()
                Dim objOpp As New BusinessLogic.Opportunities.OppotunitiesIP
                Dim dtDocLinkName As New DataTable
                With objOpp
                    .DomainID = Session("DomainID")
                    .DivisionID = lngDivId
                    dtDocLinkName = .GetDocumentFilesLink("P")
                End With

                If dtDocLinkName IsNot Nothing AndAlso dtDocLinkName.Rows.Count > 0 Then

                    Dim docRows = (From dRow In dtDocLinkName.Rows
                                   Where CCommon.ToString(dRow("cUrlType")) = "L"
                                   Select New With {Key .fileName = dRow("VcFileName"), .docName = dRow("VcDocName"), .extention = dRow("vcFileType"), .fileId = dRow("numGenericDocID")}).Distinct()
                    If docRows.Count() > 0 Then
                        Dim _docName = String.Empty
                        Dim countId = 0
                        For Each dr In docRows
                            Dim strOriginalFileName = CCommon.ToString(dr.docName) & CCommon.ToString(dr.extention)
                            Dim strDocName = CCommon.ToString(dr.fileName)
                            Dim fileID = CCommon.ToString(dr.fileId)
                            Dim strFileLogicalPath = String.Empty
                            If File.Exists(CCommon.GetDocumentPhysicalPath(Session("DomainID")) & strDocName) Then
                                Dim f As FileInfo = New FileInfo(CCommon.GetDocumentPhysicalPath(Session("DomainID")) & strDocName)
                                Dim strFileSize As String = f.Length.ToString
                                strFileLogicalPath = CCommon.GetDocumentPath(Session("DomainID")) & HttpUtility.UrlEncode(strDocName).Replace("+", " ")
                            End If

                            Dim docLink As New HyperLink

                            Dim imgDelete As New ImageButton
                            imgDelete.ImageUrl = "../images/Delete24.png"
                            imgDelete.Height = 13
                            imgDelete.ToolTip = "Delete"
                            imgDelete.ID = "imgDelete" + CCommon.ToString(countId)
                            imgDelete.Attributes.Add("onclick", "DeleteDocumentOrLink(" & fileID & ");return false;")

                            Dim Space As New LiteralControl
                            Space.Text = "&nbsp;"

                            docLink.Text = strOriginalFileName
                            docLink.NavigateUrl = strFileLogicalPath
                            docLink.ID = "hpl" + CCommon.ToString(countId)
                            docLink.Target = "_blank"
                            Dim createDocDiv As HtmlGenericControl = New HtmlGenericControl("DIV")
                            createDocDiv.Controls.Add(docLink)
                            createDocDiv.Controls.Add(Space)
                            createDocDiv.Controls.Add(imgDelete)
                            tdDocument.Controls.Add(createDocDiv)
                            countId = countId + 1
                        Next
                        imgDocument.Visible = True
                    Else
                        imgDocument.Visible = False
                    End If

                    Dim linkRows = (From dRow In dtDocLinkName.Rows
                                    Where CCommon.ToString(dRow("cUrlType")) = "U"
                                    Select New With {Key .linkURL = dRow("VcFileName"), Key .linkName = dRow("VcDocName"), .fileId = dRow("numGenericDocID")}).Distinct()
                    If linkRows.Count() > 0 Then
                        Dim _linkName = String.Empty
                        Dim _linkURL = String.Empty
                        Dim fileLinkID = String.Empty
                        Dim countLinkId = 0

                        For Each dr In linkRows
                            '_linkName = _linkName + ", " + CCommon.ToString(dr.linkName)
                            '_linkURL = _linkURL + ",<br/> " + CCommon.ToString(dr.linkURL)

                            _linkName = CCommon.ToString(dr.linkName)
                            _linkURL = CCommon.ToString(dr.linkURL)
                            fileLinkID = CCommon.ToString(dr.fileId)
                            Dim uriBuilder As UriBuilder = New UriBuilder(_linkURL)
                            'UriBuilder builder = New UriBuilder(myUrl)
                            'charityNameText.NavigateUrl = builder.Uri.AbsoluteUri

                            Dim link As New HyperLink
                            Dim imgLinkDelete As New ImageButton
                            imgLinkDelete.ImageUrl = "../images/Delete24.png"
                            imgLinkDelete.Height = 13
                            imgLinkDelete.ToolTip = "Delete"
                            imgLinkDelete.ID = "imgLinkDelete" + CCommon.ToString(countLinkId)
                            imgLinkDelete.Attributes.Add("onclick", "DeleteDocumentOrLink(" & fileLinkID & ");return false;")

                            link.Text = _linkName
                            link.NavigateUrl = uriBuilder.Uri.AbsoluteUri
                            'link.NavigateUrl = _linkURL
                            link.ID = "hplLink" + CCommon.ToString(countLinkId)
                            link.Target = "_blank"
                            Dim createLinkDiv As HtmlGenericControl = New HtmlGenericControl("DIV")
                            createLinkDiv.Controls.Add(link)
                            createLinkDiv.Controls.Add(imgLinkDelete)
                            tdLink.Controls.Add(createLinkDiv)
                            countLinkId = countLinkId + 1
                        Next
                        imgLink.Visible = True
                    Else
                        imgLink.Visible = False
                    End If
                Else
                    imgDocument.Visible = False
                    imgLink.Visible = False
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Protected Sub btnLinkClose_Click(sender As Object, e As EventArgs)
            UploadFile("U", "", txtLinkURL.Text, txtLinkName.Text, "", 0, 0, "A")
            Response.Redirect(Request.RawUrl, False)
        End Sub
        'Modified by Neelam on 02/16/2018 - Added function to save File/Link data in the database*/
        Sub UploadFile(ByVal URLType As String, ByVal strFileType As String, ByVal strFileName As String, ByVal DocName As String, ByVal DocDesc As String,
                       ByVal DocumentStatus As Long, DocCategory As Long, ByVal strType As String)
            Try
                Dim arrOutPut As String()
                Dim objDocuments As New DocumentList()
                With objDocuments
                    .DomainID = Session("DomainId")
                    .UserCntID = Session("UserContactID")
                    .UrlType = URLType
                    .DocumentStatus = DocumentStatus
                    .DocCategory = DocCategory
                    .FileType = strFileType
                    .DocName = DocName
                    .DocDesc = DocDesc
                    .FileName = strFileName
                    .DocumentSection = strType
                    .RecID = lngDivId
                    .DocumentType = IIf(hdnContactID.Value > 0, 2, 1) '1=generic,2=specific
                    arrOutPut = .SaveDocuments()
                End With
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        Protected Sub btnDeleteDocOrLink_Click(sender As Object, e As EventArgs)
            Dim objDocuments As New DocumentList()
            objDocuments.GenDocID = CCommon.ToLong(hdnFileId.Value)
            objDocuments.DeleteDocumentOrLink()
            GetDocumentFilesLink()
        End Sub
        'Modified by Neelam on 02/16/2018 - Added functionality to attach files*/
        Protected Sub btnAttach_Click(sender As Object, e As EventArgs)
            Try
                If (fileupload.PostedFile.ContentLength > 0) Then
                    Dim strFName As String()
                    Dim strFilePath, strFileName, strFileType As String
                    strFileName = Path.GetFileName(fileupload.PostedFile.FileName)
                    If Directory.Exists(CCommon.GetDocumentPhysicalPath(Session("DomainID"))) = False Then ' If Folder Does not exists create New Folder.
                        Directory.CreateDirectory(CCommon.GetDocumentPhysicalPath(Session("DomainID")))
                    End If
                    Dim newFileName As String = Path.GetFileNameWithoutExtension(strFileName) & DateTime.UtcNow.ToString("yyyyMMddhhmmssfff") & Path.GetExtension(strFileName)
                    strFilePath = CCommon.GetDocumentPhysicalPath(Session("DomainID")) & newFileName
                    strFName = Split(strFileName, ".")
                    strFileType = "." & strFName(strFName.Length - 1)                     'Getting the Extension of the File
                    fileupload.PostedFile.SaveAs(strFilePath)
                    UploadFile("L", strFileType, newFileName, strFName(0), "", 0, 0, "A")
                    Dim script As String = "function f(){$find(""" + RadWinDocumentFiles.ClientID + """).hide(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);"
                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "key", script, True)
                    Response.Redirect(Request.RawUrl, False)
                Else
                    ClientScript.RegisterClientScriptBlock(Me.GetType, "NoFiles", "alert('Please attach a file to upload.');return false;", True)
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub
        Private Sub BindDetail()
            Try
                Dim dtActDetails As DataTable
                If objActionItem Is Nothing Then objActionItem = New ActionItem
                objActionItem.CommID = CCommon.ToLong(hdnCommunicationID.Value)
                objActionItem.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
                Dim lngType As Long = CCommon.ToLong(hdnlngType.Value)

                If (lngType = 0) Then
                    dtActDetails = objActionItem.GetActionItemDetails

                    If dtActDetails.Rows.Count > 0 Then
                        If objCommon Is Nothing Then objCommon = New CCommon
                        objCommon.CommID = CCommon.ToLong(hdnCommunicationID.Value)
                        objCommon.charModule = "A"
                        objCommon.PhoneExt = ""
                        objCommon.CustomerRelation = ""
                        objCommon.GetCompanySpecificValues1()
                        hdnDivisionID.Value = objCommon.DivisionID

                        lnkCompany.Text = "<b>" & objCommon.CompanyName & "</b>"
                        lnkContact.Text = objCommon.ContactName
                        lblContactPhone.Text = objCommon.Phone
                        If objCommon.PhoneExt <> "" Then
                            lblContactPhone.Text = lblContactPhone.Text & "(" & objCommon.PhoneExt & ")"
                        End If
                        If objCommon.CustomerRelation <> "" Then
                            lblRelationCustomerType.Text = "(" & objCommon.CustomerRelation & ")"
                        End If
                        btnSendEmail.Visible = True
                        btnSendEmail.Attributes.Add("onclick", "return fn_Mail('" & objCommon.EmailID & "','" & objCommon.ContactID & "')")

                        imbBtnEmail.AlternateText = objCommon.EmailID
                        imbBtnEmail.ToolTip = objCommon.EmailID
                        imbBtnEmail.Attributes.Add("onclick", "return fn_Mail('" & objCommon.EmailID & "','" & objCommon.ContactID & "')")
                        lblPhone.Text = objCommon.Phone

                        hdnContactID.Value = dtActDetails.Rows(0).Item("numContactId")
                        divImageUploadArea.Visible = True
                        imgOpenDocument.Attributes.Add("onclick", "OpenDocumentFiles(" & hdnContactID.Value.ToString() & ");return false;")
                        hplOpenDocument.Attributes.Add("onclick", "OpenDocumentFiles(" & hdnContactID.Value.ToString() & ");return false;")
                        If Not ddlType.Items.FindByValue(dtActDetails.Rows(0).Item("bitTask")) Is Nothing Then
                            ddlType.Items.FindByValue(dtActDetails.Rows(0).Item("bitTask")).Selected = True
                        End If

                        If Not IsDBNull(dtActDetails.Rows(0).Item("bitFollowUpAnyTime")) Then
                            If (dtActDetails.Rows(0).Item("bitFollowUpAnyTime") = True) Then
                                chkFollowUp.Checked = True
                            Else
                                chkFollowUp.Checked = False
                            End If
                        End If


                        txtcomments.Text = dtActDetails.Rows(0).Item("textdetails")
                        tdComments.Text = dtActDetails.Rows(0).Item("textdetails")
                        radDueDate.SelectedDate = dtActDetails.Rows(0).Item("dtStartTime")
                        txtActivityID.Text = dtActDetails.Rows(0).Item("numActivityId")
                        'If it has been created from the Tickler section then the assign to will be 0, hence dont assign it
                        If dtActDetails.Rows(0).Item("numAssign") <> 0 Then
                            ddlUserNames.ClearSelection()
                            Try
                                ddlUserNames.Items.FindByValue(dtActDetails.Rows(0).Item("numAssign")).Selected = True
                                tdAssignTo.Text = ddlUserNames.SelectedItem.Text
                            Catch ex As Exception
                            End Try
                        End If
                        ViewState("AssignedTo") = ddlUserNames.SelectedValue

                        lblCreatedBy.Text = dtActDetails.Rows(0).Item("CreatedBy")
                        lblRecordOwner.Text = dtActDetails.Rows(0).Item("RecOwner")
                        lblModifiedBy.Text = dtActDetails.Rows(0).Item("ModifiedBy")
                        If Not ddlStatus.Items.FindByValue(dtActDetails.Rows(0).Item("numStatus")) Is Nothing Then
                            ddlStatus.Items.FindByValue(dtActDetails.Rows(0).Item("numStatus")).Selected = True
                        End If

                        Correspondence1.lngRecordID = dtActDetails.Rows(0).Item("numDivisionId")
                        Correspondence1.Mode = 7
                        lngDivId = dtActDetails.Rows(0).Item("numDivisionId")
                        GetDocumentFilesLink()
                        Correspondence1.bPaging = False
                        Correspondence1.getCorrespondance()

                        If Not ddlActivity.Items.FindByValue(dtActDetails.Rows(0).Item("numActivity")) Is Nothing Then
                            ddlActivity.Items.FindByValue(dtActDetails.Rows(0).Item("numActivity")).Selected = True
                        End If

                        If Not ddlMinutes.Items.FindByValue(dtActDetails.Rows(0).Item("intRemainderMins")) Is Nothing Then
                            ddlMinutes.Items.FindByValue(dtActDetails.Rows(0).Item("intRemainderMins")).Selected = True
                        End If

                        If Not IsDBNull(dtActDetails.Rows(0).Item("bitOutlook")) Then
                            If (dtActDetails.Rows(0).Item("bitOutlook") = True) Then
                                chkOutlook.Checked = True
                            Else : chkOutlook.Checked = False
                            End If
                        End If

                        If dtActDetails.Rows(0).Item("tintRemStatus") = 0 Then
                            chkRemindMe.Checked = False
                        Else : chkRemindMe.Checked = True
                        End If

                        If dtActDetails.Rows(0).Item("bitClosedflag") = 0 Then
                            chkClose.Checked = False
                        Else : chkClose.Checked = True
                        End If
                        If dtActDetails.Rows(0).Item("bitTask") <> 973 Then
                            radcmbStartTime.SelectedItem.Selected = False
                            If Not radcmbStartTime.Items.FindItemByText(Format(dtActDetails.Rows(0).Item("dtStartTime"), "h:mm")) Is Nothing Then
                                radcmbStartTime.Items.FindItemByText(Format(dtActDetails.Rows(0).Item("dtStartTime"), "h:mm")).Selected = True
                            End If
                            radcmbEndTime.SelectedItem.Selected = False
                            If Not radcmbEndTime.Items.FindItemByText(Format(dtActDetails.Rows(0).Item("dtEndTime"), "h:mm")) Is Nothing Then
                                radcmbEndTime.Items.FindItemByText(Format(dtActDetails.Rows(0).Item("dtEndTime"), "h:mm")).Selected = True
                            End If
                            If Format(dtActDetails.Rows(0).Item("dtStartTime"), "tt") = "AM" Then
                                chkAM.Checked = True
                            Else : chkPM.Checked = True
                            End If
                            If Format(dtActDetails.Rows(0).Item("dtEndTime"), "tt") = "AM" Then
                                chkEndAM.Checked = True
                            Else : chkEndPM.Checked = True
                            End If
                        End If
                        txtFromToTime.InnerText = radcmbStartTime.SelectedItem.Text & Format(dtActDetails.Rows(0).Item("dtStartTime"), "tt") & " - " & radcmbEndTime.SelectedItem.Text & Format(dtActDetails.Rows(0).Item("dtEndTime"), "tt")
                        If dtActDetails.Rows(0).Item("bitTask") = 973 Then
                            ddlUserNames.Enabled = False
                            radcmbStartTime.Enabled = False
                            chkAM.Disabled = True
                            chkPM.Disabled = True
                            radcmbEndTime.Enabled = False
                            chkEndAM.Disabled = True
                            chkEndPM.Disabled = True
                            chkClose.Enabled = False
                        Else
                            ddlUserNames.Enabled = True
                            radcmbStartTime.Enabled = True
                            chkAM.Disabled = False
                            chkPM.Disabled = False
                            radcmbEndTime.Enabled = True
                            chkEndAM.Disabled = False
                            chkEndPM.Disabled = False
                            chkClose.Enabled = True
                        End If


                        hdnCorrespondenceID.Value = dtActDetails.Rows(0).Item("numCorrespondenceID")


                        If Not ddlFollowUpStatus.Items.FindByValue(dtActDetails.Rows(0).Item("numFollowUpStatus")) Is Nothing Then
                            ddlFollowUpStatus.Items.FindByValue(dtActDetails.Rows(0).Item("numFollowUpStatus")).Selected = True
                        End If

                        CCommon.UpdateItemRadComboValues("1", dtActDetails.Rows(0).Item("numDivisionId"))

                        If CCommon.ToLong(dtActDetails.Rows(0).Item("numLinkedOrganization")) > 0 Then
                            LoadLinkedOrganizationDetail(CCommon.ToLong(dtActDetails.Rows(0).Item("numLinkedOrganization")),
                                                         CCommon.ToLong(dtActDetails.Rows(0).Item("numLinkedContact")))
                        End If

                        Dim lngTerrID As Long = CCommon.ToLong(dtActDetails.Rows(0).Item("numOrgTerId"))
                        Dim lngRecOwnID As Long = CCommon.ToLong(dtActDetails.Rows(0).Item("numCreatedBy"))
                        m_aryRightsForActItem = GetUserRightsForPage_Other(1, 2)
                        If m_aryRightsForActItem(RIGHTSTYPE.DELETE) = 0 Then
                            btnActDelete.Visible = False
                        ElseIf m_aryRightsForActItem(RIGHTSTYPE.DELETE) = 1 Then
                            Try
                                If lngRecOwnID <> Session("UserContactID") Then
                                    btnActDelete.Visible = False
                                End If
                            Catch ex As Exception
                            End Try

                        ElseIf m_aryRightsForActItem(RIGHTSTYPE.DELETE) = 2 Then
                            Try
                                Dim i As Integer
                                Dim dtTerritory As DataTable
                                dtTerritory = Session("UserTerritory")
                                If lngTerrID = 0 Then
                                    btnActDelete.Visible = True
                                Else
                                    Dim chkDelete As Boolean = False
                                    For i = 0 To dtTerritory.Rows.Count - 1
                                        If lngTerrID = dtTerritory.Rows(i).Item("numTerritoryId") Then chkDelete = True
                                    Next
                                    If chkDelete = False Then
                                        btnActDelete.Visible = False
                                    End If
                                End If
                            Catch ex As Exception
                            End Try
                        End If

                        If m_aryRightsForActItem(RIGHTSTYPE.UPDATE) = 0 Then
                            btnSave.Visible = False
                            btnSaveNext.Visible = False
                            btnSaveOnly.Visible = False
                            btnFollowup.Visible = False
                        ElseIf m_aryRightsForActItem(RIGHTSTYPE.UPDATE) = 1 Then
                            Try
                                If lngRecOwnID <> Session("UserContactID") Then
                                    btnSave.Visible = False
                                    btnSaveNext.Visible = False
                                    btnSaveOnly.Visible = False
                                    btnFollowup.Visible = False
                                End If
                            Catch ex As Exception
                            End Try

                        ElseIf m_aryRightsForActItem(RIGHTSTYPE.UPDATE) = 2 Then
                            Try
                                Dim i As Integer
                                Dim dtTerritory As DataTable
                                dtTerritory = Session("UserTerritory")
                                If lngTerrID = 0 Then
                                    btnSave.Visible = True
                                    btnSaveNext.Visible = True
                                    btnSaveOnly.Visible = True
                                    btnFollowup.Visible = True
                                Else
                                    Dim chkDelete As Boolean = False
                                    For i = 0 To dtTerritory.Rows.Count - 1
                                        If lngTerrID = dtTerritory.Rows(i).Item("numTerritoryId") Then chkDelete = True
                                    Next
                                    If chkDelete = False Then
                                        btnSave.Visible = False
                                        btnSaveNext.Visible = False
                                        btnSaveOnly.Visible = False
                                        btnFollowup.Visible = False
                                    End If
                                End If
                            Catch ex As Exception
                            End Try
                        End If
                    End If
                Else
                    If (lngType = 1) Then
                        dtActDetails = objActionItem.GetActivityDetails
                        If dtActDetails.Rows.Count > 0 Then
                            hdnDivisionID.Value = dtActDetails.Rows(0).Item("numDivisionId")
                            hdnContactID.Value = dtActDetails.Rows(0).Item("numContactId")

                            lnkCompany.Text = "<b>" & IIf(IsDBNull(dtActDetails.Rows(0).Item("vcCompanyName")), "", dtActDetails.Rows(0).Item("vcCompanyName")) & "</b> (" & IIf(IsDBNull(dtActDetails.Rows(0).Item("ContactName")), "", dtActDetails.Rows(0).Item("ContactName")) & ")"
                            lnkContact.Text = IIf(IsDBNull(dtActDetails.Rows(0).Item("ContactName")), "", dtActDetails.Rows(0).Item("ContactName"))



                            imbBtnEmail.AlternateText = IIf(IsDBNull(dtActDetails.Rows(0).Item("vcEmail")), "", dtActDetails.Rows(0).Item("vcEmail"))
                            imbBtnEmail.ToolTip = IIf(IsDBNull(dtActDetails.Rows(0).Item("vcEmail")), "", dtActDetails.Rows(0).Item("vcEmail"))
                            imbBtnEmail.Attributes.Add("onclick", "return fn_Mail('" & IIf(IsDBNull(dtActDetails.Rows(0).Item("vcEmail")), "", dtActDetails.Rows(0).Item("vcEmail")) & "','" & IIf(IsDBNull(dtActDetails.Rows(0).Item("numContactId")), "", dtActDetails.Rows(0).Item("numContactId")) & "')")
                            lblPhone.Text = IIf(IsDBNull(dtActDetails.Rows(0).Item("numPhone")), "", dtActDetails.Rows(0).Item("numPhone"))
                            ViewState("numContactId") = dtActDetails.Rows(0).Item("numContactId")

                            If Not ddlType.Items.FindByValue(dtActDetails.Rows(0).Item("bitTask")) Is Nothing Then
                                ddlType.Items.FindByValue(dtActDetails.Rows(0).Item("bitTask")).Selected = True
                            End If

                            txtcomments.Text = String.Empty
                            radDueDate.SelectedDate = IIf(IsDBNull(dtActDetails.Rows(0).Item("dtStartTime")), "", dtActDetails.Rows(0).Item("dtStartTime"))
                            txtActivityID.Text = IIf(IsDBNull(dtActDetails.Rows(0).Item("numActivityId")), "", dtActDetails.Rows(0).Item("numActivityId"))


                            ViewState("AssignedTo") = ddlUserNames.SelectedValue

                            If Not ddlStatus.Items.FindByValue(dtActDetails.Rows(0).Item("Priority")) Is Nothing Then
                                ddlStatus.Items.FindByValue(dtActDetails.Rows(0).Item("Priority")).Selected = True
                                tdPriority.Text = ddlStatus.SelectedItem.Text
                            End If



                            Correspondence1.lngRecordID = IIf(IsDBNull(dtActDetails.Rows(0).Item("numDivisionId")), 0, dtActDetails.Rows(0).Item("numDivisionId"))
                            Correspondence1.Mode = 7
                            Correspondence1.bPaging = False
                            Correspondence1.getCorrespondance()

                            If Not ddlActivity.Items.FindByValue(dtActDetails.Rows(0).Item("Activity")) Is Nothing Then
                                ddlActivity.Items.FindByValue(dtActDetails.Rows(0).Item("Activity")).Selected = True
                                tdActivity.Text = ddlActivity.SelectedItem.Text
                            End If

                            If Not ddlFollowUpStatus.Items.FindByValue(dtActDetails.Rows(0).Item("FollowUpStatus")) Is Nothing Then
                                ddlFollowUpStatus.Items.FindByValue(dtActDetails.Rows(0).Item("FollowUpStatus")).Selected = True
                            End If

                            If Not dtActDetails.Rows(0).Item("Comments") Is Nothing Then
                                txtcomments.Text = IIf(IsDBNull(dtActDetails.Rows(0).Item("Comments")), "", dtActDetails.Rows(0).Item("Comments"))
                            End If

                            If dtActDetails.Rows(0).Item("bitTask") <> 973 Then
                                radcmbStartTime.SelectedItem.Selected = False
                                If Not radcmbStartTime.Items.FindItemByText(Format(dtActDetails.Rows(0).Item("dtStartTime"), "h:mm")) Is Nothing Then
                                    radcmbStartTime.Items.FindItemByText(Format(dtActDetails.Rows(0).Item("dtStartTime"), "h:mm")).Selected = True
                                End If
                                radcmbEndTime.SelectedItem.Selected = False
                                If Not radcmbEndTime.Items.FindItemByText(Format(dtActDetails.Rows(0).Item("dtEndTime"), "h:mm")) Is Nothing Then
                                    radcmbEndTime.Items.FindItemByText(Format(dtActDetails.Rows(0).Item("dtEndTime"), "h:mm")).Selected = True
                                End If
                                If Format(dtActDetails.Rows(0).Item("dtStartTime"), "tt") = "AM" Then
                                    chkAM.Checked = True
                                Else : chkPM.Checked = True
                                End If
                                If Format(dtActDetails.Rows(0).Item("dtEndTime"), "tt") = "AM" Then
                                    chkEndAM.Checked = True
                                Else : chkEndPM.Checked = True
                                End If
                            End If

                            If dtActDetails.Rows(0).Item("bitTask") = 973 Then
                                DivAssignTo.Visible = True
                                ddlUserNames.Enabled = False
                                radcmbStartTime.Enabled = False
                                chkAM.Disabled = True
                                chkPM.Disabled = True
                                radcmbEndTime.Enabled = False
                                chkEndAM.Disabled = True
                                chkEndPM.Disabled = True
                                chkClose.Enabled = False
                            ElseIf dtActDetails.Rows(0).Item("bitTask") = 982 Then
                                DivAssignTo.Visible = False
                                radcmbStartTime.Enabled = False
                                chkAM.Disabled = True
                                chkPM.Disabled = True
                                radcmbEndTime.Enabled = False
                                chkEndAM.Disabled = True
                                chkEndPM.Disabled = True
                                chkClose.Enabled = False

                            Else
                                DivAssignTo.Visible = True
                                ddlUserNames.Enabled = True
                                radcmbStartTime.Enabled = True
                                chkAM.Disabled = False
                                chkPM.Disabled = False
                                radcmbEndTime.Enabled = True
                                chkEndAM.Disabled = False
                                chkEndPM.Disabled = False
                                chkClose.Enabled = True
                            End If


                            CCommon.UpdateItemRadComboValues("1", IIf(IsDBNull(dtActDetails.Rows(0).Item("numDivisionId")), "", dtActDetails.Rows(0).Item("numDivisionId")))

                        End If
                    End If
                End If
            Catch Ex As Exception
                Throw Ex
            End Try
        End Sub
        Private Sub LoadControls()
            Try
                plhCustomFields.Controls.Clear()
                If CCommon.ToBool(Session("bitDisplayCustomField")) = True Then
                    Dim ds As DataSet
                    Dim objPageLayout As New CPageLayout

                    Dim fields() As String

                    objLeads.DomainID = Session("DomainID")
                    objLeads.UserCntID = Session("UserContactId")
                    objLeads.DivisionID = CCommon.ToLong(hdnDivisionID.Value)
                    objLeads.GetCompanyDetails()

                    objPageLayout.CoType = "T"
                    objPageLayout.UserCntID = Session("UserContactId")
                    objPageLayout.RecordId = CCommon.ToLong(hdnDivisionID.Value)
                    objPageLayout.DomainID = Session("DomainID")
                    objPageLayout.PageId = 1
                    objPageLayout.FormId = 42
                    objPageLayout.PageType = 3
                    objPageLayout.numRelCntType = objLeads.CompanyType

                    btnLayout.Attributes.Add("onclick", "return ShowLayout('T','" & objLeads.CompanyType & "','42');")

                    ds = objPageLayout.GetTableInfoDefault()  ' getting the table structure 
                    dtTableInfo = ds.Tables(0)

                    intRecord = 0
                    If Not dtTableInfo Is Nothing AndAlso dtTableInfo.Rows.Count > 0 Then
                        For Each dr As DataRow In dtTableInfo.Rows
                            Dim dtData As DataTable

                            If (dr("fld_type") = "ListBox" Or dr("fld_type") = "SelectBox") Then
                                If dr("ListRelID") > 0 Then
                                    objCommon.Mode = 3
                                    objCommon.DomainID = Session("DomainID")
                                    objCommon.PrimaryListItemID = objPageControls.GetPrimaryListItemID(dr("ListRelID"), dtTableInfo, objLeads)
                                    objCommon.SecondaryListID = dr("numListId")
                                    dtData = objCommon.GetFieldRelationships.Tables(0)
                                Else
                                    dtData = objCommon.GetMasterListItems(dr("numListID"), Session("DomainID"))
                                End If
                            End If
                            intRecord = intRecord + 1
                            plhCustomFields.Controls.Add(CreateCells(dr, boolIntermediatoryPage, dtSource:=dtData, RecordID:=CCommon.ToLong(hdnDivisionID.Value)))
                            'Dim divClearFix As New HtmlGenericControl("div")
                            'divClearFix.Attributes.Add("class", "clearfix")
                            ''col-sm - 6 col-md-6
                            'If intRecord Mod 2 = 0 Then
                            '    plhCustomFields.Controls.Add(divClearFix)
                            'End If
                        Next

                        'Add Client Side validation for custom fields
                        If Not boolIntermediatoryPage Then
                            Dim strValidation As String = objPageControls.GenerateValidationScript(dtTableInfo)
                            ClientScript.RegisterClientScriptBlock(Me.GetType, "CFvalidation", strValidation, True)
                        End If
                    End If


                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        Private Sub sb_FillEmpList()
            Try
                If objContacts Is Nothing Then objContacts = New CContacts

                Dim dtEmployeeList As DataTable
                objContacts.DomainID = Session("DomainID")
                dtEmployeeList = objContacts.EmployeeList
                ddlUserNames.DataSource = dtEmployeeList
                ddlUserNames.DataTextField = "vcUserName"
                ddlUserNames.DataValueField = "numContactID"
                ddlUserNames.DataBind()
                If Not ddlUserNames.Items.FindByValue(Session("UserContactID")) Is Nothing Then
                    ddlUserNames.ClearSelection()
                    ddlUserNames.Items.FindByValue(Session("UserContactID")).Text = "My Self"
                    ddlUserNames.Items.FindByValue(Session("UserContactID")).Selected = True
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        Private Sub SavePersistent()
            Try
                PersistTable.Add("CustomFields", hdnCustomFieldExpandCollapse.Value)
                PersistTable.Add("AttendeesFields", hdnAttendeesExpandCollapse.Value)
                PersistTable.Save()
            Catch ex As Exception
                Throw
            End Try
        End Sub
        Private Sub LoadCompanyDetail()
            Try
                hdnDivisionID.Value = radCmbCompany.SelectedValue

                Dim objLeads As New BACRM.BusinessLogic.Leads.LeadsIP
                objLeads.DivisionID = radCmbCompany.SelectedValue
                objLeads.DomainID = Session("DomainID")
                objLeads.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
                objLeads.GetCompanyDetails()

                ddlFollowUpStatus.ClearSelection()
                If Not ddlFollowUpStatus.Items.FindByValue(objLeads.FollowUpStatus) Is Nothing Then
                    ddlFollowUpStatus.Items.FindByValue(objLeads.FollowUpStatus).Selected = True
                End If

                LoadControls()
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        Private Sub LoadContactDetail()
            Try
                Dim objOpportunities As New COpportunities
                objOpportunities.DivisionID = radCmbCompany.SelectedValue
                ddlContact.DataSource = objOpportunities.ListContact().Tables(0).DefaultView()
                ddlContact.DataTextField = "ContactType"
                ddlContact.DataValueField = "numcontactId"
                ddlContact.DataBind()
                ddlContact.Items.Insert(0, New ListItem("--Select One--", "0"))
                If ddlContact.Items.Count >= 2 Then
                    ddlContact.Items(1).Selected = True

                    Dim objLeads As New BACRM.BusinessLogic.Leads.LeadsIP
                    objLeads.ContactID = ddlContact.SelectedValue
                    objLeads.DomainID = Session("DomainID")
                    objLeads.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
                    objLeads.GetLeadDetails()

                    imbBtnEmail.AlternateText = objLeads.Email
                    imbBtnEmail.ToolTip = objLeads.Email
                    imbBtnEmail.Attributes.Add("onclick", "return fn_Mail('" & objLeads.Email & "','" & objLeads.ContactID & "')")
                    lblPhone.Text = objLeads.ContactPhone
                End If
            Catch ex As Exception
                Throw
            End Try
        End Sub
        Private Sub LoadTemplateDetail()
            Try
                ddlType.ClearSelection()
                ddlStatus.ClearSelection()
                ddlActivity.ClearSelection()
                ddlMinutes.ClearSelection()

                If objActionItem Is Nothing Then objActionItem = New ActionItem
                With objActionItem
                    .RowID = CCommon.ToLong(listActionItemTemplate.SelectedValue)
                End With

                Dim dt As DataTable = objActionItem.LoadThisActionItemTemplateData()

                If Not dt Is Nothing AndAlso dt.Rows.Count > 0 Then
                    Dim strDate As String = DateAdd(DateInterval.Minute, -Session("ClientMachineUTCTimeOffset"), Date.UtcNow).AddDays(Convert.ToDouble(dt.Rows(0)("DueDays")))
                    radDueDate.SelectedDate = strDate
                    txtcomments.Text = CCommon.ToString(dt.Rows(0)("Comments"))

                    If Not ddlStatus.Items.FindByValue(CCommon.ToLong(dt.Rows(0)("Priority"))) Is Nothing Then
                        ddlStatus.Items.FindByValue(CCommon.ToLong(dt.Rows(0)("Priority"))).Selected = True
                    End If

                    If Not ddlActivity.Items.FindByValue(CCommon.ToLong(dt.Rows(0)("Activity"))) Is Nothing Then
                        ddlActivity.Items.FindByValue(CCommon.ToLong(dt.Rows(0)("Activity"))).Selected = True
                    End If

                    If Not ddlType.Items.FindByValue(CCommon.ToLong(dt.Rows(0)("numTaskType"))) Is Nothing Then
                        ddlType.Items.FindByValue(CCommon.ToLong(dt.Rows(0)("numTaskType"))).Selected = True
                    End If

                    If Not ddlMinutes.Items.FindByValue(dt.Rows(0)("numRemindBeforeMinutes")) Is Nothing Then
                        ddlMinutes.Items.FindByValue(dt.Rows(0)("numRemindBeforeMinutes")).Selected = True
                    End If

                    chkRemindMe.Checked = CCommon.ToBool(dt.Rows(0)("bitRemind"))
                End If
                UpdatePanelReminder.Update()
                UpdatePanelTicklerDetail.Update()
                UpdatePanelComment.Update()
            Catch ex As Exception
                Throw
            End Try
        End Sub
        Private Sub InsertCommunication()
            Try
                Dim intTaskFlg As Integer   'Task flag for identifying whether task or Communication
                Dim lngCommId As Long
                Dim lngActivityId As Long = 0

                'Start and End Time Validation
                If ddlType.SelectedValue <> 973 And ddlType.SelectedValue <> 974 And chkFollowUp.Checked = False Then
                    Dim numSTime, numETime As Double
                    numSTime = Double.Parse(radcmbStartTime.SelectedItem.Text.Trim.Split(":")(0) + "." + radcmbStartTime.SelectedItem.Text.Trim.Split(":")(1))
                    numETime = Double.Parse(radcmbEndTime.SelectedItem.Text.Trim.Split(":")(0) + "." + radcmbEndTime.SelectedItem.Text.Trim.Split(":")(1))

                    If (chkAM.Checked = True And (numSTime = 12 Or numSTime = 12.3)) Then
                        numSTime = 0
                    End If

                    If (chkEndAM.Checked = True And (numETime = 12 Or numETime = 12.3)) Then
                        numETime = 0
                    End If

                    If chkPM.Checked = True And Not (numSTime = 12 Or numSTime = 12.3) Then
                        numSTime = numSTime + 12
                    End If

                    If chkEndPM.Checked = True And Not (numETime = 12 Or numETime = 12.3) Then
                        numETime = numETime + 12
                    End If


                    If (numSTime > numETime) Then
                        Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "TimeValidation", "alert('End Time must be greater than Start Time.')", True)
                        bError = True
                        Exit Sub
                    End If
                End If

                Dim strStartTime, strEndTime, strDate, strFollowUp As String
                Dim intAssignTo, intEndTime, intTime, intEndChk, intChk As Integer
                strDate = IIf(radDueDate.SelectedDate Is Nothing, DateTime.Now.Date, radDueDate.SelectedDate)

                If chkFollowUp.Checked = False Then
                    strStartTime = strDate.Trim & " " & radcmbStartTime.SelectedItem.Text.Trim & ":00" & IIf(chkAM.Checked = True, " AM", " PM")
                    strEndTime = strDate.Trim & " " & radcmbEndTime.SelectedItem.Text.Trim & ":00" & IIf(chkEndAM.Checked = True, " AM", " PM")
                Else
                    strStartTime = strDate.Trim & " 8:00:00 AM"
                    strEndTime = strDate.Trim & " 8:00:00 AM"
                End If


                If ddlUserNames.Enabled = True Then
                    intAssignTo = ddlUserNames.SelectedItem.Value
                    intEndTime = radcmbEndTime.SelectedItem.Value
                    intEndChk = IIf(chkEndAM.Checked = True, 1, 0)
                    intTime = radcmbStartTime.SelectedItem.Value
                    intChk = IIf(chkAM.Checked = True, 1, 0)
                End If

                If ddlType.SelectedValue <> 973 And ddlType.SelectedValue <> 974 And chkFollowUp.Checked = False Then
                    Dim strSplitStartTimeDate As Date                                               'To store the Date and Time for the start of the Action Item
                    Dim strSplitEndTimeDate As Date         'To store the Date and Time for the end of the Action Item
                    strSplitStartTimeDate = CType(strStartTime, DateTime).AddMinutes(Session("ClientMachineUTCTimeOffset"))        'Convert the Start Date and Time to UT
                    strSplitEndTimeDate = CType(strEndTime, DateTime).AddMinutes(Session("ClientMachineUTCTimeOffset"))           'Convert the End Date and Time to UTC
                    Dim strDesc As String = "Organization: " & radCmbCompany.Text & Environment.NewLine & "Contact Name: " & ddlContact.SelectedItem.Text & Environment.NewLine & "Assigned To: " & ddlUserNames.SelectedItem.Text & Environment.NewLine & "Comments:" & txtcomments.Text
                    If ddlUserNames.SelectedItem.Text = "My Self" Then
                        If chkOutlook.Checked = True Then
                            If objOutLook Is Nothing Then objOutLook = New COutlook

                            objOutLook.UserCntID = Session("UserContactId")
                            objOutLook.DomainID = Session("DomainId")
                            Dim dtTable As DataTable
                            dtTable = objOutLook.GetResourceId
                            If dtTable.Rows.Count > 0 Then
                                objOutLook.AllDayEvent = False
                                objOutLook.ActivityDescription = strDesc
                                objOutLook.Location = ""
                                objOutLook.Subject = ddlType.SelectedItem.Text
                                objOutLook.Duration = DateDiff(DateInterval.Second, strSplitStartTimeDate, strSplitEndTimeDate)
                                objOutLook.StartDateTimeUtc = strSplitStartTimeDate
                                objOutLook.EnableReminder = True
                                objOutLook.ReminderInterval = 900
                                objOutLook.ShowTimeAs = 3
                                objOutLook.Importance = 0
                                objOutLook.RecurrenceKey = -999
                                objOutLook.ResourceName = dtTable.Rows(0).Item("ResourceId")
                                lngActivityId = objOutLook.AddActivity()
                            End If
                        End If
                    Else
                        If chkOutlook.Checked = True Then
                            If objOutLook Is Nothing Then objOutLook = New COutlook
                            objOutLook.UserCntID = ddlUserNames.SelectedItem.Value
                            objOutLook.DomainID = Session("DomainId")
                            Dim dtTable As DataTable
                            dtTable = objOutLook.GetResourceId
                            If dtTable.Rows.Count > 0 Then
                                objOutLook.AllDayEvent = False
                                objOutLook.ActivityDescription = strDesc
                                objOutLook.Location = ""
                                objOutLook.Subject = ddlType.SelectedItem.Text
                                objOutLook.Duration = DateDiff(DateInterval.Second, strSplitStartTimeDate, strSplitEndTimeDate)
                                objOutLook.StartDateTimeUtc = strSplitStartTimeDate
                                objOutLook.EnableReminder = True
                                objOutLook.ReminderInterval = 900
                                objOutLook.ShowTimeAs = 3
                                objOutLook.Importance = 2
                                objOutLook.RecurrenceKey = -999
                                objOutLook.ResourceName = dtTable.Rows(0).Item("ResourceId")
                                lngActivityId = objOutLook.AddActivity()
                            End If
                        End If
                    End If

                    If Session("AttendeeTable") IsNot Nothing Then
                        If chkOutlook.Checked = True Then
                            If objOutLook Is Nothing Then objOutLook = New COutlook

                            dsTemp = Session("AttendeeTable")
                            Dim dtAttendee As DataTable
                            dtAttendee = dsTemp.Tables(0)

                            For Each dr As DataRow In dtAttendee.Rows
                                If dr("tinUserType") = "1" Then
                                    If ddlUserNames.SelectedItem.Value <> dr("numContactID") Then
                                        objOutLook.UserCntID = dr("numContactID")
                                        objOutLook.DomainID = Session("DomainId")
                                        Dim dtTable As DataTable
                                        dtTable = objOutLook.GetResourceId
                                        If dtTable.Rows.Count > 0 Then
                                            objOutLook.AllDayEvent = False
                                            objOutLook.ActivityDescription = strDesc
                                            objOutLook.Location = ""
                                            objOutLook.Subject = ddlType.SelectedItem.Text
                                            objOutLook.Duration = DateDiff(DateInterval.Second, strSplitStartTimeDate, strSplitEndTimeDate)
                                            objOutLook.StartDateTimeUtc = strSplitStartTimeDate
                                            objOutLook.EnableReminder = True
                                            objOutLook.ReminderInterval = 900
                                            objOutLook.ShowTimeAs = 3
                                            objOutLook.Importance = 2
                                            objOutLook.RecurrenceKey = -999
                                            objOutLook.ResourceName = dtTable.Rows(0).Item("ResourceId")
                                            dr("ActivityID") = objOutLook.AddActivity()
                                        End If
                                    End If
                                End If
                            Next

                            dsTemp.AcceptChanges()
                        End If
                    End If

                End If

                objActionItem = New ActionItem

                With objActionItem
                    .CommID = 0
                    .Task = ddlType.SelectedValue
                    .ContactID = ddlContact.SelectedItem.Value
                    .DivisionID = IIf(radCmbCompany.SelectedValue = "", 0, radCmbCompany.SelectedValue)
                    .Details = txtcomments.Text
                    .AssignedTo = intAssignTo
                    .UserCntID = Session("UserContactID")
                    .DomainID = Session("DomainID")
                    .BitClosed = IIf(chkClose.Checked = True, 1, 0)
                    If CCommon.ToLong(ddlType.SelectedValue) = 973 Then
                        .StartTime = IIf(radDueDate.SelectedDate Is Nothing, DateTime.Now.Date, radDueDate.SelectedDate)
                        .EndTime = DateAdd(DateInterval.Day, 1, .StartTime)
                    Else
                        .StartTime = strStartTime
                        .EndTime = strEndTime
                    End If
                    .Activity = ddlActivity.SelectedValue
                    .Status = ddlStatus.SelectedValue
                    .Remainder = ddlMinutes.SelectedValue
                    .RemainderStatus = CCommon.ToShort(chkPopupRemainder.Checked)
                    .ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
                    .bitOutlook = IIf(chkOutlook.Checked = True, 1, 0)
                    .ActivityId = lngActivityId
                    .strContactIds = hdnContactID.Value
                    .FollowUpAnyTime = IIf(chkFollowUp.Checked = True, 1, 0)

                    If Session("AttendeeTable") IsNot Nothing Then
                        dsTemp = Session("AttendeeTable")
                        dsTemp.Tables(0).TableName = "AttendeeTable"

                        .strAttendee = dsTemp.GetXml
                    End If

                    .numLinkedOrganization = IIf(String.IsNullOrEmpty(hdnLinkedOrg.Value), 0, CCommon.ToLong(hdnLinkedOrg.Value))
                    .numLinkedContact = IIf(String.IsNullOrEmpty(hdnLinkedOrgContact.Value), 0, CCommon.ToLong(hdnLinkedOrgContact.Value))
                End With

                Dim numcommId As Long = objActionItem.SaveCommunicationinfo()
                If (chkFollowUpStatus.Checked) Then
                    Dim strContactIds As String
                    If (hdnContactID.Value = "") Then
                        strContactIds = ddlContact.SelectedValue
                    Else
                        strContactIds = hdnContactID.Value
                    End If
                    Dim objCampaignUpdateFollowUp As New Campaign()
                    objCampaignUpdateFollowUp.DomainID = Session("DomainID")
                    objCampaignUpdateFollowUp.strBroadCastDtls = strContactIds
                    objCampaignUpdateFollowUp.bitUpdateFollowUpStatus = chkFollowUpStatus.Checked
                    objCampaignUpdateFollowUp.numFollowUpStatusId = ddlUpdateFollowUpStatus.SelectedValue
                    objCampaignUpdateFollowUp.UpdateFollowUpDates()
                End If
                'Added By Sachin Sadhu||Date:4thAug2014
                'Purpose :To Added Ticker data in work Flow queue based on created Rules
                '          Using Change tracking
                Dim objWfA As New Workflow()
                objWfA.DomainID = Session("DomainID")
                objWfA.UserCntID = Session("UserContactID")
                objWfA.RecordID = numcommId
                objWfA.SaveWFActionItemsQueue()
                'ss//end of code

                AddToRecentlyViewed(RecetlyViewdRecordType.ActionItem, numcommId)


                'Update FollowUp Status -Bug ID 270
                'Dim objAcc As New BACRM.BusinessLogic.Account.CAccounts
                'objAcc.ContactID = ddlContact.SelectedItem.Value
                'objAcc.FollowUpStatus = ddlFollowUpStatus.SelectedValue
                'objAcc.UpdateFollowUpStatus()

                ''Sending mail to Assigneee
                If ViewState("AssignedTo") Is Nothing Then ViewState("AssignedTo") = 0
                If Session("UserContactID") <> ddlUserNames.SelectedItem.Value Then
                    If ddlUserNames.SelectedValue > 0 And ViewState("AssignedTo") <> ddlUserNames.SelectedValue Then
                        CAlerts.SendAlertToAssignee(6, Session("UserContactID"), ddlUserNames.SelectedValue, numcommId, Session("DomainID")) 'bugid 767
                        ViewState("AssignedTo") = ddlUserNames.SelectedValue
                    End If
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        Private Sub UpdateCommunication()
            If ddlUserNames.Items.Count = 0 Then Exit Sub
            Dim intTaskFlg As Integer
            Try

                If ddlType.SelectedValue <> 973 And ddlType.SelectedValue <> 974 And chkFollowUp.Checked = False Then
                    Dim numSTime, numETime As Double
                    numSTime = Double.Parse(radcmbStartTime.SelectedItem.Text.Trim.Split(":")(0) + "." + radcmbStartTime.SelectedItem.Text.Trim.Split(":")(1))
                    numETime = Double.Parse(radcmbEndTime.SelectedItem.Text.Trim.Split(":")(0) + "." + radcmbEndTime.SelectedItem.Text.Trim.Split(":")(1))

                    If (chkAM.Checked = True And (numSTime = 12 Or numSTime = 12.3)) Then
                        numSTime = 0
                    End If

                    If (chkEndAM.Checked = True And (numETime = 12 Or numETime = 12.3)) Then
                        numETime = 0
                    End If

                    If chkPM.Checked = True And Not (numSTime = 12 Or numSTime = 12.3) Then
                        numSTime = numSTime + 12
                    End If

                    If chkEndPM.Checked = True And Not (numETime = 12 Or numETime = 12.3) Then
                        numETime = numETime + 12
                    End If


                    If (numSTime > numETime) Then
                        Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "TimeValidation", "alert('End Time must be greater than Start Time.')", True)
                        bError = True
                        Exit Sub
                    End If
                End If
                'Communication means in the DB the flag  value is equal to 1. Task means it is equal to 0.
                'intTaskFlg = IIf(cmbMainType.Value = 0, 0, 1)
                intTaskFlg = ddlType.SelectedItem.Value



                Dim strCalendar, strStartTime, strEndTime, strDate As String
                strDate = IIf(radDueDate.SelectedDate Is Nothing, DateTime.Now.Date, radDueDate.SelectedDate)

                If chkFollowUp.Checked = False Then
                    strStartTime = strDate.Trim & " " & radcmbStartTime.SelectedItem.Text.Trim & ":00" & IIf(chkAM.Checked = True, " AM", " PM")
                    strEndTime = strDate.Trim & " " & radcmbEndTime.SelectedItem.Text.Trim & ":00" & IIf(chkEndAM.Checked = True, " AM", " PM")
                Else
                    strStartTime = strDate.Trim & " 8:00:00 AM"
                    strEndTime = strDate.Trim & " 8:00:00 AM"
                End If


                If CCommon.ToLong(ddlType.SelectedValue) <> 973 And CCommon.ToLong(ddlType.SelectedValue) <> 974 And chkFollowUp.Checked = False Then
                    Dim strSplitStartTimeDate As Date                                               'To store the Date and Time for the start of the Action Item
                    Dim strSplitEndTimeDate As Date         'To store the Date and Time for the end of the Action Item
                    strSplitStartTimeDate = CType(strStartTime, DateTime).AddMinutes(Session("ClientMachineUTCTimeOffset"))        'Convert the Start Date and Time to UT
                    strSplitEndTimeDate = CType(strEndTime, DateTime).AddMinutes(Session("ClientMachineUTCTimeOffset"))          'Convert the End Date and Time to UTC
                    Dim strDesc As String = "Organization: " & lnkCompany.Text & Environment.NewLine & "Contact Name: " & lnkContact.Text & Environment.NewLine & "Assigned To: " & ddlUserNames.SelectedItem.Text & Environment.NewLine & "Comments:" & txtcomments.Text
                    If ddlUserNames.SelectedItem.Text = "My Self" Then
                        If Len(Trim(GetQueryStringVal("CommId"))) = 0 Then
                            If chkOutlook.Checked = True Then
                                If objOutLook Is Nothing Then
                                    objOutLook = New COutlook
                                End If
                                objOutLook.UserCntID = Session("UserContactId")
                                objOutLook.DomainID = Session("DomainId")
                                Dim dtTable As DataTable
                                dtTable = objOutLook.GetResourceId
                                If dtTable.Rows.Count > 0 Then
                                    objOutLook.AllDayEvent = False
                                    objOutLook.ActivityDescription = strDesc
                                    objOutLook.Location = ""
                                    objOutLook.Subject = ddlType.SelectedItem.Text
                                    objOutLook.Duration = DateDiff(DateInterval.Second, strSplitStartTimeDate, strSplitEndTimeDate)
                                    objOutLook.StartDateTimeUtc = strSplitStartTimeDate
                                    objOutLook.EnableReminder = True
                                    objOutLook.ReminderInterval = 900
                                    objOutLook.ShowTimeAs = 3
                                    objOutLook.Importance = 2
                                    objOutLook.RecurrenceKey = -999
                                    objOutLook.ResourceName = dtTable.Rows(0).Item("ResourceId")
                                    If txtActivityID.Text = "0" Then
                                        txtActivityID.Text = CStr(objOutLook.AddActivity())
                                    Else

                                        objOutLook.ActivityID = CInt(txtActivityID.Text)
                                        objOutLook.UpdateActivity()
                                    End If
                                End If


                                'strCalendar = clsProspects.fn_InsertOWAActionItem(Session("SiteType"), strSplitStartTimeDate, strSplitEndTimeDate, Session("ServerName"), Session("UserEmail"), txtcomments.Text)
                                'clsProspects.fn_DeleteOWAActionFromEx(Session("SiteType"), Session("ServerName"), Session("UserEmail"), HidStrEml.Value)
                            End If
                        Else
                            If chkClose.Checked = True Then
                                If objOutLook Is Nothing Then
                                    objOutLook = New COutlook
                                End If
                                objOutLook.ActivityID = CInt(txtActivityID.Text)
                                objOutLook.RemoveActivity()
                            Else
                                If chkOutlook.Checked = True Then

                                    If objOutLook Is Nothing Then
                                        objOutLook = New COutlook
                                    End If
                                    objOutLook.UserCntID = Session("UserContactId")
                                    objOutLook.DomainID = Session("DomainId")
                                    Dim dtTable As DataTable
                                    dtTable = objOutLook.GetResourceId
                                    If dtTable.Rows.Count > 0 Then
                                        objOutLook.AllDayEvent = False
                                        objOutLook.ActivityDescription = strDesc
                                        objOutLook.Location = ""
                                        objOutLook.Subject = ddlType.SelectedItem.Text
                                        objOutLook.Duration = DateDiff(DateInterval.Second, strSplitStartTimeDate, strSplitEndTimeDate)
                                        objOutLook.StartDateTimeUtc = strSplitStartTimeDate
                                        objOutLook.EnableReminder = True
                                        objOutLook.ReminderInterval = 900
                                        objOutLook.ShowTimeAs = 3
                                        objOutLook.Importance = 2
                                        objOutLook.RecurrenceKey = -999
                                        objOutLook.ResourceName = dtTable.Rows(0).Item("ResourceId")
                                        If txtActivityID.Text = "0" Then
                                            txtActivityID.Text = CStr(objOutLook.AddActivity())
                                        Else
                                            objOutLook.ActivityID = CInt(txtActivityID.Text)
                                            objOutLook.UpdateActivity()
                                        End If
                                    End If
                                Else
                                    If CCommon.ToInteger(txtActivityID.Text) > 0 Then
                                        If objOutLook Is Nothing Then
                                            objOutLook = New COutlook
                                        End If
                                        objOutLook.ActivityID = CInt(txtActivityID.Text)
                                        objOutLook.RemoveActivity()
                                    End If
                                End If
                            End If
                        End If

                    Else

                        If objContacts Is Nothing Then
                            objContacts = New CContacts
                        End If
                        If Len(Trim(GetQueryStringVal("CommId"))) = 0 Then
                            If chkOutlook.Checked = True Then
                                If objOutLook Is Nothing Then
                                    objOutLook = New COutlook
                                End If
                                objOutLook.UserCntID = ddlUserNames.SelectedItem.Value
                                objOutLook.DomainID = Session("DomainId")
                                Dim dtTable As DataTable
                                dtTable = objOutLook.GetResourceId
                                If dtTable.Rows.Count > 0 Then
                                    objOutLook.AllDayEvent = False
                                    objOutLook.ActivityDescription = strDesc
                                    objOutLook.Location = ""
                                    objOutLook.Subject = ddlType.SelectedItem.Text
                                    objOutLook.Duration = DateDiff(DateInterval.Second, strSplitStartTimeDate, strSplitEndTimeDate)
                                    objOutLook.StartDateTimeUtc = strSplitStartTimeDate
                                    objOutLook.EnableReminder = True
                                    objOutLook.ReminderInterval = 900
                                    objOutLook.ShowTimeAs = 3
                                    objOutLook.Importance = 2
                                    objOutLook.RecurrenceKey = -999
                                    objOutLook.ResourceName = dtTable.Rows(0).Item("ResourceId")
                                    If txtActivityID.Text = "0" Then
                                        txtActivityID.Text = CStr(objOutLook.AddActivity())
                                    Else

                                        objOutLook.ActivityID = CInt(txtActivityID.Text)
                                        objOutLook.UpdateActivity()
                                    End If
                                End If

                                'strCalendar = clsProspects.fn_InsertOWAActionItem(Session("SiteType"), strSplitStartTimeDate, strSplitEndTimeDate, Session("ServerName"), IIf(ddlUserNames.SelectedItem.Text = "My Self", Session("UserEmail"), objContacts.EmployeeEmail(ddlUserNames.SelectedItem.Value)), txtcomments.Text)
                            End If
                        Else
                            If chkClose.Checked = True Then
                                If objOutLook Is Nothing Then
                                    objOutLook = New COutlook
                                End If
                                objOutLook.ActivityID = CInt(txtActivityID.Text)
                                objOutLook.RemoveActivity()
                                ''For deleting the corresponding calendar entry in the Exchange database.
                                'clsProspects.fn_DeleteOWAActionFromEx(Session("SiteType"), Session("ServerName"), IIf(ddlUserNames.SelectedItem.Text = "My Self", Session("UserEmail"), objContacts.EmployeeEmail(ddlUserNames.SelectedItem.Value)), HidStrEml.Value)
                                'strCalendar = HidStrEml.Value
                            Else
                                If chkOutlook.Checked = True Then
                                    If objOutLook Is Nothing Then
                                        objOutLook = New COutlook
                                    End If
                                    objOutLook.UserCntID = ddlUserNames.SelectedItem.Value
                                    objOutLook.DomainID = Session("DomainId")
                                    Dim dtTable As DataTable
                                    dtTable = objOutLook.GetResourceId
                                    If dtTable.Rows.Count > 0 Then
                                        objOutLook.AllDayEvent = False
                                        objOutLook.ActivityDescription = strDesc
                                        objOutLook.Location = ""
                                        objOutLook.Subject = ddlType.SelectedItem.Text
                                        objOutLook.Duration = DateDiff(DateInterval.Second, strSplitStartTimeDate, strSplitEndTimeDate)
                                        objOutLook.StartDateTimeUtc = strSplitStartTimeDate
                                        objOutLook.EnableReminder = True
                                        objOutLook.ReminderInterval = 900
                                        objOutLook.ShowTimeAs = 3
                                        objOutLook.Importance = 2
                                        objOutLook.RecurrenceKey = -999
                                        objOutLook.ResourceName = dtTable.Rows(0).Item("ResourceId")
                                        If txtActivityID.Text = "0" Then
                                            txtActivityID.Text = CStr(objOutLook.AddActivity())
                                        Else

                                            objOutLook.ActivityID = CInt(txtActivityID.Text)
                                            objOutLook.UpdateActivity()
                                        End If
                                    End If

                                    'clsProspects.fn_DeleteOWAActionFromEx(Session("SiteType"), Session("ServerName"), IIf(ddlUserNames.SelectedItem.Text = "My Self", Session("UserEmail"), objContacts.EmployeeEmail(ddlUserNames.SelectedItem.Value)), HidStrEml.Value)
                                    'strCalendar = clsProspects.fn_InsertOWAActionItem(Session("SiteType"), strSplitStartTimeDate, strSplitEndTimeDate, Session("ServerName"), IIf(ddlUserNames.SelectedItem.Text = "My Self", Session("UserEmail"), objContacts.EmployeeEmail(ddlUserNames.SelectedItem.Value)), txtcomments.Text)
                                End If
                            End If
                        End If

                    End If

                    If Session("AttendeeTable") IsNot Nothing Then
                        If chkOutlook.Checked = True Or chkClose.Checked = True Then
                            objOutLook = New COutlook

                            dsTemp = Session("AttendeeTable")
                            Dim dtAttendee As DataTable
                            dtAttendee = dsTemp.Tables(0)

                            For Each dr As DataRow In dtAttendee.Rows
                                If dr("tinUserType") = "1" Then
                                    If ddlUserNames.SelectedItem.Value <> dr("numContactID") Then

                                        objOutLook = New COutlook

                                        If CCommon.ToLong(hdnCommunicationID.Value) > 0 Then
                                            objActionItem = New ActionItem

                                            objActionItem.CommID = CCommon.ToLong(hdnCommunicationID.Value)
                                            objActionItem.UserCntID = dr("numContactID")

                                            Dim dtAttendeeActivity As DataTable
                                            dtAttendeeActivity = objActionItem.CommunicationAttendees_Detail

                                            If dtAttendeeActivity.Rows.Count > 0 Then
                                                objOutLook.ActivityID = dtAttendeeActivity.Rows(0).Item("ActivityID")
                                            End If
                                        End If

                                        If chkClose.Checked = True And objOutLook.ActivityID > 0 Then
                                            objOutLook.RemoveActivity()
                                        ElseIf chkOutlook.Checked = True Then
                                            objOutLook.UserCntID = dr("numContactID")
                                            objOutLook.DomainID = Session("DomainId")
                                            Dim dtTable As DataTable
                                            dtTable = objOutLook.GetResourceId

                                            If dtTable.Rows.Count > 0 Then
                                                objOutLook.AllDayEvent = False
                                                objOutLook.ActivityDescription = strDesc
                                                objOutLook.Location = ""
                                                objOutLook.Subject = ddlType.SelectedItem.Text
                                                objOutLook.Duration = DateDiff(DateInterval.Second, strSplitStartTimeDate, strSplitEndTimeDate)
                                                objOutLook.StartDateTimeUtc = strSplitStartTimeDate
                                                objOutLook.EnableReminder = True
                                                objOutLook.ReminderInterval = 900
                                                objOutLook.ShowTimeAs = 3
                                                objOutLook.Importance = 2
                                                objOutLook.RecurrenceKey = -999
                                                objOutLook.ResourceName = dtTable.Rows(0).Item("ResourceId")
                                                If objOutLook.ActivityID = 0 Then
                                                    dr("ActivityID") = objOutLook.AddActivity()
                                                Else
                                                    dr("ActivityID") = objOutLook.ActivityID
                                                    objOutLook.UpdateActivity()
                                                End If
                                            End If
                                        End If
                                    End If
                                End If
                            Next

                            dsTemp.AcceptChanges()
                        End If
                    End If

                End If
                Dim intAssignTo, intEndTime, intTime, intEndChk, intChk As Integer
                Dim strFollowUp As String


                If ddlUserNames.Enabled = True Then
                    intAssignTo = ddlUserNames.SelectedItem.Value
                    intEndTime = radcmbStartTime.SelectedItem.Value
                    intEndChk = IIf(chkEndAM.Checked = True, 1, 0)
                    intTime = radcmbEndTime.SelectedItem.Value
                    intChk = IIf(chkAM.Checked = True, 1, 0)
                End If

                objActionItem = New ActionItem

                With objActionItem
                    .CommID = CCommon.ToLong(hdnCommunicationID.Value)
                    .Task = intTaskFlg
                    .Details = txtcomments.Text
                    .AssignedTo = intAssignTo
                    .UserCntID = Session("UserContactID")
                    .DomainID = Session("DomainID")
                    .BitClosed = IIf(chkClose.Checked = True, 1, 0)
                    .CalendarName = strCalendar
                    If CCommon.ToLong(ddlType.SelectedValue) = 973 Then
                        .StartTime = IIf(radDueDate.SelectedDate Is Nothing, DateTime.Now.Date, radDueDate.SelectedDate)
                        .EndTime = DateAdd(DateInterval.Day, 1, .StartTime)
                    Else
                        .StartTime = strStartTime
                        .EndTime = strEndTime
                    End If
                    .Activity = ddlActivity.SelectedItem.Value
                    .Status = ddlStatus.SelectedItem.Value
                    .Remainder = ddlMinutes.SelectedItem.Value
                    .RemainderStatus = CCommon.ToShort(chkRemindMe.Checked)
                    .ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
                    .bitOutlook = IIf(chkOutlook.Checked = True, 1, 0)
                    .CaseID = lngCaseid
                    .CaseTimeId = 0
                    .CaseExpId = 0
                    .ActivityId = CInt(txtActivityID.Text)
                    .FollowUpAnyTime = IIf(chkFollowUp.Checked = True, 1, 0)

                    If Session("AttendeeTable") IsNot Nothing Then
                        dsTemp = Session("AttendeeTable")
                        dsTemp.Tables(0).TableName = "AttendeeTable"
                        .strAttendee = dsTemp.GetXml
                    End If

                    .numLinkedOrganization = IIf(String.IsNullOrEmpty(hdnLinkedOrg.Value), 0, CCommon.ToLong(hdnLinkedOrg.Value))
                    .numLinkedContact = IIf(String.IsNullOrEmpty(hdnLinkedOrgContact.Value), 0, CCommon.ToLong(hdnLinkedOrgContact.Value))
                End With
                objActionItem.SaveCommunicationinfo()
                ''Add To Correspondense if Open record is selected

                'Added By Sachin Sadhu||Date:4thAug2014
                'Purpose :To Added Ticker data in work Flow queue based on created Rules
                '          Using Change tracking
                Dim objWfA As New Workflow()
                objWfA.DomainID = Session("DomainID")
                objWfA.UserCntID = Session("UserContactID")
                objWfA.RecordID = CCommon.ToLong(hdnCommunicationID.Value)
                objWfA.SaveWFActionItemsQueue()
                'ss//end of code

                'Update FollowUp Status -Bug ID 270
                Dim objAcc As New BACRM.BusinessLogic.Account.CAccounts
                objAcc.ContactID = hdnContactID.Value
                objAcc.FollowUpStatus = ddlFollowUpStatus.SelectedValue
                objAcc.UpdateFollowUpStatus()

                ''Sending mail to Assigneee
                If Session("UserContactID") <> ddlUserNames.SelectedValue Then
                    If ddlUserNames.SelectedValue > 0 And ViewState("AssignedTo") <> ddlUserNames.SelectedValue Then
                        CAlerts.SendAlertToAssignee(6, Session("UserContactID"), ddlUserNames.SelectedValue, CCommon.ToLong(hdnCommunicationID.Value), Session("DomainID")) 'bugid 767
                        ViewState("AssignedTo") = ddlUserNames.SelectedValue
                    End If
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        Function sb_PageRedirect() As String
            Try
                Dim strRedirect As String = ""
                Dim RedirectForm As String = GetQueryStringVal("frm")

                If RedirectForm IsNot Nothing Then
                    If RedirectForm.ToLower() = "contactdetails" Then
                        strRedirect = "~/contact/frmContacts.aspx?frm=" & GetQueryStringVal("frm1") & "&CntId=" & hdnRedirectID.Value & "&tabkey=Correspondence"
                    ElseIf RedirectForm.ToLower() = "contactdtl" Then
                        strRedirect = "~/contact/frmContacts.aspx?frm=" & GetQueryStringVal("frm1") & "&CntId=" & hdnRedirectID.Value & "&tabkey=Correspondence"
                    ElseIf RedirectForm.ToLower() = "tickler" Then
                        strRedirect = "~/common/frmticklerdisplay.aspx"
                    ElseIf RedirectForm.ToLower() = "accounts" Then
                        strRedirect = "~/Account/frmAccounts.aspx?frm=accounts&klds+7kldf=fjk-las&DivId=" & hdnDivisionID.Value & "&tabkey=Correspondence"
                    ElseIf RedirectForm.ToLower() = "accountsdtl" Then
                        strRedirect = "~/Account/frmAccounts.aspx?frm=accounts&klds+7kldf=fjk-las&DivId=" & hdnDivisionID.Value & "&tabkey=Correspondence"
                    ElseIf RedirectForm.ToLower() = "prospects" Then
                        strRedirect = "~/prospects/frmProspects.aspx?frm=prospects&DivID=" & hdnRedirectID.Value & "&tabkey=Correspondence"
                    ElseIf RedirectForm.ToLower() = "prospectsdtl" Then
                        strRedirect = "~/prospects/frmProspects.aspx?frm=prospects&DivID=" & hdnRedirectID.Value & "&tabkey=Correspondence"
                    ElseIf RedirectForm.ToLower() = "opportunitylist" Then
                        strRedirect = "`/opportunity/opptlist.aspx"
                    ElseIf RedirectForm.ToLower() = "leads" Or RedirectForm.ToLower() = "leadedetails" Then
                        strRedirect = "~/Leads/frmLeads.aspx?DivID=" & hdnRedirectID.Value & "&tabkey=Correspondence"
                    ElseIf RedirectForm.ToLower() = "Leaddtl" Then
                        strRedirect = "~/Leads/frmLeads.aspx?DivID=" & hdnRedirectID.Value & "&tabkey=Correspondence"
                    ElseIf RedirectForm.ToLower() = "opportunitydtl" Then
                        strRedirect = "~/opportunity/frmOpportunities.aspx?OpID=" & GetQueryStringVal("RecordID") & "&tabkey=Correspondence"
                    ElseIf RedirectForm.ToLower() = "projectdtl" Then
                        strRedirect = "~/projects/frmProjects.aspx?ProId=" & GetQueryStringVal("RecordID") & "&tabkey=Correspondence"
                    ElseIf RedirectForm.ToLower() = "casedtl" Then
                        strRedirect = "~/Cases/frmCases.aspx?frm=caselist&CaseID=" & GetQueryStringVal("RecordID") & "&tabkey=Correspondence"
                    ElseIf RedirectForm.ToLower() = "cases" Then
                        strRedirect = "~/Cases/frmCases.aspx?frm=caselist&CaseID=" & GetQueryStringVal("CaseId") & "&tabkey=Correspondence"
                    Else
                        strRedirect = "~/common/frmticklerdisplay.aspx?SelectedIndex=0"
                    End If
                Else
                    strRedirect = "~/common/frmticklerdisplay.aspx?SelectedIndex=0"
                End If
                Return strRedirect
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Private Sub BindActionItemTemplateData()
            Try
                If objActionItem Is Nothing Then objActionItem = New ActionItem
                Dim dtActionItems As DataTable
                objActionItem.DomainID = Session("DomainId")
                objActionItem.UserCntID = Session("UserContactId")
                dtActionItems = objActionItem.LoadActionItemTemplateData()

                If Not dtActionItems Is Nothing Then
                    listActionItemTemplate.DataSource = dtActionItems
                    listActionItemTemplate.DataTextField = "TemplateName"
                    listActionItemTemplate.DataValueField = "RowID"
                    listActionItemTemplate.DataBind()
                    listActionItemTemplate.Items.Insert(0, New ListItem("--Select One--", 0))
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        Private Sub DisplayError(ByVal ex As String)
            DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ex
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
        End Sub
        Private Function CreateCells(ByVal dr As DataRow, ByVal IntermediatoryPage As Boolean, Optional ByVal dtSource As DataTable = Nothing, Optional ByVal RecordID As Long = 0) As HtmlGenericControl
            Try
                Dim divMain As New HtmlGenericControl("div")
                divMain.Attributes.Add("class", "col-sm-6 col-md-3")

                Dim divFormGroup As New HtmlGenericControl("div")
                divFormGroup.Attributes.Add("css", "form-group")

                Dim label As New HtmlGenericControl("label")

                Dim divValue As New HtmlGenericControl("div")
                Dim labelValue As New HtmlGenericControl("label")


                Dim ddl As DropDownList
                Dim radcmb As RadComboBox
                Dim lst As ListBox
                Dim txt As TextBox
                Dim chk As CheckBox
                Dim hpl As HyperLink
                Dim btn As Button

                If CStr(dr("fld_type")) = "Popup" Then
                    hpl = New HyperLink
                    hpl.Text = dr("vcFieldName").ToString & IIf(IntermediatoryPage, " : ", "").ToString
                    hpl.Font.Bold = True
                    hpl.NavigateUrl = "#"
                    If Not IsDBNull(dr("PopupFunctionName")) Then
                        hpl.Attributes.Add("onclick", "return " & dr("PopupFunctionName").ToString & "(" & RecordID & ")")
                    End If
                    label.Controls.Add(hpl)
                Else
                    'tblCell.Text = dr("vcFieldName").ToString & IIf(CCommon.ToInteger(dr("bitIsRequired")) = 1 And IntermediatoryPage = False, "<font color=""red"">*</font>", "").ToString() & IIf(IntermediatoryPage, " : ", "").ToString

                    Dim lblFieldName As Label
                    lblFieldName = New Label
                    lblFieldName.Text = dr("vcFieldName").ToString & IIf((CCommon.ToInteger(dr("bitIsRequired")) = 1 Or CCommon.ToBool(dr("bitIsEmail")) = True Or CCommon.ToBool(dr("bitIsNumeric")) = True Or CCommon.ToBool(dr("bitIsAlphaNumeric")) = True Or CCommon.ToBool(dr("bitIsLengthValidation")) = True) And IntermediatoryPage = False, "<font color=""red"">*</font>", "").ToString() & IIf(IntermediatoryPage, " : ", "").ToString
                    label.Controls.Add(lblFieldName)
                End If

                If Not IsDBNull(dr("vcToolTip")) And dr("vcToolTip").ToString.Trim.Length > 0 Then
                    Dim lblToolTip As Label
                    lblToolTip = New Label
                    lblToolTip.Text = "[?]"
                    lblToolTip.CssClass = "tip"
                    lblToolTip.ToolTip = dr("vcToolTip").ToString.Trim
                    label.Controls.Add(lblToolTip)
                End If

                divFormGroup.Controls.Add(label)

                dr("vcFieldName") = dr("vcFieldName").ToString.Replace("$", "")
                If CStr(dr("fld_type")) = "SelectBox" Then
                    labelValue.InnerText = CCommon.ToString(dr("vcValue"))
                ElseIf CStr(dr("fld_type")) = "TextBox" Or CStr(dr("fld_type")) = "Website" Or CStr(dr("fld_type")) = "TextArea" Or CStr(dr("fld_type")) = "Email" Then
                    If CBool(dr("bitCustomField")) = False Then
                        labelValue.InnerText = dr("vcValue").ToString
                    Else
                        If dr("vcValue").ToString <> "0" Then
                            labelValue.InnerText = dr("vcValue").ToString
                        Else
                            labelValue.InnerText = ""
                        End If
                    End If
                ElseIf CStr(dr("fld_type")) = "CheckBox" Then
                    labelValue.InnerText = CCommon.ToString(dr("vcValue").ToString)
                ElseIf CStr(dr("fld_type")) = "DateField" Then
                    Dim strDate As String = CCommon.ToString(dr("vcValue"))
                    If strDate = "0" Then strDate = ""
                    If strDate = "01/01/0001 00:00:00" Or strDate = "01/01/1753 00:00:00" Then strDate = ""

                    If strDate <> "" Then
                        labelValue.InnerText = strDate
                    End If
                ElseIf CStr(dr("fld_type")) = "Popup" Or CStr(dr("fld_type")) = "Label" Then
                    labelValue.InnerText = dr("vcValue").ToString
                End If

                divValue.Controls.Add(labelValue)
                divFormGroup.Controls.Add(divValue)
                divMain.Controls.Add(divFormGroup)

                Return divMain
            Catch ex As Exception
                Throw
            End Try
        End Function
        Private Sub LoadLinkedOrganizationDetail(ByVal divisionID As Long, ByVal contactID As Long)
            Try
                hdnLinkedOrg.Value = divisionID
                hdnLinkedOrgContact.Value = contactID

                Dim objLeads As New BACRM.BusinessLogic.Leads.LeadsIP
                objLeads.DivisionID = divisionID
                objLeads.DomainID = Session("DomainID")
                objLeads.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
                objLeads.GetCompanyDetails()

                lblLinkedOrg.Text = objLeads.CompanyName

                If Not ddlFollowUpStatus.Items.FindByValue(objLeads.FollowUpStatus) Is Nothing AndAlso CCommon.ToLong(objLeads.FollowUpStatus) > 0 Then
                    lblLinkedOrgFolloUp.Text = ddlFollowUpStatus.Items.FindByValue(objLeads.FollowUpStatus).Text
                End If

                objLeads.ContactID = contactID
                objLeads.DomainID = Session("DomainID")
                objLeads.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
                objLeads.GetLeadDetails()

                lblLinkedOrgContact.Text = objLeads.FirstName & " " & objLeads.LastName

                lblLinkedOrgPhone.Text = objLeads.ContactPhone

                imbBtnLinkedOrgEmail.AlternateText = objLeads.Email
                imbBtnLinkedOrgEmail.ToolTip = objLeads.Email
                imbBtnLinkedOrgEmail.Attributes.Add("onclick", "return fn_Mail('" & objLeads.Email & "','" & objLeads.ContactID & "')")
            Catch ex As Exception
                Throw
            End Try
        End Sub
#End Region

#Region "Event Handlers"
#Region "Buttons"
        Private Sub btnReloadTempateData_Click(sender As Object, e As EventArgs) Handles btnReloadTempateData.Click
            Try
                LoadTemplateDetail()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub
        Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
            Try
                Save()

                If bError = True Then
                    Exit Sub
                End If

                Session("AttendeeTable") = Nothing

                If GetQueryStringVal("Popup") = "True" Then
                    Dim strScript As String = "<script language=JavaScript>"
                    strScript += "self.close();"
                    strScript += "</script>"
                    If (Not Page.IsStartupScriptRegistered("clientScript")) Then Page.RegisterStartupScript("clientScript", strScript)
                Else : Response.Redirect(sb_PageRedirect())
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub
        Private Sub btnSaveOnly_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveOnly.Click
            Try
                Save()
                LoadLinkedOrganizationDetail(CCommon.ToLong(hdnLinkedOrg.Value), CCommon.ToLong(hdnLinkedOrgContact.Value))
                If bError = True Then
                    Exit Sub
                End If

            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub
        Private Sub btnSaveNext_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveNext.Click
            Try
                Save()

                If bError = True Then
                    Exit Sub
                End If

                Session("AttendeeTable") = Nothing

                If Session("strCorrTickler") IsNot Nothing Then

                    Dim strCorrTickler As String = ""

                    strCorrTickler = Session("strCorrTickler")

                    Dim arrayCorrTickler As String() = strCorrTickler.Split(",")

                    Dim iCommId As Int32 = Array.IndexOf(arrayCorrTickler, String.Format("{0}:{1}:{2}:{3}", CCommon.ToString(hdnCommunicationID.Value), GetQueryStringVal("CaseId"), GetQueryStringVal("CaseTimeId"), GetQueryStringVal("CaseExpId"))) + 1

                    If arrayCorrTickler.Length - 1 = iCommId Or arrayCorrTickler.Length - 1 = iCommId - 1 Then
                        Session("strCorrTickler") = Nothing
                    End If

                    'Session("strCorrTickler") = String.Join(",", arrayCorrTickler)
                    'If strCorrTickler.Contains(",") Then
                    '    strCorrTickler = strCorrTickler.Remove(0, strCorrTickler.IndexOf(",") + 1)
                    '    Session("strCorrTickler") = strCorrTickler
                    'Else
                    '    Session("strCorrTickler") = Nothing
                    'End If

                    Session("AttendeeTable") = Nothing

                    If arrayCorrTickler.Length - 1 >= iCommId Then
                        Dim arrayCorrespondence As String() = arrayCorrTickler(iCommId).Split(":")
                        Dim lngCommIDNew As Long
                        Dim lngCaseIdNew As Long
                        Dim lngCasetimeIdNew As Long
                        Dim lngCaseExpIdNew As Long

                        lngCommIDNew = arrayCorrespondence(0)
                        lngCaseIdNew = arrayCorrespondence(1)
                        lngCasetimeIdNew = arrayCorrespondence(2)
                        lngCaseExpIdNew = arrayCorrespondence(3)

                        Dim strRedirect As String = String.Format("../admin/actionitemdetails.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&CommId={0}&frm={1}&CaseId={2}&CaseTimeId={3}&CaseExpId={4}", lngCommIDNew, GetQueryStringVal("frm"), lngCaseIdNew, lngCasetimeIdNew, lngCaseExpIdNew)

                        Response.Redirect(strRedirect)
                    Else
                        Response.Redirect(sb_PageRedirect())
                    End If
                    '"../admin/actionitemdetails.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&CommId=" + a + "&frm=tickler&CaseId=" + b + "&CaseTimeId=" + c + "&CaseExpId=" + d + "&SI1=" + selectedIndex
                ElseIf GetQueryStringVal("Popup") = "True" Then
                    Dim strScript As String = "<script language=JavaScript>"
                    strScript += "self.close();"
                    strScript += "</script>"
                    If (Not Page.IsStartupScriptRegistered("clientScript")) Then Page.RegisterStartupScript("clientScript", strScript)
                Else : Response.Redirect(sb_PageRedirect())
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub
        Private Sub btnActDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnActDelete.Click
            Try
                Dim str As String = sb_PageRedirect()

                Session("AttendeeTable") = Nothing

                If objActionItem Is Nothing Then objActionItem = New ActionItem
                objActionItem.CommID = CCommon.ToLong(hdnCommunicationID.Value)
                objActionItem.DeleteActionItem()

                If GetQueryStringVal("Popup") = "True" Then
                    Dim strScript As String = "<script language=JavaScript>"
                    strScript += "self.close();"
                    strScript += "</script>"
                    If (Not Page.IsStartupScriptRegistered("clientScript")) Then Page.RegisterStartupScript("clientScript", strScript)
                Else : Response.Redirect(str)
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub
        Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
            Try
                Session("AttendeeTable") = Nothing

                Response.Redirect(sb_PageRedirect())
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub
        Private Sub btnFollowup_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnFollowup.Click
            Try
                chkClose.Checked = True
                Save()

                Session("AttendeeTable") = Nothing

                If bError = True Then
                    Exit Sub
                End If

                If objCommon Is Nothing Then objCommon = New CCommon
                objCommon.CommID = GetQueryStringVal("CommId")
                objCommon.charModule = "A"
                objCommon.GetCompanySpecificValues1()
                Response.Redirect("../admin/actionitemdetails.aspx?frm=" & GetQueryStringVal("frm") & "&cntid=" & objCommon.ContactID & "&Popup=" & GetQueryStringVal("Popup"), False)

            Catch ex As Exception
                Response.Redirect(sb_PageRedirect())
            End Try
        End Sub
        Private Sub btnCustomFieldsExpandCollapse_Click(sender As Object, e As EventArgs) Handles btnCustomFieldsExpandCollapse.Click
            Try
                SavePersistent()

                If CCommon.ToBool(CCommon.ToInteger(hdnCustomFieldExpandCollapse.Value)) Then
                    ScriptManager.RegisterClientScriptBlock(UpdatePanelActionTemplate, UpdatePanelActionTemplate.GetType(), "CustomFieldExpandCollapse", "$('#divCustomFieldsBody').hide(); $('#btnCustomExpandCollapse').html(""<i class='fa fa-plus'></i>""); $('#divCustomFieldsMain').addClass(""collapsed-box"");", True)
                Else
                    ScriptManager.RegisterClientScriptBlock(UpdatePanelActionTemplate, UpdatePanelActionTemplate.GetType(), "CustomFieldExpandCollapse", "$('#divCustomFieldsBody').show(); $('#btnCustomExpandCollapse').html(""<i class='fa fa-minus'></i>""); $('#divCustomFieldsMain').removeClass(""collapsed-box"");", True)
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub
        Private Sub btnAttendeesPersistExpandCollapse_Click(sender As Object, e As EventArgs) Handles btnAttendeesPersistExpandCollapse.Click
            Try
                SavePersistent()

                If CCommon.ToBool(CCommon.ToInteger(hdnAttendeesExpandCollapse.Value)) Then
                    ScriptManager.RegisterClientScriptBlock(UpdatePanelActionTemplate, UpdatePanelActionTemplate.GetType(), "AttendeesExpandCollapse", "$('#divAttendeesBody').hide(); $('#btnAttendeesExpandCollapse').html(""<i class='fa fa-plus'></i>""); $('#divAttendeesMain').addClass(""collapsed-box"");", True)
                Else
                    ScriptManager.RegisterClientScriptBlock(UpdatePanelActionTemplate, UpdatePanelActionTemplate.GetType(), "AttendeesExpandCollapse", "$('#divAttendeesBody').show(); $('#btnAttendeesExpandCollapse').html(""<i class='fa fa-minus'></i>""); $('#divAttendeesMain').removeClass(""collapsed-box"");", True)
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub
#End Region

#Region "DropDowns"
        Private Sub ddlContact_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlContact.SelectedIndexChanged
            Try
                BindFollowUpStatus()
                Dim objLeads As New BACRM.BusinessLogic.Leads.LeadsIP
                objLeads.ContactID = ddlContact.SelectedValue
                objLeads.DomainID = Session("DomainID")
                objLeads.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
                objLeads.GetLeadDetails()

                imbBtnEmail.AlternateText = objLeads.Email
                imbBtnEmail.ToolTip = objLeads.Email
                imbBtnEmail.Attributes.Add("onclick", "return fn_Mail('" & objLeads.Email & "','" & objLeads.ContactID & "')")
                lblPhone.Text = objLeads.ContactPhone
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub
        Private Sub listActionItemTemplate_SelectedIndexChanged(sender As Object, e As EventArgs) Handles listActionItemTemplate.SelectedIndexChanged
            Try
                If CCommon.ToLong(hdnCommunicationID.Value) = 0 Then
                    LoadTemplateDetail()
                    BindFollowUpStatus()
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub
        Private Sub radCmbCompany_SelectedIndexChanged(sender As Object, e As RadComboBoxSelectedIndexChangedEventArgs) Handles radCmbCompany.SelectedIndexChanged
            Try
                ddlContact.Items.Clear()
                BindFollowUpStatus()
                If radCmbCompany.SelectedValue <> "" Then
                    LoadCompanyDetail()
                    LoadContactDetail()

                    hdnDivisionID.Value = radCmbCompany.SelectedValue

                    Correspondence1.lngRecordID = radCmbCompany.SelectedValue
                    Correspondence1.Mode = 7
                    Correspondence1.bPaging = False
                    Correspondence1.getCorrespondance()
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub
#End Region

#Region "HyperLinks"
        Private Sub lnkContact_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkContact.Click
            Try
                If objCommon Is Nothing Then objCommon = New CCommon
                objCommon.CommID = CCommon.ToLong(hdnCommunicationID.Value)
                objCommon.charModule = "A"
                objCommon.GetCompanySpecificValues1()
                Response.Redirect("../contact/frmContacts.aspx?frm=ActItem&CntId=" & objCommon.ContactID & "&CommID=" & CCommon.ToLong(hdnCommunicationID.Value))

            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub
        Private Sub lnkCompany_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkCompany.Click
            Try
                If objCommon Is Nothing Then objCommon = New CCommon
                objCommon.CommID = CCommon.ToLong(hdnCommunicationID.Value)
                objCommon.charModule = "A"
                objCommon.GetCompanySpecificValues1()
                If objCommon.CRMType = 0 Then
                    Response.Redirect("../Leads/frmLeads.aspx?frm=ActItem&DivID=" & objCommon.DivisionID & "&CommID=" & CCommon.ToLong(hdnCommunicationID.Value))
                ElseIf objCommon.CRMType = 1 Then
                    Response.Redirect("../prospects/frmProspects.aspx?frm=ActItem&DivID=" & objCommon.DivisionID & "&CommID=" & CCommon.ToLong(hdnCommunicationID.Value))
                ElseIf objCommon.CRMType = 2 Then
                    Response.Redirect("../account/frmAccounts.aspx?frm=ActItem&klds+7kldf=fjk-las&DivId=" & objCommon.DivisionID & "&CommID=" & CCommon.ToLong(hdnCommunicationID.Value))
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub
#End Region
#End Region

    End Class
End Namespace