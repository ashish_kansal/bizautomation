<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" Codebehind="frmCompanyAssociationFrom.aspx.vb" Inherits="BACRM.UserInterface.Admin.frmCompanyAssociationFrom"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
	<head runat="server">
        <link rel="stylesheet" href="~/CSS/master.css" type="text/css" />
		<title>Organization Association</title>
		<script language=javascript >
		function Close()
		{
			window.close()
			return false;
		}
		</script>
	</HEAD>
	<body >
		<form id="frmAssociationFrom" method="post" runat="server">
		<table width="100%" class="aspTable" style=" height:200px">
				<tr><td valign="top">
			<table width="100%" >
				<tr>
					<td align="right"><asp:Button ID="btnClose" Runat="server" Text="Close" Width="50" CssClass="button"></asp:Button></td>
				</tr>
			</table>
			
			<asp:datagrid id="dgAssociation" runat="server" Width="100%" CssClass="dg" 
				AutoGenerateColumns="False" AllowSorting="True">
				<AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
				<ItemStyle CssClass="is"></ItemStyle>
				<HeaderStyle CssClass="hs"></HeaderStyle>
				<Columns>
					<asp:BoundColumn DataField="Company" HeaderText="Was Associated From"></asp:BoundColumn>
					<asp:BoundColumn DataField="vcData" HeaderText="As Its"></asp:BoundColumn>
				</Columns>
			</asp:datagrid>
			</td></tr>
			</table>
		</form>
	</body>
</HTML>
