﻿Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Accounting

Namespace BACRM.UserInterface.Admin

    Public Class frmAdvAccRes
        Inherits BACRMPage
        Dim boolExport As Boolean = False
        Dim boolUpdate As Boolean
        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            Try
                'CLEAR ERROR ON RELOAD
                DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ""
                DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "none")

                'CLEAR ALERT MESSAGE
                divMessage.Style.Add("display", "none")
                litMessage.Text = ""

                GetUserRightsForPage(9, 12)

                If Not IsPostBack Then
                    ViewState("AdvancedSearchCondition") = Session("AdvancedSearchCondition")
                    ViewState("AdvancedSearchSavedSearchID") = Session("AdvancedSearchSavedSearchID")
                    Session("AdvancedSearchCondition") = Nothing
                    Session("AdvancedSearchSavedSearchID") = Nothing

                    txtCurrrentPage.Text = 1

                    If m_aryRightsForPage(RIGHTSTYPE.EXPORT) = 0 Then btnExport.Visible = False

                    Dim lngSearchID As Long = CCommon.ToLong(GetQueryStringVal("SearchID"))
                    If lngSearchID > 0 Then
                        Dim objSearch As New FormGenericAdvSearch
                        objSearch.SetSavedSearchQuery(lngSearchID, 59)
                    End If

                    BindGridView(True)
                End If

                If txtReload.Text = "true" Then
                    BindGridView(True)
                    txtReload.Text = "false"
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Sub BindGridView(ByVal CreateCol As Boolean)
            Try
                Dim objAdmin As New FormGenericAdvSearch
                Dim dtResults As DataTable
      
                With objAdmin
                    .QueryWhereCondition = Session("WhereContditionAcc") & CCommon.ToString(Session("TimeQuery")) 'when sliding date is selected then we need to store where condition and time condition seperately
                    .DomainID = Session("DomainID")
                    .UserCntID = Session("UserContactID")
                    .AuthenticationGroupID = Session("UserGroupID")
                    .FormID = 59
                    .ViewID = 1
                    .SortCharacter = txtSortChar.Text.Trim()
                    .SortcolumnName = txtSortColumn.Text
                    If txtCurrrentPage.Text.Trim = "" Then txtCurrrentPage.Text = 1
                    .CurrentPage = txtCurrrentPage.Text.Trim()
                    .PageSize = Session("PagingRows")
                    .TotalRecords = 0
                    If txtSortOrder.Text = "D" Then
                        .columnSortOrder = "Desc"
                    Else : .columnSortOrder = "Asc"
                    End If
                    .columnName = "datCreatedDate"

                    If boolExport = True Then
                        .GetAll = True
                        dtResults = .AdvancedSearchFinancialSearch()
                        Dim iExportCount As Integer

                        dtResults.Columns.RemoveAt(1)
                        dtResults.Columns.RemoveAt(0)
                        Response.Clear()
                        For iExportCount = 0 To dtResults.Columns.Count - 1
                            dtResults.Columns(iExportCount).ColumnName = dtResults.Columns(iExportCount).ColumnName.Split("~")(0)
                        Next
                        For Each column As DataColumn In dtResults.Columns
                            Response.Write(column.ColumnName + ",")
                        Next
                        Response.Write(Environment.NewLine)
                        For Each row As DataRow In dtResults.Rows
                            For iExportCount = 0 To dtResults.Columns.Count - 1
                                Response.Write(row(iExportCount).ToString().Replace(";", String.Empty) & ",")
                            Next
                            Response.Write(Environment.NewLine)
                        Next
                        Response.ContentType = "text/csv"
                        Response.AppendHeader("Content-Disposition", "attachment; filename=" + "File" + Format(Now, "ddmmyyyyhhmmss") + ".csv")
                        Response.End()
                        .GetAll = False
                    End If

                    dtResults = .AdvancedSearchFinancialSearch()

                    bizPager.PageSize = Session("PagingRows")
                    bizPager.CurrentPageIndex = txtCurrrentPage.Text
                    bizPager.RecordCount = objAdmin.TotalRecords

                End With
               
                Dim drow As DataRow
                For Each dr As DataRow In dtResults.Rows
                    If CCommon.ToDouble(dr("numDebitAmt")) = 0 Then
                        dr("numDebitAmt") = ReturnMoney(CCommon.ToDouble(dr("numCreditAmt"))) & " Cr"
                    Else
                        dr("numDebitAmt") = ReturnMoney(CCommon.ToDouble(dr("numDebitAmt"))) & " Dr"
                    End If
                Next

                RepGeneralLedger.DataSource = dtResults
                RepGeneralLedger.DataBind()
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Function ReturnMoney(ByVal Money)
            Try
                If Not IsDBNull(Money) Then Return String.Format("{0:#,##0.00}", Money)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Private Sub btnExport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExport.Click
            Try
                boolExport = True
                BindGridView(True)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Private Sub btnUpdateValues_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpdateValues.Click
            Try
                boolUpdate = True
                BindGridView(True)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Private Sub btnGo1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGo1.Click
            Try
                BindGridView(True)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub
        Private Sub RepGeneralLedger_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles RepGeneralLedger.ItemCommand
            Try
                '' Dim hplType As HyperLink
                Dim linJournalId As Long = CCommon.ToLong(CType(e.Item.FindControl("lblJournalId"), Label).Text)
                Dim lintCheckId As Long = CCommon.ToLong(CType(e.Item.FindControl("lblCheckId"), Label).Text)
                Dim lintCashCreditCardId As Long = CCommon.ToLong(CType(e.Item.FindControl("lblCashCreditCardId"), Label).Text)
                Dim lintTransactionId As Long = CCommon.ToLong(CType(e.Item.FindControl("lblTransactionId"), Label).Text)
                Dim lintOppId As Long = CCommon.ToLong(CType(e.Item.FindControl("lblOppId"), Label).Text)
                Dim lintOppBizDocsId As Long = CCommon.ToLong(CType(e.Item.FindControl("lblOppBizDocsId"), Label).Text)
                Dim lintDepositId As Long = CCommon.ToLong(CType(e.Item.FindControl("lblDepositId"), Label).Text)
                Dim lintCategoryHDRID As Long = CCommon.ToLong(CType(e.Item.FindControl("lblCategoryHDRID"), Label).Text)
                Dim lintChartAcntId As Long = CCommon.ToLong(CType(e.Item.FindControl("lblChartAcntId"), Label).Text)
                Dim lintBillPaymentID As Long = CCommon.ToLong(CType(e.Item.FindControl("lblBillPaymentID"), Label).Text)
                Dim lintBillID As Long = CCommon.ToLong(CType(e.Item.FindControl("lblBillID"), Label).Text)

                If e.CommandName = "Edit" Then
                    If linJournalId <> 0 And lintCheckId = 0 And lintCashCreditCardId = 0 And lintOppId = 0 And lintOppBizDocsId = 0 And lintDepositId = 0 And lintCategoryHDRID = 0 And lintBillPaymentID = 0 Then
                        Response.Redirect("../Accounting/frmNewJournalEntry.aspx?frm=GeneralLedger&JournalId=" & linJournalId)
                    ElseIf lintCheckId <> 0 Then
                        Response.Redirect("../Accounting/frmWriteCheck.aspx?frm=GeneralLedger&JournalId=" & linJournalId & "&CheckHeaderID=" & lintCheckId)
                    ElseIf lintCashCreditCardId <> 0 Then
                        Response.Redirect("../Accounting/frmCash.aspx?frm=GeneralLedger&JournalId=" & linJournalId & "&CashCreditCardId=" & lintCashCreditCardId)
                    ElseIf lintBillPaymentID <> 0 Then
                        Response.Redirect("../Accounting/frmVendorPayment.aspx?frm=GeneralLedger&BillPaymentID=" & lintBillPaymentID)
                    ElseIf lintDepositId <> 0 Then
                        Dim objMakeDeposit As New MakeDeposit
                        objMakeDeposit.DepositId = lintDepositId
                        objMakeDeposit.DomainID = Session("DomainID")
                        objMakeDeposit.Mode = 0
                        Dim dtMakeDeposit As DataTable = objMakeDeposit.GetDepositDetails().Tables(0)

                        If dtMakeDeposit.Rows.Count > 0 Then
                            If CCommon.ToShort(dtMakeDeposit.Rows(0)("tintDepositePage")) = 2 Then
                                Response.Redirect("../opportunity/frmAmtPaid.aspx?frm=GeneralLedger&DepositeID=" & lintDepositId)
                            ElseIf CCommon.ToShort(dtMakeDeposit.Rows(0)("tintDepositePage")) = 1 Then
                                Response.Redirect("../Accounting/frmMakeDeposit.aspx?frm=GeneralLedger&JournalId=" & linJournalId & "&ChartAcntId=" & lintChartAcntId & "&DepositId=" & lintDepositId)
                            End If
                        End If
                    End If
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Private Sub RepGeneralLedger_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles RepGeneralLedger.ItemDataBound
            Try
                If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
                    Dim lblOppId As Label = e.Item.FindControl("lblOppId")
                    Dim lblOppBizDocsId As Label = e.Item.FindControl("lblOppBizDocsId")
                    Dim lintOppId As Integer
                    Dim lintOppBizDocsId As Integer
                    Dim lintCategoryHDRID As Integer
                    Dim lnkType As LinkButton = e.Item.FindControl("lnkType")
                    Dim lblCategoryHDRID As Label = e.Item.FindControl("lblCategoryHDRID")
                    Dim lblTEType As Label = e.Item.FindControl("lblTEType")
                    Dim lblCategory As Label = e.Item.FindControl("lblCategory")
                    Dim lblUserCntID As Label = e.Item.FindControl("lblUserCntID")
                    Dim lblFromDate As Label = e.Item.FindControl("lblFromDate")

                    lintOppId = IIf(lblOppId.Text = "&nbsp;" OrElse lblOppId.Text = "", 0, lblOppId.Text)
                    lintOppBizDocsId = IIf(lblOppBizDocsId.Text = "&nbsp;" OrElse lblOppBizDocsId.Text = "", 0, lblOppBizDocsId.Text)
                    lintCategoryHDRID = IIf(lblCategoryHDRID.Text = "&nbsp;" OrElse lblCategoryHDRID.Text = "", 0, lblCategoryHDRID.Text)

                    If lintOppId <> 0 And lintOppBizDocsId <> 0 Then
                        lnkType.Attributes.Add("onclick", "return OpenBizIncome('" & "../opportunity/frmBizInvoice.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&OpID=" & lintOppId & "&OppBizId=" & lintOppBizDocsId & "')")
                    ElseIf CCommon.ToLong(DataBinder.Eval(e.Item.DataItem, "numBillID")) <> 0 Then
                        If CCommon.ToLong(DataBinder.Eval(e.Item.DataItem, "numLandedCostOppId")) <> 0 Then
                            lnkType.Attributes.Add("onclick", "return OpenLandedCost('" & CCommon.ToLong(DataBinder.Eval(e.Item.DataItem, "numLandedCostOppId")) & "')")
                        Else
                            lnkType.Attributes.Add("onclick", "return OpenBill('" & CCommon.ToLong(DataBinder.Eval(e.Item.DataItem, "numBillID")) & "')")
                        End If
                    ElseIf lintCategoryHDRID <> 0 Then

                            Select Case lblTEType.Text
                                Case 0
                                    If lblFromDate.Text <> "" Then
                                        lnkType.Attributes.Add("onclick", "return OpenTimeExpense('" & "../TimeAndExpense/frmAddTimeExp.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=GeneralLedger&CatID=" & lintCategoryHDRID & "&CntID=" & lblUserCntID.Text & "&Date=" & CDate(lblFromDate.Text) & "')")
                                    Else
                                        lnkType.Attributes.Add("onclick", "return OpenTimeExpense('" & "../TimeAndExpense/frmAddTimeExp.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=GeneralLedger&CatID=" & lintCategoryHDRID & "&CntID=" & lblUserCntID.Text & "')")
                                    End If
                                Case 1
                                    If lblCategory.Text = 1 Then
                                        lnkType.Attributes.Add("onclick", "return OpenTimeExpense('" & "../projects/frmProTime.aspx?frm=TE&ProStageID=0&Proid=0&DivId=&Date=&CatID=&CntID=" & lblUserCntID.Text & "&CatHdrId=" & lintCategoryHDRID & "')")
                                    Else
                                        lnkType.Attributes.Add("onclick", "return OpenTimeExpense('" & "../opportunity/frmOppExpense.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=GeneralLedger&CatHdrId=" & lintCategoryHDRID & "&CntID=" & lblUserCntID.Text & "')")
                                    End If
                                Case 2
                                    If lblCategory.Text = 1 Then
                                        lnkType.Attributes.Add("onclick", "return OpenTimeExpense('" & "../projects/frmProTime.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=GeneralLedger&CatHdrId=" & lintCategoryHDRID & "&CntID=" & lblUserCntID.Text & "')")
                                    Else
                                        lnkType.Attributes.Add("onclick", "return OpenTimeExpense('" & "../projects/frmProExpense.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=GeneralLedger&CatHdrId=" & lintCategoryHDRID & "&CntID=" & lblUserCntID.Text & "')")
                                    End If
                                Case 3
                                    If lblCategory.Text = 1 Then
                                        lnkType.Attributes.Add("onclick", "return OpenTimeExpense('" & "../cases/frmCasetime.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=GeneralLedger&CatHdrId=" & lintCategoryHDRID & "&CntID=" & lblUserCntID.Text & "')")
                                    Else
                                        lnkType.Attributes.Add("onclick", "return OpenTimeExpense('" & "../cases/frmCaseExpense.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=GeneralLedger&CatHDRID=" & lintCategoryHDRID & "&CntID=" & lblUserCntID.Text & "')")
                                    End If
                            End Select
                        End If
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub DisplayError(ByVal exception As String)
            Try
                DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = exception
                DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
                DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Focus()
            Catch ex As Exception

            End Try
        End Sub

        Private Sub ShowMessage(ByVal message As String)
            Try
                divMessage.Style.Add("display", "")
                litMessage.Text = message
                divMessage.Focus()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Protected Sub bizPager_PageChanged(sender As Object, e As EventArgs)
            Try
                txtCurrrentPage.Text = bizPager.CurrentPageIndex
                BindGridView(True)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Private Sub lkbBackToSearchCriteria_Click(sender As Object, e As EventArgs) Handles lkbBackToSearchCriteria.Click
            Try
                Session("AdvancedSearchCondition") = ViewState("AdvancedSearchCondition")
                Session("AdvancedSearchSavedSearchID") = ViewState("AdvancedSearchSavedSearchID")
                Response.Redirect("~/admin/frmAdvancedSearchNew.aspx?I=1", False)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub
    End Class

End Namespace