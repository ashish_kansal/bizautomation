<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" CodeBehind="frmSelectTabs.aspx.vb"
    Inherits="BACRM.UserInterface.Admin.frmSelectTabs" MasterPageFile="~/common/PopupBootstrap.Master" %>

<%@ Register Assembly="Telerik.Web.UI, Version=2011.2.712.40, Culture=neutral, PublicKeyToken=121fae78165ba3d4" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Select Tabs</title>
    <style>
        .col-md-6{
            padding-left: 0px;
    padding-right: 0px;
        }
        .button {
    -moz-box-shadow: 0 1px 0 0 #c7c9cf;
    -webkit-box-shadow: 0 1px 0 0 #c7c9cf;
    box-shadow: 0 1px 0 0 #c7c9cf;
    background: -webkit-gradient(linear,left top,left bottom,color-stop(0.05,#fdfefe),color-stop(1,#dce3ea));
    background: -moz-linear-gradient(center top,#fdfefe 5%,#dce3ea 100%);
    filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#fdfefe',endColorstr='#dce3ea');
    background-color: #fdfefe;
    -moz-border-radius: 5px;
    -webkit-border-radius: 5px;
    border-radius: 5px;
    border: 1px solid #cadbed;
    display: inline-block;
    color: #414c6d;
    font-family: Arial;
    padding: 2px 7px;
    text-decoration: none;
    text-shadow: 1px 1px 3px #cadbed;
    font-weight: normal!important;
    font-size: 12px!important;
    cursor: pointer;
}.hs td{
         background: #e1e1e1;
    font-weight: bold;
 }
 fieldset{
     border: 1px solid #1473B4 !important;
         padding: 6px;
 }
 legend{
         width: auto;
         border-bottom:0px !important;
         margin-left: 15px;
         margin-left: 15px;
    font-size: 15px;
    font-weight: bold;
    color: #1473b4;
 }
    </style>
    <script type="text/javascript" src="../javascript/AdvSearchScripts.js"></script>
    <script type="text/javascript">
        sortitems = 0;  // 0-False , 1-True
        function move(fbox, tbox) {

            for (var i = 0; i < fbox.options.length; i++) {
                if (fbox.options[i].selected && fbox.options[i].value != "") {
                    /// to check for duplicates 
                    for (var j = 0; j < tbox.options.length; j++) {
                        if (tbox.options[j].value == fbox.options[i].value) {
                            alert("Item is already selected");
                            return false;
                        }
                    }

                    var no = new Option();
                    no.value = fbox.options[i].value;
                    no.text = fbox.options[i].text;
                    tbox.options[tbox.options.length] = no;
                    fbox.options[i].value = "";
                    fbox.options[i].text = "";

                }
            }
            BumpUp(fbox);
            if (sortitems) SortD(tbox);
            return false;
        }

        function remove1(fbox, tbox) {

            for (var i = 0; i < fbox.options.length; i++) {
                if (fbox.options[i].selected && fbox.options[i].value != "") {
                    /// to check for duplicates 
                    for (var j = 0; j < tbox.options.length; j++) {
                        if (tbox.options[j].value == fbox.options[i].value) {
                            fbox.options[i].value = "";
                            fbox.options[i].text = "";
                            BumpUp(fbox);
                            if (sortitems) SortD(tbox);
                            return false;


                            //alert("Item is already selected");
                            //return false;
                        }
                    }

                    var no = new Option();
                    no.value = fbox.options[i].value;
                    no.text = fbox.options[i].text;
                    tbox.options[tbox.options.length] = no;
                    fbox.options[i].value = "";
                    fbox.options[i].text = "";

                }
            }
            BumpUp(fbox);
            if (sortitems) SortD(tbox);
            return false;
        }

        function BumpUp(box) {
            for (var i = 0; i < box.options.length; i++) {
                if (box.options[i].value == "") {
                    for (var j = i; j < box.options.length - 1; j++) {
                        box.options[j].value = box.options[j + 1].value;
                        box.options[j].text = box.options[j + 1].text;
                    }
                    var ln = i;
                    break;
                }
            }
            if (ln < box.options.length) {
                box.options.length -= 1;
                BumpUp(box);
            }
        }


        function SortD(box) {
            var temp_opts = new Array();
            var temp = new Object();
            for (var i = 0; i < box.options.length; i++) {
                temp_opts[i] = box.options[i];
            }
            for (var x = 0; x < temp_opts.length - 1; x++) {
                for (var y = (x + 1) ; y < temp_opts.length; y++) {
                    if (temp_opts[x].text > temp_opts[y].text) {
                        temp = temp_opts[x].text;
                        temp_opts[x].text = temp_opts[y].text;
                        temp_opts[y].text = temp;
                        temp = temp_opts[x].value;
                        temp_opts[x].value = temp_opts[y].value;
                        temp_opts[y].value = temp;
                    }
                }
            }
            for (var i = 0; i < box.options.length; i++) {
                box.options[i].value = temp_opts[i].value;
                box.options[i].text = temp_opts[i].text;
            }
        }
        function Save() {
            var str = '';
            for (var i = 0; i < document.getElementById("lstAddfld").options.length; i++) {
                var SelectedValue;
                SelectedValue = document.getElementById("lstAddfld").options[i].value;
                str = str + SelectedValue + ','
            }
            document.getElementById("txthidden").value = str;
        }
        function NoDeleteMessage() {
            alert("You Are not Authorized to Delete the Selected Record !");
            return false;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
   <div class="pull-right">
        <asp:Button ID="btnClose" runat="server" Text="Cancel" CssClass="btn btn-primary"></asp:Button>
   </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageTitle" runat="server" ClientIDMode="Static">
    Edit Labels & Hide/Show
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="Content" runat="server" ClientIDMode="Static">
    <asp:ScriptManager runat="server"></asp:ScriptManager>
    <div class="col-md-6">
        <fieldset>
    <legend>Edit Main Tab Labels</legend>
        <div class="col-md-12" style="    padding-left: 0px;
    padding-right: 0px;">
        <asp:Literal ID="litMessage" runat="server" EnableViewState="False"></asp:Literal>
        <div class="clear"></div>
        <div class="form-inline">
            <label>Tab Name</label>
            <asp:TextBox ID="txtTabName" runat="server" CssClass="signup"></asp:TextBox>
            <label>Group</label>
             <asp:DropDownList ID="ddlTabType" runat="server" AutoPostBack="true" Width="130"
                    CssClass="signup">
                    <asp:ListItem Text="Internal Application" Value="1"></asp:ListItem>
                    <asp:ListItem Text="Customer Portal" Value="2"></asp:ListItem>
                    <asp:ListItem Text="PRM" Value="3"></asp:ListItem>
                </asp:DropDownList>
            <label>URL</label>
             <asp:TextBox ID="txtURL"  runat="server" CssClass="signup"></asp:TextBox>&nbsp;
                <asp:Button ID="btnAddNewTab" runat="server" CssClass="button" Text="Add" Width="40" />
        </div>
    </div>
    <div class="col-md-12">
             <asp:DataGrid ID="dgTabs" runat="server" CssClass="table table-bordered"  AutoGenerateColumns="False"
                    AllowSorting="True" Width="100%">
                    <AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
                    <ItemStyle CssClass="is"></ItemStyle>
                    <HeaderStyle CssClass="hs"></HeaderStyle>
                    <Columns>
                        <asp:BoundColumn Visible="false" DataField="numTabId"></asp:BoundColumn>
                        <asp:BoundColumn Visible="false" DataField="bitFixed"></asp:BoundColumn>
                        <asp:TemplateColumn>
                            <ItemTemplate>
                                <asp:LinkButton runat="server" Text="Edit" CommandName="Edit" ID="lnkbtnEdt"></asp:LinkButton>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:LinkButton ID="lnkbtnUpdt" runat="server" Text="Update" CommandName="Update"></asp:LinkButton>&nbsp;
                                <asp:LinkButton runat="server" Text="Cancel" CommandName="Cancel" ID="lnkbtnCncl"></asp:LinkButton>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn>
                            <ItemTemplate>
                                <asp:Label ID="lblTabID" runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.numTabId") %>'>
                                </asp:Label>
                                <asp:Label ID="lblFixed" runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.bitFixed") %>'>
                                </asp:Label>
                                <%# Container.ItemIndex + 1%>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Tab">
                            <ItemTemplate>
                                <asp:Label ID="lblTab" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.numTabName") %>'>
                                </asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtETab" runat="server" CssClass="signup" Text='<%# DataBinder.Eval(Container, "DataItem.numTabName") %>'>
                                </asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="URL">
                            <ItemTemplate>
                                <asp:Label ID="lblURL" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.vcURL") %>'>
                                </asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtEURL" runat="server" CssClass="signup" Width="150" Text='<%# DataBinder.Eval(Container, "DataItem.vcURL") %>'>
                                </asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderStyle-Width="10" ItemStyle-Width="10">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkdelete" runat="server" Visible="false">
											        <span style="color:#730000">*</span></asp:LinkButton>
                                <asp:Button ID="btnDelete" runat="server" CssClass="button Delete" Text="X" CommandName="Delete">
                                </asp:Button>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                </asp:DataGrid>
    </div>
            </fieldset>
    </div>
    <div class="col-md-6" style="padding-left:5px;">
         <fieldset>
    <legend>Tab Authorization</legend>
         <div class="input-part">
        <div class="right-input">
            <div class="form-inline pull-right">
                <div class="form-group">
                    <label>Apply configuration to</label>
                    <telerik:RadComboBox ID="rcbGroups" runat="server" CheckBoxes="true"></telerik:RadComboBox>
                </div>
                <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="button"></asp:Button>
                <asp:Button ID="btnSaveClose" runat="server" Text="Save &amp; Close" CssClass="button"></asp:Button>
               
            </div>
        </div>
    </div>
    <div class="row padbottom10">
        <div class="col-xs-12 form-horizontal">
            <div class="form-group">
                    <label class="col-xs-2 control-label">Group</label>
                    <div class="col-xs-10">
                        <asp:DropDownList ID="ddlGroup" runat="server" AutoPostBack="True" CssClass="form-control"></asp:DropDownList>
                    </div>
                </div>
            <div class="form-group" id="divRelationship" runat="server">
                    <label class="col-xs-2 control-label">Relationship</label>
                    <div class="col-xs-10">
                        <asp:DropDownList ID="ddlRelationship" AutoPostBack="true" runat="server" CssClass="form-control"></asp:DropDownList>
                    </div>
                </div>
            <div class="form-group" id="divProfile" runat="server">
                    <label class="col-xs-2 control-label">Profile</label>
                    <div class="col-xs-10">
                        <asp:DropDownList ID="ddlProfile" AutoPostBack="true" runat="server" CssClass="form-control"></asp:DropDownList>
                    </div>
                </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <table style="width: 100%" class="table-responsive tblNoBorder">
                <tr>
                    <td style="text-align: center" class="normal1"><b>Available Tabs</b><br />
                        <asp:ListBox ID="lstAvailablefld" runat="server" Width="250" Height="300" CssClass="signup"></asp:ListBox>
                    </td>
                    <td style="text-align: center; vertical-align: middle">
                        <asp:Button ID="btnAdd" CssClass="btn btn-primary" runat="server" Text="Add >"></asp:Button>
                        <br />
                        <br />
                        <asp:Button ID="btnRemove" CssClass="btn btn-danger" runat="server" Text="< Remove"></asp:Button>
                    </td>
                    <td style="text-align: center" class="normal1"><b>Selected Tabs</b><br />
                        <asp:ListBox ID="lstAddfld" runat="server" Width="250" Height="300" CssClass="signup"></asp:ListBox>
                    </td>
                    <td style="text-align: center; vertical-align: middle">
                        <a id="btnMoveup" class="btn btn-primary" onclick="javascript:MoveUp(document.getElementById('lstAddfld'));"><i class="fa fa-arrow-up"></i></a>
                        <br />
                        <br />
                        <a id="btnMoveDown" class="btn btn-primary" onclick="javascript:MoveDown(document.getElementById('lstAddfld'));"><i class="fa fa-arrow-down"></i></a>
                    </td>
                </tr>
            </table>
        </div>
    </div>
            </fieldset>

    <asp:TextBox Style="display: none" ID="txthidden" runat="server"></asp:TextBox>
        </div>
</asp:Content>
