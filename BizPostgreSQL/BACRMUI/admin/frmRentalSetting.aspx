﻿<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" CodeBehind="frmRentalSetting.aspx.vb"
    Inherits="BACRM.UserInterface.Admin.frmRentalSetting" MasterPageFile="~/common/PopupBootstrap.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Rental Setting</title>
    <style>
        #table9 tr td{
            border:0px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <div class=" pull-right">
         <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btn btn-primary" OnClientClick="return Save();"></asp:Button>
                    <asp:Button ID="btnClose" runat="server" CssClass="btn btn-primary"  OnClientClick="return Close()"  Text="Close"></asp:Button>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageTitle" runat="server" ClientIDMode="Static">
    Rental Setting
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="Content" runat="server" ClientIDMode="Static">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <asp:Table ID="table9" runat="server">
                        <asp:TableRow>
                            <asp:TableCell VerticalAlign="Top">
                                <table class="table table-responsive tblNoBorder">
                                    <tr>
                                        <td class="Feature" align="right">
                                            <%--Rental Items Settings--%>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="normal1" align="right">&nbsp;
                                        </td>
                                        <td class="normal1" align="left">
                                            <asp:CheckBox ID="chkRental" runat="server" />
                                            Rental items belong to the following item classification
                                    <asp:DropDownList ID="ddlRentalItemClass" runat="server" CssClass="signup">
                                    </asp:DropDownList>
                                            <asp:Label ID="Label42" Text="[?]" CssClass="tip" runat="server" ToolTip="This setting (defined in item details) allows Biz to recognize a “rental item”, which is treated differently from other items during the order management process." />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="normal1" align="right">&nbsp;
                                        </td>
                                        <td class="normal1" align="left">Rental items Hourly UOM
                                    <asp:DropDownList ID="ddlRentalHourlyUOM" runat="server" CssClass="signup">
                                    </asp:DropDownList>
                                            &nbsp;&nbsp; Rental items Daily UOM
                                    <asp:DropDownList ID="ddlRentalDailyUOM" runat="server" CssClass="signup">
                                    </asp:DropDownList>
                                            <asp:Label ID="Label43" Text="[?]" CssClass="tip" runat="server" ToolTip="Units of measure are set from within this section (see Configure Units within this setting page). If you rent items by the hour, select hour (even if you also rent them by the day), otherwise only select daily. " />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="normal1" align="right">&nbsp;
                                        </td>
                                        <td class="normal1" align="left">Rental items price based on
                                    <asp:RadioButtonList ID="rblRentalPriceBasedOn" runat="server" CssClass="signup"
                                        RepeatDirection="Horizontal" RepeatLayout="Flow">
                                        <asp:ListItem Text="Hourly" Value="1" Selected="True">
                                        </asp:ListItem>
                                        <asp:ListItem Text="Daily" Value="2">
                                        </asp:ListItem>
                                    </asp:RadioButtonList>
                                        </td>
                                    </tr>
                                </table>
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
</asp:Content>