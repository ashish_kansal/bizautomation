Imports BACRM.BusinessLogic.Common
Partial Class frmLinks
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Dim objCommon As New CCommon()

            If ConfigurationManager.AppSettings("Help") = "Local" Then

                Response.Redirect("http://localhost/Help/?pageurl=" & CCommon.ToString(Session("Help")) & "&a=" & objCommon.Encrypt(Session("AccessID")))

            ElseIf ConfigurationManager.AppSettings("Help") = "Staging" Then

                Response.Redirect("http://bizautomation.dyndns.biz/Help/?pageurl=" & CCommon.ToString(Session("Help")) & "&a=" & objCommon.Encrypt(Session("AccessID")))

            Else

                Response.Redirect("http://help.bizautomation.com/?pageurl=" & CCommon.ToString(Session("Help")) & "&a=" & objCommon.Encrypt(Session("AccessID")))

            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

End Class
