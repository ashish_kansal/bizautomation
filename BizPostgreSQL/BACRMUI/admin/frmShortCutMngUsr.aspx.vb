Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Admin
Partial Public Class frmShortCutMngUsr
    Inherits BACRMPage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            btnAddFav.Attributes.Add("OnClick", "return move(document.form1.lstAvailablefldFav,document.form1.lstAddfldFav)")
            btnRemoveFav.Attributes.Add("OnClick", "return remove1(document.form1.lstAddfldFav,document.form1.lstAvailablefldFav)")
            btnMoveDownFav.Attributes.Add("OnClick", "return MoveDown(document.form1.lstAddfldFav)")
            btnMoveupFav.Attributes.Add("OnClick", "return MoveUp(document.form1.lstAddfldFav)")
            btnSave.Attributes.Add("OnClick", "return Save()")
            btnSaveClose.Attributes.Add("OnClick", "return Save()")
            btnAdd.Attributes.Add("OnClick", "return Check()")
            If Not IsPostBack Then

                BindTab()

                If rblTabEvent.Items.FindByValue(Session("TabEvent")) IsNot Nothing Then
                    rblTabEvent.ClearSelection()
                    rblTabEvent.Items.FindByValue(Session("TabEvent")).Selected = True
                End If

                Dim lngTab As Long = CCommon.ToLong(GetQueryStringVal("tab"))
                If lngTab > 0 Then
                    If ddlTab.Items.FindByValue(lngTab) IsNot Nothing Then
                        ddlTab.ClearSelection()
                        ddlTab.Items.FindByValue(lngTab).Selected = True
                    End If
                End If

                PopulateData()
                BindGrid()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
    Sub BindTab()
        Try
            Dim dtTabData As DataTable
            Dim objTab As New Tabs
            objTab.GroupID = Session("UserGroupID")
            objTab.RelationshipID = 0
            objTab.DomainID = Session("DomainID")
            dtTabData = objTab.GetTabData()

            ddlTab.DataSource = dtTabData
            ddlTab.DataTextField = "numTabName"
            ddlTab.DataValueField = "numTabID"
            ddlTab.DataBind()

            ddlTab.Items.Insert(0, New ListItem("--Select One--", "0"))
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub ddlTab_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlTab.SelectedIndexChanged
        Try
            PopulateData()
            BindGrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Sub PopulateData()
        Try
            Dim objUserGroups As New CShortCutBar
            objUserGroups.GroupId = Session("UserGroupID")
            objUserGroups.DomainId = Session("DomainID")
            objUserGroups.ContactId = Session("UserContactID")
            objUserGroups.TabId = ddlTab.SelectedValue

            Dim ds As New DataSet
            ds = objUserGroups.GetAvailableFieldsUser()
            Dim dtTable1 As DataTable = ds.Tables(0)
            Dim dtTable2 As DataTable = ds.Tables(1)
            Dim dtTab As DataTable
            dtTab = Session("DefaultTab")
            For Each row As DataRow In dtTable1.Rows
                Select Case row.Item("VcLinkName").ToString
                    Case "MyLeads"
                        row.Item("VcLinkName") = "My " & dtTab.Rows(0).Item("vcLead").ToString & "s"
                    Case "WebLeads"
                        row.Item("VcLinkName") = "Web " & dtTab.Rows(0).Item("vcLead").ToString & "s"
                    Case "PublicLeads"
                        row.Item("VcLinkName") = "Public " & dtTab.Rows(0).Item("vcLead").ToString & "s"
                    Case "Contacts"
                        row.Item("VcLinkName") = dtTab.Rows(0).Item("vcContact").ToString & "s"
                    Case "Prospects"
                        row.Item("VcLinkName") = dtTab.Rows(0).Item("vcProspect").ToString & "s"
                    Case "Accounts"
                        row.Item("VcLinkName") = dtTab.Rows(0).Item("vcAccount").ToString & "s"
                End Select
            Next
            For Each row As DataRow In dtTable2.Rows
                Select Case row.Item("VcLinkName").ToString
                    Case "MyLeads"
                        row.Item("VcLinkName") = "My " & dtTab.Rows(0).Item("vcLead").ToString & "s"
                    Case "WebLeads"
                        row.Item("VcLinkName") = "Web " & dtTab.Rows(0).Item("vcLead").ToString & "s"
                    Case "PublicLeads"
                        row.Item("VcLinkName") = "Public " & dtTab.Rows(0).Item("vcLead").ToString & "s"
                    Case "Contacts"
                        row.Item("VcLinkName") = dtTab.Rows(0).Item("vcContact").ToString & "s"
                    Case "Prospects"
                        row.Item("VcLinkName") = dtTab.Rows(0).Item("vcProspect").ToString & "s"
                    Case "Accounts"
                        row.Item("VcLinkName") = dtTab.Rows(0).Item("vcAccount").ToString & "s"
                End Select

                Dim str As String = ""
                If CCommon.ToBool(row.Item("bitInitialPage")) = True Then
                    str = "(*) "
                End If

                If CCommon.ToBool(row.Item("bitFavourite")) = True Then
                    str += "(#) "
                End If

                row.Item("VcLinkName") = str & row.Item("VcLinkName")
            Next

            dtTable1.AcceptChanges()
            dtTable2.AcceptChanges()
            lstAvailablefldFav.DataSource = dtTable1
            lstAvailablefldFav.DataTextField = "VcLinkName"
            lstAvailablefldFav.DataValueField = "Id"
            lstAvailablefldFav.DataBind()

            lstAddfldFav.DataSource = dtTable2
            lstAddfldFav.DataTextField = "VcLinkName"
            lstAddfldFav.DataValueField = "Id"
            lstAddfldFav.DataBind()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            Save()
            PopulateData()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Sub Save()
        Try

            objCommon.Mode = 20
            objCommon.UpdateRecordID = Session("UserContactID")
            objCommon.UpdateValueID = rblTabEvent.SelectedValue
            objCommon.UpdateSingleFieldValue()
            Session("TabEvent") = rblTabEvent.SelectedValue

            Dim strFav As String
            Dim strInitialPage = CCommon.ToString(txtInitialPage.Text)


            Dim dtFav As New DataTable
            Dim i As Integer
            Dim dr As DataRow

            dtFav.Columns.Add("numLinkId")
            dtFav.Columns.Add("numOrder")
            dtFav.Columns.Add("bitInitialPage")
            dtFav.Columns.Add("tintLinkType")
            dtFav.Columns.Add("bitFavourite")

            Dim strValesFav As String()
            strValesFav = txthiddenFav.Text.Split(",")

            Dim strFavouriteLink As String()
            strFavouriteLink = txtFavouriteLink.Text.Split(",")

            Dim strValesLink As String()

            Dim ds As New DataSet
            For i = 0 To strValesFav.Length - 2
                dr = dtFav.NewRow
                strValesLink = strValesFav(i).Split("~")

                dr("numLinkId") = strValesLink(0)
                dr("tintLinkType") = strValesLink(1)

                dr("numOrder") = i + 1
                dr("bitInitialPage") = IIf(strValesFav(i) = strInitialPage, True, False)

                dr("bitFavourite") = IIf(strValesFav(i) = strFavouriteLink(i), True, False)
                dtFav.Rows.Add(dr)
            Next

            dtFav.TableName = "Table"
            ds.Tables.Add(dtFav.Copy)
            strFav = ds.GetXml
            ds.Tables.Remove(ds.Tables(0))

            Dim objShortCutBar As New CShortCutBar
            objShortCutBar.GroupId = Session("UserGroupID")
            objShortCutBar.DomainId = Session("DomainID")
            objShortCutBar.ContactId = Session("UserContactID")
            objShortCutBar.TabId = ddlTab.SelectedValue
            objShortCutBar.StrFav = strFav
            Dim Saved As Boolean
            Saved = objShortCutBar.SaveDisplayFieldsUser()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        Try
            Dim objShortCutBar As New CShortCutBar
            objShortCutBar.GroupId = Session("UserGroupID")
            objShortCutBar.DomainId = Session("DomainID")
            objShortCutBar.strName = txtName.Text
            objShortCutBar.strUrl = txtUrl.Text
            objShortCutBar.ContactId = Session("UserContactID")
            objShortCutBar.TabId = ddlTab.SelectedValue
            objShortCutBar.SaveCustomFields()
            BindGrid()
            PopulateData()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Sub BindGrid()
        Try
            Dim dtTable As DataTable
            Dim objShortCutBar As New CShortCutBar
            objShortCutBar.GroupId = Session("UserGroupID")
            objShortCutBar.DomainId = Session("DomainID")
            objShortCutBar.ContactId = Session("UserContactID")
            objShortCutBar.TabId = ddlTab.SelectedValue
            dtTable = objShortCutBar.GetCustomFldsGrp
            dgCustomFields.DataSource = dtTable
            dgCustomFields.DataBind()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub dgCustomFields_EditCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgCustomFields.EditCommand
        Try
            dgCustomFields.EditItemIndex = e.Item.ItemIndex
            Call BindGrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub dgCustomFields_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgCustomFields.ItemCommand
        Try
            If e.CommandName = "Cancel" Then
                dgCustomFields.EditItemIndex = e.Item.ItemIndex
                dgCustomFields.EditItemIndex = -1
                Call BindGrid()
            End If

            If e.CommandName = "Delete" Then
                Dim objShortCutBar As New CShortCutBar
                objShortCutBar.GroupId = Session("UserGroupID")
                objShortCutBar.DomainId = Session("DomainID")

                objShortCutBar.LinkId = CType(e.Item.FindControl("lblListItemID"), Label).Text
                objShortCutBar.strName = ""
                objShortCutBar.strUrl = ""
                objShortCutBar.ByteMode = 0
                objShortCutBar.EditDeleteCustomFldGrp()
                Call BindGrid()
                PopulateData()
            End If
            If e.CommandName = "Update" Then
                Dim objShortCutBar As New CShortCutBar
                objShortCutBar.GroupId = Session("UserGroupID")
                objShortCutBar.DomainId = Session("DomainID")

                objShortCutBar.LinkId = CType(e.Item.FindControl("lblListItemID"), Label).Text
                objShortCutBar.strName = CType(e.Item.FindControl("txtDgName"), TextBox).Text
                objShortCutBar.strUrl = CType(e.Item.FindControl("txtDgUrl"), TextBox).Text
                objShortCutBar.ByteMode = 1
                objShortCutBar.EditDeleteCustomFldGrp()
                '
                'objCommon.ListItemID = CType(e.Item.FindControl("lblListItemID"), Label).Text
                'objCommon.ListItemName = CType(e.Item.FindControl("txtEItemName"), TextBox).Text
                'objCommon.UserCntID = Session("UserContactID")
                'objCommon.ManageItemList()
                dgCustomFields.EditItemIndex = -1
                Call BindGrid()
                PopulateData()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub btnSaveClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveClose.Click
        Try
            Save()
            Response.Write("<script>opener.Click_Button(); self.close();</script>")
            'Dim lstr As String
            'lstr = "<script language=javascript>"
            'lstr += "window.opener('Click_Button');"
            'lstr += "</script>"
            'Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "NewAccounts", lstr)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub


End Class