﻿Imports BACRM.BusinessLogic.Common
Imports System.IO
Imports BACRM.BusinessLogic.Accounting

Partial Public Class frmExportData
    Inherits BACRMPage
    Dim mobjGeneralLedger As GeneralLedger

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ""
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "none")

            If Not IsPostBack Then

                If mobjGeneralLedger Is Nothing Then mobjGeneralLedger = New GeneralLedger
                mobjGeneralLedger.DomainID = CCommon.ToLong(Session("DomainID"))
                mobjGeneralLedger.Year = CInt(Now.Year)

                calFrom.SelectedDate = mobjGeneralLedger.GetFiscalDate()
                calTo.SelectedDate = DateAdd(DateInterval.Minute, -Session("ClientMachineUTCTimeOffset"), Date.UtcNow)

                BindData()
                If Date.Now.Hour >= 18 And Date.Now.Hour <= 22 Then
                    btnSave.Visible = True
                    lblMsg.Visible = False
                    'Else
                    '    btnSave.Visible = False
                    '    lblMsg.Visible = True
                End If
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSave.Click
        Dim strZipFile As String
        Dim files As New ArrayList
        Try
            Dim strExportTable As String = ""
            For Each Item As ListItem In cblExport.Items
                If Item.Selected Then
                    strExportTable = strExportTable + Item.Value + ","
                End If
            Next
            strExportTable = strExportTable.TrimEnd(",")

            Dim objExport As New CSVExport
            objExport.DomainID = Session("DomainID")
            objExport.ExportTables = strExportTable
            objExport.ManageExportSettings()

            Dim ds As DataSet
            Dim fileName As String
            Dim dt As DataTable
            Dim fs As FileStream
            Dim sw As StreamWriter

            For Each Item As ListItem In cblExport.Items
                If Item.Selected Then
                    objExport.Table = Item.Value
                    objExport.FromDate = calFrom.SelectedDate
                    objExport.ToDate = calTo.SelectedDate
                    ds = objExport.ExportData()

                    Select Case Item.Value
                        Case 1
                            dt = ds.Tables(0)
                            CreateColumnInDataTable(ds.Tables(1), dt)

                            For Each dRow As DataRow In dt.Rows
                                For Each dr1 As DataRow In ds.Tables(2).Rows
                                    If dr1("RecId") = dRow("DivisionID") Then
                                        dRow(dr1("Fld_label").ToString) = dr1("Fld_Value").ToString
                                    End If
                                Next
                            Next
                            dt.AcceptChanges()

                            fileName = ConfigurationManager.AppSettings("PortalLocation") & "\Documents\Docs\Organization_" & CCommon.ToLong(Session("DomainID")) & ".csv"
                            files.Add(fileName)
                            fs = New FileStream(fileName, FileMode.Create)
                            sw = New StreamWriter(fs)
                            CSVExport.ProduceCSV(dt, sw, True)
                        Case 2
                            dt = ds.Tables(0)

                            CreateColumnInDataTable(ds.Tables(2), dt)

                            For Each dRow As DataRow In dt.Rows
                                For Each dr1 As DataRow In ds.Tables(3).Rows
                                    If dr1("RecId") = dRow("OpportunityID") Then
                                        If dt.Columns.Contains(dr1("Fld_label").ToString) Then
                                            dRow(dr1("Fld_label").ToString) = dr1("Fld_Value").ToString
                                        End If
                                    End If
                                Next
                            Next
                            dt.AcceptChanges()

                            fileName = ConfigurationManager.AppSettings("PortalLocation") & "\Documents\Docs\Opporunity_" & CCommon.ToLong(Session("DomainID")) & ".csv"
                            files.Add(fileName)
                            fs = New FileStream(fileName, FileMode.Create)
                            sw = New StreamWriter(fs)
                            CSVExport.ProduceCSV(dt, sw, True)
                            'OpportunityItems
                            If Not ds.Tables(1) Is Nothing Then
                                fileName = ConfigurationManager.AppSettings("PortalLocation") & "\Documents\Docs\OpporunityItems_" & CCommon.ToLong(Session("DomainID")) & ".csv"
                                files.Add(fileName)
                                fs = New FileStream(fileName, FileMode.Create)
                                sw = New StreamWriter(fs)
                                CSVExport.ProduceCSV(ds.Tables(1), sw, True)
                            End If
                        Case 3 'Projects
                            dt = ds.Tables(0)
                            CreateColumnInDataTable(ds.Tables(1), dt)

                            For Each dRow As DataRow In dt.Rows
                                For Each dr1 As DataRow In ds.Tables(2).Rows
                                    If dr1("RecId") = dRow("ProjID") Then
                                        dRow(dr1("Fld_label").ToString) = dr1("Fld_Value").ToString
                                    End If
                                Next
                            Next
                            dt.AcceptChanges()

                            fileName = ConfigurationManager.AppSettings("PortalLocation") & "\Documents\Docs\Projects_" & CCommon.ToLong(Session("DomainID")) & ".csv"
                            files.Add(fileName)
                            fs = New FileStream(fileName, FileMode.Create)
                            sw = New StreamWriter(fs)
                            CSVExport.ProduceCSV(dt, sw, True)
                        Case 4 'Cases
                            dt = ds.Tables(0)
                            CreateColumnInDataTable(ds.Tables(1), dt)

                            For Each dRow As DataRow In dt.Rows
                                For Each dr1 As DataRow In ds.Tables(2).Rows
                                    If dr1("RecId") = dRow("CaseID") Then
                                        dRow(dr1("Fld_label").ToString) = dr1("Fld_Value").ToString
                                    End If
                                Next
                            Next
                            dt.AcceptChanges()

                            fileName = ConfigurationManager.AppSettings("PortalLocation") & "\Documents\Docs\Cases_" & CCommon.ToLong(Session("DomainID")) & ".csv"
                            files.Add(fileName)
                            fs = New FileStream(fileName, FileMode.Create)
                            sw = New StreamWriter(fs)
                            CSVExport.ProduceCSV(dt, sw, True)
                        Case 5
                            dt = ds.Tables(0)
                            fileName = ConfigurationManager.AppSettings("PortalLocation") & "\Documents\Docs\KnowledgeBase_" & CCommon.ToLong(Session("DomainID")) & ".csv"
                            files.Add(fileName)
                            fs = New FileStream(fileName, FileMode.Create)
                            sw = New StreamWriter(fs)
                            CSVExport.ProduceCSV(dt, sw, True)
                        Case 6
                            dt = ds.Tables(0)
                            fileName = ConfigurationManager.AppSettings("PortalLocation") & "\Documents\Docs\Contracts_" & CCommon.ToLong(Session("DomainID")) & ".csv"
                            files.Add(fileName)
                            fs = New FileStream(fileName, FileMode.Create)
                            sw = New StreamWriter(fs)
                            CSVExport.ProduceCSV(dt, sw, True)
                        Case 7
                            dt = ds.Tables(0)
                            fileName = ConfigurationManager.AppSettings("PortalLocation") & "\Documents\Docs\GeneralTransactions_" & CCommon.ToLong(Session("DomainID")) & ".csv"
                            files.Add(fileName)
                            fs = New FileStream(fileName, FileMode.Create)
                            sw = New StreamWriter(fs)
                            CSVExport.ProduceCSV(dt, sw, True)
                        Case 8
                            dt = ds.Tables(0)
                            CreateColumnInDataTable(ds.Tables(2), dt)
                            'Try
                            For Each dRow As DataRow In dt.Rows
                                For Each dr1 As DataRow In ds.Tables(3).Rows
                                    If dr1("RecId") = dRow("Item ID") Then
                                        If dt.Columns.Contains(dr1("Fld_label").ToString) Then
                                            dRow(dr1("Fld_label").ToString) = dr1("Fld_Value").ToString
                                        End If
                                    End If
                                Next
                            Next
                            'Catch ex As Exception

                            'End Try
                            dt.AcceptChanges()

                            fileName = ConfigurationManager.AppSettings("PortalLocation") & "\Documents\Docs\Items_" & CCommon.ToLong(Session("DomainID")) & ".csv"
                            files.Add(fileName)
                            fs = New FileStream(fileName, FileMode.Create)
                            sw = New StreamWriter(fs)
                            CSVExport.ProduceCSV(dt, sw, True)

                            'Vendors
                            fileName = ConfigurationManager.AppSettings("PortalLocation") & "\Documents\Docs\Vendors_" & CCommon.ToLong(Session("DomainID")) & ".csv"
                            files.Add(fileName)
                            fs = New FileStream(fileName, FileMode.Create)
                            sw = New StreamWriter(fs)
                            CSVExport.ProduceCSV(ds.Tables(1), sw, True)
                        Case 9
                            dt = ds.Tables(0)
                            fileName = ConfigurationManager.AppSettings("PortalLocation") & "\Documents\Docs\ChartOfAccounts_" & CCommon.ToLong(Session("DomainID")) & ".csv"
                            files.Add(fileName)
                            fs = New FileStream(fileName, FileMode.Create)
                            sw = New StreamWriter(fs)
                            CSVExport.ProduceCSV(dt, sw, True)
                        Case Else

                    End Select

                End If
            Next



            strZipFile = objExport.ZipFiles(files)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
            Exit Sub
        End Try
        Response.Clear()
        Response.ContentType = "application/octet-stream"
        Response.AddHeader("pragma", "no-cache")
        Response.AddHeader("Content-Disposition", "inline; filename=ExportedData_" & Date.Now.ToShortDateString & ".zip")
        Response.WriteFile(strZipFile, True)

        File.Delete(strZipFile)

        For i As Integer = 0 To files.Count - 1
            File.Delete(files(i).ToString)
        Next

        Response.End()
    End Sub
    Private Sub BindData()
        Try
            Dim objExport As New CSVExport
            Dim ds As DataSet
            objExport.DomainID = Session("DomainID")
            ds = objExport.GetExportDataSettings()
            If ds.Tables.Count = 1 Then
                If ds.Tables(0).Rows.Count > 0 Then
                    For Each dr As DataRow In ds.Tables(0).Rows
                        If Not cblExport.Items.FindByValue(dr("ID").ToString) Is Nothing Then
                            cblExport.Items.FindByValue(dr("ID").ToString).Selected = True
                        End If
                    Next
                    litLastExport.Text = "Last date of export: " & ds.Tables(0).Rows(0)("dtLastExportedOn")
                End If
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Sub CreateColumnInDataTable(ByVal SourceTable As DataTable, ByRef DestinationTable As DataTable)
        Try
            For Each dr As DataRow In SourceTable.Rows
                If DestinationTable.Columns.Contains(dr("Fld_label").ToString) Then
                    'prevent duplicate column error.. append duplicate column with number 
                    For i As Int16 = 1 To 20
                        If Not DestinationTable.Columns.Contains(dr("Fld_label").ToString & i.ToString) Then
                            DestinationTable.Columns.Add(dr("Fld_label").ToString & i.ToString)
                            Exit For
                        End If
                    Next
                Else
                    DestinationTable.Columns.Add(dr("Fld_label").ToString)
                End If
            Next
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub btnBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBack.Click
        Try
            If GetQueryStringVal("frm") = "Import" Then
                Response.Redirect("../Items/frmImportFiles.aspx?Mode=2", False)
            Else
                Response.Redirect("frmDomainDetails.aspx", False)
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Private Sub DisplayError(ByVal ex As String)
        DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ex
        DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
    End Sub
End Class