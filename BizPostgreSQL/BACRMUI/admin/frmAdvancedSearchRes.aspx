<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmAdvancedSearchRes.aspx.vb"
    EnableEventValidation="false" Inherits="BACRM.UserInterface.Admin.frmAdvancedSearchRes"
    MasterPageFile="~/common/GridMasterRegular.Master" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Assembly="AspNetPager, Version=7.4.4.0, Culture=neutral, PublicKeyToken=fb0a0fe055d40fd4" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>
<%@ Register Src="../common/frmBizSorting.ascx" TagName="frmBizSorting" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <meta http-equiv="Page-Enter" content="blendTrans(Duration=0.01)">
    <meta http-equiv="Page-Exit" content="blendTrans(Duration=0.01)">
    <title>Advanced Search</title>
    <script type="text/javascript">
        function Ownership() {
            var str = ''
            if (document.getElementById('chkSelectAll').checked) {
                str = '?mode=1'
            }
            else {
                var RecordIDs = GetCheckedRowValues()
                if (RecordIDs.length > 0) {
                    str = '?mode=2&CntIds=' + RecordIDs
                }
                else {
                    alert('Please select atleast one record!!');
                    return false;
                }
            }

            var pop = window.open('../admin/frmTransfer.aspx' + str, 'Transfer', "width=340,height=150,status=no,top=100,left=150");
            pop.focus()
            return false;
            return false
        }

        function CreateSales() {
            var str = '';
            var RecordIDs = GetCheckedRowValuesMassSalesOrder();
            if (RecordIDs.length > 0) {
                str = '?OppType=1&OppStatus=1&bulk=1&CntIds=' + RecordIDs
            }
            else {
                alert('Please select atleast one record!!');
                return false;
            }

            var pop = window.open('../opportunity/frmNewOrder.aspx' + str, 'Create Sales', "width=1200,height=645,status=no,top=100,left=100");
            pop.focus()
            return false;

        }

        function EditSearchView() {
            window.open('../admin/frmAdvSearchColumnCustomization.aspx', "", "width=500,height=300,status=no,scrollbars=yes,left=155,top=160")
            return false;
        }


        function MassUpdate() {
            window.open('../admin/frmAdvSearchMassUpdater.aspx?FormID=1', "", "width=500,height=400,status=no,scrollbars=yes,left=155,top=160")
            return false;
        }

        function MassDelete(a) {
            document.getElementById("txtReload").value = true;
            str = 'Important !  You are about to delete all the records selected.'
            if (confirm(str) == true) {
                document.getElementById('hdnDeleteMode').value = a;
                document.getElementById('btnMassDelete').click();
                return true;
            }
            else {
                return false;
            }
        }

        function PMessage() {
            document.getElementById("txtReload").value = true;
        }

        function OpenWindow(a, b, c, d) {
            var str;
            if (b == 0) {
                str = "../Leads/frmLeads.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=AdvSearch&DivID=" + a;
            }
            else if (b == 1) {
                str = "../prospects/frmProspects.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=AdvSearch&DivID=" + a;
            }
            else if (b == 2) {
                str = "../account/frmAccounts.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=AdvSearch&klds+7kldf=fjk-las&DivId=" + a;
            }
            if (d == 1) {
                window.open(str);
            }
            else {
                document.location.href = str;
            }
        }

        function OpenContact(a, b, c) {
            var str;
            str = "../contact/frmContacts.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=AdvSearch&ft6ty=oiuy&CntId=" + a;

            if (c == 1) {
                window.open(str);
            }
            else {
                document.location.href = str;
            }
        }

        function FilterWithinRecords(labelName, DBColumnName, divName) {
            document.getElementById('spColName').innerHTML = labelName.replace(/\+/g, " ");
            document.getElementById('txtColumnName').value = DBColumnName;

            $("[id$=" + divName + "]").modal('show');

            document.getElementById('txtColValue').focus();
        }

        function HideDivElement(divName) {
            $("[id$=" + divName + "]").modal('hide');
        }

        function getContacts() {
            if (document.getElementById("chkSelectAll").checked == true) {
                window.open('../admin/frmAddEmailGroup.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&SelectAll=true&GetPC=' + document.getElementById("cbPrimaryContactsOnly").checked, "", "width=1000,height=700,status=no,scrollbars=yes,left=155,top=100")
                return false;
            }
            else {
                var RecordIDs = GetCheckedRowValues()
                if (RecordIDs.length > 0) {
                    window.open('../admin/frmAddEmailGroup.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&SelectAll=false&strContID=' + RecordIDs, "", "width=1000,height=700,status=no,scrollbars=yes,left=155,top=100")
                }
                else {
                    alert('Please select atleast one record!!');
                }

                return false;
            }
        }

        function OpemEmail(a, b) {
            window.open('../contact/frmComposeWindow.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&Lsemail=' + a + '&pqwRT=' + b, 'ComposeWindow', 'toolbar=no,titlebar=no,top=100,left=100,width=1200,height=645,scrollbars=yes,resizable=yes')
            return false;
        }

        function UpdateValues() {
            document.getElementById("txtReload").value = true;
            document.getElementById("btnUpdateValues").click()
        }

        function UpdateDripCampaign() {
            document.getElementById("txtReload").value = true;
            document.getElementById("btnUpdateDripCampaign").click()
        }

        function UpdateCampaign() {
            document.getElementById("txtReload").value = true;
            document.getElementById("btnUpdateCampaign").click()
        }

        function OpenSearch(a) {
            window.open('../Admin/frmSavedSearch.aspx?FormID=' + a, '', 'toolbar=no,menubar=yes,titlebar=no,left=100,width=400,height=200,scrollbars=yes,resizable=yes');
            return false;
        }

        function GetCheckedRowValues() {
            var RecordIDs = '';
            $("[id$=gvSearch] tr").each(function () {
                if ($(this).find("#chkSelect").is(':checked')) {
                    RecordIDs = RecordIDs + $(this).find("#lbl1").text() + ',';
                }
            });
            RecordIDs = RecordIDs.substring(0, RecordIDs.length - 1);
            return RecordIDs;
        }

        function GetCheckedRowValuesMassSalesOrder() {
            var RecordIDs = '';
            $("[id$=gvSearch] tr").each(function () {
                if ($(this).find("#chkSelect").is(':checked')) {
                    RecordIDs = RecordIDs + $(this).find("#lblDivID").text() + '-' + $(this).find("#lbl1").text() + ',';
                }
            });
            RecordIDs = RecordIDs.substring(0, RecordIDs.length - 1);
            return RecordIDs;
        }

        function OpenGoogleMap() {

            var RecordIDs = GetCheckedRowValues();
            if (RecordIDs.length > 0) {
                str = '?mode=2&CntIds=' + RecordIDs;

                window.open('../Admin/frmGoogleMap.aspx' + str, '', 'toolbar=no,menubar=yes,titlebar=no,left=100,width=800,height=600,scrollbars=yes,resizable=yes');
                return false;
            }
            else {
                alert('Please select atleast one record to display map!!');
                return false
            }
        }

        // Grid Column Resize
        $(document).ready(function () {
            $("[id$=gvSearch]").colResizable({
                //fixed: false,
                liveDrag: true,
                gripInnerHtml: "<div class='grip2'></div>",
                draggingClass: "dragging",
                minWidth: 30,
                onResize: onActionGridColumnResized
            });
        });

        var onActionGridColumnResized = function (e) {
            var columns = $(e.currentTarget).find("th");
            var msg = "";
            columns.each(function () {
                //console.log($(this));

                var thId = $(this).attr("id");
                var thWidth = $(this).width();
                msg += thId + ':' + thWidth + ';';
            }
            )


            $.ajax({
                type: "POST",
                url: "../common/Common.asmx/SaveAdvanceSearchGridColumnWidth",
                data: "{str:'" + msg + "',viewID:'1'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json"
            });
        };
    </script>
    <style type="text/css">
        #hovermenu {
            border: 1px solid black;
            background-color: #f3f4f5;
        }

            #hovermenu a {
                font: bold 11px "Segoe UI",Arial,sans-serif;
                padding: 2px;
                padding-left: 4px;
                display: block;
                width: 100%;
                color: black;
                text-decoration: none;
                border-bottom: 1px solid black;
            }

            html > body #hovermenu a {
                /*Non IE rule*/
                width: auto;
            }

                #hovermenu a:hover {
                    background-color: #515E81;
                    color: white;
                }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server"
    ClientIDMode="Static">
    <div id="divMessage" runat="server" class="row padbottom10" style="display: none">
        <div class="col-xs-12">
            <div class="alert alert-warning">
                <h4><i class="icon fa fa-warning"></i>Alert!</h4>
                <asp:Literal ID="litMessage" runat="server" EnableViewState="False"></asp:Literal>
            </div>
        </div>
    </div>
    <div class="row padbottom10">
        <div class="col-xs-12">
            <div class="pull-right">
                <asp:CheckBox ID="cbPrimaryContactsOnly" runat="server" Text="Only show primary contact" AutoPostBack="True"></asp:CheckBox>
                <asp:DropDownList ID="ddlSort" runat="server" CssClass="form-control" Style="display: none"></asp:DropDownList>
                <asp:CheckBox ID="chkSelectAll" runat="server" Text="Select All" />
                <asp:HyperLink ID="hplCreateSales" Text="Create Sales Order" CssClass="btn btn-primary" runat="server" onclick="return CreateSales();"></asp:HyperLink>
                <asp:HyperLink runat="server" CssClass="btn btn-primary" ID="hplSaveSearch" onclick="return OpenSearch(1);" Text="Save this search"></asp:HyperLink>
                <asp:Button ID="btnMap" Text="Map" CssClass="btn btn-primary" runat="server" OnClientClick="return OpenGoogleMap()"></asp:Button>
                <asp:Button ID="btnTransfer" OnClientClick="return Ownership()" Text="Transfer Owner/Assignee" CssClass="btn btn-primary" runat="server"></asp:Button>
                <asp:Button ID="btnAddEmailGroup" Text="Add Email Group" Visible="false" CssClass="btn btn-primary" runat="server"></asp:Button>
                <asp:UpdatePanel ID="UpdatePanel1" runat="server" style="display: inline">
                    <ContentTemplate>
                        <asp:Button ID="btnExportToExcel" runat="server" Text="Export to Excel" CssClass="btn btn-primary" CausesValidation="False"></asp:Button>
                    </ContentTemplate>
                    <Triggers>
                        <asp:PostBackTrigger ControlID="btnExportToExcel" />
                    </Triggers>
                </asp:UpdatePanel>
                <asp:Button ID="btnMassUpdate" runat="server" Text="Mass Update" CssClass="btn btn-primary" CausesValidation="False"></asp:Button>
                <asp:Button ID="btnPrepareMsg" runat="server" Text="Prepare Message" CssClass="btn btn-primary" CausesValidation="False"></asp:Button>
                <div class="btn-group">
                    <asp:HyperLink NavigateUrl="javascript:void(0);" ID="hplDelete" runat="server" Text=" &darr;" CssClass="btn btn-danger dropdown-toggle" data-toggle="dropdown" Style="text-decoration: none;">Delete <span class="caret"></span></asp:HyperLink>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="javascript:void(0);" onclick="MassDelete('1');;">Selected contacts </a></li>
                        <li><a href="javascript:void(0);" onclick="MassDelete('2');">Selected Organization </a></li>
                    </ul>
                </div>
                <asp:Button ID="btnMassDelete" runat="server" Text="Delete/Mass Delete" CssClass="btn btn-danger" CausesValidation="False" Style="display: none;"></asp:Button>
                <asp:LinkButton ID="lkbBackToSearchCriteria" CssClass="btn btn-primary" runat="server" CausesValidation="False"><i class="fa fa-arrow-left"></i>&nbsp;&nbsp;Back</asp:LinkButton>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server" ClientIDMode="Static">
    Advanced Search
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BizPager" runat="server" ClientIDMode="Static">
    <webdiyer:AspNetPager ID="bizPager" runat="server"
        PagingButtonSpacing="0"
        CssClass="bizgridpager"
        AlwaysShow="true"
        CurrentPageButtonClass="active"
        PagingButtonUlLayoutClass="pagination"
        PagingButtonLayoutType="UnorderedList"
        FirstPageText="<<"
        LastPageText=">>"
        NextPageText=">"
        PrevPageText="<"
        Width="100%"
        UrlPaging="false"
        ShowPageIndexBox="Never"
        NumericButtonCount="8"
        ShowCustomInfoSection="Left"
        OnPageChanged="bizPager_PageChanged"
        CustomInfoHTML="Showing records %StartRecordIndex% to %EndRecordIndex% of %RecordCount% "
        CustomInfoClass="bizpagercustominfo">
    </webdiyer:AspNetPager>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="GridPlaceHolder" runat="server"
    ClientIDMode="Static">
    <asp:UpdateProgress ID="UpdateProgress" runat="server" ClientIDMode="Static">
        <ProgressTemplate>
            <div class="overlay">
                <div class="overlayContent" style="color: #000; text-align: center; width: 250px; padding: 20px">
                    <i class="fa fa-2x fa-refresh fa-spin"></i>
                    <h3>Processing Request</h3>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <div class="row">
        <div class="col-xs-12">
            <div class="table-responsive">
                <asp:GridView ID="gvSearch" runat="server" EnableViewState="true" AutoGenerateColumns="false" CssClass="table table-bordered table-striped" Width="100%" ShowHeaderWhenEmpty="true">
                    <Columns>
                    </Columns>
                    <EmptyDataTemplate>
                        No Data Available
                    </EmptyDataTemplate>

                </asp:GridView>
            </div>
        </div>
    </div>

    <asp:TextBox ID="txtTotalPage" Style="display: none" runat="server"></asp:TextBox>
    <asp:TextBox ID="txtTotalRecords" Style="display: none" runat="server"></asp:TextBox>

    <div class="modal" id="divColHeader">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Filter within Search Results</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-xs-12">
                            <input id="cbShowAll" runat="server" type="checkbox">&nbsp;Show All
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="form-inline">
                                <label><span class="normal1" id="spColName" runat="server">Column Name</span> starts with</label>
                                <input class="form-control" id="txtColValue" runat="server" type="text" maxlength="50" width="130" height="176" />
                                <asp:TextBox ID="txtColumnName" class="form-control" runat="server" Style="display: none"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <input class="btn btn-primary" id="btnNew1Ok" runat="server" type="button" value="Go" name="btnNew1Ok" onclick="HideDivElement('divColHeader');" />
                    <input class="btn btn-default pull-left" data-dismiss="modal" id="btnNew1" onclick="HideDivElement('divColHeader')" type="button" value="Close" name="btnNew1">&nbsp;
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <asp:TextBox ID="txtSortChar" Text="0" Style="display: none" runat="server"></asp:TextBox>
    <asp:TextBox ID="txtSortColumn" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtSortOrder" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtReload" runat="server" Style="display: none">False</asp:TextBox>
    <asp:TextBox ID="txtGridColumnFilter" Style="display: none" runat="server"></asp:TextBox>
    <asp:Button ID="btnUpdateValues" runat="server" Style="display: none" />
    <asp:Button ID="btnUpdateDripCampaign" runat="server" Style="display: none" />
    <asp:Button ID="btnUpdateCampaign" runat="server" Style="display: none" />
    <asp:HiddenField ID="hdnDeleteMode" runat="server" Value="1" />
    <asp:TextBox ID="txtCurrrentPage" runat="server" Style="display: none"></asp:TextBox>
    <asp:Button ID="btnGo1" Width="25" runat="server" Style="display: none" />
    <script type="text/javascript">
        SelectAll('chkSelectAll');
    </script>
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="GridBizSorting" runat="server" ClientIDMode="Static">
    <uc1:frmBizSorting ID="frmBizSorting2" runat="server" />
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="GridSettingPopup" runat="server" ClientIDMode="Static">
    <a href="#" id="hplClearGridCondition" title="Clear All Filters" class="btn-box-tool"><i class="fa fa-2x fa-filter"></i></a>
    <asp:HyperLink runat="server" ID="hplEditResView" ToolTip="Edit advance search result view"><i class="fa fa-lg fa-gear"></i></asp:HyperLink>
</asp:Content>
