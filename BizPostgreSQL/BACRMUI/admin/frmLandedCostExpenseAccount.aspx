﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/common/Popup.Master" CodeBehind="frmLandedCostExpenseAccount.aspx.vb" Inherits=".frmLandedCostExpenseAccount" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <title>Landed Cost Expense Account</title>
    <script type="text/javascript" language="javascript">
        function Save() {

        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server">
    <div class="input-part">
        <div class="right-input">
            <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" OnClientClick="return Save();"></asp:Button>&nbsp;
            <asp:Button ID="btnClose" runat="server" CssClass="button" Text="Close" OnClientClick="return Close();"></asp:Button>&nbsp;&nbsp;
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitle" runat="server">
    Landed Cost Expense Account
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Content" runat="server">
    <table width="600px">
        <tr>
            <td class="normal4" align="center" colspan="2">
                <asp:Literal ID="litMessage" runat="server"></asp:Literal>
            </td>
        </tr>
        <tr>
            <td colspan="2">
               <asp:RadioButtonList runat="server" ID="rblLandedCost" style="margin-top:10px;" RepeatLayout="Flow" RepeatDirection="Horizontal">
                                                <asp:ListItem>Cubic Size</asp:ListItem>
                                                <asp:ListItem>Weight</asp:ListItem>
                                                <asp:ListItem Selected="True">Amount</asp:ListItem>
                                            </asp:RadioButtonList>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:GridView ID="gvLandedExpense" runat="server" AutoGenerateColumns="false"
                    CssClass="dg" Width="100%">
                    <AlternatingRowStyle CssClass="ais" />
                    <RowStyle CssClass="is" />
                    <HeaderStyle CssClass="hs" />
                    <Columns>
                        <asp:TemplateField HeaderText="Expense Account" HeaderStyle-Width="300px">
                            <ItemTemplate>
                                <asp:HiddenField runat="server" ID="hdnLCExpenseId" Value='<%# Eval("numLCExpenseId")%>' />
                                <asp:DropDownList ID="ddlExpenseAccount" runat="server" CssClass="{required:false,messages:{required:'Select Expense Account!'}}">
                                </asp:DropDownList>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Description" HeaderStyle-Width="400px">
                            <ItemTemplate>
                                <asp:TextBox ID="txtDescription" runat="server" Width="50px" AutoComplete="OFF" Text='<%# Eval("vcDescription") %>'
                                    MaxLength="50" CssClass="{required:false,messages:{required:'Enter Description'}}"></asp:TextBox>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:Button ID="btnDeleteRow" runat="server" CssClass="button Delete" Text="X" CommandName="DeleteRow"
                                    CommandArgument="<%# Container.DataItemIndex %>" OnClientClick="return confirm('Are you sure to delete selected Expense Account?')"></asp:Button>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
                <asp:Button ID="btnAdd" runat="server" CssClass="button" Text="Add New Row" />
            </td>
        </tr>
    </table>
</asp:Content>
