﻿Imports BACRM.BusinessLogic.Common

Partial Public Class frmCorresnpondense
    Inherits BACRMPage
    Dim Mode As Short
    Dim lngRecordID As Long
    Dim blnPageing As Boolean
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Mode = CCommon.ToShort(GetQueryStringVal("Mode"))
            lngRecordID = CCommon.ToLong(GetQueryStringVal("RecordID"))
            blnPageing = CCommon.ToBool(GetQueryStringVal("bPage"))
            hdnLocation.Value = Mode ' 4-Opp,5-project,6-case
            hdnRecordID.Value = lngRecordID
            frmCorrespondence1.Mode = Mode
            frmCorrespondence1.lngRecordID = lngRecordID
            frmCorrespondence1.bPaging = blnPageing
            frmCorrespondence1.getCorrespondance()

        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

End Class