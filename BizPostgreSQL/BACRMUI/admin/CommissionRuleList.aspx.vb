﻿Imports BACRM.BusinessLogic.Item
Imports BACRM.BusinessLogic.Common
Public Class CommissionRuleList
    Inherits BACRMPage
    Dim objCommissionRule As CommissionRule

   
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ""
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "none")
            GetUserRightsForPage(13, 45)
            If m_aryRightsForPage(RIGHTSTYPE.VIEW) = 0 Then Response.Redirect("../admin/authentication.aspx?mesg=AS")
            If m_aryRightsForPage(RIGHTSTYPE.ADD) = 0 Then
                btnNew.Visible = False
                'hplCommissionableContacts.Visible = False
            End If



            If Not IsPostBack Then

                objCommissionRule = New CommissionRule
                'hplCommissionableContacts.Attributes.Add("onclick", "return OpenSetting()")

                If CCommon.ToLong(Session("CommissionType")) > 0 Then
                    LoadDetails()
                Else
                    litMessage.Text = "Please Set  ""Commission structure"" from Global Settings -> Order Management -> Commission Settings."
                    btnNew.Visible = False
                End If
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub
    Private Sub DisplayError(ByVal ex As String)
        DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ex
        DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
    End Sub
    Sub LoadDetails()
        Try
            If objCommissionRule Is Nothing Then
                objCommissionRule = New CommissionRule
            End If
            objCommissionRule.RuleID = 0
            objCommissionRule.byteMode = 1
            objCommissionRule.DomainID = Session("DomainID")
          
            dgCommissionRule.DataSource = objCommissionRule.GetCommissionRule
            dgCommissionRule.DataBind()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Private Sub dgCommissionRule_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgCommissionRule.ItemCommand
        Try
            If e.CommandName = "RuleID" Then
                Response.Redirect("../admin/CommissionRuleDtl.aspx?RuleID=" & e.Item.Cells(0).Text)
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub
    Private Sub dgCommissionRule_DeleteCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgCommissionRule.DeleteCommand
        Try
            objCommissionRule = New CommissionRule
            objCommissionRule.RuleID = e.Item.Cells(0).Text
            objCommissionRule.DeleteCommissionRule()
            LoadDetails()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Private Sub dgCommissionRule_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgCommissionRule.ItemDataBound
        Try
            If e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.Item Then
                Dim btnDelete As LinkButton
                btnDelete = e.Item.FindControl("btnDelete")
                btnDelete.Attributes.Add("onclick", "return DeleteRecord()")
                If m_aryRightsForPage(RIGHTSTYPE.DELETE) = 0 Then btnDelete.Visible = False
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
End Class