Imports BACRM.BusinessLogic.Survey
Imports BACRM.BusinessLogic.Common

Namespace BACRM.UserInterface.Admin

    Partial Public Class frmAdvSurvey
        Inherits BACRMPage
        Dim objSurvey As SurveyAdministration
        Dim objCommon As CCommon
       

        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            Try
                'CLEAR ERROR ON RELOAD
                DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ""
                DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "none")

                objCommon = New CCommon
                GetUserRightsForPage(9, 10)

                If Not IsPostBack Then
                    If m_aryRightsForPage(RIGHTSTYPE.VIEW) = 0 Then Response.Redirect("../admin/authentication.aspx?mesg=AS", False)

                    LoadSurvey()
                    BindQuestions()

                End If
                BindAns(1)
                BindAns(2)
                BindAns(3)
                BindAns(4)
                BindAns(5)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Private Sub ddlSurvey_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlSurvey.SelectedIndexChanged
            Try
                BindQuestions()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        'Load the Survey Questions
        Sub LoadSurvey()
            Try
                If objSurvey Is Nothing Then objSurvey = New SurveyAdministration
                objSurvey.DomainId = Session("DomainId")

                Dim dtSurveylist As DataTable                             'Declare a datatable

                dtSurveylist = objSurvey.GetSurveyListddl
                Dim drSurveyList As DataRow                               'Declare a DataRow object

                ddlSurvey.Items.Clear()
                For Each drSurveyList In dtSurveylist.Rows                'Loop through the rows on the table containing the survey list
                    ddlSurvey.Items.Add(New ListItem(ReSetFromXML(drSurveyList.Item("vcSurName")), drSurveyList.Item("numSurID")))
                Next

                ddlSurvey.Items.Insert(0, "--Select One--")
                ddlSurvey.Items.FindByText("--Select One--").Value = "0"
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Function ReSetFromXML(ByVal sValue As String) As String
            Try
                Return Replace(Replace(Replace(Replace(sValue, "&amp;", "&"), "&#39;", "'"), "&lt;", "<"), "&gt;", ">")
            Catch ex As Exception
                Throw
            End Try
        End Function
        Sub BindQuestions()
            Try
                If ddlSurvey.SelectedIndex > 0 Then
                    If objSurvey Is Nothing Then objSurvey = New SurveyAdministration
                    objSurvey.DomainId = Session("DomainId")
                    objSurvey.SurveyId = ddlSurvey.SelectedValue
                    Dim dtTable As DataTable = objSurvey.GetSurveyQuestions()

                    ddlQuestion1.DataSource = dtTable
                    ddlQuestion1.DataTextField = "vcQuestion"
                    ddlQuestion1.DataValueField = "numQID"
                    ddlQuestion1.DataBind()
                    ddlQuestion1.Items.Insert(0, "--Select One--")
                    ddlQuestion1.Items.FindByText("--Select One--").Value = "0"

                    ddlQuestion2.DataSource = dtTable
                    ddlQuestion2.DataTextField = "vcQuestion"
                    ddlQuestion2.DataValueField = "numQID"
                    ddlQuestion2.DataBind()
                    ddlQuestion2.Items.Insert(0, "--Select One--")
                    ddlQuestion2.Items.FindByText("--Select One--").Value = "0"

                    ddlQuestion3.DataSource = dtTable
                    ddlQuestion3.DataTextField = "vcQuestion"
                    ddlQuestion3.DataValueField = "numQID"
                    ddlQuestion3.DataBind()
                    ddlQuestion3.Items.Insert(0, "--Select One--")
                    ddlQuestion3.Items.FindByText("--Select One--").Value = "0"

                    ddlQuestion4.DataSource = dtTable
                    ddlQuestion4.DataTextField = "vcQuestion"
                    ddlQuestion4.DataValueField = "numQID"
                    ddlQuestion4.DataBind()
                    ddlQuestion4.Items.Insert(0, "--Select One--")
                    ddlQuestion4.Items.FindByText("--Select One--").Value = "0"

                    ddlQuestion5.DataSource = dtTable
                    ddlQuestion5.DataTextField = "vcQuestion"
                    ddlQuestion5.DataValueField = "numQID"
                    ddlQuestion5.DataBind()
                    ddlQuestion5.Items.Insert(0, "--Select One--")
                    ddlQuestion5.Items.FindByText("--Select One--").Value = "0"
                Else
                    ddlQuestion1.Items.Clear()
                    ddlQuestion2.Items.Clear()
                    ddlQuestion3.Items.Clear()
                    ddlQuestion4.Items.Clear()
                    ddlQuestion5.Items.Clear()
                    ddlQuestion1.Items.Insert(0, "--Select One--")
                    ddlQuestion1.Items.FindByText("--Select One--").Value = "0"
                    ddlQuestion2.Items.Insert(0, "--Select One--")
                    ddlQuestion2.Items.FindByText("--Select One--").Value = "0"
                    ddlQuestion3.Items.Insert(0, "--Select One--")
                    ddlQuestion3.Items.FindByText("--Select One--").Value = "0"
                    ddlQuestion4.Items.Insert(0, "--Select One--")
                    ddlQuestion4.Items.FindByText("--Select One--").Value = "0"
                    ddlQuestion5.Items.Insert(0, "--Select One--")
                    ddlQuestion5.Items.FindByText("--Select One--").Value = "0"
                    tbQ1.Controls.Clear()
                    tbQ2.Controls.Clear()
                    tbQ3.Controls.Clear()
                    tbQ4.Controls.Clear()
                    tbQ5.Controls.Clear()
                End If
            Catch ex As Exception
                Throw
            End Try
        End Sub
        
        'This sub is used to Bind the Answers of a selected Question
        Sub BindAns(ByVal intQus As Integer)
            Try
                If objSurvey Is Nothing Then objSurvey = New SurveyAdministration
                Dim intAnsType As Int16 = 0
                Dim numQID As Integer = 0
                Dim Table As HtmlTable
                objSurvey.SurveyId = ddlSurvey.SelectedValue
                objSurvey.DomainId = Session("DomainId")
                Select Case intQus
                    Case 1
                        If ddlQuestion1.SelectedValue <> "0" Then
                            numQID = ddlQuestion1.SelectedValue.Split("~")(0)
                            intAnsType = ddlQuestion1.SelectedValue.Split("~")(1)
                        End If
                        Table = tbQ1
                    Case 2
                        If ddlQuestion2.SelectedValue <> "0" Then
                            numQID = ddlQuestion2.SelectedValue.Split("~")(0)
                            intAnsType = ddlQuestion2.SelectedValue.Split("~")(1)
                        End If
                        Table = tbQ2
                    Case 3
                        If ddlQuestion3.SelectedValue <> "0" Then
                            numQID = ddlQuestion3.SelectedValue.Split("~")(0)
                            intAnsType = ddlQuestion3.SelectedValue.Split("~")(1)
                        End If
                        Table = tbQ3
                    Case 4
                        If ddlQuestion4.SelectedValue <> "0" Then
                            numQID = ddlQuestion4.SelectedValue.Split("~")(0)
                            intAnsType = ddlQuestion4.SelectedValue.Split("~")(1)
                        End If
                        Table = tbQ4
                    Case 5
                        If ddlQuestion5.SelectedValue <> "0" Then
                            numQID = ddlQuestion5.SelectedValue.Split("~")(0)
                            intAnsType = ddlQuestion5.SelectedValue.Split("~")(1)
                        End If
                        Table = tbQ5
                End Select
                objSurvey.QuestionId = numQID
                Dim dtAns As DataTable = objSurvey.GetSurveyAnswers()
                If dtAns.Rows.Count > 0 Then
                    Dim drAnswer As DataRow
                    Dim oControlBox As Object
                    Dim tblCell As New HtmlTableCell                                            'Instantiate a New tableCell
                    Dim tblRow As New HtmlTableRow

                    Select Case intAnsType
                        Case 1
                            For Each drAnswer In dtAns.Rows
                                oControlBox = New RadioButton                               'instantiate a Radio Button
                                oControlBox.GroupName = intQus & "Gro" & drAnswer.Item("numSurID") & "_" & numQID
                                oControlBox.ID = intQus & "ans_" & drAnswer.Item("numSurID") & "_" & numQID & "_" & drAnswer.Item("numAnsID") 'set the name of the Radio Button
                                oControlBox.Text = Server.HtmlDecode(drAnswer.Item("vcAnsLabel")) 'Set the label
                                oControlBox.CssClass = "text"                               'Set the class attribute for the Radio Button control
                                tblCell.Controls.Add(oControlBox)                           'Add the Textbox to teh cell
                                tblCell.Controls.Add(New LiteralControl("&nbsp;"))            'Add the line carraige teh cell
                                tblRow.Cells.Add(tblCell)                                   'Add the cell to teh row
                                Table.Rows.Add(tblRow)
                            Next
                        Case 2
                            For Each drAnswer In dtAns.Rows
                                tblCell.Controls.Add(New LiteralControl(Server.HtmlDecode(drAnswer.Item("vcAnsLabel")))) 'Add the Textbox to teh cell
                                tblCell.Controls.Add(New LiteralControl("&nbsp;"))            'Add the line carraige teh cell

                                oControlBox = New TextBox                                   'Instantiate a TextBox
                                oControlBox.ID = intQus & "ans_" & drAnswer.Item("numSurID") & "_" & numQID & "_" & drAnswer.Item("numAnsID") 'set the name of the textbox
                                oControlBox.Width = Unit.Pixel(200)                         'set the width of the textbox
                                oControlBox.Height = Unit.Pixel(20)                         'set the height of the textbox
                                oControlBox.TextMode = TextBoxMode.SingleLine                'This is a multiline textarea
                                oControlBox.CssClass = "signup"                             'Set the class attribute for the textbox control
                                tblCell.Controls.Add(oControlBox)                           'Add the Textbox to teh cell
                                tblCell.Controls.Add(New LiteralControl("&nbsp;"))            'Add the line carraige teh cell
                                tblRow.Cells.Add(tblCell)                                   'Add the cell to teh row
                                'Increment the ans row counter
                                Table.Rows.Add(tblRow)
                            Next
                        Case 4
                            oControlBox = New DropDownList                                  'instantiate a drop down
                            oControlBox.ID = intQus & "ans_" & dtAns.Rows(0).Item("numSurID") & "_" & numQID 'set the name of the textbox
                            oControlBox.Width = Unit.Pixel(230)                             'set the width of the drop down
                            oControlBox.CssClass = "signup"                                 'Set the class attribute for the selectbox control
                            Dim newListItem As ListItem                                     'Declare a ListIntem
                            For Each drAnswer In dtAns.Rows
                                newListItem = New ListItem                                  'Create a new listitem
                                newListItem.Text = Server.HtmlDecode(drAnswer.Item("vcAnsLabel")) 'Set the Answer as label
                                newListItem.Value = drAnswer.Item("numAnsID") 'Set the value for the answer
                                oControlBox.Items.Add(newListItem)
                            Next
                            newListItem = New ListItem                                      'Create a new listitem
                            newListItem.Text = "---Select One---"                           'Set the display text
                            newListItem.Value = "javascript: HideNextRow('" & dtAns.Rows(0).Item("numSurID") & "_" & numQID & "','ThisRow');" 'Set the Value to hide this tr
                            oControlBox.Items.Insert(0, newListItem)                        'Make this for the first element

                            tblCell.Controls.Add(oControlBox)                               'Add the Textbox to teh cell
                            tblRow.Cells.Add(tblCell)                                       'Add the cell to the row
                            Table.Rows.Add(tblRow)
                        Case Else
                            For Each drAnswer In dtAns.Rows
                                oControlBox = New CheckBox                                  'instantiate a CheckBox
                                oControlBox.ID = intQus & "ans_" & drAnswer.Item("numSurID") & "_" & numQID & "_" & drAnswer.Item("numAnsID") 'set the name of the CheckBox
                                oControlBox.Text = Server.HtmlDecode(drAnswer.Item("vcAnsLabel")) 'Set the label
                                oControlBox.CssClass = "text"                               'Set the class attribute for the CheckBox control
                                tblCell.Controls.Add(oControlBox)                           'Add the Textbox to teh cell
                                tblCell.Controls.Add(New LiteralControl("&nbsp;"))            'Add the line carraige teh cell
                                tblRow.Cells.Add(tblCell)                                   'Add the cell to teh row
                                
                                Table.Rows.Add(tblRow)
                            Next
                    End Select
                End If
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Private Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
            Try
                Dim str As String = ""
                If ddlSurvey.SelectedIndex > 0 Then
                    If ddlQuestion1.SelectedIndex > 0 Then
                        str = BindSqlQuery(ddlQuestion1.SelectedValue.Split("~")(0), ddlQuestion1.SelectedValue.Split("~")(1), 1)
                    End If
                End If
                If ddlSurvey.SelectedIndex > 0 Then
                    If ddlQuestion2.SelectedIndex > 0 Then
                        str = str & BindSqlQuery(ddlQuestion2.SelectedValue.Split("~")(0), ddlQuestion2.SelectedValue.Split("~")(1), 2)
                    End If
                End If
                If ddlSurvey.SelectedIndex > 0 Then
                    If ddlQuestion3.SelectedIndex > 0 Then
                        str = str & BindSqlQuery(ddlQuestion3.SelectedValue.Split("~")(0), ddlQuestion3.SelectedValue.Split("~")(1), 3)
                    End If
                End If
                If ddlSurvey.SelectedIndex > 0 Then
                    If ddlQuestion4.SelectedIndex > 0 Then
                        str = str & BindSqlQuery(ddlQuestion4.SelectedValue.Split("~")(0), ddlQuestion4.SelectedValue.Split("~")(1), 4)
                    End If
                End If
                If ddlSurvey.SelectedIndex > 0 Then
                    If ddlQuestion5.SelectedIndex > 0 Then
                        str = str & BindSqlQuery(ddlQuestion5.SelectedValue.Split("~")(0), ddlQuestion5.SelectedValue.Split("~")(1), 5)
                    End If
                End If

                If chkSurveyBet.Checked = True Then
                    str = str & " And numSurRating between " & IIf(IsNumeric(txtRatingStart.Text) = True, txtRatingStart.Text, 0) & " and " & IIf(IsNumeric(txtRatingEnd.Text) = True, txtRatingEnd.Text, 0)
                End If

                If str.Length >= 5 Then 
                    Session("WhereConditionSurvey") = str.Remove(0, 4)
                Else : Session("WhereConditionSurvey") = str
                End If
                Response.Redirect("../Admin/FrmAdvSurveyRes.aspx")
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        'Function To Bind the Sql Query for each Question
        Function BindSqlQuery(ByVal numQID As Integer, ByVal intAnsType As Int16, ByVal intQus As Int16) As String
            Try
                Dim str As String = ""
                Dim dtAns As DataTable
                Dim Rel As String
                Dim drAnswer As DataRow
                If radAnd.Checked = True Then
                    Rel = " And "
                Else : Rel = " Or  "
                End If

                objSurvey.SurveyId = ddlSurvey.SelectedValue
                objSurvey.DomainId = Session("DomainId")
                objSurvey.QuestionId = numQID
                dtAns = objSurvey.GetSurveyAnswers()

                For Each drAnswer In dtAns.Rows

                    Select Case intAnsType
                        Case 1
                            Dim radButton As RadioButton
                            radButton = CType(Page.FindControl(intQus & "ans_" & drAnswer.Item("numSurID") & "_" & numQID & "_" & drAnswer.Item("numAnsID")), RadioButton)
                            If Not radButton Is Nothing Then
                                If radButton.Checked Then
                                    str = str & Rel & "(SRM.numsurid = " & drAnswer.Item("numSurID") & "  and SR.numQuestionId =" & numQID & " and SR.numAnsId=" & drAnswer.Item("numAnsID") & ") "
                                End If
                            End If
                        Case 2
                            Dim txtBox As TextBox
                            txtBox = CType(Page.FindControl(intQus & "ans_" & drAnswer.Item("numSurID") & "_" & numQID & "_" & drAnswer.Item("numAnsID")), TextBox)
                            If Not txtBox Is Nothing Then
                                If txtBox.Text <> "" Then
                                    str = str & Rel & "(SRM.numsurid = " & drAnswer.Item("numSurID") & "  and SR.numQuestionId =" & numQID & " and SR.vcAnsText = '" & txtBox.Text & "') "
                                End If
                            End If
                        Case 4
                            Dim ddlList As DropDownList
                            ddlList = CType(Page.FindControl(intQus & "ans_" & drAnswer.Item("numSurID") & "_" & numQID), DropDownList)
                            If Not ddlList Is Nothing Then
                                If ddlList.SelectedIndex <> 0 Then
                                    str = str & Rel & "(SRM.numsurid = " & drAnswer.Item("numSurID") & "  and SR.numQuestionId =" & numQID & " and SR.numAnsId=" & ddlList.SelectedValue & ") "
                                    Exit For
                                Else : Exit For
                                End If
                            End If
                        Case Else
                            Dim chkBox As CheckBox
                            chkBox = CType(Page.FindControl(intQus & "ans_" & drAnswer.Item("numSurID") & "_" & numQID & "_" & drAnswer.Item("numAnsID")), CheckBox)
                            If Not chkBox Is Nothing Then
                                If chkBox.Checked Then str = str & Rel & "(SRM.numsurid = " & drAnswer.Item("numSurID") & "  and SR.numQuestionId =" & numQID & " and SR.numAnsId=" & drAnswer.Item("numAnsID") & ") "
                            End If
                    End Select
                Next
                Return str
            Catch ex As Exception
                Throw
            End Try
        End Function

        Private Sub DisplayError(ByVal exception As String)
            Try
                DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = exception
                DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
                DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Focus()
            Catch ex As Exception

            End Try
        End Sub
    End Class
End Namespace