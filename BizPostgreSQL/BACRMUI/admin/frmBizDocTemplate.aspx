﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmBizDocTemplate.aspx.vb"
    Inherits=".frmBizDocTemplate" MasterPageFile="~/common/DetailPage.Master" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <style>
        .customerdetail {
            min-height: 0px !important;
        }

        .reDropDownBody {
            width:300px !important;
        }

        .taleLayout {
            width: 100%;
            border-spacing: 0px;
            padding: 0px;
        }

            .taleLayout > tbody > tr {
                height: 40px;
            }

                .taleLayout > tbody > tr > td:first-child {
                    font-weight: bold;
                    white-space: nowrap;
                }

        input[type='radio'] {
            background-color: #FFFF99;
            color: Navy;
            position: relative;
            bottom: 3px;
            vertical-align: middle;
        }

        input[type='checkbox'] {
            background-color: #FFFF99;
            color: Navy;
            position: relative;
            vertical-align: middle;
        }

        .RadEditor iframe
        {
            height:83% !important;
        }
    </style>
    <script type="text/JavaScript">
        function ShowMessage() {
            //alert("you can not delete default bizdoc template,your option is to create another template which is default for given bizdoc type and try removing current bizdoc template again!");
            var confirm_value = document.createElement("INPUT");
            confirm_value.type = "hidden";
            confirm_value.name = "confirm_value";
            if (confirm("There can only be One default template and you already have one. Would you like to replace your existing default with this one?")) {
                confirm_value.value = "Yes";
            } else {
                confirm_value.value = "No";
                document.getElementById("chkDefault").checked = false;
            }
            document.forms[0].appendChild(confirm_value);

            //alert("You can't delete a default template. If you want to delete this template, switch the default setting to another template, then delete this one!");
            return false;
        }

        function Save() {
            if (document.form1.ddlBizDoc != null) {
                if (document.form1.ddlBizDoc.value == "0") {
                    alert("Select BizDoc")
                    document.form1.ddlBizDoc.focus()
                    return false;
                }
                if (document.form1.txtTemplateName.value == '') {
                    alert("Enter Template Name")
                    return false;
                }
            }
            return true;
        }
        function OnClientCommandExecuting(editor, args) {
            var name = args.get_name();
            var val = args.get_value();
            if (name == "BizDocElement" || name == "CustomField") {
                editor.pasteHtml(val);
                //Cancel the further execution of the command as such a command does not exist in the editor command list
                args.set_cancel(true);
            }
        }
        function Openfooter(a) {
            var TemplateID = document.getElementById("<%=hdfTemplateID.ClientID%>").value;
            window.open('../opportunity/frmFooterUpload.aspx?frm=BizDocs&OppType=' + a + '&TemplateID=' + TemplateID + '', '', 'toolbar=no,titlebar=no,top=300,width=400,height=100,scrollbars=yes,resizable=yes');
            return false;
        }
        function OpenLogoPage() {
            var TemplateID = document.getElementById("<%=hdfTemplateID.ClientID%>").value;
            window.open('../opportunity/frmLogoUpload.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=BizDocs&TemplateID=' + TemplateID + '', '', 'toolbar=no,titlebar=no,top=300,width=400,height=200,scrollbars=yes,resizable=yes');
            return false;
        }
        function OpenBizInvoice() {
            var TemplateType = document.getElementById("<%=hdfTemplateType.ClientID%>").value;

            var TemplateID = document.getElementById("<%=hdfTemplateID.ClientID%>").value;
            var TemplateName = document.getElementById("<%=hdfTemplateName.ClientID%>").value;

            window.open('../opportunity/frmBizInvoicePreview.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&TemplateName=' + TemplateName + '&TemplateType=' + TemplateType + '&TemplateID=' + TemplateID, '', 'toolbar=no,menubar=yes,titlebar=no,left=100,top=15,width=750,height=700,scrollbars=yes,resizable=yes');
            return false;
        }
        function OpenAtch() {
            var a = document.getElementById("<%=ddlBizDoc.ClientID%>").value;
            var b = document.getElementById("<%=ddlOppType.ClientID%>").value;
            var c = document.getElementById("<%=hdfTemplateID.ClientID%>").value;
            window.open("../opportunity/frmBizDocAttachments.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&BizDocID=" + a + "&OppType=" + (b = 7 ? 1 : 2) + "&TempID=" + 0, "", "width=800,height=400,status=no,scrollbars=yes,left=155,top=160");
            return false;
        }
        function getdata() {
            var parentEditor = $find('<%=RadEditor1.ClientID%>');
            var div = $find("#dvsac");
            div = parentEditor.get_html();
            return false;
        }

        /*Function added by Neelam on 09/29/2017 - Added functionality to open BizDoc to see what design change looks like*/
        function OpenModifiedBizDoc() {
            var btnFetch = document.getElementById("<%= btnFetchOppId.ClientID %>");
            btnFetch.click();
        }
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="RecordInformationPanel" runat="server"
    ClientIDMode="Static">

    <div class="row padbottom10">
        <div class="col-md-12">
            <div class="pull-right">
                <asp:LinkButton ID="btnSave" runat="server" CssClass="btn btn-primary" OnClientClick="javascript:return Save();"><i class="fa fa-floppy-o">&nbsp;Update your changes</i></asp:LinkButton>
                <asp:LinkButton ID="btnClose" runat="server" CssClass="btn btn-primary"><i class="fa fa-times-circle-o">&nbsp;Close</i></asp:LinkButton>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="RecordActionPanel" runat="server"
    ClientIDMode="Static">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="DetailPageTitle" runat="server"
    ClientIDMode="Static">
    <asp:Label ID="lblTitle" runat="server" Text="Modify BizDoc Design" />
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="TabsPlaceHolder" runat="server"
    ClientIDMode="Static">
    <asp:Label ID="lblError" runat="server" CssClass="text-center" ForeColor="Red"> </asp:Label>
    <div style="background-color: #fff;">
        <table width="100%">
            <tr>
                <td class="normal4" align="center">
                    <asp:Literal ID="litMessage" runat="server" EnableViewState="False"></asp:Literal>
                </td>
            </tr>
        </table>
        <table width="100%" border="0" class="table table-responsive">
            <tr id="trOppType" runat="server">
                <td class="text" align="right" style="white-space: nowrap;"><b>Opportunities / Form<font color="#ff0000">*</font></b>
                </td>
                <td>
                    <asp:DropDownList runat="server" ID="ddlOppType" CssClass="form-control" AutoPostBack="true" Width="150">
                        <asp:ListItem Text="Sales" Value="1">
                        </asp:ListItem>
                        <asp:ListItem Text="Purchase" Value="2">
                        </asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td style="width: 100%">Default&nbsp;<asp:CheckBox runat="server" ID="chkDefault" CssClass="signup" AutoPostBack="true" OnCheckedChanged="chkDefault_OnCheckedChanged" />&nbsp;
                    &nbsp;Enable&nbsp;<asp:CheckBox runat="server" ID="chkEnabled" CssClass="signup" />
                    <a href="#" title="Upload Logo for Header" onclick="return OpenLogoPage();" id="aHeaderlogo" runat="server">
                        <img src="../images/header.png" alt="Upload for Header" style="width: 20px; height: 20px; vertical-align: middle" /></a>
                    <a href="#" title="Upload Logo for Footer" onclick="return Openfooter(1);" id="afooterlogo" runat="server">
                        <img src="../images/footer.png" alt="Upload for Footer" style="width: 18px; height: 20px; vertical-align: middle" /></a>

                    <asp:ImageButton ID="ibtnPreview" runat="server" ToolTip="Go for Preview" ImageUrl="~/images/icons/cart_large.png" Style="width: 26px; height: 23px; vertical-align: middle" />
                    <asp:RadioButton ID="rdbPortrait" runat="server" Checked="true" Text="Portrait" GroupName="grpOrientation" />
                    <asp:RadioButton ID="rdbLandscape" runat="server" Text="Landscape" GroupName="grpOrientation" />
                    <asp:CheckBox ID="chkKeepFooterBottom" runat="server" Text="Footer at the bottom of Page" ToolTip="Change in bizdoc html is still required to make it work"></asp:CheckBox>
                    <div style="font-weight: bold; float: right;" runat="server" id="divBiz">
                        BizDoc ID:
                        <asp:Label runat="server" ID="lblBizDocId"></asp:Label>
                    </div>
                    <asp:CheckBox ID="chkDisplayKitChild" runat="server" Text="Display child-items within kits as line items on BizDocs"></asp:CheckBox>
                    <div style="font-weight: bold; float: right;">
                        What's it look like now?
                    <a href="#" title="What's it look like now?" onclick="OpenModifiedBizDoc()" id="aBizLookLike" runat="server">
                        <img src="../images/GLReport.png" alt="What's it look like now?" style="width: 18px; height: 20px; vertical-align: middle" /></a>
                    </div>
                    <div style="display: none;">
                        <asp:Button ID="btnFetchOppId" runat="server" Text="Button" />
                    </div>
                </td>
            </tr>
            <tr id="trBizDoc" runat="server">
                <td class="text" align="right"><b>BizDoc<font color="#ff0000">*</font>:</b>
                </td>
                <td align="left" class="text">
                    <asp:DropDownList runat="server" ID="ddlBizDoc" CssClass="form-control" AutoPostBack="false" Width="150">
                    </asp:DropDownList>
                </td>
                <td>
                    <span id="spnBizDoc" runat="server">When this template is emailed,auto-attach the</span><asp:HyperLink ID="hplBizDoc" runat="server" CssClass="hyperlink">Attachments</asp:HyperLink>
                </td>
            </tr>
            <tr id="trTemplateName" runat="server">
                <td class="text" align="right"><b>Template<font color="#ff0000">*</font></b>
                </td>
                <td align="left" class="text">
                    <asp:TextBox runat="server" ID="txtTemplateName" CssClass="form-control" MaxLength="50" Width="150"></asp:TextBox>
                </td>
                <td>
                    <table>
                        <tr>
                            <td><b>Default To:</b>&nbsp;&nbsp;&nbsp;</td>
                            <td><b>Relationship</b>&nbsp;</td>
                            <td>
                                <asp:DropDownList ID="ddlRelationship" runat="server" CssClass="form-control" AutoPostBack="true" Width="150"></asp:DropDownList>
                            </td>
                            <td><b>&nbsp;&nbsp;&nbsp;Profile</b>&nbsp;</td>
                            <td>
                                <asp:DropDownList ID="ddlProfile" runat="server" CssClass="form-control" AutoPostBack="true" Width="150"></asp:DropDownList>
                            </td>
                            <td>
                                <b>&nbsp;&nbsp;&nbsp;Class&nbsp;</b>
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlClass" runat="server" CssClass="form-control" AutoPostBack="true" Width="150"></asp:DropDownList>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td class="text" colspan="3">
                    <table>
                        <tr>
                            <td colspan="2" align="right" class="text"><b>Base this BizDoc design on this (existing) BizDoc template:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Select BizDoc Type</b>&nbsp;&nbsp;&nbsp;</td>

                            <td align="left" class="text">
                                <asp:DropDownList ID="ddlBizDocType" runat="server" CssClass="form-control" AutoPostBack="true" Width="150"></asp:DropDownList>
                            </td>
                            <td class="text" align="right"><b>&nbsp;&nbsp;&nbsp;Select BizDoc Template</b>&nbsp;</td>
                            <td class="text" align="left">
                                <asp:DropDownList ID="ddlBizDocTemplate" runat="server" CssClass="form-control" AutoPostBack="true" Width="150"></asp:DropDownList>
                            </td>
                            <div runat="server" id="divBizDocID">
                                <td class="text" align="right"><b>&nbsp;&nbsp;&nbsp;BizDoc ID:</b>&nbsp;</td>
                                <td class="text" align="right">
                                    <asp:Label runat="server" ID="lblBizDocId1"></asp:Label>
                                </td>
                            </div>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td class="normal1" align="left" valign="top"><b>Template HTML:</b>
                </td>
                <td colspan="2" align="left">
                    <telerik:RadEditor ID="RadEditor1" runat="server" Height="515px" Width="900px" OnClientPasteHtml="OnClientPasteHtml"
                        OnClientCommandExecuting="OnClientCommandExecuting" ToolsFile="~/Marketing/EditorTools.xml">
                        <Content>
        
                        </Content>
                        <Tools>
                            <telerik:EditorToolGroup>
                                <telerik:EditorDropDown Name="BizDocElement" Text="Insert Element" >
                                    <telerik:EditorDropDownItem Name="Amount Paid Popup" Value="#AmountPaidPopUp#" />
                                    <telerik:EditorDropDownItem Name="Amount Paid" Value="#AmountPaid#" />
                                    <telerik:EditorDropDownItem Name="Assignee Name" Value="#AssigneeName#" />
                                    <telerik:EditorDropDownItem Name="Assignee Email" Value="#AssigneeEmail#" />
                                    <telerik:EditorDropDownItem Name="Assignee Phone No" Value="#AssigneePhoneNo#" />

                                    <telerik:EditorDropDownItem Name="Balance Due" Value="#BalanceDue#" />
                                    <telerik:EditorDropDownItem Name="BillingContact" Value="#BillingContact#" />
                                    <telerik:EditorDropDownItem Name="Billing Terms Name" Value="#BillingTermsName#" />
                                    <telerik:EditorDropDownItem Name="Billing Terms" Value="#BillingTerms#" />
                                    <telerik:EditorDropDownItem Name="Billing Terms-From Date" Value="#BillingTermFromDate#" />
                                    <telerik:EditorDropDownItem Name="BizDoc-Comments" Value="#Comments#" />
                                    <telerik:EditorDropDownItem Name="BizDoc Created Date" Value="#BizDocCreatedDate#" />
                                    <telerik:EditorDropDownItem Name="BizDoc ID" Value="#BizDocID#" />
                                    <telerik:EditorDropDownItem Name="BizDoc Status" Value="#BizDocStatus#" />
                                    <telerik:EditorDropDownItem Name="BizDoc Summary" Value="#BizDocSummary#" />
                                    <telerik:EditorDropDownItem Name="BizDoc Template Name" Value="#BizDocTemplateName#" />
                                    <telerik:EditorDropDownItem Name="BizDoc Type" Value="#BizDocType#" />

                                    <telerik:EditorDropDownItem Name="Change Customer/Vendor Bill To Header" Value="#OppOrderChangeBillToHeader(Bill To)#" />
                                    <telerik:EditorDropDownItem Name="Change Customer/Vendor Ship To Header" Value="#OppOrderChangeShipToHeader(Ship To)#" />
                                    <telerik:EditorDropDownItem Name="Change Employer Bill To Header" Value="#EmployerChangeBillToHeader(Bill To)#" />
                                    <telerik:EditorDropDownItem Name="Change Employer Ship To Header" Value="#EmployerChangeShipToHeader(Ship To)#" />
                                    <telerik:EditorDropDownItem Name="Currency" Value="#Currency#" />
                                    <telerik:EditorDropDownItem Name="Customer/Vendor Bill To Address Name" Value="#Customer/VendorBillToAddressName#" />
                                    <telerik:EditorDropDownItem Name="Customer/Vendor Bill To Address" Value="#Customer/VendorBillToAddress#" />
                                    <telerik:EditorDropDownItem Name="Customer/Vendor Bill To City" Value="#Customer/VendorBillToCity#" />
                                    <telerik:EditorDropDownItem Name="Customer/Vendor Bill To Company Name" Value="#Customer/VendorBillToCompanyName#" />
                                    <telerik:EditorDropDownItem Name="Customer/Vendor Bill To Country" Value="#Customer/VendorBillToCountry#" />
                                    <telerik:EditorDropDownItem Name="Customer/Vendor Bill To Postal" Value="#Customer/VendorBillToPostal#" />
                                    <telerik:EditorDropDownItem Name="Customer/Vendor Bill To State" Value="#Customer/VendorBillToState#" />
                                    <telerik:EditorDropDownItem Name="Customer/Vendor Bill To Street" Value="#Customer/VendorBillToStreet#" />
                                    <telerik:EditorDropDownItem Name="Customer/Vendor Organization Comments" Value="#Customer/VendorOrganizationComments#" />
                                    <telerik:EditorDropDownItem Name="Customer/Vendor Organization Contact Email" Value="#Customer/VendorOrganizationContactEmail#" />
                                    <telerik:EditorDropDownItem Name="Customer/Vendor Organization Contact Name" Value="#Customer/VendorOrganizationContactName#" />
                                    <telerik:EditorDropDownItem Name="Customer/Vendor Organization Contact Phone" Value="#Customer/VendorOrganizationContactPhone#" />
                                    <telerik:EditorDropDownItem Name="Customer/Vendor Organization Name" Value="#Customer/VendorOrganizationName#" />
                                    <telerik:EditorDropDownItem Name="Customer/Vendor Organization Phone" Value="#Customer/VendorOrganizationPhone#" />
                                    <telerik:EditorDropDownItem Name="Customer/Vendor Ship To Address Name" Value="#Customer/VendorShipToAddressName#" />
                                    <telerik:EditorDropDownItem Name="Customer/Vendor Ship To Address" Value="#Customer/VendorShipToAddress#" />
                                    <telerik:EditorDropDownItem Name="Customer/Vendor Ship To City" Value="#Customer/VendorShipToCity#" />
                                    <telerik:EditorDropDownItem Name="Customer/Vendor Ship To Company Name" Value="#Customer/VendorShipToCompanyName#" />
                                    <telerik:EditorDropDownItem Name="Customer/Vendor Ship To Country" Value="#Customer/VendorShipToCountry#" />
                                    <telerik:EditorDropDownItem Name="Customer/Vendor Ship To Postal" Value="#Customer/VendorShipToPostal#" />
                                    <telerik:EditorDropDownItem Name="Customer/Vendor Ship To State" Value="#Customer/VendorShipToState#" />
                                    <telerik:EditorDropDownItem Name="Customer/Vendor Ship To Street" Value="#Customer/VendorShipToStreet#" />

                                    <telerik:EditorDropDownItem Name="Deal or Order ID" Value="#OrderID#" />
                                    <telerik:EditorDropDownItem Name="Discount" Value="#Discount#" />
                                    <telerik:EditorDropDownItem Name="Due Date Change Popup" Value="#ChangeDueDate#" />
                                    <telerik:EditorDropDownItem Name="Due Date" Value="#DueDate#" />
                                    <telerik:EditorDropDownItem Name="Drop Ship" Value="#DropShip#" />

                                    <telerik:EditorDropDownItem Name="Employer Bill To Address Name" Value="#EmployerBillToAddressName#" />
                                    <telerik:EditorDropDownItem Name="Employer Bill To Address" Value="#EmployerBillToAddress#" />
                                    <telerik:EditorDropDownItem Name="Employer Bill To City" Value="#EmployerBillToCity#" />
                                    <telerik:EditorDropDownItem Name="Employer Bill To Company Name" Value="#EmployerBillToCompanyName#" />
                                    <telerik:EditorDropDownItem Name="Employer Bill To Country" Value="#EmployerBillToCountry#" />
                                    <telerik:EditorDropDownItem Name="Employer Bill To Postal" Value="#EmployerBillToPostal#" />
                                    <telerik:EditorDropDownItem Name="Employer Bill To State" Value="#EmployerBillToState#" />
                                    <telerik:EditorDropDownItem Name="Employer Bill To Street" Value="#EmployerBillToStreet#" />
                                    <telerik:EditorDropDownItem Name="Employer Organization Name" Value="#EmployerOrganizationName#" />
                                    <telerik:EditorDropDownItem Name="Employer Organization Phone" Value="#EmployerOrganizationPhone#" />
                                    <telerik:EditorDropDownItem Name="Employer Ship To Address Name" Value="#EmployerShipToAddressName#" />
                                    <telerik:EditorDropDownItem Name="Employer Ship To Address" Value="#EmployerShipToAddress#" />
                                    <telerik:EditorDropDownItem Name="Employer Ship To City" Value="#EmployerShipToCity#" />
                                    <telerik:EditorDropDownItem Name="Employer Ship To Company Name" Value="#EmployerShipToCompanyName#" />
                                    <telerik:EditorDropDownItem Name="Employer Ship To Country" Value="#EmployerShipToCountry#" />
                                    <telerik:EditorDropDownItem Name="Employer Ship To Postal" Value="#EmployerShipToPostal#" />
                                    <telerik:EditorDropDownItem Name="Employer Ship To State" Value="#EmployerShipToState#" />
                                    <telerik:EditorDropDownItem Name="Employer Ship To Street" Value="#EmployerShipToStreet#" />
                                    <telerik:EditorDropDownItem Name="Inventory Status" Value="#InventoryStatus#" />
                                    <telerik:EditorDropDownItem Name="Logo" Value="#Logo#" />
                                    
                                    <telerik:EditorDropDownItem Name="Opp/Order Bill To Address Name" Value="#OppOrderBillToAddressName#" />
                                    <telerik:EditorDropDownItem Name="Opp/Order Bill To Address" Value="#OppOrderBillToAddress#" />
                                    <telerik:EditorDropDownItem Name="Opp/Order Bill To City" Value="#OppOrderBillToCity#" />
                                    <telerik:EditorDropDownItem Name="Opp/Order Bill To Company Name" Value="#OppOrderBillToCompanyName#" />
                                    <telerik:EditorDropDownItem Name="Opp/Order Bill To Country" Value="#OppOrderBillToCountry#" />
                                    <telerik:EditorDropDownItem Name="Opp/Order Bill To Postal" Value="#OppOrderBillToPostal#" />
                                    <telerik:EditorDropDownItem Name="Opp/Order Bill To State" Value="#OppOrderBillToState#" />
                                    <telerik:EditorDropDownItem Name="Opp/Order Bill To Street" Value="#OppOrderBillToStreet#" />

                                    <telerik:EditorDropDownItem Name="Opp/Order Ship To Address Name" Value="#OppOrderShipToAddressName#" />
                                    <telerik:EditorDropDownItem Name="Opp/Order Ship To Address" Value="#OppOrderShipToAddress#" />
                                    <telerik:EditorDropDownItem Name="Opp/Order Ship To City" Value="#OppOrderShipToCity#" />
                                    <telerik:EditorDropDownItem Name="Opp/Order Ship To Company Name" Value="#OppOrderShipToCompanyName#" />
                                    <telerik:EditorDropDownItem Name="Opp/Order Ship To Country" Value="#OppOrderShipToCountry#" />
                                    <telerik:EditorDropDownItem Name="Opp/Order Ship To Postal" Value="#OppOrderShipToPostal#" />
                                    <telerik:EditorDropDownItem Name="Opp/Order Ship To State" Value="#OppOrderShipToState#" />
                                    <telerik:EditorDropDownItem Name="Opp/Order Ship To Street" Value="#OppOrderShipToStreet#" />
                                    <telerik:EditorDropDownItem Name="Order ID Barcode" Value="#OrderIDBarCode#" />
                                    <telerik:EditorDropDownItem Name="Order Record Owner" Value="#OrderRecOwner#" />
                                    <telerik:EditorDropDownItem Name="Opp/Order Created Date" Value="#OrderCreatedDate#" />

                                    <telerik:EditorDropDownItem Name="Packing Slip ID" Value="#PackingSlipID#" />
                                    <telerik:EditorDropDownItem Name="PO/Invoice #" Value="#P.O.NO#" />
                                    <telerik:EditorDropDownItem Name="Products" Value="#Products#" />
                                     <telerik:EditorDropDownItem Name="Parcel Shipping Account #" Value="#ParcelShippingAccount#" />  

                                     <telerik:EditorDropDownItem Name="Reference #" Value="#Reference#" />                                  

                                    <telerik:EditorDropDownItem Name="Ship Via" Value="#ShippingCompany#" />
                                    <telerik:EditorDropDownItem Name="Shipping Service" Value="#ShippingService#" />                                      
                                    <telerik:EditorDropDownItem Name="ShippingContact" Value="#ShippingContact#" />
                                    <telerik:EditorDropDownItem Name="SO-Comments" Value="#SOComments#" />

                                    <telerik:EditorDropDownItem Name="Total Quantity" Value="#TotalQuantity#" />
                                    <telerik:EditorDropDownItem Name="Tracking Numbers" Value="#TrackingNo#" />
                                    <telerik:EditorDropDownItem Name="Total Qyt by UOM" Value="#TotalQytbyUOM#" />
                                </telerik:EditorDropDown>
                                <telerik:EditorDropDown Name="CustomField" Text="Custom Field">
                                </telerik:EditorDropDown>
                            </telerik:EditorToolGroup>
                        </Tools>
                    </telerik:RadEditor>
                    <div id="dvsac"></div>
                </td>
            </tr>
            <tr>
                <td class="text" align="right" valign="top"><b>Paste your BizDoc Style here:</b>
                </td>
                <td colspan="2">
                    <asp:TextBox ID="txtCSS" runat="server" TextMode="MultiLine" Width="600" Height="200">
                    </asp:TextBox>
                </td>
            </tr>
        </table>
        <asp:HiddenField ID="hdfTemplateID" Value="0" runat="server" />
        <asp:HiddenField ID="hdfTemplateType" Value="0" runat="server" />
        <asp:HiddenField ID="hdfTemplateName" Value="0" runat="server" />
        <hr />


        <br />
        <br />

    </div>
    <asp:UpdateProgress ID="UpdateProgress" runat="server" ClientIDMode="Static">
        <ProgressTemplate>
            <div class="overlay">
                <div class="overlayContent" style="color: #000; text-align: center; width: 250px; padding: 20px">
                    <i class="fa fa-2x fa-refresh fa-spin"></i>
                    <h3>Processing Request</h3>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="UtilityLinksPanel" runat="server">
</asp:Content>
