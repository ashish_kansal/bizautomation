<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmTaxDetails.aspx.vb"
    MasterPageFile="~/common/PopupBootstrapBlank.Master" Inherits="BACRM.UserInterface.Admin.frmTaxDetails" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Tax Details</title>
    <script type="text/javascript">
        $(document).ready(function () {
            var tab = $("[id$=hdnSelectedTab]").val();
            activaTab(tab);

            $('.nav-tabs a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
                var target = $(e.target).attr("href") // activated tab
                $("[id$=hdnSelectedTab]").val(target);
            });

            $("[id$=chkPurchaseTaxCredit]").change(function () {
                $.ajax({
                    type: "POST",
                    url: '../WebServices/CommonService.svc/UpdateSingleFieldValue',
                    contentType: "application/json",
                    dataType: "json",
                    data: JSON.stringify({
                        "mode": 57
                        , "updateRecordID": 0
                        , "updateValueID": ($(this).is(":checked") ? 1 : 0)
                        , "comments": ""
                    }),
                    beforeSend: function () {
                        $("#UpdateProgress").show();
                    },
                    complete: function () {
                        $("#UpdateProgress").hide();
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        alert("Unknown error ocurred");
                    }
                });
            });
        })

        function OpenTaxItem() {
            window.open('../admin2/frmTaxWizard.aspx', "", "width=800,height=400,status=no,scrollbars=yes,left=155,top=160");
            return false;
        }
        function DeleteRecord() {
            if (confirm('Are you sure, you want to delete the selected record?')) {
                return true;
            }
            else {
                return false;
            }
        }
        function CheckNumber(cint, e) {
            var k;
            document.all ? k = e.keyCode : k = e.which;
            if (cint == 1) {
                if (!(k > 47 && k < 58 || k == 44 || k == 46 || k == 8 || k == 39 || k == 16)) {
                    if (e.preventDefault) {
                        e.preventDefault();
                    }
                    else
                        e.returnValue = false;
                    return false;
                }
            }
            if (cint == 2) {
                if (!(k > 47 && k < 58 || k == 8 || k == 37 || k == 39 || k == 16)) {
                    if (e.preventDefault) {
                        e.preventDefault();
                    }
                    else
                        e.returnValue = false;
                    return false;
                }
            }
        }

        function SaveTaxType() {
            if ($("[id$=txtTaxName").val() == "") {
                alert("Enter Tax Type");
                document.form1.txtTaxName.focus();
                return false;
            }
            if ($("[id$=ddlTaxAccount").val() == 0) {
                alert("Please Select Tax Account");
                document.form1.ddlTaxAccount.focus();
                return false;
            }

            return true;
        }

        function DeleteTaxType() {
            if (confirm('Are you sure, you want to delete the selected record?')) {
                return true;
            }
            else {
                return false;
            }
        }

        function activaTab(tab) {
            $('.nav-tabs a[href="' + tab + '"]').tab('show');
        };

        function setDllValues() {
            if ($('[id$=tbldtls]').find("tr").length < 2) {
                alert("Please Upload the file first")
                return false;
            }
            var intCnt;
            inCnt = $('[id$=txtDllValue]').val();
            $('[id$=txtDllValue]').val("");
            var dllValue;
            dllValue = "";
            for (var x = 0; x <= inCnt; x++) {
                if (dllValue != "") {
                    dllValue = dllValue + "," + $("[id$=ddlDestination" + x + "]").val();
                }
                else {
                    dllValue = $("[id$=ddlDestination" + x + "]").val();
                }
            }
            $('[id$=txtDllValue]').val(dllValue);
        }

        function checkFileExt() {

            if (document.getElementById('txtFile').value.length == 0) {
                alert("Plz Select File");
                return false;
            }
            else {

                var data = document.getElementById('txtFile').value

                var extension = data.substr(data.lastIndexOf('.'));

                if (extension == '.csv' || extension == '.CSV') {

                    return true;
                }
                else {
                    alert("Plz Select a .csv File");
                    return false;
                }

            }
        }

        function displayPBar() {

            if ($('[id$=tbldtls]').find("tr").length <= 2) {
                alert("please click on display records before importing")
                return false;
            }
            if (document.getElementById('pgBar').style.display == "none")
                document.getElementById('pgBar').style.display = "";
            else
                document.getElementById('pgBar').style.display = "none";
        }
    </script>
    <style type="text/css">
        .tblInsertTax tr {
            height: 30px;
        }

        .tblInsertTax > tbody > tr > td:first-child, .tblInsertTax > tbody > tr > td:nth-child(3) {
            font-weight: bold;
            white-space: nowrap;
            text-align: right;
            width: 15%;
        }
    </style>
</asp:Content>
<asp:Content ContentPlaceHolderID="Content" runat="server">
    <asp:ScriptManager runat="server"></asp:ScriptManager>
    <asp:UpdateProgress ID="UpdateProgress" runat="server" ClientIDMode="Static">
        <ProgressTemplate>
            <div class="overlay" style="z-index: 10000">
                <div class="overlayContent" style="color: #000; text-align: center; width: 250px; padding: 20px; background-color: #fff">
                    <i class="fa fa-2x fa-refresh fa-spin"></i>
                    <h3>Processing Request</h3>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <div class="container modal-lg" style="padding: 10px;">
        <div class="row padbottom10">
            <div class="col-xs-12">
                <div class="pull-left">
                    <b style="font-size: 18px">Configure Taxes</b>
                </div>
                <div class="pull-right">
                    <asp:CheckBox runat="server" ID="chkPurchaseTaxCredit" Text="Calculate Taxes on Purchases (e.g. P.O.s)" />
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="panel with-nav-tabs panel-primary">
                    <div class="panel-heading">
                        <ul class="nav nav-tabs" data-tabs="tabs">
                            <li class="active"><a data-toggle="tab" href="#tab1">Tax Tables</a></li>
                            <li><a data-toggle="tab" href="#tab2">Upload Tax Tables</a></li>
                            <li><a data-toggle="tab" href="#tab3">Custom Tax Types</a></li>
                        </ul>
                    </div>
                    <div class="panel-body">
                        <div class="tab-content">
                            <div id="tab1" class="tab-pane fade active in">
                                <div class="row">
                                    <div class="col-sm-12 col-md-3">
                                        <div class="form-group">
                                            <label class="text-right">Country</label>
                                            <asp:DropDownList ID="ddlCountry" AutoPostBack="true" runat="server" CssClass="form-control">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-md-3">
                                        <div class="form-group">
                                            <label>Base tax on</label>
                                            <div>
                                                <asp:RadioButtonList ID="rblBaseTaxOnArea" runat="server" RepeatDirection="Horizontal"
                                                    CssClass="signup">
                                                    <asp:ListItem Text="State" Value="0" Selected="True"></asp:ListItem>
                                                    <asp:ListItem Text="City" Value="1"></asp:ListItem>
                                                    <asp:ListItem Text="Zip Code/Postal" Value="2"></asp:ListItem>
                                                </asp:RadioButtonList>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-md-2">
                                        <div class="form-group">
                                            <label>State</label>
                                            <asp:DropDownList ID="ddlState" runat="server" CssClass="form-control">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-md-2">
                                        <div class="form-group">
                                            <label>City</label>
                                            <asp:TextBox runat="server" ID="txtCity" MaxLength="100" CssClass="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-md-2">
                                        <div class="form-group">
                                            <label>Zip/Postal</label>
                                            <asp:TextBox runat="server" ID="txtZipPostal" MaxLength="20" CssClass="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="row padbottom10">
                                    <div class="col-sm-12 col-md-3">
                                        <div class="form-group">
                                            <label>Tax Type</label>
                                            <asp:DropDownList ID="ddlTaxType" runat="server" CssClass="form-control" AutoPostBack="true">
                                            </asp:DropDownList>
                                            <asp:TextBox ID="txtTaxUniqueName" runat="server" Visible="false" CssClass="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-md-5">
                                        <div class="form-group">
                                            <label>Tax</label>
                                            <ul class="list-inline">
                                                <li style="vertical-align: top">
                                                    <asp:TextBox runat="server" CssClass="form-control" ID="txtTax" onkeypress="CheckNumber(1,event);"></asp:TextBox></li>
                                                <li style="vertical-align: middle">
                                                    <asp:RadioButtonList Style="margin-top: 10px;" ID="rdblTaxValueType" runat="server" RepeatDirection="Horizontal" CellPadding="0" CellSpacing="0">
                                                        <asp:ListItem Text="%" Value="1" Selected="True" />
                                                        <asp:ListItem Text="Flat Amount" Value="2" />
                                                    </asp:RadioButtonList>
                                                </li>
                                            </ul>

                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-md-4" style="line-height: 68px;">
                                        <asp:Button runat="server" ID="btnSave" Text="Add" CssClass="btn btn-primary" />
                                    </div>
                                </div>
                                <div class="row padbottom10">
                                    <div class="col-xs-12">
                                        <div class="pull-left">
                                            <ul class="list-inline">
                                                <li><b>Filters:</b></li>
                                                <li>
                                                    <div class="form-inline">
                                                        <div class="form-group">
                                                            <label>Tax Type</label>
                                                            <asp:DropDownList ID="ddlSearchTaxType" runat="server" CssClass="form-control" AutoPostBack="true">
                                                            </asp:DropDownList>
                                                        </div>
                                                        <div class="form-group">
                                                            <label>Country</label>
                                                            <asp:DropDownList ID="ddlSearchCountry" CssClass="form-control" AutoPostBack="true" runat="server"></asp:DropDownList>
                                                        </div>
                                                        <div class="form-group">
                                                            <label>State</label>
                                                            <asp:DropDownList ID="ddlSearchState" runat="server" CssClass="form-control" AutoPostBack="true">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="pull-right">
                                            <asp:Button runat="server" ID="btnDelete" CssClass="btn btn-danger" OnClientClick="return DeleteRecord();" Text="Delete" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row padbottom10">
                                    <div class="col-xs-12">
                                        <div class="table-responsive">
                                            <asp:GridView ID="gvTax" runat="server" Width="100%" CssClass="table table-bordered table-primary" AllowSorting="true"
                                                AutoGenerateColumns="False" DataKeyNames="numTaxId" RowStyle-HorizontalAlign="Center" UseAccessibleHeader="true">
                                                <Columns>
                                                    <asp:BoundField DataField="vcTaxName" HeaderText="Tax Type"></asp:BoundField>
                                                    <asp:BoundField DataField="country" HeaderText="Country"></asp:BoundField>
                                                    <asp:BoundField DataField="state" HeaderText="State"></asp:BoundField>
                                                    <asp:BoundField DataField="vccity" HeaderText="City"></asp:BoundField>
                                                    <asp:BoundField DataField="vcZipPostal" HeaderText="Zip/Postal"></asp:BoundField>
                                                    <asp:BoundField DataField="vcTax" HeaderText="Tax"></asp:BoundField>
                                                    <asp:TemplateField ItemStyle-HorizontalAlign="Center">
                                                        <HeaderTemplate>
                                                            <asp:CheckBox ID="chkAll" onclick="SelectAll('chkAll','chk')" runat="server" />
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:CheckBox ID="chkSelect" CssClass="chk" runat="server" />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="tab2" class="tab-pane fade">
                                <div id="divImportMessage" runat="server" class="row" style="display: none">
                                    <div class="col-xs-12">
                                        <div class="alert alert-warning">
                                            <h4><i class="icon fa fa-warning"></i>Alert!</h4>
                                            <asp:Literal ID="litMessage" runat="server"></asp:Literal>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12 text-center">
                                        <img src="../images/pgBar.gif" runat="server" id="pgBar" style="display:none" />
                                    </div>
                                </div>
                                <div class="row padbottom10">
                                    <div class="col-xs-12">
                                        <b>Step 1 - <i>Download csv template</i></b>
                                        <hr style="margin-top: 5px;margin-bottom:5px;" />
                                        <a href="ImportTaxDetails.csv">Download CSV Template</a> or
                                        <asp:LinkButton ID="hplExistingTax" runat="server" CssClass="signup">Download CSV Template with existing Tax</asp:LinkButton>
                                    </div>
                                </div>
                                <br />
                                <div class="row padbottom10">
                                    <div class="col-xs-12">
                                        <b>Step 2 - <i>Upload csv file with data</i></b>
                                        <hr style="margin-top: 5px;margin-bottom:5px;" />
                                        <div class="form-inline">
                                            <div class="form-group">
                                                <input runat="server" id="txtFile" type="file" class="signup" />
                                            </div>
                                            <asp:Button runat="server" ID="btnUpload" Text="Upload" CssClass="btn btn-primary"></asp:Button>
                                        </div>
                                    </div>
                                </div>
                                <br />
                                <div class="row padbottom10">
                                    <div class="col-xs-12">
                                        <b>Step 3 - <i>Mapping</i></b>
                                        <hr style="margin-top: 5px;margin-bottom:5px;" />
                                        <asp:TextBox ID="txtDllValue" runat="server" Height="16px" Style="display: none"></asp:TextBox>
                                        <div style="min-height:100px; max-height: 250px; width:100%; overflow: scroll;">
                                            <asp:Table ID="tbldtls" runat="server" Width="100%" GridLines="none" BorderWidth="0" CellSpacing="1">
                                            </asp:Table>
                                        </div>
                                    </div>
                                </div>
                                <br />
                                <div class="row padbottom10">
                                    <div class="col-xs-12">
                                        <b>Step 4</b>
                                        <hr style="margin-top: 5px;margin-bottom:5px;" />
                                        <asp:Button ID="btnDisplay" runat="server" Text="Display Records" CssClass="btn btn-primary"></asp:Button>
                                    </div>
                                </div>
                                <br />
                                <div class="row padbottom10">
                                    <div class="col-xs-12">
                                        <b>Step 5 - <i>Import csv</i></b>
                                        <hr style="margin-top: 5px;margin-bottom:5px;" />
                                        <asp:CheckBox runat="server" ID="chkDeleteExisting" CssClass="signup" Text="Delete existing tax before insert" />
                                        <asp:Button ID="btnImports" runat="server" Text="Import Records to database" CssClass="btn btn-primary"></asp:Button>
                                    </div>
                                </div>
                            </div>
                            <div id="tab3" class="tab-pane fade">
                                <div class="row padbottom10">
                                    <div class="col-xs-12">
                                        <div class="form-inline">
                                            <div class="form-group">
                                                <label>Tax Type<span class="text-red"> *</span></label>
                                                <asp:TextBox ID="txtTaxName" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                            <div class="form-group">
                                                <label>Tax Account<span class="text-red"> *</span></label>
                                                <asp:DropDownList ID="ddlTaxAccount" runat="server" CssClass="form-control">
                                                </asp:DropDownList>
                                            </div>
                                            <asp:Button ID="btnAddTaxType" runat="server" Text="Create Tax Item" CssClass="btn btn-primary" OnClick="btnAddTaxType_Click" OnClientClick="return SaveTaxType();" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row padbottom10">
                                    <div class="col-xs-12">
                                        <div class="table-responsive">
                                            <asp:DataGrid ID="dgTaxItems" AutoGenerateColumns="false" CssClass="table table-bordered table-striped" runat="server" UseAccessibleHeader="true" Width="100%" OnItemDataBound="dgTaxItems_ItemDataBound">
                                                <Columns>
                                                    <asp:BoundColumn DataField="" Visible="false"></asp:BoundColumn>
                                                    <asp:TemplateColumn HeaderText="Tax Type" Visible="false">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblTaxItemID" runat="server" Text='<%# DataBinder.Eval(Container,"DataItem.numTaxItemID") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:TemplateColumn HeaderText="Tax Type">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblTaxName" runat="server" Text='<%# DataBinder.Eval(Container,"DataItem.vcTaxName") %>'></asp:Label>
                                                        </ItemTemplate>
                                                        <EditItemTemplate>
                                                            <asp:TextBox ID="txtTaxName" CssClass="form-control" runat="server" Text='<%# DataBinder.Eval(Container,"DataItem.vcTaxName") %>'></asp:TextBox>
                                                        </EditItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:TemplateColumn HeaderText="Account Name">
                                                        <ItemTemplate>
                                                            <%# Eval("vcAccountName") %>
                                                        </ItemTemplate>
                                                        <EditItemTemplate>
                                                        </EditItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:TemplateColumn HeaderStyle-Width="160" ItemStyle-Width="160" ItemStyle-HorizontalAlign="Center">
                                                        <ItemTemplate>
                                                            <asp:LinkButton runat="server" CommandName="Edit" CssClass="btn btn-xs btn-info" ID="lnkbtnEdt"><i class="fa fa-pencil"></i></asp:LinkButton>
                                                            <asp:LinkButton runat="server" ID="lkbDelete" CssClass="btn btn-xs btn-danger" CommandName="Delete" OnClientClick="return DeleteTaxType()"><i class="fa fa-trash-o"></i></asp:LinkButton>
                                                        </ItemTemplate>
                                                        <EditItemTemplate>
                                                            <asp:LinkButton ID="lnkbtnUpdt" runat="server" CssClass="btn btn-primary" Text="Update" CommandName="Update"></asp:LinkButton>&nbsp;
                                                            <asp:LinkButton runat="server" Text="Cancel" CssClass="btn btn-primary" CommandName="Cancel" ID="lnkbtnCncl"></asp:LinkButton>
                                                        </EditItemTemplate>
                                                    </asp:TemplateColumn>
                                                </Columns>
                                            </asp:DataGrid>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <asp:HiddenField runat="server" ID="hdnSelectedTab" Value="#tab1" />
    <asp:TextBox runat="server" Visible="false" ID="txtTaxId" Text="0"></asp:TextBox>
</asp:Content>

