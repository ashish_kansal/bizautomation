﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="AddStageDependency.aspx.vb"
    Inherits=".AddStageDependency" MasterPageFile="~/common/Popup.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Add MileStone Stage Dependency</title>
    <script language="javascript" type="text/javascript">
        function Close() {
            opener.location.reload(true); self.close();
        }

        function CheckDependencies() {
            if (document.getElementById("ddlProjectStage").value == 0) {
                alert('Select Stage');
                document.getElementById("ddlProjectStage").focus();
                return false;
            }
            return true;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <div class="input-part">
        <div class="right-input">
            <asp:Button ID="btnClose" Text="Close" CssClass="button" runat="server" Width="50">
            </asp:Button>&nbsp;&nbsp;
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageTitle" runat="server" ClientIDMode="Static">
    Upstream Dependencies
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="Content" runat="server" ClientIDMode="Static">
    <table>
        <tr>
            <td colspan="2" align="center">
                Stages that need to be completed before this one begins.
            </td>
        </tr>
        <tr>
            <td width="20%">
                <asp:DropDownList ID="ddlProjectStage" runat="server" CssClass="signup">
                </asp:DropDownList>
            </td>
            <td>
                <asp:Button ID="btnDependencies" runat="server" CssClass="button" Text="Add" OnClientClick="return CheckDependencies()" />
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:GridView ID="gvDependencies" runat="server" AllowSorting="true" AutoGenerateColumns="False"
                    CssClass="tbl" DataKeyNames="numStageDetailId,numDependantOnId" Width="600px">
                    <AlternatingRowStyle CssClass="is" />
                    <RowStyle CssClass="ais" />
                    <HeaderStyle CssClass="hs" />
                    <Columns>
                        <asp:BoundField HeaderText="Stage Name" DataField="vcStageName" ItemStyle-HorizontalAlign="Center" />
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:LinkButton ID="DeleteButton" runat="server" CausesValidation="False" CommandArgument="<%# Container.DataItemIndex %>"
                                    CommandName="DeleteDependency" OnClientClick="return confirm('Are you sure to delete this Dependency?');"
                                    Text="Delete"> </asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </td>
        </tr>
    </table>
</asp:Content>
