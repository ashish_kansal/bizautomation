﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmCustomReportTest.aspx.vb"
    Inherits=".frmCustomReportTest" MasterPageFile="~/common/GridMasterRegular.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Custom Report Test</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server"
    ClientIDMode="Static">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server" ClientIDMode="Static">
    Custom Report Test
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BizPager" runat="server" ClientIDMode="Static">
    &nbsp;
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="GridPlaceHolder" runat="server"
    ClientIDMode="Static">
    <asp:Table ID="Table2" runat="server" GridLines="None" Width="100%" CssClass="aspTable"
        CellSpacing="0" CellPadding="0">
        <asp:TableRow>
            <asp:TableCell VerticalAlign="Top">
                <table width="100%" border="0">
                    <tr>
                        <td class="text" align="left">
                            Domain Id &nbsp;<asp:TextBox ID="txtDomainId" runat="server" Text="1"></asp:TextBox>
                            &nbsp;
                            <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="button"></asp:Button>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:GridView ID="gvCustomReports" runat="server" EnableViewState="true" AutoGenerateColumns="false"
                                CssClass="tbl" Width="100%" DataKeyNames="numCustomReportID">
                                <%--<AlternatingRowStyle CssClass="ais" />
                                <RowStyle CssClass="is" />
                                <HeaderStyle CssClass="hs" />--%>
                                <Columns>
                                    <asp:BoundField DataField="numCustomReportID" Visible="true" HeaderText="CustomReportID" />
                                    <asp:BoundField DataField="vcReportName" Visible="true" HeaderText="Report Name" />
                                    <asp:BoundField DataField="numDomainID" Visible="true" HeaderText="DomainID" />
                                    <asp:BoundField DataField="numCreatedBy" Visible="true" HeaderText="Created By" />
                                    <asp:BoundField DataField="numROWCOUNT" Visible="true" HeaderText="Row Count" />
                                    <asp:BoundField DataField="vcError" Visible="true" HeaderText="Error" />
                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
</asp:Content>
