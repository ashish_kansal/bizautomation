﻿Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Admin

Public Class frmPortalDashboard
    Inherits BACRMPage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then

                objCommon.sb_FillComboFromDBwithSel(ddlRelationship, 5, Session("DomainID"))
                objCommon.sb_FillComboFromDBwithSel(ddlProfile, 21, Session("DomainID"))
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
    Private Sub BindData()
        Try
            Dim objAdmin As New UserAccess
            objAdmin.DomainID = Session("DomainID")
            objAdmin.Relationship = ddlRelationship.SelectedValue
            objAdmin.ProfileID = ddlProfile.SelectedValue
            Dim ds As DataSet = objAdmin.GetPortalDashboardReports
            rptrAvailableReports.DataSource = ds.Tables(0)
            rptrAvailableReports.DataBind()

            rptrCol1.DataSource = ds.Tables(1)
            rptrCol1.DataBind()

            rptrCol2.DataSource = ds.Tables(2)
            rptrCol2.DataBind()
            If Not ClientScript.IsStartupScriptRegistered("MakeDragable") Then
                ClientScript.RegisterStartupScript(Me.GetType, "MakeDragable", "MakeDragable();", True)
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    
    Private Sub ddlRelationship_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlRelationship.SelectedIndexChanged
        Try
            BindData()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub ddlProfile_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlProfile.SelectedIndexChanged
        Try
            BindData()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub btnSaveClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveClose.Click
        Try
            Save()
            Page.ClientScript.RegisterStartupScript(Me.GetType, "close", "Close();", True)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            Save()
            BindData()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
    Private Sub Save()
        Try
            Dim dt As New DataTable
            dt.Columns.Add("numRelationshipID")
            dt.Columns.Add("numProfileID")
            dt.Columns.Add("numReportID")
            dt.Columns.Add("tintColumnNumber")
            dt.Columns.Add("tintRowOrder")

            Dim str() As String = txthidden.Text.Split(":")
            Dim strCol() As String = str(1).TrimStart("col1(".ToCharArray()).TrimEnd(")").Split(",")
            Dim dr As DataRow

            For i As Integer = 0 To strCol.Length - 1
                If CCommon.ToLong(strCol(i)) > 0 Then
                    dr = dt.NewRow()
                    dr("numReportID") = strCol(i)
                    dr("numRelationshipID") = ddlRelationship.SelectedValue
                    dr("numProfileID") = ddlProfile.SelectedValue
                    dr("tintRowOrder") = i + 1
                    dr("tintColumnNumber") = "1"
                    dt.Rows.Add(dr)
                End If
            Next

            strCol = str(2).TrimStart("col2(".ToCharArray()).TrimEnd(")").Split(",")
            For i As Integer = 0 To strCol.Length - 1
                If CCommon.ToLong(strCol(i)) > 0 Then
                    dr = dt.NewRow()
                    dr("numReportID") = strCol(i)
                    dr("numRelationshipID") = ddlRelationship.SelectedValue
                    dr("numProfileID") = ddlProfile.SelectedValue
                    dr("tintRowOrder") = i + 1
                    dr("tintColumnNumber") = "2"
                    dt.Rows.Add(dr)
                End If
            Next
            Dim ds As New DataSet
            ds.Tables.Add(dt)

            Dim objAdmin As New UserAccess
            objAdmin.StrItems = ds.GetXml
            objAdmin.DomainID = Session("DomainID")
            objAdmin.Relationship = ddlRelationship.SelectedValue
            objAdmin.ProfileID = ddlProfile.SelectedValue
            objAdmin.ManagePortalDashboardReports()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
End Class