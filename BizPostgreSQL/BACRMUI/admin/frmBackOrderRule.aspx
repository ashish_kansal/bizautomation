﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/common/Popup.Master" CodeBehind="frmBackOrderRule.aspx.vb" Inherits=".frmBackOrderRule" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    
     <link rel="stylesheet" href="../CSS/bootstrap.min.css" />
    <link rel="stylesheet" href="../CSS/font-awesome.min.css" />
    <link rel="stylesheet" href="../CSS/biz.css" />
     
    <link rel="stylesheet" href="../CSS/jquery-ui.css" />
   
    <link rel="stylesheet" href="../CSS/select2.css" />
     <script language="javascript" type="text/javascript">
          sortitems = 0; 
          function move() {
            var fbox = $("#lstAvailableGroups")[0];
            var tbox = $("#lstSelectedGroups")[0];

            for (var i = 0; i < fbox.options.length; i++) {
                if (fbox.options[i].selected && fbox.options[i].value != "") {

                    /// to check for duplicates 
                    for (var j = 0; j < tbox.options.length; j++) {
                        if (tbox.options[j].value == fbox.options[i].value) {
                            alert("Item is already selected");
                            return false;
                        }
                    }

                    var no = new Option();
                    no.value = fbox.options[i].value;
                    no.text = fbox.options[i].text;
                    tbox.options[tbox.options.length] = no;
                    fbox.options[i].value = "";
                    fbox.options[i].text = "";

                }
            }
            BumpUp(fbox);
            if (sortitems) SortD(tbox);
            return false;
        }

        function remove() {
            var fbox = $("#lstSelectedGroups")[0];
            var tbox = $("#lstAvailableGroups")[0];

            for (var i = 0; i < fbox.options.length; i++) {
                if (fbox.options[i].selected && fbox.options[i].value != "") {
                    /// to check for duplicates 
                    for (var j = 0; j < tbox.options.length; j++) {
                        if (tbox.options[j].value == fbox.options[i].value) {
                            fbox.options[i].value = "";
                            fbox.options[i].text = "";
                            BumpUp(fbox);
                            if (sortitems) SortD(tbox);
                            return false;

                        }
                    }

                    var no = new Option();
                    no.value = fbox.options[i].value;
                    no.text = fbox.options[i].text;
                    tbox.options[tbox.options.length] = no;
                    fbox.options[i].value = "";
                    fbox.options[i].text = "";

                }
            }
            BumpUp(fbox);
            if (sortitems) SortD(tbox);
            return false;
        }

        function BumpUp(box) {
            for (var i = 0; i < box.options.length; i++) {
                if (box.options[i].value == "") {
                    for (var j = i; j < box.options.length - 1; j++) {
                        box.options[j].value = box.options[j + 1].value;
                        box.options[j].text = box.options[j + 1].text;
                    }
                    var ln = i;
                    break;
                }
            }
            if (ln < box.options.length) {
                box.options.length -= 1;
                BumpUp(box);
            }
        }

        function SortD(box) {
            var temp_opts = new Array();
            var temp = new Object();
            for (var i = 0; i < box.options.length; i++) {
                temp_opts[i] = box.options[i];
            }
            for (var x = 0; x < temp_opts.length - 1; x++) {
                for (var y = (x + 1) ; y < temp_opts.length; y++) {
                    if (temp_opts[x].text > temp_opts[y].text) {
                        temp = temp_opts[x].text;
                        temp_opts[x].text = temp_opts[y].text;
                        temp_opts[y].text = temp;
                        temp = temp_opts[x].value;
                        temp_opts[x].value = temp_opts[y].value;
                        temp_opts[y].value = temp;
                    }
                }
            }
            for (var i = 0; i < box.options.length; i++) {
                box.options[i].value = temp_opts[i].value;
                box.options[i].text = temp_opts[i].text;
            }
         }

         function Save() {
               str = '';
            for (var i = 0; i < document.getElementById('lstSelectedGroups').options.length; i++) {
                var SelectedText, SelectedValue;
                SelectedValue = document.getElementById('lstSelectedGroups').options[i].value;
                SelectedText = document.getElementById('lstSelectedGroups').options[i].text;
                str = str + SelectedValue + ','
            }
            document.getElementById('hdnSelectedGroups').value = str;

            return true;
         }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server" ClientIDMode="Static">
     <div class="input-part">
        <div class="right-input">
            <asp:Button runat="server" ID="btnSaveClose" OnClientClick="return Save()" OnClick="btnSaveClose_Click" Text="Save & Close" CssClass="button" Width="50" />
        </div>
    </div>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="PageTitle" runat="server" ClientIDMode="Static">
    Back-Order rule (Internal sales orders only)
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="Content" runat="server" ClientIDMode="Static">
    <asp:ScriptManager ID="sp" runat="server"></asp:ScriptManager>
<asp:UpdatePanel ID="UpdatePanel1" runat="server" ChildrenAsTriggers="true">
<ContentTemplate>
    <table style="width:700px;">
        <tr>
            <td colspan="3" style="padding-bottom:20px">
                <i>
                    Users belonging to the selected permission groups will not be allowed to add items on backorder while creating new sales orders
                </i>
            </td>
        </tr>
        <tr>
            <td><b>Permission Groups Available</b><br>
                 <asp:ListBox ID="lstAvailableGroups" runat="server" Height="200px" Width="200px" CssClass="form-control"
                 EnableViewState="False"></asp:ListBox>
            </td>
            <td  align="center" valign="middle" width="10%">               
                <asp:LinkButton ID="btnAddGroup" runat="server" ClientIDMode="Static" CssClass="btn btn-primary" OnClientClick="return move();">Add&nbsp;&nbsp;<i class="fa fa-arrow-right"></i></asp:LinkButton>                      
                    <br />
                    <br />                     
                <asp:LinkButton ID="btnRemoveGroup" runat="server" ClientIDMode="Static" CssClass="btn btn-danger" OnClientClick="return remove();"><i class="fa fa-arrow-left"></i>&nbsp;&nbsp;Remove</asp:LinkButton>
                      
            </td>
             <td align="center" class="normal1"><b>Selected Permission Groups</b><br>
                <asp:ListBox ID="lstSelectedGroups" runat="server"  Height="200px" Width="200px" CssClass="form-control"
                    EnableViewState="False"></asp:ListBox>
                 <asp:HiddenField ID="hdnSelectedGroups" runat="server" />
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <br />
                <i><b>Note</b> – This setting doesn’t impact your Biz-Commerce web-store(s) which doesn’t allow backorders unless enabled to do so. To enable backorders on Biz-Commerce you need to either open the Ecommerce tab within the item record and select the “Allow backorder in e-Commerce” check box, or to batch update that check box, you can use the mass update function button from an item search result in advanced search, or by using mass update via the import & update wizard. 
</i>
            </td>
        </tr>
    </table>
</ContentTemplate>
</asp:UpdatePanel>
</asp:Content>
