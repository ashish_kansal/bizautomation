﻿Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Opportunities

Public Class frmLandedCostVendors
    Inherits System.Web.UI.Page

    Private Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Try
            CCommon.InitializeOrganizationClientSideTemplate(Session("DomainID"), Session("UserContactID"), radCmbCompany)
            CCommon.UpdateItemRadComboValues("1", radCmbCompany.SelectedValue)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
        End Try
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then
                BindDatagrid()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
    Sub BindDatagrid()
        Try
            Dim objLC As New LandedCost
            objLC.DomainID = Session("DomainID")
            Dim dtList As DataTable
            dtList = objLC.GetLandedCostVendors
            gvLCVendor.DataSource = dtList
            gvLCVendor.DataBind()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        Try
            litMessage.Text = ""

            Dim objLC As New LandedCost

            With objLC
                .DomainID = Session("DomainID")
                .VendorId = radCmbCompany.SelectedValue
                .SaveLandedCostVendors()
            End With

            BindDatagrid()
        Catch ex As Exception
            If ex.Message = "Vendor_Exists" Then
                litMessage.Text = "Vendor already exists."
            Else
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            End If
        End Try
    End Sub

    Private Sub gvLCVendor_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles gvLCVendor.RowCommand
        Try
            If e.CommandName = "DeleteVendor" Then
                Dim objLC As New LandedCost

                objLC.DomainID = Session("DomainID")
                objLC.LCVendorId = gvLCVendor.DataKeys(e.CommandArgument).Value

                objLC.DeleteLandedCostVendors()

                BindDatagrid()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
End Class