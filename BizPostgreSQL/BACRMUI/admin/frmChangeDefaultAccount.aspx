﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmChangeDefaultAccount.aspx.vb"
    Inherits=".frmChangeDefaultAccount" MasterPageFile="~/common/Popup.Master" ClientIDMode="Predictable" %>

<asp:Content ID="Content3" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>UOM - Unit Of Measurement Conversion</title>
    <script language="javascript" type="text/javascript">
        function DefaultAccounts() {
            window.open("../admin/frmDefaultAccountMapping.aspx", '', 'toolbar=no,titlebar=no,left=200, top=200,width=700,height=400,scrollbars=yes,resizable=yes')
            return false;
        }
        function ProjectAccounting() {
            window.open("../admin/frmProjectsAccountMapping.aspx", '', 'toolbar=no,titlebar=no,left=200, top=200,width=600,height=200,scrollbars=yes,resizable=yes')
            return false;
        }
        function ContactTypeAccounting() {
            window.open("../admin/frmContactTypeMapping.aspx", '', 'toolbar=no,titlebar=no,left=200, top=200,width=600,height=200,scrollbars=yes,resizable=yes')
            return false;
        }
        function Shipping() {
            window.open("../admin/frmShippingMethodMapping.aspx?1=1", '', 'toolbar=no,titlebar=no,left=200, top=200,width=400,height=150,scrollbars=yes,resizable=yes')
            return false;
        }
        function Relationship() {
            window.open("../admin/frmRelationshipMapping.aspx?1=1", '', 'toolbar=no,titlebar=no,left=200, top=200,width=650,height=250,scrollbars=yes,resizable=yes')
            return false;
        }
        function Close() {
            window.close();
        }

        function ClientSideClick() {
            var isGrpOneValid = Page_ClientValidate("vgRequired");
            var check = Check();

            var i;
            for (i = 0; i < Page_Validators.length; i++) {
                ValidatorValidate(Page_Validators[i]); //this forces validation in all groups
            }

            if (isGrpOneValid && check) {
                return true;
            }
            else
                return false;
        }

        function Check() {
            if (Number(document.getElementById("hfTotalRow").value) > 0) {
                for (var i = 1; i <= Number(document.getElementById("hfTotalRow").value) ; i++) {
                    if (document.getElementById("ddlUnit_1_" + i).value == document.getElementById("ddlUnit_2_" + i).value) {
                        alert('Base and Conversion Unit are not same!!!');
                        return false;
                    }
                    for (var j = 1; j <= Number(document.getElementById("hfTotalRow").value) ; j++) {
                        if (j != i) {
                            if ((document.getElementById("ddlUnit_1_" + i).value == document.getElementById("ddlUnit_1_" + j).value && document.getElementById("ddlUnit_2_" + i).value == document.getElementById("ddlUnit_2_" + j).value)
                                  || (document.getElementById("ddlUnit_1_" + i).value == document.getElementById("ddlUnit_2_" + j).value && document.getElementById("ddlUnit_2_" + i).value == document.getElementById("ddlUnit_1_" + j).value)) {
                                alert('Select distinct base and conversion unit!!!');
                                return false;
                            }
                        }
                    }
                }
            }
            else {
                return false;
            }

            return true;
        }
    </script>
    <style type="text/css">
        .row {
            background-color: #DBE5F1;
            text-align: left;
            color: black;
            font-family: Arial;
            font-size: 8pt;
        }

        .arow {
            background-color: #fffff;
            text-align: left;
            color: black;
            font-family: Arial;
            font-size: 8pt;
        }

        input[type='checkbox'] {
            position: relative;
            bottom: 1px;
            vertical-align: middle;
        }

        .tooltip {
            top: 0px !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <div class="input-part">
        <div class="right-input">
            <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save"
                OnClientClick="return ClientSideClick()"></asp:Button>
            <asp:Button ID="btnSaveClose" runat="server" CssClass="button" Text="Save &amp; Close"
                OnClientClick="return ClientSideClick()"></asp:Button>
            <asp:Button ID="btnCancel" runat="server" CssClass="button" Width="50" Text="Close"></asp:Button>&nbsp;&nbsp;
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="PageTitle" runat="server" ClientIDMode="Static">
    Change Default Account Mapping
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="Content" runat="server" ClientIDMode="Static">
    <table style="min-width: 450px;" >

        <tr>
            <td class="normal1" align="right" style="width: 55%;">Accounts for Relationship
            </td>
            <td align="left" class="normal1">
                <asp:HyperLink ID="hplRelationship" runat="server" Text="Set" onclick="return Relationship();"
                    CssClass="hyperlink">
                </asp:HyperLink>
                <asp:Label ID="Label10" Text="[?]" CssClass="tip" runat="server" ToolTip="Allows you to configure chart of accounts for relationship(i.e Customers,vendors).From here select the COA under which the balances of customer/Vendor will be reflected." />
            </td>
        </tr>
        <tr>
            <td class="normal1" align="right" style="width: 55%;">Accounts for Contact Type
            </td>
            <td align="left" class="normal1">
                <asp:HyperLink ID="hplContactTypeAccounts" runat="server" Text="Set" onclick="return ContactTypeAccounting();"
                    CssClass="hyperlink">
                </asp:HyperLink>
                <asp:Label ID="Label12" Text="[?]" CssClass="tip" runat="server" ToolTip="Select the default liability account from here under which the new contact will fall.Liability account set here will be used in Billable Time and Expense journal entry, helps you to keep track of liablity based on contact type. i.e employee, sub-contractor" />
            </td>
        </tr>
        <tr>
            <td class="normal1" align="right" style="width: 55%;">Accounts for Shipping Method
            </td>
            <td align="left" class="normal1">
                <asp:HyperLink ID="hplShippingAccounts" runat="server" Text="Set" onclick="return Shipping();"
                    CssClass="hyperlink">
                </asp:HyperLink>
                <asp:Label ID="Label13" Text="[?]" CssClass="tip" runat="server" ToolTip="Select the mode of shipping from here as well the default account for shipping Income under which the shipping charges received from customer will be credited." />
            </td>
        </tr>
        <tr>
            <td class="normal1" align="right" style="width: 55%;">Accounts for Project
            </td>
            <td align="left" class="normal1">
                <asp:HyperLink ID="hplProjectAccounts" runat="server" Text="Set" onclick="return ProjectAccounting();"
                    CssClass="hyperlink">
                </asp:HyperLink>
                <asp:Label ID="Label14" Text="[?]" CssClass="tip" runat="server" ToolTip="Selection of default expense account for the projects where all the expenses for the projects are charged to the default account." />
            </td>
        </tr>
        <tr>
            <td class="normal1" align="right" style="width: 55%;">Default Accounts
            </td>
            <td align="left" class="normal1">
                <asp:HyperLink ID="hplDefaultAccounts" runat="server" Text="Set" onclick="return DefaultAccounts();"
                    CssClass="hyperlink">
                </asp:HyperLink>
                <asp:Label ID="Label15" Text="[?]" CssClass="tip" runat="server" ToolTip="Selection of default account will help to allocate the entries relating to the accounts." />
            </td>
        </tr>
        <tr>
            <td class="normal1" align="right">Income Account
            </td>
            <td align="left">
                <asp:DropDownList ID="ddlIncomeAccount" runat="server" CssClass="signup" Width="200px">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="normal1" align="right">Asset Account
            </td>
            <td align="left">
                <asp:DropDownList ID="ddlAssetAccount" runat="server" CssClass="signup" Width="200px">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="normal1" align="right">COGS
            </td>
            <td align="left">
                <asp:DropDownList ID="ddlCOGS" runat="server" CssClass="signup" Width="200px">
                </asp:DropDownList>
            </td>
        </tr>
    </table>
</asp:Content>
