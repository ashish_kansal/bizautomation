﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmImportTaxDetails.aspx.vb"
    Inherits=".frmImportTaxDetails" MasterPageFile="~/common/Popup.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Imports Tax Details</title>
    <script type="text/javascript">
        function setDllValues() {
            if (document.getElementById('tbldtls').rows.length < 2) {
                alert("Please Upload the file first")
                return false;
            }
            var intCnt;
            inCnt = document.getElementById('txtDllValue').value;
            document.getElementById('txtDllValue').value = "";
            var dllValue;
            dllValue = "";
            for (var x = 0; x <= inCnt; x++) {
                if (dllValue != "") {
                    dllValue = dllValue + "," + document.getElementById('ddlDestination' + x).value;
                }
                else {
                    dllValue = document.getElementById('ddlDestination' + x).value;
                }
            }
            document.getElementById('txtDllValue').value = dllValue;
        }

        function checkFileExt() {

            if (document.getElementById('txtFile').value.length == 0) {
                alert("Plz Select File");
                return false;
            }
            else {

                var data = document.getElementById('txtFile').value

                var extension = data.substr(data.lastIndexOf('.'));

                if (extension == '.csv' || extension == '.CSV') {

                    return true;
                }
                else {
                    alert("Plz Select a .csv File");
                    return false;
                }

            }
        }

        function displayPBar() {

            if (document.getElementById('tbldtls').rows.length <= 2) {
                alert("please click on display records before importing")
                return false;
            }
            if (document.getElementById('pgBar').style.display == "none")
                document.getElementById('pgBar').style.display = "";
            else
                document.getElementById('pgBar').style.display = "none";
        }     
    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <table width="100%">
        <tr>
            <td class="normal4" align="center">
                <asp:Literal ID="litMessage" runat="server"></asp:Literal>
            </td>
        </tr>
        <tr>
            <td>
                <img src="../images/pgBar.gif" runat="server" id="pgBar">
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageTitle" runat="server" ClientIDMode="Static">
    Import Tax Details
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="Content" runat="server"
    ClientIDMode="Static">
    <asp:Table ID="Table2" CellPadding="0" CellSpacing="0" Width="100%" runat="server"
        BorderColor="black" GridLines="None" Height="400" BorderWidth="1" CssClass="aspTable">
        <asp:TableRow>
            <asp:TableCell VerticalAlign="Top">
                <br />
                <br />
                <table width="100%" cellspacing="4">
                    <tr>
                        <td class="normal1" align="right">
                            Step&nbsp;1
                        </td>
                        <td class="normal1" colspan="2">
                            <a href="ImportTaxDetails.csv">Download CSV Template</a> or
                            <asp:LinkButton ID="hplExistingTax" runat="server" CssClass="signup">Download CSV Template with existing Tax</asp:LinkButton>
                        </td>
                    </tr>
                    <tr>
                        <td class="normal1" align="right">
                            Step 2
                        </td>
                        <td class="normal1" colspan="2">
                            Source&nbsp;
                            <input runat="server" id="txtFile" type="file" class="signup">
                            &nbsp;
                            <asp:Button runat="server" ID="btnUpload" Text="Upload" CssClass="button"></asp:Button>
                            &nbsp;(Upload the CSV file to view before you save it to database)
                        </td>
                    </tr>
                    <tr>
                        <td class="normal1" align="right">
                            Step 3
                        </td>
                        <td colspan="2" class="normal1">
                            Mapping
                            <asp:TextBox ID="txtDllValue" runat="server" Height="16px" Style="display: none"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                        <td colspan="2" class="normal1">
                            <div style="height: 250px; width: 1000px; overflow: scroll;">
                                <asp:Table ID="tbldtls" runat="server" Width="100%" GridLines="none" BorderWidth="0"
                                    CellSpacing="1">
                                </asp:Table>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="normal1" align="right">
                            Step 4
                        </td>
                        <td colspan="2" class="normal1">
                            <asp:Button ID="btnDisplay" runat="server" Text="Display Records" CssClass="button"
                                Width="100"></asp:Button>
                        </td>
                    </tr>
                    <tr>
                        <td class="normal1" align="right">
                            Step 5
                        </td>
                        <td colspan="2" class="normal1">
                            <asp:CheckBox runat="server" ID="chkDeleteExisting" CssClass="signup" Text="Delete existing tax before insert" />
                            &nbsp;&nbsp;&nbsp;
                            <asp:Button ID="btnImports" runat="server" Text="Import Records to database" CssClass="button"
                                Width="170px"></asp:Button>
                        </td>
                    </tr>
                </table>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
</asp:Content>
