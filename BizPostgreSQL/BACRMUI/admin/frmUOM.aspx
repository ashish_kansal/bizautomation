﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmUOM.aspx.vb" Inherits=".frmUOM"
    MasterPageFile="~/common/Popup.Master" %>

<asp:Content ID="Content3" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Unit Of Measurement</title>
    <script type="text/javascript">
        function DeleteRecord() {
            if (confirm('Are you sure, you want to delete the selected record?')) {
                return true;
            }
            else {
                return false;
            }
        }
    </script>

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <div class="input-part">
        <div class="right-input">
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="PageTitle" runat="server" ClientIDMode="Static">
    Unit Of Measurement
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="Content" runat="server" ClientIDMode="Static">
    <table cellspacing="0" cellpadding="2" width="100%" border="0">
        <tr style="line-height: 43px">
            <td class="normal4" align="center">
                <asp:Literal ID="litMessage" runat="server"></asp:Literal>
            </td>
        </tr>
    </table>

    <asp:Table ID="Table2" runat="server" GridLines="None" BorderColor="black" Width="600px"
        BorderWidth="1" CssClass="aspTable" CellSpacing="0" CellPadding="0">
        <asp:TableRow>
            <asp:TableCell VerticalAlign="Top">
                <table id="table1" width="100%">
                    <tr>
                        <td class="normal1" align="right">Unit<font color="red">*</font>
                        </td>
                        <td class="normal1">
                            <asp:HiddenField ID="hfUnitId" runat="server" Value="0" />
                            <asp:HiddenField ID="hdnUnitType" runat="server" />
                            <asp:TextBox ID="txtUnit" runat="server" CssClass="signup" Width="250"></asp:TextBox><asp:RequiredFieldValidator
                                ID="RequiredFieldValidator1" runat="server" ErrorMessage="Unit Name Required!!!"
                                Display="Dynamic" ControlToValidate="txtUnit" ValidationGroup="vgUnit" SetFocusOnError="true"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                </table>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell VerticalAlign="Middle" HorizontalAlign="Center">
                <div>
                    <br />
                    <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" ValidationGroup="vgUnit"></asp:Button>&nbsp;&nbsp;
                    <asp:Button ID="btnSaveClose" runat="server" CssClass="button" Text="Save &amp; Close"
                        ValidationGroup="vgUnit"></asp:Button>&nbsp;&nbsp;
                    <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Close"></asp:Button>&nbsp;&nbsp;
                    <br />
                    <br />
                </div>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell VerticalAlign="Top">
                <asp:GridView ID="gvUOM" runat="server" Width="100%" CssClass="tbl" AllowSorting="true"
                    AutoGenerateColumns="False" DataKeyNames="numUOMId">
                    <AlternatingRowStyle CssClass="ais" />
                    <RowStyle CssClass="is"></RowStyle>
                    <HeaderStyle CssClass="hs"></HeaderStyle>
                    <Columns>
                        <asp:BoundField DataField="vcUnitName" HeaderText="Unit" ItemStyle-HorizontalAlign="Center" />
                        <asp:BoundField DataField="vcUnitType" HeaderText="Unit Type" ItemStyle-HorizontalAlign="Center" />
                        <asp:BoundField DataField="vcEnable" HeaderText="Enable" ItemStyle-HorizontalAlign="Center" />
                        <%--<asp:BoundField DataField="" HeaderText="Decimal Points" ItemStyle-HorizontalAlign="Center" />--%>
                        <asp:ButtonField CommandName="EditUnit" ButtonType="Link" Text="Edit" />

                        <asp:TemplateField HeaderStyle-Width="10" FooterStyle-Width="10" ItemStyle-Width="10">
                            <ItemTemplate>

                                <asp:Button ID="btnDelete" runat="server" CssClass="button Delete" Text="X" CommandName="DeleteUnit"
                                    CommandArgument='<%# Eval("numUOMId") %>' OnClientClick="return DeleteRecord();"></asp:Button>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>

</asp:Content>
