<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" CodeBehind="authentication.aspx.vb"
    Inherits="BACRM.UserInterface.Admin.authentication" MasterPageFile="~/common/GridMasterRegular.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>BACRM Authentication</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server"
    ClientIDMode="Static">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server" ClientIDMode="Static">
    Authorization failed!
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BizPager" runat="server" ClientIDMode="Static">
    &nbsp;
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="GridPlaceHolder" runat="server"
    ClientIDMode="Static">
    <table align="center" width="100%" style="height: 200px">
        <tr>
            <td class="normal13" align="center" style="color: Red" valign="middle">
                <b>You don�t have permission to use this resource. Contact your administrator</b>
                <br />
                <div runat="server" id="dvPermission" visible="false" class="info">
                    Note: Your administrator can give you permission, by going to Administration | Manage Authorization | <asp:Label Text="" runat="server" ID="lblModuleName" /> (from module drop down) | <asp:Label Text="" runat="server" ID="lblPermissionName" /> (row).
                </div>
            </td>
        </tr>
    </table>
</asp:Content>
