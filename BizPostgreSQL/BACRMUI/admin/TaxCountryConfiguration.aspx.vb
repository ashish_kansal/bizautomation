﻿Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Common
Public Class TaxCountryConfiguration
    Inherits BACRMPage

    Dim objCommon As CCommon
    Dim objTaxDtl As TaxDetails

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then
                objCommon = New CCommon
                objCommon.sb_FillComboFromDBwithSel(ddlCountry, 40, Session("DomainID"))
                BindDatagrid()
            End If
            btnCancel.Attributes.Add("onclick", "return Close()")
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
    Sub BindDatagrid()
        Try
            objTaxDtl = New TaxDetails
            objTaxDtl.DomainId = Session("DomainID")
            objTaxDtl.mode = 1

            Dim dtList As DataTable
            dtList = objTaxDtl.ManageTaxCountryConfiguration(Of DataTable)()

            gvTaxConfi.DataSource = dtList
            gvTaxConfi.DataBind()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If Page.IsValid Then
            Try
                save()
                BindDatagrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End If
    End Sub

    Private Sub btnSaveClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveClose.Click
        If Page.IsValid Then
            Try
                save()
                Dim strScript As String = "<script language=JavaScript>self.close()</script>"
                If (Not Page.IsStartupScriptRegistered("clientScript")) Then Page.RegisterStartupScript("clientScript", strScript)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End If
    End Sub

    Sub save()
        Try
            objTaxDtl = New TaxDetails
            objTaxDtl.DomainId = Session("DomainID")
            objTaxDtl.mode = 3

            objTaxDtl.Country = ddlCountry.SelectedValue
            'objTaxDtl.BaseTax = rblBaseTax.SelectedValue
            objTaxDtl.BaseTaxOnArea = rblBaseTaxOnArea.SelectedValue

            objTaxDtl.ManageTaxCountryConfiguration(Of IDataReader)(CCommon.ToLong(hfTaxCountryConfiId.Value))

            ddlCountry.Enabled = True
            ddlCountry.ClearSelection()
            ddlCountry.SelectedValue = "0"

            'rblBaseTax.ClearSelection()
            'rblBaseTax.SelectedValue = "2"

            rblBaseTaxOnArea.ClearSelection()
            rblBaseTaxOnArea.SelectedValue = "0"

            hfTaxCountryConfiId.Value = ""

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub gvTaxConfi_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvTaxConfi.RowCommand
        Try
            If e.CommandName = "EditConfi" Then
                objTaxDtl = New TaxDetails
                objTaxDtl.DomainId = Session("DomainID")
                objTaxDtl.mode = 2

                Dim objIdataReader As IDataReader
                objIdataReader = objTaxDtl.ManageTaxCountryConfiguration(Of IDataReader)(gvTaxConfi.DataKeys(e.CommandArgument).Value)

                If objIdataReader.Read Then
                    If Not ddlCountry.Items.FindByValue(objIdataReader("numCountry")) Is Nothing Then
                        ddlCountry.ClearSelection()
                        ddlCountry.Items.FindByValue(objIdataReader("numCountry")).Selected = True
                    End If

                    ddlCountry.Enabled = False

                    'If Not rblBaseTax.Items.FindByValue(objIdataReader("tintBaseTax")) Is Nothing Then
                    '    rblBaseTax.ClearSelection()
                    '    rblBaseTax.Items.FindByValue(objIdataReader("tintBaseTax")).Selected = True
                    'End If

                    If Not rblBaseTaxOnArea.Items.FindByValue(objIdataReader("tintBaseTaxOnArea")) Is Nothing Then
                        rblBaseTaxOnArea.ClearSelection()
                        rblBaseTaxOnArea.Items.FindByValue(objIdataReader("tintBaseTaxOnArea")).Selected = True
                    End If

                    hfTaxCountryConfiId.Value = objIdataReader("numTaxCountryConfi")
                End If
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
End Class