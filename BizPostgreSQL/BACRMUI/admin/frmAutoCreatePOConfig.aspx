﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmAutoCreatePOConfig.aspx.vb"
    Inherits=".frmAutoCreatePOConfig" MasterPageFile="~/common/Popup.Master" ClientIDMode="Predictable" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<asp:Content ID="Content3" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">

    <title>UOM - Unit Of Measurement Conversion</title>
    <script language="javascript" type="text/javascript">
        function Close() {
            window.close();
        }

        function ClientSideClick() {
            var isGrpOneValid = Page_ClientValidate("vgRequired");
            var check = Check();

            var i;
            for (i = 0; i < Page_Validators.length; i++) {
                ValidatorValidate(Page_Validators[i]); //this forces validation in all groups
            }

            if (isGrpOneValid && check) {
                return true;
            }
            else
                return false;
        }

        function Check() {
            if (Number(document.getElementById("hfTotalRow").value) > 0) {
                for (var i = 1; i <= Number(document.getElementById("hfTotalRow").value) ; i++) {
                    if (document.getElementById("ddlUnit_1_" + i).value == document.getElementById("ddlUnit_2_" + i).value) {
                        alert('Base and Conversion Unit are not same!!!');
                        return false;
                    }
                    for (var j = 1; j <= Number(document.getElementById("hfTotalRow").value) ; j++) {
                        if (j != i) {
                            if ((document.getElementById("ddlUnit_1_" + i).value == document.getElementById("ddlUnit_1_" + j).value && document.getElementById("ddlUnit_2_" + i).value == document.getElementById("ddlUnit_2_" + j).value)
                                  || (document.getElementById("ddlUnit_1_" + i).value == document.getElementById("ddlUnit_2_" + j).value && document.getElementById("ddlUnit_2_" + i).value == document.getElementById("ddlUnit_1_" + j).value)) {
                                alert('Select distinct base and conversion unit!!!');
                                return false;
                            }
                        }
                    }
                }
            }
            else {
                return false;
            }

            return true;
        }
    </script>
    <style type="text/css">
        .row {
            background-color: #DBE5F1;
            text-align: left;
            color: black;
            font-family: Arial;
            font-size: 8pt;
        }

        .arow {
            background-color: #ffffff;
            text-align: left;
            color: black;
            font-family: Arial;
            font-size: 8pt;
        }

        input[type='checkbox'] {
            position: relative;
            bottom: 1px;
            vertical-align: middle;
        }

        .tooltipCust {
            position: relative;
            display: inline-block;
            border-bottom: 1px dotted black;
        }

        .tooltipCust .tooltiptext {
            visibility: hidden;
            width: 500px;
            background-color: #555;
            color: #fff;
            border-radius: 6px;
            padding: 5px;
            position: absolute;
            z-index: 1;
            bottom: 125%;
            opacity: 0;
            transition: opacity 0.3s;
        }

        .tooltipCust .tooltiptext::after {
            content: "";
            position: absolute;
            top: 100%;
            left: 50%;
            margin-left: -5px;
            border-width: 5px;
            border-style: solid;
            border-color: #555 transparent transparent transparent;
        }

        .tooltipCust:hover .tooltiptext {
            visibility: visible;
            opacity: 1;
        }
    </style>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <div class="input-part">
        <div class="right-input">
            <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save"
                OnClientClick="return ClientSideClick()"></asp:Button>
            <asp:Button ID="btnSaveClose" runat="server" CssClass="button" Text="Save &amp; Close"
                OnClientClick="return ClientSideClick()"></asp:Button>
            <asp:Button ID="btnCancel" runat="server" CssClass="button" Width="50" Text="Close"></asp:Button>&nbsp;&nbsp;
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="PageTitle" runat="server" ClientIDMode="Static">
    Item Replenishment Formula
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="Content" runat="server" ClientIDMode="Static">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <table style="min-width: 800px">
        <tr>
            <td>
                <asp:CheckBox ID="chkReOrderPoint" runat="server" Text="After a Sales Order is created, if the sum total of “On-Order” & “On-Hand” of an inventory item is =< the “Reorder Point”, create a " />
                <asp:DropDownList ID="ddlOppStatus" runat="server">
                    <asp:ListItem Value="0" Text="Purchase Opportunity Prompt" />
                    <asp:ListItem Value="1" Text="Purchase Order Prompt" />
                    <asp:ListItem Value="2" Text="Purchase Opportunity Record" />
                    <asp:ListItem Value="3" Text="Purchase Order Record" />
                </asp:DropDownList> using the following recommended unit qty (formula will also be used when a new PO is created): <br />
                <table>
                    <tr>
                        <td>
                            <asp:RadioButton ID="rbRecommendation1" GroupName="RecommendationType" runat="server" Checked="true" /> Add “Re-Order Qty” or “Vendor Min Order Qty” (whichever is greater) + “Back Order Qty” - “On-Order Qty <asp:CheckBox runat="server" ID="chkRecommendation1" /> + Requisitions”
                            <br /><asp:RadioButton ID="rbRecommendation2" GroupName="RecommendationType" runat="server" /> Add “Re-Order Qty” or “Vendor Min Order Qty” or “Back-Order Qty” (whichever is greater) - “On-Order Qty <asp:CheckBox runat="server" ID="chkRecommendation2" /> + Requisitions”
                            <br /><asp:RadioButton ID="rbRecommendation3" GroupName="RecommendationType" runat="server" />  Add “Back Order Qty” - “On-Order Qty <asp:CheckBox runat="server" ID="chkRecommendation3" /> + Requisitions” + “Re-Order Qty” or “Vendor Min Order Qty” (whichever is greater) 
                        </td>
                    </tr>
                </table>
                Also set the order status to <asp:DropDownList ID="ddlOrderStatusReOrderPoint" runat="server">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td>
                <br />
                <div class="tooltipCust">Note: Re-Order points are treated differently when a Kit or Assembly is involved. 
                  <span class="tooltiptext">
                      <b>Kit Items:</b> Kits don’t have re-order quantities or re-order points. Kits are made up of items, which do have these two levels, which should be taken into consideration when setting child-inventory item re-order points and quantities.
                      <br />
                      <br />
                      <b>Assembly Items:</b> When on-hand reaches the re-order point, depending on the rule option (radio button), a trigger is created. Unlike regular items or even kits, the trigger doesn’t generate purchase prompts or orders used to replenish inventory. Instead, it creates work orders (WOs), in the quanity set within the “Re-Order Qty” field. If the WOs can’t be “completed” due to BOMs lacking sufficient qty, then the auto-create rule will either create PO records for primary vendors, or allow you to manually create POs froim the WO’s BOM list by filtering for WOs that contain a back-ordered BOM (items).
                  </span>
                </div>
            </td>
        </tr>
    </table>
</asp:Content>
