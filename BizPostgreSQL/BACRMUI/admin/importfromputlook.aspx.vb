Imports BACRM.BusinessLogic.Contacts
Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Common
Imports WebReference
Imports System.net
Namespace BACRM.UserInterface.Admin
    Public Class importfromputlook
        Inherits BACRMPage
        Protected WithEvents cmbCompany As System.Web.UI.HtmlControls.HtmlSelect
        Protected WithEvents lblErrMsg As System.Web.UI.WebControls.Label
        Protected WithEvents imgbtnCancel As System.Web.UI.WebControls.ImageButton
        Protected WithEvents btnAdd As System.Web.UI.WebControls.Button
        Protected WithEvents btnRemove As System.Web.UI.WebControls.Button
        Protected WithEvents lstContacts As System.Web.UI.WebControls.ListBox
        Protected WithEvents lstContactsAdd As System.Web.UI.WebControls.ListBox
        Protected WithEvents btnImport As System.Web.UI.WebControls.Button
        Protected WithEvents btnCancel As System.Web.UI.WebControls.Button
        Protected WithEvents hdnValue As System.Web.UI.WebControls.TextBox
        Protected WithEvents Table2 As System.Web.UI.WebControls.Table
        Protected WithEvents litMessage As System.Web.UI.WebControls.Literal

        Dim m_strCompanyName As String 'Variable to hold the Company Name
        Dim esb As New ExchangeServiceBinding()
#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        Function esbCredentials() As ExchangeServiceBinding
            Try
                ServicePointManager.CertificatePolicy = New MyCertificateValidation
                esb.Url = Session("ExchURL") & "/EWS/exchange.asmx"
                esb.Credentials = New NetworkCredential(Session("ExchUserName"), Session("ExchPassword"), Session("ExchDomain"))

                If HttpContext.Current.Session("AccessMethod") = True Then
                    Dim ExchImpersonateAccc As New ExchangeImpersonationType
                    Dim connectionSid As New ConnectingSIDType
                    connectionSid.PrimarySmtpAddress = Session("UserEmail")
                    'connectionSid.PrimarySmtpAddress = "administrator@bizdemo.com"
                    ExchImpersonateAccc.ConnectingSID = connectionSid
                    esb.ExchangeImpersonation = ExchImpersonateAccc
                End If
                Return esb
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Try
                If IsPostBack() = False Then sb_FillContactsFromOutLook()
                Session("Help") = Request.Url.Segments(Request.Url.Segments.Length - 1)
                If Not Session("litMessage") Is Nothing Then
                    litMessage.Text = Session("litMessage")
                    Session("litMessage") = Nothing
                End If
                btnAdd.Attributes.Add("OnClick", "return move(document.Form1.lstContacts,document.Form1.lstContactsAdd)")
                btnRemove.Attributes.Add("OnClick", "return remove(document.Form1.lstContactsAdd,document.Form1.lstContacts)")
                btnImport.Attributes.Add("onclick", "return Import()")
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub sb_FillContactsFromOutLook()
            Try
                esb = esbCredentials()
                '  Dim objDR As System.Data.DataRow
                Dim findItemRequest As New FindItemType()
                findItemRequest.Traversal = ItemQueryTraversalType.Shallow

                Dim ptIsupdated As PathToExtendedFieldType = New PathToExtendedFieldType
                ptIsupdated.DistinguishedPropertySetId = DistinguishedPropertySetType.PublicStrings
                ptIsupdated.DistinguishedPropertySetIdSpecified = True
                ptIsupdated.PropertyName = "bitUpdatedInSQL"
                ptIsupdated.PropertyType = MapiPropertyTypeType.String

                Dim itemProperties As New ItemResponseShapeType()
                itemProperties.BaseShape = DefaultShapeNamesType.AllProperties

                Dim basepath(0) As BasePathToElementType
                basepath(0) = ptIsupdated

                Dim folderIDArray(1) As DistinguishedFolderIdType
                folderIDArray(0) = New DistinguishedFolderIdType()
                folderIDArray(0).Id = DistinguishedFolderIdNameType.contacts

                findItemRequest.ParentFolderIds = folderIDArray
                findItemRequest.ItemShape = itemProperties
                findItemRequest.ItemShape.AdditionalProperties = basepath
                Dim findItemResponse As FindItemResponseType = esb.FindItem(findItemRequest)
                findItemResponse = findItemResponse

                Dim findItemMsgResponse As New FindItemResponseMessageType
                findItemMsgResponse = findItemResponse.ResponseMessages.Items(0)
                Dim dtTable As New DataTable
                Dim dr As DataRow
                dtTable.Columns.Add("Value")
                dtTable.Columns.Add("Text")
                If findItemResponse.ResponseMessages.Items(0).ResponseClass = ResponseClassType.Success Then
                    Dim ItemDtls As ArrayOfRealItemsType = findItemMsgResponse.RootFolder.Item
                    Dim ItemDtlsCount As Integer = 1
                    If Not IsNothing(ItemDtls.Items) Then
                       
                        For ItemDtlsCount = 0 To ItemDtls.Items.Length - 1
                            Dim z As ContactItemType
                            z = CType(ItemDtls.Items(ItemDtlsCount), ContactItemType)
                            Dim displayName As String = z.DisplayName
                            Dim firstName As String = z.GivenName
                            Dim lastName As String = z.Surname
                            Dim middleName As String = z.MiddleName
                            Dim emailAddress As String = z.EmailAddresses(0).Value
                            Dim cmpName As String = z.CompanyName
                            Dim itemId As String = z.ItemId.Id
                            Dim changeKey As String = z.ItemId.ChangeKey
                            If Not IsNothing(z.ExtendedProperty) Then
                                Dim i As Integer
                                For i = 0 To z.ExtendedProperty.Length - 1
                                    If (z.ExtendedProperty(i).ExtendedFieldURI.PropertyName = "bitUpdatedInSQL") Then
                                        If Not (z.ExtendedProperty(i).Item = "true") Then
                                            dr = dtTable.NewRow
                                            dr("Text") = (lastName & " , " & firstName).Trim
                                            dr("Value") = itemId & "~" & changeKey
                                            dtTable.Rows.Add(dr)
                                        End If
                                    End If
                                Next i
                            Else
                                dr = dtTable.NewRow
                                dr("Text") = (lastName & " , " & firstName).Trim
                                dr("Value") = itemId & "~" & changeKey
                                dtTable.Rows.Add(dr)
                            End If

                            'Dim i As Integer = 0
                        Next
                    End If
                End If
                Dim dv As New DataView
                dv = dtTable.DefaultView
                dv.Sort = "Text"
                If txtName.Text <> "" Then
                    dv.RowFilter = "Text LIKE '%" & txtName.Text.Trim & "%'"
                End If
                lstContacts.DataSource = dv
                lstContacts.DataTextField = "Text"
                lstContacts.DataValueField = "Value"
                lstContacts.DataBind()
                'Dim strExchContactsFolder As String = Session("SiteType") & "//" & Session("ServerName") & "/exchange/" & Session("UserEmail") & "/Contacts/"
                'Dim strSQLString As String
                'strSQLString = "<?xml version=""1.0""?>"
                'strSQLString = strSQLString & "<D:searchrequest xmlns:D = ""DAV:"" >"
                'strSQLString = strSQLString & "<D:sql>SELECT "
                'strSQLString = strSQLString & AddQuotes("DAV:displayname") & ", " & AddQuotes("urn:schemas:contacts:o") & ", " & AddQuotes("urn:schemas:contacts:bitUpdatedInSQL") & ", " & AddQuotes("urn:schemas:mailheader:subject")
                'strSQLString = strSQLString & "," & AddQuotes("urn:schemas:contacts:givenName") 'FirstName
                'strSQLString = strSQLString & "," & AddQuotes("urn:schemas:contacts:email1")
                'strSQLString = strSQLString & "," & AddQuotes("urn:schemas:contacts:sn")
                'strSQLString = strSQLString & " FROM " & AddQuotes(strExchContactsFolder)
                'strSQLString = strSQLString & " WHERE "
                'strSQLString = strSQLString & AddQuotes("urn:schemas:contacts:email1") & " != ''"
                ''strSQLString = strSQLString & " AND "
                ''strSQLString = strSQLString & AddQuotes("urn:schemas:contacts:bitUpdatedInSQL") & "= ''"
                'strSQLString = strSQLString & "</D:sql></D:searchrequest>"

                'Dim bytes() As Byte
                'Dim RequestStream As System.IO.Stream
                'Dim ResponseStream As System.IO.Stream
                'Dim xmlReader As System.Xml.XmlTextReader
                'Dim strWebRequest As System.Net.HttpWebRequest
                'Dim strWebResponse As System.Net.HttpWebResponse
                'Dim ResponseXmlDoc As System.Xml.XmlDocument
                'Dim lstiObject As ListItem

                ''Creating the XML Node List
                'Dim HrefNodes As System.Xml.XmlNodeList
                'Dim SubjectNodeList As System.Xml.XmlNodeList
                'Dim companyList As System.Xml.XmlNodeList
                'Dim bitUpdatedList As System.Xml.XmlNodeList
                'Dim fNameList As System.Xml.XmlNodeList
                'Dim lNameList As System.Xml.XmlNodeList
                'Dim dispNameList As System.Xml.XmlNodeList
                'Dim eMail As System.Xml.XmlNodeList
                ''Create the search HttpWebRequest object.
                'strWebRequest = CType(System.Net.WebRequest.Create(strExchContactsFolder), System.Net.HttpWebRequest)

                ''Add the network credential to the request.
                ''strWebRequest.Credentials = System.Net.CredentialCache.DefaultCredentials
                'Dim cred As System.Net.NetworkCredential = New System.Net.NetworkCredential("superuser", ConfigurationManager.AppSettings("superuser"), ConfigurationManager.AppSettings("DomainForExchange"))
                'Dim credcache As System.Net.CredentialCache = New System.Net.CredentialCache
                'credcache.Add(New Uri(strExchContactsFolder), "Basic", cred)

                'strWebRequest.Credentials = credcache

                ''Specify the search method.
                'strWebRequest.Method = "SEARCH"

                '' Set the Content Type header.
                'strWebRequest.ContentType = "text/xml"

                '' Set the Translate header.
                'strWebRequest.Headers.Add("Translate", "F")


                '' Encode the body using UTF-8.
                'bytes = System.Text.Encoding.UTF8.GetBytes(strSQLString)

                '' Set the content header length.  This must be
                '' done before writing data to the request stream.
                'strWebRequest.ContentLength = bytes.Length

                ''Get a reference to the request stream.
                'RequestStream = strWebRequest.GetRequestStream()

                ''Write the message body to the request stream.
                'RequestStream.Write(bytes, 0, bytes.Length)

                ''Close the Stream object to release the connection for further use.
                'RequestStream.Close()

                '' Send the SEARCH method request and get the
                '' response from the server.
                'strWebResponse = strWebRequest.GetResponse()

                ''Get the XML Response stream.
                'ResponseStream = strWebResponse.GetResponseStream()

                '' Create the XmlDocument object from the XML response stream.
                'ResponseXmlDoc = New System.Xml.XmlDocument
                'ResponseXmlDoc.Load(ResponseStream)

                ''Inserting the values in the xmlNodes.
                'dispNameList = ResponseXmlDoc.GetElementsByTagName("a:href")
                'fNameList = ResponseXmlDoc.GetElementsByTagName("d:givenName")
                'lNameList = ResponseXmlDoc.GetElementsByTagName("d:sn")
                'eMail = ResponseXmlDoc.GetElementsByTagName("d:email1")
                'SubjectNodeList = ResponseXmlDoc.GetElementsByTagName("e:subject")
                'companyList = ResponseXmlDoc.GetElementsByTagName("d:o")
                'bitUpdatedList = ResponseXmlDoc.GetElementsByTagName("d:bitUpdatedInSQL")
                'Dim dtTable As New DataTable
                'Dim dr As DataRow
                'dtTable.Columns.Add("Value")
                'dtTable.Columns.Add("Text")
                'If SubjectNodeList.Count > 0 Then
                '    Dim i As Integer
                '    For i = 0 To SubjectNodeList.Count - 1
                '        If bitUpdatedList(i).InnerText = "" Then
                '            dr = dtTable.NewRow
                '            dr("Text") = (fNameList(i).InnerText & " " & lNameList(i).InnerText).Trim
                '            dr("Value") = dispNameList(i).InnerText
                '            dtTable.Rows.Add(dr)
                '        End If
                '    Next
                '    Dim dv As New DataView
                '    dv = dtTable.DefaultView
                '    dv.Sort = "Text"
                '    If txtName.Text <> "" Then
                '        dv.RowFilter = "Text LIKE '%" & txtName.Text.Trim & "%'"
                '    End If
                '    lstContacts.DataSource = dv
                '    lstContacts.DataTextField = "Text"
                '    lstContacts.DataValueField = "Value"
                '    lstContacts.DataBind()
                'End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub UpdateOwacontact(ByVal ItemId As String, ByVal ChangeKey As String)
            Try
                esb = esbCredentials()
                Dim ItemIDType As ItemIdType
                ItemIDType = New ItemIdType
                ItemIDType.Id = ItemId
                ItemIDType.ChangeKey = ChangeKey

                Dim ItemChangeType As ItemChangeType = New ItemChangeType
                ItemChangeType.Item = ItemIDType
                ItemChangeType.Updates = New ItemChangeDescriptionType(0) {}

                Dim setItem As SetItemFieldType = New SetItemFieldType

                'Dim indexedField As PathToUnindexedFieldType = New PathToUnindexedFieldType
                'indexedField.FieldURI = UnindexedFieldURIType.contactsContactSource
                'setItem.Item = indexedField

                'Dim cont As ContactItemType = New ContactItemType
                'cont.Department = "dc"
                'setItem.Item1 = cont
                'ItemChangeType.Updates(0) = setItem

                Dim extendFieldURI As PathToExtendedFieldType = New PathToExtendedFieldType
                extendFieldURI.DistinguishedPropertySetId = DistinguishedPropertySetType.PublicStrings
                extendFieldURI.DistinguishedPropertySetIdSpecified = True
                extendFieldURI.PropertyName = "bitUpdatedInSQL"
                extendFieldURI.PropertyType = MapiPropertyTypeType.String

                setItem.Item = extendFieldURI
                Dim extendedproperty(0) As ExtendedPropertyType

                extendedproperty(0) = New ExtendedPropertyType
                extendedproperty(0).ExtendedFieldURI = extendFieldURI
                extendedproperty(0).Item = "true"

                Dim contact As New ContactItemType
                contact.ExtendedProperty = extendedproperty
                setItem.Item1 = contact
                ItemChangeType.Updates(0) = setItem

                Dim updateItem As New UpdateItemType
                updateItem.ItemChanges = New ItemChangeType(0) {}
                updateItem.ItemChanges(0) = ItemChangeType
                updateItem.MessageDisposition = MessageDispositionType.SaveOnly
                updateItem.MessageDispositionSpecified = True
                updateItem.ConflictResolution = ConflictResolutionType.AutoResolve

                Dim response As UpdateItemResponseType = esb.UpdateItem(updateItem)
                response = response

                '================================================================================
                ' Purpose: This procedure will Update the Bit Value for SQL Updates into te Contact 
                '       of Exchange.
                '
                ' Parameters: strContactURLPath = Will hold the Exchange path for the contact EML 
                '               file. It is this eml file which has to be updated with the 
                '               bitUpdatedinSQL flag.
                '
                ' History
                ' Ver   Date        Ref     Author              Reason
                ' 1     21/01/2004          Vishwanath         Created
                '
                ' Non Compliance (any deviation from standards)
                '   No deviations from the standards.
                '================================================================================
                'Dim strSubmissionURL As String
                'Dim strXMLNSInfo As String
                'Dim strCalInfo As String
                'Dim strApptRequest As String
                'Dim bResult As Boolean

                ''strMeetingURL = "http://" & strExServer & "/exchange/" & strMailbox & "/" & strFolder & "/" & strMeetingItem
                'strSubmissionURL = Session("SiteType") & "//" & Session("ServerName") & "/exchange/" & Session("UserEmail") & "/##DavMailSubmissionURI##/"

                'strXMLNSInfo = "xmlns:g=""DAV:"" " & _
                '"xmlns:c=""urn:schemas:contacts:"" " & _
                '"xmlns:e=""http://schemas.microsoft.com/exchange/"" " & _
                '"xmlns:mapi=""http://schemas.microsoft.com/mapi/"" " & _
                '"xmlns:x=""xml:"" xmlns:cal=""urn:schemas:calendar:"" " & _
                '"xmlns:mail=""urn:schemas:httpmail:"">"

                '' Calendar item properties.
                'strCalInfo = "<c:bitUpdatedInSQL>1</c:bitUpdatedInSQL>"

                'strApptRequest = "<?xml version=""1.0""?>" & _
                '"<g:propertyupdate " & strXMLNSInfo & _
                ' "<g:set>" & _
                '  "<g:prop>" & _
                '   "<g:contentclass>urn:content-classes:person</g:contentclass>" & _
                '   "<e:outlookmessageclass>IPM.Contact</e:outlookmessageclass>" & _
                '    strCalInfo & vbCrLf & _
                '  "</g:prop>" & _
                ' "</g:set>" & _
                '"</g:propertyupdate>"
                'bResult = False

                '' Create meeting associated appointment in calendar.
                'bResult = UpdatedinSQL(strContactURLPath, _
                '           strApptRequest)
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        'Public Shared Function UpdatedinSQL( _
        'ByVal strURL, _
        'ByVal strApptRequest) As Boolean
        '    '================================================================================
        '    ' Purpose: This function will update the .eml file.
        '    '
        '    ' History
        '    ' Ver   Date        Author            Reason
        '    ' 1     20/01/2004  Vishwanath K      Created
        '    '
        '    ' Non Compliance (any deviation from standards)
        '    '   No deviations from the standards.   
        '    '================================================================================
        '    Dim bytes() As Byte
        '    Dim myWebRequest As System.Net.WebRequest
        '    Dim strWebRequest As System.Net.HttpWebRequest
        '    Dim strWebResponse As System.Net.HttpWebResponse
        '    Dim RequestStream As System.IO.Stream
        '    Dim ResponseStream As System.IO.Stream
        '    Try

        '        strWebRequest = CType(System.Net.WebRequest.Create(strURL), System.Net.HttpWebRequest)

        '        'Add the network credential to the request.
        '        'strWebRequest.Credentials = System.Net.CredentialCache.DefaultCredentials
        '        Dim cred As System.Net.NetworkCredential = New System.Net.NetworkCredential("superuser", ConfigurationManager.AppSettings("superuser"), ConfigurationManager.AppSettings("DomainForExchange"))
        '        Dim credcache As System.Net.CredentialCache = New System.Net.CredentialCache
        '        credcache.Add(New Uri(strURL), "Basic", cred)

        '        strWebRequest.Credentials = credcache
        '        'Specify the search method.
        '        strWebRequest.Method = "PROPPATCH"

        '        'SEt the content type header.
        '        strWebRequest.ContentType = "text/xml"

        '        'Encode the body using UTF-8.
        '        bytes = System.Text.Encoding.UTF8.GetBytes(strApptRequest)

        '        'Set the content header length. This must be done before writing data to the 
        '        'request stream.
        '        strWebRequest.ContentLength = bytes.Length

        '        'Get a reference to the request stream.
        '        RequestStream = strWebRequest.GetRequestStream()

        '        'Write the message body to the request stream.
        '        RequestStream.Write(bytes, 0, bytes.Length)

        '        'Close the Stream object to release the connection for further use.
        '        RequestStream.Close()

        '        'Send the SEARCH method request and get the response from the server.
        '        strWebResponse = strWebRequest.GetResponse()

        '        ResponseStream = strWebResponse.GetResponseStream
        '        Return True
        '    Catch ex As Exception
        '        'lblErr.Text = Err.Number & ": " & Err.Description
        '        'Return ex.Message
        '        Return False
        '    End Try
        'End Function

        Private Sub btnImport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnImport.Click
            Try
                esb = esbCredentials()
                Dim i As Integer = 0
                Dim strUsersAdded As String = ""
                Dim strUsersNotAdded As String = ""
                Dim litMessage As String = ""
                Dim itemid As String() = hdnValue.Text.Split(",")
                While i < itemid.Length - 1

                    Dim strItem As String()
                    strItem = itemid(i).Split("~")

                    Dim getItemRequest As New GetItemType
                    Dim getItemIDType(0) As ItemIdType

                    Dim gettemResponseShapeType As New ItemResponseShapeType
                    gettemResponseShapeType.BaseShape = DefaultShapeNamesType.AllProperties
                    gettemResponseShapeType.IncludeMimeContent = True
                    getItemRequest.ItemShape = gettemResponseShapeType

                    getItemIDType(0) = New ItemIdType
                    getItemIDType(0).Id = strItem(0)
                    getItemIDType(0).ChangeKey = strItem(1)
                    getItemRequest.ItemIds = getItemIDType

                    Dim getItemresponse As GetItemResponseType
                    getItemresponse = esb.GetItem(getItemRequest)

                    For Each contact As ItemInfoResponseMessageType In getItemresponse.ResponseMessages.Items
                        Dim item As ItemType = contact.Items.Items(0)
                        Dim ContactType As ContactItemType
                        ContactType = CType(item, ContactItemType)
                        Dim Subject As String = IIf(IsNothing(ContactType.Subject), "", ContactType.Subject)
                        Dim dispName As String = IIf(IsNothing(ContactType.DisplayName), "", ContactType.DisplayName)
                        Dim fName As String = IIf(IsNothing(ContactType.GivenName), "", ContactType.GivenName)
                        Dim lName As String = IIf(IsNothing(ContactType.Surname), "", ContactType.Surname)
                        Dim compName As String = IIf(IsNothing(ContactType.CompanyName), "", ContactType.CompanyName)
                        Dim dept As String = IIf(IsNothing(ContactType.Department), "", ContactType.Department)
                        Dim fax As String = IIf(IsNothing(ContactType.PhoneNumbers(1).Value), "", ContactType.PhoneNumbers(1).Value)
                        Dim Ophone As String = IIf(IsNothing(ContactType.PhoneNumbers(13).Value), "", ContactType.PhoneNumbers(13).Value)
                        Dim Hphone As String = IIf(IsNothing(ContactType.PhoneNumbers(8).Value), "", ContactType.PhoneNumbers(8).Value)

                        Dim email As String = IIf(IsNothing(ContactType.EmailAddresses(0).Value), "", ContactType.EmailAddresses(0).Value)
                       
                        Dim street As String = IIf(IsNothing(ContactType.PhysicalAddresses(1).Street), "", ContactType.PhysicalAddresses(1).Street)
                        Dim city As String = IIf(IsNothing(ContactType.PhysicalAddresses(1).City), "", ContactType.PhysicalAddresses(1).City)
                        Dim state As String = IIf(IsNothing(ContactType.PhysicalAddresses(1).State), "", ContactType.PhysicalAddresses(1).State)
                        Dim country As String = IIf(IsNothing(ContactType.PhysicalAddresses(1).CountryOrRegion), "", ContactType.PhysicalAddresses(1).CountryOrRegion)
                        Dim homepage As String = IIf(IsNothing(ContactType.BusinessHomePage), "", ContactType.BusinessHomePage)

                        Dim postalcode As String = IIf(IsNothing(ContactType.PhysicalAddresses(1).PostalCode), "", ContactType.PhysicalAddresses(1).PostalCode)

                        Dim mobile As String = IIf(IsNothing(ContactType.PhoneNumbers(11).Value), "", ContactType.PhoneNumbers(11).Value)
                        'dim gender as String  = contacttype.AssistantName
                        Dim secretary As String = IIf(IsNothing(ContactType.AssistantName), "", ContactType.AssistantName)
                        Dim secretaryphone As String = IIf(IsNothing(ContactType.PhoneNumbers(0).Value), "", ContactType.PhoneNumbers(0).Value)
                        Dim bitUpdatedList As String
                        If IsNothing(ContactType.ExtendedProperty) Then
                            bitUpdatedList = 0
                        Else : bitUpdatedList = 1
                        End If

                        Dim objContacts As New CContacts
                        Dim objImpWzd As New ImportWizard
                        With objContacts
                            If compName = "" Then
                                .CompanyName = IIf(Len(lName) > 50, Left(lName, 50), lName) & "," & IIf(Len(fName) > 50, Left(fName, 50), fName)
                            Else : .CompanyName = IIf(Len(compName) > 100, Left(fName, 100), fName)
                            End If
                            .Department = 0
                            .FirstName = IIf(Len(fName) > 50, Left(fName, 50), fName)
                            .LastName = IIf(Len(lName) > 50, Left(lName, 50), lName)
                            .GivenName = IIf(Len(fName) > 100, Left(fName, 100), fName)
                            .ContactPhone = IIf(Len(Ophone) > 15, Left(Ophone, 15), Ophone)
                            .HomePhone = IIf(Len(Hphone) > 15, Left(Hphone, 15), Hphone)
                            .Fax = IIf(Len(fax) > 15, Left(fax, 15), fax)
                            .CellPhone = IIf(Len(mobile) > 15, Left(mobile, 15), mobile)
                            If email <> "" Then
                                Dim strEmail1 As String, intStartFrom As Integer
                                strEmail1 = email
                                intStartFrom = InStr(1, strEmail1, "<", CompareMethod.Text)
                                If intStartFrom > 0 Then
                                    'Pick only text in between '<' and '>'
                                    Dim intEnd As Integer = InStr(1, strEmail1, ">", CompareMethod.Text)
                                    strEmail1 = Mid(strEmail1, intStartFrom + 1, intEnd - intStartFrom - 1)
                                End If

                                .Email = Left(strEmail1, 50).Trim
                            End If
                            .Street = IIf(Len(street) > 50, Left(street, 50), street)
                            .City = IIf(Len(city) > 50, Left(city, 50), city)
                            .State = objImpWzd.GetStateAndCountry(1, IIf(Len(state) > 50, Left(state, 50), state), Session("DomainID"))
                            .Country = objImpWzd.GetStateAndCountry(0, IIf(Len(country) > 50, Left(country, 50), country), Session("DomainID"))
                            .Website = IIf(Len(homepage) > 255, Left(homepage, 255), homepage)
                            .PostalCode = IIf(Len(postalcode) > 12, Left(postalcode, 12), postalcode)
                            'If genderList(i).InnerText <> "" Then
                            '    If genderList(i).InnerText = 0 Then
                            'Male
                            .Sex = "0"
                            '    ElseIf genderList(i).InnerText = 2 Then
                            ''Female
                            '.Sex = "F"
                            '    ElseIf genderList(i).InnerText = 1 Then
                            ''Female
                            '.Sex = "M"
                            '    End If
                            'End If

                            If secretary <> "" Then
                                Dim strAsstName As String = IIf(Len(secretary) > 50, Left(secretary, 50), secretary)
                                'Check if teh Assistant Name Consists of First and Last Name by checking for space.
                                If InStr(strAsstName, " ", CompareMethod.Text) > 0 Then
                                    'The name consists of First and Last Names.
                                    'Break them apart as First And Last Names.
                                    .FirstName = Mid(strAsstName, 1, InStr(strAsstName, " ", CompareMethod.Text) - 1)
                                    .LastName = Mid(strAsstName, InStr(strAsstName, " ", CompareMethod.Text) + 1, Len(strAsstName) - InStr(strAsstName, " ", CompareMethod.Text) + 1)
                                Else
                                    'There is only one name.
                                    .LastName = strAsstName
                                End If
                            End If
                            .AssPhone = IIf(Len(secretaryphone) > 50, Left(secretaryphone, 50), secretaryphone)
                            .DomainID = Session("DomainID")
                            .UserCntID = Session("UserContactID")
                            .TerritoryID = 0
                            If GetQueryStringVal(Request.QueryString("enc"), "frm") = "prospects" Then
                                .CRMType = 1
                            Else : .CRMType = 2
                            End If
                        End With

                        Dim str As String
                        str = objContacts.CheckEmailExistence
                        If str = "" Then
                            objContacts.ImportContactFromOutlook()
                            UpdateOwacontact(strItem(0), strItem(1))
                            strUsersAdded = strUsersAdded & objContacts.FirstName & " " & objContacts.LastName & ", &nbsp"
                        Else : strUsersNotAdded = strUsersNotAdded & objContacts.FirstName & " " & objContacts.LastName & ", &nbsp"
                        End If
                        '                End If
                    Next
                    i = i + 1
                End While

                'Dim strExchContactsFolder As String = Session("SiteType") & "//" & Session("ServerName") & "/exchange/" & Session("UserEmail") & "/Contacts/"
                'Dim bytes() As Byte
                'Dim RequestStream As System.IO.Stream
                'Dim ResponseStream As System.IO.Stream
                'Dim xmlReader As System.Xml.XmlTextReader
                'Dim strWebRequest As System.Net.HttpWebRequest
                'Dim strWebResponse As System.Net.HttpWebResponse
                'Dim ResponseXmlDoc As System.Xml.XmlDocument

                'Creating the XML Node List
                'Dim HrefNodes As System.Xml.XmlNodeList
                'Dim SubjectNodeList As System.Xml.XmlNodeList
                'Dim dispNameList As System.Xml.XmlNodeList
                'Dim fNameList As System.Xml.XmlNodeList
                'Dim lNameList As System.Xml.XmlNodeList
                'Dim compList As System.Xml.XmlNodeList
                'Dim deptList As System.Xml.XmlNodeList
                'Dim faxList As System.Xml.XmlNodeList
                'Dim OphoneList As System.Xml.XmlNodeList
                'Dim HphoneList As System.Xml.XmlNodeList
                'Dim emailList As System.Xml.XmlNodeList
                'Dim streetList As System.Xml.XmlNodeList
                'Dim cityList As System.Xml.XmlNodeList
                'Dim stateList As System.Xml.XmlNodeList
                'Dim countryList As System.Xml.XmlNodeList
                'Dim homepageList As System.Xml.XmlNodeList
                'Dim postalcodeList As System.Xml.XmlNodeList
                'Dim mobileList As System.Xml.XmlNodeList
                'Dim genderList As System.Xml.XmlNodeList
                'Dim secretaryList As System.Xml.XmlNodeList
                'Dim secretaryphoneList As System.Xml.XmlNodeList
                'Dim bitUpdatedList As System.Xml.XmlNodeList
                'Dim strUsersAdded, strUsersNotAdded, litMessage As String


                'Define the SQL String for getting Company Names from contacts folder of Exchange.
                'Dim strSQLString As String
                'strSQLString = "<?xml version=""1.0""?>"
                'strSQLString = strSQLString & "<D:searchrequest xmlns:D = ""DAV:"" >"
                'strSQLString = strSQLString & "<D:sql>SELECT "
                'strSQLString = strSQLString & AddQuotes("DAV:displayname")  'EML Name
                'strSQLString = strSQLString & "," & AddQuotes("urn:schemas:contacts:givenName") 'FirstName
                'strSQLString = strSQLString & "," & AddQuotes("urn:schemas:contacts:sn")    'LastName
                'strSQLString = strSQLString & "," & AddQuotes("urn:schemas:contacts:o") 'Company
                'strSQLString = strSQLString & "," & AddQuotes("urn:schemas:contacts:department")    'Dept Name
                'strSQLString = strSQLString & "," & AddQuotes("urn:schemas:contacts:facsimiletelephonenumber")  'Fax Number
                'strSQLString = strSQLString & "," & AddQuotes("urn:schemas:contacts:telephoneNumber")   'Phone NUmber
                'strSQLString = strSQLString & "," & AddQuotes("urn:schemas:contacts:homePhone")   'Home Phone NUmber
                'strSQLString = strSQLString & "," & AddQuotes("urn:schemas:contacts:email1")    'Email
                'strSQLString = strSQLString & "," & AddQuotes("urn:schemas:contacts:street")    'Street
                'strSQLString = strSQLString & "," & AddQuotes("urn:schemas:contacts:l") 'City
                'strSQLString = strSQLString & "," & AddQuotes("urn:schemas:contacts:st")    'State
                'strSQLString = strSQLString & "," & AddQuotes("urn:schemas:contacts:co")    'Country
                'strSQLString = strSQLString & "," & AddQuotes("urn:schemas:contacts:businesshomepage")  'Web
                'strSQLString = strSQLString & "," & AddQuotes("urn:schemas:contacts:postalcode")    'POstCode
                'strSQLString = strSQLString & "," & AddQuotes("urn:schemas:contacts:mobile")    'Mobile NUmber
                'strSQLString = strSQLString & "," & AddQuotes("urn:schemas:contacts:gender")    'Sex 0=Unspecified, 1=Female, 2=Male
                'strSQLString = strSQLString & "," & AddQuotes("urn:schemas:contacts:secretary") 'Assistant Full Name
                'strSQLString = strSQLString & "," & AddQuotes("urn:schemas:contacts:secretaryphone")    'Assistant Phone
                'strSQLString = strSQLString & "," & AddQuotes("urn:schemas:contacts:bitUpdatedInSQL")   'Updated in SQL
                'strSQLString = strSQLString & "," & AddQuotes("urn:schemas:mailheader:subject")
                'strSQLString = strSQLString & " FROM "
                'strSQLString = strSQLString & AddQuotes(strExchContactsFolder)
                'strSQLString = strSQLString & " WHERE "
                'strSQLString = strSQLString & AddQuotes("urn:schemas:contacts:email1") & " != ''"
                'strSQLString = strSQLString & "</D:sql></D:searchrequest>"

                'Create the search HttpWebRequest object.
                'strWebRequest = CType(System.Net.WebRequest.Create(strExchContactsFolder), System.Net.HttpWebRequest)

                'Add the network credential to the request.
                'strWebRequest.Credentials = System.Net.CredentialCache.DefaultCredentials
                'Dim cred As System.Net.NetworkCredential = New System.Net.NetworkCredential("superuser", ConfigurationManager.AppSettings("superuser"), ConfigurationManager.AppSettings("DomainForExchange"))
                'Dim credcache As System.Net.CredentialCache = New System.Net.CredentialCache
                'credcache.Add(New Uri(strExchContactsFolder), "Basic", cred)

                'strWebRequest.Credentials = credcache
                'Specify the search method.
                'strWebRequest.Method = "SEARCH"


                ' Set the Content Type header.
                'strWebRequest.ContentType = "text/xml"

                ' Set the Translate header.
                'strWebRequest.Headers.Add("Translate", "F")

                ' Encode the body using UTF-8.
                'bytes = System.Text.Encoding.UTF8.GetBytes(strSQLString)

                ' Set the content header length.  This must be
                ' done before writing data to the request stream.
                'strWebRequest.ContentLength = bytes.Length

                'Get a reference to the request stream.
                'RequestStream = strWebRequest.GetRequestStream()

                'Write the message body to the request stream.
                'RequestStream.Write(bytes, 0, bytes.Length)

                'Close the Stream object to release the connection for further use.
                'RequestStream.Close()



                ' Send the SEARCH method request and get the
                ' response from the server.
                'strWebResponse = strWebRequest.GetResponse()

                'Get the XML Response stream.
                'ResponseStream = strWebResponse.GetResponseStream()

                ' Create the XmlDocument object from the XML response stream.
                'ResponseXmlDoc = New System.Xml.XmlDocument
                'ResponseXmlDoc.Load(ResponseStream)

                'Inserting the values in the xmlNodes.
                'SubjectNodeList = ResponseXmlDoc.GetElementsByTagName("e:subject")
                'dispNameList = ResponseXmlDoc.GetElementsByTagName("a:href")
                'fNameList = ResponseXmlDoc.GetElementsByTagName("d:givenName")
                'lNameList = ResponseXmlDoc.GetElementsByTagName("d:sn")
                'compList = ResponseXmlDoc.GetElementsByTagName("d:o")
                'deptList = ResponseXmlDoc.GetElementsByTagName("d:department")
                'faxList = ResponseXmlDoc.GetElementsByTagName("d:facsimiletelephonenumber")
                'OphoneList = ResponseXmlDoc.GetElementsByTagName("d:telephoneNumber")
                'HphoneList = ResponseXmlDoc.GetElementsByTagName("d:homePhone")
                'emailList = ResponseXmlDoc.GetElementsByTagName("d:email1")
                'streetList = ResponseXmlDoc.GetElementsByTagName("d:street")
                'cityList = ResponseXmlDoc.GetElementsByTagName("d:l")
                'stateList = ResponseXmlDoc.GetElementsByTagName("d:st")
                'countryList = ResponseXmlDoc.GetElementsByTagName("d:co")
                'homepageList = ResponseXmlDoc.GetElementsByTagName("d:businesshomepage")
                'postalcodeList = ResponseXmlDoc.GetElementsByTagName("d:postalcode")
                'mobileList = ResponseXmlDoc.GetElementsByTagName("d:mobile")
                'genderList = ResponseXmlDoc.GetElementsByTagName("d:gender")
                'secretaryList = ResponseXmlDoc.GetElementsByTagName("d:secretary")
                'secretaryphoneList = ResponseXmlDoc.GetElementsByTagName("d:secretaryphone")
                'bitUpdatedList = ResponseXmlDoc.GetElementsByTagName("d:bitUpdatedInSQL")

                'If SubjectNodeList.Count > 0 Then
                '    Dim i As Integer

                '    For i = 0 To SubjectNodeList.Count - 1
                '        If bitUpdatedList(i).InnerText = "" Then
                '            Dim strDetails As String()
                '            Dim strName As String()
                '            Dim k As Integer
                '            strDetails = hdnValue.Text.Split(",")
                '            For k = 0 To strDetails.Length - 2
                '                strName = strDetails(k).Split("~")
                '                If dispNameList(i).InnerText = strName(0) Then
                '                    Dim objContacts As New CContacts
                '                    Dim objImpWzd As New ImportWizard
                '                    With objContacts
                '                        If compList(i).InnerText = "" Then
                '                            .CompanyName = IIf(Len(lNameList(i).InnerText) > 50, Left(lNameList(i).InnerText, 50), lNameList(i).InnerText) & "," & IIf(Len(fNameList(i).InnerText) > 50, Left(fNameList(i).InnerText, 50), fNameList(i).InnerText)
                '                        Else
                '                            .CompanyName = IIf(Len(compList(i).InnerText) > 100, Left(fNameList(i).InnerText, 100), fNameList(i).InnerText)
                '                        End If
                '                        '.Department = IIf(Len(deptList(i).InnerText) > 50, Left(deptList(i).InnerText, 50), deptList(i).InnerText)
                '                        .FirstName = IIf(Len(fNameList(i).InnerText) > 50, Left(fNameList(i).InnerText, 50), fNameList(i).InnerText)
                '                        .LastName = IIf(Len(lNameList(i).InnerText) > 50, Left(lNameList(i).InnerText, 50), lNameList(i).InnerText)
                '                        .GivenName = IIf(Len(dispNameList(i).InnerText) > 100, Left(dispNameList(i).InnerText, 100), dispNameList(i).InnerText)
                '                        .Phone = IIf(Len(OphoneList(i).InnerText) > 15, Left(OphoneList(i).InnerText, 15), OphoneList(i).InnerText)
                '                        .HomePhone = IIf(Len(HphoneList(i).InnerText) > 15, Left(HphoneList(i).InnerText, 15), HphoneList(i).InnerText)
                '                        .Fax = IIf(Len(faxList(i).InnerText) > 15, Left(faxList(i).InnerText, 15), faxList(i).InnerText)
                '                        .CellPhone = IIf(Len(mobileList(i).InnerText) > 15, Left(mobileList(i).InnerText, 15), mobileList(i).InnerText)
                '                        If emailList(i).InnerText <> "" Then
                '                            Dim strEmail1 As String, intStartFrom As Integer
                '                            strEmail1 = emailList(i).InnerText
                '                            intStartFrom = InStr(1, strEmail1, "<", CompareMethod.Text)
                '                            If intStartFrom > 0 Then
                '                                'Pick only text in between '<' and '>'
                '                                Dim intEnd As Integer = InStr(1, strEmail1, ">", CompareMethod.Text)
                '                                strEmail1 = Mid(strEmail1, intStartFrom + 1, intEnd - intStartFrom - 1)
                '                            End If

                '                            .Email = Left(strEmail1, 50).Trim
                '                        End If
                '                        .Street = IIf(Len(streetList(i).InnerText) > 50, Left(streetList(i).InnerText, 50), streetList(i).InnerText)
                '                        .City = IIf(Len(cityList(i).InnerText) > 50, Left(cityList(i).InnerText, 50), cityList(i).InnerText)
                '                        .State = objImpWzd.GetStateAndCountry(1, IIf(Len(stateList(i).InnerText) > 50, Left(stateList(i).InnerText, 50), stateList(i).InnerText))
                '                        .Country = objImpWzd.GetStateAndCountry(0, IIf(Len(countryList(i).InnerText) > 50, Left(countryList(i).InnerText, 50), countryList(i).InnerText))
                '                        .Website = IIf(Len(homepageList(i).InnerText) > 255, Left(homepageList(i).InnerText, 255), homepageList(i).InnerText)
                '                        .PostalCode = IIf(Len(postalcodeList(i).InnerText) > 12, Left(postalcodeList(i).InnerText, 12), postalcodeList(i).InnerText)
                '                        If genderList(i).InnerText <> "" Then
                '                            If genderList(i).InnerText = 0 Then
                '                                'Male
                '                                .Sex = "0"
                '                            ElseIf genderList(i).InnerText = 2 Then
                '                                'Female
                '                                .Sex = "F"
                '                            ElseIf genderList(i).InnerText = 1 Then
                '                                'Female
                '                                .Sex = "M"
                '                            End If
                '                        End If

                '                        If secretaryList(i).InnerText <> "" Then
                '                            Dim strAsstName As String = IIf(Len(secretaryList(i).InnerText) > 50, Left(secretaryList(i).InnerText, 50), secretaryList(i).InnerText)
                '                            'Check if teh Assistant Name Consists of First and Last Name by checking for space.
                '                            If InStr(strAsstName, " ", CompareMethod.Text) > 0 Then
                '                                'The name consists of First and Last Names.
                '                                'Break them apart as First And Last Names.
                '                                .FirstName = Mid(strAsstName, 1, InStr(strAsstName, " ", CompareMethod.Text) - 1)
                '                                .LastName = Mid(strAsstName, InStr(strAsstName, " ", CompareMethod.Text) + 1, Len(strAsstName) - InStr(strAsstName, " ", CompareMethod.Text) + 1)
                '                            Else
                '                                'There is only one name.
                '                                .LastName = strAsstName
                '                            End If
                '                        End If
                '                        .AssPhone = IIf(Len(secretaryphoneList(i).InnerText) > 50, Left(secretaryphoneList(i).InnerText, 50), secretaryphoneList(i).InnerText)
                '                        .DomainID = Session("DomainID")
                '                        .UserCntID = Session("UserContactID")
                '                        .TerritoryID = 0
                '                        If GetQueryStringVal(Request.QueryString("enc"),"frm") = "prospects" Then
                '                            .CRMType = 1
                '                        Else
                '                            .CRMType = 2
                '                        End If
                '                    End With

                '                    Dim str As String
                '                    str = objContacts.CheckEmailExistence
                '                    If str = "" Then
                '                        objContacts.ImportContactFromOutlook()
                '                        UpdateOwacontact(dispNameList(i).InnerText)
                '                        strUsersAdded = strUsersAdded & objContacts.FirstName & " " & objContacts.LastName & ", &nbsp"
                '                    Else
                '                        strUsersNotAdded = strUsersNotAdded & objContacts.FirstName & " " & objContacts.LastName & ", &nbsp"
                '                    End If

                '                End If

                '            Next
                '        End If
                '    Next
                'End If
                If strUsersAdded <> "" Then
                    strUsersAdded = strUsersAdded.TrimEnd(",")
                    litMessage = strUsersAdded & " are succesfully added to Contacts in BizAutomation. <br>"
                End If
                If strUsersNotAdded <> "" Then
                    strUsersNotAdded = strUsersNotAdded.TrimEnd(",")
                    litMessage = litMessage & "Could Not Add " & strUsersNotAdded & " because email address is already assigned to a contact in BizAutomation."
                End If
                If litMessage <> "" Then Session("litMessage") = litMessage
                Response.Redirect("../admin/importfromputlook.aspx?frm" & GetQueryStringVal(Request.QueryString("enc"), "frm"))
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
            Try
                If GetQueryStringVal(Request.QueryString("enc"), "frm") = "prospects" Then
                    Response.Redirect("../prospects/frmProspectList.aspx")
                Else : Response.Redirect("../account/frmAccountList.aspx")
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
            Try
                sb_FillContactsFromOutLook()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

    End Class
End Namespace