﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On

Namespace BACRM.UserInterface.Admin

    Partial Public Class frmRentalSetting

        '''<summary>
        '''btnSave control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents btnSave As Global.System.Web.UI.WebControls.Button

        '''<summary>
        '''btnClose control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents btnClose As Global.System.Web.UI.WebControls.Button

        '''<summary>
        '''ScriptManager1 control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents ScriptManager1 As Global.System.Web.UI.ScriptManager

        '''<summary>
        '''table9 control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents table9 As Global.System.Web.UI.WebControls.Table

        '''<summary>
        '''chkRental control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents chkRental As Global.System.Web.UI.WebControls.CheckBox

        '''<summary>
        '''ddlRentalItemClass control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents ddlRentalItemClass As Global.System.Web.UI.WebControls.DropDownList

        '''<summary>
        '''Label42 control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents Label42 As Global.System.Web.UI.WebControls.Label

        '''<summary>
        '''ddlRentalHourlyUOM control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents ddlRentalHourlyUOM As Global.System.Web.UI.WebControls.DropDownList

        '''<summary>
        '''ddlRentalDailyUOM control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents ddlRentalDailyUOM As Global.System.Web.UI.WebControls.DropDownList

        '''<summary>
        '''Label43 control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents Label43 As Global.System.Web.UI.WebControls.Label

        '''<summary>
        '''rblRentalPriceBasedOn control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents rblRentalPriceBasedOn As Global.System.Web.UI.WebControls.RadioButtonList
    End Class
End Namespace
