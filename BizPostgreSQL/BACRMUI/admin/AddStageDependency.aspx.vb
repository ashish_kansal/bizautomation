﻿Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Projects
Imports BACRM.BusinessLogic.Opportunities
Public Class AddStageDependency
    Inherits BACRMPage

    Dim objAdmin As New CAdmin
    Dim dtParentStage As New DataTable

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            btnClose.Attributes.Add("onclick", "return Close()")
            BindDependency()
        End If
    End Sub
    Sub BindDependency()
        Try
            objAdmin.Mode = 3
            objAdmin.DomainID = Session("DomainId")
            objAdmin.ProjectID = CCommon.ToLong(GetQueryStringVal("ProId"))
            objAdmin.numStageDetailsId = CCommon.ToLong(GetQueryStringVal("StageId"))

            dtParentStage = objAdmin.GetStageItemDependency()

            ddlProjectStage.DataSource = dtParentStage
            ddlProjectStage.DataTextField = "vcStageName"
            ddlProjectStage.DataValueField = "numStageDetailsId"
            ddlProjectStage.DataBind()
            ddlProjectStage.Items.Insert(0, "--Select One--")
            ddlProjectStage.Items.FindByText("--Select One--").Value = 0

            Dim dtStageItemDependency As DataTable
            objAdmin.Mode = 4
            dtStageItemDependency = objAdmin.GetStageItemDependency()
            gvDependencies.DataSource = dtStageItemDependency
            gvDependencies.DataBind()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub btnDependencies_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDependencies.Click
        Try
            objAdmin = New CAdmin
            objAdmin.numStageDetailsId = CCommon.ToLong(GetQueryStringVal("StageId"))
            objAdmin.numDependantOnID = ddlProjectStage.SelectedValue
            objAdmin.Mode = 0
            objAdmin.ManageStageItemDependency()

            BindDependency()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
    Private Sub gvDependencies_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvDependencies.RowCommand
        If e.CommandName = "DeleteDependency" Then
            Try
                objAdmin = New CAdmin

                objAdmin.numStageDetailsId = gvDependencies.DataKeys(e.CommandArgument).Values("numStageDetailId").ToString()
                objAdmin.numDependantOnID = gvDependencies.DataKeys(e.CommandArgument).Values("numDependantOnId").ToString()
                objAdmin.Mode = 1
                objAdmin.ManageStageItemDependency()

                BindDependency()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End If
    End Sub

End Class