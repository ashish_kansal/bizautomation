Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Contacts

Namespace BACRM.UserInterface.Admin

    Partial Public Class frmItemSearchRes
        Inherits BACRMPage
        Dim boolExport As Boolean = False
        Dim boolUpdate As Boolean
        Dim objCommon As CCommon
        Dim RegularSearch As String
        Dim CustomSearch As String

        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            Try
                'CLEAR ERROR ON RELOAD
                DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ""
                DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "none")

                'CLEAR ALERT MESSAGE
                divMessage.Style.Add("display", "none")
                litMessage.Text = ""

                objCommon = New CCommon
                If Not IsPostBack Then
                    ViewState("AdvancedSearchCondition") = Session("AdvancedSearchCondition")
                    ViewState("AdvancedSearchSavedSearchID") = Session("AdvancedSearchSavedSearchID")
                    Session("AdvancedSearchCondition") = Nothing
                    Session("AdvancedSearchSavedSearchID") = Nothing

                    txtCurrrentPage.Text = 1

                    Dim m_aryRightsEditSearchViews(), m_aryRightsMUpdate() As Integer
                    GetUserRightsForPage(9, 11)

                    m_aryRightsEditSearchViews = GetUserRightsForPage_Other(9, 2)
                    m_aryRightsMUpdate = GetUserRightsForPage_Other(9, 4)

                    If m_aryRightsEditSearchViews(RIGHTSTYPE.VIEW) <> 0 Then hplEditResView.Attributes.Add("onclick", "return EditSearchView();")
                    If m_aryRightsForPage(RIGHTSTYPE.EXPORT) = 0 Then btnExport.Visible = False
                    If m_aryRightsMUpdate(RIGHTSTYPE.VIEW) = 0 Then btnMassUpdate.Visible = False

                    'Saved search
                    If CCommon.ToString(Session("SavedSearchCondition")) = "" Then hplSaveSearch.Visible = False
                    Dim lngSearchID As Long = CCommon.ToLong(GetQueryStringVal("SearchID"))
                    If lngSearchID > 0 Then
                        Dim objSearch As New FormGenericAdvSearch
                        objSearch.SetSavedSearchQuery(lngSearchID, 15)
                    End If

                    PersistTable.Load()
                    If PersistTable.Count > 0 Then
                        txtSortColumn.Text = CCommon.ToString(PersistTable(PersistKey.SortColumnName))
                        txtSortChar.Text = CCommon.ToString(PersistTable(PersistKey.SortCharacter))
                        txtSortOrder.Text = CCommon.ToString(PersistTable(PersistKey.SortOrder))
                        ddlSort.ClearSelection()
                        If Not ddlSort.Items.FindByValue(CCommon.ToString(PersistTable(PersistKey.FilterBy))) Is Nothing Then ddlSort.Items.FindByValue(CCommon.ToString(PersistTable(PersistKey.FilterBy))).Selected = True
                        txtGridColumnFilter.Text = CCommon.ToString(PersistTable(PersistKey.GridColumnSearch))
                    End If

                    BindGridView(True)
                End If
               
                If txtReload.Text = "true" Then
                    BindGridView(True)
                    txtReload.Text = "false"
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Sub BindGridView(ByVal CreateCol As Boolean)
            Try
                Dim objAdmin As New FormGenericAdvSearch
                'Dim SortChar As Char
                Dim ds As DataSet
                Dim dtResults As DataTable
                Dim dtColumns As DataTable

                With objAdmin
                    .QueryWhereCondition = Session("WhereContditionOpp") & CCommon.ToString(Session("TimeQuery")) 'when sliding date is selected then we need to store where condition and time condition seperately
                    .DomainID = Session("DomainID")
                    .UserCntID = Session("UserContactID")
                    .PageSize = Session("PagingRows")
                    .FormID = 15
                    .DisplayColumns = CCommon.ToString(GetQueryStringVal("displayColumns"))
                    .SortCharacter = txtSortChar.Text.Trim()
                    .SortcolumnName = txtSortColumn.Text
                    .SortcolumnName = .SortcolumnName.Replace("cmp", "c")
                    If txtCurrrentPage.Text.Trim = "" Then txtCurrrentPage.Text = 1
                    .CurrentPage = txtCurrrentPage.Text.Trim()
                    .PageSize = Session("PagingRows")
                    .TotalRecords = 0
                    If txtSortOrder.Text = "D" Then
                        .columnSortOrder = "Desc"
                    Else : .columnSortOrder = "Asc"
                    End If
                    If Not IsPostBack Then
                        ddlSort.DataSource = .getSearchFieldList.Tables(1)              'Set the datasource for the available fields
                        ddlSort.DataTextField = "vcFormFieldName"                       'set the text field
                        ddlSort.DataValueField = "vcDbColumnName"                       'set the value attribut
                        ddlSort.DataBind()
                    End If
                    .columnName = ddlSort.SelectedValue

                    If boolExport = True Then
                        .GetAll = True
                        ds = .AdvancedSearchOpp()
                        dtResults = ds.Tables(0)
                        Dim iExportCount As Integer
                        dtResults.Columns.RemoveAt(1)
                        dtResults.Columns.RemoveAt(0)
                        Response.Clear()
                        For iExportCount = 0 To dtResults.Columns.Count - 1
                            If dtResults.Columns(iExportCount).ColumnName.StartsWith("Cust") AndAlso dtResults.Columns(iExportCount).ColumnName.EndsWith("~1") Then
                                If ds.Tables.Count > 1 AndAlso ds.Tables(1).Rows.Count > 0 Then
                                    For Each drColumn As DataRow In ds.Tables(1).Rows
                                        If CCommon.ToString(drColumn("vcDbColumnName")) & "~" & CCommon.ToString(drColumn("numFieldID")) & "~1" = dtResults.Columns(iExportCount).ColumnName Then
                                            dtResults.Columns(iExportCount).ColumnName = CCommon.ToString(drColumn("vcFieldName"))
                                        End If
                                    Next
                                End If
                            Else
                                dtResults.Columns(iExportCount).ColumnName = dtResults.Columns(iExportCount).ColumnName.Split("~")(0)
                            End If
                        Next
                        For Each column As DataColumn In dtResults.Columns
                            Response.Write(column.ColumnName + ",")
                        Next
                        Response.Write(Environment.NewLine)
                        For Each row As DataRow In dtResults.Rows
                            For iExportCount = 0 To dtResults.Columns.Count - 1
                                Response.Write(row(iExportCount).ToString().Replace(";", String.Empty) & ",")
                            Next
                            Response.Write(Environment.NewLine)
                        Next
                        Response.ContentType = "text/csv"
                        Response.AppendHeader("Content-Disposition", "attachment; filename=" + "File" + Format(Now, "ddmmyyyyhhmmss") + ".csv")
                        Response.End()
                        .GetAll = False
                    End If
                    'Added by chintan- for mass updating Orders ,as of now for order status
                    If boolUpdate = True Then
                        If chkSelectAll.Checked = False Then
                            Session("OppIDs") = ""
                            Dim gvRow As GridViewRow
                            For Each gvRow In gvSearch.Rows
                                If Not CType(gvRow.FindControl("chkSelect"), CheckBox) Is Nothing AndAlso CType(gvRow.FindControl("chkSelect"), CheckBox).Checked = True Then
                                    Session("OppIDs") = Session("OppIDs") & CType(gvRow.FindControl("lbl1"), Label).Text & ","
                                End If
                            Next
                            Session("OppIDs") = Session("OppIDs").ToString.TrimEnd(",")
                            If Session("OppIDs") <> "" Then
                                .strMassUpdate = CCommon.ToString(Session("UpdateQuery")).Replace("@$replace", "@$replace where OppMas.numOppId in ( " & Session("OppIDs") & "))").Replace("##OppCodes##", Session("OppIDs"))
                                .bitMassUpdate = 1
                            Else
                                .bitMassUpdate = 0
                            End If
                        Else
                            Session("OppIDs") = Nothing
                            .strMassUpdate = Replace(Session("UpdateQuery"), "@$replace", "@$replace join tt_TEMPTABLEAdvancedSearchOpp T on T.numOppID=OppMas.numOppID)")
                            .bitMassUpdate = 1
                        End If
                        .lookTable = Session("LookTable")
                        Session("UpdateQuery") = Nothing
                        Session("LookTable") = Nothing
                    End If

                    GridColumnSearchCriteria()
                    .RegularSearchCriteria = RegularSearch
                    .CustomSearchCriteria = CustomSearch

                    ds = .AdvancedSearchOpp()
                    dtResults = ds.Tables(0)
                    dtColumns = ds.Tables(1)

                    bizPager.PageSize = Session("PagingRows")
                    bizPager.CurrentPageIndex = txtCurrrentPage.Text
                    bizPager.RecordCount = objAdmin.TotalRecords
                End With

                PersistTable.Clear()
                PersistTable.Add(PersistKey.CurrentPage, txtCurrrentPage.Text)
                PersistTable.Add(PersistKey.SortCharacter, txtSortChar.Text.Trim())
                PersistTable.Add(PersistKey.SortColumnName, txtSortColumn.Text.Trim())
                PersistTable.Add(PersistKey.SortOrder, txtSortOrder.Text.Trim())
                PersistTable.Add(PersistKey.FilterBy, ddlSort.SelectedValue)
                PersistTable.Add(PersistKey.GridColumnSearch, txtGridColumnFilter.Text)
                PersistTable.Save()

                Dim htGridColumnSearch As New Hashtable

                If txtGridColumnFilter.Text.Trim.Length > 0 Then
                    Dim strValues() As String = txtGridColumnFilter.Text.Trim(";").Split(";")
                    Dim strIDValue() As String

                    For j = 0 To strValues.Length - 1
                        strIDValue = strValues(j).Split(":")

                        htGridColumnSearch.Add(strIDValue(0), strIDValue(1))
                    Next
                End If

                '   CreateCol = False
                If CreateCol = True Then
                    gvSearch.Columns.Clear()
                    Dim bField As BoundField
                    Dim Tfield As TemplateField
                    '    Dim i As Integer
                    '    For i = 0 To dtResults.Columns.Count - 1
                    '        If dtResults.Columns(i).ColumnName = "Opportunity Name" Or dtResults.Columns(i).ColumnName = "BizDocName" Then
                    '            Tfield = New TemplateField
                    '            Dim str As String()
                    '            str = dtResults.Columns(i).ColumnName.Split("~")

                    '            Tfield.HeaderTemplate = New MyTempItem(ListItemType.Header, str(0), str(1), dtResults.Columns(0).ColumnName, dtResults.Columns(1).ColumnName)
                    '            Tfield.ItemTemplate = New MyTempItem(ListItemType.Item, str(0), str(1), dtResults.Columns(0).ColumnName, dtResults.Columns(1).ColumnName)
                    '            gvSearch.Columns.Add(Tfield)
                    '        Else
                    '            If i < IIf(dtResults.Columns.Contains("numOppBizDocsID"), 2, 1) Then
                    '                bField = New BoundField
                    '                bField.DataField = dtResults.Columns(i).ColumnName
                    '                bField.Visible = False
                    '                gvSearch.Columns.Add(bField)
                    '            Else
                    '                Tfield = New TemplateField
                    '                Dim str As String()
                    '                str = dtResults.Columns(i).ColumnName.Split("~")
                    '                Tfield.HeaderTemplate = New MyTempItem(ListItemType.Header, str(0), str(1), dtResults.Columns(0).ColumnName, dtResults.Columns(1).ColumnName)

                    '                Tfield.ItemTemplate = New MyTempItem(ListItemType.Item, str(0), str(1), dtResults.Columns(0).ColumnName, dtResults.Columns(1).ColumnName)
                    '                gvSearch.Columns.Add(Tfield)
                    '            End If
                    '        End If
                    '    Next

                    For Each drRow As DataRow In dtColumns.Rows
                        Tfield = New TemplateField

                        Tfield.HeaderTemplate = New GridTemplate(ListItemType.Header, drRow, 0, htGridColumnSearch, 15, objAdmin.columnName, objAdmin.columnSortOrder)

                        Tfield.ItemTemplate = New GridTemplate(ListItemType.Item, drRow, 0, htGridColumnSearch, 15, objAdmin.columnName, objAdmin.columnSortOrder)
                        gvSearch.Columns.Add(Tfield)
                    Next

                    Dim dr As DataRow
                    dr = dtColumns.NewRow()
                    dr("vcAssociatedControlType") = "DeleteCheckBox"
                    dr("intColumnWidth") = "30"

                   Tfield = New TemplateField
                    Tfield.HeaderTemplate = New GridTemplate(ListItemType.Header, dr, 0, htGridColumnSearch, 15)
                    Tfield.ItemTemplate = New GridTemplate(ListItemType.Item, dr, 0, htGridColumnSearch, 15)
                    gvSearch.Columns.Add(Tfield)
                    '    If i >= 2 Then
                    '        Tfield = New TemplateField
                    '        Tfield.ItemStyle.Width = New Unit(25)
                    '        Tfield.HeaderTemplate = New MyTempItem(ListItemType.Header, "CheckBox", "", dtResults.Columns(0).ColumnName, dtResults.Columns(1).ColumnName)
                    '        Tfield.ItemTemplate = New MyTempItem(ListItemType.Item, "CheckBox", "", dtResults.Columns(0).ColumnName, dtResults.Columns(1).ColumnName)
                    '        gvSearch.Columns.Add(Tfield)
                    '    End If
                End If

                gvSearch.DataSource = dtResults
                gvSearch.DataBind()
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub GridColumnSearchCriteria()
            Try
                If txtGridColumnFilter.Text.Trim.Length > 0 Then
                    Dim strValues() As String = txtGridColumnFilter.Text.Trim(";").Split(";")

                    Dim strIDValue(), strID(), strCustom As String
                    Dim strRegularCondition As New ArrayList
                    Dim strCustomCondition As New ArrayList


                    For i As Integer = 0 To strValues.Length - 1
                        strIDValue = strValues(i).Split(":")
                        strID = strIDValue(0).Split("~")

                        If strID(0).Contains("CFW.Cust") Then
                            Select Case strID(3).Trim()
                                Case "TextBox", "TextArea"
                                    strCustomCondition.Add(" DM.numDivisionID in (select distinct DivisionMaster.numDivisionID from DivisionMaster left join CFW_Fld_Values CFW ON DivisionMaster.numDivisionID=CFW.RecId where DivisionMaster.numDomainId=" & Session("DomainID") & " AND CFW.fld_id =" & strID(0).Replace("CFW.Cust", "") & " and CFW.Fld_Value " & " iLIKE '%" & strIDValue(1).Replace("'", "''") & "%')")
                                Case "CheckBox"
                                    If strIDValue(1).ToLower() = "yes" Then
                                        strCustomCondition.Add(" DM.numDivisionID in (select distinct DivisionMaster.numDivisionID from DivisionMaster left join CFW_Fld_Values CFW ON DivisionMaster.numDivisionID=CFW.RecId where DivisionMaster.numDomainId=" & Session("DomainID") & " AND CFW.fld_id =" & strID(0).Replace("CFW.Cust", "") & " and COALESCE(CFW.Fld_Value,'') " & "=true)")
                                    ElseIf strIDValue(1).ToLower() = "no" Then
                                        strCustomCondition.Add(" DM.numDivisionID in (select distinct DivisionMaster.numDivisionID from DivisionMaster left join CFW_Fld_Values CFW ON DivisionMaster.numDivisionID=CFW.RecId where DivisionMaster.numDomainId=" & Session("DomainID") & " AND 1 = (CASE WHEN (SELECT COUNT(*) FROM CFW_Fld_Values CFWInner WHERE CFWInner.RecId=DivisionMaster.numDivisionID AND CFWInner.fld_id =" & strID(0).Replace("CFW.Cust", "") & " AND CFWInner.Fld_Value=true) > 0 THEN 0 ELSE 1 END))")
                                    End If
                                Case "SelectBox"
                                    strCustomCondition.Add(" DM.numDivisionID in (select distinct DivisionMaster.numDivisionID from DivisionMaster left join CFW_Fld_Values CFW ON DivisionMaster.numDivisionID=CFW.RecId where DivisionMaster.numDomainId=" & Session("DomainID") & " AND CFW.fld_id =" & strID(0).Replace("CFW.Cust", "") & " and CFW.Fld_Value " & "='" & strIDValue(1) & "')")
                                Case "DateField"
                                    If strID(4) = "From" Then
                                        Dim fromDate As Date
                                        If Date.TryParse(strIDValue(1), fromDate) Then
                                            strCustomCondition.Add(" DM.numDivisionID in (select distinct DivisionMaster.numDivisionID from DivisionMaster left join CFW_Fld_Values CFW ON DivisionMaster.numDivisionID=CFW.RecId where DivisionMaster.numDomainId=" & Session("DomainID") & " AND CFW.fld_id =" & strID(0).Replace("CFW.Cust", "") & " and CFW.Fld_Value <> '' and CAST(CFW.Fld_Value AS DATE) " & " >= '" & fromDate.ToString("MM/dd/yyyy") & "')")
                                        End If
                                    ElseIf strID(4) = "To" Then
                                        Dim toDate As Date
                                        If Date.TryParse(strIDValue(1), toDate) Then
                                            strCustomCondition.Add(" DM.numDivisionID in (select distinct DivisionMaster.numDivisionID from DivisionMaster left join CFW_Fld_Values CFW ON DivisionMaster.numDivisionID=CFW.RecId where DivisionMaster.numDomainId=" & Session("DomainID") & " AND CFW.fld_id =" & strID(0).Replace("CFW.Cust", "") & " and CFW.Fld_Value <> '' and CAST(CFW.Fld_Value AS DATE) " & " <= '" & toDate.ToString("MM/dd/yyyy") & "')")
                                        End If
                                    End If
                                Case "CheckBoxList"
                                    Dim items As String() = strIDValue(1).Split(",")
                                    Dim searchString As String = ""

                                    For Each item As String In items
                                        searchString = searchString & If(searchString.Length > 0, " OR ", "") & " fn_GetCustFldStringValue(" & strID(0).Replace("CFW.Cust", "") & ",DivisionMaster.numDivisionID,CFW.Fld_Value) " & " iLIKE '%" & item.Replace("'", "''") & "%'"
                                    Next

                                    strCustomCondition.Add(" DM.numDivisionID in (select distinct DivisionMaster.numDivisionID from DivisionMaster left join CFW_Fld_Values CFW ON DivisionMaster.numDivisionID=CFW.RecId where DivisionMaster.numDomainId=" & Session("DomainID") & " AND CFW.fld_id =" & strID(0).Replace("CFW.Cust", "") & " AND (" & searchString & "))")
                                Case Else
                                    strCustomCondition.Add(" DM.numDivisionID in (select distinct DivisionMaster.numDivisionID from DivisionMaster left join CFW_Fld_Values CFW ON DivisionMaster.numDivisionID=CFW.RecId where DivisionMaster.numDomainId=" & Session("DomainID") & " AND CFW.fld_id =" & strID(0).Replace("CFW.Cust", "") & " and CFW.Fld_Value " & strCustom & ")")
                            End Select
                        Else
                            Select Case strID(3).Trim()
                                Case "Website", "Email", "TextBox"
                                    strRegularCondition.Add(strID(0) & " iLIKE '%" & strIDValue(1).Replace("'", "''") & "%'")
                                Case "SelectBox"
                                    strRegularCondition.Add(strID(0) & "=" & strIDValue(1))
                                Case "TextArea"
                                    strRegularCondition.Add(" Cast(" & strID(0) & " as varchar(5000)) iLIKE '%" & strIDValue(1).Replace("'", "''") & "%'")
                                Case "DateField"
                                    If strID(4) = "From" Then
                                        Dim fromDate As Date
                                        If Date.TryParse(strIDValue(1), fromDate) Then
                                            strRegularCondition.Add(strID(0) & " >= '" & fromDate.ToString("MM/dd/yyyy") & "'")
                                        End If
                                    ElseIf strID(4) = "To" Then
                                        Dim toDate As Date
                                        If Date.TryParse(strIDValue(1), toDate) Then
                                            strRegularCondition.Add(strID(0) & " <= '" & toDate.ToString("MM/dd/yyyy") & "'")
                                        End If
                                    End If
                            End Select
                        End If
                    Next
                    RegularSearch = String.Join(" and ", strRegularCondition.ToArray()).Replace("cmp", "c")
                    RegularSearch = RegularSearch.Replace("Opp", "OppMas")
                    RegularSearch = RegularSearch.Replace("OBD", "OpportunityBizDocs")
                    CustomSearch = String.Join(" and ", strCustomCondition.ToArray())
                Else
                    RegularSearch = ""
                    CustomSearch = ""
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub btnExport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExport.Click
            Try
                boolExport = True
                BindGridView(True)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Private Sub btnUpdateValues_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpdateValues.Click
            Try
                boolUpdate = True
                BindGridView(True)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Private Sub btnGo1_Click(sender As Object, e As System.EventArgs) Handles btnGo1.Click
            Try
                txtCurrrentPage.Text = "1"
                BindGridView(True)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Protected Sub bizPager_PageChanged(sender As Object, e As EventArgs)
            Try
                txtCurrrentPage.Text = bizPager.CurrentPageIndex
                BindGridView(True)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Private Sub DisplayError(ByVal exception As String)
            Try
                DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = exception
                DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
                DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Focus()
            Catch ex As Exception

            End Try
        End Sub

        Private Sub ShowMessage(ByVal message As String)
            Try
                divMessage.Style.Add("display", "")
                litMessage.Text = message
                divMessage.Focus()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Private Sub lkbBackToSearchCriteria_Click(sender As Object, e As EventArgs) Handles lkbBackToSearchCriteria.Click
            Try
                Session("AdvancedSearchCondition") = ViewState("AdvancedSearchCondition")
                Session("AdvancedSearchSavedSearchID") = ViewState("AdvancedSearchSavedSearchID")
                Response.Redirect("~/admin/frmAdvancedSearchNew.aspx", False)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub
    End Class

    Public Class MyTempItem
        Implements ITemplate

        Dim TemplateType As ListItemType
        Dim Field1, Field2, Field3, Field4 As String

        Sub New(ByVal type As ListItemType, ByVal fld1 As String, ByVal fld2 As String, ByVal fld3 As String, ByVal fld4 As String)
            Try
                TemplateType = type
                Field1 = fld1
                Field2 = fld2
                Field3 = fld3
                Field4 = fld4
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub InstantiateIn(ByVal Container As Control) Implements ITemplate.InstantiateIn
            Try
                Dim lbl1 As Label = New Label()
                Dim lnkButton As New LinkButton
                Dim lnk As New HyperLink
                Select Case TemplateType
                    Case ListItemType.Header
                        If Field1 <> "CheckBox" Then
                            lnkButton.Text = Field1
                            lnkButton.Attributes.Add("onclick", "return SortColumn('" & Field2 & "')")
                            Container.Controls.Add(lnkButton)
                        Else
                            Dim chk As New CheckBox
                            chk.ID = "chkGridSelectAll"
                            chk.Attributes.Add("onclick", "return SelectAll('chkGridSelectAll','chkSelect')")
                            Container.Controls.Add(chk)
                        End If
                    Case ListItemType.Item
                        If Field1 <> "CheckBox" Then
                            AddHandler lbl1.DataBinding, AddressOf BindStringColumn
                            Container.Controls.Add(lbl1)
                        Else
                            Dim chk As New CheckBox
                            chk.ID = "chk"
                            chk.CssClass = "chkSelect"

                            AddHandler lbl1.DataBinding, AddressOf Bindvalue
                            Container.Controls.Add(chk)
                            Container.Controls.Add(lbl1)
                        End If
                End Select
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub Bindvalue(ByVal Sender As Object, ByVal e As EventArgs)
            Try
                Dim lbl1 As Label = CType(Sender, Label)
                Dim Container As GridViewRow = CType(lbl1.NamingContainer, GridViewRow)
                lbl1.ID = "lbl1"
                lbl1.Text = DataBinder.Eval(Container.DataItem, Field3) 'numOppId
                lbl1.Attributes.Add("style", "display:none")
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub BindStringColumn(ByVal Sender As Object, ByVal e As EventArgs)
            Try
                Dim lbl1 As Label = CType(Sender, Label)
                Dim Container As GridViewRow = CType(lbl1.NamingContainer, GridViewRow)
                If Field2 = "vcPoppName" Or Field2 = "vcBizDocID" Or Field2 = "vcBizDocName" Then
                    lbl1.Text = IIf(IsDBNull(DataBinder.Eval(Container.DataItem, Field1 & "~" & Field2)), "", DataBinder.Eval(Container.DataItem, Field1 & "~" & Field2))
                    Dim intermediatory As Integer
                    intermediatory = IIf(System.Web.HttpContext.Current.Session("EnableIntMedPage") = 1, 1, 0)
                    If Field2 = "vcPoppName" Then
                        lbl1.Text = "<a  href='javascript:OpenOpp(" & DataBinder.Eval(Container.DataItem, Field3) & "," & intermediatory & ",0)'>" & lbl1.Text & "</a> &nbsp;&nbsp;<a href='javascript:OpenOpp(" & DataBinder.Eval(Container.DataItem, Field3) & "," & intermediatory & ",1)'><img src='../images/open_new_window_notify.gif' width='15px' /></a>"
                    ElseIf Field2 = "vcBizDocID" Or Field2 = "vcBizDocName" Then
                        lbl1.Attributes.Add("onclick", "return OpenBizDoc(" & DataBinder.Eval(Container.DataItem, Field3) & "," & IIf(IsDBNull(DataBinder.Eval(Container.DataItem, Field4)), 0, DataBinder.Eval(Container.DataItem, Field4)) & ")")
                        lbl1.Text = "<a  href=#>" & lbl1.Text & "</a> &nbsp;&nbsp;&nbsp;"
                    End If
                Else : lbl1.Text = IIf(IsDBNull(DataBinder.Eval(Container.DataItem, Field1 & "~" & Field2)), "", DataBinder.Eval(Container.DataItem, Field1 & "~" & Field2))
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub BindStringColumn1(ByVal Sender As Object, ByVal e As EventArgs)
            Try
                Dim lbl1 As Label = CType(Sender, Label)
                Dim Container As GridViewRow = CType(lbl1.NamingContainer, GridViewRow)
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

    End Class
End Namespace