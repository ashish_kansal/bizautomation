﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmAdvAccRes.aspx.vb"
    Inherits="BACRM.UserInterface.Admin.frmAdvAccRes" MasterPageFile="~/common/GridMasterRegular.Master" %>

<%@ Register Assembly="AspNetPager, Version=7.4.4.0, Culture=neutral, PublicKeyToken=fb0a0fe055d40fd4" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>
<%@ Register Src="../common/frmBizSorting.ascx" TagName="frmBizSorting" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Financial Transaction Search Results</title>
    <script type="text/javascript">
        function EditSearchView() {
            window.open('../admin/frmAdvSearchColumnCustomization.aspx?FormId=59', "", "width=500,height=300,status=no,scrollbars=yes,left=155,top=160")
            return false;
        }
        function Back() {
            document.location.href = '../admin/frmAdvAcc.aspx';
            return false
        }
        function OpenTimeExpense(url) {

            window.open(url, "TimeExoenses", 'toolbar=no,titlebar=no,left=200, top=300,width=550,height=400,scrollbars=no,resizable=yes');
            return false;
        }
        function OpenBizIncome(url) {
            window.open(url, "BizInvoiceGL", 'toolbar=no,titlebar=no,left=200, top=300,width=750,height=550,scrollbars=no,resizable=yes');
            return false;
        }
        function OpenBill(a) {
            window.open('../Accounting/frmAddBill.aspx?BillID=' + a, '', 'toolbar=no,menubar=yes,titlebar=no,left=100,top=15,width=750,height=450,scrollbars=yes,resizable=yes');
            return false;
        }
        function OpenLandedCost(a) {
            window.open("../opportunity/frmLandedCostBill.aspx?OppId=" + a, '', 'toolbar=no,titlebar=no,left=300,width=800,height=500,scrollbars=yes,resizable=yes');
            return false;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server"
    ClientIDMode="Static">
    <div id="divMessage" runat="server" class="row padbottom10" style="display: none">
        <div class="col-xs-12">
            <div class="alert alert-warning">
                <h4><i class="icon fa fa-warning"></i>Alert!</h4>
                <asp:Literal ID="litMessage" runat="server" EnableViewState="False"></asp:Literal>
            </div>
        </div>
    </div>
    <div class="row padbottom10">
        <div class="col-xs-12">
            <div class="pull-right">
                <asp:UpdatePanel ID="UpdatePanel1" runat="server" style="display: inline;">
                    <ContentTemplate>
                        <asp:Button runat="server" ID="btnExport" CssClass="btn btn-primary" Text="Export To Excel" />
                    </ContentTemplate>
                    <Triggers>
                        <asp:PostBackTrigger ControlID="btnExport" />
                    </Triggers>
                </asp:UpdatePanel>
                <asp:LinkButton ID="lkbBackToSearchCriteria" CssClass="btn btn-primary" runat="server" CausesValidation="False"><i class="fa fa-arrow-left"></i>&nbsp;&nbsp;Back</asp:LinkButton>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server" ClientIDMode="Static">
    Search Results
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BizPager" runat="server" ClientIDMode="Static">
    <webdiyer:AspNetPager ID="bizPager" runat="server"
        PagingButtonSpacing="0"
        CssClass="bizgridpager"
        AlwaysShow="true"
        CurrentPageButtonClass="active"
        PagingButtonUlLayoutClass="pagination"
        PagingButtonLayoutType="UnorderedList"
        FirstPageText="<<"
        LastPageText=">>"
        NextPageText=">"
        PrevPageText="<"
        Width="100%"
        UrlPaging="false"
        NumericButtonCount="8"
        ShowPageIndexBox="Never"
        ShowCustomInfoSection="Left"
        OnPageChanged="bizPager_PageChanged"
        CustomInfoHTML="Showing records %StartRecordIndex% to %EndRecordIndex% of %RecordCount% "
        CustomInfoClass="bizpagercustominfo">
    </webdiyer:AspNetPager>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="GridPlaceHolder" runat="server"
    ClientIDMode="Static">
    <asp:UpdateProgress ID="UpdateProgress" runat="server" ClientIDMode="Static">
        <ProgressTemplate>
            <div class="overlay">
                <div class="overlayContent" style="color: #000; text-align: center; width: 250px; padding: 20px">
                    <i class="fa fa-2x fa-refresh fa-spin"></i>
                    <h3>Processing Request</h3>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <div class="row">
        <div class="col-xs-12">
            <div class="table-responsive">
                <asp:Repeater ID="RepGeneralLedger" runat="server">
                    <HeaderTemplate>
                        <table class="table table-bordered table-striped" border="0" width="100%">
                            <tr class="hs">
                                <th>Date
                                </th>
                                <th>Transaction Type
                                </th>
                                <th>Name
                                </th>
                                <th>Memo/Description
                                </th>
                                <th>Account
                                </th>
                                <th>Class
                                </th>
                                <th>Amount
                                </th>
                                <th>Narration
                                </th>
                            </tr>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr>
                            <td>
                                <asp:Label ID="lblJDate" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "Date")%>'></asp:Label>
                            </td>
                            <td>
                                <asp:LinkButton ID="lnkType" runat="server" CssClass="hyperlink" CommandName="Edit"><%#DataBinder.Eval(Container.DataItem, "TransactionType")%></asp:LinkButton>
                                <asp:Label ID="lblType" runat="server" Visible="false"><%#DataBinder.Eval(Container.DataItem, "TransactionType")%></asp:Label>
                            </td>
                            <td>
                                <%#Eval("CompanyName")%>
                            </td>
                            <td>
                                <%#Eval("TranDesc")%>
                            </td>
                            <td>
                                <%# Eval("vcAccountName")%>
                            </td>
                            <td>
                                <%# Eval( "vcClass")%>
                            </td>
                            <td align="right" class="money">
                                <%#Eval("numDebitAmt")%>
                                <asp:Label ID="lblJournalId" runat="server" Style="display: none" Text='<%# DataBinder.Eval(Container.DataItem,"JournalId")%>'></asp:Label>
                                <asp:Label ID="lblCheckId" runat="server" Style="display: none" Text='<%# DataBinder.Eval(Container.DataItem,"CheckId")%>'></asp:Label>
                                <asp:Label ID="lblCashCreditCardId" runat="server" Style="display: none" Text='<%# DataBinder.Eval(Container.DataItem,"CashCreditCardId")%>'></asp:Label>
                                <asp:Label ID="lblChartAcntId" runat="server" Style="display: none" Text='<%# DataBinder.Eval(Container.DataItem,"numAccountID")%>'></asp:Label>
                                <asp:Label ID="lblTransactionId" runat="server" Style="display: none" Text='<%# DataBinder.Eval(Container.DataItem,"numTransactionId")%>'></asp:Label>
                                <asp:Label ID="lblOppId" Style="display: none" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"numOppId")%>'></asp:Label>
                                <asp:Label ID="lblOppBizDocsId" Style="display: none" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"numOppBizDocsId")%>'>'</asp:Label>
                                <asp:Label ID="lblDepositId" Style="display: none" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"numDepositId")%>'></asp:Label>
                                <asp:Label ID="lblCategoryHDRID" runat="server" Style="display: none" Text='<%# DataBinder.Eval(Container.DataItem,"numCategoryHDRID")%>'></asp:Label>
                                <asp:Label ID="lblTEType" Style="display: none" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"tintTEType")%>'></asp:Label>
                                <asp:Label ID="lblCategory" runat="server" Style="display: none" Text='<%# DataBinder.Eval(Container.DataItem,"numCategory")%>'></asp:Label>
                                <asp:Label ID="lblUserCntID" runat="server" Style="display: none" Text='<%# DataBinder.Eval(Container.DataItem,"numUserCntID")%>'></asp:Label>
                                <asp:Label ID="lblFromDate" runat="server" Style="display: none" Text='<%# DataBinder.Eval(Container.DataItem,"dtFromDate")%>'></asp:Label>
                                <asp:Label ID="lblBillPaymentID" Style="display: none" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"numBillPaymentID")%>'></asp:Label>
                                <asp:Label ID="lblBillID" Style="display: none" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"numBillID")%>'></asp:Label>
                            </td>
                            <td>
                                <%#Eval("TranRef")%> <%#Eval("Narration")%>
                            </td>
                        </tr>
                    </ItemTemplate>
                </asp:Repeater>
            </div>
        </div>
    </div>
    <asp:TextBox ID="txtTotalPage" Style="display: none" runat="server"></asp:TextBox>
    <asp:TextBox ID="txtTotalRecords" Style="display: none" runat="server"></asp:TextBox>
    <asp:TextBox ID="txtSortChar" Text="0" Style="display: none" runat="server"></asp:TextBox>
    <asp:TextBox ID="txtSortColumn" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtSortOrder" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtReload" runat="server" Style="display: none">False</asp:TextBox>
    <asp:Button ID="btnUpdateValues" runat="server" Style="display: none" />
    <asp:TextBox ID="txtCurrrentPage" runat="server" Style="display: none"></asp:TextBox>
    <asp:Button ID="btnGo1" Width="25" runat="server" Style="display: none" />
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="GridBizSorting" runat="server" ClientIDMode="Static">
    <uc1:frmBizSorting ID="frmBizSorting2" runat="server" />
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="GridSettingPopup" runat="server" ClientIDMode="Static">
    <asp:HyperLink runat="server" ID="hplEditResView" ToolTip="Edit advance search result view"><i class="fa fa-lg fa-gear" onclick="return EditSearchView()"></i></asp:HyperLink>
</asp:Content>
