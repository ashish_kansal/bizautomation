Imports System.Text.RegularExpressions
Imports BACRM.BusinessLogic.Marketing

''' -----------------------------------------------------------------------------
''' Project	 : BACRM.UserInterface.Reports
''' Class	 : frmFormConfigWizard
''' 
''' -----------------------------------------------------------------------------
''' <summary>
'''     This is a wizard for the configuration of the following forms
'''     1) Adv. Search
'''     2) Lead Box
'''     3) Order Form
'''     4) Survey
'''     5) Campaign
''' </summary>
''' <remarks>
''' </remarks>
''' <history>
''' 	[Debasish]	08/07/2005	Created
''' </history>
''' -----------------------------------------------------------------------------
Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Opportunities
Imports System.IO
Imports BACRM.BusinessLogic.Survey
Imports System.Web.Services
Imports Newtonsoft.Json

Namespace BACRM.UserInterface.Admin

    Public Class frmFormConfigWizard
        Inherits BACRMPage
        Dim objCommon As CCommon
        Dim numGrpId As Integer                                 'Contains the Group Id for the form
#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Dim sXMLFilePath As String                              'store the filepath to the xml
        Dim cAOIField As String                                 'String to store if the field is an AOI or not
        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.
        Private designerPlaceholderDeclaration As System.Object

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This event is fired eachtime thepage is called. In this event we will 
        '''     get the data from the DB and populate the Tables for the wizard.
        ''' </summary>
        ''' <param name="sender">Represents the sender object.</param>
        ''' <param name="e">Represents the EventArgs.</param>
        ''' <remarks>
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	08/07/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Try
                GetUserRightsForPage(13, 3)
                If Not IsPostBack Then
                    DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ""
                    DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "none")
                    If m_aryRightsForPage(RIGHTSTYPE.ADD) = 0 Then
                        btnSaveConfiguration.Visible = False
                    End If

                    Dim listItem As New ListItem
                    objCommon = New CCommon
                    Dim dtRelationship As DataTable = objCommon.GetMasterListItems(5, Session("DomainID"))
                    For Each drRelation As DataRow In dtRelationship.Rows
                        listItem = New ListItem
                        listItem.Text = CCommon.ToString(drRelation("vcData"))
                        listItem.Value = CCommon.ToLong(drRelation("numListItemID"))
                        ddlRelationShip.Items.Add(listItem)
                    Next

                    ddlContactType.DataSource = objCommon.GetMasterListItems(8, Session("DomainID"))
                    ddlContactType.DataTextField = "vcData"
                    ddlContactType.DataValueField = "numListItemID"
                    ddlContactType.DataBind()

                    listItem = New ListItem
                    listItem.Text = "-- Select One --"
                    listItem.Value = 0

                    ddlRelationShip.Items.Insert(0, listItem)
                    ddlContactType.Items.Insert(0, listItem)

                    Dim objConfigWizard As New FormConfigWizard
                    objConfigWizard.DomainID = Session("DomainId")
                    ddlUserGroup.DataSource = objConfigWizard.getAuthenticationGroups()
                    ddlUserGroup.DataTextField = "vcGroupName"             'set the text attribute
                    ddlUserGroup.DataValueField = "numGroupID"             'set the value attribute
                    ddlUserGroup.DataBind()

                    If Session("UserGroupID") IsNot Nothing Then
                        If ddlUserGroup.Items.FindByValue(Session("UserGroupID")) IsNot Nothing Then
                            ddlUserGroup.Items.FindByValue(Session("UserGroupID")).Selected = True
                            ddlUserGroup.Items.FindByValue(Session("UserGroupID")).Attributes.Add("style", "color:green")
                        End If
                    End If

                    Dim radItem As Telerik.Web.UI.RadComboBoxItem
                    For Each item As ListItem In ddlUserGroup.Items
                        If CCommon.ToLong(item.Value) <> CCommon.ToLong(ddlUserGroup.SelectedValue) Then
                            radItem = New Telerik.Web.UI.RadComboBoxItem
                            radItem.Text = item.Text
                            radItem.Value = item.Value
                            rcbGroups.Items.Add(radItem)
                        End If
                    Next

                    BindBizFormWizardModule()
                    BindFormList()

                    objConfigWizard.UserID = Session("UserID")                                      'Set value of User id
                    Dim dtGroupNames As DataTable                                                   'declare a datatable
                    dtGroupNames = objConfigWizard.getGroupNames()                                  'call to get the groups
                    Dim dRow As DataRow                                                             'declare a datarow
                    dRow = dtGroupNames.NewRow()                                                    'get a new row object
                    dtGroupNames.Rows.InsertAt(dRow, 0)                                             'insert the row
                    dtGroupNames.Rows(0).Item("numGrpID") = 0                                       'set the values for the first entry
                    dtGroupNames.Rows(0).Item("vcGrpName") = "-- Select One --"                     'set the group name
                    ddlGroup.DataSource = dtGroupNames.DefaultView                                  'set the datasource
                    ddlGroup.DataTextField = "vcGrpName"                                            'set the text attribute to group name
                    ddlGroup.DataValueField = "numGrpID"                                            'set the value attribute ot the grou pid
                    ddlGroup.DataBind()
                End If

                PostInitializeControls()        'Takes charge of post initialization settings of the controls
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This method is used to databind the listboxes/ radionbuttonlist to the dataset
        ''' </summary>
        ''' <remarks>
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	09/07/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Sub DataBindElements()
            Try
                btnSaveConfiguration.Enabled = True

                If ddlBizFormModule.SelectedValue = "2" AndAlso Not ddlFormList.SelectedItem Is Nothing AndAlso ddlFormList.SelectedItem.Value.StartsWith("1~") Then 'custom replationships
                    If Not ddlFormList.SelectedItem Is Nothing AndAlso ddlFormList.SelectedItem.Value.StartsWith("1~3~") Then
                        LoadFieldsForDetailsAndColumnSettings(36, "", 3, 1, CCommon.ToLong(GetFormValue()), False, True)
                    Else
                        LoadFieldsForDetailsAndColumnSettings(36, "", 1, 1, CCommon.ToLong(GetFormValue()), True, True)
                    End If
                ElseIf GetFormValue() = "99" Then 'Lead Details 
                    LoadFieldsForDetailsAndColumnSettings(34, "L", 3, 1, CCommon.ToLong(ddlRelationShip.SelectedValue), 0, 0)
                ElseIf GetFormValue() = "100" Then 'Prospect Details 
                    LoadFieldsForDetailsAndColumnSettings(35, "P", 3, 1, CCommon.ToLong(ddlRelationShip.SelectedValue), 0, 0)
                ElseIf GetFormValue() = "101" Then 'Account Details  
                    LoadFieldsForDetailsAndColumnSettings(36, "P", 3, 1, CCommon.ToLong(ddlRelationShip.SelectedValue), 0, 0)
                ElseIf GetFormValue() = "146" Then 'Vendor Details
                    LoadFieldsForDetailsAndColumnSettings(36, "P", 3, 1, 47, 0, 0)
                ElseIf GetFormValue() = "147" Then 'Employer Details
                    LoadFieldsForDetailsAndColumnSettings(36, "P", 3, 1, 93, 0, 0)
                ElseIf GetFormValue() = "102" Then 'Contact Details  
                    LoadFieldsForDetailsAndColumnSettings(10, "c", 3, 4, CCommon.ToLong(ddlContactType.SelectedValue), 0, 0)
                ElseIf GetFormValue() = "103" Then 'Project Details 
                    LoadFieldsForDetailsAndColumnSettings(13, "R", 3, 11, 0, 0, 0)
                ElseIf GetFormValue() = "104" Then 'Case Details  
                    LoadFieldsForDetailsAndColumnSettings(12, "S", 3, 3, 0, 0, 0)
                ElseIf GetFormValue() = "105" Then 'Sales Opportunity Details  
                    LoadFieldsForDetailsAndColumnSettings(38, "O", 3, 2, 0, 0, 0)
                ElseIf GetFormValue() = "106" Then 'Sales Order Details 
                    LoadFieldsForDetailsAndColumnSettings(39, "O", 3, 2, 0, 0, 0)
                ElseIf GetFormValue() = "107" Then 'Purchase Opportunity Details 
                    LoadFieldsForDetailsAndColumnSettings(40, "u", 3, 6, 0, 0, 0)
                ElseIf GetFormValue() = "108" Then 'Purchase Order Details 
                    LoadFieldsForDetailsAndColumnSettings(41, "u", 3, 6, 0, 0, 0)
                ElseIf GetFormValue() = "109" Then 'Lead Grid Columns
                    LoadFieldsForDetailsAndColumnSettings(34, "L", 1, 1, 1, 1, 0)
                ElseIf GetFormValue() = "110" Then 'Prospect Grid Columns
                    LoadFieldsForDetailsAndColumnSettings(35, "P", 1, 1, 3, 1, 0)
                ElseIf GetFormValue() = "111" Then 'Account Grid Columns
                    LoadFieldsForDetailsAndColumnSettings(36, "P", 1, 1, 2, 1, 0)
                ElseIf GetFormValue() = "112" Then 'Vendor Grid Columns
                    LoadFieldsForDetailsAndColumnSettings(36, "P", 1, 1, 47, 1, 0)
                ElseIf GetFormValue() = "113" Then 'Employer Grid Columns
                    LoadFieldsForDetailsAndColumnSettings(36, "P", 1, 1, 93, 1, 0)
                ElseIf GetFormValue() = "114" Then 'Contact Grid Columns
                    LoadFieldsForDetailsAndColumnSettings(10, "c", 1, 4, CCommon.ToLong(ddlContactType.SelectedValue), 1, 0)
                ElseIf GetFormValue() = "115" Then 'Project Grid Columns
                    LoadFieldsForDetailsAndColumnSettings(13, "R", 1, 11, 0, 1, 0)
                ElseIf GetFormValue() = "116" Then 'Case Grid Columns
                    LoadFieldsForDetailsAndColumnSettings(12, "S", 1, 3, 0, 1, 0)
                ElseIf GetFormValue() = "117" Then 'Tickler Grid Columns
                    LoadFieldsForDetailsAndColumnSettings(43, "", 1, 0, 0, 1, 0)
                ElseIf GetFormValue() = "118" Then 'Sales Opportunity Grid Columns
                    LoadFieldsForDetailsAndColumnSettings(38, "", 1, 2, 38, 1, 0)
                ElseIf GetFormValue() = "119" Then 'Sales Order Grid Columns
                    LoadFieldsForDetailsAndColumnSettings(39, "", 1, 2, 39, 1, 0)
                ElseIf GetFormValue() = "120" Then 'Purchase Opportunity Grid Columns
                    LoadFieldsForDetailsAndColumnSettings(40, "", 1, 6, 40, 1, 0)
                ElseIf GetFormValue() = "121" Then 'Purchase Order Grid Configuration
                    LoadFieldsForDetailsAndColumnSettings(41, "", 1, 6, 41, 1, 0)
                ElseIf GetFormValue() = "26" Then 'Products & services Setting (Sales)
                    LoadFieldsForDetailsAndColumnSettings(26, "", 1, 5, 0, 1, 0)
                ElseIf GetFormValue() = "129" Then 'Products & services Setting (Purchase)
                    LoadFieldsForDetailsAndColumnSettings(129, "", 1, 5, 0, 1, 0)
                ElseIf GetFormValue() = "130" Then 'Item Grid
                    LoadFieldsForDetailsAndColumnSettings(21, "", 1, 5, 0, 1, 0)
                ElseIf GetFormValue() = "131" Then
                    BindActivityFormDetails()
                Else
                    '================================================================================
                    ' Purpose: Calls the subroutine to databind the AOI listbox to dataset
                    '
                    ' History
                    ' Ver   Date        Ref     Author              Reason Created
                    ' 1     09/07/2005          Debasish Nag        Calls the subroutine to databind 
                    '                                               the AOI listbox to dataset
                    '================================================================================
                    Dim objConfigWizard As New FormConfigWizard             'Create an object of form config wizard
                    objConfigWizard.DomainID = Session("DomainID")          'Set value of domain id
                    objConfigWizard.numBizFormModuleID = CCommon.ToLong(ddlBizFormModule.SelectedValue)
                    objConfigWizard.OnlySurvey = False                      'Set value of Survey only flag

                    Dim dsAvailableAOIList As DataSet                     'Declare a datatable
                    Dim dtAvailableFldList As DataTable                     'Declare a datatable to store available fields
                    Dim dtColumnOneFldList As DataTable                     'Declare a datatable to store fields in columns 1
                    Dim dtColumnTwoFldList As DataTable                     'Declare a datatable to store fields in columns 2
                    Dim dtColumnThreeFldList As DataTable                   'Declare a datatable to store fields in columns 3
                    Dim cCustomFieldsAssociated As String                   'String to store if the custom field is associated or not

                    Dim dtFormsList As DataTable                            'Declare a datatable to contain the form list
                    Dim dtFldNameList As DataTable                          'Declare a datatable to contain the form fields names
                    Dim dtMandatoryFldNameList As DataTable                 'Declare a datatable to contain the mandatory form fields names
                    '================================================================================
                    ' Purpose: Calls the subroutine to databind the forms radiolist to dataset
                    '
                    ' History
                    ' Ver   Date        Ref     Author              Reason Created
                    ' 1     09/07/2005          Debasish Nag        Calls the subroutine to databind 
                    '                                               the forms radiolist to dataset
                    '================================================================================

                    dtFormsList = objConfigWizard.getFormList()                         'call function to get the forms
                    If dtFormsList.Rows.Count = 0 Then                                  'If there are no master data for forms
                        Exit Sub                                                        'exit the process
                    End If

                    If GetFormValue() = "102" Or GetFormValue() = "114" Then 'Contact Details 
                        pnlContact.Visible = True
                    Else
                        pnlContact.Visible = False
                    End If

                    'If GetFormValue() > 5 And GetFormValue() < 15 Then
                    If GetFormValue() = 8 Or GetFormValue() = 7 Then
                        Dim BizDocId As Int64 = 0
                        Dim BizDocTemplateId As Int64 = 0

                        If ddlBizDoc.Items.Count > 0 Then
                            BizDocId = ddlBizDoc.SelectedValue
                        End If

                        BindBizDocs()

                        If ddlBizDoc.Items.FindByValue(BizDocId) IsNot Nothing Then
                            ddlBizDoc.ClearSelection()
                            ddlBizDoc.Items.FindByValue(BizDocId).Selected = True
                        End If

                        If ddlBizDocTemplate.Items.Count > 0 Then
                            BizDocTemplateId = CCommon.ToLong(ddlBizDocTemplate.SelectedValue)
                        End If

                        BindBizDocsTemplate()

                        If ddlBizDocTemplate.Items.Count = 0 Then
                            lblError.Text = "First create BizDoc Template for selected BizDoc."
                            btnSaveConfiguration.Enabled = False
                        Else
                            lblError.Text = ""
                        End If

                        If ddlBizDocTemplate.Items.FindByValue(BizDocTemplateId) IsNot Nothing Then
                            ddlBizDocTemplate.ClearSelection()
                            ddlBizDocTemplate.Items.FindByValue(BizDocTemplateId).Selected = True
                        End If

                        Dim objBizDocs As New OppBizDocs
                        objBizDocs.BizDocId = ddlBizDoc.SelectedValue
                        objBizDocs.DomainID = Session("DomainID")
                        objBizDocs.BizDocTemplateID = CCommon.ToLong(ddlBizDocTemplate.SelectedValue)
                        hplBizDoc.Text = "Attachments(" & objBizDocs.GetBizDocAttachments.Rows.Count & ")"
                    End If
                    objConfigWizard.FormID = GetFormValue()
                    cCustomFieldsAssociated = dtFormsList.Rows(ddlFormList.SelectedIndex).Item("cCustomFieldsAssociated") 'get the flag to check of custom fields are to be displayed or not
                    cAOIField = dtFormsList.Rows(ddlFormList.SelectedIndex).Item("cAOIAssociated") 'get the aoi flag
                    numGrpId = IIf(IsDBNull(dtFormsList.Rows(ddlFormList.SelectedIndex).Item("numGrpId")), 0, dtFormsList.Rows(ddlFormList.SelectedIndex).Item("numGrpId")) 'Get the numGrpId, the Group associated with the form
                    sXMLFilePath = CCommon.GetDocumentPhysicalPath()  'set the file path
                    hdXMLPath.Value = sXMLFilePath                                      'Set the same path into a hidden variable
                    '================================================================================
                    ' Purpose:  Calls the subroutine to databind the AOI/ Available/Column One/ Column two and column three
                    '           Fields listbox to dataset. Since Authentication Groups is there only for Adv. Search
                    '           so there is no need to filter on auth group ids
                    '
                    ' History
                    ' Ver   Date        Ref     Author              Reason Created
                    ' 1     09/07/2005          Debasish Nag        Calls the subroutine to databind 
                    '                                               the listboxes to datatables
                    '================================================================================


                    '================================================================================
                    ' Purpose: databinding and processing form header information
                    '
                    ' History
                    ' Ver   Date        Ref     Author              Reason Created
                    ' 1     09/07/2005          Debasish Nag        process form header information 
                    '================================================================================

                    If objConfigWizard.FormID = 1 Or objConfigWizard.FormID = 2 Or objConfigWizard.FormID = 3 Or objConfigWizard.FormID = 4 Or objConfigWizard.FormID = 6 Or objConfigWizard.FormID = 128 Then 'called only for adv search, Email Profile, leadbox and order form

                        Dim sControlValue As String                                                 'declare a variable to store the contol value
                        sControlValue = dtFormsList.Rows(ddlFormList.SelectedIndex).Item("vcAdditionalParam") 'get the additional param value
                        litDynamicFormHeaderText.Visible = True         'unhide the placeholder control
                        divlitDynamicFormHeaderText.Visible = True
                        createDynamicHeaderControls(objConfigWizard.FormID, sControlValue)          'called for creating dynamic header control
                        If litDynamicFormHeaderText.Text = "" Then
                            divplhDynamicFormHeaders.Visible = False
                            divlitDynamicFormHeaderText.Visible = False
                        End If
                    Else : litDynamicFormHeaderText.Text = ""
                        divlitDynamicFormHeaderText.Visible = False
                        divplhDynamicFormHeaders.Visible = False
                    End If
                    createDynamicFormGroups(objConfigWizard.FormID, numGrpId)                       'called for creating dynamic groups for the form

                    cCustomFieldsAssociated = IIf((cCustomFieldsAssociated = "Y"), "", "C")         'filter condition for custom fields/ regular fields
                    If cAOIField = "N" Then                                                         'Check if AOI's are associated with a form or not
                        tblBzFormWizardConfigAOI.Visible = False                                    'hide the form if aoi's are not associated  
                        tblBzFormWizardConfigAOIHeader.Visible = False
                    Else
                        tblBzFormWizardConfigAOI.Visible = True                                     'unhide the form if aoi's are not associated  
                        tblBzFormWizardConfigAOIHeader.Visible = True
                    End If

                    '================================================================================
                    ' Purpose: Retrieving data related to the form and the auth group
                    '
                    ' History
                    ' Ver   Date        Ref     Author              Reason Created
                    ' 1     09/07/2005          Debasish Nag        Get Form and Auth Grp data
                    '================================================================================
                    'objConfigWizard.AuthenticationGroupID = ddlGroup.SelectedValue

                    'If dtFldNameList.Rows.Count > 0 Then
                    If GetFormValue() = 3 Then
                        If Convert.ToString(ddlLeadFormList.SelectedValue) = "0" Then
                            objConfigWizard.numSubFormId = 0
                        Else
                            objConfigWizard.numSubFormId = ddlLeadFormList.SelectedValue
                        End If
                    Else
                        objConfigWizard.numSubFormId = 0
                    End If

                    If GetFormValue() = 1 Or GetFormValue() = 2 Or GetFormValue() = 6 Or GetFormValue() = 15 Or GetFormValue() = 17 Or GetFormValue() = 18 Or GetFormValue() = 29 Or GetFormValue() = 59 Then
                        objConfigWizard.AuthenticationGroupID = ddlUserGroup.SelectedValue
                        objConfigWizard.BizDocTemplateID = 0
                    ElseIf GetFormValue() = 7 Or GetFormValue() = 8 Then
                        objConfigWizard.AuthenticationGroupID = ddlBizDoc.SelectedItem.Value
                        If Not ddlBizDocTemplate.SelectedItem Is Nothing Then
                            objConfigWizard.BizDocTemplateID = ddlBizDocTemplate.SelectedItem.Value
                        End If
                    ElseIf GetFormValue() = 56 Then
                        objConfigWizard.AuthenticationGroupID = ddlUserGroup.SelectedValue
                        objConfigWizard.BizDocTemplateID = ddlRelationShip.SelectedValue

                    ElseIf GetFormValue() = 5 Then
                        If ddlSurvey.Items.Count > 0 Then
                            objConfigWizard.AuthenticationGroupID = ddlSurvey.SelectedValue
                        Else
                            objConfigWizard.AuthenticationGroupID = 0
                        End If
                    End If

                    If GetFormValue() = 7 Or GetFormValue() = 8 Or GetFormValue() = 1 Or GetFormValue() = 15 Or GetFormValue() = 17 Or GetFormValue() = 18 Or GetFormValue() = 29 Or GetFormValue() = 59 Or GetFormValue() = 56 Then
                        dtFldNameList = objConfigWizard.getFieldNameListFromDB()                  'call function to get the fields names
                    Else
                        dtFldNameList = objConfigWizard.getFieldNameList(sXMLFilePath)                  'call function to get the fields names
                    End If

                    'End If
                    dtAvailableFldList = objConfigWizard.getFieldList(cCustomFieldsAssociated) 'call function to get the Available fields; pass column = 0
                    dtMandatoryFldNameList = objConfigWizard.getMandatoryFieldNameList(sXMLFilePath) 'Call the function to return the mandatory fields
                    dtAvailableFldList = HTMLDecodeFieldList(objConfigWizard.establishRelation(dtAvailableFldList, dtFldNameList, dtMandatoryFldNameList)) ' call to establish relation

                    '================================================================================
                    ' Purpose: databinding and processing form listboxes 1, 2 and 3
                    '
                    ' History
                    ' Ver   Date        Ref     Author              Reason Created
                    ' 1     09/31/2005          Debasish Nag        Databind the listboxes 1, 2 and 3
                    '================================================================================

                    lstAvailablefld.Items.Clear()
                    lstSelectedfldOne.Items.Clear()
                    lstSelectedfldTwo.Items.Clear()
                    lstSelectedfldThree.Items.Clear()
                    lstSelectedfldAOI.Items.Clear()


                    Dim dvAvailableList As New DataView(dtAvailableFldList)                 'create a dataview object
                    dvAvailableList.RowFilter = "intColumnNum = 0 and numFormId = " & GetFormValue() & " and numAuthGroupId = 0" 'set the row filter to 0 for available fields






                    dvAvailableList.Sort = "vcFormFieldName asc"                            'sort order    
                    'lstAvailablefld.DataSource = dvAvailableList.ToTable(True, "numFormFieldId", "vcFormFieldName")                        'Set the datasource for the available fields
                    lstAvailablefld.DataSource = dvAvailableList
                    lstAvailablefld.DataTextField = "vcNewFormFieldName"                   'set the text field
                    lstAvailablefld.DataValueField = "numFormFieldId"                   'set the value attribut
                    lstAvailablefld.DataBind()                                          'Databind the fields


                    Dim dvAvailableListOne As New DataView(dtAvailableFldList)          'create a dataview object
                    If GetFormValue() = 3 AndAlso Convert.ToString(ddlLeadFormList.SelectedValue) <> "0" Then
                        dvAvailableListOne.RowFilter = "intColumnNum = 1 and numFormId = " & objConfigWizard.FormID & "  and numSubFormId = " & ddlLeadFormList.SelectedValue & "   and numAuthGroupId = " & objConfigWizard.AuthenticationGroupID 'set the row filter for selected fields in column 1
                    Else
                        dvAvailableListOne.RowFilter = "intColumnNum = 1 and numFormId = " & objConfigWizard.FormID & "  and numAuthGroupId = " & objConfigWizard.AuthenticationGroupID 'set the row filter for selected fields in column 1
                    End If

                    dvAvailableListOne.Sort = "intRowNum asc"                           'sort order    
                    lstSelectedfldOne.DataSource = dvAvailableListOne                   'Set the datasource for the column one fields
                    lstSelectedfldOne.DataTextField = "vcNewFormFieldName"              'set the text field
                    lstSelectedfldOne.DataValueField = "numFormFieldId"                 'set the value attribut
                    lstSelectedfldOne.DataBind()                                        'Databind the fields

                    Dim iCount As Integer
                    For iCount = 0 To lstSelectedfldOne.Items.Count - 1
                        If Not lstAvailablefld.Items.FindByValue(lstSelectedfldOne.Items(iCount).Value) Is Nothing Then
                            lstAvailablefld.Items.Remove(lstSelectedfldOne.Items(iCount))
                        End If
                    Next

                    Dim dvAvailableListTwo As New DataView(dtAvailableFldList)          'create a dataview object
                    If GetFormValue() = 3 AndAlso Convert.ToString(ddlLeadFormList.SelectedValue) <> "0" Then
                        dvAvailableListTwo.RowFilter = "intColumnNum = 2 and numFormId = " & objConfigWizard.FormID & "  and numSubFormId = " & ddlLeadFormList.SelectedValue & " and numAuthGroupId = " & objConfigWizard.AuthenticationGroupID 'set the row filter for selected fields in column 2
                    Else
                        dvAvailableListTwo.RowFilter = "intColumnNum = 2 and numFormId = " & objConfigWizard.FormID & "  and numAuthGroupId = " & objConfigWizard.AuthenticationGroupID 'set the row filter for selected fields in column 2
                    End If


                    dvAvailableListTwo.Sort = "intRowNum asc"                           'sort order    
                    lstSelectedfldTwo.DataSource = dvAvailableListTwo                   'Set the datasource for the column three fields
                    lstSelectedfldTwo.DataTextField = "vcNewFormFieldName"              'set the text field
                    lstSelectedfldTwo.DataValueField = "numFormFieldId"                 'set the value attribut
                    lstSelectedfldTwo.DataBind()                                        'Databind the fields

                    For iCount = 0 To lstSelectedfldTwo.Items.Count - 1
                        If Not lstAvailablefld.Items.FindByValue(lstSelectedfldTwo.Items(iCount).Value) Is Nothing Then
                            lstAvailablefld.Items.Remove(lstSelectedfldTwo.Items(iCount))
                        End If
                    Next

                    Dim dvAvailableListThree As New DataView(dtAvailableFldList)        'create a dataview object
                    If GetFormValue() = 3 AndAlso Convert.ToString(ddlLeadFormList.SelectedValue) <> "0" Then
                        dvAvailableListThree.RowFilter = "intColumnNum = 3 and numFormId = " & objConfigWizard.FormID & " and numSubFormId = " & ddlLeadFormList.SelectedValue & " and numAuthGroupId = " & objConfigWizard.AuthenticationGroupID 'set the row filter for selected fields in column 3
                    Else
                        dvAvailableListThree.RowFilter = "intColumnNum = 3 and numFormId = " & objConfigWizard.FormID & " and numAuthGroupId = " & objConfigWizard.AuthenticationGroupID 'set the row filter for selected fields in column 3
                    End If

                    dvAvailableListThree.Sort = "intRowNum asc"                         'sort order    
                    lstSelectedfldThree.DataSource = dvAvailableListThree               'Set the datasource for the column three fields
                    lstSelectedfldThree.DataTextField = "vcNewFormFieldName"            'set the text field
                    lstSelectedfldThree.DataValueField = "numFormFieldId"               'set the value attribut
                    lstSelectedfldThree.DataBind()                                      'Databind the fields

                    For iCount = 0 To lstSelectedfldThree.Items.Count - 1
                        If Not lstAvailablefld.Items.FindByValue(lstSelectedfldThree.Items(iCount).Value) Is Nothing Then
                            lstAvailablefld.Items.Remove(lstSelectedfldThree.Items(iCount))
                        End If
                    Next

                    If cAOIField = "Y" Then                                                 'Check if AOI's are associated with a form or not
                        dsAvailableAOIList = objConfigWizard.getAOIList()                   'call function to get the AOI's
                        lstAvailablefldAOI.DataSource = dsAvailableAOIList.Tables(0)                  'Set the datasource for the AOI's
                        lstAvailablefldAOI.DataTextField = "vcAOIName"                'set the textfield
                        lstAvailablefldAOI.DataValueField = "numAOIID"                'set the value attribute
                        lstAvailablefldAOI.DataBind()                                       'Databind the AOI

                        lstSelectedfldAOI.DataSource = dsAvailableAOIList.Tables(1)                    'Set the datasource for the AOI's
                        lstSelectedfldAOI.DataTextField = "vcAOIName"                 'set the textfield
                        lstSelectedfldAOI.DataValueField = "numAOIID"                 'set the value attribute
                        lstSelectedfldAOI.DataBind()                                        'Databind the AOI
                        For iCount = 0 To lstSelectedfldAOI.Items.Count - 1
                            If Not lstAvailablefldAOI.Items.FindByValue(lstSelectedfldAOI.Items(iCount).Value) Is Nothing Then
                                lstAvailablefldAOI.Items.Remove(lstSelectedfldAOI.Items(iCount))
                            End If
                        Next
                    End If

                    '================================================================================
                    ' Purpose: Entities which are already used in the LeadBox Auto Rules cannot be deleted. So making a list of 
                    '           entities which cannot be deleted
                    '
                    ' History
                    ' Ver   Date        Ref     Author              Reason Created
                    ' 1     01/09/2006          Debasish Nag        Get referenced entities which cannot be deleted
                    '================================================================================
                    Dim sUsedFieldsInAutoRules As String = String.Empty                         'variable to store the fields whcih are referenced in Auto Rules
                    If objConfigWizard.FormID = 3 Then                                          'Have this only for LeadBox
                        sUsedFieldsInAutoRules = objConfigWizard.getAutoRuleReferencedFields()  'Get fields which are referenced in Auto Rules
                    End If
                    '================================================================================
                    ' Purpose: Calls the subroutine to create the client side script
                    '
                    ' History
                    ' Ver   Date        Ref     Author              Reason Created
                    ' 1     09/07/2005          Debasish Nag        Calls the function to create
                    '                                               the client side script
                    '================================================================================
                    Dim sClientSideString As New System.Text.StringBuilder                              'Declare a string builder
                    Dim iRowIndex As Int16 = 0                                                          'Declare an integer to hold the row index

                    sClientSideString.Append(vbCrLf & "<script language='javascript'>")                  'Start with the script block
                    sClientSideString.Append(vbCrLf & "var arrFieldConfig = new Array();" & vbCrLf)      'Create a array to hold the field information in javascript
                    sClientSideString.Append(createClientSideScript(dtAvailableFldList, iRowIndex))      'Call function to create a client side script
                    'If cAOIField = "Y" Then                                                              'Check if AOI's are associated with a form or not
                    '    sClientSideString.Append(createClientSideScript(dtAvailableAOIList, iRowIndex))  'Call function to create a client side script
                    'End If
                    sClientSideString.Append(vbCrLf & "var sUsedFieldsInAutoRules = '" & sUsedFieldsInAutoRules & "';" & vbCrLf) 'The list of fields which are referenced in Auto Rules and cannot be removed
                    sClientSideString.Append("</script>")                                                'end the client side javascript block
                    'litClientScript.Text &= vbCrLf & sClientSideString.ToString()

                    'Added by Neelam Kapila || 09/29/2017 - Added function to show BizDocId 
                    If ddlBizDocTemplate.SelectedValue IsNot Nothing And ddlBizDocTemplate.SelectedValue <> "" And ddlBizDocTemplate.SelectedValue <> "0" Then
                        divBizDoc.Style.Add("display", "block")
                        lblBizDocId.Text = ddlBizDocTemplate.SelectedValue
                    Else
                        divBizDoc.Style.Add("display", "none")
                    End If
                    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Fields", sClientSideString.ToString(), False)
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Function DisplaySurveyLists()
            Try
                Dim objSurvey As New SurveyAdministration                 'Declare and create a object of SurveyAdministration
                objSurvey.DomainID = Session("DomainID")                  'Set the Doamin Id
                Dim dtSurveylist As DataTable                             'Declare a datatable
                dtSurveylist = objSurvey.getSurveyList()                  'call function to get the list of available surveys
                Dim drSurveyList As DataRow                               'Declare a DataRow object

                ddlSurvey.Items.Clear()
                For Each drSurveyList In dtSurveylist.Rows                'Loop through the rows on the table containing the survey list
                    ddlSurvey.Items.Add(New ListItem(ReSetFromXML(drSurveyList.Item("vcSurName")), drSurveyList.Item("numSurID")))
                Next

                If ddlSurvey.Items.Count = 0 Then
                    'btnSaveConfiguration.Visible = False
                    lblSurveyMessage.Text = "You must first create a survey before generating the survey form."
                End If
                'ddlSurvey.Items.Insert(0, "--Select One--")
                'ddlSurvey.Items.FindByText("--Select One--").Value = "0"
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Function ReSetFromXML(ByVal sValue As String) As String
            Try
                Return Replace(Replace(Replace(Replace(sValue, "&amp;", "&"), "&#39;", "'"), "&lt;", "<"), "&gt;", ">")
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This method is used to databind the listboxes to the dataset
        ''' </summary>
        ''' <remarks>
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	09/07/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Sub PostInitializeControls()
            Try
                '================================================================================
                ' Purpose: Does post initialization settings of the controls
                '
                ' History
                ' Ver   Date        Ref     Author              Reason Created
                ' 1     09/07/2005          Debasish Nag        Initialization extra settings 
                '                                               of the controls
                '===============================================================================
                lstSelectedfldOne.Attributes.Add("onchange", "javascript: Column1SelectionChanged(); editLable(document.form1.lstSelectedfldOne,document.form1.txtNewNameFldOne);")     'calls for edition of the selected field
                lstSelectedfldTwo.Attributes.Add("onchange", "javascript: Column2SelectionChanged(); editLable(document.form1.lstSelectedfldTwo,document.form1.txtNewNameFldTwo);")     'calls for edition of the selected field
                lstSelectedfldThree.Attributes.Add("onchange", "javascript: Column3SelectionChanged(); editLable(document.form1.lstSelectedfldThree,document.form1.txtNewNameFldThree);")     'calls for edition of the selected field
                'btnLeadBoxForm.Attributes.Add("onclick", "return OpenLeadbox('../admin2/frmCreateLeadBoxURL.aspx?R=1');")
                'btnRegistrationForm.Attributes.Add("onclick", "return OpenForm('" & ConfigurationManager.AppSettings("RegistrationForm") & "?D=" & Session("DomainID") & "');")

                If ddlSurvey.Items.Count > 0 Then
                    If ddlSurvey.SelectedValue > 0 Then
                        btnSuveryForm.Attributes.Add("onclick", "return OpenForm('" & ConfigurationManager.AppSettings("SurveyScreen") & "?D=" & Session("DomainID") & "&numSurId=" & ddlSurvey.SelectedValue & "');")
                    Else
                        btnSuveryForm.Attributes.Add("onclick", "return OpenForm('" & ConfigurationManager.AppSettings("SurveyScreen") & "?D=" & Session("DomainID") & "');")
                    End If
                End If

                hplBizDoc.Attributes.Add("onclick", "return OpenAtch()")
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This method is used to handle saving of the configuration after the 
        '''     "Save Configuration" button is clicked
        ''' </summary>
        ''' <param name="sender">Represents the sender object.</param>
        ''' <param name="e">Represents the EventArgs.</param>
        ''' <remarks>
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	09/07/2005	Created
        ''' </history>
        '''  -----------------------------------------------------------------------------

        Private Sub btnSaveConfiguration_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSaveConfiguration.Click
            Try
                If ddlBizFormModule.SelectedValue = "2" AndAlso Not ddlFormList.SelectedItem Is Nothing Then
                    Dim listItemGroup As New System.Collections.Generic.List(Of Long)
                    If ddlUserGroup.SelectedValue <> "0" AndAlso CCommon.ToLong(ddlUserGroup.SelectedValue) > 0 Then
                        listItemGroup.Add(ddlUserGroup.SelectedValue)
                    End If

                    For Each item As Telerik.Web.UI.RadComboBoxItem In rcbGroups.CheckedItems
                        listItemGroup.Add(item.Value)
                    Next

                    For Each groupID As Long In listItemGroup
                        If chkGlobalSetting.Checked Then
                            Dim tintFormType As String = ddlFormList.SelectedValue.Split("~")(1)

                            For Each item As ListItem In ddlFormList.Items
                                If item.Value.Contains("~") AndAlso item.Value.Split("~")(1) = tintFormType Then
                                    For relIndex As Integer = 0 To ddlRelationShip.Items.Count - 1
                                        SetConfiguration(GetFormValue(item.Value), groupID, ddlRelationShip.Items.Item(relIndex).Value, ddlContactType.SelectedValue, item.Value)
                                    Next
                                End If
                            Next
                        Else
                            SetConfiguration(GetFormValue(), groupID, ddlRelationShip.SelectedValue, ddlContactType.SelectedValue, ddlFormList.SelectedValue)
                        End If
                    Next
                ElseIf GetFormValue() = "1" Or GetFormValue() = "15" Or GetFormValue() = "17" Or GetFormValue() = "18" _
                    Or GetFormValue() = "29" Or GetFormValue() = "59" _
                    Or GetFormValue() = "103" Or GetFormValue() = "104" _
                     Or GetFormValue() = "115" Or GetFormValue() = "116" Or GetFormValue() = "117" _
                     Or GetFormValue() = "26" Or GetFormValue() = "129" Or GetFormValue() = "130" Then
                    Dim listItemGroup As New System.Collections.Generic.List(Of Long)
                    If ddlUserGroup.SelectedValue <> "0" AndAlso CCommon.ToLong(ddlUserGroup.SelectedValue) > 0 Then
                        listItemGroup.Add(ddlUserGroup.SelectedValue)
                    End If

                    For Each item As Telerik.Web.UI.RadComboBoxItem In rcbGroups.CheckedItems
                        listItemGroup.Add(item.Value)
                    Next

                    For Each groupID As Long In listItemGroup
                        SetConfiguration(GetFormValue(), groupID, ddlRelationShip.SelectedValue, ddlContactType.SelectedValue, ddlFormList.SelectedValue)
                    Next
                ElseIf GetFormValue() = "105" Or GetFormValue() = "106" Or GetFormValue() = "107" Or GetFormValue() = "108" Then
                    Dim listItemGroup As New System.Collections.Generic.List(Of Long)
                    If ddlUserGroup.SelectedValue <> "0" AndAlso CCommon.ToLong(ddlUserGroup.SelectedValue) > 0 Then
                        listItemGroup.Add(ddlUserGroup.SelectedValue)
                    End If

                    For Each item As Telerik.Web.UI.RadComboBoxItem In rcbGroups.CheckedItems
                        listItemGroup.Add(item.Value)
                    Next

                    For Each groupID As Long In listItemGroup
                        If chkGlobalSetting.Checked Then
                            SetConfiguration(105, groupID, ddlRelationShip.SelectedValue, ddlContactType.SelectedValue, "105")
                            SetConfiguration(106, groupID, ddlRelationShip.SelectedValue, ddlContactType.SelectedValue, "106")
                            SetConfiguration(107, groupID, ddlRelationShip.SelectedValue, ddlContactType.SelectedValue, "107")
                            SetConfiguration(108, groupID, ddlRelationShip.SelectedValue, ddlContactType.SelectedValue, "108")
                        Else
                            SetConfiguration(GetFormValue(), groupID, ddlRelationShip.SelectedValue, ddlContactType.SelectedValue, ddlFormList.SelectedValue)
                        End If
                    Next
                ElseIf GetFormValue() = "118" Or GetFormValue() = "119" Or GetFormValue() = "120" Or GetFormValue() = "121" Then
                    Dim listItemGroup As New System.Collections.Generic.List(Of Long)
                    If ddlUserGroup.SelectedValue <> "0" AndAlso CCommon.ToLong(ddlUserGroup.SelectedValue) > 0 Then
                        listItemGroup.Add(ddlUserGroup.SelectedValue)
                    End If

                    For Each item As Telerik.Web.UI.RadComboBoxItem In rcbGroups.CheckedItems
                        listItemGroup.Add(item.Value)
                    Next

                    For Each groupID As Long In listItemGroup
                        If chkGlobalSetting.Checked Then
                            SetConfiguration(118, groupID, ddlRelationShip.SelectedValue, ddlContactType.SelectedValue, "118")
                            SetConfiguration(119, groupID, ddlRelationShip.SelectedValue, ddlContactType.SelectedValue, "119")
                            SetConfiguration(120, groupID, ddlRelationShip.SelectedValue, ddlContactType.SelectedValue, "120")
                            SetConfiguration(121, groupID, ddlRelationShip.SelectedValue, ddlContactType.SelectedValue, "121")
                        Else
                            SetConfiguration(GetFormValue(), groupID, ddlRelationShip.SelectedValue, ddlContactType.SelectedValue, ddlFormList.SelectedValue)
                        End If
                    Next
                ElseIf GetFormValue() = "102" Or GetFormValue() = "114" Then 'contact
                    Dim listItemGroup As New System.Collections.Generic.List(Of Long)
                    If ddlUserGroup.SelectedValue <> "0" AndAlso CCommon.ToLong(ddlUserGroup.SelectedValue) > 0 Then
                        listItemGroup.Add(ddlUserGroup.SelectedValue)
                    End If

                    For Each item As Telerik.Web.UI.RadComboBoxItem In rcbGroups.CheckedItems
                        listItemGroup.Add(item.Value)
                    Next

                    For Each groupID As Long In listItemGroup
                        If chkGlobalSetting.Checked Then
                            For contactTypeIndex As Integer = 0 To ddlContactType.Items.Count - 1
                                SetConfiguration(GetFormValue(), groupID, ddlRelationShip.SelectedValue, ddlContactType.Items.Item(contactTypeIndex).Value, ddlFormList.SelectedValue)
                            Next
                        Else
                            SetConfiguration(GetFormValue(), groupID, ddlRelationShip.SelectedValue, ddlContactType.SelectedValue, ddlFormList.SelectedValue)
                        End If
                    Next
                Else
                    SetConfiguration(GetFormValue(), ddlUserGroup.SelectedValue, ddlRelationShip.SelectedValue, ddlContactType.SelectedValue, ddlFormList.SelectedValue)
                End If

                rcbGroups.Items.Clear()
                Dim radItem As Telerik.Web.UI.RadComboBoxItem
                For Each item As ListItem In ddlUserGroup.Items
                    If item.Value <> "0" AndAlso item.Value <> ddlUserGroup.SelectedValue Then
                        radItem = New Telerik.Web.UI.RadComboBoxItem
                        radItem.Text = item.Text
                        radItem.Value = item.Value
                        rcbGroups.Items.Add(radItem)
                    End If
                Next

                chkGlobalSetting.Checked = False
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try

        End Sub
        Sub SetConfiguration(ByVal formID As Long, ByVal userGroupID As Int16, ByVal relId As String, ByVal contactTypeID As String, ByVal formIDString As String)
            Try
                Dim objConfigWizard As New FormConfigWizard                         'Create an object of form config wizard
                Dim sXMLString As String                                            'Declare a string to contain the xml
                sXMLString = hdXMLString.Value.ToString()                           'Get the xml string
                Try
                    If ddlBizFormModule.SelectedValue = "2" AndAlso formIDString.StartsWith("1~") Then 'custom replationships
                        If formIDString.StartsWith("1~3~") Then
                            SaveMasterConfiguration(sXMLString, 36, "", 3, 1, formID, False, userGroupID)
                        Else
                            SaveMasterConfiguration(sXMLString, 36, "", 1, 1, formID, True, userGroupID)
                        End If
                    ElseIf formID = "99" Then 'Lead Details 
                        SaveMasterConfiguration(sXMLString, 34, "L", 3, 1, CCommon.ToLong(relId), 0, userGroupID)
                    ElseIf formID = "100" Then 'Prospect Details
                        SaveMasterConfiguration(sXMLString, 35, "P", 3, 1, CCommon.ToLong(relId), 0, userGroupID)
                    ElseIf formID = "101" Then 'Account Details
                        SaveMasterConfiguration(sXMLString, 36, "P", 3, 1, CCommon.ToLong(relId), 0, userGroupID)
                    ElseIf formID = "146" Then 'Vendor Details
                        SaveMasterConfiguration(sXMLString, 36, "P", 3, 1, 47, 0, userGroupID)
                    ElseIf formID = "147" Then 'Employer Details
                        SaveMasterConfiguration(sXMLString, 36, "P", 3, 1, 93, 0, userGroupID)
                    ElseIf formID = "102" Then 'Contact Details  ' Changed Here
                        SaveMasterConfiguration(sXMLString, 10, "c", 3, 4, CCommon.ToLong(contactTypeID), 0, userGroupID)
                    ElseIf formID = "103" Then 'Project Details 
                        SaveMasterConfiguration(sXMLString, 13, "R", 3, 11, 0, 0, userGroupID)
                    ElseIf formID = "104" Then 'Case Details  
                        SaveMasterConfiguration(sXMLString, 12, "S", 3, 3, 0, 0, userGroupID)
                    ElseIf formID = "105" Then 'Sales Opportunity Details  
                        SaveMasterConfiguration(sXMLString, 38, "O", 3, 2, 0, 0, userGroupID)
                    ElseIf formID = "106" Then 'Sales Order Details 
                        SaveMasterConfiguration(sXMLString, 39, "O", 3, 2, 0, 0, userGroupID)
                    ElseIf formID = "107" Then 'Purchase Opportunity Details 
                        SaveMasterConfiguration(sXMLString, 40, "u", 3, 6, 0, 0, userGroupID)
                    ElseIf formID = "108" Then 'Purchase Order Details 
                        SaveMasterConfiguration(sXMLString, 41, "u", 3, 6, 0, 0, userGroupID)
                    ElseIf formID = "109" Then 'Lead Grid Columns
                        SaveMasterConfiguration(sXMLString, 34, "L", 1, 1, 1, 1, userGroupID)
                    ElseIf formID = "110" Then 'Prospect Grid Columns
                        SaveMasterConfiguration(sXMLString, 35, "P", 1, 1, 3, 1, userGroupID)
                    ElseIf formID = "111" Then 'Account Grid Columns
                        SaveMasterConfiguration(sXMLString, 36, "P", 1, 1, 2, 1, userGroupID)
                    ElseIf formID = "112" Then 'Vendor Grid Columns
                        SaveMasterConfiguration(sXMLString, 36, "P", 1, 1, 47, 1, userGroupID)
                    ElseIf formID = "113" Then 'Employer Grid Columns
                        SaveMasterConfiguration(sXMLString, 36, "P", 1, 1, 93, 1, userGroupID)
                    ElseIf formID = "114" Then 'Contact Grid Columns
                        SaveMasterConfiguration(sXMLString, 10, "c", 1, 4, CCommon.ToLong(contactTypeID), 1, userGroupID)
                    ElseIf formID = "115" Then 'Project Grid Columns
                        SaveMasterConfiguration(sXMLString, 13, "R", 1, 11, 0, 1, userGroupID)
                    ElseIf formID = "116" Then 'Case Grid Columns
                        SaveMasterConfiguration(sXMLString, 12, "S", 1, 3, 0, 1, userGroupID)
                    ElseIf formID = "117" Then 'Tickler Grid Columns
                        SaveMasterConfiguration(sXMLString, 43, "", 1, 0, 0, 1, userGroupID)
                    ElseIf formID = "118" Then 'Sales Opportunity Grid Columns
                        SaveMasterConfiguration(sXMLString, 38, "", 1, 2, 38, 1, userGroupID)
                    ElseIf formID = "119" Then 'Sales Order Grid Columns
                        SaveMasterConfiguration(sXMLString, 39, "", 1, 2, 39, 1, userGroupID)
                    ElseIf formID = "120" Then 'Purchase Opportunity Grid Columns
                        SaveMasterConfiguration(sXMLString, 40, "", 1, 6, 40, 1, userGroupID)
                    ElseIf formID = "121" Then 'Purchase Order Grid Configuration
                        SaveMasterConfiguration(sXMLString, 41, "", 1, 6, 41, 1, userGroupID)
                    ElseIf formID = "26" Then 'Products & services Setting (Sales)
                        SaveMasterConfiguration(sXMLString, 26, "", 1, 5, 0, 1, userGroupID)
                    ElseIf formID = "129" Then 'Products & services Setting (Purchase)
                        SaveMasterConfiguration(sXMLString, 129, "", 1, 5, 0, 1, userGroupID)
                    ElseIf formID = "130" Then 'Item Grid Columns
                        SaveMasterConfiguration(sXMLString, 21, "", 1, 5, 0, 1, userGroupID)
                    ElseIf formID = "131" Then 'Activity Form Coniguration
                        SetActivityFormDetails()
                    Else

                        objConfigWizard.FormID = formID                  'set the form id
                        objConfigWizard.DomainID = Session("DomainID")                      'Set the domain id
                        If formID = 1 Or formID = 2 Or formID = 6 Or formID = 15 Or formID = 17 Or formID = 18 Or formID = 29 Or formID = 59 Then
                            objConfigWizard.AuthenticationGroupID = userGroupID ' Changed Here
                            objConfigWizard.BizDocTemplateID = 0
                        ElseIf formID = 7 Or formID = 8 Then
                            objConfigWizard.AuthenticationGroupID = ddlBizDoc.SelectedItem.Value
                            If Not ddlBizDocTemplate.SelectedItem Is Nothing Then
                                objConfigWizard.BizDocTemplateID = ddlBizDocTemplate.SelectedItem.Value
                            End If
                        ElseIf formID = 56 Then
                            objConfigWizard.AuthenticationGroupID = userGroupID 'Changed Here
                            objConfigWizard.BizDocTemplateID = relId 'Changed Here
                        ElseIf formID = 5 Then
                            Dim objSurvey As New SurveyAdministration                           'Create an object of Survey Administration Class
                            objSurvey.DomainID = Session("DomainID")                            'Set the Domain ID
                            objSurvey.SurveyId = ddlSurvey.SelectedValue                               'Set the Survey ID
                            Dim dtSurveyMasterInfo As DataTable                             'Declare a DataTable
                            dtSurveyMasterInfo = objSurvey.getSurveyMasterData()            'Get the Survey Master information in a datatable

                            Dim boolRelationshipProfile As Boolean = dtSurveyMasterInfo.Rows(0).Item("bitRelationshipProfile")
                            Dim boolWorkFlowRule As Boolean = False

                            objSurvey.SurveyInfo = objSurvey.getSurveyInformation4Execution
                            objSurvey.SurveyInfo.Tables(0).TableName = "SurveyMaster"           'Set the name of the table of Survey Master Infor
                            objSurvey.SurveyInfo.Tables(1).TableName = "SurveyQuestionMaster"   'Set the name of the table of Questions
                            objSurvey.SurveyInfo.Tables(2).TableName = "SurveyAnsMaster"        'Set the name of the table of Answers
                            objSurvey.SurveyInfo.Tables(3).TableName = "SurveyWorkflowRules"    'Set the name of the table of Work Flow Rules
                            objSurvey.SurveyInfo.Tables(4).TableName = "SurveyMatrixMaster"    'Set the name of the table of Work Flow Rules

                            Dim dtAnswerRules As DataTable
                            Dim drAnswerRulesRow As DataRow
                            Dim dtSurveyWorkflowRules As DataTable
                            dtSurveyWorkflowRules = objSurvey.SurveyInfo.Tables(3)

                            'For Each drAnswerRulesRow In dtSurveyWorkflowRules.Rows
                            '    If (drAnswerRulesRow.Item("numRuleId") = 3) Then
                            '        boolWorkFlowRule = True
                            '        Exit For
                            '    End If
                            'Next

                            'If boolWorkFlowRule Or boolRelationshipProfile Then
                            If boolRelationshipProfile Then
                                If Not (sXMLString.ToLower.Contains("firstname") AndAlso sXMLString.ToLower.Contains("lastname")) Then
                                    litClientAlertMessageScript.Text = "<script language='javascript'>alert('First Name and Last Name fields are required to create a new record. Please add them from the BizForms Wizard and try again.');</script>"
                                    DataBindElements()
                                    Exit Sub
                                End If
                            End If

                            objConfigWizard.AuthenticationGroupID = ddlSurvey.SelectedValue
                        Else
                            objConfigWizard.AuthenticationGroupID = 0
                        End If
                        If formID = 7 Or formID = 8 Then
                            objConfigWizard.BizDocID = ddlBizDoc.SelectedValue
                            objConfigWizard.strFormFieldID = hdBizDocSumm.Value
                            objConfigWizard.BizDocTemplateID = CCommon.ToLong(ddlBizDocTemplate.SelectedValue)

                            lstSummaryAddedFlds.DataSource = objConfigWizard.ManageBizDocSumm
                            lstSummaryAddedFlds.DataTextField = "vcFormFieldName"
                            lstSummaryAddedFlds.DataValueField = "numFormFieldId"
                            lstSummaryAddedFlds.DataBind()
                        End If

                        If formID = 30 Then
                            Dim ErrorMesageToLog As String = String.Empty
                            Dim strErrorMessage As String = SearchLucene.ReIndexAllWebsiteOfDomain(CCommon.ToLong(Session("DomainID")), 0, ErrorMesageToLog)
                            If ErrorMesageToLog.Length > 0 Then
                                ExceptionModule.ExceptionPublish(New Exception(ErrorMesageToLog), Session("DomainID"), Session("UserContactID"), Request)
                            End If
                            If strErrorMessage.Length > 0 Then
                                litClientAlertMessageScript.Text = "<script language='javascript'> alert('" & HttpUtility.JavaScriptStringEncode(strErrorMessage) & "');</script>"
                                Exit Sub
                            End If

                        End If

                        If formID = 3 Then
                            objConfigWizard.numSubFormId = ddlLeadFormList.SelectedValue
                            objConfigWizard.numAssignTo = ddlAssignToForm.SelectedValue
                            objConfigWizard.numDripCampaign = ddlDripCampaign.SelectedValue
                            objConfigWizard.bitDripCampaign = chkDripCampaign.Checked
                            objConfigWizard.bitByPassRoutingRules = chkAssignToFrom.Checked
                        End If

                        sXMLFilePath = hdXMLPath.Value                                      'set the file path

                        If formID = 7 Or formID = 8 Or formID = 1 Or formID = 15 Or formID = 17 Or formID = 18 Or formID = 29 Or formID = 56 Or formID = 59 _
                           Or formID = 67 Then
                            objConfigWizard.SaveDatabaseConfig(sXMLString)                    'call sub to save config to database
                        Else
                            objConfigWizard.SaveXMLConfig(sXMLString, sXMLFilePath)             'call sub to save the xml
                            objConfigWizard.SaveDatabaseConfig(sXMLString)                    'call sub to save config to database
                        End If

                        If tblBzFormWizardConfigAOI.Visible = True Then
                            Dim dsNew As New DataSet
                            Dim dtTable As New DataTable
                            dtTable.Columns.Add("numAOIID")
                            Dim i As Integer
                            Dim dr As DataRow
                            Dim str As String()
                            str = hdnAOI.Value.Split(",")
                            For i = 0 To str.Length - 2
                                dr = dtTable.NewRow
                                dr("numAOIID") = str(i)
                                dtTable.Rows.Add(dr)
                            Next

                            dtTable.TableName = "Table"
                            dsNew.Tables.Add(dtTable.Copy)
                            objConfigWizard.strAOI = dsNew.GetXml
                            dsNew.Tables.Remove(dsNew.Tables(0))
                            objConfigWizard.ManageDynamicAOI()
                        End If

                        Dim sObjFormAdditionalParamValue As String                          'declare a variable to store the form additonal param

                        sObjFormAdditionalParamValue = hdFormAdditionalParam.Value          'retrieve the text from the dynamic form additional parameter
                        objConfigWizard.SaveFormConfigParams(sObjFormAdditionalParamValue, IIf(ddlGroup.Enabled, IIf(ddlGroup.SelectedIndex = -1, 0, ddlGroup.SelectedValue), 0)) 'call sub to save the form header parameter
                    End If


                Catch ex1 As Exception
                    ExceptionModule.ExceptionPublish(ex1, Session("DomainID"), Session("UserContactID"), Request)
                    'Hide error and also retain the value of "hdSaveConfigurationStatus"
                    litClientAlertMessageScript.Text = "<script language='javascript'>alert('A Error Occurred while processing your request. Please try again else report to Administrator.');</script>"
                Finally
                    DataBindElements()                                                  'Calls the subroutine to databind the listboxes to dataset
                    hdSaveConfigurationStatus.Value = "No"                              'reset the save indicator

                End Try
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This subroutine creates a client side script of dynamic field information
        '''     and then posts the script to the client side.This is just creating array
        '''     and there is no javascript functionality embedded in the code behind
        ''' </summary>
        ''' <param name="dtTable">Represents the datatable object.</param>
        ''' <param name="iRowIndex">Represents the array index.</param>
        ''' <remarks>
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	09/07/2005	Created
        ''' </history>
        '''  -----------------------------------------------------------------------------
        Function createClientSideScript(ByVal dtTable As DataTable, ByRef iRowIndex As Int16) As String
            Try
                Dim sDynmicFieldInfo As New System.Text.StringBuilder                   'Create a Stringbuilder object
                Dim drDataRow As DataRow                                                'Declare a datarow to scrollthrough the rows in the datatable
                Dim boollookbacktable As Boolean = False
                If dtTable.Columns.Contains("vcLookBackTableName") Then
                    boollookbacktable = True
                End If
                For Each drDataRow In dtTable.Rows                                      'Start scrolling through the table rows
                    If boollookbacktable Then
                        sDynmicFieldInfo.Append("arrFieldConfig[" & iRowIndex & "] = new classFormFieldConfig(" & GetFormValue() & ",'" & CCommon.ToString(drDataRow.Item("numFormFieldId")) & "','" & Replace(CCommon.ToString(drDataRow.Item("vcFormFieldName")), "'", "\'") & "','" & Replace(CCommon.ToString(drDataRow.Item("vcNewFormFieldName")), "'", "\'") & "','" & Replace(CCommon.ToString(drDataRow.Item("vcDbColumnName")), "'", "\'") & "','" & CCommon.ToString(drDataRow.Item("vcListItemType")) & "','" & CCommon.ToString(drDataRow.Item("vcAssociatedControlType")) & "','" & CCommon.ToString(drDataRow.Item("vcFieldType")) & "','" & CCommon.ToString(drDataRow.Item("vcFieldDataType")) & "'," & CCommon.ToString(drDataRow.Item("intColumnNum")) & "," & CCommon.ToString(drDataRow.Item("intRowNum")) & ",'" & If(CCommon.ToString(drDataRow.Item("boolRequired")) = "False", "0", If(CCommon.ToString(drDataRow.Item("boolRequired")) = "True", "1", CCommon.ToString(drDataRow.Item("boolRequired")))) & "'," & CCommon.ToString(drDataRow.Item("numListID")) & ",'" & If(CCommon.ToString(drDataRow.Item("boolAOIField")) = "False", "0", If(CCommon.ToString(drDataRow.Item("boolAOIField")) = "True", "1", CCommon.ToString(drDataRow.Item("boolAOIField")))) & "','" & CCommon.ToString(drDataRow.Item("bitDefaultMandatory")) & "','" & CCommon.ToString(drDataRow.Item("vcLookBackTableName")) & "');" & vbCrLf & "") 'start creating the array elements, each holding field information
                    Else : sDynmicFieldInfo.Append("arrFieldConfig[" & iRowIndex & "] = new classFormFieldConfig(" & GetFormValue() & ",'" & CCommon.ToString(drDataRow.Item("numFormFieldId")) & "','" & Replace(CCommon.ToString(drDataRow.Item("vcFormFieldName")), "'", "\'") & "','" & Replace(CCommon.ToString(drDataRow.Item("vcNewFormFieldName")), "'", "\'") & "','" & Replace(CCommon.ToString(drDataRow.Item("vcDbColumnName")), "'", "\'") & "','" & CCommon.ToString(drDataRow.Item("vcListItemType")) & "','" & CCommon.ToString(drDataRow.Item("vcAssociatedControlType")) & "','" & CCommon.ToString(drDataRow.Item("vcFieldType")) & "','" & CCommon.ToString(drDataRow.Item("vcFieldDataType")) & "'," & CCommon.ToString(drDataRow.Item("intColumnNum")) & "," & CCommon.ToString(drDataRow.Item("intRowNum")) & ",'" & If(CCommon.ToString(drDataRow.Item("boolRequired")) = "False", "0", If(CCommon.ToString(drDataRow.Item("boolRequired")) = "True", "1", CCommon.ToString(drDataRow.Item("boolRequired")))) & "'," & CCommon.ToString(drDataRow.Item("numListID")) & ",'" & If(CCommon.ToString(drDataRow.Item("boolAOIField")) = "False", "0", If(CCommon.ToString(drDataRow.Item("boolAOIField")) = "True", "1", CCommon.ToString(drDataRow.Item("boolAOIField")))) & "','" & CCommon.ToString(drDataRow.Item("bitDefaultMandatory")) & "','');" & vbCrLf & "") 'start creating the array elements, each holding field information
                    End If
                    iRowIndex += 1                                                      'increment the row counter
                Next
                Return sDynmicFieldInfo.ToString()                                      'return the client script
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This subroutine HTML Decodes the row elements
        ''' </summary>
        ''' <param name="dtTable">Represents the datatable object.</param>
        ''' <remarks>
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	02/27/2006	Created
        ''' </history>
        '''  -----------------------------------------------------------------------------
        Function HTMLDecodeFieldList(ByVal dtTable As DataTable) As DataTable
            Try
                Dim objrow As DataRow
                For Each objrow In dtTable.Rows                                                       'loop through the rows in parent table
                    objrow.Item("vcNewFormFieldName") = Server.HtmlDecode(objrow.Item("vcNewFormFieldName").ToString()) 'add new name to the column item
                Next
                Return dtTable
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This subroutine creates dynamic field for the form header
        ''' </summary>
        ''' <param name="iFormId">Represents the Form Id.</param>
        ''' <param name="sControlValue">Represents the control value for the form.</param>
        ''' <remarks>
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	09/07/2005	Created
        ''' </history>
        '''  -----------------------------------------------------------------------------
        Function createDynamicHeaderControls(ByVal iFormId As Int16, ByVal sControlValue As String)
            Try
                Dim objConfigWizard As New FormConfigWizard                                 'Create an object of form config wizard
                If iFormId = 3 Then                                                         'leadbox form
                    'litDynamicFormHeaderText.Text = "On Submit redirect user to page:"      'message on literal control for leadbox
                    'Dim objAdditionalParamControl As New TextBox                            'declare a text box
                    'objAdditionalParamControl.CssClass = "signup"                           'set the class attribute
                    'objAdditionalParamControl.ID = "txtobjAdditionalParam"                  'set the name of the control
                    'objAdditionalParamControl.MaxLength = 100                               'set the max nos of character that can be entered as 100
                    'objAdditionalParamControl.Width = Unit.Pixel(200)
                    'objAdditionalParamControl.Text = sControlValue                          'set the control value
                    'plhDynamicFormHeaders.Controls.Add(objAdditionalParamControl)           'add the text control
                    tdUserGroups.Visible = False
                    plhDynamicFormHeaders.Visible = True
                    divplhDynamicFormHeaders.Visible = True
                ElseIf iFormId = 1 Or iFormId = 2 Or iFormId = 6 Or iFormId = 15 Or iFormId = 17 Or iFormId = 18 Or iFormId = 29 Or iFormId = 56 Or iFormId = 59 Then                                                     'Advance Search Form
                    tdUserGroups.Visible = True
                    plhDynamicFormHeaders.Visible = False
                    divplhDynamicFormHeaders.Visible = False
                ElseIf iFormId = 4 Then                                                                        'order form form
                    litDynamicFormHeaderText.Text = "If customers are created by self ordering, set their account details relationship to:"      'message on literal control for order form
                    objConfigWizard.DomainID = Session("DomainID")                          'Set value of domain id
                    Dim objAdditionalParamControl As New DropDownList                       'declare a dropdown list
                    objAdditionalParamControl.CssClass = "signup"                           'set the class attribute
                    objAdditionalParamControl.ID = "ddlobjAdditionalParam"                  'set the name of the control

                    Dim dtRelations As DataTable                                            'declare a datatable
                    dtRelations = objConfigWizard.getListDetails(5) 'call to get the relationships
                    Dim dRow As DataRow                                                     'declare a datarow
                    dRow = dtRelations.NewRow()                                             'get a new row object
                    dtRelations.Rows.InsertAt(dRow, 0)                                      'insert the row
                    dtRelations.Rows(0).Item("numItemID") = 0                               'set the value for first entry
                    dtRelations.Rows(0).Item("vcItemName") = "---Select One---"             'set the text for the first entey
                    dtRelations.Rows(0).Item("vcItemType") = "L"                            'type= list
                    dtRelations.Rows(0).Item("flagConst") = 0                               'requried by the stored procedure
                    objAdditionalParamControl.DataSource = dtRelations.DefaultView          'set the datasource
                    objAdditionalParamControl.DataTextField = "vcItemName"                  'set the text attribute
                    objAdditionalParamControl.DataValueField = "numItemID"                  'set the value attribute
                    objAdditionalParamControl.DataBind()                                    'databind the drop down

                    If Not objAdditionalParamControl.Items.FindByValue(sControlValue) Is Nothing Then
                        objAdditionalParamControl.Items.FindByValue(sControlValue).Selected = True 'show the appropriate item as selected
                    End If
                    plhDynamicFormHeaders.Controls.Add(objAdditionalParamControl)           'add the text control
                    tdUserGroups.Visible = False                        'Nothing to be displayed

                    plhDynamicFormHeaders.Visible = True
                    divplhDynamicFormHeaders.Visible = True
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This subroutine creates dynamic groups for the form 
        ''' </summary>
        ''' <param name="iFormId">Represents the Form Id.</param>
        ''' <param name="numGrpId">Represents the Group Id for the form.</param>
        ''' <remarks>
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	18/07/2005	Created
        ''' </history>
        '''  -----------------------------------------------------------------------------
        Function createDynamicFormGroups(ByVal iFormId As Int16, ByVal numGrpId As Integer)
            Try
                If iFormId = 3 Then                                                                 'Only LeadBox to have Groups Drop down; Change Request Carl on 8th Jan 2006
                    ddlGroup.Visible = True                                                         'do not allow selection
                    divGroup.Visible = True
                    lblGroupDdlLable.Visible = True                                                 'display the lable at all
                Else
                    ddlGroup.Visible = False                                                        'do not allow selection
                    divGroup.Visible = False
                    lblGroupDdlLable.Visible = False                                                'do not display the lable at all
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Private Sub ddlBizDoc_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlBizDoc.SelectedIndexChanged
            Try
                BindBizDocsTemplate()

                If ddlBizDocTemplate.Items.Count = 0 Then
                    lblError.Text = "First create BizDoc Template for selected BizDoc."
                Else
                    lblError.Text = ""
                End If
                DataBindElements()
                LoadSummary()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Sub ddlBizDocTemplate_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlBizDocTemplate.SelectedIndexChanged
            Try
                DataBindElements()
                LoadSummary()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Sub BindBizDocs()
            Try
                objCommon = New CCommon
                objCommon.DomainID = Session("DomainID")
                objCommon.BizDocType = IIf(GetFormValue() = 7, 1, 2)


                Dim dtBizDoc As DataTable = objCommon.GetBizDocType
                For Each dr As DataRow In dtBizDoc.Rows
                    If dr("numListItemID") = IIf(GetFormValue() = 7, Session("AuthoritativeSalesBizDoc"), Session("AuthoritativePurchaseBizDoc")) Then
                        dr("vcData") = dr("vcData") & " - Authoritative"
                    End If
                Next
                ddlBizDoc.DataSource = dtBizDoc
                ddlBizDoc.DataTextField = "vcData"
                ddlBizDoc.DataValueField = "numListItemID"
                ddlBizDoc.DataBind()
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub BindDripCampaignList()
            Dim objCampaign As New Campaign
            objCampaign.DomainID = Session("DomainID")
            Dim dt As DataTable
            dt = objCampaign.GetECampaigns()
            ddlDripCampaign.DataSource = dt
            ddlDripCampaign.DataTextField = "vcECampName"
            ddlDripCampaign.DataValueField = "numECampaignID"
            ddlDripCampaign.DataBind()
            ddlDripCampaign.Items.Insert(0, New ListItem("-Select-", "0"))
        End Sub
        Sub BindDripAssigntoEmpList()
            Dim objCommon As New CCommon
            Dim dt As DataTable
            dt = objCommon.ConEmpList(Session("DomainID"), 0, 0)
            ddlAssignToForm.DataSource = dt
            ddlAssignToForm.DataTextField = "vcUserName"
            ddlAssignToForm.DataValueField = "numContactID"
            ddlAssignToForm.DataBind()
            ddlAssignToForm.Items.Insert(0, New ListItem("-Select-", "0"))
        End Sub
        Sub BindBizDocsTemplate()
            Try
                Dim objOppBizDoc As New OppBizDocs
                objOppBizDoc.DomainID = Session("DomainID")
                objOppBizDoc.BizDocId = ddlBizDoc.SelectedValue
                objOppBizDoc.OppType = IIf(GetFormValue() = 7, 1, 2)

                Dim dtBizDocTemplate As DataTable = objOppBizDoc.GetBizDocTemplateList()

                ddlBizDocTemplate.DataSource = dtBizDocTemplate
                ddlBizDocTemplate.DataTextField = "vcTemplateName"
                ddlBizDocTemplate.DataValueField = "numBizDocTempID"
                ddlBizDocTemplate.DataBind()
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        Dim FormEvnt As Int16 = 0
        Private Sub ddlFormList_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlFormList.SelectedIndexChanged
            Try
                chkGlobalSetting.Checked = False
                chkGlobalSetting.Visible = False
                If (CCommon.ToLong(GetFormValue()) = 3 AndAlso FormEvnt = 0) Then
                    BindSubFormList()
                    BindSubFormDetails()
                    BindDripCampaignList()
                    BindDripAssigntoEmpList()
                    FormEvnt = 0
                End If
                DataBindElements()
                If Not ddlGroup.Items.FindByValue(numGrpId) Is Nothing Then ddlGroup.Items.FindByValue(numGrpId).Selected = True
                ActivityForm.Visible = False
                ActivtyFormGCalendar.Visible = False
                MainRow.Visible = True
                tdState.Visible = True
                btnSuveryForm.Visible = True
                divFormFieldGroup.Visible = False
                tblBzFormWizardConfigAOIHeader.Visible = True
                If ddlBizFormModule.SelectedValue = "2" AndAlso Not ddlFormList.SelectedItem Is Nothing AndAlso ddlFormList.SelectedValue.StartsWith("1~") Then 'custom replationships
                    If Not ddlFormList.SelectedItem Is Nothing AndAlso ddlFormList.SelectedItem.Value.StartsWith("1~3~") Then
                        If Not ddlRelationShip.Items.FindByValue(ddlFormList.SelectedItem.Value.Replace("1~3~", "")) Is Nothing Then
                            ddlRelationShip.SelectedValue = ddlFormList.SelectedItem.Value.Replace("1~3~", "")
                        End If
                        SetVisibility(True, True, True, True, False, False, False, False, False, False, False, False, False, False, False, False, True, "Apply to all Relationships")
                        divFormFieldGroup.Visible = True
                    Else
                        SetVisibility(False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False)
                    End If
                ElseIf GetFormValue() = "99" Then 'Lead Details 
                    If Not ddlRelationShip.Items.FindByValue("46") Is Nothing Then
                        ddlRelationShip.SelectedValue = 46
                    End If
                    SetVisibility(True, True, True, True, False, False, False, False, False, False, False, False, False, False, False, False, True, "Apply to all Relationships")
                    divFormFieldGroup.Visible = True
                ElseIf GetFormValue() = "100" Then 'Prospect Details 
                    If Not ddlRelationShip.Items.FindByValue("46") Is Nothing Then
                        ddlRelationShip.SelectedValue = 46
                    End If
                    SetVisibility(True, True, True, True, False, False, False, False, False, False, False, False, False, False, False, False, True, "Apply to all Relationships")
                    divFormFieldGroup.Visible = True
                ElseIf GetFormValue() = "101" Then 'Account Details  
                    If Not ddlRelationShip.Items.FindByValue("46") Is Nothing Then
                        ddlRelationShip.SelectedValue = 46
                    End If
                    SetVisibility(True, True, True, True, False, False, False, False, False, False, False, False, False, False, False, False, True, "Apply to all Relationships")
                    divFormFieldGroup.Visible = True
                ElseIf GetFormValue() = "146" Then 'Vendor Details  
                    If Not ddlRelationShip.Items.FindByValue("47") Is Nothing Then
                        ddlRelationShip.SelectedValue = 47
                    End If
                    SetVisibility(True, True, True, True, False, False, False, False, False, False, False, False, False, False, False, False, True, "Apply to all Relationships")
                    divFormFieldGroup.Visible = True
                ElseIf GetFormValue() = "147" Then 'Employer Details
                    If Not ddlRelationShip.Items.FindByValue("93") Is Nothing Then
                        ddlRelationShip.SelectedValue = 93
                    End If
                    SetVisibility(True, True, True, True, False, False, False, False, False, False, False, False, False, False, False, False, True, "Apply to all Relationships")
                    divFormFieldGroup.Visible = True
                ElseIf GetFormValue() = "102" Then 'Contact Details  
                    SetVisibility(True, True, True, True, False, False, False, False, False, False, False, False, False, False, False, True, True, "Apply to all Contact Types")
                    divFormFieldGroup.Visible = True
                ElseIf GetFormValue() = "103" Then 'Project Details 
                    SetVisibility(True, True, True, True, False, False, False, False, False, False, False, False, False, False, False, False, False, "")
                    divFormFieldGroup.Visible = True
                ElseIf GetFormValue() = "104" Then 'Case Details  
                    SetVisibility(True, True, True, True, False, False, False, False, False, False, False, False, False, False, False, False, False, "")
                    divFormFieldGroup.Visible = True
                ElseIf GetFormValue() = "105" Then 'Sales Opportunity Details  
                    SetVisibility(True, True, True, True, False, False, False, False, False, False, False, False, False, False, False, False, True, "Apply to all Opportunities & Orders")
                    divFormFieldGroup.Visible = True
                ElseIf GetFormValue() = "106" Then 'Sales Order Details 
                    SetVisibility(True, True, True, True, False, False, False, False, False, False, False, False, False, False, False, False, True, "Apply to all Opportunities & Orders")
                    divFormFieldGroup.Visible = True
                ElseIf GetFormValue() = "107" Then 'Purchase Opportunity Details 
                    SetVisibility(True, True, True, True, False, False, False, False, False, False, False, False, False, False, False, False, True, "Apply to all Opportunities & Orders")
                    divFormFieldGroup.Visible = True
                ElseIf GetFormValue() = "108" Then 'Purchase Order Details 
                    SetVisibility(True, True, True, True, False, False, False, False, False, False, False, False, False, False, False, False, True, "Apply to all Opportunities & Orders")
                    divFormFieldGroup.Visible = True
                ElseIf GetFormValue() = "109" Then 'Lead Grid Columns
                    SetVisibility(False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, True, "Apply to all Relationships")
                ElseIf GetFormValue() = "110" Then 'Prospect Grid Columns
                    SetVisibility(False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, True, "Apply to all Relationships")
                ElseIf GetFormValue() = "111" Then 'Account Grid Columns
                    SetVisibility(False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, True, "Apply to all Relationships")
                ElseIf GetFormValue() = "112" Then 'Vendor Grid Columns  
                    SetVisibility(False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, True, "Apply to all Relationships")
                ElseIf GetFormValue() = "113" Then 'Employer Grid Columns  
                    SetVisibility(False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, True, "Apply to all Relationships")
                ElseIf GetFormValue() = "114" Then 'Contact Grid Columns
                    SetVisibility(False, False, False, False, False, False, False, False, False, False, False, False, False, True, False, False, True, "Apply to all Contact Types")
                ElseIf GetFormValue() = "115" Then 'Project Grid Columns
                    SetVisibility(False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, "")
                ElseIf GetFormValue() = "116" Then 'Case Grid Columns
                    SetVisibility(False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, "")
                ElseIf GetFormValue() = "117" Then 'Tickler Grid Columns
                    SetVisibility(False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, "")
                ElseIf GetFormValue() = "118" Then 'Sales Opportunity Grid Columns
                    SetVisibility(False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, True, "Apply to all Opportunities & Orders")
                ElseIf GetFormValue() = "119" Then 'Sales Order Grid Columns
                    SetVisibility(False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, True, "Apply to all Opportunities & Orders")
                ElseIf GetFormValue() = "120" Then 'Purchase Opportunity Grid Columns
                    SetVisibility(False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, True, "Apply to all Opportunities & Orders")
                ElseIf GetFormValue() = "121" Then 'Purchase Order Grid Configuration
                    SetVisibility(False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, True, "Apply to all Opportunities & Orders")
                ElseIf GetFormValue() = "26" Then 'Products & services Setting (Sales)
                    SetVisibility(False, False, False, False, False, False, False, False, False, False, False, False, False, False, True, False, False, "")
                ElseIf GetFormValue() = "129" Then 'Products & services Setting (Purchase)
                    SetVisibility(False, False, False, False, False, False, False, False, False, False, False, False, False, False, True, False, False, "")
                ElseIf GetFormValue() = "130" Then 'Item Grid Columns
                    SetVisibility(False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, "")
                ElseIf GetFormValue() = "131" Then 'Activity Form 
                    ActivityForm.Visible = True
                    ActivtyFormGCalendar.Visible = True
                    MainRow.Visible = False
                    divbtnGenerateHtml.Visible = False
                    iframeRedirectionDiv.Visible = False
                    tdState.Visible = False
                    tblBzFormWizardConfigAOIHeader.Visible = False
                    btnSuveryForm.Visible = False
                    BindActivityFormDetails()
                Else
                    If (GetFormValue() = "1" Or GetFormValue() = "15" Or GetFormValue() = "17" Or GetFormValue() = "18" Or GetFormValue() = "29" _
                    Or GetFormValue() = "59") Then
                        chkGlobalSetting.Visible = False
                    ElseIf GetFormValue() = "56" Then
                        chkGlobalSetting.Visible = False
                    Else
                        chkGlobalSetting.Visible = False
                    End If


                    If GetFormValue() = 6 Or
                        GetFormValue() = 7 Or
                        GetFormValue() = 8 Or
                        GetFormValue() = 56 Or
                        GetFormValue() = 1 Or
                        GetFormValue() = 15 Or
                        GetFormValue() = 17 Or
                        GetFormValue() = 18 Or
                        GetFormValue() = 29 Or
                        GetFormValue() = 59 Or
                        GetFormValue() = 67 Then

                        If GetFormValue() = 7 Or GetFormValue() = 8 Then
                            tdUserGroups.Visible = False
                            lblHeader.Text = "BizDoc Column Fields Configuration"
                            tdBizDocs.Visible = True
                            divBizDoc.Visible = True
                        Else
                            tdUserGroups.Visible = True
                            tdBizDocs.Visible = False
                            divBizDoc.Visible = False
                            lblHeader.Text = "Fields Configuration"
                        End If

                        btnAddTwo.Visible = False
                        btnAddThree.Visible = False

                        trThree.Visible = False
                        trThreeButton.Visible = False

                        lblColumn1.Text = "Grid / List View Columns"
                        lblColumn1Note.Visible = True
                        trTwo.Visible = False
                        trTwoButton.Visible = False

                        btnRemoveTwo.Visible = False

                    Else
                        tdBizDocs.Visible = False
                        divBizDoc.Visible = False
                        btnAddTwo.Visible = True
                        btnAddThree.Visible = True

                        trThree.Visible = True
                        trThreeButton.Visible = True

                        lblColumn1.Text = "Added to column 1"
                        lblColumn1Note.Visible = False

                        trTwo.Visible = True
                        trTwoButton.Visible = True

                        btnRemoveTwo.Visible = True
                    End If

                    If GetFormValue() = 7 Or GetFormValue() = 8 Or GetFormValue() = 17 Or GetFormValue() = 18 Or GetFormValue() = 29 Or GetFormValue() = 30 Then
                        tdState.Visible = False
                    Else
                        tdState.Visible = True
                    End If

                    If GetFormValue() = 3 Or GetFormValue() = 128 Then
                        btnValidateOne.Visible = True
                        btnGenerateHtml.Visible = True
                        divbtnGenerateHtml.Visible = True
                        iframeRedirectionDiv.Visible = True
                        Dim intchkEnableLinkedin As Integer
                        If GetFormValue() = 3 Then
                            chkEnableLinkedin.Visible = True
                            divchkEnableLinkedin.Visible = True
                            divAddForm.Visible = True
                            divlistForm.Visible = True
                            divAssigntoForm.Visible = True
                            divDripCampaign.Visible = True

                        Else
                            chkEnableLinkedin.Visible = False
                            divchkEnableLinkedin.Visible = False
                            divAddForm.Visible = False
                            divlistForm.Visible = False
                            divAssigntoForm.Visible = False
                            divDripCampaign.Visible = False
                        End If
                        btnGenerateHtml.Attributes.Add("onclick", "return GenerateHtmlForm(" & GetFormValue() & "," & ddlLeadFormList.SelectedValue & ")")
                    Else
                        iframeRedirectionDiv.Visible = False
                        btnValidateOne.Visible = False
                        btnGenerateHtml.Visible = False
                        divbtnGenerateHtml.Visible = False
                        chkEnableLinkedin.Visible = False
                        divchkEnableLinkedin.Visible = False
                        divAddForm.Visible = False
                        divlistForm.Visible = False
                        divAssigntoForm.Visible = False
                        divDripCampaign.Visible = False
                    End If


                    If GetFormValue() = 8 Or GetFormValue() = 7 Then
                        tblBizDocs.Visible = True
                        tdSurvey.Visible = False
                        If CCommon.ToLong(Session("AuthoritativeSalesBizDoc")) = 0 Or CCommon.ToLong(Session("AuthoritativePurchaseBizDoc")) = 0 Then
                            litClientAlertMessageScript.Text = "<script language='javascript'>alert('Please Set Authoritative BizDocs and Try Again.');</script>"
                            btnSaveConfiguration.Visible = False
                            Exit Sub
                        End If

                        If ddlBizDocTemplate.Items.Count = 1 Then
                            lblError.Text = "Please create BizDoc Template for selected BizDoc."
                        Else
                            lblError.Text = ""
                        End If
                        LoadSummary()

                    ElseIf GetFormValue() = 5 Then
                        DisplaySurveyLists()

                        tdSurvey.Visible = True
                        tblBizDocs.Visible = False
                        tdUserGroups.Visible = False
                    ElseIf GetFormValue() = 67 Then
                        tdSurvey.Visible = False
                        tblBizDocs.Visible = False
                        tdUserGroups.Visible = False
                    Else
                        tdSurvey.Visible = False
                        tblBizDocs.Visible = False
                    End If

                    If GetFormValue() = 58 Then
                        tdUserGroups.Visible = True
                    End If
                End If
                If divFormFieldGroup.Visible = True Then

                    BindFormFieldGroups()
                End If

                If GetFormValue() = "115" Or GetFormValue() = "116" Or GetFormValue() = "117" Then
                    lblTip.Visible = True
                Else
                    lblTip.Visible = False
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Sub BindActivityFormDetails()
            Try
                Dim objConfigWizard As New FormConfigWizard
                objConfigWizard.DomainID = Session("DomainID")
                objConfigWizard.AuthenticationGroupID = ddlUserGroup.SelectedValue
                objConfigWizard.UserCntID = Session("UserContactID")
                Dim ds As New DataSet
                ds = objConfigWizard.getActivityConfiguration()
                If (ds.Tables(0).Rows.Count > 0) Then
                    chkFollowUpStatus.Checked = Convert.ToBoolean(ds.Tables(0).Rows(0)("bitFollowupStatus"))
                    chkDisplayPriority.Checked = Convert.ToBoolean(ds.Tables(0).Rows(0)("bitPriority"))
                    chkDisplayActivity.Checked = Convert.ToBoolean(ds.Tables(0).Rows(0)("bitActivity"))
                    chkCustomFieldSection.Checked = Convert.ToBoolean(ds.Tables(0).Rows(0)("bitCustomField"))
                    chkAttendeeSection.Checked = Convert.ToBoolean(ds.Tables(0).Rows(0)("bitAttendee"))
                    chkFollowupAnytime.Checked = Convert.ToBoolean(ds.Tables(0).Rows(0)("bitFollowupAnytime"))
                Else
                    chkFollowUpStatus.Checked = True
                    chkDisplayPriority.Checked = True
                    chkDisplayActivity.Checked = True
                    chkCustomFieldSection.Checked = True
                    chkAttendeeSection.Checked = True
                    chkFollowupAnytime.Checked = True
                End If

                If (ds.Tables(1).Rows.Count > 0) Then
                    chkTitle.Checked = Convert.ToBoolean(ds.Tables(1).Rows(0)("bitTitle"))
                    chkLocation.Checked = Convert.ToBoolean(ds.Tables(1).Rows(0)("bitLocation"))
                    chkDescription.Checked = Convert.ToBoolean(ds.Tables(1).Rows(0)("bitDescription"))
                Else
                    chkTitle.Checked = False
                    chkLocation.Checked = False
                    chkDescription.Checked = False
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

         Sub SetActivityFormDetails()
            Try
                Dim objConfigWizard As New FormConfigWizard
                objConfigWizard.DomainID = Session("DomainID")
                objConfigWizard.AuthenticationGroupID = ddlUserGroup.SelectedValue
                objConfigWizard.bitFollowUpStatus = chkFollowUpStatus.Checked
                objConfigWizard.bitPriority = chkDisplayPriority.Checked
                objConfigWizard.bitActivity = chkDisplayActivity.Checked
                objConfigWizard.bitCustomField = chkCustomFieldSection.Checked
                objConfigWizard.bitAttendee = chkAttendeeSection.Checked
                objConfigWizard.bitFollowupAnytime = chkFollowupAnytime.Checked
                objConfigWizard.Title = chkTitle.Checked
                objConfigWizard.Description = chkDescription.Checked
                objConfigWizard.Location = chkLocation.Checked
                objConfigWizard.UserCntID = Session("UserContactID")
                Dim res As String
                res = objConfigWizard.setActivityConfiguration()
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub LoadSummary()
            Try
                Dim objConfigWizard As New FormConfigWizard
                objConfigWizard.DomainID = Session("DomainID")
                objConfigWizard.BizDocID = ddlBizDoc.SelectedValue
                objConfigWizard.FormID = GetFormValue()
                objConfigWizard.BizDocTemplateID = CCommon.ToLong(ddlBizDocTemplate.SelectedValue)

                Dim ds As DataSet
                ds = objConfigWizard.GetFieldFormListForBizDocsSumm
                lstSummaryAvlFlds.DataTextField = "vcFormFieldName"
                lstSummaryAvlFlds.DataValueField = "numFormFieldId"
                lstSummaryAvlFlds.DataSource = ds.Tables(0)
                lstSummaryAvlFlds.DataBind()

                lstSummaryAddedFlds.DataTextField = "vcFormFieldName"
                lstSummaryAddedFlds.DataValueField = "numFormFieldId"
                lstSummaryAddedFlds.DataSource = ds.Tables(1)
                lstSummaryAddedFlds.DataBind()
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        'Private Sub btnBizDocTemplate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBizDocTemplate.Click
        '    Try
        '        Response.Redirect("../admin/frmBizDocTemplate.aspx", False)
        '    Catch ex As Exception
        '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
        '        DisplayError(ex.Message)
        '    End Try
        'End Sub

        'Private Sub ddlSurvey_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlSurvey.SelectedIndexChanged
        '    If ddlSurvey.SelectedValue > 0 Then
        '        btnSuveryForm.Attributes.Add("onclick", "return OpenForm('" & ConfigurationManager.AppSettings("SurveyScreen") & "?D=" & Session("DomainID") & "&numSurId=" & ddlSurvey.SelectedValue & "');")
        '    Else
        '        btnSuveryForm.Attributes.Add("onclick", "return OpenForm('" & ConfigurationManager.AppSettings("SurveyScreen") & "?D=" & Session("DomainID") & "');")
        '    End If
        'End Sub

        Private Sub SetVisibility(ByVal bitTrTwo As Boolean, ByVal bitBtnAddTwo As Boolean, ByVal bitBtnRemoveTwo As Boolean,
                                  ByVal bitTrTwoButton As Boolean, ByVal bitTrThree As Boolean, ByVal bitBtnAddThree As Boolean,
                                  ByVal bitBtnRemoveThree As Boolean, ByVal bitTrThreeButton As Boolean, ByVal bitTdState As Boolean,
                                  ByVal bitTdSurvey As Boolean, ByVal bitTblBizDocs As Boolean, ByVal bitConfigAOI As Boolean, ByVal bitConfigAOIHeader As Boolean,
                                  ByVal bitContact As Boolean, ByVal bitOppType As Boolean, ByVal bitBizDocs As Boolean, Optional ByVal bitShowGlobalSettings As Boolean = False, Optional ByVal globalMessage As String = "")
            Try
                If bitTrTwo Then
                    lblColumn1.Text = "Added to column 1"
                    lblColumn1Note.Visible = False
                Else
                    lblColumn1.Text = "Grid / List View Columns"
                    lblColumn1Note.Visible = True
                End If

                trTwo.Visible = bitTrTwo
                btnAddTwo.Visible = bitBtnAddTwo
                btnRemoveTwo.Visible = bitBtnRemoveTwo
                trTwoButton.Visible = bitTrTwoButton
                trThree.Visible = bitTrThree
                btnAddThree.Visible = bitBtnAddThree
                btnRemoveThree.Visible = bitBtnRemoveThree
                trThreeButton.Visible = bitTrThreeButton
                tdState.Visible = bitTdState
                tdSurvey.Visible = bitTdSurvey
                tblBizDocs.Visible = bitTblBizDocs
                tblBzFormWizardConfigAOI.Visible = bitConfigAOI
                tblBzFormWizardConfigAOIHeader.Visible = bitConfigAOIHeader
                pnlContact.Visible = bitContact
                tdBizDocs.Visible = bitBizDocs
                divBizDoc.Visible = bitBizDocs

                tdSurvey.Visible = False
                tdUserGroups.Visible = True
                lblHeader.Text = "Fields Configuration"
                chkGlobalSetting.Visible = bitShowGlobalSettings
                chkGlobalSetting.Text = globalMessage
            Catch ex As Exception

            End Try
        End Sub

        Private Sub BindBizFormWizardModule()
            Try
                Dim objConfigWizard As New FormConfigWizard
                objConfigWizard.DomainID = Session("DomainID")
                ddlBizFormModule.DataSource = objConfigWizard.GetBizFormWizardModuleList()
                ddlBizFormModule.DataTextField = "vcModule"
                ddlBizFormModule.DataValueField = "numBizFormModuleID"
                ddlBizFormModule.DataBind()
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub BindFormList()
            Try
                ddlFormList.Items.Clear()


                Dim dtData As New DataTable
                dtData.Columns.Add("numFormId")
                dtData.Columns.Add("vcFormName")

                Dim dr As DataRow

                Dim objConfigWizard As New FormConfigWizard
                objConfigWizard.DomainID = Session("DomainID")          'Set value of domain id
                objConfigWizard.numBizFormModuleID = CCommon.ToLong(ddlBizFormModule.SelectedValue)
                objConfigWizard.OnlySurvey = False
                Dim dtFormsList As DataTable = objConfigWizard.getFormList()

                If Not dtFormsList Is Nothing AndAlso dtFormsList.Rows.Count > 0 Then
                    If ddlBizFormModule.SelectedValue = "2" Then
                        For Each drItem As DataRow In dtFormsList.Rows
                            If Not divFormFilter.Visible Or rblFormType.SelectedItem Is Nothing Then
                                dr = dtData.NewRow
                                dr("numFormId") = "0~" & CCommon.ToShort(drItem("tintPageType")) & "~" & CCommon.ToString(drItem("numFormId"))
                                dr("vcFormName") = drItem("vcFormName")
                                dtData.Rows.Add(dr)
                            ElseIf rblFormType.SelectedValue = "3" AndAlso CCommon.ToShort(drItem("tintPageType")) = 3 Then
                                dr = dtData.NewRow
                                dr("numFormId") = "0~3~" & CCommon.ToString(drItem("numFormId"))
                                dr("vcFormName") = drItem("vcFormName")
                                dtData.Rows.Add(dr)
                            ElseIf rblFormType.SelectedValue = "1" AndAlso CCommon.ToShort(drItem("tintPageType")) = 1 Then
                                dr = dtData.NewRow
                                dr("numFormId") = "0~1~" & CCommon.ToString(drItem("numFormId"))
                                dr("vcFormName") = drItem("vcFormName")
                                dtData.Rows.Add(dr)
                            End If
                        Next
                    Else
                        For Each drItem As DataRow In dtFormsList.Rows
                            If (Not divFormFilter.Visible Or rblFormType.SelectedItem Is Nothing Or ((rblFormType.SelectedValue = "3" AndAlso CCommon.ToShort(drItem("tintPageType")) = "3") Or (rblFormType.SelectedValue = "1" AndAlso CCommon.ToShort(drItem("tintPageType")) = "1"))) Then
                                dr = dtData.NewRow
                                dr("numFormId") = drItem("numFormId")
                                dr("vcFormName") = drItem("vcFormName")
                                dtData.Rows.Add(dr)
                            End If
                        Next
                    End If
                End If

                If ddlBizFormModule.SelectedValue = "2" Then
                    'Loads custom relationships fields
                    Dim dtCustomRelationShip As DataTable
                    Dim objCommon As New CCommon
                    objCommon.ListID = 5 'Relationship
                    objCommon.DomainID = Session("DomainID")
                    dtCustomRelationShip = objCommon.GetMasterListItemsWithRights()

                    If Not dtCustomRelationShip Is Nothing AndAlso dtCustomRelationShip.Rows.Count > 0 Then
                        For Each drItem As DataRow In dtCustomRelationShip.Rows
                            If CCommon.ToLong(drItem("constFlag")) <> 1 Then
                                If Not divFormFilter.Visible Or (rblFormType.SelectedItem Is Nothing Or rblFormType.SelectedValue = "3") Then
                                    dr = dtData.NewRow
                                    dr("numFormId") = "1~3~" & CCommon.ToString(drItem("numListItemID"))
                                    dr("vcFormName") = CCommon.ToString(drItem("vcData")) & " Details"
                                    dtData.Rows.Add(dr)
                                End If

                                If Not divFormFilter.Visible Or (rblFormType.SelectedItem Is Nothing Or rblFormType.SelectedValue = "1") Then
                                    dr = dtData.NewRow
                                    dr("numFormId") = "1~1~" & CCommon.ToString(drItem("numListItemID"))
                                    dr("vcFormName") = CCommon.ToString(drItem("vcData")) & " Grid Columns"
                                    dtData.Rows.Add(dr)
                                End If
                            End If
                        Next
                    End If
                End If

                dtData.DefaultView.Sort = "vcFormName ASC"

                ddlFormList.DataSource = dtData.DefaultView.ToTable()           'set the datasource for the form radiolist
                ddlFormList.DataTextField = "vcFormName"                        'set the textfield
                ddlFormList.DataValueField = "numFormId"                        'set the value attribute
                ddlFormList.DataBind()                                          'Databind the AOI

                If ddlFormList.Items.Count > 0 Then
                    ddlFormList.SelectedIndex = 0
                End If

                If objCommon Is Nothing Then
                    objCommon = New CCommon
                End If

                If GetQueryStringVal("FormID") <> "" Then
                    If Not ddlFormList.Items.FindByValue(GetQueryStringVal("FormID")) Is Nothing Then
                        ddlFormList.ClearSelection()
                        ddlFormList.Items.FindByValue(GetQueryStringVal("FormID")).Selected = True
                    End If
                End If

                If GetFormValue() = "5" Then
                    DisplaySurveyLists()
                End If

                ddlFormList_SelectedIndexChanged(Nothing, Nothing)
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Function GetFormValue(Optional ByVal formIDString As String = "") As Long
            Try
                Dim formID As Long

                If formIDString <> "" AndAlso formIDString.Contains("~") Then
                    formID = CCommon.ToLong(formIDString.Split("~")(2))
                ElseIf ddlFormList.SelectedValue.Contains("~") Then
                    formID = CCommon.ToLong(ddlFormList.SelectedValue.Split("~")(2))
                Else
                    formID = CCommon.ToLong(ddlFormList.SelectedValue)
                End If

                Return formID
            Catch ex As Exception
                Throw
            End Try
        End Function
        Private Sub SaveMasterConfiguration(ByVal strXml As String, ByVal hfFormId As Long, ByVal contacttype As String, ByVal hfPType As String, ByVal pageId As Long, ByVal numRelCntType As Long, ByVal bitGridConfiguration As Boolean, ByVal userGroupID As String)
            Try
                Dim objBizFormWizardFormConf As New BizFormWizardMasterConfiguration
                objBizFormWizardFormConf.DomainID = CCommon.ToLong(Session("DomainID"))
                objBizFormWizardFormConf.numFormID = hfFormId
                objBizFormWizardFormConf.numGroupID = CCommon.ToLong(userGroupID)
                objBizFormWizardFormConf.numRelCntType = numRelCntType
                objBizFormWizardFormConf.tintPageType = hfPType
                objBizFormWizardFormConf.numPageID = pageId
                objBizFormWizardFormConf.bitGridConfiguration = bitGridConfiguration
                objBizFormWizardFormConf.numFormFieldGroupId = ddlFieldGroupId.SelectedValue
                objBizFormWizardFormConf.SaveFormConfiguration(strXml)
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        Private Sub LoadFormFieldGroup()
            ddlFieldGroupId.Items.Clear()

        End Sub
        Private Sub LoadFieldsForDetailsAndColumnSettings(ByVal hfFormId As Long, ByVal contacttype As String, ByVal hfPType As String, ByVal pageId As Long, ByVal numRelCntType As Long, ByVal bitGridConfiguration As Boolean, ByVal bitCustomRelationship As Boolean)
            Try
                Dim dtAvailableFields As DataTable
                Dim dtFieldsInColumn1 As DataTable
                Dim dtFieldsInColumn2 As DataTable


                dtAvailableFields = GetFields(0, hfFormId, contacttype, hfPType, pageId, numRelCntType, bitGridConfiguration)
                dtFieldsInColumn1 = GetFields(1, hfFormId, contacttype, hfPType, pageId, numRelCntType, bitGridConfiguration)
                dtFieldsInColumn2 = GetFields(2, hfFormId, contacttype, hfPType, pageId, numRelCntType, bitGridConfiguration)

                lstAvailablefld.DataSource = dtAvailableFields
                lstAvailablefld.DataTextField = "vcNewFormFieldName"
                lstAvailablefld.DataValueField = "numFormFieldId"
                lstAvailablefld.DataBind()

                lstSelectedfldOne.DataSource = dtFieldsInColumn1
                lstSelectedfldOne.DataTextField = "vcNewFormFieldName"
                lstSelectedfldOne.DataValueField = "numFormFieldId"
                lstSelectedfldOne.DataBind()

                lstSelectedfldTwo.DataSource = dtFieldsInColumn2
                lstSelectedfldTwo.DataTextField = "vcNewFormFieldName"
                lstSelectedfldTwo.DataValueField = "numFormFieldId"
                lstSelectedfldTwo.DataBind()


                If Not dtFieldsInColumn1 Is Nothing AndAlso dtFieldsInColumn1.Rows.Count > 0 Then
                    Dim newRow As DataRow

                    For Each dr As DataRow In dtFieldsInColumn1.Rows
                        newRow = dtAvailableFields.NewRow()
                        newRow("vcNewFormFieldName") = dr("vcNewFormFieldName")
                        newRow("vcFormFieldName") = dr("vcFormFieldName")
                        newRow("numFormFieldId") = dr("numFormFieldId")
                        newRow("vcAssociatedControlType") = dr("vcAssociatedControlType")
                        newRow("numListID") = dr("numListID")
                        newRow("vcDbColumnName") = dr("vcDbColumnName")
                        newRow("vcListItemType") = dr("vcListItemType")
                        newRow("intColumnNum") = dr("intColumnNum")
                        newRow("intRowNum") = dr("intRowNum")
                        newRow("boolRequired") = dr("boolRequired")
                        newRow("numAuthGroupID") = dr("numAuthGroupID")
                        newRow("vcFieldDataType") = dr("vcFieldDataType")
                        newRow("boolAOIField") = dr("boolAOIField")
                        newRow("numFormID") = dr("numFormID")
                        newRow("vcLookBackTableName") = dr("vcLookBackTableName")
                        newRow("vcFieldAndType") = dr("vcFieldAndType")
                        newRow("bitDefaultMandatory") = dr("bitDefaultMandatory")
                        newRow("numFormGroupId") = dr("numFormGroupId")
                        dtAvailableFields.Rows.Add(newRow)
                    Next

                    dtAvailableFields.AcceptChanges()
                End If

                If Not dtFieldsInColumn2 Is Nothing AndAlso dtFieldsInColumn2.Rows.Count > 0 Then
                    Dim newRow As DataRow

                    For Each dr As DataRow In dtFieldsInColumn2.Rows
                        newRow = dtAvailableFields.NewRow()
                        newRow("vcNewFormFieldName") = dr("vcNewFormFieldName")
                        newRow("vcFormFieldName") = dr("vcFormFieldName")
                        newRow("numFormFieldId") = dr("numFormFieldId")
                        newRow("vcAssociatedControlType") = dr("vcAssociatedControlType")
                        newRow("numListID") = dr("numListID")
                        newRow("vcDbColumnName") = dr("vcDbColumnName")
                        newRow("vcListItemType") = dr("vcListItemType")
                        newRow("intColumnNum") = dr("intColumnNum")
                        newRow("intRowNum") = dr("intRowNum")
                        newRow("boolRequired") = dr("boolRequired")
                        newRow("numAuthGroupID") = dr("numAuthGroupID")
                        newRow("vcFieldDataType") = dr("vcFieldDataType")
                        newRow("boolAOIField") = dr("boolAOIField")
                        newRow("numFormID") = dr("numFormID")
                        newRow("vcLookBackTableName") = dr("vcLookBackTableName")
                        newRow("vcFieldAndType") = dr("vcFieldAndType")
                        newRow("bitDefaultMandatory") = dr("bitDefaultMandatory")
                        newRow("numFormGroupId") = dr("numFormGroupId")
                        dtAvailableFields.Rows.Add(newRow)
                    Next

                    dtAvailableFields.AcceptChanges()
                End If

                Dim sUsedFieldsInAutoRules As String = String.Empty                         'variable to store the fields whcih are referenced in Auto Rules

                '================================================================================
                ' Purpose: Calls the subroutine to create the client side script
                '
                ' History
                ' Ver   Date        Ref     Author              Reason Created
                ' 1     09/07/2005          Debasish Nag        Calls the function to create
                '                                               the client side script
                '================================================================================
                Dim sClientSideString As New System.Text.StringBuilder                              'Declare a string builder
                Dim iRowIndex As Int16 = 0                                                          'Declare an integer to hold the row index

                sClientSideString.Append(vbCrLf & "<script language='javascript'>")                  'Start with the script block
                sClientSideString.Append(vbCrLf & "var arrFieldConfig = new Array();" & vbCrLf)      'Create a array to hold the field information in javascript
                sClientSideString.Append(createClientSideScript(dtAvailableFields, iRowIndex))      'Call function to create a client side script
                'If cAOIField = "Y" Then                                                              'Check if AOI's are associated with a form or not
                '    sClientSideString.Append(createClientSideScript(dtAvailableAOIList, iRowIndex))  'Call function to create a client side script
                'End If
                sClientSideString.Append(vbCrLf & "var sUsedFieldsInAutoRules = '" & sUsedFieldsInAutoRules & "';" & vbCrLf) 'The list of fields which are referenced in Auto Rules and cannot be removed
                sClientSideString.Append("</script>")                                                'end the client side javascript block
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Fields", sClientSideString.ToString(), False)
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Function GetFields(ByVal intColumn As Integer, ByVal hfFormId As Long, ByVal contacttype As String, ByVal hfPType As Long, ByVal pageId As Long, ByVal numRelCntType As Long, ByVal bitGridConfiguration As Boolean) As DataTable
            Try
                Dim dttable1 As DataTable

                Dim objBizFormWizardFormConf As New BizFormWizardMasterConfiguration
                objBizFormWizardFormConf.DomainID = CCommon.ToLong(Session("DomainID"))
                objBizFormWizardFormConf.numFormID = hfFormId
                objBizFormWizardFormConf.numGroupID = CCommon.ToLong(ddlUserGroup.SelectedValue)
                objBizFormWizardFormConf.numRelCntType = numRelCntType
                objBizFormWizardFormConf.tintPageType = hfPType
                objBizFormWizardFormConf.numPageID = pageId
                objBizFormWizardFormConf.bitGridConfiguration = bitGridConfiguration
                objBizFormWizardFormConf.numFormFieldGroupId = ddlFieldGroupId.SelectedValue
                If intColumn = 0 Then
                    Dim ds As DataSet
                    Dim dttable2 As New DataTable

                    ds = objBizFormWizardFormConf.GetAvailableFields()

                    If (ds.Tables.Count = 2) Then
                        dttable1 = ds.Tables(0)
                        dttable2 = ds.Tables(1)
                    Else
                        dttable1 = ds.Tables(0)
                    End If

                    If contacttype = "T" Then
                        dttable1 = dttable2
                    Else
                        If (dttable2.Rows.Count > 0) Then dttable1.Merge(dttable2)
                    End If
                Else
                    objBizFormWizardFormConf.intColumnNum = intColumn
                    dttable1 = objBizFormWizardFormConf.GetSelectedFields()
                End If

                Return dttable1
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function ConvertStringToArray(ByVal var As String) As String
            Dim mynumber As String = Regex.Replace(var, "\D", "")
            Return mynumber
        End Function

        Private Sub ddlBizFormModule_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlBizFormModule.SelectedIndexChanged
            Try
                rblFormType.ClearSelection()

                If ddlBizFormModule.SelectedValue = "2" Or ddlBizFormModule.SelectedValue = "4" Then
                    divFormFilter.Visible = True
                Else
                    divFormFilter.Visible = False
                End If

                BindFormList()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Sub ddlGroup_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlGroup.SelectedIndexChanged
            Try
                DataBindElements()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Sub ddlRelationShip_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlRelationShip.SelectedIndexChanged
            Try
                DataBindElements()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Sub ddlSurvey_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlSurvey.SelectedIndexChanged
            Try
                DataBindElements()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Sub ddlUserGroup_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlUserGroup.SelectedIndexChanged
            Try
                BindFormFieldGroups()
                DataBindElements()

                rcbGroups.Items.Clear()
                Dim radItem As Telerik.Web.UI.RadComboBoxItem
                For Each item As ListItem In ddlUserGroup.Items
                    If item.Value <> "0" AndAlso item.Value <> ddlUserGroup.SelectedValue Then
                        radItem = New Telerik.Web.UI.RadComboBoxItem
                        radItem.Text = item.Text
                        radItem.Value = item.Value
                        rcbGroups.Items.Add(radItem)
                    End If
                Next
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Sub ddlContactType_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlContactType.SelectedIndexChanged
            Try
                DataBindElements()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)

            End Try
        End Sub
        Private Sub BindFormFieldGroups()
            Try
                Dim ds As New DataSet()
                Dim objBizFormWizardFormConf As New BizFormWizardMasterConfiguration
                objBizFormWizardFormConf.DomainID = CCommon.ToLong(Session("DomainID"))
                objBizFormWizardFormConf.numFormID = GetFormValue()
                objBizFormWizardFormConf.numFormFieldGroupId = ddlFieldGroupId.SelectedValue
                objBizFormWizardFormConf.tintPageType = 4
                objBizFormWizardFormConf.numGroupID = CCommon.ToLong(ddlUserGroup.SelectedValue)
                ds = objBizFormWizardFormConf.ManageFormFieldGroup()
                ddlFieldGroupId.DataSource = ds.Tables(0)
                ddlFieldGroupId.DataValueField = "numFormFieldGroupId"
                ddlFieldGroupId.DataTextField = "vcGroupName"
                ddlFieldGroupId.DataBind()
                ddlFieldGroupId.Items.Insert(0, New ListItem("-ALL-", "0"))
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)

            End Try
        End Sub
        Private Sub BindFormFieldGroupsForSorting()
            Try
                Dim ds As New DataSet()
                Dim objBizFormWizardFormConf As New BizFormWizardMasterConfiguration
                objBizFormWizardFormConf.DomainID = CCommon.ToLong(Session("DomainID"))
                objBizFormWizardFormConf.numFormID = GetFormValue()
                objBizFormWizardFormConf.numFormFieldGroupId = ddlFieldGroupId.SelectedValue
                objBizFormWizardFormConf.tintPageType = 4
                objBizFormWizardFormConf.numGroupID = CCommon.ToLong(ddlUserGroup.SelectedValue)
                ds = objBizFormWizardFormConf.ManageFormFieldGroup()
                ddlFieldGroupId.DataSource = ds.Tables(0)
                ddlFieldGroupId.DataValueField = "numFormFieldGroupId"
                ddlFieldGroupId.DataTextField = "vcGroupName"
                ddlFieldGroupId.DataBind()
                ddlFieldGroupId.Items.Insert(0, New ListItem("-ALL-", "0"))
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)

            End Try
        End Sub
        Private Sub DisplayError(ByVal ex As String)
            DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ex
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
        End Sub
        Protected Sub btnAddFieldGroup_Click(sender As Object, e As EventArgs)
            Try
                Dim objBizFormWizardFormConf As New BizFormWizardMasterConfiguration
                objBizFormWizardFormConf.DomainID = CCommon.ToLong(Session("DomainID"))
                objBizFormWizardFormConf.numFormID = GetFormValue()
                objBizFormWizardFormConf.numFormFieldGroupId = ddlFieldGroupId.SelectedValue
                objBizFormWizardFormConf.numGroupID = CCommon.ToLong(ddlUserGroup.SelectedValue)
                objBizFormWizardFormConf.vcGroupName = txtFieldGroupName.Text
                If objBizFormWizardFormConf.numFormFieldGroupId > 0 Then
                    objBizFormWizardFormConf.tintPageType = 2
                Else
                    objBizFormWizardFormConf.tintPageType = 1
                End If
                objBizFormWizardFormConf.ManageFormFieldGroup()
                BindFormFieldGroups()
                If objBizFormWizardFormConf.OutputsStatus = 1 Then
                    litClientAlertMessageScript.Text = "<script language='javascript'> alert('Group added successfully');</script>"
                ElseIf objBizFormWizardFormConf.OutputsStatus = 3 Then
                    litClientAlertMessageScript.Text = "<script language='javascript'> alert('Group updated successfully');</script>"
                Else
                    litClientAlertMessageScript.Text = "<script language='javascript'> alert('Group already exist');</script>"
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            Finally
                DataBindElements()                                                  'Calls the subroutine to databind the listboxes to dataset
                hdSaveConfigurationStatus.Value = "No"
            End Try
        End Sub
        Protected Sub btnDeleteFieldGroup_Click(sender As Object, e As EventArgs)
            Try
                Dim objBizFormWizardFormConf As New BizFormWizardMasterConfiguration
                objBizFormWizardFormConf.DomainID = CCommon.ToLong(Session("DomainID"))
                objBizFormWizardFormConf.numFormID = GetFormValue()
                objBizFormWizardFormConf.numFormFieldGroupId = ddlFieldGroupId.SelectedValue
                objBizFormWizardFormConf.numGroupID = CCommon.ToLong(ddlUserGroup.SelectedValue)
                objBizFormWizardFormConf.tintPageType = 3
                objBizFormWizardFormConf.ManageFormFieldGroup()
                BindFormFieldGroups()
                If objBizFormWizardFormConf.OutputsStatus = 4 Then
                    litClientAlertMessageScript.Text = "<script language='javascript'> alert('Group deleted successfully');</script>"
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            Finally
                DataBindElements()                                                  'Calls the subroutine to databind the listboxes to dataset
                hdSaveConfigurationStatus.Value = "No"
            End Try
        End Sub
        Protected Sub btnAddForm_Click(sender As Object, e As EventArgs)
            Try
                Dim res As String
                Dim objFormConf As New FormConfigWizard()
                objFormConf.DomainID = Session("DomainID")
                objFormConf.numSubFormId = 0
                objFormConf.vcFormName = txtFormName.Text
                res = objFormConf.SetLeadSubFormList()
                txtFormName.Text = ""
                BindSubFormList()
                DataBindElements()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub
        Private Sub BindSubFormList()
            Try
                Dim dt As New DataTable
                Dim objFormConf As New FormConfigWizard()
                objFormConf.DomainID = Session("DomainID")
                dt = objFormConf.GetLeadSubFormList()
                ddlLeadFormList.DataSource = dt
                ddlLeadFormList.DataTextField = "vcFormName"
                ddlLeadFormList.DataValueField = "numSubFormId"
                ddlLeadFormList.DataBind()
                ddlLeadFormList.Items.Insert(0, New ListItem("-Select-", "0"))
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Sub BindSubFormDetails()
            Try
                Dim dt As New DataTable
                Dim objFormConf As New FormConfigWizard()
                objFormConf.DomainID = Session("DomainID")
                objFormConf.numSubFormId = ddlLeadFormList.SelectedValue
                dt = objFormConf.GetLeadSubFormListDetails()
                If dt.Rows.Count > 0 Then
                    chkAssignToFrom.Checked = dt.Rows(0)("bitByPassRoutingRules")
                    If Not ddlAssignToForm.Items.FindByValue(Convert.ToInt32(dt.Rows(0)("numAssignTo"))) Is Nothing Then
                        ddlAssignToForm.ClearSelection()
                        ddlAssignToForm.Items.FindByValue(Convert.ToInt32(dt.Rows(0)("numAssignTo"))).Selected = True
                    End If

                    chkDripCampaign.Checked = dt.Rows(0)("bitDripCampaign")
                    If Not ddlDripCampaign.Items.FindByValue(Convert.ToInt32(dt.Rows(0)("numDripCampaign"))) Is Nothing Then
                        ddlDripCampaign.ClearSelection()
                        ddlDripCampaign.Items.FindByValue(Convert.ToInt32(dt.Rows(0)("numDripCampaign"))).Selected = True
                    End If
                Else
                    ddlAssignToForm.ClearSelection()
                    ddlDripCampaign.ClearSelection()
                    chkDripCampaign.Checked = False
                    chkAssignToFrom.Checked = False
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Protected Sub btnDeleteForm_Click(sender As Object, e As EventArgs)
            Try
                Dim res As String
                Dim objFormConf As New FormConfigWizard()
                objFormConf.DomainID = Session("DomainID")
                objFormConf.numSubFormId = ddlLeadFormList.SelectedValue
                objFormConf.vcFormName = txtFormName.Text
                res = objFormConf.SetLeadSubFormList()
                BindSubFormList()
                DataBindElements()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Protected Sub ddlLeadFormList_SelectedIndexChanged(sender As Object, e As EventArgs)
            FormEvnt = 1
            BindSubFormDetails()
            ddlFormList_SelectedIndexChanged(Nothing, Nothing)
        End Sub

        Protected Sub ddlFieldGroupId_SelectedIndexChanged(sender As Object, e As EventArgs)
            DataBindElements()
        End Sub

        <WebMethod()>
        Public Shared Function WebMethodSortGroup(ByVal DomainID As Integer, ByVal FormId As Integer, ByVal GroupID As Integer, ByVal sortGroup As String) As String
            Try
                Dim objAdmin = New CAdmin()
                Dim output As Long = 0
                Dim header(3) As String
                header = sortGroup.Split("(")
                If (header.Length > 0) Then
                    header = header(header.Length - 1).Split(")")
                    sortGroup = header(0)
                End If
                Dim objBizFormWizardFormConf As New BizFormWizardMasterConfiguration
                objBizFormWizardFormConf.DomainID = CCommon.ToLong(DomainID)
                objBizFormWizardFormConf.numFormID = CCommon.ToLong(FormId)
                objBizFormWizardFormConf.numGroupID = CCommon.ToLong(GroupID)
                objBizFormWizardFormConf.tintPageType = 5
                objBizFormWizardFormConf.vcGroupName = sortGroup
                Dim ds As New DataSet()
                objBizFormWizardFormConf.numGroupID = CCommon.ToLong(GroupID)
                ds = objBizFormWizardFormConf.ManageFormFieldGroup()
                Dim json As String = String.Empty
                json = JsonConvert.SerializeObject("1", Formatting.None)
                Return json
            Catch ex As Exception
                Dim strError As String = ""

                strError = ex.Message
                Throw New Exception(strError)
                'Throw ex
            End Try
        End Function
        <WebMethod()>
        Public Shared Function WebMethodFetchSortGroup(ByVal DomainID As Integer, ByVal FormId As Integer, ByVal GroupID As Integer, ByVal IsAlphabetical As Boolean) As String
            Try
                Dim output As Long = 0
                Dim objBizFormWizardFormConf As New BizFormWizardMasterConfiguration
                objBizFormWizardFormConf.DomainID = CCommon.ToLong(DomainID)
                objBizFormWizardFormConf.numFormID = CCommon.ToLong(FormId)
                objBizFormWizardFormConf.tintPageType = 4
                objBizFormWizardFormConf.numGroupID = CCommon.ToLong(GroupID)
                Dim ds As New DataSet()
                ds = objBizFormWizardFormConf.ManageFormFieldGroup()
                Dim view As DataView = ds.Tables(0).DefaultView
                Dim dtTable As New DataTable
                If IsAlphabetical = True Then
                    view.Sort = "vcGroupName ASC"
                    dtTable = view.ToTable()
                Else
                    dtTable = ds.Tables(0)
                End If
                Dim json As String = String.Empty
                json = JsonConvert.SerializeObject(dtTable, Formatting.None)
                Return json
            Catch ex As Exception
                Dim strError As String = ""

                strError = ex.Message
                Throw New Exception(strError)
                'Throw ex
            End Try
        End Function

        Protected Sub rblFormType_SelectedIndexChanged(sender As Object, e As EventArgs)
            Try
                BindFormList()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub
    End Class
End Namespace