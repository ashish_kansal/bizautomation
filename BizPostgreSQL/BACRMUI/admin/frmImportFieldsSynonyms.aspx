﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/common/PopupBootstrap.Master" CodeBehind="frmImportFieldsSynonyms.aspx.vb" Inherits=".frmImportFieldsSynonyms" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        function ValidateAdd() {
            if ($("[id$=ddlImportType]").val() == "0") {
                alert("Select import type");
                $("[id$=ddlImportType]").focus();
                return false;
            }

            if ($("[id$=ddlFields]").val() == "0") {
                alert("Select field");
                $("[id$=ddlFields]").focus();
                return false;
            }

            if ($("[id$=txtValue]").val().trim() == "") {
                alert("Enter synonym value");
                $("[id$=txtValue]").focus();
                return false;
            }

            return true;
        }

        function ValidateDelete() {
            var selectedRecods = "";

            $("[id$=gvSynonyms] tr").each(function () {
                if ($(this).find("[id$=chkSelect]").length > 0) {
                    if ($(this).find("[id$=chkSelect]").is(":checked")) {
                        selectedRecods += ((selectedRecods.length > 0 ? "," : "") + $(this).find("[id$=hdnID]").val());
                    }
                }
            });

            if (selectedRecods == "") {
                alert("Select at least one record to delete");
                return false
            }

            $("[id$=hdnSelectedRecords]").val(selectedRecods);

            return true;
        }

        function SelectAllChanged(chkSelectAll) {
            $("[id$=gvSynonyms] [id$=chkSelect]").prop("checked", $(chkSelectAll).is(":checked"));            
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitle" runat="server">
    Import Fields Synonyms
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Content" runat="server">
    <div class="row padbottom10" id="divError" runat="server" style="display: none">
        <div class="col-sm-12">
            <div class="alert alert-danger">
                <h4><i class="icon fa fa-ban"></i>Error</h4>
                <p>
                    <asp:Label ID="lblError" runat="server"></asp:Label>
                </p>
            </div>
        </div>
    </div>
    <div class="row padbottom10">
        <div class="col-xs-12">
            <div class="pull-left">
                <div class="form-inline">
                    <div class="form-group">
                        <label>Import Type</label>
                        <asp:DropDownList runat="server" ID="ddlImportType" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlImportType_SelectedIndexChanged"></asp:DropDownList>
                    </div>
                    <div class="form-group">
                        <label>Fields</label>
                        <asp:DropDownList runat="server" ID="ddlFields" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlFields_SelectedIndexChanged">
                            <asp:ListItem Value="0" Text="-- Select One --"></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>
            </div>
            <div class="pull-right">
                <div class="form-inline">
                    <div class="form-group">
                        <label>Value</label>
                        <asp:TextBox runat="server" ID="txtValue" CssClass="form-control"></asp:TextBox>
                    </div>
                    <asp:Button runat="server" ID="btnAdd" Text="Add" CssClass="btn btn-primary" OnClientClick="return ValidateAdd();" OnClick="btnAdd_Click" />
                    <asp:Button runat="server" ID="btnDelete" Text="Delete" CssClass="btn btn-danger" OnClientClick="return ValidateDelete();" OnClick="btnDelete_Click" />
                </div>
            </div>
        </div>
    </div>
    <div class="row padbottom10">
        <div class="col-xs-12">
            <div class="table-responsive">
                <asp:GridView ID="gvSynonyms" runat="server" CssClass="table table-bordered table-primary" AutoGenerateColumns="false" UseAccessibleHeader="true" ShowHeaderWhenEmpty="true" OnRowDataBound="gvSynonyms_RowDataBound" >
                    <Columns>
                        <asp:BoundField HeaderText="Synonym" DataField="vcSynonym" />
                        <asp:TemplateField ItemStyle-Width="30">
                            <HeaderTemplate>
                                <input type="checkbox" id="chkSelectAll" onclick="SelectAllChanged(this);" />
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:CheckBox runat="server" ID="chkSelect" />
                                <asp:HiddenField ID="hdnID" runat="server" Value='<%# Eval("ID")%>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <EmptyDataTemplate>
                        No Records Found
                    </EmptyDataTemplate>
                </asp:GridView>
            </div>
        </div>
    </div>
    <asp:HiddenField ID="hdnSelectedRecords" runat="server" />
</asp:Content>
