<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmAdvSurvey.aspx.vb"
    Inherits="BACRM.UserInterface.Admin.frmAdvSurvey" MasterPageFile="~/common/DetailPage.Master" %>

<%@ Register TagPrefix="menu1" TagName="webmenu" Src="../include/webmenu.ascx" %>
<%@ Register Src="frmAdvSearchCriteria.ascx" TagName="frmAdvSearchCriteria" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Survey Search</title>
    <style type="text/css">
        #divMain .form-inline {
            min-height: 50px;
        }

        #divMain .form-inline > label {
            min-width: 126px;
            text-align:right;
        }
    </style>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="RecordActionPanel" runat="server"
    ClientIDMode="Static">
    <div class="row padbottom10">
        <div class="col-xs-12">
            <div class="pull-right">
                <asp:LinkButton runat="server" ID="btnSearch" CssClass="btn btn-primary" Text="Search"><i class="fa fa-search"></i>&nbsp;&nbsp;Search</asp:LinkButton>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="DetailPageTitle" runat="server"
    ClientIDMode="Static">
    Survey Search
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="TabsPlaceHolder" runat="server"
    ClientIDMode="Static">
     <asp:UpdateProgress ID="UpdateProgress" runat="server" ClientIDMode="Static">
        <ProgressTemplate>
            <div class="overlay">
                <div class="overlayContent" style="color: #000; text-align: center; width: 250px; padding: 20px">
                    <i class="fa fa-2x fa-refresh fa-spin"></i>
                    <h3>Processing Request</h3>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <div id="divMain">
        <div class="row">
            <div class="col-xs-12">
                <div class="form-inline">
                    <asp:CheckBox ID="chkSurveyBet" runat="server" Text="Survey rating between" />&nbsp;&nbsp;
                <asp:TextBox ID="txtRatingStart" runat="Server" CssClass="form-control" MaxLength="7" Width="50"></asp:TextBox>
                    &nbsp;
                <b>to</b>
                    &nbsp;
                <asp:TextBox ID="txtRatingEnd" runat="Server" CssClass="form-control" MaxLength="7" Width="50"></asp:TextBox>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="form-inline">
                    <label>Select Survey Name:</label>
                    <asp:DropDownList Width="220" AutoPostBack="true" ID="ddlSurvey" runat="server" CssClass="form-control">
                    </asp:DropDownList>
                    &nbsp;&nbsp;
                Operator
                <asp:RadioButton runat="server" GroupName="Operator" CssClass="normal1" Text="And" ID="radAnd" />
                    <asp:RadioButton runat="server" GroupName="Operator" CssClass="normal1" Text="Or" ID="radOr" />
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="form-inline">
                    <label>Question List:</label>
                    <asp:DropDownList Width="220" AutoPostBack="true" ID="ddlQuestion1" runat="server" CssClass="form-control">
                    </asp:DropDownList>
                    <table runat="server" width="100%" class="normal1" id="tbQ1" style="margin-top: 10px; margin-bottom: 10px; margin-left:126px;">
                    </table>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="form-inline">
                    <label>Question List:</label>
                    <asp:DropDownList Width="220" AutoPostBack="true" ID="ddlQuestion2" runat="server" CssClass="form-control">
                    </asp:DropDownList>
                    <table runat="server" width="100%" class="normal1" id="tbQ2" style="margin-top: 10px; margin-bottom: 10px; margin-left:126px;">
                    </table>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="form-inline">
                    <label>Question List:</label>
                    <asp:DropDownList Width="220" AutoPostBack="true" ID="ddlQuestion3" runat="server" CssClass="form-control"></asp:DropDownList>
                    <table runat="server" width="100%" class="normal1" id="tbQ3" style="margin-top: 10px; margin-bottom: 10px; margin-left:126px;">
                    </table>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="form-inline">
                    <label>Question List:</label>
                    <asp:DropDownList Width="220" AutoPostBack="true" ID="ddlQuestion4" runat="server" CssClass="form-control"></asp:DropDownList>
                    <table runat="server" width="100%" class="normal1" id="tbQ4" style="margin-top: 10px; margin-bottom: 10px; margin-left:126px;">
                    </table>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="form-inline">
                    <label>Question List:</label>
                    <asp:DropDownList Width="220" AutoPostBack="true" ID="ddlQuestion5" runat="server" CssClass="form-control"></asp:DropDownList>
                    <table runat="server" width="100%" class="normal1" id="tbQ5" style="margin-top: 10px; margin-bottom: 10px; margin-left:126px;">
                    </table>
                </div>
            </div>
        </div>
    </div>
    <uc1:frmAdvSearchCriteria ID="frmAdvSearchCriteria1" runat="server" />
    <asp:HiddenField ID="hdnFilterCriteria" runat="server" />
</asp:Content>
