'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On

Namespace BACRM.UserInterface.Admin

    Partial Public Class frmUserGroups

        '''<summary>
        '''chkLayoutGridConfiguration control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents chkLayoutGridConfiguration As Global.System.Web.UI.WebControls.CheckBox

        '''<summary>
        '''RadToolTip1 control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents RadToolTip1 As Global.Telerik.Web.UI.RadToolTip

        '''<summary>
        '''lblTooltip control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents lblTooltip As Global.System.Web.UI.WebControls.Label

        '''<summary>
        '''litMessage control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents litMessage As Global.System.Web.UI.WebControls.Literal

        '''<summary>
        '''hdnGridLayoutIds control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents hdnGridLayoutIds As Global.System.Web.UI.WebControls.HiddenField

        '''<summary>
        '''UpdateProgress control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents UpdateProgress As Global.System.Web.UI.UpdateProgress

        '''<summary>
        '''rcbGroups control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents rcbGroups As Global.Telerik.Web.UI.RadComboBox

        '''<summary>
        '''litModuleName control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents litModuleName As Global.System.Web.UI.WebControls.Literal

        '''<summary>
        '''CheckAll control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents CheckAll As Global.System.Web.UI.WebControls.CheckBox

        '''<summary>
        '''Checkbox3 control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents Checkbox3 As Global.System.Web.UI.WebControls.CheckBox

        '''<summary>
        '''Checkbox7 control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents Checkbox7 As Global.System.Web.UI.WebControls.CheckBox

        '''<summary>
        '''Checkbox10 control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents Checkbox10 As Global.System.Web.UI.WebControls.CheckBox

        '''<summary>
        '''Checkbox14 control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents Checkbox14 As Global.System.Web.UI.WebControls.CheckBox

        '''<summary>
        '''Checkbox20 control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents Checkbox20 As Global.System.Web.UI.WebControls.CheckBox

        '''<summary>
        '''Checkbox21 control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents Checkbox21 As Global.System.Web.UI.WebControls.CheckBox

        '''<summary>
        '''Checkbox22 control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents Checkbox22 As Global.System.Web.UI.WebControls.CheckBox

        '''<summary>
        '''Checkbox23 control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents Checkbox23 As Global.System.Web.UI.WebControls.CheckBox

        '''<summary>
        '''Checkbox24 control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents Checkbox24 As Global.System.Web.UI.WebControls.CheckBox

        '''<summary>
        '''Checkbox25 control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents Checkbox25 As Global.System.Web.UI.WebControls.CheckBox

        '''<summary>
        '''Checkbox26 control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents Checkbox26 As Global.System.Web.UI.WebControls.CheckBox

        '''<summary>
        '''Checkbox27 control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents Checkbox27 As Global.System.Web.UI.WebControls.CheckBox

        '''<summary>
        '''Checkbox28 control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents Checkbox28 As Global.System.Web.UI.WebControls.CheckBox

        '''<summary>
        '''Checkbox29 control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents Checkbox29 As Global.System.Web.UI.WebControls.CheckBox

        '''<summary>
        '''Checkbox30 control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents Checkbox30 As Global.System.Web.UI.WebControls.CheckBox

        '''<summary>
        '''Checkbox31 control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents Checkbox31 As Global.System.Web.UI.WebControls.CheckBox

        '''<summary>
        '''Checkbox32 control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents Checkbox32 As Global.System.Web.UI.WebControls.CheckBox

        '''<summary>
        '''Checkbox33 control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents Checkbox33 As Global.System.Web.UI.WebControls.CheckBox

        '''<summary>
        '''Checkbox34 control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents Checkbox34 As Global.System.Web.UI.WebControls.CheckBox

        '''<summary>
        '''table7 control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents table7 As Global.System.Web.UI.WebControls.Table

        '''<summary>
        '''imgMainTab control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents imgMainTab As Global.System.Web.UI.WebControls.Image

        '''<summary>
        '''hplEditMainTabs control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents hplEditMainTabs As Global.System.Web.UI.WebControls.HyperLink

        '''<summary>
        '''Image1 control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents Image1 As Global.System.Web.UI.WebControls.Image

        '''<summary>
        '''hplHideMainTabs control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents hplHideMainTabs As Global.System.Web.UI.WebControls.HyperLink

        '''<summary>
        '''Image2 control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents Image2 As Global.System.Web.UI.WebControls.Image

        '''<summary>
        '''hplManageSubTabs control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents hplManageSubTabs As Global.System.Web.UI.WebControls.HyperLink

        '''<summary>
        '''Image4 control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents Image4 As Global.System.Web.UI.WebControls.Image

        '''<summary>
        '''hplManageTreeNodePerission control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents hplManageTreeNodePerission As Global.System.Web.UI.WebControls.HyperLink
    End Class
End Namespace
