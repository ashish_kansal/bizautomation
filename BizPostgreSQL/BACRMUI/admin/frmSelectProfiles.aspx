<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" Codebehind="frmSelectProfiles.aspx.vb" Inherits="BACRM.UserInterface.Admin.frmSelectProfiles"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
	<head  runat="server">
        <link rel="stylesheet" href="~/CSS/master.css" type="text/css" />
		<title> Select Profiles</title>
		<script language="javascript">
		function Close()
		{
			window.close()
			return false;
		}
		function Save()
		{
			if (document.Form1.ddlProfile.value==0)
			{
				alert("Select Profile ")
				document.Form1.ddlProfile.focus()
				return false;
			}
			if (document.Form1.ddlRelationship.value==0)
			{
				alert("Select Relationship")
				document.Form1.ddlRelationship.focus()
				return false;
			}
		}
		function ESave(a,b)
		{
		    if (document.getElementById(a).value == 0)
			{
				alert("Select Realtionship")
				document.getElementById(a).focus()
				return false;
			}
			if (document.getElementById(b).value == 0)
			{
				alert("Select Profile")
				document.getElementById(b).focus()
				return false;
			}
		}
		function DeleteRecord()
		{
			if(confirm('Are you sure, you want to delete the selected record?'))
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		function Sort()
		{
		    if (document.Form1.ddlRelationship.value==0)
		    {
		        alert('Select Relationship')
		    }
		    else
		    {
    			window.open("../admin/frmSortList.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&RelID="+document.Form1.ddlRelationship.value ,'','toolbar=no,titlebar=no,left=200, top=200,width=600,height=400,scrollbars=yes,resizable=yes')	    	
	    	}
	    		return false;
		}
		function GetList()
		{
		   document.Form1.Button1.click()
		}
		</script>
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<br>
			<table cellSpacing="0" cellPadding="0" width="100%" border="0">
				<tr>
					<td vAlign="bottom">
					<table class="TabStyle">
							<tr>
								<td>&nbsp;&nbsp;&nbsp;Configure Profiles&nbsp;&nbsp;&nbsp;
								</td>
							</tr>
						</table>
					</td>
					<td align="right"><asp:button id="btnClose" Text="Close" CssClass="button" Runat="server" Width="50"></asp:button></td>
				</tr>
			</table>
			<asp:table id="Table2" Runat="server" Height="300" GridLines="None" BorderColor="black" Width="100%" CssClass="aspTable" 
				BorderWidth="1" CellSpacing="0" CellPadding="0">
				<asp:TableRow>
					<asp:TableCell VerticalAlign="Top">
						<br>
						<table align="center">
				<tr>
					<td class="normal1" align="right">
						Profile
					</td>
					<td>
						<asp:DropDownList ID="ddlProfile" Runat="server" CssClass="signup"></asp:DropDownList>&nbsp;
						<asp:Button ID="btnAdd" Runat="server" Width="60" Text="Add" CssClass="button"></asp:Button>
					</td>
				</tr>
				<tr>
					<td class="normal1" align="right">
						Relationship
					</td>
					<td>
						<asp:DropDownList ID="ddlRelationship" AutoPostBack="True" Runat="server" CssClass="signup"></asp:DropDownList>&nbsp;
						<asp:button runat="server" ID="btnSort" Text="Sort List"  OnClientClick ="return Sort()" CssClass="button" />
					</td>
				</tr>
				<tr>
					<td>
						<br>
					</td>
				</tr>
			</table>
			<table width="100%" CellPadding="0" CellSpacing="0">
				<tr>
					<td colSpan="5">
						<asp:datagrid id="dgReProfile" runat="server" CssClass="dg"  AutoGenerateColumns="False"
							AllowSorting="True" Width="100%">
							<AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
							<ItemStyle CssClass="is"></ItemStyle>
							<HeaderStyle CssClass="hs"></HeaderStyle>
							<Columns>
								<asp:TemplateColumn>
									<ItemTemplate>
										<asp:LinkButton runat="server" Text="Edit" CommandName="Edit" ID="lnkbtnEdt"></asp:LinkButton>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:LinkButton ID="lnkbtnUpdt" runat="server" Text="Update" CommandName="Update"></asp:LinkButton>&nbsp;
										<asp:LinkButton runat="server" Text="Cancel" CommandName="Cancel" ID="lnkbtnCncl"></asp:LinkButton>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="No" Visible="false">
									<ItemTemplate>
										<asp:Label ID="numRelProID" Runat="server" Visible=False Text= '<%# DataBinder.Eval(Container,"DataItem.numRelProID") %>'>
										</asp:Label>
										<%# Container.ItemIndex +1 %>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Profile">
									<ItemTemplate>
										<asp:Label ID="lblProfile" Runat="server" Text= '<%# DataBinder.Eval(Container,"DataItem.ProName") %>'>
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:Label ID="lblEProfile" Runat="server" Visible=False Text= '<%# DataBinder.Eval(Container,"DataItem.numProfileID") %>'>
										</asp:Label>
										<asp:DropDownList ID="ddlEProfile" Runat="server" CssClass="signup"></asp:DropDownList>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Relationship">
									<ItemTemplate>
										<asp:Label ID="lblRel" Runat="server" Text= '<%# DataBinder.Eval(Container,"DataItem.RelName") %>'>
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:Label ID="lblERel" Runat="server" Visible=False Text= '<%# DataBinder.Eval(Container,"DataItem.numRelationshipID") %>'>
										</asp:Label>
										<asp:DropDownList ID="ddlERelationship" Runat="server" CssClass="signup"></asp:DropDownList>
									
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderStyle-Width="10" ItemStyle-Width="10">
									<HeaderTemplate>
										<asp:Button ID="btnHdelete" Runat="server" CssClass="button Delete" Text="X" ></asp:Button>
									</HeaderTemplate>
									<ItemTemplate>
										<asp:Button ID="btnDelete" Runat="server" CssClass="button Delete" Text="X" CommandName="Delete"></asp:Button>
									</ItemTemplate>
								</asp:TemplateColumn>
							</Columns>
						</asp:datagrid></td>
				</tr>
			</table>
					</asp:TableCell>
				</asp:TableRow>
			</asp:table>
			 <asp:Button ID="Button1"  runat="server"   style="display:none"/>
			<table width="100%">
				<tr>
					<td class="normal4" align="center"><asp:literal id="litMessage" Runat="server"></asp:literal></td>
				</tr>
			</table>
			
		</form>
	</body>
</HTML>
