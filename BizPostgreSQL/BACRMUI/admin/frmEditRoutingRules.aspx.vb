''' -----------------------------------------------------------------------------
''' Project	 : BACRM.UserInterface.Reports
''' Class	 : frmEditRoutingRules
''' 
''' -----------------------------------------------------------------------------
''' <summary>
'''     This is a interface for creation of Routing Rules parameters for Leads
''' </summary>
''' <remarks>
''' </remarks>
''' <history>
''' 	[Debasish]	08/21/2005	Created
''' </history>
''' ''' -----------------------------------------------------------------------------
Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Common
Namespace BACRM.UserInterface.Admin

    Public Class frmEditRoutingRules
        Inherits BACRMPage

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub
        Protected WithEvents tblRoutingRule As System.Web.UI.WebControls.Table
        Protected WithEvents txtRuleDesc As System.Web.UI.WebControls.TextBox
        Protected WithEvents txtIncludedText As System.Web.UI.WebControls.TextBox
        Protected WithEvents ddlAvailableFields As System.Web.UI.WebControls.DropDownList
        Protected WithEvents ddlAvailableFieldsOptions As System.Web.UI.WebControls.DropDownList
        Protected WithEvents ddlAOIList As System.Web.UI.WebControls.ListBox
        Protected WithEvents ddlAvailableIFFields As System.Web.UI.WebControls.ListBox
        Protected WithEvents ddlSelectedIFFields As System.Web.UI.WebControls.ListBox
        Protected WithEvents ddlSelectedAOIs As System.Web.UI.WebControls.ListBox
        Protected WithEvents ddlAvailableAOIs As System.Web.UI.WebControls.ListBox
        Protected WithEvents ddlDistributionList As System.Web.UI.WebControls.ListBox
        Protected WithEvents ddlSelectedEmployees As System.Web.UI.WebControls.ListBox
        Protected WithEvents ddlAvailableEmployees As System.Web.UI.WebControls.ListBox
        Protected WithEvents RadioConditionEQ As System.Web.UI.HtmlControls.HtmlInputRadioButton
        Protected WithEvents RadioConditionInText As System.Web.UI.HtmlControls.HtmlInputRadioButton
        Protected WithEvents RadioConditionInList As System.Web.UI.HtmlControls.HtmlInputRadioButton
        Protected WithEvents btnSave As System.Web.UI.WebControls.Button
        Protected WithEvents hdDistributionList As System.Web.UI.HtmlControls.HtmlInputHidden
        Protected WithEvents hdRoutingRuleChangeIndicator As System.Web.UI.HtmlControls.HtmlInputHidden
        Protected WithEvents hdAvailableIFFieldList As System.Web.UI.HtmlControls.HtmlInputHidden
        Protected WithEvents hdIFFieldSelected As System.Web.UI.HtmlControls.HtmlInputHidden
        Protected WithEvents hdAOIList As System.Web.UI.HtmlControls.HtmlInputHidden
        Protected WithEvents hdAOINameList As System.Web.UI.HtmlControls.HtmlInputHidden
        Protected WithEvents hdSave As System.Web.UI.HtmlControls.HtmlInputHidden
        Protected WithEvents plDynamicContentPlaceholder As System.Web.UI.WebControls.Literal
        Protected WithEvents litReferencedFields As System.Web.UI.WebControls.Literal
        Protected WithEvents trPriority As System.Web.UI.HtmlControls.HtmlTableRow

        Private numRoutID As Long = -1                                       'declare a variable to hold the Routing Rule Id
        Protected WithEvents ddlPriority As System.Web.UI.WebControls.DropDownList

        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.
        Private designerPlaceholderDeclaration As System.Object

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Load
            Try
                btnAdd.Attributes.Add("onclick", "return move(document.form1.lstAvailablefld,document.form1.lstAddfld)")
                btnRemove.Attributes.Add("onclick", "return remove(document.form1.lstAddfld,document.form1.lstAvailablefld)")
                'Put user code to initialize the page here
                If Request.QueryString.HasKeys() Then                               'If Querystring contains the Routing rule Id
                    numRoutID = CCommon.ToLong(GetQueryStringVal("numRoutID"))                    'get the routing rule id from the querystring
                End If
                trAOI.Visible = False
                trValues.Visible = False
                trEqualTo.Visible = False
                trCountry.Visible = False
                trState.Visible = False
                If numRoutID = -1 Then
                    trPriority.Visible = False
                Else : trPriority.Visible = True
                End If
                If Not IsPostBack Then
                    
                    FillRulesDetailsInForm()
                    btnSave.Attributes.Add("onclick", "return Save(0)")
                End If
                AttachAdditionalAttributes()                                        'calls to add additional attributes
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Sub FillDetails()
            Try
                If ddlAvailableFields.SelectedIndex > 0 Then
                    Dim str As String()
                    str = ddlAvailableFields.SelectedItem.Value.Split("~")
                    If str(3) = "L" Then
                        trEqualTo.Visible = True
                        ddlAvailableFieldsOptions.Items.Clear()

                        objCommon.sb_FillComboFromDB(ddlAvailableFieldsOptions, str(4), Session("DomainID"))
                        btnSave.Attributes.Add("onclick", "return Save(1)")
                    ElseIf str(3) = "S" Then
                        trCountry.Visible = True
                        trState.Visible = True
                        ddlCountry.Items.Clear()

                        objCommon.sb_FillComboFromDBwithSel(ddlCountry, 40, Session("DomainID"))
                        btnSave.Attributes.Add("onclick", "return Save(4)")
                        Dim iCount As Integer
                        For iCount = 0 To lstAddfld.Items.Count - 1
                            If Not lstAvailablefld.Items.FindByValue(lstAddfld.Items(iCount).Value) Is Nothing Then lstAvailablefld.Items.Remove(lstAddfld.Items(iCount))
                        Next
                    ElseIf str(3) = "AOI" Then
                        trAOI.Visible = True
                        btnSave.Attributes.Add("onclick", "return Save(3)")
                    Else
                        trValues.Visible = True
                        btnSave.Attributes.Add("onclick", "return Save(2)")
                    End If
                Else
                    trEqualTo.Visible = False
                    trAOI.Visible = False
                    trValues.Visible = False
                    trCountry.Visible = False
                    trState.Visible = False
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub FillRulesDetailsInForm()
            Try
                Dim objAutoRules As New AutoRoutingRules
                objAutoRules.DomainID = Session("DomainID")
                ddlAvailableFields.DataSource = objAutoRules.GetLeadBoxfields
                ddlAvailableFields.DataTextField = "vcFormFieldname"
                ddlAvailableFields.DataValueField = "ID"
                ddlAvailableFields.DataBind()
                ddlAvailableFields.Items.Insert(0, "--Select One--")
                ddlAvailableFields.Items.FindByText("--Select One--").Value = "0"


                objCommon.DomainID = Session("DomainID")
                objCommon.sb_FillListFromDB(ddlAvailableAOIs, 74, Session("DomainID"))
                objCommon.sb_FillConEmp(ddlAvailableEmployees, Session("DomainID"), 0, 0)
                If numRoutID > 0 Then
                    Dim dsDTLs As DataSet
                    objAutoRules.RoutingRuleID = numRoutID
                    dsDTLs = objAutoRules.GetAutoroutDTLs
                    Dim dtTable As DataTable
                    dtTable = dsDTLs.Tables(0)
                    txtRuleDesc.Text = dtTable.Rows(0).Item("vcRoutName")
                    Dim i As Integer
                    For i = 0 To dtTable.Rows(0).Item("tintPriority") + 1
                        ddlPriority.Items.Insert(i, i + 1)
                    Next

                    ddlPriority.Items.FindByValue(dtTable.Rows(0).Item("tintPriority")).Selected = True
                    If Not IsDBNull(dtTable.Rows(0).Item("vcSelectedCoulmn")) Then
                        If Not ddlAvailableFields.Items.FindByValue(dtTable.Rows(0).Item("vcSelectedCoulmn")) Is Nothing Then
                            ddlAvailableFields.Items.FindByValue(dtTable.Rows(0).Item("vcSelectedCoulmn")).Selected = True
                        End If
                    End If

                    FillDetails()
                    If dtTable.Rows(0).Item("tintEqualTo") = 1 Then
                        If dtTable.Rows(0).Item("vcDBColumnName").IndexOf("State") > -1 Then
                            lstAddfld.DataSource = dsDTLs.Tables(2)
                            lstAddfld.DataTextField = "vcState"
                            lstAddfld.DataValueField = "numStateID"
                            lstAddfld.DataBind()
                            Dim iCount As Integer
                            For iCount = 0 To lstAddfld.Items.Count - 1
                                If Not lstAvailablefld.Items.FindByValue(lstAddfld.Items(iCount).Value) Is Nothing Then
                                    lstAvailablefld.Items.Remove(lstAddfld.Items(iCount))
                                End If
                            Next
                        Else
                            If Not IsDBNull(dtTable.Rows(0).Item("numValue")) Then
                                If Not ddlAvailableFieldsOptions.Items.FindByValue(dtTable.Rows(0).Item("numValue")) Is Nothing Then
                                    ddlAvailableFieldsOptions.Items.FindByValue(dtTable.Rows(0).Item("numValue")).Selected = True
                                End If
                            End If
                        End If
                    ElseIf dtTable.Rows(0).Item("tintEqualTo") = 2 Then
                        Dim dtValues As DataTable
                        dtValues = dsDTLs.Tables(2)
                        Dim strText As String = ""
                        For i = 0 To dtValues.Rows.Count - 1
                            strText = strText & dtValues.Rows(i).Item("vcValue") & ","
                        Next
                        txtIncludedText.Text = strText.TrimEnd(",")
                    ElseIf dtTable.Rows(0).Item("tintEqualTo") = 3 Then
                        ddlAOIList.DataSource = dsDTLs.Tables(2)
                        ddlAOIList.DataTextField = "vcAOIName"
                        ddlAOIList.DataValueField = "vcValue"
                        ddlAOIList.DataBind()
                    End If
                    ddlDistributionList.DataSource = dsDTLs.Tables(1)
                    ddlDistributionList.DataTextField = "Name"
                    ddlDistributionList.DataValueField = "numContactId"
                    ddlDistributionList.DataBind()
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Public Sub ddlAvailableFields_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ddlAvailableFields.SelectedIndexChanged
            Try
                FillDetails()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
            Try
                If ddlAvailableFields.SelectedIndex > 0 Then
                    Dim objAutoRoutRules As New AutoRoutingRules
                    Dim str As String()
                    str = ddlAvailableFields.SelectedItem.Value.Split("~")
                    Dim dtTableValues As New DataTable
                    Dim dr As DataRow
                    Dim ds As New DataSet
                    Dim i As Integer
                    Dim strV As String()
                    objAutoRoutRules.vcSelectedField = ddlAvailableFields.SelectedItem.Value
                    objAutoRoutRules.vcDBCoulmnName = str(2)
                    If str("3") = "AOI" Then
                        dtTableValues.Columns.Add("vcValue")
                        strV = txtAOI.Value.Split(",")
                        For i = 0 To strV.Length - 2
                            dr = dtTableValues.NewRow
                            dr("vcValue") = strV(i)
                            dtTableValues.Rows.Add(dr)
                        Next
                        dtTableValues.TableName = "Table"
                        ds.Tables.Add(dtTableValues.Copy)
                        objAutoRoutRules.strValues = ds.GetXml
                        ds.Tables.Remove(ds.Tables(0))
                        objAutoRoutRules.tintEqualTo = 3
                    ElseIf str("3") = "L" Then
                        objAutoRoutRules.tintEqualTo = 1
                        objAutoRoutRules.SelValue = ddlAvailableFieldsOptions.SelectedItem.Value
                    ElseIf str("3") = "S" Then
                        objAutoRoutRules.tintEqualTo = 1
                        dtTableValues.Columns.Add("vcValue")
                        strV = txtState.Value.Split(",")
                        For i = 0 To strV.Length - 2
                            dr = dtTableValues.NewRow
                            dr("vcValue") = strV(i)
                            dtTableValues.Rows.Add(dr)
                        Next
                        dtTableValues.TableName = "Table"
                        ds.Tables.Add(dtTableValues.Copy)
                        objAutoRoutRules.strValues = ds.GetXml
                        ds.Tables.Remove(ds.Tables(0))
                    Else
                        dtTableValues.Columns.Add("vcValue")
                        strV = txtIncludedText.Text.Split(",")
                        For i = 0 To strV.Length - 1
                            dr = dtTableValues.NewRow
                            dr("vcValue") = strV(i)
                            dtTableValues.Rows.Add(dr)
                        Next
                        dtTableValues.TableName = "Table"
                        ds.Tables.Add(dtTableValues.Copy)
                        objAutoRoutRules.strValues = ds.GetXml
                        ds.Tables.Remove(ds.Tables(0))
                        objAutoRoutRules.tintEqualTo = 2
                    End If

                    Dim dtTable As New DataTable
                    dtTable.Columns.Add("numEmpId")
                    dtTable.Columns.Add("intAssignOrder")
                    strV = txtEmp.Value.Split(",")
                    For i = 0 To strV.Length - 2
                        dr = dtTable.NewRow
                        dr("numEmpId") = strV(i)
                        dr("intAssignOrder") = i + 1
                        dtTable.Rows.Add(dr)
                    Next

                    dtTable.TableName = "Table"
                    ds.Tables.Add(dtTable.Copy)
                    objAutoRoutRules.strDistributionList = ds.GetXml
                    ds.Tables.Remove(ds.Tables(0))

                    objAutoRoutRules.DomainID = Session("DomainID")
                    objAutoRoutRules.UserCntID = Session("UserContactID")

                    objAutoRoutRules.vcRoutName = txtRuleDesc.Text
                    If numRoutID = -1 Then
                        objAutoRoutRules.Priority = 0
                        numRoutID = objAutoRoutRules.InsertRoutingRule()
                    Else
                        objAutoRoutRules.Priority = ddlPriority.SelectedItem.Value
                        objAutoRoutRules.RoutingRuleID = numRoutID
                        objAutoRoutRules.UpdateRoutingRule()
                    End If
                    Response.Redirect("../admin/frmEditRoutingRules.aspx?numRoutID=" & numRoutID)
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Sub AttachAdditionalAttributes()
            Try
                ddlAvailableFields.Attributes.Add("onchange", "javascript: SetSelectedAvailableIFField(document.form1);")                                 'Adds onchange event to the drop down of If Fields
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub ddlCountry_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCountry.SelectedIndexChanged
            Try
                FillState(lstAvailablefld, ddlCountry.SelectedItem.Value, Session("DomainID"))
                trCountry.Visible = True
                trState.Visible = True
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

    End Class
End Namespace
