<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" CodeBehind="frmComAssociationTo.aspx.vb"
    Inherits="BACRM.UserInterface.Admin.frmComAssociationTo" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link rel="stylesheet" href="~/CSS/master.css" type="text/css" />
    <title>Organization Association To</title>
    <link rel="stylesheet" href="~/CSS/NewUiStyle.css" type="text/css" />
    <script src="../JavaScript/jquery.min.js" type="text/javascript"></script>
    <script src="../JavaScript/jquery-ui.min.js" type="text/javascript"></script>
    <script src="../JavaScript/jquery.Tooltip.js" type="text/javascript"></script>
    <script type="text/javascript" src="../JavaScript/jquery.validate.custom-script.js"></script>
    <script type="text/javascript" src="../JavaScript/jquery.validate.min.js"></script>
    <script type="text/javascript" src="../JavaScript/Common.js"></script>
    <script type="text/javascript">
        function pageLoad(sender, args) {
            initComboExtensions();
            cbCompany = $find("radCmbCompany");
            if (cbCompany != null) {
                cbCompany.onDataBound = cb_onDataBoundOrganization;
            }
        }
        function OpenEmail(a, b) {

            window.open('../contact/frmComposeWindow.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&Lsemail=' + a + '&pqwRT=' + b, 'ComposeWindow', 'toolbar=no,titlebar=no,top=100,left=100,width=1200,height=645,scrollbars=yes,resizable=yes')
            return false;
        }
        function DeleteRecord() {
            return confirm('Are you sure, you want to delete the selected association?');
        }
        function Save() {
            if ($find('radCmbCompany').get_value() == '') {
                alert("Select Organization");
                return false;
            }
            //if (document.form1.ddlContacts.value == 0) {
            //    alert("Please Select Contact");
            //    document.form1.ddlContacts.focus();
            //    return false;
            //}
            //if (document.form1.ddlType.value == 0) {
            //    alert("Please Select Association");
            //    document.form1.ddlType.focus();
            //    return false;
            //}
            if ($find('radAssociationType').get_value() == '' || $find('radAssociationType').get_value() == '0') {
                alert("Select Association Type");
                return false;
            }
            //if (document.form1.chkAssociationLabel.checked == true) {
            //    if (document.form1.ddlType1.value == 0) {
            //        alert("Please Select Association Label");
            //        document.form1.ddlType1.focus();
            //        return false;
            //    }
            //}
        }

        function editAssociation(event,id) {
            event.preventDefault();

            document.getElementById('hdnAssID').value = id;
            document.getElementById('btnEdit').click();
        }

        function OnClientSelectedIndexChanged(sender, eventArgs) {
            document.getElementById('hdnAssID').value = '';
        }
    </script>
    <script type="text/javascript" src="../javascript/comboClientSide.js"></script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePartialRendering="true">
        </asp:ScriptManager>
        <asp:Table ID="tblAssocationToHeader" Width="100%" runat="server" BorderColor="black"
            GridLines="None" CssClass="aspTable" BorderWidth="0" CellPadding="2" CellSpacing="0"
            Height="400">
            <asp:TableRow>
                <asp:TableCell VerticalAlign="Top">
                    <table width="100%" border="0">
                        <tr>
                            <td>
                                <telerik:RadComboBox AccessKey="C" ID="radCmbCompany" Width="250px" DropDownWidth="600px"
                                    OnClientItemsRequested="OnClientItemsRequestedOrganization"
                                    OnClientSelectedIndexChanged="OnClientSelectedIndexChanged"
                                    ClientIDMode="Static"
                                    ShowMoreResultsBox="true"
                                    Height="200"
                                    Skin="Default" runat="server"  AllowCustomText="True" EnableLoadOnDemand="True" EmptyMessage="Type first few letter of Organization">
                                    <WebServiceSettings Path="../common/Common.asmx" Method="GetCompanies" />
                                </telerik:RadComboBox>
                                is a
                                <%--<asp:DropDownList ID="ddlType" Width="180" runat="server" CssClass="signup">
                                </asp:DropDownList>--%>
                                <telerik:RadComboBox ID="radAssociationType" runat="server" CheckBoxes="false">
                                </telerik:RadComboBox>
                                AND
                                <asp:CheckBox runat="server" ID="chkChild" CssClass="signup" Text="is child company of selected organization" />
                                <span class="tip" title="Selecting this check box, will make the Organization show up in the Accounts Receivable as a child (So you can receive payments issued by the parent company on behalf of the child company � from a single location)">[?]</span>
                                &nbsp;&nbsp;&nbsp;<asp:ImageButton ID="imgAddAssociation" runat="server" ImageUrl="~/images/AddRecord.png" OnClientClick="return Save();"></asp:ImageButton>&nbsp;
                                <asp:Button ID="btnDelete" CausesValidation="false" CssClass="button Delete" Text="X" runat="server" OnClientClick=""></asp:Button>
                                <asp:Button ID="btnEdit" CausesValidation="false" CssClass="button Delete" Text="edit" runat="server" OnClientClick="" style="display:none;"></asp:Button>
                            </td>
                            <%-- <td class="text" align="right">Contact<font color="#ff3333"> *</font>
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlContacts" runat="server" Width="200px" CssClass="signup">
                                </asp:DropDownList>
                            </td>
                            <td class="normal1" align="right">Association Type<font color="#ff3333"> *</font>
                            </td>
                            <td>
                                
                            </td>--%>
                        </tr>
                        <%--<tr>
                            <td colspan="5">
                                <asp:CheckBox runat="server" ID="chkAssociationLabel" CssClass="signup" Text="Display link to selected organization (from this organization) with the following association label " />
                                <asp:DropDownList ID="ddlType1" Width="180" runat="server" CssClass="signup">
                                </asp:DropDownList>
                                &nbsp;<asp:CheckBox runat="server" ID="chkHeaderLink" CssClass="signup" Text="Add header link from here too" />
                            </td>
                            <td class="normal1" align="right">
                                <asp:Button ID="btnAdd" CssClass="button" Width="50" Text="Add" runat="server" OnClientClick="return Save();"></asp:Button>&nbsp;
                            <asp:Button ID="btnDelete" CausesValidation="false" CssClass="button Delete" Text="X" runat="server" OnClientClick=""></asp:Button>&nbsp;
                            </td>
                        </tr>--%>
                    </table>
                    <table>
                        <tr>
                            <td>
                                <telerik:RadTreeView ID="uwTree" runat="server" AllowNodeEditing="false" ClientIDMode="Static">
                                </telerik:RadTreeView>
                            </td>
                        </tr>
                    </table>
                </asp:TableCell>
            </asp:TableRow>
        </asp:Table>
        <table width="100%">
            <tr>
                <td class="normal4" align="center">
                    <asp:Literal ID="litMessage" runat="server" EnableViewState="False"></asp:Literal>
                </td>
            </tr>
        </table>
        <asp:HiddenField ID="hdnAssID" runat="server" />
    </form>
    <script type="text/javascript">
        // select all desired input fields and attach tooltips to them
        $("#form1").find("span.tip").tooltip({

            // place tooltip on the right edge
            position: "center right",

            // a little tweaking of the position
            offset: [-2, 10],

            // use the built-in fadeIn/fadeOut effect
            effect: "fade",

            // custom opacity setting
            opacity: 0.8

        });
    </script>
</body>
</html>
