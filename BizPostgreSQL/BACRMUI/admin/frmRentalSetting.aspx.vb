﻿Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Common
Namespace BACRM.UserInterface.Admin
    Public Class frmRentalSetting
        Inherits BACRMPage

        Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            Try
                If Not IsPostBack Then
                    BindDropDowns()
                    LoadDomainDetails()

                End If
            Catch ex As Exception

            End Try
        End Sub
        Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
            Try
                Dim objUserAccess As New UserAccess
                objUserAccess.DomainID = Session("DomainID")
                objUserAccess.boolRentalItem = chkRental.Checked
                objUserAccess.RentalItemClass = ddlRentalItemClass.SelectedValue
                objUserAccess.RentalHourlyUOM = ddlRentalHourlyUOM.SelectedValue
                objUserAccess.RentalDailyUOM = ddlRentalDailyUOM.SelectedValue
                objUserAccess.RentalPriceBasedOn = rblRentalPriceBasedOn.SelectedValue
                objUserAccess.UpdateRentalSetting()
                Session("bitRentalItem") = chkRental.Checked
                Session("RentalItemClass") = ddlRentalItemClass.SelectedValue
                Session("RentalHourlyUOM") = ddlRentalHourlyUOM.SelectedValue
                Session("RentalDailyUOM") = ddlRentalDailyUOM.SelectedValue
                Session("RentalPriceBasedOn") = rblRentalPriceBasedOn.SelectedValue

            Catch ex As Exception

            End Try

        End Sub
        Sub LoadDomainDetails()
            Try
                Dim dtTable As DataTable
                Dim objUserAccess As New UserAccess
                objUserAccess.DomainID = Session("DomainID")
                dtTable = objUserAccess.GetDomainDetails()
                Dim strPassword As String
                Dim strDomainId As String = objCommon.Encrypt(Session("DomainId"))
                If dtTable.Rows.Count > 0 Then
                    chkRental.Checked = dtTable.Rows(0).Item("bitRentalItem")
                    If Not ddlRentalItemClass.Items.FindByValue(dtTable.Rows(0).Item("numRentalItemClass")) Is Nothing Then
                        ddlRentalItemClass.ClearSelection()
                        ddlRentalItemClass.Items.FindByValue(dtTable.Rows(0).Item("numRentalItemClass")).Selected = True
                    End If

                    If Not ddlRentalHourlyUOM.Items.FindByValue(dtTable.Rows(0).Item("numRentalHourlyUOM")) Is Nothing Then
                        ddlRentalHourlyUOM.ClearSelection()
                        ddlRentalHourlyUOM.Items.FindByValue(dtTable.Rows(0).Item("numRentalHourlyUOM")).Selected = True
                    End If

                    If Not ddlRentalDailyUOM.Items.FindByValue(dtTable.Rows(0).Item("numRentalDailyUOM")) Is Nothing Then
                        ddlRentalDailyUOM.ClearSelection()
                        ddlRentalDailyUOM.Items.FindByValue(dtTable.Rows(0).Item("numRentalDailyUOM")).Selected = True
                    End If

                    If Not rblRentalPriceBasedOn.Items.FindByValue(dtTable.Rows(0).Item("tintRentalPriceBasedOn")) Is Nothing Then
                        rblRentalPriceBasedOn.ClearSelection()
                        rblRentalPriceBasedOn.Items.FindByValue(dtTable.Rows(0).Item("tintRentalPriceBasedOn")).Selected = True
                    End If
                End If
            Catch ex As Exception

            End Try
        End Sub
        Sub BindDropDowns()
            objCommon.sb_FillComboFromDBwithSel(ddlRentalItemClass, 36, Session("DomainID")) ''tem Classification
            Dim dtUnit As DataTable

            objCommon.DomainID = Session("DomainID")
            objCommon.UOMAll = True
            dtUnit = objCommon.GetItemUOM()

            With ddlRentalDailyUOM
                .DataSource = dtUnit
                .DataTextField = "vcUnitName"
                .DataValueField = "numUOMId"
                .DataBind()
                .Items.Insert(0, "--Select One--")
                .Items.FindByText("--Select One--").Value = "0"
            End With

            With ddlRentalHourlyUOM
                .DataSource = dtUnit
                .DataTextField = "vcUnitName"
                .DataValueField = "numUOMId"
                .DataBind()
                .Items.Insert(0, "--Select One--")
                .Items.FindByText("--Select One--").Value = "0"
            End With
        End Sub
    End Class

End Namespace