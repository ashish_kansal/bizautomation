''Modified by anoop jayaraj
Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Common
Imports System.IO
Namespace BACRM.UserInterface.Admin
    Public Class frmAdvSearchMassUpdater
        Inherits BACRMPage
        Dim lngFormID As Long
#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub
        Protected WithEvents btnSave As System.Web.UI.WebControls.Button
        Protected WithEvents lstAvailablefld As System.Web.UI.WebControls.DropDownList
        Protected WithEvents tblAdvSearchMassUpdater As System.Web.UI.WebControls.Table
        Protected WithEvents litArrayString As System.Web.UI.WebControls.Literal
        Protected WithEvents hdOrigDbColumnName As System.Web.UI.HtmlControls.HtmlInputHidden
        Protected WithEvents hdLookBackTableName As System.Web.UI.HtmlControls.HtmlInputHidden
        Protected WithEvents hdColumnValue As System.Web.UI.HtmlControls.HtmlInputHidden
        Protected WithEvents hdEntityIdRefList As System.Web.UI.HtmlControls.HtmlInputHidden
        Protected WithEvents hdDbColumnListId As System.Web.UI.HtmlControls.HtmlInputHidden
        Protected WithEvents txtValue As System.Web.UI.WebControls.TextBox
        Protected WithEvents ddlValue As System.Web.UI.WebControls.DropDownList
        Protected WithEvents cbValue As System.Web.UI.WebControls.CheckBox
        Protected WithEvents hdDbAssociatedControl As System.Web.UI.HtmlControls.HtmlInputHidden
        Protected WithEvents rowValue As System.Web.UI.HtmlControls.HtmlTableRow
        Protected WithEvents litClientMessage As System.Web.UI.WebControls.Literal
        Protected WithEvents lblFieldName As System.Web.UI.WebControls.Label
        Protected WithEvents hdDbColumnListItemType As System.Web.UI.HtmlControls.HtmlInputHidden

        Dim objGenericAdvSearch As FormGenericAdvSearch                      'Create an object of the class FormGenericAdvSearch
        Protected WithEvents hdFieldDataType As System.Web.UI.HtmlControls.HtmlInputHidden
        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.
        Private designerPlaceholderDeclaration As System.Object

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            Try
                'CODEGEN: This method call is required by the Web Form Designer
                'Do not modify it using the code editor.
                InitializeComponent()
                CCommon.InitializeOrganizationClientSideTemplate(Session("DomainID"), Session("UserContactID"), radCmbCompany)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

#End Region


        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This event is fired eachtime the page is called.
        ''' </summary>
        ''' <param name="sender">Represents the sender object.</param>
        ''' <param name="e">Represents the EventArgs.</param>
        ''' <remarks>
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	18/03/2006	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Try
                'Put user code to initialize the page here
                lngFormID = CCommon.ToInteger(GetQueryStringVal("FormID"))
                If Not IsPostBack Then DataBindElements() 'Calls the subroutine to databind the listboxes to dataset
                
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This method is used to databind the listboxes/ radionbuttonlist to the dataset
        ''' </summary>
        ''' <remarks>
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	09/07/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Sub DataBindElements()
            Try
                '================================================================================
                ' Purpose: Calls the subroutine to databind the Available fields and Selected Fields listbox to dataset
                '
                ' History
                ' Ver   Date        Ref     Author              Reason Created
                ' 1     08/12/2005          Debasish Nag        Calls the subroutine to databind 
                '                                               the listbox to dataset
                '================================================================================
                objGenericAdvSearch = New FormGenericAdvSearch                          'Create an object of the class FormGenericAdvSearch
                objGenericAdvSearch.DomainID = Session("DomainID")                      'Set the Domain Id
                objGenericAdvSearch.FormID = lngFormID
                Dim dtEditableFieldList As DataTable = objGenericAdvSearch.getUpdatableFieldList() 'Declare a DataTable and Call to get the columns for editing

                lstAvailablefld.DataSource = dtEditableFieldList.DefaultView            'Set the datasource for the available fields
                lstAvailablefld.DataTextField = "vcFormFieldName"                       'set the text field
                lstAvailablefld.DataValueField = "Value"                   'set the value attribut
                lstAvailablefld.DataBind()                                              'Databind the fields
                lstAvailablefld.Items.Insert(0, New ListItem("--Select One--", "None"))  'The first item is 'Select One'
                rowValue.Visible = False                                                'This row of values is hidden
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This method is used to handle saving of the value after the 
        '''     "Save" button is clicked
        ''' </summary>
        ''' <param name="sender">Represents the sender object.</param>
        ''' <param name="e">Represents the EventArgs.</param>
        ''' <remarks>
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	18/03/2006	Created
        ''' </history>
        '''  -----------------------------------------------------------------------------
        Dim value As String
        Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
            Try
                If CStr(lstAvailablefld.SelectedItem.Value) <> "None" Then
                    Dim str As String()
                    Dim shortAddressType As Short
                    Dim shortAddressOf As Short

                    str = lstAvailablefld.SelectedItem.Value.Split("~")
                    hdDbAssociatedControl.Value = str(5)
                    hdDbColumnListItemType.Value = str(2)
                    hdDbColumnListId.Value = str(3)
                    Session("LookTable") = str(1)
                    value = txtValue.Text.Trim.Replace("'", "''")

                    If str(0) = "numCategoryID" Then
                        Session("UpdateQuery") = "SELECT USP_ItemCategory_MassUpdate(" & CCommon.ToLong(Session("DomainID")) & "," & If(rbSubscribe.Checked, 1, 2) & "," & CCommon.ToLong(ddlCategoryProfile.SelectedValue) & ",'##ItemCodes##'" & ",'" & GetSelectedCategory() & "');"
                    ElseIf str(0) = "vcManufacturer" Then
                        Session("UpdateQuery") = "update Item set " & str(0) & " = '" & value & "', numManufacturer = COALESCE((SELECT numDivisionID FROM CompanyInfo INNER JOIN DivisionMaster ON CompanyInfo.numCompanyId=DivisionMaster.numCompanyID WHERE CompanyInfo.numDOmainID=" & Session("DomainID") & " AND LOWER(vcCompanyName) = LOWER('" & value & "') LIMIT 1),0) where numDomainID = " & Session("DomainID") & " and numItemCode in (@$replace)"
                    ElseIf hdDbAssociatedControl.Value = "TextBox" Then                             'EditBox
                        If str(0) = "Custom" Then
                            If str(1) = "DivisionMaster" Then
                                Session("UpdateQuery") = "delete from CFW_FLD_Values where Fld_ID=" & lstAvailablefld.SelectedItem.Value.Split("~")(6) & " and RecId in (select @$replace   insert into  CFW_FLD_Values (Fld_ID,Fld_Value,RecId)  (select " & lstAvailablefld.SelectedItem.Value.Split("~")(6) & ",'" & txtValue.Text.Trim & "',@$replace "
                            ElseIf str(1) = "AdditionalContactsInformation" Then
                                Session("UpdateQuery") = "delete from CFW_FLD_Values_Cont where Fld_ID=" & lstAvailablefld.SelectedItem.Value.Split("~")(6) & " and RecId in (select @$replace   insert into  CFW_FLD_Values_Cont (Fld_ID,Fld_Value,RecId)  (select " & lstAvailablefld.SelectedItem.Value.Split("~")(6) & ",'" & txtValue.Text.Trim & "',@$replace "
                            End If
                        Else
                            If str(1) = "DivisionMaster" Then
                                Session("UpdateQuery") = "update DivisionMaster set " & str(0) & " = '" & value & "' where numDomainID= " & CCommon.ToLong(Session("DomainID")) & " and numDivisionID in (select  @$replace"
                            ElseIf str(1) = "AdditionalContactsInformation" Then
                                Session("UpdateQuery") = "update AdditionalContactsInformation set " & str(0) & " = '" & value & "' where numDomainID= " & CCommon.ToLong(Session("DomainID")) & " and numContactID in (select  @$replace"
                            ElseIf str(1) = "CompanyInfo" Then
                                Session("UpdateQuery") = "update CompanyInfo set " & str(0) & " = '" & value & "' where numDomainID= " & CCommon.ToLong(Session("DomainID")) & " and numCompanyID in (select  @$replace"
                            ElseIf str(1) = "AddressDetails" Then
                                SetAddressDetailsParameter(str, shortAddressType, shortAddressOf)
                                Session("UpdateQuery") = "update AddressDetails set " & str(0) & " = '" & value & "' where numDomainID= " & Session("DomainID") & " and tintAddressOf=" & shortAddressOf.ToString & " AND tintAddressType=" & shortAddressType.ToString & " AND bitIsPrimary=1 and numRecordID in (select  @$replace"
                            ElseIf str(1) = "Item" Then
                                Session("UpdateQuery") = "update Item set " & str(0) & " = '" & value & "' where numDomainID = " & Session("DomainID") & " and numItemCode in (@$replace)"
                            ElseIf str(1) = "WareHouseItems" Then
                                Session("UpdateQuery") = "update WarehouseItems set " & str(0) & " = '" & value & "' where numDomainID = " & Session("DomainID") & " and numItemID in (@$replace)"
                            ElseIf str(1) = "Vendor" Then
                                If Session("VendorIDs") = Nothing OrElse CCommon.ToString(Session("VendorIDs")).Length = 0 Then
                                    Session("UpdateQuery") = "update Vendor set " & str(0) & " = '" & value & "' where numDomainID = " & Session("DomainID") & " and numItemCode in (@$replace)"
                                Else
                                    Session("UpdateQuery") = "update Vendor set " & str(0) & " = '" & value & "' where numDomainID = " & Session("DomainID") & If(Session("VendorIDs") Is Nothing, "", " AND numVendorID IN (" & Session("VendorIDs") & ")") & " and numItemCode in (@$replace)"
                                End If
                            End If
                        End If
                    ElseIf hdDbAssociatedControl.Value = "CheckBox" Then                        'CheckBox
                        If str(0) = "Custom" Then
                            If str(1) = "DivisionMaster" Then
                                Session("UpdateQuery") = "delete from CFW_FLD_Values where Fld_ID=" & lstAvailablefld.SelectedItem.Value.Split("~")(6) & " and RecId in (select @$replace   insert into  CFW_FLD_Values (Fld_ID,Fld_Value,RecId)  (select " & lstAvailablefld.SelectedItem.Value.Split("~")(6) & ",'" & IIf(cbValue.Checked, 1, 0) & "',@$replace "
                            ElseIf str(1) = "AdditionalContactsInformation" Then
                                Session("UpdateQuery") = "delete from CFW_FLD_Values_Cont where Fld_ID=" & lstAvailablefld.SelectedItem.Value.Split("~")(6) & " and RecId in (select @$replace   insert into  CFW_FLD_Values_Cont (Fld_ID,Fld_Value,RecId)  (select " & lstAvailablefld.SelectedItem.Value.Split("~")(6) & ",'" & IIf(cbValue.Checked, 1, 0) & "',@$replace "
                            End If
                        Else
                            If str(1) = "DivisionMaster" Then
                                Session("UpdateQuery") = "update DivisionMaster set " & str(0) & " = " & IIf(cbValue.Checked, 1, 0) & " where numDomainID= " & CCommon.ToLong(Session("DomainID")) & " and numDivisionID in (select  @$replace"
                            ElseIf str(1) = "AdditionalContactsInformation" Then
                                Session("UpdateQuery") = "update AdditionalContactsInformation set " & str(0) & " = " & IIf(cbValue.Checked, 1, 0) & " where numDomainID= " & CCommon.ToLong(Session("DomainID")) & " and numContactID in (select  @$replace"
                            ElseIf str(1) = "CompanyInfo" Then
                                Session("UpdateQuery") = "update CompanyInfo set " & str(0) & " = " & IIf(cbValue.Checked, 1, 0) & " where numDomainID= " & CCommon.ToLong(Session("DomainID")) & " and numCompanyID in (select  @$replace"
                            ElseIf str(1) = "Item" Then
                                Session("UpdateQuery") = "update Item set " & str(0) & " = " & IIf(cbValue.Checked, 1, 0) & " where numDomainID = " & Session("DomainID") & " and numItemCode in (@$replace)"
                                If str(0) = "bitTaxable" Then
                                    Session("UpdateQuery") = Session("UpdateQuery") + "; DELETE FROM ItemTax WHERE numItemCode IN (@$replace) AND numTaxItemID = 0 ;   "
                                    If cbValue.Checked Then
                                        Session("UpdateQuery") = Session("UpdateQuery") + "; INSERT INTO ItemTax(numItemCode,numTaxItemID,bitApplicable) SELECT Id,0,true FROM SplitIDs('@$replace',',');"
                                    End If
                                End If
                            ElseIf str(1) = "Tax" Then
                                Session("UpdateQuery") = "delete from ItemTax where numTaxItemID=" & lstAvailablefld.SelectedItem.Value.Split("~")(6) & " and numItemCode in (select @$replace    insert into ItemTax (numItemCode,numTaxItemID,bitApplicable)  (select @$replace," & lstAvailablefld.SelectedItem.Value.Split("~")(6) & ",1 "
                            End If
                        End If
                    ElseIf hdDbAssociatedControl.Value = "TextArea" Then
                        If str(0) = "Custom" Then
                            If str(1) = "DivisionMaster" Then
                                Session("UpdateQuery") = "delete from CFW_FLD_Values where Fld_ID=" & lstAvailablefld.SelectedItem.Value.Split("~")(6) & " and RecId in (select @$replace   insert into  CFW_FLD_Values (Fld_ID,Fld_Value,RecId)  (select " & lstAvailablefld.SelectedItem.Value.Split("~")(6) & ",'" & value & "',@$replace "
                            ElseIf str(1) = "AdditionalContactsInformation" Then
                                Session("UpdateQuery") = "delete from CFW_FLD_Values_Cont where Fld_ID=" & lstAvailablefld.SelectedItem.Value.Split("~")(6) & " and RecId in (select @$replace   insert into  CFW_FLD_Values_Cont (Fld_ID,Fld_Value,RecId)  (select " & lstAvailablefld.SelectedItem.Value.Split("~")(6) & ",'" & value & "',@$replace "
                            End If
                        Else
                            If str(1) = "DivisionMaster" Then
                                Session("UpdateQuery") = "update DivisionMaster set " & str(0) & " = '" & value & "' where numDomainID= " & CCommon.ToLong(Session("DomainID")) & " and numDivisionID in (select  @$replace"
                            ElseIf str(1) = "AdditionalContactsInformation" Then
                                Session("UpdateQuery") = "update AdditionalContactsInformation set " & str(0) & " = '" & value & "' where numDomainID= " & CCommon.ToLong(Session("DomainID")) & " and numContactID in (select  @$replace"
                            ElseIf str(1) = "CompanyInfo" Then
                                Session("UpdateQuery") = "update CompanyInfo set " & str(0) & " = '" & value & "' where numDomainID= " & CCommon.ToLong(Session("DomainID")) & " and numCompanyID in (select  @$replace"
                            ElseIf str(1) = "AddressDetails" Then
                                SetAddressDetailsParameter(str, shortAddressType, shortAddressOf)
                                Session("UpdateQuery") = "update AddressDetails set " & str(0) & " = '" & value & "' where numDomainID= " & Session("DomainID") & " and tintAddressOf=" & shortAddressOf.ToString & " AND tintAddressType=" & shortAddressType.ToString & " AND bitIsPrimary=1 and numRecordID in (select  @$replace"
                            ElseIf str(1) = "Item" Then
                                Session("UpdateQuery") = "update Item set " & str(0) & " = '" & value & "' where numDomainID = " & Session("DomainID") & " and numItemCode in (@$replace)"
                            End If
                        End If
                    ElseIf hdDbAssociatedControl.Value = "SelectBox" Or hdDbAssociatedControl.Value = "ListBox" Then                       'SelectBox
                        If str(0) = "Custom" Then
                            If str(1) = "DivisionMaster" Then
                                Session("UpdateQuery") = "delete from CFW_FLD_Values where Fld_ID=" & lstAvailablefld.SelectedItem.Value.Split("~")(6) & " and RecId in (select @$replace   insert into  CFW_FLD_Values (Fld_ID,Fld_Value,RecId)  (select " & lstAvailablefld.SelectedItem.Value.Split("~")(6) & ",'" & IIf(ddlValue.SelectedValue = "", 0, ddlValue.SelectedValue) & "',@$replace "
                            ElseIf str(1) = "AdditionalContactsInformation" Then
                                Session("UpdateQuery") = "delete from CFW_FLD_Values_Cont where Fld_ID=" & lstAvailablefld.SelectedItem.Value.Split("~")(6) & " and RecId in (select @$replace   insert into  CFW_FLD_Values_Cont (Fld_ID,Fld_Value,RecId)  (select " & lstAvailablefld.SelectedItem.Value.Split("~")(6) & ",'" & IIf(ddlValue.SelectedValue = "", 0, ddlValue.SelectedValue) & "',@$replace "
                            End If
                        ElseIf str(0) = "Ownership" Then
                            If str(1) = "DivisionMaster" Then
                                Session("UpdateQuery") = "update DivisionMaster set numRecOwner = " & IIf(ddlValue.SelectedValue = "", 0, ddlValue.SelectedValue) & " where numDomainID= " & CCommon.ToLong(Session("DomainID")) & " and numDivisionID in (select  @$replace"
                            ElseIf str(1) = "AdditionalContactsInformation" Then
                                Session("UpdateQuery") = "update AdditionalContactsInformation set numRecOwner = " & IIf(ddlValue.SelectedValue = "", 0, ddlValue.SelectedValue) & " where numDomainID= " & CCommon.ToLong(Session("DomainID")) & " and numContactID in (select  @$replace"
                            End If
                        ElseIf str(0) = "Organization Campaign" Then
                            'Override current behaviour only for campaign and update values from this page itself
                            Session("UpdateQuery") = IIf(ddlValue.SelectedValue = "", -1, ddlValue.SelectedValue)
                            Response.Write("<script language='javascript'>opener.UpdateCampaign();self.close();</script>")
                            Exit Sub
                        ElseIf str(0) = "DripCampaign" Then
                            'Override current behaviour only for drip campaign and update values from this page itself
                            Session("UpdateQuery") = IIf(ddlValue.SelectedValue = "", -1, ddlValue.SelectedValue)
                            Response.Write("<script language='javascript'>opener.UpdateDripCampaign();self.close();</script>")
                            Exit Sub
                        ElseIf str(0) = "numStatus" Then
                            Session("UpdateQuery") = "SELECT USP_OpportunityMaster_ChangeOrderStatus(" & CCommon.ToLong(Session("DomainID")) & "," & CCommon.ToLong(Session("UserContactID")) & "," & IIf(ddlValue.SelectedValue = "", 0, ddlValue.SelectedValue) & ",'##OppCodes##');"
                        ElseIf str(0) = "vcUnitofMeasure" Then
                            Session("UpdateQuery") = "update Item set " & str(0) & " = '" & IIf(ddlValue.SelectedItem.Text = "---Select One---", "", ddlValue.SelectedItem.Text) & "' where numDomainID = " & Session("DomainID") & " and numItemCode in (@$replace)"
                        ElseIf str(0) = "charItemType" Then
                            Session("UpdateQuery") = "update Item set " & str(0) & " = '" & IIf(ddlValue.SelectedValue = "", 0, ddlValue.SelectedValue) & "' where numDomainID = " & Session("DomainID") & " and numItemCode in (@$replace)"
                        ElseIf str(0) = "numVendorID" Then
                            'Override current behaviour only for campaign and update values from this page itself
                            Session("UpdateQuery") = CCommon.ToLong(radCmbCompany.SelectedValue) & "~" & IIf(chkIsPrimary.Checked, "1", "0")
                            Response.Write("<script language='javascript'>opener.UpdateVendor();self.close();</script>")
                            Exit Sub

                        ElseIf str(0) = "tintCRMType" Then
                            If str(1) = "DivisionMaster" Then
                                Session("UpdateQuery") = "update DivisionMaster set tintCRMType = " & IIf(ddlValue.SelectedValue = "", 0, ddlValue.SelectedValue) & " where numDomainID= " & CCommon.ToLong(Session("DomainID")) & " and numDivisionID in (select  @$replace"
                            End If

                        Else
                            If str(1) = "DivisionMaster" Then
                                Session("UpdateQuery") = "update DivisionMaster set " & str(0) & " = " & IIf(ddlValue.SelectedValue = "", 0, ddlValue.SelectedValue) & " where numDomainID= " & CCommon.ToLong(Session("DomainID")) & " and numDivisionID in (select  @$replace"
                            ElseIf str(1) = "AdditionalContactsInformation" Then
                                Session("UpdateQuery") = "update AdditionalContactsInformation set " & str(0) & " = " & IIf(ddlValue.SelectedValue = "", 0, ddlValue.SelectedValue) & " where numDomainID= " & CCommon.ToLong(Session("DomainID")) & " and numContactID in (select  @$replace"
                            ElseIf str(1) = "CompanyInfo" Then
                                Session("UpdateQuery") = "update CompanyInfo set " & str(0) & " = " & IIf(ddlValue.SelectedValue = "", 0, ddlValue.SelectedValue) & " where numDomainID= " & CCommon.ToLong(Session("DomainID")) & " and numCompanyID in (select  @$replace"
                            ElseIf str(1) = "AddressDetails" Then
                                SetAddressDetailsParameter(str, shortAddressType, shortAddressOf)
                                Session("UpdateQuery") = "update AddressDetails set " & str(0) & " = '" & IIf(ddlValue.SelectedValue = "", 0, ddlValue.SelectedValue) & "' where numDomainID= " & Session("DomainID") & " and tintAddressOf=" & shortAddressOf.ToString & " AND tintAddressType=" & shortAddressType.ToString & " AND bitIsPrimary=1 and numRecordID in (select  @$replace"
                            ElseIf str(1) = "Item" Then
                                Session("UpdateQuery") = "update Item set " & str(0) & " = " & IIf(ddlValue.SelectedValue = "", 0, ddlValue.SelectedValue) & " where numDomainID = " & Session("DomainID") & " and numItemCode in (@$replace)"
                            End If
                        End If
                    ElseIf hdDbAssociatedControl.Value = "DateField" Then
                        Dim strFormattedDate As String = ""
                        If cal.SelectedDate <> "" Then
                            strFormattedDate = FormattedDateFromDate(cal.SelectedDate, Session("DateFormat"))
                        End If
                        If str(1) = "DivisionMaster" Then
                            Session("UpdateQuery") = "delete from CFW_FLD_Values where Fld_ID=" & lstAvailablefld.SelectedItem.Value.Split("~")(6) & " and RecId in (select @$replace   insert into  CFW_FLD_Values (Fld_ID,Fld_Value,RecId)  (select " & lstAvailablefld.SelectedItem.Value.Split("~")(6) & ",'" & strFormattedDate & "',@$replace "
                        ElseIf str(1) = "AdditionalContactsInformation" Then
                            Session("UpdateQuery") = "delete from CFW_FLD_Values_Cont where Fld_ID=" & lstAvailablefld.SelectedItem.Value.Split("~")(6) & " and RecId in (select @$replace   insert into  CFW_FLD_Values_Cont (Fld_ID,Fld_Value,RecId)  (select " & lstAvailablefld.SelectedItem.Value.Split("~")(6) & ",'" & strFormattedDate & "',@$replace "
                        ElseIf str(1) = "Item" Then
                            Session("UpdateQuery") = "update Item set " & str(0) & " = '" & strFormattedDate & "' where numDomainID = " & Session("DomainID") & " and numItemCode in (@$replace)"
                        End If
                    End If
                    'This row of valeu is now hidden

                    Response.Write("<script language='javascript'>opener.UpdateValues();self.close();</script>")
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This method is used to display the available options in the dropdown (in case of a selectable field)
        '''     or a empty textbox
        ''' </summary>
        ''' <param name="sender">Represents the sender object.</param>
        ''' <param name="e">Represents the EventArgs.</param>
        ''' <remarks>
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	18/03/2006	Created
        ''' </history>
        '''  -----------------------------------------------------------------------------
        Private Sub lstAvailablefld_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles lstAvailablefld.SelectedIndexChanged
            Try
                If CStr(lstAvailablefld.SelectedItem.Value) <> "None" Then
                    btnSave.Attributes.Remove("onclick")
                    Dim str As String()
                    str = lstAvailablefld.SelectedItem.Value.Split("~")
                    hdDbAssociatedControl.Value = str(5)
                    hdDbColumnListItemType.Value = str(2)
                    hdDbColumnListId.Value = str(3)
                    HideControls()
                    If str(0) = "numVendorID" Then
                        radCmbCompany.Visible = True
                        chkIsPrimary.Visible = True
                    ElseIf str(0) = "numCategoryID" Then
                        BindCategoryProfile()
                        loadCategory()
                        btnSave.Attributes.Add("onclick", "return ValidateCategorySelection();")
                        radCmbCompany.Visible = False
                        pnlItemCategory.Visible = True
                    ElseIf hdDbAssociatedControl.Value = "TextBox" Then                             'EditBox
                        txtValue.Visible = True                                                 'Make only the TextBox visible
                        lblFieldName.Text = "Enter " & lstAvailablefld.SelectedItem.Text        'Change the label
                        If str(0) = "intMinQty" AndAlso Session("VendorIDs") Is Nothing Then
                            btnSave.Attributes.Add("onclick", "return ConfirmUpdate();")
                        Else
                            btnSave.Attributes.Remove("onclick")
                        End If
                    ElseIf hdDbAssociatedControl.Value = "CheckBox" Then                        'CheckBox
                        cbValue.Visible = True                                                  'Make only the checkbox visible
                        lblFieldName.Text = "Check for " & lstAvailablefld.SelectedItem.Text    'Change the label
                    ElseIf hdDbAssociatedControl.Value = "TextArea" Then                        'TextBox
                        txtValue.Visible = True                                                 'Make only the TextBox visible
                        txtValue.Width = Unit.Pixel(130)                                        'set the width of the textbox
                        txtValue.Height = Unit.Pixel(40)                                        'set the height of the textbox
                        txtValue.TextMode = TextBoxMode.MultiLine                               'This is a multiline textarea
                        lblFieldName.Text = "Enter " & lstAvailablefld.SelectedItem.Text        'Change the label
                    ElseIf hdDbAssociatedControl.Value = "SelectBox" Or hdDbAssociatedControl.Value = "ListBox" Then                       'SelectBox
                        ddlValue.Visible = True                                                 'Make only the drop down visible
                        Dim dtTable As DataTable                                                'declare a datatable
                        If lstAvailablefld.SelectedItem.Text.EndsWith("State") Then             'Different treatment for state drop down
                            FillAllState(ddlValue, Session("DomainID"))
                        ElseIf str(0) = "Ownership" Then

                            objCommon.sb_FillConEmpFromDBSel(ddlValue, Session("DomainID"), 0, 0)
                        ElseIf str(0) = "Organization Campaign" Then
                            objCommon.sb_FillComboFromDB(ddlValue, 18, CCommon.ToLong(Session("DomainID")))
                        ElseIf str(0) = "DripCampaign" Then
                            Dim objCampaign As New BusinessLogic.Marketing.Campaign
                            With objCampaign
                                .SortCharacter = "0"
                                .UserCntID = Session("UserContactID")
                                .PageSize = 100
                                .TotalRecords = 0
                                .DomainID = Session("DomainID")
                                .columnSortOrder = "Asc"
                                .CurrentPage = 1
                                .columnName = "vcECampName"
                            End With
                            dtTable = objCampaign.ECampaignList
                            Dim drow As DataRow = dtTable.NewRow
                            drow("numECampaignID") = -1
                            drow("vcECampName") = "-- Disengaged --"
                            dtTable.Rows.Add(drow)
                            ddlValue.DataTextField = "vcECampName"
                            ddlValue.DataValueField = "numECampaignID"
                            ddlValue.DataSource = dtTable
                            ddlValue.DataBind()

                        ElseIf str(0) = "tintCRMType" Then
                            Dim objConfigWizard As New FormGenericFormContents                      'Create an object of class which encaptulates the functionaltiy
                            objConfigWizard.ListItemType = hdDbColumnListItemType.Value             'set the listitem type
                            objConfigWizard.DomainID = Session("DomainID")                          'Set the Domain Id
                            dtTable = objConfigWizard.GetMasterListByListId(CInt(hdDbColumnListId.Value)) 'call function to fill the datatable
                            'Dim dRow As DataRow                                                     'declare a datarow
                            'dRow = dtTable.NewRow()                                                 'get a new row object
                            'dtTable.Rows.InsertAt(dRow, 0)                                          'insert the row
                            'dtTable.Rows(0).Item("numItemID") = System.DBNull.Value                 'set the values for the first entry
                            'dtTable.Rows(0).Item("vcItemName") = "---Select One---"                 'set the group name
                            ddlValue.DataSource = dtTable                                           'set the datasource of the drop down
                            ddlValue.DataTextField = "vcItemName"                                   'Set the Text property of the drop down
                            ddlValue.DataValueField = "numItemID"                                   'Set the value attribute of the textbox
                            ddlValue.DataBind()

                        Else
                            Dim objConfigWizard As New FormGenericFormContents                      'Create an object of class which encaptulates the functionaltiy
                            objConfigWizard.ListItemType = hdDbColumnListItemType.Value             'set the listitem type
                            objConfigWizard.DomainID = Session("DomainID")                          'Set the Domain Id
                            dtTable = objConfigWizard.GetMasterListByListId(CInt(hdDbColumnListId.Value)) 'call function to fill the datatable
                            Dim dRow As DataRow                                                     'declare a datarow
                            dRow = dtTable.NewRow()                                                 'get a new row object
                            dtTable.Rows.InsertAt(dRow, 0)                                          'insert the row
                            dtTable.Rows(0).Item("numItemID") = System.DBNull.Value                 'set the values for the first entry
                            dtTable.Rows(0).Item("vcItemName") = "---Select One---"                 'set the group name
                            ddlValue.DataSource = dtTable                                           'set the datasource of the drop down
                            ddlValue.DataTextField = "vcItemName"                                   'Set the Text property of the drop down
                            ddlValue.DataValueField = "numItemID"                                   'Set the value attribute of the textbox
                            ddlValue.DataBind()
                            If str(0) = "vcUnitofMeasure" Then
                                Dim objUnitSystem As New UnitSystem
                                objUnitSystem.AddUnitSystemItems(ddlValue, Session("UnitSystem"))
                            End If
                        End If
                        lblFieldName.Text = "Select a " & lstAvailablefld.SelectedItem.Text     'Change the label
                    ElseIf hdDbAssociatedControl.Value = "DateField" Then                        'TextBox
                        cal.Visible = True                                                 'Make only the TextBox visible
                        lblFieldName.Text = "Enter " & lstAvailablefld.SelectedItem.Text        'Change the label

                    End If
                    rowValue.Visible = True                                                     'This row of valeu is now made visible
                Else : rowValue.Visible = False                                                    'This row of valeu is now hidden
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Sub HideControls()
            Try
                radCmbCompany.Visible = False
                chkIsPrimary.Visible = False
                ddlValue.Visible = False
                txtValue.Visible = False
                cbValue.Visible = False
                cal.Visible = False
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        Private Sub SetAddressDetailsParameter(ByRef str As String(), ByRef shortAddressType As Short, ByRef shortAddressOf As Short)
            Try
                If str(7).ToLower.Contains("ship") Then
                    shortAddressOf = 2
                    shortAddressType = 2
                    Session("LookTable") = "ShipAddress"
                ElseIf str(7).ToLower.Contains("bil") Then
                    shortAddressOf = 2
                    shortAddressType = 1
                    Session("LookTable") = "BillAddress"
                Else
                    shortAddressOf = 1
                    shortAddressType = 0
                    Session("LookTable") = "ContactAddress"
                End If
                str(0) = str(0).Replace("Ship", "").Replace("Bill", "").Replace("Bil", "").Replace("PostCode", "PostalCode")
                If str(0).ToLower.Contains("state") Then str(0) = "numState"
                If str(0).ToLower.Contains("country") Then str(0) = "numCountry"
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub BindCategoryProfile()
            Try
                Dim objCategoryProfile As New BACRM.BusinessLogic.ShioppingCart.CategoryProfile
                objCategoryProfile.DomainID = CCommon.ToLong(Session("DomainID"))
                Dim dt As DataTable = objCategoryProfile.GetAll()

                If Not dt Is Nothing AndAlso dt.Rows.Count > 0 Then
                    ddlCategoryProfile.DataSource = dt
                    ddlCategoryProfile.DataTextField = "vcName"
                    ddlCategoryProfile.DataValueField = "ID"
                    ddlCategoryProfile.DataBind()

                    If Not PersistTable(ddlCategoryProfile.ClientID) Is Nothing _
                        AndAlso Not ddlCategoryProfile.Items.FindByValue(PersistTable(ddlCategoryProfile.ClientID)) Is Nothing Then
                        ddlCategoryProfile.Items.FindByValue(PersistTable(ddlCategoryProfile.ClientID)).Selected = True
                    End If

                    GetCategoryProfileDetail()
                End If
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Private Sub GetCategoryProfileDetail()
            Try
                If Not ddlCategoryProfile.SelectedItem Is Nothing Then
                    Dim objCategoryProfile As New BACRM.BusinessLogic.ShioppingCart.CategoryProfile
                    objCategoryProfile.DomainID = CCommon.ToLong(Session("DomainID"))
                    objCategoryProfile.ID = CCommon.ToLong(ddlCategoryProfile.SelectedValue)
                    objCategoryProfile.GetByIDWithSites()

                    If Not objCategoryProfile.ListCategoryProfileSites Is Nothing AndAlso objCategoryProfile.ListCategoryProfileSites.Count > 0 Then
                        lblProfileSites.Text = "<b>Sites using this profile:</b> " & String.Join(", ", objCategoryProfile.ListCategoryProfileSites.Select(Function(x) x.SiteName))
                    Else
                        lblProfileSites.Text = "<b>Sites using this profile:</b> "
                    End If
                End If
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Private Sub ddlCategoryProfile_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlCategoryProfile.SelectedIndexChanged
            Try
                GetCategoryProfileDetail()
                loadCategory()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub loadCategory()
            Try
                Dim objItems As New BACRM.BusinessLogic.Item.CItems
                objItems.byteMode = 18
                objItems.DomainID = Session("DomainID")
                objItems.SiteID = 0
                objItems.CategoryProfileID = CCommon.ToLong(ddlCategoryProfile.SelectedValue)

                Dim dtCategory As DataTable
                dtCategory = objItems.SeleDelCategory

                rtvCategory.DataTextField = "vcCategoryName"
                rtvCategory.DataValueField = "numCategoryID"
                rtvCategory.DataFieldID = "numCategoryID"
                rtvCategory.DataFieldParentID = "numDepCategory"
                rtvCategory.DataSource = dtCategory
                Try
                    rtvCategory.DataBind()
                Catch ex As Exception
                    ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                    Response.Write(ex)
                End Try
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Function GetSelectedCategory()
            Try
                Dim strCategories As String = ""
                If rtvCategory.CheckedNodes.Count > 0 Then
                    'rtvParentCategory.SelectedNodes()
                    For Each item As Telerik.Web.UI.RadTreeNode In rtvCategory.CheckedNodes
                        If item.Checked = True Then
                            strCategories = strCategories + item.Value + ","
                        End If
                    Next
                End If
                strCategories = strCategories.TrimEnd(",")
                Return strCategories
            Catch ex As Exception
                Throw ex
            End Try
        End Function
    End Class
End Namespace
