''created by anoop jayaraj
Imports BACRM.BusinessLogic.Admin
Imports System.IO
Imports System.Math
Imports BACRM.BusinessLogic.Common
Imports System.Reflection
Imports BACRM.BusinessLogic.Reports
Imports System.Text

Namespace BACRM.UserInterface.Admin

    Partial Public Class frmAdvancedSearch
        Inherits BACRMPage
        Dim m_aryRightsForEComm() As Integer
        Dim objGenericAdvSearch As New FormGenericAdvSearch
        Public dtGenericFormConfig As DataTable
        Public dsGenericFormConfig As DataSet

        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            Try
                'CLEAR ERROR ON RELOAD
                DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ""
                DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "none")

                txtzipcode.Attributes.Add("onkeypress", "return CheckNumber(2,event)")
                GetUserRightsForPage(9, 1)

                If Not IsPostBack Then
                    objCommon.sb_FillComboFromDBwithSel(ddlActivity, 32, Session("DomainID"))
                    LoadDropdowns()
                    'FillItemLists()
                    callToBindRelationships()
                    m_aryRightsForEComm = GetUserRightsForPage_Other(9, 8)
                End If

                GetFormFieldList()

                GenericFormControlsGeneration.FormID = 1                                        'set the form id to Advance Search
                GenericFormControlsGeneration.DomainID = Session("DomainID")                    'Set the domain id
                GenericFormControlsGeneration.UserCntID = Session("UserContactID")                        'Set the User id
                GenericFormControlsGeneration.AuthGroupId = Session("UserGroupID")              'Set the User Authentication Group Id

                cbSearchInLeads.Attributes.Add("onclick", "javascript: MakeLeadsCheckBoxSelectionConsistant(this);") 'Ensure consistancy of selection of Leads CheckBox
                ddlLead.Attributes.Add("onchange", "javascript: MakeLeadsOptionSelectionConsistant();") 'Ensure consistancy of selection of Leads CheckBox

                GenerateForm(dsGenericFormConfig)                        'Calls function for form generation and display for non AOI fields

                Dim dvConfig As DataView
                dtGenericFormConfig = dsGenericFormConfig.Tables(1)
                dvConfig = New DataView(dtGenericFormConfig)

                dvConfig.RowFilter = " vcDbColumnName like '%Country'"
                If dvConfig.Count > 0 Then
                    Dim i As Integer

                    For i = 0 To dvConfig.Count - 1
                        Dim strControlID As String
                        Dim drState() As DataRow

                        drState = dtGenericFormConfig.Select("vcDbColumnName='" & Replace(dvConfig(i).Item("vcDbColumnName"), "Country", "State") & "'")

                        If drState.Length > 0 Then
                            strControlID = drState(0)("numFormFieldID").ToString & "_" & drState(0)("vcDbColumnName").ToString.Trim.Replace(" ", "_")

                            If Not CType(tblSearch.FindControl(strControlID), Telerik.Web.UI.RadComboBox) Is Nothing Then
                                Dim dl As DropDownList
                                dl = CType(tblSearch.FindControl(dvConfig(i).Item("numFormFieldID").ToString & "_" & dvConfig(i).Item("vcDbColumnName")), DropDownList)
                                dl.AutoPostBack = True
                                AddHandler dl.SelectedIndexChanged, AddressOf FillStateDyn
                                If Not IsPostBack Then
                                    'Set default country
                                    If Not dl.Items.FindByValue(Session("DefCountry")) Is Nothing Then
                                        dl.Items.FindByValue(Session("DefCountry")).Selected = True
                                        If dl.SelectedIndex > 0 Then
                                            FillState(CType(tblSearch.FindControl(strControlID), Telerik.Web.UI.RadComboBox), CCommon.ToLong(dl.SelectedItem.Value), Session("DomainID"))
                                        End If
                                    End If
                                End If
                            End If
                        End If
                    Next
                End If


                callFuncForFormGenerationAOI()                'Calls function to display the complete list of AOI

                litMessage.Text = GenericFormControlsGeneration.getJavascriptArray(dtGenericFormConfig) 'Create teh javascript array and store

            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Sub GetFormFieldList()
            Try
                If objGenericAdvSearch Is Nothing Then objGenericAdvSearch = New FormGenericAdvSearch

                objGenericAdvSearch.AuthenticationGroupID = Session("UserGroupID")
                objGenericAdvSearch.FormID = 1
                objGenericAdvSearch.DomainID = Session("DomainID")
                dsGenericFormConfig = objGenericAdvSearch.GetAdvancedSearchFieldList()

                dsGenericFormConfig.Tables(0).TableName = "SectionInfo"                           'give a name to the datatable
                dsGenericFormConfig.Tables(1).TableName = "AdvSearchFields"                           'give a name to the datatable

                dsGenericFormConfig.Tables(1).Columns.Add("vcNewFormFieldName", System.Type.GetType("System.String"), "[vcFormFieldName]") 'This columsn has to be an integer to tryign to manipupate the datatype as Compute(Max) does not work on string datatypes
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Function callFuncForFormGenerationAOI() As DataTable
            Try
                If objGenericAdvSearch Is Nothing Then objGenericAdvSearch = New FormGenericAdvSearch

                Dim dsFormConfig As DataSet                                                                       'Declare a DataTable
                objGenericAdvSearch.AuthenticationGroupID = Session("UserGroupID")
                objGenericAdvSearch.FormID = 1
                objGenericAdvSearch.DomainID = Session("DomainID")
                dsFormConfig = objGenericAdvSearch.getAOIList()                                                     'Get the AOIList
                GenericFormControlsGeneration.createFormControlsAOI(dsFormConfig.Tables(1), tblSearch)  'calls to create the form controls and add it to the form
            Catch ex As Exception
                Throw ex
            End Try                                                                            'Return the datatable which contains the AOI List
        End Function

        Sub callToBindRelationships()
            Try
                Dim dtRelations As DataTable                                            'declare a datatable
                dtRelations = objGenericAdvSearch.getListDetails(5) 'call to get the relationships
                Dim dRow As DataRow                                                     'declare a datarow
                dRow = dtRelations.NewRow()                                             'get a new row object
                dtRelations.Rows.InsertAt(dRow, 0)                                      'insert the row
                dtRelations.Rows(0).Item("numItemID") = 0                               'set the value for first entry
                dtRelations.Rows(0).Item("vcItemName") = "All Relationships"            'set the text for the first entey
                dtRelations.Rows(0).Item("vcItemType") = "L"                            'type= list
                dtRelations.Rows(0).Item("flagConst") = 0                               'requried by the stored procedure
                ddlRelationship.DataSource = dtRelations.DefaultView                    'set the datasource
                ddlRelationship.DataTextField = "vcItemName"                            'set the text attribute
                ddlRelationship.DataValueField = "numItemID"                            'set the value attribute
                ddlRelationship.DataBind()                                              'databind the drop down
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub GenerateForm(ByVal dsGenericFormConfig As DataSet)
            Try
                GenericFormControlsGeneration.boolAOIField = 0                                      'Set the AOI flag to non AOI

                GenericFormControlsGeneration.createDynamicFormControlsSectionWise(dsGenericFormConfig, plhControls, 1, Session("DomainID"), Session("UserContactID"))
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Public Sub copyTableDataFromOneToAnother(ByRef dtGenericFormConfig As DataTable, ByVal dtGenericFormAOIExtraFields As DataTable)
            Try
                Dim drChildTableRows, drParentTableRows As DataRow                      'Declare a Datarow Object
                Dim dcParentColumn As DataColumn                                        'Declare a DataColumn Object
                Dim iIndex As Integer = 0                                               'Declare a integer object and initialize
                For Each drChildTableRows In dtGenericFormAOIExtraFields.Rows           'Loop through the row collection
                    drParentTableRows = dtGenericFormConfig.NewRow()                    'Crete a new row object of the first table
                    For Each dcParentColumn In dtGenericFormConfig.Columns              'Loop through the column collection of parent table
                        If dtGenericFormAOIExtraFields.Columns.Contains(dcParentColumn.ColumnName) Then 'Check if the column is contained in the child table
                            drParentTableRows.Item(dcParentColumn.ColumnName) = IIf(dcParentColumn.ColumnName = "intRowNum", iIndex, drChildTableRows.Item(dcParentColumn.ColumnName)) 'Copy the value from the child to the parent column
                        End If
                    Next
                    dtGenericFormConfig.Rows.Add(drParentTableRows)                     'Add the row to the table
                    iIndex += 1                                                         'Increment teh counter
                Next
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub FillStateDyn(ByVal sender As Object, ByVal e As EventArgs)
            Try
                Dim drState() As DataRow

                drState = dtGenericFormConfig.Select("vcDbColumnName='" & sender.ID.ToString.Split("_")(1).Replace("Country", "State") & "'")

                If drState.Length > 0 Then
                    Dim strControlID As String

                    strControlID = drState(0)("numFormFieldID").ToString & "_" & drState(0)("vcDbColumnName").ToString.Trim.Replace(" ", "_")

                    FillState(CType(tblSearch.FindControl(strControlID), Telerik.Web.UI.RadComboBox), sender.SelectedItem.Value, Session("DomainID"))
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub LoadDropdowns()
            Try
                objGenericAdvSearch.UserCntID = Session("UserContactID")
                objGenericAdvSearch.DomainID = Session("DomainID")
                ddlEmailGroup.DataSource = objGenericAdvSearch.LoadDropdown()
                ddlEmailGroup.DataTextField = "Name"
                ddlEmailGroup.DataValueField = "GroupId"
                ddlEmailGroup.DataBind()
                ddlEmailGroup.Items.Insert(0, "--Select One--")
                ddlEmailGroup.Items.FindByText("--Select One--").Value = 0
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub ddlGroup_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlEmailGroup.SelectedIndexChanged
            Try
                If ddlEmailGroup.SelectedItem.Value > 0 Then
                    Session("WhereCondition") = " and ADC.numContactID in (Select numContactID from ProfileEGroupDTL where numEmailGroupID=" & ddlEmailGroup.SelectedItem.Value & ")"
                    Response.Redirect("frmAdvancedSearchRes.aspx", False)
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Private Sub btnFindDuplicates_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnFindDuplicates.Click
            Try
                Session("WhereCondition") = "DUP"
                Response.Redirect("frmAdvancedSearchRes.aspx", False)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Private Sub DisplayError(ByVal exception As String)
            Try
                DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = exception
                DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
                DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Focus()
            Catch ex As Exception

            End Try
        End Sub
    End Class
End Namespace