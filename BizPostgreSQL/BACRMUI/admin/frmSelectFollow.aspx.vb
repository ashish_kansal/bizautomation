Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Common
Namespace BACRM.UserInterface.Admin

    Partial Public Class frmSelectFollow
        Inherits BACRMPage
        Dim myDataTable As DataTable
       

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Try
                If Not IsPostBack Then
                    
                    
                    GetUserRightsForPage(13, 21)
                    If m_aryRightsForPage(RIGHTSTYPE.VIEW) = 0 Then Response.Redirect("../admin/authentication.aspx?mesg=AS")
                    objCommon.sb_FillComboFromDBwithSel(ddlRelationship, 5, Session("DomainID"))
                    objCommon.sb_FillComboFromDBwithSel(ddlFollow, 30, Session("DomainID"))
                    BindGrid()
                End If
                litMessage.Text = ""
                btnClose.Attributes.Add("onclick", "return Close()")
                btnAdd.Attributes.Add("onclick", "return Save()")
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Sub BindGrid()
            Try
                Dim objUserAccess As New UserAccess
                objUserAccess.RelID = ddlRelationship.SelectedItem.Value
                objUserAccess.DomainID = Session("DomainID")
                dgReFollow.DataSource = objUserAccess.GetRelFollowD
                dgReFollow.DataBind()
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
            Try
                Dim objUserAccess As New UserAccess
                objUserAccess.DomainID = Session("DomainID")
                objUserAccess.RelID = ddlRelationship.SelectedValue
                objUserAccess.FollowID = ddlFollow.SelectedItem.Value
                objUserAccess.ManageRelFollow()
                Call BindGrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub ddlRelationship_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlRelationship.SelectedIndexChanged
            Try
                BindGrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub dgReFollow_EditCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgReFollow.EditCommand
            Try
                dgReFollow.EditItemIndex = e.Item.ItemIndex
                Call BindGrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub dgReFollow_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgReFollow.ItemCommand
            Try
                If e.CommandName = "Cancel" Then
                    dgReFollow.EditItemIndex = e.Item.ItemIndex
                    dgReFollow.EditItemIndex = -1
                    Call BindGrid()
                End If
                If e.CommandName = "Delete" Then
                    Dim objUserAccess As New UserAccess
                    objUserAccess.RelFollowID = CType(e.Item.FindControl("numRelFollowID"), Label).Text
                    If Not objUserAccess.DeleteRelFollow = True Then litMessage.Text = "Depenedent Record exists.Cannot be deleted."
                    Call BindGrid()
                End If
                If e.CommandName = "Update" Then
                    Dim objUserAccess As New UserAccess
                    objUserAccess.RelFollowID = CType(e.Item.FindControl("numRelFollowID"), Label).Text
                    objUserAccess.RelID = CType(e.Item.FindControl("ddlERelationship"), DropDownList).SelectedItem.Value
                    objUserAccess.FollowID = CType(e.Item.FindControl("ddlEFollowup"), DropDownList).SelectedItem.Value
                    objUserAccess.ManageRelFollow()
                    dgReFollow.EditItemIndex = -1
                    Call BindGrid()
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub dgReFollow_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgReFollow.ItemDataBound
            Try
                If e.Item.ItemType = ListItemType.EditItem Then
                    Dim ddlRel, ddlFol As DropDownList
                    Dim lblRel, lblFol As Label
                    ddlRel = e.Item.FindControl("ddlERelationship")
                    ddlFol = e.Item.FindControl("ddlEFollowup")
                    lblFol = e.Item.FindControl("lblEFollowup")
                    lblRel = e.Item.FindControl("lblERel")
                    
                    objCommon.sb_FillComboFromDBwithSel(ddlRel, 5, Session("DomainID"))
                    objCommon.sb_FillComboFromDBwithSel(ddlFol, 30, Session("DomainID"))
                    ddlRel.Items.FindByValue(lblRel.Text).Selected = True
                    ddlFol.Items.FindByValue(lblFol.Text).Selected = True
                    Dim lnkbtnUpdt As LinkButton

                    lnkbtnUpdt = e.Item.FindControl("lnkbtnUpdt")
                    lnkbtnUpdt.Attributes.Add("onclick", "return ESave('" & ddlRel.ClientID & "','" & ddlFol.ClientID & "')")
                End If
                If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
                    Dim btn As Button
                    btn = e.Item.FindControl("btnDelete")
                    btn.Attributes.Add("onclick", "return DeleteRecord()")
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

    End Class
End Namespace
