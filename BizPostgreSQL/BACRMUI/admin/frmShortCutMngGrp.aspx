<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmShortCutMngGrp.aspx.vb"
    Inherits=".frmShortCutMngGrp" MasterPageFile="~/common/PopupBootstrap.Master" Title="Manage ShortCut Bar"
    ClientIDMode="Static" %>
<%@ Register Assembly="Telerik.Web.UI, Version=2011.2.712.40, Culture=neutral, PublicKeyToken=121fae78165ba3d4" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content3" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <script type="text/javascript">

        function SetInitialPage(tbox) {

            if ($("#lstAddfldFav").val() != null) {
                var SelectedText, SelectedValue;

                for (var i = 0; i < tbox.options.length; i++) {
                    if (tbox.options[i].selected && tbox.options[i].value != "") {
                        SelectedText = tbox.options[i].text;
                        SelectedValue = tbox.options[i].value;

                        if (SelectedText.indexOf('(*)') != -1)
                            SelectedText = SelectedText.replace('(*)', '');
                        else
                            SelectedText = '(*) ' + SelectedText;

                        tbox.options[i].text = SelectedText;
                    }
                    else {
                        if (tbox.options[i].text.indexOf('(*)') != -1) {
                            SelectedText = tbox.options[i].text;
                            SelectedValue = tbox.options[i].value;
                            SelectedText = SelectedText.replace('(*)', '');
                            tbox.options[i].text = SelectedText;
                        }
                    }
                }
            }

            var strFav = '';
            document.getElementById("txtInitialPage").value = 0;

            for (var i = 0; i < document.getElementById("lstAddfldFav").options.length; i++) {
                var SelectedValueFav;
                SelectedValueFav = document.getElementById("lstAddfldFav").options[i].value;
                strFav = strFav + SelectedValueFav + ',';

                if (document.getElementById("lstAddfldFav").options[i].text.indexOf('(*)') != -1) {
                    document.getElementById("txtInitialPage").value = SelectedValueFav;
                }
            }

            document.getElementById("txthiddenFav").value = strFav;

            return true;
        }
    </script>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <div class="input-part">
        <div class="right-input">
            <div class="form-inline pull-right">
                <div class="form-group">
                    <label>Apply configuration to</label>
                    <telerik:radcombobox id="rcbGroups" runat="server" checkboxes="true"></telerik:radcombobox>
                </div>
                <asp:Button ID="btnSetAsInitial" runat="server" Text="Save" CssClass="btn btn-primary" OnClientClick="return SetInitialPage(lstAddfldFav);" />
                <asp:Button runat="server" ID="btnclose" OnClientClick="javascript:self.close()" Text="Close" CssClass="btn btn-primary" />
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="PageTitle" runat="server" ClientIDMode="Static">
    Manage ShortCut Bar
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="Content" runat="server" ClientIDMode="Static">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <div class="row">
        <div class="col-xs-12">
            <div class="form-inline">
                <div class="form-group">
                    <label>Group</label>
                    <asp:DropDownList ID="ddlGroup" runat="server" AutoPostBack="true" CssClass="form-control"></asp:DropDownList>
                </div>
                <div class="form-group">
                    <label>Tab</label>
                     <asp:DropDownList ID="ddlTab" AutoPostBack="true" runat="server" CssClass="form-control"></asp:DropDownList>
                </div>
                <div class="form-group">
                    <label>URL</label>
                    <asp:ListBox ID="lstAddfldFav" runat="server" Width="200" Height="200" CssClass="form-control"></asp:ListBox>
                </div>
            </div>
        </div>
        <div class="col-xs-12">
            <asp:CheckBox ID="chkDefaultPage" runat="server" Text="Make this the initial tab that loads when users log into BizAutomation." />
        </div>
    </div>

    <asp:TextBox Style="display: none" ID="txthiddenFav" runat="server"></asp:TextBox>
    <asp:TextBox Style="display: none" ID="txtInitialPage" runat="server"></asp:TextBox>
</asp:Content>
