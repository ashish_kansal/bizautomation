﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/common/PopupBootstrap.Master" CodeBehind="frmLandedCostVendors.aspx.vb" Inherits=".frmLandedCostVendors" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <title>Landed Cost Vendors</title>
    <script type="text/javascript" src="../javascript/comboClientSide.js"></script>
    <script type="text/javascript" language="javascript">
        function Save() {
            if ($find('radCmbCompany').get_value() == "") {
                alert("Select Vendor")
                return false;
            }
        }

        function onRadComboBoxLoad(sender) {
            var div = sender.get_element();

            $telerik.$(div).bind('mouseenter', function () {
                if (!sender.get_dropDownVisible())
                    sender.showDropDown();
            });


            $telerik.$(".RadComboBoxDropDown").mouseleave(function (e) {
                hideDropDown("#" + sender.get_id(), sender, e);
            });


            $telerik.$(div).mouseleave(function (e) {
                hideDropDown(".RadComboBoxDropDown", sender, e);
            });

        }
    </script>
    <style>
        .tblNoBorder tr td{
            border:0px !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server" ClientIDMode="Static">
    <div class="input-part">
        <div class="pull-right">
            <asp:Button ID="btnClose" runat="server" CssClass="btn btn-primary" Text="Close" OnClientClick="return Close();"></asp:Button>&nbsp;&nbsp;
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitle" runat="server">
    Landed Cost Vendors
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Content" runat="server" ClientIDMode="Static">
    <asp:ScriptManager ID="ScriptManager" runat="server">
    </asp:ScriptManager>
    <table width="600px" class="table tblNoBorder">
        <tr>
            <td class="normal4" align="center" colspan="2">
                <asp:Literal ID="litMessage" runat="server"></asp:Literal>
            </td>
        </tr>
        <tr>
            <td>
                <label> Vendor</label>
            </td>
            <td style="margin-bottom:10px;">
                <telerik:RadComboBox AccessKey="C" ID="radCmbCompany" Width="90%" DropDownWidth="600px"
                    Skin="Default" runat="server" AutoPostBack="True" AllowCustomText="True" ShowMoreResultsBox="true" EnableLoadOnDemand="True"
                    OnClientItemsRequested="OnClientItemsRequestedOrganization"
                    ClientIDMode="Static" TabIndex="5" OnClientLoad="onRadComboBoxLoad">
                    <WebServiceSettings Path="../common/Common.asmx" Method="GetCompanies" />
                </telerik:RadComboBox>

                
            </td>
            <td>
                <asp:Button ID="btnSave" runat="server" CssClass="btn btn-primary" Text="Add" OnClientClick="return Save();"></asp:Button>&nbsp;
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <asp:GridView ID="gvLCVendor" runat="server" Width="100%" CssClass="table table-responisve table-bordered" AllowSorting="true"
                    AutoGenerateColumns="False" DataKeyNames="numLCVendorId" style="margin-top: 10px;">
                    <AlternatingRowStyle CssClass="ais" />
                    <RowStyle CssClass="is"></RowStyle>
                    <HeaderStyle CssClass="hs"></HeaderStyle>
                    <Columns>
                        <asp:BoundField DataField="vcCompanyName" HeaderText="Vendor" ItemStyle-HorizontalAlign="Center" />
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:Button ID="btnDeleteRow" runat="server" CssClass="btn btn-danger Delete" Text="X" CommandName="DeleteVendor"
                                    CommandArgument="<%# Container.DataItemIndex %>" OnClientClick="return confirm('Are you sure to delete selected Vendor?')"></asp:Button>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <EmptyDataTemplate>
                        No records found.
                    </EmptyDataTemplate>
                </asp:GridView>
            </td>
        </tr>
    </table>
</asp:Content>
