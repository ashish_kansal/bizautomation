﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmCorrespondense.aspx.vb"
    Inherits=".frmCorresnpondense" %>

<%@ Register Src="../common/frmCorrespondence.ascx" TagName="frmCorrespondence" TagPrefix="uc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="stylesheet" href="~/CSS/master.css" type="text/css" />
    <link rel="stylesheet" type="text/css" href="../CSS/jscal2.css" />
    <link rel="stylesheet" type="text/css" href="../CSS/border-radius.css" />
    <script type="text/javascript" src="../JavaScript/jscal2.js"></script>
    <script type="text/javascript" src="../JavaScript/en.js"></script>
    <script type="text/javascript" src="../JavaScript/jquery-1.9.1.min.js"></script>
    <script type="text/javascript">
        function openActionItem(a, b, c, d, e, f) {
            if (e == 'Email') {
                window.open("../outlook/frmMailDtl.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&numEmailHstrID=" + a, '', 'titlebar=no,top=100,left=250,width=850,height=550,scrollbars=yes,resizable=yes');
                return false;
            }
            else {
                var intLocation = document.getElementById('hdnLocation').value;
                var strFormName = '';
                if (intLocation != 8) {
                    if (intLocation == 1 || intLocation == 2 || intLocation == 3 || intLocation == 4) {
                        strFormName = 'opportunitydtl';
                    }
                    else if (intLocation == 5) {
                        strFormName = 'projectdtl';
                    }
                    else if (intLocation == 6) {
                        strFormName = 'casedtl';
                    }
                    top.window.frames[1].location.href = "../admin/ActionItemDetailsOld.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=" + strFormName + "&CommId=" + a + "&CaseId=" + b + "&CaseTimeId=" + c + "&CaseExpId=" + d + "&RecordID=" + document.getElementById('hdnRecordID').value;
                    return false;
                }
                else {
                    opener.frames.document.location.href = "../admin/ActionItemDetailsOld.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=" + strFormName + "&CommId=" + a + "&CaseId=" + b + "&CaseTimeId=" + c + "&CaseExpId=" + d + "&RecordID=" + document.getElementById('hdnRecordID').value;
                    return false;
                }
            }

        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <uc1:frmCorrespondence ID="frmCorrespondence1" runat="server" ClientIDMode="Static" />
    <asp:HiddenField ID="hdnLocation" runat="server" />
    <asp:HiddenField ID="hdnRecordID" runat="server" />
    </form>
</body>
</html>
