<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" CodeBehind="frmPortalBizDocs.aspx.vb"
    Inherits="BACRM.UserInterface.Admin.frmPortalBizDocs" MasterPageFile="~/common/Popup.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Portal BizDocs</title>
    <script language="javascript" type="text/javascript">
        function Save() {
            var str = '';
            for (var i = 0; i < document.getElementById("lstAddfld").options.length; i++) {
                var SelectedText, SelectedValue;
                SelectedValue = document.getElementById("lstAddfld").options[i].value;
                SelectedText = document.getElementById("lstAddfld").options[i].text;
                str = str + SelectedValue + ','
            }
            document.getElementById("txthidden").value = str;

        }
        function MoveUp(tbox) {

            for (var i = 1; i < tbox.options.length; i++) {
                if (tbox.options[i].selected && tbox.options[i].value != "") {

                    var SelectedText, SelectedValue;
                    SelectedValue = tbox.options[i].value;
                    SelectedText = tbox.options[i].text;
                    tbox.options[i].value = tbox.options[i - 1].value;
                    tbox.options[i].text = tbox.options[i - 1].text;
                    tbox.options[i - 1].value = SelectedValue;
                    tbox.options[i - 1].text = SelectedText;
                }
            }
            return false;
        }
        function MoveDown(tbox) {

            for (var i = 0; i < tbox.options.length - 1; i++) {
                if (tbox.options[i].selected && tbox.options[i].value != "") {

                    var SelectedText, SelectedValue;
                    SelectedValue = tbox.options[i].value;
                    SelectedText = tbox.options[i].text;
                    tbox.options[i].value = tbox.options[i + 1].value;
                    tbox.options[i].text = tbox.options[i + 1].text;
                    tbox.options[i + 1].value = SelectedValue;
                    tbox.options[i + 1].text = SelectedText;
                }
            }
            return false;
        }

        sortitems = 0;  // 0-False , 1-True
        function move(fbox, tbox) {

            for (var i = 0; i < fbox.options.length; i++) {
                if (fbox.options[i].selected && fbox.options[i].value != "") {
                    /// to check for duplicates 
                    for (var j = 0; j < tbox.options.length; j++) {
                        if (tbox.options[j].value == fbox.options[i].value) {
                            alert("Item is already selected");
                            return false;
                        }
                    }

                    var no = new Option();
                    no.value = fbox.options[i].value;
                    no.text = fbox.options[i].text;
                    tbox.options[tbox.options.length] = no;
                    fbox.options[i].value = "";
                    fbox.options[i].text = "";

                }
            }
            BumpUp(fbox);
            if (sortitems) SortD(tbox);
            return false;
        }

        function remove1(fbox, tbox) {

            for (var i = 0; i < fbox.options.length; i++) {
                if (fbox.options[i].selected && fbox.options[i].value != "") {
                    /// to check for duplicates 
                    for (var j = 0; j < tbox.options.length; j++) {
                        if (tbox.options[j].value == fbox.options[i].value) {
                            fbox.options[i].value = "";
                            fbox.options[i].text = "";
                            BumpUp(fbox);
                            if (sortitems) SortD(tbox);
                            return false;


                            //alert("Item is already selected");
                            //return false;
                        }
                    }

                    var no = new Option();
                    no.value = fbox.options[i].value;
                    no.text = fbox.options[i].text;
                    tbox.options[tbox.options.length] = no;
                    fbox.options[i].value = "";
                    fbox.options[i].text = "";

                }
            }
            BumpUp(fbox);
            if (sortitems) SortD(tbox);
            return false;
        }

        function BumpUp(box) {
            for (var i = 0; i < box.options.length; i++) {
                if (box.options[i].value == "") {
                    for (var j = i; j < box.options.length - 1; j++) {
                        box.options[j].value = box.options[j + 1].value;
                        box.options[j].text = box.options[j + 1].text;
                    }
                    var ln = i;
                    break;
                }
            }
            if (ln < box.options.length) {
                box.options.length -= 1;
                BumpUp(box);
            }
        }


        function SortD(box) {
            var temp_opts = new Array();
            var temp = new Object();
            for (var i = 0; i < box.options.length; i++) {
                temp_opts[i] = box.options[i];
            }
            for (var x = 0; x < temp_opts.length - 1; x++) {
                for (var y = (x + 1); y < temp_opts.length; y++) {
                    if (temp_opts[x].text > temp_opts[y].text) {
                        temp = temp_opts[x].text;
                        temp_opts[x].text = temp_opts[y].text;
                        temp_opts[y].text = temp;
                        temp = temp_opts[x].value;
                        temp_opts[x].value = temp_opts[y].value;
                        temp_opts[y].value = temp;
                    }
                }
            }
            for (var i = 0; i < box.options.length; i++) {
                box.options[i].value = temp_opts[i].value;
                box.options[i].text = temp_opts[i].text;
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <div class="input-part">
        <div class="right-input">
            <asp:Button ID="btnSave" CssClass="button" runat="server" Text="Save"></asp:Button>
            <asp:Button ID="btnSaveAndClose" CssClass="button" runat="server" Text="Save &amp; Close">
            </asp:Button>
            <asp:Button ID="btnClose" CssClass="button" runat="server" Text="Close"></asp:Button>&nbsp;&nbsp;
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageTitle" runat="server" ClientIDMode="Static">
    Display BizDocs on Portal / WebStore
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="Content" runat="server" ClientIDMode="Static">
    <table cellpadding="0" cellspacing="0" width="600px">
        <tr>
            <td align="center" class="normal1">
                Available BizDocs<br />
                <asp:ListBox ID="lstAvailablefld" runat="server" Width="250" Height="200" CssClass="signup">
                </asp:ListBox>
            </td>
            <td align="center" valign="middle">
                <asp:Button ID="btnAdd" CssClass="button" runat="server" Text="Add >"></asp:Button>
                <br>
                <br>
                <asp:Button ID="btnRemove" CssClass="button" runat="server" Text="< Remove"></asp:Button>
            </td>
            <td align="center" class="normal1">
                BizDocs ,which are allowed to access from outside<br>
                <asp:ListBox ID="lstAddfld" runat="server" Width="250" Height="200" CssClass="signup">
                </asp:ListBox>
            </td>
        </tr>
    </table>
    <asp:TextBox Style="display: none" ID="txthidden" runat="server"></asp:TextBox>
</asp:Content>
