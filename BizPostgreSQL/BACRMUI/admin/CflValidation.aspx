﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="CflValidation.aspx.vb"
    Inherits="BACRM.UserInterface.Admin.CflValidation" MasterPageFile="~/common/Popup.Master" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title></title>
 
    <script type="text/javascript">
        function CheckNumber(cint, e) {
            var k;
            document.all ? k = e.keyCode : k = e.which;
            if (cint == 1) {
                if (!(k > 47 && k < 58 || k == 44 || k == 46 || k == 8 || k == 37 || k == 39 || k == 16)) {
                    if (e.preventDefault) {
                        e.preventDefault();
                    }
                    else
                        e.returnValue = false;
                    return false;
                }
            }
            if (cint == 2) {
                if (!(k > 47 && k < 58 || k == 8 || k == 37 || k == 39 || k == 16 || k == 0)) {
                    if (e.preventDefault) {
                        e.preventDefault();
                    }
                    else
                        e.returnValue = false;
                    return false;
                }
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <div class = "right-input">
        <div class = "input-part">
            <table id="Table2" cellspacing="0" cellpadding="0" width="100%" border="0">
                <tr>
                    <td align="right">
                        <asp:Button ID="btnSave" Text="Save & Close" CssClass="button" runat="server"></asp:Button>&nbsp;
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageTitle" runat="server" ClientIDMode="Static">
    Custom Field Validation
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="Content" runat="server" ClientIDMode="Static">
    <asp:Table ID="Table1" CellPadding="0" CellSpacing="0" BorderWidth="1" runat="server"
        Width="500px" CssClass="aspTable" BorderColor="black" GridLines="None" Height="400">
        <asp:TableRow>
            <asp:TableCell VerticalAlign="Top">
                <table border="0" cellpadding="2" cellspacing="2">
                    <tr>
                        <td class="normal1" align="left">
                            <asp:CheckBox runat="server" ID="chkIsRequired" CssClass="signup" />
                            <label for="chkIsRequired">
                                Is Required?</label>
                        </td>
                    </tr>
                    <tr id="trTextboxValidation" runat="server">
                        <td class="normal1" align="left" colspan="2">
                            <b>Validations:</b><br />
                            <asp:RadioButton ID="rbNone" Text="None" runat="server" GroupName="one" /><br />
                            <asp:RadioButton ID="rbIsNumeric" Text="Is Numeric?" runat="server" GroupName="one" /><br />
                            <asp:RadioButton ID="rbIsAlphaNumeric" Text="Is AlphaNumeric?" runat="server" GroupName="one" /><br />
                            <asp:RadioButton ID="rbIsEmail" Text="Is Email?" runat="server" GroupName="one" /><br />
                            <asp:RadioButton ID="rbRange" Text="Is Range Validation?" runat="server" GroupName="one" />&nbsp;Min
                            Value:<asp:TextBox ID="txtMin" CssClass="signup" runat="server" Width="50" />
                            &nbsp;Max Value:<asp:TextBox ID="txtMax" CssClass="signup" runat="server" Width="50" />
                        </td>
                    </tr>
                    <tr>
                        <td class="normal1" align="left">
                            <asp:CheckBox runat="server" ID="chkFieldMessage" CssClass="signup" />
                            <label for="chkFieldMessage">
                                Want to use custom error message?</label>
                            &nbsp;<asp:TextBox runat="server" ID="txtFieldMessage" CssClass="signup" Width="250"
                                MaxLength="500"></asp:TextBox>&nbsp;<small>(500 characters)</small>
                        </td>
                    </tr>
                    <tr>
                        <td class="normal1" align="left">
                            <label>
                                {$FieldName} = Name of the Field</label>
                        </td>
                    </tr>
                </table>
            </asp:TableCell></asp:TableRow>
    </asp:Table>
</asp:Content>
