﻿Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Admin

Namespace BACRM.UserInterface.Admin
    Public Class frmParentChildFildMap
        Inherits BACRMPage

        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            Try
                'CLEAR ERROR ON RELOAD
                DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ""
                DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "none")

                'CLEAR ALERT MESSAGE
                divMessage.Style.Add("display", "none")
                litMessage.Text = ""

                If Not IsPostBack Then
                    bindData()
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToLong(ex))
            End Try
        End Sub

        Sub bindData()
            Try
                Dim dtCustFld As DataTable
                Dim objCusField As New CustomFields()
                objCusField.DomainID = Session("DomainID")
                objCusField.locId = ddlChildModuleSearch.SelectedValue

                dtCustFld = objCusField.GetParentChildCustomFieldMap()

                gvFieldMap.DataSource = dtCustFld
                gvFieldMap.DataBind()
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Private Sub ddlChildModule_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlChildModule.SelectedIndexChanged
            Try
                ddlChildField.Items.Clear()

                Dim dtDynamicFIelds As DataTable
                Dim ojbCommon As New CCommon
                dtDynamicFIelds = objCommon.GetDefaultFieldForParentChildRelationship(CCommon.ToShort(ddlChildModule.SelectedValue))

                If Not dtDynamicFIelds Is Nothing And dtDynamicFIelds.Rows.Count > 0 Then
                    Dim listItem As ListItem
                    For Each dr As DataRow In dtDynamicFIelds.Rows
                        listItem = New ListItem
                        listItem.Value = dr("fld_id") & "~0"
                        listItem.Text = dr("Fld_label")
                        ddlChildField.Items.Add(listItem)
                    Next
                End If


                Dim dtCustFld As DataTable
                Dim objCusField As New CustomFields()
                objCusField.DomainID = Session("DomainID")
                objCusField.locId = ddlChildModule.SelectedValue
                dtCustFld = objCusField.CustomFieldList

                If Not dtCustFld Is Nothing AndAlso dtCustFld.Rows.Count > 0 Then
                    Dim listItem As ListItem
                    For Each dr As DataRow In dtCustFld.Rows
                        listItem = New ListItem
                        listItem.Value = dr("fld_id") & "~1"
                        listItem.Text = dr("Fld_label")
                        ddlChildField.Items.Add(listItem)
                    Next
                End If

                ddlChildField.Items.Insert(0, New ListItem("--Select--", "0"))
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToLong(ex))
            End Try
        End Sub

        Private Sub ddlParentModule_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlParentModule.SelectedIndexChanged
            Try
                ddlParentField.Items.Clear()

                Dim dtDynamicFIelds As DataTable
                Dim dtCustFld As DataTable

                Dim ojbCommon As New CCommon
                dtDynamicFIelds = objCommon.GetDefaultFieldForParentChildRelationship(CCommon.ToShort(ddlParentModule.SelectedValue))

                If Not dtDynamicFIelds Is Nothing And dtDynamicFIelds.Rows.Count > 0 Then
                    Dim listItem As ListItem
                    For Each dr As DataRow In dtDynamicFIelds.Rows
                        listItem = New ListItem
                        listItem.Value = dr("fld_id") & "~0"
                        listItem.Text = dr("Fld_label")
                        ddlParentField.Items.Add(listItem)
                    Next
                End If

                Dim objCusField As New CustomFields()
                objCusField.DomainID = Session("DomainID")
                objCusField.locId = ddlParentModule.SelectedValue
                dtCustFld = objCusField.CustomFieldList

                If Not dtCustFld Is Nothing AndAlso dtCustFld.Rows.Count > 0 Then
                    Dim listItem As ListItem
                    For Each dr As DataRow In dtCustFld.Rows
                        listItem = New ListItem
                        listItem.Value = dr("fld_id") & "~1"
                        listItem.Text = dr("Fld_label")
                        ddlParentField.Items.Add(listItem)
                    Next
                End If

                ddlParentField.Items.Insert(0, New ListItem("--Select--", "0"))
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToLong(ex))
            End Try
        End Sub

        Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
            Try
                Dim objCusField As New CustomFields()
                objCusField.DomainID = Session("DomainID")
                objCusField.ParentModule = ddlParentModule.SelectedValue
                objCusField.ParentFieldID = ddlParentField.SelectedValue.Split("~")(0)
                objCusField.ChildModule = ddlChildModule.SelectedValue
                objCusField.ChildFieldID = ddlChildField.SelectedValue.Split("~")(0)
                objCusField.Mode = 0
                objCusField.IsCustomField = CCommon.ToBool(CCommon.ToInteger(ddlParentField.SelectedValue.Split("~")(1)))
                objCusField.ManageParentChildCustomFieldMap()

                Response.Redirect("frmParentChildFieldMap.aspx")
            Catch ex As Exception
                If ex.Message = "AlreadyExists" Then
                    ShowMessage("You can't map those two fields, the child has already been mapped to a parent module field.")
                ElseIf ex.Message = "FieldTypeMisMatch" Then
                    ShowMessage("You can't map those two fields, parent & child field types don't match.")
                Else
                    ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                    DisplayError(CCommon.ToLong(ex))
                End If
            End Try
        End Sub

        Private Sub gvFieldMap_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles gvFieldMap.RowCommand
            Try
                If e.CommandName = "DeleteRow" Then
                    Dim objCusField As New CustomFields()
                    objCusField.DomainID = Session("DomainID")
                    objCusField.ParentChildFieldID = gvFieldMap.DataKeys(e.CommandArgument).Value
                    objCusField.Mode = 1

                    objCusField.ManageParentChildCustomFieldMap()

                    bindData()
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToLong(ex))
            End Try
        End Sub

        Private Sub ddlChildModuleSearch_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlChildModuleSearch.SelectedIndexChanged
            Try
                bindData()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToLong(ex))
            End Try
        End Sub

        Private Sub DisplayError(ByVal exception As String)
            Try
                DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = exception
                DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
                DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Focus()
            Catch ex As Exception

            End Try
        End Sub

        Private Sub ShowMessage(ByVal message As String)
            Try
                divMessage.Style.Add("display", "")
                litMessage.Text = message
                divMessage.Focus()
            Catch ex As Exception
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub
    End Class
End Namespace
