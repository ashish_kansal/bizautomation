<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" CodeBehind="usergroups.aspx.vb"
    Inherits="BACRM.UserInterface.Admin.frmUserGroups" MasterPageFile="~/common/GridMasterRegular.Master" %>

<%@ Register Assembly="Telerik.Web.UI, Version=2011.2.712.40, Culture=neutral, PublicKeyToken=121fae78165ba3d4" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>User Groups</title>
    <script type="text/javascript">
        function NewGroup() {

            window.open("../admin/frmNewGroup.aspx", '', 'toolbar=no,titlebar=no,left=200, top=200,width=800,height=500,scrollbars=yes,resizable=yes')
            return false;
        }
        function SelectAllCheckboxes(spanChk) {
            var oItem = spanChk.children;
            var theBox = oItem.item(0);
            xState = theBox.checked;
            elm = theBox.form.elements;
            for (i = 0; i < elm.length; i++)
                if (elm[i].type == "checkbox" && elm[i].id != theBox.id) {

                    if (elm[i].checked != xState)
                        elm[i].click();
                }
        }

        function fbValidateInputs(strElementName) {
            if (!$("[id$=chkBx_" + strElementName + "_Allowed]").is(":checked")) {
                $("[id$=rBtn_" + strElementName + "_A]").prop("checked", false);
                $("[id$=rBtn_" + strElementName + "_O]").prop("checked", false);
                $("[id$=rBtn_" + strElementName + "_T]").prop("checked", false);
            } else {
                $("[id$=rBtn_" + strElementName + "_O]").prop("checked", true);
            }
        }

        function disableUnchecked() {
            var strElementName;
            for (intElementIndex = 0; intElementIndex < document.form1.elements.length; intElementIndex++) {
                strElementName = document.form1.elements[intElementIndex].name;
                if (strElementName.substring(0, 6) == "chkBx_") {
                    strElementName = strElementName.substring(6, strElementName.length)
                    strElementName = strElementName.substring(0, strElementName.length - 8)
                    if (!document.form1.elements[intElementIndex].checked) {
                        $("[id$=rBtn_" + strElementName + "_A]").prop("checked", false);
                        $("[id$=rBtn_" + strElementName + "_O]").prop("checked", false);
                        $("[id$=rBtn_" + strElementName + "_T]").prop("checked", false);
                    }
                }
            }
        }
        function GoBack() {
            window.location.href = "../admin/userlist.aspx";
            return false;
        }
        function SelectCheckBoxes(Id, chkrights, a, b, c) {
            var moduleID = parseInt($("[id$=ddlModule]").val());

            if (document.getElementById(a) != null) {
                document.getElementById(a).checked = false
            }
            if (document.getElementById(b) != null) {
                document.getElementById(b).checked = false
            }
            if (document.getElementById(c) != null) {
                document.getElementById(c).checked = false
            }
            if (document.getElementById(Id).checked == true) {
                $("input[type=checkbox][id$=_" + chkrights + "][id^=chkBx_" + (moduleID == -1 ? "" : moduleID + "_") + "]").each(function () {
                    if (!$(this).is(":checked")) {
                        $(this).click();
                    }
                })
            }
            else {
                $("input[type=checkbox][id$=_" + chkrights + "][id^=chkBx_" + (moduleID == -1 ? "" : moduleID + "_") + "]").each(function () {
                    if ($(this).is(":checked")) {
                        $(this).click();
                    }
                })
            }
        }
        function Selectradiobutton(Id, chkrights, a, b, c) {
            var moduleID = parseInt($("[id$=ddlModule]").val());

            if (document.getElementById(a) != null) {
                document.getElementById(a).checked = false
            }
            if (document.getElementById(b) != null) {
                document.getElementById(b).checked = false
            }
            if (document.getElementById(c) != null) {
                document.getElementById(c).checked = false
            }
            if (document.getElementById(Id).checked == true) {
                $("input[type=radio][id$=_" + chkrights + "][id^=rBtn_" + (moduleID == -1 ? "" : moduleID + "_") + "]").each(function () {
                    if (!$(this).is(":checked")) {
                        $(this).click();
                    }
                })
            }
            else {
                $("input[type=radio][id$=_" + chkrights + "][id^=rBtn_" + (moduleID == -1 ? "" : moduleID + "_") + "]").each(function () {
                    if ($(this).is(":checked")) {
                        $(this).click();
                    }
                })
            }
        }
        function OpenHelp() {
            window.open('../Help/Admin-Manage_Authorization.html', '', 'toolbar=no,titlebar=no,top=200,left=200,width=550,height=350,scrollbars=yes,resizable=yes')
            return false
        }
        function NewTab() {
            window.open("../admin/frmNewTabs.aspx", '', 'toolbar=no,titlebar=no,left=200, top=200,width=600,height=500,scrollbars=yes,resizable=yes')
            return false;
        }
        function ManageSubtab() {
            var h = screen.height;
            var w = screen.width;

            window.open('../admin/frmCustomFieldTabs.aspx', 'tabs', 'width=' + (w - 100) + ',height=' + (h - 200) + ',status=no,titlebar=no,scrollbars=yes,resizable=yes,top=50,left=50');
            return false;
        }
        function SelectTabs(a) {
            window.open("../admin/frmSelectTabs.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&GroupID=" + a, '', 'toolbar=no,titlebar=no,left=100, top=50,width=1500,height=700,scrollbars=yes,resizable=yes')
            return false;
        }
        function ManageTreeNodes() {
            var h = screen.height;
            var w = screen.width;

            window.open('../admin/frmTreeNavigationAuthorization.aspx', 'TreeNodes', 'width=1200,height=' + (h - 200) + ',status=no,titlebar=no,scrollbars=yes,resizable=yes,top=110,left=150');
            return false;
        }
    </script>
    <style>
        .overlay {
            position: fixed;
            z-index: 98;
            top: 0px;
            left: 0px;
            right: 0px;
            bottom: 0px;
            background-color: rgba(170, 170, 170, 0.5);
            filter: alpha(opacity=80);
        }

        .overlayContent {
            z-index: 99;
            margin: 250px auto;
        }
    </style>
    <style>
        .info-box-icon {
            font-size: 30px !important;
        }

        .info-box-number {
            font-size: 14px !important;
            margin-top: 11px !important;
        }

        .hs {
            background-color: #ebebeb;
            font-weight: bold;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server"
    ClientIDMode="Static">
    <a href="#" class="hyperlink" title="Displaying the layout button, enables users with access to all organization pages, contacts, opportunities/orders, projects, and support to override global field configuration so they can set their own individualized field configuration & layout. Displaying the Column configuration enables the users to override global column configuration. Both are globally configured from Admin | BizForms Wizard. ">[?]</a>
    <asp:CheckBox ID="chkLayoutGridConfiguration" Checked="false" Visible="false" Text="Display layout & Column configuration button" runat="server" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <div class="table-responsive">
        <table style="width: 100%">
            <tr>
                <td align="left">
                    <telerik:RadToolTip ID="RadToolTip1" runat="server" Width="500px" ShowEvent="onmouseover"
                        RelativeTo="Element" HideEvent="LeaveTargetAndToolTip" Position="TopRight" TargetControlID="lblTooltip">
                        <div style="color: #000000; font-size: 12px;">
                            <p>
                                Organizations, referred to as Leads, Prospects, and Accounts, are considered �Stages� in the life of a
�Customer� relationship. Custom relationships are themselves derivatives of Customers (e.g. an Organization
that has a custom relationship called �Supplier� can also be a Lead, Prospect, or Account at the same time).
Manage Authorization allows you to set permissions for both of these types of dimensions. Dimension 1
which are listed as modules in the drop-down include �Leads�, �Prospects�, and �Accounts� and Dimension 2
which is listed as �Relationships�.
                            </p>
                            <br />
                            <p>
                                BizAutomation.com will always give priority to the most restrictive setting for either of these dimensions, so
for example, say you set a permission group to have view rights to �Only Owner Record� for the �Account�
module, and for the �Relationship� module you set view rights to �All Records� for the �Supplier� custom
relationship. Now say you have a Supplier called �ABC Inc.� that ALSO happens to be an Account. In this
example, employees from that permission group would be prevented from accessing �ABC Inc.�
                            </p>
                        </div>
                    </telerik:RadToolTip>
                    <asp:Label ID="lblTooltip" runat="server" CssClass="tip" Visible="false">[?]</asp:Label>
                </td>
                <td align="right"></td>
            </tr>
        </table>
        <table align="center">
            <tr>
                <td class="normal4" align="center">

                    <asp:Literal ID="litMessage" runat="server" EnableViewState="False"></asp:Literal>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server" ClientIDMode="Static">
    Permissions & Roles&nbsp;<a href="#" onclick="return OpenHelpPopUp('admin/usergroups.aspx')"><label class="badge bg-yellow">?</label></a>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BizPager" runat="server" ClientIDMode="Static">
    <div class="pull-right">
        <div class="form-inline">
            <asp:HiddenField ID="hdnGridLayoutIds" Value="" runat="server" />
            <div class="form-group">
                <label>A:</label>
                All Records
            </div>
            &nbsp;&nbsp;
            <div class="form-group">
                <label>O:</label>
                Only Owner Records
            </div>
            &nbsp;&nbsp;
            <div class="form-group">
                <label>T:</label>
                Only Territory Records
            </div>
            <asp:LinkButton ID="btnNewGroup" runat="server" CssClass="btn btn-primary"><i class="fa fa-plus-circle"></i>&nbsp;&nbsp;Manage Permission Groups</asp:LinkButton>
        </div>
    </div>
    <asp:UpdateProgress ID="UpdateProgress" runat="server" ClientIDMode="Static">
        <ProgressTemplate>
            <div class="overlay">
                <div class="overlayContent" style="color: #000; text-align: center; width: 250px; padding: 20px">
                    <i class="fa fa-2x fa-refresh fa-spin"></i>
                    <h3>Processing Request</h3>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="GridPlaceHolder" runat="server"
    ClientIDMode="Static">
    <div class="row">
        <div class="col-xs-12">
            <div class="panel with-nav-tabs panel-primary">
                <div class="panel-heading">
                    <ul class="nav nav-tabs">
                        <li class="active"><a data-toggle="tab" href="#divModuleManagement">Module Management</a></li>
                        <li><a data-toggle="tab" href="#divTabManagement">Tab Management</a></li>
                    </ul>
                </div>
                <div class="panel-body">
                    <div class="tab-content">
                        <div id="divModuleManagement" class="tab-pane fade in active">
                            <div class="row padbottom10" id="tblMesslage">
                                <div class="col-xs-12">
                                    <div class="pull-left">
                                        <div class="form-inline">
                                            <div class="form-group">
                                                <label>
                                                    Group Type:
                                                </label>
                                                Internal Access
                                            </div>
                                            &nbsp;&nbsp;
                    <div class="form-group">
                        <label>
                            Group:
                        </label>
                        <asp:DropDownList ID="ddlGroup" runat="server" AutoPostBack="True" CssClass="form-control">
                        </asp:DropDownList>
                    </div>
                                            &nbsp;&nbsp;
                    <div class="form-group">
                        <label>
                            Module:
                        </label>
                        <asp:DropDownList ID="ddlModule" runat="server" AutoPostBack="True" CssClass="form-control">
                        </asp:DropDownList>
                    </div>
                                        </div>
                                    </div>
                                    <div class="pull-right">
                                        <div class="form-inline">
                                            <div class="form-group">
                                                <label>Apply configuration to:</label>
                                                <telerik:RadComboBox ID="rcbGroups" runat="server" CheckBoxes="true"></telerik:RadComboBox>
                                            </div>
                                            <asp:LinkButton ID="btnSave" runat="server" CssClass="btn btn-primary"><i class="fa fa-floppy-o"></i>&nbsp;&nbsp;Save</asp:LinkButton>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="table-responsive">
                                <asp:Table ID="Table2" runat="server" Height="250" GridLines="None" BorderColor="black"
                                    Width="100%" CellSpacing="0" CellPadding="0">
                                    <asp:TableRow>
                                        <asp:TableCell VerticalAlign="Top">
                                            <asp:Table ID="tblAuthorizedActionsHeader" CssClass="table table-bordered" runat="server" Style="margin-bottom: 0px;">
                                                <asp:TableRow CssClass="hs">
                                                    <asp:TableCell Width="18%" HorizontalAlign="Center" RowSpan="2" CssClass="normalbol" Style="vertical-align: middle;">
                                                        <asp:Literal ID="litModuleName" runat="server">Module Name</asp:Literal>
                                                    </asp:TableCell>
                                                    <asp:TableCell Width="16%" HorizontalAlign="Center" ColumnSpan="4" Text="Viewing"
                                                        CssClass="normalbol"></asp:TableCell>
                                                    <asp:TableCell Width="16%" HorizontalAlign="Center" ColumnSpan="4" Text="Adding"
                                                        CssClass="normalbol"></asp:TableCell>
                                                    <asp:TableCell Width="16%" HorizontalAlign="Center" ColumnSpan="4" Text="Editing"
                                                        CssClass="normalbol"></asp:TableCell>
                                                    <asp:TableCell Width="16%" HorizontalAlign="Center" ColumnSpan="4" Text="Deleting"
                                                        CssClass="normalbol"></asp:TableCell>
                                                    <asp:TableCell Width="16%" HorizontalAlign="Center" ColumnSpan="4" Text="Export / Import"
                                                        CssClass="normalbol"></asp:TableCell>
                                                </asp:TableRow>
                                                <asp:TableRow>
                                                    <asp:TableCell Width="4%" HorizontalAlign="Center" RowSpan="2" Text="Y/N " CssClass="normalbol">
                                                        <asp:CheckBox ID="CheckAll" onclick="javascript:SelectCheckBoxes('CheckAll','intViewAllowed_Allowed','Checkbox3','Checkbox7','Checkbox10');"
                                                            runat="server"></asp:CheckBox>
                                                    </asp:TableCell>
                                                    <asp:TableCell Width="4%" HorizontalAlign="Center" RowSpan="2" Text="A " CssClass="normalbol">
                                                        <asp:CheckBox ID="Checkbox3" onclick="javascript:Selectradiobutton('Checkbox3','intViewAllowed_A','CheckAll','Checkbox7','Checkbox10');"
                                                            runat="server"></asp:CheckBox>
                                                    </asp:TableCell>
                                                    <asp:TableCell Width="4%" HorizontalAlign="Center" RowSpan="2" Text="O " CssClass="normalbol">
                                                        <asp:CheckBox ID="Checkbox7" onclick="javascript:Selectradiobutton('Checkbox7','intViewAllowed_O','Checkbox3','CheckAll','Checkbox10');"
                                                            runat="server"></asp:CheckBox>
                                                    </asp:TableCell>
                                                    <asp:TableCell Width="4%" HorizontalAlign="Center" Text="T " CssClass="normalbol">
                                                        <asp:CheckBox ID="Checkbox10" onclick="javascript:Selectradiobutton('Checkbox10','intViewAllowed_T','Checkbox3','Checkbox7','CheckAll');"
                                                            runat="server"></asp:CheckBox>
                                                    </asp:TableCell>
                                                    <asp:TableCell Width="4%" HorizontalAlign="Center" RowSpan="2" Text="Y/N " CssClass="normalbol">
                                                        <asp:CheckBox ID="Checkbox14" onclick="javascript:SelectCheckBoxes('Checkbox14','intAddAllowed_Allowed','Checkbox20','Checkbox21','Checkbox22');"
                                                            runat="server"></asp:CheckBox>
                                                    </asp:TableCell>
                                                    <asp:TableCell Width="4%" HorizontalAlign="Center" RowSpan="2" Text="A " CssClass="normalbol">
                                                        <asp:CheckBox ID="Checkbox20" onclick="javascript:Selectradiobutton('Checkbox20','intAddAllowed_A','Checkbox14','Checkbox21','Checkbox22');"
                                                            runat="server"></asp:CheckBox>
                                                    </asp:TableCell>
                                                    <asp:TableCell Width="4%" HorizontalAlign="Center" RowSpan="2" Text="O " CssClass="normalbol">
                                                        <asp:CheckBox ID="Checkbox21" onclick="javascript:Selectradiobutton('Checkbox21','intAddAllowed_O','Checkbox20','Checkbox14','Checkbox22');"
                                                            runat="server"></asp:CheckBox>
                                                    </asp:TableCell>
                                                    <asp:TableCell Width="4%" HorizontalAlign="Center" Text="T " CssClass="normalbol">
                                                        <asp:CheckBox ID="Checkbox22" onclick="javascript:Selectradiobutton('Checkbox22','intAddAllowed_T','Checkbox14','Checkbox21','Checkbox20');"
                                                            runat="server"></asp:CheckBox>
                                                    </asp:TableCell>
                                                    <asp:TableCell Width="4%" HorizontalAlign="Center" RowSpan="2" Text="Y/N " CssClass="normalbol">
                                                        <asp:CheckBox ID="Checkbox23" onclick="javascript:SelectCheckBoxes('Checkbox23','intUpdateAllowed_Allowed','Checkbox24','Checkbox25','Checkbox26');"
                                                            runat="server"></asp:CheckBox>
                                                    </asp:TableCell>
                                                    <asp:TableCell Width="4%" HorizontalAlign="Center" RowSpan="2" Text="A " CssClass="normalbol">
                                                        <asp:CheckBox ID="Checkbox24" onclick="javascript:Selectradiobutton('Checkbox24','intUpdateAllowed_A','Checkbox23','Checkbox25','Checkbox26');"
                                                            runat="server"></asp:CheckBox>
                                                    </asp:TableCell>
                                                    <asp:TableCell Width="4%" HorizontalAlign="Center" RowSpan="2" Text="O " CssClass="normalbol">
                                                        <asp:CheckBox ID="Checkbox25" onclick="javascript:Selectradiobutton('Checkbox25','intUpdateAllowed_O','Checkbox24','Checkbox23','Checkbox26');"
                                                            runat="server"></asp:CheckBox>
                                                    </asp:TableCell>
                                                    <asp:TableCell Width="4%" HorizontalAlign="Center" Text="T " CssClass="normalbol">
                                                        <asp:CheckBox ID="Checkbox26" onclick="javascript:Selectradiobutton('Checkbox26','intUpdateAllowed_T','Checkbox24','Checkbox25','Checkbox23');"
                                                            runat="server"></asp:CheckBox>
                                                    </asp:TableCell>
                                                    <asp:TableCell Width="4%" HorizontalAlign="Center" RowSpan="2" Text="Y/N " CssClass="normalbol">
                                                        <asp:CheckBox ID="Checkbox27" onclick="javascript:SelectCheckBoxes('Checkbox27','intDeleteAllowed_Allowed','Checkbox28','Checkbox29','Checkbox30');"
                                                            runat="server"></asp:CheckBox>
                                                    </asp:TableCell>
                                                    <asp:TableCell Width="4%" HorizontalAlign="Center" RowSpan="2" Text="A " CssClass="normalbol">
                                                        <asp:CheckBox ID="Checkbox28" onclick="javascript:Selectradiobutton('Checkbox28','intDeleteAllowed_A','Checkbox27','Checkbox29','Checkbox30');"
                                                            runat="server"></asp:CheckBox>
                                                    </asp:TableCell>
                                                    <asp:TableCell Width="4%" HorizontalAlign="Center" RowSpan="2" Text="O " CssClass="normalbol">
                                                        <asp:CheckBox ID="Checkbox29" onclick="javascript:Selectradiobutton('Checkbox29','intDeleteAllowed_O','Checkbox28','Checkbox27','Checkbox30');"
                                                            runat="server"></asp:CheckBox>
                                                    </asp:TableCell>
                                                    <asp:TableCell Width="4%" HorizontalAlign="Center" Text="T " CssClass="normalbol">
                                                        <asp:CheckBox ID="Checkbox30" onclick="javascript:Selectradiobutton('Checkbox30','intDeleteAllowed_T','Checkbox28','Checkbox29','Checkbox27');"
                                                            runat="server"></asp:CheckBox>
                                                    </asp:TableCell>
                                                    <asp:TableCell Width="4%" HorizontalAlign="Center" RowSpan="2" Text="Y/N " CssClass="normalbol">
                                                        <asp:CheckBox ID="Checkbox31" onclick="javascript:SelectCheckBoxes('Checkbox31','intExportAllowed_Allowed','Checkbox32','Checkbox33','Checkbox34');"
                                                            runat="server"></asp:CheckBox>
                                                    </asp:TableCell>
                                                    <asp:TableCell Width="4%" HorizontalAlign="Center" RowSpan="2" Text="A " CssClass="normalbol">
                                                        <asp:CheckBox ID="Checkbox32" onclick="javascript:Selectradiobutton('Checkbox32','intExportAllowed_A','Checkbox31','Checkbox33','Checkbox34');"
                                                            runat="server"></asp:CheckBox>
                                                    </asp:TableCell>
                                                    <asp:TableCell Width="4%" HorizontalAlign="Center" RowSpan="2" Text="O " CssClass="normalbol">
                                                        <asp:CheckBox ID="Checkbox33" onclick="javascript:Selectradiobutton('Checkbox33','intExportAllowed_O','Checkbox32','Checkbox31','Checkbox34');"
                                                            runat="server"></asp:CheckBox>
                                                    </asp:TableCell>
                                                    <asp:TableCell Width="4%" HorizontalAlign="Center" Text="T " CssClass="normalbol">
                                                        <asp:CheckBox ID="Checkbox34" onclick="javascript:Selectradiobutton('Checkbox34','intExportAllowed_T','Checkbox32','Checkbox33','Checkbox31');"
                                                            runat="server"></asp:CheckBox>
                                                    </asp:TableCell>
                                                </asp:TableRow>
                                            </asp:Table>
                                            <asp:Table ID="tblAuthorizedActionsList" runat="server" Width="100%" CellSpacing="0" CellPadding="0" EnableViewState="False" CssClass="table table-striped table-bordered table-hover">
                                            </asp:Table>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                </asp:Table>
                            </div>
                        </div>
                        <div id="divTabManagement" class="tab-pane fade">
                            <div class="table-responsive">
                                <asp:Table ID="table7" CellPadding="0" CellSpacing="0" runat="server"
                                    Width="100%" CssClass="aspTable" GridLines="None" Height="350">
                                    <asp:TableRow>
                                        <asp:TableCell VerticalAlign="Top">
                                            <table width="600px">
                                                <tr class="normal1" align="right" style="display: none">
                                                    <td align="left" class="normal1" colspan="2">
                                                        <div style="padding: 20px; margin-bottom: 5px; margin-left: 25px; border: solid 1px lightgray; overflow: hidden;">
                                                            <div style="width: 50%; float: left; text-align: center;">
                                                                <div style="vertical-align: central; padding: 12px;">
                                                                    <asp:Image ID="imgMainTab" runat="server" ImageUrl="~/images/MainTabLabel.png" />
                                                                </div>
                                                            </div>
                                                            <div style="width: 50%; float: right;">
                                                                <div style="vertical-align: central; padding: 25px;">
                                                                    <asp:HyperLink ID="hplEditMainTabs" runat="server" CssClass="hyperlink" Text="Edit Main Tab Labels"
                                                                        onclick="return NewTab();">
                                                                    </asp:HyperLink>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left" class="normal1" colspan="2">
                                                        <div style="padding: 20px; margin-bottom: 5px; margin-left: 25px; border: solid 1px lightgray; overflow: hidden;">
                                                            <div style="width: 50%; float: left; text-align: center;">
                                                                <div style="vertical-align: central; padding: 10px;">
                                                                    <asp:Image ID="Image1" runat="server" ImageUrl="~/images/ShowMainTab.png" />
                                                                </div>
                                                            </div>
                                                            <div style="width: 50%; float: right;">
                                                                <div style="vertical-align: central; padding: 25px;">
                                                                    Main Tabs: 
                                                    <asp:HyperLink ID="hplHideMainTabs" runat="server" CssClass="hyperlink" Text="Edit Labels & Hide/Show"
                                                        onclick="return SelectTabs(0);">
                                                    </asp:HyperLink>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left">
                                                        <div style="padding: 20px; margin-bottom: 5px; margin-left: 25px; border: solid 1px lightgray; overflow: hidden;">
                                                            <div style="width: 50%; float: left; text-align: center;">
                                                                <div style="vertical-align: central; padding: 10px;">
                                                                    <asp:Image ID="Image2" runat="server" ImageUrl="~/images/Subtab.png" />
                                                                </div>
                                                            </div>
                                                            <div style="width: 50%; float: right;">
                                                                <div style="vertical-align: central; padding: 25px;">
                                                                    <asp:HyperLink ID="hplManageSubTabs" runat="server" CssClass="hyperlink" Text="Manage Sub-tabs"
                                                                        onclick="return ManageSubtab();">
                                                                    </asp:HyperLink>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left" class="normal1" colspan="2">
                                                        <div style="padding: 20px; margin-bottom: 5px; margin-left: 25px; border: solid 1px lightgray; overflow: hidden;">
                                                            <div style="width: 50%; float: left; text-align: center;">
                                                                <div style="vertical-align: central;">
                                                                    <asp:Image ID="Image4" runat="server" ImageUrl="~/images/TreeNodes.png" />
                                                                </div>
                                                            </div>
                                                            <div style="width: 50%; float: right;">
                                                                <div style="vertical-align: central; padding: 25px;">
                                                                    <asp:HyperLink ID="hplManageTreeNodePerission" runat="server" CssClass="hyperlink"
                                                                        Text="Default Page Configure & Hide / Show Navigation" onclick="return ManageTreeNodes();">
                                                                    </asp:HyperLink>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                </asp:Table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
