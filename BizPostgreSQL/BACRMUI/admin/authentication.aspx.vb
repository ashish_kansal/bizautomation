Imports BACRM.BusinessLogic.Common
Namespace BACRM.UserInterface.Admin
    Public Class authentication
        Inherits BACRMPage

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            'Put user code to initialize the page here
            lblModuleName.Text = GetQueryStringVal("Module")
            lblPermissionName.Text = GetQueryStringVal("Permission")

            If lblModuleName.Text.Trim.Length > 0 And lblPermissionName.Text.Trim.Length > 0 Then
                dvPermission.Visible = True
            End If
        End Sub
    End Class
End Namespace