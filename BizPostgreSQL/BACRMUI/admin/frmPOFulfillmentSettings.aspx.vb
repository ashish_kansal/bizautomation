﻿Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Opportunities

Public Class frmPOFulfillmentSettings
    Inherits BACRMPage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            BindData()
            BindDropDowns()
            BindBizDocsTemplate()
            'BindBizDocs()
        End If
    End Sub

    Private Sub btnSaveClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveClose.Click
        If Page.IsValid Then
            Try
                Save()
                Dim strScript As String = "<script language=JavaScript>self.close()</script>"
                If (Not Page.IsStartupScriptRegistered("clientScript")) Then
                    Page.RegisterStartupScript("clientScript", strScript)
                End If

            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End If
    End Sub

    Sub BindBizDocsTemplate()
        Try
            Dim objOppBizDoc As New OppBizDocs
            objOppBizDoc.DomainID = Session("DomainID")
            objOppBizDoc.BizDocId = 644
            objOppBizDoc.OppType = 2
            objOppBizDoc.byteMode = 0

            Dim dtBizDocTemplate As DataTable = objOppBizDoc.GetBizDocTemplateList()

            ddlBillTemplate.DataSource = dtBizDocTemplate
            ddlBillTemplate.DataTextField = "vcTemplateName"
            ddlBillTemplate.DataValueField = "numBizDocTempID"
            ddlBillTemplate.DataBind()
            ddlBillTemplate.Items.Insert(0, New ListItem("--Select--", 0))

            'objOppBizDoc.byteMode = 1


            'objOppBizDoc.Relationship = CCommon.ToLong(hdnReplationship.Value)
            'objOppBizDoc.Profile = CCommon.ToLong(hdnProfile.Value)
            'objOppBizDoc.AccountClass = CCommon.ToLong(hdnAccountClass.Value)
            'Dim dtDefaultBizDocTemplate As DataTable = objOppBizDoc.GetBizDocTemplateList()

            'If Not IsDBNull(dtDefaultBizDocTemplate.Rows(0).Item("numBizDocTempID")) Then
            '    If Not ddlBizDocTemplate.Items.FindByValue(dtDefaultBizDocTemplate.Rows(0).Item("numBizDocTempID")) Is Nothing Then
            '        ddlBizDocTemplate.ClearSelection()
            '        ddlBizDocTemplate.SelectedValue = dtDefaultBizDocTemplate.Rows(0).Item("numBizDocTempID").ToString
            '    End If
            'End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub BindDropDowns()
        Try
            objCommon.DomainID = Session("DomainID")
            objCommon.ListID = 176
            Dim dtPO As New DataTable
            dtPO = objCommon.GetMasterListItemsWithRights
            If dtPO IsNot Nothing AndAlso dtPO.Rows.Count > 0 Then
                Dim dvPO As DataView = dtPO.DefaultView
                dvPO.RowFilter = "numListType=2 AND tintOppOrOrder=2"

                ddlPartialQty.DataSource = dvPO
                ddlPartialQty.DataTextField = "vcData"
                ddlPartialQty.DataValueField = "numListItemID"
                ddlPartialQty.DataBind()
                ddlPartialQty.Items.Insert(0, New ListItem("--Select--", 0))

                ddlAllQty.DataSource = dvPO
                ddlAllQty.DataTextField = "vcData"
                ddlAllQty.DataValueField = "numListItemID"
                ddlAllQty.DataBind()
                ddlAllQty.Items.Insert(0, New ListItem("--Select--", 0))
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    'Sub BindBizDocs()
    '    Try
    '        objCommon.DomainID = Session("DomainID")
    '        objCommon.ListID = 176
    '        Dim dt As New DataTable
    '        dt = objCommon.GetBizDocsForPOFulfillmentSettings

    '        ddlBizDoc.DataSource = dt
    '        ddlBizDoc.DataTextField = "vcData"
    '        ddlBizDoc.DataValueField = "numListItemID"
    '        ddlBizDoc.DataBind()
    '        ddlBizDoc.Items.Insert(0, New ListItem("--Select--", 0))

    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Sub

    Private Sub Save()
        Try
            objCommon.DomainID = Session("DomainID")

            If ddlPartialQty.SelectedIndex <> 0 Then
                objCommon.PartialReceiveOrderStatus = CCommon.ToLong(ddlPartialQty.SelectedValue)
                objCommon.BitPartialQtyOrderStatus = chkPartialQty.Checked
            ElseIf ddlAllQty.SelectedIndex <> 0 Then
                objCommon.FullReceiveOrderStatus = CCommon.ToLong(ddlAllQty.SelectedValue)
                objCommon.BitAllQtyOrderStatus = chkAllQty.Checked
                'If ddlBizDoc.SelectedIndex <> 0 Then
                '    objCommon.BizDocID = CCommon.ToLong(ddlBizDoc.SelectedValue)
                '    objCommon.BitAddBizDoc = chkAddBizDoc.Checked
                'End If
                objCommon.BitClosePO = chkClosePurchaseOrder.Checked
            End If
            objCommon.UserCntID = CCommon.ToLong(Session("UserContactID"))
            objCommon.UpdatePOFulfillmentSettings(ddlBillTemplate.SelectedValue)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Sub BindData()
        Try
            Dim dtTable As DataTable
            objCommon.DomainID = Session("DomainID")
            dtTable = objCommon.GetPOFulfillmentSettings

            If (dtTable IsNot Nothing AndAlso dtTable.Rows.Count > 0) Then
                If dtTable.Rows(0).Item("numBizDocID") IsNot Nothing AndAlso dtTable.Rows(0).Item("numBizDocID") IsNot DBNull.Value AndAlso CCommon.ToLong(dtTable.Rows(0).Item("numBizDocID")) <> 0 Then
                    ddlBillTemplate.SelectedValue = CCommon.ToLong(dtTable.Rows(0).Item("numBizDocID"))
                End If

                If dtTable.Rows(0).Item("bitPartialQtyOrderStatus") IsNot Nothing AndAlso CCommon.ToBool(dtTable.Rows(0).Item("bitPartialQtyOrderStatus")) = True Then
                    chkPartialQty.Checked = True
                    If dtTable.Rows(0).Item("numPartialReceiveOrderStatus") IsNot Nothing Then
                        ddlPartialQty.SelectedValue = CCommon.ToLong(dtTable.Rows(0).Item("numPartialReceiveOrderStatus"))
                    End If
                ElseIf dtTable.Rows(0).Item("bitAllQtyOrderStatus") IsNot Nothing AndAlso CCommon.ToBool(dtTable.Rows(0).Item("bitAllQtyOrderStatus")) = True Then
                    chkAllQty.Checked = True
                    If dtTable.Rows(0).Item("numFullReceiveOrderStatus") IsNot Nothing Then
                        ddlAllQty.SelectedValue = CCommon.ToLong(dtTable.Rows(0).Item("numFullReceiveOrderStatus"))
                    End If
                    'If dtTable.Rows(0).Item("bitBizDoc") IsNot Nothing AndAlso CCommon.ToBool(dtTable.Rows(0).Item("bitBizDoc")) = True Then
                    '    chkAddBizDoc.Checked = True
                    '    If dtTable.Rows(0).Item("numBizDocID") IsNot Nothing Then
                    '        ddlBizDoc.SelectedValue = CCommon.ToLong(dtTable.Rows(0).Item("numBizDocID"))
                    '    End If
                    'End If
                    If dtTable.Rows(0).Item("bitClosePO") IsNot Nothing AndAlso CCommon.ToBool(dtTable.Rows(0).Item("bitClosePO")) = True Then
                        chkClosePurchaseOrder.Checked = True
                    End If
                End If
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
End Class