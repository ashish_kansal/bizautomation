Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.common
Imports BACRM.BusinessLogic.Contacts

Namespace BACRM.UserInterface.Admin
    Partial Public Class frmAdvSurveyRes
        Inherits BACRMPage
        Dim strColumn As String
        Dim boolExport As Boolean
        Dim objCommon As CCommon

        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            Try
                'CLEAR ERROR ON RELOAD
                DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ""
                DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "none")

                'CLEAR ALERT MESSAGE
                divMessage.Style.Add("display", "none")
                litMessage.Text = ""

                If Not IsPostBack Then
                    txtCurrrentPage.Text = 1
                    BindGridView(True)
                End If

                If txtReload.Text = "true" Then
                    BindGridView(True)
                    txtReload.Text = "false"
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Sub BindGridView(ByVal CreateCol As Boolean)
            Try
                Dim objAdmin As New FormGenericAdvSearch
                Dim dtResults As DataTable

                With objAdmin
                    .QueryWhereCondition = Session("WhereConditionSurvey")
                    .DomainID = Session("DomainID")
                    .UserCntID = Session("UserContactID")
                    .PageSize = Session("PagingRows")
                    .FormID = 19
                    .DisplayColumns = CCommon.ToString(GetQueryStringVal("displayColumns"))
                    .SortCharacter = txtSortChar.Text.Trim()
                    .SortcolumnName = txtSortColumn.Text
                    If txtCurrrentPage.Text.Trim = "" Then txtCurrrentPage.Text = 1
                    .CurrentPage = txtCurrrentPage.Text.Trim()
                    .PageSize = Session("PagingRows")
                    .TotalRecords = 0
                    If txtSortOrder.Text = "D" Then
                        .columnSortOrder = "Desc"
                    Else : .columnSortOrder = "Asc"
                    End If
                    If Not IsPostBack Then
                        ddlSort.DataSource = .getSearchFieldList.Tables(1)              'Set the datasource for the available fields
                        ddlSort.DataTextField = "vcFormFieldName"                       'set the text field
                        ddlSort.DataValueField = "vcDbColumnName"                       'set the value attribut
                        ddlSort.DataBind()
                    End If
                    .columnName = ddlSort.SelectedValue

                    If boolExport = True Then
                        .GetAll = True
                        dtResults = .AdvancedSearchSurvey()
                        Dim iExportCount As Integer
                        dtResults.Columns.RemoveAt(4)
                        dtResults.Columns.RemoveAt(3)
                        dtResults.Columns.RemoveAt(2)
                        dtResults.Columns.RemoveAt(1)
                        dtResults.Columns.RemoveAt(0)
                        Response.Clear()
                        For iExportCount = 0 To dtResults.Columns.Count - 1
                            dtResults.Columns(iExportCount).ColumnName = dtResults.Columns(iExportCount).ColumnName.Split("~")(0)
                        Next
                        For Each column As DataColumn In dtResults.Columns
                            Response.Write(column.ColumnName + ",")
                        Next
                        Response.Write(Environment.NewLine)
                        For Each row As DataRow In dtResults.Rows
                            For iExportCount = 0 To dtResults.Columns.Count - 1
                                Response.Write(row(iExportCount).ToString().Replace(";", String.Empty) & ",")
                            Next
                            Response.Write(Environment.NewLine)
                        Next
                        Response.ContentType = "text/csv"
                        Response.AppendHeader("Content-Disposition", "attachment; filename=" + "File" + Format(Now, "ddmmyyyyhhmmss") + ".csv")
                        Response.End()
                        .GetAll = False
                    End If

                    .GetAll = False
                    dtResults = .AdvancedSearchSurvey()

                    bizPager.PageSize = Session("PagingRows")
                    bizPager.CurrentPageIndex = txtCurrrentPage.Text
                    bizPager.RecordCount = objAdmin.TotalRecords
                End With
                Dim i As Integer
                If CreateCol = True Then
                    Dim bField As BoundField
                    Dim Tfield As TemplateField

                    For i = 0 To dtResults.Columns.Count - 1
                        If dtResults.Columns(i).ColumnName = "Contact Email" Or dtResults.Columns(i).ColumnName = "Organization Name" Or dtResults.Columns(i).ColumnName = "First Name" Or dtResults.Columns(i).ColumnName = "Last Name" Then
                            Tfield = New TemplateField
                            Dim str As String()
                            str = dtResults.Columns(i).ColumnName.Split("~")

                            Tfield.HeaderTemplate = New MyTemp(ListItemType.Header, 19, str(0), str(1), dtResults.Columns(0).ColumnName, dtResults.Columns(1).ColumnName, dtResults.Columns(4).ColumnName)
                            Tfield.ItemTemplate = New MyTemp(ListItemType.Item, 19, str(0), str(1), dtResults.Columns(0).ColumnName, dtResults.Columns(1).ColumnName, dtResults.Columns(4).ColumnName)
                            gvSearch.Columns.Add(Tfield)
                        Else
                            If i < 5 Then
                                bField = New BoundField
                                bField.DataField = dtResults.Columns(i).ColumnName
                                bField.Visible = False
                                gvSearch.Columns.Add(bField)
                            Else
                                Tfield = New TemplateField
                                Dim str As String()
                                str = dtResults.Columns(i).ColumnName.Split("~")
                                Tfield.HeaderTemplate = New MyTemp(ListItemType.Header, 19, str(0), str(1), dtResults.Columns(0).ColumnName, dtResults.Columns(1).ColumnName, dtResults.Columns(4).ColumnName)

                                Tfield.ItemTemplate = New MyTemp(ListItemType.Item, 19, str(0), str(1), dtResults.Columns(0).ColumnName, dtResults.Columns(1).ColumnName, dtResults.Columns(4).ColumnName)
                                gvSearch.Columns.Add(Tfield)
                            End If
                        End If
                    Next
                    'If i >= 5 Then
                    Tfield = New TemplateField
                    Tfield.HeaderTemplate = New MyTemp(ListItemType.Header, 19, "CheckBox", "", dtResults.Columns(0).ColumnName, dtResults.Columns(1).ColumnName, dtResults.Columns(4).ColumnName)
                    Tfield.ItemTemplate = New MyTemp(ListItemType.Item, 19, "CheckBox", "", dtResults.Columns(0).ColumnName, dtResults.Columns(1).ColumnName, dtResults.Columns(4).ColumnName)
                    gvSearch.Columns.Add(Tfield)
                    'End If
                End If
                gvSearch.DataSource = dtResults
                gvSearch.DataBind()
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub btnPrepareMsg_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPrepareMsg.Click
            Try
                Session("ContIDs") = txtContactId.Text.ToString.TrimEnd(",")
                Session("EMailCampCondition") = " and ADC.numDomainId=" & Session("DomainId")
                Response.Redirect("../Marketing/frmEmailBroadCasting.aspx?PC=false&SAll=0")
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Private Sub gvSearch_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvSearch.RowDataBound
            Try
                If e.Row.RowType = DataControlRowType.DataRow Then
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub btnExportToExcel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExportToExcel.Click
            Try
                boolExport = True
                BindGridView(True)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Private Sub btnGo1_Click(sender As Object, e As System.EventArgs) Handles btnGo1.Click
            Try
                BindGridView(True)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Protected Sub bizPager_PageChanged(sender As Object, e As EventArgs)
            Try
                txtCurrrentPage.Text = bizPager.CurrentPageIndex
                BindGridView(True)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Private Sub DisplayError(ByVal exception As String)
            Try
                DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = exception
                DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
                DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Focus()
            Catch ex As Exception

            End Try
        End Sub

        Private Sub ShowMessage(ByVal message As String)
            Try
                divMessage.Style.Add("display", "")
                litMessage.Text = message
                divMessage.Focus()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub
    End Class
End Namespace