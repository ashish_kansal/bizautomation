﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmContactTypeMapping.aspx.vb"
    Inherits=".frmContactTypeMapping" MasterPageFile="~/common/Popup.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Contact Type Mapping</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <div class="input-part">
        <div class="right-input">
            <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save"></asp:Button>&nbsp;
            <asp:Button ID="btnClose" runat="server" CssClass="button" Text="Close" OnClientClick="return Close();">
            </asp:Button>&nbsp;
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageTitle" runat="server" ClientIDMode="Static">
    Contact Type Mapping
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="Content" runat="server" ClientIDMode="Static">
    <table width="600px">
        <tr>
            <td class="normal4" align="center" colspan="2">
                <asp:Literal ID="litMessage" runat="server"></asp:Literal>
            </td>
        </tr>
        <tr>
            <td class="normal1" align="right">
                Contact Type
            </td>
            <td>
                <asp:DropDownList runat="server" ID="ddlContactType" CssClass="signup" Width="350"
                    AutoPostBack="true">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="normal1" align="right">
                Liability
            </td>
            <td class="normal1">
                <asp:DropDownList ID="ddlAccount" runat="server" CssClass="signup" Width="350">
                </asp:DropDownList>
            </td>
        </tr>
    </table>
</asp:Content>
