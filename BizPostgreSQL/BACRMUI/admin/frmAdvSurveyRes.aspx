<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmAdvSurveyRes.aspx.vb"
    Inherits="BACRM.UserInterface.Admin.frmAdvSurveyRes" MasterPageFile="~/common/GridMasterRegular.Master" %>

<%@ Register TagPrefix="menu1" TagName="webmenu" Src="../include/webmenu.ascx" %>
<%@ Register Assembly="AspNetPager, Version=7.4.4.0, Culture=neutral, PublicKeyToken=fb0a0fe055d40fd4" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>
<%@ Register Src="../common/frmBizSorting.ascx" TagName="frmBizSorting" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Survey Search Result</title>
    <script type="text/javascript">
        function EditSearchView() {
            window.open('../admin/frmAdvSearchColumnCustomization.aspx?FormId=19', "", "width=500,height=300,status=no,scrollbars=yes,left=155,top=160")
            return false;
        }

        function Back() {
            document.location.href = "../admin/frmAdvSurvey.aspx"
            return false;
        }

        function PrepareMessage() {
            var RecordIDs = '';
            $("[id$=gvSearch] tr").each(function () {
                if ($(this).find("#chk").is(':checked')) {
                    RecordIDs = RecordIDs + $(this).find("#lbl1").text() + ',';
                }
            });
            RecordIDs = RecordIDs.substring(0, RecordIDs.length - 1);
            document.getElementById('txtContactId').value = RecordIDs;

            if (RecordIDs.length > 0)
                return true;
            else {
                alert('Please select atleast one record!!');
                return false;
            }
        }

        function FilterWithinRecords(labelName, DBColumnName, divName) {
            return false
        }

        function OpenWindow(a, b, c) {
            var str;
            if (b == 0) {
                str = "../Leads/frmLeads.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=AdvSearchSur&DivID=" + a;
            }
            else if (b == 1) {
                str = "../prospects/frmProspects.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=AdvSearchSur&DivID=" + a;
            }
            else if (b == 2) {
                str = "../account/frmAccounts.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=AdvSearchSur&klds+7kldf=fjk-las&DivId=" + a;
            }

            document.location.href = str;
        }

        function OpenContact(a, b) {
            var str;
            str = "../contact/frmContacts.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=AdvSearchSur&ft6ty=oiuy&CntId=" + a;
            document.location.href = str;
        }

        function OpemEmail(a, b) {
            window.open('../contact/frmComposeWindow.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&Lsemail=' + a + '&pqwRT=' + b, 'ComposeWindow', 'toolbar=no,titlebar=no,top=100,left=100,width=1200,height=645,scrollbars=yes,resizable=yes')
            return false;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server"
    ClientIDMode="Static">
    <div id="divMessage" runat="server" class="row padbottom10" style="display: none">
        <div class="col-xs-12">
            <div class="alert alert-warning">
                <h4><i class="icon fa fa-warning"></i>Alert!</h4>
                <asp:Literal ID="litMessage" runat="server" EnableViewState="False"></asp:Literal>
            </div>
        </div>
    </div>
    <div class="row padbottom10">
        <div class="col-xs-12">
            <div class="pull-right">
                <div class="form-inline">
                    <asp:DropDownList ID="ddlSort" runat="server" CssClass="form-control">
                    </asp:DropDownList>
                    <asp:Button ID="btnPrepareMsg" OnClientClick="return PrepareMessage()" runat="server" Text="Prepare Message" CssClass="btn btn-primary" CausesValidation="False"></asp:Button>
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server" style="display: inline">
                        <ContentTemplate>
                            <asp:Button ID="btnExportToExcel" runat="server" Text="Export to Excel" CssClass="btn btn-primary" CausesValidation="False"></asp:Button>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <asp:Button ID="btnBackToSearchCriteria" OnClientClick="return Back()" Text="Back" CssClass="btn btn-primary" runat="server" CausesValidation="False"></asp:Button>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server" ClientIDMode="Static">
    Survey Search
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BizPager" runat="server" ClientIDMode="Static">
    <webdiyer:AspNetPager ID="bizPager" runat="server"
        PagingButtonSpacing="0"
        CssClass="bizgridpager"
        AlwaysShow="true"
        CurrentPageButtonClass="active"
        PagingButtonUlLayoutClass="pagination"
        PagingButtonLayoutType="UnorderedList"
        FirstPageText="<<"
        LastPageText=">>"
        NextPageText=">"
        PrevPageText="<"
        Width="100%"
        UrlPaging="false"
        NumericButtonCount="8"
        ShowPageIndexBox="Never"
        ShowCustomInfoSection="Left"
        OnPageChanged="bizPager_PageChanged"
        CustomInfoHTML="Showing records %StartRecordIndex% to %EndRecordIndex% of %RecordCount% "
        CustomInfoClass="bizpagercustominfo">
    </webdiyer:AspNetPager>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="GridPlaceHolder" runat="server"
    ClientIDMode="Static">
    <asp:UpdateProgress ID="UpdateProgress" runat="server" ClientIDMode="Static">
        <ProgressTemplate>
            <div class="overlay">
                <div class="overlayContent" style="color: #000; text-align: center; width: 250px; padding: 20px">
                    <i class="fa fa-2x fa-refresh fa-spin"></i>
                    <h3>Processing Request</h3>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <div class="row">
        <div class="col-xs-12">
            <div class="table-responsive">
                <asp:GridView ID="gvSearch" runat="server" EnableViewState="true" AutoGenerateColumns="false" CssClass="table table-bordered table-striped" Width="100%">
                    <Columns>
                    </Columns>
                </asp:GridView>
            </div>
        </div>
    </div>
    <asp:TextBox ID="txtTotalPage" Style="display: none" runat="server"></asp:TextBox>
    <asp:TextBox ID="txtTotalRecords" Style="display: none" runat="server"></asp:TextBox>
    <asp:TextBox ID="txtSortChar" Text="0" Style="display: none" runat="server"></asp:TextBox>
    <asp:TextBox ID="txtSortColumn" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtSortOrder" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtReload" runat="server" Style="display: none">False</asp:TextBox>
    <asp:TextBox ID="txtContactId" runat="server" Style="display: none"></asp:TextBox>
    <asp:Button ID="btnUpdateValues" runat="server" Style="display: none" />
    <asp:TextBox ID="txtCurrrentPage" runat="server" Style="display: none"></asp:TextBox>
    <asp:Button ID="btnGo1" Width="25" runat="server" Style="display: none" />
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="GridBizSorting" runat="server" ClientIDMode="Static">
    <uc1:frmBizSorting ID="frmBizSorting2" runat="server" />
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="GridSettingPopup" runat="server" ClientIDMode="Static">
    <asp:HyperLink runat="server" ID="hplEditResView" ToolTip="Edit advance search result view" onclick="return EditSearchView()"><i class="fa fa-lg fa-gear"></i></asp:HyperLink>
</asp:Content>