Imports System.Collections.Generic
Imports System.Web.Services
Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Marketing
Imports Newtonsoft.Json

Namespace BACRM.UserInterface.Admin
    Partial Public Class frmAdminBusinessProcess
        Inherits BACRMPage
        Dim objAdmin As CAdmin
        Dim dtAssignto, dtEmailTemplate, dtActivity, dtEvent, dtTime, dtRemider, dtStartDate, dtParentStage, dtType, dStageDropDown As DataTable
        Dim strCssClass As String = "arow"
        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            Try
                DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ""
                DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "none")
                Dim scriptManager__1 As ScriptManager = ScriptManager.GetCurrent(Me.Page)
                scriptManager__1.RegisterPostBackControl(Me.btnSave)
                objCommon = New CCommon
                DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ""
                DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "none")
                If Session("UserContactID") <> Session("AdminID") Then 'Logged in User is Admin of Domain? yes then override permission and give him access

                    GetUserRightsForPage(13, 30)
                    If m_aryRightsForPage(RIGHTSTYPE.VIEW) = 0 Then
                        Response.Redirect("../admin/authentication.aspx?mesg=AS")
                    ElseIf m_aryRightsForPage(RIGHTSTYPE.ADD) = 0 Then
                        btnSave.Visible = False
                    End If
                    If m_aryRightsForPage(RIGHTSTYPE.DELETE) = 0 Then btnDelete.Visible = False
                End If
                dtAssignto = objCommon.ConEmpList(Session("DomainId"), False, 0)
                dtActivity = objCommon.GetMasterListItems(32, Session("DomainId"))
                dtType = objCommon.GetMasterListItems(73, Session("DomainId"))
     
                If Not IsPostBack Then
                    Dim dtData As DataTable = objCommon.ConEmpList(Session("DomainID"), 0, 0)
                    If Not dtData Is Nothing AndAlso dtData.Rows.Count > 0 Then
                        If dtData.Columns.Contains("tintGroupType") Then
                            dtData = dtData.Select("tintGroupType <> 4").CopyToDataTable()
                        End If

                        ddlBuildManager.DataSource = dtData
                        ddlBuildManager.DataTextField = "vcUserName"
                        ddlBuildManager.DataValueField = "numContactID"
                        ddlBuildManager.DataBind()                    
                    End If

                    ddlBuildManager.Items.Insert(0, "--Select One--")
                    ddlBuildManager.Items.FindByText("--Select One--").Value = "0"

                    If CCommon.ToShort(GetQueryStringVal("tintProcessType")) = 3 Then
                        hdnProcessType.Value = "3"
                    End If
                End If

                lblMessage.Text = ""
                ManageStageVisibitity()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

#Region "Implementation"

        Private Sub LoadEmailTemplate()
            Try
                Dim ddlEmailTemplate As DropDownList = CType(CCommon.FindControlRecursive(Page.Master, "ddlEmailTemplate" & "_" & ddlProcessList.SelectedValue), DropDownList)
                ddlEmailTemplate.DataSource = dtEmailTemplate
                ddlEmailTemplate.DataTextField = "VcDocName"
                ddlEmailTemplate.DataValueField = "numGenericDocID"
                ddlEmailTemplate.DataBind()
                ddlEmailTemplate.Items.Insert(0, "--Select One--")
                ddlEmailTemplate.Items.FindByText("--Select One--").Value = "0"
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub ProcessList()
            Try
                If objAdmin Is Nothing Then objAdmin = New CAdmin
                objAdmin.DomainID = Session("DomainId")
                objAdmin.Mode = ddlProcess.SelectedValue                    'Here Mode is Being utilised Pro_Type of Sales_process_List_Master Table
                ddlProcessList.DataSource = objAdmin.LoadProcessList()
                ddlProcessList.DataTextField = "Slp_Name"
                ddlProcessList.DataValueField = "Slp_Id"
                ddlProcessList.DataBind()
                ddlProcessList.Items.Insert(0, "--Select One--")
                ddlProcessList.Items.FindByText("--Select One--").Value = "0"
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        ' This sub Mangaes the view of the Configuration and Default Stage Values

        Private Sub ManageStageVisibitity()
            Try
                Select Case ddlConfiguration.SelectedValue
                    Case 1
                        divstage5.Visible = False
                        divstage20.Visible = False
                        divstage25.Visible = False
                        divstage35.Visible = False
                        divstage40.Visible = False
                        divstage50.Visible = True
                        divstage60.Visible = False
                        divstage65.Visible = False
                        divstage75.Visible = False
                        divstage80.Visible = False
                        divstage95.Visible = True
                    Case 2
                        divstage5.Visible = False
                        divstage20.Visible = False
                        divstage25.Visible = True
                        divstage35.Visible = False
                        divstage40.Visible = False
                        divstage50.Visible = True
                        divstage60.Visible = False
                        divstage65.Visible = False
                        divstage75.Visible = True
                        divstage80.Visible = False
                        divstage95.Visible = False
                    Case 3
                        divstage5.Visible = False
                        divstage20.Visible = True
                        divstage25.Visible = False
                        divstage35.Visible = False
                        divstage40.Visible = True
                        divstage50.Visible = False
                        divstage60.Visible = True
                        divstage65.Visible = False
                        divstage75.Visible = False
                        divstage80.Visible = True
                        divstage95.Visible = False
                    Case 4
                        divstage5.Visible = True
                        divstage20.Visible = True
                        divstage25.Visible = False
                        divstage35.Visible = True
                        divstage40.Visible = False
                        divstage50.Visible = True
                        divstage60.Visible = False
                        divstage65.Visible = True
                        divstage75.Visible = False
                        divstage80.Visible = True
                        divstage95.Visible = True
                End Select
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub ddlProcess_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlProcess.SelectedIndexChanged
            Try
                ProcessList()
                'CType(CCommon.FindControlRecursive(Page.Master,"txtMStarge5"), TextBox).Text = ""
                'CType(CCommon.FindControlRecursive(Page.Master,"txtMStarge20"), TextBox).Text = ""
                'CType(CCommon.FindControlRecursive(Page.Master,"txtMStarge25"), TextBox).Text = ""
                'CType(CCommon.FindControlRecursive(Page.Master,"txtMStarge35"), TextBox).Text = ""
                'CType(CCommon.FindControlRecursive(Page.Master,"txtMStarge40"), TextBox).Text = ""
                'CType(CCommon.FindControlRecursive(Page.Master,"txtMStarge50"), TextBox).Text = ""
                'CType(CCommon.FindControlRecursive(Page.Master,"txtMStarge60"), TextBox).Text = ""
                'CType(CCommon.FindControlRecursive(Page.Master,"txtMStarge65"), TextBox).Text = ""
                'CType(CCommon.FindControlRecursive(Page.Master,"txtMStarge75"), TextBox).Text = ""
                'CType(CCommon.FindControlRecursive(Page.Master,"txtMStarge80"), TextBox).Text = ""
                'CType(CCommon.FindControlRecursive(Page.Master,"txtMStarge95"), TextBox).Text = ""
                txtName.Text = ""
                tbl5.Controls.Clear()
                tbl20.Controls.Clear()
                tbl25.Controls.Clear()
                tbl35.Controls.Clear()
                tbl40.Controls.Clear()
                tbl50.Controls.Clear()
                tbl60.Controls.Clear()
                tbl65.Controls.Clear()
                tbl75.Controls.Clear()
                tbl80.Controls.Clear()
                tbl95.Controls.Clear()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub
#End Region

#Region "Building The Process List"
        Private Sub BindDropDown()
            Try
                If objAdmin Is Nothing Then objAdmin = New CAdmin
                Dim dtTable As DataTable
                objAdmin.Mode = 0
                objAdmin.DomainID = Session("DomainId")
                objAdmin.SalesProsID = ddlProcessList.SelectedValue
                dtTable = objAdmin.StageItemDetails()
                dStageDropDown = dtTable
                txtMSMaxStage5.Text = 0
                txtMSMaxStage20.Text = 0
                txtMSMaxStage25.Text = 0
                txtMSMaxStage35.Text = 0
                txtMSMaxStage40.Text = 0
                txtMSMaxStage50.Text = 0
                txtMSMaxStage60.Text = 0
                txtMSMaxStage65.Text = 0
                txtMSMaxStage75.Text = 0
                txtMSMaxStage80.Text = 0
                txtMSMaxStage95.Text = 0

                If dtTable.Rows.Count > 0 Then
                    For i As Integer = 0 To dtTable.Rows.Count - 1
                        Select Case CInt(dtTable.Rows(i).Item("numStagePercentageId"))
                            Case 1
                                txtMSMaxStage5.Text = CInt(txtMSMaxStage5.Text) + 1
                                BuildStageDropDown(dtTable.Rows(i), dtTable.Rows(i).Item("numStagePercentageId"), txtMSMaxStage5.Text, tbl5)
                            Case 2
                                txtMSMaxStage20.Text = CInt(txtMSMaxStage20.Text) + 1
                                BuildStageDropDown(dtTable.Rows(i), dtTable.Rows(i).Item("numStagePercentageId"), txtMSMaxStage20.Text, tbl20)
                            Case 3
                                txtMSMaxStage25.Text = CInt(txtMSMaxStage25.Text) + 1
                                BuildStageDropDown(dtTable.Rows(i), dtTable.Rows(i).Item("numStagePercentageId"), txtMSMaxStage25.Text, tbl25)
                            Case 4
                                txtMSMaxStage35.Text = CInt(txtMSMaxStage35.Text) + 1
                                BuildStageDropDown(dtTable.Rows(i), dtTable.Rows(i).Item("numStagePercentageId"), txtMSMaxStage35.Text, tbl35)
                            Case 5
                                txtMSMaxStage40.Text = CInt(txtMSMaxStage40.Text) + 1
                                BuildStageDropDown(dtTable.Rows(i), dtTable.Rows(i).Item("numStagePercentageId"), txtMSMaxStage40.Text, tbl40)
                            Case 6
                                txtMSMaxStage50.Text = CInt(txtMSMaxStage50.Text) + 1
                                BuildStageDropDown(dtTable.Rows(i), dtTable.Rows(i).Item("numStagePercentageId"), txtMSMaxStage50.Text, tbl50)
                            Case 7
                                txtMSMaxStage60.Text = CInt(txtMSMaxStage60.Text) + 1
                                BuildStageDropDown(dtTable.Rows(i), dtTable.Rows(i).Item("numStagePercentageId"), txtMSMaxStage60.Text, tbl60)
                            Case 8
                                txtMSMaxStage65.Text = CInt(txtMSMaxStage65.Text) + 1
                                BuildStageDropDown(dtTable.Rows(i), dtTable.Rows(i).Item("numStagePercentageId"), txtMSMaxStage65.Text, tbl65)
                            Case 9
                                txtMSMaxStage75.Text = CInt(txtMSMaxStage75.Text) + 1
                                BuildStageDropDown(dtTable.Rows(i), dtTable.Rows(i).Item("numStagePercentageId"), txtMSMaxStage75.Text, tbl75)
                            Case 10
                                txtMSMaxStage80.Text = CInt(txtMSMaxStage80.Text) + 1
                                BuildStageDropDown(dtTable.Rows(i), dtTable.Rows(i).Item("numStagePercentageId"), txtMSMaxStage80.Text, tbl80)
                            Case 11
                                txtMSMaxStage95.Text = CInt(txtMSMaxStage95.Text) + 1
                                BuildStageDropDown(dtTable.Rows(i), dtTable.Rows(i).Item("numStagePercentageId"), txtMSMaxStage95.Text, tbl95)
                        End Select
                    Next
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        Public Sub BuildStageDropDown(ByVal dtRow As DataRow, ByVal numStagePercentageId As Integer, ByVal numStage As Integer, ByVal tblTable As Table)
            Try
                Dim ddlParentStage As DropDownList = tblTable.FindControl("ddlParentStage_" & numStagePercentageId & "_" & numStage & "_" & ddlProcessList.SelectedValue)

                Dim a As DataTable = dStageDropDown.Select("numStagePercentageId=" & numStagePercentageId).CopyToDataTable
                ddlParentStage.DataSource = a
                ddlParentStage.DataTextField = "vcStageName"
                ddlParentStage.DataValueField = "numStageDetailsId"
                ddlParentStage.DataBind()
                ddlParentStage.Items.Insert(0, "--Select One--")
                ddlParentStage.Items.FindByText("--Select One--").Value = 0
                If ddlParentStage.Items.FindByValue(dtRow.Item("numParentStageID")) IsNot Nothing Then
                    ddlParentStage.Items.FindByValue(dtRow.Item("numParentStageID")).Selected = True
                End If


                If ddlParentStage.Items.FindByValue(dtRow.Item("numStageDetailsId")) IsNot Nothing Then
                    ddlParentStage.Items.Remove(ddlParentStage.Items.FindByValue(dtRow.Item("numStageDetailsId")))
                End If

            Catch ex As Exception
                Throw ex
            End Try
        End Sub


        Private Sub BindTable()
            Try
                If objAdmin Is Nothing Then objAdmin = New CAdmin
                Dim dtTable As DataTable
                objAdmin.Mode = 0
                objAdmin.DomainID = Session("DomainId")
                objAdmin.SalesProsID = ddlProcessList.SelectedValue
                dtTable = objAdmin.StageItemDetails()

                txtMSMaxStage5.Text = 0
                txtMSMaxStage20.Text = 0
                txtMSMaxStage25.Text = 0
                txtMSMaxStage35.Text = 0
                txtMSMaxStage40.Text = 0
                txtMSMaxStage50.Text = 0
                txtMSMaxStage60.Text = 0
                txtMSMaxStage65.Text = 0
                txtMSMaxStage75.Text = 0
                txtMSMaxStage80.Text = 0
                txtMSMaxStage95.Text = 0

                If dtTable.Rows.Count > 0 Then
                    If objCommon Is Nothing Then objCommon = New CCommon
                    'objCommon.intOption = 19
                    'dtEvent = objCommon.GetDefaultValues
                    'objCommon.intOption = 18
                    'dtRemider = objCommon.GetDefaultValues
                    'objCommon.intOption = 17
                    'dtTime = objCommon.GetDefaultValues
                    'objCommon.intOption = 16
                    'dtStartDate = objCommon.GetDefaultValues

                    'dtStageStatus = objCommon.GetMasterListItems(42, Session("DomainID"))
                    objAdmin.Mode = 1
                    dtParentStage = objAdmin.StageItemDetails()

                    Dim rfvMileStone As RequiredFieldValidator = New RequiredFieldValidator()

                    Dim txtMStarge As TextBox
                    txtMStarge = New TextBox
                    txtMStarge.ID = "txtMStarge5" & "_" & ddlProcessList.SelectedValue
                    BindMileStoneTextbox(txtMStarge)
                    span5.Controls.Add(txtMStarge)

                    rfvMileStone = New RequiredFieldValidator()
                    With rfvMileStone
                        .ErrorMessage = "Milestone 5% Name Required"
                        .ControlToValidate = txtMStarge.ID
                        .Text = "*"
                        .ValidationGroup = "vgRequired"
                        .SetFocusOnError = True
                    End With
                    span5.Controls.Add(rfvMileStone)

                    'Validations.Controls.Add(timeValidation)

                    txtMStarge = New TextBox
                    txtMStarge.ID = "txtMStarge20" & "_" & ddlProcessList.SelectedValue
                    BindMileStoneTextbox(txtMStarge)
                    span20.Controls.Add(txtMStarge)

                    rfvMileStone = New RequiredFieldValidator()
                    With rfvMileStone
                        .ErrorMessage = "Milestone 20% Name Required"
                        .ControlToValidate = txtMStarge.ID
                        .Text = "*"
                        .ValidationGroup = "vgRequired"
                        .SetFocusOnError = True
                    End With
                    span20.Controls.Add(rfvMileStone)

                    txtMStarge = New TextBox
                    txtMStarge.ID = "txtMStarge25" & "_" & ddlProcessList.SelectedValue
                    BindMileStoneTextbox(txtMStarge)
                    span25.Controls.Add(txtMStarge)

                    rfvMileStone = New RequiredFieldValidator()
                    With rfvMileStone
                        .ErrorMessage = "Milestone 25% Name Required"
                        .ControlToValidate = txtMStarge.ID
                        .Text = "*"
                        .ValidationGroup = "vgRequired"
                        .SetFocusOnError = True
                    End With
                    span25.Controls.Add(rfvMileStone)

                    txtMStarge = New TextBox
                    txtMStarge.ID = "txtMStarge35" & "_" & ddlProcessList.SelectedValue
                    BindMileStoneTextbox(txtMStarge)
                    span35.Controls.Add(txtMStarge)

                    rfvMileStone = New RequiredFieldValidator()
                    With rfvMileStone
                        .ErrorMessage = "Milestone 35% Name Required"
                        .ControlToValidate = txtMStarge.ID
                        .Text = "*"
                        .ValidationGroup = "vgRequired"
                        .SetFocusOnError = True
                    End With
                    span35.Controls.Add(rfvMileStone)

                    txtMStarge = New TextBox
                    txtMStarge.ID = "txtMStarge40" & "_" & ddlProcessList.SelectedValue
                    BindMileStoneTextbox(txtMStarge)
                    span40.Controls.Add(txtMStarge)

                    rfvMileStone = New RequiredFieldValidator()
                    With rfvMileStone
                        .ErrorMessage = "Milestone 40% Name Required"
                        .ControlToValidate = txtMStarge.ID
                        .Text = "*"
                        .ValidationGroup = "vgRequired"
                        .SetFocusOnError = True
                    End With
                    span40.Controls.Add(rfvMileStone)

                    txtMStarge = New TextBox
                    txtMStarge.ID = "txtMStarge50" & "_" & ddlProcessList.SelectedValue
                    BindMileStoneTextbox(txtMStarge)
                    span50.Controls.Add(txtMStarge)

                    rfvMileStone = New RequiredFieldValidator()
                    With rfvMileStone
                        .ErrorMessage = "Milestone 50% Name Required"
                        .ControlToValidate = txtMStarge.ID
                        .Text = "*"
                        .ValidationGroup = "vgRequired"
                        .SetFocusOnError = True
                    End With
                    span50.Controls.Add(rfvMileStone)

                    txtMStarge = New TextBox
                    txtMStarge.ID = "txtMStarge60" & "_" & ddlProcessList.SelectedValue
                    BindMileStoneTextbox(txtMStarge)
                    span60.Controls.Add(txtMStarge)

                    rfvMileStone = New RequiredFieldValidator()
                    With rfvMileStone
                        .ErrorMessage = "Milestone 60% Name Required"
                        .ControlToValidate = txtMStarge.ID
                        .Text = "*"
                        .ValidationGroup = "vgRequired"
                        .SetFocusOnError = True
                    End With
                    span60.Controls.Add(rfvMileStone)

                    txtMStarge = New TextBox
                    txtMStarge.ID = "txtMStarge65" & "_" & ddlProcessList.SelectedValue
                    BindMileStoneTextbox(txtMStarge)
                    span65.Controls.Add(txtMStarge)

                    rfvMileStone = New RequiredFieldValidator()
                    With rfvMileStone
                        .ErrorMessage = "Milestone 65% Name Required"
                        .ControlToValidate = txtMStarge.ID
                        .Text = "*"
                        .ValidationGroup = "vgRequired"
                        .SetFocusOnError = True
                    End With
                    span65.Controls.Add(rfvMileStone)

                    txtMStarge = New TextBox
                    txtMStarge.ID = "txtMStarge75" & "_" & ddlProcessList.SelectedValue
                    BindMileStoneTextbox(txtMStarge)
                    span75.Controls.Add(txtMStarge)

                    rfvMileStone = New RequiredFieldValidator()
                    With rfvMileStone
                        .ErrorMessage = "Milestone 75% Name Required"
                        .ControlToValidate = txtMStarge.ID
                        .Text = "*"
                        .ValidationGroup = "vgRequired"
                        .SetFocusOnError = True
                    End With
                    span75.Controls.Add(rfvMileStone)

                    txtMStarge = New TextBox
                    txtMStarge.ID = "txtMStarge80" & "_" & ddlProcessList.SelectedValue
                    BindMileStoneTextbox(txtMStarge)
                    span80.Controls.Add(txtMStarge)

                    rfvMileStone = New RequiredFieldValidator()
                    With rfvMileStone
                        .ErrorMessage = "Milestone 80% Name Required"
                        .ControlToValidate = txtMStarge.ID
                        .Text = "*"
                        .ValidationGroup = "vgRequired"
                        .SetFocusOnError = True
                    End With
                    span80.Controls.Add(rfvMileStone)

                    txtMStarge = New TextBox
                    txtMStarge.ID = "txtMStarge95" & "_" & ddlProcessList.SelectedValue
                    BindMileStoneTextbox(txtMStarge)
                    span95.Controls.Add(txtMStarge)

                    rfvMileStone = New RequiredFieldValidator()
                    With rfvMileStone
                        .ErrorMessage = "Milestone 95% Name Required"
                        .ControlToValidate = txtMStarge.ID
                        .Text = "*"
                        .ValidationGroup = "vgRequired"
                    End With
                    span95.Controls.Add(rfvMileStone)

                    'Dim chkSendEmail As New CheckBox
                    'chkSendEmail.ID = "chkSendEmail" & "_" & ddlProcessList.SelectedValue
                    'span1.Controls.Add(chkSendEmail)

                    'Dim ddlEmailTemplate As New DropDownList
                    'ddlEmailTemplate.ID = "ddlEmailTemplate" & "_" & ddlProcessList.SelectedValue
                    'ddlEmailTemplate.CssClass = "signup"
                    'span2.Controls.Add(ddlEmailTemplate)
                    'LoadEmailTemplate()



                    'If Not CType(CCommon.FindControlRecursive(Page.Master,"ddlEmailTemplate" & "_" & ddlProcessList.SelectedValue), DropDownList).Items.FindByValue(dtTable.Rows(0).Item("numTemplateId")) Is Nothing Then
                    '    'ddlEmailTemplate.ClearSelection()
                    '    CType(CCommon.FindControlRecursive(Page.Master,"ddlEmailTemplate" & "_" & ddlProcessList.SelectedValue), DropDownList).Items.FindByValue(dtTable.Rows(0).Item("numTemplateId")).Selected = True
                    'End If
                    'If dtTable.Rows(0).Item("numTemplateId") <> 0 Then
                    '    CType(CCommon.FindControlRecursive(Page.Master,"chkSendEmail" & "_" & ddlProcessList.SelectedValue), CheckBox).Checked = True
                    'End If
                    For i As Integer = 0 To dtTable.Rows.Count - 1
                        Select Case CInt(dtTable.Rows(i).Item("numStagePercentageId"))
                            Case 1
                                txtMSMaxStage5.Text = CInt(txtMSMaxStage5.Text) + 1
                                CType(CCommon.FindControlRecursive(Page.Master, "txtMStarge5" & "_" & ddlProcessList.SelectedValue), TextBox).Text = IIf(IsDBNull(dtTable.Rows(i).Item("vcMileStoneName")), "", dtTable.Rows(i).Item("vcMileStoneName"))
                                BuildStage(dtTable.Rows(i), dtTable.Rows(i).Item("numStagePercentageId"), txtMSMaxStage5.Text, tbl5)
                            Case 2
                                txtMSMaxStage20.Text = CInt(txtMSMaxStage20.Text) + 1
                                CType(CCommon.FindControlRecursive(Page.Master, "txtMStarge20" & "_" & ddlProcessList.SelectedValue), TextBox).Text = IIf(IsDBNull(dtTable.Rows(i).Item("vcMileStoneName")), "", dtTable.Rows(i).Item("vcMileStoneName"))
                                BuildStage(dtTable.Rows(i), dtTable.Rows(i).Item("numStagePercentageId"), txtMSMaxStage20.Text, tbl20)
                            Case 3
                                txtMSMaxStage25.Text = CInt(txtMSMaxStage25.Text) + 1
                                CType(CCommon.FindControlRecursive(Page.Master, "txtMStarge25" & "_" & ddlProcessList.SelectedValue), TextBox).Text = IIf(IsDBNull(dtTable.Rows(i).Item("vcMileStoneName")), "", dtTable.Rows(i).Item("vcMileStoneName"))
                                BuildStage(dtTable.Rows(i), dtTable.Rows(i).Item("numStagePercentageId"), txtMSMaxStage25.Text, tbl25)
                            Case 4
                                txtMSMaxStage35.Text = CInt(txtMSMaxStage35.Text) + 1
                                CType(CCommon.FindControlRecursive(Page.Master, "txtMStarge35" & "_" & ddlProcessList.SelectedValue), TextBox).Text = IIf(IsDBNull(dtTable.Rows(i).Item("vcMileStoneName")), "", dtTable.Rows(i).Item("vcMileStoneName"))
                                BuildStage(dtTable.Rows(i), dtTable.Rows(i).Item("numStagePercentageId"), txtMSMaxStage35.Text, tbl35)
                            Case 5
                                txtMSMaxStage40.Text = CInt(txtMSMaxStage40.Text) + 1
                                CType(CCommon.FindControlRecursive(Page.Master, "txtMStarge40" & "_" & ddlProcessList.SelectedValue), TextBox).Text = IIf(IsDBNull(dtTable.Rows(i).Item("vcMileStoneName")), "", dtTable.Rows(i).Item("vcMileStoneName"))
                                BuildStage(dtTable.Rows(i), dtTable.Rows(i).Item("numStagePercentageId"), txtMSMaxStage40.Text, tbl40)
                            Case 6
                                txtMSMaxStage50.Text = CInt(txtMSMaxStage50.Text) + 1
                                CType(CCommon.FindControlRecursive(Page.Master, "txtMStarge50" & "_" & ddlProcessList.SelectedValue), TextBox).Text = IIf(IsDBNull(dtTable.Rows(i).Item("vcMileStoneName")), "", dtTable.Rows(i).Item("vcMileStoneName"))
                                BuildStage(dtTable.Rows(i), dtTable.Rows(i).Item("numStagePercentageId"), txtMSMaxStage50.Text, tbl50)
                            Case 7
                                txtMSMaxStage60.Text = CInt(txtMSMaxStage60.Text) + 1
                                CType(CCommon.FindControlRecursive(Page.Master, "txtMStarge60" & "_" & ddlProcessList.SelectedValue), TextBox).Text = IIf(IsDBNull(dtTable.Rows(i).Item("vcMileStoneName")), "", dtTable.Rows(i).Item("vcMileStoneName"))
                                BuildStage(dtTable.Rows(i), dtTable.Rows(i).Item("numStagePercentageId"), txtMSMaxStage60.Text, tbl60)
                            Case 8
                                txtMSMaxStage65.Text = CInt(txtMSMaxStage65.Text) + 1
                                CType(CCommon.FindControlRecursive(Page.Master, "txtMStarge65" & "_" & ddlProcessList.SelectedValue), TextBox).Text = IIf(IsDBNull(dtTable.Rows(i).Item("vcMileStoneName")), "", dtTable.Rows(i).Item("vcMileStoneName"))
                                BuildStage(dtTable.Rows(i), dtTable.Rows(i).Item("numStagePercentageId"), txtMSMaxStage65.Text, tbl65)
                            Case 9
                                txtMSMaxStage75.Text = CInt(txtMSMaxStage75.Text) + 1
                                CType(CCommon.FindControlRecursive(Page.Master, "txtMStarge75" & "_" & ddlProcessList.SelectedValue), TextBox).Text = IIf(IsDBNull(dtTable.Rows(i).Item("vcMileStoneName")), "", dtTable.Rows(i).Item("vcMileStoneName"))
                                BuildStage(dtTable.Rows(i), dtTable.Rows(i).Item("numStagePercentageId"), txtMSMaxStage75.Text, tbl75)
                            Case 10
                                txtMSMaxStage80.Text = CInt(txtMSMaxStage80.Text) + 1
                                CType(CCommon.FindControlRecursive(Page.Master, "txtMStarge80" & "_" & ddlProcessList.SelectedValue), TextBox).Text = IIf(IsDBNull(dtTable.Rows(i).Item("vcMileStoneName")), "", dtTable.Rows(i).Item("vcMileStoneName"))
                                BuildStage(dtTable.Rows(i), dtTable.Rows(i).Item("numStagePercentageId"), txtMSMaxStage80.Text, tbl80)
                            Case 11
                                txtMSMaxStage95.Text = CInt(txtMSMaxStage95.Text) + 1
                                CType(CCommon.FindControlRecursive(Page.Master, "txtMStarge95" & "_" & ddlProcessList.SelectedValue), TextBox).Text = IIf(IsDBNull(dtTable.Rows(i).Item("vcMileStoneName")), "", dtTable.Rows(i).Item("vcMileStoneName"))
                                BuildStage(dtTable.Rows(i), dtTable.Rows(i).Item("numStagePercentageId"), txtMSMaxStage95.Text, tbl95)
                        End Select
                    Next
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Public Sub BuildStage(ByVal dtRow As DataRow, ByVal numStagePercentageId As Integer, ByVal numStage As Integer, ByVal tblTable As Table)
            Try
                Dim ObjRow As TableRow
                Dim ObjCell As TableCell
                Dim lblText As Label

                '=======================================================================================
                '=======================================================================================
                ' Row 1
                '=======================================================================================
                '=======================================================================================
                ObjRow = New TableRow
                ObjRow.CssClass = strCssClass
                ObjCell = New TableCell

                Dim divForm As New HtmlGenericControl("div")
                divForm.Attributes.Add("class", "form-group")

                Dim Chk As New CheckBox
                Chk.CssClass = "normal1"
                Chk.ID = "Chk_" & numStagePercentageId & "_" & numStage & "_" & ddlProcessList.SelectedValue
                Chk.Text = "Stage"

                Dim divLabel As New HtmlGenericControl("label")
                divLabel.Controls.Add(Chk)
                divForm.Controls.Add(divLabel)

                lblText = New Label
                lblText.Text = dtRow.Item("numStageDetailsId")
                lblText.ID = "lbl_" & numStagePercentageId & "_" & numStage & "_" & ddlProcessList.SelectedValue
                lblText.Attributes.Add("style", "display:none")
                divForm.Controls.Add(lblText)

                Dim txtstage As New TextBox
                txtstage.CssClass = "form-control"
                txtstage.ID = "txtstage_" & numStagePercentageId & "_" & numStage & "_" & ddlProcessList.SelectedValue
                txtstage.Text = IIf(CCommon.ToString(dtRow.Item("vcStageName")).Length > 0, CCommon.ToString(dtRow.Item("vcStageName")), "- Enter Stage Name -")
                divForm.Controls.Add(txtstage)

                ObjCell.Controls.Add(divForm)

                Dim rfvMileStone As RequiredFieldValidator = New RequiredFieldValidator()

                rfvMileStone = New RequiredFieldValidator()
                With rfvMileStone
                    .ErrorMessage = "Stage Name Required"
                    .ControlToValidate = txtstage.ID
                    .Text = "*"
                    .ValidationGroup = "vgRequired"
                    .SetFocusOnError = True
                End With
                ObjCell.Controls.Add(rfvMileStone)


                divForm = New HtmlGenericControl("div")
                divForm.Attributes.Add("class", "form-group")

                divLabel = New HtmlGenericControl("label")
                divLabel.InnerText = "Assign To"
                divForm.Controls.Add(divLabel)

                Dim ddlAssignTo As New DropDownList
                'ddlAssignTo.Width = Unit.Pixel(150)
                ddlAssignTo.CssClass = "form-control"
                ddlAssignTo.ID = "ddlAssignTo_" & numStagePercentageId & "_" & numStage & "_" & ddlProcessList.SelectedValue
                ddlAssignTo.DataSource = dtAssignto
                ddlAssignTo.DataTextField = "vcUserName"
                ddlAssignTo.DataValueField = "numContactID"
                ddlAssignTo.DataBind()
                ddlAssignTo.Items.Insert(0, "--Select One--")
                ddlAssignTo.Items.FindByText("--Select One--").Value = "0"
                If Not ddlAssignTo.Items.FindByValue(dtRow.Item("numAssignTo")) Is Nothing Then
                    ddlAssignTo.Items.FindByValue(dtRow.Item("numAssignTo")).Selected = True
                End If
                divForm.Controls.Add(ddlAssignTo)

                ObjCell.Controls.Add(divForm)


                lblText = New Label
                lblText.Text = "Make sub-stage of &nbsp;"
                ObjCell.Controls.Add(lblText)


                Dim ddlParentStage As New DropDownList
                ddlParentStage.CssClass = "form-control"
                ddlParentStage.ID = "ddlParentStage_" & numStagePercentageId & "_" & numStage & "_" & ddlProcessList.SelectedValue
                ObjCell.Controls.Add(ddlParentStage)
                ObjRow.Cells.Add(ObjCell)

                ObjCell = New TableCell

                Dim literal As Literal
                literal = New Literal
                literal.Text = "<p class='formfield'>"
                ObjCell.Controls.Add(literal)

                lblText = New Label
                lblText.Text = "Description &nbsp;"
                ObjCell.Controls.Add(lblText)

                Dim txtDescription As New TextBox
                txtDescription.TextMode = TextBoxMode.MultiLine
                txtDescription.CssClass = "form-control"
                'txtDescription.Width = Unit.Pixel(350)
                txtDescription.ID = "txtDescription_" & numStagePercentageId & "_" & numStage & "_" & ddlProcessList.SelectedValue
                txtDescription.Text = dtRow.Item("vcDescription")
                ObjCell.Controls.Add(txtDescription)
                ObjRow.Cells.Add(ObjCell)

                lblText = New Label
                lblText.Text = "This stage comprises % of completion"
                ObjCell.Controls.Add(lblText)

                Dim txtPer As New TextBox
                'txtPer.Width = Unit.Pixel(30)
                txtPer.CssClass = "form-control"
                txtPer.ID = "txtPer_" & numStagePercentageId & "_" & numStage & "_" & ddlProcessList.SelectedValue
                txtPer.Text = dtRow.Item("tintPercentage")
                txtPer.MaxLength = 3
                txtPer.Attributes.Add("onkeypress", "CheckNumber(2,event)")
                ObjCell.Controls.Add(txtPer)

                'lblText = New Label
                'lblText.Text = "&nbsp;&nbsp;"
                'ObjCell.Controls.Add(lblText)

                rfvMileStone = New RequiredFieldValidator()
                With rfvMileStone
                    .ErrorMessage = "Stage % of completion Required"
                    .ControlToValidate = txtPer.ID
                    .Text = "*"
                    .ValidationGroup = "vgRequired"
                    .SetFocusOnError = True
                End With
                ObjCell.Controls.Add(rfvMileStone)

                Dim rvPer As New RangeValidator
                With rvPer
                    .MinimumValue = 0
                    .MaximumValue = 100
                    .ControlToValidate = txtPer.ID
                    .Text = "*"
                    .ValidationGroup = "vgRequired"
                    .Type = ValidationDataType.Integer
                    .ErrorMessage = "Stage % of completion should be betwwen 0 to 100"
                    .SetFocusOnError = True
                End With
                ObjCell.Controls.Add(rvPer)

                lblText = New Label
                lblText.Text = "Set Due Date Days from Start Date"
                ObjCell.Controls.Add(lblText)


                Dim ddlDueDays As New DropDownList
                ddlDueDays.CssClass = "form-control"
                ddlDueDays.ID = "ddlDueDays_" & numStagePercentageId & "_" & numStage & "_" & ddlProcessList.SelectedValue
                For index As Integer = 0 To 30
                    ddlDueDays.Items.Add(New ListItem(index.ToString, index.ToString))
                Next
                If Not ddlDueDays.Items.FindByValue(dtRow.Item("intDueDays")) Is Nothing Then
                    ddlDueDays.Items.FindByValue(dtRow.Item("intDueDays")).Selected = True
                End If
                ObjCell.Controls.Add(ddlDueDays)

                'lblText = New Label
                'lblText.Text = " &nbsp;"
                'ObjCell.Controls.Add(lblText)


                Dim lbDependency As New LinkButton
                lbDependency.Text = "Add Up Stream Dependency (" + dtRow.Item("numDependencyCount").ToString() + ")"
                lbDependency.CssClass = "hyperlink"
                objAdmin.SalesProsID = ddlProcessList.SelectedValue
                objAdmin.SalesProsID = ddlProcessList.SelectedValue
                lbDependency.OnClientClick = String.Format("javascript:return OpenTransfer('AddStageDependency.aspx?ProId={0}&StageId={1}')", ddlProcessList.SelectedValue, dtRow.Item("numStageDetailsId"))
                ObjCell.Controls.Add(lbDependency)

                literal = New Literal
                literal.Text = "</p>"
                ObjCell.Controls.Add(literal)


                ObjRow.Cells.Add(ObjCell)
                tblTable.Rows.Add(ObjRow)

            Catch ex As Exception
                Throw ex
            End Try
        End Sub

#End Region

#Region "Add Stage And Deleting Stage"

        Private Sub Addstage()
            Try
                If objAdmin Is Nothing Then objAdmin = New CAdmin
                objAdmin.DomainID = Session("DomainId")
                objAdmin.UserCntID = Session("UserContactId")
                objAdmin.SalesProsID = ddlProcessList.SelectedValue
                objAdmin.StageDetail = ""
                objAdmin.StagePerID = txtAddStage.Text
                objAdmin.Configuration = ddlConfiguration.SelectedValue
                objAdmin.Assignto = 0
                objAdmin.Percentage = 0
                objAdmin.MileStoneName = ""
                objAdmin.Description = ""
                objAdmin.DueDays = 0

                objAdmin.StartDate = System.Data.SqlTypes.SqlDateTime.MinValue.Value
                objAdmin.EndDate = System.Data.SqlTypes.SqlDateTime.MinValue.Value
                objAdmin.ParentStageID = 0
                objAdmin.ProjectID = 0
                objAdmin.InsertStageDetails()
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub DeleteStage()
            Try
                If objAdmin Is Nothing Then objAdmin = New CAdmin
                objAdmin.DomainID = Session("DomainId")
                objAdmin.strStageDetailsIds = txtDeleteStage.Text
                objAdmin.DeleteSubStage()
                txtDeleteStage.Text = 0
                Dim DString As String = "~/Admin/frmAdminBusinessProcess.aspx?slp_id=" & ddlProcessList.SelectedValue & "&ProcessId=" & ddlProcess.SelectedValue
                Response.Redirect(DString)

            Catch ex As Exception
                Throw ex
            End Try
        End Sub
#End Region

        Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
            If Page.IsValid Then
                Try
                    If validateMileStone() Then
                        If ddlProcessList.SelectedIndex <> 0 Then
                            If objAdmin Is Nothing Then objAdmin = New CAdmin
                            objAdmin.DomainID = Session("DomainId")
                            objAdmin.UserCntID = Session("UserContactId")
                            objAdmin.SalesProsID = ddlProcessList.SelectedValue
                            objAdmin.Configuration = ddlConfiguration.SelectedValue
                            If hdnProcessType.Value = "3" Then
                                objAdmin.BuildManager = CCommon.ToLong(ddlBuildManager.SelectedValue)
                            End If
                            objAdmin.CLLostFlag = 0
                            objAdmin.DelFlag = 0

                            If ddlProcessList.SelectedValue <> 0 Then
                                If txtName.Text <> ddlProcessList.SelectedItem.Text Then
                                    With objAdmin
                                        .SalesProsName = txtName.Text
                                        .ProsType = ddlProcess.SelectedValue
                                        .ManageSalesProcessList()
                                    End With
                                    ddlProcessList.SelectedItem.Text = txtName.Text
                                End If
                            End If

                            Dim i As Integer = 0

                            Select Case ddlConfiguration.SelectedValue
                                Case 1
                                    If txtMSMaxStage50.Text <> 0 Then
                                        For i = 1 To CInt(txtMSMaxStage50.Text)
                                            Save(objAdmin, CType(CCommon.FindControlRecursive(Page.Master, "txtMStarge50" & "_" & ddlProcessList.SelectedValue), TextBox).Text, 6, i)
                                        Next
                                    End If
                                    If txtMSMaxStage95.Text <> 0 Then
                                        For i = 1 To CInt(txtMSMaxStage95.Text)
                                            Save(objAdmin, CType(CCommon.FindControlRecursive(Page.Master, "txtMStarge95" & "_" & ddlProcessList.SelectedValue), TextBox).Text, 11, i)
                                        Next
                                    End If
                                Case 2
                                    If txtMSMaxStage25.Text <> 0 Then
                                        For i = 1 To CInt(txtMSMaxStage25.Text)
                                            Save(objAdmin, CType(CCommon.FindControlRecursive(Page.Master, "txtMStarge25" & "_" & ddlProcessList.SelectedValue), TextBox).Text, 3, i)
                                        Next
                                    End If
                                    If txtMSMaxStage50.Text <> 0 Then
                                        For i = 1 To CInt(txtMSMaxStage50.Text)
                                            Save(objAdmin, CType(CCommon.FindControlRecursive(Page.Master, "txtMStarge50" & "_" & ddlProcessList.SelectedValue), TextBox).Text, 6, i)
                                        Next
                                    End If
                                    If txtMSMaxStage75.Text <> 0 Then
                                        For i = 1 To CInt(txtMSMaxStage75.Text)
                                            Save(objAdmin, CType(CCommon.FindControlRecursive(Page.Master, "txtMStarge75" & "_" & ddlProcessList.SelectedValue), TextBox).Text, 9, i)
                                        Next
                                    End If
                                Case 3
                                    If txtMSMaxStage20.Text <> 0 Then
                                        For i = 1 To CInt(txtMSMaxStage20.Text)
                                            Save(objAdmin, CType(CCommon.FindControlRecursive(Page.Master, "txtMStarge20" & "_" & ddlProcessList.SelectedValue), TextBox).Text, 2, i)
                                        Next
                                    End If
                                    If txtMSMaxStage40.Text <> 0 Then
                                        For i = 1 To CInt(txtMSMaxStage40.Text)
                                            Save(objAdmin, CType(CCommon.FindControlRecursive(Page.Master, "txtMStarge40" & "_" & ddlProcessList.SelectedValue), TextBox).Text, 5, i)
                                        Next
                                    End If
                                    If txtMSMaxStage60.Text <> 0 Then
                                        For i = 1 To CInt(txtMSMaxStage60.Text)
                                            Save(objAdmin, CType(CCommon.FindControlRecursive(Page.Master, "txtMStarge60" & "_" & ddlProcessList.SelectedValue), TextBox).Text, 7, i)
                                        Next
                                    End If
                                    If txtMSMaxStage80.Text <> 0 Then
                                        For i = 1 To CInt(txtMSMaxStage80.Text)
                                            Save(objAdmin, CType(CCommon.FindControlRecursive(Page.Master, "txtMStarge80" & "_" & ddlProcessList.SelectedValue), TextBox).Text, 10, i)
                                        Next
                                    End If
                                Case 4
                                    If txtMSMaxStage5.Text <> 0 Then
                                        For i = 1 To CInt(txtMSMaxStage5.Text)
                                            Save(objAdmin, CType(CCommon.FindControlRecursive(Page.Master, "txtMStarge5" & "_" & ddlProcessList.SelectedValue), TextBox).Text, 1, i)
                                        Next
                                    End If
                                    If txtMSMaxStage20.Text <> 0 Then
                                        For i = 1 To CInt(txtMSMaxStage20.Text)
                                            Save(objAdmin, CType(CCommon.FindControlRecursive(Page.Master, "txtMStarge20" & "_" & ddlProcessList.SelectedValue), TextBox).Text, 2, i)
                                        Next
                                    End If
                                    If txtMSMaxStage35.Text <> 0 Then
                                        For i = 1 To CInt(txtMSMaxStage35.Text)
                                            Save(objAdmin, CType(CCommon.FindControlRecursive(Page.Master, "txtMStarge35" & "_" & ddlProcessList.SelectedValue), TextBox).Text, 4, i)
                                        Next
                                    End If
                                    If txtMSMaxStage50.Text <> 0 Then
                                        For i = 1 To CInt(txtMSMaxStage50.Text)
                                            Save(objAdmin, CType(CCommon.FindControlRecursive(Page.Master, "txtMStarge50" & "_" & ddlProcessList.SelectedValue), TextBox).Text, 6, i)
                                        Next
                                    End If
                                    If txtMSMaxStage65.Text <> 0 Then
                                        For i = 1 To CInt(txtMSMaxStage65.Text)
                                            Save(objAdmin, CType(CCommon.FindControlRecursive(Page.Master, "txtMStarge65" & "_" & ddlProcessList.SelectedValue), TextBox).Text, 8, i)
                                        Next
                                    End If

                                    If txtMSMaxStage80.Text <> 0 Then
                                        For i = 1 To CInt(txtMSMaxStage80.Text)
                                            Save(objAdmin, CType(CCommon.FindControlRecursive(Page.Master, "txtMStarge80" & "_" & ddlProcessList.SelectedValue), TextBox).Text, 10, i)
                                        Next
                                    End If
                                    If txtMSMaxStage95.Text <> 0 Then
                                        For i = 1 To CInt(txtMSMaxStage95.Text)
                                            Save(objAdmin, CType(CCommon.FindControlRecursive(Page.Master, "txtMStarge95" & "_" & ddlProcessList.SelectedValue), TextBox).Text, 11, i)
                                        Next
                                    End If
                            End Select
                            BindDropDown()
                        End If
                    Else
                        Response.Write("<script language='javascript'>alert('You must add values to all the percentages within stages in milestones so that the sum total is equal to 100%')</script>")
                    End If
                Catch ex As Exception
                    ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                    DisplayError(ex.Message)
                End Try
            End If
        End Sub

        Function vatidateStage(ByVal TotalStage As Integer, ByVal StagePerID As Integer) As Boolean
            Dim bStage As Boolean = False
            Dim sum As Integer = 0
            Dim i As Integer = 0
            Dim dtStage As DataTable

            For i = 1 To CInt(TotalStage)
                If CType(CCommon.FindControlRecursive(Page.Master, "ddlParentStage_" & StagePerID & "_" & i & "_" & ddlProcessList.SelectedValue), DropDownList).SelectedValue = 0 Then
                    sum = sum + CType(CCommon.FindControlRecursive(Page.Master, "txtPer_" & StagePerID & "_" & i & "_" & ddlProcessList.SelectedValue), TextBox).Text
                    bStage = True
                End If
            Next

            If bStage Then
                If sum <> 100 Then
                    Return False
                End If
            End If

            bStage = False
            sum = 0

            dtStage = dStageDropDown.Select("numStagePercentageId=" & StagePerID).CopyToDataTable

            For Each row As DataRow In dtStage.Rows
                For i = 1 To CInt(TotalStage)
                    If row("numStageDetailsId") = CType(CCommon.FindControlRecursive(Page.Master, "ddlParentStage_" & StagePerID & "_" & i & "_" & ddlProcessList.SelectedValue), DropDownList).SelectedValue Then
                        sum = sum + CType(CCommon.FindControlRecursive(Page.Master, "txtPer_" & StagePerID & "_" & i & "_" & ddlProcessList.SelectedValue), TextBox).Text
                        bStage = True
                    End If
                Next

                If sum <> 100 AndAlso bStage Then
                    Return False
                Else
                    sum = 0
                    bStage = False
                End If
            Next row

            Return True
        End Function
        Function validateMileStone() As Boolean
            If objAdmin Is Nothing Then objAdmin = New CAdmin
            Dim dtTable As DataTable
            objAdmin.Mode = 0
            objAdmin.DomainID = Session("DomainId")
            objAdmin.SalesProsID = ddlProcessList.SelectedValue
            dtTable = objAdmin.StageItemDetails()
            dStageDropDown = dtTable


            Select Case ddlConfiguration.SelectedValue
                Case 1
                    If txtMSMaxStage50.Text <> 0 Then

                        If vatidateStage(CInt(txtMSMaxStage50.Text), 6) = False Then
                            Return False
                        End If

                    End If
                    If txtMSMaxStage95.Text <> 0 Then

                        If vatidateStage(CInt(txtMSMaxStage95.Text), 11) = False Then
                            Return False
                        End If
                    End If
                Case 2
                    If txtMSMaxStage25.Text <> 0 Then

                        If vatidateStage(CInt(txtMSMaxStage25.Text), 3) = False Then
                            Return False
                        End If

                    End If
                    If txtMSMaxStage50.Text <> 0 Then

                        If vatidateStage(CInt(txtMSMaxStage50.Text), 6) = False Then
                            Return False
                        End If

                    End If
                    If txtMSMaxStage75.Text <> 0 Then

                        If vatidateStage(CInt(txtMSMaxStage75.Text), 9) = False Then
                            Return False
                        End If

                    End If
                Case 3
                    If txtMSMaxStage20.Text <> 0 Then

                        If vatidateStage(CInt(txtMSMaxStage20.Text), 2) = False Then
                            Return False
                        End If

                    End If
                    If txtMSMaxStage40.Text <> 0 Then

                        If vatidateStage(CInt(txtMSMaxStage40.Text), 5) = False Then
                            Return False
                        End If

                    End If
                    If txtMSMaxStage60.Text <> 0 Then

                        If vatidateStage(CInt(txtMSMaxStage60.Text), 7) = False Then
                            Return False
                        End If

                    End If
                    If txtMSMaxStage80.Text <> 0 Then

                        If vatidateStage(CInt(txtMSMaxStage80.Text), 10) = False Then
                            Return False
                        End If

                    End If
                Case 4
                    If txtMSMaxStage5.Text <> 0 Then

                        If vatidateStage(CInt(txtMSMaxStage5.Text), 1) = False Then
                            Return False
                        End If

                    End If
                    If txtMSMaxStage20.Text <> 0 Then

                        If vatidateStage(CInt(txtMSMaxStage20.Text), 2) = False Then
                            Return False
                        End If

                    End If
                    If txtMSMaxStage35.Text <> 0 Then

                        If vatidateStage(CInt(txtMSMaxStage35.Text), 4) = False Then
                            Return False
                        End If

                    End If
                    If txtMSMaxStage50.Text <> 0 Then

                        If vatidateStage(CInt(txtMSMaxStage50.Text), 6) = False Then
                            Return False
                        End If

                    End If
                    If txtMSMaxStage65.Text <> 0 Then

                        If vatidateStage(CInt(txtMSMaxStage65.Text), 8) = False Then
                            Return False
                        End If

                    End If

                    If txtMSMaxStage80.Text <> 0 Then

                        If vatidateStage(CInt(txtMSMaxStage80.Text), 10) = False Then
                            Return False
                        End If

                    End If
                    If txtMSMaxStage95.Text <> 0 Then

                        If vatidateStage(CInt(txtMSMaxStage95.Text), 11) = False Then
                            Return False
                        End If

                    End If
            End Select
            Return True
        End Function

        Sub Save(ByVal objAdmin As CAdmin, ByVal MileStoneName As String, ByVal StagePerID As Integer, ByVal i As Integer)
            Try
                objAdmin.numStageDetailsId = CType(CCommon.FindControlRecursive(Page.Master, "lbl_" & StagePerID & "_" & i & "_" & ddlProcessList.SelectedValue), Label).Text
                objAdmin.StageDetail = CType(CCommon.FindControlRecursive(Page.Master, "txtstage_" & StagePerID & "_" & i & "_" & ddlProcessList.SelectedValue), TextBox).Text
                objAdmin.StagePerID = StagePerID
                objAdmin.Assignto = CType(CCommon.FindControlRecursive(Page.Master, "ddlAssignTo_" & StagePerID & "_" & i & "_" & ddlProcessList.SelectedValue), DropDownList).SelectedValue
                If (CType(CCommon.FindControlRecursive(Page.Master, "txtPer_" & StagePerID & "_" & i & "_" & ddlProcessList.SelectedValue), TextBox).Text = "") Then
                    objAdmin.Percentage = 0
                Else : objAdmin.Percentage = IIf(IsNumeric(CType(CCommon.FindControlRecursive(Page.Master, "txtPer_" & StagePerID & "_" & i & "_" & ddlProcessList.SelectedValue), TextBox).Text), CType(CCommon.FindControlRecursive(Page.Master, "txtPer_" & StagePerID & "_" & i & "_" & ddlProcessList.SelectedValue), TextBox).Text, 0)
                End If
                objAdmin.MileStoneName = MileStoneName
                objAdmin.ParentStageID = CType(CCommon.FindControlRecursive(Page.Master, "ddlParentStage_" & StagePerID & "_" & i & "_" & ddlProcessList.SelectedValue), DropDownList).SelectedValue
                objAdmin.DueDays = CType(CCommon.FindControlRecursive(Page.Master, "ddlDueDays_" & StagePerID & "_" & i & "_" & ddlProcessList.SelectedValue), DropDownList).SelectedValue

                objAdmin.StartDate = System.Data.SqlTypes.SqlDateTime.MinValue.Value
                objAdmin.EndDate = System.Data.SqlTypes.SqlDateTime.MinValue.Value
                'If CType(CCommon.FindControlRecursive(Page.Master,"chkSendEmail" & "_" & ddlProcessList.SelectedValue), CheckBox).Checked = True Then
                '    objAdmin.TemplateId = CType(CCommon.FindControlRecursive(Page.Master,"ddlEmailtemplate" & "_" & ddlProcessList.SelectedValue), DropDownList).SelectedValue
                'Else : objAdmin.TemplateId = 0
                'End If

                'objAdmin.numEvent = CType(CCommon.FindControlRecursive(Page.Master,"ddlEvent_" & StagePerID & "_" & i & "_" & ddlProcessList.SelectedValue), DropDownList).SelectedValue
                'objAdmin.numReminder = CType(CCommon.FindControlRecursive(Page.Master,"ddlReminder_" & StagePerID & "_" & i & "_" & ddlProcessList.SelectedValue), DropDownList).SelectedValue
                'objAdmin.numET = CType(CCommon.FindControlRecursive(Page.Master,"ddlEmailtemplate_" & StagePerID & "_" & i & "_" & ddlProcessList.SelectedValue), DropDownList).SelectedValue
                'objAdmin.numActivity = CType(CCommon.FindControlRecursive(Page.Master,"ddlActivity_" & StagePerID & "_" & i & "_" & ddlProcessList.SelectedValue), DropDownList).SelectedValue
                'objAdmin.numType = CType(CCommon.FindControlRecursive(Page.Master,"ddlType_" & StagePerID & "_" & i & "_" & ddlProcessList.SelectedValue), DropDownList).SelectedValue
                'objAdmin.numSD = CType(CCommon.FindControlRecursive(Page.Master,"ddlStartDate_" & StagePerID & "_" & i & "_" & ddlProcessList.SelectedValue), DropDownList).SelectedValue
                'objAdmin.numSTT = CType(CCommon.FindControlRecursive(Page.Master,"ddlStartTime_" & StagePerID & "_" & i & "_" & ddlProcessList.SelectedValue), DropDownList).SelectedValue
                'objAdmin.numSTP = CType(CCommon.FindControlRecursive(Page.Master,"ddlStartPeriod_" & StagePerID & "_" & i & "_" & ddlProcessList.SelectedValue), DropDownList).SelectedValue
                'objAdmin.numETT = CType(CCommon.FindControlRecursive(Page.Master,"ddlEndTime_" & StagePerID & "_" & i & "_" & ddlProcessList.SelectedValue), DropDownList).SelectedValue
                'objAdmin.numETP = CType(CCommon.FindControlRecursive(Page.Master,"ddlEndPeriod_" & StagePerID & "_" & i & "_" & ddlProcessList.SelectedValue), DropDownList).SelectedValue
                objAdmin.Description = CType(CCommon.FindControlRecursive(Page.Master, "txtDescription_" & StagePerID & "_" & i & "_" & ddlProcessList.SelectedValue), TextBox).Text
                'If CType(CCommon.FindControlRecursive(Page.Master,"chkChgStatus_" & StagePerID & "_" & i & "_" & ddlProcessList.SelectedValue), CheckBox).Checked Then
                '    objAdmin.numChgStatus = CType(CCommon.FindControlRecursive(Page.Master,"ddlChgStatus_" & StagePerID & "_" & i & "_" & ddlProcessList.SelectedValue), DropDownList).SelectedValue
                'Else : objAdmin.numChgStatus = 0
                'End If

                'objAdmin.bitChgStatus = CType(CCommon.FindControlRecursive(Page.Master,"chkChgStatus_" & StagePerID & "_" & i & "_" & ddlProcessList.SelectedValue), CheckBox).Checked
                'objAdmin.bitClose = CType(CCommon.FindControlRecursive(Page.Master,"chkClose_" & StagePerID & "_" & i & "_" & ddlProcessList.SelectedValue), CheckBox).Checked
                objAdmin.UpdateStageDetails()
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub ddlProcessList_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlProcessList.SelectedIndexChanged
            Try
                If ddlProcessList.SelectedIndex <> 0 Then
                    txtName.Text = ddlProcessList.SelectedItem.Text

                    objAdmin.DomainID = Session("DomainId")
                    objAdmin.SalesProsID = ddlProcessList.SelectedValue
                    objAdmin.Mode = ddlProcess.SelectedValue
                    Dim dtConfiguration = objAdmin.GetBusinessProcessListConfiguration()
                    If dtConfiguration.Rows(0).Item("tintConfiguration") IsNot Nothing And Not IsDBNull(dtConfiguration.Rows(0).Item("tintConfiguration")) Then
                        Dim tintConfiguration As Integer = dtConfiguration.Rows(0).Item("tintConfiguration")
                        ddlConfiguration.ClearSelection()
                        ddlConfiguration.Items.FindByValue(tintConfiguration).Selected = True

                        ManageStageVisibitity()
                    End If

                Else : txtName.Text = ""
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)

            End Try
        End Sub
        Private Sub BindMileStoneTextbox(ByRef txtMStarge As TextBox)
            Try
                ' txtMStarge.Attributes.Add("width", "200")
                'txtMStarge.Width = Unit.Pixel(400)
                txtMStarge.CssClass = "form-control"
                txtMStarge.Text = ""
                'txtMStarge.DefaultBlankText = " Milestone " + count.ToString
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub DisplayError(ByVal ex As String)
            DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ex
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
        End Sub

        <WebMethod()>
        Public Shared Function WebMethodGetProcessList(ByVal DomainID As Integer, ByVal Mode As Integer) As String
            Try
                Dim objAdmin = New CAdmin()
                Dim dtTable As DataTable
                objAdmin.DomainID = DomainID
                objAdmin.Mode = Mode
                dtTable = objAdmin.LoadProcessList()
                Dim json As String = String.Empty
                json = JsonConvert.SerializeObject(dtTable, Formatting.None)
                Return json
            Catch ex As Exception
                Dim strError As String = ""

                strError = ex.Message
                Throw New Exception(strError)
                'Throw ex
            End Try
        End Function

        <WebMethod()>
        Public Shared Function WebMethodSaveNewProcess(ByVal DomainID As Integer, ByVal Mode As Integer, ByVal ConfigurationId As Integer, ByVal UserContactId As Long, ByVal ProcessName As String) As String
            Try
                Dim objAdmin = New CAdmin()
                Dim slpid As Long = 0
                objAdmin.DomainID = DomainID
                objAdmin.UserCntID = UserContactId
                objAdmin.Configuration = ConfigurationId
                objAdmin.SalesProsName = ProcessName
                objAdmin.ProsType = Mode
                slpid = objAdmin.ManageSalesProcessList()
                Dim json As String = String.Empty
                json = JsonConvert.SerializeObject(slpid, Formatting.None)
                Return json
            Catch ex As Exception
                Dim strError As String = ""

                strError = ex.Message
                Throw New Exception(strError)
                'Throw ex
            End Try
        End Function

        <WebMethod()>
        Public Shared Function WebMethodDeleteProcess(ByVal DomainID As Integer, ByVal ProcessId As Long) As String
            Try
                Dim objAdmin = New CAdmin()
                Dim slpid As Long = 0
                objAdmin.DomainID = DomainID
                objAdmin.SalesProsID = ProcessId
                slpid = objAdmin.DelBusinessPros()
                Dim json As String = String.Empty
                json = JsonConvert.SerializeObject(slpid, Formatting.None)
                Return json
            Catch ex As Exception
                Dim strError As String = ""

                strError = ex.Message
                Throw New Exception(strError)
                'Throw ex
            End Try
        End Function

        <WebMethod()>
        Public Shared Function WebMethodGetConfigurationOnProcessList(ByVal DomainID As Integer, ByVal ProcessId As Long, ByVal Mode As Integer) As String
            Try
                Dim objAdmin = New CAdmin()
                Dim tintConfiguration As Integer
                objAdmin.DomainID = DomainID
                objAdmin.SalesProsID = ProcessId
                objAdmin.Mode = Mode
                Dim dtConfiguration = objAdmin.GetBusinessProcessListConfiguration()
                Dim json As String = String.Empty
                json = JsonConvert.SerializeObject(dtConfiguration, Formatting.None)
                Return json
            Catch ex As Exception
                Dim strError As String = ""

                strError = ex.Message
                Throw New Exception(strError)
                'Throw ex
            End Try
        End Function

        <WebMethod()>
        Public Shared Function WebMethodGetSavedStageDetails(ByVal DomainID As Integer, ByVal ProcessId As Long, ByVal OppID As Long, ByVal ProjectID As Long) As String
            Try
                Dim objAdmin = New CAdmin()
                Dim dtTable As DataTable
                objAdmin.Mode = 0
                objAdmin.DomainID = DomainID
                objAdmin.SalesProsID = ProcessId
                objAdmin.OppID = OppID
                objAdmin.ProjectID = ProjectID
                dtTable = objAdmin.StageItemDetails()
                Dim json As String = String.Empty
                json = JsonConvert.SerializeObject(dtTable, Formatting.None)
                Return json
            Catch ex As Exception
                Dim strError As String = ""

                strError = ex.Message
                Throw New Exception(strError)
                'Throw ex
            End Try
        End Function

        <WebMethod()>
        Public Shared Function WebMethodAddStageTaskGrading(ByVal DomainID As Long, ByVal UserContactId As Long, ByVal StageDetailsId As Long, ByVal AssigneIds As String, ByVal GradeId As String, ByVal TaskId As Long, ByVal StageTaskGradingDetailsId As Long, ByVal Mode As Int16) As String
            Try
                Dim objAdmin = New CAdmin()
                objAdmin.DomainID = DomainID
                objAdmin.CreatedBy = UserContactId
                objAdmin.numStageDetailsId = StageDetailsId
                objAdmin.vcAssignedIds = AssigneIds
                objAdmin.numTaskId = TaskId
                objAdmin.GradeIds = GradeId
                objAdmin.Mode = Mode
                objAdmin.numStageDetailsGradeId = StageTaskGradingDetailsId
                Dim res As String
                res = objAdmin.ManageStageItemGradeDetails()
                Dim json As String = String.Empty
                json = JsonConvert.SerializeObject(objAdmin.Status, Formatting.None)
                Return json
            Catch ex As Exception
                Dim strError As String = ""

                strError = ex.Message
                Throw New Exception(strError)
                'Throw ex
            End Try
        End Function

        <WebMethod()>
        Public Shared Function WebMethodGetStageTaskGrading(ByVal DomainID As Long, ByVal StageDetailsId As Long) As String
            Try
                Dim objAdmin = New CAdmin()
                objAdmin.DomainID = DomainID
                objAdmin.numStageDetailsId = StageDetailsId
                objAdmin.Mode = 3
                Dim json As String = String.Empty
                Dim dtConfiguration = objAdmin.GetStageItemGradeDetails()
                json = JsonConvert.SerializeObject(dtConfiguration, Formatting.None)
                Return json
            Catch ex As Exception
                Dim strError As String = ""

                strError = ex.Message
                Throw New Exception(strError)
                'Throw ex
            End Try
        End Function
        <WebMethod()>
        Public Shared Function WebMethodAddStagesToMileStone(ByVal DomainID As Long, ByVal UserContactId As Long, ByVal StagePerID As Long, ByVal ConfigurationId As Long, ByVal ProcessId As Long, ByVal StageName As String, ByVal Percentage As Long) As String
            Try
                Dim objAdmin = New CAdmin()
                Dim output As Boolean = False
                objAdmin.DomainID = DomainID
                objAdmin.UserCntID = UserContactId
                objAdmin.SalesProsID = ProcessId
                objAdmin.StageDetail = ""
                objAdmin.StagePerID = StagePerID
                objAdmin.Configuration = ConfigurationId
                objAdmin.Assignto = 0
                objAdmin.Percentage = Percentage
                objAdmin.MileStoneName = StageName
                objAdmin.Description = ""
                objAdmin.DueDays = 0

                objAdmin.StartDate = System.Data.SqlTypes.SqlDateTime.MinValue.Value
                objAdmin.EndDate = System.Data.SqlTypes.SqlDateTime.MinValue.Value
                objAdmin.ParentStageID = 0
                objAdmin.ProjectID = 0
                output = objAdmin.InsertStageDetails()
                Dim json As String = String.Empty
                json = JsonConvert.SerializeObject(output, Formatting.None)
                Return json
            Catch ex As Exception
                Dim strError As String = ""

                strError = ex.Message
                Throw New Exception(strError)
                'Throw ex
            End Try
        End Function


        <WebMethod()>
        Public Shared Function WebMethodGetTeams(ByVal DomainID As Integer) As String
            Try
                Dim objCommon = New CCommon()
                Dim dtConfiguration = objCommon.GetMasterListItems(35, DomainID)
                Dim json As String = String.Empty
                json = JsonConvert.SerializeObject(dtConfiguration, Formatting.None)
                Return json
            Catch ex As Exception
                Dim strError As String = ""

                strError = ex.Message
                Throw New Exception(strError)
                'Throw ex
            End Try
        End Function

        <WebMethod()>
        Public Shared Function WebMethodDeleteStages(ByVal DomainID As Integer, ByVal strStageDetailsIds As String) As String
            Try
                Dim objAdmin = New CAdmin()
                Dim slpid As Long = 0
                objAdmin.DomainID = DomainID
                objAdmin.strStageDetailsIds = strStageDetailsIds
                objAdmin.DeleteSubStage()
                Dim json As String = String.Empty
                json = JsonConvert.SerializeObject(True, Formatting.None)
                Return json
            Catch ex As Exception
                Dim strError As String = ""

                strError = ex.Message
                Throw New Exception(strError)
                'Throw ex
            End Try
        End Function

        <WebMethod()>
        Public Shared Function WebMethodUpdateProcess(ByVal DomainID As Long, ByVal UserContactId As Long, ByVal ProcessId As Long, ByVal ConfigurationId As Long, ByVal AssignToTeams As Boolean, ByVal AutomaticStartTimer As Boolean, ByVal SalesProsName As String, ByVal TaskValidatorId As Long, ByVal BuildManager As Long) As String
            Try
                Dim objAdmin = New CAdmin()
                Dim output As Boolean = False
                objAdmin.DomainID = DomainID
                objAdmin.UserCntID = UserContactId
                objAdmin.SalesProsID = ProcessId
                objAdmin.SalesProsName = SalesProsName
                objAdmin.Configuration = ConfigurationId
                objAdmin.bitAssigntoTeams = AssignToTeams
                objAdmin.bitAutomaticStartTimer = AutomaticStartTimer
                objAdmin.TaskValidatorId = TaskValidatorId
                objAdmin.BuildManager = BuildManager
                output = objAdmin.ManageSalesProcessList()
                Dim json As String = String.Empty
                json = JsonConvert.SerializeObject(output, Formatting.None)
                Return json
            Catch ex As Exception
                Dim strError As String = ""

                strError = ex.Message
                Throw New Exception(strError)
                'Throw ex
            End Try
        End Function

        <WebMethod()>
        Public Shared Function WebMethodUpdateStageDetails(ByVal DomainID As Long, ByVal UserContactId As Long, ByVal ConfigurationId As Long, ByVal ProcessId As Long, ByVal arrayList As List(Of StageDetails)) As String
            Try
                Dim objAdmin = New CAdmin()
                Dim output As Boolean = False

                For Each d In arrayList
                    objAdmin = New CAdmin()
                    objAdmin.DomainID = DomainID
                    objAdmin.Configuration = ConfigurationId
                    objAdmin.UserCntID = UserContactId
                    objAdmin.SalesProsID = ProcessId
                    objAdmin.numStageDetailsId = d.numStageDetailsId
                    objAdmin.StageDetail = d.StageDetail
                    objAdmin.StagePerID = d.StagePerID
                    objAdmin.Percentage = d.Percentage
                    objAdmin.MileStoneName = d.MileStoneName
                    objAdmin.DueDays = d.DueDays
                    objAdmin.StartDate = System.Data.SqlTypes.SqlDateTime.MinValue.Value
                    objAdmin.EndDate = System.Data.SqlTypes.SqlDateTime.MinValue.Value
                    objAdmin.bitIsDueDaysUsed = d.bitIsDueDaysUsed
                    objAdmin.numTeamId = d.numTeamId
                    objAdmin.bitRunningDynamicMode = d.bitRunningDynamicMode
                    objAdmin.numStageOrder = d.numStageOrder

                    objAdmin.UpdateStageDetails()
                Next
                output = True
                Dim json As String = String.Empty
                json = JsonConvert.SerializeObject(output, Formatting.None)
                Return json
            Catch ex As Exception
                Dim strError As String = ""

                strError = ex.Message
                Throw New Exception(strError)
                'Throw ex
            End Try
        End Function

        <WebMethod()>
        Public Shared Function WebMethodGetTaskAssignTo(ByVal DomainID As Integer, ByVal numTeamId As Long) As String
            Try
                Dim objCommon = New CCommon()
                Dim dtConfiguration As New DataTable()
                If numTeamId > 0 Then
                    dtConfiguration = objCommon.ConEmpListByTeam(DomainID, numTeamId)
                Else
                    dtConfiguration = objCommon.ConEmpList(DomainID, False, 0)
                End If
                Dim json As String = String.Empty
                json = JsonConvert.SerializeObject(dtConfiguration, Formatting.None)
                Return json
            Catch ex As Exception
                Dim strError As String = ""

                strError = ex.Message
                Throw New Exception(strError)
                'Throw ex
            End Try
        End Function

        <WebMethod()>
        Public Shared Function WebMethodSortTask(ByVal DomainID As Integer, ByVal sortTask As String) As String
            Try
                Dim objAdmin = New CAdmin()
                Dim output As Long = 0
                objAdmin.DomainID = DomainID
                Dim header(3) As String
                header = sortTask.Split("(")
                If (header.Length > 0) Then
                    header = header(header.Length - 1).Split(")")
                    sortTask = header(0)
                End If
                objAdmin.strTaskName = sortTask

                output = objAdmin.UpdateTaskOrder()
                Dim json As String = String.Empty
                json = JsonConvert.SerializeObject(output, Formatting.None)
                Return json
            Catch ex As Exception
                Dim strError As String = ""

                strError = ex.Message
                Throw New Exception(strError)
                'Throw ex
            End Try
        End Function

        <WebMethod()>
        Public Shared Function WebMethodAddStageTask(ByVal DomainID As Long, ByVal UserContactId As Long, ByVal StageDetailsId As Long, ByVal TaskName As String, ByVal Hours As Long, ByVal Minutes As Long, ByVal Assignto As Long, ByVal ParentTaskId As Long, ByVal OppID As Long, ByVal IsTaskClosed As Boolean, ByVal numTaskId As Long, ByVal IsTaskSaved As Boolean, ByVal IsAutoClosedTaskConfirmed As Boolean, ByVal ProjectID As Long, ByVal intTaskType As Int16, ByVal WorkOrderID As Long) As String
            Try
                Dim objAdmin = New CAdmin()
                Dim output As Boolean = False
                objAdmin.DomainID = DomainID
                objAdmin.UserCntID = UserContactId
                objAdmin.numStageDetailsId = StageDetailsId
                objAdmin.strTaskName = TaskName
                objAdmin.numHours = Hours
                objAdmin.numMinutes = Minutes
                objAdmin.Assignto = Assignto
                objAdmin.ParentTaskID = ParentTaskId
                objAdmin.OppID = OppID
                objAdmin.ProjectID = ProjectID
                objAdmin.IsTaskClosed = IsTaskClosed
                objAdmin.IsTaskSaved = IsTaskSaved
                objAdmin.numTaskId = numTaskId
                objAdmin.intTaskType = intTaskType
                objAdmin.IsAutoClosedTaskConfirmed = IsAutoClosedTaskConfirmed
                objAdmin.WorkOrderID = WorkOrderID

                objAdmin.AddStageTask()
                output = True
                Dim json As String = String.Empty
                json = JsonConvert.SerializeObject(output, Formatting.None)
                Return json
            Catch ex As Exception
                Dim strError As String = ""

                strError = ex.Message
                Throw New Exception(strError)
                'Throw ex
            End Try
        End Function

        <WebMethod()>
        Public Shared Function WebMethodGetTaskList(ByVal DomainID As Integer, ByVal StageDetailsId As Long, ByVal OppId As Long, ByVal ProjectId As Long, ByVal bitAlphabetical As Boolean) As String
            Try
                Dim objAdmin = New CAdmin()
                Dim dtConfiguration As New DataTable()

                objAdmin.DomainID = DomainID
                objAdmin.numStageDetailsId = StageDetailsId
                objAdmin.OppID = OppId
                objAdmin.ProjectID = ProjectId
                objAdmin.bitAlphabetical = bitAlphabetical
                dtConfiguration = objAdmin.GetTaskByStageDetailsId()
                Dim json As String = String.Empty
                json = JsonConvert.SerializeObject(dtConfiguration, Formatting.None)
                Return json
            Catch ex As Exception
                Dim strError As String = ""

                strError = ex.Message
                Throw New Exception(strError)
                'Throw ex
            End Try
        End Function

        <WebMethod()>
        Public Shared Function WebMethodGetFilteredTaskList(ByVal DomainID As Integer, ByVal OppId As Long, ByVal strTaskName As String) As String
            Try
                Dim objAdmin = New CAdmin()
                Dim dtConfiguration As New DataTable()

                objAdmin.DomainID = DomainID
                objAdmin.OppID = OppId
                objAdmin.strTaskName = strTaskName
                dtConfiguration = objAdmin.GetTaskByStageDetailsId()
                Dim json As String = String.Empty
                json = JsonConvert.SerializeObject(dtConfiguration, Formatting.None)
                Return json
            Catch ex As Exception
                Dim strError As String = ""

                strError = ex.Message
                Throw New Exception(strError)
                'Throw ex
            End Try
        End Function

        <WebMethod()>
        Public Shared Function WebMethodDeleteTask(ByVal DomainID As Integer, ByVal TaskId As Long) As String
            Try
                Dim objAdmin = New CAdmin()
                objAdmin.DomainID = DomainID
                objAdmin.numTaskId = TaskId
                objAdmin.DeleteTask()
                Dim json As String = String.Empty
                json = JsonConvert.SerializeObject("1", Formatting.None)
                Return json
            Catch ex As Exception
                If ex.Message.Contains("TASK_IS_MARKED_AS_FINISHED") Then
                    Throw New Exception("Can't delete task once it is finished.")
                ElseIf ex.Message.Contains("TASK_IS_ALREADY_STARTED") Then
                    Throw New Exception("Can't delete task once it is started.")
                ElseIf ex.Message.Contains("CHILD_TASK_IS_MARKED_AS_FINISHED") Then
                    Throw New Exception("Can't delete task once any child task is finished.")
                ElseIf ex.Message.Contains("CHILD_TASK_IS_ALREADY_STARTED") Then
                    Throw New Exception("Can't delete task once any child task is started.")
                ElseIf ex.Message.Contains("STAGE_MUST_HAVE_AT_LEAST_ONE_TASK") Then
                    Throw New Exception("Can't delete task because stage must have at least one task.")
                Else
                    Throw New Exception(ex.Message)
                End If
            End Try
        End Function

        <WebMethod()>
        Public Shared Function WebMethodAddProcesstoOpportunity(ByVal DomainID As Long, ByVal UserContactId As Long, ByVal ProcessId As Long, ByVal OppID As Long, ByVal ProjectId As Long) As String
            Try
                Dim objAdmin = New CAdmin()
                Dim output As String = "1"
                objAdmin.DomainID = DomainID
                objAdmin.UserCntID = UserContactId
                objAdmin.SalesProsID = ProcessId
                objAdmin.OppID = OppID
                objAdmin.ProjectID = ProjectId

                objAdmin.ManageProcessForOpportunity()
                Dim json As String = String.Empty
                json = JsonConvert.SerializeObject(output, Formatting.None)
                Return json
            Catch ex As Exception
                Dim strError As String = ""

                strError = ex.Message
                Throw New Exception(strError)
                'Throw ex
            End Try
        End Function

        <WebMethod()>
        Public Shared Function WebMethodUpdateStartDateProcesstoOpportunity(ByVal numStageDetailsId As Long, ByVal OppID As Long, ByVal ProjectID As Long, ByVal StartDate As DateTime) As String
            Try
                Dim objAdmin = New CAdmin()
                Dim output As String = "1"
                objAdmin.OppID = OppID
                objAdmin.ProjectID = ProjectID
                objAdmin.numStageDetailsId = numStageDetailsId
                objAdmin.StartDate = StartDate

                objAdmin.UpdateStartDateProcesstoOpportunity()
                Dim json As String = String.Empty
                json = JsonConvert.SerializeObject(output, Formatting.None)
                Return json
            Catch ex As Exception
                Dim strError As String = ""

                strError = ex.Message
                Throw New Exception(strError)
                'Throw ex
            End Try
        End Function

        <WebMethod()>
        Public Shared Function WebMethodDeleteProcessFromOpportunity(ByVal OppID As Long, ByVal ProjectId As Long) As String
            Try
                Dim objAdmin = New CAdmin()
                Dim output As String = "1"
                objAdmin.OppID = OppID
                objAdmin.ProjectID = ProjectId

                objAdmin.DeleteProcessFromOpportunity()
                Dim json As String = String.Empty
                json = JsonConvert.SerializeObject(output, Formatting.None)
                Return json
            Catch ex As Exception
                Dim strError As String = ""

                strError = ex.Message
                Throw New Exception(strError)
                'Throw ex
            End Try
        End Function


        <WebMethod()>
        Public Shared Function WebMethodGetDataForGanttChart(ByVal DomainId As Long, ByVal OppID As Long, ByVal ProjectID As Long) As String
            Try
                Dim objAdmin = New CAdmin()
                Dim output As String = "1"
                objAdmin.OppID = OppID
                objAdmin.ProjectID = ProjectID
                objAdmin.DomainID = DomainId
                Dim dtGanttChart As DataTable
                Dim MileStoneId As Integer = 1
                Dim lstDataforGanttChart = New List(Of TaskForGanttChart)
                Dim _taskGanttChart = New TaskForGanttChart()
                dtGanttChart = objAdmin.GetTaskForGanttChart()
                Dim arrMilestone = dtGanttChart.AsEnumerable().GroupBy(Function(r) r.Field(Of String)("MileStoneName")).Distinct()
                For Each strMileStone In arrMilestone
                    Dim MilestonePercentageCompleted As Decimal = 0
                    Dim MileStoneStartDate = dtGanttChart.AsEnumerable().Where(Function(r) r.Field(Of String)("MileStoneName") = strMileStone.Key).Select(Function(r) r.Field(Of DateTime)("StageStartDate")).Min()
                    Dim MileStoneEndDate = dtGanttChart.AsEnumerable().Where(Function(r) r.Field(Of String)("MileStoneName") = strMileStone.Key).Select(Function(r) r.Field(Of DateTime)("StageEndDate")).Max()
                    _taskGanttChart = New TaskForGanttChart()
                    _taskGanttChart.pID = MileStoneId
                    _taskGanttChart.pName = strMileStone.Key
                    _taskGanttChart.pStart = Convert.ToDateTime(MileStoneStartDate).ToString("yyyy-MM-dd")
                    _taskGanttChart.pEnd = Convert.ToDateTime(MileStoneEndDate).ToString("yyyy-MM-dd")
                    _taskGanttChart.pParent = 0
                    _taskGanttChart.pGroup = 1
                    _taskGanttChart.pOpen = 1
                    _taskGanttChart.pRes = ""
                    _taskGanttChart.pClass = "ggroupblack"
                    lstDataforGanttChart.Add(_taskGanttChart)
                    Dim arrStageDetails = dtGanttChart.AsEnumerable().Where(Function(r) r.Field(Of String)("MileStoneName") = strMileStone.Key).Select(Function(r) r.Field(Of Decimal)("numStageDetailsId")).Distinct()
                    For Each numStageDetailsId In arrStageDetails
                        Dim StageStartDate = dtGanttChart.AsEnumerable().Where(Function(r) r.Field(Of Decimal)("numStageDetailsId") = numStageDetailsId).Select(Function(r) r.Field(Of DateTime)("StageStartDate")).Min()
                        Dim StageEndDate = dtGanttChart.AsEnumerable().Where(Function(r) r.Field(Of Decimal)("numStageDetailsId") = numStageDetailsId).Select(Function(r) r.Field(Of DateTime)("StageEndDate")).Max()
                        Dim StageDetails = dtGanttChart.AsEnumerable().Where(Function(r) r.Field(Of Decimal)("numStageDetailsId") = numStageDetailsId).Select(Function(r) r).FirstOrDefault()
                        Dim arrTaskDetails = dtGanttChart.AsEnumerable().Where(Function(r) r.Field(Of Decimal)("numStageDetailsId") = numStageDetailsId).Select(Function(r) r.Field(Of Decimal)("numTaskId")).Distinct()
                        Dim arrCompletedTask = dtGanttChart.AsEnumerable().Where(Function(r) r.Field(Of Decimal)("numStageDetailsId") = numStageDetailsId And r.Field(Of Boolean)("bitTaskClosed") = True).Select(Function(r) r.Field(Of Decimal)("numTaskId")).Count()
                        Dim StageCompleteionPercentage As Decimal = 0
                        If (arrCompletedTask > 0 AndAlso arrTaskDetails.Count() > 0) Then
                            StageCompleteionPercentage = Math.Round((arrCompletedTask / arrTaskDetails.Count()) * 100)
                        End If
                        If (arrCompletedTask = arrTaskDetails.Count()) Then
                            MilestonePercentageCompleted = MilestonePercentageCompleted + StageDetails.Field(Of Int32)("tinProgressPercentage")
                        End If

                        _taskGanttChart = New TaskForGanttChart()
                        _taskGanttChart.pID = numStageDetailsId
                        _taskGanttChart.pName = Convert.ToString(StageDetails.Field(Of String)("vcStageName"))
                        _taskGanttChart.pStart = Convert.ToDateTime(StageStartDate).ToString("yyyy-MM-dd")
                        _taskGanttChart.pEnd = Convert.ToDateTime(StageEndDate).ToString("yyyy-MM-dd")
                        _taskGanttChart.pParent = MileStoneId
                        _taskGanttChart.pComp = StageCompleteionPercentage
                        _taskGanttChart.pGroup = 1
                        _taskGanttChart.pOpen = 1
                        _taskGanttChart.pClass = "gtaskblue"
                        _taskGanttChart.pComp = StageCompleteionPercentage
                        _taskGanttChart.pRes = ""
                        lstDataforGanttChart.Add(_taskGanttChart)

                        For Each numTaskId In arrTaskDetails
                            Dim TaskDetails = dtGanttChart.AsEnumerable().Where(Function(r) r.Field(Of Decimal)("numTaskId") = numTaskId).Select(Function(r) r).FirstOrDefault()
                            _taskGanttChart = New TaskForGanttChart()
                            _taskGanttChart.pID = numTaskId
                            _taskGanttChart.pName = Convert.ToString(TaskDetails.Field(Of String)("vcTaskName"))

                            Dim UserLastTaskEndDate = (From p In lstDataforGanttChart Where p.pRes = TaskDetails.Field(Of String)("vcContactName") AndAlso p.pParent = numStageDetailsId Select p.pEnd).LastOrDefault()
                            Dim noOfDays As Decimal = 0
                            If (TaskDetails.Field(Of Decimal)("numHours") > 0) Then
                                noOfDays = Math.Round(TaskDetails.Field(Of Decimal)("numHours") / 8)
                            End If
                            If (UserLastTaskEndDate Is Nothing) Then
                                _taskGanttChart.pStart = Convert.ToDateTime(StageStartDate).ToString("yyyy-MM-dd")
                                _taskGanttChart.pEnd = Convert.ToDateTime(StageStartDate).AddDays(noOfDays).ToString("yyyy-MM-dd")
                            Else
                                Dim convertedDate As String() = Convert.ToString(UserLastTaskEndDate).Split("-")
                                If (convertedDate.Length = 3) Then
                                    UserLastTaskEndDate = Convert.ToDateTime(convertedDate(1) + "/" + convertedDate(2) + "/" + convertedDate(0)).ToString("yyyy-MM-dd")
                                Else
                                    UserLastTaskEndDate = Convert.ToDateTime(StageStartDate).ToString("yyyy-MM-dd")
                                End If
                                _taskGanttChart.pStart = Convert.ToDateTime(UserLastTaskEndDate).ToString("yyyy-MM-dd")
                                _taskGanttChart.pEnd = Convert.ToDateTime(UserLastTaskEndDate).AddDays(noOfDays).ToString("yyyy-MM-dd")
                            End If
                            _taskGanttChart.pParent = numStageDetailsId
                            _taskGanttChart.pGroup = 0
                            If (TaskDetails.Field(Of Boolean)("bitTaskClosed") = True) Then
                                _taskGanttChart.pClass = "gtaskgreen"
                                _taskGanttChart.pComp = 100
                            Else
                                _taskGanttChart.pClass = "gtaskred"
                                _taskGanttChart.pComp = 0
                            End If
                            _taskGanttChart.pRes = TaskDetails.Field(Of String)("vcContactName")
                            lstDataforGanttChart.Add(_taskGanttChart)
                        Next
                    Next
                    Dim UpdateMileStoneCompletedPercentage = (From p In lstDataforGanttChart Where p.pID = MileStoneId AndAlso p.pName = strMileStone.Key Select p).FirstOrDefault()
                    UpdateMileStoneCompletedPercentage.pComp = MilestonePercentageCompleted
                    MileStoneId = MileStoneId + 1
                Next


                Dim json As String = String.Empty
                json = JsonConvert.SerializeObject(lstDataforGanttChart, Formatting.None)
                Return json
            Catch ex As Exception
                Dim strError As String = ""

                strError = ex.Message
                Throw New Exception(strError)
                'Throw ex
            End Try
        End Function
    End Class
    Public Class StageDetails
        Public Property numStageDetailsId As Long
        Public Property StagePerID As Long
        Public Property StageDetail As String
        Public Property numTeamId As Long
        Public Property Percentage As Long
        Public Property MileStoneName As String
        Public Property DueDays As Integer
        Public Property bitIsDueDaysUsed As Boolean
        Public Property bitRunningDynamicMode As Boolean
        Public Property numStageOrder As Long
    End Class
    Public Class TaskForGanttChart
        Public Property pID As Long
        Public Property pName As String
        Public Property pStart As String
        Public Property pEnd As String
        Public Property pPlanStart As String
        Public Property pPlanEnd As String
        Public Property pClass As String
        Public Property pLink As String
        Public Property pMile As Integer
        Public Property pRes As String
        Public Property pComp As Integer
        Public Property pGroup As Integer
        Public Property pParent As Integer
        Public Property pOpen As Integer
        Public Property pDepend As String
        Public Property pCaption As String
        Public Property pNotes As String
    End Class
End Namespace
