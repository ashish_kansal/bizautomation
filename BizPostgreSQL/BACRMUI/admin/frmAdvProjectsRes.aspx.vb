Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Admin
Namespace BACRM.UserInterface.Admin
    Partial Public Class frmAdvProjectsRes
        Inherits BACRMPage
        Dim boolExport As Boolean = False

        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            Try
                'CLEAR ERROR ON RELOAD
                DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ""
                DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "none")

                'CLEAR ALERT MESSAGE
                divMessage.Style.Add("display", "none")
                litMessage.Text = ""

                If Not IsPostBack Then
                    ViewState("AdvancedSearchCondition") = Session("AdvancedSearchCondition")
                    ViewState("AdvancedSearchSavedSearchID") = Session("AdvancedSearchSavedSearchID")
                    Session("AdvancedSearchCondition") = Nothing
                    Session("AdvancedSearchSavedSearchID") = Nothing

                    BindGridView(True)
                End If

                If txtReload.Text = "true" Then
                    BindGridView(True)
                    txtReload.Text = "false"
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Sub BindGridView(Optional ByVal CreateCol As Boolean = True)
            Try
                Dim objAdmin As New FormGenericAdvSearch
                Dim dtResults As DataTable
                With objAdmin
                    .QueryWhereCondition = Session("WhereContditionPro")
                    .DomainID = Session("DomainID")
                    .UserCntID = Session("UserContactID")
                    .PageSize = Session("PagingRows")
                    .FormID = 18
                    .DisplayColumns = CCommon.ToString(GetQueryStringVal("displayColumns"))
                    .SortCharacter = txtSortChar.Text.Trim()
                    .SortcolumnName = txtSortColumn.Text
                    If txtCurrrentPage.Text.Trim = "" Then txtCurrrentPage.Text = 1
                    .CurrentPage = txtCurrrentPage.Text.Trim()
                    .PageSize = Session("PagingRows")
                    .TotalRecords = 0
                    If txtSortOrder.Text = "D" Then
                        .columnSortOrder = "Desc"
                    Else : .columnSortOrder = "Asc"
                    End If
                    If Not IsPostBack Then
                        ddlSort.DataSource = .getSearchFieldList.Tables(1)              'Set the datasource for the available fields
                        ddlSort.DataTextField = "vcFormFieldName"                       'set the text field
                        ddlSort.DataValueField = "vcDbColumnName"                       'set the value attribut
                        ddlSort.DataBind()
                    End If
                    .columnName = ddlSort.SelectedValue

                    If boolExport = True Then
                        .GetAll = True
                        dtResults = .AdvancedSearchProjects()
                        Dim iExportCount As Integer
                        dtResults.Columns.RemoveAt(0)
                        Response.Clear()
                        For iExportCount = 0 To dtResults.Columns.Count - 1
                            dtResults.Columns(iExportCount).ColumnName = dtResults.Columns(iExportCount).ColumnName.Split("~")(0)
                        Next
                        For Each column As DataColumn In dtResults.Columns
                            Response.Write(column.ColumnName + ",")
                        Next
                        Response.Write(Environment.NewLine)
                        For Each row As DataRow In dtResults.Rows
                            For iExportCount = 0 To dtResults.Columns.Count - 1
                                Response.Write(row(iExportCount).ToString().Replace(";", String.Empty) & ",")
                            Next
                            Response.Write(Environment.NewLine)
                        Next
                        Response.ContentType = "text/csv"
                        Response.AppendHeader("Content-Disposition", "attachment; filename=" + "File" + Format(Now, "ddmmyyyyhhmmss") + ".csv")
                        Response.End()
                        .GetAll = False
                    End If
                    dtResults = .AdvancedSearchProjects()

                    bizPager.PageSize = Session("PagingRows")
                    bizPager.CurrentPageIndex = txtCurrrentPage.Text
                    bizPager.RecordCount = objAdmin.TotalRecords
                End With
                '   CreateCol = False
                If CreateCol = True Then
                    Dim bField As BoundField
                    Dim Tfield As TemplateField
                    Dim i As Integer
                    For i = 0 To dtResults.Columns.Count - 1
                        If dtResults.Columns(i).ColumnName = "Project Name" Then
                            Tfield = New TemplateField
                            Dim str As String()
                            str = dtResults.Columns(i).ColumnName.Split("~")

                            Tfield.HeaderTemplate = New MyTempPro(ListItemType.Header, str(0), str(1), dtResults.Columns(0).ColumnName)
                            Tfield.ItemTemplate = New MyTempPro(ListItemType.Item, str(0), str(1), dtResults.Columns(0).ColumnName)
                            gvSearch.Columns.Add(Tfield)
                        Else
                            If i < 1 Then
                                bField = New BoundField
                                bField.DataField = dtResults.Columns(i).ColumnName
                                bField.Visible = False
                                gvSearch.Columns.Add(bField)
                            Else
                                Tfield = New TemplateField
                                Dim str As String()
                                str = dtResults.Columns(i).ColumnName.Split("~")
                                Tfield.HeaderTemplate = New MyTempPro(ListItemType.Header, str(0), str(1), dtResults.Columns(0).ColumnName)

                                Tfield.ItemTemplate = New MyTempPro(ListItemType.Item, str(0), str(1), dtResults.Columns(0).ColumnName)
                                gvSearch.Columns.Add(Tfield)
                            End If
                        End If
                    Next
                End If
                gvSearch.DataSource = dtResults
                gvSearch.DataBind()
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub btnExport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExport.Click
            boolExport = True
            BindGridView(True)
        End Sub

        Private Sub btnGo1_Click(sender As Object, e As System.EventArgs) Handles btnGo1.Click
            Try
                BindGridView()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Protected Sub bizPager_PageChanged(sender As Object, e As EventArgs)
            Try
                txtCurrrentPage.Text = bizPager.CurrentPageIndex
                BindGridView(True)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Private Sub DisplayError(ByVal exception As String)
            Try
                DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = exception
                DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
                DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Focus()
            Catch ex As Exception

            End Try
        End Sub

        Private Sub ShowMessage(ByVal message As String)
            Try
                divMessage.Style.Add("display", "")
                litMessage.Text = message
                divMessage.Focus()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Private Sub lkbBackToSearchCriteria_Click(sender As Object, e As EventArgs) Handles lkbBackToSearchCriteria.Click
            Try
                Session("AdvancedSearchCondition") = ViewState("AdvancedSearchCondition")
                Session("AdvancedSearchSavedSearchID") = ViewState("AdvancedSearchSavedSearchID")
                Response.Redirect("~/admin/frmAdvancedSearchNew.aspx", False)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub
    End Class

    Public Class MyTempPro
        Implements ITemplate

        Dim TemplateType As ListItemType
        Dim Field1, Field2, Field3 As String

        Sub New(ByVal type As ListItemType, ByVal fld1 As String, ByVal fld2 As String, ByVal fld3 As String)
            Try
                TemplateType = type
                Field1 = fld1
                Field2 = fld2
                Field3 = fld3
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub InstantiateIn(ByVal Container As Control) Implements ITemplate.InstantiateIn
            Try
                Dim lbl1 As Label = New Label()
                Dim lnkButton As New LinkButton
                Dim lnk As New HyperLink
                Select Case TemplateType
                    Case ListItemType.Header
                        If Field1 <> "CheckBox" Then
                            lnkButton.ID = Field2
                            lnkButton.Text = Field1
                            lnkButton.Attributes.Add("onclick", "return SortColumn('" & Field2 & "')")
                            Container.Controls.Add(lnkButton)
                        Else
                            '  Dim chk As New CheckBox
                            'chk.ID = "chk"
                            '  chk.Attributes.Add("onclick", "return SelectAll('gvSearch_ctl01_chk')")
                            ' Container.Controls.Add(chk)
                        End If
                    Case ListItemType.Item
                        If Field1 <> "CheckBox" Then
                            AddHandler lbl1.DataBinding, AddressOf BindStringColumn
                            Container.Controls.Add(lbl1)
                        Else
                            ' Dim chk As New CheckBox
                            ' chk.ID = "chk"
                            ' AddHandler lbl1.DataBinding, AddressOf Bindvalue
                            'Container.Controls.Add(chk)
                            'Container.Controls.Add(lbl1)
                        End If
                End Select
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub Bindvalue(ByVal Sender As Object, ByVal e As EventArgs)
            Try
                Dim lbl1 As Label = CType(Sender, Label)
                Dim Container As GridViewRow = CType(lbl1.NamingContainer, GridViewRow)
                lbl1.ID = "lbl1"
                lbl1.Text = DataBinder.Eval(Container.DataItem, Field3)
                lbl1.Attributes.Add("style", "display:none")
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub BindStringColumn(ByVal Sender As Object, ByVal e As EventArgs)
            Try
                Dim lbl1 As Label = CType(Sender, Label)
                Dim Container As GridViewRow = CType(lbl1.NamingContainer, GridViewRow)
                If Field1 = "Project Name" Then
                    lbl1.Text = IIf(IsDBNull(DataBinder.Eval(Container.DataItem, Field1 & "~" & Field2)), "", DataBinder.Eval(Container.DataItem, Field1 & "~" & Field2))
                    Dim intermediatory As Integer
                    intermediatory = IIf(System.Web.HttpContext.Current.Session("EnableIntMedPage") = 1, 1, 0)
                    If Field1 = "Project Name" Then
                        lbl1.Text = "<a  href='javascript:OpenProject(" & DataBinder.Eval(Container.DataItem, Field3) & "," & intermediatory & ",0)'>" & lbl1.Text & "</a> &nbsp;&nbsp;"
                        lbl1.Text += "<a  href='javascript:OpenProject(" & DataBinder.Eval(Container.DataItem, Field3) & "," & intermediatory & ",1)'><img src='../images/open_new_window_notify.gif' width='15px' /></a>"
                    End If
                Else : lbl1.Text = IIf(IsDBNull(DataBinder.Eval(Container.DataItem, Field1 & "~" & Field2)), "", DataBinder.Eval(Container.DataItem, Field1 & "~" & Field2))
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub BindStringColumn1(ByVal Sender As Object, ByVal e As EventArgs)
            Try
                Dim lbl1 As Label = CType(Sender, Label)
                Dim Container As GridViewRow = CType(lbl1.NamingContainer, GridViewRow)
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

    End Class
End Namespace