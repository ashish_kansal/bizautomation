﻿Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Common

Namespace BACRM.UserInterface.Admin
    Public Class frmAddShippingServiceAbbr
        Inherits BACRMPage

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub
        'Protected WithEvents btnClose As System.Web.UI.WebControls.Button
        'Protected WithEvents Table2 As System.Web.UI.WebControls.Table
        'Protected WithEvents btnAdd As System.Web.UI.WebControls.Button
        'Protected WithEvents litMessage As System.Web.UI.WebControls.Literal
        'Dim myDataTable As DataTable
        'Protected WithEvents ddlShipVia As System.Web.UI.WebControls.DropDownList
        'Protected WithEvents ddlShippingService As System.Web.UI.WebControls.DropDownList
        'Protected WithEvents txtState As System.Web.UI.WebControls.TextBox
        'Protected WithEvents dgCountry As System.Web.UI.WebControls.DataGrid
        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.
        Private designerPlaceholderDeclaration As System.Object

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

#Region "Member Variables"

        Private objShippingService As ShippingService

#End Region

#Region "Page Events"

        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            Try
                GetUserRightsForPage(13, 23)

                If m_aryRightsForPage(RIGHTSTYPE.ADD) = 0 Then
                    btnAdd.Visible = False
                End If

                If Not IsPostBack Then
                    BindShipViaddl()
                    BindGrid()
                End If
                btnClose.Attributes.Add("onclick", "return Close()")
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

#End Region

#Region "Private Methods"

        Sub BindShipViaddl()
            Try
                Dim objCommon As New CCommon
                objCommon.DomainID = Session("DomainID")
                objCommon.ListID = 82 ' For Ship Via

                Dim dtShipVia As New DataTable

                dtShipVia = objCommon.GetMasterListItemsWithRights

                ddlShipVia.DataSource = dtShipVia
                ddlShipVia.DataTextField = "vcData"
                ddlShipVia.DataValueField = "numListItemID"
                ddlShipVia.DataBind()

                ddlShipVia.Items.Insert(0, New ListItem("-- Select One --", 0))
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Sub BindGrid()
            Try
                objShippingService = New ShippingService
                objShippingService.DomainID = Session("DomainID")
                objShippingService.ShipVia = CCommon.ToLong(ddlShipVia.SelectedValue)
                dgShippingService.DataSource = objShippingService.GetByShipVia()
                dgShippingService.DataBind()
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub DisplayError(ByVal errorMessage As String)
            Try
                lblError.Text = errorMessage
                divError.Style.Add("display", "")
            Catch ex As Exception
                'DO NOT THROW ERROR
            End Try
        End Sub

#End Region

#Region "Event Handlers"

        Protected Sub ddlShipVia_SelectedIndexChanged(sender As Object, e As EventArgs)
            Try
                BindGrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Protected Sub btnAdd_Click(sender As Object, e As EventArgs)
            Try
                If CCommon.ToLong(ddlShipVia.SelectedValue) = 0 Or String.IsNullOrEmpty(txtShippingService.Text.Trim()) Then
                    ScriptManager.RegisterClientScriptBlock(Me.Page, Me.Page.GetType(), "ValidationError", "alert('Ship via and Shipping service are required.');", True)
                    Exit Sub
                Else
                    objShippingService = New ShippingService
                    objShippingService.DomainID = Session("DomainID")
                    objShippingService.ShipVia = CCommon.ToLong(ddlShipVia.SelectedValue)
                    objShippingService.ShippingService = CCommon.ToString(txtShippingService.Text).Trim()
                    objShippingService.Abbrevations = CCommon.ToString(txtAbbreviations.Text).Trim()
                    objShippingService.Save()

                    Call BindGrid()

                    txtShippingService.Text = ""
                    txtAbbreviations.Text = ""
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try

        End Sub

        Private Sub dgShippingService_ItemDataBound(sender As Object, e As DataGridItemEventArgs) Handles dgShippingService.ItemDataBound
            Try
                If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
                    If CCommon.ToLong(DataBinder.Eval(e.Item.DataItem, "numDomainID")) = 0 Then
                        e.Item.FindControl("lkbDelete").Visible = False
                        e.Item.FindControl("lblDefault").Visible = True
                    Else
                        e.Item.FindControl("lkbDelete").Visible = True
                        e.Item.FindControl("lblDefault").Visible = False
                    End If
                ElseIf e.Item.ItemType = ListItemType.EditItem Then
                    If CCommon.ToLong(DataBinder.Eval(e.Item.DataItem, "numDomainID")) = 0 Then
                        DirectCast(e.Item.FindControl("txtEShippingService"), TextBox).Enabled = False
                    Else
                        DirectCast(e.Item.FindControl("txtEShippingService"), TextBox).Enabled = True
                    End If
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Protected Sub dgShippingService_ItemCommand(source As Object, e As DataGridCommandEventArgs)
            Try
                Dim numShippingServiceId As Long = CType(e.Item.FindControl("lblShipppingServiceID"), Label).Text

                If e.CommandName = "Cancel" Then
                    dgShippingService.EditItemIndex = e.Item.ItemIndex
                    dgShippingService.EditItemIndex = -1
                    Call BindGrid()
                End If

                If e.CommandName = "Delete" Then
                    objShippingService = New ShippingService
                    objShippingService.DomainID = Session("DomainID")
                    objShippingService.ShippingServiceID = numShippingServiceId
                    objShippingService.Delete()

                    Call BindGrid()
                End If

                If e.CommandName = "Update" Then
                    If String.IsNullOrEmpty(CCommon.ToString(CType(e.Item.FindControl("txtEShippingService"), TextBox).Text)) Then
                        ScriptManager.RegisterClientScriptBlock(Me.Page, Me.Page.GetType(), "ValidationError", "alert('Shipping service is required.');", True)
                        Exit Sub
                    Else
                        objShippingService = New ShippingService
                        objShippingService.DomainID = Session("DomainID")
                        objShippingService.ShippingServiceID = numShippingServiceId
                        objShippingService.ShippingService = CCommon.ToString(CType(e.Item.FindControl("txtEShippingService"), TextBox).Text).Trim()
                        objShippingService.Abbrevations = CCommon.ToString(CType(e.Item.FindControl("txtEAbbreviations"), TextBox).Text).Trim()
                        objShippingService.Save()

                        dgShippingService.EditItemIndex = -1
                        Call BindGrid()
                    End If
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Protected Sub dgShippingService_EditCommand(source As Object, e As DataGridCommandEventArgs)
            Try
                dgShippingService.EditItemIndex = e.Item.ItemIndex

               

                Call BindGrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

#End Region
        
        
    End Class

End Namespace