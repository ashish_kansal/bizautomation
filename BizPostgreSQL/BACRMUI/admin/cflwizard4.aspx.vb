Imports BACRM.BusinessLogic.Common
Namespace BACRM.UserInterface.Admin
    Public Class cflwizard4
        Inherits BACRMPage
        Protected WithEvents Imagnext As System.Web.UI.WebControls.ImageButton
        Protected WithEvents CFWlbl As System.Web.UI.WebControls.TextBox

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Try
                If Session("fldlbl") <> "" Then CFWlbl.Text = Session("fldlbl")
                Session("fldlbl") = ""
                
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub ImageButton1_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs)

        End Sub

        Private Sub Imaback_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs)
            Try
                Response.Redirect("../admin/cflwizard3.aspx")
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub Imagnext_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Imagnext.Click
            Try
                If Len(Trim((CFWlbl.Text))) = 0 Then
                    CFWlbl.Text = ""
                    Response.Write("<script language=javascript>")
                    Response.Write("alert('Specify label')")
                    Response.Write("</script>")
                    Exit Sub
                End If
                Dim fldlbl As String
                fldlbl = CFWlbl.Text
                Session("fldlbl") = fldlbl
                Session("tabord") = ""
                Response.Redirect("../admin/cflwizard5.aspx")
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

    End Class
End Namespace