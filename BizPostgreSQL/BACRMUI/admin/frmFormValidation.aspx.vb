﻿Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Common

Namespace BACRM.UserInterface.Admin
    Public Class frmFormValidation
        Inherits BACRMPage
       
        Dim SortField As String
        Dim m_lngStartFrom As Long
#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Try
                DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ""
                DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "none")
                GetUserRightsForPage(13, 42)

                If Not IsPostBack Then

                    LoadLocation()
                    ' = Request.Url.Segments(Request.Url.Segments.Length - 1) 
                    BindList()
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub
        Private Sub DisplayError(ByVal ex As String)
            DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ex
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
        End Sub
        Sub LoadLocation()
            Try
                Dim objConfigWizard As New FormConfigWizard             'Create an object of form config wizard
                objConfigWizard.DomainID = Session("DomainID")          'Set value of domain id
                objConfigWizard.tintType = 5

                Dim dtFormsList As DataTable
                dtFormsList = objConfigWizard.getFormList()

                Dim ds As DataSet = dtFormsList.DataSet
                objConfigWizard.tintType = 6

                dtFormsList = New DataTable
                dtFormsList = objConfigWizard.getFormList()
                ds.Merge(dtFormsList.DataSet, True)

                Dim view As DataView = ds.Tables(0).DefaultView
                view.Sort = "vcFormName"

                ddlFilter.DataSource = view
                ddlFilter.DataTextField = "vcFormName"
                ddlFilter.DataValueField = "numFormId"
                ddlFilter.DataBind()
                ddlFilter.SelectedIndex = 0
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub BindList()
            Try
                Dim dtFld As DataTable
                Dim objField As New FormConfigWizard()
                objField.DomainID = Session("DomainID")
                objField.FormID = ddlFilter.SelectedValue
                objField.tintType = 0
                dtFld = objField.GetDynamicFormFieldList

                gvFormFields.DataSource = dtFld
                gvFormFields.DataBind()
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub ddlFilter_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlFilter.SelectedIndexChanged
            Try
                BindList()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        'Private Sub gvFormFields_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvFormFields.RowCommand
        '    If e.CommandName = "Save" Then
        '        Try
        '            Dim objField As New FormConfigWizard()

        '            Dim index As Integer = Convert.ToInt32(e.CommandArgument)
        '            Dim row As GridViewRow = gvFormFields.Rows(index)

        '            Dim txtvcNewFormFieldName As TextBox
        '            txtvcNewFormFieldName = CType(row.FindControl("txtvcNewFormFieldName"), TextBox)

        '            objField.DomainID = Session("DomainID")
        '            objField.FormID = ddlFilter.SelectedValue
        '            objField.FormFieldId = gvFormFields.DataKeys(index).Value
        '            objField.vcFormFieldName = txtvcNewFormFieldName.Text

        '            objField.saveDynamicFormFieldList()

        '            BindList()
        '        Catch ex As Exception
        '            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
        '            Response.Write(ex)
        '        End Try
        '    End If
        'End Sub

        Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSave.Click
            Try
                Dim objField As New FormConfigWizard()
                Dim txtvcNewFormFieldName As TextBox = New TextBox

                For Each gvr As GridViewRow In gvFormFields.Rows
                    txtvcNewFormFieldName = CType(gvr.FindControl("txtvcNewFormFieldName"), TextBox)

                    If txtvcNewFormFieldName.Text.Trim.Length = 0 Then
                        txtvcNewFormFieldName.Text = gvr.Cells(0).Text
                    End If

                    If txtvcNewFormFieldName.Text <> CType(gvr.FindControl("hdvcNewFormFieldName"), HiddenField).Value.Trim Or CType(gvr.FindControl("txtvcToolTip"), TextBox).Text.Trim <> gvr.Cells(5).Text Then
                        objField.DomainID = Session("DomainID")
                        objField.FormID = ddlFilter.SelectedValue
                        objField.FormFieldId = gvFormFields.DataKeys(gvr.RowIndex).Value
                        objField.vcFormFieldName = txtvcNewFormFieldName.Text.Trim
                        objField.vcToolTip = CType(gvr.FindControl("txtvcToolTip"), TextBox).Text.Trim

                        objField.saveDynamicFormFieldList()
                    End If
                Next

                BindList()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Sub gvFormFields_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvFormFields.RowDataBound
            If e.Row.RowType = ListItemType.AlternatingItem Or e.Row.RowType = ListItemType.Item Then
                Dim str As String = ""

                If (DataBinder.Eval(e.Row.DataItem, "bitIsRequired")) Then
                    str += ",Required"
                End If

                If (DataBinder.Eval(e.Row.DataItem, "bitIsEmail")) Then
                    str += ",Email"
                ElseIf (DataBinder.Eval(e.Row.DataItem, "bitIsAlphaNumeric")) Then
                    str += ",Alpha Numeric"
                ElseIf (DataBinder.Eval(e.Row.DataItem, "bitIsNumeric")) Then
                    str += ",Numeric"
                ElseIf (DataBinder.Eval(e.Row.DataItem, "bitIsLengthValidation")) Then
                    str += ",Length Validation"
                End If

                e.Row.Cells(3).Text = str.Trim(",")
            End If
        End Sub
    End Class
End Namespace