﻿Imports BACRM.BusinessLogic.Accounting
Imports BACRM.BusinessLogic.Common
Partial Public Class frmRelationshipMapping
    Inherits BACRMPage
    Dim lngRelationshipID As Long
    Dim lngARAccountID As Long
    Dim lngAPAccountID As Long
   
    
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            GetUserRightsForPage(13, 1)

            If m_aryRightsForPage(RIGHTSTYPE.VIEW) = 0 Then
                Response.Redirect("../admin/authentication.aspx?mesg=AS")
            ElseIf m_aryRightsForPage(RIGHTSTYPE.ADD) = 0 Then
                btnSave.Visible = False
            End If

            If Not IsPostBack Then
                objCommon.sb_FillComboFromDBwithSel(ddlRelationship, 5, Session("DomainID"))
                BindDropdown()
            End If

            If Not ViewState("RelationshipID") Is Nothing Then
                lngRelationshipID = ViewState("RelationshipID")
            End If

            litMessage.Text = ""
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
    Private Sub BindRelationship()
        Try
            Dim objCOA As New ChartOfAccounting
            Dim ds As DataSet
            With objCOA
                .COARelationshipID = 0
                .RelationshipID = ddlRelationship.SelectedValue
                .DomainId = Session("DomainID")
                ds = .GetCOARelationship()
            End With
            ddlAPAccountType.ClearSelection()
            ddlARAccountType.ClearSelection()
            ddlARAccount.ClearSelection()
            ddlAPAccount.ClearSelection()
            If ds.Tables(0).Rows.Count > 0 Then
                Dim dr As DataRow = ds.Tables(0).Rows(0)
                hdnPrimaryKey.Value = dr.Item("numCOARelationshipID")
                ViewState("RelationshipID") = dr.Item("numRelationshipID")
                If Not IsDBNull(dr.Item("numARParentAcntTypeID")) Then
                    If Not ddlARAccountType.Items.FindByValue(dr.Item("numARParentAcntTypeID")) Is Nothing Then
                        ddlARAccountType.Items.FindByValue(dr.Item("numARParentAcntTypeID")).Selected = True
                    End If
                End If
                If Not IsDBNull(dr.Item("numAPParentAcntTypeID")) Then
                    If Not ddlAPAccountType.Items.FindByValue(dr.Item("numAPParentAcntTypeID")) Is Nothing Then
                        ddlAPAccountType.Items.FindByValue(dr.Item("numAPParentAcntTypeID")).Selected = True
                    End If
                End If
                If Not IsDBNull(dr.Item("numARAccountId")) Then
                    If Not ddlARAccount.Items.FindByValue(dr.Item("numARAccountId") & "~" & dr.Item("numARParentAcntTypeID")) Is Nothing Then
                        ddlARAccount.Items.FindByValue(dr.Item("numARAccountId") & "~" & dr.Item("numARParentAcntTypeID")).Selected = True
                    End If
                End If
                If Not IsDBNull(dr.Item("numAPAccountId")) Then
                    If Not ddlAPAccount.Items.FindByValue(dr.Item("numAPAccountId") & "~" & dr.Item("numAPParentAcntTypeID")) Is Nothing Then
                        ddlAPAccount.Items.FindByValue(dr.Item("numAPAccountId") & "~" & dr.Item("numAPParentAcntTypeID")).Selected = True
                    End If
                End If

                lblARAccountName.Text = dr.Item("ARAccountName")
                lblAPAccountName.Text = dr.Item("APAccountName")
                lngARAccountID = dr.Item("numARAccountId")
                lngAPAccountID = dr.Item("numAPAccountId")
                hdnARAccountID.Value = lngARAccountID
                hdnAPAccountID.Value = lngAPAccountID
                hdnARAccountTypeID.Value = dr.Item("numARParentAcntTypeID")
                hdnAPAccountTypeID.Value = dr.Item("numAPParentAcntTypeID")
                pnlAutoCreate.Visible = False
                rbManuall.Checked = True
            Else
                lblARAccountName.Text = ""
                lblAPAccountName.Text = ""
                lngARAccountID = 0
                lngAPAccountID = 0
                hdnARAccountID.Value = 0
                hdnAPAccountID.Value = 0
                hdnARAccountTypeID.Value = 0
                hdnAPAccountTypeID.Value = 0
                hdnPrimaryKey.Value = 0
                pnlAutoCreate.Visible = True
                rbAutoCreate.Checked = True
            End If
            If lngARAccountID > 0 Then
                ddlARAccountType.Enabled = False
                'ddlARAccount.Enabled = False
            Else
                ddlARAccountType.Enabled = True
                'ddlARAccount.Enabled = True
            End If
            If lngAPAccountID > 0 Then
                ddlAPAccountType.Enabled = False
                'ddlAPAccount.Enabled = False
            Else
                ddlAPAccountType.Enabled = True
                'ddlAPAccount.Enabled = True
            End If

            'If lngARAccountID > 0 And lngAPAccountID > 0 Then
            '    btnSave.Visible = False
            'Else
            '    btnSave.Visible = True
            'End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Private Sub BindDropdown()
        Try
            Dim objCOA As New ChartOfAccounting
            objCOA.DomainId = Session("DomainId")
            objCOA.Mode = 1 'get all account type with code length = 4
            Dim dt As DataTable = objCOA.GetAccountTypes().Tables(0)
            Dim dRows() As DataRow = dt.Select("VcAccountCode = '0101'", "")
            For Each dr As DataRow In dRows
                hdnARAccountTypeID.Value = dr("numAccountTypeID")
            Next
            dRows = dt.Select("VcAccountCode = '0102'", "")
            For Each dr As DataRow In dRows
                hdnAPAccountTypeID.Value = dr("numAccountTypeID")
            Next

            Dim ds As DataSet
            objCOA.DomainId = Session("DomainID")
            objCOA.Mode = 2
            objCOA.AccountTypeID = hdnARAccountTypeID.Value
            ds = objCOA.GetAccountTypes()
            ddlARAccountType.DataSource = ds.Tables(0)
            ddlARAccountType.DataTextField = "vcAccountType1"
            ddlARAccountType.DataValueField = "numAccountTypeID"
            ddlARAccountType.DataBind()
            ddlARAccountType.Items.Insert(0, New ListItem("-- Select One --", "0"))

            objCOA.AccountTypeID = hdnAPAccountTypeID.Value
            ds = objCOA.GetAccountTypes()
            ddlAPAccountType.DataSource = ds.Tables(0)
            ddlAPAccountType.DataTextField = "vcAccountType1"
            ddlAPAccountType.DataValueField = "numAccountTypeID"
            ddlAPAccountType.DataBind()
            ddlAPAccountType.Items.Insert(0, New ListItem("-- Select One --", "0"))


            objCOA.DomainId = Session("DomainId")
            objCOA.AccountCode = "0101"
            Dim dtChartAcnt As DataTable = objCOA.GetParentCategory()
            LoadChartOfAccounts(ddlARAccount, dtChartAcnt)

            objCOA.AccountCode = "0102"
            dtChartAcnt = objCOA.GetParentCategory()
            LoadChartOfAccounts(ddlAPAccount, dtChartAcnt)

            hdnARAccountTypeID.Value = 0
            hdnAPAccountTypeID.Value = 0
            hdnARAccountID.Value = 0
            hdnAPAccountID.Value = 0
          
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Sub LoadChartOfAccounts(ByVal ddlAccounts As DropDownList, ByVal dtChartAcnt As DataTable)
        Try
            ddlAccounts.Items.Clear()
            Dim item As ListItem
            For Each dr As DataRow In dtChartAcnt.Rows
                item = New ListItem()
                item.Text = dr("vcAccountName")
                item.Value = dr("numAccountID") & "~" & dr("numParntAcntTypeId") 'added parent account type code , bug 949
                item.Attributes("OptionGroup") = dr("vcAccountType")
                ddlAccounts.Items.Add(item)
            Next
            ddlAccounts.Items.Insert(0, New ListItem("--Select One --", "0"))
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            Dim objCOA As New ChartOfAccounting
            'Auto greate Account
            If rbAutoCreate.Checked Then
                'Create AR and AP Accounts for whichever Type is selected
                If ddlARAccountType.SelectedValue > 0 And CCommon.ToLong(hdnARAccountID.Value) = 0 Then
                    With objCOA
                        .AccountId = lngARAccountID
                        .CategoryName = ddlRelationship.SelectedItem.Text & " -A/R"
                        .CategoryDescription = ddlRelationship.SelectedItem.Text
                        .OpeningDate = Now.UtcNow
                        .decOpeningBalance = 0
                        .decOriginalOpeningBal = 0
                        .BitDepreciation = 0
                        .BitFixed = 0
                        .Active = 1
                        .DomainId = Session("DomainId")
                        .UserCntId = Session("UserContactId")
                        .numListItemID = 0
                        .AccountTypeID = hdnARAccountTypeID.Value
                        .ParentAccountId = ddlARAccountType.SelectedValue
                        .IsProfitLossAcnt = False
                        Try
                            lngARAccountID = .CreateRecordAddAccountChartInfo()
                        Catch ex As Exception
                            If ex.Message = "NoProfitLossACC" Then
                                litMessage.Text = "No Profit / Loss Account found,your option is to add account of account type Equity with profit/loss enabled and try again"
                                Exit Sub
                            ElseIf ex.Message = "NoCurrentFinancialYearSet" Then
                                'litMessage.Text = "Please set Current Financial Year from ""Accounting->Financial Year""  to create new Chart of Account"
                                litMessage.Text = "Please set Current Financial Year from ""Accounting->Financial Year"" to save record"
                                Exit Sub
                            Else
                                Throw ex
                            End If
                        End Try
                        hdnARAccountID.Value = lngARAccountID
                    End With
                End If
                If ddlAPAccountType.SelectedValue > 0 And CCommon.ToLong(hdnAPAccountID.Value) = 0 Then
                    With objCOA
                        .AccountId = lngAPAccountID
                        .CategoryName = ddlRelationship.SelectedItem.Text & " -A/P"
                        .CategoryDescription = ddlRelationship.SelectedItem.Text
                        .OpeningDate = Now.UtcNow
                        .decOpeningBalance = 0
                        .decOriginalOpeningBal = 0
                        .BitDepreciation = 0
                        .BitFixed = 0
                        .Active = 1
                        .DomainId = Session("DomainId")
                        .UserCntId = Session("UserContactId")
                        .numListItemID = 0
                        .AccountTypeID = hdnAPAccountTypeID.Value
                        .ParentAccountId = ddlAPAccountType.SelectedValue
                        .IsProfitLossAcnt = False
                        Try

                            lngAPAccountID = .CreateRecordAddAccountChartInfo()
                        Catch ex As Exception
                            If ex.Message = "NoProfitLossACC" Then
                                litMessage.Text = "No Profit / Loss Account found,your option is to add account of account type Equity with profit/loss enabled and try again"
                                Exit Sub
                            ElseIf ex.Message = "NoCurrentFinancialYearSet" Then
                                'litMessage.Text = "Please set Current Financial Year from ""Accounting->Financial Year""  to create new Chart of Account"
                                litMessage.Text = "Please set Current Financial Year from ""Accounting->Financial Year"" to save record"
                                Exit Sub
                            Else
                                Throw ex
                            End If
                        End Try
                        hdnAPAccountID.Value = lngAPAccountID
                    End With
                End If
            Else
                If ddlARAccount.SelectedValue <> "0" Then
                    hdnARAccountID.Value = ddlARAccount.SelectedValue.Split("~")(0)
                    hdnARAccountTypeID.Value = ddlARAccount.SelectedValue.Split("~")(1)
                Else
                    hdnARAccountID.Value = 0
                    hdnARAccountTypeID.Value = 0
                End If
                If ddlAPAccount.SelectedValue <> "0" Then
                    hdnAPAccountID.Value = ddlAPAccount.SelectedValue.Split("~")(0)
                    hdnAPAccountTypeID.Value = ddlAPAccount.SelectedValue.Split("~")(1)
                Else
                    hdnAPAccountID.Value = 0
                    hdnAPAccountTypeID.Value = 0
                End If
            End If


            


            If ddlRelationship.SelectedValue <> 0 And (hdnARAccountID.Value > 0 Or hdnAPAccountID.Value > 0) Then
                With objCOA
                    .COARelationshipID = CCommon.ToLong(hdnPrimaryKey.Value)
                    .RelationshipID = ddlRelationship.SelectedValue
                    .ARAccountTypeID = IIf(rbAutoCreate.Checked, ddlARAccountType.SelectedValue, hdnARAccountTypeID.Value)
                    .APAccountTypeID = IIf(rbAutoCreate.Checked, ddlAPAccountType.SelectedValue, hdnAPAccountTypeID.Value)
                    .ARAccountID = CCommon.ToLong(hdnARAccountID.Value)
                    .APAccountID = CCommon.ToLong(hdnAPAccountID.Value)
                    .DomainId = Session("DomainID")
                    .UserCntId = Session("UserContactID")
                    hdnPrimaryKey.Value = .ManageCOARelationship()
                    lngRelationshipID = ddlRelationship.SelectedValue
                End With
            Else
                litMessage.Text = "Problem in Saving values"
            End If
            'rebind accounts dropdown which are just created
            If rbAutoCreate.Checked Then BindDropdown()
            BindRelationship()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
    Private Sub ddlRelationship_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlRelationship.SelectedIndexChanged
        Try
            If ddlRelationship.SelectedValue <> "" Then
                BindRelationship()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
End Class