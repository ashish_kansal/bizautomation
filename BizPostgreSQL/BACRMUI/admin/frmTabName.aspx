<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmTabName.aspx.vb" Inherits="BACRM.UserInterface.Admin.frmTabName"
    MasterPageFile="~/common/Popup.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Tab Name</title>
    <script>
        function Check() {
            if (document.getElementById("txtLead").value == '') {
                alert('Enter Lead Tab Name');
                return false
            }

            if (document.getElementById("txtContact").value == '') {
                alert('Enter Contact Tab Name');
                return false
            }
            if (document.getElementById("txtProspect").value == '') {
                alert('Enter Prospect Tab Name');
                return false
            }
            if (document.getElementById("txtAccount").value == '') {
                alert('Enter Account Tab Name');
                return false
            }
            return true
        }
       
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <div class="input-part">
        <div class="right-input">
            <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save"></asp:Button>
            <asp:Button ID="btnSaveClose" runat="server" CssClass="button" Text="Save & Close">
            </asp:Button>
            <asp:Button ID="btnClose" runat="server" CssClass="button" Text="Close" OnClientClick="javascript:window.close()">
            </asp:Button>&nbsp;&nbsp;
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageTitle" runat="server" ClientIDMode="Static">
    Relable default relationship
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="Content" runat="server" ClientIDMode="Static">
    <table class="normal1" width="600px" cellpadding="0" cellspacing="0">
        <tr>
            <td align="right" class="normal7">
                Lead &nbsp&nbsp
            </td>
            <td align="left" class="normal1">
                <asp:TextBox CssClass="signup" ID="txtLead" runat="server" Text="Lead" MaxLength="20"></asp:TextBox>
                Plural:<asp:TextBox CssClass="signup" ID="txtLeadPlural" runat="server" Text="Leads"
                    MaxLength="20"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td align="right" class="normal7">
                Contact &nbsp&nbsp
            </td>
            <td align="left">
                <asp:TextBox CssClass="signup" ID="txtContact" runat="server" Text="Contact" MaxLength="20"></asp:TextBox>
                Plural:<asp:TextBox CssClass="signup" ID="txtContactPlural" runat="server" Text="Contacts"
                    MaxLength="20"></asp:TextBox>
            </td>
            <tr>
                <td align="right" class="normal7">
                    Prospect &nbsp&nbsp
                </td>
                <td align="left">
                    <asp:TextBox CssClass="signup" ID="txtProspect" runat="server" Text="Prospect" MaxLength="20"></asp:TextBox>
                    Plural:<asp:TextBox CssClass="signup" ID="txtProspectPlural" runat="server" Text="Prospects"
                        MaxLength="20"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td align="right" class="normal7">
                    Account &nbsp&nbsp
                </td>
                <td align="left">
                    <asp:TextBox CssClass="signup" ID="txtAccount" runat="server" Text="Account" MaxLength="20"></asp:TextBox>
                    Plural:<asp:TextBox CssClass="signup" ID="txtAccountPlural" runat="server" Text="Accounts"
                        MaxLength="20"></asp:TextBox>
                </td>
            </tr>
    </table>
</asp:Content>
