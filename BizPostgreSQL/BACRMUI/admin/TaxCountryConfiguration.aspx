﻿<%@ Page Title="Tax Country Configuration" Language="vb" AutoEventWireup="false"
    MasterPageFile="~/common/Popup.Master" CodeBehind="TaxCountryConfiguration.aspx.vb"
    Inherits=".TaxCountryConfiguration" %>

<asp:Content ID="Content3" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Tax Country Configuration</title>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <div class="input-part">
        <div class="right-input">
            <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save"></asp:Button>
            <asp:Button ID="btnSaveClose" runat="server" CssClass="button" Text="Save &amp; Close">
            </asp:Button>
            <asp:Button ID="btnCancel" runat="server" CssClass="button" Width="50" Text="Close">
            </asp:Button>&nbsp;&nbsp;
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="PageTitle" runat="server" ClientIDMode="Static">
    Tax Country Configuration
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="Content" runat="server" ClientIDMode="Static">
    <asp:Table ID="Table2" runat="server" GridLines="None" BorderColor="black" Width="600px"
        BorderWidth="1" CssClass="aspTable" CellSpacing="0" CellPadding="0">
        <asp:TableRow>
            <asp:TableCell VerticalAlign="Top">
                <table id="table1" width="100%">
                    <tr class="normal1" align="right">
                        <td class="normal1" align="right">
                            Country
                        </td>
                        <td align="left">
                            <asp:HiddenField ID="hfTaxCountryConfiId" runat="server" Value="0" />
                            <asp:DropDownList CssClass="signup" ID="ddlCountry" runat="server">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <%--<tr>
                        <td class="normal1" align="right">
                            Base tax calculation on :
                        </td>
                        <td align="left">
                            <asp:RadioButtonList ID="rblBaseTax" runat="server" RepeatDirection="Horizontal"
                                CssClass="signup">
                                <asp:ListItem Text="Shipping Address" Value="2" Selected="True"></asp:ListItem>
                                <asp:ListItem Text="Billing Address" Value="1"></asp:ListItem>
                            </asp:RadioButtonList>
                        </td>
                    </tr>--%>
                    <tr>
                        <td class="normal1" align="right">
                            Base tax on :
                        </td>
                        <td align="left">
                            <asp:RadioButtonList ID="rblBaseTaxOnArea" runat="server" RepeatDirection="Horizontal"
                                CssClass="signup">
                                <asp:ListItem Text="State" Value="0" Selected="True"></asp:ListItem>
                                <asp:ListItem Text="City" Value="1"></asp:ListItem>
                                <asp:ListItem Text="Zip Code/Postal" Value="2"></asp:ListItem>
                            </asp:RadioButtonList>
                        </td>
                    </tr>
                </table>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell VerticalAlign="Top">
                <asp:GridView ID="gvTaxConfi" runat="server" Width="100%" CssClass="tbl" AllowSorting="true"
                    AutoGenerateColumns="False"  DataKeyNames="numTaxCountryConfi">
                    <AlternatingRowStyle CssClass="ais" />
                    <RowStyle CssClass="is"></RowStyle>
                    <HeaderStyle CssClass="hs"></HeaderStyle>
                    <Columns>
                        <asp:BoundField DataField="vCountryName" HeaderText="Country" ItemStyle-HorizontalAlign="Center" />
                        <%--<asp:BoundField DataField="vcBaseTax" HeaderText="Base tax calculation on" ItemStyle-HorizontalAlign="Center" />--%>
                        <asp:BoundField DataField="vcBaseTaxOnArea" HeaderText="Base tax on" ItemStyle-HorizontalAlign="Center" />
                        <asp:ButtonField CommandName="EditConfi" ButtonType="Link" Text="Edit" />
                    </Columns>
                </asp:GridView>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
</asp:Content>
