﻿Imports BACRM.BusinessLogic.Common
Public Class authenticationpopup
    Inherits BACRMPage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Put user code to initialize the page here
        lblModuleName.Text = GetQueryStringVal("Module")
        lblPermissionName.Text = GetQueryStringVal("Permission")

        If lblModuleName.Text.Trim.Length > 0 And lblPermissionName.Text.Trim.Length > 0 Then
            dvPermission.Visible = True
        End If
    End Sub

End Class