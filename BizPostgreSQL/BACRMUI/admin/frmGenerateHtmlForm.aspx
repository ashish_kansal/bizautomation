﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmGenerateHtmlForm.aspx.vb"
    Inherits=".frmGenerateHtmlForm" ValidateRequest="false" MasterPageFile="~/common/Popup.Master" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>HTML Form Generate</title>
    <script language="javascript" type="text/javascript">
        function Close() {
            window.close();
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <div class="input-part">
        <div class="right-input">
            <table id="Table3" cellpadding="2" cellspacing="2" width="100%">
                <tr>
                    <td align="right">
                        <asp:Button ID="btnClose" runat="server" CssClass="button" Width="50" Text="Close"></asp:Button>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <table>
        <tr>
            <td class="normal4" align="center">
                <asp:Literal ID="litMessage" runat="server"></asp:Literal>
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageTitle" runat="server" ClientIDMode="Static">
    HTML Form Generate
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="Content" runat="server" ClientIDMode="Static">
    <asp:ScriptManager runat="server" id="sc1"/>
    <asp:Table ID="Table2" runat="server" GridLines="None" Width="100%"
        CssClass="aspTable" CellSpacing="2" CellPadding="2">
        <asp:TableRow>
            <asp:TableCell VerticalAlign="Top">
                <table>
                    <tr>
                        <td class="normal1" align="right">URL from Which Lead is submited to Biz<font color="red">*</font>
                        </td>
                        <td class="normal1">
                            <asp:HiddenField ID="hfURLId" runat="server" Value="0" />
                            <asp:TextBox ID="txtURL" runat="server" CssClass="signup" Width="250"></asp:TextBox><asp:RequiredFieldValidator
                                ID="RequiredFieldValidator1" runat="server" ErrorMessage="Lead URL Required!!!" Display="Dynamic"
                                ControlToValidate="txtURL" ValidationGroup="vgURL" SetFocusOnError="true"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td class="normal1" align="right">On Submit redirect user to page:<font color="red">*</font>
                        </td>
                        <td class="normal1">
                            <asp:TextBox ID="txtSuccessURL" runat="server" CssClass="signup" Width="250"></asp:TextBox><asp:RequiredFieldValidator
                                ID="RequiredFieldValidator2" runat="server" ErrorMessage="Submit Redirect URL Required!!!"
                                Display="Dynamic" ControlToValidate="txtSuccessURL" ValidationGroup="vgURL" SetFocusOnError="true"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td class="normal1" align="right">On Submit redirect user to page when error encounters:<font color="red">*</font>
                        </td>
                        <td class="normal1">
                            <asp:TextBox ID="txtFailURL" runat="server" CssClass="signup" Width="250"></asp:TextBox><asp:RequiredFieldValidator
                                ID="RequiredFieldValidator3" runat="server" ErrorMessage="Error Redirect URL Required!!!"
                                Display="Dynamic" ControlToValidate="txtFailURL" ValidationGroup="vgURL" SetFocusOnError="true"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td class="normal1" align="right"></td>
                        <td class="normal1">
                            <asp:Button ID="btnAddEdit" runat="server" CssClass="button" Text="Add/Edit" ValidationGroup="vgURL"></asp:Button>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <asp:GridView ID="gvURL" runat="server" Width="100%" CssClass="dg" AllowSorting="true"
                                AutoGenerateColumns="False" DataKeyNames="numURLId">
                                <AlternatingRowStyle CssClass="ais" />
                                <RowStyle CssClass="is"></RowStyle>
                                <HeaderStyle CssClass="hs"></HeaderStyle>
                                <Columns>
                                    <asp:BoundField DataField="vcURL" HeaderText="Lead is submited from following URL(s)" ItemStyle-HorizontalAlign="Center" />
                                    <asp:BoundField DataField="vcSuccessURL" HeaderText="On submit take customer to" ItemStyle-HorizontalAlign="Center" />
                                    <asp:BoundField DataField="vcFailURL" HeaderText="On erronous submit take customer to" ItemStyle-HorizontalAlign="Center" />
                                    <asp:ButtonField CommandName="EditURL" ButtonType="Link" Text="Edit" />
                                    <asp:ButtonField CommandName="DeleteURL" ButtonType="Link" Text="Delete" />
                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" class="normal1">
                            <b>Copy HTML code from following editor and save it locally and upload it to URL from Which Lead is submited to Biz</b>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <asp:PlaceHolder ID="plhFormControls" runat="server" EnableViewState="True" ClientIDMode="Static"></asp:PlaceHolder>
                            <asp:PlaceHolder ID="plhFormControlsAOI" runat="server" ClientIDMode="Static"></asp:PlaceHolder>
                            <%--<asp:TextBox ID="txtFormHTML" runat="server" TextMode="MultiLine" Rows="20" Columns="80"
                                ReadOnly="true"></asp:TextBox>--%>

                            <telerik:RadEditor ID="RadEditor1" runat="server" Height="515px"
                                ToolsFile="~/Marketing/EditorTools.xml"
                                AllowScripts="true">
                                <Content>
                                </Content>
                                <Tools>
                                </Tools>
                            </telerik:RadEditor>
                        </td>
                    </tr>
                </table>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
</asp:Content>

