''' -----------------------------------------------------------------------------
''' Project	 : BACRM.UserInterface.Admin
''' Class	 : frmDuplicateRecordsSearch
''' 
''' -----------------------------------------------------------------------------
''' <summary>
'''     This screen allows setting the filteration criteria for searching duplicates in Contacts and Organizations
''' </summary>
''' <remarks>
''' </remarks>
''' <history>
''' 	[Debasish]	12/05/2005	Created
''' </history>
''' -----------------------------------------------------------------------------
Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Reports
Imports BACRM.BusinessLogic.Common
Imports System.IO
Imports System.Math
Namespace BACRM.UserInterface.Admin
    Public Class frmDuplicateRecordsSearch
        Inherits BACRMPage

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
            Me.ID = "frmDuplicateRecordsSearch"

        End Sub
        Protected WithEvents tblSearchExtraHeader As System.Web.UI.WebControls.Table
        Protected WithEvents btnDupSearch As System.Web.UI.WebControls.Button
        Protected WithEvents tblCheckSettings As System.Web.UI.WebControls.Table
        Protected WithEvents trDuplicateOrgSettings As System.Web.UI.WebControls.TableRow
        Protected WithEvents trDuplicateContactsSettings As System.Web.UI.WebControls.TableRow
        Protected WithEvents rbSearchFor As System.Web.UI.WebControls.RadioButtonList
        Protected WithEvents cbCheckAll As System.Web.UI.WebControls.CheckBox
        Protected WithEvents cbCreatedOn As System.Web.UI.WebControls.CheckBox
        Protected WithEvents cbSearchInLeads As System.Web.UI.WebControls.CheckBox
        Protected WithEvents cbSearchInProspects As System.Web.UI.WebControls.CheckBox
        Protected WithEvents cbSearchInAccounts As System.Web.UI.WebControls.CheckBox
        'Checkboxes related to the Contact
        Protected WithEvents cbCntNoItemHistory As System.Web.UI.WebControls.CheckBox
        Protected WithEvents cbCntSameParent As System.Web.UI.WebControls.CheckBox
        Protected WithEvents cbCntSameDivision As System.Web.UI.WebControls.CheckBox
        Protected WithEvents cbCntSamePhone As System.Web.UI.WebControls.CheckBox
        Protected WithEvents cbCntIgnoreNames As System.Web.UI.WebControls.CheckBox
        Protected WithEvents cbCntSomeOtherFields As System.Web.UI.WebControls.CheckBox
        'Checkboxes related to the Organization
        Protected WithEvents cbOrgNoAssociation As System.Web.UI.WebControls.CheckBox
        Protected WithEvents cbOrgSameDivision As System.Web.UI.WebControls.CheckBox
        Protected WithEvents cbOrgSameWebsite As System.Web.UI.WebControls.CheckBox
        Protected WithEvents cbOrgSomeOtherFields As System.Web.UI.WebControls.CheckBox

        Protected WithEvents ddlRelationship As System.Web.UI.WebControls.DropDownList
        Protected WithEvents ddlOrgSomeOtherFields As System.Web.UI.WebControls.DropDownList
        Protected WithEvents ddlCntSomeOtherFields As System.Web.UI.WebControls.DropDownList
        Protected WithEvents txtFromDateDisplay As System.Web.UI.WebControls.TextBox
        Protected WithEvents txtToDateDisplay As System.Web.UI.WebControls.TextBox
        Protected WithEvents txtCntFirstName As System.Web.UI.WebControls.TextBox
        Protected WithEvents txtCntLastName As System.Web.UI.WebControls.TextBox
        Protected WithEvents ValidationSummary As System.Web.UI.WebControls.ValidationSummary
        Protected WithEvents custVCRMTypeConditionCheck As System.Web.UI.WebControls.CustomValidator
        Protected WithEvents custVDateRangeConditionCheck As System.Web.UI.WebControls.CustomValidator
        Protected WithEvents dgDuplicateSearchResults As System.Web.UI.WebControls.DataGrid
        Protected WithEvents lblNext As System.Web.UI.WebControls.Label
        Protected WithEvents lnk2 As System.Web.UI.WebControls.LinkButton
        Protected WithEvents lnk3 As System.Web.UI.WebControls.LinkButton
        Protected WithEvents lnk4 As System.Web.UI.WebControls.LinkButton
        Protected WithEvents lnk5 As System.Web.UI.WebControls.LinkButton
        Protected WithEvents lnkFirst As System.Web.UI.WebControls.LinkButton
        Protected WithEvents lnkPrevious As System.Web.UI.WebControls.LinkButton
        Protected WithEvents lblPage As System.Web.UI.WebControls.Label
        Protected WithEvents txtCurrentPage As System.Web.UI.WebControls.TextBox
        Protected WithEvents lblOf As System.Web.UI.WebControls.Label
        Protected WithEvents lblTotal As System.Web.UI.WebControls.Label
        Protected WithEvents lnkNext As System.Web.UI.WebControls.LinkButton
        Protected WithEvents lnkLast As System.Web.UI.WebControls.LinkButton
        Protected WithEvents litClientMessage As System.Web.UI.WebControls.Literal
        Protected WithEvents txtTotalPage As System.Web.UI.WebControls.TextBox
        Protected WithEvents txtColumnSorted As System.Web.UI.WebControls.TextBox
        Protected WithEvents txtSortOrder As System.Web.UI.WebControls.TextBox
        Protected WithEvents tblDuplicateSearchResults As System.Web.UI.HtmlControls.HtmlTable
        Dim objDuplicates As New objDuplicates
        Dim sColumnSorted As String
        Dim sSortOrder As String
        Protected WithEvents txtRecordsOnPage As System.Web.UI.WebControls.TextBox
        Protected WithEvents btnAssociateWithAnotherParent As System.Web.UI.WebControls.Button
        Protected WithEvents btnMergeCopyContacts As System.Web.UI.WebControls.Button
        Protected WithEvents custVldFirstAndLastName As System.Web.UI.WebControls.CustomValidator

        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.
        Private designerPlaceholderDeclaration As System.Object

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This event is fired each time thepage is called. In this event we will 
        '''     get the data from the DB create the form.
        ''' </summary>
        ''' <param name="sender">Represents the sender object.</param>
        ''' <param name="e">Represents the EventArgs.</param>
        ''' <remarks>
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	12/05/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Try
                'Put user code to initialize the page here
                With objDuplicates
                    .DomainID = Session("DomainID")                     'Set the Domain Id
                End With
                If Not IsPostBack Then
                    
                    callToBindRelationships()                           'Call to bind Relationships
                    callForClientEventsLinking()                        'Call to attach the client side events to the controls
                    callToBindOrgAndContactFields()                     'Calls a function which will bind the Contacts and Organization fields
                    tblDuplicateSearchResults.Visible = False           'Hide the Results Table
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This function is called to bind the relationship dro down list
        ''' </summary>
        ''' <remarks>
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	08/13/2005	Created
        ''' </history>
        '''-----------------------------------------------------------------------------
        Function callToBindRelationships()
            Try
                Dim dtRelations As DataTable                                            'declare a datatable
                dtRelations = objDuplicates.getListDetails(5) 'call to get the relationships
                Dim dRow As DataRow                                                     'declare a datarow
                dRow = dtRelations.NewRow()                                             'get a new row object
                dtRelations.Rows.InsertAt(dRow, 0)                                      'insert the row
                dtRelations.Rows(0).Item("numItemID") = 0                               'set the value for first entry
                dtRelations.Rows(0).Item("vcItemName") = "All Relationships"            'set the text for the first entey
                dtRelations.Rows(0).Item("vcItemType") = "L"                            'type= list
                dtRelations.Rows(0).Item("flagConst") = 0                               'requried by the stored procedure
                ddlRelationship.DataSource = dtRelations.DefaultView                    'set the datasource
                ddlRelationship.DataTextField = "vcItemName"                            'set the text attribute
                ddlRelationship.DataValueField = "numItemID"                            'set the value attribute
                ddlRelationship.DataBind()                                              'databind the drop down
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This function is called to bind the other fields drop down list
        ''' </summary>
        ''' <remarks>
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	12/06/2005	Created
        ''' </history>
        '''-----------------------------------------------------------------------------
        Function callToBindOrgAndContactFields()
            Try
                Dim objFields As New CustomReports                                          'Create a object of custom reports (functionality already existing)
                Dim dtReportFields, dtCustomReportFields As DataTable                       'declare a datatable to store the default fields and the custom fields resp.
                dtReportFields = objFields.getColumnsForCustomReport(1)                     'call to get the default fields for 1: Organization and contacts
                dtCustomReportFields = objFields.getCustomColumnsForOrgContactCustomReport(0, 0) 'Custom Fields for Org and Contact
                Dim drCustomReportFieldsCopy As DataRow                                     'Declare a DataRow
                For Each drCustomReportFieldsCopy In dtCustomReportFields.Rows
                    dtReportFields.ImportRow(drCustomReportFieldsCopy)                      'Import the Custom Field list
                Next

                Dim dvReportFieldsGroup1, dvReportFieldsGroup2 As DataView                  'declare a databviews for each of the list box
                dvReportFieldsGroup1 = dtReportFields.DefaultView                           'get the default view
                dvReportFieldsGroup2 = dtReportFields.DefaultView                           'get the default view

                dvReportFieldsGroup1.RowFilter = "numFieldGroupID = 1"                      'set the rowfilter to only Organization Details
                If dvReportFieldsGroup1.Count > 0 Then
                    dvReportFieldsGroup1.Sort = "vcScrFieldName asc"                        'set the sort
                    ddlOrgSomeOtherFields.DataSource = dvReportFieldsGroup1                 'set a datashource 
                    ddlOrgSomeOtherFields.DataTextField = "vcScrFieldName"                  'set the text for the listbox
                    ddlOrgSomeOtherFields.DataValueField = "vcDbFieldName"                  'set the data text format
                    ddlOrgSomeOtherFields.DataBind()                                        'databind to display the data
                End If

                dvReportFieldsGroup2.RowFilter = "numFieldGroupID = 2"                      'set the rowfilter to only Contact Details
                If dvReportFieldsGroup2.Count > 0 Then
                    dvReportFieldsGroup2.Sort = "vcScrFieldName asc"                        'set the sort
                    ddlCntSomeOtherFields.DataSource = dvReportFieldsGroup2                 'set a datashource 
                    ddlCntSomeOtherFields.DataTextField = "vcScrFieldName"                  'set the text for the listbox
                    ddlCntSomeOtherFields.DataValueField = "vcDbFieldName"                  'set the data text format
                    ddlCntSomeOtherFields.DataBind()                                        'databind to display the data
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This function is to attach client side scripts to the controls.
        ''' </summary>
        ''' <remarks>
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	12/06/2005	Created
        ''' </history>
        '''-----------------------------------------------------------------------------
        Function callForClientEventsLinking()
            Try
                cbCreatedOn.Attributes.Add("onclick", "javascript: BlankOutDatesOnUncheck(this);") 'Attach scripts to blank the dates when unchecked
                rbSearchFor.Attributes.Add("onclick", "javascript: DisplaySearchSettings();") 'Attach scripts to toggle the display of rows
                cbCheckAll.Attributes.Add("onclick", "javascript: SearchRecordsIn();") 'Attach scripts to associate Leads/ Prospects/ Accounts and Relationships
                cbSearchInLeads.Attributes.Add("onclick", "javascript: RemoveSearchAllSelection(this);") 'Attach scripts to remove the check on Select All (if unchecked)
                cbSearchInProspects.Attributes.Add("onclick", "javascript: RemoveSearchAllSelection(this);") 'Attach scripts to remove the check on Select All (if unchecked)
                cbSearchInAccounts.Attributes.Add("onclick", "javascript: RemoveSearchAllSelection(this);") 'Attach scripts to remove the check on Select All (if unchecked)
                ddlRelationship.Attributes.Add("onchange", "javascript: RemoveSearchAllSelection(this);") 'Attach scripts to remove the check on Select All (if All is not selected)
                btnAssociateWithAnotherParent.Attributes.Add("onclick", "javascript: return OpenAssociationToWindow();") 'Attach the scripts to initiate association with another parent
                btnMergeCopyContacts.Attributes.Add("onclick", "javascript: return OpenMergeCopyWindow();") 'Attach the scripts to initiate merge/ copy contact to antoher parent
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This event is fired each time as search is called for finding duplicate contacts or divisions
        ''' </summary>
        ''' <param name="sender">Represents the sender object.</param>
        ''' <param name="e">Represents the EventArgs.</param>
        ''' <remarks>
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	12/19/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Private Sub btnDupSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDupSearch.Click
            Try
                If sender.GetType.FullName = "System.Web.UI.WebControls.Button" Then
                    Dim btnSrcButton As Button = CType(sender, Button)                  'typecast to button
                    If btnSrcButton.ID.ToString = "btnDupSearch" Then                   'Check for the source of the event
                        txtColumnSorted.Text = ""                                       'Set the default Sort Column to none
                        txtSortOrder.Text = ""                                          'Set the default Sort Order to none
                        txtCurrentPage.Text = 1                                         'Defaults to the 1st page
                    End If
                End If
                If Trim(txtColumnSorted.Text) = "" Then
                    If rbSearchFor.Items(0).Selected Then                               'If Contacts Search is being performed
                        txtColumnSorted.Text = "First / Last Name" 'Set the default Sort Column for Org Sort
                        txtSortOrder.Text = "Asc"                                       'Set the default Sort Order
                    Else
                        txtColumnSorted.Text = "Organization Name/ Division"          'Set the default Sort Column for Org Sort
                        txtSortOrder.Text = "Asc"                                       'Set the default Sort Order
                    End If
                End If
                PerformSearch()                                                         'Call to perform search
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This event is fired each time as search is called for finding duplicate contacts or divisions
        ''' </summary>
        ''' <param name="sender">Represents the sender object.</param>
        ''' <param name="e">Represents the EventArgs.</param>
        ''' <remarks>
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	12/19/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Sub PerformSearch()
            Try
                If rbSearchFor.Items(0).Selected Then                                       'If Contacts Search is being performed
                    Dim objDuplicatesCnt As New SearchableContFilters                       'Create a new instance of class implementing searching for duplicates in contacts
                    With objDuplicatesCnt
                        .DomainID = Session("DomainID")                                     'Set the Domain ID
                        .SearchWithin = IIf(cbSearchInLeads.Checked, 0, 9) & "," & IIf(cbSearchInProspects.Checked, 1, 9) & "," & IIf(cbSearchInAccounts.Checked, 2, 9) 'Create a list of CRM types (9 is used to denote nothing)
                        .FilterRelationship = ddlRelationship.SelectedValue                 'Get the Relationship
                        .SortDbColumnName = txtColumnSorted.Text                            'Column which is used for Sorting
                        .SortOrder = txtSortOrder.Text                                      'Sort ASC/DESC
                        .PageSize = Session("PagingRows")   'Set the nos of records/ page
                        .CurrentPageIndex = txtCurrentPage.Text                             'Set the Current Page Index
                        If cbCreatedOn.Checked Then
                            If calFrom.SelectedDate <> "" Then
                                .CreationDateStart = calFrom.SelectedDate               'Set the start date range
                            Else : .CreationDateStart = ""                                     'Set the blank date
                            End If
                            If calTo.SelectedDate <> "" Then
                                .CreationDateEnd = calTo.SelectedDate
                            Else : .CreationDateEnd = ""                                       'Set the blank date
                            End If
                        End If
                        .CntNoItemHistoryFlag = cbCntNoItemHistory.Checked                  'Contact has/ does not have an item history
                        .CntSameParentFlag = cbCntSameParent.Checked                        'Contacts in the same organization and multiple records
                        .CntSameDivisionFlag = cbCntSameDivision.Checked                    'Contacts in the same division and same names but multiple records
                        .CntSamePhoneFlag = cbCntSamePhone.Checked                          'Contact having the same phone numbers but again multiple records
                        .CntIgnoreNamesFlag = cbCntIgnoreNames.Checked                      'Ignore contacts with first and last names.... / not to ignore
                        .CntFirstName = IIf(cbCntIgnoreNames.Checked, txtCntFirstName.Text, "") 'If request for ignoring name then get the first name here
                        .CntLastName = IIf(cbCntIgnoreNames.Checked, txtCntLastName.Text, "") 'If request for ignoring name then get the last name here
                        .CntMatchingFields = IIf(cbCntSomeOtherFields.Checked, ddlCntSomeOtherFields.SelectedItem.Value, "") 'Match specific information about the Contact
                    End With
                    dgDuplicateSearchResults.DataSource = GetRowsInPageRange(objDuplicatesCnt.PerformDuplicateCntSearch()) 'Call to initiate search
                    dgDuplicateSearchResults.DataBind()                                     'Bind the DataSource
                    trDuplicateContactsSettings.Attributes.Remove("style")                  'Remove the style for the tr
                    trDuplicateContactsSettings.Attributes.Add("style", "display:inline")   'Display the row
                    trDuplicateOrgSettings.Attributes.Remove("style")                       'Remove the style for the tr
                    trDuplicateOrgSettings.Attributes.Add("style", "display:none")          'Display the row
                    btnMergeCopyContacts.Visible = True                                     'Display "Merge/ Copy Contacts" button
                    btnAssociateWithAnotherParent.Visible = False                           'Hide "Associate Divisions with another Parent" button
                ElseIf rbSearchFor.Items(1).Selected Then                                   'If Organization Search is being performed
                    Dim objDuplicatesOrg As New SearchableOrgFilters                        'Create a new instance of class implementing searching for duplicates in organizations
                    With objDuplicatesOrg
                        .DomainID = Session("DomainID")                                     'Set the Domain ID
                        .SearchWithin = IIf(cbSearchInLeads.Checked, 0, 9) & "," & IIf(cbSearchInProspects.Checked, 1, 9) & "," & IIf(cbSearchInAccounts.Checked, 2, 9) 'Create a list of CRM types (9 is used to denote nothing)
                        .FilterRelationship = ddlRelationship.SelectedValue                 'Get the Relationship
                        .SortDbColumnName = txtColumnSorted.Text                            'Column which is used for Sorting
                        .SortOrder = txtSortOrder.Text                                      'Sort ASC/DESC
                        .PageSize = Session("PagingRows")   'Set the nos of records/ page
                        .CurrentPageIndex = txtCurrentPage.Text                             'Set the Current Page Index
                        If cbCreatedOn.Checked Then
                            If calFrom.SelectedDate <> "" Then
                                .CreationDateStart = calFrom.SelectedDate
                            Else : .CreationDateStart = ""                                     'Set the blank date
                            End If
                            If calTo.SelectedDate <> "" Then
                                .CreationDateEnd = calTo.SelectedDate
                            Else : .CreationDateEnd = ""                                       'Set the blank date
                            End If
                        End If
                        .OrgNoAssociationFlag = cbOrgNoAssociation.Checked                  'Get Organizations which have associations/ non-associations
                        .OrgSameDivisionFlag = cbOrgSameDivision.Checked                    'Get Organizations where Division names are duplicated/ non duplicate
                        .OrgSameWebsite = cbOrgSameWebsite.Checked                          'Get Organizations having the same websites/ whatever 
                        .OrgMatchingFields = IIf(cbOrgSomeOtherFields.Checked, ddlOrgSomeOtherFields.SelectedItem.Value, "") 'Match specific information about the Organization
                    End With
                    dgDuplicateSearchResults.DataSource = GetRowsInPageRange(objDuplicatesOrg.PerformDuplicateOrgSearch())  'Call to initiate Search
                    dgDuplicateSearchResults.DataBind()                                     'Bind the DataSource
                    trDuplicateOrgSettings.Attributes.Remove("style")                       'Remove the style for the tr
                    trDuplicateOrgSettings.Attributes.Add("style", "display:inline")        'Display the row
                    trDuplicateContactsSettings.Attributes.Remove("style")                  'Remove the style for the tr
                    trDuplicateContactsSettings.Attributes.Add("style", "display:none")     'Display the row
                    btnMergeCopyContacts.Visible = False                                    'Hide "Merge/ Copy Contacts" button
                    btnAssociateWithAnotherParent.Visible = True                            'Display "Associate Divisions with another Parent" button
                End If
                tblDuplicateSearchResults.Visible = True                                    'Display the Results Table
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This event is fired for each dataitem binding. This function hides the unwanted columns from the datagrid
        ''' </summary>
        ''' <param name="sender">Represents the sender object.</param>
        ''' <param name="e">Represents the EventArgs.</param>
        ''' <remarks>
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	12/19/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Private Sub dgDuplicateSearchResults_ItemDataBound(ByVal sender As Object, ByVal e As DataGridItemEventArgs) Handles dgDuplicateSearchResults.ItemDataBound
            Try
                If rbSearchFor.Items(0).Selected Then
                    e.Item.Cells(0).Visible = False                                         'Hide the column which contais the RowNum Id
                    e.Item.Cells(1).Visible = False                                         'Hide the column which contais the Company Id
                    e.Item.Cells(2).Visible = False                                         'Hide the column which contais the Division Id
                    e.Item.Cells(3).Visible = False                                         'Hide the column which contais the Contact Id
                ElseIf rbSearchFor.Items(1).Selected Then
                    e.Item.Cells(0).Visible = False                                         'Hide the column which contais the RowNum Id
                    e.Item.Cells(1).Visible = False                                         'Hide the column which contais the Company Id
                    e.Item.Cells(2).Visible = False                                         'Hide the column which contais the Division Id
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This method is used to pick up a selected range of rows
        ''' </summary>
        ''' <remarks> Returns the table with limited rows
        ''' </remarks>
        ''' <param name="dtResultTable">Table which contains the search result to be displayed in the DataGrid</param>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	08/20/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Function GetRowsInPageRange(ByVal dsDuplicateReportResultTable As DataSet) As DataTable
            Try
                txtTotalPage.Text = Math.Ceiling(dsDuplicateReportResultTable.Tables(1).Rows(0).Item("DupCount") / Session("PagingRows")) 'Bind the nos of pages
                lblTotal.Text = Math.Ceiling(dsDuplicateReportResultTable.Tables(1).Rows(0).Item("DupCount") / Session("PagingRows")) 'Display the mos of pages
                Dim sbRecordsOnPage As New System.Text.StringBuilder                                            'Instantiate a new StringBuilder Object

                Dim intDuplicateReportResultsRowIndex As Integer                                                'Declare an index variable
                Dim iCurrentRow As Integer                                                                      'Store the current row for the cloned table
                For intDuplicateReportResultsRowIndex = 0 To dsDuplicateReportResultTable.Tables(0).Rows.Count - 1       'Loop through the row index from the table
                    If rbSearchFor.Items(0).Selected Then
                        dsDuplicateReportResultTable.Tables(0).Rows(iCurrentRow).Item(5) = "<a href='javascript: GoContactDetails(" & dsDuplicateReportResultTable.Tables(0).Rows(iCurrentRow).Item("numContactId") & ")'>" & dsDuplicateReportResultTable.Tables(0).Rows(iCurrentRow).Item(5) & "</a>" 'Hyperlink Contacts
                        dsDuplicateReportResultTable.Tables(0).Rows(iCurrentRow).Item(12) = "<a href='javascript: GoOrgDetails(" & dsDuplicateReportResultTable.Tables(0).Rows(iCurrentRow).Item("numDivisionId") & ")'>" & dsDuplicateReportResultTable.Tables(0).Rows(iCurrentRow).Item(12) & "</a>" 'Hyperlink Org Details
                        sbRecordsOnPage.Append(dsDuplicateReportResultTable.Tables(0).Rows(iCurrentRow).Item("numContactId") & ",") 'Create a string of Contact Ids
                    ElseIf rbSearchFor.Items(1).Selected Then
                        dsDuplicateReportResultTable.Tables(0).Rows(iCurrentRow).Item(4) = "<a href='javascript: GoOrgDetails(" & dsDuplicateReportResultTable.Tables(0).Rows(iCurrentRow).Item("numDivisionId") & ")'>" & dsDuplicateReportResultTable.Tables(0).Rows(iCurrentRow).Item(4) & "</a>" 'Hyperlink Org Details
                        sbRecordsOnPage.Append(dsDuplicateReportResultTable.Tables(0).Rows(iCurrentRow).Item("numDivisionId") & ",") 'Create a string of Division Ids
                    End If
                    iCurrentRow += 1                                                                        'Increment the row index
                Next
                If sbRecordsOnPage.ToString().Length > 0 Then
                    txtRecordsOnPage.Text = sbRecordsOnPage.ToString().Remove(sbRecordsOnPage.ToString().Length - 1, 1) 'Store in the Text Box the comma separated list of ids
                Else : txtRecordsOnPage.Text = ""                                                               'Store in the Text Box that there are no records
                End If
                If dsDuplicateReportResultTable.Tables(0).Rows.Count = 0 Then
                    btnAssociateWithAnotherParent.Enabled = False
                    btnMergeCopyContacts.Enabled = False
                Else
                    btnAssociateWithAnotherParent.Enabled = True
                    btnMergeCopyContacts.Enabled = True
                End If
                Return dsDuplicateReportResultTable.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This event is fired each time the DataGrid is sorted.
        ''' </summary>
        ''' <param name="sender">Represents the sender object.</param>
        ''' <param name="e">Represents the EventArgs.</param>
        ''' <remarks>
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	12/28/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Private Sub dgDuplicateSearchResults_SortCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles dgDuplicateSearchResults.SortCommand
            Try
                sColumnSorted = e.SortExpression.ToString()                 'Retrieve the column sorting
                If txtColumnSorted.Text <> sColumnSorted Then
                    txtColumnSorted.Text = sColumnSorted
                    txtSortOrder.Text = "Desc"                              'Default sort is Descending
                Else
                    If txtSortOrder.Text = "Asc" Then                       'Toggle sort order
                        txtSortOrder.Text = "Desc"
                    Else : txtSortOrder.Text = "Asc"
                    End If
                End If
                PerformSearch()                                             'Call to perform search
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This method is used to fill the search results for Duplicate Search Screen when the page index changes
        ''' </summary>
        ''' <remarks> When the page number is specifically entered in the textbox, it executes to display the records for the page
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	12/28/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Private Sub txtCurrentPage_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtCurrentPage.TextChanged
            Try
                If Not IsNumeric(txtCurrentPage.Text) Then
                    litClientMessage.Text = "<script language=javascript>alert('Invalid page number entered, please re-enter.');</script>" 'Alert to user about invalid page number
                Else : PerformSearch()                                                         'Call to perform search
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This method executed when the last page icon is clicked on the DataGrid Page
        ''' </summary>
        ''' <remarks> When the page number is specifically changed by clicking on the last icon, 
        '''     this executes to display the records for the page
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	12/28/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Private Sub lnkLast_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkLast.Click
            Try
                txtCurrentPage.Text = txtTotalPage.Text                                         'Set the Page Index
                PerformSearch()                                                         'Call to perform search
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This method executed when the previous page icon is clicked on the DataGrid Page
        ''' </summary>
        ''' <remarks> When the page number is specifically changed by clicking on the last icon, 
        '''     this executes to display the records for the page
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	12/28/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Private Sub lnkPrevious_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkPrevious.Click
            Try
                If Convert.ToInt64(txtCurrentPage.Text) > 1 Then                                'If the first page is already displayed then exit
                    txtCurrentPage.Text = txtCurrentPage.Text - 1                               'decrement the page number
                    PerformSearch()                                                         'Call to perform search
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This method executed when the first page icon is clicked on the DataGrid Page
        ''' </summary>
        ''' <remarks> When the page number is specifically changed by clicking on the last icon, 
        '''     this executes to display the records for the page
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	12/28/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Private Sub lnkFirst_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkFirst.Click
            Try
                txtCurrentPage.Text = 1                                                         'Set the first page index to 1
                PerformSearch()                                                         'Call to perform search
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This method executed when the next page icon is clicked on the DataGrid Page
        ''' </summary>
        ''' <remarks> When the page number is specifically changed by clicking on the last icon, 
        '''     this executes to display the records for the page
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	12/28/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Private Sub lnkNext_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkNext.Click
            Try
                If Convert.ToInt64(txtCurrentPage.Text) < Convert.ToInt64(txtTotalPage.Text) Then 'check if this is not the last page
                    txtCurrentPage.Text = txtCurrentPage.Text + 1                               'increment the page number
                    PerformSearch()                                                         'Call to perform search
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This method executed when the Next: 2 page icon is clicked on the DataGrid Page
        ''' </summary>
        ''' <remarks> When the page number is specifically changed by clicking on the last icon, 
        '''     this executes to display the records for the page
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	12/28/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Private Sub lnk2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnk2.Click
            Try
                If Convert.ToInt64(txtCurrentPage.Text) + 2 <= Convert.ToInt64(txtTotalPage.Text) Then 'check if incrementing the page counter exhausts the page
                    txtCurrentPage.Text = txtCurrentPage.Text + 2                               'increment the page number by 2
                    PerformSearch()                                                         'Call to perform search
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This method executed when the Next: 3 page icon is clicked on the DataGrid Page
        ''' </summary>
        ''' <remarks> When the page number is specifically changed by clicking on the last icon, 
        '''     this executes to display the records for the page
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	12/28/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Private Sub lnk3_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnk3.Click
            Try
                If Convert.ToInt64(txtCurrentPage.Text) + 3 <= Convert.ToInt64(txtTotalPage.Text) Then 'check if incrementing the page counter exhausts the page
                    txtCurrentPage.Text = txtCurrentPage.Text + 3                               'increment the page number by 3
                    PerformSearch()                                                         'Call to perform search
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This method executed when the Next: 4 page icon is clicked on the DataGrid Page
        ''' </summary>
        ''' <remarks> When the page number is specifically changed by clicking on the last icon, 
        '''     this executes to display the records for the page
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	12/28/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Private Sub lnk4_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnk4.Click
            Try
                If Convert.ToInt64(txtCurrentPage.Text) + 4 <= Convert.ToInt64(txtTotalPage.Text) Then  'check if incrementing the page counter exhausts the page
                    txtCurrentPage.Text = txtCurrentPage.Text + 4                               'increment the page number by 4
                    PerformSearch()                                                         'Call to perform search
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This method executed when the Next: 5 page icon is clicked on the DataGrid Page
        ''' </summary>
        ''' <remarks> When the page number is specifically changed by clicking on the last icon, 
        '''     this executes to display the records for the page
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	12/28/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Private Sub lnk5_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnk5.Click
            Try
                If Convert.ToInt64(txtCurrentPage.Text) + 5 <= Convert.ToInt64(txtTotalPage.Text) Then  'check if incrementing the page counter exhausts the page
                    txtCurrentPage.Text = txtCurrentPage.Text + 5                               'increment the page number by 5
                    PerformSearch()                                                         'Call to perform search
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

    End Class
End Namespace
