'' Modified By anoop jayaraj
Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Common

Namespace BACRM.UserInterface.Admin
    Public Class Cflwizardsum
        Inherits BACRMPage
        Protected WithEvents btnBack As System.Web.UI.WebControls.Button
        Protected WithEvents btnCancel As System.Web.UI.WebControls.Button
        Protected WithEvents btnFinish As System.Web.UI.WebControls.Button
        Protected WithEvents lblgrp As System.Web.UI.WebControls.Label
        Protected WithEvents ddlTab As System.Web.UI.WebControls.DropDownList
        Protected WithEvents lblfldtyp As System.Web.UI.WebControls.Label
        Protected WithEvents txtFldLabel As System.Web.UI.WebControls.TextBox
        Dim PageId As String
#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Try
                litMessage.Text = ""
                If Not IsPostBack Then
                    
                    If GetQueryStringVal( "fldid") <> "" Then
                        btnBack.Visible = False
                        Dim dtFldDetails As DataTable
                        Dim objCusFld As New CustomFields
                        objCusFld.CusFldId = CCommon.ToLong(GetQueryStringVal("fldid"))
                        dtFldDetails = objCusFld.GetFieldDetails
                        Session("PageId") = dtFldDetails.Rows(0).Item("grp_id")
                        Session("TabId") = dtFldDetails.Rows(0).Item("subgrp")
                        Session("fldtype") = dtFldDetails.Rows(0).Item("fld_type")
                        Session("fldlbl") = dtFldDetails.Rows(0).Item("fld_label")
                        txtFldLabel.Text = dtFldDetails.Rows(0).Item("fld_label")
                        txtToolTip.Text = dtFldDetails.Rows(0).Item("vcToolTip")
                        chkAutoComplete.Checked = dtFldDetails.Rows(0).Item("bitAutoComplete")
                        txtURL.Text = IIf(IsDBNull(dtFldDetails.Rows(0).Item("vcURL")), "", dtFldDetails.Rows(0).Item("vcURL"))
                    End If
                    PageId = Session("PageId")
                    fillTab()
                    If Not ddlTab.Items.FindByValue(Session("TabId")) Is Nothing Then
                        ddlTab.Items.FindByValue(Session("TabId")).Selected = True
                    End If
                    lblfldtyp.Text = Session("fldtype")
                    If lblfldtyp.Text = "SelectBox" Then
                        chkAutoComplete.Visible = True
                    Else
                        chkAutoComplete.Visible = False
                    End If
                    If Session("fldtype") = "Link" Then
                        trURL.Visible = True
                    End If
                    If PageId = 1 Then
                        lblgrp.Text = "Leads/Prospects/Accounts"
                    ElseIf PageId = 4 Then
                        lblgrp.Text = "Contact Details"
                    ElseIf PageId = 2 Then
                        lblgrp.Text = "Sales Opportunity Details"
                    ElseIf PageId = 3 Then
                        lblgrp.Text = "Case Details"
                    ElseIf PageId = 5 Then
                        lblgrp.Text = "Item Details"
                    ElseIf PageId = 6 Then
                        lblgrp.Text = "Purchase Opportunity Details"
                    ElseIf PageId = 7 Then
                        lblgrp.Text = "Product & Services (Sales)"
                    ElseIf PageId = 8 Then
                        lblgrp.Text = "Product & Services (Purchase)"
                    ElseIf PageId = 9 Then
                        lblgrp.Text = "Item Attributes"
                    ElseIf PageId = 11 Then
                        lblgrp.Text = "Projects"
                    ElseIf PageId = 12 Then
                        lblgrp.Text = "Prospects"
                    ElseIf PageId = 13 Then
                        lblgrp.Text = "Accounts"
                    ElseIf PageId = 14 Then
                        lblgrp.Text = "Leads"
                    End If
                End If
                btnFinish.Attributes.Add("onclick", "return Finish()")
                btnBack.Attributes.Add("onclick", "return Back()")
                btnCancel.Attributes.Add("onclick", "return Close()")
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Sub fillTab()
            Try
                'If PageId <> 1 Then 'When its Lead/Prospects/Accounts allow only custom field to be added to main detail section
                Dim dtTab As DataTable
                Dim objCusField As New CustomFields()
                objCusField.locId = PageId
                objCusField.DomainID = Session("DomainID")
                dtTab = objCusField.Tabs
                ddlTab.DataSource = dtTab
                ddlTab.DataTextField = "grp_name"
                ddlTab.DataValueField = "grp_id"
                ddlTab.DataBind()
                'End If

                ddlTab.Items.Insert(0, "--Select One--")
                ddlTab.Items.FindByText("--Select One--").Value = -1
                If Not (PageId = 12 Or PageId = 13 Or PageId = 14) Then 'When its seperate Location like accounts then allow custom fields to be added only to custom subtabs
                    ddlTab.Items.Insert(1, "Add to Main Detail Section")
                    ddlTab.Items.FindByText("Add to Main Detail Section").Value = 0
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub btnFinish_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnFinish.Click
            Try
                If txtFldLabel.Text.Contains(":") Or txtFldLabel.Text.Contains("(") Or txtFldLabel.Text.Contains(")") Or txtFldLabel.Text.Contains("]") Or txtFldLabel.Text.Contains("[") Or txtFldLabel.Text.Contains("*") Or txtFldLabel.Text.Contains("~") Or txtFldLabel.Text.Contains(".") Or txtFldLabel.Text.Contains("'") Then
                    litMessage.Text = "Special characters e.g. '.', ':', ')', '(', ']', '[', '*', '~', &quot; ' &quot; are not allowed."
                    Exit Sub
                End If
                Dim objCusField As New CustomFields()
                objCusField.locId = Session("PageId")
                objCusField.TabId = ddlTab.SelectedItem.Value
                objCusField.UserId = Session("UserId")
                objCusField.FieldLabel = txtFldLabel.Text
                objCusField.FieldType = Session("fldtype")
                objCusField.CusFldId = CCommon.ToLong(GetQueryStringVal("fldid"))
                objCusField.DomainID = Session("DomainID")
                objCusField.URL = txtURL.Text
                objCusField.AutoComplete = chkAutoComplete.Checked
                objCusField.vcToolTip = txtToolTip.Text.Trim

                objCusField.CflSave()
                Session("PageId") = Nothing
                Session("TabId") = Nothing
                Session("fldtype") = Nothing
                Session("fldlbl") = Nothing
                Response.Write("<script language=javascript>")
                Response.Write("window.opener.document.location.reload();")
                Response.Write("window.close();")
                Response.Write("</script>")
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

    End Class
End Namespace