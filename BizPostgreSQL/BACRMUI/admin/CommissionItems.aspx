﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="CommissionItems.aspx.vb"
    Inherits=".CommissionItems" MasterPageFile="~/common/Popup.Master" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <script>
        function DeleteRecord() {
            if (confirm('Are you sure, you want to delete the selected record?')) {
                return true;
            }
            else {
                return false;
            }
        }

        $(document).ready(function () {
            $(window).on('beforeunload', function () {
                UpdateParent();
            });
            $(document).on("submit", "form", function (event) {
                $(window).off('beforeunload');
            });
        });

        function UpdateParent() {
            var count = $("#hdnRowCount").val() || 0;

            if ($("#hdnSelectedOption").val() == "1") {
                window.opener.document.getElementById("lblIndividualItem").innerText = "(" + count + ")";
                window.opener.document.getElementById("lblClassification").innerText = "(0)";
                window.opener.document.getElementById("hdnSelectedItemOrClassifiction").value = count
            } else if ($("#hdnSelectedOption").val() == "2") {
                window.opener.document.getElementById("lblClassification").innerText = "(" + count + ")";
                window.opener.document.getElementById("lblIndividualItem").innerText = "(0)";
                window.opener.document.getElementById("hdnSelectedItemOrClassifiction").value = count
            } else if ($("#hdnSelectedOption").val() == "3") {
                window.opener.document.getElementById("lblIndividualOrg").innerText = "(" + count + ")";
                window.opener.document.getElementById("lblRelProfile").innerText = "(0)";
                window.opener.document.getElementById("hdnSelectedOrgOrProfile").value = count
            } else if ($("#hdnSelectedOption").val() == "4") {
                window.opener.document.getElementById("lblRelProfile").innerText = "(" + count + ")";
                window.opener.document.getElementById("lblIndividualOrg").innerText = "(0)";
                window.opener.document.getElementById("hdnSelectedOrgOrProfile").value = count
            }

            window.close();
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <div class="input-part">
        <div class="right-input">
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageTitle" runat="server" ClientIDMode="Static">
    <asp:Label ID="lblTitle" runat="server"></asp:Label>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="Content" runat="server" ClientIDMode="Static">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <table align="center" width="800px">
        <tr>
            <td align="center" class="normal4">
                <asp:Literal ID="litMessage" runat="server" EnableViewState="False"></asp:Literal>
            </td>
        </tr>
    </table>
    <asp:HiddenField ID="hdnRowCount" runat="server" />
    <asp:HiddenField ID="hdnSelectedOption" runat="server" />
    <asp:Table ID="Table3" Width="800px" runat="server" BorderWidth="1" Height="350"
        GridLines="None" CssClass="aspTable" BorderColor="black" CellSpacing="0" CellPadding="0">
        <asp:TableRow>
            <asp:TableCell VerticalAlign="Top">
                <asp:Panel runat="server" ID="pnlItems" Visible="false">
                    <table width="100%">
                        <tr>
                            <td class="normal1" nowrap>Select Item
                                <telerik:RadComboBox ID="radItem" runat="server" Width="150" DropDownWidth="200px"
                                    EnableLoadOnDemand="true">
                                    <WebServiceSettings Path="../common/Common.asmx" Method="GetItems" />
                                </telerik:RadComboBox>
                                &nbsp;
                            </td>
                            <td align="right">
                                <asp:UpdatePanel ID="updateprogress1" runat="server">
                                    <ContentTemplate>
                                        <asp:LinkButton ID="btnAddItem" Style="text-decoration: none;" CssClass="btn btn-primary" runat="server">Add</asp:LinkButton>
                                        <asp:LinkButton ID="btnExportItem" Style="text-decoration: none;" CssClass="btn btn-primary" runat="server">Export to Excel</asp:LinkButton>
                                        <asp:LinkButton ID="btnRemItem" Style="text-decoration: none;" CssClass="btn btn-primary" runat="server" OnClientClick="return DeleteRecord();">Remove selected</asp:LinkButton>
                                        <asp:LinkButton ID="btnClose" Style="text-decoration: none;" CssClass="btn btn-primary" runat="server" OnClientClick="window.close();">Close</asp:LinkButton>
                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:PostBackTrigger ControlID="btnAddItem" />
                                        <asp:PostBackTrigger ControlID="btnExportItem" />
                                        <asp:PostBackTrigger ControlID="btnRemItem" />
                                        <asp:PostBackTrigger ControlID="btnClose" />
                                    </Triggers>
                                </asp:UpdatePanel>
                            </td>
                        </tr>
                        <tr>
                            <td class="normal1" colspan="2">
                                <asp:DataGrid ID="dgItems" AllowSorting="True" runat="server" Width="100%" CssClass="dg"
                                    AutoGenerateColumns="False">
                                    <AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
                                    <ItemStyle CssClass="is"></ItemStyle>
                                    <HeaderStyle CssClass="hs" Wrap="false"></HeaderStyle>
                                    <Columns>
                                        <asp:BoundColumn Visible="False" DataField="numComRuleItemID"></asp:BoundColumn>
                                        <asp:BoundColumn Visible="False" DataField="numComRuleID"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="vcItemName" HeaderText="Item Name"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="vcModelID" HeaderText="Model ID"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="txtItemDesc" HeaderText="Description"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="ItemClassification" HeaderText="Item Classification"></asp:BoundColumn>
                                        <asp:TemplateColumn>
                                            <HeaderTemplate>
                                                <asp:CheckBox ID="chkSelectAll" runat="server" onclick="SelectAll('chkSelectAll','chkItems')" />
                                            </HeaderTemplate>
                                            <HeaderStyle HorizontalAlign="Left" />
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chk" runat="server" CssClass="chkItems" />
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                    </Columns>
                                </asp:DataGrid>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <asp:Panel runat="server" ID="pnlItemClassification" Visible="false">
                    <table width="800px">
                        <tr>
                            <td class="normal1" nowrap>Select Item Classification
                                <asp:DropDownList ID="ddlItemClassification" runat="server" CssClass="signup">
                                </asp:DropDownList>
                                &nbsp;
                                <asp:Button ID="btnAddItemClass" CssClass="btn btn-primary" runat="server" Text="Add"></asp:Button>
                            </td>
                            <td align="right">
                                <asp:Button ID="btnExportItemClassification" runat="server" CssClass="btn btn-primary" Text="Export to Excel" />&nbsp;
                                <asp:Button ID="btnRemoveItemClass" CssClass="btn btn-primary" runat="server" Text="Remove selected" OnClientClick="return DeleteRecord();"></asp:Button>&nbsp;
                                <asp:Button ID="btnCloseIC" CssClass="btn btn-primary" runat="server" Text="Close" OnClientClick="window.close();"></asp:Button>
                            </td>
                        </tr>
                        <tr>
                            <td class="normal1" colspan="2">
                                <asp:DataGrid ID="dgItemClass" AllowSorting="True" runat="server" Width="100%" CssClass="dg"
                                    AutoGenerateColumns="False">
                                    <AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
                                    <ItemStyle CssClass="is"></ItemStyle>
                                    <HeaderStyle CssClass="hs" Wrap="false"></HeaderStyle>
                                    <Columns>
                                        <asp:BoundColumn Visible="False" DataField="numComRuleItemID"></asp:BoundColumn>
                                        <asp:BoundColumn Visible="False" DataField="numComRuleID"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="ItemClassification" HeaderText="Item Classification"></asp:BoundColumn>
                                        <asp:BoundColumn DataFormatString="{0:##,#00}" DataField="ItemsCount" HeaderText="Items belonging to this classification"></asp:BoundColumn>
                                        <asp:TemplateColumn>
                                            <HeaderTemplate>
                                                <asp:CheckBox ID="chkSelectAll" runat="server" onclick="SelectAll('chkSelectAll','chkItemClass')" />
                                            </HeaderTemplate>
                                            <HeaderStyle HorizontalAlign="Left" />
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chk" runat="server" CssClass="chkItemClass" />
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                    </Columns>
                                </asp:DataGrid>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <asp:Panel runat="server" ID="pnlOrganizations" Visible="false">
                    <table width="100%">
                        <tr>
                            <td class="normal1" nowrap>Select Organization
                                <telerik:RadComboBox AccessKey="C" ID="radCmbCompany" Width="195px" DropDownWidth="600px"
                                    OnClientItemsRequested="OnClientItemsRequestedOrganization"
                                    ClientIDMode="Static"
                                    ShowMoreResultsBox="true"
                                    Skin="Default" runat="server" AllowCustomText="True" EnableLoadOnDemand="True">
                                    <WebServiceSettings Path="../common/Common.asmx" Method="GetCompanies" />
                                </telerik:RadComboBox>
                                &nbsp;
                                <asp:Button ID="btnAddOrg" CssClass="btn btn-primary" runat="server" Text="Add"></asp:Button>
                            </td>
                            <td align="right">
                                <asp:Button ID="btnExportOrg" runat="server" CssClass="btn btn-primary" Text="Export to Excel" />&nbsp;
                                <asp:Button ID="btnRemoveOrg" CssClass="btn btn-primary" runat="server" Text="Remove from commission rule"
                                    OnClientClick="return DeleteRecord();"></asp:Button>&nbsp;
                                <asp:Button ID="btnCloseOrg" CssClass="btn btn-primary" runat="server" Text="Close" OnClientClick="window.close();"></asp:Button>
                            </td>
                        </tr>
                        <tr>
                            <td class="normal1" colspan="2">
                                <asp:DataGrid ID="dgOrg" AllowSorting="True" runat="server" Width="100%" CssClass="dg"
                                    AutoGenerateColumns="False">
                                    <AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
                                    <ItemStyle CssClass="is"></ItemStyle>
                                    <HeaderStyle CssClass="hs" Wrap="false"></HeaderStyle>
                                    <Columns>
                                        <asp:BoundColumn Visible="False" DataField="numComRuleOrgID"></asp:BoundColumn>
                                        <asp:BoundColumn Visible="False" DataField="numComRuleID"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="vcCompanyName" HeaderText="Organization Name"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="PrimaryContact" HeaderText="Primary Contact (First / Last Name & Email)"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="Relationship" HeaderText="Relationship, Profile"></asp:BoundColumn>
                                        <asp:TemplateColumn>
                                            <HeaderTemplate>
                                                <asp:CheckBox ID="chkOrgSelectAll" runat="server" onclick="SelectAll('chkOrgSelectAll','dgOrgchk')" />
                                            </HeaderTemplate>
                                            <HeaderStyle HorizontalAlign="Left" />
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chk" runat="server" CssClass="dgOrgchk" />
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                    </Columns>
                                </asp:DataGrid>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <asp:Panel runat="server" ID="pnlRelProfiles" Visible="false">
                    <table width="1000">
                        <tr>
                            <td class="normal1" nowrap>Select Relationship & Profile
                                <asp:DropDownList ID="ddlRelationship" runat="server" Width="150" CssClass="signup">
                                </asp:DropDownList>
                                &nbsp;
                                <asp:DropDownList ID="ddlProfile" runat="server" Width="150" CssClass="signup">
                                </asp:DropDownList>
                                &nbsp;
                                <asp:Button ID="btnAddRelProfile" CssClass="btn btn-primary" runat="server" Text="Add"></asp:Button>
                            </td>
                            <td align="right">
                                <asp:Button ID="btnExportRelProfile" runat="server" CssClass="btn btn-primary" Text="Export to Excel" />&nbsp;
                                 <asp:Button ID="btnRemoveRelProfile" CssClass="btn btn-primary" runat="server" Text="Remove from commission rule"
                                    OnClientClick="return DeleteRecord();"></asp:Button>&nbsp;
                                <asp:Button ID="btnCloseRel" CssClass="btn btn-primary" runat="server" Text="Close" OnClientClick="window.close();"></asp:Button>
                            </td>
                        </tr>
                        <tr>
                            <td class="normal1" colspan="2">
                                <asp:DataGrid ID="dgRelationship" AllowSorting="True" runat="server" Width="100%"
                                    CssClass="dg" AutoGenerateColumns="False">
                                    <AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
                                    <ItemStyle CssClass="is"></ItemStyle>
                                    <HeaderStyle CssClass="hs" Wrap="false"></HeaderStyle>
                                    <Columns>
                                        <asp:BoundColumn Visible="False" DataField="numComRuleOrgID"></asp:BoundColumn>
                                        <asp:BoundColumn Visible="False" DataField="numComRuleID"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="RelProfile" HeaderText="Relationship, Profile"></asp:BoundColumn>
                                        <asp:TemplateColumn>
                                            <HeaderTemplate>
                                                <asp:CheckBox ID="chkRelationshipSelectAll" runat="server" onclick="SelectAll('chkRelationshipSelectAll','dgRelationshipchk')" />
                                            </HeaderTemplate>
                                            <HeaderStyle HorizontalAlign="Left" />
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chk" runat="server" CssClass="dgRelationshipchk" />
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                    </Columns>
                                </asp:DataGrid>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
</asp:Content>
