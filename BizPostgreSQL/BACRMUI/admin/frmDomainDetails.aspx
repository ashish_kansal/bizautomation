<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmDomainDetails.aspx.vb"
    Inherits="BACRM.UserInterface.Admin.frmDomainDetails" ValidateRequest="false"
    MasterPageFile="~/common/DetailPage.Master" WarningLevel="0" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Global Settings</title>
    <script type="text/javascript" src="../javascript/comboClientSide.js"></script>
    <script src="../Scripts/tooltip/jquery.tooltip.js"></script>
    <link href="../Scripts/tooltip/tooltip.css" rel="stylesheet" />
    <link href="../Scripts/tooltip/tooltip-white.css" rel="stylesheet" />
    <style>
        .tip {
            background: #ffc !important;
            padding: 1px 7px !important;
            border: 1px solid #8a8181 !important;
            color: #000 !important;
            font-weight: 600 !important;
            z-index: 10000;
        }

        .innertooltipTitle .headingsmallfont1 {
            /* text-decoration: underline; */
            text-decoration: underline;
            font-weight: bold;
        }

        .innertooltipTitle .subheading {
            text-decoration: underline;
        }
    </style>
    <script type="text/javascript">

        $(document).ready(function () {

            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_pageLoaded(pageLoaded);
        });

        function pageLoaded() {
            InitializeValidation();
            $('.tip').tooltip({
                position: 'l', forcePosition: true,

            });
            $('#litMessage').fadeIn().delay(5000).fadeOut();

            $("#chkRemoveGlobalWarehouse").on("change", function () {
                if ($(this).is(":checked")) {
                    alert("Selecting this option and clicking save will remove Global Warehouse from your item inventory which are not used in any order and have no inventory levels.");
                }
            });

            $("#chkUsePredefinedCustomer").on("change", function () {
                if ($(this).is(":checked")) {
                    $("input.btn-add-mcm").show();
                    $("input.btn-remove-mcm").show();
                } else {
                    $("input.btn-add-mcm").hide();
                    $("input.btn-remove-mcm").hide();
                }
            });
        }
        function New() {
            //if (document.getElementById('ddlSites').selectedIndex == 0) {
            //    //alert("Please select a site to create New Shipping Rule.");
            //    if (confirm("This will create generic shipping rules without using sites, are you sure to want to create generic shipping rules ?")) {
            //        window.location.href = "frmShippingRule.aspx";
            //        return true;
            //    }
            //    else {
            //        return false;
            //    }
            //}
            //else {
            window.location.href = "../Ecommerce/frmShippingRule.aspx";
            //}
        }
        function DeleteMessage() {
            alert("You Are not Authorized to Delete the Selected Record !");
            return false;
        }
        function DeleteRecord() {
            if (confirm('Are you sure, you want to delete the selected record?')) {
                return true;
            }
            else {
                return false;
            }
        }

        function OpenClassificationsWindow() {

            window.open("../ECommerce/frmShippingClassifications.aspx", '', 'toolbar=no,titlebar=no,top=200,width=900,height=600,left=200,scrollbars=yes,resizable=yes')
            return false;
        }
        function OpenComDTL(a, b) {
            window.open('../Items/frmShippingCMPDTLs.aspx?ListItemID=' + a + '&ListItem=' + b, '', 'toolbar=no,titlebar=no,left=300, top=100,width=600,height=500,scrollbars=yes,resizable=yes');
            return false;
        }
        function OpenAssigntoContact() {
            window.open("../admin/frmAssigntoContact.aspx", '', 'toolbar=no,titlebar=no,left=400, top=200,width=200,height=200,scrollbars=yes,resizable=yes')
            return false;
        }

        function OpenClass() {
            window.open("../admin/frmClassCustomization.aspx", '', 'toolbar=no,titlebar=no,left=400, top=200,width=200,height=200,scrollbars=yes,resizable=yes')
            return false;
        }
        function DefaultTabNames() {
            window.open("../admin/frmTabName.aspx", '', 'toolbar=no,titlebar=no,left=400, top=200,width=700,height=200,scrollbars=yes,resizable=yes')
            return false;
        }
        function NewTab() {
            window.open("../admin/frmNewTabs.aspx", '', 'toolbar=no,titlebar=no,left=200, top=200,width=600,height=500,scrollbars=yes,resizable=yes')
            return false;
        }
        function ManageSubtab() {
            var h = screen.height;
            var w = screen.width;

            window.open('../admin/frmCustomFieldTabs.aspx', 'tabs', 'width=' + (w - 100) + ',height=' + (h - 200) + ',status=no,titlebar=no,scrollbars=yes,resizable=yes,top=50,left=50');
            return false;
        }
        function OpenBillPaymentBizDoc() {
            window.open("../admin/frmBillPaymentBizDoc.aspx", '', 'toolbar=no,titlebar=no,left=200, top=100,width=1000,height=400,scrollbars=yes,resizable=yes')
            return false;
        }
        function ChangeAccountMapping() {
            window.open('../admin/frmChangeDefaultAccount.aspx', 'TreeNodes', 'width=600,height=500,status=no,titlebar=no,scrollbars=yes,resizable=yes,top=110,left=150');
            return false;
        }
        function ManageTreeNodes() {
            var h = screen.height;
            var w = screen.width;

            window.open('../admin/frmTreeNavigationAuthorization.aspx', 'TreeNodes', 'width=1200,height=' + (h - 200) + ',status=no,titlebar=no,scrollbars=yes,resizable=yes,top=110,left=150');
            return false;
        }
        function OpenGoogleAnalyticsDetails() {
            window.open('../admin/frmGoogleAnalyticsConfiguration.aspx', '', 'toolbar=no,titlebar=no,top=200,left=200,width=700,height=550,scrollbars=yes,resizable=yes')
            return false;
        }
        function OpenMinimumUnitPrice() {
            window.open('../admin/frmMinimumUnitPrice.aspx', '', 'toolbar=no,titlebar=no,top=200,left=200,width=700,height=350,scrollbars=yes,resizable=yes')
            return false;
        }

        function OpenAutoCreatePoConfig() {
            window.open('../admin/frmAutoCreatePOConfig.aspx', '', 'toolbar=no,titlebar=no,top=200,left=200,width=700,height=350,scrollbars=yes,resizable=yes')
            return false;
        }
        function ShortCut() {
            window.open("../admin/frmShortCutMngGrp.aspx", '', 'toolbar=no,titlebar=no,left=150, top=150,width=800,height=500,scrollbars=yes,resizable=yes')
            return false;
        }
        function SelectTabs(a) {
            window.open("../admin/frmSelectTabs.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&GroupID=" + a, '', 'toolbar=no,titlebar=no,left=100, top=50,width=1500,height=700,scrollbars=yes,resizable=yes')
            return false;
        }
        function PortalBizDocs() {
            window.open("../admin/frmPortalBizDocs.aspx", '', 'toolbar=no,titlebar=no,left=200, top=200,width=650,height=300,scrollbars=yes,resizable=yes')
            return false;
        }
        function Shipping() {
            window.open("../admin/frmShippingMethodMapping.aspx?1=1", '', 'toolbar=no,titlebar=no,left=200, top=200,width=400,height=150,scrollbars=yes,resizable=yes')
            return false;
        }
        function CreditCard() {
            window.open("../admin/frmCreditCardMapping.aspx", '', 'toolbar=no,titlebar=no,left=200, top=200,width=600,height=150,scrollbars=yes,resizable=yes')
            return false;
        }
        function Relationship() {
            window.open("../admin/frmRelationshipMapping.aspx?1=1", '', 'toolbar=no,titlebar=no,left=200, top=200,width=650,height=250,scrollbars=yes,resizable=yes')
            return false;
        }
        function DefaultAccounts() {
            window.open("../admin/frmDefaultAccountMapping.aspx", '', 'toolbar=no,titlebar=no,left=200, top=200,width=700,height=400,scrollbars=yes,resizable=yes')
            return false;
        }
        function ProjectAccounting() {
            window.open("../admin/frmProjectsAccountMapping.aspx", '', 'toolbar=no,titlebar=no,left=200, top=200,width=600,height=200,scrollbars=yes,resizable=yes')
            return false;
        }
        function ContactTypeAccounting() {
            window.open("../admin/frmContactTypeMapping.aspx", '', 'toolbar=no,titlebar=no,left=200, top=200,width=600,height=200,scrollbars=yes,resizable=yes')
            return false;
        }
        function AuthoritativeBizDoc() {
            window.open("../admin/frmAuthoritativeBizDocs.aspx", '', 'toolbar=no,titlebar=no,left=200, top=200,width=500,height=150,scrollbars=yes,resizable=yes')
            return false;
        }
        function OpenBizDocListFilter() {
            window.open("../admin/frmBizDocs.aspx", '', 'toolbar=no,titlebar=no,left=200, top=100,width=1000,height=400,scrollbars=yes,resizable=yes')
            return false;
        }
        function OpenCurrency() {

            window.open("../admin2/frmCurrencyRates.aspx", '', 'toolbar=no,titlebar=no,left=200, top=100,width=500,height=500,scrollbars=yes,resizable=yes')
            return false;
        }

        function OpenTaxDetails() {
            var h = screen.height;
            var w = screen.width;
            window.open("../Admin/frmTaxDetails.aspx", '', 'toolbar=no,titlebar=no,left=50, top=50,width=' + (w - 100) + ',height=' + (h - 200) + ',scrollbars=yes,resizable=yes')
            return false;
        }
        function OpenApprovalConfig() {
            if ($("#chkApprovalTimeExp").is(":checked")) {
                window.open("../Admin/frmApprovalConfig.aspx", '', 'toolbar=no,titlebar=no,left=200, top=100,width=1000,height=500,scrollbars=yes,resizable=yes')
            }
            return false;
        }

        function OpenNameTemplate() {
            window.open('../admin/frmNameTemplate.aspx', '', 'toolbar=no,titlebar=no,left=200,top=10,width=750,height=500,scrollbars=yes,resizable=yes');
            return false;
        }
        function OpenSMTPPopUp(a, b) {
            window.open('../admin/frmSMTPPopup.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&UserId=' + a + '&Mode=' + b, '', 'toolbar=no,titlebar=no,left=200,top=250,width=900,height=200,scrollbars=yes,resizable=yes');
            return false;
        }
        function OpenBizDocTemplate(a) {
            window.location.href = "../admin/frmBizDocTemplate.aspx?TemplateType=" + a;
        }

        function OpenBizDocTemplateList() {
            window.location.href = "../admin/frmBizDocTemplateList.aspx";
        }

        function OpenFulfillmentBizDocsList() {
            window.open('../opportunity/frmFulfillmentBizDocsList.aspx', '', 'toolbar=no,titlebar=no,left=200,top=10,width=750,height=500,scrollbars=yes,resizable=yes');
            return false;
        }

        function PortalColumnSettings() {
            window.open('../admin/frmPortalColumnSettings.aspx', '', 'toolbar=no,titlebar=no,left=200,top=10,width=1050,height=500,scrollbars=yes,resizable=yes');
            return false;
        }


        function Hide() {

            if (document.getElementById("radIndAccess").checked == true) {
                trUserName.style.display = 'none';
                trPassword.style.display = 'none';
            }
            else {
                trUserName.style.display = '';
                trPassword.style.display = '';
            }
        }
        function OpenSetting(a) {
            if (a == 1) {
                window.open('frmDupFieldSettings.aspx?FormID=24', '', 'toolbar=no,titlebar=no,top=200,left=200,width=550,height=350,scrollbars=yes,resizable=yes')
                return false;
            }
            else if (a == 2) {
                window.open('../Opportunity/frmConfEmbeddedCost.aspx?I=1', '', 'toolbar=no,titlebar=no,top=200,left=200,width=700,height=550,scrollbars=yes,resizable=yes')
                return false;
            }
            else if (a == 3) {
                window.open('../admin/frmUOM.aspx', '', 'toolbar=no,titlebar=no,top=200,left=200,width=700,height=550,scrollbars=yes,resizable=yes')
                return false;
            }
            else if (a == 4) {
                window.open('../admin/frmUOMConversion.aspx', '', 'toolbar=no,titlebar=no,top=200,left=200,width=700,height=550,scrollbars=yes,resizable=yes')
                return false;
            }
            return false;
        }

        function OpenSettings(type, FormID) {
            window.open('../Items/frmConfItemList.aspx?FormID=' + FormID + '&type=' + type, '', 'toolbar=no,titlebar=no,top=200,left=200,width=550,height=350,scrollbars=yes,resizable=yes')
            return false;
        }
        function OpenContractDetails() {
            window.open('../admin/frmContactAddressDefault.aspx', '', 'toolbar=no,titlebar=no,top=200,left=200,width=700,height=550,scrollbars=yes,resizable=yes')
            return false;
        }

        function AcceptTerms() {
            if (document.getElementById('chkKeepCreditCardInfo').checked == true) {
                window.open('frmAcceptTerms.aspx', '', 'toolbar=no,titlebar=no,top=200,left=200,width=550,height=550,scrollbars=yes,resizable=yes')
                return false;
            }
            else {
                return true;
            }
        }

        function OpenTaxCountryConfi() {

            window.open("../admin/TaxCountryConfiguration.aspx", '', 'toolbar=no,titlebar=no,left=200, top=100,width=500,height=500,scrollbars=yes,resizable=yes')
            return false;
        }
        function openRentalSetting() {

            window.open("../admin/frmRentalSetting.aspx", '', 'toolbar=no,titlebar=no,left=200, top=100,width=800,height=500,scrollbars=yes,resizable=yes')
            return false;
        }
        function openElectiveItemFields() {

            window.open("../admin/frmItemsFieldSetting.aspx", '', 'toolbar=no,titlebar=no,left=200, top=100,width=800,height=500,scrollbars=yes,resizable=yes')
            return false;
        }


        function TreeNodeAuthorization() {
            window.open("../admin/frmTreeNavigationAuthorization.aspx", '', 'toolbar=no,titlebar=no,left=200, top=100,width=1024,height=700,scrollbars=yes,resizable=yes');
            return false;
        }

        function OpenTerms(TID, u) {
            setCookie('termsCookie', 'Terms', 1);
            if (u == 1) {
                if (confirm('Term is already in use. Are you sure you want to edit term ?') == true) {
                    window.open("../admin/frmAddTerms.aspx?TermsID=" + TID, + '', 'toolbar=no,titlebar=no,left=200, top=100,width=400,height=300,scrollbars=yes,resizable=yes');
                }
            }
            else {
                window.open("../admin/frmAddTerms.aspx?TermsID=" + TID, + '', 'toolbar=no,titlebar=no,left=200, top=100,width=400,height=300,scrollbars=yes,resizable=yes');
            }
            return false;
        }

        function ShowDialog() {
            alert("Term is already in use, cannot edit/delete.");
            return false;
        }

        function Save() {
            if (document.getElementById("ddlShippingServiceItem").value > 0 && document.getElementById("ddlDiscountServiceItem").value > 0) {
                if (document.getElementById("ddlShippingServiceItem").value == document.getElementById("ddlDiscountServiceItem").value) {
                    alert("Shipping Service Item and Discount Service Item must be different.")
                    return false;
                }
            }

            if (Number($("#hdnPurchaseTaxCredit").val()) == 0 && $("#chkPurchaseTaxCredit").is(":checked")) {
                alert("You must first go to Global Settings | Accounting | General | Default Accounts | Purchase Tax Credit, and select a proper account.")
                return false;
            }
        }

        function openPortal() {
            window.open("../Admin/frmPortalDashboard.aspx", "", "width=900,height=400,status=no,scrollbars=yes,left=155,top=160");
            return false;
        }

        function OpenLandedCostVendors() {
            window.open("../Admin/frmLandedCostVendors.aspx", '', 'toolbar=no,titlebar=no,left=200, top=100,width=800,height=500,scrollbars=yes,resizable=no')
            return false;
        }
        function OpenColorConfigPage() {
            window.open("../Common/frmBizThemeConfg.aspx", '', 'toolbar = no, titlebar = no,left=200, top = 100, width = 800, height = 500, scrollbars = yes, resizable = yes');
            return false;
        }
        function OpenLandedCostExpenseAccount() {
            window.open("../Admin/frmLandedCostExpenseAccount.aspx", '', 'toolbar=no,titlebar=no,left=200, top=100,width=1000,height=500,scrollbars=yes,resizable=yes')
            return false;
        }
        function OpenPriceLevelNames() {
            window.open('../Items/frmPricingNamesTable.aspx', '', 'toolbar=no,titlebar=no,left=200,top=250,width=750,height=550,scrollbars=yes,resizable=yes');
            return false;
        }

        function OpenPOFulfillmentSettings() {
            window.open('../admin/frmPOFulfillmentSettings.aspx', '', 'toolbar=no,titlebar=no,left=200,top=10,width=800,height=500,scrollbars=yes,resizable=yes');
            return false;
        }

        function OpenBackOrderSettings() {
            window.open('../admin/frmBackOrderRule.aspx', '', 'toolbar=no,titlebar=no,left=200,top=10,width=780,height=830,scrollbars=yes,resizable=yes');
            return false;
        }

        function GoogleMailAuthenticate() {
            var h = screen.height;
            var w = screen.width;
            window.open('../GoogleMailAuthentication.aspx', '', 'toolbar=no,titlebar=no,left=50,top=50,width=' + (w - 100) + ',height=' + (h - 200) + ',scrollbars=yes,resizable=yes');
            return false;
        }

        function Office365MailAuthenticate() {
            var h = screen.height;
            var w = screen.width;
            window.open('../Office365MailAuthentication.aspx', '', 'toolbar=no,titlebar=no,left=50,top=50,width=' + (w - 100) + ',height=' + (h - 200) + ',scrollbars=yes,resizable=yes');
            return false;
        }

        var tabstrip;

        function onClientTabLoad(sender) {
            //Get reference to tabstrip
            tabstrip = sender;
        }

        function onClientTabSelected(sender, args) {

            //Persist selected tab text
            var tabText = args.get_tab().get_text();
            //Save text to a cookie using JavaScript
            if (tabText != "Accounting") {
                var accountingCookie = getCookie(accountingCookie);
                //If text from the cookie exists
                if (accountingCookie != "" || accountingCookie != null) {
                    document.cookie = 'accountingCookie' + '=; expires=Thu, 01-Jan-70 00:00:01 GMT;';
                    document.cookie = 'termsCookie' + '=; expires=Thu, 01-Jan-70 00:00:01 GMT;';
                }
            }
            else {
                saveTabIndex(tabText);
            }
        }

        //pageLoad automatically called by ASPNET AJAX when page ready
        function pageLoad() {
            $("[id*=rblMailProvider] input").on("click", function () {
                if ($(this).val() == 3) {
                    $("[id$=hplMail]").hide();
                    $("[id$=hplSMTP]").show();
                    $("[id$=hplSupportIMAP]").show();
                } else if ($(this).val() == 2) {
                    $("[id$=hplMail]").attr("onclick", "return Office365MailAuthenticate()");
                    $("[id$=hplMail]").text("Allow BizAutomation to access Office365 Mail");
                    $("[id$=hplMail]").show();
                    $("[id$=hplSMTP]").hide();
                    $("[id$=hplSupportIMAP]").hide();
                } else {
                    $("[id$=hplMail]").attr("onclick", "return GoogleMailAuthenticate()");
                    $("[id$=hplMail]").text("Allow BizAutomation to access Google Mail");
                    $("[id$=hplMail]").show();
                    $("[id$=hplSMTP]").hide();
                    $("[id$=hplSupportIMAP]").hide();
                }
            });

            //Must do tab strip select here so that 
            //multipage is initialized and updated
            loadTabIndex(tabstrip);
        }

        //Cookie name
        var tabCookieName = "accountingCookie";

        function saveTabIndex(tabText) {
            //Call JS function to save cookie name, tab text,
            //and days before cookie should expire
            setCookie(tabCookieName, tabText, 1);
        }

        //Cookie operation helper function
        //Save the value to a cookie and set expiration date
        function setCookie(c_name, value, expiredays) {
            var exdate = new Date();
            //exdate.setDate(exdate.getDate() + expiredays);
            exdate.setMinutes(exdate.getMinutes() + 1); // Create a date 1 minutes from now
            document.cookie = c_name + "=" + escape(value) +
                ((expiredays == null) ? "" : ";expires=" + exdate.toGMTString());
        }

        function loadTabIndex(tabstrip) {
            //If tabstrip reference isn't null
            if (tabstrip != null) {
                //Get cookie value
                var tabText = getCookie(tabCookieName);
                //If text from the cookie exists
                if (tabText != "" || tabText != null) {
                    //Set tabstrip selected index
                    var tab = tabstrip.findTabByText(tabText); //Get tab object

                    if (tab != null) {
                        var termsCookie = getCookie('termsCookie');
                        if (termsCookie != null && termsCookie != "") {
                            tab.select(); //Select tab
                           <%-- var radtabstrip = $find("<%=radTabAccount.ClientID %>");
                            radtabstrip.set_selectedIndex(0);--%>
                        }
                    }
                }
            }
        }

        //Cookie helper function
        //Gets a cookie based on supplied name and returns value
        //as a string
        function getCookie(c_name) {
            try {
                if (document.cookie.length > 0) {
                    c_start = document.cookie.indexOf(c_name + "=");
                    if (c_start != -1) {
                        c_start = c_start + c_name.length + 1;
                        c_end = document.cookie.indexOf(";", c_start);
                        if (c_end == -1) c_end = document.cookie.length;
                        return unescape(document.cookie.substring(c_start, c_end));
                    }
                }
            }
            catch (err) { }
            //If there is an error or no cookie
            //return an empty string
            return "";
        }

        function OpenModalReorderPoint() {
            try {
                $.when(LoadReorderPointConfiguration()).then(function () {
                    $("#modalReorderPoint").modal("show");
                });
            } catch (e) {
                alert("Unknown error ocurred");
            }
        }

        function LoadReorderPointConfiguration() {
            return $.ajax({
                type: "POST",
                url: '../WebServices/CommonService.svc/GetReorderPointConfiguration',
                contentType: "application/json",
                dataType: "json",
                beforeSend: function () {
                    $("#UpdateProgress").show();
                },
                complete: function () {
                    $("#UpdateProgress").hide();
                },
                success: function (data) {
                    var obj = $.parseJSON(data.GetReorderPointConfigurationResult);

                    $("#ddlReorderPointDays").val(obj.Days);
                    $("#ddlReorderPointPercent").val(obj.Percent);
                    $("input[name=ReorderPointBasedOn][value=" + (obj.BasedOn || 1) + "]").attr('checked', 'checked');
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    alert("Unknown error ocurred");
                }
            });
        }

        function SaveReorderPoint() {
            $.when(SaveReorderPoint1(), SaveReorderPoint2(), SaveReorderPoint3()).then(function () {
                $("#modalReorderPoint").modal("hide");
            });
        }

        function SaveReorderPoint1() {
            return $.ajax({
                type: "POST",
                url: '../WebServices/CommonService.svc/UpdateSingleFieldValue',
                contentType: "application/json",
                dataType: "json",
                data: JSON.stringify({
                    "mode": 54
                    , "updateRecordID": 0
                    , "updateValueID": parseInt($('input[name="ReorderPointBasedOn"]:checked').val())
                    , "comments": ""
                }),
                beforeSend: function () {
                    $("#UpdateProgress").show();
                },
                complete: function () {
                    $("#UpdateProgress").hide();
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    alert("Unknown error ocurred");
                }
            });
        }

        function SaveReorderPoint2() {
            return $.ajax({
                type: "POST",
                url: '../WebServices/CommonService.svc/UpdateSingleFieldValue',
                contentType: "application/json",
                dataType: "json",
                data: JSON.stringify({
                    "mode": 55
                    , "updateRecordID": 0
                    , "updateValueID": parseInt($("#ddlReorderPointDays").val())
                    , "comments": ""
                }),
                beforeSend: function () {
                    $("#UpdateProgress").show();
                },
                complete: function () {
                    $("#UpdateProgress").hide();
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    alert("Unknown error ocurred");
                }
            });
        }

        function SaveReorderPoint3() {
            return $.ajax({
                type: "POST",
                url: '../WebServices/CommonService.svc/UpdateSingleFieldValue',
                contentType: "application/json",
                dataType: "json",
                data: JSON.stringify({
                    "mode": 56
                    , "updateRecordID": 0
                    , "updateValueID": parseInt($("#ddlReorderPointPercent").val())
                    , "comments": ""
                }),
                beforeSend: function () {
                    $("#UpdateProgress").show();
                },
                complete: function () {
                    $("#UpdateProgress").hide();
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    alert("Unknown error ocurred");
                }
            });
        }

        function LoadMarketplaces() {
            return $.ajax({
                type: "POST",
                url: '../WebServices/CommonService.svc/GetMarketplaces',
                contentType: "application/json",
                dataType: "json",
                beforeSend: function () {
                    $("#UpdateProgress").show();
                },
                complete: function () {
                    $("#UpdateProgress").hide();
                },
                success: function (data) {
                    var html = $.parseJSON(data.GetMarketplacesResult);
                    $("#divEChannelHub").html(html);

                    RefreshOrderSource();
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    alert("Unknown error ocurred");
                }
            });
        }

        function OpenModalEChannelhub() {
            try {
                $.when(LoadMarketplaces()).then(function () {
                    if ($("#chkUsePredefinedCustomer").is(":checked")) {
                        $("input.btn-add-mcm").show();
                        $("input.btn-remove-mcm").show();
                    } else {
                        $("input.btn-add-mcm").hide();
                        $("input.btn-remove-mcm").hide();
                    }

                    $("#modalEChannelhub").modal("show");
                });
            } catch (e) {
                alert("Unknown error ocurred");
            }
        }

        function SaveCall1() {
            return $.ajax({
                type: "POST",
                url: '../WebServices/CommonService.svc/UpdateSingleFieldValue',
                contentType: "application/json",
                dataType: "json",
                data: JSON.stringify({
                    "mode": 51
                    , "updateRecordID": 0
                    , "updateValueID": ($("[id$=chkReceieveOrderWithoutItem]").is(":checked") ? 1 : 0)
                    , "comments": ""
                }),
                beforeSend: function () {
                    $("#UpdateProgress").show();
                },
                complete: function () {
                    $("#UpdateProgress").hide();
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    alert("Unknown error ocurred");
                }
            });
        }

        function SaveCall2() {
            return $.ajax({
                type: "POST",
                url: '../WebServices/CommonService.svc/UpdateSingleFieldValue',
                contentType: "application/json",
                dataType: "json",
                data: JSON.stringify({
                    "mode": 52
                    , "updateRecordID": 0
                    , "updateValueID": parseInt($("[id$=ddlUnMappedItem]").val())
                    , "comments": ""
                }),
                beforeSend: function () {
                    $("#UpdateProgress").show();
                },
                complete: function () {
                    $("#UpdateProgress").hide();
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    alert("Unknown error ocurred");
                }
            });
        }

        function SaveCall3() {
            return $.ajax({
                type: "POST",
                url: '../WebServices/CommonService.svc/UpdateSingleFieldValue',
                contentType: "application/json",
                dataType: "json",
                data: JSON.stringify({
                    "mode": 53
                    , "updateRecordID": 0
                    , "updateValueID": ($("[id$=chkUsePredefinedCustomer]").is(":checked") ? 1 : 0)
                    , "comments": ""
                }),
                beforeSend: function () {
                    $("#UpdateProgress").show();
                },
                complete: function () {
                    $("#UpdateProgress").hide();
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    alert("Unknown error ocurred");
                }
            });
        }

        function SaveMarketplaces() {
            var selectedMarketPlaces = "";

            if ($("[id$=chkReceieveOrderWithoutItem]").is(":checked") && parseInt($("[id$=ddlUnMappedItem]").val()) == 0) {
                alert("Select item to use when inbound order contains items not mapped to BizAutomation");
                $("[id$=ddlUnMappedItem]").focus();
                return false;
            } else {
                $("[id$=divEChannelHub] input[type='checkbox']").each(function () {
                    if ($(this).is(":checked")) {
                        selectedMarketPlaces += ((selectedMarketPlaces.length > 0 ? "," : "") + parseInt($(this).attr("id").replace("chkMarketplace", "")));
                    }
                });

                $("#UpdateProgress").show();

                $.when(SaveCall1(), SaveCall2(), SaveCall3()).then(function () {
                    $.ajax({
                        type: "POST",
                        url: '../WebServices/CommonService.svc/UpdateSingleFieldValue',
                        contentType: "application/json",
                        dataType: "json",
                        data: JSON.stringify({
                            "mode": 48
                            , "updateRecordID": 0
                            , "updateValueID": 0
                            , "comments": selectedMarketPlaces
                        }),
                        beforeSend: function () {
                            $("#UpdateProgress").show();
                        },
                        complete: function () {
                            $("#UpdateProgress").hide();
                        },
                        success: function (data) {
                            $("[id$=modalEChannelhub]").modal("hide");
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            alert("Unknown error ocurred");
                        }
                    });
                });
            }
        }

        function RefreshOrderSource() {
            $("#ddlOrderSource option").not(":first").remove();

            $("[id$=divEChannelHub] input[type='checkbox']").each(function () {
                if ($(this).is(":checked")) {
                    var temp = $(this).parent().find("label.lblMarketplaceName").clone();
                    temp.children("i:first").remove();

                    $("#ddlOrderSource").append("<option value='" + $(this).attr("id").replace("chkMarketplace", "") + "'>" + temp.html().trim() + "</option>");
                }
            });
        }

        function MarketplaceSelectionChanged() {
            try {
                RefreshOrderSource();
            } catch (e) {
                alert("Unknown error occurred.");
            }
        }

        function GetMarkerplaceCustomer() {
            var radCmbCompany = $find('radCmbCompany');

            if (radCmbCompany != null) {
                if (radCmbCompany.get_value() != null && radCmbCompany.get_value().length > 0) {
                    return parseInt(radCmbCompany.get_value());
                }
                else {
                    return 0;
                }
            }
            else {
                return 0;
            }
        }

        function SaveMarketplaceCustomerMapping() {
            try {
                if ($("[id$=chkUsePredefinedCustomer]").is(":checked")) {
                    var marketplaceID = parseInt($("#ddlOrderSource").val());
                    var divisionID = GetMarkerplaceCustomer();

                    if (marketplaceID > 0) {
                        if (divisionID > 0) {
                            $.ajax({
                                type: "POST",
                                url: '../WebServices/CommonService.svc/SaveMarketplaceCustomer',
                                contentType: "application/json",
                                dataType: "json",
                                data: JSON.stringify({
                                    "marketplaceID": marketplaceID
                                    , "divisionID": divisionID
                                }),
                                beforeSend: function () {
                                    $("#UpdateProgress").show();
                                },
                                complete: function () {
                                    $("#UpdateProgress").hide();
                                },
                                success: function (data) {
                                    LoadMarketplaces();
                                },
                                error: function (jqXHR, textStatus, errorThrown) {
                                    alert("Unknown error ocurred");
                                }
                            });
                        } else {
                            alert("Select organization");
                        }
                    } else {
                        alert("Select order source");
                        $("#ddlOrderSource").focus();
                    }
                }
            } catch (e) {
                alert("Unknown error occurred.");
            }
        }

        function RemoveMarketplaceCustomerMapping() {
            try {
                if ($("[id$=chkUsePredefinedCustomer]").is(":checked")) {
                    var marketplaceID = parseInt($("#ddlOrderSource").val());

                    if (marketplaceID > 0) {
                        $.ajax({
                            type: "POST",
                            url: '../WebServices/CommonService.svc/SaveMarketplaceCustomer',
                            contentType: "application/json",
                            dataType: "json",
                            data: JSON.stringify({
                                "marketplaceID": marketplaceID
                                , "divisionID": divisionID
                            }),
                            beforeSend: function () {
                                $("#UpdateProgress").show();
                            },
                            complete: function () {
                                $("#UpdateProgress").hide();
                            },
                            success: function (data) {
                                LoadMarketplaces();
                            },
                            error: function (jqXHR, textStatus, errorThrown) {
                                alert("Unknown error ocurred");
                            }
                        });
                    } else {
                        alert("Select order source");
                        $("#ddlOrderSource").focus();
                    }
                }
            } catch (e) {
                alert("Unknown error occurred.");
            }
        }

        function UpdateStaticCost() {
            try {
                if (confirm("This function will update all static cost for all items and vendors with values from dynamic costs. If your sale pricing is based on static cost plus margin, this update will impact your sales pricing. Are you sure you want to do this ?")) {
                    try {
                        var combobox = $find("<%= rcbItemGroupsDynamicCost.ClientID %>");
                        var checkedItems = combobox.get_checkedItems();

                        if (checkedItems != null && checkedItems.length > 0) {
                            $("[id$=UpdateProgress]").show();

                            $.ajax({
                                type: "POST",
                                url: '../WebServices/CommonService.svc/VendorCostTable/UpdateStaticCost',
                                contentType: "application/json",
                                dataType: "json",
                                data: JSON.stringify({
                                    "itemGroups": checkedItems.join(",")
                                }),
                                success: function (data) {
                                    $("[id$=UpdateProgress]").hide();
                                },
                                error: function (jqXHR, textStatus, errorThrown) {
                                    $("[id$=UpdateProgress]").hide();

                                    if (IsJsonString(jqXHR.responseText)) {
                                        var objError = $.parseJSON(jqXHR.responseText)
                                        alert("Error occurred while updating static costs: " + replaceNull(objError.ErrorMessage));
                                    } else {
                                        alert("Unknown error occurred while updating static costs");
                                    }
                                }
                            });
                        } else {
                            alert("Select item groups");
                        }


                    } catch (e) {
                        $("[id$=UpdateProgress]").hide();
                        alert("Unknown error occurred while updating static costs");
                    }
                }
            } catch (e) {
                alert("Unknown error occurred while updaing static costs");
            }

            return false;
        }

        function OpenVendorCostTable() {
            var w = 800;
            var h = 700;
            var left = (screen.width / 2) - (w / 2);
            var top = (screen.height / 2) - (h / 2);
            window.open("../common/frmManageVendorCostTable.aspx?IsUpdateDefaultVendorCostRange=1&VendorID=0&ItemID=0&VendorTCode=0", '', 'toolbar=no,titlebar=no,width=' + w + ',height=' + h + ',top=' + top + ',left=' + left + ',scrollbars=yes,resizable=yes');
            return false;
        }
    </script>
    <style type="text/css">
        .tblNoBorder td, .tblNoBorder th {
            border: 0px solid #fff !important;
        }

        fieldset {
            border: 1px solid #e1e1e1 !important;
            padding: 6px;
            margin: 10px;
        }

        legend {
            width: auto;
            border-bottom: 0px !important;
            margin-left: -50px;
            font-size: 15px;
            font-size: 18px;
            font-style: italic;
            color: #81b2cf;
            font-weight: bolder;
            margin-bottom: 3px !important;
        }

        .tblNoBorder > tbody > tr > td {
            padding: 8px 5px;
        }

        .tblNoBorder {
            width: auto;
            max-width: 100%;
        }

        @media (max-width:40em) {
            .tblNoBorder table, .tblNoBorder thead, .tblNoBorder tbody, .tblNoBorder tfoot, .tblNoBorder th, .tblNoBorder td, .tblNoBorder tr {
                display: block;
            }

            .tblNoBorder .editable_select {
                min-height: 30px;
            }

            .tblNoBorder td, .tblNoBorder th {
                text-align: left;
            }

            .tblNoBorder input[type="text"] {
                width: 90%;
            }
        }

        .Feature {
            font-weight: bolder;
            font-family: Segoe UI,Arial,Tamoha;
            text-decoration: underline;
            display: block;
        }

        .customerdetail {
            display: none;
        }

        .titleClass {
            background-color: #000;
            border: 1px solid #fff;
            padding: 10px 15px;
            width: 500px;
            display: none;
            color: #fff;
            text-align: left;
            font-size: 12px;
            -moz-box-shadow: 0 0 10px #000;
            -webkit-box-shadow: 0 0 10px #000;
        }

        .title-inner {
            max-width: 350px !important;
            text-align: left;
        }
    </style>
    <style>
        .innertooltipTitle .headingsmallfont {
            text-decoration: underline;
            font-weight: bold;
        }

        .innertooltipTitle .subheading {
            text-decoration: underline;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="RecordInformationPanel" runat="server"
    ClientIDMode="Static">

    <div class="row padbottom10">
        <div class="col-xs-12">
            <div id="divMessage" runat="server" class="row padbottom10" style="display: none">
                <div class="col-xs-12">
                    <div class="alert alert-warning">
                        <h4><i class="icon fa fa-warning"></i>Alert!</h4>
                        <asp:Literal ID="litMessage" runat="server" EnableViewState="False"></asp:Literal>
                    </div>
                </div>
            </div>
            <%--  <div class="pull-left" style="color: red">
                <asp:Literal ID="litMessage" runat="server"></asp:Literal>
            </div>--%>
            <div class="pull-right">
                <asp:LinkButton ID="btnSave" runat="server" CssClass="btn btn-primary"><i class="fa fa-floppy-o"></i>&nbsp;&nbsp;Save</asp:LinkButton>
            </div>
        </div>
    </div>

    <asp:UpdateProgress ID="UpdateProgress" runat="server" ClientIDMode="Static">
        <ProgressTemplate>
            <div class="overlay" style="z-index: 10000">
                <div class="overlayContent" style="color: #000; text-align: center; width: 250px; padding: 20px; background-color: #fff">
                    <i class="fa fa-2x fa-refresh fa-spin"></i>
                    <h3>Processing Request</h3>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="RecordActionPanel" runat="server"
    ClientIDMode="Static">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="TabsPlaceHolder" runat="server"
    ClientIDMode="Static">
    <telerik:RadTabStrip ID="radOppTab" runat="server" UnSelectChildren="True" EnableEmbeddedSkins="true"
        Skin="Default" ClickSelectedTab="True" MultiPageID="radMultiPage_OppTab" SelectedIndex="0" OnClientTabSelected="onClientTabSelected" OnClientLoad="onClientTabLoad">
        <Tabs>
            <telerik:RadTab Text="General" Value="General" PageViewID="radPageView_General">
            </telerik:RadTab>
            <telerik:RadTab Text="Order Management" Value="OrderManagement" PageViewID="radPageView_OrderManagement">
            </telerik:RadTab>
            <telerik:RadTab Text="Shipping" Value="ShippingSettings" PageViewID="radPageView_ShippingSettings">
            </telerik:RadTab>
            <telerik:RadTab Text="Accounting" Value="Accounting" PageViewID="radPageView_Accounting">
            </telerik:RadTab>
            <telerik:RadTab Text="Organization & Contacts" Visible="false" Value="CompaniesContacts" PageViewID="radPageView_CompaniesContacts">
            </telerik:RadTab>
            <telerik:RadTab Text="Portal" Value="Portal" PageViewID="radPageView_Portal" Visible="false">
            </telerik:RadTab>
        </Tabs>
    </telerik:RadTabStrip>
    <telerik:RadMultiPage ID="radMultiPage_OppTab" runat="server" SelectedIndex="0" CssClass="pageView">
        <telerik:RadPageView ID="radPageView_General" runat="server">
            <center><h4 style="margin-bottom: 0px; margin-top: 25px;"><b>GENERAL GLOBAL DEFAULTS</b></h4></center>
            <div class="col-md-3 col-md-offset-9">
                <i>
                    <asp:HyperLink ID="hplExportData" runat="server" Style="color: #000" CssClass="hyperlink" NavigateUrl="~/admin/frmExportData.aspx"
                        Text="Export Data to CSV">
                    </asp:HyperLink></i>
            </div>
            <asp:Table ID="table1" CellPadding="0" CellSpacing="0" BorderWidth="1" runat="server"
                Width="100%" CssClass="aspTable" BorderColor="black" GridLines="None" Style="border-left: 1px; border-right: 1px; border-bottom: 1px;">
                <asp:TableRow>
                    <asp:TableCell VerticalAlign="Top">
                        <center>
                        <table border="0" class="table table-responsive tblNoBorder" cellpadding="4"  style="margin: auto;">
                            <tr>
 
                                <td class="normal1" colspan="2" align="right" style="width: 200px;"> 
                                    <asp:HyperLink ID="hplGridColor" runat="server" CssClass="hyperlink" NavigateUrl="~/admin/frmGridColorScheme.aspx"
                                        Text="Customize Grid-Color based on Triggers" />&nbsp;&nbsp;
                                    <asp:Label ID="Label11" Text="?" CssClass="tip" runat="server" title="<div class='innertooltipTitle'><h5>Customize Grid Color based on Triggers</h5>If you want to use a value from a drop-down list or check box selection to<br/>change the row color on the list.</div>" />
                                    &nbsp;&nbsp;
                                    <a href="#" title="Global Color Theme" onclick="return OpenColorConfigPage();" id="aChangeColourTheme" runat="server">Global Color Theme</a>
                                </td>
                                <td style="display:none" class="normal1" align="right" colspan="2">Allow document check-out
                                
                                    <asp:CheckBox Text="" runat="server" ID="chkEnableDocRep" CssClass="signup" />
                                    <asp:Label ID="Label19" Text="?" CssClass="tip" runat="server" title="Enabling this feature allows you ensure that any document which is 'checked out' by User-A, is not modifiable by any other user of your organization untill  User-A 'check-in' that document." />
                                </td>
                                <td  style="width: 30px;">
                                    <asp:Label ID="Label44" Text="?" CssClass="tip" runat="server" title="<div  class='innertooltipTitle'><h5>Global Color Theme</h5>Use this to change the default blue color theme used throughout<br/>BizAutomation to your own custom color.  You can also change the global<br/>logo on the top left of the page, as well as the login theme, so users will get<br/>a customized look and feel.</div>" />
                                </td>
                                <td  style="width: 30px;">
                                    <asp:Label ID="Label4" Text="?" CssClass="tip" runat="server" title="Sets number of rows for list pages." />
                                </td>
                                <td  style="width: 265px;" class="normal1">Row count on list views (per page)&nbsp;
                                    <asp:DropDownList ID="ddlPaginRows" CssClass="signup" runat="server">
                                        <asp:ListItem Value="10">10</asp:ListItem>
                                        <asp:ListItem Value="20">20</asp:ListItem>
                                        <asp:ListItem Value="30">30</asp:ListItem>
                                        <asp:ListItem Value="40">40</asp:ListItem>
                                        <asp:ListItem Value="50">50</asp:ListItem>
                                        <asp:ListItem Value="100">100</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" align="right">
                                    <a href="#" data-toggle="modal" data-target="#divTracking">Web site tracking script</a>
                                    <div id="divTracking" class="modal fade" role="dialog">
                                        <div class="modal-dialog">

                                            <!-- Modal content-->
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                <h4 class="modal-title">Tracking Script</h4>
                                                </div>
                                                <div class="modal-body">
                                                <asp:TextBox runat="server" CssClass="normal1" TextMode="MultiLine" ID="txtWebAly"
                                        Width="550" Height="160" Text="<script type='text/javascript' language='javascript' > function pr(n) { document.write(n.replace('{#URL#}', encodeURIComponent(document.location)).replace('{#REF#}', encodeURIComponent(document.referrer)), '\r\n'); } var str = '%3Cscript src=\'ServerName/WebTrack.aspx?TrackID={#TrackID#}&URL={#URL#}&Ref={#REF#}\' type=\'text/javascript\'' + 'async %3E%3C/script%3E';  pr(unescape(str)); </script>"></asp:TextBox>
                                                </div>
                                                <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                </div>
                                            </div>

                                        </div>
                                    </div>

                                    &nbsp; &nbsp;
                                    <asp:Label ID="Label108" Text="?" CssClass="tip" runat="server" title="<div  class='innertooltipTitle'><h5>Website Tracking Script</h5>BizAutomation can track pages hosted by BizAutomation (requires at least<br/>one Biz-Commerce license). Assuming visitors have cookies enabled, this<br/>feature enables you to track what site a customer (Lead/Prospect/Account)<br/>came from, what pages on your site they visited and for how long, and<br/>(most importantly) when. By tracking when they visit your pages, you can<br/>capture them in filtering to include them in email broadcasting campaigns<br/>using BizAutomation�s own email marketing tools.</div>" />
                                    &nbsp; &nbsp;
                                    <asp:HyperLink ID="hplParentChildFieldMap" runat="server" CssClass="hyperlink" NavigateUrl="~/admin/frmParentChildFieldMap.aspx"
                                        Text="Parent / Child Field Mapping"></asp:HyperLink>
                                </td>
                                <td>
                                    <asp:Label ID="Label61" Text="?" CssClass="tip" runat="server" title="<div  class='innertooltipTitle'><h5>Parent/Child Field Mapping</h5>This feature enables you to update a field based on the<br/>value of another (for example - say you want the<br/>customer assignee to Automatically be used in the<br/>Assignee field of that customer�s sales orders).</div>" />
                                </td>

                                <td>
                                    <asp:Label ID="Label56" Text="?" CssClass="tip" runat="server" title="<div  class='innertooltipTitle'><h5>Global Date Format</h5>Changes the date format throughout the suite such as Invoices, A/R, etc..</div>" />
                                </td>

                                <td class="normal1">Global Date Format&nbsp;
                                    <asp:DropDownList ID="ddlDateFormat" CssClass="signup" runat="server">
                                        <asp:ListItem Value="MM/DD/YYYY">MM/DD/YYYY</asp:ListItem>
                                        <asp:ListItem Value="MM-DD-YYYY">MM-DD-YYYY</asp:ListItem>
                                        <asp:ListItem Value="DD/MON/YYYY">DD/MON/YYYY</asp:ListItem>
                                        <asp:ListItem Value="DD-MON-YYYY">DD-MON-YYYY</asp:ListItem>
                                        <asp:ListItem Value="DD/MONTH/YYYY">DD/MONTH/YYYY</asp:ListItem>
                                        <asp:ListItem Value="DD-MONTH-YYYY">DD-MONTH-YYYY</asp:ListItem>
                                        <asp:ListItem Value="DD/MM/YYYY">DD/MM/YYYY</asp:ListItem>
                                        <asp:ListItem Value="DD-MM-YYYY">DD-MM-YYYY</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" align="right">
                                        <a onclick="openElectiveItemFields()" href="javascript:void(0)" class="hyperlink">Elective Item Fields</a>&nbsp;&nbsp;
                                    <asp:Label ID="Label109" Text="?" CssClass="tip" runat="server" title="<div  class='innertooltipTitle'><h5>Elective Item Fields</h5>Hide any of the fields listed from item records.</div>" />
                                    &nbsp;&nbsp;
                                        <a onclick="openRentalSetting()" href="javascript:void(0)" class="hyperlink">Rental Settings</a>
                                </td>
                                <td>
                                    <asp:Label ID="Label80" Text="?" CssClass="tip" runat="server" title="<div  class='innertooltipTitle'><h5>Rental Settings</h5>These settings only apply to items that are long term assets which within<br/>the �Asset Items� list (not short term assets which are the inventory items<br/>you sell).</div>" />
                                </td>
                                <td>
                                    <asp:Label ID="Label60" Text="?" CssClass="tip" runat="server" title="<div  class='innertooltipTitle'><h5>Last Viewed Record Count</h5>Sets number of pages to remember on the left panel of<br/>the suite which displays icons representing key records<br/>you were last in (sorted from newest to oldest).</div>" />
                                </td>
                                <td>Last Viewed Record Count&nbsp;
                                    <asp:DropDownList ID="ddlLastViewd" runat="server" CssClass="signup">
                                        <asp:ListItem Text="6" Value="6">
                                        </asp:ListItem>
                                        <asp:ListItem Text="7" Value="7">
                                        </asp:ListItem>
                                        <asp:ListItem Text="8" Value="8">
                                        </asp:ListItem>
                                        <asp:ListItem Text="9" Value="9">
                                        </asp:ListItem>
                                        <asp:ListItem Text="10" Value="10">
                                        </asp:ListItem>
                                        <asp:ListItem Text="11" Value="11">
                                        </asp:ListItem>
                                        <asp:ListItem Text="12" Value="12">
                                        </asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" align="right">
                                    Enable SmartyStreets address verification
                                    <asp:TextBox ID="txtSmartystreetsAddrVerificationKeys" runat="server" ToolTip="Enter license keys here"></asp:TextBox>
                                    &nbsp;
                                      <asp:CheckBox ID="chkSmartystreetsAddrVerification" runat="server" />
                                </td>
                                <td>
                                    <asp:Label ID="Label107" Text="?" CssClass="tip" runat="server" title="<div class='innertooltipTitle'><h5>Enable SmartyStreets address verification & validation</h5>When basic Google address validation is not enough (which BizAutomation<br/>uses by default) and you need to maximize address verification and <br/>validation, then use this free connection to SmartyStreets (available in free<br/>and paid versions both for U.S. and international addresses).  </div>" />
                                </td>
                                <td>
                                    <asp:Label ID="Label59" Text="?" CssClass="tip" runat="server" title="<div  class='innertooltipTitle'><h5>Log User Out if Inactive</h5>This defines your session duration. After the set time, BizAutomation will<br/>require you log back in.</div>" />
                                </td>
                                <td class="normal1">Log users out if inactive for
                               
                                    <asp:DropDownList ID="ddlSessionTimeout" runat="server" CssClass="signup">
                                        <asp:ListItem Text="1" Value="1" Selected="True">
                                        </asp:ListItem>
                                        <asp:ListItem Text="2" Value="2">
                                        </asp:ListItem>
                                        <asp:ListItem Text="3" Value="3">
                                        </asp:ListItem>
                                        <asp:ListItem Text="4" Value="4">
                                        </asp:ListItem>
                                        <asp:ListItem Text="5" Value="5">
                                        </asp:ListItem>
                                        <asp:ListItem Text="6" Value="6">
                                        </asp:ListItem>
                                        <asp:ListItem Text="7" Value="7">
                                        </asp:ListItem>
                                        <asp:ListItem Text="8" Value="8">
                                        </asp:ListItem>
                                    </asp:DropDownList>
                                    &nbsp;hour(s)
                                    
                                </td>
                                <td class="normal1">     
                                    <asp:Label ID="Label1102" Text="?" CssClass="tip" runat="server" title="<div  class='innertooltipTitle'><h5>Turn on Resource Scheduling</h5>Selecting this check box will display a �Schedule Resources� button within the order details<br/>page (top right). This will be useful if after a sale your business needs to execute a series of<br/>tasks, but you don�t want to go as far as to run those tasks within a dedicated project record<br/>(The task list will run within the Milestones & Stages section of the order itself). When using<br/>this feature, we suggest enabling the �Total Progress� column within the Activities module.</div>" />
                                    &nbsp;                                    
                                    <asp:CheckBox ID="chkResourceScheduling" runat="server" />
                                    Turn on Resource Scheduling ?
                                </td>
                            </tr>



                            <tr>
                                        <td class="normal1" colspan="2" align="right" style="width:50%">Display contract elements&nbsp;
                                            <asp:CheckBox ID="chkDisplayContract" runat="server" />
                                        </td>
                                        <td>
                                            <asp:Label ID="Label104" Text="?" CssClass="tip" runat="server" title="<div  class='innertooltipTitle'><h5>Display contract elements</h5>This setting displays contract features as follows:<br/> (A) Displays Time balances within Action item and Project records. <br/>(B). Incident balances in Cases (which automatically deduct 1 incident from the balance when a case is created) <br/>and warrantee balances in Sales Returns and Cases (when an item within the case is referenced). </div>" />
                                        </td>
                                        <td >   
                                            <asp:Label ID="Label105" Text="?" CssClass="tip" runat="server" title="<div  class='innertooltipTitle'><h5>Enable the time element</h5>This setting enables employees from the drop-down list to deduct time balances left in contracts from Action item and Project records <br/>so that when the time check box is selected it deducts time from the contract time balance or if un-checked adds it back <br/>(NOTE � Click check box element will ONLY display if there is enough time balance in the contract).</div>" />
                                        </td>
                                        <td colspan="2" style="width:50%">Enable the time element for these users
                                            <telerik:RadComboBox ID="rdbEnableUsers" runat="server" style="margin-top:-10px" CheckBoxes="true"></telerik:RadComboBox>
                                        </td>


                                    </tr>

                            <tr style="display: none">
                                <td class="normal1" align="right">Domain Name
                                </td>
                                <td>
                                    <asp:TextBox ID="txtDomain" CssClass="signup" runat="server" Width="200" title="">
                                    </asp:TextBox>
                                    <asp:Label ID="Label1" Text="?" CssClass="tip" runat="server" title="Enter Your Organization Name" />
                                </td>
                                <td class="normal1" align="right">Domain Desc
                                </td>
                                <td align="left">
                                    <asp:TextBox ID="txtDesc" runat="server" CssClass="signup" Width="250">
                                    </asp:TextBox>
                                </td>
                            </tr>
                            <tr style="display:none;">
                                <td align="right" colspan="2">Start Days (From today)
                                    <asp:TextBox ID="txtStartDate" CssClass="signup" runat="server" Width="50">
                                    </asp:TextBox> 
                                </td>
                                <td>
                                   <asp:Label ID="Label2" Text="?" CssClass="tip" runat="server" title="To set the start date in the initial<br/><b style='color:red'> list </b>depending on date criteria - setting 1
                                            here means start date will be yesterday" />
                                </td>
                                <td align="left">
                                    <asp:Label ID="Label58" Text="?" CssClass="tip" runat="server" title="To set the start date in the initial list depending on date criteria - setting 1
                                            here means start date will be yesterday" />
                                </td>
                                <td colspan="2">Days between Start & End date&nbsp;
                                
                                    <asp:TextBox ID="txtInterval" CssClass="signup" runat="server" Width="50">
                                    </asp:TextBox>
                                </td>





                            </tr>
                            <tr style="display:none;">
                                <td class="normal1" style="display: none" align="right">Default Country
                                </td>
                                <td align="left" style="display: none">
                                    <asp:DropDownList CssClass="signup" ID="ddlCountry1" runat="server">
                                    </asp:DropDownList>
                                </td>

                                <td class="normal1" align="right" colspan="2">BizAutomation Calendar Time Format
                                
                                    <asp:DropDownList ID="ddlCalendarTimeFormat" runat="server" CssClass="signup">
                                        <asp:ListItem Value="12">12 Hours</asp:ListItem>
                                        <asp:ListItem Value="24">24 Hours</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                                <td>
                                    <asp:Label ID="Label57" Text="?" CssClass="tip" runat="server" title="When you want to process payment for BizDoc(i.e Invoice) thorugh credit card Default payment gateway will be used for capturing amount from credit card. WebStore also uses default payment gateway to process payments for orders placed online" />
                                </td>
                                
                            </tr>
                            <tr style="display:none;">
                                
                                <td>
                                    <asp:Label ID="Label25" Text="?" CssClass="tip" runat="server"
                                        title="Once you copy the web tracking script, paste it somewhere within the body tags of the web to lead form or the check out page, to track visitor behavior from leads, prospects, and accounts that fill in these forms." />
                                </td>

                                

                            </tr>
                            <tr style="display:none;">
                                
                                <td>
                                    <asp:Label ID="Label98" Text="?" CssClass="tip" runat="server" title="Customize Grid-Color based on Triggers" />
                                </td>
                                 
                            </tr>
                            <tr style="display:none">
                                
                               
                                <td>
                                    <asp:Label ID="Label62" Text="?" CssClass="tip" runat="server" title="Select on right list box, fields that define what duplicate is. E.g. � Selecting email means that when entering a contact, if the email value being entered is already in the system, it will warn you that it�s a duplicate, but selecting first name AND email fields will define a duplicate only if both that email and first name are found in the database, one by itself will not trigger the duplicate rule when entering data. " />
                                </td>
                                <td class="normal1" align="justify">
                                    <%--<asp:CheckBox Text="" runat="server" ID="chkInlineEdit" CssClass="signup" />
                                            <asp:Label Text="?" CssClass="tip" runat="server" title="Enables editing of field without opening record in edit mode." />--%>
                                   
                                </td>
                            </tr>
                            <tr style="display:none">
                                <td class="normal1" colspan="2" align="right"><b>Email to Case</b> &nbsp;
                                    <asp:HyperLink ID="hplSupportSMTP" runat="server" CssClass="hyperlink">SMTP Configuration</asp:HyperLink></td>
                                <td>
                                    <asp:Label ID="Label63" Text="?" CssClass="tip" runat="server" title="When you want to process payment for BizDoc(i.e Invoice) thorugh credit card Default payment gateway will be used for capturing amount from credit card. WebStore also uses default payment gateway to process payments for orders placed online" />
                                </td>
                                <td>
                                    <asp:Label ID="Label64" Text="?" CssClass="tip" runat="server" title="When you want to process payment for BizDoc(i.e Invoice) thorugh credit card Default payment gateway will be used for capturing amount from credit card. WebStore also uses default payment gateway to process payments for orders placed online" />
                                </td>
                                <td class="normal1"><b>Email to Case</b>&nbsp;
                      
                                </td>
                            </tr>
                            
                            <tr style="display:none;">
                                
                                
                            </tr>
                            <tr style="display:none;">
                                <td colspan="2" align="right">
                                </td>
                                <td>
                                    <asp:Label ID="Label106" Text="?" CssClass="tip" runat="server" title="Enabling SmartyStreets Address Verification" />
                                </td>
                               
                                
                            </tr>
                            <tr style="display:none;">
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr style="display: none">


                                <td class="normal1" align="justify" colspan="2">
                                    <asp:HyperLink Text="Edit Unsubscribe Page Template" CssClass="hyperlink" runat="server"
                                        ID="hplUnsubscribe" onclick="OpenBizDocTemplate('1');">
                                    </asp:HyperLink>
                                </td>
                            </tr>
                            <tr style="display: none">
                                <td class="normal1" align="right">Compose Window
                                </td>
                                <td align="left">
                                    <asp:DropDownList CssClass="signup" ID="ddlComposeWindow" runat="server">
                                        <asp:ListItem Value="1">1) Outlook Compose Window</asp:ListItem>
                                        <asp:ListItem Value="2">2) Biz Compose Window</asp:ListItem>
                                    </asp:DropDownList>
                                    <asp:Label ID="Label49" Text="?" CssClass="tip" runat="server" title="1) Clicking email address in Biz would open compose email window in Microsoft Outlook installed on your computer<br> 2) Clicking email address in Biz would open compose email window in browser itself" />
                                </td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr style="display: none">
                                <td class="normal1" align="right">Pay-period
                                </td>
                                <td align="left">
                                   <%-- <asp:DropDownList CssClass="signup" ID="ddlPayPeriod" runat="server">
                                        <asp:ListItem Value="1">Monthly (Once per month)</asp:ListItem>
                                        <asp:ListItem Value="2">Twice per month (Semi-monthly)</asp:ListItem>
                                        <asp:ListItem Value="3">Once per week (Weekly), ending on Monday-Sunday</asp:ListItem>
                                        <asp:ListItem Value="4">Once per week (Weekly), ending on Tuesday-Monday</asp:ListItem>
                                        <asp:ListItem Value="5">Once per week (Weekly), ending on Wednesday-Tuesday</asp:ListItem>
                                        <asp:ListItem Value="6">Once per week (Weekly), ending on Thursday-Wednesday</asp:ListItem>
                                        <asp:ListItem Value="7">Once per week (Weekly), ending on Friday-Thursday</asp:ListItem>
                                        <asp:ListItem Value="8">Once per week (Weekly), ending on Saturday-Friday</asp:ListItem>
                                        <asp:ListItem Value="9">Once per week (Weekly), ending on Sunday-Monday</asp:ListItem>
                                        <asp:ListItem Value="10">Once per week (Weekly), ending on Monday-Sunday</asp:ListItem>
                                        <asp:ListItem Value="11">Every 2 weeks (Bi-monthly), ending on Tuesday-Monday</asp:ListItem>
                                        <asp:ListItem Value="12">Every 2 weeks (Bi-monthly), ending on Wednesday-Tuesday</asp:ListItem>
                                        <asp:ListItem Value="13">Every 2 weeks (Bi-monthly), ending on Thursday-Wednesday</asp:ListItem>
                                        <asp:ListItem Value="14">Every 2 weeks (Bi-monthly), ending on Friday-Thurday</asp:ListItem>
                                        <asp:ListItem Value="15">Every 2 weeks (Bi-monthly), ending on Saturday-Friday</asp:ListItem>
                                        <asp:ListItem Value="16">Every 2 weeks (Bi-monthly), ending on Sunday-Monday</asp:ListItem>
                                    </asp:DropDownList>--%>
                                </td>

                            </tr>
                            <tr style="display: none">
                                <td class="normal1" style="display: none" align="right">Populate User dropdownlist based on
                                </td>
                                <td style="display: none" align="left">
                                    <asp:DropDownList CssClass="signup" ID="ddlPopulateUserCrt" runat="server">
                                        <asp:ListItem Value="0">All</asp:ListItem>
                                        <asp:ListItem Value="2">Team</asp:ListItem>
                                        <%--<asp:ListItem Value="1">Territory</asp:ListItem>--%>
                                    </asp:DropDownList>
                                    <asp:Label ID="Label22" Text="?" CssClass="tip" runat="server" title="Populate user drop down based on ALL or Team � Team lists employees within your team scope (From Admin | User Admin | Internal users), All lists all employees. " />
                                </td>
                                <td class="normal1" align="right">Display BizDocs on Portal / WebStore
                                </td>
                                <td align="left" class="normal1">
                                    <asp:HyperLink ID="hplPortalBizDocs" runat="server" Text="Set" onclick="return PortalBizDocs();"
                                        CssClass="hyperlink">
                                    </asp:HyperLink>
                                </td>
                            </tr>
                            <%--<tr>
                                <td class="normal1" align="right">
                                    <label for="chkInteMedPage">
                                        Enable Intermediatory Page</label>
                                </td>
                                <td align="left">
                                    <asp:CheckBox ID="chkInteMedPage" runat="server" />
                                </td>
                            </tr>--%>

                            <tr style="display: none">
                                <td class="Feature" align="right">Features
                                </td>
                            </tr>
                            <tr style="display: none">

                                <td class="normal1" style="display: none" align="justify" colspan="2">
                                    <asp:HyperLink Text="Edit Action Items Attendees Page Template" CssClass="hyperlink"
                                        runat="server" ID="hplActionItemAttendee" onclick="OpenBizDocTemplate('2');">
                                    </asp:HyperLink>
                                </td>
                            </tr>
                            <tr>

                                <%--<td class="normal1" align="right">
                                    Inline Edit (Beta):
                                </td>--%>
                            </tr>
                            
                            <tr style="display: none">
                                <td class="Feature" align="right">Google Integration<asp:Label ID="Label5" Text="?" CssClass="tip" runat="server"
                                    title="It works only for users whose IMAP details are valid. To set IMAP details go to Administration->User Administration->Internal Users" />
                                </td>
                            </tr>
                            <tr style="display: none">
                                <td align="left" colspan="2">&nbsp;</td>
                                <td class="normal1" style="display: none">
                                    <label for="chkGtoBContact">
                                        Turn on Google to Biz Contacts Integration</label>
                                    <asp:CheckBox ID="chkGtoBContact" runat="server" />
                                    <asp:Label ID="Label6" Text="?" CssClass="tip" runat="server" title="Biz will synchronize new contacts created in your Google App Account every 30 minits. Provided that IMAP settings are valid for each user and Contact Groups are set to synchronize(Look for 'Google to Biz Contacts Groups' in 'User Administration->Internal Users->User Details popup') ." />
                                </td>
                                <td align="center" colspan="2">
                                    <b>Google Analytics Configurations</b>
                                </td>
                            </tr>
                            <tr style="display: none">
                                <td align="left" colspan="2">&nbsp;</td>
                                <td class="normal1" style="display: none" align="left" colspan="2">
                                    <label for="chkBtoGContact">
                                        Turn on Biz to Google Contacts Integration</label>
                                    <asp:CheckBox ID="chkBtoGContact" runat="server" />
                                    <asp:Label ID="Label7" Text="?" CssClass="tip" runat="server" title="Enabling this will show a button 'Update Google' in Contact Details and Account Details.Its required that your IMAP settings are valid in biz." />
                                </td>
                                <td class="normal1" align="right">User E-mail : 
                                </td>
                                <td>
                                    <asp:TextBox ID="txtGAUserEmail" CssClass="signup" runat="server" Width="200" title="">
                                    </asp:TextBox>

                                </td>
                            </tr>
                            <tr style="display: none">
                                <td class="normal1" align="left" colspan="2">
                                    <label for="chkGtoBCalendar">
                                        Turn on Google to Biz Calendar Integration</label>
                                    <asp:CheckBox ID="chkGtoBCalendar" runat="server" />
                                    <asp:Label ID="Label8" Text="?" CssClass="tip" runat="server" title="Biz will syncronize calender events created in your Google App Account every 10 minits. Provided that IMAP settings are valid for each user.<br> <b>Note:</b> Recurring calender events sync is not supported." />
                                </td>
                                <td class="normal1" align="right">User Password : 
                                </td>
                                <td>
                                    <asp:TextBox ID="txtGAPassword" CssClass="signup" runat="server" Width="200" title="">
                                    </asp:TextBox>

                                </td>
                            </tr>
                            <tr style="display: none">
                                <td class="normal1" align="left" colspan="2">
                                    <label for="chkBtoGCalendar">
                                        Turn on Biz to Google Calendar Integration</label>
                                    <asp:CheckBox ID="chkBtoGCalendar" runat="server" />
                                    <asp:Label ID="Label9" Text="?" CssClass="tip" runat="server" title="Biz will syncronize calender events created in your Google App Account every 10 minits. Provided that IMAP settings are valid for each user." />
                                </td>
                                <td class="normal1" align="right">User Profile Id : 
                                </td>
                                <td>
                                    <asp:TextBox ID="txtGAProfileID" CssClass="signup" runat="server" Width="200" title="">
                                    </asp:TextBox>

                                </td>
                            </tr>
                            <tr style="display: none">
                                <td class="normal1" align="right">Outlook Calendar Time format
                                </td>
                                <td align="left">
                                <%--    <asp:DropDownList ID="ddlCalendarTimeFormat" runat="server" CssClass="signup">
                                        <asp:ListItem Value="12">1:00pm</asp:ListItem>
                                        <asp:ListItem Value="24">13:00</asp:ListItem>
                                    </asp:DropDownList>--%>
                                </td>

                            </tr>


                        </table>
                            <fieldset>
                                <legend style="margin:auto">Global Messaging & Calendaring</legend>
                                <table border="0" class="table table-responsive tblNoBorder" style="margin:auto;">
                                    <tr>
                                        <td colspan="2" align="right" style="width:50%;">
                                            <b style="display:none">Google:</b>
                                            <a style="display:none" id="hplConfgAnalytics" onclick="return OpenGoogleAnalyticsDetails()"  class="hyperlink">Configure Analytics</a>
                                            Enable Calendar Sync
                                            <asp:CheckBox ID="chkEnableCalender" runat="server" />
                                        </td>
                                        <td>
                                            <asp:Label ID="Label65" Text="?" CssClass="tip" runat="server" title="<div  class='innertooltipTitle'><h5>Enable Calendar Sync for Google & Microsoft Calendars</h5>This feature enables you  to synchronize your calendaring events from Google�s<br/>	GSuite / Gmail or Microsoft�s Office 365 Messaging & Collaboration platform with<br/>the �Activities� module in BizAutomation. Once configured, BizAutomation will show<br/>your calendar events in chronological order as one of your activities. You�ll also have<br/>the ability to post activities from BizAutomation (Action Items) to your external<br/>calendar by selecting the �Add to Calendar� check box within activity details.</div>" />
                                        </td>
                                        <td> 
                                            <asp:Label ID="Label23" Text="?" CssClass="tip" runat="server" title="<div  class='innertooltipTitle'><h5>Overview of IMAP and SMTP messaging in BizAutomation</h5><span class='headingsmallfont'>Inbound IMAP:</span> There are 2  configurations we use to deliver inbound messages;<br/>(1)<span class='subheading'> Individual IMAP:</span> Configured within the Human Resources | Internal Users section, where users are<br/>configured with their own personalized email account such as bob@yourcompany.com (similar to the process of<br/>configuring IMAP on your own Gmail or Outlook email app)<br/>(2)<span class='subheading'> Global IMAP:  </span> Uses the account for non-personal generic inbound messaging  (e.g. info@yourcompany.com)<br/>Portions of the suite that use global IMAP include web to lead forms used to generate organization records in<br/>BizAutomation from your external web site, and web to case forms used to generate trouble tickets (i.e. �Cases�)<br/>within the support module.<br/><br/><span class='headingsmallfont'>Outbound SMTP:  </span> SMTP is used for sending messages from BizAutomation. There are 3 levels used; <br/>(1)<span class='subheading'> Individual SMTP:</span> Configured within the Human Resources | Internal Users section, where each users is<br/>configured with their own personalized email account such as bob@yourcompany.com (similar to the process of<br/>configuring SMTP  on your own Gmail or Outlook email app)<br/>(2)<span class='subheading'> Low Volume SMTP:    </span> Leverages a single Gmail or Office 365 SMTP email account for generic outbound<br/>messaging (e.g. info@yourcompany.com).  �Low Volume� refers to the fact that the Gmail or Office 365 SMTP servers<br/>will be used to relay messages, which have relatively low sending limit in terms of to how many messages can be sent<br/>out per day, often limited to a few hundred messages per batch / day. BizAutomation�s workflow automation tool <br/>which includes email alerts, uses this approach.<br/>(2)<span class='subheading'> High Volume SMTP:   </span> Configured for high volume email marketing, where the limit is in the tens or even <br/>hundreds of thousands of email messages being sent out per batch. The only module in BizAutomation that uses bulk<br/>SMTP (which requires the configuration of Amazon SES) is the email broadcasting service available for any saved<br/>search or filter performed from either of the two advanced search tools within BizAutomation. </div>" />
                                        </td>
                                        <td  class="normal1" colspan="2" align="left" style="width:50%;">
                                            <ul class="list-inline">
                                                
                                                <li>
                                                    <asp:HyperLink ID="hplMail" runat="server" CssClass="hyperlink" style="display:none">Allow BizAutomation to access Google Mail</asp:HyperLink>
                                                    <asp:HyperLink ID="hplSupportIMAP" runat="server" CssClass="hyperlink" style="display:none;margin-right:20px">Inbound <i style="color:#000">(IMAP)</i></asp:HyperLink>
                                                    <asp:HyperLink ID="hplSMTP" runat="server" CssClass="hyperlink" style="display:none">Low Volume Outbound (Not Configured)</asp:HyperLink>
                                                   
                                                </li>
                                            </ul>
                                    
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" align="right">
                                            <ul class="list-inline">
                                                <li>
                                                    <asp:RadioButtonList ID="rblMailProvider" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow">
                                                        <asp:ListItem Text="Gmail" Value="1"></asp:ListItem>
                                                        <asp:ListItem Text="Office 365" Value="2"></asp:ListItem>
                                                        <asp:ListItem Text="Other" Value="3"></asp:ListItem>
                                                    </asp:RadioButtonList>
                                                </li>
                                            </ul>
                                        </td>
                                        <td>

                                        </td>
                                        <td>

                                        </td>
                                        <td colspan="2">
                                             <asp:HyperLink ID="hplMarketingEmailConfig" Target="_blank" NavigateUrl="~/Marketing/frmEmailBroadcastSetting.aspx" runat="server" CssClass="hyperlink" >High Volume Outbound<i style="color:#000">(Relay Server for email broadcasting)</i></asp:HyperLink>
                                        </td>
                                    </tr>
                                </table>
                                 
                            </fieldset>
                            <fieldset>
                                <legend style="margin:auto">Organization & Contact Defaults</legend>
                                <table border="0" class="table table-responsive tblNoBorder" style="margin: auto;">
                            <tr>
                                <td class="normal1" colspan="2" align="right" style="width:200px">Organization Search Criteria:&nbsp;
                                    <asp:RadioButtonList ID="rblOrganizationSearchCriteria" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow"
                                        CssClass="signup">
                                        <asp:ListItem Text="Start With" Value="1" Selected="True">
                                        </asp:ListItem>
                                        <asp:ListItem Text="Contains" Value="2">
                                        </asp:ListItem>
                                    </asp:RadioButtonList>
                                </td>

                                <td  style="width:30px">
                                    <asp:Label ID="Label28" Text="?" CssClass="tip" runat="server" title="<div  class='innertooltipTitle'>When entering a value in simple search, the default pattern against which BizAutomation will look to match values<br/> you enter for Organization records will default to values based on the values starting with the characters you�re entering<br/> or where the field values contain the characters you�re entering</div>" />
                                </td>
                                <td  style="width:30px">

                                    <asp:Label ID="Label33" Text="?" CssClass="tip" runat="server" title="Select default Bill-to and Ship-to country." />
                                </td>
                                <td  style="width:415px" colspan="2">Default Country
                                    <asp:DropDownList CssClass="signup" ID="ddlCountry" runat="server">
                                    </asp:DropDownList>
                                </td>


                            </tr>
                            <tr >

                                <td colspan="2" align="right">Organization Column Display:
                                    <asp:HyperLink ID="hlpOrganizationSearch" runat="server" NavigateUrl="#" onclick="return OpenSettings('0','96');">Configuration</asp:HyperLink>
                                </td>
                                <td>
                                    <asp:Label ID="Label35" Text="?" CssClass="tip" runat="server" title="<div  class='innertooltipTitle'>Set which organization fields to use when trying to look up organization records.<br/><br/><u> For example</u>: Say you want to find ABC Inc., which has a<br/> custom field called �License Number� with a value of �123�.<br/> By selecting �License Number� as one of the search fields,<br/> ABC will come up when you search for �123�.</div>" />
                                </td>

                                <td>
                                    <asp:Label ID="Label34" Text="?" CssClass="tip" runat="server" title="<div  class='innertooltipTitle'>This enables you to set what Organization columns you want to display while searching for Organizations.<br/><br/><u>For example</u><br/>Say you�re creating a new Sales Order and are searching for Organization ABC and want the 1st column<br/>to show Organization Name, the 2nd to show Bill-To State, and the 3rd to show Rating.</div>" />
                                </td>
                                <td colspan="2">Organization Look Ahead Field Search:&nbsp;
                                    <asp:HyperLink ID="hlpOrganizationFields" runat="server" NavigateUrl="#" onclick="return OpenSettings('0','97');">Configuration</asp:HyperLink>
                                </td>


                            </tr>
                            <tr >
                                <td class="normal1" colspan="2" align="right">Min # of characters when searching an Organization:
                                    <asp:DropDownList ID="ddlChr" runat="server" CssClass="signup">
                                        <%--<asp:ListItem Value="0">0</asp:ListItem>
                                        <asp:ListItem Value="1">1</asp:ListItem>--%>
                                        <asp:ListItem Value="2">2</asp:ListItem>
                                        <asp:ListItem Value="3">3</asp:ListItem>
                                        <asp:ListItem Value="4">4</asp:ListItem>
                                        <asp:ListItem Value="5">5</asp:ListItem>
                                    </asp:DropDownList>
                                </td>

                                <td>
                                    <asp:Label ID="Label29" Text="?" CssClass="tip" runat="server" title="<div  class='innertooltipTitle'>Min # of characters when searching an Organization.</div>" />
                                </td>
                                <td>
                                    <asp:Label ID="Label55" Text="?" CssClass="tip" runat="server" title="<div  class='innertooltipTitle'>Set DefaultContact Address .</div>" />
                                </td>
                                <td colspan="2" align="left">Contact Address Defaults:&nbsp;
                                    <asp:HyperLink ID="hlpContactAddress" runat="server" NavigateUrl="#" onclick="return OpenContractDetails();">Configuration</asp:HyperLink>
                                </td>
                                <td style="display: none">Auto Populate Address
                                </td>
                                <td style="display: none" align="left">
                                    <asp:CheckBox ID="chkPrimaryAddress" runat="server" Text="" />
                                    When creating a new contact, populate its primary address, with the
                                   <asp:DropDownList ID="ddlAddress" runat="server" CssClass="signup">
                                       <asp:ListItem Text="Bill To" Value="1">
                                       </asp:ListItem>
                                       <asp:ListItem Text="Ship To" Value="2">
                                       </asp:ListItem>
                                   </asp:DropDownList>
                                    address of the parent organization it belongs to.
                                </td>
                            </tr>
                            <tr>
                                <td class="normal1" align="right" colspan="2"> 
                                    <asp:LinkButton ID="lnkLeadSource" runat="server">Set lead source based on referring page data</asp:LinkButton>
                                    <div id="divLeadSource" class="modal fade" role="dialog">
                                        <asp:UpdatePanel ID="upLeadSource" runat="server" class="modal-dialog modal-lg" UpdateMode="Conditional" ChildrenAsTriggers="true">
                                            <ContentTemplate>
                                                 <div class="modal-content">
                                                    <div class="modal-header">
                                                        <%--<button type="button" class="close" data-dismiss="modal" style="font-size: 27px">&times;</button>
                                                        <h4 class="modal-title">How do you want BizAutomation to select your Organization Rating?</h4>--%>
                                                        <button type="button" class="btn btn-primary" data-dismiss="modal">Close Page</button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <asp:Label ID="Label75" runat="server" Text="" ForeColor="Red"></asp:Label>
                                                        <div class="row padbottom10">
                                                            <div class="col-sm-12 col-md-5"> 
                                                                <div class="form-group">
                                                                    <label>If referring page in web analysis section of an organization record, contains this domain name / value when record is created:</label>
                                                                    <asp:TextBox runat="server" ID="txtSearchDomain" CssClass="form-control"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-12 col-md-5"> 
                                                                <div class="form-group">
                                                                    <label>Then set the following value within the �Lead Source� drop-down field. :</label>
                                                                    <asp:DropDownList ID="ddlLeadSource" runat="server" CssClass="form-control"></asp:DropDownList>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-12 col-md-2">
                                                                <asp:Button ID="btnAddLeadSource" runat="server" CssClass="btn btn-primary btn-block" Text="Add"></asp:Button>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-xs-12">
                                                                <div class="table-responsive">
                                                                    <asp:GridView ID="gvLeadSource" CssClass="table table-bordered table-striped" runat="server" ShowHeaderWhenEmpty ="true" 
                                                                        AutoGenerateColumns="false" CellPadding="3" OnRowDeleting="gvLeadSource_RowDeleting"
                                                                        CellSpacing="3" BorderColor="#f0f0f0" DataKeyNames="numTrackingID" UseAccessibleHeader="true">
                                                                        <Columns>
                                                                            <asp:BoundField HeaderText="Domain Name" DataField="vcOrginalRef" ItemStyle-HorizontalAlign="Center" />
                                                                            <asp:BoundField HeaderText="LeadSource" DataField="vcReferrer" ItemStyle-HorizontalAlign="Center" />
                                                                            <asp:TemplateField ItemStyle-Width="25">
                                                                                <ItemTemplate>
                                                                                    <asp:LinkButton runat="server" ClientIDMode="AutoID" id="lkbLeadDelete" CssClass="btn btn-xs btn-danger" CommandName="Delete" ><i class="fa fa-trash-o"></i></asp:LinkButton>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                        </Columns>
                                                                        <EmptyDataTemplate>No Records Found.</EmptyDataTemplate>
                                                                    </asp:GridView>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </div>
                                   <%-- <asp:CheckBox ID="chkOverRideAssigntoList" runat="server"></asp:CheckBox>
                                    <asp:HyperLink ID="HyperLink2" runat="server" NavigateUrl="#" onclick="return OpenAssigntoContact();">Override default "Assign to" list with this one</asp:HyperLink>--%>
                                </td>
                                <td>
                                    <asp:Label ID="Label21" Text="?" CssClass="tip" runat="server" title="<div  class='innertooltipTitle'>   If you use BizAutomation�s �Web site tracking script� (found within the �General� section), we<br/> recommend you map common denominator lead source URL values displayed within the �web<br/> analysis� sub-tab section when organization records (e.g. �leads�) are generated from the �web to<br/> lead� form on BizAutomation (This URL is often the landing page lead forms from 3rd party web sites<br/> that link to your own �web to lead� form). Because these URLs can contain many characters that<br/> obfuscate lead source list filtering and reporting, the idea here is to group these into a common lead<br/> source value such as say �Google Ads�. You do that by mapping values from source page URLs in<br/> these �Referring Pages� (you can see the referring page URL from Organization details | �Web<br/> Analysis�). <br/><br/><u>For Example</u>: Let�s say you have the following 2 referring pages, which you want to map to a single<br/> lead source value you�ve added to the �Lead Source� drop-down field from the �Admin Panel<br/> (Add/Edit Drop-Down Lists | Module �Leads/Prospects/Accounts� List �Lead Source�): <br/> (1)https://www.google.com/p/22222/Ad1  <br/>(2) https://www.google.com/p/11111/Ad2 <br/><br/>Assume you want them both to map to a �Lead Source� called �Google Ads�. From here you<br/> would add these lead source values to the �Domain� column on the left, and on the right pane, you<br/> would select �Google Ads� resulting in both rows mapping to the same lead source value.</div>" />
                                </td>
                                <td>
                                    <asp:Label ID="Label97" Text="?" CssClass="tip" runat="server" title="<div  class='innertooltipTitle'><h5>Organization Rating Rules</h5>Use sales order amount ranges to automatically select rating<br/> values defined from: Add & Edit Drop-Down Lists | Module<br/> (Organizations) List (Organization Rating) so you can sort and <br/>filter organizations from the list page, based on actual spend <br/>performance, rather than manual selection based on opinion.</div>" />
                                </td>
                                <td colspan="2">
                                    <asp:LinkButton ID="lkbOpenOrganizationRating" runat="server">Organization Rating Rules</asp:LinkButton>
                                    <div id="divOrganizationRatingRule" class="modal fade" role="dialog">
                                        <asp:UpdatePanel ID="upOrganizationRating" runat="server" class="modal-dialog modal-lg" style="width:86%" UpdateMode="Conditional" ChildrenAsTriggers="true">
                                            <ContentTemplate>
                                                 <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal" style="font-size: 27px">&times;</button>
                                                        <h4 class="modal-title">How do you want BizAutomation to select your Organization Rating?</h4>
                                                    </div>
                                                    <div class="modal-body">
                                                        <asp:Label ID="lblError" runat="server" Text="" ForeColor="Red"></asp:Label>
                                                        <div class="row padbottom10">
                                                            <div class="col-xs-12">
                                                                <div class="form-inline">
                                                                    <div class="form-group">
                                                                        <label>If the performance (Order Sub-total for period selected) amount for </label>
                                                                        <asp:DropDownList ID="ddlPerformance" runat="server" CssClass="form-control">
                                                                            <asp:ListItem Text="-- Select One --" Value="0" />
                                                                            <asp:ListItem Text="Last 3 MTD" Value="1" />
                                                                            <asp:ListItem Text="Last 6 MTD" Value="2" />
                                                                            <asp:ListItem Text="Last 1 YTD" Value="3" />
                                                                        </asp:DropDownList>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label>&nbsp;is > =&nbsp;</label>
                                                                        <telerik:RadNumericTextBox ID="radORFrom" CssClass="form-control" Width="100" BorderColor="#d2d6de" runat="server" NumberFormat-DecimalDigits="0" />
                                                                
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label>&nbsp;and < =&nbsp;</label>
                                                                        <telerik:RadNumericTextBox ID="radORTo" CssClass="form-control" Width="100" BorderColor="#d2d6de" runat="server" NumberFormat-DecimalDigits="0" />
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label>&nbsp;set Organization Rating to&nbsp;</label>
                                                                        <asp:DropDownList ID="ddlRating" runat="server" CssClass="form-control">
                                                                        </asp:DropDownList>
                                                                    </div>
                                                                    <asp:Button ID="btnAddOrganizationRatingRule" runat="server" CssClass="btn btn-primary" Text="Add"></asp:Button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-xs-12">
                                                                <div class="table-responsive">
                                                                    <asp:GridView ID="gvOrganizationRating" AutoGenerateColumns="false" ShowHeaderWhenEmpty="true" UseAccessibleHeader="true" CssClass="table table-bordered table-striped" runat="server">
                                                                        <Columns>
                                                                            <asp:BoundField HeaderText="Organizatio Rating Rules" DataField="vcOrganizationRatingRule" />
                                                                            <asp:TemplateField ItemStyle-Width="25">
                                                                                <ItemTemplate>
                                                                                    <asp:LinkButton runat="server" ClientIDMode="AutoID" id="lkbDelete" CssClass="btn btn-xs btn-danger" CommandName="Delete" CommandArgument='<%# Eval("numORRID")%>' ><i class="fa fa-trash-o"></i></asp:LinkButton>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                        </Columns>
                                                                        <EmptyDataTemplate>
                                                                            No Data Available
                                                                        </EmptyDataTemplate>
                                                                    </asp:GridView>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                                                        <asp:LinkButton ID="lkbSaveOrganizationRatingRule" runat="server" CssClass="btn btn-primary">Save & Close</asp:LinkButton>
                                                    </div>
                                                </div>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </div>
                                </td>
                            </tr>
                                    <tr>
                                        <td colspan="2" style="width:50%"></td>
                                        <td></td>
                                        <td>
                                            <asp:Label ID="Label24" Text="?" CssClass="tip" runat="server" title="<div  class='innertooltipTitle'><h5>Organization & Contact Duplicates</h5>   Select the fields that define what a duplicate is. For example, if you select �Email�<br/> then when adding a lead or contact a warning message will appear when adding<br/> a new record if the email address belongs to an existing record, however if you<br/> were to select �Email� and �Organization� than those two values would have to<br/> be added to the new record form in order to trigger the warning message<br/> (Duplicate checking is performed when you save or save & close a new record). </div>" />
                                        </td>
                                        <td colspan="2" style="width:50%">
                                            <asp:HyperLink Text="Organization & Contact Duplicates" CssClass="hyperlink" runat="server" ID="hplDupSettings">
                                            </asp:HyperLink>
                                        </td>
                                    </tr>
                        </table>
                            </fieldset>
                            <fieldset>
                                <legend style="margin:auto">Action Items</legend>
                                <table class="table table-responsive tblNoBorder" style="MARGIN-LEFT: 35PX;">
                                    <tr>
                                        <td colspan="2" align="right">
                                            Display Custom Field Section
                                        </td>
                                        <td>
                                            <asp:CheckBox ID="chkCustomFieldSection" runat="server" />
                                        </td>
                                        <td colspan="3" style="font-weight:bold;">
                                            3rd party calendar data inserted in Comments
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" align="right">
                                            Default-check �Follow-up Anytime�
                                        </td>
                                        <td>
                                            <asp:CheckBox ID="chkFollowupAnytime" runat="server" />
                                        </td>
                                        <td colspan="3">
                                             <asp:CheckBox ID="chkTitle" runat="server" />Title &nbsp;
                                              <asp:CheckBox ID="chkLocation" runat="server" />Location &nbsp;
                                                <asp:CheckBox ID="chkDescription" runat="server" />Description
                                        </td>
                                    </tr>
                                </table>
                                <table class="table table-responsive tblNoBorder" style="margin-left:115PX;">
                                    <tr>
                                        <td colspan="7">Turn on and select who should see which action items from their activities type column list:</td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" style="float:right;"><label>Auto-populate users based on permission group</label></td>
                                        <td>
                                            <asp:DropDownList ID="ddlUserGroup" AutoPostBack="true" CssClass="form-control" OnSelectedIndexChanged="ddlUserGroup_SelectedIndexChanged" runat="server"></asp:DropDownList>
                                        </td>
                                         <td colspan="2" style="float:right;display:none"><label>Use Only Action Items</label></td>
                                        <td>
                                            <asp:CheckBox ID="chkActionItems" style="display:none" runat="server"></asp:CheckBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        
                                        <td style="float:right;">
                                            <asp:CheckBox ID="chkPurchaseOrderApproval" runat="server" />
                                        </td>
                                        <td style="float:right;">
                                            Requisition & Purchase Order Approvals:
                                        </td>
                                        <td>
                                            <telerik:RadComboBox ID="rcbPurchaseOrderApproval" runat="server" CheckBoxes="true"></telerik:RadComboBox>
                                        </td>
                                        <td colspan="3" style="float:right;">
                                            Display if Requisition or PO is > $
                                        </td>
                                        <td>
                                            <asp:TextBox runat="server" ID="txtDisplayPO"></asp:TextBox>
                                            <asp:rangevalidator ID="Rangevalidator1" errormessage="Please enter value between 1-9999." forecolor="Red" controltovalidate="txtDisplayPO" minimumvalue="0" maximumvalue="9999" runat="server" Type="Integer">
                                            </asp:rangevalidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        
                                        <td style="float:right;">
                                            <asp:CheckBox ID="chkARInvoiceDue" runat="server" />
                                        </td>
                                        <td style="float:right;">
                                            A/R Invoices Due:
                                        </td>
                                        <td>
                                            <telerik:RadComboBox ID="rcbARInvoiceDue" runat="server" CheckBoxes="true"></telerik:RadComboBox>
                                        </td>
                                        
                                        <td style="float:right;">
                                            <asp:CheckBox ID="chkSalesOrderToClose" runat="server" />
                                        </td>
                                        <td colspan="2" style="float:right;">
                                            Sales Orders to Close:
                                        </td>
                                        <td>
                                            <telerik:RadComboBox ID="rcbSalesOrderToClose" runat="server" CheckBoxes="true"></telerik:RadComboBox>                                            
                                        </td>
                                    </tr>
                                    <tr>
                                        
                                        <td style="float:right;">
                                            <asp:CheckBox ID="chkAPBillsDue" runat="server" />
                                        </td>
                                        <td style="float:right;">
                                            A/P Bills Due:
                                        </td>
                                        <td>
                                            <telerik:RadComboBox ID="rcbAPBillsDue" runat="server" CheckBoxes="true"></telerik:RadComboBox>
                                        </td>
                                        
                                        <td style="float:right;">
                                            <asp:CheckBox ID="chkItemsToPutAway" runat="server" />
                                        </td>
                                        <td colspan="2" style="float:right;">
                                            Items to Put-Away:
                                        </td>
                                        <td>
                                            <telerik:RadComboBox ID="rcbItemsToPutAway" runat="server" CheckBoxes="true"></telerik:RadComboBox>                                            
                                        </td>
                                    </tr>
                                    <tr>
                                        
                                        <td style="float:right;">
                                            <asp:CheckBox ID="chkItemsTOPickPackShip" runat="server" />
                                        </td>
                                        <td style="float:right;">
                                            Items to Pick, Pack & Ship:
                                        </td>
                                        <td>
                                            <telerik:RadComboBox ID="rcbItemsTOPickPackShip" runat="server" CheckBoxes="true"></telerik:RadComboBox>
                                        </td>
                                        
                                        <td style="float:right;">
                                            <asp:CheckBox ID="chkItemsToBill" runat="server" />
                                        </td>
                                        <td colspan="2" style="float:right;">
                                            Items to Bill:
                                        </td>
                                        <td>
                                            <telerik:RadComboBox ID="rcbItemsToBill" runat="server" CheckBoxes="true"></telerik:RadComboBox>                                            
                                        </td>
                                    </tr>
                                    <tr>
                                         <td style="float:right;">
                                            <asp:CheckBox ID="chkItemToInvoice" runat="server" />
                                        </td>
                                        <td style="float:right;">
                                            Items to Invoice:
                                        </td>
                                       
                                        <td>
                                            <telerik:RadComboBox ID="rcbItemToInvoice" runat="server" CheckBoxes="true"></telerik:RadComboBox>
                                        </td>
                                        <td style="float:right;">
                                            <asp:CheckBox ID="chkPOSToClose" runat="server" />
                                        </td>
                                        <td colspan="2" style="float:right;">
                                            POs to Close:
                                        </td>
                                        
                                        <td>
                                            <telerik:RadComboBox ID="rcbPOSToClose" runat="server" CheckBoxes="true"></telerik:RadComboBox>                                            
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" style="float:right;">
                                        </td>
                                        <td>
                                            
                                        </td>
                                        <td style="float:right;display:none">
                                            <asp:CheckBox ID="chlPurchaseorderToClose" runat="server" />
                                        </td>
                                        <td colspan="2" style="float:right;display:none">
                                            Purchase Orders to Close:
                                        </td>
                                        
                                        <td style="display:none">
                                            <telerik:RadComboBox ID="rcbPurchaseorderToClose" runat="server" CheckBoxes="true"></telerik:RadComboBox>                                            
                                        </td>
                                        <td style="float:right;">
                                            <asp:CheckBox ID="chkWorkOrderBOMS" runat="server" />
                                        </td>
                                        <td colspan="2" style="float:right;">
                                            Work Order BOMs to Pick:
                                        </td>
                                        
                                        <td>
                                            <telerik:RadComboBox ID="rcbWorkOrderBOMS" runat="server" CheckBoxes="true"></telerik:RadComboBox>                                            
                                        </td>
                                    </tr>
                                     <tr>
                                        <td colspan="2">
                                            &nbsp;
                                        </td>
                                         
                                    </tr>
                                </table>

                            </fieldset>
                        </center>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>

        </telerik:RadPageView>
        <telerik:RadPageView ID="radPageView_ShippingSettings" runat="server">
            <center><h4 style="margin-bottom: 0px; margin-top: 25px;"><b>SHIPPING SETTINGS</b></h4></center>
            <div class="col-md-3 col-md-offset-9">
                <i>&nbsp;</i>
            </div>
            <asp:Table ID="table13" CellPadding="0" CellSpacing="0" BorderWidth="1" runat="server" Style="border-bottom: 1px; border-left: 1px; border-right: 1px;"
                Width="100%" CssClass="aspTable" BorderColor="black" GridLines="None">
                <asp:TableRow>
                    <asp:TableCell VerticalAlign="Top">
                        <center>
                        <table border="0" class="table table-responsive tblNoBorder" >
                            <tr>
                                <td class="normal1" colspan="2" align="right">Default Shipping Company
                               
                                    <telerik:RadComboBox runat="server" ID="radShipCompany" AccessKey="I" Height="130px" Width="130px" DropDownWidth="130px"
                                        EnableVirtualScrolling="false" ChangeTextOnKeyBoardNavigation="false" ClientIDMode="Static">
                                    </telerik:RadComboBox>
                                </td>
                                <td align="left">
                                    <%--<asp:Label ID="Label20" Text="?" CssClass="tip" runat="server" title="Allows you to Pre-select default shippping company in BizDoc Edit page when you want to manually generating shipping label for selected bizdoc." />--%>
                                </td>
                                <td align="left">
                                    <%--<asp:Label ID="Label45" Text="?" CssClass="tip" runat="server" title="Allows you to Pre-select default shippping company in BizDoc Edit page when you want to manually generating shipping label for selected bizdoc." />--%>
                                </td>
                                <td  colspan="2">
                                    <asp:CheckBox runat="server" ID="chkUseBizdocAmount" Text="Use Bizdoc Amt. as Total Insured & Customs Value" />
                                </td>
                            </tr>
                            <tr>
                                <td class="normal1" colspan="2" align="right">
                                    Total Insured Value
                                    <asp:TextBox ID="txtTotalInsuredValue" CssClass="required_decimal {required:false ,number:true, messages:{required:'Please provide value!',number:'Provide valid value!'}}" runat="server" Width="50">
                                    </asp:TextBox>
                                    Shipping Label Image Width
                                    <asp:TextBox ID="txtWidth" CssClass="required_integer {required:false ,number:true, messages:{required:'Please provide value!',number:'Provide valid value!'}}" runat="server" Width="50">
                                    </asp:TextBox>
                                </td>
                                <td>
                                    <%--<asp:Label ID="Label50" Text="?" CssClass="tip" runat="server" title="Allows you to Pre-select default shippping company in BizDoc Edit page when you want to manually generating shipping label for selected bizdoc." />--%>
                                </td>
                                <td>
                                    <%--<asp:Label ID="Label54" Text="?" CssClass="tip" runat="server" title="Allows you to Pre-select default shippping company in BizDoc Edit page when you want to manually generating shipping label for selected bizdoc." />--%>
                                </td>
                                <td class="normal1" align="left" colspan="2">Shipping Label Image Height
                                
                                    <asp:TextBox ID="txtHeight" CssClass="required_integer {required:false ,number:true, messages:{required:'Please provide value!',number:'Provide valid value!'}}" runat="server" Width="50">
                                    </asp:TextBox>Total Customs Value
                                    <asp:TextBox ID="txtTotalCustomsValue" CssClass="required_decimal {required:false ,number:true, messages:{required:'Please provide value!',number:'Provide valid value!'}}" runat="server" Width="50">
                                    </asp:TextBox>
                                </td>
                            </tr>
                            <tr style="display:none">
                                <td class="normal1" align="right" colspan="2">
                                </td>
                                <td>
                                    <asp:Label ID="Label51" Text="?" CssClass="tip" runat="server" title="Allows you to Pre-select default shippping company in BizDoc Edit page when you want to manually generating shipping label for selected bizdoc." />
                                </td>
                                <td>
                                    <asp:Label ID="Label52" Text="?" CssClass="tip" runat="server" title="Allows you to Pre-select default shippping company in BizDoc Edit page when you want to manually generating shipping label for selected bizdoc." />
                                </td>
                                <td class="normal1" align="left" colspan="2" >
                                </td>
                            </tr>
                            <tr>
                                <td class="normal1" align="right" valign="top" colspan="2">
                                    <asp:CheckBox runat="server" TextAlign="Left" ID="chkDefaultRateType" Text="Default Rate Type for Shipping Rates" />
                                </td>
                                <td>
                                    <%--<asp:Label ID="Label53" Text="?" CssClass="tip" runat="server" title="Allows you to Pre-select default shippping company in BizDoc Edit page when you want to manually generating shipping label for selected bizdoc." />--%>
                                </td>
                                <td>
                                    <%--<asp:Label ID="Label81" Text="?" CssClass="tip" runat="server" title="Shipping Service Item" />--%>
                                </td>
                                <td class="normal1" align="left" colspan="2">
                                    Default Item used for Shipping within Orders
                                        <asp:DropDownList ID="ddlShippingServiceItem" runat="server" CssClass="signup" Width="100px">
                                    </asp:DropDownList>
                                </td>   
                            </tr>
                            <tr>
                                <td class="normal1" align="right" colspan="2">Printer IP Address <asp:TextBox ID="txtPrinterIP" runat="server" Width="150"></asp:TextBox></td>
                                <td></td>
                                <td></td>
                                <td class="normal1" align="left" colspan="2" >Printer Port <asp:TextBox ID="txtPrinterPort" runat="server" Width="50"></asp:TextBox>
                                    &nbsp;&nbsp;<a href="../Items/frmShipingCompany.aspx" target="_blank">Manage Shipping Rule</a>

                                </td>
                            </tr>
                        </table>

                              <telerik:RadTabStrip style="display:none" ID="radStripShipping" runat="server" UnSelectChildren="True" EnableEmbeddedSkins="true"
                Skin="Default" ClickSelectedTab="True" MultiPageID="radMUltipage_ShippingManagement" SelectedIndex="0">
                <Tabs>
                    <telerik:RadTab Text="Parcel Shipping (FedEx, UPS, USPS)" Value="Parcel_Shipping" PageViewID="radPageView_Parcel_Shipping">
                    </telerik:RadTab>
                    <telerik:RadTab Text="Static Shipping-rate Tables & Promotions" Value="Static_Shipping" PageViewID="radPageView_Static_Shipping">
                    </telerik:RadTab>
                </Tabs>
            </telerik:RadTabStrip>
            <telerik:RadMultiPage style="display:none" ID="radMUltipage_ShippingManagement" runat="server" SelectedIndex="0" CssClass="pageView">
                <telerik:RadPageView ID="radPageView_Parcel_Shipping" runat="server">
                    <div class="row">
        <div class="pull-right" style="padding-bottom:5px; padding-right:10px;" >
            <asp:LinkButton runat="server" id="btnSaveParcelShipping" OnClick="btnSaveParcelShipping_Click" class="btn btn-primary">&nbsp;&nbsp;Save</asp:LinkButton>  &nbsp; 
        </div>
        <div class="col-xs-12">
            <div class="table-responsive">
                <table style="width:100%; ">
                    <tr>
                        <td style="Width:25%; vertical-align:top;">
                            <asp:DataGrid ID="dgShipcmp" AllowSorting="true" runat="server" CssClass="table table-bordered table-striped" AutoGenerateColumns="False" UseAccessibleHeader="true">
                                <Columns>
                                    <asp:BoundColumn Visible="False" DataField="numListItemID"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="vcData" visible="false" HeaderText="Shipping Company"></asp:BoundColumn>
                                    <asp:TemplateColumn HeaderStyle-Width="20%" ItemStyle-Width="10%" >
                                        <HeaderTemplate>
                                            <table style="width:100%">
                                                <tr>
                                                    <td>
                                                        <label>Ship-Via <%--<asp:Label ID="Label88" Text="?" CssClass="tip" runat="server" title="To edit this list go to �Add & Edit Drop-Down Lists� from the admin section. Select the �Opportunities & Orders� module and �Ship Via� from the list drop down." />--%></label>
                                                    </td>
                                                </tr>
                                            </table>                                 
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <table style="width:100%">
                                                <tr>
                                                    <td style="width:80%">
                                                        <asp:Label ID="lblShipCmp" Text='<%#Eval("vcData") %>' runat="server"></asp:Label>
                                                    </td>
                                                    <td style="width:20%">
                                                        <asp:HyperLink ID="hplEditData"  ImageUrl="~/images/edit.png" runat="server"></asp:HyperLink>
                                                    </td>
                                                </tr>
                                            </table>                                 
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn Visible="false">
                                        <ItemTemplate>
                                            <asp:HyperLink ID="hlData" Text='<%#Eval("vcData") %>' runat="server"></asp:HyperLink>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <%--<asp:BoundColumn Visible="False" DataField="numListItemID"></asp:BoundColumn>
                                    <asp:HyperLinkColumn NavigateUrl="#" DataTextField="vcData" HeaderText="Shipping Company"></asp:HyperLinkColumn>
                                    <asp:TemplateColumn Visible="false">
                                        <ItemTemplate>
                                            <asp:HyperLink ID="hlData" Text='<%#Eval("vcData") %>' runat="server"></asp:HyperLink>
                                        </ItemTemplate>
                                    </asp:TemplateColumn> --%>                       
                                </Columns>
                            </asp:DataGrid>
                        </td>
                        <td style="width:3%"> &nbsp;</td>
                        <td style="Width:72%;  vertical-align:top;">
                            <asp:Panel ID="pnlRealTimeShippingQuotes" runat="server">
                                <asp:DataGrid ID="dgRealTimeShippingQuotes" AllowSorting="True" runat="server" CssClass="table table-striped table-bordered" UseAccessibleHeader="true"
                                    AutoGenerateColumns="False" HorizontalAlign="Left" CellSpacing="2"
                                    CellPadding="2">
                                    <AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
                                    <ItemStyle CssClass="is"></ItemStyle>
                                    <HeaderStyle CssClass="hs" Wrap="false"></HeaderStyle>
                                    <Columns>
                                        <asp:BoundColumn Visible="False" DataField="numServiceTypeID"></asp:BoundColumn>
                                        <asp:TemplateColumn>
                                            <HeaderTemplate>
                                                <asp:CheckBox ID="chkAllServicetype" runat="server" onclick="SelectAll('chkAllServicetype','chk')"
                                                    Text="Enabled " /><%--<asp:Label ID="Label88" Text=" ?" CssClass="tip" runat="server" title="Selecting to enable a shipping service adds that shipping service to the list of live rates you can obtain while building an order internally or on the web-store. Keep in mind that the more services you enable, the longer it takes to get back a rate from the live query. Too many rates and thus wait time, can have a deleterious affect on your customer�s check-out experience." />--%>
                                            </HeaderTemplate>
                                            <HeaderStyle HorizontalAlign="Left" />
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chk" CssClass="chk" runat="server" Checked='<%# Eval("bitEnabled") %>' />
                                                <asp:Label Text='<%# Eval("intNsoftEnum") %>' runat="server" ID="lblNSoftEnm" Style='display: none;' />
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="Shipping Carrier Service" ItemStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <asp:Label Text='<%# Eval("vcServiceName") %>' runat="server" ID="lblServiceName" />
                                                <asp:TextBox runat="server" ID="txtShippingCaption" Text='<%# Eval("vcServiceName") %>'
                                                    CssClass="form-control" />
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="From" ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <asp:TextBox runat="server" ID="txtFrom" Text='<%#Eval("intFrom")%>' CssClass="form-control"
                                                    onkeypress="CheckNumber(2,event)" Width="75px" />
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="To" ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <asp:TextBox runat="server" ID="txtTo" Text='<%#Eval("intTo")%>' CssClass="form-control"
                                                    onkeypress="CheckNumber(2,event)" Width="75px" />
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="MarkUp" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="120px">
                                            <HeaderTemplate>
                                                <span style="float: left">MarkUp</span> <span style="float: right">
                                                    <asp:CheckBox ID="chkIsPercentage" Text="" runat="server" onclick="SelectAll('chkIsPercentage','chkIsPercentageClass')" /></span>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <div class="form-inline">
                                                    <asp:TextBox runat="server" ID="txtMarkup" Text='<%# Eval("fltMarkup") %>' CssClass="form-control"
                                                        onkeypress="CheckNumber(1,event)" Width="50px" />
                                                    <label for="chkIsPercentage" runat="server" id="lblIsPercentage">
                                                        Is %</label><asp:CheckBox class="chkIsPercentageClass" Text="" runat="server" ID="chkIsPercentage"
                                                            Checked='<%# Eval("bitMarkupType") %>' />
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="Rate" ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <asp:TextBox runat="server" ID="txtRate" Text='<%# Eval("monRate") %>' CssClass="form-control"
                                                    onkeypress="CheckNumber(1,event)" Visible="false" Width="50px" />
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderStyle-Width="10" FooterStyle-Width="10" ItemStyle-Width="10">
                                            <ItemTemplate>
                                                <asp:Button ID="btnDelete" runat="server" CssClass="btn btn-danger" Text="X" CommandName="Delete"
                                                    Visible="false" CommandArgument='<%# Eval("numServiceTypeID") %>' OnClientClick="return DeleteRecord()"></asp:Button>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                    </Columns>
                                </asp:DataGrid>
                            </asp:Panel>
                        </td>
                    </tr>
                </table>
                

                
            </div>
        </div>
    </div>
                </telerik:RadPageView>
                 <telerik:RadPageView ID="radPageView_Static_Shipping" runat="server">
                     <div class="">
        <div class="" id="Div1" runat="server">
            <table width="100%">
                <tr>
                    <td>
                        <table>
                        <tr>
                            <td>
                                <asp:Label ID="lbl1" runat="server" Text="If any discount causes shopping cart to be =< $0 add the following minimum shipping cost:"></asp:Label>&nbsp;
                                <asp:TextBox ID="txtminShippingCost" runat="server"></asp:TextBox>&nbsp;
                            </td>
                            <td>
                                <asp:Label ID="lblRelProfile" runat="server" Text="Relationships / Profiles "></asp:Label>&nbsp;
                                <asp:DropDownList ID="ddlRelProfile" Width="250px" runat="server" CssClass="form-control" style="display:unset;" autopostback="true" OnSelectedIndexChanged="ddlRelProfile_SelectedIndexChanged" ></asp:DropDownList>&nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:CheckBox ID="chkbxExcludeClassifications" runat="server" />&nbsp;
                               Exclude items in the <a href="#" onclick="return OpenClassificationsWindow();">following classifications.</a>
                            </td>
                        </tr>
                        </table>
                    </td>
                    <td style="vertical-align:top; float:right;">
                        <div class="pull-right" >
                       <%-- <uc1:SiteSwitch ID="SiteSwitch1" runat="server" />--%>
                                &nbsp;
                        <asp:LinkButton runat="server" id="btnStaticShippingSave" OnClick="btnStaticShippingSave_Click" class="btn btn-primary">&nbsp;&nbsp;Save</asp:LinkButton>  &nbsp; 
                        <asp:LinkButton runat="server" OnClientClick="return New();" id="btnNew" class="btn btn-primary"><i class="fa fa-plus-circle"></i>&nbsp;&nbsp;New Shipping Rule</asp:LinkButton>
                        
                        <a href="#" class="help">&nbsp;</a>
                    </div>
                    </td>
                </tr>
            </table>             
        </div>
                         <div class="col-md-12 table-responsive">
    <table cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td>
                 <asp:CheckBox ID="chkbxEnableStaticShip" runat="server" />
              <label>  Enable Static Shipping Rules based on Order-Amounts <asp:Label ID="Label95" Text="?" CssClass="tip" runat="server" title="Checking this box means that BizAutomation will automatically add the shipping language to new orders, and will automatically add the shipping line item when an order is created internally or externally (via the web store), with the amounts calculated from the shipping rule. " /></label>
            </td>
        </tr>
        <tr>
            <td> 
                <asp:DataGrid ID="dgShippingRuleList" runat="server" Width="100%" CssClass="table table-striped table-bordered" UseAccessibleHeader="true" AutoGenerateColumns="False">
                    <AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
                    <ItemStyle CssClass="is"></ItemStyle>
                    <HeaderStyle CssClass="hs" Wrap="false"></HeaderStyle>
                    <Columns>
                        <asp:BoundColumn Visible="False" DataField="numRuleID"></asp:BoundColumn>
                        <asp:ButtonColumn DataTextField="vcRuleName" CommandName="RuleID" HeaderStyle-Width="15%" HeaderText="<font>Rule Name</font>">
                        </asp:ButtonColumn>
                         <asp:BoundColumn  DataField="vcShipFrom" HeaderText="Ship-From" HeaderStyle-Width="15%" ></asp:BoundColumn>
                         <asp:BoundColumn  DataField="vcShipTo" HeaderText="Ship-To" HeaderStyle-Width="28%" ></asp:BoundColumn>
                         <asp:BoundColumn  DataField="vcShippingCharges" HeaderText="Shipping Charges" HeaderStyle-Width="25%" ></asp:BoundColumn>
                         <asp:BoundColumn  DataField="vcAppliesTo" HeaderText="Applies To" HeaderStyle-Width="17%" ></asp:BoundColumn>
                        <asp:TemplateColumn HeaderText ="Shipping Method" Visible="false"> 
                         <ItemTemplate >
                            <asp:Label Text='<%# IIf(Eval("tinShippingMethod")="1","Fixed Shipping Quotes","Real Time Shipping Quotes") %>' runat="server" ID="lblShippingMethod" />
                         </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText ="Fixed shipping Quotes Mode" Visible="false"> 
                         <ItemTemplate >
                            <asp:Label Text='<%# IIf(Eval("tinShippingMethod")="2","-",Eval("vcFixedShippingQuotesMode")) %>' runat="server" ID="lblFixedShippingQuotes" />
                         </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:BoundColumn DataField="vcTaxMode" Visible="false"  HeaderText="<font>Tax Mode</font>">
                        </asp:BoundColumn>
                        <%--<asp:BoundColumn HeaderText="Item it applies to" DataField="ItemList"></asp:BoundColumn>
                        <asp:BoundColumn HeaderText="Customer it applies to" DataField="CustomerList"></asp:BoundColumn>--%>
                        <asp:TemplateColumn>
                            <ItemStyle width="10px"/>
                            <ItemTemplate>
                                <asp:Button ID="btnDelete" runat="server" CssClass="btn btn-danger" Text="X" CommandName="Delete"></asp:Button>
                                <asp:LinkButton ID="lnkdelete" runat="server" Visible="false">
								<font color="#730000">*</font></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                </asp:DataGrid>
            </td>
        </tr>
    </table>
        </div>
    </div>
                     <div class="clearfix"></div>
                </telerik:RadPageView>
                </telerik:RadMultiPage>
                        </center>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </telerik:RadPageView>
        <telerik:RadPageView ID="radPageView_CompaniesContacts" runat="server">
            <center><h4 style="margin-bottom: 30px; margin-top: 25px;"><b>ORGANIZATION & CONTACTS DEFAULTS</b></h4></center>
            <asp:Table ID="table3" CellPadding="0" CellSpacing="0" BorderWidth="1" runat="server"
                Width="100%" CssClass="aspTable" BorderColor="black" GridLines="None" Height="350">
                <asp:TableRow>
                    <asp:TableCell VerticalAlign="Top">
                        <center>
                        </center>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </telerik:RadPageView>
        <telerik:RadPageView ID="radPageView_OrderManagement" runat="server">
            <center><h4 style="margin-bottom: 0px; margin-top: 25px;"><b>GLOBAL ORDER SETTINGS</b></h4></center>
            <div class="col-md-3 col-md-offset-9">
                <i>
                    <a id="hplExportData" class="hyperlink" href="frmExportData.aspx" style="color: #000;">&nbsp;</a></i>
            </div>
            <asp:Table ID="table2" CellPadding="0" CellSpacing="0" BorderWidth="1" Style="border-bottom: 1px; border-left: 1px; border-right: 1px;"
                runat="server"
                Width="100%" CssClass="aspTable" BorderColor="black" GridLines="None">
                <asp:TableRow>
                    <asp:TableCell VerticalAlign="Top">
                        <center>
                                <table class="table table-responsive tblNoBorder">
                                    <tr>
                                        <td align="right" colspan="2" style="width:50%">
                                            <b>Item:</b> 
                                            <asp:HyperLink Text=" Column Display Configuration" CssClass="hyperlink" runat="server" ID="hplSettings"
                                                onclick="return OpenSettings('0','22');"></asp:HyperLink>
                                            &nbsp;
                                            &nbsp; <asp:HyperLink Text="Field Search Configuration" Style="margin-left: 2px;" CssClass="hyperlink" runat="server" ID="hplSearch"
                                                onclick="return OpenSettings('1','22');"></asp:HyperLink>
                                        </td>
                                        <td>
                                            <asp:Label ID="Label48" Text="?" CssClass="tip" runat="server" title="<div  class='innertooltipTitle'><h5>Column Display & Field Search Configuration</h5>Use these settings to configure which fields display as well as which values<br/>are used when searching and filtering items via look-ahead fields such as<br/>those used to add items to sales orders and purchase orders.</div>" />
                                        </td>
                                        <td>
                                            <asp:Label ID="lbltitleBackOrder" Text="?" CssClass="tip" runat="server" title="<div  class='innertooltipTitle'><h5>Back-Order rule (Internal sales orders only)</h5>By default BizAutomation allows inventory backorders on internal Sales Orders  (Items <br/>do not go negative, rather the backorder count will go positive) but not on Biz- <br/>Commerce sites.  You can reverse this for all items added to internal sales orders using<br/>this tool. To enable backorder on your Biz-Commerce site(s), you�ll need to (A).  open<br/>the Ecommerce tab within the item record and select the �Allow backorder in e-<br/>Commerce� check box on an item by item basis (B). batch update by using the mass<br/>update function button from an item search result in advanced search, or (C). Use<br/>mass update via the import & update wizard. </div>" />
                                        </td>
                                        <td colspan="2" style="width:50%">
                                            <asp:HyperLink Text="Back-Order rule" Style="text-decoration:underline;" onclick="OpenBackOrderSettings();" CssClass="hyperlink" runat="server" ID="hplBackOrderRule"></asp:HyperLink><i> (Internal sales orders only)</i>
                                        </td>
                                       
                                    </tr>


                                    <tr>
                                        <td colspan="2" align="right">
                                            <b>Item:</b>  Min characters to use when looking up an item
                                        
                                            <asp:DropDownList ID="DropDownList2" runat="server">
                                        <asp:ListItem Value="0">0</asp:ListItem>
                                        <asp:ListItem Value="1">1</asp:ListItem>
                                        <asp:ListItem Value="2">2</asp:ListItem>
                                        <asp:ListItem Value="3">3</asp:ListItem>
                                        <asp:ListItem Value="4">4</asp:ListItem>
                                        <asp:ListItem Value="5">5</asp:ListItem>
                                    </asp:DropDownList>
                                        </td>
                                        <td>
                                            <asp:Label ID="Label69" Text="?" CssClass="tip" runat="server" title="<div  class='innertooltipTitle'>When creating an order, adding characters into the item field in order for the system<br/> to select an item for the order is required. This sets how many characters are<br/> required before the system attempts to guess which item you�re trying to add to the order<br/> (e.g. If setting is 0 or 1 then typing �G� will find all items beginning with �G�, if it�s 2, Biz will require 2 characters,<br/> so typing �G� will result in nothing, but typing �Gr� will result in Biz trying to find all items beginning with �Gr�.).</div>" />

                                        </td>
                                        <%--<td colspan="2" align="right">
                                            <b>Item:</b>
                                        </td>
                                        <td>
                                            <asp:Label ID="Label67" Text="?" CssClass="tip" runat="server" title="This enables you to set the Item fields on which you want to search when looking up an Item. For example, say you want to find ABC Item, which has a custom field value of 123� by selecting that custom field as one of the search fields, you would be able to find ABC Item by entering 123 if that custom field was selected in this configuration" />
                                        </td>--%>
                                        <td>
                                            <asp:Label ID="Label67" Text="?" CssClass="tip" runat="server" title="<div  class='innertooltipTitle'><h5>Commit Allocation</h5>Allocation is referred to in some systems as �Reserved Stock�<br/>which reduces the �Available� but not the �On-Hand� count.<br/><span class='subheading'>When Sales Order is Created</span> (Default Setting): Self explanatory.<br/><span class='subheading'>When Fulfillment Order</span> (Packing Slip) is created: A Fulfillment Order is the BizDoc<br/>responsible for releasing allocation, and can be called (If labeled as such) the �Packing<br/>Slip�. If this setting is selected, it means that you don�t want to reserve inventory for<br/>customers, and only want to use On-Hand qty (This setting renders the Available and<br/>Allocation counts effectively meaningless).</div>" />
                                        </td>
                                        <td colspan="2">
                                            <b>Commit allocation:</b> <asp:DropDownList ID="ddlCommitAllocation" runat="server">
                                                <asp:ListItem Text="When Sales Order is created" Value="1" />
                                                <asp:ListItem Text="When Fulfillment Order (Packing Slip) is added" Value="2" />
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        
                                        <td colspan="2"  align="right"><b>Allow duplicate line items</b> <asp:CheckBox ID="chkAllowDuplicateLineItems" runat="server" /></td>
                                        <td>
                                            <asp:Label ID="Label76" Text="?" CssClass="tip" runat="server" title="This will allow users to add same item(s) multiple times to order/opportunity." />

                                        </td>
                                        <td>
                                             <asp:Label ID="Label41" Text="?" CssClass="tip" runat="server" title="<div  class='innertooltipTitle'>	<h5>Units of Measures (UOM)</h5>	<span class='subheading'>Definitions</span>- If you don�t find a definition you need, you can relabel or add your own as <br/> 	well as remove an existing definition that you don�t want to populate on the list.<br/>     <span class='subheading'>Relationships</span>- Use this feature to setup how UOMs relate to one another in terms of <br/> 	quantities (e.g. 1 �Six Pack� = 1 �Bottle�). BizAutomation will automatically calculate <br/>	sale and purchase UOMs based on definitions and relationship formulas you setup.</div>" />
                                        </td>
                                        <td colspan="2"><b>Units of Measure (UOM): </b>   <asp:HiddenField ID="hdnUnitSystem" runat="server" />
                                            &nbsp;<asp:HyperLink Text="Definations" CssClass="hyperlink" runat="server" ID="hplUnit"></asp:HyperLink>
                                            <span style="font-style:Bold;">|</span>
                                            <asp:HyperLink Text="Relationships" CssClass="hyperlink" runat="server" ID="hpUOMConversion"></asp:HyperLink></td>
                                    </tr>

                                    <tr>
                                        <td colspan="2" align="right"><b>New sales opp/order default settings </b> 
                                            <asp:DropDownList ID="ddlMarkupDiscountOption" runat="server">
                                                <asp:ListItem Text="- Discount" Value="0"></asp:ListItem>
                                                <asp:ListItem Text="+ Markup" Value="1"></asp:ListItem>
                                            </asp:DropDownList>&nbsp;
                                              <asp:DropDownList ID="ddlMarkupDiscountValue" runat="server">
                                                <asp:ListItem Text="%" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="Amount Entered" Value="2"></asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                        <td> <asp:Label ID="lblNewSalestitle" Text="?" CssClass="tip" runat="server" title="<div  class='innertooltipTitle'>   When you create a sales opportunity or order, you have the option to discount<br/> from list, or markup from cost. If the markup option is selected,<br/> it will use the �Default Cost� basis selected within <br/>Admin | Global Settings | Order Mgt. which will now be reflected within the <br/>�Cost� field within the new opp./order form. <br/>If cost basis is 0.00, the only usable option will be<br/> �Amount Entered� as its value becomes the named unit price for that item.</div>" />                                         
                                        </td>
                                        <%--<td>
                                            <asp:Label ID="Label73" Text="?" CssClass="tip" runat="server" title="This will remove the Global Warehouse from your item inventory," />
                                        </td>--%>
                                        <td><asp:Label ID="Label113" Text="?" CssClass="tip" runat="server" title="Auto-Assign Serial #s to serialized items on Sales Orders" />
                                            </td>
                                        <td style="display:none">
                                            <asp:CheckBox ID="chkRemoveGlobalWarehouse" Text="" runat="server" /> <b>Do not use Global Warehouses</b>&nbsp; &nbsp;
                                            <asp:Label ID="Label72" Text="?" CssClass="tip" runat="server" title="If you create sales orders for serialized items, and you sell these items in sequence, meaning that you can predict the serial numbers that will be added, this option automates the assignment of serial numbers to these items, which avoids having to manually add them later to the order later, a big time saver." />
                                            </td>
                                        <td>
                                            <asp:CheckBox ID="chkAutoSerialNoAssign" runat="server" Text="Auto-Assign Serial #s to serialized items on Sales Orders" />
                                        </td>
                                    </tr>
                                    <tr>     
                                        <td colspan="2" align="right">
                                            <asp:CheckBox ID="chk3PL" runat="server" Text="Receive External Sales Orders"/>&nbsp;&nbsp;
                                            <asp:CheckBox ID="chkEDI" runat="server" Text="External Ship & ASN Request" />
                                        </td>
                                        <td>
                                            <asp:Label ID="Label26" Text="?" CssClass="tip" runat="server" title="<div  class='innertooltipTitle'>	<h5>Receive External Sales Orders</h5>	This checkbox will be selected by BizAutomation if you sign up for 3rd party<br/>	marketplace and / or EDI integration (this is an add-on service). It enables<br/>	BizAutomation to receive data feeds from 3rd party marketplaces via eChannelhub and<br/>	big box retail organizations via EDI integration hubs.</div><div  class='innertooltipTitle'>	<h5>External Ship & ASN Request</h5>	This checkbox will be selected by BizAutomation if you sign up for 3rd party 3PL<br/>	integration (this is an add-on service). </div>" />
                                        </td>
                                        <td>
                                            <asp:Label ID="Label73" Text="?" CssClass="tip" runat="server" title="<div  class='innertooltipTitle'>	<h5>Marketplaces</h5>	BizAutomation connects to marketplaces via eChannelhub, but they have to be selected from this list (which<br/>	also sets the values within the �Sources� drop down within the order record). Rules by which orders are<br/>	generated from marketplaces, must also be configured from within this section.<br/></div>" />
                                        </td>
                                        <td colspan="2" align="left">
                                            <b>Marketplaces:</b> <a href="javascript:OpenModalEChannelhub();">Configure</a>
                                        </td>
                                        
                                    </tr>
                                    <tr>
                                        <td colspan="2" align="right">
                                            <b>New Sales Order: </b> 
                                            <asp:HyperLink Text="Form Customization" CssClass="hyperlink" runat="server" ID="HyperLink3" Target="_blank" NavigateUrl="~/opportunity/frmConfNewOrder.aspx"
                                                ></asp:HyperLink>&nbsp;&nbsp;
                                            <asp:Label ID="Label114" Text="?" CssClass="tip" runat="server" title="Form Customization" />&nbsp;&nbsp;
                                       <asp:HyperLink Text="Apply Minimum Order Amounts" CssClass="hyperlink" Target="_blank" runat="server" ID="HyperLink4"
                                                NavigateUrl="~/admin/frmMasterList.aspx?ListId=21"></asp:HyperLink>
                                        </td>
                                        <td> 
                                            <asp:Label ID="Label115" Text="?" CssClass="tip" runat="server" title="<div class='innertooltipTitle'><h5>Apply Minimum Order Amounts</h5>This feature lets you set minimum amounts required per order based on order sub-<br/>total by organization profile (Customers can have all the profiles you wish).<br/><br/> E.g. - Say your organization profiles include something like �Small Distributor� and<br/> �Large Distributor�,  and you want to require minimum amount per order for all<br/> small distributors of $100, and $1,000 for large distributors.</div>" />&nbsp;&nbsp;
                                        </td>
                                        <td>

                                        </td>
                                        <td colspan="2" align="left">
                                            <asp:CheckBox ID="chkDoNotShowDropshipWindow" runat="server" />
                                            After sales order is created, don�t open back-order or drop-ship items popup window

                                        </td>
                                    </tr>



                                    <tr style="display:none">
                                        
                                        <td>
                                        </td>
                                        <td colspan="2"><b>Default e-Commerce site for Add to Cart:</b>
                                        <asp:DropDownList ID="ddlSites" runat="server" ></asp:DropDownList>
                                        </td>
                                    </tr>
                                    
                                    <tr style="display:none">
                                        
                                        <td>
                                            <a data-toggle="title" data-html="true" data-placement="top" title="The following only applies to outbound messages sent from within Sales or Purchase Opportunities and/or Orders. Selecting this check box does the following:<br/><br/>1. Every time you email out a BizDoc by clicking the icon to open a compose message window with the BizDoc attached as a PDF, when you click �Send�, BizAutomation will put the TO and CC email addresses in a temporary table for that organization.<br/><br/>2. The NEXT time you send a BizDoc of this type (e.g. Invoice, Quote, etc..) from a record (i.e. Opportunity or Order) these same email addresses will be used within the same TO and CC fields as used in the previous Opp/Order record (Note - each BizDoc type will have its own unique set of email addresses, so that clicking an invoice for example can yield a separate set of email addresses vs a Quote or Sales Order).">?</a>
                                        
                                    </tr>
                                    <tr style="display:none">
                                        
                                        <td>
                                           <br /><br /> <asp:Label ID="Label32" Text="?" CssClass="tip" runat="server" title="When creating a P.O. from S.O., set the default Bill To address to" />
                                            </td>
                                        
                                    </tr>
                                   <tr style="display:none">
                                       <td></td>
                                       <td></td>
                                       <td><asp:Label ID="Label94" Text="?" CssClass="tip" runat="server" title="Select Purchase BizDoc responsible for Shipping Labels & Trackings #" /></td>
                                       <td colspan="2">Select Purchase BizDoc responsible for Shipping Labels & Trackings #
                                             <asp:DropDownList ID="ddlDefaultPurchaseBizDoc" Width="120px" runat="server"
                                                CssClass="signup">
                                            </asp:DropDownList></td>
                                   </tr>
                                    <tr style="display:none">
                                        
                                        <td>
                                            <asp:Label ID="Label100" Text="?" CssClass="tip" runat="server" title="Employer will be used for when creating P.O. from short-cut bar" /></td>
                                        
                                    </tr>
                                    <tr style="display:none">
                                        <td colspan="2"  align="right">
                                            <%--<asp:CheckBox ID="chkRemoveVendorPOValidation" TextAlign="Left" runat="server" Text="Allow ANY item to be added to a PO (Not just items vended by the PO's Vendor)" />--%></td>
                                        <td>
                                            <asp:Label ID="Label40" Text="?" CssClass="tip" runat="server" title="Normally, when creating purchase order record which require that you enter a vendor name first, Biz will only allow items be added that are previously set up for that vendor within item details. Checking this will remove that validation. " />
                                        </td>
                                        
                                    </tr>
                                    <tr style="display:none">
                                        
                                        <td>          <asp:Label ID="Label47" Text="?" CssClass="tip" runat="server" title="This tool is used to define the look and feel of each BizDoc, and is endlessly configurable via HTML and CSS templates. You can create multiple templates for each BizDoc, including which fields should appear on the upper section of the document (including custom fields). We suggest that only users with web design experience attempt to modify BizDocs using this tool. Modifying BizDoc columns and footers is accomplished from within the �BizForms Wizard� section of administration." /></td>
                                        <td colspan="2">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                              <asp:Label ID="Label66" Text="?" CssClass="tip" runat="server" title="When selecting from your list of BizDocs this filter lets you control which BizDocs actually appear on the list (you can create BizDoc names from Master List Admin)." />&nbsp;
                                            	
                                        </td>                                        
                                    </tr>
                                    <tr style="display:none">
                                        
                                     <td>
                                            <asp:Label ID="Label92" Text="?" CssClass="tip" runat="server" title="Selecting the �Flat� radio-button will enable the first of any of the approvers selected to approve the submission where as a �Hierarchical� selection will require that ALL approvers submit their approval before the Opportunity Promotion submission is considered �Approved�." />
                                        </td>
                                        <td colspan="2">
                                            <b>Approval Process:</b>
                                            <asp:RadioButtonList ID="rdnPromotionApproval" runat="server" RepeatDirection="Horizontal">
                                                <asp:ListItem Value="1" Selected="True">Flat</asp:ListItem>
                                                <asp:ListItem Value="2">Hierarchical</asp:ListItem>
                                             </asp:RadioButtonList> 
                                        </td>
                                       
                                    </tr>
                                </table>
                                <fieldset>
                                    <legend style="    margin: auto;">BizDocs</legend>
                                    <table class="table table-responsive tblNoBorder">
                                          <tr>
                                              <td colspan="2" align="right" class="normal1" style="width:50%">
                                                  Select Sales BizDoc responsible for Shipping Labels & Trackings #
                                                    <asp:DropDownList ID="ddlDefaultSalesBizDoc" Width="200px" runat="server"
                                                        CssClass="signup">
                                                    </asp:DropDownList>
                                              </td>
                                               <td>
                                                   <asp:Label ID="Label93" Text="?" CssClass="tip" runat="server" title="<div  class='innertooltipTitle'>	<h5>BizDoc responsible for Shipping Labels & Tracking # </h5>	Because each sales order can have multiple ship dates and thus release from allocation<br/>	dates (we collectively refer to these as �Release Dates�) each with their own BizDoc<br/>	(invoice, packing slip, etc..) one of these BizDocs has to be designated as the one<br/>	containing the labels and tracking #s. If you invoice before you ship and want to print<br/>	labels and tracking #s as early as possible after the invoice is generated, you might want<br/>	to select this BizDoc, but keep in mind that there may be delay between when the<br/>	numbers are generated and when you ultimately pick, pack, and ship product, so more<br/>	often than not we recommend you select the packing slip or fulfillment order (a<br/>	fulfillment order triggers the release of allocation and can act as the packing slip or be<br/>	used separately from the packing slip if so desired).</div>" />
                                               </td>
                                              <td>
                                                   <asp:Label ID="Label116" Text="?" CssClass="tip" runat="server" title="<div  class='innertooltipTitle'><h5>BizDoc Design Templates:</h5>BizDocs are public facing documents derived from order data such as Invoices, Quotes,<br/>and Purchase Orders. Use this section to:<br/><br/> (1). Change the labeling, style / theme / skin / format (using HTML and CSS code).<br/>(2). Add merge fields from object values (Items, Orders, Organizations, and Contacts). <br/>(3). Change default document labels (e.g. change the �Quote� to �Estimate�).<br/>(4). Need multiple versions of the same BizDoc or have multiple brands where each one<br/>requires its own template design including unique logos, color themes, addresses, etc.. <br/>(i.e. you can map BizDoc templates to organizations so that every time an order is <br/>created for one of these organizations, the correct template is added).<br/><br/><u>Note</u> - All BizDocs reference line items (from orders & opportunities). The columns for <br/>the grid used (which itself is a merge field called #Products#) is configured from:  Admin<br/>(Sales) or BizDoc (Purchase)</div>" />
                                               </td>
                                             <td colspan="2" style="width:50%">
                                                 <asp:HyperLink Text="Design / Templates" CssClass="hyperlink" Style="margin-left: 2px" runat="server" ID="hplEditBizDocTemplate" onclick="OpenBizDocTemplateList();"></asp:HyperLink>(HTML & CSS)
                                             </td>
                                          </tr> 
                                        <tr style="display:none">
                                              <td colspan="2">
                                                  
                                                    
                                                    <asp:Label ID="Label997" Text="?" CssClass="tip" runat="server" title="When creating a Sales Opportunity, next to the �Save & Open� button on the form, also show a Create" /></td>
                                                      <td colspan="2" style="width:45%">1st document sent customers is a &nbsp;
                                                    <asp:DropDownList ID="ddlSalesOppBizDoc" runat="server" Width="200" CssClass="signup">
                                                    </asp:DropDownList>

                                              </td>
                                            
                                             <td>
                                                   <asp:Label ID="Label27" Text="?" CssClass="tip" runat="server" title="Select Sales BizDoc responsible for Shipping Labels & Trackings #" />
                                              </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2" align="right">
                                                Use previous record�s email addresses from TO & CC when sending BizDocs
                                                <asp:CheckBox runat="server" ID="chkUsePreviousEmailBizDoc"/>
                                            </td>
                                            <td>
                                                <asp:Label ID="Label110" Text="?" CssClass="tip" runat="server" title="<div  class='innertooltipTitle'>	<h5>Use previous record�s email addresses when sending BizDocs</h5>	This feature tells BizAutomation to remember who you sent BizDocs to last time, so that next <br/>	time you send a Quotes, Invoices, POs, etc.. to the same email addresses in the TO and CC fields <br/>	used in the last message sent, so they�re automatically carried over to that next message. If you <br/>	add or remove an address, BizAutomation will remember for the next time you send that same <br/>	BizDoc out.</div>" />
                                            </td>
                                            <td>
                                                <asp:Label ID="Label111" Text="?" CssClass="tip" runat="server" title="<div  class='innertooltipTitle'>	<h5>Edit BizDoc & Order Naming Conventions</h5>	Change prefix values on documents such as Invoices, and  Orders. Use merge fields to help <br/>	define unique document IDs.<br/>	    <h5>BizDocs used for selling vs purchasing</h5>	BizDocs on the sale side represent documents that reflect your sales order, some of which might  <br/>	also be used to share with clients such as invoices, sales orders, quotes, and so forth (BizDoc  <br/>	labeling is customizable). The same can be set on the procurement side for purchase orders.</div>" />
                                            </td>
                                            <td colspan="2" align="left" class="normal1">
                                                <asp:HyperLink Text="Edit BizDoc & Order Naming Conventions" CssClass="hyperlink" runat="server" ID="hplNameTemplate" onclick="OpenNameTemplate();"></asp:HyperLink>  &nbsp;&nbsp;      
                                                <asp:HyperLink Text="BizDocs used for selling vs purchasing" CssClass="hyperlink" runat="server" ID="hplSalePurchaseBizDoc" onclick="OpenBizDocListFilter();"></asp:HyperLink>        
                                            </td>
                                        </tr>
                                          <tr style="display:none;">
                                              <td colspan="2" align="right" class="normal1">
                                                 <b> BizDoc List Filter</b>&nbsp;<asp:HyperLink Text="Configure" CssClass="hyperlink" runat="server" ID="hplBizDocListFilter" onclick="OpenBizDocListFilter();"></asp:HyperLink>
                                              </td>
                                              <td></td>
                                              <td></td>
                                              <td colspan="2" class="normal1">
                                                  
                                              </td>
                                          </tr> 
                                          <tr style="display:none;">
                                        
                                              <td>
                                                  <asp:Label ID="Label70" Text="?" CssClass="tip" runat="server" title="Use to change the length of the auto generated IDs assigned by Biz to Orders and BizDocs (e.g. Invoice, P.O., etc..) . Don�t forget to make sure to save the original character string before playing around with this, just in case you need to go back to the original settings." />
                                               </td>
                                        <td>
                                            <asp:Label ID="Label68" Text="?" CssClass="tip" runat="server" title="This will set the BizDoc created within the Purchase Order, that will only contain �Drop Ship� items in the Sales Order that PO was created from. This is available so you can quickly send your Vendor a PO document with only drop-shipped items from the Sales Order" />
                                        </td>
                                        <td colspan="2">
                                            BizDoc for �Drop-ship� item in POs created from SOs:&nbsp;
                                            <asp:DropDownList ID="ddlPODropShipBizDoc" Width="100px" runat="server" CssClass="signup" AutoPostBack="true">
                                            </asp:DropDownList>&nbsp;<asp:DropDownList ID="ddlPODropShipBizDocTemplate" Width="100px" runat="server" CssClass="signup">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    </table>
                                </fieldset>
                                <fieldset>
                                    <legend style="    margin: auto;">Procurement</legend>
                                    <table class="table table-responsive tblNoBorder">
                                       <tr>
                                        <td align="right" colspan="2">
                                            Bill-to address default <i>(PO from SO)</i>&nbsp;
                                            <asp:DropDownList ID="ddlBillTOAddress" runat="server" Width="200" CssClass="signup">
                                                <asp:ListItem Text="Employer" Value="1">
                                                </asp:ListItem>
                                                <asp:ListItem Text="Customer" Value="2">
                                                </asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                        <td>
                                        </td>
                                        <td>
                                        </td>
                                        <td colspan="2" >
                                            Ship-to address default <i>(PO from SO)</i>
                                            <asp:DropDownList ID="ddlShipToAddress" Width="200px" runat="server"
                                                CssClass="signup">
                                                <asp:ListItem Text="Employer" Value="1">
                                                </asp:ListItem>
                                                <asp:ListItem Text="Customer" Value="2">
                                                </asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                       </tr>
                                       <tr>
                                           <td align="right" colspan="2">
                                               Only items linked to a PO�s Vendor can be added to a PO
                                                <asp:CheckBox ID="chkRemoveVendorPOValidation" TextAlign="Left" runat="server" />
                                           </td>
                                           <td>
                                            <asp:Label ID="Label71" Text="?" CssClass="tip" runat="server" title="<div  class='innertooltipTitle'>	<h5>Purchase Order Fulfillment Settings</h5>	The formula selected here can be used to power both the purchase as and manufacturing <br/>	demand. A purchase demand plan deals with items you buy from vendors, resulting in a  <br/>	item quantity recommendation you can use to generate purchase orders, where as a   <br/>	manufacturing demand plan deals with finished goods or assemblies resulting in an item  <br/>    quantity recommendations you you can use to generate work orders.</div><div  class='innertooltipTitle'>	<h5>Only items linked to a PO�s Vendor can be added to a PO</h5>	Select which Bill (BizDoc template) is added to the Purchase Order when all quantities are <br/>	received, as well as  the order status values that should be set to when partial and full  <br/>	quantities are received.</div>" />
                                           </td>
                                           <td>
                                            <asp:Label ID="Label112" Text="?" CssClass="tip" runat="server" title="<div  class='innertooltipTitle'>	<h5>Demand Plan: Item Replenishment Formula</h5>	The formula selected here can be used to power both the purchase as and manufacturing <br/>	demand. A purchase demand plan deals with items you buy from vendors, resulting in a <br/>	item quantity recommendation you can use to generate purchase orders, where as a <br/>	manufacturing demand plan deals with finished goods or assemblies resulting in an item <br/>    quantity recommendations you you can use to generate work orders.</div><div  class='innertooltipTitle'>	<h5>Reorder Point & Quantity Formula</h5>	Within the inventory sub-tab of each item record you�ll find the �Reorder Point� value.<br/>	This value is a trigger point used for whichever procurement formula you select (see  <br/>	Global Settings | Order Mgt | Procurement).  The reorder point value can be static (based  <br/>	on whatever value you enter manually which doesn�t change) or dynamic, meaning it will <br/>   fluctuate based on based on the formula configured here. </div>" />
                                           </td>
                                           <td colspan="2">
                                               <b>Demand Plan :</b>
                                               &nbsp; <a href="#" onclick="return OpenAutoCreatePoConfig()">Item Replenishment Formula</a>
                                               &nbsp;<a href="javascript:OpenModalReorderPoint();">Re-Order Point & Qty Formula</a>
                                           </td>
    
                                       </tr>
                                        <tr>
                                            <td colspan="2" align="right" style="width:50%">
                                                <asp:HyperLink Text="PO Fulfillment Settings" CssClass="hyperlink" runat="server" ID="hplPOFulfillment" onclick="OpenPOFulfillmentSettings();"></asp:HyperLink>
                                            </td>
                                            <td></td>
                                            <td>
                                                <asp:Label ID="Label15" Text="?" CssClass="tip" runat="server" title="<div  class='innertooltipTitle'>BizAutomation automatically updates an item�s dynamic cost column for each vendor added from vendor sub-tab (in item records) from Purchase Orders (POs), but the static column does not update until you choose to click the �Update static cost columns� function button. This way, price lists issued to customers based on cost plus margin, stay static until it�s time to update them from dynamic cost columns.</div>" />
                                            </td>
                                            <td colspan="2" align="left" style="width:50%">
                                                <div class="form-inline">
                                                    <div class="input-group">
                                                      <telerik:RadComboBox ID="rcbItemGroupsDynamicCost" runat="server" CheckBoxes="true" EmptyMessage="Select Item Groups"></telerik:RadComboBox>
                                                      <div class="input-group-btn">
                                                            <button class="btn btn-primary" type="button" onclick="return UpdateStaticCost();">Update static cost columns</button>
                                                      </div>
                                                    </div>
                                                    &nbsp;&nbsp;
                                                    <a href="javascript:OpenVendorCostTable();">Set default cost level range</a>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr style="display:none;">
                                            
                                        <td></td>
                                            <td></td>
                                            <td><b>Purchase recommendation formula for demand plan:</b> 
                                        </td>
                                        </tr>
                                        <tr>
                                            
                                            <td></td>
                                            <td></td>
                                            
                                        </tr>
                                    </table>
                                </fieldset>
                                </center>
                        <table border="0" style="display: none">
                            <tr>
                                <td class="normal1" align="right">&nbsp;
                                </td>
                                <td class="normal1" align="left">
                                    <%--<asp:CheckBox ID="chkRemoveVendorPOValidation" runat="server" Text="Remove Vendor validation when creating new Purchase Orders" />--%>
                                    <asp:Label ID="Label37" Text="?" Style="display: none;" CssClass="tip" runat="server" title="Normally, when creating purchase order record which require that you enter a vendor name first, Biz will only allow items be added that are previously set up for that vendor within item details. Checking this will remove that validation. " />

                                    <asp:CheckBox ID="chkSearchOrderCustomerHistory" Style="margin-left: 30px;" runat="server" Text="Search within customer history when creating Orders" />
                                    <asp:Label ID="Label74" Text="?" CssClass="tip" runat="server" title="When creating a new Sales Order, this setting will only let you bring up items found within the selected customer�s previous sales orders." />
                                </td>
                            </tr>
                            <tr style="display: none">
                                <td class="normal1" align="right">&nbsp;
                                </td>
                                <td class="normal1" align="left">
                                    <asp:CheckBox ID="chkAmountPastDue" Checked="true" runat="server" />
                                    <asp:Label ID="lblAmountPastDue" runat="server">When creating a new Sales Order, if
                                        there is an amount past due greater than
                                        <asp:TextBox ID="txtAmountPastDue" runat="server" Text="1" CssClass="text" onkeypress="javascript:CheckNumber(1,event);"
                                            Width="50px" MaxLength="10">
                                        </asp:TextBox>
                                        make �Amount Past Due� red and flash it. </asp:Label>
                                </td>
                            </tr>
                        </table>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
            <div id="modalEChannelhub" class="modal fade" role="dialog">
                <div class="modal-dialog modal-md">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title">eChannelHub</h4>
                        </div>
                        <div class="modal-body">
                            <div class="row padbottom10">
                                <div class="col-xs-12">
                                    <asp:CheckBox runat="server" ID="chkReceieveOrderWithoutItem"></asp:CheckBox>
                                    <b>If inbound order contains items not mapped to BizAutomation, receive the order with an item
                                        <asp:DropDownList runat="server" ID="ddlUnMappedItem" Width="180"></asp:DropDownList>
                                        and enable the non-mapped items to be added or mapped.</b>
                                    <br />
                                    <i><b>Note</b> - By default, BizAutomation does not create sales order if inbound order from data feed, contains items without a mapped linking ID (e.g. SKU or ASIN)</i>
                                </div>
                            </div>
                            <div class="row padbottom10">
                                <div class="col-xs-12">
                                    <asp:CheckBox runat="server" ID="chkUsePredefinedCustomer"></asp:CheckBox>
                                    <b>All inbound orders from order source
                                        <select id="ddlOrderSource" style="width: 150px">
                                            <option value="0">-- Select One --</option>
                                        </select>
                                        should use the following organization </b>
                                    <telerik:RadComboBox AccessKey="C" Width="200" ID="radCmbCompany" DropDownWidth="600px"
                                        Skin="Default" runat="server" AutoPostBack="False" AllowCustomText="True" ShowMoreResultsBox="true" EnableLoadOnDemand="True"
                                        OnClientItemsRequested="OnClientItemsRequestedOrganization"
                                        ClientIDMode="Static">
                                        <WebServiceSettings Path="../common/Common.asmx" Method="GetCompanies" />
                                    </telerik:RadComboBox>
                                    <input type="button" class="btn btn-primary btn-add-mcm" value="Add" onclick="return SaveMarketplaceCustomerMapping();" style="display: none" />
                                    <input type="button" class="btn btn-danger btn-remove-mcm" value="Remove" onclick="    return RemoveMarketplaceCustomerMapping();" style="display: none" />
                                    <br />
                                    <i>Separate organization <span style="text-decoration: underline">will not be created</span> for these orders because the selected organization will always be used</i>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12" id="divEChannelHub">
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <asp:HiddenField ID="hdnShipPriorities" runat="server" />
                            <button type="button" class="btn btn-primary" id="btnSaveShippingConfig" onclick="return SaveMarketplaces();">Save</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                        </div>
                    </div>
                </div>
            </div>
        </telerik:RadPageView>
        <telerik:RadPageView ID="radPageView_Accounting" runat="server">
            <center><h4 style="margin-bottom: 0px; margin-top: 25px;"><b>GLOBAL ACCOUNTING SETTINGS</b></h4></center>
            <div class="col-md-3 col-md-offset-9">
                <i>
                    <a style="color: #000" class="hyperlink" href="#" onclick="return ChangeAccountMapping()">Financial Maps</a>
                </i>
            </div>
            <asp:Table ID="table12" CellPadding="0" CellSpacing="0" BorderWidth="1" runat="server"
                Width="100%" CssClass="aspTable" BorderColor="black" GridLines="None" Style="border-bottom: 1px; border-left: 1px; border-right: 1px;">
                <asp:TableRow>
                    <asp:TableCell VerticalAlign="Top" Width="100%">
                        <center>
                        <div>
                                <table border="0" class="table table-responsive tblNoBorder" style="    margin-left: -40px">
                                    <tr style="display:none;">
                                        <td colspan="2"  align="right"></td>
                                        <td><asp:Label ID="Label10" Text="?" CssClass="tip" runat="server" title="Base Currency of Trade" /></td>
                                        <td><asp:Label ID="Label12" Text="?" CssClass="tip" runat="server" title="Enable Configure" /></td>
                                        <td colspan="2">
                                            
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" align="right">
                                            <ul class="list-inline">
                                                <li>Invoices sent via email should go to </li>
                                                <li><asp:DropDownList ID="ddlARContactPosition" style="width:200px" runat="server" CssClass="form-control"></asp:DropDownList></li>
                                            </ul>
                                            
                                        </td>
                                        <td>
                                            <asp:Label ID="Label117" Text="?" CssClass="tip" runat="server" title="<div  class='innertooltipTitle'>	<h5>Invoices sent via email</h5>	When invoicing by email, this setting will default the email address in the �TO� field to<br/>	whichever address belongs to the position selected here. Please note that this rule will<br/>	be overridden by a rule in the �Global Settings | General | BizDocs� section titled �Use <br/>	previous record�s email addresses from TO & CC when sending BizDocs�.<br/><br/>	You can edit contact positions from the Admin Panel, within the �Add & Edit Drop-<br/>	Down Lists� section (Select �Contact� in the module drop-down, and �Contact<br/>	Position� in the List drop-down). <br/><br/>   If the selected contact position doesn�t exists for the customer being sent an Invoice,<br/>   (or if the �Use previous record�s email address� rule has no email address in memory),<br/>   then the primary contact for the customer record will be used.</div>" /> 
                                        </td>
                                       <td>
                                            <asp:Label ID="Label118" Text="?" CssClass="tip" runat="server" title="<div  class='innertooltipTitle'>	<h5>Configure Taxes</h5>	Once you upload tax tables from CSVs within the �Upload Tax Tables� section (or manually add tax<br/>	rows from this section), that list will be displayed in the tax tables sub-tab section.<br/><br/>    Taxes are calculated based on 1 of 3 radio button settings available within the tax tables  form (State,<br/>	City, or Zip/Postal values). The amounts added calculate percentage values using taxable line items<br/>	present within a sales opportunity or sales order, but only when the following 3 conditions are true:<br/><br/>	(1)<span class='subheading'> The Organization is taxable:</span> For an organization to be taxable, the tax check box within that<br/>	organization�s record has to be selected which all organizations default to. To un-set it, go to<br/>	Organization Details | Default Settings | Sales Tax(Default). When you do this, orders of that<br/>	organization will no longer calculate sales tax.<br/>	(2))<span class='subheading'> The item(s) added to the order is/are taxable:</span> To set it go to Item Details | Accounting & Measures<br/>	| Sales Tax(Default).<br/>		(3))<span class='subheading'> The ship-to is taxable:</span> The value within the sales opportunity or order�s �Ship-To� address must<br/>	be taxable such as the State, City, or Zip/Postal code - depending on how it�s configured within the<br/>	Tax Tables form.<br/><br/>		Note - Automated tax calculations via 3rd party API (e.g. �TaxJar�) are available for an additional fee.<br/>	Please speak to a BizAutomation representative for details.</div>" /> 
                                        </td>
                                        <td>
                                            <a href="#" onclick="return OpenTaxDetails();">Configure Taxes</a>&nbsp;&nbsp;
                                             <asp:CheckBox ID="chkAutolinkUnappliedPayment"  Checked="true" Text="Link unapplied payments to Invoices in Sales Orders" runat="server"  />
                                        </td>
                                    </tr>
                                    <tr>
                                         
                                        <td colspan="2" align="right">
                                            <b>When receiving payments default to:</b><br/>
                                            <ul class="list-inline">
                                                <li><asp:RadioButton Text="Group with other undeposited funds" runat="server" ID="radUnderpositedFund" GroupName="DepositeGroup" /></li>
                                                <li><asp:RadioButton Text="Deposit to:" runat="server" ID="radDepositeTo" GroupName="DepositeGroup" /></li>
                                                <li><asp:DropDownList ID="ddlDepositTo" style="width:200px" runat="server" CssClass="form-control" ></asp:DropDownList></li>
                                            </ul>
                                        </td>
                                        <td>
                                             
                                        </td>
                                        <td>
                                            <asp:Label ID="Label14" Text="?" CssClass="tip" runat="server" title="<div  class='innertooltipTitle'>  <h5>Landed Cost</h5>   Landed cost is added from the bottom section of closed Purchase Order (PO) detail pages.<br/>  After closing a PO (requires all items are fully received and billed which contributes the<br/>  cost to A/P), as you receive invoices from service vendors (e.g. Ocean freight, Customs<br/>  fees, etc..) and add these costs within the landed cost section of the order, BizAutomation<br/>  will increase the average cost of a PO�s line items in a weighted manor.<br/>  <br/>  <span class='headingsmallfont1'>Example:</span>Assume there�s a PO with 2 line items and total landed cost amount is $100:<br>  Item-1: Qty of 10 x $100 ea. ($1,000 total) weighs 100 lbs total, and is 10�x10�x10�<br/>  Item-2: Qty of 10 x $10 ea ($100 Total) weighs 10 lbs total, and is 1�x1�x1�<br/>  <br/>  <span class='headingsmallfont1'>Here�s how landed cost would contribute to the average cost of each item:</span>  <br/>  (1) Amount -  Total amount of 2 line items $1,000 + $100 = $1100<br/>  Item 1 - ($100 * $1,000) / $1100 = $90.90<br/>  Item 2 -  ($100 * $100) / $1100 = $9.09<br/>  <br/>  (2) Weight - Total weight of 2 line items 100 + 10 = 110<br/>  Item 1 - ($100 * 100) / 110 = $90.90<br/>  Item 2 -  ($100 * 10) / 110 = $9.09<br/>  <br/>  (3) Cubic Size - Total (Width * Height * Length)  of 2 line items 1000+ 1 = 1001<br/>  Item 1 - ($100 * 1000) / 1001 = $99.90<br/>  Item 2 -  ($100 * 1) / 1001 = $0.09<br/> </div>" />
                                        </td>
                                        <td colspan="2">
                                             <asp:CheckBox runat="server" ID="chkLandedCost" /> 
                                            <div style="display:none">Enable Landed Cost on <asp:RadioButtonList runat="server" ID="rblLandedCost" style="margin-top:10px;" RepeatLayout="Flow" RepeatDirection="Horizontal">
                                                <asp:ListItem>Cubic Size</asp:ListItem>
                                                <asp:ListItem>Weight</asp:ListItem>
                                                <asp:ListItem Selected="True">Amount</asp:ListItem>
                                            </asp:RadioButtonList><asp:HyperLink ID="hplLandedCostVendors" runat="server" Text="Configure Vendors" onclick="return OpenLandedCostVendors();"
                                                CssClass="hyperlink">
                                            </asp:HyperLink></div>
                                             Enable  <asp:HyperLink ID="hplLandedCostExpenseAccount" runat="server" Text="Landed Cost Configuration" onclick="return OpenLandedCostExpenseAccount();"
                                                CssClass="hyperlink">
                                            </asp:HyperLink>
                                            <br />
                                            <asp:CheckBox runat="server" ID="chkUseDeluxeCheckStock" /> Use Deluxe brand Check Stock Product Number: 8108102 (FLEX LASER TOP CHECK) 
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" align="right">
                                            Decimal Points used on Quotes & Invoices <asp:DropDownList ID="ddlDecimalPoints" runat="server" CssClass="signup">
                                                <asp:ListItem Text="2" Value="2"></asp:ListItem>
                                                <asp:ListItem Text="3" Value="3"></asp:ListItem>
                                                <asp:ListItem Text="4" Value="4"></asp:ListItem>
                                                <asp:ListItem Text="5" Value="5"></asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                        <td>
                                        </td>
                                        <td>
                                            <asp:Label ID="Label16" Text="?" CssClass="tip" runat="server" title="<div  class='innertooltipTitle'><h5>Keep Credit Card Values in Database</h5> When enabled, BizAutomation will keep credit card<br/> values within the database so you can charge your<br/> customer�s credit card(s) in the future (If you need to<br/> mass charge invoices for example) without requiring<br/> the values be re-entered again (assumes you take<br/> responsibility and have the card holder�s permission).<br/> These values will be encrypted for security.</div>" />
                                        </td>
                                        <td colspan="2">
                                            <asp:CheckBox ID="chkKeepCreditCardInfo" TextAlign="Right" Text="Keep Credit Card Values in Database" runat="server" onclick="return AcceptTerms();" />
                                            &nbsp &nbsp &nbsp
                                            <asp:Label Visible="false" ID="Label39" Text="?" CssClass="tip" runat="server" title="<div class='innertooltipTitle'><h5>Credit Card Profit Margins</h5>  Select the percentage you want to charge when receiving payment against invoices credit card methods of<br/>  payment.</div> " />
                                            &nbsp
                                            <asp:Label ID="lblAuthrizecardpercentage" runat="server" Text="When processing credit cards, charge" style="font-weight:bold;"></asp:Label>
                                            <asp:DropDownList ID="ddlAuthorizePercentage" runat="server" ></asp:DropDownList> %
                                              
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right" colspan="2">
                                             <a href="#" data-toggle="modal" data-target="#divCardConnectURL">Show payment button on customer statements</a>&nbsp;<asp:CheckBox runat="server" ID="chkShowCardConnectLink" />
                                            <div id="divCardConnectURL" class="modal fade" role="dialog">
                                                <div class="modal-dialog">
                                                    <!-- Modal content-->
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                            <h4 class="modal-title">CardConnect(BluePay) Configuration</h4>
                                                        </div>
                                                        <div class="modal-body">
                                                            <div class="form-group">
                                                                <label>Form Name</label>
                                                                <asp:TextBox runat="server" CssClass="form-control" ID="txtBluePayFormName"></asp:TextBox>
                                                            </div>
                                                            <div class="form-group">
                                                                <label>Redirect URL (Successful Payment)</label>
                                                                <asp:TextBox runat="server" CssClass="form-control" ID="txtBluePaySuccessURL"></asp:TextBox>
                                                            </div>
                                                            <div class="form-group">
                                                                <label>Redirect URL (Declined Payment)</label>
                                                                <asp:TextBox runat="server" CssClass="form-control" ID="txtBluePayDeclineURL"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-primary" data-dismiss="modal">Save & Close</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                             &nbsp; <asp:Label ID="Label18" Text="?" CssClass="tip" runat="server" title="<div  class='innertooltipTitle'>	<h5>Payment button on customer statements </h5>	To use this feature you�ll need a tokenized payment page configured with Card<br/>	Connect (Apply at https://cardconnect.com/partner/bizautomation).<br/><br/>	The payment link displays a �Pay Now� button within outbound A/R statements and<br/>	email alerts. From the email message the link will redirect your customer to a payment<br/>	page operated by Card Connect. Depending on how you instruct Card Connect during<br/>	implementation, you can accept payments via ACH, Credit Card, or both.</div>" />&nbsp;
                                        
                                            <asp:CheckBox ID="chkEnableDiferredIncome" TextAlign="Left" Text="Enable Deferred Income" runat="server" />
                                            </td>
                                        <td>
                                            <asp:Label ID="Label17" Text="?" CssClass="tip" runat="server" title="<div  class='innertooltipTitle'>	<h5>Deferred Income</h5>	Deferred Income is a liability because it refers to revenue that hasn�t yet been earned, but <br/>	represents products and services that are owed to the customer. As the product or service <br/>	is delivered over time, it�s recognized as revenue on the income statement. <br/><br/>	When enabled, the �Deferred Income� account under liabilities will be credited instead of  <br/>�Sales� which is an Income account (The A/R Account�is debited whether deferred income <br/>	is set or not). As income is received, �Deferred Income� is debited, and the �Sales� account <br/>	is credited.</div>" />
                                        </td>
                                         <td>
                                             <asp:Label ID="Label42" Text="?" CssClass="tip" runat="server" title="<div  class='innertooltipTitle'>   <h5>Default Cost</h5>   The cost setting here is used in sales commission calculations (if<br/> used), pricing formulas that require cost information, and gross<br/> profit calculations within sales orders. When a purchase order is<br/> created from a sales order however, where items in both<br/> records match, the cost of the PO�s line items will be used<br/> instead (bypassing default), regardless of default cost setting.  <br/><br/>  <h5>Bill Status Workflow</h5>   Select which Bill (BizDoc template) is added to the Purchase Order<br/> when all quantities are received, as well as the order status values<br/> that should be set (One for partial and another when full quantities<br/> are received).</div>" />
                                              
                                        </td>
                                        <td>
                                            <b>Default Cost:	</b>
                                            <asp:RadioButton ID="rdbAvgCost" runat="server" Text="Average Cost" GroupName="DefaultCost" />
                                            <asp:RadioButton ID="rdbPVCost" runat="server" Text="Primary Vendor Cost" GroupName="DefaultCost" />&nbsp;&nbsp;
                                            <a href="#" onclick="return OpenBillPaymentBizDoc()">BizDoc Bill Status Workflow</a>
                                            <div style="display:none">
                                           

                                            </div>
</td>
                                             
                                    </tr>
                                    <tr>
                                        <td align="right" colspan="2">Default Month Financial Reports should begin 
                                         <asp:DropDownList CssClass="signup" ID="ddlFiscalStartMonth" runat="server">
                                                <asp:ListItem Value="1">January</asp:ListItem>
                                                <asp:ListItem Value="2">February</asp:ListItem>
                                                <asp:ListItem Value="3">March</asp:ListItem>
                                                <asp:ListItem Value="4">April</asp:ListItem>
                                                <asp:ListItem Value="5">May</asp:ListItem>
                                                <asp:ListItem Value="6">June</asp:ListItem>
                                                <asp:ListItem Value="7">July</asp:ListItem>
                                                <asp:ListItem Value="8">August</asp:ListItem>
                                                <asp:ListItem Value="9">September</asp:ListItem>
                                                <asp:ListItem Value="10">October</asp:ListItem>
                                                <asp:ListItem Value="11">November</asp:ListItem>
                                                <asp:ListItem Value="12">December</asp:ListItem>
                                            </asp:DropDownList>
                                            </td>
                                        <td>
                                        </td>
                                        <td>
                                            <asp:Label ID="Label31" Text="?" CssClass="tip" runat="server" title="<div class='innertooltipTitle'>  <h5>Multi-Entity Class Tracking </h5>  Used as an alternative to full multi-entity segmentation that require complete separation of subsidiaries,<br/>  branches, and inventory - Multi-Entity Class Tracking provides a simple and convenient way to separate<br/>  entities by transaction. These entities can be companies (which can be further segmented by warehouse<br/>  location and BizDoc templating), brands, departments, or even territories within a single company. By<br/>  setting a class definition and hierarchy, BizAutomation will categorize  transactions so that financial<br/>  reports can be filtered by class or be rolled into full transactional views.<br/>  <br/>  <span class='headingsmallfont1'>Organization</span>-Setting the drop-down value to Organization (Must be account level both for customers and<br/>  suppliers) requires that each organization be given a class value from the �Default Settings� sub-tab<br/>  section. When this happens, BizAutomation will automatically assign any transaction related to that<br/>  organization to the class its associated with.<br/>  <br/>  <span class='headingsmallfont1'>Employee</span>-Setting the drop-down value to Employee requires that you set a class value to each employee<br/>  in your organization so that anytime they create a transaction such as an order, BizAutomation will<br/>  automatically assign that class to the transaction.<br/>  <br/>  <span class='headingsmallfont1'>Class Hierarchy </span>-Use this form to drag-and-drop values into their hierarchical order. It�s used for visual<br/>  purposes only within the drop-down object in an organization�s default settings page. You can add or<br/>  remove values from: Admin Panel | Add & edit drop-down lists | Module �Accounting� List �Class�</div>" />
                                            
                                        </td>
                                        <td colspan="2">
                                            Separate Entities by   <asp:DropDownList ID="rblDefaultClass" runat="server" CssClass="signup">
                                                <asp:ListItem Text="None (Disabled)" Value="0" Selected="True"></asp:ListItem>
                                                <asp:ListItem Text="User" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="Organization" Value="2"></asp:ListItem>
                                            </asp:DropDownList>using Multi-Entity Class Tracking  &nbsp;&nbsp;                              
                                            <a href="#" onclick="OpenClass();">Set class hierarchy</a>
                                        
                                             

                                            
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" align="right">
                                            Default Item used for Discounts within Orders
                                            <asp:DropDownList ID="ddlDiscountServiceItem" runat="server" CssClass="signup" Width="200px">
                                            </asp:DropDownList>
                                        </td>
                                        <td>
                                        </td>
                                        <td>
                                        </td>
                                        <td colspan="2">
                                            Pay Period 
                                             <asp:DropDownList CssClass="signup" ID="ddlPayPeriod" runat="server" Width="250px">
                                        <asp:ListItem Value="1">Monthly (Once per month)</asp:ListItem>
                                        <asp:ListItem Value="2">Twice per month (Semi-monthly)</asp:ListItem>
                                        <asp:ListItem Value="3">Once per week (Weekly), ending on Monday-Sunday</asp:ListItem>
                                        <asp:ListItem Value="4">Once per week (Weekly), ending on Tuesday-Monday</asp:ListItem>
                                        <asp:ListItem Value="5">Once per week (Weekly), ending on Wednesday-Tuesday</asp:ListItem>
                                        <asp:ListItem Value="6">Once per week (Weekly), ending on Thursday-Wednesday</asp:ListItem>
                                        <asp:ListItem Value="7">Once per week (Weekly), ending on Friday-Thursday</asp:ListItem>
                                        <asp:ListItem Value="8">Once per week (Weekly), ending on Saturday-Friday</asp:ListItem>
                                        <asp:ListItem Value="9">Once per week (Weekly), ending on Sunday-Monday</asp:ListItem>
                                        <asp:ListItem Value="10">Once per week (Weekly), ending on Monday-Sunday</asp:ListItem>
                                        <asp:ListItem Value="11">Every 2 weeks (Bi-monthly), ending on Tuesday-Monday</asp:ListItem>
                                        <asp:ListItem Value="12">Every 2 weeks (Bi-monthly), ending on Wednesday-Tuesday</asp:ListItem>
                                        <asp:ListItem Value="13">Every 2 weeks (Bi-monthly), ending on Thursday-Wednesday</asp:ListItem>
                                        <asp:ListItem Value="14">Every 2 weeks (Bi-monthly), ending on Friday-Thurday</asp:ListItem>
                                        <asp:ListItem Value="15">Every 2 weeks (Bi-monthly), ending on Saturday-Friday</asp:ListItem>
                                        <asp:ListItem Value="16">Every 2 weeks (Bi-monthly), ending on Sunday-Monday</asp:ListItem>
                                                 <asp:ListItem Value="17">Custom Dates</asp:ListItem>
                                    </asp:DropDownList>
                                           
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" align="right">
                                            Add a matching Invoice when a <asp:DropDownList ID="ddlInvoicing" runat="server">
                                                <asp:ListItem Text="Packing Slip" Value="1" />
                                                <asp:ListItem Text="Fulfillment Order" Value="2" />
                                            </asp:DropDownList> is added
                                            <asp:CheckBox ID="chkInvoicing" runat="server"></asp:CheckBox>
                                            <div style="display:none;">
                                            <div style="float:left"><asp:CheckBox ID="chkApprovalTimeExp" Text="" runat="server"/> Enable </div>
                                            <div style="float:left;padding-left:5px;">
                                            <asp:RadioButtonList ID="rdnTimeExpProcess" runat="server" RepeatDirection="Horizontal">
                                                <asp:ListItem Value="1" Selected="True">Flat</asp:ListItem>
                                                <asp:ListItem Value="2">Hierarchical</asp:ListItem>
                                             </asp:RadioButtonList> 
                                            </div>
                                            <div style="float:left;padding-left:5px;">
                                                Approval Process for Time & Expense
                                            </div>
                                                </div>
                                        </td>
                                        <td>
                                            <asp:Label ID="Label43" Text="?" CssClass="tip" runat="server" title="<div  class='innertooltipTitle'><h5>Adding matching Invoice(s)</h5> This feature triggers BizAutomation to generate a matching Invoice based on the Packing Slip or<br/> Fulfillment Order added, whether it�s a partial or full reflection of the sales order. When this<br/> happens, each BizDoc will contain the other�s ID within their �Reference� merge field. You can add,<br/> edit, or remove this merge field from: Global Settings | Order Management | BizDocs | Design /<br/> Templates (HTML & CSS). <br/><br/>We make the distinction between a Packing Slip and Fulfillment Order because a Packing Slip can also<br/> double as the Fulfillment Order (The Fulfillment Order is responsible for releasing allocation of<br/> whatever line items and quantities it contains), but can also be setup as a separate BizDoc, in which<br/> case adding a Packing Slip to an order does not release allocation. <br/><br/>If you have a rule in workflow automation that automatically creates Invoice BizDocs, you�ll need to<br/> turn it off before enabling this feature. </div>" />
                                        </td>
                                         <td>
                                            <asp:Label ID="Label82" Text="?" CssClass="tip" runat="server" title="<div  class='innertooltipTitle'> <h5>Foreign Currencies</h5>  To see all currencies, click the �Foreign Currencies� link, and un-select the <br/>�Show only active currencies� check box.<br/>  <br/>  When a foreign currency is added, here are the areas of the suite that merit attention:<br/>  <br/>  (1) The �Alt Function� link next to the list price field in item details lets you override<br/>  the calculated list price based on the exchange rates  (CSV mass update is available)<br/>  The only reason to use this would be if you want your list price to look a certain way<br/>  (e.g. $99.95) instead of whatever the calculation ends up being (e.g. $94.02).<br/>  <br/>  (2) �Price Levels� within item details can be configured according to foreign currencies<br/>  (CSV mass update is available). Price rules however, will automatically calculate<br/>  against currency exchange rates according to the customer�s default currency.<br/>  <br/>  (3) When creating new orders, if the customer or vendor isn�t defaulted to a foreign<br/>  currency, one can be selected at the point of sale from that new order form.<br/>  <br/>  (4) An Account or Vendor organization�s  trading currency can be defaulted by opening<br/>  the record, clicking the �Default Setting� tab, and setting the currency within the<br/>  �Trading Currency� drop-down.<br/>  <br/>  (5) Within the Chart of Accounts, an off-setting account titled �Foreign Exchange<br/>  Gain/Loss� will adjust as currencies fluctuate relative to the base currency, when<br/>  invoices and bills are added and paid.<br/></div>" />
                                        </td>
                                        <td colspan="2">
                                            <div class="form-inline">
                                            <asp:CheckBox ID="chkMultiCurrency" Text="Enable" runat="server" />
                                           &nbsp;<asp:HyperLink ID="hplSetCurrency" runat="server" Text="Foreign Currencies" onclick="return OpenCurrency();"
                                        CssClass="hyperlink">
                                    </asp:HyperLink>&nbsp;&nbsp;Domestic Currency
<asp:DropDownList ID="ddlCurrency" runat="server" CssClass="signup" style="width:200px">
                                            </asp:DropDownList>
                                                </div>
                                            <div  style="display:none;">
                                            <asp:CheckBox ID="chkNonIExpenseAccount" Text="" Checked="true" runat="server" CssClass="signup" />
                                            <label for="chkNonIExpenseAccount">Record Non-Inventory Item Expense ?</label>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                                    <fieldset style="padding:0px;display:none">
                                    <legend style="margin-bottom: 1px;margin:auto;display:none">Taxes</legend>
                                         <table border="0" class="table table-responsive tblNoBorder" style="margin: 0px;    margin-left: 3.6%;">
                                            <tr>
                                                <td colspan="2">
                                         <asp:HyperLink ID="hplTaxDetails" runat="server" Text="Tax Details" onclick="return OpenTaxDetails();"
                                                CssClass="hyperlink">
                                            </asp:HyperLink>&nbsp;&nbsp;&nbsp;
                                                    <asp:HyperLink ID="HyperLink2" runat="server" Text="Tax Country Configure"
                                                onclick="return OpenTaxCountryConfi();" CssClass="hyperlink">
                                            </asp:HyperLink>
                                                    </td>
                                                <td>
                                                    <asp:Label ID="Label101" Text="?" CssClass="tip" runat="server" title="Tax Configure" />
                                                </td>
                                                <td >
                                                    <asp:Label ID="Label102" Text="?" CssClass="tip" runat="server" title="Calculate Taxes on Purchases" />
                                                </td>
                                                <td colspan="2">
                                        <asp:CheckBox runat="server" ID="chkPurchaseTaxCredit" Text="Calculate Taxes on Purchases (e.g. P.O.s)" />
                                            <asp:HiddenField ID="hdnPurchaseTaxCredit" runat="server" Value="0" />
                                                    </td>
                                                </tr>
                                            </table>
                                </fieldset>
                                    <fieldset>
                                        <legend style="margin:auto;">Price & Cost Management</legend>
                                        
                                        <table border="0" class="table table-responsive tblNoBorder" style="margin-bottom:10px;">
                                            <tr>
                                               <td colspan="2" align="right">
                                                    <asp:CheckBox ID="chkDiscountOnUnitPrice" TextAlign="Left" runat="server" Text="Do not reflect Discount Amounts in the Sub-total footer of a BizDoc &nbsp;" /></td>
                                                
                                        <td></td>
                                        <td></td>
                                        <td colspan="2">
                                             <b>Display Unit Price as: </b><asp:DropDownList ID="rblPriceBookDiscount" runat="server">
                                                <asp:ListItem Value="0" Text="List price minus price rule discounts" Selected="True"></asp:ListItem>
                                                <asp:ListItem Value="1" Text="List price & discounts (% or Flat Amt) in separate field" Selected="True"></asp:ListItem>
                                            </asp:DropDownList>
                                           
                                        </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2" align="right" >
                                                    
                                            <a onclick="return OpenMinimumUnitPrice();">Minimum Profit Margin Enforcement</a> 
                                                    <telerik:RadComboBox ID="rcbPriceMarginApproval" runat="server" CheckBoxes="true"></telerik:RadComboBox>
                                                    <asp:CheckBox ID="chkMinUnitPrice" TextAlign="Left"  runat="server" Text="" />
                                       
                                                    <asp:Label ID="lblPriceMargin" style="display:none" runat="server" Text="Disable"></asp:Label>
                                                </td>
                                        <td>
                                            <asp:Label ID="Label91" Text="?" CssClass="tip" runat="server" title="<div  class='innertooltipTitle'>  <h5>Minimum Profit Margin Enforcement</h5>  This tool provides the means by which minimum profit margin rules can be set. Once a rule is<br/>  triggered, an approval record is created within the �Activities� list for authorized users (This is<br/>  what the check box drop-down is for).<br/>  The rule doesn�t prevent a sales opportunity or order from being generated internally (does not<br/>  apply to orders generated from the web store or 3rd party marketplace) but it does prevent the<br>  issuing of any BizDoc (e.g. Quote, Invoice, Sales Order) which can be sent to a customer.</div>" />
                                        </td>
                                                <td  >
                                                     <asp:Label ID="Label13" Text="?" CssClass="tip" runat="server" title="<div class='innertooltipTitle'><h5>Default Sales Pricing</h5>  Pricing defaults apply to new sales opportunities/orders. BizAutomation supports 2 types of unit<br/>  pricing:<br/>  (1) <span class='headingsmallfont1'>Price Rules</span>- Set from Global Settings | Price & Cost Rules<br/>  (2) <span class='headingsmallfont1'>Price Levels</span>- These relate specifically to the item, are set from within the item record, and can<br>  be imported and updated via CSV.<br/>  <br/>  Only one can be set as the default, unless you simply want to set the �Last price� as the default,<br/>  which is also an option.</div>" />
                                                </td>
                                        <td colspan="2">
                                             <b>Default Sales Pricing:</b>
                                            &nbsp;
                                            <asp:RadioButton ID="rdbPriceLevel" runat="server" Text="Price Levels" GroupName="DefaultSalesPricing" />
                                            &nbsp;
                                            <asp:RadioButton ID="rdbPriceRule" runat="server" Text="Price Rules" GroupName="DefaultSalesPricing" />
                                            &nbsp;
                                            <asp:RadioButton ID="rdbLastPrice" runat="server" Text="Last price" GroupName="DefaultSalesPricing" />
                                        &nbsp;
                                           <asp:HyperLink ID="hplPriceLevelNames" runat="server" CssClass="hyperlink" NavigateUrl="Javascript:OpenPriceLevelNames();">Set Price Level Names</asp:HyperLink>
                                        </td>
                                            </tr>
                                        </table>
                                    </fieldset>
                                    <fieldset>
                                        <legend style="margin:auto">Sales Commision Default</legend>
                                        <table border="0" class="table table-responsive tblNoBorder" style="margin-bottom:10px;    margin-left: -183px;">
                                            <tr>
                                               
                                                <td></td>
                                                <td style="width:50px;"></td>
                                                <td colspan="3">
                                                   &nbsp;<asp:CheckBox ID="chkCommisonBasedOn" style="margin-bottom:8px" runat="server" TextAlign="Left" Text="Base commission on gross profits instead of full amounts" /><br />
                    <asp:DropDownList runat="server" ID="ddlCommissionBasedOn"  style="margin-bottom:8px">
                        <asp:ListItem Text="Based on spread between primary vendor cost & sale price" Value="1"></asp:ListItem>
                        <asp:ListItem Text="Based on spread between average cost & sale price" Value="2"></asp:ListItem>
                    </asp:DropDownList>
                                                    <br />
                                                    <asp:DropDownList runat="server" ID="ddlVendorCostType">
                            <asp:ListItem Text="Base on dynamic cost values" Value="1"></asp:ListItem>
                            <asp:ListItem Text="Base on static cost values" Value="2"></asp:ListItem>
                        </asp:DropDownList>
                                                    <br />
                                                    <asp:CheckBox runat="server" ID="chkIncludeTaxAndShippingInCommission" TextAlign="Left" Text="Include shipping in sales commission calculation" />
                                                </td>
                                                 <td>
                                                    <asp:Label ID="Label103" Text="?" CssClass="tip" runat="server" title="<div  class='innertooltipTitle'><h5>Sales Commissions</h5>  Global settings configured in this section work hand in hand with rules added<br/>  from the Sales Commissions module, which can be found in Human<br/>  Resources | Sales Commissions.<br/>  <br>  If you configure commission on gross profits, the cost portion (aka �floor�) of<br>  the equation (the other being the sale price of goods and services aka<br/>  �ceiling�) will be based on whatever cost you define within the general section<br/>  within �Global Accounting Settings� | �Default Cost�. However, anytime a<br>  purchase order (PO) is created or matched to a sales order�s (SO) line item,<br>  the cost of the PO will be used regardless of default cost setting.<br>  <br>  Taxes are not factored into commission amounts. All commission earned will<br/>  be deducted from �Credit Memos�, and �Refund Amounts� when applied<br>  after a commission total is calculated. To see commission amounts by pay<br/>  period go to Human Resources | Payroll. View access can be configured so<br/>  that sales employees can only see their commissions (where as managers<br/>  and executive roles can see all commissions).</div>" />
                                                    
                                                </td>
                                                <td style="    width: 36px;"></td>
                                                <td colspan="2">
                                                    <asp:RadioButtonList ID="rblCommissionType" runat="server">
                                                        <asp:ListItem Text="Invoice Paid Amount" Value="1">
                                                        </asp:ListItem>
                                                        <asp:ListItem Text="Invoice Sub-Total Amount (Paid or Unpaid)" Value="2">
                                                        </asp:ListItem>
                                                        <asp:ListItem Text="Sales Order Sub-Total Amount" Value="3">
                                                        </asp:ListItem>
                                                    </asp:RadioButtonList>
                                                </td>
                                            </tr>
                                        </table>
                                    </fieldset>
                                    <fieldset>
                                        <legend style="margin:auto">Terms</legend>
                                        <asp:Table ID="table8" CellPadding="0" CellSpacing="0" runat="server"
                        Width="100%" BorderColor="black" GridLines="None" >
                        <asp:TableRow>
                            <asp:TableCell VerticalAlign="Top">
                                <table class="table table-responsive tblNoBorder" style="width: 100% !important">
                                    <tr class="normal1" align="right">
                                        <td align="right" class="normal1">
                                            <asp:LinkButton ID="btnAddNewTerms" runat="server" CssClass="btn btn-primary" OnClientClick="return OpenTerms(0);"><i class="fa fa-plus-circle"></i>&nbsp;&nbsp;Add New Term</asp:LinkButton>
                                            <%--<asp:Button runat="server" ID="btnAddNewTerms" class="button" Text="Add New Term"  />--%>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:GridView ID="gvTerms" runat="server" EnableViewState="true" AutoGenerateColumns="false"
                                                CssClass="table table-responsive table-bordered tblNoBorder" AllowSorting="true" Style="width: 100% !important" DataKeyNames="numTermsID" CellPadding="2" CellSpacing="2">
                                                <Columns>
                                                    <asp:TemplateField HeaderText="Sr No" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="8%">
                                                        <ItemTemplate>
                                                            <%# Container.DataItemIndex + 1 %>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Term Name" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="80%">
                                                        <ItemTemplate>
                                                            <asp:HiddenField runat="server" ID="hdnTermsID" Value='<%#Eval("numTermsID")%>' />
                                                            <asp:Label runat="server" ID="lblTerms" Text='<%#Eval("vcTerms") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Apply To" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="8%">
                                                        <ItemTemplate>
                                                            <asp:Label runat="server" ID="lblApplyTo" Text='<%#IIf(Eval("tintApplyTo") = 1, "Sales Orders", "Purchase Order")%>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderStyle-Width="3%" ItemStyle-HorizontalAlign="Center">
                                                        <ItemTemplate>
                                                            <%--<asp:LinkButton runat="server" ID="lnkBtnEdit"></asp:LinkButton>--%>
                                                            <asp:ImageButton runat="server" ID="imgbtnEdit" ImageAlign="Middle" ImageUrl="~/images/edit.png" />
                                                            <%--<asp:Button ID="btnEdit" runat="server" CssClass="button" Text="Edit" OnClientClick='return OpenTerms(<%#Eval("numTermsID")%>);'></asp:Button>--%>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderStyle-Width="3%" ItemStyle-HorizontalAlign="Center">
                                                        <ItemTemplate>
                                                            <asp:Button ID="btnDeleteRow" runat="server" CssClass="btn btn-danger" Text="X" CommandName="DeleteRow"
                                                                CommandArgument='<%#Eval("numTermsID")%>'></asp:Button>
                                                            <asp:Label runat="server" ID="lblDelete" Visible="false"></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                                <EmptyDataTemplate>
                                                    <center>No Terms Found.</center>
                                                </EmptyDataTemplate>
                                            </asp:GridView>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" align="right">
                                            Default Item used for overhead cost within assemblies
                                            <asp:DropDownList ID="ddlOverheadServiceItem" runat="server" CssClass="signup" Width="100px">
                                            </asp:DropDownList>
                                        </td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td colspan="2"></td>
                                    </tr>
                                </table>
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                                    </fieldset>
                                </div>
                        <table border="0" style="display: none">
                            <tr>
                                <td class="normal1" align="right" style="width: 55%;">Authoritative BizDocs
                                </td>
                                <td align="left" class="normal1">
                                    <asp:HyperLink Text="Set" CssClass="hyperlink" runat="server" ID="hplAuthoritativeBizDocs" onclick="AuthoritativeBizDoc();"></asp:HyperLink>
                                    <asp:Label ID="Label83" Text="?" CssClass="tip" runat="server" title="Sets the names of the documents within orders that will impact accounting (Non-Authoritative BizDocs do not impact accounting). Most people will set �Invoice� on the name used on the Sales side, and �Purchase Order� on the Purchase side (so if you don�t know what to do, just select these)." />
                                </td>
                            </tr>
                            <tr>
                                <td class="normal1" align="right" style="width: 55%;">Accounts for Relationship
                                </td>
                                <td align="left" class="normal1">
                                    <asp:HyperLink ID="hplRelationship" runat="server" Text="Set" onclick="return Relationship();"
                                        CssClass="hyperlink">
                                    </asp:HyperLink>
                                    <asp:Label ID="Label84" Text="?" CssClass="tip" runat="server" title="Allows you to configure chart of accounts for relationship(i.e Customers,vendors).From here select the COA under which the balances of customer/Vendor will be reflected." />
                                </td>
                            </tr>
                            <tr>
                                <td class="normal1" align="right" style="width: 55%;">Accounts for Contact Type
                                </td>
                                <td align="left" class="normal1">
                                    <asp:HyperLink ID="hplContactTypeAccounts" runat="server" Text="Set" onclick="return ContactTypeAccounting();"
                                        CssClass="hyperlink">
                                    </asp:HyperLink>
                                    <asp:Label ID="Label85" Text="?" CssClass="tip" runat="server" title="Select the default liability account from here under which the new contact will fall.Liability account set here will be used in Billable Time and Expense journal entry, helps you to keep track of liablity based on contact type. i.e employee, sub-contractor" />
                                </td>
                            </tr>
                            <tr>
                                <td class="normal1" align="right" style="width: 55%;">Accounts for Shipping Method
                                </td>
                                <td align="left" class="normal1">
                                    <asp:HyperLink ID="hplShippingAccounts" runat="server" Text="Set" onclick="return Shipping();"
                                        CssClass="hyperlink">
                                    </asp:HyperLink>
                                    <asp:Label ID="Label86" Text="?" CssClass="tip" runat="server" title="Select the mode of shipping from here as well the default account for shipping Income under which the shipping charges received from customer will be credited." />
                                </td>
                            </tr>
                            <tr>
                                <td class="normal1" align="right" style="width: 55%;">Accounts for Project
                                </td>
                                <td align="left" class="normal1">
                                    <asp:HyperLink ID="hplProjectAccounts" runat="server" Text="Set" onclick="return ProjectAccounting();"
                                        CssClass="hyperlink">
                                    </asp:HyperLink>
                                    <asp:Label ID="Label87" Text="?" CssClass="tip" runat="server" title="Selection of default expense account for the projects where all the expenses for the projects are charged to the default account." />
                                </td>
                            </tr>
                            <tr>
                                <td class="normal1" align="right" style="width: 55%;">Default Accounts
                                </td>
                                <td align="left" class="normal1">
                                    <asp:HyperLink ID="hplDefaultAccounts" runat="server" Text="Set" onclick="return DefaultAccounts();"
                                        CssClass="hyperlink">
                                    </asp:HyperLink>
                                    <asp:Label ID="Label88"  Text="?" CssClass="tip" runat="server" title="Selection of default account will help to allocate the entries relating to the accounts." />
                                </td>
                            </tr>
                            <tr>
                                <td class="normal1" align="right" style="width: 55%;">Base Currency
                                </td>
                                <td>
                                    <%--  <asp:DropDownList ID="ddlCurrency" runat="server" CssClass="signup">
                                            </asp:DropDownList>--%>
                                            &nbsp;
                                   <%-- <asp:HyperLink ID="hplSetCurrency" runat="server" Text="Configure" onclick="return OpenCurrency();"
                                        CssClass="hyperlink">
                                    </asp:HyperLink>--%>
                                </td>
                            </tr>
                            <tr style="width: 55%;">
                                <td class="normal1" align="right">Enable Multi Currency
                                </td>
                                <td>
                                    <%--<asp:CheckBox ID="chkMultiCurrency" runat="server" />--%>
                                </td>
                                <td class="normal1" align="right" style="display: none;">
                                    <label for="chkEmbeddedCost">
                                        Enable Embedded Cost</label>
                                </td>
                                <td align="left" style="display: none;">
                                    <asp:CheckBox ID="chkEmbeddedCost" runat="server" CssClass="signup" />
                                    &nbsp;<asp:HyperLink Text="Configure" CssClass="hyperlink" runat="server" ID="hplConfigEmbeddedCost"></asp:HyperLink>
                                </td>
                            </tr>
                        </table>
                            </center>
                    </asp:TableCell>
                    <asp:TableCell VerticalAlign="Top" Style="display: none">

                        <table width="100%" class="aspTable">

                            <tr class="normal1" align="right">
                                <td align="left" class="normal1" colspan="2">
                                    <div style="margin-bottom: 5px; margin-left: 20px; padding-top: 5px; border: solid 1px lightgray; overflow: hidden;">
                                        <div style="margin-top: 8px; margin-left: 12px;">
                                            When Bill payment is made against BizDoc based Bills, issued from within a Purchase Order (PO):
                                        </div>
                                        <div id="liRule15" runat="server" style="margin-top: 8px; margin-left: 12px;">
                                            If a balance due is left on the Bill, change Bill status value to
                        <asp:DropDownList ID="ddlBizDocStatus15" runat="server" CssClass="signup">
                        </asp:DropDownList>
                                        </div>
                                        <div id="liRule16" runat="server" style="margin-top: 8px; margin-left: 12px;">
                                            If no balance due is left (meaning that it�s been paid in full), then change Bill status value to
                        <asp:DropDownList ID="ddlBizDocStatus16" runat="server" CssClass="signup">
                        </asp:DropDownList>


                                        </div>
                                        <br />
                                    </div>
                                </td>
                            </tr>
                        </table>


                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>


            <asp:Table ID="table6" CellPadding="0" CellSpacing="0" BorderWidth="1" runat="server"
                Width="100%" CssClass="aspTable" BorderColor="black" GridLines="None" Height="350" Style="display: none">
                <asp:TableRow>
                    <asp:TableCell VerticalAlign="Top">
                        <table border="0">
                            <tr>
                                <td class="normal1" align="right">Default map class to
                                            
                                </td>
                                <td class="normal1" align="left" colspan="3">
                                    <%-- <asp:RadioButtonList ID="rblDefaultClass" runat="server" CssClass="signup" RepeatDirection="Horizontal" RepeatLayout="Flow">
                                                <asp:ListItem Text="None" Value="0" Selected="True"></asp:ListItem>
                                                <asp:ListItem Text="User" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="Company" Value="2"></asp:ListItem>
                                            </asp:RadioButtonList>--%>
                                </td>
                            </tr>
                            <tr>
                                <td class="normal1" align="right">Turn on �Deferred Income�:
                                </td>
                                <td align="left">
                                    <%--<asp:CheckBox ID="chkEnableDiferredIncome" runat="server" />
                                            <asp:Label ID="Label18" Text="?" CssClass="tip" runat="server" title="Deferred income is a liability because it refers to revenue that has not yet been earned, but represents products or services that are owed to the customer. As the product or service is delivered over time, it is recognized as revenue on the income statement.<br /><br />Checking this feature will enable you to defer income for line items within Sales Orders using the BizDocs sub-tab that are not already Invoiced (e.g. say you have an SO with 1 line item with 10 units, and  you invoice it for 6. This means you can defer a maximum qty of 4 for that item)." />--%>
                                </td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td class="normal1" align="right">Discount Service Item:</td>
                                <td align="left">
                                    <%--<asp:DropDownList ID="ddlDiscountServiceItem" runat="server" CssClass="signup" Width="200px">
                                            </asp:DropDownList>--%>
                                </td>
                                <td class="normal1" align="right">&nbsp;Keep Credit Card Values in Database?:
                                </td>
                                <td align="left">
                                    <%-- <asp:CheckBox ID="chkKeepCreditCardInfo" Text="" runat="server" onclick="return AcceptTerms();" />--%>
                                </td>
                            </tr>
                            <tr>
                                <td class="normal1" align="right">Shipping Service Item:
                                </td>
                                <td align="left">
                                    <%-- <asp:DropDownList ID="ddlShippingServiceItem" runat="server" CssClass="signup" Width="200px">
                                            </asp:DropDownList>--%>
                                </td>
                                <td class="normal1" align="right">Default month on which accounting reports should start
                                </td>
                                <td align="left">
                                    <%-- <asp:DropDownList CssClass="signup" ID="ddlFiscalStartMonth" runat="server">
                                                <asp:ListItem Value="1">January</asp:ListItem>
                                                <asp:ListItem Value="2">February</asp:ListItem>
                                                <asp:ListItem Value="3">March</asp:ListItem>
                                                <asp:ListItem Value="4">April</asp:ListItem>
                                                <asp:ListItem Value="5">May</asp:ListItem>
                                                <asp:ListItem Value="6">June</asp:ListItem>
                                                <asp:ListItem Value="7">July</asp:ListItem>
                                                <asp:ListItem Value="8">August</asp:ListItem>
                                                <asp:ListItem Value="9">September</asp:ListItem>
                                                <asp:ListItem Value="10">October</asp:ListItem>
                                                <asp:ListItem Value="11">November</asp:ListItem>
                                                <asp:ListItem Value="12">December</asp:ListItem>
                                            </asp:DropDownList>--%>
                                </td>
                            </tr>
                            <tr>
                                <td class="normal1" align="right">Decimal Points (Only for BizDoc Popup):
                                </td>
                                <td align="left">
                                    <%--   <asp:DropDownList ID="ddlDecimalPoints" runat="server" CssClass="signup">
                                                <asp:ListItem Text="1" Value="1">
                                                </asp:ListItem>
                                                <asp:ListItem Text="2" Value="2">
                                                </asp:ListItem>
                                                <asp:ListItem Text="3" Value="3">
                                                </asp:ListItem>
                                                <asp:ListItem Text="4" Value="4">
                                                </asp:ListItem>
                                            </asp:DropDownList>--%>
                                </td>
                                <td class="normal1" align="right"></td>
                                <td align="left"></td>
                            </tr>
                            <tr>
                                <td class="normal1" align="right"></td>
                                <td align="left"></td>
                                <td class="normal1" align="right">Show Income, Expense,COGs balances in Chart of Accounts
                                </td>
                                <td align="left">
                                    <asp:CheckBox runat="server" ID="chkShowBalance" />
                                </td>
                            </tr>
                            <tr>
                                <td class="normal1" align="right">Income Account
                                </td>
                                <td align="left">
                                    <asp:DropDownList ID="ddlIncomeAccount" runat="server" CssClass="signup" Width="200px">
                                    </asp:DropDownList>
                                </td>
                                <td align="right">Calculate taxes on Purchases (e.g. P.Os)
                                </td>
                                <td>
                                    <%-- <asp:CheckBox runat="server" ID="chkPurchaseTaxCredit" />
                                            <asp:HiddenField ID="hdnPurchaseTaxCredit" runat="server" Value="0" />--%>
                                </td>
                            </tr>
                            <tr>
                                <td class="normal1" align="right">Asset Account
                                </td>
                                <td align="left">
                                    <asp:DropDownList ID="ddlAssetAccount" runat="server" CssClass="signup" Width="200px">
                                    </asp:DropDownList>
                                </td>
                                <td align="right">Enable Project Tracking
                                </td>
                                <td>
                                    <asp:CheckBox runat="server" ID="chkProjectTracking" />
                                </td>
                            </tr>
                            <tr>
                                <td class="normal1" align="right">COGS
                                </td>
                                <td align="left">
                                    <asp:DropDownList ID="ddlCOGS" runat="server" CssClass="signup" Width="200px">
                                    </asp:DropDownList>
                                </td>
                                <td align="right">Enable Campaign Tracking
                                </td>
                                <td>
                                    <asp:CheckBox runat="server" ID="chkCampaignTracking" />
                                </td>
                            </tr>
                            <tr>
                                <td align="right">Use Landed Cost
                                </td>
                                <td>
                                    <%-- <asp:CheckBox runat="server" ID="chkLandedCost" />--%>
                                </td>
                                <td align="right">Landed Cost Default to the following:                 
                                </td>
                                <td>
                                    <%--<asp:RadioButtonList runat="server" ID="rblLandedCost" RepeatDirection="Horizontal">
                                                <asp:ListItem>Cubic Size</asp:ListItem>
                                                <asp:ListItem>Weight</asp:ListItem>
                                                <asp:ListItem Selected="True">Amount</asp:ListItem>
                                            </asp:RadioButtonList>--%>
                                </td>
                            </tr>
                            <tr>
                                <td class="normal1" align="right">
                                    <%--<asp:HyperLink ID="hplLandedCostVendors" runat="server" Text="Landed Cost Vendors" onclick="return OpenLandedCostVendors();"
                                                CssClass="hyperlink">
                                            </asp:HyperLink>--%>
                                </td>
                                <td align="left"></td>
                                <td class="normal1" align="right">
                                    <%--   <asp:HyperLink ID="hplLandedCostExpenseAccount" runat="server" Text="Landed Cost Expense Account" onclick="return OpenLandedCostExpenseAccount();"
                                                CssClass="hyperlink">
                                            </asp:HyperLink>--%>
                                </td>
                                <td align="left"></td>
                            </tr>
                        </table>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
            <telerik:RadMultiPage ID="radMultiPage_AccountingTab" Visible="false" runat="server" SelectedIndex="0" CssClass="pageView">
                <telerik:RadPageView ID="radPageView_GeneralAcc" runat="server">
                </telerik:RadPageView>
                <telerik:RadPageView ID="radPageView_Taxes" runat="server">
                    <asp:Table ID="table5" CellPadding="0" CellSpacing="0" BorderWidth="1" runat="server"
                        Width="100%" CssClass="aspTable" BorderColor="black" GridLines="None" Height="350">
                        <asp:TableRow>
                            <asp:TableCell VerticalAlign="Top">
                                <table class="table table-responsive tblNoBorder">
                                    <tr>

                                        <td class="normal1" align="right">Base tax calculation on :
                                        </td>
                                        <td align="left">
                                            <asp:RadioButtonList ID="rblBaseTax" runat="server" RepeatDirection="Horizontal"
                                                CssClass="signup">
                                                <asp:ListItem Text="Shipping Address" Value="2" Selected="True">
                                                </asp:ListItem>
                                                <asp:ListItem Text="Billing Address" Value="1">
                                                </asp:ListItem>
                                            </asp:RadioButtonList>
                                        </td>
                                        <td></td>

                                    </tr>
                                    <tr>
                                        <td class="normal1" align="right">Base tax on :
                                        </td>
                                        <td align="left">
                                            <asp:RadioButtonList ID="rblBaseTaxOnArea" runat="server" RepeatDirection="Horizontal"
                                                CssClass="signup">
                                                <asp:ListItem Text="State" Value="0" Selected="True">
                                                </asp:ListItem>
                                                <asp:ListItem Text="City" Value="1">
                                                </asp:ListItem>
                                                <asp:ListItem Text="Zip Code/Postal" Value="2">
                                                </asp:ListItem>
                                            </asp:RadioButtonList>
                                        </td>
                                        <td class="normal1" align="left">
                                            <asp:HyperLink ID="hplTaxCountryConfi" runat="server" Text="Tax Country Configure"
                                                onclick="return OpenTaxCountryConfi();" CssClass="hyperlink">
                                            </asp:HyperLink>
                                        </td>
                                    </tr>
                                </table>
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                </telerik:RadPageView>
                <telerik:RadPageView ID="radPageView_Terms" runat="server">
                </telerik:RadPageView>
            </telerik:RadMultiPage>
        </telerik:RadPageView>
        <telerik:RadPageView ID="radPageView_Portal" runat="server" Visible="false">
            <center><h4 style="margin-bottom: 30px; margin-top: 9px;"><b>GLOBAL PORTAL SETTINGS</b></h4></center>
            <asp:Table ID="table4" CellPadding="0" CellSpacing="0" BorderWidth="1" runat="server"
                Width="100%" CssClass="aspTable" BorderColor="black" GridLines="None" Height="350">
                <asp:TableRow>
                    <asp:TableCell VerticalAlign="Top">
                        <center>
                            <table class="table table-responsive tblNoBorder">
                                <tr>
                                    <td colspan="2">Change Tab Labels / Click to hide tabs </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:TextBox ID="txtSalesOrderTabs" style="width:200px" runat="server"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:CheckBox ID="chkSalesOrderTabs" runat="server"></asp:CheckBox>
                                    </td>
                                </tr>
                                 <tr>
                                    <td>
                                        <asp:TextBox ID="txtSalesQuotesTabs" style="width:200px" runat="server"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:CheckBox ID="chkSalesQuotesTabs" runat="server"></asp:CheckBox>
                                    </td>
                                </tr>
                                 <tr>
                                    <td>
                                        <asp:TextBox ID="txtItemPurchaseHistoryTabs" style="width:200px" runat="server"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:CheckBox ID="chkItemPurchaseHistoryTabs" runat="server"></asp:CheckBox>
                                    </td>
                                </tr>
                                 <tr>
                                    <td>
                                        <asp:TextBox ID="txtItemsFrequentlyPurchasedTabs" style="width:200px" runat="server"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:CheckBox ID="chkItemsFrequentlyPurchasedTabs" runat="server"></asp:CheckBox>
                                    </td>
                                </tr>
                                 <tr>
                                    <td>
                                        <asp:TextBox ID="txtOpenCasesTabs" style="width:200px" runat="server"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:CheckBox ID="chkOpenCasesTabs" runat="server"></asp:CheckBox>
                                    </td>
                                </tr>
                                 <tr>
                                    <td>
                                        <asp:TextBox ID="txtOpenRMATabs" style="width:200px" runat="server"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:CheckBox ID="chkOpenRMATabs" runat="server"></asp:CheckBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:TextBox ID="txtSupportTabs" style="width:200px" runat="server"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:CheckBox ID="chkSupportTabs" runat="server"></asp:CheckBox>
                                    </td>
                                </tr>
                            </table>
                        <table class="table table-responsive tblNoBorder" style="display:none">
                            <tr>
                                <td class="normal1" align="right">No of Characters for Item Search
                                </td>
                                <td align="left">
                                    <asp:DropDownList ID="ddlChrItemSearch" runat="server">
                                        <asp:ListItem Value="0">0</asp:ListItem>
                                        <asp:ListItem Value="1">1</asp:ListItem>
                                        <asp:ListItem Value="2">2</asp:ListItem>
                                        <asp:ListItem Value="3">3</asp:ListItem>
                                        <asp:ListItem Value="4">4</asp:ListItem>
                                        <asp:ListItem Value="5">5</asp:ListItem>
                                    </asp:DropDownList>
                                    <asp:Label ID="Label3" Text="?" CssClass="tip" runat="server" title="When creating an order, adding characters into the item field in order for the system to select an item for the order is required. This sets how many characters are required before the system attempts to guess which item you�re trying to add to the order (e.g. If setting is 0 or 1 then typing �G� will find all items beginning with �G�, if it�s 2, Biz will require 2 characters, so typing �G� will result in nothing, but typing �Gr� will result in Biz trying to find all items beginning with �Gr�.)." />
                                </td>

                                <td class="normal1" align="right">Display BizDocs on Portal / WebStore
                                </td>
                                <td align="left" class="normal1">
                                    <asp:HyperLink ID="HyperLink1" runat="server" Text="Set" onclick="return PortalBizDocs();"
                                        CssClass="hyperlink">
                                    </asp:HyperLink>
                                </td>
                            </tr>
                            <tr class="normal1" align="right">
                                <td>Portal Logo
                                </td>
                                <td align="left" class="normal1">
                                    <asp:FileUpload ID="FUPortalLogo" runat="server" CssClass="signup" />
                                    (Recommended Image height: 43 pixels)
                                    &nbsp; <asp:LinkButton ID="btnSaveProfile" runat="server" CssClass="btn btn-primary" Style="float: right"><i class="fa fa-floppy-o"></i>&nbsp;&nbsp;Save Profile</asp:LinkButton>
                                </td>
                            </tr>
                            <tr>
                                <td class="normal1" align="right">Customer Portal :
                                </td>
                                <td align="left" class="normal1">
                                    <%--<asp:Label ID="Label18" Text="?" CssClass="tip" runat="server" title="<b>Which portal tabs (modules) do you want to expose ?</b><br /><p><b>Step 1.</b> Click on �Tab Management�, then �Hide / Show Main Tabs�.<br /><b>Step 2.</b> From the �Group Drop-down�, click on �Default Extranet�. <br /><b>Step 3.</b> Selecting a relationship, then adding tabs to your portal, will give ANY external user belonging to that relationship, access to those tabs/modules when the log into the portal. To further filter access, you can set tab access by profile but you�ll need to remove all tabs from relationship only settings first. What�s an External User ? External users can be created from your shopping cart (�create account�), or by clicking on Administration | User Administration | External Users. To add External Users you first have to add a new Organization record (Relationship) then add contact users from that record.</p><br /><p><b>Which dashboard summary dashboard reports (aka �Portlets�) do you want to expose ?</b></p><br /><p><b>Step 1.</b> Click on �Tab Management�, then �Hide / Show Main Tabs� .<br /><b>Step 2.</b> Click on �Portal Dashboard Setup�.<br /><b>Step 3.</b> Selecting a relationship, then drag and dropping portlets you want to expose to column 1 or 2 (these are default locations, users can also drag and drop portlets within the dashboard). This will give ANY external user belonging to that relationship, access to those Portlets when the log into the Dashboard tab within the portal. To further filter access, you can set to expose Portlets by profile but you�ll need to remove all Portlets from relationship only settings first.</p>"></asp:Label>
                                    &nbsp;http://<asp:TextBox runat="server" ID="txtPortalName" CssClass="signup" Width="150px"
                                        MaxLength="20" />.bizautomation.com&nbsp;&nbsp;
                                    <asp:Button Text="Create Portal" runat="server" ID="btnCreatePortal" CssClass="button" />
                                    &nbsp;--%>
                                     Preview:&nbsp;
                                    <asp:HyperLink Target="_blank" ID="hplCusPortal" runat="server" CssClass="hyperlink">
                                    </asp:HyperLink>
                                </td>
                            </tr>
                            <tr>
                                <td align="right"></td>
                                <td align="left">
                                    <asp:HyperLink ID="hplPortalColumnSettings" runat="server" CssClass="hyperlink" Text="Portal Grid - Column Control" onclick="return PortalColumnSettings();">
                                    </asp:HyperLink>&nbsp;&nbsp;&nbsp;
                                    <asp:HyperLink ID="hplPortalDashboard" runat="server" CssClass="hyperlink" onclick="openPortal()"
                                        Text="Portal Dashboard Setup"></asp:HyperLink>
                                    <%--<div style="padding: 20px; margin-bottom: 5px; margin-left: 25px; border: solid 1px lightgray; overflow: hidden;">
                                        <div style="width: 50%; float: left; text-align: center;">
                                            <div style="vertical-align: central; padding: 20px;">
                                                <asp:Image ID="Image6" runat="server" ImageUrl="~/images/PortalImage.png" />
                                            </div>
                                        </div>
                                        <div style="width: 50%; float: right;">
                                            <div style="vertical-align: central; padding: 25px;">
                                                
                                            </div>
                                        </div>
                                    </div>--%>
                                </td>
                            </tr>
                            <%--<tr>
                                <td class="normal1" align="right">Enable Portal Customization:
                                </td>
                                <td align="left">
                                    <asp:CheckBox runat="server" ID="chkEnablePortalCustomization" Text="" />&nbsp;&nbsp;
                                    <asp:HyperLink ID="hplCustomizePortal" runat="server" CssClass="hyperlink" Text="Customize Portal"
                                        NavigateUrl="~/ECommerce/frmCssList.aspx?Type=2">
                                    </asp:HyperLink>
                                </td>
                            </tr>--%>
                            <tr>
                                <td align="right" valign="top">Hide Tabs from Portal :
                                </td>
                                <td>
                                    <asp:CheckBoxList ID="chkLstHideTabs" runat="server">
                                        <asp:ListItem Selected="false" Text="Hide DashBoard" Value="1"></asp:ListItem>
                                        <asp:ListItem Selected="False" Text="Hide Sales Order" Value="2"></asp:ListItem>
                                        <asp:ListItem Selected="False" Text="Hide Purchase Order" Value="3"></asp:ListItem>
                                        <asp:ListItem Selected="False" Text="Hide Cases" Value="4"></asp:ListItem>
                                        <asp:ListItem Selected="False" Text="Hide Knowledge Base" Value="5"></asp:ListItem>
                                        <asp:ListItem Selected="False" Text="Hide Projects" Value="6"></asp:ListItem>
                                        <asp:ListItem Selected="False" Text="Hide Documents" Value="7"></asp:ListItem>
                                    </asp:CheckBoxList>
                                </td>
                            </tr>
                            <%-- <tr>
                                <td>Secondory Sort column :
                                    <asp:Label ID="Label3" Text="?" CssClass="tip" runat="server" title="This column will sort cart items after selected sort column within item cart." />
                                </td>
                                <td>
                                    
                                </td>
                            </tr>--%>
                        </table></center>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </telerik:RadPageView>
    </telerik:RadMultiPage><%--<asp:Button Text="Test" runat="server" id="btntest"/>--%><asp:TextBox ID="hdnDivisionValue" runat="server" Style="display: none">
    </asp:TextBox><asp:HiddenField ID="hfPortalLogo" runat="server" />
    <asp:HiddenField ID="hdnOldPortalName" runat="server" />
    <div id="modalReorderPoint" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Re-Order Point</h4>
                </div>
                <div class="modal-body">
                    <div class="row padbottom10">
                        <div class="col-xs-12">
                            <ul class="list-inline">
                                <li><a data-toggle="title" data-placement="bottom" title="Values will automatically set for subscribed items (Check box next to re-order value in item record). Example: Assume 30 days is selected, and an item sold 300 units in that period. Daily average (30) x Primary Vendor lead time (days) which is set from that vendor�s Ship-to address link (assume it�s 30 days) = a Re-Order point of 300. Re-Order Qty would be the 300 at 0% selected, or 330 if 10% is selected (etc.).">?</a><b>Re-Order Point:</b> Based on</li>
                                <li style="padding-left: 0px; padding-right: 0px;">
                                    <select id="ddlReorderPointDays" class="form-control">
                                        <option value="30">30</option>
                                        <option value="60">60</option>
                                        <option value="90">90</option>
                                    </select></li>
                                <li>days of unit sales</li>
                            </ul>
                            <input type="radio" name="ReorderPointBasedOn" value="1" checked="checked" /><label>Period ending this week</label>
                            <br />
                            <input type="radio" name="ReorderPointBasedOn" value="2" /><label>Period ending 1 year ago</label>
                        </div>
                    </div>
                    <div class="row padbottom10">
                        <div class="col-xs-12">
                            <ul class="list-inline">
                                <li><b>Re-Order Qty: </b>Re-Order Point + </li>
                                <li style="padding-left: 0px;">
                                    <select id="ddlReorderPointPercent" class="form-control">
                                        <option value="0">0</option>
                                        <option value="5">5</option>
                                        <option value="10">10</option>
                                    </select>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" id="btnSaveReorderPoint" onclick="return SaveReorderPoint();">Save</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        // select all desired input fields and attach titles to them
        $("#form1").find("span.tip").title({

            // place title on the right edge
            position: "center right",

            // a little tweaking of the position
            offset: [-2, 10],

            // use the built-in fadeIn/fadeOut effect
            effect: "fade",

            // custom opacity setting
            opacity: 0.7

        });
    </script>

</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="DetailPageTitle" runat="server"
    ClientIDMode="Static">
    Global Settings
</asp:Content>
