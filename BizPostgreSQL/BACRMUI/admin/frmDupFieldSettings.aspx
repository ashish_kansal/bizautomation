﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmDupFieldSettings.aspx.vb"
    Inherits=".frmDupFieldSettings" MasterPageFile="~/common/Popup.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Column Customization</title>
    <script language="javascript" src="../javascript/AdvSearchScripts.js"></script>
    <script language="javascript" type="text/javascript">
        function Save() {
            var str = '';
            for (var i = 0; i < document.getElementById("lstSelectedfld").options.length; i++) {
                var SelectedValue;
                SelectedValue = document.getElementById("lstSelectedfld").options[i].value;
                str = str + SelectedValue + ','
            }
            document.getElementById("hdnCol").value = str;
            //  alert( hdnCol.value)
        }
		
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <div class="input-part">
        <div class="right-input">
            <asp:Button runat="server" ID="btnSave" Text="Save" CssClass="button" Width="50" />
            <input class="button" id="btnClose" style="width: 50" onclick="javascript:window.close()"
                type="button" value="Close">&nbsp;&nbsp;
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageTitle" runat="server" ClientIDMode="Static">
  Duplicate Field(s) Search
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="Content" runat="server" ClientIDMode="Static">
    <table cellpadding="0" cellspacing="0" width="600px">
        <tr>
            <td align="center" class="normal1" valign="top">
                Available Fields<br>
                &nbsp;&nbsp;
                <asp:ListBox ID="lstAvailablefld" runat="server" Width="150" Height="200" CssClass="signup"
                    EnableViewState="False">
                    <asp:ListItem Text="Organization Name" Value="1"></asp:ListItem>
                    <asp:ListItem Text="Organization Website" Value="2"></asp:ListItem>
                    <asp:ListItem Text="Organization Phone" Value="3"></asp:ListItem>
                    <asp:ListItem Text="Contact First Name" Value="4"></asp:ListItem>
                    <asp:ListItem Text="Contact Last Name" Value="5"></asp:ListItem>
                    <asp:ListItem Text="Contact Phone" Value="6"></asp:ListItem>
                    <asp:ListItem Text="Contact Email" Value="7"></asp:ListItem>
                </asp:ListBox>
            </td>
            <td align="center" class="normal1" valign="middle">
                <input type="button" id="btnAdd" class="button" value="Add >" onclick="javascript:move(lstAvailablefld,lstSelectedfld)">
                <br>
                <br>
                <input type="button" id="btnRemove" class="button" value="< Remove" onclick="javascript:remove1(lstSelectedfld,lstAvailablefld);">
            </td>
            <td align="center" class="normal1">
                Selected Fields<br>
                <asp:ListBox ID="lstSelectedfld" runat="server" Width="150" Height="200" CssClass="signup"
                    EnableViewState="False"></asp:ListBox>
            </td>
            <td align="center" class="normal1" valign="middle">
            </td>
        </tr>
    </table>
    <input id="hdnCol" type="hidden" name="hdXMLString" runat="server" value="" />
    <input id="hdSave" type="hidden" name="hdSave" runat="server" value="False" />
</asp:Content>
