﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmAdvAcc.aspx.vb" Inherits="BACRM.UserInterface.Admin.frmAdvAcc"
    MasterPageFile="~/common/DetailPage.Master" %>

<%@ Import Namespace="BACRM.BusinessLogic.Admin" %>
<%@ Import Namespace="BACRM.BusinessLogic.Reports" %>
<%@ Register TagPrefix="menu1" TagName="webmenu" Src="../include/webmenu.ascx" %>
<%@ Register TagPrefix="BizCalendar" TagName="Calendar" Src="../include/calandar.ascx" %>
<%@ Register Src="frmAdvSearchCriteria.ascx" TagName="frmAdvSearchCriteria" TagPrefix="uc1" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Import Namespace="BACRM.BusinessLogic.Common" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Advanced Financial Transaction Search</title>
    <link rel="stylesheet" type="text/css" href="../CSS/jscal2.css" />
    <link rel="stylesheet" type="text/css" href="../CSS/border-radius.css" />
    <script type="text/javascript" src="../JavaScript/jscal2.js"></script>
    <script type="text/javascript" src="../JavaScript/en.js"></script>
    <script src="../javascript/AdvSearchScripts.js" type="text/javascript"></script>
    <script src="../javascript/AdvSearchColumnCustomization.js"
        type="text/javascript"></script>
    <script language="vb" runat="server">
        Private Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
            Try
                Dim str As String = ""
                Dim sbFriendlyQuery As New StringBuilder
               
                'Crete/Mofified Date  filter
                If rbFixedDate.Checked = True Then 'fixed date
                    Session("TimeQuery") = GetDateQuery(calFrom.SelectedDate, calTo.SelectedDate)
                    'If chkCreated.Checked Or chkModified.Checked Then
                    FormGenericAdvSearch.GenerateUserFriendlySearchString(sbFriendlyQuery, "Transaction Date ", "rbFixedDate", calFrom.SelectedDate & " and " & calTo.SelectedDate, "", "Between")
                    'End If
                ElseIf rbSlidingDate.Checked = True Then 'sliding date
                    Select Case ddlDateSpan.SelectedValue
                        Case 365
                            Session("TimeQuery") = GetDateQuery(CStr(New Date(Now.Year, 1, 1)), CStr(Date.Today))
                        Case Else
                            Session("TimeQuery") = GetDateQuery(Date.Today.AddDays(-ddlDateSpan.SelectedValue), Date.Today)
                    End Select
                    Session("SlidingDays") = ddlDateSpan.SelectedValue
                   
                    FormGenericAdvSearch.GenerateUserFriendlySearchString(sbFriendlyQuery, "Transaction Date ", "ddlDateSpan", ddlDateSpan.SelectedItem.Text, "", "Equal To")
                End If
                
                Dim i As Integer
                Dim ControlType, strFieldName, strFieldValue, strFieldID, strFriendlyName, strControlID As String
                Dim dr As System.Data.DataRow
                
                For i = 0 To dtGenericFormConfig.Rows.Count - 1
                    dr = dtGenericFormConfig.Rows(i)
                    strFieldID = dr("numFormFieldID").ToString.Replace("C", "").Replace("D", "").Replace("R", "")
                    ControlType = dr("vcAssociatedControlType").ToString.ToLower.Replace(" ", "") 'replace whitespace with blank 
                    strFieldName = dr("vcDbColumnName").ToString.Trim.Replace(" ", "_")
                    strFriendlyName = dr("vcFormFieldName").ToString
                    strControlID = dr("numFormFieldID").ToString & "_" & dr("vcDbColumnName").ToString.Trim.Replace(" ", "_")
                    
                    Select Case ControlType
                        Case "selectbox"
                            strFieldValue = IIf(CType(tblMain.FindControl(strControlID), DropDownList).SelectedIndex > 0, CType(tblMain.FindControl(strControlID), DropDownList).SelectedValue, 0)
                            If IsNumeric(strFieldValue) AndAlso strFieldValue > 0 Then FormGenericAdvSearch.GenerateUserFriendlySearchString(sbFriendlyQuery, strFriendlyName, strFieldName, CType(tblMain.FindControl(strControlID), DropDownList).SelectedItem.Text, hdnSaveSearchCriteria.Value.Trim, "Equal To")
                        Case "listbox"
                            'Create Only User Friendly Query here, perform actuall query at bottom
                            strFieldValue = ""
                            Dim lstBox As Telerik.Web.UI.RadComboBox = CType(tblMain.FindControl(strControlID), Telerik.Web.UI.RadComboBox)
                            Dim fldText As String = ""

                            For Each item As RadComboBoxItem In lstBox.CheckedItems
                                If CCommon.ToLong(item.Value) > 0 Then
                                    strFieldValue = strFieldValue & item.Value & ","
                                    fldText = fldText & item.Text & " or "
                                End If
                            Next
                            
                            strFieldValue = strFieldValue.TrimEnd(",")
                            
                            If strFieldValue.Length > 1 Then FormGenericAdvSearch.GenerateUserFriendlySearchString(sbFriendlyQuery, strFriendlyName, strFieldName, fldText.Trim.TrimEnd("r").TrimEnd("o"), hdnSaveSearchCriteria.Value.Trim, "Equals To")
                        Case "checkbox"
                            strFieldValue = IIf(CType(tblMain.FindControl(strControlID), DropDownList).SelectedIndex > 0, CType(tblMain.FindControl(strControlID), DropDownList).SelectedValue, -1)
                            If CCommon.ToLong(strFieldValue) > -1 Then FormGenericAdvSearch.GenerateUserFriendlySearchString(sbFriendlyQuery, strFriendlyName, strFieldName, CType(tblMain.FindControl(strControlID), DropDownList).SelectedItem.Text, hdnSaveSearchCriteria.Value.Trim, "Equal To")
                        Case "datefield"
                            strFieldValue = CType(tblMain.FindControl(strControlID), BACRM.Include.calandar).SelectedDate
                            If CType(tblMain.FindControl("hdn_" & strControlID), HiddenField).Value = 11 Then 'between condition is set
                                If strFieldValue <> "" Then FormGenericAdvSearch.GenerateUserFriendlySearchString(sbFriendlyQuery, strFriendlyName, strFieldName, CType(tblMain.FindControl("hdn_" & strControlID & "_value"), HiddenField).Value, hdnSaveSearchCriteria.Value.Trim, "Between")
                            Else
                                If strFieldValue <> "" Then FormGenericAdvSearch.GenerateUserFriendlySearchString(sbFriendlyQuery, strFriendlyName, strFieldName, strFieldValue, hdnSaveSearchCriteria.Value.Trim, "Equal To")
                            End If
                        Case Else
                            strFieldValue = Replace(CType(tblMain.FindControl(strControlID), TextBox).Text.Trim, "'", "''")
                            If strFieldValue <> "" Then FormGenericAdvSearch.GenerateUserFriendlySearchString(sbFriendlyQuery, strFriendlyName, strFieldName, strFieldValue, hdnSaveSearchCriteria.Value.Trim, IIf(dr("vcFieldDataType").ToString.ToLower = "v", "Contains", "Equal To"))
                    End Select
                    
                    
                    Dim Prefix As String = ""
                    If dr("vcLookBackTableName") = "CompanyInfo" Then
                        Prefix = "C."
                    ElseIf dr("vcLookBackTableName") = "DivisionMaster" Then
                        Prefix = "DM."
                    ElseIf dr("vcLookBackTableName") = "General_Journal_Header" Then
                        Prefix = "GJH."
                    ElseIf dr("vcLookBackTableName") = "General_Journal_Details" Then
                        Prefix = "GJD."
                    ElseIf dr("vcLookBackTableName") = "CheckHeader" Then
                        Prefix = "CH."
                    End If
                    
                    If ControlType = "selectbox" Or ControlType = "checkbox" Then
                        If strFieldValue <> "0" Then
                            If strFieldName = "numAssignedTo" Then
                                str = str & " and (" & FormGenericAdvSearch.GetSearchCondition(hdnFilterCriteria.Value, "GJH.numCreatedBy", strControlID, strFieldValue, dr("vcFieldDataType"))
                                str = str & " or " & FormGenericAdvSearch.GetSearchCondition(hdnFilterCriteria.Value, "GJH.numModifiedBy", strControlID, strFieldValue, dr("vcFieldDataType")) & ")"
                            Else
                                ''Based On the Look Back Table Sub Queries 
                                str = str & " and " & FormGenericAdvSearch.GetSearchCondition(hdnFilterCriteria.Value, strFieldName, strControlID, strFieldValue, dr("vcFieldDataType"), TablePrefix:=Prefix)
                            End If
                        End If
                    ElseIf ControlType = "listbox" Then
                        If strFieldValue.ToString.Trim.Length > 0 Then
                            str = str & " and " & Prefix & strFieldName & " in (" & strFieldValue & ")"
                        End If
                    ElseIf ControlType = "datefield" Then
                        If CCommon.ToString(strFieldValue).Length > 0 Then
                            If hdnFilterCriteria.Value.ToLower.IndexOf(strFieldName & "~11") >= 0 Then
                                str = str & " and " & FormGenericAdvSearch.GetSearchCondition(hdnFilterCriteria.Value, strFieldName, strControlID, strFieldValue, dr("vcFieldDataType"))
                            Else
                                Dim dt As Date = DateFromFormattedDate(strFieldValue, Session("DateFormat"))
                                str = str & " and " & strFieldName & " = '" & dt.Year.ToString + "-" + dt.Month.ToString + "-" + dt.Day.ToString + "'"
                            End If
                        End If
                    Else 'EditBox
                        If CType(tblMain.FindControl(strControlID), TextBox).Text <> "" Then
                            If strFieldName = "monAmount" Then
                                str = str & " and (" & FormGenericAdvSearch.GetSearchCondition(hdnFilterCriteria.Value, "GJD.numCreditAmt", strControlID, strFieldValue, dr("vcFieldDataType"))
                                str = str & " or " & FormGenericAdvSearch.GetSearchCondition(hdnFilterCriteria.Value, "GJD.numDebitAmt", strControlID, strFieldValue, dr("vcFieldDataType")) & ")"
                            ElseIf strFieldName = "vcReference" Then
                                str = str & " and (" & FormGenericAdvSearch.GetSearchCondition(hdnFilterCriteria.Value, strFieldName, strControlID, strFieldValue, dr("vcFieldDataType"), TablePrefix:=Prefix) & ")"
                                'str = str & " or " & FormGenericAdvSearch.GetSearchCondition(hdnFilterCriteria.Value, "VJC.Narration", strControlID, strFieldValue, dr("vcFieldDataType")) & ")"
                            ElseIf strFieldName = "varDescription" Then
                                str = str & " and (" & FormGenericAdvSearch.GetSearchCondition(hdnFilterCriteria.Value, "GJD.varDescription", strControlID, strFieldValue, dr("vcFieldDataType"))
                                str = str & " or " & FormGenericAdvSearch.GetSearchCondition(hdnFilterCriteria.Value, "GJH.varDescription", strControlID, strFieldValue, dr("vcFieldDataType")) & ")"
                            Else : str = str & " and " & FormGenericAdvSearch.GetSearchCondition(hdnFilterCriteria.Value, strFieldName, strControlID, strFieldValue, dr("vcFieldDataType"), TablePrefix:=Prefix)
                            End If
                        End If
                    End If
                Next
                
                FormGenericAdvSearch.SaveSearchCriteria(hdnSaveSearchCriteria.Value, 15)
                
                Session("WhereContditionAcc") = str
                Session("SavedSearchCondition") = str
                Session("UserFriendlyQuery") = sbFriendlyQuery.ToString.Trim.TrimEnd(New Char() {"d", "n", "a"})

                Response.Redirect("../Admin/frmAdvAccRes.aspx")
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub
        Function GetListBoxSelectedValues(ByVal lb As ListBox, ByRef fldText1 As String) As String
            Try
                Dim strFieldValue As String = ""
                Dim fldText As String = ""
                For Each Item As ListItem In lb.Items
                    If Item.Selected Then
                        strFieldValue = strFieldValue & Item.Value & ","
                        fldText = fldText & Item.Text & " or "
                    End If
                Next
                strFieldValue = strFieldValue.TrimEnd(",")
                fldText1 = fldText
                Return strFieldValue
            Catch ex As Exception
                Throw
            End Try
        End Function
    
        ''' <summary>
        ''' returns Date query for saved search
        ''' </summary>
        ''' <param name="strFromDate"></param>
        ''' <param name="strToDate"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Function GetDateQuery(ByVal strFromDate As String, ByVal strToDate As String) As String
            Try
                If strFromDate <> "" And strToDate <> "" Then
                    If rbSlidingDate.Checked Then Session("TimeExpression") = " and ( GJH.datEntry_Date between '{FROM}' and '{TO}' ) "
                    Return " and ( GJH.datEntry_Date between  '" & strFromDate & "' and '" & CDate(strToDate & " 23:59:59") & "')"
                End If
                Return ""
            Catch ex As Exception
                Throw
            End Try
        End Function
        
        Private Sub DisplayError(ByVal exception As String)
            Try
                DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = exception
                DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
                DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Focus()
            Catch ex As Exception

            End Try
        End Sub
    </script>
    <style type="text/css">
        #tblMain .form-inline {
            min-height:50px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="RecordActionPanel" runat="server"
    ClientIDMode="Static">
    <div class="row padbottom10">
        <div class="col-xs-12">
            <div class="pull-right">
                <asp:LinkButton runat="server" ID="btnSearch" CssClass="btn btn-primary" OnClientClick="SetUserSearchCriteria();"><i class="fa fa-search"></i>&nbsp;&nbsp;Search</asp:LinkButton>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="DetailPageTitle" runat="server"
    ClientIDMode="Static">
    Advanced Financial Transaction Search
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="TabsPlaceHolder" runat="server"
    ClientIDMode="Static">
    <asp:Literal ID="litMessage" runat="server" EnableViewState="False"></asp:Literal>
    <asp:UpdateProgress ID="UpdateProgress" runat="server" ClientIDMode="Static">
        <ProgressTemplate>
            <div class="overlay">
                <div class="overlayContent" style="color: #000; text-align: center; width: 250px; padding: 20px">
                    <i class="fa fa-2x fa-refresh fa-spin"></i>
                    <h3>Processing Request</h3>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:ValidationSummary runat="server" ID="valsummery" CssClass="normal4" />
    <div runat="server" id="tblMain">
        <div class="box box-primary box-solid">
            <div class="box-header with-border">
                <h3 class="box-title">General</h3>

                <div class="box-tools pull-right">
                </div>
                <!-- /.box-tools -->
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="form-group">
                    <label>Transaction Date</label>
                    <div class="form-inline">
                        <asp:RadioButton Text="" runat="server" ID="rbFixedDate" GroupName="dateGroup" Checked="true" />
                        From
                        <BizCalendar:Calendar ID="calFrom" runat="server" ClientIDMode="AutoID" />
                        To
                        <BizCalendar:Calendar ID="calTo" runat="server" ClientIDMode="AutoID" />
                    </div>
                    <div class="form-inline">
                        <asp:RadioButton Text="" runat="server" ID="rbSlidingDate" GroupName="dateGroup" />
                        <asp:DropDownList runat="server" ID="ddlDateSpan" CssClass="form-control">
                            <asp:ListItem Text="Today" Value="1" />
                            <asp:ListItem Text="Week (Last 7 days)" Value="7" />
                            <asp:ListItem Text="Month (Last 30 days)" Value="30" />
                            <asp:ListItem Text="Quarter (Last 90 days)" Value="90" />
                            <asp:ListItem Text="Last 6 months" Value="180" />
                            <asp:ListItem Text="This Year" Value="365" />
                        </asp:DropDownList>
                    </div>
                </div>
            </div>
        </div>
        <asp:PlaceHolder ID="plhControls" runat="server"></asp:PlaceHolder>
    </div>
    <uc1:frmAdvSearchCriteria ID="frmAdvSearchCriteria1" runat="server" />
    <asp:HiddenField ID="hdnFilterCriteria" runat="server" />
    <asp:HiddenField ID="hdnSaveSearchCriteria" runat="server" />
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="UtilityLinksPanel" runat="server">
    
</asp:Content>
