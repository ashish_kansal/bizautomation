﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmBizDocs.aspx.vb" Inherits=".frmBizDocs"
    MasterPageFile="~/common/Popup.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>BizDoc</title>
    <script language="javascript" type="text/javascript">

        sortitems = 1;  // 0-False , 1-True
        function move(fbox, tbox) {

            for (var i = 0; i < fbox.options.length; i++) {
                if (fbox.options[i].selected && fbox.options[i].value != "") {
                    /// to check for duplicates 
                    for (var j = 0; j < tbox.options.length; j++) {
                        if (tbox.options[j].value == fbox.options[i].value) {
                            alert("Item is already selected");
                            return false;
                        }
                    }

                    var no = new Option();
                    no.value = fbox.options[i].value;
                    no.text = fbox.options[i].text;
                    tbox.options[tbox.options.length] = no;
                    //                    fbox.options[i].value = "";
                    //                    fbox.options[i].text = "";

                }
            }
            BumpUp(fbox);
            if (sortitems) SortD(tbox);
            return false;
        }

        function remove1(fbox, tbox) {

            for (var i = 0; i < fbox.options.length; i++) {
                if (fbox.options[i].selected && fbox.options[i].value != "") {
                    /// to check for duplicates 
                    for (var j = 0; j < tbox.options.length; j++) {
                        if (tbox.options[j].value == fbox.options[i].value) {
                            fbox.options[i].value = "";
                            fbox.options[i].text = "";
                            BumpUp(fbox);
                            if (sortitems) SortD(tbox);
                            return false;


                            //alert("Item is already selected");
                            //return false;
                        }
                    }

                    //                    var no = new Option();
                    //                    no.value = fbox.options[i].value;
                    //                    no.text = fbox.options[i].text;
                    //                    tbox.options[tbox.options.length] = no;
                    fbox.options[i].value = "";
                    fbox.options[i].text = "";

                }
            }
            BumpUp(fbox);
            if (sortitems) SortD(tbox);
            return false;
        }

        function BumpUp(box) {
            for (var i = 0; i < box.options.length; i++) {
                if (box.options[i].value == "") {
                    for (var j = i; j < box.options.length - 1; j++) {
                        box.options[j].value = box.options[j + 1].value;
                        box.options[j].text = box.options[j + 1].text;
                    }
                    var ln = i;
                    break;
                }
            }
            if (ln < box.options.length) {
                box.options.length -= 1;
                BumpUp(box);
            }
        }


        function SortD(box) {
            var temp_opts = new Array();
            var temp = new Object();
            for (var i = 0; i < box.options.length; i++) {
                temp_opts[i] = box.options[i];
            }
            for (var x = 0; x < temp_opts.length - 1; x++) {
                for (var y = (x + 1); y < temp_opts.length; y++) {
                    if (temp_opts[x].text > temp_opts[y].text) {
                        temp = temp_opts[x].text;
                        temp_opts[x].text = temp_opts[y].text;
                        temp_opts[y].text = temp;
                        temp = temp_opts[x].value;
                        temp_opts[x].value = temp_opts[y].value;
                        temp_opts[y].value = temp;
                    }
                }
            }
            for (var i = 0; i < box.options.length; i++) {
                box.options[i].value = temp_opts[i].value;
                box.options[i].text = temp_opts[i].text;
            }
        }

        function Save() {
            str = '';
            for (var i = 0; i < document.getElementById("lstSale").options.length; i++) {
                var SelectedText, SelectedValue;
                SelectedValue = document.getElementById("lstSale").options[i].value;
                SelectedText = document.getElementById("lstSale").options[i].text;
                str = str + SelectedValue + ','
            }
            document.getElementById("txtSaleHidden").value = str;

            str = '';
            for (var i = 0; i < document.getElementById("lstPurchase").options.length; i++) {
                var SelectedText, SelectedValue;
                SelectedValue = document.getElementById("lstPurchase").options[i].value;
                SelectedText = document.getElementById("lstPurchase").options[i].text;
                str = str + SelectedValue + ','
            }
            document.getElementById("txtPurchaseHidden").value = str;
        }
        function SaleSideSort(type) {
            window.open("../admin/frmBizSaleSide.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&type=" + type, '', 'toolbar=no,titlebar=no,left=200, top=200,width=600,height=400,scrollbars=yes,resizable=yes')
            return false;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <div class="input-part">
        <div class="right-input">
            <asp:Button ID="btnSortSalesSide" OnClientClick="SaleSideSort(1)" runat="server" CssClass="button" Text="Sort Sales Side"></asp:Button>
            <asp:Button ID="btnSortPurchaseSide" OnClientClick="SaleSideSort(2)" runat="server" CssClass="button" Text="Sort Purchase Side"></asp:Button>
            <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save"></asp:Button>
            <asp:Button ID="btnSaveClose" runat="server" CssClass="button" Text="Save & Close">
            </asp:Button>
            <asp:Button ID="btnClose" runat="server" CssClass="button" Text="Close" OnClientClick="javascript:window.close()">
            </asp:Button>&nbsp;&nbsp;
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageTitle" runat="server" ClientIDMode="Static">
    BizDoc List Filter
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="Content" runat="server" ClientIDMode="Static">
    <asp:Table ID="table1" CellPadding="0" CellSpacing="0" BorderWidth="1" runat="server"
        Width="100%" CssClass="aspTable" BorderColor="black" GridLines="None" Height="250">
        <asp:TableRow>
            <asp:TableCell VerticalAlign="Top">
                <table cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td align="left" class="normal1" width="25%">
                            <strong>Available BizDoc Type</strong><br />
                            <br />
                            <asp:ListBox ID="lstAvailable" runat="server" Width="250" Height="200" CssClass="signup"
                                ClientIDMode="Static"></asp:ListBox>
                        </td>
                        <td align="center" valign="middle" width="13%">
                            <asp:Button ID="btnAddSale" CssClass="button" runat="server" Text="Add to Sale >"
                                ClientIDMode="Static"></asp:Button>
                            <br />
                            <br />
                            <asp:Button ID="btnAddPrchase" CssClass="button" runat="server" Text="Add to Purchase >"
                                ClientIDMode="Static"></asp:Button>
                            <br />
                            <br />
                            <br />
                            <br />
                            <asp:Button ID="btnRemoveSale" CssClass="button" runat="server" Text="< Remove" ClientIDMode="Static">
                            </asp:Button>
                        </td>
                        <td align="left" class="normal1" width="25%">
                            <strong>Display on the Sale Side</strong><br />
                            <br />
                            <asp:ListBox ID="lstSale" runat="server" Width="250" Height="200" CssClass="signup"
                                ClientIDMode="Static"></asp:ListBox>
                            <asp:TextBox Style="display: none" ID="txtSaleHidden" runat="server" ClientIDMode="Static"></asp:TextBox>
                        </td>
                        <td align="center" valign="middle" width="12%">
                            <asp:Button ID="btnRemovePurchase" CssClass="button" runat="server" Text="< Remove"
                                ClientIDMode="Static"></asp:Button>
                        </td>
                        <td align="left" class="normal1" width="25%">
                            <strong>Display on the Purchase Side</strong><br />
                            <br />
                            <asp:ListBox ID="lstPurchase" runat="server" Width="250" Height="200" CssClass="signup"
                                ClientIDMode="Static"></asp:ListBox>
                            <asp:TextBox Style="display: none" ID="txtPurchaseHidden" runat="server" ClientIDMode="Static"></asp:TextBox>
                        </td>
                    </tr>
                </table>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
    <asp:Literal ID="litClientScript" runat="server" EnableViewState="False"></asp:Literal>
</asp:Content>
