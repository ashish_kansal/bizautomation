﻿Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Admin
Imports Telerik.Web.UI

Namespace BACRM.UserInterface.Admin
    Public Class frmClassCustomization
        Inherits BACRMPage

        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            Try
                If Not IsPostBack Then
                    BindTree()
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub
        Private Sub BindTree()
            Try
                Dim objAdmin As New CAdmin
                objAdmin.DomainID = Session("DomainID")
                objAdmin.Mode = 0
                Dim dt As DataTable = objAdmin.GetClass()
                RadTreeView1.DataTextField = "ClassName"
                RadTreeView1.DataValueField = "numChildClassID"
                RadTreeView1.DataFieldID = "numChildClassID"
                RadTreeView1.DataFieldParentID = "numParentClassID"
                RadTreeView1.DataSource = dt
                RadTreeView1.DataBind()
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        'Protected Sub RadTreeView1_HandleDrop(ByVal sender As Object, ByVal e As RadTreeNodeDragDropEventArgs)
        '    Dim sourceNode As RadTreeNode = e.SourceDragNode
        '    Dim destNode As RadTreeNode = e.DestDragNode
        '    Dim dropPosition As RadTreeViewDropPosition = e.DropPosition

        '    If destNode IsNot Nothing Then
        '        'drag&drop is performed between trees

        '        'dropped node will at the same level as a destination node
        '        If sourceNode.TreeView.SelectedNodes.Count <= 1 Then
        '            PerformDragAndDrop(dropPosition, sourceNode, destNode)
        '        ElseIf sourceNode.TreeView.SelectedNodes.Count > 1 Then
        '            For Each node As RadTreeNode In sourceNode.TreeView.SelectedNodes
        '                PerformDragAndDrop(dropPosition, node, destNode)
        '            Next
        '        End If
        '        'Else
        '        '    'dropped node will be a sibling of the destination node
        '        '    If sourceNode.TreeView.SelectedNodes.Count <= 1 Then
        '        '        If Not sourceNode.IsAncestorOf(destNode) Then
        '        '            sourceNode.Owner.Nodes.Remove(sourceNode)
        '        '            destNode.Nodes.Add(sourceNode)
        '        '        End If
        '        '    ElseIf sourceNode.TreeView.SelectedNodes.Count > 1 Then
        '        '        For Each node As RadTreeNode In RadTreeView1.SelectedNodes
        '        '            If Not node.IsAncestorOf(destNode) Then
        '        '                node.Owner.Nodes.Remove(node)
        '        '                destNode.Nodes.Add(node)
        '        '            End If
        '        '        Next
        '        '    End If

        '        destNode.Expanded = True
        '        sourceNode.TreeView.ClearSelectedNodes()
        '    End If
        'End Sub
        'Private Shared Sub PerformDragAndDrop(ByVal dropPosition As RadTreeViewDropPosition, ByVal sourceNode As RadTreeNode, ByVal destNode As RadTreeNode)
        '    If sourceNode.Equals(destNode) OrElse sourceNode.IsAncestorOf(destNode) Then
        '        Return
        '    End If
        '    sourceNode.Owner.Nodes.Remove(sourceNode)

        '    Select Case dropPosition
        '        Case RadTreeViewDropPosition.Over
        '            ' child
        '            If Not sourceNode.IsAncestorOf(destNode) Then
        '                destNode.Nodes.Add(sourceNode)
        '            End If
        '            Exit Select

        '        Case RadTreeViewDropPosition.Above
        '            ' sibling - above                    
        '            destNode.InsertBefore(sourceNode)
        '            Exit Select

        '        Case RadTreeViewDropPosition.Below
        '            ' sibling - below
        '            destNode.InsertAfter(sourceNode)
        '            Exit Select
        '    End Select
        'End Sub

        Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
            Try
                Dim objAdmin As New CAdmin
                objAdmin.DomainID = Session("DomainID")
                objAdmin.strItems = GetItems()
                If objAdmin.ManageClass() Then

                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub
        Function GetItems() As String
            Try
                Dim ds As New DataSet
                Dim dt As New DataTable
                dt.Columns.Add("numParentClassID")
                dt.Columns.Add("numChildClassID")
                For Each node As RadTreeNode In RadTreeView1.GetAllNodes()
                    Dim dr As DataRow = dt.NewRow
                    dr("numChildClassID") = node.Value
                    If Not node.ParentNode Is Nothing Then
                        dr("numParentClassID") = node.ParentNode.Value
                    Else
                        dr("numParentClassID") = 0
                    End If
                    If dr("numChildClassID") > 0 Then
                        dt.Rows.Add(dr)
                    End If
                Next
                ds.Tables.Add(dt.Copy)
                ds.Tables(0).TableName = "Item"
                Return ds.GetXml()
            Catch ex As Exception
                Throw ex
            End Try
        End Function
    End Class
End Namespace
