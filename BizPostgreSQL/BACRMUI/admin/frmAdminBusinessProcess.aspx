<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmAdminBusinessProcess.aspx.vb"
    Inherits="BACRM.UserInterface.Admin.frmAdminBusinessProcess" MasterPageFile="~/common/DetailPage.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="AutoID">
    <title>Business Process</title>
    <meta http-equiv="Page-Enter" content="blendTrans(Duration=0.01)">
    <meta http-equiv="Page-Exit" content="blendTrans(Duration=0.01)">
    <link rel="stylesheet" href="../CSS/lists.css" type="text/css" />
    <script language="JavaScript" type="text/javascript" src="../javascript/coordinates.js"></script>
    <script language="JavaScript" type="text/javascript" src="../javascript/drag.js"></script>
    <script language="JavaScript" type="text/javascript" src="../javascript/dragdrop.js"></script>
    <style>
        .modal-header .close {
            margin-top: 6px;
            color: #fff;
            opacity: 10;
        }

        .modal-header {
            padding: 8px;
            background-color: #0d8ed2;
            color: #fff;
        }

        #taskModal .col-md-6, #taskModal .col-md-12, #taskModal .col-md-3, #taskModal .col-md-2, #taskModal .col-md-1 {
            padding: 5px;
        }

        .rdnProcess {
            padding-left: 5px;
            padding-right: 5px;
        }

        .tab-content {
            border: 1px solid #1473b4;
            border-top: 0px;
        }

        .nav-tabs {
            border-top: 4px solid #1473b4;
            background: #1473b4;
            border-radius: 4px;
            border-left: 4px solid #1473b4;
        }

            .nav-tabs > li > a {
                color: #fff !important;
            }

            .nav-tabs > li.active > a {
                color: #1473b4 !important;
            }

            .nav-tabs > li > a:hover {
                background-color: #3198de;
                color: #71b3e9;
                border: none !important;
            }



        .boxForList .box-primary .box-header {
            background-color: #1473b4;
        }

        .boxForList .box-header .form-inline label {
            color: #fff !important;
            margin-right: 10px;
        }

        .boxForList .box-header .form-inline .form-control {
            padding: 9px;
            height: 27px;
            margin-right: 10px;
        }

        .boxForList .box-header .form-inline .btn-xs {
            margin-right: 10px;
        }

        .indivStages .form-inline .form-control, .indivStages .form-inline .btn-xs, .indivStages .form-inline label {
            margin-right: 1px;
        }

        .boxForList .box-title a {
            color: #ffffff !important;
            font-size: 17px;
            display: block;
            padding-top: 4px;
            padding-left: 10px;
        }

        .boxForList .box-body {
            padding: 0px !important;
            min-height: 96px;
            border: 1px solid #c8c2c2;
        }

        .text-delete-danger {
            color: #dd4b39;
        }

        .boxForList .box-title {
            display: block !important;
        }

        .boxForList .box-header {
            padding: 3px !important;
        }

        .boxForList .internalBoxHeader {
            background-color: #e2e2e2;
            padding-top: 6px;
        }

        .boxForList .box .box-group > .box {
            margin-bottom: 2px !important;
        }

        .boxForList .indivStages {
            padding: 6px;
            background-color: rgba(204, 204, 204, 0.25);
            margin: 4px;
            border: 1px solid #e1e1e1;
        }
    </style>
    <script language="javascript" type="text/ecmascript">

        function AddStage(a) {

            if (document.getElementById("ddlProcessList").value == 0) {
                alert('Select the Process')
                document.getElementById("ddlProcessList").focus()
                return false
            }
            else {
                document.getElementById("txtAddStage").value = a
            }
        }

        function ClientSideClick() {
            var isGrpOneValid = Page_ClientValidate("vgRequired");
            var check = Check();

            var i;
            for (i = 0; i < Page_Validators.length; i++) {
                ValidatorValidate(Page_Validators[i]); //this forces validation in all groups
            }

            //display all summaries.
            for (i = 0; i < Page_ValidationSummaries.length; i++) {
                summary = Page_ValidationSummaries[i];
                //does this summary need to be displayed?
                if (fnJSDisplaySummary(summary.validationGroup)) {
                    summary.style.display = "none"; //"none"; "inline";
                    alert(" -Milestone Name Required \n -Stage Name Required \n -Stage % of completion Required \n -Stage % of completion should be betwwen 0 to 100 ");
                }
            }

            if (isGrpOneValid && check) {
                return true;
            }
            else
                return false;
        }


        /* determines if a Validation Summary for a given group needs to display */
        function fnJSDisplaySummary(valGrp) {
            var rtnVal = false;
            for (i = 0; i < Page_Validators.length; i++) {
                if (Page_Validators[i].validationGroup == valGrp) {
                    if (!Page_Validators[i].isvalid) { //at least one is not valid.
                        rtnVal = true;
                        break; //exit for-loop, we are done.
                    }
                }
            }
            return rtnVal;
        }

        function Check() {
            if (document.getElementById("ddlProcessList").value == 0) {
                alert('Select The Process');
                document.getElementById("ddlProcessList").focus();
                return false;
            }
            return true;
        }
        function DelStage(a, b) {
            if (document.getElementById("ddlProcessList").value == 0) {
                alert('Select The Process')
                document.getElementById("ddlProcessList").focus()
                return false
            }
            else {
                var splid = document.getElementById("ddlProcessList").value
                var noOfStages = 0
                var StageDetailsId = ''
                noOfStages = document.getElementById('txtMSMaxStage' + a).value
                if (noOfStages != 0) {
                    var i = 1;
                    for (i = 1; i <= noOfStages; i++) {
                        if (document.getElementById('ctl00_ctl00_MainContent_TabsPlaceHolder_Chk_' + b + '_' + i + '_' + splid).checked == true) {
                            if (StageDetailsId == '') {
                                StageDetailsId = document.getElementById('ctl00_ctl00_MainContent_TabsPlaceHolder_lbl_' + b + '_' + i + '_' + splid).innerHTML
                            }
                            else {
                                StageDetailsId = StageDetailsId + ',' + document.getElementById('ctl00_ctl00_MainContent_TabsPlaceHolder_lbl_' + b + '_' + i + '_' + splid).innerHTML
                            }
                        }
                    }
                    if (StageDetailsId == '') {
                        return false
                    }
                    else {
                        document.getElementById("txtDeleteStage").value = StageDetailsId
                    }

                }
                else {
                    return false
                }
                //  alert(document.getElementById("txtDeleteStage").value);
            }

        }
        function OpenNew() {
            window.open('../Admin/AdminBusinessProcess.aspx', '', 'toolbar=no,titlebar=no,top=200,left=400,width=450,height=150,scrollbars=yes,resizable=yes')
            return false
        }
        function OpenSubStage(a) {
            window.open('../Admin/frmSubStageList.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&ProcessID=' + a, '', 'toolbar=no,titlebar=no,left=400, top=100,width=500,height=400,scrollbars=yes,resizable=yes');
        }
        function OpenTransfer(url) {
            window.open(url, '', "toolbar=no,titlebar=no,top=200,left=400,width=450,height=200,scrollbars=yes,resizable=yes");
            return false;
        }


        //Prasanta Business Process Functions
        $(document).ready(function () {
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_pageLoaded(pageLoaded);
        });

        function pageLoaded(){
            if($("[id$=hdnProcessType]").val() == 3){
                $('input[name="rdnProcessType"]').removeAttr('checked');
                $('input[name="rdnProcessType"][value="3"]').prop('checked', true);
            }
            BindProcessList();
            $("#ddlProcesses").change(function () {
                var DomainID = '<%= Session("DomainId")%>';
                var dataParam = "{DomainID:'" + DomainID + "',ProcessId:'" + $("#ddlProcesses option:selected").val() + "',Mode:'" + $("input[name=rdnProcessType]:checked").val() + "'}";
                $.ajax({
                    type: "POST",
                    url: "../admin/frmAdminBusinessProcess.aspx/WebMethodGetConfigurationOnProcessList",
                    data: dataParam,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {
                        debugger
                        $("#UpdateProgress").css("display", "none");
                        var Jresponse = $.parseJSON(response.d);
                        if (Jresponse.length > 0) {
                            var ProcessDetails = Jresponse[0];
                            SetDefaultSetupForConfig(ProcessDetails.tintConfiguration);
                            $("#txtName").val(ProcessDetails.Slp_Name);
                            $("#ddlStag").val(ProcessDetails.tintConfiguration);
                            $("#hdnConfigurationId").val(ProcessDetails.tintConfiguration);
                            $("#chkTimerWindowOpens").prop("checked", ProcessDetails.bitAutomaticStartTimer);
                            $("#chkAssignStagetoTeams").prop("checked", ProcessDetails.bitAssigntoTeams);
                            $("#ddlTaskValidation").val(ProcessDetails.numTaskValidatorId);
                            $("[id$=ddlBuildManager]").val(ProcessDetails.numBuildManager);
                        }
                    },
                    failure: function (response) {
                        $("#UpdateProgress").css("display", "none");
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        $("#UpdateProgress").css("display", "none");
                    }, complete: function () {
                        $("#UpdateProgress").css("display", "none");
                    }
                });
            })

            $('#txtTaskHours').on('keydown keyup change', function(){
                var hours = parseInt($(this).val());
                var charLength = $(this).val().length;
                if(hours > 24){
                    $(this).val(24);
                }
            });

            $('#txtTaskMinutes').on('keydown keyup change', function(){
                var hours = parseInt($(this).val());
                var charLength = $(this).val().length;
                if(hours > 59){
                    $(this).val(59);
                }
            });
        }

        function BindProcessList(selectedValue) {
            $("#DropDownListaccordion").html("");
            $("[id$=hdnProcessType]").val($("input[name=rdnProcessType]:checked").val());
            $("[id$=ddlBuildManager]").val("0");
            if($("[id$=hdnProcessType]").val() === "3"){
                $("#divBuildManager").show();
            } else {
                $("#divBuildManager").hide();
            }

            var DomainID = '<%= Session("DomainId")%>';
            var dataParam = "{DomainID:'" + DomainID + "',Mode:'" + $("input[name=rdnProcessType]:checked").val() + "'}";
            $("#ddlProcesses").empty();
            $("#UpdateProgress").css("display", "block");
            $.ajax({
                type: "POST",
                url: "../admin/frmAdminBusinessProcess.aspx/WebMethodGetProcessList",
                data: dataParam,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {

                    $("#UpdateProgress").css("display", "none");
                    var Jresponse = $.parseJSON(response.d);
                    var itemAppend = '';
                    itemAppend = "<option value=\"0\">--Select One--</option>";
                    $("#ddlProcesses").append(itemAppend);
                    itemAppend = '';
                    $.each(Jresponse, function (index, value) {
                        itemAppend = itemAppend + "<option value=" + value.Slp_Id + ">" + value.Slp_Name + "</option>";
                    });
                    $("#ddlProcesses").append(itemAppend);
                    if (selectedValue > 0) {
                        $("#ddlProcesses").val(selectedValue);
                    }
                },
                failure: function (response) {
                    $("#UpdateProgress").css("display", "none");
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    $("#UpdateProgress").css("display", "none");
                }, complete: function () {
                }
            });
        }

        function SaveNewProcess() {
            var UserContactId ='<%= Session("UserContactId")%>'
            var DomainID = '<%= Session("DomainId")%>';
            if ($("#txtName").val() == "") {
                alert("Please enter process name");
                $("#txtName").focus();
                return false;
            }
            var dataParam = "{DomainID:'" + DomainID + "',Mode:'" + $("input[name=rdnProcessType]:checked").val() + "',ConfigurationId:'" + $("#ddlStag option:selected").val() + "',UserContactId:'" + UserContactId + "',ProcessName:'" + $("#txtName").val() + "'}";
            $.ajax({
                type: "POST",
                url: "../admin/frmAdminBusinessProcess.aspx/WebMethodSaveNewProcess",
                data: dataParam,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {

                    $("#UpdateProgress").css("display", "none");
                    var Jresponse = $.parseJSON(response.d);
                    if (parseFloat(Jresponse) == 0) {
                        alert("Process already exist!");
                        return false;
                    } else {
                        alert("Process added successfully!");
                        $("#txtName").val("");
                        $("#ddlStag").val(1);
                        BindProcessList();
                        return false;
                    }
                },
                failure: function (response) {
                    $("#UpdateProgress").css("display", "none");
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    $("#UpdateProgress").css("display", "none");
                }, complete: function () {
                    $("#UpdateProgress").css("display", "none");
                }
            });
        }
        function updateStageDetails() {
            var jsonArr = [];
            var UserContactId ='<%= Session("UserContactId")%>'
            var DomainID = '<%= Session("DomainId")%>';
            $.each(stageDetailsList, function (index, value) {
                var dueDays = $("#txtDaysForDueDate_" + value + "").val();
                if ($("#chkAssignStagetoTeams").prop("checked") == false) {
                    $("#ddlTeam_" + value + "").val("0");
                }
                if (dueDays == "") {
                    dueDays = 0;
                }
                var jsonObj = {
                    numStageDetailsId: value,
                    StagePerID: $("#txtStageName_" + value + "").attr("StagePerID"),
                    StageDetail: $("#txtStageName_" + value + "").val(),
                    numTeamId: $("#ddlTeam_" + value + " option:selected").val(),
                    Percentage: $("#txtProgressPercentage_" + value + "").val(),
                    MileStoneName: $("#mileStone_txtName_" + $("#txtStageName_" + value + "").attr("StagePerID") + "").val(),
                    DueDays: dueDays,
                    bitIsDueDaysUsed: $("#chkDue_" + value + "").prop("checked"),
                    bitRunningDynamicMode: $("#chkRunningDynamicMode_" + value + "").prop("checked"),
                    numStageOrder: $("#txtSequence_" + value + "").val()
                };
                jsonArr.push(jsonObj);
            });
            if (jsonArr.length > 0) {
                //var dataParam = "{DomainID:'" + DomainID + "',UserContactId:'" + UserContactId + "',arrayList:" +JSON.stringify(jsonArr) + "}";
                $.ajax({
                    type: "POST",
                    url: "../admin/frmAdminBusinessProcess.aspx/WebMethodUpdateStageDetails",
                    data: JSON.stringify({
                        DomainID: DomainID,
                        UserContactId: UserContactId,
                        ConfigurationId: $("#hdnConfigurationId").val(),
                        ProcessId: $("#ddlProcesses option:selected").val(),
                        arrayList: jsonArr
                    }),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {
                        //var Jresponse = $.parseJSON(response.d);
                        //if (Jresponse == true) {
                        //    SetDefaultSetupForConfig($("#hdnConfigurationId").val());
                        //}
                    },
                    failure: function (response) {
                        $("#UpdateProgress").css("display", "none");
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        $("#UpdateProgress").css("display", "none");
                    }, complete: function () {
                        $("#UpdateProgress").css("display", "none");
                    }
                });
            }
        }
        function UpdateProcess() {
            var UserContactId ='<%= Session("UserContactId")%>'
            var DomainID = '<%= Session("DomainId")%>';
            $("#UpdateProgress").css("display", "block");
            var dataParam = "{DomainID:'" + DomainID + "',UserContactId:'" + UserContactId + "',ConfigurationId:'" + $("#hdnConfigurationId").val() + "',AssignToTeams:'" + $("#chkAssignStagetoTeams").is(":checked") + "',AutomaticStartTimer:'" + $("#chkTimerWindowOpens").is(":checked") + "',ProcessId:'" + $("#ddlProcesses option:selected").val() + "',SalesProsName:'" + $("#txtName").val() + "',TaskValidatorId:'"+$("#ddlTaskValidation option:selected").val()+"',BuildManager:'" + $("[id$=ddlBuildManager]").val() + "'}";
            $.ajax({
                type: "POST",
                url: "../admin/frmAdminBusinessProcess.aspx/WebMethodUpdateProcess",
                data: dataParam,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    var Jresponse = $.parseJSON(response.d);
                    if (Jresponse == true) {
                        $("#UpdateProgress").css("display", "none");
                        debugger
                        if ($("#chkAssignStagetoTeams").prop("checked") == true) {
                            $(".ddlTeam").css("display", "inline-block");
                        }
                        var IsValid = true;
                        $.each(stageDetailsList, function (index, value) {
                            IsValid = true;
                            var openToDiv = "milestone_" + $("#txtStageName_" + value + "").attr("Configuration") + "_" + $("#txtStageName_" + value + "").attr("StagePerID") + "_" + $("#txtStageName_" + value + "").attr("Percentage");
                            if ($("#txtStageName_" + value + "").val() == "") {
                                alert("Please enter stage details");
                                $("#txtStageName_" + value + "").focus();
                                $(".panel-collapse").collapse('hide');
                                setTimeout(function () { $('#' + openToDiv + '').collapse('show'); }, 1000);

                                IsValid = false;
                            } else if ($("#chkAssignStagetoTeams").prop("checked") == true && $("#ddlTeam_" + value + " option:selected").val() == 0) {
                                alert("Please select teams");
                                $("#ddlTeam_" + value + "").focus();
                                $(".panel-collapse").collapse('hide');
                                setTimeout(function () { $('#' + openToDiv + '').collapse('show'); }, 1000);
                                IsValid = false;
                            } else if ($("#txtProgressPercentage_" + value + "").val() == "") {
                                alert("Please enter stage comprises percentage");
                                $("#txtProgressPercentage_" + value + "").focus();
                                $(".panel-collapse").collapse('hide');
                                setTimeout(function () { $('#' + openToDiv + '').collapse('show'); }, 1000);
                                IsValid = false;
                            } else if ($("#chkDue_" + value + "").prop("checked") == true && $("#txtDaysForDueDate_" + value + "").val() == "") {
                                alert("Please enter due date");
                                $("#txtDaysForDueDate_" + value + "").focus();
                                $(".panel-collapse").collapse('hide');
                                setTimeout(function () { $('#' + openToDiv + '').collapse('show'); }, 1000);
                                IsValid = false;
                            }
                            return IsValid;
                        });
                        debugger
                        if (IsValid == true) {
                            $.each(MileStoneArray, function (index, value) {
                                IsValid = true;
                                var totalPercentage = 0;
                                var isStagePercentageNotAdded = false;
                                $('#milestone_' + value + ' .chkStageDelete').each(function (index, obj) {
                                    var StagePerDetailsId = $(this).attr("values");
                                    var IndivStagePercentage = 0;
                                    IndivStagePercentage = $("#txtProgressPercentage_" + StagePerDetailsId + "").val();
                                    totalPercentage = parseFloat(totalPercentage) + parseFloat(IndivStagePercentage);

                                    if(parseFloat(IndivStagePercentage) == 0){
                                        isStagePercentageNotAdded = true;
                                    }
                                });

                                if(isStagePercentageNotAdded){
                                    alert("Stage(s) percentage must be greater than 0");
                                    IsValid = false;
                                }
                                if (totalPercentage != 100 && totalPercentage != 0) {
                                    alert("All stages comprise of milestone total should be 100%");
                                    IsValid = false;
                                }
                                if (IsValid == false) {
                                    $(".panel-collapse").collapse('hide');
                                    setTimeout(function () { $('#milestone_' + value + '').collapse('show'); }, 1000);
                                }
                                return IsValid;
                            });
                        }

                        var jsonArr = [];
                        if (IsValid == true) {
                            $.each(stageDetailsList, function (index, value) {
                                var dueDays = $("#txtDaysForDueDate_" + value + "").val();
                                if ($("#chkAssignStagetoTeams").prop("checked") == false) {
                                    $("#ddlTeam_" + value + "").val("0");
                                }
                                if (dueDays == "") {
                                    dueDays = 0;
                                }
                                var jsonObj = {
                                    numStageDetailsId: value,
                                    StagePerID: $("#txtStageName_" + value + "").attr("StagePerID"),
                                    StageDetail: $("#txtStageName_" + value + "").val(),
                                    numTeamId: $("#ddlTeam_" + value + " option:selected").val(),
                                    Percentage: $("#txtProgressPercentage_" + value + "").val(),
                                    MileStoneName: $("#mileStone_txtName_" + $("#txtStageName_" + value + "").attr("StagePerID") + "").val(),
                                    DueDays: dueDays,
                                    bitIsDueDaysUsed: $("#chkDue_" + value + "").prop("checked"),
                                    bitRunningDynamicMode: $("#chkRunningDynamicMode_" + value + "").prop("checked"),
                                    numStageOrder: $("#txtSequence_" + value + "").val()
                                };
                                jsonArr.push(jsonObj);
                            });
                            if (jsonArr.length > 0) {
                                //var dataParam = "{DomainID:'" + DomainID + "',UserContactId:'" + UserContactId + "',arrayList:" +JSON.stringify(jsonArr) + "}";
                                $.ajax({
                                    type: "POST",
                                    url: "../admin/frmAdminBusinessProcess.aspx/WebMethodUpdateStageDetails",
                                    data: JSON.stringify({
                                        DomainID: DomainID,
                                        UserContactId: UserContactId,
                                        ConfigurationId: $("#hdnConfigurationId").val(),
                                        ProcessId: $("#ddlProcesses option:selected").val(),
                                        arrayList: jsonArr
                                    }),
                                    contentType: "application/json; charset=utf-8",
                                    dataType: "json",
                                    success: function (response) {
                                        var Jresponse = $.parseJSON(response.d);
                                        if (Jresponse == true) {
                                            SetDefaultSetupForConfig($("#hdnConfigurationId").val());
                                            alert("Changes saved successfully");
                                        }
                                    },
                                    failure: function (response) {
                                        $("#UpdateProgress").css("display", "none");
                                    },
                                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                                        $("#UpdateProgress").css("display", "none");
                                    }, complete: function () {
                                        $("#UpdateProgress").css("display", "none");
                                    }
                                });
                            }
                        }
                        var ddlValue = $("#ddlProcesses option:selected").val();
                        $('#ddlProcesses option[value=' + ddlValue + ']').text($("#txtName").val());
                    }
                },
                failure: function (response) {
                    $("#UpdateProgress").css("display", "none");
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    $("#UpdateProgress").css("display", "none");
                }, complete: function () {
                    $("#UpdateProgress").css("display", "none");
                }
            });
        }
        function deleteProcess() {
            var UserContactId ='<%= Session("UserContactId")%>'
            var DomainID = '<%= Session("DomainId")%>';
            if ($("#ddlProcesses option:selected").val() == "0") {
                alert("Please select process");
                $("#ddlProcesses").focus();
                return false;
            }
            $("#UpdateProgress").css("display", "block");
            var dataParam = "{DomainID:'" + DomainID + "',ProcessId:'" + $("#ddlProcesses option:selected").val() + "'}";
            $.ajax({
                type: "POST",
                url: "../admin/frmAdminBusinessProcess.aspx/WebMethodDeleteProcess",
                data: dataParam,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {

                    $("#UpdateProgress").css("display", "none");
                    var Jresponse = $.parseJSON(response.d);
                    if (parseFloat(Jresponse) == 1) {
                        alert("Process deleted successfully!");
                        $("#txtName").val("");
                        $("#ddlStag").val(1);
                        BindProcessList();
                        return false;
                    } else {
                        alert("You can't delete this Business Process because its being used by Project/Opportunity.");
                        return false;
                    }
                },
                failure: function (response) {
                    $("#UpdateProgress").css("display", "none");
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    $("#UpdateProgress").css("display", "none");
                }, complete: function () {
                    $("#UpdateProgress").css("display", "none");
                }
            });
        }

        var MileStoneArray = [];
        function SetDefaultSetupForConfig(configId) {
            MileStoneArray = [];
            if (configId == 1) {
                MileStoneArray.push("" + configId + "_12_100");
            } else if (configId == 2) {
                MileStoneArray.push("" + configId + "_6_50");
                MileStoneArray.push("" + configId + "_12_100");
            } else if (configId == 3) {
                MileStoneArray.push("" + configId + "_15_33");
                MileStoneArray.push("" + configId + "_16_66");
                MileStoneArray.push("" + configId + "_12_100");
            } else if (configId == 4) {
                MileStoneArray.push("" + configId + "_3_25");
                MileStoneArray.push("" + configId + "_6_50");
                MileStoneArray.push("" + configId + "_9_75");
                MileStoneArray.push("" + configId + "_12_100");
            } else if (configId == 5) {
                MileStoneArray.push("" + configId + "_2_20");
                MileStoneArray.push("" + configId + "_5_40");
                MileStoneArray.push("" + configId + "_7_60");
                MileStoneArray.push("" + configId + "_10_80");
                MileStoneArray.push("" + configId + "_12_100");
            }
            var itemAppend = '';
            $("#DropDownListaccordion").html("");
            var idToShow = MileStoneArray[0];
            $.each(MileStoneArray, function (index, value) {

                var sData = value.split("_");
                console.log(sData[1]);
                itemAppend = '';
                itemAppend = itemAppend + "<div class=\"box box-primary\">";
                itemAppend = itemAppend + "<div class=\"box-header\">";
                itemAppend = itemAppend + "<div class=\"pull-left col-md-7\">";
                itemAppend = itemAppend + "<h4 class=\"box-title\">";
                itemAppend = itemAppend + "<a data-toggle=\"collapse\" class=\"collapseMenu\" data-parent=\"#DropDownListaccordion\" href=\"#milestone_" + value + "\" aria-expanded=\"false\" class=\"collapsed\">Milestone " + sData[2] + "%</a>";
                itemAppend = itemAppend + "</h4>";
                itemAppend = itemAppend + "</div>";
                itemAppend = itemAppend + "<div class=\"pull-right\">";
                itemAppend = itemAppend + "<div class=\"form-inline\">";
                itemAppend = itemAppend + "<label>Milestone Name</label>";
                itemAppend = itemAppend + "<input type=\"text\" class=\"form-control\" id=\"mileStone_txtName_" + sData[1] + "\" />";
                itemAppend = itemAppend + "<button class=\"btn btn-xs btn-warning\" type=\"button\" onclick=\"addStage(" + sData[1] + ",'" + configId + "','milestone_" + value + "',0)\">Add Stage</button>";
                itemAppend = itemAppend + "<button class=\"btn btn-xs btn-danger\" type=\"button\" onclick=\"deleteStages()\">Delete Stage</button>";
                itemAppend = itemAppend + "</div>";
                itemAppend = itemAppend + "</div>";
                itemAppend = itemAppend + "</div>";
                itemAppend = itemAppend + "<div id=\"milestone_" + value + "\" class=\"panel-collapse collapse\" aria-expanded=\"false\" style=\"height: 0px;\">";
                itemAppend = itemAppend + "<div class=\"box-body\">";

                itemAppend = itemAppend + "</div>";
                itemAppend = itemAppend + "</div>";
                itemAppend = itemAppend + "</div>";
                $("#DropDownListaccordion").append(itemAppend);
            });

            getStageDetails();
            $('#milestone_' + idToShow + '').collapse('show');
        }
        var stageDetailsList = [];
        function getStageDetails() {

            var DomainID = '<%= Session("DomainId")%>';
            var dataParam = "{DomainID:'" + DomainID + "',ProcessId:'" + $("#ddlProcesses option:selected").val() + "',OppID:'0',ProjectID:'0'  }";
            $("#UpdateProgress").css("display", "block");
            stageDetailsList = [];
            $.ajax({
                type: "POST",
                url: "../admin/frmAdminBusinessProcess.aspx/WebMethodGetSavedStageDetails",
                data: dataParam,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    $("#UpdateProgress").css("display", "none");
                    var Jresponse = $.parseJSON(response.d);
                    var itemAppend = '';
                    $("#DropDownListaccordion .box-body").html('');
                    var stageSequence = 1;
                    $.each(Jresponse, function (index, value) {
                        itemAppend = '';
                        stageDetailsList.push(value.numStageDetailsId);
                        var configPercentage = 0;
                        if (value.numStagePercentageId == 12) {
                            configPercentage = 100;
                        } else if (value.numStagePercentageId == 6) {
                            configPercentage = 50;
                        } else if (value.numStagePercentageId == 15) {
                            configPercentage = 33;
                        } else if (value.numStagePercentageId == 16) {
                            configPercentage = 66;
                        } else if (value.numStagePercentageId == 3) {
                            configPercentage = 25;
                        } else if (value.numStagePercentageId == 2) {
                            configPercentage = 20;
                        } else if (value.numStagePercentageId == 9) {
                            configPercentage = 75;
                        } else if (value.numStagePercentageId == 5) {
                            configPercentage = 40;
                        } else if (value.numStagePercentageId == 7) {
                            configPercentage = 60;
                        } else if (value.numStagePercentageId == 10) {
                            configPercentage = 80;
                        }
                        var appendToDiv = "milestone_" + value.tintConfiguration + "_" + value.numStagePercentageId + "_" + configPercentage;
                        itemAppend = itemAppend + "<div class=\"indivStages\">";
                        itemAppend = itemAppend + "<div class=\"form-inline\">";
                        itemAppend = itemAppend + "<input type=\"checkbox\" values=\"" + value.numStageDetailsId + "\" class=\"chkStageDelete\"></input><label><span class=\"text-danger\">*</span>Stage</label>";
                        itemAppend = itemAppend + "<input type=\"text\" class=\"form-control\" id=\"txtStageName_" + value.numStageDetailsId + "\" Configuration=\"" + value.tintConfiguration + "\" StagePerID=\"" + value.numStagePercentageId + "\"  Percentage=\"" + configPercentage + "\" value=\"" + value.vcStageName + "\" placeholder=\"Enter details with up to 60 charecters\" style=\"width: 26%;\" />";
                        itemAppend = itemAppend + "<label><span class=\"text-danger\">*</span>Team</label>";
                        var disabled = '';
                        if ($("#chkAssignStagetoTeams").prop("checked") == true) {
                            disabled = 'display:inline-block';
                        } else {
                            disabled = 'display:none';
                        }
                        var dueChecked = '';
                        var runDynamiModeChecked = '';
                        if (value.bitIsDueDaysUsed == true) {
                            dueChecked = 'checked';
                        }
                        if (value.bitRunningDynamicMode == true) {
                            runDynamiModeChecked = 'checked';
                        }
                        var hours = value.numHours;
                        var minutes = value.numMinutes;
                        var days = 0;
                        var week = 0;
                        if (minutes > 59) {
                            var addedHours = Math.floor(minutes / 60);
                            hours = hours + addedHours;
                            minutes = minutes % 60;
                        }
                        var estimateTime = hours + " Hours & " + minutes + " Minutes";
                        //if (hours > 7) {
                        //    days = Math.floor(hours / 8);
                        //    minutes = minutes + ((hours % 8) * 60);
                        //    estimateTime = days + " Days & " + minutes + " Minutes;
                        //}
                        itemAppend = itemAppend + "<select class=\"form-control ddlTeam\"  id=\"ddlTeam_" + value.numStageDetailsId + "\" style=\"width: 14%;" + disabled + "\">";
                        itemAppend = itemAppend + "<option value=\"0\">select</option>";
                        itemAppend = itemAppend + "</select>";
                        itemAppend = itemAppend + "<label>Estimated time to complete tasks</label>";
                        itemAppend = itemAppend + "<span id=\"spanEstimateTime_" + value.numStageDetailsId + "\">" + estimateTime + "</span>";
                        itemAppend = itemAppend + "<label><span class=\"text-danger\">*</span>This stage comprises</label>";
                        itemAppend = itemAppend + "<input type=\"text\" id=\"txtProgressPercentage_" + value.numStageDetailsId + "\" value=\"" + value.tintPercentage + "\" attr=\"" + value.numStagePercentageId + "\" class=\"form-control\" style=\"width: 4%\" /><span>% of the milestone</span>";
                        itemAppend = itemAppend + "<input type=\"text\" class=\"form-control pull-right\" style='width:50px' id=\"txtSequence_" + value.numStageDetailsId + "\" value=\"" + value.numStageOrder + "\" />&nbsp;&nbsp;";
                        if ($("input[name=rdnProcessType]:checked").val() == "3") {
                            itemAppend = itemAppend + "<button  type=\"button\"  onclick=\"openGradingModal(" + value.numStageDetailsId + ")\"  style=\"margin-right: 9px;margin-top: 2px;\" class=\"btn btn-sm btn-primary pull-right\">A+</button>";
                        }
                        itemAppend = itemAppend + "</div>";
                        itemAppend = itemAppend + "<div class=\"pull-left\" style=\"width:65%\">";
                        itemAppend = itemAppend + "<div class=\"form-inline\">";
                        itemAppend = itemAppend + "<label><span class=\"text-danger\">*</span><a href=\"#\" onclick=\"openTaskModal(" + value.numStageDetailsId + ")\" style=\"text-decoration:underline\">Task:</a></label>";
                        itemAppend = itemAppend + "<span id=\"spanTaskDetails_" + value.numStageDetailsId + "\">" + value.TaskName + "</span>";
                        itemAppend = itemAppend + "</div>";
                        itemAppend = itemAppend + "</div>";
                        itemAppend = itemAppend + "<div class=\"pull-right\">";
                        itemAppend = itemAppend + "<div class=\"form-inline\">";
                        itemAppend = itemAppend + "<input type=\"checkbox\" " + dueChecked + " id=\"chkDue_" + value.numStageDetailsId + "\"/><label>Due</label>";
                        itemAppend = itemAppend + "<input type=\"text\" id=\"txtDaysForDueDate_" + value.numStageDetailsId + "\" value=\"" + value.intDueDays + "\" class=\"form-control\" style=\"width: 50px;padding: 5px;height: 28px;\" /><label>days after stage start date</label>";
                        itemAppend = itemAppend + "<a href=\"#\">[?]</a>";
                        itemAppend = itemAppend + "<input type=\"checkbox\" " + runDynamiModeChecked + " id=\"chkRunningDynamicMode_" + value.numStageDetailsId + "\"/><label>Run in dynamic mode</label>";
                        itemAppend = itemAppend + "</div>";
                        itemAppend = itemAppend + "</div>";
                        itemAppend = itemAppend + "<div class=\"clearfix\"></div>";
                        itemAppend = itemAppend + "</div>";

                        $("#" + appendToDiv + " .box-body").append(itemAppend);
                        $("#mileStone_txtName_" + value.numStagePercentageId + "").val(value.vcMilestoneName);
                        GetTeams(value.numStageDetailsId, value.numTeamId);
                    });
                },
                failure: function (response) {
                    $("#UpdateProgress").css("display", "none");
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    $("#UpdateProgress").css("display", "none");
                }, complete: function () {
                }
            });
        }
        function openGradingModal(StageDetailsId) {
            GetGradeTeams();
            openTaskAssignes();
            GetTaskByStageDetailsId(StageDetailsId);
            GetGradingDetails(StageDetailsId);
            $("#hdnGradeStageDetailsId").val(StageDetailsId);
            $("#taskGradingBasedOnStagingDetailsModal").modal("show");
        }
        function AddGradingDetails() {
            var UserContactId ='<%= Session("UserContactId")%>'
            var DomainID = '<%= Session("DomainId")%>';
            var assigneArray = [];
            assigneArray = $("#ddlTaskGradeAssignees").val();
            if (assigneArray==null) {
                alert("Please select assignee");
                $("#ddlTaskGradeAssignees").focus();
                return false;
            }
            if ($("#ddlTaskGradeTaskList option:Selected").val() == "0") {
                alert("Please select a task");
                return false;
            }
            if ($("#ddlTaskGradeList option:Selected").val() == "0") {
                alert("Please select a grade");
                return false;
            } else {
                $("#UpdateProgress").css("display", "block");
                var dataParam = "{DomainID:'" + DomainID + "',UserContactId:'" + UserContactId + "',StageDetailsId:'" + $("#hdnGradeStageDetailsId").val() + "',AssigneIds:'" + $("#ddlTaskGradeAssignees").val().toString() + "',GradeId:'" + $("#ddlTaskGradeList option:selected").val() + "',TaskId:'" + $("#ddlTaskGradeTaskList option:selected").val() + "',StageTaskGradingDetailsId:'0',Mode:'1'}";
                $.ajax({
                    type: "POST",
                    url: "../admin/frmAdminBusinessProcess.aspx/WebMethodAddStageTaskGrading",
                    data: dataParam,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {

                        $("#UpdateProgress").css("display", "none");
                        var Jresponse = $.parseJSON(response.d);
                        GetGradingDetails($("#hdnGradeStageDetailsId").val());
                        //if (Jresponse == '1') {
                        //    GetGradingDetails($("#hdnGradeStageDetailsId").val());
                        //    return false;
                        //} else if (Jresponse == '2') {
                        //    alert("Combination is already exist");
                        //}
                    },
                    failure: function (response) {
                        $("#UpdateProgress").css("display", "none");
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        $("#UpdateProgress").css("display", "none");
                    }, complete: function () {
                        $("#UpdateProgress").css("display", "none");
                    }
                });
            }
        }
        function DeleteGradingDetails(StageTaskGradeDetailsId) {
            var UserContactId ='<%= Session("UserContactId")%>'
            var DomainID = '<%= Session("DomainId")%>';
            $("#UpdateProgress").css("display", "block");
            var dataParam = "{DomainID:'" + DomainID + "',UserContactId:'" + UserContactId + "',StageDetailsId:'" + $("#hdnGradeStageDetailsId").val() + "',AssigneIds:'" + $("#ddlTaskGradeAssignees option:selected").val() + "',GradeId:'" + $("#ddlTaskGradeList option:selected").val() + "',TaskId:'" + $("#ddlTaskGradeTaskList option:selected").val() + "',StageTaskGradingDetailsId:'" + StageTaskGradeDetailsId+"',Mode:'2'}";
            $.ajax({
                type: "POST",
                url: "../admin/frmAdminBusinessProcess.aspx/WebMethodAddStageTaskGrading",
                data: dataParam,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {

                    $("#UpdateProgress").css("display", "none");
                    var Jresponse = $.parseJSON(response.d);
                    GetGradingDetails($("#hdnGradeStageDetailsId").val());
                    return false;
                    //if (Jresponse == "3") {
                    //    return false;
                    //}
                },
                failure: function (response) {
                    $("#UpdateProgress").css("display", "none");
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    $("#UpdateProgress").css("display", "none");
                }, complete: function () {
                    $("#UpdateProgress").css("display", "none");
                }
            });
        }
        function GetGradingDetails(StageDetailsId) {
            var DomainID = '<%= Session("DomainId")%>';
            var dataParam = "{DomainID:'" + DomainID + "',StageDetailsId:'" + StageDetailsId+"'}";
            $("#UpdateProgress").css("display", "block");
            $("#tblGradeTaskDetails tbody").html('');
            $.ajax({
                type: "POST",
                url: "../admin/frmAdminBusinessProcess.aspx/WebMethodGetStageTaskGrading",
                data: dataParam,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    $("#UpdateProgress").css("display", "none");
                    var Jresponse = $.parseJSON(response.d);
                    var itemAppend = '';
                    $("#tblGradeTaskDetails tbody").html("");
                    $.each(Jresponse, function (index, value) {
                        itemAppend = '';

                        itemAppend = itemAppend + "<tr>"
                        itemAppend = itemAppend + "<td>" + value.vcAssignedId + "</td>"
                        itemAppend = itemAppend + "<td>" + value.vcTaskName + "</td>"
                        itemAppend = itemAppend + "<td>" + value.vcGradeId + "</td>"
                        itemAppend = itemAppend + "<td><button type=\"button\" class=\"btn btn-sm btn-danger\" onclick=\"DeleteGradingDetails('"+value.numStageDetailsGradeId +"')\"><i class=\"fa fa-trash\"></i></button></td>";
                        itemAppend = itemAppend + "</tr>"
                        $("#tblGradeTaskDetails tbody").append(itemAppend);
                    });
                },
                failure: function (response) {
                    $("#UpdateProgress").css("display", "none");
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    $("#UpdateProgress").css("display", "none");
                }, complete: function () {
                }
            });
        }
        function GetGradeTeams() {
            var DomainID = '<%= Session("DomainId")%>';
            var dataParam = "{DomainID:'" + DomainID + "'}";
            $("#UpdateProgress").css("display", "block");
            $("#ddlTaskGradeTeam").html('');
            $.ajax({
                type: "POST",
                url: "../admin/frmAdminBusinessProcess.aspx/WebMethodGetTeams",
                data: dataParam,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    $("#UpdateProgress").css("display", "none");
                    var Jresponse = $.parseJSON(response.d);
                    var itemAppend = '';
                    itemAppend = itemAppend + '<option value=\"0\">--Select Team--</option>'
                    $("#ddlTaskGradeTeam").append(itemAppend);
                    $.each(Jresponse, function (index, value) {
                        itemAppend = '';

                        itemAppend = itemAppend + "<option value=\"" + value.numListItemID + "\">" + value.vcData + "</option>"
                        $("#ddlTaskGradeTeam").append(itemAppend);
                    });
                },
                failure: function (response) {
                    $("#UpdateProgress").css("display", "none");
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    $("#UpdateProgress").css("display", "none");
                }, complete: function () {
                }
            });
        }
        
        function openTaskAssignes() {
            var DomainID = '<%= Session("DomainId")%>';
            var teamId = $("#ddlTaskGradeTeam option:selected").val();
            if (teamId == undefined) {
                teamId = 0;
            }
            var dataParam = "{DomainID:'" + DomainID + "',numTeamId:'" + teamId+"'}";
            $("#UpdateProgress").css("display", "block");
            $("#ddlTaskGradeAssignees").html('');
            $.ajax({
                type: "POST",
                url: "../admin/frmAdminBusinessProcess.aspx/WebMethodGetTaskAssignTo",
                data: dataParam,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    $("#UpdateProgress").css("display", "none");
                    var Jresponse = $.parseJSON(response.d);
                    //var itemAppend = '';
                    //itemAppend = itemAppend + '<option value=\"0\">--Select Assign to--</option>'
                    //$("#ddlTaskGradeAssignees").append(itemAppend);
                    $.each(Jresponse, function (index, value) {
                        itemAppend = '';

                        itemAppend = itemAppend + "<option value=\"" + value.numContactID + "\">" + value.vcUserName + "</option>"
                        $("#ddlTaskGradeAssignees").append(itemAppend);
                    });
                    $("#ddlTaskGradeAssignees").multiselect('destroy');
                    $('#ddlTaskGradeAssignees').multiselect({
                        enableClickableOptGroups: true,
                        onSelectAll: function () {
                            console.log("select-all-nonreq");
                        },
                        optionClass: function (element) {
                            var value = $(element).attr("class");
                            return value;
                        }
                    });

                    $("#UpdateProgress").css("display", "none");
                },
                failure: function (response) {
                    $("#UpdateProgress").css("display", "none");
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    $("#UpdateProgress").css("display", "none");
                }, complete: function () {
                }
            });
        }
        function GetTaskByStageDetailsId(StageDetailsId, bitAlphabetical = false) {
            var DomainID = '<%= Session("DomainId")%>';
            var dataParam = "{DomainID:'" + DomainID + "',StageDetailsId:'" + StageDetailsId + "',OppId:0,ProjectId:0,bitAlphabetical:'" + bitAlphabetical + "'}";
            $("#UpdateProgress").css("display", "block");
            $("#ddlTaskGradeTaskList").empty();
            $.ajax({
                type: "POST",
                url: "../admin/frmAdminBusinessProcess.aspx/WebMethodGetTaskList",
                data: dataParam,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    $("#UpdateProgress").css("display", "none");
                    var Jresponse = $.parseJSON(response.d);
                    var itemAppend = '';
                    itemAppend = '';
                    itemAppend = itemAppend + "<option value='0'>Select Task</option>"
                    $("#ddlTaskGradeTaskList").append(itemAppend);
                    $.each(Jresponse, function (index, value) {
                        itemAppend = '';
                        itemAppend = itemAppend + "<option value=" + value.numTaskId+">" + value.vcTaskName + "</option>"
                        $("#ddlTaskGradeTaskList").append(itemAppend);
                        
                    });
                },
                failure: function (response) {
                    $("#UpdateProgress").css("display", "none");
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    $("#UpdateProgress").css("display", "none");
                }, complete: function () {
                }
            });
        }

            function openTaskModal(StageDetailsId) {
                updateStageDetails();
                var DomainID = '<%= Session("DomainId")%>';
            var dataParam = "{DomainID:'" + DomainID + "',numTeamId:'" + $("#ddlTeam_" + StageDetailsId + "").val() + "'}";
            $("#UpdateProgress").css("display", "block");
            $("#ddlTaskAssignTo").html('');
            $("#hdnStageTaskDetailsId").val(StageDetailsId);
            $.ajax({
                type: "POST",
                url: "../admin/frmAdminBusinessProcess.aspx/WebMethodGetTaskAssignTo",
                data: dataParam,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    $("#UpdateProgress").css("display", "none");
                    var Jresponse = $.parseJSON(response.d);
                    var itemAppend = '';
                    itemAppend = itemAppend + '<option value=\"0\">--Select Assign to--</option>'
                    $("#ddlTaskAssignTo").append(itemAppend);
                    $.each(Jresponse, function (index, value) {
                        itemAppend = '';

                        itemAppend = itemAppend + "<option value=\"" + value.numContactId + "\">" + value.vcUserName + "</option>"
                        $("#ddlTaskAssignTo").append(itemAppend);
                    });
                    GetTask(StageDetailsId);
                    $("#UpdateProgress").css("display", "none");
                    $("#taskModal").modal('show');
                },
                failure: function (response) {
                    $("#UpdateProgress").css("display", "none");
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    $("#UpdateProgress").css("display", "none");
                }, complete: function () {
                }
            });
        }
        function AddTasktoStage() {
            var numTaskId = 0;
            numTaskId = $("#hdnTaskId").val();
            var UserContactId ='<%= Session("UserContactId")%>'
            var DomainID = '<%= Session("DomainId")%>';

            if ($("#txtTaskHours").val() == "") {
                $("#txtTaskHours").val("0");
            }
            if ($("#txtTaskMinutes").val() == "") {
                $("#txtTaskMinutes").val("0");
            }
            if ($("#hdnStageTaskDetailsId").val() == "0") {
                alert("Please select a stage");
                return false;
            }
            if ($("#txtTaskName").val() == "") {
                alert("Please enter task name");
                $("#txtTaskName").focus();
                return false;
            } else {
                var TaskType = 0;
                if ($("#rdnTaskType_0").prop("checked") == true) {
                    TaskType = 1;
                } else if ($("#rdnTaskType_1").prop("checked") == true) {
                    TaskType = 2;
                }
                $("#UpdateProgress").css("display", "block");
                var dataParam = "{DomainID:'" + DomainID + "',UserContactId:'" + UserContactId + "',StageDetailsId:'" + $("#hdnStageTaskDetailsId").val() + "',TaskName:'" + $("#txtTaskName").val() + "',Hours:'" + $("#txtTaskHours").val() + "',Minutes:'" + $("#txtTaskMinutes").val() + "',Assignto:'" + $("#ddlTaskAssignTo option:selected").val() + "',ParentTaskId:'0',OppID:'0',IsTaskClosed:false,'numTaskId':" + numTaskId + ",IsTaskSaved:false,IsAutoClosedTaskConfirmed:false,ProjectID:'0',intTaskType:" + TaskType+",WorkOrderID:0}";
                $.ajax({
                    type: "POST",
                    url: "../admin/frmAdminBusinessProcess.aspx/WebMethodAddStageTask",
                    data: dataParam,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {

                        $("#UpdateProgress").css("display", "none");
                        var Jresponse = $.parseJSON(response.d);
                        if (Jresponse == true) {
                            $("#txtTaskMinutes").val('');
                            $("#txtTaskHours").val('');
                            $("#txtTaskName").val('');
                            $("#rdnTaskType_0").removeAttr("checked")
                            $("#rdnTaskType_1").removeAttr("checked")
                            openTaskModal($("#hdnStageTaskDetailsId").val())

                            getStageDetails()
                            $("#hdnTaskId").val(0);
                            //$("#hdnStageTaskDetailsId").val(0);
                            return false;
                        }
                    },
                    failure: function (response) {
                        $("#UpdateProgress").css("display", "none");
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        $("#UpdateProgress").css("display", "none");
                    }, complete: function () {
                        $("#UpdateProgress").css("display", "none");
                    }
                });
            }
        }

        function GetTask(StageDetailsId, bitAlphabetical = false) {
            var DomainID = '<%= Session("DomainId")%>';
            var dataParam = "{DomainID:'" + DomainID + "',StageDetailsId:'" + StageDetailsId + "',OppId:0,ProjectId:0,bitAlphabetical:'" + bitAlphabetical + "'}";
            $("#UpdateProgress").css("display", "block");
            $("#tblTaskDetails tbody").html('');
            $("#divSortTaskDetails").html('');
            var sortItemAppend = '';
            sortItemAppend = sortItemAppend + "<ul id='x' class='sortable boxy'>";
            $.ajax({
                type: "POST",
                url: "../admin/frmAdminBusinessProcess.aspx/WebMethodGetTaskList",
                data: dataParam,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    $("#UpdateProgress").css("display", "none");
                    var Jresponse = $.parseJSON(response.d);
                    var itemAppend = '';
                    $.each(Jresponse, function (index, value) {
                        if (value.intTaskType == 1) {
                            $("#rdnTaskType_0").prop("checked", "checked");
                        } else if (value.intTaskType == 2) {
                            $("#rdnTaskType_1").prop("checked", "checked");
                        } else {
                            $("#rdnTaskType_1").removeAttr("checked");
                        }
                        itemAppend = '';
                        itemAppend = itemAppend + "<tr><td>" + value.vcTaskName + "</td><td>" + value.numHours + "</td><td>" + value.numMinutes + "</td><td>" + value.vcContactName + "</td>"
                        itemAppend = itemAppend + "<td><button type=\"button\" onclick=\"deleteTask(" + value.numTaskId + ")\" class=\"btn btn-xs btn-danger\"><i class=\"fa fa-trash\"></i></button>&nbsp;<button type=\"button\" onclick=\"editTask(" + value.numTaskId + ",'" + value.vcTaskName + "','" + value.numHours + "','" + value.numMinutes + "','" + value.numAssignTo + "')\" class=\"btn btn-xs btn-primary\"><i class=\"fa fa-pencil\"></i></button></td>";
                        itemAppend = itemAppend + "</tr>";
                        sortItemAppend = sortItemAppend + "<li id='" + value.numTaskId + "'>" + value.vcTaskName + "</li>";
                        $("#tblTaskDetails tbody").append(itemAppend);
                    });

                    sortItemAppend = sortItemAppend + "</ul>";
                    $("#divSortTaskDetails").append(sortItemAppend);
                    debugger;
                    if (document.getElementById("x") != null) {
                        var list = document.getElementById("x");
                        DragDrop.makeListContainer(list, 'g2');
                        list.onDragOver = function () { this.style["background"] = "none"; };
                        list.onDragOut = function () { this.style["background"] = "none"; };
                    }
                },
                failure: function (response) {
                    $("#UpdateProgress").css("display", "none");
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    $("#UpdateProgress").css("display", "none");
                }, complete: function () {
                }
            });
        }
            function editTask(TaskId, TaskName, Hours, Minutes, AssignTo) {
                $("#hdnTaskId").val(TaskId);
                $("#txtTaskName").val(TaskName);
                $("#txtTaskHours").val(Hours);
                $("#txtTaskMinutes").val(Minutes);
                $("#ddlTaskAssignTo").val(AssignTo);
            }
            function deleteTask(TaskId) {
                var DomainID = '<%= Session("DomainId")%>';
            var dataParam = "{DomainID:'" + DomainID + "',TaskId:'" + TaskId + "'}";
            $("#UpdateProgress").css("display", "block");
            $.ajax({
                type: "POST",
                url: "../admin/frmAdminBusinessProcess.aspx/WebMethodDeleteTask",
                data: dataParam,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    $("#UpdateProgress").css("display", "none");
                    var Jresponse = $.parseJSON(response.d);
                    if (Jresponse == "1") {
                        alert("Task Deleted Successfully");
                        GetTask($("#hdnStageTaskDetailsId").val());
                    }
                },
                failure: function (response) {
                    $("#UpdateProgress").css("display", "none");
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    $("#UpdateProgress").css("display", "none");
                }, complete: function () {
                }
            });
        }
        function GetTeams(StageDetailsId, selectedValue) {
            var DomainID = '<%= Session("DomainId")%>';
            var dataParam = "{DomainID:'" + DomainID + "'}";
            $("#UpdateProgress").css("display", "block");
            $("#ddlTeam_" + StageDetailsId + "").html('');
            $.ajax({
                type: "POST",
                url: "../admin/frmAdminBusinessProcess.aspx/WebMethodGetTeams",
                data: dataParam,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    $("#UpdateProgress").css("display", "none");
                    var Jresponse = $.parseJSON(response.d);
                    var itemAppend = '';
                    itemAppend = itemAppend + '<option value=\"0\">--Select Team--</option>'
                    $("#ddlTeam_" + StageDetailsId + "").append(itemAppend);
                    $.each(Jresponse, function (index, value) {
                        itemAppend = '';

                        itemAppend = itemAppend + "<option value=\"" + value.numListItemID + "\">" + value.vcData + "</option>"
                        $("#ddlTeam_" + StageDetailsId + "").append(itemAppend);
                    });
                    $("#ddlTeam_" + StageDetailsId + "").val(selectedValue);
                },
                failure: function (response) {
                    $("#UpdateProgress").css("display", "none");
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    $("#UpdateProgress").css("display", "none");
                }, complete: function () {
                }
            });
        }
        function addStage(StagePerID, ConfigurationId, idToShow, Percentage) {
            updateStageDetails();
            var UserContactId ='<%= Session("UserContactId")%>'
            var DomainID = '<%= Session("DomainId")%>';
            if ($("#ddlProcesses option:selected").val() == "0") {
                alert("Please select process");
                $("#ddlProcesses").focus();
                return false;
            }
            if ($("#mileStone_txtName_" + StagePerID + "").val() == "") {
                alert("Please enter mileStone");
                $("#mileStone_txtName_" + StagePerID + "").focus();
                return false;
            }
            $("#UpdateProgress").css("display", "block");
            var dataParam = "{DomainID:'" + DomainID + "',UserContactId:'" + UserContactId + "',ProcessId:'" + $("#ddlProcesses option:selected").val() + "',StagePerID:'" + StagePerID + "',ConfigurationId:'" + ConfigurationId + "',StageName:'" + $("#mileStone_txtName_" + StagePerID + "").val() + "',Percentage:'" + Percentage + "'}";
            $.ajax({
                type: "POST",
                url: "../admin/frmAdminBusinessProcess.aspx/WebMethodAddStagesToMileStone",
                data: dataParam,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    var Jresponse = $.parseJSON(response.d);

                    if (Jresponse == true) {
                        $(".panel-collapse").collapse('hide');
                        setTimeout(function () { $('#' + idToShow + '').collapse('show'); }, 1000);
                        //$(".panel-collapse").collapse('hide');
                        //$('#' + idToShow + '').collapse('show');
                        getStageDetails();
                        return false;
                    } else {
                        $("#UpdateProgress").css("display", "none");
                    }
                },
                failure: function (response) {
                    $("#UpdateProgress").css("display", "none");
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    $("#UpdateProgress").css("display", "none");
                }, complete: function () {
                    $("#UpdateProgress").css("display", "none");
                }
            });
        }

        function deleteStages() {
            updateStageDetails();
            var UserContactId ='<%= Session("UserContactId")%>'
            var DomainID = '<%= Session("DomainId")%>';
            var strStageDetailsIds = '';
            $("#UpdateProgress").css("display", "block");
            var datatoDelete = '';
            $('.chkStageDelete').each(function (index, obj) {

                if (this.checked === true) {
                    strStageDetailsIds = strStageDetailsIds + ',' + $(this).attr("values");
                }
            });
            var dataParam = "{DomainID:'" + DomainID + "',strStageDetailsIds:'" + strStageDetailsIds + "'}";
            $.ajax({
                type: "POST",
                url: "../admin/frmAdminBusinessProcess.aspx/WebMethodDeleteStages",
                data: dataParam,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {

                    var Jresponse = $.parseJSON(response.d);
                    if (Jresponse == true) {
                        SetDefaultSetupForConfig($("#hdnConfigurationId").val());
                    } else {
                        $("#UpdateProgress").css("display", "none");
                    }
                },
                failure: function (response) {
                    $("#UpdateProgress").css("display", "none");
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    $("#UpdateProgress").css("display", "none");
                }, complete: function () {
                    $("#UpdateProgress").css("display", "none");
                }
            });
        }
        function openSortModal() {
            $("#sortTaskModal").modal("show")
        }
        function sortAlphabetically() {
            GetTask($("#hdnStageTaskDetailsId").val(), true);
        }
        function saveSortTask() {
            var sortTask = DragDrop.serData('g2', null);
            //sortTask = sortTask.replace("x(", "");
            //sortTask = sortTask.replace(")", "");
            var UserContactId ='<%= Session("UserContactId")%>'
            var DomainID = '<%= Session("DomainId")%>';
            $("#UpdateProgress").css("display", "block");
            var dataParam = "{DomainID:'" + DomainID + "',sortTask:'" + sortTask + "'}";
            $.ajax({
                type: "POST",
                url: "../admin/frmAdminBusinessProcess.aspx/WebMethodSortTask",
                data: dataParam,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {

                    var Jresponse = $.parseJSON(response.d);
                    debugger;
                    if (Jresponse == "0") {
                        GetTask($("#hdnStageTaskDetailsId").val(), false);
                    }
                    $("#UpdateProgress").css("display", "none");
                },
                failure: function (response) {
                    $("#UpdateProgress").css("display", "none");
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    $("#UpdateProgress").css("display", "none");
                }, complete: function () {
                    $("#UpdateProgress").css("display", "none");
                }
            });
        }
    </script>
    <style type="text/css">
        .arow {
            background-color: #fffff;
            text-align: left;
            color: black;
            font-family: Arial;
            font-size: 8pt;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="RecordInformationPanel" runat="server"
    ClientIDMode="Static">
    <div class="row padbottom10">
        <div class="col-xs-12">
            <div class="pull-left">
                <div class="form-inline">
                    <input type="hidden" id="hdnConfigurationId" value="0" />
                    <asp:DropDownList ID="ddlConfiguration" Style="display: none" CssClass="form-control" runat="server" AutoPostBack="True">
                        <asp:ListItem Value="1" Selected="True">Configuration 1</asp:ListItem>
                        <asp:ListItem Value="2">Configuration 2</asp:ListItem>
                        <asp:ListItem Value="3">Configuration 3 </asp:ListItem>
                        <asp:ListItem Value="4">Configuration 4</asp:ListItem>
                        <asp:ListItem Value="5">Configuration 5</asp:ListItem>
                    </asp:DropDownList>
                    <input type="radio" name="rdnProcessType" value="0" onclick="BindProcessList()" checked /><label>Sales</label>
                    <input type="radio" name="rdnProcessType" value="2" onclick="BindProcessList()" /><label>Purchase</label>
                    <input type="radio" name="rdnProcessType" value="1" onclick="BindProcessList()" /><label>Projects</label>
                    <input type="radio" name="rdnProcessType" value="3" onclick="BindProcessList()" /><label>Build</label>

                    <asp:DropDownList ID="ddlProcess" GroupName="rdbProcessType" CssClass="form-control" runat="server" AutoPostBack="True" Style="display: none">
                        <asp:ListItem Value="0">Sales Process</asp:ListItem>
                        <asp:ListItem Value="2">Purchase Process</asp:ListItem>
                        <asp:ListItem Value="1">Projects</asp:ListItem>
                    </asp:DropDownList>
                    <asp:RequiredFieldValidator ID="rfvKitWareHouse" runat="server" ErrorMessage="Select The Process"
                        ControlToValidate="ddlProcess" Display="Dynamic" SetFocusOnError="true" ValidationGroup="vgRequired"
                        Text="*"></asp:RequiredFieldValidator>
                    <label style="padding-left: 40px !important">Process List:</label>
                    <select class="form-control" id="ddlProcesses" style="width: 222px;">
                    </select>
                    <asp:DropDownList ID="ddlProcessList" Style="display: none" CssClass="form-control" runat="server"
                        AutoPostBack="True">
                    </asp:DropDownList>
                    <label>Add New Process :</label>
                    <asp:TextBox runat="server" ID="txtName" placeholder="- Add a new process name here -" Style="width: 220px;" CssClass="form-control"></asp:TextBox>
                    <asp:DropDownList ID="ddlStag" CssClass="form-control" runat="server" Width="200">
                        <asp:ListItem Value="1" Selected="True">Configuration 1 (100%)</asp:ListItem>
                        <asp:ListItem Value="2">Configuration 2(50%,100%)</asp:ListItem>
                        <asp:ListItem Value="3">Configuration 3 (33%,66%,100%)</asp:ListItem>
                        <asp:ListItem Value="4">Configuration 4 (25%,50%,75%,100%)</asp:ListItem>
                        <asp:ListItem Value="5">Configuration 5 (20%,40%,60%,80%,100%)</asp:ListItem>
                    </asp:DropDownList>
                </div>
            </div>
            <div class="pull-right">
                <asp:LinkButton runat="server" ID="btnNewProcess" Style="display: none" CssClass="btn btn-primary" Text="" OnClientClick="return OpenNew()"><i class="fa fa-plus-circle"></i>&nbsp;New Business Process</asp:LinkButton>
                <button class="btn btn-primary" type="button" onclick="SaveNewProcess()"><i class="fa fa-plus"></i>&nbsp;&nbsp;Add</button>
                <button type="button" id="btnDelete" onclick="deleteProcess()" runat="server" class="btn btn-danger"><i class="fa fa-trash"></i>&nbsp;&nbsp;Delete</button>

            </div>

        </div>
        <div class="pull-right">

            <span style="font-size: 14px !important; margin-right: 10px;">(<i><b>Note</b> - The sum total of all stages within each milestone must equal 100%</i>)</span>
        </div>
    </div>

    <!-- Modal -->
    <div id="taskGradingBasedOnStagingDetailsModal" class="modal fade" role="dialog">
        <div class="modal-dialog modal-md">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Gradation</h4>
                </div>
                <div class="modal-body">
                    <input type="hidden" id="hdnGradeStageDetailsId" />
                    <div class="col-md-3">
                        <label>Team</label>
                        <select class="form-control" id="ddlTaskGradeTeam" onchange="openTaskAssignes()">
                            <option value="0">-Select-</option>
                        </select>
                    </div>

                    <div class="col-md-3">
                        <label>Assignee<span class="text-danger">*</span></label>
                        <div style="width: 100%">
                            <select class="form-control" id="ddlTaskGradeAssignees" multiple>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <label>Task<span class="text-danger">*</span></label>
                        <select class="form-control" id="ddlTaskGradeTaskList">
                        </select>
                    </div>

                    <div class="col-md-3">
                        <label>Grade<span class="text-danger">*</span></label>
                        <select class="form-control" id="ddlTaskGradeList">
                            <option value="0">-SELECT-</option>
                            <option value="A+">A+</option>
                            <option value="A">A</option>
                            <option value="A-">A-</option>
                            <option value="B+">B+</option>
                            <option value="B">B</option>
                            <option value="B-">B-</option>
                            <option value="C+">C+</option>
                            <option value="C">C</option>
                            <option value="C-">C-</option>
                        </select>
                    </div>
                    <div class="col-md-12" style="margin-top: 10px;">
                        <table class="table table-bordered table-striped" id="tblGradeTaskDetails">
                            <thead>
                                <tr>
                                    <th>Assigne</th>
                                    <th>Task</th>
                                    <th>Grad</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" onclick="AddGradingDetails()">Save</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>

    <!-- Modal -->
    <div id="taskModal" class="modal fade" role="dialog">
        <div class="modal-dialog modal-md">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Task</h4>
                </div>
                <div class="modal-body">
                    <input type="hidden" id="hdnStageTaskDetailsId" />
                    <div class="col-md-4">
                        <span>Note - At least 1 task is required per stage.</span>
                    </div>
                    <div class="col-md-8">
                        <asp:RadioButtonList ID="rdnTaskType" runat="server">
                            <asp:ListItem Value="1">Parallel:  Can start anytime after build process is created.</asp:ListItem>
                            <asp:ListItem Value="2">Sequential: Can start after previous task is finished</asp:ListItem>
                        </asp:RadioButtonList>
                    </div>

                    <div class="col-md-6">
                        <label><span class="text-danger">*</span>Task</label>
                        <input type="text" class="form-control" id="txtTaskName" />
                    </div>
                    <div class="col-md-1" style="width: 12%;">
                        <label>Hours</label>
                        <input type="number" class="form-control" id="txtTaskHours" max="24" min="0" />
                    </div>
                    <div class="col-md-1" style="width: 12%;">
                        <label>Minutes</label>
                        <input type="number" class="form-control" id="txtTaskMinutes" max="60" min="1" />
                    </div>

                    <div class="col-md-3">
                        <label>Assign to</label>
                        <select class="form-control" id="ddlTaskAssignTo">
                        </select>
                    </div>
                    <div class="col-md-12">
                        <input type="hidden" id="hdnTaskId" value="0" />
                        <table class="table table-bordered table-striped" id="tblTaskDetails">
                            <thead>
                                <tr>
                                    <th>Task</th>
                                    <th>Hours</th>
                                    <th>Minutes</th>
                                    <th>Assign to</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" onclick="openSortModal()"><i class="fa fa-sort"></i>Sort</button>
                    <button type="button" class="btn btn-primary" onclick="AddTasktoStage()">Add Task</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>


    <div id="sortTaskModal" class="modal fade" role="dialog">
        <div class="modal-dialog modal-md" style="width: 40%">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Task</h4>
                </div>
                <div class="modal-body">

                    <div class="col-md-12" id="divSortTaskDetails">
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" onclick="sortAlphabetically()">Sort Alphabetically</button>
                    <button type="button" class="btn btn-primary" onclick="saveSortTask()">Save</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="RecordActionPanel" runat="server"
    ClientIDMode="Static">
    <asp:Label ID="lblMessage" runat="server"></asp:Label>
    <br />
    <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="false" ShowSummary="false" ValidationGroup="vgRequired" />
    <asp:TextBox ID="txtMSMaxStage5" Style="display: none" runat="server" Text="0"></asp:TextBox>
    <asp:TextBox ID="txtMSMaxStage20" Style="display: none" runat="server" Text="0"></asp:TextBox>
    <asp:TextBox ID="txtMSMaxStage25" Style="display: none" runat="server" Text="0"></asp:TextBox>
    <asp:TextBox ID="txtMSMaxStage35" Style="display: none" runat="server" Text="0"></asp:TextBox>
    <asp:TextBox ID="txtMSMaxStage40" Style="display: none" runat="server" Text="0"></asp:TextBox>
    <asp:TextBox ID="txtMSMaxStage50" Style="display: none" runat="server" Text="0"></asp:TextBox>
    <asp:TextBox ID="txtMSMaxStage60" Style="display: none" runat="server" Text="0"></asp:TextBox>
    <asp:TextBox ID="txtMSMaxStage65" Style="display: none" runat="server" Text="0"></asp:TextBox>
    <asp:TextBox ID="txtMSMaxStage75" Style="display: none" runat="server" Text="0"></asp:TextBox>
    <asp:TextBox ID="txtMSMaxStage80" Style="display: none" runat="server" Text="0"></asp:TextBox>
    <asp:TextBox ID="txtMSMaxStage95" Style="display: none" runat="server" Text="0"></asp:TextBox>
    <asp:TextBox ID="txtAddStage" Style="display: none" runat="server" Text="0"></asp:TextBox>
    <asp:TextBox ID="txtDeleteStage" Style="display: none" runat="server" Text="0"></asp:TextBox>
    <input id="hdnConfigurationVal" type="hidden" name="hdnConfigurationVal" runat="server">
    <input id="mode" type="hidden" name="mode" runat="server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="DetailPageTitle" runat="server"
    ClientIDMode="Static">
    Milestones &amp; Stages&nbsp;<a href="#" onclick="return OpenHelpPopUp('admin/frmAdminBusinessProcess.aspx')"><label class="badge bg-yellow">?</label></a>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="TabsPlaceHolder" runat="server"
    ClientIDMode="AutoID">
    <asp:UpdateProgress ID="UpdateProgress" runat="server" ClientIDMode="Static">
        <ProgressTemplate>
            <div class="overlay">
                <div class="overlayContent" style="color: #000; text-align: center; width: 250px; padding: 20px">
                    <i class="fa fa-2x fa-refresh fa-spin"></i>
                    <h3>Processing Request</h3>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <div class="row padbottom10">
        <div class="col-xs-12">
            <div class="form-inline">
                <div class="form-group" id="divBuildManager" style="display: none;">
                    <label>Auto-select the following build manager</label>
                    <asp:DropDownList ID="ddlBuildManager" runat="server" CssClass="form-control"></asp:DropDownList>
                </div>
                &nbsp;&nbsp;&nbsp;
                <div class="form-group">
                    <asp:CheckBox ID="chkTimerWindowOpens" Text="Automatically start timer when stage task time window opens" runat="server" />
                </div>
                &nbsp;&nbsp;&nbsp;
                <div class="form-group">
                    <asp:CheckBox ID="chkAssignStagetoTeams" Text="Assign stages to teams" runat="server" />
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="box-group boxForList" id="DropDownListaccordion">
                <!-- we are adding the .panel class so bootstrap.js collapse plugin detects it -->
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
    <div style="background-color: #fff; display: none">
        <div class="box  box-primary box-solid" id="divstage5" runat="server">
            <div class="box-header with-border">
                <h3 class="box-title">Milestone 5%</h3>
            </div>
            <div class="box-body">
                <div class="row padbottom10">
                    <div class="col-xs-12">
                        <div class="pull-left">
                            <div class="form-inline">
                                <label>Milestone Name:</label>

                                <span id="span5" runat="server"></span>

                            </div>
                        </div>
                        <div class="pull-right">
                            <asp:Button ID="btnAddStage5" OnClientClick="return AddStage(1)" CssClass="btn btn-sm btn-primary" runat="server" Text="Add Stage"></asp:Button>
                            <asp:Button ID="btnDelStage5" OnClientClick="return DelStage(5,1)" CssClass="btn btn-sm btn-danger" runat="server" Text="Delete Stage"></asp:Button>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-xs-12">
                        <div class="table-responsive">
                            <asp:Table runat="server" ID="tbl5" Width="100%" CellSpacing="0" CssClass="table table-bordered table-striped"
                                CellPadding="0">
                            </asp:Table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <div class="box  box-primary box-solid" id="divstage20" runat="server">
            <div class="box-header with-border">
                <h3 class="box-title">Milestone 20%</h3>
            </div>
            <div class="box-body">
                <div class="row padbottom10">
                    <div class="col-xs-12">
                        <div class="pull-left">
                            <div class="form-inline">
                                <label>Milestone Name:</label>
                                <span id="span20" runat="server"></span>
                            </div>
                        </div>
                        <div class="pull-right">

                            <asp:Button ID="btnAddStage20" OnClientClick="return AddStage(2)" CssClass="btn btn-sm btn-primary"
                                runat="server" Text="Add Stage"></asp:Button>
                            <asp:Button ID="btnDelStage20" OnClientClick="return DelStage(20,2)" CssClass="btn btn-sm btn-danger"
                                runat="server" Text="Delete Stage"></asp:Button>

                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <div class="table-responsive">
                            <asp:Table runat="server" ID="tbl20" Width="100%" CssClass="table table-bordered table-striped" CellSpacing="0"
                                CellPadding="0">
                            </asp:Table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="box  box-primary box-solid" id="divstage25" runat="server">
            <div class="box-header with-border">
                <h3 class="box-title">Milestone 25%</h3>
            </div>
            <div class="box-body">
                <div class="row padbottom10">
                    <div class="col-xs-12">
                        <div class="pull-left">
                            <div class="form-inline">
                                <label>Milestone Name:</label>
                                <span id="span25" runat="server"></span>
                            </div>
                        </div>
                        <div class="pull-right">
                            <asp:Button ID="btnAddStage25" OnClientClick="return AddStage(3)" CssClass="btn btn-sm btn-primary"
                                runat="server" Text="Add Stage"></asp:Button>
                            <asp:Button ID="btnDelStage25" OnClientClick="return DelStage(25,3)" CssClass="btn btn-sm btn-danger"
                                runat="server" Text="Delete Stage"></asp:Button>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <div class="table-responsive">
                            <asp:Table runat="server" ID="tbl25" Width="100%" CssClass="table table-bordered table-striped" CellSpacing="0"
                                CellPadding="0">
                            </asp:Table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="box  box-primary box-solid" id="divstage35" runat="server">
            <div class="box-header with-border">
                <h3 class="box-title">Milestone 35%</h3>
            </div>
            <div class="box-body">
                <div class="row padbottom10">
                    <div class="col-xs-12">
                        <div class="pull-left">
                            <div class="form-inline">
                                <label>Milestone Name:</label>
                                <span id="span35" runat="server"></span>
                            </div>
                        </div>
                        <div class="pull-right">
                            <asp:Button ID="btnAddStage35" OnClientClick="return AddStage(4)" CssClass="btn btn-sm btn-primary"
                                runat="server" Text="Add Stage"></asp:Button>
                            <asp:Button ID="btnDelstage35" OnClientClick="return DelStage(35,4)" CssClass="btn btn-sm btn-danger"
                                runat="server" Text="Delete Stage"></asp:Button>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <div class="table-responsive">
                            <asp:Table runat="server" ID="tbl35" Width="100%" CssClass="table table-bordered table-striped" CellSpacing="0"
                                CellPadding="0">
                            </asp:Table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="box  box-primary box-solid" id="divstage40" runat="server">
            <div class="box-header with-border">
                <h3 class="box-title">Milestone 40%</h3>
            </div>
            <div class="box-body">
                <div class="row padbottom10">
                    <div class="col-xs-12">
                        <div class="pull-left">
                            <div class="form-inline">
                                <label>Milestone Name:</label>
                                <span id="span40" runat="server"></span>
                            </div>
                        </div>
                        <div class="pull-right">
                            <asp:Button ID="btnAddStage40" OnClientClick="return AddStage(5)" CssClass="btn btn-sm btn-primary"
                                runat="server" Text="Add Stage"></asp:Button>
                            <asp:Button ID="btnDelStage40" OnClientClick="return DelStage(40,5)" CssClass="btn btn-sm btn-danger"
                                runat="server" Text="Delete Stage"></asp:Button>
                        </div>

                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <div class="table-responsive">
                            <asp:Table runat="server" ID="tbl40" Width="100%" CssClass="table table-bordered table-striped" CellSpacing="0"
                                CellPadding="0">
                            </asp:Table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="box  box-primary box-solid" id="divstage50" runat="server">
            <div class="box-header with-border">
                <h3 class="box-title">Milestone 50%</h3>
            </div>
            <div class="box-body">
                <div class="row padbottom10">
                    <div class="col-xs-12">
                        <div class="pull-left">
                            <div class="form-inline">
                                <label>&nbsp;Milestone Name:</label>
                                <span id="span50" runat="server"></span>
                            </div>
                        </div>
                        <div class="pull-right">
                            <asp:Button ID="btnAddStage50" OnClientClick="return AddStage(6)" CssClass="btn btn-sm btn-primary" runat="server" Text="Add Stage"></asp:Button>
                            <asp:Button ID="btnDelStage50" OnClientClick="return DelStage(50,6)" CssClass="btn btn-sm btn-danger" runat="server" Text="Delete Stage"></asp:Button>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <div class="table-responsive">
                            <asp:Table runat="server" ID="tbl50" Width="100%" CssClass="table table-bordered table-striped" CellSpacing="0" CellPadding="0">
                            </asp:Table>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <div class="box  box-primary box-solid" id="divstage60" runat="server">
            <div class="box-header with-border">
                <h3 class="box-title">Milestone 60%</h3>
            </div>
            <div class="box-body">
                <div class="row padbottom10">
                    <div class="col-xs-12">
                        <div class="pull-left">
                            <div class="form-inline">
                                <label>Milestone Name:</label>
                                <span id="span60" runat="server"></span>
                            </div>
                        </div>
                        <div class="pull-right">
                            <asp:Button ID="Button1" OnClientClick="return AddStage(7)" CssClass="btn btn-sm btn-primary"
                                runat="server" Text="Add Stage"></asp:Button>
                            <asp:Button ID="Button2" OnClientClick="return DelStage(60,7)" CssClass="btn btn-sm btn-danger"
                                runat="server" Text="Delete Stage"></asp:Button>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <div class="table-responsive">
                            <asp:Table runat="server" ID="tbl60" Width="100%" CssClass="table table-bordered table-striped" CellSpacing="0"
                                CellPadding="0">
                            </asp:Table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <div class="box  box-primary box-solid" id="divstage65" runat="server">
            <div class="box-header with-border">
                <h3 class="box-title">Milestone 65%</h3>
            </div>
            <div class="box-body">
                <div class="row padbottom10">
                    <div class="col-xs-12">
                        <div class="pull-left">
                            <div class="form-inline">
                                <label>Milestone Name:</label>
                                <span id="span65" runat="server"></span>
                            </div>
                        </div>
                        .
                        <div class="pull-right">
                            <asp:Button ID="btnAddStage65" OnClientClick="return AddStage(8)" CssClass="btn btn-sm btn-primary"
                                runat="server" Text="Add Stage"></asp:Button>
                            <asp:Button ID="btnDelStage65" OnClientClick="return DelStage(65,8)" CssClass="btn btn-sm btn-danger"
                                runat="server" Text="Delete Stage"></asp:Button>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <div class="table-responsive">
                            <asp:Table runat="server" ID="tbl65" Width="100%" CssClass="table table-bordered table-striped" CellSpacing="0"
                                CellPadding="0">
                            </asp:Table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="box  box-primary box-solid" id="divstage75" runat="server">
            <div class="box-header with-border">
                <h3 class="box-title">Milestone 75%</h3>
            </div>
            <div class="box-body">

                <div class="row padbottom10">
                    <div class="col-xs-12">
                        <div class="pull-left">
                            <div class="form-inline">
                                <label>Milestone Name:</label>
                                <span id="span75" runat="server"></span>
                            </div>
                        </div>
                        .
                        <div class="pull-right">
                            <asp:Button ID="btnAddStage75" OnClientClick="return AddStage(9)" CssClass="btn btn-sm btn-primary"
                                runat="server" Text="Add Stage"></asp:Button>
                            <asp:Button ID="btnDelStage75" OnClientClick="return DelStage(75.9)" CssClass="btn btn-sm btn-danger"
                                runat="server" Text="Delete Stage"></asp:Button>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <div class="table-responsive">
                            <asp:Table runat="server" ID="tbl75" Width="100%" CssClass="table table-bordered table-striped" CellSpacing="0"
                                CellPadding="0">
                            </asp:Table>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div class="box  box-primary box-solid" id="divstage80" runat="server">
            <div class="box-header with-border">
                <h3 class="box-title">Milestone 80%</h3>
            </div>
            <div class="box-body">

                <div class="row padbottom10">
                    <div class="col-xs-12">
                        <div class="pull-left">
                            <div class="form-inline">
                                <label>Milestone Name:</label>
                                <span id="span80" runat="server"></span>
                            </div>
                        </div>
                        .
                        <div class="pull-right">
                            <asp:Button ID="btnAddStage80" OnClientClick="return AddStage(10)" CssClass="btn btn-sm btn-primary"
                                runat="server" Text="Add Stage"></asp:Button>
                            <asp:Button ID="btnDelStage80" OnClientClick="return DelStage(80,10)" CssClass="btn btn-sm btn-danger"
                                runat="server" Text="Delete Stage"></asp:Button>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <div class="table-responsive">
                            <asp:Table runat="server" ID="tbl80" Width="100%" CssClass="table table-bordered table-striped" CellSpacing="0"
                                CellPadding="0">
                            </asp:Table>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div class="box  box-primary box-solid" id="divstage95" runat="server">
            <div class="box-header with-border">
                <h3 class="box-title">Milestone 95%</h3>
            </div>
            <div class="box-body">

                <div class="row padbottom10">
                    <div class="col-xs-12">
                        <div class="pull-left">
                            <div class="form-inline">
                                <label>Milestone Name:</label>
                                <span id="span95" runat="server"></span>
                            </div>
                        </div>
                        .
                        <div class="pull-right">
                            <asp:Button ID="btnAddStage95" OnClientClick="return AddStage(11)" CssClass="btn btn-sm btn-primary"
                                runat="server" Text="Add Stage"></asp:Button>
                            <asp:Button ID="btnDelStage95" OnClientClick="return DelStage(95,11)" CssClass="btn btn-sm btn-danger"
                                runat="server" Text="Delete Stage"></asp:Button>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-xs-12">
                        <div class="table-responsive">
                            <asp:Table runat="server" ID="tbl95" Width="100%" CssClass="table table-bordered table-striped" CellSpacing="0"
                                CellPadding="0">
                            </asp:Table>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="UtilityLinksPanel" runat="server">
    <div class="form-inline">
        <a href="#" title="As users click the task row check box to mark it complete, if any previous task has not been marked complete, you can have BizAutomation either auto-complete these tasks, or warn the user that tasks aren�t complete, then provide the option to auto-mark them as complete. �All previous� refers to all tasks within current or previous stages.
">[?]</a><label>&nbsp;Task Validation</label>
        <select class="form-control" id="ddlTaskValidation" style="width: 250px">
            <option value="0">- No validation - </option>
            <option value="1">Automatically auto-complete previous tasks (Within stage)</option>
            <option value="2">Warn & provide option to auto-complete (Within stage)</option>
            <option value="3">Automatically auto-complete previous tasks (All previous)</option>
            <option value="4">Warn & provide option to auto-complete (All Previous)</option>
        </select>
        <button type="button" class="btn btn-sm btn-primary" id="btnSaveStageDetails" onclick="UpdateProcess()"><i class="fa fa-floppy-o"></i>&nbsp;&nbsp;Save</button>

    </div>
    <asp:LinkButton ID="btnSave" CssClass="btn btn-sm btn-primary" Style="display: none" runat="server" ValidationGroup="vgRequired" OnClientClick="return ClientSideClick()"><i class="fa fa-floppy-o"></i>&nbsp;&nbsp;Save</asp:LinkButton>
    <asp:HiddenField ID="hdnProcessType" runat="server" />
</asp:Content>
