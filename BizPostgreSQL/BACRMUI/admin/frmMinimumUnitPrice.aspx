﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmMinimumUnitPrice.aspx.vb"
    Inherits=".frmMinimumUnitPrice" MasterPageFile="~/common/Popup.Master" ClientIDMode="Predictable" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<asp:Content ID="Content3" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">

    <title>UOM - Unit Of Measurement Conversion</title>
    <script language="javascript" type="text/javascript">
        function Close() {
            if (window.opener != null) {
                //window.opener.location.reload(true);
            }
            window.close();
        }

        function ClientSideClick() {
            var isGrpOneValid = Page_ClientValidate("vgRequired");
            var check = Check();

            var i;
            for (i = 0; i < Page_Validators.length; i++) {
                ValidatorValidate(Page_Validators[i]); //this forces validation in all groups
            }

            if (isGrpOneValid && check) {
                return true;
            }
            else
                return false;
        }

        function Check() {
            if (Number(document.getElementById("hfTotalRow").value) > 0) {
                for (var i = 1; i <= Number(document.getElementById("hfTotalRow").value) ; i++) {
                    if (document.getElementById("ddlUnit_1_" + i).value == document.getElementById("ddlUnit_2_" + i).value) {
                        alert('Base and Conversion Unit are not same!!!');
                        return false;
                    }
                    for (var j = 1; j <= Number(document.getElementById("hfTotalRow").value) ; j++) {
                        if (j != i) {
                            if ((document.getElementById("ddlUnit_1_" + i).value == document.getElementById("ddlUnit_1_" + j).value && document.getElementById("ddlUnit_2_" + i).value == document.getElementById("ddlUnit_2_" + j).value)
                                  || (document.getElementById("ddlUnit_1_" + i).value == document.getElementById("ddlUnit_2_" + j).value && document.getElementById("ddlUnit_2_" + i).value == document.getElementById("ddlUnit_1_" + j).value)) {
                                alert('Select distinct base and conversion unit!!!');
                                return false;
                            }
                        }
                    }
                }
            } 
            else {
                return false;
            }
            return true;
        }
        function OpenUnitPriceApproverPopUp() {
            window.open("../Admin/frmUnitPriceApprover.aspx?ID=" + document.getElementById("hdnUnitPriceApprover").value, "", "width=900,height=400,status=no,scrollbars=yes,left=155,top=160");
            return false;
        }
    </script>
    <style type="text/css">
        .row {
            background-color: #DBE5F1;
            text-align: left;
            color: black;
            font-family: Arial;
            font-size: 8pt;
        }

        .arow {
            background-color: #fffff;
            text-align: left;
            color: black;
            font-family: Arial;
            font-size: 8pt;
        }

        input[type='checkbox'] {
            position: relative;
            bottom: 1px;
            vertical-align: middle;
        }

        .tooltip {
            top: 0px !important;
        }

    </style>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <div class="input-part">
        <div class="right-input">
            <asp:CheckBox ID="chkEnable" runat="server" Text="Enable" />
            <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save"
                OnClientClick="return ClientSideClick()"></asp:Button>
            <asp:Button ID="btnSaveClose" runat="server" CssClass="button" Text="Save &amp; Close"
                OnClientClick="return ClientSideClick()"></asp:Button>
            <asp:Button ID="btnCancel" runat="server" CssClass="button" Width="50" Text="Close"></asp:Button>&nbsp;&nbsp;
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="PageTitle" runat="server" ClientIDMode="Static">
    Price Margin Enforcement Rules
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="Content" runat="server" ClientIDMode="Static">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    
    
    <table cellpadding="0" cellspacing="0" width="950">
        <tr>
            <td>
                <b>Activate Sales Opportunities & Orders Minimum Price Margin Control</b> 
            </td>
            <td>
                <a href="#" style="display:none" onclick="OpenUnitPriceApproverPopUp()">Authorized Approvers</a>
            </td>
            <td>
               <asp:Label Visible="false" ID="Label1" Text="[?]" CssClass="tip" runat="server" ToolTip="Use this tool to enforce minimum price margins when creating sales orders. You do this by setting rules that tigger approval requests when minimum profit margins thresholds are crossed. 
Once you’ve set your rules separated by item classifications, you’ll need to assign authorized approvers. Margin violations will still allow for creation of Sales Order records, but without the ability to create BizDocs (invoices, order confirmations, etc..) – until one of the authorized approvers have approved the margin (which restores the ability to add BizDocs again). 
To populate your approver list, set approvers from the Administration | Workflow Automation | Approval Processes, then from “Select Module” drop down, select “Order Approval”. Adding approvers there will add their name with a check box in the “Authorized Approvers” list here. 
When you select the check box, these approvers will begin to receive approval requests within their &quot;Activity list&quot;." />
            </td>
        </tr>
        <tr>
            <td>
                Applies to items within the following item classification &nbsp;&nbsp;<asp:DropDownList ID="ddlItemClassification" runat="server" CssClass="signup" AutoPostBack="true" OnSelectedIndexChanged="ddlItemClassification_SelectedIndexChanged">
                    </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <br />
                <b>Trigger approval rule WHEN:</b>
                <br />
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:CheckBox ID="chkCost" runat="server" />
                User sets a unit price that’s less than
                <telerik:RadNumericTextBox ID="radTxtAbovePercent" runat="server" Width="50" NumberFormat-DecimalDigits="2"></telerik:RadNumericTextBox>% above cost (based on whatever cost basis set up within Administration | Global Settings | Order Mgt. (Default Cost)).
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:CheckBox ID="chkPrice" runat="server" />
                User sets a unit price that’s greater than
                <telerik:RadNumericTextBox ID="radTxtBelowPercent" runat="server" Width="50" ></telerik:RadNumericTextBox>% below
                                                        <asp:DropDownList ID="ddlBelow" runat="server">
                                                            <asp:ListItem Text="-- Select One --" Value="0"></asp:ListItem>
                                                            <asp:ListItem Text="List Price" Value="1"></asp:ListItem>
                                                            <asp:ListItem Text="Price Rule" Value="2"></asp:ListItem>
                                                            <asp:ListItem Text="Price Level" Value="3"></asp:ListItem>
                                                        </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:CheckBox ID="chkViolateApproval" Text="If price margin is violated while building a Sales Order, make it a Sales Opportunity instead." runat="server" />
                <asp:HiddenField ID="hdnUnitPriceApprover" runat="server" />
            </td>
        </tr>
    </table>
</asp:Content>
