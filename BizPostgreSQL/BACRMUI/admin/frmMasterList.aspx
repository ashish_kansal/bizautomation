<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmMasterList.aspx.vb"
    Inherits="BACRM.UserInterface.Admin.frmMasterList" MasterPageFile="~/common/GridMasterRegular.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <link rel="stylesheet" href="~/CSS/master.css" type="text/css" />
    <link href="../JavaScript/MultiSelect/bootstrap-multiselect.css" rel="stylesheet" />
    <script src="../JavaScript/MultiSelect/bootstrap-multiselect.js" type="text/javascript"></script>
    <title>Master List Administration</title>
    <style>
        .tab-content {
            border: 1px solid #1473b4;
            border-top: 0px;
        }

        .nav-tabs {
            border-top: 4px solid #1473b4;
            background: #1473b4;
            border-radius: 4px;
            border-left: 4px solid #1473b4;
        }

            .nav-tabs > li > a {
                color: #fff !important;
            }

            .nav-tabs > li.active > a {
                color: #1473b4 !important;
            }
            bindT
            .nav-tabs > li > a:hover {
                background-color: #3198de;
                color: #71b3e9;
                border: none !important;
            }


        .box-header.with-border {
            display: none !important;
        }

        .box-body {
            padding: 0px !important;
        }

        .boxForList .box-primary .box-header {
            background-color: #1473b4;
        }

        .boxForList .box-title a {
            color: #ffffff !important;
            font-size: 15px;
            display: block;
        }

        .boxForList .box-body {
            padding: 0px !important;
        }

        .text-delete-danger {
            color: #dd4b39;
        }

        .boxForList .box-title {
            display: block !important;
        }

        .boxForList .box-header {
            padding: 3px !important;
        }

        .boxForList .internalBoxHeader {
            background-color: #e2e2e2;
            padding-top: 6px;
        }

        .boxForList .box .box-group > .box {
            margin-bottom: 2px !important;
        }

         #DropDownListaccordion .multiselect-container {
            max-height: 50vh;
            overflow-y: scroll;
        }
    </style>
    <script type="text/javascript">

        function State() {

            window.open("../admin/frmAddState.aspx", '', 'toolbar=no,titlebar=no,left=200, top=200,width=600,height=400,scrollbars=yes,resizable=yes')
            return false;
        }
        function OpenDropdownRel() {

            window.open("../admin2/frmDropDownRelationshipList.aspx?I=1", '', 'toolbar=no,titlebar=no,left=200, top=200,width=700,height=400,scrollbars=yes,resizable=yes')
            return false;
        }
        function ShippingServiceAbbr() {
            var h = screen.height;
            var w = screen.width;

            window.open("../admin/frmAddShippingServiceAbbr.aspx", '', 'toolbar=no,titlebar=no,top=50,left=50,width=' + (w - 100) + ',height=' + (h - 200) + ',scrollbars=yes,resizable=yes')
            return false;
        }

        function Sort() {
            window.open("../admin/frmSortList.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&ID=" + document.getElementById("ddlMasterList").value, '', 'toolbar=no,titlebar=no,left=200, top=200,width=600,height=400,scrollbars=yes,resizable=yes')
            return false;
        }

        function AuthoritativeBizDoc() {

            window.open("../admin/frmAuthoritativeBizDocs.aspx", '', 'toolbar=no,titlebar=no,left=200, top=200,width=500,height=150,scrollbars=yes,resizable=yes')
            return false;
        }
        function OpenCurrency() {

            window.open("../admin2/frmCurrencyRates.aspx", '', 'toolbar=no,titlebar=no,left=200, top=100,width=500,height=500,scrollbars=yes,resizable=yes')
            return false;
        }
        function OpenBizDocListFilter() {
            window.open("../admin/frmBizDocs.aspx", '', 'toolbar=no,titlebar=no,left=200, top=100,width=1000,height=400,scrollbars=yes,resizable=yes')
            return false;
        }
        function GoBack() {
            window.location.href = "../admin/frmAdminSection.aspx";
            return false;
        }

        function goBack() {
            document.location = "adminlinks.aspx";
        }
        function DeleteRecord() {
            var str;
            str = 'Are you sure, you want to delete the selected record?'
            if (confirm(str)) {
                return true;
            }
            else {
                return false;
            }
        }
        function DeleteMessage() {
            alert("You Are not Authorized to Delete the Selected Record !");
            return false;
        }
        function EditMessage() {
            alert("You Are not Authorized to Edit the Selected Record !");
            return false;
        }
        function GetList() {
            document.getElementById("Button1").click()
        }
        function Save() {
            if (document.getElementById("txtItemName").value == "") {
                alert("Enter Item Name")
                document.getElementById("txtItemName").focus()
                return false;
            }
            else if (document.getElementById("hfDataType").value == "numeric") {
                var re = new RegExp("^[0-9]+$");
                if (!document.getElementById("txtItemName").value.match(re)) {
                    alert("Enter Only Numeric Value")
                    document.getElementById("txtItemName").focus()
                    return false;
                }
            }
            else if (document.getElementById("txtEnforceSubTotal").value != "" && !document.getElementById("chkEnforceSubTotal").checked) {
                alert("Please select Enforce minimum order sub-total amount checkbox. ")
                document.getElementById("chkEnforceSubTotal").focus()
                return false;
            }
            else if (document.getElementById("txtEnforceSubTotal").value == "" && document.getElementById("chkEnforceSubTotal").checked) {
                alert("Please enter Enforce minimum order sub-total amount. ")
                document.getElementById("txtEnforceSubTotal").focus()
                return false;
            }
            else if (document.getElementById("txtEnforceSubTotal").value != "") {
                var re = new RegExp("^[0-9]+$");
                if (!document.getElementById("txtEnforceSubTotal").value.match(re)) {
                    alert("Enter Only Numeric Value")
                    document.getElementById("txtEnforceSubTotal").focus()
                    return false;
                }
            }
            return true;
        }

        function EditGrid() {
            if (document.getElementById("txtEnforceSubTotal").value != "" && !document.getElementById("chkEnforceSubTotal").checked) {
                alert("Please select Enforce minimum order sub-total amount checkbox. ")
                document.getElementById("chkEnforceSubTotal").focus()
                return false;
            }
            else if (document.getElementById("txtEnforceSubTotal").value == "" && document.getElementById("chkEnforceSubTotal").checked) {
                alert("Please enter Enforce minimum order sub-total amount. ")
                document.getElementById("txtEnforceSubTotal").focus()
                return false;
            }
            else if (document.getElementById("txtEnforceSubTotal").value != "") {
                var re = new RegExp("^[0-9]+$");
                if (!document.getElementById("txtEnforceSubTotal").value.match(re)) {
                    alert("Enter Only Numeric Value")
                    document.getElementById("txtEnforceSubTotal").focus()
                    return false;
                }
            }
            return true;
        }

        function OpenHelp() {
            window.open('../Help/Admin-Master_List_Administration.html', '', 'toolbar=no,titlebar=no,top=200,left=200,width=550,height=350,scrollbars=yes,resizable=yes')
            return false
        }


        function CreateDropDownListRelationShip() {
            var DomainID = '<%= Session("DomainId")%>';
            var moduleID = ($("[id$=ddlRModule] option:selected").val() || 0);
            var PrimaryDropDown = ($("#ddlPrimaryDropDown option:selected").val() || 0);
            var SecondaryDropDown = ($("#ddlSecondaryDropDown option:selected").val() || 0);

            if (moduleID == 0) {
                alert("Select module");
                $("[id$=ddlRModule]").focus();
                return false;
            }
            if (PrimaryDropDown == 0) {
                alert("Select parent");
                $("[id$=ddlPrimaryDropDown]").focus();
                return false;
            }
            if (SecondaryDropDown == 0) {
                alert("Select child");
                $("[id$=ddlSecondaryDropDown]").focus();
                return false;
            }

            var dataParam = "{DomainID:'" + DomainID + "',PrimaryDropDown:'" + PrimaryDropDown + "',SecondaryDropDown:'" + SecondaryDropDown + "',ModuleID:'" + moduleID + "',IsTaskRelation:false}";

            if (PrimaryDropDown == SecondaryDropDown) {
                alert("Parent and child selection can not be same.");
                return false;
            }
            $("#UpdateProgress").css("display", "block");
            $.ajax({
                type: "POST",
                url: "../admin/frmMasterList.aspx/WebMethodCreateRelationShip",
                data: dataParam,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    
                    $("#UpdateProgress").css("display", "none");
                    var Jresponse = $.parseJSON(response.d);
                    if (parseFloat(Jresponse) == 0) {
                        alert("Selected relationship already exist!");
                        return false;
                    } else {
                        bindDropDownRelationShipSection();
                        return false;
                    }
                },
                failure: function (response) {
                    $("#UpdateProgress").css("display", "none");
                    //Console.Log('Sorry!.,Getting records failed!.,');
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    $("#UpdateProgress").css("display", "none");
                    var errMessage = errorThrown + '., Please contact the BizAutomation support.';
                    alert(errMessage.toString());
                }, complete: function () {
                }
            });
        }
        function bindDropDownRelationShipSection(isCallFromSecondaryDropDown) {
            
            if (typeof isCallFromSecondaryDropDown === "undefined" || isCallFromSecondaryDropDown === null) {
                isCallFromSecondaryDropDown = false;
            }
            if (!isCallFromSecondaryDropDown) {
                $("#ddlSecondaryDropDown option[value='0']").prop('selected', true);
            }
            $("#ddlSecondaryDropDown option").prop('disabled', false);
            var DomainID = '<%= Session("DomainId")%>';
            var moduleID = $("[id$=ddlRModule] option:selected").val();
            var PrimaryDropDown = $("#ddlPrimaryDropDown option:selected").val();
            var SecondaryDropDown = $("#ddlSecondaryDropDown option:selected").val();

            if ((PrimaryDropDown || 0) > 0) {
                $("#ddlSecondaryDropDown option[value='" + PrimaryDropDown + "']").prop('disabled', true);
            }

            var dataParam = "{DomainID:'" + DomainID + "',ModuleID:'" + (moduleID || 0) + "',PrimaryDropDown:'" + (PrimaryDropDown || 0) + "',SecondaryDropDown:'" + (SecondaryDropDown || 0) + "',IsTaskRelation:false}";
            $("#UpdateProgress").css("display", "block");
            $.ajax({
                type: "POST",
                url: "../admin/frmMasterList.aspx/WebMethodLoadRelationShip",
                data: dataParam,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    
                    $("#UpdateProgress").css("display", "none");
                    
                    var Jresponse = $.parseJSON(response.d);
                    var itemAppend = '';
                    $("#DropDownListaccordion").html("");
                    $.each(Jresponse, function (index, value) {
                        itemAppend = "";
                        var data = value.RelationshipName;
                        var split = data.split("-");
                        itemAppend = itemAppend + "<div class=\"panel box box-primary\">";
                        itemAppend = itemAppend + "<div class=\"box-header\">";

                        itemAppend = itemAppend + "<div class=\"col-md-5\">";
                        itemAppend = itemAppend + "<h4 class=\"box-title\">";
                        itemAppend = itemAppend + " <a data-toggle=\"collapse\" data-parent=\"#DropDownListaccordion\" onclick=\"DropdownRelationShipWithHeader(" + value.numFieldRelID + ")\" href=\"#collapse" + value.numFieldRelID + "\" aria-expanded=\"false\" class=\"collapsed\">" + split[0] + "(Parent)</a>";
                        itemAppend = itemAppend + "</h4>";
                        itemAppend = itemAppend + "</div>";

                        itemAppend = itemAppend + "<div class=\"col-md-5\">";
                        itemAppend = itemAppend + "<h4 class=\"box-title\">";
                        itemAppend = itemAppend + " <a data-toggle=\"collapse\" data-parent=\"#DropDownListaccordion\" onclick=\"DropdownRelationShipWithHeader(" + value.numFieldRelID + ")\" href=\"#collapse" + value.numFieldRelID + "\" aria-expanded=\"false\" class=\"collapsed\">" + split[1] + "(Child)</a>";
                        itemAppend = itemAppend + "</h4>";
                        itemAppend = itemAppend + "</div>";


                        itemAppend = itemAppend + "<div class=\"col-md-2\">";
                        itemAppend = itemAppend + " <a href=\"javascript:void(0)\" onclick=\"DeleteDropDownListRelationShip(" + value.numFieldRelID + ")\" class=\"btn btn-danger btn-xs\"><i class=\"fa fa-trash text-delete-danger1\"></i></a>";
                        itemAppend = itemAppend + "</div>";

                        itemAppend = itemAppend + "</div>";

                        itemAppend = itemAppend + "<div id=\"collapse" + value.numFieldRelID + "\" class=\"panel-collapse collapse\" aria-expanded=\"false\" style=\"height: 0px;\">";

                        var boxBodyHeader = "";
                        boxBodyHeader = boxBodyHeader + "<div class=\"col-md-12 internalBoxHeader\">";
                        boxBodyHeader = boxBodyHeader + "<div class=\"form-inline\">";
                        boxBodyHeader = boxBodyHeader + "When <i><b>" + split[0] + "</b></i> selected is <select style=\"width:160px\" onchange=\"getItemByPrimaryDropDown(" + value.numFieldRelID + ")\" id=\"ddlPrimaryDropDown" + value.numFieldRelID + "\"><option value=\"0\">Select</option></select>&nbsp; Populate <i><b>" + split[1] + "</b></i> with <select class=\"option-droup-multiSelection-Group\" style=\"width:160px\" id=\"ddlSecoundaryDropDown" + value.numFieldRelID + "\" multiple=\"multiple\"><option value=\"0\">Select</option></select>&nbsp;&nbsp;";
                        boxBodyHeader = boxBodyHeader + "<a class=\"btn btn-sm btn-warning\" href=\"javascript:void(0)\" onclick=\"AddMoreItemtoRelationShip(" + value.numFieldRelID + ")\">Add</a>";
                        boxBodyHeader = boxBodyHeader + "</div>";
                        boxBodyHeader = boxBodyHeader + "</div>";

                        itemAppend = itemAppend + "<div class=\"box-body\">" + boxBodyHeader + "<div class=\"clearfix\"></div><table id=\"divItemsByDropdownSelected" + value.numFieldRelID + "\" class=\"table table-striped table-bordered\"></table></div>";
                        itemAppend = itemAppend + "</div>";
                        itemAppend = itemAppend + "</div>";

                        itemAppend = itemAppend + "</div>";
                        $("#DropDownListaccordion").append(itemAppend);
                    });
                },
                failure: function (response) {
                    //Console.Log('Sorry!.,Getting records failed!.,');
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    var errMessage = errorThrown + '., Please contact the BizAutomation support.';
                    $("#lblErrMessage").text(errMessage.toString());
                }, complete: function () {
                }
            });
        }

        function DropdownRelationShipWithHeader(id) {
            var DomainID = '<%= Session("DomainId")%>';
            var dataParam = "{DomainID:'" + DomainID + "',FieldRelID:'" + id + "',IsTaskRelation:false}";
            $("#divItemsByDropdownSelected" + id + "").html("");
            $("#UpdateProgress").css("display", "block");
            $.ajax({
                type: "POST",
                url: "../admin/frmMasterList.aspx/WebMethodLoadRelationShipDetails",
                data: dataParam,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    
                    $("#UpdateProgress").css("display", "none");
                    
                    var Jresponse = $.parseJSON(response.d);
                    var itemAppend = '';
                    var value = Jresponse[0];
                    BindDropDownsById(value.numModuleID, value.numPrimaryListID, "ddlPrimaryDropDown" + id);
                    BindDropDownsById(value.numModuleID, value.numSecondaryListID, "ddlSecoundaryDropDown" + id);
                },
                failure: function (response) {
                    //Console.Log('Sorry!.,Getting records failed!.,');
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    var errMessage = errorThrown + '., Please contact the BizAutomation support.';
                    $("#lblErrMessage").text(errMessage.toString());
                }, complete: function () {
                }
            });
        }
        function BindDropDownsById(moduleID, Id, DropDownId) {
            var DomainID = '<%= Session("DomainId")%>';

            if (moduleID == undefined) {
                moduleID = 0;
            }
            var dataParam = "{DomainID:'" + DomainID + "',ModuleID:" + moduleID + ",ListId:'" + Id + "'}";
            $("#" + DropDownId + "").empty();

            if (!$("#" + DropDownId).hasClass("option-droup-multiSelection-Group")) {
                $("#" + DropDownId + "").append("<option value=\"0\">Select</option>");
            }

            if (moduleID == 14 && DropDownId.startsWith("ddlSecoundaryDropDow")) {
                $("#" + DropDownId + "").append("<option value=\"-1\">Do not display</option>")
            }

            $("#" + DropDownId + "").attr("relationId", Id);
            $("#UpdateProgress").css("display", "block");
            $.ajax({
                type: "POST",
                url: "../admin/frmMasterList.aspx/WebMethodLoadRelationShipDetailsDropDown",
                data: dataParam,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    
                    $("#UpdateProgress").css("display", "none");
                    
                    var Jresponse = $.parseJSON(response.d);
                    var itemAppend = '';

                    if (moduleID == 14) {
                        $.each(Jresponse, function (index, value) {
                            itemAppend = itemAppend + "<option value=\"" + value.numItemCode + "\">" + value.vcItemName + "</option>";
                        });
                    } else {
                        $.each(Jresponse, function (index, value) {
                            itemAppend = itemAppend + "<option value=\"" + value.numListItemID + "\">" + value.vcData + "</option>";
                        });
                    }
                    $("#" + DropDownId + "").append(itemAppend);

                    if (DropDownId.startsWith("ddlPrimaryDropDown")) {
                        var fieldRelID = DropDownId.replace("ddlPrimaryDropDown", "");
                        getItemByPrimaryDropDown(fieldRelID);
                    }


                    if ($("#" + DropDownId).hasClass("option-droup-multiSelection-Group")) {
                        $("#" + DropDownId).multiselect({
                            includeSelectAllOption: true
                        });
                    }
                },
                failure: function (response) {
                    //Console.Log('Sorry!.,Getting records failed!.,');
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    var errMessage = errorThrown + '., Please contact the BizAutomation support.';
                    $("#lblErrMessage").text(errMessage.toString());
                }, complete: function () {
                }
            });
        }
        function getItemByPrimaryDropDown(id) {
            var PrimaryDropdownItems = $("#ddlPrimaryDropDown" + id + "").val();
            var PrimaryDropdownItemsRelId = $("#ddlPrimaryDropDown" + id + "").attr("relationId");
            var DomainID = '<%= Session("DomainId")%>';
            var dataParam = "{DomainID:'" + DomainID + "',FieldRelID:'" + id + "',PrimaryDropdownItems:'" + PrimaryDropdownItems + "',IsTaskRelation:false}";
            $("#divItemsByDropdownSelected" + id + "").html("");
            $("#UpdateProgress").css("display", "block");
            $.ajax({
                type: "POST",
                url: "../admin/frmMasterList.aspx/WebMethodLoadRelationShipDetailsByPrimaryDropDown",
                data: dataParam,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    
                    $("#UpdateProgress").css("display", "none");
                    
                    var Jresponse = $.parseJSON(response.d);
                    var itemAppend = '';
                    var selectedValues = "";
                    $.each(Jresponse, function (index, value) {
                        selectedValues = selectedValues + (selectedValues == "" ? "" : ",") + value.numSecondaryListItemID.toString();
                        itemAppend = itemAppend + "<tr><td class=\"col-md-5\">" + value.PrimaryListItem + "</td><td class=\"col-md-5\">" + value.SecondaryListItem + "</td><td class=\"col-md-2\"><a href=\"javascript:void(0)\" onclick=\"removeListItem(" + value.numFieldRelDTLID + "," + id + ")\" class=\"btn btn-danger btn-xs\"><i class=\"fa fa-trash text-delete-danger1\"></i></a></td></tr>";
                    });
                    $("#divItemsByDropdownSelected" + id + "").append(itemAppend);

                    if (parseInt(PrimaryDropdownItems) > 0) {
                        if ($("#ddlSecoundaryDropDown" + id + "").hasClass("option-droup-multiSelection-Group")) {
                            var selectedValuesArray = selectedValues.split(",");
                            $("#ddlSecoundaryDropDown" + id + "").val(selectedValuesArray);
                            $("#ddlSecoundaryDropDown" + id + "").multiselect("refresh");
                        }
                    }
                },
                failure: function (response) {
                    //Console.Log('Sorry!.,Getting records failed!.,');
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    var errMessage = errorThrown + '., Please contact the BizAutomation support.';
                    $("#lblErrMessage").text(errMessage.toString());
                }, complete: function () {
                }
            });
        }
        function AddMoreItemtoRelationShip(numFieldRelID) {
            
            var DomainID = '<%= Session("DomainId")%>';
            var PrimaryDropDown = $("#ddlPrimaryDropDown" + numFieldRelID + " option:selected").val();

            if ($("#ddlSecoundaryDropDown" + numFieldRelID).hasClass("option-droup-multiSelection-Group")) {
                if ($("#ddlSecoundaryDropDown" + numFieldRelID).val().length > 0 && PrimaryDropDown != "0") {
                    $("#ddlSecoundaryDropDown" + numFieldRelID).val().forEach(function (item, index) {
                        var dataParam = "{DomainID:'" + DomainID + "',FieldRelID:'" + numFieldRelID + "',PrimaryListItemID:'" + PrimaryDropDown + "',SecondaryListItemID:'" + item + "'}";
                        $("#UpdateProgress").css("display", "block");
                        $.ajax({
                            type: "POST",
                            url: "../admin/frmMasterList.aspx/WebMethodAddRelationItems",
                            data: dataParam,
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            success: function (response) {
                                
                                $("#UpdateProgress").css("display", "none");
                                var Jresponse = $.parseJSON(response.d);
                                if (parseFloat(Jresponse) == 0) {
                                    alert("Duplicate Record !");
                                    return false;
                                } else {
                                    getItemByPrimaryDropDown(numFieldRelID);
                                    return false;
                                }
                            },
                            failure: function (response) {
                                $("#UpdateProgress").css("display", "none");
                                //Console.Log('Sorry!.,Getting records failed!.,');
                                return false;
                            },
                            error: function (XMLHttpRequest, textStatus, errorThrown) {
                                $("#UpdateProgress").css("display", "none");
                                var errMessage = errorThrown + '., Please contact the BizAutomation support.';
                                alert(errMessage.toString());
                                return false;
                            }, complete: function () {
                            }
                        });

                    });
                } else {
                    alert("Please select atleast one option");
                }
            } else {
                var SecondaryDropDown = $("#ddlSecoundaryDropDown" + numFieldRelID + " option:selected").val();

                if (SecondaryDropDown != "0" && PrimaryDropDown != "0") {
                    var dataParam = "{DomainID:'" + DomainID + "',FieldRelID:'" + numFieldRelID + "',PrimaryListItemID:'" + PrimaryDropDown + "',SecondaryListItemID:'" + SecondaryDropDown + "'}";
                    $("#UpdateProgress").css("display", "block");
                    $.ajax({
                        type: "POST",
                        url: "../admin/frmMasterList.aspx/WebMethodAddRelationItems",
                        data: dataParam,
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (response) {
                            
                            $("#UpdateProgress").css("display", "none");
                            var Jresponse = $.parseJSON(response.d);
                            if (parseFloat(Jresponse) == 0) {
                                alert("Duplicate Record !");
                                return false;
                            } else {
                                getItemByPrimaryDropDown(numFieldRelID);
                                return false;
                            }
                        },
                        failure: function (response) {
                            $("#UpdateProgress").css("display", "none");
                            //Console.Log('Sorry!.,Getting records failed!.,');
                            return false;
                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            $("#UpdateProgress").css("display", "none");
                            var errMessage = errorThrown + '., Please contact the BizAutomation support.';
                            alert(errMessage.toString());
                            return false;
                        }, complete: function () {
                        }
                    });
                } else {
                    alert("Please select atleast one option");
                }
            }
        }
        function removeListItem(FieldRelDTLID, FieldRelID) {
            var DomainID = '<%= Session("DomainId")%>';
            var dataParam = "{DomainID:'" + DomainID + "',FieldRelDTLID:'" + FieldRelDTLID + "'}";
            $("#UpdateProgress").css("display", "block");
            $.ajax({
                type: "POST",
                url: "../admin/frmMasterList.aspx/WebMethodDeleteRelationItems",
                data: dataParam,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    
                    $("#UpdateProgress").css("display", "none");
                    var Jresponse = $.parseJSON(response.d);
                    if (parseFloat(Jresponse) == 0) {
                        alert("Duplicate Record !");
                        return false;
                    } else {
                        getItemByPrimaryDropDown(FieldRelID);
                        return false;
                    }
                },
                failure: function (response) {
                    $("#UpdateProgress").css("display", "none");
                    //Console.Log('Sorry!.,Getting records failed!.,');
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    $("#UpdateProgress").css("display", "none");
                    var errMessage = errorThrown + '., Please contact the BizAutomation support.';
                    alert(errMessage.toString());
                }, complete: function () {
                }
            });
        }
        function DeleteDropDownListRelationShip(FieldRelID) {
            var DomainID = '<%= Session("DomainId")%>';
            var dataParam = "{DomainID:'" + DomainID + "',FieldRelID:'" + FieldRelID + "'}";
            $("#UpdateProgress").css("display", "block");
            $.ajax({
                type: "POST",
                url: "../admin/frmMasterList.aspx/WebMethodDeleteRelation",
                data: dataParam,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    
                    $("#UpdateProgress").css("display", "none");
                    var Jresponse = $.parseJSON(response.d);
                    if (parseFloat(Jresponse) == 1) {
                        bindDropDownRelationShipSection();
                        return false;
                    }
                },
                failure: function (response) {
                    $("#UpdateProgress").css("display", "none");
                    //Console.Log('Sorry!.,Getting records failed!.,');
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    $("#UpdateProgress").css("display", "none");
                    var errMessage = errorThrown + '., Please contact the BizAutomation support.';
                    alert(errMessage.toString());
                }, complete: function () {
                }
            });
        }
        function BindProcessList() {
            var DomainID = '<%= Session("DomainId")%>';
            var dataParam = "{DomainID:'" + DomainID + "',Mode:'-1'}";
            $("#ddlProcessList").empty();
            $("#UpdateProgress").css("display", "block");
            $.ajax({
                type: "POST",
                url: "../admin/frmAdminBusinessProcess.aspx/WebMethodGetProcessList",
                data: dataParam,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {

                    $("#UpdateProgress").css("display", "none");
                    var Jresponse = $.parseJSON(response.d);
                    var itemAppend = '';
                    itemAppend = "<option value=\"0\">--Select One--</option>";
                    $("#ddlProcessList").append(itemAppend);
                    itemAppend = '';
                    $.each(Jresponse, function (index, value) {
                        itemAppend = itemAppend + "<option value=" + value.Slp_Id + ">" + value.Slp_Name + "</option>";
                    });
                    $("#ddlProcessList").append(itemAppend);
                },
                failure: function (response) {
                    $("#UpdateProgress").css("display", "none");
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    $("#UpdateProgress").css("display", "none");
                }, complete: function () {
                }
            });
        }
        function getStageList() {
            var DomainID = '<%= Session("DomainId")%>';
            var dataParam = "{DomainID:'" + DomainID + "',ProcessId:'" + $("#ddlProcessList option:selected").val() + "',OppID:'0',ProjectID:'0'}";
            $("#ddlStages").empty();
            $("#UpdateProgress").css("display", "block");
            stageDetailsList = [];
            $.ajax({
                type: "POST",
                url: "../admin/frmAdminBusinessProcess.aspx/WebMethodGetSavedStageDetails",
                data: dataParam,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    var itemAppend = '';
                    var Jresponse = $.parseJSON(response.d);
                    itemAppend = "<option value=\"0\">--Select One--</option>";
                    $("#ddlStages").append(itemAppend);
                    itemAppend = '';
                    $.each(Jresponse, function (index, value) {
                        if (value.bitRunningDynamicMode == true) {
                            itemAppend = itemAppend + "<option value=" + value.numStageDetailsId + ">" + value.vcStageName + "</option>";
                        }
                    });
                    $("#ddlStages").append(itemAppend);
                    $("#UpdateProgress").css("display", "none");
                },
                failure: function (response) {
                    $("#UpdateProgress").css("display", "none");
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    $("#UpdateProgress").css("display", "none");
                }, complete: function () {
                }
            });
        }
        function getTaskList() {
            var StageDetailsId = $("#ddlStages option:selected").val();
            var DomainID = '<%= Session("DomainId")%>';
            var dataParam = "{DomainID:'" + DomainID + "',StageDetailsId:'" + StageDetailsId + "',OppId:0,ProjectId:0,bitAlphabetical:false}";
            $("#UpdateProgress").css("display", "block");
            $("#ddlStagesTask").empty();
            $.ajax({
                type: "POST",
                url: "../admin/frmAdminBusinessProcess.aspx/WebMethodGetTaskList",
                data: dataParam,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    $("#UpdateProgress").css("display", "none");
                    var Jresponse = $.parseJSON(response.d);
                    var itemAppend = '';
                    itemAppend = "<option value=\"0\">--Select One--</option>";
                    $("#ddlStagesTask").append(itemAppend);
                    itemAppend = '';
                    $.each(Jresponse, function (index, value) {
                        itemAppend = itemAppend + "<option value=" + value.numTaskId + ">" + value.vcTaskName + "</option>";
                    });
                    $("#ddlStagesTask").append(itemAppend);
                },
                failure: function (response) {
                    $("#UpdateProgress").css("display", "none");
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    $("#UpdateProgress").css("display", "none");
                }, complete: function () {
                }
            });
        }

        //Task Accordian Code
        function CreateTaskListRelationShip() {
            var DomainID = '<%= Session("DomainId")%>';
            var PrimaryDropDown = $("#ddlStages option:selected").val();
            var SecondaryDropDown = $("#ddlStagesTask option:selected").val();
            var dataParam = "{DomainID:'" + DomainID + "',PrimaryDropDown:'" + PrimaryDropDown + "',SecondaryDropDown:'" + SecondaryDropDown + "',ModuleID:'0',IsTaskRelation:true}";
            $("#UpdateProgress").css("display", "block");
            $.ajax({
                type: "POST",
                url: "../admin/frmMasterList.aspx/WebMethodCreateRelationShip",
                data: dataParam,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    
                    $("#UpdateProgress").css("display", "none");
                    var Jresponse = $.parseJSON(response.d);
                    if (parseFloat(Jresponse) == 0) {
                        alert("Selected relationship already exist!");
                        return false;
                    } else {
                        bindTaskRelationShipSection();
                        return false;
                    }
                },
                failure: function (response) {
                    $("#UpdateProgress").css("display", "none");
                    //Console.Log('Sorry!.,Getting records failed!.,');
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    $("#UpdateProgress").css("display", "none");
                    var errMessage = errorThrown + '., Please contact the BizAutomation support.';
                    alert(errMessage.toString());
                }, complete: function () {
                }
            });
        }
        function TaskRelationShipWithHeader(id, FieldRelID) {
            var DomainID = '<%= Session("DomainId")%>';
            var dataParam = "{DomainID:'" + DomainID + "',StageDetailsId:'" + id + "',OppId:0,ProjectId:0,bitAlphabetical:false}";
            $("#ddlPrimaryDropDown" + FieldRelID).empty();
            $("#ddlSecoundaryDropDown" + FieldRelID).empty();
            $("#divItemsByTaskSelected" + id + "").html("");
            $("#UpdateProgress").css("display", "block");
            $.ajax({
                type: "POST",
                url: "../admin/frmAdminBusinessProcess.aspx/WebMethodGetTaskList",
                data: dataParam,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    
                    $("#UpdateProgress").css("display", "none");
                    
                    var Jresponse = $.parseJSON(response.d);
                    var itemAppend = '';
                    itemAppend = itemAppend + "<option value='0'>--Select--</option>";
                    $("#ddlPrimaryDropDown" + FieldRelID).append(itemAppend);
                    $("#ddlSecoundaryDropDown" + FieldRelID).append(itemAppend);
                    itemAppend = '';
                    $.each(Jresponse, function (index, value) {
                        itemAppend = itemAppend + "<option value='" + value.numTaskId + "'>" + value.vcTaskName + "</option>"
                    });
                    $("#ddlPrimaryDropDown" + FieldRelID).append(itemAppend);
                    $("#ddlSecoundaryDropDown" + FieldRelID).append(itemAppend);
                },
                failure: function (response) {
                    //Console.Log('Sorry!.,Getting records failed!.,');
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    var errMessage = errorThrown + '., Please contact the BizAutomation support.';
                    $("#lblErrMessage").text(errMessage.toString());
                }, complete: function () {
                }
            });
        }
        function bindTaskRelationShipSection() {
            var DomainID = '<%= Session("DomainId")%>';
            var dataParam = "{DomainID:" + DomainID + ",ModuleID:0,PrimaryDropDown:0,SecondaryDropDown:0,IsTaskRelation:true}";
            $("#UpdateProgress").css("display", "block");
            $.ajax({
                type: "POST",
                url: "../admin/frmMasterList.aspx/WebMethodLoadRelationShip",
                data: dataParam,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    
                    $("#UpdateProgress").css("display", "none");
                    
                    var Jresponse = $.parseJSON(response.d);
                    var itemAppend = '';
                    $("#TaskListaccordion").html("");
                    $.each(Jresponse, function (index, value) {
                        var data = value.RelationshipName;
                        if (data != null) {
                            var split = data.split("-");

                            itemAppend = "";
                            var data = value.RelationshipName;
                            itemAppend = itemAppend + "<div class=\"panel box box-primary\">";
                            itemAppend = itemAppend + "<div class=\"box-header\">";

                            itemAppend = itemAppend + "<div class=\"col-md-5\">";
                            itemAppend = itemAppend + "<h4 class=\"box-title\">";
                            itemAppend = itemAppend + " <a data-toggle=\"collapse\" data-parent=\"#TaskListaccordion\" onclick=\"TaskRelationShipWithHeader(" + value.numPrimaryListID + "," + value.numFieldRelID + ")\" href=\"#collapse" + value.numFieldRelID + "\" aria-expanded=\"false\" class=\"collapsed\">Relationship (Parent)</a>";
                            itemAppend = itemAppend + "</h4>";
                            itemAppend = itemAppend + "</div>";

                            itemAppend = itemAppend + "<div class=\"col-md-5\">";
                            itemAppend = itemAppend + "<h4 class=\"box-title\">";
                            itemAppend = itemAppend + " <a data-toggle=\"collapse\" data-parent=\"#TaskListaccordion\" onclick=\"TaskRelationShipWithHeader(" + value.numPrimaryListID + "," + value.numFieldRelID + ")\" href=\"#collapse" + value.numFieldRelID + "\" aria-expanded=\"false\" class=\"collapsed\">Profile (Child)</a>";
                            itemAppend = itemAppend + "</h4>";
                            itemAppend = itemAppend + "</div>";


                            itemAppend = itemAppend + "<div class=\"col-md-2\">";
                            itemAppend = itemAppend + " <a href=\"javascript:void(0)\" onclick=\"DeleteTaskListRelationShip(" + value.numFieldRelID + ")\" class=\"btn btn-danger btn-xs\"><i class=\"fa fa-trash text-delete-danger1\"></i></a>";
                            itemAppend = itemAppend + "</div>";

                            itemAppend = itemAppend + "</div>";

                            itemAppend = itemAppend + "<div id=\"collapse" + value.numFieldRelID + "\" class=\"panel-collapse collapse\" aria-expanded=\"false\" style=\"height: 0px;\">";

                            var boxBodyHeader = "";
                            boxBodyHeader = boxBodyHeader + "<div class=\"col-md-12 internalBoxHeader\">";
                            boxBodyHeader = boxBodyHeader + "<div class=\"form-inline\">";
                            boxBodyHeader = boxBodyHeader + "When <i><b>" + split[0] + " Task</b></i> selected is <select style=\"width:160px\" onchange=\"getTaskByPrimaryDropDown(" + value.numFieldRelID + ")\" id=\"ddlPrimaryDropDown" + value.numFieldRelID + "\"><option value=\"0\">Select</option></select>&nbsp; Populate <i><b>" + split[0] + " Task</b></i> with <select style=\"width:160px\" id=\"ddlSecoundaryDropDown" + value.numFieldRelID + "\"><option value=\"0\">Select</option></select>&nbsp;&nbsp;";
                            boxBodyHeader = boxBodyHeader + "<a class=\"btn btn-sm btn-warning\" href=\"javascript:void(0)\" onclick=\"AddMoreItemtoTaskRelationShip(" + value.numFieldRelID + ")\">Add</a>";
                            boxBodyHeader = boxBodyHeader + "</div>";
                            boxBodyHeader = boxBodyHeader + "</div>";

                            itemAppend = itemAppend + "<div class=\"box-body\">" + boxBodyHeader + "<div class=\"clearfix\"></div><table id=\"divItemsByTaskSelected" + value.numFieldRelID + "\" class=\"table table-striped table-bordered\"></table></div>";
                            itemAppend = itemAppend + "</div>";
                            itemAppend = itemAppend + "</div>";

                            itemAppend = itemAppend + "</div>";
                            $("#TaskListaccordion").append(itemAppend);
                        }
                    });
                },
                failure: function (response) {
                    //Console.Log('Sorry!.,Getting records failed!.,');
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    var errMessage = errorThrown + '., Please contact the BizAutomation support.';
                    $("#lblErrMessage").text(errMessage.toString());
                }, complete: function () {
                }
            });
        }
        function getTaskByPrimaryDropDown(id) {
            var PrimaryDropdownItems = $("#ddlPrimaryDropDown" + id + "").val();
            var DomainID = '<%= Session("DomainId")%>';
            var dataParam = "{DomainID:'" + DomainID + "',FieldRelID:'" + id + "',PrimaryDropdownItems:'" + PrimaryDropdownItems + "',IsTaskRelation:true}";
            $("#divItemsByTaskSelected" + id + "").html("");
            if (PrimaryDropdownItems > 0) {
                $("#UpdateProgress").css("display", "block");
                $.ajax({
                    type: "POST",
                    url: "../admin/frmMasterList.aspx/WebMethodLoadRelationShipDetailsByPrimaryDropDown",
                    data: dataParam,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {
                        
                        $("#UpdateProgress").css("display", "none");
                        
                        var Jresponse = $.parseJSON(response.d);
                        var itemAppend = '';
                        $.each(Jresponse, function (index, value) {
                            itemAppend = itemAppend + "<tr><td class=\"col-md-10\">" + value.SecondaryListItem + "</td><td class=\"col-md-2\"><a href=\"javascript:void(0)\" onclick=\"removeTaskListItem(" + value.numFieldRelDTLID + "," + id + ")\" class=\"btn btn-danger btn-xs\"><i class=\"fa fa-trash text-delete-danger1\"></i></a></td></tr>";
                        });
                        $("#divItemsByTaskSelected" + id + "").append(itemAppend);
                    },
                    failure: function (response) {
                        //Console.Log('Sorry!.,Getting records failed!.,');
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        var errMessage = errorThrown + '., Please contact the BizAutomation support.';
                        $("#lblErrMessage").text(errMessage.toString());
                    }, complete: function () {
                    }
                });
            }
        }
        function AddMoreItemtoTaskRelationShip(numFieldRelID) {
            
            var DomainID = '<%= Session("DomainId")%>';
            var PrimaryDropDown = $("#ddlPrimaryDropDown" + numFieldRelID + " option:selected").val();
            var SecondaryDropDown = $("#ddlSecoundaryDropDown" + numFieldRelID + " option:selected").val();
            if (SecondaryDropDown == PrimaryDropDown) {
                alert("Both Relationship cant be same");
                return false;
            }
            if ((SecondaryDropDown != "0" || PrimaryDropDown != "0")) {
                var dataParam = "{DomainID:'" + DomainID + "',FieldRelID:'" + numFieldRelID + "',PrimaryListItemID:'" + PrimaryDropDown + "',SecondaryListItemID:'" + SecondaryDropDown + "'}";
                $("#UpdateProgress").css("display", "block");
                $.ajax({
                    type: "POST",
                    url: "../admin/frmMasterList.aspx/WebMethodAddRelationItems",
                    data: dataParam,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {
                        
                        $("#UpdateProgress").css("display", "none");
                        var Jresponse = $.parseJSON(response.d);
                        if (parseFloat(Jresponse) == 0) {
                            alert("Duplicate Record !");
                            return false;
                        } else {
                            getTaskByPrimaryDropDown(numFieldRelID);
                            return false;
                        }
                    },
                    failure: function (response) {
                        $("#UpdateProgress").css("display", "none");
                        //Console.Log('Sorry!.,Getting records failed!.,');
                        return false;
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        $("#UpdateProgress").css("display", "none");
                        var errMessage = errorThrown + '., Please contact the BizAutomation support.';
                        alert(errMessage.toString());
                        return false;
                    }, complete: function () {
                    }
                });
            } else {
                alert("Please select atleast one option");
            }
        }
        function removeTaskListItem(FieldRelDTLID, FieldRelID) {
            var DomainID = '<%= Session("DomainId")%>';
            var dataParam = "{DomainID:'" + DomainID + "',FieldRelDTLID:'" + FieldRelDTLID + "'}";
            $("#UpdateProgress").css("display", "block");
            $.ajax({
                type: "POST",
                url: "../admin/frmMasterList.aspx/WebMethodDeleteRelationItems",
                data: dataParam,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    
                    $("#UpdateProgress").css("display", "none");
                    var Jresponse = $.parseJSON(response.d);
                    if (parseFloat(Jresponse) == 0) {
                        alert("Duplicate Record !");
                        return false;
                    } else {
                        getTaskByPrimaryDropDown(FieldRelID);
                        return false;
                    }
                },
                failure: function (response) {
                    $("#UpdateProgress").css("display", "none");
                    //Console.Log('Sorry!.,Getting records failed!.,');
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    $("#UpdateProgress").css("display", "none");
                    var errMessage = errorThrown + '., Please contact the BizAutomation support.';
                    alert(errMessage.toString());
                }, complete: function () {
                }
            });
        }
        function DeleteTaskListRelationShip(FieldRelID) {
            var DomainID = '<%= Session("DomainId")%>';
            var dataParam = "{DomainID:'" + DomainID + "',FieldRelID:'" + FieldRelID + "'}";
            $("#UpdateProgress").css("display", "block");
            $.ajax({
                type: "POST",
                url: "../admin/frmMasterList.aspx/WebMethodDeleteRelation",
                data: dataParam,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    
                    $("#UpdateProgress").css("display", "none");
                    var Jresponse = $.parseJSON(response.d);
                    if (parseFloat(Jresponse) == 1) {
                        bindTaskRelationShipSection();
                        return false;
                    }
                },
                failure: function (response) {
                    $("#UpdateProgress").css("display", "none");
                    //Console.Log('Sorry!.,Getting records failed!.,');
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    $("#UpdateProgress").css("display", "none");
                    var errMessage = errorThrown + '., Please contact the BizAutomation support.';
                    alert(errMessage.toString());
                }, complete: function () {
                }
            });
        }
        $(document).ready(function () {
            BindProcessList();
            $('#txtMinOrderAmount').keyup(function () {
                var numbers = $(this).val();
                $(this).val(numbers.replace(/\D/, ''));
            });

        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server"
    ClientIDMode="Static">

    <div class="row padbottom10">
        <div class="col-xs-12">
            <div class="pull-left">
                <div class="form-inline">
                    <div class="form-group">
                        <label>Module:</label>
                        <asp:DropDownList ID="ddlModule" runat="server" CssClass="form-control" AutoPostBack="true">
                        </asp:DropDownList>
                    </div>
                    <div class="form-group">
                        <label>List:</label>
                        <asp:DropDownList ID="ddlMasterList" runat="server" CssClass="form-control" AutoPostBack="true">
                        </asp:DropDownList>
                    </div>
                    <div class="form-group" id="tdBizDocType" runat="server" style="display: none">
                        <label></label>
                        <asp:DropDownList ID="ddlListType" runat="server" CssClass="form-control" AutoPostBack="true">
                        </asp:DropDownList>
                    </div>
                    <div class="form-group" id="tdOrderStatusType" runat="server" style="display: none">
                        <label></label>
                        <asp:DropDownList ID="ddlOrderStatusType" runat="server" CssClass="form-control" AutoPostBack="true">
                            <asp:ListItem Text="--All--" Value="0"></asp:ListItem>
                            <asp:ListItem Text="Sales" Value="1"></asp:ListItem>
                            <asp:ListItem Text="Purchase" Value="2"></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div class="form-group" id="tdOppOrOrder" runat="server" style="display: none">
                        <label></label>
                        <asp:DropDownList ID="ddlOppOrOrder" runat="server" CssClass="form-control" AutoPostBack="true">
                            <asp:ListItem Text="--All--" Value="0"></asp:ListItem>
                            <asp:ListItem Text="Opportunity" Value="1"></asp:ListItem>
                            <asp:ListItem Text="Order" Value="2"></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <asp:LinkButton ID="btnSort" runat="server" CssClass="btn btn-primary"><i class="fa fa-sort"></i>&nbsp;&nbsp;Sort List</asp:LinkButton><asp:HiddenField ID="hfDataType" runat="server" />
                    <div class="form-group" id="divEnforceSubTotal" runat="server" style="padding-left: 20px; display: none;">
                        <asp:Label ID="lblToolTipSubTotal" Text="[?]" CssClass="tip" runat="server" ToolTip="When this check box is selected, BizAutomation will require that sub-total amounts for sales orders related to customers with this profile, be equal to or greater than the minimum amount entered in this rule. This will be applied for both internal, and e-commerce orders (if you use our e-commerce). If the rule is violated, a message  will appear, stating &quot;You must spend a minimum of (amt min) to complete this order.&quot;" />
                        <asp:CheckBox ID="chkEnforceSubTotal" runat="server" Text="Enforce minimum order sub-total amount of" />
                        <asp:TextBox ID="txtEnforceSubTotal" runat="server" Width="80px"></asp:TextBox>
                    </div>
                </div>
            </div>
            <asp:Panel ID="pnlItemName" runat="server" DefaultButton="btnAdd" class="pull-right">
                <div class="form-inline">
                    <label>
                        <asp:Label ID="lblItemLabel" runat="server" Text="Item Name:"></asp:Label></label>
                    <asp:TextBox ID="txtItemName" runat="server" CssClass="form-control"></asp:TextBox>
                    <asp:LinkButton ID="btnAdd" runat="server" OnClientClick="return Save();" CssClass="btn btn-primary"><i class="fa fa-plus-circle"></i>&nbsp;&nbsp;Add</asp:LinkButton>
                    <asp:LinkButton ID="btnState" runat="server" CssClass="btn btn-primary"><i class="fa fa-map-marker"></i>&nbsp;&nbsp;State/Province/Region</asp:LinkButton>
                    <asp:LinkButton ID="lnkbtnShippingServiceAbbr" runat="server" CssClass="btn btn-primary" Visible="false" OnClientClick="return ShippingServiceAbbr();"><i class="fa fa-map-marker"></i>&nbsp;&nbsp;Shipping Service Abbreviation</asp:LinkButton>
                </div>

            </asp:Panel>
        </div>
        <div style="padding-left: 20px;">
            <asp:Label ID="lblLeadSourceWarning" runat="server" Visible="false" ForeColor="#0066cc" Text="<b>Warning</b>: If you�re mapping �Referring page� values (Global Settings | Organizations & Contacts) to set your �Lead Source�, know that changes to this drop-down will affect those mappings."></asp:Label>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">

    <asp:Label ID="lblErrMessage" Visible="true" Style="color: Crimson;" runat="server" EnableViewState="False"></asp:Label>
    <asp:UpdateProgress ID="UpdateProgress" runat="server" ClientIDMode="Static">
        <ProgressTemplate>
            <div class="overlay">
                <div class="overlayContent" style="color: #000; text-align: center; width: 250px; padding: 20px">
                    <i class="fa fa-2x fa-refresh fa-spin"></i>
                    <h3>Processing Request</h3>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>


    <asp:Literal ID="litMessage" runat="server" EnableViewState="False"></asp:Literal>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server" ClientIDMode="Static">
    List Details&nbsp;<a href="#" onclick="return OpenHelpPopUp('admin/frmMasterList.aspx')"><label class="badge bg-yellow">?</label></a>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BizPager" runat="server" ClientIDMode="Static">
    &nbsp;
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="GridPlaceHolder" runat="server"
    ClientIDMode="Static">
    <div class="row">
        <div class="col-xs-12">
            <div class="table-responsive">
            </div>
        </div>
    </div>
    <ul class="nav nav-tabs">
        <li class="active"><a data-toggle="tab" href="#listDetails">List Details</a></li>
        <li><a data-toggle="tab" href="#ddlRelationShip" onclick="bindDropDownRelationShipSection()">Drop-down field relationships</a></li>
        <li><a data-toggle="tab" href="#divTaskRelationship" onclick="bindTaskRelationShipSection();">Task relationships</a></li>
    </ul>
    <div class="tab-content">
        <div id="listDetails" class="tab-pane fade in active">
            <asp:DataGrid ID="dgItemList" runat="server" CssClass="table table-bordered table-striped" AutoGenerateColumns="False"
                AllowSorting="True" Width="100%" UseAccessibleHeader="true">
                <Columns>
                    <asp:BoundColumn DataField="constFlag" Visible="false"></asp:BoundColumn>
                    <asp:BoundColumn DataField="bitDelete" Visible="false"></asp:BoundColumn>
                    <asp:TemplateColumn HeaderText="No" HeaderStyle-Width="10" ItemStyle-Width="40">
                        <ItemTemplate>
                            <asp:Label ID="lblListItemID" runat="server" Style="display: none" Text='<%# DataBinder.Eval(Container,"DataItem.numListItemID") %>'></asp:Label>
                            <%# Container.ItemIndex +1 %>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn>
                        <HeaderTemplate>
                            <asp:Label ID="lblItemHeader" runat="server" Text="Item Name"></asp:Label>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblItemName" runat="server" Text='<%# DataBinder.Eval(Container,"DataItem.vcData") %>'>
                            </asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtEItemName" runat="server" CssClass="form-control" Text='<%# DataBinder.Eval(Container,"DataItem.vcData") %>'> 
                            </asp:TextBox>
                        </EditItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Biz Doc Type">
                        <ItemTemplate>
                            <asp:Label ID="lblBizDocType" runat="server" Text='<%# DataBinder.Eval(Container,"DataItem.vcListType") %>'>
                            </asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:DropDownList ID="ddlListTypeGrid" CssClass="form-control" runat="server" DataValueField="numListItemID"
                                DataTextField="vcData">
                            </asp:DropDownList>
                        </EditItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Order Type">
                        <ItemTemplate>
                            <asp:Label ID="lblOrderType" runat="server" Text='<%# DataBinder.Eval(Container,"DataItem.vcOrderType") %>'>
                            </asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:DropDownList ID="ddlOrderTypeGrid" runat="server" CssClass="form-control">
                                <asp:ListItem Value="0" Text="--All--"></asp:ListItem>
                                <asp:ListItem Value="1" Text="Sales"></asp:ListItem>
                                <asp:ListItem Value="2" Text="Purchase"></asp:ListItem>
                            </asp:DropDownList>
                        </EditItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Opportunity/Order">
                        <ItemTemplate>
                            <asp:Label ID="lblOppStatus" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.vcOppOrOrder")%>'>
                            </asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:DropDownList ID="ddlOppOrOrderGrid" runat="server" CssClass="form-control">
                                <asp:ListItem Value="0" Text="--All--"></asp:ListItem>
                                <asp:ListItem Value="1" Text="Opportunity"></asp:ListItem>
                                <asp:ListItem Value="2" Text="Order"></asp:ListItem>
                            </asp:DropDownList>
                        </EditItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Minimum Order Amount" Visible="false">
                        <ItemTemplate>
                            <asp:Label ID="lblMinOrderAmount" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.fltMinOrderAmount")%>'>
                            </asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtMinOrderAmount" runat="server" CssClass="form-control" Text='<%# DataBinder.Eval(Container, "DataItem.fltMinOrderAmount") %>'> 
                            </asp:TextBox>
                        </EditItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Original BizDoc Name">
                        <ItemTemplate>
                            <asp:Label ID="lblOriginalBizDocName" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.vcListItemGroupName") %>'>
                            </asp:Label>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Follow-up Status Cell Group" Visible="false">
                        <ItemTemplate>
                            <asp:Label ID="lblFollowUpStatsGroupName" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.vcListItemGroupName") %>'>
                            </asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:DropDownList ID="ddlFollowUpStatusGroup" CssClass="form-control" runat="server" DataValueField="numListItemID"
                                DataTextField="vcData">
                            </asp:DropDownList>
                        </EditItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Cell Color" Visible="false">
                        <ItemTemplate>
                            <asp:Label ID="lblFollowupStatusCellColor" CssClass='<%# DataBinder.Eval(Container, "DataItem.vcColorScheme") %>' runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.vcColorScheme").ToString().Replace("gv", "") %>'>
                            </asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:DropDownList ID="ddlColorScheme" CssClass="form-control" runat="server">
                                <asp:ListItem Value="" Text="Select"></asp:ListItem>
                                <asp:ListItem Value="gvColor1" Text="Color1"></asp:ListItem>
                                <asp:ListItem Value="gvColor2" Text="Color2"></asp:ListItem>
                                <asp:ListItem Value="gvColor3" Text="Color3"></asp:ListItem>
                                <asp:ListItem Value="gvColor4" Text="Color4"></asp:ListItem>
                                <asp:ListItem Value="gvColor5" Text="Color5"></asp:ListItem>
                                <asp:ListItem Value="gvColor6" Text="Color6"></asp:ListItem>
                                <asp:ListItem Value="gvColor7" Text="Color7"></asp:ListItem>
                                <asp:ListItem Value="gvColor8" Text="Color8"></asp:ListItem>
                                <asp:ListItem Value="gvColor9" Text="Color9"></asp:ListItem>
                                <asp:ListItem Value="gvColor10" Text="Color10"></asp:ListItem>
                                <asp:ListItem Value="gvColor11" Text="Color11"></asp:ListItem>
                                <asp:ListItem Value="gvColor12" Text="Color12"></asp:ListItem>
                            </asp:DropDownList>
                        </EditItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Blog Category" Visible="false">
                        <ItemTemplate>
                            <asp:Label ID="lblBlogCategoryName" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.vcListItemGroupName") %>'>
                            </asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:DropDownList ID="ddlBlogCategory" CssClass="form-control" runat="server" DataValueField="numListItemID"
                                DataTextField="vcData">
                            </asp:DropDownList>
                        </EditItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderStyle-Width="64" ItemStyle-Width="64" ItemStyle-Wrap="false">
                        <ItemTemplate>
                            <asp:LinkButton runat="server" CssClass="btn btn-info btn-xs" CommandName="Edit" ID="lnkbtnEdt"><i class="fa fa-pencil"></i></asp:LinkButton>
                            <asp:LinkButton ID="lnkdelete" runat="server" Visible="false">
											        <font color="#730000">*</font></asp:LinkButton>
                            <asp:LinkButton ID="btnDelete" runat="server" CssClass="btn btn-danger btn-xs" CommandName="Delete"><i class="fa fa-trash"></i></asp:LinkButton>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:LinkButton ID="lnkbtnUpdt" runat="server" Text="Update" CssClass="btn btn-primary" CommandName="Update" OnClientClick="return EditGrid();"></asp:LinkButton>&nbsp;
                    <asp:LinkButton runat="server" Text="Cancel" CommandName="Cancel" ID="lnkbtnCncl" CssClass="btn btn-default"></asp:LinkButton>
                        </EditItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn Visible="false">
                        <ItemTemplate>
                            <asp:Label ID="lblIsEnforceMinOrderAmount" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.bitEnforceMinOrderAmount")%>'>
                            </asp:Label>
                        </ItemTemplate>
                    </asp:TemplateColumn>

                </Columns>
            </asp:DataGrid>
        </div>
        <div id="ddlRelationShip" class="tab-pane fade">
            <asp:UpdatePanel ID="updatePanelRelationship" runat="server">
                <ContentTemplate>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-inline" style="padding: 10px;">
                                <label>Add new relationship module:</label>
                                <asp:DropDownList ID="ddlRModule" runat="server" CssClass="form-control" AutoPostBack="true">
                                </asp:DropDownList>
                                <asp:Label runat="server" ID="lblParent" Text="Select Parent Drop-down field"></asp:Label>
                                <asp:DropDownList ID="ddlPrimaryDropDown" runat="server" CssClass="form-control" onchange="bindDropDownRelationShipSection(false);">
                                </asp:DropDownList>
                                <asp:Label runat="server" ID="lblChild" Text="Select Child Drop-down field"></asp:Label>
                                <asp:DropDownList ID="ddlSecondaryDropDown" runat="server" CssClass="form-control" onchange="bindDropDownRelationShipSection(true);">
                                </asp:DropDownList>
                                <a href="javascript:void(0)" id="btnCreate" class="btn btn-primary" onclick="CreateDropDownListRelationShip()">Add</a>
                            </div>
                        </div>
                    </div>

                </ContentTemplate>
            </asp:UpdatePanel>
            <div class="row">
                <div class="col-md-12">
                    <div class="box-group boxForList" id="DropDownListaccordion">
                    </div>
                </div>
            </div>

        </div>
        <div id="divTaskRelationship" class="tab-pane fade">
            <div class="row">
                <div class="col-md-12">
                    <div class="form-inline" style="padding: 10px;">
                        <label>Add new relationship module:</label>
                        <select id="ddlProcessList" style="width: 200px" onchange="getStageList()">
                            <option value="0">--Select--</option>
                        </select>
                        <label>Select Parent:</label>
                        <select id="ddlStages" style="width: 200px" onchange="getTaskList()">
                            <option value="0">--Select--</option>
                        </select>
                        <label>Select Child:</label>
                        <select id="ddlStagesTask" style="width: 200px">
                            <option value="0">--Select--</option>
                        </select>
                        <button type="button" class="btn btn-sm btn-primary" onclick="CreateTaskListRelationShip()">Add</button>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="box-group boxForList" id="TaskListaccordion">
                    </div>
                </div>
            </div>
        </div>
    </div>

    <asp:Button ID="Button1" CssClass="button" runat="server" Text="Add" Width="60" Style="display: none" />
</asp:Content>
<asp:Content ContentPlaceHolderID="GridSettingPopup" runat="server">
    <asp:LinkButton ID="btnOpenDropdoel" runat="server" CssClass="btn btn-sm btn-primary"><i class="fa fa-retweet"></i>&nbsp;&nbsp;Dropdown Relationship</asp:LinkButton>
</asp:Content>
