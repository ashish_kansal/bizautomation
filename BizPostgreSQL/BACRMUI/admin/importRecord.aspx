<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" CodeBehind="importRecord.aspx.vb"
    Inherits="BACRM.UserInterface.Admin.importRecord" %>

<%@ Register TagPrefix="menu1" TagName="webmenu" Src="../include/webmenu.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link rel="stylesheet" href="~/CSS/master.css" type="text/css" />
    <title>Imports Wizard</title>
    <script language="javascript">
        function setDllValues() {
            if (document.getElementById('tbldtls').rows.length < 2) {
                alert("Please Upload the file first")
                return false;
            }
            var intCnt;
            inCnt = document.Form1.txtDllValue.value;
            document.Form1.txtDllValue.value = "";
            var dllValue;
            dllValue = "";
            for (var x = 0; x <= inCnt; x++) {
                if (dllValue != "") {
                    dllValue = dllValue + "," + document.getElementById('ddlDestination' + x).value;
                }
                else {
                    dllValue = document.getElementById('ddlDestination' + x).value;
                }
            }
            document.Form1.txtDllValue.value = dllValue;
        }

        function checkFileExt() {

            if (document.Form1.txtFile.value.length == 0) {
                alert("Plz Select File");
                return false;
            }
            else {

                var data = document.Form1.txtFile.value

                var extension = data.substr(data.lastIndexOf('.'));

                if (extension == '.csv' || extension == '.CSV') {
                    //|| extension =='xls' || extension =='XLS'               
                    return true;
                }
                else {
                    alert("Plz Select a .csv File");
                    return false;
                }

            }
        }

        function displayPBar() {

            if (document.getElementById('tbldtls').rows.length <= 2) {
                alert("please click on display records before importing")
                return false;
            }
            if (document.Form1.ddlSelection.value == -1) {
                alert("please select destination")
                return false;
            }
            if (document.Form1.pgBar.style.display == "none")
                document.Form1.pgBar.style.display = "";
            else
                document.Form1.pgBar.style.display = "none";
        }
        function ShowGroup() {
            if (document.Form1.ddlSelection.value == 0) {
                document.Form1.ddlGroup.style.display = '';
                document.getElementById('lblGroup').style.display = '';
            }
            else {
                document.Form1.ddlGroup.style.display = 'none';
                document.getElementById('lblGroup').style.display = 'none';
            }
        }
        function OpenConf() {
            window.open("../admin/frmImportRecordConf.aspx?ImportType=1", '', 'width=800,height=300,status=no,titlebar=no,scrollbar=yes,resizable=yes,top=110,left=150')
            return false;
        }
    </script>
</head>
<body>
    <form id="Form1" method="post" runat="server">
    <menu1:webmenu ID="webmenu1" runat="server"></menu1:webmenu>
     <table width="100%">
        <tr>
            <td class="normal4" align="center">
                <asp:Literal ID="litMessage" runat="server"></asp:Literal>
            </td>
        </tr>
    </table>
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td valign="bottom">
                <table class="TabStyle">
                    <tr>
                        <td>
                            &nbsp;&nbsp;&nbsp;Import Wizard&nbsp;&nbsp;&nbsp;
                        </td>
                    </tr>
                </table>
            </td>
            <td valign="middle">
                <img src="../images/pgBar.gif" runat="server" id="pgBar">
            </td>
            <td align="right">
                <a href="http://help.bizautomation.com/?pageurl=importRecord.aspx"
                    target="_blank" title="help">Trouble importing Organizations [?]</a>&nbsp;
                <asp:Button ID="btnConfg" runat="server" Text="Configure" CssClass="button" Width="100">
                </asp:Button>
            </td>
        </tr>
    </table>
    <asp:Table ID="Table2" CellPadding="0" CellSpacing="0" Width="100%" runat="server"
        BorderColor="black" GridLines="None" Height="400" BorderWidth="1" CssClass="aspTable">
        <asp:TableRow>
            <asp:TableCell VerticalAlign="Top">
                <br>
                <br>
                <table width="100%" cellspacing="4">
                    <tr>
                        <td class="normal1" align="center" width="20">
                            1
                        </td>
                        <td colspan="2">
                            <asp:DropDownList runat="server" CssClass="signup" ID="ddlSelection">
                                <asp:ListItem Value="-1">Choose Destination</asp:ListItem>
                                <asp:ListItem Value="0">Leads</asp:ListItem>
                                <asp:ListItem Value="1">Prospects</asp:ListItem>
                                <asp:ListItem Value="2">Accounts</asp:ListItem>
                            </asp:DropDownList>
                            &nbsp;
                            <asp:Label ID="lblGroup" runat="server" CssClass="text"> Group</asp:Label>
                            &nbsp;
                            <asp:DropDownList ID="ddlGroup" runat="server" CssClass="signup" Width="130">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="normal1" align="center">
                            2
                        </td>
                        <td class="normal1" colspan="2">
                            Relationship :
                            <asp:DropDownList runat="server" Width="130px" CssClass="signup" ID="ddlRelationship"
                                AutoPostBack="true">
                            </asp:DropDownList>
                            &nbsp;<asp:LinkButton ID="lbDownloadCSV" Text="Download CSV template for selected relationship"
                                runat="server" CssClass="hyperlink" />
                        </td>
                    </tr>
                    <tr>
                        <td class="normal1" align="center">
                            3
                        </td>
                        <td class="normal1" colspan="2">
                            <asp:RadioButton runat="server" GroupName="Rad" Checked="True" ID="rdRouting"></asp:RadioButton>
                            Let Routing Rule assign ownership<br>
                            <asp:RadioButton runat="server" GroupName="Rad" ID="rdAssign"></asp:RadioButton>
                            Assign Ownership To :
                            <asp:DropDownList runat="server" CssClass="signup" ID="ddlAssign">
                                <asp:ListItem Value="0">--Select One--</asp:ListItem>
                            </asp:DropDownList>
                            <br />
                            <asp:RadioButton runat="server" GroupName="Rad" ID="rbRecOwnerColumn"></asp:RadioButton>Use
                            Record Owner column to assign ownership, if not found then set default ownership
                            to :
                            <asp:DropDownList runat="server" CssClass="signup" ID="ddlAssign1">
                                <asp:ListItem Value="0">--Select One--</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="normal1" align="center">
                            4
                        </td>
                        <td class="normal1" colspan="2">
                            Lead Source
                            <asp:DropDownList runat="server" CssClass="signup" ID="ddlInfoSource">
                            </asp:DropDownList>
                            &nbsp;(How did you hear about us)
                        </td>
                    </tr>
                    <tr>
                        <td class="normal1" align="center">
                            5
                        </td>
                        <td colspan="2" class="normal1">
                            <asp:CheckBox runat="server" ID="chkMultipleContacts" CssClass="signup" Text="Want to import multiple contacts to One Organization ?" />
                            &nbsp;<a href="http://help.bizautomation.com/?pageurl=importRecord.aspx"
                                target="_blank" title="help">Read before you import multiple contacts [?]</a>
                        </td>
                    </tr>
                    <tr>
                        <td class="normal1" align="center">
                        </td>
                        <td colspan="2" class="normal1">
                            <asp:CheckBox runat="server" ID="chkCorrespondance" CssClass="signup" Text="Will you be importing Correspondance/Action items for uploaded Organizations?" />
                            &nbsp;<a href="http://help.bizautomation.com/?pageurl=importRecord.aspx"
                                target="_blank" title="help">Why its important [?]</a>
                        </td>
                    </tr>
                    <tr>
                        <td class="normal1" align="center">
                        </td>
                        <td colspan="2" class="normal1">
                            <asp:CheckBox runat="server" ID="chkAssociations" CssClass="signup" Text="Will you be importing Organizations Association?" />
                        </td>
                    </tr>
                    <tr>
                        <td class="normal1" align="center">
                            6
                        </td>
                        <td class="normal1" colspan="2">
                            Choose your source &amp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                        <td class="normal1" colspan="2">
                            Source&nbsp;
                            <input runat="server" id="txtFile" type="file" class="signup">
                            &nbsp;
                            <asp:Button runat="server" ID="btnUpload" Text="Upload" CssClass="button"></asp:Button>
                            &nbsp;(Upload the CSV file to view before you save it to database)
                        </td>
                    </tr>
                    <tr>
                        <td class="normal1" align="center">
                            7
                        </td>
                        <td colspan="2" class="normal1">
                            Mapping
                            <asp:TextBox ID="txtDllValue" runat="server" Height="16px" Style="display: none"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                        <td colspan="2" class="normal1">
                            <div style="height: 250px; width: 1000px; overflow: scroll;">
                                <asp:Table ID="tbldtls" runat="server" Width="100%" GridLines="none" BorderWidth="0"
                                    CellSpacing="1">
                                </asp:Table>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="normal1" align="center">
                            8
                        </td>
                        <td colspan="2" class="normal1">
                            <asp:Button ID="btnDisplay" runat="server" Text="Display Records" CssClass="button"
                                Width="100"></asp:Button>
                        </td>
                    </tr>
                    <tr>
                        <td class="normal1" align="center">
                            9
                        </td>
                        <td colspan="2" class="normal1">
                            <asp:Button ID="btnImports" runat="server" Text="Import Records to database" CssClass="button"
                                Width="170px"></asp:Button>
                        </td>
                    </tr>
                </table>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
    </form>
</body>
</html>
