<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" CodeBehind="frmEditRoutingRules.aspx.vb"
    Inherits="BACRM.UserInterface.Admin.frmEditRoutingRules" ValidateRequest="false"
    MasterPageFile="~/common/Popup.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Manage Routing Rules</title>
    <script language="javascript" src="../javascript/AutoLeadsRoutingRules.js"></script>
    <script language="javascript" type="text/javascript">
        function Save(a) {
            if (document.form1.txtRuleDesc.value == '') {
                alert("Enter Rule Description")
                document.form1.txtRuleDesc.focus();
                return false;
            }
            if (document.form1.ddlAvailableFields.value == 0) {
                alert("Select Field")
                document.form1.ddlAvailableFields.focus();
                return false;
            }

            if (a == 1) {
                if (document.form1.ddlAvailableFieldsOptions.value == 0) {
                    alert("Select Field")
                    document.form1.ddlAvailableFields.focus();
                    return false;
                }
            }
            else if (a == 2) {
                if (document.form1.txtIncludedText.value == '') {
                    alert("Enter Field Description")
                    document.form1.txtIncludedText.focus();
                    return false;
                }
            }
            else if (a == 3) {
                if (document.form1.ddlAOIList.options.length == 0) {
                    alert("Select Areas of Interests")
                    document.form1.ddlAOIList.focus();
                    return false;
                }
            }
            else if (a == 4) {
                if (document.form1.lstAddfld.options.length == 0) {
                    alert("Select States")
                    document.form1.lstAddfld.focus();
                    return false;
                }
            }
            if (document.form1.ddlDistributionList.options.length == 0) {
                alert("Select Circular Distribution List")
                document.form1.ddlDistributionList.focus();
                return false;
            }
            var str = '';
            if (document.form1.ddlAOIList != null) {
                for (var i = 0; i < document.form1.ddlAOIList.options.length; i++) {
                    var SelectedValue;
                    SelectedValue = document.form1.ddlAOIList.options[i].value;
                    str = str + SelectedValue + ','
                }
                document.form1.txtAOI.value = str

            }
            str = ''
            for (var i = 0; i < document.form1.ddlDistributionList.options.length; i++) {
                var SelectedValue;
                SelectedValue = document.form1.ddlDistributionList.options[i].value;
                str = str + SelectedValue + ','
            }
            document.form1.txtEmp.value = str
            str = ''
            if (document.form1.lstAddfld != null) {
                for (var i = 0; i < document.form1.lstAddfld.options.length; i++) {
                    var SelectedValue;
                    SelectedValue = document.form1.lstAddfld.options[i].value;
                    str = str + SelectedValue + ','
                }
                document.form1.txtState.value = str
            }

        }
        function Back() {
            opener.location.reload(true);
            window.close();
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <div class="input-part">
        <div class="right-input">
            <table cellspacing="0" cellpadding="0" width="100%">
                <tr>
                    <td class="normal1" align="right">
                        <input type="hidden" runat="server" id="hdDistributionList" name="hdDistributionList"
                            value="">
                        <input type="hidden" runat="server" id="hdRoutingRuleChangeIndicator" name="hdRoutingRuleChangeIndicator"
                            value="0">
                        <input type="hidden" runat="server" id="hdAOIList" value="" name="hdAOIList">
                        <input type="hidden" runat="server" id="hdAOINameList" value="" name="hdAOINameList">
                        <input type="hidden" runat="server" id="hdAvailableIFFieldList" name="hdAvailableIFFieldList"
                            value="">
                        <input type="hidden" runat="server" id="hdSave" value="False" name="hdSave">
                        <input type="hidden" runat="server" id="hdIFFieldSelected" value="" name="hdIFFieldSelected">
                        <input type="button" style="display: none" onclick="return ProcessPreSubmitValidation(document.form1)">
                        <asp:Button ID="btnSave" Width="60" CssClass="button" runat="server" Text="Save">
                        </asp:Button>
                        <input type="button" class="button" value="Close" style="width: 60" onclick="Back()">
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <asp:Literal ID="plDynamicContentPlaceholder" runat="server"></asp:Literal>
    <asp:Literal ID="litReferencedFields" runat="server"></asp:Literal>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageTitle" runat="server" ClientIDMode="Static">
    Routing Rule
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="Content" runat="server" ClientIDMode="Static">
    <asp:Table ID="tblRoutingRule" runat="server" Height="400" BorderWidth="1" BorderColor="black"
        GridLines="None" CssClass="aspTable" Width="100%" CellPadding="2" CellSpacing="2">
        <asp:TableRow>
            <asp:TableCell Width="100%" VerticalAlign="top">
                <table cellpadding="2" cellspacing="2" width="100%">
                    <tr>
                        <td>
                            <table width="100%" runat="server" id="tblRoutingRuleDetails">
                                <tr>
                                    <td class="normal1" height="23" align="right">
                                        Rule Description :
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtRuleDesc" runat="server" CssClass="signup" AutoPostBack="False"
                                            Width="335" MaxLength="50"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr id="trPriority" runat="server">
                                    <td class="normal1" height="23" align="right">
                                        Priority :
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddlPriority" runat="server" Width="50" CssClass="signup">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="normal1" align="right">
                                        Select Field :
                                    </td>
                                    <td class="normal1">
                                        <asp:DropDownList ID="ddlAvailableFields" runat="server" CssClass="signup" AutoPostBack="True"
                                            Width="200" OnSelectedIndexChanged="ddlAvailableFields_SelectedIndexChanged">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr id="trCountry" runat="server">
                                    <td class="normal1" align="right">
                                        Select Country :
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddlCountry" runat="server" CssClass="signup" AutoPostBack="True"
                                            Width="200">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr id="trState" runat="server">
                                    <td colspan="2">
                                        <table cellpadding="5" cellspacing="5" width="100%">
                                            <tr>
                                                <td align="center" class="normal1">
                                                    Available States :
                                                </td>
                                                <td>
                                                    <asp:ListBox ID="lstAvailablefld" runat="server" Width="250" Height="200" CssClass="signup">
                                                    </asp:ListBox>
                                                </td>
                                                <td align="center" valign="middle">
                                                    <asp:Button ID="btnAdd" CssClass="button" runat="server" Text="Add >"></asp:Button>
                                                    <asp:Button ID="btnRemove" CssClass="button" runat="server" Text="< Remove"></asp:Button>
                                                </td>
                                                <td align="center" class="normal1">
                                                    Selected States :
                                                    <br>
                                                    <asp:ListBox ID="lstAddfld" runat="server" Width="250" Height="200" CssClass="signup">
                                                    </asp:ListBox>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr id="trEqualTo" runat="server">
                                    <td class="normal1" height="23" align="right">
                                        IS EQUAL TO :
                                    </td>
                                    <td class="normal1" height="23">
                                        <asp:DropDownList ID="ddlAvailableFieldsOptions" runat="server" CssClass="signup"
                                            Width="200">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr id="trValues" runat="server">
                                    <td class="normal1" valign="top" height="23" align="right">
                                        INCLUDES ANY OF THE FOLLOWING<br>
                                        (Separated by commas)
                                    </td>
                                    <td class="normal1" height="23">
                                        <asp:TextBox ID="txtIncludedText" runat="server" CssClass="signup" Width="200" Height="80"
                                            TextMode="MultiLine"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr id="trAOI" runat="server">
                                    <td class="normal1" valign="top" height="23" align="right">
                                        INCLUDES THE FOLLOWING AOIs :
                                    </td>
                                    <td class="normal1" valign="top">
                                        <asp:ListBox ID="ddlAOIList" runat="server" CssClass="signup" Rows="4" Width="200" Height="100px">
                                        </asp:ListBox><br /><br />
                                        <input class="button" id="btnEditIncludeList" type="button" value="Build AOIs Include List"
                                            onclick="showSelectableOptions('trRadioTwo','trRadioThree');" style="width: 125;">&nbsp;<br /><br />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="normal1" height="23" align="right">
                                        THEN CREATE LEAD, THEN ROUTE AND<br />
                                        ASSIGN TO :
                                    </td>
                                    <td class="normal1">
                                        <asp:ListBox ID="ddlDistributionList" runat="server" CssClass="signup" Rows="4" Width="200" Height="100">
                                        </asp:ListBox><br /><br />
                                        <input class="button" id="btnCreateDistributionList" type="button" value="Create Distribution List"
                                            onclick="showSelectableOptions('trRadioThree','trRadioTwo');" style="width: 125;"><br /><br />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </asp:TableCell>
            </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell Width="100%" VerticalAlign="Top" HorizontalAlign="Right">
                <br />
                <table cellspacing="2" cellpadding="2" style="
    width: 100%;
">
                    <tr id="trRadioTwo" style="display: none;">
                        <td valign="top">
                            <table cellspacing="2" cellpadding="2" width="100%" border="0">
                                <tr>
                                    <td class="normal1" valign="middle" align="left" colspan="2" height="23">
                                        <b>AOI Include List</b>
                                    </td>
                                    <td valign="middle" align="right" colspan="2" height="23">
                                        <input class="button" id="btnNew2Ok" onclick="makeDataTransfer(document.form1.ddlSelectedAOIs,document.form1.ddlAOIList,'trRadioTwo')"
                                            type="button" value="Ok" name="btnNew2Ok">
                                        <input class="button" id="btnNew2" onclick="HideTableRow('trRadioTwo')" type="button"
                                            value="Close" name="btnNew2">&nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" class="normal1" valign="top">
                                        AOIs<br>
                                        <asp:ListBox ID="ddlAvailableAOIs" runat="server" Width="150" Height="174" CssClass="signup"
                                            EnableViewState="True"></asp:ListBox>
                                    </td>
                                    <td align="center" valign="middle">
                                        <input type="button" id="btnAddTwo" class="button" value="Add >" style="width: 57;"
                                            onclick="javascript:move(document.form1.ddlAvailableAOIs,document.form1.ddlSelectedAOIs)">
                                        <br>
                                        <br>
                                        <input type="button" id="btnRemoveTwo" class="button" value="< Remove" style="width: 57;"
                                            onclick="javascript:remove(document.form1.ddlSelectedAOIs,document.form1.ddlAvailableAOIs)">
                                    </td>
                                    <td class="normal1">
                                        AOIs Chosen<br>
                                        <asp:ListBox ID="ddlSelectedAOIs" runat="server" Width="150" Height="174" CssClass="signup"
                                            EnableViewState="False"></asp:ListBox>
                                    </td>
                                    <td align="center" valign="middle">
                                        <img id="btnMoveupTwo" src="../images/upArrow.gif" onclick="javascript:MoveUp(document.form1.ddlSelectedAOIs)" />
                                        <br>
                                        <img id="btnMoveDownTwo" src="../images/downArrow1.gif" onclick="javascript:MoveDown(document.form1.ddlSelectedAOIs,0)" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr id="trRadioThree" style="display: none;">
                        <td valign="top">
                            <table cellspacing="2" cellpadding="2" width="100%" border="0">
                                <tr>
                                    <td class="normal1" valign="middle" align="left" colspan="2" height="23">
                                        <b>Circular Distribution List</b>
                                    </td>
                                    <td valign="middle" align="right" colspan="2" height="23">
                                        <input class="button" id="btnNew3Ok" onclick="makeDataTransfer(document.form1.ddlSelectedEmployees,document.form1.ddlDistributionList,'trRadioThree')"
                                            type="button" value="Ok" name="btnNew3Ok">
                                        <input class="button" id="btnNew3" onclick="HideTableRow('trRadioThree')" type="button"
                                            value="Close" name="btnNew3">&nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" class="normal1" valign="top">
                                        Employees<br>
                                        <asp:ListBox ID="ddlAvailableEmployees" runat="server" Width="150" Height="174" CssClass="signup"
                                            EnableViewState="true"></asp:ListBox>
                                    </td>
                                    <td align="center" valign="middle">
                                        <input type="button" id="btnAddThree" class="button" value="Add >" style="width: 57;"
                                            onclick="javascript:move(document.form1.ddlAvailableEmployees,document.form1.ddlSelectedEmployees)">
                                        <br>
                                        <br>
                                        <input type="button" id="btnRemoveThree" class="button" value="< Remove" style="width: 57;"
                                            onclick="javascript:remove(document.form1.ddlSelectedEmployees,document.form1.ddlAvailableEmployees)">
                                    </td>
                                    <td class="normal1">
                                        Distribution List<br>
                                        <asp:ListBox ID="ddlSelectedEmployees" runat="server" Width="150" Height="174" CssClass="signup"
                                            EnableViewState="False"></asp:ListBox>
                                    </td>
                                    <td align="center" valign="middle">
                                        <img id="btnMoveupThree" src="../images/upArrow.gif" onclick="javascript:MoveUp(document.form1.ddlSelectedEmployees)" />
                                        <br>
                                        <img id="btnMoveDownThree" src="../images/downArrow1.gif" onclick="javascript:MoveDown(document.form1.ddlSelectedEmployees,0)" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
    <input type="hidden" id="txtAOI" runat="server" />
    <input type="hidden" id="txtEmp" runat="server" />
    <input type="hidden" id="txtState" runat="server" />
</asp:Content>
