﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmUOMConversion.aspx.vb"
    Inherits=".frmUOMConversion" MasterPageFile="~/common/Popup.Master" ClientIDMode="Predictable" %>

<asp:Content ID="Content3" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>UOM - Unit Of Measurement Conversion</title>
    <script language="javascript" type="text/javascript">
        function Close() {
            window.close();
        }

        function ClientSideClick() {
            var isGrpOneValid = Page_ClientValidate("vgRequired");
            var check = Check();

            var i;
            for (i = 0; i < Page_Validators.length; i++) {
                ValidatorValidate(Page_Validators[i]); //this forces validation in all groups
            }

            if (isGrpOneValid && check) {
                return true;
            }
            else
                return false;
        }

        function Check() {
            if (Number(document.getElementById("hfTotalRow").value) > 0) {
                for (var i = 1; i <= Number(document.getElementById("hfTotalRow").value); i++) {
                    if (document.getElementById("ddlUnit_1_" + i).value == document.getElementById("ddlUnit_2_" + i).value) {
                        alert('Base and Conversion Unit are not same!!!');
                        return false;
                    }
                    for (var j = 1; j <= Number(document.getElementById("hfTotalRow").value); j++) {
                        if (j != i) {
                            if ((document.getElementById("ddlUnit_1_" + i).value == document.getElementById("ddlUnit_1_" + j).value && document.getElementById("ddlUnit_2_" + i).value == document.getElementById("ddlUnit_2_" + j).value)
                                  || (document.getElementById("ddlUnit_1_" + i).value == document.getElementById("ddlUnit_2_" + j).value && document.getElementById("ddlUnit_2_" + i).value == document.getElementById("ddlUnit_1_" + j).value)) {
                                alert('Select distinct base and conversion unit!!!');
                                return false;
                            }
                        }
                    }
                }
            }
            else {
                return false;
            }

            return true;
        }
    </script>
    <style type="text/css">
        .row
        {
            background-color: #DBE5F1;
            text-align: left;
            color: black;
            font-family: Arial;
            font-size: 8pt;
        }
        .arow
        {
            background-color: #fffff;
            text-align: left;
            color: black;
            font-family: Arial;
            font-size: 8pt;
        }

        input[type='checkbox'] {
            position: relative;
            bottom: 1px;
            vertical-align: middle;
        }

        .tooltip {
            top:0px !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <div class="input-part">
        <asp:CheckBox ID="chkItemLevelUOM" runat="server" Font-Bold="true" Text="Set UOMs at Item Level" /><asp:Label ID="Label1" CssClass="tip" runat="server" ToolTip="When selecting this option, you’ll enable UOM calculations within item, and require that UOMs be configured based on base units within that item. For example, if you sell in Eaches, but buy in Cases, but a “Case” for item called “Beer” consists of 24 “Eaches”, but within an item called “Soap” consists of 10 “Eaches”, you would be able to leverage the same “Case” UOM differently across each item." Text="[?]"></asp:Label>
        <div class="right-input">
            <asp:HiddenField ID="hfTotalRow" runat="server" Value="0" />
            <asp:Button ID="btnAddNew" CssClass="button" runat="server" Text="Add New"></asp:Button>
            <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" ValidationGroup="vgRequired"
                OnClientClick="return ClientSideClick()"></asp:Button>
            <asp:Button ID="btnSaveClose" runat="server" CssClass="button" Text="Save &amp; Close"
                ValidationGroup="vgRequired" OnClientClick="return ClientSideClick()"></asp:Button>
            <asp:Button ID="btnCancel" runat="server" CssClass="button" Width="50" Text="Close">
            </asp:Button>&nbsp;&nbsp;
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="PageTitle" runat="server" ClientIDMode="Static">
    Unit Of Measurement Conversion
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="Content" runat="server" ClientIDMode="Static">
    <asp:Table ID="tblConversion" runat="server" GridLines="None" BorderColor="black"
        Width="700px" BorderWidth="1" CssClass="aspTable" CellSpacing="0" CellPadding="0"
        HorizontalAlign="Center">
    </asp:Table>
    <br />
    <b>Example of UOM configuration</b>
    <table border="1" cellpadding="3" cellspacing="0">
        <tr>
            <td style="font-weight: 700">
                Unit of measure
            </td>
            <td>
                <strong>Quantity </strong>
            </td>
            <td>
                <strong>Equivalent </strong>
            </td>
        </tr>
        <tr>
            <td>
                Can
            </td>
            <td>
                1
            </td>
            <td>
                Can
            </td>
        </tr>
        <tr>
            <td>
                Six-pack
            </td>
            <td>
                6
            </td>
            <td>
                Can
            </td>
        </tr>
        <tr>
            <td>
                12-pack
            </td>
            <td>
                2
            </td>
            <td>
                Can
            </td>
        </tr>
        <tr>
            <td>
                Case
            </td>
            <td>
                24
            </td>
            <td>
                Can
            </td>
        </tr>
        <tr>
            <td>
                Case
            </td>
            <td>
                4
            </td>
            <td>
                Six-pack
            </td>
        </tr>
    </table>
</asp:Content>
