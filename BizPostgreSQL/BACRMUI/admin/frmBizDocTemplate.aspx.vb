﻿Imports Telerik.Web.UI
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Opportunities
Imports System.IO
Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Marketing

Partial Public Class frmBizDocTemplate
    Inherits BACRMPage

    Dim TemplateType As Short
    Dim BizDocTempId As Long
    Dim sOppType As Short

    'http://www.telerik.com/help/aspnet-ajax/addcustomdropdowns.html
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ""
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "none")

            TemplateType = CCommon.ToShort(GetQueryStringVal("TemplateType"))
            BizDocTempId = CCommon.ToLong(GetQueryStringVal("BizDocTempId"))
            sOppType = CCommon.ToShort(GetQueryStringVal("OppType"))
            'Added by Neelam Kapila || 09/29/2017 - Added function to show BizDocId 
            If BizDocTempId = 0 Then
                divBiz.Visible = False
            Else
                divBiz.Visible = True
                lblBizDocId.Text = BizDocTempId
            End If

            divBizDocID.Visible = False

            If Not IsPostBack Then

                If TemplateType = 0 Then
                    GetUserRightsForPage(13, 3)

                    If m_aryRightsForPage(RIGHTSTYPE.ADD) = 0 Then
                        btnSave.Visible = False
                    End If
                    'Author:sachin sadhu||Date:04thJan2014
                    'Purpose:To Disable header,footer n preview  buttons on New Biz doc creation
                    If BizDocTempId = 0 Then
                        hplBizDoc.Visible = False
                        spnBizDoc.Visible = False
                        aHeaderlogo.Visible = False
                        afooterlogo.Visible = False

                    Else
                        hplBizDoc.Visible = True
                        spnBizDoc.Visible = True
                        aHeaderlogo.Visible = True
                        afooterlogo.Visible = True
                    End If
                    'End of Code by Sachin

                    If ddlOppType.Items.FindByValue(sOppType) IsNot Nothing Then
                        ddlOppType.Items.FindByValue(sOppType).Selected = True
                    End If
                    BindBizDoc()

                    Dim ddl As EditorDropDown = CType(RadEditor1.FindTool("BizDocElement"), EditorDropDown)
                    If sOppType = 1 Then
                        ddl.Items.Add("Partner Source", "#PartnerSource#")
                        ddl.Items.Add("Release Date", "#ReleaseDate#")
                    ElseIf sOppType = 2 Then
                        ddl.Items.Add("Customer PO#", "#CustomerPO##")
                        ddl.Items.Add("Vendor Invoice", "#VendorInvoice#")
                        ddl.Items.Add("Required Date", "#RequiredDate#")
                    End If


                    Dim ddList As New System.Collections.Generic.List(Of Telerik.Web.UI.EditorDropDownItem)
                    For Each item As Telerik.Web.UI.EditorDropDownItem In ddl.Items
                        ddList.Add(item)
                    Next

                    ddl.Items.Clear()
                    Dim i As Integer = 0
                    For Each item As Telerik.Web.UI.EditorDropDownItem In ddList.OrderBy(Function(x) x.Name)
                        ddl.Items.Insert(i, item)
                        i = i + 1
                    Next
                Else
                    Dim ddl As EditorDropDown = CType(RadEditor1.FindTool("BizDocElement"), EditorDropDown)
                    ddl.Items.Clear()

                    ddl.Items.Add("Logo", "#Logo#")

                    If TemplateType = 1 Then
                        lblTitle.Text = "Modify Unsubscribe Page Design"
                        ddl.Items.Add("Email TextBox", "#EmailTextBox#")
                    ElseIf TemplateType = 2 Then
                        lblTitle.Text = "Modify Action Items Attendees Page Design"
                        ddl.Items.Add("Attendees Status DropDown", "#AttendeesStatusDropDown#")
                    End If

                    ddl.Items.Add("Submit Button", "#SubmitButton#")

                    trOppType.Visible = False
                    trBizDoc.Visible = False
                    trTemplateName.Visible = False
                End If

                objCommon.sb_FillComboFromDBwithSel(ddlRelationship, 5, Session("DomainID"))
                objCommon.sb_FillComboFromDBwithSel(ddlProfile, 21, Session("DomainID"))
                FillClass()
                BindData()
                hplBizDoc.Attributes.Add("onclick", "return OpenAtch()")
            End If
            'btnSave.Attributes.Add("onclick", "return DeleteRecord()")
            'Set Image Manage and Template Manager

            Campaign.SetRadEditorPath(RadEditor1, CCommon.ToLong(Session("DomainID")), Page)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(Convert.ToString(ex))
        End Try

    End Sub

    'Added By:Sachin Sadhu||Date:11thmarch2014
    'Purpose:To prompt user on Insertion of  duplicate records of Default templates
    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try

            Dim objOppBizDoc As New OppBizDocs
            objOppBizDoc.DomainID = Session("DomainID")
            objOppBizDoc.BizDocId = IIf(TemplateType = 0, ddlBizDoc.SelectedValue, 0)
            objOppBizDoc.OppType = IIf(TemplateType = 0, ddlOppType.SelectedValue, 0)
            objOppBizDoc.TemplateType = TemplateType

            Dim dt As DataTable = objOppBizDoc.GetDefautlBizDocTemplateList()


            If BizDocTempId = 0 Then 'On Creation of New
                If chkDefault.Checked Then
                    If dt.Rows.Count > 0 Then
                        Dim confirmValue As String = Request.Form("confirm_value")
                        If confirmValue = "Yes" Then
                            ''if the user clicks OK and confirms replacing of default template
                            SaveBizTemplate()
                        Else
                            chkDefault.Checked = False
                            SaveBizTemplate()
                        End If
                    Else
                        SaveBizTemplate()
                    End If
                Else
                    SaveBizTemplate()
                End If
            Else 'On Update
                SaveBizTemplate()
            End If
            'Commented by Sachin

            'Dim objOppBizDoc As New OppBizDocs
            'objOppBizDoc.DomainID = Session("DomainID")

            'objOppBizDoc.BizDocId = IIf(TemplateType = 0, ddlBizDoc.SelectedValue, 0)
            'objOppBizDoc.OppType = IIf(TemplateType = 0, ddlOppType.SelectedValue, 0)
            'objOppBizDoc.TemplateType = TemplateType
            'objOppBizDoc.byteMode = IIf(TemplateType = 0, chkEnabled.Checked, 1)
            'objOppBizDoc.boolDefault = IIf(TemplateType = 0, chkDefault.Checked, 1)
            'objOppBizDoc.TemplateName = IIf(TemplateType = 0, txtTemplateName.Text, "")
            'objOppBizDoc.strText = Server.HtmlEncode(RadEditor1.Content)
            'objOppBizDoc.BizDocCSS = Server.HtmlEncode(txtCSS.Text.Trim())

            'objOppBizDoc.BizDocTemplateID = BizDocTempId

            ''  objOppBizDoc.ManageBizDocTemplate()

            'If (TemplateType = 0) Then
            '    Response.Redirect("../Admin/frmBizDocTemplateList.aspx")
            'End If
        Catch ex As Exception
            If ex.Message.Contains("BIZDOC_RELATIONSHIP_PROFILE_CLASS_EXISTS") Then
                litMessage.Text = "Bizdoc with same relationship and pofile or class is already exists."
            Else
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(Convert.ToString(ex))
            End If
        End Try
    End Sub

    Protected Sub SaveBizTemplate()
        Try

            Dim objOppBizDoc As New OppBizDocs
            objOppBizDoc.DomainID = Session("DomainID")

            objOppBizDoc.BizDocId = IIf(TemplateType = 0, ddlBizDoc.SelectedValue, 0)
            objOppBizDoc.OppType = IIf(TemplateType = 0, ddlOppType.SelectedValue, 0)
            objOppBizDoc.TemplateType = TemplateType
            objOppBizDoc.byteMode = CCommon.ToShort(IIf(TemplateType = 0, chkEnabled.Checked, 1))
            objOppBizDoc.boolDefault = IIf(TemplateType = 0, chkDefault.Checked, 1)
            objOppBizDoc.TemplateName = IIf(TemplateType = 0, txtTemplateName.Text, "")
            objOppBizDoc.strText = Server.HtmlEncode(RadEditor1.Content)
            objOppBizDoc.BizDocCSS = Server.HtmlEncode(txtCSS.Text.Trim())
            objOppBizDoc.numOrientation = IIf(rdbPortrait.Checked, 1, 2)
            objOppBizDoc.bitKeepFooterBottom = chkKeepFooterBottom.Checked
            objOppBizDoc.BizDocTemplateID = BizDocTempId
            objOppBizDoc.Relationship = ddlRelationship.SelectedValue
            objOppBizDoc.Profile = ddlProfile.SelectedValue
            objOppBizDoc.AccountClass = ddlClass.SelectedValue
            objOppBizDoc.bitDisplayKitChild = chkDisplayKitChild.Checked

            objOppBizDoc.ManageBizDocTemplate()

            'If (TemplateType = 0) Then
            '    Response.Redirect("../Admin/frmBizDocTemplateList.aspx", False)
            'End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    'End of Code By Sachin

    Private Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            If (TemplateType = 0) Then
                Response.Redirect("../Admin/frmBizDocTemplateList.aspx")
            Else
                Response.Redirect("../admin/frmDomainDetails.aspx", False)
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(Convert.ToString(ex))
        End Try
    End Sub

    Private Sub BindCustomField()
        Try
            Dim ddl As EditorDropDown = CType(RadEditor1.FindTool("CustomField"), EditorDropDown)
            ddl.Items.Clear()
            Dim dtCustFld As DataTable
            Dim objCusField As New CustomFields()
            objCusField.DomainID = Session("DomainID")
            objCusField.locId = IIf(ddlOppType.SelectedValue = 1, 2, 6)
            dtCustFld = objCusField.CustomFieldList
            For Each row As DataRow In dtCustFld.Rows
                ddl.Items.Add(row("fld_label"), "#" & row("fld_label").ToString.Trim.Replace(" ", "") & "#")
            Next
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub BindCustomerStatementFields()
        Try
            Dim ddl As EditorDropDown = CType(RadEditor1.FindTool("BizDocElement"), EditorDropDown)
            ddl.Items.Clear()

            'Dim objFormWizard As New FormConfigWizard
            'Dim dtFields As New DataTable
            'objFormWizard.DomainID = Session("DomainID")
            'objFormWizard.FormID = 126
            'objFormWizard.AuthenticationGroupID = 0
            'objFormWizard.BizDocTemplateID = 0
            'dtFields = objFormWizard.getFieldList("")

            'Dim dvFilter As DataView
            'dvFilter = dtFields.DefaultView
            'dvFilter.RowFilter = "vcLookBackTableName = ''"
            'dtFields = dvFilter.ToTable()

            'For Each row As DataRow In dtFields.Rows
            '    ddl.Items.Add(row("vcFormFieldName"), "#" & row("vcFormFieldName").ToString.Trim.Replace(" ", "") & "#")
            'Next

            'Add Manually Fields for Organization, Address, Logo, Currency
            ddl.Items.Add("Currency", "#Currency#")
            ddl.Items.Add("Logo", "#Logo#")

            ddl.Items.Add("Customer/Vendor Organization Name", "#Customer/VendorOrganizationName#")
            ddl.Items.Add("Customer/Vendor Organization Phone", "#Customer/VendorOrganizationPhone#")

            ddl.Items.Add("Customer/Vendor Organization Contact Name", "#Customer/VendorOrganizationContactName#")
            ddl.Items.Add("Customer/Vendor Organization Contact Phone", "#Customer/VendorOrganizationContactPhone#")
            ddl.Items.Add("Customer/Vendor Organization Contact Email", "#Customer/VendorOrganizationContactEmail#")

            ddl.Items.Add("Customer/Vendor Organization Bill To Street", "#Customer/VendorBillToStreet#")
            ddl.Items.Add("Customer/Vendor Organization Bill To City", "#Customer/VendorBillToCity#")
            ddl.Items.Add("Customer/Vendor Organization Bill To Postal", "#Customer/VendorBillToPostal#")
            ddl.Items.Add("Customer/Vendor Organization Bill To State", "#Customer/VendorBillToState#")
            ddl.Items.Add("Customer/Vendor Organization Bill To Country", "#Customer/VendorBillToCountry#")

            ddl.Items.Add("Statement Date", "#StatementDate#")
            ddl.Items.Add("Balance Due", "#BalanceDue#")
            ddl.Items.Add("Today-30(Current Amt Due)", "#Today-30(CurrentAmtDue)#")
            ddl.Items.Add("31-60(Current Amt Due)", "#31-60(CurrentAmtDue)#")
            ddl.Items.Add("61-90(Current Amt Due)", "#61-90(CurrentAmtDue)#")
            ddl.Items.Add("Over 91(Current Amt Due)", "#Over91(CurrentAmtDue)#")
            ddl.Items.Add("Today-30(Past Due)", "#Today-30(PastDue)#")
            ddl.Items.Add("31-60(Past Due)", "#61-90(PastDue)#")
            ddl.Items.Add("61-90(Past Due)", "#61-90(PastDue)#")
            ddl.Items.Add("Over 91(Past Due)", "#Over91(PastDue)#")

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub BindData()
        Try
            Dim objOppBizDoc As New OppBizDocs
            objOppBizDoc.DomainID = Session("DomainID")
            objOppBizDoc.TemplateType = TemplateType
            'Added by Neelam Kapila || 09/29/2017 - Added function to show BizDocId 
            objOppBizDoc.BizDocTemplateID = BizDocTempId

            Dim dt As DataTable = objOppBizDoc.GetBizDocTemplate()
            If dt.Rows.Count > 0 Then
                RadEditor1.Content = HttpUtility.HtmlDecode(CCommon.ToString(dt.Rows(0)("txtBizDocTemplate")))
                txtCSS.Text = HttpUtility.HtmlDecode(CCommon.ToString(dt.Rows(0)("txtCSS")))
                chkEnabled.Checked = dt.Rows(0)("bitEnabled")
                chkDefault.Checked = dt.Rows(0)("bitDefault")
                txtTemplateName.Text = dt.Rows(0)("vcTemplateName")
                ddlRelationship.SelectedValue = dt.Rows(0)("numRelationship")
                ddlClass.SelectedValue = dt.Rows(0)("numAccountClass")
                ddlProfile.SelectedValue = dt.Rows(0)("numProfile")
                chkDisplayKitChild.Checked = CCommon.ToBool(dt.Rows(0)("bitDisplayKitChild"))

                If TemplateType = 0 Then
                    If ddlBizDoc.Items.FindByValue(dt.Rows(0)("numBizDocID")) IsNot Nothing Then
                        ddlBizDoc.ClearSelection()
                        ddlBizDoc.Items.FindByValue(dt.Rows(0)("numBizDocID")).Selected = True
                    End If

                    If ddlOppType.Items.FindByValue(dt.Rows(0)("numOppType")) IsNot Nothing Then
                        ddlOppType.ClearSelection()
                        ddlOppType.Items.FindByValue(dt.Rows(0)("numOppType")).Selected = True
                    End If
                    'Added by sachin


                    'ddlBizDoc.Enabled = False
                    'ddlOppType.Enabled = False
                    'Get count of attached doc count
                    Dim objBizDocs As New OppBizDocs
                    objBizDocs.BizDocId = ddlBizDoc.SelectedValue
                    objBizDocs.DomainID = Session("DomainID")
                    objBizDocs.BizDocTemplateID = BizDocTempId
                    hdfTemplateID.Value = BizDocTempId
                    hdfTemplateType.Value = TemplateType
                    hdfTemplateName.Value = ddlBizDoc.SelectedItem.Text
                    hplBizDoc.Text = " following documents(" & objBizDocs.GetBizDocAttachments.Rows.Count & ")"

                    If CCommon.ToLong(dt.Rows(0)("numOrientation")) = 1 Then
                        rdbPortrait.Checked = True
                    ElseIf CCommon.ToLong(dt.Rows(0)("numOrientation")) = 2 Then
                        rdbLandscape.Checked = True
                    End If

                    chkKeepFooterBottom.Checked = CCommon.ToBool(dt.Rows(0)("bitKeepFooterBottom"))

                    If CCommon.ToLong(ddlBizDoc.SelectedValue) = enmBizDocTemplate_BizDocID.CustomerStatement Then
                        BindCustomerStatementFields()
                    End If

                End If
            ElseIf TemplateType = 0 Then
                'read from file
                'Dim objReader As StreamReader
                'objReader = New StreamReader(ConfigurationManager.AppSettings("BACRMLocation").ToString() & "\admin\BizDocTemplate.htm")
                'strContents = objReader.ReadToEnd()
                'objReader.Close()

                Dim strContents As String
                If ddlBizDoc.SelectedValue = enmBizDocTemplate_BizDocID.Packing_Slip_Template Then
                    strContents = BACRM.BusinessLogic.ShioppingCart.Sites.ReadFile(ConfigurationManager.AppSettings("BACRMLocation").ToString() & "\admin\PackingListTemplate.htm")
                ElseIf ddlOppType.SelectedValue = 1 Then
                    strContents = BACRM.BusinessLogic.ShioppingCart.Sites.ReadFile(ConfigurationManager.AppSettings("BACRMLocation").ToString() & "\admin\SalesBizDocTemplate.htm")

                ElseIf ddlOppType.SelectedValue = 2 Then

                    strContents = BACRM.BusinessLogic.ShioppingCart.Sites.ReadFile(ConfigurationManager.AppSettings("BACRMLocation").ToString() & "\admin\PurchaseBizDocTemplate.htm")
                End If

                RadEditor1.Content = Server.HtmlDecode(strContents)

                txtCSS.Text = BACRM.BusinessLogic.ShioppingCart.Sites.ReadFile(ConfigurationManager.AppSettings("BACRMLocation").ToString() & "\admin\BizDocTemplateDefaultCSS.css")
                '  " .title{color: #696969; /*padding:10px;*/font: bold 30px Arial, Helvetica, sans-serif;}" & Environment.NewLine & _
                '".RowHeader{background-color: #dedede;color: #333;font-family: Arial;font-size: 8pt;}" & Environment.NewLine & _
                '".RowHeader.hyperlink{color: #333;}" & Environment.NewLine & _
                '".ItemHeader, .ItemStyle, .AltItemStyle{border-width: 1px;padding: 8px;font: normal 12px/17px arial;border-style: solid;border-color: #666666;background-color: #dedede;}" & Environment.NewLine & _
                '".ItemHeader{border-width: 1px;padding: 8px;border-style: solid;border-color: #666666;}" & Environment.NewLine & _
                '".ItemStyle td{border-width: 1px;padding: 8px;border-style: solid;border-color: #666666;background-color: #ffffff;}" & Environment.NewLine & _
                '".AltItemStyle{background-color: White;border-color: black;padding: 8px;}" & Environment.NewLine & _
                '".AltItemStyle td{padding: 8px;}" & Environment.NewLine & _
                '".ItemHeader td{border-width: 1px;padding: 8px;font-weight: bold;border-style: solid;border-color: #666666;background-color: #dedede;}" & Environment.NewLine & _
                '".ItemHeader td th{border-width: 1px;padding: 8px;border-style: solid;border-color: #666666;background-color: #dedede;}" & Environment.NewLine & _
                '"#tblBizDocSumm{font: normal 12px verdana;color: #333;border-width: 1px;border-color: #666666;border-collapse: collapse;background-color: #dedede;}" & Environment.NewLine & _
                '"#tblBizDocSumm td{border-width: 1px;padding: 8px;border-style: solid;border-color: #666666;}" & Environment.NewLine & _
                '".WordWrapSerialNo{width: 30%;word-break: break-all;}"

                chkEnabled.Checked = False
                chkDefault.Checked = False
            End If

            If TemplateType = 0 Then
                BindCustomField()
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub BindBizDoc()
        Try
            objCommon.DomainID = Session("DomainID")
            objCommon.BizDocType = ddlOppType.SelectedValue

            ddlBizDoc.DataSource = objCommon.GetBizDocType
            ddlBizDoc.DataTextField = "vcData"
            ddlBizDoc.DataValueField = "numListItemID"
            ddlBizDoc.DataBind()
            'Added by Neelam Kapila || 09/30/2017 - Added function to select BizDoc Type 

            ddlBizDocType.DataSource = objCommon.GetBizDocType
            ddlBizDocType.DataTextField = "vcData"
            ddlBizDocType.DataValueField = "numListItemID"
            ddlBizDocType.DataBind()

        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    'Added by Neelam Kapila || 09/30/2017 - Added function to show BizDoc Template
    Sub BindBizDocsTemplate()
        Try
            Dim objOppBizDoc As New OppBizDocs
            objOppBizDoc.DomainID = Session("DomainID")
            objOppBizDoc.BizDocId = ddlBizDocType.SelectedValue
            objOppBizDoc.OppType = ddlOppType.SelectedValue

            Dim dtBizDocTemplate As DataTable = objOppBizDoc.GetBizDocTemplateList()

            ddlBizDocTemplate.DataSource = dtBizDocTemplate
            ddlBizDocTemplate.DataTextField = "vcTemplateName"
            ddlBizDocTemplate.DataValueField = "numBizDocTempID"
            ddlBizDocTemplate.DataBind()

            ddlBizDocTemplate.Items.Insert(0, New ListItem("--Select--", 0))
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub ddlOppType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlOppType.SelectedIndexChanged
        Try
            BindBizDoc()
            BindData()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(Convert.ToString(ex))
        End Try
    End Sub

    'Added By:Sachin Sadhu||Date:23rdDec2013
    'Purpose:To generate Dynamic preview of BizDoc

    Private Sub ibtnPreview_OnClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles ibtnPreview.Click
        Session("editorPreview") = RadEditor1.Content
        Session("editorPreviewCss") = txtCSS.Text

        Page.ClientScript.RegisterStartupScript(Me.GetType(), "MyKey", "OpenBizInvoice();", True)




    End Sub

    'End of code by Sachin

    'Added By:Sachin Sadhu||Date:11thmarch2014
    'Purpose:To prompt user on Insertion of  duplicate records of Default templates

    Protected Sub chkDefault_OnCheckedChanged(sender As [Object], e As EventArgs)

        Try



            Dim objOppBizDoc As New OppBizDocs
            objOppBizDoc.DomainID = Session("DomainID")
            objOppBizDoc.BizDocId = IIf(TemplateType = 0, ddlBizDoc.SelectedValue, 0)
            objOppBizDoc.OppType = IIf(TemplateType = 0, ddlOppType.SelectedValue, 0)
            objOppBizDoc.TemplateType = TemplateType

            Dim dt As DataTable = objOppBizDoc.GetDefautlBizDocTemplateList()


            If BizDocTempId = 0 Then 'On Creation of New
                If chkDefault.Checked Then
                    If dt.Rows.Count > 0 Then

                        ScriptManager.RegisterStartupScript(Me.Page, Me.Page.GetType(), "script", "<script type='text/javascript'> ShowMessage();</script>", False)

                    Else
                        'SaveBizTemplate()
                    End If
                Else
                    'SaveBizTemplate()
                End If
            Else 'On Update
                'SaveBizTemplate()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(Convert.ToString(ex))
        End Try
    End Sub
    'End of Addition by Sachin Sadhu

    Private Sub FillClass()
        Try
            Dim dtClass As DataTable
            Dim objAdmin As New CAdmin
            objAdmin.DomainID = Session("DomainID")
            objAdmin.Mode = 1
            dtClass = objAdmin.GetClass()

            If dtClass.Rows.Count > 0 Then
                ddlClass.DataTextField = "ClassName"
                ddlClass.DataValueField = "numChildClassID"
                ddlClass.DataSource = dtClass
                ddlClass.DataBind()
            End If

            ddlClass.Items.Insert(0, New ListItem("--Select One --", "0"))
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub DisplayError(ByVal ex As String)
        Try
            DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ex
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Focus()
        Catch exception As Exception

        End Try
    End Sub

    'Added by Neelam Kapila || 09/29/2017 - Added functionality to open BizDoc to see what design change looks Like

    Private Sub btnFetchOppId_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnFetchOppId.Click
        Try
            Dim OpID, OppBizId As Long
            Dim objOppBizDoc As New OppBizDocs
            objOppBizDoc.BizDocTemplateID = BizDocTempId
            Dim dt As DataTable = objOppBizDoc.GetOpportunityModifiedBizDocs()
            If dt.Rows.Count > 0 Then
                OpID = CCommon.ToLong(dt.Rows(0).Item("numOppID"))
                OppBizId = CCommon.ToLong(dt.Rows(0).Item("numOppBizDocsId"))

                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, Guid.NewGuid().ToString(), "<script>window.open('../opportunity/frmBizInvoice.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&OpID=" + Convert.ToString(OpID) + "&OppBizId=" + Convert.ToString(OppBizId) + "&Print=0', '', 'toolbar=no,menubar=yes,titlebar=no,left=100,top=15,width=850,height=700,scrollbars=yes,resizable=yes');</script>", False)
            Else
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "NoData", "alert('There are no associated BizDoc templates for this entity.');", True)
            End If
        Catch ex As Exception
            If ex.Message.Contains("BIZDOC_RELATIONSHIP_PROFILE_CLASS_EXISTS") Then
                litMessage.Text = "Bizdoc with same relationship and pofile or class is already exists."
            Else
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(Convert.ToString(ex))
            End If
        End Try
    End Sub

    Private Sub ddlBizDocType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlBizDocType.SelectedIndexChanged
        Try
            BindBizDocsTemplate()

            If ddlBizDocTemplate.Items.Count = 0 Then
                lblError.Text = "First create BizDoc Template for selected BizDoc."
            Else
                lblError.Text = ""
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Private Sub ddlBizDocTemplate_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlBizDocTemplate.SelectedIndexChanged
        Try
            If CCommon.ToLong(ddlBizDocTemplate.SelectedValue) > 0 Then
                divBizDocID.Visible = True
                lblBizDocId1.Text = ddlBizDocTemplate.SelectedValue

                Dim objOppBizDoc As New OppBizDocs
                objOppBizDoc.DomainID = Session("DomainID")
                objOppBizDoc.TemplateType = 0
                objOppBizDoc.BizDocTemplateID = ddlBizDocTemplate.SelectedValue
                Dim dt As DataTable = objOppBizDoc.GetBizDocTemplate()
                If Not dt Is Nothing AndAlso dt.Rows.Count > 0 Then
                    RadEditor1.Content = HttpUtility.HtmlDecode(CCommon.ToString(dt.Rows(0)("txtBizDocTemplate")))
                    txtCSS.Text = HttpUtility.HtmlDecode(CCommon.ToString(dt.Rows(0)("txtCSS")))
                End If
            Else
                divBizDocID.Visible = False
                lblBizDocId1.Text = ""
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

End Class