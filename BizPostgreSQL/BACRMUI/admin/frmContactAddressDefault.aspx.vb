﻿Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Common
Public Class frmContactAddressDefault
    Inherits BACRMPage
    Dim objCommon As CCommon

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then
                BindData()
            End If

            If IsPostBack Then

            End If
            btnCancel.Attributes.Add("onclick", "return Close()")
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
    Sub BindData()
        Try
            Dim dtTable As DataTable
            Dim objUserAccess As New UserAccess
            objUserAccess.DomainID = Session("DomainID")
            dtTable = objUserAccess.GetDomainDetails()
            If dtTable.Rows(0).Item("bitAutoPopulateAddress") = True Then
                chkPrimaryAddress.Checked = True
                If Not ddlAddress.Items.FindByValue(dtTable.Rows(0).Item("tintPoulateAddressTo")) Is Nothing Then
                    ddlAddress.Items.FindByValue(dtTable.Rows(0).Item("tintPoulateAddressTo")).Selected = True
                End If
            Else : chkPrimaryAddress.Checked = False
                ddlAddress.SelectedIndex = -1
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If Page.IsValid Then
            Try
                save()
                BindData()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End If
    End Sub

    Private Sub btnSaveClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveClose.Click
        If Page.IsValid Then
            Try
                save()
                Dim strScript As String = "<script language=JavaScript>self.close()</script>"
                If (Not Page.IsStartupScriptRegistered("clientScript")) Then Page.RegisterStartupScript("clientScript", strScript)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End If
    End Sub

    Private Sub save()
        Try
            Dim objUserAccess As New UserAccess
            objUserAccess.DomainID = Session("DomainID")


            If chkPrimaryAddress.Checked Then
                objUserAccess.boolAutoPopulateAddress = True
                objUserAccess.PoulateAddressTo = ddlAddress.SelectedValue
            End If
            objUserAccess.UpdateDomainDefaultAddress()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub



End Class