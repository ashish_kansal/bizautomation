﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmRelationshipMapping.aspx.vb"
    Inherits=".frmRelationshipMapping" MasterPageFile="~/common/Popup.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Accounting Relationship Mapping</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <div class="input-part">
        <div class="right-input">
            <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save"></asp:Button>&nbsp;
            <asp:Button ID="btnClose" runat="server" CssClass="button" Text="Close" OnClientClick="return Close();">
            </asp:Button>&nbsp;&nbsp;
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageTitle" runat="server" ClientIDMode="Static">
    Accounting Relationship Mapping
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="Content" runat="server" ClientIDMode="Static">
    <table width="600px">
        <tr>
            <td class="normal4" align="center" colspan="2">
                <asp:Literal ID="litMessage" runat="server"></asp:Literal>
            </td>
        </tr>
        <tr>
            <td class="normal1" align="right">
                Relationship
            </td>
            <td>
                <asp:DropDownList runat="server" ID="ddlRelationship" CssClass="signup" Width="350"
                    AutoPostBack="true">
                </asp:DropDownList>
            </td>
        </tr>
        <asp:Panel runat="server" ID="pnlAutoCreate">
            <tr>
                <td class="normal1" align="left" colspan="2">
                    <asp:RadioButton ID="rbAutoCreate" GroupName="Account" Style="font: 12px 'Segoe UI',Arial,sans-serif"
                        Text="Auto create new chart of account under selected account type ?" runat="server"
                        CssClass="signup" />
                </td>
            </tr>
            <tr>
                <td class="normal1" align="right">
                    Parent Type A/R
                </td>
                <td class="normal1">
                    <asp:DropDownList ID="ddlARAccountType" runat="server" CssClass="signup" Width="350">
                    </asp:DropDownList>
                    &nbsp;
                    <asp:Label ID="lblARAccountName" runat="server" Text=""></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="normal1" align="right">
                    Parent Type A/P
                </td>
                <td class="normal1">
                    <asp:DropDownList ID="ddlAPAccountType" runat="server" CssClass="signup" Width="350">
                    </asp:DropDownList>
                    &nbsp;
                    <asp:Label ID="lblAPAccountName" runat="server" Text=""></asp:Label>
                </td>
            </tr>
        </asp:Panel>
        <tr>
            <td class="normal1" align="left" colspan="2">
                <asp:RadioButton ID="rbManuall" GroupName="Account" Style="font: 12px 'Segoe UI',Arial,sans-serif"
                    Text="Select chart of account from existing accounts" runat="server" CssClass="signup" />
            </td>
        </tr>
        <tr>
            <td class="normal1" align="right">
                A/R Account
            </td>
            <td class="normal1">
                <asp:DropDownList ID="ddlARAccount" runat="server" CssClass="signup" Width="350">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="normal1" align="right">
                A/P Account
            </td>
            <td class="normal1">
                <asp:DropDownList ID="ddlAPAccount" runat="server" CssClass="signup" Width="350">
                </asp:DropDownList>
            </td>
        </tr>
    </table>
    <asp:HiddenField ID="hdnPrimaryKey" runat="server" />
    <asp:HiddenField ID="hdnARAccountTypeID" runat="server" />
    <asp:HiddenField ID="hdnAPAccountTypeID" runat="server" />
    <asp:HiddenField ID="hdnARAccountID" runat="server" />
    <asp:HiddenField ID="hdnAPAccountID" runat="server" />
</asp:Content>
