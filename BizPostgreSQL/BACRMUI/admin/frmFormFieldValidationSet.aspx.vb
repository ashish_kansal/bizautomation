﻿Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Admin

Namespace BACRM.UserInterface.Admin
    Public Class frmFormFieldValidationSet
        Inherits BACRMPage

        Dim lngFldID, lngFormID, lngFormType As Long
        Public Const strFieldMessage = "Please enter a value of {$FieldName}."
    
        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            lngFormID = CCommon.ToLong(GetQueryStringVal("FormId"))
            lngFldID = CCommon.ToLong(GetQueryStringVal("fldid"))
            lngFormType = CCommon.ToLong(GetQueryStringVal("FType"))
            Try
                If Not IsPostBack Then
                    txtMax.Attributes.Add("onkeypress", "CheckNumber(2,event)")
                    txtMin.Attributes.Add("onkeypress", "CheckNumber(2,event)")
                    BindData()
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub
        Private Sub BindData()
            Try
                Dim dtFldDetails As DataTable
                Dim objField As New FormConfigWizard()

                objField.DomainID = Session("DomainID")
                objField.FormID = lngFormID
                objField.FormFieldId = lngFldID

                dtFldDetails = objField.GetFormFieldValidationDetails

                If dtFldDetails.Rows.Count > 0 Then

                    lblFormFieldName.Text = "Field Name : " & dtFldDetails.Rows(0)("vcFormFieldName")
                    lblAssociatedControlType.Text = "Field Type : " & dtFldDetails.Rows(0)("vcAssociatedControlType")

                    txtToolTip.Text = dtFldDetails.Rows(0)("vcToolTip")

                    If dtFldDetails.Rows(0)("vcAssociatedControlType") = "SelectBox" Or dtFldDetails.Rows(0)("vcAssociatedControlType") = "DateField" Or lngFormType = 0 Then
                        trTextboxValidation.Visible = False
                    End If

                    chkIsRequired.Checked = CCommon.ToBool(dtFldDetails.Rows(0)("bitIsRequired"))
                    chkFieldMessage.Checked = CCommon.ToBool(dtFldDetails.Rows(0)("bitFieldMessage"))
                    'If chkFieldMessage.Checked Then
                    '    txtFieldMessage.Text = dtFldDetails.Rows(0)("vcFieldMessage")
                    'End If
                    If CCommon.ToString(dtFldDetails.Rows(0)("vcFieldMessage")) = "" Then
                        txtFieldMessage.Text = strFieldMessage
                    Else
                        txtFieldMessage.Text = dtFldDetails.Rows(0)("vcFieldMessage")
                    End If
                    If CCommon.ToBool(dtFldDetails.Rows(0)("bitIsNumeric")) Then
                        rbIsNumeric.Checked = True
                    ElseIf CCommon.ToBool(dtFldDetails.Rows(0)("bitIsAlphaNumeric")) Then
                        rbIsAlphaNumeric.Checked = True
                    ElseIf CCommon.ToBool(dtFldDetails.Rows(0)("bitIsEmail")) Then
                        rbIsEmail.Checked = True
                    ElseIf CCommon.ToBool(dtFldDetails.Rows(0)("bitIsLengthValidation")) Then
                        rbRange.Checked = True
                        txtMin.Text = dtFldDetails.Rows(0)("intMinLength")
                        txtMax.Text = dtFldDetails.Rows(0)("intMaxLength")
                    Else
                        rbNone.Checked = True
                    End If

                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSave.Click
            Try
                Dim strFormFieldID As String

                Dim dr As DataRow
                Dim dtFieldDetails As New DataTable
                dtFieldDetails.Columns.Add("bitIsRequired")
                dtFieldDetails.Columns.Add("bitIsNumeric")
                dtFieldDetails.Columns.Add("bitIsAlphaNumeric")
                dtFieldDetails.Columns.Add("bitIsEmail")
                dtFieldDetails.Columns.Add("bitIsLengthValidation")
                dtFieldDetails.Columns.Add("intMaxLength")
                dtFieldDetails.Columns.Add("intMinLength")
                dtFieldDetails.Columns.Add("bitFieldMessage")
                dtFieldDetails.Columns.Add("vcFieldMessage")
                dtFieldDetails.Columns.Add("vcToolTip")

                dtFieldDetails.TableName = "Table"


                dr = dtFieldDetails.NewRow
                dr("bitIsRequired") = IIf(chkIsRequired.Checked, "1", "0")
                dr("bitIsNumeric") = IIf(rbIsNumeric.Checked, "1", "0")
                dr("bitIsAlphaNumeric") = IIf(rbIsAlphaNumeric.Checked, "1", "0")
                dr("bitIsEmail") = IIf(rbIsEmail.Checked, "1", "0")
                dr("bitIsLengthValidation") = IIf(rbRange.Checked, "1", "0")
                dr("intMaxLength") = CCommon.ToInteger(txtMax.Text)
                dr("intMinLength") = CCommon.ToInteger(txtMin.Text)
                dr("bitFieldMessage") = IIf(chkFieldMessage.Checked, "1", "0")
                dr("vcFieldMessage") = CCommon.ToString(txtFieldMessage.Text)
                dr("vcToolTip") = CCommon.ToString(txtToolTip.Text)

                dtFieldDetails.Rows.Add(dr)


                Dim ds As New DataSet
                ds.Tables.Add(dtFieldDetails)
                strFormFieldID = ds.GetXml()
                ds.Tables.Remove(dtFieldDetails)

                Dim objField As New FormConfigWizard()

                objField.DomainID = Session("DomainID")
                objField.FormID = lngFormID
                objField.FormFieldId = lngFldID
                objField.strFormFieldID = strFormFieldID
                objField.ManageFormFieldValidation()

                Response.Write("<script>self.close();</script>")
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub
    End Class
End Namespace