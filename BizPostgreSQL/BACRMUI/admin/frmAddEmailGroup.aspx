<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmAddEmailGroup.aspx.vb"
    Inherits="BACRM.UserInterface.Admin.frmAddEmailGroup" MasterPageFile="~/common/Popup.Master" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Add Email Group</title>
    <script language="Javascript">

        function Save(a) {
            if (a == 1) {

                if (document.getElementById("ddlGroup").value == "0") {
                    alert("Select Group ")
                    document.getElementById("ddlGroup").focus()
                    return false;
                }
            }
            else
                if (document.getElementById("txtGroupName").value == "") {
                    alert("Enter Group Name ")
                    document.getElementById("txtGroupName").focus()
                    return false;
                }

        }		
    </script>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageTitle" runat="server" ClientIDMode="Static">
    Add Email Group
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="Content" runat="server" ClientIDMode="Static">
    <asp:ScriptManager EnablePartialRendering="true" runat="server" ID="scriptManager1"
        AllowCustomErrorsRedirect="true">
    </asp:ScriptManager>
    <table>
        <td width="100%">
        </td>
        <td align="center">
            <asp:UpdateProgress ID="up1" runat="server" DynamicLayout="false">
                <ProgressTemplate>
                    <asp:Image ID="Image1" ImageUrl="~/images/updating.gif" runat="server" />
                </ProgressTemplate>
            </asp:UpdateProgress>
        </td>
    </table>
    <asp:UpdatePanel ID="updatepanel1" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="true">
        <ContentTemplate>
            <table width="600px" border="0">
                <tr>
                    <td align="right" class="normal1" colspan="2">
                        <asp:Button ID="btnSaveGroup" CssClass="button" Text="Save In New Group" runat="server"
                            Visible="false"></asp:Button>
                        <asp:Button ID="btnSaveGroupExist" CssClass="button" Text="Save In Existing Group"
                            runat="server" Visible="false"></asp:Button>
                        <asp:Button ID="btnSaveClose" runat="server" Text="Save &amp; Close" CssClass="button">
                        </asp:Button>
                        <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="button"></asp:Button>
                    </td>
                </tr>
                <tr>
                    <td align="center" colspan="2">
                        <asp:Label ID="lblRecordCount" runat="server" Visible="false"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table>
                            <tr>
                                <td>
                                   <span style="float:left"> <asp:RadioButton ID="rdbExisting" runat="server" Text="Save In Existing Group :"
                                        CssClass="normal1" Font-Bold="true" GroupName="Group" OnCheckedChanged="rdbNew_CheckedChanged"
                                        Checked="true" AutoPostBack="true" />
                                    <asp:Label ID="labelGroup" runat="server" Text="Select Group" CssClass="normal1"></asp:Label></span>
                                    <asp:DropDownList ID="ddlGroup" runat="server" CssClass="signup" Width="200" AutoPostBack="true">
                                    </asp:DropDownList>
                                    <asp:Button ID="btnDeleteGroup" runat="server" Text="Delete Group" CssClass="button">
                                    </asp:Button>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                     <span style="float:left">  <asp:RadioButton ID="rdbNew" runat="server" Text="Save in New Group :" CssClass="normal1"
                                        Font-Bold="true" GroupName="Group" OnCheckedChanged="rdbNew_CheckedChanged" AutoPostBack="true" />
                                 <asp:Label ID="labelNewGroup" runat="server" Text="Enter Group Name" CssClass="normal1"></asp:Label></span>
                                    <asp:TextBox ID="txtGroupName" runat="server" CssClass="signup"></asp:TextBox>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td id="hidenav" nowrap align="right" runat="server">
                        <table>
                            <tr>
                                <td>
                                    <asp:Label ID="lblNext" runat="server" CssClass="Text_bold">Next:</asp:Label>
                                </td>
                                <td class="normal1">
                                    <asp:LinkButton ID="lnk2" runat="server">2</asp:LinkButton>
                                </td>
                                <td class="normal1">
                                    <asp:LinkButton ID="lnk3" runat="server">3</asp:LinkButton>
                                </td>
                                <td class="normal1">
                                    <asp:LinkButton ID="lnk4" runat="server">4</asp:LinkButton>
                                </td>
                                <td class="normal1">
                                    <asp:LinkButton ID="lnk5" runat="server">5</asp:LinkButton>
                                </td>
                                <td>
                                    <asp:LinkButton ID="lnkFirst" runat="server"><div class="LinkArrow"><<</div>
                                    </asp:LinkButton>
                                </td>
                                <td>
                                    <asp:LinkButton ID="lnkPrevious" runat="server"><div class="LinkArrow"><</div>
                                    </asp:LinkButton>
                                </td>
                                <td class="normal1">
                                    <asp:Label ID="lblPage" runat="server">Page</asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtCurrrentPage" runat="server" CssClass="signup" Width="28px" AutoPostBack="true"
                                        MaxLength="5"></asp:TextBox>
                                </td>
                                <td class="normal1">
                                    <asp:Label ID="lblOf" runat="server">of</asp:Label>
                                </td>
                                <td class="normal1">
                                    <asp:Label ID="lblTotal" runat="server"></asp:Label>
                                </td>
                                <td>
                                    <asp:LinkButton ID="lnkNext" runat="server" CssClass="LinkArrow"><div class="LinkArrow">></div>
                                    </asp:LinkButton>
                                </td>
                                <td>
                                    <asp:LinkButton ID="lnkLast" runat="server"><div class="LinkArrow">>></div>
                                    </asp:LinkButton>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <td colspan="12" align="right">
                                        <asp:Button ID="btnRemove" CssClass="button" Text="Remove from the list" runat="server">
                                        </asp:Button>
                                    </td>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <hr />
                        <strong>List of Email</strong>
                        <asp:Table ID="tblAddEmailGroup" runat="server" CellPadding="0" CellSpacing="0" Width="100%"
                            GridLines="None" CssClass="aspTable" BorderColor="black" BorderWidth="1" Height="300">
                            <asp:TableRow>
                                <asp:TableCell VerticalAlign="Top">
                                    <asp:DataGrid ID="dgAddEmailGroup" CellPadding="0" CellSpacing="0" runat="server"
                                        CssClass="dg" AutoGenerateColumns="false" Width="100%">
                                        <AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
                                        <ItemStyle CssClass="is"></ItemStyle>
                                        <HeaderStyle CssClass="hs"></HeaderStyle>
                                        <Columns>
                                            <asp:BoundColumn Visible="false" DataField="numContactId"></asp:BoundColumn>
                                            <asp:TemplateColumn HeaderText="Company Name">
                                                <HeaderTemplate>
                                                    Company Name
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <%#DataBinder.Eval(Container.DataItem, "vcCompanyName")%>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="First Name">
                                                <HeaderTemplate>
                                                    First Name
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <%# DataBinder.Eval(Container.DataItem, "vcFirstName") %>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="Last Name">
                                                <HeaderTemplate>
                                                    Last Name
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <%# DataBinder.Eval(Container.DataItem, "vcLastName") %>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="Email Id">
                                                <HeaderTemplate>
                                                    Email Id
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <%#DataBinder.Eval(Container.DataItem, "vcEmail")%>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="Delete">
                                                <HeaderTemplate>
                                                    Delete
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chkDelete" runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                        </Columns>
                                    </asp:DataGrid>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>&nbsp</asp:TableCell></asp:TableRow>
                        </asp:Table>
                    </td>
                </tr>
            </table>
            <asp:TextBox ID="txtColumnName" runat="server" Style="display: none"></asp:TextBox>
            <asp:TextBox ID="txtTotalPage" Style="display: none" runat="server"></asp:TextBox>
            <asp:TextBox ID="txtTotalRecords" Style="display: none" runat="server"></asp:TextBox>
            <asp:TextBox ID="txtSortChar" Style="display: none" runat="server"></asp:TextBox>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnSaveClose" />
            <asp:PostBackTrigger ControlID="btnClose" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
