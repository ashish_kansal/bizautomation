'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On

Namespace BACRM.UserInterface.Admin

    Partial Public Class frmAdvSurvey

        '''<summary>
        '''btnSearch control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents btnSearch As Global.System.Web.UI.WebControls.LinkButton

        '''<summary>
        '''UpdateProgress control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents UpdateProgress As Global.System.Web.UI.UpdateProgress

        '''<summary>
        '''chkSurveyBet control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents chkSurveyBet As Global.System.Web.UI.WebControls.CheckBox

        '''<summary>
        '''txtRatingStart control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents txtRatingStart As Global.System.Web.UI.WebControls.TextBox

        '''<summary>
        '''txtRatingEnd control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents txtRatingEnd As Global.System.Web.UI.WebControls.TextBox

        '''<summary>
        '''ddlSurvey control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents ddlSurvey As Global.System.Web.UI.WebControls.DropDownList

        '''<summary>
        '''radAnd control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents radAnd As Global.System.Web.UI.WebControls.RadioButton

        '''<summary>
        '''radOr control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents radOr As Global.System.Web.UI.WebControls.RadioButton

        '''<summary>
        '''ddlQuestion1 control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents ddlQuestion1 As Global.System.Web.UI.WebControls.DropDownList

        '''<summary>
        '''tbQ1 control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents tbQ1 As Global.System.Web.UI.HtmlControls.HtmlTable

        '''<summary>
        '''ddlQuestion2 control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents ddlQuestion2 As Global.System.Web.UI.WebControls.DropDownList

        '''<summary>
        '''tbQ2 control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents tbQ2 As Global.System.Web.UI.HtmlControls.HtmlTable

        '''<summary>
        '''ddlQuestion3 control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents ddlQuestion3 As Global.System.Web.UI.WebControls.DropDownList

        '''<summary>
        '''tbQ3 control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents tbQ3 As Global.System.Web.UI.HtmlControls.HtmlTable

        '''<summary>
        '''ddlQuestion4 control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents ddlQuestion4 As Global.System.Web.UI.WebControls.DropDownList

        '''<summary>
        '''tbQ4 control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents tbQ4 As Global.System.Web.UI.HtmlControls.HtmlTable

        '''<summary>
        '''ddlQuestion5 control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents ddlQuestion5 As Global.System.Web.UI.WebControls.DropDownList

        '''<summary>
        '''tbQ5 control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents tbQ5 As Global.System.Web.UI.HtmlControls.HtmlTable

        '''<summary>
        '''frmAdvSearchCriteria1 control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents frmAdvSearchCriteria1 As Global.BACRM.UserInterface.Admin.frmAdvSearchCriteria

        '''<summary>
        '''hdnFilterCriteria control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents hdnFilterCriteria As Global.System.Web.UI.WebControls.HiddenField
    End Class
End Namespace
