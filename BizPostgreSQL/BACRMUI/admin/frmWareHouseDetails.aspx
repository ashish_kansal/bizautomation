<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmWareHouseDetails.aspx.vb" Inherits="BACRM.UserInterface.Admin.frmWareHouseDetails" %>
<%@ Register TagPrefix="menu1"  TagName="webmenu" src="../include/webmenu.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
   <link rel="stylesheet" href="~/CSS/master.css" type="text/css" />
    <title>WareHouse Details</title>
</head>
<body>
    <form id="form1" runat="server">
   <menu1:webmenu id="webmenu1" runat="server"></menu1:webmenu>
     <br />
			<table cellSpacing="0" cellPadding="0" width="100%" border="0">
				<tr>
					<td vAlign="bottom">
						<table class="TabStyle">
							<tr>
								<td>&nbsp;&nbsp;&nbsp;WareHouse Details&nbsp;&nbsp;&nbsp;
								</td>
							</tr>
						</table>
					</td>
					<td align="right">
					    <asp:button runat="server" ID="btnSave" Text="Save" CssClass="button" />
					</td>					
				</tr>
			</table> 
			<asp:UpdatePanel runat="server" ID="up1"  UpdateMode="Always">
			    <ContentTemplate>
        	        <asp:table id="tbl" BorderWidth="1" CellPadding="0" CellSpacing="0" Runat="server" Width="100%" CssClass="aspTable"
				        BorderColor="black" GridLines="None">
				        <asp:TableRow>
				            <asp:TableCell>
				                <br />
				                <table width="100%"  class="normal1">
				                    <tr>
				                        <td  align="right">
				                           Country&nbsp; 
				                        </td>
				                        <td align="left">
				                           <asp:DropDownList ID="ddlCountry" Width="200" AutoPostBack="true" runat="server" CssClass="signup"></asp:DropDownList>           
				                        </td>
				                    </tr>
				                    <tr>
				                        <td  align="right">
				                            State&nbsp; 
				                        </td>
				                        <td align="left">
				                            <asp:DropDownList ID="ddlState" Width="200" runat="server" CssClass="signup"></asp:DropDownList>
				                        </td>
				                    </tr>
				                    <tr>
				                        <td  align="right">
				                            WareHouse&nbsp;
				                        </td>
				                        <td align="left">
				                            <asp:DropDownList runat="server" ID="ddlWareHouse" CssClass="signup" Width="200"></asp:DropDownList>
				                        </td>
				                    </tr>
				                   
				                </table>				         
				            </asp:TableCell>				    
				        </asp:TableRow>			            
				        <asp:TableRow>
				            <asp:TableCell>
				                   <asp:datagrid id="dgwarehouse" runat="server"  CssClass="dg" Width="100%" BorderColor="white" AutoGenerateColumns="False">
								        <AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
								        <ItemStyle CssClass="is"></ItemStyle>
								        <HeaderStyle CssClass="hs"></HeaderStyle>
								        <Columns>								             
								             <asp:BoundColumn DataField="numWareHouseDetailID" Visible="false"></asp:BoundColumn>
								             <asp:BoundColumn DataField="country" HeaderText="Country"></asp:BoundColumn>
								             <asp:BoundColumn DataField="state" HeaderText="State"></asp:BoundColumn>
								             <asp:BoundColumn DataField="WareHouse" DataFormatString="{0:#,##0.00}" HeaderText="WareHouse"></asp:BoundColumn>
								             <asp:TemplateColumn HeaderStyle-Width="10" ItemStyle-Width="10">
								                <ItemTemplate>
									                <asp:Button ID="btnDelete" Runat="server" CssClass="Delete" Text="r" CommandName="Delete"></asp:Button>
								                </ItemTemplate>
								             </asp:TemplateColumn>
								        </Columns>
				                    </asp:DataGrid>
				            </asp:TableCell>
				        </asp:TableRow>
			        </asp:table> 
			        <asp:TextBox runat="server" Visible="false" ID="txtWareHouseDetailID" Text="0"></asp:TextBox>  
			        <table width="100%" class="normal4">
			            <tr>
			                <td align="center">
			                    <asp:Literal ID="ltrMessage" Visible="false" runat="server" ></asp:Literal> 
			                </td>
			            </tr>
			        </table> 
			        
			    </ContentTemplate>
			</asp:UpdatePanel>    
    </form>
</body>
</html>
