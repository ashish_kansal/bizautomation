Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Admin
Partial Public Class frmShortCutMngGrp
    Inherits BACRMPage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then
                GetUserRightsForPage(13, 25)
                If m_aryRightsForPage(RIGHTSTYPE.VIEW) = 0 Then
                    Response.Redirect("../admin/authentication.aspx?mesg=AS")
                ElseIf m_aryRightsForPage(RIGHTSTYPE.UPDATE) = 0 Then
                    btnSetAsInitial.Visible = False
                End If

                PopulateAuthorizedGroupListData()
                BindTab()
                PopulateData()

                Dim radItem As Telerik.Web.UI.RadComboBoxItem
                For Each item As ListItem In ddlGroup.Items
                    If item.Value <> "0" AndAlso CCommon.ToLong(item.Value) <> CCommon.ToLong(Session("UserGroupID")) Then
                        radItem = New Telerik.Web.UI.RadComboBoxItem
                        radItem.Text = item.Text
                        radItem.Value = item.Value
                        rcbGroups.Items.Add(radItem)
                    End If
                Next
            End If

            If Session("UserGroupID") IsNot Nothing Then
                If ddlGroup.Items.FindByValue(Session("UserGroupID")) IsNot Nothing Then
                    ddlGroup.Items.FindByValue(Session("UserGroupID")).Attributes.Add("style", "color:green")
                End If
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Sub PopulateAuthorizedGroupListData()
        Try
            ddlGroup.Items.Clear()  'Clear all the items
            Dim objUserGroups As New UserGroups
            Dim dtGroupName As DataTable
            objUserGroups.DomainId = Session("DomainID")
            objUserGroups.SelectedGroupTypes = "1,4"
            dtGroupName = objUserGroups.GetAutorizationGroup

            ddlGroup.DataSource = dtGroupName.DataSet.Tables(0).DefaultView
            ddlGroup.DataTextField = "vcGroupName"
            ddlGroup.DataValueField = "numGroupId"
            ddlGroup.DataBind()
            ddlGroup.Items.Insert(0, "--Select One--")
            ddlGroup.Items.FindByText("--Select One--").Value = 0

            If Session("UserGroupID") IsNot Nothing Then
                If ddlGroup.Items.FindByValue(Session("UserGroupID")) IsNot Nothing Then
                    ddlGroup.ClearSelection()
                    ddlGroup.Items.FindByValue(Session("UserGroupID")).Selected = True
                End If
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub BindTab()
        Try
            Dim dtTabData As DataTable
            Dim objTab As New Tabs
            objTab.GroupID = ddlGroup.SelectedValue
            objTab.RelationshipID = 0
            objTab.DomainID = Session("DomainID")
            dtTabData = objTab.GetTabData()

            ddlTab.DataSource = dtTabData
            ddlTab.DataTextField = "numTabName"
            ddlTab.DataValueField = "numTabID"
            ddlTab.DataBind()

            ddlTab.Items.Insert(0, New ListItem("--Select One--", "0"))
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub PopulateData()
        Try
            Dim objUserGroups As New CShortCutBar
            Dim ds As New DataSet
            If ddlGroup.SelectedValue = 0 AndAlso ddlTab.SelectedValue > 0 Then
                lstAddfldFav.Items.Clear()
            Else
                objUserGroups.GroupId = ddlGroup.SelectedValue
                objUserGroups.DomainId = Session("DomainID")
                objUserGroups.TabId = ddlTab.SelectedValue

                ds = objUserGroups.GetAvailableFields()

                Dim dtTable1 As DataTable = ds.Tables(0)

                Dim dtTab As DataTable
                dtTab = Session("DefaultTab")

                For Each row As DataRow In dtTable1.Rows
                    Select Case row.Item("VcLinkName").ToString
                        Case "MyLeads"
                            row.Item("VcLinkName") = "My " & dtTab.Rows(0).Item("vcLead").ToString & "s"
                        Case "WebLeads"
                            row.Item("VcLinkName") = "Web " & dtTab.Rows(0).Item("vcLead").ToString & "s"
                        Case "PublicLeads"
                            row.Item("VcLinkName") = "Public " & dtTab.Rows(0).Item("vcLead").ToString & "s"
                        Case "Contacts"
                            row.Item("VcLinkName") = dtTab.Rows(0).Item("vcContact").ToString & "s"
                        Case "Prospects"
                            row.Item("VcLinkName") = dtTab.Rows(0).Item("vcProspect").ToString & "s"
                        Case "Accounts"
                            row.Item("VcLinkName") = dtTab.Rows(0).Item("vcAccount").ToString & "s"
                    End Select

                    If CCommon.ToBool(row.Item("bitInitialPage")) = True Then
                        row.Item("VcLinkName") = "(*) " & row.Item("VcLinkName")
                    End If
                Next

                dtTable1.AcceptChanges()

                lstAddfldFav.DataSource = dtTable1
                lstAddfldFav.DataTextField = "VcLinkName"
                lstAddfldFav.DataValueField = "Id"
                lstAddfldFav.DataBind()
                If ds IsNot Nothing AndAlso ds.Tables(1).Rows.Count > 0 Then
                    chkDefaultPage.Checked = CCommon.ToBool(ds.Tables(1).Rows(0)(0))
                End If
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub ddlGroup_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGroup.SelectedIndexChanged
        Try
            BindTab()
            PopulateData()

            rcbGroups.Items.Clear()
            Dim radItem As Telerik.Web.UI.RadComboBoxItem
            For Each item As ListItem In ddlGroup.Items
                If item.Value <> "0" AndAlso item.Value <> ddlGroup.SelectedValue Then
                    radItem = New Telerik.Web.UI.RadComboBoxItem
                    radItem.Text = item.Text
                    radItem.Value = item.Value
                    rcbGroups.Items.Add(radItem)
                End If
            Next
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub ddlTab_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlTab.SelectedIndexChanged
        Try
            PopulateData()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub btnSetAsInitial_Click(sender As Object, e As EventArgs) Handles btnSetAsInitial.Click
        Try
            Dim strInitialPage = CCommon.ToString(txtInitialPage.Text)

            Dim dtFav As New DataTable

            Dim i As Integer
            Dim dr As DataRow

            dtFav.Columns.Add("numLinkId")
            dtFav.Columns.Add("numOrder")
            dtFav.Columns.Add("bitInitialPage")
            dtFav.Columns.Add("tintLinkType")

            Dim strValesFav As String()
            strValesFav = txthiddenFav.Text.Split(",")

            Dim strValesLink As String()

            Dim ds As New DataSet
            For i = 0 To strValesFav.Length - 2
                dr = dtFav.NewRow
                strValesLink = strValesFav(i).Split("~")

                dr("numLinkId") = strValesLink(0)
                dr("tintLinkType") = strValesLink(1)

                dr("numOrder") = i + 1
                dr("bitInitialPage") = IIf(strValesFav(i) = strInitialPage, True, False)
                dtFav.Rows.Add(dr)
            Next

            dtFav.TableName = "Table"
            ds.Tables.Add(dtFav.Copy)


            Dim objShortCutBar As New CShortCutBar

            Dim listItemGroup As New System.Collections.Generic.List(Of Long)
            If ddlGroup.SelectedValue <> "0" AndAlso CCommon.ToLong(ddlGroup.SelectedValue) > 0 Then
                listItemGroup.Add(ddlGroup.SelectedValue)
            End If

            For Each item As Telerik.Web.UI.RadComboBoxItem In rcbGroups.CheckedItems
                listItemGroup.Add(item.Value)
            Next
            Dim Saved As Boolean = False
            For Each groupID As Long In listItemGroup
                objShortCutBar = New CShortCutBar
                objShortCutBar.GroupId = groupID
                objShortCutBar.DomainID = Session("DomainID")
                objShortCutBar.StrFav = ds.GetXml
                objShortCutBar.TabId = ddlTab.SelectedValue
                objShortCutBar.DefaultTab = chkDefaultPage.Checked
                objShortCutBar.SaveAllowedFields()

                Saved = True
            Next

            If Saved = True Then PopulateData()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
End Class