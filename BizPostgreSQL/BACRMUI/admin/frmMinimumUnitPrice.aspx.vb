﻿Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Common
Public Class frmMinimumUnitPrice
    Inherits BACRMPage
    Dim objCommon As CCommon

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then
                BindData()
                BindItemClassification()
            End If
            btnCancel.Attributes.Add("onclick", "return Close()")
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Sub BindData()
        Try
            chkEnable.Checked = CCommon.ToBool(Session("IsMinUnitPriceRule"))
            Dim objCommon As New CCommon()
            Dim dtTable As DataTable
            Dim objUserAccess As New UserAccess
            objUserAccess.DomainID = Session("DomainID")
            If CCommon.ToLong(ddlItemClassification.SelectedValue) > 0 Then
                objUserAccess.ListItemId = ddlItemClassification.SelectedValue
            Else
                objUserAccess.ListItemId = Nothing
            End If

            dtTable = objUserAccess.GetDomainDetailsForPriceMargin()

            If (dtTable IsNot Nothing AndAlso dtTable.Rows.Count > 0) Then
                Session("GetDomainDetails") = dtTable
                radTxtAbovePercent.Text = CCommon.ToString(dtTable.Rows(0).Item("numAbovePercent"))
                radTxtBelowPercent.Text = CCommon.ToString(dtTable.Rows(0).Item("numBelowPercent"))
                If dtTable.Rows(0).Item("numBelowPriceField") IsNot Nothing AndAlso CCommon.ToString(dtTable.Rows(0).Item("numBelowPriceField")) <> "" Then
                    ddlBelow.SelectedValue = CCommon.ToString(dtTable.Rows(0).Item("numBelowPriceField"))
                End If
                hdnUnitPriceApprover.Value = CCommon.ToString(dtTable.Rows(0).Item("vcUnitPriceApprover"))
                chkCost.Checked = CCommon.ToBool(dtTable.Rows(0).Item("bitCostApproval"))
                chkPrice.Checked = CCommon.ToBool(dtTable.Rows(0).Item("bitListPriceApproval"))
                chkViolateApproval.Checked = CCommon.ToBool(dtTable.Rows(0).Item("bitMarginPriceViolated"))
            Else
                Session("GetDomainDetails") = Nothing
                radTxtAbovePercent.Text = ""
                radTxtBelowPercent.Text = ""
                ddlBelow.SelectedIndex = -1
                chkCost.Checked = False
                chkPrice.Checked = False
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If Page.IsValid Then
            Try
                save()
                BindData()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End If
    End Sub

    Private Sub btnSaveClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveClose.Click
        If Page.IsValid Then
            Try
                save()
                Dim strScript As String = "<script language=JavaScript>if (window.opener != null) {window.opener.location.reload(true);Close();}self.close();</script>"
                If (Not Page.IsStartupScriptRegistered("clientScript")) Then Page.RegisterStartupScript("clientScript", strScript)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End If
    End Sub

    Private Sub save()
        Try
            Dim objUserAccess As New UserAccess
            Dim dt As New DataTable()
            dt = Session("GetDomainDetails")
            objUserAccess.ListItemId = CCommon.ToLong(ddlItemClassification.SelectedValue)
            objUserAccess.DomainID = Session("DomainID")
            objUserAccess.AbovePercent = CCommon.ToDouble(radTxtAbovePercent.Text)
            objUserAccess.BelowPercent = CCommon.ToDouble(radTxtBelowPercent.Text)
            objUserAccess.BelowPriceField = CCommon.ToLong(ddlBelow.SelectedValue)
            objUserAccess.IsCostApproval = chkCost.Checked
            objUserAccess.IsListPriceApproval = chkPrice.Checked
            objUserAccess.bitMarginPriceViolated = chkViolateApproval.Checked
            objUserAccess.UnitPriceApprover = hdnUnitPriceApprover.Value
            objUserAccess.IsMinUnitPriceRule = chkEnable.Checked
            objUserAccess.UpdateApprovalProcessItemClassification()

            Session("bitMarginPriceViolated") = chkViolateApproval.Checked
            Session("UnitPriceApprover") = hdnUnitPriceApprover.Value
            Session("IsMinUnitPriceRule") = chkEnable.Checked
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub BindItemClassification()
        Try
            If objCommon Is Nothing Then objCommon = New CCommon
            objCommon.sb_FillComboFromDBwithSel(ddlItemClassification, 36, Session("DomainID"))
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Protected Sub ddlItemClassification_SelectedIndexChanged(sender As Object, e As EventArgs)
        BindData()
    End Sub

End Class