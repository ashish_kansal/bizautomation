''' -----------------------------------------------------------------------------
''' Project	 : BACRM.UserInterface.Reports
''' Class	 : frmAdvSearchColumnCustomization
''' 
''' -----------------------------------------------------------------------------
''' <summary>
'''     This is a wizard for the configuration of the columns which are displayed on Advance Search for a selected Group
''' </summary>
''' <remarks>
''' </remarks>
''' <history>
''' 	[Debasish]	09/12/2005	Created
''' </history>
''' -----------------------------------------------------------------------------
Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Common
Imports System.IO
Namespace BACRM.UserInterface.Admin
    Public Class frmAdvSearchColumnCustomization
        Inherits BACRMPage

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Protected WithEvents btnSave As System.Web.UI.WebControls.Button
        Protected WithEvents lstAvailablefld As System.Web.UI.WebControls.ListBox
        Protected WithEvents lstSelectedfld As System.Web.UI.WebControls.ListBox
        Protected WithEvents hdXMLString As System.Web.UI.HtmlControls.HtmlInputHidden
        Protected WithEvents tblAdvSearchFieldCustomization As System.Web.UI.WebControls.Table
        Protected WithEvents hdSave As System.Web.UI.HtmlControls.HtmlInputHidden

        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.
        Private designerPlaceholderDeclaration As System.Object

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This event is fired eachtime thepage is called. In this event we will 
        '''     get the data from the DB and populate the Tables for the drop downs.
        ''' </summary>
        ''' <param name="sender">Represents the sender object.</param>
        ''' <param name="e">Represents the EventArgs.</param>
        ''' <remarks>
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	08/12/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Dim FormID As Integer = 1

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Try
                'Put user code to initialize the page here
                If GetQueryStringVal( "FormId") <> "" Then
                    FormID = CCommon.ToInteger(GetQueryStringVal("FormId"))
                End If
                If Not IsPostBack Then
                    hdnIsFromNewAdvancedSearch.Value = CCommon.ToInteger(GetQueryStringVal("IsNewSearch"))

                    DataBindElements()
                    PostInitializeControls()                            'Call to attach click event to the button
                End If
                If hdSave.Value = "False" Then DataBindElements() 'Calls the subroutine to databind the listboxes to dataset
                btnSave.Attributes.Add("onclick", "return Save()")
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This method is used to databind the listboxes/ radionbuttonlist to the dataset
        ''' </summary>
        ''' <remarks>
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	09/07/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Sub DataBindElements()
            Try
                '================================================================================
                ' Purpose: Calls the subroutine to databind the Available fields and Selected Fields listbox to dataset
                '
                ' History
                ' Ver   Date        Ref     Author              Reason Created
                ' 1     08/12/2005          Debasish Nag        Calls the subroutine to databind 
                '                                               the listbox to dataset
                '================================================================================
                Dim objGenericAdvSearch As New FormGenericAdvSearch
                objGenericAdvSearch.FormID = FormID
                objGenericAdvSearch.DomainID = Session("DomainID")
                objGenericAdvSearch.UserCntID = Session("UserContactID")
                objGenericAdvSearch.DisplayColumns = CCommon.ToString(GetQueryStringVal("displayColumns"))
                Dim dsSearchFieldList As DataSet = objGenericAdvSearch.getSearchFieldList()

                lstAvailablefld.DataSource = dsSearchFieldList.Tables(0)
                lstAvailablefld.DataTextField = "vcFormFieldName"
                lstAvailablefld.DataValueField = "vcFieldID"
                lstAvailablefld.DataBind()

                lstSelectedfld.DataSource = dsSearchFieldList.Tables(1)
                lstSelectedfld.DataTextField = "vcFormFieldName"
                lstSelectedfld.DataValueField = "vcFieldID"
                lstSelectedfld.DataBind()

                Dim iCount As Integer
                For iCount = 0 To lstSelectedfld.Items.Count - 1
                    If Not lstAvailablefld.Items.FindByValue(lstSelectedfld.Items(iCount).Value) Is Nothing Then
                        lstAvailablefld.Items.Remove(lstSelectedfld.Items(iCount))
                    End If
                Next
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        ''' -----------------------------------------------------------------------------
        '''<summary>
        '''     Removes duplicate rows from given DataTable
        '''</summary>
        '''<param name="tblOne">Table to scan for duplicate rows</param>
        '''<param name="tblTwo">Reference table for matchign duplicate rows</param>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	09/07/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Private Sub RemoveDuplicatesRows(ByRef tblOne As DataTable, ByVal tblTwo As DataTable, ByVal keyColumn As String)
            Try
                Dim objParentCol As DataColumn = tblOne.Columns(keyColumn)                                                'Create an object of the first column
                Dim objChildCol As DataColumn = tblTwo.Columns(keyColumn)                                                 'Create an object of the second column
                Dim objFieldIdRelation As DataRelation                                                                    'create relation object
                objFieldIdRelation = New DataRelation("FieldRelation", objParentCol, objChildCol, False)                  'create parent child relation 
                tblOne.DataSet.Relations.Add(objFieldIdRelation)                                                          'Add the relation to the dataset
                Dim objParentrow, objChildRow As DataRow
                For Each objChildRow In tblTwo.Rows                                                                       'loop through the rows in child table
                    Dim colParentRows() As DataRow = objChildRow.GetParentRows(objFieldIdRelation)                        'Create an array of parent rows
                    If colParentRows.Length > 0 Then
                        For Each objParentrow In colParentRows                                                            'Loop through the parent rows
                            tblOne.Rows.Remove(objParentrow)                                                              'Remove the Parent Row
                        Next
                    End If
                Next
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This method is used to handle saving of the configuration after the 
        '''     "Save" button is clicked
        ''' </summary>
        ''' <param name="sender">Represents the sender object.</param>
        ''' <param name="e">Represents the EventArgs.</param>
        ''' <remarks>
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	08/12/2005	Created
        ''' </history>
        '''  -----------------------------------------------------------------------------
        Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
            Try
                If hdnIsFromNewAdvancedSearch.Value = "1" Then
                    ScriptManager.RegisterClientScriptBlock(Me, Me.Page.GetType(), "CloseAndReturnSelectedColumns", "CloseAndReturnSelectedColumns();", True)
                Else
                    Dim objGenericAdvSearch As New FormGenericAdvSearch
                    objGenericAdvSearch.FormID = FormID
                    objGenericAdvSearch.DomainID = Session("DomainID")
                    objGenericAdvSearch.UserCntID = Session("UserContactID")
                    'set the Viewid from the drop down
                    Dim dsNew As New DataSet
                    Dim dtTable As New DataTable
                    dtTable.Columns.Add("numFormFieldID")
                    dtTable.Columns.Add("bitCustom")
                    dtTable.Columns.Add("tintOrder")
                    Dim i As Integer
                    Dim dr As DataRow
                    Dim str As String()
                    str = hdnCol.Value.Split(",")
                    For i = 0 To str.Length - 1
                        dr = dtTable.NewRow
                        dr("numFormFieldID") = CCommon.ToLong(str(i).Split("~")(0))
                        dr("bitCustom") = CCommon.ToShort(str(i).Split("~")(1))
                        dr("tintOrder") = i
                        dtTable.Rows.Add(dr)
                    Next

                    dtTable.TableName = "Table"
                    dsNew.Tables.Add(dtTable.Copy)
                    objGenericAdvSearch.strAdvSerCol = dsNew.GetXml
                    dsNew.Tables.Remove(dsNew.Tables(0))
                    objGenericAdvSearch.ManageAdvSerColnConf()

                    DataBindElements()                                                      'Calls the subroutine to databind the listboxes to dataset
                    hdSave.Value = "False"
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This method is used to databind the listboxes to the dataset
        ''' </summary>
        ''' <remarks>
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	09/07/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Sub PostInitializeControls()
            Try
                '================================================================================
                ' Purpose: Does post initialization settings of the controls
                '
                ' History
                ' Ver   Date        Ref     Author              Reason Created
                ' 1     09/07/2005          Debasish Nag        Initialization extra settings 
                '                                               of the controls
                '================================================================================
                btnSave.Attributes.Add("OnClick", "javascript: return validateSelectedSearchFields(document.frmAdvSearchColumnCustomization);")     'calls for validation
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
    End Class
End Namespace
