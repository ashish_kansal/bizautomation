﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/common/DetailPage.Master" CodeBehind="frmAdvancedSearchNew.aspx.vb" Inherits="BACRM.UserInterface.Admin.frmAdvancedSearchNew" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript" src="../JavaScript/jquery-ui.min.js"></script>
    <style type="text/css">
        .form-control-select2 {
            display: block;
            width: 100%;
            height: 34px;
            font-size: 14px;
            line-height: 1.42857143;
            color: #555;
            background-color: #fff;
            background-image: none;
            border: 1px solid #ccc;
            border-radius: 0px;
            -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
            box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
            -webkit-transition: border-color ease-in-out .15s,-webkit-box-shadow ease-in-out .15s;
            -o-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            margin-top: 0px;
            margin-left: 0px;
        }

        .treeChildCol1 {
            width: 8.666667% !important;
        }

        .form-control-select2 > .select2-choices {
            background-image: none !important;
            border: none !important;
        }

        .input-group .input-group-addon {
            background-color: #e0e0e0;
        }

        .modal-body {
            /*height: 500px;*/
            max-height: calc(100vh - 200px);
            min-height: calc(100vh - 200px);
            overflow-y: auto;
        }

        .input-group-addon {
            cursor: pointer;
        }
    </style>
    <script type="text/javascript">
        var selectedFeildsModule = [];
        var totalConditions = 0;

        $(document).ready(function () {
            LoadModuleAndSearchFields()


            $(document).ajaxStart(function () {
                $("[id$=divLoader]").show();
            });

            $(document).ajaxComplete(function () {
                $("[id$=divLoader]").hide();
            });

            if ($("[id$=ddlSavedSearch]").val() != 0) {
                SavedAdvancedSearchSelected();
            } else if ($("[id$=hdnSearchCondition]").val() != "") {
                LoadSearchConditions();
            }
        });

        function LoadSearchConditions() {
            var searchConditions = JSON.parse($("[id$=hdnSearchCondition]").val());
            var addedFieldID;

            searchConditions.forEach(function (e) {
                try {
                    addedFieldID = AddCondition(e.FormID + "~" + e.FieldID + "~" + e.AssociatedControlType + "~" + e.IsCustomField + "~" + e.ListID + "~" + e.ListItemType + "~" + e.LookBackTableName + "~" + e.DbColumnName + "~" + e.FieldType, e.FieldName, false);

                    var searchOperatorDropdown = $("[id$='selCondition" + addedFieldID + "']")[0];
                    $(searchOperatorDropdown).val(e.SearchOperator);
                    changeControl("tdControl" + addedFieldID, e.AssociatedControlType, searchOperatorDropdown)

                    if (e.SearchOperator == "10" || e.SearchOperator == "11" || e.SearchOperator == "19") {
                        $("[id$='fieldFrom" + addedFieldID + "']").val(e.SearchFrom);
                        $("[id$='fieldTo" + addedFieldID + "']").val(e.SearchTo);

                        if (e.SearchOperator == "19") {
                            $("[id$='fieldFrom" + field + "']").datepicker({ dateFormat: 'mm/dd/yy' });
                            $("[id$='fieldTo" + field + "']").datepicker({ dateFormat: 'mm/dd/yy' });
                        }
                    } else {
                        if (e.AssociatedControlType == "TextBox" || e.AssociatedControlType == "TextArea" || e.AssociatedControlType == "Label") {
                            $("[id$='field" + addedFieldID + "']").select2("data", JSON.parse(e.SearchValueSelect2));
                        } else if (e.AssociatedControlType == "CheckBoxList" || e.AssociatedControlType == "SelectBox" || e.AssociatedControlType == "ListBox") {
                            $("[id$='field" + addedFieldID + "']").select2("data", JSON.parse(e.SearchValueSelect2));
                        } else if (e.AssociatedControlType == "CheckBox") {
                            if (e.SearchValue) {
                                $("[id$='field" + addedFieldID + "']").prop("checked", true);
                            } else {
                                $("[id$='field" + addedFieldID + "']").prop("checked", false);
                            }
                        }
                    }
                } catch (err) {

                }
            });
        }


        function LoadModuleAndSearchFields() {
            $("[id$=divLoader]").show();

            try {
                $.ajax({
                    type: "POST",
                    url: '../common/Common.asmx/GetAdvancedSearchModuleAndFields',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        var obj = $.parseJSON(data.d);

                        var modules = $.parseJSON(obj.modules);
                        var fields = $.parseJSON(obj.fields);
                        var i = 0;
                        var field;

                        modules.forEach(function (e) {
                            $("#divSearchFields").append("<div class='row padbottom10'>");
                            $("#divSearchFields").append("<div class='col-xs-12'><ul class='list-inline' style='margin-bottom:2px; cursor:pointer' onclick='collapse(" + e.numFormID + ");'><li style='vertical-align:middle'><i class='fa " + (i == 0 ? "fa-caret-down" : "fa-caret-right") + "' id='fa" + e.numFormID + "' style='font-size:22px;'></i></li><li style='font-size:16px'>" + e.vcFormName + "</li></ul></div>");
                            $("#divSearchFields").append("<div class='col-xs-12' id='divChild" + e.numFormID + "'>");
                            $("#divSearchFields").append("</div>");
                            $("#divSearchFields").append("</div>");
                            if (i != 0) {
                                $("#divChild" + e.numFormID).hide();
                            }
                            i++;
                        });

                        fields.forEach(function (e) {
                            field = e.numFormId + "~" + e.numFieldId + "~" + e.vcAssociatedControlType + "~" + e.bitCustomField + "~" + e.numListID + "~" + e.vcListItemType + "~" + e.vcLookBackTableName + "~" + e.vcDbColumnName + "~" + e.vcFieldType + "~" + e.vcFieldDataType
                            $("#divChild" + e.numFormId).append("<div class='row'>");
                            $("#divChild" + e.numFormId).append("<div class='col-xs-1'></div>");
                            $("#divChild" + e.numFormId).append("<div class=\"col-xs-10\"><a style='cursor:pointer;' onclick=\"return AddCondition('" + field + "','" + e.vcFieldName + "',true);\">" + e.vcFieldName + "</a></div>");
                            $("#divChild" + e.numFormId).append("</div>");
                        });

                        $("[id*=divChild]").each(function () {
                            if ($(this).children().length == 0) {
                                $(this).append("<div class='row'>");
                                $(this).append("<div class='col-xs-1'></div>");
                                $(this).append("<div class='col-xs-10'><i style='color:red'>Fields not configured.</i></div>");
                                $(this).append("</div>");
                            }
                        });
                    },
                    results: function (data) {
                        columns = $.parseJSON(data);
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        alert(textStatus);
                    }
                });
            } catch (err) {
                alert("Unknown error occured.");
                return false;
            }

            $("[id$=divLoader]").hide();
        }

        function SavedAdvancedSearchSelected() {
            totalConditions = 0;
            $("#tblCondition tr").remove();

            if ($("[id$=ddlSavedSearch]").val() != "0") {
                try {
                    $.ajax({
                        type: "POST",
                        url: '../common/Common.asmx/GetUserSavedAdvancedSearch',
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        data: JSON.stringify({
                            searchID: $("[id$=ddlSavedSearch]").val()
                        }),
                        success: function (data) {
                            var obj = $.parseJSON(data.d);

                            if (obj != null && obj.length > 0) {
                                $("#lbSelectedUsers > option").each(function () {
                                    $(this).remove().appendTo("#lbAvailableUsers");
                                });

                                if (obj[0].vcSharedWith != "") {
                                    $("#lbAvailableUsers > option").each(function () {
                                        if (("," + obj[0].vcSharedWith + ",").indexOf($(this).val()) != -1) {
                                            $(this).remove().appendTo("#lbSelectedUsers");
                                        }
                                    });
                                }


                                if (obj[0].vcSearchConditionJson != null) {
                                    $("[id$=hdnDisplayColumns]").val(obj[0].vcDisplayColumns);
                                    var searchConditions = JSON.parse(obj[0].vcSearchConditionJson);
                                    var addedFieldID;

                                    searchConditions.forEach(function (e) {
                                        try {
                                            addedFieldID = AddCondition(e.FormID + "~" + e.FieldID + "~" + e.AssociatedControlType + "~" + e.IsCustomField + "~" + e.ListID + "~" + e.ListItemType + "~" + e.LookBackTableName + "~" + e.DbColumnName + "~" + e.FieldType, e.FieldName, false);

                                            var searchOperatorDropdown = $("[id$='selCondition" + addedFieldID + "']")[0];
                                            $(searchOperatorDropdown).val(e.SearchOperator);
                                            changeControl("tdControl" + addedFieldID, e.AssociatedControlType, searchOperatorDropdown)

                                            if (e.SearchOperator == "10" || e.SearchOperator == "11" || e.SearchOperator == "19") {
                                                $("[id$='fieldFrom" + addedFieldID + "']").val(e.SearchFrom);
                                                $("[id$='fieldTo" + addedFieldID + "']").val(e.SearchTo);

                                                if (e.SearchOperator == "19") {
                                                    $("[id$='fieldFrom" + field + "']").datepicker({ dateFormat: 'mm/dd/yy' });
                                                    $("[id$='fieldTo" + field + "']").datepicker({ dateFormat: 'mm/dd/yy' });
                                                }
                                            } else {
                                                if (e.AssociatedControlType == "TextBox" || e.AssociatedControlType == "TextArea" || e.AssociatedControlType == "Label") {
                                                    $("[id$='field" + addedFieldID + "']").select2("data", JSON.parse(e.SearchValueSelect2));
                                                } else if (e.AssociatedControlType == "CheckBoxList" || e.AssociatedControlType == "SelectBox" || e.AssociatedControlType == "ListBox") {
                                                    $("[id$='field" + addedFieldID + "']").select2("data", JSON.parse(e.SearchValueSelect2));
                                                } else if (e.AssociatedControlType == "CheckBox") {
                                                    if (e.SearchValue) {
                                                        $("[id$='field" + addedFieldID + "']").prop("checked", true);
                                                    } else {
                                                        $("[id$='field" + addedFieldID + "']").prop("checked", false);
                                                    }
                                                }
                                            }
                                        } catch (err) {

                                        }
                                    });

                                } else {
                                    OpenSearchResult(obj[0].numSearchID, obj[0].numFormID);
                                    $("[id$=hdnDisplayColumns]").val("");
                                }
                            }
                        },
                        results: function (data) {
                            columns = $.parseJSON(data);
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            alert(textStatus);
                        }
                    });
                } catch (err) {
                    alert("Unknown error occured.");
                    return false;
                }
            }
        }

        function OpenSearchResult(a, b) {
            if (b == 15) {
                document.location.href = 'frmItemSearchRes.aspx?SearchID=' + a;
            }
            if (b == 1) {
                document.location.href = 'frmAdvancedSearchRes.aspx?SearchID=' + a;
            }
            if (b == 29) {
                document.location.href = 'frmAdvSerInvItemsRes.aspx?SearchID=' + a;
            }
        }

        function collapse(id) {
            if ($("#divChild" + id).is(":visible")) {
                $("#divChild" + id).hide();
                $("#fa" + id).removeClass("fa-caret-down");
                $("#fa" + id).addClass("fa-caret-right");
            } else {
                $("#divChild" + id).show();
                $("#fa" + id).removeClass("fa-caret-right");
                $("#fa" + id).addClass("fa-caret-down");
            }
        }

        function AddCondition(field, fieldName, isLoadAutocomplete) {
            field = (totalConditions + 1).toString() + "~" + field
            var data = field.split("~");
            var controlType = data[3];
            var module = data[1];

            if (module != 1 && selectedFeildsModule.indexOf(module) == -1 && selectedFeildsModule.length > 1) {
                alert("You can not add fields from 2 or more child objects at the same time. You can combine fields from \"Organizations & Contacts\" and 1 child object (max) or a single child object at a time");
            }
            else {
                if (selectedFeildsModule.indexOf(module) == -1) {
                    selectedFeildsModule.push(module);
                }

                totalConditions += 1;

                var tr = "<tr style=\"line-height:40px; border-top:1px solid #ddd; border-bottom:1px solid #ddd;\" id=\"" + field + "\">";
                tr = tr + "<td style=\"white-space:nowrap; text-align:right\"><label>" + fieldName + "</label></td>";
                tr = tr + "<td style=\"padding-right:5px;\">" + getConditionTypes(controlType, field) + "</td>";
                tr = tr + "<td style=\"width:100%;padding-left:5px;padding-right:5px;background-color: #dddddd45;\" id=\"tdControl" + field + "\">" + getControlType(controlType, field) + "</td>";

                tr = tr + "<td style=\"width:25px;padding-left:5px;\"><button class=\"btn btn-xs btn-danger\" onclick=\"return RemoveCondition(this);\"><i class=\"fa fa-trash-o\"></i></button></td>";
                tr = tr + "</tr>";

                $("#tblCondition tbody").append(tr + "<tr class=\"spacer\"></tr>");

                if (isLoadAutocomplete) {
                    if (controlType == "TextBox" || controlType == "TextArea" || controlType == "Label") {
                        var data = field.split("~");
                        loadTokenizer("field" + field, data[5], data[6]);
                    } else if (controlType == "CheckBoxList" || controlType == "SelectBox" || controlType == "ListBox") {
                        var data = field.split("~");
                        loadAutocomplete("field" + field, data[5], data[6]);
                    }
                }
            }

            if (isLoadAutocomplete) {
                $('html, body').animate({
                    scrollTop: 0
                }, 100);
            }

            return field;
        }

        function RemoveCondition(button) {
            $(button).parent("td").parent("tr").remove();

            selectedFeildsModule = [];

            $("#tblCondition tr[class!='spacer']").each(function () {
                if (selectedFeildsModule.indexOf($(this).attr("id").split("~")[0]) === -1) {
                    selectedFeildsModule.push($(this).attr("id").split("~")[0]);
                }
            });

            totalConditions -= 1;

            return false;
        }

        function getConditionTypes(controlType, field) {
            var ui = "<select class=\"form-control search-condition\" id='selCondition" + field + "' style=\"width:150px; margin-left:5px;\" onChange=\"return changeControl('tdControl" + field + "','" + controlType + "',this);\">";

            if (controlType == "TextBox" || controlType == "TextArea" || controlType == "Label") {
                if (field.split("~").length >= 11 && field.split("~")[10] == "N") {
                    ui = ui + "<option value=\"10\">Number Range</option>";
                } else {
                    ui = ui + "<option value=\"1\">Contains</option>";
                    ui = ui + "<option value=\"15\">Not Contains</option>";
                    ui = ui + "<option value=\"2\">Starts With</option>";
                    ui = ui + "<option value=\"3\">Ends With</option>";
                    ui = ui + "<option value=\"4\">Contains no value (null)</option>";
                }
            } else if (controlType == "CheckBox") {
                ui = ui + "<option value=\"16\">Selected</option>";
            } else if (controlType == "CheckBoxList" || controlType == "SelectBox" || controlType == "ListBox") {
                ui = ui + "<option value=\"17\">Includes</option>";
                ui = ui + "<option value=\"18\">Excludes</option>";
            } else if (controlType == "DateField") {
                ui = ui + "<option value=\"11\">Date Range</option>";
                ui = ui + "<option value=\"19\">Date Selection</option>";
            }

            var ui = ui + "</select>";

            return ui;
        }

        function getControlType(controlType, field) {
            var ui = "";

            if (controlType == "TextBox" || controlType == "TextArea" || controlType == "Label") {
                if (field.split("~").length >= 11 && field.split("~")[10] == "N") {
                    ui += "<ul class='list-inline' style='margin-bottom:0px'><li><b>From </b></li><li><input type='text' autocomplete='false' class='form-control' id='fieldFrom" + field + "' /></li><li><b> To </b></li><li><input type='text' autocomplete='false' class='form-control' id='fieldTo" + field + "' /></li></ul>";
                } else {
                    ui += "<input type='text' class='form-control-select2' id='field" + field + "' autocomplete='false' />";
                }
            } else if (controlType == "CheckBoxList" || controlType == "SelectBox" || controlType == "ListBox") {
                ui += "<div class='input-group'>";
                ui += "<input type='text' class='form-control-select2' id='field" + field + "' autocomplete='false' />";
                ui += "<span class='input-group-addon' id='basic-addon1' onclick='return openOptionsList(\"" + field + "\")'><i class='fa fa-list-ul' aria-hidden='true'></i></span>";
                ui += "</div>";
            } else if (controlType == "CheckBox") {
                ui += "<input type='checkbox' id='field" + field + "' />";
            } else if (controlType == "DateField") {
                ui += "<ul class='list-inline' style='margin-bottom:0px'><li><b>Days before \"Today\" </b></li><li><input type='text' autocomplete='false' class='form-control' style='width:90px' id='fieldFrom" + field + "' /></li><li><b> Days after \"Today\" </b></li><li><input type='text' autocomplete='false' class='form-control' style='width:90px' id='fieldTo" + field + "' /></li><li>(0 = \"Today\")</li></ul>";
            }

            return ui;
        }

        function changeControl(controlPlaceholderId, controlType, control) {
            if (controlType == "TextBox" || controlType == "TextArea" || controlType == "Label") {
                if ($(control).val() == "10") {
                    $("[id$='" + controlPlaceholderId + "']").html("<ul class='list-inline' style='margin-bottom:0px'><li><b>From </b></li><li><input type='text' autocomplete='false' class='form-control' id='" + controlPlaceholderId.replace("tdControl", "fieldFrom") + "' /></li><li><b> To </b></li><li><input type='text' autocomplete='false' class='form-control' id='" + controlPlaceholderId.replace("tdControl", "fieldTo") + "' /></li></ul>");
                } else {
                    $("[id$='" + controlPlaceholderId + "']").html("<input type='text' class='form-control-select2' id='" + controlPlaceholderId.replace("tdControl", "field") + "' autocomplete='false' />");
                }
            } else if (controlType == "DateField") {
                if ($(control).val() == "19") {
                    $("[id$='" + controlPlaceholderId + "']").html("<ul class='list-inline' style='margin-bottom:0px'><li><b>From </b></li><li><input type='text' autocomplete='false' class='form-control' style='width:90px' id='" + controlPlaceholderId.replace("tdControl", "fieldFrom") + "' /></li><li><b> To </b></li><li><input type='text' autocomplete='false' class='form-control' style='width:90px' id='" + controlPlaceholderId.replace("tdControl", "fieldTo") + "' /></li></ul>");
                } else {
                    $("[id$='" + controlPlaceholderId + "']").html("<ul class='list-inline' style='margin-bottom:0px'><li><b>Days before \"Today\" </b></li><li><input type='text' autocomplete='false' class='form-control' style='width:90px' id='" + controlPlaceholderId.replace("tdControl", "fieldFrom") + "' /></li><li><b> Days after \"Today\" </b></li><li><input type='text' autocomplete='false' class='form-control' style='width:90px' id='" + controlPlaceholderId.replace("tdControl", "fieldTo") + "' /></li><li>(0 = \"Today\")</li></ul>");
                }
            }

            if (controlType == "TextBox" || controlType == "TextArea" || controlType == "Label") {
                var data = controlPlaceholderId.split("~");
                loadTokenizer(controlPlaceholderId.replace("tdControl", "field"), data[5], data[6]);
            } else if (controlType == "CheckBoxList" || controlType == "SelectBox" || controlType == "ListBox") {
                var data = controlPlaceholderId.split("~");
                loadAutocomplete(controlPlaceholderId.replace("tdControl", "field"), data[5], data[6]);
            } else if (controlType == "DateField" && $(control).val() == "19") {
                $("[id$='" + controlPlaceholderId.replace("tdControl", "fieldFrom") + "']").datepicker({ dateFormat: 'mm/dd/yy' });
                $("[id$='" + controlPlaceholderId.replace("tdControl", "fieldTo") + "']").datepicker({ dateFormat: 'mm/dd/yy' });
            }

            return true;
        }

        function loadAutocomplete(controlId, listID, listItemType) {
            $("[id$='" + controlId + "']").select2(
            {
                placeholder: "",
                minimumInputLength: 0,
                multiple: true,
                width: "100%",
                dropdownCssClass: 'bigdrop',
                dataType: "json",
                allowClear: true,
                ajax: {
                    quietMillis: 500,
                    url: '../Common/Common.asmx/GetAdvancedSearchListDetails',
                    type: 'POST',
                    params: {
                        contentType: 'application/json; charset=utf-8'
                    },
                    dataType: 'json',
                    data: function (term, page) {
                        return JSON.stringify({
                            numListID: listID,
                            vcListItemType: listItemType,
                            searchText: term,
                            pageIndex: (page - 1) * 20,
                            pageSize: 20
                        });
                    },
                    results: function (data, page) {
                        if (data.hasOwnProperty("d")) {
                            if (data.d == "Session Expired") {
                                alert("Session expired.");
                            } else {
                                data = $.parseJSON(data.d)
                            }
                        }
                        else
                            data = $.parseJSON(data);

                        var more = (page.page * 20) < data.Total;
                        return { results: $.parseJSON(data.results || null), more: more };
                    }
                }
            });
        }

        function loadTokenizer(controlId, listID, listItemType) {
            $("[id$='" + controlId + "']").select2(
            {
                placeholder: "",
                minimumInputLength: 1,
                multiple: true,
                width: "100%",
                allowClear: true,
                tags: true,
                formatResult: function (item) { return item.text; }
            });
        }

        function openOptionsList(field) {
            var selectedValues = $("[id$='field" + field + "']").select2('data');

            var data = field.split("~");

            $.ajax({
                type: "POST",
                url: '../Common/Common.asmx/GetAdvancedSearchListDetails',
                data: JSON.stringify({
                    numListID: data[5],
                    vcListItemType: data[6],
                    searchText: '',
                    pageIndex: 0,
                    pageSize: 1000000000
                }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    $("#divOptions").html('');
                    $("#divOptions").attr('forcondition', field);

                    var result;

                    if (data.hasOwnProperty("d"))
                        data = $.parseJSON(data.d)
                    else
                        data = $.parseJSON(data);

                    result = $.parseJSON(data.results || null);

                    if (result != null) {
                        var i = 1;
                        var ui = "";
                        result.forEach(function (e) {
                            if (i == 1) {
                                ui = ui + "</div>";
                                ui = ui + "<div class='row'>";
                            }

                            if ($.grep(selectedValues, function (obj) { return obj.id == e.id }).length > 0) {
                                ui = ui + "<div class='col-xs-12 col-sm-4 col-md-3'><div class='checkbox'><label><input type='checkbox' checked='checked' id='" + e.id + "'>" + e.text + "</label></div></div>";
                            } else {
                                ui = ui + "<div class='col-xs-12 col-sm-4 col-md-3'><div class='checkbox'><label><input type='checkbox' id='" + e.id + "'>" + e.text + "</label></div></div>";
                            }

                            if (i == 4) {
                                i = 1;
                            } else {
                                i++;
                            }
                        });

                        if (ui.length > 0) {
                            ui = ui + "</div>";
                        }

                        $("#divOptions").append(ui);
                        $("#modal-options").modal("show");
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    alert(textStatus);
                }
            });
        }

        function addSelectedValues() {
            var selectedValues = [];

            $("#divOptions input[type='checkbox']").each(function () {
                if ($(this).is(":checked")) {
                    selectedValues.push({ id: $(this).attr("id"), text: $(this).parent().text().trim() });
                }
            });

            $("[id$='" + $("#divOptions").attr("forcondition") + "']").select2('data', selectedValues);

            $("#modal-options").modal("hide");
        }

        function clearSelectedValues() {
            $("#divOptions input[type='checkbox']").prop("checked", false);
        }

        function getSearchConditions() {
            var selectedConditions = [];

            try {
                $("#tblCondition tr").each(function () {

                    var operator = $(this).find("select.search-condition").val();
                    var fieldName = $(this).find("td:first").text();
                    var fields = $(this).find("input[id*='field']");

                    if (fields.length > 0) {
                        var field;
                        var formId;
                        var fieldId;
                        var controlType;
                        var isCustomField;
                        var listID;
                        var listItemType;
                        var lookBackTableName;
                        var dbColumnName;
                        var prefix;
                        var fieldType;
                        var searchValueSelect2;

                        field = fields[0].id;
                        formId = field.split("~")[1];
                        fieldId = field.split("~")[2];
                        controlType = field.split("~")[3];
                        isCustomField = field.split("~")[4];
                        listID = field.split("~")[5];
                        listItemType = field.split("~")[6];
                        lookBackTableName = field.split("~")[7];
                        dbColumnName = field.split("~")[8];
                        fieldType = field.split("~")[9];

                        if (lookBackTableName == "DivisionMaster") {
                            prefix = "DM.";
                        } else if (lookBackTableName == "AdditionalContactsInformation") {
                            prefix = "ADC.";
                        } else {
                            prefix = "";
                        }

                        var searchText = "";
                        var from = "";
                        var to = "";

                        if (fields.length == 1) {
                            if (controlType == "TextBox" || controlType == "TextArea" || controlType == "Label") {
                                var selectedValues = $("[id$='" + field + "']").select2('data');
                                if (selectedValues != null && selectedValues.length > 0) {
                                    searchValueSelect2 = JSON.stringify(selectedValues);

                                    selectedValues.forEach(function (e) {
                                        searchText = searchText + (searchText == "" ? "" : ",") + e.id;
                                    });
                                }
                            } else if (controlType == "CheckBoxList" || controlType == "SelectBox" || controlType == "ListBox") {
                                var selectedValues = $("[id$='" + field + "']").select2('data');
                                if (selectedValues != null && selectedValues.length > 0) {
                                    searchValueSelect2 = JSON.stringify(selectedValues);

                                    selectedValues.forEach(function (e) {
                                        searchText = searchText + (searchText == "" ? "" : ",") + e.id;
                                    });
                                }
                            } else if (controlType == "CheckBox") {
                                searchText = fields[0].checked;
                            }
                        } else if (fields.length == 2) {
                            if (controlType == "TextBox" || controlType == "TextArea" || controlType == "Label") {
                                from = fields[0].value;
                                to = fields[1].value;
                            } else if (controlType == "DateField") {
                                from = fields[0].value;
                                to = fields[1].value;
                            }
                        }

                        if (searchText.toString() != "" || from != "" || to != "") {
                            selectedConditions.push({
                                FormID: formId
                                , FieldID: fieldId
                                , AssociatedControlType: controlType
                                , IsCustomField: isCustomField
                                , ListID: listID
                                , FieldName: fieldName
                                , ListItemType: listItemType
                                , LookBackTableName: lookBackTableName
                                , DbColumnName: dbColumnName
                                , SearchOperator: operator
                                , SearchValue: searchText
                                , SearchFrom: from
                                , SearchTo: to
                                , Prefix: prefix
                                , FieldType: fieldType
                                , SearchValueSelect2: searchValueSelect2
                            });
                        }
                    }

                });

                if (selectedConditions.length == 0) {
                    alert("Add search condition(s)");
                    return false;
                } else {
                    $("[id$=hdnSearchCondition]").val(JSON.stringify(selectedConditions));
                    return true;
                }
            } catch (err) {
                alert("Unknown error occured.");
                return false;
            }
        }

        function SaveAdvancedSearch() {

            if ($("[id$='txtSearchName']").val() == "") {
                alert("Enter name.");
                $("[id$='txtSearchName']").focus();
            } else {
                try {
                    if (getSearchConditions()) {
                        var isConfirmed = false;
                        var selectedConditions = JSON.parse($("[id$=hdnSearchCondition]").val());

                        var selectedModule = 0;
                        selectedConditions.forEach(function (e) {
                            if (e.FormID == 1 && selectedModule != 1) {
                                selectedModule = 1;
                            } else {
                                selectedModule = e.FormID
                                return false;
                            }
                        });

                        if ($("[id$=ddlSavedSearch]").val() != "0") {
                            if (confirm("Selected saved advanced search will be modified. Do you want to proceed?")) {
                                isConfirmed = true;
                            }
                        } else {
                            isConfirmed = true;
                        }

                        if (isConfirmed) {
                            if (selectedModule != 0) {
                                SaveAdvancedSearchPost(selectedModule)
                            } else {
                                alert("Something went wrong.");
                            }
                        }
                    }
                } catch (err) {
                    alert("Unknown error occured.");
                    return false;
                }

                $("[id$='txtSearchName']").val("");
                $("#lbSelectedUsers > option").each(function () {
                    $(this).remove().appendTo("#lbAvailableUsers");
                });

                $("#lbAvailableUsers option:first-child").attr("selected", true);
                $("#lbSelectedUsers option:last-child").attr("selected", true);

                $("[id$=divSaveSearch]").modal("hide");
            }
        }

        function ShowSaveSearch() {
            if ($("[id$='ddlSavedSearch']").val() == "0") {
                $("[id$='txtSearchName']").val("");
            } else {
                $("[id$='txtSearchName']").val($("[id$='ddlSavedSearch'] option:selected").text());
            }

            $("[id$=divSaveSearch]").modal("show");
        }

        function SaveAdvancedSearchPost(formID) {
            $.ajax({
                type: "POST",
                url: '../common/Common.asmx/SaveAdvancedSearch',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                data: JSON.stringify({
                    searchID: $("[id$=ddlSavedSearch]").val(),
                    formID: formID,
                    searchName: $("[id$=txtSearchName]").val(),
                    searchConditions: $("[id$=hdnSearchCondition]").val(),
                    shareWithIdsCommaSeperated: $("#lbSelectedUsers option").map(function () {
                        return $(this).val();
                    }).get().join(','),
                    displayColumns: $("[id$=hdnDisplayColumns]").val()
                }),
                success: function (data) {
                    alert('New Advanced Search Saved Successfully!');
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    alert(textStatus);
                }
            });
        }

        function DeleteSavedAdvancedSearch() {
            if ($("[id$=ddlSavedSearch]").val != "0") {
                if (confirm("Selected saved advanced search will be deleted. Do you want to proceed?")) {
                    $.ajax({
                        type: "POST",
                        url: '../common/Common.asmx/DeleteAdvancedSearch',
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        data: JSON.stringify({
                            searchID: $("[id$=ddlSavedSearch]").val()
                        }),
                        success: function (data) {
                            alert('New Advanced Search Saved Successfully!');
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            alert(textStatus);
                        }
                    });
                }
            } else {
                alert('Select saved advanced search.');
                $("[id$=ddlSavedSearch]").focus();
            }
        }

        function AddUser() {
            var selectedItem = $("#lbAvailableUsers > option:selected");

            if (selectedItem.length > 0) {
                selectedItem.remove().appendTo("#lbSelectedUsers");

                $("#lbAvailableUsers option:first-child").attr("selected", true);
                $("#lbSelectedUsers option:last-child").attr("selected", true);
            }
            else {
                alert("Select user");
            }

            return false;
        }

        function AddAllUsers() {
            $("#lbAvailableUsers > option").each(function () {
                $(this).remove().appendTo("#lbSelectedUsers");
            });

            $("#lbAvailableUsers option:first-child").attr("selected", true);
            $("#lbSelectedUsers option:last-child").attr("selected", true);

            return false;
        }

        function RemoveUser() {
            var selectedItem = $("#lbSelectedUsers > option:selected");

            if (selectedItem.length > 0) {
                selectedItem.remove().appendTo("#lbAvailableUsers");

                $("#lbAvailableUsers option:first-child").attr("selected", true);
                $("#lbSelectedUsers option:last-child").attr("selected", true);
            }
            else {
                alert("Select user");
            }

            return false;
        }

        function ConfigureDisplayColumns() {
            var formID = 1;

            if (getSearchConditions()) {
                var selectedConditions = JSON.parse($("[id$=hdnSearchCondition]").val());

                var selectedModule = 0;
                selectedConditions.forEach(function (e) {
                    if (e.FormID == 1 && selectedModule != 1) {
                        selectedModule = 1;
                    } else {
                        selectedModule = e.FormID
                        return false;
                    }
                });

                if (selectedModule != 0) {
                    window.open('../admin/frmAdvSearchColumnCustomization.aspx?FormId=' + selectedModule + '&IsNewSearch=1&displayColumns=' + $("[id$=hdnDisplayColumns]").val(), "", "width=500,height=300,status=no,scrollbars=yes,left=155,top=160");
                } else {
                    alert("Invalid search module.");
                    return false;
                }
            }

            return false;
        }

        function ClearSearchConditions() {
            totalConditions = 0;
            $("#tblCondition tr").remove();
            $("[id$=hdnDisplayColumns]").val("");
            $("[id$=ddlSavedSearch").val("0");
            selectedFeildsModule = [];

            $("#lbSelectedUsers > option").each(function () {
                $(this).remove().appendTo("#lbAvailableUsers");
            });
        }

        function GetSelectedColumns(displayColumns) {
            $("[id$=hdnDisplayColumns]").val(displayColumns);
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="RecordInformationPanel" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="RecordActionPanel" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="DetailPageTitle" runat="server">
    Advanced Search
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="UtilityLinksPanel" runat="server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="TabsPlaceHolder" runat="server">
    <div class="overlay" id="divLoader" style="display: none">
        <div class="overlayContent" style="color: #000; text-align: center; width: 250px; padding: 20px">
            <i class="fa fa-2x fa-refresh fa-spin"></i>
            <h3>Processing Request</h3>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 col-md-3" style="padding-right: 0px;">
            <div class="box box-primary box-solid">
                <div class="box-header with-border">
                    <h3 class="box-title">Search Fields</h3>

                    <div class="box-tools pull-right">
                    </div>
                    <!-- /.box-tools -->
                </div>
                <!-- /.box-header -->
                <div class="box-body" id="divSearchFields">
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-md-9">
            <div class="row padbottom10">
                <div class="col-xs-12">
                    <div class="form-inline">
                        <label>Saved Search:</label>
                        <asp:DropDownList runat="server" ID="ddlSavedSearch" AutoPostBack="false" class="form-control" Style="width: 400px" onchange="return SavedAdvancedSearchSelected();"></asp:DropDownList>
                        <button id="btnDeleteSavedSearch" class="btn btn-danger" onclick="DeleteSavedAdvancedSearch();">Delete Saved Search</button>
                    </div>
                </div>
            </div>
            <div class="box box-primary box-solid">
                <div class="box-header with-border">
                    <h3 class="box-title">Search Conditions</h3>

                    <div class="box-tools pull-right">
                    </div>
                    <!-- /.box-tools -->
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table style="width: 100%" id="tblCondition">
                        <tbody>
                        </tbody>
                    </table>
                </div>
                <div class="box-footer">
                    <a class="pull-left" href="#" style="line-height: 35px;" onclick="return ConfigureDisplayColumns();">Search Result Columns</a>


                    <button type="button" class="btn btn-danger pull-right" style="margin-left: 5px" onclick="return ClearSearchConditions()">Clear Search</button>
                    <asp:Button runat="server" ID="btnSearch" class="btn btn-primary pull-right" Style="margin-left: 5px" Text="Search" OnClientClick="return getSearchConditions();"></asp:Button>
                    <button type="button" id="btnSaveSearch" class="btn btn-primary pull-right" onclick="ShowSaveSearch();">Save / Modify Saved Search</button>

                    <asp:HiddenField ID="hdnSearchCondition" runat="server" />
                    <asp:HiddenField ID="hdnDisplayColumns" runat="server" />
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modal-options" style="display: none;">
        <div class="modal-dialog" style="width: 90%">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span></button>
                    <h4 class="modal-title">Browse</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-xs-12" id="divOptions">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" onclick="return addSelectedValues()">Add Selected & Close</button>
                    <button type="button" class="btn btn-primary" onclick="return clearSelectedValues()">Clear</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <div class="modal fade" id="divSaveSearch" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span></button>
                    <h4 class="modal-title">Save Advanced Search</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-xs-12 padbottom10">
                            <div class="form-inline">
                                <label>Name:</label>
                                <input id="txtSearchName" type="text" class="form-control" style="width: 300px" />
                            </div>
                        </div>
                    </div>
                    <div class="row" id="divSharedWith" runat="server">
                        <div class="col-xs-12">
                            <h4 style="background-color: #f7f7f7; font-size: 18px; text-align: center; padding: 7px 10px; margin-top: 0;">Share With</h4>
                            <div class="alert alert-info" style="padding: 5px;">
                                Saved search will be added to these user’s “Saved search” list.
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-sm-5">
                                    <b>Available Users</b> <a href="#" onclick="return AddAllUsers();">Add add</a>
                                    <br />
                                    <asp:ListBox ID="lbAvailableUsers" runat="server" Height="200" Width="100%" SelectionMode="Single"></asp:ListBox>
                                </div>
                                <div class="col-xs-12 col-sm-2 text-center" style="margin-top: 20px; padding-left: 0px; padding-right: 0px;">
                                    <button class="btn btn-primary" onclick="return AddUser();">Add<i class="fa fa-arrow-right"></i></button>
                                    <button class="btn btn-danger" onclick="return RemoveUser();" style="margin-top: 20px;"><i class="fa fa-arrow-left"></i>Remove</button>
                                </div>
                                <div class="col-xs-12 col-sm-5">
                                    <b>Selected Users</b>
                                    <br />
                                    <asp:ListBox ID="lbSelectedUsers" runat="server" Height="200" Width="100%" SelectionMode="Single"></asp:ListBox>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" onclick="return SaveAdvancedSearch()">Save</button>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
