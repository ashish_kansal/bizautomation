﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmAdvSerInvItemsRes.aspx.vb"
    Inherits="BACRM.UserInterface.Admin.frmAdvSerInvItemsRes" MasterPageFile="~/common/GridMasterRegular.Master" %>

<%@ Register Assembly="AspNetPager, Version=7.4.4.0, Culture=neutral, PublicKeyToken=fb0a0fe055d40fd4" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>
<%@ Register Src="../common/frmBizSorting.ascx" TagName="frmBizSorting" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <meta http-equiv="Page-Enter" content="blendTrans(Duration=0.01)">
    <meta http-equiv="Page-Exit" content="blendTrans(Duration=0.01)">
    <title></title>
    <script type="text/javascript">
        function EditSearchView() {
            window.open('../admin/frmAdvSearchColumnCustomization.aspx?FormId=29', "", "width=500,height=300,status=no,scrollbars=yes,left=155,top=160")
            return false;
        }

        function MassUpdate() {
            window.open('../admin/frmAdvSearchMassUpdater.aspx?FormID=29', "", "width=500,height=400,status=no,scrollbars=yes,left=155,top=160")
            return false;
        }

        function MassDelete() {
            document.getElementById("txtReload").value = true;
            str = 'Important !  You are about to delete all the records selected.'
            if (confirm(str) == true) {
                return true;
            }
            else {
                return false;
            }
        }

        function PMessage() {
            document.getElementById("txtReload").value = true;
        }

        function SerialNo(a, b) {
            window.open('../Items/frmSerializeitems.aspx?ItemCode=' + a + '&Serialize=' + b, '', 'toolbar=no,titlebar=no,left=200,top=250,width=750,height=550,scrollbars=yes,resizable=yes');
        }

        function OpenItem(a, b) {
            var str;
            str = "../Items/frmKitDetails.aspx?ItemCode=" + a;

            if (b == 1) {
                window.open(str + "&frm=All Items");
            }
            else {
                document.location.href = str + "&frm=AdvSearch";
            }
        }

        function FilterWithinRecords(labelName, DBColumnName, divName) {
            document.getElementById('spColName').innerHTML = labelName.replace(/\+/g, " ");
            document.getElementById('txtColumnName').value = DBColumnName;
            document.getElementById(divName).style.visibility = 'visible';
            document.getElementById('txtColValue').focus();
        }

        function HideDivElement(divName) {
            document.getElementById(divName).style.visibility = 'hidden';
        }

        function getContacts() {
            if (document.getElementById("chkSelectAll").checked == true) {
                window.open('../admin/frmAddEmailGroup.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&SelectAll=true&GetPC=' + document.getElementById("cbPrimaryContactsOnly").checked, "", "width=1000,height=700,status=no,scrollbars=yes,left=155,top=100")
                return false;
            }
            else {
                var RecordIDs = '';
                $("[id$=gvSearch] tr").each(function () {
                    if ($(this).find("#chk").is(':checked')) {
                        RecordIDs = RecordIDs + $(this).find("#lbl1").text() + ',';
                    }
                });

                RecordIDs = RecordIDs.substring(0, RecordIDs.length - 1);

                if (RecordIDs.length > 0) {
                    window.open('../admin/frmAddEmailGroup.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&SelectAll=false&strContID=' + RecordIDs, "", "width=1000,height=700,status=no,scrollbars=yes,left=155,top=100")
                }
                else {
                    alert('Please select atleast one record!!');
                }

                return false;
            }
        }

        function UpdateValues() {
            document.getElementById("txtReload").value = true;
            document.getElementById("btnUpdateValues").click()
        }

        function UpdateVendor() {
            document.getElementById("txtReload").value = true;
            document.getElementById("btnUpdateVendor").click()
        }

        function OpenSearch(a) {
            window.open('../Admin/frmSavedSearch.aspx?FormID=' + a, '', 'toolbar=no,menubar=yes,titlebar=no,left=100,width=400,height=200,scrollbars=yes,resizable=yes');
            return false;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server"
    ClientIDMode="Static">
    <div id="divMessage" runat="server" class="row padbottom10" style="display: none">
        <div class="col-xs-12">
            <div class="alert alert-warning">
                <h4><i class="icon fa fa-warning"></i>Alert!</h4>
                <asp:Literal ID="litMessage" runat="server" EnableViewState="False"></asp:Literal>
            </div>
        </div>
    </div>
    <div class="row padbottom10">
        <div class="col-xs-12">
            <div class="pull-right">
                <asp:CheckBox ID="chkSelectAll" runat="server" Text="Select All" />
                <asp:HyperLink runat="server" CssClass="btn btn-primary" ID="hplSaveSearch" onclick="return OpenSearch(29);" Text="Save this search"></asp:HyperLink>
                <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional" style="display: inline">
                    <ContentTemplate>
                        <asp:Button ID="btnExportToExcel" runat="server" Text="Export to Excel" CssClass="btn btn-primary" CausesValidation="False"></asp:Button>
                    </ContentTemplate>
                    <Triggers>
                        <asp:PostBackTrigger ControlID="btnExportToExcel" />
                    </Triggers>
                </asp:UpdatePanel>
                <asp:Button ID="btnMassUpdate" runat="server" Text="Mass Update" CssClass="btn btn-primary" OnClientClick="return MassUpdate();" CausesValidation="False"></asp:Button>
                <asp:Button ID="btnMassDelete" runat="server" Text="Delete/Mass Delete" CssClass="btn btn-danger" OnClientClick="return MassDelete();" CausesValidation="False"></asp:Button>
                <asp:LinkButton ID="lkbBackToSearchCriteria" CssClass="btn btn-primary" runat="server" CausesValidation="False"><i class="fa fa-arrow-left"></i>&nbsp;&nbsp;Back</asp:LinkButton>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server" ClientIDMode="Static">
    Advanced Search
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BizPager" runat="server" ClientIDMode="Static">
    <webdiyer:AspNetPager ID="bizPager" runat="server"
        PagingButtonSpacing="0"
        CssClass="bizgridpager"
        AlwaysShow="true"
        CurrentPageButtonClass="active"
        PagingButtonUlLayoutClass="pagination"
        PagingButtonLayoutType="UnorderedList"
        FirstPageText="<<"
        LastPageText=">>"
        NextPageText=">"
        PrevPageText="<"
        Width="100%"
        UrlPaging="false"
        NumericButtonCount="8"
        ShowCustomInfoSection="Left"
        ShowPageIndexBox="Never"
        OnPageChanged="bizPager_PageChanged"
        CustomInfoHTML="Showing records %StartRecordIndex% to %EndRecordIndex% of %RecordCount% "
        CustomInfoClass="bizpagercustominfo">
    </webdiyer:AspNetPager>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="GridPlaceHolder" runat="server"
    ClientIDMode="Static">
    <asp:UpdateProgress ID="UpdateProgress" runat="server" ClientIDMode="Static">
        <ProgressTemplate>
            <div class="overlay">
                <div class="overlayContent" style="color: #000; text-align: center; width: 250px; padding: 20px">
                    <i class="fa fa-2x fa-refresh fa-spin"></i>
                    <h3>Processing Request</h3>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <div class="col-xs-12">
        <div class="row">
            <div class="table-responsive">
                <asp:GridView ID="gvSearch" runat="server" EnableViewState="true" AutoGenerateColumns="false" CssClass="table table-bordered table-striped" Width="100%">
                    <Columns>
                    </Columns>
                </asp:GridView>
            </div>
        </div>
    </div>

    <asp:TextBox ID="txtTotalPage" Style="display: none" runat="server"></asp:TextBox>
    <asp:TextBox ID="txtTotalRecords" Style="display: none" runat="server"></asp:TextBox>
    <asp:TextBox ID="txtSortChar" Text="0" Style="display: none" runat="server"></asp:TextBox>
    <asp:TextBox ID="txtSortColumn" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtSortOrder" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtReload" runat="server" Style="display: none">False</asp:TextBox>
    <asp:Button ID="btnUpdateValues" runat="server" Style="display: none" />
    <asp:Button ID="btnUpdateVendor" runat="server" Style="display: none" />
    <asp:TextBox ID="txtCurrrentPage" runat="server" Style="display: none"></asp:TextBox>
    <asp:Button ID="btnGo1" Width="25" runat="server" Style="display: none" />
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="GridBizSorting" runat="server" ClientIDMode="Static">
    <uc1:frmBizSorting ID="frmBizSorting2" runat="server" />
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="GridSettingPopup" runat="server" ClientIDMode="Static">
    <asp:HyperLink runat="server" ID="hplEditResView" ToolTip="Edit advance search result view"><i class="fa fa-lg fa-gear"></i></asp:HyperLink>
</asp:Content>