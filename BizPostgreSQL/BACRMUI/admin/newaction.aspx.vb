
Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Contacts
Imports BACRM.BusinessLogic.Opportunities
Imports BACRM.BusinessLogic.Documents
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Marketing
Imports BACRM.BusinessLogic.Alerts
Imports BACRM.BusinessLogic.Outlook
Imports BACRM.BusinessLogic.Projects
Imports BACRM.BusinessLogic.Case
Imports BACRM.BusinessLogic.Workflow

Namespace BACRM.UserInterface.Admin
    Public Class newaction
        Inherits BACRMPage
        Dim objCampaign As Campaign
        Dim objContacts As CContacts
        Dim objActionItem As ActionItem
        Dim objOutLook As COutlook
        Dim objAlerts As CAlerts
        Dim numcommId As Long = 0
        Dim bError As Boolean = False
        Dim lngCommId As Long
        Dim dsTemp As DataSet

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()

            Try
                CCommon.InitializeOrganizationClientSideTemplate(Session("DomainID"), Session("UserContactID"), radCmbCompany)
                CCommon.UpdateItemRadComboValues("1", radCmbCompany.SelectedValue)
                objCommon.InitializeClientSideTemplate(radCmbItem)

            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

#End Region

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Try
                ' bcz user may delete some action items using Pop window
                ' so we need to load all the items once again
                If Not Request.QueryString.Get("doPostback") Is Nothing Then
                    Dim url As String
                    url = Request.Url.ToString
                    url = url.Replace("&doPostback=yes", "").TrimEnd()
                    'If url.IndexOf("&selectedAction=") >= 0 Then ' if added in previous request then removE IT
                    '    Dim currentValue As String = Request.QueryString.Get("selectedAction")
                    '    url = url.Replace("&selectedAction=" & currentValue, "").TrimEnd()
                    'End If
                    'url = url + "&selectedAction=" & listActionItemTemplate.SelectedIndex.ToString
                    Response.Redirect(url)
                End If
                objCommon = New CCommon
                CCommon.CheckdirtyForm(Page)
                If Not IsPostBack Then
                    Session("AttendeeTable") = Nothing
                    radCmbCompany.Focus()
                    If GetQueryStringVal("Popup") = "True" Then
                        btnCancel.Attributes.Add("onclick", "return Close()")
                        Page.Master.FindControl("webmenu1").Visible = False
                    End If
                    objCommon.sb_FillComboFromDBwithSel(ddlTaskType, 73, Session("DomainID"))

                    If ddlTaskType.Items.FindByValue("974") IsNot Nothing Then
                        ddlTaskType.Items.Remove(ddlTaskType.Items.FindByValue("974"))
                    End If

                    objCommon.sb_FillComboFromDBwithSel(ddlStatus, 447, Session("DomainID"))
                    objCommon.sb_FillComboFromDBwithSel(ddlActivity, 32, Session("DomainID"))
                    objCommon.sb_FillComboFromDBwithSel(ddlFollowUpStatus, 30, Session("DomainID"))
                    sb_FillEmpList()
                    If objCampaign Is Nothing Then objCampaign = New Campaign
                    objCampaign.DomainID = Session("DomainID")
                    ddlEmailTemplate.DataSource = objCampaign.GetEmailTemplates
                    ddlEmailTemplate.DataTextField = "VcDocName"
                    ddlEmailTemplate.DataValueField = "numGenericDocID"
                    ddlEmailTemplate.DataBind()
                    ddlEmailTemplate.Items.Insert(0, "--Select One--")
                    ddlEmailTemplate.Items.FindByText("--Select One--").Value = 0
                End If
                Dim strDate As String = DateAdd(DateInterval.Minute, -Session("ClientMachineUTCTimeOffset"), Date.UtcNow)
                If ddlTaskType.SelectedItem.Value = "973" Then
                    ddlAssignTo.Enabled = False
                    ddltime.Enabled = False
                    chkAM.Disabled = True
                    chkPM.Disabled = True
                    ddlEndTime.Enabled = False
                    chkEndAM.Disabled = True
                    chkEndPM.Disabled = True
                ElseIf ddlTaskType.SelectedItem.Value = "974" Then
                    ddltime.Enabled = False
                    chkAM.Disabled = True
                    chkPM.Disabled = True
                    ddlEndTime.Enabled = False
                    chkEndAM.Disabled = True
                    chkEndPM.Disabled = True
                    ddlAssignTo.Enabled = True
                Else
                    ddlAssignTo.Enabled = True
                    ddltime.Enabled = True
                    chkAM.Disabled = False
                    chkPM.Disabled = False
                    ddlEndTime.Enabled = True
                    chkEndAM.Disabled = False
                    chkEndPM.Disabled = False

                End If

                If chkFollowUp.Checked Then
                    ddltime.Enabled = False
                    chkAM.Disabled = True
                    chkPM.Disabled = True
                    ddlEndTime.Enabled = False
                    chkEndAM.Disabled = True
                    chkEndPM.Disabled = True
                    ddlAssignTo.Enabled = True
                End If

                If GetQueryStringVal("frm") = "Cases" Then
                    btnSave.Visible = True
                Else : btnSave.Visible = False
                End If

                If Not IsPostBack Then
                    ' attach java script evene
                    editActionItem.Attributes.Add("onClick", "OpenActionItemWindow();return false;")
                    editActionItem.NavigateUrl = "../Admin/newaction.aspx?Mode=Edit" & "&ID=-1"
                    If GetQueryStringVal("frm") = "Cases" Then
                        Dim lst As New ListItem
                        lst.Text = "Task"
                        lst.Value = "972"
                        ddlTaskType.Items.Clear()
                        ddlTaskType.Items.Add(lst)
                        'ddlTaskType.Items.FindByValue("971").Selected = True
                        ddlTaskType.Enabled = False
                    End If

                    Dim objUserAccess As UserAccess
                    objUserAccess = New UserAccess

                    Dim dtUserAccessDetails As DataTable
                    objUserAccess.UserId = Session("UserID")
                    objUserAccess.DomainID = Session("DomainID")
                    dtUserAccessDetails = objUserAccess.GetUserAccessDetails
                    If dtUserAccessDetails.Rows.Count > 0 Then
                        If Not ddlTaskType.Items.FindByValue(dtUserAccessDetails.Rows(0).Item("numDefaultTaskType")) Is Nothing Then
                            ddlTaskType.ClearSelection()
                            ddlTaskType.Items.FindByValue(dtUserAccessDetails.Rows(0).Item("numDefaultTaskType")).Selected = True
                        End If

                        If dtUserAccessDetails.Rows(0).Item("tintDefaultRemStatus") = 0 Then
                            chkPopupRemainder.Checked = False
                        Else : chkPopupRemainder.Checked = True
                        End If

                        chkOutlook.Checked = dtUserAccessDetails.Rows(0).Item("bitOutlook")
                    End If

                    cal.SelectedDate = strDate
                    Correspondence1.Mode = 7
                    Correspondence1.bPaging = False


                    If GetQueryStringVal("CntID") <> "" Or GetQueryStringVal("DivID") <> "" Or GetQueryStringVal("ProID") <> "" Or GetQueryStringVal("OpID") <> "" Or GetQueryStringVal("CaseID") <> "" Then
                        If objCommon Is Nothing Then objCommon = New CCommon
                        objCommon.UserCntID = Session("UserContactID")
                        If GetQueryStringVal("CntID") <> "" Then
                            objCommon.ContactID = CCommon.ToLong(GetQueryStringVal("CntID"))
                            objCommon.charModule = "C"
                        ElseIf GetQueryStringVal("DivID") <> "" Then
                            objCommon.DivisionID = CCommon.ToLong(GetQueryStringVal("DivID"))
                            objCommon.charModule = "D"
                        ElseIf GetQueryStringVal("ProID") <> "" Then
                            objCommon.ProID = CCommon.ToLong(GetQueryStringVal("ProID"))
                            objCommon.charModule = "P"
                        ElseIf GetQueryStringVal("OpID") <> "" Then
                            objCommon.OppID = CCommon.ToLong(GetQueryStringVal("OpID"))
                            objCommon.charModule = "O"
                        ElseIf GetQueryStringVal("CaseID") <> "" Then
                            objCommon.CaseID = CCommon.ToLong(GetQueryStringVal("CaseID"))
                            objCommon.charModule = "S"
                        End If
                        objCommon.GetCompanySpecificValues1()
                        Dim strCompanyName As String
                        objContacts = New CContacts
                        objContacts.DivisionID = objCommon.DivisionID
                        strCompanyName = objContacts.GetCompanyName
                        radCmbCompany.Text = strCompanyName
                        radCmbCompany.SelectedValue = objCommon.DivisionID

                        If objCommon.DivisionID Then
                            Correspondence1.lngRecordID = objCommon.DivisionID

                            Correspondence1.getCorrespondance()
                        End If

                        txtcomments.Focus()
                        sb_LoadContacts()
                        If Not ddlTaskContact.Items.FindByValue(objCommon.ContactID) Is Nothing Then ddlTaskContact.Items.FindByValue(objCommon.ContactID).Selected = True
                        SetFollowUpStatus()
                    End If

                    If Request.QueryString.Get("Mode") Is Nothing Then LoadActionItemTemplateData()

                    If GetQueryStringVal("frm") = "outlook" Then
                        Dim objOutlook As New COutlook
                        Dim dttable As DataTable
                        objOutlook.numEmailHstrID = CCommon.ToLong(GetQueryStringVal("EmailHstrId"))
                        objOutlook.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
                        dttable = objOutlook.getMail()
                        If dttable.Rows.Count > 0 Then
                            txtcomments.Text = CCommon.ToString(dttable.Rows(0).Item("vcBodyText")).TrimLength(200)
                        End If
                        Page.Master.FindControl("webmenu1").Visible = False
                    End If

                    lngCommId = CCommon.ToLong(GetQueryStringVal("CommId"))

                    If lngCommId > 0 Then
                        sb_DisplayDetails()
                    End If
                End If
                btnSaveClose.Attributes.Add("onclick", "return Save()")
                hplEmpAvaliability.Attributes.Add("onclick", "return openEmpAvailability();")
                hplTaskType.Attributes.Add("onclick", "return openTaskType();")
                hplPopupRemainder.Attributes.Add("onclick", "return openPopupRemainder(1);")
                hplOutlook.Attributes.Add("onclick", "return openPopupRemainder(2);")
                ddDueDate.Attributes.Add("onchange", "return duedateChage('" & CCommon.GetValidationDateFormat() & "');")
                hplOpenRecord.Attributes.Add("onclick", "return openRecord();")

                hfDateFormat.Value = Session("DateFormat").ToString().ToLower().Replace("m", "M").Replace("Month", "MMM")
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub sb_DisplayDetails()
            Try
                Dim dtActDetails As DataTable
                If objActionItem Is Nothing Then objActionItem = New ActionItem
                objActionItem.CommID = lngCommId
                objActionItem.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
                dtActDetails = objActionItem.GetActionItemDetails



                If dtActDetails.Rows.Count > 0 Then
                    'If dtActDetails.Rows(0).Item("bitTask") = 1 Then
                    '    ddlType.SelectedIndex = 1
                    'ElseIf dtActDetails.Rows(0).Item("bitTask") = 3 Then
                    '    ddlType.SelectedIndex = 0
                    'ElseIf dtActDetails.Rows(0).Item("bitTask") = 4 Then
                    '    ddlType.SelectedIndex = 2
                    'ElseIf dtActDetails.Rows(0).Item("bitTask") = 5 Then
                    '    ddlType.SelectedIndex = 3
                    'End If

                    If Not ddlTaskType.Items.FindByValue(dtActDetails.Rows(0).Item("bitTask")) Is Nothing Then
                        ddlTaskType.ClearSelection()
                        ddlTaskType.Items.FindByValue(dtActDetails.Rows(0).Item("bitTask")).Selected = True
                    End If

                    If Not IsDBNull(dtActDetails.Rows(0).Item("bitFollowUpAnyTime")) Then
                        If (dtActDetails.Rows(0).Item("bitFollowUpAnyTime") = True) Then
                            chkFollowUp.Checked = True
                        Else : chkFollowUp.Checked = False
                        End If
                    End If


                    'txtcomments.Text = dtActDetails.Rows(0).Item("textdetails")
                    cal.SelectedDate = dtActDetails.Rows(0).Item("dtStartTime")
                    'If it has been created from the Tickler section then the assign to will be 0, hence dont assign it
                    If dtActDetails.Rows(0).Item("numAssign") <> 0 Then
                        ddlAssignTo.ClearSelection()
                        Try
                            ddlAssignTo.Items.FindByValue(dtActDetails.Rows(0).Item("numAssign")).Selected = True
                        Catch ex As Exception
                        End Try
                    End If
                    ViewState("AssignedTo") = ddlAssignTo.SelectedValue

                    If Not ddlStatus.Items.FindByValue(dtActDetails.Rows(0).Item("numStatus")) Is Nothing Then
                        ddlStatus.Items.FindByValue(dtActDetails.Rows(0).Item("numStatus")).Selected = True
                    End If

                    If Not ddlActivity.Items.FindByValue(dtActDetails.Rows(0).Item("numActivity")) Is Nothing Then
                        ddlActivity.Items.FindByValue(dtActDetails.Rows(0).Item("numActivity")).Selected = True
                    End If

                    If Not ddlSnooze.Items.FindByValue(dtActDetails.Rows(0).Item("intSnoozeMins")) Is Nothing Then
                        ddlSnooze.Items.FindByValue(dtActDetails.Rows(0).Item("intSnoozeMins")).Selected = True
                    End If

                    If Not ddlRemainder.Items.FindByValue(dtActDetails.Rows(0).Item("intRemainderMins")) Is Nothing Then
                        ddlRemainder.Items.FindByValue(dtActDetails.Rows(0).Item("intRemainderMins")).Selected = True
                    End If

                    If Not IsDBNull(dtActDetails.Rows(0).Item("bitOutlook")) Then
                        If (dtActDetails.Rows(0).Item("bitOutlook") = True) Then
                            chkOutlook.Checked = True
                        Else : chkOutlook.Checked = False
                        End If
                    End If

                    If dtActDetails.Rows(0).Item("tintRemStatus") = 0 Then
                        chkPopupRemainder.Checked = False
                    Else : chkPopupRemainder.Checked = True
                    End If


                    If dtActDetails.Rows(0).Item("bitTask") <> 973 Then
                        ddltime.ClearSelection()
                        If Not ddltime.Items.FindByText(Format(dtActDetails.Rows(0).Item("dtStartTime"), "h:mm")) Is Nothing Then
                            ddltime.Items.FindByText(Format(dtActDetails.Rows(0).Item("dtStartTime"), "h:mm")).Selected = True
                        End If
                        ddlEndTime.ClearSelection()
                        If Not ddlEndTime.Items.FindByText(Format(dtActDetails.Rows(0).Item("dtEndTime"), "h:mm")) Is Nothing Then
                            ddlEndTime.Items.FindByText(Format(dtActDetails.Rows(0).Item("dtEndTime"), "h:mm")).Selected = True
                        End If
                        If Format(dtActDetails.Rows(0).Item("dtStartTime"), "tt") = "AM" Then
                            chkAM.Checked = True
                        Else : chkPM.Checked = True
                        End If
                        If Format(dtActDetails.Rows(0).Item("dtEndTime"), "tt") = "AM" Then
                            chkEndAM.Checked = True
                        Else : chkEndPM.Checked = True
                        End If
                    End If
                    If Not ddlEmailTemplate.Items.FindByValue(dtActDetails.Rows(0).Item("numEmailTemplate")) Is Nothing Then
                        ddlEmailTemplate.Items.FindByValue(dtActDetails.Rows(0).Item("numEmailTemplate")).Selected = True
                    End If
                    If dtActDetails.Rows(0).Item("bitAlert") = False Then
                        chkAlert.Checked = False
                    Else : chkAlert.Checked = True
                    End If
                    rdlEmailTemplate.SelectedValue = IIf(dtActDetails.Rows(0).Item("bitSendEmailTemp") = True, 1, 0)
                    txtHours.Text = dtActDetails.Rows(0).Item("tintHours")
                    If dtActDetails.Rows(0).Item("bitTask") = 973 Then
                        ddlAssignTo.Enabled = False
                        ddltime.Enabled = False
                        chkAM.Disabled = True
                        chkPM.Disabled = True
                        ddlEndTime.Enabled = False
                        chkEndAM.Disabled = True
                        chkEndPM.Disabled = True
                    Else
                        ddlAssignTo.Enabled = True
                        ddltime.Enabled = True
                        chkAM.Disabled = False
                        chkPM.Disabled = False
                        ddlEndTime.Enabled = True
                        chkEndAM.Disabled = False
                        chkEndPM.Disabled = False
                    End If

                    If chkFollowUp.Checked Then
                        ddltime.Enabled = False
                        chkAM.Disabled = True
                        chkPM.Disabled = True
                        ddlEndTime.Enabled = False
                        chkEndAM.Disabled = True
                        chkEndPM.Disabled = True
                    End If

                    If Not ddlAssignTO1.Items.FindByValue(dtActDetails.Rows(0).Item("tintCorrType")) Is Nothing Then
                        ddlAssignTO1.Items.FindByValue(dtActDetails.Rows(0).Item("tintCorrType")).Selected = True
                    End If

                    If ddlAssignTO1.SelectedValue = 7 Then
                        If CCommon.ToLong(dtActDetails.Rows(0).Item("numOpenRecordID")) > 0 Then
                            objCommon = New CCommon
                            objCommon.DomainID = Session("DomainID")

                            Dim vcItemName As String
                            objCommon.Mode = 4
                            objCommon.Str = dtActDetails.Rows(0).Item("numOpenRecordID")
                            vcItemName = objCommon.GetSingleFieldValue()

                            radCmbItem.Text = vcItemName
                            radCmbItem.SelectedValue = dtActDetails.Rows(0).Item("numOpenRecordID")

                            txtAmount.Text = CCommon.GetDecimalFormat(CCommon.ToDecimal(dtActDetails.Rows(0).Item("monMRItemAmount")))

                            trItemPinTo.Visible = True

                            hplOpenRecord.Visible = False
                            ddlOpenRecord.Visible = False
                        End If
                    Else
                        If Not ddlOpenRecord.Items.FindByValue(dtActDetails.Rows(0).Item("numOpenRecordID")) Is Nothing Then
                            ddlOpenRecord.Items.FindByValue(dtActDetails.Rows(0).Item("numOpenRecordID")).Selected = True
                        End If

                        trItemPinTo.Visible = False

                        hplOpenRecord.Visible = True
                        ddlOpenRecord.Visible = True
                    End If

                    If Not ddlFollowUpStatus.Items.FindByValue(dtActDetails.Rows(0).Item("numFollowUpStatus")) Is Nothing Then
                        ddlFollowUpStatus.Items.FindByValue(dtActDetails.Rows(0).Item("numFollowUpStatus")).Selected = True
                    End If

                    'LoadControls()
                End If

            Catch Ex As Exception
                Throw Ex
            End Try
        End Sub
        Private Sub Page_PreRender(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.PreRender

        End Sub

        '//Adding EmployList in dropdown list

        Private Sub sb_FillEmpList()
            Try
                objCommon = New CCommon
                If Session("PopulateUserCriteria") = 1 Then
                    objCommon.DivisionID = CCommon.ToLong(radCmbCompany.SelectedValue)
                    objCommon.charModule = "D"
                    objCommon.GetCompanySpecificValues1()
                    objCommon.sb_FillConEmpFromTerritories(ddlAssignTo, Session("DomainID"), 0, 0, objCommon.TerittoryID)
                ElseIf Session("PopulateUserCriteria") = 2 Then
                    objCommon.sb_FillConEmpFromDBUTeam(ddlAssignTo, Session("DomainID"), Session("UserContactID"))
                Else : objCommon.sb_FillConEmpFromDBSel(ddlAssignTo, Session("DomainID"), 0, 0)
                End If
                'If objContacts Is Nothing Then objContacts = New CContacts
                'objContacts.DomainID = Session("DomainID")
                'ddlAssignTo.DataSource = objContacts.EmployeeList
                'ddlAssignTo.DataTextField = "vcUserName"
                'ddlAssignTo.DataValueField = "numContactID"
                'ddlAssignTo.DataBind()
                If Not ddlAssignTo.Items.FindByValue(Session("UserContactID")) Is Nothing Then
                    ddlAssignTo.ClearSelection()
                    ddlAssignTo.Items.FindByValue(Session("UserContactID")).Text = "My Self"
                    ddlAssignTo.Items.FindByValue(Session("UserContactID")).Selected = True
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub sb_LoadContacts()
            Try
                ddlTaskContact.Items.Clear()
                Dim objOpportunities As New COpportunities
                objOpportunities.DivisionID = radCmbCompany.SelectedValue
                ddlTaskContact.DataSource = objOpportunities.ListContact().Tables(0).DefaultView()
                ddlTaskContact.DataTextField = "ContactType"
                ddlTaskContact.DataValueField = "numcontactId"
                ddlTaskContact.DataBind()
                ddlTaskContact.Items.Insert(0, New ListItem("--Select One--", "0"))
                If ddlTaskContact.Items.Count = 2 Then
                    ddlTaskContact.Items(1).Selected = True
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Function sb_SaveActionItem() As Boolean
            Dim intTaskFlg As Integer   'Task flag for identifying whether task or Communication
            Dim lngCommId As Long
            Dim lngActivityId As Long = 0
            Try

                If ddlTaskType.SelectedValue <> 973 And ddlTaskType.SelectedValue <> 974 And chkFollowUp.Checked = False Then
                    Dim numSTime, numETime As Double
                    numSTime = Double.Parse(ddltime.SelectedItem.Text.Trim.Split(":")(0) + "." + ddltime.SelectedItem.Text.Trim.Split(":")(1))
                    numETime = Double.Parse(ddlEndTime.SelectedItem.Text.Trim.Split(":")(0) + "." + ddlEndTime.SelectedItem.Text.Trim.Split(":")(1))

                    If (chkAM.Checked = True And (numSTime = 12 Or numSTime = 12.3)) Then
                        numSTime = 0
                    End If

                    If (chkEndAM.Checked = True And (numETime = 12 Or numETime = 12.3)) Then
                        numETime = 0
                    End If

                    If chkPM.Checked = True And Not (numSTime = 12 Or numSTime = 12.3) Then
                        numSTime = numSTime + 12
                    End If

                    If chkEndPM.Checked = True And Not (numETime = 12 Or numETime = 12.3) Then
                        numETime = numETime + 12
                    End If

                    If (numSTime > numETime) Then
                        Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "TimeValidation", "alert('End Time must be greater than Start Time.')", True)
                        bError = True
                        Exit Function
                    End If
                End If

                sb_SaveActionItem = True
                Dim strStartTime, strEndTime, strDate As String

                strDate = cal.SelectedDate
                strStartTime = strDate.Trim & " " & ddltime.SelectedItem.Text.Trim & ":00" & IIf(chkAM.Checked = True, " AM", " PM")
                strEndTime = strDate.Trim & " " & ddlEndTime.SelectedItem.Text.Trim & ":00" & IIf(chkEndAM.Checked = True, " AM", " PM")

                'Checking whether the Type is Task or Communication or Notes
                'intTaskFlg = IIf(ddlTaskType.SelectedItem.Text = "Task", 0, 1)
                'If ddlTaskType.SelectedItem.Text = "Communication" Then
                '    intTaskFlg = 1
                'ElseIf ddlTaskType.SelectedItem.Text = "Task" Then
                '    intTaskFlg = 3
                'ElseIf ddlTaskType.SelectedItem.Text = "Notes" Then
                '    intTaskFlg = 4
                'ElseIf ddlTaskType.SelectedItem.Text = "Follow-up Anytime" Then
                '    intTaskFlg = 5
                'ElseIf ddlTaskType.SelectedItem.Text = "" Then
                '    intTaskFlg = 4
                'End If

                Dim strCalendar As String
                If ddlTaskType.SelectedValue <> 973 And ddlTaskType.SelectedValue <> 974 And chkFollowUp.Checked = False Then
                    Dim strSplitStartTimeDate As Date                                               'To store the Date and Time for the start of the Action Item
                    Dim strSplitEndTimeDate As Date         'To store the Date and Time for the end of the Action Item
                    strSplitStartTimeDate = CType(strStartTime, DateTime).AddMinutes(Session("ClientMachineUTCTimeOffset"))        'Convert the Start Date and Time to UT
                    strSplitEndTimeDate = CType(strEndTime, DateTime).AddMinutes(Session("ClientMachineUTCTimeOffset"))           'Convert the End Date and Time to UTC
                    Dim strDesc As String = "Organization: " & radCmbCompany.Text & Environment.NewLine & "Contact Name: " & ddlTaskContact.SelectedItem.Text & Environment.NewLine & "Assigned To: " & ddlAssignTo.SelectedItem.Text & Environment.NewLine & "Comments:" & txtcomments.Text
                    If ddlAssignTo.SelectedItem.Text = "My Self" Then
                        If chkOutlook.Checked = True Then
                            If objOutLook Is Nothing Then objOutLook = New COutlook

                            objOutLook.UserCntID = Session("UserContactId")
                            objOutLook.DomainID = Session("DomainId")
                            Dim dtTable As DataTable
                            dtTable = objOutLook.GetResourceId
                            If dtTable.Rows.Count > 0 Then
                                objOutLook.AllDayEvent = False
                                objOutLook.ActivityDescription = strDesc
                                objOutLook.Location = ""
                                objOutLook.Subject = ddlTaskType.SelectedItem.Text
                                objOutLook.Duration = DateDiff(DateInterval.Second, strSplitStartTimeDate, strSplitEndTimeDate)
                                objOutLook.StartDateTimeUtc = strSplitStartTimeDate
                                objOutLook.EnableReminder = True
                                objOutLook.ReminderInterval = 900
                                objOutLook.ShowTimeAs = 3
                                objOutLook.Importance = 0
                                objOutLook.RecurrenceKey = -999
                                objOutLook.ResourceName = dtTable.Rows(0).Item("ResourceId")
                                lngActivityId = objOutLook.AddActivity()
                            End If
                            'strCalendar = clsProspects.fn_InsertOWAActionItem(Session("SiteType"), strSplitStartTimeDate, strSplitEndTimeDate, Session("ServerName"), Session("UserEmail"), txtComments.Text)
                        End If
                    Else
                        If chkOutlook.Checked = True Then
                            If objOutLook Is Nothing Then objOutLook = New COutlook
                            objOutLook.UserCntID = ddlAssignTo.SelectedItem.Value
                            objOutLook.DomainID = Session("DomainId")
                            Dim dtTable As DataTable
                            dtTable = objOutLook.GetResourceId
                            If dtTable.Rows.Count > 0 Then
                                objOutLook.AllDayEvent = False
                                objOutLook.ActivityDescription = strDesc
                                objOutLook.Location = ""
                                objOutLook.Subject = ddlTaskType.SelectedItem.Text
                                objOutLook.Duration = DateDiff(DateInterval.Second, strSplitStartTimeDate, strSplitEndTimeDate)
                                objOutLook.StartDateTimeUtc = strSplitStartTimeDate
                                objOutLook.EnableReminder = True
                                objOutLook.ReminderInterval = 900
                                objOutLook.ShowTimeAs = 3
                                objOutLook.Importance = 2
                                objOutLook.RecurrenceKey = -999
                                objOutLook.ResourceName = dtTable.Rows(0).Item("ResourceId")
                                lngActivityId = objOutLook.AddActivity()
                            End If
                            '  Dim objContacts As New CContacts
                            ' strCalendar = clsProspects.fn_InsertOWAActionItem(Session("SiteType"), strSplitStartTimeDate, strSplitEndTimeDate, Session("ServerName"), IIf(ddlAssignTo.SelectedItem.Text = "My Self", Session("UserEmail"), objContacts.EmployeeEmail(ddlAssignTo.SelectedItem.Value)), txtComments.Text)
                        End If
                    End If

                    If Session("AttendeeTable") IsNot Nothing Then
                        If chkOutlook.Checked = True Then
                            If objOutLook Is Nothing Then objOutLook = New COutlook

                            dsTemp = Session("AttendeeTable")
                            Dim dtAttendee As DataTable
                            dtAttendee = dsTemp.Tables(0)

                            For Each dr As DataRow In dtAttendee.Rows
                                If dr("tinUserType") = "1" Then
                                    If ddlAssignTo.SelectedItem.Value <> dr("numContactID") Then
                                        objOutLook.UserCntID = dr("numContactID")
                                        objOutLook.DomainID = Session("DomainId")
                                        Dim dtTable As DataTable
                                        dtTable = objOutLook.GetResourceId
                                        If dtTable.Rows.Count > 0 Then
                                            objOutLook.AllDayEvent = False
                                            objOutLook.ActivityDescription = strDesc
                                            objOutLook.Location = ""
                                            objOutLook.Subject = ddlTaskType.SelectedItem.Text
                                            objOutLook.Duration = DateDiff(DateInterval.Second, strSplitStartTimeDate, strSplitEndTimeDate)
                                            objOutLook.StartDateTimeUtc = strSplitStartTimeDate
                                            objOutLook.EnableReminder = True
                                            objOutLook.ReminderInterval = 900
                                            objOutLook.ShowTimeAs = 3
                                            objOutLook.Importance = 2
                                            objOutLook.RecurrenceKey = -999
                                            objOutLook.ResourceName = dtTable.Rows(0).Item("ResourceId")
                                            dr("ActivityID") = objOutLook.AddActivity()
                                        End If
                                    End If
                                End If
                            Next

                            dsTemp.AcceptChanges()
                        End If
                    End If

                End If

                Dim intAssignTo As Integer

                If ddlAssignTo.Enabled = True Then intAssignTo = ddlAssignTo.SelectedItem.Value
                Dim chkSnoozeStatus, chkRemainderStatus As Integer
                If chkPopupRemainder.Checked = True Then
                    chkRemainderStatus = 1
                Else : chkRemainderStatus = 0
                End If
                If ddlSnooze.SelectedItem.Value > 0 Then
                    chkSnoozeStatus = 1
                Else : chkSnoozeStatus = 0
                End If

                objActionItem = New ActionItem

                With objActionItem
                    .CommID = 0
                    .Task = ddlTaskType.SelectedValue

                    .ContactID = ddlTaskContact.SelectedItem.Value
                    .DivisionID = radCmbCompany.SelectedValue
                    .Details = txtcomments.Text
                    .AssignedTo = intAssignTo
                    .UserCntID = Session("UserContactID")
                    .DomainID = Session("DomainID")
                    If ddlTaskType.SelectedValue = 973 Then
                        .BitClosed = 1
                    Else : .BitClosed = 0
                    End If
                    .CalendarName = strCalendar
                    If ddlTaskType.SelectedValue = 973 Or ddlTaskType.SelectedValue = 974 Then
                        .StartTime = cal.SelectedDate
                        .EndTime = .StartTime.ToString("MM/dd/yyyy 23:59:59 ") & "PM"  'DateAdd(DateInterval.Day, 1, .StartTime)
                    Else
                        .StartTime = strStartTime
                        .EndTime = strEndTime
                    End If
                    .Activity = ddlActivity.SelectedItem.Value
                    .Status = ddlStatus.SelectedItem.Value
                    .Snooze = ddlSnooze.SelectedItem.Value
                    .SnoozeStatus = chkSnoozeStatus
                    .Remainder = ddlRemainder.SelectedItem.Value
                    .RemainderStatus = chkRemainderStatus
                    .ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
                    .bitOutlook = IIf(chkOutlook.Checked = True, 1, 0)
                    .SendEmailTemplate = rdlEmailTemplate.SelectedValue
                    .EmailTemplate = ddlEmailTemplate.SelectedValue
                    .Hours = IIf(txtHours.Text = "", 0, txtHours.Text)
                    .Alert = IIf(chkAlert.Checked = True, 1, 0)
                    .ActivityId = lngActivityId
                    .FollowUpAnyTime = IIf(chkFollowUp.Checked = True, 1, 0)
                    If GetQueryStringVal("frm") = "Cases" Then
                        .CaseID = CCommon.ToLong(GetQueryStringVal("CaseID"))
                        '.CaseTimeId = CInt(txtCaseTimeID.Text)
                        '.CaseExpId = CInt(txtCaseExpenseID.Text)
                    End If

                    If Session("AttendeeTable") IsNot Nothing Then
                        dsTemp = Session("AttendeeTable")
                        dsTemp.Tables(0).TableName = "AttendeeTable"

                        .strAttendee = dsTemp.GetXml
                    End If
                End With

                numcommId = objActionItem.SaveCommunicationinfo()

                'Added By Sachin Sadhu||Date:4thAug2014
                'Purpose :To Added Ticker data in work Flow queue based on created Rules
                '          Using Change tracking
                Dim objWfA As New Workflow()
                objWfA.DomainID = Session("DomainID")
                objWfA.UserCntID = Session("UserContactID")
                objWfA.RecordID = numcommId
                objWfA.SaveWFActionItemsQueue()
                'ss//end of code

                AddToRecentlyViewed(RecetlyViewdRecordType.ActionItem, numcommId)

                ''Add To Correspondense if Open record is selected
                If ddlAssignTO1.SelectedValue = 7 Then
                    If Not (radCmbItem.SelectedValue = "") Then
                        objActionItem.CorrespondenceID = 0
                        objActionItem.CommID = numcommId
                        objActionItem.EmailHistoryID = 0
                        objActionItem.CorrType = ddlAssignTO1.SelectedValue
                        objActionItem.OpenRecordID = radCmbItem.SelectedValue
                        objActionItem.DomainID = Session("DomainID")

                        objActionItem.MRItemAmount = CCommon.ToDecimal(txtAmount.Text)
                        objActionItem.ManageCorrespondence()
                    End If
                Else
                    If CCommon.ToLong(ddlOpenRecord.SelectedValue) > 0 Then
                        objActionItem.CorrespondenceID = 0
                        objActionItem.CommID = numcommId
                        objActionItem.EmailHistoryID = 0
                        objActionItem.CorrType = ddlAssignTO1.SelectedValue
                        objActionItem.OpenRecordID = ddlOpenRecord.SelectedValue
                        objActionItem.DomainID = Session("DomainID")
                        objActionItem.ManageCorrespondence()
                    End If
                End If


                'Update FollowUp Status -Bug ID 270
                Dim objAcc As New BACRM.BusinessLogic.Account.CAccounts
                objAcc.ContactID = ddlTaskContact.SelectedItem.Value
                objAcc.FollowUpStatus = ddlFollowUpStatus.SelectedValue
                objAcc.UpdateFollowUpStatus()

                ''Sending mail to Assigneee
                If ViewState("AssignedTo") Is Nothing Then ViewState("AssignedTo") = 0
                If Session("UserContactID") <> ddlAssignTo.SelectedItem.Value Then
                    If ddlAssignTo.SelectedValue > 0 And ViewState("AssignedTo") <> ddlAssignTo.SelectedValue Then
                        CAlerts.SendAlertToAssignee(6, Session("UserContactID"), ddlAssignTo.SelectedValue, numcommId, Session("DomainID")) 'bugid 767
                        ViewState("AssignedTo") = ddlAssignTo.SelectedValue
                    End If
                    'Try
                    '    objAlerts = New CAlerts
                    '    Dim dtDetails As DataTable
                    '    objAlerts.AlertDTLID = 5 'Alert DTL ID for sending alerts in opportunities
                    '    objAlerts.DomainID = Session("DomainID")
                    '    dtDetails = objAlerts.GetIndAlertDTL
                    '    If dtDetails.Rows.Count > 0 Then
                    '        If dtDetails.Rows(0).Item("tintAlertOn") = 1 Then
                    '            Dim dtEmailTemplate As DataTable
                    '            Dim objDocuments As New DocumentList
                    '            objDocuments.GenDocID = dtDetails.Rows(0).Item("numEmailTemplate")
                    '            objDocuments.DomainID = Session("DomainId")
                    '            dtEmailTemplate = objDocuments.GetDocByGenDocID
                    '            If dtEmailTemplate.Rows.Count > 0 Then
                    '                Dim objSendMail As New Email
                    '                Dim dtMergeFields As New DataTable
                    '                Dim drNew As DataRow
                    '                dtMergeFields.Columns.Add("Assignee")
                    '                dtMergeFields.Columns.Add("CommName")
                    '                drNew = dtMergeFields.NewRow
                    '                drNew("Assignee") = ddlAssignTo.SelectedItem.Text
                    '                drNew("CommName") = txtcomments.Text
                    '                dtMergeFields.Rows.Add(drNew)
                    '                If objCommon Is Nothing Then objCommon = New CCommon
                    '                objCommon.byteMode = 0
                    '                objCommon.ContactID = ddlAssignTo.SelectedItem.Value
                    '                objSendMail.SendEmail(dtEmailTemplate.Rows(0).Item("vcSubject"), dtEmailTemplate.Rows(0).Item("vcDocdesc"), IIf(dtDetails.Rows(0).Item("tintCCManager") = 1, objCommon.GetManagerEmail, ""), Session("UserEmail"), objCommon.GetContactsEmail(), dtMergeFields)
                    '            End If
                    '        End If
                    '    End If
                    'Catch ex As Exception
                    '    Throw ex
                    'End Try
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Private Sub btnSaveClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSaveClose.Click
            Try
                sb_SaveActionItem()

                If bError = True Then
                    Exit Sub
                End If

                Session("AttendeeTable") = Nothing

                If GetQueryStringVal("Popup") = "True" Then
                    Dim strScript As String = "<script language=JavaScript>"
                    strScript += "self.close();"
                    strScript += "</script>"
                    If (Not Page.IsStartupScriptRegistered("clientScript")) Then Page.RegisterStartupScript("clientScript", strScript)
                Else : PageDirect()
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Sub PageDirect()
            Try
                If GetQueryStringVal("frm") = "contactdetails" Then
                    Response.Redirect("../contact/frmContacts.aspx?frm=" & GetQueryStringVal("frm1") & "&CntId=" & GetQueryStringVal("CntID") & "&tabkey=Correspondence")
                ElseIf GetQueryStringVal("frm") = "Accounts" Then
                    If objCommon Is Nothing Then objCommon = New CCommon
                    objCommon.ContactID = CCommon.ToLong(GetQueryStringVal("CntID"))
                    objCommon.charModule = "C"
                    objCommon.GetCompanySpecificValues1()
                    Response.Redirect("../account/frmAccounts.aspx?frm=" & GetQueryStringVal("frm1") & "&klds+7kldf=fjk-las&DivId=" & objCommon.DivisionID & "&tabkey=Correspondence")

                ElseIf GetQueryStringVal("frm") = "Prospects" Then
                    If objCommon Is Nothing Then objCommon = New CCommon
                    objCommon.ContactID = CCommon.ToLong(GetQueryStringVal("CntID"))
                    objCommon.charModule = "C"
                    objCommon.GetCompanySpecificValues1()
                    Response.Redirect("../prospects/frmProspects.aspx?frm=" & GetQueryStringVal("frm1") & "&DivID=" & objCommon.DivisionID & "&tabkey=Correspondence")

                ElseIf GetQueryStringVal("frm") = "Leadedetails" Then
                    If objCommon Is Nothing Then objCommon = New CCommon
                    objCommon.ContactID = CCommon.ToLong(GetQueryStringVal("CntID"))
                    objCommon.charModule = "C"
                    objCommon.GetCompanySpecificValues1()
                    Response.Redirect("../Leads/frmLeads.aspx?frm=" & GetQueryStringVal("frm1") & "&DivID=" & objCommon.DivisionID & "&tabkey=Correspondence")

                ElseIf GetQueryStringVal("frm") = "Cases" Then
                    Response.Redirect("../Cases/frmCases.aspx?frm=caselist&CaseID=" & GetQueryStringVal("CaseId") & "&tabkey=Correspondence")

                ElseIf GetQueryStringVal("frm") = "tickler" Then
                    Response.Redirect("../common/frmTicklerDisplay.aspx")
                ElseIf GetQueryStringVal("frm") = "outlook" Then
                    Response.Write("<script>window.close();</script>")
                Else : Response.Redirect("../common/frmTicklerDisplay.aspx")
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
            Try
                Session("AttendeeTable") = Nothing

                PageDirect()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub LoadActionItemTemplateData()
            Try
                If objActionItem Is Nothing Then objActionItem = New ActionItem
                Dim dtActionItems As DataTable
                objActionItem.DomainID = Session("DomainId")
                objActionItem.UserCntID = Session("UserContactId")
                dtActionItems = objActionItem.LoadActionItemTemplateData()

                If Not dtActionItems Is Nothing Then
                    listActionItemTemplate.DataSource = dtActionItems
                    listActionItemTemplate.DataTextField = "TemplateName"
                    listActionItemTemplate.DataValueField = "RowID"
                    listActionItemTemplate.DataBind()
                    listActionItemTemplate.Items.Insert(0, New ListItem("--Select One--", 0))
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub listActionItemTemplate_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles listActionItemTemplate.SelectedIndexChanged
            Try
                If Not listActionItemTemplate.SelectedItem Is Nothing And listActionItemTemplate.SelectedIndex > 0 Then
                    editActionItem.NavigateUrl = ""
                    editActionItem.NavigateUrl = "../Admin/newaction.aspx?Mode=Edit" & "&ID=" & listActionItemTemplate.SelectedItem.Value
                Else : editActionItem.NavigateUrl = "../Admin/newaction.aspx?Mode=Edit" & "&ID=-1"
                End If

                ' get this template data and bind it to current page
                Dim actionItemID As Int32
                Dim thisActionItemTable As DataTable
                If listActionItemTemplate.SelectedIndex > 0 Then
                    actionItemID = Convert.ToInt32(listActionItemTemplate.SelectedValue)
                    thisActionItemTable = LoadThisActionItemData(actionItemID) ' this will data from data base
                    BindTemplateData(thisActionItemTable) ' pass data table contains values
                Else
                    Dim strDate As String = DateAdd(DateInterval.Minute, -Session("ClientMachineUTCTimeOffset"), Date.UtcNow)
                    txtcomments.Text = ""
                    cal.SelectedDate = strDate
                    txtcomments.Text = ""
                    ddlStatus.SelectedIndex = 0
                    ddlActivity.SelectedIndex = 0
                    ddlTaskType.SelectedIndex = 0
                End If
                ' if selected value is 5 then we have to set date as 15/06/1981, Anytime ' eblow code will do same
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Function LoadThisActionItemData(ByVal actionItemID As Int32) As DataTable
            Try
                If objActionItem Is Nothing Then objActionItem = New ActionItem
                With objActionItem
                    .RowID = actionItemID
                End With
                Return objActionItem.LoadThisActionItemTemplateData()
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Private Sub BindTemplateData(ByVal thisActionItemTable As DataTable)
            Try
                If thisActionItemTable.Rows.Count > 0 Then
                    'templateName.Text = thisActionItemTable.Rows(0)("TemplateName")
                    Dim strDate As String = DateAdd(DateInterval.Minute, -Session("ClientMachineUTCTimeOffset"), Date.UtcNow).AddDays(Convert.ToDouble(thisActionItemTable.Rows(0)("DueDays")))
                    cal.SelectedDate = strDate
                    txtcomments.Text = thisActionItemTable.Rows(0)("Comments")
                    ddlStatus.ClearSelection()
                    ddlActivity.ClearSelection()
                    ddlTaskType.ClearSelection()
                    ddlEmailTemplate.ClearSelection()
                    If Not ddlStatus.Items.FindByValue(thisActionItemTable.Rows(0)("Priority")) Is Nothing Then
                        ddlStatus.Items.FindByValue(thisActionItemTable.Rows(0)("Priority")).Selected = True
                    End If
                    If Not ddlActivity.Items.FindByValue(thisActionItemTable.Rows(0)("Activity")) Is Nothing Then
                        ddlActivity.Items.FindByValue(thisActionItemTable.Rows(0)("Activity")).Selected = True
                    End If
                    Dim _type As String = thisActionItemTable.Rows(0)("Type")
                    ddlTaskType.SelectedIndex = -1
                    If Not ddlTaskType.Items.FindByText(_type.Trim()) Is Nothing Then
                        ddlTaskType.Items.FindByText(_type.Trim()).Selected = True
                    End If

                    If Not ddlEmailTemplate.Items.FindByValue(thisActionItemTable.Rows(0).Item("numEmailTemplate")) Is Nothing Then
                        ddlEmailTemplate.Items.FindByValue(thisActionItemTable.Rows(0).Item("numEmailTemplate")).Selected = True
                    End If
                    If thisActionItemTable.Rows(0).Item("bitAlert") = False Then
                        chkAlert.Checked = False
                    Else : chkAlert.Checked = True
                    End If
                    rdlEmailTemplate.SelectedValue = IIf(thisActionItemTable.Rows(0).Item("bitSendEmailTemp") = True, 1, 0)
                    txtHours.Text = thisActionItemTable.Rows(0).Item("tintHours")
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Public Function StripTags(ByVal HTML As String) As String
            Try
                ' Removes tags from passed HTML
                Dim objRegEx As  _
                    System.Text.RegularExpressions.Regex
                Dim str As String = objRegEx.Replace(HTML, "<[^>]*>", "").Replace("P {", "")
                str = str.Replace("MARGIN-TOP: 0px; MARGIN-BOTTOM: 0px", "")
                Return str.Trim
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Private Sub btnLoadData_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLoadData.Click
            Try
                BindTemplateData(LoadThisActionItemData(listActionItemTemplate.SelectedValue))
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
            Try
                sb_SaveActionItem()

                If bError = True Then
                    Exit Sub
                End If
                Session("AttendeeTable") = Nothing
                Response.Redirect("../admin/ActionItemDetailsOld.aspx?frm=Cases&CommId=" & numcommId & "&CaseId=" & GetQueryStringVal("CaseId"))
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub radCmbCompany_ItemsRequested(ByVal o As Object, ByVal e As Telerik.Web.UI.RadComboBoxItemsRequestedEventArgs) Handles radCmbCompany.ItemsRequested
            Try
                If e.Text <> "" And Len(e.Text) >= IIf(Session("ChrForCompSearch") = 0, 1, Session("ChrForCompSearch")) Then

                    If objCommon Is Nothing Then objCommon = New CCommon
                    With objCommon
                        .DomainID = Session("DomainID")
                        .Filter = Trim(e.Text) & "%"
                        .UserCntID = Session("UserContactID")
                        radCmbCompany.DataSource = objCommon.PopulateOrganization.Tables(0).DefaultView
                        radCmbCompany.DataTextField = "vcCompanyname"
                        radCmbCompany.DataValueField = "numDivisionID"
                        radCmbCompany.DataBind()
                    End With
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub radCmbCompany_SelectedIndexChanged(ByVal o As Object, ByVal e As Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs) Handles radCmbCompany.SelectedIndexChanged
            Try
                ' btnTime.Attributes.Add("onclick", "openTime('" & GetQueryStringVal( "CaseId") & "','" & GetQueryStringVal( "CntId") & "','" & ddlCompanyName.SelectedValue & "','" & txtCaseTimeID.Text & "')")
                'btnExpense.Attributes.Add("onclick", "openExpense('" & GetQueryStringVal( "CaseId") & "','" & GetQueryStringVal( "CntId") & "','" & ddlCompanyName.SelectedValue & "','" & txtCaseExpenseID.Text & "')")
                If radCmbCompany.SelectedValue <> "" Then
                    sb_LoadContacts()
                    sb_FillEmpList()
                    SetFollowUpStatus()

                    Correspondence1.lngRecordID = radCmbCompany.SelectedValue
                    Correspondence1.Mode = 7
                    Correspondence1.bPaging = False
                    Correspondence1.getCorrespondance()

                    CCommon.UpdateItemRadComboValues("1", radCmbCompany.SelectedValue)
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub ddlAssignTO1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAssignTO1.SelectedIndexChanged
            Try
                ddlOpenRecord.Items.Clear()

                If ddlAssignTO1.SelectedValue = 7 Then
                    trItemPinTo.Visible = True

                    hplOpenRecord.Visible = False
                    ddlOpenRecord.Visible = False
                Else
                    If ddlAssignTO1.SelectedValue > 0 Then
                        objContacts = New CContacts
                        objContacts.FillOpenRecords(CCommon.ToLong(radCmbCompany.SelectedValue), Session("DomainId"), ddlOpenRecord, ddlAssignTO1.SelectedValue)
                    End If

                    trItemPinTo.Visible = False

                    hplOpenRecord.Visible = True
                    ddlOpenRecord.Visible = True
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub
        Sub SetFollowUpStatus()
            Try
                Dim objLeads As New BACRM.BusinessLogic.Leads.LeadsIP
                objLeads.DivisionID = radCmbCompany.SelectedValue
                objLeads.DomainID = Session("DomainID")
                objLeads.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
                objLeads.GetCompanyDetails()
                ddlFollowUpStatus.ClearSelection()
                If Not ddlFollowUpStatus.Items.FindByValue(objLeads.FollowUpStatus) Is Nothing Then
                    ddlFollowUpStatus.Items.FindByValue(objLeads.FollowUpStatus).Selected = True
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
    End Class
End Namespace