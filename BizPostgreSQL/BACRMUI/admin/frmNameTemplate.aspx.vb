﻿Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Common
Public Class frmNameTemplate
    Inherits BACRMPage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then
                BindData()

                btnClose.Attributes.Add("onclick", "return Close()")
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Sub BindData()
        Try
            Dim objAdmin As New CAdmin
            objAdmin.DomainID = Session("DomainID")
            objAdmin.byteMode = 0

            gvNameTemplate.DataSource = objAdmin.ManageNameTemplate(1)
            gvNameTemplate.DataBind()

            gvBizDoc.DataSource = objAdmin.ManageNameTemplate(2)
            gvBizDoc.DataBind()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            Dim dtTable As New DataTable
            dtTable.TableName = "Table"
            Dim ds As New DataSet
            dtTable.Columns.Add("numRecordID")
            dtTable.Columns.Add("vcModuleName")
            dtTable.Columns.Add("vcNameTemplate")
            dtTable.Columns.Add("numSequenceId")
            dtTable.Columns.Add("numMinLength")
            Dim dr As DataRow
            For Each gvr As GridViewRow In gvNameTemplate.Rows
                dr = dtTable.NewRow
                dr("numRecordID") = gvNameTemplate.DataKeys(gvr.RowIndex).Value
                dr("vcModuleName") = CType(gvr.FindControl("lblModuleName"), Label).Text
                dr("vcNameTemplate") = CType(gvr.FindControl("txtNameTemplate"), TextBox).Text
                dr("numSequenceId") = CType(gvr.FindControl("lblSequence"), Label).Text
                dr("numMinLength") = CType(gvr.FindControl("txtMinLength"), TextBox).Text
                dtTable.Rows.Add(dr)
            Next
            ds.Tables.Add(dtTable)
            Dim objAdmin As New CAdmin
            objAdmin.DomainID = Session("DomainID")
            objAdmin.strItems = ds.GetXml
            objAdmin.byteMode = 1
            objAdmin.ManageNameTemplate(1)

        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub


    Private Sub btnSaveBizDoc_Click(sender As Object, e As System.EventArgs) Handles btnSaveBizDoc.Click
        Try
            Dim dtTable As New DataTable
            dtTable.TableName = "Table"
            Dim ds As New DataSet
            dtTable.Columns.Add("numRecordID")
            dtTable.Columns.Add("vcModuleName")
            dtTable.Columns.Add("vcNameTemplate")
            dtTable.Columns.Add("numSequenceId")
            dtTable.Columns.Add("numMinLength")
            Dim dr As DataRow
            For Each gvr As GridViewRow In gvBizDoc.Rows
                dr = dtTable.NewRow
                dr("numRecordID") = gvBizDoc.DataKeys(gvr.RowIndex).Value
                dr("vcNameTemplate") = CType(gvr.FindControl("txtNameTemplate"), TextBox).Text
                dr("numSequenceId") = CType(gvr.FindControl("lblSequence"), Label).Text
                dr("numMinLength") = CType(gvr.FindControl("txtMinLength"), TextBox).Text
                dtTable.Rows.Add(dr)
            Next
            ds.Tables.Add(dtTable)
            Dim objAdmin As New CAdmin
            objAdmin.DomainID = Session("DomainID")
            objAdmin.strItems = ds.GetXml
            objAdmin.byteMode = 1
            objAdmin.ManageNameTemplate(2)

        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
End Class