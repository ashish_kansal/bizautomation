﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class frmImport

    '''<summary>
    '''hdnItemLinkingID control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hdnItemLinkingID As Global.System.Web.UI.WebControls.HiddenField

    '''<summary>
    '''litMessage control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents litMessage As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''hdnFileSize control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hdnFileSize As Global.System.Web.UI.WebControls.HiddenField

    '''<summary>
    '''pnlStep1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlStep1 As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''rdbupdate control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents rdbupdate As Global.System.Web.UI.WebControls.RadioButtonList

    '''<summary>
    '''ddlImport control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlImport As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''trOrderImport control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents trOrderImport As Global.System.Web.UI.HtmlControls.HtmlTableRow

    '''<summary>
    '''rblImportOrderItemIdentifier control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents rblImportOrderItemIdentifier As Global.System.Web.UI.WebControls.RadioButtonList

    '''<summary>
    '''ddlNoOfOrders control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlNoOfOrders As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''divSingleOrder control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents divSingleOrder As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''rbExistingCustomer control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents rbExistingCustomer As Global.System.Web.UI.WebControls.RadioButton

    '''<summary>
    '''radCmbCompany control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents radCmbCompany As Global.Telerik.Web.UI.RadComboBox

    '''<summary>
    '''ddlContact control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlContact As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''rbNewCustomer control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents rbNewCustomer As Global.System.Web.UI.WebControls.RadioButton

    '''<summary>
    '''txtNewOrganization control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtNewOrganization As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtFirstName control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtFirstName As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtLastName control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtLastName As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtEmail control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtEmail As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''divMultipleOrders control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents divMultipleOrders As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''rbMatchUsingOrganizationID control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents rbMatchUsingOrganizationID As Global.System.Web.UI.WebControls.RadioButton

    '''<summary>
    '''rbMatchUsingField control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents rbMatchUsingField As Global.System.Web.UI.WebControls.RadioButton

    '''<summary>
    '''ddlMatchField control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlMatchField As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''trAddressType control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents trAddressType As Global.System.Web.UI.HtmlControls.HtmlTableRow

    '''<summary>
    '''rdbList control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents rdbList As Global.System.Web.UI.WebControls.RadioButtonList

    '''<summary>
    '''trOptions control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents trOptions As Global.System.Web.UI.HtmlControls.HtmlTableRow

    '''<summary>
    '''ddlItemGroup control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlItemGroup As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''chkLotNo control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents chkLotNo As Global.System.Web.UI.WebControls.CheckBox

    '''<summary>
    '''chkSerializedItem control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents chkSerializedItem As Global.System.Web.UI.WebControls.CheckBox

    '''<summary>
    '''updatedownload1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents updatedownload1 As Global.System.Web.UI.UpdatePanel

    '''<summary>
    '''lbDownloadCSV control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lbDownloadCSV As Global.System.Web.UI.WebControls.LinkButton

    '''<summary>
    '''lbDownloadCSVWithData control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lbDownloadCSVWithData As Global.System.Web.UI.WebControls.LinkButton

    '''<summary>
    '''btnConfg control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnConfg As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''trUpload control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents trUpload As Global.System.Web.UI.HtmlControls.HtmlTableRow

    '''<summary>
    '''uploadpanel2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents uploadpanel2 As Global.System.Web.UI.UpdatePanel

    '''<summary>
    '''txtFile control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtFile As Global.System.Web.UI.HtmlControls.HtmlInputFile

    '''<summary>
    '''btnUpload control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnUpload As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''trInfo control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents trInfo As Global.System.Web.UI.HtmlControls.HtmlTableRow

    '''<summary>
    '''Panel1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Panel1 As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''pnlStep2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlStep2 As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''rptrParent control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents rptrParent As Global.System.Web.UI.WebControls.Repeater

    '''<summary>
    '''pnlStep3 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlStep3 As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''btnContinue control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnContinue As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''gvPreviewData control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents gvPreviewData As Global.System.Web.UI.WebControls.GridView

    '''<summary>
    '''pnlStep4 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlStep4 As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''pnlStep5 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlStep5 As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''olError control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents olError As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''btnOK control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnOK As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''pnlStep6 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlStep6 As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''btnImport control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnImport As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''btnCancelImport control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnCancelImport As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''btnPreImportAnalysis control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnPreImportAnalysis As Global.System.Web.UI.WebControls.Button
End Class
