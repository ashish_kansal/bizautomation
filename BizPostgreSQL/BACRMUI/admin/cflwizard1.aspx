<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" CodeBehind="cflwizard1.aspx.vb"
    Inherits="BACRM.UserInterface.Admin.cflwizard1" MasterPageFile="~/common/Popup.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
     <title>Custom Field Wizard</title>
    <style>
        .info
        {
            color: #00529B;
            background-color: #BDE5F8;
            background-image: url('../images/info.png');
            border: 1px solid;
            margin: 10px 5px 0px 5px;
            padding: 15px 10px 15px 50px;
            background-repeat: no-repeat;
            background-position: 10px center;
            font-size: 8pt;
        }
    </style>
    <script language="javascript">
        function Close() {
            window.close()
            return false;
        }
        function Back() {
            window.location.href = "../admin/cflwizard2.aspx"
            return false;
        }
        function Next() {
            if (document.getElementById('subgrp').value == -1) {
                alert("Select Tab")
                document.getElementById('subgrp').focus()
                return false;
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
   
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageTitle" runat="server" ClientIDMode="Static">
    Step 2
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="Content" runat="server" ClientIDMode="Static">
   <table width="400px" height="100%" class="aspTableDTL">
        <tr>
            <td class="normal2" align="center" colspan="2">
                <br>
                &nbsp;&nbsp;Select the Tab view in which the Custom field<br>
                &nbsp; to be placed.
                <br>
                <br>
                <br>
                <br>
            </td>
        </tr>
        <tr>
            <td align="right" class="normal1" valign="top">
                Select Tab View
            </td>
            <td valign="top">
                &nbsp;<asp:DropDownList ID="subgrp" runat="server" Width="150" CssClass="signup">
                </asp:DropDownList>
            </td>
        </tr>
        <tr id="trMessage" runat="server">
            <td colspan="2" style="height: 50px">
                <asp:Panel runat="server" ID="pnlSMTPError" CssClass="info" Visible="false">
                    Plese Add Sub tab for Normal Fields from Administration->Manage Authorization->Manage
                    Sub Tabs.
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td align="center" colspan="2">
                <asp:Button ID="btnBack" Width="45" runat="server" CssClass="button" Text="Back">
                </asp:Button>
                <asp:Button ID="btnNext" Width="45" runat="server" CssClass="button" Text="Next">
                </asp:Button>
                <asp:Button ID="btnCancel" Width="50" runat="server" CssClass="button" Text="Close">
                </asp:Button>
                <br/>
                <br/>
                <br/>
                <br/>
                <br/>
                <br />
            </td>
        </tr>
    </table>
</asp:Content>
