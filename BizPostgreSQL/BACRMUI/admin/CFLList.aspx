<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" CodeBehind="CFLList.aspx.vb"
    Inherits="BACRM.UserInterface.Admin.CFLList" MasterPageFile="~/common/GridMasterRegular.Master" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Custom Fields</title>
    <script language="javascript">
        function DeleteRecord() {
            if (confirm('The following field and all its data will be permanently deleted from the system. Are you sure you want to do this ?')) {
                return true;
            }
            else {
                return false;
            }
        }
        function DeleteMessage() {
            alert("You Are not Authorized to Delete the Selected Record !");
            return false;
        }
        function EditMessage() {
            alert("You Are not Authorized to Edit the Selected Record !");
            return false;
        }
        function OpenAdminlnk() {
            window.location.href = "../admin/frmAdminSection.aspx"
            return false;
        }
        function OpenEditCF() {
            window.location.href = "../admin/frmEditCustomFields.aspx"
            return false;
        }
        function fldwiz() {
            var LoWindow = window.open('cflwizard2.aspx?location=' + document.getElementById('ddlFilter').value, 'wizard', 'width=800,height=300,status=no,titlebar=no,scrollbar=yes,resizable=yes,top=110,left=150');
            LoWindow.focus();
            return false;
        }
        function fnDelcfld(fldid) {
            if (confirm('Delete custom Field: All data for this field will be lost - Are You sure?')) {
                document.form1.action = "cfllist.aspx?Mode=Delcfld&fldid=" + fldid;
                document.form1.submit();

            }
        }

        function OpenSumm(a) {
            window.open("../admin/Cflwizardsum.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&fldid=" + a, '', 'width=400,height=300,status=no,titlebar=no,resizable=0,scrollbar=yes,top=110,left=150')
            return false;
        }
        function OpenValidation(a, b) {
            window.open("../admin/CflValidation.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&fldid=" + a + '&fldType=' + b, '', 'width=400,height=250,status=no,titlebar=no,resizable=0,scrollbar=yes,top=110,left=150')
            return false;
        }
        function OpenHelp() {
            window.open('../Help/Admin-Custom_Fields.html', '', 'toolbar=no,titlebar=no,top=200,left=200,width=550,height=350,scrollbars=yes,resizable=yes')
            return false
        }
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server"
    ClientIDMode="Static">
    <div class="row padbottom10">
        <div class="col-xs-12">
            <div class="pull-left">
                <div class="form-inline">
                    <label>Location:</label>
                    <asp:DropDownList runat="server" AutoPostBack="true" ID="ddlFilter" CssClass="form-control">
                    </asp:DropDownList>
                </div>
            </div>
            <div class="pull-right">
                <asp:LinkButton ID="btnCreateFields" runat="server" CssClass="btn btn-primary"><i class="fa fa-plus-circle"></i>&nbsp;&nbsp;Create New Fields</asp:LinkButton>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server" ClientIDMode="Static">
    Custom Fields&nbsp;<a href="#" onclick="return OpenHelpPopUp('admin/CFLList.aspx')"><label class="badge bg-yellow">?</label></a>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BizPager" runat="server" ClientIDMode="Static">
    &nbsp;
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="GridPlaceHolder" runat="server"
    ClientIDMode="Static">
    <asp:UpdateProgress ID="UpdateProgress" runat="server" ClientIDMode="Static">
        <ProgressTemplate>
            <div class="overlay">
                <div class="overlayContent" style="color: #000; text-align: center; width: 250px; padding: 20px">
                    <i class="fa fa-2x fa-refresh fa-spin"></i>
                    <h3>Processing Request</h3>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <div class="row">
        <div class="col-xs-12">
            <div class="pull-right">
                <asp:Button ID="btnSaveCustomField" runat="server" Text="Save" CssClass="btn btn-primary" OnClick="btnSaveCustomField_Click" />
            </div>
            <div class="clearfix"></div>
            <div class="table-responsive">
                <asp:DataGrid ID="dgCustomFields" runat="server" Width="100%" CssClass="table table-striped table-bordered" AutoGenerateColumns="False"
                    AllowSorting="True" UseAccessibleHeader="true">
                    <Columns>
                        <asp:TemplateColumn Visible="false">
                            <ItemTemplate>
                                <asp:Label ID="lblFldId" runat="server" Visible="False" Text='<%# DataBinder.Eval(Container.DataItem, "fld_id") %>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Field Name" SortExpression="Fld_label" HeaderStyle-HorizontalAlign="Center"
                            ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:HyperLink ID="hplFldName" NavigateUrl="#" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Fld_label") %>'>
                                </asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:BoundColumn DataField="fld_type" SortExpression="fld_type" HeaderText="Field Type"></asp:BoundColumn>
                        <asp:BoundColumn DataField="TabName" SortExpression="TabName" HeaderText="Tab Name"></asp:BoundColumn>
                         <asp:TemplateColumn HeaderText="Location">
                            <ItemStyle HorizontalAlign="Center" />
                            <ItemTemplate>
                                <asp:HiddenField ID="hdnCustomFieldItemLocation" Value='<%# DataBinder.Eval(Container.DataItem, "vcItemsLocation") %>' runat="server" />
                               <asp:Label ID="lblLocation" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "loc_name") %>'></asp:Label>
                                <telerik:RadComboBox ID="rcbItems" runat="server" CheckBoxes="true">
        <Items>
            <telerik:RadComboBoxItem Text="Services" Value="1" />
            <telerik:RadComboBoxItem Text="Kits" Value="2" />
            <telerik:RadComboBoxItem Text="Assemblies" Value="3" />
            <telerik:RadComboBoxItem Text="Inventory Items" Value="4" />
            <telerik:RadComboBoxItem Text="Non-Inventory Items" Value="5" />
            <telerik:RadComboBoxItem Text="Serialized/LOT #s Items" Value="6" />
            <telerik:RadComboBoxItem Text="Matrix Items" Value="7" />
            <telerik:RadComboBoxItem Text="Asset Items" Value="8" />
        </Items>
    </telerik:RadComboBox>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <%--<asp:BoundColumn DataField="loc_name" SortExpression="loc_name" HeaderText="Location"></asp:BoundColumn>--%>
                        <asp:BoundColumn Visible="true" HeaderText="Validation" />
                        <asp:TemplateColumn HeaderText="Set Validation">
                            <ItemStyle HorizontalAlign="Center" />
                            <ItemTemplate>
                                <a href="#" onclick="javascript:OpenValidation('<%# DataBinder.Eval(Container.DataItem, "fld_id") %>','<%# DataBinder.Eval(Container.DataItem, "fld_type") %>')"
                                    class="hyperlink">Set Validation</a>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn ItemStyle-Width="25">
                            <ItemTemplate>
                                <asp:LinkButton ID="btnDelete" runat="server" CssClass="btn btn-danger btn-xs" CommandName="Delete"><i class="fa fa-trash"></i></asp:LinkButton>
                                <asp:LinkButton ID="lnkdelete" runat="server" Visible="false">
											<font color="#730000">*</font></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                    <PagerStyle Visible="False" ForeColor="#000066" BackColor="White" Mode="NumericPages"></PagerStyle>
                </asp:DataGrid>
            </div>
        </div>
    </div>

</asp:Content>
