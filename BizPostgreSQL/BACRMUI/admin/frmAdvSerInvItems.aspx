﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmAdvSerInvItems.aspx.vb"
    Inherits="BACRM.UserInterface.Admin.frmAdvSerInvItems" MasterPageFile="~/common/DetailPage.Master" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Src="frmAdvSearchCriteria.ascx" TagName="frmAdvSearchCriteria" TagPrefix="uc1" %>
<%@ Import Namespace="BACRM.BusinessLogic.Common" %>
<%@ Import Namespace="BACRM.BusinessLogic.Admin" %>
<%@ Register Src="frmMySavedSearch.ascx" TagName="frmMySavedSearch" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title></title>
    <link rel="stylesheet" type="text/css" href="../CSS/jscal2.css" />
    <link rel="stylesheet" type="text/css" href="../CSS/border-radius.css" />
    <script type="text/javascript" src="../JavaScript/jscal2.js"></script>
    <script type="text/javascript" src="../JavaScript/en.js"></script>
    <script src="../javascript/AdvSearchScripts.js" type="text/javascript"></script>
    <script src="../javascript/AdvSearchColumnCustomization.js" type="text/javascript"></script>
    <script language="vb" runat="server">
        
        Private Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
            Try
                Session("VendorIDs") = Nothing
                Session("WhereConditionItem") = GetSearchString()
                Session("SavedSearchCondition") = Session("WhereConditionItem")
                FormGenericAdvSearch.SaveSearchCriteria(hdnSaveSearchCriteria.Value, 29)
                
                'Response.Write(Session("WhereConditionItem"))
                
                Response.Redirect(Page.ResolveClientUrl("~/Admin/frmAdvSerInvItemsRes.aspx"), False)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub
        Public Function GetSearchString() As String
            Try
                Dim sb, sbFriendlyQuery As New StringBuilder
                Dim i As Integer
                Dim dr As System.Data.DataRow
                Dim ControlType, strFieldName, strFieldValue, strFieldID, strFriendlyName, strControlID As String
                For i = 0 To Me.dtGenericFormConfig.Rows.Count - 1
                    dr = dtGenericFormConfig.Rows(i)
                    
                    strFieldID = dr("numFormFieldID").ToString.Replace("C", "").Replace("D", "").Replace("R", "")
                    ControlType = dr("vcAssociatedControlType").ToString.ToLower.Replace(" ", "") 'replace whitespace with blank 
                    strFieldName = dr("vcDbColumnName").ToString.Trim.Replace(" ", "_")
                    strFriendlyName = dr("vcFormFieldName").ToString
                    strControlID = dr("numFormFieldID").ToString & "_" & dr("vcDbColumnName").ToString.Trim.Replace(" ", "_")
                    
                    'Set field value based on control type
                    Select Case ControlType
                        Case "selectbox"
                            strFieldValue = IIf(CType(plhControls.FindControl(strControlID), DropDownList).SelectedIndex > 0, CType(plhControls.FindControl(strControlID), DropDownList).SelectedValue, 0)
                            If CCommon.ToLong(strFieldValue) > 0 Or strFieldName = "charItemType" Then FormGenericAdvSearch.GenerateUserFriendlySearchString(sbFriendlyQuery, strFriendlyName, strFieldName, CType(plhControls.FindControl(strControlID), DropDownList).SelectedItem.Text, hdnSaveSearchCriteria.Value.Trim, "Equal To")
                        Case "listbox"
                            strFieldValue = ""
                            Dim lstBox As Telerik.Web.UI.RadComboBox = CType(plhControls.FindControl(strControlID), Telerik.Web.UI.RadComboBox)
                            Dim fldText As String = ""
                            
                            For Each item As RadComboBoxItem In lstBox.CheckedItems
                                strFieldValue = strFieldValue & item.Value & ","
                                fldText = fldText & item.Text & " or "
                            Next
                            
                            strFieldValue = strFieldValue.TrimEnd(",")
                            If strFieldValue.Length > 1 Then FormGenericAdvSearch.GenerateUserFriendlySearchString(sbFriendlyQuery, strFriendlyName, strFieldName, fldText.Trim.TrimEnd("r").TrimEnd("o"), hdnSaveSearchCriteria.Value.Trim, "Equals To")
                        Case "checkboxlist"
                            strFieldValue = ""
                            Dim chkbl As CheckBoxList = CType(plhControls.FindControl(strControlID), CheckBoxList)
                            Dim fldText As String = ""

                            For Each item As ListItem In chkbl.Items
                                If item.Selected Then
                                    strFieldValue = strFieldValue & If(strFieldValue.Length > 0, " OR ", "") & " CONCAT(',',Fld_Value,',') ILIKE '%," & item.Value & ",%'"
                                    fldText = fldText & item.Text & " or "
                                End If
                            Next

                            strFieldValue = strFieldValue.TrimEnd("OR")
                            If strFieldValue.Length > 1 Then FormGenericAdvSearch.GenerateUserFriendlySearchString(sbFriendlyQuery, strFriendlyName, strFieldName, fldText.Trim.TrimEnd("r").TrimEnd("o"), hdnSaveSearchCriteria.Value.Trim, "Equals To")
                        Case "checkbox"
                            strFieldValue = IIf(CType(plhControls.FindControl(strControlID), DropDownList).SelectedIndex > 0, CType(plhControls.FindControl(strControlID), DropDownList).SelectedValue, -1)
                            If CCommon.ToLong(strFieldValue) > -1 Then FormGenericAdvSearch.GenerateUserFriendlySearchString(sbFriendlyQuery, strFriendlyName, strFieldName, CType(plhControls.FindControl(strControlID), DropDownList).SelectedItem.Text, hdnSaveSearchCriteria.Value.Trim, "Equal To")
                        Case "datefield"
                            strFieldValue = CType(plhControls.FindControl(strControlID), BACRM.Include.calandar).SelectedDate
                            If CType(plhControls.FindControl("hdn_" & strControlID), HiddenField).Value = 11 Then 'between condition is set
                                If strFieldValue <> "" Then FormGenericAdvSearch.GenerateUserFriendlySearchString(sbFriendlyQuery, strFriendlyName, strFieldName, CType(plhControls.FindControl("hdn_" & strControlID & "_value"), HiddenField).Value, hdnSaveSearchCriteria.Value.Trim, "Between")
                            Else
                                If strFieldValue <> "" Then FormGenericAdvSearch.GenerateUserFriendlySearchString(sbFriendlyQuery, strFriendlyName, strFieldName, strFieldValue, hdnSaveSearchCriteria.Value.Trim, "Equal To")
                            End If
                        Case Else
                            strFieldValue = Replace(CType(plhControls.FindControl(strControlID), TextBox).Text.Trim, "'", "''")
                            If strFieldValue <> "" Then FormGenericAdvSearch.GenerateUserFriendlySearchString(sbFriendlyQuery, strFriendlyName, strFieldName, strFieldValue, hdnSaveSearchCriteria.Value.Trim, IIf(dr("vcFieldDataType").ToString.ToLower = "v", "Contains", "Equal To"))
                    End Select
                    
                    
                    
                    If ControlType = "selectbox" Then 'Dropdown
                        If dr("vcFieldType") = "R" Then
                            If strFieldValue <> "0" Then
                                If strFieldName = "numBaseUnit" Or strFieldName = "numPurchaseUnit" Or strFieldName = "numSaleUnit" Then
                                    sb.Append(" and " & FormGenericAdvSearch.GetSearchCondition(hdnFilterCriteria.Value, strFieldName, strControlID, CType(plhControls.FindControl(strControlID), DropDownList).SelectedItem.Value, dr("vcFieldDataType")))
                                ElseIf strFieldName = "charItemType" Then
                                    sb.Append(" and " & strFieldName & " ='" & strFieldValue & "'")
                                Else
                                    sb.Append(" and " & strFieldName & " = " & strFieldValue)
                                End If
                            End If
                        ElseIf dr("vcFieldType") = "C" Or dr("vcFieldType") = "P" Then
                            If strFieldValue <> "0" Then
                                sb.Append(" and I.numItemCode in (select RecId from CFW_FLD_Values_Item where Fld_ID =" & strFieldID & " and Fld_Value = '" & strFieldValue & "' )")
                            End If
                        ElseIf dr("vcFieldType") = "D" Then
                            If strFieldValue <> "0" Then
                                sb.Append(" and DM.numDivisionID in (select RecId from CFW_FLD_Values_Item where Fld_ID =" & strFieldID & " and Fld_Value = '" & strFieldValue & "' )")
                            End If
                        ElseIf dr("vcFieldType") = "I" Then
                            If strFieldValue <> "0" Then
                                sb.Append(" and I.numItemCode in (Select I.numItemCode from ")
                                sb.Append(" Item I INNER JOIN WareHouseItems WI ON WI.numItemID = I.numItemCode ")
                                sb.Append(" INNER JOIN CFW_Fld_Values_Serialized_Items SI ON I.bitSerialized = SI.bitSerialized AND WI.numWareHouseItemID = SI.RecId")
                                sb.Append(" WHERE SI.Fld_ID=" & strFieldID & " and WI.numWareHouseItemID > 0 And I.numDomainID = " & Session("DomainID") & " ")
                                sb.Append(" and SI.Fld_Value = '" & strFieldValue & "' )")
                            End If
                        End If
                    ElseIf ControlType = "listbox" Then
                        If strFieldValue.Length > 1 Then
                            If strFieldName = "numVendorID" Then
                                sb.Append(" AND I.numItemCode IN (SELECT DISTINCT numItemCode FROM vendor WHERE numVendorID in (" & strFieldValue & ") ) ")
                                Session("VendorIDs") = strFieldValue
                            End If
                        End If
                    ElseIf ControlType = "checkboxlist" Then
                        If Not String.IsNullOrEmpty(strFieldValue) Then
                            If dr("vcFieldType") = "C" Or dr("vcFieldType") = "P" Then
                                If strFieldValue <> "0" Then
                                    sb.Append(" and I.numItemCode in (select RecId from CFW_FLD_Values_Item where Fld_ID =" & strFieldID & " AND (" & strFieldValue & "))")
                                End If
                            ElseIf dr("vcFieldType") = "D" Then
                                If strFieldValue <> "0" Then
                                    sb.Append(" and DM.numDivisionID in (select RecId from CFW_FLD_Values_Item where Fld_ID =" & strFieldID & " AND (" & strFieldValue & "))")
                                End If
                            End If
                        End If
                    ElseIf ControlType = "checkbox" Then 'Added by chintan, reason: custom field checkbox code isn't handled
                        If strFieldValue <> "-1" Then
                            If dr("vcFieldType") = "R" Then
                                'If strFieldValue = "1" Then
                                '    sb.Append(" and " & strFieldName & " = 1 ")
                                'End If
                                sb.Append(" and " & FormGenericAdvSearch.GetSearchCondition(hdnFilterCriteria.Value, strFieldName, strControlID, CType(plhControls.FindControl(strControlID), DropDownList).SelectedItem.Value, dr("vcFieldDataType")))
                            ElseIf dr("vcFieldType") = "D" Then
                                'If strFieldValue = "1" Then
                                'sb.Append(" and DM.numDivisionID in (select RecId from CFW_FLD_Values_Item where Fld_ID =" & strFieldID & " and Fld_Value = '1' )")
                                'End If
                                sb.Append(" and DM.numDivisionID in (select RecId from CFW_FLD_Values_Item where Fld_ID =" & strFieldID & " and Fld_Value = '" & strFieldValue & "' )")
                            End If
                        End If
                    ElseIf ControlType = "datefield" Then
                        If dr("vcFieldType") = "R" Then
                            If CCommon.ToString(strFieldValue).Length > 0 Then
                                If strFieldName = "dtDateEntered" Then
                                    If hdnFilterCriteria.Value.ToLower.IndexOf("dtdateentered~11") >= 0 Then
                                        sb.Append(" and " & FormGenericAdvSearch.GetSearchCondition(hdnFilterCriteria.Value, strFieldName, strControlID, strFieldValue, dr("vcFieldDataType")))
                                    Else
                                        Dim dt As Date = DateFromFormattedDate(strFieldValue, Session("DateFormat"))
                                        sb.Append(" and " & strFieldName & " = '" & dt.Year.ToString + "-" + dt.Month.ToString + "-" + dt.Day.ToString + "'")
                                    End If
                                End If
                            End If
                        ElseIf dr("vcFieldType") = "D" Then
                            If CCommon.ToString(strFieldValue).Length > 0 Then
                                sb.Append(" and DM.numDivisionID in (select RecId from CFW_FLD_Values_Item where Fld_ID =" & strFieldID & " and " & FormGenericAdvSearch.GetSearchCondition(hdnFilterCriteria.Value, strFieldName, strControlID, strFieldValue, dr("vcFieldDataType"), IsCustomField:=True) & ")")
                            End If
                        End If
                    Else 'Textbox
                        If CType(plhControls.FindControl(strControlID), TextBox).Text <> "" Then
                            If dr("vcFieldType") = "R" Then
                                If strFieldName = "monListPrice" Then
                                    sb.Append(" AND numItemCode IN (SELECT distinct numItemID FROM WareHouseItems WHERE numDomainID=" & Session("DomainID") & " and " & FormGenericAdvSearch.GetSearchCondition(hdnFilterCriteria.Value, strFieldName, strControlID, strFieldValue, dr("vcFieldDataType")).Replace("monListPrice", "monWListPrice"))
                                    sb.Append(" union select distinct numitemcode from Item Where numDomainID=" & Session("DomainID") & " and " & FormGenericAdvSearch.GetSearchCondition(hdnFilterCriteria.Value, strFieldName, strControlID, strFieldValue, dr("vcFieldDataType")) & " )")
                                ElseIf dr("vcLookBackTableName") = "WareHouseItems" Then
                                    If strFieldName = "vcBarCode" Then
                                        sb.Append(" AND numItemCode IN (SELECT numItemID FROM WareHouseItems WHERE numDomainID=" & Session("DomainID") & " AND " & FormGenericAdvSearch.GetSearchCondition(hdnFilterCriteria.Value, strFieldName, strControlID, strFieldValue, dr("vcFieldDataType")) & " group by numItemID)")
                                    ElseIf strFieldName = "vcWHSKU" Then
                                        sb.Append(" AND numItemCode IN (SELECT numItemID FROM WareHouseItems WHERE numDomainID=" & Session("DomainID") & " AND " & FormGenericAdvSearch.GetSearchCondition(hdnFilterCriteria.Value, strFieldName, strControlID, strFieldValue, dr("vcFieldDataType")) & " group by numItemID)")
                                    Else
                                        sb.Append(" AND numItemCode IN (SELECT numItemID FROM WareHouseItems WHERE numDomainID=" & Session("DomainID") & " group by numItemID having " & FormGenericAdvSearch.GetSearchCondition(hdnFilterCriteria.Value, strFieldName, strControlID, strFieldValue, dr("vcFieldDataType")) & " group by numItemID) )")
                                    End If
                                ElseIf strFieldName = "vcSerialNo" Then
                                    sb.Append(" AND numWareHouseItemID IN (SELECT numWareHouseItemID FROM WareHouseItmsDTL WHERE " & FormGenericAdvSearch.GetSearchCondition(hdnFilterCriteria.Value, strFieldName, strControlID, strFieldValue, dr("vcFieldDataType")) & " AND COALESCE(numQty,0) > 0) AND WI.numDomainID=" & Session("DomainID"))
                                    'sb.Append(" AND numItemCode IN (SELECT distinct numItemID FROM WareHouseItems WHERE numDomainID=" & Session("DomainID") & " AND numWareHouseItemID IN (SELECT numWareHouseItemID FROM WareHouseItmsDTL WHERE " & FormGenericAdvSearch.GetSearchCondition(hdnFilterCriteria.Value, strFieldName, strFieldValue, dr("vcFieldDataType")) & "))")
                                Else
                                    sb.Append(" and " & FormGenericAdvSearch.GetSearchCondition(hdnFilterCriteria.Value, strFieldName, strControlID, strFieldValue, dr("vcFieldDataType")))
                                End If
                            ElseIf dr("vcFieldType") = "C" Or dr("vcFieldType") = "P" Then
                                sb.Append(" and I.numItemCode in (SELECT RecId FROM CFW_FLD_Values_Item WHERE Fld_ID =" & strFieldID & " and " & FormGenericAdvSearch.GetSearchCondition(hdnFilterCriteria.Value, strFieldName, strControlID, strFieldValue, dr("vcFieldDataType"), IsCustomField:=True) & ")")
                            ElseIf dr("vcFieldType") = "D" Then
                                sb.Append(" and DM.numDivisionID in (select RecId from CFW_FLD_Values_Item where Fld_ID =" & strFieldID & " and " & FormGenericAdvSearch.GetSearchCondition(hdnFilterCriteria.Value, strFieldName, strControlID, strFieldValue, dr("vcFieldDataType"), IsCustomField:=True) & ")")
                            End If
                        End If
                    End If
                Next
                Session("UserFriendlyQuery") = sbFriendlyQuery.ToString.Trim.TrimEnd(New Char() {"d", "n", "a"})
                Return sb.ToString
            Catch ex As Exception
                Throw ex
            End Try
        End Function

       
    </script>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="DetailPageTitle" runat="server"
    ClientIDMode="Static">
    Items Search
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="RecordActionPanel" runat="server"
    ClientIDMode="Static">
    <div class="row padbottom10">
        <div class="col-xs-12">
            <div class="pull-left">
                <asp:Literal ID="litMessage" runat="server" EnableViewState="False"></asp:Literal>
            </div>
            <div class="pull-right">
                <asp:LinkButton runat="server" ID="btnSearch" CssClass="btn btn-primary" Text="Search" OnClientClick="SetUserSearchCriteria();"><i class="fa fa-search"></i>&nbsp;&nbsp;Search</asp:LinkButton>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="TabsPlaceHolder" runat="server"
    ClientIDMode="Static">
    <asp:UpdateProgress ID="UpdateProgress" runat="server" ClientIDMode="Static">
        <ProgressTemplate>
            <div class="overlay">
                <div class="overlayContent" style="color: #000; text-align: center; width: 250px; padding: 20px">
                    <i class="fa fa-2x fa-refresh fa-spin"></i>
                    <h3>Processing Request</h3>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:ValidationSummary runat="server" ID="valsummery" CssClass="normal4" />


    <asp:PlaceHolder ID="plhControls" runat="server"></asp:PlaceHolder>


    <uc1:frmAdvSearchCriteria ID="frmAdvSearchCriteria1" runat="server" ClientIDMode="Static" />

    <asp:HiddenField ID="hdnFilterCriteria" runat="server" />
    <asp:HiddenField ID="hdnSaveSearchCriteria" runat="server" />
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="UtilityLinksPanel" runat="server">
    <uc2:frmMySavedSearch ID="frmMySavedSearch1" runat="server" FormID="29" />
</asp:Content>
