﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmProjectsAccountMapping.aspx.vb"
    Inherits=".frmProjectsAccountMapping" MasterPageFile="~/common/Popup.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Project Account Mapping</title>
    <script language="Javascript" type="text/javascript">
        function Save() {
            if (document.getElementById("ddlProject").value == 0) {
                alert("Please Select Project");
                document.getElementById("ddlProject").focus();
                return false;
            }
            if (document.getElementById("ddlAccount").value == 0) {
                alert("Please Select Account");
                document.getElementById("ddlAccount").focus();
                return false;
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <div class="input-part">
        <div class="right-input">
            <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" OnClientClick="return Save();">
            </asp:Button>
            <asp:Button ID="btnClose" runat="server" CssClass="button" Text="Close" OnClientClick="return Close();">
            </asp:Button>&nbsp;&nbsp;
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageTitle" runat="server" ClientIDMode="Static">
    Project Account Mapping
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="Content" runat="server" ClientIDMode="Static">
    <table width="600px">
        <tr>
            <td class="normal4" align="center" colspan="2">
                <asp:Literal ID="litMessage" runat="server"></asp:Literal>
            </td>
        </tr>
        <tr>
            <td class="normal1" align="right">
                Open Project
            </td>
            <td>
                <asp:DropDownList ID="ddlProject" AutoPostBack="True" runat="server" CssClass="signup"
                    Width="350">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="normal1" align="right">
                Time & Expense Account
            </td>
            <td>
                <asp:DropDownList ID="ddlAccount" runat="server" CssClass="signup" Width="350">
                </asp:DropDownList>
            </td>
        </tr>
    </table>
</asp:Content>
