﻿Imports BACRM.BusinessLogic.Item
Imports BACRM.BusinessLogic.Common

Public Class CommissionItems
    Inherits BACRMPage
    Dim objCommissionRule As CommissionRule
    Dim lngRuleId As Long
    Dim StepValue, SelectedOption As Short

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            lngRuleId = CCommon.ToLong(GetQueryStringVal("RuleId"))
            StepValue = CCommon.ToShort(GetQueryStringVal("StepValue"))
            SelectedOption = CCommon.ToShort(GetQueryStringVal("SelectedOption"))
            If Not IsPostBack Then
                hdnSelectedOption.Value = SelectedOption
                If lngRuleId > 0 Then
                    LoadDetails()
                End If
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Sub LoadDetails()
        Try
            objCommissionRule = New CommissionRule

            Select Case StepValue
                Case 3 'Item
                    Select Case SelectedOption
                        Case 1
                            lblTitle.Text = "Item"
                            pnlItems.Visible = True
                            objCommissionRule.RuleAppType = 1
                        Case 2
                            lblTitle.Text = "Item Classification"
                            pnlItemClassification.Visible = True
                            objCommissionRule.RuleAppType = 2
                    End Select
                Case 4 'Organization
                    Select Case SelectedOption
                        Case 3
                            lblTitle.Text = "Organization"
                            pnlOrganizations.Visible = True
                            objCommissionRule.RuleAppType = 3
                        Case 4
                            lblTitle.Text = "Organization Relationship & Profile"
                            pnlRelProfiles.Visible = True
                            objCommissionRule.RuleAppType = 4
                    End Select
            End Select


            objCommissionRule.RuleID = lngRuleId
            objCommissionRule.DomainID = Session("DomainID")

            Dim ds As DataSet

            ds = objCommissionRule.GetCommissionRuleItems


            If objCommissionRule.RuleAppType = 1 Then
                dgItems.DataSource = ds.Tables(0)
                dgItems.DataBind()
                hdnRowCount.Value = ds.Tables(0).Rows.Count
            ElseIf objCommissionRule.RuleAppType = 2 Then
                ddlItemClassification.DataTextField = "ItemClassification"
                ddlItemClassification.DataValueField = "numItemClassification"
                ddlItemClassification.DataSource = ds.Tables(0)
                ddlItemClassification.DataBind()
                ddlItemClassification.Items.Insert(0, New ListItem("--Select One--", 0))

                dgItemClass.DataSource = ds.Tables(1)
                dgItemClass.DataBind()
                hdnRowCount.Value = ds.Tables(1).Rows.Count
            ElseIf objCommissionRule.RuleAppType = 3 Then
                dgOrg.DataSource = ds.Tables(0)
                dgOrg.DataBind()
                hdnRowCount.Value = ds.Tables(0).Rows.Count
            ElseIf objCommissionRule.RuleAppType = 4 Then
                objCommon.sb_FillComboFromDBwithSel(ddlRelationship, 5, Session("DomainID"))
                objCommon.sb_FillComboFromDBwithSel(ddlProfile, 21, Session("DomainID"))

                dgRelationship.DataSource = ds.Tables(0)
                dgRelationship.DataBind()
                hdnRowCount.Value = ds.Tables(0).Rows.Count
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub btnAddItem_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAddItem.Click
        Try
            If radItem.SelectedValue <> "" Then
                objCommissionRule = New CommissionRule
                objCommissionRule.RuleID = lngRuleId
                objCommissionRule.RuleAppType = 1
                objCommissionRule.RuleValue = radItem.SelectedValue
                objCommissionRule.byteMode = 0
                objCommissionRule.ManageCommissionRuleItems()
                LoadDetails()
            End If

            hdnRowCount.Value = dgItems.Items.Count
        Catch ex As Exception
            If ex.Message = "DUPLICATE" Then
                litMessage.Text = "A Commission Rule with this same of Item(s) and Contact(s) already exists. Please modify your Commison rule to make sure it's unique"
            Else
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End If
        End Try
    End Sub

    Private Sub btnRemItem_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRemItem.Click
        Try
            objCommissionRule = New CommissionRule
            For Each Item As DataGridItem In dgItems.Items
                If CType(Item.FindControl("chk"), CheckBox).Checked Then
                    objCommissionRule.RuleDTLID = CCommon.ToLong(Item.Cells(0).Text)
                    objCommissionRule.byteMode = 1
                    objCommissionRule.RuleAppType = 1
                    objCommissionRule.ManageCommissionRuleItems()
                End If
            Next
            LoadDetails()

            hdnRowCount.Value = dgItems.Items.Count
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub btnExportItem_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExportItem.Click
        ExportToExcel.DataGridToExcel(dgItems, Response)
    End Sub

    Private Sub btnExportItemClassification_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExportItemClassification.Click
        ExportToExcel.DataGridToExcel(dgItemClass, Response)
    End Sub

    Private Sub btnAddItemClass_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddItemClass.Click
        Try
            If ddlItemClassification.SelectedValue > 0 Then
                objCommissionRule = New CommissionRule
                objCommissionRule.RuleID = lngRuleId
                objCommissionRule.RuleAppType = 2
                objCommissionRule.RuleValue = ddlItemClassification.SelectedValue
                objCommissionRule.byteMode = 0
                objCommissionRule.ManageCommissionRuleItems()
                LoadDetails()
            End If

            hdnRowCount.Value = dgItemClass.Items.Count
        Catch ex As Exception
            If ex.Message = "DUPLICATE" Then
                litMessage.Text = "A Price Rule with this same combination of Item(s) and Organization(s) already exists. Please modify your price rule to make sure it's unique"
            Else
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End If
        End Try
    End Sub

    Private Sub btnRemoveItemClass_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRemoveItemClass.Click
        Try
            objCommissionRule = New CommissionRule
            For Each Item As DataGridItem In dgItemClass.Items
                If CType(Item.FindControl("chk"), CheckBox).Checked Then
                    objCommissionRule.RuleDTLID = CCommon.ToLong(Item.Cells(0).Text)
                    objCommissionRule.byteMode = 1
                    objCommissionRule.RuleAppType = 2
                    objCommissionRule.ManageCommissionRuleItems()
                End If
            Next
            LoadDetails()

            hdnRowCount.Value = dgItemClass.Items.Count
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub btnAddOrg_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddOrg.Click
        Try
            If CCommon.ToLong(radCmbCompany.SelectedValue) > 0 Then
                objCommissionRule = New CommissionRule
                objCommissionRule.RuleID = lngRuleId
                objCommissionRule.RuleAppType = 3
                objCommissionRule.RuleValue = radCmbCompany.SelectedValue
                objCommissionRule.byteMode = 0
                objCommissionRule.ManageCommissionRuleItems()

                LoadDetails()
            End If

            hdnRowCount.Value = dgOrg.Items.Count
        Catch ex As Exception
            If ex.Message = "DUPLICATE" Then
                litMessage.Text = "A Commission Rule with this same combination of Item(s) and Organization(s) already exists. Please modify your commission rule to make sure it's unique"
            Else
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End If
        End Try
    End Sub

    Private Sub btnRemoveOrg_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRemoveOrg.Click
        Try
            objCommissionRule = New CommissionRule
            For Each Item As DataGridItem In dgOrg.Items
                If CType(Item.FindControl("chk"), CheckBox).Checked Then
                    objCommissionRule.RuleDTLID = CCommon.ToLong(Item.Cells(0).Text)
                    objCommissionRule.byteMode = 1
                    objCommissionRule.ManageCommissionRuleItems()
                End If
            Next
            LoadDetails()

            hdnRowCount.Value = dgOrg.Items.Count
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub btnExportOrg_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExportOrg.Click
        ExportToExcel.DataGridToExcel(dgOrg, Response)
    End Sub

    Private Sub btnAddRelProfile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddRelProfile.Click
        Try
            If CCommon.ToLong(ddlRelationship.SelectedValue) > 0 And CCommon.ToLong(ddlProfile.SelectedValue) > 0 Then
                objCommissionRule = New CommissionRule
                objCommissionRule.RuleID = lngRuleId
                objCommissionRule.RuleAppType = 4
                objCommissionRule.RuleValue = ddlRelationship.SelectedValue
                objCommissionRule.Profile = ddlProfile.SelectedValue
                objCommissionRule.byteMode = 0
                objCommissionRule.ManageCommissionRuleItems()
                LoadDetails()
            End If

            hdnRowCount.Value = dgRelationship.Items.Count
        Catch ex As Exception
            If ex.Message = "DUPLICATE" Then
                litMessage.Text = "A Commission Rule with this same combination of Item(s) and Organization(s) already exists. Please modify your commission rule to make sure it's unique"
            Else
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End If
        End Try
    End Sub

    Private Sub btnRemoveRelProfile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRemoveRelProfile.Click
        Try
            objCommissionRule = New CommissionRule
            For Each Item As DataGridItem In dgRelationship.Items
                If CType(Item.FindControl("chk"), CheckBox).Checked Then
                    objCommissionRule.RuleDTLID = CCommon.ToLong(Item.Cells(0).Text)
                    objCommissionRule.byteMode = 1
                    objCommissionRule.ManageCommissionRuleItems()
                End If
            Next
            LoadDetails()

            hdnRowCount.Value = dgRelationship.Items.Count
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub btnExportRelProfile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExportRelProfile.Click
        ExportToExcel.DataGridToExcel(dgRelationship, Response)
    End Sub
End Class