<%@ Page Language="vb" AutoEventWireup="false" ValidateRequest="false" EnableEventValidation="false"
    CodeBehind="newaction.aspx.vb" Inherits="BACRM.UserInterface.Admin.newaction"
    MasterPageFile="~/common/DetailPage.Master" %>

<%@ Register TagPrefix="BizCalendar" TagName="Calendar" Src="../include/calandar.ascx" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Src="../common/frmCorrespondence.ascx" TagName="frmCorrespondence" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="ActionAttendees.ascx" TagName="ActionAttendees" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Action Items </title>
    <link rel="stylesheet" type="text/css" href="../CSS/jscal2.css" />
    <link rel="stylesheet" type="text/css" href="../CSS/border-radius.css" />
    <style type="text/css">
        .column {
            float: left;
        }

        html > body .RadComboBoxDropDown_Vista .rcbItem, html > body .RadComboBoxDropDown_Vista .rcbHovered, html > body .RadComboBoxDropDown_Vista .rcbDisabled, html > body .RadComboBoxDropDown_Vista .rcbLoading {
            display: inline-block;
        }
    </style>
    <script type="text/javascript" src="../JavaScript/jscal2.js"></script>
    <script type="text/javascript" src="../JavaScript/en.js"></script>
    <script src="../JavaScript/dateFormat.js" type="text/javascript"></script>
    <script src="../JavaScript/prototype-1.6.0.2.compressed.js" type="text/javascript"></script>
    <script src="../JavaScript/comboClientSide.js" type="text/javascript"></script>
    <script language="javascript">
        function openTime(a, b, c, d) {

            window.open("../cases/frmCasetime.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&caseId=" + a + "&CntId=" + b + "&DivId=" + c + "&CaseTimeId=" + document.getElementById("txtCaseTimeID").value, '', 'toolbar=no,titlebar=no,left=500, top=300,width=650,height=300,scrollbars=yes,resizable=yes');
            return false;
        }
        function Close() {
            window.close()
            return false;
        }
        function openExpense(a, b, c, d) {

            window.open("../cases/frmCaseExpense.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&caseId=" + a + "&CntId=" + b + "&DivId=" + c + "&CaseExpId=" + document.getElementById("txtCaseExpenseID").value, '', 'toolbar=no,titlebar=no,left=500, top=300,width=650,height=300,scrollbars=yes,resizable=yes');
            return false;
        }

        function Save() {
            if ($find('radCmbCompany').get_value() == "") {
                alert("Select Company")
                return false;
            }
            if (document.getElementById("ddlTaskContact").selectedIndex == -1) {
                alert("Select Contact")
                document.getElementById("ddlTaskContact").focus();
                return false;
            }
            if (document.getElementById("ddlTaskContact").value == 0) {
                alert("Select Contact")
                document.getElementById("ddlTaskContact").focus();
                return false;
            }
            if (document.getElementById("ddlTaskType").value == 0) {
                alert("Select Action Item Type")
                document.getElementById("ddlTaskType").focus();
                return false;
            }

            if (IsNumeric(document.getElementById("txtHours").value) == false) {
                alert("Enter Valid Hour value")
                document.getElementById("txtHours").focus();
                return false;
            }
        }

        function IsNumeric(strString)
            //  check for valid numeric strings	
        {
            var strValidChars = "0123456789.-";
            var strChar;
            var blnResult = true;
            var strString;
            if (strString.length == 0) return true;

            //  test strString consists of valid characters listed above
            for (i = 0; i < strString.length && blnResult == true; i++) {
                strChar = strString.charAt(i);
                if (strValidChars.indexOf(strChar) == -1) {
                    return false;
                }
            }
            return blnResult;
        }
        function openEmpAvailability() {
            var strTime, strStart, strEnd;
            if (document.getElementById("chkAM").checked == true) {
                strStart = 'AM'
            }
            else {
                strStart = 'PM'
            }
            if (document.getElementById("chkEndAM").checked == true) {
                strEnd = 'AM'
            }
            else {
                strEnd = 'PM'
            }
            strTime = document.getElementById("ddltime").options[document.getElementById("ddltime").selectedIndex].text + ' ' + strStart + '-' + document.getElementById("ddlEndTime").options[document.getElementById("ddlEndTime").selectedIndex].text + ' ' + strEnd;
            window.open("../ActionItems/frmEmpAvailability.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&Date=" + document.getElementById("ctl00_TabsPlaceHolder_cal_txtDate").value + '&Time=' + strTime, '', 'toolbar=no,titlebar=no,top=0,left=100,width=850,height=500,scrollbars=yes,resizable=yes');
            return false;
        }
        function AssignTo(a, b) {
            document.getElementById("ddlAssignTo").value = a;
            document.getElementById("ctl00_TabsPlaceHolder_cal_txtDate").value = b;
            return false;
        }
        function AssignCaseTimeId(a) {
            document.getElementById("txtCaseTimeID").value = a;
            //			alert("break")
            //			alert(document.getElementById("txtCaseTimeID.value)
            //document.getElementById("ctl00_TabsPlaceHolder_cal_txtDate.value=b;
            return false;
        }
        function AssignCaseExpenseId(a) {
            document.getElementById("txtCaseExpenseID").value = a;
            return false;
        }

        function OpenActionItemWindow() {
            var objEditActionItem = document.getElementById('editActionItem')
            var urlQString = objEditActionItem.href
            var start = urlQString.lastIndexOf("?") + 1
            urlQString = urlQString.substring(start)
            window.open('../ActionItems/frmActionItemTemplate.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&' + urlQString, '', 'toolbar=no,titlebar=no,top=100,left=250,width=700,height=350,scrollbars=yes,resizable=yes')
        }
        function openTaskType() {
            window.open('../ActionItems/frmDefaultTaskType.aspx', '', 'toolbar=no,titlebar=no,top=100,left=200,width=500,height=150,scrollbars=yes,resizable=yes')
        }
        function openPopupRemainder(a) {
            window.open('../ActionItems/frmDefaultPopupRemainder.aspx?Type=' + a, '', 'toolbar=no,titlebar=no,top=100,left=200,width=500,height=150,scrollbars=yes,resizable=yes')
        }
        function Loaddata() {
            document.getElementById("btnLoadData").click()
            return false;
        }

        function openActionItem(a, b, c, d, e, f) {
            if (e == 'Email') {
                window.open("../outlook/frmMailDtl.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&numEmailHstrID=" + a, '', 'titlebar=no,top=100,left=250,width=850,height=550,scrollbars=yes,resizable=yes');
                return false;
            }
            else {
                window.location.href = "../admin/ActionItemDetailsOld.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=prospects&CommId=" + a + "&CaseId=" + b + "&CaseTimeId=" + c + "&CaseExpId=" + d;
                return false;
            }

        }
        function duedateChage(format) {
            if (!isDate(document.getElementById('ctl00_TabsPlaceHolder_cal_txtDate').value, format)) {
                alert("Enter Valid Due date");
                document.getElementById('ctl00_TabsPlaceHolder_cal_txtDate').focus();
                return false;
            }

            var SDate = new Date(getDateFromFormat(document.getElementById('ctl00_TabsPlaceHolder_cal_txtDate').value, format));
            //var startDate = formatDate(d, 'dd-MM-yyyy');

            var ddDueDate = document.getElementById('ddDueDate').value;
            var dateFormat = document.getElementById('hfDateFormat').value;

            if (ddDueDate == 'W') {
                startDate = new Date(SDate.getFullYear(), SDate.getMonth(), SDate.getDate() + 7);
            }
            else if (ddDueDate == 'M') {
                startDate = new Date(SDate.getFullYear(), SDate.getMonth() + 1, SDate.getDate());
            }
            else if (ddDueDate == 'Q') {
                startDate = new Date(SDate.getFullYear(), SDate.getMonth() + 3, SDate.getDate());
            }
            else if (ddDueDate == 'Y') {
                startDate = new Date(SDate.getFullYear() + 1, SDate.getMonth(), SDate.getDate());
            }
            else {
                startDate = new Date(SDate.getFullYear(), SDate.getMonth(), SDate.getDate());
            }

            document.getElementById('ctl00_TabsPlaceHolder_cal_txtDate').value = formatDate(startDate, format);

        }

        function openRecord() {
            var lngOpenRecord = document.getElementById("ddlOpenRecord").value;
            var lngAssignTO = document.getElementById("ddlAssignTO1").value;

            if (lngOpenRecord > 0 && lngAssignTO > 0) {
                if (lngAssignTO == 1 || lngAssignTO == 2
                    || lngAssignTO == 3 || lngAssignTO == 4) { //1:Sales Opportunity 2:Purchase Opportunity 3:Sales Order 4:Purchase Order
                    document.location = '../opportunity/frmOpportunities.aspx?OpID=' + lngOpenRecord;
                }
                else if (lngAssignTO == 5) { //Project
                    document.location = '../projects/frmProjects.aspx?ProId=' + lngOpenRecord;
                }
                else if (lngAssignTO == 6) { //Cases
                    document.location = '../cases/frmCases.aspx?CaseID=' + lngOpenRecord;
                }
                else if (lngAssignTO == 7) { //Item
                    document.location = '../Items/frmKitDetails.aspx?ItemCode=' + lngOpenRecord;
                }
            }

            return false;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="RecordInformationPanel" runat="server"
    ClientIDMode="Static">
    <table id="table4" cellspacing="0" cellpadding="0" border="0" align="right">
        <tr>
            <td align="center">
                <asp:Label ID="lblError" runat="server" Font-Names="Verdana" Font-Size="9px" Height="2px"
                    ForeColor="Red" Visible="False"></asp:Label>
            </td>
            <td align="right" class="normal1">
                <span style="float: left">Action Item Templates&nbsp;</span><asp:DropDownList ID="listActionItemTemplate"
                    CssClass="signup" runat="server" Width="180" AutoPostBack="true">
                    <asp:ListItem Value="0">--Select One--</asp:ListItem>
                </asp:DropDownList>
                <span style="float: left">&nbsp;
                    <asp:HyperLink ID='editActionItem' runat="server" Font-Size="8" NavigateUrl="../Admin/newaction.aspx?Mode=Edit"
                        Text="Edit" ForeColor="#180073"></asp:HyperLink>
                    &nbsp;&nbsp;</span>
                <asp:Button ID="btnSave" Text="Save" CssClass="button" runat="server"></asp:Button>
                <asp:Button ID="btnSaveClose" Text="Save &amp; Close" CssClass="button" runat="server"></asp:Button>
                <asp:Button ID="btnCancel" Text="Cancel" CssClass="button" runat="server"></asp:Button>
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="RecordActionPanel" runat="server"
    ClientIDMode="Static">
    <table bordercolor="black" cellspacing="0" cellpadding="0" width="100%" border="0">
        <tr>
            <td class="tr1" align="right">
                <asp:CheckBox ID="chkOutlook" runat="server" CssClass="text"></asp:CheckBox>&nbsp;
                <asp:HyperLink ID="hplOutlook" runat="server" NavigateUrl="#">
										<font color="#180073">Add to Calendar</font></asp:HyperLink>
                &nbsp;&nbsp;&nbsp;
                <asp:CheckBox ID="chkPopupRemainder" runat="server" CssClass="text"></asp:CheckBox>&nbsp;
                <asp:HyperLink ID="hplPopupRemainder" runat="server" NavigateUrl="#">
										<font color="#180073">Have Popup Remind Assignee Prior to Event</font></asp:HyperLink>
                &nbsp;&nbsp;&nbsp;
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="DetailPageTitle" runat="server"
    ClientIDMode="Static">
    New Action Item
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="TabsPlaceHolder" runat="server"
    ClientIDMode="Static">
    <div style="background-color: #fff">
        <asp:Table ID="table5" runat="server" GridLines="None" BorderColor="black" Width="100%"
            BorderWidth="1" CssClass="aspTable">
            <asp:TableRow>
                <asp:TableCell VerticalAlign="Top">
                    <br>
                    <table id="table1" width="100%">
                        <tr>
                            <td rowspan="30" valign="top">
                                <img src="../images/ActionItem-32.gif" />
                            </td>
                            <td class="normal1" align="right">Customer<font color="red">*</font>
                            </td>
                            <td>
                                <telerik:RadComboBox AccessKey="C" ID="radCmbCompany" Width="195px" DropDownWidth="600px"
                                    OnClientItemsRequested="OnClientItemsRequestedOrganization"
                                    ShowMoreResultsBox="true"
                                    Skin="Default" runat="server" AutoPostBack="True" AllowCustomText="True" EnableLoadOnDemand="True" ClientIDMode="Static">
                                    <WebServiceSettings Path="../common/Common.asmx" Method="GetCompanies" />
                                </telerik:RadComboBox>
                            </td>
                            <td class="normal1" align="right">Follow-up Status
                            </td>
                            <td class="normal1">
                                <asp:DropDownList ID="ddlFollowUpStatus" runat="server" CssClass="signup">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td class="normal1" align="right">Contact<font color="red">*</font>
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlTaskContact" CssClass="signup" runat="server" Width="180"
                                    AutoPostBack="true">
                                    <asp:ListItem Value="0">--Select One--</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td class="normal1" align="right">Priority
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlStatus" CssClass="signup" runat="server" Width="180">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td class="text" align="right" width="125" height="2">Due date
                            </td>
                            <td class="text">
                                <table>
                                    <tr>
                                        <td>
                                            <BizCalendar:Calendar ID="cal" runat="server" ClientIDMode="AutoID" />
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="ddDueDate" runat="server" CssClass="signup">
                                                <asp:ListItem Value="0">Select</asp:ListItem>
                                                <asp:ListItem Value="W">Next Week</asp:ListItem>
                                                <asp:ListItem Value="M">Next Month</asp:ListItem>
                                                <asp:ListItem Value="Q">Next Quarter</asp:ListItem>
                                                <asp:ListItem Value="Y">Next Year</asp:ListItem>
                                            </asp:DropDownList>
                                            <asp:HiddenField ID="hfDateFormat" runat="server" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td class="normal1" align="right">
                                <asp:HyperLink ID="hplTaskType" runat="server" NavigateUrl="#">
										<font color="#180073">Type</font></asp:HyperLink>
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlTaskType" CssClass="signup" runat="server" Width="180" AutoPostBack="true">
                                </asp:DropDownList>
                                &nbsp;
                            <asp:CheckBox ID="chkFollowUp" runat="server" CssClass="signup" Text="Follow-up Any Time"
                                AutoPostBack="true" />
                            </td>
                        </tr>
                        <tr>
                            <td class="normal1" align="right">
                            Activity
                            <td>
                                <asp:DropDownList ID="ddlActivity" CssClass="signup" runat="server" Width="180">
                                </asp:DropDownList>
                            </td>
                            <td class="text" align="right">Assign To
                            </td>
                            <td class="normal1">
                                <asp:DropDownList ID="ddlAssignTo" CssClass="signup" runat="server" Width="180">
                                </asp:DropDownList>
                                &nbsp;&nbsp;&nbsp;
                                <asp:HyperLink ID="hplEmpAvaliability" runat="server" NavigateUrl="#">
										<font color="#180073">Availability</font></asp:HyperLink>
                            </td>
                        </tr>
                        <tr>
                            <td class="text" align="right">Pin To
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlAssignTO1" CssClass="signup" runat="server" AutoPostBack="true">
                                    <asp:ListItem Text="--Select One--" Value="0"></asp:ListItem>
                                    <asp:ListItem Text="Sales Opportunity" Value="1"></asp:ListItem>
                                    <asp:ListItem Text="Purchase Opportunity" Value="2"></asp:ListItem>
                                    <asp:ListItem Text="Sales Order" Value="3"></asp:ListItem>
                                    <asp:ListItem Text="Purchase Order" Value="4"></asp:ListItem>
                                    <asp:ListItem Text="Project" Value="5"></asp:ListItem>
                                    <asp:ListItem Text="Cases" Value="6"></asp:ListItem>
                                    <asp:ListItem Text="Item" Value="7"></asp:ListItem>
                                </asp:DropDownList>
                                <asp:HiddenField ID="hdnSearchOrderCustomerHistory" runat="server" Value="0" />
                                <asp:HiddenField ID="hdnCmbDivisionID" runat="server" />
                                <asp:HiddenField ID="hdnCmbOppType" runat="server" Value="1" />
                            </td>
                            <td class="text" align="right">
                                <asp:HyperLink ID="hplOpenRecord" CssClass="text" runat="server" Text="Open record">
                                </asp:HyperLink>
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlOpenRecord" CssClass="signup" runat="server">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr id="trItemPinTo" runat="server" visible="false">
                            <td class="text" align="right">Item
                            </td>
                            <td>
                                <telerik:RadComboBox runat="server" ID="radCmbItem" AccessKey="I" Skin="Default"
                                    Height="200px" Width="300px" DropDownWidth="515px" ShowMoreResultsBox="true"
                                    EnableVirtualScrolling="false" ChangeTextOnKeyBoardNavigation="false" OnClientItemsRequesting="OnClientItemsRequesting"
                                    OnClientItemsRequested="OnClientItemsRequested" OnClientDropDownOpening="OnClientDropDownOpening"
                                    EnableLoadOnDemand="true" ClientIDMode="Static">
                                    <WebServiceSettings Path="../common/Common.asmx" Method="GetItemNames" />
                                </telerik:RadComboBox>
                            </td>
                            <td class="text" align="right">Amount
                            </td>
                            <td>
                                <asp:TextBox ID="txtAmount" runat="server" CssClass="signup" Width="70"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="normal1" align="right">Reminder<img src="../images/Bell-16.gif" />
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlRemainder" CssClass="signup" runat="server" Width="180">
                                    <asp:ListItem Value="0">--Select One--</asp:ListItem>
                                    <asp:ListItem Value="5">5 minutes</asp:ListItem>
                                    <asp:ListItem Value="15">15 minutes</asp:ListItem>
                                    <asp:ListItem Value="30">30 Minutes</asp:ListItem>
                                    <asp:ListItem Value="60">1 Hour</asp:ListItem>
                                    <asp:ListItem Value="240">4 Hour</asp:ListItem>
                                    <asp:ListItem Value="480">8 Hour</asp:ListItem>
                                    <asp:ListItem Value="1440">24 Hour</asp:ListItem>
                                </asp:DropDownList>
                                <asp:Label ID="lblre" CssClass="text" runat="server">&nbsp;(Set Before Event)
                                </asp:Label>
                            </td>
                            <td class="normal1" align="right">Snooze
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlSnooze" CssClass="signup" runat="server" Width="180">
                                    <asp:ListItem Value="0">--Select One--</asp:ListItem>
                                    <asp:ListItem Value="5">5 minutes</asp:ListItem>
                                    <asp:ListItem Value="15">15 minutes</asp:ListItem>
                                    <asp:ListItem Value="30">30 Minutes</asp:ListItem>
                                    <asp:ListItem Value="60">1 Hour</asp:ListItem>
                                    <asp:ListItem Value="240">4 Hour</asp:ListItem>
                                    <asp:ListItem Value="480">8 Hour</asp:ListItem>
                                    <asp:ListItem Value="1440">24 Hour</asp:ListItem>
                                </asp:DropDownList>
                                <asp:Label ID="Label1" CssClass="text" runat="server">&nbsp;(Set After Event)
                                </asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="text" align="right">Start Time
                            </td>
                            <td class="text">
                                <img src="../images/Clock-16.gif" />
                                <asp:DropDownList ID="ddltime" CssClass="signup" runat="server" Width="55">
                                    <asp:ListItem Selected="False" Value="23">12:00</asp:ListItem>
                                    <asp:ListItem Selected="False" Value="24">12:30</asp:ListItem>
                                    <asp:ListItem Selected="False" Value="1">1:00</asp:ListItem>
                                    <asp:ListItem Selected="False" Value="2">1:30</asp:ListItem>
                                    <asp:ListItem Selected="False" Value="3">2:00</asp:ListItem>
                                    <asp:ListItem Selected="False" Value="4">2:30</asp:ListItem>
                                    <asp:ListItem Selected="False" Value="5">3:00</asp:ListItem>
                                    <asp:ListItem Selected="False" Value="6">3:30</asp:ListItem>
                                    <asp:ListItem Selected="False" Value="7">4:00</asp:ListItem>
                                    <asp:ListItem Selected="False" Value="8">4:30</asp:ListItem>
                                    <asp:ListItem Selected="False" Value="9">5:00</asp:ListItem>
                                    <asp:ListItem Selected="False" Value="10">5:30</asp:ListItem>
                                    <asp:ListItem Selected="False" Value="11">6:00</asp:ListItem>
                                    <asp:ListItem Selected="False" Value="12">6:30</asp:ListItem>
                                    <asp:ListItem Selected="False" Value="13">7:00</asp:ListItem>
                                    <asp:ListItem Selected="False" Value="14">7:30</asp:ListItem>
                                    <asp:ListItem Selected="true" Value="15">8:00</asp:ListItem>
                                    <asp:ListItem Selected="False" Value="16">8:30</asp:ListItem>
                                    <asp:ListItem Selected="False" Value="17">9:00</asp:ListItem>
                                    <asp:ListItem Selected="False" Value="18">9:30</asp:ListItem>
                                    <asp:ListItem Selected="False" Value="19">10:00</asp:ListItem>
                                    <asp:ListItem Selected="False" Value="20">10:30</asp:ListItem>
                                    <asp:ListItem Selected="False" Value="21">11:00</asp:ListItem>
                                    <asp:ListItem Selected="False" Value="22">11:30</asp:ListItem>
                                </asp:DropDownList>
                                &nbsp;AM
                            <input id="chkAM" type="radio" checked value="0" name="AM" runat="server">
                                PM
                            <input id="chkPM" type="radio" value="1" name="AM" runat="server">
                            </td>
                            <td class="text" align="right">Email Alert
                            </td>
                            <td>
                                <asp:CheckBox ID="chkAlert" runat="server" /><asp:DropDownList ID="ddlEmailTemplate"
                                    CssClass="signup" runat="server" Width="180">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td class="text" align="right">End Time
                            </td>
                            <td class="text">
                                <img src="../images/Clock-16.gif" />
                                <asp:DropDownList ID="ddlEndTime" CssClass="signup" runat="server" Width="55">
                                    <asp:ListItem Selected="False" Value="23">12:00</asp:ListItem>
                                    <asp:ListItem Selected="False" Value="24">12:30</asp:ListItem>
                                    <asp:ListItem Selected="False" Value="1">1:00</asp:ListItem>
                                    <asp:ListItem Selected="False" Value="2">1:30</asp:ListItem>
                                    <asp:ListItem Selected="False" Value="3">2:00</asp:ListItem>
                                    <asp:ListItem Selected="False" Value="4">2:30</asp:ListItem>
                                    <asp:ListItem Selected="False" Value="5">3:00</asp:ListItem>
                                    <asp:ListItem Selected="False" Value="6">3:30</asp:ListItem>
                                    <asp:ListItem Selected="False" Value="7">4:00</asp:ListItem>
                                    <asp:ListItem Selected="False" Value="8">4:30</asp:ListItem>
                                    <asp:ListItem Selected="False" Value="9">5:00</asp:ListItem>
                                    <asp:ListItem Selected="False" Value="10">5:30</asp:ListItem>
                                    <asp:ListItem Selected="False" Value="11">6:00</asp:ListItem>
                                    <asp:ListItem Selected="False" Value="12">6:30</asp:ListItem>
                                    <asp:ListItem Selected="False" Value="13">7:00</asp:ListItem>
                                    <asp:ListItem Selected="False" Value="14">7:30</asp:ListItem>
                                    <asp:ListItem Selected="true" Value="15">8:00</asp:ListItem>
                                    <asp:ListItem Selected="False" Value="16">8:30</asp:ListItem>
                                    <asp:ListItem Selected="False" Value="17">9:00</asp:ListItem>
                                    <asp:ListItem Selected="False" Value="18">9:30</asp:ListItem>
                                    <asp:ListItem Selected="False" Value="19">10:00</asp:ListItem>
                                    <asp:ListItem Selected="False" Value="20">10:30</asp:ListItem>
                                    <asp:ListItem Selected="False" Value="21">11:00</asp:ListItem>
                                    <asp:ListItem Selected="False" Value="22">11:30</asp:ListItem>
                                </asp:DropDownList>
                                &nbsp;AM
                            <input id="chkEndAM" type="radio" checked value="0" name="EndAM" runat="server">
                                PM
                            <input id="chkEndPM" type="radio" value="1" name="EndAM" runat="server">
                            </td>
                            <td align="right"></td>
                            <td nowrap>
                                <table>
                                    <tr>
                                        <td></td>
                                        <td class="normal1">
                                            <asp:TextBox ID="txtHours" runat="server" CssClass="signup" Width="30"></asp:TextBox>&nbsp;hours&nbsp;
                                        </td>
                                        <td class="normal1">
                                            <asp:RadioButtonList ID="rdlEmailTemplate" runat="server" RepeatDirection="Horizontal">
                                                <asp:ListItem Value="1" Selected="true" Text="after due date"></asp:ListItem>
                                                <asp:ListItem Value="0" Text="before due date"></asp:ListItem>
                                            </asp:RadioButtonList>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="5">
                                <div class="box box-default box-solid" id="divAttendeesMain">
                                    <div class="box-header with-border">
                                        <h3 class="box-title">Attendees</h3>

                                        <div class="box-tools pull-right">
                                            <button type="button" class="btn btn-box-tool" data-widget="collapse" id="btnAttendeesExpandCollapse">
                                                <i class="fa fa-minus"></i>
                                            </button>
                                        </div>
                                        <!-- /.box-tools -->
                                    </div>
                                    <!-- /.box-header -->
                                    <div class="box-body" id="divAttendeesBody" style="display: block;">
                                        <uc2:ActionAttendees ID="ActionAttendees1" runat="server" AttendeesFor="1" />
                                    </div>
                                    <!-- /.box-body -->
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="5">
                                <table>
                                    <tr>
                                        <td class="normal1" width="40%">Comments
                                        </td>
                                        <td class="normal1" width="60%">Recent Correspondence
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="normal1" valign="top" width="40%">
                                            <asp:TextBox ID="txtcomments" TabIndex="6" runat="server" Width="560" Height="180"
                                                CssClass="signup" TextMode="MultiLine" Wrap="true" MaxLength="10"></asp:TextBox>
                                        </td>
                                        <td valign="top" width="60%">
                                            <uc1:frmCorrespondence ID="Correspondence1" runat="server" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td class="text" id="tdEndTime" align="right" colspan="3" runat="server"></td>
                        </tr>
                    </table>
                    <br>
                </asp:TableCell>
            </asp:TableRow>
        </asp:Table>
        <asp:Button ID="btnLoadData" runat="server" Style="display: none" />
        <asp:TextBox runat="server" ID="txtCaseTimeID" Style="display: none" Text="0"></asp:TextBox>
        <asp:TextBox runat="server" ID="txtCaseExpenseID" Style="display: none" Text="0"></asp:TextBox>
    </div>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="UtilityLinksPanel" runat="server">
</asp:Content>
