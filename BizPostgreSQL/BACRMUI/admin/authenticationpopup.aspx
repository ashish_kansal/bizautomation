﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/common/PopupBootstrap.Master" CodeBehind="authenticationpopup.aspx.vb" Inherits=".authenticationpopup" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <title>BACRM Authentication</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitle" runat="server">
    Authorization failed!
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Content" runat="server">
    <table align="center" width="100%" style="height: 200px">
        <tr>
            <td class="normal13" align="center" style="color: Red" valign="middle">
                <b>You don’t have permission to use this resource. Contact your administrator</b>
                <br />
                <div runat="server" id="dvPermission" visible="false" class="info">
                    Note: Your administrator can give you permission, by going to Administration | Manage Authorization | <asp:Label Text="" runat="server" ID="lblModuleName" /> (from module drop down) | <asp:Label Text="" runat="server" ID="lblPermissionName" /> (row).
                </div>
            </td>
        </tr>
    </table>
</asp:Content>
