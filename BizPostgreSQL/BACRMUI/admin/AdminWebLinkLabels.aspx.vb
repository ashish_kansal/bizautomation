Imports BACRM.BusinessLogic.Leads
Imports BACRM.BusinessLogic.Common
Namespace BACRM.UserInterface.Admin
    Public Class AdminWebLinkLabels
        Inherits BACRMPage
        Protected WithEvents txtLink1Label As System.Web.UI.WebControls.TextBox
        Protected WithEvents txtLink2Label As System.Web.UI.WebControls.TextBox
        Protected WithEvents txtLink3Label As System.Web.UI.WebControls.TextBox
        Protected WithEvents btnSaveClose As System.Web.UI.WebControls.Button
        Protected WithEvents btnClose As System.Web.UI.WebControls.Button
        Protected WithEvents tblProducts As System.Web.UI.WebControls.Table
        Protected WithEvents txtLink4Label As System.Web.UI.WebControls.TextBox
        Dim lngDivID As Long
#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Try
                lngDivID = CCommon.ToLong(GetQueryStringVal("rtyWR"))
                If IsPostBack = False Then sb_GetLabels()
                
                btnClose.Attributes.Add("onclick", "return Close()")
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub sb_GetLabels()
            Try
                Dim objLeads As New CLeads
                Dim dtWebLinks As DataTable
                objLeads.DivisionID = lngDivID
                dtWebLinks = objLeads.WebLinks
                If dtWebLinks.Rows.Count > 0 Then
                    If IsDBNull(dtWebLinks.Rows(0).Item("vcWebLink1Label")) = False Then txtLink1Label.Text = dtWebLinks.Rows(0).Item("vcWebLink1Label")
                    If IsDBNull(dtWebLinks.Rows(0).Item("vcWebLink2Label")) = False Then txtLink2Label.Text = dtWebLinks.Rows(0).Item("vcWebLink2Label")
                    If IsDBNull(dtWebLinks.Rows(0).Item("vcWebLink3Label")) = False Then txtLink3Label.Text = dtWebLinks.Rows(0).Item("vcWebLink3Label")
                    If IsDBNull(dtWebLinks.Rows(0).Item("vcWebLink4Label")) = False Then txtLink4Label.Text = dtWebLinks.Rows(0).Item("vcWebLink4Label")
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub btnSaveClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveClose.Click
            Try
                Dim objLeads As New CLeads
                objLeads.DivisionID = lngDivID
                objLeads.WebLabel1 = txtLink1Label.Text
                objLeads.WebLabel2 = txtLink2Label.Text
                objLeads.WebLabel3 = txtLink3Label.Text
                objLeads.WebLabel4 = txtLink4Label.Text
                objLeads.UpdateWebLinks()
                Response.Write("<script>opener.location.reload(true); self.close();</script>")
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

    End Class
End Namespace