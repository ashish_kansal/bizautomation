﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmBizDocTemplateList.aspx.vb"
    Inherits=".frmBizDocTemplateList" MasterPageFile="~/common/GridMasterRegular.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>BizDoc Template List</title>
    <script language="javascript" type="text/javascript">
        function OpenBizDocTemplate(a, b) {
            window.location.href = "../admin/frmBizDocTemplate.aspx?TemplateType=0&OppType=" + b + "&BizDocTempId=" + a;
            return false;
        }
        function DeleteRecord() {
            if (confirm('Are you sure, you want to delete the selected record?')) {
                return true;
            }
            else {
                return false;
            }
        }
        function DeleteMessage() {
            //alert("you can not delete default bizdoc template,your option is to create another template which is default for given bizdoc type and try removing current bizdoc template again!");
            alert("You can't delete a default template. If you want to delete this template, switch the default setting to another template, then delete this one!");
            return false;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server"
    ClientIDMode="Static">
    <div class="row padbottom10">
        <div class="col-xs-12">
            <div class="pull-left">
                <div class="form-inline">
                    <div class="form-group">
                        <label>Opportunities / Orders Type:</label>
                        <asp:DropDownList runat="server" ID="ddlOppType" CssClass="form-control" AutoPostBack="true">
                            <asp:ListItem Text="All" Value="0"></asp:ListItem>
                            <asp:ListItem Text="Sales" Value="1"></asp:ListItem>
                            <asp:ListItem Text="Purchase" Value="2"></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div class="form-group">
                        <label>BizDoc Type</label>
                        <asp:DropDownList runat="server" ID="ddlBizDoc" CssClass="form-control" AutoPostBack="true">
                        </asp:DropDownList>
                    </div>
                </div>
            </div>
            <div class="pull-right">
                <asp:Button ID="btnAddNew" runat="server" CssClass="btn btn-primary" Text="Add New"></asp:Button>
            </div>
        </div>
    </div>
    <asp:UpdateProgress ID="UpdateProgress" runat="server" ClientIDMode="Static">
        <ProgressTemplate>
            <div class="overlay">
                <div class="overlayContent" style="color: #000; text-align: center; width: 250px; padding: 20px">
                    <i class="fa fa-2x fa-refresh fa-spin"></i>
                    <h3>Processing Request</h3>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server" ClientIDMode="Static">
    BizDoc Template List
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BizPager" runat="server" ClientIDMode="Static">
    &nbsp;
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="GridPlaceHolder" runat="server"
    ClientIDMode="Static">
    <div class="row">
        <div class="col-xs-12">
            <div class="table-responsive">
                <asp:GridView ID="gvBizDoc" runat="server" UseAccessibleHeader="true" AutoGenerateColumns="False" CssClass="table table-striped table-bordered" Width="100%">
                    <Columns>
                         <asp:BoundField HeaderText="BizDoc ID" DataField="numBizDocTempID" ItemStyle-HorizontalAlign="Center" />
                        <asp:BoundField HeaderText="Form" DataField="vcOppType" ItemStyle-HorizontalAlign="Center" />
                        <asp:BoundField HeaderText="BizDoc" DataField="vcBizDocType" ItemStyle-HorizontalAlign="Center" />
                        <asp:TemplateField HeaderText="Template" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <a href="javascript:void(0);" onclick="OpenBizDocTemplate('<%# Eval("numBizDocTempID") %>','<%# Eval("numOppType") %>');">
                                    <%# Eval("vcTemplateName")%>
                                </a>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField HeaderText="Relationship" DataField="Relationship" ItemStyle-HorizontalAlign="Center" />
                        <asp:BoundField HeaderText="Profile" DataField="Profile" ItemStyle-HorizontalAlign="Center" />
                        <asp:CheckBoxField HeaderText="Enabled" DataField="bitEnabled" ReadOnly="true" />
                        <asp:CheckBoxField HeaderText="Default" DataField="bitDefault" ReadOnly="true" />
                        <asp:TemplateField HeaderStyle-Width="10" FooterStyle-Width="10" ItemStyle-Width="10">
                            <ItemTemplate>
                                <asp:LinkButton ID="btnnotDelete" runat="server" CssClass="btn btn-danger btn-xs" Font-Bold="true" Visible="false"><i class="fa fa-trash"></i></asp:LinkButton>
                                <asp:LinkButton ID="btnDelete" runat="server" CssClass="btn btn-danger btn-xs" CommandName="Delete" CommandArgument='<%# Eval("numBizDocTempID") %>'><i class="fa fa-trash"></i></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </div>
        </div>
    </div>

</asp:Content>
