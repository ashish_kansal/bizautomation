﻿Imports System.IO
Imports BACRMBUSSLOGIC.BussinessLogic
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Item
Imports System.Reflection
Imports LumenWorks.Framework.IO.Csv
Imports LumenWorks.Framework.Tests.Unit
Imports BACRM.BusinessLogic.Reports
Imports System.Data.OleDb
Imports System.ComponentModel
Imports BACRM.BusinessLogic.Leads
Imports BACRM.BusinessLogic.Contacts
Imports BACRM.BusinessLogic.Account


Public Class frmImport
    Inherits BACRMPage

#Region "GLOBAL VARIABLES"

    Dim dtCSV As New DataTable
    Dim dtMap As New DataTable
    Dim dtPart1 As New DataTable
    Dim dtPart2 As New DataTable
    Dim listMappedFields As System.Collections.Generic.List(Of Integer)

    Dim strFileName As String = ""
    Dim sMode As Short
    Dim intItemType As Integer
    Dim intImportFileID As Long

    'CONSTANT FOR STORING FILE NAME IN VIEW STATE
    Private Const _VW_FILE_NAME As String = "_vw_file_name"
    Private Const _VW_MAPPED_DATATABLE As String = "_vw_mapped_datatable"

    Dim lngFileSize As Long = CCommon.ToLong(ConfigurationManager.AppSettings("MaxUploadFileSize"))
    Dim lngFileRecords As Long = CCommon.ToLong(ConfigurationManager.AppSettings("MaxFileRecords"))

#End Region

#Region "OBJECT DECLARATION"

    Dim objImport As ImportWizard = Nothing
#End Region

#Region "MANAGE PANEL VISIBILITY"

    Private Sub ManagePanels(ByVal intStep As Integer)
        Try
            If intStep = 1 Then
                pnlStep1.Visible = True
                pnlStep2.Visible = False
                pnlStep3.Visible = False
                pnlStep4.Visible = False
                pnlStep5.Visible = False
                pnlStep6.Visible = False
            ElseIf intStep = 2 Then
                pnlStep1.Visible = False
                pnlStep2.Visible = True
                pnlStep3.Visible = False
                pnlStep4.Visible = False
                pnlStep5.Visible = False
                pnlStep6.Visible = False
            ElseIf intStep = 3 Then
                pnlStep1.Visible = False
                pnlStep2.Visible = True
                pnlStep3.Visible = True
                pnlStep4.Visible = False
                pnlStep5.Visible = False
                pnlStep6.Visible = False
            ElseIf intStep = 4 Then
                pnlStep1.Visible = False
                pnlStep2.Visible = False
                pnlStep3.Visible = False
                pnlStep4.Visible = True
                pnlStep5.Visible = False
                pnlStep6.Visible = False
            ElseIf intStep = 5 Then
                pnlStep1.Visible = False
                pnlStep2.Visible = False
                pnlStep3.Visible = False
                pnlStep4.Visible = False
                pnlStep5.Visible = True
                pnlStep6.Visible = False
            ElseIf intStep = 6 Then
                pnlStep1.Visible = False
                pnlStep2.Visible = False
                pnlStep3.Visible = False
                pnlStep4.Visible = False
                pnlStep5.Visible = False
                pnlStep6.Visible = True
            End If
            'End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
#End Region

#Region "PAGE EVENTS"

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        CCommon.InitializeOrganizationClientSideTemplate(Session("DomainID"), Session("UserContactID"), radCmbCompany)
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ""
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "none")

            DomainID = Session("DomainID")
            UserCntID = Session("UserContactID")

            intImportFileID = CCommon.ToInteger(GetQueryStringVal("ifd"))
            intItemType = CCommon.ToInteger(GetQueryStringVal("iy"))

            If intImportFileID = 0 AndAlso intItemType = 0 Then
                sMode = 0
                ManagePanels(1)
                btnContinue.Visible = True
            Else
                sMode = 1
                ManagePanels(6)
                btnContinue.Visible = False
            End If

            If Not Page.IsPostBack Then
                _BindImportDropDown()
                _BindItemGroup()
            End If

            btnUpload.Attributes.Add("onclick", "return checkFileExt(" & (lngFileSize * 1024) & ");")
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try

    End Sub

    Private Sub DisplayError(ByVal ex As String)
        DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ex
        DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
    End Sub

#End Region

#Region "BUTTON EVENTS"

    Private Sub btnUpload_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpload.Click
        Try
            Dim SaveLocation As String
            ViewState(_VW_FILE_NAME) = Nothing

            If sMode = 0 Then
                If ddlImport.SelectedValue = IMPORT_TYPE.SalesOrder Or ddlImport.SelectedValue = IMPORT_TYPE.PurchaseOrder Then
                    Dim objImport As New ImportWizard
                    objImport.DomainId = Session("DomainID")
                    objImport.ImportType = ddlImport.SelectedValue
                    objImport.Relationship = 0
                    Dim ds As DataSet = objImport.GetConfiguration()
                    If ds.Tables.Count > 1 AndAlso ds.Tables(1).Rows.Count > 0 Then
                        If ds.Tables(1).Select("numFieldID=" & rblImportOrderItemIdentifier.SelectedValue).Length = 0 Then
                            ScriptManager.RegisterClientScriptBlock(Me, Me.Page.GetType(), "ValidationError", "alert('Field used to pick items for order is not selected in column configuration.');", True)
                            Exit Sub
                        End If
                    Else
                        ScriptManager.RegisterClientScriptBlock(Me, Me.Page.GetType(), "ValidationError", "alert('Configure colums for import.');", True)
                        Exit Sub
                    End If

                    If ddlNoOfOrders.SelectedValue = "1" Then 'Single order per file
                        If rbExistingCustomer.Checked Then
                            If Not (CCommon.ToLong(radCmbCompany.SelectedValue) > 0 AndAlso CCommon.ToLong(ddlContact.SelectedValue) > 0) Then
                                ScriptManager.RegisterClientScriptBlock(Me, Me.Page.GetType(), "ValidationError", "alert('Select orgnization and contact to import order.');", True)
                                Exit Sub
                            End If
                        Else
                            If String.IsNullOrEmpty(txtNewOrganization.Text.Trim()) Or String.IsNullOrEmpty(txtFirstName.Text.Trim()) Or String.IsNullOrEmpty(txtLastName.Text.Trim()) Then
                                ScriptManager.RegisterClientScriptBlock(Me, Me.Page.GetType(), "ValidationError", "alert('Enter new orgnization name and contact fisrt and last name to import order.');", True)
                                Exit Sub
                            End If
                        End If
                    Else 'multiple orders from single file
                        If rbMatchUsingOrganizationID.Checked Then
                            If ds.Tables(1).Select("numFieldID=539").Length = 0 Then
                                ScriptManager.RegisterClientScriptBlock(Me, Me.Page.GetType(), "ValidationError", "alert('Organization ID option is used to pick organization for order but it is not selected in column configuration.');", True)
                                Exit Sub
                            End If
                        Else
                            If CCommon.ToLong(ddlMatchField.SelectedValue) = 0 Then
                                ScriptManager.RegisterClientScriptBlock(Me, Me.Page.GetType(), "ValidationError", "alert('Select field to match organization.');$('[id$=ddlMatchField]').focus();", True)
                                Exit Sub
                            End If

                            If ds.Tables(1).Select("numFieldID=3").Length = 0 Then
                                ScriptManager.RegisterClientScriptBlock(Me, Me.Page.GetType(), "ValidationError", "alert('Organization Name is not selected in column configuration.');", True)
                                Exit Sub
                            End If

                            If ds.Tables(1).Select("numFieldID=" & ddlMatchField.SelectedValue).Length = 0 Then
                                ScriptManager.RegisterClientScriptBlock(Me, Me.Page.GetType(), "ValidationError", "alert('When CSV values mapped to the organization name and the following field match option is used to pick organization for order but another field selected to match organization is not selected in column configuration.');", True)
                                Exit Sub
                            End If
                        End If
                    End If

                    If ds.Tables(1).Select("numFieldID=258").Length = 0 Then
                        ScriptManager.RegisterClientScriptBlock(Me, Me.Page.GetType(), "ValidationError", "alert('Units is not selected in column configuration.');", True)
                        Exit Sub
                    End If

                    If ds.Tables(1).Select("numFieldID=259").Length = 0 Then
                        ScriptManager.RegisterClientScriptBlock(Me, Me.Page.GetType(), "ValidationError", "alert('Unit Price is not selected in column configuration.');", True)
                        Exit Sub
                    End If

                    If ds.Tables(1).Select("numFieldID=293").Length = 0 Then
                        ScriptManager.RegisterClientScriptBlock(Me, Me.Page.GetType(), "ValidationError", "alert('Warehouse is not selected in column configuration.');", True)
                        Exit Sub
                    End If
                ElseIf ddlImport.SelectedValue = IMPORT_TYPE.JournalEntries Then
                    Dim objImport As New ImportWizard
                    objImport.DomainId = Session("DomainID")
                    objImport.ImportType = ddlImport.SelectedValue
                    objImport.Relationship = 0
                    Dim ds As DataSet = objImport.GetConfiguration()

                    If ds.Tables.Count > 1 AndAlso ds.Tables(1).Rows.Count > 0 Then
                        If ds.Tables(1).Select("numFieldID=931").Length = 0 Or
                         ds.Tables(1).Select("numFieldID=932").Length = 0 Or
                         ds.Tables(1).Select("numFieldID=933").Length = 0 Or
                         ds.Tables(1).Select("numFieldID=934").Length = 0 Then
                            ScriptManager.RegisterClientScriptBlock(Me, Me.Page.GetType(), "ValidationError", "alert('Date, Chart of Accounts to Credit, Chart of Accounts to Debit and Amount must be selected in column configuration.');", True)
                            Exit Sub
                        End If
                    Else
                        ScriptManager.RegisterClientScriptBlock(Me, Me.Page.GetType(), "ValidationError", "alert('Configure colums for import.');", True)
                        Exit Sub
                    End If
                End If

                If Not txtFile.PostedFile Is Nothing And txtFile.PostedFile.ContentLength > 0 Then
                    Dim fileName As String = System.IO.Path.GetFileName(txtFile.PostedFile.FileName)
                    SaveLocation = CCommon.GetDocumentPhysicalPath(Session("DomainID")) & fileName
                    CheckRenameFile(SaveLocation, fileName)

                    Try
                        txtFile.PostedFile.SaveAs(SaveLocation)
                        ViewState(_VW_FILE_NAME) = fileName
                        Session("FileLocation") = SaveLocation
                        litMessage.Text = "The file has been uploaded"
                    Catch ex As Exception
                        Throw ex
                    End Try
                Else
                    DisplayError("Please select a file to upload.")
                End If
            ElseIf sMode = 1 Then
                objImport = New ImportWizard(intImportFileID, Session("DomainID"))
                SaveLocation = CCommon.GetDocumentPhysicalPath(Session("DomainID")) & objImport.ImportFileName
                ViewState(_VW_FILE_NAME) = objImport.ImportFileName
                Session("FileLocation") = SaveLocation
            End If

            displayDropdowns()
            ManagePanels(2)
            ScriptManager.RegisterStartupScript(Me, Me.Page.GetType(), "CorrectFieldMapping", "CorrectFieldMapping();", True)
            ClientScript.RegisterStartupScript(Me.GetType, "SetSerial", "SetSerialNo();", True)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try

    End Sub

    Private Sub btnContinue_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnContinue.Click
        Try
            ManagePanels(4)
            ScriptManager.RegisterClientScriptBlock(Me, Me.Page.GetType(), "PreImportAnalysis", "RunPreImportAnalysis();", True)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

#End Region

#Region "DROP DOWN EVENTS"

    Private Sub ddlImport_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlImport.SelectedIndexChanged
        Try
            If ddlImport.SelectedValue > 0 Then

                If ddlImport.SelectedValue = IMPORT_TYPE.Item OrElse
                   ddlImport.SelectedValue = IMPORT_TYPE.Item_WareHouse OrElse
                   ddlImport.SelectedValue = IMPORT_TYPE.Item_Vendor OrElse
                   ddlImport.SelectedValue = IMPORT_TYPE.Item_Assembly OrElse
                   ddlImport.SelectedValue = IMPORT_TYPE.Item_Images Then

                    If BACRM.BusinessLogic.Accounting.ChartOfAccounting.GetDefaultAccount("OE", Session("DomainID")) = 0 Then
                        'Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "SerializeItemValidate", "alert('Please Map Default Opening Balance Equity Account for Your Company from Administration->Domain Details->Accounting->Default Accounts.')", True)
                        DisplayError("Please Map Default Opening Balance Equity Account for Your Company from Administration->Global Settings->Accounting->Default Accounts.")
                        ddlImport.SelectedValue = 0
                        Exit Sub
                    End If

                    If BACRM.BusinessLogic.Accounting.ChartOfAccounting.GetDefaultAccount("IA", Session("DomainID")) = 0 Then
                        DisplayError("Please Map Default Inventory Adjustment Account for Your Company from Administration->Global Settings->Accounting->Default Accounts.")
                        ddlImport.SelectedValue = 0
                        Exit Sub
                    End If
                End If

                btnConfg.Attributes.Add("onClick", "return OpenConf('4','" & ddlImport.SelectedValue & "')")

                lbDownloadCSV.Text = ddlImport.SelectedItem.Text & " Template"
                lbDownloadCSVWithData.Text = ddlImport.SelectedItem.Text & " Template With Data"

                rdbupdate.Items.FindByValue("1").Enabled = True
                rdbupdate.Items.FindByValue("3").Enabled = True

                If ddlImport.SelectedValue = IMPORT_TYPE.Item Then
                    trOrderImport.Visible = False
                    lbDownloadCSVWithData.Visible = True
                    lbDownloadCSV.Visible = True
                    trOptions.Visible = False
                    btnConfg.Visible = True
                    trInfo.Visible = False
                    trAddressType.Visible = False

                ElseIf ddlImport.SelectedValue = IMPORT_TYPE.Item_WareHouse Then
                    trOrderImport.Visible = False
                    lbDownloadCSVWithData.Visible = True
                    lbDownloadCSV.Visible = False
                    trOptions.Visible = True
                    btnConfg.Visible = False
                    trInfo.Visible = True
                    trAddressType.Visible = False

                ElseIf ddlImport.SelectedValue = IMPORT_TYPE.Item_Images Then
                    trOrderImport.Visible = False
                    lbDownloadCSVWithData.Visible = True
                    lbDownloadCSV.Visible = True
                    trOptions.Visible = False
                    btnConfg.Visible = False
                    trInfo.Visible = False
                    trAddressType.Visible = False

                ElseIf ddlImport.SelectedValue = IMPORT_TYPE.Item_Assembly Then
                    trOrderImport.Visible = False
                    lbDownloadCSVWithData.Visible = True
                    lbDownloadCSV.Visible = True
                    trOptions.Visible = False
                    btnConfg.Visible = False
                    trInfo.Visible = False
                    trAddressType.Visible = False

                ElseIf ddlImport.SelectedValue = IMPORT_TYPE.Item_Vendor Then
                    trOrderImport.Visible = False
                    lbDownloadCSVWithData.Visible = True
                    lbDownloadCSV.Visible = True
                    trOptions.Visible = False
                    btnConfg.Visible = False
                    trInfo.Visible = False
                    trAddressType.Visible = False

                ElseIf ddlImport.SelectedValue = IMPORT_TYPE.Organization Then
                    trOrderImport.Visible = False
                    lbDownloadCSVWithData.Visible = False
                    lbDownloadCSV.Visible = True
                    trOptions.Visible = False
                    btnConfg.Visible = True
                    trInfo.Visible = False
                    trAddressType.Visible = False

                ElseIf ddlImport.SelectedValue = IMPORT_TYPE.Organization_Correspondence Then
                    trOrderImport.Visible = False
                    lbDownloadCSVWithData.Visible = False
                    lbDownloadCSV.Visible = True
                    trOptions.Visible = False
                    btnConfg.Visible = False
                    trInfo.Visible = False
                    trAddressType.Visible = False

                    rdbupdate.Items.FindByValue("2").Selected = True
                    rdbupdate.Items.FindByValue("1").Enabled = False
                    rdbupdate.Items.FindByValue("3").Enabled = False
                ElseIf ddlImport.SelectedValue = IMPORT_TYPE.Contact Then
                    trOrderImport.Visible = False
                    lbDownloadCSVWithData.Visible = False
                    lbDownloadCSV.Visible = True
                    trOptions.Visible = False
                    btnConfg.Visible = True
                    trInfo.Visible = False
                    trAddressType.Visible = False
                ElseIf ddlImport.SelectedValue = IMPORT_TYPE.Address_Details Then
                    trOrderImport.Visible = False
                    lbDownloadCSVWithData.Visible = True
                    lbDownloadCSV.Visible = False
                    trOptions.Visible = False
                    btnConfg.Visible = False
                    trInfo.Visible = False
                    trAddressType.Visible = True
                ElseIf ddlImport.SelectedValue = IMPORT_TYPE.SalesOrder Then
                    trOrderImport.Visible = True
                    lbDownloadCSVWithData.Visible = False
                    lbDownloadCSV.Visible = True
                    trOptions.Visible = False
                    btnConfg.Visible = True
                    trInfo.Visible = False
                    trAddressType.Visible = False

                    rdbupdate.Items.FindByValue("2").Selected = True
                    rdbupdate.Items.FindByValue("1").Enabled = False
                    rdbupdate.Items.FindByValue("3").Enabled = False
                ElseIf ddlImport.SelectedValue = IMPORT_TYPE.PurchaseOrder Then
                    trOrderImport.Visible = True
                    lbDownloadCSVWithData.Visible = False
                    lbDownloadCSV.Visible = True
                    trOptions.Visible = False
                    btnConfg.Visible = True
                    trInfo.Visible = False
                    trAddressType.Visible = False

                    rdbupdate.Items.FindByValue("2").Selected = True
                    rdbupdate.Items.FindByValue("1").Enabled = False
                    rdbupdate.Items.FindByValue("3").Enabled = False
                ElseIf ddlImport.SelectedValue = IMPORT_TYPE.JournalEntries Then
                    trOrderImport.Visible = False
                    lbDownloadCSVWithData.Visible = False
                    lbDownloadCSV.Visible = True
                    trOptions.Visible = False
                    btnConfg.Visible = True
                    trInfo.Visible = False
                    trAddressType.Visible = False

                    rdbupdate.Items.FindByValue("2").Selected = True
                    rdbupdate.Items.FindByValue("1").Enabled = False
                    rdbupdate.Items.FindByValue("3").Enabled = False
                End If
            End If

        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

#End Region

#Region "BIND DROP DOWNS"
    Dim dtCategory As DataTable = Nothing
    Dim dtSection As DataTable = Nothing
    Dim dsMain As DataSet = Nothing

    Private Sub _BindImportDropDown(Optional ByVal ddlImportField As DropDownList = Nothing)
        Try
            objImport = New ImportWizard
            With objImport
                If ddlImportField Is Nothing Then
                    .ImportMasterID = 0
                    .Mode = 1
                    dtCategory = objImport.GetImportMasterCategories().Tables(0)

                    ddlImport.DataSource = dtCategory
                    ddlImport.DataTextField = "vcImportName"
                    ddlImport.DataValueField = "intImportMasterID"
                    ddlImport.DataBind()
                    ddlImport.Items.Insert(0, New ListItem("--Select One--", "0"))

                    If sMode = 1 Then
                        ddlImport.SelectedValue = intItemType
                        trUpload.Visible = False
                        btnUpload_Click(Nothing, Nothing)
                    Else
                        trUpload.Visible = True
                    End If

                Else
                    If dtMap IsNot Nothing Then
                        For Each dr As DataRow In dtMap.Rows
                            If listMappedFields Is Nothing Or (Not listMappedFields Is Nothing AndAlso Not listMappedFields.Contains(CCommon.ToInteger(dr("ID")))) Then
                                ddlImportField.Items.Add(New ListItem(CCommon.ToString(dr("Destination")), CCommon.ToInteger(dr("ID"))))
                            End If
                        Next
                    End If

                    ddlImportField.Items.Insert(0, New ListItem("--Do not import--", "0"))
                End If
            End With
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub _BindItemGroup()
        Try
            Dim objItems As New CItems
            objItems.DomainID = Session("DomainID")
            objItems.ItemCode = 0
            ddlItemGroup.DataSource = objItems.GetItemGroups.Tables(0)
            ddlItemGroup.DataTextField = "vcItemGroup"
            ddlItemGroup.DataValueField = "numItemGroupID"
            ddlItemGroup.DataBind()
            ddlItemGroup.Items.Insert(0, New ListItem("--Select One--", "0"))
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
#End Region

#Region "DISPLAY DROPDOWNS"

    Private Sub displayDropdowns()
        Try
            Dim dtHeading As New DataTable
            Dim fileLocation As String = Session("FileLocation")
            Dim streamReader As New StreamReader(fileLocation)
            Dim intCount As Integer = 0
            Dim intCountFor As Integer = 0
            Dim intStart As Integer
            Dim strSplitValue() As String = {""}
            Dim strSplitHeading() As String = {""}
            Dim strLine As String = streamReader.ReadLine()

            dtMap.Columns.Add("ID")
            dtMap.Columns.Add("Destination")
            dtMap.Columns.Add("ColName")

            CreateDataTable(dtPart1)
            CreateDataTable(dtPart2)

            Try
                intStart = 0
                Do While Not strLine Is Nothing
                    Dim drMap As DataRow
                    drMap = dtMap.NewRow
                    strSplitHeading = Split(strLine, ",")
                    For intCountFor = intStart To strSplitHeading.Length - 1
                        drMap = dtMap.NewRow
                        drMap("Destination") = strSplitHeading(intCountFor).Replace("""", "").ToString
                        drMap("Destination") = drMap("Destination") & " (Col: " & intCountFor + 1 & " )"
                        drMap("ColName") = strSplitHeading(intCountFor).Replace("""", "").ToString
                        drMap("ID") = intCountFor + 1
                        dtMap.Rows.Add(drMap)
                    Next
                    Exit Do
                Loop

                'Create temp Datatable that contains single row value to bind Parent Repeater Datasource with single row.
                '----------------------------------------------------------------------
                Dim dtTemp As New DataTable
                dtTemp.Columns.Add("ID")
                Dim drTemp As DataRow
                drTemp = dtTemp.NewRow
                drTemp("ID") = 1
                dtTemp.Rows.Add(drTemp)
                dtTemp.AcceptChanges()

                objImport = New ImportWizard
                With objImport
                    .ImportMasterID = CInt(ddlImport.SelectedValue)
                    .Mode = 2
                    .ImportFileID = IIf(sMode = 0, 0, intImportFileID)
                    .DomainId = Session("DomainID")
                    .InsertUpdateType = rdbupdate.SelectedValue
                    .ItemLinkingID = CCommon.ToLong(hdnItemLinkingID.Value)
                    dsMain = objImport.GetImportMasterCategories()
                    dtSection = dsMain.Tables(0)
                    If dsMain.Tables.Count > 2 AndAlso dsMain.Tables(2).Rows.Count > 0 Then
                        dtCategory = dsMain.Tables(2)
                    Else
                        dtCategory = dsMain.Tables(1)
                    End If

                End With

                rptrParent.DataSource = dtSection
                rptrParent.DataBind()
                '----------------------------------------------------------------------
            Catch ex As Exception
                Throw ex
            Finally
                streamReader.Close()
            End Try
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

#End Region

#Region "CREATE TABLE SCHEMA"

    Private Sub createTableSchema(ByVal dtMappedData As DataTable)
        Try
            Dim dataTableDestination As New DataTable
            Dim dataTableHeading As New DataTable
            Dim dataTable As New DataTable
            Dim dsData As New DataSet
            Dim numRows As Integer
            Dim intCnt As Integer
            Dim dataRowHeading As DataRow
            objImport = New ImportWizard

            dataTableHeading.Columns.Add("FormFieldID").SetOrdinal(0)
            dataTableHeading.Columns.Add("FormFieldName").SetOrdinal(1)
            dataTableHeading.Columns.Add("ImportFieldID").SetOrdinal(2)
            dataTableHeading.Columns.Add("dbFieldName").SetOrdinal(3)

            If sMode = 0 Then
                If dtMappedData IsNot Nothing AndAlso dtMappedData.Rows.Count > 0 Then
                    dsData.Tables.Add(dtMappedData)
                    dataTable = dtMappedData
                End If
            ElseIf sMode = 1 Then
                objImport.DomainId = Session("DomainID")
                objImport.ImportFileID = intImportFileID
                dsData = objImport.LoadMappedData()
                dataTable = dsData.Tables(1)
            End If
            numRows = dataTable.Rows.Count()

            For intCnt = 0 To numRows - 1
                'If sMode = 1 Then
                '    If dtMappedData.Select("FormFieldID = " & dataTable.Rows(intCnt).Item("FormFieldID")).Length > 0 Then
                '        dataRowHeading = dataTableHeading.NewRow
                '        dataRowHeading("FormFieldID") = dataTable.Rows(intCnt).Item("FormFieldID")
                '        dataRowHeading("FormFieldName") = dataTable.Rows(intCnt).Item("FormFieldName")
                '        dataRowHeading("ImportFieldID") = dataTable.Rows(intCnt).Item("ImportFieldID")
                '        dataRowHeading("dbFieldName") = dataTable.Rows(intCnt).Item("dbFieldName")
                '        dataTableHeading.Rows.Add(dataRowHeading)
                '    End If
                'Else
                dataRowHeading = dataTableHeading.NewRow
                dataRowHeading("FormFieldID") = dataTable.Rows(intCnt).Item("FormFieldID")
                dataRowHeading("FormFieldName") = dataTable.Rows(intCnt).Item("FormFieldName")
                dataRowHeading("ImportFieldID") = dataTable.Rows(intCnt).Item("ImportFieldID")
                dataRowHeading("dbFieldName") = dataTable.Rows(intCnt).Item("dbFieldName")
                dataTableHeading.Rows.Add(dataRowHeading)
                'End If
            Next

            Session("dataTableHeadingObj") = dataTableHeading
            Session("IntCount") = intCnt - 1

            Dim drHead As DataRow
            dataTableDestination.Columns.Add("SrNo")
            dataTableDestination.Columns("SrNo").AutoIncrement = True
            dataTableDestination.Columns("SrNo").AutoIncrementSeed = 1
            dataTableDestination.Columns("SrNo").AutoIncrementStep = 1

            For Each drHead In dataTableHeading.Rows
                dataTableDestination.Columns.Add(drHead.Item(1))
            Next

            Session("dataTableDestinationObj") = dataTableDestination
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

#End Region

#Region "CREATE DATATABLE"

    Private Sub CreateDataTable(ByRef dtResult As DataTable)
        If dtResult.Columns.Count <= 0 Then
            dtResult.Columns.Add("SrNo")
            dtResult.Columns.Add("intImportFieldID")
            dtResult.Columns.Add("numFormFieldID")
            dtResult.Columns.Add("vcSectionNames")
            dtResult.Columns.Add("vcFormFieldName")
            dtResult.Columns.Add("vcDbColumnName")
            dtResult.Columns.Add("intSectionID")
            dtResult.Columns.Add("intMapColumnNo")
            dtResult.Columns.Add("IsCustomField")
            dtResult.Columns.Add("bitRequired")
            dtResult.Columns.Add("vcReqString")

            Dim primaryKey(1) As DataColumn
            primaryKey(1) = dtResult.Columns("vcDbColumnName")
            dtResult.PrimaryKey = primaryKey
            dtResult.AcceptChanges()
        End If
    End Sub

#End Region

#Region "REPEATER EVENTS"

    Private Sub rptrParent_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rptrParent.ItemCommand

        Try
            If e.CommandName = "Confirm" Then
                ClientScript.RegisterStartupScript(Me.GetType, "SetSerial", "SetSerialNo();", True)
                ClientScript.RegisterStartupScript(Me.GetType, "Step42", "$('#divStep4').toggle();", True)

                Dim dtMappedData As New DataTable
                dtMappedData = GetDataTableFromRepeater("rptrChild", 2)


                If ddlImport.SelectedValue = IMPORT_TYPE.Item Then
                    If rdbupdate.SelectedValue = "1" Then 'Update
                        Dim itemLinkingID As Integer = CCommon.ToInteger(DirectCast(rptrParent.Controls(0).Controls(0).FindControl("ddlItemLinking"), DropDownList).SelectedValue)

                        If itemLinkingID = 0 Then
                            ClientScript.RegisterStartupScript(Me.GetType, "AlertItemID", "alert('Select Linking ID.');", True)
                            Exit Sub
                        ElseIf itemLinkingID = 1 Then 'ItemID
                            If dtMappedData.Select("FormFieldID=211").Count = 0 Then
                                ClientScript.RegisterStartupScript(Me.GetType, "AlertItemID", "alert('Item Id is selected as Linking ID but it is not added as column in uploaded csv file.');", True)
                                Exit Sub
                            End If
                        ElseIf itemLinkingID = 2 Then 'SKU
                            If dtMappedData.Select("FormFieldID=281").Count = 0 Then
                                ClientScript.RegisterStartupScript(Me.GetType, "AlertItemID", "alert('SKU is selected as Linking ID but it is not added as column in uploaded csv file.');", True)
                                Exit Sub
                            End If
                        ElseIf itemLinkingID = 3 Then 'UPC
                            If dtMappedData.Select("FormFieldID=203").Count = 0 Then
                                ClientScript.RegisterStartupScript(Me.GetType, "AlertItemID", "alert('UPC is selected as Linking ID but it is not added as column in uploaded csv file.');", True)
                                Exit Sub
                            End If
                        ElseIf itemLinkingID = 4 Then 'MODELID
                            If dtMappedData.Select("FormFieldID=193").Count = 0 Then
                                ClientScript.RegisterStartupScript(Me.GetType, "AlertItemID", "alert('Model ID is selected as Linking ID but it is not added as column in uploaded csv file.');", True)
                                Exit Sub
                            End If
                        ElseIf itemLinkingID = 7 Then 'Item Name
                            If dtMappedData.Select("FormFieldID=189").Count = 0 Then
                                ClientScript.RegisterStartupScript(Me.GetType, "AlertItemID", "alert('Item Name is selected as Linking ID but it is not added as column in uploaded csv file.');", True)
                                Exit Sub
                            End If
                        End If

                        hdnItemLinkingID.Value = itemLinkingID
                    ElseIf rdbupdate.SelectedValue = "2" Then 'Insert
                        If dtMappedData.Select("FormFieldID=189").Count = 0 Or
                          dtMappedData.Select("FormFieldID=294").Count = 0 Or
                          dtMappedData.Select("FormFieldID=271").Count = 0 Or
                          dtMappedData.Select("FormFieldID=272").Count = 0 Or
                          dtMappedData.Select("FormFieldID=270").Count = 0 Then
                            ClientScript.RegisterStartupScript(Me.GetType, "AlertItemID", "alert('Item Name, Item Type, Income Account, Asset Account and COGs/Expense Account are required for importing new item(s).');", True)
                            Exit Sub
                        End If
                    ElseIf rdbupdate.SelectedValue = "3" Then 'Insert & Update
                        If dtMappedData.Select("FormFieldID=189").Count = 0 Or
                          dtMappedData.Select("FormFieldID=294").Count = 0 Or
                          dtMappedData.Select("FormFieldID=271").Count = 0 Or
                          dtMappedData.Select("FormFieldID=272").Count = 0 Or
                          dtMappedData.Select("FormFieldID=270").Count = 0 Then
                            ClientScript.RegisterStartupScript(Me.GetType, "AlertItemID", "alert('Item Name, Item Type, Income Account, Asset Account and COGs/Expense Account are required for importing new item(s).');", True)
                            Exit Sub
                        End If

                        Dim itemLinkingID As Integer = CCommon.ToInteger(DirectCast(rptrParent.Controls(0).Controls(0).FindControl("ddlItemLinking"), DropDownList).SelectedValue)
                        If itemLinkingID = 0 Then
                            ClientScript.RegisterStartupScript(Me.GetType, "AlertItemID", "alert('Linking ID is required for updating item(s).');", True)
                            Exit Sub
                        ElseIf itemLinkingID = 1 Then 'ItemID
                            If dtMappedData.Select("FormFieldID=211").Count = 0 Then
                                ClientScript.RegisterStartupScript(Me.GetType, "AlertItemID", "alert('Item Id is selected as Linking ID but it is not added as column in uploaded csv file.');", True)
                                Exit Sub
                            End If
                        ElseIf itemLinkingID = 2 Then 'SKU
                            If dtMappedData.Select("FormFieldID=281").Count = 0 Then
                                ClientScript.RegisterStartupScript(Me.GetType, "AlertItemID", "alert('SKU is selected as Linking ID but it is not added as column in uploaded csv file.');", True)
                                Exit Sub
                            End If
                        ElseIf itemLinkingID = 3 Then 'UPC
                            If dtMappedData.Select("FormFieldID=203").Count = 0 Then
                                ClientScript.RegisterStartupScript(Me.GetType, "AlertItemID", "alert('UPC is selected as Linking ID but it is not added as column in uploaded csv file.');", True)
                                Exit Sub
                            End If
                        ElseIf itemLinkingID = 4 Then 'MODELID
                            If dtMappedData.Select("FormFieldID=193").Count = 0 Then
                                ClientScript.RegisterStartupScript(Me.GetType, "AlertItemID", "alert('Model ID is selected as Linking ID but it is not added as column in uploaded csv file.');", True)
                                Exit Sub
                            End If
                        ElseIf itemLinkingID = 7 Then 'Item Name
                            If dtMappedData.Select("FormFieldID=189").Count = 0 Then
                                ClientScript.RegisterStartupScript(Me.GetType, "AlertItemID", "alert('Item Name is selected as Linking ID but it is not added as column in uploaded csv file.');", True)
                                Exit Sub
                            End If
                        End If

                        hdnItemLinkingID.Value = itemLinkingID
                    End If
                ElseIf ddlImport.SelectedValue = IMPORT_TYPE.Organization Then
                    If rdbupdate.SelectedValue = "1" Then 'Update
                        Dim linkingID As Short = CCommon.ToShort(DirectCast(rptrParent.Controls(0).Controls(0).FindControl("ddlOrganizationLinking"), DropDownList).SelectedValue)
                        If linkingID = 0 Then
                            ClientScript.RegisterStartupScript(Me.GetType, "AlertItemID", "alert('Linking ID is required for updating organization(s).');", True)
                            Exit Sub
                        ElseIf linkingID = 1 Then 'Non Biz Company ID
                            If dtMappedData.Select("FormFieldID=380").Count = 0 Then
                                ClientScript.RegisterStartupScript(Me.GetType, "AlertNonBizCompanyID", "alert('Non Biz Company ID is selected as Linking ID but it is not added as column in uploaded csv file.');", True)
                                Exit Sub
                            End If
                        ElseIf linkingID = 2 Then 'Organization ID
                            If dtMappedData.Select("FormFieldID=539").Count = 0 Then
                                ClientScript.RegisterStartupScript(Me.GetType, "AlertOrganizationID", "alert('Organization ID is selected as Linking ID but it is not added as column in uploaded csv file.');", True)
                                Exit Sub
                            End If
                        End If

                        hdnItemLinkingID.Value = linkingID
                    ElseIf rdbupdate.SelectedValue = "2" Then 'Insert
                        If dtMappedData.Select("FormFieldID=380").Count = 0 Or dtMappedData.Select("FormFieldID=3").Count = 0 Or dtMappedData.Select("FormFieldID=6").Count = 0 Or dtMappedData.Select("FormFieldID=451").Count = 0 Then
                            ClientScript.RegisterStartupScript(Me.GetType, "AlertOrganizationImport", "alert('Non Biz Company ID(Unique for each organization), Organization Name, Relationship and Relationship Type are required for importing new organization(s).');", True)
                            Exit Sub
                        End If
                    ElseIf rdbupdate.SelectedValue = "3" Then 'Insert & Update
                        If dtMappedData.Select("FormFieldID=380").Count = 0 Or dtMappedData.Select("FormFieldID=3").Count = 0 Or dtMappedData.Select("FormFieldID=6").Count = 0 Or dtMappedData.Select("FormFieldID=451").Count = 0 Then
                            ClientScript.RegisterStartupScript(Me.GetType, "AlertOrganizationImport", "alert('Non Biz Company ID(Unique for each organization), Organization Name, Relationship and Relationship Type are required for importing new organization(s).');", True)
                            Exit Sub
                        End If

                        Dim linkingID As Short = CCommon.ToShort(DirectCast(rptrParent.Controls(0).Controls(0).FindControl("ddlOrganizationLinking"), DropDownList).SelectedValue)
                        If linkingID = 0 Then
                            ClientScript.RegisterStartupScript(Me.GetType, "AlertItemID", "alert('Linking ID is required for updating organization(s).');", True)
                            Exit Sub
                        ElseIf linkingID = 1 Then 'Non Biz Company ID
                            If dtMappedData.Select("FormFieldID=380").Count = 0 Then
                                ClientScript.RegisterStartupScript(Me.GetType, "AlertNonBizCompanyID", "alert('Non Biz Company ID is selected as Linking ID but it is not added as column in uploaded csv file.');", True)
                                Exit Sub
                            End If
                        ElseIf linkingID = 2 Then 'Organization ID
                            If dtMappedData.Select("FormFieldID=539").Count = 0 Then
                                ClientScript.RegisterStartupScript(Me.GetType, "AlertOrganizationID", "alert('Organization ID is selected as Linking ID but it is not added as column in uploaded csv file.');", True)
                                Exit Sub
                            End If
                        End If

                        hdnItemLinkingID.Value = linkingID
                    End If
                ElseIf ddlImport.SelectedValue = IMPORT_TYPE.Contact Then
                    If rdbupdate.SelectedValue = "1" Then 'Update
                        Dim linkingID As Short = CCommon.ToShort(DirectCast(rptrParent.Controls(0).Controls(0).FindControl("ddlContactLinking"), DropDownList).SelectedValue)
                        If linkingID = 0 Then
                            ClientScript.RegisterStartupScript(Me.GetType, "AlertItemID", "alert('Linking ID is required for updating contact(s).');", True)
                            Exit Sub
                        ElseIf linkingID = 1 Then 'Non Biz Contact ID
                            If dtMappedData.Select("FormFieldID=386").Count = 0 Then
                                ClientScript.RegisterStartupScript(Me.GetType, "AlertNonBizContactID", "alert('Non Biz Contact ID is selected as Linking ID but it is not added as column in uploaded csv file.');", True)
                                Exit Sub
                            End If
                        ElseIf linkingID = 2 Then 'Contact ID
                            If dtMappedData.Select("FormFieldID=860").Count = 0 Then
                                ClientScript.RegisterStartupScript(Me.GetType, "AlertContactID", "alert('Contact ID is selected as Linking ID but it is not added as column in uploaded csv file.');", True)
                                Exit Sub
                            End If
                        End If

                        hdnItemLinkingID.Value = linkingID
                    ElseIf rdbupdate.SelectedValue = "3" Then 'Insert & Update
                        If (dtMappedData.Select("FormFieldID=380").Count = 0 AndAlso dtMappedData.Select("FormFieldID=539").Count = 0) Or dtMappedData.Select("FormFieldID=386").Count = 0 Or dtMappedData.Select("FormFieldID=51").Count = 0 Or dtMappedData.Select("FormFieldID=52").Count = 0 Then
                            ClientScript.RegisterStartupScript(Me.GetType, "AlertOrganizationImport", "alert('Non Biz Company ID/Organization ID(Unique for each organization), Non Biz Contact ID(Unique for each contact), Contact First Name and Contact Last Name are required for importing new contact(s).');", True)
                            Exit Sub
                        End If

                        Dim linkingID As Short = CCommon.ToShort(DirectCast(rptrParent.Controls(0).Controls(0).FindControl("ddlContactLinking"), DropDownList).SelectedValue)
                        If linkingID = 0 Then
                            ClientScript.RegisterStartupScript(Me.GetType, "AlertItemID", "alert('Linking ID is required for adding new contact(s) to particular organization.');", True)
                            Exit Sub
                        ElseIf linkingID = 5 Then  'Non Biz Company ID & Non Biz Contact ID
                            If dtMappedData.Select("FormFieldID=380").Count = 0 Or dtMappedData.Select("FormFieldID=386").Count = 0 Then
                                ClientScript.RegisterStartupScript(Me.GetType, "AlertNonBizCompanyID", "alert('Non Biz Company ID & Non Biz Contact ID is selected as Linking ID but it is not added as column in uploaded csv file.');", True)
                                Exit Sub
                            End If
                        ElseIf linkingID = 6 Then 'Non Biz Company ID & Contact ID
                            If dtMappedData.Select("FormFieldID=860").Count = 0 Or dtMappedData.Select("FormFieldID=380").Count = 0 Then
                                ClientScript.RegisterStartupScript(Me.GetType, "AlertOrganizationID", "alert('Non Biz Company ID & Contact ID is selected as Linking ID but it is not added as column in uploaded csv file.');", True)
                                Exit Sub
                            End If
                        ElseIf linkingID = 7 Then 'Organization ID & Non Biz Contact ID
                            If dtMappedData.Select("FormFieldID=539").Count = 0 Or dtMappedData.Select("FormFieldID=386").Count = 0 Then
                                ClientScript.RegisterStartupScript(Me.GetType, "AlertOrganizationID", "alert('Organization ID & Non Biz Contact ID is selected as Linking ID but it is not added as column in uploaded csv file.');", True)
                                Exit Sub
                            End If
                        ElseIf linkingID = 8 Then 'Organization ID & Contact ID
                            If dtMappedData.Select("FormFieldID=539").Count = 0 Or dtMappedData.Select("FormFieldID=860").Count = 0 Then
                                ClientScript.RegisterStartupScript(Me.GetType, "AlertOrganizationID", "alert('Organization ID & Contact ID is selected as Linking ID but it is not added as column in uploaded csv file.');", True)
                                Exit Sub
                            End If
                        End If

                        hdnItemLinkingID.Value = linkingID
                    Else 'Insert
                        If dtMappedData.Select("FormFieldID=386").Count = 0 Or dtMappedData.Select("FormFieldID=51").Count = 0 Or dtMappedData.Select("FormFieldID=52").Count = 0 Then
                            ClientScript.RegisterStartupScript(Me.GetType, "AlertOrganizationImport", "alert('Non Biz Contact ID(Unique for each contact), Contact First Name and Contact Last Name are required for importing new contact(s).');", True)
                            Exit Sub
                        End If

                        Dim linkingID As Short = CCommon.ToShort(DirectCast(rptrParent.Controls(0).Controls(0).FindControl("ddlContactLinking"), DropDownList).SelectedValue)
                        If linkingID = 0 Then
                            ClientScript.RegisterStartupScript(Me.GetType, "AlertItemID", "alert('Linking ID is required for adding new contact(s) to particular organization.');", True)
                            Exit Sub
                        ElseIf linkingID = 3 Then 'Non Biz Company ID
                            If dtMappedData.Select("FormFieldID=380").Count = 0 Then
                                ClientScript.RegisterStartupScript(Me.GetType, "AlertNonBizCompanyID", "alert('Non Biz Company ID is selected as Linking ID but it is not added as column in uploaded csv file.');", True)
                                Exit Sub
                            End If
                        ElseIf linkingID = 4 Then 'Organization ID
                            If dtMappedData.Select("FormFieldID=539").Count = 0 Then
                                ClientScript.RegisterStartupScript(Me.GetType, "AlertOrganizationID", "alert('Organization ID is selected as Linking ID but it is not added as column in uploaded csv file.');", True)
                                Exit Sub
                            End If
                        End If

                        hdnItemLinkingID.Value = linkingID
                    End If
                ElseIf ddlImport.SelectedValue = IMPORT_TYPE.SalesOrder Or ddlImport.SelectedValue = IMPORT_TYPE.PurchaseOrder Then
                    Dim iteLinkingID As Long = CCommon.ToLong(rblImportOrderItemIdentifier.SelectedValue)
                    hdnItemLinkingID.Value = iteLinkingID

                    If dtMappedData.Select("FormFieldID=" & iteLinkingID).Count = 0 Then
                        ScriptManager.RegisterClientScriptBlock(Me, Me.Page.GetType(), "ValidationError", "alert('Field used to pick items for order is not selected in column configuration.');", True)
                        Exit Sub
                    End If

                    If CCommon.ToShort(ddlNoOfOrders.SelectedValue) = "2" Then 'multiple order per file
                        If rbMatchUsingOrganizationID.Checked Then
                            If dtMappedData.Select("FormFieldID=539").Length = 0 Then
                                ScriptManager.RegisterClientScriptBlock(Me, Me.Page.GetType(), "ValidationError", "alert('Organization ID option is used to pick organization for order but it is not selected in column configuration.');", True)
                                Exit Sub
                            End If
                        Else
                            If CCommon.ToLong(ddlMatchField.SelectedValue) = 0 Then
                                ScriptManager.RegisterClientScriptBlock(Me, Me.Page.GetType(), "ValidationError", "alert('Select field to match organization.');$('[id$=ddlMatchField]').focus();", True)
                                Exit Sub
                            End If

                            If dtMappedData.Select("FormFieldID=3").Length = 0 Then
                                ScriptManager.RegisterClientScriptBlock(Me, Me.Page.GetType(), "ValidationError", "alert('When CSV values mapped to the organization name and the following field match option is used to pick organization for order but Organization Name is not selected in column configuration.');", True)
                                Exit Sub
                            End If

                            If dtMappedData.Select("FormFieldID=" & CCommon.ToLong(ddlMatchField.SelectedValue)).Count = 0 Then
                                ScriptManager.RegisterClientScriptBlock(Me, Me.Page.GetType(), "ValidationError", "alert('When CSV values mapped to the organization name and the following field match option is used to pick organization for order but another field selected to match organization is not selected in column configuration.');", True)
                                Exit Sub
                            End If
                        End If
                    End If

                    If dtMappedData.Select("FormFieldID=258").Length = 0 Then
                        ScriptManager.RegisterClientScriptBlock(Me, Me.Page.GetType(), "ValidationError", "alert('Units is not selected in column configuration.');", True)
                        Exit Sub
                    End If

                    If dtMappedData.Select("FormFieldID=259").Length = 0 Then
                        ScriptManager.RegisterClientScriptBlock(Me, Me.Page.GetType(), "ValidationError", "alert('Unit Price is not selected in column configuration.');", True)
                        Exit Sub
                    End If

                    If dtMappedData.Select("FormFieldID=293").Length = 0 Then
                        ScriptManager.RegisterClientScriptBlock(Me, Me.Page.GetType(), "ValidationError", "alert('Warehouse is not selected in column configuration.');", True)
                        Exit Sub
                    End If
                ElseIf ddlImport.SelectedValue = IMPORT_TYPE.JournalEntries Then
                    If dtMappedData.Select("FormFieldID=931").Length = 0 Or
                         dtMappedData.Select("FormFieldID=932").Length = 0 Or
                         dtMappedData.Select("FormFieldID=933").Length = 0 Or
                         dtMappedData.Select("FormFieldID=934").Length = 0 Then
                        ScriptManager.RegisterClientScriptBlock(Me, Me.Page.GetType(), "ValidationError", "alert('Date, Chart of Accounts to Credit, Chart of Accounts to Debit and Amount must be selected in column configuration.');", True)
                        Exit Sub
                    End If
                End If

                If sMode = 0 AndAlso dtMappedData.Rows.Count <= 0 Then Exit Sub

                sMode = 0
                ViewState(_VW_MAPPED_DATATABLE) = dtMappedData
                'SaveMappedData(dtMappedData)
                createTableSchema(dtMappedData)

                Dim arryList() As String
                Dim strdllValue As String = ""
                Dim intArryValue As Integer = 0

                Dim dataTableDestination As New DataTable
                Dim dataTableHeading As New DataTable
                Dim fileLocation As String = Session("FileLocation")
                Dim streamReader As StreamReader
                streamReader = File.OpenText(fileLocation)
                Dim intCount As Integer = Session("IntCount")
                Dim intCountFor As Integer = 0
                Dim strSplitValue() As String = {""}
                Dim strSplitHeading() As String = {""}
                Dim intCountSession As Integer = Session("IntCount")
                dataTableHeading = Session("dataTableHeadingObj")
                dataTableDestination = Session("dataTableDestinationObj")
                dataTableDestination.Clear()
                Dim csv As CsvReader = New CsvReader(streamReader, True)
                Dim dataRowDestination As DataRow
                Dim drHead As DataRow
                Try
                    csv.MissingFieldAction = MissingFieldAction.ReplaceByEmpty
                    csv.SupportsMultiline = True

                    If dtMappedData.Rows.Count > 0 Then
                        For intCnt As Integer = 0 To dtMappedData.Rows.Count - 1
                            strdllValue = strdllValue & "," & dtMappedData.Rows(intCnt)("ImportFieldID")
                        Next
                    Else
                        For intCnt As Integer = 0 To dataTableHeading.Rows.Count - 1
                            strdllValue = strdllValue & "," & dataTableHeading.Rows(intCnt)("ImportFieldID")
                        Next
                    End If

                    strdllValue = strdllValue.TrimStart(",")
                    strdllValue = strdllValue.TrimEnd(",")
                    arryList = Split(strdllValue, ",")

                    Dim i1000 As Integer = 0
                    While (csv.ReadNextRecord())
                        Dim intEmpty As Integer = 0
                        dataRowDestination = dataTableDestination.NewRow
                        intCount = 0
                        For Each drHead In dataTableHeading.Rows
                            intArryValue = CType(arryList(intCount) - 1, Integer)
                            dataRowDestination(drHead.Item(1)) = csv(intArryValue).ToString
                            intCount += 1

                            If csv(intArryValue).ToString = "" Then
                                intEmpty += 1
                            End If
                        Next
                        If intEmpty <> arryList.Length Then
                            dataTableDestination.Rows.Add(dataRowDestination)
                        End If
                        i1000 = i1000 + 1

                        If dataTableDestination.Rows.Count >= lngFileRecords Then
                            DisplayError("Excel rows should not be more than " & lngFileRecords & ". Please verify & upload maximum " & lngFileRecords & " records per file.")
                            Exit Sub
                        End If
                    End While

                    Session("dataTableDestinationObj") = dataTableDestination

                    Dim dtStore As New DataTable
                    If dataTableDestination.Columns.Contains("Item ID") = True Then
                        'dataTableDestination.Columns.Remove("Item ID")
                    End If

                    dtStore = dataTableDestination
                    'Display first 500 records only
                    Dim dvStore As DataView
                    dvStore = dtStore.DefaultView
                    dvStore.RowFilter = "SrNo < 501"
                    gvPreviewData.DataSource = dvStore
                    gvPreviewData.DataBind()

                Catch ex As Exception
                    If ex.Message = "An item with the same key has already been added." Then
                        streamReader.Close()
                        DisplayError("Duplicate column names found in CSV file, Your option is to remove duplicate columns and upload it again.")
                        Exit Sub
                    End If
                    DisplayError(ex.Message)
                Finally
                    streamReader.Close()
                End Try
            End If

            ManagePanels(3)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Private Sub rptrParent_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptrParent.ItemDataBound
        Try
            Select Case e.Item.ItemType
                Case ListItemType.Item
                    Dim hdnID As HiddenField = e.Item.FindControl("hdnID")
                    If hdnID IsNot Nothing Then
                        Dim dtTemp As DataTable = dtCategory.Copy
                        Dim dvCategory As DataView = dtTemp.DefaultView
                        dvCategory.RowFilter = "intSectionID = " & CInt(hdnID.Value)

                        If dvCategory.Count > 0 Then
                            Dim intRow As Integer = 0
                            For Each drRow As DataRowView In dvCategory
                                If intRow < (dvCategory.Count / 2) Then
                                    dtPart1.ImportRow(drRow.Row)
                                Else
                                    dtPart2.ImportRow(drRow.Row)
                                End If

                                dtPart1.AcceptChanges()
                                dtPart2.AcceptChanges()
                                intRow += 1
                            Next
                        End If

                        Dim rpt1 As Repeater = CType(e.Item.FindControl("rptrChild1"), Repeater)
                        With rpt1
                            .DataSource = dtPart1
                            .DataBind()
                        End With

                        Dim rpt2 As Repeater = CType(e.Item.FindControl("rptrChild2"), Repeater)
                        With rpt2
                            .DataSource = dtPart2
                            .DataBind()
                        End With

                        dtPart1.Rows.Clear()
                        dtPart2.Rows.Clear()
                    End If
                Case ListItemType.AlternatingItem
                    Dim hdnID As HiddenField = e.Item.FindControl("hdnID")
                    If hdnID IsNot Nothing Then
                        Dim dtTemp As DataTable = dtCategory.Copy
                        Dim dvCategory As DataView = dtTemp.DefaultView
                        dvCategory.RowFilter = "intSectionID = " & CInt(hdnID.Value)

                        If dvCategory.Count > 0 Then
                            Dim intRow As Integer = 0
                            For Each drRow As DataRowView In dvCategory
                                If intRow < (dvCategory.Count / 2) Then
                                    dtPart1.ImportRow(drRow.Row)
                                Else
                                    dtPart2.ImportRow(drRow.Row)
                                End If

                                dtPart1.AcceptChanges()
                                dtPart2.AcceptChanges()
                                intRow += 1
                            Next
                        End If

                        Dim rpt1 As Repeater = CType(e.Item.FindControl("rptrChild1"), Repeater)
                        With rpt1
                            .DataSource = dtPart1
                            .DataBind()
                        End With

                        Dim rpt2 As Repeater = CType(e.Item.FindControl("rptrChild2"), Repeater)
                        With rpt2
                            .DataSource = dtPart2
                            .DataBind()
                        End With

                        dtPart1.Rows.Clear()
                        dtPart2.Rows.Clear()
                    End If

                Case ListItemType.Header
                    Dim lblMainHeader As Label = CType(e.Item.FindControl("lblMainHeader"), Label)
                    If lblMainHeader IsNot Nothing Then
                        lblMainHeader.Text = "Item Field Mapping"
                    End If

                    Dim divLinking As HtmlGenericControl = CType(e.Item.FindControl("divLinking"), HtmlGenericControl)
                    If Not divLinking Is Nothing AndAlso (ddlImport.SelectedValue = IMPORT_TYPE.Item_Price_Level Or (ddlImport.SelectedValue = IMPORT_TYPE.Item AndAlso (rdbupdate.SelectedValue = "1" Or rdbupdate.SelectedValue = "3")) Or (ddlImport.SelectedValue = IMPORT_TYPE.Organization AndAlso (rdbupdate.SelectedValue = "1" Or rdbupdate.SelectedValue = "3")) Or ddlImport.SelectedValue = IMPORT_TYPE.Contact) Then
                        divLinking.Visible = True

                        If ddlImport.SelectedValue = IMPORT_TYPE.Item Then
                            Dim ddlItemLinking As DropDownList = CType(e.Item.FindControl("ddlItemLinking"), DropDownList)
                            If Not ddlItemLinking Is Nothing Then
                                ddlItemLinking.Visible = True

                                If CCommon.ToInteger(hdnItemLinkingID.Value) > 0 AndAlso Not ddlItemLinking.Items.FindByValue(hdnItemLinkingID.Value) Is Nothing Then
                                    ddlItemLinking.Items.FindByValue(hdnItemLinkingID.Value).Selected = True
                                End If
                            End If
                        ElseIf ddlImport.SelectedValue = IMPORT_TYPE.Organization Then
                            Dim ddlOrganizationLinking As DropDownList = CType(e.Item.FindControl("ddlOrganizationLinking"), DropDownList)
                            If Not ddlOrganizationLinking Is Nothing Then
                                ddlOrganizationLinking.Visible = True

                                If CCommon.ToInteger(hdnItemLinkingID.Value) > 0 AndAlso Not ddlOrganizationLinking.Items.FindByValue(hdnItemLinkingID.Value) Is Nothing Then
                                    ddlOrganizationLinking.Items.FindByValue(hdnItemLinkingID.Value).Selected = True
                                End If
                            End If
                        ElseIf ddlImport.SelectedValue = IMPORT_TYPE.Contact Then
                            Dim ddlContactLinking As DropDownList = CType(e.Item.FindControl("ddlContactLinking"), DropDownList)
                            If Not ddlContactLinking Is Nothing Then
                                ddlContactLinking.Visible = True

                                If rdbupdate.SelectedValue = "1" Then
                                    ddlContactLinking.Items(1).Attributes.Remove("disabled")
                                    ddlContactLinking.Items(2).Attributes.Remove("disabled")
                                    ddlContactLinking.Items(3).Attributes.Add("disabled", "disabled")
                                    ddlContactLinking.Items(4).Attributes.Add("disabled", "disabled")
                                    ddlContactLinking.Items(5).Attributes.Add("disabled", "disabled")
                                    ddlContactLinking.Items(6).Attributes.Add("disabled", "disabled")
                                    ddlContactLinking.Items(7).Attributes.Add("disabled", "disabled")
                                    ddlContactLinking.Items(8).Attributes.Add("disabled", "disabled")
                                    ddlContactLinking.Items(9).Attributes.Add("disabled", "disabled")
                                    ddlContactLinking.SelectedIndex = 0
                                ElseIf rdbupdate.SelectedValue = "2" Then
                                    ddlContactLinking.Items(1).Attributes.Add("disabled", "disabled")
                                    ddlContactLinking.Items(2).Attributes.Add("disabled", "disabled")
                                    ddlContactLinking.Items(3).Attributes.Remove("disabled")
                                    ddlContactLinking.Items(4).Attributes.Remove("disabled")
                                    ddlContactLinking.Items(5).Attributes.Add("disabled", "disabled")
                                    ddlContactLinking.Items(6).Attributes.Add("disabled", "disabled")
                                    ddlContactLinking.Items(7).Attributes.Add("disabled", "disabled")
                                    ddlContactLinking.Items(8).Attributes.Add("disabled", "disabled")
                                    ddlContactLinking.Items(9).Attributes.Remove("disabled")
                                ElseIf rdbupdate.SelectedValue = "3" Then
                                    ddlContactLinking.Items(1).Attributes.Add("disabled", "disabled")
                                    ddlContactLinking.Items(2).Attributes.Add("disabled", "disabled")
                                    ddlContactLinking.Items(3).Attributes.Add("disabled", "disabled")
                                    ddlContactLinking.Items(4).Attributes.Add("disabled", "disabled")
                                    ddlContactLinking.Items(5).Attributes.Remove("disabled")
                                    ddlContactLinking.Items(6).Attributes.Remove("disabled")
                                    ddlContactLinking.Items(7).Attributes.Remove("disabled")
                                    ddlContactLinking.Items(8).Attributes.Remove("disabled")
                                    ddlContactLinking.Items(9).Attributes.Add("disabled", "disabled")
                                End If

                                If CCommon.ToInteger(hdnItemLinkingID.Value) > 0 AndAlso Not ddlContactLinking.Items.FindByValue(hdnItemLinkingID.Value) Is Nothing Then
                                    ddlContactLinking.Items.FindByValue(hdnItemLinkingID.Value).Selected = True
                                End If
                            End If
                        End If


                    End If
            End Select
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Protected Sub rptrChild1_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs)
        Try

            If CType(sender, Repeater).ID.ToString = "rptrChild1" Then

                Dim ddlImportField1 As DropDownList = CType(e.Item.FindControl("ddlImportField1"), DropDownList)
                If ddlImportField1 IsNot Nothing Then

                    If DataBinder.Eval(e.Item.DataItem, "bitRequired") = True Then
                        ddlImportField1.Attributes.Add("class", "{required:true, messages:{required:'Select " & DataBinder.Eval(e.Item.DataItem, "vcFormFieldName") & " import field!'}}")
                    End If

                    _BindImportDropDown(ddlImportField1)

                    If DataBinder.Eval(e.Item.DataItem, "intMapColumnNo") > 0 Then
                        If Not ddlImportField1.Items.FindByValue(DataBinder.Eval(e.Item.DataItem, "intMapColumnNo")) Is Nothing Then
                            ddlImportField1.Items.FindByValue(DataBinder.Eval(e.Item.DataItem, "intMapColumnNo")).Selected = True
                        End If
                    Else
                        Dim isColumnMapped As Boolean = False

                        If dtMap.Rows.Count > 0 Then
                            For Each drRow As DataRow In dtMap.Rows
                                If drRow("ColName") = DataBinder.Eval(e.Item.DataItem, "vcFormFieldName") Then
                                    If Not ddlImportField1.Items.FindByValue(drRow("ID")) Is Nothing Then
                                        ddlImportField1.Items.FindByValue(drRow("ID")).Selected = True
                                        DirectCast(DirectCast(e.Item.FindControl("tblChild1"), Table).FindControl("trChildHeader1"), TableHeaderRow).BackColor = System.Drawing.ColorTranslator.FromHtml("#d5ffd7")
                                        isColumnMapped = True

                                        If listMappedFields Is Nothing Then
                                            listMappedFields = New System.Collections.Generic.List(Of Integer)
                                        End If

                                        listMappedFields.Add(CCommon.ToInteger(drRow("ID")))

                                        Exit For
                                    End If
                                End If
                            Next

                            'If same name column was not found in csv try to look for synonyms
                            If Not isColumnMapped Then
                                Dim objDycFieldMasterSynonym As New DycFieldMasterSynonym
                                objDycFieldMasterSynonym.DomainID = CCommon.ToLong(Session("DomainID"))
                                objDycFieldMasterSynonym.FieldID = CCommon.ToLong(DataBinder.Eval(e.Item.DataItem, "numFormFieldID"))
                                objDycFieldMasterSynonym.IsCustomField = CCommon.ToBool(DataBinder.Eval(e.Item.DataItem, "IsCustomField"))
                                Dim dtSynonym As DataTable = objDycFieldMasterSynonym.GetByFieldID()

                                If Not dtSynonym Is Nothing AndAlso dtSynonym.Rows.Count > 0 Then
                                    For Each drSynonym As DataRow In dtSynonym.AsEnumerable().OrderByDescending(Function(x) CCommon.ToString(x("vcSynonym")).Length)
                                        If dtMap.Select("ColName like '%" & CCommon.ToString(drSynonym("vcSynonym")) & "%'").Length > 0 Then
                                            Dim drRow As DataRow = dtMap.Select("ColName like '%" & CCommon.ToString(drSynonym("vcSynonym")) & "%'")(0)
                                            If Not ddlImportField1.Items.FindByValue(drRow("ID")) Is Nothing Then
                                                ddlImportField1.Items.FindByValue(drRow("ID")).Selected = True
                                                DirectCast(DirectCast(e.Item.FindControl("tblChild1"), Table).FindControl("trChildHeader1"), TableHeaderRow).BackColor = System.Drawing.ColorTranslator.FromHtml("#d5ffd7")
                                                'DirectCast(DirectCast(e.Item.FindControl("tblChild1"), Table).FindControl("trChildHeader1"), TableHeaderRow).BackColor = System.Drawing.ColorTranslator.FromHtml("#fffdcd")
                                                isColumnMapped = True

                                                Exit For
                                            End If
                                        End If
                                    Next
                                End If
                            End If

                            'If not found in synonyms list then compare based on word exist in 
                            If Not isColumnMapped Then
                                Dim tempMatchedWord As Integer = 0
                                Dim machedWord As Integer = 0
                                Dim matchFieldID As Integer = 0

                                For Each drRow As DataRow In dtMap.Rows
                                    tempMatchedWord = HasMatchingWord(CCommon.ToString(drRow("ColName")), CCommon.ToString(DataBinder.Eval(e.Item.DataItem, "vcFormFieldName")))
                                    If tempMatchedWord > 0 Then
                                        If tempMatchedWord > machedWord Then
                                            machedWord = tempMatchedWord
                                            matchFieldID = CCommon.ToInteger(drRow("ID"))
                                        End If
                                    End If
                                Next

                                If matchFieldID > 0 AndAlso Not ddlImportField1.Items.FindByValue(matchFieldID) Is Nothing Then
                                    ddlImportField1.Items.FindByValue(matchFieldID).Selected = True
                                    DirectCast(DirectCast(e.Item.FindControl("tblChild1"), Table).FindControl("trChildHeader1"), TableHeaderRow).BackColor = System.Drawing.ColorTranslator.FromHtml("#fffdcd")
                                    isColumnMapped = True

                                    If listMappedFields Is Nothing Then
                                        listMappedFields = New System.Collections.Generic.List(Of Integer)
                                    End If
                                End If
                            End If
                        End If

                        If Not isColumnMapped Then
                            If Not ddlImportField1.Items.FindByValue(e.Item.ItemIndex + 1) Is Nothing Then
                                ddlImportField1.Items.FindByValue(e.Item.ItemIndex + 1).Selected = True
                                DirectCast(DirectCast(e.Item.FindControl("tblChild1"), Table).FindControl("trChildHeader1"), TableHeaderRow).BackColor = System.Drawing.ColorTranslator.FromHtml("#ffd9d9")
                                isColumnMapped = True
                            End If
                        End If

                        If Not isColumnMapped Then
                            DirectCast(DirectCast(e.Item.FindControl("tblChild1"), Table).FindControl("trChildHeader1"), TableHeaderRow).BackColor = System.Drawing.ColorTranslator.FromHtml("#efefef")
                        End If
                    End If
                End If
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Private Function HasMatchingWord(ByVal left As String, ByVal right As String) As Integer
        Try
            Dim charSeparators() As Char = {" "}
            Dim hashSet As New System.Collections.Generic.HashSet(Of String)(left.ToLower().Split(charSeparators, StringSplitOptions.RemoveEmptyEntries))
            Return right.Split(charSeparators, StringSplitOptions.RemoveEmptyEntries).Count(Function(x) hashSet.Contains(x.ToLower()))
        Catch ex As Exception
            Throw
        End Try
    End Function

    Protected Sub rptrChild2_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs)
        Try

            If CType(sender, Repeater).ID.ToString = "rptrChild2" Then
                Dim rowCount As Integer = DirectCast(DirectCast(DirectCast(DirectCast(CType(sender, Repeater).Parent, System.Web.UI.WebControls.TableCell).Parent, System.Web.UI.WebControls.TableHeaderRow).Parent, System.Web.UI.WebControls.Table).Rows(0).Cells(0).FindControl("rptrChild1"), System.Web.UI.WebControls.Repeater).Items.Count

                Dim ddlImportField2 As DropDownList = CType(e.Item.FindControl("ddlImportField2"), DropDownList)
                If ddlImportField2 IsNot Nothing Then

                    If DataBinder.Eval(e.Item.DataItem, "bitRequired") = True Then
                        ddlImportField2.Attributes.Add("class", "{required:true, messages:{required:'Select " & DataBinder.Eval(e.Item.DataItem, "vcFormFieldName") & " import field!'}}")
                    End If

                    _BindImportDropDown(ddlImportField2)

                    If DataBinder.Eval(e.Item.DataItem, "intMapColumnNo") > 0 Then
                        If Not ddlImportField2.Items.FindByValue(DataBinder.Eval(e.Item.DataItem, "intMapColumnNo")) Is Nothing Then
                            ddlImportField2.Items.FindByValue(DataBinder.Eval(e.Item.DataItem, "intMapColumnNo")).Selected = True

                        End If
                    Else
                        Dim isColumnMapped As Boolean = False

                        If dtMap.Rows.Count > 0 Then
                            For Each drRow As DataRow In dtMap.Rows
                                If drRow("ColName") = DataBinder.Eval(e.Item.DataItem, "vcFormFieldName") Then
                                    If Not ddlImportField2.Items.FindByValue(drRow("ID")) Is Nothing Then
                                        ddlImportField2.Items.FindByValue(drRow("ID")).Selected = True
                                        DirectCast(DirectCast(e.Item.FindControl("tblChild2"), Table).FindControl("trChildHeader2"), TableHeaderRow).BackColor = System.Drawing.ColorTranslator.FromHtml("#d5ffd7")

                                        isColumnMapped = True

                                        If listMappedFields Is Nothing Then
                                            listMappedFields = New System.Collections.Generic.List(Of Integer)
                                        End If

                                        listMappedFields.Add(CCommon.ToInteger(drRow("ID")))

                                        Exit For
                                    End If
                                End If
                            Next

                            'If same name column was not found in csv try to look for synonyms
                            If Not isColumnMapped Then
                                Dim objDycFieldMasterSynonym As New DycFieldMasterSynonym
                                objDycFieldMasterSynonym.DomainID = CCommon.ToLong(Session("DomainID"))
                                objDycFieldMasterSynonym.FieldID = CCommon.ToLong(DataBinder.Eval(e.Item.DataItem, "numFormFieldID"))
                                objDycFieldMasterSynonym.IsCustomField = CCommon.ToBool(DataBinder.Eval(e.Item.DataItem, "IsCustomField"))
                                Dim dtSynonym As DataTable = objDycFieldMasterSynonym.GetByFieldID()

                                If Not dtSynonym Is Nothing AndAlso dtSynonym.Rows.Count > 0 Then
                                    For Each drSynonym As DataRow In dtSynonym.AsEnumerable().OrderByDescending(Function(x) CCommon.ToString(x("vcSynonym")).Length)
                                        If dtMap.Select("ColName like '%" & CCommon.ToString(drSynonym("vcSynonym")) & "%'").Length > 0 Then
                                            Dim drRow As DataRow = dtMap.Select("ColName like '%" & CCommon.ToString(drSynonym("vcSynonym")) & "%'")(0)
                                            If Not ddlImportField2.Items.FindByValue(drRow("ID")) Is Nothing Then
                                                ddlImportField2.Items.FindByValue(drRow("ID")).Selected = True
                                                DirectCast(DirectCast(e.Item.FindControl("tblChild2"), Table).FindControl("trChildHeader2"), TableHeaderRow).BackColor = System.Drawing.ColorTranslator.FromHtml("#d5ffd7")
                                                isColumnMapped = True

                                                Exit For
                                            End If
                                        End If
                                    Next
                                End If
                            End If

                            'If not found in synonyms list then compare based on word exist in 
                            If Not isColumnMapped Then
                                Dim tempMatchedWord As Integer = 0
                                Dim machedWord As Integer = 0
                                Dim matchFieldID As Integer = 0

                                For Each drRow As DataRow In dtMap.Rows
                                    tempMatchedWord = HasMatchingWord(CCommon.ToString(drRow("ColName")), CCommon.ToString(DataBinder.Eval(e.Item.DataItem, "vcFormFieldName")))
                                    If tempMatchedWord > 0 Then
                                        If tempMatchedWord > machedWord Then
                                            machedWord = tempMatchedWord
                                            matchFieldID = CCommon.ToInteger(drRow("ID"))
                                        End If
                                    End If
                                Next

                                If matchFieldID > 0 AndAlso Not ddlImportField2.Items.FindByValue(matchFieldID) Is Nothing Then
                                    ddlImportField2.Items.FindByValue(matchFieldID).Selected = True
                                    DirectCast(DirectCast(e.Item.FindControl("tblChild2"), Table).FindControl("trChildHeader2"), TableHeaderRow).BackColor = System.Drawing.ColorTranslator.FromHtml("#fffdcd")
                                    isColumnMapped = True

                                    If listMappedFields Is Nothing Then
                                        listMappedFields = New System.Collections.Generic.List(Of Integer)
                                    End If
                                End If
                            End If
                        End If

                        If Not isColumnMapped Then
                            If dtMap.Select("ID='" & rowCount + e.Item.ItemIndex + 1 & "'").Length > 0 Then
                                Dim drRow As DataRow = dtMap.Select("ID='" & rowCount + e.Item.ItemIndex + 1 & "'")(0)
                                If Not ddlImportField2.Items.FindByValue(drRow("ID")) Is Nothing Then
                                    ddlImportField2.Items.FindByValue(drRow("ID")).Selected = True
                                    DirectCast(DirectCast(e.Item.FindControl("tblChild2"), Table).FindControl("trChildHeader2"), TableHeaderRow).BackColor = System.Drawing.ColorTranslator.FromHtml("#ffd9d9")
                                    isColumnMapped = True
                                End If
                            End If
                        End If

                        If Not isColumnMapped Then
                            DirectCast(DirectCast(e.Item.FindControl("tblChild2"), Table).FindControl("trChildHeader2"), TableHeaderRow).BackColor = System.Drawing.ColorTranslator.FromHtml("#efefef")
                        End If
                    End If
                End If
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Private Sub gvPreviewData_DataBound(sender As Object, e As EventArgs) Handles gvPreviewData.DataBound
        Try
            Dim strLink As String
            For Each gvRow As GridViewRow In gvPreviewData.Rows
                If gvRow.RowType = DataControlRowType.DataRow AndAlso ddlImport.SelectedValue = IMPORT_TYPE.Item_Images Then
                    strLink = "<a href='" & CCommon.GetDocumentPath(Session("DomainID")) & gvRow.Cells(3).Text & "'target=""_blank"">" & gvRow.Cells(3).Text & "</a>"
                    gvRow.Cells(3).Text = strLink
                    strLink = "<a href='" & CCommon.GetDocumentPath(Session("DomainID")) & gvRow.Cells(4).Text & "'target=""_blank"">" & gvRow.Cells(4).Text & "</a>"
                    gvRow.Cells(4).Text = strLink
                End If
            Next
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

#End Region

#Region "CREATE DATATABLE FROM REPEATER"

    Private Function GetDataTableFromRepeater(ByVal rptName As String, ByVal rptNo As Integer) As DataTable
        Dim dtResult As New DataTable
        Try
            dtResult.Columns.Add("SrNo")
            dtResult.Columns.Add("FormFieldID", GetType(System.Double))
            dtResult.Columns.Add("FormFieldName")
            dtResult.Columns.Add("dbFieldName")
            dtResult.Columns.Add("ImportFieldID")
            dtResult.Columns.Add("ImportFieldName")
            dtResult.Columns.Add("IsCustomField")
            dtResult.AcceptChanges()

            'GET PARENT REPEATER ITEMS
            For Each rptDataItem As RepeaterItem In rptrParent.Items
                Dim rptChild As Repeater

                For intRptCount As Integer = 1 To rptNo
                    'GET CHILD REPEATER
                    rptChild = rptDataItem.FindControl(rptName & intRptCount)

                    'GET CHILD REPEATER ITEMS
                    For Each rptChildItem As RepeaterItem In rptChild.Items
                        'GET CHILD CONTROL FROM REPEATER ITEMS

                        Dim lblSrNo As Label = rptChildItem.FindControl("lblSrNo" & intRptCount)
                        Dim hdnFormFieldId As HiddenField = rptChildItem.FindControl("hdnFormFieldId" & intRptCount)
                        Dim lblFormFieldName As Label = rptChildItem.FindControl("lblFormFieldName" & intRptCount)
                        Dim hdnDbFieldName As HiddenField = rptChildItem.FindControl("hdnDbFieldName" & intRptCount)
                        Dim ddlImportField As DropDownList = rptChildItem.FindControl("ddlImportField" & intRptCount)
                        Dim hdnIsCustomField As HiddenField = rptChildItem.FindControl("hdnIsCustomField" & intRptCount)

                        If ddlImportField.SelectedValue <> 0 Then
                            Dim drRow As DataRow
                            drRow = dtResult.NewRow
                            drRow(0) = lblSrNo.Text
                            drRow(1) = hdnFormFieldId.Value
                            drRow(2) = lblFormFieldName.Text
                            drRow(3) = hdnDbFieldName.Value
                            drRow(4) = ddlImportField.SelectedItem.Value
                            drRow(5) = ddlImportField.SelectedItem.Text
                            drRow(6) = If(hdnIsCustomField.Value = 0, False, True)
                            dtResult.Rows.Add(drRow)
                            dtResult.AcceptChanges()
                        End If
                    Next
                Next
            Next

            'Session("dataTableDestinationObj") = dtResult
        Catch ex As Exception
            Throw ex
        End Try
        Return dtResult
    End Function

#End Region

#Region "SAVE MAPPED DATA"

    Public Sub SaveMappedData(ByVal dtMappedTable As DataTable, Optional ByVal intStatus As Integer = enmImportDataStatus.Import_Pending)
        Try
            Dim intResult As Integer = 0
            objImport = New ImportWizard

            With objImport
                .ImportFileID = IIf(sMode = 0, 0, intImportFileID)
                .ImportFileName = ViewState(_VW_FILE_NAME)
                .MasterID = CInt(ddlImport.SelectedValue)
                .RecordAdded = 0
                .RecordUpdated = 0
                .Errors = 0
                .Duplicates = 0
                .HistoryID = 0
                .CreatedDate = DateTime.Now
                .ImportDate = DateTime.Now
                .DomainId = CCommon.ToLong(Session("DomainID"))
                .UserContactID = CCommon.ToLong(Session("UserContactID"))
                .Status = intStatus
                .hasSendEmail = False
                .MappedData = dtMappedTable
                .History_Added_Value = ""
                .HistoryDateTime = DateTime.Now
                .Mode = sMode
                .ItemLinkingID = CCommon.ToLong(hdnItemLinkingID.Value)
                .InsertUpdateType = rdbupdate.SelectedValue
                .IsSingleOrder = If(ddlNoOfOrders.SelectedValue = "1", True, False)
                .IsExistingOrganization = rbExistingCustomer.Checked
                .DivisionID = If(rbExistingCustomer.Checked, CCommon.ToLong(radCmbCompany.SelectedValue), 0)
                .ContactID = If(rbExistingCustomer.Checked, CCommon.ToLong(ddlContact.SelectedValue), 0)
                .CompanyName = If(rbExistingCustomer.Checked, "", txtNewOrganization.Text.Trim())
                .ContactFirstName = If(rbExistingCustomer.Checked, "", txtFirstName.Text.Trim())
                .ContactLastName = If(rbExistingCustomer.Checked, "", txtLastName.Text.Trim())
                .ContactEmail = If(rbExistingCustomer.Checked, "", txtEmail.Text.Trim())
                .IsMatchOrganizationID = If(ddlNoOfOrders.SelectedValue = "1", False, rbMatchUsingOrganizationID.Checked)
                .MatchFieldID = If(ddlNoOfOrders.SelectedValue = "1", False, If(rbMatchUsingOrganizationID.Checked, 0, CCommon.ToLong(ddlMatchField.SelectedValue)))
                intResult = .SaveImportMappedData()
            End With
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
#End Region

#Region "LOAD MAPPED DATA"

    Private Sub LoadMappedData(ByVal importFileID As Long)
        Try
            Dim dsMappedData As DataSet
            objImport = New ImportWizard

            With objImport
                .ImportFileID = importFileID
                dsMappedData = .LoadMappedData()

                '.ImportFileName = strFileName
                '.MasterID = CInt(ddlImport.SelectedValue)
                '.RecordAdded = 0
                '.RecordUpdated = 0
                '.Errors = 0
                '.Duplicates = 0
                '.HistoryID = 0
                '.CreatedDate = Nothing
                '.ImportDate = Nothing
                '.DomainId = Session("DomainID")
                '.UserContactID = UserCntID
                '.Status = 0
                '.hasSendEmail = False
                '.MappedData = dtMappedTable
                'intResult = .SaveImportMappedData()
            End With
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
#End Region

#Region "CHECK/RENAME FILE AS PER ITS EXISTANCE IN DIRECTORY"

    Private Sub CheckRenameFile(ByRef strFileFullName As String, ByRef strFileName As String)
        Try
            'Check whether specified directory exists or not, If not exists then create it.
            If Not Directory.Exists(CCommon.GetDocumentPhysicalPath(Session("DomainID"))) Then
                Directory.CreateDirectory(CCommon.GetDocumentPhysicalPath(Session("DomainID")))
            End If

            'Check whether specified file exists or not, If exists then rename it as new file.
            Dim objFileInfo As FileInfo
            objFileInfo = New FileInfo(strFileFullName)

            Dim strFileNameOnly As String = objFileInfo.Name.Replace(objFileInfo.Extension, "")
            Dim strFiles() As String = Directory.GetFiles(CCommon.GetDocumentPhysicalPath(Session("DomainID")))
            For intCnt As Integer = 0 To strFiles.Length - 1
                If strFileFullName.ToLower = strFiles(intCnt).ToLower Then
                    strFileFullName = strFileNameOnly & "(" & intCnt & ")" & objFileInfo.Extension
                    strFileName = strFileFullName
                    strFileFullName = CCommon.GetDocumentPhysicalPath(Session("DomainID")) & strFileFullName
                    intCnt = 0
                End If
            Next

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

#End Region

#Region "LINKS TO DOWNLOAD TEMPLATE"

    Private Sub lbDownloadCSV_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbDownloadCSV.Click

        Dim strImportFileName As String = ""
        If ddlImport.SelectedValue = IMPORT_TYPE.Item Then

            lbDownloadCSVWithData.Visible = True
            strImportFileName = "Item"

        ElseIf ddlImport.SelectedValue = IMPORT_TYPE.Item_WareHouse Then

            lbDownloadCSVWithData.Visible = True
            strImportFileName = "Item WareHouse"

            'If ddlItemGroup.SelectedIndex > 0 OrElse chkLotNo.Checked OrElse chkSerializedItem.Checked Then
            lbDownloadCSVWithData_Click(sender, e)
            Exit Sub
            'End If

        ElseIf ddlImport.SelectedValue = IMPORT_TYPE.Item_Images Then

            lbDownloadCSVWithData.Visible = True
            strImportFileName = "Item Images"

        ElseIf ddlImport.SelectedValue = IMPORT_TYPE.Item_Assembly Then

            lbDownloadCSVWithData.Visible = True
            strImportFileName = "Item Assembly"

        ElseIf ddlImport.SelectedValue = IMPORT_TYPE.Item_Vendor Then

            lbDownloadCSVWithData.Visible = True
            strImportFileName = "Item Vendor"

        ElseIf ddlImport.SelectedValue = IMPORT_TYPE.Organization Then

            lbDownloadCSVWithData.Visible = False
            strImportFileName = "Organization"

        ElseIf ddlImport.SelectedValue = IMPORT_TYPE.Organization_Correspondence Then

            lbDownloadCSVWithData.Visible = False
            strImportFileName = "Organization Correspondence"

            'ElseIf ddlImport.SelectedValue = IMPORT_TYPE.Item_Web_Api_Detail Then

            '    lbDownloadCSVWithData.Visible = False
            '    strImportFileName = "Item Web Api Details"

        ElseIf ddlImport.SelectedValue = IMPORT_TYPE.Address_Details Then

            lbDownloadCSVWithData.Visible = True
            strImportFileName = "Address Details"

        ElseIf ddlImport.SelectedValue = IMPORT_TYPE.SalesOrder Or ddlImport.SelectedValue = IMPORT_TYPE.PurchaseOrder Then
            hdnItemLinkingID.Value = rblImportOrderItemIdentifier.SelectedValue
        End If

        Dim dsResult As New DataSet
        Dim dtImportFields As New DataTable

        objImport = New ImportWizard
        objImport.ImportMasterID = ddlImport.SelectedValue
        objImport.Mode = 2
        objImport.ImportFileID = 0
        objImport.DomainId = Session("DomainID")
        objImport.InsertUpdateType = rdbupdate.SelectedValue
        objImport.ItemLinkingID = CCommon.ToLong(hdnItemLinkingID.Value)
        dsResult = objImport.GetImportMasterCategories()

        If dsResult IsNot Nothing AndAlso dsResult.Tables.Count > 2 Then
            dtImportFields = dsResult.Tables(2)
            If dtImportFields.Rows.Count = 0 Then dtImportFields = dsResult.Tables(1)
        Else
            dtImportFields = dsResult.Tables(1)
        End If

        If dtImportFields.Rows.Count = 0 Then
            DisplayError("Please Configure Fields")
            Exit Sub
        End If

        Dim dt As New DataTable
        For Each dr As DataRow In dtImportFields.Rows
            If dt.Columns.Contains(dr("vcFormFieldName").ToString.Replace(",", " ")) = True Then
                Continue For
            End If
            dt.Columns.Add(dr("vcFormFieldName").ToString.Replace(",", " "))
        Next
        Response.Clear()
        Response.Buffer = True
        Response.ContentType = "application/vnd.ms-excel"
        Response.AddHeader("Content-Disposition", "inline;filename=" & strImportFileName.Replace(" ", "_") & "." & Now.Month.ToString & "." & Now.Day.ToString & "." & Now.Year.ToString & ".csv")
        Response.Charset = ""
        Me.EnableViewState = False
        CSVExport.ProduceCSV(dt, Response.Output, True)
        Response.End()

    End Sub

    Private Sub lbDownloadCSVWithData_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbDownloadCSVWithData.Click

        If ddlImport.SelectedValue = 0 Then Exit Sub

        Dim objItems As New CItems
        Dim dt As New DataTable
        Dim DataTable As New DataTable

        Dim objItem As New CItems
        objItem.DomainID = Session("DomainID")
        objItem.ItemGroupID = 0

        If ddlImport.SelectedValue = IMPORT_TYPE.Item Then
            DataTable = objItem.GetItemForUpdateWizard()

        ElseIf ddlImport.SelectedValue = IMPORT_TYPE.Item_Vendor Then
            DataTable = objItem.GetItemVendorForUpdate()

        ElseIf ddlImport.SelectedValue = IMPORT_TYPE.Item_WareHouse Then
            objItem.DomainID = Session("DomainID")
            objItem.ItemGroupID = ddlItemGroup.SelectedValue
            objItem.byteMode = 1

            If chkLotNo.Checked Or chkSerializedItem.Checked Then
                objItem.bitSerialized = chkSerializedItem.Checked
                objItem.bitLotNo = chkLotNo.Checked
                objItem.byteMode = 2
            End If
            DataTable = objItem.GetImportWareHouseItems()

        ElseIf ddlImport.SelectedValue = IMPORT_TYPE.Item_Assembly OrElse ddlImport.SelectedValue = IMPORT_TYPE.Item_Images Then
            objItem.MasterID = ddlImport.SelectedValue
            objItem.DomainID = Session("DomainID")
            DataTable = objItem.GetDataForImport()

            'ElseIf ddlImport.SelectedValue = IMPORT_TYPE.Item_Images Then
            '    objItem.MasterID = ddlImport.SelectedValue
            '    objItem.DomainID = Session("DomainID")
            '    DataTable = objItem.GetDataForImport()

        ElseIf ddlImport.SelectedValue = IMPORT_TYPE.Address_Details Then
            Dim objLeads As New CLeads
            DomainID = Session("DomainID")
            objLeads.DomainID = Session("DomainID")
            objLeads.tintUpdateMode = CCommon.ToInteger(rdbList.SelectedValue)
            DataTable = objLeads.GetAddressDetailsForImport()

        End If

        If DataTable IsNot Nothing Then
            Response.Clear()
            Response.Buffer = True
            Response.ContentType = "application/vnd.ms-excel"
            Response.AddHeader("Content-Disposition", "inline;filename=" & ddlImport.SelectedItem.Text.Replace(" ", "_") & "." & Now.Month.ToString & "." & Now.Day.ToString & "." & Now.Year.ToString & ".csv")
            Response.Charset = ""
            Me.EnableViewState = False
            CSVExport.ProduceCSV(DataTable, Response.Output, True)
            Response.End()
        End If
    End Sub

#End Region

    Protected Sub ddlNoOfOrders_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlNoOfOrders.SelectedIndexChanged
        Try
            If ddlNoOfOrders.SelectedValue = "1" Then 'Single Order
                ddlMatchField.SelectedValue = "0"
                rbMatchUsingField.Checked = True
                divSingleOrder.Visible = True
                divMultipleOrders.Visible = False
            Else
                radCmbCompany.Items.Clear()
                radCmbCompany.Text = ""
                radCmbCompany.SelectedValue = ""
                ddlContact.Items.Clear()
                rbExistingCustomer.Checked = True
                txtNewOrganization.Text = ""
                txtFirstName.Text = ""
                txtLastName.Text = ""
                txtEmail.Text = ""
                divSingleOrder.Visible = False
                divMultipleOrders.Visible = True
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Private Sub radCmbCompany_SelectedIndexChanged(ByVal o As Object, ByVal e As Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs) Handles radCmbCompany.SelectedIndexChanged
        Try
            Dim fillCombo As New BACRM.BusinessLogic.Opportunities.COpportunities
            With fillCombo
                .DivisionID = radCmbCompany.SelectedValue
                ddlContact.DataSource = fillCombo.ListContact().Tables(0).DefaultView()
                ddlContact.DataTextField = "Name"
                ddlContact.DataValueField = "numcontactId"
                ddlContact.DataBind()
            End With

            ddlContact.Items.Insert(0, New ListItem("---Select One---", "0"))

            If ddlContact.Items.Count >= 2 Then
                ddlContact.Items(1).Selected = True
            End If

            ddlContact.Focus()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Protected Sub btnCancelImport_Click(sender As Object, e As EventArgs)
        Try
            ViewState(_VW_FILE_NAME) = Nothing
            ViewState(_VW_MAPPED_DATATABLE) = Nothing
            Response.Redirect("../admin/frmImportList.aspx", False)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Protected Sub btnImport_Click(sender As Object, e As EventArgs)
        Try
            Dim dtMappedData As DataTable
            dtMappedData = DirectCast(ViewState(_VW_MAPPED_DATATABLE), DataTable)

            If sMode = 0 AndAlso dtMappedData.Rows.Count <= 0 Then Exit Sub
            SaveMappedData(dtMappedData)

            ViewState(_VW_FILE_NAME) = Nothing
            ViewState(_VW_MAPPED_DATATABLE) = Nothing

            Response.Redirect("../admin/frmImportList.aspx", False)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Protected Async Sub btnPreImportAnalysis_Click(sender As Object, e As EventArgs)
        Try
            Dim ds As New DataSet

            Dim dtMappedData As DataTable
            dtMappedData = DirectCast(ViewState(_VW_MAPPED_DATATABLE), DataTable)
            dtMappedData.Columns.Remove("SrNo")
            dtMappedData.Columns.Remove("FormFieldName")
            dtMappedData.Columns.Remove("dbFieldName")
            dtMappedData.Columns.Remove("ImportFieldName")
            ds.Tables.Add(dtMappedData)

            Dim DomainID As Long = CCommon.ToLong(Session("DomainID"))
            Dim ImportFileName As String = ViewState(_VW_FILE_NAME)
            Dim MasterID As Integer = CInt(ddlImport.SelectedValue)
            Dim ItemLinkingID As Long = CCommon.ToLong(hdnItemLinkingID.Value)
            Dim InsertUpdateType As Short = CCommon.ToShort(rdbupdate.SelectedValue)
            Dim IsSingleOrder As Boolean = If(ddlNoOfOrders.SelectedValue = "1", True, False)
            Dim IsExistingOrganization As Boolean = rbExistingCustomer.Checked
            Dim DivisionID As Long = If(rbExistingCustomer.Checked, CCommon.ToLong(radCmbCompany.SelectedValue), 0)
            Dim ContactID As Long = If(rbExistingCustomer.Checked, CCommon.ToLong(ddlContact.SelectedValue), 0)
            Dim CompanyName As String = If(rbExistingCustomer.Checked, "", txtNewOrganization.Text.Trim())
            Dim ContactFirstName As String = If(rbExistingCustomer.Checked, "", txtFirstName.Text.Trim())
            Dim ContactLastName As String = If(rbExistingCustomer.Checked, "", txtLastName.Text.Trim())
            Dim ContactEmail As String = If(rbExistingCustomer.Checked, "", txtEmail.Text.Trim())
            Dim IsMatchOrganizationID As Boolean = If(ddlNoOfOrders.SelectedValue = "1", False, rbMatchUsingOrganizationID.Checked)
            Dim MatchFieldID As Long = If(ddlNoOfOrders.SelectedValue = "1", 0, If(rbMatchUsingOrganizationID.Checked, 0, CCommon.ToLong(ddlMatchField.SelectedValue)))

            Dim listError As System.Collections.Generic.List(Of String) = Await PreImportAnalysis(Session("DomainID"),
                                                                                                  ds,
                                                                                                  ImportFileName,
                                                                                                  MasterID,
                                                                                                  ItemLinkingID,
                                                                                                  InsertUpdateType,
                                                                                                  IsSingleOrder,
                                                                                                  IsExistingOrganization,
                                                                                                  DivisionID,
                                                                                                  ContactID,
                                                                                                  CompanyName,
                                                                                                  ContactFirstName,
                                                                                                  ContactLastName,
                                                                                                  ContactEmail,
                                                                                                  IsMatchOrganizationID,
                                                                                                  MatchFieldID)

            If Not listError Is Nothing AndAlso listError.Count > 0 Then
                Dim listEncoded As New System.Collections.Generic.List(Of String)
                Dim li As HtmlGenericControl
                For Each item As String In listError
                    li = New HtmlGenericControl("li")
                    li.InnerHtml = item
                    olError.Controls.Add(li)
                Next

                ManagePanels(5)
            Else
                ManagePanels(6)
            End If
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Async Function PreImportAnalysis(ByVal DomainID As Long,
                                             ByVal ds As DataSet,
                                             ByVal ImportFileName As String,
                                             ByVal MasterID As Integer,
                                             ByVal ItemLinkingID As Long,
                                             ByVal InsertUpdateType As Short,
                                             ByVal IsSingleOrder As Boolean,
                                             ByVal IsExistingOrganization As Boolean,
                                             ByVal DivisionID As Long,
                                             ByVal ContactID As Long,
                                             ByVal CompanyName As String,
                                             ByVal ContactFirstName As String,
                                             ByVal ContactLastName As String,
                                             ByVal ContactEmail As String,
                                             ByVal IsMatchOrganizationID As Boolean,
                                             ByVal MatchFieldID As Long) As Threading.Tasks.Task(Of System.Collections.Generic.List(Of String))
        Try
            Dim listError As New System.Collections.Generic.List(Of String)



            Try

                Dim dataTableHeading As New DataTable
                dataTableHeading.Columns.Add("FormFieldID").SetOrdinal(0)
                dataTableHeading.Columns.Add("FormFieldName").SetOrdinal(1)
                dataTableHeading.Columns.Add("ImportFieldID").SetOrdinal(2)
                dataTableHeading.Columns.Add("dbFieldName").SetOrdinal(3)
                dataTableHeading.Columns.Add("vcAssociatedControlType").SetOrdinal(4)
                dataTableHeading.Columns.Add("bitDefault").SetOrdinal(5)
                dataTableHeading.Columns.Add("vcFieldType").SetOrdinal(6)
                dataTableHeading.Columns.Add("vcListItemType").SetOrdinal(7)
                dataTableHeading.Columns.Add("vcLookBackTableName").SetOrdinal(8)
                dataTableHeading.Columns.Add("vcOrigDBColumnName").SetOrdinal(9)
                dataTableHeading.Columns.Add("vcPropertyName").SetOrdinal(10)
                dataTableHeading.Columns.Add("bitCustomField").SetOrdinal(11)
                dataTableHeading.Columns.Add("IsPrimary").SetOrdinal(12)
                dataTableHeading.Columns.Add("IsErrorDetected").SetOrdinal(13)
                dataTableHeading.Columns.Add("vcFieldDataType").SetOrdinal(14)
                dataTableHeading.Columns.Add("intFieldMaxLength").SetOrdinal(15)

                objImport = New ImportWizard
                objImport.DomainId = Session("DomainID")
                objImport.MasterID = MasterID
                Dim dtFields As DataTable = objImport.GetMappedFieldDetails(ds.GetXml())

                Dim intCnt As Integer
                Dim numRows As Integer = 0

                If Not dtFields Is Nothing AndAlso dtFields.Rows.Count > 0 Then
                    numRows = dtFields.Rows.Count()
                End If

                Dim dataRowHeading As DataRow
                For intCnt = 0 To numRows - 1
                    dataRowHeading = dataTableHeading.NewRow
                    dataRowHeading("FormFieldID") = dtFields.Rows(intCnt).Item("FormFieldID")
                    dataRowHeading("FormFieldName") = dtFields.Rows(intCnt).Item("FormFieldName")
                    dataRowHeading("ImportFieldID") = dtFields.Rows(intCnt).Item("ImportFieldID")
                    dataRowHeading("dbFieldName") = dtFields.Rows(intCnt).Item("dbFieldName")
                    dataRowHeading("vcAssociatedControlType") = dtFields.Rows(intCnt).Item("vcAssociatedControlType")
                    dataRowHeading("bitDefault") = dtFields.Rows(intCnt).Item("bitDefault")
                    dataRowHeading("vcFieldType") = dtFields.Rows(intCnt).Item("vcFieldType")
                    dataRowHeading("vcListItemType") = dtFields.Rows(intCnt).Item("vcListItemType")
                    dataRowHeading("vcLookBackTableName") = dtFields.Rows(intCnt).Item("vcLookBackTableName")
                    dataRowHeading("vcOrigDBColumnName") = dtFields.Rows(intCnt).Item("vcOrigDBColumnName")
                    dataRowHeading("vcPropertyName") = dtFields.Rows(intCnt).Item("vcPropertyName")
                    dataRowHeading("bitCustomField") = dtFields.Rows(intCnt).Item("bitCustomField")
                    dataRowHeading("vcFieldDataType") = CCommon.ToString(dtFields.Rows(intCnt).Item("vcFieldDataType"))
                    dataRowHeading("intFieldMaxLength") = CCommon.ToInteger(dtFields.Rows(intCnt).Item("intFieldMaxLength"))
                    dataRowHeading("IsErrorDetected") = False

                    If (CCommon.ToString(dtFields.Rows(intCnt).Item("dbFieldName")) = "numItemCode" AndAlso MasterID = CCommon.ToInteger(IMPORT_TYPE.Item) AndAlso InsertUpdateType = 1 AndAlso ItemLinkingID = 1) OrElse
                        (CCommon.ToString(dtFields.Rows(intCnt).Item("dbFieldName")) = "vcSKU" AndAlso MasterID = CCommon.ToInteger(IMPORT_TYPE.Item) AndAlso InsertUpdateType = 1 AndAlso ItemLinkingID = 2) OrElse
                        (CCommon.ToString(dtFields.Rows(intCnt).Item("dbFieldName")) = "numBarCodeId" AndAlso MasterID = CCommon.ToInteger(IMPORT_TYPE.Item) AndAlso InsertUpdateType = 1 AndAlso ItemLinkingID = 3) OrElse
                        (CCommon.ToString(dtFields.Rows(intCnt).Item("dbFieldName")) = "vcModelID" AndAlso MasterID = CCommon.ToInteger(IMPORT_TYPE.Item) AndAlso InsertUpdateType = 1 AndAlso ItemLinkingID = 4) OrElse
                        (CCommon.ToString(dtFields.Rows(intCnt).Item("dbFieldName")) = "vcItemName" AndAlso MasterID = CCommon.ToInteger(IMPORT_TYPE.Item) AndAlso InsertUpdateType = 1 AndAlso ItemLinkingID = 5) OrElse
                       (CCommon.ToString(dtFields.Rows(intCnt).Item("dbFieldName")) = "numWareHouseItemID" AndAlso MasterID = CCommon.ToInteger(IMPORT_TYPE.Item_WareHouse)) OrElse
                       (CCommon.ToString(dtFields.Rows(intCnt).Item("dbFieldName")) = "numItemImageId" AndAlso MasterID = CCommon.ToInteger(IMPORT_TYPE.Item_Images)) OrElse
                       (CCommon.ToString(dtFields.Rows(intCnt).Item("dbFieldName")) = "numVendorID" AndAlso MasterID = CCommon.ToInteger(IMPORT_TYPE.Item_Vendor)) OrElse
                       (CCommon.ToString(dtFields.Rows(intCnt).Item("dbFieldName")) = "numItemDetailID" AndAlso MasterID = CCommon.ToInteger(IMPORT_TYPE.Item_Assembly)) OrElse
                       (CCommon.ToString(dtFields.Rows(intCnt).Item("dbFieldName")) = "vcNonBizCompanyID" AndAlso MasterID = CCommon.ToInteger(IMPORT_TYPE.Organization) AndAlso InsertUpdateType = 1 AndAlso ItemLinkingID = 1) OrElse
                       (CCommon.ToString(dtFields.Rows(intCnt).Item("dbFieldName")) = "numDivisionID" AndAlso MasterID = CCommon.ToInteger(IMPORT_TYPE.Organization) AndAlso InsertUpdateType = 1 AndAlso ItemLinkingID = 2) OrElse
                       (CCommon.ToString(dtFields.Rows(intCnt).Item("dbFieldName")) = "vcNonBizContactID" AndAlso MasterID = CCommon.ToInteger(IMPORT_TYPE.Contact) AndAlso InsertUpdateType = 1 AndAlso (ItemLinkingID = 1 Or ItemLinkingID = 5 Or ItemLinkingID = 7)) OrElse
                       (CCommon.ToString(dtFields.Rows(intCnt).Item("dbFieldName")) = "numContactID" AndAlso MasterID = CCommon.ToInteger(IMPORT_TYPE.Contact) AndAlso InsertUpdateType = 1 AndAlso (ItemLinkingID = 2 Or ItemLinkingID = 6 Or ItemLinkingID = 8)) OrElse
                       (CCommon.ToString(dtFields.Rows(intCnt).Item("dbFieldName")) = "numAddressID" AndAlso MasterID = CCommon.ToInteger(IMPORT_TYPE.Address_Details)) OrElse
                       (CCommon.ToString(dtFields.Rows(intCnt).Item("dbFieldName")) = "vcNonBizContactID" AndAlso MasterID = CCommon.ToInteger(IMPORT_TYPE.Organization_Correspondence)) Then
                        dataRowHeading("IsPrimary") = 1
                    Else
                        dataRowHeading("IsPrimary") = 0
                    End If

                    dataTableHeading.Rows.Add(dataRowHeading)
                Next


                Dim dataTableDestination As New DataTable
                dataTableDestination.Columns.Add("SrNo")
                dataTableDestination.Columns("SrNo").AutoIncrement = True
                dataTableDestination.Columns("SrNo").AutoIncrementSeed = 1
                dataTableDestination.Columns("SrNo").AutoIncrementStep = 1

                Dim intColCnt As Integer = 0
                Dim drHead As DataRow
                For Each drHead In dataTableHeading.Rows
                    If dataTableDestination.Columns.Contains(CCommon.ToString(drHead.Item("FormFieldID") & "_" & drHead.Item("dbFieldName"))) = True Then
                        Continue For
                    Else
                        dataTableDestination.Columns.Add(CCommon.ToString(drHead.Item("FormFieldID") & "_" & drHead.Item("dbFieldName")))
                        dataTableDestination.Columns.Add(CCommon.ToString(drHead.Item("FormFieldID") & "_" & drHead.Item("dbFieldName") & "_Actual"))
                    End If
                Next

                Dim arryList() As String
                Dim strdllValue As String = ""
                Dim intArryValue As Integer = 0
                Dim fileLocation As String = CCommon.GetDocumentPhysicalPath(CCommon.ToLong(objImport.DomainId)) & ImportFileName

                If System.IO.File.Exists(fileLocation) Then
                    Dim streamReader As StreamReader
                    Dim intCount As Integer = intCnt

                    streamReader = File.OpenText(fileLocation)
                    dataTableDestination.Clear()

                    Dim csv As CsvReader = New CsvReader(streamReader, True)
                    Dim dataRowDestination As DataRow

                    Try
                        csv.MissingFieldAction = MissingFieldAction.ReplaceByEmpty
                        csv.SupportsMultiline = True

                        For intCntNew As Integer = 0 To dataTableHeading.Rows.Count - 1
                            strdllValue = strdllValue & "," & CCommon.ToString(dataTableHeading.Rows(intCntNew)("ImportFieldID"))
                        Next

                        strdllValue = strdllValue.TrimStart(CChar(","))
                        strdllValue = strdllValue.TrimEnd(CChar(","))
                        arryList = Split(strdllValue, ",")

                        Dim i1000 As Integer = 0
                        Dim rowIndex As Integer = 1
                        While (csv.ReadNextRecord() AndAlso rowIndex <= 100)
                            Dim intEmpty As Integer = 0
                            dataRowDestination = dataTableDestination.NewRow
                            intCount = 0

                            Dim intCustom As Integer = 0
                            For Each drHead In dataTableHeading.Rows
                                intArryValue = CType(CDbl(arryList(intCount)) - 1, Integer)
                                dataRowDestination(DirectCast(drHead.Item("FormFieldID") & "_" & drHead.Item("dbFieldName"), String)) = csv(intArryValue).ToString
                                dataRowDestination(DirectCast(drHead.Item("FormFieldID") & "_" & drHead.Item("dbFieldName") & "_Actual", String)) = csv(intArryValue).ToString
                                intCount += 1
                                If csv(intArryValue).ToString = "" Then
                                    intEmpty += 1
                                End If
                            Next

                            If intEmpty <> arryList.Length Then
                                dataTableDestination.Rows.Add(dataRowDestination)
                            End If
                            i1000 = i1000 + 1

                            rowIndex = rowIndex + 1
                        End While

                        Dim dtDataHold As New DataTable
                        dtDataHold = dataTableDestination.Copy

                        objImport.ImportFileID = intImportFileID
                        objImport.DomainId = Session("DomainID")
                        objImport.MasterID = MasterID
                        objImport.Mode = 2
                        Dim dsDropDownData As DataSet = objImport.GetDropDownData(ds.GetXml())


                        Dim strFormFieldName As String = ""
                        Dim strFormFieldID As Object = Nothing
                        Dim vcFieldtype As Char
                        Dim vcAssociatedControlType As String = ""
                        Dim strValueString As String = ""

                        If dsDropDownData IsNot Nothing AndAlso dsDropDownData.Tables.Count > 0 Then

                            For Each dr As DataRow In dataTableHeading.Rows
                                vcFieldtype = dr("vcFieldType")
                                If vcFieldtype = "R" Then
                                    vcAssociatedControlType = dr("vcAssociatedControlType")
                                    strFormFieldName = CCommon.ToString(dr("dbFieldName"))
                                    strFormFieldID = CCommon.ToLong(dr("FormFieldID"))
                                    Select Case vcAssociatedControlType
                                        Case "SelectBox"
                                            If dtDataHold.Columns.Contains(strFormFieldID & "_" & strFormFieldName) = True Then
                                                For intRow As Integer = 0 To dtDataHold.Rows.Count - 1
                                                    If dsDropDownData.Tables.Count > 1 AndAlso dsDropDownData.Tables(1).Select("vcFieldID='" & strFormFieldID & "_" & strFormFieldName & "'").Length > 0 Then
                                                        Dim drRow() As DataRow

                                                        strValueString = CCommon.ToString(dtDataHold.Rows(intRow)(strFormFieldID & "_" & strFormFieldName)).Trim().ToLower().Replace("'", "''")
                                                        If strFormFieldName = "numGrpID" AndAlso (strValueString.ToLower.Contains("my") = True OrElse
                                                                                               strValueString.ToLower.Contains("myleads") = True OrElse
                                                                                               strValueString.ToLower.Contains("my leads") = True) Then

                                                            strValueString = "My Leads"
                                                        ElseIf strFormFieldName = "numGrpID" AndAlso (strValueString.ToLower.Contains("web") = True OrElse
                                                                                                   strValueString.ToLower.Contains("webleads") = True OrElse
                                                                                                   strValueString.ToLower.Contains("web leads") = True) Then

                                                            strValueString = "WebLeads"
                                                        ElseIf strFormFieldName = "numGrpID" AndAlso (strValueString.ToLower.Contains("public") = True OrElse
                                                                                                   strValueString.ToLower.Contains("publicleads") = True OrElse
                                                                                                   strValueString.ToLower.Contains("public leads") = True) Then

                                                            strValueString = "PublicLeads"
                                                        End If

                                                        If strFormFieldName = "vcExportToAPI" OrElse strFormFieldName = "numCategoryID" Then
                                                            strValueString = SetValues(strValueString, ",", dsDropDownData.Tables(1).Select("vcFieldID='" & strFormFieldID & "_" & strFormFieldName & "'").CopyToDataTable())
                                                            dtDataHold.Rows(intRow)(strFormFieldID & "_" & strFormFieldName) = strValueString
                                                        ElseIf strFormFieldName = "" Or strFormFieldName = "" Or strFormFieldName = "" Or strFormFieldName = "" Then
                                                        Else
                                                            drRow = dsDropDownData.Tables(1).Select("vcFieldID='" & strFormFieldID & "_" & strFormFieldName & "' AND Value = '" & CCommon.ToString(strValueString) & "'")
                                                            If drRow.Length > 0 Then
                                                                dtDataHold.Rows(intRow)(strFormFieldID & "_" & strFormFieldName) = CCommon.ToString(drRow(0)("Key"))
                                                            Else
                                                                If dsDropDownData.Tables(1).Columns.Contains("vcAbbreviations") Then
                                                                    drRow = dsDropDownData.Tables(1).Select("vcFieldID='" & strFormFieldID & "_" & strFormFieldName & "' AND vcAbbreviations = '" & CCommon.ToString(strValueString) & "'")

                                                                    If drRow.Length > 0 Then
                                                                        dtDataHold.Rows(intRow)(strFormFieldID & "_" & strFormFieldName) = CCommon.ToString(drRow(0)("Key"))
                                                                    Else
                                                                        dtDataHold.Rows(intRow)(strFormFieldID & "_" & strFormFieldName) = 0
                                                                    End If
                                                                Else
                                                                    dtDataHold.Rows(intRow)(strFormFieldID & "_" & strFormFieldName) = 0
                                                                End If
                                                            End If
                                                        End If
                                                    End If

                                                    dtDataHold.AcceptChanges()
                                                Next
                                            End If
                                    End Select
                                End If
                            Next
                        End If

                        If dtDataHold IsNot Nothing AndAlso dtDataHold.Rows.Count > 0 Then
                            If MasterID = IMPORT_TYPE.Item Then
                                ValidateImportItems(Session("DomainID"), MasterID, InsertUpdateType, dtDataHold, ItemLinkingID, dataTableHeading, listError)
                            ElseIf MasterID = IMPORT_TYPE.Organization Then
                                ValidateImportOrganizations(Session("DomainID"), MasterID, InsertUpdateType, dtDataHold, ItemLinkingID, dataTableHeading, listError)
                            ElseIf MasterID = IMPORT_TYPE.Contact Then
                                ValidateImportContacts(Session("DomainID"), MasterID, InsertUpdateType, dtDataHold, ItemLinkingID, dataTableHeading, listError)
                            ElseIf MasterID = IMPORT_TYPE.Organization_Correspondence Then
                                ValidateImportOrganizationCorrespondence(Session("DomainID"), MasterID, InsertUpdateType, dtDataHold, dataTableHeading, listError)
                            ElseIf MasterID = IMPORT_TYPE.Item_WareHouse Then
                                ValidateImportItemWarehouse(Session("DomainID"), MasterID, InsertUpdateType, dtDataHold, dataTableHeading, listError)
                            ElseIf MasterID = IMPORT_TYPE.Item_Vendor Then
                                ValidateImportItemVendors(Session("DomainID"), MasterID, InsertUpdateType, dtDataHold, dataTableHeading, listError)
                            ElseIf MasterID = IMPORT_TYPE.Item_Images Then
                                ValidateImportItemImages(Session("DomainID"), MasterID, InsertUpdateType, dtDataHold, dataTableHeading, listError)
                            ElseIf MasterID = IMPORT_TYPE.Item_Assembly Then
                                ValidateImportKitSubItems(Session("DomainID"), MasterID, InsertUpdateType, dtDataHold, dataTableHeading, listError)
                            ElseIf MasterID = IMPORT_TYPE.Address_Details Then
                                ValidateImportAddressDetails(Session("DomainID"), MasterID, InsertUpdateType, dtDataHold, dataTableHeading, listError)
                            ElseIf MasterID = IMPORT_TYPE.SalesOrder Then
                                ValidateImportOrder(Session("DomainID"), dtDataHold, dataTableHeading, 1, ItemLinkingID, IsSingleOrder, IsExistingOrganization, DivisionID, ContactID, CompanyName, ContactFirstName, ContactLastName, IsMatchOrganizationID, MatchFieldID, listError)
                            ElseIf MasterID = IMPORT_TYPE.PurchaseOrder Then
                                ValidateImportOrder(Session("DomainID"), dtDataHold, dataTableHeading, 2, ItemLinkingID, IsSingleOrder, IsExistingOrganization, DivisionID, ContactID, CompanyName, ContactFirstName, ContactLastName, IsMatchOrganizationID, MatchFieldID, listError)
                            ElseIf MasterID = IMPORT_TYPE.JournalEntries Then
                                ValidateImportAccountingEntries(Session("DomainID"), dtDataHold, dataTableHeading, listError)
                            End If
                        Else
                            listError.Add("File is empty. Please upload file with some data.")
                        End If
                    Catch ex As Exception
                        listError.Add("Not able to process csv. File read Errors.")
                    Finally
                        streamReader.Close()
                    End Try
                Else
                    listError.Add("File doesn't exists. Please import this file again.")
                End If
            Catch ex As Exception
                listError.Add(ex.Message)
            End Try

            Return listError
        Catch ex As Exception
            Throw
        End Try
    End Function

    Private Sub ValidateImportItems(ByVal lngDomainID As Long, ByVal MasterID As Long, ByVal tintImportType As Short, ByVal dtDataHold As DataTable, ByVal linkingID As Long, ByVal dataTableHeading As DataTable, ByRef listErrors As System.Collections.Generic.List(Of String))
        Try
            Dim lngCSVRow As Integer = 0

            Dim strGetPrimaryColumnName As String = "ItemCode"
            Dim strFieldName As String = ""
            Dim strFieldType As String = ""
            Dim intFieldMaxLength As Integer = 0
            Dim strFormFieldName As String = ""
            Dim strFormFieldID As Object = Nothing
            Dim vcAssociatedControlType As String = ""
            Dim strValueString As String = ""
            Dim PropertyItem As PropertyInfo = Nothing
            Dim objClass As Object = Nothing
            Dim strPropertyName As String
            Dim objItemSave As MethodInfo = Nothing
            Dim objItemValue As New Object

            If (tintImportType = 2 Or tintImportType = 3) AndAlso Not dtDataHold.Columns.Contains("270_numIncomeChartAcntId") Then
                listErrors.Add("<b>Income Account</b> is required but column does not exists in csv or not mapped.")
            End If
            If (tintImportType = 2 Or tintImportType = 3) AndAlso Not dtDataHold.Columns.Contains("271_numAssetChartAcntId") Then
                listErrors.Add("<b>Asset Account</b> is required but column does not exists in csv or not mapped.")
            End If
            If (tintImportType = 2 Or tintImportType = 3) AndAlso Not dtDataHold.Columns.Contains("272_numCOGsChartAcntId") Then
                listErrors.Add("<b>COGs/Expense Account</b> is required but column does not exists in csv or not mapped.")
            End If
            If (tintImportType = 2 Or tintImportType = 3) AndAlso Not dtDataHold.Columns.Contains("294_charItemType") Then
                listErrors.Add("<b>Item Type</b> is required but column does not exists in csv or not mapped.")
            End If
            If (tintImportType = 2 Or tintImportType = 3) AndAlso Not dtDataHold.Columns.Contains("189_vcItemName") Then
                listErrors.Add("<b>Item Name</b> is required but column does not exists in csv or not mapped.")
            End If

            If (tintImportType = 1 Or tintImportType = 3) AndAlso linkingID = 1 AndAlso Not dtDataHold.Columns.Contains("211_numItemCode") Then
                listErrors.Add("<b>Item ID</b> used as linking id to update item but does not exists or not mapped in csv.")
            ElseIf (tintImportType = 1 Or tintImportType = 3) AndAlso linkingID = 2 AndAlso Not dtDataHold.Columns.Contains("281_vcSKU") Then
                listErrors.Add("<b>SKU</b> used as linking id to update item but does not exists or not mapped in csv.")
            ElseIf (tintImportType = 1 Or tintImportType = 3) AndAlso linkingID = 3 AndAlso Not dtDataHold.Columns.Contains("203_numBarCodeId") Then
                listErrors.Add("<b>UPC</b> used as linking id to update item but does not exists or not mapped in csv.")
            ElseIf (tintImportType = 1 Or tintImportType = 3) AndAlso linkingID = 4 AndAlso Not dtDataHold.Columns.Contains("193_vcModelID") Then
                listErrors.Add("<b>Model ID</b> used as linking id to update item but does not exists or not mapped in csv.")
            ElseIf (tintImportType = 1 Or tintImportType = 3) AndAlso linkingID = 7 AndAlso Not dtDataHold.Columns.Contains("189_vcItemName") Then
                listErrors.Add("<b>Item Name</b> used as linking id to update item but does not exists or not mapped in csv.")
            End If

            If linkingID = 2 Then
                If dtDataHold.Columns.Contains("281_vcSKU") Then dtDataHold.Columns("281_vcSKU").SetOrdinal(0)
            ElseIf linkingID = 3 Then
                If dtDataHold.Columns.Contains("203_numBarCodeId") Then dtDataHold.Columns("203_numBarCodeId").SetOrdinal(0)
            ElseIf linkingID = 4 Then
                If dtDataHold.Columns.Contains("193_vcModelID") Then dtDataHold.Columns("193_vcModelID").SetOrdinal(0)
            ElseIf linkingID = 5 Then
                If dtDataHold.Columns.Contains("237_vcWHSKU") Then dtDataHold.Columns("237_vcWHSKU").SetOrdinal(0)
            ElseIf linkingID = 6 Then
                If dtDataHold.Columns.Contains("236_vcBarCode") Then dtDataHold.Columns("236_vcBarCode").SetOrdinal(0)
            ElseIf linkingID = 7 Then
                If dtDataHold.Columns.Contains("189_vcItemName") Then dtDataHold.Columns("189_vcItemName").SetOrdinal(0)
            Else
                If dtDataHold.Columns.Contains("211_numItemCode") Then dtDataHold.Columns("211_numItemCode").SetOrdinal(0)
            End If

            Dim dtCustomField As DataTable

            For intC As Integer = 0 To dtDataHold.Rows.Count - 1
                lngCSVRow = intC + 2

                Try
                    dtCustomField = New DataTable()
                    dtCustomField.Columns.Add("Fld_ID").SetOrdinal(0)
                    dtCustomField.Columns.Add("Name").SetOrdinal(1)
                    dtCustomField.Columns.Add("Fld_Value").SetOrdinal(2)
                    dtCustomField.TableName = "CusFlds"
                    dtCustomField.AcceptChanges()

                    objClass = New CItems

                    Dim dvTempToSort As DataView
                    dvTempToSort = dataTableHeading.DefaultView
                    dvTempToSort.Sort = "IsPrimary DESC"
                    dataTableHeading = dvTempToSort.ToTable()

                    For intHead As Integer = 0 To dataTableHeading.Rows.Count - 1
                        If dataTableHeading.Rows(intHead)("dbFieldName") Is DBNull.Value OrElse dataTableHeading.Rows(intHead)("dbFieldName") = "" OrElse CCommon.ToBool(dataTableHeading.Rows(intHead)("IsErrorDetected")) Then
                            Continue For
                        End If

                        strFormFieldName = dataTableHeading.Rows(intHead)("dbFieldName")
                        strFormFieldID = dataTableHeading.Rows(intHead)("FormFieldID")
                        strPropertyName = dataTableHeading.Rows(intHead)("vcPropertyName")
                        strFieldName = CCommon.ToString(dataTableHeading.Rows(intHead)("FormFieldName"))
                        strFieldType = CCommon.ToString(dataTableHeading.Rows(intHead)("vcFieldDataType"))
                        intFieldMaxLength = CCommon.ToInteger(dataTableHeading.Rows(intHead)("intFieldMaxLength"))

                        If (strFieldType.ToLower() = "m" Or strFieldType.ToLower() = "n") AndAlso
                            (CCommon.ToString(dataTableHeading.Rows(intHead)("vcAssociatedControlType")).ToLower() = "textbox" Or CCommon.ToString(dataTableHeading.Rows(intHead)("vcAssociatedControlType")).ToLower() = "textarea" Or CCommon.ToString(dataTableHeading.Rows(intHead)("vcAssociatedControlType")).ToLower() = "label") AndAlso
                            Not String.IsNullOrEmpty(CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).Trim()) AndAlso
                            Not Double.TryParse(CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)), Nothing) Then
                            dataTableHeading.Rows(intHead)("IsErrorDetected") = True
                            listErrors.Add("<b>" & strFieldName & "</b> (Column-" & dataTableHeading.Rows(intHead)("ImportFieldID") & ", Row-" & lngCSVRow & ") is mapped to a numeric field, but contains non-numeric characters.")
                            Continue For
                        End If

                        If intFieldMaxLength > 0 AndAlso
                            Not String.IsNullOrEmpty(CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).Trim()) AndAlso
                            CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).Length > intFieldMaxLength Then
                            dataTableHeading.Rows(intHead)("IsErrorDetected") = True
                            listErrors.Add("<b>" & strFieldName & "</b> (Column-" & dataTableHeading.Rows(intHead)("ImportFieldID") & ", Row-" & lngCSVRow & ") contains " & CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).Length & " characters, but the destination field only supports " & intFieldMaxLength & ".")
                            Continue For
                        End If

                        Dim objPropValue As Object = Nothing


                        If CBool(dataTableHeading.Rows(intHead)("bitCustomField")) = True Then
                            Dim drCustom As DataRow
                            drCustom = dtCustomField.NewRow
                            drCustom("Fld_ID") = strFormFieldID
                            drCustom("Name") = strFormFieldName

                            If CCommon.ToString(dataTableHeading.Rows(intHead)("vcAssociatedControlType")).ToLower() = "checkbox" Then
                                If CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "true" OrElse
                                 CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "yes" OrElse
                                 CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "1" OrElse
                                 CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "y" Then
                                    drCustom("Fld_Value") = 1
                                ElseIf CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "false" OrElse
                                    CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "no" OrElse
                                    CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "0" OrElse
                                    CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "n" Then
                                    drCustom("Fld_Value") = 0
                                Else
                                    dataTableHeading.Rows(intHead)("IsErrorDetected") = True
                                    listErrors.Add("<b>" & strFieldName & "</b> (Column-" & dataTableHeading.Rows(intHead)("ImportFieldID") & ", Row-" & lngCSVRow & ") Contains an un-supported value (check boxes must be yes/no, true/false or 1/0).")
                                    Continue For
                                End If
                            ElseIf CCommon.ToString(dataTableHeading.Rows(intHead)("vcAssociatedControlType")).ToLower() = "selectbox" AndAlso CCommon.ToLong(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)) = 0 AndAlso Not String.IsNullOrEmpty(CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName & "_Actual")).Trim()) Then
                                dataTableHeading.Rows(intHead)("IsErrorDetected") = True
                                listErrors.Add("<b>" & strFieldName & "</b> (Column-" & dataTableHeading.Rows(intHead)("ImportFieldID") & ", Row-" & lngCSVRow & ") Contains values that don’t exist on mapped drop-down field.")
                                Continue For
                            Else
                                drCustom("Fld_Value") = dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)
                            End If

                            dtCustomField.Rows.Add(drCustom)
                            dtCustomField.AcceptChanges()
                        Else
                            If tintImportType = 2 AndAlso strFormFieldID = 271 AndAlso CCommon.ToLong(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)) = 0 Then
                                dataTableHeading.Rows(intHead)("IsErrorDetected") = True
                                listErrors.Add("<b>" & strFieldName & "</b> (Column-" & dataTableHeading.Rows(intHead)("ImportFieldID") & ", Row-" & lngCSVRow & ") Chart of accounts does not exists.")
                                Continue For
                            ElseIf tintImportType = 2 AndAlso strFormFieldID = 272 AndAlso CCommon.ToLong(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)) = 0 Then
                                dataTableHeading.Rows(intHead)("IsErrorDetected") = True
                                listErrors.Add("<b>" & strFieldName & "</b> (Column-" & dataTableHeading.Rows(intHead)("ImportFieldID") & ", Row-" & lngCSVRow & ") Chart of accounts does not exists.")
                                Continue For
                            ElseIf tintImportType = 2 AndAlso strFormFieldID = 270 AndAlso CCommon.ToLong(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)) = 0 Then
                                dataTableHeading.Rows(intHead)("IsErrorDetected") = True
                                listErrors.Add("<b>" & strFieldName & "</b> (Column-" & dataTableHeading.Rows(intHead)("ImportFieldID") & ", Row-" & lngCSVRow & ") Chart of accounts does not exists.")
                                Continue For
                            ElseIf tintImportType = 2 AndAlso strFormFieldID = 294 AndAlso (dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName) = "0" Or String.IsNullOrEmpty(CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName & "_Actual")).Trim())) Then
                                dataTableHeading.Rows(intHead)("IsErrorDetected") = True
                                listErrors.Add("<b>" & strFieldName & "</b> (Column-" & dataTableHeading.Rows(intHead)("ImportFieldID") & ", Row-" & lngCSVRow & ") Contains an un-supported value (must be Inventory Item or Non-Inventory Item or Service).")
                                Continue For
                            ElseIf tintImportType = 2 AndAlso strFormFieldID = 189 AndAlso String.IsNullOrEmpty(CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName))) Then
                                dataTableHeading.Rows(intHead)("IsErrorDetected") = True
                                listErrors.Add("<b>" & strFieldName & "</b> (Column-" & dataTableHeading.Rows(intHead)("ImportFieldID") & ", Row-" & lngCSVRow & ") Cell is empty and must be populated because it’s mapped to a required field.")
                                Continue For
                            ElseIf CCommon.ToString(dataTableHeading.Rows(intHead)("vcAssociatedControlType")).ToLower() = "checkbox" Then
                                If CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "true" OrElse
                                 CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "yes" OrElse
                                 CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "1" OrElse
                                 CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "y" Then
                                    objPropValue = CType(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName), Object)
                                ElseIf CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "false" OrElse
                                    CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "no" OrElse
                                    CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "0" OrElse
                                    CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "n" Then
                                    objPropValue = CType(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName), Object)
                                Else
                                    dataTableHeading.Rows(intHead)("IsErrorDetected") = True
                                    listErrors.Add("<b>" & strFieldName & "</b> (Column-" & dataTableHeading.Rows(intHead)("ImportFieldID") & ", Row-" & lngCSVRow & ") Contains an un-supported value (check boxes must be yes/no, true/false or 1/0).")
                                    Continue For
                                End If
                            ElseIf CCommon.ToString(dataTableHeading.Rows(intHead)("vcAssociatedControlType")).ToLower() = "selectbox" AndAlso strFormFieldID <> 294 AndAlso CCommon.ToLong(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)) = 0 AndAlso Not String.IsNullOrEmpty(CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName & "_Actual")).Trim()) Then
                                dataTableHeading.Rows(intHead)("IsErrorDetected") = True
                                listErrors.Add("<b>" & strFieldName & "</b> (Column-" & dataTableHeading.Rows(intHead)("ImportFieldID") & ", Row-" & lngCSVRow & ") Contains values that don’t exist on mapped drop-down field.")
                                Continue For
                            Else
                                objPropValue = CType(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName), Object)
                            End If
                        End If

                        If strFormFieldName = "numItemCode" AndAlso linkingID = 1 Then
                            Try
                                Dim objCom As New CCommon
                                objCom.DomainID = lngDomainID
                                Dim dtLinkingData As DataTable = objCom.GetRecordByLinkingID(MasterID, linkingID, CCommon.ToString(dtDataHold.Rows(intC)("211_numItemCode")))
                                If Not dtLinkingData Is Nothing Then
                                    objClass.ItemCode = CCommon.ToLong(dtLinkingData.Rows(0)("numItemCode"))
                                    objClass.DomainID = lngDomainID
                                    objClass.ClientTimeZoneOffset = 0
                                    objClass.GetItemDetailForImport()
                                End If
                            Catch ex As Exception
                                If tintImportType = 1 Or (tintImportType = 3 AndAlso Not String.IsNullOrEmpty(CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName & "_Actual")).Trim())) Then
                                    dataTableHeading.Rows(intHead)("IsErrorDetected") = True
                                    listErrors.Add("<b>" & strFieldName & "</b> (Column-" & dataTableHeading.Rows(intHead)("ImportFieldID") & ", Row-" & lngCSVRow & ") contains values that don’t exist.")
                                    Continue For
                                End If
                            End Try
                        ElseIf strFormFieldName = "vcSKU" AndAlso linkingID = 2 Then
                            Try
                                Dim objCom As New CCommon
                                objCom.DomainID = lngDomainID
                                Dim dtLinkingData As DataTable = objCom.GetRecordByLinkingID(MasterID, linkingID, CCommon.ToString(dtDataHold.Rows(intC)("281_vcSKU")))
                                If Not dtLinkingData Is Nothing Then
                                    objClass.ItemCode = CCommon.ToLong(dtLinkingData.Rows(0)("numItemCode"))
                                    objClass.DomainID = lngDomainID
                                    objClass.ClientTimeZoneOffset = 0
                                    objClass.GetItemDetailForImport()
                                End If
                            Catch ex As Exception
                                If tintImportType = 1 Or (tintImportType = 3 AndAlso Not String.IsNullOrEmpty(CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName & "_Actual")).Trim())) Then
                                    dataTableHeading.Rows(intHead)("IsErrorDetected") = True
                                    listErrors.Add("<b>" & strFieldName & "</b> (Column-" & dataTableHeading.Rows(intHead)("ImportFieldID") & ", Row-" & lngCSVRow & ") contains values that don’t exist.")
                                    Continue For
                                End If
                            End Try
                        ElseIf strFormFieldName = "numBarCodeId" AndAlso linkingID = 3 Then
                            Try
                                Dim objCom As New CCommon
                                objCom.DomainID = lngDomainID
                                Dim dtLinkingData As DataTable = objCom.GetRecordByLinkingID(MasterID, linkingID, CCommon.ToString(dtDataHold.Rows(intC)("203_numBarCodeId")))
                                If Not dtLinkingData Is Nothing Then
                                    objClass.ItemCode = CCommon.ToLong(dtLinkingData.Rows(0)("numItemCode"))
                                    objClass.DomainID = lngDomainID
                                    objClass.ClientTimeZoneOffset = 0
                                    objClass.GetItemDetailForImport()
                                End If
                            Catch ex As Exception
                                If tintImportType = 1 Or (tintImportType = 3 AndAlso Not String.IsNullOrEmpty(CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName & "_Actual")).Trim())) Then
                                    dataTableHeading.Rows(intHead)("IsErrorDetected") = True
                                    listErrors.Add("<b>" & strFieldName & "</b> (Column-" & dataTableHeading.Rows(intHead)("ImportFieldID") & ", Row-" & lngCSVRow & ") contains values that don’t exist.")
                                    Continue For
                                End If
                            End Try
                        ElseIf strFormFieldName = "vcModelID" AndAlso linkingID = 4 Then
                            Try
                                Dim objCom As New CCommon
                                objCom.DomainID = lngDomainID
                                Dim dtLinkingData As DataTable = objCom.GetRecordByLinkingID(MasterID, linkingID, CCommon.ToString(dtDataHold.Rows(intC)("193_vcModelID")))
                                If Not dtLinkingData Is Nothing Then
                                    objClass.ItemCode = CCommon.ToLong(dtLinkingData.Rows(0)("numItemCode"))
                                    objClass.DomainID = lngDomainID
                                    objClass.ClientTimeZoneOffset = 0
                                    objClass.GetItemDetailForImport()
                                Else
                                    dataTableHeading.Rows(intHead)("IsErrorDetected") = True
                                    listErrors.Add("<b>" & strFieldName & "</b> (Column-" & dataTableHeading.Rows(intHead)("ImportFieldID") & ", Row-" & lngCSVRow & ") contains values that don’t exist.")
                                    Continue For
                                End If
                            Catch ex As Exception
                                If tintImportType = 1 Or (tintImportType = 3 AndAlso Not String.IsNullOrEmpty(CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName & "_Actual")).Trim())) Then
                                    dataTableHeading.Rows(intHead)("IsErrorDetected") = True
                                    listErrors.Add("<b>" & strFieldName & "</b> (Column-" & dataTableHeading.Rows(intHead)("ImportFieldID") & ", Row-" & lngCSVRow & ") contains values that don’t exist.")
                                    Continue For
                                End If
                            End Try
                        ElseIf strFormFieldName = "vcItemName" AndAlso linkingID = 7 Then
                            Try
                                Dim objCom As New CCommon
                                objCom.DomainID = lngDomainID
                                Dim dtLinkingData As DataTable = objCom.GetRecordByLinkingID(MasterID, linkingID, CCommon.ToString(dtDataHold.Rows(intC)("189_vcItemName")))
                                If Not dtLinkingData Is Nothing Then
                                    objClass.ItemCode = CCommon.ToLong(dtLinkingData.Rows(0)("numItemCode"))
                                    objClass.DomainID = lngDomainID
                                    objClass.ClientTimeZoneOffset = 0
                                    objClass.GetItemDetailForImport()
                                Else
                                    dataTableHeading.Rows(intHead)("IsErrorDetected") = True
                                    listErrors.Add("<b>" & strFieldName & "</b> (Column-" & dataTableHeading.Rows(intHead)("ImportFieldID") & ", Row-" & lngCSVRow & ") contains values that don’t exist.")
                                    Continue For
                                End If
                            Catch ex As Exception
                                If tintImportType = 1 Or (tintImportType = 3 AndAlso Not String.IsNullOrEmpty(CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName & "_Actual")).Trim())) Then
                                    dataTableHeading.Rows(intHead)("IsErrorDetected") = True
                                    listErrors.Add("<b>" & strFieldName & "</b> (Column-" & dataTableHeading.Rows(intHead)("ImportFieldID") & ", Row-" & lngCSVRow & ") contains values that don’t exist.")
                                    Continue For
                                End If
                            End Try
                        End If

                        If CCommon.ToString(strPropertyName).Length > 0 Then
                            SetProperty(objClass, strPropertyName, objPropValue)
                        End If
                    Next

                    SetProperty(objClass, "DomainID", lngDomainID)

                    If DirectCast(objClass, BACRM.BusinessLogic.Item.CItems).Assembly = True Then
                        DirectCast(objClass, BACRM.BusinessLogic.Item.CItems).KitParent = False
                    End If

                    If CCommon.ToBool(DirectCast(objClass, BACRM.BusinessLogic.Item.CItems).IsMatrix) Then
                        Dim groupID As Long = CCommon.ToLong(DirectCast(objClass, BACRM.BusinessLogic.Item.CItems).ItemGroupID)
                        If groupID > 0 Then
                            Dim dtItemExistingAttributes As DataTable = Nothing

                            'CHECK IF REQUIRED ATTRIBUTES ARE PROVIDED TO CREATE ITEM LEVEL MATRIX ITEM
                            Dim objItem As New CItems
                            objItem.DomainID = lngDomainID
                            objItem.ItemGroupID = groupID
                            Dim dsAttributes As DataSet = objItem.GetItemGroups()

                            'Get Item Existing Attributes
                            If CCommon.ToLong(DirectCast(objClass, BACRM.BusinessLogic.Item.CItems).ItemCode) > 0 Then
                                objItem.ItemCode = CCommon.ToLong(DirectCast(objClass, BACRM.BusinessLogic.Item.CItems).ItemCode)
                                dtItemExistingAttributes = objItem.GetItemAttributes()
                            End If


                            If Not (Not dsAttributes Is Nothing AndAlso dsAttributes.Tables.Count > 2 AndAlso dsAttributes.Tables(2).Rows.Count > 0) Then
                                If listErrors.Where(Function(x) x.Contains("Item Group is not configured with attributes.")).Count = 0 Then
                                    listErrors.Add("(Row-" & lngCSVRow & ") Item Group is not configured with attributes.")
                                End If
                                Continue For
                            End If
                        Else
                            If listErrors.Where(Function(x) x.Contains("Item group is required for matrix item.")).Count = 0 Then
                                listErrors.Add("(Row-" & lngCSVRow & ") Item group is required for matrix item.")
                            End If
                            Continue For
                        End If
                    End If
                Catch ex As Exception
                    If (Not ex.InnerException Is Nothing AndAlso ex.InnerException.Message.Contains("ITEM_NAME_NOT_SELECTED")) Or ex.Message.Contains("ITEM_NAME_NOT_SELECTED") Then
                        If listErrors.Where(Function(x) x.Contains("Item Name is Empty.")).Count = 0 Then
                            listErrors.Add("(Row-" & lngCSVRow & ") Item Name is Empty.")
                        End If
                    ElseIf (Not ex.InnerException Is Nothing AndAlso ex.InnerException.Message.Contains("ITEM_TYPE_NOT_SELECTED")) Or ex.Message.Contains("ITEM_TYPE_NOT_SELECTED") Then
                        If listErrors.Where(Function(x) x.Contains("Item Type is Empty or Invalid.")).Count = 0 Then
                            listErrors.Add("(Row-" & lngCSVRow & ") Item Type is Empty or Invalid.")
                        End If
                    ElseIf (Not ex.InnerException Is Nothing AndAlso ex.InnerException.Message.Contains("INVALID_COGS_ACCOUNT")) Or ex.Message.Contains("INVALID_COGS_ACCOUNT") Then
                        If listErrors.Where(Function(x) x.Contains("Invalid Cost of Goods Sold Account.")).Count = 0 Then
                            listErrors.Add("(Row-" & lngCSVRow & ") Invalid Cost of Goods Sold Account.")
                        End If
                    ElseIf (Not ex.InnerException Is Nothing AndAlso ex.InnerException.Message.Contains("INVALID_ASSET_ACCOUNT")) Or ex.Message.Contains("INVALID_ASSET_ACCOUNT") Then
                        If listErrors.Where(Function(x) x.Contains("Invalid Asset Account.")).Count = 0 Then
                            listErrors.Add("(Row-" & lngCSVRow & ") Invalid Asset Account.")
                        End If
                    ElseIf (Not ex.InnerException Is Nothing AndAlso ex.InnerException.Message.Contains("INVALID_INCOME_ACCOUNT")) Or ex.Message.Contains("INVALID_INCOME_ACCOUNT") Then
                        If listErrors.Where(Function(x) x.Contains("Invalid Income Account.")).Count = 0 Then
                            listErrors.Add("(Row-" & lngCSVRow & ") Invalid Income Account.")
                        End If
                    ElseIf (Not ex.InnerException Is Nothing AndAlso ex.InnerException.Message.Contains("ITEM_NOT_FOUND_WITH_GIVEN_ITEMCODE")) Or ex.Message.Contains("ITEM_NOT_FOUND_WITH_GIVEN_ITEMCODE") Then
                        If listErrors.Where(Function(x) x.Contains("No item matched to item code for update.")).Count = 0 Then
                            listErrors.Add("(Row-" & lngCSVRow & ") No item matched to item code for update.")
                        End If
                    ElseIf (Not ex.InnerException Is Nothing AndAlso ex.InnerException.Message.Contains("MULTIPLE_ITEMS_FOUND_WITH_GIVEN_ITEMCODE")) Or ex.Message.Contains("MULTIPLE_ITEMS_FOUND_WITH_GIVEN_ITEMCODE") Then
                        If listErrors.Where(Function(x) x.Contains("Multiple items matched to item code for update.")).Count = 0 Then
                            listErrors.Add("(Row-" & lngCSVRow & ") Multiple items matched to item code for update.")
                        End If
                    ElseIf (Not ex.InnerException Is Nothing AndAlso ex.InnerException.Message.Contains("ITEM_NOT_FOUND_WITH_GIVEN_SKU")) Or ex.Message.Contains("ITEM_NOT_FOUND_WITH_GIVEN_SKU") Then
                        If listErrors.Where(Function(x) x.Contains("No item matched to SKU for update.")).Count = 0 Then
                            listErrors.Add("(Row-" & lngCSVRow & ") No item matched to SKU for update.")
                        End If
                    ElseIf (Not ex.InnerException Is Nothing AndAlso ex.InnerException.Message.Contains("MULTIPLE_ITEMS_FOUND_WITH_GIVEN_SKU")) Or ex.Message.Contains("MULTIPLE_ITEMS_FOUND_WITH_GIVEN_SKU") Then
                        If listErrors.Where(Function(x) x.Contains("Multiple items matched to SKU for update.")).Count = 0 Then
                            listErrors.Add("(Row-" & lngCSVRow & ") Multiple items matched to SKU for update.")
                        End If
                    ElseIf (Not ex.InnerException Is Nothing AndAlso ex.InnerException.Message.Contains("ITEM_NOT_FOUND_WITH_GIVEN_UPC")) Or ex.Message.Contains("ITEM_NOT_FOUND_WITH_GIVEN_UPC") Then
                        If listErrors.Where(Function(x) x.Contains("No item matched to UPC for update.")).Count = 0 Then
                            listErrors.Add("(Row-" & lngCSVRow & ") No item matched to UPC for update.")
                        End If
                    ElseIf (Not ex.InnerException Is Nothing AndAlso ex.InnerException.Message.Contains("MULTIPLE_ITEMS_NOT_FOUND_WITH_GIVEN_UPC")) Or ex.Message.Contains("MULTIPLE_ITEMS_NOT_FOUND_WITH_GIVEN_UPC") Then
                        If listErrors.Where(Function(x) x.Contains("Multiple items matched to UPC for update.")).Count = 0 Then
                            listErrors.Add("(Row-" & lngCSVRow & ") Multiple items matched to UPC for update.")
                        End If
                    ElseIf (Not ex.InnerException Is Nothing AndAlso ex.InnerException.Message.Contains("ITEM_NOT_FOUND_WITH_GIVEN_ModelID")) Or ex.Message.Contains("ITEM_NOT_FOUND_WITH_GIVEN_ModelID") Then
                        If listErrors.Where(Function(x) x.Contains("No item matched to Model ID for update.")).Count = 0 Then
                            listErrors.Add("(Row-" & lngCSVRow & ") No item matched to Model ID for update.")
                        End If
                    ElseIf (Not ex.InnerException Is Nothing AndAlso ex.InnerException.Message.Contains("MULTIPLE_ITEMS_NOT_FOUND_WITH_GIVEN_ModelID")) Or ex.Message.Contains("MULTIPLE_ITEMS_NOT_FOUND_WITH_GIVEN_ModelID") Then
                        If listErrors.Where(Function(x) x.Contains("Multiple items matched to Model ID for update.")).Count = 0 Then
                            listErrors.Add("(Row-" & lngCSVRow & ") Multiple items matched to Model ID for update.")
                        End If
                    ElseIf (Not ex.InnerException Is Nothing AndAlso ex.InnerException.Message.Contains("ITEM_NOT_FOUND_WITH_GIVEN_ItemName")) Or ex.Message.Contains("ITEM_NOT_FOUND_WITH_GIVEN_ItemName") Then
                        If listErrors.Where(Function(x) x.Contains("No item matched to Item Name for update.")).Count = 0 Then
                            listErrors.Add("(Row-" & lngCSVRow & ") No item matched to Item Name for update.")
                        End If
                    ElseIf (Not ex.InnerException Is Nothing AndAlso ex.InnerException.Message.Contains("MULTIPLE_ITEMS_FOUND_WITH_GIVEN_ItemName")) Or ex.Message.Contains("MULTIPLE_ITEMS_FOUND_WITH_GIVEN_ItemName") Then
                        If listErrors.Where(Function(x) x.Contains("Multiple items matched to Item Name for update.")).Count = 0 Then
                            listErrors.Add("(Row-" & lngCSVRow & ") Multiple items matched to Item Name for update.")
                        End If
                    ElseIf (Not ex.InnerException Is Nothing AndAlso ex.InnerException.Message.Contains("ITEM_NOT_FOUND")) Or ex.Message.Contains("ITEM_NOT_FOUND") Then
                        If listErrors.Where(Function(x) x.Contains("No item matched to Linking ID.")).Count = 0 Then
                            listErrors.Add("(Row-" & lngCSVRow & ") No item matched to Linking ID.")
                        End If
                    ElseIf (Not ex.InnerException Is Nothing AndAlso ex.InnerException.Message.Contains("INVALID_PRICE_RULE_TYPE")) Or ex.Message.Contains("INVALID_PRICE_RULE_TYPE") Then
                        If listErrors.Where(Function(x) x.Contains("Invalid Price Rule Type.")).Count = 0 Then
                            listErrors.Add("(Row-" & lngCSVRow & ") Invalid Price Rule Type.")
                        End If
                    ElseIf (Not ex.InnerException Is Nothing AndAlso ex.InnerException.Message.Contains("INVALID_PRICE_LEVEL_DISCOUNT_TYPE")) Or ex.Message.Contains("INVALID_PRICE_LEVEL_DISCOUNT_TYPE") Then
                        If listErrors.Where(Function(x) x.Contains("Invalid Price Level Discount Type.")).Count = 0 Then
                            listErrors.Add("(Row-" & lngCSVRow & ") Invalid Price Level Discount Type.")
                        End If
                    Else
                        listErrors.Add("(Row-" & lngCSVRow & ") Unknown error occured in pre import analysis.")
                    End If
                End Try
            Next
        Catch ex As Exception
            listErrors.Add("Unknown error occured - " & ex.Message)
        End Try
    End Sub

    Private Sub ValidateImportOrganizations(ByVal lngDomainID As Long, ByVal MasterID As Long, ByVal tintImportType As Short, ByVal dtDataHold As DataTable, ByVal linkingID As Long, ByVal dataTableHeading As DataTable, ByRef listErrors As System.Collections.Generic.List(Of String))
        Try
            Dim lngCSVRow As Integer = 0
            Dim strGetPrimaryColumnName As String = "DivisionID"
            Dim strFieldName As String = ""
            Dim strFieldType As String = ""
            Dim intFieldMaxLength As Integer = 0
            Dim strFormFieldName As String = ""
            Dim strFormFieldID As Object = Nothing
            Dim vcAssociatedControlType As String = ""
            Dim strValueString As String = ""
            Dim PropertyItem As PropertyInfo = Nothing
            Dim objClass As Object = Nothing
            Dim strPropertyName As String
            Dim objItemSave As MethodInfo = Nothing
            Dim objItemValue As New Object

            If (tintImportType = 2 Or tintImportType = 3) AndAlso Not dtDataHold.Columns.Contains("3_vcCompanyName") Then
                listErrors.Add("<b>Organization</b> is required but column does not exists in csv or not mapped.")
            End If
            If (tintImportType = 2 Or tintImportType = 3) AndAlso Not dtDataHold.Columns.Contains("6_numCompanyType") Then
                listErrors.Add("<b>Relationship</b> is required but column does not exists in csv or not mapped.")
            End If
            If (tintImportType = 2 Or tintImportType = 3) AndAlso Not dtDataHold.Columns.Contains("380_vcNonBizCompanyID") Then
                listErrors.Add("<b>Relationship Type</b> is required but column does not exists in csv or not mapped.")
            End If
            If (tintImportType = 2 Or tintImportType = 3) AndAlso Not dtDataHold.Columns.Contains("451_tintCRMType") Then
                listErrors.Add("<b>Non Biz Company ID</b> is required but column does not exists in csv or not mapped.")
            End If

            If (tintImportType = 1 Or tintImportType = 3) AndAlso linkingID = 1 AndAlso Not dtDataHold.Columns.Contains("380_vcNonBizCompanyID") Then
                listErrors.Add("<b>Non Biz Company ID</b> used as linking id to update organization but does not exists or not mapped in csv.")
            ElseIf (tintImportType = 1 Or tintImportType = 3) AndAlso linkingID = 2 AndAlso Not dtDataHold.Columns.Contains("539_numDivisionID") Then
                listErrors.Add("<b>Organization ID</b> used as linking id to update organization but does not exists or not mapped in csv.")
            End If

            If linkingID = 1 Then
                If dtDataHold.Columns.Contains("380_vcNonBizCompanyID") Then dtDataHold.Columns("380_vcNonBizCompanyID").SetOrdinal(0)
            ElseIf linkingID = 2 Then
                If dtDataHold.Columns.Contains("539_numDivisionID") Then dtDataHold.Columns("539_numDivisionID").SetOrdinal(0)
            End If

            Dim dtCustomField As New DataTable
            dtCustomField.Columns.Add("Fld_ID").SetOrdinal(0)
            dtCustomField.Columns.Add("Name").SetOrdinal(1)
            dtCustomField.Columns.Add("Fld_Value").SetOrdinal(2)
            dtCustomField.TableName = "CusFlds"
            dtCustomField.AcceptChanges()

            For intC As Integer = 0 To dtDataHold.Rows.Count - 1
                lngCSVRow = intC + 2
                Try
                    objClass = New CLeads
                    If linkingID = 1 Then
                        If dtDataHold.Columns.Contains("380_vcNonBizCompanyID") Then dtDataHold.Columns("380_vcNonBizCompanyID").SetOrdinal(0)
                    Else
                        If dtDataHold.Columns.Contains("539_numDivisionID") Then dtDataHold.Columns("539_numDivisionID").SetOrdinal(0)
                    End If

                    Dim dvTempToSort As DataView
                    dvTempToSort = dataTableHeading.DefaultView
                    dvTempToSort.Sort = "IsPrimary DESC"
                    dataTableHeading = dvTempToSort.ToTable()


                    For intHead As Integer = 0 To dataTableHeading.Rows.Count - 1
                        If dataTableHeading.Rows(intHead)("dbFieldName") Is DBNull.Value OrElse dataTableHeading.Rows(intHead)("dbFieldName") = "" OrElse CCommon.ToBool(dataTableHeading.Rows(intHead)("IsErrorDetected")) Then
                            Continue For
                        End If

                        strFormFieldName = dataTableHeading.Rows(intHead)("dbFieldName")
                        strFormFieldID = dataTableHeading.Rows(intHead)("FormFieldID")
                        strPropertyName = dataTableHeading.Rows(intHead)("vcPropertyName")
                        strFieldName = CCommon.ToString(dataTableHeading.Rows(intHead)("FormFieldName"))
                        strFieldType = CCommon.ToString(dataTableHeading.Rows(intHead)("vcFieldDataType"))
                        intFieldMaxLength = CCommon.ToInteger(dataTableHeading.Rows(intHead)("intFieldMaxLength"))

                        If (strFieldType.ToLower() = "m" Or strFieldType.ToLower() = "n") AndAlso
                            (CCommon.ToString(dataTableHeading.Rows(intHead)("vcAssociatedControlType")).ToLower() = "textbox" Or CCommon.ToString(dataTableHeading.Rows(intHead)("vcAssociatedControlType")).ToLower() = "textarea" Or CCommon.ToString(dataTableHeading.Rows(intHead)("vcAssociatedControlType")).ToLower() = "label") AndAlso
                            Not String.IsNullOrEmpty(CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).Trim()) AndAlso
                            Not Double.TryParse(CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)), Nothing) Then
                            dataTableHeading.Rows(intHead)("IsErrorDetected") = True
                            listErrors.Add("<b>" & strFieldName & "</b> (Column-" & dataTableHeading.Rows(intHead)("ImportFieldID") & ", Row-" & lngCSVRow & ") is mapped to a numeric field, but contains non-numeric characters.")
                            Continue For
                        End If

                        If intFieldMaxLength > 0 AndAlso
                            Not String.IsNullOrEmpty(CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).Trim()) AndAlso
                            CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).Length > intFieldMaxLength Then
                            dataTableHeading.Rows(intHead)("IsErrorDetected") = True
                            listErrors.Add("<b>" & strFieldName & "</b> (Column-" & dataTableHeading.Rows(intHead)("ImportFieldID") & ", Row-" & lngCSVRow & ") contains " & CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).Length & " characters, but the destination field only supports " & intFieldMaxLength & ".")
                            Continue For
                        End If

                        Dim objPropValue As Object = Nothing

                        If CBool(dataTableHeading.Rows(intHead)("bitCustomField")) = True Then
                            Dim drCustom As DataRow
                            drCustom = dtCustomField.NewRow
                            drCustom("Fld_ID") = strFormFieldID
                            drCustom("Name") = strFormFieldName

                            If CCommon.ToString(dataTableHeading.Rows(intHead)("vcAssociatedControlType")).ToLower() = "checkbox" Then
                                If CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "true" OrElse
                                 CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "yes" OrElse
                                 CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "1" OrElse
                                 CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "y" Then
                                    drCustom("Fld_Value") = 1
                                ElseIf CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "false" OrElse
                                    CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "no" OrElse
                                    CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "0" OrElse
                                    CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "n" Then
                                    drCustom("Fld_Value") = 0
                                Else
                                    dataTableHeading.Rows(intHead)("IsErrorDetected") = True
                                    listErrors.Add("<b>" & strFieldName & "</b> (Column-" & dataTableHeading.Rows(intHead)("ImportFieldID") & ", Row-" & lngCSVRow & ") Contains an un-supported value (check boxes must be yes/no, true/false or 1/0).")
                                    Continue For
                                End If
                            ElseIf CCommon.ToString(dataTableHeading.Rows(intHead)("vcAssociatedControlType")).ToLower() = "selectbox" AndAlso CCommon.ToLong(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)) = 0 AndAlso Not String.IsNullOrEmpty(CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName & "_Actual")).Trim()) Then
                                dataTableHeading.Rows(intHead)("IsErrorDetected") = True
                                listErrors.Add("<b>" & strFieldName & "</b> (Column-" & dataTableHeading.Rows(intHead)("ImportFieldID") & ", Row-" & lngCSVRow & ") Contains values that don’t exist on mapped drop-down field.")
                                Continue For
                            Else
                                drCustom("Fld_Value") = dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)
                            End If

                            dtCustomField.Rows.Add(drCustom)
                            dtCustomField.AcceptChanges()
                        Else
                            If tintImportType = 2 AndAlso strFormFieldID = 6 AndAlso CCommon.ToLong(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)) = 0 Then
                                dataTableHeading.Rows(intHead)("IsErrorDetected") = True
                                listErrors.Add("<b>" & strFieldName & "</b> (Column-" & dataTableHeading.Rows(intHead)("ImportFieldID") & ", Row-" & lngCSVRow & ") Contains an un-supported value.")
                                Continue For
                            ElseIf tintImportType = 2 AndAlso strFormFieldID = 451 AndAlso CCommon.ToLong(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)) = 0 Then
                                dataTableHeading.Rows(intHead)("IsErrorDetected") = True
                                listErrors.Add("<b>" & strFieldName & "</b> (Column-" & dataTableHeading.Rows(intHead)("ImportFieldID") & ", Row-" & lngCSVRow & ") Contains an un-supported value.")
                                Continue For
                            ElseIf tintImportType = 2 AndAlso strFormFieldID = 380 AndAlso String.IsNullOrEmpty(CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName))) Then
                                dataTableHeading.Rows(intHead)("IsErrorDetected") = True
                                listErrors.Add("<b>" & strFieldName & "</b> (Column-" & dataTableHeading.Rows(intHead)("ImportFieldID") & ", Row-" & lngCSVRow & ") Cell is empty and must be populated because it’s mapped to a required field.")
                                Continue For
                            ElseIf tintImportType = 2 AndAlso strFormFieldID = 3 AndAlso String.IsNullOrEmpty(CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName))) Then
                                dataTableHeading.Rows(intHead)("IsErrorDetected") = True
                                listErrors.Add("<b>" & strFieldName & "</b> (Column-" & dataTableHeading.Rows(intHead)("ImportFieldID") & ", Row-" & lngCSVRow & ") Cell is empty and must be populated because it’s mapped to a required field.")
                                Continue For
                            ElseIf CCommon.ToString(dataTableHeading.Rows(intHead)("vcAssociatedControlType")).ToLower() = "checkbox" Then
                                If CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "true" OrElse
                                 CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "yes" OrElse
                                 CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "1" OrElse
                                 CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "y" Then
                                    objPropValue = CType(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName), Object)
                                ElseIf CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "false" OrElse
                                    CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "no" OrElse
                                    CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "0" OrElse
                                    CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "n" Then
                                    objPropValue = CType(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName), Object)
                                Else
                                    dataTableHeading.Rows(intHead)("IsErrorDetected") = True
                                    listErrors.Add("<b>" & strFieldName & "</b> (Column-" & dataTableHeading.Rows(intHead)("ImportFieldID") & ", Row-" & lngCSVRow & ") Contains an un-supported value (check boxes must be yes/no, true/false or 1/0).")
                                    Continue For
                                End If
                            ElseIf CCommon.ToString(dataTableHeading.Rows(intHead)("vcAssociatedControlType")).ToLower() = "selectbox" AndAlso CCommon.ToLong(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)) = 0 AndAlso Not String.IsNullOrEmpty(CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName & "_Actual")).Trim()) Then
                                dataTableHeading.Rows(intHead)("IsErrorDetected") = True
                                listErrors.Add("<b>" & strFieldName & "</b> (Column-" & dataTableHeading.Rows(intHead)("ImportFieldID") & ", Row-" & lngCSVRow & ") Contains values that don’t exist on mapped drop-down field.")
                                Continue For
                            Else
                                objPropValue = CType(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName), Object)
                            End If
                        End If


                        If strFormFieldName = "vcNonBizCompanyID" AndAlso linkingID = 1 Then
                            Try
                                Dim objCom As New CCommon
                                objCom.DomainID = lngDomainID
                                Dim dtLinkingData As DataTable = objCom.GetRecordByLinkingID(MasterID, linkingID, CCommon.ToString(dtDataHold.Rows(intC)("380_vcNonBizCompanyID")))
                                If Not dtLinkingData Is Nothing Then
                                    objClass.DivisionID = CCommon.ToLong(dtLinkingData.Rows(0)("numDivisionID"))
                                    objClass.DomainID = lngDomainID
                                    objClass.GetOrganizationDetailForImport()
                                End If
                            Catch ex As Exception
                                If tintImportType = 1 Or (tintImportType = 3 AndAlso Not String.IsNullOrEmpty(CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName & "_Actual")).Trim())) Then
                                    dataTableHeading.Rows(intHead)("IsErrorDetected") = True
                                    listErrors.Add("<b>" & strFieldName & "</b> (Column-" & dataTableHeading.Rows(intHead)("ImportFieldID") & ", Row-" & lngCSVRow & ") contains values that don’t exist.")
                                    Continue For
                                End If
                            End Try
                        ElseIf strFormFieldName = "numDivisionID" AndAlso linkingID = 2 Then
                            Try
                                Dim objCom As New CCommon
                                objCom.DomainID = lngDomainID
                                Dim dtLinkingData As DataTable = objCom.GetRecordByLinkingID(MasterID, linkingID, CCommon.ToString(dtDataHold.Rows(intC)("539_numDivisionID")))
                                If Not dtLinkingData Is Nothing Then
                                    objClass.DivisionID = CCommon.ToLong(dtLinkingData.Rows(0)("numDivisionID"))
                                    objClass.DomainID = lngDomainID
                                    objClass.GetOrganizationDetailForImport()
                                End If
                            Catch ex As Exception
                                If tintImportType = 1 Or (tintImportType = 3 AndAlso Not String.IsNullOrEmpty(CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName & "_Actual")).Trim())) Then
                                    dataTableHeading.Rows(intHead)("IsErrorDetected") = True
                                    listErrors.Add("<b>" & strFieldName & "</b> (Column-" & dataTableHeading.Rows(intHead)("ImportFieldID") & ", Row-" & lngCSVRow & ") contains values that don’t exist.")
                                    Continue For
                                End If
                            End Try
                        End If

                        If CCommon.ToString(strPropertyName).Length > 0 Then
                            SetProperty(objClass, strPropertyName, objPropValue)
                        End If
                    Next

                    SetProperty(objClass, "DomainID", lngDomainID)

                    If objClass.DivisionID = 0 Then
                        If Not String.IsNullOrEmpty(objClass.NonBizCompanyID) AndAlso Not String.IsNullOrEmpty(objClass.CompanyName) AndAlso CCommon.ToLong(objClass.CompanyType) > 0 AndAlso CCommon.ToInteger(objClass.CRMType) > 0 Then
                            Dim dtLinkingData As DataTable = Nothing
                            Try
                                Dim objCom As New CCommon
                                objCom.DomainID = lngDomainID
                                dtLinkingData = objCom.GetRecordByLinkingID(MasterID, 1, objClass.NonBizCompanyID)
                            Catch ex As Exception
                                'DO NOT THROW ERROR BECAUSE LOGIC IS JUST FOR CHECKING WHETHER OTHER RECORS DO NOT EXISTS
                            End Try

                            If Not dtLinkingData Is Nothing Then
                                If listErrors.Where(Function(x) x.Contains("Non biz company id is already userd for another organization.")).Count = 0 Then
                                    listErrors.Add("(Row-" & lngCSVRow & ") Non biz company id is already userd for another organization.")
                                End If
                                Continue For
                            End If
                        Else
                            If listErrors.Where(Function(x) x.Contains("Non Biz Company ID, Organization, Relationship and Relationship Type are required.")).Count = 0 Then
                                listErrors.Add("(Row-" & lngCSVRow & ") Non Biz Company ID, Organization, Relationship and Relationship Type are required.")
                            End If
                            Continue For
                        End If
                    End If
                Catch ex As Exception
                    If ex.Message.Contains("REQUIRED_FIELDS_NOT_ADDED") Or (Not ex.InnerException Is Nothing AndAlso ex.InnerException.Message.Contains("REQUIRED_FIELDS_NOT_ADDED")) Then
                        If listErrors.Where(Function(x) x.Contains("Non Biz Company ID, Organization Name, Relationship and Relationship Type can not be blank.")).Count = 0 Then
                            listErrors.Add("(Row-" & lngCSVRow & ") Non Biz Company ID, Organization Name, Relationship and Relationship Type can not be blank.")
                        End If
                    ElseIf ex.Message.Contains("USED_NON_BIZ_COMPANY_ID") Or (Not ex.InnerException Is Nothing AndAlso ex.InnerException.Message.Contains("USED_NON_BIZ_COMPANY_ID")) Then
                        If listErrors.Where(Function(x) x.Contains("Non Biz Company ID already used for other organization.")).Count = 0 Then
                            listErrors.Add("(Row-" & lngCSVRow & ") Non Biz Company ID already used for other organization.")
                        End If
                    ElseIf (Not ex.InnerException Is Nothing AndAlso ex.InnerException.Message.Contains("ORGANIZATION_NOT_FOUND_WITH_GIVEN_NON BIZ COMPANY ID")) Or ex.Message.Contains("ORGANIZATION_NOT_FOUND_WITH_GIVEN_NON BIZ COMPANY ID") Then
                        If listErrors.Where(Function(x) x.Contains("No organization matched to Non Biz Company ID for update.")).Count = 0 Then
                            listErrors.Add("(Row-" & lngCSVRow & ") No organization matched to Non Biz Company ID for update.")
                        End If
                    ElseIf (Not ex.InnerException Is Nothing AndAlso ex.InnerException.Message.Contains("MULTIPLE_ORGANIZATIONS_FOUND_WITH_GIVEN_NON BIZ COMPANY ID")) Or ex.Message.Contains("MULTIPLE_ORGANIZATIONS_FOUND_WITH_GIVEN_NON BIZ COMPANY ID") Then
                        If listErrors.Where(Function(x) x.Contains("Multiple organizations matched to Non Biz Company ID for update.")).Count = 0 Then
                            listErrors.Add("(Row-" & lngCSVRow & ") Multiple organizations matched to Non Biz Company ID for update.")
                        End If
                    ElseIf (Not ex.InnerException Is Nothing AndAlso ex.InnerException.Message.Contains("ORGANIZATION_NOT_FOUND_WITH_GIVEN_ORGANIZATION ID")) Or ex.Message.Contains("ORGANIZATION_NOT_FOUND_WITH_GIVEN_ORGANIZATION ID") Then
                        If listErrors.Where(Function(x) x.Contains("No organization matched to Organization ID for update.")).Count = 0 Then
                            listErrors.Add("(Row-" & lngCSVRow & ") No organization matched to Organization ID for update.")
                        End If
                    ElseIf (Not ex.InnerException Is Nothing AndAlso ex.InnerException.Message.Contains("MULTIPLE_ORGANIZATIONS_FOUND_WITH_GIVEN_ORGANIZATION ID")) Or ex.Message.Contains("MULTIPLE_ORGANIZATIONS_FOUND_WITH_GIVEN_ORGANIZATION ID") Then
                        If listErrors.Where(Function(x) x.Contains("Multiple organizations matched to Organization ID for update.")).Count = 0 Then
                            listErrors.Add("(Row-" & lngCSVRow & ") Multiple organizations matched to Organization ID for update.")
                        End If
                    ElseIf (Not ex.InnerException Is Nothing AndAlso ex.InnerException.Message.Contains("ORGANIZATION_NOT_FOUND")) Or ex.Message.Contains("ORGANIZATION_NOT_FOUND") Then
                        If listErrors.Where(Function(x) x.Contains("No organization matched to Linking ID.")).Count = 0 Then
                            listErrors.Add("(Row-" & lngCSVRow & ") No organization matched to Linking ID.")
                        End If
                    Else
                        listErrors.Add("(Row-" & lngCSVRow & ") Unknown error occured in pre import analysis.")
                    End If
                End Try
            Next
        Catch ex As Exception
            listErrors.Add("Unknown error occured - " & ex.Message)
        End Try
    End Sub

    Private Sub ValidateImportContacts(ByVal lngDomainID As Long, ByVal MasterID As Long, ByVal tintImportType As Short, ByVal dtDataHold As DataTable, ByVal linkingID As Long, ByVal dataTableHeading As DataTable, ByRef listErrors As System.Collections.Generic.List(Of String))
        Try
            Dim lngCSVRow As Integer = 0
            Dim strGetPrimaryColumnName As String = "ContactID"
            Dim strFieldName As String = ""
            Dim strFieldType As String = ""
            Dim intFieldMaxLength As Integer = 0
            Dim strFormFieldName As String = ""
            Dim strFormFieldID As Object = Nothing
            Dim vcAssociatedControlType As String = ""
            Dim strValueString As String = ""
            Dim strImportType As String = "Contact"
            Dim PropertyItem As PropertyInfo = Nothing
            Dim objClass As Object = Nothing
            Dim strPropertyName As String
            Dim objItemSave As MethodInfo = Nothing
            Dim objItemValue As New Object

            If (tintImportType = 2 Or tintImportType = 3) AndAlso Not dtDataHold.Columns.Contains("386_vcNonBizContactID") Then
                listErrors.Add("<b>Non Biz Contact ID Type</b> is required but column does not exists in csv or not mapped.")
            End If
            If (tintImportType = 2 Or tintImportType = 3) AndAlso Not dtDataHold.Columns.Contains("51_vcFirstName") Then
                listErrors.Add("<b>First Name</b> is required but column does not exists in csv or not mapped.")
            End If
            If (tintImportType = 2 Or tintImportType = 3) AndAlso Not dtDataHold.Columns.Contains("52_vcLastName") Then
                listErrors.Add("<b>Last Name</b> is required but column does not exists in csv or not mapped.")
            End If

            If tintImportType = 1 AndAlso linkingID = 1 AndAlso Not dtDataHold.Columns.Contains("386_vcNonBizContactID") Then
                listErrors.Add("<b>Non Biz Contact ID</b> used as linking id to update contact but does not exists or not mapped in csv.")
            ElseIf tintImportType = 1 AndAlso linkingID = 2 AndAlso Not dtDataHold.Columns.Contains("860_numContactID") Then
                listErrors.Add("<b>Contact ID</b> used as linking id to update contact but does not exists or not mapped in csv.")
            ElseIf tintImportType = 2 AndAlso linkingID = 3 AndAlso Not dtDataHold.Columns.Contains("380_vcNonBizCompanyID") Then
                listErrors.Add("<b>Non Biz Company ID</b> used as linking id to pick organization but does not exists or not mapped in csv.")
            ElseIf tintImportType = 2 AndAlso linkingID = 4 AndAlso Not dtDataHold.Columns.Contains("539_numDivisionID") Then
                listErrors.Add("<b>Organization ID</b> used as linking id to pick organization but does not exists or not mapped in csv.")
            ElseIf tintImportType = 3 AndAlso linkingID = 5 AndAlso Not dtDataHold.Columns.Contains("380_vcNonBizCompanyID") AndAlso Not dtDataHold.Columns.Contains("386_vcNonBizContactID") Then
                listErrors.Add("<b>Non Biz Company ID</b> used as linking id to pick organization but does not exists or not mapped in csv.")
                listErrors.Add("<b>Non Biz Contact ID</b> used as linking id to update contact but does not exists or not mapped in csv.")
            ElseIf tintImportType = 3 AndAlso linkingID = 6 AndAlso Not dtDataHold.Columns.Contains("380_vcNonBizCompanyID") AndAlso Not dtDataHold.Columns.Contains("860_numContactID") Then
                listErrors.Add("<b>Non Biz Company ID</b> used as linking id to pick organization but does not exists or not mapped in csv.")
                listErrors.Add("<b>Contact ID</b> used as linking id to update contact but does not exists or not mapped in csv.")
            ElseIf tintImportType = 3 AndAlso linkingID = 7 AndAlso Not dtDataHold.Columns.Contains("539_numDivisionID") AndAlso Not dtDataHold.Columns.Contains("386_vcNonBizContactID") Then
                listErrors.Add("<b>Organization ID</b> used as linking id to pick organization but does not exists or not mapped in csv.")
                listErrors.Add("<b>Non Biz Contact ID</b> used as linking id to update contact but does not exists or not mapped in csv.")
            ElseIf tintImportType = 3 AndAlso linkingID = 8 AndAlso Not dtDataHold.Columns.Contains("539_numDivisionID") AndAlso Not dtDataHold.Columns.Contains("860_numContactID") Then
                listErrors.Add("<b>Organization ID</b> used as linking id to pick organization but does not exists or not mapped in csv.")
                listErrors.Add("<b>Contact ID</b> used as linking id to update contact but does not exists or not mapped in csv.")
            End If

            If tintImportType = 1 AndAlso linkingID = 1 Then
                If dtDataHold.Columns.Contains("386_vcNonBizContactID") Then dtDataHold.Columns("386_vcNonBizContactID").SetOrdinal(0)
            ElseIf tintImportType = 1 AndAlso linkingID = 2 Then
                If dtDataHold.Columns.Contains("860_numContactID") Then dtDataHold.Columns("860_numContactID").SetOrdinal(0)
            ElseIf tintImportType = 2 AndAlso linkingID = 3 Then
                If dtDataHold.Columns.Contains("380_vcNonBizCompanyID") Then dtDataHold.Columns("380_vcNonBizCompanyID").SetOrdinal(0)
            ElseIf tintImportType = 2 AndAlso linkingID = 4 Then
                If dtDataHold.Columns.Contains("539_numDivisionID") Then dtDataHold.Columns("539_numDivisionID").SetOrdinal(0)
            ElseIf tintImportType = 3 AndAlso linkingID = 5 Then
                If dtDataHold.Columns.Contains("380_vcNonBizCompanyID") Then dtDataHold.Columns("380_vcNonBizCompanyID").SetOrdinal(0)
                If dtDataHold.Columns.Contains("386_vcNonBizContactID") Then dtDataHold.Columns("386_vcNonBizContactID").SetOrdinal(1)
            ElseIf tintImportType = 3 AndAlso linkingID = 6 Then
                If dtDataHold.Columns.Contains("380_vcNonBizCompanyID") Then dtDataHold.Columns("380_vcNonBizCompanyID").SetOrdinal(0)
                If dtDataHold.Columns.Contains("860_numContactID") Then dtDataHold.Columns("860_numContactID").SetOrdinal(1)
            ElseIf tintImportType = 3 AndAlso linkingID = 7 Then
                If dtDataHold.Columns.Contains("539_numDivisionID") Then dtDataHold.Columns("539_numDivisionID").SetOrdinal(0)
                If dtDataHold.Columns.Contains("386_vcNonBizContactID") Then dtDataHold.Columns("386_vcNonBizContactID").SetOrdinal(1)
            ElseIf tintImportType = 3 AndAlso linkingID = 8 Then
                If dtDataHold.Columns.Contains("539_numDivisionID") Then dtDataHold.Columns("539_numDivisionID").SetOrdinal(0)
                If dtDataHold.Columns.Contains("860_numContactID") Then dtDataHold.Columns("860_numContactID").SetOrdinal(1)
            End If

            Dim dtCustomField As New DataTable
            dtCustomField.Columns.Add("Fld_ID").SetOrdinal(0)
            dtCustomField.Columns.Add("Name").SetOrdinal(1)
            dtCustomField.Columns.Add("Fld_Value").SetOrdinal(2)
            dtCustomField.TableName = "CusFlds"
            dtCustomField.AcceptChanges()

            For intC As Integer = 0 To dtDataHold.Rows.Count - 1
                Try
                    lngCSVRow = intC + 2

                    objClass = New CContacts

                    If linkingID = 1 Or linkingID = 5 Or linkingID = 7 Then
                        If dtDataHold.Columns.Contains("386_vcNonBizContactID") Then dtDataHold.Columns("386_vcNonBizContactID").SetOrdinal(0)
                    Else
                        If dtDataHold.Columns.Contains("860_numContactID") Then dtDataHold.Columns("860_numContactID").SetOrdinal(0)
                    End If

                    Dim dvTempToSort As DataView
                    dvTempToSort = dataTableHeading.DefaultView
                    dvTempToSort.Sort = "IsPrimary DESC"
                    dataTableHeading = dvTempToSort.ToTable()

                    For intHead As Integer = 0 To dataTableHeading.Rows.Count - 1
                        If dataTableHeading.Rows(intHead)("dbFieldName") Is DBNull.Value OrElse dataTableHeading.Rows(intHead)("dbFieldName") = "" OrElse CCommon.ToBool(dataTableHeading.Rows(intHead)("IsErrorDetected")) Then
                            Continue For
                        End If

                        strFormFieldName = dataTableHeading.Rows(intHead)("dbFieldName")
                        strFormFieldID = dataTableHeading.Rows(intHead)("FormFieldID")
                        strPropertyName = dataTableHeading.Rows(intHead)("vcPropertyName")
                        strFieldName = CCommon.ToString(dataTableHeading.Rows(intHead)("FormFieldName"))
                        strFieldType = CCommon.ToString(dataTableHeading.Rows(intHead)("vcFieldDataType"))
                        intFieldMaxLength = CCommon.ToInteger(dataTableHeading.Rows(intHead)("intFieldMaxLength"))

                        If (strFieldType.ToLower() = "m" Or strFieldType.ToLower() = "n") AndAlso
                            (CCommon.ToString(dataTableHeading.Rows(intHead)("vcAssociatedControlType")).ToLower() = "textbox" Or CCommon.ToString(dataTableHeading.Rows(intHead)("vcAssociatedControlType")).ToLower() = "textarea" Or CCommon.ToString(dataTableHeading.Rows(intHead)("vcAssociatedControlType")).ToLower() = "label") AndAlso
                            Not String.IsNullOrEmpty(CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).Trim()) AndAlso
                            Not Double.TryParse(CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)), Nothing) Then
                            dataTableHeading.Rows(intHead)("IsErrorDetected") = True
                            listErrors.Add("<b>" & strFieldName & "</b> (Column-" & dataTableHeading.Rows(intHead)("ImportFieldID") & ", Row-" & lngCSVRow & ") is mapped to a numeric field, but contains non-numeric characters.")
                            Continue For
                        End If

                        If intFieldMaxLength > 0 AndAlso
                            Not String.IsNullOrEmpty(CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).Trim()) AndAlso
                            CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).Length > intFieldMaxLength Then
                            dataTableHeading.Rows(intHead)("IsErrorDetected") = True
                            listErrors.Add("<b>" & strFieldName & "</b> (Column-" & dataTableHeading.Rows(intHead)("ImportFieldID") & ", Row-" & lngCSVRow & ") contains " & CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).Length & " characters, but the destination field only supports " & intFieldMaxLength & ".")
                            Continue For
                        End If

                        Dim objPropValue As Object = Nothing

                        If CBool(dataTableHeading.Rows(intHead)("bitCustomField")) = True Then
                            Dim drCustom As DataRow
                            drCustom = dtCustomField.NewRow
                            drCustom("Fld_ID") = strFormFieldID
                            drCustom("Name") = strFormFieldName

                            If CCommon.ToString(dataTableHeading.Rows(intHead)("vcAssociatedControlType")).ToLower() = "checkbox" Then
                                If CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "true" OrElse
                                 CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "yes" OrElse
                                 CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "1" OrElse
                                 CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "y" Then
                                    drCustom("Fld_Value") = 1
                                ElseIf CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "false" OrElse
                                    CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "no" OrElse
                                    CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "0" OrElse
                                    CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "n" Then
                                    drCustom("Fld_Value") = 0
                                Else
                                    dataTableHeading.Rows(intHead)("IsErrorDetected") = True
                                    listErrors.Add("<b>" & strFieldName & "</b> (Column-" & dataTableHeading.Rows(intHead)("ImportFieldID") & ", Row-" & lngCSVRow & ") Contains an un-supported value (check boxes must be yes/no, true/false or 1/0).")
                                    Continue For
                                End If
                            ElseIf CCommon.ToString(dataTableHeading.Rows(intHead)("vcAssociatedControlType")).ToLower() = "selectbox" AndAlso CCommon.ToLong(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)) = 0 AndAlso Not String.IsNullOrEmpty(CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName & "_Actual")).Trim()) Then
                                dataTableHeading.Rows(intHead)("IsErrorDetected") = True
                                listErrors.Add("<b>" & strFieldName & "</b> (Column-" & dataTableHeading.Rows(intHead)("ImportFieldID") & ", Row-" & lngCSVRow & ") Contains values that don’t exist on mapped drop-down field.")
                                Continue For
                            Else
                                drCustom("Fld_Value") = dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)
                            End If

                            dtCustomField.Rows.Add(drCustom)
                            dtCustomField.AcceptChanges()
                        Else
                            If tintImportType = 2 AndAlso strFormFieldID = 51 AndAlso String.IsNullOrEmpty(CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName))) Then
                                dataTableHeading.Rows(intHead)("IsErrorDetected") = True
                                listErrors.Add("<b>" & strFieldName & "</b> (Column-" & dataTableHeading.Rows(intHead)("ImportFieldID") & ", Row-" & lngCSVRow & ") Cell is empty and must be populated because it’s mapped to a required field.")
                                Continue For
                            ElseIf tintImportType = 2 AndAlso strFormFieldID = 52 AndAlso String.IsNullOrEmpty(CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName))) Then
                                dataTableHeading.Rows(intHead)("IsErrorDetected") = True
                                listErrors.Add("<b>" & strFieldName & "</b> (Column-" & dataTableHeading.Rows(intHead)("ImportFieldID") & ", Row-" & lngCSVRow & ") Cell is empty and must be populated because it’s mapped to a required field.")
                                Continue For
                            ElseIf tintImportType = 2 AndAlso strFormFieldID = 386 AndAlso String.IsNullOrEmpty(CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName))) Then
                                dataTableHeading.Rows(intHead)("IsErrorDetected") = True
                                listErrors.Add("<b>" & strFieldName & "</b> (Column-" & dataTableHeading.Rows(intHead)("ImportFieldID") & ", Row-" & lngCSVRow & ") Cell is empty and must be populated because it’s mapped to a required field.")
                                Continue For
                            ElseIf CCommon.ToString(dataTableHeading.Rows(intHead)("vcAssociatedControlType")).ToLower() = "checkbox" Then
                                If CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "true" OrElse
                                 CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "yes" OrElse
                                 CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "1" OrElse
                                 CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "y" Then
                                    objPropValue = CType(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName), Object)
                                ElseIf CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "false" OrElse
                                    CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "no" OrElse
                                    CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "0" OrElse
                                    CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "n" Then
                                    objPropValue = CType(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName), Object)
                                Else
                                    dataTableHeading.Rows(intHead)("IsErrorDetected") = True
                                    listErrors.Add("<b>" & strFieldName & "</b> (Column-" & dataTableHeading.Rows(intHead)("ImportFieldID") & ", Row-" & lngCSVRow & ") Contains an un-supported value (check boxes must be yes/no, true/false or 1/0).")
                                    Continue For
                                End If
                            ElseIf CCommon.ToString(dataTableHeading.Rows(intHead)("vcAssociatedControlType")).ToLower() = "selectbox" AndAlso CCommon.ToLong(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)) = 0 AndAlso Not String.IsNullOrEmpty(CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName & "_Actual")).Trim()) Then
                                dataTableHeading.Rows(intHead)("IsErrorDetected") = True
                                listErrors.Add("<b>" & strFieldName & "</b> (Column-" & dataTableHeading.Rows(intHead)("ImportFieldID") & ", Row-" & lngCSVRow & ") Contains values that don’t exist on mapped drop-down field.")
                                Continue For
                            Else
                                objPropValue = CType(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName), Object)
                            End If
                        End If


                        If strFormFieldName = "vcNonBizContactID" AndAlso linkingID = 1 Then
                            Try
                                Dim objCom As New CCommon
                                objCom.DomainID = lngDomainID
                                Dim dtLinkingData As DataTable = objCom.GetRecordByLinkingID(MasterID, linkingID, CCommon.ToString(dtDataHold.Rows(intC)("386_vcNonBizContactID")))
                                If Not dtLinkingData Is Nothing Then
                                    objClass.ContactID = CCommon.ToLong(dtLinkingData.Rows(0)("numContactID"))
                                    objClass.DomainID = lngDomainID
                                    objClass.ClientTimeZoneOffset = 0 'Session("ClientMachineUTCTimeOffset")
                                    objClass.GetContactDetailForImport()
                                End If
                            Catch ex As Exception
                                If tintImportType = 1 Or (tintImportType = 3 AndAlso Not String.IsNullOrEmpty(CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName & "_Actual")).Trim())) Then
                                    dataTableHeading.Rows(intHead)("IsErrorDetected") = True
                                    listErrors.Add("<b>" & strFieldName & "</b> (Column-" & dataTableHeading.Rows(intHead)("ImportFieldID") & ", Row-" & lngCSVRow & ") contains values that don’t exist.")
                                    Continue For
                                End If
                            End Try
                        ElseIf strFormFieldName = "vcNonBizContactID" AndAlso (linkingID = 5 Or linkingID = 7) Then
                            Try
                                Dim objCom As New CCommon
                                objCom.DomainID = lngDomainID
                                Dim dtLinkingData As DataTable = objCom.GetRecordByLinkingID(MasterID, 1, CCommon.ToString(dtDataHold.Rows(intC)("386_vcNonBizContactID")))
                                If Not dtLinkingData Is Nothing Then
                                    objClass.ContactID = CCommon.ToLong(dtLinkingData.Rows(0)("numContactID"))
                                    objClass.DomainID = lngDomainID
                                    objClass.ClientTimeZoneOffset = 0 'Session("ClientMachineUTCTimeOffset")
                                    objClass.GetContactDetailForImport()
                                End If
                            Catch ex As Exception
                                'DO NOT THROW ERROR BECAUSE linking ID 5 and 7 are for INSERT & UPDATE
                            End Try
                        ElseIf strFormFieldName = "numContactID" AndAlso linkingID = 2 Then
                            Try
                                Dim objCom As New CCommon
                                objCom.DomainID = lngDomainID
                                Dim dtLinkingData As DataTable = objCom.GetRecordByLinkingID(MasterID, linkingID, CCommon.ToString(dtDataHold.Rows(intC)("860_numContactID")))
                                If Not dtLinkingData Is Nothing Then
                                    objClass.ContactID = CCommon.ToLong(dtLinkingData.Rows(0)("numContactID"))
                                    objClass.DomainID = lngDomainID
                                    objClass.ClientTimeZoneOffset = 0 'Session("ClientMachineUTCTimeOffset")
                                    objClass.GetContactDetailForImport()
                                End If
                            Catch ex As Exception
                                If tintImportType = 1 Or (tintImportType = 3 AndAlso Not String.IsNullOrEmpty(CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName & "_Actual")).Trim())) Then
                                    dataTableHeading.Rows(intHead)("IsErrorDetected") = True
                                    listErrors.Add("<b>" & strFieldName & "</b> (Column-" & dataTableHeading.Rows(intHead)("ImportFieldID") & ", Row-" & lngCSVRow & ") contains values that don’t exist.")
                                    Continue For
                                End If
                            End Try
                        ElseIf strFormFieldName = "numContactID" AndAlso (linkingID = 6 Or linkingID = 8) Then
                            Try
                                Dim objCom As New CCommon
                                objCom.DomainID = lngDomainID
                                Dim dtLinkingData As DataTable = objCom.GetRecordByLinkingID(MasterID, 2, CCommon.ToString(dtDataHold.Rows(intC)("860_numContactID")))
                                If Not dtLinkingData Is Nothing Then
                                    objClass.ContactID = CCommon.ToLong(dtLinkingData.Rows(0)("numContactID"))
                                    objClass.DomainID = lngDomainID
                                    objClass.ClientTimeZoneOffset = 0 'Session("ClientMachineUTCTimeOffset")
                                    objClass.GetContactDetailForImport()
                                End If
                            Catch ex As Exception
                                'DO NOT THROW ERROR BECAUSE linking ID 6 and 8 are for INSERT & UPDATE
                            End Try
                        End If


                        If strFormFieldName = "vcNonBizCompanyID" AndAlso linkingID = 3 Then
                            Try
                                Dim objCom As New CCommon
                                objCom.DomainID = lngDomainID
                                Dim dtLinkingData As DataTable = objCom.GetRecordByLinkingID(133, 1, CCommon.ToString(dtDataHold.Rows(intC)("380_vcNonBizCompanyID")))
                                If Not dtLinkingData Is Nothing Then
                                    objClass.DivisionID = CCommon.ToLong(dtLinkingData.Rows(0)("numDivisionID"))
                                    objClass.CompanyID = CCommon.ToLong(dtLinkingData.Rows(0)("numCompanyID"))
                                End If
                                SetProperty(objClass, strPropertyName, objPropValue)
                            Catch ex As Exception
                                dataTableHeading.Rows(intHead)("IsErrorDetected") = True
                                listErrors.Add("<b>" & strFieldName & "</b> (Column-" & dataTableHeading.Rows(intHead)("ImportFieldID") & ", Row-" & lngCSVRow & ") contains values that don’t exist.")
                                Continue For
                            End Try
                        ElseIf strFormFieldName = "vcNonBizCompanyID" AndAlso (linkingID = 5 Or linkingID = 6) AndAlso objClass.DivisionID = 0 Then
                            Try
                                Dim objCom As New CCommon
                                objCom.DomainID = lngDomainID
                                Dim dtLinkingData As DataTable = objCom.GetRecordByLinkingID(133, 1, CCommon.ToString(dtDataHold.Rows(intC)("380_vcNonBizCompanyID")))
                                If Not dtLinkingData Is Nothing Then
                                    objClass.DivisionID = CCommon.ToLong(dtLinkingData.Rows(0)("numDivisionID"))
                                    objClass.CompanyID = CCommon.ToLong(dtLinkingData.Rows(0)("numCompanyID"))
                                End If
                            Catch ex As Exception
                                'DO NOT THROW ERROR BECAUSE linking ID 6 and 8 are for INSERT & UPDATE
                            End Try
                            SetProperty(objClass, strPropertyName, objPropValue)
                        ElseIf strFormFieldName = "numDivisionID" AndAlso linkingID = 4 Then
                            Try
                                Dim objCom As New CCommon
                                objCom.DomainID = lngDomainID
                                Dim dtLinkingData As DataTable = objCom.GetRecordByLinkingID(133, 2, CCommon.ToString(dtDataHold.Rows(intC)("539_numDivisionID")))
                                If Not dtLinkingData Is Nothing Then
                                    objClass.DivisionID = CCommon.ToLong(dtLinkingData.Rows(0)("numDivisionID"))
                                    objClass.CompanyID = CCommon.ToLong(dtLinkingData.Rows(0)("numCompanyID"))
                                End If
                                SetProperty(objClass, strPropertyName, objPropValue)
                            Catch ex As Exception
                                dataTableHeading.Rows(intHead)("IsErrorDetected") = True
                                listErrors.Add("<b>" & strFieldName & "</b> (Column-" & dataTableHeading.Rows(intHead)("ImportFieldID") & ", Row-" & lngCSVRow & ") contains values that don’t exist.")
                                Continue For
                            End Try
                        ElseIf strFormFieldName = "numDivisionID" AndAlso (linkingID = 7 Or linkingID = 8) AndAlso objClass.DivisionID = 0 Then
                            Try
                                Dim objCom As New CCommon
                                objCom.DomainID = lngDomainID
                                Dim dtLinkingData As DataTable = objCom.GetRecordByLinkingID(133, 2, CCommon.ToString(dtDataHold.Rows(intC)("539_numDivisionID")))
                                If Not dtLinkingData Is Nothing Then
                                    objClass.DivisionID = CCommon.ToLong(dtLinkingData.Rows(0)("numDivisionID"))
                                    objClass.CompanyID = CCommon.ToLong(dtLinkingData.Rows(0)("numCompanyID"))
                                End If
                            Catch ex As Exception
                                'DO NOT THROW ERROR BECAUSE linking ID 6 and 8 are for INSERT & UPDATE
                            End Try
                            SetProperty(objClass, strPropertyName, objPropValue)
                        ElseIf CCommon.ToString(strPropertyName).Length > 0 Then
                            SetProperty(objClass, strPropertyName, objPropValue)
                        End If
                    Next

                    SetProperty(objClass, "DomainID", lngDomainID)

                    If tintImportType = 1 AndAlso CCommon.ToLong(DirectCast(objClass, BACRM.BusinessLogic.Contacts.CContacts).ContactID) = 0 Then 'Update
                        Throw New Exception("CONTACT_NOT_FOUND")
                    End If

                    If objClass.ContactID = 0 Then
                        If Not String.IsNullOrEmpty(objClass.NonBizContactID) AndAlso Not String.IsNullOrEmpty(objClass.FirstName) AndAlso Not String.IsNullOrEmpty(objClass.LastName) Then
                            Dim dtLinkingData As DataTable = Nothing
                            Try
                                Dim objCom As New CCommon
                                objCom.DomainID = lngDomainID
                                dtLinkingData = objCom.GetRecordByLinkingID(MasterID, 1, objClass.NonBizContactID)
                            Catch ex As Exception
                                'DO NOT THROW ERROR BECAUSE IT IS JUST FOR CHECKING WHETHER RECORD EXISTS ARE NOT WITH THAT LINKING ID
                            End Try

                            If Not dtLinkingData Is Nothing Then
                                If listErrors.Where(Function(x) x.Contains("Non biz contact id is already userd for another contact.")).Count = 0 Then
                                    listErrors.Add("(Row-" & lngCSVRow & ") Non biz contact id is already userd for another contact.")
                                End If
                                Continue For
                            End If
                        Else
                            If listErrors.Where(Function(x) x.Contains("Non Biz Contact ID, First Name and Last Name are required.")).Count = 0 Then
                                listErrors.Add("(Row-" & lngCSVRow & ") Non Biz Contact ID, First Name and Last Name are required.")
                            End If
                            Continue For
                        End If
                    End If
                Catch ex As Exception
                    If ex.Message.Contains("INVALID_LINKING_ID") Or (Not ex.InnerException Is Nothing AndAlso ex.InnerException.Message.Contains("INVALID_LINKING_ID")) Then
                        If listErrors.Where(Function(x) x.Contains("Invalid or missing Non Biz Company ID / Organization ID for adding new contact.")).Count = 0 Then
                            listErrors.Add("(Row-" & lngCSVRow & ") Invalid or missing Non Biz Company ID / Organization ID for adding new contact.")
                        End If
                    ElseIf ex.Message.Contains("REQUIRED_FIELD_MISSING_VALUES") Or (Not ex.InnerException Is Nothing AndAlso ex.InnerException.Message.Contains("REQUIRED_FIELD_MISSING_VALUES")) Then
                        If listErrors.Where(Function(x) x.Contains("Non Biz Contact ID, Contact First Name and Contact Last Name are required for adding new contact.")).Count = 0 Then
                            listErrors.Add("(Row-" & lngCSVRow & ") Non Biz Contact ID, Contact First Name and Contact Last Name are required for adding new contact.")
                        End If
                    ElseIf (Not ex.InnerException Is Nothing AndAlso ex.InnerException.Message.Contains("CONTACT_NOT_FOUND_WITH_GIVEN_NON BIZ CONTACT ID")) Or ex.Message.Contains("CONTACT_NOT_FOUND_WITH_GIVEN_NON BIZ CONTACT ID") Then
                        If listErrors.Where(Function(x) x.Contains("No contact matched to Non Biz Contact ID for update.")).Count = 0 Then
                            listErrors.Add("(Row-" & lngCSVRow & ") No contact matched to Non Biz Contact ID for update.")
                        End If
                    ElseIf (Not ex.InnerException Is Nothing AndAlso ex.InnerException.Message.Contains("MULTIPLE_CONTACTS_FOUND_WITH_GIVEN_NON BIZ CONTACT ID")) Or ex.Message.Contains("MULTIPLE_CONTACTS_FOUND_WITH_GIVEN_NON BIZ CONTACT ID") Then
                        If listErrors.Where(Function(x) x.Contains("Multiple contacts matched to Non Biz Contact ID for update.")).Count = 0 Then
                            listErrors.Add("(Row-" & lngCSVRow & ") Multiple contacts matched to Non Biz Contact ID for update.")
                        End If
                    ElseIf (Not ex.InnerException Is Nothing AndAlso ex.InnerException.Message.Contains("CONTACT_NOT_FOUND_WITH_GIVEN_CONTACT ID")) Or ex.Message.Contains("CONTACT_NOT_FOUND_WITH_GIVEN_CONTACT ID") Then
                        If listErrors.Where(Function(x) x.Contains("No contact matched to Contact ID for update.")).Count = 0 Then
                            listErrors.Add("(Row-" & lngCSVRow & ") No contact matched to Contact ID for update.")
                        End If
                    ElseIf (Not ex.InnerException Is Nothing AndAlso ex.InnerException.Message.Contains("MULTIPLE_CONTACTS_FOUND_WITH_GIVEN_CONTACT ID")) Or ex.Message.Contains("MULTIPLE_CONTACTS_FOUND_WITH_GIVEN_CONTACT ID") Then
                        If listErrors.Where(Function(x) x.Contains("Multiple concats matched to Contact ID for update.")).Count = 0 Then
                            listErrors.Add("(Row-" & lngCSVRow & ") Multiple concats matched to Contact ID for update.")
                        End If
                    ElseIf (Not ex.InnerException Is Nothing AndAlso ex.InnerException.Message.Contains("CONTACT_NOT_FOUND")) Or ex.Message.Contains("CONTACT_NOT_FOUND") Then
                        If listErrors.Where(Function(x) x.Contains("No contact matched to Linking ID.")).Count = 0 Then
                            listErrors.Add("(Row-" & lngCSVRow & ") No contact matched to Linking ID.")
                        End If
                    ElseIf ex.Message.Contains("USED_NON_BIZ_CONTACT_ID") Or (Not ex.InnerException Is Nothing AndAlso ex.InnerException.Message.Contains("USED_NON_BIZ_CONTACT_ID")) Then
                        If listErrors.Where(Function(x) x.Contains("Non Biz Contact ID already used for other contact.")).Count = 0 Then
                            listErrors.Add("(Row-" & lngCSVRow & ") Non Biz Contact ID already used for other contact.")
                        End If
                    ElseIf (Not ex.InnerException Is Nothing AndAlso ex.InnerException.Message.Contains("ORGANIZATION_NOT_FOUND_WITH_GIVEN_NON BIZ COMPANY ID")) Or ex.Message.Contains("ORGANIZATION_NOT_FOUND_WITH_GIVEN_NON BIZ COMPANY ID") Then
                        If listErrors.Where(Function(x) x.Contains("No organization matched to Non Biz Company ID for adding new contact.")).Count = 0 Then
                            listErrors.Add("(Row-" & lngCSVRow & ") No organization matched to Non Biz Company ID for adding new contact.")
                        End If
                    ElseIf (Not ex.InnerException Is Nothing AndAlso ex.InnerException.Message.Contains("MULTIPLE_ORGANIZATIONS_FOUND_WITH_GIVEN_NON BIZ COMPANY ID")) Or ex.Message.Contains("MULTIPLE_ORGANIZATIONS_FOUND_WITH_GIVEN_NON BIZ COMPANY ID") Then
                        If listErrors.Where(Function(x) x.Contains("Multiple organizations matched to Non Biz Company ID for adding new contact.")).Count = 0 Then
                            listErrors.Add("(Row-" & lngCSVRow & ") Multiple organizations matched to Non Biz Company ID for adding new contact.")
                        End If
                    ElseIf (Not ex.InnerException Is Nothing AndAlso ex.InnerException.Message.Contains("ORGANIZATION_NOT_FOUND_WITH_GIVEN_ORGANIZATION ID")) Or ex.Message.Contains("ORGANIZATION_NOT_FOUND_WITH_GIVEN_ORGANIZATION ID") Then
                        If listErrors.Where(Function(x) x.Contains("No organization matched to Organization ID for adding new contact.")).Count = 0 Then
                            listErrors.Add("(Row-" & lngCSVRow & ") No organization matched to Organization ID for adding new contact.")
                        End If
                    ElseIf (Not ex.InnerException Is Nothing AndAlso ex.InnerException.Message.Contains("MULTIPLE_ORGANIZATIONS_FOUND_WITH_GIVEN_ORGANIZATION ID")) Or ex.Message.Contains("MULTIPLE_ORGANIZATIONS_FOUND_WITH_GIVEN_ORGANIZATION ID") Then
                        If listErrors.Where(Function(x) x.Contains("Multiple organizations matched to Organization ID for adding new contact.")).Count = 0 Then
                            listErrors.Add("(Row-" & lngCSVRow & ") Multiple organizations matched to Organization ID for adding new contact.")
                        End If
                    Else
                        listErrors.Add("(Row-" & lngCSVRow & ") Unknown error occured in pre import analysis.")
                    End If

                    Continue For
                End Try
            Next
        Catch ex As Exception
            listErrors.Add("Unknown error occured - " & ex.Message)
        End Try
    End Sub

    Private Sub ValidateImportOrganizationCorrespondence(ByVal lngDomainID As Long, ByVal MasterID As Long, ByVal tintImportType As Short, ByVal dtDataHold As DataTable, ByVal dataTableHeading As DataTable, ByRef listErrors As System.Collections.Generic.List(Of String))
        Try
            Dim lngCSVRow As Integer = 0
            Dim strGetPrimaryColumnName As String = "CommID"
            Dim strFieldName As String = ""
            Dim strFieldType As String = ""
            Dim intFieldMaxLength As Integer = 0
            Dim strFormFieldName As String = ""
            Dim strFormFieldID As Object = Nothing
            Dim vcAssociatedControlType As String = ""
            Dim strValueString As String = ""
            Dim strImportType As String = "Organization Correspondence"
            Dim PropertyItem As PropertyInfo = Nothing
            Dim objClass As Object = Nothing
            Dim strPropertyName As String
            Dim objItemSave As MethodInfo = Nothing
            Dim objItemValue As New Object

            If Not dtDataHold.Columns.Contains("386_vcNonBizContactID") Then
                listErrors.Add("<b>Non Biz Conact ID</b> is required but column does not exists in csv or not mapped.")
            End If

            Dim dtCustomField As New DataTable
            dtCustomField.Columns.Add("Fld_ID").SetOrdinal(0)
            dtCustomField.Columns.Add("Name").SetOrdinal(1)
            dtCustomField.Columns.Add("Fld_Value").SetOrdinal(2)
            dtCustomField.TableName = "CusFlds"
            dtCustomField.AcceptChanges()

            For intC As Integer = 0 To dtDataHold.Rows.Count - 1
                lngCSVRow = intC + 2

                Try
                    objClass = New ActionItem
                    If dtDataHold.Columns.Contains("386_vcNonBizContactID") Then
                        dtDataHold.Columns("386_vcNonBizContactID").SetOrdinal(0)
                    End If

                    Dim dvTempToSort As DataView
                    dvTempToSort = dataTableHeading.DefaultView
                    dvTempToSort.Sort = "IsPrimary DESC"
                    dataTableHeading = dvTempToSort.ToTable()


                    For intHead As Integer = 0 To dataTableHeading.Rows.Count - 1
                        If dataTableHeading.Rows(intHead)("dbFieldName") Is DBNull.Value OrElse dataTableHeading.Rows(intHead)("dbFieldName") = "" OrElse CCommon.ToBool(dataTableHeading.Rows(intHead)("IsErrorDetected")) Then
                            Continue For
                        End If

                        strFormFieldName = dataTableHeading.Rows(intHead)("dbFieldName")
                        strFormFieldID = dataTableHeading.Rows(intHead)("FormFieldID")
                        strPropertyName = dataTableHeading.Rows(intHead)("vcPropertyName")
                        strFieldName = CCommon.ToString(dataTableHeading.Rows(intHead)("FormFieldName"))
                        strFieldType = CCommon.ToString(dataTableHeading.Rows(intHead)("vcFieldDataType"))
                        intFieldMaxLength = CCommon.ToInteger(dataTableHeading.Rows(intHead)("intFieldMaxLength"))

                        If (strFieldType.ToLower() = "m" Or strFieldType.ToLower() = "n") AndAlso
                            (CCommon.ToString(dataTableHeading.Rows(intHead)("vcAssociatedControlType")).ToLower() = "textbox" Or CCommon.ToString(dataTableHeading.Rows(intHead)("vcAssociatedControlType")).ToLower() = "textarea" Or CCommon.ToString(dataTableHeading.Rows(intHead)("vcAssociatedControlType")).ToLower() = "label") AndAlso
                            Not String.IsNullOrEmpty(CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).Trim()) AndAlso
                            Not Double.TryParse(CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)), Nothing) Then
                            dataTableHeading.Rows(intHead)("IsErrorDetected") = True
                            listErrors.Add("<b>" & strFieldName & "</b> (Column-" & dataTableHeading.Rows(intHead)("ImportFieldID") & ", Row-" & lngCSVRow & ") is mapped to a numeric field, but contains non-numeric characters.")
                            Continue For
                        End If

                        If intFieldMaxLength > 0 AndAlso
                            Not String.IsNullOrEmpty(CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).Trim()) AndAlso
                            CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).Length > intFieldMaxLength Then
                            dataTableHeading.Rows(intHead)("IsErrorDetected") = True
                            listErrors.Add("<b>" & strFieldName & "</b> (Column-" & dataTableHeading.Rows(intHead)("ImportFieldID") & ", Row-" & lngCSVRow & ") contains " & CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).Length & " characters, but the destination field only supports " & intFieldMaxLength & ".")
                            Continue For
                        End If

                        Dim objPropValue As Object = Nothing

                        If CBool(dataTableHeading.Rows(intHead)("bitCustomField")) = True Then
                            Dim drCustom As DataRow
                            drCustom = dtCustomField.NewRow
                            drCustom("Fld_ID") = strFormFieldID
                            drCustom("Name") = strFormFieldName

                            If CCommon.ToString(dataTableHeading.Rows(intHead)("vcAssociatedControlType")).ToLower() = "checkbox" Then
                                If CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "true" OrElse
                                 CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "yes" OrElse
                                 CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "1" OrElse
                                 CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "y" Then
                                    drCustom("Fld_Value") = 1
                                ElseIf CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "false" OrElse
                                    CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "no" OrElse
                                    CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "0" OrElse
                                    CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "n" Then
                                    drCustom("Fld_Value") = 0
                                Else
                                    dataTableHeading.Rows(intHead)("IsErrorDetected") = True
                                    listErrors.Add("<b>" & strFieldName & "</b> (Column-" & dataTableHeading.Rows(intHead)("ImportFieldID") & ", Row-" & lngCSVRow & ") Contains an un-supported value (check boxes must be yes/no, true/false or 1/0).")
                                    Continue For
                                End If
                            ElseIf CCommon.ToString(dataTableHeading.Rows(intHead)("vcAssociatedControlType")).ToLower() = "selectbox" AndAlso CCommon.ToLong(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)) = 0 AndAlso Not String.IsNullOrEmpty(CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName & "_Actual")).Trim()) Then
                                dataTableHeading.Rows(intHead)("IsErrorDetected") = True
                                listErrors.Add("<b>" & strFieldName & "</b> (Column-" & dataTableHeading.Rows(intHead)("ImportFieldID") & ", Row-" & lngCSVRow & ") Contains values that don’t exist on mapped drop-down field.")
                                Continue For
                            Else
                                drCustom("Fld_Value") = dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)
                            End If

                            dtCustomField.Rows.Add(drCustom)
                            dtCustomField.AcceptChanges()
                        Else
                            If CCommon.ToString(dataTableHeading.Rows(intHead)("vcAssociatedControlType")).ToLower() = "checkbox" Then
                                If CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "true" OrElse
                                 CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "yes" OrElse
                                 CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "1" OrElse
                                 CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "y" Then
                                    objPropValue = CType(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName), Object)
                                ElseIf CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "false" OrElse
                                    CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "no" OrElse
                                    CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "0" OrElse
                                    CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "n" Then
                                    objPropValue = CType(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName), Object)
                                Else
                                    dataTableHeading.Rows(intHead)("IsErrorDetected") = True
                                    listErrors.Add("<b>" & strFieldName & "</b> (Column-" & dataTableHeading.Rows(intHead)("ImportFieldID") & ", Row-" & lngCSVRow & ") Contains an un-supported value (check boxes must be yes/no, true/false or 1/0).")
                                    Continue For
                                End If
                            ElseIf CCommon.ToString(dataTableHeading.Rows(intHead)("vcAssociatedControlType")).ToLower() = "selectbox" AndAlso CCommon.ToLong(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)) = 0 AndAlso Not String.IsNullOrEmpty(CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName & "_Actual")).Trim()) Then
                                dataTableHeading.Rows(intHead)("IsErrorDetected") = True
                                listErrors.Add("<b>" & strFieldName & "</b> (Column-" & dataTableHeading.Rows(intHead)("ImportFieldID") & ", Row-" & lngCSVRow & ") Contains values that don’t exist on mapped drop-down field.")
                                Continue For
                            Else
                                objPropValue = CType(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName), Object)
                            End If
                        End If

                        If CCommon.ToString(strPropertyName).Length > 0 Then
                            SetProperty(objClass, strPropertyName, objPropValue)
                        End If
                    Next

                    SetProperty(objClass, "DomainID", lngDomainID)
                    SetProperty(objClass, "UserCntID", CCommon.ToLong(Session("UserContactID")))

                    'Load Contact ID and Div ID from Non Biz Contact ID
                    '--------------------------------------------------------------------
                    Dim objLeads As New CLeads
                    objLeads.DomainID = lngDomainID
                    objLeads.byteMode = 1
                    objLeads.NonBizContactID = objClass.NonBizContactID
                    objLeads.ManageImportActionItemReference()
                    '--------------------------------------------------------------------

                    If CCommon.ToLong(objLeads.DivisionID) = 0 Then
                        Throw New Exception("COMPANY_NOT_FOUND")
                    ElseIf CCommon.ToLong(objLeads.ContactID) = 0 Then
                        Throw New Exception("CONTACT_NOT_FOUND")
                    End If
                Catch ex As Exception
                    If ex.Message.Contains("Details/Comments_NOT_PROVIDED") Or (Not ex.InnerException Is Nothing AndAlso ex.InnerException.Message.Contains("Details/Comments_NOT_PROVIDED")) Then
                        If listErrors.Where(Function(x) x.Contains("Details/Comments is required.")).Count = 0 Then
                            listErrors.Add("(Row-" & lngCSVRow & ") Details/Comments is required.")
                        End If
                    ElseIf ex.Message.Contains("COMPANY_NOT_FOUND") Or (Not ex.InnerException Is Nothing AndAlso ex.InnerException.Message.Contains("COMPANY_NOT_FOUND")) Then
                        If listErrors.Where(Function(x) x.Contains("Organization not found.")).Count = 0 Then
                            listErrors.Add("(Row-" & lngCSVRow & ") Organization not found.")
                        End If
                    ElseIf (Not ex.InnerException Is Nothing AndAlso ex.InnerException.Message.Contains("CONTACT_NOT_FOUND")) Or ex.Message.Contains("CONTACT_NOT_FOUND") Then
                        If listErrors.Where(Function(x) x.Contains("Contact not found.")).Count = 0 Then
                            listErrors.Add("(Row-" & lngCSVRow & ") Contact not found.")
                        End If
                    ElseIf ex.Message.Contains("NON_BIZ_CONTACT_ID_NOT_ADDED_IN_CSV") Or (Not ex.InnerException Is Nothing AndAlso ex.InnerException.Message.Contains("NON_BIZ_CONTACT_ID_NOT_ADDED_IN_CSV")) Then
                        If listErrors.Where(Function(x) x.Contains("Non Biz Contact ID is required.")).Count = 0 Then
                            listErrors.Add("(Row-" & lngCSVRow & ") Non Biz Contact ID is required.")
                        End If
                    Else
                        listErrors.Add("(Row-" & lngCSVRow & ") Unknown error occured in pre import analysis.")
                    End If
                End Try
            Next
        Catch ex As Exception
            listErrors.Add("Unknown error occured - " & ex.Message)
        End Try
    End Sub

    Private Sub ValidateImportItemWarehouse(ByVal lngDomainID As Long, ByVal MasterID As Long, ByVal tintImportType As Short, ByVal dtDataHold As DataTable, ByVal dataTableHeading As DataTable, ByRef listErrors As System.Collections.Generic.List(Of String))
        Try
            Dim lngCSVRow As Long = 0
            Dim strGetPrimaryColumnName As String = "WareHouseItemID"
            Dim strFieldName As String = ""
            Dim strFieldType As String = ""
            Dim intFieldMaxLength As Integer = 0
            Dim strFormFieldName As String = ""
            Dim strFormFieldID As Object = Nothing
            Dim vcAssociatedControlType As String = ""
            Dim strValueString As String = ""
            Dim strImportType As String = "Item Warehouse"
            Dim PropertyItem As PropertyInfo = Nothing
            Dim objClass As Object = Nothing
            Dim strPropertyName As String
            Dim objItemSave As MethodInfo = Nothing
            Dim objItemValue As New Object

            If (tintImportType = 1 Or tintImportType = 3) AndAlso Not dtDataHold.Columns.Contains("235_numWareHouseItemID") Then
                listErrors.Add("<b>Warehouse Item ID</b> is required but column does not exists in csv or not mapped.")
            End If

            If (tintImportType = 2 Or tintImportType = 3) AndAlso Not dtDataHold.Columns.Contains("211_numItemCode") Then
                listErrors.Add("<b>Item ID</b> is required but column does not exists in csv or not mapped.")
            End If

            Dim dtCustomField As New DataTable
            dtCustomField.Columns.Add("Fld_ID").SetOrdinal(0)
            dtCustomField.Columns.Add("Name").SetOrdinal(1)
            dtCustomField.Columns.Add("Fld_Value").SetOrdinal(2)
            dtCustomField.TableName = "CusFlds"
            dtCustomField.AcceptChanges()

            For intC As Integer = 0 To dtDataHold.Rows.Count - 1
                Try
                    lngCSVRow = intC + 2

                    objClass = New CItems
                    If objItemSave Is Nothing Then objItemSave = objClass.GetType().GetMethod("AddUpdateWareHouseForItems")
                    If dtDataHold.Columns.Contains("235_numWareHouseItemID") Then
                        dtDataHold.Columns("235_numWareHouseItemID").SetOrdinal(0)
                    End If



                    Dim dvTempToSort As DataView
                    dvTempToSort = dataTableHeading.DefaultView
                    dvTempToSort.Sort = "IsPrimary DESC"
                    dataTableHeading = dvTempToSort.ToTable()


                    For intHead As Integer = 0 To dataTableHeading.Rows.Count - 1
                        If dataTableHeading.Rows(intHead)("dbFieldName") Is DBNull.Value OrElse dataTableHeading.Rows(intHead)("dbFieldName") = "" OrElse CCommon.ToBool(dataTableHeading.Rows(intHead)("IsErrorDetected")) Then
                            Continue For
                        End If

                        strFormFieldName = dataTableHeading.Rows(intHead)("dbFieldName")
                        strFormFieldID = dataTableHeading.Rows(intHead)("FormFieldID")
                        strPropertyName = dataTableHeading.Rows(intHead)("vcPropertyName")
                        strFieldName = CCommon.ToString(dataTableHeading.Rows(intHead)("FormFieldName"))
                        strFieldType = CCommon.ToString(dataTableHeading.Rows(intHead)("vcFieldDataType"))
                        intFieldMaxLength = CCommon.ToInteger(dataTableHeading.Rows(intHead)("intFieldMaxLength"))

                        If (strFieldType.ToLower() = "m" Or strFieldType.ToLower() = "n") AndAlso
                            (CCommon.ToString(dataTableHeading.Rows(intHead)("vcAssociatedControlType")).ToLower() = "textbox" Or CCommon.ToString(dataTableHeading.Rows(intHead)("vcAssociatedControlType")).ToLower() = "textarea" Or CCommon.ToString(dataTableHeading.Rows(intHead)("vcAssociatedControlType")).ToLower() = "label") AndAlso
                            Not String.IsNullOrEmpty(CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).Trim()) AndAlso
                            Not Double.TryParse(CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)), Nothing) Then
                            dataTableHeading.Rows(intHead)("IsErrorDetected") = True
                            listErrors.Add("<b>" & strFieldName & "</b> (Column-" & dataTableHeading.Rows(intHead)("ImportFieldID") & ", Row-" & lngCSVRow & ") is mapped to a numeric field, but contains non-numeric characters.")
                            Continue For
                        End If

                        If intFieldMaxLength > 0 AndAlso
                            Not String.IsNullOrEmpty(CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).Trim()) AndAlso
                            CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).Length > intFieldMaxLength Then
                            dataTableHeading.Rows(intHead)("IsErrorDetected") = True
                            listErrors.Add("<b>" & strFieldName & "</b> (Column-" & dataTableHeading.Rows(intHead)("ImportFieldID") & ", Row-" & lngCSVRow & ") contains " & CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).Length & " characters, but the destination field only supports " & intFieldMaxLength & ".")
                            Continue For
                        End If

                        Dim objPropValue As Object = Nothing

                        If CBool(dataTableHeading.Rows(intHead)("bitCustomField")) = True Then
                            Dim drCustom As DataRow
                            drCustom = dtCustomField.NewRow
                            drCustom("Fld_ID") = strFormFieldID
                            drCustom("Name") = strFormFieldName

                            If CCommon.ToString(dataTableHeading.Rows(intHead)("vcAssociatedControlType")).ToLower() = "checkbox" Then
                                If CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "true" OrElse
                                 CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "yes" OrElse
                                 CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "1" OrElse
                                 CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "y" Then
                                    drCustom("Fld_Value") = 1
                                ElseIf CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "false" OrElse
                                    CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "no" OrElse
                                    CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "0" OrElse
                                    CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "n" Then
                                    drCustom("Fld_Value") = 0
                                Else
                                    dataTableHeading.Rows(intHead)("IsErrorDetected") = True
                                    listErrors.Add("<b>" & strFieldName & "</b> (Column-" & dataTableHeading.Rows(intHead)("ImportFieldID") & ", Row-" & lngCSVRow & ") Contains an un-supported value (check boxes must be yes/no, true/false or 1/0).")
                                    Continue For
                                End If
                            ElseIf CCommon.ToString(dataTableHeading.Rows(intHead)("vcAssociatedControlType")).ToLower() = "selectbox" AndAlso CCommon.ToLong(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)) = 0 AndAlso Not String.IsNullOrEmpty(CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName & "_Actual")).Trim()) Then
                                dataTableHeading.Rows(intHead)("IsErrorDetected") = True
                                listErrors.Add("<b>" & strFieldName & "</b> (Column-" & dataTableHeading.Rows(intHead)("ImportFieldID") & ", Row-" & lngCSVRow & ") Contains values that don’t exist on mapped drop-down field.")
                                Continue For
                            Else
                                drCustom("Fld_Value") = dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)
                            End If

                            dtCustomField.Rows.Add(drCustom)
                            dtCustomField.AcceptChanges()
                        Else
                            If CCommon.ToString(dataTableHeading.Rows(intHead)("vcAssociatedControlType")).ToLower() = "checkbox" Then
                                If CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "true" OrElse
                                 CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "yes" OrElse
                                 CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "1" OrElse
                                 CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "y" Then
                                    objPropValue = CType(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName), Object)
                                ElseIf CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "false" OrElse
                                    CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "no" OrElse
                                    CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "0" OrElse
                                    CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "n" Then
                                    objPropValue = CType(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName), Object)
                                Else
                                    dataTableHeading.Rows(intHead)("IsErrorDetected") = True
                                    listErrors.Add("<b>" & strFieldName & "</b> (Column-" & dataTableHeading.Rows(intHead)("ImportFieldID") & ", Row-" & lngCSVRow & ") Contains an un-supported value (check boxes must be yes/no, true/false or 1/0).")
                                    Continue For
                                End If
                            ElseIf CCommon.ToString(dataTableHeading.Rows(intHead)("vcAssociatedControlType")).ToLower() = "selectbox" AndAlso CCommon.ToLong(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)) = 0 AndAlso Not String.IsNullOrEmpty(CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName & "_Actual")).Trim()) Then
                                dataTableHeading.Rows(intHead)("IsErrorDetected") = True
                                listErrors.Add("<b>" & strFieldName & "</b> (Column-" & dataTableHeading.Rows(intHead)("ImportFieldID") & ", Row-" & lngCSVRow & ") Contains values that don’t exist on mapped drop-down field.")
                                Continue For
                            Else
                                objPropValue = CType(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName), Object)

                                If strFormFieldID = 211 And (tintImportType = 2 Or tintImportType = 3) Then
                                    Try
                                        Dim objCom As New CCommon
                                        objCom.DomainID = lngDomainID
                                        Dim dtLinkingData As DataTable = objCom.GetRecordByLinkingID(20, 1, CCommon.ToString(objPropValue))
                                    Catch ex As Exception
                                        dataTableHeading.Rows(intHead)("IsErrorDetected") = True
                                        listErrors.Add("<b>" & strFieldName & "</b> (Column-" & dataTableHeading.Rows(intHead)("ImportFieldID") & ", Row-" & lngCSVRow & ") contains values that don’t exist.")
                                        Continue For
                                    End Try
                                End If
                            End If
                        End If


                        If strFormFieldID = 235 AndAlso (tintImportType = 1 Or tintImportType = 3) Then
                            objClass.WareHouseItemID = CCommon.ToLong(objPropValue)
                            objClass.DomainID = lngDomainID
                            objClass.GetWarehouseItemDetails()

                            If (tintImportType = 2 Or (tintImportType = 3 AndAlso Not String.IsNullOrEmpty(CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName & "_Actual")).Trim()))) AndAlso CCommon.ToLong(objClass.WareHouseItemID) = 0 Then
                                listErrors.Add("<b>" & strFieldName & "</b> (Column-" & dataTableHeading.Rows(intHead)("ImportFieldID") & ", Row-" & lngCSVRow & ") contains values that don’t exist.")
                                Continue For
                            End If

                            objPropValue = CCommon.ToLong(objClass.WareHouseItemID)
                        End If


                        If CCommon.ToString(strPropertyName).Length > 0 Then
                            If strPropertyName.Contains("OnHand") = True Then
                                objPropValue = If(CCommon.ToLong(objPropValue) < 0, 0, objPropValue)
                            End If

                            SetProperty(objClass, strPropertyName, objPropValue)
                        End If
                    Next
                Catch ex As Exception
                    If ex.Message.Contains("Warehouse_Item_ID_REQUIRED") Or (Not ex.InnerException Is Nothing AndAlso ex.InnerException.Message.Contains("Warehouse_Item_ID_REQUIRED")) Then
                        If listErrors.Where(Function(x) x.Contains("Warehouse Item ID column is not added to CSV.")).Count = 0 Then
                            listErrors.Add("(Row-" & lngCSVRow & ") Warehouse Item ID column is not added to CSV.")
                        End If
                    ElseIf ex.Message.Contains("WAREHOUSE_DETAIL_NOT_FOUND") Or (Not ex.InnerException Is Nothing AndAlso ex.InnerException.Message.Contains("WAREHOUSE_DETAIL_NOT_FOUND")) Then
                        If listErrors.Where(Function(x) x.Contains("Invalid Warehouse Item ID.")).Count = 0 Then
                            listErrors.Add("(Row-" & lngCSVRow & ") Invalid Warehouse Item ID.")
                        End If
                    ElseIf ex.Message.Contains("ItemID_COLUMN_REQUIRED") Or (Not ex.InnerException Is Nothing AndAlso ex.InnerException.Message.Contains("ItemID_COLUMN_REQUIRED")) Then
                        If listErrors.Where(Function(x) x.Contains("Item ID column is required to insert warehouse.")).Count = 0 Then
                            listErrors.Add("(Row-" & lngCSVRow & ") Item ID column is required to insert warehouse.")
                        End If
                    ElseIf ex.Message.Contains("INVALID_ITEM_CODE") Or (Not ex.InnerException Is Nothing AndAlso ex.InnerException.Message.Contains("INVALID_ITEM_CODE")) Then
                        If listErrors.Where(Function(x) x.Contains("Invalid Item Code.")).Count = 0 Then
                            listErrors.Add("(Row-" & lngCSVRow & ") Invalid Item Code.")
                        End If
                    Else
                        listErrors.Add("(Row-" & lngCSVRow & ") Unknown error occured in pre import analysis.")
                    End If
                End Try
            Next
        Catch ex As Exception
            listErrors.Add("Unknown error occured - " & ex.Message)
        End Try
    End Sub

    Private Sub ValidateImportItemVendors(ByVal lngDomainID As Long, ByVal MasterID As Long, ByVal tintImportType As Short, ByVal dtDataHold As DataTable, ByVal dataTableHeading As DataTable, ByRef listErrors As System.Collections.Generic.List(Of String))
        Try
            Dim lngCSVRow As Integer = 0
            Dim strGetPrimaryColumnName As String = "VendorTcode"
            Dim strFieldName As String = ""
            Dim strFieldType As String = ""
            Dim intFieldMaxLength As Integer = 0
            Dim strFormFieldName As String = ""
            Dim strFormFieldID As Object = Nothing
            Dim vcAssociatedControlType As String = ""
            Dim strValueString As String = ""
            Dim strImportType As String = "Item Vendor"
            Dim PropertyItem As PropertyInfo = Nothing
            Dim objClass As Object = Nothing
            Dim strPropertyName As String
            Dim objItemSave As MethodInfo = Nothing
            Dim objItemValue As New Object

            Dim dtCustomField As New DataTable
            dtCustomField.Columns.Add("Fld_ID").SetOrdinal(0)
            dtCustomField.Columns.Add("Name").SetOrdinal(1)
            dtCustomField.Columns.Add("Fld_Value").SetOrdinal(2)
            dtCustomField.TableName = "CusFlds"
            dtCustomField.AcceptChanges()

            If (tintImportType = 1 Or tintImportType = 3) AndAlso Not dtDataHold.Columns.Contains("349_numVendorID") Then
                listErrors.Add("<b>Vendor</b> is required but column does not exists in csv or not mapped.")
            End If

            If Not dtDataHold.Columns.Contains("211_numItemCode") Then
                listErrors.Add("<b>Item ID</b> is required but column does not exists in csv or not mapped.")
            End If

            For intC As Integer = 0 To dtDataHold.Rows.Count - 1
                Try
                    lngCSVRow = intC + 2

                    objClass = New CItems

                    If dtDataHold.Columns.Contains("349_numVendorID") Then
                        dtDataHold.Columns("349_numVendorID").SetOrdinal(0)
                    ElseIf dtDataHold.Columns.Contains("211_numItemCode") Then
                        dtDataHold.Columns("211_numItemCode").SetOrdinal(0)
                    End If

                    Dim dvTempToSort As DataView
                    dvTempToSort = dataTableHeading.DefaultView
                    dvTempToSort.Sort = "IsPrimary DESC"
                    dataTableHeading = dvTempToSort.ToTable()


                    For intHead As Integer = 0 To dataTableHeading.Rows.Count - 1
                        If dataTableHeading.Rows(intHead)("dbFieldName") Is DBNull.Value OrElse dataTableHeading.Rows(intHead)("dbFieldName") = "" OrElse CCommon.ToBool(dataTableHeading.Rows(intHead)("IsErrorDetected")) Then
                            Continue For
                        End If

                        strFormFieldName = dataTableHeading.Rows(intHead)("dbFieldName")
                        strFormFieldID = dataTableHeading.Rows(intHead)("FormFieldID")
                        strPropertyName = dataTableHeading.Rows(intHead)("vcPropertyName")
                        strFieldName = CCommon.ToString(dataTableHeading.Rows(intHead)("FormFieldName"))
                        strFieldType = CCommon.ToString(dataTableHeading.Rows(intHead)("vcFieldDataType"))
                        intFieldMaxLength = CCommon.ToInteger(dataTableHeading.Rows(intHead)("intFieldMaxLength"))

                        If (strFieldType.ToLower() = "m" Or strFieldType.ToLower() = "n") AndAlso
                            (CCommon.ToString(dataTableHeading.Rows(intHead)("vcAssociatedControlType")).ToLower() = "textbox" Or CCommon.ToString(dataTableHeading.Rows(intHead)("vcAssociatedControlType")).ToLower() = "textarea" Or CCommon.ToString(dataTableHeading.Rows(intHead)("vcAssociatedControlType")).ToLower() = "label") AndAlso
                            Not String.IsNullOrEmpty(CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).Trim()) AndAlso
                            Not Double.TryParse(CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)), Nothing) Then
                            dataTableHeading.Rows(intHead)("IsErrorDetected") = True
                            listErrors.Add("<b>" & strFieldName & "</b> (Column-" & dataTableHeading.Rows(intHead)("ImportFieldID") & ", Row-" & lngCSVRow & ") is mapped to a numeric field, but contains non-numeric characters.")
                            Continue For
                        End If

                        If intFieldMaxLength > 0 AndAlso
                            Not String.IsNullOrEmpty(CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).Trim()) AndAlso
                            CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).Length > intFieldMaxLength Then
                            dataTableHeading.Rows(intHead)("IsErrorDetected") = True
                            listErrors.Add("<b>" & strFieldName & "</b> (Column-" & dataTableHeading.Rows(intHead)("ImportFieldID") & ", Row-" & lngCSVRow & ") contains " & CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).Length & " characters, but the destination field only supports " & intFieldMaxLength & ".")
                            Continue For
                        End If

                        Dim objPropValue As Object = Nothing

                        If CBool(dataTableHeading.Rows(intHead)("bitCustomField")) = True Then
                            Dim drCustom As DataRow
                            drCustom = dtCustomField.NewRow
                            drCustom("Fld_ID") = strFormFieldID
                            drCustom("Name") = strFormFieldName

                            If CCommon.ToString(dataTableHeading.Rows(intHead)("vcAssociatedControlType")).ToLower() = "checkbox" Then
                                If CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "true" OrElse
                                 CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "yes" OrElse
                                 CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "1" OrElse
                                 CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "y" Then
                                    drCustom("Fld_Value") = 1
                                ElseIf CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "false" OrElse
                                    CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "no" OrElse
                                    CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "0" OrElse
                                    CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "n" Then
                                    drCustom("Fld_Value") = 0
                                Else
                                    dataTableHeading.Rows(intHead)("IsErrorDetected") = True
                                    listErrors.Add("<b>" & strFieldName & "</b> (Column-" & dataTableHeading.Rows(intHead)("ImportFieldID") & ", Row-" & lngCSVRow & ") Contains an un-supported value (check boxes must be yes/no, true/false or 1/0).")
                                    Continue For
                                End If
                            ElseIf CCommon.ToString(dataTableHeading.Rows(intHead)("vcAssociatedControlType")).ToLower() = "selectbox" AndAlso CCommon.ToLong(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)) = 0 AndAlso Not String.IsNullOrEmpty(CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName & "_Actual")).Trim()) Then
                                dataTableHeading.Rows(intHead)("IsErrorDetected") = True
                                listErrors.Add("<b>" & strFieldName & "</b> (Column-" & dataTableHeading.Rows(intHead)("ImportFieldID") & ", Row-" & lngCSVRow & ") Contains values that don’t exist on mapped drop-down field.")
                                Continue For
                            Else
                                drCustom("Fld_Value") = dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)
                            End If

                            dtCustomField.Rows.Add(drCustom)
                            dtCustomField.AcceptChanges()
                        Else
                            If CCommon.ToString(dataTableHeading.Rows(intHead)("vcAssociatedControlType")).ToLower() = "checkbox" Then
                                If CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "true" OrElse
                                 CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "yes" OrElse
                                 CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "1" OrElse
                                 CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "y" Then
                                    objPropValue = CType(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName), Object)
                                ElseIf CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "false" OrElse
                                    CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "no" OrElse
                                    CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "0" OrElse
                                    CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "n" Then
                                    objPropValue = CType(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName), Object)
                                Else
                                    dataTableHeading.Rows(intHead)("IsErrorDetected") = True
                                    listErrors.Add("<b>" & strFieldName & "</b> (Column-" & dataTableHeading.Rows(intHead)("ImportFieldID") & ", Row-" & lngCSVRow & ") Contains an un-supported value (check boxes must be yes/no, true/false or 1/0).")
                                    Continue For
                                End If
                            ElseIf CCommon.ToString(dataTableHeading.Rows(intHead)("vcAssociatedControlType")).ToLower() = "selectbox" AndAlso CCommon.ToLong(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)) = 0 AndAlso Not String.IsNullOrEmpty(CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName & "_Actual")).Trim()) Then
                                dataTableHeading.Rows(intHead)("IsErrorDetected") = True
                                listErrors.Add("<b>" & strFieldName & "</b> (Column-" & dataTableHeading.Rows(intHead)("ImportFieldID") & ", Row-" & lngCSVRow & ") Contains values that don’t exist on mapped drop-down field.")
                                Continue For
                            Else
                                objPropValue = CType(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName), Object)
                            End If
                        End If


                        If strFormFieldID = 349 Then
                            Dim dr As DataRow() = dataTableHeading.Select("dbFieldName='numItemCode'")

                            If Not dr Is Nothing AndAlso dr.Length > 0 Then
                                objClass.ItemCode = CCommon.ToLong(dtDataHold.Rows(intC)(dr(0)("FormFieldID") & "_" & dr(0)("dbFieldName")))
                            End If

                            objClass.VendorID = CCommon.ToLong(objPropValue)
                            objClass.DomainID = lngDomainID
                            objClass.GetItemVendorDetails()

                            If (tintImportType = 1 Or (tintImportType = 3 AndAlso Not String.IsNullOrEmpty(CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName & "_Actual")).Trim()))) AndAlso CCommon.ToLong(objClass.VendorTcode) = 0 Then
                                listErrors.Add("<b>" & strFieldName & "</b> (Column-" & dataTableHeading.Rows(intHead)("ImportFieldID") & ", Row-" & lngCSVRow & ") contains values that don’t exist.")
                                Continue For
                            End If
                        ElseIf strFormFieldID = 211 AndAlso (tintImportType = 2 Or tintImportType = 3) Then
                            Try
                                Dim objCom As New CCommon
                                objCom.DomainID = lngDomainID
                                Dim dtLinkingData As DataTable = objCom.GetRecordByLinkingID(20, 1, CCommon.ToString(objPropValue))
                            Catch ex As Exception
                                dataTableHeading.Rows(intHead)("IsErrorDetected") = True
                                listErrors.Add("<b>" & strFieldName & "</b> (Column-" & dataTableHeading.Rows(intHead)("ImportFieldID") & ", Row-" & lngCSVRow & ") contains values that don’t exist.")
                                Continue For
                            End Try
                        End If
                    Next
                Catch ex As Exception
                    If ex.Message.Contains("INVALID_ITEM_VENDOR") Or (Not ex.InnerException Is Nothing AndAlso ex.InnerException.Message.Contains("INVALID_ITEM_VENDOR")) Then
                        If listErrors.Where(Function(x) x.Contains("Vendor don’t exist.")).Count = 0 Then
                            listErrors.Add("(Row-" & lngCSVRow & ") Vendor don’t exist.")
                        End If
                    ElseIf (Not ex.InnerException Is Nothing AndAlso ex.InnerException.Message.Contains("ITEM_NOT_FOUND_WITH_GIVEN_ITEMCODE")) Or ex.Message.Contains("ITEM_NOT_FOUND_WITH_GIVEN_ITEMCODE") Then
                        If listErrors.Where(Function(x) x.Contains("Item don’t exist.")).Count = 0 Then
                            listErrors.Add("(Row-" & lngCSVRow & ") Item don’t exist.")
                        End If
                    ElseIf (Not ex.InnerException Is Nothing AndAlso ex.InnerException.Message.Contains("MULTIPLE_ITEMS_FOUND_WITH_GIVEN_ITEMCODE")) Or ex.Message.Contains("MULTIPLE_ITEMS_FOUND_WITH_GIVEN_ITEMCODE") Then
                        If listErrors.Where(Function(x) x.Contains("Multiple items matched to item code for update.")).Count = 0 Then
                            listErrors.Add("(Row-" & lngCSVRow & ") Multiple items matched to item code for update.")
                        End If
                    ElseIf (Not ex.InnerException Is Nothing AndAlso ex.InnerException.Message.Contains("INVALID_ITEM_OR_VENDOR")) Or ex.Message.Contains("INVALID_ITEM_OR_VENDOR") Then
                        If listErrors.Where(Function(x) x.Contains("No record found to update with given item and vendor.")).Count = 0 Then
                            listErrors.Add("(Row-" & lngCSVRow & ") No record found to update with given item and vendor.")
                        End If
                    ElseIf (Not ex.InnerException Is Nothing AndAlso ex.InnerException.Message.Contains("MULTIPLE_SERIAL_NO")) Or ex.Message.Contains("MULTIPLE_SERIAL_NO") Then
                        If listErrors.Where(Function(x) x.Contains("You can add one serial no per row.")).Count = 0 Then
                            listErrors.Add("(Row-" & lngCSVRow & ") You can add one serial no per row.")
                        End If
                    Else
                        listErrors.Add("(Row-" & lngCSVRow & ") Unknown error occured in pre import analysis.")
                    End If
                End Try
            Next
        Catch ex As Exception
            listErrors.Add("Unknown error occured - " & ex.Message)
        End Try
    End Sub

    Private Sub ValidateImportItemImages(ByVal lngDomainID As Long, ByVal MasterID As Long, ByVal tintImportType As Short, ByVal dtDataHold As DataTable, ByVal dataTableHeading As DataTable, ByRef listErrors As System.Collections.Generic.List(Of String))
        Try
            Dim lngCSVRow As Integer = 0
            Dim strGetPrimaryColumnName As String = "numItemImageId"
            Dim strFieldName As String = ""
            Dim strFieldType As String = ""
            Dim intFieldMaxLength As Integer = 0
            Dim strFormFieldName As String = ""
            Dim strFormFieldID As Object = Nothing
            Dim vcAssociatedControlType As String = ""
            Dim strValueString As String = ""
            Dim strImportType As String = "Item Images"
            Dim PropertyItem As PropertyInfo = Nothing
            Dim objClass As Object = Nothing
            Dim strPropertyName As String
            Dim objItemSave As MethodInfo = Nothing
            Dim objItemValue As New Object

            If (tintImportType = 1 Or tintImportType = 3) AndAlso Not dtDataHold.Columns.Contains("381_numItemImageId") Then
                listErrors.Add("<b>Item Image ID</b> is required but column does not exists in csv or not mapped.")
            End If
            If (tintImportType = 2 Or tintImportType = 3) AndAlso Not dtDataHold.Columns.Contains("211_numItemCode") Then
                listErrors.Add("<b>Item Code</b> is required but column does not exists in csv or not mapped.")
            End If

            Dim dtCustomField As New DataTable
            dtCustomField.Columns.Add("Fld_ID").SetOrdinal(0)
            dtCustomField.Columns.Add("Name").SetOrdinal(1)
            dtCustomField.Columns.Add("Fld_Value").SetOrdinal(2)
            dtCustomField.TableName = "CusFlds"
            dtCustomField.AcceptChanges()

            For intC As Integer = 0 To dtDataHold.Rows.Count - 1
                Try
                    lngCSVRow = intC + 2

                    objClass = New CItems
                    If objItemSave Is Nothing Then objItemSave = objClass.GetType().GetMethod("AddUpdateImageItem")
                    If dtDataHold.Columns.Contains("381_numItemImageId") Then
                        dtDataHold.Columns("381_numItemImageId").SetOrdinal(0)
                    ElseIf dtDataHold.Columns.Contains("211_numItemCode") Then
                        dtDataHold.Columns("211_numItemCode").SetOrdinal(0)
                    End If

                    Dim dvTempToSort As DataView
                    dvTempToSort = dataTableHeading.DefaultView
                    dvTempToSort.Sort = "IsPrimary DESC"
                    dataTableHeading = dvTempToSort.ToTable()


                    For intHead As Integer = 0 To dataTableHeading.Rows.Count - 1
                        If dataTableHeading.Rows(intHead)("dbFieldName") Is DBNull.Value OrElse dataTableHeading.Rows(intHead)("dbFieldName") = "" OrElse CCommon.ToBool(dataTableHeading.Rows(intHead)("IsErrorDetected")) Then
                            Continue For
                        End If

                        strFormFieldName = dataTableHeading.Rows(intHead)("dbFieldName")
                        strFormFieldID = dataTableHeading.Rows(intHead)("FormFieldID")
                        strPropertyName = dataTableHeading.Rows(intHead)("vcPropertyName")
                        strFieldName = CCommon.ToString(dataTableHeading.Rows(intHead)("FormFieldName"))
                        strFieldType = CCommon.ToString(dataTableHeading.Rows(intHead)("vcFieldDataType"))
                        intFieldMaxLength = CCommon.ToInteger(dataTableHeading.Rows(intHead)("intFieldMaxLength"))

                        If (strFieldType.ToLower() = "m" Or strFieldType.ToLower() = "n") AndAlso
                            (CCommon.ToString(dataTableHeading.Rows(intHead)("vcAssociatedControlType")).ToLower() = "textbox" Or CCommon.ToString(dataTableHeading.Rows(intHead)("vcAssociatedControlType")).ToLower() = "textarea" Or CCommon.ToString(dataTableHeading.Rows(intHead)("vcAssociatedControlType")).ToLower() = "label") AndAlso
                            Not String.IsNullOrEmpty(CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).Trim()) AndAlso
                            Not Double.TryParse(CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)), Nothing) Then
                            dataTableHeading.Rows(intHead)("IsErrorDetected") = True
                            listErrors.Add("<b>" & strFieldName & "</b> (Column-" & dataTableHeading.Rows(intHead)("ImportFieldID") & ", Row-" & lngCSVRow & ") is mapped to a numeric field, but contains non-numeric characters.")
                            Continue For
                        End If

                        If intFieldMaxLength > 0 AndAlso
                            Not String.IsNullOrEmpty(CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).Trim()) AndAlso
                            CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).Length > intFieldMaxLength Then
                            dataTableHeading.Rows(intHead)("IsErrorDetected") = True
                            listErrors.Add("<b>" & strFieldName & "</b> (Column-" & dataTableHeading.Rows(intHead)("ImportFieldID") & ", Row-" & lngCSVRow & ") contains " & CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).Length & " characters, but the destination field only supports " & intFieldMaxLength & ".")
                            Continue For
                        End If

                        Dim objPropValue As Object = Nothing

                        If CBool(dataTableHeading.Rows(intHead)("bitCustomField")) = True Then
                            Dim drCustom As DataRow
                            drCustom = dtCustomField.NewRow
                            drCustom("Fld_ID") = strFormFieldID
                            drCustom("Name") = strFormFieldName

                            If CCommon.ToString(dataTableHeading.Rows(intHead)("vcAssociatedControlType")).ToLower() = "checkbox" Then
                                If CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "true" OrElse
                                 CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "yes" OrElse
                                 CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "1" OrElse
                                 CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "y" Then
                                    drCustom("Fld_Value") = 1
                                ElseIf CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "false" OrElse
                                    CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "no" OrElse
                                    CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "0" OrElse
                                    CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "n" Then
                                    drCustom("Fld_Value") = 0
                                Else
                                    dataTableHeading.Rows(intHead)("IsErrorDetected") = True
                                    listErrors.Add("<b>" & strFieldName & "</b> (Column-" & dataTableHeading.Rows(intHead)("ImportFieldID") & ", Row-" & lngCSVRow & ") Contains an un-supported value (check boxes must be yes/no, true/false or 1/0).")
                                    Continue For
                                End If
                            ElseIf CCommon.ToString(dataTableHeading.Rows(intHead)("vcAssociatedControlType")).ToLower() = "selectbox" AndAlso CCommon.ToLong(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)) = 0 AndAlso Not String.IsNullOrEmpty(CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName & "_Actual")).Trim()) Then
                                dataTableHeading.Rows(intHead)("IsErrorDetected") = True
                                listErrors.Add("<b>" & strFieldName & "</b> (Column-" & dataTableHeading.Rows(intHead)("ImportFieldID") & ", Row-" & lngCSVRow & ") Contains values that don’t exist on mapped drop-down field.")
                                Continue For
                            Else
                                drCustom("Fld_Value") = dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)
                            End If

                            dtCustomField.Rows.Add(drCustom)
                            dtCustomField.AcceptChanges()
                        Else
                            If CCommon.ToString(dataTableHeading.Rows(intHead)("vcAssociatedControlType")).ToLower() = "checkbox" Then
                                If CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "true" OrElse
                                 CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "yes" OrElse
                                 CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "1" OrElse
                                 CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "y" Then
                                    objPropValue = CType(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName), Object)
                                ElseIf CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "false" OrElse
                                    CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "no" OrElse
                                    CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "0" OrElse
                                    CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "n" Then
                                    objPropValue = CType(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName), Object)
                                Else
                                    dataTableHeading.Rows(intHead)("IsErrorDetected") = True
                                    listErrors.Add("<b>" & strFieldName & "</b> (Column-" & dataTableHeading.Rows(intHead)("ImportFieldID") & ", Row-" & lngCSVRow & ") Contains an un-supported value (check boxes must be yes/no, true/false or 1/0).")
                                    Continue For
                                End If
                            ElseIf CCommon.ToString(dataTableHeading.Rows(intHead)("vcAssociatedControlType")).ToLower() = "selectbox" AndAlso CCommon.ToLong(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)) = 0 AndAlso Not String.IsNullOrEmpty(CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName & "_Actual")).Trim()) Then
                                dataTableHeading.Rows(intHead)("IsErrorDetected") = True
                                listErrors.Add("<b>" & strFieldName & "</b> (Column-" & dataTableHeading.Rows(intHead)("ImportFieldID") & ", Row-" & lngCSVRow & ") Contains values that don’t exist on mapped drop-down field.")
                                Continue For
                            Else
                                objPropValue = CType(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName), Object)
                            End If
                        End If


                        If strFormFieldName = "numItemImageId" Then
                            objClass.numItemImageId = CCommon.ToLong(objPropValue)
                            objClass.DomainID = lngDomainID
                            objClass.GetItemImageDetails()

                            If (tintImportType = 1 Or (tintImportType = 3 AndAlso Not String.IsNullOrEmpty(CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).Trim()))) AndAlso CCommon.ToLong(objClass.numItemImageId) = 0 Then
                                dataTableHeading.Rows(intHead)("IsErrorDetected") = True
                                listErrors.Add("<b>" & strFieldName & "</b> (Column-" & dataTableHeading.Rows(intHead)("ImportFieldID") & ", Row-" & lngCSVRow & ") contains values that don’t exist.")
                                Continue For
                            End If

                            objPropValue = CCommon.ToLong(objClass.numItemImageId)
                        ElseIf strFormFieldID = 211 AndAlso (tintImportType = 2 Or tintImportType = 3) Then
                            Try
                                Dim objCom As New CCommon
                                objCom.DomainID = lngDomainID
                                Dim dtLinkingData As DataTable = objCom.GetRecordByLinkingID(20, 1, CCommon.ToString(objPropValue))
                            Catch ex As Exception
                                dataTableHeading.Rows(intHead)("IsErrorDetected") = True
                                listErrors.Add("<b>" & strFieldName & "</b> (Column-" & dataTableHeading.Rows(intHead)("ImportFieldID") & ", Row-" & lngCSVRow & ") contains values that don’t exist.")
                                Continue For
                            End Try
                        End If

                        If CCommon.ToString(strPropertyName).Length > 0 Then
                            SetProperty(objClass, strPropertyName, objPropValue)
                        End If
                    Next
                Catch ex As Exception
                    If ex.Message.Contains("INVALID_ITEM_IMAGE_ID") Or (Not ex.InnerException Is Nothing AndAlso ex.InnerException.Message.Contains("INVALID_ITEM_IMAGE_ID")) Then
                        If listErrors.Where(Function(x) x.Contains("Item Image ID don’t exist.")).Count = 0 Then
                            listErrors.Add("(Row-" & lngCSVRow & ") Item Image ID don’t exist.")
                        End If
                    ElseIf (Not ex.InnerException Is Nothing AndAlso ex.InnerException.Message.Contains("ITEM_NOT_FOUND_WITH_GIVEN_ITEMCODE")) Or ex.Message.Contains("ITEM_NOT_FOUND_WITH_GIVEN_ITEMCODE") Then
                        If listErrors.Where(Function(x) x.Contains("Item don’t exist.")).Count = 0 Then
                            listErrors.Add("(Row-" & lngCSVRow & ") Item don’t exist.")
                        End If
                    ElseIf (Not ex.InnerException Is Nothing AndAlso ex.InnerException.Message.Contains("MULTIPLE_ITEMS_FOUND_WITH_GIVEN_ITEMCODE")) Or ex.Message.Contains("MULTIPLE_ITEMS_FOUND_WITH_GIVEN_ITEMCODE") Then
                        If listErrors.Where(Function(x) x.Contains("Multiple items found.")).Count = 0 Then
                            listErrors.Add("(Row-" & lngCSVRow & ") Multiple items found.")
                        End If
                    Else
                        listErrors.Add("(Row-" & lngCSVRow & ") Unknown error occured in pre import analysis.")
                    End If
                End Try
            Next
        Catch ex As Exception
            listErrors.Add("Unknown error occured - " & ex.Message)
        End Try
    End Sub

    Private Sub ValidateImportKitSubItems(ByVal lngDomainID As Long, ByVal MasterID As Long, ByVal tintImportType As Short, ByVal dtDataHold As DataTable, ByVal dataTableHeading As DataTable, ByRef listErrors As System.Collections.Generic.List(Of String))
        Try
            Dim lngCSVRow As Integer = 0
            Dim strGetPrimaryColumnName As String = "ItemDetailID"
            Dim strFieldName As String = ""
            Dim strFieldType As String = ""
            Dim intFieldMaxLength As Integer = 0
            Dim strFormFieldName As String = ""
            Dim strFormFieldID As Object = Nothing
            Dim vcAssociatedControlType As String = ""
            Dim strValueString As String = ""
            Dim strImportType As String = "Item Assembly"
            Dim PropertyItem As PropertyInfo = Nothing
            Dim objClass As Object = Nothing
            Dim strPropertyName As String
            Dim objItemSave As MethodInfo = Nothing
            Dim objItemValue As New Object

            If (tintImportType = 1 Or tintImportType = 3) AndAlso Not dtDataHold.Columns.Contains("385_numItemDetailID") Then
                listErrors.Add("<b>Item Detail ID</b> is required but column does not exists in csv or not mapped.")
            End If

            'If (tintImportType = 2 Or tintImportType = 3) AndAlso Not dtDataHold.Columns.Contains("211_numItemCode") Then
            '    listErrors.Add("<b>Item ID</b> is required but column does not exists in csv or not mapped.")
            'End If

            Dim dtCustomField As New DataTable
            dtCustomField.Columns.Add("Fld_ID").SetOrdinal(0)
            dtCustomField.Columns.Add("Name").SetOrdinal(1)
            dtCustomField.Columns.Add("Fld_Value").SetOrdinal(2)
            dtCustomField.TableName = "CusFlds"
            dtCustomField.AcceptChanges()

            For intC As Integer = 0 To dtDataHold.Rows.Count - 1
                Try
                    lngCSVRow = intC + 2

                    objClass = New CItems
                    If dtDataHold.Columns.Contains("385_numItemDetailID") Then dtDataHold.Columns("385_numItemDetailID").SetOrdinal(0)

                    Dim dvTempToSort As DataView
                    dvTempToSort = dataTableHeading.DefaultView
                    dvTempToSort.Sort = "IsPrimary DESC"
                    dataTableHeading = dvTempToSort.ToTable()


                    For intHead As Integer = 0 To dataTableHeading.Rows.Count - 1
                        If dataTableHeading.Rows(intHead)("dbFieldName") Is DBNull.Value OrElse dataTableHeading.Rows(intHead)("dbFieldName") = "" OrElse CCommon.ToBool(dataTableHeading.Rows(intHead)("IsErrorDetected")) Then
                            Continue For
                        End If

                        strFormFieldName = dataTableHeading.Rows(intHead)("dbFieldName")
                        strFormFieldID = dataTableHeading.Rows(intHead)("FormFieldID")
                        strPropertyName = dataTableHeading.Rows(intHead)("vcPropertyName")
                        strFieldName = CCommon.ToString(dataTableHeading.Rows(intHead)("FormFieldName"))
                        strFieldType = CCommon.ToString(dataTableHeading.Rows(intHead)("vcFieldDataType"))
                        intFieldMaxLength = CCommon.ToInteger(dataTableHeading.Rows(intHead)("intFieldMaxLength"))

                        If (strFieldType.ToLower() = "m" Or strFieldType.ToLower() = "n") AndAlso
                            (CCommon.ToString(dataTableHeading.Rows(intHead)("vcAssociatedControlType")).ToLower() = "textbox" Or CCommon.ToString(dataTableHeading.Rows(intHead)("vcAssociatedControlType")).ToLower() = "textarea" Or CCommon.ToString(dataTableHeading.Rows(intHead)("vcAssociatedControlType")).ToLower() = "label") AndAlso
                            Not String.IsNullOrEmpty(CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).Trim()) AndAlso
                            Not Double.TryParse(CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)), Nothing) Then
                            dataTableHeading.Rows(intHead)("IsErrorDetected") = True
                            listErrors.Add("<b>" & strFieldName & "</b> (Column-" & dataTableHeading.Rows(intHead)("ImportFieldID") & ", Row-" & lngCSVRow & ") is mapped to a numeric field, but contains non-numeric characters.")
                            Continue For
                        End If

                        If intFieldMaxLength > 0 AndAlso
                            Not String.IsNullOrEmpty(CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).Trim()) AndAlso
                            CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).Length > intFieldMaxLength Then
                            dataTableHeading.Rows(intHead)("IsErrorDetected") = True
                            listErrors.Add("<b>" & strFieldName & "</b> (Column-" & dataTableHeading.Rows(intHead)("ImportFieldID") & ", Row-" & lngCSVRow & ") contains " & CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).Length & " characters, but the destination field only supports " & intFieldMaxLength & ".")
                            Continue For
                        End If

                        Dim objPropValue As Object = Nothing

                        If CBool(dataTableHeading.Rows(intHead)("bitCustomField")) = True Then
                            Dim drCustom As DataRow
                            drCustom = dtCustomField.NewRow
                            drCustom("Fld_ID") = strFormFieldID
                            drCustom("Name") = strFormFieldName

                            If CCommon.ToString(dataTableHeading.Rows(intHead)("vcAssociatedControlType")).ToLower() = "checkbox" Then
                                If CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "true" OrElse
                                 CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "yes" OrElse
                                 CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "1" OrElse
                                 CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "y" Then
                                    drCustom("Fld_Value") = 1
                                ElseIf CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "false" OrElse
                                    CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "no" OrElse
                                    CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "0" OrElse
                                    CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "n" Then
                                    drCustom("Fld_Value") = 0
                                Else
                                    dataTableHeading.Rows(intHead)("IsErrorDetected") = True
                                    listErrors.Add("<b>" & strFieldName & "</b> (Column-" & dataTableHeading.Rows(intHead)("ImportFieldID") & ", Row-" & lngCSVRow & ") Contains an un-supported value (check boxes must be yes/no, true/false or 1/0).")
                                    Continue For
                                End If
                            ElseIf CCommon.ToString(dataTableHeading.Rows(intHead)("vcAssociatedControlType")).ToLower() = "selectbox" AndAlso CCommon.ToLong(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)) = 0 AndAlso Not String.IsNullOrEmpty(CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName & "_Actual")).Trim()) Then
                                dataTableHeading.Rows(intHead)("IsErrorDetected") = True
                                listErrors.Add("<b>" & strFieldName & "</b> (Column-" & dataTableHeading.Rows(intHead)("ImportFieldID") & ", Row-" & lngCSVRow & ") Contains values that don’t exist on mapped drop-down field.")
                                Continue For
                            Else
                                drCustom("Fld_Value") = dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)
                            End If

                            dtCustomField.Rows.Add(drCustom)
                            dtCustomField.AcceptChanges()
                        Else
                            If CCommon.ToString(dataTableHeading.Rows(intHead)("vcAssociatedControlType")).ToLower() = "checkbox" Then
                                If CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "true" OrElse
                                 CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "yes" OrElse
                                 CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "1" OrElse
                                 CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "y" Then
                                    objPropValue = CType(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName), Object)
                                ElseIf CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "false" OrElse
                                    CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "no" OrElse
                                    CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "0" OrElse
                                    CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "n" Then
                                    objPropValue = CType(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName), Object)
                                Else
                                    dataTableHeading.Rows(intHead)("IsErrorDetected") = True
                                    listErrors.Add("<b>" & strFieldName & "</b> (Column-" & dataTableHeading.Rows(intHead)("ImportFieldID") & ", Row-" & lngCSVRow & ") Contains an un-supported value (check boxes must be yes/no, true/false or 1/0).")
                                    Continue For
                                End If
                            ElseIf CCommon.ToString(dataTableHeading.Rows(intHead)("vcAssociatedControlType")).ToLower() = "selectbox" AndAlso CCommon.ToLong(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)) = 0 AndAlso Not String.IsNullOrEmpty(CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName & "_Actual")).Trim()) Then
                                dataTableHeading.Rows(intHead)("IsErrorDetected") = True
                                listErrors.Add("<b>" & strFieldName & "</b> (Column-" & dataTableHeading.Rows(intHead)("ImportFieldID") & ", Row-" & lngCSVRow & ") Contains values that don’t exist on mapped drop-down field.")
                                Continue For
                            Else
                                objPropValue = CType(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName), Object)
                            End If
                        End If

                        If strFormFieldID = 385 Then
                            objClass.ItemDetailID = CCommon.ToLong(objPropValue)
                            objClass.DomainID = lngDomainID
                            objClass.GetKitSubItemDetails()

                            If (tintImportType = 1 Or (tintImportType = 3 AndAlso Not String.IsNullOrEmpty(CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName & "_Actual")).Trim()))) AndAlso CCommon.ToLong(objClass.ItemDetailID) = 0 Then
                                dataTableHeading.Rows(intHead)("IsErrorDetected") = True
                                listErrors.Add("<b>" & strFieldName & "</b> (Column-" & dataTableHeading.Rows(intHead)("ImportFieldID") & ", Row-" & lngCSVRow & ") contains values that don’t exist.")
                                Continue For
                            End If
                        ElseIf strFormFieldID = 211 Then
                            Try
                                Dim objCom As New CCommon
                                objCom.DomainID = lngDomainID
                                Dim dtLinkingData As DataTable = objCom.GetRecordByLinkingID(20, 1, CCommon.ToString(objPropValue))
                            Catch ex As Exception
                                If tintImportType = 2 Or tintImportType = 3 Then
                                    dataTableHeading.Rows(intHead)("IsErrorDetected") = True
                                    listErrors.Add("<b>" & strFieldName & "</b> (Column-" & dataTableHeading.Rows(intHead)("ImportFieldID") & ", Row-" & lngCSVRow & ") contains values that don’t exist.")
                                    Continue For
                                End If
                            End Try
                        End If

                        If CCommon.ToString(strPropertyName).Length > 0 Then
                            SetProperty(objClass, strPropertyName, objPropValue)
                        End If
                    Next
                Catch ex As Exception
                    Dim errorMessage As String

                    If ex.Message.Contains("ITEM_DETAIL_ID_REQUIRED") Or (Not ex.InnerException Is Nothing AndAlso ex.InnerException.Message.Contains("ITEM_DETAIL_ID_REQUIRED")) Then
                        If listErrors.Where(Function(x) x.Contains("Item Detail ID is required for update.")).Count = 0 Then
                            listErrors.Add("(Row-" & lngCSVRow & ") Item Detail ID is required for update.")
                        End If
                    ElseIf ex.Message.Contains("INVALID_ITEM_DETAIL_ID") Or (Not ex.InnerException Is Nothing AndAlso ex.InnerException.Message.Contains("INVALID_ITEM_DETAIL_ID")) Then
                        If listErrors.Where(Function(x) x.Contains("Invalid item detail id.")).Count = 0 Then
                            listErrors.Add("(Row-" & lngCSVRow & ") Invalid item detail id.")
                        End If
                    ElseIf (Not ex.InnerException Is Nothing AndAlso ex.InnerException.Message.Contains("INVALID_ITEM")) Or ex.Message.Contains("INVALID_ITEM") Then
                        If listErrors.Where(Function(x) x.Contains("Item don't exists.")).Count = 0 Then
                            listErrors.Add("(Row-" & lngCSVRow & ") Item don't exists.")
                        End If
                    Else
                        If listErrors.Where(Function(x) x.Contains("Unknown error occured in pre import analysis.")).Count = 0 Then
                            listErrors.Add("(Row-" & lngCSVRow & ") Unknown error occured in pre import analysis.")
                        End If
                    End If
                End Try
            Next
        Catch ex As Exception
            listErrors.Add("Unknown error occured - " & ex.Message)
        End Try
    End Sub

    Private Sub ValidateImportAddressDetails(ByVal lngDomainID As Long, ByVal MasterID As Long, ByVal tintImportType As Short, ByVal dtDataHold As DataTable, ByVal dataTableHeading As DataTable, ByRef listErrors As System.Collections.Generic.List(Of String))
        Try
            Dim lngCSVRow As Integer = 0
            Dim strGetPrimaryColumnName As String = "AddressID"
            Dim strFieldName As String = ""
            Dim strFieldType As String = ""
            Dim intFieldMaxLength As Integer = 0
            Dim strFormFieldName As String = ""
            Dim strFormFieldID As Object = Nothing
            Dim vcAssociatedControlType As String = ""
            Dim strValueString As String = ""
            Dim strImportType As String = "Address Details"
            Dim PropertyItem As PropertyInfo = Nothing
            Dim objClass As Object = Nothing
            Dim strPropertyName As String
            Dim objItemSave As MethodInfo = Nothing
            Dim objItemValue As New Object

            If (tintImportType = 1 Or tintImportType = 3) AndAlso Not dtDataHold.Columns.Contains("529_numAddressID") Then
                listErrors.Add("<b>Address ID</b> is required but column does not exists in csv or not mapped.")
            End If

            If (tintImportType = 2 Or tintImportType = 3) AndAlso Not dtDataHold.Columns.Contains("531_numRecordID") Then
                listErrors.Add("<b>Record ID</b> is required but column does not exists in csv or not mapped.")
            End If

            If dtDataHold.Columns.Contains("529_numAddressID") Then
                dtDataHold.Columns("529_numAddressID").SetOrdinal(0)
            End If

            If dtDataHold.Columns.Contains("534_tintAddressOf") Then
                dtDataHold.Columns("534_tintAddressOf").SetOrdinal(1)
            End If

            Dim dtCustomField As New DataTable
            dtCustomField.Columns.Add("Fld_ID").SetOrdinal(0)
            dtCustomField.Columns.Add("Name").SetOrdinal(1)
            dtCustomField.Columns.Add("Fld_Value").SetOrdinal(2)
            dtCustomField.TableName = "CusFlds"
            dtCustomField.AcceptChanges()

            For intC As Integer = 0 To dtDataHold.Rows.Count - 1
                lngCSVRow = intC + 2

                objClass = New CContacts

                Dim dvTempToSort As DataView
                dvTempToSort = dataTableHeading.DefaultView
                dvTempToSort.Sort = "IsPrimary DESC"
                dataTableHeading = dvTempToSort.ToTable()

                Try
                    For intHead As Integer = 0 To dataTableHeading.Rows.Count - 1
                        If dataTableHeading.Rows(intHead)("dbFieldName") Is DBNull.Value OrElse dataTableHeading.Rows(intHead)("dbFieldName") = "" OrElse CCommon.ToBool(dataTableHeading.Rows(intHead)("IsErrorDetected")) Then
                            Continue For
                        End If

                        strFormFieldName = dataTableHeading.Rows(intHead)("dbFieldName")
                        strFormFieldID = dataTableHeading.Rows(intHead)("FormFieldID")
                        strPropertyName = dataTableHeading.Rows(intHead)("vcPropertyName")
                        strFieldName = CCommon.ToString(dataTableHeading.Rows(intHead)("FormFieldName"))
                        strFieldType = CCommon.ToString(dataTableHeading.Rows(intHead)("vcFieldDataType"))
                        intFieldMaxLength = CCommon.ToInteger(dataTableHeading.Rows(intHead)("intFieldMaxLength"))

                        If (strFieldType.ToLower() = "m" Or strFieldType.ToLower() = "n") AndAlso
                            (CCommon.ToString(dataTableHeading.Rows(intHead)("vcAssociatedControlType")).ToLower() = "textbox" Or CCommon.ToString(dataTableHeading.Rows(intHead)("vcAssociatedControlType")).ToLower() = "textarea" Or CCommon.ToString(dataTableHeading.Rows(intHead)("vcAssociatedControlType")).ToLower() = "label") AndAlso
                            Not String.IsNullOrEmpty(CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).Trim()) AndAlso
                            Not Double.TryParse(CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)), Nothing) Then
                            dataTableHeading.Rows(intHead)("IsErrorDetected") = True
                            listErrors.Add("<b>" & strFieldName & "</b> (Column-" & dataTableHeading.Rows(intHead)("ImportFieldID") & ", Row-" & lngCSVRow & ") is mapped to a numeric field, but contains non-numeric characters.")
                            Continue For
                        End If

                        If intFieldMaxLength > 0 AndAlso
                            Not String.IsNullOrEmpty(CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).Trim()) AndAlso
                            CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).Length > intFieldMaxLength Then
                            dataTableHeading.Rows(intHead)("IsErrorDetected") = True
                            listErrors.Add("<b>" & strFieldName & "</b> (Column-" & dataTableHeading.Rows(intHead)("ImportFieldID") & ", Row-" & lngCSVRow & ") contains " & CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).Length & " characters, but the destination field only supports " & intFieldMaxLength & ".")
                            Continue For
                        End If

                        Dim objPropValue As Object = Nothing

                        If CBool(dataTableHeading.Rows(intHead)("bitCustomField")) = True Then
                            Dim drCustom As DataRow
                            drCustom = dtCustomField.NewRow
                            drCustom("Fld_ID") = strFormFieldID
                            drCustom("Name") = strFormFieldName

                            If CCommon.ToString(dataTableHeading.Rows(intHead)("vcAssociatedControlType")).ToLower() = "checkbox" Then
                                If CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "true" OrElse
                                 CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "yes" OrElse
                                 CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "1" OrElse
                                 CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "y" Then
                                    drCustom("Fld_Value") = 1
                                ElseIf CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "false" OrElse
                                    CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "no" OrElse
                                    CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "0" OrElse
                                    CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "n" Then
                                    drCustom("Fld_Value") = 0
                                Else
                                    dataTableHeading.Rows(intHead)("IsErrorDetected") = True
                                    listErrors.Add("<b>" & strFieldName & "</b> (Column-" & dataTableHeading.Rows(intHead)("ImportFieldID") & ", Row-" & lngCSVRow & ") Contains an un-supported value (check boxes must be yes/no, true/false or 1/0).")
                                    Continue For
                                End If
                            ElseIf CCommon.ToString(dataTableHeading.Rows(intHead)("vcAssociatedControlType")).ToLower() = "selectbox" AndAlso CCommon.ToLong(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)) = 0 AndAlso Not String.IsNullOrEmpty(CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName & "_Actual")).Trim()) Then
                                dataTableHeading.Rows(intHead)("IsErrorDetected") = True
                                listErrors.Add("<b>" & strFieldName & "</b> (Column-" & dataTableHeading.Rows(intHead)("ImportFieldID") & ", Row-" & lngCSVRow & ") Contains values that don’t exist on mapped drop-down field.")
                                Continue For
                            Else
                                drCustom("Fld_Value") = dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)
                            End If

                            dtCustomField.Rows.Add(drCustom)
                            dtCustomField.AcceptChanges()
                        Else
                            If strFormFieldID = 532 Then
                                If CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower() <> "bill to" AndAlso CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower() <> "ship to" Then
                                    dataTableHeading.Rows(intHead)("IsErrorDetected") = True
                                    listErrors.Add("<b>" & strFieldName & "</b> (Column-" & dataTableHeading.Rows(intHead)("ImportFieldID") & ", Row-" & lngCSVRow & ") contains an un-supported value (must be bill to or ship to).")
                                    Continue For
                                End If
                            ElseIf strFormFieldID = 534 Then
                                If CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower() <> "organization" AndAlso CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower() <> "contact" Then
                                    dataTableHeading.Rows(intHead)("IsErrorDetected") = True
                                    listErrors.Add("<b>" & strFieldName & "</b> (Column-" & dataTableHeading.Rows(intHead)("ImportFieldID") & ", Row-" & lngCSVRow & ") contains an un-supported value (must be organization or contact).")
                                    Continue For
                                End If
                            End If

                            If CCommon.ToString(dataTableHeading.Rows(intHead)("vcAssociatedControlType")).ToLower() = "checkbox" Then
                                If CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "true" OrElse
                                 CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "yes" OrElse
                                 CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "1" OrElse
                                 CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "y" Then
                                    objPropValue = CType(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName), Object)
                                ElseIf CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "false" OrElse
                                    CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "no" OrElse
                                    CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "0" OrElse
                                    CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "n" Then
                                    objPropValue = CType(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName), Object)
                                Else
                                    dataTableHeading.Rows(intHead)("IsErrorDetected") = True
                                    listErrors.Add("<b>" & strFieldName & "</b> (Column-" & dataTableHeading.Rows(intHead)("ImportFieldID") & ", Row-" & lngCSVRow & ") Contains an un-supported value (check boxes must be yes/no, true/false or 1/0).")
                                    Continue For
                                End If
                            ElseIf CCommon.ToString(dataTableHeading.Rows(intHead)("vcAssociatedControlType")).ToLower() = "selectbox" AndAlso CCommon.ToLong(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)) = 0 AndAlso Not String.IsNullOrEmpty(CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName & "_Actual")).Trim()) Then
                                dataTableHeading.Rows(intHead)("IsErrorDetected") = True
                                listErrors.Add("<b>" & strFieldName & "</b> (Column-" & dataTableHeading.Rows(intHead)("ImportFieldID") & ", Row-" & lngCSVRow & ") Contains values that don’t exist on mapped drop-down field.")
                                Continue For
                            Else
                                objPropValue = CType(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName), Object)
                            End If
                        End If


                        If strPropertyName.Contains("AddressType") = True Then
                            If CCommon.ToString(objPropValue).ToLower().Contains("bill") Then
                                objPropValue = CContacts.enmAddressType.BillTo
                            Else
                                objPropValue = CContacts.enmAddressType.ShipTo
                            End If

                        ElseIf strPropertyName.Contains("AddresOf") = True Then
                            If CCommon.ToString(objPropValue).ToLower().Contains("org") Then
                                objPropValue = CContacts.enmAddressOf.Organization

                            ElseIf CCommon.ToString(objPropValue).ToLower().Contains("con") Then
                                objPropValue = CContacts.enmAddressOf.Contact
                                CType(objClass, CContacts).AddressType = CContacts.enmAddressType.None
                            End If
                        End If

                        If strFormFieldID = 531 Then
                            If tintImportType = 2 Or tintImportType = 3 Then
                                If CCommon.ToString(dtDataHold.Rows(intC)("534_tintAddressOf")).ToLower().Contains("org") Then
                                    Try
                                        Dim objCom As New CCommon
                                        objCom.DomainID = lngDomainID
                                        Dim dtLinkingData As DataTable = objCom.GetRecordByLinkingID(133, 2, CCommon.ToString(dtDataHold.Rows(intC)("531_RecordID")))
                                    Catch ex As Exception
                                        dataTableHeading.Rows(intHead)("IsErrorDetected") = True
                                        listErrors.Add("<b>" & strFieldName & "</b> (Column-" & dataTableHeading.Rows(intHead)("ImportFieldID") & ", Row-" & lngCSVRow & ") contains values that don’t exist.")
                                        Continue For
                                    End Try
                                ElseIf CCommon.ToString(dtDataHold.Rows(intC)("534_tintAddressOf")).ToLower().Contains("con") Then
                                    Try
                                        Dim objCom As New CCommon
                                        objCom.DomainID = lngDomainID
                                        Dim dtLinkingData As DataTable = objCom.GetRecordByLinkingID(134, 2, CCommon.ToString(dtDataHold.Rows(intC)("531_RecordID")))
                                    Catch ex As Exception
                                        dataTableHeading.Rows(intHead)("IsErrorDetected") = True
                                        listErrors.Add("<b>" & strFieldName & "</b> (Column-" & dataTableHeading.Rows(intHead)("ImportFieldID") & ", Row-" & lngCSVRow & ") contains values that don’t exist.")
                                        Continue For
                                    End Try
                                End If
                            End If
                        End If

                        If CCommon.ToString(strPropertyName).Length > 0 Then
                            SetProperty(objClass, strPropertyName, objPropValue)
                        End If
                    Next
                Catch ex As Exception
                    If ex.Message.Contains("ADDRESSID_REQUIRED") Or (Not ex.InnerException Is Nothing AndAlso ex.InnerException.Message.Contains("ADDRESSID_REQUIRED")) Then
                        If listErrors.Where(Function(x) x.Contains("Address ID is reqired to update address.")).Count = 0 Then
                            listErrors.Add("(Row-" & lngCSVRow & ") Address ID is reqired to update address.")
                        End If
                    ElseIf ex.Message.Contains("INVALID_ADDRESS_ID") Or (Not ex.InnerException Is Nothing AndAlso ex.InnerException.Message.Contains("INVALID_ADDRESS_ID")) Then
                        If listErrors.Where(Function(x) x.Contains("Invalid Address ID.")).Count = 0 Then
                            listErrors.Add("(Row-" & lngCSVRow & ") Invalid Address ID.")
                        End If
                    ElseIf (Not ex.InnerException Is Nothing AndAlso ex.InnerException.Message.Contains("ORGANIZATION_NOT_FOUND_WITH_GIVEN_ORGANIZATION ID")) Or ex.Message.Contains("ORGANIZATION_NOT_FOUND_WITH_GIVEN_ORGANIZATION ID") Then
                        If listErrors.Where(Function(x) x.Contains("No organization matched to record id for insert.")).Count = 0 Then
                            listErrors.Add("(Row-" & lngCSVRow & ") No organization matched to record id for insert.")
                        End If
                    ElseIf (Not ex.InnerException Is Nothing AndAlso ex.InnerException.Message.Contains("MULTIPLE_ORGANIZATIONS_FOUND_WITH_GIVEN_ORGANIZATION ID")) Or ex.Message.Contains("MULTIPLE_ORGANIZATIONS_FOUND_WITH_GIVEN_ORGANIZATION ID") Then
                        If listErrors.Where(Function(x) x.Contains("Multiple organizations matched to record id for insert.")).Count = 0 Then
                            listErrors.Add("(Row-" & lngCSVRow & ") Multiple organizations matched to record id for insert.")
                        End If
                    ElseIf (Not ex.InnerException Is Nothing AndAlso ex.InnerException.Message.Contains("CONTACT_NOT_FOUND_WITH_GIVEN_CONTACT ID")) Or ex.Message.Contains("CONTACT_NOT_FOUND_WITH_GIVEN_CONTACT ID") Then
                        If listErrors.Where(Function(x) x.Contains("No contact matched to record id for insert.")).Count = 0 Then
                            listErrors.Add("(Row-" & lngCSVRow & ") No contact matched to record id for insert.")
                        End If
                    ElseIf (Not ex.InnerException Is Nothing AndAlso ex.InnerException.Message.Contains("MULTIPLE_CONTACTS_FOUND_WITH_GIVEN_CONTACT ID")) Or ex.Message.Contains("MULTIPLE_CONTACTS_FOUND_WITH_GIVEN_CONTACT ID") Then
                        If listErrors.Where(Function(x) x.Contains("Multiple contacts matched to record id for insert.")).Count = 0 Then
                            listErrors.Add("(Row-" & lngCSVRow & ") Multiple contacts matched to record id for insert.")
                        End If
                    Else
                        listErrors.Add("(Row-" & lngCSVRow & ") Unknown error occured in pre import analysis.")
                    End If
                End Try
            Next
        Catch ex As Exception
            listErrors.Add("Unknown error occured - " & ex.Message)
        End Try
    End Sub

    Private Sub ValidateImportOrder(ByVal lngDomainID As Long,
                                    ByVal dtDataHold As DataTable,
                                    ByVal dataTableHeading As DataTable,
                                    ByVal oppType As Short,
                                    ByVal linkingID As Short,
                                    ByVal IsSingleOrder As Boolean,
                                    ByVal IsExistingOrganization As Boolean,
                                    ByVal DivisionID As Long,
                                    ByVal ContactID As Long,
                                    ByVal CompanyName As String,
                                    ByVal ContactFirstName As String,
                                    ByVal ContactLastName As String,
                                    ByVal IsMatchOrganizationID As Boolean,
                                    ByVal MatchFieldID As Long,
                                    ByRef listErrors As System.Collections.Generic.List(Of String))
        Try
            Dim lngCSVRow As Integer = 1
            Dim objItem As New CItems
            Dim objOrderImport As OrderImport

            If linkingID = 189 And Not dtDataHold.Columns.Contains("189_vcItemName") Then
                listErrors.Add("<b>Item Name</b> is selected to pick items but column does not exists or not mapped in csv.")
            ElseIf linkingID = 281 AndAlso Not dtDataHold.Columns.Contains("281_vcSKU") Then
                listErrors.Add("<b>SKU</b> is selected to pick items but column does not exists or not mapped in csv.")
            ElseIf linkingID = 203 AndAlso Not dtDataHold.Columns.Contains("203_numBarCodeId") Then
                listErrors.Add("<b>UPC</b> is selected to pick items but column does not exists or not mapped in csv.")
            ElseIf linkingID = 899 AndAlso Not dtDataHold.Columns.Contains("899_CustomerPartNo") Then
                listErrors.Add("<b>Customer Part#</b> is selected to pick items but column does not exists or not mapped in csv.")
            ElseIf linkingID = 211 AndAlso Not dtDataHold.Columns.Contains("211_numItemCode") Then
                listErrors.Add("<b>Item ID</b> is selected to pick items but column does not exists or not mapped in csv.")
            ElseIf linkingID = 291 AndAlso Not dtDataHold.Columns.Contains("291_vcPartNo") Then
                listErrors.Add("<b>Vendor Part#</b> is selected to pick items but column does not exists or not mapped in csv.")
            End If

            If Not dtDataHold.Columns.Contains("258_numUnitHour") Then
                listErrors.Add("<b>Units</b> is required field.")
            End If
            If Not dtDataHold.Columns.Contains("259_monPrice") Then
                listErrors.Add("<b>Unit Price</b> is required field.")
            End If
            If Not dtDataHold.Columns.Contains("293_numWarehouseID") Then
                listErrors.Add("<b>Warehouse</b> is required field.")
            End If

            If IsSingleOrder Then
                If IsExistingOrganization Then
                    If DomainID = 0 Or ContactID = 0 Then
                        listErrors.Add("<b>Organization and Contact</b> are required when order needs to be imported for existing organization in step 1 before uploading csv.")
                    End If
                Else
                    If String.IsNullOrEmpty(CompanyName) Or Not String.IsNullOrEmpty(ContactFirstName) Or Not String.IsNullOrEmpty(ContactLastName) Then
                        listErrors.Add("<b>Organization Name, Contact First & Last Name</b> are required when order needs to be imported for new organization in step 1 before uploading csv.")
                    End If
                End If
            Else
                If IsMatchOrganizationID Then
                    If Not dtDataHold.Columns.Contains("539_numDivisionID") Then
                        listErrors.Add("<b>Organization ID</b> is required when it is used to pick customer to import order(s).")
                    End If
                Else
                    If MatchFieldID = 0 Then
                        listErrors.Add("Second match field with Organization Name is required to pick customer to import order(s).")
                    ElseIf Not dtDataHold.Columns.Contains("3_vcCompanyName") Then
                        listErrors.Add("<b>Organization Name</b> is required to pick customer to import order(s).")
                    Else
                        Dim isMatchFieldMapped As Boolean = False

                        For Each column As DataColumn In dtDataHold.Columns
                            If column.ColumnName.StartsWith(MatchFieldID & "_") Then
                                isMatchFieldMapped = True
                                Exit For
                            End If
                        Next

                        If Not isMatchFieldMapped Then
                            listErrors.Add("<b>Organization and Contact</b> are not mapped in step 2.")
                        End If
                    End If
                End If
            End If

            Dim dtAccount As DataTable
            Dim listInvalidCustomer As New System.Collections.Generic.List(Of String)
            Dim listInvalidItems As New System.Collections.Generic.List(Of String)
            Dim listValidCustomers As New System.Collections.Generic.List(Of Tuple(Of String, Long, Long))

            For Each dr As DataRow In dtDataHold.Rows
                lngCSVRow = lngCSVRow + 1

                Try
                    If Not IsSingleOrder Then
                        If IsMatchOrganizationID Then
                            If listInvalidCustomer.Where(Function(x) x = CCommon.ToString(dr("539_numDivisionID"))).Count = 0 Then
                                If Not listValidCustomers.Exists(Function(x) x.Item1 = CCommon.ToString(dr("539_numDivisionID"))) Then
                                    Dim objAccount As New CAccounts
                                    objAccount.DivisionID = CCommon.ToLong(dr("539_numDivisionID"))
                                    dtAccount = objAccount.GetDefaultSettingValue("numPrimaryContact")

                                    If Not dtAccount Is Nothing AndAlso dtAccount.Rows.Count > 0 Then
                                        objOrderImport = New OrderImport
                                        objOrderImport.DivisionID = CCommon.ToLong(dr("539_numDivisionID"))
                                        objOrderImport.ContactID = CCommon.ToLong(dtAccount.Rows(0)("numContactID"))
                                        listValidCustomers.Add(New Tuple(Of String, Long, Long)(CCommon.ToString(dr("539_numDivisionID")), objOrderImport.DivisionID, objOrderImport.ContactID))
                                    Else
                                        listInvalidCustomer.Add(CCommon.ToString(dr("539_numDivisionID")))
                                        If listErrors.Where(Function(x) x.Contains("Customer don't exists.")).Count = 0 Then
                                            listErrors.Add("(Row-" & lngCSVRow & ") Customer don't exists.")
                                        End If
                                    End If
                                End If
                            Else
                                If listErrors.Where(Function(x) x.Contains("Customer don't exists.")).Count = 0 Then
                                    listErrors.Add("(Row-" & lngCSVRow & ") Customer don't exists.")
                                End If
                            End If
                        Else
                            Dim vcMatchFieldValue As String = ""

                            Select Case MatchFieldID
                                Case 14
                                    vcMatchFieldValue = CCommon.ToString(dr("14_numAssignedTo"))
                                Case 28
                                    vcMatchFieldValue = CCommon.ToString(dr("28_numCompanyIndustry"))
                                Case 539
                                    vcMatchFieldValue = CCommon.ToString(dr("539_numDivisionID"))
                                Case 3
                                    vcMatchFieldValue = CCommon.ToString(dr("3_vcCompanyName"))
                                Case 10
                                    vcMatchFieldValue = CCommon.ToString(dr("10_vcComPhone"))
                                Case 5
                                    vcMatchFieldValue = CCommon.ToString(dr("5_vcProfile"))
                                Case 30
                                    vcMatchFieldValue = CCommon.ToString(dr("30_numStatusID"))
                                Case 6
                                    vcMatchFieldValue = CCommon.ToString(dr("6_numCompanyType"))
                                Case 451
                                    vcMatchFieldValue = CCommon.ToString(dr("451_tintCRMType"))
                                Case 21
                                    vcMatchFieldValue = CCommon.ToString(dr("21_numTerID"))
                                Case 535
                                    vcMatchFieldValue = CCommon.ToString(dr("535_vcOppRefOrderNo"))
                            End Select

                            If listInvalidCustomer.Where(Function(x) x = CCommon.ToString(dr("3_vcCompanyName")) & "#" & vcMatchFieldValue).Count = 0 Then
                                If Not listValidCustomers.Exists(Function(x) x.Item1 = CCommon.ToString(dr("3_vcCompanyName")) & "#" & vcMatchFieldValue) Then
                                    Try
                                        Dim objAccount As New CAccounts
                                        objAccount.DomainID = lngDomainID
                                        dtAccount = objAccount.OrderImportGetCustomer(CCommon.ToString(dr("3_vcCompanyName")), MatchFieldID, vcMatchFieldValue)

                                        If Not dtAccount Is Nothing AndAlso dtAccount.Rows.Count > 0 Then
                                            objOrderImport = New OrderImport
                                            objOrderImport.MatchID = CCommon.ToString(dr("3_vcCompanyName")) & "#" & vcMatchFieldValue
                                            objOrderImport.DivisionID = CCommon.ToLong(dtAccount.Rows(0)("numDivisionID"))
                                            objOrderImport.ContactID = CCommon.ToLong(dtAccount.Rows(0)("numContactID"))
                                            listValidCustomers.Add(New Tuple(Of String, Long, Long)(CCommon.ToString(dr("3_vcCompanyName")) & "#" & vcMatchFieldValue, objOrderImport.DivisionID, objOrderImport.ContactID))
                                        Else
                                            listInvalidCustomer.Add(CCommon.ToString(dr("3_vcCompanyName")) & "#" & vcMatchFieldValue)
                                            If listErrors.Where(Function(x) x.Contains("Customer don't exists.")).Count = 0 Then
                                                listErrors.Add("(Row-" & lngCSVRow & ") Customer don't exists.")
                                            End If
                                            Continue For
                                        End If
                                    Catch ex As Exception
                                        listInvalidCustomer.Add(CCommon.ToString(dr("3_vcCompanyName")) & "#" & vcMatchFieldValue)
                                        If listErrors.Where(Function(x) x.Contains("Customer don't exists.")).Count = 0 Then
                                            listErrors.Add("(Row-" & lngCSVRow & ") Customer don't exists.")
                                        End If
                                        Continue For
                                    End Try
                                End If
                            Else
                                If listErrors.Where(Function(x) x.Contains("Customer don't exists.")).Count = 0 Then
                                    listErrors.Add("(Row-" & lngCSVRow & ") Customer don't exists.")
                                End If
                                Continue For
                            End If
                        End If
                    Else
                        objOrderImport = New OrderImport
                        objOrderImport.DivisionID = CCommon.ToLong(DivisionID)
                        objOrderImport.ContactID = CCommon.ToLong(ContactID)
                    End If

                    Dim searchField As String = ""
                    Dim searchText As String = ""
                    If linkingID = 189 Then
                        searchText = CCommon.ToString(dr("189_vcItemName"))
                        searchField = "Item Name"
                    ElseIf linkingID = 281 Then
                        searchText = CCommon.ToString(dr("281_vcSKU"))
                        searchField = "SKU"
                    ElseIf linkingID = 203 Then
                        searchText = CCommon.ToString(dr("203_numBarCodeId"))
                        searchField = "UPC"
                    ElseIf linkingID = 899 Then
                        searchText = CCommon.ToString(dr("899_CustomerPartNo"))
                        searchField = "Customer Part#"
                    ElseIf linkingID = 211 Then
                        searchText = CCommon.ToString(dr("211_numItemCode"))
                        searchField = "Item ID"
                    ElseIf linkingID = 291 Then
                        searchText = CCommon.ToString(dr("291_vcPartNo"))
                        searchField = "Vendor Part#"
                    Else
                        If listErrors.Where(Function(x) x.Contains("Field not selected to pick item(s) for order.")).Count = 0 Then
                            listErrors.Add("(Row-" & lngCSVRow & ") Field not selected to pick item(s) for order.")
                        End If
                        Continue For
                    End If

                    objItem = New CItems
                    Dim dtBizItem As DataTable = objItem.SearchItemByFieldAndText(Session("DomainID"), objOrderImport.DivisionID, linkingID, searchText)

                    If Not (Not dtBizItem Is Nothing AndAlso dtBizItem.Rows.Count > 0) Then
                        If listErrors.Where(Function(x) x.Contains("Item don't Exists")).Count = 0 Then
                            listErrors.Add("(Row-" & lngCSVRow & ") Item don't Exists")
                        End If
                        Continue For
                    End If

                    Dim isValidData As Boolean

                    For Each drHead As DataRow In dataTableHeading.Rows
                        isValidData = True

                        Dim fieldID As String = CCommon.ToString(drHead("FormFieldID"))
                        Dim dbColumnName As String = CCommon.ToString(drHead("dbFieldName"))
                        Dim strFieldName As String = CCommon.ToString(drHead("FormFieldName"))
                        Dim strFieldType As String = CCommon.ToString(drHead("vcFieldDataType"))
                        Dim intFieldMaxLength As Integer = CCommon.ToInteger(drHead("intFieldMaxLength"))

                        If (strFieldType.ToLower() = "m" Or strFieldType.ToLower() = "n") AndAlso
                            (CCommon.ToString(drHead("vcAssociatedControlType")).ToLower() = "textbox" Or CCommon.ToString(drHead("vcAssociatedControlType")).ToLower() = "textarea" Or CCommon.ToString(drHead("vcAssociatedControlType")).ToLower() = "label") AndAlso
                            Not String.IsNullOrEmpty(CCommon.ToString(dr(fieldID & "_" & dbColumnName)).Trim()) AndAlso
                            Not Double.TryParse(CCommon.ToString(dr(fieldID & "_" & dbColumnName)), Nothing) Then
                            drHead("IsErrorDetected") = True
                            listErrors.Add("<b>" & strFieldName & "</b> (Column-" & drHead("ImportFieldID") & ", Row-" & lngCSVRow & ") is mapped to a numeric field, but contains non-numeric characters.")
                            Continue For
                        End If

                        If intFieldMaxLength > 0 AndAlso
                            Not String.IsNullOrEmpty(CCommon.ToString(dr(fieldID & "_" & dbColumnName)).Trim()) AndAlso
                            CCommon.ToString(dr(fieldID & "_" & dbColumnName)).Length > intFieldMaxLength Then
                            drHead("IsErrorDetected") = True
                            listErrors.Add("<b>" & strFieldName & "</b> (Column-" & drHead("ImportFieldID") & ", Row-" & lngCSVRow & ") contains " & CCommon.ToString(dr(fieldID & "_" & dbColumnName)).Length & " characters, but the destination field only supports " & intFieldMaxLength & ".")
                            Continue For
                        End If

                        If Not CCommon.ToBool(drHead("IsErrorDetected")) Then
                            Select Case CCommon.ToLong(drHead("FormFieldID"))
                                Case 258
                                    If String.IsNullOrEmpty(CCommon.ToString(dr(fieldID & "_" & dbColumnName & "_Actual")).Trim()) Then
                                        drHead("IsErrorDetected") = True
                                        listErrors.Add("<b>Units</b> (Column-" & drHead("ImportFieldID") & ", Row-" & lngCSVRow & ") Cell is empty and must be populated because it’s mapped to a required field.")
                                        Continue For
                                    End If
                                Case 259
                                    If String.IsNullOrEmpty(CCommon.ToString(dr(fieldID & "_" & dbColumnName & "_Actual")).Trim()) Then
                                        drHead("IsErrorDetected") = True
                                        listErrors.Add("<b>Unit Price</b> (Column-" & drHead("ImportFieldID") & ", Row-" & lngCSVRow & ") Cell is empty and must be populated because it’s mapped to a required field.")
                                        Continue For
                                    End If
                                Case 293 'Warehouse Name
                                    If CCommon.ToLong(dr(fieldID & "_" & dbColumnName)) = 0 Then
                                        drHead("IsErrorDetected") = True
                                        listErrors.Add("<b>Warehouse</b> (Column-" & drHead("ImportFieldID") & ", Row-" & lngCSVRow & ") don't exists.")
                                        Continue For
                                    End If
                                Case 870 'Inclusion Details (ID may be different in your local database)
                                    Try
                                        objItem.ValidateKitChildsOrderImport(Session("DomainID"), CCommon.ToLong(dtBizItem.Rows(0)("numItemCode")), CCommon.ToString(dr(fieldID & "_" & dbColumnName)))
                                    Catch ex As Exception
                                        drHead("IsErrorDetected") = True

                                        If ex.Message.Contains("INVALID_INCLISION_DETAIL") Then
                                            listErrors.Add("<b>Inclusion Details</b> (Column-" & drHead("ImportFieldID") & ", Row-" & lngCSVRow & ") Invalid inclusion detail.")
                                            Continue For
                                        ElseIf ex.Message.Contains("CHILD_KIT_VALUES_NOT_PROVIDED") Then
                                            listErrors.Add("<b>Inclusion Details</b> (Column-" & drHead("ImportFieldID") & ", Row-" & lngCSVRow & ") Required child kits are not included.")
                                            Continue For
                                        ElseIf ex.Message.Contains("KIT_ITEM_NOT_FOUND") Then
                                            listErrors.Add("<b>Inclusion Details</b> (Column-" & drHead("ImportFieldID") & ", Row-" & lngCSVRow & ") Kit item not found.")
                                            Continue For
                                        ElseIf ex.Message.Contains("MULTIPLE_KIT_ITEM_FOUND") Then
                                            listErrors.Add("<b>Inclusion Details</b> (Column-" & drHead("ImportFieldID") & ", Row-" & lngCSVRow & ") Multiple items found with kit item.")
                                            Continue For
                                        ElseIf ex.Message.Contains("KIT_CHILD_ITEM_NOT_FOUND") Then
                                            listErrors.Add("<b>Inclusion Details</b> (Column-" & drHead("ImportFieldID") & ", Row-" & lngCSVRow & ") Kit child item not found.")
                                            Continue For
                                        ElseIf ex.Message.Contains("MULTIPLE_KIT_CHILD_ITEM_FOUND") Then
                                            listErrors.Add("<b>Inclusion Details</b> (Column-" & drHead("ImportFieldID") & ", Row-" & lngCSVRow & ") Multiple items found wiht kit child item.")
                                            Continue For
                                        ElseIf ex.Message.Contains("INVALID_KIT_ITEM") Then
                                            listErrors.Add("<b>Inclusion Details</b> (Column-" & drHead("ImportFieldID") & ", Row-" & lngCSVRow & ") Invlaid kit item.")
                                            Continue For
                                        ElseIf ex.Message.Contains("INVALID_KIT_CHILD_ITEM") Then
                                            listErrors.Add("<b>Inclusion Details</b> (Column-" & drHead("ImportFieldID") & ", Row-" & lngCSVRow & ") Invlaid kit child item.")
                                            Continue For
                                        Else
                                            listErrors.Add("<b>Inclusion Details</b> (Column-" & drHead("ImportFieldID") & ", Row-" & lngCSVRow & ") Unknow error occured in validating inclusion details")
                                            Continue For
                                        End If
                                    End Try

                            End Select
                        Else
                            Continue For
                        End If
                    Next
                Catch ex As Exception
                    listErrors.Add("(Row-" & lngCSVRow & ") Unknown error occured - " & ex.Message)
                End Try
            Next
        Catch ex As Exception
            listErrors.Add("Unknown error occured - " & ex.Message)
        End Try
    End Sub

    Private Sub ValidateImportAccountingEntries(ByVal lngDomainID As Long, ByVal dtDataHold As DataTable, ByVal dataTableHeading As DataTable, ByRef listErrors As System.Collections.Generic.List(Of String))
        Try
            Dim strImportType As String = "Accounting Entries"
            Dim strAddedJournalIds As String = ""
            Dim listCSVRows As New System.Collections.Generic.List(Of Tuple(Of Integer, Boolean, String))
            Dim listValidCustomers As New System.Collections.Generic.List(Of Tuple(Of String, Long))
            Dim listInvalidCustomer As New System.Collections.Generic.List(Of String)
            Dim lngCSVRow As Long = 1

            Dim isValidConfiguration As Boolean = True
            Dim errorMessage As String = ""

            If Not dtDataHold.Columns.Contains("931_datEntry_Date") Then
                listErrors.Add("<b>Date</b> is required field.")
            End If
            If Not dtDataHold.Columns.Contains("932_numChartAcntId") Then
                listErrors.Add("<b>Chart of Accounts to Credit</b> is required field.")
            End If
            If Not dtDataHold.Columns.Contains("933_numChartAcntId") Then
                listErrors.Add("<b>Chart of Accounts to Debit</b> is required field.")
            End If
            If Not dtDataHold.Columns.Contains("934_numAmount") Then
                listErrors.Add("<b>Amount</b> is required field.")
            End If

            Dim entryDate As Date
            Dim dtAccount As DataTable

            For Each dr As DataRow In dtDataHold.Rows
                lngCSVRow = lngCSVRow + 1

                Try
                    For Each drHead As DataRow In dataTableHeading.Rows
                        Dim fieldID As String = CCommon.ToString(drHead("FormFieldID"))
                        Dim dbColumnName As String = CCommon.ToString(drHead("dbFieldName"))
                        Dim strFieldName As String = CCommon.ToString(drHead("FormFieldName"))
                        Dim strFieldType As String = CCommon.ToString(drHead("vcFieldDataType"))
                        Dim intFieldMaxLength As Integer = CCommon.ToInteger(drHead("intFieldMaxLength"))

                        If (strFieldType.ToLower() = "m" Or strFieldType.ToLower() = "n") AndAlso
                            (CCommon.ToString(drHead("vcAssociatedControlType")).ToLower() = "textbox" Or CCommon.ToString(drHead("vcAssociatedControlType")).ToLower() = "textarea" Or CCommon.ToString(drHead("vcAssociatedControlType")).ToLower() = "label") AndAlso
                            Not String.IsNullOrEmpty(CCommon.ToString(dr(fieldID & "_" & dbColumnName)).Trim()) AndAlso
                            Not Double.TryParse(CCommon.ToString(dr(fieldID & "_" & dbColumnName)), Nothing) Then
                            drHead("IsErrorDetected") = True
                            listErrors.Add("<b>" & strFieldName & "</b> (Column-" & drHead("ImportFieldID") & ", Row-" & lngCSVRow & ") is mapped to a numeric field, but contains non-numeric characters.")
                            Continue For
                        End If

                        If intFieldMaxLength > 0 AndAlso
                            Not String.IsNullOrEmpty(CCommon.ToString(dr(fieldID & "_" & dbColumnName)).Trim()) AndAlso
                            CCommon.ToString(dr(fieldID & "_" & dbColumnName)).Length > intFieldMaxLength Then
                            drHead("IsErrorDetected") = True
                            listErrors.Add("<b>" & strFieldName & "</b> (Column-" & drHead("ImportFieldID") & ", Row-" & lngCSVRow & ") contains " & CCommon.ToString(dr(fieldID & "_" & dbColumnName)).Length & " characters, but the destination field only supports " & intFieldMaxLength & ".")
                            Continue For
                        End If

                        Select Case CCommon.ToLong(drHead("FormFieldID"))
                            Case 3 'Vendor
                                If Not String.IsNullOrEmpty(CCommon.ToString(dr(fieldID & "_" & dbColumnName)).Trim()) Then
                                    If listInvalidCustomer.Exists(Function(x) x.ToLower() = CCommon.ToString(dr(fieldID & "_" & dbColumnName)).ToLower()) Then
                                        Continue For
                                    ElseIf Not listValidCustomers.Exists(Function(x) x.Item1.ToLower() = CCommon.ToString(dr(fieldID & "_" & dbColumnName)).ToLower()) Then
                                        Dim objAccount As New CAccounts
                                        objAccount.DomainID = lngDomainID
                                        dtAccount = objAccount.OrderImportGetCustomer(CCommon.ToString(dr("3_vcCompanyName")), 3, "")

                                        If Not dtAccount Is Nothing AndAlso dtAccount.Rows.Count > 0 Then
                                            listValidCustomers.Add(New Tuple(Of String, Long)(CCommon.ToString(dr("3_vcCompanyName")), CCommon.ToLong(dtAccount.Rows(0)("numDivisionID"))))
                                        Else
                                            drHead("IsErrorDetected") = True
                                            listInvalidCustomer.Add(CCommon.ToString(dr("3_vcCompanyName")))
                                            listErrors.Add("<b>Vendor</b> (Column-" & drHead("ImportFieldID") & ", Row-" & lngCSVRow & ") don't exists.")
                                            Continue For
                                        End If
                                    End If
                                End If
                            Case 931 'Date
                                If String.IsNullOrEmpty(dr(fieldID & "_" & dbColumnName)) Then
                                    drHead("IsErrorDetected") = True
                                    listErrors.Add("<b>Date</b> (Column-" & drHead("ImportFieldID") & ", Row-" & lngCSVRow & ") is invalid.")
                                    Continue For
                                ElseIf Not DateTime.TryParseExact(CCommon.ToString(dr(fieldID & "_" & dbColumnName)), "MM/dd/yyyy", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, entryDate) Then
                                    drHead("IsErrorDetected") = True
                                    listErrors.Add("<b>Date</b> (Column-" & drHead("ImportFieldID") & ", Row-" & lngCSVRow & ") is invalid.")
                                    Continue For
                                End If
                            Case 932 'Chart of Accounts to Credit
                                If CCommon.ToLong(dr(fieldID & "_" & dbColumnName)) = 0 Then
                                    listErrors.Add("<b>Chart of Accounts to Credit</b> (Column-" & drHead("ImportFieldID") & ", Row-" & lngCSVRow & ") is invalid.")
                                    Continue For
                                End If
                            Case 933 'Chart of Accounts to Debit
                                If CCommon.ToLong(dr(fieldID & "_" & dbColumnName)) = 0 Then
                                    listErrors.Add("<b>Chart of Accounts to Debit</b> (Column-" & drHead("ImportFieldID") & ", Row-" & lngCSVRow & ") is invalid.")
                                    Continue For
                                End If
                        End Select
                    Next
                Catch ex As Exception
                    listErrors.Add("(Row-" & lngCSVRow & ") Unknown error occured - " & ex.Message)
                End Try
            Next
        Catch ex As Exception
            listErrors.Add("Unknown error occured - " & ex.Message)
        End Try
    End Sub

    Private Function SetValues(ByVal strSplitString As String, ByVal splitChar As String, ByVal dtTable As DataTable) As String
        Dim strResult As String = ""
        Try
            For Each item As String In strSplitString.Split(splitChar)
                Dim drRow() As DataRow = dtTable.Select("Value ='" & item.Trim() & "'")
                If drRow.Length > 0 Then
                    strResult = CCommon.ToString(drRow(0)("Key")).Trim() & "," & strResult
                End If
            Next

            strResult = strResult.TrimEnd(",")
        Catch ex As Exception
            Throw ex
        End Try

        Return strResult
    End Function

    Public Sub SetProperty(ByRef objClassObject As Object, ByVal sPropertyName As String, ByVal sPropertyValue As String)
        Try
            Dim theType As Type = objClassObject.GetType
            Dim PropertyItem As PropertyInfo = theType.GetProperty(sPropertyName, BindingFlags.Public Or BindingFlags.Instance)
            Dim fillObject As Object = objClassObject

            With PropertyItem
                If PropertyItem.Name = sPropertyName Then                                           'Compare the class property name with that of the db column name
                    Select Case PropertyItem.PropertyType.ToString()                                'Selecting the datataype of teh property
                        Case "System.String" : PropertyItem.SetValue(fillObject, sPropertyValue, Nothing)

                        Case "System.Int64"                          'Integer type properties
                            If IsNumeric(sPropertyValue) Then PropertyItem.SetValue(fillObject, CCommon.ToLong(sPropertyValue), Nothing) 'Typecasting to Int64 before setting the value

                        Case "System.Int32"                          'Integer type properties
                            If IsNumeric(sPropertyValue) Then PropertyItem.SetValue(fillObject, CCommon.ToInteger(sPropertyValue), Nothing) 'Typecasting to Int32 before setting the value

                        Case "System.Int16"                          'Integer type(Short - Int16) properties
                            If sPropertyValue.ToString().ToLower().Contains("yes") OrElse
                               sPropertyValue.ToString().ToLower().Contains("y") OrElse
                               sPropertyValue.ToString().ToLower().Contains("true") OrElse
                               sPropertyValue.ToString().ToLower().Contains("1") Then
                                PropertyItem.SetValue(fillObject, CShort(1), Nothing)

                            ElseIf sPropertyValue.ToString().ToLower().Contains("no") OrElse
                                   sPropertyValue.ToString().ToLower().Contains("n") OrElse
                                   sPropertyValue.ToString().ToLower().Contains("false") OrElse
                                   sPropertyValue.ToString().ToLower().Contains("0") Then
                                PropertyItem.SetValue(fillObject, CShort(0), Nothing)

                            Else
                                If IsNumeric(sPropertyValue) Then PropertyItem.SetValue(fillObject, CShort(sPropertyValue), Nothing) 'Typecasting to integer before setting the value

                            End If

                        Case "System.Double"                                                        'Double type properties
                            If IsNumeric(sPropertyValue) Then PropertyItem.SetValue(fillObject, CDbl(sPropertyValue), Nothing) 'Typecasting to Double before setting the value

                        Case "System.Decimal"                                                        'Double type properties
                            If IsNumeric(sPropertyValue) Then PropertyItem.SetValue(fillObject, CDec(sPropertyValue), Nothing) 'Typecasting to Decimal before setting the value

                        Case "System.DateTime"                                                        'Date type properties
                            If IsDate(sPropertyValue) Then PropertyItem.SetValue(fillObject, CDate(sPropertyValue), Nothing) 'Typecasting to DateTime before setting the value

                        Case "System.Boolean"                                                       'Boolean properties
                            If sPropertyValue.ToString().ToLower() = "yes" OrElse
                                sPropertyValue.ToString().ToLower() = "y" OrElse
                                sPropertyValue.ToString().ToLower() = "true" OrElse
                                sPropertyValue.ToString().ToLower() = "1" Then

                                sPropertyValue = True
                                If sPropertyName.Trim.ToLower() = "NoTax".ToLower() Then
                                    sPropertyValue = False
                                End If
                            Else
                                sPropertyValue = False
                                If sPropertyName.Trim.ToLower() = "NoTax".ToLower() Then
                                    sPropertyValue = True
                                End If
                            End If

                            If IsBoolean(sPropertyValue) Then PropertyItem.SetValue(fillObject, CBool(sPropertyValue), Nothing) 'Typecasting to boolean before setting the value

                        Case "BACRM.BusinessLogic.Contacts.CContacts+enmAddressType"
                            PropertyItem.SetValue(fillObject, CType(sPropertyValue, BACRM.BusinessLogic.Contacts.CContacts.enmAddressType), Nothing)

                        Case "BACRM.BusinessLogic.Contacts.CContacts+enmAddressOf"
                            PropertyItem.SetValue(fillObject, CType(sPropertyValue, BACRM.BusinessLogic.Contacts.CContacts.enmAddressOf), Nothing)

                    End Select
                    Exit Sub
                End If
            End With
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Function IsBoolean(ByVal sTrg As String) As Boolean
        Try
            Dim intValue As Boolean = Convert.ToBoolean(sTrg)                                   'Try Casting to a boolean
        Catch Ex As InvalidCastException
            Return False                                                                        'if it throws an error then return false to indicate that it is non boolean
        End Try
        Return True                                                                             'Test passed, its booelan
    End Function

    Private Class OrderImport
        Public Property MatchID As String

        Public Property DivisionID As Long
        Public Property ContactID As Long
        Public Property AssignedTo As Long
        Public Property OrderStatus As Long
        Public Property CustomerPO As String

        Public Property CompanyName As String
        Public Property ContactFirstName As String
        Public Property ContactLastName As String
        Public Property ContactEmail As String

        Public Property BillToStreet As String
        Public Property BillToCity As String
        Public Property BillToState As Long
        Public Property BillToPostalCode As String
        Public Property BillToCountry As Long

        Public Property ShipToStreet As String
        Public Property ShipToCity As String
        Public Property ShipToState As Long
        Public Property ShipToPostalCode As String
        Public Property ShipToCountry As Long

        Public Property ShipVia As Long
        Public Property ShippingService As Long

        Public Property IsValid As Boolean
        Public Property ErrorMessage As String
    End Class

    Private Sub btnOK_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnOK.Click
        Try
            Dim dtMappedData As DataTable
            dtMappedData = DirectCast(ViewState(_VW_MAPPED_DATATABLE), DataTable)

            If sMode = 0 AndAlso dtMappedData.Rows.Count <= 0 Then Exit Sub
            SaveMappedData(dtMappedData)

            ViewState(_VW_FILE_NAME) = Nothing
            ViewState(_VW_MAPPED_DATATABLE) = Nothing

            Response.Redirect("../admin/frmImportList.aspx", False)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub
End Class