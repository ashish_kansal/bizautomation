Imports System.Web.Services
Imports BACRM.BusinessLogic.Common
Imports Newtonsoft.Json
Imports BACRM.BusinessLogic.Item

Namespace BACRM.UserInterface.Admin

    Partial Public Class frmMasterList
        Inherits BACRMPage

        Dim m_aryRightsForABDocs(), m_aryRightsForFollow(), m_aryRightsForProfile(), m_aryRightsForSPR(), m_aryRightsForTerr() As Integer


        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            Try
                DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ""
                DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "none")
                GetUserRightsForPage(13, 1)

                If m_aryRightsForPage(RIGHTSTYPE.ADD) = 0 Then
                    btnAdd.Visible = False
                End If
                If Not IsPostBack Then


                    m_aryRightsForFollow = GetUserRightsForPage_Other(13, 21)

                    m_aryRightsForSPR = GetUserRightsForPage_Other(13, 23)
                    If m_aryRightsForSPR(RIGHTSTYPE.VIEW) = 0 Then
                        btnState.Visible = False
                    End If
                    BindModule()
                    ddlRModule_SelectedIndexChanged(sender, e)
                    ddlModule.DataSource = objCommon.GetListModule()
                    ddlModule.DataTextField = "vcModuleName"
                    ddlModule.DataValueField = "numModuleID"
                    ddlModule.DataBind()

                    If GetQueryStringVal("ModuleId") <> "" Then
                        Dim strModuleId As String = GetQueryStringVal("ModuleId")
                        If ddlModule.Items.FindByValue(strModuleId) IsNot Nothing Then
                            ddlModule.ClearSelection()
                            ddlModule.Items.FindByValue(strModuleId).Selected = True
                        End If
                    End If

                    objCommon.LoadListMaster(ddlMasterList, Session("DomainID"), ddlModule.SelectedValue)

                    If GetQueryStringVal("ListId") <> "" Then
                        Dim strListId As String = GetQueryStringVal("ListId")
                        If ddlMasterList.Items.FindByValue(strListId) IsNot Nothing Then
                            ddlMasterList.ClearSelection()
                            ddlMasterList.Items.FindByValue(strListId).Selected = True
                            ddlMasterList_SelectedIndexChanged(Nothing, Nothing)
                        End If
                    End If

                    BindGrid()
                End If

                btnState.Attributes.Add("onclick", "return State()")
                btnOpenDropdoel.Attributes.Add("onclick", "return OpenDropdownRel()")
                btnSort.Attributes.Add("onclick", "return Sort()")

                If ddlMasterList.SelectedValue = "35" Then
                    lblItemLabel.Text = "Work Center:"
                Else
                    lblItemLabel.Text = "Item Name:"
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Sub DisplayError(ByVal ex As String)
            DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ex
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
        End Sub
        Private Sub ddlRModule_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlRModule.SelectedIndexChanged
            Try
                If CCommon.ToLong(ddlRModule.SelectedValue) = 14 Then
                    lblParent.Text = "Select Parent"
                    lblChild.Text = "Select Child"

                    Dim objItem As New CItems
                    objItem.DomainID = CCommon.ToLong(Session("DomainID"))
                    Dim dt As DataTable = objItem.GetKits()

                    If Not dt Is Nothing AndAlso dt.Rows.Count > 1 Then
                        ddlPrimaryDropDown.DataSource = dt
                        ddlPrimaryDropDown.DataTextField = "vcItemName"
                        ddlPrimaryDropDown.DataValueField = "numItemCode"
                        ddlPrimaryDropDown.DataBind()

                        ddlSecondaryDropDown.DataSource = dt
                        ddlSecondaryDropDown.DataTextField = "vcItemName"
                        ddlSecondaryDropDown.DataValueField = "numItemCode"
                        ddlSecondaryDropDown.DataBind()

                        ddlPrimaryDropDown.Items.Insert(0, New ListItem("-- Select One --", 0))
                        ddlSecondaryDropDown.Items.Insert(0, New ListItem("-- Select One --", 0))
                    End If
                Else
                    lblParent.Text = "Select Parent Drop-down field"
                    lblChild.Text = "Select Child Drop-down field"

                    If ddlRModule.SelectedValue >= 0 Then
                        objCommon = New CCommon
                        objCommon.LoadListMaster(ddlPrimaryDropDown, Session("DomainID"), CCommon.ToLong(ddlRModule.SelectedValue), True)
                        objCommon.LoadListMaster(ddlSecondaryDropDown, Session("DomainID"), CCommon.ToLong(ddlRModule.SelectedValue), True)

                        ddlPrimaryDropDown.Items.Insert(0, New ListItem("-- Select One --", 0))
                        ddlSecondaryDropDown.Items.Insert(0, New ListItem("-- Select One --", 0))
                    End If
                End If

                ScriptManager.RegisterClientScriptBlock(Me, Me.Page.GetType(), "bindDropDownRelationShipSection", "bindDropDownRelationShipSection();", True)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try

        End Sub
        Private Sub BindModule()
            Try
                objCommon = New CCommon
                Dim dt As DataTable = objCommon.GetListModule()
                dt.DefaultView.RowFilter = "numModuleID<>8"

                ddlRModule.DataSource = dt.DefaultView
                ddlRModule.DataTextField = "vcModuleName"
                ddlRModule.DataValueField = "numModuleID"
                ddlRModule.DataBind()

                ddlRModule.Items.Add(New ListItem("Kits with child kits", 14))
                ddlRModule.Items.Insert(0, New ListItem("-- Select One --", 0))
            Catch ex As Exception
                Throw ex
            End Try

        End Sub
        Sub BindBizDocType()
            Try
                objCommon.DomainID = Session("DomainID")
                objCommon.ListID = CCommon.ToLong(27)
                ddlListType.DataSource = objCommon.GetMasterListItemsWithRights
                ddlListType.DataTextField = "vcData"
                ddlListType.DataValueField = "numListItemID"
                ddlListType.DataBind()

                ddlListType.Items.Insert("0", New ListItem("--All--", 0))
            Catch ex As Exception

            End Try
        End Sub


        Sub BindGrid()
            Try
                objCommon.DomainID = Session("DomainID")
                objCommon.ListID = CCommon.ToLong(ddlMasterList.SelectedValue)
                Dim dtMasterListSource As New DataTable
                Dim dtMasterListRights As New DataTable
                If objCommon.ListID = 9 Then
                    dtMasterListSource = objCommon.GetOpportunitySource

                Else
                    dtMasterListRights = objCommon.GetMasterListItemsWithRights
                End If
                Dim dvSource As DataView = dtMasterListSource.DefaultView
                Dim dvRights As DataView = dtMasterListRights.DefaultView

                If CCommon.ToInteger(ddlMasterList.SelectedValue) = 11 Then 'BizDoc status
                    If CCommon.ToInteger(ddlListType.SelectedValue) > 0 Then
                        dvRights.RowFilter = "numListType=" + ddlListType.SelectedValue
                    End If
                ElseIf CCommon.ToInteger(ddlMasterList.SelectedValue) = 176 Then ''Order Status
                    If CCommon.ToInteger(ddlOrderStatusType.SelectedValue) > 0 And CCommon.ToShort(ddlOppOrOrder.SelectedValue) > 0 Then
                        dvRights.RowFilter = "numListType=" + ddlOrderStatusType.SelectedValue + " AND tintOppOrOrder=" + ddlOppOrOrder.SelectedValue
                    ElseIf CCommon.ToInteger(ddlOrderStatusType.SelectedValue) > 0 Then
                        dvRights.RowFilter = "numListType=" + ddlOrderStatusType.SelectedValue
                    ElseIf CCommon.ToShort(ddlOppOrOrder.SelectedValue) > 0 Then
                        dvRights.RowFilter = "tintOppOrOrder=" + ddlOppOrOrder.SelectedValue
                    End If
                End If

                If CCommon.ToInteger(ddlMasterList.SelectedValue) = 18 Then
                    lblLeadSourceWarning.Visible = True
                Else
                    lblLeadSourceWarning.Visible = False
                End If



                If objCommon.ListID = 9 Then
                    dgItemList.DataSource = dvSource
                Else
                    dgItemList.DataSource = dvRights
                End If
                'If objCommon.ListID = 9 Then
                '    dgItemList.DataSource = objCommon.GetOpportunitySource
                'Else
                '    dgItemList.DataSource = objCommon.GetMasterListItemsWithRights
                'End If

                dgItemList.DataBind()
                'dgItemList.EditItemIndex = -1

                Dim dtDataType As DataTable = objCommon.GetListDetail
                If dtDataType.Rows.Count > 0 Then
                    hfDataType.Value = IIf(IsDBNull(dtDataType.Rows(0).Item("vcDataType")), "string", dtDataType.Rows(0).Item("vcDataType"))
                End If

                If CCommon.ToInteger(ddlMasterList.SelectedValue) = 27 Then  'Show Original Biz Doc Name
                    dgItemList.Columns(8).Visible = True
                Else
                    dgItemList.Columns(8).Visible = False
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub ddlMasterList_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlMasterList.SelectedIndexChanged
            Try
                lnkbtnShippingServiceAbbr.Visible = False

                If CCommon.ToInteger(ddlMasterList.SelectedValue) = 11 Then

                    tdBizDocType.Style.Add("display", "")
                    tdOrderStatusType.Style.Add("display", "none")
                    tdOppOrOrder.Style.Add("display", "none")

                    BindBizDocType()
                ElseIf CCommon.ToInteger(ddlMasterList.SelectedValue) = 176 Then
                    tdOrderStatusType.Style.Add("display", "")
                    tdOppOrOrder.Style.Add("display", "")
                    tdBizDocType.Style.Add("display", "none")
                ElseIf CCommon.ToInteger(ddlMasterList.SelectedValue) = 21 Then 'Organization Profile
                    divEnforceSubTotal.Style.Add("display", "")
                ElseIf CCommon.ToInteger(ddlMasterList.SelectedValue) = 82 Then  'Ship Via
                    lnkbtnShippingServiceAbbr.Visible = True
                Else
                    tdBizDocType.Style.Add("display", "none")
                    tdOrderStatusType.Style.Add("display", "none")
                    tdOppOrOrder.Style.Add("display", "none")
                    divEnforceSubTotal.Style.Add("display", "none")
                End If

                BindGrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub
        Private Sub ddlListType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlListType.SelectedIndexChanged, ddlOrderStatusType.SelectedIndexChanged, ddlOppOrOrder.SelectedIndexChanged
            Try
                BindGrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
            Try
                objCommon.DomainID = Session("DomainID")
                objCommon.ListItemName = txtItemName.Text
                objCommon.ListID = ddlMasterList.SelectedValue
                objCommon.UserCntID = Session("UserContactID")
                'Added By:Sachin Sadhu||Date:13thJune2014
                'Purpose:Added One Field numListType to save  bizdoc type wise BizDoc status 

                If CCommon.ToInteger(ddlMasterList.SelectedValue) = 11 Then
                    objCommon.lngListType = CCommon.ToLong(ddlListType.SelectedValue)
                    objCommon.tintOppOrder = 0
                ElseIf CCommon.ToInteger(ddlMasterList.SelectedValue) = 176 Then
                    objCommon.lngListType = CCommon.ToLong(ddlOrderStatusType.SelectedValue)
                    objCommon.tintOppOrder = CCommon.ToShort(ddlOppOrOrder.SelectedValue)
                ElseIf CCommon.ToInteger(ddlMasterList.SelectedValue) = 21 Then
                    objCommon.EnforceMinOrderAmount = chkEnforceSubTotal.Checked
                    If txtEnforceSubTotal.Text <> "" Then
                        objCommon.MinOrderAmount = CCommon.ToDecimal(txtEnforceSubTotal.Text)
                    Else
                        objCommon.MinOrderAmount = 0
                    End If
                Else
                    objCommon.lngListType = 0
                    objCommon.tintOppOrder = 0
                End If
                'End of code
                objCommon.ManageItemList()
                txtItemName.Text = ""
                Call BindGrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Sub dgItemList_EditCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgItemList.EditCommand
            Try
                dgItemList.EditItemIndex = e.Item.ItemIndex
                Call BindGrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Sub dgItemList_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgItemList.ItemCommand
            Try
                If e.CommandName = "Cancel" Then
                    dgItemList.EditItemIndex = e.Item.ItemIndex
                    dgItemList.EditItemIndex = -1
                    Call BindGrid()
                End If

                If e.CommandName = "Delete" Then
                    Dim OpeningBal As Decimal
                    objCommon.ListItemID = CType(e.Item.FindControl("lblListItemID"), Label).Text
                    objCommon.DomainID = Session("DomainID")
                    objCommon.ListID = ddlMasterList.SelectedValue
                    Try
                        If objCommon.ListID = 64 Then
                            OpeningBal = objCommon.CheckOpeningBalance()
                            If OpeningBal = 0 Then
                                objCommon.DeleteItemList()
                            Else : litMessage.Text = "Account Class has Balance amount.Cannot be deleted."
                            End If
                        Else
                            objCommon.DeleteItemList()
                        End If
                    Catch ex As Exception
                        If ex.Message.Contains("USED_AS_AUTHORITATIVE_BIZDOC") Then
                            litMessage.Text = "You can't delete bizdoc type as it is used as authoritative bizdoc."
                        Else
                            litMessage.Text = "Depenedent Record exists.Cannot be deleted."
                        End If
                    End Try

                    Call BindGrid()
                End If

                If e.CommandName = "Update" Then

                    If ddlMasterList.SelectedValue = "73" Or ddlMasterList.SelectedValue = "27" Then
                        Dim objListDetailsName As New BACRM.BusinessLogic.Admin.ListDetailsName
                        objListDetailsName.DomainID = Session("DomainID")
                        objListDetailsName.ListID = CCommon.ToLong(ddlMasterList.SelectedValue)
                        objListDetailsName.ListItemID = CCommon.ToLong(CType(e.Item.FindControl("lblListItemID"), Label).Text)
                        objListDetailsName.Name = CCommon.ToString(CType(e.Item.FindControl("txtEItemName"), TextBox).Text)
                        objListDetailsName.Save()
                        dgItemList.EditItemIndex = e.Item.ItemIndex
                        dgItemList.EditItemIndex = -1
                        Call BindGrid()
                    Else
                        objCommon.ListItemID = CType(e.Item.FindControl("lblListItemID"), Label).Text
                        objCommon.ListItemName = CType(e.Item.FindControl("txtEItemName"), TextBox).Text
                        objCommon.UserCntID = Session("UserContactID")
                        'Added By:Sachin Sadhu||Date:17thJune2014
                        'Purpose:Added One Field numListType to save  bizdoc type wise BizDoc status 

                        If CCommon.ToInteger(ddlMasterList.SelectedValue) = 11 Then
                            objCommon.lngListType = CCommon.ToLong(ddlListType.SelectedValue)
                            objCommon.tintOppOrder = 0
                        ElseIf CCommon.ToInteger(ddlMasterList.SelectedValue) = 176 Then
                            objCommon.lngListType = CCommon.ToLong(CType(e.Item.FindControl("ddlOrderTypeGrid"), DropDownList).SelectedValue)
                            objCommon.tintOppOrder = CCommon.ToLong(CType(e.Item.FindControl("ddlOppOrOrderGrid"), DropDownList).SelectedValue)
                        ElseIf CCommon.ToInteger(ddlMasterList.SelectedValue) = 21 Then
                            objCommon.ListID = CCommon.ToLong(ddlMasterList.SelectedValue)
                            objCommon.DomainID = Session("DomainID")
                            Dim txtMinOrderAmount = CType(e.Item.FindControl("txtMinOrderAmount"), TextBox).Text
                            If txtMinOrderAmount <> Nothing AndAlso txtMinOrderAmount <> "" AndAlso txtMinOrderAmount <> 0 Then
                                objCommon.EnforceMinOrderAmount = True 'chkEnforceSubTotal.Checked
                                objCommon.MinOrderAmount = CCommon.ToDecimal(txtMinOrderAmount)
                            Else
                                objCommon.EnforceMinOrderAmount = False
                                objCommon.MinOrderAmount = 0
                            End If
                        ElseIf CCommon.ToInteger(ddlMasterList.SelectedValue) = 30 Then
                            objCommon.ListID = CCommon.ToLong(ddlMasterList.SelectedValue)
                            objCommon.DomainID = Session("DomainID")
                            Dim ddlColorScheme As DropDownList = DirectCast(e.Item.FindControl("ddlColorScheme"), DropDownList)
                            Dim ddlFollowUpStatusGroup As DropDownList = DirectCast(e.Item.FindControl("ddlFollowUpStatusGroup"), DropDownList)
                            objCommon.vcColorScheme = ddlColorScheme.SelectedValue
                            objCommon.numListItemGroupId = ddlFollowUpStatusGroup.SelectedValue
                        ElseIf CCommon.ToInteger(ddlMasterList.SelectedValue) = 1344 Then
                            objCommon.ListID = CCommon.ToLong(ddlMasterList.SelectedValue)
                            objCommon.DomainID = Session("DomainID")
                            Dim ddlBlogCategory As DropDownList = DirectCast(e.Item.FindControl("ddlBlogCategory"), DropDownList)
                            objCommon.numListItemGroupId = ddlBlogCategory.SelectedValue
                        Else
                            objCommon.lngListType = 0
                            objCommon.tintOppOrder = 0
                        End If

                        'End of code
                        objCommon.ManageItemList()
                        txtItemName.Text = ""
                        dgItemList.EditItemIndex = e.Item.ItemIndex
                        dgItemList.EditItemIndex = -1
                        Call BindGrid()
                    End If
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Sub dgItemList_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgItemList.ItemDataBound
            Try
                If e.Item.ItemType = ListItemType.Header Then
                    If Not e.Item.FindControl("lblItemHeader") Is Nothing Then
                        If ddlMasterList.SelectedValue = "35" Then
                            DirectCast(e.Item.FindControl("lblItemHeader"), Label).Text = "Work Center"
                        Else
                            DirectCast(e.Item.FindControl("lblItemHeader"), Label).Text = "Item Name"
                        End If
                    End If
                End If
                If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
                    Dim btn As LinkButton
                    btn = e.Item.FindControl("btnDelete")
                    Dim btnDelete As LinkButton
                    Dim lnkDelete As LinkButton
                    Dim lnkbtnEdt As LinkButton

                    lnkDelete = e.Item.FindControl("lnkDelete")
                    btnDelete = e.Item.FindControl("btnDelete")
                    lnkbtnEdt = e.Item.FindControl("lnkbtnEdt")

                    If m_aryRightsForPage(RIGHTSTYPE.DELETE) = 0 Then
                        btnDelete.Visible = False
                        lnkDelete.Visible = True
                        lnkDelete.Attributes.Add("onclick", "return DeleteMessage()")
                    Else
                        btnDelete.Visible = True
                        lnkDelete.Visible = False
                        lnkDelete.Attributes.Add("onclick", "return DeleteRecord()")
                    End If

                    If m_aryRightsForPage(RIGHTSTYPE.UPDATE) = 0 Then lnkbtnEdt.Attributes.Add("onclick", "return EditMessage()")
                    If e.Item.Cells(0).Text = True Then
                        Dim lnk As LinkButton
                        lnk = e.Item.FindControl("lnkdelete")
                        lnk.Visible = True
                        btn.Visible = False
                        lnk.Attributes.Add("onclick", "return DeleteMessage()")

                        If ddlMasterList.SelectedValue <> "73" AndAlso ddlMasterList.SelectedValue <> "27" AndAlso ddlMasterList.SelectedValue <> "338" Then
                            lnk = e.Item.FindControl("lnkbtnEdt")
                            lnk.Visible = False
                        End If
                    Else : btn.Attributes.Add("onclick", "return DeleteRecord()")
                    End If
                    If e.Item.Cells(1).Text = True Then
                        Dim lnk As LinkButton
                        lnk = e.Item.FindControl("lnkdelete")
                        lnk.Visible = True
                        btn.Visible = False
                        lnk.Attributes.Add("onclick", "return DeleteMessage()")
                    End If
                    If ddlMasterList.SelectedValue = 30 Then
                        dgItemList.Columns(9).Visible = True
                        dgItemList.Columns(10).Visible = True
                    Else
                        dgItemList.Columns(9).Visible = False
                        dgItemList.Columns(10).Visible = False
                    End If
                    If ddlMasterList.SelectedValue = 1344 Then
                        dgItemList.Columns(11).Visible = True
                    Else
                        dgItemList.Columns(11).Visible = False
                    End If
                    If ddlMasterList.SelectedValue = 21 Then
                        dgItemList.Columns(7).Visible = True
                        Dim lbl As Label = e.Item.FindControl("lblMinOrderAmount")
                        Dim subTotal = lbl.Text
                        If subTotal = 0 Then
                            lbl.Text = ""
                        End If
                    Else
                        dgItemList.Columns(7).Visible = False
                    End If
                End If
                If e.Item.ItemType = ListItemType.EditItem Then
                    Dim ddlListTypeGrid As DropDownList = DirectCast(e.Item.FindControl("ddlListTypeGrid"), DropDownList)
                    Dim ddlColorScheme As DropDownList = DirectCast(e.Item.FindControl("ddlColorScheme"), DropDownList)
                    Dim ddlOppOrOrderGrid As DropDownList = DirectCast(e.Item.FindControl("ddlOppOrOrderGrid"), DropDownList)
                    Dim ddlFollowUpStatusGroup As DropDownList = DirectCast(e.Item.FindControl("ddlFollowUpStatusGroup"), DropDownList)
                    Dim ddlBlogCategory As DropDownList = DirectCast(e.Item.FindControl("ddlBlogCategory"), DropDownList)

                    If hfDataType.Value = "numeric" Then
                        CType(e.Item.FindControl("txtEItemName"), TextBox).Attributes.Add("onkeypress", "CheckNumber(1,event)")
                    End If
                    objCommon.DomainID = Session("DomainID")
                    objCommon.ListID = CCommon.ToLong(27)
                    ddlListTypeGrid.DataSource = objCommon.GetMasterListItemsWithRights
                    ddlListTypeGrid.DataTextField = "vcData"
                    ddlListTypeGrid.DataValueField = "numListItemID"
                    ddlListTypeGrid.DataBind()
                    ddlListTypeGrid.Items.Insert("0", New ListItem("--All--", 0))

                    ddlFollowUpStatusGroup.DataSource = objCommon.GetMasterListItems(CCommon.ToLong(1047), Session("DomainID"))
                    ddlFollowUpStatusGroup.DataTextField = "vcData"
                    ddlFollowUpStatusGroup.DataValueField = "numListItemID"
                    ddlFollowUpStatusGroup.DataBind()
                    ddlFollowUpStatusGroup.Items.Insert("0", New ListItem("--All--", 0))

                    ddlBlogCategory.DataSource = objCommon.GetMasterListItems(CCommon.ToLong(1343), Session("DomainID"))
                    ddlBlogCategory.DataTextField = "vcData"
                    ddlBlogCategory.DataValueField = "numListItemID"
                    ddlBlogCategory.DataBind()
                    ddlBlogCategory.Items.Insert("0", New ListItem("--All--", 0))


                    For Each d As ListItem In ddlColorScheme.Items
                        d.Attributes.Add("class", d.Value)
                    Next

                End If
                If (CCommon.ToInteger(ddlMasterList.SelectedValue) = 30) Then ''Follow Up Status
                    Dim ddlColorScheme As DropDownList = DirectCast(e.Item.FindControl("ddlColorScheme"), DropDownList)
                    Dim ddlFollowUpStatusGroup As DropDownList = DirectCast(e.Item.FindControl("ddlFollowUpStatusGroup"), DropDownList)
                    Dim ddlBlogCategory As DropDownList = DirectCast(e.Item.FindControl("ddlBlogCategory"), DropDownList)
                    If Not ddlColorScheme Is Nothing AndAlso Not ddlColorScheme.Items.FindByValue(CCommon.ToString(DataBinder.Eval(e.Item.DataItem, "vcColorScheme"))) Is Nothing Then
                        ddlColorScheme.Items.FindByValue(CCommon.ToString(DataBinder.Eval(e.Item.DataItem, "vcColorScheme"))).Selected = True
                    End If
                    If Not ddlFollowUpStatusGroup Is Nothing AndAlso Not ddlFollowUpStatusGroup.Items.FindByValue(CCommon.ToString(DataBinder.Eval(e.Item.DataItem, "numListItemGroupId"))) Is Nothing Then
                        ddlFollowUpStatusGroup.Items.FindByValue(CCommon.ToString(DataBinder.Eval(e.Item.DataItem, "numListItemGroupId"))).Selected = True
                    End If
                End If
                If (CCommon.ToInteger(ddlMasterList.SelectedValue) = 1344) Then ''Follow Up Status
                    Dim ddlBlogCategory As DropDownList = DirectCast(e.Item.FindControl("ddlBlogCategory"), DropDownList)
                    If Not ddlBlogCategory Is Nothing AndAlso Not ddlBlogCategory.Items.FindByValue(CCommon.ToString(DataBinder.Eval(e.Item.DataItem, "numListItemGroupId"))) Is Nothing Then
                        ddlBlogCategory.Items.FindByValue(CCommon.ToString(DataBinder.Eval(e.Item.DataItem, "numListItemGroupId"))).Selected = True
                    End If
                End If
                If (CCommon.ToInteger(ddlMasterList.SelectedValue) = 11) Then ''BizDocStatus
                    dgItemList.Columns(4).Visible = True
                    dgItemList.Columns(5).Visible = False
                    dgItemList.Columns(6).Visible = False
                ElseIf (CCommon.ToInteger(ddlMasterList.SelectedValue) = 176) Then 'Order Status
                    dgItemList.Columns(5).Visible = True
                    dgItemList.Columns(6).Visible = True
                    dgItemList.Columns(4).Visible = False

                    Dim ddlOrderTypeGrid As DropDownList = DirectCast(e.Item.FindControl("ddlOrderTypeGrid"), DropDownList)
                    Dim ddlOppOrOrderGrid As DropDownList = DirectCast(e.Item.FindControl("ddlOppOrOrderGrid"), DropDownList)
                    If Not ddlOrderTypeGrid Is Nothing AndAlso Not ddlOrderTypeGrid.Items.FindByValue(CCommon.ToString(DataBinder.Eval(e.Item.DataItem, "numListType"))) Is Nothing Then
                        ddlOrderTypeGrid.Items.FindByValue(CCommon.ToString(DataBinder.Eval(e.Item.DataItem, "numListType"))).Selected = True
                    End If

                    If Not ddlOppOrOrderGrid Is Nothing AndAlso Not ddlOppOrOrderGrid.Items.FindByValue(CCommon.ToString(DataBinder.Eval(e.Item.DataItem, "tintOppOrOrder"))) Is Nothing Then
                        ddlOppOrOrderGrid.Items.FindByValue(CCommon.ToString(DataBinder.Eval(e.Item.DataItem, "numListType"))).Selected = True
                    End If
                Else
                    dgItemList.Columns(4).Visible = False
                    dgItemList.Columns(5).Visible = False
                    dgItemList.Columns(6).Visible = False

                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click
            Try
                BindGrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Sub ddlModule_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlModule.SelectedIndexChanged
            Try
                objCommon.LoadListMaster(ddlMasterList, Session("DomainID"), ddlModule.SelectedValue)

                BindGrid()
                If CCommon.ToInteger(ddlModule.SelectedValue) = 2 Then 'Orders
                    'nothing to code..leave it as it is
                Else
                    tdBizDocType.Style.Add("display", "none")
                    tdOrderStatusType.Style.Add("display", "none")
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Sub dgItemList_UpdateCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgItemList.UpdateCommand
            'If e.CommandName = "Update" Then
            '    objCommon.ListItemID = CType(e.Item.FindControl("lblListItemID"), Label).Text
            '    objCommon.ListItemName = CType(e.Item.FindControl("txtEItemName"), TextBox).Text
            '    objCommon.UserCntID = Session("UserContactID")
            '    objCommon.ManageItemList()
            '    dgItemList.EditItemIndex = -1
            '    Call BindGrid()
            'End If
        End Sub

        <WebMethod()>
        Public Shared Function WebMethodCreateRelationShip(ByVal DomainID As Integer, ByVal PrimaryDropDown As Long, ByVal SecondaryDropDown As Long, ByVal ModuleID As Long, ByVal IsTaskRelation As Boolean) As String
            Try
                Dim objCommon As CCommon
                objCommon = New CCommon
                Dim FieldRelID As Long
                objCommon.Mode = 1  '' To Insert data to Header Table
                objCommon.DomainID = DomainID
                objCommon.ModuleID = ModuleID
                objCommon.PrimaryListID = PrimaryDropDown
                objCommon.SecondaryListID = SecondaryDropDown
                objCommon.IsTaskRelation = IsTaskRelation
                FieldRelID = objCommon.ManageFieldRelationships

                Dim json As String = String.Empty
                json = JsonConvert.SerializeObject(FieldRelID, Formatting.None)
                Return json
            Catch ex As Exception
                Dim strError As String = ""

                strError = ex.Message
                Throw New Exception(strError)
                'Throw ex
            End Try

        End Function
        <WebMethod()>
        Public Shared Function WebMethodLoadRelationShip(ByVal DomainID As Integer, ByVal ModuleID As Long, ByVal PrimaryDropDown As Long, ByVal SecondaryDropDown As Long, ByVal IsTaskRelation As Boolean) As String
            Try
                Dim objCommon As CCommon
                objCommon = New CCommon
                Dim dtFieldRelationshipItems As DataTable
                objCommon.DomainID = DomainID
                objCommon.ModuleID = ModuleID
                objCommon.Mode = 1  ''To get all the relationships
                objCommon.IsTaskRelation = IsTaskRelation
                objCommon.PrimaryListID = PrimaryDropDown
                objCommon.SecondaryListID = SecondaryDropDown
                dtFieldRelationshipItems = objCommon.GetFieldRelationships.Tables(0)

                Dim json As String = String.Empty
                json = JsonConvert.SerializeObject(dtFieldRelationshipItems, Formatting.None)
                Return json
            Catch ex As Exception
                Dim strError As String = ""

                strError = ex.Message
                Throw New Exception(strError)
                'Throw ex
            End Try

        End Function

        <WebMethod()>
        Public Shared Function WebMethodDeleteRelationShipDetails(ByVal DomainID As Integer, ByVal FieldRelID As Long) As String
            Try
                Dim objCommon As CCommon
                objCommon = New CCommon
                Dim lngAccess As Long
                objCommon.Mode = 4 ' To get Header Table information
                objCommon.FieldRelID = FieldRelID
                lngAccess = objCommon.ManageFieldRelationships

                Dim json As String = String.Empty
                json = JsonConvert.SerializeObject(lngAccess, Formatting.None)
                Return json
            Catch ex As Exception
                Dim strError As String = ""

                strError = ex.Message
                Throw New Exception(strError)
                'Throw ex
            End Try

        End Function

        <WebMethod()>
        Public Shared Function WebMethodLoadRelationShipDetails(ByVal DomainID As Integer, ByVal FieldRelID As Long, ByVal IsTaskRelation As Boolean) As String
            Try
                Dim objCommon As CCommon
                objCommon = New CCommon
                Dim dtFieldRelInfo As DataTable
                objCommon.Mode = 2 ' To get Header Table information
                objCommon.FieldRelID = FieldRelID
                objCommon.IsTaskRelation = IsTaskRelation
                dtFieldRelInfo = objCommon.GetFieldRelationships.Tables(0)

                Dim json As String = String.Empty
                json = JsonConvert.SerializeObject(dtFieldRelInfo, Formatting.None)
                Return json
            Catch ex As Exception
                Dim strError As String = ""

                strError = ex.Message
                Throw New Exception(strError)
                'Throw ex
            End Try

        End Function


        <WebMethod()>
        Public Shared Function WebMethodLoadRelationShipDetailsDropDown(ByVal DomainID As Integer, ByVal ModuleID As Long, ByVal ListId As Long) As String
            Try
                If ModuleID = 14 Then
                    Dim objItem As New CItems
                    objItem.DomainID = DomainID
                    objItem.ItemCode = ListId
                    Dim dt As DataTable = objItem.GetChildKits()

                    Dim json As String = String.Empty
                    json = JsonConvert.SerializeObject(dt, Formatting.None)
                    Return json
                Else
                    Dim objCommon As CCommon
                    objCommon = New CCommon
                    Dim dtFieldRelInfo As DataTable
                    dtFieldRelInfo = objCommon.GetMasterListItems(ListId, DomainID)

                    Dim json As String = String.Empty
                    json = JsonConvert.SerializeObject(dtFieldRelInfo, Formatting.None)
                    Return json
                End If
            Catch ex As Exception
                Dim strError As String = ""

                strError = ex.Message
                Throw New Exception(strError)
                'Throw ex
            End Try

        End Function

        <WebMethod()>
        Public Shared Function WebMethodLoadRelationShipDetailsByPrimaryDropDown(ByVal DomainID As Integer, ByVal FieldRelID As Long, ByVal PrimaryDropdownItems As Long, ByVal IsTaskRelation As Boolean) As String
            Try
                Dim objCommon As CCommon
                objCommon = New CCommon
                Dim dtFieldRelationshipItems As DataTable
                objCommon.Mode = 2 ' To get Header Table information
                objCommon.FieldRelID = FieldRelID
                objCommon.IsTaskRelation = IsTaskRelation
                objCommon.PrimaryListItemID = PrimaryDropdownItems
                dtFieldRelationshipItems = objCommon.GetFieldRelationships.Tables(1)

                Dim json As String = String.Empty
                json = JsonConvert.SerializeObject(dtFieldRelationshipItems, Formatting.None)
                Return json
            Catch ex As Exception
                Dim strError As String = ""

                strError = ex.Message
                Throw New Exception(strError)
                'Throw ex
            End Try

        End Function

        <WebMethod()>
        Public Shared Function WebMethodDeleteRelationItems(ByVal DomainID As Integer, ByVal FieldRelDTLID As Long) As String
            Try
                Dim objCommon As CCommon
                objCommon = New CCommon
                objCommon = New CCommon
                objCommon.Mode = 3 ' Delete only from detail table
                objCommon.FieldRelDTLID = FieldRelDTLID
                objCommon.ManageFieldRelationships()

                Dim json As String = String.Empty
                json = JsonConvert.SerializeObject("1", Formatting.None)
                Return json
            Catch ex As Exception
                Dim strError As String = ""

                strError = ex.Message
                Throw New Exception(strError)
                'Throw ex
            End Try

        End Function


        <WebMethod()>
        Public Shared Function WebMethodAddRelationItems(ByVal DomainID As Integer, ByVal FieldRelID As Long, ByVal PrimaryListItemID As Long, ByVal SecondaryListItemID As Long) As String
            Try
                Dim objCommon As CCommon
                objCommon = New CCommon
                objCommon.Mode = 2 ' Insert into Detail Table
                objCommon.FieldRelID = FieldRelID
                objCommon.PrimaryListItemID = PrimaryListItemID
                objCommon.SecondaryListItemID = SecondaryListItemID
                Dim output As Long
                output = objCommon.ManageFieldRelationships()

                Dim json As String = String.Empty
                json = JsonConvert.SerializeObject(output, Formatting.None)
                Return json
            Catch ex As Exception
                Dim strError As String = ""

                strError = ex.Message
                Throw New Exception(strError)
                'Throw ex
            End Try

        End Function

        <WebMethod()>
        Public Shared Function WebMethodDeleteRelation(ByVal DomainID As Integer, ByVal FieldRelID As Long) As String
            Try
                Dim objCommon As CCommon
                objCommon = New CCommon
                objCommon.DomainID = DomainID
                objCommon.FieldRelID = FieldRelID
                objCommon.Mode = 4
                objCommon.ManageFieldRelationships()
                Dim json As String = String.Empty
                json = JsonConvert.SerializeObject("1", Formatting.None)
                Return json
            Catch ex As Exception
                Dim strError As String = ""

                strError = ex.Message
                Throw New Exception(strError)
                'Throw ex
            End Try

        End Function
    End Class
End Namespace