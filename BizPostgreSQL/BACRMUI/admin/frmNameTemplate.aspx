﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmNameTemplate.aspx.vb"
    Inherits=".frmNameTemplate" MasterPageFile="~/common/Popup.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Define Name Template</title>
    <script type="text/javascript" language="javascript">
        $(document).ready(function () {
            InitializeValidation();
        });

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <table width="100%">
        <tr>
            <td>
                <div class="input-part">
                    <div class="right-input">
                        <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="button" />&nbsp;&nbsp;
                    </div>
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <ul id="messagebox" class="errorInfo" style="display: none">
                </ul>
                <asp:Label ID="litMessage" EnableViewState="false" runat="server"></asp:Label>
            </td>
        </tr>
    </table>
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePartialRendering="true">
    </asp:ScriptManager>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageTitle" runat="server" ClientIDMode="Static">
    Update Name Template
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="Content" runat="server" ClientIDMode="Static">
    <asp:Table ID="table2" CellPadding="0" CellSpacing="0" BorderWidth="1" runat="server"
        Width="800px" CssClass="aspTable" BorderColor="black" GridLines="None" Height="350">
        <asp:TableRow>
            <asp:TableCell HorizontalAlign="Right">
                <br />
                <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btn btn-primary" />
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell VerticalAlign="Top">
                <br />
                <asp:GridView ID="gvNameTemplate" runat="server" Width="100%" CssClass="tbl" AllowSorting="true"
                    AutoGenerateColumns="False" DataKeyNames="numRecordID">
                    <AlternatingRowStyle CssClass="ais" />
                    <RowStyle CssClass="is"></RowStyle>
                    <HeaderStyle CssClass="hs"></HeaderStyle>
                    <Columns>
                        <asp:TemplateField HeaderText="Order Type">
                            <ItemTemplate>
                                <asp:Label Text='<%# Eval("vcModuleName") %>' runat="server" ID="lblModuleName" CssClass="signup" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Prefix">
                            <ItemTemplate>
                                <asp:TextBox Width="200" ID="txtNameTemplate" CssClass="signup" runat="server" Text='<%# Eval("vcNameTemplate") %>'
                                    MaxLength="100"></asp:TextBox>
                                - XXXX
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Sequence #">
                            <ItemTemplate>
                                <asp:Label ID="lblSequence" CssClass="signup" runat="server" Text='<%# Eval("numSequenceId") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Min Length">
                            <ItemTemplate>
                                <%--<asp:TextBox Width="50" ID="txtMinLength" CssClass="signup" runat="server" Text='<%# Eval("numMinLength") %>'
                                    MaxLength="2" onkeypress="CheckNumber(2,event)"></asp:TextBox>--%>
                                <asp:TextBox Width="50" ID="txtMinLength" CssClass="required_integer {required:true ,number:true,maxlength:2, messages:{required:'Please provide value!',number:'provide valid length!'}} signup"
                                    runat="server" Text='<%# Eval("numMinLength") %>'></asp:TextBox>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell CssClass="rowHeader">
             <br />
            BizDocs
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell HorizontalAlign="Right">
                <br />
                <asp:Button ID="btnSaveBizDoc" runat="server" Text="Save" CssClass="btn btn-primary" />
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell VerticalAlign="Top">
                <br />
                <asp:GridView ID="gvBizDoc" runat="server" Width="100%" CssClass="tbl" AllowSorting="true"
                    AutoGenerateColumns="False" DataKeyNames="numRecordID">
                    <AlternatingRowStyle CssClass="ais" />
                    <RowStyle CssClass="is"></RowStyle>
                    <HeaderStyle CssClass="hs"></HeaderStyle>
                    <Columns>
                        <asp:BoundField HeaderText="BizDoc Type" DataField="vcModuleName" />
                        <asp:TemplateField HeaderText="Prefix">
                            <ItemTemplate>
                                <asp:TextBox Width="200" ID="txtNameTemplate" CssClass="signup" runat="server" Text='<%# Eval("vcNameTemplate") %>'
                                    MaxLength="100"></asp:TextBox>
                                - XXXX
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Sequence #">
                            <ItemTemplate>
                                <asp:Label ID="lblSequence" CssClass="signup" runat="server" Text='<%# Eval("numSequenceId") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Min Length">
                            <ItemTemplate>
                                <%--<asp:TextBox Width="50" ID="txtMinLength" CssClass="signup" runat="server" Text='<%# Eval("numMinLength") %>'
                                    MaxLength="2" onkeypress="CheckNumber(2,event)"></asp:TextBox>--%>
                                <asp:TextBox Width="50" ID="txtMinLength" CssClass="required_integer {required:true ,number:true,maxlength:2, messages:{required:'Please provide value!',number:'provide valid length!'}} signup"
                                    runat="server" Text='<%# Eval("numMinLength") %>'></asp:TextBox>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell CssClass="signup"><br />
        <b>Legends</b>
<ul>
<li>$OrganizationName$  will be replaced by Name of Organization</li>
<li>$OrderID$ Will be replaced by BizAutomation Unique Identified for Order</li>
<li>$BizDocID$ Will be replaced by BizAutomation Unique Identified for Bizdoc</li>
<li>$YEAR$ will be replaced by Current Year</li>
<li>$MONTH$ will be replaced by Current Month</li>
<li>$DAY$ will be replaced by Current Day</li>
<li>$QUARTER$ will be replaced by Current Quarter</li>
</ul>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
</asp:Content>
