﻿Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Contacts
Imports BACRM.BusinessLogic.ShioppingCart
Public Class frmPortalColumnSettings
    Inherits BACRMPage

#Region "GLOBAL OBJECT/VARIABLE DECLARATION"

    Dim objContact As CContacts

#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            btnSave.Attributes.Add("onclick", "Save()")

            If Not IsPostBack Then
                BindLists()
            End If

        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Sub BindLists()
        Try
            Dim ds As DataSet
            objContact = New CContacts
            objContact.FormId = CCommon.ToLong(ddlForms.SelectedValue)
            objContact.ContactType = 0
            objContact.DomainID = Session("DomainId")
            'objContact.UserCntID = Session("UserContactId")

            ds = objContact.GetColumnConfiguration
            lstAvailablefld.DataSource = ds.Tables(0)
            lstAvailablefld.DataTextField = "vcFieldName"
            lstAvailablefld.DataValueField = "numFieldID"
            lstAvailablefld.DataBind()
            lstSelectedfld.DataSource = ds.Tables(1)
            lstSelectedfld.DataValueField = "numFieldID"
            lstSelectedfld.DataTextField = "vcFieldName"
            lstSelectedfld.DataBind()
            If Not lstAvailablefld.Items.FindByText("Opp Name") Is Nothing Then
                Dim item As ListItem
                item = lstAvailablefld.Items.FindByText("Opp Name")
                item.Text = "Sales Order Name"
            End If
            If Not lstSelectedfld.Items.FindByText("Opp Name") Is Nothing Then
                Dim item As ListItem
                item = lstSelectedfld.Items.FindByText("Opp Name")
                item.Text = "Sales Order Name"
            End If

            btnClose.Attributes.Add("onclick", "javascript:window.close();return false;")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            If objContact Is Nothing Then objContact = New CContacts

            Dim dsNew As New DataSet
            Dim dtTable As New DataTable
            dtTable.Columns.Add("numFieldID")
            dtTable.Columns.Add("bitCustom")
            dtTable.Columns.Add("tintOrder")
            Dim i As Integer

            Dim dr As DataRow
            Dim str As String()
            str = hdnCol.Value.Split(",")
            For i = 0 To str.Length - 2
                dr = dtTable.NewRow
                dr("numFieldID") = str(i).Split("~")(0)
                dr("bitCustom") = str(i).Split("~")(1)
                dr("tintOrder") = i
                dtTable.Rows.Add(dr)
            Next

            dtTable.TableName = "Table"
            dsNew.Tables.Add(dtTable.Copy)

            objContact.FormId = CCommon.ToLong(ddlForms.SelectedValue)
            objContact.ContactType = 0
            objContact.DomainID = Session("DomainId")
            objContact.UserCntID = 0 'Session("UserContactId")

            objContact.strXml = dsNew.GetXml
            objContact.SaveContactColumnConfiguration()
            dsNew.Tables.Remove(dsNew.Tables(0))

            BindLists()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub



    Private Sub ddlForms_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlForms.SelectedIndexChanged
        Try
            BindLists()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
End Class