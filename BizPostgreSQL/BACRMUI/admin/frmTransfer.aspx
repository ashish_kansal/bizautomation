<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmTransfer.aspx.vb" Inherits="BACRM.UserInterface.Admin.frmTransfer"
    MasterPageFile="~/common/Popup.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Transfer Owner / Assigned To</title>
    <script language="javascript">
        function check() {
            if (document.getElementById("ddlEmployee").value == 0 && document.getElementById("ddlAssign").value == 0) {
                alert("Please Select Employee")
                document.getElementById("ddlEmployee").focus()
                return false;
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <div class="input-part">
        <div class="right-input">
            <asp:Button ID="btnTransfer" OnClientClick="return check()" runat="server" Text="Transfer"
                CssClass="button"></asp:Button>
            &nbsp;
            <asp:Button ID="btnCancel" OnClientClick="Close()" runat="server" Text="Close" CssClass="button">
            </asp:Button>&nbsp;&nbsp;
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageTitle" runat="server" ClientIDMode="Static">
    Transfer Record
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="Content" runat="server" ClientIDMode="Static">
    <table width="600px">
        <tr>
            <td class="normal4" align="center" colspan="2">
                <asp:Literal ID="litMessage" runat="server" EnableViewState="False"></asp:Literal>
            </td>
        </tr>
        <tr>
            <td class="normal1" align="right" valign="top">
                Record Owner
            </td>
            <td>
                <asp:DropDownList ID="ddlEmployee" CssClass="signup" runat="server">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="normal1" align="right" valign="top">
                Assigned To
            </td>
            <td>
                <asp:DropDownList ID="ddlAssign" CssClass="signup" runat="server">
                </asp:DropDownList>
            </td>
        </tr>
    </table>
</asp:Content>
