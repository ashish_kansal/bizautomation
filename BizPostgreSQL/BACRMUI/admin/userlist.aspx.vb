'Created By         :Debasish
'Modification Date  :18-Apr-2003
' Modified By Anoop Jayaraj
'9/june/2005
Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Common
Namespace BACRM.UserInterface.Admin
    Public Class frmUserList
        Inherits BACRMPage
        Dim strColumn As String
        Dim objCommonMaster As CCommon
       

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
            Me.ID = "frmUserList"

        End Sub

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Try
                DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ""
                DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "none")
                If Not IsPostBack Then
                    txtSortColumnA.Text = "UserName"
                    txtSortOrderA.Text = "ASC"
                    txtSortColumnE.Text = "Organization"
                    txtSortOrderE.Text = "ASC"
                    If GetQueryStringVal("SelectedIndex") <> "" Then radOppTab.SelectedIndex = GetQueryStringVal("SelectedIndex")

                    GetUserRightsForPage(13, 2)

                    If m_aryRightsForPage(RIGHTSTYPE.VIEW) = 0 Then
                        Response.Redirect("../admin/authentication.aspx?mesg=AS")
                    End If

                    If GetQueryStringVal("SelectedIndex") <> "" Then radOppTab.SelectedIndex = GetQueryStringVal("SelectedIndex")

                    BindUniversalSettings()
                    BindDatagrid()

                    radOppTab.MultiPage.FindPageViewByID(radOppTab.SelectedTab.PageViewID).Selected = True
                End If
                GetSubscribtionCount()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Sub DisplayError(ByVal ex As String)
            DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ex
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
        End Sub

        Private Sub GetSubscribtionCount()
            Try
                Dim objSubscribers As New Subscribers
                objSubscribers.DomainID = CCommon.ToLong(Session("DomainID"))
                Dim dt As DataTable = objSubscribers.GetSubscribtionCount()

                If Not dt Is Nothing AndAlso dt.Rows.Count > 0 Then
                    lblFullUsers.Text = CCommon.ToString(dt.Rows(0)("vcFullUsers"))
                    lblLimitedAccessUsers.Text = CCommon.ToString(dt.Rows(0)("vcLimitedAccessUsers"))
                    lblBusinessPortalUsers.Text = CCommon.ToString(dt.Rows(0)("vcBusinessPortalUsers"))
                End If
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Private Sub BindUniversalSettings()
            Try
                Dim objUniversalSMTPIMAP As New UniversalSMTPIMAP
                objUniversalSMTPIMAP.DomainID = CCommon.ToLong(Session("DomainID"))
                Dim dt As DataTable = objUniversalSMTPIMAP.GetByDomain()

                If Not dt Is Nothing AndAlso dt.Rows.Count > 0 Then
                    txtUniversalSMTPURL.Text = CCommon.ToString(dt.Rows(0)("vcSMTPServer"))
                    txtUniversalSMTPPort.Text = CCommon.ToString(dt.Rows(0)("numSMTPPort"))
                    chkUniversalSMTPSSL.Checked = CCommon.ToBool(dt.Rows(0)("bitSMTPSSL"))
                    chkUniversalSMTPUserAuthentication.Checked = CCommon.ToBool(dt.Rows(0)("bitSMTPAuth"))

                    txtUniversalIMAPURL.Text = CCommon.ToString(dt.Rows(0)("vcIMAPServer"))
                    txtUniversalIMAPPort.Text = CCommon.ToString(dt.Rows(0)("numIMAPPort"))
                    chkUniversalIMAPSSL.Checked = CCommon.ToBool(dt.Rows(0)("bitIMAPSSL"))
                    chkUniversalIMAPUserAuthentication.Checked = CCommon.ToBool(dt.Rows(0)("bitIMAPAuth"))
                End If
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Sub BindDatagrid()
            Try
                If radOppTab.SelectedIndex = 2 Then
                    Dim objWorkSchedule As New WorkSchedule
                    objWorkSchedule.DomainID = CCommon.ToLong(Session("DomainID"))
                    Dim dtEmployee As DataTable = objWorkSchedule.GetAll()
                    rptWorkSchedule.DataSource = dtEmployee
                    rptWorkSchedule.DataBind()

                    bizPager.PageSize = 100
                    bizPager.RecordCount = dtEmployee.Rows.Count
                    bizPager.CurrentPageIndex = 1
                Else
                    Dim dtUserList As DataTable
                    Dim objUserList As New UserGroups

                    With objUserList
                        .UserId = Session("UserID")
                        .DomainID = Session("DomainID")

                        If radOppTab.SelectedIndex = 0 Then
                            If txtSortColumnA.Text <> "" Then
                                .ColumnName = txtSortColumnA.Text
                            Else : .ColumnName = ""
                            End If
                            .columnSortOrder = txtSortOrderA.Text
                            If txtCurrrentPage.Text.Trim = "" Then txtCurrrentPage.Text = 1
                            .CurrentPage = txtCurrrentPage.Text.Trim()
                            .CustomSearch = InternalGridColumnSearchCriteria()
                        Else
                            If txtCurrrentPage.Text.Trim = "" Then txtCurrrentPage.Text = 1
                            .CurrentPage = txtCurrrentPage.Text.Trim()

                            If txtSortColumnE.Text <> "" Then
                                .ColumnName = txtSortColumnE.Text
                            Else : .ColumnName = ""
                            End If

                            .columnSortOrder = txtSortOrderE.Text
                            .CustomSearch = ExternalGridColumnSearchCriteria()
                        End If
                        .PageSize = Session("PagingRows")
                        .TotalRecords = 0
                    End With

                    If radOppTab.SelectedIndex = 0 Then
                        dtUserList = objUserList.GetUserList()
                    Else
                        dtUserList = objUserList.GetExtranetCompany()

                        If Not dtUserList Is Nothing AndAlso dtUserList.Rows.Count > 0 Then
                            For Each dr As DataRow In dtUserList.Rows
                                If Not String.IsNullOrEmpty(CCommon.ToString(dr("vcPassword"))) Then
                                    dr("vcPassword") = objCommon.Decrypt(CCommon.ToString(dr("vcPassword")))
                                End If
                            Next
                        End If
                    End If

                    bizPager.PageSize = Session("PagingRows")
                    bizPager.RecordCount = objUserList.TotalRecords
                    bizPager.CurrentPageIndex = txtCurrrentPage.Text

                    If radOppTab.SelectedIndex = 0 Then
                        dgUsers.DataSource = dtUserList
                        dgUsers.DataBind()
                    Else
                        dgExternalAccess.DataSource = dtUserList
                        dgExternalAccess.DataBind()
                    End If
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub dgUsers_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgUsers.ItemDataBound
            Try
                If e.Item.ItemType = ListItemType.Header Then
                    objCommonMaster = New CCommon
                    Dim lbUserName As LinkButton = DirectCast(e.Item.FindControl("lbUserName"), LinkButton)
                    Dim lbEmailID As LinkButton = DirectCast(e.Item.FindControl("lbEmailID"), LinkButton)
                    Dim lbLoginActivity As LinkButton = DirectCast(e.Item.FindControl("lbLoginActivity"), LinkButton)

                    lbUserName.Text = lbUserName.Text.Replace("&#9660;", "").Replace("&#9650;", "")
                    lbEmailID.Text = lbEmailID.Text.Replace("&#9660;", "").Replace("&#9650;", "")
                    lbLoginActivity.Text = lbLoginActivity.Text.Replace("&#9660;", "").Replace("&#9650;", "")

                    If Not String.IsNullOrEmpty(txtSortColumnA.Text) Then
                        Dim sortArrow As String = IIf(txtSortOrderA.Text = "DESC", "&#9660;", "&#9650;")

                        If txtSortColumnA.Text = "UserName" Then
                            lbUserName.Text = lbUserName.Text & " " & sortArrow
                        ElseIf txtSortColumnA.Text = "Email" Then
                            lbEmailID.Text = lbEmailID.Text & " " & sortArrow
                        ElseIf txtSortColumnA.Text = "LoginActivity" Then
                            lbLoginActivity.Text = lbLoginActivity.Text & " " & sortArrow
                        End If
                    Else
                        lbUserName.Text = lbUserName.Text & " " & "&#9650;"
                    End If

                    If hdnInternalUserFilter.Value.Trim.Length > 0 Then
                        Dim strValues() As String = hdnInternalUserFilter.Value.Trim(";").Split(";")

                        Dim strIDValue() As String

                        For i As Integer = 0 To strValues.Length - 1
                            strIDValue = strValues(i).Split(":")

                            Select Case strIDValue(0)
                                Case "txtUsernameHeader"
                                    DirectCast(e.Item.FindControl("txtUsernameHeader"), TextBox).Text = strIDValue(1)
                                Case "txtEmailHeader"
                                    DirectCast(e.Item.FindControl("txtEmailHeader"), TextBox).Text = strIDValue(1)
                                Case "ddlStatusHeader"
                                    If Not DirectCast(e.Item.FindControl("ddlStatusHeader"), DropDownList).Items.FindByValue(strIDValue(1)) Is Nothing Then
                                        DirectCast(e.Item.FindControl("ddlStatusHeader"), DropDownList).Items.FindByValue(strIDValue(1)).Selected = True
                                    End If
                                Case "ddlLoginHeader"
                                    If Not DirectCast(e.Item.FindControl("ddlLoginHeader"), DropDownList).Items.FindByValue(strIDValue(1)) Is Nothing Then
                                        DirectCast(e.Item.FindControl("ddlLoginHeader"), DropDownList).Items.FindByValue(strIDValue(1)).Selected = True
                                    End If
                            End Select
                        Next
                    End If
                End If
                If e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.Item Then
                    DirectCast(e.Item.FindControl("txtSMTPIMAPPassword"), TextBox).Attributes.Add("value", If(String.IsNullOrEmpty(DirectCast(e.Item.DataItem, System.Data.DataRowView)("vcEmailAliasPassword")), "", objCommonMaster.Decrypt(DirectCast(e.Item.DataItem, System.Data.DataRowView)("vcEmailAliasPassword"))))
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub txtCurrrentPage_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtCurrrentPage.TextChanged
            Try
                BindDatagrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Sub btnGo1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGo1.Click
            Try
                BindDatagrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Sub dgExternalAccess_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgExternalAccess.ItemDataBound
            Try
                If e.Item.ItemType = ListItemType.Header Then
                    Dim lbOrganization As LinkButton = DirectCast(e.Item.FindControl("lbOrganization"), LinkButton)
                    Dim lbFirstName As LinkButton = DirectCast(e.Item.FindControl("lbFirstName"), LinkButton)
                    Dim lbLastName As LinkButton = DirectCast(e.Item.FindControl("lbLastName"), LinkButton)
                    Dim lbEmailAddress As LinkButton = DirectCast(e.Item.FindControl("lbEmailAddress"), LinkButton)
                    Dim lbLoginActivity As LinkButton = DirectCast(e.Item.FindControl("lbLoginActivity"), LinkButton)

                    lbOrganization.Text = lbOrganization.Text.Replace("&#9660;", "").Replace("&#9650;", "")
                    lbFirstName.Text = lbFirstName.Text.Replace("&#9660;", "").Replace("&#9650;", "")
                    lbLastName.Text = lbLastName.Text.Replace("&#9660;", "").Replace("&#9650;", "")
                    lbEmailAddress.Text = lbEmailAddress.Text.Replace("&#9660;", "").Replace("&#9650;", "")
                    lbLoginActivity.Text = lbLoginActivity.Text.Replace("&#9660;", "").Replace("&#9650;", "")

                    If Not String.IsNullOrEmpty(txtSortColumnE.Text) Then
                        Dim sortArrow As String = IIf(txtSortOrderE.Text = "DESC", "&#9660;", "&#9650;")

                        If txtSortColumnE.Text = "Organization" Then
                            lbOrganization.Text = lbOrganization.Text & " " & sortArrow
                        ElseIf txtSortColumnE.Text = "FirstName" Then
                            lbFirstName.Text = lbFirstName.Text & " " & sortArrow
                        ElseIf txtSortColumnE.Text = "LastName" Then
                            lbLastName.Text = lbLastName.Text & " " & sortArrow
                        ElseIf txtSortColumnE.Text = "EmailAddress" Then
                            lbEmailAddress.Text = lbEmailAddress.Text & " " & sortArrow
                        ElseIf txtSortColumnE.Text = "LoginActivity" Then
                            lbLoginActivity.Text = lbLoginActivity.Text & " " & sortArrow
                        End If
                    Else
                        lbOrganization.Text = lbOrganization.Text & " " & "&#9650;"
                    End If

                    If hdnExternalUserFilter.Value.Trim.Length > 0 Then
                        Dim strValues() As String = hdnExternalUserFilter.Value.Trim(";").Split(";")

                        Dim strIDValue() As String

                        For i As Integer = 0 To strValues.Length - 1
                            strIDValue = strValues(i).Split(":")

                            Select Case strIDValue(0)
                                Case "txtOrgnizationHeader"
                                    DirectCast(e.Item.FindControl("txtOrgnizationHeader"), TextBox).Text = strIDValue(1)
                                Case "txtFirstNameHeader"
                                    DirectCast(e.Item.FindControl("txtFirstNameHeader"), TextBox).Text = strIDValue(1)
                                Case "txtLastNameHeader"
                                    DirectCast(e.Item.FindControl("txtLastNameHeader"), TextBox).Text = strIDValue(1)
                                Case "txtEmailHeader"
                                    DirectCast(e.Item.FindControl("txtEmailHeader"), TextBox).Text = strIDValue(1)
                                Case "ddlLoginHeader"
                                    If Not DirectCast(e.Item.FindControl("ddlLoginHeader"), DropDownList).Items.FindByValue(strIDValue(1)) Is Nothing Then
                                        DirectCast(e.Item.FindControl("ddlLoginHeader"), DropDownList).Items.FindByValue(strIDValue(1)).Selected = True
                                    End If
                                Case "ddlEcommerceHeader"
                                    If Not DirectCast(e.Item.FindControl("ddlEcommerceHeader"), DropDownList).Items.FindByValue(strIDValue(1)) Is Nothing Then
                                        DirectCast(e.Item.FindControl("ddlEcommerceHeader"), DropDownList).Items.FindByValue(strIDValue(1)).Selected = True
                                    End If
                                Case "ddlPortalHeader"
                                    If Not DirectCast(e.Item.FindControl("ddlPortalHeader"), DropDownList).Items.FindByValue(strIDValue(1)) Is Nothing Then
                                        DirectCast(e.Item.FindControl("ddlPortalHeader"), DropDownList).Items.FindByValue(strIDValue(1)).Selected = True
                                    End If
                            End Select
                        Next
                    End If
                ElseIf e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
                    Dim drv As DataRowView = DirectCast(e.Item.DataItem, DataRowView)
                    DirectCast(e.Item.FindControl("chkEcommerce"), CheckBox).Checked = CCommon.ToBool(drv("tintAccessAllowed"))
                    DirectCast(e.Item.FindControl("chkPortal"), CheckBox).Checked = CCommon.ToBool(drv("bitPartnerAccess"))

                    Dim lblPassword As Label = DirectCast(e.Item.FindControl("lblPassword"), Label)

                    If Not lblPassword Is Nothing Then
                        Dim cell As TableCell = CType(lblPassword.Parent, TableCell)
                        cell.Attributes.Add("id", CCommon.ToString(drv("vcPasswordInlineEditID")))
                        cell.Attributes.Add("class", "click")
                        cell.Attributes.Add("onmousemove", "bgColor='lightgoldenRodYellow'")
                        cell.Attributes.Add("onmouseout", "bgColor=''")
                        cell.Controls.Remove(lblPassword)
                        cell.Text = CCommon.ToString(drv("vcPassword"))
                    End If
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Sub radOppTab_TabClick(sender As Object, e As Telerik.Web.UI.RadTabStripEventArgs) Handles radOppTab.TabClick
            Try
                BindDatagrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Protected Sub bizPager_PageChanged(sender As Object, e As EventArgs)
            Try
                txtCurrrentPage.Text = bizPager.CurrentPageIndex
                BindDatagrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Protected Sub btnSaveChanges_Click(sender As Object, e As EventArgs)
            Try
                Dim objUniversalSMTPIMAP As New UniversalSMTPIMAP
                objUniversalSMTPIMAP.DomainID = CCommon.ToLong(Session("DomainID"))
                objUniversalSMTPIMAP.SMTPServerURL = txtUniversalSMTPURL.Text.Trim()
                objUniversalSMTPIMAP.SMTPPort = CCommon.ToInteger(txtUniversalSMTPPort.Text)
                objUniversalSMTPIMAP.IsSMPSSL = chkUniversalSMTPSSL.Checked
                objUniversalSMTPIMAP.IsSMTPUserAuthentication = chkUniversalSMTPUserAuthentication.Checked
                objUniversalSMTPIMAP.IMAPServerURL = txtUniversalIMAPURL.Text.Trim()
                objUniversalSMTPIMAP.IMAPPort = CCommon.ToInteger(txtUniversalIMAPPort.Text)
                objUniversalSMTPIMAP.IsIMAPSSL = chkUniversalIMAPSSL.Checked
                objUniversalSMTPIMAP.IsIMAPUserAuthentication = chkUniversalIMAPUserAuthentication.Checked
                objUniversalSMTPIMAP.Save()


                Dim dr As DataRow
                Dim dtUserList As New DataTable("Users")
                dtUserList.Columns.Add("numUserID", GetType(System.Int64))
                dtUserList.Columns.Add("bitActivateFlag", GetType(System.Boolean))


                Dim objUser As New UserAccess
                objUser.DomainID = CCommon.ToLong(Session("DomainID"))
                Dim objCommon As New CCommon
                For Each drItem As DataGridItem In dgUsers.Items
                    dr = dtUserList.NewRow()
                    dr("numUserID") = CCommon.ToLong(DirectCast(drItem.FindControl("hdnUserID"), HiddenField).Value)
                    dr("bitActivateFlag") = CCommon.ToLong(DirectCast(drItem.FindControl("chkActive"), CheckBox).Checked)
                    dtUserList.Rows.Add(dr)

                    objUser.UserId = CCommon.ToLong(DirectCast(drItem.FindControl("hdnUserID"), HiddenField).Value)
                    objUser.strEmail = CCommon.ToString(DirectCast(drItem.FindControl("txtEmailAlias"), TextBox).Text)

                    If Not String.IsNullOrEmpty(DirectCast(drItem.FindControl("txtSMTPIMAPPassword"), TextBox).Text.Trim()) Then
                        objUser.SMTPPassWord = objCommon.Encrypt(DirectCast(drItem.FindControl("txtSMTPIMAPPassword"), TextBox).Text.Trim())
                    Else
                        objUser.SMTPPassWord = ""
                    End If

                    objUser.UpdateUserEmailAlias()
                Next

                Dim ds As New DataSet
                ds.Tables.Add(dtUserList)

                objUser.xmlStr = ds.GetXml()
                objUser.UpdateActiveUsers()

                ScriptManager.RegisterClientScriptBlock(Me, Me.Page.GetType(), "UsersChangeSaved", "document.location.href=document.location.href;", True)
            Catch ex As Exception
                If ex.Message.Contains("FULL_USERS_EXCEED") Then
                    ScriptManager.RegisterClientScriptBlock(Me, Me.Page.GetType(), "FullUsersExceed", "alert('You have exceeded the number of full subscriber licenses. You can either unsubscribe existing licenses and replace them with new ones (i.e. swap one for another), or buy more full access licenses from BizAutomation.')", True)
                ElseIf ex.Message.Contains("LIMITED_ACCESS_USERS_EXCEED") Then
                    ScriptManager.RegisterClientScriptBlock(Me, Me.Page.GetType(), "LimitedAccessUsersExceed", "alert('You have exceeded the number of limited access subscriber licenses. You can either unsubscribe existing licenses and replace them with new ones (i.e. swap one for another), or buy more limited access licenses from BizAutomation.')", True)
                Else
                    ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                    DisplayError(ex.Message)
                End If
                
            End Try
        End Sub

        Protected Sub dgUsers_ItemCommand(source As Object, e As DataGridCommandEventArgs)
            Try
                If e.CommandName = "Sort" Then
                    If txtSortColumnA.Text <> CCommon.ToString(e.CommandArgument) Then
                        txtSortColumnA.Text = CCommon.ToString(e.CommandArgument)
                        txtSortOrderA.Text = "ASC"
                    Else
                        If txtSortOrderA.Text = "DESC" Then
                            txtSortOrderA.Text = "ASC"
                        Else
                            txtSortOrderA.Text = "DESC"
                        End If
                    End If

                    BindDatagrid()
                End If

                If e.CommandName = "Test" Then
                    Dim email As String = DirectCast(e.Item.FindControl("txtEmailAlias"), TextBox).Text.Trim()
                    Dim password As String = DirectCast(e.Item.FindControl("txtSMTPIMAPPassword"), TextBox).Text.Trim()

                    If String.IsNullOrEmpty(email) Or String.IsNullOrEmpty(password) Then
                        ScriptManager.RegisterClientScriptBlock(Me, Me.Page.GetType(), "TestValidation", "alert('E-mail Alias and SMTP & IMAP Password are required.');", True)
                    Else
                        Dim objUserAccess As New UserAccess
                        objUserAccess.UserId = CCommon.ToLong(e.CommandArgument)
                        objUserAccess.DomainID = Session("DomainID")
                        Dim dtUserAccessDetails As DataTable = objUserAccess.GetUserAccessDetails

                        If Not dtUserAccessDetails Is Nothing AndAlso dtUserAccessDetails.Rows.Count > 0 Then
                            Dim objEmail As New Email
                            Dim isValidSMTP As Boolean = False
                            Try
                                objEmail.ValidateSMTP(DirectCast(e.Item.FindControl("txtEmailAlias"), TextBox).Text.Trim(), DirectCast(e.Item.FindControl("txtSMTPIMAPPassword"), TextBox).Text.Trim(), CCommon.ToInteger(dtUserAccessDetails.Rows(0)("numSMTPPort")), CCommon.ToString(dtUserAccessDetails.Rows(0)("vcSMTPServer")), CCommon.ToBool(dtUserAccessDetails.Rows(0)("bitSMTPSSL")), CCommon.ToBool(dtUserAccessDetails.Rows(0)("bitSMTPAuth")))
                                isValidSMTP = True
                            Catch ex As Exception
                                'DO NOT RAISE ERROR
                            End Try
                            Dim isValidIMAP As Boolean = False
                            Try
                                objEmail.ValidateImap(CCommon.ToString(dtUserAccessDetails.Rows(0)("vcImapServerUrl")), CCommon.ToInteger(dtUserAccessDetails.Rows(0)("numPort")), CCommon.ToBool(dtUserAccessDetails.Rows(0)("bitUseUserName")), DirectCast(e.Item.FindControl("txtEmailAlias"), TextBox).Text.Trim(), DirectCast(e.Item.FindControl("txtSMTPIMAPPassword"), TextBox).Text.Trim(), CCommon.ToBool(dtUserAccessDetails.Rows(0)("bitSSl")))
                                isValidIMAP = True
                            Catch ex As Exception
                                'DO NOT RAISE ERROR
                            End Try
                            If isValidSMTP AndAlso isValidIMAP Then
                                DirectCast(e.Item.FindControl("lkbTest"), LinkButton).CssClass = "btn btn-xs btn-success"
                                DirectCast(e.Item.FindControl("lkbTest"), LinkButton).Text = "<i class=""fa fa-thumbs-up"" aria-hidden=""true""></i>"
                                DirectCast(e.Item.FindControl("lkbTest"), LinkButton).Visible = True

                                Dim objUser As New UserAccess
                                Dim objCommon As New CCommon
                                objUser.DomainID = CCommon.ToLong(Session("DomainID"))
                                objUser.UserId = CCommon.ToLong(e.CommandArgument)
                                objUser.strEmail = email
                                objUser.SMTPPassWord = objCommon.Encrypt(password)
                                objUser.UpdateUserEmailAlias()
                            Else
                                DirectCast(e.Item.FindControl("lkbTest"), LinkButton).CssClass = "btn btn-xs btn-danger"
                                DirectCast(e.Item.FindControl("lkbTest"), LinkButton).Text = "<i class=""fa fa-thumbs-down"" aria-hidden=""true""></i>"
                                DirectCast(e.Item.FindControl("lkbTest"), LinkButton).Visible = True

                                If isValidSMTP AndAlso Not isValidIMAP Then
                                    ScriptManager.RegisterClientScriptBlock(Me, Me.Page.GetType(), "TestValidation", "alert('SMTP configuration is valid but IMAP configuration is invalid.');", True)
                                ElseIf Not isValidSMTP AndAlso isValidIMAP Then
                                    ScriptManager.RegisterClientScriptBlock(Me, Me.Page.GetType(), "TestValidation", "alert('IMAP configuration is valid but SMTP configuration is invalid.');", True)
                                Else
                                    ScriptManager.RegisterClientScriptBlock(Me, Me.Page.GetType(), "TestValidation", "alert('SMTP & IMAP configuration is invalid. Please check email, pssword and smtp & imap configuration details.');", True)
                                End If
                            End If
                        End If


                    End If
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Protected Sub gvWorkSchedule_RowDataBound(sender As Object, e As GridViewRowEventArgs)
            
        End Sub

        Protected Sub btnSaveWorkSchedule_Click(sender As Object, e As EventArgs)
            Try
                Dim hdnWorkScheduleID As HiddenField
                Dim hdnUserCntID As HiddenField
                Dim rcbDays As Telerik.Web.UI.RadComboBox
                Dim txtWorkHours As Telerik.Web.UI.RadNumericTextBox
                Dim txtWorkMinutes As Telerik.Web.UI.RadNumericTextBox
                Dim txtProductiveHours As Telerik.Web.UI.RadNumericTextBox
                Dim txtProductiveMinutes As Telerik.Web.UI.RadNumericTextBox
                Dim txtStartHours As Telerik.Web.UI.RadNumericTextBox
                Dim txtStartMinutes As Telerik.Web.UI.RadNumericTextBox
                Dim rblStartTime As RadioButtonList
                Dim workDays As String

                Dim objWorkSchedule As New WorkSchedule
                objWorkSchedule.DomainID = CCommon.ToLong(Session("DomainID"))

                For Each row As RepeaterItem In rptWorkSchedule.Items
                    hdnWorkScheduleID = DirectCast(row.FindControl("hdnWorkScheduleID"), HiddenField)
                    hdnUserCntID = DirectCast(row.FindControl("hdnUserCntID"), HiddenField)
                    rcbDays = DirectCast(row.FindControl("rcbDays"), Telerik.Web.UI.RadComboBox)
                    txtWorkHours = DirectCast(row.FindControl("txtWorkHours"), Telerik.Web.UI.RadNumericTextBox)
                    txtWorkMinutes = DirectCast(row.FindControl("txtWorkMinutes"), Telerik.Web.UI.RadNumericTextBox)
                    txtProductiveHours = DirectCast(row.FindControl("txtProductiveHours"), Telerik.Web.UI.RadNumericTextBox)
                    txtProductiveMinutes = DirectCast(row.FindControl("txtProductiveMinutes"), Telerik.Web.UI.RadNumericTextBox)
                    txtStartHours = DirectCast(row.FindControl("txtStartHours"), Telerik.Web.UI.RadNumericTextBox)
                    txtStartMinutes = DirectCast(row.FindControl("txtStartMinutes"), Telerik.Web.UI.RadNumericTextBox)
                    rblStartTime = DirectCast(row.FindControl("rblStartTime"), RadioButtonList)

                    workDays = ""
                    For Each item As Telerik.Web.UI.RadComboBoxItem In rcbDays.CheckedItems
                        workDays = workDays & If(workDays.Length > 0, ",", "") & item.Value
                    Next

                    objWorkSchedule.WorkScheduleID = CCommon.ToLong(hdnWorkScheduleID.Value)
                    objWorkSchedule.ContactID = CCommon.ToLong(hdnUserCntID.Value)
                    objWorkSchedule.WorkDays = workDays
                    objWorkSchedule.WorkHours = CCommon.ToShort(txtWorkHours.Text)
                    objWorkSchedule.WorkMinutes = CCommon.ToShort(txtWorkMinutes.Text)
                    objWorkSchedule.ProductiveHours = CCommon.ToShort(txtProductiveHours.Text)
                    objWorkSchedule.ProductiveMinutes = CCommon.ToShort(txtProductiveMinutes.Text)
                    objWorkSchedule.StartOfDay = DateTime.ParseExact(String.Format("{0:D2}", CCommon.ToShort(txtStartHours.Text)) & ":" & String.Format("{0:D2}", CCommon.ToShort(txtStartMinutes.Text)) & " " & If(rblStartTime.SelectedValue = "1", "AM", "PM"), "hh:mm tt", System.Globalization.CultureInfo.InvariantCulture).TimeOfDay
                    objWorkSchedule.WorkMinutes = CCommon.ToShort(txtWorkMinutes.Text)
                    objWorkSchedule.Save()
                Next

                BindDatagrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Protected Sub rptWorkSchedule_ItemDataBound(sender As Object, e As RepeaterItemEventArgs)
            Try
                If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
                    Dim drv As DataRowView = DirectCast(e.Item.DataItem, DataRowView)

                    Dim rcbDays As Telerik.Web.UI.RadComboBox = DirectCast(e.Item.FindControl("rcbDays"), Telerik.Web.UI.RadComboBox)
                    Dim txtWorkHours As Telerik.Web.UI.RadNumericTextBox = DirectCast(e.Item.FindControl("txtWorkHours"), Telerik.Web.UI.RadNumericTextBox)
                    Dim txtWorkMinutes As Telerik.Web.UI.RadNumericTextBox = DirectCast(e.Item.FindControl("txtWorkMinutes"), Telerik.Web.UI.RadNumericTextBox)
                    Dim txtProductiveHours As Telerik.Web.UI.RadNumericTextBox = DirectCast(e.Item.FindControl("txtProductiveHours"), Telerik.Web.UI.RadNumericTextBox)
                    Dim txtProductiveMinutes As Telerik.Web.UI.RadNumericTextBox = DirectCast(e.Item.FindControl("txtProductiveMinutes"), Telerik.Web.UI.RadNumericTextBox)
                    Dim txtStartHours As Telerik.Web.UI.RadNumericTextBox = DirectCast(e.Item.FindControl("txtStartHours"), Telerik.Web.UI.RadNumericTextBox)
                    Dim txtStartMinutes As Telerik.Web.UI.RadNumericTextBox = DirectCast(e.Item.FindControl("txtStartMinutes"), Telerik.Web.UI.RadNumericTextBox)
                    Dim rblStartTime As RadioButtonList = DirectCast(e.Item.FindControl("rblStartTime"), RadioButtonList)

                    If Not String.IsNullOrEmpty(CCommon.ToString(drv("vcWorkDays"))) Then
                        Dim arrDays As String() = CCommon.ToString(drv("vcWorkDays")).Split(",")
                        If Not arrDays Is Nothing AndAlso arrDays.Length > 0 Then
                            For Each strDay In arrDays
                                If Not rcbDays.Items.FindItemByValue(strDay) Is Nothing Then
                                    rcbDays.Items.FindItemByValue(strDay).Checked = True
                                End If
                            Next
                        End If
                    End If
                    
                    If CCommon.ToShort(drv("numWorkHours")) > 0 Then
                        txtWorkHours.Text = CCommon.ToShort(drv("numWorkHours"))
                    End If

                    If CCommon.ToShort(drv("numWorkMinutes")) > 0 Then
                        txtWorkMinutes.Text = CCommon.ToShort(drv("numWorkMinutes"))
                    End If

                    If CCommon.ToShort(drv("numProductiveHours")) > 0 Then
                        txtProductiveHours.Text = CCommon.ToShort(drv("numProductiveHours"))
                    End If

                    If CCommon.ToShort(drv("numProductiveMinutes")) > 0 Then
                        txtProductiveMinutes.Text = CCommon.ToShort(drv("numProductiveMinutes"))
                    End If

                    If Not String.IsNullOrEmpty(CCommon.ToString(drv("tmStartOfDay"))) Then
                        Dim startOfDay As TimeSpan = DirectCast(drv("tmStartOfDay"), TimeSpan)

                        If startOfDay.Hours > 0 Then
                            If startOfDay.Hours > 12 Then
                                txtStartHours.Text = startOfDay.Hours - 12
                            Else
                                txtStartHours.Text = startOfDay.Hours
                            End If
                        End If

                        If startOfDay.Minutes > 0 Then
                            txtStartMinutes.Text = startOfDay.Minutes
                        End If

                        If startOfDay.Hours <= 12 Then
                            rblStartTime.Items.FindByValue("1").Selected = True
                        Else
                            rblStartTime.Items.FindByValue("2").Selected = True
                        End If
                    End If
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Protected Sub btnAddDaysOff_Click(sender As Object, e As EventArgs)
            Try
                Dim dtDaysOffFrom As Date
                Dim dtDaysOffTo As Date
                Dim isEmployeeSelected As Boolean = False

                If DateTime.TryParse(rdpDaydOffFrom.SelectedDate, dtDaysOffFrom) AndAlso DateTime.TryParse(rdpDaydOffTo.SelectedDate, dtDaysOffTo) Then
                    If dtDaysOffTo > dtDaysOffFrom Then
                        ScriptManager.RegisterClientScriptBlock(Me, Me.Page.GetType(), "DaysOffInvalid", "alert('Days off to can not before days off from.')", True)
                    Else
                        For Each item As RepeaterItem In rptWorkSchedule.Items
                            If DirectCast(item.FindControl("chkSelectEmployee"), CheckBox).Checked Then
                                isEmployeeSelected = True
                            End If
                        Next

                        If isEmployeeSelected Then
                            Dim objWorkScheduleDaysOff As New WorkScheduleDaysOff
                            objWorkScheduleDaysOff.DomainID = CCommon.ToLong(Session("DomainID"))

                            For Each item As RepeaterItem In rptWorkSchedule.Items
                                If DirectCast(item.FindControl("chkSelectEmployee"), CheckBox).Checked Then
                                    objWorkScheduleDaysOff.ContactID = CCommon.ToLong(DirectCast(item.FindControl("hdnUserCntID"), HiddenField).Value)
                                    objWorkScheduleDaysOff.WorkScheduleID = CCommon.ToLong(DirectCast(item.FindControl("hdnWorkScheduleID"), HiddenField).Value)
                                    objWorkScheduleDaysOff.DaysOffFrom = dtDaysOffFrom
                                    objWorkScheduleDaysOff.DaysOffTo = dtDaysOffTo
                                    objWorkScheduleDaysOff.Save()
                                End If
                            Next

                            BindDatagrid()
                        Else
                            ScriptManager.RegisterClientScriptBlock(Me, Me.Page.GetType(), "EmployeeNotSelected", "alert('Select atleast one record.')", True)
                        End If
                    End If
                Else
                    ScriptManager.RegisterClientScriptBlock(Me, Me.Page.GetType(), "DaysOffRequired", "alert('Select days off from and to.')", True)
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Protected Sub btnRemoveDaysOff_Click(sender As Object, e As EventArgs)
            Try
                Dim workScheduleID As Long = DirectCast(sender, Button).CommandArgument

                If workScheduleID > 0 Then
                    Dim objWorkScheduleDaysOff As New WorkScheduleDaysOff
                    objWorkScheduleDaysOff.WorkScheduleID = workScheduleID
                    objWorkScheduleDaysOff.Delete()
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Protected Sub dgExternalAccess_ItemCommand(source As Object, e As DataGridCommandEventArgs)
            Try
                If e.CommandName = "Sort" Then
                    If txtSortColumnE.Text <> CCommon.ToString(e.CommandArgument) Then
                        txtSortColumnE.Text = CCommon.ToString(e.CommandArgument)
                        txtSortOrderE.Text = "ASC"
                    Else
                        If txtSortOrderE.Text = "DESC" Then
                            txtSortOrderE.Text = "ASC"
                        Else
                            txtSortOrderE.Text = "DESC"
                        End If
                    End If
                End If

                BindDatagrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Function ExternalGridColumnSearchCriteria() As String
            Try
                Dim RegularSearch As String = ""

                If hdnExternalUserFilter.Value.Trim.Length > 0 Then
                    Dim strValues() As String = hdnExternalUserFilter.Value.Trim(";").Split(";")

                    Dim strIDValue(), strID(), strCustom As String
                    Dim strRegularCondition As New ArrayList
                    Dim strCustomCondition As New ArrayList


                    For i As Integer = 0 To strValues.Length - 1
                        strIDValue = strValues(i).Split(":")

                        Select Case strIDValue(0)
                            Case "txtOrgnizationHeader"
                                strRegularCondition.Add("Com.vcCompanyName ilike '%" & strIDValue(1).Replace("'", "''") & "%'")
                            Case "txtFirstNameHeader"
                                strRegularCondition.Add("A.vcFirstName ilike '%" & strIDValue(1).Replace("'", "''") & "%'")
                            Case "txtLastNameHeader"
                                strRegularCondition.Add("A.vcLastName ilike '%" & strIDValue(1).Replace("'", "''") & "%'")
                            Case "txtEmailHeader"
                                strRegularCondition.Add("A.vcEmail ilike '%" & strIDValue(1).Replace("'", "''") & "%'")
                            Case "ddlLoginHeader"
                                If strIDValue(1) = "1" Then
                                    strRegularCondition.Add("EXISTS (SELECT numAppAccessID FROM UserAccessedDTL UAD WHERE UAD.numDomainID=Div.numDomainID AND UAD.numDivisionID=Div.numDivisionID AND UAD.numContactID=A.numContactID AND dtLoggedInTime BETWEEN DATEADD(DAY,-7,GETUTCDATE()) AND GETUTCDATE())")
                                ElseIf strIDValue(1) = "2" Then
                                    strRegularCondition.Add("EXISTS (SELECT numAppAccessID FROM UserAccessedDTL UAD WHERE UAD.numDomainID=Div.numDomainID AND UAD.numDivisionID=Div.numDivisionID AND UAD.numContactID=A.numContactID AND dtLoggedInTime BETWEEN DATEADD(DAY,-30,GETUTCDATE()) AND GETUTCDATE())")
                                ElseIf strIDValue(1) = "3" Then
                                    strRegularCondition.Add("NOT EXISTS (SELECT numAppAccessID FROM UserAccessedDTL UAD WHERE UAD.numDomainID=Div.numDomainID AND UAD.numDivisionID=Div.numDivisionID AND UAD.numContactID=A.numContactID AND dtLoggedInTime BETWEEN DATEADD(DAY,-30,GETUTCDATE()) AND GETUTCDATE())")
                                ElseIf strIDValue(1) = "4" Then
                                    strRegularCondition.Add("NOT EXISTS (SELECT numAppAccessID FROM UserAccessedDTL UAD WHERE UAD.numDomainID=Div.numDomainID AND UAD.numDivisionID=Div.numDivisionID AND UAD.numContactID=A.numContactID AND dtLoggedInTime BETWEEN DATEADD(DAY,-60,GETUTCDATE()) AND GETUTCDATE())")
                                ElseIf strIDValue(1) = "5" Then
                                    strRegularCondition.Add("NOT EXISTS (SELECT numAppAccessID FROM UserAccessedDTL UAD WHERE UAD.numDomainID=Div.numDomainID AND UAD.numDivisionID=Div.numDivisionID AND UAD.numContactID=A.numContactID)")
                                End If
                            Case "ddlEcommerceHeader"
                                If strIDValue(1) = "1" Then
                                    strRegularCondition.Add("COALESCE(ExtDTL.tintAccessAllowed,0)=1")
                                ElseIf strIDValue(1) = "0" Then
                                    strRegularCondition.Add("COALESCE(ExtDTL.tintAccessAllowed,0)=0")
                                End If
                            Case "ddlPortalHeader"
                                If strIDValue(1) = "1" Then
                                    strRegularCondition.Add("COALESCE(ExtDTL.bitPartnerAccess,0)=1")
                                ElseIf strIDValue(1) = "0" Then
                                    strRegularCondition.Add("COALESCE(ExtDTL.bitPartnerAccess,0)=0")
                                End If
                        End Select
                    Next

                    RegularSearch = String.Join(" AND ", strRegularCondition.ToArray())
                End If

                Return RegularSearch
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Private Function InternalGridColumnSearchCriteria() As String
            Try
                Dim RegularSearch As String = ""

                If hdnInternalUserFilter.Value.Trim.Length > 0 Then
                    Dim strValues() As String = hdnInternalUserFilter.Value.Trim(";").Split(";")

                    Dim strIDValue(), strID(), strCustom As String
                    Dim strRegularCondition As New ArrayList
                    Dim strCustomCondition As New ArrayList


                    For i As Integer = 0 To strValues.Length - 1
                        strIDValue = strValues(i).Split(":")

                        Select Case strIDValue(0)
                            Case "txtUsernameHeader"
                                strRegularCondition.Add("(ADC.vcfirstname ilike '%" & strIDValue(1).Replace("'", "''") & "%' OR ADC.vcLastName ilike '%" & strIDValue(1).Replace("'", "''") & "%')")
                            Case "txtEmailHeader"
                                strRegularCondition.Add("UserMaster.vcEmailID ilike '%" & strIDValue(1).Replace("'", "''") & "%'")
                            Case "ddlStatusHeader"
                                If strIDValue(1) = "1" Then
                                    strRegularCondition.Add("COALESCE(UserMaster.bitActivateFlag,0) = 1")
                                ElseIf strIDValue(1) = "0" Then
                                    strRegularCondition.Add("COALESCE(UserMaster.bitActivateFlag,0) = 0")
                                End If
                            Case "ddlLoginHeader"
                                If strIDValue(1) = "1" Then
                                    strRegularCondition.Add("EXISTS (SELECT numAppAccessID FROM UserAccessedDTL UAD WHERE UAD.numDomainID=Div.numDomainID AND UAD.numDivisionID=Div.numDivisionID AND UAD.numContactID=ADC.numContactID AND dtLoggedInTime BETWEEN DATEADD(DAY,-7,GETUTCDATE()) AND GETUTCDATE())")
                                ElseIf strIDValue(1) = "2" Then
                                    strRegularCondition.Add("EXISTS (SELECT numAppAccessID FROM UserAccessedDTL UAD WHERE UAD.numDomainID=Div.numDomainID AND UAD.numDivisionID=Div.numDivisionID AND UAD.numContactID=ADC.numContactID AND dtLoggedInTime BETWEEN DATEADD(DAY,-30,GETUTCDATE()) AND GETUTCDATE())")
                                ElseIf strIDValue(1) = "3" Then
                                    strRegularCondition.Add("NOT EXISTS (SELECT numAppAccessID FROM UserAccessedDTL UAD WHERE UAD.numDomainID=Div.numDomainID AND UAD.numDivisionID=Div.numDivisionID AND UAD.numContactID=ADC.numContactID AND dtLoggedInTime BETWEEN DATEADD(DAY,-30,GETUTCDATE()) AND GETUTCDATE())")
                                ElseIf strIDValue(1) = "4" Then
                                    strRegularCondition.Add("NOT EXISTS (SELECT numAppAccessID FROM UserAccessedDTL UAD WHERE UAD.numDomainID=Div.numDomainID AND UAD.numDivisionID=Div.numDivisionID AND UAD.numContactID=ADC.numContactID AND dtLoggedInTime BETWEEN DATEADD(DAY,-60,GETUTCDATE()) AND GETUTCDATE())")
                                ElseIf strIDValue(1) = "5" Then
                                    strRegularCondition.Add("NOT EXISTS (SELECT numAppAccessID FROM UserAccessedDTL UAD WHERE UAD.numDomainID=Div.numDomainID AND UAD.numDivisionID=Div.numDivisionID AND UAD.numContactID=ADC.numContactID)")
                                End If
                        End Select
                    Next

                    RegularSearch = String.Join(" AND ", strRegularCondition.ToArray())
                End If

                Return RegularSearch
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Protected Sub btnSaveExternal_Click(sender As Object, e As EventArgs)
            Try
                Dim objSubscribers As New Subscribers
                objSubscribers.DomainID = CCommon.ToLong(Session("DomainID"))

                Dim dt As New DataTable
                dt.Columns.Add("numExtranetDtlID")
                dt.Columns.Add("tintAccessAllowed")
                dt.Columns.Add("bitPartnerAccess")

                Dim dr As DataRow
                For Each dgi As DataGridItem In dgExternalAccess.Items
                    dr = dt.NewRow()
                    dr("numExtranetDtlID") = CCommon.ToLong(DirectCast(dgi.FindControl("hdnExtranetDtlID"), HiddenField).Value)
                    dr("tintAccessAllowed") = CCommon.ToBool(DirectCast(dgi.FindControl("chkEcommerce"), CheckBox).Checked)
                    dr("bitPartnerAccess") = CCommon.ToBool(DirectCast(dgi.FindControl("chkPortal"), CheckBox).Checked)
                    dt.Rows.Add(dr)
                Next

                Dim ds As New DataSet
                ds.Tables.Add(dt)

                objSubscribers.ManageExternalUsersAccess(ds.GetXml())
            Catch ex As Exception
                If ex.Message.Contains("BUSINESS_PORTAL_USERS_EXCEED") Then
                    ScriptManager.RegisterClientScriptBlock(Me, Me.Page.GetType(), "PortalUsersExceed", "alert('You have exceeded the number of portal subscriber licenses. You can either unsubscribe existing licenses and replace them with new ones (i.e. swap one for another), or buy more portal access licenses from BizAutomation.')", True)
                Else
                    ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                    DisplayError(ex.Message)
                End If
            End Try
        End Sub
    End Class
End Namespace