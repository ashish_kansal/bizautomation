<%@ Register TagPrefix="menu1" TagName="webmenu" Src="../include/webmenu.ascx" %>

<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" CodeBehind="frmFieldView.aspx.vb"
    Inherits="BACRM.UserInterface.Admin.frmFieldView" MasterPageFile="~/common/GridMasterRegular.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Field View By Relationship </title>
    <script language="javascript">
        function Close() {
            window.close();
            return false;
        }
        function Save() {
            if (document.getElementById('ddlLocation').value == 0) {
                alert("Select Location")
                document.getElementById('ddlLocation').focus();
                return false;
            }
            if (document.getElementById('ddlLocation').value == 1) {
                if (document.getElementById('ddlRelationship').value == 0) {
                    alert(" Select Relationship")
                    document.getElementById('ddlRelationship').focus();
                    return false;
                }
            }
            if (document.getElementById('ddlLocation').value == 4) {
                if (document.getElementById('ddlContact').value == 0) {
                    alert(" Select Contact Type")
                    document.getElementById('ddlContact').focus();
                    return false;
                }
            }
            if (document.getElementById('ddltabs').value == -2) {
                alert("Select Tab")
                document.getElementById('ddltabs').focus();
                return false;
            }
            var str = '';
            for (var i = 0; i < document.getElementById('lstAddfld').options.length; i++) {
                var SelectedText, SelectedValue;
                SelectedValue = document.getElementById('lstAddfld').options[i].value;
                SelectedText = document.getElementById('lstAddfld').options[i].text;
                str = str + SelectedValue + ','
            }
            document.getElementById('txthidden').value = str;

        }
        function MoveUp(tbox) {
            var j = 0;
            for (var i = 1; i < tbox.options.length; i++) {
                if (tbox.options[i - 1] != null) {
                    if (tbox.options[i].selected && tbox.options[i].value != "") {

                        var SelectedText, SelectedValue;
                        SelectedValue = tbox.options[i].value;
                        SelectedText = tbox.options[i].text;
                        tbox.options[i].value = tbox.options[i - 1].value;
                        tbox.options[i].text = tbox.options[i - 1].text;
                        tbox.options[i - 1].value = SelectedValue;
                        tbox.options[i - 1].text = SelectedText;
                        j = i - 1;
                    }
                }
            }
            tbox.options[j].selected = true;
            return false;
        }
        function MoveDown(tbox) {
            var j = 0;
            for (var i = 0; i < tbox.options.length - 1; i++) {
                if (tbox.options[i - 1] != null) {
                    if (tbox.options[i].selected && tbox.options[i].value != "") {

                        var SelectedText, SelectedValue;
                        SelectedValue = tbox.options[i].value;
                        SelectedText = tbox.options[i].text;
                        tbox.options[i].value = tbox.options[i + 1].value;
                        tbox.options[i].text = tbox.options[i + 1].text;
                        tbox.options[i + 1].value = SelectedValue;
                        tbox.options[i + 1].text = SelectedText;
                        j = i + 1;
                    }
                }
            }
            tbox.options[j].selected = true;
            return false;
        }

        sortitems = 0;  // 0-False , 1-True
        function move(fbox, tbox) {

            for (var i = 0; i < fbox.options.length; i++) {
                if (fbox.options[i].selected && fbox.options[i].value != "") {
                    /// to check for duplicates 
                    for (var j = 0; j < tbox.options.length; j++) {
                        if (tbox.options[j].value == fbox.options[i].value) {
                            alert("Item is already selected");
                            return false;
                        }
                    }

                    var no = new Option();
                    no.value = fbox.options[i].value;
                    no.text = fbox.options[i].text;
                    tbox.options[tbox.options.length] = no;
                    fbox.options[i].value = "";
                    fbox.options[i].text = "";

                }
            }
            BumpUp(fbox);
            if (sortitems) SortD(tbox);
            return false;
        }

        function remove1(fbox, tbox) {

            for (var i = 0; i < fbox.options.length; i++) {
                if (fbox.options[i].selected && fbox.options[i].value != "") {
                    /// to check for duplicates 
                    for (var j = 0; j < tbox.options.length; j++) {
                        if (tbox.options[j].value == fbox.options[i].value) {
                            fbox.options[i].value = "";
                            fbox.options[i].text = "";
                            BumpUp(fbox);
                            if (sortitems) SortD(tbox);
                            return false;


                            //alert("Item is already selected");
                            //return false;
                        }
                    }

                    var no = new Option();
                    no.value = fbox.options[i].value;
                    no.text = fbox.options[i].text;
                    tbox.options[tbox.options.length] = no;
                    fbox.options[i].value = "";
                    fbox.options[i].text = "";

                }
            }
            BumpUp(fbox);
            if (sortitems) SortD(tbox);
            return false;
        }

        function BumpUp(box) {
            for (var i = 0; i < box.options.length; i++) {
                if (box.options[i].value == "") {
                    for (var j = i; j < box.options.length - 1; j++) {
                        box.options[j].value = box.options[j + 1].value;
                        box.options[j].text = box.options[j + 1].text;
                    }
                    var ln = i;
                    break;
                }
            }
            if (ln < box.options.length) {
                box.options.length -= 1;
                BumpUp(box);
            }
        }


        function SortD(box) {
            var temp_opts = new Array();
            var temp = new Object();
            for (var i = 0; i < box.options.length; i++) {
                temp_opts[i] = box.options[i];
            }
            for (var x = 0; x < temp_opts.length - 1; x++) {
                for (var y = (x + 1) ; y < temp_opts.length; y++) {
                    if (temp_opts[x].text > temp_opts[y].text) {
                        temp = temp_opts[x].text;
                        temp_opts[x].text = temp_opts[y].text;
                        temp_opts[y].text = temp;
                        temp = temp_opts[x].value;
                        temp_opts[x].value = temp_opts[y].value;
                        temp_opts[y].value = temp;
                    }
                }
            }
            for (var i = 0; i < box.options.length; i++) {
                box.options[i].value = temp_opts[i].value;
                box.options[i].text = temp_opts[i].text;
            }
        }
        function CheckRelorContactType() {
            if (document.getElementById('ddlLocation').value == 0) {
                alert("Select Location")
                document.getElementById('ddlLocation').focus();
                return false;
            }
            if (document.getElementById('ddltabs').value == -2) {
                alert("Select Tab")
                document.getElementById('ddltabs').focus();
                return false;
            }
            if (document.getElementById('ddlLocation').value == 1) {
                if (document.getElementById('ddlRelationship').value != 0) {
                    alert(" Deselect Relationship")
                    document.getElementById('ddlRelationship').focus();
                    return false;
                }
            }
            if (document.getElementById('ddlLocation').value == 4) {
                if (document.getElementById('ddlContact').value != 0) {
                    alert(" Deselect Contact Type")
                    document.getElementById('ddlContact').focus();
                    return false;
                }
            }
            var str = '';
            for (var i = 0; i < document.getElementById('lstAddfld').options.length; i++) {
                var SelectedText, SelectedValue;
                SelectedValue = document.getElementById('lstAddfld').options[i].value;
                SelectedText = document.getElementById('lstAddfld').options[i].text;
                str = str + SelectedValue + ','
            }
            document.getElementById('txthidden').value = str;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server"
    ClientIDMode="Static">
    <div class="row padbottom10">
        <div class="col-xs-12">
            <div class="pull-right">
                <asp:LinkButton ID="btnAddAll" runat="server" CssClass="btn btn-primary"><i class="fa fa-plus-circle"></i>&nbsp;&nbsp;Add a Custom Field to all Relationships or Contact Types</asp:LinkButton>
                <asp:LinkButton ID="btnSave" runat="server" CssClass="btn btn-primary"><i class="fa fa-floppy-o"></i>&nbsp;&nbsp;Save</asp:LinkButton>
            </div>
        </div>
    </div>
    
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server" ClientIDMode="Static">
    Map custom fields to relationship and tabs
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BizPager" runat="server" ClientIDMode="Static">
    &nbsp;
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="GridPlaceHolder" runat="server"
    ClientIDMode="Static">
    <asp:UpdateProgress ID="UpdateProgress" runat="server" ClientIDMode="Static">
        <ProgressTemplate>
            <div class="overlay">
                <div class="overlayContent" style="color: #000; text-align: center; width: 250px; padding: 20px">
                    <i class="fa fa-2x fa-refresh fa-spin"></i>
                    <h3>Processing Request</h3>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <div class="row">
        <div class="col-md-12">
            <div class="col-md-4">
                <div class="form-group">

                    <label>Location</label>
                    <asp:DropDownList ID="ddlLocation" runat="server" CssClass="form-control" AutoPostBack="True">
                    </asp:DropDownList>
                </div>
            </div>
            <div class="col-md-4" id="tdRelationship" runat="server">
                <div class="form-group">
                    <label>Relationship</label>
                    <asp:DropDownList ID="ddlRelationship" AutoPostBack="True" runat="server" CssClass="form-control">
                    </asp:DropDownList>
                </div>
            </div>
            <div class="col-md-4" id="tdContactType" runat="server">
                <div class="form-group">
                    <label>Contact Type</label>
                    <asp:DropDownList ID="ddlContact" runat="server" CssClass="form-control" AutoPostBack="True">
                    </asp:DropDownList>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label>Tab</label>
                    <asp:DropDownList ID="ddltabs" runat="server" AutoPostBack="True" CssClass="form-control">
                    </asp:DropDownList>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="col-md-3">
                <div class="form-group">

                    <label>Available Fields</label>
                    <asp:ListBox ID="lstAvailablefld" runat="server" Height="200" CssClass="form-control"></asp:ListBox>

                </div>
            </div>
            <div class="col-md-2 col-md-offset-1" style="margin-top: 60px;">
                <div class="form-group">
                    <asp:LinkButton ID="btnAdd" runat="server" CssClass="btn btn-primary">Add&nbsp;&nbsp;<i class="fa fa-arrow-right"></i></asp:LinkButton>

                    <br />
                    <br />
                    <asp:LinkButton ID="btnRemove" runat="server" CssClass="btn btn-danger"><i class="fa fa-arrow-left"></i>&nbsp;&nbsp;Remove</asp:LinkButton>

                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">

                    <label>Selected Fields</label>
                    <asp:ListBox ID="lstAddfld" runat="server" Height="200" CssClass="form-control"></asp:ListBox>
                </div>
            </div>
            <div class="col-md-2 col-md-offset-1" style="margin-top: 60px;">
                <div class="form-group">
                    <a id="btnMoveup" class="btn btn-primary" onclick="javascript:MoveUp(document.getElementById('lstAddfld'));"><i class="fa fa-arrow-up"></i></a>
                    <br />
                    <br />
                    <a id="btnMoveDown" class="btn btn-primary" onclick="javascript:MoveDown(document.getElementById('lstAddfld'));"><i class="fa fa-arrow-down"></i></a>
                </div>
            </div>

        </div>
    </div>

    <%--<asp:Table ID="tblOppr" BorderWidth="1" runat="server" Width="100%" BorderColor="black"
        GridLines="None" CssClass="aspTable">
        <asp:TableRow>
            <asp:TableCell>
                <table cellpadding="0" cellspacing="0" width="100%">
                    
                    <tr>
                        <td colspan="6">
                            <table cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td align="center" class="normal1">
                                        <br>
                                        
                                    </td>
                                    <td align="center" valign="middle">
                                       
                                    </td>
                                    <td align="center" class="normal1">
                                        <br />
                                        
                                    </td>
                                    <td align="center" valign="middle">
                                        
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>--%>
    <asp:TextBox Style="display: none" ID="txthidden" runat="server"></asp:TextBox>
</asp:Content>
