Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Common
Namespace BACRM.UserInterface.Admin

Public Class frmPortalWorkflow
    Inherits BACRMPage

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub
        Protected WithEvents btnClose As System.Web.UI.WebControls.Button
        Protected WithEvents chkCreateOpp As System.Web.UI.WebControls.CheckBox
        Protected WithEvents ddlStatus As System.Web.UI.WebControls.DropDownList
        Protected WithEvents ddlRelationShip As System.Web.UI.WebControls.DropDownList
        Protected WithEvents chkCreateBizDoc As System.Web.UI.WebControls.CheckBox
        Protected WithEvents ddlBizDocName As System.Web.UI.WebControls.DropDownList
        Protected WithEvents chkAllowAccess As System.Web.UI.WebControls.CheckBox
        Protected WithEvents txtSuccesfullURL As System.Web.UI.WebControls.TextBox
        Protected WithEvents txtUnSuccesfullURL As System.Web.UI.WebControls.TextBox
        Protected WithEvents Table2 As System.Web.UI.WebControls.Table
        Protected WithEvents btnSave As System.Web.UI.WebControls.Button
        Protected WithEvents btnSaveClose As System.Web.UI.WebControls.Button
       
    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Try
                If Not IsPostBack Then
                    

                    GetUserRightsForPage(13, 11)
                    If m_aryRightsForPage(RIGHTSTYPE.VIEW) = 0 Then Response.Redirect("../admin/authentication.aspx?mesg=AS")
                    objCommon.sb_FillComboFromDBwithSel(ddlStatus, 1, Session("DomainID"))
                    objCommon.sb_FillComboFromDBwithSel(ddlRelationShip, 5, Session("DomainID"))
                    objCommon.sb_FillComboFromDBwithSel(ddlBizDocName, 27, Session("DomainID"))
                    LoadInformation()
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Sub LoadInformation()
            Try
                Dim objUserAccces As New UserAccess
                Dim dtPortalworkflow As DataTable
                dtPortalworkflow = objUserAccces.PortalWorkflowDtls
                If dtPortalworkflow.Rows.Count > 0 Then
                    If dtPortalworkflow.Rows(0).Item("tintCreateOppStatus") = 0 Then
                        chkCreateOpp.Checked = False
                    Else : chkCreateOpp.Checked = True
                    End If
                    If Not IsDBNull(dtPortalworkflow.Rows(0).Item("numStatus")) Then
                        If Not ddlStatus.Items.FindByValue(dtPortalworkflow.Rows(0).Item("numStatus")) Is Nothing Then
                            ddlStatus.Items.FindByValue(dtPortalworkflow.Rows(0).Item("numStatus")).Selected = True
                        End If
                    End If
                    If Not IsDBNull(dtPortalworkflow.Rows(0).Item("numRelationShip")) Then
                        If Not ddlRelationShip.Items.FindByValue(dtPortalworkflow.Rows(0).Item("numRelationShip")) Is Nothing Then
                            ddlRelationShip.Items.FindByValue(dtPortalworkflow.Rows(0).Item("numRelationShip")).Selected = True
                        End If
                    End If
                    If Not IsDBNull(dtPortalworkflow.Rows(0).Item("numBizDocName")) Then
                        If Not ddlBizDocName.Items.FindByValue(dtPortalworkflow.Rows(0).Item("numBizDocName")) Is Nothing Then
                            ddlBizDocName.Items.FindByValue(dtPortalworkflow.Rows(0).Item("numBizDocName")).Selected = True
                        End If
                    End If
                    If dtPortalworkflow.Rows(0).Item("tintCreateBizDoc") = 0 Then
                        chkCreateBizDoc.Checked = False
                    Else : chkCreateBizDoc.Checked = True
                    End If
                    If dtPortalworkflow.Rows(0).Item("tintAccessAllowed") = 0 Then
                        chkAllowAccess.Checked = False
                    Else : chkAllowAccess.Checked = True
                    End If
                    txtSuccesfullURL.Text = IIf(IsDBNull(dtPortalworkflow.Rows(0).Item("vcOrderSuccessfullPage")), "", dtPortalworkflow.Rows(0).Item("vcOrderSuccessfullPage"))
                    txtUnSuccesfullURL.Text = IIf(IsDBNull(dtPortalworkflow.Rows(0).Item("vcOrderUnsucessfullPage")), "", dtPortalworkflow.Rows(0).Item("vcOrderUnsucessfullPage"))
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
            Try
                Save()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub btnSaveClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveClose.Click
            Try
                Save()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Sub Save()
            Try
                Dim objUserAccces As New UserAccess
                objUserAccces.CreateOppStatus = IIf(chkCreateOpp.Checked = True, 1, 0)
                objUserAccces.Status = ddlStatus.SelectedItem.Value
                objUserAccces.Relationship = ddlRelationShip.SelectedItem.Value
                objUserAccces.CreateBizDoc = IIf(chkCreateBizDoc.Checked = True, 1, 0)
                objUserAccces.BizDocName = ddlBizDocName.SelectedItem.Value
                objUserAccces.AccessAllowed = IIf(chkAllowAccess.Checked = True, 1, 0)
                objUserAccces.PageAfterSuccReg = txtSuccesfullURL.Text
                objUserAccces.PageAfterUnSuccReg = txtUnSuccesfullURL.Text
                objUserAccces.PortalWorkflow()
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

    End Class
End Namespace
