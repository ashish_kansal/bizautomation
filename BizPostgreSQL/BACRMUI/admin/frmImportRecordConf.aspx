<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmImportRecordConf.aspx.vb"
    Inherits="BACRM.UserInterface.Admin.frmImportRecordConf" MasterPageFile="~/common/Popup.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Import Record Configuration</title>
    <script language="javascript" src="../javascript/AdvSearchScripts.js"></script>
    <script language="javascript" type="text/javascript">
        function Close() {
            window.close();
            return false;
        }
        function Save() {
            var str = '';
            for (var i = 0; i < document.getElementById("lstAddfld").options.length; i++) {
                var SelectedValue;
                SelectedValue = document.getElementById("lstAddfld").options[i].value;
                str = str + SelectedValue + ','
            }
            document.getElementById("txthidden").value = str;
        }

        sortitems = 0;  // 0-False , 1-True
        function move(fbox, tbox) {

            for (var i = 0; i < fbox.options.length; i++) {
                if (fbox.options[i].selected && fbox.options[i].value != "") {
                    /// to check for duplicates 
                    for (var j = 0; j < tbox.options.length; j++) {
                        if (tbox.options[j].value == fbox.options[i].value) {
                            alert("Item is already selected");
                            return false;
                        }
                    }

                    var no = new Option();
                    no.value = fbox.options[i].value;
                    no.text = fbox.options[i].text;
                    tbox.options[tbox.options.length] = no;
                    fbox.options[i].value = "";
                    fbox.options[i].text = "";

                }
            }
            BumpUp(fbox);
            if (sortitems) SortD(tbox);
            return false;
        }

        function remove1(fbox, tbox) {

            for (var i = 0; i < fbox.options.length; i++) {
                if (fbox.options[i].selected && fbox.options[i].value != "") {
                    /// to check for duplicates 
                    for (var j = 0; j < tbox.options.length; j++) {
                        if (tbox.options[j].value == fbox.options[i].value) {
                            fbox.options[i].value = "";
                            fbox.options[i].text = "";
                            BumpUp(fbox);
                            if (sortitems) SortD(tbox);
                            return false;


                            //alert("Item is already selected");
                            //return false;
                        }
                    }

                    var no = new Option();
                    no.value = fbox.options[i].value;
                    no.text = fbox.options[i].text;
                    tbox.options[tbox.options.length] = no;
                    fbox.options[i].value = "";
                    fbox.options[i].text = "";

                }
            }
            BumpUp(fbox);
            if (sortitems) SortD(tbox);
            return false;
        }

        function BumpUp(box) {
            for (var i = 0; i < box.options.length; i++) {
                if (box.options[i].value == "") {
                    for (var j = i; j < box.options.length - 1; j++) {
                        box.options[j].value = box.options[j + 1].value;
                        box.options[j].text = box.options[j + 1].text;
                    }
                    var ln = i;
                    break;
                }
            }
            if (ln < box.options.length) {
                box.options.length -= 1;
                BumpUp(box);
            }
        }


        function SortD(box) {
            var temp_opts = new Array();
            var temp = new Object();
            for (var i = 0; i < box.options.length; i++) {
                temp_opts[i] = box.options[i];
            }
            for (var x = 0; x < temp_opts.length - 1; x++) {
                for (var y = (x + 1) ; y < temp_opts.length; y++) {
                    if (temp_opts[x].text > temp_opts[y].text) {
                        temp = temp_opts[x].text;
                        temp_opts[x].text = temp_opts[y].text;
                        temp_opts[y].text = temp;
                        temp = temp_opts[x].value;
                        temp_opts[x].value = temp_opts[y].value;
                        temp_opts[y].value = temp;
                    }
                }
            }
            for (var i = 0; i < box.options.length; i++) {
                box.options[i].value = temp_opts[i].value;
                box.options[i].text = temp_opts[i].text;
            }
        }
        function CheckRelorContactType() {
            if (document.getElementById("ddlLocation").value == 0) {
                alert("Select Location")
                document.getElementById("ddlLocation").focus();
                return false;
            }
            if (document.getElementById("ddltabs").value == -2) {
                alert("Select Tab")
                document.getElementById("ddltabs").focus();
                return false;
            }
            if (document.getElementById("ddlLocation").value == 1) {
                if (document.getElementById("ddlRelationship").value != 0) {
                    alert(" Deselect Relationship")
                    document.getElementById("ddlRelationship").focus();
                    return false;
                }
            }
            if (document.getElementById("ddlLocation").value == 4) {
                if (document.getElementById("ddlContact").value != 0) {
                    alert(" Deselect Contact Type")
                    document.getElementById("ddlContact").focus();
                    return false;
                }
            }
            var str = '';
            for (var i = 0; i < document.getElementById("lstAddfld").options.length; i++) {
                var SelectedText, SelectedValue;
                SelectedValue = document.getElementById("lstAddfld").options[i].value;
                SelectedText = document.getElementById("lstAddfld").options[i].text;
                str = str + SelectedValue + ','
            }
            document.getElementById("txthidden").value = str;
        }

        $(document).ready(function () {
            $("[id$=lstAddfld] option").each(function () {
                if ($("[id$=lstAvailablefld] option[value='" + this.value + "']") != null) {
                    $("[id$=lstAvailablefld] option[value='" + this.value + "']").remove();
                }
            });
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    
    <div class="input-part">
        <div class="right-input">
            <table cellspacing="2" cellpadding="2" width="100%" border="0">
                <tr>
                    <td align="right">
                        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
                        <asp:UpdatePanel ID="UpdatePanel1" runat="server" ChildrenAsTriggers="true">
                            <ContentTemplate>
                                <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btn btn-primary"></asp:Button>
                                <asp:Button ID="btnSaveClose" runat="server" Text="Save &amp; Close" CssClass="btn btn-primary"></asp:Button>
                                <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btn btn-primary"></asp:Button>
                            </ContentTemplate>
                        </asp:UpdatePanel>

                    </td>
                </tr>
            </table>
        </div>
    </div>
    <table style="width:100%">
        <tr>
            <td>
                <asp:Label ID="lblException" runat="server" ForeColor="Red"></asp:Label>
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageTitle" runat="server" ClientIDMode="Static">
    Record Configuration
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="Content" runat="server" ClientIDMode="Static">
    <table>
        <tr>
            <td>
                <table cellpadding="5" cellspacing="5" width="100%">
                    <tr>
                        <td align="center" class="normal1" valign="top">Available Fields<br />
                            <asp:ListBox ID="lstAvailablefld" runat="server" Width="400" Height="300" CssClass="signup"></asp:ListBox>
                        </td>
                        <td align="center" valign="middle">
                            <asp:Button ID="btnAdd" CssClass="button" runat="server" Text="Add >"></asp:Button>
                            <br />
                            <br />
                            <asp:Button ID="btnRemove" CssClass="button" runat="server" Text="< Remove"></asp:Button>
                        </td>
                        <td align="center" class="normal1">Selected Fields<br />
                            <asp:ListBox ID="lstAddfld" runat="server" Width="400" Height="300" CssClass="signup"></asp:ListBox>
                        </td>
                        <td align="center" valign="middle">
                            <img id="btnMoveup" src="../images/upArrow.gif" onclick="javascript:MoveUp(document.getElementById('lstAddfld'));"
                                alt="" />
                            <br />
                            <br />
                            <img id="btnMoveDown" src="../images/downArrow1.gif" onclick="javascript:MoveDown(document.getElementById('lstAddfld'));" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td id="tdLinking" runat="server" visible="false">
                <b><span style="color: red">*</span> Because this wizard includes an update option, please select a linking ID</b>
                <asp:DropDownList ID="ddlLinkingIDs" runat="server"></asp:DropDownList>
            </td>
        </tr>
    </table>
    <asp:TextBox Style="display: none" ID="txthidden" runat="server"></asp:TextBox>
</asp:Content>
