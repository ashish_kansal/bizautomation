﻿Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Common
Public Class frmUOMConversion
    Inherits BACRMPage
    Dim objCommon As CCommon

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then
                BindData()
            End If

            If IsPostBack Then
                CreateTable(hfTotalRow.Value)
            End If
            btnCancel.Attributes.Add("onclick", "return Close()")
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
    Sub BindData()
        Try
            tblConversion.Controls.Clear()
            chkItemLevelUOM.Checked = CCommon.ToBool(Session("EnableItemLevelUOM"))

            'Dim ObjRow As TableRow
            'Dim ObjCell As TableCell
            Dim txtConv As New TextBox
            Dim ddlUnit As New DropDownList

            objCommon = New CCommon

            objCommon.DomainID = Session("DomainID")
            Dim dtList As DataTable
            dtList = objCommon.GetUOMConversion()

            Dim dtUnit As DataTable
            dtUnit = objCommon.GetItemUOM()
            Dim i As Integer = 0

            For i = 0 To dtList.Rows.Count - 1
                CreateRow(i + 1)

                'txtConv = Me.FindControl("txtConv_1_" & i + 1)
                'txtConv.Text = dtList.Rows(i)("decConv1")

                ddlUnit = tblConversion.FindControl("ddlUnit_1_" & i + 1)
                If Not ddlUnit.Items.FindByValue(dtList.Rows(i)("numUOM1")) Is Nothing Then
                    ddlUnit.Items.FindByValue(dtList.Rows(i)("numUOM1")).Selected = True
                End If

                txtConv = tblConversion.FindControl("txtConv_2_" & i + 1)
                txtConv.Text = String.Format("{0:#,##0.00}", CCommon.ToDecimal(dtList.Rows(i)("decConv2")))

                ddlUnit = tblConversion.FindControl("ddlUnit_2_" & i + 1)
                If Not ddlUnit.Items.FindByValue(dtList.Rows(i)("numUOM2")) Is Nothing Then
                    ddlUnit.Items.FindByValue(dtList.Rows(i)("numUOM2")).Selected = True
                End If
                'ObjRow = New TableRow
                'ObjRow.CssClass = IIf(i = 0, "row", IIf(i / 2 = 0, "row", "arow"))
                'ObjCell = New TableCell

                'txtConv = New TextBox
                'txtConv.CssClass = "signup"
                'txtConv.ID = "txtConv_1_" & i + 1
                'txtConv.Text = dtList.Rows(i)("decConv1")
                'txtConv.Width = Unit.Pixel(400)
                'ObjCell.Controls.Add(txtConv)

                'ddlUnit = New DropDownList
                'ddlUnit.Width = Unit.Pixel(150)
                'ddlUnit.CssClass = "signup"
                'ddlUnit.ID = "ddlUnit_1_" & i + 1
                'ddlUnit.DataSource = dtUnit
                'ddlUnit.DataTextField = "vcUnitName"
                'ddlUnit.DataValueField = "numUOMId"
                'ddlUnit.DataBind()
                'ddlUnit.Items.Insert(0, "--Select One--")
                'ddlUnit.Items.FindByText("--Select One--").Value = "0"
                'If Not ddlUnit.Items.FindByValue(dtList.Rows(i)("numUOM1")) Is Nothing Then
                '    ddlUnit.Items.FindByValue(dtList.Rows(i)("numUOM1")).Selected = True
                'End If
                'ObjCell.Controls.Add(ddlUnit)

                'txtConv = New TextBox
                'txtConv.CssClass = "signup"
                'txtConv.ID = "txtConv_2_" & i + 1
                'txtConv.Text = dtList.Rows(i)("decConv1")
                'txtConv.Width = Unit.Pixel(400)
                'ObjCell.Controls.Add(txtConv)

                'ddlUnit = New DropDownList
                'ddlUnit.Width = Unit.Pixel(150)
                'ddlUnit.CssClass = "signup"
                'ddlUnit.ID = "ddlUnit_2_" & i + 1
                'ddlUnit.DataSource = dtUnit
                'ddlUnit.DataTextField = "vcUnitName"
                'ddlUnit.DataValueField = "numUOMId"
                'ddlUnit.DataBind()
                'ddlUnit.Items.Insert(0, "--Select One--")
                'ddlUnit.Items.FindByText("--Select One--").Value = "0"
                'If Not ddlUnit.Items.FindByValue(dtList.Rows(i)("numUOM1")) Is Nothing Then
                '    ddlUnit.Items.FindByValue(dtList.Rows(i)("numUOM1")).Selected = True
                'End If
                'ObjCell.Controls.Add(ddlUnit)

                'ObjRow.Cells.Add(ObjCell)
                'tblConversion.Rows.Add(ObjRow)
            Next

            hfTotalRow.Value = i
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If Page.IsValid Then
            Try
                save()
                BindData()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End If
    End Sub

    Private Sub btnSaveClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveClose.Click
        If Page.IsValid Then
            Try
                save()
                Dim strScript As String = "<script language=JavaScript>self.close()</script>"
                If (Not Page.IsStartupScriptRegistered("clientScript")) Then Page.RegisterStartupScript("clientScript", strScript)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End If
    End Sub

    Private Sub save()
        Try
            Dim dtConversion As New DataTable
            Dim drRow As DataRow
            Dim ds As New DataSet

            dtConversion.TableName = "UnitConversion"
            dtConversion.Columns.Add("numUOM1")
            dtConversion.Columns.Add("decConv1")
            dtConversion.Columns.Add("numUOM2")
            dtConversion.Columns.Add("decConv2")

            For i As Integer = 1 To CInt(hfTotalRow.Value)
                'objCommon.Conv1 = CType(Me.FindControl("txtConv_1_" & i + 1), TextBox).Text
                'objCommon.Conv2 = CType(Me.FindControl("txtConv_2_" & i + 1), TextBox).Text

                'objCommon.UnitId = CType(Me.FindControl("ddlUnit_1_" & i + 1), DropDownList).SelectedValue
                'objCommon.UnitId1 = CType(Me.FindControl("ddlUnit_2_" & i + 1), DropDownList).SelectedValue

                'objCommon.ManageUOMConversion()
                drRow = dtConversion.NewRow

                drRow("numUOM1") = CType(tblConversion.FindControl("ddlUnit_1_" & i), DropDownList).SelectedValue
                drRow("decConv1") = CCommon.ToDouble(CType(tblConversion.FindControl("lblConv_1_" & i), Label).Text)
                drRow("numUOM2") = CType(tblConversion.FindControl("ddlUnit_2_" & i), DropDownList).SelectedValue
                drRow("decConv2") = CCommon.ToDouble(CType(tblConversion.FindControl("txtConv_2_" & i), TextBox).Text)

                dtConversion.Rows.Add(drRow)
            Next
            ds.Tables.Add(dtConversion)

            objCommon = New CCommon
            objCommon.DomainID = Session("DomainID")
            objCommon.strUnitConversion = ds.GetXml
            objCommon.EnableItemLevelUOM = chkItemLevelUOM.Checked
            objCommon.ManageUOMConversion()

            Session("EnableItemLevelUOM") = chkItemLevelUOM.Checked
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Sub CreateTable(ByVal RowNo As Integer)
        Try
            For i As Integer = 1 To RowNo
                CreateRow(i)
            Next
            hfTotalRow.Value = RowNo
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Sub CreateRow(ByVal RowNo As Integer)
        Try
            Dim ObjRow As TableRow
            Dim ObjCell As TableCell
            Dim txtConv As New TextBox
            Dim lblConv As New Label

            Dim ddlUnit As New DropDownList
            Dim rfvConv As RequiredFieldValidator = New RequiredFieldValidator()
            Dim rfvUnit As RequiredFieldValidator = New RequiredFieldValidator()

            objCommon = New CCommon
            objCommon.DomainID = Session("DomainID")
            Dim dtUnit As DataTable
            dtUnit = objCommon.GetItemUOM()

            ObjRow = New TableRow
            ObjRow.CssClass = IIf((RowNo Mod 2) = 0, "row", "arow")

            ObjCell = New TableCell
            ObjCell.Width = Unit.Percentage(15)
            ObjCell.HorizontalAlign = HorizontalAlign.Center

            lblConv = New Label
            lblConv.CssClass = "signup"
            lblConv.ID = "lblConv_1_" & RowNo
            lblConv.Text = "1"
            lblConv.Font.Bold = True
            lblConv.Enabled = False
            lblConv.Width = Unit.Pixel(50)
            ObjCell.Controls.Add(lblConv)

            'rfvConv = New RequiredFieldValidator()
            'With rfvConv
            '    .ErrorMessage = "Conversion Required"
            '    .ID = "rfvConv_1_" & RowNo
            '    .ControlToValidate = txtConv.ID
            '    .Text = "*"
            '    .ValidationGroup = "vgRequired"
            '    .SetFocusOnError = True
            'End With
            'ObjCell.Controls.Add(rfvConv)

            ObjRow.Cells.Add(ObjCell)

            ObjCell = New TableCell
            ObjCell.Width = Unit.Percentage(25)
            ddlUnit = New DropDownList
            ddlUnit.Width = Unit.Pixel(150)
            ddlUnit.CssClass = "signup"
            ddlUnit.ID = "ddlUnit_1_" & RowNo
            ddlUnit.DataSource = dtUnit
            ddlUnit.DataTextField = "vcUnitName"
            ddlUnit.DataValueField = "numUOMId"
            ddlUnit.DataBind()
            ddlUnit.Items.Insert(0, "--Select One--")
            ddlUnit.Items.FindByText("--Select One--").Value = "0"
            ObjCell.Controls.Add(ddlUnit)

            rfvUnit = New RequiredFieldValidator()
            With rfvUnit
                .ErrorMessage = "Select Base Unit"
                .ID = "rfvUnit_1_" & RowNo
                .ControlToValidate = ddlUnit.ID
                .Text = "*"
                .InitialValue = "0"
                .ValidationGroup = "vgRequired"
                .SetFocusOnError = True
                .Display = ValidatorDisplay.Dynamic
            End With
            ObjCell.Controls.Add(rfvUnit)
            ObjRow.Cells.Add(ObjCell)

            ObjCell = New TableCell
            ObjCell.Width = Unit.Percentage(10)
            ObjCell.Font.Bold = True
            ObjCell.HorizontalAlign = HorizontalAlign.Center
            ObjCell.Controls.Add(New LiteralControl("="))
            ObjRow.Cells.Add(ObjCell)

            ObjCell = New TableCell
            ObjCell.Width = Unit.Percentage(25)
            txtConv = New TextBox
            txtConv.CssClass = "signup"
            txtConv.ID = "txtConv_2_" & RowNo
            txtConv.Width = Unit.Pixel(100)
            ObjCell.Controls.Add(txtConv)

            rfvConv = New RequiredFieldValidator()
            With rfvConv
                .ErrorMessage = "Conversion Required"
                .ID = "rfvConv_2_" & RowNo
                .ControlToValidate = txtConv.ID
                .Text = "*"
                .ValidationGroup = "vgRequired"
                .SetFocusOnError = True
                .Display = ValidatorDisplay.Dynamic
            End With
            ObjCell.Controls.Add(rfvConv)
            ObjRow.Cells.Add(ObjCell)

            ObjCell = New TableCell
            ObjCell.Width = Unit.Percentage(25)
            ddlUnit = New DropDownList
            ddlUnit.Width = Unit.Pixel(150)
            ddlUnit.CssClass = "signup"
            ddlUnit.ID = "ddlUnit_2_" & RowNo
            ddlUnit.DataSource = dtUnit
            ddlUnit.DataTextField = "vcUnitName"
            ddlUnit.DataValueField = "numUOMId"
            ddlUnit.DataBind()
            ddlUnit.Items.Insert(0, "--Select One--")
            ddlUnit.Items.FindByText("--Select One--").Value = "0"
            ObjCell.Controls.Add(ddlUnit)

            rfvUnit = New RequiredFieldValidator()
            With rfvUnit
                .ErrorMessage = "Select Conversion Unit"
                .ID = "rfvUnit_2_" & RowNo
                .ControlToValidate = ddlUnit.ID
                .Text = "*"
                .InitialValue = "0"
                .ValidationGroup = "vgRequired"
                .SetFocusOnError = True
                .Display = ValidatorDisplay.Dynamic
            End With
            ObjCell.Controls.Add(rfvUnit)
            ObjRow.Cells.Add(ObjCell)
            tblConversion.Rows.Add(ObjRow)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub btnAddNew_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAddNew.Click
        Try
            CreateRow(hfTotalRow.Value + 1)
            hfTotalRow.Value = hfTotalRow.Value + 1
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
End Class