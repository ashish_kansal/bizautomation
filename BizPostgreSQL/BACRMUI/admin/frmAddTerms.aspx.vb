''Modified by anoop jayaraj
Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Common
Imports System.IO
Namespace BACRM.UserInterface.Admin
    Public Class frmAddTerms
        Inherits BACRMPage
        Dim lngFormID As Long
#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub
        Protected WithEvents btnSave As System.Web.UI.WebControls.Button
        Protected WithEvents lstAvailablefld As System.Web.UI.WebControls.DropDownList
        Protected WithEvents tblAdvSearchMassUpdater As System.Web.UI.WebControls.Table
        Protected WithEvents litArrayString As System.Web.UI.WebControls.Literal
        Protected WithEvents hdOrigDbColumnName As System.Web.UI.HtmlControls.HtmlInputHidden
        Protected WithEvents hdLookBackTableName As System.Web.UI.HtmlControls.HtmlInputHidden
        Protected WithEvents hdColumnValue As System.Web.UI.HtmlControls.HtmlInputHidden
        Protected WithEvents hdEntityIdRefList As System.Web.UI.HtmlControls.HtmlInputHidden
        Protected WithEvents hdDbColumnListId As System.Web.UI.HtmlControls.HtmlInputHidden
        Protected WithEvents txtValue As System.Web.UI.WebControls.TextBox
        Protected WithEvents ddlValue As System.Web.UI.WebControls.DropDownList
        Protected WithEvents cbValue As System.Web.UI.WebControls.CheckBox
        Protected WithEvents hdDbAssociatedControl As System.Web.UI.HtmlControls.HtmlInputHidden
        Protected WithEvents rowValue As System.Web.UI.HtmlControls.HtmlTableRow
        Protected WithEvents litClientMessage As System.Web.UI.WebControls.Literal
        Protected WithEvents lblFieldName As System.Web.UI.WebControls.Label
        Protected WithEvents hdDbColumnListItemType As System.Web.UI.HtmlControls.HtmlInputHidden

        Dim objGenericAdvSearch As FormGenericAdvSearch                      'Create an object of the class FormGenericAdvSearch
        Protected WithEvents hdFieldDataType As System.Web.UI.HtmlControls.HtmlInputHidden
        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.
        Private designerPlaceholderDeclaration As System.Object

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

#Region "Variable Declaration"
        Dim objOpp As New BusinessLogic.Opportunities.OppotunitiesIP
#End Region

#Region "Page Events"

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Try
                DomainID = Session("DomainID")
                UserCntID = Session("UserContactID")
                hdnTermsID.Value = GetQueryStringVal("TermsID")

                If Not IsPostBack Then
                    If CCommon.ToLong(hdnTermsID.Value) > 0 Then
                        LoadTerms()
                    End If

                End If

            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, DomainID, UserCntID, Request)
                Response.Write(ex)
            End Try
        End Sub

#End Region

#Region "Button Events"

        Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
            Try
                If objOpp Is Nothing Then objOpp = New BusinessLogic.Opportunities.OppotunitiesIP
                With objOpp

                    .TermsID = CCommon.ToLong(hdnTermsID.Value)
                    .Terms = txtTermsName.Text.ToString().Trim()
                    .ApplyTo = CCommon.ToShort(ddlApplyTo.SelectedValue)
                    .NetDueInDays = CCommon.ToInteger(txtNetDueInDays.Text)
                    .DiscountForTerms = CCommon.ToDouble(txtDiscountPercentage.Text)
                    .DiscountPaidInDays = CCommon.ToInteger(txtDiscountPaidDays.Text)
                    .ListItemID = CCommon.ToLong(hdnListItemID.Value)
                    .DomainID = DomainID
                    .IsActive = True
                    .InterestPercentage = CCommon.ToDecimal(txtInterestPercentage.Text)
                    .InterestPercentageIfPaidAfterDays = CCommon.ToInteger(txtInterestPaidAfterDays.Text)
                    .CreatedBy = UserCntID
                    .dtCreatedDate = DateTime.UtcNow
                    .ModifiedBy = UserCntID
                    .dtModifiedDate = DateTime.UtcNow
                    .SaveTerms()
                    'hdnTermsID.Value = .TermsID
                End With

                ClientScript.RegisterClientScriptBlock(Me.GetType, "CloseForm", "SaveClose();", True)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, DomainID, UserCntID, Request)
                Response.Write(ex)
            End Try
        End Sub

#End Region

#Region "Functions/Subroutines"

        Private Sub LoadTerms()
            Try
                If objOpp Is Nothing Then objOpp = New BusinessLogic.Opportunities.OppotunitiesIP
                Dim dtTerms As New DataTable
                With objOpp
                    .DomainID = DomainID
                    .TermsID = CCommon.ToLong(hdnTermsID.Value)
                    .ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
                    dtTerms = .GetTerms()
                End With

                If dtTerms IsNot Nothing AndAlso dtTerms.Rows.Count > 0 Then
                    hdnTermsID.Value = CCommon.ToLong(dtTerms.Rows(0)("numTermsID"))
                    txtTermsName.Text = CCommon.ToString(dtTerms.Rows(0)("vcTerms"))
                    If ddlApplyTo.Items.FindByValue(CCommon.ToShort(dtTerms.Rows(0)("tintApplyTo"))) IsNot Nothing Then
                        ddlApplyTo.Items.FindByValue(CCommon.ToShort(dtTerms.Rows(0)("tintApplyTo"))).Selected = True
                    End If

                    txtNetDueInDays.Text = CCommon.ToInteger(dtTerms.Rows(0)("numNetDueInDays"))
                    txtDiscountPercentage.Text = CCommon.ToDouble(dtTerms.Rows(0)("numDiscount"))
                    txtDiscountPaidDays.Text = CCommon.ToInteger(dtTerms.Rows(0)("numDiscountPaidInDays"))
                    hdnListItemID.Value = CCommon.ToLong(dtTerms.Rows(0)("numListItemID"))
                    lblLastModifiedBy.Text = CCommon.ToString(dtTerms.Rows(0)("ModifiedBy"))

                    txtInterestPercentage.Text = CCommon.ToDecimal(dtTerms.Rows(0)("numInterestPercentage"))
                    txtInterestPaidAfterDays.Text = CCommon.ToInteger(dtTerms.Rows(0)("numInterestPercentageIfPaidAfterDays"))
                    
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

#End Region


    End Class
End Namespace
