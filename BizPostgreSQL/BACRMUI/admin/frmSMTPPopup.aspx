﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmSMTPPopup.aspx.vb"
    Inherits="frmSMTPPopup" MasterPageFile="~/common/Popup.Master" %>

<asp:Content ID="Content3" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <style>
        .info
        {
            color: #00529B;
            background-color: #BDE5F8;
            background-image: url('../images/info.png');
            border: 1px solid;
            margin: 10px 5px 0px 5px;
            padding: 15px 10px 15px 50px;
            background-repeat: no-repeat;
            background-position: 10px center;
        }
    </style>
    <script>
        function Close() {
            opener.location.reload(true);
            window.close();
        }
    </script>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <div class="input-part">
        <div class="right-input">
            <asp:Button runat="server" ID="btnSave" Text="Save" CssClass="button" Width="50" />
            <asp:Button runat="server" ID="btnClose" Text="Close" CssClass="button" Width="50" />&nbsp;&nbsp;
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="PageTitle" runat="server" ClientIDMode="Static">
    <asp:Label ID="lblTitle" runat="server" Text=" SMTP & IMAP Configuration"></asp:Label>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
    <table width="800px">
        <tr>
            <td class="normal4" align="center">
                <asp:Literal ID="litMessage" runat="server" EnableViewState="false"></asp:Literal>
            </td>
        </tr>
    </table>
    <table cellpadding="0" cellspacing="0" width="100%" class="normal1">
        <tr>
            <td align="right">
                User Name&nbsp;
            </td>
            <td>
                <asp:TextBox ID="txtUserName" runat="server" CssClass="signup" Width="200px"></asp:TextBox>
            </td>
            <td align="right">
                Password&nbsp;
            </td>
            <td>
                <asp:TextBox TextMode="Password" ID="txtPassWord" runat="server" CssClass="signup"
                    Width="200px" autocomplete="new-password"></asp:TextBox>
            </td>
            <td align="right" id="tdSender1" runat="server" visible="false">
                Sender Name&nbsp;
            </td>
            <td id="tdSender2" runat="server" visible="false">
                <asp:TextBox ID="txtSenderName" runat="server" CssClass="signup" Width="150px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td colspan="2">
            </td>
            <br />
        </tr>
        <tr>
            <td align="right">
                Server Url&nbsp;
            </td>
            <td>
                <asp:TextBox ID="txtServerUrl" runat="server" CssClass="signup" Width="200px"></asp:TextBox>
            </td>
            <td align="right">
                Port&nbsp;
            </td>
            <td>
                <asp:TextBox ID="txtServerPort" runat="server" CssClass="signup" Width="50px"></asp:TextBox>
                &nbsp;<asp:CheckBox ID="chkSSL" Text="SSL" runat="server" CssClass="normal1" />&nbsp;
                <asp:CheckBox runat="server" ID="chkSmtpAuth" CssClass="normal1" Text="Use Authentication" />
            </td>
        </tr>
    </table>
    <asp:Panel runat="server" ID="pnlSMTPError" CssClass="info">
        <b>For Google Apps Account</b><br />
        Incoming Mail (IMAP) Server : Url - imap.gmail.com,Port-993,Use SSL-checked,Use
        User Name-unchecked
        <br />
        Outgoing Mail (SMTP) Server : Url - smtp.gmail.com,Port - 587 or 465,Use SSL-checked,Use
        Authentication-checked
    </asp:Panel>
</asp:Content>
