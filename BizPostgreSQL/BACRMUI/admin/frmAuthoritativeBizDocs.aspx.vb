Imports BACRM.BusinessLogic.Common
Partial Public Class frmAuthoritativeBizDocs
    Inherits BACRMPage
   


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            GetUserRightsForPage(13, 20)

            If m_aryRightsForPage(RIGHTSTYPE.VIEW) = 0 Then
                Response.Redirect("../admin/authentication.aspx?mesg=AS")
            ElseIf m_aryRightsForPage(RIGHTSTYPE.ADD) = 0 Then
                btnSave.Visible = False
            End If

            If Not IsPostBack Then
                
                LoadDropdown()
                LoadSaveDetails()
            End If
            btnClose.Attributes.Add("onclick", "return Close()")
            btnSave.Attributes.Add("onclick", "return Save()")
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try

    End Sub

    Private Sub LoadDropdown()
        Try
            objCommon.DomainID = Session("DomainID")
            ' objCommon.ListID = 27
            'dtTable = objCommon.GetBizDocs
            Dim dtTable As DataTable
            objCommon.BizDocType = 1
            dtTable = objCommon.GetBizDocType
            For Each dr As DataRow In dtTable.Rows
                If dr("numListItemID") = CCommon.ToLong(Session("AuthoritativeSalesBizDoc")) Then
                    dr("vcData") = dr("vcData") & " - Authoritative"
                End If
            Next

            ddlSalesOpportunity.DataSource = dtTable
            ddlSalesOpportunity.DataTextField = "vcData"
            ddlSalesOpportunity.DataValueField = "numListItemID"
            ddlSalesOpportunity.DataBind()
            ddlSalesOpportunity.Items.Insert(0, New ListItem("--Select One --", "0"))

            objCommon.BizDocType = 2
            dtTable = objCommon.GetBizDocType
            For Each dr As DataRow In dtTable.Rows
                If dr("numListItemID") = CCommon.ToLong(Session("AuthoritativePurchaseBizDoc")) Then
                    dr("vcData") = dr("vcData") & " - Authoritative"
                End If
            Next

            ddlPurchaseOpportunity.DataSource = dtTable
            ddlPurchaseOpportunity.DataTextField = "vcData"
            ddlPurchaseOpportunity.DataValueField = "numListItemID"
            ddlPurchaseOpportunity.DataBind()
            ddlPurchaseOpportunity.Items.Insert(0, New ListItem("--Select One --", "0"))

        Catch ex As Exception
            Throw ex
        End Try

    End Sub

    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            objCommon = New CCommon
            Dim lintcount As Integer
            objCommon.DomainID = Session("DomainID")
            lintcount = objCommon.GetAuthoritativeBizDocs()
            objCommon.numAuthoritativeMode = lintcount
            objCommon.numAuthoritativePurchase = ddlPurchaseOpportunity.SelectedItem.Value
            objCommon.numAuthoritativeSales = ddlSalesOpportunity.SelectedItem.Value
            objCommon.SaveAuthoritativeBizDocs()
            Session("AuthoritativeSalesBizDoc") = ddlSalesOpportunity.SelectedValue
            Session("AuthoritativePurchaseBizDoc") = ddlPurchaseOpportunity.SelectedValue
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub LoadSaveDetails()
        Try
            Dim dtAuthoritativeBizdocs As DataTable
            objCommon.DomainID = Session("DomainID")
            dtAuthoritativeBizdocs = objCommon.GetAuthoritativeBizDocsDetails()
            If dtAuthoritativeBizdocs.Rows.Count > 0 Then
                If Not IsDBNull(dtAuthoritativeBizdocs.Rows(0).Item("numAuthoritativePurchase")) Then
                    If Not ddlPurchaseOpportunity.Items.FindByValue(dtAuthoritativeBizdocs.Rows(0).Item("numAuthoritativePurchase")) Is Nothing Then
                        ddlPurchaseOpportunity.Items.FindByValue(dtAuthoritativeBizdocs.Rows(0).Item("numAuthoritativePurchase")).Selected = True
                    End If
                    objCommon.numAuthoritativePurchase = dtAuthoritativeBizdocs.Rows(0).Item("numAuthoritativePurchase")
                End If

                If Not IsDBNull(dtAuthoritativeBizdocs.Rows(0).Item("numAuthoritativeSales")) Then
                    If Not ddlSalesOpportunity.Items.FindByValue(dtAuthoritativeBizdocs.Rows(0).Item("numAuthoritativeSales")) Is Nothing Then
                        ddlSalesOpportunity.Items.FindByValue(dtAuthoritativeBizdocs.Rows(0).Item("numAuthoritativeSales")).Selected = True
                    End If
                    objCommon.numAuthoritativeSales = dtAuthoritativeBizdocs.Rows(0).Item("numAuthoritativeSales")
                End If
                objCommon.DomainID = Session("DomainID")
                objCommon.GetAuthorizativeBizDocsFromOpportunity()
                If objCommon.CountSalesBizDocs > 0 Then
                    ddlSalesOpportunity.Enabled = False
                End If
                If objCommon.CountPurchaseBizDocs > 0 Then ddlPurchaseOpportunity.Enabled = False
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

End Class