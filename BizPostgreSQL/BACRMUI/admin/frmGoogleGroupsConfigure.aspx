﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmGoogleGroupsConfigure.aspx.vb"
    Inherits=".frmGoogleGroupsConfigure" MasterPageFile="~/common/Popup.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <div class="input-part">
        <div class="right-input">
            <table cellspacing="0" cellpadding="0" width="100%">
                <tr>
                    <td align="right" height="23">
                        <asp:Button runat="server" ID="btnSave" Text="Save" CssClass="button" Width="50" />
                        <asp:Button runat="server" ID="btnClose" Text="Close" CssClass="button" Width="50" />
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageTitle" runat="server" ClientIDMode="Static">
    <table>
        <tr>
            <td id="cellCaption" runat="server">
                Groups Configuration
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="Content" runat="server" ClientIDMode="Static">
    <asp:Table ID="tblConfigureGroups" runat="server" GridLines="None" CssClass="aspTable"
        BorderColor="black" BorderWidth="1" Width="500px">
        <asp:TableRow>
            <asp:TableCell>
                <table>
                    <tr>
                        <td class="normal1" align="right">
                            Group
                        </td>
                        <td class="normal1">
                            <asp:DropDownList ID="ddlGroups" Width="207" runat="server" CssClass="signup">
                            </asp:DropDownList>
                            <asp:CheckBoxList ID="cblGroups" runat="server" CssClass="signup" RepeatColumns="3"
                                RepeatDirection="Horizontal">
                            </asp:CheckBoxList>
                        </td>
                    </tr>
                </table>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
</asp:Content>
