'================================================================================
'===============================================================================
'Modified By Anoop Jayaraj
'================================================================================

Imports System.Data.SqlClient
Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Marketing
Namespace BACRM.UserInterface.Admin
    Public Class AdminBusinessProcess
        Inherits BACRMPage
       
        Dim objAdmin As CAdmin
#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Try
                btnSave.Attributes.Add("onclick", "return Save()")
                
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
            Try
                If objAdmin Is Nothing Then objAdmin = New CAdmin
                Dim slpid As Long = 0
                objAdmin.DomainID = Session("DomainId")
                objAdmin.UserCntID = Session("UserContactId")
                objAdmin.Configuration = ddlStag.SelectedValue
                objAdmin.SalesProsName = txtName.Text
                objAdmin.ProsType = ddlProcess.SelectedValue
                slpid = objAdmin.ManageSalesProcessList()
                txtName.Text = ""
                Dim str As String
                str = "<script>window.opener.location.href='../Admin/frmAdminBusinessProcess.aspx?slp_id=" & slpid & "&ProcessId=" & ddlProcess.SelectedValue & "';self.close()</script>"
                Response.Write(str)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

    End Class
End Namespace
