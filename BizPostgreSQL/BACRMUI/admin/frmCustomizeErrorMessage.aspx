﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmCustomizeErrorMessage.aspx.vb"
    Inherits=".frmCustomizeErrorMessage" MasterPageFile="~/common/ECommerceMenuMaster.Master" %>

<%@ Register Src="~/ECommerce/SiteSwitch.ascx" TagName="SiteSwitch" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Custom Error Messages</title>.
    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server"
    ClientIDMode="Static">
    <div class="row">
        <div class="col-md-12" id="Table2" runat="server">
            <div class="pull-right form-inline">
                <asp:Label ID="Label1" Text=" Application " Visible="false" CssClass="form-control" runat="server"></asp:Label>
                <asp:DropDownList runat="server" ID="ddlApplication" AutoPostBack="true" Visible="false" CssClass="form-control">
                    <asp:ListItem Text="--Select--" Value="0"></asp:ListItem>
                    <asp:ListItem Text="Biz Automation" Value="1">
                    </asp:ListItem>
                    <asp:ListItem Text="Portal" Value="2">
                    </asp:ListItem>
                    <asp:ListItem Text="E-Commerce" Selected="True" Enabled="true" Value="3">
                    </asp:ListItem>
                </asp:DropDownList>
                <asp:Label ID="lblSite" Text=" Site " runat="server"></asp:Label>
                <asp:DropDownList ID="ddlSites" runat="server" AutoPostBack="true" CssClass="form-control">
                </asp:DropDownList>
                  <asp:LinkButton ID="btnSave" CssClass="btn btn-primary" runat="server"><i class="fa fa fa-floppy-o"></i>&nbsp;&nbsp;Save</asp:LinkButton>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server" ClientIDMode="Static">
    Custom Error Messages&nbsp;<a href="#" onclick="return OpenHelpPopUp('admin/frmCustomizeErrorMessage.aspx')"><label class="badge bg-yellow">?</label></a>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BizPager" runat="server" ClientIDMode="Static">
    &nbsp;
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="GridPlaceHolder" runat="server"
    ClientIDMode="Static">
    <asp:UpdateProgress ID="UpdateProgress" runat="server" ClientIDMode="Static">
        <ProgressTemplate>
            <div class="overlay">
                <div class="overlayContent" style="color: #000; text-align: center; width: 250px; padding: 20px">
                    <i class="fa fa-2x fa-refresh fa-spin"></i>
                    <h3>Processing Request</h3>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:Table ID="tbl1" CellPadding="0" CellSpacing="0" runat="server" Width="100%"
        CssClass="aspTable" GridLines="None" Height="400">

        <asp:TableRow>
            <asp:TableCell VerticalAlign="Top">
                <asp:GridView ID="gvErrorDescription" runat="server" AutoGenerateColumns="False"
                    CssClass="table table-striped table-bordered" AllowSorting="True" Width="100%" DataKeyNames="numErrorId" ItemStyle-HorizontalAlign="Center" UseAccessibleHeader="true">
                    <AlternatingRowStyle CssClass="ais" />
                    <RowStyle CssClass="is" />
                    <HeaderStyle CssClass="hs" />
                    <Columns>
                        <asp:BoundField DataField="vcErrorCode" Visible="true" HeaderText="Error Code" ItemStyle-Width="20%" />
                        <asp:BoundField DataField="DescParent" Visible="true" HeaderText="Description" />
                        <asp:TemplateField HeaderText="Set Description" ItemStyle-Width="40%">
                            <ItemTemplate>
                                <asp:TextBox ID="txtSetDescription" runat="server" Text='<%# Eval("DescChild") %>'
                                    CssClass="form-control" AutoComplete="OFF" MaxLength="250">
                                </asp:TextBox>
                                <asp:HiddenField ID="hdSetDescription" runat="server" Value='<%# Eval("DescChild") %>' />
                                <asp:HiddenField ID="hdnumErrorId" runat="server" Value='<%# Eval("numErrorId") %>' />
                                <asp:HiddenField ID="hdnumErrorDetailId" runat="server" Value='<%# Eval("numErrorDetailId") %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <PagerStyle ForeColor="#000066" BackColor="White"></PagerStyle>
                </asp:GridView>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
</asp:Content>
