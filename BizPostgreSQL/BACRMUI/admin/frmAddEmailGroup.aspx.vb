Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.common
Imports BACRM.BusinessLogic.Contacts
imports System.Text

Namespace BACRM.UserInterface.Admin
    Public Class frmAddEmailGroup
        Inherits BACRMPage
        Dim SelectAll As String
        Dim GetPC As String
        Dim strContId As String
        Dim ContactId As Array
        Dim dtResults As DataTable
        Dim dtResultsDB As DataTable
        Dim delete As Boolean
        Dim save As Boolean
        Dim i As Integer
        Dim booldelete As Boolean
       
        Dim objCommon As CCommon

        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            Try
                If Not IsPostBack Then
                    
                    objCommon = New CCommon
                    GetUserRightsForPage(9, 5)
                    If m_aryRightsForPage(RIGHTSTYPE.VIEW) = 0 Then
                        Response.Redirect("../admin/authentication.aspx?mesg=AC")
                    Else
                        If m_aryRightsForPage(RIGHTSTYPE.ADD) = 0 Then
                            btnSaveClose.Visible = False
                            btnSaveGroup.Visible = False
                            btnSaveGroupExist.Visible = False
                        ElseIf m_aryRightsForPage(RIGHTSTYPE.UPDATE) = 0 Then
                            btnSaveClose.Visible = False
                            btnSaveGroup.Visible = False
                            btnSaveGroupExist.Visible = False
                        End If
                        If m_aryRightsForPage(RIGHTSTYPE.DELETE) = 0 Then
                            btnDeleteGroup.Visible = False
                            btnRemove.Visible = False
                        End If
                    End If
                    If Not (Session("dtResults") Is Nothing) Then Session("dtResults") = Nothing
                    txtCurrrentPage.Text = 1
                    'txtGroupName.Visible = False
                    labelGroup.Text = "Select Group"
                    LoadDropdowns()
                    BindDataGrid(False, False)
                    btnSaveClose.Attributes.Add("onclick", "return Save(1)")
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Sub BindDataGrid(ByVal delete As Boolean, ByVal save As Boolean)
            Try
                Dim objAdmin As New BAddEmailGroup
                SelectAll = GetQueryStringVal( "SelectAll")
                If (SelectAll = "true") Then
                    GetPC = GetQueryStringVal( "GetPC")
                    Dim SortChar As Char
                    If ViewState("SortChar") <> "" Then
                        SortChar = ViewState("SortChar")
                    Else : SortChar = "0"
                    End If
                    With objAdmin
                        .ViewID = 0
                        .QueryWhereCondition = Session("WhereCondition")
                        If GetPC = "true" Then
                            .QueryWhereCondition = .QueryWhereCondition & " and ADC.bitPrimaryContact=true "
                        End If
                        .AreasOfInt = Session("AreasOfInt")
                        .DomainID = Session("DomainID")
                        .UserCntID = Session("UserContactID")
                        .AuthenticationGroupID = Session("UserGroupID")
                        If txtCurrrentPage.Text.Trim <> "" Then
                            .CurrentPage = txtCurrrentPage.Text
                        Else : .CurrentPage = 1
                        End If
                        .PageSize = Session("PagingRows")
                        .TotalRecords = 0
                        .UserRightType = 3
                        .FormID = 1

                        If txtCurrrentPage.Text.Trim <> "" Then
                            .CurrentPage = txtCurrrentPage.Text
                        Else : .CurrentPage = 1
                        End If
                        .PageSize = Session("PagingRows")
                        .TotalRecords = 0
                        If Session("Asc") = 1 Then
                            .columnSortOrder = "Desc"
                        Else : .columnSortOrder = "Asc"
                        End If
                        .SortCharacter = ""
                        Dim i As Integer
                        If SelectAll = True Then
                            .GetAll = True
                            dtResults = .AdvancedSearch()
                            Session("dtResults") = dtResults

                            'For i = 0 To dtResults.Rows.Count - 1
                            '    Try
                            '        Dim objContact As New CContacts
                            '        objContact.ContactID = dtResults.Rows(i).Item(0)
                            '        objContact.RemoveContact()
                            '    Catch ex As Exception

                            '    End Try
                            'Next
                            .GetAll = False
                        End If
                        dtResults = .AdvancedSearch()
                        Session("dtResults") = dtResults

                        'If (save = True Or delete = True) Then
                        '    Dim dsNew As New DataSet

                        '    If dtResults.Rows.Count > 0 Then
                        '        dtResults.TableName = "Table"
                        '        dsNew.Tables.Add(dtResults.Copy)

                        '        objAdmin.DomainID = Session("DomainID")
                        '        If ddlGroup.SelectedValue = "0" Then
                        '            objAdmin.strGroupName = txtGroupName.Text
                        '            objAdmin.strGroupID = "0"
                        '        Else
                        '            objAdmin.strGroupName = ""
                        '            objAdmin.strGroupID = ddlGroup.SelectedValue
                        '        End If
                        '        .UserCntID = Session("UserContactID")
                        '        objAdmin.ContactId = dsNew.GetXml
                        '        dsNew.Tables.Remove(dsNew.Tables(0))
                        '    End If
                        '    objAdmin.Save()

                        'End If

                        ' If (delete = True) Then


                        'Dim intRow As Integer = 0
                        'Dim numContactId As Integer = 0
                        'Dim numContacTa As Double = 0
                        'Dim intRows As Integer = dgAddEmailGroup.Items.Count - 1
                        'Dim blnChecked As Boolean = False
                        'Dim GridItem As DataGridItem
                        'Dim dtTable As New DataTable

                        'For intRow = 0 To intRows
                        '    GridItem = dgAddEmailGroup.Items(intRow)
                        '    numContactId = CInt(GridItem.Cells(0).Text().Trim())
                        '    blnChecked = DirectCast(GridItem.FindControl("chkDelete"), CheckBox).Checked

                        '    If blnChecked Then
                        '        objAdmin.ContactId = numContactId
                        '        objAdmin.RemoveContact()
                        '    End If
                        'Next
                        'BindDataGrid(False, False)
                        ' End If

                        If .TotalRecords = 0 Then
                            hidenav.Visible = False
                            lblRecordCount.Text = 0
                        Else
                            hidenav.Visible = True
                            lblRecordCount.Text = String.Format("{0:#,###}", .TotalRecords)
                            Dim strTotalPage As String()
                            Dim decTotalPage As Decimal
                            decTotalPage = lblRecordCount.Text / Session("PagingRows")
                            decTotalPage = Math.Round(decTotalPage, 2)
                            strTotalPage = CStr(decTotalPage).Split(".")
                            If (lblRecordCount.Text Mod Session("PagingRows")) = 0 Then
                                lblTotal.Text = strTotalPage(0)
                                txtTotalPage.Text = strTotalPage(0)
                            Else
                                lblTotal.Text = strTotalPage(0) + 1
                                txtTotalPage.Text = strTotalPage(0) + 1
                            End If
                            txtTotalRecords.Text = lblRecordCount.Text
                        End If
                    End With
                Else
                    If (SelectAll = "false") Then
                        strContId = GetQueryStringVal( "strContID")
                        If Not (strContId = "") Then
                            objAdmin.ContactId = strContId
                            dtResults = objAdmin.getContact()
                            Session("dtResults") = dtResults  'keeping in session
                            hidenav.Visible = False
                        End If
                    End If
                End If
                dtResults = Session("dtResults")
                dgAddEmailGroup.DataSource = dtResults
                dgAddEmailGroup.DataBind()
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub LoadDropdowns()
            Try
                Dim objAdmin As New BAddEmailGroup
                objAdmin.UserCntID = Session("UserContactID")
                objAdmin.DomainID = Session("DomainID")
                ddlGroup.DataSource = objAdmin.LoadDropdown()
                ddlGroup.DataTextField = "Name"
                ddlGroup.DataValueField = "GroupId"
                ddlGroup.DataBind()
                ddlGroup.Items.Insert(0, "--Select One--")
                ddlGroup.Items.FindByText("--Select One--").Value = 0
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub lnkLast_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkLast.Click
            Try
                txtCurrrentPage.Text = txtTotalPage.Text
                BindDataGrid(False, False)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub lnkPrevious_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkPrevious.Click
            Try
                If txtCurrrentPage.Text = 1 Then
                Else : txtCurrrentPage.Text = txtCurrrentPage.Text - 1
                End If
                BindDataGrid(False, False)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub lnkFirst_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkFirst.Click
            Try
                txtCurrrentPage.Text = 1
                BindDataGrid(False, False)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub lnkNext_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkNext.Click
            Try
                If txtCurrrentPage.Text = txtTotalPage.Text Then
                Else : txtCurrrentPage.Text = txtCurrrentPage.Text + 1
                End If
                BindDataGrid(False, False)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub lnk2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnk2.Click
            Try
                If txtCurrrentPage.Text + 1 = txtTotalPage.Text Or txtCurrrentPage.Text + 1 > txtTotalPage.Text Then
                Else : txtCurrrentPage.Text = txtCurrrentPage.Text + 2
                End If
                BindDataGrid(False, False)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub lnk3_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnk3.Click
            Try
                If txtCurrrentPage.Text + 2 = txtTotalPage.Text Or txtCurrrentPage.Text + 2 > txtTotalPage.Text Then
                Else : txtCurrrentPage.Text = txtCurrrentPage.Text + 3
                End If
                BindDataGrid(False, False)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub lnk4_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnk4.Click
            Try
                If txtCurrrentPage.Text + 3 = txtTotalPage.Text Or txtCurrrentPage.Text + 3 > txtTotalPage.Text Then
                Else : txtCurrrentPage.Text = txtCurrrentPage.Text + 4
                End If
                BindDataGrid(False, False)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub lnk5_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnk5.Click
            Try
                If txtCurrrentPage.Text + 4 = txtTotalPage.Text Or txtCurrrentPage.Text + 4 > txtTotalPage.Text Then
                Else : txtCurrrentPage.Text = txtCurrrentPage.Text + 5
                End If
                BindDataGrid(False, False)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub btnRemove_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRemove.Click
            Try
                Dim i As Integer = 0
                Dim intRow As Integer = 0
                Dim numContactId As Double = 0
                Dim numContacTa As Double = 0
                Dim intRows As Integer = dgAddEmailGroup.Items.Count - 1
                Dim blnChecked As Boolean = False
                Dim GridItem As DataGridItem
                Dim dtTable As DataTable
                Dim objAdmin As New BAddEmailGroup
                Dim dsNew As New DataSet
                dtTable = Session("dtResults")          'getting the session table

                For intRow = 0 To intRows
                    GridItem = dgAddEmailGroup.Items(intRow)
                    numContactId = CInt(GridItem.Cells(0).Text().Trim())
                    blnChecked = DirectCast(GridItem.FindControl("chkDelete"), CheckBox).Checked
                    If blnChecked Then
                        If dtTable.Rows.Count > 0 Then                      'looping through the datatable to delte the row checked
                            While (i <= dtTable.Rows.Count - 1)
                                numContacTa = CInt(dtTable.Rows(i).Item("numContactId"))
                                If numContacTa = numContactId Then
                                    dtTable.Rows(i).Delete()
                                    dtTable.AcceptChanges()
                                    Exit While
                                End If
                                i += 1
                            End While
                        End If
                    End If
                Next
                If dtTable.Rows.Count > 0 Then
                    dtTable.TableName = "Table"
                    dsNew.Tables.Add(dtTable.Copy)
                    If ddlGroup.SelectedValue = "0" Then
                        objAdmin.strGroupName = txtGroupName.Text
                        objAdmin.strGroupID = "0"
                    Else
                        objAdmin.strGroupName = ""
                        objAdmin.strGroupID = ddlGroup.SelectedValue
                    End If

                    objAdmin.DomainID = Session("DomainID")
                    objAdmin.UserCntID = Session("UserContactID")
                    objAdmin.ContactId = dsNew.GetXml
                    dsNew.Tables.Remove(dsNew.Tables(0))
                End If
                objAdmin.RemoveContact()

                Session("dtResults") = dtTable  'storing the edited table in session
                dgAddEmailGroup.DataSource = dtTable
                dgAddEmailGroup.DataBind()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub btnSaveClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveClose.Click
            Try
                SelectAll = GetQueryStringVal( "SelectAll")
                Dim objAdmin As New BAddEmailGroup
                Dim dsNew As New DataSet
                Dim dtTable As DataTable

                dtTable = Session("dtResults")
                If dtTable.Rows.Count > 0 Then
                    dtTable.TableName = "Table"
                    dsNew.Tables.Add(dtTable.Copy)
                    If ddlGroup.SelectedValue = "0" Then
                        objAdmin.strGroupName = txtGroupName.Text
                        objAdmin.strGroupID = "0"
                    Else
                        objAdmin.strGroupName = ""
                        objAdmin.strGroupID = ddlGroup.SelectedValue
                    End If

                    objAdmin.DomainID = Session("DomainID")
                    objAdmin.UserCntID = Session("UserContactID")
                    objAdmin.ContactId = dsNew.GetXml
                    dsNew.Tables.Remove(dsNew.Tables(0))
                End If
                objAdmin.Save()
                Session("dtResults") = Nothing   'removing from session
                Response.Write("<script>self.close();</script>")
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub btnSaveGroup_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveGroup.Click
            Try
                'txtGroupName.Visible = True
                'ddlGroup.Visible = False
                btnDeleteGroup.Visible = False
                btnSaveGroupExist.Visible = True
                btnSaveGroup.Visible = False
                btnSaveClose.Attributes.Add("onclick", "return Save(2)")
                labelGroup.Text = "Enter Group Name"
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub btnSaveGroupExist_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveGroupExist.Click
            Try
                'txtGroupName.Visible = False
                'ddlGroup.Visible = True
                btnSaveGroupExist.Visible = False
                btnDeleteGroup.Visible = True
                btnSaveGroup.Visible = True
                labelGroup.Text = "Select Group"
                btnSaveClose.Attributes.Add("onclick", "return Save(1)")
                LoadDropdowns()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub ddlGroup_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGroup.SelectedIndexChanged
            Try
                Dim objAdmin As New BAddEmailGroup
                Dim dsNew As New DataSet
                Dim dtTable As DataTable
                Dim dtTableDB As DataTable
                dtTable = Session("dtResults")
                If Not (ddlGroup.SelectedValue = 0) Then
                    If Not (Session("dtResults") Is Nothing) Then
                        If dtTable.Rows.Count > 0 Then
                            dtTable.TableName = "Table"
                            dsNew.Tables.Add(dtTable.Copy)
                            If ddlGroup.SelectedValue <> "0" Then objAdmin.strGroupID = ddlGroup.SelectedValue
                            objAdmin.DomainID = Session("DomainID")
                            objAdmin.UserCntID = Session("UserContactID")
                            objAdmin.ContactId = dsNew.GetXml
                            dsNew.Tables.Remove(dsNew.Tables(0))
                        End If
                        dtTableDB = objAdmin.getContactData()
                        dtTable.Merge(dtTableDB)
                        dgAddEmailGroup.DataSource = dtTable
                        dgAddEmailGroup.DataBind()
                    Else
                        objAdmin.strGroupID = ddlGroup.SelectedValue
                        objAdmin.DomainID = Session("DomainID")
                        objAdmin.UserCntID = Session("UserContactID")
                        objAdmin.ContactId = "<NewDataSet> <Table>    <numContactId>0</numContactId>    <vcFirstName>\</vcFirstName>    <vcLastName>\</vcLastName>    <vcEmail /><vcCompanyName>\</vcCompanyName>  </Table></NewDataSet>"
                        dtTableDB = objAdmin.getContactData()
                        Session("dtResults") = dtTableDB
                        dgAddEmailGroup.DataSource = dtTableDB
                        dgAddEmailGroup.DataBind()
                    End If
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
            Try
                Session("dtResults") = Nothing   'removing from session
                Response.Write("<script>self.close();</script>")
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub btnDeleteGroup_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDeleteGroup.Click
            Try
                Dim objEmailGroup As New BAddEmailGroup
                objEmailGroup.strGroupID = ddlGroup.SelectedValue
                objEmailGroup.DeleteEmailGroup()
                LoadDropdowns()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Protected Sub rdbNew_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs)
            If rdbNew.Checked Then
                btnSaveClose.Attributes.Add("onclick", "return Save(2)")
            Else
                btnSaveClose.Attributes.Add("onclick", "return Save(1)")
            End If
        End Sub
    End Class
End Namespace
