''' ----------------------------------------------------------------------------
''' Project	 : BACRM.UserInterface.Admin
''' Class	 : frmUserGroups
''' 
''' -----------------------------------------------------------------------------
''' 
''' <summary>
'''     This form is used to set the user rights to each screen and modules.
''' </summary>
''' <remarks>
''' </remarks>
''' <history>
''' 	[Maha]	17/06/2005	Created
''' </history>
''' -----------------------------------------------------------------------------

Imports System.Web.UI
Imports System.Data
Imports System.Drawing.Color
Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Common
Namespace BACRM.UserInterface.Admin
    Public Class frmUserGroups
        Inherits BACRMPage

#Region "Declaration"


        Dim objchkModulePage As CheckBox
        Protected WithEvents ddlModule As System.Web.UI.WebControls.DropDownList
        Protected WithEvents ddlGroup As System.Web.UI.WebControls.DropDownList
        Protected WithEvents btnTabView As System.Web.UI.WebControls.Button
        Protected WithEvents btnNewGroup As System.Web.UI.WebControls.LinkButton
        Protected WithEvents btnSave As System.Web.UI.WebControls.LinkButton
        'Protected WithEvents btnCancel As System.Web.UI.WebControls.Button
        Protected WithEvents Table2 As System.Web.UI.WebControls.Table
        Protected WithEvents tblAuthorizedActionsHeader As System.Web.UI.WebControls.Table
        Protected WithEvents tblAuthorizedActionsList As System.Web.UI.WebControls.Table
        Protected WithEvents btnPortalBizDocs As System.Web.UI.WebControls.Button
        Dim bolDisplayLinks As Boolean  'Whether links to modify the Pages etc are to be enabled.

#End Region

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
            Me.ID = "frmUserGroups"

        End Sub

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub


#End Region

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This event fires whenever the page loaded.
        ''' </summary>
        ''' <param name="sender">Represents the sender as object.</param>
        ''' <param name="e">Represents the event args.</param>
        ''' <remarks>
        ''' </remarks>
        ''' <history>
        ''' 	[Maha]	17/06/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Load
            Try
                If Not IsPostBack Then
                    DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ""
                    DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "none")
                    PopulateAuthorizedGroupListData()   'Populate group list data
                    PopulateModuleList()   'Populate module list data
                End If
                If Session("UserContactID") <> Session("AdminID") Then 'Logged in User is Admin of Domain? yes then override permission and give him access


                    GetUserRightsForPage(13, 25)
                    If m_aryRightsForPage(RIGHTSTYPE.VIEW) = 0 Then
                        Response.Redirect("../admin/authentication.aspx?mesg=AS")
                    ElseIf m_aryRightsForPage(RIGHTSTYPE.UPDATE) = 0 Then
                        btnSave.Visible = False
                    End If
                End If

                If Session("UserGroupID") IsNot Nothing Then
                    For Each item As ListItem In ddlGroup.Items
                        If item.Value.StartsWith(Session("UserGroupID") & "~") Then
                            item.Attributes.Add("style", "color:green")
                        End If
                    Next
                End If
                litMessage.Text = ""
                btnNewGroup.Attributes.Add("onclick", "return NewGroup();")

                Dim controlName As String = Page.Request.Params("__EVENTTARGET")

                If controlName Is Nothing Or (Not controlName Is Nothing AndAlso Not controlName.Contains("ddlModule") AndAlso Not controlName.Contains("ddlGroup")) Then
                    If CCommon.ToLong(ddlGroup.SelectedValue.Split("~")(0)) > 0 AndAlso (CCommon.ToLong(ddlModule.SelectedValue) > 0 Or CCommon.ToLong(ddlModule.SelectedValue) = -1) Then
                        PopulateCheckBoxData()
                    End If
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        ''' This method is used to retrieve all the System Modules and populate into the 
        ''' dropdown list.
        ''' </summary>
        ''' <remarks>
        ''' This method calls the data access layer and in which it calls the appropriate
        ''' stored procedure from there.
        ''' </remarks>
        ''' <history>
        ''' 	[Maha]	14/06/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Sub PopulateModuleList()
            Try
                ddlModule.Items.Clear()  'Clear all the items
                Dim objUserGroups As New UserGroups
                Dim dtModuleName As DataTable
                objUserGroups.GroupType = ddlGroup.SelectedValue.Split("~")(1)
                dtModuleName = objUserGroups.GetAllSystemModules

                ddlModule.DataSource = dtModuleName.DataSet.Tables(0).DefaultView
                ddlModule.DataTextField = "vcModuleName"
                ddlModule.DataValueField = "numModuleID"
                ddlModule.DataBind()
                ddlModule.Items.Insert(0, "--Select One--")
                ddlModule.Items.FindByText("--Select One--").Value = 0

                If objUserGroups.GroupType <> 2 AndAlso objUserGroups.GroupType <> 4 Then
                    ddlModule.Items.Insert(1, New ListItem("All Modules", "-1"))
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        ''' Executes whenever the Page loads and requests the population of groups data
        ''' </summary>
        ''' <param name="intGroupID"></param>
        ''' <remarks>
        ''' </remarks>
        ''' <history>
        ''' 	[Maha]	14/06/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Sub PopulateAuthorizedGroupListData()
            Try
                ddlGroup.Items.Clear()  'Clear all the items

                Dim objUserGroups As New UserGroups
                Dim dtGroupName As DataTable
                objUserGroups.DomainId = Session("DomainID")
                objUserGroups.SelectedGroupTypes = "1,2,4"
                dtGroupName = objUserGroups.GetAutorizationGroup


                ddlGroup.DataSource = dtGroupName.DataSet.Tables(0).DefaultView
                ddlGroup.DataTextField = "vcGroupName"
                ddlGroup.DataValueField = "numGroupID1"
                ddlGroup.DataBind()
                ddlGroup.Items.Insert(0, "--Select One--")
                ddlGroup.Items.FindByText("--Select One--").Value = 0

                Dim radItem As Telerik.Web.UI.RadComboBoxItem
                For Each item As ListItem In ddlGroup.Items
                    If item.Value <> "0" AndAlso item.Value.Split("~")(0) <> Session("UserGroupID") Then
                        radItem = New Telerik.Web.UI.RadComboBoxItem
                        radItem.Text = item.Text
                        radItem.Value = item.Value
                        rcbGroups.Items.Add(radItem)
                    End If
                Next
                If Session("UserGroupID") IsNot Nothing Then
                    For Each item As ListItem In ddlGroup.Items
                        If item.Value.StartsWith(Session("UserGroupID") & "~") Then
                            item.Selected = True
                        End If
                    Next
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub PopulateCheckBoxData()
            Try
                Dim objUserGroups As New UserGroups
                Dim dtRecModules As DataTable
                Dim dtRecModulesPage As DataTable

                CType(tblAuthorizedActionsHeader.Rows(0).Cells(0).Controls(0), Literal).Text = "<b>" & ddlModule.SelectedItem.Text & "</b>"   'the module name

                objUserGroups.ModuleId = ddlModule.SelectedItem.Value
                objUserGroups.GroupId = CCommon.ToLong(ddlGroup.SelectedItem.Value.Split("~")(0))
                dtRecModulesPage = objUserGroups.GetAllSelectedSystemModulesPagesAndAccesses

                If ddlModule.SelectedValue = 4 Or ddlModule.SelectedValue = 11 Or ddlModule.SelectedValue = 2 Or ddlModule.SelectedValue = 10 Or ddlModule.SelectedValue = 12 Or ddlModule.SelectedValue = 3 Or ddlModule.SelectedValue = 7 Then
                    chkLayoutGridConfiguration.Visible = True
                Else
                    chkLayoutGridConfiguration.Visible = False
                End If

                objUserGroups.GroupType = ddlGroup.SelectedValue.Split("~")(1)
                Dim dtModuleName As DataTable = objUserGroups.GetAllSystemModules

                If CCommon.ToLong(ddlModule.SelectedValue) = -1 Then
                    For Each drModule As DataRow In dtModuleName.Rows
                        Dim tblRowPageAuthRights As New TableRow
                        Dim cell As New TableHeaderCell
                        cell.Text = CCommon.ToString(drModule("vcModuleName"))
                        cell.ColumnSpan = 21
                        tblRowPageAuthRights.Cells.Add(cell)
                        tblAuthorizedActionsList.Rows.Add(tblRowPageAuthRights)

                        If dtRecModulesPage.Select("numModuleID=" & CCommon.ToLong(drModule("numModuleID"))).Length > 0 Then
                            LoadModulePages(dtRecModulesPage.Select("numModuleID=" & CCommon.ToLong(drModule("numModuleID"))).CopyToDataTable())
                        End If
                    Next
                Else
                    LoadModulePages(dtRecModulesPage)
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub LoadModulePages(ByVal dtRecModulesPage As DataTable)
            Try
                Dim hdnModuleID, hdnPageID As HiddenField
                Dim lblModuleName, lblPageName As Label   'Declare a label
                Dim chkModulePage As CheckBox   'Declare a check box
                Dim radioModulePage As RadioButton   'Declare a Radio
                Dim strSqlModulePages As String 'declare a string to contain query for retreiving pages in the module
                Dim intRowCounter, intColsCounter As Integer   'set a counter variable
                Dim charFirstIndex As String    'The first cahsr of Add, delete, modify etc
                Dim litControl As LiteralControl    'Declare a literal control

                Dim tblCellPageAuthRights As TableCell  'Declare a html table cell object
                Dim tblRowPageAuthRights As TableRow  'Declare a html table row object

                intRowCounter = 1   'Set the default value to the row counter
                intColsCounter = 0  'Set the default column counter

                Dim boolCheckedStatus, boolCheckAnyActionAllowed As Boolean    'A boolean variable
                Dim intCount As Integer
                Dim innerCount As Integer

                For innerCount = 0 To dtRecModulesPage.Rows.Count - 1


                    tblRowPageAuthRights = New TableRow     'init a table row object
                    'tblRowPageAuthRights.BackColor = White
                    If intRowCounter Mod 2 Then tblRowPageAuthRights.Attributes.Add("class", "tr1")
                    lblPageName = New Label     'init a lable control
                    lblPageName.Text = IIf(Len(dtRecModulesPage.Rows(innerCount).Item("vcPageDesc")) > 70, Left(dtRecModulesPage.Rows(innerCount).Item("vcPageDesc"), 67) & "...", dtRecModulesPage.Rows(innerCount).Item("vcPageDesc"))
                    lblPageName.ToolTip = dtRecModulesPage.Rows(innerCount).Item("vcPageDesc")

                    lblPageName.CssClass = "text"   'Set the css stype
                    tblCellPageAuthRights = New TableCell   'init a tabel cell object
                    tblCellPageAuthRights.Style.Item("width") = "18%"   'The page name occupies 58%
                    tblCellPageAuthRights.Controls.Add(lblPageName) 'Add the label to the first column

                    hdnModuleID = New HiddenField()
                    hdnModuleID.ID = "hdnModuleID_" & CCommon.ToLong(dtRecModulesPage.Rows(innerCount)("numModuleID")) & "_" & intRowCounter
                    hdnModuleID.ClientIDMode = UI.ClientIDMode.AutoID
                    hdnModuleID.Value = CCommon.ToLong(dtRecModulesPage.Rows(innerCount)("numModuleID"))
                    tblCellPageAuthRights.Controls.Add(hdnModuleID)

                    hdnPageID = New HiddenField()
                    hdnPageID.ID = "hdnPageID_" & CCommon.ToLong(dtRecModulesPage.Rows(innerCount)("numModuleID")) & "_" & intRowCounter
                    hdnPageID.ClientIDMode = UI.ClientIDMode.AutoID
                    hdnPageID.Value = CCommon.ToLong(dtRecModulesPage.Rows(innerCount)("numPageID"))
                    tblCellPageAuthRights.Controls.Add(hdnPageID)

                    If (dtRecModulesPage.Rows(innerCount).Item("vcToolTip").ToString.Length > 0) Then
                        Dim lblToolTip As Label
                        lblToolTip = New Label
                        lblToolTip.Text = "[?]"
                        lblToolTip.CssClass = "tip"
                        lblToolTip.ToolTip = dtRecModulesPage.Rows(innerCount).Item("vcToolTip")
                        tblCellPageAuthRights.Controls.Add(lblToolTip)
                    End If

                    tblRowPageAuthRights.Cells.Add(tblCellPageAuthRights)   'Add thetable cell to the table row

                    For Each column As DataColumn In dtRecModulesPage.Columns
                        If column.ColumnName.ToLower() = "intviewallowed" Or _
                            column.ColumnName.ToLower() = "intaddallowed" Or _
                            column.ColumnName.ToLower() = "intupdateallowed" Or _
                            column.ColumnName.ToLower() = "intdeleteallowed" Or _
                            column.ColumnName.ToLower() = "intexportallowed" Then
                            For intColsCounter = -1 To 2
                                If intColsCounter = 0 Then 'All records
                                    charFirstIndex = "A"
                                ElseIf intColsCounter = 1 Then 'Owner records
                                    charFirstIndex = "O"
                                ElseIf intColsCounter = 2 Then 'Territory records only
                                    charFirstIndex = "T"
                                End If
                                tblCellPageAuthRights = New TableCell   'Init a table cell object
                                tblCellPageAuthRights.Style.Item("width") = "4%"   'The other fields occupies 3%
                                tblCellPageAuthRights.VerticalAlign = VerticalAlign.Top 'Align the contents to the top
                                tblCellPageAuthRights.HorizontalAlign = HorizontalAlign.Center 'Align the contents to the center
                                If (column.ColumnName.ToString() = "intViewAllowed" And dtRecModulesPage.Rows(innerCount).Item("bitIsViewApplicable")) Or (column.ColumnName.ToString() = "intAddAllowed" And dtRecModulesPage.Rows(innerCount).Item("bitIsAddApplicable")) Or (column.ColumnName.ToString() = "intUpdateAllowed" And dtRecModulesPage.Rows(innerCount).Item("bitIsUpdateApplicable")) Or (column.ColumnName.ToString() = "intDeleteAllowed" And dtRecModulesPage.Rows(innerCount).Item("bitIsDeleteApplicable")) Or (column.ColumnName.ToString() = "intExportAllowed" And dtRecModulesPage.Rows(innerCount).Item("bitIsExportApplicable")) Then   'All records
                                    boolCheckedStatus = False   'deault value
                                    If IsDBNull(dtRecModulesPage.Rows(innerCount).Item(column.ColumnName.ToString())) Then
                                        boolCheckedStatus = False
                                    ElseIf charFirstIndex = "A" And dtRecModulesPage.Rows(innerCount).Item(column.ColumnName.ToString()) = 3 Then 'All records
                                        boolCheckedStatus = True
                                    ElseIf charFirstIndex = "O" And dtRecModulesPage.Rows(innerCount).Item(column.ColumnName.ToString()) = 1 Then 'Owner records
                                        boolCheckedStatus = True
                                    ElseIf charFirstIndex = "T" And dtRecModulesPage.Rows(innerCount).Item(column.ColumnName.ToString()) = 2 Then 'Territory records only
                                        boolCheckedStatus = True
                                    End If
                                    If intColsCounter = -1 Then
                                        chkModulePage = New CheckBox    'Init a Checkbox
                                        chkModulePage.ID = "chkBx_" & dtRecModulesPage.Rows(innerCount).Item("numModuleID") & "_" & dtRecModulesPage.Rows(innerCount).Item("numPageID") & "_" & column.ColumnName.ToString() & "_Allowed"  'Setting the ID
                                        If IsDBNull(dtRecModulesPage.Rows(innerCount).Item(column.ColumnName.ToString())) Then
                                            boolCheckAnyActionAllowed = False    'Now rights has been allowed
                                        ElseIf dtRecModulesPage.Rows(innerCount).Item(column.ColumnName.ToString()) > 0 Then
                                            boolCheckAnyActionAllowed = True    'Some rights has been allowed
                                        Else : boolCheckAnyActionAllowed = False    'Now rights has been allowed
                                        End If
                                        chkModulePage.Checked = boolCheckAnyActionAllowed   'Mark checked / Unchecked
                                        chkModulePage.Attributes.Add("onclick", "javascript:fbValidateInputs('" & dtRecModulesPage.Rows(innerCount).Item("numModuleID") & "_" & dtRecModulesPage.Rows(innerCount).Item("numPageID") & "_" & column.ColumnName.ToString() & "')")
                                        tblCellPageAuthRights.Controls.Add(chkModulePage)   'Add the radio to the table cell
                                        tblRowPageAuthRights.Cells.Add(tblCellPageAuthRights)   'Add the table cell to the table row
                                    End If
                                    If intColsCounter <> -1 Then
                                        If (column.ColumnName.ToLower() = "intviewallowed" AndAlso intColsCounter = 0 AndAlso CCommon.ToBool(dtRecModulesPage.Rows(innerCount).Item("bitViewPermission3Applicable"))) Or _
                                            (column.ColumnName.ToLower() = "intviewallowed" AndAlso intColsCounter = 1 AndAlso CCommon.ToBool(dtRecModulesPage.Rows(innerCount).Item("bitViewPermission1Applicable"))) Or _
                                            (column.ColumnName.ToLower() = "intviewallowed" AndAlso intColsCounter = 2 AndAlso CCommon.ToBool(dtRecModulesPage.Rows(innerCount).Item("bitViewPermission2Applicable"))) Or _
                                            (column.ColumnName.ToLower() = "intaddallowed" AndAlso intColsCounter = 0 AndAlso CCommon.ToBool(dtRecModulesPage.Rows(innerCount).Item("bitAddPermission3Applicable"))) Or _
                                            (column.ColumnName.ToLower() = "intaddallowed" AndAlso intColsCounter = 1 AndAlso CCommon.ToBool(dtRecModulesPage.Rows(innerCount).Item("bitAddPermission1Applicable"))) Or _
                                            (column.ColumnName.ToLower() = "intaddallowed" AndAlso intColsCounter = 2 AndAlso CCommon.ToBool(dtRecModulesPage.Rows(innerCount).Item("bitAddPermission2Applicable"))) Or _
                                            (column.ColumnName.ToLower() = "intupdateallowed" AndAlso intColsCounter = 0 AndAlso CCommon.ToBool(dtRecModulesPage.Rows(innerCount).Item("bitUpdatePermission3Applicable"))) Or _
                                            (column.ColumnName.ToLower() = "intupdateallowed" AndAlso intColsCounter = 1 AndAlso CCommon.ToBool(dtRecModulesPage.Rows(innerCount).Item("bitUpdatePermission1Applicable"))) Or _
                                            (column.ColumnName.ToLower() = "intupdateallowed" AndAlso intColsCounter = 2 AndAlso CCommon.ToBool(dtRecModulesPage.Rows(innerCount).Item("bitUpdatePermission2Applicable"))) Or _
                                            (column.ColumnName.ToLower() = "intdeleteallowed" AndAlso intColsCounter = 0 AndAlso CCommon.ToBool(dtRecModulesPage.Rows(innerCount).Item("bitDeletePermission3Applicable"))) Or _
                                            (column.ColumnName.ToLower() = "intdeleteallowed" AndAlso intColsCounter = 1 AndAlso CCommon.ToBool(dtRecModulesPage.Rows(innerCount).Item("bitDeletePermission1Applicable"))) Or _
                                            (column.ColumnName.ToLower() = "intdeleteallowed" AndAlso intColsCounter = 2 AndAlso CCommon.ToBool(dtRecModulesPage.Rows(innerCount).Item("bitDeletePermission2Applicable"))) Or _
                                            (column.ColumnName.ToLower() = "intexportallowed" AndAlso intColsCounter = 0 AndAlso CCommon.ToBool(dtRecModulesPage.Rows(innerCount).Item("bitExportPermission3Applicable"))) Or _
                                            (column.ColumnName.ToLower() = "intexportallowed" AndAlso intColsCounter = 1 AndAlso CCommon.ToBool(dtRecModulesPage.Rows(innerCount).Item("bitExportPermission1Applicable"))) Or _
                                            (column.ColumnName.ToLower() = "intexportallowed" AndAlso intColsCounter = 2 AndAlso CCommon.ToBool(dtRecModulesPage.Rows(innerCount).Item("bitExportPermission2Applicable"))) Then
                                            radioModulePage = New RadioButton    'Init a Radio
                                            radioModulePage.ID = "rBtn_" & dtRecModulesPage.Rows(innerCount).Item("numModuleID") & "_" & dtRecModulesPage.Rows(innerCount).Item("numPageID") & "_" & column.ColumnName.ToString() & "_" & charFirstIndex 'Setting the ID
                                            radioModulePage.GroupName = "rBtn_" & dtRecModulesPage.Rows(innerCount).Item("numModuleID") & "_" & dtRecModulesPage.Rows(innerCount).Item("numPageID") & "_" & column.ColumnName.ToString()    'Set the group name
                                            radioModulePage.Checked = boolCheckedStatus   'Mark checked / Unchecked
                                            tblCellPageAuthRights.Controls.Add(radioModulePage)   'Add the radio to the table cell
                                        End If
                                    End If
                                    If 1 <> 1 And charFirstIndex = "T" Then radioModulePage.Enabled = False
                                Else
                                    litControl = New LiteralControl("&nbsp;")
                                    tblCellPageAuthRights.Controls.Add(litControl)   'Add the literal to the table cell
                                End If
                                tblRowPageAuthRights.Cells.Add(tblCellPageAuthRights)   'Add the table cell to the table row
                            Next
                        End If
                    Next

                    tblAuthorizedActionsList.Rows.Add(tblRowPageAuthRights)    'Add the table row to the table

                    intRowCounter += 1  'Increment the row counter
                Next
            Catch ex As Exception
                Throw
            End Try
        End Sub

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        ''' 
        ''' </summary>
        ''' <param name="sender"></param>
        ''' <param name="e"></param>
        ''' <remarks>
        ''' </remarks>
        ''' <history>
        ''' 	[Maha]	15/06/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
            Try
                Dim strSqlModulePages As String 'declare a string to contain query for retreiving pages in the module
                Dim intCounter, intRowCounter, intStartTableColumnCounter, intColumnCounter As Integer   'set a counter variable
    
                Dim listItemGroup As New System.Collections.Generic.List(Of Long)
                If CCommon.ToLong(ddlGroup.SelectedValue.Split("~")(0)) > 0 Then
                    listItemGroup.Add(ddlGroup.SelectedValue.Split("~")(0))
                End If

                For Each item As Telerik.Web.UI.RadComboBoxItem In rcbGroups.CheckedItems
                    listItemGroup.Add(item.Value)
                Next

                For Each groupID As Long In listItemGroup
                    For Each tr As TableRow In tblAuthorizedActionsList.Rows
                        SavePermission(groupID, tr, "intViewAllowed", 1)
                        SavePermission(groupID, tr, "intAddAllowed", 5)
                        SavePermission(groupID, tr, "intUpdateAllowed", 9)
                        SavePermission(groupID, tr, "intDeleteAllowed", 13)
                        SavePermission(groupID, tr, "intExportAllowed", 17)
                    Next
                Next

                litMessage.Text = "<span class='btn btn-success'>Authorization saved successfully.</span>"

                'remove cached Auth rights dataset file for currunt domain
                Dim strApplicationDataSetName As String = "UserAuthRightsDataSet_" & CCommon.ToLong(Session("DomainID")).ToString()
                Application(strApplicationDataSetName) = Nothing
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Sub SavePermission(ByVal groupID As Long, ByVal tr As TableRow, ByVal columnName As String, ByVal startIndex As Integer)
            Try
                Dim radioModulePageCheckBox As RadioButton
                Dim moduleID As Long = 0
                Dim pageID As Long = 0
                For Each objControl As Control In tr.Cells(0).Controls
                    If CCommon.ToString(objControl.ID).StartsWith("hdnModuleID_") Then
                        moduleID = CCommon.ToLong(DirectCast(objControl, HiddenField).Value)
                    ElseIf CCommon.ToString(objControl.ID).StartsWith("hdnPageID_") Then
                        pageID = CCommon.ToLong(DirectCast(objControl, HiddenField).Value)
                    End If
                Next

                If moduleID > 0 AndAlso pageID > 0 Then
                    Dim objUserGroups As New UserGroups
                    objUserGroups.ModuleId = moduleID
                    objUserGroups.GroupId = groupID
                    objUserGroups.PageId = pageID
                    objUserGroups.DomainID = Session("DomainID")
                    objUserGroups.ColumnName = columnName   ' daRecModulesPages.GetName(intCounter)

                    Dim ActionCheckBoxFlag As String = "None" ' Indicate no checkboxes are checked

                    'All records
                    If tr.Cells(startIndex + 1).Controls.Count > 0 Then
                        If tr.Cells(startIndex + 1).Controls(0).GetType().ToString() = "System.Web.UI.WebControls.RadioButton" Then
                            radioModulePageCheckBox = CType(tr.Cells(startIndex + 1).Controls(0), RadioButton)  'Init a radio
                            If radioModulePageCheckBox.Checked Then
                                ActionCheckBoxFlag = "All"
                                objUserGroups.ColumnValue = 3
                            End If
                        End If
                    End If

                    'Owner records
                    If tr.Cells(startIndex + 2).Controls.Count > 0 Then
                        If tr.Cells(startIndex + 2).Controls(0).GetType().ToString() = "System.Web.UI.WebControls.RadioButton" Then
                            radioModulePageCheckBox = CType(tr.Cells(startIndex + 2).Controls(0), RadioButton)  'Init a radio
                            If radioModulePageCheckBox.Checked Then
                                ActionCheckBoxFlag = "Owner"
                                objUserGroups.ColumnValue = 1
                            End If
                        End If
                    End If

                    'Territory records
                    If tr.Cells(startIndex + 3).Controls.Count > 0 Then
                        If tr.Cells(startIndex + 3).Controls(0).GetType().ToString() = "System.Web.UI.WebControls.RadioButton" Then
                            radioModulePageCheckBox = CType(tr.Cells(startIndex + 3).Controls(0), RadioButton)  'Init a radio
                            If radioModulePageCheckBox.Checked Then
                                ActionCheckBoxFlag = "Territory"
                                objUserGroups.ColumnValue = 2
                            End If
                        End If
                    End If

                    If ActionCheckBoxFlag = "None" Then objUserGroups.ColumnValue = 0

                    objUserGroups.SetAllSelectedSystemModulesPagesAndAccesses()
                End If
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Private Sub ddlGroup_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGroup.SelectedIndexChanged
            Try
                CntCheckBox()
                ddlModule.Items.Clear()
                PopulateModuleList()
                lblTooltip.Visible = False

                If CCommon.ToLong(ddlGroup.SelectedValue.Split("~")(0)) > 0 AndAlso (CCommon.ToLong(ddlModule.SelectedValue) > 0 Or CCommon.ToLong(ddlModule.SelectedValue) = -1) Then
                    PopulateCheckBoxData()
                Else
                    tblAuthorizedActionsList.Rows.Clear()
                End If

                rcbGroups.Items.Clear()
                Dim radItem As Telerik.Web.UI.RadComboBoxItem
                For Each item As ListItem In ddlGroup.Items
                    If item.Value <> "0" AndAlso item.Value <> ddlGroup.SelectedValue AndAlso item.Value.Split("~")(1) <> "4" AndAlso item.Value.Split("~")(1) <> "5" AndAlso item.Value.Split("~")(1) <> "6" Then
                        radItem = New Telerik.Web.UI.RadComboBoxItem
                        radItem.Text = item.Text
                        radItem.Value = item.Value.Split("~")(0)
                        rcbGroups.Items.Add(radItem)
                    End If
                Next
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Sub ddlModule_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlModule.SelectedIndexChanged
            Try
                If ddlModule.SelectedValue = "4" Or ddlModule.SelectedValue = "2" Or ddlModule.SelectedValue = "3" Or ddlModule.SelectedValue = "32" Then
                    lblTooltip.Visible = True
                Else
                    lblTooltip.Visible = False
                End If
                CntCheckBox()

                If CCommon.ToLong(ddlGroup.SelectedValue.Split("~")(0)) > 0 AndAlso (CCommon.ToLong(ddlModule.SelectedValue) > 0 Or CCommon.ToLong(ddlModule.SelectedValue) = -1) Then
                    PopulateCheckBoxData()
                Else
                    tblAuthorizedActionsList.Rows.Clear()
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Sub CntCheckBox()
            Try
                CheckAll.Checked = False
                Checkbox3.Checked = False
                Checkbox10.Checked = False
                Checkbox14.Checked = False
                Checkbox20.Checked = False
                Checkbox21.Checked = False
                Checkbox22.Checked = False
                Checkbox23.Checked = False
                Checkbox24.Checked = False
                Checkbox25.Checked = False
                Checkbox26.Checked = False
                Checkbox27.Checked = False
                Checkbox28.Checked = False
                Checkbox29.Checked = False
                Checkbox30.Checked = False
                Checkbox31.Checked = False
                Checkbox32.Checked = False
                Checkbox33.Checked = False
                Checkbox34.Checked = False
                Checkbox7.Checked = False

                'If ddlGroupType.SelectedValue <> 1 Then
                If 1 <> 1 Then
                    Checkbox10.Visible = False
                    Checkbox22.Visible = False
                    Checkbox26.Visible = False
                    Checkbox30.Visible = False
                    Checkbox34.Visible = False
                Else
                    Checkbox10.Visible = True
                    Checkbox22.Visible = True
                    Checkbox26.Visible = True
                    Checkbox30.Visible = True
                    Checkbox34.Visible = True
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        Private Sub DisplayError(ByVal ex As String)
            DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ex
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
        End Sub
    End Class
End Namespace