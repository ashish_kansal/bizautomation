﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/common/DetailPage.Master"
    CodeBehind="frmGridColorScheme.aspx.vb" Inherits="BACRM.UserInterface.Admin.frmGridColorScheme" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <title>Customize Grid Colors</title>
    <link href="../CSS/GridColorScheme.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        function Save() {
            if (document.getElementById("ddlFieldList").selectedIndex == 0) {
                alert("Select a Field");
                document.getElementById("ddlFieldList").focus();
                return false;
            }

            if ($find('ddlColorScheme').get_value() == 0) {
                alert("Select Color Scheme");
                return false;
            }

            var ddlFieldListValue = ddlFieldList.value.split('~');

            if (ddlFieldListValue[1] == 'DateField') {
                if (document.getElementById("rdbAfterBeforeRecordDate").checked == true) {
                    if (document.getElementById("txtValue1").value == '') {
                        alert("Enter Value for To")
                        return false;
                    }
                    if (document.getElementById("txtValue2").value == '') {
                        alert("Enter Value for From")
                        return false;
                    }
                }
            }
            else {
                if (document.getElementById("ddlValueTrigger").selectedIndex == 0) {
                    alert("Select When field value of record match to selected value");
                    document.getElementById("ddlValueTrigger").focus();
                    return false;
                }
            }

            return true;
        }

        $(document).ready(function () {
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_pageLoaded(pageLoaded);
        })

        function pageLoaded() {
            $("#ddlFieldList").change(function () {
                var selectedFieldId = $("#ddlFieldList").val();
                if (selectedFieldId != "0") {
                    var selectedFieldArray = selectedFieldId.split("~");
                    selectedFieldId = selectedFieldArray[0];
                }
                if (parseFloat($("#hdnSavedFieldId").val()) > 1 && selectedFieldId!=$("#hdnSavedFieldId").val()) {
                    if (confirm("You can only use a single field as the source trigger for grid row color, so changing this value to the one you’ve selected will cause BizAutomation to delete all the field values and their mapped colors from the grid below, so you can map colors to values form the field you’re selecting. Are you sure you want to do this ?") == true) {
                        $("#hdnValidateOtherRowColor").val("1");
                    } else {
                        $("#hdnValidateOtherRowColor").val("0");
                    }
                } else {
                    $("#hdnValidateOtherRowColor").val("0");
                }
            })
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="RecordInformationPanel" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="RecordActionPanel" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="DetailPageTitle" runat="server">
    Customize Grid Colors
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="UtilityLinksPanel" runat="server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="TabsPlaceHolder" runat="server">
    <div id="divMessage" runat="server" class="row padbottom10" style="display: none">
        <div class="col-xs-12">
            <div class="alert alert-warning">
                <h4><i class="icon fa fa-warning"></i>Alert!</h4>
                <asp:Literal ID="litMessage" runat="server"></asp:Literal>
            </div>
        </div>
    </div>
    <div class="row padbottom10">
        <div class="col-md-12">
            <label>Use this tool to change grid (aka list view) row colors for records to a color other than the default white.</label>
        </div>
        <div class="col-xs-5">
            <div class="form-group">
                <label>WHEN Record-Type is a:</label>
                <asp:DropDownList runat="server" AutoPostBack="true" ID="ddlForm" CssClass="form-control">
                </asp:DropDownList>
            </div>
        </div>

        <div class="col-xs-5">
            <div class="form-group">
                <label>AND Record-Field is <small>(Only one field can be selected per record type)</small>:</label>
                <asp:DropDownList runat="server" AutoPostBack="true" ID="ddlFieldList" CssClass="form-control">
                    <asp:ListItem Value="0">-- Select --</asp:ListItem>
                </asp:DropDownList>
                <asp:HiddenField runat="server" Value="0" ID="hdnSavedFieldId"></asp:HiddenField>
                <asp:HiddenField runat="server" Value="0" ID="hdnValidateOtherRowColor"></asp:HiddenField>
                
            </div>
        </div>
        <div class="col-xs-5">
            <div class="form-group">
                <label>AND Record-Field has the following value:</label>
                <asp:DropDownList runat="server" ID="ddlValueTrigger" CssClass="form-control">
                    <asp:ListItem Value="0">-- Select --</asp:ListItem>
                </asp:DropDownList>
                <asp:Panel ID="pnlDateField" runat="server" Visible="false">
                    <asp:RadioButton ID="rdbRecordDate" runat="server" Text="Today" Checked="true" GroupName="RecordDate" /><br />
                    <asp:RadioButton ID="rdbAfterBeforeRecordDate" runat="server" GroupName="RecordDate" />
                    <asp:TextBox ID="txtValue1" runat="server" Width="30px" MaxLength="3"></asp:TextBox>
                    to
                    <asp:TextBox ID="txtValue2" runat="server" Width="30px" MaxLength="3"></asp:TextBox>
                    days
                    <asp:DropDownList runat="server" ID="ddlAfterBeforeRecordDate" CssClass="signup">
                        <asp:ListItem Value="After">After</asp:ListItem>
                        <asp:ListItem Value="Before">Before</asp:ListItem>
                    </asp:DropDownList>
                    record date
                </asp:Panel>
            </div>
        </div>
        <div class="col-xs-5">
            <div class="form-group">
                <label>Make record’s grid row color:</label>
                <div>
                    <telerik:RadComboBox runat="server" ID="ddlColorScheme" ClientIDMode="Static" Width="100%">
                        <Items>
                            <telerik:RadComboBoxItem Value="0" Text="-- Select --" />
                            <telerik:RadComboBoxItem Value="gvColor1" Text="Color1" CssClass="gvColor1" />
                            <telerik:RadComboBoxItem Value="gvColor2" Text="Color2" CssClass="gvColor2" />
                            <telerik:RadComboBoxItem Value="gvColor3" Text="Color3" CssClass="gvColor3" />
                            <telerik:RadComboBoxItem Value="gvColor4" Text="Color4" CssClass="gvColor4" />
                            <telerik:RadComboBoxItem Value="gvColor5" Text="Color5" CssClass="gvColor5" />
                            <telerik:RadComboBoxItem Value="gvColor6" Text="Color6" CssClass="gvColor6" />
                            <telerik:RadComboBoxItem Value="gvColor7" Text="Color7" CssClass="gvColor7" />
                            <telerik:RadComboBoxItem Value="gvColor8" Text="Color8" CssClass="gvColor8" />
                            <telerik:RadComboBoxItem Value="gvColor9" Text="Color9" CssClass="gvColor9" />
                            <telerik:RadComboBoxItem Value="gvColor10" Text="Color10" CssClass="gvColor10" />
                            <telerik:RadComboBoxItem Value="gvColor11" Text="Color11" CssClass="gvColor11" />
                            <telerik:RadComboBoxItem Value="gvColor12" Text="Color12" CssClass="gvColor12" />
                            <%--<telerik:RadComboBoxItem Value="gvColor13" Text="Color13" CssClass="gvColor13" />
                            <telerik:RadComboBoxItem Value="gvColor14" Text="Color14" CssClass="gvColor14" />
                            <telerik:RadComboBoxItem Value="gvColor15" Text="Color15" CssClass="gvColor15" />
                            <telerik:RadComboBoxItem Value="gvColor16" Text="Color16" CssClass="gvColor16" />
                            <telerik:RadComboBoxItem Value="gvColor17" Text="Color17" CssClass="gvColor17" />--%>
                        </Items>
                    </telerik:RadComboBox>
                </div>
            </div>

            
        </div>
        <div class="col-xs-2">
            <label>&nbsp;</label>
            <div>
                <asp:Button ID="btnAdd" runat="server" Text="Add" CssClass="btn btn-primary" />
                </div>
            </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="table-responsive">
                <asp:GridView ID="gvColorScheme" runat="server" EnableViewState="true" AutoGenerateColumns="false"
                    CssClass="table table-bordered table-responsive" Width="100%" ShowHeaderWhenEmpty="true" DataKeyNames="numFieldColorSchemeID" UseAccessibleHeader="true">
                    <Columns>
                        <asp:BoundField DataField="vcFormName" HeaderText="Form" />
                        <asp:BoundField DataField="vcFieldName" HeaderText="Field" />
                        <asp:BoundField DataField="vcFieldValue" HeaderText="Value Trigger On" />
                        <asp:BoundField DataField="vcColorScheme" HeaderText="Grid Row Color" />
                        <asp:TemplateField ItemStyle-Width="25">
                            <ItemTemplate>
                                <asp:LinkButton ID="lkbDelete" CommandName="DeleteRow" ClientIDMode="AutoID" CssClass="btn btn-xs btn-danger" CommandArgument='<%#Eval("numFieldColorSchemeID")%>' runat="server"><i class="fa fa-trash-o"></i></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <EmptyDataTemplate>
                        No records Found.
                    </EmptyDataTemplate>
                </asp:GridView>
            </div>
        </div>
    </div>
    <asp:UpdateProgress ID="UpdateProgress" runat="server" ClientIDMode="Static">
        <ProgressTemplate>
            <div class="overlay">
                <div class="overlayContent" style="color: #000; text-align: center; width: 250px; padding: 20px">
                    <i class="fa fa-2x fa-refresh fa-spin"></i>
                    <h3>Processing Request</h3>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
</asp:Content>
