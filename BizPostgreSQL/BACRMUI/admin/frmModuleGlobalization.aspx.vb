﻿Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Common

Namespace BACRM.UserInterface.Admin
    Public Class frmModuleGlobalization
        Inherits System.Web.UI.Page

        Dim SortField As String
        Dim m_lngStartFrom As Long

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Try
                If Not IsPostBack Then

                    LoadLocation()
                    BindList()
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Sub LoadLocation()
            Try
                Dim objConfigWizard As New FormConfigWizard             'Create an object of form config wizard
                objConfigWizard.DomainID = Session("DomainID")          'Set value of domain id

                Dim dtFormsList As DataTable
                dtFormsList = objConfigWizard.GetDycModuleMaster()

                ddlEntity.DataSource = dtFormsList
                ddlEntity.DataTextField = "vcModuleName"
                ddlEntity.DataValueField = "numModuleID"
                ddlEntity.DataBind()
                ddlEntity.SelectedIndex = 0
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub BindList()
            Try
                Dim dtFld As DataTable
                Dim objField As New FormConfigWizard()

                objField.DomainID = Session("DomainID")
                objField.ModuleID = ddlEntity.SelectedValue

                dtFld = objField.GetDycFieldMaster

                gvFields.DataSource = dtFld
                gvFields.DataBind()
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub ddlEntity_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles ddlEntity.SelectedIndexChanged
            Try
                BindList()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSave.Click
            Try
                Dim objField As New FormConfigWizard()
                Dim txtNewFieldName As TextBox = New TextBox

                For Each gvr As GridViewRow In gvFields.Rows
                    txtNewFieldName = CType(gvr.FindControl("txtNewFieldName"), TextBox)

                    If txtNewFieldName.Text.Trim.Length = 0 Then
                        txtNewFieldName.Text = gvr.Cells(0).Text
                    End If

                    objField.DomainID = Session("DomainID")
                    objField.ModuleID = ddlEntity.SelectedValue
                    objField.FormFieldId = gvFields.DataKeys(gvr.RowIndex).Value
                    objField.vcFormFieldName = txtNewFieldName.Text
                    objField.vcToolTip = CType(gvr.FindControl("txtToolTip"), TextBox).Text.Trim

                    objField.SaveDycFieldMaster()
                Next

                BindList()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub
    End Class

End Namespace