﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/common/Popup.Master" CodeBehind="frmConfEmployeeLocation.aspx.vb" Inherits=".frmConfEmployeeLocation" %>

<%@ Register Assembly="AspNetPager" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <title>Employee Location Configuration</title>
    <style type="text/css">
        .pagn a {
            float: left;
            display: block;
            text-align: center;
            line-height: 19px;
            color: black !important;
            font-weight: bold;
            width: 19px;
            height: 19px;
            text-decoration: none;
        }
        .empTable {
            width: 500px;
            margin: 10px;
            border-collapse: collapse;
        }
        .empTable tr:nth-of-type(odd) {
            background: #F0F7FF;
        }
        .empTable th {
            background: #345296;
            color: white;
            font-weight: bold;
            height: 25px;
        }
        .empTable td, th {
            padding: 0px;
            border: 1px solid #CACED9;
            text-align: center;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server">
    <div class="input-part">
        <div class="right-input">
            <asp:Button ID="btnSaveClose" runat="server" CssClass="button" Text="Save &amp; Close"></asp:Button>
            <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Close" OnClientClick="Close();" CausesValidation="false"></asp:Button>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitle" runat="server">
    Employee Location Configuration
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Content" runat="server">
    <asp:DataList ID="dlEmployeeLocation" runat="server">
        <HeaderTemplate>
            <table class="empTable">
                <tr>
                    <th>Employee</th>
                    <th>Location</th>
                </tr>
        </HeaderTemplate>
        <ItemTemplate>
            <tr>
                <td>
                    <asp:HiddenField ID="hfEmplyeeID" runat="server" />
                    <asp:Label ID="lblEmployeeName" runat="server" Text="Label"></asp:Label></td>
                <td>
                    <asp:DropDownList ID="ddlLocation" runat="server"></asp:DropDownList>
                </td>
            </tr>
        </ItemTemplate>
        <FooterTemplate>
            </table>
        </FooterTemplate>
    </asp:DataList>
    <asp:TextBox ID="txtSortCharAUsers" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtTotalPageAUsers" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtTotalRecordsAUsers" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtCurrrentPage" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtSortColumnA" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtSortOrder" runat="server" Style="display: none"></asp:TextBox>
    <asp:Button ID="btnGo1" runat="server" Style="display: none" />
</asp:Content>
