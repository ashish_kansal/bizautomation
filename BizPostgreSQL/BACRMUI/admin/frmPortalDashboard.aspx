﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmPortalDashboard.aspx.vb"
    Inherits=".frmPortalDashboard" MasterPageFile="~/common/Popup.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Portal Dashboard</title>
   
    <meta http-equiv="Page-Enter" content="blendTrans(Duration=0.01)">
    <meta http-equiv="Page-Exit" content="blendTrans(Duration=0.01)">
    <link rel="stylesheet" href="../css/lists.css" type="text/css" />
    <script language="JavaScript" type="text/javascript" src="../javascript/coordinates.js"></script>
    <script language="JavaScript" type="text/javascript" src="../javascript/drag.js"></script>
    <script language="JavaScript" type="text/javascript" src="../javascript/dragdrop.js"></script>
    <script language="JavaScript" type="text/javascript"><!--
        function MakeDragable() {
            var list = document.getElementById('Avail');
            DragDrop.makeListContainer(list, 'g2');
            list.onDragOver = function () { this.style["background"] = "none"; };
            list.onDragOut = function () { this.style["background"] = "none"; };

            list = document.getElementById('col1');
            DragDrop.makeListContainer(list, 'g2');
            list.onDragOver = function () { this.style["background"] = "none"; };
            list.onDragOut = function () { this.style["background"] = "none"; };

            list = document.getElementById('col2');
            DragDrop.makeListContainer(list, 'g2');
            list.onDragOver = function () { this.style["background"] = "none"; };
            list.onDragOut = function () { this.style["background"] = "none"; };
        }
        function Save() {
            document.getElementById('txthidden').value = DragDrop.serData('g2', null);
            //alert(document.getElementById('txthidden').value);
        }
        function Close() {
            window.close()
            return false;
        }
    </script>
    <style type="text/css">
        ul.boxy
        {
            width: 21em;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <div class="input-part">
        <div class="right-input">
            <table cellspacing="0" cellpadding="0" width="100%" border="0">
                <tr>
                    <td align="right">
                        <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="button" OnClientClick="Save();">
                        </asp:Button>
                        <asp:Button ID="btnSaveClose" runat="server" Text="Save &amp; Close" CssClass="button"
                            OnClientClick="Save();"></asp:Button>
                        <asp:Button ID="btnClose" runat="server" Text="Cancel" CssClass="button" OnClientClick="return Close();">
                        </asp:Button>
                        &nbsp;&nbsp;
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageTitle" runat="server" ClientIDMode="Static">
    Customize Portal Dashboard
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="Content" runat="server" ClientIDMode="Static">
 <asp:Table ID="Table2" runat="server" Height="250" GridLines="None" BorderColor="black"
        Width="100%" CssClass="aspTable" BorderWidth="1" CellSpacing="0" CellPadding="0">
        <asp:TableRow>
            <asp:TableCell VerticalAlign="Top">
                <br />
                <table width="100%">
                    <tr id="trRelationship" runat="server">
                        <td class="normal1" align="right">
                            Relationship
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlRelationship" Width="200" AutoPostBack="true" runat="server"
                                CssClass="signup">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr id="trProfile" runat="server">
                        <td class="normal1" align="right">
                            Profile
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlProfile" Width="200" AutoPostBack="true" runat="server"
                                CssClass="signup">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <table cellpadding="0" cellspacing="0" width="800px">
                                <tr>
                                    <td class="hs" style="width: 200px">
                                        Available Reports
                                    </td>
                                    <td width="50px">
                                    </td>
                                    <td class="hs" style="width: 18em">
                                        Reports added to Column 1
                                    </td>
                                    <td width="50px">
                                    </td>
                                    <td class="hs" style="width: 210px">
                                        Reports added to Column 2
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Repeater runat="server" ID="rptrAvailableReports">
                                            <HeaderTemplate>
                                                <ul id="Avail" class="sortable boxy" style="height: 250px">
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <li id="<%# Eval("numReportID") %>">
                                                    <%# Eval("vcReportName") %></li>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                </ul></FooterTemplate>
                                        </asp:Repeater>
                                    </td>
                                    <td>
                                    </td>
                                    <td align="center" valign="middle">
                                        <asp:Repeater runat="server" ID="rptrCol1">
                                            <HeaderTemplate>
                                                <ul id="col1" class="sortable boxy" style="height: 250px">
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <li id="<%# Eval("numReportID") %>">
                                                    <%# Eval("vcReportName") %></li>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                </ul></FooterTemplate>
                                        </asp:Repeater>
                                    </td>
                                    <td>
                                    </td>
                                    <td align="center" class="normal1">
                                        <asp:Repeater runat="server" ID="rptrCol2">
                                            <HeaderTemplate>
                                                <ul id="col2" class="sortable boxy" style="height: 250px">
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <li id="<%# Eval("numReportID") %>">
                                                    <%# Eval("vcReportName") %></li>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                </ul></FooterTemplate>
                                        </asp:Repeater>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
    <asp:TextBox Style="display: none" ID="txthidden" runat="server"></asp:TextBox>
</asp:Content>
