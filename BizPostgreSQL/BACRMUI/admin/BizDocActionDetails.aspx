<%@ Page Language="vb" AutoEventWireup="false" ValidateRequest="false" EnableEventValidation="false"
    CodeBehind="BizDocActionDetails.aspx.vb" Inherits="BACRM.UserInterface.Admin.BizDocActionDetails"
    MasterPageFile="~/common/DetailPage.Master" %>

<%@ Register TagPrefix="BizCalendar" TagName="Calendar" Src="../include/calandar.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Action Item Details</title>
    <meta http-equiv="Page-Enter" content="blendTrans(Duration=0.01)">
    <meta http-equiv="Page-Exit" content="blendTrans(Duration=0.01)">
    <link rel="stylesheet" type="text/css" href="../CSS/jscal2.css" />
    <link rel="stylesheet" type="text/css" href="../CSS/border-radius.css" />
    <script type="text/javascript" src="../JavaScript/jscal2.js"></script>
    <script type="text/javascript" src="../JavaScript/en.js"></script>
    <script language="javascript" type="text/javascript">
        function fn_Mail(txtMailAddr, a) {
            if (txtMailAddr != '') {
                window.open('../contact/frmComposeWindow.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&LsEmail=' + txtMailAddr + '&pqwRT=' + a, 'ComposeWindow', 'toolbar=no,titlebar=no,top=100,left=100,width=1200,height=645,scrollbars=yes,resizable=yes')
            }

            return false;
        }

        function openCase(a) {
            window.location(a)
        }

        function Close() {
            window.close()
            return false;
        }

        function openTime(a, b, c, d) {

            window.open("../cases/frmCasetime.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&caseId=" + a + "&CntId=" + b + "&DivId=" + c + "&CommId=" + d, '', 'toolbar=no,titlebar=no,left=500, top=300,width=650,height=300,scrollbars=yes,resizable=yes');
            return false;
        }

        function openExp(a, b, c, d) {

            window.open("../cases/frmCaseExpense.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&caseId=" + a + "&CntId=" + b + "&DivId=" + c + "&CommId=" + d, '', 'toolbar=no,titlebar=no,left=500, top=300,width=650,height=300,scrollbars=yes,resizable=yes');
            return false;
        }

        function Loaddata() {
            document.getElementById("btnLoadData").click()
            return false;
        }

        function OpenNew() {
            var bln;
            bln = confirm('If you click "Ok" this action item will be closed and sent to history (leaving an audit trail), and a new follow-up action item will be created for this organization. Would you like to continue ?')
            if (bln == true) {
                return true;
            }
            else {
                return false;
            }
        }

        function openEmpAvailability() {
            var strTime, strStart, strEnd;
            if (document.getElementById("chkAM").checked == true) {
                strStart = 'AM'
            }
            else {
                strStart = 'PM'
            }
            if (document.getElementById("chkEndAM").checked == true) {
                strEnd = 'AM'
            }
            else {
                strEnd = 'PM'
            }
            strTime = document.getElementById("ddltime").options[document.getElementById("ddltime").selectedIndex].text + ' ' + strStart + '-' + document.getElementById("ddlEndTime").options[document.getElementById("ddlEndTime").selectedIndex].text + ' ' + strEnd;
            window.open("../ActionItems/frmEmpAvailability.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&Date=" + document.getElementById("ctl00_TabsPlaceHolder_cal_txtDate").value + '&Time=' + strTime, '', 'toolbar=no,titlebar=no,top=0,left=100,width=850,height=500,scrollbars=yes,resizable=yes');
            return false;
        }

        function AssignTo(a, b) {
            document.getElementById("ddlUserNames").value = a;
            document.getElementById("ctl00_TabsPlaceHolder_cal_txtDate").value = b;
            return false;
        }

        function OpenBizInvoice(a, b, c, d, e) {
            if (c == 'B') {
                window.open('../opportunity/frmBizInvoice.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&OpID=' + a + '&OppBizId=' + b, '', 'toolbar=no,menubar=yes,titlebar=no,left=100,top=15,width=680,height=700,scrollbars=yes,resizable=yes');
            }
            else if (c == 'S') {
                window.open('../documents/frmAddSpecificDocuments.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&tyuTN=' + b, '', 'toolbar=no,menubar=yes,titlebar=no,left=100,top=15,width=680,height=700,scrollbars=yes,resizable=yes');
            }
            else if (c == 'D') {
                window.location.href = "../Documents/frmDocuments.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&DocId=" + b + "&SI1=" + d + "&frm=BizDocAction&BizDocDate=" + e
            }
            return false;
        }

        function OpenOpp(a, b, c, d, e, f) {
            if (a > 0) {
                var str;
                window.open('../opportunity/frmOpportunities.aspx?frm=OpenBizDoc&OpID=' + a + '&OppType=' + b, '', 'toolbar=no,menubar=yes,titlebar=no,left=100,top=15,width=680,height=700,scrollbars=yes,resizable=yes');
                //                   window.opener.location.href = str;
            }
            else {
                if (d == 'S') {
                    window.open('../documents/frmAddSpecificDocuments.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&tyuTN=' + c, '', 'toolbar=no,menubar=yes,titlebar=no,left=100,top=15,width=680,height=700,scrollbars=yes,resizable=yes');
                }
                else if (d = 'D') {
                    window.location.href = "../Documents/frmDocuments.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&DocId=" + c + "&SI1=" + e + "&frm=BizDocAction&BizDocDate=" + f
                }
            }
            return false;
        }
        function OpenApprovers(a, b, c) {
            window.open('../documents/frmDocApprovers.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&DocID=' + a + '&DocType=' + b + '&AppStatus=' + c, '', 'toolbar=no,menubar=yes,titlebar=no,left=100,top=15,width=680,height=700,scrollbars=yes,resizable=yes');
            return false;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="RecordInformationPanel" runat="server"
    ClientIDMode="Static">
    <div class="input-part">
        <div class="right-input">
            <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save &amp; Close">
            </asp:Button>
            <asp:Button ID="btnCancel" runat="server" CssClass="button" Width="50" Text="Close">
            </asp:Button>&nbsp;&nbsp;
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="RecordActionPanel" runat="server"
    ClientIDMode="Static">
    <asp:Label ID="lblcommet" Style="font-size: 9px" Font-Size="9px" Visible="False"
        runat="server" Font-Names="Verdana"></asp:Label>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="DetailPageTitle" runat="server"
    ClientIDMode="Static">
    Biz Doc&nbsp;Action Item Details
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="TabsPlaceHolder" runat="server"
    ClientIDMode="Static">
    <div style="background-color: #fff">
        <asp:Table ID="tbl" BorderWidth="1" CellPadding="0" CellSpacing="0" runat="server"
            Width="100%" CssClass="aspTableDTL" BorderColor="black" GridLines="None" Height="100%">
            <asp:TableRow>
                <asp:TableCell>
                    <%--<asp:Table ID="tbl" BorderWidth="1" CellPadding="0" CellSpacing="0" runat="server"
        Width="100%" CssClass="aspTable" BorderColor="black" GridLines="None">
        <asp:TableRow>
            <asp:TableCell>--%>
                    <br>
                    <table id="table1" width="100%" border="0">
                        <tr>
                            <td rowspan="25" valign="top">
                                <img src="../images/ActionItem-32.gif" />
                            </td>
                            <td class="normal1" align="right">
                                Company Name:
                            </td>
                            <td class="normal1">
                                <asp:LinkButton ID="lnkCompany" runat="server" CommandName="Company"></asp:LinkButton>
                            </td>
                            <td class="normal1" align="right">
                                Contact:
                            </td>
                            <td class="normal1">
                                <asp:LinkButton ID="lnkContact" runat="server" CommandName="Contact"></asp:LinkButton>
                            </td>
                        </tr>
                        <tr>
                            <td class="normal1" align="right">
                                Email:
                            </td>
                            <td class="normal1">
                                <asp:HyperLink runat="server" ID="hplEmail" CssClass="hyperlink"></asp:HyperLink>
                                <td class="normal1" align="right">
                                    Phone :
                                </td>
                                <td class="normal1">
                                    <asp:Label ID="lblPhone" runat="server" CssClass="text"></asp:Label>
                                    <asp:Label ID="lbldate" Visible="False" runat="server" CssClass="text"></asp:Label>
                                </td>
                        </tr>
                        <tr>
                            <td class="normal1" align="right">
                                Priority
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlStatus" runat="server" Width="180px" CssClass="signup">
                                </asp:DropDownList>
                            </td>
                            <td class="normal1" align="right">
                                Type
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlType" TabIndex="3" runat="server" Width="180px" CssClass="signup"
                                    AutoPostBack="true">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td class="normal1" align="right">
                                Due Date
                            </td>
                            <td class="normal1">
                                <BizCalendar:Calendar ID="cal" runat="server" ClientIDMode="AutoID"/>
                            </td>
                            <td class="normal1" align="right">
                                Assign To
                            </td>
                            <td class="normal1">
                                <asp:DropDownList ID="ddlUserNames" runat="server" Width="180px" CssClass="signup">
                                </asp:DropDownList>
                                &nbsp;&nbsp;&nbsp;<asp:HyperLink ID="hplEmpAvaliability" runat="server" NavigateUrl="#"><font color="#180073">Availability</font></asp:HyperLink>
                            </td>
                        </tr>
                        <tr>
                            <asp:Panel Visible="false" runat="server" ID="pnlTimeExpense">
                                <td align="right">
                                    <asp:LinkButton ID="btnCaseName" runat="server" Text="Name" CssClass="hyperlink"></asp:LinkButton>
                                </td>
                                <td align="left">
                                    <table>
                                        <tr>
                                            <td align="right">
                                                &nbsp;&nbsp;&nbsp;
                                                <asp:LinkButton ID="btnTime" runat="server" Text="Time" CssClass="hyperlink"></asp:LinkButton>&nbsp;&nbsp;&nbsp;
                                            </td>
                                            <td align="right">
                                                <asp:LinkButton ID="btnExp" runat="server" Text="Expense" CssClass="hyperlink"></asp:LinkButton>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </asp:Panel>
                        </tr>
                        <tr>
                            <td class="normal1" align="left" colspan="4">
                                <asp:DataGrid ID="dgActionItem" AutoGenerateColumns="False" runat="server" Width="98%"
                                    CssClass="dg">
                                    <AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
                                    <ItemStyle CssClass="is"></ItemStyle>
                                    <Columns>
                                        <asp:BoundColumn DataField="OrderType" HeaderText="Type Of Order" ReadOnly="True">
                                        </asp:BoundColumn>
                                        <asp:TemplateColumn HeaderText="Order ID">
                                            <ItemTemplate>
                                                <a href="javascript:void(0)" onclick="OpenOpp('<%# Eval("numOppId") %>','<%# Eval("tintopptype") %>','<%# Eval("numoppbizdocsid") %>','<%# Eval("BizDocTypeID") %>',<%# lngBizDocActId %>,'<%# BizDocDate  %>');">
                                                    <%#Eval("OrderId")%>
                                                </a>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:BoundColumn DataField="OrderAmount" HeaderText="Total Order Amt" ReadOnly="True">
                                        </asp:BoundColumn>
                                        <asp:TemplateColumn HeaderText="BizDoc Id">
                                            <ItemTemplate>
                                                <a href="javascript:void(0)" onclick="OpenBizInvoice('<%# Eval("numOppId") %>','<%# Eval("numoppbizdocsid") %>','<%# Eval("BizDocTypeID") %>',<%# lngBizDocActId %>,'<%# BizDocDate  %>');">
                                                    <%#Eval("BizDocID")%>
                                                </a>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn SortExpression="vcfiletype" HeaderText="File Type">
                                            <ItemTemplate>
                                                <asp:HyperLink ID="hplLink" Target="_blank" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "FileType") %>'>
                                                </asp:HyperLink>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:BoundColumn DataField="BizDocAmount" HeaderText="Total BizDoc Amt" ReadOnly="True">
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="BizDocPaidAmount" HeaderText="BizDoc Amt Paid" ReadOnly="True">
                                        </asp:BoundColumn>
                                        <asp:TemplateColumn HeaderText="Pending">
                                            <ItemTemplate>
                                                <a href="javascript:void(0)" onclick="OpenApprovers('<%# Eval("numoppbizdocsid") %>','<%# Eval("FileType") %>',0);">
                                                    <%#Eval("Pending")%>
                                                </a>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="Approved">
                                            <ItemTemplate>
                                                <a href="javascript:void(0)" onclick="OpenApprovers('<%# Eval("numoppbizdocsid") %>','<%# Eval("FileType") %>',1);">
                                                    <%#Eval("Approved")%>
                                                </a>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="Declined">
                                            <ItemTemplate>
                                                <a href="javascript:void(0)" onclick="OpenApprovers('<%# Eval("numoppbizdocsid") %>','<%# Eval("FileType") %>',2);">
                                                    <%#Eval("Declined")%>
                                                </a>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="Accept">
                                            <ItemTemplate>
                                                <asp:RadioButton ID="chkAccept" runat="server" GroupName="AcceptDecline" />
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="Decline">
                                            <ItemTemplate>
                                                <asp:RadioButton ID="chkDecline" runat="server" GroupName="AcceptDecline" />
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:BoundColumn DataField="numOppId" HeaderText="numOppId" Visible="False"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="numOppBizDocsId" HeaderText="numOppBizDocsId" Visible="False">
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="VcFileName" HeaderText="VcFileName" Visible="False">
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="BizDocTypeID" HeaderText="BizDocTypeID" Visible="False">
                                        </asp:BoundColumn>
                                        <asp:TemplateColumn HeaderText="Comments">
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtComment" CssClass="signup" Width="180" runat="server" Text='<%#Eval("vcComment")%>'>
                                                </asp:TextBox>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:BoundColumn DataField="numBizDocId" HeaderText="numBizDocId" Visible="False">
                                        </asp:BoundColumn>
                                    </Columns>
                                    <HeaderStyle CssClass="hs"></HeaderStyle>
                                </asp:DataGrid>
                            </td>
                        </tr>
                        <tr>
                        </tr>
                    </table>
                </asp:TableCell>
            </asp:TableRow>
        </asp:Table>
        <br>
        <asp:TextBox ID="txtfdate" TabIndex="4" runat="server" Width="50px" Visible="False"
            AutoPostBack="true" Enabled="False" CssClass="signup"></asp:TextBox><input class="signup"
                id="hidcompanyname" type="hidden" size="2" name="hidcompanyname" runat="server">
        <asp:Label ID="lblsingleq" runat="server" Font-Size="9px" Font-Names="Verdana" Visible="False"
            ForeColor="Red">Single Quotes are not allowed</asp:Label>
        <input type="hidden" id="HidStrEml" runat="server" value="0" name="HidStrEml">
        <input id="HidStartTime" type="hidden" value="0" runat="server" name="HidStartTime">
        <input id="HidEndTime" type="hidden" value="0" runat="server" name="HidEndTime">
        <asp:TextBox ID="txtUTCTime" runat="server" Style="display: none"></asp:TextBox>
        <asp:TextBox ID="TextBox1" runat="server" Style="display: none"></asp:TextBox>
        <asp:TextBox runat="server" ID="txtActivityID" Style="display: none" Text="0"></asp:TextBox>
        <asp:HiddenField ID="hdnCorrespondenceID" runat="server" />
        &nbsp;<asp:TextBox ID="txtfdate0" TabIndex="4" runat="server" Width="50px" Visible="False"
            AutoPostBack="true" Enabled="False" CssClass="signup"></asp:TextBox>
        <asp:Literal ID="ltMessage" runat="server"></asp:Literal>
    </div>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="UtilityLinksPanel" runat="server">
</asp:Content>
