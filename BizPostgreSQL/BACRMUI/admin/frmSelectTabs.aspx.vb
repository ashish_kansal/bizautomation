Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Common
Namespace BACRM.UserInterface.Admin

    Public Class frmSelectTabs
        Inherits BACRMPage

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub
        Protected WithEvents chkTabList As System.Web.UI.WebControls.CheckBoxList
        Protected WithEvents btnClose As System.Web.UI.WebControls.Button
        Protected WithEvents Table2 As System.Web.UI.WebControls.Table
        Protected WithEvents btnSave As System.Web.UI.WebControls.Button
        Protected WithEvents btnSaveClose As System.Web.UI.WebControls.Button
        Protected WithEvents ddlRelationship As System.Web.UI.WebControls.DropDownList
        Protected WithEvents trRelationship As System.Web.UI.HtmlControls.HtmlTableRow

        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.
        Private designerPlaceholderDeclaration As System.Object

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Try
                btnClose.Attributes.Add("onclick", "return Close()")
                btnAdd.Attributes.Add("OnClick", "return move(lstAvailablefld,lstAddfld)")
                btnRemove.Attributes.Add("OnClick", "return remove1(lstAddfld,lstAvailablefld)")
                'btnMoveDown.Attributes.Add("OnClick", "return MoveDown(document.Form1.lstAddfld)")
                'btnMoveup.Attributes.Add("OnClick", "return MoveUp(document.Form1.lstAddfld)")
                btnSave.Attributes.Add("onclick", "return Save()")
                btnSaveClose.Attributes.Add("onclick", "return Save()")
                If Not IsPostBack Then



                    GetUserRightsForPage(13, 25)
                    If m_aryRightsForPage(RIGHTSTYPE.VIEW) = 0 Then
                        Response.Redirect("../admin/authentication.aspx?mesg=AS")
                    ElseIf m_aryRightsForPage(RIGHTSTYPE.UPDATE) = 0 Then
                        btnSave.Visible = False
                        btnSaveClose.Visible = False
                        btnAddNewTab.Visible = False
                    End If

                    objCommon.sb_FillComboFromDBwithSel(ddlRelationship, 5, Session("DomainID"))
                    objCommon.sb_FillComboFromDBwithSel(ddlProfile, 21, Session("DomainID"))
                    PopulateGroups()

                    Dim radItem As Telerik.Web.UI.RadComboBoxItem
                    For Each item As ListItem In ddlGroup.Items
                        If item.Value <> "0" AndAlso CCommon.ToLong(item.Value.Split("~")(0)) <> CCommon.ToLong(GetQueryStringVal("GroupID")) Then
                            radItem = New Telerik.Web.UI.RadComboBoxItem
                            radItem.Text = item.Text
                            radItem.Value = item.Value.Split("~")(0)
                            rcbGroups.Items.Add(radItem)
                        End If
                    Next

                    If Not IsDBNull(GetQueryStringVal("GroupID")) Then
                        If Not ddlGroup.Items.FindByValue(GetQueryStringVal("GroupID")) Is Nothing Then
                            ddlGroup.Items.FindByValue(GetQueryStringVal("GroupID")).Selected = True
                        End If
                    End If
                    LoadInformation()
                    If Session("UserGroupID") IsNot Nothing Then
                        If ddlTabType.Items.FindByValue(Session("UserGroupID")) IsNot Nothing Then
                            ddlTabType.Items.FindByValue(Session("UserGroupID")).Attributes.Add("style", "color:green")
                        End If
                    End If
                    btnClose.Attributes.Add("onclick", "return Close();")
                End If
                If ddlGroup.SelectedIndex = 0 Then
                    btnSave.Visible = False
                    btnSaveClose.Visible = False
                    Exit Sub
                Else
                    btnSave.Visible = True
                    btnSaveClose.Visible = True
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub
        Private Sub ddlTabType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlTabType.SelectedIndexChanged
            Try
                DataGridBind()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub btnAddNewTab_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddNewTab.Click
            Try
                If ddlTabType.SelectedValue = "0" Then
                    For Each items As ListItem In ddlTabType.Items
                        If items.Value <> "0" Then
                            Dim objTabs As New Tabs
                            objTabs.TabName = txtTabName.Text.Trim
                            objTabs.TabURL = txtURL.Text.Trim
                            objTabs.TabType = items.Value.Split("~")(1)
                            objTabs.DomainID = Session("DomainID")
                            objTabs.ManageMasterTabs()
                        End If
                    Next
                Else
                    Dim objTabs As New Tabs
                    objTabs.TabName = txtTabName.Text.Trim
                    objTabs.TabURL = txtURL.Text.Trim
                    objTabs.TabType = ddlTabType.SelectedValue.Split("~")(1)
                    objTabs.DomainID = Session("DomainID")
                    objTabs.ManageMasterTabs()
                End If
                DataGridBind()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub
        Sub PopulateGroups()

            Dim objUserGroups As New UserGroups
            objUserGroups.DomainID = Session("DomainID")
            ddlGroup.DataSource = objUserGroups.GetAutorizationGroup
            ddlGroup.DataTextField = "vcGroupName"
            ddlGroup.DataValueField = "numGroupID1"
            ddlGroup.DataBind()
            ddlGroup.Items.Insert(0, "--Select One--")
            ddlGroup.Items.FindByText("--Select One--").Value = 0

            objUserGroups = New UserGroups
            objUserGroups.DomainID = Session("DomainID")
            ddlTabType.DataSource = objUserGroups.GetAutorizationGroup
            ddlTabType.DataTextField = "vcGroupName"
            ddlTabType.DataValueField = "numGroupID1"
            ddlTabType.DataBind()
            ddlTabType.Items.Insert(0, "ALL")
            ddlTabType.Items.FindByText("ALL").Value = 0

            If Session("UserGroupID") IsNot Nothing Then
                If ddlTabType.Items.FindByValue(Session("UserGroupID")) IsNot Nothing Then
                    ddlTabType.ClearSelection()
                    ddlTabType.Items.FindByValue(Session("UserGroupID")).Selected = True
                End If
            End If

        End Sub
        Sub DataGridBind()
            Try
                If ddlTabType.SelectedIndex > 0 Then
                    Dim objTabs As New Tabs
                    objTabs.DomainID = Session("DomainID")
                    objTabs.TabType = ddlTabType.SelectedValue.Split("~")(1)
                    dgTabs.DataSource = objTabs.GetTabDomainSpecific
                    dgTabs.DataBind()
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        Sub LoadInformation()
            Try
                If Not ddlGroup.SelectedIndex = 0 Then
                    If ddlGroup.SelectedValue.Split("~")(1) = 2 Then
                        divRelationship.Visible = True
                        divProfile.Visible = True
                        If Not (CCommon.ToLong(ddlProfile.SelectedValue) > 0 Or CCommon.ToLong(ddlRelationship.SelectedValue) > 0) Then
                            lstAddfld.Items.Clear()
                            lstAvailablefld.Items.Clear()
                            Exit Sub
                        End If
                    Else
                        divRelationship.Visible = False
                        divProfile.Visible = False
                        ddlRelationship.SelectedValue = 0
                        ddlProfile.SelectedValue = 0
                    End If
                End If

                Dim objUserGroups As New UserGroups
                Dim ds As DataSet

                objUserGroups.GroupId = ddlGroup.SelectedValue.Split("~")(0)
                objUserGroups.RelationShip = ddlRelationship.SelectedValue
                objUserGroups.ProfileID = ddlProfile.SelectedValue
                objUserGroups.DomainID = Session("DomainID")
                ds = objUserGroups.GetTabsForAuthorization

                lstAvailablefld.DataSource = ds.Tables(0)
                lstAvailablefld.DataTextField = "numTabName"
                lstAvailablefld.DataValueField = "numTabID"
                lstAvailablefld.DataBind()

                lstAddfld.DataSource = ds.Tables(1)
                lstAddfld.DataTextField = "numTabName"
                lstAddfld.DataValueField = "numTabID"
                lstAddfld.DataBind()
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
            Try
                Save()
                LoadInformation()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub dgTabs_EditCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgTabs.EditCommand
            Try
                dgTabs.EditItemIndex = e.Item.ItemIndex
                DataGridBind()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub dgTabs_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgTabs.ItemCommand
            Try
                If e.CommandName = "Cancel" Then
                    dgTabs.EditItemIndex = e.Item.ItemIndex
                    dgTabs.EditItemIndex = -1
                    DataGridBind()
                End If
                If e.CommandName = "Delete" Then
                    If e.Item.Cells(1).Text = "False" Then
                        Dim objTabs As New Tabs
                        objTabs.TabID = e.Item.Cells(0).Text
                        objTabs.DeleteMasterTabs()
                        DataGridBind()
                    End If
                End If
                If e.CommandName = "Update" Then
                    Dim objTabs As New Tabs
                    objTabs.TabID = CType(e.Item.FindControl("lblTabID"), Label).Text
                    objTabs.TabName = CType(e.Item.FindControl("txtETab"), TextBox).Text
                    objTabs.DomainID = Session("DomainID")
                    objTabs.TabURL = CType(e.Item.FindControl("txtEURL"), TextBox).Text
                    objTabs.TabType = ddlTabType.SelectedValue.Split("~")(1)
                    objTabs.ManageMasterTabs()
                    dgTabs.EditItemIndex = -1
                    DataGridBind()

                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub dgTabs_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgTabs.ItemDataBound
            Try
                If e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.Item Then
                    If e.Item.Cells(1).Text = True Then
                        Dim lnkdelete As LinkButton
                        Dim btnDelete As Button
                        lnkdelete = e.Item.FindControl("lnkdelete")
                        btnDelete = e.Item.FindControl("btnDelete")
                        btnDelete.Visible = False
                        lnkdelete.Visible = True
                        lnkdelete.Attributes.Add("onclick", "return NoDeleteMessage()")
                    End If
                End If
                If e.Item.ItemType = ListItemType.EditItem Then
                    If CType(e.Item.FindControl("lblFixed"), Label).Text = True Then
                        Dim txtURL As TextBox
                        txtURL = e.Item.FindControl("txtEURL")
                        txtURL.Enabled = False
                    End If
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub Save()
            Try
                Dim ds As New DataSet
                Dim dtTabs As New DataTable
                dtTabs.Columns.Add("TabId")
                dtTabs.Columns.Add("bitallowed")
                dtTabs.Columns.Add("numOrder")

                Dim i As Integer = 0
                Dim strVales As String() = txthidden.Text.Split(",")

                Dim dr As DataRow
                For i = 0 To strVales.Length - 2
                    dr = dtTabs.NewRow
                    dr("TabId") = strVales(i)
                    dr("bitallowed") = 1
                    dr("numOrder") = i + 1
                    dtTabs.Rows.Add(dr)
                Next

                dtTabs.TableName = "Table"
                ds.Tables.Add(dtTabs.Copy)


                Dim listItemGroup As New System.Collections.Generic.List(Of Long)
                If ddlGroup.SelectedValue <> "0" AndAlso CCommon.ToLong(ddlGroup.SelectedValue.Split("~")(0)) > 0 Then
                    listItemGroup.Add(ddlGroup.SelectedValue.Split("~")(0))
                End If

                For Each item As Telerik.Web.UI.RadComboBoxItem In rcbGroups.CheckedItems
                    listItemGroup.Add(item.Value)
                Next

                For Each groupID As Long In listItemGroup
                    Dim objUserGroups As New UserGroups
                    objUserGroups.GroupId = groupID
                    objUserGroups.strTabs = ds.GetXml()
                    objUserGroups.RelationShip = 0
                    objUserGroups.Type = 0 ' 1 for subtabs , null or 0 for main tabs
                    objUserGroups.LocationID = 0
                    If CCommon.ToLong(ddlRelationship.SelectedValue) > 0 Then
                        objUserGroups.RelationShip = ddlRelationship.SelectedValue
                    End If
                    If CCommon.ToLong(ddlProfile.SelectedValue) > 0 Then
                        objUserGroups.ProfileID = ddlProfile.SelectedValue
                    End If
                    objUserGroups.ManageTabsAuthorization()
                Next
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub btnSaveClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveClose.Click
            Try
                Save()
                Response.Write("<script>window.close();</script>")
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub ddlGroup_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGroup.SelectedIndexChanged
            Try
                LoadInformation()

                rcbGroups.Items.Clear()
                Dim radItem As Telerik.Web.UI.RadComboBoxItem
                For Each item As ListItem In ddlGroup.Items
                    If item.Value <> "0" AndAlso item.Value.Split("~")(0) <> ddlGroup.SelectedValue.Split("~")(0) Then
                        radItem = New Telerik.Web.UI.RadComboBoxItem
                        radItem.Text = item.Text
                        radItem.Value = item.Value.Split("~")(0)
                        rcbGroups.Items.Add(radItem)
                    End If
                Next
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub ddlRelationship_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlRelationship.SelectedIndexChanged
            Try
                LoadInformation()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub ddlProfile_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlProfile.SelectedIndexChanged
            Try
                LoadInformation()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub
    End Class
End Namespace
