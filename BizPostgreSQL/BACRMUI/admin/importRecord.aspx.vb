
Imports System.IO
Imports BACRMBUSSLOGIC.BussinessLogic
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Leads
Imports System.Reflection
Imports LumenWorks.Framework.IO.Csv
Imports LumenWorks.Framework.Tests.Unit
Imports BACRM.BusinessLogic.Prospects

Namespace BACRM.UserInterface.Admin
    Public Class importRecord
        Inherits BACRMPage

#Region "Decleration"
        Dim strFileName As String  'Variable to hold the FileName string
        Dim lngDivisionID As Long
        Dim lngContactID As Long
        Dim txtState As String
        Dim txtCountry As String
        Dim cmbAnnualRevenue As String
        Dim cmbEmployees As Long

        Protected WithEvents chkRelationship As System.Web.UI.WebControls.CheckBox
        Protected WithEvents ddlRelationship As System.Web.UI.WebControls.DropDownList
        Protected WithEvents chkContact As System.Web.UI.WebControls.CheckBox
        Protected WithEvents rdRouting As System.Web.UI.WebControls.RadioButton
        Protected WithEvents rdAssign As System.Web.UI.WebControls.RadioButton
        Protected WithEvents ddlAssign As System.Web.UI.WebControls.DropDownList
        Protected WithEvents btnUpload As System.Web.UI.WebControls.Button
        Protected WithEvents btnImports As System.Web.UI.WebControls.Button
        Protected WithEvents txtFile As System.Web.UI.HtmlControls.HtmlInputFile
        Protected WithEvents litMessage As System.Web.UI.WebControls.Literal
        Protected WithEvents btnDisplay As System.Web.UI.WebControls.Button
        Protected WithEvents txtDllValue As System.Web.UI.WebControls.TextBox
        Protected WithEvents Table2 As System.Web.UI.WebControls.Table
        Protected WithEvents pgBar As System.Web.UI.HtmlControls.HtmlImage
        Protected WithEvents ddlGroup As System.Web.UI.WebControls.DropDownList
        Protected WithEvents lblGroup As System.Web.UI.WebControls.Label
        Protected WithEvents tbldtls As System.Web.UI.WebControls.Table


#End Region

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub
        Protected WithEvents Bdy As System.Web.UI.HtmlControls.HtmlControl
        Protected WithEvents ddlSelection As System.Web.UI.WebControls.DropDownList
        Protected WithEvents ddlInfoSource As System.Web.UI.WebControls.DropDownList
        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.
        Private designerPlaceholderDeclaration As System.Object

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

#Region "Page Load"
        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Try
                If Not IsPostBack Then
                    pgBar.Style.Add("display", "none")
                    
                    objCommon.sb_FillComboFromDBwithSel(ddlInfoSource, 18, Session("DomainID"))
                    objCommon.sb_FillComboFromDBwithSel(ddlRelationship, 5, Session("DomainID"))
                    Dim objImportWizard As New ImportWizard
                    objImportWizard.DomainId = Session("DomainID")
                    Dim dtUserAssign As DataTable
                    dtUserAssign = objImportWizard.AssignUserDetails
                    ddlAssign.DataSource = dtUserAssign
                    ddlAssign.DataTextField = "vcGivenName"
                    ddlAssign.DataValueField = "numUserId"
                    ddlAssign.DataBind()
                    ddlAssign1.DataSource = dtUserAssign
                    ddlAssign1.DataTextField = "vcGivenName"
                    ddlAssign1.DataValueField = "numUserId"
                    ddlAssign1.DataBind()
                    lblGroup.Style.Add("display", "none")
                    ddlGroup.Style.Add("display", "none")
                    objCommon.sb_FillGroupsFromDBSel(ddlGroup)
                    
                End If
                If Session("FileLocation") <> "" Then displayDropdowns()
                If ddlSelection.SelectedItem.Value = 0 Then
                    ddlGroup.Attributes.Add("style", "display:''")
                    lblGroup.Attributes.Add("style", "display:''")
                Else
                    ddlGroup.Attributes.Add("style", "display:none")
                    lblGroup.Attributes.Add("style", "display:none")
                End If
                btnUpload.Attributes.Add("onClick", "return checkFileExt()")
                btnDisplay.Attributes.Add("onClick", "return setDllValues()")
                btnImports.Attributes.Add("onClick", "return displayPBar()")
                ddlSelection.Attributes.Add("onchange", "return ShowGroup()")
                btnConfg.Attributes.Add("onClick", "return OpenConf()")
                litMessage.Text = ""
                btnImports.Visible = True
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub
#End Region

#Region "Display Dropdowns"
        Private Sub displayDropdowns()
            Try
                Dim dataTableHeading As New DataTable
                Dim fileLocation As String = Session("FileLocation")
                Dim streamReader As New StreamReader(fileLocation)
                Dim intCount As Integer = 0
                Dim intCountFor As Integer = 0
                Dim strSplitValue() As String
                Dim strSplitHeading() As String
                Dim strLine As String = streamReader.ReadLine()
                Dim dataTableMap As New DataTable
                dataTableMap.Columns.Add("Destination")
                dataTableMap.Columns.Add("ID")
                Try
                    Do While Not strLine Is Nothing
                        Dim dataRowMap As DataRow
                        dataRowMap = dataTableMap.NewRow
                        strSplitHeading = Split(strLine, ",")

                        '              Dim duplicates = From u In strSplitHeading Group By u Into Group _
                        'Where Group.Count() > 1 _
                        'Select Group



                        For intCountFor = 0 To strSplitHeading.Length - 1
                            dataRowMap = dataTableMap.NewRow
                            dataRowMap("Destination") = strSplitHeading(intCountFor).Replace("""", "").ToString
                            dataRowMap("ID") = intCountFor
                            dataTableMap.Rows.Add(dataRowMap)
                        Next
                        Exit Do
                    Loop
                    Dim trHeader As New TableRow
                    Dim trDetail As New TableRow
                    Dim tableHC As TableHeaderCell
                    Dim tableDtl As TableCell
                    Dim drHeading As DataRow
                    intCount = 0
                    dataTableHeading = Session("dataTableHeadingObj")
                    Dim ddlDestination As DropDownList
                    Dim arryList() As String
                    Dim strdllValue As String
                    Dim intArryValue As Integer
                    If Session("dllValue") <> "" Then
                        strdllValue = Session("dllValue")
                        arryList = Split(strdllValue, ",")
                    End If
                    tbldtls.Rows.Clear()
                    For Each drHeading In dataTableHeading.Rows
                        trHeader.CssClass = "hs"
                        tableHC = New TableHeaderCell
                        tableHC.ID = intCount
                        tableHC.Text = drHeading.Item(1)
                        tableHC.CssClass = "normal5"
                        trHeader.Cells.Add(tableHC)
                        tableDtl = New TableCell
                        ddlDestination = New DropDownList
                        ddlDestination.CssClass = "signup"
                        ddlDestination.ID = "ddlDestination" & intCount.ToString
                        ddlDestination.DataSource = dataTableMap
                        ddlDestination.DataTextField = "Destination"
                        ddlDestination.DataValueField = "ID"
                        ddlDestination.DataBind()
                        If Session("dllValue") <> "" Then
                            If arryList.Length > 9 Then
                                intArryValue = CType(arryList(intCount), Integer)
                                ddlDestination.SelectedIndex = intArryValue
                            ElseIf ddlDestination.Items.Count > intCount Then
                                ddlDestination.SelectedIndex = intCount
                            End If
                        ElseIf ddlDestination.Items.Count > intCount Then
                            ddlDestination.SelectedIndex = intCount
                        End If
                        tableDtl.Controls.Add(ddlDestination)
                        trDetail.Cells.Add(tableDtl)
                        intCount += 1
                        trDetail.CssClass = "is"
                        tbldtls.Rows.Add(trDetail)
                        tbldtls.Rows.AddAt(0, trHeader)
                    Next
                    Session("IntCount") = intCount - 1
                    If txtDllValue.Text <> "" Then
                        Session("dllValue") = txtDllValue.Text
                    End If
                    txtDllValue.Text = intCount - 1
                Catch ex As Exception
                    Throw ex
                Finally
                    streamReader.Close()
                End Try

            Catch ex As Exception
                Throw ex
            End Try

        End Sub
#End Region

#Region "Display Click"
        Private Sub btnDisplay_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDisplay.Click
            Try
                Dim dataTableDestination As New DataTable
                Dim dataTableHeading As New DataTable
                Dim fileLocation As String = Session("FileLocation")
                Dim streamReader As StreamReader
                streamReader = File.OpenText(fileLocation)
                Dim intCount As Integer = Session("IntCount")
                Dim intCountFor As Integer = 0
                Dim strSplitValue() As String
                Dim strSplitHeading() As String
                ' Dim strLine As String = streamReader.ReadLine()
                Dim arryList() As String
                Dim strdllValue As String
                Dim intArryValue As Integer
                Dim intCountSession As Integer = Session("IntCount")
                dataTableDestination = Session("dataTableDestinationObj")
                dataTableDestination.Clear()
                Dim csv As CsvReader = New CsvReader(streamReader, True)
                Try
                    csv.MissingFieldAction = MissingFieldAction.ReplaceByEmpty
                    csv.SupportsMultiline = True

                    strdllValue = Session("dllValue")
                    arryList = Split(strdllValue, ",")

                    'strLine = streamReader.ReadLine()
                    'Do While Not strLine Is Nothing
                    '    Dim dataRowDestination As DataRow
                    '    dataRowDestination = dataTableDestination.NewRow
                    '    strSplitValue = Split(strLine, ",")
                    '    dataTableHeading = Session("dataTableHeadingObj")
                    '    Dim drHead As DataRow
                    '    intCount = 0
                    '    For Each drHead In dataTableHeading.Rows
                    '        intArryValue = CType(arryList(intCount), Integer)
                    '        dataRowDestination(drHead.Item(1)) = strSplitValue(intArryValue).ToString
                    '        intCount += 1
                    '    Next
                    '    dataTableDestination.Rows.Add(dataRowDestination)
                    '    strLine = streamReader.ReadLine()
                    'Loop

                    'Dim headers As String() = csv.GetFieldHeaders()
                    'Dim i As Integer
                    Dim dataRowDestination As DataRow
                    ''For i = 0 To csv.FieldCount - 1
                    ''    dataTableDestination.Columns.Add(headers(i))
                    ''    'Response.Write(String.Format("{0} = {1};", headers(i), csv(i)))
                    ''Next
                    Dim drHead As DataRow
                    dataTableHeading = Session("dataTableHeadingObj")
                    Dim i1000 As Integer = 0
                    While (csv.ReadNextRecord())
                        dataRowDestination = dataTableDestination.NewRow
                        intCount = 0
                        For Each drHead In dataTableHeading.Rows
                            intArryValue = CType(arryList(intCount), Integer)
                            dataRowDestination(drHead.Item(1)) = csv(intArryValue).ToString
                            intCount += 1
                        Next
                        Try
                            dataTableDestination.Rows.Add(dataRowDestination)
                        Catch ex As Exception
                            If ex.Message.Contains("constrained to be unique") Then
                                streamReader.Close()
                                litMessage.Text = "Column " & strUniqueContID & " has duplicate values, Your option is ensure values in " & strUniqueContID & " column are unique and upload it again."
                                Exit Sub
                            End If
                            Throw ex
                        End Try

                        i1000 = i1000 + 1
                        If i1000 = 7500 Then ' Modified by Anoop 29/04/2009
                            Exit While
                        End If
                    End While
                    Dim drDetination As DataRow
                    Dim iCount As Integer = 0
                    Dim iCountRow As Integer = 1
                    Dim dtCol As DataColumn
                    Dim trDetail As TableRow
                    Dim tableCell As TableCell
                    'tbldtls.Rows.Clear()
                    Dim k As Integer = 0
                    For Each drDetination In dataTableDestination.Rows
                        trDetail = New TableRow
                        If k = 0 Then
                            trDetail.CssClass = "ais"
                            k = 1
                        Else
                            trDetail.CssClass = "is"
                            k = 0
                        End If
                        For iCount = 1 To dataTableDestination.Columns.Count - 1
                            tableCell = New TableCell
                            tableCell.Text = drDetination.Item(iCount)
                            tableCell.CssClass = "normal1"
                            trDetail.Cells.Add(tableCell)
                        Next
                        iCountRow += 1
                        tbldtls.Rows.AddAt(iCountRow, trDetail)
                    Next
                Catch ex As Exception
                    If ex.Message = "An item with the same key has already been added." Then
                        streamReader.Close()
                        litMessage.Text = "Duplicate column names found in CSV file, Your option is to remove duplicate columns and upload it again."
                        Exit Sub
                    End If
                    Response.Write(ex)
                Finally
                    streamReader.Close()
                End Try
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try

        End Sub
#End Region

#Region "Create Table Schema"
        Const strOrgCont As String = "Non Biz Company ID"
        Const strUniqueContID As String = "Non Biz Contact ID"
        'Const strUniqueID As String = "Unique ID"
        Private Sub createTableSchema()
            Try
                Dim dataTableDestination As New DataTable
                Dim dataTableHeading As New DataTable
                Dim dataTable As New DataTable
                Dim numRows As Integer
                Dim i As Integer
                dataTableHeading.Columns.Add("Id")
                dataTableHeading.Columns.Add("Destination")
                dataTableHeading.Columns.Add("FType")
                dataTableHeading.Columns.Add("cCtype")
                dataTableHeading.Columns.Add("vcAssociatedControlType")
                dataTableHeading.Columns.Add("vcDbColumnName")
                dataTableHeading.Columns.Add("numListID")
                Dim dataRowHeading As DataRow

                If chkMultipleContacts.Checked Then
                    dataRowHeading = dataTableHeading.NewRow
                    dataRowHeading("Destination") = strOrgCont
                    dataRowHeading("Id") = 1
                    dataRowHeading("FType") = 0
                    dataRowHeading("cCtype") = "V"
                    dataRowHeading("vcAssociatedControlType") = "SystemField"
                    dataRowHeading("vcDbColumnName") = "numCompanyID"
                    dataRowHeading("numListID") = 0
                    dataTableHeading.Rows.Add(dataRowHeading)
                End If
                If chkCorrespondance.Checked Then
                    dataRowHeading = dataTableHeading.NewRow
                    dataRowHeading("Destination") = strUniqueContID
                    dataRowHeading("Id") = 2
                    dataRowHeading("FType") = 0
                    dataRowHeading("cCtype") = "V"
                    dataRowHeading("vcAssociatedControlType") = "SystemField"
                    dataRowHeading("vcDbColumnName") = "numContactID"
                    dataRowHeading("numListID") = 0
                    dataTableHeading.Rows.Add(dataRowHeading)
                End If
                If chkAssociations.Checked Then
                    dataRowHeading = dataTableHeading.NewRow
                    dataRowHeading("Destination") = "ParentAssociationName"
                    dataRowHeading("Id") = 3
                    dataRowHeading("FType") = 0
                    dataRowHeading("cCtype") = "V"
                    dataRowHeading("vcAssociatedControlType") = "SystemField"
                    dataRowHeading("vcDbColumnName") = ""
                    dataRowHeading("numListID") = 0
                    dataTableHeading.Rows.Add(dataRowHeading)

                    dataRowHeading = dataTableHeading.NewRow
                    dataRowHeading("Destination") = "ChildAssociationName"
                    dataRowHeading("Id") = 4
                    dataRowHeading("FType") = 0
                    dataRowHeading("cCtype") = "V"
                    dataRowHeading("vcAssociatedControlType") = "SystemField"
                    dataRowHeading("vcDbColumnName") = ""
                    dataRowHeading("numListID") = 0
                    dataTableHeading.Rows.Add(dataRowHeading)

                    dataRowHeading = dataTableHeading.NewRow
                    dataRowHeading("Destination") = "ParentOrganizationID"
                    dataRowHeading("Id") = 5
                    dataRowHeading("FType") = 0
                    dataRowHeading("cCtype") = "V"
                    dataRowHeading("vcAssociatedControlType") = "SystemField"
                    dataRowHeading("vcDbColumnName") = ""
                    dataRowHeading("numListID") = 0
                    dataTableHeading.Rows.Add(dataRowHeading)
                End If

                Dim objImport As New ImportWizard
                objImport.DomainId = Session("DomainID")
                objImport.Relationship = ddlRelationship.SelectedValue
                objImport.ImportType = 1
                dataTable = objImport.GetConfigurationTable()
                numRows = dataTable.Rows.Count()
                For i = 0 To numRows - 1
                    dataRowHeading = dataTableHeading.NewRow
                    dataRowHeading("Destination") = dataTable.Rows(i).Item("vcFormFieldName")
                    dataRowHeading("Id") = dataTable.Rows(i).Item("numFormFieldId")
                    dataRowHeading("FType") = IIf(dataTable.Rows(i).Item("bitCustomFld") = "False", "0", "1")
                    dataRowHeading("cCtype") = dataTable.Rows(i).Item("cCtype")
                    dataRowHeading("vcAssociatedControlType") = dataTable.Rows(i).Item("vcAssociatedControlType")
                    dataRowHeading("vcDbColumnName") = dataTable.Rows(i).Item("vcDbColumnName")
                    dataRowHeading("numListID") = dataTable.Rows(i).Item("numListID")
                    dataTableHeading.Rows.Add(dataRowHeading)
                Next





                Session("dataTableHeadingObj") = dataTableHeading
                Dim drHead As DataRow
                dataTableDestination.Columns.Add("SlNo")
                dataTableDestination.Columns("SlNo").AutoIncrement = True
                dataTableDestination.Columns("SlNo").AutoIncrementSeed = 1
                dataTableDestination.Columns("SlNo").AutoIncrementStep = 1

                For Each drHead In dataTableHeading.Rows
                    dataTableDestination.Columns.Add(drHead.Item(1))
                Next

                If chkCorrespondance.Checked Then
                    'set Unique key on contact ID
                    dataTableDestination.Columns(strUniqueContID).Unique = True
                End If
                Session("dataTableDestinationObj") = dataTableDestination
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
#End Region

#Region "Upload Click"
        Private Sub btnUpload_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUpload.Click
            Try
                If Not txtFile.PostedFile Is Nothing And txtFile.PostedFile.ContentLength > 0 Then
                    pgBar.Style.Add("display", "")
                    Dim fileName As String = System.IO.Path.GetFileName(txtFile.PostedFile.FileName)
                    Dim SaveLocation As String = Server.MapPath("../Documents/Docs") & "\" & fileName
                    Try
                        txtFile.PostedFile.SaveAs(SaveLocation)
                        Session("FileLocation") = SaveLocation
                        litMessage.Text = "The file has been uploaded"
                        createTableSchema()
                        displayDropdowns()

                        pgBar.Style.Add("display", "none")
                    Catch ex As Exception
                        pgBar.Style.Add("display", "none")
                        Throw ex
                    End Try
                Else
                    Session("IntCount") = ""
                    Session("FileLocation") = ""
                    Session("dataTableDestinationObj") = ""
                    Session("dataTableHeadingObj") = ""
                    Session("dllValue") = ""
                    tbldtls.Rows.Clear()
                    litMessage.Text = "Please select a file to upload."
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub
#End Region

#Region "Imports Click"
        'How to import multiple contact in one company
        'UniqueID CompanyName/ContactName CompanyID
        ' 1       ABC Inc     				0
        ' 2       XYZ INC     				0
        ' 3       Mr abc      				1
        ' 4       Mr xyz      				2
        ' 5       Mr pqr      				9
        'When Company ID is 0 then new Company Will be created
        'When Company ID exist then new contact will be created for selected company ID
        'When Company ID is not found then Contact will be treated as new company itself
        Private Sub btnImports_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnImports.Click

            Dim objLeads As New CLeads
            Dim objImpWzd As New ImportWizard
            Dim objAutoRoutRles As New AutoRoutingRules
            Try
                Dim dtAutoRoutingRules As New DataTable
                Dim drAutoRow As DataRow
                Dim ds As New DataSet
                If rdRouting.Checked = True Then
                    dtAutoRoutingRules.TableName = "Table"
                    dtAutoRoutingRules.Columns.Add("vcDbColumnName")
                    dtAutoRoutingRules.Columns.Add("vcDbColumnText")
                End If
                Dim dataTableDestination As New DataTable
                Dim dataTableHeading As New DataTable
                Dim lngCompanyId, lngDivisionID, lngContactID As Long
                dataTableDestination = Session("dataTableDestinationObj")
                dataTableHeading = Session("dataTableHeadingObj")

                If chkMultipleContacts.Checked Then
                    'Store Database PKs
                    dataTableDestination.Columns.Add("#NumCompanyID")
                    dataTableDestination.Columns.Add("#NumDivisionID")
                    dataTableDestination.Columns.Add("#NumContactID")
                End If


                Dim drDestination As DataRow
                Dim drHead As DataRow
                Dim intCount As Integer = 0
                For Each drDestination In dataTableDestination.Rows
                    objLeads = New CLeads
                    objLeads.CompanyID = 0
                    objLeads.DivisionID = 0
                    objLeads.ContactID = 0
                    objLeads.CompanyName = ""
                    If ddlSelection.SelectedItem.Value = 0 Then
                        objLeads.GroupID = ddlGroup.SelectedItem.Value
                    Else
                        objLeads.GroupID = 5
                    End If
                    Dim vcFieldtype As Char
                    Dim vcAssociatedControlType As String

                    For Each dr As DataRow In dataTableHeading.Rows
                        vcFieldtype = dr("cCtype")
                        If vcFieldtype = "R" Then
                            If CCommon.ToString(drDestination(dr("Destination").ToString)).Trim.Length > 0 Then
                                vcAssociatedControlType = dr("vcAssociatedControlType")
                                Select Case vcAssociatedControlType
                                    Case "TextBox"
                                        AssignValuesEditBox(objLeads, drDestination(dr("Destination").ToString).ToString, dr("vcDbColumnName"))
                                    Case "SelectBox"

                                        If dr("vcDbColumnName").IndexOf("State") > -1 Then
                                            drDestination(dr("Destination").ToString) = objImpWzd.GetStateAndCountry(1, CCommon.ToString(drDestination(dr("Destination").ToString)), Session("DomainID"))
                                        ElseIf dr("vcDbColumnName") = "numAssignedTo" Then
                                            drDestination(dr("Destination").ToString) = objImpWzd.GetStateAndCountry(14, CCommon.ToString(drDestination(dr("Destination").ToString)), Session("DomainID"))
                                        ElseIf dr("vcDbColumnName") = "numRecOwner" Then
                                            If rbRecOwnerColumn.Checked Then
                                                drDestination(dr("Destination").ToString) = objImpWzd.GetStateAndCountry(14, CCommon.ToString(drDestination(dr("Destination").ToString)), Session("DomainID"))
                                                If CCommon.ToLong(drDestination(dr("Destination").ToString)) = 0 Then
                                                    drDestination(dr("Destination").ToString) = ddlAssign1.SelectedValue 'set default value
                                                End If
                                            End If
                                        Else
                                            drDestination(dr("Destination").ToString) = objImpWzd.GetStateAndCountry(18, CCommon.ToString(drDestination(dr("Destination").ToString)), Session("DomainID"), CCommon.ToLong(dr("numListID")))
                                        End If
                                        AssignValuesSelectBox(objLeads, drDestination(dr("Destination").ToString).ToString, dr("vcDbColumnName"))
                                    Case "TextArea"
                                        AssignValuesTextBox(objLeads, drDestination(dr("Destination").ToString).ToString, dr("vcDbColumnName"))
                                    Case "SystemField"
                                        drDestination(dr("Destination").ToString) = dr("vcDbColumnName").ToString()
                                End Select
                            End If
                        End If
                    Next

                    'objLeads.FirstName = IIf(IsDBNull(drDestination.Item(1)), "", drDestination.Item(1).Trim)
                    objLeads.CRMType = ddlSelection.SelectedItem.Value
                    objLeads.InfoSource = ddlInfoSource.SelectedItem.Value
                    objLeads.CompanyType = ddlRelationship.SelectedItem.Value

                    objLeads.DivisionName = "-"
                    objLeads.DomainID = Session("DomainID")
                    objLeads.ContactType = 70
                    objLeads.PrimaryContact = True

                    If objLeads.CompanyName = "" Then
                        objLeads.CompanyName = objLeads.LastName & ", " & objLeads.FirstName
                    End If
                    'objLeads.LastName = IIf(IsDBNull(drDestination.Item(2)), "", drDestination.Item(2).Trim)
                    'objLeads.Street = IIf(IsDBNull(drDestination.Item(3)), "", drDestination.Item(3).Trim)
                    'objLeads.City = IIf(IsDBNull(drDestination.Item(4)), "", drDestination.Item(4).Trim)



                    If objLeads.Country = 0 And objLeads.SCountry = 0 And objLeads.PCountry = 0 Then
                        objLeads.Country = Session("DefCountry")
                    End If
                    'objLeads.Phone = IIf(IsDBNull(drDestination.Item(7)), "", drDestination.Item(7).Trim)

                    'objLeads.Email = IIf(IsDBNull(drDestination.Item(8)), "", drDestination.Item(8).Trim)
                    'objLeads.PhoneExt = IIf(IsDBNull(drDestination.Item(9)), "", drDestination.Item(9).Trim)

                    'objLeads.PostalCode = IIf(IsNumeric(IIf(IsDBNull(drDestination.Item(11)), "", drDestination.Item(10).Trim)) = True, drDestination.Item(11).Trim, "")
                    If rdRouting.Checked = True Then
                        objAutoRoutRles.DomainID = Session("DomainID")
                        dtAutoRoutingRules.Rows.Clear()
                        For Each dr As DataRow In dataTableHeading.Rows
                            drAutoRow = dtAutoRoutingRules.NewRow
                            drAutoRow("vcDbColumnName") = dr("vcDbColumnName")
                            drAutoRow("vcDbColumnText") = drDestination(dr("Destination").ToString).ToString
                            dtAutoRoutingRules.Rows.Add(drAutoRow)
                        Next

                        ds.Tables.Add(dtAutoRoutingRules.Copy)
                        objAutoRoutRles.strValues = ds.GetXml
                        ds.Tables.Remove(ds.Tables(0))
                        objLeads.UserCntID = objAutoRoutRles.GetRecordOwner
                    ElseIf rdAssign.Checked Then
                        objLeads.UserCntID = ddlAssign.SelectedItem.Value
                    ElseIf objLeads.UserCntID = 0 Then
                        objLeads.UserCntID = Session("UserContactID")
                    End If



                    If chkMultipleContacts.Checked Then
                        'Check if Linking ID column has got values for company and division id
                        If CCommon.ToString(drDestination(strOrgCont)).Trim.Length > 0 Then
                            'search in dataset for given PK
                            For i1 As Integer = 0 To dataTableDestination.Rows.Count - 1
                                If dataTableDestination.Rows(i1)(strOrgCont).ToString.Trim = CCommon.ToString(drDestination(strOrgCont)).Trim Then
                                    If CCommon.ToLong(dataTableDestination.Rows(i1)("#NumCompanyID")) > 0 And CCommon.ToLong(dataTableDestination.Rows(i1)("#NumDivisionID")) Then
                                        lngCompanyId = CCommon.ToLong(dataTableDestination.Rows(i1)("#NumCompanyID"))
                                        lngDivisionID = CCommon.ToLong(dataTableDestination.Rows(i1)("#NumDivisionID"))
                                        objLeads.CompanyID = lngCompanyId
                                        objLeads.DivisionID = lngDivisionID
                                        objLeads.ContactType = 0
                                        objLeads.PrimaryContact = False
                                    End If
                                End If
                            Next
                        End If
                    End If

                    If lngCompanyId = 0 Then

                        lngCompanyId = objLeads.CreateRecordCompanyInfo
                        objLeads.CompanyID = lngCompanyId
                        If chkMultipleContacts.Checked Then drDestination("#NumCompanyID") = lngCompanyId

                        lngDivisionID = objLeads.CreateRecordDivisionsInfo
                        objLeads.DivisionID = lngDivisionID
                        If chkMultipleContacts.Checked Then drDestination("#NumDivisionID") = lngDivisionID

                    End If

                    lngContactID = objLeads.CreateRecordAddContactInfo
                    objLeads.ContactID = lngContactID
                    If chkMultipleContacts.Checked Then drDestination("#NumContactID") = lngContactID


                    'Below section is hard coded for Innpoints
                    If Session("DomainID") = 132 Then
                        'Add Association between Parent Company A, and Child COmpany B
                        Dim objProspects As New CProspects                          'Create a new object of prospects
                        Dim vcDisplayMessage As String = ""                         'Declare a String Variable which will store the message if any
                        Dim vcReturnFlag As String                                  'Declare an String variable

                        'If radCmbCompany.SelectedValue = lngAssFromDivID Then
                        '    litMessage.Text = "<script>alert('Can not associate to company which you are in.');</script>"
                        '    Exit Sub
                        'End If
                        If CCommon.ToString(drDestination("ParentOrganizationID")).Trim.Length > 0 Then
                            'search in dataset for given PK
                            For i1 As Integer = 0 To dataTableDestination.Rows.Count - 1
                                If dataTableDestination.Rows(i1)(strOrgCont).ToString.Trim = CCommon.ToString(drDestination("ParentOrganizationID")).Trim Then
                                    If CCommon.ToLong(dataTableDestination.Rows(i1)("#NumCompanyID")) > 0 And CCommon.ToLong(dataTableDestination.Rows(i1)("#NumDivisionID")) Then
                                        With objProspects
                                            .AssociationID = 0                                      'New Assocation Id
                                            .DivisionID = CCommon.ToLong(dataTableDestination.Rows(i1)("#NumDivisionID"))             'Specity the Division Id
                                            .ContactId = CCommon.ToLong(dataTableDestination.Rows(i1)("#NumContactID"))
                                            .AssociationType = objImpWzd.GetStateAndCountry(13, CCommon.ToString(drDestination("ParentAssociationName")), Session("DomainID"))           'The Association Type
                                            .AssociationTypeForLabel = objImpWzd.GetStateAndCountry(13, CCommon.ToString(drDestination("ChildAssociationName")), Session("DomainID"))
                                            .bitDeleted = 0                                         'Deleted flag is 0 since we are adding an assocoation
                                            .UserCntID = Session("UserContactID")                             'Set the User Id into the class property
                                            .DomainID = Session("DomainID")                         'Set the Domain ID
                                            .AssociationFrom = lngDivisionID                          'Set the Division Id
                                            .HeaderLink = 1

                                            vcReturnFlag = .ManageAssociation()                 'Add the new assocition to the list of existing assocations
                                            'If vcReturnFlag = "Duplicate" Then
                                            '    litMessage.Text = "<script>alert('Selected Association Already exist');</script>"
                                            'End If
                                        End With
                                    End If
                                End If
                            Next

                        End If
                    End If

                    If chkCorrespondance.Checked Then
                        'Store Non BizContact Id for Future reference to upload notes and correspondance
                        objLeads.ManageImportActionItemReference(CCommon.ToString(drDestination(strUniqueContID)))
                    End If

                    'Import all custom fields
                    intCount = 0
                    For Each drHead In dataTableHeading.Rows

                        If drHead.Item(2) <> 0 Then

                            objImpWzd.FldId = drHead("Id")

                            If drHead.Item(3) = "L" Then
                                objImpWzd.RecId = lngDivisionID
                            ElseIf drHead.Item(3) = "C" Then
                                objImpWzd.RecId = lngContactID
                            End If

                            If CCommon.ToString(drDestination(drHead("Destination"))).Trim.Length > 0 Then
                                If drHead.Item("vcAssociatedControlType") = "SelectBox" Then
                                    objImpWzd.FldValue = objImpWzd.GetStateAndCountry(18, IIf(IsDBNull(drDestination(drHead("Destination").ToString)), 0, drDestination(drHead("Destination").ToString)), Session("DomainID"), CCommon.ToLong(drHead("numListID")))
                                    If CCommon.ToLong(objImpWzd.FldValue) > 0 Then
                                        objImpWzd.UpdateCustomeField()
                                    End If
                                ElseIf drHead.Item("vcAssociatedControlType") = "DateField" Then
                                    If IsDate(drDestination(drHead("Destination").ToString)) = True Then
                                        objImpWzd.FldValue = FormattedDateFromDate(drDestination(drHead("Destination").ToString), Session("DateFormat"))
                                    Else : objImpWzd.FldValue = ""
                                    End If
                                    If objImpWzd.FldValue.Length > 0 Then
                                        objImpWzd.UpdateCustomeField()
                                    End If
                                ElseIf drHead.Item("vcAssociatedControlType") = "CheckBox" Then
                                    Try
                                        If UCase(drDestination(drHead("Destination").ToString)) = "TRUE" Or UCase(drDestination(drHead("Destination").ToString)) = "YES" Then
                                            objImpWzd.FldValue = 1
                                        Else : objImpWzd.FldValue = 0
                                        End If
                                    Catch ex As Exception
                                        objImpWzd.FldValue = 0
                                    End Try
                                    objImpWzd.UpdateCustomeField()
                                Else
                                    objImpWzd.FldValue = CCommon.ToString(drDestination(drHead("Destination")))
                                    If objImpWzd.FldValue.Length > 0 Then
                                        objImpWzd.UpdateCustomeField()
                                    End If
                                End If
                            End If
                        End If
                        intCount += 1
                    Next
                    lngContactID = 0
                    lngCompanyId = 0
                    lngDivisionID = 0
                Next


                litMessage.Text = "Records are sucessfully saved into database"
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            Finally
                pgBar.Style.Add("display", "none")
                Session("IntCount") = ""
                Session("FileLocation") = ""
                Session("dataTableDestinationObj") = ""
                Session("dataTableHeadingObj") = ""
                Session("dllValue") = ""
                tbldtls.Rows.Clear()
            End Try
        End Sub
#End Region

        Public Sub SetProperties(ByRef objLeadBoxData As FormGenericLeadBox, ByVal sPropertyName As String, ByVal sPropertyValue As String)
            Try
                Dim theType As Type = objLeadBoxData.GetType
                Dim myProperties() As PropertyInfo = theType.GetProperties((BindingFlags.Public Or BindingFlags.Instance))
                Dim fillObject As Object = objLeadBoxData
                Dim PropertyItem As PropertyInfo
                For Each PropertyItem In myProperties                                                       'Loop thru Writeonly public properties
                    With PropertyItem
                        If PropertyItem.Name = sPropertyName Then                                           'Compare the class property name with that of the db column name
                            Select Case PropertyItem.PropertyType.ToString()                                'Selecting the datataype of teh property
                                Case "System.String"
                                    PropertyItem.SetValue(fillObject, sPropertyValue, Nothing)
                                Case "System.Int64", "System.Int32", "System.Int16"                         'Integer type properties
                                    If IsNumeric(sPropertyValue) Then
                                        PropertyItem.SetValue(fillObject, CInt(sPropertyValue), Nothing)    'Typecasting to integer before setting the value
                                    End If
                                Case "System.Boolean"                                                       'Boolean properties
                                    If IsBoolean(sPropertyValue) Then
                                        PropertyItem.SetValue(fillObject, CBool(sPropertyValue), Nothing)   'Typecasting to boolean before setting the value
                                    End If
                            End Select
                            Exit Sub
                        End If
                    End With
                Next
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Function IsBoolean(ByVal sTrg As String) As Boolean
            Try
                Dim intValue As Boolean = Convert.ToBoolean(sTrg)                                   'Try Casting to a boolean
            Catch Ex As InvalidCastException
                Return False                                                                        'if it throws an error then return false to indicate that it is non boolean
            End Try
            Return True                                                                             'Test passed, its booelan
        End Function

        Sub AssignValuesEditBox(ByVal objLeads As CLeads, ByVal vcValue As String, ByVal vcColumnName As String)
            Try
                Select Case vcColumnName
                    Case "vcFirstName"
                        objLeads.FirstName = IIf(vcValue.Length = 0, "-", vcValue)
                    Case "vcLastName"
                        objLeads.LastName = IIf(vcValue.Length = 0, "-", vcValue)
                    Case "vcTitle"
                        objLeads.Title = vcValue
                    Case "vcEmail"
                        objLeads.Email = vcValue
                    Case "vcComPhone"
                        objLeads.ComPhone = vcValue
                    Case "vcStreet"
                        objLeads.PStreet = vcValue
                    Case "vcCity"
                        objLeads.PCity = vcValue
                    Case "numPhone"
                        objLeads.ContactPhone = vcValue
                    Case "numPhoneExtension"
                        objLeads.PhoneExt = vcValue
                    Case "vcWebSite"
                        objLeads.WebSite = vcValue
                    Case "numCell"
                        objLeads.Cell = vcValue
                    Case "numHomePhone"
                        objLeads.HomePhone = vcValue
                    Case "vcDivisionName"
                        objLeads.DivisionName = vcValue
                    Case "numCompanyIndustry"
                        objLeads.CompanyIndustry = vcValue
                    Case "vcPostalCode"
                        objLeads.PPostalCode = vcValue
                    Case "txtComments"
                        objLeads.Comments = vcValue
                    Case "vcCompanyName"
                        objLeads.CompanyName = vcValue
                    Case "vcFax"
                        objLeads.Fax = vcValue
                    Case "vcShipPostCode"
                        objLeads.SPostalCode = vcValue
                    Case "vcShipCity"
                        objLeads.SCity = vcValue
                    Case "vcShipStreet"
                        objLeads.SStreet = vcValue
                    Case "vcShipStreet"
                        objLeads.SStreet = vcValue
                    Case "vcComFax"
                        objLeads.ComFax = vcValue
                    Case "vcBillStreet"
                        objLeads.Street = vcValue
                    Case "vcBillCity"
                        objLeads.City = vcValue
                    Case "vcBillPostCode"
                        objLeads.PostalCode = vcValue
                    Case "vcCompanyDiff"
                        objLeads.CompanyDiffValue = vcValue
                End Select
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub AssignValuesSelectBox(ByVal objLeads As CLeads, ByVal vcValue As String, ByVal vcColumnName As String)
            Try
                Select Case vcColumnName
                    Case "numRecOwner"
                        objLeads.UserCntID = CCommon.ToLong(vcValue)
                    Case "numAssignedTo"
                        objLeads.AssignedTo = CCommon.ToLong(vcValue)
                    Case "vcCategory"
                        objLeads.Category = CLng(vcValue)
                    Case "numState"
                        objLeads.PState = CLng(vcValue)
                    Case "numCountry"
                        objLeads.PCountry = CLng(vcValue)
                    Case "vcPosition"
                        objLeads.Position = CLng(vcValue)
                    Case "numTeam"
                        objLeads.Team = CLng(vcValue)
                    Case "numAnnualRevID"
                        objLeads.AnnualRevenue = CLng(vcValue)
                    Case "numNoOfEmployeesId"
                        objLeads.NumOfEmp = CLng(vcValue)
                    Case "vcHow"
                        objLeads.InfoSource = CLng(vcValue)
                    Case "vcProfile"
                        objLeads.Profile = CLng(vcValue)
                    Case "numEmpStatus"
                        objLeads.EmpStatus = CLng(vcValue)
                    Case "numCompanyIndustry"
                        objLeads.CompanyIndustry = CLng(vcValue)
                    Case "numCompanyType"
                        objLeads.CompanyType = CLng(vcValue)
                    Case "numCompanyRating"
                        objLeads.CompanyRating = CLng(vcValue)
                    Case "numStatusID"
                        objLeads.StatusID = CLng(vcValue)
                    Case "vcShipCountry"
                        objLeads.SCountry = CLng(vcValue)
                    Case "numTerID"
                        objLeads.TerritoryID = CLng(vcValue)
                    Case "vcShipState"
                        objLeads.SState = CLng(vcValue)
                    Case "numCampaignID"
                        objLeads.CampaignID = CLng(vcValue)
                    Case "numCompanyCredit"
                        objLeads.CompanyCredit = CLng(vcValue)
                    Case "vcDepartment"
                        objLeads.Department = CLng(vcValue)
                    Case "numFollowUpStatus"
                        objLeads.FollowUpStatus = CLng(vcValue)
                    Case "vcBilState"
                        objLeads.State = CLng(vcValue)
                    Case "vcBillCountry"
                        objLeads.Country = CLng(vcValue)
                    Case "numCompanyDiff"
                        objLeads.CompanyDiff = CLng(vcValue)
                End Select
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub AssignValuesTextBox(ByVal objLeads As CLeads, ByVal vcValue As String, ByVal vcColumnName As String)
            Try
                Select Case vcColumnName
                    Case "txtComments"
                        objLeads.Comments = vcValue
                    Case "txtNotes"
                        objLeads.Notes = vcValue
                End Select
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub lbDownloadCSV_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbDownloadCSV.Click
            Dim objImport As New ImportWizard
            objImport.DomainId = Session("DomainID")
            objImport.Relationship = ddlRelationship.SelectedValue
            objImport.ImportType = 1
            Dim DataTable As DataTable = objImport.GetConfigurationTable()

            If DataTable.Rows.Count = 0 Then
                litMessage.Text = "Please Configure Fields"
                Exit Sub
            End If

            Dim dt As New DataTable
            For Each dr As DataRow In DataTable.Rows
                dt.Columns.Add(dr("vcFormFieldName").ToString.Replace(",", " "))
            Next
            Response.Clear()
            Response.Buffer = True
            Response.ContentType = "application/vnd.ms-excel"
            Response.AddHeader("Content-Disposition", "inline;filename=ImportOrganizationTemplate." & Now.Month.ToString & "." & Now.Day.ToString & "." & Now.Year.ToString & ".csv")
            Response.Charset = ""
            Me.EnableViewState = False
            CSVExport.ProduceCSV(dt, Response.Output, True)
            Response.End()
        End Sub

        Function ValidateMandatoryColumns() As Boolean
            Try
                Dim objImport As New ImportWizard
                objImport.DomainId = Session("DomainID")
                objImport.Relationship = ddlRelationship.SelectedValue
                objImport.ImportType = 1
                Dim DataTable As DataTable = objImport.GetConfigurationTable()

                If DataTable.Select(" numFormFieldId = '337'", "").Count = 0 Then
                    litMessage.Text = "Field ""First Name"" is mandatory,your option is to add given field to Column Configuration and try again"
                    btnImports.Visible = False
                    Exit Function
                End If
                If DataTable.Select(" numFormFieldId = '338'", "").Count = 0 Then
                    litMessage.Text = "Field ""Last Name"" is mandatory,your option is to add given field to Column Configuration and try again"
                    btnImports.Visible = False
                    Exit Function
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Private Sub ddlRelationship_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlRelationship.SelectedIndexChanged
            Try
                ValidateMandatoryColumns()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub
    End Class
End Namespace
