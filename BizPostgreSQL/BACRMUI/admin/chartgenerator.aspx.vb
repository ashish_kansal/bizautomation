Imports System
Imports System.Drawing
Imports System.Drawing.Imaging
Imports System.IO
'Imports ASPNET.StarterKit.Chart
Imports System.Web.UI.DataVisualization.Charting
Imports BACRM.BusinessLogic.Common
Namespace BACRM.UserInterface.Admin
    Public Class chartgenerator
        Inherits BACRMPage


#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Try
                ' set return type to png image format
                Response.ContentType = "image/png"
                Dim xValues, yValues, chartType, print As String
                Dim boolPrint As Boolean

                ' Get input parameters from query string
                chartType = GetQueryStringVal( "chartType")
                xValues = HttpUtility.UrlDecode(GetQueryStringVal( "xValues"))
                yValues = GetQueryStringVal( "yValues")
                print = GetQueryStringVal( "Print")

                If chartType Is Nothing Then chartType = ""

                ' check for printing option 
                If print Is Nothing Then
                    boolPrint = False
                Else
                    Try
                        boolPrint = Convert.ToBoolean(print)
                    Catch
                    End Try
                End If

                If Not (xValues Is Nothing) And Not (yValues Is Nothing) Then

                    Dim Chart1 As Chart = New Chart()
                    Chart1.Series.Add(New Series())
                    Chart1.Width = "800"

                    Dim strArray As String() = yValues.Split("|".ToCharArray())

                    Dim yVal As Double() = New Double(strArray.Length - 1) {}
                    For i As Integer = 0 To strArray.Length - 1
                        yVal(i) = Double.Parse(strArray(i))
                    Next

                    Dim xName As String() = xValues.Split("|".ToCharArray())

                    Chart1.Series(0).Points.DataBindXY(xName, yVal)

                    Chart1.Palette = System.Web.UI.DataVisualization.Charting.ChartColorPalette.EarthTones
                    Chart1.ImageType = System.Web.UI.DataVisualization.Charting.ChartImageType.Png

                    Chart1.ChartAreas.Add(New ChartArea())
                    Chart1.ChartAreas(0).Area3DStyle.Enable3D = True

                    Dim bgColor As Color
                    If boolPrint Then
                        bgColor = Color.White
                    Else : bgColor = Color.FromArgb(255, 253, 244)
                    End If
                    Dim StockBitMap As Bitmap
                    Dim memStream As New MemoryStream
                    Select Case chartType
                        Case "bar"
                            Chart1.Series(0).ChartType = SeriesChartType.Bar
                        Case "pie"
                            Chart1.Legends.Add(New Legend("Type"))
                            Chart1.Legends("Type").Title = "Type"
                            Chart1.Series(0).Legend = "Type"
                            'Chart1.Legends(0).Enabled = False

                            Chart1.Series(0).ChartType = SeriesChartType.Pie
                        Case "column"
                            Chart1.Series(0).ChartType = SeriesChartType.Column
                    End Select
                    'Select Case chartType
                    '    Case "bar"
                    '        Chart1.Series(0).ChartType = SeriesChartType.Bar

                    '        'Dim bar As New BarGraph(bgColor)
                    '        'bar.VerticalLabel = "Num Responses"
                    '        'bar.VerticalTickCount = 5
                    '        'bar.ShowLegend = True
                    '        'bar.ShowData = False
                    '        'bar.Height = 400
                    '        'bar.Width = 700

                    '        'bar.CollectDataPoints(xValues.Split("|".ToCharArray()), yValues.Split("|".ToCharArray()))
                    '        'StockBitMap = bar.Draw()
                    '    Case Else
                    '        Chart1.Series(0).ChartType = SeriesChartType.Pie

                    '        'Dim pc As New PieChart(bgColor)
                    '        'pc.CollectDataPoints(xValues.Split("|".ToCharArray()), yValues.Split("|".ToCharArray()))
                    '        'StockBitMap = pc.Draw()
                    'End Select

                    ' Render BitMap Stream Back To Client
                    'StockBitMap.Save(memStream, ImageFormat.Png)
                    Chart1.SaveImage(memStream)
                    memStream.WriteTo(Response.OutputStream)
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub 'Page_Load
    End Class
End Namespace