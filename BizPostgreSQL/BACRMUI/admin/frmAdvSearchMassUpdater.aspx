<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" CodeBehind="frmAdvSearchMassUpdater.aspx.vb"
    Inherits="BACRM.UserInterface.Admin.frmAdvSearchMassUpdater" ValidateRequest="false"
    MasterPageFile="~/common/Popup.Master" %>

<%@ Register TagPrefix="BizCalendar" TagName="Calendar" Src="../include/calandar.ascx" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <link rel="stylesheet" type="text/css" href="../CSS/jscal2.css" />
    <link rel="stylesheet" type="text/css" href="../CSS/border-radius.css" />
    <script type="text/javascript" src="../JavaScript/jscal2.js"></script>
    <script type="text/javascript" src="../JavaScript/en.js"></script>
    <title>Advance Search Mass Update</title>
    <script type="text/javascript" language="javascript" src="../javascript/AdvSearchColumnCustomization.js"></script>
    <script type="text/javascript">
        function ConfirmUpdate() {
            if (confirm('This will update Vendor Minimum quantity of all selected item vendors. Are you sure you want to update Vendor Minimun Quantity of all selected item vendors ?') == true) {
                //alert(1);
                return true;
            }
            else {
                //alert(2);
                return false;
            }
        }

        function treeExpandAllNodes() {
            var treeView = $find("rtvCategory");
            var nodes = treeView.get_allNodes();

            for (var i = 0; i < nodes.length; i++) {
                if (nodes[i].get_nodes() != null) {
                    nodes[i].expand();
                }
            }
        }

        function treeCollapseAllNodes() {
            var treeView = $find("rtvCategory");
            var nodes = treeView.get_allNodes();

            for (var i = 0; i < nodes.length; i++) {
                if (nodes[i].get_nodes() != null) {
                    nodes[i].collapse();
                }
            }
        }

        function ValidateCategorySelection() {
            if ($("[id$=rbSubscribe]").is(":checked")) {
                if (confirm("This will Subscribe the items from your selected search results to the categories in category profile you�ve selected here. Are you sure you want to do that?")) {
                    return true;
                } else {
                    return false;
                }
            } else if ($("[id$=rbUnSubscribe]").is(":checked")) {
                if (confirm("This will Un-Subscribe the items from your selected search results to the categories in category profile you�ve selected here. Are you sure you want to do that?")) {
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <div class="input-part">
        <div class="right-input">
            <asp:Button ID="btnSave" Text="Update" runat="server" CssClass="button"></asp:Button>&nbsp;&nbsp;<input
                class="button" id="btnClose" onclick="javascript: CloseThisWin()" type="button"
                value="Close">&nbsp;&nbsp;
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageTitle" runat="server" ClientIDMode="Static">
    Mass Update
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="Content" runat="server" ClientIDMode="Static">
    <asp:ScriptManager runat="server" ID="scmgr" />
    <asp:Table ID="tblAdvSearchMassUpdater" runat="server" Width="800px" GridLines="None"
        BorderColor="black" CssClass="aspTable" BorderWidth="1" Height="250">
        <asp:TableRow>
            <asp:TableCell VerticalAlign="Top">
                <table cellpadding="2" cellspacing="0" width="100%">
                    <tr>
                        <td align="Right" class="normal1">Available Fields
                        </td>
                        <td class="normal1">
                            <asp:DropDownList ID="lstAvailablefld" runat="server" CssClass="signup" AutoPostBack="True" style="height:20px !important">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr id="rowValue" runat="server">
                        <td align="Right" class="normal1">
                            <asp:Label ID="lblFieldName" runat="Server"></asp:Label>
                        </td>
                        <td class="normal1">
                            <asp:TextBox ID="txtValue" runat="Server" CssClass="signup" Visible="False" EnableViewState="false"
                                MaxLength="50"></asp:TextBox><asp:DropDownList ID="ddlValue" runat="server" Visible="False"
                                    CssClass="signup">
                                </asp:DropDownList>
                            <asp:CheckBox ID="cbValue" runat="server" Visible="False" EnableViewState="false"></asp:CheckBox><BizCalendar:Calendar ID="cal" Visible="false" runat="server" />
                            <telerik:RadComboBox AccessKey="V" ID="radCmbCompany" Width="195px" DropDownWidth="600px"
                                OnClientItemsRequested="OnClientItemsRequestedOrganization"
                                ClientIDMode="Static"
                                ShowMoreResultsBox="true"
                                Visible="true" Skin="Default" runat="server" AllowCustomText="True" EnableLoadOnDemand="True">
                                <WebServiceSettings Path="../common/Common.asmx" Method="GetCompanies" />
                            </telerik:RadComboBox>
                            <asp:CheckBox ID="chkIsPrimary" runat="server" Visible="False" EnableViewState="false"
                                Text="Set as Primary Vendor"></asp:CheckBox>
                            <asp:Panel runat="server" ID="pnlItemCategory" Visible="false">
                                <asp:UpdatePanel runat="server" ID="uplItemCategory" UpdateMode="Conditional" ChildrenAsTriggers="true">
                                    <ContentTemplate>
                                        <table>
                                            <tr>
                                                <td colspan="2">
                                                    <asp:RadioButton ID="rbSubscribe" runat="server" Text="Subscribe" GroupName="ItemCategory" Checked="true" />
                                                    <asp:RadioButton ID="rbUnSubscribe" runat="server" Text="Un-Subscribe" GroupName="ItemCategory" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width:100px"><b>Category Profile:</b></td>
                                                <td>
                                                    <asp:DropDownList ID="ddlCategoryProfile" runat="server" CssClass="signup" AutoPostBack="true"></asp:DropDownList></td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                    <asp:Label ID="lblProfileSites" runat="server" Text=""></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                    <div style="border: 1px solid lightgray; padding: 5px; overflow: auto; height: 507px; width: 400px; padding-top: 6px;">
                                                        <a href="javascript: treeExpandAllNodes();">Expand All</a>
                                                        &nbsp;<a href="javascript: treeCollapseAllNodes();">Collapse All</a>
                                                        <telerik:RadTreeView ID="rtvCategory" CheckBoxes="true" ClientIDMode="Static" runat="server">
                                                            <DataBindings>
                                                                <telerik:RadTreeNodeBinding Expanded="true"></telerik:RadTreeNodeBinding>
                                                            </DataBindings>
                                                        </telerik:RadTreeView>
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </asp:Panel>
                        </td>
                    </tr>
                </table>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
    <asp:Literal ID="litArrayString" runat="server" EnableViewState="True"></asp:Literal>
    <input type="hidden" runat="server" id="hdDbAssociatedControl"><input type="hidden"
        runat="server" id="hdDbColumnListId">
    <input type="hidden" runat="server" id="hdDbColumnListItemType">
    <input type="hidden" runat="server" id="hdOrigDbColumnName">
    <input type="hidden" runat="server" id="hdFieldDataType">
    <input type="hidden" runat="server" id="hdLookBackTableName">
    <input type="hidden" runat="server" id="hdColumnValue">
    <input type="hidden" runat="server" id="hdEntityIdRefList">
    <asp:Literal ID="litClientMessage" runat="server" EnableViewState="False"></asp:Literal>
</asp:Content>
