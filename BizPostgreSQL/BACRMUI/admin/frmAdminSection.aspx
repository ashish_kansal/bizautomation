<%@ Register TagPrefix="menu1" TagName="webmenu" Src="../include/webmenu.ascx" %>

<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" CodeBehind="frmAdminSection.aspx.vb"
    Inherits="BACRM.UserInterface.Admin.frmAdminSection" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Page-Enter" content="blendTrans(Duration=0.2)">
    <meta http-equiv="Page-Exit" content="blendTrans(Duration=0.2)">
    <link rel="stylesheet" href="~/CSS/master.css" type="text/css" />
    <title>Admin Section</title>

    <script language="javascript">
        function OpenTickler() {
            window.location.href = "../common/frmticklerdisplay.aspx"
            return false;
        }
        function AlertB4GTableRecreation() {
            return confirm('You have chosen to refresh the data for the Custom Reports. Please confirm.');
        }
    </script>

</head>
<body>
    <form id="Form1" method="post" runat="server">
    <menu1:webmenu ID="webmenu1" runat="server"></menu1:webmenu>
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td valign="bottom">
                <table class="TabStyle">
                    <tr>
                        <td>
                            &nbsp;&nbsp;&nbsp;Admin Section&nbsp;&nbsp;&nbsp;
                        </td>
                    </tr>
                </table>
            </td>
            <td align="right">
                <input type="button" value="Duplicates and Association Mgmt." style="width: 200px"
                    class="button" onclick="document.location.href='frmDuplicateRecordSearch.aspx';">&nbsp;
                <asp:Button ID="btnReCreateCustomReportsData" Width="170px" runat="server" CssClass="button"
                    Text="Refresh Custom Report Data"></asp:Button>&nbsp;
                <asp:Button ID="btnBack" Width="50" runat="server" CssClass="button" Text="Back">
                </asp:Button>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:Table ID="tbl1" CellPadding="0" CellSpacing="0" BorderWidth="1" runat="server"
                    Width="100%" BorderColor="black" GridLines="None">
                    <asp:TableRow>
                        <asp:TableCell HorizontalAlign="Center" VerticalAlign="Top">
									<br>
									<IMG SRC="../images/AdminSection1.gif" useMap="#Map1" border="0">
                        </asp:TableCell>
                        <asp:TableCell HorizontalAlign="Center" VerticalAlign="Top">
									<br>
									<IMG SRC="../images/AdminSection2.gif" useMap="#Map2" border="0">
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </td>
        </tr>
    </table>
    <map name="Map1">
        <area shape="RECT" coords="4,11,287,40" href="../admin/frmMasterList.aspx">
        <area shape="RECT" coords="4,112,287,138" href="../admin/frmFormConfigWizard.aspx">
        <area shape="RECT" coords="4,233,287,264" href="../admin/AdminBusinessProcess.aspx">
        <area shape="RECT" coords="4,342,287,371" href="../admin/frmManageAutoRoutingRules.aspx">
        <area shape="RECT" coords="4,464,287,491" href="../items/Item.aspx">
    </map>
    <map name="Map2">
        <area shape="RECT" coords="6,12,290,38" href="../admin/userlist.aspx">
        <area shape="RECT" coords="6,112,290,141" href="../admin2/frmAdminAOI.aspx">
        <area shape="RECT" coords="6,236,290,263" href="../Alerts/frmBAM.aspx">
        <area shape="RECT" coords="6,346,290,373" href="../admin/frmAccountingIntegration.aspx">
        <area shape="RECT" coords="6,463,290,491" href="../admin/cfllist.aspx">
    </map>
    </form>
</body>
</html>
