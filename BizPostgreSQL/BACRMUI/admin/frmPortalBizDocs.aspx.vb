Imports BACRM.BusinessLogic.Opportunities
Imports BACRM.BusinessLogic.Common
Namespace BACRM.UserInterface.Admin

    Public Class frmPortalBizDocs
        Inherits BACRMPage

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub
        Protected WithEvents btnSave As System.Web.UI.WebControls.Button
        Protected WithEvents btnSaveAndClose As System.Web.UI.WebControls.Button
        Protected WithEvents btnClose As System.Web.UI.WebControls.Button
        Protected WithEvents tbl As System.Web.UI.WebControls.Table
        Protected WithEvents lstAvailablefld As System.Web.UI.WebControls.ListBox
        Protected WithEvents btnAdd As System.Web.UI.WebControls.Button
        Protected WithEvents btnRemove As System.Web.UI.WebControls.Button
        Protected WithEvents lstAddfld As System.Web.UI.WebControls.ListBox
        Protected WithEvents txthidden As System.Web.UI.WebControls.TextBox


        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.
        Private designerPlaceholderDeclaration As System.Object

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Try
                If Not IsPostBack Then
                    


                   
                    GetUserRightsForPage(13, 25)
                    If m_aryRightsForPage(RIGHTSTYPE.VIEW) = 0 Then
                        Response.Redirect("../admin/authentication.aspx?mesg=AS")
                    ElseIf m_aryRightsForPage(RIGHTSTYPE.UPDATE) = 0 Then
                        btnSave.Visible = False
                        btnSaveAndClose.Visible = False
                    End If

                    objCommon.sb_FillListFromDB(lstAvailablefld, 27, Session("DomainID"))
                    FilllIstBox()
                End If
                btnAdd.Attributes.Add("OnClick", "return move(lstAvailablefld,lstAddfld)")
                btnRemove.Attributes.Add("OnClick", "return remove1(lstAddfld,lstAvailablefld)")
                btnSave.Attributes.Add("onclick", "return Save()")
                btnSaveAndClose.Attributes.Add("onclick", "return Save()")
                btnClose.Attributes.Add("OnClick", "return Close()")
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Sub FilllIstBox()
            Try
                Dim objOppBizDocs As New OppBizDocs
                Dim dtPortalBizDocs As DataTable
                objOppBizDocs.DomainID = Session("DomainID")
                dtPortalBizDocs = objOppBizDocs.GetPortalBizDocs
                lstAddfld.DataSource = dtPortalBizDocs
                lstAddfld.DataTextField = "vcData"
                lstAddfld.DataValueField = "numListItemID"
                lstAddfld.DataBind()
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub save()
            Try
                Dim objOppBizDocs As New OppBizDocs
                objOppBizDocs.strBizDocID = txthidden.Text
                objOppBizDocs.DomainID = Session("DomainID")
                objOppBizDocs.ManagePortalBizDocs()
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
            Try
                save()
                FilllIstBox()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub btnSaveAndClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveAndClose.Click
            Try
                save()
                Response.Write("<script>window.close();</script>")
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

    End Class
End Namespace
