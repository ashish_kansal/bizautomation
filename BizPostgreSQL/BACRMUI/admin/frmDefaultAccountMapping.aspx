﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmDefaultAccountMapping.aspx.vb"
    Inherits=".frmDefaultAccountMapping" MasterPageFile="~/common/Popup.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Default Account Mapping</title>
    <script type="text/javascript" language="javascript">
        function Save() {
            var ddlSalseTax = Number($("#ddlSalseTax").val());
            var ddlPurchaseTaxCredit = Number($("#ddlPurchaseTaxCredit").val());

            if (ddlSalseTax > 0 && ddlPurchaseTaxCredit > 0 && ddlSalseTax == ddlPurchaseTaxCredit) {
                alert("Sales Tax Payable account and Purchase Tax Credit account would not be same.");
                return false;
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <div class="input-part">
        <div class="right-input">
            <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" OnClientClick="return Save();"></asp:Button>&nbsp;
            <asp:Button ID="btnClose" runat="server" CssClass="button" Text="Close" OnClientClick="return Close();"></asp:Button>&nbsp;&nbsp;
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageTitle" runat="server" ClientIDMode="Static">
    Default Account Mapping
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="Content" runat="server" ClientIDMode="Static">
    <table width="600px">
        <tr>
            <td class="normal4" align="center" colspan="2">
                <asp:Literal ID="litMessage" runat="server"></asp:Literal>
            </td>
        </tr>
        <tr>
            <td class="normal1" align="right">Account Receivable
            </td>
            <td>
                <asp:DropDownList ID="ddlARAccount" runat="server" CssClass="signup" Width="350">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="normal1" align="right">Account Payable
            </td>
            <td>
                <asp:DropDownList ID="ddlAPAccount" runat="server" CssClass="signup" Width="350">
                </asp:DropDownList>
            </td>
        </tr>
        <%-- <tr>
                        <td class="normal1" align="right">
                            Billable Time & Expenses
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlBillable" runat="server" CssClass="signup" Width="350">
                            </asp:DropDownList>
                        </td>
                    </tr>--%>
        <tr>
            <td class="normal1" align="right">Sales Tax Payable
            </td>
            <td>
                <asp:DropDownList ID="ddlSalseTax" runat="server" CssClass="signup" Width="350">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="normal1" align="right">Purchase Tax Credit
            </td>
            <td>
                <asp:DropDownList ID="ddlPurchaseTaxCredit" runat="server" CssClass="signup" Width="350">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="normal1" align="right">Discount Given
            </td>
            <td>
                <asp:DropDownList ID="ddlDiscountGiven" runat="server" CssClass="signup" Width="350">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="normal1" align="right">Shipping Income
            </td>
            <td>
                <asp:DropDownList ID="ddlShippingIncome" runat="server" CssClass="signup" Width="350">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="normal1" align="right">Late Charges
            </td>
            <td>
                <asp:DropDownList ID="ddlLateCharges" runat="server" CssClass="signup" Width="350">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="normal1" align="right">UnDeposited Funds
            </td>
            <td>
                <asp:DropDownList ID="ddlUndepositedFunds" runat="server" CssClass="signup" Width="350">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="normal1" align="right">COGS
            </td>
            <td>
                <asp:DropDownList ID="ddlCOGs" runat="server" CssClass="signup" Width="350">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="normal1" align="right">Employee Payroll Expense
            </td>
            <td>
                <asp:DropDownList ID="ddlEmpPayExp" runat="server" CssClass="signup" Width="350">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="normal1" align="right">Contract Employee Payroll Expense
            </td>
            <td>
                <asp:DropDownList ID="ddlCEmpPayExp" runat="server" CssClass="signup" Width="350">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="normal1" align="right">Payroll Liability
            </td>
            <td>
                <asp:DropDownList ID="ddlPayrollLiability" runat="server" CssClass="signup" Width="350">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="normal1" align="right">Work in Progress
            </td>
            <td>
                <asp:DropDownList ID="ddlWorkInProgress" runat="server" CssClass="signup" Width="350">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="normal1" align="right">Deductions form Employee Payroll
            </td>
            <td>
                <asp:DropDownList ID="ddlDeductionPayroll" runat="server" CssClass="signup" Width="350">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="normal1" align="right">Reconciliation Discrepancies
            </td>
            <td>
                <asp:DropDownList ID="ddlReconciliationDiscrepancies" runat="server" CssClass="signup" Width="350">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="normal1" align="right">UnCategorized Income 
            </td>
            <td>
                <asp:DropDownList ID="ddlUnCategorizedIncome" runat="server" CssClass="signup" Width="350">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="normal1" align="right">UnCategorized Expense
            </td>
            <td>
                <asp:DropDownList ID="ddlUncategorizedExpense" runat="server" CssClass="signup" Width="350">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="normal1" align="right">Foreign Exchange Gain/Loss
            </td>
            <td>
                <asp:DropDownList ID="ddlForeignExchange" runat="server" CssClass="signup" Width="350">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="normal1" align="right">Inventory Adjustment
            </td>
            <td>
                <asp:DropDownList ID="ddlInventoryAdjustment" runat="server" CssClass="signup" Width="350">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="normal1" align="right">Sales Clearing <font style="color:red">*</font>
            </td>
            <td>
                <asp:DropDownList ID="ddlSalesClearing" runat="server" CssClass="signup" Width="350">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="normal1" align="right">Purchase Clearing 
            </td>
            <td>
                <asp:DropDownList ID="ddlPurchaseClearing" runat="server" CssClass="signup" Width="350">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="normal1" align="right">Purchase Price Variance 
            </td>
            <td>
                <asp:DropDownList ID="ddlPurchasePriceVariance" runat="server" CssClass="signup" Width="350">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="normal1" align="right">Opening Balance Equity
            </td>
            <td>
                <asp:DropDownList ID="ddlOpeningBalance" runat="server" CssClass="signup" Width="350">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="normal1" align="right">Reimbursable Expense Receivable
            </td>
            <td>
                <asp:DropDownList ID="ddlReimbursable" runat="server" CssClass="signup" Width="350">
                </asp:DropDownList>
            </td>
        </tr>
        <%-- <tr>
                        <td class="normal1" align="right">
                            Non Employee Commission Expense
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlCommissionExpense" runat="server" CssClass="signup" Width="350">
                            </asp:DropDownList>
                        </td>
                    </tr>--%>
    </table>
</asp:Content>
