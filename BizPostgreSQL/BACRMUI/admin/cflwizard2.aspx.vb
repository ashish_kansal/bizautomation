
'' Modified By anoop jayaraj
Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Common
Namespace BACRM.UserInterface.Admin
    Public Class cflwizard2
        Inherits BACRMPage
        Protected WithEvents CFWFgrp As System.Web.UI.WebControls.DropDownList
        Protected WithEvents btnNext As System.Web.UI.WebControls.Button
        Protected WithEvents btnCancel As System.Web.UI.WebControls.Button
        Protected location As String

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Try

                If IsPostBack = False Then
                    
                    location = CCommon.ToInteger(GetQueryStringVal( "location"))
                    LoadLocation()
                    If location > 0 Then
                        If Not CFWFgrp.Items.FindByValue(location) Is Nothing Then
                            CFWFgrp.Items.FindByValue(location).Selected = True
                        End If
                        Session("PageId") = ""
                    End If
                End If
                Session("PageId") = ""
                Session("TabId") = ""
                Session("fldtype") = ""
                Session("fldlbl") = ""
                Session("fldord") = ""
                btnNext.Attributes.Add("onclick", "return Validate()")
                btnCancel.Attributes.Add("onclick", "return Close()")
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Sub LoadLocation()
            Try
                Dim dtRelationship As DataTable
                Dim ObjCusFlds As New CustomFields
                ObjCusFlds.DomainID = Session("DomainID")
                dtRelationship = ObjCusFlds.GetAdminRelationship
                CFWFgrp.DataSource = dtRelationship
                CFWFgrp.DataTextField = "loc_name"
                CFWFgrp.DataValueField = "loc_id"
                CFWFgrp.DataBind()
                CFWFgrp.Items.Insert(0, "--Select One--")
                CFWFgrp.Items.FindByText("--Select One--").Value = 0
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub btnNext_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNext.Click
            Try
                Session("PageId") = CFWFgrp.SelectedItem.Value
                Response.Redirect("../admin/cflwizard1.aspx?location=" + CFWFgrp.SelectedValue)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

    End Class
End Namespace