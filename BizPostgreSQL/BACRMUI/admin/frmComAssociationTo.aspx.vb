
Imports BACRM.BusinessLogic.Opportunities
Imports BACRM.BusinessLogic.Prospects
Imports BACRM.BusinessLogic.Common
Imports Telerik.Web.UI

Namespace BACRM.UserInterface.Admin
    Public Class frmComAssociationTo
        Inherits BACRMPage
        Dim vcDivIDList, vcContactIDList As String
        Dim lngAssFromDivID As Long
        Dim boolAssociateDivision, boolAssociateContacts As Boolean
        Dim boolAllowSingleAssociationsDisplay As Boolean = True

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
            Me.ID = "frmComAssociationTo"

        End Sub

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            Try
                InitializeComponent()
                CCommon.InitializeOrganizationClientSideTemplate(Session("DomainID"), Session("UserContactID"), radCmbCompany)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

#End Region

        '''' -----------------------------------------------------------------------------
        '''' <summary>
        ''''     This event is fired each time thepage is called. In this event we will 
        ''''     get the data from the DB create the form.
        '''' </summary>
        '''' <param name="sender">Represents the sender object.</param>
        '''' <param name="e">Represents the EventArgs.</param>
        '''' <remarks>
        '''' </remarks>
        '''' <history>
        '''' 	[Debasish Tapan Nag]	12/29/2005	Modified
        '''' </history>
        '''' -----------------------------------------------------------------------------
        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Try
                lngAssFromDivID = CCommon.ToLong(GetQueryStringVal("numDivisionId"))              'Getting the Division Id from the Query String
                If Not IsPostBack Then

                    objCommon = New CCommon
                    objCommon.UserCntID = Session("UserContactID")
                    objCommon.DivisionID = lngAssFromDivID
                    objCommon.charModule = "D"
                    objCommon.GetCompanySpecificValues1()

                    chkChild.Text = String.Format("is child company of <strong>{0}</strong>", objCommon.CompanyName)

                    '            If boolAssociateDivision Then
                    'Check if we are associatiating divisions or copying/ merging contacts
                    objCommon.sb_FillComboFromDBwithSel(radAssociationType, 43, Session("DomainID"))  'Fill drop down of Associations
                    'objCommon.sb_FillComboFromDBwithSel(ddlType1, 43, Session("DomainID"))  'Fill drop down of Associations
                    BindGridOfAssociationTo()                                   'Bind the DataGrid with existing associations
                End If
                btnDelete.Attributes.Add("onclick", "return DeleteRecord();")               'Attach client side event to the button to close the window using Javascript
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        '''' -----------------------------------------------------------------------------
        '''' <summary>
        ''''     This function is called to bind existing associations to the Data Grid
        '''' </summary>
        '''' <remarks>
        '''' </remarks>
        '''' <history>
        '''' 	[Debasish Tapan Nag]	12/29/2005	Modified
        '''' </history>
        ''''-----------------------------------------------------------------------------
        Public Sub BindGridOfAssociationTo()
            Try
                uwTree.Nodes.Clear()
                Dim objProspects As New CProspects                          'Create a object of Prospects
                objProspects.DivisionID = lngAssFromDivID                   'Set the DivisionId Property
                objProspects.bitAssociatedTo = 1                            'Get Organizations Associated to the current Organization
                Dim dsAssocations As DataSet
                Dim dtAssocations As DataTable
                dsAssocations = objProspects.GetAssociationTo()             'Declare a DataTable
                dtAssocations = dsAssocations.Tables(0)

                Dim Parent As New RadTreeNode
                If dsAssocations.Tables(1).Rows.Count > 0 Then
                    Dim drParent As DataRow = dsAssocations.Tables(1).Rows(0)
                    Parent.Text = "<font color=#180073><b>" + drParent("vcCompanyName").ToString + "</b></font>" + IIf(String.IsNullOrEmpty(drParent("ShipTo")), " ", "<font color=red>(" + drParent("ShipTo").ToString + ")</font>")
                    Parent.Value = drParent("numDivisionID").ToString + "~0"
                    Parent.Expanded = True
                End If
                If dtAssocations.Rows.Count > 0 Then
                    Dim treeNodeA As RadTreeNode
                    For Each dr As DataRow In dtAssocations.Rows
                        treeNodeA = New RadTreeNode
                        treeNodeA.Text = "<font color=#180073><b>" + dr("TargetCompany").ToString + "</b></font>" + IIf(String.IsNullOrEmpty(dr("ShipTo")), " ", " <font color=red>(" + dr("ShipTo").ToString + ")</font> ") + dr("vcData").ToString + " " + dr("bitParentOrg").ToString + IIf(CCommon.ToBool(dr("Allowedit")), "  <img src='../images/edit.png' onclick='editAssociation(event," + dr("numAssociationId").ToString + ")'/>", "")
                        treeNodeA.NavigateUrl = GetTargetURL(dr("numDivisionID"), dr("tintCRMType"))
                        treeNodeA.Target = "rightFrame"
                        treeNodeA.Value = dr("numDivisionID").ToString + "~" + dr("numAssociationID").ToString
                        treeNodeA.Expanded = False
                        'treeNodeA.Nodes.Add(New RadTreeNode())
                        Parent.Nodes.Add(treeNodeA)
                    Next
                End If
                If Not Parent Is Nothing Then
                    uwTree.Nodes.Add(Parent)
                End If

                uwTree.ExpandAllNodes()
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        '''' -----------------------------------------------------------------------------
        '''' <summary>
        ''''     Trigerred when the Add button is clicked. This function adds a new assocaiton to the list of existing assocation
        '''' </summary>
        '''' <param name="sender">Represents the sender object.</param>
        '''' <param name="e">Represents the EventArgs.</param>
        '''' <remarks>
        '''' </remarks>
        '''' <history>
        '''' 	[Debasish Tapan Nag]	12/30/2005	Modified
        '''' </history>
        '''' -----------------------------------------------------------------------------
        Private Sub imgAddAssociation_Click(sender As Object, e As ImageClickEventArgs) Handles imgAddAssociation.Click
            Try
                Dim objProspects As New CProspects                          'Create a new object of prospects
                Dim vcDisplayMessage As String = ""                         'Declare a String Variable which will store the message if any
                Dim vcReturnFlag As String                                  'Declare an String variable

                If radCmbCompany.SelectedValue = lngAssFromDivID Then
                    litMessage.Text = "<script>alert('Can not associate to company which you are in.');</script>"
                    Exit Sub
                End If
                With objProspects
                    .AssociationID = 0                                      'New Assocation Id
                    '.CompanyID = radCmbCompany.SelectedValue              'Set the Org Id into the class property
                    .DivisionID = radCmbCompany.SelectedValue            'Specity the Division Id
                    '.ContactId = ddlContacts.SelectedValue
                    .AssociationType = radAssociationType.SelectedValue           'The Association Type
                    '.AssociationTypeForLabel = IIf(chkAssociationLabel.Checked, ddlType1.SelectedValue, 0)
                    .bitDeleted = 0                                         'Deleted flag is 0 since we are adding an assocoation
                    .UserCntID = Session("UserContactID")                             'Set the User Id into the class property
                    .DomainID = Session("DomainID")                         'Set the Domain ID
                    '.BoolParentOrg = cbParentOrgIndicator.Checked           'Indicator if the current org is the parent org or not
                    '.BoolChildOrg = cbChildOrgIndicator.Checked             'Indicator if the current org is the child org or not
                    '.bitSharePortal = chkShare.Checked
                    .AssociationFrom = lngAssFromDivID                         'Set the Division Id
                    '.HeaderLink = IIf(chkHeaderLink.Checked, 1, 0)
                    .BoolChildOrg = chkChild.Checked

                    vcReturnFlag = .ManageAssociation()                 'Add the new assocition to the list of existing assocations

                    If vcReturnFlag = "Duplicate" Then
                        litMessage.Text = "<script>alert('Already exists association with organization.');</script>"
                    ElseIf vcReturnFlag = "DuplicateChild" Then
                        litMessage.Text = "<script>alert('Selected organization is already child of other organization.');</script>"
                    Else
                        radCmbCompany.ClearSelection()
                        radAssociationType.ClearSelection()
                        chkChild.Checked = False
                        hdnAssID.Value = ""
                    End If
                End With
                BindGridOfAssociationTo()                                   'Refresh hte list of associations
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        '''' -----------------------------------------------------------------------------
        '''' <summary>
        ''''     Triggered as a result of an event in the Assocation DataGrid. Mainly called when Delete button is clicked here
        '''' </summary>
        '''' <param name="sender">Represents the sender object.</param>
        '''' <param name="e">Represents the EventArgs.</param>
        '''' <remarks>
        '''' </remarks>
        '''' <history>
        '''' 	[Debasish Tapan Nag]	12/30/2005	Modified
        '''' </history>
        '''' -----------------------------------------------------------------------------
        'Private Sub dgAssociation_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgAssociation.ItemCommand
        '    Try
        '        If e.CommandName = "Delete" Then                                        'The following section is for deleting a association
        '            Dim objProspects As New CProspects                                  'Create a new object of Prospects
        '            objProspects.AssociationID = e.Item.Cells(0).Text                   'Set the id of the assocaiton from the datagrid
        '            objProspects.UserCntID = Session("UserContactID")                             'Set the user Id
        '            If objProspects.DeleteAssociation = False Then                      'Error occurred during setting the assocation active status as deleted
        '                litMessage.Text = "<script language=javascript>alert('Dependent Records Exists. Cannot Be deleted.');</script>" 'Display message that the association cannot be deleted
        '            Else : BindGridOfAssociationTo()                                       'Delete successful so refresh the Associations in the DataGrid
        '            End If
        '        End If
        '    Catch ex As Exception
        '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
        '        Response.Write(ex)
        '    End Try
        'End Sub


        'Private Sub radCmbCompany_SelectedIndexChanged(ByVal o As Object, ByVal e As Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs) Handles radCmbCompany.SelectedIndexChanged
        '    Try
        '        If radCmbCompany.SelectedValue <> "" Then
        '            FillContact(ddlContacts)
        '        End If
        '    Catch ex As Exception
        '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
        '        Response.Write(ex)
        '    End Try
        'End Sub
        'Public Function FillContact(ByVal ddlCombo As DropDownList)
        '    Try
        '        Dim fillCombo As New COpportunities
        '        With fillCombo
        '            .DivisionID = radCmbCompany.SelectedValue
        '            ddlCombo.DataSource = fillCombo.ListContact().Tables(0).DefaultView()
        '            ddlCombo.DataTextField = "Name"
        '            ddlCombo.DataValueField = "numcontactId"
        '            ddlCombo.DataBind()
        '        End With
        '        ddlCombo.Items.Insert(0, New ListItem("---Select One---", "0"))
        '        If ddlCombo.Items.Count = 2 Then
        '            ddlCombo.Items(1).Selected = True
        '        End If
        '    Catch ex As Exception
        '        Throw ex
        '    End Try
        'End Function
        Private Sub uwTree_NodeExpand(sender As Object, e As Telerik.Web.UI.RadTreeNodeEventArgs) Handles uwTree.NodeExpand
            Try
                If Not e.Node.Level = 0 Then
                    e.Node.Nodes.RemoveAt(0)
                    Dim objProspects As New CProspects                          'Create a object of Prospects
                    objProspects.DivisionID = e.Node.Value.ToString.Split("~")(0)                   'Set the DivisionId Property
                    objProspects.bitAssociatedTo = 1                            'Get Organizations Associated to the current Organization
                    Dim dtAssocations As DataTable = objProspects.GetAssociationTo().Tables(0) 'Declare a DataTable


                    If dtAssocations.Rows.Count > 0 Then
                        Dim treeNodeA As RadTreeNode
                        For Each dr As DataRow In dtAssocations.Rows
                            treeNodeA = New RadTreeNode
                            treeNodeA.Text = "<font color=#180073><b>" + dr("TargetCompany").ToString + "</b></font> (" + dr("vcData") + ") <font color=red><i>" + dr("Relationship").ToString + "/" + dr("OrgProfile").ToString + ":</i></font>" + dr("ContactName").ToString + ", " + dr("numPhone").ToString + ", <span onclick=OpenEmail('" & dr("vcEmail").ToString & "','" & dr("numContactID").ToString() & "')><a href=#>" + dr("vcEmail").ToString + "</a></span>"
                            treeNodeA.NavigateUrl = GetTargetURL(dr("numDivisionID"), dr("tintCRMType"))
                            treeNodeA.Target = "rightFrame"
                            treeNodeA.Value = dr("numDivisionID").ToString + "~" + dr("numAssociationID").ToString
                            treeNodeA.Expanded = False
                            treeNodeA.Nodes.Add(New RadTreeNode())
                            e.Node.Nodes.Add(treeNodeA)
                        Next
                    End If
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub
        Private Function GetTargetURL(ByVal lngDivID As Long, ByVal CRMType As Short) As String
            Select Case CRMType
                Case 0
                    Return "../Leads/frmLeads.aspx?frm=LeadsList&DivID=" + lngDivID.ToString
                Case 1
                    Return "../prospects/frmProspects.aspx?frm=LeadsList&DivID=" + lngDivID.ToString
                Case 2
                    Return "../account/frmAccounts.aspx?frm=LeadsList&DivId=" + lngDivID.ToString
            End Select
        End Function

        Private Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click
            Try
                Dim objProspects As New CProspects
                'FindChecked(uwTree.Nodes)
                'If Not checkedNode Is Nothing Then
                '    If checkedNode.Level = 1 Then
                '        hdnAssID.Value = checkedNode.Value.ToString.Split("~")(1)
                '    Else
                '        litMessage.Text = "<script>alert('You are only allowed to delete associations on the 1st level of the tree');</script>"
                '        Exit Sub
                '    End If
                'End If
                If CCommon.ToLong(hdnAssID.Value) > 0 Then
                    objProspects.AssociationID = hdnAssID.Value
                    objProspects.UserCntID = Session("UserContactID")
                    objProspects.DeleteAssociation()

                    radCmbCompany.ClearSelection()
                    radAssociationType.ClearSelection()
                    chkChild.Checked = False

                    BindGridOfAssociationTo()
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub
        'Dim flag As Boolean = False
        'Dim checkedNode As RadTreeNode
        'Private Function FindChecked(ByVal nodes As RadTreeNodeCollection) As Long
        '    If flag = True Then Exit Function
        '    For Each node As RadTreeNode In nodes
        '        If node.Checked Then
        '            checkedNode = node
        '            flag = True
        '            Exit Function
        '        Else
        '            If node.Nodes.Count > 0 Then
        '                FindChecked(node.Nodes)
        '            End If
        '        End If
        '    Next
        'End Function

        Private Sub btnEdit_Click(sender As Object, e As EventArgs) Handles btnEdit.Click
            Try
                If CCommon.ToLong(hdnAssID.Value) > 0 Then
                    Dim objProspects As New CProspects                          'Create a object of Prospects
                    objProspects.AssociationID = hdnAssID.Value
                    objProspects.DomainID = Session("DomainID")
                    Dim dsAssocations As DataSet
                    Dim dtAssocations As DataTable
                    dsAssocations = objProspects.GetAssociationTo()             'Declare a DataTable
                    dtAssocations = dsAssocations.Tables(0)

                    If dtAssocations.Rows.Count > 0 Then
                        radCmbCompany.Text = dtAssocations(0)("vcCompanyName")
                        radCmbCompany.SelectedValue = dtAssocations(0)("numDivisionId")

                        If Not radAssociationType.Items.FindItemByValue(dtAssocations(0)("numReferralType")) Is Nothing Then
                            radAssociationType.ClearSelection()
                            radAssociationType.Items.FindItemByValue(dtAssocations(0)("numReferralType")).Selected = True
                        End If

                        chkChild.Checked = CCommon.ToBool(dtAssocations(0)("bitChildOrg"))
                    End If
                End If

            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub
    End Class
End Namespace