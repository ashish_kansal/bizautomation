﻿Imports BACRMBUSSLOGIC.BussinessLogic
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Admin
Imports System.IO
Imports Telerik.Web.UI

Public Class frmImportList
    Inherits BACRMPage

#Region "OBJECT DECLARATION"

    Dim objImport As ImportWizard

#End Region

#Region "GLOBAL VARIABLES"

    Dim dsImport As DataSet

#End Region

#Region "PAGE EVENTS"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ""
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "none")
            DomainID = Session("DomainID")
            UserCntID = Session("UserContactID")

            GetUserRightsForPage(MODULEID.Administration, PAGEID.IMPORT_WIZARD)

            If Not IsPostBack Then
                BindGrid()
            End If

        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try

    End Sub
    Private Sub DisplayError(ByVal ex As String)
        DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ex
        DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
    End Sub
#End Region

#Region "GRIDVIEW EVENTS "

    Private Sub gvImports_PageIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvImports.PageIndexChanged
        Try

        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Private Sub gvImports_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvImports.PageIndexChanging
        Try
            gvImports.PageIndex = e.NewPageIndex
            BindGrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Private Sub gvImports_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvImports.RowCommand
        Try
            If e.CommandName.ToLower() = "RollBack".ToLower() Then
                Dim intResult As Integer = 0
                objImport = New ImportWizard()

                With objImport
                    .ImportFileID = CLng(e.CommandArgument)
                    .DomainId = DomainID
                    .UserContactID = UserCntID
                    .Status = enmImportDataStatus.Import_Rollback_Pending
                    .Mode = 2
                    intResult = .SaveImportMappedData()
                End With

                BindGrid()

            ElseIf e.CommandName.ToLower() = "X".ToLower() Then

                Dim intRecords As Integer

                objImport = New ImportWizard(e.CommandArgument, DomainID)
                objImport.UserContactID = UserCntID
                intRecords = objImport.DeleteMappedFileData()

                If intRecords > 0 Then
                    If File.Exists(CCommon.GetDocumentPhysicalPath(objImport.DomainId) & objImport.ImportFileName) Then
                        File.Delete(CCommon.GetDocumentPhysicalPath(objImport.DomainId) & objImport.ImportFileName)
                    End If
                End If
                BindGrid()

            ElseIf e.CommandName.ToLower() = "OpenRecords".ToLower() Then

                Dim MasterID As Integer = e.CommandArgument

                Dim row As GridViewRow = DirectCast(DirectCast(e.CommandSource, LinkButton).NamingContainer, GridViewRow)
                Dim lblItemType As HiddenField = DirectCast(row.Cells(5).FindControl("hdnItemCodes"), HiddenField)
                'Dim strCondition As String = ""

                If CCommon.ToLong(lblItemType.Value) > 0 Then

                    'strCondition = lblItemType.Value.ToString
                    'If strCondition.Length > 4000 Then
                    '    strCondition = Mid(strCondition, 1, strCondition.LastIndexOf(",", 3000))
                    'End If

                    If MasterID = 20 Then 'Item
                        'Session("WhereConditionItem") = " AND I.numItemCode IN (SELECT Items FROM Split('" & lblItemType.Value.Trim & "',','))"
                        Session("WhereConditionItem") = " AND I.numItemCode IN (SELECT OutParam FROM SplitString((SELECT vcHistory_Added_Value FROM Import_History WHERE intImportFileID = " & CCommon.ToLong(lblItemType.Value) & "),','))"

                        Session("SavedSearchCondition") = Session("WhereConditionItem")

                        Response.Redirect("../Admin/frmAdvSerInvItemsRes.aspx")
                    ElseIf MasterID = 133 Then 'Organization
                        Session("WhereCondition") = " AND DM.numDivisionID IN (SELECT OutParam FROM SplitString((SELECT vcHistory_Added_Value FROM Import_History WHERE intImportFileID = " & CCommon.ToLong(lblItemType.Value) & "),','))"
                        Session("SavedSearchCondition") = Session("WhereCondition")

                        Response.Redirect("../Admin/frmAdvancedSearchRes.aspx?frm=Import")
                    End If
                End If

            End If

        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Private Sub gvImports_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvImports.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim btnDelete As LinkButton = DirectCast(e.Row.FindControl("btnDelete"), LinkButton)
                Dim btnRollback As LinkButton = DirectCast(e.Row.FindControl("btnRollback"), LinkButton)
                If CBool(DataBinder.Eval(e.Row.DataItem, "IsForRollback")) = True AndAlso CBool(DataBinder.Eval(e.Row.DataItem, "intStatus")) = True Then
                    btnRollback.Visible = True
                ElseIf CBool(DataBinder.Eval(e.Row.DataItem, "intStatus")) = False AndAlso CBool(DataBinder.Eval(e.Row.DataItem, "IsForRollback")) = False Then
                    btnRollback.Visible = False
                    btnDelete.Visible = True
                End If

                If CCommon.ToShort(DataBinder.Eval(e.Row.DataItem, "intStatus")) = 1 Or CCommon.ToShort(DataBinder.Eval(e.Row.DataItem, "intStatus")) = 3 Then
                    If CCommon.ToLong(DataBinder.Eval(e.Row.DataItem, "numRecordAdded")) > 0 Or CCommon.ToLong(DataBinder.Eval(e.Row.DataItem, "numRecordUpdated")) > 0 Then
                        btnDelete.Visible = False
                    Else
                        btnDelete.Visible = True
                    End If
                End If
                
                If CCommon.ToInteger(DataBinder.Eval(e.Row.DataItem, "MasterID")) = IMPORT_TYPE.Item_WareHouse Then
                    btnRollback.Visible = False
                End If

                Dim lnkLog As HyperLink
                lnkLog = DirectCast(e.Row.FindControl("lnkLog"), HyperLink)
                If lnkLog IsNot Nothing Then
                    Dim strLink As String
                    strLink = CCommon.GetDocumentPath(DomainID) & "Import_Log_" & DataBinder.Eval(e.Row.DataItem, "ImportFileID") & ".txt"
                    'lnkLog.Attributes.Add("onclick", "OpenLog(" & DomainID & "," & DataBinder.Eval(e.Row.DataItem, "ImportFileID") & ");")
                    lnkLog.Attributes.Add("onclick", "OpenLog('" & strLink & "')")


                End If

                Dim imgDownload As ImageButton
                imgDownload = DirectCast(e.Row.FindControl("imgDownload"), ImageButton)
                If imgDownload IsNot Nothing Then
                    Dim strLink As String
                    strLink = CCommon.GetDocumentPath(DomainID) & DataBinder.Eval(e.Row.DataItem, "ImportFileName")
                    imgDownload.Attributes.Add("onclick", "OpenLog('" & strLink & "')")
                End If
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Private Sub gvImports_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvImports.RowDeleting
        Try
            'BindGrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

#End Region

#Region "BIND GRIDVIEW SUBROUTINE"

    Private Sub BindGrid()
        Try
            objImport = New ImportWizard
            dsImport = New DataSet

            objImport.ImportFileID = 0
            objImport.DomainId = DomainID
            dsImport = objImport.LoadMappedData()
            If dsImport IsNot Nothing AndAlso dsImport.Tables.Count > 1 Then
                If txtCurrrentPage.Text.Trim = "" Then txtCurrrentPage.Text = 1

                'If Not IsPostBack Then

                bizPager.PageSize = Session("PagingRows")
                gvImports.PageSize = bizPager.PageSize
                bizPager.RecordCount = dsImport.Tables(0).Rows.Count
                bizPager.CurrentPageIndex = txtCurrrentPage.Text
                'End If

                Dim intPageIndex As Integer = IIf(txtCurrrentPage.Text.Trim = "" OrElse txtCurrrentPage.Text.Trim Is Nothing, 0, txtCurrrentPage.Text.Trim)
                gvImports.PageIndex = IIf(intPageIndex = 0, 0, intPageIndex - 1)
                bizPager.CurrentPageIndex = IIf(txtCurrrentPage.Text.Trim = "" OrElse txtCurrrentPage.Text.Trim Is Nothing, 0, txtCurrrentPage.Text.Trim)

                Dim dvSort As DataView = dsImport.Tables(0).DefaultView

                If txtSortChar.Text <> "" AndAlso txtSortChar.Text <> "0" Then
                    dvSort.RowFilter = "ImportFileName ilike '%" & txtSortChar.Text & "%'"
                ElseIf txtSortChar.Text = "0" Then
                    dvSort.RowFilter = ""
                End If

                dvSort.Sort = If(txtSortColumn.Text = "", "ImportFileID DESC", txtSortColumn.Text & " " & If(txtSortOrder.Text = "D", "Desc", "Asc"))

                gvImports.DataSource = dvSort
                gvImports.DataBind()
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

#End Region

    Private Sub btnGo1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGo1.Click
        BindGrid()
    End Sub

    Private Sub gvImports_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles gvImports.Sorting

        Dim strColumn As String = e.SortExpression
        If txtSortColumn.Text <> strColumn Then
            txtSortColumn.Text = strColumn
            txtSortOrder.Text = "A"
        Else
            If txtSortOrder.Text = "D" Then
                txtSortOrder.Text = "A"
            Else : txtSortOrder.Text = "D"
            End If
        End If

        ' BindGrid(strColumn, txtSortChar.Text, txtSortColumn.Text)
    End Sub
    Protected Sub bizPager_PageChanged(sender As Object, e As EventArgs)
        Try
            txtCurrrentPage.Text = bizPager.CurrentPageIndex
            BindGrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub
End Class