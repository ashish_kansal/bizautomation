Imports BACRM.BusinessLogic.Item
Imports BACRM.BusinessLogic.Opportunities
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Admin
Namespace BACRM.UserInterface.Admin
    Partial Public Class frmAdvCases
        Inherits BACRMPage

        Dim objGenericAdvSearch As New FormGenericAdvSearch
        Dim dtGenericFormConfig As DataTable
        Public dsGenericFormConfig As DataSet
        Dim objCommon As CCommon

        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            Try
                'CLEAR ERROR ON RELOAD
                DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ""
                DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "none")

                objCommon = New CCommon
                GetUserRightsForPage(9, 8)

                If Not IsPostBack Then
                    calFrom.SelectedDate = DateAdd(DateInterval.Month, -3, Now)
                    calTo.SelectedDate = Now
                End If

                GetFormFieldList()
                dtGenericFormConfig = dsGenericFormConfig.Tables(1)

                GenericFormControlsGeneration.FormID = 17                                        'set the form id to Advance Search
                GenericFormControlsGeneration.DomainID = Session("DomainID")                    'Set the domain id
                GenericFormControlsGeneration.UserCntID = Session("UserContactID")                        'Set the User id
                GenericFormControlsGeneration.AuthGroupId = Session("UserGroupID")              'Set the User Authentication Group Id

                callFuncForFormGenerationNonAOI(dsGenericFormConfig)                        'Calls function for form generation and display for non AOI fields
                litMessage.Text = GenericFormControlsGeneration.getJavascriptArray(dsGenericFormConfig.Tables(1)) 'Create teh javascript array and store
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Sub GetFormFieldList()
            Try
                If objGenericAdvSearch Is Nothing Then objGenericAdvSearch = New FormGenericAdvSearch

                objGenericAdvSearch.AuthenticationGroupID = Session("UserGroupID")
                objGenericAdvSearch.FormID = 17
                objGenericAdvSearch.DomainID = Session("DomainID")
                dsGenericFormConfig = objGenericAdvSearch.GetAdvancedSearchFieldList()

                dsGenericFormConfig.Tables(0).TableName = "SectionInfo"                           'give a name to the datatable
                dsGenericFormConfig.Tables(1).TableName = "AdvSearchFields"                           'give a name to the datatable

                dsGenericFormConfig.Tables(1).Columns.Add("vcNewFormFieldName", System.Type.GetType("System.String"), "[vcFormFieldName]") 'This columsn has to be an integer to tryign to manipupate the datatype as Compute(Max) does not work on string datatypes
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Sub callFuncForFormGenerationNonAOI(ByVal dsGenericFormConfig As DataSet)
            Try
                GenericFormControlsGeneration.boolAOIField = 0                                      'Set the AOI flag to non AOI
                GenericFormControlsGeneration.createDynamicFormControlsSectionWise(dsGenericFormConfig, plhControls, 17, Session("DomainID"), Session("UserContactID"))
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Private Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
            Try
                Dim str As String = ""
                If calFrom.SelectedDate <> "" And calTo.SelectedDate <> "" And radCreated.Checked = True Then
                    str = str & " and Cases.bintCreatedDate between  '" & calFrom.SelectedDate & "'::TIMESTAMP and '" & calTo.SelectedDate & "'::TIMESTAMP"
                End If

                If calFrom.SelectedDate <> "" And calTo.SelectedDate <> "" And radDue.Checked = True Then
                    str = str & " and Cases.intTargetResolveDate between  '" & calFrom.SelectedDate & "'::TIMESTAMP and '" & calTo.SelectedDate & "'::TIMESTAMP"
                End If

                Dim strControlID As String
                Dim dr As DataRow
                Dim v_Prefix As String

                For i As Integer = 0 To dtGenericFormConfig.Rows.Count - 1
                    dr = dtGenericFormConfig.Rows(i)
                    strControlID = dr("numFormFieldID").ToString & "_" & dr("vcDbColumnName").ToString.Trim.Replace(" ", "_")

                    v_Prefix = "Cases."

                    If CCommon.ToString(dr("vcLookBackTableName")) = "AdditionalContactsInformation" Then
                        v_Prefix = "ADC."
                    ElseIf CCommon.ToString(dr("vcLookBackTableName")) = "DivisionMaster" Then
                        v_Prefix = "DM."
                    ElseIf CCommon.ToString(dr("vcLookBackTableName")) = "CompanyInfo" Then
                        v_Prefix = "C."
                    ElseIf CCommon.ToString(dr("vcLookBackTableName")) = "Cases" Then
                        v_Prefix = "Cases."
                    End If

                    If dtGenericFormConfig.Rows(i).Item("vcAssociatedControlType") = "SelectBox" Or dtGenericFormConfig.Rows(i).Item("vcAssociatedControlType").ToString = "CheckBox" Then
                        If dtGenericFormConfig.Rows(i).Item("vcFieldType") = "R" Then
                            If CType(tblMain.FindControl(strControlID), DropDownList).SelectedIndex > 0 Then
                                str = str & " and " & v_Prefix & dtGenericFormConfig.Rows(i).Item("vcDbColumnName") & " = " & CType(tblMain.FindControl(strControlID), DropDownList).SelectedValue
                            End If
                        ElseIf dtGenericFormConfig.Rows(i).Item("vcFieldType") = "C" Then
                            If CType(tblMain.FindControl(strControlID), DropDownList).SelectedIndex > 0 Then
                                str = str & " and " & v_Prefix & "numCaseId in (select RecId from CFW_FLD_Values_Case where Fld_ID =" & Replace(Replace(dtGenericFormConfig.Rows(i).Item("numFormFieldID"), "C", ""), "D", "") & " and LOWER(Fld_Value) = LOWER('" & CType(tblMain.FindControl(strControlID), DropDownList).SelectedValue & "') )"
                            End If
                        End If
                    ElseIf dtGenericFormConfig.Rows(i).Item("vcAssociatedControlType").ToString.Replace(" ", "") = "DateField" Then

                    ElseIf dtGenericFormConfig.Rows(i).Item("vcAssociatedControlType") = "CheckBoxList" Then
                        Dim strFieldValue As String = ""
                        Dim chkbl As CheckBoxList = CType(tblMain.FindControl(strControlID), CheckBoxList)

                        For Each item As ListItem In chkbl.Items
                            If item.Selected Then
                                strFieldValue = strFieldValue & If(strFieldValue.Length > 0, " OR ", "") & " CONCAT(',',Fld_Value,',') ILIKE '%," & item.Value & ",%'"
                            End If
                        Next

                        If strFieldValue.Length > 0 Then
                            If dtGenericFormConfig.Rows(i).Item("vcFieldType") = "C" Then
                                str = str & " and " & v_Prefix & "numCaseId in (select RecId from CFW_FLD_Values_Case where Fld_ID =" & Replace(Replace(dtGenericFormConfig.Rows(i).Item("numFormFieldID"), "C", ""), "D", "") & " AND (" & strFieldValue & "))"
                            End If
                        End If
                    Else
                        If CType(tblMain.FindControl(strControlID), TextBox).Text <> "" Then
                            If dtGenericFormConfig.Rows(i).Item("vcFieldType") = "R" Then
                                str = str & " and " & v_Prefix & dtGenericFormConfig.Rows(i).Item("vcDbColumnName") & " ILIKE  '%" & Replace(CType(tblMain.FindControl(strControlID), TextBox).Text, "'", "''") & "%' "
                            ElseIf dtGenericFormConfig.Rows(i).Item("vcFieldType") = "C" Then
                                str = str & " and " & v_Prefix & "numCaseId in (select RecId from CFW_FLD_Values_Case where Fld_ID =" & Replace(Replace(dtGenericFormConfig.Rows(i).Item("numFormFieldID"), "C", ""), "D", "") & " and Fld_Value ILIKE '%" & Replace(CType(tblMain.FindControl(strControlID), TextBox).Text, "'", "''") & "%' )"
                            End If
                        End If
                    End If
                Next
                Session("WhereContditionCase") = str
                Response.Redirect("../Admin/frmAdvCaseRes.aspx")
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Private Sub DisplayError(ByVal exception As String)
            Try
                DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = exception
                DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
                DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Focus()
            Catch ex As Exception

            End Try
        End Sub
    End Class
End Namespace