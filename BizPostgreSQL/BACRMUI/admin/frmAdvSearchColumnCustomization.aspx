<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" CodeBehind="frmAdvSearchColumnCustomization.aspx.vb"
    Inherits="BACRM.UserInterface.Admin.frmAdvSearchColumnCustomization" ValidateRequest="false"
    MasterPageFile="~/common/Popup.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Advance Search Column Customization</title>
    <script language="javascript" src="../javascript/AdvSearchScripts.js"></script>
    <script language="javascript" type="text/javascript">
        function Save() {
            var str = '';
            for (var i = 0; i < document.getElementById("lstSelectedfld").options.length; i++) {
                var SelectedValue;
                SelectedValue = document.getElementById("lstSelectedfld").options[i].value;
                str = str + (str == "" ? SelectedValue : ',' + SelectedValue);
            }

            if ($("[id$=hdnIsFromNewAdvancedSearch]").val() == "1") {
               
                window.opener.GetSelectedColumns(str);
                window.close();

                return false;
            } else {
                document.getElementById("hdnCol").value = str;
                return true;
            }
        }


        function CloseAndReturnSelectedColumns() {
            var str = '';

            for (var i = 0; i < document.getElementById("lstSelectedfld").options.length; i++) {
                var SelectedValue;
                SelectedValue = document.getElementById("lstSelectedfld").options[i].value;
                str = str + SelectedValue + ','
            }

            window.opener.GetSelectedColumns(str.trim(","));
            window.close();
        }

        function CloseWindow() {
            if ($().val() == "1") {
            } else {
                javascript: window.opener.location.reload(true);
                window.close();
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <div class="input-part">
        <div class="right-input">
        <asp:Button ID="btnSave" Text="Save" runat="server" CssClass="button" Width="50" OnClientClick="return Save();"></asp:Button>
            <input class="button" id="btnClose" style="width: 50px" onclick="CloseWindow()"
                type="button" value="Close" />&nbsp;&nbsp;
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageTitle" runat="server" ClientIDMode="Static">
    Edit Search Result Layout
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="Content" runat="server" ClientIDMode="Static">
    <asp:Table ID="tblAdvSearchFieldCustomization" runat="server" Width="470" GridLines="None"
        CssClass="aspTable" BorderColor="black" BorderWidth="1">
        <asp:TableRow>
            <asp:TableCell>
                <table cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td align="center" class="normal1" valign="top">
                            Available Fields<br>
                            &nbsp;&nbsp;
                            <asp:ListBox ID="lstAvailablefld" runat="server" Width="150" Height="200" CssClass="signup"
                                EnableViewState="False"></asp:ListBox>
                        </td>
                        <td align="center" class="normal1" valign="middle">
                            <input type="button" id="btnAdd" class="button" value="Add >" onclick="javascript: move(lstAvailablefld,lstSelectedfld);">
                            <br>
                            <br>
                            <input type="button" id="btnRemove" class="button" value="< Remove" onclick="javascript:remove1(lstSelectedfld,lstAvailablefld);">
                        </td>
                        <td align="center" class="normal1">
                            Selected Fields/ Choose Order<br>
                            <asp:ListBox ID="lstSelectedfld" runat="server" Width="150" Height="200" CssClass="signup"
                                EnableViewState="False"></asp:ListBox>
                        </td>
                        <td align="center" class="normal1" valign="middle">
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <img id="btnMoveupOne" src="../images/upArrow.gif" onclick="javascript:MoveUp(document.getElementById('lstSelectedfld'));" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <br>
                            <br>
                            <img id="btnMoveDownOne" src="../images/downArrow1.gif" onclick="javascript:MoveDown(document.getElementById('lstSelectedfld'));" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            (Max 15)
                        </td>
                    </tr>
                </table>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
    <input id="hdnCol" type="hidden" name="hdXMLString" runat="server" value="" />
    <input id="hdSave" type="hidden" name="hdSave" runat="server" value="False" />
    <asp:HiddenField runat="server" ID="hdnIsFromNewAdvancedSearch" />
</asp:Content>
