﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="frmAdvSearchCriteria.ascx.vb"
    Inherits="BACRM.UserInterface.Admin.frmAdvSearchCriteria" ClientIDMode="Static" %>
<%@ Register TagPrefix="BizCalendar" TagName="Calendar" Src="~/include/calandar.ascx" %>
<link rel="stylesheet" type="text/css" href="../CSS/jscal2.css" />
<link rel="stylesheet" type="text/css" href="../CSS/border-radius.css" />
<script type="text/javascript" src="../JavaScript/jscal2.js"></script>
<script type="text/javascript" src="../JavaScript/en.js"></script>
<style type="text/css">
    .SearchOperator {
        color: Green;
        cursor: pointer;
        padding-left: 5px;
    }

    .ListBox {
        font-family: Arial;
        font-size: 8pt;
        color: Black;
    }
</style>
<script type="text/javascript">
    var FieldName = '';
    var TargetElement;
    var FieldDataType;
    function SetFilterCriteria(a, FieldDataType1, event, element) {
        FieldName = a;

        $("[id$=dvFilterCriteria]").modal('show');


        TargetElement = element;
        FieldDataType = FieldDataType1;
        if (FieldDataType.toUpperCase() == "V") {//varchar
            document.getElementById('dvVarcharFilters').style.display = '';
            document.getElementById('dvNumericFilters').style.display = 'none';
            document.getElementById('dvDateFilters').style.display = 'none';
            document.getElementById('dvDropdownFilter').style.display = 'none';
            document.getElementById('rbContains').checked = true;
        }
        else if (FieldDataType.toUpperCase() == "N" || FieldDataType.toUpperCase() == "M") { //numeric
            document.getElementById('dvVarcharFilters').style.display = 'none';
            document.getElementById('dvNumericFilters').style.display = '';
            document.getElementById('dvDateFilters').style.display = 'none';
            document.getElementById('dvDropdownFilter').style.display = 'none';
            document.getElementById('rbEquals').checked = true;
        }
        else if (FieldDataType.toUpperCase() == "D") {

            document.getElementById('dvVarcharFilters').style.display = 'none';
            document.getElementById('dvNumericFilters').style.display = 'none';
            document.getElementById('dvDateFilters').style.display = '';
            document.getElementById('dvDropdownFilter').style.display = 'none';
            document.getElementById('rbEqualToDate').checked = true;

        }
        else if (FieldDataType.toUpperCase() == "S") {//SelectBox

            document.getElementById('dvVarcharFilters').style.display = 'none';
            document.getElementById('dvNumericFilters').style.display = 'none';
            document.getElementById('dvDateFilters').style.display = 'none';
            document.getElementById('dvDropdownFilter').style.display = '';
            document.getElementById('rbEqualToDD').checked = true;
        }
        if (document.getElementById("hdn_" + FieldName) != null) {
            var radioButtons = $("[name$=filter]");
            for (var x = 0; x < radioButtons.length; x++) {
                if (document.getElementById("hdn_" + FieldName).value == radioButtons[x].value) {
                    radioButtons[x].checked = true;
                }
            }
        }


    }
    function setFilter() {
        $("[id$=dvFilterCriteria]").modal('hide');


        var radioButtons = $("[name$=filter]");
        var intCriteriaValue;
        for (var x = 0; x < radioButtons.length; x++) {
            if (radioButtons[x].checked) {
                intCriteriaValue = parseInt(radioButtons[x].value);
                if (intCriteriaValue == 10) {//rbBetween
                    //for range based fields
                    document.getElementById(FieldName).value = document.getElementById("txtFrom").value + ' to ' + document.getElementById("txtTo").value;
                    document.getElementById(FieldName).disabled = true;
                    TargetElement.innerHTML = radioButtons[x].nextSibling.innerHTML;
                }
                else if (intCriteriaValue == 11) {//rbDateRange
                    //for range based fields
                    document.getElementById(FieldName + '_txtDate').value = $Search("calFilterFrom_txtDate").value //+ ' to ' + $Search("calFilterTo_txtDate").value;
                    document.getElementById("hdn_" + FieldName + "_value").value = $Search("calFilterFrom_txtDate").value + ' to ' + $Search("calFilterTo_txtDate").value;
                    TargetElement.innerHTML = radioButtons[x].nextSibling.innerHTML;
                    document.getElementById("hdn_" + FieldName).value = intCriteriaValue;
                    FieldName = FieldName + '_txtDate';
                }
                else if (intCriteriaValue == 14) {//rbEqualToDate
                    //for range based fields
                    TargetElement.innerHTML = radioButtons[x].nextSibling.innerHTML;
                    document.getElementById("hdn_" + FieldName).value = intCriteriaValue;
                    FieldName = FieldName + '_txtDate';
                }
                else if (intCriteriaValue == 4)//rbIsNull
                {
                    document.getElementById(FieldName).value = 'null';
                    document.getElementById(FieldName).disabled = true;
                    TargetElement.innerHTML = radioButtons[x].nextSibling.innerHTML;
                }
                else {
                    document.getElementById(FieldName).disabled = false;
                    TargetElement.innerHTML = radioButtons[x].nextSibling.innerHTML;
                }

            }
        }
        document.getElementById(FieldName).focus();
        document.getElementById("hdn_" + FieldName).value = intCriteriaValue;

    }
    function SetUserSearchCriteria() {
        var elements = document.getElementsByTagName("input")
        var length = elements.length;
        var returnElements = [];
        var current;
        var SaveSearchCriteria = document.getElementById('hdnSaveSearchCriteria')
        var arrValues;
        var tempvalue;
        SaveSearchCriteria.value = '';
        for (var i = 0; i < length; i++) {
            current = elements[i];
            tempvalue = ""
            if (current.id.indexOf("hdn_") == 0) {
                if (current.value > 0) {
                    //                    console.log(current.id + current.value)
                    if (current.value == 10) { //Between condition
                        arrValues = document.getElementById(current.id.replace("hdn_", "")).value.split(" to ");
                        if (arrValues.length = 2) {
                            tempvalue = "~" + arrValues[0] + '~' + arrValues[1];
                            document.getElementById(current.id.replace("hdn_", "")).disabled = false;
                        }
                    }
                    if (current.value == 11) { //Between Date condition

                        arrValues = document.getElementById(current.id + "_value").value.split(" to ");
                        if (arrValues.length = 2) {
                            tempvalue = "~" + arrValues[0] + '~' + arrValues[1];
                            document.getElementById(current.id).disabled = false;
                        }
                    }
                    if (current.value == 4) {
                        document.getElementById(current.id.replace("hdn_", "")).disabled = false;
                    }
                    SaveSearchCriteria.value = SaveSearchCriteria.value + current.id.replace("hdn_", "") + "~" + current.value + tempvalue + "^";
                }
            }
        }
        document.getElementById('hdnFilterCriteria').value = SaveSearchCriteria.value;
        //        alert(SaveSearchCriteria.value)
    }

    function CheckNumber(cint, e) {
        var k;
        document.all ? k = e.keyCode : k = e.which;
        if (cint == 1) {
            if (!(k > 47 && k < 58 || k == 44 || k == 46 || k == 8 || k == 37 || k == 39 || k == 16)) {
                if (e.preventDefault) {
                    e.preventDefault();
                }
                else
                    e.returnValue = false;
                return false;
            }
        }
        if (cint == 2) {
            if (!(k > 47 && k < 58 || k == 8 || k == 37 || k == 39 || k == 16)) {
                if (e.preventDefault) {
                    e.preventDefault();
                }
                else
                    e.returnValue = false;
                return false;
            }
        }
    }
    function $Search(name, tag, elm) {
        var tag = tag || "*";
        var elm = elm || document;
        var elements = (tag == "*" && elm.all) ? elm.all : elm.getElementsByTagName(tag);
        var returnElements = [];
        var current;
        var length = elements.length;

        for (var i = 0; i < length; i++) {
            current = elements[i];
            if (current.id.indexOf(name) > 0) {
                returnElements.push(current);
            }
        }
        return returnElements[0];
    };
    // xEvent r11, Copyright 2001-2007 Michael Foster (Cross-Browser.com)
    // Part of X, a Cross-Browser Javascript Library, Distributed under the terms of the GNU LGPL
    function xEvent(evt) // object prototype
    {
        var e = evt || window.event;
        if (!e) return;
        this.type = e.type;
        this.target = e.target || e.srcElement;
        this.relatedTarget = e.relatedTarget;
        if (xDef(e.pageX)) { this.pageX = e.pageX; this.pageY = e.pageY; }
        else if (xDef(e.clientX)) { this.pageX = e.clientX + xScrollLeft(); this.pageY = e.clientY + xScrollTop(); }
        if (xDef(e.offsetX)) { this.offsetX = e.offsetX; this.offsetY = e.offsetY; }
        else if (xDef(e.layerX)) { this.offsetX = e.layerX; this.offsetY = e.layerY; }
        else { this.offsetX = this.pageX - xPageX(this.target); this.offsetY = this.pageY - xPageY(this.target); }
        this.keyCode = e.keyCode || e.which || 0;
        this.shiftKey = e.shiftKey; this.ctrlKey = e.ctrlKey; this.altKey = e.altKey;
        if (typeof e.type == 'string') {
            if (e.type.indexOf('click') != -1) { this.button = 0; }
            else if (e.type.indexOf('mouse') != -1) {
                this.button = e.button;
            }
        }
    }

</script>

<div class="modal" id="dvFilterCriteria">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Change Filter</h4>
            </div>
            <div class="modal-body">
                <div id="dvVarcharFilters">
                    <div class="row">
                        <div class="col-xs-12">
                            <asp:RadioButton CssClass="signup" Text="Contains " runat="server" ID="rbContains" value="1" GroupName="filter" Checked="true" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <asp:RadioButton CssClass="signup" Text="Starts With " runat="server" ID="rbStartsWith" value="2" GroupName="filter" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <asp:RadioButton CssClass="signup" Text="Ends With " runat="server" ID="rbEndsWith" value="3" GroupName="filter" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <asp:RadioButton CssClass="signup" Text="Contains no value " runat="server" ID="rbIsNull" value="4" GroupName="filter" />(null)
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <asp:RadioButton CssClass="signup" Text="Or " runat="server" ID="rbOrfilter" GroupName="filter" value="5" />
                            (e.g. Bob, Paul will find Bob or Paul)
                        </div>
                    </div>
                </div>
                <div id="dvNumericFilters">
                    <table cellspacing="0" cellpadding="2" width="100%" bgcolor="white" border="0">
                        <tr>
                            <td class="Normal1">
                                <asp:RadioButton CssClass="signup" Text="Equal To " runat="server" ID="rbEquals"
                                    value="6" GroupName="filter" />
                            </td>
                        </tr>
                        <tr>
                            <td class="Normal1">
                                <asp:RadioButton CssClass="signup" Text="Not Equal To " runat="server" ID="rbNotEquals"
                                    value="7" GroupName="filter" />
                            </td>
                        </tr>
                        <tr>
                            <td class="Normal1">
                                <asp:RadioButton CssClass="signup" Text="Less than " runat="server" ID="rbLessthan"
                                    value="8" GroupName="filter" />
                            </td>
                        </tr>
                        <tr>
                            <td class="Normal1">
                                <asp:RadioButton CssClass="signup" Text="Greater than " runat="server" ID="rbGreaterThan"
                                    value="9" GroupName="filter" />
                            </td>
                        </tr>
                        <tr>
                            <td class="text">
                                <asp:RadioButton CssClass="signup" Text="Between " runat="server" ID="rbBetween"
                                    value="10" GroupName="filter" />&nbsp;
                                <asp:TextBox runat="server" ID="txtFrom" CssClass="signup" Width="50px" TabIndex="500" />&nbsp;and&nbsp;<asp:TextBox
                                    runat="server" ID="txtTo" CssClass="signup" Width="50px" TabIndex="501" />
                            </td>
                        </tr>
                    </table>
                </div>
                <div id="dvDateFilters">
                    <table cellspacing="0" cellpadding="2" width="100%" bgcolor="white" border="0">
                        <tr>
                            <td class="text">
                                <asp:RadioButton CssClass="signup" Text="Equal To " runat="server" ID="rbEqualToDate"
                                    value="14" GroupName="filter" />&nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td class="text">
                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tr>
                                        <td>
                                            <asp:RadioButton CssClass="signup" Text="Between" runat="server" ID="rbDateRange"
                                                value="11" GroupName="filter" />&nbsp;
                                        </td>
                                        <td>
                                            <BizCalendar:Calendar ID="calFilterFrom" runat="server" ClientIDMode="AutoID" />
                                        </td>
                                        <td>&nbsp;and&nbsp;
                                        </td>
                                        <td>
                                            <BizCalendar:Calendar ID="calFilterTo" runat="server" ClientIDMode="AutoID" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </div>
                <div id="dvDropdownFilter">
                    <table cellspacing="0" cellpadding="2" width="100%" bgcolor="white" border="0">
                        <tr>
                            <td class="Normal1">
                                <asp:RadioButton CssClass="signup" Text="Equal To " runat="server" ID="rbEqualToDD"
                                    value="12" GroupName="filter" />
                            </td>
                        </tr>
                        <tr>
                            <td class="Normal1">
                                <asp:RadioButton CssClass="signup" Text="Not Equal To " runat="server" ID="rbNotEqualToDD"
                                    value="13" GroupName="filter" />
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="modal-footer">
                <input class="btn btn-default pull-left" data-dismiss="modal" id="btnClose" onclick="document.getElementById('dvFilterCriteria').style.display = 'none';" type="button" value="Close" name="btnClose" />
                <input class="btn btn-primary" id="btnOk" runat="server" type="button" value="Ok" onclick="setFilter();" />
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

