Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Reports
Imports BACRM.BusinessLogic.Common
Namespace BACRM.UserInterface.Admin

Public Class frmAdminSection
        Inherits BACRMPage

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub
        Protected WithEvents btnBack As System.Web.UI.WebControls.Button
        Protected WithEvents tbl1 As System.Web.UI.WebControls.Table
        Protected WithEvents btnReCreateCustomReportsData As System.Web.UI.WebControls.Button

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Try
                 ' = Request.Url.Segments(Request.Url.Segments.Length - 1) ' = "Admin"
                btnBack.Attributes.Add("onclick", "return OpenTickler()")
                btnReCreateCustomReportsData.Attributes.Add("onclick", "return AlertB4GTableRecreation()") 'Ask to confirmation to recreating the Global Temporary Tables
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This event is fired each time the "Recreate Global Tables for Custom Reports" is clicked. In this event we will 
        '''     recreate the tables and re-populate it with latest data
        ''' </summary>
        ''' <param name="sender">Represents the sender object.</param>
        ''' <param name="e">Represents the EventArgs.</param>
        ''' <remarks>
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	11/16/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Private Sub btnReCreateCustomReportsData_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReCreateCustomReportsData.Click
            Try
                Dim objCustomReports As New CustomReports           'Create an object of Custom Reports
                objCustomReports.RefreshInterval = ConfigurationManager.AppSettings("CustomReportRefreshInterval") 'Set the refresh interval for the global temp tables
                objCustomReports.RecreateGlobalCustomReportTables() 'Call to recreate the Global Custom Tables
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub
    End Class

End Namespace
