﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/common/Popup.Master" CodeBehind="actionitemorganization.aspx.vb" Inherits=".actionitemorganization" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        function Add() {

            if (document.getElementById("hdnDivisionID").value == "" || document.getElementById("hdnContactID").value == "") {
                alert("Organization and contact are required.");
                return false;
            } else {
                var organizationID = 0, organizationName = "", contactID = 0, contactName = "", followUpID = 0, email = "", phone = "";

                organizationID = document.getElementById("hdnDivisionID").value;
                organizationName = document.getElementById("hdnDivisionName").value;
                contactID = document.getElementById("hdnContactID").value;
                contactName = document.getElementById("hdnContactName").value;
                followUpID = document.getElementById("hdnFollowUp").value;
                email = document.getElementById("hdnEmail").value;
                phone = document.getElementById("hdnPhone").value;

                window.opener.AddOrganization(organizationID, organizationName, contactID, contactName, followUpID, email, phone);
                window.Close();
            }
        }
        function Remove() {
            window.opener.RemoveOrganization();
            window.Close();
        }
    </script>
    <style type="text/css">
        .rcbInput {
            height: 19px !important;
        }

        #tblOrganization td:first-child {
            font-weight: bold;
            text-align: right;
            width: 55px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server">
    <div style="float: right">
        <asp:Button ID="btnSaveClose" runat="server" Text="Save & Close" CssClass="button" OnClientClick="return Add();" />
        <asp:Button ID="btnRemove" runat="server" Text="Remove" Visible="false" CssClass="button" OnClientClick="return Remove();" />
        <asp:Button ID="btnClose" runat="server" Text="Close" OnClientClick="return Close();" CssClass="button" />
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitle" runat="server">
    Add Organization
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Content" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server" ChildrenAsTriggers="true">
        <ContentTemplate>
            <div style="height: 200px;">
                <table id="tblOrganization" width="600" style="padding-top: 10px; padding-bottom: 10px;">
                    <tr>
                        <td>Organization&nbsp;<span style="color: red; font-size: 12px">*</span></td>
                        <td>
                            <telerik:RadComboBox AccessKey="C" ID="radCmbCompany" Style="width: 200px !important;" DropDownWidth="500px"
                                OnClientItemsRequested="OnClientItemsRequestedOrganization"
                                ShowMoreResultsBox="true"
                                Skin="Default" runat="server" AutoPostBack="True" AllowCustomText="True" EnableLoadOnDemand="True" ClientIDMode="Static">
                                <WebServiceSettings Path="../common/Common.asmx" Method="GetCompanies" />
                            </telerik:RadComboBox>
                        </td>
                    </tr>
                    <tr>
                        <td>Contact&nbsp;<span style="color: red; font-size: 12px">*</span></td>
                        <td>
                            <asp:DropDownList ID="ddlContact" CssClass="signup" runat="server" AutoPostBack="true" Style="width: 200px !important;">
                                <asp:ListItem Value="0">--Select One--</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                </table>
            </div>
            <asp:HiddenField ID="hdnDivisionID" runat="server" />
            <asp:HiddenField ID="hdnDivisionName" runat="server" />
            <asp:HiddenField ID="hdnContactID" runat="server" />
            <asp:HiddenField ID="hdnContactName" runat="server" />
            <asp:HiddenField ID="hdnFollowUp" runat="server" />
            <asp:HiddenField ID="hdnPhone" runat="server" />
            <asp:HiddenField ID="hdnEmail" runat="server" />
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
