﻿<%@ Page Language="vb" AutoEventWireup="true" CodeBehind="frmBizSaleSide.aspx.vb" Inherits="BACRM.UserInterface.Admin.frmBizSaleSide"
    MasterPageFile="~/common/Popup.Master" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <link rel="stylesheet" href="../CSS/lists.css" type="text/css" />
    <title>Sort Item List</title>
    <script language="JavaScript" type="text/javascript" src="../javascript/coordinates.js"></script>
    <script language="JavaScript" type="text/javascript" src="../javascript/drag.js"></script>
    <script language="JavaScript" type="text/javascript" src="../javascript/dragdrop.js"></script>
    <script language="JavaScript" type="text/javascript">
        $(document).ready(function () {
            if (document.getElementById("x") != null) {
                list = document.getElementById("x");
                DragDrop.makeListContainer(list, 'g2');
                list.onDragOver = function () { this.style["background"] = "none"; };
                list.onDragOut = function () { this.style["background"] = "none"; };
            }
        });

        function getSort() {
            order = document.getElementById("order");
            order.value = DragDrop.serData('g2', null);

        }
        function Save() {
            document.getElementById("btnGo").click();
            return false;
        }
        function Sort() {
            document.getElementById("btnSort").click();
            return false;
        }

        function Close() {
            //window.opener.GetList()
            window.close()
            return false;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <div class="input-part">
        <div class="right-input">
          <asp:Label runat="server" ID="lblButtons">
    </asp:Label>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageTitle" runat="server" ClientIDMode="Static">
    Sort List Items
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="Content" runat="server" ClientIDMode="Static">
    <asp:ScriptManager ID="ScriptManager1" EnablePartialRendering="true" runat="server">
    </asp:ScriptManager>
    <asp:Label runat="server" ID="lblMain">
    </asp:Label>
    <asp:HiddenField ID="order" runat="server" />
    <asp:Button runat="server" ID="btnGo" Style="display: none" />
    <asp:Button runat="server" ID="btnSort" Style="display: none" />
</asp:Content>
