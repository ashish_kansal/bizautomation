﻿Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Common
Public Class frmAutoCreatePOConfig
    Inherits BACRMPage
    Dim objCommon As CCommon

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then
                BindData()
            End If

            If IsPostBack Then

            End If
            btnCancel.Attributes.Add("onclick", "return Close()")
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
    Sub BindData()
        Try
            Dim objCommon As New CCommon()
            objCommon.sb_FillComboFromDBwithSel(ddlOrderStatusReOrderPoint, 176, Session("DomainID"), "2")
            Dim dtTable As DataTable
            Dim objUserAccess As New UserAccess
            objUserAccess.DomainID = Session("DomainID")
            dtTable = objUserAccess.GetDomainDetails()
            If dtTable.Rows(0).Item("bitReOrderPoint") = True Then
                chkReOrderPoint.Checked = True
                ddlOrderStatusReOrderPoint.SelectedValue = dtTable.Rows(0).Item("numReOrderPointOrderStatus")
                ddlOppStatus.SelectedValue = CCommon.ToShort(dtTable.Rows(0).Item("tintOppStautsForAutoPOBackOrder"))
                If CCommon.ToShort(dtTable.Rows(0).Item("tintUnitsRecommendationForAutoPOBackOrder")) = 3 Then
                    rbRecommendation3.Checked = True
                    chkRecommendation3.Checked = CCommon.ToBool(dtTable.Rows(0).Item("bitIncludeRequisitions"))
                ElseIf CCommon.ToShort(dtTable.Rows(0).Item("tintUnitsRecommendationForAutoPOBackOrder")) = 2 Then
                    rbRecommendation2.Checked = True
                    chkRecommendation2.Checked = CCommon.ToBool(dtTable.Rows(0).Item("bitIncludeRequisitions"))
                Else
                    rbRecommendation1.Checked = True
                    chkRecommendation1.Checked = CCommon.ToBool(dtTable.Rows(0).Item("bitIncludeRequisitions"))
                End If
            Else
                chkReOrderPoint.Checked = False
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If Page.IsValid Then
            Try
                save()
                BindData()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End If
    End Sub

    Private Sub btnSaveClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveClose.Click
        If Page.IsValid Then
            Try
                save()
                Dim strScript As String = "<script language=JavaScript>self.close()</script>"
                If (Not Page.IsStartupScriptRegistered("clientScript")) Then Page.RegisterStartupScript("clientScript", strScript)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End If
    End Sub

    Private Sub save()
        Try
            Dim objUserAccess As New UserAccess
            objUserAccess.DomainID = Session("DomainID")
            objUserAccess.bitReOrderPoint = chkReOrderPoint.Checked
            objUserAccess.CreateOppStatus = ddlOppStatus.SelectedValue

            If rbRecommendation3.Checked Then
                objUserAccess.UnitRecommendationType = 3
                objUserAccess.IsIncludeRequisitions = chkRecommendation3.Checked
            ElseIf rbRecommendation2.Checked Then
                objUserAccess.UnitRecommendationType = 2
                objUserAccess.IsIncludeRequisitions = chkRecommendation2.Checked
            Else
                objUserAccess.UnitRecommendationType = 1
                objUserAccess.IsIncludeRequisitions = chkRecommendation1.Checked
            End If


            objUserAccess.ReOrderPointOrderStatus = CCommon.ToLong(ddlOrderStatusReOrderPoint.SelectedValue)
            objUserAccess.UpdateDomainAutoCreatePOConfig()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub



End Class