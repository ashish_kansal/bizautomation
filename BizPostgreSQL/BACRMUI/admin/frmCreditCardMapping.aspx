﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmCreditCardMapping.aspx.vb"
    Inherits=".frmCreditCardMapping" MasterPageFile="~/common/Popup.Master" ClientIDMode="Static" %>

<asp:Content ID="Content3" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Credit Card Mapping</title>
    <script language="Javascript" type="text/javascript">
        function Save() {
            if (document.getElementById("ddlCreditCard").value == 0) {
                alert("Please Select CreditCard Type");
                document.getElementById("ddlCreditCard").focus();
                return false;
            }
            //Commented by chintan bug Fix -1385 #3
            //            if (document.getElementById("ddlAPAccount").value == 0) {
            //                alert("Please Select A/P Account");
            //                document.getElementById("ddlAPAccount").focus();
            //                return false;
            //            }
            if (document.getElementById("txtCharge").value == "") {
                alert("Enter Transaction Charge");
                document.getElementById("txtCharge").focus();
                return false;
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <div class="input-part">
        <div class="right-input">
            <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" OnClientClick="return Save();">
            </asp:Button>
            <asp:Button ID="btnClose" runat="server" CssClass="button" Text="Close" OnClientClick="return Close();">
            </asp:Button>&nbsp;&nbsp;
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="PageTitle" runat="server" ClientIDMode="Static">
Credit Card Mapping
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="Content" runat="server" ClientIDMode="Static">
    <table width="600px">
        <tr>
            <td class="normal4" align="center" colspan="2">
                <asp:Literal ID="litMessage" runat="server"></asp:Literal>
            </td>
        </tr>
        <tr>
            <td class="normal1" align="right">
                CreditCard Type
            </td>
            <td>
                <asp:DropDownList ID="ddlCreditCard" AutoPostBack="True" runat="server" CssClass="signup"
                    Width="350">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="normal1" align="right">
                Transaction charge Bare By
            </td>
            <td>
                <asp:RadioButtonList runat="server" CssClass="signup" ID="rblBareBy" RepeatDirection="Horizontal"
                    AutoPostBack="true">
                    <asp:ListItem Text="Employer" Value="0" Selected="True" />
                    <asp:ListItem Text="Customer" Value="1" />
                </asp:RadioButtonList>
            </td>
        </tr>
        <tr>
            <td class="normal1" align="right">
                Transaction Charge Account
            </td>
            <td>
                <asp:DropDownList ID="ddlAPAccount" runat="server" CssClass="signup" Width="350">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td align="right" class="normal1">
                % Charged per transaction
                <asp:TextBox runat="server" ID="txtCharge" CssClass="signup" Width="20" onkeypress="CheckNumber(1,event);"></asp:TextBox>
            </td>
        </tr>
    </table>
</asp:Content>
