﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="CommissionRuleDtl.aspx.vb"
    Inherits=".CommissionRuleDtl" MasterPageFile="~/common/GridMasterRegular.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title></title>
    <style>
        .StepClass {
            color: Black;
            font-size: 12px;
            font-family: Arial; /*font-weight:bold;*/
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#rbIndividualItem").change(function () {
                $("#hdnSelectedItemOrClassifiction").val("0");
            });

            $("#rbItemClassification").change(function () {
                $("#hdnSelectedItemOrClassifiction").val("0");
            });

            $("#rbAllItems").change(function () {
                $("#hdnSelectedItemOrClassifiction").val("0");
            });

            $("#rbIndividualOrg").change(function () {
                $("#hdnSelectedOrgOrProfile").val("0");
            });

            $("#rbRelProfile").change(function () {
                $("#hdnSelectedOrgOrProfile").val("0");
            });

            $("#rbAllOrg").change(function () {
                $("#hdnSelectedOrgOrProfile").val("0");
            });
        });

        function CheckRad(rad) {
            if (rad.checked == false) {
                alert("Please select the radio button")
                return false;
            }
        }

        function OpenConfig(StepValue, selectedOption, RuleID) {
            window.open("../admin/CommissionItems.aspx?StepValue=" + StepValue + "&SelectedOption=" + selectedOption + "&RuleID=" + RuleID, '', 'toolbar=no,titlebar=no,top=200,width=900,height=400,left=200,scrollbars=yes,resizable=yes');
        }

        function Save() {

            if (document.getElementById("txtRuleName").value == "") {
                alert("Enter rule name");
                document.getElementById('txtRuleName').focus();
                return false;
            }

            var BasedOn = document.getElementById("ddlBasedOn").value


            var idRoot = '<%= dgCommissionTable.ClientID%>';
            var From = 0;
            var To = 0;
            var From1 = 0;
            var To1 = 0;
            var idtxt = '';
            var idtxt1 = '';
            for (var i = 2; i < document.getElementById(idRoot).rows.length + 1; i++) {
                if (i < 10) {
                    idtxt = '_ctl0'
                }
                else {
                    idtxt = '_ctl'
                }
                From = parseFloat(document.getElementById(idRoot + idtxt + i + '_' + 'txtFrom').value);
                To = parseFloat(document.getElementById(idRoot + idtxt + i + '_' + 'txtTo').value);

                if (isNaN(From) == false && isNaN(To) == false) {

                    if (BasedOn == 4) {
                        if (From > 100 || To > 100) {
                            alert('Project Total Income % : Percentage Range must between 0 to 100')
                            return false;
                        }
                    }

                    if (From >= To) {
                        alert('To Value must be greater than From Value (' + From + '-' + To + ')')
                        return false;
                    }
                    for (var j = 2; j < document.getElementById(idRoot).rows.length + 1; j++) {
                        if (j < 10) {
                            idtxt1 = '_ctl0'
                        }
                        else {
                            idtxt1 = '_ctl'
                        }
                        if (j != i) {
                            From1 = parseFloat(document.getElementById(idRoot + idtxt1 + j + '_' + 'txtFrom').value);
                            To1 = parseFloat(document.getElementById(idRoot + idtxt1 + j + '_' + 'txtTo').value);

                            if (From1 != "" && To1 != "") {

                                if (From1 >= From && From1 <= To) {
                                    // alert(From1 + '>=' + From + ' && ' + From1 + '<=' + To);
                                    alert('Commission Range (' + From1 + '-' + To1 + ') must be unique');
                                    return false;
                                }
                                if (To1 >= From && To1 <= To) {
                                    // alert(From1 + '>=' + From + ' && ' + From1 + '<=' + To);
                                    alert('Commission Range (' + From1 + '-' + To1 + ') must be unique');
                                    return false;
                                }
                            }
                        }
                    }
                }
            }

            str = '';
            for (var i = 0; i < document.getElementById('lstSelectedContact').options.length; i++) {
                var SelectedText, SelectedValue;
                SelectedValue = document.getElementById('lstSelectedContact').options[i].value;
                SelectedText = document.getElementById('lstSelectedContact').options[i].text;
                str = str + SelectedValue + ','
            }
            document.getElementById('txtContactsHidden').value = str;

            return true;
        }

        function keyPressed(TB, e) {
            var tblGrid = document.getElementById("<%= dgCommissionTable.ClientID%>");

            var rowcount = tblGrid.rows.length;
            var TBID = document.getElementById(TB);

            var key;
            if (window.event) { e = window.event; }
            key = e.keyCode;
            if (key == 37 || key == 38 || key == 39 || key == 40) {
                for (Index = 1; Index < rowcount; Index++) {

                    for (childIndex = 0; childIndex <
              tblGrid.rows[Index].cells.length; childIndex++) {

                        if (tblGrid.rows[Index].cells[childIndex].children[0] != null) {
                            if (tblGrid.rows[Index].cells[
                 childIndex].children[0].id == TBID.id) {

                                if (key == 40) {
                                    if (Index + 1 < rowcount) {
                                        if (tblGrid.rows[Index + 1].cells[
                     childIndex].children[0] != null) {
                                            if (tblGrid.rows[Index + 1].cells[
                       childIndex].children[0].type == 'text') {

                                                //downvalue

                                                tblGrid.rows[Index + 1].cells[
                             childIndex].children[0].focus();
                                                return false;
                                            }
                                        }
                                    }
                                }
                                if (key == 38) {
                                    if (tblGrid.rows[Index - 1].cells[
                     childIndex].children[0] != null) {
                                        if (tblGrid.rows[Index - 1].cells[
                       childIndex].children[0].type == 'text') {
                                            //upvalue

                                            tblGrid.rows[Index - 1].cells[
                             childIndex].children[0].focus();
                                            return false;
                                        }
                                    }
                                }

                                if (key == 37 && (childIndex != 0)) {

                                    if ((tblGrid.rows[Index].cells[
                      childIndex - 1].children[0]) != null) {

                                        if (tblGrid.rows[Index].cells[
                       childIndex - 1].children[0].type == 'text') {
                                            //left

                                            if (tblGrid.rows[Index].cells[
                         childIndex - 1].children[0].value != '') {
                                                var cPos =
                           getCaretPos(tblGrid.rows[Index].cells[
                                       childIndex - 1].children[0], 'left');
                                                if (cPos) {
                                                    tblGrid.rows[Index].cells[
                                 childIndex - 1].children[0].focus();
                                                    return false;
                                                }
                                                else {
                                                    return false;
                                                }
                                            }
                                            tblGrid.rows[Index].cells[childIndex - 1].children[0].focus();
                                            return false;
                                        }
                                    }
                                }

                                if (key == 39) {
                                    if (tblGrid.rows[Index].cells[childIndex + 1].children[0] != null) {
                                        if (tblGrid.rows[Index].cells[
                       childIndex + 1].children[0].type == 'text') {
                                            //right

                                            if (tblGrid.rows[Index].cells[
                         childIndex + 1].children[0].value != '') {
                                                var cPosR =
                           getCaretPos(tblGrid.rows[Index].cells[
                                       childIndex + 1].children[0], 'right');
                                                if (cPosR) {
                                                    tblGrid.rows[Index].cells[
                                 childIndex + 1].children[0].focus();
                                                    return false;
                                                }
                                                else {
                                                    return false;
                                                }
                                            }
                                            tblGrid.rows[Index].cells[childIndex + 1].children[0].focus();
                                            return false;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        function getCaretPos(control, way) {
            var movement;
            if (way == 'left') {
                movement = -1;
            }
            else {
                movement = 1;
            }
            if (control.createTextRange) {
                control.caretPos = document.selection.createRange().duplicate();
                if (control.caretPos.move("character", movement) != '') {
                    return false;
                }
                else {
                    return true;
                }
            }
        }

        sortitems = 0;  // 0-False , 1-True
        function moveContact() {
            var fbox = $("#lstAvailableContacts")[0];
            var tbox = $("#lstSelectedContact")[0];

            for (var i = 0; i < fbox.options.length; i++) {
                if (fbox.options[i].selected && fbox.options[i].value != "") {

                    if ($("[id$=rbPartner]").is(":checked") && fbox.options[i].value.split("~")[2] != "1") {
                        alert("You are not allowed to select employees when rule is for partner");
                        return false;
                    } else if (!$("[id$=rbPartner]").is(":checked") && fbox.options[i].value.split("~")[2] == "1") {
                        alert("You are not allowed to select partner when rule is for sales order assignee or owner.");
                        return false;
                    }

                    /// to check for duplicates 
                    for (var j = 0; j < tbox.options.length; j++) {
                        if (tbox.options[j].value == fbox.options[i].value) {
                            alert("Item is already selected");
                            return false;
                        }
                    }

                    var no = new Option();
                    no.value = fbox.options[i].value;
                    no.text = fbox.options[i].text;
                    tbox.options[tbox.options.length] = no;
                    fbox.options[i].value = "";
                    fbox.options[i].text = "";

                }
            }
            BumpUp(fbox);
            if (sortitems) SortD(tbox);
            return false;
        }

        function removeContact() {
            var fbox = $("#lstSelectedContact")[0];
            var tbox = $("#lstAvailableContacts")[0];

            for (var i = 0; i < fbox.options.length; i++) {
                if (fbox.options[i].selected && fbox.options[i].value != "") {
                    /// to check for duplicates 
                    for (var j = 0; j < tbox.options.length; j++) {
                        if (tbox.options[j].value == fbox.options[i].value) {
                            fbox.options[i].value = "";
                            fbox.options[i].text = "";
                            BumpUp(fbox);
                            if (sortitems) SortD(tbox);
                            return false;


                            //alert("Item is already selected");
                            //return false;
                        }
                    }

                    var no = new Option();
                    no.value = fbox.options[i].value;
                    no.text = fbox.options[i].text;
                    tbox.options[tbox.options.length] = no;
                    fbox.options[i].value = "";
                    fbox.options[i].text = "";

                }
            }
            BumpUp(fbox);
            if (sortitems) SortD(tbox);
            return false;
        }

        function BumpUp(box) {
            for (var i = 0; i < box.options.length; i++) {
                if (box.options[i].value == "") {
                    for (var j = i; j < box.options.length - 1; j++) {
                        box.options[j].value = box.options[j + 1].value;
                        box.options[j].text = box.options[j + 1].text;
                    }
                    var ln = i;
                    break;
                }
            }
            if (ln < box.options.length) {
                box.options.length -= 1;
                BumpUp(box);
            }
        }

        function SortD(box) {
            var temp_opts = new Array();
            var temp = new Object();
            for (var i = 0; i < box.options.length; i++) {
                temp_opts[i] = box.options[i];
            }
            for (var x = 0; x < temp_opts.length - 1; x++) {
                for (var y = (x + 1) ; y < temp_opts.length; y++) {
                    if (temp_opts[x].text > temp_opts[y].text) {
                        temp = temp_opts[x].text;
                        temp_opts[x].text = temp_opts[y].text;
                        temp_opts[y].text = temp;
                        temp = temp_opts[x].value;
                        temp_opts[x].value = temp_opts[y].value;
                        temp_opts[y].value = temp;
                    }
                }
            }
            for (var i = 0; i < box.options.length; i++) {
                box.options[i].value = temp_opts[i].value;
                box.options[i].text = temp_opts[i].text;
            }
        }
        function OpenHelp() {
            window.open('../Help/Admin-Commission_Rules.html', '', 'toolbar=no,titlebar=no,top=200,left=200,width=550,height=350,scrollbars=yes,resizable=yes')
            return false
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server"
    ClientIDMode="Static">
    <div class="row padbottom10">
        <div class="col-xs-12">
            <div class="pull-right">
                <asp:LinkButton ID="btnSave" runat="server" CssClass="btn btn-primary" OnClientClick="return Save()"><i class="fa fa-floppy-o"></i>&nbsp;&nbsp;Save</asp:LinkButton>
                <asp:LinkButton ID="btnSaveClose" runat="server" CssClass="btn btn-primary" OnClientClick="return Save()"><i class="fa fa-floppy-o"></i>&nbsp;&nbsp;Save & Close</asp:LinkButton>
                <asp:LinkButton ID="btnClose" runat="server" CssClass="btn btn-primary"><i class="fa fa-times-circle"></i>&nbsp;&nbsp;Close</asp:LinkButton>
            </div>
        </div>
    </div>
    <asp:UpdateProgress ID="UpdateProgress" runat="server" ClientIDMode="Static">
        <ProgressTemplate>
            <div class="overlay">
                <div class="overlayContent" style="color: #000; text-align: center; width: 250px; padding: 20px">
                    <i class="fa fa-2x fa-refresh fa-spin"></i>
                    <h3>Processing Request</h3>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <asp:Literal ID="litMessage" runat="server"></asp:Literal>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server" ClientIDMode="Static">
    Commission Rule&nbsp;<a href="#" onclick="return OpenHelp()"><label class="badge bg-yellow">?</label></a>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BizPager" runat="server" ClientIDMode="Static">
    &nbsp;
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="GridPlaceHolder" runat="server"
    ClientIDMode="Static">
    <div class="table-responsive">
        <div class="row padbottom10">
            <div class="col-md-12">
                <div class="form-inline">
                    <label>Commission Rule Name:</label>
                    <asp:TextBox ID="txtRuleName" runat="server" Width="200" CssClass="form-control" MaxLength="100"></asp:TextBox>
                </div>
            </div>
        </div>

        <table style="padding-left: 5px; padding-right: 5px; padding-bottom: 5px; width: 100%">
            <asp:Panel ID="pnlSel" runat="server" Visible="false">
                <tr>
                    <td style="width: 50%; vertical-align: top;">
                        <table width="100%" cellpadding="0" cellspacing="0" class="table table-bordered">
                            <tr>
                                <td style="background-color: #dcdcdc; height: 30px; padding-left: 5px;">
                                    <b>Step 1: Set Commission Table</b>
                                </td>
                            </tr>
                            <tr>
                                <td style="padding: 10px;">
                                    <div class="alert alert-info">
                                        <strong>Info!</strong> Changes in this step will not be updated once commission rule is used for calculation as it will affect over payment calculation. You can create new commission rule with different values
                                    </div>
                                    <table width="100%">
                                        <tr>
                                            <td style="text-align: right; white-space: nowrap"><b>Based On</b></td>
                                            <td style="padding-left: 5px">
                                                <asp:DropDownList ID="ddlBasedOn" runat="server" CssClass="form-control" Width="145">
                                                    <asp:ListItem Text="Amount Sold" Value="1"></asp:ListItem>
                                                    <asp:ListItem Text="Units Sold" Value="2"></asp:ListItem>
                                                </asp:DropDownList>
                                            </td>
                                            <td width="100%"></td>
                                            <td style="text-align: right; white-space: nowrap"><b>Commission reward</b></td>
                                            <td style="padding-left: 5px">
                                                <asp:DropDownList ID="ddlCommsionType" runat="server" CssClass="form-control" Width="145">
                                                    <asp:ListItem Text="Percentage" Value="1"></asp:ListItem>
                                                    <asp:ListItem Text="Flat Amount" Value="2"></asp:ListItem>
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                        </tr>
                                    </table>
                                    <br />
                                    <asp:DataGrid ID="dgCommissionTable" runat="server" CssClass="table table-striped table-bordered" Width="100%" AutoGenerateColumns="False"
                                        ClientIDMode="AutoID" UseAccessibleHeader="true">
                                        <Columns>
                                            <asp:BoundColumn DataField="numComRuleDtlID" Visible="false"></asp:BoundColumn>
                                            <asp:TemplateColumn>
                                                <HeaderTemplate>
                                                    From&nbsp;<asp:Label ID="Label1" runat="server" ToolTip="Please note that pay period from Global Settings -> General -> Pay-Period will be used as duration so select commission range accordingly." CssClass="tip">[?]</asp:Label>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:TextBox ID="txtFrom" TextMode="SingleLine" Width="80" runat="server" CssClass="form-control"
                                                        Text='<%# Eval("intFrom") %>' onkeyup="keyPressed(this.id,event)" AUTOCOMPLETE="OFF"></asp:TextBox>
                                                </ItemTemplate>
                                                <ItemStyle VerticalAlign="Bottom" HorizontalAlign="Center" />
                                                <HeaderStyle VerticalAlign="Bottom" HorizontalAlign="Center" />
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="To">
                                                <ItemTemplate>
                                                    <asp:TextBox ID="txtTo" TextMode="SingleLine" Width="80" runat="server" CssClass="form-control"
                                                        Text='<%# Eval("intTo") %>' onkeyup="keyPressed(this.id,event)" AUTOCOMPLETE="OFF"></asp:TextBox>
                                                </ItemTemplate>
                                                <ItemStyle VerticalAlign="Bottom" HorizontalAlign="Center" />
                                                <HeaderStyle VerticalAlign="Bottom" HorizontalAlign="Center" />
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="Commission">
                                                <ItemTemplate>
                                                    <asp:TextBox ID="txtComAmount" TextMode="SingleLine" Width="80" runat="server" Text='<%# Eval("decCommission") %>'
                                                        CssClass="form-control" onkeyup="keyPressed(this.id,event)" AUTOCOMPLETE="OFF"></asp:TextBox>
                                                </ItemTemplate>
                                                <ItemStyle VerticalAlign="Bottom" HorizontalAlign="Center" />
                                                <HeaderStyle VerticalAlign="Bottom" HorizontalAlign="Center" />
                                            </asp:TemplateColumn>
                                        </Columns>
                                    </asp:DataGrid>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td></td>
                    <td style="width: 50%; vertical-align: top">
                        <table width="100%" cellpadding="0" cellspacing="0" class="table table-bordered" style="margin-bottom: 0px">
                            <tr>
                                <td style="background-color: #dcdcdc; height: 30px; padding-left: 5px;">
                                    <b>Step 2: Select items</b>
                                </td>
                            </tr>
                            <tr>
                                <td style="background-color: #f7f7f7; padding: 10px;">
                                    <asp:RadioButton ID="rbIndividualItem" runat="server" CssClass="signup" GroupName="Items" />
                                    <asp:HyperLink ID="hplIndividualItem" runat="server" CssClass="hyperlink" Text="<label for='rbIndividualItem'>Apply to items individually</label>"></asp:HyperLink>
                                    <br />
                                    <asp:RadioButton ID="rbItemClassification" runat="server" CssClass="signup" GroupName="Items" />
                                    <asp:HyperLink ID="hplClassification" CssClass="hyperlink" Text="<label for='rbItemClassification'>Apply to items with the selected item classifications </label>" runat="server"> </asp:HyperLink>
                                    <br />
                                    <asp:RadioButton ID="rbAllItems" runat="server" CssClass="signup" GroupName="Items" />
                                    <label for="rbAllItems">All Items</label>

                                    <asp:HiddenField ID="hdnSelectedItemOrClassifiction" runat="server" />
                                </td>
                            </tr>
                        </table>
                        <table width="100%" cellpadding="0" cellspacing="0" class="table table-bordered" style="margin-bottom: 0px">
                            <tr>
                                <td style="background-color: #dcdcdc; height: 30px; padding-left: 5px;">
                                    <b>Step 3: Select Leads, Prospects and Accounts</b>
                                </td>
                            </tr>
                            <tr>
                                <td style="background-color: #f7f7f7; padding: 10px;">
                                    <asp:RadioButton ID="rbIndividualOrg" runat="server" CssClass="signup" GroupName="Organization" />
                                    <asp:HyperLink ID="hplIndividualOrg" CssClass="hyperlink" Text="<label for='rbIndividualOrg'>Apply to customers individually</label>" runat="server"> </asp:HyperLink>
                                    <br />
                                    <asp:RadioButton ID="rbRelProfile" runat="server" CssClass="signup" GroupName="Organization" />
                                    <asp:HyperLink ID="hplRelProfile" CssClass="hyperlink" Text="<label for='rbRelProfile'>Apply to customers with the following Relationships & Profiles</label>" runat="server"> </asp:HyperLink>
                                    <br />
                                    <asp:RadioButton ID="rbAllOrg" runat="server" CssClass="signup" GroupName="Organization" />
                                    <label for="rbAllOrg">All Customers</label>
                                    <asp:HiddenField ID="hdnSelectedOrgOrProfile" runat="server" />
                                </td>
                            </tr>
                        </table>
                        <table width="100%" cellpadding="0" cellspacing="0" class="table table-bordered" style="margin-bottom: 0px">
                            <tr>
                                <td style="background-color: #dcdcdc; height: 30px; padding-left: 5px;">
                                    <b>Step 4: Apply this rule to the following contacts</b>
                                </td>
                            </tr>
                            <tr>
                                <td style="background-color: #f7f7f7; padding: 10px;">
                                    <table border="0" style="width: 100%">
                                        <tr>
                                            <td class="normal1" align="left" colspan="3">
                                                <asp:RadioButton ID="rbAssign" AutoPostBack="true" OnCheckedChanged="rbAssign_CheckedChanged" runat="server" Text="When Assignee of a Sales Order"
                                                    GroupName="gnAssignOwner" />
                                                <asp:RadioButton ID="rbOwner" AutoPostBack="true" OnCheckedChanged="rbOwner_CheckedChanged" runat="server" Text="When Owner of a Sales Order" GroupName="gnAssignOwner" />
                                                <asp:RadioButton ID="rbPartner" AutoPostBack="true" OnCheckedChanged="rbPartner_CheckedChanged" runat="server" Text="When Partner of a Sales Order" GroupName="gnAssignOwner" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <br />
                                            </td>
                                        </tr>
                                        <tr>
                                            <asp:UpdatePanel ID="UpdatePanel1" runat="server" ChildrenAsTriggers="true">
                                                <ContentTemplate>
                                                    <td align="center" class="normal1">
                                                        <b>Available Employees & Partners</b><br />
                                                        <asp:ListBox ID="lstAvailableContacts" runat="server" Width="250" Height="250" CssClass="signup"
                                                            ClientIDMode="Static"></asp:ListBox>
                                                    </td>
                                                    <td align="center" valign="middle" width="10%">
                                                        <asp:LinkButton ID="btnContactAdd" runat="server" ClientIDMode="Static" CssClass="btn btn-primary" OnClientClick="return moveContact();">Add&nbsp;&nbsp;<i class="fa fa-arrow-right"></i></asp:LinkButton>

                                                        <br />
                                                        <br />
                                                        <asp:LinkButton ID="btnContactRemove" runat="server" ClientIDMode="Static" CssClass="btn btn-danger" OnClientClick="return removeContact();"><i class="fa fa-arrow-left"></i>&nbsp;&nbsp;Remove</asp:LinkButton>

                                                    </td>
                                                    <td align="center" class="normal1"><b>Selected Employees & Partners</b><br />
                                                        <asp:ListBox ID="lstSelectedContact" runat="server" Width="250" Height="250" CssClass="signup"
                                                            ClientIDMode="Static"></asp:ListBox>
                                                        <asp:TextBox Style="display: none" ID="txtContactsHidden" runat="server" ClientIDMode="Static"></asp:TextBox>
                                                    </td>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </asp:Panel>
        </table>
    </div>
</asp:Content>
