''' -----------------------------------------------------------------------------
''' Project	 : BACRM.UserInterface.Reports
''' Class	 : frmManageAutoRoutingRules
''' 
''' -----------------------------------------------------------------------------
''' <summary>
'''     This is a interface for creation of Routing Rules parameters for Leads
''' </summary>
''' <remarks>
''' </remarks>
''' <history>
''' 	[Debasish]	08/21/2005	Created
''' </history>
''' ''' -----------------------------------------------------------------------------
Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Common
Namespace BACRM.UserInterface.Admin

    Public Class frmManageApprovalConfig
        Inherits BACRMPage

        Dim approval As New ApprovalConfig
        Dim dt As New DataTable
        Dim dtUser As DataTable
        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Load
            Try
                DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ""
                DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "none")
                If Not IsPostBack Then
                    GetUserRightsForPage(13, 7)
                    If m_aryRightsForPage(RIGHTSTYPE.VIEW) = 0 Then Response.Redirect("../admin/authentication.aspx?mesg=AS")
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub
        Private Sub DisplayError(ByVal ex As String)
            DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ex
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
        End Sub
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This method is used to raise the button event to teh datagrid
        ''' </summary>
        ''' <remarks>
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	09/21/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Sub FillUserInDataGrid(ddlUser As DropDownList)
            Try
                ddlUser.DataSource = dtUser
                ddlUser.DataTextField = "vcUserName"
                ddlUser.DataValueField = "numUserId"
                ddlUser.DataBind()
                ddlUser.Items.Insert(0, New ListItem("-Select User-", "0"))
            Catch ex As Exception
                Throw ex
            End Try
            'databind the datgrid
        End Sub


        Protected Sub ddlModule_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlModule.SelectedIndexChanged
            approval.chrAction = "VU"
            approval.numDomainId = Session("DomainId")
            approval.numModuleId = ddlModule.SelectedValue
            dtUser = approval.GetAllUsers()
           

            approval.chrAction = "V"
            approval.numDomainId = Session("DomainId")
            approval.numModuleId = ddlModule.SelectedValue
            dt = approval.GetAllUsers()

            grdApprovalConfig.DataSource = dtUser
            grdApprovalConfig.DataBind()
        End Sub

        Protected Sub grdApprovalConfig_RowDataBound(sender As Object, e As GridViewRowEventArgs)
            Dim int1stApprover As Long
            Dim int2ndApprover As Long
            Dim int3rdApprover As Long
            Dim int4thApprover As Long
            Dim int5thApprover As Long
            Dim hdnUserid As HiddenField
            Dim ddl1stApprover As DropDownList
            Dim ddl2ndApprover As DropDownList
            Dim ddl3rdApprover As DropDownList
            Dim ddl4thApprover As DropDownList
            Dim ddl5thApprover As DropDownList
            If e.Row.RowType = DataControlRowType.DataRow Then
                ddl1stApprover = DirectCast(e.Row.FindControl("ddl1stLevelApprover"), DropDownList)
                ddl2ndApprover = DirectCast(e.Row.FindControl("ddl2ndLevelApprover"), DropDownList)
                ddl3rdApprover = DirectCast(e.Row.FindControl("ddl3rdLevelApprover"), DropDownList)
                ddl4thApprover = DirectCast(e.Row.FindControl("ddl4thLevelApprover"), DropDownList)
                ddl5thApprover = DirectCast(e.Row.FindControl("ddl5thLevelApprover"), DropDownList)
                FillUserInDataGrid(ddl1stApprover)
                FillUserInDataGrid(ddl2ndApprover)
                FillUserInDataGrid(ddl3rdApprover)
                FillUserInDataGrid(ddl4thApprover)
                FillUserInDataGrid(ddl5thApprover)
                hdnUserid = DirectCast(e.Row.FindControl("hdnUserId"), HiddenField)
                Dim dr As DataRow = dt.Select("numUserId=" + hdnUserid.Value + "").FirstOrDefault()
                ddl1stApprover.SelectedValue = dr("numLevel1Authority")
                ddl2ndApprover.SelectedValue = dr("numLevel2Authority")
                ddl3rdApprover.SelectedValue = dr("numLevel3Authority")
                ddl4thApprover.SelectedValue = dr("numLevel4Authority")
                ddl5thApprover.SelectedValue = dr("numLevel5Authority")
            End If

        End Sub

        Protected Sub btnSave_Click(sender As Object, e As EventArgs)
            Dim hdnUserid As HiddenField
            Dim ddl1stApprover As DropDownList
            Dim ddl2ndApprover As DropDownList
            Dim ddl3rdApprover As DropDownList
            Dim ddl4thApprover As DropDownList
            Dim ddl5thApprover As DropDownList
            Dim ds As New DataSet
            Dim dtXml As New DataTable
            dtXml.Columns.Add("numUserId")
            dtXml.Columns.Add("numLevel1Authority")
            dtXml.Columns.Add("numLevel2Authority")
            dtXml.Columns.Add("numLevel3Authority")
            dtXml.Columns.Add("numLevel4Authority")
            dtXml.Columns.Add("numLevel5Authority")
            For i As Integer = 0 To grdApprovalConfig.Rows.Count - 1
                hdnUserid = DirectCast(grdApprovalConfig.Rows(i).FindControl("hdnUserId"), HiddenField)
                ddl1stApprover = DirectCast(grdApprovalConfig.Rows(i).FindControl("ddl1stLevelApprover"), DropDownList)
                ddl2ndApprover = DirectCast(grdApprovalConfig.Rows(i).FindControl("ddl2ndLevelApprover"), DropDownList)
                ddl3rdApprover = DirectCast(grdApprovalConfig.Rows(i).FindControl("ddl3rdLevelApprover"), DropDownList)
                ddl4thApprover = DirectCast(grdApprovalConfig.Rows(i).FindControl("ddl4thLevelApprover"), DropDownList)
                ddl5thApprover = DirectCast(grdApprovalConfig.Rows(i).FindControl("ddl5thLevelApprover"), DropDownList)
                dtXml.Rows.Add(hdnUserid.Value, ddl1stApprover.SelectedValue, ddl2ndApprover.SelectedValue, ddl3rdApprover.SelectedValue, ddl4thApprover.SelectedValue, ddl5thApprover.SelectedValue)
            Next
            ds.Tables.Add(dtXml.Copy)
            ds.Tables(0).TableName = "ApprovalConfig"
            Dim xml As String = ds.GetXml()
            approval.chrAction = "A"
            approval.numDomainId = Session("DomainId")
            approval.numModuleId = ddlModule.SelectedValue
            approval.strXml = xml
            Dim res As Boolean
            res = approval.SetAllUsers()
        End Sub
    End Class
End Namespace