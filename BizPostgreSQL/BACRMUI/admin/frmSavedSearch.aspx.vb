﻿Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Admin

Public Class frmSavedSearch
    Inherits BACRMPage
    Dim lngFormID As Long
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        lngFormID = CCommon.ToLong(GetQueryStringVal("FormID"))

    End Sub

    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            If lngFormID > 0 And txtSearchName.Text.Trim.Length > 1 Then
                Dim objSearch As New FormGenericAdvSearch
                objSearch.byteMode = 2
                objSearch.DomainID = Session("DomainID")
                objSearch.UserCntID = Session("UserContactID")
                objSearch.QueryWhereCondition = CCommon.ToString(Session("SavedSearchCondition"))
                objSearch.SlidingDays = CCommon.ToInteger(Session("SlidingDays"))
                objSearch.TimeExpression = CCommon.ToString(Session("TimeExpression"))
                objSearch.SearchName = txtSearchName.Text.Trim
                objSearch.SearchQueryforDisplay = CCommon.ToString(Session("UserFriendlyQuery"))
                objSearch.FormID = lngFormID
                objSearch.ManageSavedSearch()
                litMessage.Text = "Your search criteria is saved sucessfully"
                btnSave.Visible = False
            Else
                litMessage.Text = "Please enter search name"
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
End Class