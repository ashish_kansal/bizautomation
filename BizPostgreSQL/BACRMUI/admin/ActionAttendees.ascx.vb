﻿Imports BACRM.BusinessLogic.Contacts
Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Marketing
Imports BACRM.BusinessLogic.Outlook
Imports BACRM.BusinessLogic.Projects
Imports BACRM.BusinessLogic.Opportunities
Imports BACRM.BusinessLogic.Leads
Imports BACRM.BusinessLogic.Alerts

Public Class ActionAttendees
    Inherits BACRMUserControl
    Dim lngCommId, lngProjectID As Long
    Dim dsTemp As DataSet
    Dim objCommon As CCommon
    Dim objActionItem As ActionItem

#Region "Public Properties"

    Public Property AttendeesFor As Short '1: Action Item, 2: Project

#End Region


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            lngCommId = CCommon.ToLong(GetQueryStringVal("CommId"))
            lngProjectID = CCommon.ToLong(GetQueryStringVal("ProId"))

            If Not IsPostBack Then
                sb_DisplayDetails()

                If lngCommId > 0 Then
                    btnSendAlert.Visible = True
                    gvAttendee.Columns(8).Visible = True
                End If

                ddlAttendeeUserType.SelectedValue = "2"
                trAttendeeCmbCompany.Visible = True
                hplEmpAvaliability.Visible = False

                If AttendeesFor <> 1 AndAlso AttendeesFor <> 2 Then
                    btnAttendeeAdd.ViewStateMode = False
                    btnSendAlert.Visible = False
                End If
            End If
            btnAttendeeAdd.Attributes.Add("onclick", "return SaveAttendee()")
            hplEmpAvaliability.Attributes.Add("onclick", "return openEmpAvailability();")
            radAttendeeCmbCompany.Focus()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub
    Private Sub sb_DisplayDetails()
        Try
            If AttendeesFor = 1 Then
                Dim dtActDetails As DataTable
                If objActionItem Is Nothing Then objActionItem = New ActionItem
                objActionItem.CommID = lngCommId
                objActionItem.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")

                dsTemp = objActionItem.GetCommunicationAttendees
                If dsTemp.Tables(0).Rows.Count > 0 Then
                    Session("AttendeeTable") = dsTemp

                    Dim dtAttendee As New DataTable
                    dtAttendee = dsTemp.Tables(0)
                    dtAttendee.PrimaryKey = New DataColumn() {dsTemp.Tables(0).Columns("numContactID")}

                    gvAttendee.DataSource = dtAttendee
                    gvAttendee.DataBind()
                End If
            ElseIf AttendeesFor = 2 Then
                Dim objProjectIP As New ProjectIP
                objProjectIP.DomainID = CCommon.ToLong(Session("DomainID"))
                objProjectIP.ClientTimeZoneOffset = CCommon.ToLong(Session("ClientMachineUTCTimeOffset"))
                objProjectIP.ProjectID = lngProjectID
                dsTemp = objProjectIP.GetAssociatedContacts()
                If dsTemp.Tables(0).Rows.Count > 0 Then
                    Session("AttendeeTable") = dsTemp

                    Dim dtAttendee As New DataTable
                    dtAttendee = dsTemp.Tables(0)
                    dtAttendee.PrimaryKey = New DataColumn() {dsTemp.Tables(0).Columns("numContactID")}

                    gvAttendee.DataSource = dtAttendee
                    gvAttendee.DataBind()
                End If
            End If
        Catch Ex As Exception
            Throw Ex
        End Try
    End Sub
    Private Sub ddlAttendeeUserType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAttendeeUserType.SelectedIndexChanged
        If ddlAttendeeUserType.SelectedValue = "1" Then
            objCommon = New CCommon
            objCommon.sb_FillConEmpFromDBSel(ddlAttendeeUser, Session("DomainID"), 0, 0)

            ddlAttendeeUser.ClearSelection()
            radAttendeeCmbCompany.Text = Session("CompanyName")
            radAttendeeCmbCompany.SelectedValue = Session("UserDivisionID")
            trAttendeeCmbCompany.Visible = True
            hplEmpAvaliability.Visible = True
        ElseIf ddlAttendeeUserType.SelectedValue = "2" Then
            ddlAttendeeUser.Items.Clear()
            trAttendeeCmbCompany.Visible = True

            radAttendeeCmbCompany.Text = ""
            radAttendeeCmbCompany.SelectedValue = 0
            ddlAttendeeUser.ClearSelection()
            hplEmpAvaliability.Visible = False
        End If
    End Sub

    Private Sub radAttendeeCmbCompany_ItemsRequested(ByVal o As Object, ByVal e As Telerik.Web.UI.RadComboBoxItemsRequestedEventArgs) Handles radAttendeeCmbCompany.ItemsRequested
        Try
            If e.Text <> "" And Len(e.Text) >= IIf(Session("ChrForCompSearch") = 0, 1, Session("ChrForCompSearch")) Then

                If objCommon Is Nothing Then objCommon = New CCommon
                With objCommon
                    .DomainID = Session("DomainID")
                    .Filter = Trim(e.Text) & "%"
                    .UserCntID = Session("UserContactID")
                    radAttendeeCmbCompany.DataSource = objCommon.PopulateOrganization.Tables(0).DefaultView
                    radAttendeeCmbCompany.DataTextField = "vcCompanyname"
                    radAttendeeCmbCompany.DataValueField = "numDivisionID"
                    radAttendeeCmbCompany.DataBind()
                End With
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Private Sub radAttendeeCmbCompany_SelectedIndexChanged(ByVal o As Object, ByVal e As Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs) Handles radAttendeeCmbCompany.SelectedIndexChanged
        Try
            If radAttendeeCmbCompany.SelectedValue <> "" Then
                ddlAttendeeUser.Items.Clear()
                Dim objOpportunities As New COpportunities
                objOpportunities.DivisionID = radAttendeeCmbCompany.SelectedValue
                ddlAttendeeUser.DataSource = objOpportunities.ListContact().Tables(0).DefaultView()
                ddlAttendeeUser.DataTextField = "ContactType"
                ddlAttendeeUser.DataValueField = "numcontactId"
                ddlAttendeeUser.DataBind()
                ddlAttendeeUser.Items.Insert(0, New ListItem("--Select One--", "0"))
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Private Sub btnAttendeeAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAttendeeAdd.Click
        Try
            If AttendeesFor = 1 Then
                If objActionItem Is Nothing Then objActionItem = New ActionItem
                objActionItem.CommID = lngCommId
                objActionItem.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
                objActionItem.ContactID = ddlAttendeeUser.SelectedValue
                objActionItem.Type = 0
                objActionItem.ManageCommunicationAttendees()
            ElseIf AttendeesFor = 2 Then
                Dim objProjectIP As New ProjectIP
                objProjectIP.DomainID = CCommon.ToLong(Session("DomainID"))
                objProjectIP.ProjectID = lngProjectID
                objProjectIP.ContactID = ddlAttendeeUser.SelectedValue
                objProjectIP.bytemode = 0
                objProjectIP.AddProContacts()
            End If

            sb_DisplayDetails()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            If ex.ToString().Contains("Contact already exist") Then
                DisplayError("Contact already exist for this Activity")
            Else
                DisplayError(CCommon.ToString(ex))
            End If

        End Try
    End Sub

    Sub createSet()
        Try
            dsTemp = New DataSet
            Dim dtAttendee As New DataTable
            dtAttendee.Columns.Add("vcContact")
            dtAttendee.Columns.Add("numContactID", System.Type.GetType("System.Int32"))

            dtAttendee.Columns.Add("vcCompanyname")
            dtAttendee.Columns.Add("vcStatus")

            dtAttendee.Columns.Add("tinUserType")
            dtAttendee.Columns.Add("ActivityID", System.Type.GetType("System.Int32"))

            'dtAttendee.Columns.Add("numDivisionID")
            dsTemp.Tables.Add(dtAttendee)

            dtAttendee.PrimaryKey = New DataColumn() {dsTemp.Tables(0).Columns("numContactId")}

            Session("AttendeeTable") = dsTemp
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub gvAttendee_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvAttendee.RowCommand
        If e.CommandName = "Delete" Then
            Try
                If AttendeesFor = 1 Then
                    If objActionItem Is Nothing Then objActionItem = New ActionItem
                    objActionItem.CommID = lngCommId
                    objActionItem.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
                    objActionItem.ContactID = e.CommandArgument
                    objActionItem.Type = 1
                    objActionItem.ManageCommunicationAttendees()
                ElseIf AttendeesFor = 2 Then
                    Dim objProjectIP As New ProjectIP
                    objProjectIP.DomainID = CCommon.ToLong(Session("DomainID"))
                    objProjectIP.ProjectID = lngProjectID
                    objProjectIP.ContactID = e.CommandArgument
                    objProjectIP.bytemode = 1
                    objProjectIP.AddProContacts()
                End If


                sb_DisplayDetails()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End If

    End Sub

    Protected Sub btnSendAlert_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSendAlert.Click
        Try
            For Each gvr As GridViewRow In gvAttendee.Rows
                If CType(gvr.FindControl("chkNotify"), CheckBox).Checked = True Then

                    Dim objSendEmail As New Email

                    objSendEmail.DomainID = Session("DomainID")
                    objSendEmail.TemplateCode = "#SYS#Notify_ActionItems_Attendees"
                    objSendEmail.ModuleID = 6
                    objSendEmail.RecordIds = lngCommId
                    objSendEmail.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
                    objSendEmail.FromEmail = Session("UserEmail")

                    Dim strToEmail As String = ""
                    objCommon = New CCommon
                    objCommon.ContactID = gvAttendee.DataKeys(gvr.RowIndex).Value
                    strToEmail = objCommon.GetContactsEmail()

                    objSendEmail.ToEmail = strToEmail

                    Dim QString As String = QueryEncryption.EncryptQueryString("EId=" & lngCommId & "&CId=" & gvAttendee.DataKeys(gvr.RowIndex).Value & "&D=" & Session("DomainID"))
                    objSendEmail.AdditionalMergeFields.Add("TicklerAcceptLink", "<a href='" & Session("PortalURL").ToString.Replace("Login.aspx", "Common/ActionItemsAttendees.aspx") & QString & "'>Accept/Decline Event</a>")
                    objSendEmail.AdditionalMergeFields.Add("TicklerCompanyContact", gvr.Cells(0).Text & "," & gvr.Cells(1).Text)

                    If strToEmail.Length > 2 Then objSendEmail.SendEmailTemplate()
                End If
            Next
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Private Sub gvAttendee_RowDeleted(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeletedEventArgs) Handles gvAttendee.RowDeleted

    End Sub

    Private Sub gvAttendee_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvAttendee.RowDeleting

    End Sub

    Private Sub DisplayError(ByVal errorMessage As String)
        Try
            DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = errorMessage
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Focus()
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub gvAttendee_RowDataBound(sender As Object, e As GridViewRowEventArgs)
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim drv As DataRowView = DirectCast(e.Row.DataItem, DataRowView)

                If Not e.Row.FindControl("lnkDelete") Is Nothing AndAlso CCommon.ToBool(drv("bitDefault")) Then
                    e.Row.FindControl("lnkDelete").Visible = False
                End If
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub
End Class