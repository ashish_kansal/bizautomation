'Modified  By :Anoop Jayaraj
Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Leads
Imports System.Net
Imports System.IO
Imports System.Xml.Serialization
Imports BACRM.BusinessLogic.Item
Imports Telerik.Web.UI

Namespace BACRM.UserInterface.Admin
    Public Class userdetails
        Inherits BACRMPage


#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region
        Dim lngUserID As Integer
        Dim lngGroupId As Long
        Dim objUserAccess As UserAccess


        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Load
            Try
                lngUserID = CCommon.ToLong(GetQueryStringVal("UserID"))   'Take the user id from the querystring

                hplSMTP.Attributes.Add("onclick", "return OpenSMTPPopUp(" & lngUserID & ",1)")
                hplImap.Attributes.Add("onclick", "return OpenSMTPPopUp(" & lngUserID & ",2)")


                If Not Page.IsPostBack Then

                    objUserAccess = New UserAccess
                    LoadDropdowns()
                    PopulateGroups()
                    If lngUserID > 0 Then
                        LoadInformation()
                    End If
                End If
                btnAdd.Attributes.Add("OnClick", "return move(document.getElementById('lstTerritory'),document.getElementById('lstTerritoryAdd'))")
                btnRemove.Attributes.Add("OnClick", "return remove1(document.getElementById('lstTerritoryAdd'),document.getElementById('lstTerritory'))")
                btnAddTeam.Attributes.Add("OnClick", "return move(document.getElementById('lstTeamAvail'),document.getElementById('lstTeamAdd'))")
                btnRemTeam.Attributes.Add("OnClick", "return remove1(document.getElementById('lstTeamAdd'),document.getElementById('lstTeamAvail'))")
                btnSaveClose.Attributes.Add("onclick", "return Save()")
                btnCancel.Attributes.Add("onclick", "return Close()")
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Sub PopulateGroups()
            Try
                objUserAccess.SelectedGroupTypes = "1,4"
                objUserAccess.DomainID = Session("DomainID")
                ddlGroups.DataTextField = "vcGroupName"
                ddlGroups.DataValueField = "numGroupID"
                ddlGroups.DataSource = objUserAccess.GetGroups
                ddlGroups.DataBind()
                ddlGroups.Items.Insert(0, New ListItem("--Select One--", "0"))
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub LoadDropdowns()
            Try
                objUserAccess.UserId = lngUserID
                objUserAccess.DomainID = Session("DomainID")
                ddlUserDetails.Items.Clear()
                ddlUserDetails.DataSource = objUserAccess.GetUserListForUserDetails()
                ddlUserDetails.DataTextField = "vcGivenName"
                ddlUserDetails.DataValueField = "numContactId"
                ddlUserDetails.DataBind()
                ddlUserDetails.Items.Insert(0, "--Select One--")
                ddlUserDetails.Items.FindByText("--Select One--").Value = 0
                objCommon.sb_FillListFromDB(lstTeamAvail, 35, Session("DomainID"))
                objCommon.sb_FillListFromDB(lstTerritory, 78, Session("DomainID"))
                CAdmin.sb_BindClassCombo(ddlClass, Session("DomainID"))

                Dim objItem As New CItems
                objItem.DomainID = Session("DomainID")
                ddlWarehouse.DataSource = objItem.GetWareHouses
                ddlWarehouse.DataTextField = "vcWareHouse"
                ddlWarehouse.DataValueField = "numWareHouseID"
                ddlWarehouse.DataBind()
                ddlWarehouse.Items.Insert(0, New ListItem("--Select One--", "0"))

                Dim objDashboardTemplate As New BACRM.BusinessLogic.CustomReports.DashboardTemplate
                objDashboardTemplate.DomainID = CCommon.ToLong(Session("DomainID"))
                'ddlDashboardTemplate.DataSource = objDashboardTemplate.GetAllByDomain()
                'ddlDashboardTemplate.DataTextField = "vcTemplateName"
                'ddlDashboardTemplate.DataValueField = "numTemplateID"
                'ddlDashboardTemplate.DataBind()
                'ddlDashboardTemplate.Items.Insert(0, New ListItem("--Select One--", "0"))

                rcbDashboardTemplate.DataSource = objDashboardTemplate.GetAllByDomain()
                rcbDashboardTemplate.DataTextField = "vcTemplateName"
                rcbDashboardTemplate.DataValueField = "numTemplateID"
                rcbDashboardTemplate.DataBind()
                'rcbDashboardTemplate.Items.Insert(0, New RadComboBoxItem("--Select One--", "0"))
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub LoadInformation()
            Try
                Dim dtUserAccessDetails As DataTable
                objUserAccess.UserId = lngUserID
                objUserAccess.DomainID = Session("DomainID")
                dtUserAccessDetails = objUserAccess.GetUserAccessDetails

                If dtUserAccessDetails.Rows.Count > 0 Then
                    If CCommon.ToString(dtUserAccessDetails.Rows(0).Item("vcSMTPServer")).ToLower().Contains("office365") Then
                        hplGoogleAuthenticate.Attributes.Add("onclick", "return Office365Authenticate()")
                    Else
                        hplGoogleAuthenticate.Attributes.Add("onclick", "return GoogleAuthenticate()")
                    End If

                    rblMailProvider.SelectedValue = CCommon.ToShort(dtUserAccessDetails.Rows(0)("tintMailProvider"))
                    chkOauthImap.Checked = CCommon.ToBool(dtUserAccessDetails.Rows(0)("bitOauthImap"))
                    If CCommon.ToShort(dtUserAccessDetails.Rows(0)("tintMailProvider")) = 3 Then
                        chkOauthImap.Style.Add("display", "none")
                        hplMail.Style.Add("display", "none")
                        hplSMTP.Style.Remove("display")
                        hplImap.Style.Remove("display")
                    ElseIf CCommon.ToShort(dtUserAccessDetails.Rows(0)("tintMailProvider")) = 2 Then
                        hplSMTP.Style.Add("display", "none")
                        hplImap.Style.Add("display", "none")
                        chkOauthImap.Style.Remove("display")
                        hplMail.Style.Remove("display")
                        hplMail.Attributes.Add("onclick", "return Office365MailAuthenticate()")
                        hplMail.Text = "Allow BizAutomation to access Office365 Mail (" & IIf(Not String.IsNullOrEmpty(dtUserAccessDetails.Rows(0)("vcMailAccessToken")) AndAlso Not String.IsNullOrEmpty(dtUserAccessDetails.Rows(0)("vcMailRefreshToken")), "Configured", "Not Configured") & ")"
                    Else
                        hplSMTP.Style.Add("display", "none")
                        hplImap.Style.Add("display", "none")
                        chkOauthImap.Style.Remove("display")
                        hplMail.Style.Remove("display")
                        hplMail.Attributes.Add("onclick", "return GoogleMailAuthenticate()")
                        hplMail.Text = "Allow BizAutomation to access Google Mail (" & IIf(Not String.IsNullOrEmpty(dtUserAccessDetails.Rows(0)("vcMailAccessToken")) AndAlso Not String.IsNullOrEmpty(dtUserAccessDetails.Rows(0)("vcMailRefreshToken")), "Configured", "Not Configured") & ")"
                    End If

                    Session("GoogleEmailId") = dtUserAccessDetails.Rows(0).Item("vcEmailId")
                    Session("GoogleContactId") = dtUserAccessDetails.Rows(0).Item("numUserDetailId")

                    txtEmailUser.Text = IIf(IsDBNull(dtUserAccessDetails.Rows(0).Item("vcEmailId")), "", dtUserAccessDetails.Rows(0).Item("vcEmailId"))
                    lngGroupId = IIf(IsDBNull(dtUserAccessDetails.Rows(0).Item("numGroupID")), 0, dtUserAccessDetails.Rows(0).Item("numGroupID"))
                    txtUserName.Text = dtUserAccessDetails.Rows(0).Item("vcUserName")
                    txtUserDesc.Text = dtUserAccessDetails.Rows(0).Item("vcUserDesc")
                    txtEmailID.Text = dtUserAccessDetails.Rows(0).Item("vcEmailID")
                    chkIsOnPayroll.Checked = CCommon.ToBool(dtUserAccessDetails.Rows(0).Item("bitPayroll"))
                    txtHourRate.Text = CCommon.ToDouble(dtUserAccessDetails.Rows(0).Item("monHourlyRate"))
                    txtOvertimeRate.Text = CCommon.ToDouble(dtUserAccessDetails.Rows(0).Item("monOverTimeRate"))
                    If CCommon.ToShort(dtUserAccessDetails.Rows(0).Item("tintPayrollType")) = 2 Then
                        rbPayrollSalary.Checked = True
                        rbPayrollHourly.Checked = False
                    Else
                        rbPayrollHourly.Checked = True
                        rbPayrollSalary.Checked = False
                    End If
                    If Not ddlHourType.Items.FindByValue(CCommon.ToShort(dtUserAccessDetails.Rows(0).Item("tintHourType"))) Is Nothing Then
                        ddlHourType.Items.FindByValue(CCommon.ToShort(dtUserAccessDetails.Rows(0).Item("tintHourType"))).Selected = True
                    End If
                    txtLinkedinId.Text = Convert.ToString(dtUserAccessDetails.Rows(0).Item("vcLinkedinId"))
                    If dtUserAccessDetails.Rows(0).Item("intAssociate") = 1 Then
                        chkRelation.Checked = True
                    Else : chkRelation.Checked = False
                    End If
                    If dtUserAccessDetails.Rows(0).Item("bitActivateFlag") = True Then
                        ddlStatus.SelectedIndex = 0
                    Else : ddlStatus.SelectedIndex = 1
                    End If

                    Dim strPassword As String
                    If Not String.IsNullOrEmpty(CCommon.ToString(dtUserAccessDetails.Rows(0).Item("vcPassword"))) Then
                        strPassword = objCommon.Decrypt(dtUserAccessDetails.Rows(0).Item("vcPassword"))
                    End If
                    txtPassword.Attributes("Value") = strPassword
                    If Not IsDBNull(dtUserAccessDetails.Rows(0).Item("numUserDetailId")) Then
                        If Not ddlUserDetails.Items.FindByValue(dtUserAccessDetails.Rows(0).Item("numUserDetailId")) Is Nothing Then
                            ddlUserDetails.Items.FindByValue(dtUserAccessDetails.Rows(0).Item("numUserDetailId")).Selected = True
                        End If
                    End If
                    If Not IsDBNull(dtUserAccessDetails.Rows(0).Item("numGroupID")) Then
                        If Not ddlGroups.Items.FindByValue(dtUserAccessDetails.Rows(0).Item("numGroupID")) Is Nothing Then
                            ddlGroups.Items.FindByValue(dtUserAccessDetails.Rows(0).Item("numGroupID")).Selected = True
                        End If
                    End If

                    hplSMTP.Text = "SMTP Configuration (Not Configured)"
                    hplImap.Text = "IMAP Configuration (Not Configured)"

                    If dtUserAccessDetails.Rows(0).Item("bitSMTPServer") Then
                        If IIf(IsDBNull(dtUserAccessDetails.Rows(0).Item("vcSMTPServer")), "", dtUserAccessDetails.Rows(0).Item("vcSMTPServer")) <> "" Then
                            hplSMTP.Text = "SMTP Configuration (Configured Successfully)"
                        End If
                    End If

                    If dtUserAccessDetails.Rows(0).Item("bitImap") = True Then
                        If IIf(IsDBNull(dtUserAccessDetails.Rows(0).Item("vcImapServerUrl")), "", dtUserAccessDetails.Rows(0).Item("vcImapServerUrl")) <> "" Then
                            hplImap.Text = "IMAP Configuration (Configured Successfully)"
                            hplGoogleAuthenticate.Visible = True
                        End If
                    End If
                    If Not IsDBNull(dtUserAccessDetails.Rows(0).Item("numDefaultClass")) Then
                        If Not ddlClass.Items.FindByValue(dtUserAccessDetails.Rows(0).Item("numDefaultClass")) Is Nothing Then
                            ddlClass.ClearSelection()
                            ddlClass.Items.FindByValue(dtUserAccessDetails.Rows(0).Item("numDefaultClass")).Selected = True
                        End If
                    End If

                    If Not IsDBNull(dtUserAccessDetails.Rows(0).Item("numDefaultWarehouse")) Then
                        If Not ddlWarehouse.Items.FindByValue(dtUserAccessDetails.Rows(0).Item("numDefaultWarehouse")) Is Nothing Then
                            ddlWarehouse.ClearSelection()
                            ddlWarehouse.Items.FindByValue(dtUserAccessDetails.Rows(0).Item("numDefaultWarehouse")).Selected = True
                        End If
                    End If

                    'If Not ddlDashboardTemplate.Items.FindByValue(CCommon.ToLong(dtUserAccessDetails.Rows(0).Item("numDashboardTemplateID"))) Is Nothing Then
                    '    ddlDashboardTemplate.Items.FindByValue(CCommon.ToLong(dtUserAccessDetails.Rows(0).Item("numDashboardTemplateID"))).Selected = True
                    'End If

                    For Each item As RadComboBoxItem In rcbDashboardTemplate.Items
                        If ("," & CCommon.ToString(dtUserAccessDetails.Rows(0)("vcDashboardTemplateIDs")) & ",").Contains(item.Value) Then
                            item.Checked = True
                        End If
                    Next

                End If

                objUserAccess.UserCntID = ddlUserDetails.SelectedValue
                lstTerritoryAdd.DataSource = objUserAccess.GetTerritoryForUsers
                lstTerritoryAdd.DataTextField = "vcTerName"
                lstTerritoryAdd.DataValueField = "numTerritoryID"
                lstTerritoryAdd.DataBind()

                lstTeamAdd.DataSource = objUserAccess.GetTeamForUsers
                lstTeamAdd.DataTextField = "vcData"
                lstTeamAdd.DataValueField = "numlistitemid"
                lstTeamAdd.DataBind()
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub btnSaveClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveClose.Click
            Try
                If String.IsNullOrEmpty(txtEmailID.Text.Trim()) Or String.IsNullOrEmpty(txtPassword.Text) Or CCommon.ToLong(ddlUserDetails.SelectedValue) = 0 Or CCommon.ToLong(ddlGroups.SelectedValue) = 0 Then
                    litMessage.Text = "Use, Email, Password and Permission Group are required."
                Else
                    Save()
                    Response.Write("<script>self.close();</script>")
                End If
            Catch ex As Exception
                If ex.Message.Contains("FULL_USERS_EXCEED") Then
                    ScriptManager.RegisterClientScriptBlock(Me, Me.Page.GetType(), "FullUsersExceed", "alert('You have exceeded the number of full subscriber licenses. You can either unsubscribe existing licenses and replace them with new ones (i.e. swap one for another), or buy more full access licenses from BizAutomation.')", True)
                ElseIf ex.Message.Contains("LIMITED_ACCESS_USERS_EXCEED") Then
                    ScriptManager.RegisterClientScriptBlock(Me, Me.Page.GetType(), "LimitedAccessUsersExceed", "alert('You have exceeded the number of limited access subscriber licenses. You can either unsubscribe existing licenses and replace them with new ones (i.e. swap one for another), or buy more limited access licenses from BizAutomation.')", True)
                Else
                    ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                    Response.Write(ex)
                End If
            End Try
        End Sub

        Sub Save()
            Try
                objUserAccess = New UserAccess
                objUserAccess.DomainID = Session("DomainID")
                If ddlUserDetails.SelectedIndex < 1 Then
                    Dim objLeads As New CLeads
                    With objLeads
                        .DivisionID = objUserAccess.GetDivisionFromDomain
                        .FirstName = txtUserName.Text
                        .LastName = ""
                        .Email = txtEmailID.Text
                        .DomainID = Session("DomainID")
                        .UserCntID = Session("UserContactID")
                        objUserAccess.ContactID = .CreateRecordAddContactInfo
                    End With
                Else : objUserAccess.ContactID = ddlUserDetails.SelectedItem.Value
                End If

                objUserAccess.UserName = txtUserName.Text
                objUserAccess.UserDesc = txtUserDesc.Text
                objUserAccess.GroupID = ddlGroups.SelectedItem.Value
                objUserAccess.UserId = lngUserID
                If ddlStatus.SelectedIndex = 0 Then
                    objUserAccess.AccessAllowed = 1
                Else : objUserAccess.AccessAllowed = 0
                End If
                If ddlUserDetails.SelectedIndex > 0 Then
                    objUserAccess.strTerritory = hdnValue.Text.Trim
                    objUserAccess.strTeam = txtTeamValue.Text.Trim
                    'objUserAccess.strEmail = txtEmail.Text.Trim
                End If

                objUserAccess.UserCntID = Session("UserContactID")
                objUserAccess.Email = txtEmailID.Text
                objUserAccess.IsOnPayroll = chkIsOnPayroll.Checked
                objUserAccess.HourlyRate = CCommon.ToDouble(txtHourRate.Text)
                objUserAccess.OverTimeRate = CCommon.ToDouble(txtOvertimeRate.Text)
                objUserAccess.PayrollType = If(rbPayrollSalary.Checked, 2, 1)
                objUserAccess.HourType = ddlHourType.SelectedValue
                Dim plainText As String
                Dim cipherText As String
                plainText = txtPassword.Text
                cipherText = objCommon.Encrypt(plainText)
                objUserAccess.Password = cipherText
                objUserAccess.DefaultClass = ddlClass.SelectedValue
                objUserAccess.DefaultWarehouse = ddlWarehouse.SelectedValue
                objUserAccess.LinkedinId = txtLinkedinId.Text                
                objUserAccess.OAuthImap = chkOauthImap.Checked
                If chkRelation.Checked = True Then
                    objUserAccess.intAssociate = 1
                Else : objUserAccess.intAssociate = 0
                End If
                objUserAccess.MailProvider = rblMailProvider.SelectedValue
                objUserAccess.UpdateUserAccessDetails()
                Session("DefaultClass") = objUserAccess.DefaultClass
                Session("DefaultWarehouse") = objUserAccess.DefaultWarehouse

            Catch ex As Exception
                Throw
            End Try
        End Sub

        Protected Sub btnUpld_Click(sender As Object, e As EventArgs) Handles btnUpld.Click

        End Sub

        Private Sub btnUpdateDashboardTemplate_Click(sender As Object, e As EventArgs) Handles btnUpdateDashboardTemplate.Click
            Try
                objUserAccess = New UserAccess
                objUserAccess.DomainID = CCommon.ToLong(Session("DomainID"))
                objUserAccess.UserId = lngUserID

                Dim vcDashboardTemplateIDs As String = "", dashboardTemplateID As String = ""

                If rcbDashboardTemplate.CheckedItems.Count > 0 Then

                    For Each item As RadComboBoxItem In rcbDashboardTemplate.CheckedItems
                        dashboardTemplateID = If(dashboardTemplateID = "", item.Value, item.Value)
                        vcDashboardTemplateIDs += If(vcDashboardTemplateIDs = "", item.Value, "," & item.Value)
                    Next

                End If

                objUserAccess.UpdateDefaultDashboardTemplate(CCommon.ToLong(dashboardTemplateID), vcDashboardTemplateIDs)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub
    End Class
End Namespace