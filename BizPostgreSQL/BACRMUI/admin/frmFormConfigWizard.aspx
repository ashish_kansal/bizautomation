<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" CodeBehind="frmFormConfigWizard.aspx.vb"
    Inherits="BACRM.UserInterface.Admin.frmFormConfigWizard" ValidateRequest="false"
    MasterPageFile="~/common/GridMasterRegular.Master" %>
<%@ Register Assembly="Telerik.Web.UI, Version=2011.2.712.40, Culture=neutral, PublicKeyToken=121fae78165ba3d4" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Form Configuration Wizard </title>
    <script type="text/javascript" src="../javascript/FormConfigWizard.js"></script>
    
    <link rel="stylesheet" href="../CSS/lists.css" type="text/css" />
    <script language="JavaScript" type="text/javascript" src="../javascript/coordinates.js"></script>
    <script language="JavaScript" type="text/javascript" src="../javascript/drag.js"></script>
    <script language="JavaScript" type="text/javascript" src="../javascript/dragdrop.js"></script>
    <script type="text/javascript">
        function OpenAtch() {
            window.open("../opportunity/frmBizDocAttachments.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&BizDocID=" + $("[id$=ddlBizDoc]").val() + "&OppType=" + ($("[id$=ddlFormList]").val() == 7 ? 1 : 2) + "&TempID=" + $("[id$=ddlBizDocTemplate]").val(), "", "width=800,height=400,status=no,scrollbars=yes,left=155,top=160");
            return false;
        }
        function OpenEditor(URL) {
            window.open(URL, "", "width=800,height=400,status=no,scrollbars=yes,left=155,top=160");
            return false;
        }
        function validateSubform() {
            var formName = $("#<%#txtFormName.ClientID%>").val();
            if (formName == "" || formName.length < 3) {
                alert("Please enter a valid form name having 3 min charecter");
                $("#<%#txtFormName.ClientID%>").focus();
                return false;
            } else {
                return true;
            }
        }
        function deletevalidateSubform() {
            var formName = $("#<%#ddlLeadFormList.ClientID%> option:selected").val();
            if (parseInt(formName) <= 0) {
                alert("Please Select form");
                return false;
            }
        }
        function OpenTaxItem() {
            window.open('../admin2/frmTaxWizard.aspx', "", "width=800,height=400,status=no,scrollbars=yes,left=155,top=160");
            return false;
        }
        function OpenLeadbox(URL) {
            window.open(URL, "", "");
            return false;
        }


        function OpenValidation(a, b, c) {
            a = $("#<%=ddlFormList.ClientID%> option:selected").val();
                b = $("#<%=lstSelectedfldOne.ClientID%> option:selected").val().replace(/[^\d]/g, "");
                window.open("../admin/frmFormFieldValidationSet.aspx?FormId=" + a + "&fldid=" + b + "&FType=" + c, '', 'width=400,height=280,status=no,titlebar=no,resizable=0,scrollbar=yes,top=110,left=150')
                return false;
            }
            function GenerateHtmlForm(Form, SubForm) {
                var enableLinkedin = 0;
                var chkUseinIframe = 0;
                if ($("#chkEnableLinkedin").length) {
                    if (document.getElementById('chkEnableLinkedin').checked) {
                        enableLinkedin = 1
                    }
                }
                if ($("#chkUseinIframe").length) {
                    if (document.getElementById('chkUseinIframe').checked) {
                        chkUseinIframe = 1
                    }
                }
                window.open('../admin/frmGenerateHtmlForm.aspx?OpenPopup=' + chkUseinIframe + '&SubForm=' + SubForm + '&EnableLinkedin=' + enableLinkedin + '&frmId=' + Form, "", "width=800,height=400,status=no,scrollbars=yes,left=155,top=160");
                return false;
            }
            function OpenHelp() {
                window.open('../Help/Admin-BizForms_Wizard.html', '', 'toolbar=no,titlebar=no,top=200,left=200,width=550,height=350,scrollbars=yes,resizable=yes')
                return false
            }
           
            function Confirm() {
                if (validateFormData(document.getElementById("form1"))) {
                    var moduleid = $("#ddlBizFormModule").val();
                    var formid = $("#ddlFormList").val();
                    var formName = $("#ddlFormList option:selected").text()
                    if (moduleid == "1" || moduleid == "2" || moduleid == "4" || (moduleid == "3" && (formid != 7 && formid != 8 && formid != 58)) || (moduleid = 5 && formid != 67))
                    {                       
                        var mesg = "Layout or column configured by users belonging to this permission group will be overridden. Are you sure you want to do this?"
                        if ($("#chkGlobalSetting").is(':checked') == true)
                        {                            
                            if (formid == "56" || formid == "99" || formid == "100" || formid == "101")
                                mesg = "This will replace existing configurations for all users and relationships for " + formName + " form. Are you sure you want to do this ?"
                            else if (formid == "1" || formid == "15" || formid == "17" || formid == "18" || formid == "29" || formid == "30" || formid == "59" || formid == "101" || formid == "103" || formid == "104"
                                || formid == "105" || formid == "106" || formid == "107" || formid == "108" || formid == "109" || formid == "110" || formid == "111" || formid == "112" || formid == "113" || formid == "115" || formid == "116"
                                || formid == "117" || formid == "118" || formid == "119" || formid == "120" || formid == "121" || formid == "122" || formid == "130")
                                mesg = "This will replace existing configurations for all users for " + formName + " form. Are you sure you want to do this ?"
                            else if (formid == "102" || formid == "114")
                                mesg = "This will replace existing configurations for all users and contact types for " + formName + " form. Are you sure you want to do this ?"                                                 
                        }  
                        if (confirm(mesg))
                            return true;
                        else
                            return false;   
                    }
                    else
                    {
                        return true;
                    }
                }
                else
                {
                    return false;
                }
        }
        function GroupModalForSorting(bitAlphabetical) {
            var DomainID = '<%= Session("DomainId")%>';
            var dataParam = "{DomainID:'" + DomainID + "',FormId:'" + $("#ddlFormList option:selected").val() + "',GroupID:'0',IsAlphabetical:'" + bitAlphabetical + "'}";
            $("#UpdateProgress").css("display", "block");
            $("#divSortGroupDetails").html('');
            var sortItemAppend = '';
            sortItemAppend = sortItemAppend + "<ul id='x' class='sortable boxy'>";
            $.ajax({
                type: "POST",
                url: "../admin/frmFormConfigWizard.aspx/WebMethodFetchSortGroup",
                data: dataParam,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    $("#UpdateProgress").css("display", "none");
                    var Jresponse = $.parseJSON(response.d);
                    var itemAppend = '';
                    $.each(Jresponse, function (index, value) {
                        sortItemAppend = sortItemAppend + "<li id='" + value.numFormFieldGroupId + "'>" + value.vcGroupName + "</li>";
                    });
                    sortItemAppend = sortItemAppend + "</ul>";
                    $("#divSortGroupDetails").append(sortItemAppend);
                    debugger;
                    if (document.getElementById("x") != null) {
                        var list = document.getElementById("x");
                        DragDrop.makeListContainer(list, 'g2');
                        list.onDragOver = function () { this.style["background"] = "none"; };
                        list.onDragOut = function () { this.style["background"] = "none"; };
                    }
                    $("#sortGroupModal").modal("show");
                }
            });
        }
        function SaveSortGroupModal() {
            var DomainID = '<%= Session("DomainId")%>';
            var sortTask = DragDrop.serData('g2', null);
            var dataParam = "{DomainID:'" + DomainID + "',FormId:'" + $("#ddlFormList option:selected").val() + "',sortGroup:'" + sortTask +"',GroupID:0}";
            
            $("#UpdateProgress").css("display", "block");
            $.ajax({
                type: "POST",
                url: "../admin/frmFormConfigWizard.aspx/WebMethodSortGroup",
                data: dataParam,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    $("#UpdateProgress").css("display", "none");

                }
            })
        }
        function CheckAllGroups(chkAll) {
            var Combo = $find("<%= rcbGroups.ClientID%>");
            var items = Combo.get_items();

            for (var x = 0; x < Combo.get_items().get_count() ; x++) {
                if ($(chkAll).is(':checked'))
                    items._array[x].check();
                else
                    items._array[x].uncheck();
            }
        }
        function Column1SelectionChanged() {
            $("[id$=lstAvailablefld]").prop('selectedIndex', -1);
            $("[id$=lstSelectedfldTwo]").prop('selectedIndex', -1);
            $("[id$=lstSelectedfldThree]").prop('selectedIndex', -1);
        }
        function Column2SelectionChanged() {
            $("[id$=lstAvailablefld]").prop('selectedIndex', -1);
            $("[id$=lstSelectedfldOne]").prop('selectedIndex', -1);
            $("[id$=lstSelectedfldThree]").prop('selectedIndex', -1);
        }
        function Column3SelectionChanged() {
            $("[id$=lstAvailablefld]").prop('selectedIndex', -1);
            $("[id$=lstSelectedfldTwo]").prop('selectedIndex', -1);
            $("[id$=lstSelectedfldOne]").prop('selectedIndex', -1);
        }
        function AvailablefldelectionChanged() {
            $("[id$=lstSelectedfldOne]").prop('selectedIndex', -1);
            $("[id$=lstSelectedfldTwo]").prop('selectedIndex', -1);
            $("[id$=lstSelectedfldThree]").prop('selectedIndex', -1);
        }
    </script>
    <style>
        .tblNoBorder td, .tblNoBorder th {
            border: 0px solid #fff !important;
        }

        @media (max-width:40em) {
            .tblNoBorder table, .tblNoBorder thead, .tblNoBorder tbody, .tblNoBorder tfoot, .tblNoBorder th, .tblNoBorder td, .tblNoBorder tr {
                display: block;
            }

            .tblNoBorder .editable_select {
                min-height: 30px;
            }

            .tblNoBorder td, .tblNoBorder th {
                text-align: left;
            }
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server"
    ClientIDMode="Static">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <asp:Label ID="lblError" runat="server" CssClass="text-center" ForeColor="Red"> </asp:Label>
    <div class="row padbottom10">
        <div class="col-xs-12">
            <div class="pull-left">
                <div class="form-inline">
                    <div class="form-group">
                        <label>Module:</label>
                        <asp:DropDownList ID="ddlBizFormModule" runat="server" AutoPostBack="True" CssClass="form-control" Width="240">
                        </asp:DropDownList>
                    </div>
                     &nbsp;&nbsp;
                    <div id="divFormFilter" runat="server" visible="false" class="form-group">
                        <asp:RadioButtonList ID="rblFormType" runat="server" AutoPostBack="true" OnSelectedIndexChanged="rblFormType_SelectedIndexChanged">
                            <asp:ListItem Value="3" Text="Field Layout (Page Details)"></asp:ListItem>
                            <asp:ListItem Value="1" Text="Grid Column Layout"></asp:ListItem>
                        </asp:RadioButtonList>
                    </div>
                    &nbsp;&nbsp;
                    <div class="form-group">
                        <label>Form:</label>
                        <asp:DropDownList ID="ddlFormList" runat="server" AutoPostBack="True" CssClass="form-control">
                        </asp:DropDownList>
                        <asp:Label ID="lblTip" runat="server" Text="[?]" CssClass="tip" ToolTip="Global grid settings will be overridden if within the grid columns are set individually (these are set by clicking the gear icon found on the corner of the grid view page)." Visible="false"></asp:Label>
                    </div>                   
                    <div id="tdUserGroups" runat="server" visible="false" class="form-group">
                        &nbsp;&nbsp;
                            <div class="form-group">
                                <label>User Group:</label>
                               <asp:DropDownList ID="ddlUserGroup" runat="server" CssClass="form-control" AutoPostBack="true">
                                </asp:DropDownList>
                            </div>
                        &nbsp;&nbsp;
                        <div class="form-group" id="pnlContact" runat="server" visible="false">
                            <div class="form-group">
                                <label>Contact Type:</label>
                                <asp:DropDownList ID="ddlContactType" runat="server" CssClass="form-control" AutoPostBack="true">
                                </asp:DropDownList>
                            </div>
                        </div>
                        &nbsp;&nbsp;
                        <div class="form-group">
                            <label>Apply to other user groups</label>
                            <telerik:RadComboBox ID="rcbGroups" runat="server" CheckBoxes="true" EmptyMessage="Select User Groups">
                                <FooterTemplate>
                                    <asp:CheckBox runat="server" ID="chkAll" onclick="CheckAllGroups(this);" Text="Select All" />
                                </FooterTemplate>
                            </telerik:RadComboBox>
                        </div>
                        &nbsp;&nbsp;
                        <asp:CheckBox ID="chkGlobalSetting" Checked="false" Text="Apply to all Relationships" runat="server" Visible="false" />
                    </div>
                    <div class="form-group" id="divGroup" runat="server">
                        <label runat="server" id="lblGroupDdlLable">Group:</label>
                        <asp:DropDownList ID="ddlGroup" runat="server" CssClass="form-control" AutoPostBack="true">
                        </asp:DropDownList>
                    </div>
                    <div id="tdBizDocs" runat="server" class="form-group">
                        <div class="form-group">
                            <label>Bizdoc:</label>
                            <asp:DropDownList ID="ddlBizDoc" runat="server" CssClass="form-control" AutoPostBack="true">
                            </asp:DropDownList>
                        </div>
                        <div class="form-group">
                            <label>Template:</label>
                            <asp:DropDownList ID="ddlBizDocTemplate" runat="server" CssClass="form-control" AutoPostBack="true">
                            </asp:DropDownList>
                            <asp:HyperLink ID="hplBizDoc" runat="server" CssClass="hyperlink">Attachments</asp:HyperLink>
                        </div>
                    </div>
                    <div id="divBizDoc" runat="server" style="float:right;">
                        <label>&nbsp;BizDoc ID: </label>
                        <asp:Label runat="server" ID="lblBizDocId" CssClass="normal4"></asp:Label>
                    </div>
                    <div class="form-group" id="tdSurvey" runat="server" visible="false">
                        <label>Map registration configuration to the following survey:</label>
                        <asp:DropDownList ID="ddlSurvey" runat="server" CssClass="form-control" AutoPostBack="true">
                        </asp:DropDownList>
                        <asp:Label ID="lblSurveyMessage" runat="server" CssClass="normal4"> </asp:Label>
                    </div>
                    <div class="form-group" id="divlitDynamicFormHeaderText" runat="server" visible="false">
                        <label></label>
                        <div>
                            <asp:Literal ID="litDynamicFormHeaderText" runat="server" Visible="False"></asp:Literal>
                        </div>
                    </div>
                    <div class="form-group" runat="server" id="divplhDynamicFormHeaders" visible="false">
                        <label></label>
                        <div>
                            <asp:PlaceHolder ID="plhDynamicFormHeaders" runat="server" Visible="False"></asp:PlaceHolder>
                        </div>
                    </div>
                    <div class="form-group" runat="server" id="divlitCustomFieldPopupLinker" visible="false">
                        <label></label>
                        <div>
                            <asp:Literal ID="litCustomFieldPopupLinker" runat="server" Visible="False" EnableViewState="False"></asp:Literal>
                        </div>
                    </div>
                    <div class="form-group" id="divchkEnableLinkedin" runat="server" visible="false">
                        <label></label>
                        <asp:CheckBox ID="chkEnableLinkedin" Checked="false" Text="Allow Populate From Linkedin" runat="server" />
                    </div>
                    <div class="form-group" id="divbtnGenerateHtml" runat="server">
                        <label></label>
                        <asp:Button ID="btnGenerateHtml" Text="HTML Form" runat="server" CssClass="btn btn-primary" CausesValidation="False" Visible="true"></asp:Button>
                    </div>
                    <div style="margin-top: 10px;">
                        <div class="form-group" id="divAddForm" runat="server">
                            <label>Form Name</label>
                            <asp:TextBox ID="txtFormName" Style="max-width: 80px;" CssClass="form-control" runat="server"></asp:TextBox>
                            <asp:LinkButton ID="btnAddForm" OnClick="btnAddForm_Click" OnClientClick="return validateSubform()" CssClass="btn btn-sm btn-primary" runat="server"><i class="fa fa-plus-circle" aria-hidden="true"></i></asp:LinkButton>
                            <asp:LinkButton ID="btnDeleteForm" OnClick="btnDeleteForm_Click" OnClientClick="deletevalidateSubform()" CssClass="btn btn-sm btn-danger" runat="server"><i class="fa fa-trash" aria-hidden="true"></i></asp:LinkButton>
                        </div>
                        <div class="form-group" id="divlistForm" runat="server">
                            <label>Form Name : </label>
                            <asp:DropDownList ID="ddlLeadFormList" AutoPostBack="true" OnSelectedIndexChanged="ddlLeadFormList_SelectedIndexChanged" CssClass="form-control" runat="server"></asp:DropDownList>
                        </div>
                        <div class="form-group" id="divAssigntoForm" runat="server">
                            <asp:CheckBox ID="chkAssignToFrom" runat="server" />
                            <label style="max-width: 120px">Assign to</label>
                            <asp:DropDownList ID="ddlAssignToForm" CssClass="form-control" Style="margin-top: 0px;" runat="server"></asp:DropDownList>
                        </div>
                        <div class="form-group" id="divDripCampaign" runat="server">

                            <asp:CheckBox ID="chkDripCampaign" Text="" runat="server" />
                            <label style="max-width: 200px">On record creation assign to the following Automated Follow-up campaign</label>
                            <asp:DropDownList ID="ddlDripCampaign" CssClass="form-control" Style="margin-top: -20px;" runat="server"></asp:DropDownList>
                        </div>
                        <div class="form-group" id="iframeRedirectionDiv" runat="server">
                            <asp:CheckBox ID="chkUseinIframe" Text="" runat="server" />
                            <label style="max-width: 100px">Redirect from Iframe</label>
                        </div>
                    </div>
                    <%--<div class="form-group" id="divbtnCreateTaxItem" runat="server">
                            <label></label>
                            <asp:Button ID="btnCreateTaxItem" Text="Create Tax Item" runat="server" CssClass="btn btn-primary" CausesValidation="False"></asp:Button>
                        </div>--%>
                </div>
            </div>
            
            <div class="pull-right">
                <asp:DropDownList ID="ddlRelationShip" runat="server" CssClass="form-control" AutoPostBack="true" Visible="false"></asp:DropDownList>
                <input type="hidden" runat="server" id="hdXMLString" value="" name="hdXMLString" />
                <input type="hidden" runat="server" id="hdSaveConfigurationStatus" value="No" name="hdSaveConfigurationStatus" />
                <input type="hidden" runat="server" id="hdFormAdditionalParam" value="" name="hdFormAdditionalParam" />
                <input type="hidden" runat="server" id="hdnAOI" value="" name="hdnAOI" />
                <input type="hidden" runat="server" id="hdXMLPath" value="" name="hdXMLPath" />
                <input type="hidden" runat="server" id="hdBizDocSumm" value="" name="hdBizDocSumm" />
                <asp:UpdatePanel ID="updatesave" runat="server" style="display: inline">
                    <ContentTemplate>
                        <asp:Button ID="btnSaveConfiguration" runat="server" OnClientClick="return Confirm();" CssClass="btn btn-primary" Style="font-family: 'Source Sans Pro', 'Helvetica Neue', Helvetica, Arial, sans-serif, FontAwesome;" CausesValidation="False" Text="&#xf0c7;&nbsp;&nbsp;Save"></asp:Button>
                    </ContentTemplate>
                    <Triggers>
                        <asp:PostBackTrigger ControlID="btnSaveConfiguration" />
                    </Triggers>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server" ClientIDMode="Static">
    BizForm Wizard&nbsp;<a href="#" onclick="return OpenHelpPopUp('admin/frmFormConfigWizard.aspx')"><label class="badge bg-yellow">?</label></a>     
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BizPager" runat="server" ClientIDMode="Static">
  <div class="form-inline" id="divFormFieldGroup" runat="server" visible="false">
      <label>Field Group : </label>
                        <asp:DropDownList ID="ddlFieldGroupId" AutoPostBack="true" OnSelectedIndexChanged="ddlFieldGroupId_SelectedIndexChanged" CssClass="form-control" runat="server">
                            <asp:ListItem Value="0">-ALL-</asp:ListItem>
                        </asp:DropDownList>
                        <asp:TextBox ID="txtFieldGroupName" CssClass="form-control" runat="server"></asp:TextBox>
                        <asp:Button ID="btnAddFieldGroup" OnClick="btnAddFieldGroup_Click" runat="server" Text="Add" CssClass="btn btn-sm btn-primary" />
      <a href="#" onclick="return GroupModalForSorting(false);" class="btn btn-sm btn-primary"><i class="fa fa-sort"></i>&nbsp;&nbsp;Sort</a>
                        <asp:Button ID="btnDeleteFieldGroup" OnClick="btnDeleteFieldGroup_Click" runat="server" Text="Remove" CssClass="btn btn-sm btn-danger" />
                    </div>
        <div id="sortGroupModal" class="modal fade" role="dialog">
        <div class="modal-dialog modal-md" style="width: 40%">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Gorup Name Sorting</h4>
                </div>
                <div class="modal-body">

                    <div class="col-md-12" id="divSortGroupDetails">
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" onclick="GroupModalForSorting(true)">Sort Alphabetically</button>
                    <button type="button" class="btn btn-primary" onclick="SaveSortGroupModal()">Save</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="GridPlaceHolder" runat="server"
    ClientIDMode="Static">
    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
        <ContentTemplate>
            <div class="box box-solid box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">
                        <asp:Label ID="lblHeader" runat="server" Text="Fields Configuration"></asp:Label></h3>
                   
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-xs-12">
                            <table class="table table-responsive tblNoBorder">
                                <tr id="MainRow" runat="server">
                                    <td align="center" class="normal1" valign="top"><b>Available Fields</b><br>
                                        <asp:ListBox ID="lstAvailablefld" runat="server" Height="200" CssClass="form-control"
                                            EnableViewState="False" onchange="AvailablefldelectionChanged();"></asp:ListBox>
                                        &nbsp;&nbsp;&nbsp;
                                    </td>
                                    <td align="center" valign="middle" width="90px">
                                        <table style="margin-top: 20px">
                                            <tr>
                                                <td style="height: 30px; text-align: center; vertical-align: middle">
                                                    <a id="btnAddOne" class="btn btn-primary"
                                                        onclick="javascript: move(document.getElementById('lstAvailablefld'), document.getElementById('lstSelectedfldOne'), 1, 0); move(document.getElementById('lstSelectedfldTwo'), document.getElementById('lstSelectedfldOne'), 1, 0); move(document.getElementById('lstSelectedfldThree'), document.getElementById('lstSelectedfldOne'), 1, 0);">Add to 1&nbsp;&nbsp;<i class="fa fa-arrow-right"></i></a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="height: 5px; text-align: center; vertical-align: middle"></td>
                                            </tr>
                                            <tr>
                                                <td style="height: 30px; text-align: center; vertical-align: middle">
                                                    <a id="btnAddTwo" runat="server" class="btn btn-primary"
                                                        onclick="javascript: move(document.getElementById('lstAvailablefld'), document.getElementById('lstSelectedfldTwo'), 2, 0); move(document.getElementById('lstSelectedfldOne'), document.getElementById('lstSelectedfldTwo'), 2, 0); move(document.getElementById('lstSelectedfldThree'), document.getElementById('lstSelectedfldTwo'), 2, 0);">Add to 2&nbsp;&nbsp;<i class="fa fa-arrow-right"></i></a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="height: 5px; text-align: center; vertical-align: middle"></td>
                                            </tr>
                                            <tr>
                                                <td style="height: 30px; text-align: center; vertical-align: middle">
                                                    <a id="btnAddThree" runat="server" class="btn btn-primary"
                                                        onclick="javascript: move(document.getElementById('lstAvailablefld'), document.getElementById('lstSelectedfldThree'), 3, 0); move(document.getElementById('lstSelectedfldOne'), document.getElementById('lstSelectedfldThree'), 3, 0); move(document.getElementById('lstSelectedfldTwo'), document.getElementById('lstSelectedfldThree'), 3, 0);">Add to 3&nbsp;&nbsp;<i class="fa fa-arrow-right"></i></a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="height: 5px; text-align: center; vertical-align: middle"></td>
                                            </tr>
                                            <tr>
                                                <td style="height: 5px; text-align: center; vertical-align: middle"></td>
                                            </tr>
                                            <tr>
                                                <td style="height: 30px; text-align: center; vertical-align: middle">
                                                    <a id="btnRemoveOne" class="btn btn-danger"
                                                        onclick="javascript: remove1(document.getElementById('lstSelectedfldOne'), document.getElementById('lstAvailablefld'), 0)"><i class="fa fa-arrow-left"></i>&nbsp;&nbsp;Remove</a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="height: 45px; text-align: center; vertical-align: middle">
                                                    <a id="btnValidateOne" runat="server" class="btn btn-success"
                                                        onclick="javascript: OpenValidation(0, document.getElementById('lstAvailablefld'), 0)"></i>&nbsp;&nbsp;Required</a>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td align="center" class="normal1"><asp:Label runat="server" ID="lblColumn1" Text="Added to column 1" Font-Bold="true"></asp:Label><asp:Label runat="server" ID="lblColumn1Note" Text="(Top Downward displays Left to Right)" Font-Italic="true" Visible="false"></asp:Label><br>
                                        
                                        <asp:ListBox ID="lstSelectedfldOne" runat="server" Height="200" CssClass="form-control" onchange="Column1SelectionChanged()"
                                            EnableViewState="False"></asp:ListBox>
                                    </td>
                                    <td align="center" valign="middle" width="40px">
                                        <table style="margin-top: 20px">
                                            <tr>
                                                <td style="height: 30px; text-align: center; vertical-align: middle">
                                                    <a id="btnMoveupOne" class="btn btn-primary" onclick="javascript:MoveUp(document.getElementById('lstSelectedfldOne'),0)"><i class="fa fa-arrow-up"></i></a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="height: 10px; text-align: center; vertical-align: middle"></td>
                                            </tr>
                                            <tr>
                                                <td style="height: 30px; text-align: center; vertical-align: middle">
                                                    <a id="btnMoveDownOne" class="btn btn-primary" onclick="javascript:MoveDown(document.getElementById('lstSelectedfldOne'),0)"><i class="fa fa-arrow-down"></i></a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="height: 10px; text-align: center; vertical-align: middle"></td>
                                            </tr>
                                            <tr>
                                                <td style="height: 10px; text-align: center; vertical-align: middle"></td>
                                            </tr>
                                            <tr>
                                                <td style="height: 10px; text-align: center; vertical-align: middle"></td>
                                            </tr>
                                            <tr>
                                                <td style="height: 10px; text-align: center; vertical-align: middle"></td>
                                            </tr>
                                            <tr>
                                                <td style="height: 30px; text-align: center; vertical-align: middle">
                                                    <a id="btnRemoveTwo" class="btn btn-danger" value="< Remove" style="width: 65;"
                                                        runat="server" onclick="javascript: remove1(document.getElementById('lstSelectedfldTwo'), document.getElementById('lstAvailablefld'), 0)"><i class="fa fa-arrow-left"></i>&nbsp;&nbsp;Remove</a>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td align="center" class="normal1" id="trTwo" runat="server"><b>Added to column 2</b><br>
                                        <asp:ListBox ID="lstSelectedfldTwo" runat="server" Height="200" CssClass="form-control" onchange="Column1SelectionChanged()"
                                            EnableViewState="False"></asp:ListBox>
                                    </td>
                                    <td align="center" valign="middle" id="trTwoButton" runat="server" width="90px">
                                        <table style="margin-top: 20px">
                                            <tr>
                                                <td style="height: 30px; text-align: center; vertical-align: middle">
                                                    <a id="btnMoveupTwo" class="btn btn-primary" onclick="javascript:MoveUp(document.getElementById('lstSelectedfldTwo'),0)"><i class="fa fa-arrow-up"></i></a>

                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="height: 10px; text-align: center; vertical-align: middle"></td>
                                            </tr>
                                            <tr>
                                                <td style="height: 30px; text-align: center; vertical-align: middle">
                                                    <a id="btnMoveDownTwo" class="btn btn-primary" onclick="javascript:MoveDown(document.getElementById('lstSelectedfldTwo'),0)"><i class="fa fa-arrow-down"></i></a>

                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="height: 10px; text-align: center; vertical-align: middle"></td>
                                            </tr>
                                            <tr>
                                                <td style="height: 10px; text-align: center; vertical-align: middle"></td>
                                            </tr>
                                            <tr>
                                                <td style="height: 10px; text-align: center; vertical-align: middle"></td>
                                            </tr>
                                            <tr>
                                                <td style="height: 10px; text-align: center; vertical-align: middle"></td>
                                            </tr>
                                            <tr>
                                                <td style="height: 30px; text-align: center; vertical-align: middle">
                                                    <a id="btnRemoveThree" runat="server" class="btn btn-danger"
                                                        onclick="javascript: remove1(document.getElementById('lstSelectedfldThree'), document.getElementById('lstAvailablefld'), 0)"><i class="fa fa-arrow-left"></i>&nbsp;&nbsp;Remove</a>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td align="center" class="normal1" id="trThree" runat="server"><b>Added to column 3</b><br>
                                        <asp:ListBox ID="lstSelectedfldThree" runat="server" Height="200" CssClass="form-control"
                                            EnableViewState="False"></asp:ListBox>
                                    </td>
                                    <td align="center" valign="middle" id="trThreeButton" runat="server" width="90px">
                                        <table style="margin-top: 20px">
                                            <tr>
                                                <td style="height: 30px; text-align: center; vertical-align: middle">
                                                    <a id="btnMoveupThree" class="btn btn-primary" onclick="javascript:MoveUp(document.getElementById('lstSelectedfldThree'),0)"><i class="fa fa-arrow-up"></i></a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="height: 10px; text-align: center; vertical-align: middle"></td>
                                            </tr>
                                            <tr>
                                                <td style="height: 30px; text-align: center; vertical-align: middle">
                                                    <a id="btnMoveDownThree" class="btn btn-primary" onclick="javascript:MoveDown(document.getElementById('lstSelectedfldThree'),0)"><i class="fa fa-arrow-down"></i></a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="height: 10px; text-align: center; vertical-align: middle"></td>
                                            </tr>
                                            <tr>
                                                <td style="height: 10px; text-align: center; vertical-align: middle"></td>
                                            </tr>
                                            <tr>
                                                <td style="height: 10px; text-align: center; vertical-align: middle"></td>
                                            </tr>
                                            <tr>
                                                <td style="height: 10px; text-align: center; vertical-align: middle"></td>
                                            </tr>
                                            <tr>
                                                <td style="height: 30px; text-align: center; vertical-align: middle"></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr id="ActivityForm" runat="server" visible="false">
                                    <td>
                                        <table class="table">
                                            <tr>
                                                <td>
                                                    <asp:CheckBox ID="chkFollowUpStatus" runat="server" />Display Follow-up Status</td>
                                                <td>
                                                    <asp:CheckBox ID="chkDisplayPriority" runat="server" />Display Priority</td>
                                                <td>
                                                    <asp:CheckBox ID="chkDisplayActivity" runat="server" />Display Activity</td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:CheckBox ID="chkCustomFieldSection" runat="server" />Display Custom Field Section
                                                    <br />
                                                    (includes the �Layout� button)</td>
                                                <td>
                                                    <asp:CheckBox ID="chkAttendeeSection" runat="server" />Display Attendees Section</td>
                                                <td>
                                                    <asp:CheckBox ID="chkFollowupAnytime" runat="server" />Check Follow-up Anytime by default, when new Action Item is created. </td>
                                            </tr>
                                        </table>

                                      
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div class="row" id="tdState" runat="server">
                        <div class="col-xs-12">
                            <div class="callout callout-info">
                                <h4>Please ensure that every state has a corresponding country selected for the form.</h4>

                                <p>For example - If you selected Ship To State, you must select Ship To Country, or your State field will not function correctly.</p>
                                <p>
                                    Also, If you have selected "Shipping Address same as Billing Address", then both "Bill To" and "Ship To" fields for City, Country, Postal Code, State and Street
                must be selected.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="box box-solid box-primary" id="ActivtyFormGCalendar" runat="server" visible="true">
                <div class="box-header with-border">
                    <h3 class="box-title">3rd party callendar data added to the �Comments� field within an Activity record</h3>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-xs-12">
                          <b>When connected to a 3rd party calendar,  what data from calendar do you want inserted in the action item �Comments� field ?</b><br><br>
                                      <table class="table table-responsive tblNoBorder">
                               
                                            <tr>
                                                <td style="width:150px">
                                                    <asp:CheckBox ID="chkTitle" runat="server" />Title</td>
                                                <td style="width:150px">
                                                    <asp:CheckBox ID="chkLocation" runat="server" />Location</td>
                                                <td>
                                                    <asp:CheckBox ID="chkDescription" runat="server" />Description</td>
                                            </tr>
                                        
                                </table>
                            </div>
                        </div>
                    </div>
                </div>  
            <div class="box box-solid box-primary" id="tblBizDocs" runat="server" visible="false">
                <div class="box-header with-border">
                    <h3 class="box-title">BizDoc Summary Fields Configuration</h3>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-xs-12">
                            <table class="table table-responsive tblNoBorder">
                                <tr>
                                    <td colspan="8" style="padding-bottom: 10px; padding-top: 10px; padding-left: 25px; font-size: 13px;">
                                        <b></b>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" class="normal1" valign="top"><b>Available Fields</b><br>
                                        <asp:ListBox ID="lstSummaryAvlFlds" runat="server" Height="200" CssClass="form-control"></asp:ListBox>
                                        &nbsp;&nbsp;&nbsp;
                                    </td>
                                    <td align="center" valign="middle" width="90px" style="padding-top: 20px;">
                                        <a id="btnAdd" class="btn btn-primary"
                                            onclick="javascript: move(document.getElementById('lstSummaryAvlFlds'), document.getElementById('lstSummaryAddedFlds'), 1, 0)">Add to 1&nbsp;&nbsp;<i class="fa fa-arrow-right"></i></a>
                                        <br />
                                        <br />
                                        <br />
                                        <br />
                                        <br />
                                        <br />
                                        <br />
                                        <a id="Button4" class="btn btn-danger"
                                            onclick="javascript: remove1(document.getElementById('lstSummaryAddedFlds'), document.getElementById('lstSummaryAvlFlds'), 0)"><i class="fa fa-arrow-left"></i>&nbsp;&nbsp;Remove</a>
                                    </td>
                                    <td align="center" class="normal1"><b>Added to column 1</b><br>
                                        <asp:ListBox ID="lstSummaryAddedFlds" runat="server" Height="200" CssClass="form-control"></asp:ListBox>
                                    </td>
                                    <td align="center" valign="middle" width="40px" style="padding-top: 20px;">
                                        <a id="Button6" class="btn btn-primary" onclick="javascript:MoveUp(document.getElementById('lstSummaryAddedFlds'),0)"><i class="fa fa-arrow-up"></i></a>
                                        <br />
                                        <br />
                                        <a id="Button7" class="btn btn-primary" onclick="javascript:MoveDown(document.getElementById('lstSummaryAddedFlds'),0)"><i class="fa fa-arrow-down"></i></a>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box box-solid box-primary" id="tblBzFormWizardConfigAOIHeader" runat="server">
                <div class="box-header with-border">
                    <h3 class="box-title">Areas Of Interest</h3>
                </div>
                <div class="box-body">
                    <table cellpadding="0" cellspacing="0" width="100%" class="table table-responsive tblNoBorder" id="tblBzFormWizardConfigAOI" runat="server">
                        <tr>
                            <td align="center" class="normal1">
                                <asp:ListBox ID="lstAvailablefldAOI" runat="server" Height="100" CssClass="form-control"></asp:ListBox>
                            </td>
                            <td align="center" valign="middle">
                                <a id="btnAddAOI" class="btn btn-primary" onclick="javascript: move(document.getElementById('lstAvailablefldAOI'), document.getElementById('lstSelectedfldAOI'), 1, 1)">Add &nbsp;&nbsp;<i class="fa fa-arrow-right"></i></a>
                                <br />
                                <br />
                                &nbsp;&nbsp;
                                        <a id="btnRemoveAOI" class="btn btn-danger" onclick="javascript: remove1(document.getElementById('lstSelectedfldAOI'), document.getElementById('lstAvailablefldAOI'), 1)"><i class="fa fa-arrow-left"></i>&nbsp;&nbsp;Remove</a>&nbsp;&nbsp;
                            </td>
                            <td align="center" class="normal1">
                                <asp:ListBox ID="lstSelectedfldAOI" runat="server" Height="100" CssClass="form-control"></asp:ListBox>
                            </td>
                            <td align="left" valign="middle">
                                <br />
                                &nbsp;&nbsp;&nbsp;
                                        <a id="btnMoveupAOI" class="btn btn-primary" style="margin-bottom: 10px" onclick="javascript:MoveUp(document.getElementById('lstSelectedfldAOI'),1)"><i class="fa fa-arrow-up"></i></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        <br />
                                &nbsp;&nbsp;&nbsp;
                                        <a id="btnMovedownAOI" class="btn btn-primary" onclick="javascript:MoveDown(document.getElementById('lstSelectedfldAOI'),1)"><i class="fa fa-arrow-down"></i></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        <br />
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <div class="row">
        <div class="col-xs-12 text-center">
            <asp:Button ID="btnSuveryForm" CssClass="btn btn-success" Text="Open The Survey Registration Form"
                runat="server" CausesValidation="False"></asp:Button>
        </div>
    </div>

    <asp:CustomValidator ID="custValidateBusinessShipAddress" runat="server" ClientValidationFunction="validateBusinessAndShipAddress"
        ErrorMessage="You have selected the field, 'Shipping Address same as Billing Address'. You must select all the 'Bill To' and 'Ship To' fields."
        Display="None" EnableClientScript="True"></asp:CustomValidator>
    <asp:ValidationSummary ID="ValidationSummary" runat="server" HeaderText="Please check the following value(s)"
        ShowSummary="False" ShowMessageBox="True" DisplayMode="List"></asp:ValidationSummary>
    <asp:Literal ID="litClientScript" runat="server"></asp:Literal>
    <asp:Literal ID="litClientAlertMessageScript" runat="server" EnableViewState="False"></asp:Literal>
    <asp:UpdateProgress ID="UpdateProgress" runat="server" ClientIDMode="Static">
        <ProgressTemplate>
            <div class="overlay">
                <div class="overlayContent" style="color: #000; text-align: center; width: 250px; padding: 20px">
                    <i class="fa fa-2x fa-refresh fa-spin"></i>
                    <h3>Processing Request</h3>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
</asp:Content>
