﻿Imports BACRM.BusinessLogic.Common
Imports Telerik.Web.UI
Imports BACRM.BusinessLogic.Opportunities

Public Class actionitemorganization
    Inherits BACRMPage

#Region "Page Events"
    Protected Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        Try
            CCommon.InitializeOrganizationClientSideTemplate(Session("DomainID"), Session("UserContactID"), radCmbCompany)
            CCommon.UpdateItemRadComboValues("1", radCmbCompany.SelectedValue)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(CCommon.ToString(ex))
        End Try
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not Page.IsPostBack Then
                If Not String.IsNullOrEmpty(GetQueryStringVal("orgID")) Then
                    hdnDivisionID.Value = GetQueryStringVal("orgID")
                    hdnDivisionName.Value = GetQueryStringVal("orgName")

                    Dim item As New RadComboBoxItem
                    item.Value = hdnDivisionID.Value
                    item.Text = hdnDivisionName.Value
                    item.Selected = True

                    radCmbCompany.Items.Add(item)
                    radCmbCompany_SelectedIndexChanged(radCmbCompany, Nothing)
                    btnRemove.Visible = True
                End If

                If Not String.IsNullOrEmpty(GetQueryStringVal("cntID")) Then
                    hdnContactID.Value = GetQueryStringVal("cntID")
                    ddlContact.SelectedValue = hdnContactID.Value
                End If
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(CCommon.ToString(ex))
        End Try
    End Sub
#End Region

#Region "Private Methods"
    Private Sub LoadCompanyDetail()
        Try
            Dim objLeads As New BACRM.BusinessLogic.Leads.LeadsIP
            objLeads.DivisionID = radCmbCompany.SelectedValue
            objLeads.DomainID = Session("DomainID")
            objLeads.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
            objLeads.GetCompanyDetails()

            hdnFollowUp.Value = objLeads.FollowUpStatus
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Private Sub BindContact()
        Try
            Dim objOpportunities As New COpportunities
            objOpportunities.DivisionID = radCmbCompany.SelectedValue
            ddlContact.DataSource = objOpportunities.ListContact().Tables(0).DefaultView()
            ddlContact.DataTextField = "ContactType"
            ddlContact.DataValueField = "numcontactId"
            ddlContact.DataBind()
            ddlContact.Items.Insert(0, New ListItem("--Select One--", "0"))
            If ddlContact.Items.Count >= 2 Then
                ddlContact.Items(1).Selected = True
                hdnContactName.Value = ddlContact.Items(1).Text
                hdnContactID.Value = ddlContact.Items(1).Value
                LoadContactDetail()
            End If
        Catch ex As Exception
            Throw
        End Try
    End Sub
    Private Sub LoadContactDetail()
        Try
            Dim objLeads As New BACRM.BusinessLogic.Leads.LeadsIP
            objLeads.ContactID = hdnContactID.Value
            objLeads.DomainID = Session("DomainID")
            objLeads.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
            objLeads.GetLeadDetails()

            hdnEmail.Value = objLeads.Email
            hdnPhone.Value = objLeads.ContactPhone
        Catch ex As Exception
            Throw
        End Try
    End Sub
#End Region

#Region "Event Handlers"
    Private Sub radCmbCompany_SelectedIndexChanged(sender As Object, e As RadComboBoxSelectedIndexChangedEventArgs) Handles radCmbCompany.SelectedIndexChanged
        Try
            ddlContact.Items.Clear()
            hdnContactID.Value = ""
            hdnEmail.Value = ""
            hdnPhone.Value = ""

            If CCommon.ToLong(radCmbCompany.SelectedValue) > 0 Then
                hdnDivisionID.Value = radCmbCompany.SelectedValue
                If Not radCmbCompany.SelectedItem Is Nothing Then
                    hdnDivisionName.Value = radCmbCompany.SelectedItem.Text
                Else
                    hdnDivisionName.Value = radCmbCompany.Text
                End If
            Else
                hdnDivisionID.Value = ""
                hdnDivisionName.Value = ""
            End If

            If radCmbCompany.SelectedValue <> "" Then
                LoadCompanyDetail()
                BindContact()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(CCommon.ToString(ex))
        End Try
    End Sub
    Private Sub ddlContact_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlContact.SelectedIndexChanged
        Try
            If CCommon.ToLong(ddlContact.SelectedValue) > 0 Then
                hdnContactID.Value = ddlContact.SelectedValue
                hdnContactName.Value = ddlContact.SelectedItem.Text
            Else
                hdnContactID.Value = ""
                hdnContactName.Value = ""
            End If

            LoadContactDetail()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(CCommon.ToString(ex))
        End Try
    End Sub
#End Region

End Class