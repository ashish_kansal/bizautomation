<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmItemSearchRes.aspx.vb"
    Inherits="BACRM.UserInterface.Admin.frmItemSearchRes" MasterPageFile="~/common/GridMasterRegular.Master" %>

<%@ Register TagPrefix="menu1" TagName="webmenu" Src="../include/webmenu.ascx" %>
<%@ Register Assembly="AspNetPager, Version=7.4.4.0, Culture=neutral, PublicKeyToken=fb0a0fe055d40fd4" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>
<%@ Register Src="../common/frmBizSorting.ascx" TagName="frmBizSorting" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Item Search Results</title>
    <script type="text/javascript">
        function EditSearchView() {
            window.open('../admin/frmAdvSearchColumnCustomization.aspx?FormId=15', "", "width=500,height=300,status=no,scrollbars=yes,left=155,top=160")
            return false;
        }

        function Back() {
            document.location.href = '../admin/frmAdvancedSearchNew.aspx';
            return false
        }

        function OpenOpp(a, b, c) {
            var str;

            str = "../opportunity/frmOpportunities.aspx?frm=OppSearch&OpID=" + a;

            if (c == 1) {
                window.open(str);
            }
            else {
                document.location.href = str;
            }
        }

        function OpenBizDoc(a, b) {
            window.open('../opportunity/frmBizInvoice.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&OpID=' + a + '&OppBizId=' + b, '', 'toolbar=no,menubar=yes,titlebar=no,left=100,width=680,height=700,scrollbars=yes,resizable=yes');
            return false;
        }

        function MassUpdate() {
            window.open('../admin/frmAdvSearchMassUpdater.aspx?FormID=15', "", "width=500,height=400,status=no,scrollbars=yes,left=155,top=160")
            return false;
        }

        function UpdateValues() {
            document.getElementById("txtReload").value = true;
            document.getElementById("btnUpdateValues").click()
        }

        function OpenSearch(a) {
            window.open('../Admin/frmSavedSearch.aspx?FormID=' + a, '', 'toolbar=no,menubar=yes,titlebar=no,left=100,width=400,height=200,scrollbars=yes,resizable=yes');
            return false;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server"
    ClientIDMode="Static">
    <div id="divMessage" runat="server" class="row padbottom10" style="display: none">
        <div class="col-xs-12">
            <div class="alert alert-warning">
                <h4><i class="icon fa fa-warning"></i>Alert!</h4>
                <asp:Literal ID="litMessage" runat="server" EnableViewState="False"></asp:Literal>
            </div>
        </div>
    </div>
    <div class="row padbottom10">
        <div class="col-xs-12">
            <div class="pull-right">
                <asp:CheckBox ID="chkSelectAll" runat="server" Text="Select All Pages" />&nbsp;&nbsp;
                <asp:DropDownList ID="ddlSort" runat="server" CssClass="form-control" Style="display: none"></asp:DropDownList>
                <asp:HyperLink runat="server" CssClass="btn btn-primary" ID="hplSaveSearch" onclick="return OpenSearch(15);" Text="Save this search"></asp:HyperLink>
                <asp:UpdatePanel ID="UpdatePanel1" runat="server" style="display: inline" UpdateMode="Conditional">
                    <ContentTemplate>
                        <asp:Button runat="server" ID="btnExport" CssClass="btn btn-primary" Text="Export To Excel" />
                    </ContentTemplate>
                    <Triggers>
                        <asp:PostBackTrigger ControlID="btnExport" />
                    </Triggers>
                </asp:UpdatePanel>
                <asp:Button ID="btnMassUpdate" runat="server" Text="Mass Update" CssClass="btn btn-primary" CausesValidation="False" OnClientClick="return MassUpdate();"></asp:Button>
                <asp:LinkButton ID="lkbBackToSearchCriteria" CssClass="btn btn-primary" runat="server" CausesValidation="False"><i class="fa fa-arrow-left"></i>&nbsp;&nbsp;Back</asp:LinkButton>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server" ClientIDMode="Static">
    Search Results
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BizPager" runat="server" ClientIDMode="Static">
    <webdiyer:AspNetPager ID="bizPager" runat="server"
        PagingButtonSpacing="0"
        CssClass="bizgridpager"
        AlwaysShow="true"
        CurrentPageButtonClass="active"
        PagingButtonUlLayoutClass="pagination"
        PagingButtonLayoutType="UnorderedList"
        FirstPageText="<<"
        LastPageText=">>"
        NextPageText=">"
        PrevPageText="<"
        Width="100%"
        UrlPaging="false"
        NumericButtonCount="8"
        ShowCustomInfoSection="Left"
        ShowPageIndexBox="Never"
        OnPageChanged="bizPager_PageChanged"
        CustomInfoHTML="Showing records %StartRecordIndex% to %EndRecordIndex% of %RecordCount% "
        CustomInfoClass="bizpagercustominfo">
    </webdiyer:AspNetPager>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="GridPlaceHolder" runat="server"
    ClientIDMode="Static">
    <asp:UpdateProgress ID="UpdateProgress" runat="server" ClientIDMode="Static">
        <ProgressTemplate>
            <div class="overlay">
                <div class="overlayContent" style="color: #000; text-align: center; width: 250px; padding: 20px">
                    <i class="fa fa-2x fa-refresh fa-spin"></i>
                    <h3>Processing Request</h3>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <div class="row">
        <div class="col-xs-12">
            <div class="table-responsive">
                <asp:GridView ID="gvSearch" runat="server" EnableViewState="true" AutoGenerateColumns="false" CssClass="table table-bordered table-striped" Width="100%">
                    <Columns>
                    </Columns>
                </asp:GridView>
            </div>
        </div>
    </div>
    <asp:TextBox ID="txtTotalPage" Style="display: none" runat="server"></asp:TextBox>
    <asp:TextBox ID="txtTotalRecords" Style="display: none" runat="server"></asp:TextBox>
    <asp:TextBox ID="txtSortChar" Text="0" Style="display: none" runat="server"></asp:TextBox>
    <asp:TextBox ID="txtSortColumn" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtSortOrder" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtReload" runat="server" Style="display: none">False</asp:TextBox>
    <asp:TextBox ID="txtGridColumnFilter" Style="display: none" runat="server"></asp:TextBox>
    <asp:Button ID="btnUpdateValues" runat="server" Style="display: none" />
    <asp:TextBox ID="txtCurrrentPage" runat="server" Style="display: none"></asp:TextBox>
    <asp:Button ID="btnGo1" Width="25" runat="server" Style="display: none" />
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="GridBizSorting" runat="server" ClientIDMode="Static">
    <uc1:frmBizSorting ID="frmBizSorting2" runat="server" />
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="GridSettingPopup" runat="server" ClientIDMode="Static">
    <a href="#" id="hplClearGridCondition" title="Clear All Filters" class="btn-box-tool"><i class="fa fa-2x fa-filter"></i></a>
    <asp:HyperLink runat="server" ID="hplEditResView" ToolTip="Edit advance search result view"><i class="fa fa-lg fa-gear"></i></asp:HyperLink>
</asp:Content>