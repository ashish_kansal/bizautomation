Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Common
Partial Public Class frmNewTabs
    Inherits BACRMPage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then
               
                
                GetUserRightsForPage(13, 25)
                If m_aryRightsForPage(RIGHTSTYPE.VIEW) = 0 Then
                    Response.Redirect("../admin/authentication.aspx?mesg=AS")
                ElseIf m_aryRightsForPage(RIGHTSTYPE.UPDATE) = 0 Then
                    btnAdd.Visible = False
                End If

                PopulateGroups()

                
            End If

            If Session("UserGroupID") IsNot Nothing Then
                If ddlTabType.Items.FindByValue(Session("UserGroupID")) IsNot Nothing Then
                    ddlTabType.Items.FindByValue(Session("UserGroupID")).Attributes.Add("style", "color:green")
                End If
            End If
            btnClose.Attributes.Add("onclick", "return Close();")
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Sub DataGridBind()
        Try
            If ddlTabType.SelectedIndex > 0 Then
                Dim objTabs As New Tabs
                objTabs.DomainID = Session("DomainID")
                objTabs.TabType = ddlTabType.SelectedValue.Split("~")(1)
                dgTabs.DataSource = objTabs.GetTabDomainSpecific
                dgTabs.DataBind()
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Sub PopulateGroups()
        Dim objUserGroups As New UserGroups
        objUserGroups.DomainId = Session("DomainID")
        ddlTabType.DataSource = objUserGroups.GetAutorizationGroup
        ddlTabType.DataTextField = "vcGroupName"
        ddlTabType.DataValueField = "numGroupID1"
        ddlTabType.DataBind()
        ddlTabType.Items.Insert(0, "--Select One--")
        ddlTabType.Items.FindByText("--Select One--").Value = 0

        If Session("UserGroupID") IsNot Nothing Then
            If ddlTabType.Items.FindByValue(Session("UserGroupID")) IsNot Nothing Then
                ddlTabType.ClearSelection()
                ddlTabType.Items.FindByValue(Session("UserGroupID")).Selected = True
            End If
        End If
    End Sub

    Private Sub ddlTabType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlTabType.SelectedIndexChanged
        Try
            DataGridBind()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        Try
            Dim objTabs As New Tabs
            objTabs.TabName = txtTabName.Text.Trim
            objTabs.TabURL = txtURL.Text.Trim
            objTabs.TabType = ddlTabType.SelectedValue.Split("~")(1)
            objTabs.DomainID = Session("DomainID")
            objTabs.ManageMasterTabs()
            DataGridBind()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub dgTabs_EditCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgTabs.EditCommand
        Try
            dgTabs.EditItemIndex = e.Item.ItemIndex
            DataGridBind()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub dgTabs_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgTabs.ItemCommand
        Try
            If e.CommandName = "Cancel" Then
                dgTabs.EditItemIndex = e.Item.ItemIndex
                dgTabs.EditItemIndex = -1
                DataGridBind()
            End If
            If e.CommandName = "Delete" Then
                If e.Item.Cells(1).Text = "False" Then
                    Dim objTabs As New Tabs
                    objTabs.TabID = e.Item.Cells(0).Text
                    objTabs.DeleteMasterTabs()
                    DataGridBind()
                 End If
            End If
            If e.CommandName = "Update" Then
                Dim objTabs As New Tabs
                objTabs.TabID = CType(e.Item.FindControl("lblTabID"), Label).Text
                objTabs.TabName = CType(e.Item.FindControl("txtETab"), TextBox).Text
                objTabs.DomainID = Session("DomainID")
                objTabs.TabURL = CType(e.Item.FindControl("txtEURL"), TextBox).Text
                objTabs.TabType = ddlTabType.SelectedValue.Split("~")(1)
                objTabs.ManageMasterTabs()
                dgTabs.EditItemIndex = -1
                DataGridBind()
                
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub dgTabs_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgTabs.ItemDataBound
        Try
            If e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.Item Then
                If e.Item.Cells(1).Text = True Then
                    Dim lnkdelete As LinkButton
                    Dim btnDelete As Button
                    lnkdelete = e.Item.FindControl("lnkdelete")
                    btnDelete = e.Item.FindControl("btnDelete")
                    btnDelete.Visible = False
                    lnkdelete.Visible = True
                    lnkdelete.Attributes.Add("onclick", "return NoDeleteMessage()")
                End If
            End If
            If e.Item.ItemType = ListItemType.EditItem Then
                If CType(e.Item.FindControl("lblFixed"), Label).Text = True Then
                    Dim txtURL As TextBox
                    txtURL = e.Item.FindControl("txtEURL")
                    txtURL.Enabled = False
                End If
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

End Class