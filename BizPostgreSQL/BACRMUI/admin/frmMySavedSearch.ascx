﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="frmMySavedSearch.ascx.vb"
    Inherits=".frmMySavedSearch" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<style type="text/css">
    #hovermenu
    {
        border: 1px solid black;
        width: 170px;
        background-color: #f3f4f5;
    }
    
    #hovermenu a
    {
        font: bold 11px "Segoe UI" ,Arial,sans-serif;
        padding: 2px;
        padding-left: 4px;
        display: block;
        width: 100%;
        color: black;
        text-decoration: none;
        border-bottom: 1px solid black;
    }
    
    html > body #hovermenu a
    {
        /*Non IE rule*/
        width: auto;
    }
    
    #hovermenu a:hover
    {
        background-color: #515E81;
        color: white;
    }
</style>
<script type="text/javascript">
    function OpenSearchResult(a, b) {
        if (b == 15) {
            document.location.href = 'frmItemSearchRes.aspx?SearchID=' + a;
        }
        if (b == 1) {
            document.location.href = 'frmAdvancedSearchRes.aspx?SearchID=' + a;
        }
        if (b == 29) {
            document.location.href = 'frmAdvSerInvItemsRes.aspx?SearchID=' + a;
        }
    }
</script>
<asp:HyperLink NavigateUrl="#" ID="hplMySavedSearch" CssClass="btn btn-xs btn-default" runat="server" Text="My Saved Search" />
<asp:Panel runat="server" ID="pnlSavedSearch">
    <div id="hovermenu">
        <a href="#" id="hplNoRecords" runat="server" visible="false">No records available</a>
        <asp:Repeater runat="server" ID="rptSavedSearch">
            <ItemTemplate>
                <a href="javascript:void(0);" onclick="OpenSearchResult('<%# Eval("numSearchID") %>','<%# Eval("numFormID") %>');">
                    <%# If(Eval("vcSearchName").ToString().Length > 27, Eval("vcSearchName").ToString().Substring(0, 26) & "..", Eval("vcSearchName").ToString)%></a>
            </ItemTemplate>
        </asp:Repeater>
    </div>
</asp:Panel>
<ajaxToolkit:HoverMenuExtender ID="HoverMenuExtender1" runat="server" TargetControlID="hplMySavedSearch"
    PopupControlID="pnlSavedSearch" OffsetY="20">
</ajaxToolkit:HoverMenuExtender>
