Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Prospects
Namespace BACRM.UserInterface.Admin
    Public Class frmCompanyAssociationFrom
        Inherits BACRMPage

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region
        Protected WithEvents dgAssociation As System.Web.UI.WebControls.DataGrid
        Protected WithEvents btnClose As System.Web.UI.WebControls.Button
        Dim lngDivID As Long

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Try
                lngDivID = CCommon.ToLong(GetQueryStringVal("rtyWR"))
                If IsPostBack = False Then bindGrid()
                
                btnClose.Attributes.Add("onclick", "return Close()")
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Sub bindGrid()
            Try
                Dim objProspects As New CProspects
                Dim dtAssociationFrom As DataTable
                objProspects.DivisionID = lngDivID
                'objProspects.DivisionIdList = lngDivID
                objProspects.bitAssociatedTo = 0
                dtAssociationFrom = objProspects.GetAssociationTo().Tables(0)
                dgAssociation.DataSource = dtAssociationFrom
                dgAssociation.DataBind()
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

    End Class
End Namespace