﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmSavedSearchList.aspx.vb"
    Inherits=".frmSavedSearchList" MasterPageFile="~/common/GridMasterRegular.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <script type="text/javascript">
        function OpenSearchResult(a, b) {
            if (b == 15) {
                document.location.href = 'frmItemSearchRes.aspx?SearchID=' + a + '&frm=SavedSearch';
            }
            if (b == 1) {
                document.location.href = 'frmAdvancedSearchRes.aspx?SearchID=' + a + '&frm=SavedSearch';
            }
            if (b == 29) {
                document.location.href = 'frmAdvSerInvItemsRes.aspx?SearchID=' + a + '&frm=SavedSearch';
            }
        }

    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server" ClientIDMode="Static">
    My Saved Search
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BizPager" runat="server" ClientIDMode="Static">
    &nbsp;
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="GridPlaceHolder" runat="server"
    ClientIDMode="Static">
    <asp:UpdateProgress ID="UpdateProgress" runat="server" ClientIDMode="Static">
        <ProgressTemplate>
            <div class="overlay">
                <div class="overlayContent" style="color: #000; text-align: center; width: 250px; padding: 20px">
                    <i class="fa fa-2x fa-refresh fa-spin"></i>
                    <h3>Processing Request</h3>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <div class="row">
        <div class="col-xs-12">
            <div class="table-responsive">
                <asp:DataGrid ID="dgSavedSearch" AllowSorting="True" runat="server" Width="100%"
                    CssClass="table table-bordered table-striped" AutoGenerateColumns="False" UseAccessibleHeader="true">
                    <Columns>
                        <asp:BoundColumn Visible="False" DataField="numSearchID"></asp:BoundColumn>
                        <asp:TemplateColumn HeaderText="Search Name">
                            <ItemTemplate>
                                <a href="javascript:void(0);" onclick="javascript:OpenSearchResult('<%# Eval("numSearchID") %>','<%# Eval("numFormID") %>')">
                                    <%# Eval("vcSearchName") %></a>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:BoundColumn HeaderText="Search Query" DataField="vcSearchQueryDisplay"></asp:BoundColumn>
                        <asp:BoundColumn HeaderText="Search Form" DataField="vcFormName"></asp:BoundColumn>
                        <asp:TemplateColumn ItemStyle-Width="25">
                            <ItemTemplate>
                                <asp:LinkButton ID="btnDelete" runat="server" CssClass="btn btn-xs btn-danger" CommandName="Delete"><i class="fa fa-trash"></i></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                </asp:DataGrid>
            </div>
        </div>
    </div>
</asp:Content>
