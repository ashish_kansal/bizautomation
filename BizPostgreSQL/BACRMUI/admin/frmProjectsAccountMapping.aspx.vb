﻿Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Accounting
Imports BACRM.BusinessLogic.Projects
Partial Public Class frmProjectsAccountMapping
    Inherits BACRMPage
    Dim lngTransChargeID As Long
   

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            GetUserRightsForPage(13, 1)

            If m_aryRightsForPage(RIGHTSTYPE.VIEW) = 0 Then
                Response.Redirect("../admin/authentication.aspx?mesg=AS")
            ElseIf m_aryRightsForPage(RIGHTSTYPE.ADD) = 0 Then
                btnSave.Visible = False
            End If

            If Not IsPostBack Then
                
                BindProject()
                BindCOA()
            End If
            If Not ViewState("TransChargeID") Is Nothing Then
                lngTransChargeID = ViewState("TransChargeID")
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
    Private Sub BindProject()
        Try
            Dim objProject As New Project
            objProject.DomainID = Session("DomainID")
            ddlProject.DataTextField = "vcProjectName"
            ddlProject.DataValueField = "numProId"
            ddlProject.DataSource = objProject.GetOpenProject()
            ddlProject.DataBind()
            ddlProject.Items.Insert(0, "--Select One--")
            ddlProject.Items.FindByText("--Select One--").Value = "0"
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Private Sub BindData()
        Try
            Dim objProject As New Project
            Dim dt As DataTable
            objProject.DomainID = Session("DomainID")
            objProject.ProjectID = ddlProject.SelectedValue
            dt = objProject.GetOpenProject()

            If dt.Rows.Count > 0 Then
                If Not IsDBNull(dt.Rows(0).Item("numAccountId")) Then
                    If Not ddlAccount.Items.FindByValue(dt.Rows(0).Item("numAccountId")) Is Nothing Then
                        ddlAccount.Items.FindByValue(dt.Rows(0).Item("numAccountId")).Selected = True
                    End If
                End If
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Private Sub BindCOA()
        Try
            Dim objCOA As New ChartOfAccounting
            objCOA.DomainId = Session("DomainId")
            objCOA.AccountCode = "0104"
            Dim dtChartAcntDetails As DataTable = objCOA.GetParentCategory()
            Dim item As ListItem
            For Each dr As DataRow In dtChartAcntDetails.Rows
                item = New ListItem()
                item.Text = dr("vcAccountName")
                item.Value = dr("numAccountID")
                item.Attributes("OptionGroup") = dr("vcAccountType")
                ddlAccount.Items.Add(item)
            Next
            ddlAccount.Items.Insert(0, New ListItem("--Select One --", "0"))
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            Dim objProject As New Project
            objProject.DomainID = Session("DomainID")
            objProject.ProjectID = ddlProject.SelectedValue
            objProject.AccountID = ddlAccount.SelectedValue
            objProject.UpdateProjectAccount()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub ddlProject_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlProject.SelectedIndexChanged
        Try
            ddlAccount.ClearSelection()
            If ddlProject.SelectedValue > 0 Then
                BindData()
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
End Class