﻿Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Accounting

Public Class frmChangeDefaultAccount
    Inherits BACRMPage
    Dim objCommon As CCommon

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then
                BindData()
            End If

            If IsPostBack Then

            End If
            btnCancel.Attributes.Add("onclick", "return Close()")
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
    Sub BindData()
        Try
            Dim dtChartAcntDetails As DataTable
            Dim objCOA As New ChartOfAccounting
            objCOA.DomainID = Session("DomainId")
            objCOA.AccountCode = "0103"
            dtChartAcntDetails = objCOA.GetParentCategory()
            Dim item As ListItem
            For Each dr As DataRow In dtChartAcntDetails.Rows
                item = New ListItem()
                item.Text = dr("vcAccountName")
                item.Value = dr("numAccountID")
                item.Attributes("OptionGroup") = dr("vcAccountType")
                ddlIncomeAccount.Items.Add(item)
            Next
            objCOA.AccountCode = "0101"
            dtChartAcntDetails = objCOA.GetParentCategory()
            For Each dr As DataRow In dtChartAcntDetails.Rows
                item = New ListItem()
                item.Text = dr("vcAccountName")
                item.Value = dr("numAccountID")
                item.Attributes("OptionGroup") = dr("vcAccountType")
                ddlAssetAccount.Items.Add(item)
            Next
            objCOA.AccountCode = "0106"
            dtChartAcntDetails = objCOA.GetParentCategory()
            For Each dr As DataRow In dtChartAcntDetails.Rows
                item = New ListItem()
                item.Text = dr("vcAccountName")
                item.Value = dr("numAccountID")
                item.Attributes("OptionGroup") = dr("vcAccountType")
                ddlCOGS.Items.Add(item)
            Next
            ddlIncomeAccount.Items.Insert(0, New ListItem("--Select One --", "0"))
            ddlAssetAccount.Items.Insert(0, New ListItem("--Select One --", "0"))
            ddlCOGS.Items.Insert(0, New ListItem("--Select One --", "0"))

            Dim dtTable As DataTable
            Dim objUserAccess As New UserAccess
            objUserAccess.DomainID = Session("DomainID")
            dtTable = objUserAccess.GetDomainDetails()
            If Not ddlIncomeAccount.Items.FindByValue(dtTable.Rows(0).Item("numIncomeAccID")) Is Nothing Then
                ddlIncomeAccount.ClearSelection()
                ddlIncomeAccount.Items.FindByValue(dtTable.Rows(0).Item("numIncomeAccID")).Selected = True
            End If
            If Not ddlCOGS.Items.FindByValue(dtTable.Rows(0).Item("numCOGSAccID")) Is Nothing Then
                ddlCOGS.ClearSelection()
                ddlCOGS.Items.FindByValue(dtTable.Rows(0).Item("numCOGSAccID")).Selected = True
            End If
            If Not ddlAssetAccount.Items.FindByValue(dtTable.Rows(0).Item("numAssetAccID")) Is Nothing Then
                ddlAssetAccount.ClearSelection()
                ddlAssetAccount.Items.FindByValue(dtTable.Rows(0).Item("numAssetAccID")).Selected = True
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    
    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If Page.IsValid Then
            Try
                save()
                BindData()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End If
    End Sub

    Private Sub btnSaveClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveClose.Click
        If Page.IsValid Then
            Try
                save()
                Dim strScript As String = "<script language=JavaScript>self.close()</script>"
                If (Not Page.IsStartupScriptRegistered("clientScript")) Then Page.RegisterStartupScript("clientScript", strScript)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End If
    End Sub

    Private Sub save()
        Try
            Dim objUserAccess As New UserAccess
            objUserAccess.DomainID = Session("DomainID")
            objUserAccess.AssetAccountID = CCommon.ToLong(ddlAssetAccount.SelectedValue)
            objUserAccess.IncomeAccountID = CCommon.ToLong(ddlIncomeAccount.SelectedValue)
            objUserAccess.COGSAccountID = CCommon.ToLong(ddlCOGS.SelectedValue)
            objUserAccess.UpdateDomainChangeDefaultAccount()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub



End Class