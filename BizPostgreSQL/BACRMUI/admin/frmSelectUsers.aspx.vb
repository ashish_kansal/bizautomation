﻿Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Reports

Namespace BACRM.UserInterface.Admin
    Public Class frmSelectUsers
        Inherits BACRMPage

        Dim iType As Integer
        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            Try
                iType = CCommon.ToInteger(GetQueryStringVal( "Type"))
                If Not IsPostBack Then
                    
                    
                    Dim dtAvailUsers As DataTable

                    If iType = 1 Then
                        dtAvailUsers = objCommon.ConEmpList(Session("DomainID"), Session("UserContactID"))
                    Else
                        dtAvailUsers = objCommon.ConEmpList(Session("DomainID"), 0, 0)
                    End If

                    lstUserAvail.DataTextField = "vcUserName"
                    lstUserAvail.DataValueField = "numContactID"
                    lstUserAvail.DataSource = dtAvailUsers
                    lstUserAvail.DataBind()

                    Dim objRep As New PredefinedReports
                    Dim dtUserSelected As DataTable
                    objRep.UserCntID = Session("UserContactId")
                    objRep.DomainID = Session("DomainID")
                    objRep.ReportType = iType
                    dtUserSelected = objRep.GetUsersForForRep
                    lstUserSelected.DataSource = dtUserSelected
                    lstUserSelected.DataTextField = "vcUserName"
                    lstUserSelected.DataValueField = "numSelectedUserCntID"
                    lstUserSelected.DataBind()
                    If dtUserSelected.Rows.Count > 0 Then
                        rblUserType.SelectedValue = dtUserSelected.Rows(0)("tintUserType")
                    End If
                End If
                btnSave.Attributes.Add("onclick", "return Save()")

                If iType = 1 Then
                    rblUserType.Visible = False
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub
        Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
            Try
                Dim objRep As New PredefinedReports
                objRep.strUsers = hdnValue.Text
                objRep.UserCntID = Session("UserContactId")
                objRep.DomainID = Session("DomainID")
                objRep.ReportType = iType
                objRep.byteMode = rblUserType.SelectedValue
                objRep.ManageUsersForForRept()

                If iType = 1 Then
                    Response.Write("<script>window.opener.location.reload(true); self.close();</script>")
                Else
                    Response.Write("<script>opener.PopupCheck(); self.close();</script>")
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

    End Class
End Namespace
