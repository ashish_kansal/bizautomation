Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Admin
Imports Telerik.Web.UI

Partial Public Class frmTreeNavigationAuthorization
    Inherits BACRMPage

    Dim objCommon As New CCommon

#Region "PAGE EVENTS"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            btnSave.Attributes.Add("OnClick", "return Save()")
            DomainID = Session("DomainID")

            If Not IsPostBack Then

                GetUserRightsForPage(13, 25)
                If m_aryRightsForPage(RIGHTSTYPE.VIEW) = 0 Then
                    Response.Redirect("../admin/authentication.aspx?mesg=AS")
                ElseIf m_aryRightsForPage(RIGHTSTYPE.UPDATE) = 0 Then
                    btnSave.Visible = False
                    btnSetAsInitial.Visible = False
                End If

                PopulateAuthorizedGroupListData()
                BindTab()
                BindGrid()

                PopulateShortCutAuthorizedGroupListData()
                ShortCutBindTab()
                PopulateShortCutData()

                Dim radItem As Telerik.Web.UI.RadComboBoxItem
                For Each item As ListItem In ddlSrtCutGroup.Items
                    If item.Value <> "0" AndAlso CCommon.ToLong(item.Value) <> CCommon.ToLong(Session("UserGroupID")) Then
                        radItem = New Telerik.Web.UI.RadComboBoxItem
                        radItem.Text = item.Text
                        radItem.Value = item.Value
                        rcbShrCutGroups.Items.Add(radItem)
                    End If
                Next
            End If

            If Session("UserGroupID") IsNot Nothing Then
                If ddlGroup.Items.FindByValue(Session("UserGroupID")) IsNot Nothing Then
                    ddlGroup.Items.FindByValue(Session("UserGroupID")).Attributes.Add("style", "color:green")
                End If
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

#End Region



#Region "PRIVATE/PUBLIC SUBS/FUNCTIONS"
    Sub PopulateShortCutAuthorizedGroupListData()
        Try
            ddlSrtCutGroup.Items.Clear()  'Clear all the items
            Dim objUserGroups As New UserGroups
            Dim dtGroupName As DataTable
            objUserGroups.DomainID = Session("DomainID")
            objUserGroups.SelectedGroupTypes = "1,4"
            dtGroupName = objUserGroups.GetAutorizationGroup

            ddlSrtCutGroup.DataSource = dtGroupName.DataSet.Tables(0).DefaultView
            ddlSrtCutGroup.DataTextField = "vcGroupName"
            ddlSrtCutGroup.DataValueField = "numGroupId"
            ddlSrtCutGroup.DataBind()
            ddlSrtCutGroup.Items.Insert(0, "--Select One--")
            ddlSrtCutGroup.Items.FindByText("--Select One--").Value = 0

            If Session("UserGroupID") IsNot Nothing Then
                If ddlSrtCutGroup.Items.FindByValue(Session("UserGroupID")) IsNot Nothing Then
                    ddlSrtCutGroup.ClearSelection()
                    ddlSrtCutGroup.Items.FindByValue(Session("UserGroupID")).Selected = True
                End If
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub ShortCutBindTab()
        Try
            Dim dtTabData As DataTable
            Dim objTab As New Tabs
            objTab.GroupID = ddlSrtCutGroup.SelectedValue
            objTab.RelationshipID = 0
            objTab.DomainID = Session("DomainID")
            dtTabData = objTab.GetTabData()

            ddlSrtCutTab.DataSource = dtTabData
            ddlSrtCutTab.DataTextField = "numTabName"
            ddlSrtCutTab.DataValueField = "numTabID"
            ddlSrtCutTab.DataBind()

            ddlSrtCutTab.Items.Insert(0, New ListItem("--Select One--", "0"))
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub PopulateShortCutData()
        Try
            Dim objUserGroups As New CShortCutBar
            Dim ds As New DataSet
            If ddlSrtCutGroup.SelectedValue = 0 AndAlso ddlSrtCutTab.SelectedValue > 0 Then
                lstAddfldFav.Items.Clear()
            Else
                objUserGroups.GroupId = ddlSrtCutGroup.SelectedValue
                objUserGroups.DomainID = Session("DomainID")
                objUserGroups.TabId = ddlSrtCutTab.SelectedValue

                ds = objUserGroups.GetAvailableFields()

                Dim dtTable1 As DataTable = ds.Tables(0)

                Dim dtTab As DataTable
                dtTab = Session("DefaultTab")

                For Each row As DataRow In dtTable1.Rows
                    Select Case row.Item("VcLinkName").ToString
                        Case "MyLeads"
                            row.Item("VcLinkName") = "My " & dtTab.Rows(0).Item("vcLead").ToString & "s"
                        Case "WebLeads"
                            row.Item("VcLinkName") = "Web " & dtTab.Rows(0).Item("vcLead").ToString & "s"
                        Case "PublicLeads"
                            row.Item("VcLinkName") = "Public " & dtTab.Rows(0).Item("vcLead").ToString & "s"
                        Case "Contacts"
                            row.Item("VcLinkName") = dtTab.Rows(0).Item("vcContact").ToString & "s"
                        Case "Prospects"
                            row.Item("VcLinkName") = dtTab.Rows(0).Item("vcProspect").ToString & "s"
                        Case "Accounts"
                            row.Item("VcLinkName") = dtTab.Rows(0).Item("vcAccount").ToString & "s"
                    End Select

                    If CCommon.ToBool(row.Item("bitInitialPage")) = True Then
                        row.Item("VcLinkName") = "(*) " & row.Item("VcLinkName")
                    End If
                Next

                dtTable1.AcceptChanges()

                lstAddfldFav.DataSource = dtTable1
                lstAddfldFav.DataTextField = "VcLinkName"
                lstAddfldFav.DataValueField = "Id"
                lstAddfldFav.DataBind()
                If ds IsNot Nothing AndAlso ds.Tables(1).Rows.Count > 0 Then
                    chkDefaultPage.Checked = CCommon.ToBool(ds.Tables(1).Rows(0)(0))
                End If
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub


    Sub PopulateAuthorizedGroupListData()
        Try
            ddlGroup.Items.Clear()  'Clear all the items
            Dim objUserGroups As New UserGroups
            Dim dtGroupName As DataTable
            objUserGroups.DomainID = Session("DomainID")
            objUserGroups.SelectedGroupTypes = "1,4"
            dtGroupName = objUserGroups.GetAutorizationGroup

            ddlGroup.DataSource = dtGroupName.DataSet.Tables(0).DefaultView
            ddlGroup.DataTextField = "vcGroupName"
            ddlGroup.DataValueField = "numGroupId"
            ddlGroup.DataBind()
            ddlGroup.Items.Insert(0, "--Select One--")
            ddlGroup.Items.FindByText("--Select One--").Value = 0

            If Session("UserGroupID") IsNot Nothing Then
                If ddlGroup.Items.FindByValue(Session("UserGroupID")) IsNot Nothing Then
                    ddlGroup.ClearSelection()
                    ddlGroup.Items.FindByValue(Session("UserGroupID")).Selected = True
                End If
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub BindTab()
        Try
            Dim dtTabData As DataTable
            Dim objTab As New Tabs
            objTab.GroupID = ddlGroup.SelectedValue
            objTab.RelationshipID = 0
            objTab.DomainID = Session("DomainID")
            objTab.intMode = 1
            dtTabData = objTab.GetTabData()

            Dim dtTabView As New DataView
            dtTabView = dtTabData.DefaultView
            dtTabView.Sort = "numTabName Asc"
            dtTabData = dtTabView.ToTable()

            ddlTab.DataSource = dtTabData
            ddlTab.DataTextField = "numTabName"
            ddlTab.DataValueField = "numTabID"
            ddlTab.DataBind()

            ddlTab.Items.Insert(0, New ListItem("--Select One--", "0"))
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub BindGrid()
        Try
            Dim dtTable As DataTable
            If objCommon Is Nothing Then objCommon = New CCommon
            objCommon.GroupID = ddlGroup.SelectedValue
            objCommon.DomainID = Session("DomainID")
            objCommon.TabId = ddlTab.SelectedValue

            If ddlTab.SelectedValue = "7" Then 'Relationships
                dgNodes.Visible = False
                pnlRelationship.Visible = True
                objCommon.UserCntID = Session("UserContactID")
                dtTable = objCommon.GetTreeNodesForRelationship(0)

                radTV.DataTextField = "vcNodeName"
                radTV.DataValueField = "ID"
                radTV.DataFieldID = "ID"
                radTV.DataFieldParentID = "numParentID"
                radTV.DataSource = dtTable
                radTV.DataBind()
                radTV.ExpandAllNodes()

                Dim dr() As DataRow
                For Each node As RadTreeNode In radTV.GetAllNodes
                    dr = dtTable.Select("ID=" & node.Value)
                    If Not dr Is Nothing AndAlso dr.Length > 0 Then
                        node.Checked = CCommon.ToBool(dr(0)("bitVisible"))
                        node.Attributes.Add("numPageNavID", CCommon.ToString(dr(0)("numPageNavID")))
                        node.Attributes.Add("numListItemID", CCommon.ToString(dr(0)("numListItemID")))
                        node.Attributes.Add("numParentID", CCommon.ToString(dr(0)("numParentID")))
                        node.Attributes.Add("tintType", CCommon.ToString(dr(0)("tintType")))
                        node.Attributes.Add("numOriginalParentID", CCommon.ToString(dr(0)("numOriginalParentID")))
                    End If
                Next
            Else
                dgNodes.Visible = True
                pnlRelationship.Visible = False
                dtTable = objCommon.GetPageNavigationAuthorizationDetails
                dgNodes.DataSource = dtTable
                dgNodes.DataBind()
            End If


        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub DisplayError(ByVal ex As String)
        lblError.Text = ex
        divError.Style.Remove("display")
    End Sub

#End Region

#Region "BUTTON EVENTS"

    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            Dim dtFinal As New DataTable

            If ddlTab.SelectedValue = "7" Then
                dtFinal.Columns.Add("numPageNavID")
                dtFinal.Columns.Add("numListItemID")
                dtFinal.Columns.Add("bitVisible")
                dtFinal.Columns.Add("numParentID")
                dtFinal.Columns.Add("numOrder")
                dtFinal.Columns.Add("tintType")
                dtFinal.AcceptChanges()

                Dim dr As DataRow
                Dim nodes As RadTreeNode = radTV.SelectedNode

                Dim i As Integer = 0
                For Each node As RadTreeNode In radTV.GetAllNodes()
                    dr = dtFinal.NewRow()
                    dr("numPageNavID") = If(String.IsNullOrEmpty(node.Attributes("numPageNavID")), DBNull.Value, CCommon.ToLong(node.Attributes("numPageNavID")))
                    dr("numListItemID") = If(String.IsNullOrEmpty(node.Attributes("numListItemID")), DBNull.Value, CCommon.ToLong(node.Attributes("numListItemID")))
                    dr("bitVisible") = CCommon.ToBool(node.Checked)
                    dr("numParentID") = CCommon.ToLong(node.Attributes("numOriginalParentID"))
                    dr("tintType") = CCommon.ToInteger(node.Attributes("tintType"))
                    dr("numOrder") = CCommon.ToInteger(i)
                    dtFinal.Rows.Add(dr)

                    i = i + 1
                Next

                dtFinal.AcceptChanges()
            Else
                dtFinal.Columns.Add("numPageNavID")
                dtFinal.Columns.Add("bitVisible")
                dtFinal.Columns.Add("tintType")
                dtFinal.AcceptChanges()

                Dim chkTreeNode As CheckBox = Nothing
                Dim hdnPageNavID As HiddenField = Nothing
                Dim hdnType As HiddenField = Nothing
                Dim drRow As DataRow = Nothing
                If dgNodes.Items.Count > 0 Then
                    For iCnt As Integer = 0 To dgNodes.Items.Count - 1
                        drRow = dtFinal.NewRow

                        hdnPageNavID = DirectCast(dgNodes.Items(iCnt).FindControl("hdnPageNavID"), HiddenField)
                        If hdnPageNavID IsNot Nothing Then
                            drRow("numPageNavID") = CCommon.ToLong(hdnPageNavID.Value)
                        Else
                            drRow("numPageNavID") = 0
                        End If

                        chkTreeNode = DirectCast(dgNodes.Items(iCnt).FindControl("chkSelect"), CheckBox)
                        If chkTreeNode IsNot Nothing AndAlso chkTreeNode.Checked = True Then
                            drRow("bitVisible") = CCommon.ToBool(chkTreeNode.Checked)
                        Else
                            drRow("bitVisible") = CCommon.ToBool(False)
                        End If

                        hdnType = DirectCast(dgNodes.Items(iCnt).FindControl("hdnType"), HiddenField)
                        If hdnType IsNot Nothing Then
                            drRow("tintType") = CCommon.ToShort(hdnType.Value)
                        Else
                            drRow("tintType") = 0
                        End If

                        If CCommon.ToLong(drRow("numPageNavID")) > 0 Then dtFinal.Rows.Add(drRow)
                    Next
                    dtFinal.AcceptChanges()

                End If
            End If

            Dim dsFinal As New DataSet
            dsFinal.Tables.Add(dtFinal)
            dsFinal.AcceptChanges()

            Dim listItemGroup As New System.Collections.Generic.List(Of Long)
            If ddlGroup.SelectedValue <> "0" AndAlso CCommon.ToLong(ddlGroup.SelectedValue) > 0 Then
                listItemGroup.Add(ddlGroup.SelectedValue)
            End If

            For Each item As Telerik.Web.UI.RadComboBoxItem In rcbGroups.CheckedItems
                listItemGroup.Add(item.Value)
            Next

            For Each groupID As Long In listItemGroup
                objCommon.GroupID = groupID
                objCommon.TabId = CCommon.ToLong(ddlTab.SelectedValue)
                objCommon.DomainID = DomainID
                objCommon.xmlStr = dsFinal.GetXml()

                objCommon.ManagePageNavigationAuthorization()
            Next

            ScriptManager.RegisterClientScriptBlock(Me, Me.Page.GetType(), "Saved", "alert('Permissions saved successfully.');", True)

            BindGrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Private Sub btnMoveDown_Click(sender As Object, e As EventArgs) Handles btnMoveDown.Click
        Try
            Dim currentNode As RadTreeNode = radTV.SelectedNode
            Dim nextNode As RadTreeNode = radTV.SelectedNode.Next

            Dim nextNodeParent As RadTreeNode = nextNode.Parent
            Dim index As Integer = nextNodeParent.Nodes.IndexOf(nextNode)

            radTV.SelectedNode.Remove()
            nextNodeParent.Nodes.Insert(index, currentNode)

            currentNode.Selected = True
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Private Sub btnMoveUp_Click(sender As Object, e As EventArgs) Handles btnMoveUp.Click
        Try
            Dim currentNode As RadTreeNode = radTV.SelectedNode
            Dim previousNode As RadTreeNode = radTV.SelectedNode.Prev

            Dim previousNodeParent As RadTreeNode = previousNode.Parent
            Dim index As Integer = previousNodeParent.Nodes.IndexOf(previousNode)

            radTV.SelectedNode.Remove()
            previousNodeParent.Nodes.Insert(index, currentNode)

            currentNode.Selected = True
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Private Sub ddlSrtCutGroup_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlSrtCutGroup.SelectedIndexChanged
        Try
            ShortCutBindTab()
            PopulateShortCutData()

            rcbShrCutGroups.Items.Clear()
            Dim radItem As Telerik.Web.UI.RadComboBoxItem
            For Each item As ListItem In ddlSrtCutGroup.Items
                If item.Value <> "0" AndAlso item.Value <> ddlSrtCutGroup.SelectedValue Then
                    radItem = New Telerik.Web.UI.RadComboBoxItem
                    radItem.Text = item.Text
                    radItem.Value = item.Value
                    rcbShrCutGroups.Items.Add(radItem)
                End If
            Next
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub ddlSrtCutTab_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlSrtCutTab.SelectedIndexChanged
        Try
            PopulateShortCutData()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub btnSetAsInitial_Click(sender As Object, e As EventArgs) Handles btnSetAsInitial.Click
        Try
            Dim strInitialPage = CCommon.ToString(txtInitialPage.Text)

            Dim dtFav As New DataTable

            Dim i As Integer
            Dim dr As DataRow

            dtFav.Columns.Add("numLinkId")
            dtFav.Columns.Add("numOrder")
            dtFav.Columns.Add("bitInitialPage")
            dtFav.Columns.Add("tintLinkType")

            Dim strValesFav As String()
            strValesFav = txthiddenFav.Text.Split(",")

            Dim strValesLink As String()

            Dim ds As New DataSet
            For i = 0 To strValesFav.Length - 2
                dr = dtFav.NewRow
                strValesLink = strValesFav(i).Split("~")

                dr("numLinkId") = strValesLink(0)
                dr("tintLinkType") = strValesLink(1)

                dr("numOrder") = i + 1
                dr("bitInitialPage") = IIf(strValesFav(i) = strInitialPage, True, False)
                dtFav.Rows.Add(dr)
            Next

            dtFav.TableName = "Table"
            ds.Tables.Add(dtFav.Copy)


            Dim objShortCutBar As New CShortCutBar

            Dim listItemGroup As New System.Collections.Generic.List(Of Long)
            If ddlSrtCutGroup.SelectedValue <> "0" AndAlso CCommon.ToLong(ddlSrtCutGroup.SelectedValue) > 0 Then
                listItemGroup.Add(ddlSrtCutGroup.SelectedValue)
            End If

            For Each item As Telerik.Web.UI.RadComboBoxItem In rcbShrCutGroups.CheckedItems
                listItemGroup.Add(item.Value)
            Next
            Dim Saved As Boolean = False
            For Each groupID As Long In listItemGroup
                objShortCutBar = New CShortCutBar
                objShortCutBar.GroupId = groupID
                objShortCutBar.DomainID = Session("DomainID")
                objShortCutBar.StrFav = ds.GetXml
                objShortCutBar.TabId = ddlSrtCutTab.SelectedValue
                objShortCutBar.DefaultTab = chkDefaultPage.Checked
                objShortCutBar.SaveAllowedFields()

                Saved = True
            Next

            If Saved = True Then PopulateShortCutData()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

#End Region

#Region "DROP DOWN EVENTS"

    Private Sub ddlGroup_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGroup.SelectedIndexChanged
        Try
            BindTab()
            BindGrid()

            rcbGroups.Items.Clear()
            Dim radItem As Telerik.Web.UI.RadComboBoxItem
            For Each item As ListItem In ddlGroup.Items
                If item.Value <> "0" AndAlso item.Value <> ddlGroup.SelectedValue Then
                    radItem = New Telerik.Web.UI.RadComboBoxItem
                    radItem.Text = item.Text
                    radItem.Value = item.Value
                    rcbGroups.Items.Add(radItem)
                End If
            Next
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Private Sub ddlTab_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlTab.SelectedIndexChanged
        Try
            BindGrid()
            'ClientScript.RegisterStartupScript(Me.GetType, "resize", "windowResize();", True)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

#End Region

End Class