﻿Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Contacts
Imports System.Text

Namespace BACRM.UserInterface.Admin

    Public Class frmAdvancedSearchNew
        Inherits BACRMPage

#Region "Page Events"

        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            Try
                If Not Page.IsPostBack Then
                    Dim m_aryRightsSharedWithViews() As Integer

                    m_aryRightsSharedWithViews = GetUserRightsForPage_Other(9, 13)
                    If m_aryRightsSharedWithViews(RIGHTSTYPE.VIEW) = 0 Then divSharedWith.Visible = False


                    BindEmployees()
                    LoadUserSavedAdvancedSearch()

                    If Not String.IsNullOrEmpty(Session("AdvancedSearchCondition")) Then
                        hdnSearchCondition.Value = Session("AdvancedSearchCondition")
                    End If

                    If CCommon.ToInteger(Session("AdvancedSearchSavedSearchID")) > 0 Then
                        ddlSavedSearch.SelectedValue = Session("AdvancedSearchSavedSearchID")
                    End If
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

#End Region

#Region "Private Methods"

        Private Sub BindEmployees()
            Try
                Dim objContact As New CContacts
                objContact.DomainID = Session("DomainID")
                Dim dtEmployees As DataTable = objContact.EmployeeList

                If Not dtEmployees Is Nothing AndAlso dtEmployees.Rows.Count > 0 Then
                    Dim listItem As ListItem

                    For Each dr As DataRow In dtEmployees.Rows
                        listItem = New ListItem
                        listItem.Text = CCommon.ToString(dr("vcUserName"))
                        listItem.Value = CCommon.ToString(dr("numContactID"))
                        lbAvailableUsers.Items.Add(listItem)
                    Next
                End If
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Private Sub LoadUserSavedAdvancedSearch()
            Try
                Dim objSearch As New FormGenericAdvSearch
                objSearch.byteMode = 1
                objSearch.SearchID = 0
                objSearch.DomainID = Session("DomainID")
                objSearch.UserCntID = Session("UserContactID")
                ddlSavedSearch.DataSource = objSearch.ManageSavedSearch()
                ddlSavedSearch.DataTextField = "vcSearchName"
                ddlSavedSearch.DataValueField = "numSearchID"
                ddlSavedSearch.DataBind()

                ddlSavedSearch.Items.Insert(0, New ListItem("-- Select One --", 0))
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Private Sub SearchOrganizations()
            Try
                Dim obj As New FormGenericAdvSearch
                Dim str As String = obj.SearchOrganizations(CCommon.ToLong(Session("DomainID")), CCommon.ToInteger(Session("ClientMachineUTCTimeOffset")), hdnSearchCondition.Value)

                Session("WhereCondition") = str
                Response.Redirect("~/Admin/frmAdvancedSearchRes.aspx?displayColumns=" & hdnDisplayColumns.Value, False)
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Private Sub SearchOppOrders()
            Try
                Dim obj As New FormGenericAdvSearch
                Dim str As String = obj.SearchOppOrders(CCommon.ToLong(Session("DomainID")), CCommon.ToInteger(Session("ClientMachineUTCTimeOffset")), hdnSearchCondition.Value)

                Session("WhereContditionOpp") = str
                Session("SavedSearchCondition") = str
                Response.Redirect("~/Admin/frmItemSearchRes.aspx?displayColumns=" & hdnDisplayColumns.Value, False)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Private Sub SearchCases()
            Try
                Dim obj As New FormGenericAdvSearch
                Dim str As String = obj.SearchCases(CCommon.ToLong(Session("DomainID")), CCommon.ToInteger(Session("ClientMachineUTCTimeOffset")), hdnSearchCondition.Value)

                Session("WhereContditionCase") = str
                Response.Redirect("~/Admin/frmAdvCaseRes.aspx?displayColumns=" & hdnDisplayColumns.Value, False)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Private Sub SearchProjects()
            Try
                Dim obj As New FormGenericAdvSearch
                Dim str As String = obj.SearchProjects(CCommon.ToLong(Session("DomainID")), CCommon.ToInteger(Session("ClientMachineUTCTimeOffset")), hdnSearchCondition.Value)

                Session("WhereContditionPro") = str
                Response.Redirect("~/Admin/frmAdvProjectsRes.aspx?displayColumns=" & hdnDisplayColumns.Value, False)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Private Sub SearchItems()
            Try
                Dim obj As New FormGenericAdvSearch
                Dim str As String = obj.SearchItems(CCommon.ToLong(Session("DomainID")), CCommon.ToInteger(Session("ClientMachineUTCTimeOffset")), hdnSearchCondition.Value)

                Session("WhereConditionItem") = str
                Session("SavedSearchCondition") = str
                Response.Redirect(Page.ResolveClientUrl("~/Admin/frmAdvSerInvItemsRes.aspx") & "?displayColumns=" & hdnDisplayColumns.Value, False)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Private Sub SearchFinancialTransactions()
            Try
                Dim obj As New FormGenericAdvSearch
                Dim str As String = obj.SearchFinancialTransactions(CCommon.ToLong(Session("DomainID")), CCommon.ToInteger(Session("ClientMachineUTCTimeOffset")), hdnSearchCondition.Value)

                Session("WhereContditionAcc") = str
                Session("SavedSearchCondition") = str
                Response.Redirect("~/Admin/frmAdvAccRes.aspx?displayColumns=" & hdnDisplayColumns.Value, False)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Private Sub DisplayError(ByVal exception As String)
            Try
                DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = exception
                DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
                DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Focus()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

#End Region

#Region "Event Handlers"

        Private Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnSearch.Click
            Try
                If Not String.IsNullOrEmpty(hdnSearchCondition.Value) Then
                    Session("AdvancedSearchCondition") = hdnSearchCondition.Value
                    Session("AdvancedSearchDisplayColumns") = hdnDisplayColumns.Value
                    Session("AdvancedSearchSavedSearchID") = ddlSavedSearch.SelectedValue

                    Dim serializer As New System.Web.Script.Serialization.JavaScriptSerializer
                    Dim listConditions As System.Collections.Generic.List(Of AdvancedSearchCondition) = serializer.Deserialize(Of System.Collections.Generic.List(Of AdvancedSearchCondition))(hdnSearchCondition.Value)

                    If Not listConditions Is Nothing AndAlso listConditions.Count > 0 Then
                        Dim listModules As System.Collections.Generic.IEnumerable(Of Long) = listConditions.Select(Function(x) x.FormID).Distinct()

                        Dim searchModule As Long


                        If listModules.Count > 1 Then
                            searchModule = listModules.Where(Function(x) x <> 1).First()
                        Else
                            searchModule = listModules(0)
                        End If

                        Select Case searchModule
                            Case 1
                                SearchOrganizations()
                            Case 15
                                SearchOppOrders()
                            Case 17
                                SearchCases()
                            Case 18
                                SearchProjects()
                            Case 29
                                SearchItems()
                            Case 59
                                SearchFinancialTransactions()
                        End Select
                    Else
                        ScriptManager.RegisterClientScriptBlock(Me, Me.Page.GetType(), "Validation", "alert('Add search condition(s)');", True)
                    End If
                Else
                    ScriptManager.RegisterClientScriptBlock(Me, Me.Page.GetType(), "Validation", "alert('Add search condition(s)');", True)
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

#End Region


        Private Class AdvancedSearchCondition
            Public Property FormID As Long
            Public Property FieldID As Long
            Public Property AssociatedControlType As String
            Public Property FieldName As String
            Public Property ListItemType As String
            Public Property LookBackTableName As String
            Public Property DbColumnName As String
            Public Property SearchOperator As Integer
            Public Property SearchValue As String
            Public Property SearchFrom As String
            Public Property SearchTo As String
            Public Property Prefix As String
            Public Property FieldType As String
        End Class

        Private Class AdvancedSearchConditionCollection
            Public Property SearchConditions As System.Collections.Generic.IEnumerable(Of AdvancedSearchCondition)
        End Class

    End Class



End Namespace