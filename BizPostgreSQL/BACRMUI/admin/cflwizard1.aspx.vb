'' Modified By anoop jayaraj
Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Common
Namespace BACRM.UserInterface.Admin
    Public Class cflwizard1
        Inherits BACRMPage
        Dim cts As New DropDownList
        Protected WithEvents subgrp As System.Web.UI.WebControls.DropDownList
        Protected WithEvents btnBack As System.Web.UI.WebControls.Button
        Protected WithEvents btnNext As System.Web.UI.WebControls.Button
        Protected WithEvents btnCancel As System.Web.UI.WebControls.Button
        Dim PageId As String
        Dim location As String
#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Try
                PageId = Session("PageId")
                location = GetQueryStringVal("location")
                If IsPostBack = False Then loadgrp(PageId)
                
                If Session("TabId") <> "" Then subgrp.Items.FindByValue(Session("TabId")).Selected = True
                Session("TabId") = ""
                btnNext.Attributes.Add("onclick", "return Next()")
                btnBack.Attributes.Add("onclick", "return Back()")
                btnCancel.Attributes.Add("onclick", "return Close()")
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub loadgrp(ByVal grpid As String)
            Try
                'If grpid <> 1 Then 'When its Lead/Prospects/Accounts allow only custom field to be added to main detail section
                Dim dtTab As DataTable
                Dim objCusField As New CustomFields()
                objCusField.locId = grpid
                objCusField.DomainID = Session("DomainID")
                dtTab = objCusField.Tabs
                subgrp.DataSource = dtTab
                subgrp.DataTextField = "grp_name"
                subgrp.DataValueField = "grp_id"
                subgrp.DataBind()
                'End If
                If subgrp.Items.Count = 0 And (grpid = 12 Or grpid = 13 Or grpid = 14) Then
                    pnlSMTPError.Visible = True
                End If
                subgrp.Items.Insert(0, "--Select One--")
                subgrp.Items.FindByText("--Select One--").Value = -1
                If Not (grpid = 12 Or grpid = 13 Or grpid = 14) Then 'When its seperate Location like accounts then allow custom fields to be added only to custom subtabs

                    subgrp.Items.Insert(1, "Add to Main Detail Section")
                    subgrp.Items.FindByText("Add to Main Detail Section").Value = 0

                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub subgrp_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles subgrp.SelectedIndexChanged
            Try
                Session("TabId") = subgrp.SelectedItem.Value
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub btnNext_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNext.Click
            Try
                Session("TabId") = subgrp.SelectedItem.Value
                Response.Redirect("../admin/cflwizard3.aspx?location=" + location)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

    End Class
End Namespace