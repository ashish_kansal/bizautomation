﻿Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Admin

Public Class frmImportFieldsSynonyms
    Inherits BACRMPage

#Region "Page Events"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            lblError.Text = ""
            divError.Style.Add("display", "none")

            If Not Page.IsPostBack Then
                BindDropDown()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

#End Region

#Region "Private Methods"

    Private Sub BindDropDown()
        Try
            Dim objImport As New ImportWizard
            objImport.ImportMasterID = 0
            objImport.Mode = 1
            ddlImportType.DataSource = objImport.GetImportMasterCategories().Tables(0)
            ddlImportType.DataTextField = "vcImportName"
            ddlImportType.DataValueField = "intImportMasterID"
            ddlImportType.DataBind()
            ddlImportType.Items.Insert(0, New ListItem("--Select One--", "0"))
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub BindSynonymsList()
        Try
            Dim objDycFieldMasterSynonym As New DycFieldMasterSynonym
            objDycFieldMasterSynonym.DomainID = CCommon.ToLong(Session("DomainID"))
            objDycFieldMasterSynonym.FieldID = CCommon.ToLong(ddlFields.SelectedValue.Split("~")(0))
            objDycFieldMasterSynonym.IsCustomField = CCommon.ToBool(ddlFields.SelectedValue.Split("~")(1))
            gvSynonyms.DataSource = objDycFieldMasterSynonym.GetByFieldID()
            gvSynonyms.DataBind()
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub DisplayError(ByVal errorMessage As String)
        Try
            lblError.Text = errorMessage
            divError.Style.Add("display", "")
        Catch ex As Exception
            'DO NOT THROW ERROR
        End Try
    End Sub

#End Region

#Region "Event Handlers"

    Protected Sub btnAdd_Click(sender As Object, e As EventArgs)
        Try
            If CCommon.ToLong(ddlImportType.SelectedValue) = 0 Then
                ddlImportType.Focus()
                ScriptManager.RegisterClientScriptBlock(Me, Me.Page.GetType(), "SelectImportType", "alert('Select import type')", True)
            ElseIf ddlFields.SelectedValue = "0" Then
                ddlFields.Focus()
                ScriptManager.RegisterClientScriptBlock(Me, Me.Page.GetType(), "SelectImportType", "alert('Select field')", True)
            ElseIf String.IsNullOrEmpty(txtValue.Text.Trim()) Then
                txtValue.Focus()
                ScriptManager.RegisterClientScriptBlock(Me, Me.Page.GetType(), "SelectImportType", "alert('Enter synonym value')", True)
            Else
                Dim objDycFieldMasterSynonym As New DycFieldMasterSynonym
                objDycFieldMasterSynonym.DomainID = CCommon.ToLong(Session("DomainID"))
                objDycFieldMasterSynonym.FieldID = CCommon.ToLong(ddlFields.SelectedValue.Split("~")(0))
                objDycFieldMasterSynonym.IsCustomField = CCommon.ToLong(ddlFields.SelectedValue.Split("~")(1))
                objDycFieldMasterSynonym.Synonym = txtValue.Text.Trim()
                objDycFieldMasterSynonym.Save()

                txtValue.Text = ""
                BindSynonymsList()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Protected Sub btnDelete_Click(sender As Object, e As EventArgs)
        Try
            If String.IsNullOrEmpty(hdnSelectedRecords.Value.Trim()) Then
                ScriptManager.RegisterClientScriptBlock(Me, Me.Page.GetType(), "SelectImportType", "alert('Select at least one record to delete')", True)
            Else
                Dim objDycFieldMasterSynonym As New DycFieldMasterSynonym
                objDycFieldMasterSynonym.DomainID = CCommon.ToLong(Session("DomainID"))
                objDycFieldMasterSynonym.SelectedRecords = hdnSelectedRecords.Value.Trim()
                objDycFieldMasterSynonym.Delete()

                BindSynonymsList()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Protected Sub ddlFields_SelectedIndexChanged(sender As Object, e As EventArgs)
        Try
            gvSynonyms.DataSource = Nothing
            gvSynonyms.DataBind()

            If ddlFields.SelectedValue <> "0" Then
                BindSynonymsList()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Protected Sub ddlImportType_SelectedIndexChanged(sender As Object, e As EventArgs)
        Try
            ddlFields.Items.Clear()

            If CCommon.ToLong(ddlImportType.SelectedValue) > 0 Then
                Dim objImport As New ImportWizard
                objImport.DomainId = Session("DomainID")
                objImport.ImportMasterID = CCommon.ToLong(ddlImportType.SelectedValue)
                objImport.ImportType = 4
                objImport.Mode = 2
                objImport.ImportFileID = 0
                Dim ds As DataSet = objImport.GetImportMasterCategories()

                If Not ds Is Nothing AndAlso ds.Tables.Count > 1 AndAlso ds.Tables(1).Rows.Count > 0 Then
                    ddlFields.DataSource = ds.Tables(1).AsEnumerable().OrderBy(Function(x) x("vcFormFieldName")).AsDataView()
                    ddlFields.DataTextField = "vcFormFieldName"
                    ddlFields.DataValueField = "KeyFieldID"
                    ddlFields.DataBind()
                End If
            End If

            ddlFields.Items.Insert(0, New ListItem("--Select One--", "0"))
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Protected Sub gvSynonyms_RowDataBound(sender As Object, e As GridViewRowEventArgs)
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim drv As DataRowView = DirectCast(e.Row.DataItem, DataRowView)

                If CCommon.ToBool(drv("bitDefault")) Then
                    DirectCast(e.Row.FindControl("chkSelect"), CheckBox).Visible = False
                End If
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

#End Region
    
End Class