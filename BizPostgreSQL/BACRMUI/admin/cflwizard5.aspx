<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" Codebehind="cflwizard5.aspx.vb" MasterPageFile="~/common/Popup.Master" Inherits="BACRM.UserInterface.Admin.cflwizard5" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
				
       
		<title>Custom Field Wizard</title>
		
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
   
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageTitle" runat="server" ClientIDMode="Static">
    Step 5
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="Content" runat="server" ClientIDMode="Static">
 		<table id="Table1" height="100%" cellSpacing="0" cellPadding="0" width="400px" border="0">
				<tr>
					<td width="5" bgColor="#506490">&nbsp;</td>
					</td>
					<td background="../images/left_side_pic.gif" colSpan="3" height="20"><font face="Arial" color="#ffffff" size="2"><b>&nbsp;&nbsp;Step&nbsp;5 
								of &nbsp;6 - Choose Order</b></font></td>
				</tr>
				<tr>
					<!-- <td width="20%"><IMG src="../images/pic-2.jpg"></td> -->
					<td width="5" bgColor="#506490">&nbsp;</td>
					<td valign="top">
						<table id="Table2">
							<tr>
								<td width="80%" class="Normal2"><br>
									&nbsp;&nbsp;By choosing order placement you are also selecting the tab view
									<BR>
									&nbsp;&nbsp;in which the field will appear. Starting from left to right on the
									<BR>
									&nbsp;&nbsp;custom tab page, each tab view (of which there can be a
									<br>
									&nbsp;&nbsp;maximum of 8) can contain 9 fields.<br>
									<br>
								</td>
							</tr>
							<tr>
								<td width="80%">&nbsp;
									<br>
								</td>
							</tr>
							<tr>
								<td>
									<table>
										<tr>
											<td valign="middle" class="Text_bold">Choose Order</td>
											<td><asp:dropdownlist id="cfword" runat="server" Width="120px" CssClass="Signup">
													<asp:ListItem Value="0">-- Select One --</asp:ListItem>
													<asp:ListItem Value="1">1</asp:ListItem>
													<asp:ListItem Value="2">2</asp:ListItem>
													<asp:ListItem Value="3">3</asp:ListItem>
													<asp:ListItem Value="4">4</asp:ListItem>
													<asp:ListItem Value="5">5</asp:ListItem>
													<asp:ListItem Value="6">6</asp:ListItem>
													<asp:ListItem Value="7">7</asp:ListItem>
													<asp:ListItem Value="8">8</asp:ListItem>
													<asp:ListItem Value="9">9</asp:ListItem>
												</asp:dropdownlist>
											</td>
											<td></td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td></td>
							</tr>
							<tr>
								<td></td>
							</tr>
							<tr>
								<td align="center"><A href="cflwizard4.aspx"><IMG src="../images/button_back.jpg" border="0"></A><asp:imagebutton id="ImageButton1" runat="server" ImageUrl="../images/button_next.jpg"></asp:imagebutton><A href="javascript:window.close()"><IMG src="../images/button_cancel.jpg" border="0"></A>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
							</tr>
						</table>
					</td>
					<td width="5" bgColor="#506490">&nbsp;</td>
				</tr>
				<tr>
					<td style="HEIGHT: 5px" bgColor="#506490" colSpan="3"></td>
				</tr>
			</table>
</asp:Content>
