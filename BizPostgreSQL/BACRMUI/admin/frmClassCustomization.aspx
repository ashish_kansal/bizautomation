﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/common/Popup.Master"
    CodeBehind="frmClassCustomization.aspx.vb" Inherits="BACRM.UserInterface.Admin.frmClassCustomization" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content3" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <script type="text/javascript">
        var gridId = "";
        function isMouseOverGrid(target) {
            parentNode = target;
            while (parentNode != null) {
                if (parentNode.id == gridId) {
                    return parentNode;
                }
                parentNode = parentNode.parentNode;
            }

            return null;
        }

        function onNodeDragging(sender, args) {
            var target = args.get_htmlElement();

            if (!target) return;

            if (target.tagName == "INPUT") {
                target.style.cursor = "hand";
            }

            var grid = isMouseOverGrid(target)
            if (grid) {
                grid.style.cursor = "hand";
            }
        }

        function dropOnHtmlElement(args) {
            if (droppedOnInput(args))
                return;

            if (droppedOnGrid(args))
                return;
        }

        function droppedOnGrid(args) {
            var target = args.get_htmlElement();

            while (target) {
                if (target.id == gridId) {
                    args.set_htmlElement(target);
                    return;
                }

                target = target.parentNode;
            }
            args.set_cancel(true);
        }

        function droppedOnInput(args) {
            var target = args.get_htmlElement();
            if (target.tagName == "INPUT") {
                target.style.cursor = "default";
                target.value = args.get_sourceNode().get_text();
                args.set_cancel(true);
                return true;
            }
        }

        function dropOnTree(args) {
            var text = "";

            if (args.get_sourceNodes().length) {
                var i;
                for (i = 0; i < args.get_sourceNodes().length; i++) {
                    var node = args.get_sourceNodes()[i];
                    text = text + ', ' + node.get_text();
                }
            }
        }

        function clientSideEdit(sender, args) {
            var destinationNode = args.get_destNode();

            if (destinationNode) {
                var firstTreeView = $find('RadTreeView1');

                firstTreeView.trackChanges();
                var sourceNodes = args.get_sourceNodes();
                for (var i = 0; i < sourceNodes.length; i++) {
                    var sourceNode = sourceNodes[i];
                    sourceNode.get_parent().get_nodes().remove(sourceNode);

                    if (args.get_dropPosition() == "over") destinationNode.get_nodes().add(sourceNode);
                    if (args.get_dropPosition() == "above") insertBefore(destinationNode, sourceNode);
                    if (args.get_dropPosition() == "below") insertAfter(destinationNode, sourceNode);
                }
                destinationNode.set_expanded(true);
                firstTreeView.commitChanges();

            }
        }

        function insertBefore(destinationNode, sourceNode) {
            var destinationParent = destinationNode.get_parent();
            var index = destinationParent.get_nodes().indexOf(destinationNode);
            destinationParent.get_nodes().insert(index, sourceNode);
        }

        function insertAfter(destinationNode, sourceNode) {
            var destinationParent = destinationNode.get_parent();
            var index = destinationParent.get_nodes().indexOf(destinationNode);
            destinationParent.get_nodes().insert(index + 1, sourceNode);
        }

        function onNodeDropping(sender, args) {
            var dest = args.get_destNode();
            if (dest) {

                clientSideEdit(sender, args);
                args.set_cancel(true);
                return;

                dropOnTree(args);
            }
            else {
                dropOnHtmlElement(args);
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <div class="input-part">
        <div class="right-input">
            <asp:Button Text="Save" runat="server" CssClass="button" ID="btnSave" />
            <asp:Button Text="Close" runat="server" CssClass="button" ID="btnClose" OnClientClick="Close();" />&nbsp;&nbsp;
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="PageTitle" runat="server" ClientIDMode="Static">
    Arrange Class
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="Content" runat="server" ClientIDMode="Static">
  <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <table width="600px">
        <tr>
            <td class="normal4" align="center">
                <asp:Literal ID="litMessage" runat="server" EnableViewState="False"></asp:Literal>
            </td>
        </tr>
        <tr>
            <td>
                <telerik:RadTreeView ID="RadTreeView1" runat="server" ClientIDMode="Static" EnableDragAndDrop="true"
                    EnableDragAndDropBetweenNodes="true" OnClientNodeDropping="onNodeDropping" OnClientNodeDragging="onNodeDragging">
                    <DataBindings>
                        <telerik:RadTreeNodeBinding Expanded="True" />
                    </DataBindings>
                </telerik:RadTreeView>
            </td>
        </tr>
    </table>
</asp:Content>
