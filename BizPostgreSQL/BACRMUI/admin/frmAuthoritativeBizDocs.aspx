<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmAuthoritativeBizDocs.aspx.vb"
    Inherits=".frmAuthoritativeBizDocs" MasterPageFile="~/common/Popup.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Authoritative BizDocs </title>
    <script language="javascript" type="text/javascript">
        function Save() {
            if (document.getElementById("ddlPurchaseOpportunity").value == document.getElementById("ddlSalesOpportunity").value) {
                alert("Please Select different BizDocs for Purchase and Sales Opportunity");
                return false;
            }
        }
		
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <div class="input-part">
        <div class="right-input">
            <asp:Button ID="btnSave" Text="Save" CssClass="button" runat="server" Width="50">
            </asp:Button>
            <asp:Button ID="btnClose" Text="Close" CssClass="button" runat="server" Width="50">
            </asp:Button>&nbsp;&nbsp;
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageTitle" runat="server" ClientIDMode="Static">
    Authoritative BizDocs
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="Content" runat="server" ClientIDMode="Static">
    <asp:Table ID="Table2" runat="server" GridLines="None" BorderColor="black" Width="600px"
        CssClass="aspTable" BorderWidth="1" CellSpacing="0" CellPadding="0">
        <asp:TableRow>
            <asp:TableCell VerticalAlign="Top">
                <br>
                <table align="center">
                    <tr>
                        <td class="normal1" align="right">
                            Authoritative BizDoc Type for Sales Order
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlSalesOpportunity" runat="server" CssClass="signup">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="normal1" align="right">
                            Authoritative BizDoc Type for Purchase Order
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlPurchaseOpportunity" runat="server" CssClass="signup">
                            </asp:DropDownList>
                        </td>
                    </tr>
                </table>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
</asp:Content>
