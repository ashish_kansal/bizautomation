<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" Codebehind="frmPortalWorkflow.aspx.vb" Inherits="BACRM.UserInterface.Admin.frmPortalWorkflow"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
	<head  runat="server">
        <link rel="stylesheet" href="~/CSS/master.css" type="text/css" />
		<title>Portal Workflow</title>
	</HEAD>
	<body >
		<form id="Form1" method="post" runat="server">
			<br>
			<table cellSpacing="0" cellPadding="0" width="100%" border="0">
				<tr>
					<td vAlign="bottom">
						<table class="TabStyle">
							<tr>
								<td>&nbsp;&nbsp;&nbsp;Commerce &amp; 
									Portal Workflow Rules Editor&nbsp;&nbsp;&nbsp;
								</td>
							</tr>
						</table>
					</td>
					<td align="right">
					<asp:button id="btnSave" Runat="server" CssClass="button" Text="Save"></asp:button>
					<asp:button id="btnSaveClose" Runat="server" CssClass="button" Text="Save & Close"></asp:button>
						<asp:button id="btnClose" Runat="server" CssClass="button" Text="Close"></asp:button></td>
				</tr>
			</table>
			<asp:table id="Table2" CellPadding="0" CellSpacing="0" BorderWidth="1" Runat="server" Width="100%" CssClass="aspTable" 
				BorderColor="black" GridLines="None" Height="300">
				<asp:TableRow>
					<asp:TableCell VerticalAlign="Top">
						<br>
						<table width="550" cellpadding=5 cellspacing=5>
							<tr>
								<td class="normal1" valign=top >1.&nbsp;
									<asp:CheckBox ID="chkCreateOpp" Runat="server"></asp:CheckBox>
								</td>
								<td class="normal1">
									After registration is completed, if order is abandoned or declined by a credit 
									card processor, do you want to create a record in Biz with the data ? If so, 
									the status will be set to &nbsp;
									<asp:DropDownList ID="ddlStatus" Runat="server" CssClass="signup" Width="130"></asp:DropDownList>
									and the relationship should be set to &nbsp;
									<asp:DropDownList ID="ddlRelationShip" Runat="server" CssClass="signup" Width="130"></asp:DropDownList>
								</td>
							</tr>
							<tr>
								<td class="normal1" valign=top>2.&nbsp;
									<asp:CheckBox ID="chkCreateBizDoc" Runat="server"></asp:CheckBox>
								</td>
								<td class="normal1" valign=top>
									After a Deal Won or Order has been successfully completed, create a BizDoc with 
									the following label:
									<asp:DropDownList ID="ddlBizDocName" Runat="server" CssClass="signup" Width="130"></asp:DropDownList>
									, and send the customer an email with a link to the BizDoc and the username and 
									password required to open it (required that rule #3 be activated).
								</td>
							</tr>
							<tr>
								<td class="normal1" valign=top>3.&nbsp;
									<asp:CheckBox ID="chkAllowAccess" Runat="server"></asp:CheckBox>
								</td>
								<td class="normal1">
									When a new Account is created, automatically check the �Allowed to access the 
									portal� check box in the User Access Permissions section of the admin panel 
									(External Access section), and have Biz create a unique password.
								</td>
							</tr>
							<tr>
								<td class="normal1" valign=top>4.&nbsp;&nbsp;
								</td>
								<td class="normal1">
									If Order is successful, open this page: &nbsp;
									<asp:TextBox ID="txtSuccesfullURL" Runat="server" Width="250" CssClass="signup"></asp:TextBox>
								</td>
							</tr>
							<tr>
								<td class="normal1" align="right">
								</td>
								<td class="normal1">
									If Order is not successful, open this page: &nbsp;
									<asp:TextBox ID="txtUnSuccesfullURL" Runat="server" Width="250" CssClass="signup"></asp:TextBox>
								</td>
							</tr>
						</table>
					</asp:TableCell>
				</asp:TableRow>
			</asp:table>
		</form>
	</body>
</HTML>
