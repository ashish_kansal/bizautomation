﻿Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Common

Public Class frmBackOrderRule
    Inherits BACRMPage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        PopulateAuthorizedGroupListData()
    End Sub

    Sub PopulateAuthorizedGroupListData()
        Try
            lstAvailableGroups.Items.Clear()
            Dim objUserGroups As New UserGroups
            Dim dtAvailableGroup As DataTable
            objUserGroups.DomainID = Session("DomainID")
            objUserGroups.SelectedGroupTypes = "1,4"
            dtAvailableGroup = objUserGroups.GetAutorizationGroup

            lstAvailableGroups.DataSource = dtAvailableGroup
            lstAvailableGroups.DataTextField = "vcGroupName"
            lstAvailableGroups.DataValueField = "numGroupId"
            lstAvailableGroups.DataBind()

            Dim dtBackOrderSelectedGroup As DataTable

            dtBackOrderSelectedGroup = objUserGroups.GetBackOrderSelectedAutorizationGroup

            lstSelectedGroups.DataSource = dtBackOrderSelectedGroup
            lstSelectedGroups.DataTextField = "vcGroupName"
            lstSelectedGroups.DataValueField = "numGroupId"
            lstSelectedGroups.DataBind()

            For iCount = 0 To lstSelectedGroups.Items.Count - 1
                If Not lstAvailableGroups.Items.FindByValue(lstSelectedGroups.Items(iCount).Value) Is Nothing Then
                    lstAvailableGroups.Items.Remove(lstSelectedGroups.Items(iCount))
                End If
            Next

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub btnSaveClose_Click(sender As Object, e As EventArgs)

        Save()

        Dim strScript As String = "<script language=JavaScript>self.close()</script>"
        If (Not Page.IsStartupScriptRegistered("clientScript")) Then
            Page.RegisterStartupScript("clientScript", strScript)
        End If
    End Sub

    Private Sub Save()
        Try
            Dim objUserGroups As New UserGroups
            objUserGroups.DomainID = Session("DomainID")

            Dim strGroupids As String = hdnSelectedGroups.Value

            'For l_index As Integer = 0 To lstSelectedGroups.Items.Count - 1
            '    strGroupids += lstSelectedGroups.Items(l_index).Value
            'Next

            objUserGroups.SaveBackOrderAuthgroup(strGroupids)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
End Class