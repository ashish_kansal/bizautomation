﻿Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Opportunities
Imports System.IO
Imports BACRM.BusinessLogic.Admin
Public Class frmBizDocs
    Inherits BACRMPage


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            GetUserRightsForPage(13, 1)

            If m_aryRightsForPage(RIGHTSTYPE.VIEW) = 0 Then
                Response.Redirect("../admin/authentication.aspx?mesg=AS")
            ElseIf m_aryRightsForPage(RIGHTSTYPE.ADD) = 0 Then
                btnSave.Visible = False
                btnSaveClose.Visible = False
            End If

            If Not IsPostBack Then

                'Dim dtMasterItem As DataTable

                objCommon.sb_FillListFromDB(lstAvailable, 27, Session("DomainID"))

                BindBizDoc(1, lstSale)
                BindBizDoc(2, lstPurchase)
            End If
            btnAddSale.Attributes.Add("OnClick", "return move(lstAvailable,lstSale)")
            btnAddPrchase.Attributes.Add("OnClick", "return move(lstAvailable,lstPurchase)")

            btnRemoveSale.Attributes.Add("OnClick", "return remove1(lstSale,lstAvailable)")
            btnRemovePurchase.Attributes.Add("OnClick", "return remove1(lstPurchase,lstAvailable)")

            btnSave.Attributes.Add("onclick", "return Save()")
            btnSaveClose.Attributes.Add("onclick", "return Save()")
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
    Sub BindBizDoc(ByVal BizocType As Short, ByVal lstBizDoc As ListBox)
        objCommon = New CCommon

        objCommon.DomainID = Session("DomainID")
        objCommon.BizDocType = BizocType

        lstBizDoc.DataSource = objCommon.GetBizDocType
        lstBizDoc.DataTextField = "vcData"
        lstBizDoc.DataValueField = "numListItemID"
        lstBizDoc.DataBind()
    End Sub
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSave.Click
        Try
            Save()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Protected Sub btnSaveClose_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSaveClose.Click
        Try
            Save()
            Response.Write("<script>window.close()</script>")
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
    Sub Save()
        Dim dr As DataRow
        Dim ds As New DataSet

        Dim dtTable As New DataTable
        dtTable.TableName = "TableBizDoc"
        dtTable.Columns.Add("numBizDoc")
        dtTable.Columns.Add("tintBizocType")

        Dim str As String()
        Dim i As Integer
        str = txtSaleHidden.Text.Split(",")
        For i = 0 To str.Length - 2
            dr = dtTable.NewRow
            dr("numBizDoc") = str(i)
            dr("tintBizocType") = 1
            dtTable.Rows.Add(dr)
        Next

        str = txtPurchaseHidden.Text.Split(",")
        For i = 0 To str.Length - 2
            dr = dtTable.NewRow
            dr("numBizDoc") = str(i)
            dr("tintBizocType") = 2
            dtTable.Rows.Add(dr)
        Next
        ds.Tables.Add(dtTable)

        Dim objConfigWizard As New FormConfigWizard
        objConfigWizard.DomainID = Session("DomainID")
        objConfigWizard.strAOI = ds.GetXml
        objConfigWizard.ManageBizDocFilter()

        objCommon.sb_FillListFromDB(lstAvailable, 27, Session("DomainID"))

        BindBizDoc(1, lstSale)
        BindBizDoc(2, lstPurchase)
    End Sub
End Class