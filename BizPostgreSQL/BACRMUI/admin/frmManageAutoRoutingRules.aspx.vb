''' -----------------------------------------------------------------------------
''' Project	 : BACRM.UserInterface.Reports
''' Class	 : frmManageAutoRoutingRules
''' 
''' -----------------------------------------------------------------------------
''' <summary>
'''     This is a interface for creation of Routing Rules parameters for Leads
''' </summary>
''' <remarks>
''' </remarks>
''' <history>
''' 	[Debasish]	08/21/2005	Created
''' </history>
''' ''' -----------------------------------------------------------------------------
Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Common
Namespace BACRM.UserInterface.Admin

    Public Class frmManageAutoRoutingRules
        Inherits BACRMPage
       

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.
        Private designerPlaceholderDeclaration As System.Object

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This event is fired eachtime the page is called. In this event we will 
        '''     get the data from the DB and populate the Tables and the form.
        ''' </summary>
        ''' <param name="sender">Represents the sender object.</param>
        ''' <param name="e">Represents the EventArgs.</param>
        ''' <remarks> 
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	08/21/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Load
            Try
                DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ""
                DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "none")
                ' = Request.Url.Segments(Request.Url.Segments.Length - 1) ' = "AutoroutinRules"
                If Not IsPostBack Then

                    GetUserRightsForPage(13, 7)
                    If m_aryRightsForPage(RIGHTSTYPE.VIEW) = 0 Then Response.Redirect("../admin/authentication.aspx?mesg=AS")
                    FillRulesInDataGrid()                                           'calls to fill the datagrid with rules
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub
        Private Sub DisplayError(ByVal ex As String)
            DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ex
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
        End Sub
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This method is used to raise the button event to teh datagrid
        ''' </summary>
        ''' <remarks>
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	09/21/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Sub FillRulesInDataGrid()
            Try
                Dim objAutoRoutingRules As New AutoRoutingRules                                     'Create an object of form config wizard
                objAutoRoutingRules.UserCntID = Session("UserContactID")                                      'Set value of User id
                objAutoRoutingRules.DomainID = Session("DomainID")                                  'Set value of domain id
                Dim dtRoutingRules As DataTable                                                     'declare a datatable
                dtRoutingRules = objAutoRoutingRules.getRoutingRulesList()                          'call to get the routing rules

                Dim dvDefaultRoutingRules As DataView                                               'Create a dataview of default routing 
                dvDefaultRoutingRules = dtRoutingRules.DefaultView                                  'set the view data
                dvDefaultRoutingRules.RowFilter = "bitDefault = 1"                                  'Default View has bitDefault as 0
                dgRoutingRulesLeadsDefault.DataSource = dvDefaultRoutingRules                       'set the datasource
                dgRoutingRulesLeadsDefault.DataBind()                                               'databind the datgrid
                If dvDefaultRoutingRules.Count > 0 Then
                    Dim objTextBoxControl As TextBox = CType(dgRoutingRulesLeadsDefault.Items(0).Cells(1).FindControl("txtDefaultRuleDesc"), TextBox) 'Get the Text box
                    objTextBoxControl.Text = dvDefaultRoutingRules.Item(0).Item("vcRoutName")           'Set the name or the Rule
                End If
                '================================================================================
                ' Purpose:  Databind the List of available employees
                '           
                ' History
                ' Ver   Date        Ref     Author              Reason Created
                ' 1     09/21/2005          Debasish Nag        Databind the available employees
                '================================================================================
                Dim dtAvailableEmployees As DataTable                                               'Declare a datatable
                objAutoRoutingRules.RoutingRuleID = -1                                              'Set value of Routing RuleId
                dtAvailableEmployees = objAutoRoutingRules.getAvailableEmployees()                  'call function to get the list of available employees

                Dim dvAvailableEmployees As DataView                                                'create a dataview object
                dvAvailableEmployees = dtAvailableEmployees.DefaultView()                           'Set the default view as the dataview's contents
                dvAvailableEmployees.Sort = "vcUserName asc"                                        'sort order of rows
                Dim objDropDownControl As DropDownList = CType(dgRoutingRulesLeadsDefault.Items(0).Cells(2).FindControl("ddlDefaultRuleEmployee"), DropDownList) 'Get the drop down list
                objDropDownControl.DataSource = dvAvailableEmployees                                'set the datasource
                objDropDownControl.DataValueField = "numUserId"                                     'set the data value of the listbox
                objDropDownControl.DataTextField = "vcUserName"                                     'set the data value of the listbox
                objDropDownControl.DataBind()                                                       'Databind the available employees drop down
                If dvDefaultRoutingRules.Count > 0 Then
                    objAutoRoutingRules.RoutingRuleID = dvDefaultRoutingRules.Item(0).Item("numRoutID") 'Set the Rule Id
                    Dim iEmpId As Integer = CInt(objAutoRoutingRules.getDefaultRoutingRuleDetails().Tables("RuleDetails").Rows(0).Item("numEmpId")) 'Get the default Rule Details and extract the employee id
                    If Not objDropDownControl.Items.FindByValue(iEmpId) Is Nothing Then                 'Check if the text exists in the dropdown
                        objDropDownControl.Items.FindByValue(iEmpId).Selected = True                    'show the appropriate employee as selected
                    End If
                End If
                Dim objButtonControl As Button = CType(dgRoutingRulesLeadsDefault.Items(0).Cells(3).FindControl("btnUpdt"), Button) 'Get the drop down list
                'objButtonControl.Attributes.Add("onclick", "javascript: setHiddenAttribute(document.frmManageAutoRoutingRules);") 'Add a click event to set the value of the hidden variable

                Dim dvNonDefaultRoutingRules As DataView
                dvNonDefaultRoutingRules = dtRoutingRules.DefaultView                               'set the view data
                dvNonDefaultRoutingRules.RowFilter = "bitDefault <> 1"                              'Defautl View has bitDefault as non zero
                dgRoutingRulesLeads.DataSource = dvNonDefaultRoutingRules                           'set the datasource
                dgRoutingRulesLeads.DataBind()
            Catch ex As Exception
                Throw ex
            End Try
            'databind the datgrid
        End Sub

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This method is used to delete a rule from the database
        ''' </summary>
        ''' <remarks>
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	09/26/2005	Created
        ''' </history>
        Protected Overridable Sub btnDeleteAction_Command(ByVal sender As Object, ByVal e As EventArgs)
            Try
                Dim btnDelete As LinkButton = CType(sender, LinkButton)                                             'typecast to button
                Dim dgContainer As DataGridItem = CType(btnDelete.NamingContainer, DataGridItem)            'get the containeer object
                Dim gridIndex As Integer = dgContainer.ItemIndex                                            'Get the row idnex of datagrid
                Dim iRoutIndex As Integer = CInt(dgRoutingRulesLeads.Items(gridIndex).Cells(0).Text)        'Get the RoutInd from the hidden column
                Dim objConfigWizard As New AutoRoutingRules                                                 'Create an object of form config wizard
                objConfigWizard.RoutingRuleID = iRoutIndex                                                  'Set the Rout Id
                objConfigWizard.deleteRule()                                                                'Call to delete rule
                FillRulesInDataGrid()                                                                       'fill teh datagrid once again
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This method is used to update the default rule in the database
        ''' </summary>
        ''' <remarks>
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	08/03/2005	Created
        ''' </history>
        Protected Overridable Sub btnUpdt_Command(ByVal sender As Object, ByVal e As EventArgs)
            Try
                Dim btnUpdt As Button = CType(sender, Button)                                               'typecast to button
                Dim dgContainer As DataGridItem = CType(btnUpdt.NamingContainer, DataGridItem)              'get the containeer object
                Dim gridIndex As Integer = dgContainer.ItemIndex                                            'Get the row idnex of datagrid
                Dim iRoutIndex As Integer = CInt(dgRoutingRulesLeadsDefault.Items(gridIndex).Cells(0).Text) 'Get the RoutInd from the hidden column
                Dim objGenericControl As Control = dgRoutingRulesLeadsDefault.Items(gridIndex).Cells(2).FindControl("ddlDefaultRuleEmployee") 'Get the RoutInd from the hidden column
                Dim iEmployeeId As Integer = CInt(CType(objGenericControl, DropDownList).SelectedValue)     'Get the Employee Id   
                objGenericControl = dgRoutingRulesLeadsDefault.Items(gridIndex).Cells(1).FindControl("txtDefaultRuleDesc") 'Get the RoutInd from the hidden column
                Dim vcRoutName As String = CStr(CType(objGenericControl, TextBox).Text)                     'Get the Rule Name

                Dim objConfigWizard As New AutoRoutingRules                                                 'Create an object of form config wizard
                objConfigWizard.RoutingRuleID = iRoutIndex                                                  'Set the Rout Id
                objConfigWizard.DomainID = Session("DomainID")                                              'Set the Domain Id
                objConfigWizard.UserCntID = Session("UserContactID")                                                  'Set the User Id
                objConfigWizard.vcRoutName = vcRoutName                                                     'Set teh Rule Name
                'objConfigWizard.EmpIdList = iEmployeeId                                                     'Set the Rout Id
                Dim dtTable As New DataTable
                dtTable.Columns.Add("numEmpId")
                dtTable.Columns.Add("intAssignOrder")
                Dim dr As DataRow
                dr = dtTable.NewRow
                dr("numEmpId") = iEmployeeId
                dr("intAssignOrder") = "1"
                dtTable.Rows.Add(dr)

                Dim ds As New DataSet
                dtTable.TableName = "Table"
                ds.Tables.Add(dtTable.Copy)
                objConfigWizard.strDistributionList = ds.GetXml
                ds.Tables.Remove(ds.Tables(0))

                objConfigWizard.tintEqualTo = 0                                                           'Set the data value, here it is of no importance
                objConfigWizard.Priority = 0                                                              'Set the AOIIds, here it is of no importance
                objConfigWizard.UpdateRoutingRule()                                                         'Call to update default rule
                FillRulesInDataGrid()
                'fill teh datagrid once again
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Sub dgRoutingRulesLeads_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgRoutingRulesLeads.ItemDataBound
            Try
                If e.Item.ItemType = ListItemType.AlternatingItem Or ListItemType.Item Then
                    e.Item.Cells(1).Attributes.Add("onclick", "return EditAutoRoutingRules(" & e.Item.Cells(0).Text & ")")
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

    End Class
End Namespace