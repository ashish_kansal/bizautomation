<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="AdminBusinessProcess.aspx.vb"
    Inherits="BACRM.UserInterface.Admin.AdminBusinessProcess" MasterPageFile="~/common/Popup.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Business Process</title>
    <script language="javascript" type="text/javascript">
        function Save() {
            if (document.getElementById('txtName').value == '') {
                alert('Enter The Process Name')
                document.getElementById('txtName').focus
                return false
            }
            else
                return true
        }
		      
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <div class="input-part">
        <div class="right-input">
            <asp:Button ID="btnSave" runat="server" Text="Save & Close" CssClass="button" />
            <asp:Button ID="btnClose" runat="server" OnClientClick="javascript:self.close()"
                Text="Close" CssClass="button" />&nbsp;&nbsp;
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageTitle" runat="server" ClientIDMode="Static">
    New Process
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="Content" runat="server" ClientIDMode="Static">
    <asp:Table ID="tblMile" runat="server" Width="600px" GridLines="None" BorderColor="black"
        BorderWidth="1" CssClass="aspTable">
        <asp:TableRow>
            <asp:TableCell HorizontalAlign="Right" CssClass="normal1">
			                       Process Name:
            </asp:TableCell>
            <asp:TableCell HorizontalAlign="left">
                <asp:TextBox ID="txtName" runat="server" Width="200" CssClass="signup" MaxLength="45"></asp:TextBox>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell HorizontalAlign="Right" CssClass="normal1">
			                        Milestone Configuration 
            </asp:TableCell>
            <asp:TableCell HorizontalAlign="left">
                <asp:DropDownList ID="ddlStag" CssClass="signup" runat="server" Width="150">
                    <asp:ListItem Value="1" Selected="True">Configuration 1 (50%,95%)</asp:ListItem>
                    <asp:ListItem Value="2">Configuration 2(25%,50%,75%)</asp:ListItem>
                    <asp:ListItem Value="3">Configuration 3 (20%,40%,60%,80%)</asp:ListItem>
                    <asp:ListItem Value="4">Configuration 4 (5%,20%,35%,50%,65%,80%,95%)</asp:ListItem>
                </asp:DropDownList>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell HorizontalAlign="Right" CssClass="normal1">
			                       Process Type:
            </asp:TableCell>
            <asp:TableCell HorizontalAlign="left">
                <asp:DropDownList ID="ddlProcess" CssClass="signup" runat="server" Width="150">
                    <asp:ListItem Value="0">Sales Process</asp:ListItem>
                    <asp:ListItem Value="2">Purchase Process</asp:ListItem>
                    <asp:ListItem Value="1">Projects</asp:ListItem>
                </asp:DropDownList>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
</asp:Content>
