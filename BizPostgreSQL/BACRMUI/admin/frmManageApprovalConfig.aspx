<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" CodeBehind="frmManageApprovalConfig.aspx.vb"
    Inherits="BACRM.UserInterface.Admin.frmManageApprovalConfig" MasterPageFile="~/common/GridMasterRegular.Master" %>

<%@ Register TagPrefix="ie" Namespace="Microsoft.Web.UI.WebControls" Assembly="Microsoft.Web.UI.WebControls" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Manage Approval Configuration</title>
    <style type="text/css">
         .hs {
            background-color: #ebebeb;
            font-weight:bold;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server"
    ClientIDMode="Static">
    <div class="row padbottom10">
        <div class="col-xs-12">
            <div class="pull-left">
                <div class="form-inline">
                    <div class="form-group">
                        <label>Select Module</label>
                        <asp:DropDownList ID="ddlModule" CssClass="form-control" runat="server" AutoPostBack="True">
                            <asp:ListItem Value="0">-Select Module-</asp:ListItem>
                            <asp:ListItem Value="1">Time and Expense</asp:ListItem>
                            <asp:ListItem Value="2">Purchase/Sales Order Approval</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>
            </div>
            <div class="pull-right">
                <asp:LinkButton ID="btnSave" runat="server" OnClick="btnSave_Click" CssClass="btn btn-primary"><i class="fa fa-save"></i>&nbsp;&nbsp;Save Approval Configuration</asp:LinkButton>
            </div>
        </div>
    </div>
    <asp:UpdateProgress ID="UpdateProgress" runat="server" ClientIDMode="Static">
        <ProgressTemplate>
            <div class="overlay">
                <div class="overlayContent" style="color: #000; text-align: center; width: 250px; padding: 20px">
                    <i class="fa fa-2x fa-refresh fa-spin"></i>
                    <h3>Processing Request</h3>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server" ClientIDMode="Static">
    Approval Configuration&nbsp;<a href="#" onclick="return OpenHelpPopUp('admin/frmManageApprovalConfig.aspx')"><label class="badge bg-yellow">?</label></a>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BizPager" runat="server" ClientIDMode="Static">
    &nbsp;
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="GridPlaceHolder" runat="server"
    ClientIDMode="Static">
    <asp:GridView ID="grdApprovalConfig" ShowHeaderWhenEmpty="true" OnRowDataBound="grdApprovalConfig_RowDataBound" CssClass="table table-responsive table-bordered" AutoGenerateColumns="false" runat="server">
        <Columns>
            <asp:TemplateField HeaderText="User Name">
                <ItemTemplate>
                    <asp:Label ID="lblUser" Text='<%#Eval("vcUserName")%>' runat="server"></asp:Label>
                    <asp:HiddenField ID="hdnUserId" Value='<%#Eval("numUserId")%>' runat="server" />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="1st Level Approver"> 
                <ItemTemplate>
                    <asp:DropDownList ID="ddl1stLevelApprover" CssClass="form-control" runat="server"></asp:DropDownList>
                </ItemTemplate>
            </asp:TemplateField>
             <asp:TemplateField HeaderText="2nd Level Approver">
                <ItemTemplate>
                    <asp:DropDownList ID="ddl2ndLevelApprover" CssClass="form-control" runat="server"></asp:DropDownList>
                </ItemTemplate>
            </asp:TemplateField>
             <asp:TemplateField HeaderText="3rd Level Approver"> 
                <ItemTemplate>
                    <asp:DropDownList ID="ddl3rdLevelApprover" CssClass="form-control" runat="server"></asp:DropDownList>
                </ItemTemplate>
            </asp:TemplateField>
             <asp:TemplateField HeaderText="4th Level Approver"> 
                <ItemTemplate>
                    <asp:DropDownList ID="ddl4thLevelApprover" CssClass="form-control" runat="server"></asp:DropDownList>
                </ItemTemplate>
            </asp:TemplateField>
             <asp:TemplateField HeaderText="5th Level Approver"> 
                <ItemTemplate>
                    <asp:DropDownList ID="ddl5thLevelApprover" CssClass="form-control" runat="server"></asp:DropDownList>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
</asp:Content>
