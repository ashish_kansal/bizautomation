﻿Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.ShioppingCart
Imports System.Xml
Imports System.IO



Public Class frmCustomizeErrorMessage
    Inherits BACRMPage
    Dim objErrorMsg As ErrorMessage
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ""
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "none")
            If Not IsPostBack Then
                BindSites()
                If Not Session("SiteID") Is Nothing Then
                    ddlSites.SelectedValue = Session("SiteID")
                End If
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try

    End Sub
    Private Sub DisplayError(ByVal ex As String)
        DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ex
        DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
    End Sub
    Private Sub ddlApplication_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlApplication.SelectedIndexChanged
        Try
            If objErrorMsg Is Nothing Then objErrorMsg = New ErrorMessage
            objErrorMsg.DomainID = CCommon.ToString(Session("DomainID"))
            objErrorMsg.SiteID = CCommon.ToString(Session("SiteId"))
            objErrorMsg.CultureID = 0

            If CCommon.ToInteger(ddlApplication.SelectedValue) > 0 Then
                objErrorMsg.Application = ddlApplication.SelectedValue
                bindGridview()
            End If
            'Else Part Remaining ...

        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub
    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            If Session("SiteID") > 0 Then
                If objErrorMsg Is Nothing Then objErrorMsg = New ErrorMessage
                objErrorMsg.DomainID = Session("DomainId")
                objErrorMsg.SiteID = Session("SiteID")
                objErrorMsg.CultureID = 0
                objErrorMsg.Application = ddlApplication.SelectedValue
                objErrorMsg.strErrorMessages = GetErrorMessages()
                Dim a As Boolean = objErrorMsg.ManageErrorDetails()
                bindGridview()
                Dim strXmlfile As String = CCommon.GetDocumentPhysicalPath(CCommon.ToInteger(Session("DomainId"))) & "ErrorMsg" & "_" & CCommon.ToString(Session("DomainId")) & "_" & CCommon.ToString(Session("SiteId")) & "_" & ddlApplication.SelectedValue & "_" & 0 & ".xml"
                If File.Exists(strXmlfile) Then
                    File.Delete(strXmlfile)
                End If
            End If
            btnSave.Attributes.Add("onclick", "Save();")
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Function GetErrorMessages() As String
        Try
            Dim ds As New DataSet
            Dim dt As New DataTable
            dt.Columns.Add("numErrorDetailId")
            dt.Columns.Add("vcErrorDesc")
            dt.Columns.Add("numErrorId")

            Dim i As Integer = 0
            For Each row As TableRow In gvErrorDescription.Rows
                If (CCommon.ToInteger(CType(row.FindControl("hdnumErrorDetailId"), HiddenField).Value) = 0 And CType(row.FindControl("txtSetDescription"), TextBox).Text.Length > 0) Or (CCommon.ToInteger(CType(row.FindControl("hdnumErrorDetailId"), HiddenField).Value) > 0) Then
                    Dim dr As DataRow = dt.NewRow
                    dr("numErrorDetailId") = CType(row.FindControl("hdnumErrorDetailId"), HiddenField).Value
                    dr("vcErrorDesc") = CType(row.FindControl("txtSetDescription"), TextBox).Text
                    dr("numErrorId") = gvErrorDescription.DataKeys(i)("numErrorId")
                    dt.Rows.Add(dr)
                End If
                i = i + 1
            Next
            ds.Tables.Add(dt.Copy)
            ds.Tables(0).TableName = "ErrorMessages"
            Return ds.GetXml()
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Sub BindSites()
        Try
            Dim objSite As New Sites
            objSite.DomainID = Session("DomainID")
            ddlSites.DataSource = objSite.GetSites()
            ddlSites.DataTextField = "vcSiteName"
            ddlSites.DataValueField = "numSiteID"
            ddlSites.DataBind()
            ddlSites.Items.Insert(0, "--Select One--")
            ddlSites.Items.FindByText("--Select One--").Value = 0
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub ddlSites_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlSites.SelectedIndexChanged
        Try
            Session("SiteID") = ddlSites.SelectedValue
            Dim objSite As New Sites
            Dim dtSite As DataTable
            objSite.SiteID = ddlSites.SelectedValue
            objSite.DomainID = Session("DomainID")
            dtSite = objSite.GetSites()
            Session("SitePreviewURL") = dtSite.Rows(0)("vcPreviewURL")
            ' Response.Redirect(Request.Url.OriginalString)


            If objErrorMsg Is Nothing Then objErrorMsg = New ErrorMessage
            objErrorMsg.DomainID = CCommon.ToString(Session("DomainID"))
            objErrorMsg.SiteID = CCommon.ToString(Session("SiteId"))
            objErrorMsg.CultureID = 0

            If CCommon.ToInteger(ddlApplication.SelectedValue) > 0 Then
                objErrorMsg.Application = ddlApplication.SelectedValue
                bindGridview()
            End If
            'Else Part Remaining ...


        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Public Sub bindGridview()
        Try
            gvErrorDescription.DataSource = objErrorMsg.GetAllErrorMessages()
            gvErrorDescription.DataBind()
        Catch ex As Exception
            Throw ex
        End Try
        
    End Sub

End Class