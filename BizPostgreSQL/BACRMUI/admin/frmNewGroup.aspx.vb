Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Common
Namespace BACRM.UserInterface.Admin
    Public Class frmNewGroup
        Inherits BACRMPage

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub
        Protected WithEvents btnSave As System.Web.UI.WebControls.Button
        Protected WithEvents btnClose As System.Web.UI.WebControls.Button
        Protected WithEvents rfv As System.Web.UI.WebControls.RequiredFieldValidator
        Protected WithEvents dgGroups As System.Web.UI.WebControls.DataGrid
        Dim myRow As DataRow
        Protected WithEvents litMessage As System.Web.UI.WebControls.Literal
        Protected WithEvents txtGroup As System.Web.UI.WebControls.TextBox
        Protected WithEvents ddlGroupType As System.Web.UI.WebControls.DropDownList
        Protected WithEvents btnAdd As System.Web.UI.WebControls.Button
        Protected WithEvents Table2 As System.Web.UI.WebControls.Table
        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.
        Private designerPlaceholderDeclaration As System.Object

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Try
               

                GetUserRightsForPage(13, 25)
                If m_aryRightsForPage(RIGHTSTYPE.VIEW) = 0 Then
                    Response.Redirect("../admin/authentication.aspx?mesg=AS")
                ElseIf m_aryRightsForPage(RIGHTSTYPE.UPDATE) = 0 Then
                    'btnSave.Visible = False
                End If

                If Not IsPostBack Then BindGrid()
                
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub dgGroups_EditCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgGroups.EditCommand
            Try
                dgGroups.EditItemIndex = e.Item.ItemIndex
                Call BindGrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub dgGroups_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgGroups.ItemCommand
            Try
                If e.CommandName = "Cancel" Then
                    dgGroups.EditItemIndex = e.Item.ItemIndex
                    dgGroups.EditItemIndex = -1
                    Call BindGrid()
                End If

                If e.CommandName = "Delete" Then
                    Dim objUserGroups As New UserGroups
                    objUserGroups.GroupId = CType(e.Item.FindControl("lblGroupID"), Label).Text
                    objUserGroups.DomainId = Session("DomainID")
                    If Not objUserGroups.DeleteAuthorizationGroups() = True Then
                        litMessage.Text = "Depenedent Record exists.Cannot be deleted."
                    End If
                    Call BindGrid()
                End If
                If e.CommandName = "Update" Then
                    Dim objUserGroups As New UserGroups
                    objUserGroups.GroupId = CType(e.Item.FindControl("lblGroupID"), Label).Text
                    objUserGroups.GroupName = CType(e.Item.FindControl("txtEGroup"), TextBox).Text
                    objUserGroups.GroupType = CType(e.Item.FindControl("ddlEGroupType"), DropDownList).SelectedItem.Value
                    objUserGroups.DomainId = Session("DomainID")
                    objUserGroups.ManageAuthorizationGroups()
                    dgGroups.EditItemIndex = -1
                    Call BindGrid()
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Sub BindGrid()
            Try
                Dim objUserGroups As New UserGroups
                objUserGroups.DomainId = Session("DomainID")
                dgGroups.DataSource = objUserGroups.GetAutorizationGroup
                dgGroups.DataBind()
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub dgGroups_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgGroups.ItemDataBound
            Try
                If e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.EditItem Then
                    Dim drv As DataRowView = DirectCast(e.Item.DataItem, DataRowView)
                    Dim lnkDelete As LinkButton = DirectCast(e.Item.FindControl("lnkDelete"), LinkButton)

                    If CCommon.ToBool(drv("bitConsFlag")) Then
                        lnkDelete.Visible = False
                    Else
                        lnkDelete.Visible = True
                    End If
                End If
                If e.Item.ItemType = ListItemType.EditItem Then
                    Dim ddl As DropDownList
                    ddl = e.Item.FindControl("ddlEGroupType")
                    ddl.Items.FindByValue(CType(e.Item.FindControl("lblEGroupType"), Label).Text).Selected = True
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
            Try
                Dim objUserGroups As New UserGroups
                objUserGroups.GroupId = 0
                objUserGroups.GroupName = txtGroup.Text
                objUserGroups.GroupType = ddlGroupType.SelectedItem.Value
                objUserGroups.DomainId = Session("DomainID")
                objUserGroups.ManageAuthorizationGroups()
                Call BindGrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
            Try
                Response.Write("<script>opener.location.reload(true);window.close();</script>")
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

    End Class
End Namespace