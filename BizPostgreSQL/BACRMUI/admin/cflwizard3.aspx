<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" CodeBehind="cflwizard3.aspx.vb" Inherits="BACRM.UserInterface.Admin.cflwizard3" MasterPageFile="~/common/Popup.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Custom Field Wizard</title>
    <script language="javascript">
        function Close() {
            window.close()
            return false;
        }
        function Next() {
            if (document.getElementById('CFWFldtype').value == 0) {
                alert("Select Type")
                document.getElementById('CFWFldtype').focus()
                return false;
            }
        }
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageTitle" runat="server" ClientIDMode="Static">
    Step 3
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="Content" runat="server" ClientIDMode="Static">
    <table width="400px" height="100%" class="aspTableDTL">
        <tr>
            <td class="normal2" align="center" colspan="2">
                <br />
                &nbsp;&nbsp;Select which type of custom field you would like to create.<br />
                <br />
                <br />
                <br />
                <br />
            </td>
        </tr>
        <tr>
            <td valign="top" class="normal1" align="right">Choose Field Type</td>
            <td valign="top">&nbsp;<asp:DropDownList ID="CFWFldtype" runat="server" CssClass="signup">
                <asp:ListItem Value="0">-- Select One --</asp:ListItem>
                <asp:ListItem Value="TextBox">TextBox</asp:ListItem>
                <asp:ListItem Value="SelectBox">SelectBox</asp:ListItem>
                <asp:ListItem Value="CheckBox">CheckBox</asp:ListItem>
                <asp:ListItem Value="TextArea">TextArea</asp:ListItem>
                <asp:ListItem Value="DateField">DateField</asp:ListItem>
                <asp:ListItem Value="Link">Link</asp:ListItem>
                <asp:ListItem Value="CheckBoxList">CheckBoxList</asp:ListItem>
            </asp:DropDownList><br />
                <br />
                <br />
                <br />
            </td>
        </tr>
        <tr>
            <td align="center" colspan="2">
                <asp:Button ID="btnBack" Width="45" runat="server" CssClass="button" Text="Back"></asp:Button>
                <asp:Button ID="btnNext" Width="45" runat="server" CssClass="button" Text="Next"></asp:Button>
                <asp:Button ID="btnCancel" Width="50" runat="server" CssClass="button" Text="Close"></asp:Button>
                <br />
                <br />
                <br />

                <br />
                <br />
                <br />
            </td>
        </tr>
    </table>
</asp:Content>
