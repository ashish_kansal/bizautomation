﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmPOFulfillmentSettings.aspx.vb" Inherits=".frmPOFulfillmentSettings"
    MasterPageFile="~/common/Popup.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Purchase Order Fulfillment Settings</title>
    <script language="javascript" type="text/javascript">
        $(document).ready(function () {
            $("select[id$=ddlPartialQty]").change(function () {
                if ($("select option:selected").index() > 0) {
                    $('#chkPartialQty').prop('checked', true);
                }
                else {
                    $('#chkPartialQty').prop('checked', false);
                }
            })

            $("select[id$=ddlAllQty]").change(function () {
                if ($("select option:selected").index() > 0) {
                    $('#chkAllQty').prop('checked', true);
                }
                else {
                    $('#chkAllQty').prop('checked', false);
                }
            })

            /*$("select[id$=ddlBizDoc]").change(function () {
                if ($("select option:selected").index() > 0) {
                    $('#chkAddBizDoc').prop('checked', true);
                }
                else {
                    $('#chkAddBizDoc').prop('checked', false);
                }
            })*/
        });

        function UncheckPartial() {
            var isCheckPartialQty = $('input[name=chkPartialQty]').is(':checked')
            if (isCheckPartialQty == false) {
                $("#ddlPartialQty").prop('selectedIndex', 0);
            }
        }

        function UncheckAll() {
            var isCheckAllQty = $('input[name=chkAllQty]').is(':checked')
            if (isCheckAllQty == false) {
                $("#ddlAllQty").prop('selectedIndex', 0);
            }
            /*UncheckBizDoc();
            $('#chkAddBizDoc').prop('checked', false);*/
            $('#chkClosePurchaseOrder').prop('checked', false);
        }

        /*function UncheckBizDoc() {
            var isCheckAddBizDoc = $('input[name=chkAddBizDoc]').is(':checked')
            if (isCheckAddBizDoc == false) {
                $("#ddlBizDoc").prop('selectedIndex', 0);
            }
        }*/
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <div class="input-part">
        <div class="right-input">
            <asp:Button runat="server" ID="btnSaveClose" Text="Save & Close" CssClass="button" Width="50" />
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageTitle" runat="server" ClientIDMode="Static">
    <asp:Label ID="lblTitle" runat="server" Text="Purchase Order Fulfillment Settings"></asp:Label>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="Content" runat="server" ClientIDMode="Static">
    <table width="100%">
        <tr>
            <td style="padding-left: 20px;">
                <%--<asp:CheckBox ID="chkAddBizDoc" onclick="UncheckBizDoc()" Text="AND add the following BizDoc to the Purchase Order:" runat="server" />--%>
            Add the following Bill BizDoc template to the Purchase Order when all quantities are received:
            </td>
            <td>
                <%--<asp:DropDownList ID="ddlBizDoc" runat="server" CssClass="signup"></asp:DropDownList>--%>
                <asp:DropDownList ID="ddlBillTemplate" runat="server" CssClass="signup"></asp:DropDownList>
            </td>
        </tr>
        <tr style="display:none">
            <td colspan="2"><i>(This will be the Bill generated whether items are received against a vendor invoice, or directly against the purchase order)</i>
            </td>
        </tr>
        <tr><td><br/><br/></td></tr>
        <tr>
            <td>
                <asp:CheckBox ID="chkPartialQty" onclick="UncheckPartial()" Text="When a partial quantity is received against a Purchase Order, set its Order Status to:" runat="server" /></td>
            <td>
                <asp:DropDownList ID="ddlPartialQty" runat="server" CssClass="signup"></asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td>
                <asp:CheckBox ID="chkAllQty" onclick="UncheckAll()" Text="When all quantities for all line items within a Purchase Order are received, set its Order Status to:" runat="server" /></td>
            <td>
                <asp:DropDownList ID="ddlAllQty" runat="server" CssClass="signup"></asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td style="padding-left: 20px;">
                <asp:CheckBox ID="chkClosePurchaseOrder" Text="AND close the Purchase Order" runat="server" />
            </td>
        </tr>
    </table>
</asp:Content>
