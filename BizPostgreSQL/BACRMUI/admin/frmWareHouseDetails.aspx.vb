
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Item
Namespace BACRM.UserInterface.Admin
    Partial Public Class frmWareHouseDetails
        Inherits BACRMPage
        Dim objItem As CItems
        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            If Not IsPostBack Then
                Dim objCommon As New CCommon
                objCommon.sb_FillComboFromDBwithSel(ddlCountry, 40, Session("DomainID"))
                objItem = New CItems
                objItem.DomainID = Session("DomainID")
                ddlWareHouse.DataSource = objItem.GetWareHouses
                ddlWareHouse.DataTextField = "vcWareHouse"
                ddlWareHouse.DataValueField = "numWareHouseID"
                ddlWareHouse.DataBind()
                ddlWareHouse.Items.Insert(0, "All")
                ddlWareHouse.Items.FindByText("All").Value = 0
                BindDataGrid()
            End If
        End Sub
        Sub BindDataGrid()
            Try

                If objItem Is Nothing Then
                    objItem = New CItems
                End If
                objItem.DomainID = Session("DomainId")
                dgwarehouse.DataSource = objItem.GetWareHousesDetails()
                dgwarehouse.DataBind()
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub ddlCountry_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCountry.SelectedIndexChanged
            FillState(ddlState, ddlCountry.SelectedItem.Value, Session("DomainID"))
            ltrMessage.Visible = False
        End Sub

        Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
            If objItem Is Nothing Then
                objItem = New CItems
            End If
            objItem.WareHouseDetailID = 0
            objItem.Country = ddlCountry.SelectedValue
            objItem.State = ddlState.SelectedValue
            objItem.WarehouseID = ddlWareHouse.SelectedValue
            objItem.mode = False
            objItem.DomainID = Session("DomainId")
            If objItem.AddWarehouseDetails = 0 Then
                ltrMessage.Visible = True
                ltrMessage.Text = "WareHouse Already Set for the State Selected"
            Else
                ltrMessage.Visible = False
                ddlCountry.SelectedIndex = 0
                ddlState.Items.Clear()
                ddlWareHouse.SelectedIndex = 0
                BindDataGrid()
            End If

        End Sub

        Private Sub dgwarehouse_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgwarehouse.ItemCommand
            If e.CommandName = "Delete" Then
                If objItem Is Nothing Then
                    objItem = New CItems
                End If
                objItem.WareHouseDetailID = e.Item.Cells(0).Text
                objItem.DomainID = Session("DomainId")
                objItem.Country = 0
                objItem.WarehouseID = 0
                objItem.State = 0
                objItem.mode = True
                objItem.AddWarehouseDetails()
                BindDataGrid()
            End If
        End Sub
    End Class
End Namespace