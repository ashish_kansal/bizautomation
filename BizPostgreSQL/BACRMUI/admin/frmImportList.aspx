﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmImportList.aspx.vb"
    Inherits=".frmImportList" MasterPageFile="~/common/GridMasterRegular.Master" %>
<%@ Register  Assembly="AspNetPager, Version=7.4.4.0, Culture=neutral, PublicKeyToken=fb0a0fe055d40fd4" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>
<%@ Import Namespace="System.ComponentModel" %>
<%@ Import Namespace="BACRM.BusinessLogic.Common" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Import List</title>
    <script language="javascript" type="text/javascript">

        //        function fn_doPage(pgno) {
        //            $('#hdnPageIndex').val(pgno);
        //            if ($('#hdnPageIndex').val() != 0 || $('#hdnPageIndex').val() > 0) {
        //                //alert($('#hdnPageIndex').val());
        //                //document.forms[0].submit();
        //                //location.reload();
        //                //alert($('#btnGo1'));
        //                //$('#btnGo1').click();
        //                $('#btnGo1').trigger('click');
        //            }
        //        }

        function Redirect(a, b, c, d) {
            if ((c == 0 && d == 0) || (c == 1 && d == 1)) {
                window.location.href = '../admin/frmImport.aspx?ifd=' + a + '&iy=' + b + '';
            }
        }

        function OpenLog(link) {
            //window.open("http://static.bizautomation.com/" + DomainID + "/Import_Log_" + ImportFileID + ".txt", '', 'toolbar=no,titlebar=no,top=0,left=100,width=1000,height=500,scrollbars=yes,resizable=yes')
            //window.open("http://localhost/BACRMPortal/Documents/Docs/" + DomainID + "/Import_Log_" + ImportFileID + ".txt", '', 'toolbar=no,titlebar=no,top=0,left=100,width=1000,height=500,scrollbars=yes,resizable=yes')
            window.open(link, '', 'toolbar=no,titlebar=no,top=0,left=100,width=1000,height=500,scrollbars=yes,resizable=yes')
            return false;
        }

        function fnSortByCharCode(varSortChar) {
            $('#txtSortCharDeals').value = varSortChar;
            if ($('#txtCurrrentPage') != null) {
                $('#txtCurrrentPage').value = 1;
            }
            document.Form1.btnGo1.click();
        }
        function SortColumnByCode(a) {
            //            document.Form1.txtSortColumn.value = a;
            //            document.Form1.submit();
            //            return false;
            $('#txtSortColumn').value = a;
            if ($('#txtSortColumn').value == a) {
                if ($('#txtSortOrder').value == 'A')
                    $('#txtSortOrder').value = 'D';
                else
                    $('#txtSortOrder').value = 'A';
            }
            location.reload();
            return false;
        }

        function DeleteRecord() {
            if (confirm('Are you sure, you want to delete the selected record?')) {
                return true;
            }
            else {
                return false;
            }
        }

        function OpenSynonymsList() {
            var h = screen.height;
            var w = screen.width;

            window.open('../admin/frmImportFieldsSynonyms.aspx', '', 'toolbar=no,titlebar=no,top=50,left=50,width=' + (w - 100) + ',height=' + (h - 200) + ',scrollbars=yes,resizable=yes');
            return false;
        }

    </script>
    <style>
        .tblDataGrid tr:first-child td {
            background:#e5e5e5;
        }
    </style>
     <style>
        .overlay {
            position: fixed;
            z-index: 98;
            top: 0px;
            left: 0px;
            right: 0px;
            bottom: 0px;
            background-color: rgba(170, 170, 170, 0.5);
            filter: alpha(opacity=80);
        }

        .overlayContent {
            z-index: 99;
            margin: 250px auto;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server"
    ClientIDMode="Static">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <div class="row padbottom10">
        <div class="col-xs-12">
            <div class="pull-left">
                <asp:Button ID="btnGo1" runat="server" Text="go1" Style="width: 1px; visibility: hidden" />
                <a href="#" onclick="window.open('http://help.bizautomation.com/?pageurl=frmImportFiles.aspx','Help');" title="help">Trouble importing Items [?]</a>
            </div>
            <div class="pull-right">
                <%If m_aryRightsForPage(RIGHTSTYPE.EXPORT) = 0 Then%>

                <%Else%>
                <a href="javascript:OpenSynonymsList();" class="btn btn-primary">Synonyms List</a>
                <a href="../admin/frmImport.aspx?ifd=0&iy=0" class="btn btn-primary"><i class="fa fa-database"></i>&nbsp;&nbsp;Data Import Wizard</a>
        
                <%End If%>
            </div>
        </div>
    </div>
    <asp:UpdateProgress ID="UpdateProgress" runat="server" ClientIDMode="Static">
        <ProgressTemplate>
            <div class="overlay">
                <div class="overlayContent" style="color: #000; background-color: #FFF; text-align: center; width: 200px; padding: 20px">
                    <img src="../images/PlUploadImages/throbber.gif" alt="" /><br />
                    <strong>Processing Request</strong>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server" ClientIDMode="Static">
    Import/Update File List
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BizPager" runat="server" ClientIDMode="Static">
     <webdiyer:AspNetPager ID="bizPager" runat="server"
        PagingButtonSpacing="0"
        CssClass="bizgridpager"
        AlwaysShow="true"
        CurrentPageButtonClass="active"
        PagingButtonUlLayoutClass="pagination"
        PagingButtonLayoutType="UnorderedList"
        FirstPageText="<<"
        LastPageText=">>"
        NextPageText=">"
        PrevPageText="<"
        Width="100%"
        UrlPaging="false"
         ShowPageIndexBox="Never"
        NumericButtonCount="8"
        ShowCustomInfoSection="Left"
        OnPageChanged="bizPager_PageChanged"
        CustomInfoHTML="Showing records %StartRecordIndex% to %EndRecordIndex% of %RecordCount% "
        CustomInfoClass="bizpagercustominfo">
    </webdiyer:AspNetPager>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="GridPlaceHolder" runat="server"
    ClientIDMode="Static">
    <div class="table-responsive">
    <asp:GridView ID="gvImports" runat="server" EnableViewState="true" AutoGenerateColumns="false"
        CssClass="table table-responsive table-bordered tblDataGrid" AllowPaging="true" AllowSorting="true" Width="100%" DataKeyNames="ImportFileID" CellPadding="2" CellSpacing="2">
        <Columns>
            <asp:TemplateField>
                <ItemTemplate>
                    <%# Container.DataItemIndex +1 %>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField>
                <HeaderTemplate>
                    <a href="#" onclick="SortColumn('ImportFileName')">Import File Name</a>
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:ImageButton runat="server" ID="imgDownload" CausesValidation="false" AlternateText="Download csv" ImageUrl="~/images/download.png" style="height:15px; vertical-align:middle"/>&nbsp;
                    <%If m_aryRightsForPage(RIGHTSTYPE.EXPORT) = 0 Then%>
                    <asp:Label ID="lblImportFileName2" Text='<%#Eval("ImportFileName") %>' runat="server"></asp:Label>
                    <%Else%>
                    <a onclick='Redirect(<%#Eval("ImportFileID") %>,<%#Eval("MasterID") %>,<%#Eval("intStatus") %>,<%#Eval("IsForRollback") %>)'>
                        <asp:Label ID="lblImportFileName1" Text='<%#Eval("ImportFileName") %>' runat="server"></asp:Label>
                    </a>
                    <%End If%>                    
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField HeaderText="Type" DataField="ImportType" SortExpression="ImportType"
                ShowHeader="true" ItemStyle-HorizontalAlign="Center" />
            <asp:BoundField HeaderText="Created By" DataField="CreatedBy" SortExpression="CreatedBy"
                ShowHeader="true" ItemStyle-HorizontalAlign="Center" />
            <asp:BoundField HeaderText="Created Date" DataField="CreatedDate" SortExpression="CreatedDate"
                ShowHeader="true" ItemStyle-HorizontalAlign="Center" />
            <asp:BoundField HeaderText="Status" DataField="Status" SortExpression="Status" ItemStyle-HorizontalAlign="Center"
                ShowHeader="true" />
            <asp:TemplateField HeaderText="Records" SortExpression="Records" ItemStyle-HorizontalAlign="Center">
                <ItemTemplate>
                    <asp:LinkButton ID="lbRecords" runat="server" CommandArgument='<%#Eval("MasterID")%>'
                        Text='<%#eval("Records") %>' CommandName='OpenRecords' />
                    <asp:HiddenField ID="hdnItemCodes" runat="server" Value='<%#Eval("ImportFileID")%>' />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField ItemStyle-HorizontalAlign="Center">
                <HeaderTemplate>
                    Action
                </HeaderTemplate>
                <ItemTemplate>
                    <%If m_aryRightsForPage(RIGHTSTYPE.DELETE) = 0 Then%>
                    <%Else%>
                    <asp:LinkButton ID="btnDelete" runat="server" CommandArgument='<%#Eval("ImportFileID")%>' OnClientClick="return DeleteRecord()" CssClass="btn btn-xs btn-danger" CommandName="X" Visible="false"><i class="fa fa-trash-o"></i></asp:LinkButton>
                    <asp:LinkButton ID="btnRollback" runat="server" CommandArgument='<%#Eval("ImportFileID")%>' CssClass="btn btn-sm btn-primary" CommandName="Rollback" Visible="false">Rollback</asp:LinkButton>
                    <%End If%>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Logs" ItemStyle-HorizontalAlign="Center">
                <ItemTemplate>
                    <asp:HyperLink ID="lnkLog" runat="server" ImageUrl="~/images/Audit.png" Target="_blank" Text="View Log"/>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
        <PagerTemplate>
        </PagerTemplate>
    </asp:GridView>
        </div>
    <asp:TextBox ID="txtCurrrentPage" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtSortColumn" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtSortOrder" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtSortChar" runat="server" Style="display: none"></asp:TextBox>
</asp:Content>
