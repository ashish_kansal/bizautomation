﻿Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Admin
Public Class frmSavedSearchList
    Inherits BACRMPage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            'CLEAR ERROR ON RELOAD
            DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ""
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "none")

            If Not IsPostBack Then
                LoadDetails()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Sub LoadDetails()
        Try
            Dim objSearch As New FormGenericAdvSearch
            objSearch.byteMode = 1
            objSearch.SearchID = 0
            objSearch.FormID = 0
            objSearch.DomainID = Session("DomainID")
            objSearch.UserCntID = Session("UserContactID")
            Dim dt As DataTable = objSearch.ManageSavedSearch()
            dgSavedSearch.DataSource = dt
            dgSavedSearch.DataBind()
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub dgSavedSearch_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgSavedSearch.ItemCommand
        Try
            If e.CommandName = "Delete" Then

                Dim objSearch As New FormGenericAdvSearch
                objSearch.byteMode = 3
                objSearch.SearchID = e.Item.Cells(0).Text
                objSearch.DomainID = Session("DomainID")
                objSearch.UserCntID = Session("UserContactID")
                objSearch.ManageSavedSearch()

                LoadDetails()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Private Sub DisplayError(ByVal exception As String)
        Try
            DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = exception
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Focus()
        Catch ex As Exception

        End Try
    End Sub
End Class