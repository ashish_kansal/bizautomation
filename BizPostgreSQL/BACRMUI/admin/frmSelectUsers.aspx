﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmSelectUsers.aspx.vb"
    Inherits="BACRM.UserInterface.Admin.frmSelectUsers" MasterPageFile="~/common/Popup.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Select Users</title>
    <script language="javascript" type="text/javascript">
        function MoveUp(tbox) {

            for (var i = 1; i < tbox.options.length; i++) {
                if (tbox.options[i].selected && tbox.options[i].value != "") {

                    var SelectedText, SelectedValue;
                    SelectedValue = tbox.options[i].value;
                    SelectedText = tbox.options[i].text;
                    tbox.options[i].value = tbox.options[i - 1].value;
                    tbox.options[i].text = tbox.options[i - 1].text;
                    tbox.options[i - 1].value = SelectedValue;
                    tbox.options[i - 1].text = SelectedText;
                }
            }
            return false;
        }
        function MoveDown(tbox) {

            for (var i = 0; i < tbox.options.length - 1; i++) {
                if (tbox.options[i].selected && tbox.options[i].value != "") {

                    var SelectedText, SelectedValue;
                    SelectedValue = tbox.options[i].value;
                    SelectedText = tbox.options[i].text;
                    tbox.options[i].value = tbox.options[i + 1].value;
                    tbox.options[i].text = tbox.options[i + 1].text;
                    tbox.options[i + 1].value = SelectedValue;
                    tbox.options[i + 1].text = SelectedText;
                }
            }
            return false;
        }

        sortitems = 0;  // 0-False , 1-True
        function move(fbox, tbox) {

            for (var i = 0; i < fbox.options.length; i++) {
                if (fbox.options[i].selected && fbox.options[i].value != "") {
                    /// to check for duplicates 
                    for (var j = 0; j < tbox.options.length; j++) {
                        if (tbox.options[j].value == fbox.options[i].value) {
                            alert("Item is already selected");
                            return false;
                        }
                    }

                    var no = new Option();
                    no.value = fbox.options[i].value;
                    no.text = fbox.options[i].text;
                    tbox.options[tbox.options.length] = no;
                    fbox.options[i].value = "";
                    fbox.options[i].text = "";

                }
            }
            BumpUp(fbox);
            if (sortitems) SortD(tbox);
            return false;
        }

        function remove1(fbox, tbox) {

            for (var i = 0; i < fbox.options.length; i++) {
                if (fbox.options[i].selected && fbox.options[i].value != "") {
                    /// to check for duplicates 
                    for (var j = 0; j < tbox.options.length; j++) {
                        if (tbox.options[j].value == fbox.options[i].value) {
                            fbox.options[i].value = "";
                            fbox.options[i].text = "";
                            BumpUp(fbox);
                            if (sortitems) SortD(tbox);
                            return false;


                            //alert("Item is already selected");
                            //return false;
                        }
                    }

                    var no = new Option();
                    no.value = fbox.options[i].value;
                    no.text = fbox.options[i].text;
                    tbox.options[tbox.options.length] = no;
                    fbox.options[i].value = "";
                    fbox.options[i].text = "";

                }
            }
            BumpUp(fbox);
            if (sortitems) SortD(tbox);
            return false;
        }

        function BumpUp(box) {
            for (var i = 0; i < box.options.length; i++) {
                if (box.options[i].value == "") {
                    for (var j = i; j < box.options.length - 1; j++) {
                        box.options[j].value = box.options[j + 1].value;
                        box.options[j].text = box.options[j + 1].text;
                    }
                    var ln = i;
                    break;
                }
            }
            if (ln < box.options.length) {
                box.options.length -= 1;
                BumpUp(box);
            }
        }


        function SortD(box) {
            var temp_opts = new Array();
            var temp = new Object();
            for (var i = 0; i < box.options.length; i++) {
                temp_opts[i] = box.options[i];
            }
            for (var x = 0; x < temp_opts.length - 1; x++) {
                for (var y = (x + 1); y < temp_opts.length; y++) {
                    if (temp_opts[x].text > temp_opts[y].text) {
                        temp = temp_opts[x].text;
                        temp_opts[x].text = temp_opts[y].text;
                        temp_opts[y].text = temp;
                        temp = temp_opts[x].value;
                        temp_opts[x].value = temp_opts[y].value;
                        temp_opts[y].value = temp;
                    }
                }
            }
            for (var i = 0; i < box.options.length; i++) {
                box.options[i].value = temp_opts[i].value;
                box.options[i].text = temp_opts[i].text;
            }
        }
        function Save() {
            var str = '';
            //            var rdolist1 = -1; 
            //            if (document.form1.rblUserType) {
            //                var rdolist = document.getElementsByName("rblUserType");

            //                if (rdolist[0].checked)
            //                    rdolist1 = 1;
            //                if (rdolist[1].checked)
            //                    rdolist1 = 2;
            //                if (rdolist[2].checked)
            //                    rdolist1 = 3;
            //            }

            //            if (rdolist1 == 3) {
            //                var fbox = document.form1.lstUserAvail;
            //                var tbox = document.form1.lstUserSelected;
            //                var Available = false;

            //                for (var i = 0; i < fbox.options.length; i++) {
            //                    for (var j = 0; j < tbox.options.length; j++) {
            //                        if (tbox.options[j].value == fbox.options[i].value) {
            //                            Available = true;
            //                        }
            //                    }

            //                    if (Available == false) {
            //                        var no = new Option();
            //                        no.value = fbox.options[i].value;
            //                        no.text = fbox.options[i].text;
            //                        tbox.options[tbox.options.length] = no;
            //                        fbox.options[i].value = "";
            //                        fbox.options[i].text = "";
            //                    }

            //                    Available = false;
            //                }
            //            }

            for (var i = 0; i < document.form1.lstUserSelected.options.length; i++) {
                var SelectedText, SelectedValue;
                SelectedValue = document.form1.lstUserSelected.options[i].value;
                SelectedText = document.form1.lstUserSelected.options[i].text;
                str = str + SelectedValue + ','
            }

            document.form1.hdnValue.value = str;
        }
        function Close() {
            window.close()
            return false;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <div class="right-input">
        <div class="input-part">
            <table cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td align="right">
                        <asp:Button ID="btnSave" runat="server" Text="Save &amp; Close" CssClass="button">
                        </asp:Button>
                        &nbsp;
                        <input type="button" id="btnClose" runat="server" value="Close" class="button" onclick="javascript: Close();">
                        &nbsp;
                    </td>
                    <td class="leftBorder" valign="bottom">
                        <a href="#" class="help">&nbsp;</a> &nbsp;&nbsp;
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageTitle" runat="server" ClientIDMode="Static">
    Select Users
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="Content" runat="server" ClientIDMode="Static">
    <table cellspacing="0" cellpadding="0" width="100%" border="0">
        <tr>
            <td colspan="3" height="7">
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <asp:Table ID="tblUsers" runat="server" BorderColor="black" GridLines="None" Width="100%"
                    CellSpacing="0" CssClass="aspTable" CellPadding="0" BorderWidth="1">
                    <asp:TableRow>
                        <asp:TableCell>
                            <table border="0">
                                <tr>
                                    <td class="normal1" colspan="3" >
                                        <asp:RadioButtonList runat="server" ID="rblUserType">
                                            <asp:ListItem Text="Records where “Record Owner” is one of the selected users" Value="1" />
                                            <asp:ListItem Text="Records where “Record Assignee” is one of the selected users"
                                                Value="2" />
                                            <asp:ListItem Text="Records where selected users are Record owners or asignees" Value="3"
                                                Selected="True" />
                                        </asp:RadioButtonList>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="normal1" >
                                        Available Users<br>
                                        <br>
                                        <asp:ListBox ID="lstUserAvail" runat="server" Width="200" Height="80" CssClass="signup"
                                            SelectionMode="Multiple"></asp:ListBox>
                                    </td>
                                    <td align="center">
                                        <input type="button" id="btnAdd" class="button" value="Add >" onclick="javascript:move(document.form1.lstUserAvail,document.form1.lstUserSelected)">
                                        <br>
                                        <input type="button" id="btnRemove" class="button" value="< Remove" onclick="javascript:remove1(document.form1.lstUserSelected,document.form1.lstUserAvail)">
                                    </td>
                                    <td class="normal1">
                                        The user will ONLY be able to access
                                        <br>
                                        records from the following Users.<br>
                                        <asp:ListBox ID="lstUserSelected" Width="200" Height="80" runat="server" CssClass="signup"
                                            SelectionMode="Multiple"></asp:ListBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="normal1" colspan="3">
                                        <b>How to use</b> – In addition to the query criteria, records will be filtered
                                        the above selected record ownership or assignee selections.
                                    </td>
                                </tr>
                            </table>
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </td>
        </tr>
    </table>
    <asp:Literal ID="ltClientScript" runat="server"></asp:Literal>
    <asp:TextBox ID="hdnValue" runat="server" Style="display: none"></asp:TextBox>
</asp:Content>
