<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" Codebehind="cflwizard2.aspx.vb" Inherits="BACRM.UserInterface.Admin.cflwizard2"  MasterPageFile="~/common/Popup.Master"%>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
		<title>Custom Field Wizard</title>
		<script language="javascript">
		    function Close() {
		        window.close()
		        return false;
		    }
		    function Validate() {
		        if (document.getElementById('CFWFgrp').value == 0) {
		            alert("Select Group")
		            document.getElementById('CFWFgrp').focus()
		            return false;
		        }
		    }
		</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
   
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageTitle" runat="server" ClientIDMode="Static">
    Step 1
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="Content" runat="server" ClientIDMode="Static">
 	<table width="400px" width ="100%" class="aspTableDTL">
				<tr>
					<td class="normal2" colspan="2" align="center">Select the area within BizAutomation 
						CRM<br>
						where you want your custom field to reside.
                        <br>
                        <br>
						
					</td>
				</tr>
				<tr>
					<td class="normal2" colspan="2" align="center"><i> *Selecting Leads/Prospects/Accounts 
							will add your<BR>
							custom field to the details section of the<BR>
							Leads, Prospects, AND Accounts tab view screens. </i>
						<br>
						<br>
					</td>
				</tr>
				<tr>
					<td class="normal1" align="right" valign="top">Select Location <FONT color="#ff3300">*</FONT></td>
					<td valign="top">&nbsp;<asp:dropdownlist id="CFWFgrp" runat="server" cssclass="signup"></asp:dropdownlist><br>
						<br>
						<br />
					</td>
				</tr>
				<tr>
					<td align="center" colspan="2">
						<asp:Button ID="btnNext" Width="45" Runat="server" CssClass="button" Text="Next"></asp:Button>
						<asp:Button ID="btnCancel" Width="50" Runat="server" CssClass="button" Text="Close"></asp:Button>
						<br>
						<br>
						<br>
						<br>
						<br><br>
					</td>
				</tr>
			</table>
</asp:Content>
