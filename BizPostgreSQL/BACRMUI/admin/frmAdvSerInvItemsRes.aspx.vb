﻿Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Item

Namespace BACRM.UserInterface.Admin
    Public Class frmAdvSerInvItemsRes
        Inherits BACRMPage
        Dim strColumn As String
        Dim boolDelete, boolExport, boolUpdate As Boolean

        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            Try
                'CLEAR ERROR ON RELOAD
                DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ""
                DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "none")

                'CLEAR ALERT MESSAGE
                divMessage.Style.Add("display", "none")
                litMessage.Text = ""

                GetUserRightsForPage(9, 11)
                If Not IsPostBack Then
                    ViewState("AdvancedSearchCondition") = Session("AdvancedSearchCondition")
                    ViewState("AdvancedSearchSavedSearchID") = Session("AdvancedSearchSavedSearchID")
                    Session("AdvancedSearchCondition") = Nothing
                    Session("AdvancedSearchSavedSearchID") = Nothing

                    Dim m_aryRightsEditSearchViews(), m_aryRightsMDelete(), m_aryRightsMUpdate() As Integer
                    If m_aryRightsForPage(RIGHTSTYPE.VIEW) = 0 Then Response.Redirect("../admin/authentication.aspx?mesg=AS", False)

                    m_aryRightsEditSearchViews = GetUserRightsForPage_Other(9, 2)
                    m_aryRightsMDelete = GetUserRightsForPage_Other(9, 3)
                    m_aryRightsMUpdate = GetUserRightsForPage_Other(9, 4)

                    If m_aryRightsEditSearchViews(RIGHTSTYPE.VIEW) <> 0 Then hplEditResView.Attributes.Add("onclick", "return EditSearchView();")
                    If m_aryRightsForPage(RIGHTSTYPE.EXPORT) = 0 Then btnExportToExcel.Visible = False
                    If m_aryRightsMDelete(RIGHTSTYPE.VIEW) = 0 Then btnMassDelete.Visible = False
                    If m_aryRightsMUpdate(RIGHTSTYPE.VIEW) = 0 Then btnMassUpdate.Visible = False

                    'Saved search
                    If CCommon.ToString(Session("SavedSearchCondition")) = "" Then hplSaveSearch.Visible = False
                    Dim lngSearchID As Long = CCommon.ToLong(GetQueryStringVal("SearchID"))
                    If lngSearchID > 0 Then
                        Dim objSearch As New FormGenericAdvSearch
                        objSearch.SetSavedSearchQuery(lngSearchID, 29)
                    End If

                    txtCurrrentPage.Text = 1
                    BindGridView(True)
                    chkSelectAll.Attributes.Add("onclick", "return SelectAll(" & chkSelectAll.ClientID & ")")
                End If

                If txtReload.Text = "true" Then
                    BindGridView(True)
                    txtReload.Text = "false"
                End If

            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub
        Sub BindGridView(ByVal CreateCol As Boolean)
            Try
                Dim objAdmin As New FormGenericAdvSearch
                'Dim SortChar As Char
                'If ViewState("SortChar") <> "" Then
                '    SortChar = ViewState("SortChar")
                'Else : SortChar = "0"
                'End If
                Dim dsResults As DataSet
                With objAdmin
                    .QueryWhereCondition = Session("WhereConditionItem")
                    .AreasOfInt = Session("AreasOfInt")
                    .DomainID = Session("DomainID")
                    .UserCntID = Session("UserContactID")
                    .DisplayColumns = CCommon.ToString(GetQueryStringVal("displayColumns"))
                    .PageSize = Session("PagingRows")
                    .UserRightType = m_aryRightsForPage(RIGHTSTYPE.VIEW)
                    .FormID = 29
                    .SortCharacter = txtSortChar.Text.Trim()
                    .SortcolumnName = IIf(txtSortColumn.Text.ToLower() = "bintcreateddate", "DM.bintCreatedDate", txtSortColumn.Text)
                    If txtCurrrentPage.Text.Trim = "" Then txtCurrrentPage.Text = 1
                    .CurrentPage = txtCurrrentPage.Text.Trim()
                    .PageSize = Session("PagingRows")
                    .TotalRecords = 0
                    If txtSortOrder.Text = "D" Then
                        .columnSortOrder = "Desc"
                    Else : .columnSortOrder = "Asc"
                    End If

                    'If cbShowAll.Checked = False Then
                    '    .columnSearch = txtColValue.Value
                    '    .columnName = txtColumnName.Text
                    'Else
                    .columnSearch = ""
                    .columnName = ""
                    'End If
                    If ViewState("SortChar") <> "" Then .columnName = "" 'ddlSort.SelectedValue

                    Dim i As Integer
                    If boolDelete = True Then
                        .GetAll = True
                        dsResults = .AdvancedSearchItems()

                        Dim objItems As New CItems
                        Dim ds As DataSet
                        Dim strItemName As String
                        Dim decAverageCost As Decimal
                        Dim lngAssetCOA As Long
                        Dim dtItemImages As DataTable
                        Dim dtItemDetails As DataTable
                        For i = 0 To dsResults.Tables(0).Rows.Count - 1
                            Try
                                objItems.ItemCode = dsResults.Tables(0).Rows(i)("numItemCode")
                                objItems.DomainID = Session("DomainID")
                               
                                dtItemImages = objItems.ImageItemDetails()

                                'get warehouse data
                                objItems.byteMode = 1
                                ds = objItems.GetItemWareHouses()
                                'get item details
                                dtItemDetails = objItems.ItemDetails()
                                lngAssetCOA = CCommon.ToLong(dtItemDetails.Rows(0)("numAssetChartAcntId"))
                                strItemName = CCommon.ToString(dtItemDetails.Rows(0)("vcItemName"))

                                Using objTransactionScope As New System.Transactions.TransactionScope(Transactions.TransactionScopeOption.Required, New System.Transactions.TransactionOptions() With {.IsolationLevel = Transactions.IsolationLevel.ReadCommitted, .Timeout = Transactions.TransactionManager.MaximumTimeout})
                                    objItems.DeleteItems()

                                    'Make Adjustment entry for Stock
                                    If ds.Tables(0).Rows.Count > 0 Then
                                        For Each dr As DataRow In ds.Tables(0).Rows
                                            objItems.MakeItemQtyAdjustmentJournal(objItems.ItemCode, CCommon.ToString(strItemName), -1 * CCommon.ToDouble(dr("OnHand")), CCommon.ToDecimal(decAverageCost), CCommon.ToLong(lngAssetCOA), Session("UserContactID"), Session("DomainID"))
                                        Next
                                    End If

                                    objTransactionScope.Complete()
                                End Using

                                Dim FilePath As String = CCommon.GetDocumentPhysicalPath(Session("DomainID")) & "WebAPI_Item_" & CCommon.ToString(Session("DomainID")) & "_" & CCommon.ToString(objItems.ItemCode) & ".xml"
                                If System.IO.File.Exists(FilePath) Then
                                    System.IO.File.Delete(FilePath)
                                End If

                                If dtItemImages.Rows.Count > 0 Then
                                    For Each row As DataRow In dtItemImages.Rows
                                        If row("vcPathForImage") <> "" Then
                                            If System.IO.File.Exists(CCommon.GetDocumentPhysicalPath(Context.Session("DomainID")) & row("vcPathForImage")) Then
                                                System.IO.File.Delete(CCommon.GetDocumentPhysicalPath(Context.Session("DomainID")) & row("vcPathForImage"))
                                            End If
                                        End If
                                        If row("vcPathForTImage") <> "" Then
                                            If System.IO.File.Exists(CCommon.GetDocumentPhysicalPath(Context.Session("DomainID")) & row("vcPathForTImage")) Then
                                                System.IO.File.Delete(CCommon.GetDocumentPhysicalPath(Context.Session("DomainID")) & row("vcPathForTImage"))
                                            End If
                                        End If
                                    Next
                                End If

                            Catch ex As Exception
                                ShowMessage("Some items can not be deleted, cause: Dependent Records Exists")
                            End Try
                        Next
                        .GetAll = False

                    End If
                    If boolExport = True Then
                        .GetAll = True
                        dsResults = .AdvancedSearchItems()
                        dsResults.Tables(0).Columns.RemoveAt(3)
                        dsResults.Tables(0).Columns.RemoveAt(2)
                        dsResults.Tables(0).Columns.RemoveAt(1)
                        dsResults.Tables(0).Columns.RemoveAt(0)
                        Dim iExportCount As Integer
                        Response.Clear()
                        For iExportCount = 0 To dsResults.Tables(0).Columns.Count - 1
                            dsResults.Tables(0).Columns(iExportCount).ColumnName = dsResults.Tables(0).Columns(iExportCount).ColumnName.Split("~")(0)
                        Next
                        For Each column As DataColumn In dsResults.Tables(0).Columns
                            Response.Write(column.ColumnName + ",")
                        Next
                        Response.Write(Environment.NewLine)
                        For Each row As DataRow In dsResults.Tables(0).Rows
                            For iExportCount = 0 To dsResults.Tables(0).Columns.Count - 1
                                Response.Write("""" & row(iExportCount).ToString().Replace(";", String.Empty).Replace(Environment.NewLine, "").Replace("""", """""") & """" & ",")
                            Next
                            Response.Write(Environment.NewLine)
                        Next
                        Response.ContentType = "text/csv"
                        Response.AppendHeader("Content-Disposition", "attachment; filename=" + "File" + Format(Now, "ddmmyyyyhhmmss") + ".csv")
                        Response.End()
                        .GetAll = False
                    End If
                    If boolUpdate = True Then
                        If chkSelectAll.Checked = False Then
                            GetCheckedValues()
                            If Session("ItemIDs") <> "" Then
                                .strMassUpdate = CCommon.ToString(Session("UpdateQuery")).Replace("@$replace", Session("ItemIDs")).Replace("##ItemCodes##", Session("ItemIDs"))
                                .bitMassUpdate = 1
                            Else
                                .bitMassUpdate = 0
                            End If
                        Else
                            Session("ItemIDs") = Nothing

                            If CCommon.ToString(Session("UpdateQuery")).Contains("USP_ItemCategory_MassUpdate") Then
                                .strMassUpdate = Session("UpdateQuery")
                            Else
                                .strMassUpdate = Session("UpdateQuery").Replace("SELECT Id,0,1 FROM SplitIDs('@$replace',',')", "Select numItemID,0,1 FROM #tempTable").Replace("@$replace", "select numItemID from #tempTable")
                            End If

                            .bitMassUpdate = 1
                        End If
                        .lookTable = Session("LookTable")
                        Session("UpdateQuery") = Nothing
                        Session("LookTable") = Nothing
                    End If
                    dsResults = .AdvancedSearchItems()
                    Dim dtColumns As DataTable
                    dtColumns = dsResults.Tables(1)

                    bizPager.PageSize = Session("PagingRows")
                    bizPager.CurrentPageIndex = txtCurrrentPage.Text
                    bizPager.RecordCount = objAdmin.TotalRecords

                    If CreateCol = True Then
                        If gvSearch.Columns.Count > 0 Then
                            gvSearch.Columns.Clear()
                        End If

                        Dim bField As BoundField
                        Dim Tfield As TemplateField

                        For i = 0 To dtColumns.Rows.Count - 1
                            If i < 1 Or dtColumns.Rows(i).Item("vcDbColumnName") = "ID" Then
                                bField = New BoundField
                                bField.DataField = dtColumns.Rows(i).Item("vcDbColumnName")
                                bField.Visible = False
                                gvSearch.Columns.Add(bField)
                            Else
                                Tfield = New TemplateField
                                Tfield.HeaderTemplate = New MyTemp1(ListItemType.Header, dtColumns.Rows(i).Item("vcFormFieldName"), dtColumns.Rows(i).Item("vcFormFieldName") + "~" + dtColumns.Rows(i).Item("vcDbColumnName"), "numItemCode")
                                Tfield.ItemTemplate = New MyTemp1(ListItemType.Item, dtColumns.Rows(i).Item("vcFormFieldName"), dtColumns.Rows(i).Item("vcFormFieldName") + "~" + dtColumns.Rows(i).Item("vcDbColumnName"), "numItemCode")
                                gvSearch.Columns.Add(Tfield)
                            End If
                        Next
                        'If i >= 5 Then
                        Tfield = New TemplateField
                        Tfield.HeaderStyle.Width = New Unit(25)
                        Tfield.HeaderTemplate = New MyTemp1(ListItemType.Header, "CheckBox", "", "numItemCode")
                        Tfield.ItemTemplate = New MyTemp1(ListItemType.Item, "CheckBox", "", "numItemCode")
                        gvSearch.Columns.Add(Tfield)
                        'End If
                    End If

                    gvSearch.DataSource = dsResults
                    gvSearch.DataBind()
                End With
            Catch ex As Exception
                If ex.Message.Contains("Invalid column name") Then
                    ShowMessage("Please add column you searched into current search result view")
                    Exit Sub
                Else
                    Throw ex
                End If
            End Try
        End Sub
        Private Sub GetCheckedValues()
            Try
                Session("ItemIDs") = ""
                Session("ItemIDs") = ""
                Dim gvRow As GridViewRow
                For Each gvRow In gvSearch.Rows
                    If CType(gvRow.FindControl("chk"), CheckBox).Checked = True Then
                        Session("ItemIDs") = Session("ItemIDs") & CType(gvRow.FindControl("lbl1"), Label).Text & ","
                    End If
                Next
                Session("ItemIDs") = Session("ItemIDs").ToString.TrimEnd(",")
            Catch ex As Exception
                Throw
            End Try
        End Sub
        Private Sub gvSearch_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvSearch.RowDataBound
            Try
                If e.Row.RowType = DataControlRowType.DataRow Then
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub btnMassDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnMassDelete.Click
            Try
                If chkSelectAll.Checked = True Then
                    boolDelete = True
                    BindGridView(False)
                Else
                    Dim objItems As New CItems
                    Dim gvRow As GridViewRow
                    'Dim ds As DataSet
                    'Dim strItemName As String
                    'Dim decAverageCost As Decimal
                    'Dim lngAssetCOA As Long
                    Dim dtItemImages As DataTable
                    'Dim dtItemDetails As DataTable
                    For Each gvRow In gvSearch.Rows
                        If CType(gvRow.FindControl("chk"), CheckBox).Checked = True Then

                            objItems.ItemCode = CCommon.ToLong(CType(gvRow.FindControl("lbl1"), Label).Text)
                            objItems.DomainID = Session("DomainID")


                            dtItemImages = objItems.ImageItemDetails()

                            ''get warehouse data
                            'objItems.byteMode = 1
                            'ds = objItems.GetItemWareHouses()
                            ''get item details
                            'dtItemDetails = objItems.ItemDetails()
                            'lngAssetCOA = CCommon.ToLong(dtItemDetails.Rows(0)("numAssetChartAcntId"))
                            'strItemName = CCommon.ToString(dtItemDetails.Rows(0)("vcItemName"))
                            'decAverageCost = CCommon.ToString(dtItemDetails.Rows(0)("monAverageCost"))

                            If objItems.DeleteItems = False Then
                                ShowMessage("Some items can not be deleted, cause: Dependent Records Exists")
                            End If

                            ''Make Adjustment entry for Stock
                            'If ds.Tables(0).Rows.Count > 0 Then
                            '    For Each dr As DataRow In ds.Tables(0).Rows
                            '        objItems.MakeItemQtyAdjustmentJournal(objItems.ItemCode, CCommon.ToString(strItemName), -1 * CCommon.ToInteger(dr("OnHand")), CCommon.ToDecimal(decAverageCost), CCommon.ToLong(lngAssetCOA), Session("UserContactID"), Session("DomainID"))
                            '    Next
                            'End If

                            Dim FilePath As String = CCommon.GetDocumentPhysicalPath(Session("DomainID")) & "WebAPI_Item_" & CCommon.ToString(Session("DomainID")) & "_" & CCommon.ToString(objItems.ItemCode) & ".xml"
                            If System.IO.File.Exists(FilePath) Then
                                System.IO.File.Delete(FilePath)
                            End If

                            If dtItemImages.Rows.Count > 0 Then
                                For Each row As DataRow In dtItemImages.Rows
                                    If row("vcPathForImage") <> "" Then
                                        If System.IO.File.Exists(CCommon.GetDocumentPhysicalPath(Context.Session("DomainID")) & row("vcPathForImage")) Then
                                            System.IO.File.Delete(CCommon.GetDocumentPhysicalPath(Context.Session("DomainID")) & row("vcPathForImage"))
                                        End If
                                    End If
                                    If row("vcPathForTImage") <> "" Then
                                        If System.IO.File.Exists(CCommon.GetDocumentPhysicalPath(Context.Session("DomainID")) & row("vcPathForTImage")) Then
                                            System.IO.File.Delete(CCommon.GetDocumentPhysicalPath(Context.Session("DomainID")) & row("vcPathForTImage"))
                                        End If
                                    End If
                                Next
                            End If
                        End If

                    Next
                    BindGridView(False)
                End If
            Catch ex As Exception
                Select Case ex.Message
                    Case "OpportunityItems_Depend"
                        ShowMessage("Deleted items is being used by Opportunity, It can not be deleted.")
                    Case "OpportunityKitItems_Depend"
                        ShowMessage("Deleted items is being used by Opportunity Kit Items, It can not be deleted.")
                    Case "KitItems_Depend"
                        ShowMessage("Deleted items is being used by Kit Items, It can not be deleted.")
                    Case Else
                        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                        DisplayError(CCommon.ToString(ex))
                End Select
            End Try
        End Sub


        Private Sub btnExportToExcel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExportToExcel.Click
            Try
                boolExport = True
                BindGridView(True)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Private Sub btnUpdateValues_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpdateValues.Click
            Try
                boolUpdate = True
                BindGridView(True)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Private Sub btnUpdateVendor_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpdateVendor.Click
            Try
                boolUpdate = True

                Dim gvRow As GridViewRow
                If Session("UpdateQuery") <> "" Then
                    If CCommon.ToString(Session("UpdateQuery")).Split("~")(0) > 0 Then
                        For Each gvRow In gvSearch.Rows
                            If CType(gvRow.FindControl("chk"), CheckBox).Checked = True Then
                                Dim objItems As New CItems
                                objItems.DivisionID = CCommon.ToString(Session("UpdateQuery")).Split("~")(0)
                                objItems.DomainID = Session("DomainID")
                                objItems.ItemCode = CCommon.ToLong(CType(gvRow.FindControl("lbl1"), Label).Text)
                                objItems.IsPrimaryVendor = CCommon.ToString(Session("UpdateQuery")).Split("~")(1)
                                objItems.AddVendor()
                            End If
                        Next
                    End If
                End If
                Session("UpdateQuery") = ""
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Private Sub btnGo1_Click(sender As Object, e As System.EventArgs) Handles btnGo1.Click
            Try
                BindGridView(True)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Protected Sub bizPager_PageChanged(sender As Object, e As EventArgs)
            Try
                txtCurrrentPage.Text = bizPager.CurrentPageIndex
                BindGridView(True)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Private Sub DisplayError(ByVal exception As String)
            Try
                DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = exception
                DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
                DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Focus()
            Catch ex As Exception

            End Try
        End Sub

        Private Sub ShowMessage(ByVal message As String)
            Try
                divMessage.Style.Add("display", "")
                litMessage.Text = message
                divMessage.Focus()
            Catch ex As Exception
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Private Sub lkbBackToSearchCriteria_Click(sender As Object, e As EventArgs) Handles lkbBackToSearchCriteria.Click
            Try
                Session("AdvancedSearchCondition") = ViewState("AdvancedSearchCondition")
                Session("AdvancedSearchSavedSearchID") = ViewState("AdvancedSearchSavedSearchID")
                Response.Redirect("~/admin/frmAdvancedSearchNew.aspx", False)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub
    End Class
    Public Class MyTemp1
        Implements ITemplate

        Dim TemplateType As ListItemType
        Dim DisplayName, DbColumnName, PrimaryKey As String

        Sub New(ByVal type As ListItemType, ByVal strDisplayName As String, ByVal strDbColumnName As String, ByVal fld3 As String)
            Try
                TemplateType = type
                DisplayName = strDisplayName
                DbColumnName = strDbColumnName
                PrimaryKey = fld3
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub InstantiateIn(ByVal Container As Control) Implements ITemplate.InstantiateIn
            Try
                Dim lbl1 As Label = New Label()
                Dim lnkButton As New LinkButton
                'Dim lnk As New HyperLink
                Select Case TemplateType
                    Case ListItemType.Header
                        If DisplayName <> "CheckBox" Then
                            Dim str() As String = DbColumnName.Split("~")

                            lnkButton.ID = DbColumnName
                            lnkButton.Text = DisplayName
                            If str.Length >= 1 Then
                                lnkButton.Attributes.Add("onclick", "return SortColumn('" & str(1) & "')")
                            End If
                            Container.Controls.Add(lnkButton)
                        Else
                            Dim chk As New CheckBox
                            chk.ID = "chkGridSelectAll"
                            chk.Attributes.Add("onclick", "return SelectAll('chkGridSelectAll','chkSelect')")
                            Container.Controls.Add(chk)
                        End If
                    Case ListItemType.Item
                        If DisplayName <> "CheckBox" Then
                            AddHandler lbl1.DataBinding, AddressOf BindStringColumn
                            Container.Controls.Add(lbl1)
                        Else
                            Dim chk As New CheckBox
                            chk.ID = "chk"
                            chk.CssClass = "chkSelect"

                            AddHandler lbl1.DataBinding, AddressOf Bindvalue
                            Container.Controls.Add(chk)
                            Container.Controls.Add(lbl1)
                        End If
                End Select
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub Bindvalue(ByVal Sender As Object, ByVal e As EventArgs)
            Try
                Dim lbl1 As Label = CType(Sender, Label)
                Dim Container As GridViewRow = CType(lbl1.NamingContainer, GridViewRow)
                lbl1.ID = "lbl1"
                lbl1.Text = DataBinder.Eval(Container.DataItem, PrimaryKey)
                lbl1.Attributes.Add("style", "display:none")
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub BindStringColumn(ByVal Sender As Object, ByVal e As EventArgs)
            Try
                Dim lbl1 As Label = CType(Sender, Label)
                Dim Container As GridViewRow = CType(lbl1.NamingContainer, GridViewRow)

                If DisplayName = "Item Name" Then
                    lbl1.Text = "<a  href='javascript:OpenItem(" & DataBinder.Eval(Container.DataItem, PrimaryKey) & ",0)'>" & IIf(IsDBNull(DataBinder.Eval(Container.DataItem, DbColumnName)), "", DataBinder.Eval(Container.DataItem, DbColumnName)) & "</a> &nbsp;&nbsp;"
                    lbl1.Text += "<a  href='javascript:OpenItem(" & DataBinder.Eval(Container.DataItem, PrimaryKey) & ",1)'><img src='../images/open_new_window_notify.gif' width='15px' /></a>"
                ElseIf DisplayName = "Serial Number" Then
                    If DataBinder.Eval(Container.DataItem, "bitSerialized") = True Or DataBinder.Eval(Container.DataItem, "bitLotNo") = True Then
                        lbl1.Text = "<a  href='javascript:SerialNo(" & DataBinder.Eval(Container.DataItem, PrimaryKey) & "," & IIf(DataBinder.Eval(Container.DataItem, "bitSerialized") = True, 1, 0) & ")'>view</a> &nbsp;&nbsp;&nbsp;"
                    Else
                        lbl1.Text = ""
                    End If
                ElseIf DisplayName = "Vendor" Or DisplayName = "Vendor Part #" Then
                    lbl1.Text = IIf(IsDBNull(DataBinder.Eval(Container.DataItem, DbColumnName)), "", DataBinder.Eval(Container.DataItem, DbColumnName)) + "&nbsp;<a  href='javascript:OpenItem(" & DataBinder.Eval(Container.DataItem, PrimaryKey) & ",0)'> view</a> &nbsp;&nbsp;"
                    'lbl1.Text = "<a  href='javascript:OpenItem(" & DataBinder.Eval(Container.DataItem, PrimaryKey) & ",1)'><img src='../images/open_new_window_notify.gif' width='15px' /></a>"
                Else
                    lbl1.Text = IIf(IsDBNull(CType(Container.DataItem, DataRowView)(DbColumnName)), "", CCommon.ToString(CType(Container.DataItem, DataRowView)(DbColumnName)))
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub BindStringColumn1(ByVal Sender As Object, ByVal e As EventArgs)
            Try
                Dim lbl1 As Label = CType(Sender, Label)
                Dim Container As GridViewRow = CType(lbl1.NamingContainer, GridViewRow)
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

    End Class
End Namespace