<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" CodeBehind="frmCustomFieldTabs.aspx.vb"
    Inherits="BACRM.UserInterface.Admin.frmCustomFieldTabs" MasterPageFile="~/common/PopupBootstrap.Master" %>

<%@ Register Assembly="Telerik.Web.UI, Version=2011.2.712.40, Culture=neutral, PublicKeyToken=121fae78165ba3d4" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Custom Field Sub-tabs</title>
    <script type="text/javascript">
        function DeleteRecord() {
            if (confirm('Are you sure, you want to delete the selected record?')) {
                return true;
            }
            else {
                return false;
            }
        }
        function AddTab() {
            if (document.getElementById("ddlType").value == 0) {
                if (document.getElementById("txtTabName").value == '') {
                    alert("Enter Tab Name")
                    document.getElementById("txtTabName").focus()
                    return false;
                }
                if (document.getElementById("ddlLocation").value == 0) {
                    alert("Enter Location")
                    document.getElementById("ddlLocation").focus()
                    return false;
                }
            }
            else if (document.getElementById("ddlType").value == 1) {
                if (document.getElementById("txtTabName").value == '') {
                    alert("Enter Tab Name")
                    document.getElementById("txtTabName").focus()
                    return false;
                }
                if (document.getElementById("ddlLocation").value == 0) {
                    alert("Enter Location")
                    document.getElementById("ddlLocation").focus()
                    return false;
                }
                if (document.getElementById("txtUrl").value == 'http://' || document.getElementById("txtUrl").value == '') {
                    alert("Enter URL")
                    document.getElementById("txtUrl").focus()
                    return false;
                }
            }
        }
        function Save() {
            if (document.getElementById("ddlGroup").value == 0) {
                alert("Select Permission Group")
                document.getElementById("ddlGroup").focus()
                return false;
            }
            return true;
        }

        $(document).ready(function () {
            var chkBox = $("input[id$='chkSelectAll']");
            chkBox.click(
          function () {
              $("#dgTabs INPUT[type='checkbox']").prop('checked', chkBox.is(':checked'));
          });
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <div class="row">
        <div class="col-xs-12">
            <div class="pull-left">
                <div class="form-inline">
                    
                </div>
            </div>
            <div class=" pull-right">
                <div class="form-inline">
                    <div class="form-group">
                        <label>Apply configuration to</label>
                        <telerik:RadComboBox ID="rcbGroups" runat="server" CheckBoxes="true"></telerik:RadComboBox>
                    </div>
                    <asp:Button ID="btnSave" runat="server" Text="Save Permission" CssClass="btn btn-primary" OnClientClick="return Save();"></asp:Button>
                    <asp:Button ID="btnClose" runat="server" CssClass="btn btn-primary" Text="Close"></asp:Button>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageTitle" runat="server" ClientIDMode="Static">
    Custom Field Sub-tabs
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="Content" runat="server" ClientIDMode="Static">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <div class="row padbottom10" id="divError" runat="server" style="display: none">
        <div class="col-sm-12">
            <div class="alert alert-danger">
                <h4><i class="icon fa fa-ban"></i>Error</h4>
                <p>
                    <asp:Label ID="lblError" runat="server"></asp:Label>
                </p>
            </div>
        </div>
    </div>
    <div class="row padbottom10">
        <div class="col-xs-12">
            <div class="form-inline">
                <div class="form-group">
                    <label>Group</label>
                    <asp:DropDownList ID="ddlGroup" runat="server" AutoPostBack="True" CssClass="form-control"></asp:DropDownList>
                </div>
                 <div class="form-group">
                    <label>Location</label>
                    <asp:DropDownList ID="ddlLocation" AutoPostBack="True" runat="server" CssClass="form-control">
                        <asp:ListItem Value="0">--Select One--</asp:ListItem>
                        <asp:ListItem Value="1">Leads/Prospects/Accounts</asp:ListItem>
                        <asp:ListItem Value="3">Case Details</asp:ListItem>
                        <asp:ListItem Value="4">Contact Details</asp:ListItem>
                        <asp:ListItem Value="2">Sales Opportunities / Orders</asp:ListItem>
                        <asp:ListItem Value="6">Purchase Opportunities / Orders</asp:ListItem>
                        <asp:ListItem Value="11">Projects</asp:ListItem>
                        <asp:ListItem Value="12">Prospects</asp:ListItem>
                        <asp:ListItem Value="13">Accounts</asp:ListItem>
                        <asp:ListItem Value="14">Leads</asp:ListItem>
                        <asp:ListItem Value="15">Items</asp:ListItem>
                        <asp:ListItem Value="17">Activities</asp:ListItem>
                    </asp:DropDownList>
                </div>
                <div class="form-group">
                    <label>Tab Name</label>
                    <asp:TextBox ID="txtTabName" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
                <div class="form-group">
                    <label>Tab Type</label>
                    <asp:DropDownList ID="ddlType" runat="server" CssClass="form-control" AutoPostBack="true">
                        <asp:ListItem Value="0">Tab For Normal Fields</asp:ListItem>
                        <asp:ListItem Value="1">Tab For Frame</asp:ListItem>
                    </asp:DropDownList>
                </div>
                <div class="form-group" id="divURL" runat="server" visible="false">
                    <label>Frame Url</label>
                    <asp:TextBox runat="server" ID="txtUrl" Text="http://" CssClass="form-control"></asp:TextBox>
                </div>
                <asp:Button ID="btnAdd" runat="server" Text="Add Tab" CssClass="btn btn-primary"></asp:Button>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="table-responsive">
                <asp:DataGrid ID="dgTabs" runat="server" CssClass="table table-bordered table-striped" ShowHeader="true" UseAccessibleHeader="true" AutoGenerateColumns="False" AllowSorting="True" Width="100%">
                    <Columns>
                        <asp:TemplateColumn>
                            <ItemTemplate>
                                <asp:LinkButton runat="server" Text="Edit" CommandName="Edit" ID="lnkbtnEdt"></asp:LinkButton>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:LinkButton ID="lnkbtnUpdt" runat="server" Text="Update" CommandName="Update"></asp:LinkButton>&nbsp;
                                <asp:LinkButton runat="server" Text="Cancel" CommandName="Cancel" ID="lnkbtnCncl"></asp:LinkButton>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn>
                            <ItemTemplate>
                                <asp:Label ID="lblTabID" runat="server" Visible="False" Text='<%# DataBinder.Eval(Container,"DataItem.Grp_Id") %>'>
                                </asp:Label>
                                <%# Container.ItemIndex +1 %>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Tab Name">
                            <ItemTemplate>
                                <asp:Label ID="lblTabName" runat="server" Text='<%# DataBinder.Eval(Container,"DataItem.Grp_Name") %>'>
                                </asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtETabName" runat="server" CssClass="signup" Text='<%# DataBinder.Eval(Container,"DataItem.Grp_Name") %>'>
                                </asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Group Type">
                            <ItemTemplate>
                                <asp:Label ID="lblLocation" runat="server" Text='<%# DataBinder.Eval(Container,"DataItem.Loc_Name") %>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Tab Type">
                            <ItemTemplate>
                                <%# Eval("TabType") %>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn>
                            <HeaderTemplate>
                                <asp:CheckBox ID="chkSelectAll" runat="server" />
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:CheckBox ID="chk" runat="server" Checked='<%# Eval("bitAllowed") %>' />
                            </ItemTemplate>
                            <HeaderStyle Width="20" />
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderStyle-Width="10" ItemStyle-Width="10">
                            <ItemTemplate>
                                <asp:Button ID="btnDelete" runat="server" CssClass="button Delete" Text="X" CommandName="Delete"
                                    Visible='<%#IIf(Eval("tintType") = "2", "false", "true")%>'></asp:Button>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                </asp:DataGrid>
            </div>
        </div>
    </div>
</asp:Content>
