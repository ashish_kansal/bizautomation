<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" CodeBehind="frmAddState.aspx.vb"
    Inherits="BACRM.UserInterface.Admin.frmAddState" MasterPageFile="~/common/Popup.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>State</title>
    <script language="javascript">
        function Close() {
            window.close()
            return false;
        }
        function Save() {
            if (document.Form1.txtState.value == "") {
                alert("Enter State")
                document.Form1.txtState.focus()
                return false;
            }
            if (document.Form1.ddlCountry.value == 0) {
                alert("Select Country")
                document.Form1.ddlCountry.focus()
                return false;
            }
        }
        function ESave(a, b,c) {
            if (document.getElementById(a).value == "") {
                alert("Enter State")
                document.getElementById(a).focus()
                return false;
            }
            if (document.getElementById(b).value == 0) {
                alert("Select Country")
                document.getElementById(b).focus()
                return false;
            }
        }
        function DeleteRecord() {
            if (confirm('Are you sure, you want to delete the selected record?')) {
                return true;
            }
            else {
                return false;
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <div class="input-part">
        <div class="right-input">
            <table cellspacing="0" cellpadding="0" width="100%" border="0">
                <tr>
                    <td align="right">
                        <asp:Button ID="btnClose" Text="Close" CssClass="button" runat="server" Width="50">
                        </asp:Button>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table width="100%">
                            <tr>
                                <td class="normal4" align="center">
                                    <asp:Literal ID="litMessage" runat="server"></asp:Literal>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageTitle" runat="server" ClientIDMode="Static">
    State/Province/Region
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="Content" runat="server" ClientIDMode="Static">
    <table align="center" width="500px">
        <tr>
            <td class="normal1" align="right">
                State/Province/Region
            </td>
            <td>
                <asp:TextBox ID="txtState" runat="server" CssClass="signup" style="width:75% !important"></asp:TextBox>&nbsp;
                <asp:Button ID="btnAdd" runat="server" Width="60" Text="Add" CssClass="button"></asp:Button>
            </td>
        </tr>
        <tr>
            <td class="normal1" align="right">
                Abbreviations
            </td>
            <td>
                <asp:TextBox ID="txtAbbreviations" runat="server" CssClass="signup" style="width:75% !important"></asp:TextBox>&nbsp;
            </td>
        </tr>
        <tr>
        <td class="normal1" align="right">
                Shipping Zone
            </td>
            <td>
                <asp:DropDownList ID="ddlShippingZone" AutoPostBack="True" runat="server" CssClass="signup"  style="width:75% !important">
                </asp:DropDownList>
            </td>
         </tr>
        <tr>
            <td class="normal1" align="right">
                Country
            </td>
            <td>
                <asp:DropDownList ID="ddlCountry" AutoPostBack="True" runat="server" CssClass="signup"  style="width:75% !important">
                </asp:DropDownList>
            </td>
        </tr>
    </table>
    <br />
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td colspan="5">
                <asp:DataGrid ID="dgCountry" runat="server" CssClass="dg"  AutoGenerateColumns="False"
                    AllowSorting="True" Width="100%">
                    <AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
                    <ItemStyle CssClass="is"></ItemStyle>
                    <HeaderStyle CssClass="hs"></HeaderStyle>
                    <Columns>
                        <asp:TemplateColumn>
                            <ItemTemplate>
                                <asp:LinkButton runat="server" Text="Edit" CommandName="Edit" ID="lnkbtnEdt"></asp:LinkButton>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:LinkButton ID="lnkbtnUpdt" runat="server" Text="Update" CommandName="Update"></asp:LinkButton>&nbsp;
                                <asp:LinkButton runat="server" Text="Cancel" CommandName="Cancel" ID="lnkbtnCncl"></asp:LinkButton>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="No" Visible="false">
                            <ItemTemplate>
                                <asp:Label ID="lblStateID" runat="server" Visible="False" Text='<%# DataBinder.Eval(Container,"DataItem.numStateID") %>'>
                                </asp:Label>
                                <%# Container.ItemIndex +1 %>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="State">
                            <ItemTemplate>
                                <asp:Label ID="lblState" runat="server" Text='<%# DataBinder.Eval(Container,"DataItem.vcState") %>'>
                                </asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtEState" runat="server" CssClass="signup" Text='<%# DataBinder.Eval(Container,"DataItem.vcState") %>'>
                                </asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Abbreviations">
                            <ItemTemplate>
                                <asp:Label ID="lblAbbreviations" runat="server" Text='<%# DataBinder.Eval(Container,"DataItem.vcAbbreviations") %>'>
                                </asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtEAbbreviations" runat="server" CssClass="signup" Text='<%# DataBinder.Eval(Container,"DataItem.vcAbbreviations") %>'>
                                </asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Country">
                            <ItemTemplate>
                                <asp:Label ID="lblCountry" runat="server" Text='<%# DataBinder.Eval(Container,"DataItem.vcData") %>'>
                                </asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:Label ID="lblCountryID" runat="server" Visible="False" Text='<%# DataBinder.Eval(Container,"DataItem.numCountryID") %>'>
                                </asp:Label>
                                <asp:DropDownList ID="ddlECountry" runat="server" CssClass="signup">
                                </asp:DropDownList>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Shipping Zone">
                            <ItemTemplate>
                                <asp:Label ID="lblShippingZone" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.vcShippingZone")%>'>
                                </asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:Label ID="lblShippingZoneID" runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.numShippingZone")%>'>
                                </asp:Label>
                                <asp:DropDownList ID="ddlShippingZone" runat="server" CssClass="signup">
                                </asp:DropDownList>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderStyle-Width="10" ItemStyle-Width="10">
                            <HeaderTemplate>
                                <asp:Button ID="btnHdelete" runat="server" CssClass="button Delete" Text="X"></asp:Button>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Button ID="btnDelete" runat="server" CssClass="button Delete" Text="X" CommandName="Delete">
                                </asp:Button>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                </asp:DataGrid>
            </td>
        </tr>
    </table>
</asp:Content>
