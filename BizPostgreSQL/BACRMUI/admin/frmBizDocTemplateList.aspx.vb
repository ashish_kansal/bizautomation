﻿Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Opportunities

Public Class frmBizDocTemplateList
    Inherits BACRMPage

    Dim dtReports As DataTable
   

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ""
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "none")
            If Not IsPostBack Then
                PersistTable.Load()
                If PersistTable.Count > 0 Then
                    ddlOppType.SelectedValue = PersistTable(ddlOppType.ID)
                End If

                BindBizDoc()

                If PersistTable.Count > 0 AndAlso Not ddlBizDoc.Items.FindByValue(PersistTable(ddlBizDoc.ID)) Is Nothing Then
                    ddlBizDoc.SelectedValue = PersistTable(ddlBizDoc.ID)
                End If

                bindBizDocTemplate()
            End If
            btnAddNew.Attributes.Add("onclick", "return OpenBizDocTemplate(0)")
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub
    Private Sub DisplayError(ByVal ex As String)
        DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ex
        DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
    End Sub
    Sub BindBizDoc()
        Try

            objCommon.DomainID = Session("DomainID")
            objCommon.BizDocType = ddlOppType.SelectedValue

            ddlBizDoc.DataSource = objCommon.GetBizDocType
            ddlBizDoc.DataTextField = "vcData"
            ddlBizDoc.DataValueField = "numListItemID"
            ddlBizDoc.DataBind()

            ddlBizDoc.Items.Insert(0, New ListItem("All", 0))
            ddlBizDoc.SelectedIndex = 0
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub bindBizDocTemplate()
        Try
            Dim objOppBizDoc As New OppBizDocs
            objOppBizDoc.DomainID = Session("DomainID")
            objOppBizDoc.BizDocId = ddlBizDoc.SelectedValue
            objOppBizDoc.OppType = ddlOppType.SelectedValue

            Dim dt As DataTable = objOppBizDoc.GetBizDocTemplateList()

            gvBizDoc.DataSource = dt
            gvBizDoc.DataBind()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub ddlOppType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlOppType.SelectedIndexChanged
        Try
            BindBizDoc()
            bindBizDocTemplate()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Private Sub ddlBizDoc_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlBizDoc.SelectedIndexChanged
        Try
            bindBizDocTemplate()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Private Sub gvBizDoc_RowCommand(sender As Object, e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvBizDoc.RowCommand
        Try
            If e.CommandName = "Delete" Then
                Dim objOppBizDoc As New OppBizDocs
                objOppBizDoc.DomainID = Session("DomainID")
                objOppBizDoc.BizDocTemplateID = CCommon.ToLong(e.CommandArgument)
                objOppBizDoc.DeleteBizDocTemplate()

                bindBizDocTemplate()
            End If
            
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Private Sub gvBizDoc_RowDataBound(sender As Object, e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvBizDoc.RowDataBound
        Try
            If e.Row.RowType = ListItemType.AlternatingItem Or e.Row.RowType = ListItemType.Item Then
                Dim btnDelete As LinkButton
                btnDelete = e.Row.FindControl("btnDelete")
                btnDelete.Attributes.Add("onclick", "return DeleteRecord()")
                If CCommon.ToBool(CType(e.Row.DataItem, DataRowView)("bitDefault")) = True Then
                    btnDelete.Visible = False
                    'coded by Sachin
                    Dim btnnotDelete As LinkButton
                    btnnotDelete = e.Row.FindControl("btnnotDelete")
                    btnnotDelete.Visible = True
                    btnnotDelete.Attributes.Add("onclick", "return DeleteMessage()")

                    'Dim lnk As LinkButton
                    'lnk = e.Row.FindControl("lnkdelete")
                    'lnk.Visible = True
                    'lnk.Attributes.Add("onclick", "return DeleteMessage()")
                End If
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Private Sub gvBizDoc_RowDeleting(sender As Object, e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvBizDoc.RowDeleting
      
    End Sub

    Private Sub Page_PreRender(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.PreRender
        Try
            PersistTable.Clear()
            PersistTable.Add(ddlBizDoc.ID, ddlBizDoc.SelectedValue)
            PersistTable.Add(ddlOppType.ID, ddlOppType.SelectedValue)
            PersistTable.Save()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

End Class