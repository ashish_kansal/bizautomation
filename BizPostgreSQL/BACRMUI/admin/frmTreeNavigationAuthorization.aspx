<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmTreeNavigationAuthorization.aspx.vb"
    Inherits=".frmTreeNavigationAuthorization" MasterPageFile="~/common/PopupBootstrap.Master"
    ClientIDMode="Static" Title="Hide / Show Tree Nodes" %>

<%@ Register Assembly="Telerik.Web.UI, Version=2011.2.712.40, Culture=neutral, PublicKeyToken=121fae78165ba3d4" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<asp:Content ID="Content3" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <style>
         fieldset{
     border: 1px solid #1473B4 !important;
         padding: 6px;
 }
 legend{
         width: auto;
         border-bottom:0px !important;
         margin-left: 15px;
         margin-left: 15px;
    font-size: 15px;
    font-weight: bold;
    color: #1473b4;
 }
         .col-md-6{
            padding-left: 0px;
    padding-right: 0px;
        }
    </style>
    <script type="text/javascript">
        function MoveUp() {
            var treeView = $find("<%=radTV.ClientID%>");
            var selectedNode = treeView.get_selectedNode();

            if (selectedNode == null) {
                alert("Select node!")

            }
            else {
                var previousNode = selectedNode.get_previousNode();
                if (previousNode) {
                    return true;
                }
                else {
                    alert("Node cannot be moved up!")
                    return false;
                }
            }
        }

        function MoveDown() {
            var treeView = $find("<%=radTV.ClientID%>");
            var selectedNode = treeView.get_selectedNode();

            if (selectedNode == null) {
                alert("Select node!")
                return false;
            }
            else {
                var nextNode = selectedNode.get_nextNode();
                if (nextNode) {
                    return true;
                }
                else {
                    alert("Node cannot be moved down!");
                    return false;
                }
            }
        }
        function SetInitialPage(tbox) {

            if ($("#lstAddfldFav").val() != null) {
                var SelectedText, SelectedValue;

                for (var i = 0; i < tbox.options.length; i++) {
                    if (tbox.options[i].selected && tbox.options[i].value != "") {
                        SelectedText = tbox.options[i].text;
                        SelectedValue = tbox.options[i].value;

                        if (SelectedText.indexOf('(*)') != -1)
                            SelectedText = SelectedText.replace('(*)', '');
                        else
                            SelectedText = '(*) ' + SelectedText;

                        tbox.options[i].text = SelectedText;
                    }
                    else {
                        if (tbox.options[i].text.indexOf('(*)') != -1) {
                            SelectedText = tbox.options[i].text;
                            SelectedValue = tbox.options[i].value;
                            SelectedText = SelectedText.replace('(*)', '');
                            tbox.options[i].text = SelectedText;
                        }
                    }
                }
            }

            var strFav = '';
            document.getElementById("txtInitialPage").value = 0;

            for (var i = 0; i < document.getElementById("lstAddfldFav").options.length; i++) {
                var SelectedValueFav;
                SelectedValueFav = document.getElementById("lstAddfldFav").options[i].value;
                strFav = strFav + SelectedValueFav + ',';

                if (document.getElementById("lstAddfldFav").options[i].text.indexOf('(*)') != -1) {
                    document.getElementById("txtInitialPage").value = SelectedValueFav;
                }
            }

            document.getElementById("txthiddenFav").value = strFav;

            return true;
        }
    </script>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <div class="pull-right">
    <asp:Button runat="server" ID="Button1" OnClientClick="javascript:self.close()" Text="Close" CssClass="btn btn-primary" />
        </div> 
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="PageTitle" runat="server" ClientIDMode="Static">
    Default Page Configure & Hide / Show Navigation
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="Content" runat="server" ClientIDMode="Static">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <div class="col-md-6">
        <fieldset>
            <legend>Manage ShortCut Bar</legend>
            <div class="input-part">
                <div class="right-input">
                    <div class="form-inline pull-right">
                        <div class="form-group">
                            <label>Apply configuration to</label>
                            <telerik:RadComboBox ID="rcbShrCutGroups" runat="server" CheckBoxes="true"></telerik:RadComboBox>
                        </div>
                        <asp:Button ID="btnSetAsInitial" runat="server" Text="Save" CssClass="btn btn-primary" OnClientClick="return SetInitialPage(lstAddfldFav);" />
                       
                    </div>
                </div>
            </div>

            <div class="row">
                    <div class="col-xs-6">
                        <div class="form-group">
                            <label>Group</label>
                            <asp:DropDownList ID="ddlSrtCutGroup" runat="server" AutoPostBack="true" CssClass="form-control"></asp:DropDownList>
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="form-group">
                            <label>Tab</label>
                            <asp:DropDownList ID="ddlSrtCutTab" AutoPostBack="true" runat="server" CssClass="form-control"></asp:DropDownList>
                        </div>
                     
                    </div>
                <div class="col-md-12">
                       <div class="form-group">
                            <label>URL</label>
                            <asp:ListBox ID="lstAddfldFav" runat="server" Width="200" Height="200" CssClass="form-control"></asp:ListBox>
                        </div>
                </div>
                <div class="col-xs-12">
                    <asp:CheckBox ID="chkDefaultPage" runat="server" Text="Make this the initial tab that loads when users log into BizAutomation." />
                </div>
            </div>

            <asp:TextBox Style="display: none" ID="txthiddenFav" runat="server"></asp:TextBox>
            <asp:TextBox Style="display: none" ID="txtInitialPage" runat="server"></asp:TextBox>
        </fieldset>
    </div>
    <div class="col-md-6" style="padding-left: 5px;">
        <fieldset>
            <legend>Hide / Show Tree Nodes</legend>
            <div class="pull-right">
                <ul class="list-inline" style="margin-bottom: 0px">
                    <li>
                        <div class="form-inline">
                            <div class="form-group" style="margin-bottom: 0px">
                                <label>Apply configuration to</label>
                                <telerik:RadComboBox ID="rcbGroups" runat="server" CheckBoxes="true"></telerik:RadComboBox>
                            </div>
                        </div>

                    </li>
                    <li style="padding-left: 0px; padding-right: 0px">
                        <asp:Button runat="server" ID="btnSave" Text="Save" CssClass="btn btn-primary" />
                    </li>
                </ul>
            </div>
            <div class="row padbottom10" id="divError" runat="server" style="display: none">
                <div class="col-sm-12">
                    <div class="alert alert-danger">
                        <h4><i class="icon fa fa-ban"></i>Error</h4>
                        <p>
                            <asp:Label ID="lblError" runat="server"></asp:Label>
                        </p>
                    </div>
                </div>
            </div>
            <div class="row padbottom10">
                <div class="col-xs-6">
                    <div class="form-group">
                        <label>Group</label>
                        <asp:DropDownList ID="ddlGroup" runat="server" AutoPostBack="true" CssClass="form-control"></asp:DropDownList>
                    </div>
                </div>
                <div class="col-xs-6">
                    <div class="form-group">
                        <label>Tab</label>
                        <asp:DropDownList ID="ddlTab" AutoPostBack="true" runat="server" CssClass="form-control"></asp:DropDownList>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <div class="table-responsive">
                        <asp:DataGrid ID="dgNodes" Width="100%" runat="server" CssClass="table table-bordered table-striped" ShowHeader="true" UseAccessibleHeader="true" AutoGenerateColumns="False">
                            <Columns>
                                <asp:TemplateColumn HeaderText="No" HeaderStyle-Width="10" ItemStyle-Width="40">
                                    <ItemTemplate>
                                        <%# Container.ItemIndex +1 %>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Node Name">
                                    <ItemTemplate>
                                        <asp:Label runat="server" ID="lblNodeName" CssClass="normal1" Text='<%# DataBinder.Eval(Container,"DataItem.vcNodeName") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn>
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle HorizontalAlign="Center" />
                                    <HeaderTemplate>
                                        <input type="checkbox" id="chkAll" title="Check All" onclick="SelectAll('chkAll', 'chkSelect');" />
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:CheckBox ID="chkSelect" runat="server" CssClass="chkSelect" Checked='<%# DataBinder.Eval(Container,"DataItem.bitVisible") %>' />
                                        <asp:HiddenField runat="server" ID="hdnPageNavID" Value='<%# DataBinder.Eval(Container,"DataItem.numPageNavID") %>' />
                                        <asp:HiddenField runat="server" ID="hdnType" Value='<%# DataBinder.Eval(Container,"DataItem.tintType") %>' />
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                            </Columns>
                        </asp:DataGrid>
                        <asp:Panel runat="server" ID="pnlRelationship">
                            <asp:UpdatePanel runat="server" ID="uplRelationship" ChildrenAsTriggers="true">
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="btnMoveUp" />
                                    <asp:AsyncPostBackTrigger ControlID="btnMoveDown" />
                                </Triggers>
                                <ContentTemplate>
                                    <asp:Button ID="btnMoveUp" runat="server" Text="Move Up" CssClass="btn btn-primary" OnClientClick="return MoveUp();" Style="margin-left: 5px;" />
                                    <asp:Button ID="btnMoveDown" runat="server" Text="Move Down" CssClass="btn btn-primary" OnClientClick="return MoveDown();" />
                                    <telerik:RadTreeView ID="radTV" runat="server" Style="max-width: 580px; border: 1px solid #d7d7d7; margin: 5px" CheckBoxes="true" CheckChildNodes="true" TriStateCheckBoxes="false">
                                    </telerik:RadTreeView>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </asp:Panel>
                    </div>
                </div>
            </div>

        </fieldset>
    </div>

</asp:Content>
