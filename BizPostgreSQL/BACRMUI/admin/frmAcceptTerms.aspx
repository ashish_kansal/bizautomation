﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmAcceptTerms.aspx.vb"
    Inherits=".frmAcceptTerms" MasterPageFile="~/common/GridMasterRegular.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>BizAutomation CrediCard Saving Terms & Conditions</title>
    <script type="text/javascript">
        function accept1() {
            window.opener.document.getElementById('chkKeepCreditCardInfo').checked = true;
            window.close();
            return false;

        }
        function decline() {
            window.opener.document.getElementById('chkKeepCreditCardInfo').checked = false;
            window.close();
            return false;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server"
    ClientIDMode="Static">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server" ClientIDMode="Static">
    Terms & Conditions
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BizPager" runat="server" ClientIDMode="Static">
    &nbsp;
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="GridPlaceHolder" runat="server"
    ClientIDMode="Static">
    <asp:Table ID="table1" CellPadding="0" CellSpacing="0" runat="server" Width="100%"
        CssClass="asptable" GridLines="None" Height="300">
        <asp:TableRow>
            <asp:TableCell VerticalAlign="Top" HorizontalAlign="Center">
                <table>
                    <tr>
                        <td>
                            <asp:TextBox CssClass="normal2" ID="txtTerms" TextMode="MultiLine" Width="500px"
                                ReadOnly="true" runat="server" Text="Turning on this feature requires that you accept the terms and conditions associated with storing credit card values in the database.
    
BizAutomation.com uses the latest encryption technology (same as the one your bank uses) in order to store credit card values in the database securely, such that even if a hacker was to break in, they would not see any usable data from credit cards in the database. However, nobody can guarantee that some technology will not emerge that in the wrong hands, can break even the hardest to crack algorithms.  

As such, you agree to hold harmless BizAutomation.com, its licensees, agents, employees, officers, directors, distributors and retailers against any claims, demands, costs, and damages, including attorney's fees, due to any breach to the database that may result in the acquisition of credit card values.
 " Height="258px">
                            </asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <asp:Button ID="btnAccept" runat="server" Text="I accept" CssClass="button" OnClientClick="javascript:return accept1();" />
                            <asp:Button ID="btnDisAgree" runat="server" Text="I decline" CssClass="button" OnClientClick="javascript:return decline();" />
                        </td>
                    </tr>
                </table>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
</asp:Content>
