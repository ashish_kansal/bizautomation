<%@ Register TagPrefix="ie" Namespace="Microsoft.Web.UI.WebControls" Assembly="Microsoft.Web.UI.WebControls" %>
<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" Codebehind="frmDuplicateRecordSearch.aspx.vb" Inherits="BACRM.UserInterface.Admin.frmDuplicateRecordsSearch" ValidateRequest="false" %>
<%@ Register TagPrefix="menu1"  TagName="webmenu" src="../include/webmenu.ascx" %>
<%@ Register TagPrefix="BizCalendar" TagName="Calendar" src="../include/calandar.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
	<head     runat="server">		
        <link rel="stylesheet" href="~/CSS/master.css" type="text/css" />
		<title>Duplicate Records Search And Association Management</title>
    <link rel="stylesheet" type="text/css" href="../CSS/jscal2.css" />
    <link rel="stylesheet" type="text/css" href="../CSS/border-radius.css" />

    <script type="text/javascript" src="../JavaScript/jscal2.js"></script>

    <script type="text/javascript" src="../JavaScript/en.js"></script>

		<SCRIPT language="JavaScript" src="../javascript/DuplicateRecordsSearchScripts.js" type="text/javascript"></SCRIPT>
	</HEAD>
	<body >
		
		<form id="frmDuplicateRecordsSearch" method="post" runat="server">
		 <menu1:webmenu id="webmenu1" runat="server"></menu1:webmenu>
    
			<table cellSpacing="0" cellPadding="0" width="100%">
				<tr>
					<td vAlign="bottom" width="302" style="WIDTH: 302px">
					<table class="TabStyle">
							<tr>
								<td>&nbsp;&nbsp;&nbsp;Associations Mgt. &amp; 
									Duplicate Search Settings&nbsp;&nbsp;&nbsp;
								</td>
							</tr>
						</table>
					</td>
					<td class="normal1" align="right" height="23">&nbsp;&nbsp;&nbsp;
						<asp:button id="btnDupSearch" Text="Search Duplicates" Runat="server" cssclass="button"></asp:button>&nbsp;&nbsp;
						<input type="button" id="btnBack" Value="Back" class="button" onclick="javascript:ReturnToAdminPanel();">&nbsp;
					</td>
				</tr>
			</table>
			<asp:table id="tblSearchExtraHeader" Runat="server" Width="100%" CellSpacing="0" CellPadding="0"
				BorderWidth="1" BorderColor="black" GridLines="None" BackColor="#DBE3F2">
				<asp:TableRow Height="24">
					<asp:TableCell CssClass="normal1" style="padding-top:3px;padding-left:15px;padding-bottom:3px;">
						<fieldset style="width:165px;">
							<legend>
								&nbsp;Duplicate Search for</legend>
							<asp:RadioButtonList ID="rbSearchFor" Runat="server" RepeatDirection="Horizontal" AutoPostBack="False"
								CssClass="normal1" TextAlign="Right">
								<asp:ListItem Value="Contacts" Selected="True">Contacts</asp:ListItem>
								<asp:ListItem Value="Organizations">Organizations</asp:ListItem>
							</asp:RadioButtonList>
						</fieldset>
					</asp:TableCell>
					<asp:TableCell CssClass="normal1" style="padding-left:3px;padding-bottom:3px;">
						<fieldset style="width:390px;">
							<legend>
								&nbsp;
								<asp:CheckBox ID="cbCheckAll" Runat="server" AutoPostBack="False" Text="Search All" Checked="True"
									TextAlign="Left"></asp:CheckBox></legend>
							&nbsp;
							<asp:CheckBox ID="cbSearchInLeads" Runat="server" AutoPostBack="False" Text="Leads" Checked="True"
								TextAlign="Right"></asp:CheckBox>
							&nbsp;&nbsp;
							<asp:CheckBox ID="cbSearchInProspects" Runat="server" AutoPostBack="False" Text="Prospects" Checked="True"
								TextAlign="Right"></asp:CheckBox>
							&nbsp;&nbsp;
							<asp:CheckBox ID="cbSearchInAccounts" Runat="server" AutoPostBack="False" Text="Accounts" Checked="True"
								TextAlign="Right"></asp:CheckBox>
							&nbsp;&nbsp;Relationship
							<asp:DropDownList ID="ddlRelationship" Runat="server" Width="105px" AutoPostBack="False" CssClass="signup"></asp:DropDownList>
						</fieldset>
					</asp:TableCell>
					<asp:TableCell CssClass="normal1" style="padding-bottom:3px;padding-right:5px;">
						<fieldset style="width:195px;">
							<legend>
								&nbsp;
								<asp:CheckBox ID="cbCreatedOn" Runat="server" AutoPostBack="False" Text="Created On" TextAlign="Left"></asp:CheckBox></legend>
							&nbsp;&nbsp;&nbsp;
						<BizCalendar:Calendar id="calFrom" runat="server" />&nbsp;
							<BizCalendar:Calendar ID="calTo" runat="server" />
						</fieldset>
					</asp:TableCell>
				</asp:TableRow>
			</asp:table><asp:table id="tblCheckSettings" style="BORDER-TOP: 0px" Runat="server" Width="100%" BorderWidth="1" CssClass="aspTable"
				BorderColor="black" GridLines="None">
				<asp:TableRow ID="trDuplicateContactsSettings" Runat="Server" style="display:inline;">
					<asp:TableCell CssClass="normal1">
						<strong>Show duplicate contacts where first and last name are the same, and:</strong><br>
						1. 
<asp:CheckBox ID="cbCntNoItemHistory" Runat="server" AutoPostBack="False" Text="No saved action item history exists for the contact record."
							TextAlign="Right"></asp:CheckBox><br>
						2. 
<asp:CheckBox ID="cbCntSameParent" Runat="server" AutoPostBack="False" Text="The parent organization, and the email address (which can be empty) is the same as another contact but the contact date created is newer."
							TextAlign="Right"></asp:CheckBox><br>
						3. 
<asp:CheckBox ID="cbCntSameDivision" Runat="server" AutoPostBack="False" Text="The Division is the same as another contact, but the contact date created is newer."
							TextAlign="Right"></asp:CheckBox><br>
						4. 
<asp:CheckBox ID="cbCntSamePhone" Runat="server" AutoPostBack="False" Text="The phone number is the same as another contact, but the contact date created is newer."
							TextAlign="Right"></asp:CheckBox><br>
						5. 
<asp:CheckBox ID="cbCntIgnoreNames" Runat="server" AutoPostBack="False" Text="Ignore Contacts with "
							TextAlign="Right"></asp:CheckBox> the First Name 
<asp:TextBox Runat="Server" ID="txtCntFirstName" MaxLength="50" CssClass="normal1"></asp:TextBox> and Last Name 
<asp:TextBox Runat="Server" ID="txtCntLastName" MaxLength="50" CssClass="normal1"></asp:TextBox> <br>
						6. 
<asp:CheckBox ID="cbCntSomeOtherFields" Runat="server" AutoPostBack="False" Text="The following field is the same as another contact, but the contact date created is newer "
							TextAlign="Right"></asp:CheckBox> 
<asp:DropDownList Runat="Server" ID="ddlCntSomeOtherFields" CssClass="signup" Width="140px"></asp:DropDownList><br>
					</asp:TableCell>
				</asp:TableRow>
				<asp:TableRow ID="trDuplicateOrgSettings" Runat="Server" style="display:none;">
					<asp:TableCell CssClass="normal1">
						<strong>A duplicate Organization record exists where Organization name is the same, 
							and:</strong><br>
						1. 
<asp:CheckBox ID="cbOrgNoAssociation" Runat="server" AutoPostBack="False" Text="The record is not associated with another record."
							TextAlign="Right"></asp:CheckBox><br>
						2. 
<asp:CheckBox ID="cbOrgSameDivision" Runat="server" AutoPostBack="False" Text="The Division name is the same, but the Organization date created is newer."
							TextAlign="Right"></asp:CheckBox><br>
						3. 
<asp:CheckBox ID="cbOrgSameWebsite" Runat="server" AutoPostBack="False" Text="The web site URL is the same."
							TextAlign="Right"></asp:CheckBox><br>
						4. 
<asp:CheckBox ID="cbOrgSomeOtherFields" Runat="server" AutoPostBack="False" Text="The following field is the same as another Organization, but the Organization date created is newer "
							TextAlign="Right"></asp:CheckBox> 
<asp:DropDownList Runat="Server" ID="ddlOrgSomeOtherFields" CssClass="signup" Width="140px"></asp:DropDownList><br>
					</asp:TableCell>
				</asp:TableRow>
			</asp:table><br>
			<table id="tblDuplicateSearchResults" cellSpacing="0" cellPadding="0" width="100%" border="0"
				runat="server" style="DISPLAY:inline">
				<tr>
					<td vAlign="bottom" width="300">
						<table class="TabStyle" borderColor="black" cellSpacing="0" border="1">
							<tr>
								<td class="Text_bold_White" height="23">&nbsp;&nbsp;&nbsp;Associations Mgt. &amp; 
									Duplicate Search Results&nbsp;&nbsp;&nbsp;
								</td>
							</tr>
						</table>
					</td>
					<td vAlign="top" align="right">
						<table >
							<tr>
								<td><asp:label id="lblNext" runat="server" cssclass="Text_bold">Next:</asp:label></td>
								<td class="normal1"><asp:linkbutton id="lnk2" runat="server" CausesValidation="False">2</asp:linkbutton></td>
								<td class="normal1"><asp:linkbutton id="lnk3" runat="server" CausesValidation="False">3</asp:linkbutton></td>
								<td class="normal1"><asp:linkbutton id="lnk4" runat="server" CausesValidation="False">4</asp:linkbutton></td>
								<td class="normal1"><asp:linkbutton id="lnk5" runat="server" CausesValidation="False">5</asp:linkbutton></td>
								<td><asp:linkbutton id="lnkFirst" runat="server" CausesValidation="False">
										<div class="LinkArrow"><<</div>
									</asp:linkbutton></td>
								<td><asp:linkbutton id="lnkPrevious" runat="server" CausesValidation="False">
										<div class="LinkArrow"><</div>
									</asp:linkbutton></td>
								<td class="normal1"><asp:label id="lblPage" runat="server">Page</asp:label></td>
								<td><asp:textbox id="txtCurrentPage" runat="server" Text="1" Width="18px" MaxLength="5" AutoPostBack="True"
										CssClass="signup"></asp:textbox></td>
								<td class="normal1">&nbsp;
									<asp:label id="lblOf" runat="server">of</asp:label></td>
								<td class="normal1"><asp:label id="lblTotal" runat="server"></asp:label></td>
								<td><asp:linkbutton id="lnkNext" runat="server" CausesValidation="False" CssClass="LinkArrow">
										<div class="LinkArrow">></div>
									</asp:linkbutton></td>
								<td><asp:linkbutton id="lnkLast" runat="server" CausesValidation="False">
										<div class="LinkArrow">>></div>
									</asp:linkbutton></td>
							</tr>
						</table>
					</td>
					<td class="normal1" align="right" height="23">&nbsp;&nbsp;&nbsp;
						<asp:button id="btnAssociateWithAnotherParent" Text="Associate Division(s) to another Parent"
							Runat="server" cssclass="button" Width="205" CausesValidation="False"></asp:button>&nbsp;
						<asp:button id="btnMergeCopyContacts" Text="Merge/ Copy Contact into another Division" Runat="server"
							cssclass="button" Width="215" CausesValidation="False"></asp:button>&nbsp;
					</td>
				</tr>
			</table>
			<asp:table id="tblAssocationToHeader" Width="100%" Runat="server" BorderColor="black" GridLines="None" CssClass="aspTable"
				BorderWidth="1" CellPadding="2" CellSpacing="0" Height="400">
				<asp:tablerow>
				<asp:TableCell VerticalAlign="Top" >
				<br />
					<table width="100%">
			        <tr>
				<asp:datagrid id="dgDuplicateSearchResults" runat="Server" Width="100%" CellPadding="0" CssClass="dg"
				AllowPaging="False" AllowSorting="True">
				<AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
				<ItemStyle CssClass="is"></ItemStyle>
				<HeaderStyle CssClass="hs" Height="23"></HeaderStyle>
			</asp:datagrid><asp:literal id="litClientMessage" Runat="server" EnableViewState="False"></asp:literal><asp:textbox id="txtTotalPage" style="DISPLAY: none" Runat="server"></asp:textbox><asp:textbox id="txtColumnSorted" Runat="server" Visible="False"></asp:textbox><asp:textbox id="txtSortOrder" Runat="server" Visible="False"></asp:textbox>
			<asp:textbox id="txtRecordsOnPage" style="DISPLAY: none" Runat="server"></asp:textbox>
			<iframe id="IfrOpenOrgContact" src="../Marketing/frmOrgContactRedirect.aspx" frameBorder="0"
				width="10" scrolling="no" height="10" left="0" right="0"></iframe>
			<asp:validationsummary id="ValidationSummary" runat="server" HeaderText="Please check the following value(s)"
				ShowSummary="False" ShowMessageBox="True" DisplayMode="List"></asp:validationsummary><asp:customvalidator id="custVCRMTypeConditionCheck" runat="server" EnableClientScript="True" Display="None"
				ErrorMessage="Please specify whether to search in Leads and/ or Prospects and/or Accounts." ClientValidationFunction="validateSearchCriteria4CRMType"></asp:customvalidator><asp:customvalidator id="custVDateRangeConditionCheck" runat="server" EnableClientScript="True" Display="None"
				ErrorMessage="Please specify the date/ range of date filtering the records." ClientValidationFunction="validateSearchCriteria4DateRange"></asp:customvalidator>
			<asp:customvalidator id="custVldFirstAndLastName" runat="server" EnableClientScript="True" Display="None"
				ErrorMessage="Please enter the First/ Last Name of the Contact." ClientValidationFunction="validateFirstAndLastName"></asp:customvalidator>
				</asp:TableCell>
				</asp:tablerow>
			</asp:table>	
				
		</form>
	</body>
</HTML>
