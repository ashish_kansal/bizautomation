<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" Codebehind="frmSubStageList.aspx.vb" Inherits="BACRM.UserInterface.Admin.frmSubStageList" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
	<head  runat="server">
        <link rel="stylesheet" href="~/CSS/master.css" type="text/css" />
		<title>Sub Stage</title>
		<script language="javascript">
			function Close()
			{
				window.close();
				return false;
			}
		</script>
	</HEAD>
	<body >
		<form id="Form1" method="post" runat="server">
			<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td colspan="3"><br>
					</td>
				</tr>
				<tr>
					<td valign="bottom" width="100">
						<table class="TabStyle">
							<tr>
								<td>&nbsp;&nbsp;&nbsp;Sub 
									Stage(s)&nbsp;&nbsp;&nbsp;
								</td>
							</tr>
						</table>
					</td>
					<td align="left" class=normal1><asp:hyperlink id="hplNew" Runat="server">New Sub Stage</asp:hyperlink></td>
					<td align="right"><asp:Button ID="btnClose" Runat="server" CssClass="button" Text="Close"></asp:Button></td>
				</tr>
				<tr>
					<td colspan="3">
						<asp:table id="Table1" Width="100%" Runat="server" Height="350" GridLines="None" BorderColor="black" CssClass="aspTable" 
							BorderWidth="1" CellSpacing="0" CellPadding="0">
							<asp:TableRow>
								<asp:TableCell VerticalAlign="Top">
									<asp:datagrid id="dgContact" AllowSorting="True" runat="server" Width="100%" CssClass="dg" AutoGenerateColumns="False"
										>
										<AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
										<ItemStyle CssClass="is"></ItemStyle>
										<HeaderStyle CssClass="hs"></HeaderStyle>
										<Columns>
											<asp:ButtonColumn HeaderText="Sub Stage(s)" DataTextField="SubStage" CommandName="SubStage"></asp:ButtonColumn>
											<asp:TemplateColumn>
												<HeaderTemplate>
													<asp:Button ID="btnHdelete" Runat="server" CssClass="button Delete" Text="X" ></asp:Button>
												</HeaderTemplate>
												<ItemTemplate>
													<asp:TextBox ID="txtSubStageID" Runat="server" style="display:none" Text='<%# DataBinder.Eval(Container.DataItem, "numSubStageHDRId") %>'>
													</asp:TextBox>
													<asp:TextBox ID="txtSubStage" Runat="server" style="display:none" Text='<%# DataBinder.Eval(Container.DataItem, "SubStage") %>'>
													</asp:TextBox>
													<asp:Button ID="btnDelete" Runat="server" CssClass="button Delete" Text="X" CommandName="Delete"></asp:Button>
													<asp:LinkButton ID="lnkdelete" Runat="server" Visible="false">
														<font color="#730000">*</font></asp:LinkButton>
												</ItemTemplate>
											</asp:TemplateColumn>
										</Columns>
									</asp:datagrid>
								</asp:TableCell>
							</asp:TableRow>
						</asp:table>
					</td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
