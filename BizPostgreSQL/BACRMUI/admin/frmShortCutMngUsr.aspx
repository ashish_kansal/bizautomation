<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmShortCutMngUsr.aspx.vb"
    Inherits=".frmShortCutMngUsr" MasterPageFile="~/common/Popup.Master" Title="Manage ShortCut Bar"
    ClientIDMode="Static" %>

<asp:Content ID="Content5" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <script language="javascript">
        function MoveUp(tbox) {
            for (var i = 1; i < tbox.options.length; i++) {
                if (tbox.options[i].selected && tbox.options[i].value != "") {
                    var SelectedText, SelectedValue, PrevSelectedValue;
                    SelectedValue = tbox.options[i].value;
                    SelectedText = tbox.options[i].text;
                    PrevSelectedValue = tbox.options[i - 1].value;
                    tbox.options[i].value = tbox.options[i - 1].value;
                    tbox.options[i].text = tbox.options[i - 1].text;
                    tbox.options[i - 1].value = SelectedValue;
                    tbox.options[i - 1].text = SelectedText;
                    tbox.options[i - 1].selected = true;
                }
            }
            return false;
        }

        function MoveDown(tbox) {
            for (var i = 0; i < tbox.options.length - 1; i++) {
                if (tbox.options[i].selected && tbox.options[i].value != "") {
                    var SelectedText, SelectedValue, NextSelectedValue;
                    SelectedValue = tbox.options[i].value;
                    SelectedText = tbox.options[i].text;
                    NextSelectedValue = tbox.options[i + 1].value;
                    tbox.options[i].value = tbox.options[i + 1].value;
                    tbox.options[i].text = tbox.options[i + 1].text;
                    tbox.options[i + 1].value = SelectedValue;
                    tbox.options[i + 1].text = SelectedText;
                    tbox.options[i + 1].selected = true;
                    break;
                }
            }
            return false;
        }

        sortitems = 0;  // 0-False , 1-True
        function move(fbox, tbox) {

            for (var i = 0; i < fbox.options.length; i++) {
                if (fbox.options[i].selected && fbox.options[i].value != "") {
                    /// to check for duplicates 
                    for (var j = 0; j < tbox.options.length; j++) {
                        if (tbox.options[j].value == fbox.options[i].value) {
                            alert("Item is already selected");
                            return false;
                        }
                    }

                    var no = new Option();
                    no.value = fbox.options[i].value;
                    no.text = fbox.options[i].text;
                    tbox.options[tbox.options.length] = no;
                    fbox.options[i].value = "";
                    fbox.options[i].text = "";

                }
            }
            BumpUp(fbox);
            if (sortitems) SortD(tbox);
            return false;
        }

        function remove1(fbox, tbox) {

            for (var i = 0; i < fbox.options.length; i++) {
                if (fbox.options[i].selected && fbox.options[i].value != "") {
                    /// to check for duplicates 
                    for (var j = 0; j < tbox.options.length; j++) {
                        if (tbox.options[j].value == fbox.options[i].value) {
                            fbox.options[i].value = "";
                            fbox.options[i].text = "";
                            BumpUp(fbox);
                            if (sortitems) SortD(tbox);
                            return false;


                            //alert("Item is already selected");
                            //return false;
                        }
                    }

                    var no = new Option();
                    no.value = fbox.options[i].value;
                    no.text = fbox.options[i].text.replace('(*)', '').replace('(#)', '');
                    tbox.options[tbox.options.length] = no;
                    fbox.options[i].value = "";
                    fbox.options[i].text = "";

                }
            }
            BumpUp(fbox);
            if (sortitems) SortD(tbox);
            return false;
        }

        function BumpUp(box) {
            for (var i = 0; i < box.options.length; i++) {
                if (box.options[i].value == "") {
                    for (var j = i; j < box.options.length - 1; j++) {
                        box.options[j].value = box.options[j + 1].value;
                        box.options[j].text = box.options[j + 1].text;
                    }
                    var ln = i;
                    break;
                }
            }
            if (ln < box.options.length) {
                box.options.length -= 1;
                BumpUp(box);
            }
        }


        function SortD(box) {
            var temp_opts = new Array();
            var temp = new Object();
            for (var i = 0; i < box.options.length; i++) {
                temp_opts[i] = box.options[i];
            }
            for (var x = 0; x < temp_opts.length - 1; x++) {
                for (var y = (x + 1); y < temp_opts.length; y++) {
                    if (temp_opts[x].text > temp_opts[y].text) {
                        temp = temp_opts[x].text;
                        temp_opts[x].text = temp_opts[y].text;
                        temp_opts[y].text = temp;
                        temp = temp_opts[x].value;
                        temp_opts[x].value = temp_opts[y].value;
                        temp_opts[y].value = temp;
                    }
                }
            }
            for (var i = 0; i < box.options.length; i++) {
                box.options[i].value = temp_opts[i].value;
                box.options[i].text = temp_opts[i].text;
            }
        }

        function Save() {

            var strFav = '';
            var strFavouriteLink = '';
            document.getElementById("txtInitialPage").value = 0;

            for (var i = 0; i < document.getElementById("lstAddfldFav").options.length; i++) {
                var SelectedValueFav;
                SelectedValueFav = document.getElementById("lstAddfldFav").options[i].value;
                strFav = strFav + SelectedValueFav + ',';

                if (document.getElementById("lstAddfldFav").options[i].text.indexOf('(*)') != -1) {
                    document.getElementById("txtInitialPage").value = SelectedValueFav;
                }

                if (document.getElementById("lstAddfldFav").options[i].text.indexOf('(#)') != -1) {
                    strFavouriteLink = strFavouriteLink + SelectedValueFav + ',';
                }
                else {
                    strFavouriteLink = strFavouriteLink + '0,';
                }
            }

            document.getElementById("txthiddenFav").value = strFav;
            document.getElementById("txtFavouriteLink").value = strFavouriteLink;
        }

        function Check() {

        }

        function SetInitialPage(tbox) {
            var SelectedText, SelectedValue;

            for (var i = 0; i < tbox.options.length; i++) {
                if (tbox.options[i].selected && tbox.options[i].value != "") {
                    SelectedText = tbox.options[i].text;
                    SelectedValue = tbox.options[i].value;

                    if (SelectedText.indexOf('(*)') != -1)
                        SelectedText = SelectedText.replace('(*)', '');
                    else
                        SelectedText = '(*) ' + SelectedText;

                    tbox.options[i].text = SelectedText;
                }
                else {
                    if (tbox.options[i].text.indexOf('(*)') != -1) {
                        SelectedText = tbox.options[i].text;
                        SelectedValue = tbox.options[i].value;
                        SelectedText = SelectedText.replace('(*)', '');
                        tbox.options[i].text = SelectedText;
                    }
                }
            }

            return false;
        }

        function AddToFavourite(tbox) {
            var SelectedText, SelectedValue;

            for (var i = 0; i < tbox.options.length; i++) {
                if (tbox.options[i].selected && tbox.options[i].value != "") {
                    SelectedText = tbox.options[i].text;
                    SelectedValue = tbox.options[i].value;

                    if (SelectedText.indexOf('(#)') != -1)
                        SelectedText = SelectedText.replace('(#)', '');
                    else
                        SelectedText = '(#) ' + SelectedText;

                    tbox.options[i].text = SelectedText;
                }
            }

            return false;
        }
    </script>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <div class="input-part">
        <div class="right-input">
            <asp:Button runat="server" ID="btnSave" Text="Save" CssClass="button" />
            <asp:Button runat="server" ID="btnSaveClose" Text="Save & Close" CssClass="button" />
            <asp:Button runat="server" ID="btnclose" OnClientClick="javascript:self.close()"
                Text="Close" CssClass="button" />&nbsp;&nbsp;
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="PageTitle" runat="server" ClientIDMode="Static">
    Manage ShortCut Bar
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="Content" runat="server" ClientIDMode="Static">
    <asp:Table ID="Table2" runat="server" GridLines="None" BorderColor="black" Width="600px"
        BorderWidth="1" CssClass="aspTable" CellSpacing="0" CellPadding="0">
        <asp:TableRow>
            <asp:TableCell HorizontalAlign="center">
                <table width="100%">
                    <tr>
                        <td colspan="3" class="normal1">
                            Tab&nbsp;
                            <asp:DropDownList ID="ddlTab" Width="200" AutoPostBack="true" runat="server" CssClass="signup">
                            </asp:DropDownList>
                        </td>
                        <td colspan="3" class="normal1">
                            Tab Accessibility :
                            <asp:RadioButtonList ID="rblTabEvent" runat="server" CssClass="signup" RepeatDirection="Horizontal">
                                <asp:ListItem Value="0">Mouse over</asp:ListItem>
                                <asp:ListItem Value="1">Single click</asp:ListItem>
                            </asp:RadioButtonList>
                        </td>
                    </tr>
                </table>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell VerticalAlign="Top">
                <br />
                <table cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td align="center" class="normal1">
                            Available Fields<br>
                            <asp:ListBox ID="lstAvailablefldFav" runat="server" Width="200" Height="200" CssClass="signup">
                            </asp:ListBox>
                        </td>
                        <td align="center" valign="middle">
                            <asp:Button ID="btnAddFav" CssClass="button" runat="server" Text="Add >"></asp:Button>
                            <br>
                            <br>
                            <asp:Button ID="btnRemoveFav" CssClass="button" runat="server" Text="< Remove"></asp:Button>
                        </td>
                        <td align="center" class="normal1">
                            On Display<br>
                            <asp:ListBox ID="lstAddfldFav" runat="server" Width="200" Height="200" CssClass="signup">
                            </asp:ListBox>
                        </td>
                        <td align="center" valign="middle">
                            <asp:Button ID="btnMoveupFav" CssClass="button" runat="server" Text="Up"></asp:Button>
                            <br>
                            <br>
                            <asp:Button ID="btnMoveDownFav" CssClass="button" runat="server" Text="Down"></asp:Button>
                            <br />
                            <br />
                            <input type="button" id="btnSetInitialPage" runat="server" class="button" value="Set Initial Page"
                                onclick="javascript:SetInitialPage(lstAddfldFav)">
                            <br />
                            <br />
                            <input type="button" id="btnAddFavourite" runat="server" class="button" value="Set Favourite"
                                onclick="javascript:AddToFavourite(lstAddfldFav)">
                        </td>
                    </tr>
                </table>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow Width="100%">
            <asp:TableCell HorizontalAlign="Center">
                <table width="100%">
                    <tr class="normal7">
                        <td>
                            Add Custom Links
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td class="normal1" colspan="2">
                            Name
                            <asp:TextBox runat="server" ID="txtName" CssClass="signup" Width="300px"></asp:TextBox>
                            &nbsp;
                            <asp:Button ID="btnAdd" runat="server" Text="Add" CssClass="button" />
                        </td>
                    </tr>
                    <tr>
                        <td class="normal1" colspan="2">
                            Url &nbsp;&nbsp;&nbsp;<asp:TextBox runat="server" ID="txtUrl" CssClass="signup" Width="300px"></asp:TextBox>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        </td>
                    </tr>
                </table>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell Width="100%">
                <asp:DataGrid ID="dgCustomFields" runat="server" CssClass="dg" Width="100%" AutoGenerateColumns="False">
                    <AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
                    <ItemStyle CssClass="is"></ItemStyle>
                    <HeaderStyle CssClass="hs"></HeaderStyle>
                    <Columns>
                        <asp:TemplateColumn ItemStyle-Wrap="False" ItemStyle-VerticalAlign="Bottom">
                            <ItemTemplate>
                                <asp:LinkButton runat="server" Text="Edit" ID="lnkbtnEdt" CommandName="Edit"></asp:LinkButton>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:LinkButton ID="lnkbtnUpdt" runat="server" Text="Update" CommandName="Update"></asp:LinkButton>&nbsp;
                                <asp:LinkButton runat="server" Text="Cancel" CommandName="Cancel" ID="lnkbtnCncl"></asp:LinkButton>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="No" HeaderStyle-Width="10" ItemStyle-Width="40">
                            <ItemTemplate>
                                <asp:Label ID="lblListItemID" runat="server" Style="display: none" Text='<%# DataBinder.Eval(Container,"DataItem.Id") %>'></asp:Label>
                                <%# Container.ItemIndex +1 %>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Name">
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lblName" CssClass="normal1" Text='<%# DataBinder.Eval(Container,"DataItem.vcLinkName") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox runat="server" ID="txtDgName" CssClass="signup" Text='<%# DataBinder.Eval(Container,"DataItem.vcLinkName") %>'></asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Link">
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lblUrl" CssClass="normal1" Text='<%# DataBinder.Eval(Container,"DataItem.Link") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox runat="server" ID="txtDgUrl" Width="350px" CssClass="signup" Text='<%# DataBinder.Eval(Container,"DataItem.Link") %>'></asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderStyle-Width="10" ItemStyle-Width="10">
                            <ItemTemplate>
                                <asp:Button ID="btnDelete" runat="server" CssClass="button Delete" Text="X" CommandName="Delete">
                                </asp:Button>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                </asp:DataGrid>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
    <asp:TextBox Style="display: none" ID="txthiddenFav" runat="server"></asp:TextBox>
    <asp:TextBox Style="display: none" ID="txtFavouriteLink" runat="server"></asp:TextBox>
    <asp:TextBox Style="display: none" ID="txtInitialPage" runat="server"></asp:TextBox>
</asp:Content>
