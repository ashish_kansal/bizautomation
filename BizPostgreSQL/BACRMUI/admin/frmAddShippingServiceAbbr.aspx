﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/common/PopupBootstrap.Master"
    CodeBehind="frmAddShippingServiceAbbr.aspx.vb" Inherits="BACRM.UserInterface.Admin.frmAddShippingServiceAbbr" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Shipping Service Abbr</title>
    <script type="text/javascript">
        function Close() {
            window.close()
            return false;
        }
 
        function DeleteRecord() {
            if (confirm('Selectd shipping service may be used in Organization(s) or order(s). Are you sure, you want to delete the selected record?')) {
                return true;
            }
            else {
                return false;
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server">
    <div class="row">
        <div class="col-xs-12">
            <div class="pull-right">
             <asp:Button ID="btnClose" Text="Close" CssClass="btn btn-primary" runat="server"></asp:Button>
        </div>
        </div>
        
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageTitle" runat="server" ClientIDMode="Static">
    Shipping Service Abbreviation
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="Content" runat="server" ClientIDMode="Static">
    <div class="row padbottom10" id="divError" runat="server" style="display: none">
        <div class="col-sm-12">
            <div class="alert alert-danger">
                <h4><i class="icon fa fa-ban"></i>Error</h4>
                <p>
                    <asp:Label ID="lblError" runat="server"></asp:Label>
                </p>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12">
            
            
            
        </div>
    </div>
    <div class="row padbottom10">
        <div class="col-xs-12 col-md-3">
            <div class="form-group">
                <label>Ship Via</label>
                <asp:DropDownList ID="ddlShipVia" AutoPostBack="True" OnSelectedIndexChanged="ddlShipVia_SelectedIndexChanged" runat="server" CssClass="form-control">
                </asp:DropDownList>
            </div>
        </div>
        <div class="col-xs-12 col-md-4">
            <div class="form-group">
                <label>Shipping Service</label>
                <asp:TextBox ID="txtShippingService" runat="server" CssClass="form-control"></asp:TextBox>
            </div>
        </div>
        <div class="col-xs-12 col-md-4">
            <div class="form-group">
                <label>Shipping Service Abbreviation</label>
                <asp:TextBox ID="txtAbbreviations" runat="server" CssClass="form-control"></asp:TextBox>
            </div>
        </div>
        <div class="col-xs-12 col-md-1">
            <div class="form-group">
                <label></label>
                <asp:Button ID="btnAdd" runat="server" OnClick="btnAdd_Click" Text="Add" CssClass="btn btn-primary"></asp:Button>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12">
            <asp:DataGrid ID="dgShippingService" runat="server" CssClass="table table-bordered table-striped" OnItemCommand="dgShippingService_ItemCommand" OnEditCommand="dgShippingService_EditCommand" AutoGenerateColumns="False"
                AllowSorting="True" Width="100%" UseAccessibleHeader="true">
                <Columns>
                    <asp:TemplateColumn>
                        <ItemTemplate>
                            <asp:LinkButton runat="server" Text="Edit" CommandName="Edit" ID="lnkbtnEdt" CssClass="btn btn-info btn-xs" ><i class="fa fa-pencil"></i></asp:LinkButton>
                            <asp:HiddenField runat="server" ID="hdnDomainID" Value='<%# DataBinder.Eval(Container, "DataItem.numDomainID") %>' />
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:LinkButton ID="lnkbtnUpdt" runat="server" Text="Update" CommandName="Update"></asp:LinkButton>&nbsp;
                                <asp:LinkButton runat="server" Text="Cancel" CommandName="Cancel" ID="lnkbtnCncl"></asp:LinkButton>
                        </EditItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="No" Visible="false">
                        <ItemTemplate>
                            <asp:Label ID="lblShipppingServiceID" runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.numShippingServiceID") %>'>
                            </asp:Label>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Shipment Service">
                        <ItemTemplate>
                            <asp:Label ID="lblShippingService" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.vcShipmentService") %>'>
                            </asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtEShippingService" runat="server" CssClass="form-control" Text='<%# DataBinder.Eval(Container, "DataItem.vcShipmentService")%>'>
                            </asp:TextBox>
                        </EditItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Abbreviations">
                        <ItemTemplate>
                            <asp:Label ID="lblAbbreviations" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.vcAbbreviation") %>'>
                            </asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtEAbbreviations" runat="server" CssClass="form-control" Text='<%# DataBinder.Eval(Container, "DataItem.vcAbbreviation") %>'>
                            </asp:TextBox>
                        </EditItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderStyle-Width="25" ItemStyle-Width="25">
                        <ItemTemplate>
                            <asp:Label ID="lblDefault" runat="server" Text="*"></asp:Label>
                            <asp:LinkButton ID="lkbDelete" runat="server" CssClass="btn btn-danger btn-xs" CommandName="Delete" Visible="false" OnClientClick="return DeleteRecord();"><i class="fa fa-trash-o"></i></asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                </Columns>
            </asp:DataGrid>
        </div>
    </div>
</asp:Content>
