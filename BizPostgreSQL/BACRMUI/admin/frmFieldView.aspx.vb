''Created By Anoop Jayaraj
Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Common
Namespace BACRM.UserInterface.Admin

    Public Class frmFieldView
        Inherits BACRMPage

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub
        Protected WithEvents btnSave As System.Web.UI.WebControls.LinkButton
        Protected WithEvents btnSaveAndClose As System.Web.UI.WebControls.Button
        Protected WithEvents btnClose As System.Web.UI.WebControls.Button
        Protected WithEvents ddlRelationship As System.Web.UI.WebControls.DropDownList
        Protected WithEvents ddlLocation As System.Web.UI.WebControls.DropDownList
        Protected WithEvents ddltabs As System.Web.UI.WebControls.DropDownList
        Protected WithEvents lstAvailablefld As System.Web.UI.WebControls.ListBox
        Protected WithEvents btnAdd As System.Web.UI.WebControls.LinkButton

        Protected WithEvents btnRemove As System.Web.UI.WebControls.LinkButton

        Protected WithEvents lstAddfld As System.Web.UI.WebControls.ListBox
        Protected WithEvents btnMoveup As System.Web.UI.WebControls.Button
        Protected WithEvents btnMoveDown As System.Web.UI.WebControls.Button
        Protected WithEvents tblOppr As System.Web.UI.WebControls.Table
        Protected WithEvents txthidden As System.Web.UI.WebControls.TextBox
        Protected WithEvents ddlContact As System.Web.UI.WebControls.DropDownList
        Protected WithEvents tdRelationship As System.Web.UI.HtmlControls.HtmlGenericControl
        Protected WithEvents tdContactType As System.Web.UI.HtmlControls.HtmlGenericControl
        Protected WithEvents btnAddAll As System.Web.UI.WebControls.LinkButton


        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.
        Private designerPlaceholderDeclaration As System.Object

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region
       

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Try
                DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ""
                DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "none")
                If Not IsPostBack Then


                    GetUserRightsForPage(13, 16)
                    If m_aryRightsForPage(RIGHTSTYPE.VIEW) = 0 Then Response.Redirect("../admin/authentication.aspx?mesg=AS")
                    objCommon.sb_FillComboFromDBwithSel(ddlRelationship, 5, Session("DomainID"))

                    ddlRelationship.Items.Insert(1, New ListItem("Leads", 1))
                    ddlRelationship.Items.Insert(2, New ListItem("Accounts", 2))
                    ddlRelationship.Items.Insert(3, New ListItem("Prospects", 3))

                    objCommon.sb_FillComboFromDBwithSel(ddlContact, 8, Session("DomainID"))
                    LoadLocation()
                End If
                btnAdd.Attributes.Add("OnClick", "return move(lstAvailablefld,lstAddfld)")
                btnRemove.Attributes.Add("OnClick", "return remove1(lstAddfld,lstAvailablefld)")
                'btnMoveDown.Attributes.Add("OnClick", "return MoveDown(document.Form1.lstAddfld)")
                'btnMoveup.Attributes.Add("OnClick", "return MoveUp(document.Form1.lstAddfld)")
                btnSave.Attributes.Add("OnClick", "return Save()")
                ' btnSaveAndClose.Attributes.Add("OnClick", "return Save()")
                'btnClose.Attributes.Add("OnClick", "return Close()")
                btnAddAll.Attributes.Add("onClick", "return CheckRelorContactType()")
                If ddlLocation.SelectedItem.Value = 4 Then
                    tdRelationship.Visible = False
                    tdContactType.Visible = True
                Else
                    tdContactType.Visible = False
                    tdRelationship.Visible = True
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub
        Private Sub DisplayError(ByVal ex As String)
            DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ex
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
        End Sub
        Sub LoadLocation()
            Try
                Dim dtRelationship As DataTable
                Dim ObjCusFlds As New CustomFields
                dtRelationship = ObjCusFlds.GetRelationship
                ddlLocation.DataSource = dtRelationship
                ddlLocation.DataTextField = "loc_name"
                ddlLocation.DataValueField = "loc_id"
                ddlLocation.DataBind()
                ddlLocation.Items.Insert(0, "--Select One--")
                ddlLocation.Items.FindByText("--Select One--").Value = 0
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub LoadTabs()
            Try
                ddltabs.Items.Clear()
                Dim PageId As Long = ddlLocation.SelectedValue
                'If PageId <> 1 Then 'When its Lead/Prospects/Accounts allow only custom field to be added to main detail section
                Dim dtTab As DataTable
                Dim objCusField As New CustomFields()
                objCusField.locId = PageId
                objCusField.DomainID = Session("DomainID")
                dtTab = objCusField.Tabs
                ddltabs.DataSource = dtTab
                ddltabs.DataTextField = "grp_name"
                ddltabs.DataValueField = "grp_id"
                ddltabs.DataBind()
                'End If

                ddltabs.Items.Insert(0, "--Select One--")
                ddltabs.Items.FindByText("--Select One--").Value = -1
                If Not (PageId = 12 Or PageId = 13 Or PageId = 14) Then 'When its seperate Location like accounts then allow custom fields to be added only to custom subtabs
                    ddltabs.Items.Insert(1, "Add to Main Detail Section")
                    ddltabs.Items.FindByText("Add to Main Detail Section").Value = 0
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub ddlLocation_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlLocation.SelectedIndexChanged
            Try
                LoadTabs()
                FillListbox()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Sub ddlRelationship_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlRelationship.SelectedIndexChanged
            Try
                LoadTabs()
                FillListbox()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Sub FillListbox()
            Try
                If ddltabs.SelectedItem.Value > -2 Then
                    Dim dtFields As DataTable
                    Dim ObjCusFlds As New CustomFields
                    ObjCusFlds.locId = ddlLocation.SelectedItem.Value
                    ObjCusFlds.TabId = ddltabs.SelectedItem.Value
                    ObjCusFlds.DomainID = Session("DomainID")
                    dtFields = ObjCusFlds.GetFieldDetailsByLocAndTabId
                    lstAvailablefld.DataSource = dtFields
                    lstAvailablefld.DataTextField = "fld_label"
                    lstAvailablefld.DataValueField = "fld_id"
                    lstAvailablefld.DataBind()

                    'Dim dtFieldsAdded As New DataTable
                    If ddlLocation.SelectedItem.Value = 4 Then
                        ObjCusFlds.RelId = ddlContact.SelectedItem.Value
                    Else : ObjCusFlds.RelId = ddlRelationship.SelectedItem.Value
                    End If
                    ObjCusFlds.DomainID = Session("DomainID")
                    dtFields = ObjCusFlds.GetAddedFieldDetailsByLocAndTabId
                    lstAddfld.DataSource = dtFields
                    lstAddfld.DataTextField = "fld_label"
                    lstAddfld.DataValueField = "fld_id"
                    lstAddfld.DataBind()
                Else
                    lstAvailablefld.Items.Clear()
                    lstAddfld.Items.Clear()
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub ddltabs_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddltabs.SelectedIndexChanged
            Try
                FillListbox()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
            Try
                save()
                FillListbox()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        'Private Sub btnSaveAndClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSaveAndClose.Click
        '    save()
        '    Response.Write("<script>window.close();</script>")
        'End Sub

        Sub save()
            Try
                Dim ObjCusFlds As New CustomFields
                If ddlLocation.SelectedItem.Value = 4 Then
                    ObjCusFlds.RelId = ddlContact.SelectedItem.Value
                Else : ObjCusFlds.RelId = ddlRelationship.SelectedItem.Value
                End If
                ObjCusFlds.TabId = ddltabs.SelectedItem.Value
                ObjCusFlds.strFldId = txthidden.Text
                ObjCusFlds.DomainID = Session("DomainID")
                ObjCusFlds.SortField()
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub btnAddAll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddAll.Click
            Try
                Dim strCustFldValues() As String
                strCustFldValues = txthidden.Text.Split({","}, StringSplitOptions.RemoveEmptyEntries)
                Dim i, j As Integer
                Dim ObjCusFlds As New CustomFields
                If ddlLocation.SelectedItem.Value = 1 Then
                    For i = 0 To strCustFldValues.Length - 1
                        For j = 1 To ddlRelationship.Items.Count - 1
                            ObjCusFlds.CusFldId = strCustFldValues(i)
                            ObjCusFlds.RelId = ddlRelationship.Items(j).Value
                            ObjCusFlds.AddCustomFldsToAll()
                        Next
                    Next
                ElseIf ddlLocation.SelectedItem.Value = 4 Then
                    For i = 0 To strCustFldValues.Length - 1
                        For j = 1 To ddlContact.Items.Count - 1
                            ObjCusFlds.CusFldId = strCustFldValues(i)
                            ObjCusFlds.RelId = ddlContact.Items(j).Value
                            ObjCusFlds.AddCustomFldsToAll()
                        Next
                    Next
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub
    End Class
End Namespace
