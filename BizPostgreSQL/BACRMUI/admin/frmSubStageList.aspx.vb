'***************************************************************************************************************************
'     Author Name				 :  Anoop Jayaraj
'     Date Written				 :  18/2/2005
'***************************************************************************************************************************

Imports BACRM.BusinessLogic.Opportunities
Imports BACRM.BusinessLogic.Common
Namespace BACRM.UserInterface.Admin

    Public Class frmSubStageList
        Inherits BACRMPage

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Protected WithEvents hplNew As System.Web.UI.WebControls.HyperLink
        Protected WithEvents dgContact As System.Web.UI.WebControls.DataGrid
        Protected WithEvents btnClose As System.Web.UI.WebControls.Button
        Protected WithEvents Table1 As System.Web.UI.WebControls.Table

        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.
        Private designerPlaceholderDeclaration As System.Object

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Try
                hplNew.NavigateUrl = "../admin/frmAdminSubStages.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&ProcessID=" & GetQueryStringVal( "ProcessID")
                btnClose.Attributes.Add("onclick", "return Close();")
                Session("SubStages") = Nothing
                If Not IsPostBack Then
                    
                    Dim dt As DataTable
                    Dim objSubStages As New SubStages
                    objSubStages.ProcessID = CCommon.ToLong(GetQueryStringVal("ProcessID"))
                    dt = objSubStages.GetSubStageBySalesProId
                    Session("dt") = dt
                    dgContact.DataSource = dt
                    dgContact.DataBind()
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub dgContact_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgContact.ItemCommand
            Try
                Dim intSubStage As Integer
                Dim txtStageId As TextBox
                txtStageId = e.Item.FindControl("txtSubStageID")
                Dim stageName As String
                Dim txtStage As TextBox
                txtStage = e.Item.FindControl("txtSubStage")
                stageName = txtStage.Text
                intSubStage = CInt(txtStageId.Text)
                If e.CommandName = "SubStage" Then
                    Response.Redirect("../admin/frmAdminSubStages.aspx?ProcessID=" & GetQueryStringVal( "ProcessID") & "&SubStageID=" & intSubStage & "&SubStage=" & stageName)
                End If
                If e.CommandName = "Delete" Then
                    Dim objSubStages As New SubStages()
                        objSubStages.SubStageID = intSubStage
                        objSubStages.DeleteSubStage()
                    Response.Redirect("../admin/frmSubStageList.aspx?ProcessID=" & GetQueryStringVal( "ProcessID"))
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

    End Class
End Namespace
