﻿Imports System.Collections.Generic
Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Common
Imports Telerik.Web.UI

Namespace BACRM.UserInterface.Admin
    Public Class frmItemsFieldSetting
        Inherits BACRMPage

        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            Try
                If Not IsPostBack Then
                    LoadDomainDetails()
                End If
            Catch ex As Exception

            End Try
        End Sub
        Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
            Try
                Dim objUserAccess As New UserAccess
                objUserAccess.DomainID = Session("DomainID")
                objUserAccess.vcElectiveItemFields = GenerateCheckedItemString(rcbItems)
                objUserAccess.UpdateElectiveItemFields()
                Session("vcElectiveItemFields") = GenerateCheckedItemString(rcbItems)
                LoadDomainDetails()
            Catch ex As Exception

            End Try

        End Sub
        Sub LoadDomainDetails()
            Dim listSelectedUsers As List(Of String)
            listSelectedUsers = CCommon.ToString(Session("vcElectiveItemFields")).Split(",").ToList()

            For Each dr As String In listSelectedUsers
                Dim radComboItem As RadComboBoxItem
                radComboItem = rcbItems.FindItemByValue(dr)
                If radComboItem IsNot Nothing Then
                    radComboItem.Checked = True
                End If
            Next
        End Sub
        Function GenerateCheckedItemString(ByVal radComboBox As RadComboBox) As String
            Dim strTransactionType As String
            For Each item As RadComboBoxItem In radComboBox.CheckedItems
                If CCommon.ToLong(item.Value) > 0 Then
                    strTransactionType = strTransactionType & item.Value & ","
                End If
            Next
            Return strTransactionType
        End Function
    End Class

End Namespace