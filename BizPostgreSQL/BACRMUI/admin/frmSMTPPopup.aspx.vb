﻿Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Contacts
Imports BACRM.BusinessLogic.Admin
Partial Public Class frmSMTPPopup
    Inherits BACRMPage
    Dim objContact As CContacts

    Dim lngUserID As Long
    Dim objUserAccess As UserAccess
    Dim intMode As Int16
    Dim strMailId As String
    Dim strFrom As String
    Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            lngUserID = CCommon.ToLong(GetQueryStringVal("UserId"))  'Take the user id from the querystring
            intMode = CCommon.ToShort(GetQueryStringVal("Mode"))
            If Not Page.IsPostBack Then
                If intMode = 3 Then
                    LoadDomainDetails()
                    tdSender1.Visible = True
                    tdSender2.Visible = True
                ElseIf intMode = 4 Then
                    LoadSMTPInformation()
                ElseIf intMode <> 3 Then
                    LoadInformation()
                End If

            End If
            If intMode = 1 Or intMode = 3 Or intMode = 4 Then
                lblTitle.Text = "SMTP Configuration"
                chkSmtpAuth.Text = "Use Authentication"
            Else
                chkSmtpAuth.Text = "Use User Name"
                lblTitle.Text = "IMAP Configuration"
            End If
            If Not IsPostBack Then
                If (intMode = 1 Or intMode = 2) And lngUserID <> -1 Then
                    txtUserName.Text = Session("UserMailIdTest")
                    'txtUserName.Enabled = False
                End If
            End If
            
            btnClose.Attributes.Add("onclick", "Close();")
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
    Sub LoadDomainDetails()
        Try
            Dim dtTable As DataTable
            Dim objUserAccess As New UserAccess
            objUserAccess.DomainID = Session("DomainID")
            dtTable = objUserAccess.GetDomainDetails()
            Dim strPassword As String

            objCommon = New CCommon
            If dtTable.Rows.Count = 0 Then Exit Sub


            If dtTable.Rows(0).Item("bitPSMTPServer") Then
                If dtTable.Rows(0).Item("bitPSMTPAuth") = True Then
                    chkSmtpAuth.Checked = True
                    strPassword = objCommon.Decrypt(dtTable.Rows(0).Item("vcPSMTPPassword"))
                    txtPassWord.Text = strPassword
                    txtPassWord.Attributes("Value") = strPassword
                End If
                txtServerUrl.Text = dtTable.Rows(0).Item("vcPSMTPServer")
                chkSSL.Checked = dtTable.Rows(0).Item("bitPSMTPSSL")
                txtServerPort.Text = IIf(dtTable.Rows(0).Item("numPSMTPPort") = 0, "", dtTable.Rows(0).Item("numPSMTPPort"))
                chkSmtpAuth.Checked = True
                txtUserName.Text = dtTable.Rows(0).Item("vcPSMTPUserName")
                txtSenderName.Text = dtTable.Rows(0).Item("vcPSMTPDisplayName")
            End If

        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
    Sub LoadInformation()
        Try
            Dim dtUserAccessDetails As DataTable
            objUserAccess = New UserAccess
            objUserAccess.UserId = lngUserID
            objUserAccess.DomainID = Session("DomainID")
            dtUserAccessDetails = objUserAccess.GetUserAccessDetails

            If dtUserAccessDetails.Rows.Count > 0 Then
                If Not String.IsNullOrEmpty(CCommon.ToString(dtUserAccessDetails.Rows(0).Item("vcEmailAlias"))) Then
                    Session("UserMailIdTest") = CCommon.ToString(dtUserAccessDetails.Rows(0).Item("vcEmailAlias"))
                Else
                    Session("UserMailIdTest") = IIf(IsDBNull(dtUserAccessDetails.Rows(0).Item("vcEmailId")), "", dtUserAccessDetails.Rows(0).Item("vcEmailId"))
                End If


                Dim strPassword As String

                If intMode = 1 Then

                    If dtUserAccessDetails.Rows(0).Item("bitSMTPAuth") = True Then
                        chkSmtpAuth.Checked = True
                    End If
                    If CCommon.ToString(dtUserAccessDetails.Rows(0).Item("vcSMTPPassword")).Length > 1 Then
                        strPassword = objCommon.Decrypt(dtUserAccessDetails.Rows(0).Item("vcSMTPPassword").ToString())
                        txtPassWord.Attributes("Value") = strPassword
                    End If

                    txtServerUrl.Text = CCommon.ToString(dtUserAccessDetails.Rows(0).Item("vcSMTPServer"))
                    chkSSL.Checked = CCommon.ToBool(dtUserAccessDetails.Rows(0).Item("bitSMTPSSL"))
                    txtServerPort.Text = CCommon.ToString(dtUserAccessDetails.Rows(0).Item("numSMTPPort"))

                End If

                If intMode = 2 Then

                    If lngUserID = -1 Then 'For Support Email
                        txtUserName.Text = dtUserAccessDetails.Rows(0).Item("vcImapUserName")
                    End If
                    chkSmtpAuth.Checked = dtUserAccessDetails.Rows(0).Item("bitUseUserName")
                    txtServerPort.Text = IIf(IsDBNull(dtUserAccessDetails.Rows(0).Item("numPort")), "", dtUserAccessDetails.Rows(0).Item("numPort"))
                    txtServerUrl.Text = IIf(IsDBNull(dtUserAccessDetails.Rows(0).Item("vcImapServerUrl")), "", dtUserAccessDetails.Rows(0).Item("vcImapServerUrl"))
                    chkSSL.Checked = IIf(IsDBNull(dtUserAccessDetails.Rows(0).Item("bitSSl")), False, dtUserAccessDetails.Rows(0).Item("bitSSl"))
                    If IIf(IsDBNull(dtUserAccessDetails.Rows(0).Item("vcImapPassword")), "", dtUserAccessDetails.Rows(0).Item("vcImapPassword")) <> "" Then
                        strPassword = objCommon.Decrypt(dtUserAccessDetails.Rows(0).Item("vcImapPassword"))
                        txtPassWord.Attributes("Value") = strPassword
                    End If
                End If
            End If


        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Sub LoadSMTPInformation()
        Try
            Dim dtUserAccessDetails As DataTable
            objUserAccess = New UserAccess
            objUserAccess.UserId = lngUserID
            objUserAccess.DomainID = Session("DomainID")
            dtUserAccessDetails = objUserAccess.GetUserSMTPDetails

            If dtUserAccessDetails.Rows.Count > 0 Then
                Dim strPassword As String

                txtUserName.Text = dtUserAccessDetails.Rows(0).Item("vcSMTPUserName")

                If dtUserAccessDetails.Rows(0).Item("bitSMTPAuth") = True Then
                    chkSmtpAuth.Checked = True
                End If
                If CCommon.ToString(dtUserAccessDetails.Rows(0).Item("vcSMTPPassword")).Length > 1 Then
                    strPassword = objCommon.Decrypt(dtUserAccessDetails.Rows(0).Item("vcSMTPPassword").ToString())
                    txtPassWord.Attributes("Value") = strPassword
                End If

                txtServerUrl.Text = CCommon.ToString(dtUserAccessDetails.Rows(0).Item("vcSMTPServer"))
                chkSSL.Checked = CCommon.ToBool(dtUserAccessDetails.Rows(0).Item("bitSMTPSSL"))
                txtServerPort.Text = CCommon.ToString(dtUserAccessDetails.Rows(0).Item("numSMTPPort"))
            End If


        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
    Private Sub save()
        Try
            Dim plainText As String
            Dim cipherText As String
            Dim objUserAccess As New UserAccess
            objUserAccess.UserId = lngUserID
            objUserAccess.DomainID = Session("DomainID")
            If intMode = 1 Then
                plainText = txtPassWord.Text
                cipherText = objCommon.Encrypt(plainText)
                objUserAccess.SMTPPassWord = cipherText
                objUserAccess.SMTPPort = IIf(IsNumeric(txtServerPort.Text), txtServerPort.Text, 0)
                objUserAccess.SMTPServer = txtServerUrl.Text
                objUserAccess.SMTPAuth = chkSmtpAuth.Checked
                objUserAccess.SMTPSSL = chkSSL.Checked
                Session("SMTPServer") = txtServerUrl.Text
                Session("SMTPPort") = IIf(IsNumeric(txtServerPort.Text), txtServerPort.Text, 0)
                Session("SMTPAuth") = chkSmtpAuth.Checked
                Session("SMTPPassword") = cipherText
                Session("bitSMTPSSL") = chkSSL.Checked
                Session("SMTPServerIntegration") = True
                objUserAccess.bitSMTPServer = True
                objUserAccess.UpdateUserSMTP()
            ElseIf intMode = 2 Then
                objUserAccess.ContactID = lngUserID
                objUserAccess.ImapIntegration = True
                objUserAccess.ImapServerUrl = txtServerUrl.Text
                objUserAccess.ImapSSL = chkSSL.Checked
                objUserAccess.ImapSSLPort = IIf(txtServerPort.Text = "", 993, txtServerPort.Text)
                plainText = txtPassWord.Text
                cipherText = objCommon.Encrypt(plainText)
                objUserAccess.ImapPassWord = cipherText
                objUserAccess.boolUseUserName = chkSmtpAuth.Checked
                objUserAccess.UserName = If(lngUserID = -1, txtUserName.Text.Trim(), "")
                objUserAccess.ManageImapDtls()
            ElseIf intMode = 4 Then
                objUserAccess.UserName = txtUserName.Text.Trim()
                plainText = txtPassWord.Text
                cipherText = objCommon.Encrypt(plainText)
                objUserAccess.SMTPPassWord = cipherText
                objUserAccess.SMTPServer = txtServerUrl.Text
                objUserAccess.SMTPPort = IIf(IsNumeric(txtServerPort.Text), txtServerPort.Text, 0)
                objUserAccess.SMTPAuth = chkSmtpAuth.Checked
                objUserAccess.bitSMTPServer = True
                objUserAccess.SMTPSSL = chkSSL.Checked
                objUserAccess.ManageUserSMTPDetails()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
    Private Sub UpdateDomainSMTP()
        Try
            Dim objUserAccess As New UserAccess

            Dim plainText As String
            Dim cipherText As String

            objCommon = New CCommon

            plainText = txtPassWord.Text
            txtPassWord.Attributes("Value") = plainText
            cipherText = objCommon.Encrypt(plainText)

            objUserAccess.DomainID = Session("DomainID")
            objUserAccess.SMTPPassWord = cipherText
            objUserAccess.SMTPPort = IIf(IsNumeric(txtServerPort.Text), txtServerPort.Text, 0)
            objUserAccess.SMTPServer = txtServerUrl.Text
            objUserAccess.SMTPAuth = chkSmtpAuth.Checked
            objUserAccess.SMTPSSL = chkSSL.Checked
            objUserAccess.SMTPUserId = txtUserName.Text
            objUserAccess.bitSMTPServer = True
            objUserAccess.SMTPDisplayName = txtSenderName.Text.Trim
            objUserAccess.UpdateDomainSMTP()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            Dim objEmail As New Email
            If intMode = 1 Then
                If ValidateSMTP() = True Then
                    save()
                    litMessage.Text = "SMTP settings saved sucessfully"
                Else
                    objEmail.DisableSMTP(lngUserID, Session("DomainID"))
                    litMessage.Text = "Invalid SMTP Settings"
                End If
            ElseIf intMode = 2 Then
                If ValidateIMAP() = True Then
                    save()
                    litMessage.Text = "IMAP settings saved sucessfully"
                Else
                    litMessage.Text = "Invalid IMAP Settings"
                End If
            ElseIf intMode = 3 Then
                If ValidateSMTP() = True Then
                    UpdateDomainSMTP()
                    litMessage.Text = "SMTP settings saved sucessfully"
                Else
                    litMessage.Text = "Invalid SMTP Settings"
                End If

            ElseIf intMode = 4 Then
                If ValidateSMTP() = True Then
                    save()
                    litMessage.Text = "SMTP settings saved sucessfully"
                Else
                    litMessage.Text = "Invalid SMTP Settings"
                End If

            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            litMessage.Text = ex.Message
        End Try
    End Sub
    Function ValidateIMAP() As Boolean
        Try
            Dim objEmail As New Email
            'commented by chintan.cause : strmailid become nothing
            'If lngUserID = -1 Then
            '    strMailId = txtUserName.Text
            'Else
            '    strMailId = Session("UserMailIdTest")
            'End If
            strMailId = txtUserName.Text.Trim()

            Return objEmail.ValidateImap(txtServerUrl.Text, CCommon.ToInteger(txtServerPort.Text), chkSmtpAuth.Checked, strMailId, txtPassWord.Text, chkSSL.Checked)
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Function ValidateSMTP() As Boolean
        Try

            Dim objEmail As New Email
            objCommon = New CCommon

            If intMode = 3 Or intMode = 4 Then
                strMailId = txtUserName.Text
            Else
                strMailId = Session("UserMailIdTest")
            End If

            Return objEmail.ValidateSMTP(strMailId, txtPassWord.Text, objCommon.ToInteger(txtServerPort.Text), txtServerUrl.Text, chkSSL.Checked, chkSmtpAuth.Checked)
        Catch ex As Exception
            Dim objEmail As New Email
            objEmail.DisableSMTP(CCommon.ToLong(GetQueryStringVal("UserId")), Session("DomainID"))
            Throw ex
        End Try
    End Function
End Class