﻿Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Admin
Public Class frmMySavedSearch
    Inherits BACRMUserControl


    Private _FormID As Long
    Public Property FormID() As Long
        Get
            Return _FormID
        End Get
        Set(ByVal value As Long)
            _FormID = value
        End Set
    End Property


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then
                BindSearchName()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Sub BindSearchName()
        Try
            If FormID > 0 Then
                Dim objSearch As New FormGenericAdvSearch
                objSearch.byteMode = 1
                objSearch.DomainID = Session("DomainID")
                objSearch.UserCntID = Session("UserContactID")
                objSearch.FormID = FormID
                Dim dt As DataTable = objSearch.ManageSavedSearch()
                rptSavedSearch.DataSource = dt
                rptSavedSearch.DataBind()
                If dt.Rows.Count = 0 Then
                    hplNoRecords.Visible = True
                Else
                    hplMySavedSearch.Attributes.Add("style", "font-weight:bold;")
                End If
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

End Class