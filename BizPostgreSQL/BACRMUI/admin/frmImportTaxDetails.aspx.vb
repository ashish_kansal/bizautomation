﻿Imports System.IO
Imports BACRMBUSSLOGIC.BussinessLogic
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Admin
Imports System.Reflection
Imports LumenWorks.Framework.IO.Csv
Imports LumenWorks.Framework.Tests.Unit
Imports BACRM.BusinessLogic.Reports
Imports System.Text
Public Class frmImportTaxDetails
    Inherits BACRMPage


#Region "Decleration"
    Dim strFileName As String  'Variable to hold the FileName string
    Dim strUpdateValues As String

#End Region

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

#Region "Page Load"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            If Not IsPostBack Then
                pgBar.Style.Add("display", "none")
                 ' = Request.Url.Segments(Request.Url.Segments.Length - 1) ' = "Import"
            End If
            If CCommon.ToLong(Session("DomainID")) = 0 Then Response.Redirect("../admin/authentication.aspx?mesg=AS")

            If Session("FileLocation") <> "" Then
                displayDropdowns()
            End If

            btnImports.Text = "Import records to database"

            btnUpload.Attributes.Add("onClick", "return checkFileExt()")
            btnDisplay.Attributes.Add("onClick", "return setDllValues()")
            btnImports.Attributes.Add("onClick", "return displayPBar()")
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
#End Region

#Region "Display Dropdowns"
    Private Sub displayDropdowns()
        Try
            Dim dataTableHeading As New DataTable
            Dim fileLocation As String = Session("FileLocation")
            Dim streamReader As New StreamReader(fileLocation)
            Dim intCount As Integer = 0
            Dim intCountFor As Integer = 0
            Dim intStart As Integer
            Dim strSplitValue() As String
            Dim strSplitHeading() As String
            Dim strLine As String = streamReader.ReadLine()
            Dim dataTableMap As New DataTable
            dataTableMap.Columns.Add("Destination")
            dataTableMap.Columns.Add("ID")
            Try
                intStart = 0

                Do While Not strLine Is Nothing
                    Dim dataRowMap As DataRow
                    dataRowMap = dataTableMap.NewRow
                    strSplitHeading = Split(strLine, ",")
                    For intCountFor = intStart To strSplitHeading.Length - 1
                        dataRowMap = dataTableMap.NewRow
                        dataRowMap("Destination") = strSplitHeading(intCountFor).Replace("""", "").ToString
                        dataRowMap("ID") = intCountFor
                        dataTableMap.Rows.Add(dataRowMap)
                    Next
                    Exit Do
                Loop

                Dim trHeader As New TableRow
                Dim trDetail As New TableRow
                Dim tableHC As TableHeaderCell
                Dim tableDtl As TableCell
                Dim drHeading As DataRow

                intCount = 0
                dataTableHeading = Session("dataTableHeadingObj")

                Dim ddlDestination As DropDownList
                Dim txtDestination As TextBox
                Dim arryList() As String
                Dim strdllValue As String
                Dim intArryValue As Integer

                If Session("dllValue") <> "" Then
                    strdllValue = Session("dllValue")
                    arryList = Split(strdllValue, ",")
                End If

                tbldtls.Rows.Clear()
                'intCount = 2

                For Each drHeading In dataTableHeading.Rows
                    trHeader.CssClass = "hs"
                    tableHC = New TableHeaderCell
                    tableHC.ID = intCount
                    tableHC.Text = drHeading.Item(1)
                    tableHC.CssClass = "normal5"
                    trHeader.Cells.Add(tableHC)
                    tableDtl = New TableCell
                    ddlDestination = New DropDownList
                    txtDestination = New TextBox
                    ddlDestination.CssClass = "signup"
                    ddlDestination.ID = "ddlDestination" & intCount.ToString
                    ddlDestination.DataSource = dataTableMap
                    ddlDestination.DataTextField = "Destination"
                    ddlDestination.DataValueField = "ID"
                    ddlDestination.DataBind()
                    If Session("dllValue") <> "" Then
                        If arryList.Length > 9 Then
                            intArryValue = CType(arryList(intCount), Integer)
                            ddlDestination.SelectedIndex = intArryValue
                        ElseIf ddlDestination.Items.Count > intCount Then
                            ddlDestination.SelectedIndex = intCount
                        End If
                    ElseIf ddlDestination.Items.Count > intCount Then
                        ddlDestination.SelectedIndex = intCount
                    End If

                    tableDtl.Controls.Add(ddlDestination)

                    trDetail.Cells.Add(tableDtl)
                    intCount += 1
                    trDetail.CssClass = "is"
                    tbldtls.Rows.Add(trDetail)
                    tbldtls.Rows.AddAt(0, trHeader)
                Next

                Session("IntCount") = intCount - 1
                If txtDllValue.Text <> "" Then Session("dllValue") = txtDllValue.Text
                txtDllValue.Text = intCount - 1

            Catch ex As Exception
                Throw ex
            Finally
                streamReader.Close()
            End Try
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
#End Region

#Region "Display Click"
    Private Sub btnDisplay_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDisplay.Click
        Try
            Dim dataTableDestination As New DataTable
            Dim dataTableHeading As New DataTable
            Dim fileLocation As String = Session("FileLocation")
            Dim streamReader As StreamReader
            streamReader = File.OpenText(fileLocation)
            Dim intCount As Integer = Session("IntCount")
            Dim intCountFor As Integer = 0
            ' Dim strLine As String = streamReader.ReadLine()
            Dim arryList() As String
            Dim strdllValue As String
            Dim intArryValue As Integer
            Dim intCountSession As Integer = Session("IntCount")
            dataTableDestination = Session("dataTableDestinationObj")
            dataTableDestination.Clear()
            Dim csv As CsvReader = New CsvReader(streamReader, True)
            Try
                csv.MissingFieldAction = MissingFieldAction.ReplaceByEmpty
                csv.SupportsMultiline = True

                strdllValue = Session("dllValue")
                arryList = Split(strdllValue, ",")

                Dim dataRowDestination As DataRow

                Dim drHead As DataRow
                dataTableHeading = Session("dataTableHeadingObj")

                Dim dataTableRecord As New DataTable
                dataTableRecord = Session("dataTableRecord")
                dataTableRecord.Rows.Clear()
                Dim dataRowRecord As DataRow

                Dim i1000 As Integer = 0
                While (csv.ReadNextRecord())
                    dataRowDestination = dataTableDestination.NewRow
                    intCount = 0
                    For Each drHead In dataTableHeading.Rows
                        intArryValue = CType(arryList(intCount), Integer)
                        dataRowDestination(drHead.Item(1)) = csv(intArryValue).ToString
                        intCount += 1
                    Next
                    dataTableDestination.Rows.Add(dataRowDestination)
                    i1000 = i1000 + 1

                End While

                Session("dataTableDestinationObj") = dataTableDestination

                Dim drDestination As DataRow
                Dim iCount As Integer = 0
                Dim iCountRow As Integer = 1
                Dim trDetail As TableRow
                Dim tableCell As TableCell
                'tbldtls.Rows.Clear()
                Dim k As Integer = 0

                Dim objTaxDtl As New TaxDetails
                Dim objImpWzd As New ImportWizard

                For Each drDestination In dataTableDestination.Rows
                    trDetail = New TableRow
                    If k = 0 Then
                        trDetail.CssClass = "ais"
                        k = 1
                    Else
                        trDetail.CssClass = "is"
                        k = 0
                    End If

                    objTaxDtl.DomainId = Session("DomainID")
                    objTaxDtl.Country = objImpWzd.GetStateAndCountry(0, CCommon.ToString(drDestination("Country")), Session("DomainID"), 40)
                    objTaxDtl.State = objImpWzd.GetStateAndCountry(1, CCommon.ToString(drDestination("State")), Session("DomainID"))
                    objTaxDtl.TaxItemID = objImpWzd.GetStateAndCountry(19, CCommon.ToString(drDestination("Tax Type")), Session("DomainID"))

                    objTaxDtl.City = CCommon.ToString(drDestination("City"))
                    objTaxDtl.ZipPostal = CCommon.ToString(drDestination("Zip/Postal"))

                    drDestination("Tax Value") = drDestination("Tax Value").ToString.Replace("%", "")

                    Dim decTax As Decimal = IIf(IsNumeric(IIf(IsDBNull(drDestination("Tax Value")), "0", drDestination("Tax Value"))), IIf(IsDBNull(drDestination("Tax Value")), "0", drDestination("Tax Value")), 0)

                    dataRowRecord = dataTableRecord.NewRow
                    dataRowRecord("numCountry") = objTaxDtl.Country
                    dataRowRecord("numState") = objTaxDtl.State
                    dataRowRecord("vcCity") = objTaxDtl.City
                    dataRowRecord("vcZipPostal") = objTaxDtl.ZipPostal
                    dataRowRecord("numTaxItemID") = objTaxDtl.TaxItemID
                    dataRowRecord("decTax") = decTax
                    dataRowRecord("tintTaxType") = IIf(drDestination("Tax Value Type").ToString().ToLower() = "flat amount", 2, 1)

                    dataTableRecord.Rows.Add(dataRowRecord)

                    tableCell = New TableCell
                    tableCell.Text = CCommon.ToString(drDestination("Country"))
                    tableCell.CssClass = "normal1"
                    trDetail.Cells.Add(tableCell)

                    tableCell = New TableCell
                    tableCell.Text = CCommon.ToString(drDestination("State"))
                    tableCell.CssClass = "normal1"
                    trDetail.Cells.Add(tableCell)

                    tableCell = New TableCell
                    tableCell.Text = CCommon.ToString(drDestination("City"))
                    tableCell.CssClass = "normal1"
                    trDetail.Cells.Add(tableCell)

                    tableCell = New TableCell
                    tableCell.Text = CCommon.ToString(drDestination("Zip/Postal"))
                    tableCell.CssClass = "normal1"
                    trDetail.Cells.Add(tableCell)

                    tableCell = New TableCell
                    tableCell.Text = CCommon.ToString(drDestination("Tax Type"))
                    tableCell.CssClass = "normal1"
                    trDetail.Cells.Add(tableCell)

                    tableCell = New TableCell
                    tableCell.Text = CCommon.ToString(drDestination("Tax Value"))
                    tableCell.CssClass = "normal1"
                    trDetail.Cells.Add(tableCell)

                    tableCell = New TableCell
                    tableCell.Text = CCommon.ToString(drDestination("Tax Value Type"))
                    tableCell.CssClass = "normal1"
                    trDetail.Cells.Add(tableCell)

                    tableCell.CssClass = "normal1"
                    trDetail.Cells.Add(tableCell)

                    iCountRow += 1
                    tbldtls.Rows.AddAt(iCountRow, trDetail)
                    'If iCountRow = 100 Then
                    '    Exit For
                    'End If
                Next

                Session("dataTableRecord") = dataTableRecord

            Catch ex As Exception
                If ex.Message = "An item with the same key has already been added." Then
                    streamReader.Close()
                    litMessage.Text = "Duplicate column names found in CSV file, Your option is to remove duplicate columns and upload it again."
                    Exit Sub
                End If
                Response.Write(ex)
            Finally
                streamReader.Close()
            End Try
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try

    End Sub
#End Region

#Region "Create Table Schema"
    Private Sub createTableSchema()
        Try
            Dim dataTableDestination As New DataTable
            Dim dataTableHeading As New DataTable
            Dim dataTable As New DataTable
            Dim numRows As Integer
            Dim i As Integer
            dataTableHeading.Columns.Add("Id")
            dataTableHeading.Columns.Add("Destination")
            dataTableHeading.Columns.Add("FType")
            dataTableHeading.Columns.Add("cCtype")
            dataTableHeading.Columns.Add("vcAssociatedControlType")
            dataTableHeading.Columns.Add("vcDbColumnName")
            Dim dataRowHeading As DataRow

            Dim dataTableRecord As New DataTable

            Dim objImport As New ImportWizard
            objImport.DomainId = Session("DomainID")
            objImport.ImportType = 6
            dataTable = objImport.GetConfiguration.Tables(0)

            numRows = dataTable.Rows.Count()

            For i = 0 To numRows - 1
                dataRowHeading = dataTableHeading.NewRow
                dataRowHeading("Destination") = dataTable.Rows(i).Item("vcFormFieldName")
                dataRowHeading("Id") = dataTable.Rows(i).Item("numFormFieldId")
                dataRowHeading("FType") = IIf(dataTable.Rows(i).Item("bitCustomFld") = "False", "0", "1")
                dataRowHeading("cCtype") = dataTable.Rows(i).Item("cCtype")
                dataRowHeading("vcAssociatedControlType") = dataTable.Rows(i).Item("vcAssociatedControlType")
                dataRowHeading("vcDbColumnName") = dataTable.Rows(i).Item("vcDbColumnName")
                dataTableHeading.Rows.Add(dataRowHeading)

                dataTableRecord.Columns.Add(dataTable.Rows(i).Item("vcDbColumnName"))
            Next

            Session("dataTableRecord") = dataTableRecord
            Session("dataTableHeadingObj") = dataTableHeading
            Dim drHead As DataRow
            dataTableDestination.Columns.Add("SlNo")
            dataTableDestination.Columns("SlNo").AutoIncrement = True
            dataTableDestination.Columns("SlNo").AutoIncrementSeed = 1
            dataTableDestination.Columns("SlNo").AutoIncrementStep = 1
            For Each drHead In dataTableHeading.Rows
                dataTableDestination.Columns.Add(drHead.Item(1))
            Next
            Session("dataTableDestinationObj") = dataTableDestination
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
#End Region

#Region "Upload Click"
    Private Sub btnUpload_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUpload.Click
        Try
            If Not txtFile.PostedFile Is Nothing And txtFile.PostedFile.ContentLength > 0 Then
                pgBar.Style.Add("display", "")
                Dim fileName As String = System.IO.Path.GetFileName(txtFile.PostedFile.FileName)
                Dim SaveLocation As String = Server.MapPath("../Documents/Docs") & "\" & fileName
                Try
                    txtFile.PostedFile.SaveAs(SaveLocation)
                    Session("FileLocation") = SaveLocation
                    litMessage.Text = "The file has been uploaded"
                    createTableSchema()
                    displayDropdowns()
                    pgBar.Style.Add("display", "none")
                Catch ex As Exception
                    pgBar.Style.Add("display", "none")
                    Throw ex
                End Try
            Else
                Session("IntCount") = ""
                Session("FileLocation") = ""
                Session("dataTableDestinationObj") = ""
                Session("dataTableHeadingObj") = ""
                Session("dllValue") = ""
                tbldtls.Rows.Clear()
                litMessage.Text = "Please select a file to upload."
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
#End Region

#Region "Imports Click"
    Private Sub btnImports_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnImports.Click
        Dim objTaxDtl As TaxDetails
        Dim objImpWzd As New ImportWizard
        Try
            Dim dataTableRecord As New DataTable
            dataTableRecord = Session("dataTableRecord")
            Dim dataRowRecord As DataRow

            If chkDeleteExisting.Checked = True Then 'Delete All Records
                objTaxDtl = New TaxDetails
                objTaxDtl.DomainId = Session("DomainId")
                objTaxDtl.mode = 2
                objTaxDtl.ManageTaxDetails()
            End If

            For Each dataRowRecord In dataTableRecord.Rows 'Insert New Records
                objTaxDtl = New TaxDetails
                objTaxDtl.DomainId = Session("DomainId")
                objTaxDtl.Tax = dataRowRecord("decTax")
                objTaxDtl.Country = dataRowRecord("numCountry")
                objTaxDtl.State = dataRowRecord("numState")
                objTaxDtl.City = dataRowRecord("vcCity")
                objTaxDtl.ZipPostal = dataRowRecord("vcZipPostal")
                objTaxDtl.mode = 0
                objTaxDtl.TaxItemID = dataRowRecord("numTaxItemID")
                objTaxDtl.TaxValueType = dataRowRecord("tintTaxType")

                objTaxDtl.ManageTaxDetails()
            Next

            litMessage.Text = "Records are sucessfully saved into database"
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        Finally
            pgBar.Style.Add("display", "none")
            Session("IntCount") = ""
            Session("FileLocation") = ""
            Session("dataTableDestinationObj") = ""
            Session("dataTableHeadingObj") = ""
            Session("dllValue") = ""
            Session("dataTableRecord") = ""
            tbldtls.Rows.Clear()
        End Try
    End Sub
#End Region

#Region "Existing Tax Download"
    Private Sub hplExistingTax_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles hplExistingTax.Click
        Try
            Dim dtTable As DataTable
            Dim objTaxDtl As TaxDetails
            objTaxDtl = New TaxDetails
            objTaxDtl.DomainId = Session("DomainId")

            objTaxDtl.TaxItemID = 0
            objTaxDtl.Country = 0
            objTaxDtl.State = 0

            dtTable = objTaxDtl.GetTaxDetails()


            Dim attachment As String = "attachment; filename=ImportTaxDetails.csv"

            Dim context As HttpContext = HttpContext.Current
            context.Response.Clear()
            context.Response.Clear()
            context.Response.ClearHeaders()
            context.Response.ClearContent()
            context.Response.AddHeader("content-disposition", attachment)
            context.Response.ContentType = "text/csv"
            context.Response.AddHeader("Pragma", "public")

            Dim columnNames As String = "Country,State,City,Zip/Postal,Tax Type,Tax Value,Tax Value Type"
            context.Response.Write(columnNames)
            context.Response.Write(Environment.NewLine)

            Dim stringBuilder As New StringBuilder()

            For Each row As DataRow In dtTable.Rows
                stringBuilder = New StringBuilder()
                AddComma(row("Country"), stringBuilder)
                AddComma(row("State"), stringBuilder)
                AddComma(row("vcCity"), stringBuilder)
                AddComma(row("vcZipPostal"), stringBuilder)
                AddComma(row("vcTaxName"), stringBuilder)
                AddComma(row("decTaxPercentage"), stringBuilder)
                AddComma(IIf(row("tintTaxType") = "2", "Flat Amount", "Percentage"), stringBuilder)

                context.Response.Write(stringBuilder.ToString())
                context.Response.Write(Environment.NewLine)
            Next

        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        Finally
            Context.Response.End()
        End Try
    End Sub

    Private Shared Sub AddComma(ByVal value As String, ByVal stringBuilder As StringBuilder)
        stringBuilder.Append(value.Replace(","c, " "c))
        stringBuilder.Append(", ")
    End Sub
#End Region

End Class