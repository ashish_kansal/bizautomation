<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" CodeBehind="userdetails.aspx.vb"
    Inherits="BACRM.UserInterface.Admin.userdetails" MasterPageFile="~/common/Popup.Master" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>User Details</title>
    <script language="javascript">

        function OpenSMTPPopUp(a, b) {
            window.open('../admin/frmSMTPPopup.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&UserId=' + a + '&Mode=' + b, '', 'toolbar=no,titlebar=no,left=200,top=250,width=750,height=200,scrollbars=yes,resizable=yes');
            return false;
        }

        function GoogleGroupsConfigure(a, b) {
            window.open('../admin/frmGoogleGroupsConfigure.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&UserId=' + a + '&Mode=' + b, '', 'toolbar=no,titlebar=no,left=200,top=250,width=200,height=750,scrollbars=yes,resizable=yes');
            return false;
        }

        function GoogleAuthenticate() {
            window.open('../GoogleAuthenticateFirst.aspx', '', 'toolbar=no,titlebar=no,left=200,top=250,width=750,height=750,scrollbars=yes,resizable=yes');
            return false;
        }

        function Office365Authenticate() {
            var h = screen.height;
            var w = screen.width;
            window.open('../Office365Authentication.aspx', '', 'toolbar=no,titlebar=no,left=50,top=50,width=' + (w - 100) + ',height=' + (h - 200) + ',scrollbars=yes,resizable=yes');
            return false;
        }

        function GoogleMailAuthenticate() {
            var h = screen.height;
            var w = screen.width;
            window.open('../GoogleMailAuthentication.aspx', '', 'toolbar=no,titlebar=no,left=50,top=50,width=' + (w - 100) + ',height=' + (h - 200) + ',scrollbars=yes,resizable=yes');
            return false;
        }

        function Office365MailAuthenticate() {
            var h = screen.height;
            var w = screen.width;
            window.open('../Office365MailAuthentication.aspx', '', 'toolbar=no,titlebar=no,left=50,top=50,width=' + (w - 100) + ',height=' + (h - 200) + ',scrollbars=yes,resizable=yes');
            return false;
        }

        function Subscribe() {
            if ($('#ddlUserDetails').val() == 0) {
                alert('Select A User');
                return false
            }
            if ($('#txtEmailID').val() == '') {
                alert('Enter The EmailId')
                return false;
            }
            if ($('#txtPassword').val() == '') {
                alert('Enter The Password')
                return false;
            }
            return true;
        }
        function checkValidFormParmeters() {
            var Page_IsValidStatus = "False";
            var strElementName;
            for (var intElementIndex = 0; intElementIndex <= $("#form1 input").length - 1; intElementIndex++) {
                strElementName = $("#form1 input")[intElementIndex].name
                if (strElementName.substring(0, 19) == "chkBxListAuthGroups") {
                    if ($("#form1 input")[intElementIndex].checked) {
                        Page_IsValidStatus = "True"
                    }
                }
            }
            if (Page_IsValidStatus == "True") {
                if (typeof (Page_Validators) == "undefined")
                    return false;
                if (Page_IsValid) {
                    return true;
                } else {
                    $('#lblError').html("Enter the values for the fields marked by #.");
                    $('#lblError').add("visibility", "visible");
                }
            } else {
                $('#lblError').text("Select the groups to assign to the user.");
                $('#lblError').add("visibility", "visible");
            }
            return false;
        }
        function MoveUp(tbox) {

            for (var i = 1; i < tbox.options.length; i++) {
                if (tbox.options[i].selected && tbox.options[i].value != "") {

                    var SelectedText, SelectedValue;
                    SelectedValue = tbox.options[i].value;
                    SelectedText = tbox.options[i].text;
                    tbox.options[i].value = tbox.options[i - 1].value;
                    tbox.options[i].text = tbox.options[i - 1].text;
                    tbox.options[i - 1].value = SelectedValue;
                    tbox.options[i - 1].text = SelectedText;
                }
            }
            return false;
        }
        function MoveDown(tbox) {

            for (var i = 0; i < tbox.options.length - 1; i++) {
                if (tbox.options[i].selected && tbox.options[i].value != "") {

                    var SelectedText, SelectedValue;
                    SelectedValue = tbox.options[i].value;
                    SelectedText = tbox.options[i].text;
                    tbox.options[i].value = tbox.options[i + 1].value;
                    tbox.options[i].text = tbox.options[i + 1].text;
                    tbox.options[i + 1].value = SelectedValue;
                    tbox.options[i + 1].text = SelectedText;
                }
            }
            return false;
        }

        sortitems = 0;  // 0-False , 1-True
        function move(fbox, tbox) {

            for (var i = 0; i < fbox.options.length; i++) {
                if (fbox.options[i].selected && fbox.options[i].value != "") {
                    /// to check for duplicates 
                    for (var j = 0; j < tbox.options.length; j++) {
                        if (tbox.options[j].value == fbox.options[i].value) {
                            alert("Item is already selected");
                            return false;
                        }
                    }

                    var no = new Option();
                    no.value = fbox.options[i].value;
                    no.text = fbox.options[i].text;
                    tbox.options[tbox.options.length] = no;
                    fbox.options[i].value = "";
                    fbox.options[i].text = "";

                }
            }
            BumpUp(fbox);
            if (sortitems) SortD(tbox);
            return false;
        }

        function remove1(fbox, tbox) {

            for (var i = 0; i < fbox.options.length; i++) {
                if (fbox.options[i].selected && fbox.options[i].value != "") {
                    /// to check for duplicates 
                    for (var j = 0; j < tbox.options.length; j++) {
                        if (tbox.options[j].value == fbox.options[i].value) {
                            fbox.options[i].value = "";
                            fbox.options[i].text = "";
                            BumpUp(fbox);
                            if (sortitems) SortD(tbox);
                            return false
                            //alert("Item is already selected");
                            //return false;
                        }
                    }

                    var no = new Option();
                    no.value = fbox.options[i].value;
                    no.text = fbox.options[i].text;
                    tbox.options[tbox.options.length] = no;
                    fbox.options[i].value = "";
                    fbox.options[i].text = "";

                }
            }
            BumpUp(fbox);
            if (sortitems) SortD(tbox);
            return false;
        }

        function BumpUp(box) {
            for (var i = 0; i < box.options.length; i++) {
                if (box.options[i].value == "") {
                    for (var j = i; j < box.options.length - 1; j++) {
                        box.options[j].value = box.options[j + 1].value;
                        box.options[j].text = box.options[j + 1].text;
                    }
                    var ln = i;
                    break;
                }
            }
            if (ln < box.options.length) {
                box.options.length -= 1;
                BumpUp(box);
            }
        }


        function SortD(box) {
            var temp_opts = new Array();
            var temp = new Object();
            for (var i = 0; i < box.options.length; i++) {
                temp_opts[i] = box.options[i];
            }
            for (var x = 0; x < temp_opts.length - 1; x++) {
                for (var y = (x + 1) ; y < temp_opts.length; y++) {
                    if (temp_opts[x].text > temp_opts[y].text) {
                        temp = temp_opts[x].text;
                        temp_opts[x].text = temp_opts[y].text;
                        temp_opts[y].text = temp;
                        temp = temp_opts[x].value;
                        temp_opts[x].value = temp_opts[y].value;
                        temp_opts[y].value = temp;
                    }
                }
            }
            for (var i = 0; i < box.options.length; i++) {
                box.options[i].value = temp_opts[i].value;
                box.options[i].text = temp_opts[i].text;
            }
        }
        function Save() {
            var str = '';

            if ($('#ddlUserDetails').val() == 0) {
                alert('Select User');
                return false
            }
            if ($('#txtEmailID').val() == '') {
                alert('Enter EmailId')
                return false;
            }
            if ($('#txtPassword').val() == '') {
                alert('Enter Password')
                return false;
            }
            if ($('#ddlGroups').val() == 0) {
                alert('Select Permission Group');
                return false
            }

            for (var i = 0; i < document.getElementById('lstTerritoryAdd').options.length; i++) {
                var SelectedText, SelectedValue;
                SelectedValue = document.getElementById('lstTerritoryAdd').options[i].value;
                SelectedText = document.getElementById('lstTerritoryAdd').options[i].text;
                str = str + SelectedValue + ','
            }
            document.getElementById('hdnValue').value = str;
            str = '';
            for (var i = 0; i < document.getElementById('lstTeamAdd').options.length; i++) {
                var SelectedText, SelectedValue;
                SelectedValue = document.getElementById('lstTeamAdd').options[i].value;
                SelectedText = document.getElementById('lstTeamAdd').options[i].text;
                str = str + SelectedValue + ','
            }
            document.getElementById('txtTeamValue').value = str;

            return true;
        }
        function Close() {
            window.close()
            return false;
        }
        function OpenRoles(a) {
            window.open('../TimeAndExpense/frmAddRoles.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&UserCntID=' + a, '', 'toolbar=no,titlebar=no,left=300,top=400,width=500,height=400,scrollbars=yes,resizable=yes');
            return false;
        }
        function Hide() {

            if (document.getElementById('radIndAccess').checked == true) {

                trPassword.style.display = '';
            }
            else {

                trPassword.style.display = 'none';
            }

        }
        function windowResize() {
            var width = document.getElementById("resizeDiv").offsetWidth;
            var height = document.getElementById("resizeDiv").offsetHeight;
            window.resizeTo(width + 35, height + 150);
        }
        function ChangeTemplate() {
            return confirm("This will wipe out this user�s dashboard, and replace it with the configuration of portlets associated with the selected \"Dashboard template\". Are you sure you want to do this?");
        }

        $(document).ready(function () {
            $("[id*=rblMailProvider] input").on("click", function () {
                if ($(this).val() == 3) {
                    $("[id$=hplMail]").hide();
                    $("[id$=chkOauthImap]").closest("span").hide();
                    $("[id$=hplSMTP]").show();
                    $("[id$=hplImap]").show();
                } else if ($(this).val() == 2) {
                    $("[id$=chkOauthImap]").closest("span").show();
                    $("[id$=hplMail]").attr("onclick", "return Office365MailAuthenticate()");
                    $("[id$=hplMail]").text("Allow BizAutomation to access Office365 Mail");
                    $("[id$=hplMail]").show();
                    $("[id$=hplSMTP]").hide();
                    $("[id$=hplImap]").hide();
                } else {
                    $("[id$=chkOauthImap]").closest("span").show();
                    $("[id$=hplMail]").attr("onclick", "return GoogleMailAuthenticate()");
                    $("[id$=hplMail]").text("Allow BizAutomation to access Google Mail");
                    $("[id$=hplMail]").show();
                    $("[id$=hplSMTP]").hide();
                    $("[id$=hplImap]").hide();
                }
            });
        });
    </script>
    <link rel="Stylesheet" href="../CSS/bootstrap.min.css" />
    <style type="text/css">
        .tooltip {
            width:400px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <div class="input-part">
        <div class="right-input">
            <asp:Button ID="btnSaveClose" runat="server" CssClass="button" Text="Save &amp; Close"></asp:Button>
            <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Close"></asp:Button>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageTitle" runat="server" ClientIDMode="Static">
    User Details
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="Content" runat="server" ClientIDMode="Static">
    <telerik:RadScriptManager ID="RadScriptManager1" runat="server" />
    <div id="resizeDiv" style="float: left;">
        <asp:Button ID="btnUpld" runat="server" Style="display: none" Text="Button" />
        <table cellspacing="0" cellpadding="0" width="100%" border="0">
            <tr>
                <td align="right">
                    <font class="normal4">
                        <asp:Literal ID="litMessage" runat="server" EnableViewState="False"></asp:Literal></font>&nbsp;&nbsp;
                </td>
            </tr>
        </table>

        <table cellspacing="2" cellpadding="2" width="850" border="0">
            <tr style="height: 35px">
                <td style="font-weight: bold; text-align: right; padding-right: 5px;"><span style="color:red">* </span>User:</td>
                <td>
                    <asp:DropDownList ID="ddlUserDetails" Width="200" runat="server" CssClass="signup">
                    </asp:DropDownList>
                </td>
                <td></td>
                <td style="font-weight: bold; text-align: right; padding-right: 5px;">Linkedin ID:</td>
                <td>
                    <asp:TextBox ID="txtLinkedinId" TabIndex="1" runat="server" CssClass="signup" Width="200"></asp:TextBox>
                </td>
            </tr>
            <tr style="height: 35px">
                <td style="font-weight: bold; text-align: right; padding-right: 5px;">Status:</td>
                <td>
                    <asp:DropDownList ID="ddlStatus" runat="server" CssClass="signup" Width="200">
                        <asp:ListItem Value="1">Active</asp:ListItem>
                        <asp:ListItem Value="0">Suspended</asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td></td>
                <td style="font-weight: bold; text-align: right; padding-right: 5px;">Description:</td>
                <td>
                    <asp:TextBox ID="txtUserDesc" TabIndex="1" runat="server" CssClass="signup" Height="35" Width="200" TextMode="MultiLine"></asp:TextBox>
                </td>
            </tr>
            <tr style="height: 35px">
                <td style="font-weight: bold; text-align: right; padding-right: 5px;"><span style="color:red">* </span>Email:</td>
                <td>
                    <asp:TextBox ID="txtEmailID" runat="server" Width="200" CssClass="signup" autocomplete="off"></asp:TextBox>
                </td>
                <td></td>
                <td style="font-weight: bold; text-align: right; padding-right: 5px;"><span style="color:red">* </span>Password:</td>
                <td>
                    <asp:TextBox TextMode="Password" ID="txtPassword" runat="server" Width="200" CssClass="signup" autocomplete="off"></asp:TextBox>
                </td>
            </tr>
            <tr style="height: 35px">
                <td style="font-weight: bold; text-align: right; padding-right: 5px;"><span style="color:red">* </span>Permission Group:</td>
                <td>
                    <asp:DropDownList ID="ddlGroups" Width="200" runat="server" CssClass="signup">
                    </asp:DropDownList>
                </td>
                <td></td>
                <td style="font-weight: bold; text-align: right; padding-right: 5px;">Class:
                </td>
                <td>
                    <asp:DropDownList ID="ddlClass" Width="200" runat="server" CssClass="signup">
                    </asp:DropDownList>
                    <span style="display: none">Name<asp:TextBox ID="txtUserName" runat="server" Width="200"
                        CssClass="signup"></asp:TextBox>
                    </span>
                </td>
            </tr>
            <tr style="height: 35px">
                <td style="font-weight: bold; text-align: right; padding-right: 5px;"></td>
                <td></td>
                <td></td>
                <td style="font-weight: bold; text-align: right; padding-right: 5px;">External Location:</td>
                <td>
                    <asp:DropDownList ID="ddlWarehouse" runat="server" CssClass="signup">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr style="height:120px">
                <td style="font-weight: bold; text-align: right; padding-right: 5px;">Territory:</td>
                <td>Available Territories<br />
                    <asp:ListBox ID="lstTerritory" runat="server" Width="200" Height="80" CssClass="signup" SelectionMode="Multiple"></asp:ListBox>
                </td>
                <td style="text-align:center">
                    <asp:Button ID="btnAdd" Style="width: 100%;" CssClass="button" runat="server" Text="Add >>"></asp:Button>
                    <br />
                    <br />
                    <asp:Button ID="btnRemove" Style="width: 100%;" CssClass="button" runat="server" Text="<< Remove"></asp:Button>
                </td>
                <td style="font-weight: bold; text-align: right; padding-right: 5px;">Territory:</td>
                <td>Selected Territory<br />
                    <asp:ListBox ID="lstTerritoryAdd" Width="200" Height="80" runat="server" CssClass="signup" SelectionMode="Multiple"></asp:ListBox>
                </td>
            </tr>
            <tr style="height:120px">
                <td style="font-weight: bold; text-align: right; padding-right: 5px;">Team:</td>
                <td>
                     Available Teams
                    <br/>
                    <asp:ListBox ID="lstTeamAvail" runat="server" Width="200" Height="80" CssClass="signup" SelectionMode="Multiple"></asp:ListBox>
                </td>
                <td style="text-align:center">
                    <asp:Button ID="btnAddTeam" Style="width: 100%;" CssClass="button" runat="server" Text="Add >>"></asp:Button>
                    <br/>
                    <br/>
                    <asp:Button ID="btnRemTeam" Style="width: 100%;" CssClass="button" runat="server" Text="<< Remove"></asp:Button>
                </td>
                <td style="font-weight: bold; text-align: right; padding-right: 5px;">Team:</td>
                <td>
                    Selected Team<br/>
                    <asp:ListBox ID="lstTeamAdd" Width="200" Height="80" runat="server" CssClass="signup" SelectionMode="Multiple"></asp:ListBox>
                </td>
            </tr>
            <tr style="height: 35px">
                <td colspan="2">
                    <asp:CheckBox ID="chkRelation" Text="Display in Assign-to,Share with and Record owner list boxes" runat="server" /><asp:Label ID="Label2" runat="server" ToolTip="<p>Clicking this check box will populate the �Assign to� drop down list on various records such as Organizations, Contacts, Orders, Cases, Projects, and so on, but only when it meets the following criteria:</p><p>1st Within Admin | Internal Users, the user being added to the �Assign to� list must also be within the same territory scope as the other users (see the Territory Option List box within the internal user�s profile).</p><p>2nd Within Admin | Internal Users, the user must be added as an �Active� or �Suspended� user.</p><p>3rd Within the Employee record (i.e. the Contact record under the �Employer� Organization record) � Make sure that within the �Team� drop down, the team value must is selected for the user you want to include in that assign to list.</p><p>This way when you�re logged in, you will see assignees that belong to the team scope enabled for you within internal users.  For example, if you have 5 users that belong to team-1, and your territory scope includes Team-1 and all those 5 users have the check box for Assign-to clicked within their internal user profile � THEN they will all show up in the assign-to drop down list when you log into your account.</p>" Text="[?]" CssClass="tip"></asp:Label>
                </td>
                <td></td>
                 <td colspan="2" class="normal1" align="center">
                     <asp:RadioButtonList ID="rblMailProvider" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow">
                         <asp:ListItem Text="Gmail" Value="1"></asp:ListItem>
                         <asp:ListItem Text="Office 365" Value="2"></asp:ListItem>
                         <asp:ListItem Text="Other" Value="3"></asp:ListItem>
                     </asp:RadioButtonList>
                     <asp:CheckBox ID="chkOauthImap" runat="server" Text="Enable Imap" style="display:none"></asp:CheckBox>
                     <br />
                    <asp:HyperLink ID="hplMail" runat="server" CssClass="hyperlink" style="display:none">Allow BizAutomation to access Google Mail</asp:HyperLink>
                    <asp:HyperLink ID="hplSMTP" runat="server" CssClass="hyperlink" style="display:none">SMTP(Not Configured)</asp:HyperLink><br />
                    <asp:HyperLink ID="hplImap" runat="server" CssClass="hyperlink" style="display:none">IMAP(Not Configured)</asp:HyperLink>
                </td>
            </tr>
            <tr style="height: 35px">
                <td colspan="5" class="normal1">
                    <asp:Label ID="Label1" runat="server" ToolTip="<p>This list is made up of employees who�s �Enable for payroll expense� check box has been selected from their employee profile. If  �Salary� is selected, BizAutomation will automatically add �Work Hours� or �Productive Hours� at the per hour rate according to the �Work Schedule� set from the �Human Resources� module to Payroll. The hourly selection will mean that the employee will be responsible for adding hours manually from any BizAutomation module, such as �Action Items� in the Activity module (e.g. Types here are Tasks, Communications, Calendar meetings from G-Suite or Office 365 calendar entries, etc..) directly from the Time & Expense module, from within Manufacturing | Work Order | WIP Process, or from a Project.  Hours entered can be viewed from the Time Registry under �Time & Expense� sub-menu within the �Human Resources�, or the �Payroll� module.</p>" Text="[?]" CssClass="tip"></asp:Label>
                    <asp:CheckBox ID="chkIsOnPayroll" runat="server" Text="Enable for payroll expense" Font-Bold="true" />
                    <br />
                    <asp:RadioButton ID="rbPayrollHourly" runat="server" GroupName="PayrollType" Checked="true" Text="Hourly" Font-Bold="true" /> Normal rate per hour <asp:TextBox ID="txtHourRate" runat="server" Width="80" /> &nbsp;&nbsp; After <asp:DropDownList ID="ddlHourType" runat="server">
                        <asp:ListItem Text="Work" Value="1"></asp:ListItem>
                        <asp:ListItem Text="Productive" Value="2"></asp:ListItem>
                    </asp:DropDownList> &nbsp;&nbsp; overtime rate increases to &nbsp;&nbsp;  <asp:TextBox ID="txtOvertimeRate" runat="server" Width="80" />
                    <br />
                    <asp:RadioButton ID="rbPayrollSalary" runat="server" GroupName="PayrollType" Text="Salary" /> 
                </td>
            </tr>
            
             <tr style="height: 35px">
                <td colspan="6">
                    <b>Populate user�s dashboard using the following Dashboard template</b> 

                    <%--<asp:DropDownList ID="ddlDashboardTemplate" runat="server"></asp:DropDownList>--%> 
                     <telerik:RadComboBox ID="rcbDashboardTemplate" runat="server" Width="25%" CheckBoxes="true"></telerik:RadComboBox>
                    <asp:Button ID="btnUpdateDashboardTemplate" runat="server" CssClass="btn btn-primary" Text="Save" OnClientClick="return ChangeTemplate();" />

                </td>
                 
            </tr>
            <tr style="height: 35px">
                <td colspan="5">
                    <asp:HyperLink ID="hplGoogleAuthenticate" runat="server" CssClass="hyperlink" Visible="false">Allow BizAutomation to access Google Calendar/Contacts</asp:HyperLink>
                </td>
            </tr>
        </table>
        <asp:TextBox ID="txtSubscriptionId" runat="server" Style="display: none"></asp:TextBox><asp:TextBox ID="hdnValue" runat="server" Style="display: none"></asp:TextBox><asp:TextBox ID="txtTeamValue" runat="server" Style="display: none"></asp:TextBox><%--<asp:TextBox Style="display: none" ID="txtEmail" runat="server"></asp:TextBox>--%><asp:TextBox Style="display: none" ID="txtEmailUser" runat="server"></asp:TextBox>
    </div>
    <script type="text/javascript">        windowResize();</script>
</asp:Content>
