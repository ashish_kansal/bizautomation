﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="CommissionContacts.aspx.vb"
    Inherits=".CommissionContacts" MasterPageFile="~/common/Popup.Master" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Commission Contacts</title>
    <script language="javascript" type="text/javascript">
        function Save() {
            if ($find('radCmbCompany').get_value() == "") {
                alert("Select Organization")
                return false;
            }
            if (document.getElementById("ddlContactId").value == "0") {
                alert("Select contact")
                document.getElementById("ddlContactId").focus()
                return false;
            }
        }

        function OnClientItemsRequesting(sender, eventArgs) {
            var context = eventArgs.get_context();
            context["excludeEmployer"] = 1;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <div class="input-part">
        <div class="right-input">
            <asp:Button ID="btnAdd" runat="server" Text="Add" CssClass="button"></asp:Button>
            <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="button"></asp:Button>&nbsp;&nbsp;
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageTitle" runat="server" ClientIDMode="Static">
    Commission Contacts
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="Content" runat="server" ClientIDMode="Static">
    <asp:ScriptManager runat="server" ID="sm1" />
    <asp:Table ID="Table2" runat="server" GridLines="None" BorderColor="black" Width="600px"
        BorderWidth="1" CssClass="aspTable" CellSpacing="0" CellPadding="0">
        <asp:TableRow>
            <asp:TableCell VerticalAlign="Top">
                <table width="100%" border="0">
                    <tr>
                        <td class="text" align="left" width="80">Organization
                        </td>
                        <td>
                            <telerik:RadComboBox AccessKey="C" ID="radCmbCompany" Width="195px" DropDownWidth="500px"
                                OnClientItemsRequested="OnClientItemsRequestedOrganization"
                                OnClientItemsRequesting="OnClientItemsRequesting"
                                ClientIDMode="Static"
                                ShowMoreResultsBox="true" 
                                Skin="Default" runat="server" AllowCustomText="True" EnableLoadOnDemand="True"
                                AutoPostBack="false">
                                <WebServiceSettings Path="../common/Common.asmx" Method="GetCompanies" />
                            </telerik:RadComboBox>
                        </td>
                        <%-- <td class="text" align="right">
                            Contact
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlContactId" runat="server" CssClass="signup" Width="180">
                            </asp:DropDownList>
                        </td>--%>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <asp:GridView ID="gvCommissionsContacts" runat="server" EnableViewState="true" AutoGenerateColumns="false"
                                CssClass="tbl" Width="100%" DataKeyNames="numCommContactID">
                                <AlternatingRowStyle CssClass="ais" />
                                <RowStyle CssClass="is" />
                                <HeaderStyle CssClass="hs" />
                                <Columns>
                                    <asp:BoundField DataField="vcCompanyName" Visible="true" HeaderText="Organization" />
                                    <%--<asp:BoundField DataField="vcUserName" Visible="true" HeaderText="Contact" />--%>
                                    <asp:TemplateField HeaderStyle-Width="10" ItemStyle-Width="10">
                                        <ItemTemplate>
                                            <asp:Button ID="btnDelete" runat="server" CssClass="button Delete" Text="X" CommandName="DeleteContact"
                                                CommandArgument='<%# Eval("numCommContactID")  %>'></asp:Button>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
</asp:Content>
