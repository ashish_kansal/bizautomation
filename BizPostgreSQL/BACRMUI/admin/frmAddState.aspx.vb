Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Common
Namespace BACRM.UserInterface.Admin
    Public Class frmAddState
        Inherits BACRMPage

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub
        Protected WithEvents btnClose As System.Web.UI.WebControls.Button
        Protected WithEvents Table2 As System.Web.UI.WebControls.Table
        Protected WithEvents btnAdd As System.Web.UI.WebControls.Button
        Protected WithEvents litMessage As System.Web.UI.WebControls.Literal
        Dim myDataTable As DataTable
        Protected WithEvents ddlCountry As System.Web.UI.WebControls.DropDownList
        Protected WithEvents txtState As System.Web.UI.WebControls.TextBox
        Protected WithEvents dgCountry As System.Web.UI.WebControls.DataGrid
        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.
        Private designerPlaceholderDeclaration As System.Object

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Try


                GetUserRightsForPage(13, 23)

                If m_aryRightsForPage(RIGHTSTYPE.ADD) = 0 Then
                    btnAdd.Visible = False
                End If

                If Not IsPostBack Then
                    
                    objCommon.sb_FillComboFromDBwithSel(ddlCountry, 40, Session("DomainID"))
                    objCommon.sb_FillComboFromDBwithSel(ddlShippingZone, 834, Session("DomainID"))
                    BindGrid()
                End If
                litMessage.Text = ""
                btnClose.Attributes.Add("onclick", "return Close()")
                btnAdd.Attributes.Add("onclick", "return Save()")
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Sub BindGrid()
            Try
                Dim objUserAccess As New UserAccess
                objUserAccess.DomainID = Session("DomainID")
                objUserAccess.Country = ddlCountry.SelectedItem.Value
                dgCountry.DataSource = objUserAccess.SelState
                dgCountry.DataBind()
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
            Try
                Dim objUserAccess As New UserAccess
                objUserAccess.DomainID = Session("DomainID")
                objUserAccess.StateName = txtState.Text
                objUserAccess.Country = ddlCountry.SelectedItem.Value
                objUserAccess.ShippingZoneID = ddlShippingZone.SelectedItem.Value
                objUserAccess.UserId = Session("UserID")
                objUserAccess.vcAbbreviations = txtAbbreviations.Text
                objUserAccess.ManageState()
                Call BindGrid()
                txtState.Text = ""
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub dgCountry_EditCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgCountry.EditCommand
            Try
                dgCountry.EditItemIndex = e.Item.ItemIndex
                Call BindGrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub ddlCountry_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCountry.SelectedIndexChanged
            Try
                BindGrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub dgCountry_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgCountry.ItemDataBound
            Try
                If e.Item.ItemType = ListItemType.EditItem Then
                    Dim ddl As DropDownList
                    Dim lbl As Label
                    Dim txt As TextBox
                    Dim txtAv As TextBox
                    ddl = e.Item.FindControl("ddlECountry")
                    lbl = e.Item.FindControl("lblCountryID")
                    Dim ddlShippingZone As DropDownList = e.Item.FindControl("ddlShippingZone")
                    Dim lblShippingZoneID As Label = e.Item.FindControl("lblShippingZoneID")


                    objCommon.sb_FillComboFromDBwithSel(ddl, 40, Session("DomainID"))
                    ddl.Items.FindByValue(lbl.Text).Selected = True

                    objCommon.sb_FillComboFromDBwithSel(ddlShippingZone, 834, Session("DomainID"))
                    If Not ddlShippingZone.Items.FindByValue(lblShippingZoneID.Text) Is Nothing Then
                        ddlShippingZone.Items.FindByValue(lblShippingZoneID.Text).Selected = True
                    End If


                    Dim lnkbtnUpdt As LinkButton
                    txt = e.Item.FindControl("txtEState")
                    txtAv = e.Item.FindControl("txtEAbbreviations")
                    lnkbtnUpdt = e.Item.FindControl("lnkbtnUpdt")
                    lnkbtnUpdt.Attributes.Add("onclick", "return ESave('" & txt.ClientID & "','" & ddl.ClientID & "','" & txtAv.ClientID & "')")
                End If
                If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
                    Dim btn As Button
                    btn = e.Item.FindControl("btnDelete")
                    btn.Attributes.Add("onclick", "return DeleteRecord()")
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub dgCountry_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgCountry.ItemCommand
            Try
                If e.CommandName = "Cancel" Then
                    dgCountry.EditItemIndex = e.Item.ItemIndex
                    dgCountry.EditItemIndex = -1
                    Call BindGrid()
                End If
                If e.CommandName = "Delete" Then
                    Dim objUserAccess As New UserAccess
                    objUserAccess.StateID = CType(e.Item.FindControl("lblStateID"), Label).Text
                    objUserAccess.DomainID = Session("DomainID")
                    If Not objUserAccess.DelState = True Then litMessage.Text = "Depenedent Record exists.Cannot be deleted."
                    Call BindGrid()
                End If
                If e.CommandName = "Update" Then
                    Dim objUserAccess As New UserAccess
                    objUserAccess.StateID = CType(e.Item.FindControl("lblStateID"), Label).Text
                    objUserAccess.StateName = CType(e.Item.FindControl("txtEState"), TextBox).Text
                    objUserAccess.vcAbbreviations = CType(e.Item.FindControl("txtEAbbreviations"), TextBox).Text
                    objUserAccess.Country = CType(e.Item.FindControl("ddlECountry"), DropDownList).SelectedItem.Value
                    objUserAccess.ShippingZoneID = CType(e.Item.FindControl("ddlShippingZone"), DropDownList).SelectedItem.Value
                    objUserAccess.DomainID = Session("DomainID")
                    objUserAccess.UserId = Session("UserID")
                    objUserAccess.ManageState()
                    dgCountry.EditItemIndex = -1
                    Call BindGrid()
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub
     
    End Class
End Namespace
