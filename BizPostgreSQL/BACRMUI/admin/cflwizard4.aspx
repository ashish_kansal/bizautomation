<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" MasterPageFile="~/common/Popup.Master" Codebehind="cflwizard4.aspx.vb" Inherits="BACRM.UserInterface.Admin.cflwizard4" %>
<%@ OutputCache Duration="1" VaryByParam="*" Location="None" %>



<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
			<title>Custom Field Wizard</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
   
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageTitle" runat="server" ClientIDMode="Static">
    Step 4
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="Content" runat="server" ClientIDMode="Static">
 		<table id="Table1"  cellspacing="0" cellpadding="0" height = "100%"  width="400px" border="0">
				<tr>
					<td width="5" bgColor="#506490">&nbsp;</td>
					
					<td background="../images/left_side_pic.gif" colSpan="3" height="20"><font face="Arial" color="#ffffff" size="2"><b>&nbsp;&nbsp;Step&nbsp;4 
								of 6 - Specify Label</b></font></td>
				</tr>
				<tr>
					<!-- <td width="20%"><IMG src="../images/pic-2.jpg"></td> -->
					<td width="5" bgColor="#506490">&nbsp;</td>
					<td vAlign="top">
						<table id="Table2">
							<tr>
								<td width="80%" class="normal2"><br>
									&nbsp;&nbsp;Specify a label for your custom field.<br>
									<br/>
								</td>
							</tr>
							<tr>
								<td width="80%">&nbsp;&nbsp;
									<br/>
									<br/>
								</td>
							</tr>
							<tr>
								<td>
									<table>
										<tr>
											<td valign="top" class="Text_Bold">Specify Label &nbsp;</td>
											<td><asp:textbox id="CFWlbl" runat="server" MaxLength="16" cssclass="signup"></asp:textbox><br>
												<br/>
											</td>
											<td></td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td></td>
							</tr>
							<tr>
								<td></td>
							</tr>
							<tr>
								<td align="center"><br/>
									&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp; <A href="cflwizard3.aspx"><IMG src="../images/button_back.jpg" border="0"></A><asp:imagebutton id="Imagnext" runat="server" ImageUrl="../images/button_next.jpg"></asp:imagebutton><A href="javascript:window.close()"><IMG src="../images/button_cancel.jpg" border="0"></A></td>
							</tr>
						</table>
					</td>
					<td width="5" bgColor="#506490">&nbsp;</td>
				</tr>
				<tr>
					<td style="HEIGHT: 5px" bgColor="#506490" colSpan="3"></td>
				</tr>
			</table>
</asp:Content>
