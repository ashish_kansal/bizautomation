﻿Imports BACRM.BusinessLogic.Accounting
Imports BACRM.BusinessLogic.Common
Partial Public Class frmContactTypeMapping
    Inherits BACRMPage
    Dim lngMappingID As Long
   

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            GetUserRightsForPage(13, 1)
            If m_aryRightsForPage(RIGHTSTYPE.VIEW) = 0 Then
                Response.Redirect("../admin/authentication.aspx?mesg=AS")
            ElseIf m_aryRightsForPage(RIGHTSTYPE.ADD) = 0 Then
                btnSave.Visible = False
            End If

            If Not IsPostBack Then
                BindDropdown()
                
            End If
            If Not ViewState("MappingID") Is Nothing Then
                lngMappingID = ViewState("MappingID")
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
    Private Sub BindDropdown()
        Try
            Dim objCOA As New ChartOfAccounting
            objCOA.DomainId = Session("DomainId")
            objCOA.AccountCode = "010201"
            Dim dtChartAcntDetails As DataTable = objCOA.GetParentCategory()
            Dim item As ListItem
            For Each dr As DataRow In dtChartAcntDetails.Rows
                item = New ListItem()
                item.Text = dr("vcAccountName")
                item.Value = dr("numAccountID")
                item.Attributes("OptionGroup") = dr("vcAccountType")
                ddlAccount.Items.Add(item)
            Next
            ddlAccount.Items.Insert(0, New ListItem("--Select One --", "0"))

            objCommon.sb_FillComboFromDBwithSel(ddlContactType, 8, Session("DomainID"))
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            Dim objCOA As New ChartOfAccounting
            With objCOA
                .MappingID = lngMappingID
                .ContactTypeID = ddlContactType.SelectedValue
                .AccountId = ddlAccount.SelectedValue
                .DomainId = Session("DomainID")
                lngMappingID = .ManageContactTypeMapping()
            End With
            BindData()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
    Private Sub ddlContactType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlContactType.SelectedIndexChanged
        Try
            ddlAccount.ClearSelection()
            ddlAccount.SelectedIndex = -1
            lngMappingID = 0
            ViewState("MappingID") = 0
            BindData()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
    Private Sub BindData()
        Try
            Dim objCOA As New ChartOfAccounting
            Dim ds As DataSet
            With objCOA
                .MappingID = lngMappingID
                .ContactTypeID = ddlContactType.SelectedValue
                .DomainId = Session("DomainID")
                ds = .GetContactTypeMapping()
                If ds.Tables(0).Rows.Count > 0 Then
                    ViewState("MappingID") = ds.Tables(0).Rows(0).Item("numMappingID")
                    If Not IsDBNull(ds.Tables(0).Rows(0).Item("numAccountId")) Then
                        If Not ddlAccount.Items.FindByValue(ds.Tables(0).Rows(0).Item("numAccountId")) Is Nothing Then
                            ddlAccount.Items.FindByValue(ds.Tables(0).Rows(0).Item("numAccountId")).Selected = True
                        End If
                    End If
                End If
            End With
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
End Class