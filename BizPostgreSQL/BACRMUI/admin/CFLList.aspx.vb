Imports System.Collections.Generic
Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Common
Imports Telerik.Web.UI

Namespace BACRM.UserInterface.Admin
    Public Class CFLList
        Inherits BACRMPage

        Dim SortField As String
        Dim m_lngStartFrom As Long
#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Try
                DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ""
                DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "none")
                If GetQueryStringVal("RcNo") <> "" Then m_lngStartFrom = GetQueryStringVal("RcNo")
                GetUserRightsForPage(13, 10)

                If Not IsPostBack Then
                    If m_aryRightsForPage(RIGHTSTYPE.ADD) = 0 Then
                        btnCreateFields.Visible = False
                    End If
                    LoadLocation()
                    BindCflList()
                End If
                btnCreateFields.Attributes.Add("onclick", "return fldwiz()")
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub
        Private Sub DisplayError(ByVal ex As String)
            DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ex
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
        End Sub
        Sub SaveCustomFieldList()
            Dim strData As String
            strData = ""
            For Each dr As DataGridItem In dgCustomFields.Items
                Dim rcbItems As RadComboBox
                rcbItems = dr.FindControl("rcbItems")
                Dim lblFldId As Label
                lblFldId = dr.FindControl("lblFldId")
                strData = strData & "_" & lblFldId.Text & "#" & GenerateCheckedItemString(rcbItems)
            Next
            Dim objCusField As New CustomFields()
            objCusField.DomainID = Session("DomainID")
            objCusField.strDetails = strData
            objCusField.SaveCustomFldsByLocationRecId()
        End Sub
        Sub BindCflList()
            Try
                Dim dtCustFld As DataTable
                Dim objCusField As New CustomFields()
                objCusField.DomainID = Session("DomainID")
                objCusField.locId = ddlFilter.SelectedValue
                dtCustFld = objCusField.CustomFieldList
                Dim dv As DataView = New DataView(dtCustFld)
                If SortField <> "" Then dv.Sort = SortField & IIf(Session("Asc") = 0, " Asc", " Desc")
                dgCustomFields.DataSource = dv
                dgCustomFields.DataBind()
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        Function GenerateCheckedItemString(ByVal radComboBox As RadComboBox) As String
            Dim strTransactionType As String
            For Each item As RadComboBoxItem In radComboBox.CheckedItems
                If CCommon.ToLong(item.Value) > 0 Then
                    strTransactionType = strTransactionType & item.Value & ","
                End If
            Next
            Return strTransactionType
        End Function
        Sub LoadLocation()
            Try
                Dim dtRelationship As DataTable
                Dim ObjCusFlds As New CustomFields
                ObjCusFlds.DomainID = Session("DomainID")
                dtRelationship = ObjCusFlds.GetAdminRelationship
                ddlFilter.DataSource = dtRelationship
                ddlFilter.DataTextField = "loc_name"
                ddlFilter.DataValueField = "loc_id"
                ddlFilter.DataBind()
                ddlFilter.Items.Insert(0, "--Select One--")
                ddlFilter.Items.FindByText("--Select One--").Value = 0
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub dgCustomFields_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgCustomFields.ItemCommand
            If e.CommandName = "Delete" Then
                Try
                    Dim objCusField As New CustomFields
                    Dim lblFldId As Label
                    lblFldId = e.Item.FindControl("lblFldId")
                    With objCusField
                        .CusFldId = lblFldId.Text
                        .DomainID = Session("DomainID")
                        .DeleteCusFld()
                    End With
                    BindCflList()
                Catch ex As Exception
                    ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                    DisplayError(ex.Message)
                End Try
            End If
        End Sub

        Private Sub dgCustomFields_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgCustomFields.ItemDataBound
            If e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.Item Then
                Try
                    Dim lblFldId As New Label
                    Dim hplFldName As HyperLink
                    Dim btnDelete As LinkButton
                    Dim lnkDelete As LinkButton
                    Dim rcbItems As RadComboBox
                    Dim hdnCustomFieldItemLocation As HiddenField
                    lnkDelete = e.Item.FindControl("lnkDelete")
                    btnDelete = e.Item.FindControl("btnDelete")
                    lblFldId = e.Item.FindControl("lblFldId")
                    hplFldName = e.Item.FindControl("hplFldName")
                    rcbItems = e.Item.FindControl("rcbItems")
                    hdnCustomFieldItemLocation = e.Item.FindControl("hdnCustomFieldItemLocation")

                    If m_aryRightsForPage(RIGHTSTYPE.UPDATE) <> 0 Then
                        hplFldName.Attributes.Add("onclick", "return OpenSumm(" & lblFldId.Text & ")")
                    Else : hplFldName.Attributes.Add("onclick", "return EditMessage()")
                    End If

                    If m_aryRightsForPage(RIGHTSTYPE.DELETE) = 0 Then
                        btnDelete.Visible = False
                        lnkDelete.Visible = True
                        lnkDelete.Attributes.Add("onclick", "return DeleteMessage()")
                    ElseIf m_aryRightsForPage(RIGHTSTYPE.DELETE) > 0 Then
                        btnDelete.Attributes.Add("onclick", "return DeleteRecord()")
                    End If

                    If ddlFilter.SelectedValue = "5" Then
                        rcbItems.Visible = True
                        Dim listSelectedUsers As List(Of String)
                        listSelectedUsers = CCommon.ToString(hdnCustomFieldItemLocation.Value).Split(",").ToList()

                        For Each dr As String In listSelectedUsers
                            Dim radComboItem As RadComboBoxItem
                            radComboItem = rcbItems.FindItemByValue(dr)
                            If radComboItem IsNot Nothing Then
                                radComboItem.Checked = True
                            End If
                        Next
                    Else
                        rcbItems.Visible = False
                    End If
                    If (DataBinder.Eval(e.Item.DataItem, "fld_type")) = "SelectBox" Or (DataBinder.Eval(e.Item.DataItem, "fld_type")) = "CheckBoxList" Then
                        e.Item.Cells(2).Text = DataBinder.Eval(e.Item.DataItem, "fld_type") & String.Format(" <a href='frmMasterList.aspx?ModuleId=8&ListId={0}' class='hyperlink'>Edit Values</a>", DataBinder.Eval(e.Item.DataItem, "numlistid"))
                    End If

                    Dim str As String = ""

                    If (DataBinder.Eval(e.Item.DataItem, "bitIsRequired")) Then
                        str += ",Required"
                    End If

                    If (DataBinder.Eval(e.Item.DataItem, "bitIsEmail")) Then
                        str += ",Email"
                    ElseIf (DataBinder.Eval(e.Item.DataItem, "bitIsAlphaNumeric")) Then
                        str += ",Alpha Numeric"
                    ElseIf (DataBinder.Eval(e.Item.DataItem, "bitIsNumeric")) Then
                        str += ",Numeric"
                    ElseIf (DataBinder.Eval(e.Item.DataItem, "bitIsLengthValidation")) Then
                        str += ",Range Validation"
                    End If

                    e.Item.Cells(5).Text = str.Trim(",")
                Catch ex As Exception
                    Throw ex
                End Try
            End If
        End Sub

        Private Sub dgCustomFields_SortCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles dgCustomFields.SortCommand
            Try
                SortField = e.SortExpression
                If Session("Asc") = 0 Then
                    Session("Asc") = 1

                Else : Session("Asc") = 0
                End If
                BindCflList()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Sub ddlFilter_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlFilter.SelectedIndexChanged
            Try
                BindCflList()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Protected Sub btnSaveCustomField_Click(sender As Object, e As EventArgs)
            SaveCustomFieldList()
        End Sub
    End Class
End Namespace