﻿Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Item

Public Class frmConfEmployeeLocation
    Inherits BACRMPage
#Region "Member Variables"
    Private dtWareHouse As DataTable
    Private objUserAccess As UserAccess
#End Region
#Region "Page Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Try
                LoadWareHouse()
                BindData()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End If
    End Sub
#End Region
#Region "Private Methods"
    Private Sub BindData()
        Try
            Dim objUserList As New UserGroups

            With objUserList
                .UserId = Session("UserID")
                .DomainID = Session("DomainID")
                .CurrentPage = 1
                .PageSize = 1000
                .TotalRecords = 0
                .BitStatus = 1
                .SortCharacter = "0"
                .TotalRecords = 0
                .ColumnName = "Name"
                .columnSortOrder = "Asc"
                .BitStatus = 1
            End With

            Dim dtUserList As DataTable = objUserList.GetUserList()

            dlEmployeeLocation.DataSource = dtUserList
            dlEmployeeLocation.DataBind()

        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Private Sub LoadWareHouse()
        Try
            Dim objItem As New CItems
            objItem.DomainID = Session("DomainID")
            dtWareHouse = objItem.GetWareHouses
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Private Sub Save()
        Try
            objUserAccess = New UserAccess

            For Each dli As DataListItem In dlEmployeeLocation.Items
                Dim hfEmployeeID As HiddenField
                hfEmployeeID = DirectCast(dli.FindControl("hfEmplyeeID"), HiddenField)

                Dim ddlLocation As DropDownList
                ddlLocation = DirectCast(dli.FindControl("ddlLocation"), DropDownList)

                If Not hfEmployeeID Is Nothing AndAlso Not ddlLocation Is Nothing Then
                    objUserAccess.UserId = CCommon.ToLong(hfEmployeeID.Value)
                    objUserAccess.DefaultWarehouse = CCommon.ToInteger(ddlLocation.SelectedValue)
                    objUserAccess.UpdateDefaultWarehouse()
                End If
            Next
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
#End Region
#Region "Event Handlers"
    Protected Sub btnSaveClose_Click(sender As Object, e As EventArgs) Handles btnSaveClose.Click
        Try
            Save()
            ClientScript.RegisterClientScriptBlock(Me.GetType, "Script", "Close();", True)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
    Private Sub btnGo1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGo1.Click
        Try
            BindData()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
    Protected Sub dlEmployeeLocation_ItemDataBound(sender As Object, e As DataListItemEventArgs) Handles dlEmployeeLocation.ItemDataBound
        Try
            If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then

                Dim drv As DataRowView
                drv = e.Item.DataItem

                Dim hfEmployeeID As HiddenField
                hfEmployeeID = DirectCast(e.Item.FindControl("hfEmplyeeID"), HiddenField)

                Dim lblEmployee As Label
                lblEmployee = DirectCast(e.Item.FindControl("lblEmployeeName"), Label)

                Dim ddlLocation As DropDownList
                ddlLocation = DirectCast(e.Item.FindControl("ddlLocation"), DropDownList)

                If Not hfEmployeeID Is Nothing Then
                    hfEmployeeID.Value = drv("numUserID")
                    'ddlLocation.ID = ddlLocation.ID + drv("numUserID")
                End If

                If Not lblEmployee Is Nothing Then
                    lblEmployee.Text = drv("Name")
                End If

                If Not ddlLocation Is Nothing Then
                    ddlLocation.DataSource = dtWareHouse
                    ddlLocation.DataTextField = "vcWareHouse"
                    ddlLocation.DataValueField = "numWareHouseID"
                    ddlLocation.DataBind()
                    ddlLocation.Items.Insert(0, New ListItem("--Select One--", "0"))
                    If Not drv("numDefaultWarehouse") Is Nothing AndAlso Not drv("numDefaultWarehouse") = "0" Then
                        ddlLocation.SelectedValue = drv("numDefaultWarehouse")
                    End If

                End If

                'If Not rfvLocation Is Nothing Then
                '    rfvLocation.ID = rfvLocation.ID + drv("numUserID")
                '    rfvLocation.ControlToValidate = ddlLocation.ID + drv("numUserID")
                'End If
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
#End Region
End Class