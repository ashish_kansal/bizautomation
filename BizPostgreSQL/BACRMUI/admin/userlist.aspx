<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" CodeBehind="userlist.aspx.vb"
    Inherits="BACRM.UserInterface.Admin.frmUserList" MasterPageFile="~/common/GridMasterRegular.Master" %>

<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Register Assembly="AspNetPager, Version=7.4.4.0, Culture=neutral, PublicKeyToken=fb0a0fe055d40fd4" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>User List</title>
    <meta http-equiv="Page-Enter" content="blendTrans(Duration=0.01)">
    <meta http-equiv="Page-Exit" content="blendTrans(Duration=0.01)">
    <script type="text/javascript">
        var arrFieldConfig = new Array();
        arrFieldConfig[0] = new InlineEditValidation('858', 'False', 'False', 'False', 'False', 'False', 'False', '0', '0', 'e-commerce password');
        //LOCAL DB FIELD ID 40842

        $(document).ready(function () {
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_pageLoaded(pageLoaded);
        });

        function pageLoaded() {
            $("[id$=dgUsers] tr:first input:text").keydown(function (event) {
                if (event.keyCode == 13) {
                    filterInternalUsers();
                    return false;
                }
            });

            $('[id$=dgUsers] tr:first select').change(function () {
                filterInternalUsers();
            });

            $("[id$=dgExternalAccess] tr:first input:text").keydown(function (event) {
                if (event.keyCode == 13) {
                    filterExternalUsers();
                    return false;
                }
            });

            $('[id$=dgExternalAccess] tr:first select').change(function () {
                filterExternalUsers();
            });

            $("#hplClearGridCondition").click(function () {
                $("#hdnInternalUserFilter").val("");
                $('#txtSortColumnA').val("");
                $('#hdnExternalUserFilter').val("");
                $('#txtSortColumnE').val("");
                $('#btnGo1').trigger('click');
            });
        }

        function filterInternalUsers() {
            var filter_condition = '';

            var dropDownControls = $('[id$=dgUsers] tr:first select');
            dropDownControls.each(function () {
                if ($(this).get(0).selectedIndex > 0) {
                    var ddId = $(this).attr("id");
                    var ddValue = $(this).val();
                    filter_condition += ddId + ':' + ddValue + ';';
                }
            }
            )

            var textBoxControls = $('[id$=dgUsers] tr:first input:text');
            textBoxControls.each(function () {
                if ($.trim($(this).val()).length > 0 && !$(this).hasClass("rcbInput")) {
                    var txtId = $(this).attr("id");
                    var txtValue = $.trim($(this).val());
                    filter_condition += txtId + ':' + txtValue + ';';
                }
            }
            )

            $('#hdnInternalUserFilter').val(filter_condition);
            $('#txtCurrrentPage').val('1');
            $('#btnGo1').trigger('click');
        }

        function filterExternalUsers() {
            var filter_condition = '';

            var dropDownControls = $('[id$=dgExternalAccess] tr:first select');
            dropDownControls.each(function () {
                if ($(this).get(0).selectedIndex > 0) {
                    var ddId = $(this).attr("id");
                    var ddValue = $(this).val();
                    filter_condition += ddId + ':' + ddValue + ';';
                }
            }
            )

            var textBoxControls = $('[id$=dgExternalAccess] tr:first input:text');
            textBoxControls.each(function () {
                if ($.trim($(this).val()).length > 0 && !$(this).hasClass("rcbInput")) {
                    var txtId = $(this).attr("id");
                    var txtValue = $.trim($(this).val());
                    filter_condition += txtId + ':' + txtValue + ';';
                }
            }
            )

            $('#hdnExternalUserFilter').val(filter_condition);
            $('#txtCurrrentPage').val('1');
            $('#btnGo1').trigger('click');
        }

        function Goback() {
            //history.back();
            window.location.href = "../admin/frmAdminSection.aspx";
            return false;
        }
        function NoMoreUsers() {
            alert('You Cannot create more users since you have exceeded the limit')
            return false;
        }
        function GoAuthorization() {
            window.location.href = "../admin/usergroups.aspx";
            return false;
        }
        function fnEditUser(UserID) {
            strUrl = "userdetails.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&UserID=" + UserID
            window.open(strUrl, "", "width=745,height=375,status=no,scrollbars=1,left=155,top=100");
            return false;
        }
        function fnExtUser(intExtID) {
            window.open("../Extranet/frmExtranetUsers.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&Extranet=" + intExtID, "", "width=800,height=500,status=no,scrollbars=yes,left=155,top=160");
            return false;
        }
        function OpenHelpInternalUsers() {
            window.open('../Help/Admin-Internal_Users.html', '', 'toolbar=no,titlebar=no,top=200,left=200,width=550,height=350,scrollbars=yes,resizable=yes')
            return false
        }
        function OpenHelpExternalUsers() {
            window.open('../Help/External_Users.html', '', 'toolbar=no,titlebar=no,top=200,left=200,width=550,height=350,scrollbars=yes,resizable=yes')
            return false
        }

        function ConfirmDaysOffDelete() {
            return confirm("Days off will be removed for employee. Do you want to proceed?");
        }

        function chkAllEcommerceChanged(chk) {
            $("[id$=dgExternalAccess] tr").not(":first").each(function () {
                if ($(this).find("[id$=chkEcommerce]") != null) {
                    $(this).find("[id$=chkEcommerce]").prop("checked", $(chk).is(":checked"));
                }
            });
        }

        function chkAllPortalChanged(chk) {
            $("[id$=dgExternalAccess] tr").not(":first").each(function () {
                if ($(this).find("[id$=chkPortal]") != null) {
                    $(this).find("[id$=chkPortal]").prop("checked", $(chk).is(":checked"));
                }
            });
        }
    </script>
    <style>
        .tblDataGrid tr:first-child td {
            background: #e5e5e5;
        }

        #tblWorkSchedule .RadInput_Default .riTextBox, #tblWorkSchedule .RadInputMgr_Default {
            border-color: #d2d6de;
        }

        #tblWorkSchedule .RadInput table td.riCell {
            padding-right: 0px;
        }

        #tblWorkSchedule .riSpin {
            background-color: #dce3ea !important;
        }

        #tblWorkSchedule .RadInput a.riDown {
            margin-top: 9px;
        }

        #tblWorkSchedule th, #tblWorkSchedule td {
            border-color: #d2d2d2 !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server" ClientIDMode="Static">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <div class="row padbottom10">
        <div class="col-md-4">
            <div class="form-inline">
                <label>Full Users:</label>
                <asp:Label ID="lblFullUsers" runat="server" CssClass="text"></asp:Label>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-inline">
                <label>Limited Access Users:</label>
                <asp:Label ID="lblLimitedAccessUsers" runat="server" CssClass="text"></asp:Label>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-inline">
                <label>Business Portal Users:</label>
                <asp:Label ID="lblBusinessPortalUsers" runat="server" CssClass="text"></asp:Label>
            </div>
        </div>
    </div>
    <asp:UpdateProgress ID="UpdateProgress" runat="server" ClientIDMode="Static">
        <ProgressTemplate>
            <div class="overlay">
                <div class="overlayContent" style="color: #000; text-align: center; width: 250px; padding: 20px">
                    <i class="fa fa-2x fa-refresh fa-spin"></i>
                    <h3>Processing Request</h3>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server" ClientIDMode="Static">
    Administration&nbsp;<a href="#" onclick="return OpenHelpPopUp('admin/userlist.aspx')"><label class="badge bg-yellow">?</label></a>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BizPager" runat="server" ClientIDMode="Static">
    <webdiyer:AspNetPager ID="bizPager" runat="server"
        PagingButtonSpacing="0"
        CssClass="bizgridpager"
        AlwaysShow="true"
        CurrentPageButtonClass="active"
        PagingButtonUlLayoutClass="pagination"
        PagingButtonLayoutType="UnorderedList"
        FirstPageText="<<"
        LastPageText=">>"
        NextPageText=">"
        PrevPageText="<"
        Width="100%"
        ShowPageIndexBox="Never"
        UrlPaging="false"
        NumericButtonCount="8"
        ShowCustomInfoSection="Left"
        OnPageChanged="bizPager_PageChanged"
        CustomInfoHTML="Showing records %StartRecordIndex% to %EndRecordIndex% of %RecordCount% "
        CustomInfoClass="bizpagercustominfo">
    </webdiyer:AspNetPager>
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="GridSettingPopup" runat="server" ClientIDMode="Static">
    <a href="#" id="hplClearGridCondition" title="Clear All Filters" class="btn-box-tool"><i class="fa fa-2x fa-filter"></i></a>
</asp:Content>

<asp:Content ID="Content6" ContentPlaceHolderID="GridPlaceHolder" runat="server"
    ClientIDMode="Static">
    <div class="row">
        <div class="col-xs-12">
            <telerik:RadTabStrip ID="radOppTab" runat="server" UnSelectChildren="True" EnableEmbeddedSkins="true"
                Skin="Default" ClickSelectedTab="True" AutoPostBack="True" MultiPageID="radMultiPage_OppTab">
                <Tabs>
                    <telerik:RadTab Value="SubscriberAdministration"
                        PageViewID="radPageView_SubscriberAdministration">
                        <TabTemplate>
                            Subscriber Administration <a href="#" onclick="return OpenHelpInternalUsers()">
                                <label class="badge bg-yellow">?</label></a>
                        </TabTemplate>
                    </telerik:RadTab>
                    <telerik:RadTab Value="ExternalcompanyAccess" PageViewID="radPageView_ExternalcompanyAccess">
                        <TabTemplate>
                            External company Access <a href="#" onclick="return OpenHelpExternalUsers()">
                                <label class="badge bg-yellow">?</label></a>
                        </TabTemplate>
                    </telerik:RadTab>
                    <telerik:RadTab Value="WorkSchedules" PageViewID="radPageView_WorkSchedules" Style="line-height: 23px !important;">
                        <TabTemplate>
                            Work Schedules
                        </TabTemplate>
                    </telerik:RadTab>
                </Tabs>
            </telerik:RadTabStrip>
            <telerik:RadMultiPage ID="radMultiPage_OppTab" runat="server" SelectedIndex="0" CssClass="pageView">
                <telerik:RadPageView ID="radPageView_SubscriberAdministration" runat="server">
                    <div class="row padbottom10">
                        <div class="col-xs-12">
                            <div class="pull-left">
                                <div class="col-xs-12">
                                    <div class="form-inline">
                                        <div class="form-group">
                                            <label>Universal SMTP Server URL:</label>
                                            <asp:TextBox runat="server" ID="txtUniversalSMTPURL" CssClass="form-control" Width="170"></asp:TextBox>
                                        </div>
                                        <div class="form-group">
                                            <label>Port:</label>
                                            <asp:TextBox runat="server" ID="txtUniversalSMTPPort" CssClass="form-control" Width="60"></asp:TextBox>
                                        </div>
                                        <div class="form-group">
                                            <label>SSL:</label>
                                            <asp:CheckBox runat="server" ID="chkUniversalSMTPSSL"></asp:CheckBox>
                                        </div>
                                        <div class="form-group">
                                            <label>&nbsp;&nbsp;&nbsp;</label>
                                        </div>
                                        <div class="form-group">
                                            <label>Username:</label>
                                            <asp:CheckBox runat="server" ID="chkUniversalSMTPUserAuthentication"></asp:CheckBox>
                                        </div>
                                        <div class="form-group">
                                            <label>Universal IMAP Server URL:</label>
                                            <asp:TextBox runat="server" ID="txtUniversalIMAPURL" CssClass="form-control" Width="170"></asp:TextBox>
                                        </div>
                                        <div class="form-group">
                                            <label>Port:</label>
                                            <asp:TextBox runat="server" ID="txtUniversalIMAPPort" CssClass="form-control" Width="60"></asp:TextBox>
                                        </div>
                                        <div class="form-group">
                                            <label>SSL:</label>
                                            <asp:CheckBox runat="server" ID="chkUniversalIMAPSSL"></asp:CheckBox>
                                        </div>
                                        <div class="form-group">
                                            <label>Username:</label>
                                            <asp:CheckBox runat="server" ID="chkUniversalIMAPUserAuthentication"></asp:CheckBox>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <div class="pull-right">
                                <asp:Button ID="btnSaveChanges" runat="server" CssClass="btn btn-primary" Text="Save" OnClick="btnSaveChanges_Click" />
                            </div>
                        </div>
                    </div>
                    <div class="table-responsive">

                        <asp:DataGrid ID="dgUsers" runat="server" Width="100%" CssClass="table table-bordered table-striped" AllowSorting="true"
                            AutoGenerateColumns="False" UseAccessibleHeader="true" OnItemCommand="dgUsers_ItemCommand">
                            <Columns>
                                <asp:BoundColumn DataField="numUserID" Visible="false"></asp:BoundColumn>
                                <asp:TemplateColumn>
                                    <HeaderTemplate>
                                        <asp:LinkButton ID="lbUserName" runat="server" CommandName="Sort" Text="User Name" CommandArgument="UserName"></asp:LinkButton><br />
                                        <asp:TextBox runat="server" ClientIDMode="Static" ID="txtUsernameHeader" CssClass="form-control"></asp:TextBox>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <%# Eval("Name")%>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderStyle-Wrap="false" ItemStyle-Wrap="false">
                                    <HeaderTemplate>
                                        <asp:LinkButton ID="lbEmailID" runat="server" CommandName="Sort" Text="Email" CommandArgument="Email"></asp:LinkButton><br />
                                        <asp:TextBox runat="server" ClientIDMode="Static" ID="txtEmailHeader" CssClass="form-control"></asp:TextBox>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <%# Eval("vcEmailID")%>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:BoundColumn DataField="vcGroupName" HeaderText="Group" HeaderStyle-Wrap="false" ItemStyle-Wrap="false"></asp:BoundColumn>
                                <asp:TemplateColumn HeaderStyle-Wrap="false" ItemStyle-Wrap="false">
                                    <HeaderTemplate>
                                        Status<br />
                                        <asp:DropDownList runat="server" ID="ddlStatusHeader" CssClass="form-control">
                                            <asp:ListItem Text="All" Value="-1"></asp:ListItem>
                                            <asp:ListItem Text="Active" Value="1"></asp:ListItem>
                                            <asp:ListItem Text="Suspended" Value="0"></asp:ListItem>
                                        </asp:DropDownList>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <ul class="list-inline">
                                            <li><%# Eval("Active")%></li>
                                            <li>
                                                <asp:CheckBox ID="chkActive" runat="server" Checked='<%# Eval("bitActivateFlag") %>' /></li>
                                        </ul>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderStyle-Wrap="false" ItemStyle-Wrap="false">
                                    <HeaderTemplate>
                                        <asp:LinkButton ID="lbLoginActivity" runat="server" Text="Log-in Activity" CommandName="Sort" CommandArgument="LoginActivity"></asp:LinkButton>
                                        <br />
                                        <asp:DropDownList ID="ddlLoginHeader" ClientIDMode="Static" runat="server" CssClass="form-control">
                                            <asp:ListItem Text="All" Value="0"></asp:ListItem>
                                            <asp:ListItem Text="Within last 7 days" Value="1"></asp:ListItem>
                                            <asp:ListItem Text="Within last 30 days" Value="2"></asp:ListItem>
                                            <asp:ListItem Text="Hasn�t logged in within 30 days" Value="3"></asp:ListItem>
                                            <asp:ListItem Text="Hasn�t logged in within 60 days" Value="4"></asp:ListItem>
                                            <asp:ListItem Text="Hasn�t logged in ever" Value="5"></asp:ListItem>
                                        </asp:DropDownList>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <%# Eval("intLoginCount") & " times" %>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderStyle-Wrap="false" ItemStyle-Wrap="false" HeaderText="E-mail Alias">
                                    <ItemTemplate>
                                        <asp:HiddenField runat="server" ID="hdnUserID" Value='<%# Eval("numUserID") %>' />
                                        <asp:TextBox ID="txtEmailAlias" CssClass="form-control" Width="220" runat="server" Text='<%# Eval("vcEmailAlias")%>' />
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="SMTP & IMAP Password" HeaderStyle-Wrap="false" ItemStyle-Wrap="false">
                                    <ItemTemplate>
                                        <asp:TextBox ID="txtSMTPIMAPPassword" CssClass="form-control" Width="80" runat="server" TextMode="Password" value='<%# Eval("vcEmailAliasPassword")%>'></asp:TextBox>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderStyle-Width="110" ItemStyle-Width="110">
                                    <ItemTemplate>
                                        <ul class="list-inline">
                                            <li>
                                                <asp:Button runat="server" ID="btnTest" Text="Test" CssClass="btn btn-primary" CommandName="Test" CommandArgument='<%# Eval("numUserID") %>' /></li>
                                            <li>
                                                <asp:LinkButton ID="lkbTest" runat="server" Visible="false"><a></a></asp:LinkButton>
                                            </li>
                                        </ul>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                            </Columns>
                        </asp:DataGrid>
                    </div>
                </telerik:RadPageView>
                <telerik:RadPageView ID="radPageView_ExternalcompanyAccess" runat="server">
                    <div class="row padbottom10">
                        <div class="col-xs-12">
                            <div class="pull-right">
                                <asp:Button ID="btnSaveExternal" runat="server" Text="Save" CssClass="btn btn-primary" OnClick="btnSaveExternal_Click" />
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="table-responsive">
                                <asp:DataGrid ID="dgExternalAccess" runat="server" Width="100%" CssClass="table table-bordered table-striped" AutoGenerateColumns="False"
                                    AllowSorting="true" UseAccessibleHeader="true" OnItemCommand="dgExternalAccess_ItemCommand">
                                    <Columns>
                                        <asp:BoundColumn DataField="numExtranetID" Visible="false"></asp:BoundColumn>
                                        <asp:TemplateColumn>
                                            <HeaderTemplate>
                                                <asp:LinkButton ID="lbOrganization" runat="server" CommandName="Sort" Text="Organization" CommandArgument="Organization"></asp:LinkButton><br />
                                                <asp:TextBox runat="server" ClientIDMode="Static" ID="txtOrgnizationHeader" CssClass="form-control"></asp:TextBox>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <%# Eval("vcCompanyName")%>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderStyle-Wrap="false" ItemStyle-Wrap="false">
                                            <HeaderTemplate>
                                                <asp:LinkButton ID="lbFirstName" runat="server" Text="First Name" CommandName="Sort" CommandArgument="FirstName"></asp:LinkButton><br />
                                                <asp:TextBox runat="server" ClientIDMode="Static" ID="txtFirstNameHeader" CssClass="form-control"></asp:TextBox>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <%# Eval("vcFirstName")%>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderStyle-Wrap="false" ItemStyle-Wrap="false">
                                            <HeaderTemplate>
                                                <asp:LinkButton ID="lbLastName" runat="server" Text="Last Name" CommandName="Sort" CommandArgument="LastName"></asp:LinkButton><br />
                                                <asp:TextBox runat="server" ClientIDMode="Static" ID="txtLastNameHeader" CssClass="form-control"></asp:TextBox>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <%# Eval("vcLastName")%>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderStyle-Wrap="false" ItemStyle-Wrap="false">
                                            <HeaderTemplate>
                                                <asp:LinkButton ID="lbEmailAddress" ClientIDMode="Static" runat="server" Text="Email Address" CommandName="Sort" CommandArgument="EmailAddress"></asp:LinkButton>
                                                <br />
                                                <asp:TextBox runat="server" ID="txtEmailHeader" CssClass="form-control"></asp:TextBox>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <%# Eval("vcEmail")%>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderStyle-Wrap="false" ItemStyle-Wrap="false">
                                            <HeaderTemplate>
                                                <asp:LinkButton ID="lbLoginActivity" runat="server" Text="Log-in Activity" CommandName="Sort" CommandArgument="LoginActivity"></asp:LinkButton>
                                                <br />
                                                <asp:DropDownList ID="ddlLoginHeader" ClientIDMode="Static" runat="server" CssClass="form-control">
                                                    <asp:ListItem Text="All" Value="0"></asp:ListItem>
                                                    <asp:ListItem Text="Within last 7 days" Value="1"></asp:ListItem>
                                                    <asp:ListItem Text="Within last 30 days" Value="2"></asp:ListItem>
                                                    <asp:ListItem Text="Hasn�t logged in within 30 days" Value="3"></asp:ListItem>
                                                    <asp:ListItem Text="Hasn�t logged in within 60 days" Value="4"></asp:ListItem>
                                                    <asp:ListItem Text="Hasn�t logged in ever" Value="5"></asp:ListItem>
                                                </asp:DropDownList>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <%# Eval("intLoginCount") & " times" %>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderStyle-Wrap="false">
                                            <HeaderTemplate>
                                                Password
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Label runat="server" ID="lblPassword"></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderStyle-Wrap="false">
                                            <HeaderTemplate>
                                                E-Commerce Access
                                                <input type="checkbox" id="chkAllEcommerce" onchange="chkAllEcommerceChanged(this);" />
                                                <br />
                                                <asp:DropDownList runat="server" ID="ddlEcommerceHeader" CssClass="form-control">
                                                    <asp:ListItem Text="All" Value="-1"></asp:ListItem>
                                                    <asp:ListItem Text="Active" Value="1"></asp:ListItem>
                                                    <asp:ListItem Text="Suspended" Value="0"></asp:ListItem>
                                                </asp:DropDownList>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:CheckBox runat="server" ID="chkEcommerce" />
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderStyle-Wrap="false">
                                            <HeaderTemplate>
                                                Business Portal Access
                                                <input type="checkbox" id="chkAllPortal" onchange="chkAllPortalChanged(this);" />
                                                <br />
                                                <asp:DropDownList runat="server" ID="ddlPortalHeader" CssClass="form-control">
                                                    <asp:ListItem Text="All" Value="-1"></asp:ListItem>
                                                    <asp:ListItem Text="Active" Value="1"></asp:ListItem>
                                                    <asp:ListItem Text="Suspended" Value="0"></asp:ListItem>
                                                </asp:DropDownList>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:CheckBox runat="server" ID="chkPortal" />
                                                <asp:HiddenField runat="server" ID="hdnExtranetDtlID" Value='<%# Eval("numExtranetDtlID")%>' />
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                    </Columns>
                                </asp:DataGrid>
                            </div>
                        </div>
                    </div>
                </telerik:RadPageView>
                <telerik:RadPageView ID="radPageView_WorkSchedules" runat="server">
                    <div class="row padbottom10">
                        <div class="col-xs-12">
                            <div class="pull-left">
                                <div class="form-inline">
                                    <div class="form-group">
                                        <label>From:</label>
                                        <telerik:RadDatePicker runat="server" ID="rdpDaydOffFrom" Width="100" DateInput-DisplayDateFormat="MM/dd/yyyy"></telerik:RadDatePicker>
                                    </div>
                                    <div class="form-group">
                                        <label>To:</label>
                                        <telerik:RadDatePicker runat="server" ID="rdpDaydOffTo" Width="100" DateInput-DisplayDateFormat="MM/dd/yyyy"></telerik:RadDatePicker>
                                    </div>
                                    <asp:Button runat="server" ID="btnAddDaysOff" CssClass="btn btn-primary" Text="Add Days Off" OnClick="btnAddDaysOff_Click" />
                                </div>
                            </div>
                            <div class="pull-right">
                                <asp:Button runat="server" ID="btnSaveWorkSchedule" CssClass="btn btn-primary" Text="Save" OnClick="btnSaveWorkSchedule_Click" />
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="table-responsive">
                                <asp:Repeater ID="rptWorkSchedule" runat="server" OnItemDataBound="rptWorkSchedule_ItemDataBound">
                                    <HeaderTemplate>
                                        <table class="table table-bordered table-striped" id="tblWorkSchedule">
                                            <tr>
                                                <th rowspan="2" style="width: 25px"></th>
                                                <th rowspan="2">Employee</th>
                                                <th rowspan="2" style="width: 137px">Work Schedule</th>
                                                <th colspan="2" style="border-bottom: 0px;">Work</th>
                                                <th colspan="2" style="border-bottom: 0px;">Productive</th>
                                                <th colspan="3" style="border-bottom: 0px;">Start of Day (GMT)</th>
                                                <th rowspan="2">Days Off</th>
                                                <th rowspan="2" style="width: 140px"></th>
                                            </tr>
                                            <tr>
                                                <th style="width: 75px">Hours</th>
                                                <th style="width: 85px">Minutes</th>
                                                <th style="width: 75px">Hours</th>
                                                <th style="width: 85px">Minutes</th>
                                                <th style="width: 75px">Hours</th>
                                                <th style="width: 85px">Minutes</th>
                                                <th style="width: 82px;"></th>
                                            </tr>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tr>
                                            <td>
                                                <asp:CheckBox ID="chkSelectEmployee" runat="server" />
                                                <asp:HiddenField runat="server" ID="hdnWorkScheduleID" Value='<%# Eval("numWorkScheduleID")%>' />
                                                <asp:HiddenField runat="server" ID="hdnUserCntID" Value='<%# Eval("numUserDetailId")%>' />
                                            </td>
                                            <td>
                                                <a href="javascript:void(0)" onclick="return fnEditUser(<%# Eval("numUserID") %>)"><%# Eval("vcUserName")%></a>
                                            </td>
                                            <td>
                                                <telerik:RadComboBox ID="rcbDays" runat="server" Width="120" CheckBoxes="true" AllowCustomText="false" EmptyMessage="-- Select Days --">
                                                    <Items>
                                                        <telerik:RadComboBoxItem Value="2" Text="Monday" />
                                                        <telerik:RadComboBoxItem Value="3" Text="Tuesday" />
                                                        <telerik:RadComboBoxItem Value="4" Text="Wednesday" />
                                                        <telerik:RadComboBoxItem Value="5" Text="Thursday" />
                                                        <telerik:RadComboBoxItem Value="6" Text="Friday" />
                                                        <telerik:RadComboBoxItem Value="7" Text="Saturday" />
                                                        <telerik:RadComboBoxItem Value="1" Text="Sunday" />
                                                    </Items>
                                                </telerik:RadComboBox>
                                            </td>
                                            <td>
                                                <telerik:RadNumericTextBox ID="txtWorkHours" CssClass="form-control" runat="server" MaxValue="24" MinValue="0" ShowSpinButtons="true" NumberFormat-DecimalDigits="0" EmptyMessage="Hours" Width="58"></telerik:RadNumericTextBox></td>
                                            <td>
                                                <telerik:RadNumericTextBox ID="txtWorkMinutes" CssClass="form-control" runat="server" MaxValue="59" MinValue="0" ShowSpinButtons="true" NumberFormat-DecimalDigits="0" EmptyMessage="Minutes" Width="68"></telerik:RadNumericTextBox></td>
                                            <td>
                                                <telerik:RadNumericTextBox ID="txtProductiveHours" CssClass="form-control" runat="server" MaxValue="24" MinValue="0" ShowSpinButtons="true" NumberFormat-DecimalDigits="0" EmptyMessage="Hours" Width="58"></telerik:RadNumericTextBox></td>
                                            <td>
                                                <telerik:RadNumericTextBox ID="txtProductiveMinutes" CssClass="form-control" runat="server" MaxValue="59" MinValue="0" ShowSpinButtons="true" NumberFormat-DecimalDigits="0" EmptyMessage="Minutes" Width="68"></telerik:RadNumericTextBox></td>
                                            <td>
                                                <telerik:RadNumericTextBox ID="txtStartHours" CssClass="form-control" runat="server" MaxValue="12" MinValue="0" ShowSpinButtons="true" NumberFormat-DecimalDigits="0" EmptyMessage="Hours" Width="58"></telerik:RadNumericTextBox></li>
                                            </td>
                                            <td>
                                                <telerik:RadNumericTextBox ID="txtStartMinutes" CssClass="form-control" runat="server" MaxValue="59" MinValue="0" ShowSpinButtons="true" NumberFormat-DecimalDigits="0" EmptyMessage="Minutes" Width="68"></telerik:RadNumericTextBox>
                                            </td>
                                            <td>
                                                <asp:RadioButtonList ID="rblStartTime" runat="server" RepeatLayout="Flow" RepeatDirection="Horizontal">
                                                    <asp:ListItem Text="AM" Value="1" Selected="True"></asp:ListItem>
                                                    <asp:ListItem Text="PM" Value="2"></asp:ListItem>
                                                </asp:RadioButtonList>
                                            </td>
                                            <td>
                                                <%# Eval("vcDaysOff")%>
                                            </td>
                                            <td>
                                                <asp:Button ID="btnRemoveDaysOff" runat="server" CssClass="btn btn-flat btn-danger" Text="Remove Days Off" OnClick="btnRemoveDaysOff_Click" OnClientClick="return ConfirmDaysOffDelete();" CommandArgument='<%# Eval("numWorkScheduleID")%>' />
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        </table>
                                    </FooterTemplate>
                                </asp:Repeater>
                            </div>
                        </div>
                    </div>
                </telerik:RadPageView>
            </telerik:RadMultiPage>
        </div>
    </div>

    <asp:TextBox ID="txtTotalPageAUsers" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtTotalRecordsAUsers" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtTotalPageExternal" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtTotalRecordsExternal" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtCurrrentPage" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtSortColumnA" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtSortOrderA" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtSortColumnE" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtSortOrderE" runat="server" Style="display: none"></asp:TextBox>
    <asp:HiddenField ID="hdnExternalUserFilter" runat="server"></asp:HiddenField>
    <asp:HiddenField ID="hdnInternalUserFilter" runat="server" />
    <asp:Button ID="btnGo1" runat="server" Style="display: none" />
</asp:Content>
