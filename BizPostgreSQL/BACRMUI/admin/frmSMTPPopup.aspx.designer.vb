﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class frmSMTPPopup

    '''<summary>
    '''btnSave control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnSave As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''btnClose control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnClose As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''lblTitle control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblTitle As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''litMessage control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents litMessage As Global.System.Web.UI.WebControls.Literal

    '''<summary>
    '''txtUserName control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtUserName As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtPassWord control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtPassWord As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''tdSender1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents tdSender1 As Global.System.Web.UI.HtmlControls.HtmlTableCell

    '''<summary>
    '''tdSender2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents tdSender2 As Global.System.Web.UI.HtmlControls.HtmlTableCell

    '''<summary>
    '''txtSenderName control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtSenderName As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtServerUrl control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtServerUrl As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtServerPort control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtServerPort As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''chkSSL control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents chkSSL As Global.System.Web.UI.WebControls.CheckBox

    '''<summary>
    '''chkSmtpAuth control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents chkSmtpAuth As Global.System.Web.UI.WebControls.CheckBox

    '''<summary>
    '''pnlSMTPError control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlSMTPError As Global.System.Web.UI.WebControls.Panel
End Class
