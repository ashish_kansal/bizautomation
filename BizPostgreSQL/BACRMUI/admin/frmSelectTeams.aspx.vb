''' -----------------------------------------------------------------------------
''' Project	 : BACRM.UserInterface.Admin
''' Class	 : frmSelectTeams
''' 
''' -----------------------------------------------------------------------------
''' <summary>
'''     This helps filter the Teams for Adv.Search
''' </summary>
''' <remarks>
''' </remarks>
''' <history>
''' 	[Debasish]	08/07/2005	Created
''' </history>
''' -----------------------------------------------------------------------------
Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Common
Imports System.IO
Namespace BACRM.UserInterface.Admin
    Public Class frmSelectTeams
        Inherits BACRMPage


#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub
        Protected WithEvents btnSave As System.Web.UI.WebControls.Button
        Protected WithEvents btnClose As System.Web.UI.htmlControls.HtmlInputButton
        Protected WithEvents tblTeams As System.Web.UI.WebControls.Table
        Protected WithEvents lstTeamAvail As System.Web.UI.WebControls.ListBox
        Protected WithEvents lstTeamSelected As System.Web.UI.WebControls.ListBox
        Protected WithEvents hdXMLString As System.Web.UI.HtmlControls.HtmlInputText
        Protected WithEvents ltClientScript As System.Web.UI.WebControls.Literal

        Dim objTeamSavingFunc As New FormGenericAdvSearch                       'Create an class object which contains the team saving function
        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.
        Private designerPlaceholderDeclaration As System.Object

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This event is fired each time thepage is called. In this event we will 
        '''     get the data from the DB create the form.
        ''' </summary>
        ''' <param name="sender">Represents the sender object.</param>
        ''' <param name="e">Represents the EventArgs.</param>
        ''' <remarks>
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	08/07/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Try
                objTeamSavingFunc.DomainID = Session("DomainID")            'Store the domain id
                objTeamSavingFunc.UserCntID = Session("UserContactID")                'Store the User id
                If Not IsPostBack Then
                    
                    DataBindFormElements()                                  'Call to databind form elements
                    callToBindAdditionalTriggers()                          'Call to bind client side triggers to the form elements
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This function is called to bind the team list to the dropdown list.
        ''' </summary>
        ''' <remarks>
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	08/13/2005	Created
        ''' </history>
        '''-----------------------------------------------------------------------------
        Private Sub DataBindFormElements()
            Try
                Dim objUserAccess As New UserAccess                         'Create an User Access Object
                objUserAccess.UserId = Session("UserId")                    'Set the User Id
                objUserAccess.DomainID = Session("DomainID")                'Set the Domain id

                'Call to get teams assinged to the user and those selected here by the user
                Dim dsUserTeamInfo As New DataSet                           'Declare and instantiate a DataSet
                dsUserTeamInfo = objUserAccess.GetTeamForUsers.DataSet      'Get the Dataset Table 0
                dsUserTeamInfo.Tables(0).TableName = "AvailTeamUsers"       'Set the tablename
                dsUserTeamInfo.Merge(objTeamSavingFunc.getSelectedTeamsForUser().DataSet) 'Call to get teams assigned/selected by the user
                objTeamSavingFunc.RemoveDuplicatesRows(dsUserTeamInfo.Tables(0), dsUserTeamInfo.Tables(1), "numListItemId")

                lstTeamAvail.DataSource = dsUserTeamInfo.Tables("AvailTeamUsers") 'Set the datasource of the team listbox
                lstTeamAvail.DataTextField = "vcData"                       'Set the text for the listbox
                lstTeamAvail.DataValueField = "numListItemId"               'set the value attribute of the listbox
                lstTeamAvail.DataBind()                                     'databind the listbox

                lstTeamSelected.DataSource = dsUserTeamInfo.Tables("SelectedTeamUsers") 'Set the datasource of the team listbox
                lstTeamSelected.DataTextField = "vcData"                    'Set the text for the listbox
                lstTeamSelected.DataValueField = "numListItemId"            'set the value attribute of the listbox
                lstTeamSelected.DataBind()                                  'databind the listbox
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This function is called to attach additional client side events to form controls
        ''' </summary>
        ''' <remarks>
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	08/13/2005	Created
        ''' </history>
        '''-----------------------------------------------------------------------------
        Function callToBindAdditionalTriggers()
            Try
                btnSave.Attributes.Add("onclick", "return validateFormData(document.frmSelectTeams);") 'Attach client side javascript event
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This function saves the selection of teams
        ''' </summary>
        ''' <remarks>
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	08/13/2005	Created
        ''' </history>
        '''-----------------------------------------------------------------------------
        Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
            Try
                Dim sTeamContainingXML As String = Server.HtmlDecode(hdXMLString.Value.ToString().Replace("&amp;lt;", "<").Replace("&amp;gt;", ">"))      'Get the xml string
                If (objTeamSavingFunc.saveTeamsForUser(sTeamContainingXML) > 0) Then    'Call to save the data
                    ltClientScript.Text = "<script language='javascript'>CloseThisWin();</script>" 'Request to close thiw window
                    Exit Sub
                Else : ltClientScript.Text = "<script language='javascript'>alert('Unable to save your preferences of teams, please try again or else report to the Administrator.')</script>" 'Inform about failure to the user
                End If
                DataBindFormElements()                                                  'Call to databind the form elements
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

    End Class
End Namespace

