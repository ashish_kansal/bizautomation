<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmNewTabs.aspx.vb" Inherits=".frmNewTabs"
    MasterPageFile="~/common/Popup.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Tabs</title>
    <script language="javascript" type="text/javascript">
        function NoDeleteMessage() {
            alert("You Are not Authorized to Delete the Selected Record !");
            return false;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <div class="input-part">
        <div class="right-input">
            <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="button"></asp:Button>&nbsp;&nbsp;
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageTitle" runat="server" ClientIDMode="Static">
    Tabs
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="Content" runat="server" ClientIDMode="Static">
    <table width="600px">
        <tr>
            <td class="normal4" align="center" colspan="2">
                <asp:Literal ID="litMessage" runat="server" EnableViewState="False"></asp:Literal>
            </td>
        </tr>
        <tr>
            <td align="right" class="normal1">
                Tab Name
            </td>
            <td>
                <asp:TextBox ID="txtTabName" runat="server" CssClass="signup"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td align="right" class="normal1">
                Group
            </td>
            <td>
                <asp:DropDownList ID="ddlTabType" runat="server" AutoPostBack="true" Width="130"
                    CssClass="signup">
                    <asp:ListItem Text="Internal Application" Value="1"></asp:ListItem>
                    <asp:ListItem Text="Customer Portal" Value="2"></asp:ListItem>
                    <asp:ListItem Text="PRM" Value="3"></asp:ListItem>
                </asp:DropDownList>
                &nbsp;&nbsp;&nbsp;&nbsp;
            </td>
        </tr>
        <tr>
            <td align="right" class="normal1">
                URL
            </td>
            <td>
                <asp:TextBox ID="txtURL" Width="300" runat="server" CssClass="signup"></asp:TextBox>&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:Button ID="btnAdd" runat="server" CssClass="button" Text="Add" Width="50" />
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:DataGrid ID="dgTabs" runat="server" CssClass="dg"  AutoGenerateColumns="False"
                    AllowSorting="True" Width="100%">
                    <AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
                    <ItemStyle CssClass="is"></ItemStyle>
                    <HeaderStyle CssClass="hs"></HeaderStyle>
                    <Columns>
                        <asp:BoundColumn Visible="false" DataField="numTabId"></asp:BoundColumn>
                        <asp:BoundColumn Visible="false" DataField="bitFixed"></asp:BoundColumn>
                        <asp:TemplateColumn>
                            <ItemTemplate>
                                <asp:LinkButton runat="server" Text="Edit" CommandName="Edit" ID="lnkbtnEdt"></asp:LinkButton>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:LinkButton ID="lnkbtnUpdt" runat="server" Text="Update" CommandName="Update"></asp:LinkButton>&nbsp;
                                <asp:LinkButton runat="server" Text="Cancel" CommandName="Cancel" ID="lnkbtnCncl"></asp:LinkButton>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn>
                            <ItemTemplate>
                                <asp:Label ID="lblTabID" runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.numTabId") %>'>
                                </asp:Label>
                                <asp:Label ID="lblFixed" runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.bitFixed") %>'>
                                </asp:Label>
                                <%# Container.ItemIndex + 1%>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Tab">
                            <ItemTemplate>
                                <asp:Label ID="lblTab" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.numTabName") %>'>
                                </asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtETab" runat="server" CssClass="signup" Text='<%# DataBinder.Eval(Container, "DataItem.numTabName") %>'>
                                </asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="URL">
                            <ItemTemplate>
                                <asp:Label ID="lblURL" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.vcURL") %>'>
                                </asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtEURL" runat="server" CssClass="signup" Width="150" Text='<%# DataBinder.Eval(Container, "DataItem.vcURL") %>'>
                                </asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderStyle-Width="10" ItemStyle-Width="10">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkdelete" runat="server" Visible="false">
											        <span style="color:#730000">*</span></asp:LinkButton>
                                <asp:Button ID="btnDelete" runat="server" CssClass="button Delete" Text="X" CommandName="Delete">
                                </asp:Button>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                </asp:DataGrid>
            </td>
        </tr>
    </table>
</asp:Content>
