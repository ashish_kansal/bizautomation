'Modified By Anoop Jayaraj
Imports BACRM.BusinessLogic.Contacts
Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Marketing
Imports BACRM.BusinessLogic.Outlook
Imports BACRM.BusinessLogic.Projects
Imports BACRM.BusinessLogic.Opportunities
Imports BACRM.BusinessLogic.Case
Imports BACRM.BusinessLogic.Alerts

Namespace BACRM.UserInterface.Admin
    Public Class BizDocActionDetails
        Inherits BACRMPage

        Dim lngCommId As Long = 0
        Public lngBizDocActId As Long
        Dim lngContactID As Long = 0
        Dim lngCaseid As Long = 0
        Dim lngCaseTimeId As Long = 0
        Dim lngActivityId As Long = 0
        Dim objCommon As CCommon
        Dim objCampaign As Campaign
        Dim objContacts As CContacts
        Dim objActionItem As ActionItem
        Dim objOutLook As COutlook
        Public BizDocDate As String
#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Load
            Try
                'hplEmpAvaliability.Attributes.Add("onclick", "return openEmpAvailability();")
                'btnFollowup.Attributes.Add("onclick", "return OpenNew();")

                lngBizDocActId = CCommon.ToLong(GetQueryStringVal("BizDocActionId"))
                lngContactID = CCommon.ToLong(GetQueryStringVal("ContactID"))

                If GetQueryStringVal("BizDocDate") <> "" Then
                    cal.SelectedDate = CDate(GetQueryStringVal("BizDocDate"))
                    BizDocDate = cal.SelectedDate
                End If

                If lngBizDocActId <> 0 Then
                    If Not IsPostBack Then
                        objActionItem = New ActionItem
                        objCommon = New CCommon
                        objCommon.sb_FillComboFromDBwithSel(ddlType, 73, Session("DomainID"))
                        objCommon.sb_FillComboFromDBwithSel(ddlStatus, 33, Session("DomainID"))
                        Dim lst As New ListItem
                        lst.Text = "BizDoc Approval"
                        lst.Value = "972"
                        ddlType.Items.Clear()
                        ddlType.Items.Add(lst)

                        objCommon.charModule = "C"
                        objCommon.ContactID = lngContactID
                        objCommon.GetCompanySpecificValues1()
                        lnkCompany.Text = objCommon.CompanyName
                        lnkContact.Text = objCommon.ContactName
                        hplEmail.Text = objCommon.EmailID
                        hplEmail.Attributes.Add("onclick", "return fn_Mail('" & objCommon.EmailID & "','" & objCommon.ContactID & "')")
                        lblPhone.Text = objCommon.Phone
                        sb_FillEmpList()
                        sb_DisplayDetails()
                    End If
                End If




                'If GetQueryStringVal( "CaseId") <> 0 Then
                '    pnlTimeExpense.Visible = True
                '    lngCaseid = GetQueryStringVal( "CaseId")
                '    'lngCaseTimeId = GetQueryStringVal("CaseTimeId")
                '    'lngCaseExpId = GetQueryStringVal("CaseExpId")
                'Else : pnlTimeExpense.Visible = False
                'End If
                'If Not IsPostBack Then
                '    If GetQueryStringVal( "Popup") = "True" Then
                '        webmenu1.Visible = False
                '        'btnCancel.Attributes.Add("onclick", "return Close()")
                '        objActionItem = New ActionItem
                '        objActionItem.CommID = lngCommId
                '        objActionItem.ChangeSnoozeStatus()
                '    End If
                '    objCommon = New CCommon
                '    objCommon.sb_FillComboFromDBwithSel(ddlType, 73, Session("DomainID"))
                '    objContacts = New CContacts
                '    objContacts.RecID = lngCommId
                '    objContacts.Type = "A"
                '    objContacts.UserCntID = Session("UserContactID")
                '    objContacts.AddVisiteddetails()
                '    sb_FillEmpList()

                '    If lngCaseid <> 0 Then
                '        '''ddlType.ClearSelection()
                '        'ddlType.Items.Clear()

                '        'ddlType.Items.FindByValue("3").Selected = True
                '        ''If Not IsDBNull(dtAuthoritativeBizdocs.Rows(0).Item("numAuthoritativePurchase")) Then
                '        ''    If Not ddlPurchaseOpportunity.Items.FindByValue(dtAuthoritativeBizdocs.Rows(0).Item("numAuthoritativePurchase")) Is Nothing Then
                '        ''        ddlPurchaseOpportunity.Items.FindByValue(dtAuthoritativeBizdocs.Rows(0).Item("numAuthoritativePurchase")).Selected = True
                '        ''    End If
                '        ''End If
                '        Dim lst As New ListItem
                '        lst.Text = "Task"
                '        lst.Value = "972"
                '        ddlType.Items.Clear()
                '        ddlType.Items.Add(lst)
                '        'ddlTaskType.Items.FindByValue("971").Selected = True
                '        ddlType.Enabled = False
                '    End If

                '    objCampaign = New Campaign
                '    objCampaign.DomainID = Session("DomainID")
                '    'ddlEmailTemplate.DataSource = objCampaign.GetEmailTemplates
                '    'ddlEmailTemplate.DataTextField = "VcDocName"
                '    'ddlEmailTemplate.DataValueField = "numGenericDocID"
                '    'ddlEmailTemplate.DataBind()
                '    'ddlEmailTemplate.Items.Insert(0, "--Select One--")
                '    'ddlEmailTemplate.Items.FindByText("--Select One--").Value = 0
                '    objCommon.sb_FillComboFromDBwithSel(ddlStatus, 33, Session("DomainID"))
                '    'objCommon.sb_FillComboFromDBwithSel(ddlActivity, 32, Session("DomainID"))
                '    objCommon.CommID = lngCommId
                '    objCommon.charModule = "A"
                '   
                '    lnkCompany.Text = objCommon.CompanyName
                '    lnkContact.Text = objCommon.ContactName
                '    hplEmail.Text = objCommon.EmailID
                '    hplEmail.Attributes.Add("onclick", "return fn_Mail('" & objCommon.EmailID & "','" & objCommon.ContactID & "')")
                '    lblPhone.Text = objCommon.Phone
                '    sb_DisplayDetails()
                '    btnTime.Attributes.Add("onclick", "return openTime('" & GetQueryStringVal( "CaseId") & "','" & objCommon.ContactID & "','" & objCommon.DivisionID & "','" & lngCommId & "')")
                '    btnExp.Attributes.Add("onclick", "return openExp('" & GetQueryStringVal( "CaseId") & "','" & objCommon.ContactID & "','" & objCommon.DivisionID & "','" & lngCommId & "')")
                'End If

            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub sb_FillEmpList()
            Try
                If objContacts Is Nothing Then objContacts = New CContacts

                Dim dtEmployeeList As DataTable
                objContacts.DomainID = Session("DomainID")
                dtEmployeeList = objContacts.EmployeeList
                ddlUserNames.DataSource = dtEmployeeList
                ddlUserNames.DataTextField = "vcUserName"
                ddlUserNames.DataValueField = "numContactID"
                ddlUserNames.DataBind()
                If Not ddlUserNames.Items.FindByValue(lngContactID) Is Nothing Then
                    ddlUserNames.ClearSelection()
                    ddlUserNames.Items.FindByValue(lngContactID).Selected = True
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub sb_DisplayDetails()
            Try
                Dim dtActDetails As DataTable
                Dim ds As New DataSet
                If objActionItem Is Nothing Then objActionItem = New ActionItem
                objActionItem.CommID = lngCommId
                objActionItem.DomainID = Session("DomainID")
                objActionItem.UserCntID = lngContactID
                objActionItem.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
                ds = objActionItem.GetBizDocActionItemDtls(lngBizDocActId)
                dtActDetails = ds.Tables(0)
                dgActionItem.DataSource = dtActDetails
                dgActionItem.DataBind()
            Catch Ex As Exception
                Throw Ex
            End Try
        End Sub

        Private Sub sb_DeleteDetails()
            Try
                If objActionItem Is Nothing Then objActionItem = New ActionItem
                objActionItem.CommID = lngCommId
                objActionItem.DeleteActionItem()
            Catch Ex As Exception
                Throw Ex
            End Try
        End Sub

        Function sb_PageRedirect() As String
            Try
                Dim strRedirect As String = ""
                If GetQueryStringVal("frm") = "contactdetails" Then
                    If objCommon Is Nothing Then objCommon = New CCommon
                    objCommon.CommID = lngCommId
                    objCommon.charModule = "A"
                    objCommon.GetCompanySpecificValues1()
                    strRedirect = "../contact/frmContacts.aspx?frm=" & GetQueryStringVal("frm1") & "&CntId=" & objCommon.ContactID
                ElseIf GetQueryStringVal("frm") = "contactdtl" Then
                    If objCommon Is Nothing Then objCommon = New CCommon
                    objCommon.CommID = lngCommId
                    objCommon.charModule = "A"
                    objCommon.GetCompanySpecificValues1()
                    strRedirect = "../contact/frmContacts.aspx?frm=" & GetQueryStringVal("frm1") & "&CntId=" & objCommon.ContactID
                ElseIf GetQueryStringVal("frm") = "tickler" Then
                    strRedirect = "../common/frmticklerdisplay.aspx"
                ElseIf GetQueryStringVal("frm") = "accounts" Then
                    If objCommon Is Nothing Then objCommon = New CCommon
                    objCommon.CommID = lngCommId
                    objCommon.charModule = "A"
                    objCommon.GetCompanySpecificValues1()
                    strRedirect = "../Account/frmAccounts.aspx?frm=accounts&klds+7kldf=fjk-las&DivId=" & objCommon.DivisionID & ""
                ElseIf GetQueryStringVal("frm") = "accountsdtl" Then
                    If objCommon Is Nothing Then objCommon = New CCommon
                    objCommon.CommID = lngCommId
                    objCommon.charModule = "A"
                    objCommon.GetCompanySpecificValues1()
                    strRedirect = "../Account/frmAccounts.aspx?frm=accounts&klds+7kldf=fjk-las&DivId=" & objCommon.DivisionID & ""
                ElseIf GetQueryStringVal("frm") = "prospects" Then
                    If objCommon Is Nothing Then objCommon = New CCommon
                    objCommon.CommID = lngCommId
                    objCommon.charModule = "A"
                    objCommon.GetCompanySpecificValues1()
                    strRedirect = "../prospects/frmProspects.aspx?frm=prospects&DivID=" & objCommon.DivisionID & ""
                ElseIf GetQueryStringVal("frm") = "prospectsdtl" Then
                    If objCommon Is Nothing Then objCommon = New CCommon
                    objCommon.CommID = lngCommId
                    objCommon.charModule = "A"
                    objCommon.GetCompanySpecificValues1()
                    strRedirect = "../prospects/frmProspects.aspx?frm=prospects&DivID=" & objCommon.DivisionID & ""
                ElseIf GetQueryStringVal("frm") = "opportunitylist" Then
                    strRedirect = "../opportunity/opptlist.aspx"
                ElseIf GetQueryStringVal("frm") = "Leads" Or GetQueryStringVal("frm") = "Leadedetails" Then
                    If objCommon Is Nothing Then objCommon = New CCommon
                    objCommon.CommID = lngCommId
                    objCommon.charModule = "A"
                    objCommon.GetCompanySpecificValues1()
                    strRedirect = "../Leads/frmLeads.aspx?DivID=" & objCommon.DivisionID & ""
                ElseIf GetQueryStringVal("frm") = "Leaddtl" Then
                    If objCommon Is Nothing Then objCommon = New CCommon
                    objCommon.CommID = lngCommId
                    objCommon.charModule = "A"
                    objCommon.GetCompanySpecificValues1()
                    strRedirect = "../Leads/frmLeads.aspx?DivID=" & objCommon.DivisionID & ""
                ElseIf GetQueryStringVal("frm") = "Cases" Then
                    strRedirect = "../Cases/frmCases.aspx?frm=caselist&CaseID=" & GetQueryStringVal("CaseId")
                Else
                    strRedirect = "../common/frmticklerdisplay.aspx"
                End If
                Return strRedirect
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        'Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        '    Try
        '        Save()
        '        If GetQueryStringVal( "Popup") = "True" Then
        '            Dim strScript As String = "<script language=JavaScript>"
        '            strScript += "self.close();"
        '            strScript += "</script>"
        '            If (Not Page.IsStartupScriptRegistered("clientScript")) Then Page.RegisterStartupScript("clientScript", strScript)
        '        Else : Response.Redirect(sb_PageRedirect())
        '        End If
        '    Catch ex As Exception
        '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
        '        Response.Write(ex)
        '    End Try
        'End Sub

        'Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        '    Try
        '        Response.Redirect(sb_PageRedirect())
        '    Catch ex As Exception
        '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
        '        Response.Write(ex)
        '    End Try
        'End Sub

        'Private Sub btnFollowup_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnFollowup.Click
        '    Try
        '        'chkClose.Checked = True
        '        Save()
        '        If objCommon Is Nothing Then objCommon = New CCommon
        '        objCommon.CommID = GetQueryStringVal( "CommId")
        '        objCommon.charModule = "A"
        '        objCommon.GetCompanySpecificValues1()
        '        Response.Redirect("../admin/newaction.aspx?frm=" & GetQueryStringVal( "frm") & "&cntid=" & objCommon.ContactID & "&Popup=" & GetQueryStringVal( "Popup"), False)
        '    Catch ex As Exception
        '        Response.Redirect(sb_PageRedirect())
        '    End Try
        'End Sub

        Sub Save()
            Try
                'If ddlUserNames.Items.Count = 0 Then Exit Sub
                'Dim intTaskFlg As Integer
                'Try

                '    'Communication means in the DB the flag  value is equal to 1. Task means it is equal to 0.
                '    'intTaskFlg = IIf(cmbMainType.Value = 0, 0, 1)
                '    intTaskFlg = ddlType.SelectedItem.Value



                '    Dim strCalendar, strStartTime, strEndTime, strDate As String
                '    strDate = cal.SelectedDate
                '    'strStartTime = strDate.Trim & " " & ddltime.SelectedItem.Text.Trim & ":00" & IIf(chkAM.Checked = True, " AM", " PM")
                '    'strEndTime = strDate.Trim & " " & ddlEndTime.SelectedItem.Text.Trim & ":00" & IIf(chkEndAM.Checked = True, " AM", " PM")


                '    If ddlType.SelectedValue <> 973 And ddlType.SelectedValue <> 974 Then
                '        Dim strSplitStartTimeDate As Date                                               'To store the Date and Time for the start of the Action Item
                '        Dim strSplitEndTimeDate As Date         'To store the Date and Time for the end of the Action Item
                '        strSplitStartTimeDate = CType(strStartTime, DateTime).AddMinutes(Session("ClientMachineUTCTimeOffset"))        'Convert the Start Date and Time to UT
                '        strSplitEndTimeDate = CType(strEndTime, DateTime).AddMinutes(Session("ClientMachineUTCTimeOffset"))          'Convert the End Date and Time to UTC
                '        Dim strDesc As String = "Company : " & lnkCompany.Text & "<BR>" & "Contact : " & lnkContact.Text & "<BR>" & "AssignedTo : " & ddlUserNames.SelectedItem.Text & "<BR>" '& txtcomments.Text
                '        If ddlUserNames.SelectedItem.Text = "My Self" Then
                '            If Len(Trim(GetQueryStringVal( "CommId"))) = 0 Then
                '                'If chkOutlook.Checked = True Then
                '                If objOutLook Is Nothing Then
                '                    objOutLook = New COutlook
                '                End If
                '                objOutLook.UserCntID = Session("UserContactId")
                '                objOutLook.DomainId = Session("DomainId")
                '                Dim dtTable As DataTable
                '                dtTable = objOutLook.GetResourceId
                '                If dtTable.Rows.Count > 0 Then
                '                    objOutLook.AllDayEvent = False
                '                    objOutLook.ActivityDescription = strDesc
                '                    objOutLook.Location = ""
                '                    objOutLook.Subject = ddlType.SelectedItem.Text
                '                    objOutLook.Duration = DateDiff(DateInterval.Second, strSplitStartTimeDate, strSplitEndTimeDate)
                '                    objOutLook.StartDateTimeUtc = strSplitStartTimeDate
                '                    objOutLook.EnableReminder = True
                '                    objOutLook.ReminderInterval = 900
                '                    objOutLook.ShowTimeAs = 3
                '                    objOutLook.Importance = 2
                '                    objOutLook.RecurrenceKey = -999
                '                    objOutLook.ResourceName = dtTable.Rows(0).Item("ResourceId")
                '                    If txtActivityID.Text = "0" Then
                '                        txtActivityID.Text = CStr(objOutLook.AddActivity())
                '                    Else

                '                        objOutLook.ActivityID = CInt(txtActivityID.Text)
                '                        objOutLook.UpdateActivity()
                '                    End If
                '                End If


                '                'strCalendar = clsProspects.fn_InsertOWAActionItem(Session("SiteType"), strSplitStartTimeDate, strSplitEndTimeDate, Session("ServerName"), Session("UserEmail"), txtcomments.Text)
                '                'clsProspects.fn_DeleteOWAActionFromEx(Session("SiteType"), Session("ServerName"), Session("UserEmail"), HidStrEml.Value)
                '            End If
                '        Else
                '            If chkClose.Checked = True Then
                '                If objOutLook Is Nothing Then
                '                    objOutLook = New COutlook
                '                End If
                '                objOutLook.ActivityID = CInt(txtActivityID.Text)
                '                objOutLook.RemoveActivity()
                '            Else
                '                If chkOutlook.Checked = True Then

                '                    If objOutLook Is Nothing Then
                '                        objOutLook = New COutlook
                '                    End If
                '                    objOutLook.UserCntID = Session("UserContactId")
                '                    objOutLook.DomainId = Session("DomainId")
                '                    Dim dtTable As DataTable
                '                    dtTable = objOutLook.GetResourceId
                '                    If dtTable.Rows.Count > 0 Then
                '                        objOutLook.AllDayEvent = False
                '                        objOutLook.ActivityDescription = strDesc
                '                        objOutLook.Location = ""
                '                        objOutLook.Subject = ddlType.SelectedItem.Text
                '                        objOutLook.Duration = DateDiff(DateInterval.Second, strSplitStartTimeDate, strSplitEndTimeDate)
                '                        objOutLook.StartDateTimeUtc = strSplitStartTimeDate
                '                        objOutLook.EnableReminder = True
                '                        objOutLook.ReminderInterval = 900
                '                        objOutLook.ShowTimeAs = 3
                '                        objOutLook.Importance = 2
                '                        objOutLook.RecurrenceKey = -999
                '                        objOutLook.ResourceName = dtTable.Rows(0).Item("ResourceId")
                '                        If txtActivityID.Text = "0" Then
                '                            txtActivityID.Text = CStr(objOutLook.AddActivity())
                '                        Else
                '                            objOutLook.ActivityID = CInt(txtActivityID.Text)
                '                            objOutLook.UpdateActivity()
                '                        End If
                '                    End If
                '                End If
                '            End If
                '        End If

                '    Else

                '        If objContacts Is Nothing Then
                '            objContacts = New CContacts
                '        End If
                '        If Len(Trim(GetQueryStringVal( "CommId"))) = 0 Then
                '            If chkOutlook.Checked = True Then
                '                If objOutLook Is Nothing Then
                '                    objOutLook = New COutlook
                '                End If
                '                objOutLook.UserCntID = ddlUserNames.SelectedItem.Value
                '                objOutLook.DomainId = Session("DomainId")
                '                Dim dtTable As DataTable
                '                dtTable = objOutLook.GetResourceId
                '                If dtTable.Rows.Count > 0 Then
                '                    objOutLook.AllDayEvent = False
                '                    objOutLook.ActivityDescription = strDesc
                '                    objOutLook.Location = ""
                '                    objOutLook.Subject = ddlType.SelectedItem.Text
                '                    objOutLook.Duration = DateDiff(DateInterval.Second, strSplitStartTimeDate, strSplitEndTimeDate)
                '                    objOutLook.StartDateTimeUtc = strSplitStartTimeDate
                '                    objOutLook.EnableReminder = True
                '                    objOutLook.ReminderInterval = 900
                '                    objOutLook.ShowTimeAs = 3
                '                    objOutLook.Importance = 2
                '                    objOutLook.RecurrenceKey = -999
                '                    objOutLook.ResourceName = dtTable.Rows(0).Item("ResourceId")
                '                    If txtActivityID.Text = "0" Then
                '                        txtActivityID.Text = CStr(objOutLook.AddActivity())
                '                    Else

                '                        objOutLook.ActivityID = CInt(txtActivityID.Text)
                '                        objOutLook.UpdateActivity()
                '                    End If
                '                End If

                '                'strCalendar = clsProspects.fn_InsertOWAActionItem(Session("SiteType"), strSplitStartTimeDate, strSplitEndTimeDate, Session("ServerName"), IIf(ddlUserNames.SelectedItem.Text = "My Self", Session("UserEmail"), objContacts.EmployeeEmail(ddlUserNames.SelectedItem.Value)), txtcomments.Text)
                '            End If
                '        Else
                '            If chkClose.Checked = True Then
                '                If objOutLook Is Nothing Then
                '                    objOutLook = New COutlook
                '                End If
                '                objOutLook.ActivityID = CInt(txtActivityID.Text)
                '                objOutLook.RemoveActivity()
                '                ''For deleting the corresponding calendar entry in the Exchange database.
                '                'clsProspects.fn_DeleteOWAActionFromEx(Session("SiteType"), Session("ServerName"), IIf(ddlUserNames.SelectedItem.Text = "My Self", Session("UserEmail"), objContacts.EmployeeEmail(ddlUserNames.SelectedItem.Value)), HidStrEml.Value)
                '                'strCalendar = HidStrEml.Value
                '            Else
                '                If chkOutlook.Checked = True Then
                '                    If objOutLook Is Nothing Then
                '                        objOutLook = New COutlook
                '                    End If
                '                    objOutLook.UserCntID = ddlUserNames.SelectedItem.Value
                '                    objOutLook.DomainId = Session("DomainId")
                '                    Dim dtTable As DataTable
                '                    dtTable = objOutLook.GetResourceId
                '                    If dtTable.Rows.Count > 0 Then
                '                        objOutLook.AllDayEvent = False
                '                        objOutLook.ActivityDescription = strDesc
                '                        objOutLook.Location = ""
                '                        objOutLook.Subject = ddlType.SelectedItem.Text
                '                        objOutLook.Duration = DateDiff(DateInterval.Second, strSplitStartTimeDate, strSplitEndTimeDate)
                '                        objOutLook.StartDateTimeUtc = strSplitStartTimeDate
                '                        objOutLook.EnableReminder = True
                '                        objOutLook.ReminderInterval = 900
                '                        objOutLook.ShowTimeAs = 3
                '                        objOutLook.Importance = 2
                '                        objOutLook.RecurrenceKey = -999
                '                        objOutLook.ResourceName = dtTable.Rows(0).Item("ResourceId")
                '                        If txtActivityID.Text = "0" Then
                '                            txtActivityID.Text = CStr(objOutLook.AddActivity())
                '                        Else

                '                            objOutLook.ActivityID = CInt(txtActivityID.Text)
                '                            objOutLook.UpdateActivity()
                '                        End If
                '                    End If

                '                    'clsProspects.fn_DeleteOWAActionFromEx(Session("SiteType"), Session("ServerName"), IIf(ddlUserNames.SelectedItem.Text = "My Self", Session("UserEmail"), objContacts.EmployeeEmail(ddlUserNames.SelectedItem.Value)), HidStrEml.Value)
                '                    'strCalendar = clsProspects.fn_InsertOWAActionItem(Session("SiteType"), strSplitStartTimeDate, strSplitEndTimeDate, Session("ServerName"), IIf(ddlUserNames.SelectedItem.Text = "My Self", Session("UserEmail"), objContacts.EmployeeEmail(ddlUserNames.SelectedItem.Value)), txtcomments.Text)
                '                End If
                '            End If
                '        End If

                '    End If

                '    End If
                '    Dim intAssignTo, intEndTime, intTime, intEndChk, intChk As Integer
                '    Dim strFollowUp As String


                '    If ddlUserNames.Enabled = True Then
                '        intAssignTo = ddlUserNames.SelectedItem.Value
                '        'intEndTime = ddlEndTime.SelectedItem.Value
                '        'intEndChk = IIf(chkEndAM.Checked = True, 1, 0)
                '        'intTime = ddltime.SelectedItem.Value
                '        'intChk = IIf(chkAM.Checked = True, 1, 0)
                '    End If
                '    Dim chkSnoozeStatus, chkRemainderStatus As Integer
                '    If chkPopupRemainder.Checked = True Then
                '        chkRemainderStatus = 1
                '    Else
                '        chkRemainderStatus = 0
                '    End If
                '    'If ddlSnooze.SelectedItem.Value > 0 Then
                '    '    chkSnoozeStatus = 1
                '    'Else
                '    '    chkSnoozeStatus = 0
                '    'End If

                '    If objActionItem Is Nothing Then
                '        objActionItem = New ActionItem
                '    End If
                '    With objActionItem
                '        .CommID = lngCommId
                '        .Task = intTaskFlg
                '        '.Details = txtcomments.Text
                '        .AssignedTo = intAssignTo
                '        .UserCntID = Session("UserContactID")
                '        .DomainID = Session("DomainID")
                '        .BitClosed = IIf(chkClose.Checked = True, 1, 0)
                '        .CalendarName = strCalendar
                '        If ddlType.SelectedValue = 973 Then
                '            .StartTime = cal.SelectedDate
                '            .EndTime = DateAdd(DateInterval.Day, 1, .StartTime)
                '        Else
                '            .StartTime = strStartTime
                '            .EndTime = strEndTime
                '        End If
                '        '.Activity = ddlActivity.SelectedItem.Value
                '        .Status = ddlStatus.SelectedItem.Value
                '        '.Snooze = ddlSnooze.SelectedItem.Value
                '        .SnoozeStatus = chkSnoozeStatus
                '        '.Remainder = ddlRemainder.SelectedItem.Value
                '        .RemainderStatus = chkRemainderStatus
                '        .ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
                '        .bitOutlook = IIf(chkOutlook.Checked = True, 1, 0)
                '        '.SendEmailTemplate = rdlEmailTemplate.SelectedValue
                '        '.EmailTemplate = ddlEmailTemplate.SelectedValue
                '        '.Hours = IIf(txtHours.Text = "", 0, txtHours.Text)
                '        '.Alert = IIf(chkAlert.Checked = True, 1, 0)
                '        .CaseID = lngCaseid
                '        .CaseTimeId = 0
                '        .CaseExpId = 0
                '        .ActivityId = CInt(txtActivityID.Text)
                '    End With
                '    objActionItem.SaveCommunicationinfo()
                '    ''Add To Correspondense if Open record is selected
                '    'If CCommon.ToLong(ddlOpenRecord.SelectedValue) > 0 Then
                '    '    objActionItem.CorrespondenceID = CCommon.ToLong(hdnCorrespondenceID.Value)
                '    '    objActionItem.CommID = lngCommId
                '    '    objActionItem.EmailHistoryID = 0
                '    '    objActionItem.CorrType = ddlAssignTO1.SelectedValue
                '    '    objActionItem.OpenRecordID = ddlOpenRecord.SelectedValue
                '    '    objActionItem.ManageCorrespondence()
                '    'End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub lnkContact_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkContact.Click
            Try
                If objCommon Is Nothing Then objCommon = New CCommon
                objCommon.CommID = lngCommId
                objCommon.charModule = "A"
                objCommon.GetCompanySpecificValues1()
                Response.Redirect("../contact/frmContacts.aspx?frm=ActItem&CntId=" & objCommon.ContactID & "&CommID=" & lngCommId)

            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub lnkCompany_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkCompany.Click
            Try
                If objCommon Is Nothing Then objCommon = New CCommon
                objCommon.CommID = lngCommId
                objCommon.charModule = "A"
                objCommon.GetCompanySpecificValues1()
                If objCommon.CRMType = 0 Then
                    Response.Redirect("../Leads/frmLeads.aspx?frm=ActItem&DivID=" & objCommon.DivisionID & "&CommID=" & lngCommId)
                ElseIf objCommon.CRMType = 1 Then
                    Response.Redirect("../prospects/frmProspects.aspx?frm=ActItem&DivID=" & objCommon.DivisionID & "&CommID=" & lngCommId)
                ElseIf objCommon.CRMType = 2 Then
                    Response.Redirect("../account/frmAccounts.aspx?frm=ActItem&klds+7kldf=fjk-las&DivId=" & objCommon.DivisionID & "&CommID=" & lngCommId)
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        'Private Sub btnActDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnActDelete.Click
        '    Try
        '        Dim str As String = sb_PageRedirect()
        '        sb_DeleteDetails()
        '        If GetQueryStringVal( "Popup") = "True" Then
        '            Dim strScript As String = "<script language=JavaScript>"
        '            strScript += "self.close();"
        '            strScript += "</script>"
        '            If (Not Page.IsStartupScriptRegistered("clientScript")) Then Page.RegisterStartupScript("clientScript", strScript)
        '        Else : Response.Redirect(str)
        '        End If
        '    Catch ex As Exception
        '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
        '        Response.Write(ex)
        '    End Try
        'End Sub

        Private Sub btnCaseName_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCaseName.Click
            Try
                Response.Redirect("../cases/frmCases.aspx?frm=caselist&CaseID=" & GetQueryStringVal("CaseId"))

            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        'Private Sub ddlAssignTO1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAssignTO1.SelectedIndexChanged
        '    Try
        '        LoadOpenRecords()
        '    Catch ex As Exception
        '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
        '        Response.Write(ex)
        '    End Try
        'End Sub
        Private Sub LoadOpenRecords()
            Try
                'ddlOpenRecord.Items.Clear()
                'If ddlAssignTO1.SelectedValue > 0 Then
                '    If objCommon Is Nothing Then objCommon = New CCommon
                '    objCommon.CommID = lngCommId
                '    objCommon.charModule = "A"
                '    objCommon.GetCompanySpecificValues1()
                '    Dim objOpp As New MOpportunity
                '    objOpp.DomainID = Session("DomainId")
                '    objOpp.DivisionID = objCommon.DivisionID
                '    Select Case ddlAssignTO1.SelectedValue
                '        Case 1
                '            objOpp.bytemode = 1 'select only sales opportunity
                '            ddlOpenRecord.DataSource = objOpp.GetOpenRecords()
                '            ddlOpenRecord.DataTextField = "vcPOppName"
                '            ddlOpenRecord.DataValueField = "numOppId"
                '            ddlOpenRecord.DataBind()
                '            ddlOpenRecord.Items.Insert(0, "--Select One--")
                '            ddlOpenRecord.Items.FindByText("--Select One--").Value = "0"
                '        Case 2
                '            objOpp.bytemode = 2
                '            ddlOpenRecord.DataSource = objOpp.GetOpenRecords()
                '            ddlOpenRecord.DataTextField = "vcPOppName"
                '            ddlOpenRecord.DataValueField = "numOppId"
                '            ddlOpenRecord.DataBind()
                '            ddlOpenRecord.Items.Insert(0, "--Select One--")
                '            ddlOpenRecord.Items.FindByText("--Select One--").Value = "0"
                '        Case 3
                '            objOpp.bytemode = 3
                '            ddlOpenRecord.DataSource = objOpp.GetOpenRecords()
                '            ddlOpenRecord.DataTextField = "vcPOppName"
                '            ddlOpenRecord.DataValueField = "numOppId"
                '            ddlOpenRecord.DataBind()
                '            ddlOpenRecord.Items.Insert(0, "--Select One--")
                '            ddlOpenRecord.Items.FindByText("--Select One--").Value = "0"
                '        Case 4
                '            objOpp.bytemode = 4
                '            ddlOpenRecord.DataSource = objOpp.GetOpenRecords()
                '            ddlOpenRecord.DataTextField = "vcPOppName"
                '            ddlOpenRecord.DataValueField = "numOppId"
                '            ddlOpenRecord.DataBind()
                '            ddlOpenRecord.Items.Insert(0, "--Select One--")
                '            ddlOpenRecord.Items.FindByText("--Select One--").Value = "0"
                '        Case 5
                '            Dim objProject As New Project
                '            objProject.DomainID = Session("DomainID")
                '            objProject.DivisionID = objCommon.DivisionID
                '            ddlOpenRecord.DataTextField = "vcProjectName"
                '            ddlOpenRecord.DataValueField = "numProId"
                '            ddlOpenRecord.DataSource = objProject.GetOpenProject()
                '            ddlOpenRecord.DataBind()
                '            ddlOpenRecord.Items.Insert(0, "--Select One--")
                '            ddlOpenRecord.Items.FindByText("--Select One--").Value = "0"
                '        Case 6
                '            Dim ObjCases As New CCases
                '            Dim dtCaseNo As DataTable
                '            ObjCases.DivisionID = objCommon.DivisionID
                '            dtCaseNo = ObjCases.GetOpenCases
                '            ddlOpenRecord.DataTextField = "vcCaseNumber"
                '            ddlOpenRecord.DataValueField = "numCaseId"
                '            ddlOpenRecord.DataSource = dtCaseNo
                '            ddlOpenRecord.DataBind()
                '            ddlOpenRecord.Items.Insert(0, "--Select One--")
                '            ddlOpenRecord.Items.FindByText("--Select One--").Value = 0
                '    End Select

                'End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSave.Click
            Try
                Dim intLoop As Int16
                Dim blnInvalid As Boolean
                blnInvalid = True
                For intLoop = 0 To dgActionItem.Items.Count - 1
                    If CType(dgActionItem.Items(intLoop).FindControl("chkAccept"), RadioButton).Checked = True And CType(dgActionItem.Items(intLoop).FindControl("chkDecline"), RadioButton).Checked = True Then
                        ltMessage.Text = "Invalid Action"
                        blnInvalid = False
                        Exit Sub
                    End If
                Next
                objActionItem = New ActionItem

                Dim intAction As Int16
                Dim intType As Int16
                Dim strComment As String
                Dim OppBizDocID As Integer
                Dim lngOppId As Long
                Dim lngBizDocId As Long
                If blnInvalid = True Then
                    For intLoop = 0 To dgActionItem.Items.Count - 1
                        intAction = 0
                        If CType(dgActionItem.Items(intLoop).FindControl("chkAccept"), RadioButton).Checked = True Or CType(dgActionItem.Items(intLoop).FindControl("chkDecline"), RadioButton).Checked = True Then

                            If CType(dgActionItem.Items(intLoop).FindControl("chkAccept"), RadioButton).Checked = True Then intAction = 1
                            If CType(dgActionItem.Items(intLoop).FindControl("chkDecline"), RadioButton).Checked = True Then intAction = 2
                            strComment = CType(dgActionItem.Items(intLoop).FindControl("txtComment"), TextBox).Text
                            dgActionItem.Items(intLoop).Cells(4).Text = Replace(dgActionItem.Items(intLoop).Cells(4).Text, "&nbsp;", "")
                            intType = IIf(dgActionItem.Items(intLoop).Cells(15).Text = "B", 1, 0)

                            lngOppId = dgActionItem.Items(intLoop).Cells(12).Text
                            OppBizDocID = CCommon.ToLong(dgActionItem.Items(intLoop).Cells(13).Text)
                            lngBizDocId = CCommon.ToLong(dgActionItem.Items(intLoop).Cells(17).Text)

                            objActionItem.DomainID = Session("DomainID")
                            objActionItem.UserCntID = Session("UserContactID")
                            If objActionItem.UpdateBizDocActionItemDtls(lngBizDocActId, OppBizDocID, intAction, intType, strComment) = False Then
                                ltMessage.Text = "Error While Saving BizDoc Action"
                                Exit Sub
                            End If
                            'send only when bizdoc is approved
                            If intAction = 1 And lngBizDocId > 0 And lngOppId > 0 And OppBizDocID > 0 Then
                                '------ Send BizDoc Alerts - Notify record owners, their supervisors, and your trading partners when a BizDoc is created, modified, or approved.
                                Dim objAlert As New CAlerts
                                objAlert.SendBizDocAlerts(lngOppId, OppBizDocID, lngBizDocId, Session("DomainID"), CAlerts.enmBizDoc.IsApproved)
                            End If
                        End If
                    Next
                    ltMessage.Text = "BizDoc Action Saved Successfully"
                    Response.Redirect(sb_PageRedirect())
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        'Private Sub OpenGenDoc()
        '    Session("DocID") = e.Item.Cells(0).Text()
        '    Response.Redirect("../Documents/frmDocuments.aspx")
        'End Sub


        Private Sub dgActionItem_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgActionItem.ItemDataBound
            Try
                If e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.Item Then
                    Dim btnDelete As Button

                    Dim hpl As HyperLink


                    hpl = e.Item.FindControl("hplLink")
                    hpl.NavigateUrl = Replace(e.Item.Cells(14).Text, "..", "../../" & "/" & ConfigurationManager.AppSettings("PortalVirtualDirectoryName"))
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
            Response.Redirect("../common/frmTicklerDisplay.aspx")
        End Sub
    End Class
End Namespace