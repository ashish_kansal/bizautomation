﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/common/Popup.Master"
    CodeBehind="frmAssigntoContact.aspx.vb" Inherits="BACRM.UserInterface.Admin.frmAssigntoContact" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content3" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
        <script type="text/javascript" src="../javascript/comboClientSide.js"></script>
    <link href="../CSS/font-awesome.min.css" rel="stylesheet" />
    <script type="text/javascript">
        function onRadComboBoxLoad(sender) {
            var div = sender.get_element();

            $telerik.$(div).bind('mouseenter', function () {
                if (!sender.get_dropDownVisible())
                    sender.showDropDown();
            });


            $telerik.$(".RadComboBoxDropDown").mouseleave(function (e) {
                hideDropDown("#" + sender.get_id(), sender, e);
            });


            $telerik.$(div).mouseleave(function (e) {
                hideDropDown(".RadComboBoxDropDown", sender, e);
            });

        }
    </script>
    <style>
        .btn-primary {
    background-color: #1473B4;
    border-color: #367fa9;
}
        .btn {
    border-radius: 3px;
    -webkit-box-shadow: none;
    box-shadow: none;
    border: 1px solid transparent;
}
.btn-primary {
    color: #fff;
    background-color: #337ab7;
    border-color: #2e6da4;
}
.btn-danger {
    background-color: #dd4b39;
    border-color: #d73925;
}
.btn-danger {
    color: #fff;
}
.btn {
    display: inline-block;
    padding: 6px 12px;
    margin-bottom: 0;
    font-size: 14px;
    font-weight: 400;
    line-height: 1.42857143;
    text-align: center;
    white-space: nowrap;
    vertical-align: middle;
    -ms-touch-action: manipulation;
    touch-action: manipulation;
    cursor: pointer;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
    background-image: none;
    border: 1px solid transparent;
    border-radius: 4px;
}
    </style>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <div class="input-part">
        <div class="right-input">
            <asp:Button Text="Save" runat="server" CssClass="button" ID="btnSave" />
            <asp:Button Text="Close" runat="server" CssClass="button" ID="btnClose" OnClientClick="Close();" />&nbsp;&nbsp;
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="PageTitle" runat="server" ClientIDMode="Static">
    Arrange Class
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="Content" runat="server" ClientIDMode="Static">
  <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <table width="600px">
        <tr>
            <td class="normal4" align="center">
                <asp:Literal ID="litMessage" runat="server" EnableViewState="False"></asp:Literal>
            </td>
        </tr>
        <tr>
            <td>
                Assign To
            </td>
            <td>
                <asp:DropDownList ID="ddlAssignTo" runat="server"></asp:DropDownList>
            </td>
            <td colspan="4"></td>
        </tr>
        <tr>
            <td>Organization</td>
            <td><telerik:RadComboBox AccessKey="C" ID="radCmbCompany" Width="100%" DropDownWidth="600px" OnSelectedIndexChanged="radCmbCompany_SelectedIndexChanged"
                                                    Skin="Default" runat="server" AutoPostBack="True" AllowCustomText="True" ShowMoreResultsBox="true" EnableLoadOnDemand="True"
                                                    OnClientItemsRequested="OnClientItemsRequestedOrganization"
                                                    ClientIDMode="Static" TabIndex="1" OnClientLoad="onRadComboBoxLoad">
                                                    <WebServiceSettings Path="../common/Common.asmx" Method="GetCompanies" />
                                                </telerik:RadComboBox></td>
            <td>Contat</td>
            <td>
                <telerik:RadComboBox ID="radcmbContact" Width="100%" runat="server" AutoPostBack="true" TabIndex="2" OnClientLoad="onRadComboBoxLoad"></telerik:RadComboBox>
            </td>
            <td>
                <asp:LinkButton ID="btnAdd" OnClick="btnAdd_Click" CssClass="btn btn-primary" runat="server"><i class="fa fa-plus-circle" aria-hidden="true"></i></asp:LinkButton>
            </td>
            <td>
                 <asp:LinkButton ID="btnRemove" OnClick="btnRemove_Click" CssClass="btn btn-danger" runat="server">
                <i class="fa fa-trash" aria-hidden="true"></i>
                     </asp:LinkButton>
            </td>
        </tr>
    </table>
</asp:Content>
