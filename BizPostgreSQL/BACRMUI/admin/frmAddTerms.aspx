<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" CodeBehind="frmAddTerms.aspx.vb"
    Inherits="BACRM.UserInterface.Admin.frmAddTerms" MasterPageFile="~/common/Popup.Master" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Add New Terms</title>
    <script type="text/javascript" language="javascript">
        $(document).ready(function () {
            InitializeValidation();
            $('#litMessage').fadeIn().delay(5000).fadeOut();

            if ($('#hdnTermsID').val() == '' || $('#hdnTermsID').val() == '0' || $('#hdnTermsID').val() == 'undefined' || $('#hdnTermsID').val() == null) {
                $('#tdModified').hide();
            }
            else {
                $('#tdModified').show();
            }
            //// This is the function.
            //String.prototype.format = function (args) {
            //    var str = this;
            //    return str.replace(String.prototype.format.regex, function (item) {
            //        var intVal = parseInt(item.substring(1, item.length - 1));
            //        var replace;
            //        if (intVal >= 0) {
            //            replace = args[intVal];
            //        } else if (intVal === -1) {
            //            replace = "{";
            //        } else if (intVal === -2) {
            //            replace = "}";
            //        } else {
            //            replace = "";
            //        }
            //        return replace;
            //    });
            //};
            //String.prototype.format.regex = new RegExp("{-?[0-9]+}", "g");
            //// Sample usage.
            //var str = "She {1} {0}{2} by the {0}{3}. {-1}^_^{-2}";
            //str = str.format(["sea", "sells", "shells", "shore"]);
            //alert(str);
        });

        function SaveClose() {            
            opener.location.reload();
            this.Close();
        }


        function CreateTermName() {
            var DiscountPercentage;
            var DiscountPaidDays;
            var NetDueInDays;
            var TermName;
            var InterestPercentage;
            var InterestPaidAfterDays;

            TermName = '';
            DiscountPercentage = $('#txtDiscountPercentage').val();
            DiscountPaidDays = $('#txtDiscountPaidDays').val();
            NetDueInDays = $('#txtNetDueInDays').val();
            InterestPercentage = $('#txtInterestPercentage').val();
            InterestPaidAfterDays = $('#txtInterestPaidAfterDays').val();

            if (DiscountPercentage > 0) {
                TermName = TermName + ' ' + DiscountPercentage + '%';
            }
            if (DiscountPaidDays > 0) {
                TermName = TermName + ' ' + DiscountPaidDays + ' Days ';
            }
            if (NetDueInDays >= 0) {
                TermName = TermName + 'Net ' + NetDueInDays;
            }
            console.log(TermName);
            TermName = $.trim(TermName);
            $('#txtTermsName').val(TermName);
            $('#txtTermsName').text(TermName);
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <table width="100%">
        <tr>
            <td width="65%" style="vertical-align: middle; font-weight: bold" id="tdModified">Last Modified By :&nbsp;
                <asp:Label runat="server" ID="lblLastModifiedBy" Style="vertical-align: middle"></asp:Label>
            </td>
            <td>
                <div class="input-part">
                    <div class="right-input">
                        <asp:Button ID="btnSave" Text="Save & Close" runat="server" CssClass="button"></asp:Button>&nbsp;&nbsp;
            <input class="button" id="btnClose" onclick="javascript: Close()" type="button" value="Close">&nbsp;&nbsp;
                    </div>
                </div>
            </td>
        </tr>
    </table>
    <table width="100%" cellpadding="2" cellspacing="2">
        <tr>
            <td style="color: red" align="center">
                <%--<br />--%>
                <asp:Literal ID="litMessage" runat="server"></asp:Literal>
            </td>
        </tr>
        <tr>
            <td style="color: red">
                <ul id="messagebox" class="errorInfo" style="display: none">
                </ul>
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageTitle" runat="server" ClientIDMode="Static">
    Add New Terms
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="Content" runat="server" ClientIDMode="Static">
    <asp:Table ID="tblTerms" runat="server" Width="500px" GridLines="None"
        BorderColor="black" CssClass="aspTable" BorderWidth="1" Height="130px">
        <asp:TableRow>
            <asp:TableCell HorizontalAlign="Right">Terms Apply To</asp:TableCell>
            <asp:TableCell>
                <asp:DropDownList runat="server" ID="ddlApplyTo">
                    <asp:ListItem Text="Sales Order" Value="1"></asp:ListItem>
                    <asp:ListItem Text="Purchase Order" Value="2"></asp:ListItem>
                </asp:DropDownList>
            </asp:TableCell>
            <asp:TableCell></asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell HorizontalAlign="Right">Terms Name <span style="color: red">*</span></asp:TableCell>
            <asp:TableCell>
                <asp:HiddenField runat="server" ID="hdnTermsID" />
                <asp:HiddenField runat="server" ID="hdnListItemID" />
                <asp:TextBox runat="server" ID="txtTermsName"
                    CssClass="required {required:true, messages:{required:'Enter Term Name! '}}" MaxLength="100"></asp:TextBox>
            </asp:TableCell>
            <asp:TableCell></asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell HorizontalAlign="Right">Net due in <span style="color: red">*</span></asp:TableCell>
            <asp:TableCell>
                <asp:TextBox runat="server" ID="txtNetDueInDays"
                    Width="40px" onblur="CreateTermName()" CssClass="required_integer {required:true,number:true, messages:{required:'Enter Net Due Days! ',number:'provide valid Days value!'}}"></asp:TextBox>
            </asp:TableCell>
            <asp:TableCell>Days</asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell HorizontalAlign="Right">Discount Percentage is</asp:TableCell>
            <asp:TableCell>
                <asp:TextBox runat="server" ID="txtDiscountPercentage"
                    Width="40px" onblur="CreateTermName()" Text="0" CssClass="required_decimal {required:false,number:true,max:100, messages:{required:'Enter Discount Percentage! ',number:'provide valid Discount Percentage value!'}}"></asp:TextBox>
            </asp:TableCell>
            <asp:TableCell>%</asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell HorizontalAlign="Right">Apply discount % if paid&nbsp;&nbsp;>=</asp:TableCell>
            <asp:TableCell>
                <asp:TextBox runat="server" ID="txtDiscountPaidDays"
                    Width="40px" onblur="CreateTermName()" Text="0" CssClass="required_integer {required:false,number:true, messages:{required:'Enter Days! ',number:'provide valid Days value!'}}"></asp:TextBox>
            </asp:TableCell>
            <asp:TableCell>Days or more before due date</asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell HorizontalAlign="Right">Interest percentage is</asp:TableCell>
            <asp:TableCell>
                <asp:TextBox runat="server" ID="txtInterestPercentage"
                    Width="40px" onblur="CreateTermName()" Text="0" CssClass="required_decimal {required:false,number:true,max:100, messages:{required:'Enter Interest Percentage! ',number:'provide valid Interest Percentage value!'}}"></asp:TextBox>
            </asp:TableCell>
            <asp:TableCell></asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell HorizontalAlign="Right">Apply interest % if paid&nbsp;&nbsp;<=</asp:TableCell>
            <asp:TableCell>
                <asp:TextBox runat="server" ID="txtInterestPaidAfterDays"
                    Width="40px" onblur="CreateTermName()" Text="0" CssClass="required_integer {required:false,number:true, messages:{required:'Enter Days! ',number:'provide valid Days value!'}}"></asp:TextBox>
            </asp:TableCell>
            <asp:TableCell>Days or more after due date</asp:TableCell>
        </asp:TableRow>
    </asp:Table>
</asp:Content>
