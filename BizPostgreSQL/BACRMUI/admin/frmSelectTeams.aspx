<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" Codebehind="frmSelectTeams.aspx.vb" Inherits="BACRM.UserInterface.Admin.frmSelectTeams" ValidateRequest="false" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
	<head  runat="server">
        <link rel="stylesheet" href="~/CSS/master.css" type="text/css" />
		<title>Select Teams</title>
  </HEAD>
	<body >
		<form id="frmSelectTeams" method="post" runat="server">
			<table cellSpacing="0" cellPadding="0" width="100%" border="0">
				<tr>
					<td colSpan="3" height=7>
					</td>
				</tr>
				<tr>
					<td vAlign="bottom">
						<table class="TabStyle">
							<tr>
								<td>&nbsp;&nbsp;&nbsp;Select 
									Teams&nbsp;&nbsp;&nbsp;
								</td>
							</tr>
						</table>
					</td>
					<td align="right">
						<asp:button id="btnSave" Runat="server" Text="Save &amp; Close" CssClass="button"></asp:button>
						<input type="button" id="btnClose" Runat="server" Value="Close" Class="button" onclick="javascript: CloseThisWin();"></td>
				</tr>
				<tr>
					<td colSpan="3"><asp:table id="tblTeams" Runat="server" BorderColor="black" GridLines="None" Width="100%" CellSpacing="0" CssClass="aspTable"
							CellPadding="0" BorderWidth="1">
							<asp:TableRow>
								<asp:TableCell>
									<table>
										<TR>
											<TD class="normal1" nowrap>
												Available Teams<br>
												<br>
												<asp:listbox id="lstTeamAvail" runat="server" Width="200" Height="80" CssClass="signup" SelectionMode="Multiple"></asp:listbox>
											</TD>
											<td align="center">
												<input type="button" id="btnAdd" Class="button" Value="Add >" onclick="javascript:move(document.frmSelectTeams.lstTeamAvail,document.frmSelectTeams.lstTeamSelected)">
												<br>
												<input type="button" id="btnRemove" Class="button" Value="< Remove" onclick="javascript:move(document.frmSelectTeams.lstTeamSelected,document.frmSelectTeams.lstTeamAvail)">
											</td>
											<td class="normal1">
												The user will ONLY be able to access
												<br>
												records from the following teams.<br>
												<asp:listbox id="lstTeamSelected" Width="200" Height="80" runat="server" CssClass="signup" SelectionMode="Multiple"></asp:listbox>
											</td>
										</TR>
									</table>
								</asp:TableCell>
							</asp:TableRow>
						</asp:table>
					</td>
				</tr>
			</table>
			<asp:Literal ID="ltClientScript" Runat="server"></asp:Literal>
			<input type="text" name="hdXMLString" ID="hdXMLString" Runat="server" style="DISPLAY:none">
		</form>
	</body>
</HTML>
