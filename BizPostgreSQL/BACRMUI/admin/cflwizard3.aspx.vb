'' Modified By anoop jayaraj
Imports BACRM.BusinessLogic.Common
Namespace BACRM.UserInterface.Admin
    Public Class cflwizard3
        Inherits BACRMPage
        Protected WithEvents CFWFldtype As System.Web.UI.WebControls.DropDownList
        Protected WithEvents btnBack As System.Web.UI.WebControls.Button
        Protected WithEvents btnNext As System.Web.UI.WebControls.Button
        Protected WithEvents btnCancel As System.Web.UI.WebControls.Button
        Dim subgrp
        Dim location As Integer = 0
#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Try
                location = CCommon.ToInteger(GetQueryStringVal("location"))



                If Not IsPostBack Then

                    If Session("PageId") = 7 Or Session("PageId") = 8 Then
                        CFWFldtype.Items.RemoveAt(6)
                        CFWFldtype.Items.RemoveAt(5)
                    End If
                    If Session("PageId") = 9 Then
                        CFWFldtype.Items.RemoveAt(6)
                        CFWFldtype.Items.RemoveAt(5)
                        CFWFldtype.Items.RemoveAt(4)
                        CFWFldtype.Items.RemoveAt(3)
                        CFWFldtype.Items.RemoveAt(1)
                    End If
                End If
                subgrp = Request.Form("CFWsubgrp")
                If Session("fldtype") <> "" Then CFWFldtype.Items.FindByText(Session("fldtype")).Selected = True
                Session("fldtype") = ""
                btnNext.Attributes.Add("onclick", "return Next()")
                btnCancel.Attributes.Add("onclick", "return Close()")

                If (location = 19 Or location = 20) Then

                    For x As Integer = CFWFldtype.Items.Count - 1 To 0 Step -1
                        Dim item As ListItem = CFWFldtype.Items(x)
                        If Not (item.Value.ToLower.Equals("textbox")) Then
                            CFWFldtype.Items.RemoveAt(x)
                        End If
                    Next

                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub btnBack_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBack.Click
            Try
                Response.Redirect("../admin/cflwizard1.aspx")
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub btnNext_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNext.Click
            Try
                Session("fldlbl") = ""
                Session("fldtype") = CFWFldtype.SelectedItem.Value
                Response.Redirect("Cflwizardsum.aspx")
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

    End Class
End Namespace