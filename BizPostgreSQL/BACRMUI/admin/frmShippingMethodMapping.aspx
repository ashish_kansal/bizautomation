﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmShippingMethodMapping.aspx.vb"
    Inherits=".frmShippingMethodMapping" MasterPageFile="~/common/Popup.Master" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Assembly="RadComboBox.Net2" Namespace="Telerik.WebControls" TagPrefix="rad" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Shipping Method Mapping</title>
    <script language="Javascript" type="text/javascript">

        function Save() {
            if (parseInt($find('radShipCompany').get_value()) == 0) {
                alert("Please Select Shipping Method");
                $find('radShipCompany').focus();
                return false;
            }
            if (radCmbCompany.GetValue() == '') {
                alert("Please Select a Vendor")
                return false;
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <div class="input-part">
        <div class="right-input">
            <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" OnClientClick="return Save();"></asp:Button>
            <asp:Button ID="btnClose" runat="server" CssClass="button" Text="Close" OnClientClick="return Close();"></asp:Button>&nbsp;&nbsp;
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageTitle" runat="server" ClientIDMode="Static">
    Shipping Method Mapping
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="Content" runat="server" ClientIDMode="Static">
    <asp:ScriptManager ID="scrBizdocs" runat="server">
        </asp:ScriptManager>
    <table width="600px">
        <tr>
            <td class="normal4" align="center" colspan="2">
                <asp:Literal ID="litMessage" runat="server"></asp:Literal>
            </td>
        </tr>
        <tr>
            <td colspan="2" align="right"></td>
        </tr>
        <tr>
            <td class="normal1" align="right">Shipping Method
            </td>
            <td>
                <%--<asp:DropDownList ID="ddlShipCompany" AutoPostBack="True" runat="server" CssClass="signup"
                    Width="160">
                </asp:DropDownList>--%>

                <telerik:RadComboBox runat="server" ID="radShipCompany" AccessKey="I" Skin="Vista" Height="130px" Width="130px" DropDownWidth="130px"
                    AutoPostBack="true" EnableVirtualScrolling="false" ChangeTextOnKeyBoardNavigation="false" ClientIDMode="Static">
                </telerik:RadComboBox>
            </td>
        </tr>
        <tr>
            <td class="normal1" align="right">Income Account
            </td>
            <td>
                <asp:DropDownList ID="ddlIncomeAccount" runat="server" CssClass="signup" Width="200">
                </asp:DropDownList>
            </td>
        </tr>
    </table>
</asp:Content>
