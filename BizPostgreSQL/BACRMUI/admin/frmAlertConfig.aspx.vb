﻿
Imports BACRMBUSSLOGIC.BussinessLogic
Imports BACRM.BusinessLogic.Common
Imports EmailAlertConfig
Imports System.IO
Public Class frmAlertConfig
    Inherits BACRMPage
    Dim objEmailAlertConfig As EmailAlertConfig
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then
                btnClose.Attributes.Add("onclick", "Close();")
                BindData()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try

    End Sub
    Private Sub BindData()
        Try
            Dim dtEmailAlertConfig As DataTable
            objEmailAlertConfig = New EmailAlertConfig
            objEmailAlertConfig.DomainId = CCommon.ToLong(Session("DomainID"))
            objEmailAlertConfig.ContactId = CCommon.ToLong(Session("UserContactID"))
            dtEmailAlertConfig = objEmailAlertConfig.GetAlertConfig
            With dtEmailAlertConfig
                If .Rows.Count > 0 Then
                    chkTickler.Checked = True 'IIf(CCommon.ToString(.Rows(0)("bitOpenActionItem")) = "", False, .Rows(0)("bitOpenActionItem"))
                    chkCase.Checked = IIf(CCommon.ToString(.Rows(0)("bitOpenCases")) = "", False, .Rows(0)("bitOpenCases"))
                    chkProject.Checked = IIf(CCommon.ToString(.Rows(0)("bitOpenProject")) = "", False, .Rows(0)("bitOpenProject"))
                    chkOrder.Checked = IIf(CCommon.ToString(.Rows(0)("bitOpenSalesOpp")) = "", False, .Rows(0)("bitOpenSalesOpp"))
                    chkARBalance.Checked = True 'IIf(CCommon.ToString(.Rows(0)("bitBalancedue")) = "", False, .Rows(0)("bitBalancedue"))
                    chkMessages.Checked = IIf(CCommon.ToString(.Rows(0)("bitUnreadEmail")) = "", False, .Rows(0)("bitUnreadEmail"))
                    chkCampaign.Checked = True 'IIf(CCommon.ToString(.Rows(0)("bitCampaign")) = "", False, .Rows(0)("bitCampaign"))
                End If
            End With
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            SaveData()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
    Private Sub SaveData()
        Try
            objEmailAlertConfig = New EmailAlertConfig
            objEmailAlertConfig.DomainId = CCommon.ToLong(Session("DomainID"))
            objEmailAlertConfig.ContactId = CCommon.ToLong(Session("UserContactID"))
            objEmailAlertConfig.OpenActionItem = IIf(chkTickler.Checked = True, 1, 0)
            objEmailAlertConfig.OpenCases = IIf(chkCase.Checked = True, 1, 0)
            objEmailAlertConfig.OpenProject = IIf(chkProject.Checked, 1, 0)
            objEmailAlertConfig.OpenSalesOpp = IIf(chkOrder.Checked = True, 1, 0)
            objEmailAlertConfig.BalanceDue = IIf(chkARBalance.Checked = True, 1, 0)
            objEmailAlertConfig.UnReadEmail = IIf(chkMessages.Checked = True, 1, 0)
            objEmailAlertConfig.ShowCampaignAlert = IIf(chkCampaign.Checked = True, 1, 0)
   
            objEmailAlertConfig.ManageAlertConfig()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Private Sub btnSaveClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveClose.Click
        Try
            SaveData()
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "close", "Close();", True)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
End Class