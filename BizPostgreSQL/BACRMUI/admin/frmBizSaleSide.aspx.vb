﻿Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Admin
Namespace BACRM.UserInterface.Admin



    Partial Public Class frmBizSaleSide
        Inherits BACRMPage

        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            Try
                If Not IsPostBack Then Binddata()

                btnGo.Attributes.Add("onclick", "getSort()")
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub
        Private Sub btnGo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGo.Click
            Try
                Save()
                Binddata()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub
        Sub Save()
            Try
                Dim values As String
                Dim header(3) As String
                Dim val(1000) As String
                Dim xmlStr As String
                Dim dtTable As DataTable
                Dim i As Integer

                values = order.Value                                         'Getting the Sorting Order
                header = values.Split("(")
                header = header(1).Split(")")
                val = header(0).Split(",")
                Dim objDT As System.Data.DataTable
                Dim objDR As System.Data.DataRow
                Dim ds As New DataSet
                objDT = New System.Data.DataTable("Table")
                objDT.Columns.Add("numListItemID", GetType(Integer))
                objDT.Columns.Add("tintOrder", GetType(Integer))
                objDT.Columns.Add("numDomainID", GetType(Integer))
                For i = 0 To val.Length - 1
                    objDR = objDT.NewRow
                    objDR("numListItemID") = val(i)
                    objDR("tintOrder") = i + 1
                    objDR("numDomainID") = Session("domainId")
                    objDT.Rows.Add(objDR)
                Next

                ds.Tables.Add(objDT)
                xmlStr = ds.GetXml

                objCommon.xmlStr = xmlStr
                objCommon.ListID = 27
                objCommon.DomainID = Session("domainId")
                objCommon.SortUpdateItemList()
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        Sub Binddata(Optional ByVal boolSortAlphabetically As Boolean = False)
            Try
                Dim dtTable As DataTable

                If GetQueryStringVal("type") <> "" Then

                    objCommon.DomainID = Session("DomainID")
                    'objCommon.ListID = CInt(GetQueryStringVal("ID"))
                    objCommon.BizDocType = CInt(GetQueryStringVal("type"))
                    dtTable = objCommon.GetBizDocType()

                    If boolSortAlphabetically Then
                        ' Get the DefaultViewManager of a DataTable.
                        Dim view As DataView = dtTable.DefaultView
                        ' By default, the first column sorted ascending.
                        view.Sort = "vcData ASC"
                        dtTable = view.ToTable()
                    End If
                    Dim str As String = ""
                    str = "<table cellSpacing='0' cellPadding='0' border='0' align='right'>" &
                           "<tr>" &
                               "<td align='right'>" &
                                 "&nbsp;&nbsp;&nbsp; <input class='button' id='btnSortAlpha' onclick='Sort()' type='button' value='Sort Alphabetically'>&nbsp;" &
                                 "<input class='button' id='btnSave' style='width:50' onclick='Save()' type='button' value='Save'>&nbsp;" &
                                 "<input class='button' id='btnClose' style='width:50' onclick='Close()' type='button' value='Close'>" &
                               "</td>" &
                           "</tr></table> "
                    lblButtons.Text = str

                    str = ""
                    str = str & "<table class='aspTableDTL' cellpadding='0' cellspacing='0' width='400px' >" &
                             "<tr valign='top'>"
                    str = str & "<td valign='top' align='center'>" &
                               "<ul id='x' class='sortable boxy'>"
                    Dim i As New Integer
                    i = 0
                    While (i < dtTable.Rows.Count)
                        str = str & "<li id='" + dtTable.Rows(i).Item("numListItemID").ToString + "' >" + dtTable.Rows(i).Item("vcData") + "</li>"
                        i = i + 1
                    End While
                    str = str & "</ul> </td> </tr></table></td></tr></table>"
                    'Response.Write(str)
                    lblMain.Text = str
                End If
                'ElseIf GetQueryStringVal("RelId") <> "" Then

                '    Dim numRelationshipId As Long = CCommon.ToLong(GetQueryStringVal("RelId"))
                '    Dim objUserAccess As New UserAccess
                '    objUserAccess.RelID = numRelationshipId
                '    objUserAccess.DomainID = Session("DomainID")
                '    dtTable = objUserAccess.GetRelProfileD

                '    Dim str As String
                '    str = " <br><table cellSpacing='0' cellPadding='0' border='0' align='right'>" &
                '            "<tr>" &
                '                "<td vAlign='bottom'>" &
                '                    "<table class='TabStyle'>" &
                '                    "<tr>" &
                '                    "<td>&nbsp;&nbsp;&nbsp;&nbsp;Sort List Items&nbsp;&nbsp;&nbsp;</td>" &
                '                    "</tr>" &
                '                    "</table>" &
                '                "</td>" &
                '                "<td align='right'>" &
                '                    "&nbsp;&nbsp;&nbsp;<input class='button' id='btnSave' style='width:50' onclick='Save()' type='button' value='Save'>" &
                '                    "&nbsp;<input class='button' id='btnClose' style='width:50' onclick='Close()' type='button' value='Close'>" &
                '                "</td>" &
                '            "</tr>" &
                '        "</table> "
                '    str = str & "<table border='0' class='aspTableDTL' cellpadding='0' cellspacing='0' width='400ox'>" &
                '            "<tr valign='top'>" &
                '            "<td align='center'>"
                '    str = str & "<td valign='top' align='center'> <ul id='x' class='sortable boxy'>"
                '    Dim i As New Integer
                '    i = 0
                '    While (i < dtTable.Rows.Count)
                '        str = str & "<li id='" + dtTable.Rows(i).Item("numRelProID").ToString + "' >" + dtTable.Rows(i).Item("ProName") + "</li>"
                '        i = i + 1
                '    End While
                '    str = str & "</ul> </td> </tr></table></td></tr></table>"
                '    'Response.Write(str)
                '    lblMain.Text = str
                'End If


            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        Private Sub btnSort_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSort.Click
            Try
                Binddata(True)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub
    End Class
End Namespace