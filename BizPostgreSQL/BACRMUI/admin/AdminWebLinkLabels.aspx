<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" MasterPageFile="~/common/Popup.Master"
    CodeBehind="AdminWebLinkLabels.aspx.vb" Inherits="BACRM.UserInterface.Admin.AdminWebLinkLabels" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Web Link Labels</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <div class="right-input">
        <div class="input-part">
            <asp:Button ID="btnSaveClose" runat="server" CssClass="button" Text="Save &amp; Close">
            </asp:Button>
            <asp:Button ID="btnClose" runat="server" CssClass="button" Text="Close"></asp:Button>&nbsp;&nbsp;
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageTitle" runat="server" ClientIDMode="Static">
    Web Link Labels
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="Content" runat="server" ClientIDMode="Static">
    <table border="0" cellpadding="1" cellspacing="1" width="600px">
        <tr>
            <td class="normal1" align="right">
                Link 1 Label
            </td>
            <td>
                <asp:TextBox ID="txtLink1Label" MaxLength="100" runat="server" CssClass="signup"></asp:TextBox>
            </td>
            <td class="normal1" align="right">
                Link 2 Label
            </td>
            <td>
                <asp:TextBox ID="txtLink2Label" MaxLength="100" runat="server" CssClass="signup"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="normal1" align="right">
                Link 3 Label
            </td>
            <td>
                <asp:TextBox ID="txtLink3Label" MaxLength="100" runat="server" CssClass="signup"></asp:TextBox>
            </td>
            <td class="normal1" align="right">
                Link 4 Label
            </td>
            <td>
                <asp:TextBox ID="txtLink4Label" MaxLength="100" runat="server" CssClass="signup"></asp:TextBox>
            </td>
        </tr>
    </table>
</asp:Content>
