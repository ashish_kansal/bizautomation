﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmModuleGlobalization.aspx.vb"
    Inherits="BACRM.UserInterface.Admin.frmModuleGlobalization" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register TagPrefix="menu1" TagName="webmenu" Src="../include/webmenu.ascx" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <link rel="stylesheet" href="~/CSS/master.css" type="text/css" />
    <title>Entity Globalization</title>
</head>
<body id="Bdy" runat="server">
    <form id="frmcflds" method="post" runat="server">
    <menu1:webmenu ID="webmenu1" runat="server"></menu1:webmenu>
    <br />
    <table id="Table2" cellspacing="0" cellpadding="0" width="100%" border="0">
        <tr>
            <td valign="bottom">
                <table class="TabStyle">
                    <tr>
                        <td>
                            &nbsp;&nbsp;&nbsp;Entity Globalization&nbsp;&nbsp;&nbsp;
                        </td>
                    </tr>
                </table>
            </td>
            <td class="normal1" align="center">
                Entity Type
                <asp:DropDownList runat="server" AutoPostBack="true" ID="ddlEntity" CssClass="signup">
                </asp:DropDownList>
            </td>
            <td align="right">
                <asp:Button ID="btnSave" Text="Save" CssClass="button" runat="server"></asp:Button>
            </td>
        </tr>
    </table>
    <asp:Table ID="tbl1" CellPadding="0" CellSpacing="0" BorderWidth="1" runat="server"
        Width="100%" CssClass="aspTable" BorderColor="black" GridLines="None" Height="400">
        <asp:TableRow>
            <asp:TableCell VerticalAlign="Top">
                <asp:GridView ID="gvFields" runat="server" AutoGenerateColumns="False" CssClass="dg"
                     AllowSorting="True" Width="100%" DataKeyNames="numFieldId"
                    ItemStyle-HorizontalAlign="Center">
                    <AlternatingRowStyle CssClass="ais" />
                    <RowStyle CssClass="is" />
                    <HeaderStyle CssClass="hs" />
                    <Columns>
                        <asp:BoundField DataField="vcFieldName" Visible="true" HeaderText="Field Name" ItemStyle-Width="20%" />
                        <asp:TemplateField HeaderText="Set Field Name" ItemStyle-Width="25%">
                            <ItemTemplate>
                                <asp:TextBox ID="txtNewFieldName" runat="server" Text='<%# Eval("vcNewFieldName") %>'
                                    CssClass="signup" Width="250px" AutoComplete="OFF" MaxLength="50"></asp:TextBox>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="vcAssociatedControlType" Visible="true" HeaderText="Field Type" />
                        <asp:TemplateField HeaderText="ToolTip">
                            <ItemTemplate>
                                <asp:TextBox ID="txtToolTip" runat="server" Text='<%# Eval("vcToolTip") %>' CssClass="signup"
                                    Width="300px" AutoComplete="OFF" MaxLength="1000"></asp:TextBox>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <PagerStyle ForeColor="#000066" BackColor="White"></PagerStyle>
                </asp:GridView>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
    </form>
</body>
</html>
