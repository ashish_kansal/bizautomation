﻿Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.WebAPI


Namespace BACRM.UserInterface.Admin
    Public Class frmGridColorScheme
        Inherits BACRMPage

        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            Try
                litMessage.Text = ""
                divMessage.Style.Add("display", "none")
                DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ""
                DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "none")

                If Not IsPostBack Then
                    LoadFormList()
                    LoadFieldList()
                    BindColorGrid()
                End If
                litMessage.Text = ""

                btnAdd.Attributes.Add("onclick", "return Save();")
                txtValue1.Attributes.Add("onkeypress", "CheckNumber(2,event);")
                txtValue2.Attributes.Add("onkeypress", "CheckNumber(2,event);")

            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Sub LoadFormList()
            Try
                Dim objConfigWizard As New FormConfigWizard
                objConfigWizard.DomainID = Session("DomainID")
                objConfigWizard.boolAllowGridColor = True

                Dim dtFormsList As DataTable
                dtFormsList = objConfigWizard.getFormList()

                ddlForm.DataSource = dtFormsList
                ddlForm.DataTextField = "vcFormName"
                ddlForm.DataValueField = "numFormId"
                ddlForm.DataBind()

                ddlForm.SelectedIndex = 0
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub LoadFieldList()
            Try
                Dim dtFld As DataTable
                Dim objField As New FormConfigWizard()
                objField.DomainID = Session("DomainID")
                objField.FormID = ddlForm.SelectedValue
                objField.tintType = 1
                dtFld = objField.GetDynamicFormFieldList

                ddlFieldList.DataSource = dtFld
                ddlFieldList.DataTextField = "vcFormFieldName"
                ddlFieldList.DataValueField = "numFormFieldId"
                ddlFieldList.DataBind()

                ddlFieldList.Items.Insert(0, New ListItem("-- Select --", "0"))
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub BindColorGrid()
            Try
                Dim dtTable As DataTable
                Dim objField As New FormConfigWizard()
                objField.DomainID = Session("DomainID")
                objField.FormID = ddlForm.SelectedValue
                objField.tintType = 0
                dtTable = objField.GetDycFieldColorScheme
                Dim numDefaultFieldId As Integer = 0
                If dtTable.Rows.Count > 0 Then
                    numDefaultFieldId = dtTable.Rows(0)("numFieldID")
                    hdnSavedFieldId.Value = numDefaultFieldId
                    For Each d As ListItem In ddlFieldList.Items
                        Dim individualArray As Array
                        individualArray = d.Value.Split("~")
                        If (individualArray(0) = numDefaultFieldId) Then
                            ddlFieldList.ClearSelection()
                            ddlFieldList.Items.FindByValue(d.Value).Selected = True
                            BindFieldItems()
                        End If
                    Next
                    Dim lstRemovedItem As New ListItemCollection

                    For Each d As DataRow In dtTable.Rows
                        For Each item As ListItem In ddlValueTrigger.Items
                            If d("vcFieldValue") = item.Text Then
                                lstRemovedItem.Add(item)
                            End If
                        Next
                    Next
                    For Each d As ListItem In lstRemovedItem
                        ddlValueTrigger.Items.Remove(d)
                    Next
                End If
                gvColorScheme.DataSource = dtTable
                gvColorScheme.DataBind()
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub ddlForm_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles ddlForm.SelectedIndexChanged
            Try
                LoadFieldList()
                BindColorGrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Sub ddlFieldList_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles ddlFieldList.SelectedIndexChanged
            If hdnValidateOtherRowColor.Value = "1" Then
                Dim objField As New FormConfigWizard()
                objField.DomainID = Session("DomainID")
                objField.FormID = ddlForm.SelectedValue
                objField.tintType = 4
                objField.ManageDycFieldColorScheme()
                BindColorGrid()
            End If
            BindFieldItems()
        End Sub
        Sub BindFieldItems()
            Try
                Dim strValue() As String = ddlFieldList.SelectedValue.Split("~")

                If strValue.Length >= 4 Then
                    Dim dtData As DataTable

                    If strValue(1) = "DateField" Then
                        pnlDateField.Visible = True
                        ddlValueTrigger.Visible = False
                    Else
                        pnlDateField.Visible = False
                        ddlValueTrigger.Visible = True
                        If strValue(1) = "SelectBox" Or strValue(1) = "ListBox" Then
                            dtData = GetDropDownData(strValue(4), strValue(3), strValue(2))
                        ElseIf strValue(1) = "CheckBox" Then
                            dtData = New DataTable
                            dtData.Columns.Add("Value")
                            dtData.Columns.Add("Text")
                            Dim dr1 As DataRow
                            dr1 = dtData.NewRow
                            dr1("Value") = "1"
                            dr1("Text") = "Yes"
                            dtData.Rows.Add(dr1)

                            dr1 = dtData.NewRow
                            dr1("Value") = "0"
                            dr1("Text") = "No"
                            dtData.Rows.Add(dr1)
                        End If

                        ddlValueTrigger.Items.Clear()

                        If dtData IsNot Nothing AndAlso dtData.Rows.Count > 0 Then
                            ddlValueTrigger.Items.Clear()

                            ddlValueTrigger.DataSource = dtData
                            ddlValueTrigger.DataTextField = dtData.Columns(1).ColumnName
                            ddlValueTrigger.DataValueField = dtData.Columns(0).ColumnName
                            ddlValueTrigger.DataBind()
                        End If

                        ddlValueTrigger.Items.Insert(0, New ListItem("-- Select --", "0"))
                    End If
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Sub gvColorScheme_RowDataBound(sender As Object, e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvColorScheme.RowDataBound
            Try
                If e.Row.RowType = DataControlRowType.DataRow Then
                    If DataBinder.Eval(e.Row.DataItem, "vcAssociatedControlType") = "DateField" Then
                        Dim val1 As Integer = DataBinder.Eval(e.Row.DataItem, "vcFieldValue")
                        Dim val2 As Integer = DataBinder.Eval(e.Row.DataItem, "vcFieldValue1")

                        If val1 = 0 Then
                            e.Row.Cells(2).Text = "Today"
                        ElseIf val1 < 0 AndAlso val2 < 0 Then
                            If val1 = val2 Then
                                e.Row.Cells(2).Text = val2 * -1 & " days before record date"
                            Else
                                e.Row.Cells(2).Text = val2 * -1 & " to " & val1 * -1 & " days before record date"
                            End If
                        ElseIf val1 > 0 AndAlso val2 > 0 Then
                            If val1 = val2 Then
                                e.Row.Cells(2).Text = val1 & " days after record date"
                            Else
                                e.Row.Cells(2).Text = val1 & " to " & val2 & " days after record date"
                            End If
                        End If
                    End If

                    e.Row.CssClass = DataBinder.Eval(e.Row.DataItem, "vcColorScheme")
                    e.Row.Cells(3).Text = DataBinder.Eval(e.Row.DataItem, "vcColorScheme").ToString.Replace("gv", "")
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Sub gvColorScheme_RowCommand(sender As Object, e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvColorScheme.RowCommand
            Try
                If e.CommandName = "DeleteRow" Then
                    Dim objField As New FormConfigWizard()
                    objField.DomainID = Session("DomainID")
                    objField.FormID = ddlForm.SelectedValue
                    objField.FieldColorSchemeID = CCommon.ToLong(e.CommandArgument)

                    objField.tintType = 2
                    objField.ManageDycFieldColorScheme()

                    BindColorGrid()
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Sub btnAdd_Click(sender As Object, e As System.EventArgs) Handles btnAdd.Click
            Try
                Dim objField As New FormConfigWizard()
                objField.DomainID = Session("DomainID")
                objField.FormID = ddlForm.SelectedValue

                Dim strValue() As String = ddlFieldList.SelectedValue.Split("~")

                objField.FormFieldId = strValue(0)

                If strValue(1) = "DateField" Then

                    If rdbRecordDate.Checked Then
                        objField.strFieldValue = 0
                        objField.strFieldValue1 = 0
                    Else
                        Dim val1, val2 As Integer

                        val1 = txtValue1.Text
                        val2 = txtValue2.Text

                        If ddlAfterBeforeRecordDate.SelectedValue = "Before" Then
                            val1 = val1 * -1
                            val2 = val2 * -1
                        End If

                        objField.strFieldValue = IIf(val1 > val2, val2, val1)
                        objField.strFieldValue1 = IIf(val1 > val2, val1, val2)
                    End If
                ElseIf strValue(2) = "tintSource" AndAlso strValue(4) = 9 Then
                    objField.strFieldValue = ddlValueTrigger.SelectedValue.Split("~")(0)
                    objField.strFieldValue1 = ddlValueTrigger.SelectedValue.Split("~")(1)
                Else
                    objField.strFieldValue = ddlValueTrigger.SelectedValue
                    objField.strFieldValue1 = 0
                End If

                objField.strColorScheme = ddlColorScheme.SelectedValue

                objField.tintType = 1
                objField.ManageDycFieldColorScheme()

                BindColorGrid()
            Catch ex As Exception
                If ex.Message = "Field_DEPENDANT" Then
                    DisplayMessage("Some other field already used for this Form.")
                ElseIf ex.Message = "Value_DEPENDANT" Then
                    DisplayMessage("Selected combination already exists.")
                Else
                    ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                    DisplayError(ex.Message)
                End If
            End Try
        End Sub

        Function GetDropDownData(ListID As Long, ListType As String, DBColumnName As String) As DataTable
            Try
                Dim dtData As New DataTable

                If DBColumnName = "tintSource" AndAlso ListID = 9 Then
                    objCommon.DomainID = Session("DomainID")
                    dtData = objCommon.GetOpportunitySource()
                ElseIf ListID > 0 Then
                    dtData = objCommon.GetMasterListItems(ListID, CCommon.ToLong(HttpContext.Current.Session("DomainID")))
                ElseIf ListType = "U" Then
                    dtData = objCommon.ConEmpList(CCommon.ToLong(HttpContext.Current.Session("DomainID")), False, 0)
                ElseIf ListType = "AG" Then
                    dtData = objCommon.GetGroups()
                ElseIf ListType = "C" Then
                    Dim objCampaign As New BACRM.BusinessLogic.Reports.PredefinedReports
                    objCampaign.byteMode = 2
                    objCampaign.DomainID = CCommon.ToLong(HttpContext.Current.Session("DomainID"))
                    dtData = objCampaign.GetCampaign()
                ElseIf ListType = "DC" Then
                    Dim objCampaign As New BACRM.BusinessLogic.Marketing.Campaign
                    With objCampaign
                        .SortCharacter = "0"
                        .UserCntID = CCommon.ToLong(HttpContext.Current.Session("UserContactID"))
                        .PageSize = 100
                        .TotalRecords = 0
                        .DomainID = CCommon.ToLong(HttpContext.Current.Session("DomainID"))
                        .columnSortOrder = "Asc"
                        .CurrentPage = 1
                        .columnName = "vcECampName"
                        dtData = objCampaign.ECampaignList
                        Dim drow As DataRow = dtData.NewRow
                        drow("numECampaignID") = -1
                        drow("vcECampName") = "-- Disengaged --"
                        dtData.Rows.Add(drow)
                    End With
                ElseIf DBColumnName = "numManagerID" Then
                    objCommon.ContactID = objCommon.ContactID
                    objCommon.charModule = "C"
                    objCommon.GetCompanySpecificValues1()
                    dtData = objCommon.GetManagers(CCommon.ToLong(HttpContext.Current.Session("DomainID")), objCommon.ContactID, objCommon.DivisionID)
                ElseIf DBColumnName = "charSex" Then
                    dtData = New DataTable
                    dtData.Columns.Add("Value")
                    dtData.Columns.Add("Text")
                    Dim dr1 As DataRow
                    dr1 = dtData.NewRow
                    dr1("Value") = "M"
                    dr1("Text") = "Male"
                    dtData.Rows.Add(dr1)

                    dr1 = dtData.NewRow
                    dr1("Value") = "F"
                    dr1("Text") = "Female"
                    dtData.Rows.Add(dr1)
                ElseIf DBColumnName = "tintOppStatus" Then
                    dtData = New DataTable
                    dtData.Columns.Add("numListItemID")
                    dtData.Columns.Add("vcData")

                    Dim row As DataRow = dtData.NewRow
                    row("numListItemID") = 1
                    row("vcData") = "Deal Won"
                    dtData.Rows.Add(row)

                    row = dtData.NewRow
                    row("numListItemID") = 2
                    row("vcData") = "Deal Lost"
                    dtData.Rows.Add(row)
                    dtData.AcceptChanges()
                ElseIf ListType = "WI" Then
                    Dim objAPI As New WebAPI
                    objAPI.Mode = 1
                    dtData = objAPI.GetWebApi
                End If

                Return dtData
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Private Sub DisplayError(ByVal ex As String)
            DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ex
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
        End Sub

        Private Sub DisplayMessage(ByVal ex As String)
            litMessage.Text = ex
            divMessage.Style.Add("display", "")
        End Sub
    End Class
End Namespace