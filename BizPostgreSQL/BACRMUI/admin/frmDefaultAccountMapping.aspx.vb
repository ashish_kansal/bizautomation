﻿Imports BACRM.BusinessLogic.Accounting
Imports BACRM.BusinessLogic.Common
Partial Public Class frmDefaultAccountMapping
    Inherits BACRMPage


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            litMessage.Text = ""
            GetUserRightsForPage(13, 1)

            If m_aryRightsForPage(RIGHTSTYPE.VIEW) = 0 Then
                Response.Redirect("../admin/authentication.aspx?mesg=AS")
            ElseIf m_aryRightsForPage(RIGHTSTYPE.ADD) = 0 Then
                btnSave.Visible = False
            End If

            If Not IsPostBack Then

                BindDropDowns()
                LoadSavedInfo()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
    Private Sub LoadSavedInfo()
        Try
            Dim objCOA As New ChartOfAccounting
            Dim ds As DataSet
            objCOA.DomainID = Session("DomainId")
            ds = objCOA.GetDefaultAccounts()
            ddlARAccount.SelectedValue = 0
            ddlAPAccount.SelectedValue = 0
            ddlSalseTax.SelectedValue = 0
            ddlPurchaseTaxCredit.SelectedValue = 0

            ddlDiscountGiven.SelectedValue = 0
            ddlShippingIncome.SelectedValue = 0
            ddlLateCharges.SelectedValue = 0
            ddlUndepositedFunds.SelectedValue = 0
            ddlEmpPayExp.SelectedValue = 0
            ddlCEmpPayExp.SelectedValue = 0
            ddlCOGs.SelectedValue = 0
            ddlWorkInProgress.SelectedValue = 0
            'ddlCommissionExpense.SelectedValue = 0
            ddlDeductionPayroll.SelectedValue = 0
            ddlReconciliationDiscrepancies.SelectedValue = 0
            ddlUncategorizedExpense.SelectedValue = 0
            ddlUnCategorizedIncome.SelectedValue = 0
            ddlForeignExchange.SelectedValue = 0
            ddlInventoryAdjustment.SelectedValue = 0
            ddlSalesClearing.SelectedValue = 0
            ddlPurchaseClearing.SelectedValue = 0
            ddlPurchasePriceVariance.SelectedValue = 0
            ddlOpeningBalance.SelectedValue = 0

            If ds.Tables(0).Rows.Count > 0 Then
                If ds.Tables(0).Select("chChargeCode='AR'").Length > 0 Then ddlARAccount.SelectedValue = ds.Tables(0).Select("chChargeCode='AR'")(0)("numAccountID")
                If ds.Tables(0).Select("chChargeCode='AP'").Length > 0 Then ddlAPAccount.SelectedValue = ds.Tables(0).Select("chChargeCode='AP'")(0)("numAccountID")
                If ds.Tables(0).Select("chChargeCode='ST'").Length > 0 Then ddlSalseTax.SelectedValue = ds.Tables(0).Select("chChargeCode='ST'")(0)("numAccountID")
                If ds.Tables(0).Select("chChargeCode='PT'").Length > 0 Then ddlPurchaseTaxCredit.SelectedValue = ds.Tables(0).Select("chChargeCode='PT'")(0)("numAccountID")

                If ds.Tables(0).Select("chChargeCode='DG'").Length > 0 Then ddlDiscountGiven.SelectedValue = ds.Tables(0).Select("chChargeCode='DG'")(0)("numAccountID")
                If ds.Tables(0).Select("chChargeCode='SI'").Length > 0 Then ddlShippingIncome.SelectedValue = ds.Tables(0).Select("chChargeCode='SI'")(0)("numAccountID")
                If ds.Tables(0).Select("chChargeCode='LC'").Length > 0 Then ddlLateCharges.SelectedValue = ds.Tables(0).Select("chChargeCode='LC'")(0)("numAccountID")
                If ds.Tables(0).Select("chChargeCode='UF'").Length > 0 Then ddlUndepositedFunds.SelectedValue = ds.Tables(0).Select("chChargeCode='UF'")(0)("numAccountID")
                If ds.Tables(0).Select("chChargeCode='EP'").Length > 0 Then ddlEmpPayExp.SelectedValue = ds.Tables(0).Select("chChargeCode='EP'")(0)("numAccountID")
                If ds.Tables(0).Select("chChargeCode='CP'").Length > 0 Then ddlCEmpPayExp.SelectedValue = ds.Tables(0).Select("chChargeCode='CP'")(0)("numAccountID")
                If ds.Tables(0).Select("chChargeCode='CG'").Length > 0 Then ddlCOGs.SelectedValue = ds.Tables(0).Select("chChargeCode='CG'")(0)("numAccountID")
                If ds.Tables(0).Select("chChargeCode='PL'").Length > 0 Then ddlPayrollLiability.SelectedValue = ds.Tables(0).Select("chChargeCode='PL'")(0)("numAccountID")
                If ds.Tables(0).Select("chChargeCode='WP'").Length > 0 Then ddlWorkInProgress.SelectedValue = ds.Tables(0).Select("chChargeCode='WP'")(0)("numAccountID")

                If ds.Tables(0).Select("chChargeCode='DP'").Length > 0 Then ddlDeductionPayroll.SelectedValue = ds.Tables(0).Select("chChargeCode='DP'")(0)("numAccountID")
                If ds.Tables(0).Select("chChargeCode='RC'").Length > 0 Then ddlReconciliationDiscrepancies.SelectedValue = ds.Tables(0).Select("chChargeCode='RC'")(0)("numAccountID")
                If ds.Tables(0).Select("chChargeCode='UI'").Length > 0 Then ddlUnCategorizedIncome.SelectedValue = ds.Tables(0).Select("chChargeCode='UI'")(0)("numAccountID")
                If ds.Tables(0).Select("chChargeCode='UE'").Length > 0 Then ddlUncategorizedExpense.SelectedValue = ds.Tables(0).Select("chChargeCode='UE'")(0)("numAccountID")
                If ds.Tables(0).Select("chChargeCode='FE'").Length > 0 Then ddlForeignExchange.SelectedValue = ds.Tables(0).Select("chChargeCode='FE'")(0)("numAccountID")
                If ds.Tables(0).Select("chChargeCode='IA'").Length > 0 Then ddlInventoryAdjustment.SelectedValue = ds.Tables(0).Select("chChargeCode='IA'")(0)("numAccountID")
                If ds.Tables(0).Select("chChargeCode='SC'").Length > 0 Then ddlSalesClearing.SelectedValue = ds.Tables(0).Select("chChargeCode='SC'")(0)("numAccountID")
                If ds.Tables(0).Select("chChargeCode='PC'").Length > 0 Then ddlPurchaseClearing.SelectedValue = ds.Tables(0).Select("chChargeCode='PC'")(0)("numAccountID")
                If ds.Tables(0).Select("chChargeCode='PV'").Length > 0 Then ddlPurchasePriceVariance.SelectedValue = ds.Tables(0).Select("chChargeCode='PV'")(0)("numAccountID")
                If ds.Tables(0).Select("chChargeCode='OE'").Length > 0 Then ddlOpeningBalance.SelectedValue = ds.Tables(0).Select("chChargeCode='OE'")(0)("numAccountID")
                If ds.Tables(0).Select("chChargeCode='RE'").Length > 0 Then ddlReimbursable.SelectedValue = ds.Tables(0).Select("chChargeCode='RE'")(0)("numAccountID")

                'If ds.Tables(0).Select("chChargeCode='CE'").Length > 0 Then ddlCommissionExpense.SelectedValue = ds.Tables(0).Select("chChargeCode='CE'")(0)("numAccountID")

            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Private Sub BindDropDowns()
        Try
            'Account Receivables:01010105
            'Account Payable:01020402
            'Billable Time & Expenses:01040102
            'Sales Tax Payable:01020101
            'Discount Given:010301
            'Shipping Income:010302
            'Late Charges:010302
            'UnDeposited Funds:01010101
            Dim objCOA As New ChartOfAccounting
            Dim dtChartAcntDetails As DataTable
            objCOA.DomainID = Session("DomainId")

            objCOA.AccountCode = "0101"
            dtChartAcntDetails = objCOA.GetParentCategory()
            BindCOA(ddlARAccount, dtChartAcntDetails)

            objCOA.AccountCode = "0102"
            dtChartAcntDetails = objCOA.GetParentCategory()
            BindCOA(ddlAPAccount, dtChartAcntDetails)

            objCOA.AccountCode = "0102"
            dtChartAcntDetails = objCOA.GetParentCategory()
            BindCOA(ddlSalseTax, dtChartAcntDetails)
            BindCOA(ddlPurchaseTaxCredit, dtChartAcntDetails)

            Dim dt, dt1 As DataTable
            objCOA.AccountCode = "0103" 'Income
            dt = objCOA.GetParentCategory()

            objCOA.AccountCode = "0104" 'Expense
            dt1 = objCOA.GetParentCategory()
            dt.Merge(dt1)

            BindCOA(ddlDiscountGiven, dt)

            'objCOA.AccountCode = "010301"
            'dtChartAcntDetails = objCOA.GetParentCategory()
            'BindCOA(ddlDiscountGiven, dtChartAcntDetails)

            objCOA.AccountCode = "0103" 'Income
            dtChartAcntDetails = objCOA.GetParentCategory()
            BindCOA(ddlShippingIncome, dtChartAcntDetails)

            objCOA.AccountCode = "0103" 'Income
            dtChartAcntDetails = objCOA.GetParentCategory()
            BindCOA(ddlLateCharges, dtChartAcntDetails)

            objCOA.AccountCode = "0101"
            dtChartAcntDetails = objCOA.GetParentCategory()
            BindCOA(ddlUndepositedFunds, dtChartAcntDetails)

            objCOA.AccountCode = "0106" '"010403"
            dtChartAcntDetails = objCOA.GetParentCategory()
            BindCOA(ddlCOGs, dtChartAcntDetails)


            objCOA.AccountCode = "0101" 'stock accounts
            dtChartAcntDetails = objCOA.GetParentCategory()
            BindCOA(ddlWorkInProgress, dtChartAcntDetails)

            objCOA.AccountCode = "0104"
            dtChartAcntDetails = objCOA.GetParentCategory()
            BindCOA(ddlEmpPayExp, dtChartAcntDetails)

            objCOA.AccountCode = "0104"
            dtChartAcntDetails = objCOA.GetParentCategory()
            BindCOA(ddlCEmpPayExp, dtChartAcntDetails)

            objCOA.AccountCode = "0102"
            dtChartAcntDetails = objCOA.GetParentCategory()
            BindCOA(ddlPayrollLiability, dtChartAcntDetails)

            objCOA.AccountCode = "0102"
            dtChartAcntDetails = objCOA.GetParentCategory()
            BindCOA(ddlDeductionPayroll, dtChartAcntDetails)

            objCOA.AccountCode = "0104"
            dtChartAcntDetails = objCOA.GetParentCategory()
            BindCOA(ddlReconciliationDiscrepancies, dtChartAcntDetails)

            objCOA.AccountCode = "0104"
            dtChartAcntDetails = objCOA.GetParentCategory()
            BindCOA(ddlUncategorizedExpense, dtChartAcntDetails)

            objCOA.AccountCode = "0103"
            dtChartAcntDetails = objCOA.GetParentCategory()
            BindCOA(ddlUnCategorizedIncome, dtChartAcntDetails)

            objCOA.AccountCode = "0104"
            dtChartAcntDetails = objCOA.GetParentCategory()
            BindCOA(ddlForeignExchange, dtChartAcntDetails)

            objCOA.AccountCode = "0104"
            dtChartAcntDetails = objCOA.GetParentCategory()
            objCOA.AccountCode = "0103"
            Dim dtChartAcntDetails1 As DataTable = objCOA.GetParentCategory()
            dtChartAcntDetails.Merge(dtChartAcntDetails1)
            objCOA.AccountCode = "0106"
            dtChartAcntDetails1 = objCOA.GetParentCategory()
            dtChartAcntDetails.Merge(dtChartAcntDetails1)
            BindCOA(ddlInventoryAdjustment, dtChartAcntDetails)

            objCOA.AccountCode = "0101"
            dtChartAcntDetails = objCOA.GetParentCategory()
            BindCOA(ddlSalesClearing, dtChartAcntDetails)

            objCOA.AccountCode = "0102"
            dtChartAcntDetails = objCOA.GetParentCategory()
            BindCOA(ddlPurchaseClearing, dtChartAcntDetails)

            objCOA.AccountCode = "0106"
            dtChartAcntDetails = objCOA.GetParentCategory()
            BindCOA(ddlPurchasePriceVariance, dtChartAcntDetails)

            objCOA.AccountCode = "0105"
            dtChartAcntDetails = objCOA.GetParentCategory()
            BindCOA(ddlOpeningBalance, dtChartAcntDetails)

            objCOA.AccountCode = "0101"
            dtChartAcntDetails = objCOA.GetParentCategory()
            BindCOA(ddlReimbursable, dtChartAcntDetails)

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub BindCOA(ByVal ddl As DropDownList, ByVal dt As DataTable)
        Try
            Dim item As ListItem
            For Each dr As DataRow In dt.Rows
                item = New ListItem()
                item.Text = dr("vcAccountName")
                item.Value = dr("numAccountID")
                item.Attributes("OptionGroup") = dr("vcAccountType")
                ddl.Items.Add(item)
            Next

            ddl.Items.Insert(0, New ListItem("--Select One --", "0"))
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            If Not CCommon.ToLong(ddlSalesClearing.SelectedValue) > 0 Then
                litMessage.Text = "Please choose sales clearing account"
                ddlSalesClearing.Focus()
                Exit Sub
            End If

            Dim ds As New DataSet
            Dim dt As New DataTable
            Dim dr As DataRow
            dt.Columns.Add("chChargeCode")
            dt.Columns.Add("numAccountID")

            dr = dt.NewRow
            dr(0) = "AR"
            dr(1) = ddlARAccount.SelectedValue
            If CCommon.ToLong(dr(1)) > 0 Then dt.Rows.Add(dr)

            dr = dt.NewRow
            dr(0) = "AP"
            dr(1) = ddlAPAccount.SelectedValue
            If CCommon.ToLong(dr(1)) > 0 Then dt.Rows.Add(dr)

            'dr = dt.NewRow
            'dr(0) = "BE"
            'dr(1) = ddlBillable.SelectedValue
            'dt.Rows.Add(dr)

            dr = dt.NewRow
            dr(0) = "DG"
            dr(1) = ddlDiscountGiven.SelectedValue
            If CCommon.ToLong(dr(1)) > 0 Then dt.Rows.Add(dr)

            dr = dt.NewRow
            dr(0) = "LC"
            dr(1) = ddlLateCharges.SelectedValue
            If CCommon.ToLong(dr(1)) > 0 Then dt.Rows.Add(dr)

            dr = dt.NewRow
            dr(0) = "ST"
            dr(1) = ddlSalseTax.SelectedValue
            If CCommon.ToLong(dr(1)) > 0 Then dt.Rows.Add(dr)

            dr = dt.NewRow
            dr(0) = "PT"
            dr(1) = ddlPurchaseTaxCredit.SelectedValue
            If CCommon.ToLong(dr(1)) > 0 Then dt.Rows.Add(dr)

            dr = dt.NewRow
            dr(0) = "SI"
            dr(1) = ddlShippingIncome.SelectedValue
            If CCommon.ToLong(dr(1)) > 0 Then dt.Rows.Add(dr)

            dr = dt.NewRow
            dr(0) = "UF"
            dr(1) = ddlUndepositedFunds.SelectedValue
            If CCommon.ToLong(dr(1)) > 0 Then dt.Rows.Add(dr)

            dr = dt.NewRow
            dr(0) = "CG"
            dr(1) = ddlCOGs.SelectedValue
            If CCommon.ToLong(dr(1)) > 0 Then dt.Rows.Add(dr)

            dr = dt.NewRow
            dr(0) = "WP"
            dr(1) = ddlWorkInProgress.SelectedValue
            If CCommon.ToLong(dr(1)) > 0 Then dt.Rows.Add(dr)

            dr = dt.NewRow
            dr(0) = "EP"
            dr(1) = ddlEmpPayExp.SelectedValue
            If CCommon.ToLong(dr(1)) > 0 Then dt.Rows.Add(dr)

            dr = dt.NewRow
            dr(0) = "CP"
            dr(1) = ddlCEmpPayExp.SelectedValue
            If CCommon.ToLong(dr(1)) > 0 Then dt.Rows.Add(dr)

            dr = dt.NewRow
            dr(0) = "PL"
            dr(1) = ddlPayrollLiability.SelectedValue
            If CCommon.ToLong(dr(1)) > 0 Then dt.Rows.Add(dr)

            dr = dt.NewRow
            dr(0) = "DP"
            dr(1) = ddlDeductionPayroll.SelectedValue
            If CCommon.ToLong(dr(1)) > 0 Then dt.Rows.Add(dr)

            dr = dt.NewRow
            dr(0) = "RC"
            dr(1) = ddlReconciliationDiscrepancies.SelectedValue
            If CCommon.ToLong(dr(1)) > 0 Then dt.Rows.Add(dr)

            dr = dt.NewRow
            dr(0) = "UI"
            dr(1) = ddlUnCategorizedIncome.SelectedValue
            If CCommon.ToLong(dr(1)) > 0 Then dt.Rows.Add(dr)

            dr = dt.NewRow
            dr(0) = "UE"
            dr(1) = ddlUncategorizedExpense.SelectedValue
            If CCommon.ToLong(dr(1)) > 0 Then dt.Rows.Add(dr)

            dr = dt.NewRow
            dr(0) = "FE"
            dr(1) = ddlForeignExchange.SelectedValue
            If CCommon.ToLong(dr(1)) > 0 Then dt.Rows.Add(dr)

            dr = dt.NewRow
            dr(0) = "IA"
            dr(1) = ddlInventoryAdjustment.SelectedValue
            If CCommon.ToLong(dr(1)) > 0 Then dt.Rows.Add(dr)

            dr = dt.NewRow
            dr(0) = "SC"
            dr(1) = ddlSalesClearing.SelectedValue
            If CCommon.ToLong(dr(1)) > 0 Then dt.Rows.Add(dr)

            dr = dt.NewRow
            dr(0) = "PC"
            dr(1) = ddlPurchaseClearing.SelectedValue
            If CCommon.ToLong(dr(1)) > 0 Then dt.Rows.Add(dr)

            dr = dt.NewRow
            dr(0) = "PV"
            dr(1) = ddlPurchasePriceVariance.SelectedValue
            If CCommon.ToLong(dr(1)) > 0 Then dt.Rows.Add(dr)

            dr = dt.NewRow
            dr(0) = "OE"
            dr(1) = ddlOpeningBalance.SelectedValue
            If CCommon.ToLong(dr(1)) > 0 Then dt.Rows.Add(dr)

            'dr = dt.NewRow
            'dr(0) = "CE"
            'dr(1) = ddlCommissionExpense.SelectedValue
            'dt.Rows.Add(dr)

            dr = dt.NewRow
            dr(0) = "RE"
            dr(1) = ddlReimbursable.SelectedValue
            dt.Rows.Add(dr)

            ds.Tables.Add(dt.Copy)

            Dim objCOA As New ChartOfAccounting
            objCOA.DomainID = Session("DomainId")
            objCOA.Str = ds.GetXml()
            objCOA.ManageDefaultAccountsForDomain()

        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
End Class