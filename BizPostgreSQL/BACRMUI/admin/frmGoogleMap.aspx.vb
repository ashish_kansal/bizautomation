﻿Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Common
Public Class frmGoogleMap
    Inherits BACRMPage
    Dim strContactIds As String = ""
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            strContactIds = GetQueryStringVal("CntIds")
            If Not IsPostBack Then
                If Session("Google_Miles") IsNot Nothing AndAlso Session("Google_ZipCode") IsNot Nothing Then


                    hfRadius.Value = Session("Google_Miles") * 1609.344

                    Dim objSearch As New CSearch
                    objSearch.DomainID = Session("DomainId")
                    objSearch.StrItems = strContactIds

                    Dim dtTableInfo As DataTable
                    dtTableInfo = objSearch.GetGoogleMap(Session("Google_Miles"), Session("Google_ZipCode"))

                    Dim objPageControls As New PageControls
                    Dim strValidation As String = objPageControls.GenerateGoogleMapScript(dtTableInfo)
                    ClientScript.RegisterClientScriptBlock(Me.GetType, "GoogleMapScript", strValidation, True)
                Else
                    litMessage.Text = "Miles and ZipCode is must required to use Map.Please enter Miles and ZipCode and try again!!!"
                End If
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

End Class