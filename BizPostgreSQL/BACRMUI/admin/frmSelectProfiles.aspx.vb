Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Common
Namespace BACRM.UserInterface.Admin
    Public Class frmSelectProfiles
        Inherits BACRMPage

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub
        Protected WithEvents btnClose As System.Web.UI.WebControls.Button
        Protected WithEvents Table2 As System.Web.UI.WebControls.Table
        Protected WithEvents litMessage As System.Web.UI.WebControls.Literal
        Protected WithEvents btnAdd As System.Web.UI.WebControls.Button
        Protected WithEvents dgReProfile As System.Web.UI.WebControls.DataGrid
        Protected WithEvents ddlRelationship As System.Web.UI.WebControls.DropDownList
        Protected WithEvents ddlProfile As System.Web.UI.WebControls.DropDownList
        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.
        Private designerPlaceholderDeclaration As System.Object

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region
       

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Try
                If Not IsPostBack Then
                    
                    
                    GetUserRightsForPage(13, 22)
                    If m_aryRightsForPage(RIGHTSTYPE.VIEW) = 0 Then Response.Redirect("../admin/authentication.aspx?mesg=AS")
                    objCommon.sb_FillComboFromDBwithSel(ddlRelationship, 5, Session("DomainID"))
                    objCommon.sb_FillComboFromDBwithSel(ddlProfile, 21, Session("DomainID"))
                    BindGrid()
                End If
                litMessage.Text = ""
                btnClose.Attributes.Add("onclick", "return Close()")
                btnAdd.Attributes.Add("onclick", "return Save()")
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Sub BindGrid()
            Try
                Dim objUserAccess As New UserAccess
                objUserAccess.RelID = ddlRelationship.SelectedItem.Value
                objUserAccess.DomainID = Session("DomainID")
                dgReProfile.DataSource = objUserAccess.GetRelProfileD
                dgReProfile.DataBind()
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
            Try
                Dim objUserAccess As New UserAccess
                objUserAccess.DomainID = Session("DomainID")
                objUserAccess.RelID = ddlRelationship.SelectedValue
                objUserAccess.ProfileID = ddlProfile.SelectedItem.Value

                objUserAccess.ManageRelProfile()
                Call BindGrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub dgReProfile_EditCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgReProfile.EditCommand
            Try
                dgReProfile.EditItemIndex = e.Item.ItemIndex
                Call BindGrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub ddlRelationship_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlRelationship.SelectedIndexChanged
            Try
                BindGrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub dgReProfile_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgReProfile.ItemDataBound
            Try
                If e.Item.ItemType = ListItemType.EditItem Then
                    Dim ddlRel, ddlPro As DropDownList
                    Dim lblRel, lblPro As Label
                    ddlRel = e.Item.FindControl("ddlERelationship")
                    ddlPro = e.Item.FindControl("ddlEProfile")
                    lblPro = e.Item.FindControl("lblEProfile")
                    lblRel = e.Item.FindControl("lblERel")
                    
                    objCommon.sb_FillComboFromDBwithSel(ddlRel, 5, Session("DomainID"))
                    objCommon.sb_FillComboFromDBwithSel(ddlPro, 21, Session("DomainID"))
                    ddlRel.Items.FindByValue(lblRel.Text).Selected = True
                    ddlPro.Items.FindByValue(lblPro.Text).Selected = True
                    Dim lnkbtnUpdt As LinkButton

                    lnkbtnUpdt = e.Item.FindControl("lnkbtnUpdt")
                    lnkbtnUpdt.Attributes.Add("onclick", "return ESave('" & ddlRel.ClientID & "','" & ddlPro.ClientID & "')")
                End If
                If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
                    Dim btn As Button
                    btn = e.Item.FindControl("btnDelete")
                    btn.Attributes.Add("onclick", "return DeleteRecord()")
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub dgReProfile_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgReProfile.ItemCommand
            Try
                If e.CommandName = "Cancel" Then
                    dgReProfile.EditItemIndex = e.Item.ItemIndex
                    dgReProfile.EditItemIndex = -1
                    Call BindGrid()
                End If
                If e.CommandName = "Delete" Then
                    Dim objUserAccess As New UserAccess
                    objUserAccess.RelProID = CType(e.Item.FindControl("numRelProID"), Label).Text
                    If Not objUserAccess.DeleteRelProfile = True Then litMessage.Text = "Depenedent Record exists.Cannot be deleted."
                    Call BindGrid()
                End If
                If e.CommandName = "Update" Then
                    Dim objUserAccess As New UserAccess
                    objUserAccess.RelProID = CType(e.Item.FindControl("numRelProID"), Label).Text
                    objUserAccess.RelID = CType(e.Item.FindControl("ddlERelationship"), DropDownList).SelectedItem.Value
                    objUserAccess.ProfileID = CType(e.Item.FindControl("ddlEProfile"), DropDownList).SelectedItem.Value
                    objUserAccess.DomainID = Session("DomainID")
                    objUserAccess.ManageRelProfile()
                    dgReProfile.EditItemIndex = -1
                    Call BindGrid()
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click
            Try
                BindGrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

    End Class
End Namespace
