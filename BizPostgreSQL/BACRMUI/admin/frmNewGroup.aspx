<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" CodeBehind="frmNewGroup.aspx.vb"
    Inherits="BACRM.UserInterface.Admin.frmNewGroup" MasterPageFile="~/common/PopupBootstrap.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>New Group</title>
    <script type="text/javascript">
        function Close() {
            window.close()
            return false;
        }
        function DeleteRecord() {
            var str;
            str = 'Are you sure, you want to delete the selected record?'
            if (confirm(str)) {
                return true;
            }
            else {
                return false;
            }
        }
        function DeleteMessage() {
            alert("You Are not Authorized to Delete the Selected Record !");
            return false;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <div class="row padbottom10">
        <div class="col-xs-12">
            <div class="pull-right">
                <asp:Button ID="btnClose" runat="server" CssClass="btn btn-primary" Text="Close"></asp:Button>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageTitle" runat="server" ClientIDMode="Static">
    Manage Permission Groups
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="Content" runat="server" ClientIDMode="Static">
    <div class="row padbottom10">
        <div class="col-xs-12">
            <div class="form-inline">
                <div class="form-group">
                    <label>Permission Group</label>
                    <asp:TextBox ID="txtGroup" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
                <div class="form-group">
                    <label>Group Type</label>
                    <asp:DropDownList ID="ddlGroupType" runat="server" CssClass="form-control">
                        <asp:ListItem Value="1">Internal Access</asp:ListItem>
                        <asp:ListItem Value="2">Portal Access</asp:ListItem>
                        <asp:ListItem Value="3">Partner Access</asp:ListItem>
                        <%-- IMPORTANT - DO NOT USE VALUE 4,5 & 6 --%>
                    </asp:DropDownList>
                </div>
                <asp:Button ID="btnAdd" runat="server" Text="Add Group" CssClass="btn btn-primary"></asp:Button>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="table-responsive">
                <asp:DataGrid ID="dgGroups" runat="server" CssClass="table table-bordered table-striped" UseAccessibleHeader="true" AutoGenerateColumns="False"
                    AllowSorting="True" Width="100%">
                    <Columns>
                        <asp:BoundColumn Visible="false" DataField="bitConsFlag"></asp:BoundColumn>
                         <asp:TemplateColumn>
                            <ItemTemplate>
                                <asp:LinkButton runat="server" Text="Edit" CommandName="Edit" ID="lnkbtnEdt"></asp:LinkButton>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:LinkButton ID="lnkbtnUpdt" runat="server" Text="Update" CommandName="Update"></asp:LinkButton>&nbsp;
                                <asp:LinkButton runat="server" Text="Cancel" CommandName="Cancel" ID="lnkbtnCncl"></asp:LinkButton>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn>
                            <ItemTemplate>
                                <asp:Label ID="lblGroupID" runat="server" Visible="False" Text='<%# DataBinder.Eval(Container,"DataItem.numGroupID") %>'>
                                </asp:Label>
                                <%# Container.ItemIndex +1 %>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Group">
                            <ItemTemplate>
                                <asp:Label ID="lblGroup" runat="server" Text='<%# DataBinder.Eval(Container,"DataItem.vcGroupName") %>'>
                                </asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtEGroup" runat="server" CssClass="signup" Text='<%# DataBinder.Eval(Container,"DataItem.vcGroupName") %>'>
                                </asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Group Type">
                            <ItemTemplate>
                                <asp:Label ID="lblGroupType" runat="server" Text='<%# DataBinder.Eval(Container,"DataItem.GroupType") %>'>
                                </asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:Label ID="lblEGroupType" Visible="False" runat="server" Text='<%# DataBinder.Eval(Container,"DataItem.tinTGroupType") %>'>
                                </asp:Label>
                                <asp:DropDownList ID="ddlEGroupType" runat="server" CssClass="signup">
                                    <asp:ListItem Value="1">Internal Access</asp:ListItem>
                                    <asp:ListItem Value="2">Portal Access</asp:ListItem>
                                    <asp:ListItem Value="2">Partner Access</asp:ListItem>
                                </asp:DropDownList>
                            </EditItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderStyle-Width="10" ItemStyle-Width="10">
                            <ItemTemplate> 
                                <asp:LinkButton ID="lnkdelete" runat="server" CssClass="btn btn-xs btn-danger" CommandName="Delete" OnClientClick="return DeleteRecord();"><i class="fa fa-trash-o"></i></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                </asp:DataGrid>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 text-center">
                <asp:Literal ID="litMessage" runat="server" EnableViewState="False"></asp:Literal>
         </div>
    </div>
</asp:Content>
