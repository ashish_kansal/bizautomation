﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmContactAddressDefault.aspx.vb"
    Inherits=".frmContactAddressDefault" MasterPageFile="~/common/Popup.Master" ClientIDMode="Predictable" %>

<asp:Content ID="Content3" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>UOM - Unit Of Measurement Conversion</title>
    <script language="javascript" type="text/javascript">
        function Close() {
            window.close();
        }

        function ClientSideClick() {
            var isGrpOneValid = Page_ClientValidate("vgRequired");
            var check = Check();

            var i;
            for (i = 0; i < Page_Validators.length; i++) {
                ValidatorValidate(Page_Validators[i]); //this forces validation in all groups
            }

            if (isGrpOneValid && check) {
                return true;
            }
            else
                return false;
        }

        function Check() {
            if (Number(document.getElementById("hfTotalRow").value) > 0) {
                for (var i = 1; i <= Number(document.getElementById("hfTotalRow").value) ; i++) {
                    if (document.getElementById("ddlUnit_1_" + i).value == document.getElementById("ddlUnit_2_" + i).value) {
                        alert('Base and Conversion Unit are not same!!!');
                        return false;
                    }
                    for (var j = 1; j <= Number(document.getElementById("hfTotalRow").value) ; j++) {
                        if (j != i) {
                            if ((document.getElementById("ddlUnit_1_" + i).value == document.getElementById("ddlUnit_1_" + j).value && document.getElementById("ddlUnit_2_" + i).value == document.getElementById("ddlUnit_2_" + j).value)
                                  || (document.getElementById("ddlUnit_1_" + i).value == document.getElementById("ddlUnit_2_" + j).value && document.getElementById("ddlUnit_2_" + i).value == document.getElementById("ddlUnit_1_" + j).value)) {
                                alert('Select distinct base and conversion unit!!!');
                                return false;
                            }
                        }
                    }
                }
            }
            else {
                return false;
            }

            return true;
        }
    </script>
    <style type="text/css">
        .row
        {
            background-color: #DBE5F1;
            text-align: left;
            color: black;
            font-family: Arial;
            font-size: 8pt;
        }
        .arow
        {
            background-color: #fffff;
            text-align: left;
            color: black;
            font-family: Arial;
            font-size: 8pt;
        }

        input[type='checkbox'] {
            position: relative;
            bottom: 1px;
            vertical-align: middle;
        }

        .tooltip {
            top:0px !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <div class="input-part">
        <div class="right-input">
            <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" 
                OnClientClick="return ClientSideClick()"></asp:Button>
            <asp:Button ID="btnSaveClose" runat="server" CssClass="button" Text="Save &amp; Close"
                 OnClientClick="return ClientSideClick()"></asp:Button>
            <asp:Button ID="btnCancel" runat="server" CssClass="button" Width="50" Text="Close">
            </asp:Button>&nbsp;&nbsp;
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="PageTitle" runat="server" ClientIDMode="Static">
    Contact Address Defaults
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="Content" runat="server" ClientIDMode="Static">
   <table>
        <tr class="normal1" align="right">
                                <td>Auto Populate Address
                                </td>
                                <td align="left" colspan="3">
                                    <asp:CheckBox ID="chkPrimaryAddress" runat="server" Text="" />
                                    &nbsp; When creating a new contact, populate its primary address, with the
                                    <asp:DropDownList ID="ddlAddress" runat="server" CssClass="signup">
                                        <asp:ListItem Text="Bill To" Value="1">
                                        </asp:ListItem>
                                        <asp:ListItem Text="Ship To" Value="2">
                                        </asp:ListItem>
                                    </asp:DropDownList>
                                    address of the parent organization it belongs to.
                                </td>
                            </tr>
   </table>
</asp:Content>
