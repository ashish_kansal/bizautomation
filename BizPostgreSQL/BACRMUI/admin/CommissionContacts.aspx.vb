﻿Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Opportunities
Imports BACRM.BusinessLogic.Admin
Public Class CommissionContacts
    Inherits BACRMPage
    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Try
            CCommon.InitializeOrganizationClientSideTemplate(Session("DomainID"), Session("UserContactID"), radCmbCompany)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then
                bingGrid()
            End If
            btnClose.Attributes.Add("onclick", "return Close()")
            btnAdd.Attributes.Add("onclick", "return Save()")
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
    Sub bingGrid()
        Dim objUser As New UserAccess
        objUser.DomainID = Session("DomainID")

        Dim dt As DataTable = objUser.GetCommissionsContacts
        gvCommissionsContacts.DataSource = dt
        gvCommissionsContacts.DataBind()
    End Sub
    'Private Sub radCmbCompany_SelectedIndexChanged(ByVal o As Object, ByVal e As Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs) Handles radCmbCompany.SelectedIndexChanged
    '    Try
    '        If radCmbCompany.SelectedValue <> "" Then
    '            FillContact(ddlContactId, CInt(radCmbCompany.SelectedValue))
    '        End If
    '    Catch ex As Exception
    '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
    '        Response.Write(ex)
    '    End Try
    'End Sub
    Public Sub FillContact(ByVal ddlCombo As DropDownList, ByVal lngDivision As Long)
        Try
            Dim fillCombo As New COpportunities
            With fillCombo
                .DivisionID = lngDivision
                ddlCombo.DataSource = fillCombo.ListContact().Tables(0).DefaultView()
                ddlCombo.DataTextField = "Name"
                ddlCombo.DataValueField = "numcontactId"
                ddlCombo.DataBind()
            End With
            ddlCombo.Items.Insert(0, New ListItem("---Select One---", "0"))
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        Try
            Dim objUser As New UserAccess
            objUser.DivisionID = radCmbCompany.SelectedValue
            'objUser.ContactID = ddlContactId.SelectedValue
            objUser.ContactID = 0
            objUser.DomainID = Session("DomainID")
            objUser.CreateCommissionsContacts()

            bingGrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub gvCommissionsContacts_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvCommissionsContacts.RowCommand
        Try
            If e.CommandName = "DeleteContact" Then
                Dim objUser As New UserAccess
                objUser.CommContactID = e.CommandArgument
                objUser.DeleteCommissionsContacts()

                bingGrid()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
End Class