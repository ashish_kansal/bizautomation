﻿Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Contacts
Partial Public Class frmDupFieldSettings
    Inherits BACRMPage
    Dim objContact As New CContacts
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            btnSave.Attributes.Add("onclick", "Save()")
            If Not IsPostBack Then
                BindLists()
                
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
    Sub BindLists()
        Try
            Dim ds As DataSet
            objContact = New CContacts
            objContact.DomainID = Session("DomainId")
            objContact.FormId = CCommon.ToInteger(GetQueryStringVal("FormID"))
            objContact.UserCntID = 0
            objContact.ContactType = 0
            ds = objContact.GetColumnConfiguration
            'If (ds.Tables(0).Rows.Count > 0 And ds.Tables(1).Rows.Count > 0) Then
            '    For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
            '        For j As Integer = 0 To ds.Tables(1).Rows.Count - 1
            '            If ds.Tables(0).Rows(i)("numFormFieldID") = ds.Tables(1).Rows(j)("numFormFieldID") Then
            '                ds.Tables(0).Rows(i).Delete()
            '            End If
            '        Next
            '    Next
            'End If
            'ds.AcceptChanges()

            lstSelectedfld.DataSource = ds.Tables(1)
            lstSelectedfld.DataValueField = "numFieldID"
            lstSelectedfld.DataTextField = "vcFieldName"
            lstSelectedfld.DataBind()
            'For i As Integer = 0 To lstSelectedfld.Items.Count - 1
            '    Dim foundRow As DataRow = ds.Tables(0).Rows.Find(lstSelectedfld.Items(i).Value)
            '    If foundRow IsNot Nothing Then
            '        ds.Tables(0).Rows.Remove(foundRow)
            '    End If
            'Next
            'ds.AcceptChanges()
            lstAvailablefld.DataSource = ds.Tables(0)
            lstAvailablefld.DataTextField = "vcFieldName"
            lstAvailablefld.DataValueField = "numFieldID"
            lstAvailablefld.DataBind()

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            If objContact Is Nothing Then objContact = New CContacts

            Dim dsNew As New DataSet
            Dim dtTable As New DataTable
            dtTable.Columns.Add("numFieldID")
            dtTable.Columns.Add("bitCustom")
            dtTable.Columns.Add("tintOrder")
            Dim i As Integer

            Dim dr As DataRow
            Dim str As String()
            str = hdnCol.Value.Split(",")
            For i = 0 To str.Length - 2
                dr = dtTable.NewRow
                dr("numFieldID") = str(i).Split("~")(0)
                dr("bitCustom") = str(i).Split("~")(1)
                dr("tintOrder") = i
                dtTable.Rows.Add(dr)
            Next

            dtTable.TableName = "Table"
            dsNew.Tables.Add(dtTable.Copy)

            objContact.ContactType = 0
            objContact.DomainID = Session("DomainId")
            objContact.UserCntID = 0
            objContact.FormId = CCommon.ToInteger(GetQueryStringVal("FormID"))
            objContact.strXml = dsNew.GetXml
            objContact.SaveContactColumnConfiguration()
            dsNew.Tables.Remove(dsNew.Tables(0))
            BindLists()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
End Class