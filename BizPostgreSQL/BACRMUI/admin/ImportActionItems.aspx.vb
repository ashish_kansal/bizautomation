﻿Imports System.IO
Imports BACRMBUSSLOGIC.BussinessLogic
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Leads
Imports System.Reflection
Imports LumenWorks.Framework.IO.Csv
Imports LumenWorks.Framework.Tests.Unit
Namespace BACRM.UserInterface.Admin

    Public Class ImportActionItems
        Inherits BACRMPage

        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            Try
                strDueDate = "Due Date (" & Session("DateFormat").ToString() & ")"
                If Not IsPostBack Then
                    pgBar.Style.Add("display", "none")
                    Dim objImportWizard As New ImportWizard
                    objImportWizard.DomainId = Session("DomainID")
                    Dim dtUserAssign As DataTable
                    dtUserAssign = objImportWizard.AssignUserDetails
                    ddlAssign.DataSource = dtUserAssign
                    ddlAssign.DataTextField = "vcGivenName"
                    ddlAssign.DataValueField = "numUserId"
                    ddlAssign.DataBind()
                    
                End If
                If Session("FileLocation") <> "" Then displayDropdowns()
                btnUpload.Attributes.Add("onClick", "return checkFileExt()")
                btnDisplay.Attributes.Add("onClick", "return setDllValues()")
                btnImports.Attributes.Add("onClick", "return displayPBar()")
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub btnUpload_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpload.Click
            Try
                If Not txtFile.PostedFile Is Nothing And txtFile.PostedFile.ContentLength > 0 Then
                    pgBar.Style.Add("display", "")
                    Dim fileName As String = System.IO.Path.GetFileName(txtFile.PostedFile.FileName)
                    Dim SaveLocation As String = Server.MapPath("../Documents/Docs") & "\" & fileName
                    Try
                        txtFile.PostedFile.SaveAs(SaveLocation)
                        Session("FileLocation") = SaveLocation
                        litMessage.Text = "The file has been uploaded"
                        createTableSchema()
                        displayDropdowns()

                        pgBar.Style.Add("display", "none")
                    Catch ex As Exception
                        pgBar.Style.Add("display", "none")
                        Throw ex
                    End Try
                Else
                    Session("IntCount") = ""
                    Session("FileLocation") = ""
                    Session("dataTableDestinationObj") = ""
                    Session("dataTableHeadingObj") = ""
                    Session("dllValue") = ""
                    tbldtls.Rows.Clear()
                    litMessage.Text = "Please select a file to upload."
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub
        Const strOrgCont As String = "Non Biz Company ID"
        Const strUniqueContID As String = "Non Biz Contact ID"
        Const strPriority As String = "Action Item Priority"
        Const strType As String = "Action Item Type"
        Const strActivity As String = "Action Item Activity"
        Dim strDueDate As String
        Const strComments As String = "Action Item Comments"
        Const strIsColsed As String = "IsClosed?"
        Const strAssignTo As String = "Assign To"
        Private Sub createTableSchema()
            Try
                Dim dataTableDestination As New DataTable
                Dim dataTableHeading As New DataTable
                Dim dataTable As New DataTable
                dataTableHeading.Columns.Add("Id")
                dataTableHeading.Columns.Add("Destination")
                dataTableHeading.Columns.Add("FType")
                dataTableHeading.Columns.Add("cCtype")
                dataTableHeading.Columns.Add("vcAssociatedControlType")
                dataTableHeading.Columns.Add("vcDbColumnName")
                Dim dataRowHeading As DataRow

                dataRowHeading = dataTableHeading.NewRow
                dataRowHeading("Destination") = strUniqueContID
                dataRowHeading("Id") = 0
                'dataRowHeading("FType") = 0
                'dataRowHeading("cCtype") = "V"
                'dataRowHeading("vcAssociatedControlType") = "SystemField"
                'dataRowHeading("vcDbColumnName") = "numCompanyID"
                dataTableHeading.Rows.Add(dataRowHeading)

                dataRowHeading = dataTableHeading.NewRow
                dataRowHeading("Id") = 1
                dataRowHeading("Destination") = strPriority
                dataTableHeading.Rows.Add(dataRowHeading)

                dataRowHeading = dataTableHeading.NewRow
                dataRowHeading("Id") = 2
                dataRowHeading("Destination") = strType
                dataTableHeading.Rows.Add(dataRowHeading)

                dataRowHeading = dataTableHeading.NewRow
                dataRowHeading("Id") = 3
                dataRowHeading("Destination") = strActivity
                dataTableHeading.Rows.Add(dataRowHeading)

                dataRowHeading = dataTableHeading.NewRow
                dataRowHeading("Id") = 4
                dataRowHeading("Destination") = strDueDate
                dataTableHeading.Rows.Add(dataRowHeading)

                dataRowHeading = dataTableHeading.NewRow
                dataRowHeading("Id") = 5
                dataRowHeading("Destination") = strComments
                dataTableHeading.Rows.Add(dataRowHeading)

                dataRowHeading = dataTableHeading.NewRow
                dataRowHeading("Id") = 6
                dataRowHeading("Destination") = strIsColsed
                dataTableHeading.Rows.Add(dataRowHeading)

                dataRowHeading = dataTableHeading.NewRow
                dataRowHeading("Id") = 7
                dataRowHeading("Destination") = strAssignTo
                dataTableHeading.Rows.Add(dataRowHeading)


                Session("dataTableHeadingObj") = dataTableHeading
                Dim drHead As DataRow
                dataTableDestination.Columns.Add("SlNo")
                dataTableDestination.Columns("SlNo").AutoIncrement = True
                dataTableDestination.Columns("SlNo").AutoIncrementSeed = 1
                dataTableDestination.Columns("SlNo").AutoIncrementStep = 1

                For Each drHead In dataTableHeading.Rows
                    dataTableDestination.Columns.Add(drHead.Item(1))
                Next

                Session("dataTableDestinationObj") = dataTableDestination
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        Private Sub displayDropdowns()
            Try
                Dim dataTableHeading As New DataTable
                Dim fileLocation As String = Session("FileLocation")
                Dim streamReader As New StreamReader(fileLocation)
                Dim intCount As Integer = 0
                Dim intCountFor As Integer = 0
                Dim strSplitHeading() As String
                Dim strLine As String = streamReader.ReadLine()
                Dim dataTableMap As New DataTable
                dataTableMap.Columns.Add("Destination")
                dataTableMap.Columns.Add("ID")
                Try
                    Do While Not strLine Is Nothing
                        Dim dataRowMap As DataRow
                        dataRowMap = dataTableMap.NewRow
                        strSplitHeading = Split(strLine, ",")

                        For intCountFor = 0 To strSplitHeading.Length - 1
                            dataRowMap = dataTableMap.NewRow
                            dataRowMap("Destination") = strSplitHeading(intCountFor).Replace("""", "").ToString
                            dataRowMap("ID") = intCountFor
                            dataTableMap.Rows.Add(dataRowMap)
                        Next
                        Exit Do
                    Loop
                    Dim trHeader As New TableRow
                    Dim trDetail As New TableRow
                    Dim tableHC As TableHeaderCell
                    Dim tableDtl As TableCell
                    Dim drHeading As DataRow
                    intCount = 0
                    dataTableHeading = Session("dataTableHeadingObj")
                    Dim ddlDestination As DropDownList
                    Dim arryList() As String
                    Dim strdllValue As String
                    Dim intArryValue As Integer
                    If Session("dllValue") <> "" Then
                        strdllValue = Session("dllValue")
                        arryList = Split(strdllValue, ",")
                    End If
                    tbldtls.Rows.Clear()
                    For Each drHeading In dataTableHeading.Rows
                        trHeader.CssClass = "hs"
                        tableHC = New TableHeaderCell
                        tableHC.ID = intCount
                        tableHC.Text = drHeading.Item(1)
                        tableHC.CssClass = "normal5"
                        trHeader.Cells.Add(tableHC)
                        tableDtl = New TableCell
                        ddlDestination = New DropDownList
                        ddlDestination.CssClass = "signup"
                        ddlDestination.ID = "ddlDestination" & intCount.ToString
                        ddlDestination.DataSource = dataTableMap
                        ddlDestination.DataTextField = "Destination"
                        ddlDestination.DataValueField = "ID"
                        ddlDestination.DataBind()
                        If Session("dllValue") <> "" Then
                            If arryList.Length > 9 Then
                                intArryValue = CType(arryList(intCount), Integer)
                                ddlDestination.SelectedIndex = intArryValue
                            ElseIf ddlDestination.Items.Count > intCount Then
                                ddlDestination.SelectedIndex = intCount
                            End If
                        ElseIf ddlDestination.Items.Count > intCount Then
                            ddlDestination.SelectedIndex = intCount
                        End If
                        tableDtl.Controls.Add(ddlDestination)
                        trDetail.Cells.Add(tableDtl)
                        intCount += 1
                        trDetail.CssClass = "is"
                        tbldtls.Rows.Add(trDetail)
                        tbldtls.Rows.AddAt(0, trHeader)
                    Next
                    Session("IntCount") = intCount - 1
                    If txtDllValue.Text <> "" Then
                        Session("dllValue") = txtDllValue.Text
                    End If
                    txtDllValue.Text = intCount - 1
                Catch ex As Exception
                    Throw ex
                Finally
                    streamReader.Close()
                End Try

            Catch ex As Exception
                Throw ex
            End Try

        End Sub

        Private Sub btnDisplay_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDisplay.Click
            Try
                Dim dataTableDestination As New DataTable
                Dim dataTableHeading As New DataTable
                Dim fileLocation As String = Session("FileLocation")
                Dim streamReader As StreamReader
                streamReader = File.OpenText(fileLocation)
                Dim intCount As Integer = Session("IntCount")
                Dim intCountFor As Integer = 0
                Dim arryList() As String
                Dim strdllValue As String
                Dim intArryValue As Integer
                Dim intCountSession As Integer = Session("IntCount")
                dataTableDestination = Session("dataTableDestinationObj")
                dataTableDestination.Clear()
                Dim csv As CsvReader = New CsvReader(streamReader, True)
                Try
                    csv.MissingFieldAction = MissingFieldAction.ReplaceByEmpty
                    csv.SupportsMultiline = True

                    strdllValue = Session("dllValue")
                    arryList = Split(strdllValue, ",")


                    Dim dataRowDestination As DataRow
                    Dim drHead As DataRow
                    dataTableHeading = Session("dataTableHeadingObj")
                    Dim i1000 As Integer = 0
                    While (csv.ReadNextRecord())
                        dataRowDestination = dataTableDestination.NewRow
                        intCount = 0
                        For Each drHead In dataTableHeading.Rows
                            intArryValue = CType(arryList(intCount), Integer)
                            dataRowDestination(drHead.Item(1)) = csv(intArryValue).ToString
                            intCount += 1
                        Next
                        Try
                            dataTableDestination.Rows.Add(dataRowDestination)
                        Catch ex As Exception
                            If ex.Message.Contains("constrained to be unique") Then
                                streamReader.Close()
                                litMessage.Text = "Column " & strUniqueContID & " has duplicate values, Your option is ensure values in " & strUniqueContID & " column are unique and upload it again."
                                Exit Sub
                            End If
                            Throw ex
                        End Try

                        i1000 = i1000 + 1
                        If i1000 = 7500 Then ' Modified by Anoop 29/04/2009
                            Exit While
                        End If
                    End While
                    Dim drDetination As DataRow
                    Dim iCount As Integer = 0
                    Dim iCountRow As Integer = 1
                    Dim trDetail As TableRow
                    Dim tableCell As TableCell
                    'tbldtls.Rows.Clear()
                    Dim k As Integer = 0
                    For Each drDetination In dataTableDestination.Rows
                        trDetail = New TableRow
                        If k = 0 Then
                            trDetail.CssClass = "ais"
                            k = 1
                        Else
                            trDetail.CssClass = "is"
                            k = 0
                        End If
                        For iCount = 1 To dataTableDestination.Columns.Count - 1
                            tableCell = New TableCell
                            tableCell.Text = drDetination.Item(iCount)
                            tableCell.CssClass = "normal1"
                            trDetail.Cells.Add(tableCell)
                        Next
                        iCountRow += 1
                        tbldtls.Rows.AddAt(iCountRow, trDetail)
                    Next
                Catch ex As Exception
                    If ex.Message = "An item with the same key has already been added." Then
                        streamReader.Close()
                        litMessage.Text = "Duplicate column names found in CSV file, Your option is to remove duplicate columns and upload it again."
                        Exit Sub
                    End If
                    Response.Write(ex)
                Finally
                    streamReader.Close()
                End Try
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try

        End Sub

        Private Sub btnImports_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnImports.Click
            Dim objLeads As New CLeads
            Dim objImpWzd As New ImportWizard
            Dim objAutoRoutRles As New AutoRoutingRules
            Dim objActionItem As New ActionItem
            Try
                Dim dtAutoRoutingRules As New DataTable
                Dim drAutoRow As DataRow
                Dim ds As New DataSet
                If rdRouting.Checked = True Then
                    dtAutoRoutingRules.TableName = "Table"
                    dtAutoRoutingRules.Columns.Add("vcDbColumnName")
                    dtAutoRoutingRules.Columns.Add("vcDbColumnText")
                End If
                Dim dataTableDestination As New DataTable
                Dim dataTableHeading As New DataTable
                dataTableDestination = Session("dataTableDestinationObj")
                dataTableHeading = Session("dataTableHeadingObj")


                Dim drDestination As DataRow
                Dim drHead As DataRow
                Dim intCount As Integer = 0
                Dim sb As New System.Text.StringBuilder

                For Each drDestination In dataTableDestination.Rows
                    objLeads.DomainID = Session("DomainID")
                    objLeads.byteMode = 1
                    objLeads.NonBizContactID = drDestination(strUniqueContID).ToString()
                    objLeads.ManageImportActionItemReference() 'Load Contact ID and Div ID from Non Biz Contact ID

                    If objLeads.ContactID > 0 And objLeads.DivisionID > 0 Then
                        With objActionItem
                            .CommID = 0
                            .ContactID = objLeads.ContactID
                            .DivisionID = objLeads.DivisionID
                            .UserCntID = Session("UserContactID")
                            .DomainID = Session("DomainID")
                        End With
                        
                        objActionItem.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
                        For Each dr As DataRow In dataTableHeading.Rows

                            Select Case dr("Destination").ToString
                                Case strPriority
                                    objActionItem.Status = objImpWzd.GetStateAndCountry(10, CCommon.ToString(drDestination(dr("Destination").ToString)), Session("DomainID"))
                                Case strType
                                    objActionItem.Task = objImpWzd.GetStateAndCountry(11, CCommon.ToString(drDestination(dr("Destination").ToString)), Session("DomainID"))
                                Case strActivity
                                    objActionItem.Activity = objImpWzd.GetStateAndCountry(12, CCommon.ToString(drDestination(dr("Destination").ToString)), Session("DomainID"))
                                Case strDueDate
                                    objActionItem.StartTime = DateFromFormattedDate(drDestination(dr("Destination").ToString).ToString(), Session("DateFormat"))
                                    objActionItem.EndTime = DateFromFormattedDate(drDestination(dr("Destination").ToString).ToString(), Session("DateFormat"))
                                Case strComments
                                    objActionItem.Details = drDestination(dr("Destination").ToString).ToString().Trim
                                Case strIsColsed
                                    objActionItem.BitClosed = IIf(drDestination(dr("Destination").ToString).ToString().ToLower() = "true", 1, IIf(drDestination(dr("Destination").ToString).ToString().ToLower() = "yes", 1, 0))
                                Case strAssignTo
                                    objActionItem.AssignedTo = objImpWzd.GetStateAndCountry(14, CCommon.ToString(drDestination(dr("Destination").ToString)), Session("DomainID"))
                            End Select
                        Next
                        If objActionItem.Task = 973 Then
                            objActionItem.BitClosed = 1
                        Else : objActionItem.BitClosed = 0
                        End If



                        If rdRouting.Checked = True Then
                            objAutoRoutRles.DomainID = Session("DomainID")
                            dtAutoRoutingRules.Rows.Clear()
                            For Each dr As DataRow In dataTableHeading.Rows
                                drAutoRow = dtAutoRoutingRules.NewRow
                                drAutoRow("vcDbColumnName") = dr("vcDbColumnName")
                                drAutoRow("vcDbColumnText") = drDestination(dr("Destination").ToString).ToString
                                dtAutoRoutingRules.Rows.Add(drAutoRow)
                            Next

                            ds.Tables.Add(dtAutoRoutingRules.Copy)
                            objAutoRoutRles.strValues = ds.GetXml
                            ds.Tables.Remove(ds.Tables(0))
                            objActionItem.UserCntID = objAutoRoutRles.GetRecordOwner
                        Else : objActionItem.UserCntID = ddlAssign.SelectedItem.Value
                        End If


                        If objActionItem.Details.Length > 0 Then 'do not create action item where description doesn't exist
                            objActionItem.SaveCommunicationinfo()
                        End If
                    Else
                        sb.Append(drDestination(strUniqueContID).ToString() + ", ")
                    End If

                    objLeads.ContactID = 0
                    objLeads.DivisionID = 0
                Next
                If sb.ToString.Length > 0 Then
                    litMessage.Text = "Following Non Biz Contact ID(s) were not found in Biz<br> " + sb.ToString()
                Else
                    litMessage.Text = "Records are sucessfully saved into database"
                End If

            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            Finally
                pgBar.Style.Add("display", "none")
                Session("IntCount") = ""
                Session("FileLocation") = ""
                Session("dataTableDestinationObj") = ""
                Session("dataTableHeadingObj") = ""
                Session("dllValue") = ""
                tbldtls.Rows.Clear()
            End Try
        End Sub
    End Class
End Namespace
