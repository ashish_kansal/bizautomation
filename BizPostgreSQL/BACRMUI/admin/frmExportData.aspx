﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmExportData.aspx.vb"
    Inherits=".frmExportData" MasterPageFile="~/common/DetailPage.Master" %>

<%@ Register TagPrefix="BizCalendar" TagName="Calendar" Src="../include/calandar.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <script type="text/javascript">
        function checkAll(field) {
            var chkBoxList = document.getElementById(field);
            var chkBoxCount = chkBoxList.getElementsByTagName("input");
            for (var i = 0; i < chkBoxCount.length; i++) {
                chkBoxCount[i].checked = true;
                if (i == 6) {
                    if (chkBoxCount[i].checked == true) {
                        $('#tblDate').show();
                    }
                    else {
                        $('#tblDate').hide();
                    }
                }
            }
            return false;
        }

        function uncheckAll(field) {
            var chkBoxList = document.getElementById(field);
            var chkBoxCount = chkBoxList.getElementsByTagName("input");
            for (var i = 0; i < chkBoxCount.length; i++) {
                chkBoxCount[i].checked = false;
                if (i == 6) {
                    if (chkBoxCount[i].checked == true) {
                        $('#tblDate').show();
                    }
                    else {
                        $('#tblDate').hide();
                    }
                }
            }
        }

        $(document).ready(function () {
            $('#cblExport_6').change(function () {
                console.log($(this).prop('checked'));
                if ($(this).prop('checked') == true) {
                    $('#tblDate').show();
                }
                else {
                    $('#tblDate').hide();
                }
            });
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="RecordInformationPanel" runat="server"
    ClientIDMode="Static">
    <div class="row padbottom10">
        <div class="col-xs-12">
            <div class="form-inline">
                <div class="form-group">
                    <label>From:</label>
                    <BizCalendar:Calendar ID="calFrom" runat="server" ClientIDMode="AutoID" />
                </div>
                <div class="form-group">
                    <label>To:</label>
                    <BizCalendar:Calendar ID="calTo" runat="server" ClientIDMode="AutoID" />
                </div>
                <asp:UpdatePanel ID="uplExport" runat="server" style="display: inline; margin-left: 5px;">
                    <ContentTemplate>
                        <asp:Button ID="btnSave" runat="server" CssClass="btn btn-primary" Text="Export" />
                    </ContentTemplate>
                    <Triggers>
                        <asp:PostBackTrigger ControlID="btnSave" />
                    </Triggers>
                </asp:UpdatePanel>

                <asp:Button ID="btnBack" runat="server" CssClass="btn btn-default" Text="Back" />
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="RecordActionPanel" runat="server"
    ClientIDMode="Static">
    <asp:Label runat="server" ID="lblMsg" CssClass="normal4" Text="You can ONLY export data between 6-10PM california time" Visible="false"></asp:Label>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="DetailPageTitle" runat="server"
    ClientIDMode="Static">
    Export My Data
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="TabsPlaceHolder" runat="server"
    ClientIDMode="Static">
    <div class="row padbottom10">
        <div class="col-xs-12">
            <div class="pull-left">
                <a href="#" onclick="javascript:checkAll('cblExport')">Select All</a> , <a href="#" onclick="javascript:uncheckAll('cblExport')">None</a>
            </div>
            <div class="pull-right">
                <asp:Literal ID="litLastExport" runat="server" Text=""></asp:Literal>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <asp:CheckBoxList ID="cblExport" runat="server" CssClass="signup">
                <asp:ListItem Text="Contacts & Organization" Value="1"></asp:ListItem>
                <asp:ListItem Text="Sales/Purchase Opportuniry/Orders" Value="2"></asp:ListItem>
                <asp:ListItem Text="Projects" Value="3"></asp:ListItem>
                <asp:ListItem Text="Cases" Value="4"></asp:ListItem>
                <asp:ListItem Text="Knowledge Base Articles" Value="5"></asp:ListItem>
                <asp:ListItem Text="Contracts" Value="6"></asp:ListItem>
                <asp:ListItem Text="General Ledger Transactions" Value="7"></asp:ListItem>
                <asp:ListItem Text="Items" Value="8"></asp:ListItem>
                <asp:ListItem Text="Chart of Accounts" Value="9"></asp:ListItem>
            </asp:CheckBoxList>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="UtilityLinksPanel" runat="server">
</asp:Content>
