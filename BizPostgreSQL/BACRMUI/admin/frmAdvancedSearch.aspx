<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" CodeBehind="frmAdvancedSearch.aspx.vb"
    Inherits="BACRM.UserInterface.Admin.frmAdvancedSearch" MasterPageFile="~/common/DetailPage.Master" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register TagPrefix="BizCalendar" TagName="Calendar" Src="~/include/calandar.ascx" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Src="frmAdvSearchCriteria.ascx" TagName="frmAdvSearchCriteria" TagPrefix="uc1" %>
<%@ Register Src="frmMySavedSearch.ascx" TagName="frmMySavedSearch" TagPrefix="uc2" %>
<%@ Import Namespace="BACRM.BusinessLogic.Admin" %>
<%@ Import Namespace="BACRM.BusinessLogic.Reports" %>
<%@ Import Namespace="BACRM.BusinessLogic.Common" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <meta http-equiv="Page-Enter" content="blendTrans(Duration=0.01)">
    <meta http-equiv="Page-Exit" content="blendTrans(Duration=0.01)">
    <title>Advanced Search</title>
    <script src="../javascript/AdvSearchScripts.js" type="text/javascript"></script>
    <script src="../javascript/AdvSearchColumnCustomization.js" type="text/javascript"></script>
    <script src="../javascript/Validation.js" type="text/javascript"></script>
    <script type="text/javascript">
        function PopupCheck() {
        }
        function Search() {
            var str = '';
            var SelectedValue;
            for (var i = 0; i < document.getElementById("lstSelectedfld").options.length; i++) {
                SelectedValue = document.getElementById("lstSelectedfld").options[i].value;
                str = str + SelectedValue + ','
            }
            document.getElementById("hdnCol").value = str;
        }
        function Add() {
            var ItemID = $find('radItem').get_value();
            if (ItemID != "") {
                for (var j = 0; j < document.getElementById("lstSelectedfld").options.length; j++) {
                    if (ItemID == document.getElementById("lstSelectedfld").options[j].value) {
                        alert("Item is already Added");
                        return false;
                    }
                }
                var myOption;
                myOption = document.createElement("Option");
                myOption.text = $find('radItem').get_text();
                myOption.value = ItemID;
                document.getElementById("lstSelectedfld").options.add(myOption);
            }
            return false;
        }

    </script>
    <script language="vb" runat="server">
        Private Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
            Try
                Dim str As String = ""
                Dim sbFriendlyQuery As New StringBuilder
                str = " and tintCRMType in (" & IIf(cbSearchInLeads.Checked = True, 0, 4) & "," & IIf(cbSearchInProspects.Checked = True, 1, 4) & "," & IIf(cbSearchInAccounts.Checked = True, 2, 4) & ")"

                FormGenericAdvSearch.GenerateUserFriendlySearchString(sbFriendlyQuery, "Search in Leads", "cbSearchInLeads", "Checked", "", "Is")
                FormGenericAdvSearch.GenerateUserFriendlySearchString(sbFriendlyQuery, "Search in Prospects", "cbSearchInProspects", "Checked", "", "Is")
                FormGenericAdvSearch.GenerateUserFriendlySearchString(sbFriendlyQuery, "Search in Accounts", "cbSearchInAccounts", "Checked", "", "Is")

                'reset values
                Session("TimeExpression") = ""
                Session("TimeQuery") = ""
                Session("SlidingDays") = 0
                'Crete/Mofified Date  filter
                If rbFixedDate.Checked = True Then 'fixed date
                    Session("TimeQuery") = GetDateQuery(calFrom.SelectedDate, calTo.SelectedDate)
                    If cbCreatedOnFilteration.Checked Or cbModifiedOnFilteration.Checked Or chkVistedDate.Checked Then
                        FormGenericAdvSearch.GenerateUserFriendlySearchString(sbFriendlyQuery, IIf(cbCreatedOnFilteration.Checked, "Created Date ", "") & IIf(cbModifiedOnFilteration.Checked, "Modified Date ", "") & IIf(chkVistedDate.Checked, "Visited Date ", ""), "rbFixedDate", calFrom.SelectedDate & " and " & calTo.SelectedDate, "", "Between")
                    End If
                ElseIf rbSlidingDate.Checked = True Then 'Sliding date
                    Select Case ddlDateSpan.SelectedValue
                        Case 365
                            Session("TimeQuery") = GetDateQuery(CStr(New Date(Now.Year, 1, 1)), CStr(Date.Today))
                        Case Else
                            Session("TimeQuery") = GetDateQuery(Date.Today.AddDays(-ddlDateSpan.SelectedValue), Date.Today)
                    End Select
                    Session("SlidingDays") = ddlDateSpan.SelectedValue
                    If cbCreatedOnFilteration.Checked Or cbModifiedOnFilteration.Checked Or chkVistedDate.Checked Then
                        FormGenericAdvSearch.GenerateUserFriendlySearchString(sbFriendlyQuery, IIf(cbCreatedOnFilteration.Checked, "Created Date ", "") & IIf(cbModifiedOnFilteration.Checked, "Modified Date ", "") & IIf(chkVistedDate.Checked, "Visited Date ", ""), "ddlDateSpan", ddlDateSpan.SelectedItem.Text, "", "Equal To")
                    End If
                End If

                If ddlMiles.SelectedValue <> 0 And txtzipcode.Text <> "" Then
                    If IsNumeric(txtzipcode.Text) Then
                        str = str & " and $SZ$dbo.funZips(" & txtzipcode.Text & "," & ddlMiles.SelectedValue & ")$EZ$ "

                        FormGenericAdvSearch.GenerateUserFriendlySearchString(sbFriendlyQuery, "Miles", "ddlMiles", ddlMiles.SelectedItem.Text & " From ZipCode " & txtzipcode.Text, "", "")
                        Session("Google_Miles") = ddlMiles.SelectedValue
                        Session("Google_ZipCode") = txtzipcode.Text
                    End If
                End If
                If ddlRelationship.SelectedIndex > 0 Then
                    str = str & " and numCompanyType=" & ddlRelationship.SelectedValue
                    FormGenericAdvSearch.GenerateUserFriendlySearchString(sbFriendlyQuery, "Relationship", "ddlRelationship", ddlRelationship.SelectedItem.Text, "", "Equal To")
                End If
                If ddlLead.SelectedIndex > 0 Then
                    str = str & " and numGrpId=" & ddlLead.SelectedValue
                    FormGenericAdvSearch.GenerateUserFriendlySearchString(sbFriendlyQuery, "Group", "ddlLead", ddlLead.SelectedItem.Text, "", "Equal To")
                End If


                Dim i As Integer
                Dim ControlType, strFieldName, strFieldValue, strFieldID, strPrefix, strFriendlyName, strControlID As String
                Dim dr As System.Data.DataRow

                For i = 0 To dtGenericFormConfig.Rows.Count - 1
                    dr = dtGenericFormConfig.Rows(i)
                    ControlType = dr("vcAssociatedControlType").ToString.ToLower.Replace(" ", "")
                    strFieldName = dr("vcDbColumnName").ToString.Trim.Replace(" ", "_")
                    strFriendlyName = dr("vcFormFieldName").ToString
                    strFieldID = dr("numFormFieldID").ToString.Replace("C", "").Replace("D", "").Replace("R", "")
                    strControlID = dr("numFormFieldID").ToString & "_" & dr("vcDbColumnName").ToString.Trim.Replace(" ", "_")

                    If dr("vcLookBackTableName") = "DivisionMaster" Then
                        strPrefix = "DM."
                    ElseIf dr("vcLookBackTableName") = "AdditionalContactsInformation" Then
                        strPrefix = "ADC."
                    Else
                        strPrefix = ""
                    End If

                    Select Case ControlType
                        Case "selectbox"
                            If strFieldName = "vcProfile" Then
                                strFieldValue = ""
                                Dim chkbl As CheckBoxList = CType(tblSearch.FindControl(strControlID), CheckBoxList)
                                Dim fldText As String = ""


                                Dim strSearchOperator As String = "="
                                Dim intSearchIndex As Integer = hdnSaveSearchCriteria.Value.Trim().IndexOf(strFieldName)
                                Dim strTemp As String
                                If intSearchIndex >= 0 Then
                                    strTemp = hdnSaveSearchCriteria.Value.Trim().Substring(intSearchIndex)
                                    strTemp = strTemp.Substring(0, strTemp.IndexOf("^")).Split("~")(1)

                                    If strTemp = "13" Then
                                        strSearchOperator = "<>"
                                    End If
                                End If

                                For Each item As ListItem In chkbl.Items
                                    If item.Selected Then
                                        strFieldValue = strFieldValue & If(strFieldValue.Length > 0, IIf(strSearchOperator = "<>", " AND ", " OR "), "") & " vcProfile " & strSearchOperator & item.Value
                                        fldText = fldText & item.Text & " or "
                                    End If
                                Next

                                If strSearchOperator = "<>" Then
                                    strFieldValue = strFieldValue.TrimEnd("AND")
                                Else
                                    strFieldValue = strFieldValue.TrimEnd("OR")
                                End If

                                If strFieldValue.Length > 1 Then
                                    sbFriendlyQuery.Append(strFieldValue)
                                    sbFriendlyQuery.Append(" and ")
                                End If
                            Else
                                strFieldValue = IIf(CType(tblSearch.FindControl(strControlID), DropDownList).SelectedIndex > 0, CType(tblSearch.FindControl(strControlID), DropDownList).SelectedValue, 0)
                                If IsNumeric(strFieldValue) AndAlso strFieldValue > 0 Then FormGenericAdvSearch.GenerateUserFriendlySearchString(sbFriendlyQuery, strFriendlyName, strFieldName, CType(tblSearch.FindControl(strControlID), DropDownList).SelectedItem.Text, hdnSaveSearchCriteria.Value.Trim, "Equal To")
                            End If
                        Case "listbox"
                            strFieldValue = ""
                            Dim lstBox As Telerik.Web.UI.RadComboBox = CType(tblSearch.FindControl(strControlID), Telerik.Web.UI.RadComboBox)
                            Dim fldText As String = ""

                            For Each item As RadComboBoxItem In lstBox.CheckedItems
                                strFieldValue = strFieldValue & item.Value & ","
                                fldText = fldText & item.Text & " or "
                            Next

                            strFieldValue = strFieldValue.TrimEnd(",")
                            If strFieldValue.Length > 1 Then FormGenericAdvSearch.GenerateUserFriendlySearchString(sbFriendlyQuery, strFriendlyName, strFieldName, fldText.Trim.TrimEnd("r").TrimEnd("o"), hdnSaveSearchCriteria.Value.Trim, "Equals To")
                        Case "checkbox"
                            strFieldValue = IIf(CType(tblSearch.FindControl(strControlID), DropDownList).SelectedIndex > 0, CType(tblSearch.FindControl(strControlID), DropDownList).SelectedValue, -1)
                            If CCommon.ToLong(strFieldValue) > -1 Then FormGenericAdvSearch.GenerateUserFriendlySearchString(sbFriendlyQuery, strFriendlyName, strFieldName, CType(tblSearch.FindControl(strControlID), DropDownList).SelectedItem.Text, hdnSaveSearchCriteria.Value.Trim, "Equal To")
                        Case "datefield"
                            strFieldValue = CType(tblSearch.FindControl(strControlID), BACRM.Include.calandar).SelectedDate

                            If strFieldName = "vcLastSalesOrderDate" Then
                                If CType(tblSearch.FindControl("hdn_" & strControlID), HiddenField).Value = 11 Then 'between condition is set
                                    If strFieldValue <> "" Then FormGenericAdvSearch.GenerateUserFriendlySearchString(sbFriendlyQuery, strFriendlyName, "(SELECT MAX(bintCreatedDate) AS bintCreatedDate FROM OpportunityMaster WHERE numDomainID = " & Session("DomainID") & " AND numDivisionID=DM.numDivisionID AND tintOppType = 1 AND tintOppStatus = 1)", CType(tblSearch.FindControl("hdn_" & strControlID & "_value"), HiddenField).Value, hdnSaveSearchCriteria.Value.Trim, "Between")
                                Else
                                    If strFieldValue <> "" Then FormGenericAdvSearch.GenerateUserFriendlySearchString(sbFriendlyQuery, strFriendlyName, "(SELECT MAX(bintCreatedDate) AS bintCreatedDate FROM OpportunityMaster WHERE numDomainID = " & Session("DomainID") & " AND numDivisionID=DM.numDivisionID AND tintOppType = 1 AND tintOppStatus = 1)", strFieldValue, hdnSaveSearchCriteria.Value.Trim, "Equal To")
                                End If
                            Else
                                If CType(tblSearch.FindControl("hdn_" & strControlID), HiddenField).Value = 11 Then 'between condition is set
                                    If strFieldValue <> "" Then FormGenericAdvSearch.GenerateUserFriendlySearchString(sbFriendlyQuery, strFriendlyName, strFieldName, CType(tblSearch.FindControl("hdn_" & strControlID & "_value"), HiddenField).Value, hdnSaveSearchCriteria.Value.Trim, "Between")
                                Else
                                    If strFieldValue <> "" Then FormGenericAdvSearch.GenerateUserFriendlySearchString(sbFriendlyQuery, strFriendlyName, strFieldName, strFieldValue, hdnSaveSearchCriteria.Value.Trim, "Equal To")
                                End If
                            End If
                        Case "checkboxlist"
                            strFieldValue = ""
                            Dim chkbl As CheckBoxList = CType(tblSearch.FindControl(strControlID), CheckBoxList)
                            Dim fldText As String = ""

                            For Each item As ListItem In chkbl.Items
                                If item.Selected Then
                                    strFieldValue = strFieldValue & If(strFieldValue.Length > 0, " OR ", "") & " CONCAT(',',Fld_Value,',') ILIKE '%," & item.Value & ",%'"
                                    fldText = fldText & item.Text & " or "
                                End If
                            Next

                            strFieldValue = strFieldValue.TrimEnd("OR")
                            If strFieldValue.Length > 1 Then FormGenericAdvSearch.GenerateUserFriendlySearchString(sbFriendlyQuery, strFriendlyName, strFieldName, fldText.Trim.TrimEnd("r").TrimEnd("o"), hdnSaveSearchCriteria.Value.Trim, "Equals To")
                        Case Else
                            strFieldValue = Replace(CType(tblSearch.FindControl(strControlID), TextBox).Text, "'", "''")
                            If strFieldValue <> "" Then FormGenericAdvSearch.GenerateUserFriendlySearchString(sbFriendlyQuery, strFriendlyName, strFieldName, strFieldValue, hdnSaveSearchCriteria.Value.Trim, IIf(dr("vcFieldDataType").ToString.ToLower = "v", "Contains", "Equal To"))
                    End Select


                    If ControlType = "selectbox" Then 'Dropdown
                        If dr("vcFieldType") = "R" Then
                            If strFieldValue <> "0" Then
                                If strFieldName = "vcProfile" Then
                                    If strFieldValue.Length > 0 Then
                                        str = str & " and (" & strFieldValue & ")"
                                    End If
                                Else
                                    Dim strSqlCondition As String = FormGenericAdvSearch.GetSearchCondition(hdnFilterCriteria.Value, strFieldName, strControlID, strFieldValue, dr("vcFieldDataType"), boolStartingWith:=IIf(rbListSearchContext.SelectedValue = 2, True, False))

                                    If strFieldName = "numCountry" Or strFieldName = "numShipCountry" Or strFieldName = "numBillCountry" Or strFieldName = "numBillShipCountry" Then
                                        If strFieldName = "numShipCountry" Then
                                            str = str & " AND DM.numDivisionID IN (SELECT numRecordID FROM AddressDetails WHERE numDomainID=" & Session("DomainID") & " and tintAddressOf=2 AND tintAddressType=2 and " & strSqlCondition.Replace("numShipCountry", "numCountry") & ")"
                                        ElseIf strFieldName = "numBillCountry" Then
                                            str = str & " AND DM.numDivisionID IN (SELECT numRecordID FROM AddressDetails WHERE numDomainID=" & Session("DomainID") & " and tintAddressOf=2 AND tintAddressType=1 and " & strSqlCondition.Replace("numBillCountry", "numCountry") & ")"
                                        ElseIf strFieldName = "numBillShipCountry" Then
                                            str = str & " AND DM.numDivisionID IN (SELECT numRecordID FROM AddressDetails WHERE numDomainID=" & Session("DomainID") & " and tintAddressOf=2 AND " & strSqlCondition.Replace("numBillShipCountry", "numCountry") & ")"
                                        Else
                                            str = str & " AND ADC.numContactID IN (SELECT numRecordID FROM AddressDetails WHERE numDomainID=" & Session("DomainID") & " and tintAddressOf=1 AND tintAddressType=0 and " & strSqlCondition & ")"
                                        End If

                                    Else
                                        str = str & " and " & strSqlCondition
                                    End If
                                End If
                            End If
                        ElseIf dr("vcFieldType") = "C" Then
                            If strFieldValue <> "0" Then
                                Dim strFilter() As String = hdnFilterCriteria.Value.Split("^")

                                Select Case strFilter(i).Split("~")(1)
                                    Case 12 '"rbEqualToDD"
                                        str = str & " and ADC.numContactID IN (select RecId from CFW_FLD_Values_Cont where Fld_ID =" & strFieldID & " and LOWER(Fld_Value) = LOWER('" & strFieldValue & "') )"
                                    Case 13 '"rbNotEqualToDD"
                                        str = str & " and ADC.numContactID NOT IN (select RecId from CFW_FLD_Values_Cont where Fld_ID =" & strFieldID & " and LOWER(Fld_Value) = LOWER('" & strFieldValue & "') )"
                                    Case Else
                                        str = str & " and ADC.numContactID IN (select RecId from CFW_FLD_Values_Cont where Fld_ID =" & strFieldID & " and LOWER(Fld_Value) = LOWER('" & strFieldValue & "') )"
                                End Select
                            End If
                        ElseIf dr("vcFieldType") = "D" Then
                            If strFieldValue <> "0" Then
                                Dim strFilter() As String = hdnFilterCriteria.Value.Split("^")

                                Select Case strFilter(i).Split("~")(1)
                                    Case 12 '"rbEqualToDD"
                                        str = str & " and DM.numDivisionID IN (select RecId from CFW_FLD_Values where Fld_ID =" & strFieldID & " and LOWER(Fld_Value) = LOWER('" & strFieldValue & "') )"
                                    Case 13 '"rbNotEqualToDD"
                                        str = str & " and DM.numDivisionID NOT IN (select RecId from CFW_FLD_Values where Fld_ID =" & strFieldID & " and LOWER(Fld_Value) = LOWER('" & strFieldValue & "') )"
                                    Case Else
                                        str = str & " and DM.numDivisionID IN (select RecId from CFW_FLD_Values where Fld_ID =" & strFieldID & " and LOWER(Fld_Value) = LOWER('" & strFieldValue & "') )"
                                End Select
                            End If
                        End If
                    ElseIf ControlType = "checkboxlist" Then
                        If Not String.IsNullOrEmpty(strFieldValue) Then
                            If dr("vcFieldType") = "C" Then
                                If strFieldValue <> "0" Then
                                    str = str & " and ADC.numContactID in (select RecId from CFW_FLD_Values_Cont where Fld_ID =" & strFieldID & " AND (" & strFieldValue & "))"
                                End If
                            ElseIf dr("vcFieldType") = "D" Then
                                If strFieldValue <> "0" Then
                                    str = str & " and DM.numDivisionID in (select RecId from CFW_FLD_Values where Fld_ID =" & strFieldID & " AND (" & strFieldValue & "))"
                                End If
                            End If
                        End If
                    ElseIf ControlType = "listbox" Then
                        If strFieldValue.Length > 1 Then
                            If strFieldName = "numState" Or strFieldName = "numShipState" Or strFieldName = "numBillState" Or strFieldName = "numBillShipState" Then
                                If strFieldName = "numShipState" Then
                                    str = str & " AND DM.numDivisionID IN (SELECT numRecordID FROM AddressDetails WHERE numDomainID=" & Session("DomainID") & " and tintAddressOf=2 AND tintAddressType=2 and numState in (" & strFieldValue & ") )"
                                ElseIf strFieldName = "numBillState" Then
                                    str = str & " AND DM.numDivisionID IN (SELECT numRecordID FROM AddressDetails WHERE numDomainID=" & Session("DomainID") & " and tintAddressOf=2 AND tintAddressType=1 and numState in (" & strFieldValue & ") )"
                                ElseIf strFieldName = "numBillShipState" Then
                                    str = str & " AND DM.numDivisionID IN (SELECT numRecordID FROM AddressDetails WHERE numDomainID=" & Session("DomainID") & " and tintAddressOf=2 AND numState in (" & strFieldValue & ") )"
                                Else
                                    str = str & " AND ADC.numContactID IN (SELECT numRecordID FROM AddressDetails WHERE numDomainID=" & Session("DomainID") & " and tintAddressOf=1 AND tintAddressType=0 and numState in (" & strFieldValue & ") )"
                                End If
                            End If
                        End If
                    ElseIf ControlType = "checkbox" Then 'Added by chintan, reason: custom field checkbox code isn't handled
                        If strFieldValue <> "-1" Then
                            If dr("vcFieldType") = "R" Then
                                Dim strSqlCondition As String = FormGenericAdvSearch.GetSearchCondition(hdnFilterCriteria.Value, strFieldName, strControlID, strFieldValue, dr("vcFieldDataType"), boolStartingWith:=IIf(rbListSearchContext.SelectedValue = 2, True, False))
                                str = str & " and " & strSqlCondition
                            ElseIf dr("vcFieldType") = "C" Then
                                Dim strFilter() As String = hdnFilterCriteria.Value.Split("^")

                                Select Case strFilter(i).Split("~")(1)
                                    Case 12 '"rbEqualToDD"
                                        str = str & " and ADC.numContactID IN (select RecId from CFW_FLD_Values_Cont where Fld_ID =" & strFieldID & " and LOWER(Fld_Value) = LOWER('" & strFieldValue & "') )"
                                    Case 13 '"rbNotEqualToDD"
                                        str = str & " and ADC.numContactID NOT IN (select RecId from CFW_FLD_Values_Cont where Fld_ID =" & strFieldID & " and LOWER(Fld_Value) = LOWER('" & strFieldValue & "') )"
                                    Case Else
                                        str = str & " and ADC.numContactID IN (select RecId from CFW_FLD_Values_Cont where Fld_ID =" & strFieldID & " and LOWER(Fld_Value) = LOWER('" & strFieldValue & "') )"
                                End Select
                            ElseIf dr("vcFieldType") = "D" Then
                                Dim strFilter() As String = hdnFilterCriteria.Value.Split("^")

                                Select Case strFilter(i).Split("~")(1)
                                    Case 12 '"rbEqualToDD"
                                        str = str & " and DM.numDivisionID IN (select RecId from CFW_FLD_Values where Fld_ID =" & strFieldID & " and LOWER(Fld_Value) = LOWER('" & strFieldValue & "') )"
                                    Case 13 '"rbNotEqualToDD"
                                        str = str & " and DM.numDivisionID NOT IN (select RecId from CFW_FLD_Values where Fld_ID =" & strFieldID & " and LOWER(Fld_Value) = LOWER('" & strFieldValue & "') )"
                                    Case Else
                                        str = str & " and DM.numDivisionID IN (select RecId from CFW_FLD_Values where Fld_ID =" & strFieldID & " and LOWER(Fld_Value) = LOWER('" & strFieldValue & "') )"
                                End Select
                            End If
                        End If
                    ElseIf ControlType = "datefield" Then
                        If dr("vcFieldType") = "R" Then
                            If CCommon.ToString(strFieldValue).Length > 0 Then
                                If strFieldName = "vcLastSalesOrderDate" Then
                                    str = str & " and " & FormGenericAdvSearch.GetSearchCondition(hdnFilterCriteria.Value, "(SELECT MAX(bintCreatedDate) AS bintCreatedDate FROM OpportunityMaster WHERE numDomainID = " & Session("DomainID") & " AND numDivisionID=DM.numDivisionID AND tintOppType = 1 AND tintOppStatus = 1)", strControlID, strFieldValue, dr("vcFieldDataType"), TablePrefix:=strPrefix, boolStartingWith:=IIf(rbListSearchContext.SelectedValue = 2, True, False))
                                Else
                                    str = str & " and " & FormGenericAdvSearch.GetSearchCondition(hdnFilterCriteria.Value, strFieldName, strControlID, strFieldValue, dr("vcFieldDataType"), TablePrefix:=strPrefix, boolStartingWith:=IIf(rbListSearchContext.SelectedValue = 2, True, False))
                                End If
                            End If
                        ElseIf dr("vcFieldType") = "D" Then
                            If CCommon.ToString(strFieldValue).Length > 0 Then
                                str = str & " and DM.numDivisionID in (select RecId from CFW_FLD_Values where Fld_ID =" & strFieldID & " and " & FormGenericAdvSearch.GetSearchCondition(hdnFilterCriteria.Value, strFieldName, strControlID, strFieldValue, dr("vcFieldDataType"), IsCustomField:=True, boolStartingWith:=IIf(rbListSearchContext.SelectedValue = 2, True, False)) & ")"
                            End If
                        End If
                    Else 'Textbox
                        If CType(tblSearch.FindControl(strControlID), TextBox).Text <> "" Then
                            If dr("vcFieldType") = "R" Then
                                If strFieldName = "numAge" Then
                                    If IsNumeric(CType(tblSearch.FindControl(strControlID), TextBox).Text.Replace(" ", "").Replace("to", "")) = False Then
                                    Else : str = str & " and " & FormGenericAdvSearch.GetSearchCondition(hdnFilterCriteria.Value, strFieldName, strControlID, "", dr("vcFieldDataType"), boolStartingWith:=IIf(rbListSearchContext.SelectedValue = 2, True, False)) & ""
                                    End If
                                ElseIf strFieldName = "vcPostalCode" Or strFieldName = "vcShipPostCode" Or strFieldName = "vcBillPostCode" Then 'ShipPostal/BillPostal
                                    If CType(tblSearch.FindControl(strControlID), TextBox).Text <> "" Then
                                        Dim strSqlCondition As String = FormGenericAdvSearch.GetSearchCondition(hdnFilterCriteria.Value, strFieldName, strControlID, strFieldValue, dr("vcFieldDataType"), boolStartingWith:=IIf(rbListSearchContext.SelectedValue = 2, True, False))
                                        If strFieldName = "vcShipPostCode" Then
                                            str = str & " AND DM.numDivisionID IN (SELECT numRecordID FROM AddressDetails WHERE numDomainID=" & Session("DomainID") & " and tintAddressOf=2 AND tintAddressType=2 and " & strSqlCondition.Replace("vcShipPostalCode", "vcPostalCode").Replace("vcShipPostCode", "vcPostalCode") & ")"
                                        ElseIf strFieldName = "vcBillPostCode" Then
                                            str = str & " AND DM.numDivisionID IN (SELECT numRecordID FROM AddressDetails WHERE numDomainID=" & Session("DomainID") & " and tintAddressOf=2 AND tintAddressType=1 and " & strSqlCondition.Replace("vcBillPostalCode", "vcPostalCode").Replace("vcBillPostCode", "vcPostalCode") & ")"
                                        Else
                                            str = str & " AND ADC.numContactID IN (SELECT numRecordID FROM AddressDetails WHERE numDomainID=" & Session("DomainID") & " and tintAddressOf=1 AND tintAddressType=0 and " & strSqlCondition & ")"
                                        End If
                                    End If
                                ElseIf strFieldName = "vcCity" Or strFieldName = "vcShipCity" Or strFieldName = "vcBillCity" Then
                                    If CType(tblSearch.FindControl(strControlID), TextBox).Text <> "" Then
                                        Dim strSqlCondition As String = FormGenericAdvSearch.GetSearchCondition(hdnFilterCriteria.Value, strFieldName, strControlID, strFieldValue, dr("vcFieldDataType"), boolStartingWith:=IIf(rbListSearchContext.SelectedValue = 2, True, False))
                                        If strFieldName = "vcShipCity" Then
                                            str = str & " AND DM.numDivisionID IN (SELECT numRecordID FROM AddressDetails WHERE numDomainID=" & Session("DomainID") & " and tintAddressOf=2 AND tintAddressType=2 and " & strSqlCondition.Replace("vcShipCity", "vcCity") & ")"
                                        ElseIf strFieldName = "vcBillCity" Then
                                            str = str & " AND DM.numDivisionID IN (SELECT numRecordID FROM AddressDetails WHERE numDomainID=" & Session("DomainID") & " and tintAddressOf=2 AND tintAddressType=1 and " & strSqlCondition.Replace("vcBillCity", "vcCity") & ")"
                                        Else
                                            str = str & " AND ADC.numContactID IN (SELECT numRecordID FROM AddressDetails WHERE numDomainID=" & Session("DomainID") & " and tintAddressOf=1 AND tintAddressType=0 and " & strSqlCondition & ")"
                                        End If
                                    End If
                                ElseIf strFieldName = "vcStreet" Or strFieldName = "vcShipStreet" Or strFieldName = "vcBillStreet" Then
                                    If CType(tblSearch.FindControl(strControlID), TextBox).Text <> "" Then
                                        Dim strSqlCondition As String = FormGenericAdvSearch.GetSearchCondition(hdnFilterCriteria.Value, strFieldName, strControlID, strFieldValue, dr("vcFieldDataType"), boolStartingWith:=IIf(rbListSearchContext.SelectedValue = 2, True, False))
                                        If strFieldName = "vcShipStreet" Then
                                            str = str & " AND DM.numDivisionID IN (SELECT numRecordID FROM AddressDetails WHERE numDomainID=" & Session("DomainID") & " and tintAddressOf=2 AND tintAddressType=2 and " & strSqlCondition.Replace("vcShipStreet", "vcStreet") & ")"
                                        ElseIf strFieldName = "vcBillStreet" Then
                                            str = str & " AND DM.numDivisionID IN (SELECT numRecordID FROM AddressDetails WHERE numDomainID=" & Session("DomainID") & " and tintAddressOf=2 AND tintAddressType=1 and " & strSqlCondition.Replace("vcBillStreet", "vcStreet") & ")"
                                        Else
                                            str = str & " AND ADC.numContactID IN (SELECT numRecordID FROM AddressDetails WHERE numDomainID=" & Session("DomainID") & " and tintAddressOf=1 AND tintAddressType=0 and " & strSqlCondition & ")"
                                        End If
                                    End If
                                Else : str = str & " and " & FormGenericAdvSearch.GetSearchCondition(hdnFilterCriteria.Value, strFieldName, strControlID, strFieldValue, dr("vcFieldDataType"), boolStartingWith:=IIf(rbListSearchContext.SelectedValue = 2, True, False))
                                End If
                            ElseIf dr("vcFieldType") = "C" Then
                                str = str & " and ADC.numContactID in (select RecId from CFW_FLD_Values_Cont where Fld_ID =" & strFieldID & " and " & FormGenericAdvSearch.GetSearchCondition(hdnFilterCriteria.Value, strFieldName, strControlID, strFieldValue, dr("vcFieldDataType"), IsCustomField:=True) & ")"
                            ElseIf dr("vcFieldType") = "D" Then
                                str = str & " and DM.numDivisionID in (select RecId from CFW_FLD_Values where Fld_ID =" & strFieldID & " and " & FormGenericAdvSearch.GetSearchCondition(hdnFilterCriteria.Value, strFieldName, strControlID, strFieldValue, dr("vcFieldDataType"), IsCustomField:=True, boolStartingWith:=IIf(rbListSearchContext.SelectedValue = 2, True, False)) & ")" 
                            End If
                        End If
                    End If
                Next


                If rdlReportType.SelectedValue = 1 Then ' Users
                    Dim objRep As New PredefinedReports
                    Dim dtUserSelected As Data.DataTable
                    objRep.UserCntID = Session("UserContactId")
                    objRep.DomainID = Session("DomainID")
                    objRep.ReportType = 6 ' for advance search
                    dtUserSelected = objRep.GetUsersForForRep()
                    If dtUserSelected.Rows.Count > 0 Then
                        Select Case CCommon.ToInteger(dtUserSelected.Rows(0)("tintUserType"))
                            Case 1 'record owner 
                                str = str & " and DM.numRecOwner in (SELECT numSelectedUserCntID FROM ForReportsByUser where numcontactid=" & Session("UserContactID") & " and numDomainID=" & Session("DomainID") & " and tintType = 6)"
                            Case 2 ' record assignee
                                str = str & " and DM.numAssignedTo in (SELECT numSelectedUserCntID FROM ForReportsByUser where numcontactid=" & Session("UserContactID") & " and numDomainID=" & Session("DomainID") & " and tintType = 6)"
                            Case Else 'record owner or record assignee
                                str = str & " and (DM.numRecOwner in (SELECT numSelectedUserCntID FROM ForReportsByUser where numcontactid=" & Session("UserContactID") & " and numDomainID=" & Session("DomainID") & " and tintType = 6) or DM.numAssignedTo in (SELECT numSelectedUserCntID FROM ForReportsByUser where numcontactid=" & Session("UserContactID") & " and numDomainID=" & Session("DomainID") & " and tintType = 6))"
                        End Select
                    End If
                ElseIf rdlReportType.SelectedValue = 2 Then ' Team
                    str = str & " and DM.numRecOwner in (select  numContactID from AdditionalContactsInformation where numTeam in(select numTeam from ForReportsByTeam where numUserCntID=" & Session("UserContactID") & " and numDomainID=" & Session("DomainID") & " and tintType =6))"
                ElseIf rdlReportType.SelectedValue = 3 Then 'Territory
                    str = str & " and DM.numTerId in (select numTerritory from ForReportsByTerritory where numUserCntID=" & Session("UserContactID") & " and numDomainID=" & Session("DomainID") & " and tintType =6)"
                End If
                If cbOptionOne.Checked = True Then
                    str = str & " and DM.numDivisionID not in (select distinct(numDivisionID) from OpportunityMaster where numDomainID=" & Session("DomainID") & " and bintAccountClosingDate between dateadd(day,-" & IIf(IsNumeric(txtDaysOptionOne.Text) = True, txtDaysOptionOne.Text, 0) & ",getdate()) and getdate())"
                End If
                If cbOptionTwo.Checked = True Then
                    str = str & " and DM.numDivisionID in (select distinct(numDivisionID) from OpportunityMaster where numDomainID=" & Session("DomainID") & " and bintAccountClosingDate between dateadd(day,-" & IIf(IsNumeric(txtDaysOptionTwo.Text) = True, txtDaysOptionTwo.Text, 0) & ",getdate()) and getdate() and monPAmount >" & IIf(IsNumeric(txtDealAmount.Text) = True, txtDealAmount.Text, 0) & ")"
                End If
                If cbOptionThree.Checked = True Then
                    Dim strItemIds As String = ""
                    If hdnCol.Value.Length > 0 Then
                        strItemIds = hdnCol.Value.TrimEnd(",")
                        str = str & " and DM.numDivisionID in (select distinct(OM.numDivisionID) from OpportunityMaster OM INNER JOIN [OpportunityItems] OI ON OM.[numOppId] = OI.[numOppId] where OM.numDomainID=" & Session("DomainID") & " and OM.bintAccountClosingDate between dateadd(day,-" & IIf(IsNumeric(txtDaysOptionThree.Text) = True, txtDaysOptionThree.Text, 0) & ",getdate()) and getdate() and OI.[numItemCode] IN (" & strItemIds & ") ) "
                    End If
                End If

                If ddlActivity.SelectedIndex > 0 Then
                    str = str & " and ADC.numContactID in (select distinct(numcontactId) from communication where numDomainID=" & Session("DomainID") & " and numActivity = " & ddlActivity.SelectedValue & ")"
                    FormGenericAdvSearch.GenerateUserFriendlySearchString(sbFriendlyQuery, "Open Action Item Activity", "ddlActivity", ddlActivity.SelectedItem.Text, "", "Equal To")
                End If

                Dim newsearch As String = ""
                If chkorgsales.Checked Then
                    Dim fromDate As DateTime = Date.Today
                    Dim toDate As DateTime = Date.Today
                    If rbFixedDate.Checked = True AndAlso Not calFrom.SelectedDate Is Nothing AndAlso Not calTo.SelectedDate Is Nothing Then 'fixed date
                        fromDate = Convert.ToDateTime(calFrom.SelectedDate).AddMinutes(-Session("ClientMachineUTCTimeOffset"))
                        toDate = Convert.ToDateTime(calTo.SelectedDate).AddMinutes(-Session("ClientMachineUTCTimeOffset"))
                    ElseIf rbSlidingDate.Checked = True Then 'Sliding date
                        Select Case ddlDateSpan.SelectedValue
                            Case 365
                                fromDate = Convert.ToDateTime(New Date(Now.Year, 1, 1)).AddMinutes(-Session("ClientMachineUTCTimeOffset"))
                                toDate = Convert.ToDateTime(Date.Today).AddMinutes(-Session("ClientMachineUTCTimeOffset"))
                            Case Else
                                fromDate = Convert.ToDateTime(Date.Today.AddDays(-ddlDateSpan.SelectedValue)).AddMinutes(-Session("ClientMachineUTCTimeOffset"))
                                toDate = Convert.ToDateTime(Date.Today).AddMinutes(-Session("ClientMachineUTCTimeOffset"))
                        End Select
                    End If

                    If chkincorg.Checked Then
                        str = str & "  AND (((SELECT COUNT(*) FROM OpportunityMaster WHERE numDomainID=DM.numDomainID AND numDivisionID=DM.numDivisionID AND tintOppType=1 AND tintOppStatus = 1) > 0 AND (SELECT COUNT(*) FROM OpportunityMaster WHERE numDomainID=DM.numDomainID AND numDivisionID=DM.numDivisionID AND tintOppType=1 AND tintOppStatus = 1 AND CAST(bintCreatedDate AS DATE) BETWEEN CAST('" & fromDate & "' AS DATE) AND CAST('" & toDate & "' AS DATE)) = 0)"
                        str = str & " OR "
                        str = str & "  (SELECT COUNT(*) FROM OpportunityMaster WHERE numDomainID=DM.numDomainID AND numDivisionID=DM.numDivisionID AND tintOppType=1 AND tintOppStatus = 1) = 0"
                        str = str & ")"
                    Else
                        str = str & "  AND ((SELECT COUNT(*) FROM OpportunityMaster WHERE numDomainID=DM.numDomainID AND numDivisionID=DM.numDivisionID AND tintOppType=1 AND tintOppStatus = 1) > 0 AND (SELECT COUNT(*) FROM OpportunityMaster WHERE numDomainID=DM.numDomainID AND numDivisionID=DM.numDivisionID AND tintOppType=1 AND tintOppStatus = 1 AND CAST(bintCreatedDate AS DATE) BETWEEN CAST('" & fromDate & "' AS DATE) AND CAST('" & toDate & "' AS DATE)) = 0)"
                    End If
                ElseIf chkincorg.Checked Then
                    str = str & "  AND (SELECT COUNT(*) FROM OpportunityMaster WHERE numDomainID=DM.numDomainID AND numDivisionID=DM.numDivisionID AND tintOppType=1 AND tintOppStatus = 1) = 0"
                End If

                Session("WhereCondition") = str
                Session("SavedSearchCondition") = str
                str = ""
                If Not tblSearch.FindControl("cbListAOI") Is Nothing Then
                    Dim MyItem As ListItem
                    Dim cbl As CheckBoxList
                    cbl = tblSearch.FindControl("cbListAOI")
                    For Each MyItem In cbl.Items
                        If MyItem.Selected = True Then str = str & MyItem.Value & ","
                    Next
                    str = str.TrimEnd(",")
                    Session("AreasOfInt") = str
                End If

                FormGenericAdvSearch.SaveSearchCriteria(hdnSaveSearchCriteria.Value, 1)
                Session("UserFriendlyQuery") = sbFriendlyQuery.ToString.Trim.TrimEnd(New Char() {"d", "n", "a"})

                Response.Redirect("frmAdvancedSearchRes.aspx", False)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub
        Function GetDateQuery(ByVal strFromDate As String, ByVal strToDate As String) As String
            Try
                If strFromDate <> "" And strToDate <> "" And cbCreatedOnFilteration.Checked OrElse cbModifiedOnFilteration.Checked OrElse chkVistedDate.Checked Then
                    Dim strTimeExpression, strDateQuery As String
                    strDateQuery = ""
                    strTimeExpression = ""
                    If cbCreatedOnFilteration.Checked Then 'Created date checked 

                        strTimeExpression = strTimeExpression + " DM.bintCreatedDate between '{FROM}'::TIMESTAMP and '{TO}'::TIMESTAMP "
                        strDateQuery = strDateQuery + " DM.bintCreatedDate between  '" & strFromDate & "'::TIMESTAMP and '" & CDate(strToDate & " 23:59:59") & "'::TIMESTAMP "
                    End If

                    If cbModifiedOnFilteration.Checked Then 'modified date checked

                        strTimeExpression = strTimeExpression + "or DM.bintModifiedDate between '{FROM}' and '{TO}' "
                        strDateQuery = strDateQuery + "or DM.bintModifiedDate between  '" & strFromDate & "' and '" & CDate(strToDate & " 23:59:59") & "' "
                    End If

                    If chkVistedDate.Checked Then 'Visited is checked 

                        strTimeExpression = strTimeExpression + "or DM.numDivisionid in (select numDivisionId from TrackingVisitorsHDR THDR where numDomainId=" & Session("DomainId") & " and THDR.dtCreated between '{FROM}'  and '{TO}') "
                        strDateQuery = strDateQuery + "or DM.numDivisionid in (select numDivisionId from TrackingVisitorsHDR THDR where numDomainId=" & Session("DomainId") & " and THDR.dtCreated between '" & strFromDate & "'  and '" & CDate(strToDate & " 23:59:59") & "')"

                    End If
                    Session("TimeExpression") = " and (" & strTimeExpression.TrimStart(New Char() {"o", "r"}) & ")"
                    Session("TimeQuery") = " and (" & strDateQuery.TrimStart(New Char() {"o", "r"}) & ")"
                    Return Session("TimeQuery")
                End If
                Return ""
            Catch ex As Exception
                Throw ex
            End Try
        End Function
    </script>
    <style type="text/css">
        .form-inline {
            min-height: 45px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="DetailPageTitle" runat="server"
    ClientIDMode="Static">
    Advanced Search
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="UtilityLinksPanel" runat="server">
    <uc2:frmMySavedSearch ID="frmMySavedSearch1" runat="server" FormID="1" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="RecordActionPanel" runat="server"
    ClientIDMode="Static">
    <asp:Literal ID="litMessage" runat="server" EnableViewState="False"></asp:Literal>
    <div class="row">
        <div class="col-xs-12 col-sm-6 col-md-3">
            <div class="form-inline">
                <label></label>
                <asp:RadioButtonList ID="rdlReportType" AutoPostBack="False" runat="server" CssClass="normal1" RepeatLayout="Table" RepeatDirection="Horizontal">
                    <asp:ListItem Value="1" Selected="True"><a href="javascript:void(0);" onclick="javascript:document.getElementById('rdlReportType_0').checked=true;OpenSelUser(6)">User</a></asp:ListItem>
                    <asp:ListItem Value="2"><a href="javascript:void(0);" onclick="javascript:document.getElementById('rdlReportType_1').checked=true;OpenSelTeam(6)">Team Selected</a></asp:ListItem>
                    <asp:ListItem Value="3"><a href="javascript:void(0);" onclick="javascript:document.getElementById('rdlReportType_2').checked=true;OpenTerritory(6)">Territory Selected</a></asp:ListItem>
                </asp:RadioButtonList>
            </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-3">
            <div class="form-inline">
                <label>Open Action Item Activity:</label>
                <asp:DropDownList ID="ddlActivity" runat="server" CssClass="form-control">
                </asp:DropDownList>
            </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-3">
            <div class="form-inline">
                <label>Select Email Group</label>
                <asp:DropDownList ID="ddlEmailGroup" runat="server" CssClass="form-control" AutoPostBack="true">
                </asp:DropDownList>
            </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-3">
            <div class="pull-right">
                <asp:Button ID="btnFindDuplicates" runat="server" Text="Find Duplicates" class="btn btn-primary" OnClientClick="return confirm('Note:Duplicate record search will follow configuration from Administration->Global Settings, Conditions set in this form will only aply for Advance Search. Would you like to proceed?');" />
                <asp:LinkButton ID="btnSearch" runat="server" CausesValidation="True" CssClass="btn btn-primary" OnClientClick="SetUserSearchCriteria();Search();"><i class="fa fa-search"></i>&nbsp;&nbsp;Search</asp:LinkButton>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="TabsPlaceHolder" runat="server"
    ClientIDMode="Static">
    <asp:UpdateProgress ID="UpdateProgress" runat="server" ClientIDMode="Static">
        <ProgressTemplate>
            <div class="overlay">
                <div class="overlayContent" style="color: #000; text-align: center; width: 250px; padding: 20px">
                    <i class="fa fa-2x fa-refresh fa-spin"></i>
                    <h3>Processing Request</h3>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <div class="row">
        <div class="col-xs-12">
            <asp:ValidationSummary runat="server" ID="valsummery" CssClass="normal4" />
        </div>
    </div>
    <div id="tblSearch" runat="server">
        <div class="box box-primary box-solid">
            <div class="box-header with-border">
                <h3 class="box-title">General</h3>

                <div class="box-tools pull-right">
                </div>
                <!-- /.box-tools -->
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="row">
                    <div class="col-sm-12 col-md-6">
                        <div class="row">
                            <div class="col-xs-12 col-sm-6">
                                <div class="form-group">
                                    <label></label>
                                    <div>
                                        <asp:CheckBox ID="cbSearchInLeads" runat="server" AutoPostBack="False" Checked="True"
                                            Text="Leads "></asp:CheckBox>
                                        <asp:CheckBox ID="cbSearchInProspects" runat="server" AutoPostBack="False" Checked="True"
                                            Text="Prospects"></asp:CheckBox>
                                        <asp:CheckBox ID="cbSearchInAccounts" runat="server" AutoPostBack="False" Checked="True"
                                            Text="Accounts"></asp:CheckBox>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6">
                                <div class="form-group">
                                    <label></label>
                                    <asp:RadioButtonList ID="rbListSearchContext" AutoPostBack="False" runat="server"
                                        CssClass="normal1" RepeatLayout="Table" RepeatDirection="Horizontal">
                                        <asp:ListItem Value="1" Selected="True">Contains</asp:ListItem>
                                        <asp:ListItem Value="2">Starting With</asp:ListItem>
                                    </asp:RadioButtonList>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-6">
                                <div class="form-group">
                                    <label>Relationship</label>
                                    <asp:DropDownList ID="ddlRelationship" CssClass="form-control" runat="server" AutoPostBack="False">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6">
                                <div class="form-group">
                                    <label>Group</label>
                                    <asp:DropDownList ID="ddlLead" CssClass="form-control" runat="server" AutoPostBack="False">
                                        <asp:ListItem Value="0" Selected="True">All Groups</asp:ListItem>
                                        <asp:ListItem Value="1">My Web Leads</asp:ListItem>
                                        <asp:ListItem Value="2">Public Leads</asp:ListItem>
                                        <asp:ListItem Value="5">My Leads</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-6">
                                <div class="form-group">
                                    <label>Miles</label>
                                    <asp:DropDownList ID="ddlMiles" CssClass="form-control" runat="server" AutoPostBack="False">
                                        <asp:ListItem Text="-- Select One --" Value="0"></asp:ListItem>
                                        <asp:ListItem Text="5" Value="5"></asp:ListItem>
                                        <asp:ListItem Text="10" Value="10"></asp:ListItem>
                                        <asp:ListItem Text="15" Value="15"></asp:ListItem>
                                        <asp:ListItem Text="25" Value="25"></asp:ListItem>
                                        <asp:ListItem Text="50" Value="50"></asp:ListItem>
                                        <asp:ListItem Text="75" Value="75"></asp:ListItem>
                                        <asp:ListItem Text="100" Value="100"></asp:ListItem>
                                        <asp:ListItem Text="500" Value="500"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6">
                                <div class="form-group">
                                    <label>From ZipCode</label>
                                    <asp:TextBox runat="server" ID="txtzipcode" CssClass="form-control" MaxLength="5"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6">
                        <div class="box box-default box-solid">
                            <div class="box-header with-border">
                                <div class="box-title">
                                    <input type="radio" id="cbCreatedOnFilteration" runat="server" name="DateFilterAction" value="Created" >Created&nbsp;&nbsp;
                                    <input type="radio" id="cbModifiedOnFilteration" runat="server" name="DateFilterAction" value="Modified">Modified&nbsp;&nbsp;
                                    <input type="radio" id="chkVistedDate" runat="server" name="DateFilterAction" value="Visited">Visited&nbsp;&nbsp;
                                    <input type="radio" id="chkorgsales" runat="server" name="DateFilterAction" value="Exclude Organizations with Sales Orders created within these dates">
                                    <label style="width: 315px;font-weight: 400;margin-bottom: 0px;padding-bottom: 0px;display:inline-table">
                                            Exclude Organizations with Sales Orders created within these dates</label>
                                </div>
                                <div class="box-tools pull-right">
                                </div>
                                <!-- /.box-tools -->
                            </div>
                            <div class="box-body">
                                <div class="row">
                                    <div class="col-xs-12">
                                        <div class="form-inline">
                                            <label>
                                                <asp:RadioButton Text="" runat="server" ID="rbFixedDate" CssClass="signup" GroupName="dateGroup" Checked="true" />From</label>
                                            <BizCalendar:Calendar ID="calFrom" runat="server" ClientIDMode="AutoID" />
                                            <label>To</label>
                                            <BizCalendar:Calendar ID="calTo" runat="server" ClientIDMode="AutoID" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12">
                                        <div class="form-inline">
                                            <label>
                                                <asp:RadioButton Text="" runat="server" ID="rbSlidingDate" CssClass="signup" GroupName="dateGroup" /></label>
                                            <asp:DropDownList runat="server" ID="ddlDateSpan" CssClass="form-control">
                                                <asp:ListItem Text="Today" Value="1" />
                                                <asp:ListItem Text="Week (Last 7 days)" Value="7" />
                                                <asp:ListItem Text="Month (Last 30 days)" Value="30" />
                                                <asp:ListItem Text="Quarter (Last 90 days)" Value="90" />
                                                <asp:ListItem Text="Last 6 months" Value="180" />
                                                <asp:ListItem Text="This Year" Value="365" />
                                            </asp:DropDownList>
                                            &nbsp;&nbsp;&nbsp;
                                            <input type="checkbox" id="chkincorg" name="chkincorg" runat="server" class="signup" />&nbsp;&nbsp;&nbsp;Include Organizations with no sales orders
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <asp:PlaceHolder ID="plhControls" runat="server"></asp:PlaceHolder>
    </div>

    <div class="box box-primary collapsed-box box-solid">
        <div class="box-header with-border">
            <h3 class="box-title">Commerce Marketing</h3>

            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse">
                    <i class="fa fa-plus"></i>
                </button>
            </div>
            <!-- /.box-tools -->
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <div class="row">
                <div class="col-xs-12">
                    <div class="form-inline">
                        <asp:CheckBox ID="cbOptionOne" runat="server" Text="" />
                        Generate a list of Accounts that haven�t bought from us in more than
                                                <asp:TextBox runat="Server" ID="txtDaysOptionOne" MaxLength="3" Width="40" CssClass="form-control"></asp:TextBox>
                        days.
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <div class="form-inline">
                        <asp:CheckBox ID="cbOptionTwo" runat="server" Text="" />
                        Generate a list of Accounts that have purchased more than
                                                <asp:TextBox ID="txtDealAmount" runat="Server" CssClass="form-control" MaxLength="15"
                                                    Width="70"></asp:TextBox>
                        (combined deal won amount) within the last
                                                <asp:TextBox runat="Server" ID="txtDaysOptionTwo" MaxLength="3" Width="40" CssClass="form-control"></asp:TextBox>
                        days.
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <div class="form-inline">
                        <asp:CheckBox ID="cbOptionThree" runat="server" Text="" />
                        Generate a list of Accounts that have bought the <a href="javascript: ShowItemList();"
                            class="">following items</a> within the last
                                                <asp:TextBox runat="Server" ID="txtDaysOptionThree" MaxLength="3" Width="40" CssClass="form-control"></asp:TextBox>
                        days.
                    </div>
                </div>
            </div>
            <div class="row" id="trItemList" style="display: none;">
                <div class="col-xs-12">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="form-inline">
                                <telerik:RadComboBox ID="radItem" runat="server" Width="250" DropDownWidth="200px"
                                    EnableScreenBoundaryDetection="true" EnableLoadOnDemand="true" AllowCustomText="True">
                                    <WebServiceSettings Path="../common/Common.asmx" Method="GetItems" />
                                </telerik:RadComboBox>
                                <input type="button" class="btn btn-primary" onclick="Add();" value="Add" />
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="form-group">
                                <label>Added Items</label>
                                <div>
                                    <asp:ListBox ID="lstSelectedfld" runat="server" Width="150" Height="150" CssClass="signup" EnableViewState="False"></asp:ListBox>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /.box-body -->
    <uc1:frmAdvSearchCriteria ID="frmAdvSearchCriteria1" runat="server" />
    <asp:HiddenField ID="hdnCol" runat="server" />
    <asp:HiddenField ID="hdnFilterCriteria" runat="server" />
    <asp:HiddenField ID="hdnSaveSearchCriteria" runat="server" />
</asp:Content>
