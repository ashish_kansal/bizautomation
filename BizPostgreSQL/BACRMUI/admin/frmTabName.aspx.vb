Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Common
Namespace BACRM.UserInterface.Admin

    Partial Public Class frmTabName
        Inherits BACRMPage
        
       
        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            Try
                GetUserRightsForPage(13, 1)

                If m_aryRightsForPage(RIGHTSTYPE.VIEW) = 0 Then
                    Response.Redirect("../admin/authentication.aspx?mesg=AS")
                ElseIf m_aryRightsForPage(RIGHTSTYPE.ADD) = 0 Then
                    btnSave.Visible = False
                    btnSaveClose.Visible = False
                End If

                btnSave.Attributes.Add("Onclick", "return Check()")
                btnSaveClose.Attributes.Add("Onclick", "return Check()")
                If Not IsPostBack Then Loaddata()
                
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Sub Loaddata()
            Try
                Dim ObjAdmin As New CAdmin
                Dim dtTable As DataTable
                ObjAdmin.UserCntID = Session("UserContactId")
                ObjAdmin.DomainID = Session("DomainId")
                dtTable = ObjAdmin.GetDefaultTabName
                'Replacing the Session Table of Default Tabs
                Session("DefaultTab") = dtTable
                If dtTable.Rows.Count > 0 Then
                    txtLead.Text = IIf(IsDBNull(dtTable.Rows(0).Item("vcLead")), "", dtTable.Rows(0).Item("vcLead"))
                    'txtWebLead.Text = IIf(IsDBNull(dtTable.Rows(0).Item("vcWebLead")), "", dtTable.Rows(0).Item("vcWebLead"))
                    'txtPublicLead.Text = IIf(IsDBNull(dtTable.Rows(0).Item("vcPublicLead")), "", dtTable.Rows(0).Item("vcPublicLead"))
                    txtContact.Text = IIf(IsDBNull(dtTable.Rows(0).Item("vcContact")), "", dtTable.Rows(0).Item("vcContact"))
                    txtAccount.Text = IIf(IsDBNull(dtTable.Rows(0).Item("vcAccount")), "", dtTable.Rows(0).Item("vcAccount"))
                    txtProspect.Text = IIf(IsDBNull(dtTable.Rows(0).Item("vcProspect")), "", dtTable.Rows(0).Item("vcProspect"))
                    txtLeadPlural.Text = IIf(IsDBNull(dtTable.Rows(0).Item("vcLeadPlural")), "", dtTable.Rows(0).Item("vcLeadPlural"))
                    txtContactPlural.Text = IIf(IsDBNull(dtTable.Rows(0).Item("vcContactPlural")), "", dtTable.Rows(0).Item("vcContactPlural"))
                    txtAccountPlural.Text = IIf(IsDBNull(dtTable.Rows(0).Item("vcAccountPlural")), "", dtTable.Rows(0).Item("vcAccountPlural"))
                    txtProspectPlural.Text = IIf(IsDBNull(dtTable.Rows(0).Item("vcProspectPlural")), "", dtTable.Rows(0).Item("vcProspectPlural"))
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
            Try
                Dim ObjAdmin As New CAdmin
                ObjAdmin.DomainID = Session("DomainId")
                ObjAdmin.strLead = txtLead.Text
                ObjAdmin.strContact = txtContact.Text
                ObjAdmin.strProspect = txtProspect.Text
                ObjAdmin.strAccount = txtAccount.Text
                ObjAdmin.strLeadPlural = txtLeadPlural.Text
                ObjAdmin.strContactPlural = txtContactPlural.Text
                ObjAdmin.strProspectPlural = txtProspectPlural.Text
                ObjAdmin.strAccountPlural = txtAccountPlural.Text
                ObjAdmin.Mode = 1
                ObjAdmin.ManageDefaultTabName()
                Loaddata()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub btnSaveClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveClose.Click
            Try
                Dim ObjAdmin As New CAdmin
                ObjAdmin.DomainID = Session("DomainId")
                ObjAdmin.strLead = txtLead.Text
                ObjAdmin.strContact = txtContact.Text
                ObjAdmin.strProspect = txtProspect.Text
                ObjAdmin.strAccount = txtAccount.Text
                ObjAdmin.strLeadPlural = txtLeadPlural.Text
                ObjAdmin.strContactPlural = txtContactPlural.Text
                ObjAdmin.strProspectPlural = txtProspectPlural.Text
                ObjAdmin.strAccountPlural = txtAccountPlural.Text
                ObjAdmin.Mode = 1
                ObjAdmin.ManageDefaultTabName()

                'Replacing the Session Table of Default Tabs
                Dim dtTable As DataTable
                ObjAdmin.DomainID = Session("DomainId")
                dtTable = ObjAdmin.GetDefaultTabName
                Session("DefaultTab") = dtTable
                Response.Write("<script>window.close()</script>")
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

    End Class
End Namespace