﻿Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Admin
Imports Telerik.Web.UI
Imports BACRM.BusinessLogic.Opportunities

Namespace BACRM.UserInterface.Admin
    Public Class frmAssigntoContact
        Inherits BACRMPage

        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            Try
                If Not IsPostBack Then
                    FillData(0, 0)
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
            Try
                Dim objAdmin As New CAdmin
                objAdmin.DomainID = Session("DomainID")
                If objAdmin.ManageClass() Then

                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Public Sub FillContact(ByVal ddlCombo As RadComboBox)
            Try
                Dim fillCombo As New COpportunities
                With fillCombo
                    .DivisionID = radCmbCompany.SelectedValue
                    ddlCombo.DataSource = fillCombo.ListContact().Tables(0).DefaultView()
                    ddlCombo.DataTextField = "Name"
                    ddlCombo.DataValueField = "numcontactId"
                    ddlCombo.DataBind()
                End With
                ddlCombo.Items.Insert(0, New RadComboBoxItem("---Select One---", "0"))
                If ddlCombo.Items.Count >= 2 Then
                    ddlCombo.Items(1).Selected = True
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Protected Sub radCmbCompany_SelectedIndexChanged(sender As Object, e As RadComboBoxSelectedIndexChangedEventArgs)
            Try
                FillContact(radcmbContact)
            Catch ex As Exception

            End Try
        End Sub

        Protected Sub btnAdd_Click(sender As Object, e As EventArgs)
            FillData(0, radcmbContact.SelectedValue)
        End Sub

        Protected Sub btnRemove_Click(sender As Object, e As EventArgs)
            FillData(ddlAssignTo.SelectedValue, 0)
        End Sub

        Public Sub FillData(ByVal AssignTo As Long, ByVal ContactId As Long)
            Dim dt As New DataTable
            Dim commonD As New CCommon
            dt = commonD.ConOverrideEmpList(Session("DomainID"), AssignTo, ContactId)
            ddlAssignTo.Items.Clear()

            ddlAssignTo.DataSource = dt
            ddlAssignTo.DataTextField = "Name"
            ddlAssignTo.DataValueField = "numAssignedContact"
            ddlAssignTo.DataBind()
            ddlAssignTo.Items.Insert(0, New ListItem("--Select Item--", "0"))
        End Sub

    End Class
End Namespace
