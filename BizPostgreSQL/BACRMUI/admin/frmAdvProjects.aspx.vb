
Imports BACRM.BusinessLogic.Item
Imports BACRM.BusinessLogic.Opportunities
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Admin
Namespace BACRM.UserInterface.Admin

    Partial Public Class frmAdvProjects
        Inherits BACRMPage
        Dim objGenericAdvSearch As New FormGenericAdvSearch
        Dim dtGenericFormConfig As DataTable
        Public dsGenericFormConfig As DataSet
        Dim objCommon As CCommon


        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            Try
                objCommon = New CCommon
                GetUserRightsForPage(9, 9)

                If Not IsPostBack Then
                    If m_aryRightsForPage(RIGHTSTYPE.VIEW) = 0 Then Response.Redirect("../admin/authentication.aspx?mesg=AS", False)

                    calFrom.SelectedDate = DateAdd(DateInterval.Month, -3, Now)
                    calTo.SelectedDate = Now
                End If

                GetFormFieldList()
                dtGenericFormConfig = dsGenericFormConfig.Tables(1)

                GenericFormControlsGeneration.FormID = 18                                        'set the form id to Advance Search
                GenericFormControlsGeneration.DomainID = Session("DomainID")                    'Set the domain id
                GenericFormControlsGeneration.UserCntID = Session("UserContactID")                        'Set the User id
                GenericFormControlsGeneration.AuthGroupId = Session("UserGroupID")              'Set the User Authentication Group Id

                callFuncForFormGenerationNonAOI(dsGenericFormConfig)                        'Calls function for form generation and display for non AOI fields
                litMessage.Text = GenericFormControlsGeneration.getJavascriptArray(dsGenericFormConfig.Tables(1)) 'Create teh javascript array and store
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Sub GetFormFieldList()
            Try
                If objGenericAdvSearch Is Nothing Then objGenericAdvSearch = New FormGenericAdvSearch

                objGenericAdvSearch.AuthenticationGroupID = Session("UserGroupID")
                objGenericAdvSearch.FormID = 18
                objGenericAdvSearch.DomainID = Session("DomainID")
                dsGenericFormConfig = objGenericAdvSearch.GetAdvancedSearchFieldList()

                dsGenericFormConfig.Tables(0).TableName = "SectionInfo"                           'give a name to the datatable
                dsGenericFormConfig.Tables(1).TableName = "AdvSearchFields"                           'give a name to the datatable

                dsGenericFormConfig.Tables(1).Columns.Add("vcNewFormFieldName", System.Type.GetType("System.String"), "[vcFormFieldName]") 'This columsn has to be an integer to tryign to manipupate the datatype as Compute(Max) does not work on string datatypes
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Sub callFuncForFormGenerationNonAOI(ByVal dsGenericFormConfig As DataSet)
            Try
                GenericFormControlsGeneration.boolAOIField = 0                                      'Set the AOI flag to non AOI
                GenericFormControlsGeneration.createDynamicFormControlsSectionWise(dsGenericFormConfig, plhControls, 18, Session("DomainID"), Session("UserContactID"))
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Private Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
            Try
                Dim str As String = ""
                If calFrom.SelectedDate <> "" And calTo.SelectedDate <> "" And radCreated.Checked = True Then
                    str = str & " and ProMas.bintCreatedDate between  '" & calFrom.SelectedDate & "'::TIMESTAMP and '" & calTo.SelectedDate & "'::TIMESTAMP"
                End If

                If calFrom.SelectedDate <> "" And calTo.SelectedDate <> "" And radDue.Checked = True Then
                    str = str & " and ProMas.intDueDate between  '" & calFrom.SelectedDate & "'::TIMESTAMP and '" & calTo.SelectedDate & "'::TIMESTAMP"
                End If

                Dim i As Integer
                Dim ControlType, strFieldName, strFieldValue, strFieldID, strFriendlyName, strControlID, v_Prefix As String
                Dim dr As System.Data.DataRow

                For i = 0 To dtGenericFormConfig.Rows.Count - 1
                    dr = dtGenericFormConfig.Rows(i)

                    v_Prefix = "ProMas."

                    If CCommon.ToString(dr("vcLookBackTableName")) = "AdditionalContactsInformation" Then
                        v_Prefix = "ADC."
                    ElseIf CCommon.ToString(dr("vcLookBackTableName")) = "DivisionMaster" Then
                        v_Prefix = "DM."
                    ElseIf CCommon.ToString(dr("vcLookBackTableName")) = "CompanyInfo" Then
                        v_Prefix = "C."
                    ElseIf CCommon.ToString(dr("vcLookBackTableName")) = "ProjectsMaster" Then
                        v_Prefix = "ProMas."
                    End If

                    strFieldID = dr("numFormFieldID").ToString.Replace("C", "").Replace("D", "").Replace("R", "")
                    ControlType = dr("vcAssociatedControlType").ToString.ToLower.Replace(" ", "") 'replace whitespace with blank 
                    strFieldName = v_Prefix & dr("vcDbColumnName").ToString.Trim.Replace(" ", "_")
                    strFriendlyName = dr("vcFormFieldName").ToString


                    strControlID = dr("numFormFieldID").ToString & "_" & dr("vcDbColumnName").ToString.Trim.Replace(" ", "_")

                    If ControlType = "selectbox" Or ControlType = "checkbox" Then
                        If dr("vcFieldType") = "R" Then
                            If CType(tblMain.FindControl(strControlID), DropDownList).SelectedIndex > 0 Then
                                str = str & " and " & strFieldName & " = " & CType(tblMain.FindControl(strControlID), DropDownList).SelectedValue
                            End If
                        ElseIf dr("vcFieldType") = "C" Then
                            If CType(tblMain.FindControl(strControlID), DropDownList).SelectedIndex > 0 Then
                                str = str & " and ProMas.numProId in (select RecId from CFW_FLD_Values_Pro where Fld_ID =" & Replace(Replace(dr("numFormFieldID"), "C", ""), "D", "") & " and Fld_Value = '" & CType(tblMain.FindControl(strControlID), DropDownList).SelectedValue & "' )"
                            End If
                        End If
                    ElseIf ControlType = "datefield" Then
                        strFieldValue = CType(tblMain.FindControl(strControlID), BACRM.Include.calandar).SelectedDate

                        If dr("vcFieldType") = "R" Then
                            If CCommon.ToString(strFieldValue).Length > 0 Then
                                Dim dt As Date = DateFromFormattedDate(strFieldValue, Session("DateFormat"))
                                str = str & " and " & strFieldName & " = '" & dt.Year.ToString + "-" + dt.Month.ToString + "-" + dt.Day.ToString + "'"
                            End If
                        ElseIf dr("vcFieldType") = "O" Then
                            If CCommon.ToString(strFieldValue).Length > 0 Then
                                Dim dt As Date = DateFromFormattedDate(strFieldValue, Session("DateFormat"))
                                str = str & " and OppMas.numOppID in (select RecId from CFW_Fld_Values_Opp where Fld_ID =" & strFieldID & " and fld_value='" & dt.Year.ToString + "-" + dt.Month.ToString + "-" + dt.Day.ToString + "')"
                            End If
                        End If
                    ElseIf ControlType = "checkboxlist" Then
                        strFieldValue = ""
                        Dim chkbl As CheckBoxList = CType(tblMain.FindControl(strControlID), CheckBoxList)

                        For Each item As ListItem In chkbl.Items
                            If item.Selected Then
                                strFieldValue = strFieldValue & If(strFieldValue.Length > 0, " OR ", "") & " CONCAT(',',Fld_Value,',') ILIKE '%," & item.Value & ",%'"
                            End If
                        Next

                        If strFieldValue.Length > 0 Then
                            If dtGenericFormConfig.Rows(i).Item("vcFieldType") = "C" Then
                                str = str & " and ProMas.numProId in (select RecId from CFW_FLD_Values_Pro where Fld_ID =" & Replace(Replace(dtGenericFormConfig.Rows(i).Item("numFormFieldID"), "C", ""), "D", "") & " AND (" & strFieldValue & "))"
                            ElseIf dr("vcFieldType") = "O" Then
                                str = str & " and OppMas.numOppID in (select RecId from CFW_Fld_Values_Opp where Fld_ID =" & strFieldID & " AND (" & strFieldValue & "))"
                            End If
                        End If
                    Else
                        If CType(tblMain.FindControl(strControlID), TextBox).Text <> "" Then
                            If dr("vcFieldType") = "R" Then
                                str = str & " and " & strFieldName & " ILIKE  '%" & Replace(CType(tblMain.FindControl(strControlID), TextBox).Text, "'", "''") & "%' "
                            ElseIf dr("vcFieldType") = "C" Then
                                str = str & " and ProMas.numProId in (select RecId from CFW_FLD_Values_Pro where Fld_ID =" & Replace(Replace(dr("numFormFieldID"), "C", ""), "D", "") & " and Fld_Value ILIKE '%" & Replace(CType(tblMain.FindControl(strControlID), TextBox).Text, "'", "''") & "%' )"
                            End If
                        End If
                    End If
                Next
                Session("WhereContditionPro") = str
                Response.Redirect("../Admin/frmAdvProjectsRes.aspx")
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Private Sub DisplayError(ByVal exception As String)
            Try
                DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = exception
                DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
                DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Focus()
            Catch ex As Exception

            End Try
        End Sub
    End Class
End Namespace