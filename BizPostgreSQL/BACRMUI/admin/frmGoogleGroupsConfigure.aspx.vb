﻿Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Contacts
Imports BACRM.BusinessLogic.Admin
Public Class frmGoogleGroupsConfigure
    Inherits BACRMPage

    Dim lngUserContactID As Long
    Dim intMode As Int16
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            lngUserContactID = CCommon.ToLong(GetQueryStringVal("UserId"))    'Take the user id from the querystring
            intMode = CCommon.ToShort(GetQueryStringVal("Mode"))
            If Not Page.IsPostBack Then
                PopulateGroups()
                LoadInformation()
            End If
            If intMode = 1 Then
                cellCaption.InnerText = "Biz to Google Group Configuration"
                ddlGroups.Visible = True
                cblGroups.Visible = False
            Else
                cellCaption.InnerText = "Google to Biz Groups Configuration"
                ddlGroups.Visible = False
                cblGroups.Visible = True
            End If

            btnClose.Attributes.Add("onclick", "Close();")
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Sub LoadInformation()
        Try
            Dim dtUserAccessDetails As DataTable
            Dim objUserAccess As New UserAccess

            objUserAccess = New UserAccess
            objUserAccess.UserId = lngUserContactID
            objUserAccess.DomainID = Session("DomainID")
            objUserAccess.GroupType = intMode
            dtUserAccessDetails = objUserAccess.GetGoogleContactGroups

            If dtUserAccessDetails.Rows.Count > 0 Then
                If intMode = 1 Then
                    If ddlGroups.Items.FindByValue(dtUserAccessDetails.Rows(0).Item("vcGroupId")) IsNot Nothing Then
                        ddlGroups.ClearSelection()
                        ddlGroups.Items.FindByValue(dtUserAccessDetails.Rows(0).Item("vcGroupId")).Selected = True
                    End If
                ElseIf intMode = 2 Then
                    For i As Integer = 0 To dtUserAccessDetails.Rows.Count - 1
                        If cblGroups.Items.FindByValue(dtUserAccessDetails.Rows(i).Item("vcGroupId")) IsNot Nothing Then
                            cblGroups.Items.FindByValue(dtUserAccessDetails.Rows(i).Item("vcGroupId")).Selected = True
                        End If
                    Next
                End If
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
    Sub PopulateGroups()
        Try
            Dim objGContact As New GoogleContact

            If intMode = 1 Then
                ddlGroups.DataTextField = "vcGroupName"
                ddlGroups.DataValueField = "numGroupID"
                ddlGroups.DataSource = objGContact.GoogleGroups(lngUserContactID, Session("DomainID"))
                ddlGroups.DataBind()
                ddlGroups.Items.Insert(0, New ListItem("--Select One--", "0"))
            ElseIf intMode = 2 Then
                cblGroups.DataTextField = "vcGroupName"
                cblGroups.DataValueField = "numGroupID"
                cblGroups.DataSource = objGContact.GoogleGroups(lngUserContactID, Session("DomainID"))
                cblGroups.DataBind()
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub save()
        Try
            Dim objUserAccess As New UserAccess
            objUserAccess.UserId = lngUserContactID
            objUserAccess.DomainID = Session("DomainID")
            objUserAccess.GroupType = intMode
            'First Delete
            objUserAccess.byteMode = 2

            objUserAccess.ManageGoogleContactGroups()

            'Then Insert
            objUserAccess.byteMode = 1

            If intMode = 1 Then
                objUserAccess.GoogleGroupID = ddlGroups.SelectedValue
                objUserAccess.ManageGoogleContactGroups()
            ElseIf intMode = 2 Then
                For Each Item As ListItem In cblGroups.Items
                    If Item.Selected Then
                        objUserAccess.GoogleGroupID = Item.Value
                        objUserAccess.ManageGoogleContactGroups()
                    End If
                Next
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSave.Click
        Try
            save()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
        End Try
    End Sub
End Class