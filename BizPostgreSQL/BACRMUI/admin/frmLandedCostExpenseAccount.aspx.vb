﻿Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Accounting
Imports BACRM.BusinessLogic.Opportunities
Imports BACRM.BusinessLogic.Admin

Public Class frmLandedCostExpenseAccount
    Inherits BACRMPage

    Dim dtChartAcntDetails As DataTable
    Dim dtItems As New DataTable

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then
                'CCommon.AddColumnsToDataTable(dtItems, "numAccountID,vcDescription")

                BindGrid(boolPostback:=True)
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub BindGrid(Optional deleteRowIndex As Integer = -1, Optional boolAddNew As Boolean = False, Optional boolPostback As Boolean = False)
        Dim j As Integer
        Dim drRow As DataRow
        Try
            Dim dtTable As DataTable
            Dim objUserAccess As New UserAccess
            objUserAccess.DomainID = Session("DomainID")
            dtTable = objUserAccess.GetDomainDetails()
            If Not rblLandedCost.Items.FindByValue(dtTable.Rows(0).Item("vcLanedCostDefault")) Is Nothing Then
                rblLandedCost.ClearSelection()
                rblLandedCost.Items.FindByValue(dtTable.Rows(0).Item("vcLanedCostDefault")).Selected = True
            End If
            dtItems.Rows.Clear()

            Dim objLC As New LandedCost
            objLC.DomainID = Session("DomainID")
            dtItems = objLC.GetLandedCostExpenseAccount

            If boolPostback = False Then
                dtItems.Rows.Clear()

                Dim gvRow As GridViewRow
                Dim dtrow As DataRow

                For i As Integer = 0 To gvLandedExpense.Rows.Count - 1
                    If i <> deleteRowIndex Then
                        dtrow = dtItems.NewRow
                        gvRow = gvLandedExpense.Rows(i)

                        dtrow.Item("numLCExpenseId") = CCommon.ToLong(CType(gvRow.FindControl("hdnLCExpenseId"), HiddenField).Value)
                        dtrow.Item("numAccountID") = CCommon.ToLong(CType(gvRow.FindControl("ddlExpenseAccount"), DropDownList).SelectedValue)
                        dtrow.Item("vcDescription") = CType(gvRow.FindControl("txtDescription"), TextBox).Text

                        If CCommon.ToLong(CType(gvRow.FindControl("ddlExpenseAccount"), DropDownList).SelectedValue) > 0 Then
                            dtItems.Rows.Add(dtrow)
                        End If
                    End If
                Next
            End If

            If boolAddNew Or dtItems.Rows.Count = 0 Then
                drRow = dtItems.NewRow

                dtItems.Rows.Add(drRow)
            End If

            gvLandedExpense.DataSource = dtItems
            gvLandedExpense.DataBind()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub gvLandedExpense_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvLandedExpense.RowCommand
        Try
            If e.CommandName = "DeleteRow" Then
                Dim row As GridViewRow = CType(CType(e.CommandSource, Control).NamingContainer, GridViewRow)

                BindGrid(deleteRowIndex:=row.DataItemIndex)
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub gvLandedExpense_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvLandedExpense.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim ddlExpenseAccount As DropDownList = DirectCast(e.Row.FindControl("ddlExpenseAccount"), DropDownList)

                If dtChartAcntDetails Is Nothing Then
                    Dim objCOA As New ChartOfAccounting
                    objCOA.DomainID = Session("DomainID")
                    objCOA.AccountCode = "0104" 'Expense
                    dtChartAcntDetails = objCOA.GetParentCategory()

                    Dim dt1 As DataTable
                    objCOA.AccountCode = "0106" ' COGS
                    dt1 = objCOA.GetParentCategory()
                    dtChartAcntDetails.Merge(dt1)

                    objCOA.AccountCode = "0103" ' Income
                    dt1 = objCOA.GetParentCategory()
                    dtChartAcntDetails.Merge(dt1)


                    objCOA.AccountCode = "0101" ' Asset
                    dt1 = objCOA.GetParentCategory()
                    'Exclude AR 
                    Dim drArray() As DataRow = dt1.Select("vcAccountTypeCode <> '01010105'")
                    If drArray.Length > 0 Then
                        dt1 = drArray.CopyToDataTable()
                    End If
                    dtChartAcntDetails.Merge(dt1)

                    objCOA.AccountCode = "0102" ' Liability
                    dt1 = objCOA.GetParentCategory()
                    'Exclude AP
                    drArray = dt1.Select("vcAccountTypeCode <>'01020102'")
                    If drArray.Length > 0 Then
                        dt1 = drArray.CopyToDataTable()
                    End If
                    dtChartAcntDetails.Merge(dt1)

                End If

                Dim item As ListItem
                For Each dr As DataRow In dtChartAcntDetails.Rows
                    item = New ListItem()
                    item.Text = dr("vcAccountName")
                    item.Value = dr("numAccountID")
                    item.Attributes("OptionGroup") = dr("vcAccountType")
                    ddlExpenseAccount.Items.Add(item)
                Next
                ddlExpenseAccount.Items.Insert(0, New ListItem("--Select One --", "0"))

                If CCommon.ToLong(CType(e.Row.DataItem, DataRowView).Row("numAccountId")) > 0 Then
                    If Not ddlExpenseAccount.Items.FindByValue(CCommon.ToLong(CType(e.Row.DataItem, DataRowView).Row("numAccountId"))) Is Nothing Then
                        ddlExpenseAccount.Items.FindByValue(CCommon.ToLong(CType(e.Row.DataItem, DataRowView).Row("numAccountId"))).Selected = True
                    End If
                End If
            End If

        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub btnAdd_Click1(sender As Object, e As EventArgs) Handles btnAdd.Click
        Try
            BindGrid(boolAddNew:=True)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Protected Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        Try
            Dim dtDetails As New DataTable

            Dim gvRow As GridViewRow
            Dim dtrow As DataRow

            CCommon.AddColumnsToDataTable(dtDetails, "numLCExpenseId,vcDescription,numAccountId")

            For i As Integer = 0 To gvLandedExpense.Rows.Count - 1
                dtrow = dtDetails.NewRow
                gvRow = gvLandedExpense.Rows(i)

                dtrow.Item("numLCExpenseId") = CCommon.ToLong(CType(gvRow.FindControl("hdnLCExpenseId"), HiddenField).Value)
                dtrow.Item("numAccountID") = CCommon.ToLong(CType(gvRow.FindControl("ddlExpenseAccount"), DropDownList).SelectedValue)
                dtrow.Item("vcDescription") = CType(gvRow.FindControl("txtDescription"), TextBox).Text


                If CCommon.ToLong(CType(gvRow.FindControl("ddlExpenseAccount"), DropDownList).SelectedValue) > 0 Then
                    dtDetails.Rows.Add(dtrow)
                End If
            Next

            'If dtDetails.Rows.Count <= 0 Then
            '    litMessage.Visible = True
            '    litMessage.Text = "Please provide Expense Account Details."
            '    Return
            'End If

            Dim objLC As New LandedCost

            With objLC
                .DomainID = Session("DomainID")

                Dim ds As New DataSet
                ds.Tables.Add(dtDetails)
                .strItems = ds.GetXml()
                .LanedCostDefault = rblLandedCost.SelectedValue
                .SaveLandedCostExpenseAccount()
            End With
            Session("LanedCostDefault") = rblLandedCost.SelectedValue

            BindGrid(boolPostback:=True)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
End Class